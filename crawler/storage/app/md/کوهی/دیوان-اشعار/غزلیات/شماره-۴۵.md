---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>بر رخ میان قطره دریا وجود ما است</p></div>
<div class="m2"><p>فرقی مکن که قطره ز دریا کجا جدا است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستی یکی است هر چه جز او نیستی بود</p></div>
<div class="m2"><p>زانرو که اعتبار تعین همه بپا است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن مه لقا چو مردم چشم است دیده را</p></div>
<div class="m2"><p>مانند آفتاب که او عین ذره ها است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذات و صفات نقطه واحد بود بدان</p></div>
<div class="m2"><p>وان نقطه هم ز سرعت خود دایره نماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عرش خدا دل است از آن منقلب بود</p></div>
<div class="m2"><p>آنجا بدانکه رمز علی العرش استوا است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز انرو که انفکار سران جمال را</p></div>
<div class="m2"><p>جسمت که ظلمت آمد و جان تو در صفا است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون باطل است ظاهر کوهی ز روی صدق</p></div>
<div class="m2"><p>از هر چه دید اول و آخر همه خدا است</p></div></div>