---
title: >-
    شمارهٔ ۱۲۷
---
# شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>دل از محبت دنیا و آخرت بردار</p></div>
<div class="m2"><p>بشو باشک نیاز و به بین بطلعت یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چو زلف از رخ زیبای تو سر بر گردیم</p></div>
<div class="m2"><p>صفت از ذات تو هرگز نشود مایل یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهوای قد سرو تو چو در خاک رویم</p></div>
<div class="m2"><p>سر برآریم بمهر تو چو گل از گل یار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فاعل مطلق ما او است عیان می بینم</p></div>
<div class="m2"><p>هر چه خواهد بکند خاطر آن فاعل یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن امانت که خدا عرض باشیا میکرد</p></div>
<div class="m2"><p>هالک آمد همه خود بود بر آن حایل یار</p></div></div>