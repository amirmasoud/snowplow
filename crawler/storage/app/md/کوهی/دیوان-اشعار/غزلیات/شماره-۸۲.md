---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>خوش حال آن کسانی کز دام تن رهیدند</p></div>
<div class="m2"><p>چون شاهباز قدسی در لامکان رسیدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن سالکان وحدت دانی کی اند ایدل</p></div>
<div class="m2"><p>آری جنید و شبلی معروف بایزیدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بحر محیط وحدت موج و حباب دارد</p></div>
<div class="m2"><p>امواج بحر بودند در بحر آرمیدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جاوید زنده گشتند در بحر لایزالی</p></div>
<div class="m2"><p>چون ازید خداوند جام وفا چشیدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ممکن تعین یکباره در گذشتند</p></div>
<div class="m2"><p>حق را بچشم واجب بی واسطه بدیدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذرات و سایه هر دو بود اعتبار وهمی</p></div>
<div class="m2"><p>در آفتاب مطلق جاوید ناپدیدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلق جدید بشنید کوهی و زنده گردید</p></div>
<div class="m2"><p>چون دید او که یاران بار دیگر چودیدند</p></div></div>