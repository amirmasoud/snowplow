---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>گر چه چون پروانه از شمع وصالت سوختم</p></div>
<div class="m2"><p>شمع هم میسوزد از آه دل آتش فشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما ز لعل یار دندان طمع برکنده ایم</p></div>
<div class="m2"><p>چون بکام دل نمی یابیم بوسی از گران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باسگان کوی او میباش شبها تا بروز</p></div>
<div class="m2"><p>کوهیا می مال روی زرد خود بر آستان</p></div></div>