---
title: >-
    شمارهٔ ۲۱۲
---
# شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>آتش عشق بتان هر دو جهان را سوخته</p></div>
<div class="m2"><p>شمع روی یار پیدا و نهان را سوخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عکس رخسارش نه تنها سوخته گل در چمن</p></div>
<div class="m2"><p>یاد آن رو هر سحر گه بلبلان را سوخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وه چه سر است اینکه شوق وصل حی لایموت</p></div>
<div class="m2"><p>در بهشت عدن دیدم مردمانرا سوخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وصف شیرینی آن لب هر که دارد در دهان</p></div>
<div class="m2"><p>شد یقینم اینکه او کام و زبانرا سوخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لعل سیرابش که آتش پاره ای بود از ازل</p></div>
<div class="m2"><p>در دو عالم دیده پیرو جوان را سوخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشک و آه گرم کوهی چونکه با هم ساختند</p></div>
<div class="m2"><p>در زمان گفتند مردم انس و جانرا سوخته</p></div></div>