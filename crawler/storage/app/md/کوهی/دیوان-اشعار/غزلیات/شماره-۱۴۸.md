---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>مگریز از بلا بجوی خلاص</p></div>
<div class="m2"><p>حق چه فرمود لات حین مناص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه راگشت عشق مردم خوار</p></div>
<div class="m2"><p>بکشد خویش را به پای قصاص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پرند عاقبت به گلشن وصل</p></div>
<div class="m2"><p>جمله مرغان روح او اقصاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رحمت کردکار چون عام است</p></div>
<div class="m2"><p>عام را رحمتی است خاص الخاص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قرص خورشید در سماع آمد</p></div>
<div class="m2"><p>زهره قوال و ماه شد رقاص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مصحف روی او به مکتب عشق</p></div>
<div class="m2"><p>خواند کوهی به صد هزار اخلاص</p></div></div>