---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>صبح صادق حجاب صانع شد</p></div>
<div class="m2"><p>زلف بر روی یار مانع شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرح زلف و رخش بدانستم</p></div>
<div class="m2"><p>دل درویش کان جامع شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به مسمی کجا رسد هرگز</p></div>
<div class="m2"><p>هر که از وی باسم قانع شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دل آفتاب و ماه نگر</p></div>
<div class="m2"><p>لمعه ای زان جمال لامع شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم ز جان بشنود اناالحق را</p></div>
<div class="m2"><p>دل که بگشاد گوش سامع شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت قل یا عبادی آن حضرت</p></div>
<div class="m2"><p>وصل او را دو کون طامع شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دید کوهی حقیقت دل را</p></div>
<div class="m2"><p>شرع را چون بجان مطامع شد</p></div></div>