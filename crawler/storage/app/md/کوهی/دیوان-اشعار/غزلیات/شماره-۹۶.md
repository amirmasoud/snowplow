---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>سحر که بوی گل کز جانب گلزار میآید</p></div>
<div class="m2"><p>ز چین طره مشکین آن دلدار میآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدستی باده ی احمر بدستی مصحف فتوی</p></div>
<div class="m2"><p>زهی ساقی گلرویان که صوفی وا میآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان شد روشن از ظلمت چو آن رو گشاید زلف</p></div>
<div class="m2"><p>که از روی چو خورشیدش هزار انوار میآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آب دیده در کویش که آن خلد برین باشد</p></div>
<div class="m2"><p>دلم جنات تجری تحت الانهار میآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زهر شام سر زلفش هزاران کوه میپوشد</p></div>
<div class="m2"><p>که چون خورشید آنمه رو بصد اظهار میآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز عالم گوشه گیر ای جان بیاد آن خم ابرو</p></div>
<div class="m2"><p>نشین در غار دل کوهی که یار غار میآید</p></div></div>