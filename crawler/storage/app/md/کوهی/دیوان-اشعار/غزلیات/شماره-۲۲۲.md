---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>آفتابی و ماه می طلبی</p></div>
<div class="m2"><p>پادشاهی و شاه می طلبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کل شیئی شهید آیت تست</p></div>
<div class="m2"><p>اگر از ما گواه می طلبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به بینی بدیده ها خود را</p></div>
<div class="m2"><p>سرو چشم سیاه می طلبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قوت جان تو اشک خونین است</p></div>
<div class="m2"><p>ناله و درد و آه می طلبی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو خورشید در جهان فردی</p></div>
<div class="m2"><p>تو نه مال و نه جاه می طلبی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رهنمای همه توئی از ما</p></div>
<div class="m2"><p>چه طریق و چه راه می طلبی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوهیا از جگر غذائی ساز</p></div>
<div class="m2"><p>چند برگ گیاه می طلبی</p></div></div>