---
title: >-
    شمارهٔ ۲۰۸
---
# شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>سلطان عشق خیمه چو در لامکان زده</p></div>
<div class="m2"><p>یک جلوه در جهان مکین و مکان زده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک لمعه از لوامع خورشید روی او</p></div>
<div class="m2"><p>بر ماه و بر ستاره و بر آسمان زده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا برده باد بوی گل روی او به باغ</p></div>
<div class="m2"><p>بلبل هزار نعره بهر بوستان زده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شد یقین که غیر تو کس نیست در جهان</p></div>
<div class="m2"><p>اهل یقین نیند در این ره کمان زده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جام آفتاب می لعل هر زمان</p></div>
<div class="m2"><p>جانم بیاد لعل لب دلستان زده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصف لبش چو روز و شب اندر زبان ماست</p></div>
<div class="m2"><p>زانیم چه غم که درد و جهانم زبان زده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از هر دو کون خاطر کوهی چه فارغست</p></div>
<div class="m2"><p>سر با سگان کوی تو بر آستان زده</p></div></div>