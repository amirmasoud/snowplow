---
title: >-
    شمارهٔ ۱۷۲
---
# شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>تا ببیند او خم ابروی آن مه یک به یک</p></div>
<div class="m2"><p>در سجود افتاد هردم جمله جان‌ها ملک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما بیاد آن دهان در کنج خلوت شسته ایم</p></div>
<div class="m2"><p>تا بیابیم از لب جان بخش او دلبر حنک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیک سودای تو را پختیم ما از آب چشم</p></div>
<div class="m2"><p>نیست اندر مطبخ ما هیچ جز آب و نمک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع رویت تا منور کرد عالم را هنوز</p></div>
<div class="m2"><p>ماه و خورشیدند روشن از تو بر اوج فلک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من که در دریای وحدت غوطه خوردم در ازل</p></div>
<div class="m2"><p>جان ما چون یونس آمد جسم مانند سمک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رست کوهی ازمن و ما تا جمال حق بدید</p></div>
<div class="m2"><p>نیست آنرا همچو خلق این زمانه ریب وشک</p></div></div>