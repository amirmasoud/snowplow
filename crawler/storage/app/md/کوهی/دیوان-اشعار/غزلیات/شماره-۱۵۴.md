---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>سوختم از آتش رخسار مه رویان چو شمع</p></div>
<div class="m2"><p>در میان آتشم با دیده گریان چو شمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفرین بر سوز و ساز ما که شبها تا بروز</p></div>
<div class="m2"><p>شمع گریان است و ما را دولت خندان چو شمع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خانه روشن گردد و جانم شود روشن چو ماه</p></div>
<div class="m2"><p>گردرائی از در تاریک درویشان چو شمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه وصلت بازیابم در شب زلف سیاه</p></div>
<div class="m2"><p>گر کند بر من شبی روی تو نور افشان چو شمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوهیا وقت است کز ماه رخش روشن شوی</p></div>
<div class="m2"><p>چند خواهی سوختن از آتش هجران چو شمع</p></div></div>