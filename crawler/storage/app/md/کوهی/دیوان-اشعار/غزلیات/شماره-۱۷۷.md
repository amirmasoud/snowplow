---
title: >-
    شمارهٔ ۱۷۷
---
# شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>روح اگر از چاه تن افتاد بر اوج فلک</p></div>
<div class="m2"><p>رحم کرد ایزد بر او گفتند الله معک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ نقصان نیست یوسف را ز چه دانسته ایم</p></div>
<div class="m2"><p>سالمش آرد برون چون یونس از بطن سمک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم برنگ خود برآرد صبغت الله عاقبت</p></div>
<div class="m2"><p>شد نمک هر چیز می افتد بدریای نمک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز وجود حق عدم باشد یقین دانسته ام</p></div>
<div class="m2"><p>در تعین عارفان هرگز نباشد هیچ شک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو زر بگداز ز آتش زانکه در بازار عشق</p></div>
<div class="m2"><p>کوهیا صراف دارد در نظر سنگ محک</p></div></div>