---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>نمود صبح سعادت ز غیب دیداری</p></div>
<div class="m2"><p>طلوع کرد چو خورشید روی دلداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر چه دیده جان دید روی دلبر را</p></div>
<div class="m2"><p>ندیده ایم جز او هیچ یار و اغیاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدیر و صومعه دیدم بچشم او او را</p></div>
<div class="m2"><p>گهیش زاهد و عابد گهیش خماری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدام پیشه او عاشقی و معشوقی است</p></div>
<div class="m2"><p>بحسن خود متعلق بخود گرفتاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آفتاب رخ او نداشت مشرق و غرب</p></div>
<div class="m2"><p>ز جان جمله ذرات سر زد انواری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درون سینه کوهی است منزل آن شاه</p></div>
<div class="m2"><p>چنانکه احمد مرسل ز غیر در غاری</p></div></div>