---
title: >-
    شمارهٔ ۱۶۸
---
# شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>بررخ جامع میان خلق و حق</p></div>
<div class="m2"><p>جز محمد نیست بر خوان این سبق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قبله واحد بود موجود و او</p></div>
<div class="m2"><p>زان بفرمانش همی شد ماه شق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهد لولاک آمد رحمة للعالمین</p></div>
<div class="m2"><p>تا امور شرع دین بنهاد با چندین سبق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مقام لی مع الله تربیت کردش کریم</p></div>
<div class="m2"><p>یابد از وی تربیت آنکس که باشد مستحق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرد تعلیمش بدان علم لدنی بی سواد</p></div>
<div class="m2"><p>نی سیاهی و دواتی بود آنجا نه ورق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوهیا در مکتب عشق خدا تعلیم گیر</p></div>
<div class="m2"><p>جز دل بریان منه پیش معلم بر طبق</p></div></div>