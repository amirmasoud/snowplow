---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>ترک عشق رخ زیبا پسران نتوان کرد</p></div>
<div class="m2"><p>جز حدیث لب شکر دهنان نتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقد این عمر گرانمایه که جان جوهر اوست</p></div>
<div class="m2"><p>غیر صرف قدم سیمبران نتوان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چو موئی نشود در غم آن موی میان</p></div>
<div class="m2"><p>چون گهر دست در آن موی میان نتوان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق است آن بت عیار بصدق از همه رو</p></div>
<div class="m2"><p>دیگران را برخ خود نگران نتوان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوهیا لعل بتان خون جگر می نوشد</p></div>
<div class="m2"><p>این سخن در نظر بی جگران نتوان کرد</p></div></div>