---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>روشنی در چشم ما از روی آن مه پیکر است</p></div>
<div class="m2"><p>چونکه آن زهره جبین خود آفتاب اظهر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی خود می بیند او از چشمهای روشنش</p></div>
<div class="m2"><p>روی او در چشم خود دیدم بجانم مظهر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما ره اسم و صفات و فعل را دانسته ایم</p></div>
<div class="m2"><p>ذات پاک حق ز درک ما بسی بالاتر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل سقیهم ربهم حق گفت جانرا داد می</p></div>
<div class="m2"><p>هر دو عالم از خم وحدت بدان یکساغر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه موجودند از پیدا و پنهان فی المثل</p></div>
<div class="m2"><p>بر رخ آنمه لقا چون زلف و خال و عنبر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست از دریای وحدت قطره ی در بحر غرق</p></div>
<div class="m2"><p>گو مسلمانست ترسا گر جهود و کافر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یافت انسان در وجود خویش بر و بحر خود</p></div>
<div class="m2"><p>در کتاب حق تعالی خوانده ام خشک و تر است</p></div></div>