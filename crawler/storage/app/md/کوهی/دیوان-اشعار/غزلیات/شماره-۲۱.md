---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>دل چو شستم ز غیر نقش ادب</p></div>
<div class="m2"><p>گفت ما را ز لوح صاد طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ز اصل و نسب شدم فارغ</p></div>
<div class="m2"><p>گفت لایق شدی بما فارغب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اولم باده داد و سرخوش کرد</p></div>
<div class="m2"><p>بعد از آنهم نهاد لب بر لب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوسها داد بر دهان دلم</p></div>
<div class="m2"><p>با لب خویش داشت عیش و طرب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سحری بود دیدمش روشن</p></div>
<div class="m2"><p>روی چون آفتاب مه منصب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت پرورده ام بشیر و شکر</p></div>
<div class="m2"><p>گفتمش لطف کرده ی یارب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساغری داد پر ز بدر منیر</p></div>
<div class="m2"><p>می روح القدس نه آب عنب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در کشیدم همه خدا دیدم</p></div>
<div class="m2"><p>خواند بر جانم آیت اقرب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم کوهی ندیده در شب و روز</p></div>
<div class="m2"><p>جز رخ و زلف او بروز و به شب</p></div></div>