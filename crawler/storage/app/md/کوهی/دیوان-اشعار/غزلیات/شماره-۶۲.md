---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>عکس لعل لبت ای دوست چو در جان من است</p></div>
<div class="m2"><p>اشکم از دیده خونبار عقین یمن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل من کرد قبا جامه جانرا صد چاک</p></div>
<div class="m2"><p>روح بر قامت دلجوی لب پیرهن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار با ما است شب و روز نمیداند غیر</p></div>
<div class="m2"><p>خلوت ما نشناسد که در انجمن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کجا هست بدلدار قرینم از جان</p></div>
<div class="m2"><p>دل ما در طلب دوست اویس قرن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواجه در باز دل و دین همه در باز و ببین</p></div>
<div class="m2"><p>که حجاب است تو را راه در این چاه تن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چاره کار من بی سرو پا میدانم</p></div>
<div class="m2"><p>ز آتش مهر رخت سوختن و ساختن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو بلبل به چمن ناله کند با کی نیست</p></div>
<div class="m2"><p>روی چون نسترن و زلف برو یار من است</p></div></div>