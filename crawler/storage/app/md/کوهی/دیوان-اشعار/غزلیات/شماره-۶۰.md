---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>مائیم بر صفات و صفات تو عین ما است</p></div>
<div class="m2"><p>دیدم بعینه ای که توئی عین کاینات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عین کاینات عیانی چو آفتاب</p></div>
<div class="m2"><p>ذرات او به پیش تو نه صبر و نه ثبات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد ممتنع ز غیر وجود تو هر چه هست</p></div>
<div class="m2"><p>ای واجب الوجود توئی جان ممکنات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد لایزال اسم تو و لم یزل صفت</p></div>
<div class="m2"><p>اسمت ترقی است نه اسم تنزلات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما غرق بحر وحدتت ای حی لایموت</p></div>
<div class="m2"><p>در جان خویش یافته سرچشمه حیات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باقی است جان صالح و فانی نمی شود</p></div>
<div class="m2"><p>یعنی بر آفتاب بود جان ممکنات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>علم الیقین هر آینه عین الیقین شود</p></div>
<div class="m2"><p>کوهی بچشم دوست چو دیدی صفات ذات</p></div></div>