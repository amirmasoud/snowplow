---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>صبا که شام و سحر مشکبار می آید</p></div>
<div class="m2"><p>ز چین طره آن گلعذار می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آمدم بچمن چون نسیم در گلزار</p></div>
<div class="m2"><p>ز باغ سرو چمن بوی یار می آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حبیب از دل ما همچو ماه سر بر زد</p></div>
<div class="m2"><p>بسان گل که هم از جان خار می آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلی است کز لب آن عندلیب مینالد</p></div>
<div class="m2"><p>اگر چه ناله بلبل هزار می آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز عین ما نظری کرد روی خود را دید</p></div>
<div class="m2"><p>به خویش گفت که غیرم چکار می آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پیش طلعت خورشید چونکه لاشرقیست</p></div>
<div class="m2"><p>غبار چشم برد سرمه وار می آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار پرده اگر هست روی آن مه را</p></div>
<div class="m2"><p>چو آفتاب عیان در کنار می آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز غار سینه کوهی برون مشو جانا</p></div>
<div class="m2"><p>نشین که همدمت آن یار غار می آید</p></div></div>