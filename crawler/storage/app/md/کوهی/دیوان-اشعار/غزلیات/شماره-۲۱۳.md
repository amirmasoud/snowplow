---
title: >-
    شمارهٔ ۲۱۳
---
# شمارهٔ ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>بر تو باد ای جان که دل داری نگاه</p></div>
<div class="m2"><p>هیچ نگذاری زور دلا اله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیر او خود نیست موجودی دگر</p></div>
<div class="m2"><p>گر بچشم خود کنی بر حق نگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر همی خواهی وصال جاودان</p></div>
<div class="m2"><p>از خدا جز وصل او چیزی مخواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو شمعی باش شب ها تا بروز</p></div>
<div class="m2"><p>در میان سوز و اشک و دود و آه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باش همچون آسمان همت بلند</p></div>
<div class="m2"><p>تا برآید از دلت خورشید و ماه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برمه رخسار آن خورشید بین</p></div>
<div class="m2"><p>جمله موجودات یک خال سیاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دل هر ذره آن آفتاب</p></div>
<div class="m2"><p>همچو گل بنمود از برگ گیاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان موجودات از او موجود شد</p></div>
<div class="m2"><p>همچنانکه دانه روید قشر و گاه</p></div></div>