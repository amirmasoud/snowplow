---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>ای دل تو ز همنشین دگرسان گردی</p></div>
<div class="m2"><p>با هر چه نشینی به صفت آن گردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیوسته چو گرد زلف خوبان گردی</p></div>
<div class="m2"><p>این مایه ندانی که پریشان گردی</p></div></div>