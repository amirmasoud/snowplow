---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>ای حلقه مشکین تو دام دل من</p></div>
<div class="m2"><p>محنت‌کده عشق تو نام دل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشکن دل من که آخر ای دوست مدام</p></div>
<div class="m2"><p>پر باده عشق توست جام دل من</p></div></div>