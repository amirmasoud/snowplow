---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>تا هر کست ای شانه نگیرد در دست</p></div>
<div class="m2"><p>کوتاه کن از دو زلف آن دلبر دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست دگری شکافت ای شانه سرت</p></div>
<div class="m2"><p>تو زلف نگار من چه پیچی در دست</p></div></div>