---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>یک رنج تحمل کنی از صد برهی</p></div>
<div class="m2"><p>چون نیک شوی ز صحبت بد برهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نفس تو با تو هیچکس را بد نیست</p></div>
<div class="m2"><p>فارغ شوی از بدی گر از خود برهی</p></div></div>