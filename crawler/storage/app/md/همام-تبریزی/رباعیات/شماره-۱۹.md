---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>عشق تو که سرمایه این درویش است</p></div>
<div class="m2"><p>زاندازه هر هوس‌پرستی بیش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیزی‌ست که از ازل مرا در سر بود</p></div>
<div class="m2"><p>کاری‌ست که تا ابد مرا در پیش است</p></div></div>