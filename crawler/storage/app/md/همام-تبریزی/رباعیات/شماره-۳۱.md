---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>ای عرصه تبریز زیانت مرساد</p></div>
<div class="m2"><p>آسیب زمان به مردمانت مرساد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو همچو تنی و جان و دل هر دو امام</p></div>
<div class="m2"><p>دردی به دل و غمی به جانت مرساد</p></div></div>