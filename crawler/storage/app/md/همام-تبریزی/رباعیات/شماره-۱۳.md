---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>چون دیدن آن سرو روان در خواب است</p></div>
<div class="m2"><p>پس ذوق دل و راحت جان در خواب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خواب چو روی دوست می‌شاید دید</p></div>
<div class="m2"><p>بیداری بخت عاشقان در خواب است</p></div></div>