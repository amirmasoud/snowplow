---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>هر گه که گذر کنم بر آب صافی</p></div>
<div class="m2"><p>وز شوق نظر کنم در آب صافی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان گریم که آب صافی گردد</p></div>
<div class="m2"><p>از خون دو چشم من شراب صافی</p></div></div>