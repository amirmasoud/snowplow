---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>در بزم تو هر که ترک هستی نکند</p></div>
<div class="m2"><p>از باده لب‌های تو مستی نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مذهب عاشقی مسلمان نبود</p></div>
<div class="m2"><p>با روی تو هر که بت‌پرستی نکند</p></div></div>