---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>ای باد مراغه حال خویشان خون است</p></div>
<div class="m2"><p>وان یار مرا زلف پریشان چون است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون گشت دلم ز درد نادیدنشان</p></div>
<div class="m2"><p>گویی دل نازنین ایشان چون است</p></div></div>