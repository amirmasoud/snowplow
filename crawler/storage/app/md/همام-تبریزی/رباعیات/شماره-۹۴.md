---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>ای عاشق رخسار تو چون ما خیلی</p></div>
<div class="m2"><p>خود نیست به عاشقان دلت را میلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس نیست ز عاشقان به زندان لبت</p></div>
<div class="m2"><p>گر زان که لبت چنین کند واویلی</p></div></div>