---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>ای صبح که آهنگ به خونم داری</p></div>
<div class="m2"><p>از باد دل مرا چه می‌آزاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داری نفسی سردتر از یخ امشب</p></div>
<div class="m2"><p>تو زاده خورشید نه‌ای پنداری</p></div></div>