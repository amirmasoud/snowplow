---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>گه رهزن و گاه رهنمون می‌آیی</p></div>
<div class="m2"><p>گاهی ز درون گه ز برون می‌آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر لحظه به کسوه‌ای برون می-آیی</p></div>
<div class="m2"><p>آگه نشود کسی که چون می‌آیی</p></div></div>