---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>گفتار خوش و لب چو قندم باید</p></div>
<div class="m2"><p>گیسوی دراز چون کمندم باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند که یار سرو بالا داری</p></div>
<div class="m2"><p>عالی نظرم قد بلندم باید</p></div></div>