---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>جان منی ای نگار و سلطان منی</p></div>
<div class="m2"><p>سلطان منی ولی به فرمان منی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم سرو و روان هم مه تابان منی</p></div>
<div class="m2"><p>کوتاه کنم حدیث جانان منی</p></div></div>