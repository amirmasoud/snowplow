---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>یک نکته معنی ز همه دنیی به</p></div>
<div class="m2"><p>دنیی چه بود ز جنت الماوی به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهی مطلب منصب درویشی جوی</p></div>
<div class="m2"><p>کز هر دو جهان بندگی مولی به</p></div></div>