---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>جهدی بنما تا بشناسی حق را</p></div>
<div class="m2"><p>کانجا نخرند غلغل و بقبق را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از علم الهی که براق روح است</p></div>
<div class="m2"><p>جز استر زینی نرسد احمق را</p></div></div>