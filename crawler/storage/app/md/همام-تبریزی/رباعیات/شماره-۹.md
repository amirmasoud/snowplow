---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>ای چشم تو را چو من جهانی شده مست</p></div>
<div class="m2"><p>در پای تو افتاده چو گیسوی تو پست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعل لب تو ببرد آب یاقوت</p></div>
<div class="m2"><p>دندان خوشت قیمت گوهر بشکست</p></div></div>