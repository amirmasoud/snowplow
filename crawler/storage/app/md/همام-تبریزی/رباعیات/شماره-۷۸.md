---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>از مه قدحی نهاده بر کف گردون</p></div>
<div class="m2"><p>یاران گه خواب نیست خیزید کنون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما نیز به کام خود قدح برگیریم</p></div>
<div class="m2"><p>نتوان بودن ز دور گردون بیرون</p></div></div>