---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>ای طبع تو را جان لطیفان بنده</p></div>
<div class="m2"><p>ساز تو مزاج طبع را سازنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خدمت تو همه چو چنگ استاده</p></div>
<div class="m2"><p>وز غایت شرم سر به پیش افکنده</p></div></div>