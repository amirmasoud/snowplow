---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>گویند فلان سلطنتی می‌راند</p></div>
<div class="m2"><p>بهمان بد و نیک ملک نیکو داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیهوده به ریش خویشتن می‌خندند</p></div>
<div class="m2"><p>کاین کار کسی دگر همی‌گرداند</p></div></div>