---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>هر گه که قدح پر از می ناب شود</p></div>
<div class="m2"><p>از عکس می ناب چو عناب شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبش لب یار می‌برد نیست عجب</p></div>
<div class="m2"><p>با لعل گر آبگینه بی آب شود</p></div></div>