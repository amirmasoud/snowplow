---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>نی حال دلم یکان یکان می‌گوید</p></div>
<div class="m2"><p>وان راز که داشتم نهان می‌گوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رازی که به صد زبان بیان نتوان کرد</p></div>
<div class="m2"><p>از نی بشنو که بی زبان می‌گوید</p></div></div>