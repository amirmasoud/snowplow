---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>این سنگ که از آب روان می‌گردد</p></div>
<div class="m2"><p>پیوسته بسان آسمان می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نالان همه شب بسان بیماران است</p></div>
<div class="m2"><p>سرگردان‌تر ز عاشقان می‌گردد</p></div></div>