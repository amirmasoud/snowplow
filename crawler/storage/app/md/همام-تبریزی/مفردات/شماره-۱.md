---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>درخت بخل ترا کس ز بیخ برنکند</p></div>
<div class="m2"><p>مگر که صرصر حمق تو بشکند آن را</p></div></div>