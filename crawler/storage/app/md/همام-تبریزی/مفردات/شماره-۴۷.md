---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>دل گفت که زحمت تن آنجا چه بری</p></div>
<div class="m2"><p>این کار همان به که به جان فرمایی</p></div></div>