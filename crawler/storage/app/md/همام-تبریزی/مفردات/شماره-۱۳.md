---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>به آن هم مفتخر هم مبتهج شد</p></div>
<div class="m2"><p>دلش با انس و راحت ممتزج شد</p></div></div>