---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>آن همی‌خواندم همی‌بوسیدمش</p></div>
<div class="m2"><p>در سواد دیده می‌مالیدمش</p></div></div>