---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>عمری که از او جهانی ارزد</p></div>
<div class="m2"><p>در صحبت خلق رایگانی بگذشت</p></div></div>