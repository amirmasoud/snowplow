---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>می‌نویسم پیش جانان نامه‌ای</p></div>
<div class="m2"><p>کاشکی من نامه خود بودمی</p></div></div>