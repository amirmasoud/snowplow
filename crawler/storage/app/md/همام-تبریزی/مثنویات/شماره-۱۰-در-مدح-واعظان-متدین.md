---
title: >-
    شمارهٔ ۱۰ - در مدح واعظان متدین
---
# شمارهٔ ۱۰ - در مدح واعظان متدین

<div class="b" id="bn1"><div class="m1"><p>واعظانی که اهل تحقیقند</p></div>
<div class="m2"><p>به مواعظ کلید توفیقند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یافتند از خدای عز و جل</p></div>
<div class="m2"><p>عقل و ایمان وذوق وعلم وعمل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز و شب کار بندگی دارند</p></div>
<div class="m2"><p>وز بیان آب زندگی بارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نفس چون مسیح جان بخشند</p></div>
<div class="m2"><p>مایه علم از زبان بخشند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه گویند آنچه باید گفت</p></div>
<div class="m2"><p>روحشان با عروس معنی جفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مایه دار حدیث و قرآنند</p></div>
<div class="m2"><p>علم دانستنی نکو دانند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون شروعی کنند در تفسیر</p></div>
<div class="m2"><p>عقل حیران شود از ان تقریر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوش حدیثند و دلپذیر همه</p></div>
<div class="m2"><p>هم فقیهند و هم فقیر همه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پادشاهان گدای ایشانند</p></div>
<div class="m2"><p>سروران خاک پای ایشانند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در ره دین به جان همی کوشند</p></div>
<div class="m2"><p>دین به دنیا و حرص نفروشند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آب روی از برای نان نبرند</p></div>
<div class="m2"><p>به سخن نان ناکسان نخورند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زین تختند و منبر و محفل</p></div>
<div class="m2"><p>فاضل و کاملند و صاحب دل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سخن این مذکران بشنو</p></div>
<div class="m2"><p>همه تن گوش باش و آن بشنو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا رسی در سعادت عقبی</p></div>
<div class="m2"><p>بار یابی به جنت المأوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کرده ای باد در بروت که چه</p></div>
<div class="m2"><p>کبر و مستی زمن یموت که چه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای تو اندر سرای پیچا پیچ</p></div>
<div class="m2"><p>هیچ تن هیچ تن هزاران هیچ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غیر ازین تنه لطیفه یی دگر است</p></div>
<div class="m2"><p>که غرض ز آفرینش بشر است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جسم چون زان لطیفه شد خالی</p></div>
<div class="m2"><p>زیر خاکش نهان کنی حالی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زان همه خسروان روی زمین</p></div>
<div class="m2"><p>زان همه موبدان با تمکین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زان همه صف دران شیراوژن</p></div>
<div class="m2"><p>که شکستند شیر را گردن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زان همه عالمان روشن دل</p></div>
<div class="m2"><p>هریکی همچو بحر بی ساحل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زان حکیمان که فکر ایشان راه</p></div>
<div class="m2"><p>برد بالای هفتمین خرگاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زان همه خواجگان با نعمت</p></div>
<div class="m2"><p>زان همه محسنان با خدمت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه شاعران پاک سخن</p></div>
<div class="m2"><p>که سخنشان زمان نکرد کهن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زان همه مطربان خوب آواز</p></div>
<div class="m2"><p>زان همه عاشقان شاهد باز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زان همه شاهدان سیمین بر</p></div>
<div class="m2"><p>از مه و آفتاب نیکوتر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اثری در جهان نمی بینم</p></div>
<div class="m2"><p>هست نامی نشان نمی بینم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همه را عاقبت زوالی بود</p></div>
<div class="m2"><p>گشت معلوم کان خیالی بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هیچ خواندن خیال را چه عجب</p></div>
<div class="m2"><p>آنچه گفتم نبود ترک ادب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>غرض من ازین سخن صور است</p></div>
<div class="m2"><p>حال معنی و کار آن دگر است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که فنا سوی آن نیابد راه</p></div>
<div class="m2"><p>حبذا جان مردم آگاه</p></div></div>