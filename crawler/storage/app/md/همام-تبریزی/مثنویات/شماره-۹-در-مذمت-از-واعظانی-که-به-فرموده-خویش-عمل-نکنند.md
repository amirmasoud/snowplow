---
title: >-
    شمارهٔ ۹ - در مذمت از واعظانی که به فرموده خویش عمل نکنند
---
# شمارهٔ ۹ - در مذمت از واعظانی که به فرموده خویش عمل نکنند

<div class="b" id="bn1"><div class="m1"><p>ای سخن پرور بلند آواز</p></div>
<div class="m2"><p>که زبان تیز کرده ای و دراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست حرص و هوا دراز مکن</p></div>
<div class="m2"><p>دهن ماره آز باز مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا حدیث تو دل پذیر آید</p></div>
<div class="m2"><p>در دل خلق جای گیر آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>واعظانی که منبر دیوند</p></div>
<div class="m2"><p>مستفیدان دفتر دیوند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیو با دیو مردم است قرین</p></div>
<div class="m2"><p>این سخن گوید آن کند تحسین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمله گر مند در محبت زر</p></div>
<div class="m2"><p>کیسه ها می برند بر منبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>احمقان کاعتقاد می ورزند</p></div>
<div class="m2"><p>با چنین مردکان همان ارزند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون زنان شوخ و صورت آرایند</p></div>
<div class="m2"><p>گرمیی در حدیث بنمایند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صورت آراستن ز نامردیست</p></div>
<div class="m2"><p>گرمی کبر وشهوت از سردیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون فقاعاز حدیث بر جوشند</p></div>
<div class="m2"><p>نه ز آتش ز برف در جوشند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عمل خوب وانگهی تذکیر</p></div>
<div class="m2"><p>حال باید موافق تقریر نیستی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>واعظت چون دهد ز فقر خبر</p></div>
<div class="m2"><p>کوره جامه اش بنگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر گدایی کند عمامه بزرگ</p></div>
<div class="m2"><p>از دم او پناه جوید گرگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از آستین فراخ جان به جهان</p></div>
<div class="m2"><p>که گشاد اژدهای حرص دهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این همه ریش وجبه ودستار</p></div>
<div class="m2"><p>دام حرص است و مایه پندار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>می شناسد حریف خویش ابلیس</p></div>
<div class="m2"><p>می کند دعوتش بدین تلبیس</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اهل دل فارغند ازین تزویر</p></div>
<div class="m2"><p>بوی خوش می دهد خبر زعبیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>غرضم زین سخن نه بدگویی</p></div>
<div class="m2"><p>شیوه عاقلان نه بدخویی ست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ست هست مقصود من ازین گفتار</p></div>
<div class="m2"><p>رونق دین احمد مختار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رونق دین او کسی جوید</p></div>
<div class="m2"><p>کاو سخن از برای حق گوید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر که از منبرش مهراد زر است</p></div>
<div class="m2"><p>از خدا و رسول بی خبر است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هست منبر سریر پیغمبر</p></div>
<div class="m2"><p>تخت او را به احترام نگر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن گدایی که سیم خواه بود</p></div>
<div class="m2"><p>کی سزاوار تخت شاه بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لایق تخت شهریار آن است</p></div>
<div class="m2"><p>که در و خوی شهریاران است</p></div></div>