---
title: >-
    شمارهٔ ۸ - در ستایش علمی که بدان مستفیذ می توان شد
---
# شمارهٔ ۸ - در ستایش علمی که بدان مستفیذ می توان شد

<div class="b" id="bn1"><div class="m1"><p>علم باید که ره نمای بود</p></div>
<div class="m2"><p>نه فضولی شره فزای بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهل در داست علم درمان است</p></div>
<div class="m2"><p>علم آب درخت ایمان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاب گردد درخت تازه و تر</p></div>
<div class="m2"><p>خلق را منتفع کند ز ثمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میوه آن درخت طوبی وش</p></div>
<div class="m2"><p>ورع وطاعت است و خلقی خوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علما شمع مجلس افروزند</p></div>
<div class="m2"><p>خلق را علم و حکمت آموزند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه در صورت مساکینند</p></div>
<div class="m2"><p>ملک اسلام را سلاطینند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست انفاس عالم عامل</p></div>
<div class="m2"><p>همچو باد بهار با حاصل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هردو مشکین و جان فزاینده</p></div>
<div class="m2"><p>از ره لطف حق نماینده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از یکی گل به نعمت آبستن</p></div>
<div class="m2"><p>وز دگر دل به حکمت آبستن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خورده هریک چوخضر آب حیات</p></div>
<div class="m2"><p>از حلاوت حدیثشان چو نبات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان که ره بر سر معانی یافت</p></div>
<div class="m2"><p>همچو خضر آب زندگانی یافت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>علم جان دگر به جان بخشید</p></div>
<div class="m2"><p>کز فنا جاودان امان بخشید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر که از عین علم شد سیراب</p></div>
<div class="m2"><p>جان او را اجل ندید به خواب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون شود منقطع نفس زنفس</p></div>
<div class="m2"><p>برهد مرغ جان ز بند قفس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر ز علم و عمل نیابد پر</p></div>
<div class="m2"><p>باشد او را ز خاک تیره مقر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اور دو بالش بود به علم و عمل</p></div>
<div class="m2"><p>وقت پرواز بگذرد ززحل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حضرتش ارجعی همی گوید</p></div>
<div class="m2"><p>خنک آن جان که دوست می جوید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر که یزدان دو گام علم و عمل</p></div>
<div class="m2"><p>داد وی را به خطوتین و صل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از خداوند علم نافع خواه</p></div>
<div class="m2"><p>که دل غافلت کند آگاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نشود حاصل آن علوم مگر</p></div>
<div class="m2"><p>از کلام خدا و پیغمبر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بعد از آن از حدیث یارانش</p></div>
<div class="m2"><p>همنشینان و دوستارانش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سخن اولیای دین نبی</p></div>
<div class="m2"><p>نایبان محمد عربی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ره نمایی کند دل و جان را</p></div>
<div class="m2"><p>تازه دارد درخت ایمان را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>علم از وحی و کسب مقتبس است</p></div>
<div class="m2"><p>هذیان مجادلان هوس است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سخن خوب و لهجه شیرین</p></div>
<div class="m2"><p>گر نداری تو با کسی منشین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سخنی گو که شرع فرماید</p></div>
<div class="m2"><p>تا از آن مستمع بیاساید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هم به اندازه گوی آن را نیز</p></div>
<div class="m2"><p>که ز کم گفتن است مرد عزیز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سخن خوب چون شود بسیار</p></div>
<div class="m2"><p>خوار گردد نیاورند به کار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تشنگان را مده تو چندان آب</p></div>
<div class="m2"><p>کاب را در نظر نماند آب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خیر گویان چو در حدیث آیند</p></div>
<div class="m2"><p>مستمع را حیات افزایند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نفس نیک نفس خوش گفتار</p></div>
<div class="m2"><p>روح بخش است چون نسیم بهار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بوی جان زان دهن همی آید</p></div>
<div class="m2"><p>که زبان جز به خیر نگشاید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از دهانی که شد روان هذیان</p></div>
<div class="m2"><p>مبرز دیو خوانمش نه دهان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن سخن نیست گوز شیطان است</p></div>
<div class="m2"><p>زحمتش بر دماغ انسان است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سخن بد چو گند مردار است</p></div>
<div class="m2"><p>جای بدگوی بر سر دار است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون زبان حکیم گویا شد</p></div>
<div class="m2"><p>مظهر سر عقل دانا شد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آن زمانی که عقل را قلم است</p></div>
<div class="m2"><p>در بیانش فواید و حکم است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>می نویسد علوم را به دهان</p></div>
<div class="m2"><p>می کند تازه آدمی را جان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از ره سمع سوی دل ز اسرار</p></div>
<div class="m2"><p>می رساند فواید بسیار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سیرت نطق بین که زو انسان</p></div>
<div class="m2"><p>یافته ست امتیاز از حیوان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>منبع آب زندگی ست زبان</p></div>
<div class="m2"><p>چون از وعلم و حکمت است روان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از سخن تا سخن بسی فرق است</p></div>
<div class="m2"><p>سخنی وحی و دیگری زرق است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سخنی ره نمای مرد و زن است</p></div>
<div class="m2"><p>سخنی دام دیو راه زن است</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سخنی هست چون نسیم بهار</p></div>
<div class="m2"><p>که سحرگاه آید از گلزار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>راحت روح و عطر بخش مشام</p></div>
<div class="m2"><p>عاشقان را از و نصیب تمام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سخنی چون سموم آتش بار</p></div>
<div class="m2"><p>که کند نفس آدمی افکار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هر که دارد به بند عقل زبان</p></div>
<div class="m2"><p>باشد اندر پناه امن و امان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به سخنهای سخت دل مشکن</p></div>
<div class="m2"><p>سنگ خارا بر آبگینه مزن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>با لطیفان سخن مگوی درشت</p></div>
<div class="m2"><p>صورت خوب نیست لایق زشت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ای دماغت مسخر سودا</p></div>
<div class="m2"><p>به سخن مایلی چنان که تو را</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به زبان کار بر نمی آید</p></div>
<div class="m2"><p>قلمی دیگرت همی باید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تا زبان تو را دهد یاری</p></div>
<div class="m2"><p>حبابی در خزینه نگذاری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هم زبان هم قلم نگه دارند</p></div>
<div class="m2"><p>هر دو در جای خود به کار آرند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کافریننده در جهان صور</p></div>
<div class="m2"><p>دو قلم ساخت از وجود بشر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>در معانی یکی به کار آید</p></div>
<div class="m2"><p>وان دگر خود صور نگار آید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>قلمی بر صحایف اذهان</p></div>
<div class="m2"><p>می نویسد فواید دو جهان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>قلمی دیگر از درون بطون</p></div>
<div class="m2"><p>می نماید نقوش گوناگون</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کاتب هردو عقل و دین باید</p></div>
<div class="m2"><p>تا کتابت کرامت افزاید</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به زبان کار بر نمی آید</p></div>
<div class="m2"><p>عضوها هم به کار می باید</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هست هر عضوی آلت کاری</p></div>
<div class="m2"><p>روح را گشته هریکی یاری</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تن چوشهری وحاکمش جان است</p></div>
<div class="m2"><p>مدد جان ز عقل و ایمان است</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>جان زبان را می کند گویا</p></div>
<div class="m2"><p>تا نماید به خلق راه خدا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ره چو بنمود راه باید رفت</p></div>
<div class="m2"><p>بنده را پیش شاه باید رفت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>تا به کی ساکن جهان بودن</p></div>
<div class="m2"><p>بی خبر از جهان جان بودن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>سفری کن ز گلخن دنیی</p></div>
<div class="m2"><p>گذری کن به گلشن معنی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>عشق با شاهدان علوی باز</p></div>
<div class="m2"><p>شاهبازی به آشیان رو باز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>حیف باشد زمان تلف کردن</p></div>
<div class="m2"><p>چون بهایم به خفتن و خوردن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>عمر ضایع مکن به گل کاری</p></div>
<div class="m2"><p>کاردانی که چیست دلداری</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تا تو از کار گل بپردازی</p></div>
<div class="m2"><p>کرده باشد زمانه صد بازی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>جان بپرور به عقل و دانش و دین</p></div>
<div class="m2"><p>تا رسی از زمین به علیین</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تا تو در بند چار ارکانی</p></div>
<div class="m2"><p>حال روحانیان کجا دانی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>از خود این قید را چو برداری</p></div>
<div class="m2"><p>با تو جویند قدسیان یاری</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>تو چو در خانه یی ودر نگری</p></div>
<div class="m2"><p>سقف بینی ز چرخ بیخبری</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>قدم از خانه چون نهی بر بام</p></div>
<div class="m2"><p>ماه و خورشید بینی و بهرام</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>گفتن از بهر کار در کار است</p></div>
<div class="m2"><p>ورنه اینجا چه جای گفتار است</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بار دیوان مباش چون او باش</p></div>
<div class="m2"><p>طالب صحبت سلیمان باش</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سر یزدان مگوی با اغیار</p></div>
<div class="m2"><p>جوهرینه بگنج باز گذار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>گوهری گر تو را به دست آید</p></div>
<div class="m2"><p>به شهان ده که گنج را شاید</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>خرج کن نقدهای بازاری</p></div>
<div class="m2"><p>تا متاعی به خانه باز آری</p></div></div>