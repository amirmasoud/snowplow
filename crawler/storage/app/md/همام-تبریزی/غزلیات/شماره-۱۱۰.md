---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>از آن شکل و شمایل چشم بد دور</p></div>
<div class="m2"><p>که چشم عاشقان را می‌دهد نور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو را از آرزوی صورت خویش</p></div>
<div class="m2"><p>گهی آب است و گه آیینه منظور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرابی در دو لب داری که چشمت</p></div>
<div class="m2"><p>ز بویش روز و شب مست است و مخمور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دور چشم مست دل فریبت</p></div>
<div class="m2"><p>نماند زاهد صد ساله مستور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عقبی گر چنین باشند خوبان</p></div>
<div class="m2"><p>نیاساید بهشتی از لب حور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ملامت چون کنم دیوانه‌ات را</p></div>
<div class="m2"><p>که گر عاقل بماند نیست معذور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی‌بیند همامت بی‌رقیبان</p></div>
<div class="m2"><p>عسل خالی نمی‌باشد ز زنبور</p></div></div>