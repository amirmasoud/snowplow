---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>چون قامت تو سروی در بوستان نروید</p></div>
<div class="m2"><p>چون عارض تو یک گل در گلستان نروید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر باد بوی زلفت گرد چمن برآرد</p></div>
<div class="m2"><p>یک برگ گل ز شاخی بی بوی جان نروید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا وصف چشم مستت گویند پیش نرگس</p></div>
<div class="m2"><p>از خاک تیره سوسن بی صد زبان نروید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل گر دهان گشاید بی باد رویت او را</p></div>
<div class="m2"><p>رخساره لعل نبود گر در دهان نروید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر هر زمین که افتد عکسی ز چهره تو</p></div>
<div class="m2"><p>جز لاله برنیاید جز ارغوان نروید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خاک گر بمالم رخسار زرد خود را</p></div>
<div class="m2"><p>زان خاک تا قیامت جز زعفران نروید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز اشکم گیه بروید از اقامت تو یک شب</p></div>
<div class="m2"><p>بر جویبار چشمم سرو روان نروید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از جویبار وصلت یک گل نچیده‌ام من</p></div>
<div class="m2"><p>گویی ندید چشمم یا در جهان نروید</p></div></div>