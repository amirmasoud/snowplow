---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>زینهار ای دل گرت با عشق پیوندی بود</p></div>
<div class="m2"><p>غیرتت باید که بر پای هوا بندی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن روزافزون طلب، جاوید با وی عشق باز</p></div>
<div class="m2"><p>حسن خوبان مجازی تازه یک چندی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اهل دل را گر بود میلی به صورت‌های خوب</p></div>
<div class="m2"><p>عشق نتوان گفتن آن را لیک مانندی بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منزل حسن است چشم و زلف و خال دلبران</p></div>
<div class="m2"><p>عشق را با منزل معشوق پیوندی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان ما در حسن فانی شد کجا یابند باز</p></div>
<div class="m2"><p>در میان چشمهٔ حیوان اگر قندی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در میان عشق و جان چون صحبتی آمد پدید</p></div>
<div class="m2"><p>شرح نتوان داد کایشان را چه فرزندی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان شیرینم نباید جز برای مهر دوست</p></div>
<div class="m2"><p>بر همام او را به مهر خویش سوگندی بود</p></div></div>