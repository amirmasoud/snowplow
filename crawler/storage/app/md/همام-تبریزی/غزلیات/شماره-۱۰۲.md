---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>نفس کافر کیش را عشق تو در ایمان کشید</p></div>
<div class="m2"><p>دیو را حکم سلیمان باز در فرمان کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در میان ظلمت آب زندگانی جست خضر</p></div>
<div class="m2"><p>نور توفیقش به سوی چشمهٔ حیوان کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرزوی آب شیرین بافت در دریا صدف</p></div>
<div class="m2"><p>ابر نیسانی به دوش از بهر او باران کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روح قدسی کشت عیسی را معاین تاکه او</p></div>
<div class="m2"><p>رخت خود زین خاکدان بر گنبد گردان کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پادشاهی داد یوسف را سعادت بعد از آنک</p></div>
<div class="m2"><p>هم اسیر چاه شد هم زحمت زندان کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان تن آسوده را بار ریاضت بر نهاد</p></div>
<div class="m2"><p>دید دل آسایشی چون جسم بار جان کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان مشتاقان کشد از غمزهٔ جادوی تو</p></div>
<div class="m2"><p>آن جفا کز دست امت عیسی عمران کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا خرامان دید بالای تو را چشم همام</p></div>
<div class="m2"><p>کافرم گر خاطرم دیگر به سروستان کشید</p></div></div>