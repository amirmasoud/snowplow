---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>شب دراز که مانند زلف یار من است</p></div>
<div class="m2"><p>چو زلف یار به دست است، کار کار من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز روزگار همین یک دم است حاصل من</p></div>
<div class="m2"><p>که کارساز دلم یارِ سازگار من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخواهم آخرِ این شب ولی چه شاید کرد</p></div>
<div class="m2"><p>که کارها همه بیرون ز اختیار من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو صبح پرده‌دری می‌کند شکایت‌ها</p></div>
<div class="m2"><p>همی‌کنم بر آن کس که غمگسار من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میان فصل زمستان تو چون بهار منی</p></div>
<div class="m2"><p>میان خانه گلستان و لاله‌زار من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هیچ رنگ ز دستش نمی‌توانم داد</p></div>
<div class="m2"><p>ضرورت است که نقش خوشش به کار من است</p></div></div>