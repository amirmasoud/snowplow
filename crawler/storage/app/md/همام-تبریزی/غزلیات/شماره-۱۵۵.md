---
title: >-
    شمارهٔ ۱۵۵
---
# شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>تا سرم خالی نگردد از خیال ما و من</p></div>
<div class="m2"><p>خویشتن باشم حجاب روی یار خویشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن که زحمت می‌دهد جان را نخواهم صحبتش</p></div>
<div class="m2"><p>حیف باشد در میان جان و جانان پیرهن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رونق حسن پری رویان نماند در جهان</p></div>
<div class="m2"><p>گر نقاب از رخ براندازد جهان‌آرای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بود خورشید را از جانب مشرق طلوع</p></div>
<div class="m2"><p>سهل باشد گر سهیلی برنیاید از یمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان مشتاق مرا سودای زلفت در سرست</p></div>
<div class="m2"><p>لا تلومونی فذاک الشوق من حب الوطن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای ز عکس حسن رویت ز آب و گل پیدا شده</p></div>
<div class="m2"><p>خوب‌رویان در مه و خورشید تابان طعنه زن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پادشاهی و منم درویش سر بر آستان</p></div>
<div class="m2"><p>جمله اجزای وجودم سایلان بی‌سخن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جرعه‌ای از جام خود در کام این ناکام ریز</p></div>
<div class="m2"><p>آب حیوان در دهان تشنه باید ریختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چکد بر مرغ بریان قطره‌ای ز آب حیات</p></div>
<div class="m2"><p>بال بگشاید در آتش برپرد از بابزن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که از عشقت جوانی بازیابد چون همام</p></div>
<div class="m2"><p>گو دگر لاف از حیات روح حیوانی مزن</p></div></div>