---
title: >-
    شمارهٔ ۱۶۰
---
# شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>چیست دولت، صحبت صاحب‌دلان دریافتن</p></div>
<div class="m2"><p>یا حضور دوستان مهربان دریافتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی جانان را که ذوق جان مشتاقان از اوست</p></div>
<div class="m2"><p>وصل بی یک انتظاری یا گمان دریافتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سایه‌های بید بر آب روان فصل بهار</p></div>
<div class="m2"><p>در میان بوستان با دوستان دریافتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرصت روز جوانی را غنیمت می‌شمار</p></div>
<div class="m2"><p>دولت جاوید باشد آن زمان دریافتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب حیوان معانی را به شب‌خیزی طلب</p></div>
<div class="m2"><p>زان که بی ظلمت میسر نیست آن دریافتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از محبت باز جان را قوت پرواز بخش</p></div>
<div class="m2"><p>تا بر او آسان نماید آشیان دریافتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه داری پیشکش کن بر در ایوان شاه</p></div>
<div class="m2"><p>حضرت سلطان نخواهی رایگان دریافتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روی جانان را زمین و آسمان آیینه‌اند</p></div>
<div class="m2"><p>کو نظر تا عکس رویش را توان دریافتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای همام از نور روی او اگر بینا شوی</p></div>
<div class="m2"><p>عالم جان را توانی زین جهان دریافتن</p></div></div>