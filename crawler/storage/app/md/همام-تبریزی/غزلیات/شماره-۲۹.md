---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>یار ما محمل‌نشین و ساربان مستعجل است</p></div>
<div class="m2"><p>چون روان گردم که زاب دیده پایم در گل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌رود من در پیَش فریاد می‌دارم ولیک</p></div>
<div class="m2"><p>همچو آواز جرس فریاد ما بی‌حاصل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رو به هر جانب که آرد قبلۀ جان‌ها شود</p></div>
<div class="m2"><p>منزلی کانجا فرود آید زمینی مقبل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیستن بی‌روی تو صورت نمی‌بندد مرا</p></div>
<div class="m2"><p>وین تصور خود مرا بیش از فراقش قاتل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صحبت خوبان بلای جان مشتاقان بود</p></div>
<div class="m2"><p>گرچه آسان است پیوستن بریدن مشکل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کیست مانندش که تا عاشق شود خرسند از او</p></div>
<div class="m2"><p>دیگران از آب و گل منظورم از جان و دل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرو زد با قامتش لاف دروغ از راستی</p></div>
<div class="m2"><p>مردم صاحب‌نظر داند که قولش باطل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر ملامت‌گر نداند حال ما عیبش مکن</p></div>
<div class="m2"><p>ما میان موج دریاییم و او برساحل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سوز آتش شمع می‌داند که با پروانه چیست</p></div>
<div class="m2"><p>همنشین شمع سوزان از حرارت غافل است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خصم می‌گوید که نشکیبد همام از نیکوان</p></div>
<div class="m2"><p>هر که جان آشنا دارد به ایشان مایل است</p></div></div>