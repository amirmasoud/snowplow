---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>بر کف ماه نیکوان جام چو آفتاب بین</p></div>
<div class="m2"><p>نرگس نیم مست او گشته ز می خراب بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می ز گلاب عارضش گشته چو گل به رنگ می</p></div>
<div class="m2"><p>مجلس عاشقان او پر ز گل و گلاب بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جام ز دیده ساختم اشک چو باده اندر او</p></div>
<div class="m2"><p>یار معاشر مرا جام نگر شراب بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا مگر از عقیق لب رنگ دهد شراب را</p></div>
<div class="m2"><p>پیش لبش گشاده لب جام شراب ناب بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حلقه و پیچ و تاب آن زلف پر از شکن نگر</p></div>
<div class="m2"><p>مستی و شرم و ناز آن نرگس نیم خواب بین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من چو سؤال بوسه‌ای کردم از آن لب و دهن</p></div>
<div class="m2"><p>گفت برو زنخ مزن بهر خدا جواب بین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر و روی مهوشش بند دل همام را</p></div>
<div class="m2"><p>از سر زلف سرکشش حلقه و پیچ و تاب بین</p></div></div>