---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>ترکم زمی مغانه سرمست</p></div>
<div class="m2"><p>می‌آمد و عقل رفته از دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مخمور ز باده چشم جادو</p></div>
<div class="m2"><p>شوریده ز باد زلف چون شست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در باره سوار بود چون دید</p></div>
<div class="m2"><p>رخسار مرا ز زین فروجست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دستم به لب چو لعل بوسید</p></div>
<div class="m2"><p>و اندر قدمم چو خاک شد پست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برداشت ز خاک رخ پس آنگه</p></div>
<div class="m2"><p>بنشاند مرا و خویش ننشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک شیشه شراب داشت با خود</p></div>
<div class="m2"><p>زان باده که جرعه‌ای کند مست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پر کرد و یکی قدح به من داد</p></div>
<div class="m2"><p>واخوردم و دل ز غصه وارست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون مست شدم ز باده گفتم</p></div>
<div class="m2"><p>ای ترک کنون که توبه بشکست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درده می ارغوان و گر نیست</p></div>
<div class="m2"><p>دستار من از در گرو هست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترکم چو شنید همچو جوزا</p></div>
<div class="m2"><p>در خدمت من نطاق دربست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>میداد شراب ناب و نقلم</p></div>
<div class="m2"><p>از پسته خویش داد پیوست</p></div></div>