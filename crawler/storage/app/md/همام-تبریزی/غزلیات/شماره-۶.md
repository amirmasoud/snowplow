---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>روی ترکم بین مکن نسبت به خوبی ماه را</p></div>
<div class="m2"><p>ترک من در خیل دارد همچو مه پنجاه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامن خرگه براندازد به شب‌ها تا مگر</p></div>
<div class="m2"><p>گم‌رهی در منزل او باز یابد راه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رهنمایان فلک با شب‌روان ره گم کنند</p></div>
<div class="m2"><p>ترک من گر برنگیرد دامن خرگاه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر شبی در زلف مشکین، روی را پنهان کند</p></div>
<div class="m2"><p>رهبری را برفروزم شعله‌های آه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یوسف مصری اگر حسنی بدین سان داشتی</p></div>
<div class="m2"><p>عکس رویش پر مه و خورشید کردی چاه را</p></div></div>