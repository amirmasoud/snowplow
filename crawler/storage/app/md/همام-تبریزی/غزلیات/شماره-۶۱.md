---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>اگر بختم دهد باری که یارم همنشین باشد</p></div>
<div class="m2"><p>زهی مقبل که من باشم زهی دولت که این باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نباید این چنین ماهی برون از هیچ خرگاهی</p></div>
<div class="m2"><p>نظیرش گر همی‌خواهی مگر در حور عین باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان ظلمت مویش به زیر پرتو رویش</p></div>
<div class="m2"><p>گهی عقلم شود کافر گهی بر راه دین باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شمع آسمان حاجت نباشد بعد از این ما را</p></div>
<div class="m2"><p>چنین تابنده خورشیدی چو بر روی زمین باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بخرامد نگارینم نفیر از خلق برخیزد</p></div>
<div class="m2"><p>نپندارم که طوبی را شمایل این چنین باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب شیرین می‌گونش خرد را مست گرداند</p></div>
<div class="m2"><p>از آن می کاب حیوانش غلام کمترین باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدیثش دوست می‌دارم اگر خود هست نفرینم</p></div>
<div class="m2"><p>که دشنام از لب شیرین به جای آفرین باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همام آن دم که می‌گوید حدیث زلف و خالش را</p></div>
<div class="m2"><p>نفس‌ها کز دهان او برآید عنبرین باشد</p></div></div>