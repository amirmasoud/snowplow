---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>اگر نگار من از رخ نقاب بگشاید</p></div>
<div class="m2"><p>به حسن خویش جهان سر به سر بیاراید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمال خود به نقاب از نظر همی‌پوشد</p></div>
<div class="m2"><p>به سمع او برسانید، کاین نمی‌باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آفریدن شاهد غرض همین بوده‌ست</p></div>
<div class="m2"><p>که از مشاهده صاحب دلی بیآساید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به آستین و به دامان شکر کشند آنجا</p></div>
<div class="m2"><p>که پسته را به سخن یا به خنده بگشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لبش به خون دلم تشنه است و من خشنود</p></div>
<div class="m2"><p>از آن که خون منش در نظر همی‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ولی گر آب حیات است خون من به مثل</p></div>
<div class="m2"><p>دریغ باشد کاو لب بدان بیالاید</p></div></div>