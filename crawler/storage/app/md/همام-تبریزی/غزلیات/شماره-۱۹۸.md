---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>در آرزوی تو گشتم به هر دیار بسی</p></div>
<div class="m2"><p>مرا ز روی تو هرگز نشان نداد کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وجود خاکی ما را به کوی دوست چه کار</p></div>
<div class="m2"><p>که نیست لایق باغ بهشت خار و خسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی‌روم ز پی کاروان فقر مگر</p></div>
<div class="m2"><p>به گوش ما رسد از دور ناله جرسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به آتشی که در این شب ز دور می‌بینم</p></div>
<div class="m2"><p>کجا رسم که به موسی نمی‌رسد قبسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندید منزل سیمرغ چشم شهبازان</p></div>
<div class="m2"><p>خیال بین که تمنا همی‌کند مگسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به باد رفت سر سرکشان در این سودا</p></div>
<div class="m2"><p>هنوز در سر ما هست از این طلب هوسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر که باد نسیمی بیارد از گلزار</p></div>
<div class="m2"><p>که هست بلبل مسکین اسیر در قفسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جانم از نفس صبح می‌رسد بویت</p></div>
<div class="m2"><p>زعمر خویشتنم هست حاصل آن نفسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در اشتیاق تو خواهد همام جان دادن</p></div>
<div class="m2"><p>که عاشقانت از این درد مرده‌اند بسی</p></div></div>