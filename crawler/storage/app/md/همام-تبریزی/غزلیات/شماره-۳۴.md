---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>یاری که رخش قبله صاحب‌نظران است</p></div>
<div class="m2"><p>چشم و دل مردم به جمالش نگران است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم که ببوسم قدمش نیست مجالم</p></div>
<div class="m2"><p>هر جا که نهم دیده سر تاجوران است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیداست که از وصل تو حاصل چه توان یافت</p></div>
<div class="m2"><p>چون وصل تو را خوی جهان گذران است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای باخبرانِ تو همه بی‌خبر از خود</p></div>
<div class="m2"><p>وین بی‌خبری آرزوی باخبران است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصل تو که محبوب‌تر از عمر و جوانی‌ست</p></div>
<div class="m2"><p>حیف است که او نیز چو عمرم گذران است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چشم تورا میل به خون ریختن ماست</p></div>
<div class="m2"><p>ما نیز بر آنیم که چشم تو بر آن است</p></div></div>