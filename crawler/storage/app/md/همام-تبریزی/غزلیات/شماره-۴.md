---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>ساقی همان به کامشبی در گردش آری جام را</p></div>
<div class="m2"><p>وز عکس می روشن کنی چون صبح صادق شام را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می ده پیاپی تا شوم ز احوال عالم بی‌خبر</p></div>
<div class="m2"><p>چون نیست پیدا حاصلی این گردش ایام را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار طرب را ساز ده و اصحاب را آواز ده</p></div>
<div class="m2"><p>در حلقه خاصان مکش این عام کالانعام را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان حلقه‌های عنبرین آرام دل‌ها می‌بری</p></div>
<div class="m2"><p>آشوب جان‌ها کرده‌ای آن زلف بی‌آرام را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای آفتاب انجمن از عکس روی و جام می</p></div>
<div class="m2"><p>در جان ما زن آتشی تا پخته یابی خام را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای عاشقت هر شاهدی رند تو هر جا زاهدی</p></div>
<div class="m2"><p>در کار عشقت کرده دل یک باره ننگ و نام را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر دل که هست اندر جهان رغبت به زلفت می‌کند</p></div>
<div class="m2"><p>نخجیر دیدی کاو به جان جوینده باشد دام را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صوفی چو لفظت بشنود دیگر نگوید ماجرا</p></div>
<div class="m2"><p>حاجی چو بیند روی تو باطل کند احرام را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر گه که دشنامم دهی آسوده گردد جان من</p></div>
<div class="m2"><p>کز لهجه شیرین تو ذوقی بود دشنام را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من دست بوسی می‌کنم مرد لب و چشمت نیم</p></div>
<div class="m2"><p>نقل لب مستان مکن آن شکر و بادام را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دارد همام از روی تو خورشید در کاشانه شب</p></div>
<div class="m2"><p>بر راه صبح از زلف خود امشب بگستر دام را</p></div></div>