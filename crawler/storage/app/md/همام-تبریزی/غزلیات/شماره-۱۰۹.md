---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>آفتابی و ز مهرت همه دل‌ها محرور</p></div>
<div class="m2"><p>چشم روشن بود آن را که تو باشی منظور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قربتت نیست میسر به نظر خرسندم</p></div>
<div class="m2"><p>همه مردم نگرانند به خورشید از دور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انتظار نظرم پرده صبرم بدرید</p></div>
<div class="m2"><p>تا به کی نرگس مستت بود از ما مستور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچه می‌جست سکندر به میان ظلمات</p></div>
<div class="m2"><p>گو بیایید و ببینید در این چشمه نور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود آوازه دور قمری تا امروز</p></div>
<div class="m2"><p>دور روی تو شد اکنون به جهان در مشهور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نسبتی هست به دندان تو پروین را لیک</p></div>
<div class="m2"><p>هست دندان تو منظوم و ثریا منثور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌کند حسن و لطافت ز تو دریوزه بهار</p></div>
<div class="m2"><p>می‌کند وام حرارت ز دل ما با حور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر همام است به جان مشتری تو چه عجب</p></div>
<div class="m2"><p>مه و خورشید گواهند که هستم معذور</p></div></div>