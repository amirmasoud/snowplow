---
title: >-
    شمارهٔ ۱۹۹
---
# شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>نه چنان مست و خرابم ز دو چشم ساقی</p></div>
<div class="m2"><p>که مرا با دگری هست خیالی باقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستی از هستی من جز سر مویی نگذاشت</p></div>
<div class="m2"><p>وان قدر نیز نخواهم که بود ای ساقی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مست گشتیم و نخواهی تو که فانی گردیم</p></div>
<div class="m2"><p>زان که در آرزوی عربدۀ عشاقی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کجا پرتو حسنی‌ست ز رویت اثری‌ست</p></div>
<div class="m2"><p>آفتابی تو که منظور همه آفاقی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان من از سر زلف تو نسیمی بشنید</p></div>
<div class="m2"><p>عزم دارد که کند صحبت تن در باقی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود نمایان اگر از عشق تو بویی یابند</p></div>
<div class="m2"><p>پیش شاهان نفروشند دگر زراقی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیف باشد که بخوانند غزل‌های همام</p></div>
<div class="m2"><p>پیش خامی که ندارد خبر از مشتاقی</p></div></div>