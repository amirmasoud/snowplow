---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>حسنی که هست روی تو را بی‌نهایت است</p></div>
<div class="m2"><p>خوب است گل ولی نمک اینجا به غایت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افسانه‌های خسرو و شیرین ز حد گذشت</p></div>
<div class="m2"><p>ما و حدیث روی تو کانها حکایت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من فارغم ز مصر که در دولت لبت</p></div>
<div class="m2"><p>امروز کان قند و شکر این ولایت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افکند سایه زلف تو بر عارض خوشت</p></div>
<div class="m2"><p>ظلمت نگر که نور مهش در حمایت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من کیستم که دست به زلفت کنم دراز</p></div>
<div class="m2"><p>بویش صبا اگر به من آرد کفایت است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بهر عاشقان تو در شرع کفر و دین</p></div>
<div class="m2"><p>اوصاف روی و موی تو محکم روایت است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هشیار کی شود دل سرگشته همام</p></div>
<div class="m2"><p>چون مستیش ز نرگس مستت کفایت است</p></div></div>