---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>هر که او عاشق جمال بود</p></div>
<div class="m2"><p>شاهدش خود گواه حال بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بود پاکباز شاهد نیز</p></div>
<div class="m2"><p>پاک و روشن‌تر از زلال بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال اگر برخلاف این باشد</p></div>
<div class="m2"><p>دوستی هر دو را وبال بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم خود را به آب شرم بشوی</p></div>
<div class="m2"><p>تا که شایسته جمال بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیف باشد که دیده بی‌آب</p></div>
<div class="m2"><p>تشنه مشرب وصال بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیمه ز آتش چو سرخ شد آخر</p></div>
<div class="m2"><p>همه خاکستر و زگال بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زر صافی نهاد را ز آتش</p></div>
<div class="m2"><p>سرخ‌رویی با کمال بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وان که او مرد حال شد چو همام</p></div>
<div class="m2"><p>فارغ از بند قیل و قال بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست امیدم که بهره‌ای ز وصال</p></div>
<div class="m2"><p>یابد و فارغ از خیال بود</p></div></div>