---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>نباتت بر لب شکّر برآمد</p></div>
<div class="m2"><p>زمرد گرد یاقوتت درآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز گلبرگ تو سنبل سر برآورد</p></div>
<div class="m2"><p>زهی سنبل که از گل بر سر آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخت منشور خوبی داشت بی خط</p></div>
<div class="m2"><p>چو خطت بر مثال عنبر آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گواهی داد هر جا شاهدی بود</p></div>
<div class="m2"><p>که در حسن تو خطی دیگر آمد</p></div></div>