---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>هوس عمر عزیزم ز برای تو بود</p></div>
<div class="m2"><p>بکشم جور جهانی چو رضای تو بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ازل جان مرا عشق تو هم صحبت بود</p></div>
<div class="m2"><p>تا ابد در دل من مهر و وفای تو بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جای افسر شود آن سر که به پای تو رسد</p></div>
<div class="m2"><p>پادشاهی کند آن کس که گدای تو بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست امیدم که نمایی تو خداوندی‌ها</p></div>
<div class="m2"><p>ور نه از بنده چه آید که سزای تو بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خجلم زان که فرود آمده‌ای در دل تنگ</p></div>
<div class="m2"><p>چیست این منزل ویرانه که جای تو بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی خوب تو شد انگشت‌نمای خورشید</p></div>
<div class="m2"><p>مه نو کیست که انگشت‌نمای تو بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سال‌ها سجده صاحب‌نظران خواهد بود</p></div>
<div class="m2"><p>بر زمینی که نشان کف پای تو بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راحت روح و فتوح دل مشتاقان است</p></div>
<div class="m2"><p>هر حدیثی که در او وصف و ثنای تو بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخنی لایق سمعت نبود ور باشد</p></div>
<div class="m2"><p>هم غزل‌های همام ابن علای تو بود</p></div></div>