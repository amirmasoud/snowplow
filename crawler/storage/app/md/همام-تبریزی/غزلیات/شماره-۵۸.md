---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>دردمندان را ز بوی دوست درمان می‌رسد</p></div>
<div class="m2"><p>مژدۀ فرزند پیش پیر کنعان می‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یوسف کنعانی از زندان همی‌یابد خلاص</p></div>
<div class="m2"><p>خاتم دولت به انگشت سلیمان می‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خضر را نور الهی رهنمایی می‌کند</p></div>
<div class="m2"><p>کز میان تیرگی بر آب حیوان می‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امن و راحت در میان ملک پیدا می‌شود</p></div>
<div class="m2"><p>سایه کیخسرو فرخ به ایران می‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم روشن می‌شود چون صبح دولت می‌دمد</p></div>
<div class="m2"><p>این شب تاریک ظلمانی به پایان می‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌درفشد ابر و می‌گوید زمین مرده را</p></div>
<div class="m2"><p>تازه و سیراب خواهی شد که باران می‌رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلبلان را باد نوروزی بشارت می‌دهد</p></div>
<div class="m2"><p>کز ره یک ساله گل سوی گلستان می‌رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌رساند عاشقان را باد پیغامی ز دوست</p></div>
<div class="m2"><p>وه که زان همدم چه راحت‌ها به ایشان می‌رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو سلطان نبوت را ز انفاس اویس</p></div>
<div class="m2"><p>جان ما را راحتی از بوی جانان می‌رسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این نسیم خوش نفس و آسایش جان همام</p></div>
<div class="m2"><p>از غبار منزل او عنبرافشان می‌رسد</p></div></div>