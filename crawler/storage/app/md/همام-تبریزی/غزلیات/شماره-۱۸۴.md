---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>گفت از برای چیدن گل در چمن شدی</p></div>
<div class="m2"><p>از مات شرم باد که پیمان‌شکن شدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر نسیم گل اثر بوی ما نداشت</p></div>
<div class="m2"><p>تا در چمن به بوی که بی خویشتن شدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل را چه نسبت است به روی نکوی من</p></div>
<div class="m2"><p>تا باشدت بهانه که بر بوی من شدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غافل شدی مگر ز سر زلف و عارضم</p></div>
<div class="m2"><p>کآشفته بر بنفشه و برگ سمن شدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چشم تو خیال سهی سرو من برفت</p></div>
<div class="m2"><p>تا فتنه بر شمایل هر نارون شدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یعقوب وار بی‌دل و بی‌دیده گشته‌ای</p></div>
<div class="m2"><p>یوسف ندیده‌ای که بی پیرهن شدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جاد در تن بنفشه هم از بوی زلف ماست</p></div>
<div class="m2"><p>جان را به جا گذاشتی و سوی تن شدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتی که گل به رنگ چو روی بت من است</p></div>
<div class="m2"><p>در حسن ما بگو که چرا طعنه زن شدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گل کیست سرو چیست که باشد بنفشه نیز</p></div>
<div class="m2"><p>شرمت نبود تا به کدام انجمن شدی</p></div></div>