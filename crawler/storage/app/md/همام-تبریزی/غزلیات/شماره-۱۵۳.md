---
title: >-
    شمارهٔ ۱۵۳
---
# شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>به معنی چون شود صورت مزین</p></div>
<div class="m2"><p>چو قصری باشد از خورشید روشن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل رنگین اگر بویی ندارد</p></div>
<div class="m2"><p>نظر بر رنگ رخسارش میفکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مپز دیگ هوس جز با ملیحی</p></div>
<div class="m2"><p>نباید بی‌نمک خود دیگ پختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظر جز پیش منظورم نیاید</p></div>
<div class="m2"><p>چنین حسنی که وصفش می‌کنم من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل از اندیشه دنیی و عقبی</p></div>
<div class="m2"><p>تهی کردم چو او را گشت مسکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش است از پاک بازان جان‌فشانی</p></div>
<div class="m2"><p>ولی در پای یار پاک‌دامن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محبت از ره پرهیزگاری</p></div>
<div class="m2"><p>چنان آید که نور از راه روزن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نخواهم آرزوی جان که حیف است</p></div>
<div class="m2"><p>میان جان و جانان، دست و گردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محبت بر هوا چون گشت غالب</p></div>
<div class="m2"><p>همام از گلخن آمد سوی گلشن</p></div></div>