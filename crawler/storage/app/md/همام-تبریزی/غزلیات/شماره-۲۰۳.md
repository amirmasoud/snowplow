---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>مباد دل ز هوای تو یک زمان خالی</p></div>
<div class="m2"><p>که اعتبار ندارد تنی ز جان خالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همای عشق تو را هست آشیان دل من</p></div>
<div class="m2"><p>مباد سایه آن مرغ از آشیان خالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن جهان نبود مایه هیچ جانی را</p></div>
<div class="m2"><p>که ز اشتیاق تو باشد در این جهان خالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز غیرتی که مرا بر سگان کوی تو هست</p></div>
<div class="m2"><p>کنم همیشه زمینش ز استخوان خالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بقای روی تو بادا که عالم افروزست</p></div>
<div class="m2"><p>اگر شود ز مه و مهر آسمان خالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هیچ دل ز بیانم حلاوتی نرسد</p></div>
<div class="m2"><p>گر از حدیث تو باشد دمی زبان خالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کدام فایده باشد همام را ز زبان</p></div>
<div class="m2"><p>چو باشد از سخن یار مهربان خالی</p></div></div>