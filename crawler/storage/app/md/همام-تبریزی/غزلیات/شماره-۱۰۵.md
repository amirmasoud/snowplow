---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>اینک نسیمی می‌دهد کز دوست می‌آرد خبر</p></div>
<div class="m2"><p>برخیز کاستقبال او واجب بود کردن به سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای راحت جان مرحبا از دوست کی گشتی جدا</p></div>
<div class="m2"><p>دارد عزیمت سوی ما یا کرد از این جانب گذر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از زلف عنبربار او وز سرو خوش رفتار او</p></div>
<div class="m2"><p>وز روی چون گلنار او ریح الصباهات الخبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن چشم شوخ و شنگ او و ابروی پر نیرنگ او</p></div>
<div class="m2"><p>وان طرۀ شبرنگ او چون است ای باد سحر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای مشک‌بوی خوش‌نفس بودی مرا فریادرس</p></div>
<div class="m2"><p>تعجیل کن رو باز پس پیغام سوی دوست بر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او را بگو کای نازنین منشین زمانی برنشین</p></div>
<div class="m2"><p>فرسنگ در فرسنگ بین افتاده دل بر یک‌دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوش در دلم بنشسته‌ای با روح در پیوسته‌ای</p></div>
<div class="m2"><p>در چشم من بشکسته‌ای بازار خوبان سر به سر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای جان شکار تیر تو، دل بسته زنجیر تو</p></div>
<div class="m2"><p>در حیرت از تصویر تو صورت‌گر صاحب هنر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روح و دماغ کیستی چشم و چراغ کیستی</p></div>
<div class="m2"><p>ای گل ز باغ کیستی کآباد باد آن بوم و بر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای چون همام خوش‌سخن از عاشقان صد انجمن</p></div>
<div class="m2"><p>در منزلت فریاد زن از اشتیاق یک نظر</p></div></div>