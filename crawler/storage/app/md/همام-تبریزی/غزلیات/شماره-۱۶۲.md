---
title: >-
    شمارهٔ ۱۶۲
---
# شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>ای آرزوی چشمم رویت به خواب دیدن</p></div>
<div class="m2"><p>دوری نمی‌تواند پیوند ما بریدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم که جان شیرین هجران به لب رساند</p></div>
<div class="m2"><p>تا وقت آن که باشد ما را به هم رسیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موقوف التفاتم تا کی رسد اجازت</p></div>
<div class="m2"><p>از دوست یک اشارت از ما به سر دویدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا روح بر نیاید جهدی همی‌نمایم</p></div>
<div class="m2"><p>مشتاق را نشاید یک لحظه آرمیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمی که دیده باشد آن شکل و آن شمایل</p></div>
<div class="m2"><p>بی او ملول باشد از روی خوب دیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را به نیم جانی وصلت کجا فروشند</p></div>
<div class="m2"><p>ارزان بود به صد جان گر می‌توان خریدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیرت همی‌نماید بر گوش دیده من</p></div>
<div class="m2"><p>کز دور می‌تواند پیغام تو شنیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حیران شده است عقلم در صنع پادشاهی</p></div>
<div class="m2"><p>کز خاک می‌تواند خورشید آفریدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باشد همام شب‌ها در آرزوی خوابی</p></div>
<div class="m2"><p>وقتی مگر خیالت در بر توان کشیدن</p></div></div>