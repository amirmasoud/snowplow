---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>در آن نفس که بمیرم در آرزوی تو باشم</p></div>
<div class="m2"><p>بدان امید دهم جان که خاک کوی تو باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خوابگاه عدم گر هزار سال بخسبم</p></div>
<div class="m2"><p>ز خواب عاقبت آگه به بوی موی تو باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به وقت صبح قیامت که سر ز خاک برآرم</p></div>
<div class="m2"><p>به آرزوی تو خیزم به جست و جوی تو باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به مجمعی که در آیند شاهدان دو عالم</p></div>
<div class="m2"><p>نظر به سوی تو دارم غلام روی تو باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حدیث روضه نگویم گل بهشت نبویم</p></div>
<div class="m2"><p>جمال حور نجویم دوان به سوی تو باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می بهشت ننوشم ز جام و ساغر رضوان</p></div>
<div class="m2"><p>مرا به باده چه حاجت چو مست بوی تو باشم</p></div></div>