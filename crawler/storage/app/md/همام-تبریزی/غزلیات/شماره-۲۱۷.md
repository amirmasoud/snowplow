---
title: >-
    شمارهٔ ۲۱۷
---
# شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>ای منزل مبارک می‌بخشیم صفایی</p></div>
<div class="m2"><p>داری هوای مشکین از بوی آشنایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک رهت ببوسم بر روی و دیده مالم</p></div>
<div class="m2"><p>کانجا رسیده باشد روزی نشان پایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سنگریزه‌هایت چون می‌کنم سلامی</p></div>
<div class="m2"><p>آید به گوش جانم بی‌صوت مرحبایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در منزلی که جانان آنجا گذشته باشد</p></div>
<div class="m2"><p>با ذره‌های خاکش داریم ماجرایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردی ز منزل او گر بر بصر نشیند</p></div>
<div class="m2"><p>سازنده‌تر نباشد زان خاک توتیایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مقبل کسی که دارد در جان وفای یاران</p></div>
<div class="m2"><p>جانم فدای یاری کاو را بود وفایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از یاد دوست یابم آرام در فراقش</p></div>
<div class="m2"><p>جز ذکر او ندانم دل را دگر دوایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان همام دارد حاصل ز عشق جانان</p></div>
<div class="m2"><p>آن دولتی که نبود در سایه همایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن را که چتر عشقش افکند سایه بر سر</p></div>
<div class="m2"><p>سلطان ملک عالم پیشش بود گدایی</p></div></div>