---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>حسنت چو اشتیاق دلم بی‌نهایت است</p></div>
<div class="m2"><p>وز عاشقان فراغت یارم به غایت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با چشم مست و زلف پریشان نهادِ او</p></div>
<div class="m2"><p>همرنگ می‌شویم چه جای کنایت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عارف ز حال گوید و عالم ز دیگران</p></div>
<div class="m2"><p>ما و حدیث عشق تو کانها حکایت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم تو راست کرد به دل تیر غمزه را</p></div>
<div class="m2"><p>شادم که التفات دلیل عنایت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل از میان ظلمت مویت نگاه کرد</p></div>
<div class="m2"><p>روی تو دید گفت امید هدایت است</p></div></div>