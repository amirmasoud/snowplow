---
title: >-
    شمارهٔ ۱۶۳
---
# شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>در باغ چو بالایت سروی نتوان دیدن</p></div>
<div class="m2"><p>صد سرو فدا بادا هنگام خرامیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای نور الهی را از روی شما عکسی</p></div>
<div class="m2"><p>ما آینه صانع خواهیم پرستیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح از هوس رویت رفتم به گلستان‌ها</p></div>
<div class="m2"><p>باشد که کنم خود را مشغول به گل چیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل‌ها چو مرا دیدند فریاد برآوردند</p></div>
<div class="m2"><p>کان گل که تو می‌خواهی اینجا نتوان دیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ابر همی‌گریم بر غنچهٔ خندان لب</p></div>
<div class="m2"><p>تا از شکرت دیدم شیرینی خندیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرهاد اگر دیدی آن چهره شیرین را</p></div>
<div class="m2"><p>از دست شدی دستش در سنگ تراشیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چشم منی نتوان خار مژه بر هم زد</p></div>
<div class="m2"><p>ترسم که گلت یابد زحمت ز خراشیدن</p></div></div>