---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>عاشقی چیست به جان بندهٔ جانان بودن</p></div>
<div class="m2"><p>گر لبش جان طلبد دادن و خندان بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوی زلفش نظری کردن و دیدن رویش</p></div>
<div class="m2"><p>گاه کافر شدن و گاه مسلمان بودن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست در چشمه خورشید ز رویت اثری</p></div>
<div class="m2"><p>چون توان منکر خورشید پرستان بودن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرتو روی الهی چو بر انسان افتاد</p></div>
<div class="m2"><p>رسم شد شیفتۀ صورت خوبان بودن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با چنین روی و لب انصاف غرامت باشد</p></div>
<div class="m2"><p>طالب جام جم و چشمهٔ حیوان بودن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نه روی تو بود من چه کنم باغ بهشت</p></div>
<div class="m2"><p>نتوان بهر گل و میوه به زندان بودن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا نسیم سر زلفت ز صبا بشنیدم</p></div>
<div class="m2"><p>کار ما هست چو زلف تو پریشان بودن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روی بنمای که صاحب‌نظران مشتاقند</p></div>
<div class="m2"><p>تا به کی زیر نقاب از همه پنهان بودن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ره‌نشینان سر کوی تو را همچو همام</p></div>
<div class="m2"><p>غیرت آید نفسی همدم سلطان بودن</p></div></div>