---
title: >-
    شمارهٔ ۲۱۹
---
# شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>دانی چگونه باشد از دوستان جدایی</p></div>
<div class="m2"><p>چون دیده‌ای که ماند خالی ز روشنایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سهل است عاشقان را از جان خود بریدن</p></div>
<div class="m2"><p>لیکن ز روی جانان مشکل بود جدایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دوستی نباید هرگز خلل ز دوری</p></div>
<div class="m2"><p>گر در میان جانان مهری بود خدایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر زر که خالص آید بر یک عیار باشد</p></div>
<div class="m2"><p>صد بار اگر در آتش او را بیازمایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای نور چشم بینا داری فراغت از ما</p></div>
<div class="m2"><p>ما خوش بدین تمنا دائم که زان مایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر صحبتت فقیری جوید عجب نباشد</p></div>
<div class="m2"><p>با عشق در نگنجد سلطانی و گدایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما را طمع نباشد پرسیدن و عیادت</p></div>
<div class="m2"><p>از زلف خود نسیمی بفرست اگر نیایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کاو به بوی زلفت جان را نمی‌سپارد</p></div>
<div class="m2"><p>در جان او نباشد بویی ز آشنایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای چون همام شهری افتاده در کمندت</p></div>
<div class="m2"><p>زین بند کس نیابد تا جاودان رهایی</p></div></div>