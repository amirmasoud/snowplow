---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>دلم ز عهدۀ عشقت برون نمی‌آید</p></div>
<div class="m2"><p>به جای هر سر مویی مرا دلی باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهای هر سر مویت نهاده‌ام جانی</p></div>
<div class="m2"><p>زهی معامله گر دیگری نیفزاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدد ز بوی تو یابد هوای فصل بهار</p></div>
<div class="m2"><p>که چون بهشت چمن را به گل بیاراید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهید تیغ تو جان‌ها به زندگان بخشد</p></div>
<div class="m2"><p>گدای کوی تو بر خسروان ببخشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خسته‌ای که رساند نسیم بوی خوشت</p></div>
<div class="m2"><p>اگر در آتش سوزان بود بیاساید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روان شود ز لبم چشمه‌های آب حیات</p></div>
<div class="m2"><p>چو نام دوست مرا بر سر زبان آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار بار بشستم دهن به مشک و گلاب</p></div>
<div class="m2"><p>هنوز نام تو بردن مرا نمی‌شاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظر به روی تو کردن مسلم است آن را</p></div>
<div class="m2"><p>که دیده را به جمالی دگر نیالاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهی خجسته صباحی که وقت بیداری</p></div>
<div class="m2"><p>همام روی تو بیند چو دیده بگشاید</p></div></div>