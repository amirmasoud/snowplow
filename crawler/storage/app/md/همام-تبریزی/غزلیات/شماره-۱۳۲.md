---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>من از دنیا و مافیها دل اندر نیکوان بستم</p></div>
<div class="m2"><p>عجب دارم که بشکیبم ز روی خوب تا هستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا باید که در دستم بود زلف پری رویان</p></div>
<div class="m2"><p>چه باشد گر دهد یا نه مریدی بوسه بر دستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال مهر ورزیدن نبود اندر سرم لیکن</p></div>
<div class="m2"><p>چوزلف پرشکن دیدم به رغبت توبه بشکستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب شیرین می‌گونت سخن می‌گفت بشنیدم</p></div>
<div class="m2"><p>ز انفاس خوشت بویی هنوز از ذوق آن مستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز چشم اشکریز من روان شد چشمه‌ها حالی</p></div>
<div class="m2"><p>به زیر سایه سروی چو بی روی تو بنشستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا با هر سر مویت چو پیدا گشت پیوندی</p></div>
<div class="m2"><p>دگر با هیچ دلبندی سر مویی نپیوستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز شمع عارضت عکسی چو در چشم همام آمد</p></div>
<div class="m2"><p>ز شمع آسمان دیدن دو چشم خویش در بستم</p></div></div>