---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>ای پیش نقش روی تو صاحب‌دلان بی خویشتن</p></div>
<div class="m2"><p>وز چشم مستت فتنه‌ها افتاده در هر انجمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا خرقه و پشمینه را بازار دعوی بشکنی</p></div>
<div class="m2"><p>طرف کله را برشکن بنمای زلف پرشکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوی گریبان کیست کاو سر بر سر دوشت نهد</p></div>
<div class="m2"><p>بیم است کز غیرت کنم صد پاره بر تن پیرهن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بار پیراهن کشی؟ کز گل بسی نازک‌تری</p></div>
<div class="m2"><p>پیراهنی باید تو را از لاله و برگ سمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل لاف خوبی می‌زند سرو سهی سر می‌کشد</p></div>
<div class="m2"><p>سلطان حسنی هر دو را بنشان به جای خویشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آرزوی قامتت صد عاشق سرگشته را</p></div>
<div class="m2"><p>افتاده بینی بی‌خبر در پای سرو و نارون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر شب فغان عاشقان آید ز کویت همچنان</p></div>
<div class="m2"><p>کآید به وقت صبحدم فریاد مرغان از چمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا مشک جان پرور شود اجزای آهو سر به سر</p></div>
<div class="m2"><p>بفرست بر دست صبا بویی به صحرای ختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای در لبت جان‌ها نهان با ما سخن گو یک زمان</p></div>
<div class="m2"><p>تا در تنم گردد روان صد جان شیرین زان دهن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شعرهمام خوش نفس دل را بیفزاید هوس</p></div>
<div class="m2"><p>شیرین بود الفاظ او چون از لبت گوید سخن</p></div></div>