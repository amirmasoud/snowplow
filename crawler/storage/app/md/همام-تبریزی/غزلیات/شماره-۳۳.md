---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>وداع چون تو نگاری نه کار آسان است</p></div>
<div class="m2"><p>هلاک عاشق مسکین فراق جانان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگر مفارقت جان ز تن چگونه بود</p></div>
<div class="m2"><p>به جان دوست که هجران هزار چندان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز وصل خود نفسی پیش از آن که دور شوم</p></div>
<div class="m2"><p>اگر به جان بفروشی هنوز ارزان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجال دیدن رویت نماند چشمم را</p></div>
<div class="m2"><p>که شکل مردمکش زیر اشک پنهان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگو که تا نشود کاروان روان امروز</p></div>
<div class="m2"><p>که ز آب دیده اصحاب روز باران است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنوز سرو روانم ز چشم ناشده دور</p></div>
<div class="m2"><p>دل از تصور دوری چو بید لرزان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدان امید که بوسند نعل یکرانت</p></div>
<div class="m2"><p>نهاده بر سر راه تو روی یاران است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز هر طرف که نظر می‌کنم برابر تو</p></div>
<div class="m2"><p>هزار سینه نالان و چشم گریان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نظر به جانب زلف تو می‌کنم او نیز</p></div>
<div class="m2"><p>برای خاطر این خستگان پریشان است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز هم بریدن یاران به تیغ ناکامی</p></div>
<div class="m2"><p>چو هست عادت گردون مرا چه تاوان است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو می‌روی و همام از پی تو می‌نگرد</p></div>
<div class="m2"><p>ز دل بریده امید و حدیث در جان است</p></div></div>