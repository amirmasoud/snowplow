---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>بجز از صورت آراسته چیزی دگر است</p></div>
<div class="m2"><p>که آفت اهل دل و فتنه صاحب‌نظر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قد افراشته و روی نکو خواهد دل</p></div>
<div class="m2"><p>در تو چیزی است که زین هر دو دلاویزتر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قامت سرو سهی را چه توان گفت ولی</p></div>
<div class="m2"><p>قد و بالای تو را خود حرکاتی دگر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه را میل به زلف و خط و خالی باشد</p></div>
<div class="m2"><p>زان ملاحت که تو داری دل ما بی‌خبر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با نسیم سحری هست ز بویت اثری</p></div>
<div class="m2"><p>بوی گل‌های دلاویز چمن زان اثر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل که در ملک چمن مملکت خوبی داشت</p></div>
<div class="m2"><p>شد ز روی تو خجل بر سر عزم سفر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی خوب تو منجم به جماعت نبود</p></div>
<div class="m2"><p>گفت کآشوب جهان جمله ز دور قمر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل مردم همه در بند میانت بینم</p></div>
<div class="m2"><p>حیفم آید که میان تو به بند کمر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببری دل به حدیثی نکنی دلداری</p></div>
<div class="m2"><p>از تو ای شوخ چه خون‌ها که مرا در جگر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پرسشی کن که فدای لب شیرین تو باد</p></div>
<div class="m2"><p>هر چه در ناحیت مصر نبات و شکر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تشنه آب حیات لب تو بسیارند</p></div>
<div class="m2"><p>به جفایت که همام از همه‌شان تشنه‌تر است</p></div></div>