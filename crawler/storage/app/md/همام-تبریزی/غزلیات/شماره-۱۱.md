---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>چون لبت از مصر کی خیزد نبات</p></div>
<div class="m2"><p>کز نباتت می‌چکد آب حیات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوستانت ز آب حیوان بی‌نصیب</p></div>
<div class="m2"><p>تشنگان جان داده نزدیک فرات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صانع از روی تو شمعی برفروخت</p></div>
<div class="m2"><p>دفع ظلمت را میان کاینات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش نقش رویت ایمان آورند</p></div>
<div class="m2"><p>بت‌پرستان زمین سومنات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود در خوبان نظر کردن حرام</p></div>
<div class="m2"><p>حسنت آمد کرد محو سیئات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کمند زلف جان‌آویز تو</p></div>
<div class="m2"><p>جان ندارد هر که می‌جوید نجات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبر فرمایی مرا در عاشقی</p></div>
<div class="m2"><p>چون نمایم بر سر آتش ثبات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر کند چشمت به درویشان نظر</p></div>
<div class="m2"><p>مایه حسن تو را باشد زکات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زندگانی را ز سر گیرد همام</p></div>
<div class="m2"><p>گر به خاکش بگذری بعد از وفات</p></div></div>