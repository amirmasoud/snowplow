---
title: >-
    شمارهٔ ۱۹۵
---
# شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>به خوبی‌ست هر دل نوازی ایازی</p></div>
<div class="m2"><p>ولی همچو محمود کو پاک بازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخواهم که روی رقیبان ببینم</p></div>
<div class="m2"><p>گدا را ز سگ واجب است احترازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان پر ز حسن است کو مهرورزی</p></div>
<div class="m2"><p>که با ناز خوبان نماید نیازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هجران وصل دلارام ما دان</p></div>
<div class="m2"><p>به هر جا که پیداست سوزی و سازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گوش محبت شنو ذکر جانان</p></div>
<div class="m2"><p>ز آواز ناقوس و بانگ نمازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به چشم حقیقت نگه کن در اشیا</p></div>
<div class="m2"><p>که تا در دو عالم نیابی مجازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همام این سخن با کسی گو که داند</p></div>
<div class="m2"><p>به کبکی مده طعمه شاه‌بازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زهی خوش مجالی که یاران همدم</p></div>
<div class="m2"><p>از اغیار پوشیده گویند رازی</p></div></div>