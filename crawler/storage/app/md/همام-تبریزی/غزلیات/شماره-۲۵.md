---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>حسن تو را ممالک دل‌ها مسخر است</p></div>
<div class="m2"><p>مقبل کسی که وصل تو او را میسر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر منزل مبارک تو هر که بگذرد</p></div>
<div class="m2"><p>گوید که این خلاصه هر هفت کشور است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آبش چو کوثر است و چو دُر سنگ ریزه‌ها</p></div>
<div class="m2"><p>بادش نسیم عنبر و خاکش معصفر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم و دل از مشاهده‌ات بی نصیب نیست</p></div>
<div class="m2"><p>نقشت چو بر صحیفه جانم مصور است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن آفتاب را که بسوزد شعاع او</p></div>
<div class="m2"><p>چشم عقول روی چو ماه تو مظهر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کحل جواهر است غبار منازلش</p></div>
<div class="m2"><p>زان چشم عاشقان تو دایم منور است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گلزار چون بهشت شود فصل نو بهار</p></div>
<div class="m2"><p>جانم فدای آن که از این هر دو خوشتر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم به سرو اگر چه خرامیدنت خوش است</p></div>
<div class="m2"><p>بالای خوش خرام دلارام دیگر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در جان نیشکر نبود آن حلاوتی</p></div>
<div class="m2"><p>کاندر لب و حدیث و دهان تو مضمر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از بندگیت دورم و جان با تو در حضور</p></div>
<div class="m2"><p>جایی که نیست خلوت جان چشم بر در است</p></div></div>