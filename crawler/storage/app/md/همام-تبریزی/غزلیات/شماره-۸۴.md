---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>آن را که حسن و شکل و شمایل چنین بود</p></div>
<div class="m2"><p>چندان که ناز بیش کند نازنین بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقتی در آب و آینه می‌بین جمال خویش</p></div>
<div class="m2"><p>کز روزگار حاصل عمرت همین بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با خود نشین و همدم و هم‌راز خویش باش</p></div>
<div class="m2"><p>حیف آیدم که با تو کسی همنشین بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دوست آن خیال جوانی بود نه عشق</p></div>
<div class="m2"><p>هر دوستی که تا نفس واپسین بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی که زین جهان به جهان دگر شوم</p></div>
<div class="m2"><p>در جان من خیال تو نقش نگین بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر جا که می‌روی قدمی باز پس نگر</p></div>
<div class="m2"><p>سرها ببین که بر سر روی زمین بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر آدمی ملایکه انکار کرده‌اند</p></div>
<div class="m2"><p>معلومشان نبود که انسان چنین بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاغذ ز شرم پاره شود بشکند قلم</p></div>
<div class="m2"><p>گر صورتت برابر نقاش چین بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون از لبت همام حدیثی کند تمام</p></div>
<div class="m2"><p>ز آب حیات بر سخنش آفرین بود</p></div></div>