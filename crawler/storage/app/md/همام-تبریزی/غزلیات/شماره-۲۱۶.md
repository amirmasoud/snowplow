---
title: >-
    شمارهٔ ۲۱۶
---
# شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>ای گل از غنچه کی برون آیی</p></div>
<div class="m2"><p>که شدم ز انتظار سودایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبلان را نمی‌برد شب خواب</p></div>
<div class="m2"><p>تا سحرگه نقاب بگشایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با صبا گفته‌ای که می‌آیم</p></div>
<div class="m2"><p>من و این مژده و شکیبایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه پیشم هزار تن بیشند</p></div>
<div class="m2"><p>بی تو جان می‌دهم ز تنهایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده در آرزوی دیدارت</p></div>
<div class="m2"><p>وعده‌ای می‌دهد به بینایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر چشم من قدم نه تا</p></div>
<div class="m2"><p>جویباری به گل بیارایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تشنه در اشتیاق آب حیات</p></div>
<div class="m2"><p>سوخت خود رحمتی نفرمایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر نظر محرم جمال تو نیست</p></div>
<div class="m2"><p>دیده ها زان شدند هرجایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از حدیثت همام را ذوقی</p></div>
<div class="m2"><p>به زبان می‌رسد ز گویایی</p></div></div>