---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>غرض ز دیدن شام و دیار مصر و حجاز</p></div>
<div class="m2"><p>اگر حضور عزیزان بود زهی اعزاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به راه دوست گرت عزم اشتیاق بود</p></div>
<div class="m2"><p>برو که راحت جان است رنج راه دراز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود حرام سفر بر مسافری که رود</p></div>
<div class="m2"><p>به عزم یار و خبر دارد از نشیب و فراز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کبوتری که برد پیش دوست نامۀ دوست</p></div>
<div class="m2"><p>به بال ذوق میان هوا کند پرواز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شتر زتشنگی و خستگی شود غافل</p></div>
<div class="m2"><p>اگر به ذوق کند ساربان سرود آغاز</p></div></div>