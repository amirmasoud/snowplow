---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>زهی شمایل موزون و قد دلبندش</p></div>
<div class="m2"><p>که هر که دید رخش گشت آرزومندش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر او در آینه و آب ننگرد زین پس</p></div>
<div class="m2"><p>کسی نشان ندهد در زمانه مانندش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن نفس که لبش در حدیث می‌آید</p></div>
<div class="m2"><p>روان همی‌شود آب حیات از قندش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو باغبان به قدش بنگرید هر سروی</p></div>
<div class="m2"><p>که دید بر لب جویی ز بیخ برکندش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو آن سعادت بند قباش می‌بینی</p></div>
<div class="m2"><p>که هست با قد او سال و ماه پیوندش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خون لعل چنان تشنه‌ام که نتوان گفت</p></div>
<div class="m2"><p>ز رشک آن که زند دست در کمربندش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دریغ دیده مسکین من که چشم حسود</p></div>
<div class="m2"><p>ز روز وصل به شب‌های دوری افکندش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون که چشم من از روی دوست دور افتاد</p></div>
<div class="m2"><p>مگر به ماه کنم گاه گاه خرسندش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وگر همام نگیرد قرار پس تدبیر</p></div>
<div class="m2"><p>بود مطالعه طلعت خداوندش</p></div></div>