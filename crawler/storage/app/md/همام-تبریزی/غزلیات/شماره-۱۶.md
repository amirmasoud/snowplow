---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>بوی خوشت همره باد صباست</p></div>
<div class="m2"><p>آنچه صباراست میسر که راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوش چو بوی تو به گلزار برد</p></div>
<div class="m2"><p>نالۀ مرغان سحرخوان بخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل چو نسیم تو شنید از صبا</p></div>
<div class="m2"><p>گفت که این بوی بهشت از کجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای گل نوخاسته آگه نه‌ای</p></div>
<div class="m2"><p>کاین اثر بوی دلارام ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست من و دامن باد صبا</p></div>
<div class="m2"><p>ناز پی زلف نگارم چراست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باد کجا لایق سودای اوست</p></div>
<div class="m2"><p>عشق و هوا هر دو به هم نیست راست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صحبت آیینه نخواهد همام</p></div>
<div class="m2"><p>در نظرش گر چه سراسر صفاست</p></div></div>