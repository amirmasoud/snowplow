---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>بوسه‌ای را گر به جان شاید خرید</p></div>
<div class="m2"><p>جان بباید داد روز من یزید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یافتن معشوق را چون ممکن است</p></div>
<div class="m2"><p>از وصال امید نتوانم برید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پای اگر عاجز شود نتوان نشست</p></div>
<div class="m2"><p>گه به پهلو گه به سر خواهم دوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نفس آید نشاید دم زدن</p></div>
<div class="m2"><p>تا رگی جنبد نشاید آرمید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشقان کعبه را در بادیه</p></div>
<div class="m2"><p>ای بسا زحمت که می‌باید کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساروان را گو سرود آغاز کن</p></div>
<div class="m2"><p>تا در اشتر غیرتی آید پدید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز ای مطرب حدیث خوش بساز</p></div>
<div class="m2"><p>خرقه برکن از تن پیر و مرید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساقیا می ده که مشتاقان هنوز</p></div>
<div class="m2"><p>میزنند از تشنگی هل من مزید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از شراب جان مستانت همام</p></div>
<div class="m2"><p>جرعه‌ای می‌خواست خود بویی شنید</p></div></div>