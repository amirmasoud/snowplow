---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>جان را به جای جانی جای تو کس نگیرد</p></div>
<div class="m2"><p>مهر تو زنده ماند روزی که تن بمیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر درد را علاجی بنوشته‌اند یارا</p></div>
<div class="m2"><p>دردی که هست ما را درمان نمی‌پذیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دوستان ملامت کمتر کنید ما را</p></div>
<div class="m2"><p>با خستگان هجران افسانه درنگیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پروانه چون بسوزد آخر خلاص یابد</p></div>
<div class="m2"><p>بیچاره آن که دایم می‌سوزد و نمیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم مگر صبوری کار همام باشد</p></div>
<div class="m2"><p>دل را به هیچ وجهی زان رخ نمی‌گزیرد</p></div></div>