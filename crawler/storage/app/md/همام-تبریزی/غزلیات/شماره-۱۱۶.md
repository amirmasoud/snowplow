---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>این نه دردیست که بی دوست بود درمانش</p></div>
<div class="m2"><p>خُنُک آن جان که نصیبی بود از جانانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل گوید به نصیحت که مده جان به لبش</p></div>
<div class="m2"><p>عشق فریاد برآرد که مکن فرمانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای منجم نظر از ماه و ثریا بستان</p></div>
<div class="m2"><p>چون بخندد مه خوبان بنگر دندانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غنچه بر خنده خود خنده زند وقت سحر</p></div>
<div class="m2"><p>گر ببیند نمک آن دو لب خندانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر رهی را که در او پای نهی پایانی‌ست</p></div>
<div class="m2"><p>جز ره دوست که پیدا نبود پایانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه دانند که هر طایفه ورزد کیشی</p></div>
<div class="m2"><p>کیش من آن که شود جان و دلم قربانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک پایش چو منی را نرسد می‌کوشم</p></div>
<div class="m2"><p>که رسد چشم مرا گرد سم یکرانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هستی خویش نهادم همه در وجه رخش</p></div>
<div class="m2"><p>گفت کآسان نفروشم ندهم ارزانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد خیال سر زلفت سبب کفر همام</p></div>
<div class="m2"><p>روی بنمای که تا تازه شود ایمانش</p></div></div>