---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>میان ما و شما بود پیش از آن پیوند</p></div>
<div class="m2"><p>که جان علوی ما شد در این قفس در بند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجز دهان لطیفت که با نسیم گل است</p></div>
<div class="m2"><p>ندید دیده مردم گلاب‌دان از قند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب خوشت که فدا باد آب حیوانش</p></div>
<div class="m2"><p>نداد آبم و در جانم آتشی افکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چگونه از ملک انسان شریف‌تر نبود</p></div>
<div class="m2"><p>که ز آدمی چو تو پیدا همی‌شود فرزند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز ذوق بی‌خبر است آن که می‌کند تشبیه</p></div>
<div class="m2"><p>رخت به چشمهٔ خورشید و قد به سرو بلند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آب و خاک نیاید کسی بدین خوبی</p></div>
<div class="m2"><p>در آن جهان مگر از روح صورتی سازند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگو که چاره من چیست از مشاهده‌ات</p></div>
<div class="m2"><p>به حسن هیچ کسی چون نمی‌شود خرسند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه غیرتم آید میان شهر برآی</p></div>
<div class="m2"><p>زبان هر که مرا پند می‌دهد دربند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همام چون که سلامت کند ملول مشو</p></div>
<div class="m2"><p>گرش جواب نگویی به زیر لب می‌خند</p></div></div>