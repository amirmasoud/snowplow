---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>دوستی دامنت آسان نتوان داد ز دست</p></div>
<div class="m2"><p>جان شیرین منی جان نتوان داد ز دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفسی گر نشکیبم ز لبت معذورم</p></div>
<div class="m2"><p>تشنه‌ام چشمهٔ حیوان نتوان داد ز دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کردم اندیشه سر خویش توان داد به تو</p></div>
<div class="m2"><p>آن سر زلف پریشان نتوان داد ز دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون منی گر برود برگ گیاهی کم گیر</p></div>
<div class="m2"><p>قامت سرو خرامان نتوان داد ز دست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه اندوخته‌ام گر برود باکی نیست</p></div>
<div class="m2"><p>شرف صحبت جانان نتوان داد ز دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل زندانی خود را چو خلاصی جستم</p></div>
<div class="m2"><p>گفت کان چاه زنخدان نتوان داد ز دست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ملامت نشود دور همام از لب یار</p></div>
<div class="m2"><p>خار گو باش گلستان نتوان داد ز دست</p></div></div>