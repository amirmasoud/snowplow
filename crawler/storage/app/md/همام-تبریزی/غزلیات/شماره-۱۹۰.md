---
title: >-
    شمارهٔ ۱۹۰
---
# شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>ترسا بچه‌ای ناگه بر کف می گلناری</p></div>
<div class="m2"><p>از صومعه باز آمد سرمست به عیاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنشست چو عیاران آن مونس غم‌خواران</p></div>
<div class="m2"><p>از پسته خندان کرد آغاز شکر باری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افتاد ز عشق او در صومعه غوغایی</p></div>
<div class="m2"><p>جستند ز سالوسی پیران همه بیزاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دیدهٔ پیر ما شد اشک روان حالی</p></div>
<div class="m2"><p>چون دید مریدان را از عشق بدان زاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگشاد زبان کای زین این بی‌ادبی تا چند</p></div>
<div class="m2"><p>از روی چنین پیری خود شرم نمی‌داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترسا بچه گفت او را من گر چه ز می مستم</p></div>
<div class="m2"><p>ای پیر تو نیز آخر مست می پنداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من مستم و آگاهم از مستی خود باری</p></div>
<div class="m2"><p>مستی تو و می‌لافی از عالم هشیاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای پیر از این مستی هشیار شوی حالی</p></div>
<div class="m2"><p>گر نوش کنی جامی زین بادهٔ گلناری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیر از سخن کودک زد چاک گریبان را</p></div>
<div class="m2"><p>برخاست غرامت را افتاده به صد خواری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می بستد و خندان شد بر وی همه آسان شد</p></div>
<div class="m2"><p>اندر صف رندان شد مشهور به می‌خواری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دردا که چنین پیری دردی‌کش طفلی شد</p></div>
<div class="m2"><p>از گفته ترسایی برگشت ز دین‌داری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر بیدل بی معنی این رمز کجا داند</p></div>
<div class="m2"><p>مگشای همام این سر گر صاحب اسراری</p></div></div>