---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>در شهر بگویید چه فریاد و فغان است</p></div>
<div class="m2"><p>آن سرو مگر باز به بازار روان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قومی بدویدند به نظاره رویش</p></div>
<div class="m2"><p>وان را که قدم سست شد از پی نگران است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هر قدمش از همه فریاد برآمد</p></div>
<div class="m2"><p>آهسته که بر ره دل صاحب‌نظران است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شاهد اگر میل به آن است شما را</p></div>
<div class="m2"><p>این است جمالی که سراسر همه آن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب‌ها به امیدند که یک بار ببوسند</p></div>
<div class="m2"><p>خاکی که بر او از قدم دوست نشان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دیده در آن شکل و شمایل نظری کن</p></div>
<div class="m2"><p>گر زان که تو را آرزوی دیدن جان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رویی و در او چشم جهانی متحیر</p></div>
<div class="m2"><p>زلفی که پریشانی احوال جهان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون جان همام است در آن دام گرفتار</p></div>
<div class="m2"><p>گر خاطر پیر است و گر طبع جوان است</p></div></div>