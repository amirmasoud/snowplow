---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>زاهدان با شاهدان همخانه‌اند</p></div>
<div class="m2"><p>گرد هر شمعی دو صد پروانه‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اهل دل در بت پرستی آمدند</p></div>
<div class="m2"><p>شاهدان بت، دیده‌ها بتخانه‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با پری‌رویان نماند عقل و هوش</p></div>
<div class="m2"><p>جمله معذورند اگر دیوانه‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی خوب آیینه خود ساختند</p></div>
<div class="m2"><p>در سر زلف بتان چون شانه‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لب معشوق می‌نوشند می</p></div>
<div class="m2"><p>فارغ از خم‌خانه و پیمانه‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عارفان ما را ملامت می‌کنند</p></div>
<div class="m2"><p>آن گران‌جانان ز دل بیگانه‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردم بینا دل جوهرشناس</p></div>
<div class="m2"><p>چون همام اندر پی دردانه‌اند</p></div></div>