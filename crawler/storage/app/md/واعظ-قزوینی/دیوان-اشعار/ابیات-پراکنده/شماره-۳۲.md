---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>پنبه سان شکستی چوب نرمی پیشه را</p></div>
<div class="m2"><p>نیست سنگی سخت تر از سختی خود شیشه را</p></div></div>