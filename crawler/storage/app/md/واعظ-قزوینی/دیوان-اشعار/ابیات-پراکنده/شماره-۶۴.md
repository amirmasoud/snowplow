---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>گوشمالی ز فلک، گوش ترا به ز در است</p></div>
<div class="m2"><p>صفحه سیلی استاد از این نکته پر است</p></div></div>