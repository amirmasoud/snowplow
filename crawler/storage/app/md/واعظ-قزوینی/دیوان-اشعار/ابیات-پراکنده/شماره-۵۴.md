---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>گفتار نرم، آب رخ آدمیت است</p></div>
<div class="m2"><p>روی گشاده آینه حسن سیرت است!</p></div></div>