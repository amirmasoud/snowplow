---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>ز بسکه راستییی در جهان نمی بینم</p></div>
<div class="m2"><p>براه هم نتوانم سپرد دشمن را</p></div></div>