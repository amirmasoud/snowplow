---
title: >-
    شمارهٔ ۲۳۱
---
# شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>چشم بستیم از جهان، تا آشنای او شدیم</p></div>
<div class="m2"><p>از غبار درگه او، گل بر این روزن زدیم!</p></div></div>