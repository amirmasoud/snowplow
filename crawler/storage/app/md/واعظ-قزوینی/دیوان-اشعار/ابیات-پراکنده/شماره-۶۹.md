---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>عیش گیتی پر نمک و شور و شر است</p></div>
<div class="m2"><p>کیفیت از این شراب جستن غلط است</p></div></div>