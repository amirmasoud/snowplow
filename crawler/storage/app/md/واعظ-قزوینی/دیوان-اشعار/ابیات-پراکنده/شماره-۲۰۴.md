---
title: >-
    شمارهٔ ۲۰۴
---
# شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>گریه از کردار ما، مقبول جانان است وبس</p></div>
<div class="m2"><p>شبنم از گلشن، پسند مهر تابان است و بس</p></div></div>