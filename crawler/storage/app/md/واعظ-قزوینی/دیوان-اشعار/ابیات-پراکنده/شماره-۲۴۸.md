---
title: >-
    شمارهٔ ۲۴۸
---
# شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>چو افروزد رخش، سوزد دل و دین</p></div>
<div class="m2"><p>چو در پا شد لبش، برخیز و بر چین</p></div></div>