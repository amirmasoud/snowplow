---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>گر پرتو جمال تو افتد بروی آب</p></div>
<div class="m2"><p>از حیرت تو موج چو نقش نگین شود</p></div></div>