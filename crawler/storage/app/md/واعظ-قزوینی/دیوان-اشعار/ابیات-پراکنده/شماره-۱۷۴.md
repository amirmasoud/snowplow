---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>رفتم از خود، خویشتن را بسکه دزدیدم بخود</p></div>
<div class="m2"><p>رشته عمرم گسست، از بسکه پیچیدم بخود</p></div></div>