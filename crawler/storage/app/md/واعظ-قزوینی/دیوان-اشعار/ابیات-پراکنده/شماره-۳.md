---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>حاصل ندامت است غرور و شتاب را</p></div>
<div class="m2"><p>دست تأسف است گزک، این شراب را</p></div></div>