---
title: >-
    شمارهٔ ۱۹۷
---
# شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>بخشم و شهوت و حرص، اینقدر مده خود را</p></div>
<div class="m2"><p>نگاهبانی بستان، بدام و دد مگذار</p></div></div>