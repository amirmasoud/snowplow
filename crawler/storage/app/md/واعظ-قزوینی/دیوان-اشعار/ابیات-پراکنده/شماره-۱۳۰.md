---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>مبند دل به عطای جهان، که چون شبنم</p></div>
<div class="m2"><p>هرآنچه شب دهدت، روز باز می‌گیرد!</p></div></div>