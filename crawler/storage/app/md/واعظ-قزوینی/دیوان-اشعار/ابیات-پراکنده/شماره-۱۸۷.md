---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>ز غفلت، مردمان را پند گفتن خوش نمیآید</p></div>
<div class="m2"><p>چو مست خواب را بیدار کردن خوش نمیآید</p></div></div>