---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>ترکست که سازد غنی از خلق، و گر نه</p></div>
<div class="m2"><p>شه نیز بابرام ستم کم ز گدا نیست</p></div></div>