---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>منفعل نرگس از نگاهت شد</p></div>
<div class="m2"><p>گل خجل پیش روی ماهت شد</p></div></div>