---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>جان سوخت، چو آمد سخن لعل تو بر لب</p></div>
<div class="m2"><p>دل رفت، چو دیدیم رخ ماه تو درشب</p></div></div>