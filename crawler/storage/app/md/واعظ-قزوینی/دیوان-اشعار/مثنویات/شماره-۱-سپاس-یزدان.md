---
title: >-
    شمارهٔ ۱ - سپاس یزدان
---
# شمارهٔ ۱ - سپاس یزدان

<div class="b" id="bn1"><div class="m1"><p>سزاوار شکر آفریننده ییست</p></div>
<div class="m2"><p>که هر قطره از وی دل زنده ییست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبان در دهن غنچه فکر اوست</p></div>
<div class="m2"><p>سخن در نفس سبحه ذکر اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سرچشمه حکمتش خورده آب</p></div>
<div class="m2"><p>کدوی فلک، نرگس آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بس هست بحر عطایش فراخ</p></div>
<div class="m2"><p>سبو پر کند غنچه از جوی شاخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمان، جویی از قلزم حکمتش</p></div>
<div class="m2"><p>مکان، گردی از لشکر شوکتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از او در سفر مهر گیتی فروز</p></div>
<div class="m2"><p>شفق آتش کاروانگاه روز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمین را نهیبش بخاطر گذشت</p></div>
<div class="m2"><p>که از سبزه مو بر تنش راست گشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گذشتش مگر قهر او بر زبان</p></div>
<div class="m2"><p>که تبخال زد از نجوم آسمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرانگشت صنعش ز درج سپهر</p></div>
<div class="m2"><p>بخیط شعاعی کشد لعل مهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دهد روز را غازه آفتاب</p></div>
<div class="m2"><p>کشد شانه بر زلف شب از شهاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نخست از دم صبح گیتی فروز</p></div>
<div class="m2"><p>نمک آورد بر سر خوان روز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حضیض سپهر بزرگیش اوج</p></div>
<div class="m2"><p>ز بحر جلالش دو گیتی دو موج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنانست از او چشمه آفتاب</p></div>
<div class="m2"><p>کز آن سنگ آتش برد، لعل آب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز پستان خورشید تابان ز دور</p></div>
<div class="m2"><p>لب ماه نو میمکد شیر نور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخیاطی جامه گل بهار</p></div>
<div class="m2"><p>کند رشته از آب و سوزن ز خار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز باران رگ ابر، تسبیح دار</p></div>
<div class="m2"><p>شب و روز گردان بدست بهار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دود شحنه باد ازو هر طرف</p></div>
<div class="m2"><p>سر پالهنگ سحابش بکف</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنان رزق را رانده سوی بدن</p></div>
<div class="m2"><p>که بر شکر تنگ است راه دهن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پی رزق موران بی دست و پا</p></div>
<div class="m2"><p>کشد خوشه با دوش خود دانه را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز شوق لب زرق خواران ز خاک</p></div>
<div class="m2"><p>دود دانه تا آسیا سینه چاک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کند از نمو دانه گر سرکشی</p></div>
<div class="m2"><p>ز باران کند ابر لشکر کشی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنان رعد بر سبزه هی میزند</p></div>
<div class="m2"><p>که از دانه قالب تهی میکند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رود سبزه راه نمو زآن بفرق</p></div>
<div class="m2"><p>که خون میچکد از دم تیغ برق</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو بی اعتدالی نماید سحاب</p></div>
<div class="m2"><p>میانجی کند پرتو آفتاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شوند این دولشکر چو از هم جدا</p></div>
<div class="m2"><p>بدلجویی سبزه آید هوا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زهی لطف کز رحمت بیکران</p></div>
<div class="m2"><p>نتابد رخ بخشش از عاصیان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر خشم گیرد کس از خدمتش</p></div>
<div class="m2"><p>در آشتی میزند رحمتش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کریمی که از بهر عذر گناه</p></div>
<div class="m2"><p>نشان داده درگاه خود را بآه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بآیینه دل چنان داده رو</p></div>
<div class="m2"><p>که آغوش وا کرده بر یاد او</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عطا کرد، از گنج انعام خویش</p></div>
<div class="m2"><p>به دل یاد خویش و به لب نام خویش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نفس در میان شد چنان بی سکون</p></div>
<div class="m2"><p>که یک پا درون است و، یک پا برون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز دل داد شهباز غم را، نوال</p></div>
<div class="m2"><p>ز لب داد مرغ سخن را، دو بال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز مرخ و عفار دو لب صنع او</p></div>
<div class="m2"><p>برون آورد آتش گفت و گو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کند از نفس نیچه، دیگ از دهن</p></div>
<div class="m2"><p>کشد از زبان تا گلاب سخن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سخن را ز دل، همچو آب روان</p></div>
<div class="m2"><p>فرو ریزد از آبشار زبان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>روان سازد از نور نظاره ها</p></div>
<div class="m2"><p>ز دریاچه دیده فواره ها</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سخن را به تار نفسها کشان</p></div>
<div class="m2"><p>رسن در گلو آورد تا زبان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فغان کرد، ورد زبان جرس</p></div>
<div class="m2"><p>سخن کرد، پیکان تیر نفس</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به ناوک تلاش نهنگی دهد</p></div>
<div class="m2"><p>به پیکان دل پیش جنگی دهد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به فرمان او، تیغ در کینه ها</p></div>
<div class="m2"><p>دود چون نفس، راست تا سینه ها</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گه فتنه، چون باد حکمش وزد</p></div>
<div class="m2"><p>ببحر کمانها فتد جزر و مد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تعالی! چه شأن جلالست این؟!</p></div>
<div class="m2"><p>تقدس! چه قدر کمالست این؟!</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>باین پاکی ذات و این عزشان</p></div>
<div class="m2"><p>نتابد رخ لطف از خاکیان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>روان بر فلک شوکت عزتش</p></div>
<div class="m2"><p>کشان بر زمین، دامن رحمتش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چنان مهر او پرتو افگنده است</p></div>
<div class="m2"><p>کزو دانه در خاک هم زنده است</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از او سبزه ها چون زبان پرنوا</p></div>
<div class="m2"><p>از او لاله چون کاسه ها پرصدا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کند بحر و بر هر دو ذکرش، ولی</p></div>
<div class="m2"><p>بود ذکر این یک خفی، و آن جلی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بود محو نورش، چه بحر و چه بر</p></div>
<div class="m2"><p>بود پر ز شورش، چه بام و چه در</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کف ابرها، سوی بحرش دراز</p></div>
<div class="m2"><p>سر قطره ها، بر زمین نیاز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>فلکهاست، سرها بفتراک او</p></div>
<div class="m2"><p>زمینها، جبینهاست بر خاک او</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همه بنده او، چه جزو و چه کل</p></div>
<div class="m2"><p>همه زنده او، چه خار و چه گل</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز دلها رهی کرده تا کوی خویش</p></div>
<div class="m2"><p>در این ره برد ناله را سوی خویش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>فغان را دهد جوهر کر و فر</p></div>
<div class="m2"><p>دعا را دهد دست و پای اثر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بلب رخصت عرض حاجات داد</p></div>
<div class="m2"><p>بدل خار خار مناجات داد!</p></div></div>