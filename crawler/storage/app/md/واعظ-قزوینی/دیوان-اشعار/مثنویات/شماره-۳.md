---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>فرازنده دست و تیغ زبان</p></div>
<div class="m2"><p>چنین کرده تسخیر ملک بیان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که شاه ملک خیل انجم سپاه</p></div>
<div class="m2"><p>جهانبخش دریا دل دین پناه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندانم چه در مدح آن شه سزاست</p></div>
<div class="m2"><p>بجر اینکه فرزند شیر خداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بفرزندیش داده شاه نجف</p></div>
<div class="m2"><p>ز شمشیر، برهان قاطع بکف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد از تیغ او خانه انقلاب</p></div>
<div class="m2"><p>بسیلاب خون مخالف خراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز موج نهیبش دل دشمنان</p></div>
<div class="m2"><p>چو از حمله باد، ریگ روان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بمشاطگی کرد از تیغ کین</p></div>
<div class="m2"><p>ز خون عدو غازه روی دین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زره پیش شمشیر آن نامدار</p></div>
<div class="m2"><p>چو پیش تف شعله موج چنار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سمندش فشردی چو بر کوه نعل</p></div>
<div class="m2"><p>خزیدی چو خون در رگ سنگ لعل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بشمشیر تا بازوش یار شد</p></div>
<div class="m2"><p>رگ کوه انگشت زنهار شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز روزی که عدلش بدیوان نشست</p></div>
<div class="m2"><p>فراری است هر بد نهادی که هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شود تا سبکبار بهر فرار</p></div>
<div class="m2"><p>به هر سال می افگند پوست، مار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در ایم آن شاه فیروز جنگ</p></div>
<div class="m2"><p>نمیبرد ترسیدن از چهره رنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ستم را نبود آن قدر دسترس</p></div>
<div class="m2"><p>که پیری ستاند جوانی ز کس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در ایام عدلش کسی را چو حد</p></div>
<div class="m2"><p>که لب از ندامت بدندان گزد؟!</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به عهدش چنان روزگار آرمید</p></div>
<div class="m2"><p>که دل از تپیدن نشانی ندید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه دست سیاست چنان زو قوی</p></div>
<div class="m2"><p>که جرأت کند ناله در شبروی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رود از آن بهر سوی صیقل دوتا</p></div>
<div class="m2"><p>که برده است زنگ از دل آیینه را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ندادیش اگر باغبان اول آب</p></div>
<div class="m2"><p>نیارستی از گل گرفتن گلاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بشاخی اگر زور کردی ثمر</p></div>
<div class="m2"><p>نهادی بپایش همان لحظه سر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به ترویج مذهب میان را چو بست</p></div>
<div class="m2"><p>به گلگون کشورگشایی نشست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به هرسو علم راست کردی ز کین</p></div>
<div class="m2"><p>شدی راست همچون علم پشت دین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شدی شعله تیغ او چون بلند</p></div>
<div class="m2"><p>سر سرکشان ساختی چون سپند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کمند عزیمت به هر سو فگند</p></div>
<div class="m2"><p>سر مرز و بومی در آمد به بند</p></div></div>