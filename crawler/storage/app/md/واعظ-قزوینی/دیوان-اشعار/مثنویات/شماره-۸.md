---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>وزآن سو چو شیبانی بدگهر</p></div>
<div class="m2"><p>شنید این خبرهای وحشت اثر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاندیشه لرزید بر خود چنان</p></div>
<div class="m2"><p>که ویران شدش خانه عمر از آن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان بدلش زد قفا آن خبر</p></div>
<div class="m2"><p>که افتاد از آن تاج هوشش زسر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ره مرو بگرفت از ترس، پیش</p></div>
<div class="m2"><p>گریزان شد از بیم چو رنگ خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اندیشه تیغ خونریز شاه</p></div>
<div class="m2"><p>شدش قلعه مرو، آرامگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلی مهر چون برگشاید لوا</p></div>
<div class="m2"><p>کند سایه در پشت دیوار جا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمودند القصه مردان کار</p></div>
<div class="m2"><p>زهر جانبی رخنه ها استوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسی محکم آن برج و آن باره شد</p></div>
<div class="m2"><p>سپه چون شرر، قلعه چون خاره شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدینگونه بودند تا چند روز</p></div>
<div class="m2"><p>خبر شد که آمد شه خصم سوز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل شاه ترکان از آن گشت ریش</p></div>
<div class="m2"><p>دلیران خود را طلب کرد پیش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زهر سو در گفت و گو باز کرد</p></div>
<div class="m2"><p>ره مصلحت بینی آغاز کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که اینک رسید آفت کشت زیست</p></div>
<div class="m2"><p>چه گویید و تدبیر این کار چیست؟!</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگفتند کای خسرو نامدار</p></div>
<div class="m2"><p>چه حاصل دهد بودن این حصار؟!</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو رو آورد پشت ایران زمین</p></div>
<div class="m2"><p>نپاید برش قلعه آهنین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر این قلعه از محکمی خیبر است</p></div>
<div class="m2"><p>شه از نسل شیرخدا حیدر است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در این قلعه بودن نه ما را رواست</p></div>
<div class="m2"><p>که این قلعه زندان جولان ماست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپاهی ز ترکان تشنه بخون</p></div>
<div class="m2"><p>از این قلعه باید فرستی برون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که بندند با آب شمشیر کین</p></div>
<div class="m2"><p>سر راه بر شاه ایران زمین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که با دیده جوهر تیغها</p></div>
<div class="m2"><p>به بینیم شاید رخ مدعا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برین یافت چون رای ایشان قرار</p></div>
<div class="m2"><p>سپاهی چو اندوه خود بیشمار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه نامداران جنگ آزما</p></div>
<div class="m2"><p>همه شیر صولت، همه اژدها</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه تند خویان بیرحم دل</p></div>
<div class="m2"><p>همه جنگ جویان از جان گسل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همه، غره بر زور بازوی خویش</p></div>
<div class="m2"><p>همه، آتش از تندی خوی خویش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز بس باد نخوت بسر بودشان</p></div>
<div class="m2"><p>حباب است گفتی کله خودشان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از آن قلعه بردشت کین تاختند</p></div>
<div class="m2"><p>علم از رگ گردن افراختند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که ناگاه پیدا شد از گرد غم</p></div>
<div class="m2"><p>صفی صور طبل قیامت علم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ظفر، پرچم رایت عزمشان</p></div>
<div class="m2"><p>اجل، تیزی خنجر رزمشان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز حشمت، نگهبانشان از ملک</p></div>
<div class="m2"><p>ز عزت، سر گردشان بر فلک</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همه تیغ وش، جان ستان، کینه خواه</p></div>
<div class="m2"><p>شده قبضه سان یکسر آهن کلاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زره، جوهر خویشتن بودشان</p></div>
<div class="m2"><p>ز سرسختی خود، کله خودشان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سپهدار آن لشکر، اقبال شاه</p></div>
<div class="m2"><p>صف آرا، بزرگی و اجلال شاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به هم چون رسیدند آن هر دو فوج</p></div>
<div class="m2"><p>رساندند نام دلیری به اوج</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دو لشکر بهم حمله آور شدند</p></div>
<div class="m2"><p>دو غوغا بهم شور محشر شدند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز باریدن تیر و تیغ یلان</p></div>
<div class="m2"><p>شد از هر طرف جوی خونی روان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو شد روشن از برق شمشیر تیز</p></div>
<div class="m2"><p>به ترکان در آن گرد، راه گریز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عنان سوی آن راه بر تافتند</p></div>
<div class="m2"><p>دوان جانب قلعه بشتافتند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گرفتند از تیر بیرحم کیش</p></div>
<div class="m2"><p>سپر بر رخ خویش از پشت خویش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دلیران و گردان ایرانیان</p></div>
<div class="m2"><p>اجل سان زدنبال ایشان دوان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو ترکان بدان قلعه داخل شدند</p></div>
<div class="m2"><p>از آن بحر پرخون بساحل شدند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>برون حصار آن شه کامیاب</p></div>
<div class="m2"><p>فرود آمد از باد پای شتاب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>روان چتر و خرگاه آراستند</p></div>
<div class="m2"><p>بخدمت ستونها بپا خاستند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز اقبال آن شاه فرخ لقا</p></div>
<div class="m2"><p>شدش دامن خیمه بال هما</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>طنابی ز هرسوی آن بارگاه</p></div>
<div class="m2"><p>چو دست دلیران و دامان شاه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به پا خاست زآن لشکر بی حساب</p></div>
<div class="m2"><p>بسی خیمه ها چون ز دریا حباب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به هرسو طنابی ز خرگاه ها</p></div>
<div class="m2"><p>نمایان چو از کوهها راه ها</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گذر کرد چندان طناب از طناب</p></div>
<div class="m2"><p>که ره بست بر تابش آفتاب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در آن خیمه ها لشکر بیشمار</p></div>
<div class="m2"><p>چراغ ته دامن روزگار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز دامان خرگاه زرین طناب</p></div>
<div class="m2"><p>پراگنده شد آتش آفتاب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شد از سوز تب لرزه آن حصار</p></div>
<div class="m2"><p>لب خندق از خیمه تبخاله زار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو خورشید تابنده روز دگر</p></div>
<div class="m2"><p>ز بالین کهسار برداشت سر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بنظاره رزم، زال سپهر</p></div>
<div class="m2"><p>گرفت ابرو صبح از چشم مهر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دگر بار ترکان برون تاختند</p></div>
<div class="m2"><p>لوای نگونساری افراختند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بفرمود آن شاه دشمن گداز</p></div>
<div class="m2"><p>سپه را بخونریزی خصم، باز</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دگر شعله تیغ ها شد بلند</p></div>
<div class="m2"><p>دگر ره سر خصم شد چون سپند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دگر بار از بیم تیغ یلان</p></div>
<div class="m2"><p>سوی قلعه گشتند ترکان دوان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز هر سو دلیران آرام سوز</p></div>
<div class="m2"><p>چنین رزم جستند، تا هفت روز</p></div></div>