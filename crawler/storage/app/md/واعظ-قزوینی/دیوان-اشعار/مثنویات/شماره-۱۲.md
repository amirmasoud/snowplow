---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>چه عبرت از این خوبتر ای گزین</p></div>
<div class="m2"><p>که شد تاجداری چنان این چنین؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه عبرت از این خوبتر ای فلان</p></div>
<div class="m2"><p>که شد شه نشانی چنین بی نشان؟!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرآنچنان سرکشی تیز چنگ</p></div>
<div class="m2"><p>فلک ساخت جام می لاله رنگ!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشیدند بر سر گه سرخوشی</p></div>
<div class="m2"><p>شد آن سرکشی آخر این سرکشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا یکدم از خواب بیدار شو</p></div>
<div class="m2"><p>ز سرمستی کبر هشیار شو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نظر روشن از اعتباری بکن</p></div>
<div class="m2"><p>به تاریخ شاهان گذاری بکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به عبرت نظر کن سوی رفتگان</p></div>
<div class="m2"><p>که فردا شوی عبرت دیگران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بزرگی که سودی به گردون سرش</p></div>
<div class="m2"><p>نظر کن که چو خاک شد پیکرش!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حریصی که صد مطلبش بود بیش</p></div>
<div class="m2"><p>ز عهدش گذشته است صد قرن پیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حریفی که میخواست آغوش حور</p></div>
<div class="m2"><p>کشید است تنگش در آغوش، گور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شهانی که بودند مالک رقاب</p></div>
<div class="m2"><p>نیاید کنون نامشان در حساب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسی شه که نامش ز زر داشت عار</p></div>
<div class="m2"><p>که محو است نامش ز سنگ مزار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسا گرد شیر افگن پیل زور</p></div>
<div class="m2"><p>که سر رفتش آخر بباد غرور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرفتم خبر از جم و جام او</p></div>
<div class="m2"><p>از ان عاقبت تلخ شد کام او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سکندر که صد سال عالم گرفت</p></div>
<div class="m2"><p>چسان مرگش آخر بیکدم گرفت؟!</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کجا رفت پرویز و آیین او؟</p></div>
<div class="m2"><p>کجا رفت آن عیش شیرین او؟!</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه ضحاک خوردی سر مردمان؟</p></div>
<div class="m2"><p>چه سان خورد آخر سرش را جهان؟!</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه کاوس کی سوی افلاک راند؟</p></div>
<div class="m2"><p>نگه کن اجل چون بخاکش نشاند؟!</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه شد شوکت و شان افراسیاب</p></div>
<div class="m2"><p>نشان زو ندارد جهان خراب!</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چه شد زال زر آن یل شیر گیر؟</p></div>
<div class="m2"><p>چه سان کرد زال سپهرش اسیر؟!</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تهمتن که کردی از او شیر رم</p></div>
<div class="m2"><p>پلنگ اجل چون دریدش ز هم؟!</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر آمد برون بیجن از چاه و بند</p></div>
<div class="m2"><p>اجل باز در چاه گورش فگند!</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز دور زمان نگذرد اندکی</p></div>
<div class="m2"><p>که خواهی تو هم بود از ایشان یکی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو مرگی چنین هست از پی دوان</p></div>
<div class="m2"><p>خبردار باش از فریب جهان!</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه باشد جهان؟ گلخنی تنگ و تار!</p></div>
<div class="m2"><p>در او مال دنیا چو آتش شمار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مزن بهر این آتش بی ضیا</p></div>
<div class="m2"><p>چو آهن سر خویش بر سنگها</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که مانند هیزم در او عمر کاست</p></div>
<div class="m2"><p>که آخر چو دود از سرش برنخاست؟!</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو آتش که را روی گرمی نمود</p></div>
<div class="m2"><p>که از هستیش بر نیاورد دود؟!</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر چون سپندش کنی جان نثار</p></div>
<div class="m2"><p>بدور افگند آخرت چون شرار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو ترک است لاله باغ خوشی</p></div>
<div class="m2"><p>ندانم چرا داغ این آتشی؟!</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زآتش چه میماند ای عمرکاه</p></div>
<div class="m2"><p>به کف داغ را غیر روی سیاه؟!</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زر و سیمت ار حل مشکل کند</p></div>
<div class="m2"><p>بخاک سیاهت مقابل کند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بلی عقده نی ز آتش گشاد</p></div>
<div class="m2"><p>ولی داد چو هستیش را بباد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بهم بسته رنج و زر اندوختن</p></div>
<div class="m2"><p>ز آتش نگردد جدا سوختن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مدان پختگی کسب مال حرام</p></div>
<div class="m2"><p>کزین آتشت کار گردید خام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ترا فکر زر برد و دین شد ز دست</p></div>
<div class="m2"><p>تو آتش پرستی، نیی حق پرست!</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مسلمانی از اهل دنیا مجو</p></div>
<div class="m2"><p>بآتش پرستان مسلمان مگو!</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه این آتش افتاده بر جان تو</p></div>
<div class="m2"><p>که افتاده بر دین و ایمان تو!</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خنک آنکه همت بترکش گماشت</p></div>
<div class="m2"><p>براین آتش از دور دستی بداشت!</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز دور ار چه رنگین و بس دلکش است</p></div>
<div class="m2"><p>به پیشش مرو، آتش است، آتش است!!</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دل چون بهشت توای بی خبر</p></div>
<div class="m2"><p>شده گور پر آتش از فکر زر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چه غم زینکه گورت پر آذر بود؟</p></div>
<div class="m2"><p>ترا کیسه باید که پر زر بود!</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چه غم گر ز درگاه دورت فگند؟</p></div>
<div class="m2"><p>ترا طاق درگاه باید بلند!</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چه غم دل اگر عاری از دین بود؟</p></div>
<div class="m2"><p>ترا جامه باید که رنگین بود!</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز رنگینی جامه ات، باد ننگ</p></div>
<div class="m2"><p>که طرار دنیات کرده است رنگ!</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>لباس زرت، گر چه بس دلکش است</p></div>
<div class="m2"><p>برون جامه زر، درون آتش است!</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تو گر تن به راحت برآورده یی</p></div>
<div class="m2"><p>به زربفت و دیباش پرورده یی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مهیاست بهرت، مکن دل غمین</p></div>
<div class="m2"><p>قباهای زر تاری آتشین!</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مپوش، ارچه دیبای جینت کند</p></div>
<div class="m2"><p>لباسی که عریان ز دینت کند!</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بود گر قبا رنگ رنگت هوس</p></div>
<div class="m2"><p>ز فقرت همین دلق صدرنگ بس!</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگر جامه گل گلت آرزوست</p></div>
<div class="m2"><p>ترا دلق صد پاره چون گل نکوست!</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مدان از قبای علم باف کم</p></div>
<div class="m2"><p>لباسی که باشد ز چاکش علم!</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نباشد بر اندامت ای بی خبر</p></div>
<div class="m2"><p>لباسی ز بخشش برازنده تر!</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خوش آنکس که دست کرم بر گشاد</p></div>
<div class="m2"><p>ز خالق گرفت و به مخلوق داد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دل شاد را مایه دادن است</p></div>
<div class="m2"><p>گشاد دل از کیسه بگشادن است</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دهش کی رساند به مالت ضرر؟</p></div>
<div class="m2"><p>نگردد کم آتش ز خرج شرر!</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو مردن، عدویی بدنبال تست</p></div>
<div class="m2"><p>پس، از مال، دادن همین مال تست!</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز دست آنچه آید، بسایل بده</p></div>
<div class="m2"><p>اگر زر نباشد ترا، دل بده</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>محبت بسایل چو زر دادن است</p></div>
<div class="m2"><p>گشاد جبین کیسه بگشادن است</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>اگر گرم رویی، شوی کامیاب</p></div>
<div class="m2"><p>شد از روی گرم، آفتاب، آفتاب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ترش رویی ای خواجه گر کار تست</p></div>
<div class="m2"><p>گره بر جبین نیست، بر کار تست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زر و مال مانده است از رفتگان</p></div>
<div class="m2"><p>چو آتش که میماند از کاروان!</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بهار آمد و، داغ دل تازه شد</p></div>
<div class="m2"><p>به غم تنگ صحرای اندازه شد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>جنونم پذیرای تدبیر نیست</p></div>
<div class="m2"><p>گریبانیم کار زنجیر نیست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>در این فصل کار جنون مشکل است</p></div>
<div class="m2"><p>که زنجیرم از عقده های دل است</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ندانم دل خسته درد کیست؟</p></div>
<div class="m2"><p>که شاخ گل از غنچه من گریست!</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دلم داشت چون شاخ گل ز انتظار</p></div>
<div class="m2"><p>زهر غنچه چشمی براه بهار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کنون وقت شد رشک عشرت شوم</p></div>
<div class="m2"><p>روم بلبل باغ جنت شوم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بدل وصف باغی اشارت شده است</p></div>
<div class="m2"><p>که قزوین از آن باب جنت شده است</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چه باغی که وصفش کنم چون بیان</p></div>
<div class="m2"><p>گل معنیم بشکفد از زبان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چه سان خامه وصف کمالش کند؟</p></div>
<div class="m2"><p>مگر مصرع از نو نهالش کند!</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چه سان در وصفش بسفتن رسد؟</p></div>
<div class="m2"><p>هوا از لطافت بگفتن رسد!</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>هوایش چو کلک آورد بر زبان</p></div>
<div class="m2"><p>مداد آبکی گردد از وصف آن</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>در او آن چنان دیده ام ژاله را</p></div>
<div class="m2"><p>که شوید سیاهی ز دل لاله را</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>هوایش به نوعی که هر دم سحاب</p></div>
<div class="m2"><p>بگردد به گرد سرش چون حباب</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ز بس تر بود ز ابر فیضش هوا</p></div>
<div class="m2"><p>بر او افگند کشتی از گل صبا</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چنان از رطوبت زمان و زمین</p></div>
<div class="m2"><p>کز آن رنگ گل گشته کشتی نشین</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>هوا بسکه سرسبز و شاداب ساخت</p></div>
<div class="m2"><p>نهالش ز فواره نتوان شناخت</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نباشد غباری در آن آب و گل</p></div>
<div class="m2"><p>به غیر از غباری که خیزد ز دل</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>غباری چو خیزد ز پیرامنش</p></div>
<div class="m2"><p>زند شبنمی مشت بر گردنش</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ز دورش تماشا کند تا غبار</p></div>
<div class="m2"><p>کند گرد بادش به گردن سوار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>هوا را چنان داده ابر آب و رنگ</p></div>
<div class="m2"><p>که بندد از آن تیغ خورشید، زنگ</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>کشیده بر او سایبان از سحاب</p></div>
<div class="m2"><p>بر او چشم حسرت بود آفتاب</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ز خاکش چنان میدمد بی غمی</p></div>
<div class="m2"><p>که برمی جهد سبزه از خرمی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ز رنگینی سبزه اش آشکار</p></div>
<div class="m2"><p>که برده است زنگ از دل روزگار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>نبیند کس اندوه، آنجا بخواب</p></div>
<div class="m2"><p>که غم را جواب است آواز آب</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ز گیرایی دست نظاره، چون</p></div>
<div class="m2"><p>ازو میرود نکهت گل برون</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چو بید موله از آن سبزه ها</p></div>
<div class="m2"><p>پشیمان شود هر که خیزد ز جا</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ز بوی گلش رفته شبنم ز هوش</p></div>
<div class="m2"><p>از آن میبرد آفتابش بدوش</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ز نرمی، چو بر سبزه اش غلتد آب</p></div>
<div class="m2"><p>درست آیدش ز آب بیرون حباب</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بود چشم کوثر از آن سوی او</p></div>
<div class="m2"><p>که چشمی دهد آب، از جوی او</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>به آیینه داده است آبش شکست</p></div>
<div class="m2"><p>ز بس برخود از موج صیقل زده است</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ندانم، زبس آب او باصفاست</p></div>
<div class="m2"><p>که چون، از لب جوی او سبزه خاست؟!</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>شکفته گل و لاله ز اندازه بیش</p></div>
<div class="m2"><p>همه بوسه بر کلک نقاش خویش</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>چو گلچین آن خوش گلستان شود</p></div>
<div class="m2"><p>نگه، چون رگ چشم مستان شود</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>از آن، سرو برچیده دامن در او</p></div>
<div class="m2"><p>که آتش نگیرد ز گلهای او</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>کند میل، نخلش بآن سرکشی</p></div>
<div class="m2"><p>که دامن زند بر گل آتشی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>زگل زنبقش بیش خندیده است</p></div>
<div class="m2"><p>بلی زعفران خورده روییده است</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ته دامن هر نهال تری</p></div>
<div class="m2"><p>زهر بیخ سوسن بود مجمری</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ز نیلوفرش چون عرب زادگان</p></div>
<div class="m2"><p>زده نیل بر خویش سرو روان</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>بود مد آواز هر بلبلی</p></div>
<div class="m2"><p>ز رنگینی نغمه، شاخ گلی</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بهر شاخ گل، عندلیبان باغ</p></div>
<div class="m2"><p>ز شوریدگی چون نمک در چراغ</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>هوا گر ز فردوس دم میزند</p></div>
<div class="m2"><p>گل از ژاله دندان بهم میزند</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>خیابان و هر سو نهالی بلند</p></div>
<div class="m2"><p>چنین مسطر نظم کم بسته اند</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>میان دو نخلش بود جویبار</p></div>
<div class="m2"><p>چو چین در میان دو ابروی یار</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>نگیرد سر از پای نخلش چو آب</p></div>
<div class="m2"><p>کشد تیغ بر سایه گر آفتاب</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>نیابد ز سر پنجه شاخسار</p></div>
<div class="m2"><p>رهایی گریبان فصل بهار</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>عجب از شتاب ترقی در او</p></div>
<div class="m2"><p>که از پی رسد شاخ گل را نمو</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>درخت چنارش، که طوبی وش است</p></div>
<div class="m2"><p>بر سایه اش همه حسرت کش است</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>بپا جوشیش هر زمان قد کشد</p></div>
<div class="m2"><p>ز آب بقا نخل عمر ابد</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>گمانم شد از هفت گردون برش</p></div>
<div class="m2"><p>که بر ساق پیچیده نیلوفرش</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>وفا کی کند عمر آب روان</p></div>
<div class="m2"><p>که خود را کشد بر سرشاخ آن؟!</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>زتلخی است دربانش از بس جدا</p></div>
<div class="m2"><p>نباشد ز بادام تلخش عصا</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>چرا سازم از فکر او سینه ریش</p></div>
<div class="m2"><p>زبانی است هر سر و در وصف خویش</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>ز خوبی همه چیز او جابجاست</p></div>
<div class="m2"><p>دریغا گل زندگی بی بقاست</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>در او بشکفد لاله و گل بسی</p></div>
<div class="m2"><p>که ما رابخاطر نیارد کسی</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>برآید بسی غنچه از پیرهن</p></div>
<div class="m2"><p>که ما را بود سر بجیب کفن</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>بسی در چمن گردد آب روان</p></div>
<div class="m2"><p>که از هستی ما نیابد نشان</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>رسد چون بآخر مه و سال ما</p></div>
<div class="m2"><p>زند خنده یی گل ز دنبال ما</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>اجل چون گل ما بتارک زند</p></div>
<div class="m2"><p>ز دنبال ما برگ، دستک زند</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>جهان باغبان است و ما چون نهال</p></div>
<div class="m2"><p>ز پروردنش بر خود ای دل مبال</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>مشو خرم ار خدمتت میکند</p></div>
<div class="m2"><p>که شاخ گل حسرتت میکند</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>در این باغ چون غنچه هرزه خند</p></div>
<div class="m2"><p>دل خویش را بر گشودن مبند</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>مبر خویش را بر فلک همچو تاک</p></div>
<div class="m2"><p>که فردا نهی روی بر روی خاک</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>ز شادی مزن دست بر هم چو برگ</p></div>
<div class="m2"><p>که فردا شوی دست فرسود مرگ</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>چو نرگس تماشای خود تا بچند؟</p></div>
<div class="m2"><p>ندانی چه در کاسه ات می کنند؟!</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>همه تن، رگ گردنی همچو تاک!</p></div>
<div class="m2"><p>ندانی که ریزند خونت بخاک؟!</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>فلک گر دوروزی مجالت دهد</p></div>
<div class="m2"><p>بسی در لحد خاک مالت دهد!</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>نظر سوی دنیا به حسرت مکن</p></div>
<div class="m2"><p>نگه را رگ خواب غفلت مکن</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>زدل برگشا دیده عبرتی</p></div>
<div class="m2"><p>بدل نیش زدن از رگ غیرتی</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>بیا ای دل از اثر بی خبر</p></div>
<div class="m2"><p>هم از غیرت خویش آسوده تر</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>که چون تاک با دیده خون چکان</p></div>
<div class="m2"><p>بسازیم برگ ره آن جهان</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>چو برگ حنا ترک خامی کنیم</p></div>
<div class="m2"><p>برون زین چمن شادکامی کنیم</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>در این گلشن از دیده اعتبار</p></div>
<div class="m2"><p>بگرییم برخود چو ابر بهار</p></div></div>