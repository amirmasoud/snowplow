---
title: >-
    شمارهٔ ۱۴ - تاریخ انجام سفینه یی
---
# شمارهٔ ۱۴ - تاریخ انجام سفینه یی

<div class="b" id="bn1"><div class="m1"><p>این طرفه «سفینه »یی که در وی</p></div>
<div class="m2"><p>کشتی کشتی قماش معنیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرند بکف، چو اهل فضلش</p></div>
<div class="m2"><p>چون کشتی نوح و، کوه جودیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر سوی ز اهل قال، بحثی</p></div>
<div class="m2"><p>هر گوشه ز اهل حال، بزمیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر صفحه، ز قوت روح، خوانی</p></div>
<div class="m2"><p>کزوی صد عمر میتوان زیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر سطر ز معنی روانبخش</p></div>
<div class="m2"><p>جویی، از آب زندگانیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الفاظ ز نکته های سیراب</p></div>
<div class="m2"><p>هر یک صدفی پر از لآلیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پوست، چو گل نگنجد از شوق</p></div>
<div class="m2"><p>پر بسکه زرنگ و معنیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود یک گل و، باغ و بوستانها</p></div>
<div class="m2"><p>در هر ورقش، ولیک مخفیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر صفحه، ز شوخی معانی</p></div>
<div class="m2"><p>چون پرده چشم مست لیلیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تاریخ ملوک ملک فضل است</p></div>
<div class="m2"><p>یا نسخه جمع و خرج گیتیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم تاریخ این سفینه:</p></div>
<div class="m2"><p>«هی هی چه سفینه؟ بحر معنی است »!</p></div></div>