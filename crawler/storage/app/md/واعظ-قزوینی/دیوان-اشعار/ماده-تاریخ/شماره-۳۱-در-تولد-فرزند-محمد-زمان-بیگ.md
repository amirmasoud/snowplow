---
title: >-
    شمارهٔ ۳۱ - در تولد فرزند محمد زمان بیگ
---
# شمارهٔ ۳۱ - در تولد فرزند محمد زمان بیگ

<div class="b" id="bn1"><div class="m1"><p>عطا کرد ایزد بمخدوم اکرم</p></div>
<div class="m2"><p>«محمد زمان بیگ » آن پاک گوهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ثمین گوهری از محیط کرامت</p></div>
<div class="m2"><p>چه گوهر؟ سپهر شرف راست اختر!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز صلب نجابت، برآمد شراری</p></div>
<div class="m2"><p>که از نور او شد چراغش منور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحمدالله از جویبار بزرگی</p></div>
<div class="m2"><p>برافراخت نیکو نهالی چنین سر!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو تاریخ جستم پی مولد او</p></div>
<div class="m2"><p>خرد گفت:«از وی الها خورد بر»!</p></div></div>