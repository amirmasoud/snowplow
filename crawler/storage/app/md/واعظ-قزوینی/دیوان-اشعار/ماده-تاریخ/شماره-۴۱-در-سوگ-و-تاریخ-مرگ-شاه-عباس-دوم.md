---
title: >-
    شمارهٔ ۴۱ - در سوگ و تاریخ مرگ شاه عباس دوم
---
# شمارهٔ ۴۱ - در سوگ و تاریخ مرگ شاه عباس دوم

<div class="b" id="bn1"><div class="m1"><p>در جهان ماتم و شوری دیدم</p></div>
<div class="m2"><p>همه گویان ز دل و جان صدحیف!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون صدف دست بهم سودی خلق</p></div>
<div class="m2"><p>ک از آن گوهر رخشان صدحیف!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوه و صحرا همه مینالیدند</p></div>
<div class="m2"><p>که از آن ابر بهاران صدحیف!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب در جوی سراسیمه زدی</p></div>
<div class="m2"><p>سنگ بر سینه که یاران صدحیف!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر طرف باد بسر خاک کنان</p></div>
<div class="m2"><p>که کجارفت سلیمان؟ صدحیف!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش سوخته میکرد خروش</p></div>
<div class="m2"><p>که از آن شمع شبستان صدحیف!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک را سر بقدمها که کجاست</p></div>
<div class="m2"><p>مرجع خاک نهادان؟ صدحیف!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مضطرب حال، تپیدی دریا</p></div>
<div class="m2"><p>که از آن ابر در افشان صدحیف!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خشک وامانده بجا آب گهر</p></div>
<div class="m2"><p>که از آن ریزش احسان صدحیف!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ابر، چون دود سیه میگردید</p></div>
<div class="m2"><p>که چه شد آن خور تابان؟ صدحیف!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شور در خیل غزالان، که دگر</p></div>
<div class="m2"><p>خاست زنجیر ز شیران صدحیف!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتم: این شور و فغان چیست؟ یکی</p></div>
<div class="m2"><p>گفت با دیده گریان: صدحیف!</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رفته «عباس شه ثانی » از این</p></div>
<div class="m2"><p>بی بقا عالم ویران صدحیف!</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نقد حیدر، شه دین و دنیا</p></div>
<div class="m2"><p>رفته از کیسه دوران صدحیف!</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غوطه در بحر فنا زد ذوالنون</p></div>
<div class="m2"><p>شد بچه یوسف کنعان صدحیف!</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شد بظلمات سکندر، صد آه</p></div>
<div class="m2"><p>رفت بر باد سلیمان صدحیف!</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رخش اقبال بریده دم و یال</p></div>
<div class="m2"><p>که از آن صاحب میدان صدحیف!</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در طویله همه لیلی منشان</p></div>
<div class="m2"><p>گشته مجنون و خروشان صدحیف!</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مد آواز قلم کوشان، این:</p></div>
<div class="m2"><p>که ز سرمشق سواران صدحیف!</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نیزه ها بر سخن نیزه وری</p></div>
<div class="m2"><p>گشت یک سر خط بطلان صدحیف!</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شد کمانهای زرافشان نالان</p></div>
<div class="m2"><p>که از آن دست زر افشان صدحیف!</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تیغها گشته سیه پوش از غم</p></div>
<div class="m2"><p>که از آن تیغ درخشان صدحیف!</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حلقه های زره افتاده بهم</p></div>
<div class="m2"><p>که ز سر حلقه مردان صد حیف</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سپر از درد بخود پیچیده</p></div>
<div class="m2"><p>کز پناه همه خلقان صدحیف!</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>موی واکرده علم از پرچم</p></div>
<div class="m2"><p>کز سرافرازی مردان صدحیف!</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر سر خویش زنان، کوس دودست</p></div>
<div class="m2"><p>که ز آوازه احسان صدحیف!</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دفتر از مد رقم آه کشان</p></div>
<div class="m2"><p>که شد احوال پریشان صدحیف!</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تخت از پایه خود افتاده</p></div>
<div class="m2"><p>که از آن لنگر و آن شان صدحیف!</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رفته بر خویش فرو نقش نگین</p></div>
<div class="m2"><p>که از آن صاحب فرمان صدحیف!</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سنگ بر سنه زنان انگشتر</p></div>
<div class="m2"><p>کز سلیمانی دیوان صدحیف!</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دیده تاج پر از آب گهر</p></div>
<div class="m2"><p>که ز سر کرده شاهان صدحیف!</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بهر این واقعه جستم تاریخ</p></div>
<div class="m2"><p>از زبان همه، گویان: صدحیف!</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زین دو مصراع دو تاریخ آمد</p></div>
<div class="m2"><p>حیف از آن قدر سخن دان صدحیف!</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>«آه از آن شاه فریدون شیم آه »!</p></div>
<div class="m2"><p>«حیف از آن سرور ایران صد حیف »!</p></div></div>