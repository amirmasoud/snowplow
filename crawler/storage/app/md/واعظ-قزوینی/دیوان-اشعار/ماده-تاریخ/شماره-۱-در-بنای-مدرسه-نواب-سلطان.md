---
title: >-
    شمارهٔ ۱ - در بنای مدرسه نواب سلطان
---
# شمارهٔ ۱ - در بنای مدرسه نواب سلطان

<div class="b" id="bn1"><div class="m1"><p>در زمان دولت خاقان عهد</p></div>
<div class="m2"><p>آن چراغ افروز شرع مصطفی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه دین عباس ثانی، آنکه هست</p></div>
<div class="m2"><p>رایت قانون دین، از وی بپا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از رخ آیینه ایام گشت</p></div>
<div class="m2"><p>صیقل شمشیر او، ظلمت زدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرده در گردون دوام دولتش</p></div>
<div class="m2"><p>پنجه خورشید را، دست دعا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد ز همت، بانی این مدرسه</p></div>
<div class="m2"><p>آن محیط دانش و، کان سخا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پشت دین،«نواب سلطان » آنکه هست</p></div>
<div class="m2"><p>بردرش اهل جهان را التجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهتمامش کرد از این محکم اساس</p></div>
<div class="m2"><p>کاخ دانش را، ز رفعت چرخ سا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر کسب فیض، توفیقم کشید</p></div>
<div class="m2"><p>دامن دل سوی آن عالی بنا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتمش تاریخ جویان کاین کجاست؟</p></div>
<div class="m2"><p>گفت، «درد جهان را دارالشفا»!</p></div></div>