---
title: >-
    شمارهٔ ۴۹ - تاریخ بنای مهتابی نواب خان
---
# شمارهٔ ۴۹ - تاریخ بنای مهتابی نواب خان

<div class="b" id="bn1"><div class="m1"><p>مه برج اقبال، «نواب خان »</p></div>
<div class="m2"><p>که بادا الهی جهانش بکام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آوازه عدل و احسان او</p></div>
<div class="m2"><p>بود گوش این نه صدف، پرمدام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بمیدان هستی، ز لطف حقش</p></div>
<div class="m2"><p>بود توسن نیله چرخ رام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنا کرد، مهتابیی دلنشین</p></div>
<div class="m2"><p>که روز از شب آن کند نور وام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس خوشدلی راست آنجا هجوم</p></div>
<div class="m2"><p>نیابد در آن راه، اندوه شام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آن فرش، دیگر تکلف بود</p></div>
<div class="m2"><p>که فرش است مهتاب آنجا مدام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از این فکر هر ماه کاهد قمر</p></div>
<div class="m2"><p>که چون بگذرد زین مبارک مقام؟!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بحدی است کیفیت آن، که هست</p></div>
<div class="m2"><p>دز آن یاد کیفیت می حرام!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آن شام دارد ز بس فیض صبح</p></div>
<div class="m2"><p>شود بر زبان شب بخیرم، سلام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهر سو شد اندیشه تاریخ جو</p></div>
<div class="m2"><p>چو شد آن خجسته نشین تمام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دعا کرد واعظ به اخلاص و گفت:</p></div>
<div class="m2"><p>«الهی نشیند در آن شادکام »</p></div></div>