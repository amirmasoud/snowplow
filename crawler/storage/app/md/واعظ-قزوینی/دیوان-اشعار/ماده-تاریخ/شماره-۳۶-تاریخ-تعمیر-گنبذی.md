---
title: >-
    شمارهٔ ۳۶ - تاریخ تعمیر گنبذی
---
# شمارهٔ ۳۶ - تاریخ تعمیر گنبذی

<div class="b" id="bn1"><div class="m1"><p>از فضل خدا، خدیو ایران</p></div>
<div class="m2"><p>شاهی که جهان ازوست معمور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلقیس زمانه را «سلیمان »</p></div>
<div class="m2"><p>باشند زهم، همیشه مسرور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر خوان عطای او نوالش</p></div>
<div class="m2"><p>در کاسه گرفت دست فغفور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفتند ز بیمش اهل آزار</p></div>
<div class="m2"><p>بر خویش فرو چو نیش زنبور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از یاری حق نمود تعمیر</p></div>
<div class="m2"><p>این گنبذ را بسعی موفور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گنبذ نه، که عالم روان راست</p></div>
<div class="m2"><p>هم چرخ و، هم آفتاب پرنور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از حرمت اوست زائران را</p></div>
<div class="m2"><p>طاعت مقبول و، جرم مغفور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این شکل صنوبری، جهان را</p></div>
<div class="m2"><p>در سینه بود دلی پر از نور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صاحب نظری که، دیده دل</p></div>
<div class="m2"><p>افگند براین مقام پرنور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برداشت چو «دیده »، گفت تاریخ:</p></div>
<div class="m2"><p>«کعبه گردیده بیت معمور»</p></div></div>