---
title: >-
    شمارهٔ ۳۴ - در سوگ و تاریخ مرگ میرباقر
---
# شمارهٔ ۳۴ - در سوگ و تاریخ مرگ میرباقر

<div class="b" id="bn1"><div class="m1"><p>چراغ دلم «میرباقر» که داشت</p></div>
<div class="m2"><p>ز سیماش نور سیادت ظهور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به عشرت‌سرای بقا بار بست</p></div>
<div class="m2"><p>از این محنت‌آباد دار غرور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد از رفتن او، ز جان‌ها قرار</p></div>
<div class="m2"><p>شد از غیبت او ز دل‌ها حضور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز زهر غمش، عالمی تلخ‌کام</p></div>
<div class="m2"><p>ز درد فراقش، جهانی به شور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به محنت‌سرایی که اینش بقاست</p></div>
<div class="m2"><p>چرا دل نهد مرد صاحب شعور؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بس شادیش با غم آمیخته است</p></div>
<div class="m2"><p>نداند کسی ماتمش را ز سور!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو تیر از کمانخانه چرخ پیر</p></div>
<div class="m2"><p>بود راستان را به سرعت عبور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان را خدنگی به ترکش نماند</p></div>
<div class="m2"><p>کمان فلک را، همانست زور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو تاریخ فوتش، من دردمند</p></div>
<div class="m2"><p>طلب کردم، از خاطر ناصبور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خاک از دعا روی اخلاص سود</p></div>
<div class="m2"><p>بگفتا که:«قبرش بود پر ز نور»!</p></div></div>