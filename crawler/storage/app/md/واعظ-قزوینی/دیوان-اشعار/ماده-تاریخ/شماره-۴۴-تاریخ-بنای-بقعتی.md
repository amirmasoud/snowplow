---
title: >-
    شمارهٔ ۴۴ - تاریخ بنای بقعتی
---
# شمارهٔ ۴۴ - تاریخ بنای بقعتی

<div class="b" id="bn1"><div class="m1"><p>در نکو عهد سلیمان زمان</p></div>
<div class="m2"><p>خسروی کاو کرده ملک از ظلم، پاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در زمان او ندارد ذره یی</p></div>
<div class="m2"><p>کبک از شهباز و میش از گرگ، باک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیست نبود بهر عمر و دولتش</p></div>
<div class="m2"><p>جمله تن، دست دعا مانند تاک؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنده اش نواب «زینل خان »، که هست</p></div>
<div class="m2"><p>خصم شاه از دست تیغش سینه چاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خان عالیشان، که پیش همتش</p></div>
<div class="m2"><p>کوه ها یکسر نماید چون مغاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد این دلکش عمارت را بنا</p></div>
<div class="m2"><p>بهر کسب فیض از این ارواح پاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست از بس فیضناک این خوش مکان</p></div>
<div class="m2"><p>گشت تاریخش «مکانی فیضناک »</p></div></div>