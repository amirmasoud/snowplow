---
title: >-
    شمارهٔ ۵۹ - تاریخ احداث باغ خرم آباد علی بیگ »
---
# شمارهٔ ۵۹ - تاریخ احداث باغ خرم آباد علی بیگ »

<div class="b" id="bn1"><div class="m1"><p>فروزان کوکب برج سعادت</p></div>
<div class="m2"><p>علی بیگ، آن جهان عز و تمکین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوانمردی که، در گلزار جودش</p></div>
<div class="m2"><p>نبیند خار منت، دست گلچین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز روی گرم او چون مهر تابان</p></div>
<div class="m2"><p>بود گلزار حسن خلق رنگین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیفتد یارب از باد ملالی</p></div>
<div class="m2"><p>چو آب گوهر، او را بر جبین چین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهمت خرم آبادی بنا کرد</p></div>
<div class="m2"><p>کز آن گردید خرم ملک قزوین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان کزوی شد این ویرانه معمور</p></div>
<div class="m2"><p>از آن معمور بادش کشور دین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه تنها همتش، تعمیر گل کرد</p></div>
<div class="m2"><p>که هم تعمیر دلها هستش آیین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آن دانا نهد چون پای گوید</p></div>
<div class="m2"><p>به تاریخش که:«حقا جنت است این »</p></div></div>