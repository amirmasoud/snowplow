---
title: >-
    شمارهٔ ۲۶ - در تاریخ بنای کاروانسرایی
---
# شمارهٔ ۲۶ - در تاریخ بنای کاروانسرایی

<div class="b" id="bn1"><div class="m1"><p>در زمان دولت «شاه سلیمان » احتشام</p></div>
<div class="m2"><p>آنکه از معماری عدلش، جهان آباد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهره ظالم، ز بیم آن شه با عدل و داد</p></div>
<div class="m2"><p>آب شد، چندانکه سیل خانه بیداد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خزان رنگ زردی در ریاض عهد او</p></div>
<div class="m2"><p>راستی هرکس چو سرو آورد پیش، آزاد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خسروی، کز زور عدلش بیستون ظلم را</p></div>
<div class="m2"><p>آه مظلومی تواند تیشه فرهاد شد!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برق از کشتی خورد گردانه یی در عهد او</p></div>
<div class="m2"><p>از خجالت خواهد آتش آب در فولاد شد!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رو، در آبادی جهان را چون زیمن عدل اوست</p></div>
<div class="m2"><p>این بنای خیر از توفیق حق بنیاد شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چیست این دلکش بنا؟ مهمانسرایی بهر خلق</p></div>
<div class="m2"><p>کز ورودش رنج راحت گشت و، غمگین شاد شد!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این سرا، از بس که باشد دلنشین، نبود عجب</p></div>
<div class="m2"><p>گر غریبان را در این منزل وطن ازیاد شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نی سرا، کز راه تحصیل دعای مغفرت</p></div>
<div class="m2"><p>میتواند با نیش را ارشد اولاد شد!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با نیش ز آن گنج ها اندوزد از نقد ثواب</p></div>
<div class="m2"><p>مستغلی این چنین کم در جهان بنیاد شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یافت چون اتمام،واعظ کرد تاریخش رقم:</p></div>
<div class="m2"><p>«آن سرا نیز از بنای این سرا آباد شد»</p></div></div>