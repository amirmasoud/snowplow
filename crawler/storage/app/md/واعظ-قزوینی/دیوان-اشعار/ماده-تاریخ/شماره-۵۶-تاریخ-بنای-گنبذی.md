---
title: >-
    شمارهٔ ۵۶ - تاریخ بنای گنبذی
---
# شمارهٔ ۵۶ - تاریخ بنای گنبذی

<div class="b" id="bn1"><div class="m1"><p>در عهد شاه دادگر، زیبنده تاج و کمر</p></div>
<div class="m2"><p>اسکندر جمشید فر، یعنی «سلیمان » زمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهنشه گردون خیم، لشکر کش انجم حشم</p></div>
<div class="m2"><p>دریا دل احسان شیم آن مایه امن و امان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرمانروای ماء وطین، ظلمت زدای کفر و دین</p></div>
<div class="m2"><p>رونق فزای شرع و دین،پشت و پناه شیعیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دولت قلم، عقلش بنان، دوران فرس، ضبطش عنان</p></div>
<div class="m2"><p>کشور بدن، حکمش روان، مردم رمه، عدلش شبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد ناگه از حکم قضا، در مشهد طوس رضا</p></div>
<div class="m2"><p>زآن سان زمین لرزی که شد گیتی شکسته دل از آن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل چیست؟ والاقبه پر نور شاهنشاه دین!</p></div>
<div class="m2"><p>نور دو چشم مهر و مه، آن مقتدای انس و جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن کو پی کسب شرف، آیند هر دم جان بکف</p></div>
<div class="m2"><p>سایند رخ از هر طرف بر در گه او قدسیان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دیده اهل نظر، دارد بسی نسبت اگر</p></div>
<div class="m2"><p>سایند رخ از هر طرف بر درگه او قدسیان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دیده اهل نظر، دارد بسی نسبت اگر</p></div>
<div class="m2"><p>از نسبت این خاک در، نازد زمین بر آسمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از خاک او هر ذره ای، به از هزاران توتیا</p></div>
<div class="m2"><p>در طوف هر لحظه ای، خوشتر ز عمر جاودان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باشد سعادت گر چن، آبش بود این خاک در</p></div>
<div class="m2"><p>گردد جهان گر انجمن، صدرش بود این آستان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>معروض شد چون این خبر، بر رای شاه دادگر</p></div>
<div class="m2"><p>معمار اخلاصش، کمر بست از پی تعمیر آن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گردید از فضل خدا، چون حق این خدمت ادا</p></div>
<div class="m2"><p>آمد دل گیتی بجا، گردید از آن روشن جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این گنبذ خورشید ظل، تجدید چون شد گفت دل:</p></div>
<div class="m2"><p>چرخ کهن گردید نو، پیر فلک گشته جوان!</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از بهر تاریخش قلم، کرد این دو مصرع را رقم</p></div>
<div class="m2"><p>اول پی افتادن و، ثانی پی تعمیر آن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>«افتاد گر از زلزله، این کعبه اهل زمن »</p></div>
<div class="m2"><p>«شد باز آن معمور از حکم «سلیمان » زمان »</p></div></div>