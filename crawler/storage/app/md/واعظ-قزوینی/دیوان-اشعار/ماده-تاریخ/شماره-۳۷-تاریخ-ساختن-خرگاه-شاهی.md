---
title: >-
    شمارهٔ ۳۷ - تاریخ ساختن خرگاه شاهی
---
# شمارهٔ ۳۷ - تاریخ ساختن خرگاه شاهی

<div class="b" id="bn1"><div class="m1"><p>بفرمان «عباس ثانی » شهی</p></div>
<div class="m2"><p>که باشد درش، قبله گاه سپهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز لشکر کشان در رکابش، یکی</p></div>
<div class="m2"><p>شهنشاه انجم سپاه سپهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز توفیق حق خیمه یی شد تمام</p></div>
<div class="m2"><p>که شد ثانی بارگاه سپهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو چشم است حیران نظاره اش</p></div>
<div class="m2"><p>دو آیینه مهر و ماه سپهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به پیش شکوهش، ز خجلت شده است</p></div>
<div class="m2"><p>بلندی نهان در پناه سپهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکوهش نمی آورد، سر فرو</p></div>
<div class="m2"><p>بهمدوشی بارگاه سپهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه مهر است کز دیدن رفعتش</p></div>
<div class="m2"><p>فتد هرشب از سرکلاه سپهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خرد گفت از بهر تاریخ آن</p></div>
<div class="m2"><p>بگو:«ثانی بارگاه سپهر»!</p></div></div>