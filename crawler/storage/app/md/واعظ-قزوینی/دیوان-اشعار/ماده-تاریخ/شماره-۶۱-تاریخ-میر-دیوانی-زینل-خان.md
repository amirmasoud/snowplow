---
title: >-
    شمارهٔ ۶۱ - تاریخ میر دیوانی زینل خان
---
# شمارهٔ ۶۱ - تاریخ میر دیوانی زینل خان

<div class="b" id="bn1"><div class="m1"><p>شاهنشه دین پرور، دارای سلیمان فر</p></div>
<div class="m2"><p>آنکو بودش بر در، پیوسته شهان را رو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردیده ز بس کوته، دست ستم از بیمش</p></div>
<div class="m2"><p>بر خویش صبا لرزد، از گل چو ستاند بو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جودش ز جهان از بس، آیین طلب افگند</p></div>
<div class="m2"><p>بر گوش غریب آید، از فاخته هم کوکو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بس که خوش است از وی، هر شش جهت عالم</p></div>
<div class="m2"><p>آیینه از این داغست، از بهر چه شد یکرو؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرمود به «زینل خان »، چون میسری دیوان را</p></div>
<div class="m2"><p>کردند دعا شه را، خلق از دل و جان هر سو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکو که ز اندیشه، نیک وبد عالم را</p></div>
<div class="m2"><p>با دیده دل بیند، در آینه زانو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از لطف شهنشاه است، بر خلق روان حکمش</p></div>
<div class="m2"><p>از قوت سرچشمه است، غلتانی آب از جو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون یار شد این دولت، با دولت دیرینش</p></div>
<div class="m2"><p>هم یار شود یارب، توفیق خدا با او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از فکر معمایی، تاریخ طلب بودم</p></div>
<div class="m2"><p>ناگه پی تاریخش، آورد «دو دولت رو»</p></div></div>