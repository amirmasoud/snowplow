---
title: >-
    شمارهٔ ۱۴ - در فضیلت ماه صیام و مدحت چراغ دیده عباد حضرت سجاد«ع »
---
# شمارهٔ ۱۴ - در فضیلت ماه صیام و مدحت چراغ دیده عباد حضرت سجاد«ع »

<div class="b" id="bn1"><div class="m1"><p>ز شوق اهل نظر میدوند بر در و بام</p></div>
<div class="m2"><p>زکوة حسن ستانند تا ز ماه صیام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نور دیده در و بامها چراغان است</p></div>
<div class="m2"><p>برای مقدم این ماه آفتاب غلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مه مبارک رخسار میمنت مقدم</p></div>
<div class="m2"><p>مه سعادت آغاز مغفرت انجام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه ماه؟ ماه جبینی بلند بالایی!</p></div>
<div class="m2"><p>چه ماه؟ هوش ربا دلبری ز خاص و عام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه ماه! آنکه برد حسن حیرت افزایش</p></div>
<div class="m2"><p>ز کار، دست و دل خلق روزگار تمام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بزیر زیور خوبی، ز بس گرانبار است</p></div>
<div class="m2"><p>همیشه زین سبب او را بلنگر است خرام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زند بچرخ شکرخنده گاه از گل صبح</p></div>
<div class="m2"><p>کشد بخاک گه از ناز حسن، طره شام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برویش، از شب قدر است عنبرین خالی</p></div>
<div class="m2"><p>که باشد از دل و جانش هزار ماه، غلام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از این جهت شب قدر است نام او که بود</p></div>
<div class="m2"><p>چو روز و روشن از او قدر اهل بیت کرام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از اینکه گشته شب قدر او نهان، پیداست</p></div>
<div class="m2"><p>که قدر او نشناسد کس از خواص و عوام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان عزیز، که خلق از رعایت ادبش</p></div>
<div class="m2"><p>کنند خنده چو گل بی صدا در این ایام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زهی مبارک ماهی، که بسته است کمر</p></div>
<div class="m2"><p>پی نجات گنه پیشگان ز خاص و ز عام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز دیر رفتن ایام او مباش ملول</p></div>
<div class="m2"><p>ستاده تا ک ستاند برات خلق، تمام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بخاک پای جوانمردیش، ز ما صد بوس</p></div>
<div class="m2"><p>بطاق ابروی مردانه اش هزار سلام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همین برای امید سیاه رویان بس</p></div>
<div class="m2"><p>که نور تابدش از ماه نو ز جبهه شام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدست نامه آزادی سیاه و سفید</p></div>
<div class="m2"><p>رسند از پی هم این لیالی و ایام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز تازه رویی، هر روز اوست نوروزی</p></div>
<div class="m2"><p>ز دلگشایی، هر شام اوست عید صیام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کسی ز صبحش اگر چین جبهه یی بیند</p></div>
<div class="m2"><p>بعذر خواهیش آید شکفته رویی شام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز حق چه مژده رساند این مه مبارک رو</p></div>
<div class="m2"><p>که شد بخلق از آن شوق، اکل و شرب حرام؟!</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شود سیاه از آن رفته رفته چشمه بدر</p></div>
<div class="m2"><p>که شسته اند درآن نامه ها ز جرم تمام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بود بمشرب لب تشنگان چشمه فیض</p></div>
<div class="m2"><p>چو طول عمر، خوش آینده طول این ایام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شراب رحمت حق تا بخلق پیماید</p></div>
<div class="m2"><p>گرفته است بکف از هلال، زرین جام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عجب خجسته هلالی، که با کمال شرف</p></div>
<div class="m2"><p>دو تا نموده قد و، میکند بخلق سلام!</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هلال نیست، که باشد قلاده سگ نفس</p></div>
<div class="m2"><p>که پای تا بسر از حرص لقمه یی شده کام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز حلقه مه نو، رایض حکومت شرع</p></div>
<div class="m2"><p>زده است بر دهن نفس سرکش تو لگام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هلال نیست که از آمد آمد نوروز</p></div>
<div class="m2"><p>کلاه گوشه بسر بر شکسته ماه صیام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هلال نیست، بود خیل روزه داران را</p></div>
<div class="m2"><p>برای صید غزالان فیض، حلقه دام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نه ماه نو، که ز تأثیر باد نوروزی</p></div>
<div class="m2"><p>گلیست از چمن فیض، ناشکفته تمام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بمشک جرم مه و، زعفران پرتو مهر</p></div>
<div class="m2"><p>قضا بکاسه گردون نوشته هفت سلام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نموده خامه قدرت چه بوالعجب نقشی</p></div>
<div class="m2"><p>که گاه ابرو و، گه چشم و، گاه روست تمام!</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هلال نیست، که از تشنگی برحمت حق</p></div>
<div class="m2"><p>برون فتاده مه روزه را زبان را کام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>باین زبان، کند این ماه روزه داران را</p></div>
<div class="m2"><p>ز حق بنعمت الوان مغفرت اطعام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بود ز حلقه این مه اشارتی از غیب</p></div>
<div class="m2"><p>که حلقه گشته خط سیئات خلق تمام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بود باین همه قدر و کمال، این مه نو</p></div>
<div class="m2"><p>غلام حلقه بگوشی که شد هلالش نام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>غلام حلقه بگوش که؟ برگزیده حق</p></div>
<div class="m2"><p>که بود سال درازش تمام ماه صیام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چراغ دیده عباد، حضرت سجاد</p></div>
<div class="m2"><p>که آفتاب چو مه نور از او نماید وام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زیاد تشنگی روزه اش چنان شود آب</p></div>
<div class="m2"><p>که از عقیق نماند بجای، غیر از نام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فغان سپاه غمش را بروز و شب چاووش</p></div>
<div class="m2"><p>سرشک بر وصف مژگان او همیشه امام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز ذکر واقعه کربلا نیاسودی</p></div>
<div class="m2"><p>دلش که مقری تسبیح ناله بود مدام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز تند باد جلال خدا تن زارش</p></div>
<div class="m2"><p>چو موج قلزم رحمت همیشه بی آرام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز شب دو چشم ترش خواب آن قدر نگرفت</p></div>
<div class="m2"><p>که از بنفشه تواند گرفت بو، بادام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رفیق، اشک روان گاه رفتنش بسجود</p></div>
<div class="m2"><p>عصایش، از علم دود آه وقت قیام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو خوشه، زرد، ولی دانه کش ضعیفان را</p></div>
<div class="m2"><p>چو گل، شکسته، و لیکن شکفته روی، مدام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>غریب، لیک وطن سایه اش غریبان را</p></div>
<div class="m2"><p>یتیم، لیک پدر ز التفات بر ایتام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به پیش سفره نگسترده هرگز او را روز</p></div>
<div class="m2"><p>فراش خواب نیفگنده هرگز او را شام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز حلم کرده بهم یار، جرم و بخشش را</p></div>
<div class="m2"><p>بعلم داده جدایی، حلال را ز حرام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بنرمیی سوی خصم درشت می آمد</p></div>
<div class="m2"><p>برون ز دیده نگاهش، که روغن از بادام</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو چشم خویش، کف او مدام در ریزش</p></div>
<div class="m2"><p>چو بحر همت خود، دل همیشه بی آرام</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گمان شدی که مگر سایل اوست وقت کرم</p></div>
<div class="m2"><p>زبس که همت او داشت در عطا ابرام</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>فگنده بار علایق ز خویش، تاکه کشد</p></div>
<div class="m2"><p>بخانه فقرا، آب و نان بدوش مدام</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همیشه کار یتیمان خسته را، چو گهر</p></div>
<div class="m2"><p>برشته نگه التفات، داده نظام</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز بس خضوع، شدی آب در نماز؛اگر</p></div>
<div class="m2"><p>برای سجده نبودی ضرور هفت اندام</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز حرف جبهه پر سجده اش، عجب نبود</p></div>
<div class="m2"><p>زبان اگر فگند چند پوست چون بادام</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از اینکه جبهه اش افگنده چند پوست چو گل</p></div>
<div class="m2"><p>چمن چگونه نبالد بخود ز فخر مدام؟!</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>میان خیل کمین بندگان درگه او</p></div>
<div class="m2"><p>کشیده گردن خود، سرو هم باین اندام</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بپاس حرمت تقوای او، ز خجلت خویش</p></div>
<div class="m2"><p>باستخوان قفا در خزیده مغز حرام</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز دهشت نگه خشم او، بر اهل ستم</p></div>
<div class="m2"><p>زبان نگشته ترازوی عدل را به سلام</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اگر بنامه روز شمه یی ز سطوت او</p></div>
<div class="m2"><p>رقم ز کاغذ خیزد چو موی براندام</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شوند بلبل گلزار مدحش، ار نفسی</p></div>
<div class="m2"><p>فرو بحرف دگر سر نیاورند اقلام</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>عدوی او که دماغش ز دود جهل پر است</p></div>
<div class="m2"><p>بحشر کی رسدش بوی مغفرت بمشام</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز مدح او ادبم منع میکند، چکنم؟</p></div>
<div class="m2"><p>مگر بدشمن او بعد ازین دهم دشنام!</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بروی دشمن او میجهم از آن هردم</p></div>
<div class="m2"><p>که در شمار سگان درش برندم نام!</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سخن رسید بسر منزل دعا واعظ</p></div>
<div class="m2"><p>عنان بکش که نه این راه را بود انجام</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دگر مخوان حدی ای ساربان، ک از مستی</p></div>
<div class="m2"><p>گسست ناقه مضمون ز مد خامه زمام</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>قلم مناز ببازوی خود، که ممکن نیست</p></div>
<div class="m2"><p>رسد بپایه قدرش کمند طول کلام</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>غرض از این همه، اظهار بندگی است مرا</p></div>
<div class="m2"><p>وگرنه من کیم آن قدر کوو، مدح کدام؟!</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سزای درگه او نیست تحفه یی در کف</p></div>
<div class="m2"><p>مرا بغیر دعا، والسلام و الاکرام</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بود بجیب مکان، تا چو مهر صفحه خاک</p></div>
<div class="m2"><p>بود بدست زمان، تا چو سبحه شهر صیام</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>برد بخاک درش سجده، جبهه عالم</p></div>
<div class="m2"><p>کند بذکر خوشش دور، سبحه ایام</p></div></div>