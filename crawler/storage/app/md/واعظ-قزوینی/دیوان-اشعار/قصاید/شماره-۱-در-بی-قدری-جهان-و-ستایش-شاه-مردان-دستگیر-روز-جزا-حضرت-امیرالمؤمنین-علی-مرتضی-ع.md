---
title: >-
    شمارهٔ ۱ - در بی قدری جهان و ستایش شاه مردان، دستگیر روز جزا، حضرت امیرالمؤمنین علی مرتضی «ع »
---
# شمارهٔ ۱ - در بی قدری جهان و ستایش شاه مردان، دستگیر روز جزا، حضرت امیرالمؤمنین علی مرتضی «ع »

<div class="b" id="bn1"><div class="m1"><p>چیست ای دل عالم هستی؟ بیابان فنا!</p></div>
<div class="m2"><p>هر طرف موج سرابی از گذار عمرها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر گیاه سبز دروی، تیغ زهر آلوده یی</p></div>
<div class="m2"><p>هر سر خاری درو، دلدوز تیری جان ربا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قطره های اشک حسرت، شبنم برگ گلش</p></div>
<div class="m2"><p>تند باد آه نومیدیش، باد جان فزا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر قدم گردیده پهن، از نقش پایی کژدمی</p></div>
<div class="m2"><p>هر طرف خوابیده از فرسنگها صد اژدها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر رگ سنگی درو، خاری بپای رهروی</p></div>
<div class="m2"><p>هر تف ریگی شرار خرمن صد مدعا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کف خاکی، نشان دستگاه خسروی</p></div>
<div class="m2"><p>هر سر برگی زبان حال چندین بینوا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر نسیمی، سطری از حال دل چندین اسیر</p></div>
<div class="m2"><p>هر غباری، خط تاریخ هزاران بیوفا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوی خون می آید از رنگینی گلزار او!</p></div>
<div class="m2"><p>نشنوی ای پر هوس گویا ز کامی از هوی؟!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از هوس تاکی جوانی میکنی؟ پیری رسید!</p></div>
<div class="m2"><p>بهر عبرت، دیده یی از خواب غفلت برگشا!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در جوانی چه ندانستی زره، اکنون بدان</p></div>
<div class="m2"><p>قامتت را کرده خم پیری که، بینی پیش پا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گور در پیش و،تو دل واپس برای ملک و مال</p></div>
<div class="m2"><p>چاه در راه و، تو از غفلت روی رو بر قفا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>راحت ار جویی، به ملک و مال و دنیا پشت ده</p></div>
<div class="m2"><p>خواب اگر خواهی، مساز از بالش زر متکا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رخ ز درویشی نتابد هر که دارد سوز عشق</p></div>
<div class="m2"><p>شعله چون پهلو تهی سازد ز فرش بوریا؟!</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خود نمایی که ز صرصر نیست در باغ وجود</p></div>
<div class="m2"><p>گردباد عمر باشد لاله را نشو و نما</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نشکنی تا خویشتن را، لاف قرب حق مزن</p></div>
<div class="m2"><p>بی شکستن جای در مسجد ندارد بوریا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این قدر زین عاریت بر خود سپردن بهر چیست؟</p></div>
<div class="m2"><p>این قدر زین بیوفا بر خود فرو چیدن چرا؟!</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیست جای خودنمایی، تنگنای روزگار؛</p></div>
<div class="m2"><p>گل درین بستان، ز افشردن کند نشو و نما!</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آنکه دوری نیست تنگی، عالم روشندلیست</p></div>
<div class="m2"><p>تنگ نبود عکس را در خانه آیینه جا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خنده ما ناتوانان، زیر این گردان سپهر</p></div>
<div class="m2"><p>خنده گندم بود، در زیر سنگ آسیا!</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>می توان زلف وصال شاهد عقبی گرفت</p></div>
<div class="m2"><p>دست همت گر ز دامان جهان گردد رها</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در ره دنیا ز یاران کن تلاش واپسی</p></div>
<div class="m2"><p>کآید از پیشی گرفتن، سنگ بر پای عصا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نیست با سیم و زر بی اعتبار این جهان</p></div>
<div class="m2"><p>خوبیی، جز اینکه با آن میتوان کردن سخا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خرج کن چون سکه نقد خویش در بازار وجود</p></div>
<div class="m2"><p>ای که داری در میان زر، چون نقش سکه جا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شیوه بخشش بدست آور، که گردی ارجمند</p></div>
<div class="m2"><p>تکیه بر دست شهان، از رنگ دادن زد حنا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ذکر و فکر منعمان، در بندگی دانی که چیست؟</p></div>
<div class="m2"><p>ذکر هر بیچاره و، فکر معاش هر گدا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اغنیا را نیست ذکری، به زیاد اهل فقر</p></div>
<div class="m2"><p>زر شمردن، سبحه باشد در کف اهل سخا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با گدا، کوچک دلی، مانند حج اکبر است</p></div>
<div class="m2"><p>هست لبیکش اجابت کردن هر بینوا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تیغ جوهر دارد باشد در جهاد نفس شوم</p></div>
<div class="m2"><p>دست پر گوهر که افشانی بدامان گدا!</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز التفات نامردان، اهل دولت را چه نقص؟</p></div>
<div class="m2"><p>سایه افگندن چه کم می سازد از بال هما؟!</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رنج یار ناتوان، باید توانا را کشید</p></div>
<div class="m2"><p>دست بهر چشم نابینا کشد بار عصا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آنکه بهر دیگران، بر خود نپیچد روز و شب</p></div>
<div class="m2"><p>باشد از سنگین دلی، کمتر ز سنگ آسیا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آنکه باشد دامن جودش بدست اهل فقر</p></div>
<div class="m2"><p>دستگیرش دامن «حیدر» شود روز جزا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آن جهانبخشی که هرگز چون نفس، گاه سخا</p></div>
<div class="m2"><p>در کفش پیوند دادنها نشد از هم جدا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همتش دریای بی پایان و، دستش ابر جود</p></div>
<div class="m2"><p>هر سه انگشتش رگ ابری ز باران عطا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خطبه را از وی مسلم، سربلندی در جهان</p></div>
<div class="m2"><p>سکه را بر خویش بالیدن ز نام او بجا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در صف هیجا، دم تیغش، دم نزع عدو</p></div>
<div class="m2"><p>کندن شمشیر او، جان کندن خصم دغا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حمله جرأت گدازش، کشت هستی را سموم</p></div>
<div class="m2"><p>برق شمشیر اجل خویش، سحرگاه جزا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون طناب سحر، در سیماب لرزش، عمر خصم</p></div>
<div class="m2"><p>ذوالفقار او به کف، چون در کف موسی عصا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از نگاه آرزوها، خاطر او بسته چشم</p></div>
<div class="m2"><p>از نکاح شاهد دنیا، ضمیرش پارسا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>عشوه دنیا ز دستش نقد دل بیرون نبرد</p></div>
<div class="m2"><p>پیر زالی چون بتابد، پنجه شیر خدا؟!</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همچنان کز تار مژگان بگذرد نور نگاه</p></div>
<div class="m2"><p>صد قدم در پیش بود از جاده در راه هدی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رفت از آن ساعت بخود نقش نگین از غم فرو</p></div>
<div class="m2"><p>کز کفش انگشتر از بهر تصدق شد جدا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نامه شستم، چون بآب گوهر مدحش ز جرم</p></div>
<div class="m2"><p>میکنم در حضرت او، عرض حال خود ادا</p></div></div>