---
title: >-
    شمارهٔ ۱۶ - در توصیف خزان و ستایش سرور دنیا و دین و فخر آسمان و زمین حضرت امام محمد باقر «ع »
---
# شمارهٔ ۱۶ - در توصیف خزان و ستایش سرور دنیا و دین و فخر آسمان و زمین حضرت امام محمد باقر «ع »

<div class="b" id="bn1"><div class="m1"><p>باز الوان پوش شد، پیرانه سر باغ جهان</p></div>
<div class="m2"><p>میکند گلزار پرافشانی، از برگ خزان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یافت از فیض هوا هر نخل جان تازه یی</p></div>
<div class="m2"><p>آمد آب رفته عشرت بجوی گلستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خنده گر خیزد بجای اشکم از دل، دور نیست</p></div>
<div class="m2"><p>کز پیاز امروز دیگر میکند گل زعفران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر صید دل، ز هر برگ خزان بر شاخسار</p></div>
<div class="m2"><p>زد دگر دستی بترکش شاهد پیر جهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب و رنگی کز چمن می بینم اکنون، دور نیست</p></div>
<div class="m2"><p>گر بگویم نیست فصل گل، مگر فصل خزان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کمال و نقص، دوری دارد این فصل از بهار</p></div>
<div class="m2"><p>آن قدر دوری که دارد پیر کامل از جوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرد گیتی، ز اقتضای، فصل از شبنم عرق</p></div>
<div class="m2"><p>یافت صحت از تب گرمای تابستان جهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا شود هم وزن، چون گرمی و سردی روز و شب</p></div>
<div class="m2"><p>کفه میزان نمود از قرص خورشید آسمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از حرارت تا رود در چشمه کافور دی</p></div>
<div class="m2"><p>از لباس برگ، عریان میشود ز آن گلستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسکه گردیده است گلشن، بی نیاز از آب و رنگ</p></div>
<div class="m2"><p>دور نبود از چمن، گر پا کشد آب روان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از برای رفتن ایام جانسوز تموز</p></div>
<div class="m2"><p>کرده، از بس خرمی، گلشن چراغان از خزان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گشته از صد برگ،روشن هر طرف مهتابیی</p></div>
<div class="m2"><p>رفته چون موشک ز هر سو تاکها بر آسمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر چنار از تندی باد خریفی در چمن</p></div>
<div class="m2"><p>چون گل غران، خروشان گشته و آتش فشان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از پی سیر چراغان خزان، نبود عجب</p></div>
<div class="m2"><p>پیر دی از خانه گر بیرون دود چون کودکان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رایض فصل خزان، از بهر استقبال دی</p></div>
<div class="m2"><p>کرده نیلی خنگ سبزی را، ز تاک آتش عنان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از خروش زاغ، کآمد آمد فصل دی است</p></div>
<div class="m2"><p>سوزد آتش بر سریر شاخ از برگ خزان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در بساط برگها، شد بسکه گلریزان رنگ</p></div>
<div class="m2"><p>گشته مرغان را صفیر انگشت حیرت در دهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در چمن از بس فضاها از گل رنگ است پر</p></div>
<div class="m2"><p>عندلیبان را نمیگردد بحرف گل زبان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شد مجسم، بسکه از عکس خزان چون شاخ گل</p></div>
<div class="m2"><p>میتواند مرغ بر موج هوا بست آشیان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در گلستان کرده اند از بسکه رنگ و بو هجوم</p></div>
<div class="m2"><p>خنده از تنگی خزیده در مزاج زعفران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بسکه رنگین است از عکس خزان هر سو هوا</p></div>
<div class="m2"><p>باد چون دامان پر گل میرود از بوستان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گشته بر رنگینی بالا بلندان چمن</p></div>
<div class="m2"><p>پای تا سر دود آه حسرتی سرو روان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بسته هر برگی بخود پیرایه صد گونه رنگ</p></div>
<div class="m2"><p>جلوه گر گردیده طاووسان بجای بلبلان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>داده از بس رنگ تأثیر هوا هر برگ را</p></div>
<div class="m2"><p>چشم رنگینی حنا دارد ز برگ گردکان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>صفحه خاک از گل ریحان زبس پر اختر است</p></div>
<div class="m2"><p>مزد مردی کو بگوید: این زمین، آن آسمان!</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بسکه زرین است از اکسیر تأثیر هوا</p></div>
<div class="m2"><p>گشته تیغ آفتاب از عکس گلها زر نشان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در هوا از رشته نظاره اهل نظر</p></div>
<div class="m2"><p>مینماید همچو کاغذ باد، هر برگ خزان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بسکه در وقت نمو، مشق پریدن کرده است</p></div>
<div class="m2"><p>می پرد از طبله عطار هم زآن زعفران</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گشته چابک بسکه گلهای خریفی در نمو</p></div>
<div class="m2"><p>کرده از نشو و نما مشق پریدن زعفران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بوستان افروز نبود، هست آهی آتشین</p></div>
<div class="m2"><p>نیست آن زلف عروسان، هست اشک عاشقان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بسکه افگنده است هر سو در چمن مرغ دلی</p></div>
<div class="m2"><p>گشته خون آلود تیر بوستان افروز از آن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گشته چون وقت زوال آفتاب جعفری</p></div>
<div class="m2"><p>میکند تاج خروسان در چمن هر سو فغان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بسکه رنگین است گلشن، مینماید در نظر</p></div>
<div class="m2"><p>همچو داغ لاله، زاغان در میان گلستان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هست در چشم نظر بازان ز زاغان هر طرف</p></div>
<div class="m2"><p>همچو چشم سرمه دار دلبران هر آشیان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بشکفد هر دم ز شاخ خامه ای رنگین گلی</p></div>
<div class="m2"><p>گلشن طبع مرا گویی بهار است این خزان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گرچه باشد گلستان را، آب و رنگ از حد فزون</p></div>
<div class="m2"><p>بوستان نظم من هم، کی دهد باجی بآن؟!</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ما ز سیر گلشن معنی، دلی وا میکنیم</p></div>
<div class="m2"><p>کرده ما را فارغ این گلشن ز سیر گلستان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نی غلط گفتم، چه آید زین پریشان گفته ها</p></div>
<div class="m2"><p>میکنم زین حرفها، کلک زبان را امتحان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا نویسم شمه یی از مدحت شاهی که اوست</p></div>
<div class="m2"><p>سرور دنیا و دین، فخر زمین و آسمان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نور چشم مصطفی «باقر» امام پنجمین</p></div>
<div class="m2"><p>آنکه می بالد سخن برخود ز مدحش هر زمان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دل چو مه سازد ببالیدن شکست خود درست</p></div>
<div class="m2"><p>گر کند آیینه مهرویش آیینه دان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر بر برگی ز دهقان شکوه پیش عدل او</p></div>
<div class="m2"><p>میخورد از زهره شیر، آب دیگر نیستان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ور کند تحریر، حرفی از حمایتهای او</p></div>
<div class="m2"><p>بر قلم هرگز نیفتد زور دیگر از بنان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بحر عدلش تند اگر بسوی اهل جور</p></div>
<div class="m2"><p>دیگر آتش زور نتواند گرفتن ازکمان!</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کرده تا عزم شکست شیشه عمر عدو</p></div>
<div class="m2"><p>بسته هر سنگی بخدمت چون سلیمانی میان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مهره گر سازند سنگی را، ز کوه قدر او</p></div>
<div class="m2"><p>چین روز و شب رود بیرون ز طومار زمان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کشت حاجتها اگر خرم نبودی از شرر</p></div>
<div class="m2"><p>آب حلمش تخم آتش را فگندی از جهان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هرزه نالان بس که دارند احتیاط از نهی او</p></div>
<div class="m2"><p>نی ز بیم او بجز در پرده ننوازد شبان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چشم بیدارش بخواب ناز، یکسر زهر چشم</p></div>
<div class="m2"><p>فرق فرقدساش، با بالین راحت سر گران</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>صنع حق را بود چشم و، یاد حق را بود دل</p></div>
<div class="m2"><p>حکم حق را بود گوش و، حرف حق را بد زبان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کاخ ملت را ستون و، قصر دانش را اساس</p></div>
<div class="m2"><p>راه حق را رهنما و، حصن دین را پاسبان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اطلس زر دوز گردون کرده زیر اندازیش</p></div>
<div class="m2"><p>خاک تا گردیده شمع جسم او را شمعدان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر نماید یاد قدرش، ناله بر خیزد ز دل</p></div>
<div class="m2"><p>ور کند تعریف تمکینش، گران گردد زبان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از وقارش چون بگویم شمه یی، نبود عجب</p></div>
<div class="m2"><p>گر شود قدر سخن با این سبک قدری، گران</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>زنده سازد نام خود را تا زیاد او، سخن</p></div>
<div class="m2"><p>مرده آنست کو را باشد از مدحتگران</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شمع فکر مدحش از فانوس دل تا روشن است</p></div>
<div class="m2"><p>لفظ و معنی گرددش برگرد سر پروانه سان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>لیکن این خجلت که از مدحش سخن اندوخته است</p></div>
<div class="m2"><p>چون دگر خواهد برآوردن سر از جیب دهان؟!</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>حد واعظ کی بود شاها تلاش مدح تو؟</p></div>
<div class="m2"><p>نیست جز تعریف عرض حال منظوری از آن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>از شر و شور دو عالم مأمنی میخواستم</p></div>
<div class="m2"><p>داد حسن اعتقادم آستانت را نشان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تا تواند فکر خود دیدن برای روز مرگ</p></div>
<div class="m2"><p>چشم دارد دیده دل سرمه یی ز آن آستان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هست ما را وقت تنگ و، پای لنگ و، راه سنگ</p></div>
<div class="m2"><p>وقت همراهی است ای امیدگاه شیعیان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>راه مقصد سخت دورو، پای همت سخت سست</p></div>
<div class="m2"><p>لاشه تن بس ضعیف و، بار خدمت خوش گران</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چشم بینش پر ضعیف و، خط مطلب بس خفی</p></div>
<div class="m2"><p>زندگی بسیار پیر و، آرزوها بس جوان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دردمندم، نامرادم، بینوایم، عاجزم</p></div>
<div class="m2"><p>بیکسم، بیچاره ام، بی دست و پایم، الأمان!</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گر ز بیقدری چو خاکم، لیک خاک آن درم</p></div>
<div class="m2"><p>ور زبد خویی سگم، اما سگ آن آستان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>از سگان هم کمترم، گر رانیم از درگهت</p></div>
<div class="m2"><p>بر سران هم سرورم، گر خوانیم از بندگان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گر تو افروزی چراغم، آفتابم آفتاب!</p></div>
<div class="m2"><p>گر تو برداری ز خاکم، آسمانم آسمان!!</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>گو سخن راه دعا سرکن دگر، ز آنرو که هست</p></div>
<div class="m2"><p>مدعا در پرده دل نیز پیش او عیان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تا بدل، از دوستی و دشمنی باشد اثر</p></div>
<div class="m2"><p>تا بگیتی، از بهار و از خزان باشد نشان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>دوستش از خاک خیزد، همچو گلهای بهار</p></div>
<div class="m2"><p>دشمنش از خاک ریزد، همچو اوراق خزان</p></div></div>