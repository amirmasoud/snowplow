---
title: >-
    شمارهٔ ۳۰۲
---
# شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>یار در نکته سرایی نه ز کس میماند</p></div>
<div class="m2"><p>حرف در شهد لب او چو مگس میماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیردش آینه چهره ز نظاره غبار</p></div>
<div class="m2"><p>در رخ او نگه ما به نفس میماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی خود بسکه خراشیدم از دست غمش</p></div>
<div class="m2"><p>خانه آیینه من، به قفس میماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست روزی که کدورت بدل ما نرسد</p></div>
<div class="m2"><p>صبح بر آینه ما به نفس میماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست غیر از سر زانو شب و روزش بالین</p></div>
<div class="m2"><p>سر شوریده واعظ به جرس میماند</p></div></div>