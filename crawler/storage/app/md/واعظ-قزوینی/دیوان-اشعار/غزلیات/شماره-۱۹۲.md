---
title: >-
    شمارهٔ ۱۹۲
---
# شمارهٔ ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>سوی قبرستان گذاری کن، که خوش بوم و بریست</p></div>
<div class="m2"><p>سبزه هر سو خط یاری گل رخ سیمین بریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه پربار از گل صدرنگ حسرت گشته است</p></div>
<div class="m2"><p>سر نهاده بر زمین، در هر قدم شاخ زریست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر طرف آرامگاه شاه دامادیست شوخ</p></div>
<div class="m2"><p>هر قدم گردک سرای نو عروس دلبریست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر قدم در دانه ها از بس بخاک افتاده اند</p></div>
<div class="m2"><p>چشم دل گر واکنی، هر جاده عقد گوهریست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوش هوش مردمان از پنبه غفلت پر است</p></div>
<div class="m2"><p>ورنه هر نعشی بدوشی، واعظی بر منبریست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر جنون دل نهادان جهان بی بقا</p></div>
<div class="m2"><p>هر مزار نوگلی از سبزه تر محضریست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که دایم میکنی جان از پی زر یافتن</p></div>
<div class="m2"><p>عبرتی زین کاخ ویرانت، به از گنج دریست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرد واعظ ذکر مرگ و، فکر مرگ اما نکرد</p></div>
<div class="m2"><p>پیش او مردن حق، اما از برای دیگریست</p></div></div>