---
title: >-
    شمارهٔ ۲۶۳
---
# شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>پیری در خواهش بدل ریش برآورد</p></div>
<div class="m2"><p>پیشم هوس ساده رخان، ریش برآورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیری ز دلم برد برون یاد خط و زلف</p></div>
<div class="m2"><p>فریاد که دودم زدل ریش برآورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود از پی صد بار فرو بردن دیگر</p></div>
<div class="m2"><p>یک ره زدل، ار کژدم غم نیش برآورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کام بنان شکرین گشت بدآموز</p></div>
<div class="m2"><p>ما را ز لب نان جو خویش برآورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این بود گل خیر بزرگان جهانم</p></div>
<div class="m2"><p>کز همدمی مردم درویش برآورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از فقر گذشتیم و، بدولت نرسیدیم</p></div>
<div class="m2"><p>ما را طلب آن کم، ازین بیش برآورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آورد بلائی که کله بر سر خسرو</p></div>
<div class="m2"><p>او هم بستم از سر درویش برآورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن چشم بقصد دل چون سنگ تو واعظ</p></div>
<div class="m2"><p>بیهوده خدنگ ستم از کیش برآورد</p></div></div>