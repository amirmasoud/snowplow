---
title: >-
    شمارهٔ ۱۶۲
---
# شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>پیری رسید و، قامت از آن در خمیدن است</p></div>
<div class="m2"><p>کز پای، وقت خار علایق کشیدن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقراض وار شد چو قد از پیریم دوتا</p></div>
<div class="m2"><p>معلوم شد که از همه وقت بریدن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور نگه بدامن مژگان کشید پای</p></div>
<div class="m2"><p>یعنی که وقت پای بدامن کشیدن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چین بر رخم چگونه نیفتد؟ که روز وشب</p></div>
<div class="m2"><p>چون عمر، باد تند روی در وزیدن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیگر بگو چه تخم امل میتوان فشاند؟</p></div>
<div class="m2"><p>اکنون که وقت سبزه ز خاکم دمیدن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیب ذقن گزیدن خوبان، دگر بس است</p></div>
<div class="m2"><p>هنگام پشت دست بدندان گزیدن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برچیده میشود چو بساط بزرگیت</p></div>
<div class="m2"><p>دیگر چه جای این همه بر خویش چیدن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دامان دل بچنگ هوس میدهی کنون</p></div>
<div class="m2"><p>کز دست خویش، وقت گریبان دریدن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دستت بزیر سنگ دوصد بار مانده است</p></div>
<div class="m2"><p>اکنون که وقت دست ز دنیا کشیدن است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از کار شد دو بال جوانی و، پیریت</p></div>
<div class="m2"><p>دل در هوای عیش همان در پریدن است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوش نوبهار عمر به تعجیل میرود</p></div>
<div class="m2"><p>برخیز چشم من، که علاجش دویدن است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>افگنده عقل طوق گریبان بگردنم</p></div>
<div class="m2"><p>زور جنون کجاست؟ که روز دریدن است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از یار و دوست، وقت سلام وداع شد</p></div>
<div class="m2"><p>قامت مرا برای همین در خمیدن است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>واعظ خموش، موعظه گفتن دگر بس است</p></div>
<div class="m2"><p>من بعد وقت موعظه خود شنیدن است</p></div></div>