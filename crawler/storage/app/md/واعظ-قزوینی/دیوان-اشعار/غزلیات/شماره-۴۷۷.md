---
title: >-
    شمارهٔ ۴۷۷
---
# شمارهٔ ۴۷۷

<div class="b" id="bn1"><div class="m1"><p>نهادم عینک و، ملک عدم را بی خفا دیدم</p></div>
<div class="m2"><p>ازین روزن عجب بستانسرای دلگشا دیدم!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه از زور جوانیها و گه از ضعف پیریها</p></div>
<div class="m2"><p>ازین ده روزه عمر بی بقا دیدی چه ها دیدم؟!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز پا هرجا فتادم، عجز من شد دستگیر من</p></div>
<div class="m2"><p>چه یاریها که در عالم ازین بیدست و پا دیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین جزء زمان از کس ندیدم همرهی در کل</p></div>
<div class="m2"><p>بغیر اینکه گاهی دستگیری از حنا دیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز هم خواهند این خلق خدا نشناس کام دل</p></div>
<div class="m2"><p>بسی جستم، همین درگاه حق را بی گدا دیدم!</p></div></div>