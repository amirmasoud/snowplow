---
title: >-
    شمارهٔ ۵۷۵
---
# شمارهٔ ۵۷۵

<div class="b" id="bn1"><div class="m1"><p>دل پر غم، سر پر شور و، جان بینوا داری</p></div>
<div class="m2"><p>نمینالی ز بیچیزی، اگر دانی چها داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بکن با نفس کافر، دست و پا، تا دست و پا داری</p></div>
<div class="m2"><p>تو اما،ای خود آرا، دست و پا بهر حنا داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سراپا چشم باید شد، کنون چون شاخ بادامت</p></div>
<div class="m2"><p>درین پیری که چشم دستگیری از عصا داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوسهای جهان سفله دارد مضطرب حالت</p></div>
<div class="m2"><p>هوایی در سرت تا هست، آتش زیر پا داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلا هر گز نیابد در سرایت راه، ای منعم</p></div>
<div class="m2"><p>اگر پیوسته دربان بر در خود از گدا داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدنگ بد سگالی را، نشانی نیست غیر از خود</p></div>
<div class="m2"><p>نباشی دوست با خود، گر بدشمن بد روا داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مترس از تنگدستی، تا توانی دل بدست آور</p></div>
<div class="m2"><p>چه میخواهی دگر ز اسباب دنیا، گر سخا داری؟!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حلالست آن زمان خواب فراغت برتو، کز رفتن</p></div>
<div class="m2"><p>توانی کاروان عمر را یک لحظه وا داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر راحت رسان مردمی، چون خواب آسایش</p></div>
<div class="m2"><p>بهر محفل که می آیی، بچشم خلق جا داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلیل نارسیدنهاست، لاف منتهی بودن</p></div>
<div class="m2"><p>چو زنگ کاروان، دوری ز منزل تا صدا داری!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نظر بر جیفه دنیا و، لاف از اوج استغنا؟</p></div>
<div class="m2"><p>بطبع کرکسی، اما پر وبال هما داری!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مجو دیگر دلیلی با ظهور وحدتش واعظ</p></div>
<div class="m2"><p>که چون پیدایی منزل، درین ره رهنما داری</p></div></div>