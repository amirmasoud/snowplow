---
title: >-
    شمارهٔ ۵۴۰
---
# شمارهٔ ۵۴۰

<div class="b" id="bn1"><div class="m1"><p>حال ما خواهی بدانی، زلف خوبان را ببین</p></div>
<div class="m2"><p>شور خس را گر ندانی، موج توفان را ببین!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد راهش گو بیا و، چشم گریان را ببین</p></div>
<div class="m2"><p>ای نسیم از کوی او برخیز و، توفان را ببین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنجها باید کشیدن، تا دلی آید بدام</p></div>
<div class="m2"><p>پیچ و تاب حلقه زلف پریشان را ببین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنده دل را منع خود از فیض بخشی مشکل است</p></div>
<div class="m2"><p>گریه بی اختیار تو بهاران را ببین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز و شب بر گریه بی اختیارت بهر نان</p></div>
<div class="m2"><p>خنده بی اختیار برق و باران را ببین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد مقدم واعظ، آن کو بر قدمها سر نهاد</p></div>
<div class="m2"><p>شاهد این حرف حالی تیر و پیکان را ببین</p></div></div>