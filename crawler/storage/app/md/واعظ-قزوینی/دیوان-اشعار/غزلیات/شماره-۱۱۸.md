---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>درا بخاطرم ای خرمی، که جا اینجاست</p></div>
<div class="m2"><p>کجا روی چو غم دلستان ما اینجاست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببزم یار، زخود هم نمیتوانم رفت</p></div>
<div class="m2"><p>شکیب خسته دلان در فراق تا اینجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرا ز عارض چون گل نقاب نگشایی</p></div>
<div class="m2"><p>ترا گمان که مگر عقل و هوش ما اینجاست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کند شکست، ز هر استخوان من فریاد</p></div>
<div class="m2"><p>به سوی درد که: سرکوچه بلا اینجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کاینات دل خویش را یکی کردیم</p></div>
<div class="m2"><p>بهر دلی که رود درد یار ما، اینجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمانه با خم ابروی قامت پیران</p></div>
<div class="m2"><p>بسوی خاک اشارت کند، که جا اینجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حواله اش ز در خود مکن به جای دگر</p></div>
<div class="m2"><p>که خاک واعظ مسکین بینوا اینجاست!</p></div></div>