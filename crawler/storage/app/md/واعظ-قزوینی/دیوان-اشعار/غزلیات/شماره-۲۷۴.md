---
title: >-
    شمارهٔ ۲۷۴
---
# شمارهٔ ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>دشمن چو ریزشی دید، زو شور و شر نخیزد</p></div>
<div class="m2"><p>جایی که آب پاشی، زآن گرد برنخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با درد عشق یکجا، عشق جهان نگنجد</p></div>
<div class="m2"><p>یک ناله بشوری، از نیشکر نخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون دل شکست، از وی ناید سخن طرازی</p></div>
<div class="m2"><p>از کاسه شکسته، آواز برنخیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خشم نیک ذاتان، بیم ضرر نباشد</p></div>
<div class="m2"><p>آری زآتش گل، هرگز شرر نخیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تمکین بیش آرد خفت، که از ترازو</p></div>
<div class="m2"><p>کم میکنند زان سر، کز جای برنخیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی تند خو به نرمی، فرمان پذیر گردد</p></div>
<div class="m2"><p>از جای شعله هرگز، با چوب تر نخیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن وعظ دلنشین شد واعظ، که هم ز دل خواست</p></div>
<div class="m2"><p>نبود جگرگداز آه، تا از جگر نخیزد</p></div></div>