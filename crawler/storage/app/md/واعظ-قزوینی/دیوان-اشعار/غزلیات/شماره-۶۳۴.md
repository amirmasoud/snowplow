---
title: >-
    شمارهٔ ۶۳۴
---
# شمارهٔ ۶۳۴

<div class="b" id="bn1"><div class="m1"><p>با لاف عقل، بازی دنیا چه خورده‌ای؟</p></div>
<div class="m2"><p>از هیچ و پوچ این همه بر خود سپرده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش آیدت حلاوت عیش جهان به کام</p></div>
<div class="m2"><p>حق هست با تو، زهر تأسف نخورده‌ای!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از باد یاد مرگ نلرزی چو برگ بید</p></div>
<div class="m2"><p>از بس چو ریشه پای درین گل فشرده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چند مرده نفس نفس پرفسون</p></div>
<div class="m2"><p>امروز زنده باش که فرداست مرده‌ای!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهل زمانه عاشق ارباب ثروتمند</p></div>
<div class="m2"><p>معشوق بلبل است گل از بهر خرده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل‌ها شدند شعله‌ور از دامن سحر</p></div>
<div class="m2"><p>ای آه آتشین، تو چه در دل فسرده‌ای؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر برگ گل رسد به نوایی ز خوان صبح</p></div>
<div class="m2"><p>ای دل تو هم بگیر نصیبی، چه مرده‌ای؟!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر عضو من رود به رهی از هجوم ضعف</p></div>
<div class="m2"><p>چون خشت و چوب خانه سیلاب برده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در پیری آید از نفسم بوی رفتگی</p></div>
<div class="m2"><p>مانند دود شمع سحرگاه مرده‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواهی کشید رخت به سرمنزل نجات</p></div>
<div class="m2"><p>واعظ به جرم خویش اگر راه برده‌ای</p></div></div>