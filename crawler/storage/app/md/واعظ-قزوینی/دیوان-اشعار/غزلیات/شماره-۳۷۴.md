---
title: >-
    شمارهٔ ۳۷۴
---
# شمارهٔ ۳۷۴

<div class="b" id="bn1"><div class="m1"><p>دل که باشد نشیمن غم یار</p></div>
<div class="m2"><p>غم دنیا در آن نیابد بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرکه از عشق سربلندی یافت</p></div>
<div class="m2"><p>نرود زیر بار منت دستار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد، کهسار کشور عشق است</p></div>
<div class="m2"><p>دل ز غیرت پلنگ آن کهسار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکنی روترش، ز تلخی غم</p></div>
<div class="m2"><p>ز آنکه غم شربتست و، دل بیمار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل آگاه نیست بی تب عشق</p></div>
<div class="m2"><p>شمع سوزد ز دیده بیدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در ره او ز پا نمی افتی</p></div>
<div class="m2"><p>اگر افتد ترا بسر این کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پای افگنده یی چه بر سر پا؟</p></div>
<div class="m2"><p>کار افتاده است، بر سر کار!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زهد خشکی بجای مانده ز تو</p></div>
<div class="m2"><p>برده آب رخت ز بس کردار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدل آب رو کنون واعظ</p></div>
<div class="m2"><p>عرق خجلتست و گریه زار</p></div></div>