---
title: >-
    شمارهٔ ۳۶۰
---
# شمارهٔ ۳۶۰

<div class="b" id="bn1"><div class="m1"><p>خرد بگیر سر خود، که یار می‌آید</p></div>
<div class="m2"><p>جنون تهیه خود کن بهار می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار، دلبری از نو بهار من دارد</p></div>
<div class="m2"><p>که پا ز لاله و گل در نگار می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب مدار که گرداب گردباد شود</p></div>
<div class="m2"><p>کنون که کشتی ما بر کنار می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز اینکه کس نگذارد وجودشان هرگز</p></div>
<div class="m2"><p>دگر ز مردم عالم چه کار می‌آید؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گذشته لعل لب او به خاطرت واعظ</p></div>
<div class="m2"><p>که گوهر سخنت آبدار می‌آید!</p></div></div>