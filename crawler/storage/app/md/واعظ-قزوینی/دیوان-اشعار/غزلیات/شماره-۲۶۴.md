---
title: >-
    شمارهٔ ۲۶۴
---
# شمارهٔ ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>نخل امیدت به بار آه سحر می‌آورد</p></div>
<div class="m2"><p>کشت طاعت را به حاصل چشم تر می‌آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه برد از کیسه سائل خوب‌تر می‌آورد</p></div>
<div class="m2"><p>ابر آب از بحر می‌گیرد گهر می‌آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کام شیرین خواهی، آبی بر لب خشکی بزن</p></div>
<div class="m2"><p>می‌برد گر نیشکر آبی شکر می‌آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمع شد چون مال، گردد مایه طول أمل</p></div>
<div class="m2"><p>آری آب ایستاده، رشته برمی‌آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تعلق بس که مشکل می‌دهد جان وقت مرگ</p></div>
<div class="m2"><p>خواجه پنداری که زر از کیسه برمی‌آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه می‌آرد به تاج پادشاهی سر فرو</p></div>
<div class="m2"><p>چون میان اهل همت سر بر در می‌آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌توان با نرمی از سختان زبان‌ها واکشید</p></div>
<div class="m2"><p>سبزه‌ها باران نرم از سنگ بر می‌آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روشناسی از سیه‌بختان طلب کن، چشم من</p></div>
<div class="m2"><p>سرمه، با آن تیرگی، نور نظر می‌آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خشک و تر را بس که واعظ رحم می‌آید به من</p></div>
<div class="m2"><p>خامه حرفم بر زبان با چشم تر می‌آورد</p></div></div>