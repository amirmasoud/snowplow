---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>قالیت، گرنه کار کرمان است</p></div>
<div class="m2"><p>زینت خانه سفره نان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب و جاروب خانه عاشق</p></div>
<div class="m2"><p>مژه تر، سرشک ریزان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست باغی بدلگشایی خلق</p></div>
<div class="m2"><p>گل این باغ، روی خندان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سبزه گلشن محبت را</p></div>
<div class="m2"><p>روی گرم آفتاب تابان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلق زنجیرسان چو بافت بهم</p></div>
<div class="m2"><p>میهمانخانه نیست زندان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرح آرد نهفتگی از خلق</p></div>
<div class="m2"><p>پسته در زیر پوست خندان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدعا عرض گشت، گریه کجاست؟</p></div>
<div class="m2"><p>کشته شد تخم، وقت باران است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست چین بر رخ تو از پیری</p></div>
<div class="m2"><p>بر وجود تو خط بطلان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوش بر ناله یی نمی بیند</p></div>
<div class="m2"><p>ورنه واعظ هزاردستان است</p></div></div>