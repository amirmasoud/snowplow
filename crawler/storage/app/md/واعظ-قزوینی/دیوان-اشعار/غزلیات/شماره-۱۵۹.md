---
title: >-
    شمارهٔ ۱۵۹
---
# شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>از خواجه ضبط و، مال ز فرزند یا زن است</p></div>
<div class="m2"><p>دست بخیل بر زر خود، مهر خرمن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اقبال این زمانه و، ادبار آن یکی است</p></div>
<div class="m2"><p>رد کردن کمان بنشان، پشت کردن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بار نگاهداری خود برکه افگنم؟</p></div>
<div class="m2"><p>دست شکسته ام من و، لطف تو گردن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بار نگاهداری خود بر که افگنم</p></div>
<div class="m2"><p>دست شکسته ام من و، لطف تو گردن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تند است باد حادثه در عالم وجود</p></div>
<div class="m2"><p>جای چراغ روشن ما، سنگ و آهن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یابد نظام کار بزرگان، ز کوچکان</p></div>
<div class="m2"><p>بر تن لباس تیغ بامداد سوزن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردیم، فکر زندگی خود کنیم چند؟</p></div>
<div class="m2"><p>از خلق زنده اوست که در فکر مردن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>واعظ تمام عمر تو در بیخودی گذشت</p></div>
<div class="m2"><p>آیی دمی بخویش که هنگام رفتن است</p></div></div>