---
title: >-
    شمارهٔ ۳۵۶
---
# شمارهٔ ۳۵۶

<div class="b" id="bn1"><div class="m1"><p>عشق او جان خسته میخواهد</p></div>
<div class="m2"><p>از دو عالم گسسته میخواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست چندانکه حسن غازه طلب</p></div>
<div class="m2"><p>عشق رنگ شکسته میخواهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوسش زان دو لب شکر خندیست</p></div>
<div class="m2"><p>دل مربای پسته میخواهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پر مشو در بدر، که درگه دوست</p></div>
<div class="m2"><p>عاشق پا شکسته میخواهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل نبیند گشاد کار، از آن</p></div>
<div class="m2"><p>که ز درهای بسته میخواهد</p></div></div>