---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>بزرگان می کنند از کیسه غیر این تجمل را</p></div>
<div class="m2"><p>که آب از خویشتن هرگز نباشد چشمه پل را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه لازم در جواب دشمنان تصدیع خود دادن؟</p></div>
<div class="m2"><p>باسکات زبان خصم، فرمان ده تغافل را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوقت خشم هم، جز نیکی از نیکان نمی آید</p></div>
<div class="m2"><p>که غیر از نکهت گل نیست دودی آتش گل را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی بر بردباران هیچگه غالب نمی گردد</p></div>
<div class="m2"><p>نیارد بر زمین هرگز کسی پشت تحمل را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو سیم و زر شود بسیار، هم از خود فنا گردد</p></div>
<div class="m2"><p>نباشد آتشی جز جمع گشتن، خرمن گل را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آش و نان توان ایمن ز شر فتنه جویان شد</p></div>
<div class="m2"><p>فرو جز با نمک نتوان نشاندن آتش مل را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نباشد در میان تا نسبت، الفت در نمیگیرد</p></div>
<div class="m2"><p>که با گل آشنایی از پر و بال است بلبل را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مدار امید همراهی ز کس، منزل اگر خواهی</p></div>
<div class="m2"><p>نباشد رهزنی چون کاروان راه توکل را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بجز جانان زبان ناله ام را کس نمی فهمد</p></div>
<div class="m2"><p>نباشد نغمه سنجی همچو گل، افغان بلبل را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخن واعظ همین بس باشد از فیض پریشانی</p></div>
<div class="m2"><p>که با زلف بتان او آشنا کرده است سنبل را</p></div></div>