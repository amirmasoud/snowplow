---
title: >-
    شمارهٔ ۵۴۱
---
# شمارهٔ ۵۴۱

<div class="b" id="bn1"><div class="m1"><p>شوخیش را نکند گوشه نشین خانه زین</p></div>
<div class="m2"><p>جوش این باده فزون است ز پیمانه زین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ره چشم، بتان در دل کس جای کنند</p></div>
<div class="m2"><p>حلقه چشم رکابست در خانه زین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده ها از مژه پر ساخته، پروانه شدند</p></div>
<div class="m2"><p>شمع من رخ چو برافروخت ز کاشانه زین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راحت طالب مقصود ز رنج طلب است</p></div>
<div class="m2"><p>منزلی نیست درین راه به از خانه زین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا فلک خاتم و، خورشید نگین است درو</p></div>
<div class="m2"><p>گوهری چون تو، ندیده است نگین خانه زین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو فتراک بخود واعظ از آن می پیچد</p></div>
<div class="m2"><p>که چرا دیده ترا دیده بیگانه زین</p></div></div>