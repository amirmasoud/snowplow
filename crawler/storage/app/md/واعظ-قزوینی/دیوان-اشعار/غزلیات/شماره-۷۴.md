---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>هرگه آن گلزار خوبی، یاد می آرد ز ما</p></div>
<div class="m2"><p>همچو باران بهاری فیض می بارد ز ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستی ما تشنه پاشیدنست از یکدگر</p></div>
<div class="m2"><p>وای اگر شیرینی غم دست بردارد ز ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما همه چون مشت خاکیم و، نفس چون تندباد</p></div>
<div class="m2"><p>میوزد این باد، تا یک ذره نگذارد ز ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نگریم زار زار، آن شوخ گل گل نشکفد</p></div>
<div class="m2"><p>گلشن رخسار خود را تازه میدارد ز ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برق جولانیم در میدان معنی ما اگر</p></div>
<div class="m2"><p>واعظ ما نیز پای کم نمی آرد ز ما</p></div></div>