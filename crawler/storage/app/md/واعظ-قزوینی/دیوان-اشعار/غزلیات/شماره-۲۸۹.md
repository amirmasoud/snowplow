---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>آن را که به دل هیچ به جز یار نباشد</p></div>
<div class="m2"><p>غمهای جهان را بدلش بار نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برمن قلمی نیست چو سلطان هوس را</p></div>
<div class="m2"><p>غم نیست گرم جامه قلمکار نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ویرانه ما را که از آن پا برکابیم</p></div>
<div class="m2"><p>چون خانه زین، گو در و دیوار نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لرزد بسرش چتر شهنشاهی کونین</p></div>
<div class="m2"><p>آن را که بسر منت دستار نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن را که بسر خلعت فخر است ز فقرش</p></div>
<div class="m2"><p>از اطلس (و) دیباش چرا عار نباشد؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دایره رسم جهان، من که برونم</p></div>
<div class="m2"><p>گو کار من خسته بپرگار نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلگیر کدورات جهانم، که مبادا</p></div>
<div class="m2"><p>آیینه دل قابل دیدار نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن مهر ز بیطالعیم پرده نشین است</p></div>
<div class="m2"><p>کافر بچنین روز گرفتار نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طاق دل درویش، ترا مصرف زر بس</p></div>
<div class="m2"><p>گو طاق رواق تو طلاکار نباشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جایی که نه بینند ز صورت سوی معنی</p></div>
<div class="m2"><p>چون واعظ ما صورت دیوار نباشد؟!</p></div></div>