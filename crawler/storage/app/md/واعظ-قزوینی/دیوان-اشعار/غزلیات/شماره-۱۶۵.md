---
title: >-
    شمارهٔ ۱۶۵
---
# شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>کوه کن از طرفی، وز طرفی مجنون است</p></div>
<div class="m2"><p>پر ز عشق است، اگر کوه و اگر هامون است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیست، کو خانه خراب هوس دنیا نیست؟</p></div>
<div class="m2"><p>یکی از خاک نشینان درش، قارون است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش پاکان، همه اقبال جهان ادبار است</p></div>
<div class="m2"><p>نخل در آینه آب روان وارون است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اهل دل، ربط بهم از ره باطن دارند</p></div>
<div class="m2"><p>رشته گوهر صد لفظ همان مضمون است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش ابنای زمان، عقل و خرد بی شرمیست</p></div>
<div class="m2"><p>بید را نام سرافگنده چو شد، مجنون است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جامه هستیت از لوث هوس پاک کند</p></div>
<div class="m2"><p>مالش سختی ایام، تو را صابون است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ظاهر و باطن ما، در طبق اخلاص است</p></div>
<div class="m2"><p>هرچه در خانه بود آینه را بیرون است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تاب دردسر پرگویی کهسارم نیست</p></div>
<div class="m2"><p>جای مجنون سراپا رم ما، هامون است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن قدر تشنه حرف لب یاقوت توام</p></div>
<div class="m2"><p>که زبان چون قلم از کام مرا بیرون است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کیست واعظ، که کند دعوی صاحب سخنی؟</p></div>
<div class="m2"><p>این قدر بس که نگویند که ناموزون است</p></div></div>