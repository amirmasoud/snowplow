---
title: >-
    شمارهٔ ۱۸۸
---
# شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>گفتمش: آن آتش است؟ گفت که: نی،روست روست!</p></div>
<div class="m2"><p>گفتمش: آن دود چیست؟ گفت: که آن موست موست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من که بملک جهان، ندهم یک موی او</p></div>
<div class="m2"><p>دل بجهان چون دهم؟ در دل من اوست اوست!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شود او جلوه گر، در قدم سرو او</p></div>
<div class="m2"><p>جان من آب است آب، جسم منش جوست جوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه شود پردگی، مغز بود؛ مغز، مغز</p></div>
<div class="m2"><p>وآنکه بود خودنما، پوست بود پوست پوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرم بود، همچو آب؛ در گهر آدمی</p></div>
<div class="m2"><p>هست بها در حیا، آب چو در روست روست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس نپسندد ترا، زآنکه توی خودپسند</p></div>
<div class="m2"><p>گر تو به خود دشمنی، خلق بود دوست دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پا ننهد یاد دوست، در دل پرکبر و ناز</p></div>
<div class="m2"><p>دل چو شود خاکسار، یار مرا کوست کوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیم و زر و ملک و مال، دشمن جان تواند</p></div>
<div class="m2"><p>با تو کسی نیست نیست غیر غم دوست دوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم سیه مست او، گر گزدش دور نیست</p></div>
<div class="m2"><p>واعظ بی دل کباب، ز آتش آن خوست خوست</p></div></div>