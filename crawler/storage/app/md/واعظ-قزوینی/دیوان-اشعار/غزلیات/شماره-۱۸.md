---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>این قدر طول امل ره میدهی در دل چرا</p></div>
<div class="m2"><p>مصحف خود را این خط میکنی باطل چرا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیش دنیا احتلام خواب غفلت بیش نیست</p></div>
<div class="m2"><p>از خیالی این قدر آلودگی ای دل چرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از محیط آرزو بگذر نفس تا میوزد</p></div>
<div class="m2"><p>در چنین باد مرادی این قدر کاهل چرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صید مطلب تا کنی بگریز از خود همچو تیر</p></div>
<div class="m2"><p>چون کمان حلقه برخود این قدر مایل چرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قد خمید و دل همان بر زندگانی بسته است</p></div>
<div class="m2"><p>همچو ناخن مانده یی در عقده مشکل چرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانه جان را ز کاه جسم میسازد جدا</p></div>
<div class="m2"><p>این قدر لرزش ز باد مرگ ای حاصل چرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم تا وا می کنی از خواب غفلت منزل است</p></div>
<div class="m2"><p>چون ره خوابیده واعظ دوری منزل چرا؟</p></div></div>