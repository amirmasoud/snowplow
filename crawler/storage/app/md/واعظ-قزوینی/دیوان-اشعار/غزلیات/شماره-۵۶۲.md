---
title: >-
    شمارهٔ ۵۶۲
---
# شمارهٔ ۵۶۲

<div class="b" id="bn1"><div class="m1"><p>کشد از گرم رویی، نیک و بد را در بر آیینه</p></div>
<div class="m2"><p>ازین رو، نیک و بد را نیز باشد رو در آیینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نباشد شاهدی بر خوبی کس، همچو اطوارش</p></div>
<div class="m2"><p>ندارد به ز خود بر خوبی خود محضر آیینه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز یار دلنشینی، هر دمش باید جدا گشتن</p></div>
<div class="m2"><p>نمی باشد از آن یک لحظه بی چشم تر آیینه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه نالی از کدورتهای دنیا، کان کند پاکت؛</p></div>
<div class="m2"><p>بود بیجا کند گر شکوه از روشنگر آیینه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل روشن کند هموار زشتیهای دنیا را</p></div>
<div class="m2"><p>که زن را میکند مقبول طبع شوهر آیینه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکسته دل ز بس بیند هنرمندان عالم را</p></div>
<div class="m2"><p>ز چشم مردمان پوشیده دارد جوهر آیینه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو با خود آنچه کردی از جهالت پیشگی، دیگر</p></div>
<div class="m2"><p>بروی خود چه سان خواهی نگه کردن در آیینه؟!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جز اصلاح حال خلق، مطلب نیست واعظ را</p></div>
<div class="m2"><p>نگوید عیب مردم را ز روی دیگر آیینه</p></div></div>