---
title: >-
    شمارهٔ ۳۴۲
---
# شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>قد چون خمید، جمله حواست زبون شود</p></div>
<div class="m2"><p>لشکر شود شکسته، علم چون نگون شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهرت به نیکوی ز قناعت کند کسی</p></div>
<div class="m2"><p>از آب کم، شمیم گلستان فزون شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق نمیکشد بستم دست از طلب</p></div>
<div class="m2"><p>گردد بسوی دوست روان، دل چو خون شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم و چراغ گلشن هستی تویی، اگر</p></div>
<div class="m2"><p>مانند لاله کاسه ترا سرنگون شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنج و گهر بخاک فشاندن، ز عقل نیست</p></div>
<div class="m2"><p>حیف است عمر بر سر دنیای دون شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واعظ بروز مرگ هواها که در دلست</p></div>
<div class="m2"><p>گردد یک آه حسرت و از دل برون شود!</p></div></div>