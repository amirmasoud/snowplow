---
title: >-
    شمارهٔ ۴۵۸
---
# شمارهٔ ۴۵۸

<div class="b" id="bn1"><div class="m1"><p>بگذشت زندگی همه در انتظار مرگ</p></div>
<div class="m2"><p>اما چه زندگی؟ که نیامد بکار مرگ!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عینک بدیده نیست مرا، نور چشم من</p></div>
<div class="m2"><p>چشمم چهار شد بره انتظار مرگ!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر خاست گرد پیریم از شاهراه عمر</p></div>
<div class="m2"><p>معلوم شد که میرسد اینک سوار مرگ!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بهر دورباش حواسم ز راه او</p></div>
<div class="m2"><p>گردیده پیریم ز عصا چوبدار مرگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با هر دو پا بدام فتادم، چو قد خمید</p></div>
<div class="m2"><p>پشت دو تاست، خم کمند شکار مرگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بردیم مرده مرده بسر بسکه زندگی</p></div>
<div class="m2"><p>امروز نیستیم غریب دیار مرگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین پیش جنس مرگ چنین رایگان نبود</p></div>
<div class="m2"><p>برداشت دوریت ز میان اعتبار مرگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آسوده ز اضطراب معیشت نمی شود</p></div>
<div class="m2"><p>با خویشتن کسی ندهد تا قرار مرگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>واعظ، مرا نه پشت خم از ضعف پیری است</p></div>
<div class="m2"><p>قد کرده ام دوتا، که روم زیر بار مرگ</p></div></div>