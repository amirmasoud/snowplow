---
title: >-
    شمارهٔ ۴۹۳
---
# شمارهٔ ۴۹۳

<div class="b" id="bn1"><div class="m1"><p>برکنده از حیات، باندک بهانه ایم</p></div>
<div class="m2"><p>دندان کرم خورده کام زمانه ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردیده پرده هنر ما، وجود ما</p></div>
<div class="m2"><p>ز آن قدر ما نهفته، که خود در میانه ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امروز در بساط جهان، نیست خوشدلی</p></div>
<div class="m2"><p>ما مست بزم دوش، و شراب شبانه ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فارغ ز هر غمم، دل پر یاد دوست کرد</p></div>
<div class="m2"><p>ما پادشاه عالم خود زین خزانه ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ما غرض دلست و، غرض از دلست دوست</p></div>
<div class="m2"><p>دل چشم و، دوست مردم و، ما چشم خانه ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فارغ کسی چو خوشه ز برگ معاش نیست</p></div>
<div class="m2"><p>تا زنده ایم، در طلب آب و دانه ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داریم جای بر سر زلف پریوشان</p></div>
<div class="m2"><p>تا در گشاد کار ضعیفان چو شانه ایم</p></div></div>