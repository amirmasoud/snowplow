---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>نوروز گشت و هر رگ ابری بهار را</p></div>
<div class="m2"><p>دست نوازشیست بسر روزگار را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بسکه داده باد صبا برگ گل بآب</p></div>
<div class="m2"><p>هر موج گشته شاخ گلی جویبار را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا جای واکنند کنون بهر گل زدن</p></div>
<div class="m2"><p>از سر نهند اهل غرور اعتبار را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سودای داغ لاله اش از بس به سر زده است</p></div>
<div class="m2"><p>زنجیر کرده اند ز رگ کوهسار را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر سو گل پیاده به سیلاب آب و رنگ</p></div>
<div class="m2"><p>نبود عجب ز پای درآرد سوار را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بالیده بسکه غنچه ز فیض هوا بخود</p></div>
<div class="m2"><p>در تن نهفته چون دم زنبور خار را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسیار چیده اند بخود رنگ و بوی گل</p></div>
<div class="m2"><p>کو بی حمیتی که برد نام یار را؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نزدیک شد که واشودش دل ز نوبهار</p></div>
<div class="m2"><p>واعظ ز دور دیده غم روزگار را</p></div></div>