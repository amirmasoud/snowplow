---
title: >-
    شمارهٔ ۳۶۵
---
# شمارهٔ ۳۶۵

<div class="b" id="bn1"><div class="m1"><p>غم او ساخت دلم تنگ و، هم او بگشاید</p></div>
<div class="m2"><p>دانه از آب گره گشت و، ازو بگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خور حوصله خویش برد هرکس فیض</p></div>
<div class="m2"><p>طاقتی کو که نقاب از رخ او بگشاید؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشور فتح، مسخر ز شکست تو شود</p></div>
<div class="m2"><p>این دیاریست که با تیغ عدو بگشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل، خوش از صبر به تنگ آمده، کو زور غمی</p></div>
<div class="m2"><p>که ز چاک دلم این بند رفو بگشاید؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب شرمیست در آن رخ که نبندد صورت</p></div>
<div class="m2"><p>کلک نقاش اگر چهره او بگشاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کیست با او سخن کشتن واعظ گوید؟</p></div>
<div class="m2"><p>سر حرفی مگر آن تندی خو بگشاید!</p></div></div>