---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>چون دو ابروی سیاهت که به هم پیوسته است</p></div>
<div class="m2"><p>بی‌تو شب‌های درازم همه بر هم بسته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میدود دل پی طفلی، که ز شوخی سخنش</p></div>
<div class="m2"><p>تا رسیده است بخاطر، ز زبانم جسته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشک شمعم، که برآن شعله خو تابم نیست</p></div>
<div class="m2"><p>میروم دور شوم، پای گریزم بسته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچنان رفته زما بیخبر این عمر عزیز</p></div>
<div class="m2"><p>که غباری هم از او بردل ما ننشسته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون در خانه آیینه بود درگه خلق</p></div>
<div class="m2"><p>مینماید بنظر باز، ولیکن بسته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغ جان در قفس جسم اسیر است، ولی</p></div>
<div class="m2"><p>شکر واعظ که دل از دام علایق رسته است</p></div></div>