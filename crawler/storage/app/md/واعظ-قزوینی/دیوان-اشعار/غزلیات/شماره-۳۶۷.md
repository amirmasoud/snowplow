---
title: >-
    شمارهٔ ۳۶۷
---
# شمارهٔ ۳۶۷

<div class="b" id="bn1"><div class="m1"><p>هرگز بجهان کار کجان راست نیاید</p></div>
<div class="m2"><p>تیری که بود کج، بنشان راست نیاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فیض خموشی، بشنو مدح خموشی</p></div>
<div class="m2"><p>تعریف خموشی بزبان راست نیاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر مسند آسایش کونین نشستن</p></div>
<div class="m2"><p>با خسروی ملک جهان راست نیاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی همرهی دود دل خسته، دعایت</p></div>
<div class="m2"><p>چون ناوک بی پر، بنشان راست نیاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با یکدگر آمیزش پیران و جوانان</p></div>
<div class="m2"><p>چون فصل گل و فصل خزان راست نیاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر وضع جهان باش، که در بیم شکست است</p></div>
<div class="m2"><p>گر آینه با آینه دان راست نیاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترک سر خود گیر، که فکر سرو دستار</p></div>
<div class="m2"><p>با دوستی کج کلهان راست نیاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با فکر معاش از پی معنی نتوان رفت</p></div>
<div class="m2"><p>گفتار سخن، بی لب نان راست نیاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>واعظ سخنت راست بود سخت، ار آن رو</p></div>
<div class="m2"><p>با طبع کجان، این سخنان راست نیاید!</p></div></div>