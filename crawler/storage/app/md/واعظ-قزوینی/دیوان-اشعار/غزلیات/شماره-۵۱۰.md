---
title: >-
    شمارهٔ ۵۱۰
---
# شمارهٔ ۵۱۰

<div class="b" id="bn1"><div class="m1"><p>پیش لطفش میتوان با روسیاهی ساختن</p></div>
<div class="m2"><p>لیک رو نتوان ز شرم بی گناهی ساختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند پوشی خلعت پوشیده رنگ از شراب</p></div>
<div class="m2"><p>میتوان یک چند هم، با رنگ کاهی ساختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میکند از بیم طوفان حوادث فارغت</p></div>
<div class="m2"><p>ز آن همه در بافلوس خود چو ماهی ساختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بود لذت شناس بی سرانجامی کسی</p></div>
<div class="m2"><p>میتوان با سر، برای بی کلاهی ساختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو محراب از تواضع پیش خلق روزگار</p></div>
<div class="m2"><p>نام خود را میتوانی قبله گاهی ساختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست کار عمر چون فوتی بدرگاه کریم</p></div>
<div class="m2"><p>آه را میباید از دل زود راهی ساختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساختن از روی خواهش با بد و نیک جهان</p></div>
<div class="m2"><p>به که با حکم قضا، خواهی نخواهی ساختن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فکر دستار زرت، دردسری شد دائمی</p></div>
<div class="m2"><p>میتوان ز آن دائمی، با گاهگاهی ساختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر میان دوستان، خواهی سخن چینی کنی</p></div>
<div class="m2"><p>چون قلم باید بننگ روسیاهی ساختن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست قانع آنکه دارد نعمتی مانند فقر</p></div>
<div class="m2"><p>قانع آن باشد که بتواند بشاهی ساختن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منت سر هم کشد مشکل، چه جای تاج زر؟</p></div>
<div class="m2"><p>میتواند هر که با او، گاهگاهی ساختن!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>واعظ این سرمایه عمری که داری، زین دیار</p></div>
<div class="m2"><p>میتوانی خویشتن را، خوب راهی ساختن</p></div></div>