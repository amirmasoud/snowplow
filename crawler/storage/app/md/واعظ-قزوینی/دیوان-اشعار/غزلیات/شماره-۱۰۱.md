---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>پاسبان گنج ایمانی، مرو ای دل به خواب</p></div>
<div class="m2"><p>مصحف یاد حقی، خود را مکن باطل به خواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با گرانباری درین تن پای تا سر غفلتی</p></div>
<div class="m2"><p>رفته یی ای لاشه جان، در میان گل به خواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ثمر بستن که میباید بجان استادگی</p></div>
<div class="m2"><p>گشته یی ای نخل آزاد این قدر مایل به خواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیم عقلی از سبک عقلی به خود داری گمان</p></div>
<div class="m2"><p>میکنی آن را هم از تن پروری زایل به خواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر نگاه اعتبارت، جوی آب زندگیست</p></div>
<div class="m2"><p>حیف باشد چشمه آن را کنی باطل به خواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این چنین کافتاد در راحت به یک پهلو تنت</p></div>
<div class="m2"><p>روی بیداری مگر بینی تو ای جاهل به خواب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ره سیلاب خوابیدن، بود از عقل دور</p></div>
<div class="m2"><p>تن مده در رهگذار عمر مستعجل به خواب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مال چون بسیار گردد، کم شود آسودگی</p></div>
<div class="m2"><p>بگذرد چون سیری از حد، تن رود مشکل به خواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواب را خواب عدم واعظ تواند شد بدل</p></div>
<div class="m2"><p>نقد عمر بی بدل را چون دهد عاقل به خواب</p></div></div>