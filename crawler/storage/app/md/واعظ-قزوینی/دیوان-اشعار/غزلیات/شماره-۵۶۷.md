---
title: >-
    شمارهٔ ۵۶۷
---
# شمارهٔ ۵۶۷

<div class="b" id="bn1"><div class="m1"><p>نیی کریم، ز فیض آب تا نمیگردی</p></div>
<div class="m2"><p>چو آفتاب جهانتاب تا نمیگردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شرم طاعت خود آب شو، که بی ثمر است</p></div>
<div class="m2"><p>بپای نخل دعا آب تا نمیگردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز فیض بندگی حق نمیشوی لبریز</p></div>
<div class="m2"><p>تهی ز خویش چو محراب تا نمیگردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکار بحر معانی نمیتوانی کرد</p></div>
<div class="m2"><p>دو تا ز فکر چو قلاب تا نمیگردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا ز گشت عمل نیست حاصلی واعظ</p></div>
<div class="m2"><p>تمام گریه چو دولاب تا نمیگردی</p></div></div>