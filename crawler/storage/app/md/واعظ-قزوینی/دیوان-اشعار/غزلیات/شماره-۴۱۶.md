---
title: >-
    شمارهٔ ۴۱۶
---
# شمارهٔ ۴۱۶

<div class="b" id="bn1"><div class="m1"><p>با خلق همنشین و، از ایشان رمیده باش</p></div>
<div class="m2"><p>مضمون دلنشین ز خاطره پریده باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خود چنان مرو، که دگر رو به پس کنی</p></div>
<div class="m2"><p>از چشم خویش همچو سرشک چکیده باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غافل مشو ز دشمنی خویش، یک نفس</p></div>
<div class="m2"><p>بر فرق خود، همیشه چو تیغ کشیده باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تندی ستیزه دشمن متاب روی</p></div>
<div class="m2"><p>چون خون گرم بردم خنجر دویده باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>واعظ ز بیم دشمنی اهل روزگار</p></div>
<div class="m2"><p>از چهره زمانه چو رنگ پریده باش</p></div></div>