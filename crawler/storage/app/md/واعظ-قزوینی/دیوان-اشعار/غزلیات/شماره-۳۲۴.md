---
title: >-
    شمارهٔ ۳۲۴
---
# شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>فارغ از خود هر که میگردد، فراغت میکند</p></div>
<div class="m2"><p>هر که از خود چشم پوشد، خواب راحت میکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما سراپا ناقصان را، صرفه در گمنامی است</p></div>
<div class="m2"><p>زشت رسوا میشود، چندانکه شهرت میکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فتنه میبارد ز ابر سایه بال هما</p></div>
<div class="m2"><p>سر برون کی عاقل از کنج قناعت میکند؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که از همچشمی دشمن، در شهرت زدی</p></div>
<div class="m2"><p>آنچه نتوانست دشمن کرد، شهرت میکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشنود گر حرف واعظ را کسی، گو نشنود</p></div>
<div class="m2"><p>نیست کارش با کسی، خود را نصیحت میکند!</p></div></div>