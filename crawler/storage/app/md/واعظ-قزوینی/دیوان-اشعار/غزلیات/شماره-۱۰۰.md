---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>خرمی بی غم نمی باشد درین باغ خراب</p></div>
<div class="m2"><p>خنده گل دارد از پی اشک ریزان گلاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعله رخسار او را گرد سر پروانه شو</p></div>
<div class="m2"><p>تا کی از خامی بگرد خویش گردی چون کباب؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نمیگردی مرا در چشم، بیزارم ز چشم</p></div>
<div class="m2"><p>تا نمی آیی بخوابم، از نظر افتاده خواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خشم او را، گریه عاشق کجا تسکین دهد</p></div>
<div class="m2"><p>شعله آتش چه پروا دارد از اشک کباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذوق مشق بی قراری در طلب داری اگر</p></div>
<div class="m2"><p>سر خطی بر لوح صحرا هست چون موج سراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک دل آباد در عالم بکس نگذاشتست</p></div>
<div class="m2"><p>خانه ها کردست ویران، خانه دنیا خراب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ضعف پیری قوت نالیدن از دستم گرفت</p></div>
<div class="m2"><p>چون ننالم واعظ اکنون بهر ایام شباب؟</p></div></div>