---
title: >-
    شمارهٔ ۲۹۸
---
# شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>به خود دم تا فرو بردم، سخن شد</p></div>
<div class="m2"><p>به دل تا گریه دزدیدم، چمن شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ترک کام، گردد کام حاصل</p></div>
<div class="m2"><p>ز خاموشی توان صاحب سخن شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز پس آیینه سانم آشنا رو</p></div>
<div class="m2"><p>بهر خلوت که رفتم، انجمن شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در صد حرف، برمن بسته گردید</p></div>
<div class="m2"><p>خموشی تا مرا قفل دهن شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترقی، از سفر، در گردباد است</p></div>
<div class="m2"><p>تنزل کار گرداب از وطن شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان واعظ اسیر قید هستی است</p></div>
<div class="m2"><p>که نتواند دمی از خویشتن شد</p></div></div>