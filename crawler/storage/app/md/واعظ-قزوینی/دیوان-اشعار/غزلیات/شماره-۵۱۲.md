---
title: >-
    شمارهٔ ۵۱۲
---
# شمارهٔ ۵۱۲

<div class="b" id="bn1"><div class="m1"><p>از کمی پیوسته باید بیشی خود خواستن</p></div>
<div class="m2"><p>میفزاید روشنایی شمع را از کاستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کعبتین آسا درین دیرین بساط ششدری</p></div>
<div class="m2"><p>نقش کس تا می نشیند، بایدش برخاستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا شوی ممتاز، در اصلاح کار خود بکوش</p></div>
<div class="m2"><p>میدهد بر سرفرازی نخل را پیراستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیب و زینت نیکمردان را همان نیکی بس است</p></div>
<div class="m2"><p>حسن کامل فارغست از منت آراستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پای بند این جهان را، لاف آزادی خطاست</p></div>
<div class="m2"><p>از سر خس نیست ممکن شعله را برخاستن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خودنمایی ناقصان را موجب رسواییست</p></div>
<div class="m2"><p>میشود پیدا قد کوتاه در برخاستن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جرم را واعظ در آن درگاه روی دیگر است</p></div>
<div class="m2"><p>عذر تقصیرم، همان تقصیر خواهد خواستن</p></div></div>