---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>نتوان به پند کرد نکو بدسرشت را</p></div>
<div class="m2"><p>صیقل گری نمیکند آیینه خشت را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مال جهان جهنم نقدیست ای فقیر</p></div>
<div class="m2"><p>بشناس قدر مفلسی چون بهشت را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باشد به تیره روزی خویشم امیدها</p></div>
<div class="m2"><p>ابر سیاه سرمه بود چشم کشت را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از حسن خلق دیو شود در نظر پری</p></div>
<div class="m2"><p>برقع بود گشاد جبین روی زشت را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهی رسی بمنزل نیکان مباش بد</p></div>
<div class="m2"><p>لایق گل بهشت بود هم بهشت را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چند در لباس کنی دعوی صلاح</p></div>
<div class="m2"><p>خواهی بجامه کعبه نمایی کنشت را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واعظ چو خط مپیچ سر از خامه قضا</p></div>
<div class="m2"><p>نتوان ز سرنوشت دگر سرنوشت را</p></div></div>