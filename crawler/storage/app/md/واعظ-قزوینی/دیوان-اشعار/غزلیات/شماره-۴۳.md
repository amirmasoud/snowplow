---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>دولتی نیست به از تیغ تو بیباک مرا</p></div>
<div class="m2"><p>سرنوشتی نبود جز خم فتراک مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچنان گشته ام از ضعف، که بعد از مردن</p></div>
<div class="m2"><p>رستن سبزه،برون آورد از خاک مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر نفس آب حیاتی کشم از تیغ کسی</p></div>
<div class="m2"><p>به رخ دل در فیضی است ز هر چاک مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه چنان بر سر کوی تو ز خود گم گشتم</p></div>
<div class="m2"><p>که به غربال توان یافت از آن خاک مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسکه کوتاه بود روز وصالش واعظ</p></div>
<div class="m2"><p>ترسم از جیب به دامن نرسد چاک مرا</p></div></div>