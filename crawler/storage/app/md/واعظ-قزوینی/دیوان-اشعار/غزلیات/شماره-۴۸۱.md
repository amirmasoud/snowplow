---
title: >-
    شمارهٔ ۴۸۱
---
# شمارهٔ ۴۸۱

<div class="b" id="bn1"><div class="m1"><p>چو تار چنگ کند گوشمال غم سازم</p></div>
<div class="m2"><p>چو مرغ رنگ بتنگست بال پروازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسید ضعف بجایی که نارساست اگر</p></div>
<div class="m2"><p>کنند کوک بساز خموشی آوازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس جهان شده خالی ز دوستان صدیق</p></div>
<div class="m2"><p>فرامشی بود امروز محرم رازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بشوق بال و پر افشانی فضای عدم</p></div>
<div class="m2"><p>چو گل همیشه گشاد است بال پروازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسان بید موله درین چمن واعظ</p></div>
<div class="m2"><p>ز سرفکندگی خویشتن سرافرازم</p></div></div>