---
title: >-
    شمارهٔ ۴۶۷
---
# شمارهٔ ۴۶۷

<div class="b" id="bn1"><div class="m1"><p>تا کعبه با خیال تو همدوش رفته ام</p></div>
<div class="m2"><p>این راه را تمام بآغوش رفته ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دور و دراز شد سفر بیخودی مرا</p></div>
<div class="m2"><p>گویا ببوی زلف تو از هوش رفته ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حیرتم که با همه بیقوتی چه سان</p></div>
<div class="m2"><p>از یادت ای نگار قصب پوش رفته ام؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نازش ز چین جبهه برویم کشیده تیغ</p></div>
<div class="m2"><p>تا چون خطش بسیر بنا گوش رفته ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>واعظ بمجلسی که در آن بوده حرف دوست</p></div>
<div class="m2"><p>خود را ز شوق کرده فراموش رفته ام</p></div></div>