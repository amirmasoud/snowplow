---
title: >-
    شمارهٔ ۴۰۵
---
# شمارهٔ ۴۰۵

<div class="b" id="bn1"><div class="m1"><p>کاروان راه گمنامی نمیخواهد جرس</p></div>
<div class="m2"><p>دل تپیدن هرکجا باشد، نمی باید جرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذوق خاموشی، دل از کف دادگان دانند چیست</p></div>
<div class="m2"><p>از زبان زآن ور نمی افتد که دل دارد جرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در طریق عشق از بیتابی ظاهر چه سود؟</p></div>
<div class="m2"><p>از درون بر سینه خود سنگ میکوبد جرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانده یی از راه و، باکت نیست از آوارگی</p></div>
<div class="m2"><p>در رهست و، روز وشب بر خویش میلرزد جرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تن چو از نشو و نما افتد، شود بی ذوق دل</p></div>
<div class="m2"><p>ناقه از رفتن چو ماند، بینوا گردد جرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واعظ از دل تا نخیزد گفت و گو بی حاصل است</p></div>
<div class="m2"><p>رهنما زآن شد، که دائم از درون نالد جرس</p></div></div>