---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>با حوادث برنمی‌آیند مال و جاه‌ها</p></div>
<div class="m2"><p>پا نمی‌گیرد پیش تندباد این کاه‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشنایی از در حق کن طلب، زآن رو که هست</p></div>
<div class="m2"><p>چشم و ابروهای تصویر، این در و درگاه‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همرهی از لطف حق جو، تا به مقصد ره بری</p></div>
<div class="m2"><p>غیر بی‌راهی نمی‌آید، از این همراه‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بازی دولت مخور چندین، که مانع نیستند</p></div>
<div class="m2"><p>آفتاب حشر را، این خیمه و خرگاه‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با ستمگر گو چه چشم روشنی داری دگر</p></div>
<div class="m2"><p>از چراغی کان برافروزد ز درد آه‌ها؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌شمارند اهل دنیا فقر را بی‌جوهری</p></div>
<div class="m2"><p>طعن نامردی به مردان می‌زنند این داه‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل به دنیا می‌دهی و می‌ستانی رنج و غم</p></div>
<div class="m2"><p>می‌دهی نقدی چنین از کف به این تن‌خواه‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای که دلتنگی ز پستی‌های قدر خویشتن</p></div>
<div class="m2"><p>یوسفی دارد چو حسن عاقبت، این چاه‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خانه چون نبود، اثاث خانه واعظ بهر چیست؟</p></div>
<div class="m2"><p>خانه دل را مکن ویران به این دلخواه‌ها!</p></div></div>