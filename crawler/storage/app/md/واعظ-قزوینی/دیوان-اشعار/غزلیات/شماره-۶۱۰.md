---
title: >-
    شمارهٔ ۶۱۰
---
# شمارهٔ ۶۱۰

<div class="b" id="bn1"><div class="m1"><p>تو کز طول امل در بند جمع مال و سامانی</p></div>
<div class="m2"><p>ترا به ز آستین تنگدستان نیست همیانی!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه گل خوشتر دماغ همت را زینکه هر ساعت</p></div>
<div class="m2"><p>دهان بهر طلب پیش تو بگشاید پریشانی؟!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببدمستی سرت را میخورد این روزگار آخر</p></div>
<div class="m2"><p>تو دایم از سبک مغزی همان چون پسته خندانی!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود نقش این سخن، بر گنبد سبز حباب ای دل</p></div>
<div class="m2"><p>که تا آباد میگردی، ز باد مرگ ویرانی!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نباشد جامه گوهر نگارت برتن، ای غافل</p></div>
<div class="m2"><p>سگ نفست فرو برده است، هر سو برتو دندانی!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بغیر از راحت نومیدی از غیر خدا، دیگر</p></div>
<div class="m2"><p>ز ابنای زمان هرگز کسی نشنیده احسانی!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا کز رشته طول أمل، آتش بسر سوزد</p></div>
<div class="m2"><p>بروز خویشتن چون شمع باید چشم گریانی!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو دندان نعمتی داری، منال از نان خشک خود</p></div>
<div class="m2"><p>کدامین نانخورش زین به، که با دندان خوری نانی؟!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز تیغ خصم باکم نیست، لیکن میکشد اینم</p></div>
<div class="m2"><p>که زخمم واکند هر دم دهان در پیش درمانی!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بس دوری معانی راست باهم دعوی خوبی</p></div>
<div class="m2"><p>بهر جا مصرعی کردم رقم، گردید دیوانی!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر خواهی ز دانایان شماری خویش را، واعظ</p></div>
<div class="m2"><p>ز دانایی همینت بس که دانی اینکه نادانی!</p></div></div>