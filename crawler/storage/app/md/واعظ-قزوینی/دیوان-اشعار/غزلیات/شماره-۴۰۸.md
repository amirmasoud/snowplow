---
title: >-
    شمارهٔ ۴۰۸
---
# شمارهٔ ۴۰۸

<div class="b" id="bn1"><div class="m1"><p>گذشت زندگی و، شد ز دست کار افسوس</p></div>
<div class="m2"><p>نداد فرصت افسوس، صد هزار افسوس!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برفت عهد شباب و، همان علاقه بجاست</p></div>
<div class="m2"><p>نکرده بند ز خود پاره، شد بهار افسوس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گذشت عمر و، نکردیم طاعتی هرگز</p></div>
<div class="m2"><p>ز دست رفت کمند و، نشد شکار افسوس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دمی به کار نیامد ترا ز مدت عمر</p></div>
<div class="m2"><p>شکفت و ریخت چه گلها ز شاخسار افسوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تمام عمر تو بگذشت در خود آرایی</p></div>
<div class="m2"><p>نگشت دست ز دندان ترا نگار افسوس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا بسی حرکت داد رعشه پیری</p></div>
<div class="m2"><p>ز خواب وا نشدت چشم اعتبار افسوس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل شکسته بگرداب تن تباهی ماند</p></div>
<div class="m2"><p>نرفت کشتی از این ورطه برکنار افسوس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بشد بخنده و غفلت تمام عمر و، شبی</p></div>
<div class="m2"><p>بروز خود نگریستیم زار زار افسوس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز روسیاهی ایام جاهلی واعظ</p></div>
<div class="m2"><p>نشست چهره ترا چشم اشکبار افسوس!</p></div></div>