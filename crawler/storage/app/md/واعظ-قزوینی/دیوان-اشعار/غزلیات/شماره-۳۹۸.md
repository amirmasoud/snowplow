---
title: >-
    شمارهٔ ۳۹۸
---
# شمارهٔ ۳۹۸

<div class="b" id="bn1"><div class="m1"><p>گر نباشد بیش ای دل سیم و زر، با کم بساز</p></div>
<div class="m2"><p>با سر بیدرد سرکن، با دل بیغم بساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با چه خواهم خورد امشب، یک دو شب هم صبر کن</p></div>
<div class="m2"><p>با چه خواهم ساخت فردا، یک دو روزی هم بساز!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با نگاه خشک، عمری ساختی از بی غمی</p></div>
<div class="m2"><p>چشم من، یک چند، هم با دیده پرنم بساز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست غیر از حسرت یک آه حسرت در دلم</p></div>
<div class="m2"><p>پر مکن تعجیل، با من ای نفس یک دم بساز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سازگاری، در نهاد عالم امکان کجاست؟</p></div>
<div class="m2"><p>ای دل ناساز، با من ناسازی عالم بساز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرزه نالان بوته تیر تعرض میشوند</p></div>
<div class="m2"><p>خواهی ار بسیار گو باشی، بحرف کم بساز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واعظ این ده روز را، غم بیش ازین در کار نیست</p></div>
<div class="m2"><p>از قبا با ژنده، از دینار با درهم بساز</p></div></div>