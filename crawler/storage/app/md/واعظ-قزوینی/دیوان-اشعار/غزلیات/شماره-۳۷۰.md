---
title: >-
    شمارهٔ ۳۷۰
---
# شمارهٔ ۳۷۰

<div class="b" id="bn1"><div class="m1"><p>درد تو، کی توان بتن ناتوان کشید؟!</p></div>
<div class="m2"><p>کوهی چنان بموی چنین، چون توان کشید؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جا نوشته بود ز مجنون حکایتی</p></div>
<div class="m2"><p>جسم ضعیف من خط بطلان بر آن کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پر فتنه بود از نگهت روزگار ما</p></div>
<div class="m2"><p>طاقت چه خوب کرد که پا از میان کشید!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عشق لازم است توانایی آن قدر</p></div>
<div class="m2"><p>کز چشم نیم مست تو نازی توان کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در گلشنی که آن گل رخسار وا شود</p></div>
<div class="m2"><p>واعظ توان گلاب ز برگ خزان کشید</p></div></div>