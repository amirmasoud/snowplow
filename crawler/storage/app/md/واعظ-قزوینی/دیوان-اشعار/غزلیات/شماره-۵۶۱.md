---
title: >-
    شمارهٔ ۵۶۱
---
# شمارهٔ ۵۶۱

<div class="b" id="bn1"><div class="m1"><p>زال دنیا محو گردیدن ندارد این همه</p></div>
<div class="m2"><p>روی این نادیدنی دیدن ندارد این همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مال دنیا آتش آرام سوزی بیش نیست</p></div>
<div class="m2"><p>بر سرش چون دود رقصیدن ندارد این همه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر ضبط خرده یی چند، ای توانگر روز وشب</p></div>
<div class="m2"><p>غنچه سان بر خویش پیچیدن ندارد این همه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست رزق دیگران مالت، چو سنگ آسیا</p></div>
<div class="m2"><p>بر سر هر حبه لرزیدن ندارد این همه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا تو زین سر چیده یی،ز آن سر اجل برچیده است</p></div>
<div class="m2"><p>دستگاه آرزو چیدن ندارد این همه!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میکند چپ تابی این چرخ گردان باطلش</p></div>
<div class="m2"><p>رشته آمال تابیدن ندارد این همه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست جز یک پشت پاوار، از تو تا اقلیم مرگ</p></div>
<div class="m2"><p>ای دل نامرد لنگیدن ندارد این همه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده چون پوشیده شد از غیر حق، درگاه اوست</p></div>
<div class="m2"><p>کو بکو سرگشته گردیدن ندارد این همه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای که بر لب گفت و گوی کعبه ات باشد گران</p></div>
<div class="m2"><p>آستان خلق بوسیدن ندارد این همه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خانه دنیا که از گرد کدورتهاست پر</p></div>
<div class="m2"><p>ای دل از وی چشم پوشیدن ندارد این همه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نقش پای رفتگان از بهر خودسازی بس است</p></div>
<div class="m2"><p>خانه آیینه پرسیدن ندارد این همه!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از رخ خوبان، دگر کوتاه کن دست نگاه</p></div>
<div class="m2"><p>این گل بیرنگ و بو، چیدن ندارد این همه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرگ نبود عمر من، غیر از حیات تازه یی</p></div>
<div class="m2"><p>از حیات تازه ترسیدن ندارد این همه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نام مردن برده واعظ پیش طبع نازکت</p></div>
<div class="m2"><p>گفته حرفی راست، رنجیدن ندارد این همه</p></div></div>