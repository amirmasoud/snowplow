---
title: >-
    شمارهٔ ۳۵۴
---
# شمارهٔ ۳۵۴

<div class="b" id="bn1"><div class="m1"><p>تنها ز لفظ، شعر تو دلبر نمی‌شود</p></div>
<div class="m2"><p>از رنگ گل دماغ معطر نمی‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای گوهر یگانه، تو از بس یگانه‌ای</p></div>
<div class="m2"><p>حرف رخ تو نیز مکرر نمی‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عارف غمین نمی‌شود از کسر شأن خویش</p></div>
<div class="m2"><p>آیینه از شکست مکدر نمی‌شود!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز کسی بقال نگردد ز اهل حال</p></div>
<div class="m2"><p>مفلس ز حرف مال، توانگر نمی‌شود!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی ز معنی دگرانی زبان دراز؟!</p></div>
<div class="m2"><p>کلک ار سخن نوشت، سخنور نمی‌شود!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون آب زندگی است گوارا به منعمی</p></div>
<div class="m2"><p>کز وی گلوی تشنه لبی تر نمی‌شود؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واعظ ز سوز عشق، سخنور شود زبان</p></div>
<div class="m2"><p>بی‌آتش این چراغ مرا بر نمی‌شود</p></div></div>