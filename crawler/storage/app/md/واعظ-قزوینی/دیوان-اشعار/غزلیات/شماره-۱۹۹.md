---
title: >-
    شمارهٔ ۱۹۹
---
# شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>کریم اوست که منت در آب و نانش نیست</p></div>
<div class="m2"><p>فقیر دیده ور آنکس که چشم آتش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن سرای بزرگان سراغ، ای حاجت</p></div>
<div class="m2"><p>که هست نام بزرگی، ولی نشانش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر اهل فقر نباشد تحکمی کس را</p></div>
<div class="m2"><p>فتادگیست زمینی که آسمانش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان ز صدرنشینی، به عالمی شده تنگ</p></div>
<div class="m2"><p>خوشا خرابه درویش، کآستانش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمال، وسعت احوال منعمان بخیل</p></div>
<div class="m2"><p>بود چو سفره گسترده یی که نانش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ره سلوک ز پرگار یاد گیر، که او</p></div>
<div class="m2"><p>نشد ز دایره بیرون و، در میانش نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان ز عشق پر است و، ز حرف عشق تهی</p></div>
<div class="m2"><p>حکایتیست غم دل، که داستانش نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی که گلشن صنعش همیشه در نظر است</p></div>
<div class="m2"><p>چه غم اگر ز جهان باغ و بوستانش نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهاد نفس جهادی بود که غیر نگاه</p></div>
<div class="m2"><p>بخویش از همه دزدیدنت، سنانش نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سمند نفس، سمندی بود که از تندی</p></div>
<div class="m2"><p>بجز کشیدن دست از جهان،عنانش نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چگونه چاک نگردد ز غم دل واعظ</p></div>
<div class="m2"><p>چو خامه جمله زبانست و، همزبانش نیست</p></div></div>