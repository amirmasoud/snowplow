---
title: >-
    شمارهٔ ۴۴۶
---
# شمارهٔ ۴۴۶

<div class="b" id="bn1"><div class="m1"><p>ماند ز تاب و تب بدل زار من چراغ</p></div>
<div class="m2"><p>گویا که دیده است رخ یار من چراغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افتد ز پای، بسکه خراب فتادن است</p></div>
<div class="m2"><p>گر افگند فروغ بدیوار من چراغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آید چو او ببزم، نماند ز من اثر</p></div>
<div class="m2"><p>ز آن رو که ظلمتم من و، دلدار من چراغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرشب بود ز گریه خونبار و اضطراب</p></div>
<div class="m2"><p>همچشم من پیاله و، همکار من چراغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون دیده یی که وا نتوان کرد در غبار</p></div>
<div class="m2"><p>روشن نمیشود ز شب تار من چراغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیچد ز روز تیره من، بر خود آفتاب</p></div>
<div class="m2"><p>سوزد بسوز سینه افگار من چراغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واعظ ز فیض یاد رخ آتشین دوست</p></div>
<div class="m2"><p>هرگز نخواسته است شب تار من چراغ</p></div></div>