---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>بالید از رخ تو دل پر ملال ما</p></div>
<div class="m2"><p>از آفتاب به در شد آخر هلال ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما ریشه در زمین قناعت دوانده ایم</p></div>
<div class="m2"><p>چون شمع آب میخورد از خود نهال ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر چهره شکسته ما، رنگ تهمت است</p></div>
<div class="m2"><p>مالیده خون بما اثر انفعال ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما تخم در زمین دیاری فشانده ایم</p></div>
<div class="m2"><p>کابر بهار نیز نگرید بحال ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز بناله دردسر کس نداده ایم</p></div>
<div class="m2"><p>خاموشی است همچو قلم قیل و قال ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بس بحال واعظ دلخسته ناله کرد</p></div>
<div class="m2"><p>افتاد از زبان قلم هرزه نال ما</p></div></div>