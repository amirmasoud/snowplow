---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>ریش سازد ز نزاکت گل رخسار ترا</p></div>
<div class="m2"><p>گر خلد خار به پا طالب دیدار ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرصت چشم گشودن به نگاهی ندهد</p></div>
<div class="m2"><p>کس چو حیرت نکشد غیرت رخسار ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگذر از خاک من ای شوخ که نتوان دیدن</p></div>
<div class="m2"><p>در کف جلوه گری دامن رفتار ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ببالی بخود از خوبی خود در کار است</p></div>
<div class="m2"><p>دامن آینه یی آتش رخسار ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چو خورشید رسد بر فلکم سر ندهم</p></div>
<div class="m2"><p>بدو صد بال هما سایه دیوار ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واعظ ما نه ز خود این همه شیرین سخن است</p></div>
<div class="m2"><p>دیده دلچسبی شیرینی گفتار ترا</p></div></div>