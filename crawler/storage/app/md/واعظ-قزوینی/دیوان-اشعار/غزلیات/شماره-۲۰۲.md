---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>دمی بشمع کرامت، چو تندی خو نیست</p></div>
<div class="m2"><p>خطی بحرف سعادت چو چین ابرو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهای گوهر مردم بود بآب حیا</p></div>
<div class="m2"><p>بفرق خاک کسی را که آب دررو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمغز پوچ بود پیش مرد آزاده</p></div>
<div class="m2"><p>سری که روز وشب از فکر حق بزانو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبحر غفلت دنیای درون ترا خطری</p></div>
<div class="m2"><p>چوچار موجه خواب چهار پهلو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که میکند نگه اکنون بروی فضل و هنر</p></div>
<div class="m2"><p>درین زمانه نظر جز بچشم و ابرو نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین زمانه بجا نیست اعتبار کسی</p></div>
<div class="m2"><p>ندارد آینه رو، پشتش ار بپهلو نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جداست غیرت مردی و، خوی تند جدا</p></div>
<div class="m2"><p>که طاق ابروی مردانه چین ابرو نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخنوریست بتحریک دل مرا واعظ</p></div>
<div class="m2"><p>بنان اگر نبود، خامه خود سخنگو نیست</p></div></div>