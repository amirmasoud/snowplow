---
title: >-
    شمارهٔ ۵۷۹
---
# شمارهٔ ۵۷۹

<div class="b" id="bn1"><div class="m1"><p>میبرد هر دم دلم را، غمزه غارتگری</p></div>
<div class="m2"><p>میرود هر قطره خونم، بجوی خنجری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد خود را گر کنم قسمت، جهانی را بس است</p></div>
<div class="m2"><p>میتواند شد شکست من، شکست لشکری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشتی دل، چون ز دریا غمت بیرون رود؟</p></div>
<div class="m2"><p>همچو یاد کوه تمکین تو، دارد لنگری؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسکه لبریزم ز یاد غمزه خونریز او</p></div>
<div class="m2"><p>هر رگی گردیده بر جسم ضعیفم نشتری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خویش را برداشت از خاک مذلت روز حشر</p></div>
<div class="m2"><p>هرکه در راه خدا بگرفت دست دیگری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم چنان بر کشور هستی هجوم آورده است</p></div>
<div class="m2"><p>هرکه در راه خدا بگرفت دست دیگری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم چنان بر کشور هستی هجوم آورده است</p></div>
<div class="m2"><p>کز دلم تنها توانی دید عرض لشکری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درد اگر باشد کسی را، پادشاهی گو مباش</p></div>
<div class="m2"><p>واعظ از حق نان خشکی خواهد و، چشم تری!</p></div></div>