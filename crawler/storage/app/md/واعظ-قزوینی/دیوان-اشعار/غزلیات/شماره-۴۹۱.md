---
title: >-
    شمارهٔ ۴۹۱
---
# شمارهٔ ۴۹۱

<div class="b" id="bn1"><div class="m1"><p>چشم و گوش و عقل و حس رفتند و، ما وامانده ایم</p></div>
<div class="m2"><p>رفته است اسباب ما، خود پیشتر، ما مانده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شراب هستی احباب خالی گشت و ما</p></div>
<div class="m2"><p>همچو مو در کاسه افلاک بر جا مانده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواب مرگ از کوفت شاید آورد ما را برون</p></div>
<div class="m2"><p>در تلاش زندگانی آن قدر وامانده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما بساط هر تعلق زین سرا برچیده ایم</p></div>
<div class="m2"><p>بهر رفتن با عصائی بر سر پا مانده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرم رویی نیست پیدا، گرم خونی خود کجاست؟</p></div>
<div class="m2"><p>در میان این قدر مردم چه تنها مانده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان فگند از دوش خود بار گران جسم و، ما</p></div>
<div class="m2"><p>همچنان واعظ بزیر بار دنیا مانده ایم</p></div></div>