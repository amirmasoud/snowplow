---
title: >-
    شمارهٔ ۴۷۱
---
# شمارهٔ ۴۷۱

<div class="b" id="bn1"><div class="m1"><p>یارب ز چرک مال جهان بخش نفرتم</p></div>
<div class="m2"><p>ز آلایش تعلق آن ده طهارتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دست رفت پای، بده دست و پای سعی</p></div>
<div class="m2"><p>تن گشت همچو سرمه، بده چشم عبرتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد گناه، از گهرم برده آب و رنگ</p></div>
<div class="m2"><p>اشک ندامتم ده و، رنگ خجالتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دستار عقل کهنه شد و، دلق تن کثیف</p></div>
<div class="m2"><p>بستان مرا، که سخت گرفته است نکبتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آب حیات بندگی جانفزای تست</p></div>
<div class="m2"><p>باشد اگر ز زندگی خویش لذتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا روشنم شود که همه غیرتست هیچ</p></div>
<div class="m2"><p>بگشای دیده، یاری از این خواب غفلتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر تا بپاست نسخه اطوار من غلط</p></div>
<div class="m2"><p>یارب بده به خامه توفیق صحتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دادی چو ملک فقر، هم ارزانیم بدار</p></div>
<div class="m2"><p>ترسم که حرص شوم زند پا بدولتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لب تشنه تر، ز مزرع امید واعظم</p></div>
<div class="m2"><p>از چشمه سار لطف، بده آب رحمتم</p></div></div>