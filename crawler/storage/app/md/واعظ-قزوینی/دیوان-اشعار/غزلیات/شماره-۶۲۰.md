---
title: >-
    شمارهٔ ۶۲۰
---
# شمارهٔ ۶۲۰

<div class="b" id="bn1"><div class="m1"><p>کام حاصل ای دل از آه سحر گاهی کنی</p></div>
<div class="m2"><p>گر تو، ای قطع نظر از خلق، همراهی کنی!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک سراسر سیر آن زلف درازم آرزوست</p></div>
<div class="m2"><p>ای شب وصل از تو میترسم که کوتاهی کنی!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه میخواهد، بکن، تا هرچه میخواهی، کند</p></div>
<div class="m2"><p>لیک میخواهی تو دائم، هر چه میخواهی کنی!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانه سان سر سبز تا فردا برآری سر ز خاک</p></div>
<div class="m2"><p>باید امروز از غم آن چهره را کاهی کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا توانی بود از داد و دهش چون آفتاب</p></div>
<div class="m2"><p>از گرفتن مه صفت تا چند جانکاهی کنی؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا توان چون موج بود آزاد در بحر وجود</p></div>
<div class="m2"><p>چند بر خود فلس چندی دام چون ماهی کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا توان در دشت استغنا علم بودن چو کوه</p></div>
<div class="m2"><p>در پس دیوار مردم تا بکی کاهی کنی؟!</p></div></div>