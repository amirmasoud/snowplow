---
title: >-
    شمارهٔ ۶۲۶
---
# شمارهٔ ۶۲۶

<div class="b" id="bn1"><div class="m1"><p>خردمندی فزاید آدمی را بسکه تنهایی</p></div>
<div class="m2"><p>بدام عقل میترسم فتد مجنون ز یکتایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز وسعت خوشتر آید تنگی احوال عارف را</p></div>
<div class="m2"><p>فلاطونی بآن شان را، بخم کرده است دانایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز زینت های دنیا، عیب دنیا به شود ظاهر</p></div>
<div class="m2"><p>که باشد شاهدی بر زشتی شاهد خود آرایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برون کن خواهش زر، تا گشاید عقده ات از دل</p></div>
<div class="m2"><p>که این دنبل نگردد به، بود تا چرک دنیایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لباس پربهای ژنده پوشی کهنه شد دیگر</p></div>
<div class="m2"><p>نباشد عیب پوشی اهل دنیا را چو دارایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز هر خشک و تری، افتاده را یاری طمع باشد</p></div>
<div class="m2"><p>که دارند از عصای خویش کوران چشم بینایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بده تا میتوانی داد، داد دادن ای منعم</p></div>
<div class="m2"><p>که دست اغنیا را هست دادن شکر گیرایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گذشتم، توبه کردم، بر ندارم دست از آن دیگر</p></div>
<div class="m2"><p>بدستم گر فتد بار دگر دامان تنهایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان کرده است سست اوضاع دنیا دست دلها را</p></div>
<div class="m2"><p>که نتوان داشت امروز از نمک هم چشم گیرایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بامید زبان مگذار، شکر دوست را واعظ</p></div>
<div class="m2"><p>زبان را گر ز دست آید، بگوید شکر گویایی</p></div></div>