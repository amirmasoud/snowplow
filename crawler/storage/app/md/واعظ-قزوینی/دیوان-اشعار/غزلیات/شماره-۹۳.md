---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>ای بار داده کعبه کویت به راه‌ها</p></div>
<div class="m2"><p>گستاخ بارگاه قبول تو آه‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دامن امید تو، دست دعا دراز</p></div>
<div class="m2"><p>بر آستان عفو تو روی گناه‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رگ‌ها که در تن است، حقیقت‌شناس را</p></div>
<div class="m2"><p>باشد به سوی معرفتت شاهراه‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر سر ز پای کوبی شور تو بقعه‌ای</p></div>
<div class="m2"><p>دل‌ها ز های و هوی غمت خانقاه‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سینه‌هاست، هر نفسی، ذکر اره‌ای</p></div>
<div class="m2"><p>در دیده‌هاست، سبحه ذکرت نگاه‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر ناله‌ای ز لشکر درد تو رایتی است</p></div>
<div class="m2"><p>دل‌هاست از ستون غمت بارگاه‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق از دل دو نیم، سوار دو اسبه است</p></div>
<div class="m2"><p>غم‌ها به حفظ کشور یادت سپاه‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از یک نسیم حکم تو در بحر روزگار</p></div>
<div class="m2"><p>چون موج گشته‌اند روان سال و ماه‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رهرو به کجا به کعبه کوی تو پی برد؟</p></div>
<div class="m2"><p>کآواره گشته‌اند، درین دشت راه‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>واعظ اگر چه برده ز حد معصیت، ولی</p></div>
<div class="m2"><p>دارد ز عفو و بخشش و لطفت پناه‌ها</p></div></div>