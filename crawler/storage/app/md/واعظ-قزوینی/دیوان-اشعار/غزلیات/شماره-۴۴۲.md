---
title: >-
    شمارهٔ ۴۴۲
---
# شمارهٔ ۴۴۲

<div class="b" id="bn1"><div class="m1"><p>غیر تلخی، طعم دیگر نیست در نان طمع</p></div>
<div class="m2"><p>نانخورش از سرکه ابروست در خوان طمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که خواهی سازی آباد از گرفتن خویش را</p></div>
<div class="m2"><p>خانه های اعتباراتست، ویران طمع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طبع طامع را نباشد از گدایی چاره یی</p></div>
<div class="m2"><p>جز کف در یوزه نبود کاسه خوان طمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسکه بوی چرب و نرمی ناید از احسان خلق</p></div>
<div class="m2"><p>نانشان نبود ز خشکی باب دندان طمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معده، حرص و طمع از جوع کی گردد خلاص؟</p></div>
<div class="m2"><p>پر نگردد هیچ کس را هرگز انبان طمع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسکه طامع را پر است از خار خار حرص دل</p></div>
<div class="m2"><p>آتش افتد از نگاه گرم در جان طمع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رشته رزقت بدست قبض و بسط دیگری است</p></div>
<div class="m2"><p>زآن گره نگشوده هرگز کس بدندان طمع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چه خواهی، چیده بر خوان قناعت رنگ رنگ</p></div>
<div class="m2"><p>نان خود تاکی خوری واعظ ز انبان طمع؟!</p></div></div>