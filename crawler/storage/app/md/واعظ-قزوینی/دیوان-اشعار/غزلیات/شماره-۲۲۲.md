---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>فصل شباب رفت و، نیامد بکار هیچ</p></div>
<div class="m2"><p>فیضی نیافتیم ازین نوبهار هیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دنیای شوم را نبود، هیچ اعتبار</p></div>
<div class="m2"><p>با آنکه کس از او نگرفت اعتبار هیچ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بسکه این جهان نبود دلنشین مرا</p></div>
<div class="m2"><p>در دل نمی نشیند ازو جز غبار هیچ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در پیش زرپرست که فکرش همه زر است</p></div>
<div class="m2"><p>باشد شمار زر، همه روز شمار هیچ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر هوای نفس تو، آه ندامت است</p></div>
<div class="m2"><p>زین می ندیده است کسی جز خمار هیچ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واعظ نبوده بی تب و لرز از برای رزق</p></div>
<div class="m2"><p>بر وی نشد هوای جهان سازگار هیچ</p></div></div>