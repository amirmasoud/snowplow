---
title: >-
    شمارهٔ ۴۳۹
---
# شمارهٔ ۴۳۹

<div class="b" id="bn1"><div class="m1"><p>میکنند از پیریم با هم سر و سامان وداع</p></div>
<div class="m2"><p>وقت شد، یعنی که باید کرد با یاران وداع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوهر دندان نباشد، کز دهن ریزد مرا</p></div>
<div class="m2"><p>اشک بارد تن، همانا، میکند با جان وداع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خانه چشمم، ز بر هم خوردگی ماتم سراست</p></div>
<div class="m2"><p>میکند نور نگه با دیده گریان وداع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست از پیری مرا این درد در هر استخوان</p></div>
<div class="m2"><p>میکنند از روی درد امروز همراهان وداع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قطع شد آب جوانی، دست شستیم از خوشی</p></div>
<div class="m2"><p>درد پیری آمد و، کردیم با درمان وداع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وقت کوچ آمد، نشاید رفت بیفکران بخواب</p></div>
<div class="m2"><p>وقت رفتن گشت، باید کرد بیدردان وداع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می چکاند خون ز دلها ناله جانسوز او</p></div>
<div class="m2"><p>واعظ ما میکند گویا که با یاران وداع</p></div></div>