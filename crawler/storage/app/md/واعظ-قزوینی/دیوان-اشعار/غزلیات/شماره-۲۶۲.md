---
title: >-
    شمارهٔ ۲۶۲
---
# شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>حبذا زور جنون، مغلوب زنجیرم نکرد</p></div>
<div class="m2"><p>مرحبا سیل فنا، ممنون تعمیرم نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو دندانی که افتد در جوانیها مرا</p></div>
<div class="m2"><p>برنیامد از دهان حرفی که دلگیرم نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلق را از بس مزور دیده ام در حیرتم</p></div>
<div class="m2"><p>کز چه بود آیا که مادر آب در شیرم نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسکه جز من کسی نمی بیند ز زشتی روی من</p></div>
<div class="m2"><p>هیچ نقاشی بجز آیینه تصویرم نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من کیم؟دیوانه یی سرمست کز زور جنون</p></div>
<div class="m2"><p>هیچ کس جز حلقه اطفال زنجیرم نکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آرزوی خلوتی هرگز در آن صورت نبست</p></div>
<div class="m2"><p>هیچ جا چون خانه آیینه دلگیرم نکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهر پاکان را، ندیدم زیرپا مانند سیل</p></div>
<div class="m2"><p>پیری از کوه جوانی تا سرازیرم نکرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوان دنیا در خورند اشتهای حرص نیست</p></div>
<div class="m2"><p>جز قناعت نعمتی واعظ از آن سیرم نکرد</p></div></div>