---
title: >-
    شمارهٔ ۶۰۶
---
# شمارهٔ ۶۰۶

<div class="b" id="bn1"><div class="m1"><p>زبان گشود و، چنین گفت شمع نورانی:</p></div>
<div class="m2"><p>که هست نور و صفا بیش در پریشانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندیده در دل روشندلان، کسی غم دل</p></div>
<div class="m2"><p>نهفته آینه در خویش، چین پیشانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز باز گشتن باران ز ابر، دانستم</p></div>
<div class="m2"><p>تلاش مرتبه می آورد پشیمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سری که از در حق دیده سجده واری رو</p></div>
<div class="m2"><p>هزار حیف که آید فرو بسلطانی!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد ار تو قدر سخن کم، ببند لب واعظ</p></div>
<div class="m2"><p>که گشته قیمت کالا کم، از فراوانی</p></div></div>