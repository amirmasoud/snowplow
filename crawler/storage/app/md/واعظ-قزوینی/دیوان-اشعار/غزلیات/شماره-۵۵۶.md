---
title: >-
    شمارهٔ ۵۵۶
---
# شمارهٔ ۵۵۶

<div class="b" id="bn1"><div class="m1"><p>چشمش در ستم بر خلق باز کرده</p></div>
<div class="m2"><p>مژگان بخون مردم، دستی دراز کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برداشته بتکبیر، یکبارگی ز خود دست</p></div>
<div class="m2"><p>هر کس بقبله ما یکره نماز کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرچشمه حیات است لعل تو، لیک ما را</p></div>
<div class="m2"><p>زلف تو بی نیاز از عمر دراز کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا سیم درد بیغش سازد در آتش عشق</p></div>
<div class="m2"><p>تیغش دو نیم دل را، مانند گاز کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست از خرام نازش، کوتاه دست خوبان</p></div>
<div class="m2"><p>در نیکوی همین سرو، قدی دراز کرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بسکه گشته حیران آیینه بر جمالش</p></div>
<div class="m2"><p>دیگر بهم نیاید چشمی که باز کرده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فکر بلند واعظ از طبع پست خود نیست</p></div>
<div class="m2"><p>گویا که یاد قد آن سرو ناز کرده</p></div></div>