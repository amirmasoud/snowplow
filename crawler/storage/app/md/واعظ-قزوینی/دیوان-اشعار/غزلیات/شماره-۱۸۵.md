---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>درها همه بسته است، گشاده است در دوست</p></div>
<div class="m2"><p>درهای شهان، طاق نماها ز در اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با نیک و بدم، شیوه به جز یک جهتی نیست</p></div>
<div class="m2"><p>لوح دل من چون ورق آینه یک روست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با آینه آرایش خود رسم زنان است</p></div>
<div class="m2"><p>خود ساختن مرد به آیینه زانوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر اشک که دیده ز شرم گنه آید</p></div>
<div class="m2"><p>یک شعبه باریک ز آبیست که در روست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنبل بر گیسوی تو، کم بوی تر از رنگ</p></div>
<div class="m2"><p>گل پیش گل روی تو، بی رنگ تر از بوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واعظ ز تو سیم و زر و، از ما غم جانان</p></div>
<div class="m2"><p>ما را عوض بالش زر، بالش زانوست</p></div></div>