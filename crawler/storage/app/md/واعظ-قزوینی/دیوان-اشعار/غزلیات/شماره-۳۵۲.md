---
title: >-
    شمارهٔ ۳۵۲
---
# شمارهٔ ۳۵۲

<div class="b" id="bn1"><div class="m1"><p>ای که پرچین جبهه‌ات، از حرف مردن می‌شود</p></div>
<div class="m2"><p>خانه از مرگ تو فردا، پر ز شیون می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که با نام کفن، خود را نسازی آشنا</p></div>
<div class="m2"><p>این قبا آخر تو را پیراهن تن می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طالع فیروز خواهی، مهربانی کن بخلق</p></div>
<div class="m2"><p>چرب نرمی بر چراغ بخت، روغن می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌کنی تا ساز برگ عیش، وقت رفتن است</p></div>
<div class="m2"><p>می‌رود تا واشود گل، وقت چیدن می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیره‌روزان، کار خود سازند، در شب‌های تار؛</p></div>
<div class="m2"><p>در دل شب‌ها، چراغ شمع روشن می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جوانی‌ها به هوش آور، نه ده روز دگر</p></div>
<div class="m2"><p>تا تو می‌آیی به خود، هنگام رفتن می‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیره‌روزی، اهل بینش را بود عین صفا</p></div>
<div class="m2"><p>خانه چشم از چراغ سرمه روشن می‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مانع آفات واعظ، احتیاط است احتیاط</p></div>
<div class="m2"><p>پای تا سر چشم بودن، بر تو جوشن می‌شود</p></div></div>