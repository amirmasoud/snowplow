---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>شاد از غیرت ندیدم خاطر ناشاد را</p></div>
<div class="m2"><p>جلوه تا در بر کشید آن قامت شمشاد را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خردسالی تیره روزم کرد کز تابندگی</p></div>
<div class="m2"><p>پنجه خورشید سازد سیلی استاد را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زنده معشوق می باشند از بس عاشقان</p></div>
<div class="m2"><p>نقش شیرین زنده دارد شهرت فرهاد را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرم خویی پیشه کن کز چین جوهر پاک کرد</p></div>
<div class="m2"><p>صیقل از همواری خود جبهه فولاد را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیر غمخواری نیاید هرگز از آزادگان</p></div>
<div class="m2"><p>شانه گردد، اره گر بر سر نهی شمشاد را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرف واعظ چون کند در من اثر؟ کز روی سخت</p></div>
<div class="m2"><p>بارها کردم ادب‌ها سیلی استاد را؟</p></div></div>