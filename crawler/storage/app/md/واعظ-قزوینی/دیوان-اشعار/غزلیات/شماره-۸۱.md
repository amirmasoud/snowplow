---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>سرکرد وصف خوبی رویت، زبان ما</p></div>
<div class="m2"><p>بگرفت خوبی تو، سخن از دهان ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر پادشاهی همه عالم بما دهند</p></div>
<div class="m2"><p>غیر از غم تو هیچ نباشد از آن ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ایمن از حمایت گردون شود کسی؟</p></div>
<div class="m2"><p>تیغ سپرنماست فلک بهر جان ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زینسان که ما زدیم بلب مهر خامشی</p></div>
<div class="m2"><p>دشمن چگونه ساخت سخن از زبان ما؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایمن بود ز تفرقه، گنج از نهفتگی</p></div>
<div class="m2"><p>گردیده بی نشانی ما، پاسبان ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکسو غم لباس و، دگر سوی فکر نان</p></div>
<div class="m2"><p>سرداده زندگی چه بلاها بجان ما؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واعظ مصاف ما چو به تیغ شکستگی است</p></div>
<div class="m2"><p>هرگز نکرده پشت به دشمن کمان ما</p></div></div>