---
title: >-
    شمارهٔ ۴۷۳
---
# شمارهٔ ۴۷۳

<div class="b" id="bn1"><div class="m1"><p>غبار آسا نسیمی چون وزد، در پای او افتم</p></div>
<div class="m2"><p>که شاید یک نفس در دامن صحرای او افتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>براه عشق جانان وادی و منزل نمیدانم</p></div>
<div class="m2"><p>بجز این کز سر خود خیزم و، در پای او افتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندانم را دیگر بهر گلگشت سراپایش</p></div>
<div class="m2"><p>مگر در کوچه باغ زلف سر تا پای او افتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر باغ جنان را سفره بر سر نفگنم یک دم</p></div>
<div class="m2"><p>اگر در سایه سرو قد رعنای او افتم</p></div></div>