---
title: >-
    شمارهٔ ۴۹۵
---
# شمارهٔ ۴۹۵

<div class="b" id="bn1"><div class="m1"><p>بی رهنما، براه طلب پا گذاشتیم</p></div>
<div class="m2"><p>خود را بشوق راهروی وا گذاشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تدبیر دلگشایی ما، هیچکس نکرد</p></div>
<div class="m2"><p>این کار را بدامن صحرا گذاشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راضی بدلشکستگی جهل خود شدیم</p></div>
<div class="m2"><p>گردنکشی بمردم دانا گذاشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با ما هر آنچه خصم توانست کرد کرد</p></div>
<div class="m2"><p>ما انتقام خود بمدارا گذاشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اینجا کسی چو پرسش احوال ما نکرد</p></div>
<div class="m2"><p>ما حال خود بپرسش فردا گذاشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کارها ز خود نکشیدیم منتی</p></div>
<div class="m2"><p>جز اینکه کار خود بخدا واگذاشتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگذاشتیم در دل خود هیچ زندگی</p></div>
<div class="m2"><p>تا دل بزندگانی دنیا گذاشتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خون دل بخنده نشستن نبود رسم</p></div>
<div class="m2"><p>این رسم ما بگردن مینا گذاشتیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>واعظ شد اولین قدم ما بهشت فیض</p></div>
<div class="m2"><p>تا پای خواهش از سر دنیا گذاشتیم</p></div></div>