---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>صبح میسازد شب من چشم گوهرپاش را</p></div>
<div class="m2"><p>بار خاطر نیست هرگز روز من خفاش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناقبولی آن قدر دارم که بر تصویر من</p></div>
<div class="m2"><p>خط بطلان نیست هر موی قلم نقاش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم دشمن، روشن از روز سیاه من شود</p></div>
<div class="m2"><p>ظلمت شب سرمه باشد دیده خفاش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بخشم آن تندخو دامن ز ما افشاند و رفت</p></div>
<div class="m2"><p>مدعا دامن زدن بود آتش سوداش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>واعظ ما چشم تا وا کرد از غیر تو بست</p></div>
<div class="m2"><p>این چنین باید بنازم دیده بیناش را</p></div></div>