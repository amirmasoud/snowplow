---
title: >-
    شمارهٔ ۲۶۶
---
# شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>در هر سخن، سخنور صد تاب میخورد</p></div>
<div class="m2"><p>این بوستان ز خون جگر آب میخورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کج تابی حسود همان میکند دراز</p></div>
<div class="m2"><p>هرچند رشته سخنم تاب میخورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گول زبان نرم، ز نارستان مخور</p></div>
<div class="m2"><p>ماهی ز طعمه، بازی قلاب می خورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاداب خوبی است ز بس عارض خوشش</p></div>
<div class="m2"><p>هرجا که میرسد دل من آب میخورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون غنچه ام، ز خانه خرابی شکسته دل</p></div>
<div class="m2"><p>این باغ گویی آب ز سیلاب میخورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دایم بود مدار بزرگان ز کوچکان</p></div>
<div class="m2"><p>از دجله پشته آب به دولاب میخورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون میچکد چو تاک ز مد نگاه من</p></div>
<div class="m2"><p>گویا ز چشمه سار رخت آب میخورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باشد برای روزی ما گردش فلک</p></div>
<div class="m2"><p>این چرخ بهر رشته ما تاب میخورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باشد به یاد بستر خاکسترش اگر</p></div>
<div class="m2"><p>واعظ فریب جامه سنجاب میخورد</p></div></div>