---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>خواهد گشود عقده دلهای ریش را</p></div>
<div class="m2"><p>در شانه دیده زلف تو احوال خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راضی به کم نگشته پی بیش میدود</p></div>
<div class="m2"><p>نشناخته است خواجه زجدوار نیش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر قامت حیات لباس جوانیت</p></div>
<div class="m2"><p>کم داشت تر ز رنگ خضابست ریش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوشت ز کار ماند به فریاد خود برس</p></div>
<div class="m2"><p>چشمت ضعیف گشت ببین فکر خویش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی کنی مذاکره عیشهای دوش</p></div>
<div class="m2"><p>یک بار هم ملاحظه کن روز پیش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این نفس پیر گبر کجا قرب حق کجا</p></div>
<div class="m2"><p>در خانه خدا نبود ره کشیش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پیش دوست دم زدن از خویشتن خطاست</p></div>
<div class="m2"><p>کس با نفس ندیده در آیینه خویش را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ظالم شود فقیر چو نرمی ز حد بری</p></div>
<div class="m2"><p>گرگست گوسفند چو بیند حشیش را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>واعظ مباش غافل و محکم بگیر کار</p></div>
<div class="m2"><p>یعنی که واگذار بحق کار خویش را</p></div></div>