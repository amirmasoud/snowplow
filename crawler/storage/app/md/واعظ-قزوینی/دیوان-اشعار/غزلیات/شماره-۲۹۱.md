---
title: >-
    شمارهٔ ۲۹۱
---
# شمارهٔ ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>با دست او چو رنگ حنا دستیار شد</p></div>
<div class="m2"><p>خونم چو رگ ز غیرت او بیقرار شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آلودنش بخون رقیبان چه لازم است؟</p></div>
<div class="m2"><p>پایی که از خرام تواند نگار شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نقطه روشنست اگر حرف در رقم</p></div>
<div class="m2"><p>از حرف نقطه دهنت آشکار شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پا بر زمین نمیرسد از شوق جام را</p></div>
<div class="m2"><p>تا رنگ باده حسن ترا پرده دار شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آزاد نیستند بدولت رسیدگان</p></div>
<div class="m2"><p>گردید پای بند نگین تا سوار شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واعظ گرفت بسکه از آن هردم اعتبار</p></div>
<div class="m2"><p>در دیده اش جهان همه بی اعتبار شد</p></div></div>