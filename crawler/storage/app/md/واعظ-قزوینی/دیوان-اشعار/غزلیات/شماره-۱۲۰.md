---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>کجا عاقل بهستی دل نهاده است</p></div>
<div class="m2"><p>که ما خاکیم و، دوران گردباد است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین گر می شود اوقات ما صرف</p></div>
<div class="m2"><p>به ما این زندگانی هم زیاد است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیابی از تواضع هر چه خواهی</p></div>
<div class="m2"><p>که خاک پا شدن خاک مراد است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشد تیغ زبان طعن بر خویش</p></div>
<div class="m2"><p>جگر داری که با خود در جهاد است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسیدن تیغ را در دم بمقصد</p></div>
<div class="m2"><p>ز دوری از رفیق کج نهاد است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خموشی عالم امن و امانست</p></div>
<div class="m2"><p>سخن چون در میان آمد، فساد است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بس سرگشتگی پیچیده با ما</p></div>
<div class="m2"><p>نسیم گلشن ما، گردباد است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخود گر دوستی، دشمن میندوز</p></div>
<div class="m2"><p>که کم صد دوست، یک دشمن زیاد است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود خرج زبان از کیسه دل</p></div>
<div class="m2"><p>قلم را روسفیدی از مدادست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نباشد مشتری جنس هنر را</p></div>
<div class="m2"><p>گران قیمت چو شد کالا، کساد است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نفهمد گر کسی، نقص سخن نیست</p></div>
<div class="m2"><p>چه غم خط را، کسی گر بیسواد است؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو وا شد غنچه، خواند در کتابش</p></div>
<div class="m2"><p>که آخر بستگیها را گشاد است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به یکتایی، شوی ممتاز از خلق</p></div>
<div class="m2"><p>کجا یک از عددها در عداد است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود هرچند حق، کم گوی واعظ</p></div>
<div class="m2"><p>که کم قدر تو از حرف زیاد است</p></div></div>