---
title: >-
    شمارهٔ ۲۵۵
---
# شمارهٔ ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>نتوانست ز بس ضعف بدندان جا کرد</p></div>
<div class="m2"><p>گره لقمه ام از شیشه روزی وا کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فیض گمنامیم این بس که ز خلوتگه فقر</p></div>
<div class="m2"><p>شغل دنیا نتوانست مرا پیدا کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سایه بال هما بود بلای سیهی</p></div>
<div class="m2"><p>کز سرم پرتو خورشید سعادت واکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست شرمندگی دست تهی کم، چه عجب</p></div>
<div class="m2"><p>بید مجنون نتواند سر اگر بالا کرد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر از دست دل زار من آید با او</p></div>
<div class="m2"><p>آنچه در عالم یاری غم او با ما کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واعظ این فیض سخن نیست جز از همت عشق</p></div>
<div class="m2"><p>دم ما را نمک شور جنون گیرا کرد</p></div></div>