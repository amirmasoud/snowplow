---
title: >-
    شمارهٔ ۱۷۰
---
# شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>خود هیچ و، نقاب از خط شبرنگ گرفته است</p></div>
<div class="m2"><p>برما شکرین لعل تو، پر تنگ گرفته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم ندهد حصه به اعضای دگر هیچ</p></div>
<div class="m2"><p>دل، درد تو را سخت ببر تنگ گرفته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باشد غرضش دل، که ز ابرو مژگانست</p></div>
<div class="m2"><p>پیوسته کمانی بسر چنگ گرفته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن حسن غنی طبع، که من دیدم از آن شوخ!</p></div>
<div class="m2"><p>از باده چسان که مأوی بته سنگ گرفته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارد بزبان پند و، بدل بند علایق</p></div>
<div class="m2"><p>واعظ ز جهان گوشه از این ننگ گرفته است</p></div></div>