---
title: >-
    شمارهٔ ۲۸۰
---
# شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>ز دستبرد حوادث گرت خبر باشد</p></div>
<div class="m2"><p>بکیسه دست کرم، به ز مشت زر باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجو ز خاطر ناخوش، تلاش معنی خوش</p></div>
<div class="m2"><p>سخن طراز قلم، از دماغ تر باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشارتی است غبارت بدیده از پیری</p></div>
<div class="m2"><p>که بایدت پس ازین خاک در نظر باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بها فزایدت از صحبت مصاحب نیک</p></div>
<div class="m2"><p>که قیمتی بود آبی که در گهر باشد</p></div></div>