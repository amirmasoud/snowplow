---
title: >-
    شمارهٔ ۵۹۳
---
# شمارهٔ ۵۹۳

<div class="b" id="bn1"><div class="m1"><p>دوست داند زبان خاموشی</p></div>
<div class="m2"><p>ناله! جان تو، جان خاموشی!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کام گوشی نکرده هرگز تلخ</p></div>
<div class="m2"><p>یار شیرین‌زبان خاموشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بد نیندیشد از برای کسی</p></div>
<div class="m2"><p>همدم مهربان خاموشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرفراز، آن زبان که جای گرفت</p></div>
<div class="m2"><p>چون الف در میان خاموشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست بی‌آب و رنگ لعل لبی</p></div>
<div class="m2"><p>کآن نباشد ز کان خاموشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از گزند جهان بکش خود را</p></div>
<div class="m2"><p>در حصار امان خاموشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خصم را افگند ز اسب غرور</p></div>
<div class="m2"><p>نیزه جان‌ستان خاموشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر دشمن نیفگند در پیش</p></div>
<div class="m2"><p>غیر تیغ زبان خاموشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست شیرین شدن به کام جهان</p></div>
<div class="m2"><p>ثمر بوستان خاموشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کس چرا رنجد از خموش؟ که نیست</p></div>
<div class="m2"><p>خار در گلستان خاموشی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شوخ و جلف است بس کمیت زبان</p></div>
<div class="m2"><p>مده از کف عنان خاموشی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست گوشی، وگرنه بسیار است</p></div>
<div class="m2"><p>حرف‌ها در میان خاموشی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر متاع نجات می‌طلبی</p></div>
<div class="m2"><p>نیست جز در دکان خاموشی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باش واعظ خموش، زآنکه به وصف</p></div>
<div class="m2"><p>نیست محتاج شان خاموشی</p></div></div>