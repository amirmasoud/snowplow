---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>عینک شود چو شیشه دل عقل پیر را</p></div>
<div class="m2"><p>بیند به یک قماش پلاس و حریر را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشتی نشین فقر در این بحر فتنه خیز</p></div>
<div class="m2"><p>نیکو گرفته دامن موج حصیر را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جاهل کند بکوکب اقبال خویش ناز</p></div>
<div class="m2"><p>نادان چراغ کرده گمان چشم شیر را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیجاست ای بزرگ به ما خودنماییت</p></div>
<div class="m2"><p>بسیار دیده ایم امیر و وزیر را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسودگی اگر طلبی، برتری مجوی</p></div>
<div class="m2"><p>راحت در آسیاست همین سنگ زیر را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درویش را به درگه حق ربط دیگرست</p></div>
<div class="m2"><p>با مسجد است نسبت دیگر حصیر را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیگانگان ز یاری هم خویش می شوند</p></div>
<div class="m2"><p>عینک به جای پرده چشم است پیر را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>واعظ عجب که پای نهد یاد حق در آن</p></div>
<div class="m2"><p>تا از غبار غیر نروبی ضمیر را</p></div></div>