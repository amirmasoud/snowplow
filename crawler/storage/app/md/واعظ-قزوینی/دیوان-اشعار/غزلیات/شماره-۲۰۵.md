---
title: >-
    شمارهٔ ۲۰۵
---
# شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>در طریق بندگی از خویش میباید گذشت</p></div>
<div class="m2"><p>از هوای نفس کافر کیش میباید گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه هند مدعا بسیار نزدیک است، لیک</p></div>
<div class="m2"><p>مشکل این کز آب روی خویش میباید گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست آسان یکنفس همصحبت شاهان شدن</p></div>
<div class="m2"><p>ز اختلاط مردم درویش میباید گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه عشقست این در آن نتوان شدن با خانه کوچ</p></div>
<div class="m2"><p>از زن و فرزند و یار و خویش میباید گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدن ارباب دنیا، آن قدر دشوار نیست</p></div>
<div class="m2"><p>از سر یک خنده ای درویش میباید گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقان را واعظ از دکان عقل چاره ساز</p></div>
<div class="m2"><p>دردمند و خسته و دلریش میباید گذشت</p></div></div>