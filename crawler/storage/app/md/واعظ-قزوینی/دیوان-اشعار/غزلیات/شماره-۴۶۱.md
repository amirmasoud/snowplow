---
title: >-
    شمارهٔ ۴۶۱
---
# شمارهٔ ۴۶۱

<div class="b" id="bn1"><div class="m1"><p>گذشت عمر و، تو در کار کاهلی، کاهل!</p></div>
<div class="m2"><p>رسید مرگ و، تو بسیار غافلی، غافل!!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دم از کمال زنی، ز آنکه ناقصی ناقص!</p></div>
<div class="m2"><p>کنی بدانش خود ناز، جاهلی، جاهل!!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخویش بسته از آنی، که بیخودی بیخود!</p></div>
<div class="m2"><p>ز حق گسسته از آنی، که باطلی، باطل!!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خود چو تیر گریزت بود ضرور، ضرور!</p></div>
<div class="m2"><p>بخویشتن چو کمان، سخت مایلی، مایل!!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن نه پند پذیری، که واعظی واعظ!</p></div>
<div class="m2"><p>از آن به بند اسیری، که عاقلی، عاقل!!</p></div></div>