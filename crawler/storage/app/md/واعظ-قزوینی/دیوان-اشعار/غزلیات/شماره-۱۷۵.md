---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>بسکه ضعفم از نگاه او بخود بالیده است</p></div>
<div class="m2"><p>در جهان نیستی یارب چسان گنجیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشته نیلی از خط سبز آن بناگوش لطیف</p></div>
<div class="m2"><p>رنگ برروی گلش، روزی مگر گردیده است؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش او هرچند باشد پیش پا افتاده، لیک</p></div>
<div class="m2"><p>مصرع بحر طویل زلف او پیچیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزگار از بهر زنجیر دل دیوانه ام</p></div>
<div class="m2"><p>باز آن زلف سیه را ز کجا تابیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیشه دل بسکه چون جسم لطیفش نازکست</p></div>
<div class="m2"><p>میتوان دیدن، زما گر خاطرش رنجیده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک نفس گرد کدورت زنده در گورش کند</p></div>
<div class="m2"><p>تا تپیدنهای دل، بر خویشتن جنبیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تندبادی ز آستین دست رد میبایدش</p></div>
<div class="m2"><p>دفتر واعظ چو گل بسیار بر خود چیده است</p></div></div>