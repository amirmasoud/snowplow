---
title: >-
    شمارهٔ ۴۱۳
---
# شمارهٔ ۴۱۳

<div class="b" id="bn1"><div class="m1"><p>در گفتن عیب دگران بسته زبان باش</p></div>
<div class="m2"><p>از خوبی خود، عیب نمای دگران باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که چو بادام نیفتی به دهنها</p></div>
<div class="m2"><p>تا هست بتن رگ، همه تن بند زبان باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نقش بد و نیک، نگهداری دل کن</p></div>
<div class="m2"><p>بر آینه خاطر خود آینه دان باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جاده منه پای برون در ره مقصود</p></div>
<div class="m2"><p>گر زآنکه بمنزل نرسی، سنگ نشان باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راه طلب از خسته دلان، عقد گهر شد</p></div>
<div class="m2"><p>مانی چو گره چند؟ بدنبال روان باش!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در انجمن از دست مده خلوت خود را</p></div>
<div class="m2"><p>چون آب گهر ظاهر و، در پرده نهان باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا امن چو واعظ شوی از تیغ زبانها</p></div>
<div class="m2"><p>هر جا که روی، با سپر گوش گران باش</p></div></div>