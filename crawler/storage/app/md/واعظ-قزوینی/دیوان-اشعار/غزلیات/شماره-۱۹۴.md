---
title: >-
    شمارهٔ ۱۹۴
---
# شمارهٔ ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>آنکه هرلحظه نمرد از غم رویت، چون زیست؟</p></div>
<div class="m2"><p>وآنکه از گریه نشد آب، نمیدانم کیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم شهر محبت همه درویشانند</p></div>
<div class="m2"><p>ناتوانیست که در مملکت عشق قویست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست برداشتن وقت دعا ایمائی است</p></div>
<div class="m2"><p>که شفاعتگر ما پیش خدا دست تهیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این لئیمان که به همچشمی هم جود کنند</p></div>
<div class="m2"><p>چون دو چشمند که بی هم نتوانند گریست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه گفتند که: بر دوری تو صبر کنند</p></div>
<div class="m2"><p>آنکه میگفت و نمیکرد بجز واعظ کیست؟</p></div></div>