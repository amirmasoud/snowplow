---
title: >-
    شمارهٔ ۶۳۳
---
# شمارهٔ ۶۳۳

<div class="b" id="bn1"><div class="m1"><p>بنده تن تا به کی، ای جان اگر آزاده‌ای؟!</p></div>
<div class="m2"><p>خویش را ای دل، چرا چندی به دنیا داده‌ای؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن کرداری، چو یاری نیست در مصر قبول</p></div>
<div class="m2"><p>یوسف است، از چه بر آری هرکجا افتاده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناید از کلکم دگر نقش سخن از خال و خط</p></div>
<div class="m2"><p>بعد از این ما و از آن رخ گفتگوی ساده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راست ناید عمر کوته با امل‌های دراز</p></div>
<div class="m2"><p>مرگ بس نزدیک و، تو بسیار دور افتاده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نر گدایی گرچه نبود چون در حرص و طمع</p></div>
<div class="m2"><p>لیک چون وقت جهاد نفس گردد، ماده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آن نتابد بر تو در دشت تجرد نور حق</p></div>
<div class="m2"><p>کز سیه‌چال تعلق پا برون ننهاده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرده خوابی تو واعظ، ورنه هرسو زین چمن</p></div>
<div class="m2"><p>شبنمی بر گل، سحرخیزی‌ست بر سجاده‌ای</p></div></div>