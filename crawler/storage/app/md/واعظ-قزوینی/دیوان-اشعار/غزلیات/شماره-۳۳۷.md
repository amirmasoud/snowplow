---
title: >-
    شمارهٔ ۳۳۷
---
# شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>بر ما سخن سرد عزیزان نه گران بود</p></div>
<div class="m2"><p>کان بر دل ما، چون نفس شیشه گران بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دشنام به جان منت، ازین منفعلم من</p></div>
<div class="m2"><p>کآزار منش، باعث تصدیع زبان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگر نتواند بلب آورد فغانم</p></div>
<div class="m2"><p>یاد خوش آن روز، که درد تو جوان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عمر چه دیدیم، بجز فوت جوانی</p></div>
<div class="m2"><p>زیبا گل این باغ، همین رنگ خزان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آن عمر رسانید مرا زود به پیری</p></div>
<div class="m2"><p>کاین ابلق سرکش ز نفس گرم عنان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از گلشن ایام، ز دم سردی پیری</p></div>
<div class="m2"><p>در دست ز آیینه مرا برگ خزان بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واعظ نه عبث جان به بها داد سخن را</p></div>
<div class="m2"><p>هر معنی آن جوهری ارزنده به جان بود</p></div></div>