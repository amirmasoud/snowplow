---
title: >-
    شمارهٔ ۵۹۱
---
# شمارهٔ ۵۹۱

<div class="b" id="bn1"><div class="m1"><p>گاهی سری به خاطر غمناک می‌کشی</p></div>
<div class="m2"><p>دامان حسن خویش، برین خاک می‌کشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنداشتم که سایه نخل بلند تست</p></div>
<div class="m2"><p>آن طره سیاه که بر خاک می‌کشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او هم به دام خواهش خود می‌کشد ترا</p></div>
<div class="m2"><p>صیدی اگر به حلقه فتراک می‌کشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شعله بهر یکدمه گردنکشی ز کبر</p></div>
<div class="m2"><p>تا چند منت خس و خاشاک می‌کشی؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گل علاقه زین تن ناپاک ای نفس!</p></div>
<div class="m2"><p>دامن چو گردباد، چه بر خاک می‌کشی؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واعظ به خویش پیچی اگر در ره سلوک</p></div>
<div class="m2"><p>چون گرد باد رخت بر افلاک می‌کشی</p></div></div>