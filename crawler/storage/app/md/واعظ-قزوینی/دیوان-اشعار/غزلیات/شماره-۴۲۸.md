---
title: >-
    شمارهٔ ۴۲۸
---
# شمارهٔ ۴۲۸

<div class="b" id="bn1"><div class="m1"><p>بود دنیا زنی، طول أملها زلف و گیسویش</p></div>
<div class="m2"><p>بدل رنگ بنای طاق و منظر، چشم و ابرویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز فرزندان بیشرم زمانه غیر این ناید</p></div>
<div class="m2"><p>که تا آیند از پشت پدر، استند بر رویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبیند دیده اش روی گشایش در جهان هرگز</p></div>
<div class="m2"><p>کسی کز خوی بد دارد گره پیوسته ابرویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن تا صورت احوال خود بیند بچشم دل</p></div>
<div class="m2"><p>بهر کس داده اند آیینه یی روشن ز زانویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خلق زشت، خلق عالمی را خون بگریاند</p></div>
<div class="m2"><p>هر آنکو یک دو روزی آسمان خندید بر رویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی تابد جهان مایی ما، اویی او را</p></div>
<div class="m2"><p>نخست از خویش واعظ گم شو و، آنگاه میجویش</p></div></div>