---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>دلا از خواب بگشا چشم و، سر کن آه و یارب‌ها</p></div>
<div class="m2"><p>که نبود خلوت در بسته‌ای چون ظلمت شب‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بیداری توان دیدن رخ کام دوعالم را</p></div>
<div class="m2"><p>گشاده دیده از خوابست فتح‌الباب مطلب‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مس قرص قمر از وی زر خورشید می‌گردد</p></div>
<div class="m2"><p>نباشد خاک اکسیری چو گرد ظلمت شب‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی‌ماند نهان طاعت، بود چون نور اخلاصش</p></div>
<div class="m2"><p>جمال شمع را پنهان نسازد پرده شب‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به پیری سر ز هوش و پا ز قوت چشم از بینش</p></div>
<div class="m2"><p>ز آمد آمد مردن تهی کردند قالب‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر صد قرن خورد و می‌خورد، غافل مشو ای دل</p></div>
<div class="m2"><p>همان برجاست چرخ پیری را دندان کوکب‌ها؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهاد نفس، کی زین عزم‌های سست سر گیرد</p></div>
<div class="m2"><p>در این میدان چه خواهی ساخت با این مرده‌مرکب‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرو باید نشاندن آرزو، از غصه ایمن شد</p></div>
<div class="m2"><p>که پف کردن بود شب بر چراغ، افسون عقرب‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه گویان تشنه خون همند اهل زمان یارب</p></div>
<div class="m2"><p>به این نفرت که دارند از هم این بیگانه‌مشرب‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میان همدمان اکثر سخن می‌افگند دوری</p></div>
<div class="m2"><p>سخن چون در میان آمد، شوند از هم جدا لب‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز خود مأیوس و، با حق آشنا کردند خلقی را</p></div>
<div class="m2"><p>ندیدم کارسازی مثل این ارباب‌منصب‌ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر قدر سواد و خط همین باشد که می‌بینم</p></div>
<div class="m2"><p>ثوابی نیست چون آزادی طفلان ز مکتب‌ها؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دو دل زین آشنایان متفق با هم نمی‌بینم</p></div>
<div class="m2"><p>به راهی می‌رود هر یک ازیشان همچو مذهب‌ها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شکایت‌های خود را زان به روز حشر افگندم</p></div>
<div class="m2"><p>که کوتاهی کند از عرض حالم، طول این شب‌ها</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هوای زر ترا آتش به جان افگنده، ریزش کن</p></div>
<div class="m2"><p>عرق کردن مگر بخشد ترا صحت ازین تب‌ها</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نباشد صبح شب‌های فراقم را از آن واعظ</p></div>
<div class="m2"><p>که می‌بالند از روز سیاه من به خود شب‌ها</p></div></div>