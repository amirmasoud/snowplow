---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>دماغ اهل فنا، از مکاره آزاد است</p></div>
<div class="m2"><p>چراغ مجلس تصویر، ایمن از باداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل شکسته، چه غم دارد از حوادث دهر؟</p></div>
<div class="m2"><p>که بیم سیل کشد، خانه یی که آباد است!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکن حذر ز ضعیفان، به زور خویش مناز</p></div>
<div class="m2"><p>همیشه طعمه زنگار، مغز فولاد است!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تلاش نام کنی، در جهان اثر بگذار</p></div>
<div class="m2"><p>نه بیستون، که نگینی بنام فریاد است!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه سود ترک جهان، گر تعلقش برجاست</p></div>
<div class="m2"><p>نه بنده یی که گریزد ز خواجه آزاد است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فتاد رعشه بتن گر ترا ز رفتن عمر</p></div>
<div class="m2"><p>عجب مدار، که تن نخل و عمر تو باد است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صفای باطن مرد، از صفای ظاهر به</p></div>
<div class="m2"><p>چنانکه آینه سنگ، به ز فولاد است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غمی بغیر غم بندگی مخور واعظ</p></div>
<div class="m2"><p>که هرکه بنده او شد، ز هر غم آزاد است</p></div></div>