---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>در جهان گشته سمر حرف پریشانی ما</p></div>
<div class="m2"><p>پهن باشد همه جا سفره بینانی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست ما را گله از تنگی احوال جز این</p></div>
<div class="m2"><p>که ملولند عزیزان ز پریشانی ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست از سیل حوادث به جز این دلکوبی</p></div>
<div class="m2"><p>که چرا جغد کشد منت ویرانی ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردسرهای جهان چاره ندارد جز مرگ</p></div>
<div class="m2"><p>نیست جز خاک لحد صندل پیشانی ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما به قطع نظر از بیم حوادث رستیم</p></div>
<div class="m2"><p>چشم پوشیدن ما کرد نگهبانی ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه داریم چو گل بر طبق اخلاص است</p></div>
<div class="m2"><p>مانع همت ما نیست پریشانی ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجز از تندی سیلاب نیابد تسکین</p></div>
<div class="m2"><p>بسکه ایام بود تشنه ویرانی ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرچه در خاطر ما ساده دلان میگذرد</p></div>
<div class="m2"><p>همچو آیینه عیانست ز پیشانی ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مفلسان گرچه نبردند ز ما فیض، ولی</p></div>
<div class="m2"><p>خانه گنج شد آباد ز ویرانی ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ننهد آب رخ گریه اگر پا به میان</p></div>
<div class="m2"><p>که ز لطفش طلبد عذر پشیمانی ما؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غم او کرده قدم رنجه، نثاری است ضرور!</p></div>
<div class="m2"><p>نیست واعظ بعبث این گهر افشانی ما؟</p></div></div>