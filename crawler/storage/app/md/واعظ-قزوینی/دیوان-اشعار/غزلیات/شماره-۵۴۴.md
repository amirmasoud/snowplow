---
title: >-
    شمارهٔ ۵۴۴
---
# شمارهٔ ۵۴۴

<div class="b" id="bn1"><div class="m1"><p>از آنم کم، که گویم شکر احسان زیاد تو</p></div>
<div class="m2"><p>همینم بس که گاهی میکشم آهی بیاد تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تویی شاه من و، امید گاه من، پناه من</p></div>
<div class="m2"><p>منم مست تو، هشیار تو، غمگین تو، شاد تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوانبختان عشقت، فارغند از محنت پیری</p></div>
<div class="m2"><p>بود در کیسه هر عمری، که گردد صرف یاد تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هرکس کام دل جستم، ندیدم غیر ناکامی</p></div>
<div class="m2"><p>تو کام ما بده یارب، که باشد داد داد تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخود دلبستگی، قفل در فیض است بر رویم</p></div>
<div class="m2"><p>کلید آن قفل محکم را،نباشد جز گشاد تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خداوندا، بلطف خویشتن دریاب واعظ را</p></div>
<div class="m2"><p>که هست او بنده بیدست و پای نامراد تو</p></div></div>