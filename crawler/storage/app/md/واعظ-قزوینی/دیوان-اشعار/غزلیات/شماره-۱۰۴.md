---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>غنچه‌ای، باشد خموشی از گلستان ادب</p></div>
<div class="m2"><p>نرگسی، سر پیش افگندن ز بستان ادب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن بود یکسر کمالات تو، ای صاحب کمال</p></div>
<div class="m2"><p>جان آن باشد ادب، جان تو و جان ادب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حرمت پیران نگهدار ای جوان تا برخوری</p></div>
<div class="m2"><p>کسب پیری میکند طفل از دبستان ادب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌دهند از جان خراج از نقد اخلاص و دعا</p></div>
<div class="m2"><p>کشور دل‌هاست یکسر ملک سلطان ادب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی دهان از خنده بی‌جا ترا وا می‌شود</p></div>
<div class="m2"><p>می‌درد بی‌شرمیت بر تن گریبان ادب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌توان شد از ادب شیرین به کام روزگار</p></div>
<div class="m2"><p>کمترین نعمت گوارایی‌ست بر خوان ادب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست جای خار زیر پا و، جای گل به سر</p></div>
<div class="m2"><p>این بر گستاخی و، آن بار بستان ادب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا دهندت در گریبان دل خود خلق جا</p></div>
<div class="m2"><p>همچو گل واعظ مده از دست دامان ادب</p></div></div>