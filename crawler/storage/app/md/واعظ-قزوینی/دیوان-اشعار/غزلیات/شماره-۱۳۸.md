---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>در دلت آن نه رشته امل است</p></div>
<div class="m2"><p>چشم دید ترا رگ سبل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواجه را گو: برو بروها رفت</p></div>
<div class="m2"><p>بعد از این آمد آمد اجل است!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حادثات جهان چو سیلاب است</p></div>
<div class="m2"><p>خاکساری در او فراز تل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه جا پیروند سوختگان</p></div>
<div class="m2"><p>اسب را جای داغ بر کفل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست در سینه یاد حق در کل</p></div>
<div class="m2"><p>جزو چندی چه شد که در بغل است؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست غم را به دردمندان کار</p></div>
<div class="m2"><p>نکشد با مو سری که کل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل بی غم که نیست شاهان را</p></div>
<div class="m2"><p>اهل دل را همیشه در بغل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راستی قوت ضعیفان است</p></div>
<div class="m2"><p>که عصا دست گیر پای شل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل بیدرد، درد بیدرمان!</p></div>
<div class="m2"><p>چشم بی گریه، علم بی عمل است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این همه قول! کو عمل واعظ</p></div>
<div class="m2"><p>بندگی نی قصیده و غزل است</p></div></div>