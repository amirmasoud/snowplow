---
title: >-
    شمارهٔ ۴۲۵
---
# شمارهٔ ۴۲۵

<div class="b" id="bn1"><div class="m1"><p>پیری آمد، نه جوانیست دگر از ما خوش</p></div>
<div class="m2"><p>چون گل شمع بود بر سر ما گل ناخوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقت پیری نگه گرم بخوبان خنک است</p></div>
<div class="m2"><p>سیر گلشن نبود فصل دی و سرما خوش!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش فشانده است ز آلایش کثرت دامن</p></div>
<div class="m2"><p>چه عجب خاطر غمگین شود از صحرا خوش؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست دنیا، بجز از خانه پر مرداری</p></div>
<div class="m2"><p>چون در آن کرده تو پاکیزه طبیعت جاخوش؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو سه روزیست حیات تو و، ناخوش آن هم</p></div>
<div class="m2"><p>بگذران ناخوشی این دو سه روز، اما خوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم درویش بود، آنکه توان تنها خورد</p></div>
<div class="m2"><p>خوردن نعمت الوان، نبود تنها خوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واعظ امروز بهر ناخوش و خوش، خوشدل باش</p></div>
<div class="m2"><p>کآنچه ناخوش بود امروز، بود فردا خوش</p></div></div>