---
title: >-
    شمارهٔ ۳۳۴
---
# شمارهٔ ۳۳۴

<div class="b" id="bn1"><div class="m1"><p>گردید حرص افزون، آن را که مال افزود</p></div>
<div class="m2"><p>میگردد آب پر زور، چون میشود گل آلود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دل محبت زر، سودا فزاست در سر</p></div>
<div class="m2"><p>روشن بود که آذر، هرگز نبوده بی دود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن را که دست احسان، کم سوده دست عوران</p></div>
<div class="m2"><p>دست از تأسف آن، خواهد بسی بهم سود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهی شوم مکرم، داد ودهش مکن کم</p></div>
<div class="m2"><p>بر فرق خلق عالم، جا دارد ابر از جود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راه نگاه احسان، بر حال تنگدستان</p></div>
<div class="m2"><p>از چشم تنگ دوران، گردیده است مسدود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما راز دانه دل، در کشت این تن گل</p></div>
<div class="m2"><p>مقصود بود حاصل، حاصل نگشت مقصود!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بود دیده دید، فکر نبود کم دید</p></div>
<div class="m2"><p>از بهر بود گردید، عمری که بود نابود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این جان درد پرورد، صد بار امتحان کرد</p></div>
<div class="m2"><p>به بود از دوا درد، داری چه درد بهبود؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>واعظ چه کوبکویی؟ کام از در که جویی؟</p></div>
<div class="m2"><p>با خویشتن نگویی: جز دوست کیست موجود؟!</p></div></div>