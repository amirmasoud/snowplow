---
title: >-
    شمارهٔ ۳۵۳
---
# شمارهٔ ۳۵۳

<div class="b" id="bn1"><div class="m1"><p>کارها در آب و خاک فقر وارون می‌شود</p></div>
<div class="m2"><p>سرو اگر کارند اینجا، بید مجنون می‌شود!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چنین از شوق پابوس تو می‌بالد به خود</p></div>
<div class="m2"><p>زلف آخر مصرع آن قد موزون می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خیال آن دهان از دل نیاید در نظر</p></div>
<div class="m2"><p>در میان دیده و دل، بر سرش خون می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر نظر با دلبری باشد سرکار دلم</p></div>
<div class="m2"><p>بس که حسن ماه من، هر لحظه افزون می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست اشک لاله‌گون، کز شوق دیدارش نگاه</p></div>
<div class="m2"><p>تا به مژگان می‌رسد، چون من دلش خون می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تهیدستی نه واعظ مایه دیوانگی‌ست؟</p></div>
<div class="m2"><p>چیست باعث کز درختان بید مجنون می‌شود؟!</p></div></div>