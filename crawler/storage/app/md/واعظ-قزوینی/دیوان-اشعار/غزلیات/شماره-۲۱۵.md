---
title: >-
    شمارهٔ ۲۱۵
---
# شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>بآب سبزه، به جان تن، بود چه سان محتاج؟</p></div>
<div class="m2"><p>به درد عشق بود دل صد آنچنان محتاج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخنوری نتوان بی سخن شنو کردن</p></div>
<div class="m2"><p>سخن به گوش بود بیش از زبان محتاج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی بود ز گدا احتیاج شاه افزون</p></div>
<div class="m2"><p>که هست او به جهان، این به نیم نان محتاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز احتیاج خلاصند بی کس و کویان</p></div>
<div class="m2"><p>ز خانه داری باشد بزه کمان محتاج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خموش را به سخنگو همین مزیت بس</p></div>
<div class="m2"><p>که نیستند خموشان به همزبان محتاج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشیدم آنچه من از منت خسان واعظ</p></div>
<div class="m2"><p>مباد دشمن کس هم بدوستان محتاج</p></div></div>