---
title: >-
    شمارهٔ ۱۵۵
---
# شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>آه، شمعی ز شبستان سحرخیزان است</p></div>
<div class="m2"><p>ناله، نخلی ز گلستان سحرخیزان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دم صبح نباشد، همه کیفیت و شور</p></div>
<div class="m2"><p>گریه کاه دل بریان سحرخیزان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدم از تاج خروسان، که ز بیداری بخت</p></div>
<div class="m2"><p>دولت زنده دلی، زآن سحر خیزان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ستم، ظالم ازین گونه که پا میفشرد</p></div>
<div class="m2"><p>هدف ناوک افغان سحر خیزان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خانه دولت هرکس که به ظلم آباد است</p></div>
<div class="m2"><p>سیلش از اشک چو باران سحرخیزان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز در پرده گمنامی خویشند، چو شمع</p></div>
<div class="m2"><p>شب چو شد، عرصه جولان سحرخیزان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باغ فیض دل شبها، که گلش مغفرتست</p></div>
<div class="m2"><p>آبش از ناله غلتان سحرخیزان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عندلیب چمن روح فزای دم صبح</p></div>
<div class="m2"><p>صوت شور آور افغان سحرخیزان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قامت خویش چو سازند دوتا وقت رکوع</p></div>
<div class="m2"><p>دو جهان در خم چوگان سحرخیزان است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده واعظ از آنست پر از نعمت فیض</p></div>
<div class="m2"><p>کز گدایان سر خوان سحرخیزان است</p></div></div>