---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>شیشه دلها شکستن، نیست کار سنگ ما</p></div>
<div class="m2"><p>از برای آشتی پیوسته باشد جنگ ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه بنیاد وجود ما ز هم پاشیده است</p></div>
<div class="m2"><p>گرد برخیزد اگر در چهره، گردد رنگ ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از غم او ناله ما بسکه میبالد بخود</p></div>
<div class="m2"><p>نیست دور از پرده گر بیرون رود آهنگ ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال ما دنیا پرستان، حال سنگ و آهن است</p></div>
<div class="m2"><p>کز برای دیگران پیوسته باشد جنگ ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با جوانان همدمی ما را نمی زیبد کنون</p></div>
<div class="m2"><p>همدم ما کیست؟ معنی های شوخ و شنگ ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم ما واعظ چنان از حشمت فقر است پر</p></div>
<div class="m2"><p>کاین بزرگی‌ها نمی‌گنجد، به ظرف تنگ ما</p></div></div>