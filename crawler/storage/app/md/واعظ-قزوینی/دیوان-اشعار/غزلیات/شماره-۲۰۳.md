---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>خاطر بلهوس او چه وفا خواهد داشت</p></div>
<div class="m2"><p>لعل دوشابی آن دل، چه بها خواهد داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآن شوم گردو، نهم روی به پشت پایش</p></div>
<div class="m2"><p>که گه شرم نگاهی سوی ما خواهد داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشکند طرف کلاهی، فتد آوازه بشهر</p></div>
<div class="m2"><p>بشکند گر دل ما را، چه صدا خواهد داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بامن آن آتش سوزان چو شود گرم عتاب</p></div>
<div class="m2"><p>نگه خشم، یقین جانب ما خواهد داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عزم وادید خود آن قاعده دان دارد باز</p></div>
<div class="m2"><p>خانه آینه امروز صفا خواهد داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلبری نیست چو او، ور بود آیینه صفت</p></div>
<div class="m2"><p>شیوه دلبری از دلبر ما خواهد داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این نزاکت که از آن دست من امشب دیدم</p></div>
<div class="m2"><p>دستی از دور بر آتش زحنا خواهد داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر تو دوری نکنی این همه واعظ از دوست</p></div>
<div class="m2"><p>دوری خود بتو کی دوست روا خواهد داشت</p></div></div>