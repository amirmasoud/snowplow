---
title: >-
    شمارهٔ ۳۷۸
---
# شمارهٔ ۳۷۸

<div class="b" id="bn1"><div class="m1"><p>گر نپرسد حالم آن نامهربان غمخوار وار</p></div>
<div class="m2"><p>در دهان تنگ او جا نیست یک گفتاروار!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کند عناب آن لب چاره صد خسته دل؟</p></div>
<div class="m2"><p>عالمی بیمار و، شربت نیست یک بیمار وار!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه دل کندن ز بزم یاد جانان مشکلست</p></div>
<div class="m2"><p>آهم از لب باز میگردد بدل طوماروار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میکنم خالی ز بیرحمی دل او را بعجز</p></div>
<div class="m2"><p>گر امانم میدهد آن غمزه یک دیدار وار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسکه نقد هستیم خرج گداز عشق شد</p></div>
<div class="m2"><p>پیش جانان عرض حالم نیست یک اظهار وار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رشته سان آن دسته گل را بگرد سر نگشت</p></div>
<div class="m2"><p>میخلد مد نگه در دیده من خاروار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با دهان تنگ او کرده است تا نسبت درست</p></div>
<div class="m2"><p>نقطه یی هرجاست، گردم گرد او پرگار وار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا مگر یابند ره در بزم آن شیرین سخن</p></div>
<div class="m2"><p>دست صد حرفست و، دامان زبان طومار وار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در میان جز گفتگویی نیست ز آن موی کمر</p></div>
<div class="m2"><p>گرد آن کافر بسی گردیده ام زنار وار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جسم را تنها نیفگنده است از پا عشق او</p></div>
<div class="m2"><p>هوش ما را نیز قوت نیست یک رفتار وار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیگر آن شوخ تغافل پیشه نام من نبرد</p></div>
<div class="m2"><p>پیش او حرف وجودم نیست یک تکرار وار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آتش شرمم گدازد، ور نه جرم عالمی</p></div>
<div class="m2"><p>نیست پیش رحمت عامش یک استغفار وار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سایه وش باید که آسایند از او افتادگان</p></div>
<div class="m2"><p>هر که دم از سربلندی میزند دیوار وار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>میشود صیت بزرگی در جهان از وی بلند</p></div>
<div class="m2"><p>هر که میگوید سخن با نیک و بد کهسار وار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عرض حال خویش را واعظ بخاموشی گذار</p></div>
<div class="m2"><p>از حقارت مطلب ما نیست یک اظهاروار</p></div></div>