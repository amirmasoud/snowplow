---
title: >-
    شمارهٔ ۴۶۲
---
# شمارهٔ ۴۶۲

<div class="b" id="bn1"><div class="m1"><p>قامتم گردید چون قلاب، از یاد اجل</p></div>
<div class="m2"><p>میکند صیدم باین قلاب صیاد اجل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حواس و از قوی، دیگر بما چیزی نماند</p></div>
<div class="m2"><p>خرمن تن رفته رفته، رفت بر باد اجل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون گران شد گوش، واکن چشم ازین خواب گران</p></div>
<div class="m2"><p>این گرانی، هست در گوش تو فریاد اجل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قد چو خم شد، گردن تسلیم میباید کشید</p></div>
<div class="m2"><p>بر تو باشد قد خم، شمشیر جلاد اجل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخت سستی گشته زور آور ز پیری، وقت شد</p></div>
<div class="m2"><p>وارهیم از زحمت این تن، به امداد اجل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست وقت آنکه فکر خود کنم واعظ، ولی</p></div>
<div class="m2"><p>کرده ام خود را فراموش از غم یاد اجل</p></div></div>