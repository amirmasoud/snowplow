---
title: >-
    شمارهٔ ۲۷۲
---
# شمارهٔ ۲۷۲

<div class="b" id="bn1"><div class="m1"><p>نه کوه آن سرین تنها برآن موی کمر لرزد</p></div>
<div class="m2"><p>که هر عضوش ز خوبی بر سر عضو دگر لرزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برنگ شاخ گل در زیر با از نازکی ترسم</p></div>
<div class="m2"><p>که از گرد سرش گردیدنم، آن شاخ زر لرزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سبک هرچند آیم در نظر، باز غمی دارد</p></div>
<div class="m2"><p>که از دوش دلش گر افگنم، کوه و کمر لرزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نجنبد برگ رنگ از گلشن رخسار او، اما</p></div>
<div class="m2"><p>رگ سنگ از نسیم آه من، چون شاخ تر لرزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برای سیم وزر لرزند ابنای زمان واعظ</p></div>
<div class="m2"><p>عجب نبود دل ما بر سر آن سیمبر لرزد</p></div></div>