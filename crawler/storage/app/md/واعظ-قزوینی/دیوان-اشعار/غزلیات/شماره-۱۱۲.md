---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>با نازکی حسن تو، کی تاب حجاب است</p></div>
<div class="m2"><p>برروی تو، افروختن چهره نقاب است!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آتش آن چهره، دل سنگ گدازد</p></div>
<div class="m2"><p>تا دیده ترا، خانه آیینه خراب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لبریز طراوت شده از بس گل رویت</p></div>
<div class="m2"><p>دیوار چمن تا مژه خار در آب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیلاب شود بسکه تراود ز تو خوبی</p></div>
<div class="m2"><p>زین واقعه دارد خبر آن دل که خراب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدن رخش و، چاک بدامن نرساندن</p></div>
<div class="m2"><p>واعظ بده انصاف، که در بند نقاب است؟</p></div></div>