---
title: >-
    شمارهٔ ۳۱۴
---
# شمارهٔ ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>ز بس نگار من از خویش هم حجاب کند</p></div>
<div class="m2"><p>نظر در آینه مشکل که بی نقاب کند!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تراست چهره به کیفیتی که می ترسم</p></div>
<div class="m2"><p>که باده رنگ ترا آب در شراب کند!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چگونه تاب نظر بازی نگاه آرد</p></div>
<div class="m2"><p>رخی که از عرق شرم خود حجاب کند؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز پرادائی چشم سیاه او، چه عجب</p></div>
<div class="m2"><p>نگاه را به تکلم اگر حساب کند؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نداندم دل، اگر قدر نعمت دردت</p></div>
<div class="m2"><p>خدا بآتش بیدردی اش عذاب کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از درازی مژگانت این گمان دارم</p></div>
<div class="m2"><p>ترا بسایه خود فارغ از نقاب کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بسکه ذوق خود آرایی اش برای من است</p></div>
<div class="m2"><p>چه سوی آینه بیند، به من حساب کند!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بسکه برده فراق رخت ز من آرام</p></div>
<div class="m2"><p>فسانه ام نتواند ترا بخواب کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گذشت عمر و، ز هم ریخت قصر تن واعظ</p></div>
<div class="m2"><p>گذار سیل بهر جا فتد، خراب کند</p></div></div>