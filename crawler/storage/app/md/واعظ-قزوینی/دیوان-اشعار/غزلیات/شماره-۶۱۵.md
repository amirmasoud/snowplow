---
title: >-
    شمارهٔ ۶۱۵
---
# شمارهٔ ۶۱۵

<div class="b" id="bn1"><div class="m1"><p>نتواندم بعشوه جهان کرد رهزنی</p></div>
<div class="m2"><p>از مال کرده لذت درویشیم غنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با قامت خمیده پیران اشارتی است</p></div>
<div class="m2"><p>یعنی بود کمال بزرگی فروتنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز خوی خوش که مانع آسیب دشمن است</p></div>
<div class="m2"><p>از جامه حریر که دیده است جوشنی؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بزم یار دود، دلم ز آن بلند شد</p></div>
<div class="m2"><p>تا مهر و ماه را نگذارد بروزنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هستند عالمی چو گرفتار درد من</p></div>
<div class="m2"><p>راز دلم ز کیست ندانم نهفتنی؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لذات نفس را همه دیدیم پشت و رو</p></div>
<div class="m2"><p>واعظ کناره بود از آنها گزیدنی</p></div></div>