---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ای نام دلگشای تو عنوان کارها</p></div>
<div class="m2"><p>خاک در تو، آب رخ اعتبارها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید و مه دو قطره باران فیض تو</p></div>
<div class="m2"><p>مدی ز جنبش قلمت روزگارها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باشد شفق ز بیم تو هر شام بر فلک</p></div>
<div class="m2"><p>رنگ پریده‌ای ز رخ لاله‌زارها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انگشتی از برای شهادت شود بلند</p></div>
<div class="m2"><p>سروی که قد کشد ز لب جویبارها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بهر خواندن رقم قدرتت بهار</p></div>
<div class="m2"><p>اوراق گل شمرده به انگشت خارها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لطفت برات روزی مردم نوشته است</p></div>
<div class="m2"><p>با خط سبز بر ورق کشتزارها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیوانه خیال تو هرجا که پا نهد</p></div>
<div class="m2"><p>ریزد ز شور عشق تو، طرح بهارها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان داده‌اند راهروان بس که در غمت</p></div>
<div class="m2"><p>هر سنگ در رهت شده سنگ مزارها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>موج سراب نیست، که در جستجوی تو</p></div>
<div class="m2"><p>افتاده اند از پی هم بی‌قرارها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با خامه کی توان ره وصف تو قطع کرد؟</p></div>
<div class="m2"><p>منزل کجا و، رهروی نی سوارها؟!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راه ثنای ذات تو را چون روم؟ که من</p></div>
<div class="m2"><p>دارم به دوش از گنه خویش بارها!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر بایدم کشید ندامت به قدر جرم</p></div>
<div class="m2"><p>خوش کوتهست مدت این روزگارها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون آوری به حشر من روسیاه را</p></div>
<div class="m2"><p>از نسبتم شوند خجل شرمسارها!</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>واعظ اگر چه نیست امیدم به خویشتن</p></div>
<div class="m2"><p>دست من است و، دامن امیدوارها</p></div></div>