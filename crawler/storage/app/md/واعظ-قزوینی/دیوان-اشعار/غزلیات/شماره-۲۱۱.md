---
title: >-
    شمارهٔ ۲۱۱
---
# شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>در چشم شناسای ره و رسم امانت</p></div>
<div class="m2"><p>دزدیدن گردن بود از تیغ خیانت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پوشیده نظر، دیده ور از یاری مردم</p></div>
<div class="m2"><p>کور است که دارد ز عصا چشم اعانت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک فایده خواری ما اینکه عزیزان</p></div>
<div class="m2"><p>دیگر نتوانند بما داد اهانت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چیزی به بها کس ندهد جنس گران را</p></div>
<div class="m2"><p>ای خصم به ما این همه مفروش متانت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مالت بود از وارث، از آن رو نکنی صرف</p></div>
<div class="m2"><p>ای خواجه ممسک، به تو ختم است دیانت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واعظ به نفهمیدگی خویش کن اقرار</p></div>
<div class="m2"><p>با لاف فطانت نشود جمع فطانت</p></div></div>