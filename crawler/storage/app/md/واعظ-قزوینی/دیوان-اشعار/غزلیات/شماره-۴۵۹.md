---
title: >-
    شمارهٔ ۴۵۹
---
# شمارهٔ ۴۵۹

<div class="b" id="bn1"><div class="m1"><p>من با نگاه عجز و، تو دل سخت تر ز سنگ</p></div>
<div class="m2"><p>هرگز نبسته طرف خدنگ نظر ز سنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سازی اگر حجاب خود آیینه را، بجاست</p></div>
<div class="m2"><p>دارد ضرور باغ جمال تو در ز سنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شوق خاک بوسی نعل سمند تو</p></div>
<div class="m2"><p>با سر برون دوند گروه شرر ز سنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بهر سیب آن ذقن، از خلق دیده ام</p></div>
<div class="m2"><p>ظلمی که شاخ دیده برای ثمر ز سنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تیغ موج حادثه آبگون سپهر</p></div>
<div class="m2"><p>بر سر کشیده اند شررها سپر ز سنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرمی بخلق، سخت پناهی است خلق را</p></div>
<div class="m2"><p>هرگز ندیده پنبه چو مینا ضرر ز سنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نازک چو شیشه چون نشود دل ترا؟ که هست</p></div>
<div class="m2"><p>خو گرم تر ز آتش و، دل سخت تر ز سنگ!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لطف از کسان بجوی و، شرارت ز ناکسان</p></div>
<div class="m2"><p>آب از گهر طلب کن و، آتش ببر ز سنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>واعظ مخواه پاکی گوهر ز بدگهر</p></div>
<div class="m2"><p>هرگز کسی نخواسته آب گهر ز سنگ</p></div></div>