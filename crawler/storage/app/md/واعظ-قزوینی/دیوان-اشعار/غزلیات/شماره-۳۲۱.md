---
title: >-
    شمارهٔ ۳۲۱
---
# شمارهٔ ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>آشنایی بتو عیب است که بیگانه کند!</p></div>
<div class="m2"><p>کیست شمشاد که گیسوی ترا شانه کند؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بند غم هر که کشد، قدر رهایی داند</p></div>
<div class="m2"><p>عاقلم کرده از آن عشق، که دیوانه کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن چه مژگان دراز است، که گر خواباند</p></div>
<div class="m2"><p>میتواند مه من زلف بآن شانه کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن زمان عاشق سودازده غم نشناسد</p></div>
<div class="m2"><p>کآشنایی تواش از همه بیگانه کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز گردد، باسیران تو چون شام سیاه</p></div>
<div class="m2"><p>پنجه مهر اگر زلف ترا شانه کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میتواند به نگاهت سر راهی گیرد</p></div>
<div class="m2"><p>عشق اگر تربیت جرأت دیوانه کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیر شمشاد که دارد بقدت نسبت دور</p></div>
<div class="m2"><p>که تواند که سر زلف ترا شانه کند؟!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو دندان بلب از حیرت رویت ما را</p></div>
<div class="m2"><p>قدم اشک بهر جا که رسد خانه کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاهی کشور آسودگی از واعظ ماست</p></div>
<div class="m2"><p>این نه کاریست که هر عاقل و فرزانه کند</p></div></div>