---
title: >-
    شمارهٔ ۲۲۹
---
# شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>ز گلشن چون براه آن سرو قد لاله رو افتد</p></div>
<div class="m2"><p>گذارد بوی گل گل را و از دنبال او افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به آن دلکش کمندان گر خرامد جانب صحرا</p></div>
<div class="m2"><p>نسیمش از قفا چون طره های مشکبو افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنند اعضاء او باهم چو قسمت هستی ما را</p></div>
<div class="m2"><p>سعادتمند چشمی، کو به آن روی نکو افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توانم با نگاهی آتش از چشمش بر آوردن</p></div>
<div class="m2"><p>اگر چشمم بچشم آن نگاه جنگجو افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امانت دار راز خود مکن جز مخزن دل را</p></div>
<div class="m2"><p>که چون از لب برون آمد، بدست گفتگو افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کدورتهای باطن، نیست ممکن بر زبان ناید</p></div>
<div class="m2"><p>خس و خاری که در آبی بود، آخر برو افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود پا در هوا حرف زبانی در دل سالک</p></div>
<div class="m2"><p>چو عکس سبزه یی کز هر طرف بر آب جو افتد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن از دل بلب تا آیدم از ناتوانیها</p></div>
<div class="m2"><p>چو طفل نو براه افتاده تا خیزد، برو افتد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بآب توبه تا رخ شویی از گرد گنه واعظ</p></div>
<div class="m2"><p>ترا پندی بود روشن، سفیدی چون به مو افتد</p></div></div>