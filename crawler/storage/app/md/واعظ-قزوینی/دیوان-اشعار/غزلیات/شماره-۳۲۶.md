---
title: >-
    شمارهٔ ۳۲۶
---
# شمارهٔ ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>امروز کس کجا ز سخن یاد میکند؟</p></div>
<div class="m2"><p>بلبل گهی روان سخن شاد میکند!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز جز دکان گدایی نمیشود</p></div>
<div class="m2"><p>هرجا که مسجدی کسی آباد میکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبود عجب ز کثرت اگر نالم این چنین</p></div>
<div class="m2"><p>از کثرتست سیل که فریاد میکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منعم که می گدازدم از منت عطا</p></div>
<div class="m2"><p>ما را به اعتقاد خود ایجاد میکند!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوری بود که غمزه به چشم پدر کند</p></div>
<div class="m2"><p>ناقابلی که فخر به اجداد میکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واعظ (همین) ز فکر سخن سود بس مرا</p></div>
<div class="m2"><p>کز فکرهای پوچم آزاد میکند</p></div></div>