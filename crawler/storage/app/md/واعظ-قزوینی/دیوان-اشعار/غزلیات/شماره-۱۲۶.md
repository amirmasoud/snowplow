---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>بر عدو پشت نکردن سپر است</p></div>
<div class="m2"><p>تیغ خونریز دلیران جگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشمرد کس هنر امروز به هیچ</p></div>
<div class="m2"><p>در شمار آنکه کنون هست، زر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوی خوش با همه کس میسازد</p></div>
<div class="m2"><p>هیزم آتش گل، چوب تر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر ما و، ره یاران عزیز</p></div>
<div class="m2"><p>پای این راه، گذشتن ز سر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست کامی که از آن حاصل نیست</p></div>
<div class="m2"><p>بهر ما نخل دعا، شاخ زر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی تعلق رود آسان ز جهان</p></div>
<div class="m2"><p>سبکی راحله این سفر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نام امروز ز مالست بلند</p></div>
<div class="m2"><p>سکه را حکم ز بالای زر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرو بستان محبت، آه است</p></div>
<div class="m2"><p>چشمه کوه غمش چشم تر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تاب نگذاشت دگر در جایی</p></div>
<div class="m2"><p>این چه تاب رخ و تاب کمر است؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هنری نیست هنرور گشتن</p></div>
<div class="m2"><p>به هنر فخر نکردن هنر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>میبرد گریه کدورت از دل</p></div>
<div class="m2"><p>صافی آینه، از چشم تر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سخن خصم، تو نشنیده شمار</p></div>
<div class="m2"><p>سپر تیغ زمان، گوش کر است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه غم از مرگ، چو یاران رفتند؟</p></div>
<div class="m2"><p>نقش پا قافله این سفر است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نفس از حرف لبت گشته چنان</p></div>
<div class="m2"><p>که نی از ناله من، نیشکر است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فکر سامان نکنی در ره عشق</p></div>
<div class="m2"><p>که در این مرحله سر در خطر است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیش این قدر مدانان، خوبی</p></div>
<div class="m2"><p>گوشوار سخن و گوش کر است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رفعت شاه ز درویشان است</p></div>
<div class="m2"><p>در هما بال فشانی ز پر است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرچه بس قدر سخن هست، ولی</p></div>
<div class="m2"><p>خامشی واعظ، حرف دگر است!</p></div></div>