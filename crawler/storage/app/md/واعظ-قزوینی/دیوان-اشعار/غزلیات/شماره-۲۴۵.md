---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>چون بهله بصید دلم آن مست برآرد</p></div>
<div class="m2"><p>نی بهله، بتاراج جهان دست برآرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خانه آن چشم نگاهش متواریست</p></div>
<div class="m2"><p>این خونی دل را که از آن بست برآرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هردم که ز راهش برد آیینه بخانه</p></div>
<div class="m2"><p>از باده دیدار، سیه مست برآرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بوی گل آن شوخ چو از پرده برآید</p></div>
<div class="m2"><p>هر راز که در پرده دل هست برآرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظالم بستم دست برآورده نترسد</p></div>
<div class="m2"><p>مظلوم هم آخر بدعا دست برآرد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واعظ چو خس و خار، سبکباریم آخر</p></div>
<div class="m2"><p>زین قلزم خونخوار گمان هست برآرد</p></div></div>