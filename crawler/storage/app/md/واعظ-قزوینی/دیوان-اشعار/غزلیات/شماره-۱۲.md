---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>بس است شمع قناعت چون مسکن ما را</p></div>
<div class="m2"><p>چرا بمهر بود چشم روزن ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشید خجلت بی برگی از خزان چندان</p></div>
<div class="m2"><p>که کرد آب عرق سبز گلشن ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیاه باد رخ مفلسی بهر دو جهان</p></div>
<div class="m2"><p>که ناامید ز ما کرد رهزن ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زخلق یاوری هم شده است رسم قدیم</p></div>
<div class="m2"><p>مگر بباد دهد برق خرمن ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زدود مشعل ظالم هم این ستم بس نیست</p></div>
<div class="m2"><p>که کرده است سیه چشم روشن ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجاست رستم همت که تا ز چاه امل</p></div>
<div class="m2"><p>برآرد این دل سخت چو بیجن ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمر نبندد الهی کمر بکین بستن</p></div>
<div class="m2"><p>که او به کینه کمر بسته دشمن ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز جان به جامه پشمین فقر تن دادیم</p></div>
<div class="m2"><p>بس است جامه فاخر همین تن ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود ز کوتهی تیشه طالب واعظ</p></div>
<div class="m2"><p>که گشته است به سر خاک معدن ما را</p></div></div>