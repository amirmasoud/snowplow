---
title: >-
    شمارهٔ ۳۴۸
---
# شمارهٔ ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>گر چنین بر ما کمان ناز پرکش می‌شود</p></div>
<div class="m2"><p>دل ز پهلوی من از تیر تو ترکش می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنگ ره ما را ز فیض ناتوانی رهبر است</p></div>
<div class="m2"><p>عینک آری چشم پیران را عصاکش می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خرد خالص نباشد، تا جنون در پرده است</p></div>
<div class="m2"><p>تا ز والا نگذرد، کی باده بی‌غش می‌شود؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم‌نشین خاکساران شو، که دارد فیض‌ها</p></div>
<div class="m2"><p>از الفت خاکستر افزون عمر آتش می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که واعظ کامرانی مایه ناکامی است</p></div>
<div class="m2"><p>خاطرم از جمع گردیدن مشوش می‌شود</p></div></div>