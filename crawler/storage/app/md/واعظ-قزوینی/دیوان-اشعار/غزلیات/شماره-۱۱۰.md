---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>نکهت از زلف کجش سودائی سر در هواست</p></div>
<div class="m2"><p>شانه در گیسوی او، دیوانه زنجیر خاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فارغ از آزار چرخ از بی وجودی گشته ام</p></div>
<div class="m2"><p>دانه من چون شرر ایمن ز سنگ آسیاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترک خود کن اول، آنگه هر چه میخواهی بخواه</p></div>
<div class="m2"><p>دست چون از خویش برداشتی دست دعاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سعی ما گرهست ناقص، فیض جانان کامل است</p></div>
<div class="m2"><p>دست ما هرچند کوتاهست، زلف او رساست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میدهد، افتادگی تسکین تندیهای خصم</p></div>
<div class="m2"><p>خاکساریها درین طوفان، چو خاک کربلاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تو لاف بی نیازی سخت باشد ناپسند</p></div>
<div class="m2"><p>تا دل از صدرنگ خواهش پر چو کشکول گداست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسکه یاران در ره حق برخلاف مقصدند</p></div>
<div class="m2"><p>هادی این راه وقت بازگشتن رهنماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر قماش جامه نازند این خودآرایان اگر</p></div>
<div class="m2"><p>بر تن ما نیز عریانی قبای ته نماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست داد امشب حنا را، رخصت پابوس او</p></div>
<div class="m2"><p>از حنا کمتر نه یی ای گریه، وقت دست و پاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون ز خود بیرون نهی پا، وقت عرض حاجتست</p></div>
<div class="m2"><p>از تو چون خالی شد آغوش تو، محراب دعاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خلق کن با سائلان، نبود عطا گر دسترس</p></div>
<div class="m2"><p>روی خندان از کریمان نایب دست سخاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خواب آسایش اگر خواهی کنی واعظ دمی</p></div>
<div class="m2"><p>بی سرانجامیت بالین، خاکساری متکاست</p></div></div>