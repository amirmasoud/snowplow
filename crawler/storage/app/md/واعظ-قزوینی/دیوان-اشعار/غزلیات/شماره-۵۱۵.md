---
title: >-
    شمارهٔ ۵۱۵
---
# شمارهٔ ۵۱۵

<div class="b" id="bn1"><div class="m1"><p>زیاده فیض توان از شکستگان بردن</p></div>
<div class="m2"><p>که عطر گل شود افزون بوقت پژمردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود بخانه تاریک با چراغ شدن</p></div>
<div class="m2"><p>ز فیض بندگی دوست، زنده دل مردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکستگی است، نشان درستی ایمان</p></div>
<div class="m2"><p>دیانتی نبود چون بخویش نسپردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل از فشارش غم، به برون دهد معنی</p></div>
<div class="m2"><p>بعین ریزش اشکست چشم افشردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر حیات چنین مرده مرده زیستن است؟</p></div>
<div class="m2"><p>برای آمدن مرگ، میتوان مردن!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همین بسست ز الوان نعمتت واعظ</p></div>
<div class="m2"><p>که نان خویش توانی بخون دل خوردن</p></div></div>