---
title: >-
    شمارهٔ ۴۳۶
---
# شمارهٔ ۴۳۶

<div class="b" id="bn1"><div class="m1"><p>به پنج روزه حیاتست اعتبار غلط</p></div>
<div class="m2"><p>نه یک غلط دو غلط، بلکه صد هزار غلط!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گذشت آنکه شکفتی دل از بهار و خزان</p></div>
<div class="m2"><p>بود دگر طمع آن ز روزگار غلط!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طلب مکن ز جوانان کمال پیران را</p></div>
<div class="m2"><p>که هست خواستن میوه از بهار غلط!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدست باز فراغ و، بدشت صید عمل</p></div>
<div class="m2"><p>بود گذشتن ازین دشت، بی شکار غلط!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسید سیل علایق عنان گسسته و، هست</p></div>
<div class="m2"><p>متاع دل نکشیدن به یک کنار غلط!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود معارضه با ما به یک دو مصرع پوچ</p></div>
<div class="m2"><p>چو تاختن به صف از طفل نی سوار غلط!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زباغ عمر گل بندگی بچین واعظ</p></div>
<div class="m2"><p>نشستن است دگر دست در نگار غلط</p></div></div>