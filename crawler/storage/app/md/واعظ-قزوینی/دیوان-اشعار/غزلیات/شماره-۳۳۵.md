---
title: >-
    شمارهٔ ۳۳۵
---
# شمارهٔ ۳۳۵

<div class="b" id="bn1"><div class="m1"><p>سخن سیم و زر و خانه و اسباب بود</p></div>
<div class="m2"><p>سخنی فی المثل امروز اگر باب بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکجا بگذرد آب سخن سیم و زری</p></div>
<div class="m2"><p>صد دل آویخته هر سوی چو دولاب بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قبله طاعت این قوم، طلای دو تبی است</p></div>
<div class="m2"><p>طاق درهای خسان، نایب محراب بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرده کشتن اویند جهانی ز حسد</p></div>
<div class="m2"><p>هر که سر زنده درین عهد چو سیماب بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست گر بلد، ای خانه خرابی چو حباب</p></div>
<div class="m2"><p>در ویرانه ام، از کوچه سیلاب بود!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس نداند ره ویرانه ام از گمنامی</p></div>
<div class="m2"><p>زآن تهی، کلبه ام از پرتو مهتاب بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیر خر مهره ندانند خرانش ز خری</p></div>
<div class="m2"><p>سخن واعظ اگر چه گهر ناب بود</p></div></div>