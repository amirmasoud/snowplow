---
title: >-
    شمارهٔ ۱۹۶
---
# شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>خرام ناز تو، معمار شهر ویرانیست</p></div>
<div class="m2"><p>نگاه، مفتی آیین نامسلمانیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شیوه های تو ای شاه ملک استغنا</p></div>
<div class="m2"><p>کسی که ملتفت ماست، چین پیشانیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب نباشد، اگر گشته چشم دلها سیر</p></div>
<div class="m2"><p>که دست زلف تو در کیسه پریشانیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی ز خاک نشینان کوی تست مگر</p></div>
<div class="m2"><p>که عطف دامن گردون ز صبح نورانیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشیده است مگر تیغ، کوه تمکینت</p></div>
<div class="m2"><p>که آفتاب ز حیرت چو چشم قربانیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان ناز تو صبر ما مگر جنگ است</p></div>
<div class="m2"><p>که باز چشم سخنگوی، در رجز خوانیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدان ز سیل بلا واعظ اضطراب مرا</p></div>
<div class="m2"><p>تپیدن دل من در تلاش ویرانیست</p></div></div>