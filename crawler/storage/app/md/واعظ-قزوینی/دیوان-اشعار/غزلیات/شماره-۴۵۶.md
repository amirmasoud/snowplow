---
title: >-
    شمارهٔ ۴۵۶
---
# شمارهٔ ۴۵۶

<div class="b" id="bn1"><div class="m1"><p>از یار بد چه نقص، بخوش طینتان پاک؟</p></div>
<div class="m2"><p>تن گر شود پلید، چه نقصان بجان پاک؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارد کلام پاکدلان بیشتر اثر</p></div>
<div class="m2"><p>زور خدنگ بیش بود از کمان پاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر خصم، بی درشت سخن، کارگر تراست</p></div>
<div class="m2"><p>زو داد خود بگیر بتیغ زبان پاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دم گزندی از سگ نفست رسد بدل</p></div>
<div class="m2"><p>افتاده این پلید ترا خوش بجان پاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز و شبت بپاکی تن پاک صرف شد</p></div>
<div class="m2"><p>این جسم پاک را نسزد جز روان پاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با آب دیده دامن پاکی بدست آر</p></div>
<div class="m2"><p>چیزی نبسته است بدست و دهان پاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پاکیزگی وضع، بود روزی حلال</p></div>
<div class="m2"><p>نان پلید چند بدستار خوان پاک؟!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل خانه خداست، نه جای غم جهان</p></div>
<div class="m2"><p>بیرون بر این پلیدی، ازین آستان پاک!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برخاک غلتد آب ز پاکیزه گوهری</p></div>
<div class="m2"><p>ندهد ز دست خاک نهادی روان پاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای دل بساز با مزه روزی حلال</p></div>
<div class="m2"><p>محتاج هیچ نانخورشی نیست نان پاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>واعظ ترا باین همه آلودگی مگر</p></div>
<div class="m2"><p>بخشد خدا بآب رخ دیدگان پاک</p></div></div>