---
title: >-
    شمارهٔ ۳۱۶
---
# شمارهٔ ۳۱۶

<div class="b" id="bn1"><div class="m1"><p>چون به محفل رخ فرو زد، رنگ صهبا بشکند</p></div>
<div class="m2"><p>چون به گلشن قد فرازد، شاخ گلها بشکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتاب از رشک خواهد کاستن چون ماه، اگر</p></div>
<div class="m2"><p>همچو مه طرف کلاه آن ماه سیما بشکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار بد مست است و، می گستاخ می بوسد لبش</p></div>
<div class="m2"><p>کاسه می ترسم آخر بر سر ما بشکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش عقد گوهر او دم زند گر از صفا</p></div>
<div class="m2"><p>خنده اش دندان در، در کام دریا بشکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر زند آیینه دامن آتش آن چهره را</p></div>
<div class="m2"><p>رنگ در رخسار مهر عالم آرا بشکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسکه آگاهست از درد دل ما خستگان</p></div>
<div class="m2"><p>رنگ ما در روی آن آیینه سیما بشکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بشکفد دلهای یاران، چون رود زاهد ز بزم</p></div>
<div class="m2"><p>باغ گلریزان کند، وقتی که سرما بشکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از شکست دشمن خود، دل بدرد آید مرا</p></div>
<div class="m2"><p>میخلد در خاطرم، خاری که در پای بشکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهل خردان، کی شود حلم بزرگان را حریف؟!</p></div>
<div class="m2"><p>تندی سیلاب را، تمکین دریا بشکند!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون حباب از بسکه پرگردیده از باد غرور</p></div>
<div class="m2"><p>کاسه سر ترسم آخر بر سر ما بشکند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در حصارند از حوادث روز و شب سرگشتگان</p></div>
<div class="m2"><p>کشتی گرداب، کی از موج دریا بشکند؟!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتمش: مشکن دل پر درد واعظ را زجور</p></div>
<div class="m2"><p>ترسم آن بیدرد آخر حرف ما را بشکند</p></div></div>