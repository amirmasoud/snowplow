---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>قد خم شد و افتاد جهان از نظر ما</p></div>
<div class="m2"><p>واشد سر و سامان هوسها، ز سر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تن حرکت نیست، بجز گردش رنگم</p></div>
<div class="m2"><p>دیگر سفر هند حنا شد سفر ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون غنچه، زر ما گره مشت ندیده است</p></div>
<div class="m2"><p>صندوق بگو کیسه ندوزد به زر ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در باغ سخاوت نتوان کامرسش گفت</p></div>
<div class="m2"><p>خود را چو به کامی نرساند ثمر ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را به املهای جهان، بستگیی نیست</p></div>
<div class="m2"><p>چون قطره به تن رشته نگیرد گهر ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرده است زخود بیخبرم، یاد عزیزان</p></div>
<div class="m2"><p>با آنکه نگیرند عزیزان خبر ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واعظ ندهد غیر گل آتشی از وی</p></div>
<div class="m2"><p>خاری که زند دست به دامان تر ما</p></div></div>