---
title: >-
    شمارهٔ ۱۲۷
---
# شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>دل با توکلست، گرم کیسه بی زر است</p></div>
<div class="m2"><p>گر دست مفلس است، ولی دل توانگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد توانگری نه همین جمع ملک و مال</p></div>
<div class="m2"><p>بر دادن است هرکه توانا، توانگر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاس ادب بدار، که دندان کودکان</p></div>
<div class="m2"><p>کم عمر از گزیدن پستان مادر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون مو سفید گشت، دگر وقت عیش نیست</p></div>
<div class="m2"><p>آیینه در کف تو کنون به ز ساغر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با صد هنر به جامه بود خلق را نظر</p></div>
<div class="m2"><p>لیلی نشسته، چشم تو مجنون زیور است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واعظ که کرد عیب بتر دامنی مرا</p></div>
<div class="m2"><p>دامان حشر نیز ز کردار او تراست</p></div></div>