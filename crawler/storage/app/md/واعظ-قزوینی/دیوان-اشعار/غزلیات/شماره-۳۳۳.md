---
title: >-
    شمارهٔ ۳۳۳
---
# شمارهٔ ۳۳۳

<div class="b" id="bn1"><div class="m1"><p>دردمندان، بسکه رم از خودنمایی میکنند</p></div>
<div class="m2"><p>ناله های ما تلاش نارسایی میکنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیک و بد را باهم الفت نیست بیش از یک دو روز</p></div>
<div class="m2"><p>درد و صاف باده زود از هم جدایی میکنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نفس هرکس که از میگریزد یار ماست</p></div>
<div class="m2"><p>دشمنند آنان که با ما آشنایی میکنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که از جمعیت زر میکنی چون شعله رقص</p></div>
<div class="m2"><p>این شررها آخر از آتش جدایی میکنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عرصه گیتی،بود مانند فانوس خیال</p></div>
<div class="m2"><p>هر زمان در وی گروهی خودنمایی میکنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میکنم از شکوه منع خویشتن واعظ، ولی</p></div>
<div class="m2"><p>در شکایت، ناله ها، خوش بیحیایی میکنند!</p></div></div>