---
title: >-
    شمارهٔ ۵۶۶
---
# شمارهٔ ۵۶۶

<div class="b" id="bn1"><div class="m1"><p>با من همیشه درد تو دارد عنایتی</p></div>
<div class="m2"><p>صد شکر کز غم تو، ندارم شکایتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افتاده‌اند، از ره افتادگی به دور</p></div>
<div class="m2"><p>یا رب به خویش‌گمشدگان را هدایتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از مال و جاه، هست سخن پایدارتر</p></div>
<div class="m2"><p>از ملک جم چه مانده به غیر از حکایتی؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوق جمال دوست مرا در دل حزین</p></div>
<div class="m2"><p>چون نعمت بهشت ندارد نهایتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را ز طرّه تو پریشان‌تر است حال</p></div>
<div class="m2"><p>داریم از نگاه تو چشم رعایتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دزدیده می‌توان ز رخش دیدنی ربود</p></div>
<div class="m2"><p>ترسم شکست رنگ نماید سعایتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هستیم خسته، مرهم لطفی، ترحمی</p></div>
<div class="m2"><p>گشتیم سرمه، گوشه چشم عنایتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با خویش، ما حساب به وصل تو می‌کنیم</p></div>
<div class="m2"><p>پیش تو بگذرد اگر از ما حکایتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>انجام هست شکوه ما را ز هجر تو</p></div>
<div class="m2"><p>دارد شب فراق تو هم گر نهایتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمرم کجا به شکر همین می‌کند وفا</p></div>
<div class="m2"><p>کز من کسی نداشته هرگز شکایتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد پای سوده صندل درد سر وطن</p></div>
<div class="m2"><p>خوش‌تر ندیده‌ام ز غریبی ولایتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرجا که یار ماست، بود آن دیار ما</p></div>
<div class="m2"><p>واعظ چرا کنیم ز غربت شکایتی؟!</p></div></div>