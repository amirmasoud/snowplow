---
title: >-
    شمارهٔ ۴۱۵
---
# شمارهٔ ۴۱۵

<div class="b" id="bn1"><div class="m1"><p>تسخیر ملک فقر کن و، پادشاه باش</p></div>
<div class="m2"><p>از دستبرد لشکر غم، در پناه باش!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند مرده هوس خواب غفلتی</p></div>
<div class="m2"><p>برخیز زنده نفس صبحگاه باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیشت رهیست سخت و، تو در جامه خواب نرم</p></div>
<div class="m2"><p>برخیز و در تهیه اسباب راه باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرسو چه بلائی و خس پوش لذتیست</p></div>
<div class="m2"><p>ای دل تمام چشم و، سراپا نگاه باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا لطف حق کشد ببرت همچو کهربا</p></div>
<div class="m2"><p>خود بینوا و، نان ده مردم چو کاه باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واعظ، بس است دامن تر از گنه کنون!</p></div>
<div class="m2"><p>تر دامن از سرشک ز بیم گناه باش</p></div></div>