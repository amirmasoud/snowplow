---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>نیست غیر از خط بطلان دفتر ایام را</p></div>
<div class="m2"><p>میکند هر دور گردون حلقه چندین نام را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشته قیل و قال دنیا جانشین حرف مرگ</p></div>
<div class="m2"><p>نشنود ز آن گوش هوشت این صلای عام را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دل هر سنگ بنگر، نقش چندین کوهکن</p></div>
<div class="m2"><p>از لب هر گور بشنو حرف صد بهرام را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سده دلگیری آرد، دوستان را ناگهان</p></div>
<div class="m2"><p>می‌کنی در کار دل‌ها چند حرف خام را؟</p></div></div>