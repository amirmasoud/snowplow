---
title: >-
    شمارهٔ ۵۰۵
---
# شمارهٔ ۵۰۵

<div class="b" id="bn1"><div class="m1"><p>غم بود غم، شراب درویشان</p></div>
<div class="m2"><p>دل بود دل، کباب درویشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست رخسار حسن سیرت را</p></div>
<div class="m2"><p>زلف، از پیچ و تاب درویشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب شود طالع از سیه روزی</p></div>
<div class="m2"><p>شمع سان آفتاب درویشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو آب از شنا خورد برهم</p></div>
<div class="m2"><p>عالم از اضطراب درویشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل تنگست، در کتاب وجود</p></div>
<div class="m2"><p>نقطه انتخاب درویشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاه و بیگاه در ره طلبند</p></div>
<div class="m2"><p>کی نداند شتاب درویشان!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی طالب نیستند تا هستند</p></div>
<div class="m2"><p>خواب مرگست خواب درویشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواب ناز است مگر و بستر گور!</p></div>
<div class="m2"><p>راحت تن، عذاب درویشان!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل شب، بسکه در حساب خودند</p></div>
<div class="m2"><p>هست روز حساب درویشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ورق های پرده های دل است</p></div>
<div class="m2"><p>غنچه آسا کتاب درویشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست حرف اینکه خیزد از دلشان</p></div>
<div class="m2"><p>خون چکد از کباب درویشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غم دنیا، بمنعم ارزانی</p></div>
<div class="m2"><p>غم عشق است باب درویشان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باشد ازبهر دفع باد هوا</p></div>
<div class="m2"><p>تلخی غم گلاب درویشان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آخر عمر، از جوانبختی</p></div>
<div class="m2"><p>هست عین شباب درویشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هست نور شکسته رنگی عشق</p></div>
<div class="m2"><p>همه شب ماهتاب درویشان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا تویی در حساب خود واعظ</p></div>
<div class="m2"><p>نیستی در حساب درویشان</p></div></div>