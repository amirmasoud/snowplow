---
title: >-
    شمارهٔ ۵۲۷
---
# شمارهٔ ۵۲۷

<div class="b" id="bn1"><div class="m1"><p>ز شمع فیض تاریکی است، قانع را سرا روشن</p></div>
<div class="m2"><p>چراغ دولت است از سایه بال هما روشن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر خواهد خدا کارت بدست دشمنان سازد</p></div>
<div class="m2"><p>بزور باد میگردد چراغ آسیا روشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین پیری که با این چشم باید فکر خود دیدن</p></div>
<div class="m2"><p>ره اندیشه رفتن کن از شمع عصا روشن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چراغ راه احسانت رنگ خجلت حاجت</p></div>
<div class="m2"><p>چو روی مفلسی بیند، شود چشم شما روشن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فسردند آرزوهای جوانی، وقت پیری ها</p></div>
<div class="m2"><p>سه گردید روز شیروان، شد صبح تا روشن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کدورت بیش باشد، عیش و عشرتهای کامل را</p></div>
<div class="m2"><p>بود نقص حنا، باشد اگر رنگ حنا روشن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کدورات جهان بر زشتی آن سایه اندازد</p></div>
<div class="m2"><p>شود دل زین غبارت همچو چشم توتیا روشن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>براه انتظارش هر دم آهی میکشد گردن</p></div>
<div class="m2"><p>غباری بر نمی خیزد که گردد چشم ما روشن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر از بوی یوسف، دیده یعقوب بینا شد</p></div>
<div class="m2"><p>ز دیدار عزیزان چون نگردد چشم ما روشن؟!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غبار آورد گر چشمم ز پیری، چشم آن دارم</p></div>
<div class="m2"><p>که گردد چشم دل واعظ، مرا زین توتیا روشن</p></div></div>