---
title: >-
    شمارهٔ ۵۸۳
---
# شمارهٔ ۵۸۳

<div class="b" id="bn1"><div class="m1"><p>نیست او شاه که دارد کمر سیم و زری!</p></div>
<div class="m2"><p>اوست شه، کو بر هر سفله نبندد کمری!!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست جز خجلت از احباب تهیدستان را</p></div>
<div class="m2"><p>بید را جز عرق بید نباشد ثمری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرد امروز بنقش درم و دینار است</p></div>
<div class="m2"><p>جانب سکه صورت نکند کس نظری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ره بسر منزل وحدت، نبری از کثرت</p></div>
<div class="m2"><p>قدمی باخود اگر در ره حق همسفری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو آه سحری تا همه جا ره داری</p></div>
<div class="m2"><p>گر بغیر از در حق راه بجایی نبری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌توکل شده‌ای واعظ، از آن محتاجی</p></div>
<div class="m2"><p>نبری ره به در دوست، از آن دربه‌دری</p></div></div>