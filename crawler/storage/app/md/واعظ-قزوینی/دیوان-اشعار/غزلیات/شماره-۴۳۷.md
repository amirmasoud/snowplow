---
title: >-
    شمارهٔ ۴۳۷
---
# شمارهٔ ۴۳۷

<div class="b" id="bn1"><div class="m1"><p>ای مستی شباب، ترا کرده باب وعظ</p></div>
<div class="m2"><p>برگ خزان برای تو باشد کتاب وعظ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای آنکه برده غفلت دنیا ترا ز هوش</p></div>
<div class="m2"><p>بر روی دل چرا نفشانی گلاب وعظ؟!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمت ز خواب بیخبری وا نمیشود</p></div>
<div class="m2"><p>تا چهره حیات نشویی بآب وعظ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شد سواد موی تو روشن، میسر است</p></div>
<div class="m2"><p>آیینه بر گرفتن و، خواندن کتاب وعظ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این دیو نفس بدرگ وارونه کار را</p></div>
<div class="m2"><p>از خویش دور ساز، به تیر شهاب وعظ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شمع جا بمحفل قربت نمیدهند</p></div>
<div class="m2"><p>تا نفگنی برشته جان پیچ و تاب وعظ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با صدمه عتاب الهی چه میکنی؟</p></div>
<div class="m2"><p>از نازکی چون طبع ترا نیست تاب وعظ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیلاب گریه را نتواند روان کند</p></div>
<div class="m2"><p>از کوهسار سختی دل، جز سحاب وعظ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در مدرس زمانه، دوموگشتی و، همان</p></div>
<div class="m2"><p>نشناختی سیاه و سفید از کتاب وعظ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>واعظ، ز زهد خشک جهانی مکدر است</p></div>
<div class="m2"><p>تر کن دماغ پیر و جوان، از شراب وعظ</p></div></div>