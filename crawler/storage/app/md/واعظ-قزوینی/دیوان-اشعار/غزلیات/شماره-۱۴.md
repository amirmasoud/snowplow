---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>ز لطف دوست کی آید دریدن پرده ما را؟</p></div>
<div class="m2"><p>اگر خجلت بروی ما نیارد کرده ما را؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نباشد جز فرو رفتن ز خجلت بر زمین فردا</p></div>
<div class="m2"><p>اگر سرکوب ما خواهند کرد آورده ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همین نعمت ز نعمتهای الوان بس که همچون گل</p></div>
<div class="m2"><p>برخ چینی نباشد سفره گسترده ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مصور گر کشد تمثال ما ز آلوده دامانی</p></div>
<div class="m2"><p>عجب نبود ورق از خود فشاند کرده ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بیقدری بجز گرد یتیمی کس نمی گیرد</p></div>
<div class="m2"><p>گهرهای به خوناب جگر پرورده ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشان آن دهن را هم از آن شیرین سخن پرسم</p></div>
<div class="m2"><p>کند پیدا جواب او مگر گم کرده ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بروی عیب مردان پرده یی چون آبرو نبود</p></div>
<div class="m2"><p>میفگن بهر دنیا واعظ از رخ پرده ما را</p></div></div>