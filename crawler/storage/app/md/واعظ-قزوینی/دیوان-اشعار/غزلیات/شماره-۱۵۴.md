---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>بی فکر تو، ناپاک دل از لوث جهان است</p></div>
<div class="m2"><p>بی ذکر تو، دل خانه بی آب و روان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ما سخن خال و خط امروز بود زشت</p></div>
<div class="m2"><p>حرف گل رخسار، گل شمع زبان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برخیز که آمد به سرت مرگ سبکخیز</p></div>
<div class="m2"><p>هرچند که بر خاطرت این حرف گران است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز فتنه نزاید چو زبان شد بسخن جفت</p></div>
<div class="m2"><p>شور و شر عالم همه فرزند زبان است</p></div></div>