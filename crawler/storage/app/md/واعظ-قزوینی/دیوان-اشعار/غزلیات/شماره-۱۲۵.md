---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>رفت پیری چو ز حد، مرگ گوارنده تر است</p></div>
<div class="m2"><p>شربت مرگ درین شیر بجای شکر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میرسد قاصد پیری،ز عصا نامه بکف</p></div>
<div class="m2"><p>زندگی رفته، اجل آمده، کاینش خبر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهر آوازه مرگی، بگشا دیده ز خواب</p></div>
<div class="m2"><p>کان شب عمر ترا بانگ خروس سحر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همدمان فاتحه خوانند برای تو همه</p></div>
<div class="m2"><p>در ره مرگ، عمل با تو همین همسفر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فقر ایوان بلندیست، برآیی چو برآن</p></div>
<div class="m2"><p>پادشاهی و، جهانت همه باغ نظر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گریه از تلخی ایام، چو طفلان تا چند؟</p></div>
<div class="m2"><p>شیر مادر بودت، گر همه پند پدر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپر و جبه و جوشن، ببرت ای ظالم</p></div>
<div class="m2"><p>همه وابسته یک ناوک آه سحر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گریه از درد تو ای یار، مرا نور دو چشم</p></div>
<div class="m2"><p>آتش عشق تو ای دوست، مرا تاج سر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عیب جویان همه چشمند و زبان، گوشی نیست</p></div>
<div class="m2"><p>ورنه گفتار تو واعظ همه در و گهر است</p></div></div>