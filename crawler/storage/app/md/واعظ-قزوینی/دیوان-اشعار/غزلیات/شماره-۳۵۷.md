---
title: >-
    شمارهٔ ۳۵۷
---
# شمارهٔ ۳۵۷

<div class="b" id="bn1"><div class="m1"><p>عجب دانم، از زخم بسمل برآید!</p></div>
<div class="m2"><p>خدنگی، که از شست قاتل برآید!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگه جانب غیرم از دیده تر</p></div>
<div class="m2"><p>چو تیریست کز زخم مشکل برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان نیست باریک، راه خیالش</p></div>
<div class="m2"><p>که از عهده رفتن دل برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان تنگ بندد، به شوخی میان را</p></div>
<div class="m2"><p>که مطلب از آن شوخ، مشکل برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آهم نشد آنچنان تنگ محفل</p></div>
<div class="m2"><p>که حرفی از آن مه شمایل برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ستم آن قدر رفته بر من ز هجرش</p></div>
<div class="m2"><p>که از عهده اش رحم مشکل برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو از جای خیزی و، من از سر جان</p></div>
<div class="m2"><p>تو از خانه و، واعظ از دل برآید!</p></div></div>