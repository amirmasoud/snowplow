---
title: >-
    شمارهٔ ۱۷۷
---
# شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>پیری رسید و، از همه وقت کناره است</p></div>
<div class="m2"><p>جز جا بنام خویش سپردن چه چاره است؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر لوح بی بقائی خود، گر نظر کنی</p></div>
<div class="m2"><p>برگشتن نگاه تو عمر دوباره است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چشم میزنی بهم، از هم گسسته است</p></div>
<div class="m2"><p>گویی که تار عمر تو تار نظاره است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون مو سپید گشت، بدندان مبند دل</p></div>
<div class="m2"><p>سر زد چو صبح، وقت غروب ستاره است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیگر نمانده جای تو در دیده، ای نگاه</p></div>
<div class="m2"><p>جز جای خود بگریه سپردن چه چاره است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر مطلب از کناره گزیدن نه شهر تست</p></div>
<div class="m2"><p>بودن میان مردم عالم، کناره است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در عهد ماست زخمی خار گزندگی</p></div>
<div class="m2"><p>بیچاره یی که همچو گلشن جامه پاره است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>واعظ به فکر دوست زبان از سخن مبند</p></div>
<div class="m2"><p>جایی که دل نشسته زبان هیچکاره است</p></div></div>