---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>گداخت آتش عشق تو مغز جان مرا</p></div>
<div class="m2"><p>گشود درد تو طومار استخوان مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه آنچنان ز غمت روزگار من تلخ است</p></div>
<div class="m2"><p>که آورد بزبان غیر، داستان مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بسکه یافته ام فیض ها ز تنهایی</p></div>
<div class="m2"><p>ندیده است کسی با اثر فغان مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان ز غیر تو ای بی وفا گریزانم</p></div>
<div class="m2"><p>که در ره تو نگیرد کسی نشان مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز من نماند بغیر از غبار دل واعظ</p></div>
<div class="m2"><p>ز بس گداخت غمش جسم ناتوان مرا</p></div></div>