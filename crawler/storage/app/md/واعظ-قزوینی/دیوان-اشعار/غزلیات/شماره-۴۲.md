---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>نیست غیر از وصل آبی آتش جوش مرا</p></div>
<div class="m2"><p>مرهمی جز دوست نبود زخم آغوش مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سرم سودای جانان، بسکه پا افشرده است</p></div>
<div class="m2"><p>باده پرزور نتواند برد هوش مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد ز خامی در سر کار هوس عهد شباب</p></div>
<div class="m2"><p>تندی این آتش آخر ریخت سر جوش مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کشاکش از نهاد سخت خویشم سر بسر</p></div>
<div class="m2"><p>نیست غیر از خویش باری چون کمان دوش مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود و نابود مرا از بس به غارت برده دوست</p></div>
<div class="m2"><p>می توان پرسید ازو حرف فراموش مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می چکد خون از دم تیغ زبانها خلق را</p></div>
<div class="m2"><p>نیست واعظ هیچ پند از پنبه به، گوش مرا</p></div></div>