---
title: >-
    شمارهٔ ۲۱۰
---
# شمارهٔ ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>مفت آیین سخا را کی توان دامن گرفت؟</p></div>
<div class="m2"><p>داد حاتم گنجها از دست، تا دادن گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست راهی ملک دولت را به از افتادگی</p></div>
<div class="m2"><p>مصر را یوسف ز راه چاه افتادن گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی تواند تافت بازوی زبان قفل سکوت</p></div>
<div class="m2"><p>با خموشی میتوان داد دل از دشمن گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کور سازد چشم دل را، آرزوهای دراز</p></div>
<div class="m2"><p>میتواند رشته یی سرچشمه سوزن گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فکر دنیا، مانع سالک نگردد در طلب</p></div>
<div class="m2"><p>خانه نتواند ز رفتن سیل را دامن گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر دل، سخت دندانی نمی آید ز من</p></div>
<div class="m2"><p>می تواند شوخی طفلی، مرا از من گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جوانی کام دل واعظ بگیر از بندگی</p></div>
<div class="m2"><p>این طلب از عمر باید در سر خرمن گرفت</p></div></div>