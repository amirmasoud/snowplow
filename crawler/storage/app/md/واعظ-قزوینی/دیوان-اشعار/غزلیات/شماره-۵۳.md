---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>بستیم ز لب، در به رخ آفات زمان را</p></div>
<div class="m2"><p>کردیم امان نامه ازین مهر، زبان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مایل بستم بیش بود ظالم معزول</p></div>
<div class="m2"><p>پرزور شود، زه چو بگیرند کمان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آگاهی عامل، سبب راحت شاه است</p></div>
<div class="m2"><p>فریاد سگ، افسانه بود خواب شبان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بس بزبان آمد و از دوست نهفتیم</p></div>
<div class="m2"><p>شد جوهر آیینه، سخن لوح زبان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردم به دل سخت تو اظهار غم خویش</p></div>
<div class="m2"><p>بر سنگ زدم پیش تو این راز نهان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با دیده بینا نتوان از تو گذشتن</p></div>
<div class="m2"><p>عکس رخت آیینه کند آب روان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردد ز سخن سختی هر مرد نمایان</p></div>
<div class="m2"><p>تیر است ترازو، کشش زور کمان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیچیده به خود واعظ ما بسکه ز فکرت</p></div>
<div class="m2"><p>مشکل که بیابد سخنش راه زبان را</p></div></div>