---
title: >-
    شمارهٔ ۵۵۲
---
# شمارهٔ ۵۵۲

<div class="b" id="bn1"><div class="m1"><p>غیر محرومی رویت نبود کار نگاه</p></div>
<div class="m2"><p>نیست جز حسرت دیدار تو، دربار نگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکجا میروی ای شوخ، همان در نظری</p></div>
<div class="m2"><p>چه شبیه است خرام تو، برفتار نگاه!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه اشک از غم او ریخته ام، چون رگ لعل</p></div>
<div class="m2"><p>نتوانم گذرانید ز خون، تار نگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم لطف از تو نداریم، از آن روی، که نیست</p></div>
<div class="m2"><p>چشم بیمار تو را، طاقت آزار نگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزر لخت جگر، دیده نمی پردازد</p></div>
<div class="m2"><p>بسکه گرمست ز رخسار تو بازار نگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زور سر پنجه خورشید رخش را نازم</p></div>
<div class="m2"><p>که برون برده ز دست نظرم تار نگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غنچه شد دیده واعظ ز ادب پیش رخش</p></div>
<div class="m2"><p>شوق گو باز کند، این گره از کار نگاه</p></div></div>