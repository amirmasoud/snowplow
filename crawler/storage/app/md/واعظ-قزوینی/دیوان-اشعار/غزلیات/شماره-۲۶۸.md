---
title: >-
    شمارهٔ ۲۶۸
---
# شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>به درویشان فسون جاه و دولت درنمی‌گیرد</p></div>
<div class="m2"><p>کلاه پادشاهی گر دهندم، سر نمی‌گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ملک و مال، نتواند کسی از مرگ جان بردن</p></div>
<div class="m2"><p>اجل تا می‌رسد، جان می‌ستاند، زر نمی‌گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی کز بار منت پشت غیرت خم نمی‌سازد</p></div>
<div class="m2"><p>گر اندازند در پایش جهان را، برنمی‌گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کدورت نیست هرگز از جهان روشن‌نهادان را</p></div>
<div class="m2"><p>چو اخگر، شعله هرگز گرد خاکستر نمی‌گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود بر نیک و بد لازم رعایت راستگویان را</p></div>
<div class="m2"><p>توانگر بی‌سبب آیینه را در زر نمی‌گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تف خورشید دولت می‌گدازد استخوانم را</p></div>
<div class="m2"><p>همای فقرم ار چون سایه زیر پر نمی‌گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیاید راست هرگز الفت درویش با منعم</p></div>
<div class="m2"><p>که با هم اختلاط آب و روغن درنمی‌گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر از ننگ گرفتن کس شود واقف، دگر هرگز</p></div>
<div class="m2"><p>به گاه رزم، از دست عدو خنجر نمی‌گیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به اخلاق نکو، تسخیر دل‌ها می‌توان کردن</p></div>
<div class="m2"><p>که تا لشکر نگیرد پادشه، کشور نمی‌گیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به وقت سوختن، از هیزم تر می‌شود روشن</p></div>
<div class="m2"><p>که غیری در میان تا هست، صحبت در نمی‌گیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مزن در کار دنیا طعنه بی‌جوهری بر وی</p></div>
<div class="m2"><p>که از آزادگی واعظ به خود جوهر نمی‌گیرد</p></div></div>