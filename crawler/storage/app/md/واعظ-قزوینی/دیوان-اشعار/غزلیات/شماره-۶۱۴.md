---
title: >-
    شمارهٔ ۶۱۴
---
# شمارهٔ ۶۱۴

<div class="b" id="bn1"><div class="m1"><p>پیری رسید و نور نظر گشت رفتنی</p></div>
<div class="m2"><p>از عینک است چشم ترا خانه روشنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس گزیده است مرا نیش هر زبان</p></div>
<div class="m2"><p>لرزم بخویش پوشم اگر جامه سوسنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نتوان شدن بر اوج شرف، جز بپشت پا</p></div>
<div class="m2"><p>شد سر بلند سرو، ز بر چیده دامنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افگنده اند بهر تو چون خاک بستری</p></div>
<div class="m2"><p>تا چند فکر قالی و پشتی و سوزنی؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر عالمی بروی تو یا زند تیغ کین</p></div>
<div class="m2"><p>آنست ضربه تو که بر خویشتن زنی</p></div></div>