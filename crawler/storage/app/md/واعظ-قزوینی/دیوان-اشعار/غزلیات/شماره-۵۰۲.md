---
title: >-
    شمارهٔ ۵۰۲
---
# شمارهٔ ۵۰۲

<div class="b" id="bn1"><div class="m1"><p>خود هم ز ظلم خانه خرابند ظالمان</p></div>
<div class="m2"><p>دیوار و در ندارد، از آن خانه کمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مالی گرفته اند و، عوض عمر داده اند</p></div>
<div class="m2"><p>دنیا گران برآمده بهر ستمگران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دنیا بود چو مزبله یی پر ز بوی گند</p></div>
<div class="m2"><p>ز آن روی نیست پاک دلان را، دماغ آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهی اگر ز پای نیفتی، خموش باش</p></div>
<div class="m2"><p>بر نخل عمر، اره بود شمع را زبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پا بسته جهانی و، دلخوش که سالکی</p></div>
<div class="m2"><p>پا در گلی چو سرو و، کنی نام خود روان؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست ستم به گوشه نشینان نمیرسد</p></div>
<div class="m2"><p>تا حلقه بود زور ندید از کسی کمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک حرف نشنویم که باشد شنیدنی</p></div>
<div class="m2"><p>محفل پر از زبان و، سخن نیست در میان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود ناگرفته پند، مده پند دیگری</p></div>
<div class="m2"><p>پیکان به تیر جا کند، آنگاه بر نشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>واعظ چو گوش حق شنوی نیست، خود چو گل</p></div>
<div class="m2"><p>هم گوش گشته ایم درین بزم و، هم زبان!</p></div></div>