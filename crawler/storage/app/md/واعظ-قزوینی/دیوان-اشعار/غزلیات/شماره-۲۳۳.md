---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>بکش تیغ ای ستمگر تا جهانی جان به کف گردد</p></div>
<div class="m2"><p>کمان بردار، تا خورشید نارنج هدف گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن رو درج دل در دامن این دشت نگشایم</p></div>
<div class="m2"><p>که می‌ترسم گرامی‌گوهر غم‌ها تلف گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به همواری نصیحت بیش در دل‌ها اثر دارد</p></div>
<div class="m2"><p>ز نرمی قطره باران، در گوش صدف گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز فکر این غزل آمد به یادم درگه شاهی</p></div>
<div class="m2"><p>که سنگ از فیض خاک درگهش در نجف گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ظاهر گرچه پروردم، ز خاک درگهش واعظ</p></div>
<div class="m2"><p>دل دیوانه‌ام در بر مجنون جان به کف گردد</p></div></div>