---
title: >-
    شمارهٔ ۵۲۱
---
# شمارهٔ ۵۲۱

<div class="b" id="bn1"><div class="m1"><p>با چراغ دل ازین ظلمت سرا باید شدن</p></div>
<div class="m2"><p>شمع سان سر تا بپا چشم و عصا باید شدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد چو دندانی مرا از عقد دندان ها جدا</p></div>
<div class="m2"><p>گشت معلومم که از یاران جدا باید شدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم و گوش و فکر و هوش و شوق و ذوق و دست و پا</p></div>
<div class="m2"><p>پیش رفتند و، مرا نیز از قفا باید شدن!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گنده و، مردار و، کرم افتاده و، خاک سیاه!</p></div>
<div class="m2"><p>دانی ای نازک بدن، آخر چها باید شدن؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ننالم همچو نی، یک بند از داغ فراق</p></div>
<div class="m2"><p>بند بندم را، ز یکدیگر جدا باید شدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنگنای خانه دل را غم مردن بس است</p></div>
<div class="m2"><p>از برای زندگی، غمگین چرا باید شدن؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بار سنگین است، باید جمله تن شد ترک سر</p></div>
<div class="m2"><p>راه پر سنگ است، یکسر پشت پا باید شدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غیرت راه محبت، بر نمی تابد رفیق</p></div>
<div class="m2"><p>همرهی گر بایدت، از غم دوتا باید شدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیم و زر از خاکساری افسر شاهان شوند</p></div>
<div class="m2"><p>تاج سر خواهی که باشی، خاک پا باید شدن!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گریه اطفال، واعظ وقت زاییدن ز چیست؟</p></div>
<div class="m2"><p>زین که ساکن در جهان بی بقا باید شدن</p></div></div>