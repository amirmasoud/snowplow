---
title: >-
    شمارهٔ ۲۰۸
---
# شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>سوی یار از همه پرداخته میباید رفت</p></div>
<div class="m2"><p>گر همه رنگ بود، باخته میباید رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناقه عزم ضعیف است دو محمل نکشد</p></div>
<div class="m2"><p>دو دل خویش یکی ساخته میباید رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ره دوست چو آبی که شود صاف و رود</p></div>
<div class="m2"><p>همرهان را همه انداخته میباید رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کارم از دست شد ای قاصد آه سحری</p></div>
<div class="m2"><p>تا دل او همه جا تاخته می باید رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تماشای جمالش همه کس محرم نیست</p></div>
<div class="m2"><p>خویش را از نظر انداخته میباید رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشت چون صیقل آیینه قد از فکر بنا</p></div>
<div class="m2"><p>خانه گردید چو پرداخته میباید رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مصافی که عدو تیغ کجی افرازد</p></div>
<div class="m2"><p>علم راستی افراخته میباید رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپر از دوره آغوش بود مردان را</p></div>
<div class="m2"><p>بردم تیغ اجل، تاخته میباید رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جانب سنگ بلا ای ثمر باغ وجود</p></div>
<div class="m2"><p>چون شکوفه سپر انداخته میباید رفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تن به خاکستر واسوختگی باید داد</p></div>
<div class="m2"><p>همچو آیینه پرداخته میباید رفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جانب اهل کرم، دست تهی بال و پر است</p></div>
<div class="m2"><p>سوی جانان همه را باخته میباید رفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>واعظ این بار علایق که تو برداشته یی</p></div>
<div class="m2"><p>همه را عاقبت انداخته میباید رفت</p></div></div>