---
title: >-
    شمارهٔ ۴۴۴
---
# شمارهٔ ۴۴۴

<div class="b" id="bn1"><div class="m1"><p>از هجوم داغ، در تن نیست دیگر جای داغ</p></div>
<div class="m2"><p>مینهم چون فلس ماهی، داغ بر بالای داغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد و رفت خیال دوست را، نتوان نهفت</p></div>
<div class="m2"><p>نقش پای یاد جانان است در دل جای داغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشک خونین گرددش در چشم، از سرگرمیم</p></div>
<div class="m2"><p>بر سر شوریده ام شبها رسد چون پای داغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوچه آمد شد درد است، در دل زخم تیر؛</p></div>
<div class="m2"><p>ناخن سر پنجه عشق است، در تن جای داغ!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما خریداران سوداییم در بازار عشق</p></div>
<div class="m2"><p>نیست واعظ درهم و دینار ما، جز جای داغ</p></div></div>