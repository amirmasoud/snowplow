---
title: >-
    شمارهٔ ۲۰۶
---
# شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>شب ز دردم، جمله دریا حق گذشت</p></div>
<div class="m2"><p>ناله ام از گنبد ازرق گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود خونم حق آن تیر نگاه</p></div>
<div class="m2"><p>آن نگاه از خون من ناحق گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پایبند قید اطلاق ار شوی</p></div>
<div class="m2"><p>میتوان از قیدها مطلق گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر دنیا، کان خیالی باطل است</p></div>
<div class="m2"><p>مگذر از حق میتوان از حق گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قوت نطقت که با آن آدمی</p></div>
<div class="m2"><p>چون سگان در جق جق و، وق وق گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنجه شد قانع ز دنیا، تا چه ها</p></div>
<div class="m2"><p>بر حریص جاهل احمق گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خویش را کشتی ز فکر کیمیا</p></div>
<div class="m2"><p>زندگانی برتو چون زیبق گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دم مرگ آه حسرت میشود</p></div>
<div class="m2"><p>هر دمی کان نی بیاد حق گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل بدست آور، رهی تا از بلا</p></div>
<div class="m2"><p>کی توان زین بحر بی زورق گذشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میرسد از چاک دل رزق سخن</p></div>
<div class="m2"><p>چون قلم ما را مدار از شق گذشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حق یاد حق ز ما نگرفته رفت</p></div>
<div class="m2"><p>زندگی واعظ ز ما ناحق گذشت</p></div></div>