---
title: >-
    شمارهٔ ۴۸۹
---
# شمارهٔ ۴۸۹

<div class="b" id="bn1"><div class="m1"><p>چهره بگشای، که از شوق بپرواز آیم</p></div>
<div class="m2"><p>تا بود جان بتن از خود روم و، باز آیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر ز افغانم و خاموش چو ابریشم ساز</p></div>
<div class="m2"><p>ناخن دادرسی کو که به آواز آیم؟!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنم از ضعف چنان شد، که بسر منزل خویش</p></div>
<div class="m2"><p>نبرم راه ز بیهوشی اگر باز آیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبنم آسا همه تن دیده گریان گردم</p></div>
<div class="m2"><p>چون بگلگشت سراپای تو طناز آیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لطف کردی قدمی رنجه نمودی باری</p></div>
<div class="m2"><p>آن قدر باش که از خود روم و باز آیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کردم آلوده بصد عیب چو واعظ خود را</p></div>
<div class="m2"><p>تا بیاد تو ز بدگویی غماز آیم</p></div></div>