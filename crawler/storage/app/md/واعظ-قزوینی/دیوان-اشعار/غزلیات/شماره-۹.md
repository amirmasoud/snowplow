---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>ز ناله باز ندارد کسی دل ما را</p></div>
<div class="m2"><p>کسی نبسته زبان خروش دریا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ما به دلبر ما بسکه راه نزدیک است</p></div>
<div class="m2"><p>توان به بال شرر بست نامه ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوباره دیده ام امروز قد و بالایش</p></div>
<div class="m2"><p>ببین ز مستی من نشأه دو بالا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین که فشرده است یاد تمکینش</p></div>
<div class="m2"><p>که می تواند بردن ز جا دل ما را!؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کفن حریر و طلا مرده را از آن فکنند</p></div>
<div class="m2"><p>که قدر نیست در آن نشأه مال دینار را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان خلق کجا و جد و حال روی دهد</p></div>
<div class="m2"><p>کسی بجام ندیده است جوش صهبا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نخل شمع خزان کرده برگ ما سرزد</p></div>
<div class="m2"><p>ز نوبهار جوانی خبر نشد ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خاک می بردت آرزوی دنیا کاش</p></div>
<div class="m2"><p>تو هم بخاک بری آرزوی دنیا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرفته اند شهان شهرها اگر واعظ</p></div>
<div class="m2"><p>گرفته ناله من نیز کوه و صحرا را</p></div></div>