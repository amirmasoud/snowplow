---
title: >-
    شمارهٔ ۲۸۷
---
# شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>بریدن از جهان، سرمایه ارزندگی باشد</p></div>
<div class="m2"><p>که افزون قیمت شمشیر، از برندگی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخستین زینت مردان، بود پاکی ز هر نقشی</p></div>
<div class="m2"><p>که لوح ساده، سرلوح کتاب زندگی باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نداری جامیان خلق، اگر از اهل آزاری</p></div>
<div class="m2"><p>بیابان مرگ دایم شیر، از درندگی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز همکاری بود اهل طمع را دشمنی با هم</p></div>
<div class="m2"><p>گدا مردود در پیش سگ از گیرندگی باشد</p></div></div>