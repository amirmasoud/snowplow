---
title: >-
    شمارهٔ ۶۲۷
---
# شمارهٔ ۶۲۷

<div class="b" id="bn1"><div class="m1"><p>ز مرگت دوستان را، آن قدرها نیست پروایی</p></div>
<div class="m2"><p>شود زین زهر، کام جملگی شیرین بحلوائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ابنای زمان، یار نوی هر روز پیدا کن</p></div>
<div class="m2"><p>که هرگز یاری امروزشان را، نیست فردایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمیجویند در خلوت بجز خواب و خور و راحت</p></div>
<div class="m2"><p>نمیگویند در صحبت، بغیر از حرف دنیائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باین وحشت فزایان، جای الفت نیست، میخواهم</p></div>
<div class="m2"><p>دلی از شهر بیزار و، دماغ سر بصحرائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسی سر در هوا و، پا به گل بودم، کنون باید</p></div>
<div class="m2"><p>سرپا در گلی از سجده، آه عرش پیمایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فتاده بر سر هم کار و، مزدور بدن کاهل</p></div>
<div class="m2"><p>غم آرام سوزی کو و، درد کار فرمایی؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو دندانی که افتد از دهن، وقت جوانیها</p></div>
<div class="m2"><p>شوم غمگین، بر آرم از دهان گر حرف بیجایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نداری در ره دنیا، جوی اندیشه فردا</p></div>
<div class="m2"><p>همه اندیشه، اما از برای رزق فردایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود یکدست مرگ جمله، گر درویش گر منعم؛</p></div>
<div class="m2"><p>چو اسکندر بری دست تهی هرچند دارایی!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کفت خالیست از دنیا و، دل پر از غم دنیا</p></div>
<div class="m2"><p>نداری گر چه دنیا واعظ، اما ز اهل دنیایی</p></div></div>