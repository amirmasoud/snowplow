---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>ز پاس آشنایی، بهره نبود خلق عالم را</p></div>
<div class="m2"><p>نمک خوردن، چو زخم از هم جدا سازد دو همدم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گرمیهای ظاهر، چشم دلسوزی مدار از کس</p></div>
<div class="m2"><p>برای اهل ماتم، دل نسوزد شمع ماتم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نباشد نقص دولت، یاری افتادگان کردن</p></div>
<div class="m2"><p>بدوش خود کشد خورشید تابان، بار شبنم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بآن رغبت که خون هم خورند این ناکسان دایم</p></div>
<div class="m2"><p>چه بودی گر دو روزی نیز خوردندی غم هم را؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز من گر دشمنان بردند مال عالمی، اما</p></div>
<div class="m2"><p>به حق دوستی گویا به من دادند عالم را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلاصی نیست از طول أمل در زندگی ممکن</p></div>
<div class="m2"><p>مگر سنگ لحد کوبد سر این مار ارقم را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هنر در عهد ما از دین گذشتن شد، نه از دنیا</p></div>
<div class="m2"><p>کنند این سرزنش پیوسته ابراهیم ادهم را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تمام عمر همراهند باهم، لیک تا کشتن</p></div>
<div class="m2"><p>همه قابیل و هابیل است نام اولاد آدم را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بس نامهربانی رسم شد، باور نمیکردم</p></div>
<div class="m2"><p>نمیدیدم اگر پهلوی هم بادام توأم را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چسان لب وا شود واعظ که در بازار عهد ما</p></div>
<div class="m2"><p>روایی نیست از جنس سخن، جز نقش درهم را؟</p></div></div>