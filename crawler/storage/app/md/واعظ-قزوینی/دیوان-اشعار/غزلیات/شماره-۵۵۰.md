---
title: >-
    شمارهٔ ۵۵۰
---
# شمارهٔ ۵۵۰

<div class="b" id="bn1"><div class="m1"><p>گذر گاهی بود گلزار گیتی، از صبا بشنو</p></div>
<div class="m2"><p>از آن رنگی نباشد هیچکس را، از هوا بشنو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدای زر شمردن، کرده کر گوش ترا منعم</p></div>
<div class="m2"><p>چنین گر نیست، بسم الله! فریاد گدا بشنو!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی آساید از رفتن دمی این عمر مستعجل</p></div>
<div class="m2"><p>نفس رفتار عمرت را بود آواز پا، بشنو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زوال عمر شد، بر خاک میباید نهادن رو</p></div>
<div class="m2"><p>زهر گلدسته خاکی تو این بانگ رسا بشنو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>«بیا روزی بخور!» باشد، خط هر سبزه یی؛ بنگر!</p></div>
<div class="m2"><p>«غم روزی مخور!»گوید، صدای آسیا بشنو!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عطا در پرده پوشیدگی، هرگز نمیگنجد</p></div>
<div class="m2"><p>بود شهرت صدای ریزش دست عطا بشنو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غزلهای خوش واعظ، نصیحت نامه ها باشد</p></div>
<div class="m2"><p>گران بر خاطرت گر نیست، جان من بیا بشنو</p></div></div>