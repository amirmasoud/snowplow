---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>لوح دنیا از خط مهر و محبت ساده است</p></div>
<div class="m2"><p>ساده تر لوح کسی کو دل بدنیا داده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داغ یاران، محنت دنیا، نفاق همدمان</p></div>
<div class="m2"><p>جمله اسباب گذشتن از جهان آماده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر غرض شهرت نباشد، راه عزلت تنگ نیست</p></div>
<div class="m2"><p>چون بود گمگشتگی مقصد، بیابان جاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میشود فردا بسی افتادگان را دستگیر</p></div>
<div class="m2"><p>چون عصا هرکس درین گلشن ز پا افتاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیستند ارباب دنیا، مالک دلهای خود</p></div>
<div class="m2"><p>هرکه را دیدیم، واعظ دل بدنیا داده است!</p></div></div>