---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>اگر نه، از گل محنت سرشته اند مرا؟</p></div>
<div class="m2"><p>چرا بجبهه خط، چین نوشته اند مرا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان ز حاصل خود غافلم، که پنداری</p></div>
<div class="m2"><p>هنوز در گل هستی نکشته اند مرا؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز باز چیدن دامان فیض، دانستم</p></div>
<div class="m2"><p>که از غبار تعلق سرشته اند مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا کشاکش غم، از تو نگسلد هرگز</p></div>
<div class="m2"><p>به پیچ و تاب خیال تو، رشته اند مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کام مردم عالم، چسان شوم شیرین؟</p></div>
<div class="m2"><p>به تلخی سخن حق، سرشته اند مرا!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چگونه خون چکدم از کباب دل واعظ؟</p></div>
<div class="m2"><p>بتان بآتش دوری برشته اند مرا؟</p></div></div>