---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>غازه را آتش از آن چهره دگر در جان است</p></div>
<div class="m2"><p>حوض آیینه ز رخسار تو گلریزان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نظر مد نگاهیست که خوابانده ز شرم</p></div>
<div class="m2"><p>چه رساییست که با قامت آن مژگان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون وفا، پیشه عشق است و جفا شیوه حسن</p></div>
<div class="m2"><p>من همه اینم و، آن شوخ سراپا آن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فیض، پروانگی محفل ما چون نکند؟</p></div>
<div class="m2"><p>که چراغش ز صفای قدم یاران است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میکند ناله جانسوز تو آخر کاری</p></div>
<div class="m2"><p>ای دل آسوده نشین، مرد تو در میدان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جهان گردد افکار تو هر سو، چه عجب؟</p></div>
<div class="m2"><p>زآنکه گفتار تو واعظ گهر غلتان است</p></div></div>