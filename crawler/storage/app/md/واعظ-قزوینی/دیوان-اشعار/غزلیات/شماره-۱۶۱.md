---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>مایه عزت، ز مردم روی پنهان کردن است</p></div>
<div class="m2"><p>از نظر خود را نهفتن، جسم را جان کردن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سالک راه خدا بودن، باین طول امل</p></div>
<div class="m2"><p>همرهی در راه با غول بیابان کردن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کتاب گل، بآب زر نوشتست این حدیث</p></div>
<div class="m2"><p>سیم وزر اندوختن بهر پریشان کردن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داشتن سوز محبت را نهان در استخوان</p></div>
<div class="m2"><p>در نیستان آتش سوزنده پنهان کردن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناله را با این ضعیفی در دلت کردن اثر</p></div>
<div class="m2"><p>با کمان سست، تیر از پیل پران کردن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست در تعریف خود، جز قیمت خود کاستن</p></div>
<div class="m2"><p>خودفروشی سر بسر از مایه نقصان کردن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نان خود را داشتن واعظ ز محتاجان دریغ</p></div>
<div class="m2"><p>همچو ایشان خویش را محتاج یک نان کردن است</p></div></div>