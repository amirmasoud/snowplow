---
title: >-
    شمارهٔ ۵۵۴
---
# شمارهٔ ۵۵۴

<div class="b" id="bn1"><div class="m1"><p>دنیا طلب، به دنیا، دل بین چگونه داده</p></div>
<div class="m2"><p>دل داده است، بس نیست، جان هم به سر نهاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد برای دنیا، کرده است ترک دنیا</p></div>
<div class="m2"><p>آیینه از پی نقش، از نقش گشته ساده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وحشت ز خلق عالم، سرمایه سرور است</p></div>
<div class="m2"><p>در شهر نیست صحرا، ز آنست روگشاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فیض از شکستگان جو، ز آن رو که شخ کمانان</p></div>
<div class="m2"><p>دارند زور بازو، از همت کباده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرگشتگان نسازند، با راه و رسم دنیا</p></div>
<div class="m2"><p>ریگ روان نگیرد، هرگز به خویش جاده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسنش زند ز شوخی هر دم ز روزنی سر</p></div>
<div class="m2"><p>زآنسان که میکند گل، از چشم مست باده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیوانه در بیابان، خود سر دود به هر سو</p></div>
<div class="m2"><p>عاقل، ز جاده زنجیر در پای خود نهاده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بس فگنده دل را، در ورطه هوسها</p></div>
<div class="m2"><p>نور نگاه چون اشک، از چشم من فتاده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دنیا طلب، گر او را دنیا بدل نشسته</p></div>
<div class="m2"><p>در خدمتش همه عمر، او هم بجان ستاده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این عشق آتشین را، نتوان نهفت در دل</p></div>
<div class="m2"><p>پنهان نمیتوان کرد در شیشه رنگ باده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با این فتادگی خاک، بر رو جهد صبا را</p></div>
<div class="m2"><p>از خشم نرمخویان، اندیشه کن زیاده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با لقمه می توان کرد تسخیر تندخویان</p></div>
<div class="m2"><p>نبود ز طعمه دادن، به شیر را قلاده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ظالم چو افتد از کار، استاد ظالمانست</p></div>
<div class="m2"><p>سر حلقه کمانهاست، چون شد کمان کباده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نبود بروز سختی بر دولت اعتمادی</p></div>
<div class="m2"><p>چون ره فتد بکهسار، رهرو شود پیاده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>واعظ از آن گشایند وقت دعا دو کف را</p></div>
<div class="m2"><p>کآنجا مقام دیگر، دارد کف گشاده</p></div></div>