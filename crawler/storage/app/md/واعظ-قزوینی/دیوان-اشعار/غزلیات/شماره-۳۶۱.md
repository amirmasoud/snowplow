---
title: >-
    شمارهٔ ۳۶۱
---
# شمارهٔ ۳۶۱

<div class="b" id="bn1"><div class="m1"><p>ره مقصود طی کردن، نه از تقصیر می‌آید</p></div>
<div class="m2"><p>رسیدن منزل دوری‌ست، از شبگیر می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان با شورش دیوانگی آمیختم خود را</p></div>
<div class="m2"><p>که خونم در شهادت از رگ زنجیر می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشو از وعده آن سروقامت ناامید ای دل</p></div>
<div class="m2"><p>که می‌آید قیامت عاقبت، گر دیر می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خود این بند بگسل، گر جنون کاملی داری</p></div>
<div class="m2"><p>که بوی عقل ای دیوانه از زنجیر می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دست آسان نمی‌آید شهید ناز او گشتن</p></div>
<div class="m2"><p>که این آب حیات، از جوی آن شمشیر می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بس دور است راه بی‌خودی از باده حسنش</p></div>
<div class="m2"><p>جوان گر می‌رود از خویش واعظ پیر می‌آید!</p></div></div>