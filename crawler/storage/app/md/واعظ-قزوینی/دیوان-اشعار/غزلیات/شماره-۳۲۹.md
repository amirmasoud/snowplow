---
title: >-
    شمارهٔ ۳۲۹
---
# شمارهٔ ۳۲۹

<div class="b" id="bn1"><div class="m1"><p>پیش تو شکوه عزم تظلم نمیکند</p></div>
<div class="m2"><p>کز اضطراب راه سخن گم نمیکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در روز وصل، ریختم از دیده هر نفس</p></div>
<div class="m2"><p>خونی که هجر در دل مردم نمیکند!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسم شکفتگی ز جهان برفتاده است</p></div>
<div class="m2"><p>کس غیر چاک سینه، تبسم نمیکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همکاسه با حلاوت عیش زمانه است</p></div>
<div class="m2"><p>هرکس چو باده حق نمک گم نمیکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باب طعام بی مزه پر تکلفی است</p></div>
<div class="m2"><p>هرکس بنان خشک تنعم نمیکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن را ز روی مرتبه جا صدر مجلس است</p></div>
<div class="m2"><p>کو بر کسی تلاش تقدم نمیکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاخیست از درخت حماقت رگ غرور</p></div>
<div class="m2"><p>خود را کسی زیافتگی گم نمیکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما را ضعیف نالی دشمن زبون کند</p></div>
<div class="m2"><p>مظلوم آن کسی ک تظلم نمیکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر وضع خلق، هر گل صبح است خنده یی</p></div>
<div class="m2"><p>دوران چه خنده ها که به مردم نمیکند؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>واعظ ز درد من خبرت میکند اگر</p></div>
<div class="m2"><p>پیش تو دست و پای سخن گم نمیکند!</p></div></div>