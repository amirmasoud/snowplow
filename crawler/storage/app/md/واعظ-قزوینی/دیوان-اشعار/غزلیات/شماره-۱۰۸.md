---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>رفت عهد شباب و، دندان ریخت</p></div>
<div class="m2"><p>رگ ابری گذشت و، باران ریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد جوانی، نماند در سر شور</p></div>
<div class="m2"><p>رعشه پیری این نمکدان ریخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنگدل چند از غم رفتن؟</p></div>
<div class="m2"><p>برگ خود گل بروی خندان ریخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سست شد پا، ز سیل رفتن عمر</p></div>
<div class="m2"><p>کاخ تن، عاقبت ز بنیان ریخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزگارم، به نازکی پرورد</p></div>
<div class="m2"><p>چون گلم، عاقبت ز دامان ریخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود مانند گریه شادی</p></div>
<div class="m2"><p>در جوانی چو عقد دندان ریخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز گل خاریم، نشد حاصل</p></div>
<div class="m2"><p>ز آبرویی که پیش دونان ریخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شعر نتوان بهر جمادی خواند</p></div>
<div class="m2"><p>گوهر خود بخاک نتوان ریخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلک واعظ نریخت لعل خوشاب</p></div>
<div class="m2"><p>خون دل بود، کو ز مژگان ریخت</p></div></div>