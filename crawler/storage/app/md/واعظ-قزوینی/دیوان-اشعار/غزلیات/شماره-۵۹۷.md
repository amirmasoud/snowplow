---
title: >-
    شمارهٔ ۵۹۷
---
# شمارهٔ ۵۹۷

<div class="b" id="bn1"><div class="m1"><p>با همه نیرنگ، تاکی گفت و گوی سادگی؟!</p></div>
<div class="m2"><p>خودفروشی چند، با این دعوی آزادگی؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکساران را، در آن درگاه قرب دیگراست</p></div>
<div class="m2"><p>سجده گاه خلق شد، سجاده از افتادگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی نشانان بیشتر درد سر از کثرت کشند</p></div>
<div class="m2"><p>کعبه را برگرد سرگردند، از بی جادگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاف رعنائی زند، گر سرو پیش قامتش</p></div>
<div class="m2"><p>دارد آن سرو روان در خدمتش استادگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با زبان سبزه گوید دانه در خاک این سخن:</p></div>
<div class="m2"><p>نیست غیر از سرفرازی، حاصل افتادگی!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشأه دردی طلب واعظ، که گردی دلنشین</p></div>
<div class="m2"><p>نیست حرمت شیشه را در محفل از بی بادگی</p></div></div>