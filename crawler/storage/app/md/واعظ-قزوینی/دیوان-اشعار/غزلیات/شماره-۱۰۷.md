---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>یک نظر غافل نمیگردند از پاس حیات</p></div>
<div class="m2"><p>اهل دل را زآن نمیباشد بدنیا التفات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاضر دم باش کاین نفس دغل بسیار کس</p></div>
<div class="m2"><p>برده است و تشنه بازد آورده از آب حیات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم گواراتر بود آزادگان را از سرور</p></div>
<div class="m2"><p>آب تلخی بید را باشد به از آب نبات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشته بر ما زهر آب زندگی از یاد مرگ</p></div>
<div class="m2"><p>لذتی شاید بریم از عمر خود بعد از وفات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاصل آسایش کونین، هر سو خرمن است</p></div>
<div class="m2"><p>می ستاند هرکه از دست تهی دارد برات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زندگی بی عشق نبود در شمار زندگی</p></div>
<div class="m2"><p>ذکر نام دوست باشد سکه نقد حیات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوهرافشانی زبان از کیسه دل میکند</p></div>
<div class="m2"><p>ریزش کلک سخن پرداز باشد از دوات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشرت و عیش تهیدستان پس از مردن بود</p></div>
<div class="m2"><p>تا نشد بید از چمن بیرون، ندادنش نبات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی بگلزار حقیقت می‌برد مرغ دلت</p></div>
<div class="m2"><p>واعظ از دام علایق تا نمی‌یابد نجات؟</p></div></div>