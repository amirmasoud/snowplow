---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>تا زنده است دل، نیست لذت پذیر دنیا</p></div>
<div class="m2"><p>کی میشود نمک سود، ماهی ز شور دریا؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سایه، چند افتی در پای قصر و ایوان؟</p></div>
<div class="m2"><p>بردار دست از شهر، بگذار سر بصحرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با سوز عشق باشد، روشن طریق مقصد</p></div>
<div class="m2"><p>آتش بلد نخواهد هرگز براه بالا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با کوچکان بیامیز تا روشناس گردی</p></div>
<div class="m2"><p>گرچه جلی بود خط، بی نقطه نیست خوانا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای نوجوان مکش سر، از پندهای واعظ</p></div>
<div class="m2"><p>از اره زخمها خورد، تا شد نهال رعنا</p></div></div>