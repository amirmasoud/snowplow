---
title: >-
    شمارهٔ ۳۹۴
---
# شمارهٔ ۳۹۴

<div class="b" id="bn1"><div class="m1"><p>چابقونچی، اگری باخشی مژگان یراقیدور</p></div>
<div class="m2"><p>اول غمزه اوغری، گوزلری اوغری یتاقیدور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول نو بهاردین کیول اودلندی لاله تک</p></div>
<div class="m2"><p>یاندی چرا غمز نه عجب عشق اوجاقیدور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اول ظرف قانی آلمیه گر حسندور شراب</p></div>
<div class="m2"><p>آل قانی آله ساغر اگر یار ساقیدور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر توک باشمده قطع ره عشق ایله تیکان</p></div>
<div class="m2"><p>هر رگ تنمده داغ ایله بیر گل بود اقیدور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اول طره چهره آتشیدین قالخمیش تتون</p></div>
<div class="m2"><p>اول اگری قاش فتنه اودین چاقماقیدور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اول یار وصلین ایستریسنگ خلقدن اوزول</p></div>
<div class="m2"><p>وحدت کمندی عاشقه جانان قوجاقیدور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دنیا اوینده جود ایلین آخرت تاپار</p></div>
<div class="m2"><p>آچوق ال اول کریمیله قونشی چناقیدور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>واعظ اگلدی قد قوجالوقدان اوراق تک</p></div>
<div class="m2"><p>یعنی بویردن ایندی بیچیلماق چاقیدور</p></div></div>