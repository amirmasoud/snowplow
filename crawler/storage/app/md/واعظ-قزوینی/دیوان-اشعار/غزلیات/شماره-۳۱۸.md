---
title: >-
    شمارهٔ ۳۱۸
---
# شمارهٔ ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>تند خویی مرد را بیقدر در عالم کند</p></div>
<div class="m2"><p>باده از جوشیدن بسیار، خود را کم کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر برون آورد عکس از روزن آیینه گفت:</p></div>
<div class="m2"><p>فیض صحبت میتواند سنگ را آدم کند!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قامت از پیری نگردد اهل غیرت را دوتا</p></div>
<div class="m2"><p>پشت مردان را، تواضع پیش دونان خم کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسکه ترسیده است چشمم، ز آشناییهای خلق</p></div>
<div class="m2"><p>آشنایی زخم من مشکل که با مرهم کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پاک بینی شیوه خود کن، که فیض چشم پاک</p></div>
<div class="m2"><p>در سرای خسروان آیینه را محرم کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست سست و سخت دنیا قابل شادی و غم</p></div>
<div class="m2"><p>واعظ ما گریه بر خود، خنده بر عالم کند</p></div></div>