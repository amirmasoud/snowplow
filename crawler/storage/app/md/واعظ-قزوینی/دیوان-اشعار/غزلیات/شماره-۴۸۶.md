---
title: >-
    شمارهٔ ۴۸۶
---
# شمارهٔ ۴۸۶

<div class="b" id="bn1"><div class="m1"><p>ز آن جهان پاک آمدم، آلوده دامن میروم</p></div>
<div class="m2"><p>سوی این غفلت سرا، جان آمدم، تن میروم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلعذاران بسکه در خاک سیاه آسوده اند</p></div>
<div class="m2"><p>چون بگورستان روم، گویی بگلشن میروم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر دنیا میکنم خود را اسیر طبع دون</p></div>
<div class="m2"><p>از هوای دختری در چه چو بیژن میروم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو مسازم پایمال، ای خصم بی پروا، که من</p></div>
<div class="m2"><p>همچو خار از لاغری در پای دشمن میروم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از برای زال دنیا، همچو کوران هر قدم</p></div>
<div class="m2"><p>با وجود دیده، در چاهی چو سوزن میروم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از غم دنیا کشیدم این قدر، واعظ بس است!</p></div>
<div class="m2"><p>سوی خاک اکنون برای واکشیدن میروم</p></div></div>