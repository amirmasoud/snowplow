---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>دردی که بسوی چاره ساز آیی کو؟</p></div>
<div class="m2"><p>سوزی که بصد عجز و نیاز آیی کو؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتی به امید توبه گر راه گناه</p></div>
<div class="m2"><p>عمری که روی این ره و باز آیی کو؟!</p></div></div>