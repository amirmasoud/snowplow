---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>تا کی ز طمع خوار نظرها گردی</p></div>
<div class="m2"><p>از بهر طلب حلقه درها گردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر دو درم که بر یکی تیغ زنی</p></div>
<div class="m2"><p>دلاک صفت بگرد سرها گردی؟!</p></div></div>