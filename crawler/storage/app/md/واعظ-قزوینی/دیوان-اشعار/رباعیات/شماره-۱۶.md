---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>خوش آنکه بغم شکفته باشی همه شب</p></div>
<div class="m2"><p>در خون جگر نهفته باشی همه شب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرمت باد از خروس ای طایر جان</p></div>
<div class="m2"><p>کو بیدار و تو خفته باشی همه شب</p></div></div>