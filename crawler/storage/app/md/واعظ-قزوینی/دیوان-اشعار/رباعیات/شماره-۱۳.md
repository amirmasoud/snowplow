---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>هر ذره ز مهر تست جانی بی‌تاب</p></div>
<div class="m2"><p>هر شبنمی، از یاد تو چشمی پرآب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر برگ گلی است یک کتاب از سخنت</p></div>
<div class="m2"><p>هر غنچه کتابخانه‌ای پر ز کتاب</p></div></div>