---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>کس جا بدل فتادگان تا نکند</p></div>
<div class="m2"><p>معراج سوی عالم بالا نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخل از رفعت بر آسمان سرنکشد</p></div>
<div class="m2"><p>تا در دل خاک خویش را جا نکند</p></div></div>