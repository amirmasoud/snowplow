---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>زین باغ، که در وی گل شادی خودروست</p></div>
<div class="m2"><p>دلها همگی، با غم عالم یکروست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلگیر کسی را نتوان دید در او</p></div>
<div class="m2"><p>جز شاخ که از غنچه گره بر ابروست</p></div></div>