---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>هر چند کلام تو چو در و گهر است</p></div>
<div class="m2"><p>از شهد سخن کلک تو چون نیشکر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر گفتن خود مناز چندین واعظ</p></div>
<div class="m2"><p>گفتن هنری نیست، شنیدن هنر است</p></div></div>