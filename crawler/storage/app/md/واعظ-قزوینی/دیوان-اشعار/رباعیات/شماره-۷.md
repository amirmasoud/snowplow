---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>بر نقش جهان که راه زد جاهل را</p></div>
<div class="m2"><p>تا چند کنی محو تماشا دل را؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دیده عبرت بگشایی، کافیست</p></div>
<div class="m2"><p>یک مد نظر این ورق باطل را</p></div></div>