---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>ای روی تو در نمودن خود بیتاب</p></div>
<div class="m2"><p>حسنت نکشد ز نازکی بار حجاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پرده، رخت ظهور دیگر دارد</p></div>
<div class="m2"><p>بر روی تو، چون پرده چشم است نقاب</p></div></div>