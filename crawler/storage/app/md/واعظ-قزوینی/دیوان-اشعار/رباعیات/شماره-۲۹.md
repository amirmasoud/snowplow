---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>آن را که نه آتش خرد خاموش است</p></div>
<div class="m2"><p>هر شام و سحر دیگ سخا در جوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر عیب که باشدت سخا می پوشد</p></div>
<div class="m2"><p>گردید چو کاسه سرنگون، سرپوش است</p></div></div>