---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>بی رخصت دل زبان بگفتن مگشا</p></div>
<div class="m2"><p>انگشت زبانست ترا عیب نما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردد ز سخن نقص سخنگو ظاهر</p></div>
<div class="m2"><p>ز آواز شود شکست چینی پیدا</p></div></div>