---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>بی خاطر جمع، نکته دان نتوان شد</p></div>
<div class="m2"><p>بی مایه، چو ابر درفشان نتوان شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با فکر معاش، فکر معنی سخن است</p></div>
<div class="m2"><p>گویا بسخن، بی لب نان نتوان شد</p></div></div>