---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>دامن زن آتش فنا، این نفس است</p></div>
<div class="m2"><p>تیره کن مرآت صفا، این نفس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا هست نفس، نمیرهی از غم و رنج</p></div>
<div class="m2"><p>آری، رسن دار فنا این نفس است</p></div></div>