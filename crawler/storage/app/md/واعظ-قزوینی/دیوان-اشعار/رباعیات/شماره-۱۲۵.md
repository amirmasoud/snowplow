---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>ای آنکه تمام آرزو و هوسی</p></div>
<div class="m2"><p>طفلی، مستی، مخبطی، خود چه کسی؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی: که بپیری چو رسم، توبه کنم!</p></div>
<div class="m2"><p>ترسم که جوان روی، بپیری نرسی!!</p></div></div>