---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>بدخو، از خوی خود بدوزخ باشد</p></div>
<div class="m2"><p>خو آتش جانگداز و، خود یخ باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود را شکند ز سختی بیحد خصم</p></div>
<div class="m2"><p>ریزد دم شمشیر چو پرشخ باشد</p></div></div>