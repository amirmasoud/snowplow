---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>این مدعیان که راه نشناخته اند</p></div>
<div class="m2"><p>بر دعوی فقر گردن افراخته اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در برنه مرقعست این طایفه را</p></div>
<div class="m2"><p>بر دعوی خویش محضری ساخته اند</p></div></div>