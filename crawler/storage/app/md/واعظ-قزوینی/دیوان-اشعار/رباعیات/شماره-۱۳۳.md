---
title: >-
    شمارهٔ ۱۳۳
---
# شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>آجل خواهی، به فکر عاجل چه روی؟!</p></div>
<div class="m2"><p>منزل جویی، چو خر به این گل چه روی؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر طالب علمی، طلب دنیا چیست؟!</p></div>
<div class="m2"><p>حق می‌طلبی، از پی باطل چه روی؟!</p></div></div>