---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>یارب! نه سزای تست گر کرده ما</p></div>
<div class="m2"><p>نی لایق درگه تو آورده ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند فگنده ایم ما پرده ز روی</p></div>
<div class="m2"><p>یارب! تو مدر ز لطف خود پرده ما</p></div></div>