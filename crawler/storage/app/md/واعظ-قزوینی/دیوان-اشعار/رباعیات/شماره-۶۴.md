---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>نی در سر شوق و، نی بدل پروا ماند</p></div>
<div class="m2"><p>قوتها رفت و ناتوانیها ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عمر گذشته حاصلم پشت خمی است</p></div>
<div class="m2"><p>موجی از باد در کف دریا ماند</p></div></div>