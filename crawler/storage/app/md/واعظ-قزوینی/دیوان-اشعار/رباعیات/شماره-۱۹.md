---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>آنکو بدلش بیم گنه کمیاب است</p></div>
<div class="m2"><p>گر دعوی دل کند، یقین کذاب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندک گنهی خراب سازد دل را</p></div>
<div class="m2"><p>در خانه آیینه نمی سیلاب است</p></div></div>