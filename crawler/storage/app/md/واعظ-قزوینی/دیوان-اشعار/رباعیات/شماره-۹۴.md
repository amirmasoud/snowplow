---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>گر دل نشدی باین دو مصرع شادم</p></div>
<div class="m2"><p>دادی غم روزگار خوش بر بادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فکر و خیال شعرم این فایده بس</p></div>
<div class="m2"><p>کآن میکند از هر غم پوچ آزادم</p></div></div>