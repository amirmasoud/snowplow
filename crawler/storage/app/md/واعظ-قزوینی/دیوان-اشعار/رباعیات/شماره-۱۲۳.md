---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>ای دل، تو بتنگم از جفا آوردی</p></div>
<div class="m2"><p>ای تن تو مرا بسر چها آوردی؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرگ آمد و هنگام جداییست کنون</p></div>
<div class="m2"><p>ای عمر خوش آمدی، صفا آوردی</p></div></div>