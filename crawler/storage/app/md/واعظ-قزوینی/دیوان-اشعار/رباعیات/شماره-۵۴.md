---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>عمر تو بفکر دنیی دون گذرد</p></div>
<div class="m2"><p>کی یک نفست بیاد بیچون گذرد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر خیال باطلی، چند ای دل؟</p></div>
<div class="m2"><p>از حق مگذر؛ کسی ز حق چون گذرد؟!</p></div></div>