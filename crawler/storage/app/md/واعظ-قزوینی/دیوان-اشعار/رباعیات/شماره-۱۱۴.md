---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>واعظ چه کسست؟ کمترین بنده تو!</p></div>
<div class="m2"><p>مسکین تو، محتاج تو، افگنده تو!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو خواجه دلنواز بخشنده من</p></div>
<div class="m2"><p>من بنده روسیاه شرمنده تو</p></div></div>