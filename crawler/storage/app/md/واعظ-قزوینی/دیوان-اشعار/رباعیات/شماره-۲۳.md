---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>در رسته عاشقی،که نفعش ضرر است</p></div>
<div class="m2"><p>جنسش نه متاع و، نقدنی سیم و زر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جنسش، نظر عنایت حضرت دوست</p></div>
<div class="m2"><p>نقدش، زر سرخ لخت های جگر است!</p></div></div>