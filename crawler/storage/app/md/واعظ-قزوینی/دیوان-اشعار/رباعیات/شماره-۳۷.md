---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>بشنو ز گذشتگان ترا گر گوشی است</p></div>
<div class="m2"><p>میگیر عزای خود بسر گر هوشی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز بدوش تست تابوت کسان</p></div>
<div class="m2"><p>فرداست که تابوت تو هم بر دوشی است</p></div></div>