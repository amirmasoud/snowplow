---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>دایم نبود جوانی ایام ترا</p></div>
<div class="m2"><p>صبح پیریست در پی، این شام ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرداست که در دفتر ایام، اجل</p></div>
<div class="m2"><p>از قامت خم حلقه کند نام ترا</p></div></div>