---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>جانت نشود پاک ز گرد عصیان</p></div>
<div class="m2"><p>تا اشک ندامتت نشوید دامان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اعمال تو هموار نگردد، تا تو</p></div>
<div class="m2"><p>انگشت نسازی از گزیدن سوهان</p></div></div>