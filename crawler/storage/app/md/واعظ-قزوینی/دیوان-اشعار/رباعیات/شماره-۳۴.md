---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>ز آن موی میان، فکر مرا جانکاه است</p></div>
<div class="m2"><p>زآن زلف رسا، دست سخن کوتاه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش با دل زار عاشقان پیچیده است</p></div>
<div class="m2"><p>آن زلف مگر ز دودمان آه است؟!</p></div></div>