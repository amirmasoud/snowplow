---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>خود پرستی را ندانی ای پسر</p></div>
<div class="m2"><p>هان ز کس صوفی تو نشنیدی مگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر آن را بر تو واضح میکنم</p></div>
<div class="m2"><p>زود باش از من شنو نیکو نگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود پرستی این همه افعال تو</p></div>
<div class="m2"><p>جامه نو پوشیده دستار سر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنگری بر کتف خود چون پیش پس</p></div>
<div class="m2"><p>این همه فعل بد آرد درد سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حل بکن این نکته را در جان خویش</p></div>
<div class="m2"><p>گفتهٔ این یار فی الواقع نگر</p></div></div>