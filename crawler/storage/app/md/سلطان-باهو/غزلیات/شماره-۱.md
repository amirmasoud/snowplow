---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>حب دنیا راس آمد کل خطاء</p></div>
<div class="m2"><p>تا نه پنداری که این باشد عطاء</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی عطا باشد که باشد بی بقا</p></div>
<div class="m2"><p>بی بقا را تا نگوئی خود عطاء</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با قلیل الفهم گر گوید کسی</p></div>
<div class="m2"><p>این عطا هرگز مگو، باشد خطاء</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسته دل با وی نشاید مطلقا</p></div>
<div class="m2"><p>بستگی دل با خطا باشد خطاء</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یار با وی دوستی هرگز مکن</p></div>
<div class="m2"><p>لا تقل هذا عطا الا خطاء</p></div></div>