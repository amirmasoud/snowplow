---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>نهایت نیست راه عشق را یار</p></div>
<div class="m2"><p>تو یک روباش دست ازکار بردار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فنا کن خویش را در راه جانان</p></div>
<div class="m2"><p>چه کار آید ترا این درم و دنیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر یک دل نباشی در طریقش</p></div>
<div class="m2"><p>نه بینی روی او هرگز درین دار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و فی الکونین کی بیند جمالش</p></div>
<div class="m2"><p>فدا کن جان بگرد زلف آن یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دریغ از وی چه داری پاره زر را</p></div>
<div class="m2"><p>تو خاصه جان خود با یار بسپار</p></div></div>