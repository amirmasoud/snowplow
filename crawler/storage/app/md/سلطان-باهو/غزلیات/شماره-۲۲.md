---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>چو اینما تولوا شد قبلهٔ حقیقت</p></div>
<div class="m2"><p>جهتی دگر ندارم جز صاحب حقیقت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل مسجد الحرام یقین قبلهٔ من است</p></div>
<div class="m2"><p>شوق دگر ندارم جز شوقت حقیقت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیرون منه قدم ز شریعت محمدی</p></div>
<div class="m2"><p>گر عارفی تو محرم اسرار الحقیقت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باهُو بذکر هُو هُو دائم تو شُغل دار</p></div>
<div class="m2"><p>هُو هُو بکن تُو هُو هُو های حق حقیقت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای یار قبله هرکس دارد به قدر خویش</p></div>
<div class="m2"><p>تو قبله همان کن، کو قبلهٔ حقیقت</p></div></div>