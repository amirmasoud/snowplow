---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>می‌نالم از عشق تو، جان را خبری نیست</p></div>
<div class="m2"><p>بیمارم غمخوارم کس را خبری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا پای نهادیم درین راه تو جانان</p></div>
<div class="m2"><p>حیران شده ام مرده دلان را خبری نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از حال من آگاه کجا میشود آن یار</p></div>
<div class="m2"><p>هی های که این سنگدلان را خبری نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای آنکه توئی طعنه زنی محض خطاست</p></div>
<div class="m2"><p>این سوز دلم را تو چه دانی خبری نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوشبوی وفا می‌شنود یار ز هر آه</p></div>
<div class="m2"><p>آه صد این بی‌خبران را خبری نیست</p></div></div>