---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>ز دنیا تو ترک گیر که راس العبادت است</p></div>
<div class="m2"><p>آری عبادت است ولیکن عنایت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنها که تَرک کرد ز اهل عِنایت اند</p></div>
<div class="m2"><p>آن مَردِ حق شناس که اهل قناعت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عارف بگرد دُنیا ای جان کجا بگردد</p></div>
<div class="m2"><p>آنکس که ترک کرد از اهل سعادت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دنیا درین جهان چو مردار منجلاب است</p></div>
<div class="m2"><p>هر کس گرفت باخود زهر این کفایت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکس که میل کرد بهمت تمام خویش</p></div>
<div class="m2"><p>بختش به بین تو یاور ز اهل سعادت است</p></div></div>