---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>و هو معکم اینما کنتم نگر</p></div>
<div class="m2"><p>ورنه خواندی رو تو در قرآن نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قرب حق با تو چنان دارد یقین</p></div>
<div class="m2"><p>تو همیدانی که ازما دور تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاشکی از قرب او واقف شوی</p></div>
<div class="m2"><p>تا نه گردی گرد دنیا در بدر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار منزل دوستان خود دور نیست</p></div>
<div class="m2"><p>چشم باید تا شوی صاحب نظر</p></div></div>