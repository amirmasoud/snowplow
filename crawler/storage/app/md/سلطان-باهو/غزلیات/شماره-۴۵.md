---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>دنیاست عین جیفه، کلابند طالبان</p></div>
<div class="m2"><p>این قول واضح است ز نبی آخرالزمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر جیفه محنت، در وی چرا کشی</p></div>
<div class="m2"><p>توکل تو بر خدا کن هُو الله است مهربان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی رنج و محنت تو چو روزی دهد خدا</p></div>
<div class="m2"><p>جیفه است پی جیفه چه گردی تو چون سگان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هان سگ نه‌ای تو انسان، پی جیفه چیست غم</p></div>
<div class="m2"><p>انسان انیس حق شو، حق را به حق رسان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غو غو سگی مکن تو درین دار الفناء</p></div>
<div class="m2"><p>این جیفه حرامست سگی را به سگ رسان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای یار بهر جیفه تو دندان چو سگ مزن</p></div>
<div class="m2"><p>این جیفه حرام است چو غدود قصابگان</p></div></div>