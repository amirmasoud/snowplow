---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>از ذات حق تعالی اعلام بی نوا را</p></div>
<div class="m2"><p>گر عاشق تو مائی، کن ترک ماسوی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماذات ذوالجلالیم و از کبریا کمالیم</p></div>
<div class="m2"><p>ما شاه با عطائیم از ما بجو تو مارا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من ذات بی نشانم، فارغ ز این و آنم</p></div>
<div class="m2"><p>کس را غمی ندارم، غمخوار باش مارا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من با تو مهربانم، بس شوق با تو دارم</p></div>
<div class="m2"><p>ذوق دگر ندارم، جز قرب تو گدا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر شوق وصل داری، باما بکن تو زاری</p></div>
<div class="m2"><p>جز ما مجو تو یاری، خود یار باش مارا</p></div></div>