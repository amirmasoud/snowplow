---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>تعالی الله چه زیبا روی دلدار</p></div>
<div class="m2"><p>چو حسنش دیدم و دل گشت گلزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منور گشت جانم همچو خورشید</p></div>
<div class="m2"><p>هویدا گشت برما جمله اسرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم چون دید آن نور تجلی</p></div>
<div class="m2"><p>معلی گشت با ماشد باقرار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که لا مقصود فی الکونین مارا</p></div>
<div class="m2"><p>هوالله الاحد موجود بس یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فناشد ماومن خود جمله او ماند</p></div>
<div class="m2"><p>نمانده غیر او شد رنگ رخسار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نماید صورت خود خویش هر دم</p></div>
<div class="m2"><p>به حسن صورت بی مثل در یار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بکن سجده به پیش روی معشوق</p></div>
<div class="m2"><p>تو باهُو باش دائم همچو غمخوار</p></div></div>