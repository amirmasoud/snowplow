---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>طور سینا گشت موسی را مقام</p></div>
<div class="m2"><p>بی حجاب آنجا شنیدی خود کلام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقی را طور معراج دل ست</p></div>
<div class="m2"><p>هر زمان از حق رسد او را سلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل که انسان ست عرش الله بدان</p></div>
<div class="m2"><p>از حدیث حضرت، آمد این کلام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این دل انسان بیضهء ناسوتی ثمر</p></div>
<div class="m2"><p>لیک در وی سر لاهوتی تمام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذات انسان عین سر الله بدان</p></div>
<div class="m2"><p>هان شنو گفتم ترا مجمل کلام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار انسان مخزن خاصه خدا ست</p></div>
<div class="m2"><p>غیر عارف کس نداند والسلام</p></div></div>