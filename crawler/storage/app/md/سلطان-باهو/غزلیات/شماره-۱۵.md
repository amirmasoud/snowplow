---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>مبتلا در عشق گشتم، صبر ما یاران کجاست</p></div>
<div class="m2"><p>سخت بیماریست در جان مرهم جانان کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ز سوز هجر او خون گریه کردم روز و شب</p></div>
<div class="m2"><p>طاقت دوری ندارم، شاه غمخواران کجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از برای دیدن رخ ماه وش دلدار خویش</p></div>
<div class="m2"><p>شوق در جانم بسی آن ماه مشتاقان کجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشتیاق از حد گذشته جانب جانان ما</p></div>
<div class="m2"><p>وصل جانان کی شود آن گلشن شاهان کجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماسوی المحبوب شوقی نیست در جان مرا</p></div>
<div class="m2"><p>گلرخ و سیمین تن و آن نرگس مستان کجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این نهال بدن من از تشنگی گشت ست خشک</p></div>
<div class="m2"><p>جوی دهانم خشک گشته، آن ابر باران کجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرد کویش گریه کرده یار بهر یار خویش</p></div>
<div class="m2"><p>لب لسانم خشک گشته بحر بی‌پایان کجاست</p></div></div>