---
title: >-
    بخش ۲۸ - اهلیّت باده خواران
---
# بخش ۲۸ - اهلیّت باده خواران

<div class="b" id="bn1"><div class="m1"><p>چو می روح بخشی نیاید به دست</p></div>
<div class="m2"><p>دریغا که بر دستِ نا کس بُدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عذابی شدیدست و رنجی عظیم</p></div>
<div class="m2"><p>ز دستِ مخالف شرابِ حمیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر جان شیرین دهندت به زور</p></div>
<div class="m2"><p>ز دست ترش روی تلخ است و شور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبانی دوزخ حریفِ بَدست</p></div>
<div class="m2"><p>تو زو در عذابی و او از خودست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بتر از بتر چیست بدمستِ لُنب</p></div>
<div class="m2"><p>کنارت پر افعیست برخود مَجُنب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به لاحول ابلیس را دور کن</p></div>
<div class="m2"><p>به کسنی مداوای محرور کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مده خرس را آب خضر ای پسر</p></div>
<div class="m2"><p>که جز مُهره بر خر نزیبد گهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرا مفتیان می کنندش حرام</p></div>
<div class="m2"><p>سخن راست بشو زپس احترام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دریغ است در کونِ خر ریختن</p></div>
<div class="m2"><p>از آن شد سزایِ برآویختن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگر مریم انگورِ آبستن است</p></div>
<div class="m2"><p>که روحش درون همچو جان در تن است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو از روح محض است آبستنیش</p></div>
<div class="m2"><p>چرا زیر پای خران افکنیش</p></div></div>