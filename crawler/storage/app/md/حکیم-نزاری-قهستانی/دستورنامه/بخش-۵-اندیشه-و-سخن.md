---
title: >-
    بخش ۵ - اندیشه و سخن
---
# بخش ۵ - اندیشه و سخن

<div class="b" id="bn1"><div class="m1"><p>چو بر مرکبِ فکر گردم سوار</p></div>
<div class="m2"><p>نیارم گرفتن عنان استوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر از هر طرف می‌کشد بارگی</p></div>
<div class="m2"><p>مرا می‌رباید به یک بارگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو قادر نه‌ام بر کمان و کمند</p></div>
<div class="m2"><p>برون می‌دود صیدم از قیدِ بند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا خود ز عالم برون می‌برد</p></div>
<div class="m2"><p>چه عالم ز خود هم برون می‌برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشو معترض کو غلو در گرفت</p></div>
<div class="m2"><p>که این برق در خشک و در تر گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر از آتش من خبر داشتی</p></div>
<div class="m2"><p>چه پروای عیب و هنر داشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن باطنی دارد و ظاهری</p></div>
<div class="m2"><p>بدو نیک را اول و آخری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو مرد کدامی و اهل کدام</p></div>
<div class="m2"><p>نصیب خود ادراک کن والسلام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جُعَل از گلستان ندارد نصیب</p></div>
<div class="m2"><p>ز کنّاس گند و ز عطار طیب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر در سیاهیست آب حیات</p></div>
<div class="m2"><p>ببین چشمه‌ی خضر من در دوات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به زور و به زرگر به دست آمدی</p></div>
<div class="m2"><p>سکندر سیاهی پرست آمدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ازین جام اگر جرعه‌ای یافتی</p></div>
<div class="m2"><p>ز هر حرف صد چشمه بشکافتی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خضِر را ازین چشمه دادند آب</p></div>
<div class="m2"><p>توهم زین سیاهی طلب کن بیاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غلط می‌کنم از سیاهی مجوی</p></div>
<div class="m2"><p>اگر چشمه خواهی پی خضر پوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خضِر را طلب کن که آب حیات</p></div>
<div class="m2"><p>ازو بازیابی نه از ترّهات</p></div></div>