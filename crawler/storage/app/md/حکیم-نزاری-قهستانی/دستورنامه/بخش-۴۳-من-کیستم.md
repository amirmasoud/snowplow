---
title: >-
    بخش ۴۳ - من کیستم
---
# بخش ۴۳ - من کیستم

<div class="b" id="bn1"><div class="m1"><p>مرا خود در اوقاتِ ردّ و قبول</p></div>
<div class="m2"><p>نباشد غمِ ترّهاتِ جَهول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر سرِّ مردان ندارم نگاه</p></div>
<div class="m2"><p>کله سر نبیند دگر سرکلاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیارم به نا محرمان باز گفت</p></div>
<div class="m2"><p>چه گویم که با من که این راز گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه هرچه در حقِّ من گفته اند</p></div>
<div class="m2"><p>نه از من که از خویشتن گفته اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر معترض طعنه ای می زند</p></div>
<div class="m2"><p>به خود بر ز خود هم چو قز می تند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا کس نداند که من کیستم</p></div>
<div class="m2"><p>کدامم کجاام کی ام چیستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه بارست بر جانِ پر درد من</p></div>
<div class="m2"><p>که خون شد دلِ ناز پروردِ من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زبس طعنه همواره دل خسته ام</p></div>
<div class="m2"><p>دلِ خسته در لطفِ حق بسته ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو از بدوِ فطرت قلم می رود</p></div>
<div class="m2"><p>چه بردستِ من بیش و کم می رود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلم موج خون می زند لب خموش</p></div>
<div class="m2"><p>ملامت روان و من آگنده گوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همان به که فی الجمله از هیچ روی</p></div>
<div class="m2"><p>نگویم سخن تا نگویند گوی</p></div></div>