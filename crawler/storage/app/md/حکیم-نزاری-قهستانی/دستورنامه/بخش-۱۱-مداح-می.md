---
title: >-
    بخش ۱۱ - مداح می
---
# بخش ۱۱ - مداح می

<div class="b" id="bn1"><div class="m1"><p>غذای تن و قوت جان است راح</p></div>
<div class="m2"><p>چه وصفش کنم بیش از آن است راح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چهل سال مداح می بوده‌ام</p></div>
<div class="m2"><p>هنوزش به واجب بنستوده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب و روز تحسین می کرده‌ام</p></div>
<div class="m2"><p>تفاخر به آئین وی کرده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بپرورده‌ام هم چو جان در تنش</p></div>
<div class="m2"><p>که هست اتصالی به جان منش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان با دمِ او دمی داشتم</p></div>
<div class="m2"><p>که چون جان عزیزش همی‌داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه‌ش گفته‌ام جان شیرین من</p></div>
<div class="m2"><p>جم وقت جام سفالین من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه‌ش گفته‌ام یادگار مسیح</p></div>
<div class="m2"><p>غلط می‌کنم اعتبارِ مسیح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گهی مادرش گفته‌ام مریم است</p></div>
<div class="m2"><p>که چون ابن مریم مبارک دم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گهش روح ثانی نهادم لقب</p></div>
<div class="m2"><p>درافگندم آتش به آبِ عنب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر شرح خاصیت می‌دهم</p></div>
<div class="m2"><p>ندانم که انصافِ او کی دهم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هزارش صفت کرده‌ام در هزار</p></div>
<div class="m2"><p>هنوزش نگفتم یکی از هزار</p></div></div>