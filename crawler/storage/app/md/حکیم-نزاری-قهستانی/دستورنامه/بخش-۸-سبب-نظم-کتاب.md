---
title: >-
    بخش ۸ - سبب نظم کتاب
---
# بخش ۸ - سبب نظم کتاب

<div class="b" id="bn1"><div class="m1"><p>بقا باد این هر دو را بی شمار</p></div>
<div class="m2"><p>بلند اختر و بختِ فرخنده بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نیک اختری بهره‌مند از حیات</p></div>
<div class="m2"><p>رسیده به کام از بنین و بنات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو کردند در عنفوان شباب</p></div>
<div class="m2"><p>هوای سماع و نشاط شراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رسم جوانان نوخاسته</p></div>
<div class="m2"><p>عزب خانه‌ی خلوت آراسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آب رز آلوده دامان و دست</p></div>
<div class="m2"><p>زمن محترز بوده مخمور و مست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو اخلاط باهم سران داشتند</p></div>
<div class="m2"><p>به اوقات دستی در آن داشتند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو خود بوده بودم در آن گرد و درد</p></div>
<div class="m2"><p>به یک رو ندانستم انکار کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به وعظ متین و به اندرز چُست</p></div>
<div class="m2"><p>در عیب گفتم ببندم نخست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگر پس روِ استقامت شوند</p></div>
<div class="m2"><p>نصیحت به گوش خرد بشنوند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گذر کرد بر خاطرم بارها</p></div>
<div class="m2"><p>وز آن بود بر خاطرم بارها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که از بهر فرزند فرخنده فال</p></div>
<div class="m2"><p>برون آورم تنسقی حسب حال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که دستور خوانند آن را به نام</p></div>
<div class="m2"><p>اگر بخت دستور باشد مدام</p></div></div>