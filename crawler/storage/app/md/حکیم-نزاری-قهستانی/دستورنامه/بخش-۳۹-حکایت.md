---
title: >-
    بخش ۳۹ - حکایت
---
# بخش ۳۹ - حکایت

<div class="b" id="bn1"><div class="m1"><p>ربا خواره‌ای بود بیدانیی</p></div>
<div class="m2"><p>چه گویم چنانی که میدانیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>براتی به می داشتم از یکی</p></div>
<div class="m2"><p>که این بود ازو به ترک اندکی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم او هم پدر مهتر ده بدند</p></div>
<div class="m2"><p>وکیلان املاک خط ده بدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی از بهل جرد برتک نشست</p></div>
<div class="m2"><p>به بیدان شد و کرد او را به دست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>براتش بداد و فرا پیش کرد</p></div>
<div class="m2"><p>در آوردش از مجلس آن ساده مرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرومایه سوگند بسیار خَورد</p></div>
<div class="m2"><p>ز اقرار برگشت و انکار کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که از خاصه‌ی خویش وز آنِ پدر</p></div>
<div class="m2"><p>ندارم شراب و ندارم خبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز صد من در آتابه یک من شراب</p></div>
<div class="m2"><p>اگر هست درخان و مانش خراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برو جمله دل‌های روشن گواه</p></div>
<div class="m2"><p>که خم خانه‌ای دارد آن دل سیاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز جای دگر چون مهیّا نبود</p></div>
<div class="m2"><p>بسی جهد کردیم و پوزش نمود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به زاری و زر در نیاورد سر</p></div>
<div class="m2"><p>نظرها به حیرت در آن بی‌بصر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدیشان پی‌آورده بودند و بس</p></div>
<div class="m2"><p>نبردند دیگر گمانی به کس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>علی الجمله می‌کرد انکار سخت</p></div>
<div class="m2"><p>به یک من نشد معترف شور بخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حریفان فرومانده نومید و پست</p></div>
<div class="m2"><p>همه نیمه جان و همه نیمه مست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنین ناجوانمردی‌ای پیش کرد</p></div>
<div class="m2"><p>ولی هم سزای سر خویش کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درآمد ز در قاصدی ناگهان</p></div>
<div class="m2"><p>که هین پیشتر پای برگیر هان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که آمد محصل به بیدان چو گرد</p></div>
<div class="m2"><p>همه خان و مانت زبر زیر کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به تحصیل صد من شراب آمدست</p></div>
<div class="m2"><p>دو اسبه ز فرطِ شتاب آمدست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرفتش محصّل چو آن جا رسید</p></div>
<div class="m2"><p>سراپای در زخم چوبش کشید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>درآویختش قاید خانه روب</p></div>
<div class="m2"><p>زدش بر کف پای بسیار چوب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بخورد آخرالامر چوبی دویست</p></div>
<div class="m2"><p>نفس راست می‌کرد و می‌گفت نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکی آمد و شد ضمان دارِ او</p></div>
<div class="m2"><p>خبر داشت از یک نهان زار او</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کلندی بیاورد و بشکافتند</p></div>
<div class="m2"><p>دو خم پر شراب بهین یافتند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فرستاده‌ی ما ز پس می‌دوید</p></div>
<div class="m2"><p>قضا را سراسر بدان جا رسید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از آن جمله پنجاه من بار کرد</p></div>
<div class="m2"><p>چو رقاص کاچول بسیار کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>درآمد ز در همچو خر در خلاب</p></div>
<div class="m2"><p>شده پست در زیر خیک شراب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برآمد خروشی به شادی ز جمع</p></div>
<div class="m2"><p>برافروختند از فرح هم چو شمع</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>محصّل خمش برد و جانش بسوخت</p></div>
<div class="m2"><p>تنش کرد ریش و روانش بسوخت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دل ما بیازرد از آن بی‌فروغ</p></div>
<div class="m2"><p>بدان خورده سوگندهای دروغ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پس از هفته‌ای دیدمش برگذر</p></div>
<div class="m2"><p>بدو گفتم ای مردکوته نظر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگر باز در جمع مستان روی</p></div>
<div class="m2"><p>نباید که با مکر و دستان روی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نگفتی که مردی بود در میان</p></div>
<div class="m2"><p>که افتی ز آزار او در زیان</p></div></div>