---
title: >-
    بخش ۳۴ - حفظ دندان
---
# بخش ۳۴ - حفظ دندان

<div class="b" id="bn1"><div class="m1"><p>چو نقلی گرفتی به دندانِ تنگ</p></div>
<div class="m2"><p>گراینده تسکین پذیرد به سنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دندان حوالت مکن کارِ سنگ</p></div>
<div class="m2"><p>که بازش به گوهر نیاری به چنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگه دارش از استخوان زینهار</p></div>
<div class="m2"><p>توهم این نصیحت زمن یاددار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که من از پدر کردم این پند گوش</p></div>
<div class="m2"><p>ازین شرط تابرنگردی بکوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توهم ای پسر از پدر یاددار</p></div>
<div class="m2"><p>که من از پدر داشتم یادگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپاس از خدا کاندرین شست و پنج</p></div>
<div class="m2"><p>ز دندان نه زحمت کشیدم نه رنج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برومند بودم ز پند پدر</p></div>
<div class="m2"><p>تو نیز از پدر باریا بهره بر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مکن خسته دندان به بادام سخت</p></div>
<div class="m2"><p>که از نقل محکم شود لخت لخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر پسته بشکن به سنگ ای پسر</p></div>
<div class="m2"><p>در بسته نتوان شکستن به سر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو رخنه شود در مصافی درست</p></div>
<div class="m2"><p>شود پای استاده برجای سست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندارد چو لشکر بجنبد ز جای</p></div>
<div class="m2"><p>یکی در هزیمت نیفشرده پای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مه اندازشان در پی یک دگر</p></div>
<div class="m2"><p>مجنبان بلا تا نیاید به سر</p></div></div>