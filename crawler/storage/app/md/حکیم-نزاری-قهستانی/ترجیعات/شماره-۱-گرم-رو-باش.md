---
title: >-
    شمارهٔ ۱ - گرم رو باش
---
# شمارهٔ ۱ - گرم رو باش

<div class="b" id="bn1"><div class="m1"><p>ای دل ز بلا مکن تحاشی</p></div>
<div class="m2"><p>جان بر سر دل چرا نباشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آتش و آب حرص و آزی</p></div>
<div class="m2"><p>تا طالب نان و دیگ باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایام لویشه کرده بودست</p></div>
<div class="m2"><p>یعنی که گرو به یک لواشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باری به رقیب دوست گفتم</p></div>
<div class="m2"><p>ریش دل ما چه می خراشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معشوق تو گفت در حضورست</p></div>
<div class="m2"><p>اما تو طلب گر معاشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خلق نزاریا نهان باش</p></div>
<div class="m2"><p>گرچه به فجور و فسق باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آری که نه راز پادشاهان</p></div>
<div class="m2"><p>مکشوف کنند بر حواشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نی نی سخنی بزرگ گفتی</p></div>
<div class="m2"><p>یعنی که تو هم ز خواجه تاشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دریاب که سرّ ارجعی چیست</p></div>
<div class="m2"><p>تا پس رو نقد وقت باشی</p></div></div>
<div class="b2" id="bn10"><p>در سیر و سلوک گرم رو باش</p>
<p>خرمن می سوز و دانه می پاش</p></div>
<div class="b" id="bn11"><div class="m1"><p>هر طایفه مانده اند موقوف</p></div>
<div class="m2"><p>در منکر نهی و امر معروف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای امت انبیای مرسل</p></div>
<div class="m2"><p>عمر از پی نسیه کرده مصروف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا چند توان به جهل بودن</p></div>
<div class="m2"><p>بر رای و قیاس خویش مشعوف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیغمبر ما چرا به معراج</p></div>
<div class="m2"><p>گشته ست به جبرئیل موصوف</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یعنی ز حجاب خود برون آی</p></div>
<div class="m2"><p>تا بر تو شود رموز مکشوف</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرگز نرسی به قصر مقصد</p></div>
<div class="m2"><p>ساکن به خرابه های مالوف</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از غصه بر کشیده ایوان</p></div>
<div class="m2"><p>مسکن به خرابه می کند کوف</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وز ننگ وجود خرقه پوشان</p></div>
<div class="m2"><p>هرسال غنم بیفگند صوف</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جز پس رو امر و آمر وقت</p></div>
<div class="m2"><p>فی الجمله به کس مباش موقوف</p></div></div>
<div class="b2" id="bn20"><p>در سیر و سلوک گرم رو باش</p>
<p>خرمن می سوز و دانه می پاش</p></div>
<div class="b" id="bn21"><div class="m1"><p>تا کی بت وهم پروریدن</p></div>
<div class="m2"><p>در هاویه هوا دویدن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از بهر نخ و نسیج و کم خا</p></div>
<div class="m2"><p>بر خویش چو کرم غز تنیدن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کم خا چه کنند ژاژ کم خا</p></div>
<div class="m2"><p>کرباس طلب کفن خریدن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ار مکتسب حلال باید</p></div>
<div class="m2"><p>پیراهن آخرین بریدن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شد سیر دلم ز اهل طامات</p></div>
<div class="m2"><p>وز موعظه گفتن و شنیدن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سرچشمه آب زندگانی ست</p></div>
<div class="m2"><p>زین چشمه ببایدت شمیدن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یک جرعه ز جام عشق و سد جم</p></div>
<div class="m2"><p>کو صبر و لیک تا چشیدن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دوش آمد و گفت ای رمیده</p></div>
<div class="m2"><p>تا چند به غفلت آرمیدن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تعجیل کن و زپای منشین</p></div>
<div class="m2"><p>زنهار که تا به ما رسیدن</p></div></div>
<div class="b2" id="bn30"><p>در سیر و سلوک گرم رو باش</p>
<p>خرمن می سوز و دانه می پاش</p></div>
<div class="b" id="bn31"><div class="m1"><p>آن ها که همیشه با خدای اند</p></div>
<div class="m2"><p>بی خویش روند و با خودآیند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نتوان گفتن که جمله اوی اند</p></div>
<div class="m2"><p>نه ظن بود آن کزو جدای اند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آن قوم که بالغ اند و واصل</p></div>
<div class="m2"><p>مستغرق عین کبریای اند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>و آن زمره که پس روان راه اند</p></div>
<div class="m2"><p>معراج روان در قفای اند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>و آن طایفه دگر که ضدند</p></div>
<div class="m2"><p>واماندگان به وهم و رای اند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>صُمُ بُکمُ مقلدان اند</p></div>
<div class="m2"><p>در کتم عدم دگر کجای اند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جون واحد مطلق اوست آخر</p></div>
<div class="m2"><p>مغرور به خویشتن چرای اند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آن ها که سفر ز خویش کردند</p></div>
<div class="m2"><p>هم راه روندگان مای اند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جایی نزیی به خود، ز خود دور</p></div>
<div class="m2"><p>تا راه به مقصدت نمایند</p></div></div>
<div class="b2" id="bn40"><p>در سیر و سلوک گرم رو باش</p>
<p>خرمن می سوز و دانه می پاش</p></div>
<div class="b" id="bn41"><div class="m1"><p>شب ها من و مسکرات در پیش</p></div>
<div class="m2"><p>تا روز خبر ندارم از خویش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>با دوست نشسته در برابر</p></div>
<div class="m2"><p>دربسته حجاب عصمت از پیش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>با من به زبان حال گفته</p></div>
<div class="m2"><p>کای هیج ز خود چه می کنی پیش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هیهات همین و بس که سلطان</p></div>
<div class="m2"><p>تا کی نطری کند به درویش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>من کیش تو جعبه یی گرفتم</p></div>
<div class="m2"><p>قربان نشوی مگر درین کیش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مغرور مشو به اهل دنیا</p></div>
<div class="m2"><p>قاصر نظرند و باطل اندیش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تو پس رو امر وقت می باش</p></div>
<div class="m2"><p>بیگانه منه تفاوت از خویش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گه نیش عتاب ما بود نوش</p></div>
<div class="m2"><p>گه نوش عطای ما بود نیش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مرهم مطلب برین جراحت</p></div>
<div class="m2"><p>من خسته و جان فگار و دل ریش</p></div></div>
<div class="b2" id="bn50"><p>در سیر و سلوک گرم رو باش</p>
<p>خرمن می سوز و دانه می پاش</p></div>
<div class="b" id="bn51"><div class="m1"><p>بر خور ز جوانی ای جوان مرد</p></div>
<div class="m2"><p>زیرا که بر از جهان ، جوان خورد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در نفی و فنا و جسم خاکی</p></div>
<div class="m2"><p>اثبات بقا نمی توان کرد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از هر که سخن کنند و گویند</p></div>
<div class="m2"><p>ایام دمار ازو برآورد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>در تنگ فضای بیم و امید</p></div>
<div class="m2"><p>چه سخت کش و چه نازپرورد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>می آتش تیز و آدمی خاک</p></div>
<div class="m2"><p>آتش می سوز و خاک می گرد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>جهال زبان کشیده در من</p></div>
<div class="m2"><p>در مستی اگر بنالم از درد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گویند نزاریا به زاری</p></div>
<div class="m2"><p>می نال چو عندلیب بر ورد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اما نه ز گل ستان سخن گوی</p></div>
<div class="m2"><p>رو بر سر کوی دل ستان گرد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مشکل کاری عجب نگاری</p></div>
<div class="m2"><p>من عاشق جفت و او ز من فرد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دنیا نفسی ست هر که دریافت</p></div>
<div class="m2"><p>زآن پیش که آن نفس شود سرد</p></div></div>
<div class="b2" id="bn61"><p>در سیر و سلوک گرم رو باش</p>
<p>خرمن می سوز و دانه می پاش</p></div>
<div class="b" id="bn62"><div class="m1"><p>بی یار نمی شود میسر</p></div>
<div class="m2"><p>بی یار بود درخت بی بر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بی هم نفسی علی الضروره</p></div>
<div class="m2"><p>مشنو که مرا شود میّسر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>جامی و صراحیی ویاری</p></div>
<div class="m2"><p>این هر سه همیشه در برابر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شیرین صنمی به ناخن تیز</p></div>
<div class="m2"><p>افکنده هزار شور در سر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هردم غزلی غزال چشمی</p></div>
<div class="m2"><p>برگفته به لفظ روح پرور</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ساقی پسری گشاده جبهت</p></div>
<div class="m2"><p>کنجی و نهاده قفل بر در</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>خالی ز معربدان مفلس</p></div>
<div class="m2"><p>ایمن ز مقلدان منکر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>آراسته مجلسی چو فردوس</p></div>
<div class="m2"><p>در آب فسرده آتش تر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>برخاسته از کمال یاری</p></div>
<div class="m2"><p>برداشته از میانه ساغر</p></div></div>
<div class="b2" id="bn71"><p>در سیر و سلوک گرم رو باش</p>
<p>خرمن می سوز و دانه می پاش</p></div>