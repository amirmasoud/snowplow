---
title: >-
    بخش ۷ - پیغام دوم شب برای روز
---
# بخش ۷ - پیغام دوم شب برای روز

<div class="b" id="bn1"><div class="m1"><p>باز شب داد روز را پیغام</p></div>
<div class="m2"><p>که: «چنین بر مکش به چرخ اَعلام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من جهاندار بودم از اول</p></div>
<div class="m2"><p>نه تو نه مه نه مشتری نه زحل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کدخدای جهان به حکم خدای</p></div>
<div class="m2"><p>منم و من به عقل روشن رای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قله این بلند طارم را</p></div>
<div class="m2"><p>یعنی این قلعه چهارم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدبانی مقیم می بایست</p></div>
<div class="m2"><p>چشم داری عظیم می بایست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر آن بر گماشتند ترا</p></div>
<div class="m2"><p>تا به اکنون بداشتند ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر سخن در معاملت باشد</p></div>
<div class="m2"><p>دیده بان را چه منزلت باشد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو عیسی بمانده در راهی</p></div>
<div class="m2"><p>چون کنی دعوی شهنشاهی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ماورای تو چند حکام‌اند</p></div>
<div class="m2"><p>کز تو برترنشین این بام‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه بر تو محیط و مافوق‌اند</p></div>
<div class="m2"><p>همه با تخت و افسر و طوق‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست بالای من کسی دیگر</p></div>
<div class="m2"><p>منم و مملکت ز من یکسر»</p></div></div>