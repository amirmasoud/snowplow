---
title: >-
    بخش ۶ - پاسخ روز به شب
---
# بخش ۶ - پاسخ روز به شب

<div class="b" id="bn1"><div class="m1"><p>با صبا گفت: «دم مزن دیگر</p></div>
<div class="m2"><p>به رسالت قدم بزن دیگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز گرد از همین قدر سوی شب</p></div>
<div class="m2"><p>که: ز نادانی تو نیست عجب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون تویی را چه حد پایه ماست؟</p></div>
<div class="m2"><p>خود سواد تو عکس سایه ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهگهت دل که همچو رخ سیه است</p></div>
<div class="m2"><p>روشن از عکس شمعدان مه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع مه گر نکردمی روشن</p></div>
<div class="m2"><p>کی شدی گلخن تو چون گلشن؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی تاریک تو چو دود تنور</p></div>
<div class="m2"><p>در خورد راستی مقابل نور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جای تو چاه تنگ و تار بود</p></div>
<div class="m2"><p>کنج تاریک و نفت و غار بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر مقامی که باز پردازم</p></div>
<div class="m2"><p>گه گهش سایه بر سر اندازم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون شود عکس من از او خالی</p></div>
<div class="m2"><p>خلوت آباد خود کنی حالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در غلط اوفتاده ای با خویش</p></div>
<div class="m2"><p>سر و کاری نهاده‌ای با خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از دماغت برون کنم سودا</p></div>
<div class="m2"><p>نگذارم که دم زنی فردا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کی چنین بودی آمر و ناهی</p></div>
<div class="m2"><p>که مرا زیردست می خواهی؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به درازی خود مشو مغرور</p></div>
<div class="m2"><p>تیره تر هم نباشی از دیجور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من چنانت فرو برم به زمین</p></div>
<div class="m2"><p>که ز من یاد ناوری پس از این</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آمدم، برگ کار خویش بساز</p></div>
<div class="m2"><p>تا قیامت ز تو نگردم باز»</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خواست رخصت صبا و باز آمد</p></div>
<div class="m2"><p>چون مشعوِذ که مهره باز آمد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خواند هرگونه ز آفتاب خبر</p></div>
<div class="m2"><p>کرد شب را چو دود زیر و زبر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر جوابی که روز باز نوشت</p></div>
<div class="m2"><p>همچو آتش فتاد در انگشت</p></div></div>