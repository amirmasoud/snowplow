---
title: >-
    بخش ۱۶ - پاسخ روز یا خورشید
---
# بخش ۱۶ - پاسخ روز یا خورشید

<div class="b" id="bn1"><div class="m1"><p>روز شد باز گرم و بر جوشید</p></div>
<div class="m2"><p>که: «مرا کی توان به گل پوشید؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من که پیش تو خرد و مختصرم</p></div>
<div class="m2"><p>چندبار از جهان بزرگترم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرسد با منت بزرگ سری</p></div>
<div class="m2"><p>غم خود خور که تو نه مرد خَوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمترین گنج خانه ام دریاست</p></div>
<div class="m2"><p>واپسین چاکر درم جوزاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو پلاس سیه به صد تشویش</p></div>
<div class="m2"><p>به سر اندر کشیده همچو کشیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من متوّج به افسر زرکش</p></div>
<div class="m2"><p>بر سریر فلک سلیمان وَش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه را طالعش زِمَن باشد</p></div>
<div class="m2"><p>مالک ملکت زَمَن باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر تو هستی سوار بر ادهم</p></div>
<div class="m2"><p>بارگیر من است اشهب هم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر سیاه تو دور تازنده ست</p></div>
<div class="m2"><p>چرده من هنوز پازنده ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با من آخر نفاق چند کنی؟</p></div>
<div class="m2"><p>دعوی (و) طمطراق چند کنی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه کس را ز روز شرم و حیاست</p></div>
<div class="m2"><p>پای سترو  صلاح از او برجاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه چو شب کز وجود او فقها</p></div>
<div class="m2"><p>بی حیایی کنند چون سُفها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه اندیشه محال کنند</p></div>
<div class="m2"><p>میل و رغبت به زلف و خال کنند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شوخ چشم و ستیزه روی شوند</p></div>
<div class="m2"><p>بی محابا به بام و کوی شوند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شب ناگاه بی حجیب روند</p></div>
<div class="m2"><p>که تهی مغز و پرنهیب روند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون ز پیشش بهانه برخیزد</p></div>
<div class="m2"><p>صد حجاب از میانه برخیزد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با همه کس چو پیش کار شود</p></div>
<div class="m2"><p>در همه چیز یار غار شود»</p></div></div>