---
title: >-
    بخش ۵ - بردن باد صبا پیام شب را از برای روز و آغاز پیکاره
---
# بخش ۵ - بردن باد صبا پیام شب را از برای روز و آغاز پیکاره

<div class="b" id="bn1"><div class="m1"><p>شب فرستاد پیش روز رسول</p></div>
<div class="m2"><p>«کای جهانگرد فتنه جوی فضول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرشب از فتنۀ تو تیره ترم</p></div>
<div class="m2"><p>چند داری چو روز خیره سرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه بگریختم ز مشغله ای</p></div>
<div class="m2"><p>آمدی در گرفته مشعله ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه برهم زدی ولایت من</p></div>
<div class="m2"><p>برشکستی سپاه و رایت من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پای بیرون منه ز حد قیاس</p></div>
<div class="m2"><p>بر رهم بیش از این مریز الماس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعد از این ترک سرفرازی کن</p></div>
<div class="m2"><p>با بزرگان به خرده بازی کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با منت دست درستم نشود</p></div>
<div class="m2"><p>کارت از پیش بیش و کم نشود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا به شام از سحر ستیز کنی</p></div>
<div class="m2"><p>عاقبت هم ز من گریز کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون سیاهان من خروج کنند</p></div>
<div class="m2"><p>در افق بر مَعاقِب تو زنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از تو چندان کشند کز بس خون</p></div>
<div class="m2"><p>شود آلوده دامن گردون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تویی و مغرب و هزیمت خویش</p></div>
<div class="m2"><p>سوی مشرق مکن عزیمت بیش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم بر این شرط اگر قرار دهی</p></div>
<div class="m2"><p>ترک آشوب و اضطرار دهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خیز و گر نیز رای آن داری</p></div>
<div class="m2"><p>سر ز مغز سبک گران داری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا به دفع تو هم قیام کنم</p></div>
<div class="m2"><p>بعد از این قصد انتقام کنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یک شبیخون کنم به لشکر زنگ</p></div>
<div class="m2"><p>که هزیمت بری به صد فرسنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قلم فتنه سرنگون کنمت</p></div>
<div class="m2"><p>از حدود جهان برون کنمت»</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آخرالامر ایلچی صبا شد</p></div>
<div class="m2"><p>گشاده زبان و بسته قبا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>راه مقصد گرفت مستعجِل</p></div>
<div class="m2"><p>تا به خاور که بود سر منزل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رایتی دید با هزار شکوه</p></div>
<div class="m2"><p>که برآورد سر ز قله کوه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خسروی تاج و تختش از آتش</p></div>
<div class="m2"><p>چرخ در موکبش عماری کش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لشکری همچو ذره بیش از بیش</p></div>
<div class="m2"><p>تیغها برکشیده از پس و پیش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یک دل و صد هزار سور و سرور</p></div>
<div class="m2"><p>یک تن و صد هزار چشمه نور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شد ز هیبت صبا عرق ریزان</p></div>
<div class="m2"><p>رفت چون ذرّه اوفتان خیزان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هرچه نزدیکتر به خور می شد</p></div>
<div class="m2"><p>از تف و تاب گرمتر می شد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون شود محترق صبا چون برق</p></div>
<div class="m2"><p>از صبا تا سموم نبود فرق</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گرچه طاقت نداشت هم خَش خَش</p></div>
<div class="m2"><p>رفت چون باد در دم آتش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همه پیغام شب گزارد به روز</p></div>
<div class="m2"><p>گرم شد مغز آفتاب از سوز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بانگ زد بر صبا که: «یاوہ مگوی</p></div>
<div class="m2"><p>ظلمت از طبع آفتاب مجوی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا که باشد شب محال اندیش</p></div>
<div class="m2"><p>که فرستد به من رسالت خویش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از شب آوارہ تر دگر که بود؟</p></div>
<div class="m2"><p>زو جگرخواره تر بتر که بود؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به تهوّر ز من سخن گوید</p></div>
<div class="m2"><p>که بود کو سخن ز من گوید!</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کی برازد سیه گلیمی را</p></div>
<div class="m2"><p>کو براند چو من کریمی را؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حکم بر من کند به آی و مآی</p></div>
<div class="m2"><p>بنگرید آخر از برای خدای»</p></div></div>