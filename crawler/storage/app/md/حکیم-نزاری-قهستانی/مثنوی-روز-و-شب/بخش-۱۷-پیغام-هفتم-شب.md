---
title: >-
    بخش ۱۷ - پیغام هفتم شب
---
# بخش ۱۷ - پیغام هفتم شب

<div class="b" id="bn1"><div class="m1"><p>شب دگر باره در محاکا شد</p></div>
<div class="m2"><p>چون مجازات بی محابا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت: «شوخی و حد ببردی تو</p></div>
<div class="m2"><p>خوش خوشم نیک برشمردی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من نه آنم که نسبتم کردی</p></div>
<div class="m2"><p>سخت بی وقع و رتبتم کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پار خلوت نشین عشاقم</p></div>
<div class="m2"><p>آرزومند یار مشتاقم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وقت ایشان همیشه خوش دارم</p></div>
<div class="m2"><p>همه شب شان پیاله کش دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر کویشان عسس باشم</p></div>
<div class="m2"><p>با شکر مانع مگس باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاه بر بام پاسبان باشم</p></div>
<div class="m2"><p>گاه در خانه میزبان باشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زخمه عود و زخمه طنبور</p></div>
<div class="m2"><p>روی خوب و خلاصه انگور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواهم آماده با حریف و ندیم</p></div>
<div class="m2"><p>منکرم گو سؤال کن ز حکیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که نخواهد سماع روحانی؟</p></div>
<div class="m2"><p>که ننوشد شراب ریحانی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خال مصباح چون برافروزد</p></div>
<div class="m2"><p>شب خلوت ز شمع من سوزد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی حضور وجود من در جمع</p></div>
<div class="m2"><p>ندهد هیچ روشنایی شمع</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس که خواهد که روی من بیند</p></div>
<div class="m2"><p>شب همه شب ز پای ننشیند»</p></div></div>