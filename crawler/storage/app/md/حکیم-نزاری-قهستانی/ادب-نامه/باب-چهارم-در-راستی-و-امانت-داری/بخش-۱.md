---
title: >-
    بخش ۱
---
# بخش ۱

<div class="b" id="bn1"><div class="m1"><p>به گفتار و کردار اگر راستی</p></div>
<div class="m2"><p>به یک دل دو عالم بیاراستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجز بوده باز از گذشته مگوی</p></div>
<div class="m2"><p>و گر می توانی در آن ره مپوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگو نیز در حال جز آنک هست</p></div>
<div class="m2"><p>چنان گو که بر حرف ننهند دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به آینده هرگز زبانت گرو</p></div>
<div class="m2"><p>مکن ور بگردی وفا کن برو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کردار در، لاابالی مباش</p></div>
<div class="m2"><p>بلی نه مقصر نه غالی مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو مابین این دو ره پیش گیر</p></div>
<div class="m2"><p>برو پای درکش سر خویش گیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر از حد و اندازه در نگذری</p></div>
<div class="m2"><p>چو صادق روی ره به مقصد بری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ازین جد و جهدت نباشد گریز</p></div>
<div class="m2"><p>اگر پند خواهی ز من یاد گیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برو یا اخی پیشه کن راستی</p></div>
<div class="m2"><p>اگر خدمت پادشا خواستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نصیحت به هنگام کردن نکوست</p></div>
<div class="m2"><p>طریق امانت سپردن نکوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بخشنودی از هر زبانی نیوش</p></div>
<div class="m2"><p>به جد باش در حق گزاری بکوش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان حشمت ملک واجب شناس</p></div>
<div class="m2"><p>که واجب کند بر تو هر دم سپاس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز باران خود جز نکویی مگوی</p></div>
<div class="m2"><p>ز چرک حسد دامن جان بشوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر این شرایط بجای آوری</p></div>
<div class="m2"><p>سر حاسدان زیر پای آوری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خردمند را راستی پیشواست</p></div>
<div class="m2"><p>خصوصا که در خدمت پادشاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز خائن که شد پیش شه متهم</p></div>
<div class="m2"><p>وجودی نماید در اندر عدم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرو افتد از روزن چشم شاه</p></div>
<div class="m2"><p>و گر چند مسکین بود بی گناه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه سود ار کند در امانت جهاد</p></div>
<div class="m2"><p>چو بروی نماند دگر اعتماد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو مذکور شد در خیانت کسی</p></div>
<div class="m2"><p>سوی شاه وقعش نماند بسی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برد رنج بی فایده سال و ماه</p></div>
<div class="m2"><p>چو نبود قبولش به نزدیک شاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بهین پبشه ای در جهان راستی است</p></div>
<div class="m2"><p>که بی راستی عین کم کاستی است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو در باطنش راستی جای کرد</p></div>
<div class="m2"><p>نباشد به جز راستی جای مرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر راست کرداری و راست گوی</p></div>
<div class="m2"><p>ربودی به چوگان اخلاص، گوی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کسی را که شد قول و فعلش یکی</p></div>
<div class="m2"><p>برو نیست در راستکاری شکی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به هر حال با پادشا راست گوی</p></div>
<div class="m2"><p>و گر هزل باشد ره راست جوی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که هزل ار بود راستی در سخن</p></div>
<div class="m2"><p>بهست ار دروغی که باشد حسن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو در هزل گفتی دروغ از نخست</p></div>
<div class="m2"><p>نگیرند اگر راست گویی درست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مگو ای برادر سخن بر گزاف</p></div>
<div class="m2"><p>که گر راست گویی محال است لاف</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگرچه ممالیک و مالک رقاب</p></div>
<div class="m2"><p>برابر نباشند در هیچ باب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خداوندگار است فرمان روان</p></div>
<div class="m2"><p>ممالیک مامور و بر سر دوان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ولیک از ره آفرینش بشر</p></div>
<div class="m2"><p>برابر نمایند در خیر و شر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به خود و به خلق و به طبع و سرشت</p></div>
<div class="m2"><p>کسی را در ابداع ضایع نهشت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر پادشاه جهان و گداست</p></div>
<div class="m2"><p>مکافات هر یک به واجب رواست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو تو داشتی حق خدمت نگاه</p></div>
<div class="m2"><p>قضا حق خدمت کند پادشاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به ناحق مکش دست در هر کسی</p></div>
<div class="m2"><p>که من حق پیشینه دارم بسی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو خدمت نکردی و از حق شناس</p></div>
<div class="m2"><p>مکافات دیدی نکو دار پاس</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پس از نیکویی ها اگر بد کنی</p></div>
<div class="m2"><p>به کردار بد چاه خود می کنی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به نیکی چو دیدی مکافات خویش</p></div>
<div class="m2"><p>به بد هم بد آید مکافات پیش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>حقوق ترا چون مکافات کرد</p></div>
<div class="m2"><p>به نیکی بساط بدی در نورد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز تو شاه اگر چه بود حق شناس</p></div>
<div class="m2"><p>همه عمر بر خود نگیرد سپاس</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو بر جاده خدمت خویش رو</p></div>
<div class="m2"><p>که او خود عنایت کند پیش رو</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دل خویش با شاه پاکیزه دار</p></div>
<div class="m2"><p>چو پاکیزه خواهی دل شهریار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر اعتقاد تو پاک است و راست</p></div>
<div class="m2"><p>قدم در نه و هر چه خواهی تراست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>و گر خوار گیری ره خدمتش</p></div>
<div class="m2"><p>به خود بر، ببندی در نعمتش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو تو راه خدمت ز تقصیر خویش</p></div>
<div class="m2"><p>نگیری به اخلاق و اشفاق پیش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به برق تغیر مبادا که شاه</p></div>
<div class="m2"><p>تر و خشک جاهت بسوزد چو کاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>برو ای برادر چو من راست باش</p></div>
<div class="m2"><p>چنانت که شاه آن چنان خواست باش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به از نیک نامی درختی نخاست</p></div>
<div class="m2"><p>وزین شاخ نیکوترین بر، وفاست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سه نوع است در باغ دانش وفا</p></div>
<div class="m2"><p>که آن هر سه باشد در اهل صفا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>یکی آنکه در وعده و در ضمان</p></div>
<div class="m2"><p>نیابد خلاف از تو در هر زمان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دویم چون ملک شد به تو نیک بین</p></div>
<div class="m2"><p>بکوشی که آن ظن بباشد یقین</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سدیگر حق نعمت پادشاه</p></div>
<div class="m2"><p>به هر وقت و هر حال داری نگاه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بدین هر سه خصلت شوی نیکنام</p></div>
<div class="m2"><p>ملک از تو خشنود باشد مدام</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تواضع پسند و تذلل رواست</p></div>
<div class="m2"><p>خصوصا که در محضر پادشاست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به عزت گرفتن ره بندگی</p></div>
<div class="m2"><p>به بیچارگی و سرافکندگی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ولیکن چو زین جایگه بگذری</p></div>
<div class="m2"><p>نشاید تواضع که از حد بری</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چنان کن تواضع که اهل فسوس</p></div>
<div class="m2"><p>تصور نبندند بر چاپلوس</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>فریب و تملق مبر پیش شاه</p></div>
<div class="m2"><p>که نفرت کند در دلش جایگاه</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نگهدار اندازه بندگی</p></div>
<div class="m2"><p>زبان آوری و سرافکندگی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به اندازه باید که خدمت کنی</p></div>
<div class="m2"><p>نه چندان که مردم به تهمت کنی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خیانت که با پادشا کرده اند</p></div>
<div class="m2"><p>از انواع در دفتر آورده اند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ولیکن خیانت بتر زان مخواه</p></div>
<div class="m2"><p>از آن کو کشد دست در ملک شاه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>که کمتر خیانت بر خرده بین</p></div>
<div class="m2"><p>بزرگ است در معرض ملک و دین</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>خیانت روا نیست با پادشاه</p></div>
<div class="m2"><p>که خورشید ملک است و ظل اله</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>زبد پاک کن باطن و ظاهرت</p></div>
<div class="m2"><p>چو کردی نخوانند جز طاهرت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>میندیش کاندیشه نیک و بد</p></div>
<div class="m2"><p>که من می کنم کس بر آن نگذرد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>پدید است نیکی ز پیشانی ات</p></div>
<div class="m2"><p>بدی از همه عضو انسانی ات</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کسی کو به اصل و به گوهر نکوست</p></div>
<div class="m2"><p>ره پادشا را سزاوار اوست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>که اصل است سرمایه هر هنر</p></div>
<div class="m2"><p>بود بی خطر آهن بی گهر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ستور آخر از تخم نیکو خرند</p></div>
<div class="m2"><p>که از آدمی هم فراوان خرند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو از اسب و خر می گزینی اصیل</p></div>
<div class="m2"><p>زمردم گزین کن شریف و جمیل</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>اصیل از خیانت مبرا بود</p></div>
<div class="m2"><p>شکیبا به سرا و ضرا بود</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نه بدنفسی و بی وفایی کند</p></div>
<div class="m2"><p>نه بی شرمی و ژاژخایی کند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بداند حدود ره و جای خویش</p></div>
<div class="m2"><p>برون ننهد از راستی پای خویش</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>زکفران نعمت کند اجتناب</p></div>
<div class="m2"><p>گرش کرد باید غذا از تراب</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ز ناخوب باز آردش اصل پاک</p></div>
<div class="m2"><p>چو در اصل خوب آمدش آب و خاک</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بدین هر سه خصلت شوی نیکنام</p></div>
<div class="m2"><p>ملک از تو خشنود باشد مدام</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>تواضع پسند و تذلّل رواست</p></div>
<div class="m2"><p>خصوصاً که در محضر پادشاست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به عزّت گرفتن رهِ بندگی</p></div>
<div class="m2"><p>به بیچارگی و سرافکندگی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ولیکن چو زین جایگه بگذری</p></div>
<div class="m2"><p>نشاید تواضع که از حدّ بری</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چنان کن تواضع که اهل فسوس</p></div>
<div class="m2"><p>تصوّر نبندند بر چاپلوس</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>فریب و تملّق مَبَر پیش شاه</p></div>
<div class="m2"><p>که نفرت کند در دلش جایگاه</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نگهدار اندازهء بندگی</p></div>
<div class="m2"><p>زبان‌آوری و سرافکندگی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به اندازه باید که خدمت کنی</p></div>
<div class="m2"><p>نه چندان که مردم به تهمت کنی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>خیانت که با پادشا کرده‌اند</p></div>
<div class="m2"><p>ز انواع در دفتر آورده‌اند</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ولیکن خیانت بَتَر زان مخواه</p></div>
<div class="m2"><p>از آن کو کَشَد دست در مُلک شاه</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>که کمتر خیانت برِ خرده‌ بین</p></div>
<div class="m2"><p>بزرگ است در معرض ملک و دین</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>خیانت روا نیست با پادشاه</p></div>
<div class="m2"><p>که خورشید مُلک است و ظلّ اله</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>زِ بد پاک کن باطن و ظاهرت</p></div>
<div class="m2"><p>چوکردی نخوانند جز طاهرت</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>میندیش کاندیشه نیک و بد</p></div>
<div class="m2"><p>که من میکنم کس بر آن نگذرد</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>پدید است نیکی ز پیشانی‌ات</p></div>
<div class="m2"><p>بَدی از همه عضو انسانی‌ات</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>کسی کو به اصل و به گوهر نکوست</p></div>
<div class="m2"><p>ره پادشا را سزاوار اوست</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>که اصل است سرمایه هر هنر</p></div>
<div class="m2"><p>بود بی‌خطر آهن بی‌گوهر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ستور آخر از تخم نیکو خرند</p></div>
<div class="m2"><p>که از آدمی هم فراوان خرند</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>چو از اسب و خر می‌گزینی اصیل</p></div>
<div class="m2"><p>ز مردم گزین کن شریف و جمیل</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>اصیل از خیانت مُبرّا بُوَد</p></div>
<div class="m2"><p>شکیبا به سَرّا وَ ضرّا بُوَد</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>نه بد نفسی و بی‌وفایی کند</p></div>
<div class="m2"><p>نه بی‌شرمی و ژاژخایی کند</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>بداند حدود رَه و جای خویش</p></div>
<div class="m2"><p>برون ننهد از راستی پای خویش</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ز کفران نعمت کند اجتناب</p></div>
<div class="m2"><p>گرش کرد باید غذا از تُراب</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ز ناخوب باز آردش اصل پاک</p></div>
<div class="m2"><p>چو در اصل خوب آمدش آب و خاک</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>نکو نیّتی مایه مردمی است</p></div>
<div class="m2"><p>پسندیده‌تر خلقت آدمی است</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>خنک نَفْس خدمتگر پادشاه</p></div>
<div class="m2"><p>که باشد نکو نیّت و نیکخواه</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چو بر حکم نیّت عزیمت کنی</p></div>
<div class="m2"><p>سپاه عَدو را هزیمت کنی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بد و نیک هرچ آدمیزاد خواست</p></div>
<div class="m2"><p>هم از نیّت خیر گشتست راست</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>به غفلت نرفتست هر کار پیش</p></div>
<div class="m2"><p>مجاهد مصمّم کند عزم خویش</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>چو بی دل کنی در مصالح شروع</p></div>
<div class="m2"><p>از آن کرد باید به خصلت رجوع</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>کسانی که خدمت کنند اختیار</p></div>
<div class="m2"><p>به پیکار بر خود نگیرند کار</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>چو مردان به کاری کنند ابتدا</p></div>
<div class="m2"><p>به تصدیق نیّت کنند اقتدا</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>بِجِد باش در راه خدمتگری</p></div>
<div class="m2"><p>تکاسُل ز یکسو نهی بگذری</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>طمع بگسل از وایه خویشتن</p></div>
<div class="m2"><p>همین ساز سرمایه خویشتن</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>به تقدیر ایزد شود کارها</p></div>
<div class="m2"><p>تو خود آزمون کرده‌ای بارها</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بکوش، ار شود کار مشکور باش</p></div>
<div class="m2"><p>و گرنه به تقدیر معذور باش</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>به هر چیز همّت چنان بر گمار</p></div>
<div class="m2"><p>که برنارد از روزگارت دمار</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>نه چیزی طلب کز طلب کردنت</p></div>
<div class="m2"><p>بلایی کند پای در گردنت</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چو در بستن همّت از حد گذشت</p></div>
<div class="m2"><p>دو دیوانگی لازم حال گشت</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>اگر یافت مقصود عینِ بلاست</p></div>
<div class="m2"><p>وگرنه به حسرت شدن مبتلاست</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>مزن جز به نیکویی یار فال</p></div>
<div class="m2"><p>که از نیک و از بد برون نیست حال</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چو نیکو زدی فال بر گشت بد</p></div>
<div class="m2"><p>و گر بَد زدی از که نالی ز خود</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>سخن های اصحاب دولت‌سرای</p></div>
<div class="m2"><p>ز اخبار و آثار ایشان درآی</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>ز نصرت سخن گوی و فتح و ظفر</p></div>
<div class="m2"><p>ز تخت و سریر و کلاه و کمر</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>ز عدل و ز انصاف و داد و دهش</p></div>
<div class="m2"><p>ز ناز و نعیم و ز راه و روش</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>ز ازداد اینها مگو پیش شاه</p></div>
<div class="m2"><p>که بر خاطرش یابد اکراه راه</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>نزاری به خدمت بسی دم زدی</p></div>
<div class="m2"><p>بد و نیک هرگونه در هم زدی</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>نگشتی ز آسایش و رنج سیر</p></div>
<div class="m2"><p>گهی بَد دلت یافتم گه دلیر</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>سر بندگی پیش و دست رضا</p></div>
<div class="m2"><p>زده در گریبان حکم قضا</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>کمر بر میان بسته در بند عشق</p></div>
<div class="m2"><p>بریده دل از غیر پیوند عشق</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>به بوی محبّت جگر سوخته</p></div>
<div class="m2"><p>تر و خشک بر همدگر سوخته</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>ز خویش وز بیگانه در عشق دوست</p></div>
<div class="m2"><p>بریدن دل و برشکستن نکوست</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>ز خود نیست بی خویشتن بی خبر</p></div>
<div class="m2"><p>گرانبار و همچون شتر ره سپر</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>گهی مست بودی گهی هوشیار</p></div>
<div class="m2"><p>گهی سرخ روی و گهی شرمسار</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>نه موقوف جاه و نه مشغول مال</p></div>
<div class="m2"><p>بر آوازه عشق آورده حال</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>چو دیدند کز خلق بیگانه ای</p></div>
<div class="m2"><p>بر آن بود هر کس که دیوانه ای</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>زبان در کشیدند و فتوی نوشت</p></div>
<div class="m2"><p>که با خاکْ خونش بباید سرشت</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>نبودند آگه ز اسرار تو</p></div>
<div class="m2"><p>به خون سعی کردند در کار تو</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>ولیکن ز تو بی خبر بوده اند</p></div>
<div class="m2"><p>همه بی عزا نوحه گر بوده اند</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>مرا نیست مقصود جز دوست، دوست</p></div>
<div class="m2"><p>به مَردش مدان کش نه مقصود اوست</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>به ایزد که جز حق نبودم غرض</p></div>
<div class="m2"><p>دل خسته از عشق و تن بی مرض</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>بدین دَردها بسته مردی بود</p></div>
<div class="m2"><p>که هر ساعتش تازه دَردی بود</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>تَوانی نشستن به صبر و وقار</p></div>
<div class="m2"><p>گرت نیست دردی چو من بی قرار</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>اگر چند همسایه باشد نژند</p></div>
<div class="m2"><p>ز فریاد همسایه دردمند</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>ولیکن ز افسرده تا تافته</p></div>
<div class="m2"><p>تفاوت نهد تجربت یافته</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>ندارد خبر خوشدل از غم زده</p></div>
<div class="m2"><p>نباشد مُعاهِر چو ماتم زده</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>کسی را بُوَد سوز برق مَنَش</p></div>
<div class="m2"><p>که در جانش افتد نه در دامنش</p></div></div>