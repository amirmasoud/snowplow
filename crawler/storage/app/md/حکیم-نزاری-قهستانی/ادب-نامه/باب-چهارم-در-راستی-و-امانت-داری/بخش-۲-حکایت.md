---
title: >-
    بخش ۲ - حکایت
---
# بخش ۲ - حکایت

<div class="b" id="bn1"><div class="m1"><p>قوی همّتی از رجال کِبار</p></div>
<div class="m2"><p>که بر وعظ بردی به سر روزگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به اصحاب تنبیه کردی مدام</p></div>
<div class="m2"><p>که از نفس باید کشید انتقام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکوشید تا گر ز ساقی دَوْر</p></div>
<div class="m2"><p>به نوبت رسد با شما جام جَوْر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توانید در کام جان ریختن</p></div>
<div class="m2"><p>نه جستن به سستی، نه بگریختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برابر گره بستن از تلخ و شور</p></div>
<div class="m2"><p>نه مردی بُوَد گر فقیری مشور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر شیخ را مادر اندر گذشت</p></div>
<div class="m2"><p>مزاج وقار و قرارش بگشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتاد آتش رقّتش در نهاد</p></div>
<div class="m2"><p>ز چشم آب حسرت به رُخ برگشاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مریدانْش در گفت و گوی آمدند</p></div>
<div class="m2"><p>هنرمند را عیب جوی آمدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که اصحاب را وعظ گفتی و پند</p></div>
<div class="m2"><p>ترا هیچ ازینها نیامد پسند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نصیحت مگر جمله بر گفته ای</p></div>
<div class="m2"><p>یکی بهر خود باز نگرفته ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدیشان چنین گفت اصحاب کار</p></div>
<div class="m2"><p>کزین نیز باید گرفت اعتبار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز غم جان همسایه پر دَرد بود</p></div>
<div class="m2"><p>بر آتش ز مرگ زن و مرد بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از ایشان به ما جز خبر نامدی</p></div>
<div class="m2"><p>نم از چشم و سوز از جگر نامدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کنون درد همسایه بر جان ماست</p></div>
<div class="m2"><p>مرا بد فراقی که همسایه راست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هنوز آنک هرگز نخور دست مَی</p></div>
<div class="m2"><p>به جز نام مستی نداند ز مَی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه غم دارد آسوده از دردمند</p></div>
<div class="m2"><p>نبخشود قصّاب بر گوسپند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیفتاده در درد و ناخسته حلق</p></div>
<div class="m2"><p>ندارد خبر زین دو بیچاره خلق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تماشا کنان بر لب قُلْزُمی</p></div>
<div class="m2"><p>نه در موج طوفان، از آن بی غمی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>توان دید بازی بسی بر کنار</p></div>
<div class="m2"><p>توان کرد نظّاره کارزار</p></div></div>