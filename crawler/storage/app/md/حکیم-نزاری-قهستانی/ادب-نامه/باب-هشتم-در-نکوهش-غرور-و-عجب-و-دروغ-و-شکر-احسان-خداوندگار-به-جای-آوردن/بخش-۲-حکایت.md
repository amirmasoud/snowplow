---
title: >-
    بخش ۲ - حکایت
---
# بخش ۲ - حکایت

<div class="b" id="bn1"><div class="m1"><p>به بغداد بُد شاعری کامکار</p></div>
<div class="m2"><p>شد از جعفر برمکی نامدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن گوی و درّاک و حاضر جواب</p></div>
<div class="m2"><p>زبانی چو آتش حدیثی چو آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جو تیغ زبان در سخن راندی</p></div>
<div class="m2"><p>لب خشمگین را بخنداندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندیمی چو او در ملاحت نبود</p></div>
<div class="m2"><p>نظیرش به نطق و فصاحت نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر بود دون‌همّت از حرص و آز</p></div>
<div class="m2"><p>به رویش نمیشد در خیر باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توانگر به انعام جعفر به مال</p></div>
<div class="m2"><p>به سر بُرده در صدر او چند سال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر کرد روزی هوای ندیم</p></div>
<div class="m2"><p>ز گنجور درخواست طبع کریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیاورد دینار زر یک هزار</p></div>
<div class="m2"><p>بفرمود باخادم حق گزار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که بردار با من بیاور به راه</p></div>
<div class="m2"><p>هر آنگه که سوی تو کردم نگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنه پیش او تا شود شادکام</p></div>
<div class="m2"><p>چنین کرد جوینده ننگ و نام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو جعفر در آمد به صحن سرای</p></div>
<div class="m2"><p>بپژمرد طبعش از آن تیره جای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگه کرد در خانه چیزی ندید</p></div>
<div class="m2"><p>ز آثار نعمت پشیزی ندید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه در سر نه در بر نه در خانه هیچ</p></div>
<div class="m2"><p>زبنگاه دون‌همّتان ره بپیچ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حصیری به صدپاره ده ساله زیر</p></div>
<div class="m2"><p>گذشتن زباروی آن خانه دیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خُمی سرشکسته سبویی کهن</p></div>
<div class="m2"><p>میفزای اینجا دگر بر سخن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو آگاه گشت از کم و بیش او</p></div>
<div class="m2"><p>پشیمان شد از آمدن پیش او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به خادم نظر کرد و بر زد گره</p></div>
<div class="m2"><p>بر ابرو که زر پیش احمق منه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نکرد التفاتی به شاعر وزیر</p></div>
<div class="m2"><p>نیامد حدیثش دگر جایگیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بسی هزل و جِد گفت و سودش نداشت</p></div>
<div class="m2"><p>نه لب بر گشاد و نه سر برفراشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برون رفت جعفر ز نزدیک او</p></div>
<div class="m2"><p>تُفو کرد بر جان تاریک او</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یکی گفتش ای آفتاب عرب</p></div>
<div class="m2"><p>دو چیز از تو امروز دیدم عجب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکی آنکه خود را نگه داشتی</p></div>
<div class="m2"><p>به گوش آن همه هزل بگذاشتی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به چندان مضاحک نکردی نشاط</p></div>
<div class="m2"><p>فرح را چنان در نوشتی بساط</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دویم آنکه نارفته بروی خطا</p></div>
<div class="m2"><p>گرفتی ازو باز رسم عطا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دژم بودن و بخل کردن چنین</p></div>
<div class="m2"><p>نبودی ترا رسم و خو پیش ازین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدو گفت جعفر که صاحب خرد</p></div>
<div class="m2"><p>ز اندرز پیشینگان نگذرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مده سر شبانی به گرگ ای سلیم</p></div>
<div class="m2"><p>که جوری صریح است و ظلمی عظیم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مریز ای پسر دانه در شور بوم</p></div>
<div class="m2"><p>که شاخ شکر نی، نرست از زقوم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نمی‌دانم از غصة آن گدای</p></div>
<div class="m2"><p>که چونم؟ به سر می روم یا به پای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عطایی بدان میدهد نامجوی</p></div>
<div class="m2"><p>که چون دخترانش بپوشند روی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دریغ است احسان به ناحق شناس</p></div>
<div class="m2"><p>نسیجش فرستی بپوشد پلاس</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بود گنج دادن به ناارجمند</p></div>
<div class="m2"><p>به نزدیک خَلق و خدا ناپسند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هبا بود در حق آن مرد ریگ</p></div>
<div class="m2"><p>همه سعی من همچو روغن به ریگ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نبیند ز من باز هرگز عطا</p></div>
<div class="m2"><p>نگردم دگرباره گرد خطا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به ناقص وجود آنکه بخشد درم</p></div>
<div class="m2"><p>چو من بایدش باز جست از عدم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گرفتم نظر باز از آن بی خبر</p></div>
<div class="m2"><p>به چشم و دلم درنیاید دگر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نبینی که ماه منازل نورد</p></div>
<div class="m2"><p>چو اظهار احسان خورشید کرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از آنجا که تعظیم خور می کند</p></div>
<div class="m2"><p>نظر سوی او بیشتر می کند</p></div></div>