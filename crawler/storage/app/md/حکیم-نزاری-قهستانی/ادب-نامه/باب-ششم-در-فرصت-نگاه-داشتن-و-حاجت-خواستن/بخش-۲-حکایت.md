---
title: >-
    بخش ۲ - حکایت
---
# بخش ۲ - حکایت

<div class="b" id="bn1"><div class="m1"><p>شنیدم که وقتی علی الاتفاق</p></div>
<div class="m2"><p>سفر کرد عطّار سوی عراق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مَه روزه برخاست از بامداد</p></div>
<div class="m2"><p>ز دروازۀ شهر بیرون فتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به حاجت درون، شد به ویرانه‌ای</p></div>
<div class="m2"><p>سبک گشت ساکن به گوشانه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم آنجا به گوش آمدش بانگ رود</p></div>
<div class="m2"><p>که می گفت با رود شخصی سرود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آهنگ آواز او رفت پیش</p></div>
<div class="m2"><p>کهن هندویی دید مجروح و ریش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشسته به بیغولۀ خانه‌ای</p></div>
<div class="m2"><p>سبو کهنه‌ای پیش با لانه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برآوردی از درد دست نیاز</p></div>
<div class="m2"><p>که هستی تو بینا و دانای راز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز تو درد و درمان، ز تو نار و نور</p></div>
<div class="m2"><p>ببخشای بر عجز من یا غفور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بکردی نیاز و بخوردی شراب</p></div>
<div class="m2"><p>سرودی بکردی به هندی رباب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو عطّار را دید مسکین غلام</p></div>
<div class="m2"><p>بیفتاد و گفتی مگر شد تمام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گمان برد لالای گَنده بغل</p></div>
<div class="m2"><p>که عطّار با محتسِب شد بَدَل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیامد به خود روزگاری دراز</p></div>
<div class="m2"><p>پشیمان شد از کار خود پاکباز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سر آن ضعیف از زمین بر گرفت</p></div>
<div class="m2"><p>به صد لابه با او سخن در گرفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که از من چه می‌ترسی ای دردمند</p></div>
<div class="m2"><p>نظر برگشای از سخن لب مبند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غریبم نَه‌ام محتسب نی فقیه</p></div>
<div class="m2"><p>نیاید ز من کار مرد سفیه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به زیر زبان گفت لالای پیر</p></div>
<div class="m2"><p>که گر راست گویی یکی می بگیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرو ماند بیچاره در کارِ وی</p></div>
<div class="m2"><p>نَبُد خورده در مدت عمر می</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مه روزه و مرد پرهیزگار</p></div>
<div class="m2"><p>به خود گفت بس مشکل افتاد کار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو خواهد شد این مرد مسکین هلاک</p></div>
<div class="m2"><p>نگیرد به اینم جهان دار پاک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دلی گر به دست آورد مرد راز</p></div>
<div class="m2"><p>هَمَش روزه مقبول شد هم نماز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درین نیز سرّی بوَد مرد راز</p></div>
<div class="m2"><p>که سوزی است هر جنبش درد راز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سرِ بی‌خبر از کنارای شگفت</p></div>
<div class="m2"><p>فرا کرد دست از زمین بر گرفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فروریخت آن لانۀ پر شراب</p></div>
<div class="m2"><p>رخ و چشم بستردش از خاک و آب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدادش از آن لانۀ پر یکی</p></div>
<div class="m2"><p>ز خود رفته آمد به هوش اندکی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز احوال او شمّه‌ای باز جُست</p></div>
<div class="m2"><p>خبر دادش از علّت خود درست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو درمانش از آب انگور بود</p></div>
<div class="m2"><p>در آن درد بیچاره معذور بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برون رفت عطّار از آن تیره جای</p></div>
<div class="m2"><p>مریدانش استاده بر در به پای</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روان شد به سر منزل خانقاه</p></div>
<div class="m2"><p>مگر محتسب پیشش آمد به راه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گریبان گرفتش به قاضی کشید</p></div>
<div class="m2"><p>که خورده‌ست این نامسلمان نبید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدو گفت قاضی که می خورده‌ای</p></div>
<div class="m2"><p>بلی گفت، گفتش خطا کرده‌ای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به دین تو گویی حلال است می</p></div>
<div class="m2"><p>ز دینش بپرسید و از کیش وی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدو گفت در کیش ترسا رواست</p></div>
<div class="m2"><p>که هستم درین کیش چون تیر راست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نگه کرد قاضی بدان فرّ و ریب</p></div>
<div class="m2"><p>دریغ آمدش گفت ننگ است و عیب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو تو روح پاکی که ترسا بوَد</p></div>
<div class="m2"><p>چنین روح مطلق مسیحا بوَد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدین فر و فرهنگ و نور و نوا</p></div>
<div class="m2"><p>مسلمان نباشی نباشد روا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدو گفت عطار ای رهنمای</p></div>
<div class="m2"><p>مرا دیر شد تا بر این است رای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ندیدم کسی را به عهد استوار</p></div>
<div class="m2"><p>که بر وی دل من گرفتی قرار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ولیکن ترا گر چنین است خواست</p></div>
<div class="m2"><p>که بر من کنی عرضه ایمان رواست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بنازید از آن شیخ اسلام سخت</p></div>
<div class="m2"><p>زر آورد چندی و هر گونه رخت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در اسلام آیین او تازه کرد</p></div>
<div class="m2"><p>نبد هیچ نقصان در ایمان مرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>برون آمد آسوده و جمع شاد</p></div>
<div class="m2"><p>وزان مالی نیمی به درویش داد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دگر نیمه در خرج اصحاب کرد</p></div>
<div class="m2"><p>برانگیخت در رفتن از آب گرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یکی گفتش ای مقتدای رجال</p></div>
<div class="m2"><p>به یاران خبر ده ازین سرّ حال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که آغاز چون بود و انجام کار</p></div>
<div class="m2"><p>برون آر چندین دل از زیر بار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به آنها که بودند از اهل راز</p></div>
<div class="m2"><p>فرید آن مقامات را گفت باز</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همین است مقصودش ای راز جوی</p></div>
<div class="m2"><p>که گر مرد راهی دلی باز جوی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به دست آر دل طاعت این است و بس</p></div>
<div class="m2"><p>دلِ اهل دل اصل دین است و بس</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز ابلیس کس طاعت افزون نکرد</p></div>
<div class="m2"><p>چو مغرور شد عاقبت زخم خورد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو نگزاردی طاعتِ آب و گل</p></div>
<div class="m2"><p>نبینی که چونت بیاسود دل</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پس از طاعت معنوی کن قیاس</p></div>
<div class="m2"><p>دلی را به دست آور ای حق شناس</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خدا از تو خشنود آنگه بوَد</p></div>
<div class="m2"><p>که از تو به اهل دلی ره بوَد</p></div></div>