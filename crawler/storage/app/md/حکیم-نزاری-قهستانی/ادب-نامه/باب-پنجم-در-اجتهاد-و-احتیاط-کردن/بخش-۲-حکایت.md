---
title: >-
    بخش ۲ - حکایت
---
# بخش ۲ - حکایت

<div class="b" id="bn1"><div class="m1"><p>یکی مرد نحوی به کشتی نشست</p></div>
<div class="m2"><p>درِ راحت و امن بر خود ببست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز کشتی یکی مدرسه ساخته</p></div>
<div class="m2"><p>دماغ از خرد پاک پرداخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو لنگر گرانجان چو کشتی سبک</p></div>
<div class="m2"><p>به کوشش ضعیف و به دانش تنک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز ملاح روزی سر آسیمه مرد</p></div>
<div class="m2"><p>سوالی عَجَب از سر عُجب کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که در علم نحو از اصول و فروع</p></div>
<div class="m2"><p>کم و بیش هرگز نمودی شروع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو گفت در عین خوف و رجا</p></div>
<div class="m2"><p>که نحو از کجا، ناخدا از کجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ملاح گفت و بر افشاند دست</p></div>
<div class="m2"><p>که یک نِصفت از عمر ضایع شُدَست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قضا را به کشتی در امد خَلَل</p></div>
<div class="m2"><p>نیارست ملّاح کردن عمل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به نحوی چنین گفت ملاح گُرد</p></div>
<div class="m2"><p>در ان دم که بایست ناچار مرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که هان، خواجه در صنعت اشنا</p></div>
<div class="m2"><p>کدام است؟ بیگانه، یا آشنا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دَرد دلش داد نحوی جواب</p></div>
<div class="m2"><p>که من آشنایی ندانم در آب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدو گفت رَو کار شد بر تو سهل</p></div>
<div class="m2"><p>که کردی همه عمر ضایع به جهل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرورفت بیچاره و ناخدا</p></div>
<div class="m2"><p>بزد دست و پایی و زو شد جدا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیاموز در آب رفتن نخست</p></div>
<div class="m2"><p>پس آنگه به کشتی نشین جَلد و چُست</p></div></div>