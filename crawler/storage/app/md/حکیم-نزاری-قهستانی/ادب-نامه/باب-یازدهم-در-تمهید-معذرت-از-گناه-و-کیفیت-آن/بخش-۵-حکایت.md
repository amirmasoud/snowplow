---
title: >-
    بخش ۵ - حکایت
---
# بخش ۵ - حکایت

<div class="b" id="bn1"><div class="m1"><p>در ایّام کسری روشن روان</p></div>
<div class="m2"><p>که عدلش متین بود و حکمش روان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ستاره‌ شناسان آن روزگار</p></div>
<div class="m2"><p>مگر بازگفتند با شهریار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که یک هفته بارندگی ها بود</p></div>
<div class="m2"><p>وزان پس پراکندگی ها بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان آب ها جمله گردد تباه</p></div>
<div class="m2"><p>شود مردمان را دگر رسم و راه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وزان آب دیوانه گردند خلق</p></div>
<div class="m2"><p>بساط خرد درنوردند خلق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین گفت کسری بدیشان جواب</p></div>
<div class="m2"><p>که یک ماهه دارند آماده آب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر این نحوست زما بگذرد</p></div>
<div class="m2"><p>زمانه طریق جفا نسپرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو وقتش درآمد بیارید میغ</p></div>
<div class="m2"><p>نیاهیخت خورشید یک هفته تیغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس از هفته ای کاسمان گشت صاف</p></div>
<div class="m2"><p>هوا میغ را زد به هم بر، مصاف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملک ماند کز خود نه بیگانه بود</p></div>
<div class="m2"><p>دگر جمله خلق دیوانه بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شه را ندیدند همرنگ خویش</p></div>
<div class="m2"><p>برو نیز کردند آهنگ خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گروهی ز خمر جنون گشته مست</p></div>
<div class="m2"><p>به کشتن کشیدند در شاه دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو کسری چنان دید عاجز بماند</p></div>
<div class="m2"><p>حکیمان دانسته را پیش خواند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ملک گفت درمان این درد چیست</p></div>
<div class="m2"><p>بباید درین بد، نکو بنگریست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنان دید رأی حکیمی صواب</p></div>
<div class="m2"><p>که شاه و حکیمان نخوردند آب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سپاه و رعایا و شاه و وزیر</p></div>
<div class="m2"><p>نبودند همرنگ چون آب و شیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خوشا وقت مردان یکروی و رنگ</p></div>
<div class="m2"><p>برون رفته از معرض نام و ننگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بریده ز کلّ علایق طمع</p></div>
<div class="m2"><p>گرفته کم خمر وترک ورع</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برآورده در گوشه انزوا</p></div>
<div class="m2"><p>چو داود در پرده حق نوا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خلیلانه آتش برافروخته</p></div>
<div class="m2"><p>تر و خشک هستی خود سوخته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز فرعون ببریده همچون کلیم</p></div>
<div class="m2"><p>زده نیل غالی و قاصر دو نیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو عیسی ز امّت تبرّا زده</p></div>
<div class="m2"><p>سم خر ز بهر خران وازده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شده چون حبیب خدا مردوار</p></div>
<div class="m2"><p>به نان جوین قانع از روزگار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به دامن درآورده پای سفر</p></div>
<div class="m2"><p>ز سبع السّموات خوانده خبر</p></div></div>