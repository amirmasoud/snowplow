---
title: >-
    بخش ۲ - حکایت
---
# بخش ۲ - حکایت

<div class="b" id="bn1"><div class="m1"><p>به تبریز دیدم جوانی سخی</p></div>
<div class="m2"><p>که میخواندنش مشایخ اخی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بیرون شهرش مقامات بود</p></div>
<div class="m2"><p>به خدمت ملازم در اوقات بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یک موضعش خانقه ساخته</p></div>
<div class="m2"><p>به درویش و محتاج پرداخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دیگر طرف داشت میخانه ای</p></div>
<div class="m2"><p>طرب را بر آورده کاشانه ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب حوض،روی چمن،پای بید</p></div>
<div class="m2"><p>سراسر گل و سبزه بر مرز و خوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مقامی خوش القّصه و جای من</p></div>
<div class="m2"><p>فروشد به گنج طرب پای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آمد به اخلاص باری چنان</p></div>
<div class="m2"><p>که پیشم فدا کرد پیوند جان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پسر را بیاورد و پیشم کشید</p></div>
<div class="m2"><p>که مهمان نخواهم دگر چون تو دید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جگر گوشه ای بود بس دلپذیر</p></div>
<div class="m2"><p>ازو در پذیرفتمش ناگزیز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جوانی خردمند و شایسته بود</p></div>
<div class="m2"><p>به خدمتن میان بسته پیوسته بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زِهر در حکایت در انداختی</p></div>
<div class="m2"><p>زِ هر باب فصلی بپرداختی</p></div></div>