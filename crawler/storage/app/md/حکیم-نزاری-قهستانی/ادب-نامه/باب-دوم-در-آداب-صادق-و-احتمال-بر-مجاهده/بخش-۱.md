---
title: >-
    بخش ۱
---
# بخش ۱

<div class="b" id="bn1"><div class="m1"><p>در ابداع، قسّام لیل ونهار</p></div>
<div class="m2"><p>نهادست ارکان عالم چهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برین چار هر یک سه معنی دهد</p></div>
<div class="m2"><p>که شرحش زبان قلم می دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود آتشش سرکش و تند و تیز</p></div>
<div class="m2"><p>چو بادش لطیف و سبک، زود خیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو آبش شتابنده و نرم و پاک</p></div>
<div class="m2"><p>امین خویشتن دار ساکن چو خاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی خدمت پادشا را نکوست</p></div>
<div class="m2"><p>که این چند اخلاق در نفس اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرای دو در در همه روزگار</p></div>
<div class="m2"><p>بدین چار ارکان بود استوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی کو به خدمت ببندد میان</p></div>
<div class="m2"><p>تقرّب نماید به صدر کیان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر دارد این چند خصلت نخست</p></div>
<div class="m2"><p>کند آنگه ارکان خدمت درست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طمع جز به ناکامی و رنج و غم</p></div>
<div class="m2"><p>مکن چون نهادی به خدمت قدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که چون دل نهادی به آسودگی</p></div>
<div class="m2"><p>تناور نباشی به فرسودگی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به آسانی و راحت خویشتن</p></div>
<div class="m2"><p>به جور طمع در مده بیش، تن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه دانی مگر پیشت آید غمی</p></div>
<div class="m2"><p>که با خویشتن برنیایی همی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو با دل بدادی به اوّل قرار</p></div>
<div class="m2"><p>در آن غم دلت کم کند اضطرار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>و گر پادشا از سر کبر خویش</p></div>
<div class="m2"><p>شود طیره روزی ز اندازه بیش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نگویی که این طیرگی از کجاست</p></div>
<div class="m2"><p>که از خوف سلطان دری در رجاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ملک از سر نخوت و خیرگی</p></div>
<div class="m2"><p>کند بی سبب گه گهی طیرگی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وگر با تو کرد از درشتی خطاب</p></div>
<div class="m2"><p>چنان دان که با غیر تست آن عتاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو خود را غلط گیر و حجّت منه</p></div>
<div class="m2"><p>کراھیتی زان به دل ره مده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به خدمت بود آن سزاوارتر</p></div>
<div class="m2"><p>که هر دم کند شکر بسیارتر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر چه مقصّر نباشی ولیک</p></div>
<div class="m2"><p>به جدّ کوش در عذر تقصیر نیک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به فعل و عمل پای برجای دار</p></div>
<div class="m2"><p>که قول مجرّد نباید بکار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حریصی به خدمت سعادت شناس</p></div>
<div class="m2"><p>که بدبخت دائم بود ناسپاس</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو راحت طمع می کنی از معاش</p></div>
<div class="m2"><p>مشقّت کش و خویشتن بین مباش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بهش باش و خود را فرامش مکن</p></div>
<div class="m2"><p>بر اندیش از روزگار کهن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شنیدی که در عزّ و اقبال و ناز</p></div>
<div class="m2"><p>فرامش نشد پوستین بر ایاز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو هم چون ز آثار اقبال شاه</p></div>
<div class="m2"><p>گرامی شدی باز خواری مخواه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به دولت رسیدی و بر بست مال</p></div>
<div class="m2"><p>به اندک مذلّت که دیدی منال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به یک ساله خدمت چنین بر مناز</p></div>
<div class="m2"><p>به زودی برندت به سر رشته باز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خردمند نیک اختر نیکخواه</p></div>
<div class="m2"><p>برغبت کند خدمت پادشاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به صدق و ارادت به خدمت گرای</p></div>
<div class="m2"><p>ز بهر رعونت تکلّف نمای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مکن خدمت از بهر امّید و بیم</p></div>
<div class="m2"><p>خرد کار مشکل نگیر سلیم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو بی بیم کردی ببرّی امید</p></div>
<div class="m2"><p>شود خدمتت پوده چون مغز بید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مباش از بقای خود امّیدوار</p></div>
<div class="m2"><p>بقا در بقای وی امّید دار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز بیم عقوبت به خدمت مکوش</p></div>
<div class="m2"><p>نکرده گنه روی تهمت مپوش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز حرص طمع هم مکن بندگی</p></div>
<div class="m2"><p>که باشد ز خجلت سرافکندگی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>طمع چون ببّرید و انکار کرد</p></div>
<div class="m2"><p>نیاید دگر خدمت از دست مرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بود خدمت پادشا هولناک</p></div>
<div class="m2"><p>که خوف است و ترس است و بیم است و باک</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه هر کس تواند سر افراشتن</p></div>
<div class="m2"><p>مقامات خدمت نگه داشتن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>درو گه مذلّت بود گه خطر</p></div>
<div class="m2"><p>نه امن از سفر نه فراغ از حضر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>امید سلامت درو کمترست</p></div>
<div class="m2"><p>درختی است خدمت که رنجش براست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شود بر دلش باب راحت فراز</p></div>
<div class="m2"><p>در افتد به اندیشه های دراز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پدید آید از هر طرف دشمنش</p></div>
<div class="m2"><p>نکرده گنه بارکش کردنش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بود روز و شب در عذاب و زحیر</p></div>
<div class="m2"><p>نباشد مگر خدمتش دستگیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دل و جان به عجز و سرافکندگی</p></div>
<div class="m2"><p>کند وقف بر خدمت و بندگی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>و گریابد آسایشی بیش و کم</p></div>
<div class="m2"><p>نفس برنیارد زد از بیش و کم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وگر خسته دل باشد و ریش تن</p></div>
<div class="m2"><p>شود با سر خدمت خویشتن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زوایا و اطراف خدمت بجوی</p></div>
<div class="m2"><p>به پای تغافل در آن ره مپوی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز ارکان خدمت تحاشی مکن</p></div>
<div class="m2"><p>تکاسل کنی خوار باشی، مکن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو پیش آید اندک شکالی به جهل</p></div>
<div class="m2"><p>ازو در گذشتن مپندار سهل</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز خود کرده تقصیر سر پیش باش</p></div>
<div class="m2"><p>ملامتگر خویش هم خویش باش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مپوش از سر جهل بر خود خطا</p></div>
<div class="m2"><p>عقوبت برابر مکن با عطا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مبر ظن چو کردی خطا و گناه</p></div>
<div class="m2"><p>که باشد مگر بی خبر پادشاه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چه دانی که داند و گر دشمنی</p></div>
<div class="m2"><p>به قصد از ملک سازد اهرمنی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کند عرضه سهو تو بر شهریار</p></div>
<div class="m2"><p>بمانی سرافکنده و شرمسار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هر آنکس که آسایش نفس خواست</p></div>
<div class="m2"><p>به راحت در افزود و از رنج کاست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>اگر بنده بینی گر آزاد را</p></div>
<div class="m2"><p>همین است طبع آدمیزاد را</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ایا قابل حضرت پادشاه</p></div>
<div class="m2"><p>به رنج اندر افزای و راحت بکاه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز لذّات و شهوات بر خود شکن</p></div>
<div class="m2"><p>درخت تن آسایی از بن بکن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دلی چون به دست آوری چاره گر</p></div>
<div class="m2"><p>که هر دم بگردد به نوع دگر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بترک مراد دل خویش گیر</p></div>
<div class="m2"><p>مراد دل دیگری پیش گیر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نشاید نگهداشتن طبع شاه</p></div>
<div class="m2"><p>که نوعی دگر باشد از هر پگاه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دلی همچو آیینه صورت پذیر</p></div>
<div class="m2"><p>مراعات او کردن آسان مگیر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>درین شیوه خدمت مگر زیرکی</p></div>
<div class="m2"><p>به واجب کند آن هم از صد یکی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کمر هر که بر عزم خدمت ببست</p></div>
<div class="m2"><p>به بازار گانی به دریا نشست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نجات و هلاک است و امّید و بیم</p></div>
<div class="m2"><p>که نفعش بزرگ است و خوفش عظیم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به دریای شوریده کشتی مران</p></div>
<div class="m2"><p>که گرداب ژرف است و کشتی گران</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو دریا بود رام و کشتی درست</p></div>
<div class="m2"><p>توانی به اندازه توقیر جست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به طبع و به خو عادت پادشاه</p></div>
<div class="m2"><p>چو دریاست گه قاهر و گه پناه</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>کدام است کشتی، هنرهای مرد</p></div>
<div class="m2"><p>که هر جا تواند بدان کار کرد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو کشتی سلامت بود در بحار</p></div>
<div class="m2"><p>بود همگنان را به جان زینهار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هر آن کو به خدمت قدم در نهاد</p></div>
<div class="m2"><p>ضرورت بباید بر آن ایستاد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>از اندازه بیرون نباید گذشت</p></div>
<div class="m2"><p>بساط خرد در نباید نوشت</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>اگر در مسالک ز ره بازماند</p></div>
<div class="m2"><p>زمانه بر او آیت عجز خواند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>و گر بر گذشت از طریق صواب</p></div>
<div class="m2"><p>کند روزگار از دلیری خراب</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز فرموده شاه بیرون مشو</p></div>
<div class="m2"><p>به جهل از پی بخت وارون مشو</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>که افتد که بالاتر از پایگاه</p></div>
<div class="m2"><p>به کاری فرا داردت پادشاه</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو تقدیم کردی به تکلیف خویش</p></div>
<div class="m2"><p>اعادت کن آنگه منه پای پیش</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>الا تا سلامت بود بیشتر</p></div>
<div class="m2"><p>به منصب شود هر زمان پیشتر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گرت هر زمان قربتی می دهد</p></div>
<div class="m2"><p>چنان دان که وقعت فرو می نهد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو نزدیکتر خواندت پادشاه</p></div>
<div class="m2"><p>چو رفتی فرامش مکن جایگاه</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ازو محترز باش و پرهیز کن</p></div>
<div class="m2"><p>به ترتیب نی قدرت آمیز کن</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>محلّ توهر چند برتر برد</p></div>
<div class="m2"><p>چنان کز ره بندگی در خورد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نیفتی از آن منزلت در غرور</p></div>
<div class="m2"><p>تو زو دور نزدیک و نزدیک دور</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چو شیرین دهد از حرارت مجوش</p></div>
<div class="m2"><p>وگر تلخ دادت برغبت بنوش</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>که گر در تو اکراه پیدا شود</p></div>
<div class="m2"><p>ملک بر تو از خشم شیدا شود</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بمالد به دست ادب گوش تو</p></div>
<div class="m2"><p>که هرگز نگردد فراموش تو</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>و گر ندهد از دست قدرت زمام</p></div>
<div class="m2"><p>کند نقش بر سینه چون بر رخام</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>به چیزی عقوبت کند پادشاه</p></div>
<div class="m2"><p>که طاعت نماید به جنس گناه</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بود واجب از صاحب صدر شاه</p></div>
<div class="m2"><p>که آرایش جامه دارد نگاه</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بزینت در آید میان گروه</p></div>
<div class="m2"><p>که زینت کند مرد را باشکوه</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>تکلّف اثرها کند در عوام</p></div>
<div class="m2"><p>زیادت بکوشند در احترام</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>که نه هر کسی را خرد حاصل است</p></div>
<div class="m2"><p>ندانی خرد چیست، نور دل است</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بدان نور بینند عیب و هنر</p></div>
<div class="m2"><p>محک خود بگوید که چون است زر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ز اجناس مردم در ابداع خود</p></div>
<div class="m2"><p>بود بیشتر عامه و کم خرد</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ممیّز از آن روی اندک تر است</p></div>
<div class="m2"><p>که نه در همه سنگ ها گوهر است</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>به نزدیک نادان چه گوهر چه سنگ</p></div>
<div class="m2"><p>به ظاهر نظر بر لباس است و رنگ</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>به مقدار نادان و دانا درست</p></div>
<div class="m2"><p>بر عامه یکسان نماید نخست</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>بود در نظرشان کسی را شکوه</p></div>
<div class="m2"><p>که با کوکه و نخ رود در گروه</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>نماندست بر حال خود کم خرد</p></div>
<div class="m2"><p>خردمند ازین رنگ و بوکی خرد</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>گرت نیست نفس و خرد دستیار</p></div>
<div class="m2"><p>مکن خدمت پادشا اختیار</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>به فرهنگ و دانش توان کار کرد</p></div>
<div class="m2"><p>گرت نیست گرد فضولی مگرد</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>چو بی مایه بیند ترا پادشاه</p></div>
<div class="m2"><p>مزاج محلّ تو گردد تباه</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>وگر خدمتی کرده باشی پسند</p></div>
<div class="m2"><p>به جز ناپسندیده صورت مبند</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>نباید ملک را ملامت کنید</p></div>
<div class="m2"><p>که نا آزموده ترا برگزید</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>و گر خود در آید به سهو و زلل</p></div>
<div class="m2"><p>به رکنی بملک از تو اندک خلل</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بگرداند از تو از رخ اعتماد</p></div>
<div class="m2"><p>به جدّ سودمند آیدت نه جهاد</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>کسی را که شه در نظر جای کرد</p></div>
<div class="m2"><p>به تمکین و تشریف او رای کرد</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>بلی بی هنر را هنرور کند</p></div>
<div class="m2"><p>چو خورشید کز سنگ گوهر کند</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>ولی روزگاری بباید دراز</p></div>
<div class="m2"><p>که گوهر شود سنگ در کان راز</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>عذاب است سرمایه چاکری</p></div>
<div class="m2"><p>اگر خواجه تاشی وگر لشکری</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>به نزدیک شاه آن بود پیشتر</p></div>
<div class="m2"><p>که او رنج و سختی کشد بیشتر</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>عبید و ممالیک را سال و ماه</p></div>
<div class="m2"><p>ز بهر چه میپرورد پادشاه</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>نه از بهر ناز و تن آسانی است</p></div>
<div class="m2"><p>بلی از پی مملکت بانی است</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>ز چاکر نگویم تنعّم رواست</p></div>
<div class="m2"><p>که این درد را رنج و محنت دواست</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>تنعّم به جز در خور شاه نیست</p></div>
<div class="m2"><p>کش از آرزو دست کوتاه نیست</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>عذاب است و رنج و غنا و تعب</p></div>
<div class="m2"><p>که خدمت نهادند آن را لقب</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>به خدمت بکوش و فزونی مجوی</p></div>
<div class="m2"><p>همیشه ز انعام شه شکر گوی</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>فزونی نه در خدمت چاکر است</p></div>
<div class="m2"><p>در انعام شاه خدم پرور است</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>تو خود جهد می کن به وسع و توان</p></div>
<div class="m2"><p>که پیوسته باشی به خدمت دوان</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>چو با خود مقرّر کنی سال و ماه</p></div>
<div class="m2"><p>نیفتی به کفران نعمت ز راه</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>نزاری یکی پند بشنو ز من</p></div>
<div class="m2"><p>نباشی دژم پیش شاه زمن</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>در اوقات آسایش آن دم شُمَر</p></div>
<div class="m2"><p>که در حضرت شاه داری مقر</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>مپندار کز هیبت و صدمتش</p></div>
<div class="m2"><p>به بیچارگی می کنی خدمتش</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>چو در دل کراهیّتت جا کند</p></div>
<div class="m2"><p>علامات آن بر تو پیدا کند</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>مؤثّر شود در دل پادشاه</p></div>
<div class="m2"><p>عقوبت کند بعد از آن بر گناه</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>و گر شاه برخیزد از جای خویش</p></div>
<div class="m2"><p>و گر تو برون رفته باشی ز پیش</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>ز رنج نشستن به راه ادب</p></div>
<div class="m2"><p>چو آسوده گشتی فروبند لب</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>به کس باز منمای و بر خود بپوش</p></div>
<div class="m2"><p>که عیب آوران چشم دارند و گوش</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>اگر بر تو صد زحمت آمد ولیک</p></div>
<div class="m2"><p>بهش باش و پاس سخن دار نیک</p></div></div>