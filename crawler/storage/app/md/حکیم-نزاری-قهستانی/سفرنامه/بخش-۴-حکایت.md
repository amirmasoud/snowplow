---
title: >-
    بخش ۴ - حکایت
---
# بخش ۴ - حکایت

<div class="b" id="bn1"><div class="m1"><p>با یکی از جمع اخوان الصفاء</p></div>
<div class="m2"><p>رفتم از بازار در دارالشفاء</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محرمی رازی همی گفتم بدو</p></div>
<div class="m2"><p>قصه خود باز میگفتم بدو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کز شکیبایی دلم فرسوده شد</p></div>
<div class="m2"><p>نقد عمرم در سر نابوده شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیش ازینم طاقت دوری نماند</p></div>
<div class="m2"><p>احتمالم رفت و مستوری نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزگار انتظارم دیر شد</p></div>
<div class="m2"><p>خاطرم از زندگانی سیر شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سر صبر و شکیبایی شدم</p></div>
<div class="m2"><p>تا کی از هجران که سودایی شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از قضا دیوانه ای در بند بود</p></div>
<div class="m2"><p>قصۀ من هرچه گفتم می‌شنود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون حدیث صبر و سودا گوش کرد</p></div>
<div class="m2"><p>بانگ بر من زد مرا خاموش کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت اگر در سر تو سودا داریی</p></div>
<div class="m2"><p>همچو من این بند بر پا داریی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در خراسان عاشقی شرمی بدار</p></div>
<div class="m2"><p>آخرای غافل به تبریزت چه کار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو من در بند و در زندان نه ای</p></div>
<div class="m2"><p>خود گرفتم بیدلی بی جان نه ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عشق را بر خود بر عنایی مبند</p></div>
<div class="m2"><p>غافلی عاشق نه ای بر خود مخند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آتشم دیوانه در خرمن فکند</p></div>
<div class="m2"><p>شور از آن شیرین سخن در من فکند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آخرای دل تا یکی کم کاستی</p></div>
<div class="m2"><p>بشنو از دیوانه این راستی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیش عقل خویشتن برخاستن</p></div>
<div class="m2"><p>چیست رنج افزودن و جان کاستن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غره چون کفتار در غار غرور</p></div>
<div class="m2"><p>غافل از گرگ اجل قانع به زور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>موسی جان را نه ای ار خیل تاش</p></div>
<div class="m2"><p>پس رو فرعون نفس آخر مباش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر سر نمرودیان ما و من</p></div>
<div class="m2"><p>سنگ تسبیح خلیل الله زن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هم به لاحول قناعت هر نفس</p></div>
<div class="m2"><p>دفع کن تلبیس ابلیس هوس</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همچو بیژن مبتلا در چه نه ای</p></div>
<div class="m2"><p>پس چرا چون رستم اندر ره نه ای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قیس دور از کوی لیلی کی بدی</p></div>
<div class="m2"><p>گرنه کعبه ناگزیر وی بدی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بیستونی پرده فرهاد بود</p></div>
<div class="m2"><p>غایب از شیرین نه از بیداد بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چیست دامن گیرت ای بیهوده گوی</p></div>
<div class="m2"><p>جز گرانی کژ نشین و راست گوی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پنجم ماه صفر با نوکران</p></div>
<div class="m2"><p>در رکاب صاحب صاحبقران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>صاحب دیوان عالم شمس دین</p></div>
<div class="m2"><p>شاه را دستور اعظم شمس دین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وقت قامت گفتن شبخیز بود</p></div>
<div class="m2"><p>که اتفاق رفتن از تبریز بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بی توقف یک دو منزل شد یله</p></div>
<div class="m2"><p>تا به دریابی که خوانندش تله</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پیش دریا روز منزل ساختند</p></div>
<div class="m2"><p>بارگاه خواجه بر افراختند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن تله بحرى عجب مقهور بود</p></div>
<div class="m2"><p>در میانش قلعۀ معمور بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جای گنج پادشاه کامکار</p></div>
<div class="m2"><p>بر سرش سی اژدهای پاسدار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صبحدم دریا ز پس بگذاشتیم</p></div>
<div class="m2"><p>منزل آن روز دگر خوی داشتیم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خوی ز بس ترک ختایی چون ختن</p></div>
<div class="m2"><p>منزلی ایمن ولیکن پر فتن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شهری از بس لاله و گل چون بهار</p></div>
<div class="m2"><p>مرغزاری چون بهشتش بر کنار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آنگه از خوی در الاطاق آمدیم</p></div>
<div class="m2"><p>جفت غم و ز خرمی طاق آمدیم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>لحظه لحظه دم به دم در انتظار</p></div>
<div class="m2"><p>که اتفاق رجعت افتد زان دیار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مجمع اردو به الاطاق بود</p></div>
<div class="m2"><p>زانکه الاطاقشان ییلاق بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>راستی خوش موضعی خوش منزلی</p></div>
<div class="m2"><p>جای نزهت هر کرا باشد دلی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چشمه های آب سرد از هر طرف</p></div>
<div class="m2"><p>ناودان سبزه از جو چون صدف</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از پری گویی همه صحراپرست</p></div>
<div class="m2"><p>وز هوا کام شقایق پر دُرست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یا مگر فردوس بگشادند در</p></div>
<div class="m2"><p>حوریان را در جهان دادند سر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر که چشمش بر چنان ماهی فتاد</p></div>
<div class="m2"><p>تا به سالی از خودش نامد بیاد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>عالمی از تنگ چشمان تنگ دل</p></div>
<div class="m2"><p>جمله سیمین تن ولیکن سنگدل</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اهل دل را دل به زاری جان به لب</p></div>
<div class="m2"><p>چون پر بوم از کله شان مضطرب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هر که را خاطر به کاکلشان فتاد</p></div>
<div class="m2"><p>پای در ره نانهاده سر نهاد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>داشتم در دل گران دریا مگر</p></div>
<div class="m2"><p>کشتی جان را به جهد آرم به در</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گفتم آخر بگذرد بوک از وبال</p></div>
<div class="m2"><p>خود توقف را نبد چندان مجال</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا در آن بودیم لشگر بر نشست</p></div>
<div class="m2"><p>راه بیرون آمدن یاسا به بست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>رأیت کشور گشای پادشاه</p></div>
<div class="m2"><p>سوی گرجستان برون آمد به راه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شاه را بر قلب ار من راه بود</p></div>
<div class="m2"><p>ملک ار من دیدنش دلخواه بود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شهرهایی کز بلاد ارمن است</p></div>
<div class="m2"><p>معتبر پیوسته در چشم من است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بر فراز سنگ خارا ساخته</p></div>
<div class="m2"><p>ژرف رودی پیش شهر انداخته</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سقف و دیوار و ستون هر مقام</p></div>
<div class="m2"><p>کرده از سنگ تراشیده تمام</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>حصنی از خارا برآورده رفیع</p></div>
<div class="m2"><p>کرده بر بارو عملهای بدیع</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ساخته بت‌خانه ها در وی چنان</p></div>
<div class="m2"><p>کز تحیر در دهان ماند بنان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>آنچنان جا بت پرستی همچو من</p></div>
<div class="m2"><p>بگذرد از می تھی ناکرده دن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ای دریغا صبر می‌بایست کرد</p></div>
<div class="m2"><p>با مغان یک هفته می‌بایست خورد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مرد عاقل را که فرصت کرد فوت</p></div>
<div class="m2"><p>زندگانی شد بر و از غصه موت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>عزم گرجستان زالی شد درست</p></div>
<div class="m2"><p>باز گویم قصه ای از ره نخست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>عقبه های سخت و راهی سنگلاخ</p></div>
<div class="m2"><p>تنگ بر خلق جهان ملک فراخ</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>من به حیرت مانده چون خر در خلاب</p></div>
<div class="m2"><p>سینه ای پر آتش و چشمی پر آب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>رفته بر افلاک روزی چند بار</p></div>
<div class="m2"><p>در بلندی کرده بر چرخ افتخار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>او ز مقابل کوه بام آسمان</p></div>
<div class="m2"><p>آمده با تحت اسفل هر زمان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>آسمان پوشیده در لیل و نهار</p></div>
<div class="m2"><p>میغ همچون ابر چشمم سیل بار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>عالم از مهر دل من گرم بود</p></div>
<div class="m2"><p>آفتاب اندر حجاب شرم بود</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>خاک ره صد بار بر سر کردمی</p></div>
<div class="m2"><p>گرنه ز آب دیده ره تر کردمی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گر به گرجستان درون گویم که من</p></div>
<div class="m2"><p>آدمی دیدم دروغست این سخن</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>آدمی نه عالمی مریخ روی</p></div>
<div class="m2"><p>ماده و نر سبز چشم و زرد موی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دایم از خورشید چون پوشیده اند</p></div>
<div class="m2"><p>لاجرم خامند و ناجوشیده اند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>جمله ترسا ملت و اغلب کشیش</p></div>
<div class="m2"><p>شانه شان هرگز ندیده بود ریش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>معجری پوشیده بر زیر کلاه</p></div>
<div class="m2"><p>کرده همچون سوگیان معجر سیاه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گرچه مقلوبست هفت اندامشان</p></div>
<div class="m2"><p>از جهان گم بوده بادا نامشان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>عورتانشان جمله بی ایزار پای</p></div>
<div class="m2"><p>خر بسی بهتر بود زیشان بگای</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بی نمازان قحبگان زشت خس</p></div>
<div class="m2"><p>کارشان کردن ز سرگین خشت و بس</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گشته تا زانو به سرگین در ز کعب</p></div>
<div class="m2"><p>راستی را از در دیرند و لعب</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ای نزاری درج دل سر باز کن</p></div>
<div class="m2"><p>قصه کیتو کرخ آغاز کن</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>راستی را کوه و دشتش چون بهار</p></div>
<div class="m2"><p>کشت زار و لاله زاره و سبزه زار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>خاک و بومش بی نهایت مرتفع</p></div>
<div class="m2"><p>بیخ داروها در و بس منتفع</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>لیک از بس باد رستاخیز بود</p></div>
<div class="m2"><p>زخم بادش نقش از آهن می‌ربود</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>تا در و بودیم کس آشی نخورد</p></div>
<div class="m2"><p>که آتش از طوفان بادش بر نکرد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>قلب تابستان در و سرمای سخت</p></div>
<div class="m2"><p>هست از آن اغلب مواضع بیدرخت</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>شمس در خرچنگ می‌باشد هنوز</p></div>
<div class="m2"><p>برف می‌ریزد هوایش در تموز</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>قرب پنجه روز بد در وی مقام</p></div>
<div class="m2"><p>بگذرد هم عاقبت ناکام و کام</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>گرچه باشی در حوادث پای بست</p></div>
<div class="m2"><p>صبر کن که امید استخلاص هست</p></div></div>