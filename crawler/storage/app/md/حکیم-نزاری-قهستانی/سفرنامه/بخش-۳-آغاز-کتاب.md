---
title: >-
    بخش ۳ - آغاز کتاب
---
# بخش ۳ - آغاز کتاب

<div class="b" id="bn1"><div class="m1"><p>روزگاری خرم و خوش داشتیم</p></div>
<div class="m2"><p>گرچه جایی دل مشوش داشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بودمی من چندگاه از مکر و کید</p></div>
<div class="m2"><p>فارغ از رد و قبول عمر و زید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجلس عشرت مدام آراسته</p></div>
<div class="m2"><p>همنشینان جمله دل برخاسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر یک از جایی دگر تشویش ناک</p></div>
<div class="m2"><p>لیک در جمعیت از تشویش پاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ره عشق آمده چالاکی و چست</p></div>
<div class="m2"><p>با دلی صد پاره و عزمی درست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمله واحد در طریق اتحاد</p></div>
<div class="m2"><p>راست گویم یک‌دل و یک اعتقاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرده با یک حرف از شش هفت اسم</p></div>
<div class="m2"><p>رفته در یک پیرهن شش هفت جسم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همگان ثابت قدم صاحب وفا</p></div>
<div class="m2"><p>چون بود آیین اخوان الصفا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از قضا چشم بد اندر من رسید</p></div>
<div class="m2"><p>وقت ایشان باد در عیش لذیذ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرخ بی بنیاد اگر بیخم بکند</p></div>
<div class="m2"><p>قرعۀ عزم سفر بر من فکند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دشمنی خود کار چرخ تند خوست</p></div>
<div class="m2"><p>هرگز از دشمن نیاید بوی دوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا مرا از پردۀ عشاق باز</p></div>
<div class="m2"><p>در عراق افکند بی ترتیب و ساز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نی غلط کردم که خوش رفتیم و شاد</p></div>
<div class="m2"><p>ابتدا و انتها محمود باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عمدۀ آفاق تاج الدین عمید</p></div>
<div class="m2"><p>آنکه باد اقبال و عمرش بر مزید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نور مصباح دل بینای من</p></div>
<div class="m2"><p>وز طریق اصطلاح آقای من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طالع فرخنده ز اختر در گرفت</p></div>
<div class="m2"><p>وز قهستان عزم اردو بر گرفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غرّۀ شوال سه شنبه ز تون</p></div>
<div class="m2"><p>بر طریق اصفهان آمد برون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سال نو بر ششصد و هفتاد و هشت</p></div>
<div class="m2"><p>بود کز تاریخ هجرت میگذشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من که دایم هم‌عنانش بودمی</p></div>
<div class="m2"><p>یار پیدا و نهانش بودمی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بوده در سرّا و ضرّا پیش او</p></div>
<div class="m2"><p>تابع رای صلاح اندیش او</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>معتقد در نیک و بد، در نفع و ضر</p></div>
<div class="m2"><p>متفق هم در سفر هم در حضر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کرده با یاران خاص الخاص او</p></div>
<div class="m2"><p>زیر مقدم جادۀ اخلاص او</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در سر از انصاف شوری داشتم</p></div>
<div class="m2"><p>دامن شیرین ز کف نگذاشتم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آمدیم القصه با اندک شمار</p></div>
<div class="m2"><p>در صفاهان اول فصل بهار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اصفهان همچون بهشتی تازه بود</p></div>
<div class="m2"><p>بلکه صد ره خوشتر از آوازه بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زنده رودش خوشتر از جوی بهشت</p></div>
<div class="m2"><p>بر هر اطرافش درختستان و کشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گلستان در گلستان آراسته</p></div>
<div class="m2"><p>من چو بلبل با دلی برخاسته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سینه ای از آتش اندوه داغ</p></div>
<div class="m2"><p>گه به خانه گه به کوی و گه به باغ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر صفاهان زان گرفتم راه را</p></div>
<div class="m2"><p>تا ببینم یار ایرانشاه را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آن جهان فضل را جان آمده</p></div>
<div class="m2"><p>افسری بر سر ز اعیان آمده</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خرقۀ او زهد و تقوی را نظام</p></div>
<div class="m2"><p>سلک نظمش ملک معنی را نظام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مونس ایام تنهایی من</p></div>
<div class="m2"><p>راست گویم کُحل بینایی من</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>محرم راز نهان و آشکار</p></div>
<div class="m2"><p>همدم و هم صحبت لیل و نهار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یار نیکو عهد و نیکو ذات من</p></div>
<div class="m2"><p>داشته پیوسته خوش اوقات من</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر دو بعد از ترک چنگ و نای و نوش</p></div>
<div class="m2"><p>بوده از یک پیر باهم خرقه پوش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>او هنوز انصاف را پرهیزگار</p></div>
<div class="m2"><p>من شکسته توبه همچون زلف یار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>او ریاضت می‌کند با زاهدان</p></div>
<div class="m2"><p>من مروّق می‌کشم با شاهدان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>او گرفته حلقۀ مسجد به چنگ</p></div>
<div class="m2"><p>من گرفته روز و شب گیسوی چنگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>او چو دیگر پارسایان در صلاح</p></div>
<div class="m2"><p>من چو رندان باده در کف هر صباح</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خدمتش بار دگر در یافتم</p></div>
<div class="m2"><p>وز حضورش حظ وافر یافتم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>داشتم دیرینه در سر این هوس</p></div>
<div class="m2"><p>از صفاهانم مراد این بود و بس</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>وقت‌ها با من وصیت کرده بود</p></div>
<div class="m2"><p>حق صحبت نیز یاد آورده بود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کز برای خاطر من عزم کن</p></div>
<div class="m2"><p>یادگاری دوستان را نظم کن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سرگذشت این سفر در پیش گیر</p></div>
<div class="m2"><p>امتحان از طبع دوراندیش گیر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نیک و بد در هر مقامی فکر کن</p></div>
<div class="m2"><p>حاضران و غایبان را ذکر کن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>(گرچه حالی لایق تالیف نیست</p></div>
<div class="m2"><p>عذر میخواهم که بی تکلیف نیست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>می نیرزد رنجه کردن خامه ای</p></div>
<div class="m2"><p>خوش نباشد نظم محنت نامه ای</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سرگذشت این سفر زیبا و زشت</p></div>
<div class="m2"><p>می‌توان بر کهنه تقویمی نوشت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کرده ام هر گونه زین افسانه طرح</p></div>
<div class="m2"><p>بر ولا هرگز نگوید مست شرح)</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مست بودم بیشتر اوقات مست</p></div>
<div class="m2"><p>مست را ترتیب برناید ز دست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زین غرض مقصود من افسانه نیست</p></div>
<div class="m2"><p>وصف و شرح گلخن و کاشانه نیست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هست ذکر دوستان معهود من</p></div>
<div class="m2"><p>ذکر ایشانست ازین مقصود من</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر بود یک بیت ازین مقبول یار</p></div>
<div class="m2"><p>یک نشان از دوستان بس یادگار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ذکر یاران گر نباشد عیب‌ناک</p></div>
<div class="m2"><p>آن دگر گر حشو باشد نیست باک</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گرچه کردم جهد تاحالی بود</p></div>
<div class="m2"><p>وز ریا این مختصر خالی بود</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نیک و بد در هم زدم بسیار کی</p></div>
<div class="m2"><p>هست در پهلوی گل هم خار کی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خلوت آزادگان در هیچ عهد</p></div>
<div class="m2"><p>بی سبر قو بر نیاوردست جهد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نیمۀ ذوالقعده باز از اصفهان</p></div>
<div class="m2"><p>رخت ما بر بست گردون ناگهان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>موسم گل آمدیم اندر نطنز</p></div>
<div class="m2"><p>بر بهشت از خرّمی می‌کرد طنز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>یک دو روز آنجا ز بهر نای و نوش</p></div>
<div class="m2"><p>چون عصیر از می برآوردیم جوش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>در سوم روز از پِگه برخاستیم</p></div>
<div class="m2"><p>از پی کوچ اسب و استر خواستیم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بعد عید ار ساکن ار تیز آمدیم</p></div>
<div class="m2"><p>غرۀ مه را به تبریز آمدیم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>یافتم شهری مکان عیش و سور</p></div>
<div class="m2"><p>بلکه فردوسی پر از غِلمان و حور</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>جنتی کش حور پیرامن بود</p></div>
<div class="m2"><p>جای من وقتی که دل با من بود</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>سایه بان عیش بر پروین زدیم</p></div>
<div class="m2"><p>باده ها با خواجه فخرالدین زدیم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>روزها برده به شب شبها به روز</p></div>
<div class="m2"><p>بر سماع چنگ و روی دل‌فروز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سیف کاشانی خدایش بار باد</p></div>
<div class="m2"><p>وز درخت عمر برخوردار باد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>روز و شب پیوسته با ما می‌سپرد</p></div>
<div class="m2"><p>در میانش بود با ما صاف و دُرد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>شب نبودی جز به می تسکین من</p></div>
<div class="m2"><p>چنگی شمسو تا سحر بالین من</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بی خبر می‌بودم از جام شراب</p></div>
<div class="m2"><p>باز می‌رستم زمانی از عذاب</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هر چه عقل از پیش من بر خاستی</p></div>
<div class="m2"><p>موج شوق از قصر تن برخاستی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>عقل دامن گیر شوقم می‌نبود</p></div>
<div class="m2"><p>چاشنی صبر و ذوقم می نبود</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>عشق بازی کار هر دلتنگ نیست</p></div>
<div class="m2"><p>در طریق عشق نام و ننگ نیست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تا مگر با همدمی یک دم زنند</p></div>
<div class="m2"><p>شاید از دنیا و دین برهم زنند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>هم به تبریزم جوانی بدترین</p></div>
<div class="m2"><p>ای که بادا بر جوانان آفرین</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>داشتم با او به رسم یاریی</p></div>
<div class="m2"><p>وقت فرقت دست در بی کاریی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بافتی اما نه قادر بد در آن</p></div>
<div class="m2"><p>شعر شعری همچو دیگر خواهران</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بهر تسکین دل بی خویش من</p></div>
<div class="m2"><p>باز گفتی سرگذشتی پیش من</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گفت وقتی دل ببرد از من کسی</p></div>
<div class="m2"><p>همچو من بودند ازو بیدل بسی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>هیچ کس را دل نمی‌شد سیر ازو</p></div>
<div class="m2"><p>پای خلقی بر سر شمشیر ازو</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>حسن بی اندازه و بازار تیز</p></div>
<div class="m2"><p>او ز غوغای عوام اندر گریز</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چون نمی شد دیده خشنود از ویم</p></div>
<div class="m2"><p>هیچ آسایش نمی بود از ویم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بر امید انتظاری زان پسر</p></div>
<div class="m2"><p>می‌کشیدم چون نمی‌شد زان بسر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>تا ز جست‌و‌جو و گفت‌و‌گوی او</p></div>
<div class="m2"><p>عاقبت معلوم کردم خوی او</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>رغبت دیوانه دیدن داشتی</p></div>
<div class="m2"><p>هیچ این عادت فرو نگذاشتی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>هرچه عزم طوف کردی آن پسر</p></div>
<div class="m2"><p>بر در دارالشفا کردی گذر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بود از آن شیرین لبم در سینه شور</p></div>
<div class="m2"><p>سر بر آوردم به ناپاکی به زور</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بیش از آنم برگ غم خوردن نبود</p></div>
<div class="m2"><p>چاره جز دیوانگی کردن نبود</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>خویشتن را ساختم دیوانه ای</p></div>
<div class="m2"><p>می شدم نشناخت در هر خانه ای</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>می‌شکستم می‌زدم می‌سوختم</p></div>
<div class="m2"><p>بر خود القصه جهان بفروختم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>عاقبت یک روز جمع ناخوشان</p></div>
<div class="m2"><p>بر سر زنجیر بردندم کشان</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>با من از شادی نه جوهر نه عرض</p></div>
<div class="m2"><p>رفتم از بازار در دارالمرض</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>هم به دست خود در آن زندان‌سرای</p></div>
<div class="m2"><p>کردم اندر حلقۀ زنجیر پای</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>وان پسر هر هفته می‌آمد به طوف</p></div>
<div class="m2"><p>نه ز پدر بیم و نه از استاد خوف</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>پیش من چون خرمن گل می نشست</p></div>
<div class="m2"><p>حاسدان را خار در پا می‌شکست</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>من حکایتهای لایق گفتمی</p></div>
<div class="m2"><p>جمله با طبعش موافق گفتمی</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بیتها پیوسته از املای من</p></div>
<div class="m2"><p>امتحانها کردی از انشای من</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>مدتی پایم در آن زنجیر بود</p></div>
<div class="m2"><p>تا بود آنچه از ازل تقدیر بود</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چون گرفتی پیش راهی را نخست</p></div>
<div class="m2"><p>قصد مقصد کن به عزمی تندرست</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>نیست عاشق پای بند نام و ننگ</p></div>
<div class="m2"><p>نام و ننگ عاشقان شیشه ست و سنگ</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>هر که را دل خویشتن رایی کند</p></div>
<div class="m2"><p>پای در زنجیر رسوایی کند</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>سست مرد ارچه جوانمردست و راد</p></div>
<div class="m2"><p>دولتش محروم دارد از مراد</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>لعل را بیرون ز سنگ آورده‌اند</p></div>
<div class="m2"><p>گوهر از کام نهنگ آورده‌اند</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>همچو مجنون غیر لیلی هرچه هست</p></div>
<div class="m2"><p>همچو بت بر یکدگر باید شکست</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>انس و خو بادام و دد باید گرفت</p></div>
<div class="m2"><p>ترک نام و ننگی خود باید گرفت</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>سنگ نه بر دل که چون فرهاد گرد</p></div>
<div class="m2"><p>کام دل نابرده جان خواهی سپرد</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>عشق زین برنام بدنامی زند</p></div>
<div class="m2"><p>کام دل بر سنگ بدنامی زند</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>سر بنه بر سنگ غم فرهاد‌وار</p></div>
<div class="m2"><p>یاسر خود گیر و رو آزادوار</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>بود شاگردی مگر فرهاد را</p></div>
<div class="m2"><p>چون شنود آن واقعه استاد را</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>راه کوه بیستون پرسید و رفت</p></div>
<div class="m2"><p>پیش او آمد دلی پرتاب و تفت</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>دیدش افتاده در آن خارا کمر</p></div>
<div class="m2"><p>گاه سر می‌زد در آن گاهی تبر</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>اول اندر پای استاد اوفتاد</p></div>
<div class="m2"><p>بعد از آن دستش به خدمت بوسه داد</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>مشفقانه گریه ای آغاز کرد</p></div>
<div class="m2"><p>پس ملامت خانه را در باز کرد</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>گفت ای در هر هنر چون عقل فرد</p></div>
<div class="m2"><p>چون کنی کاری که هرگز کس نکرد</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>بیستون را خود به سر بشکافتن</p></div>
<div class="m2"><p>این غرض در عقل گنجد یافتن</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>تو به تلخی می‌گذاری در عذاب</p></div>
<div class="m2"><p>فارغ از شور تو شیرین در شراب</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>نام شیرین چون به گوش آمد ورا</p></div>
<div class="m2"><p>جملگی اعضا به جوش آمد ورا</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>بانگ بر شاگرد زد شوریده سر</p></div>
<div class="m2"><p>کین چه گستاخیست ای کوته نظر</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>در میان سنگی‌ست از من تا به دوست</p></div>
<div class="m2"><p>کان مرا در گردن از خرسنگ اوست</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>گر به سر هامون توانم کردنش</p></div>
<div class="m2"><p>می‌زنم تا بفکنم از گردنش</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>تا بمیرم می‌زنم سر بر کمر</p></div>
<div class="m2"><p>یا کنم با دوست دستی در کمر</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>بیستون در چشم تو دارد محل</p></div>
<div class="m2"><p>گرنه اندر دست من مومی‌ست حل</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>در دلت گر ذره ای زین غم بدی</p></div>
<div class="m2"><p>کوه در چشمت ز کاهی کم بدی</p></div></div>