---
title: >-
    بخش ۷ - حکایت
---
# بخش ۷ - حکایت

<div class="b" id="bn1"><div class="m1"><p>گفت اسپهبد به پیشین روزگار</p></div>
<div class="m2"><p>داشت با قاضی آنجا چند کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طالشی شوریده سر برجست و رفت</p></div>
<div class="m2"><p>راه قاضی را میان در بست و رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون در آمد پیش قاضی گرم گرم</p></div>
<div class="m2"><p>حال ازو پرسید قاضی نرم نرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت از اسپهبد رسولم پیش تو</p></div>
<div class="m2"><p>تا چه گوید رأی دوراندیش تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت قاضی باز گو تاحال چیست</p></div>
<div class="m2"><p>آمدن پیشم به استعجال چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من ندانم تا چه کارش بود گفت</p></div>
<div class="m2"><p>آن خدا داند نه من راز نهفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاضی بیچاره حیران ماند ازو</p></div>
<div class="m2"><p>در تعجب لب به دندان ماند ازو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرچه پرسیدی ازو در هیچ باب</p></div>
<div class="m2"><p>جز همین یک نکته نشنودی جواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون به جان آمد از آن بی عقل و هوش</p></div>
<div class="m2"><p>یک زمان اندیشه کرد و شد خموش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از حدیث مرد نادان یافت باز</p></div>
<div class="m2"><p>کآمد از پیش سپهبد بر مجاز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت قاضی آری آری راست‌ست</p></div>
<div class="m2"><p>او زمن سنگ آسیایی خواست‌ست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قاضیش اعزازها تقدیم کرد</p></div>
<div class="m2"><p>آسیا سنگی بدو تسلیم کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر گرفت آن دیو سنگ آسیا</p></div>
<div class="m2"><p>برد بعد از روزها پیش کیا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون بدید اسپهبد آن سنگ گران</p></div>
<div class="m2"><p>کرد از آن نادان تعجبها در آن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت این بر کار ناید راستم</p></div>
<div class="m2"><p>من نه این سنگ آن دگر یک خواستم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرد نادان سنگ را بر سر گرفت</p></div>
<div class="m2"><p>راه قاضی بار دیگر بر گرفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>طالش اندر پشت از آن راه دراز</p></div>
<div class="m2"><p>پیش او سنگی دگر آورده باز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر سبیل طعنه مرد ژاژخای</p></div>
<div class="m2"><p>باز گفت این قصه زان کوتاه رای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یعنی ایشان چون شما فرمانبرند</p></div>
<div class="m2"><p>تابع و منقاد امر رهبرند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفتمش ای مرد این مردانگیست</p></div>
<div class="m2"><p>این نه فرمان بردن این دیوانگیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عین فرمان عین آن دانستن است</p></div>
<div class="m2"><p>این جنونی بر عبادت بستن است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>او به جز دیوانه حمال چیست</p></div>
<div class="m2"><p>پیش جانبازان صاحب حال چیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر که او مأمور امر مطلق است</p></div>
<div class="m2"><p>گر همه باطل کند عین حق است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آمر و مأمور اگر بر اصل نیست</p></div>
<div class="m2"><p>هر دو هستند ار یقین در اصل نیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آمر مطلق خداوندست و بس</p></div>
<div class="m2"><p>نیست آمر کو بود مأمور کس</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر چه دیگر غیر فرمان خداست</p></div>
<div class="m2"><p>پیش اهل معرفت کلی هباست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لطف کن زین هرزه ها بر من مبند</p></div>
<div class="m2"><p>راست گویم بر برو تانت مخند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همچو آن صوفی ناصافی تو نیز</p></div>
<div class="m2"><p>گر چه گستاخیست بر سبلت متیز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بودم اندر قلزم اندیشه غرق</p></div>
<div class="m2"><p>می‌گذشتم بر طریقی همچو برق</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دل خدا داند کجاخاطر کجا</p></div>
<div class="m2"><p>بی خبر از عالم خوف و رجا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ناگه از پشمینه پوشان یک دو تن</p></div>
<div class="m2"><p>بر گذشتند از من بیخویشتن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اندر آن حالت چو بودم بیخبر</p></div>
<div class="m2"><p>هیچ با ایشان بیفتادم مگر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زان یکی شیخم زپس آواز داد</p></div>
<div class="m2"><p>پیشم آمد باز با من درفتاد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفت دنیا را که می بینی بهای</p></div>
<div class="m2"><p>از سه چیزست اول از فضل خدای</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دیگر از عدل شه فریاد رس</p></div>
<div class="m2"><p>وان دگر از همت درویش و بس</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اهل معنی را به درویشان راه</p></div>
<div class="m2"><p>بهترک زین شاید ار باشد نگاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از سر اسب ار سلامی داد بی</p></div>
<div class="m2"><p>بی‌شکی زین ماجرا آزاد یی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گفتمش بنگر پس و پیش سخن</p></div>
<div class="m2"><p>ماجرا گر میکنی بر اصل کن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تونه ای مرد خدای لاشریک</p></div>
<div class="m2"><p>خویشتن را با خدا کردی شریک</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هست چون پاینده دنیا ناگزیر</p></div>
<div class="m2"><p>از خدا وز پادشاه و از فقیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>با خدا پس هم تو و هم پادشاه</p></div>
<div class="m2"><p>مشترک باشید، بی هیچ اشتباه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از شما هر دو یکی ای ژاژخای</p></div>
<div class="m2"><p>فاضلست استغفرالله بر خدای</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر فقیری خویشتن بینی مکن</p></div>
<div class="m2"><p>ای مسلمان نام، بی‌دینی مکن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>این نه درویشی که طاما تست و بس</p></div>
<div class="m2"><p>بلکه سرتاسر خرافاتست و بس</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چون تو در بند سلامی مانده ای</p></div>
<div class="m2"><p>در پس ننگی و نامی مانده ای</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از سلامت کی رسد بویی بتو</p></div>
<div class="m2"><p>فقر ننماید سر مویی بتو</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در جهان آخر تو باری کیستی</p></div>
<div class="m2"><p>ای ز خر کمتر تو باری چیستی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تو کیی سلطان چه از حق سر مپیچ</p></div>
<div class="m2"><p>جز خدا دیگر همه هیچ‌ند هیچ</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شیخ چون دانست که الزام از کجاست</p></div>
<div class="m2"><p>سر فرود آورد و عذری باز خواست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هر که اندک مایه چیزی بازیافت</p></div>
<div class="m2"><p>راه با سر رشته این راز یافت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گر برانندش به دشنام و قفا</p></div>
<div class="m2"><p>دوستتر دارد که گویندش دعا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کی شود مقبول آن حضرت فقیر</p></div>
<div class="m2"><p>تا بود در بند نیک و بد اسیر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر صفا در ازرق و در اخضرست</p></div>
<div class="m2"><p>هر مرقّع پوش خضری دیگرست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پَسْر وِ خضری رها کن اعتراض</p></div>
<div class="m2"><p>نفس منکر ار سوادست ار بیاض</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ور به صورت با میان آری نفاق</p></div>
<div class="m2"><p>بر تو خواند عاقبت هذا فراق</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تا نگردد باطن درویش صاف</p></div>
<div class="m2"><p>نشکند بر لشگر سلطان مصاف</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مرد صافی کی شود صوفی به صوف</p></div>
<div class="m2"><p>شعر بی‌معنی و خط زیبا حروف</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>فقر تا خالی نباشد از فضول</p></div>
<div class="m2"><p>باز نتوان آمد از رد و قبول</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نفس را زیر قدم باید نهاد</p></div>
<div class="m2"><p>دین و دنیا در عدم باید نهاد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تا سرت زیر قدم ننهی به فخر</p></div>
<div class="m2"><p>دست معنی کی رسد بر طاق فقر</p></div></div>