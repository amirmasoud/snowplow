---
title: >-
    شمارهٔ ۷۱۵
---
# شمارهٔ ۷۱۵

<div class="b" id="bn1"><div class="m1"><p>آن موی بریده بر بنا گوش</p></div>
<div class="m2"><p>کشته ست مرا و کرده مدهوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نغزت برِ روی و غمزه و چشم</p></div>
<div class="m2"><p>خوبست سرِ دست و بازو و دوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر می خواهی که عالمی خلق</p></div>
<div class="m2"><p>زان زلف کنند حلقه در گوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مِعجر بنه از سر و کله دار</p></div>
<div class="m2"><p>دُرّاعه بیفکن و قبا پوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ترک پری صفت حدیثی</p></div>
<div class="m2"><p>زین هندوی خود به لطف بنیوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوسی که هزار جان بیرزد</p></div>
<div class="m2"><p>آسان آسان به هیچ مفروش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر آمده بوده ای ز فطرت</p></div>
<div class="m2"><p>خاص از پی خون من در آن کوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا جان ببرم مگر چو کردم</p></div>
<div class="m2"><p>ترک دل و دین و دنیی و هوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دادی بستانم از تو گر هیچ</p></div>
<div class="m2"><p>سرمست درآرمت به آغوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنرا که به یاد توست زنده</p></div>
<div class="m2"><p>یکباره چنین مکن فراموش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زآن یک نفسی که دیدمت دی</p></div>
<div class="m2"><p>تا روز نخفتم از غمت دوش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گویند نزاریا چه بودت</p></div>
<div class="m2"><p>وین گریه و ناله چیست خاموش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای بی خبران بر آتش عشق</p></div>
<div class="m2"><p>می سوزم اگر نمی زنم جوش</p></div></div>