---
title: >-
    شمارهٔ ۱۳۹۵
---
# شمارهٔ ۱۳۹۵

<div class="b" id="bn1"><div class="m1"><p>ای مونس روزگار تنهایی</p></div>
<div class="m2"><p>بر ما شب هجر چند پیمایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نزع فتاده ام نظر بر در</p></div>
<div class="m2"><p>تدبیرِ وداع کن چه می پایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر بویِ تو جانِ رفته باز آمد</p></div>
<div class="m2"><p>گر با سر کشته ی فراق آیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جاروبِ رهت کنند گیسو را</p></div>
<div class="m2"><p>گر بخرامی، بتان یغمایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دیده کشند خاک نعلینت</p></div>
<div class="m2"><p>صاحب نظران برایِ بینایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مولایِ منی و میدهم حجّت</p></div>
<div class="m2"><p>گر بستانی به من هیولایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برخیز و قیامت آشکارا کن</p></div>
<div class="m2"><p>ای قامت آفت شکیبایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نی نی بنشین وگرنه برخیزد</p></div>
<div class="m2"><p>فریاد ز بی دلانِ شیدایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیهات که طالبِ نظر دارد</p></div>
<div class="m2"><p>از پرده اگر جمال بنمایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رفتم به طبیب گفتم ای در طب</p></div>
<div class="m2"><p>مشهور جهانیان به دانایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درد دل نازک نزاری را</p></div>
<div class="m2"><p>درمان چه کنم دوا چه فرمایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نبظم بگرفت . گفت آهسته</p></div>
<div class="m2"><p>زین دل چه کنند خاصه سودایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زنهار که بی دلی از این بهتر</p></div>
<div class="m2"><p>پرهیز کن از حریف هرجایی</p></div></div>