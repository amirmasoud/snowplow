---
title: >-
    شمارهٔ ۲۶۱
---
# شمارهٔ ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>ای که جانم به فدایِ قد خوش منظرِ دوست</p></div>
<div class="m2"><p>کاش صد جان دگر داشتمی در خور دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزها می­گذرد تا خبرش نشنیدم</p></div>
<div class="m2"><p>چون کنم، با که بگویم که فرستم برِ دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دشمن اربا من ازین روی ترش خواهد داشت</p></div>
<div class="m2"><p>بس که من بوسه شیرین خورم از شکّرِ دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر همه خلقِ جهان دشمنِ عاشق باشند</p></div>
<div class="m2"><p>هیچ غم نبود اگر دوست بود یاور دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقیا باده به یارانِ دگر ده نه به من</p></div>
<div class="m2"><p>که مرا باده نباید مگر از ساغرِ دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یاسمن مشکن و گل پیش میاور که مرا</p></div>
<div class="m2"><p>سرِ جان نیست درین باغ به جان و سرِ دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطربا چنگ مسازید و مگویید غزل</p></div>
<div class="m2"><p>که سر زهره ندارم به رخِ ازهرِ دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه را حادثه­یی افتد اگر واقعه­یی</p></div>
<div class="m2"><p>مرجعی دارد و بی چاره نزاری درِ دوست</p></div></div>