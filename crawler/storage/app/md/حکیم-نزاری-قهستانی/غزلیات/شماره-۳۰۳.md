---
title: >-
    شمارهٔ ۳۰۳
---
# شمارهٔ ۳۰۳

<div class="b" id="bn1"><div class="m1"><p>جان است و هرچه نه همه جان است هیچ نیست</p></div>
<div class="m2"><p>هر چیز کان به جمله نه آن است هیج نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانجا هر آن جمله هست چنین است جمله هست</p></div>
<div class="m2"><p>زینجا هرآنچه نیست چنان است هیچ نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الّا جز او همه هیچند هرچه هست</p></div>
<div class="m2"><p>بی او اگر بهشت جنان است هیچ نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانسته ایمن است و ندانسته غافل است</p></div>
<div class="m2"><p>هر مجتهد که نی همه دان است هیچ نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کثرت نما بسی نکند روی در عدم</p></div>
<div class="m2"><p>پول سیاه اگرچه روان است هیچ نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیر ضعیف اگر به مصاف مبارزان</p></div>
<div class="m2"><p>دعوی کند که مرد جوان است هیچ نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر دو جهان بده به نزاری به یک قدح</p></div>
<div class="m2"><p>نیک و بد جهان چو جهان است هیچ نیست</p></div></div>