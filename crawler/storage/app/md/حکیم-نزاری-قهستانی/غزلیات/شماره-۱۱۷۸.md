---
title: >-
    شمارهٔ ۱۱۷۸
---
# شمارهٔ ۱۱۷۸

<div class="b" id="bn1"><div class="m1"><p>مرا گر همّتی مردانه بودی</p></div>
<div class="m2"><p>خلاصم از دلِ دیوانه بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین جانم ز دل کی سیر گشتی</p></div>
<div class="m2"><p>اگر جانانه‌ام هم خانه بودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غلط کردم که در شریان جان است</p></div>
<div class="m2"><p>چه می‌گویم که بودی یا نبودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر عاقل نبودی در حجابی</p></div>
<div class="m2"><p>چو مجنون از خرد بیگانه بودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجا پیمان به سر دانستمی برد</p></div>
<div class="m2"><p>اگر نه بر کفم پیمانه بودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کبوتر گر قناعت پیشه کردی</p></div>
<div class="m2"><p>کجا در بندِ دام و دانه بودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر هر مرغکی بر شمعِ تسلیم</p></div>
<div class="m2"><p>فدایی‌وار چون پروانه بودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا چون شب‌پره ظلمت گزیدی</p></div>
<div class="m2"><p>کجا چون جغد در ویرانه بودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر عنقا نبودی در پسِ قاف</p></div>
<div class="m2"><p>میانِ خلق کی افسانه بودی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>علی الجمله دلم زین ورطه ی شاق</p></div>
<div class="m2"><p>اگر بیرون شدی مردانه بودی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نزاری را ز مبدا تا به اکنون</p></div>
<div class="m2"><p>همه کارش چنین مستانه بودی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه عمرش غرامت بر غرامت</p></div>
<div class="m2"><p>و گر شکرانه بر شکرانه بودی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ازین‌ها وارهاندی التقاطی</p></div>
<div class="m2"><p>گرم از جانب جانانه بودی</p></div></div>