---
title: >-
    شمارهٔ ۱۳۹۱
---
# شمارهٔ ۱۳۹۱

<div class="b" id="bn1"><div class="m1"><p>در باغ چنین سروی کی خاست به رعنایی</p></div>
<div class="m2"><p>بر چرخ چنین ماهی کی تافت به زیبایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر باده کنی در سر بر سینه زنی آتش</p></div>
<div class="m2"><p>ور بوسه دهی بر لب در فتنه بیفزایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسنِ تو به هم برزد بنیادِ خردمندان</p></div>
<div class="m2"><p>عشق تو فرود آمد برخاست شکیبایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کار کشی ما را وز غم بکشی ما را</p></div>
<div class="m2"><p>تا کی ز جفا کاری تا چند ز خود رایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه دوستی نه دشمن نه بی من و نه با من</p></div>
<div class="m2"><p>بر یک سخن ای دَه دل یک روز نمی پایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از من دل و دین بردی سهل است غمِ دنیا</p></div>
<div class="m2"><p>از جان رمقی باقی مانده ست چه فرمایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی چاره نزاری شد در پای غمت کُشته</p></div>
<div class="m2"><p>ای بی غمِ بی رحمت بر کَس بنبخشایی</p></div></div>