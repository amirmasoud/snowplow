---
title: >-
    شمارهٔ ۱۳۹۶
---
# شمارهٔ ۱۳۹۶

<div class="b" id="bn1"><div class="m1"><p>شب مفارقت و روز هجر و تنهایی</p></div>
<div class="m2"><p>که را بود به چنین شب دل شکیبایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرچه از شب دیجور خود گرفته ترست</p></div>
<div class="m2"><p>ملالت سوزندگانِ شیدایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه سود کان بت یوسف جمالِ بی رحمت</p></div>
<div class="m2"><p>خبر ندارد از این ماتم زلیخایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسوخت آتش حسرت مرا و قوّت نیست</p></div>
<div class="m2"><p>که در فراق بنالم ز ناتوانایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چگونه بار جدایی کشم به پشتی صبر</p></div>
<div class="m2"><p>مرا که پشت دو تا میشود ز یکتایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همان به است که در کُنج خانه بشینم</p></div>
<div class="m2"><p>ز دست سرزنش دوستان هر جایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به عاقلان نکنم رغبت از ملالت طبع</p></div>
<div class="m2"><p>خوش است صحبت دیوانگان سودایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون که توبه شکستم درست میگویم</p></div>
<div class="m2"><p>چه بیم پرده دری و چه بیم رسوایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نزاریا چو شکر میدهی جگر می خور</p></div>
<div class="m2"><p>که هم شکر خور مایی و هم شکر خایی</p></div></div>