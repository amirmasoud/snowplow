---
title: >-
    شمارهٔ ۴۹۱
---
# شمارهٔ ۴۹۱

<div class="b" id="bn1"><div class="m1"><p>چنین سروِ روان دیگر نباشد</p></div>
<div class="m2"><p>بر و بالا ازین خوش تر نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو تو سروی اگر آید در آغوش</p></div>
<div class="m2"><p>کسی را این هوس در سر نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که را باشد چنین ماهی که چون او</p></div>
<div class="m2"><p>خورم سوگند ماه و خور نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدین شکل و شمایل آدمی زاد</p></div>
<div class="m2"><p>اگر باشد مرا باور نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قراری می دهم که الّا به کویش</p></div>
<div class="m2"><p>قرارم بعد ازین دیگر نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکن عیب ار نزاری بی قرارست</p></div>
<div class="m2"><p>شکیبِ مزهر از ازهر نباشد</p></div></div>