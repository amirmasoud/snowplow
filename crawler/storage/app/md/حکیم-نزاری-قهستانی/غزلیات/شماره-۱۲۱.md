---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>میروم سویِ خرابات که پیرم آنجاست</p></div>
<div class="m2"><p>جای دیگر چه کنم عذر پذیرم آنجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به شکلم تو جدا بینی و دور از برِ دوست</p></div>
<div class="m2"><p>جان که از وی نفسی نیست گریزم آنجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست جایی دگر آن کس که ستم کرد و برفت</p></div>
<div class="m2"><p>دامنش روزِ مکافات بگیرم آنجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم آنجا به غریبی و به تنهایی خوی</p></div>
<div class="m2"><p>سپه و مملکت و تاج و سریرم آنجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ندارم خبر از خویشتن اینجا چه عجب</p></div>
<div class="m2"><p>فهم و وهم و نظر و فکر و ضمیرم آنجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند سرگشته روم در شب تاریکِ فراق</p></div>
<div class="m2"><p>رفتم آنجا که رخ بدرِ مُنیرم آنجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به چه مشغولم ازین جا به چه آنجا نروم</p></div>
<div class="m2"><p>که دل و جان نزاری فقیرم آنجاست</p></div></div>