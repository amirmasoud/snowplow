---
title: >-
    شمارهٔ ۷۷۲
---
# شمارهٔ ۷۷۲

<div class="b" id="bn1"><div class="m1"><p>سرِ پیوند ندارد صنمِ مهر گسل</p></div>
<div class="m2"><p>خود دلش داد که بر کند چنین از ما دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوست نادیده و نا کرده وداعی با او</p></div>
<div class="m2"><p>رفتنم واقعه یی مشکل و بودن مشکل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون روم چون سپرم راه چه تدبیر کنم</p></div>
<div class="m2"><p>خاطرم قیدِ مُقام است و قضا مستعجل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاکِ ره گل کنم از گریه و ترسم که شوم</p></div>
<div class="m2"><p>خجل از صحبتِ اصحاب و بمانم در گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا سحرگاه ز تاب و تفِ اندوهِ خلیل</p></div>
<div class="m2"><p>هر شبان گاه کنم برسرِ آتش منزل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من به پیرانه سر از دستِ دل آشفته شدم</p></div>
<div class="m2"><p>روز و شب در هوسِ صحبتِ خوبانِ چگل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر تشویر کی از پیش برآید دگرم</p></div>
<div class="m2"><p>چون نشینم پس ازین با عقلا در محفل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشقم از فطرتِ اُولا به در آورد نه عقل</p></div>
<div class="m2"><p>قابلِ امر محقّ است و مشنّع مبطل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نکند از عقبِ دوست نزاری رجعت</p></div>
<div class="m2"><p>لامحال از پیِ خورشید بود سرعتِ ظل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هیچ جنبش نبود بی اثرِ جاذبه ای</p></div>
<div class="m2"><p>چه کند پس روِ هنجارِ زمام است ابل</p></div></div>