---
title: >-
    شمارهٔ ۲۸۳
---
# شمارهٔ ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>از آن زمان که زمان در تحرّک استاده ست</p></div>
<div class="m2"><p>زمانه با تو مرا عهدِ دوستی داده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر نه با تو مرا اتّصالِ روحانی ست</p></div>
<div class="m2"><p>خیالِ روی تو پییشم چرا بر استاده ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به غم وجودِ مرا پروریده دایۀ عشق</p></div>
<div class="m2"><p>که غم ز مادرِ فطرت برایِ من زاده ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دامِ زلف در افتاده ام ز دانۀ عشق</p></div>
<div class="m2"><p>دلم ببین به کجا از کجا در افتاده ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دانه می نگرد دامِ غم نمی بیند</p></div>
<div class="m2"><p>عذاب جان من از غفلتِ دل ساده ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا که جان به لب آمد ز آرزویِ لبت</p></div>
<div class="m2"><p>مگر مُقسّمِ فطرت نصیبه ننهاده ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی رود ز سرم خار خارِ جامِ الست</p></div>
<div class="m2"><p>هنوز رنجِ خمار از بخارِ آن باده ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که بود روزِ نخستین حریفِ مجلسِ انس</p></div>
<div class="m2"><p>دریغ باز چه بودی که آمدی با دست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درونِ جانِ نزاری روایحِ غم اوست</p></div>
<div class="m2"><p>ذخیره ای که ز مبدایِ کون بنهاده ست</p></div></div>