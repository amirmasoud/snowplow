---
title: >-
    شمارهٔ ۸۷۸
---
# شمارهٔ ۸۷۸

<div class="b" id="bn1"><div class="m1"><p>مرا دیوانه می خوانند و با دیوانه می مانم</p></div>
<div class="m2"><p>ز خود بیگانه می دانند و هم من نیز می دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر با بت منم اینم وگر در کعبه بنشینم</p></div>
<div class="m2"><p>نه مرد مذهب و دینم نه اهل کفر و ایمانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو در بت خانه افتادم ز دیگر خانه آزادم</p></div>
<div class="m2"><p>به نقد امروز دل شادم که عشق آسوده می رانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رغبت عشق می بازم دگر شغلی نمی سازم</p></div>
<div class="m2"><p>نه از جنت همی نازم نه از دوزخ هراسانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قلم در حکم نیک و بد قضا در ما تقدم زد</p></div>
<div class="m2"><p>ندانم مقبلم یا رد چه باید هم برین سانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>علم گشتم به نادانی چه باک ار جاهلم خوانی</p></div>
<div class="m2"><p>چه گویم چون نمی دانی که از دانسته نادانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عیان بی عیان گفتم نشان بی نشان گفتم</p></div>
<div class="m2"><p>نزاری ترک جان گفتم کنون در بند جانانم</p></div></div>