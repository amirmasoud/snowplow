---
title: >-
    شمارهٔ ۸۲۳
---
# شمارهٔ ۸۲۳

<div class="b" id="bn1"><div class="m1"><p>شبِ فراق که بی رغبتی سفر کردم</p></div>
<div class="m2"><p>نبود فایده هر چند من حذر کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به اختیار نکردم ز خدمتِ تو سفر</p></div>
<div class="m2"><p>بلی ضرورتِ تکلیف بود اگر کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سوزِ سینه به هر منزلی که بگذشتم</p></div>
<div class="m2"><p>به خونِ دل ز رهِ دیده خاک تر کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سزا بداد مرا گوش مال فرقت تو</p></div>
<div class="m2"><p>به یک دو هفته که از خدمتت سفر کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دمی ز فکرِ تو خالی نبوده ام والله</p></div>
<div class="m2"><p>گمان مبر که مگر با تو دل دگر کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن به نامِ تو گفتم اگر سخن گفتم</p></div>
<div class="m2"><p>نظر به رویِ تو کردم اگر نظر کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبا به گردِ من اندر مراجعت نرسد</p></div>
<div class="m2"><p>سمندِ توسنِ شوق ترا چو بر کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به زخمِ تیرِ ملامت سپر نیندازم</p></div>
<div class="m2"><p>چو تیغ سینه به پیش بلا سپر کردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کفایت است جنونِ مرا ملامتِ خلق</p></div>
<div class="m2"><p>که من به سعیِ ملامت بسی بتر کردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا نزاریِ دیوانه خوان و ننگ مدار</p></div>
<div class="m2"><p>که نامِ عقل به دیوانگی سمر کردم</p></div></div>