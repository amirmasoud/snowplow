---
title: >-
    شمارهٔ ۱۰۱۸
---
# شمارهٔ ۱۰۱۸

<div class="b" id="bn1"><div class="m1"><p>اگر ز دوستی اوست خلق دشمن من</p></div>
<div class="m2"><p>رواست گو همه عالم مباش جز دشمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مواصلت نشود منقطع به خوف و خطر</p></div>
<div class="m2"><p>مخالفت نکند معتقد به سرّ و علن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشان دوستی دوستان صادق چیست</p></div>
<div class="m2"><p>فدای حضرت جانان شدن به جان و به تن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بدین که من آزادم از صلاح و فساد</p></div>
<div class="m2"><p>ستون عرش بخواهد شکست گو بشکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه کرده ام که بزاری اهل معنی نیست</p></div>
<div class="m2"><p>فکنده ام همه اغلال صورت از گردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه مشرکم همه از لاشریک له زنهار</p></div>
<div class="m2"><p>بُد او یگانه و آن گه من و تو و تو و من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدان که فایده از خویشتن شناسی چیست</p></div>
<div class="m2"><p>همین که تو همه هیچی و هیچ لاف مزن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصول وحدت و کثرت به هم معاذالله</p></div>
<div class="m2"><p>من و تو شرک بود در برابر ذوالمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو هیچ منمای از خود که زیرکان دانند</p></div>
<div class="m2"><p>که گنج را نبود جز به کنج ها مسکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ضمیر من نه محل نتایج سوداست</p></div>
<div class="m2"><p>که دختری ست چو مریم به روح آبستن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سفینه های نزاری کدام بحر عمیق</p></div>
<div class="m2"><p>مرصّعات جواهر کدام دُرِّ عَدَن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غلام همت آنم که از کفی خاشاک</p></div>
<div class="m2"><p>کم است در نظر رغبتش زمین و زمن</p></div></div>