---
title: >-
    شمارهٔ ۱۰۰۰
---
# شمارهٔ ۱۰۰۰

<div class="b" id="bn1"><div class="m1"><p>مرا جانانه‌ای باید که باشد غم گسارِ من</p></div>
<div class="m2"><p>میانِ نازکش باشد همه شب در کنارِ من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه خوش باشد دل آرامی که چون از خواب برخیزد</p></div>
<div class="m2"><p>به غمزه چشمِ مستِ او کند دفعِ خمار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر باشد قیامت باشد اللّهمَّ ارزُقنا</p></div>
<div class="m2"><p>همین از بخت و دولت چشم دارد انتظارِ من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندارم هیچ باقی در جهان جز مونسِ‌ساقی</p></div>
<div class="m2"><p>گزیرم نیست از یاری همین است اضطرارِ من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طمع ببریده‌ام الّا ز روحِ روح‌بخشیِ می</p></div>
<div class="m2"><p>که هم او زنده می‌دارد دلِ امّیدوارِ من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوانی رفت و در پیری به حسرت باز می‌گویم</p></div>
<div class="m2"><p>دریغا روزگارِ من دریغا روزگارِ من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همین دردِ دلی و زاریی و مختصر نامی</p></div>
<div class="m2"><p>دگر چیزی نمی‌دانم که ماند یادگارِ من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجب دارم اگر رونق پذیرد باز بازارم</p></div>
<div class="m2"><p>چو بر هم زد به کلّی شحنه‌ی تقدیر کارِ من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر انگشتِ‌پشیمانی گزیدن سود کی دارد؟</p></div>
<div class="m2"><p>چو بیرون شد زمامِ دل ز دستِ اختیارِ من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز فرطِ تشنگی دارم دلی پر آتش حسرت</p></div>
<div class="m2"><p>که خرسندی نمی‌آرد حدیثِ آب‌دارِ من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سخن جز بر ولا گفتن نزاری مشتبه باشد</p></div>
<div class="m2"><p>چو می‌گویی مگو جز بر مدارِ استیارِ من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غرض دانی چه دانم زین نصیحت حسبِ حال خود</p></div>
<div class="m2"><p>که دُرّ عقل بیرون شد ز عِقدِ اقتدار من</p></div></div>