---
title: >-
    شمارهٔ ۱۱۸۴
---
# شمارهٔ ۱۱۸۴

<div class="b" id="bn1"><div class="m1"><p>دریغا مهر و پیوندت که بگسستی و ببریدی</p></div>
<div class="m2"><p>نمی‌دانم که را بر من بدل کردی و بگزیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو تن در دوستی دادم به کام دشمنم کردی</p></div>
<div class="m2"><p>چو کلّی با تو پیوستم ز من یک‌باره ببریدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه کردم با تو جز یاری کزان موجب قفا دادی</p></div>
<div class="m2"><p>چه گُفتم با تو جز خوبی کز آن معنی برنجیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ردم کردی به تنهایی که دیدم در تو نقصانی</p></div>
<div class="m2"><p>جزاک الله ستم کردی عفاک الله خطا دیدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا گر با وفا بودم به آخر ترکِ من گفتی</p></div>
<div class="m2"><p>چرا گر ناسزا بودم به اوّل می‌پسندیدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دعوی عمرِ من بودی که بر من باد پیمودی</p></div>
<div class="m2"><p>به معنی بختِ من بودی که از من باز گردیدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلِ بی حاصلِ خود را سر و کاری نمی‌بینم</p></div>
<div class="m2"><p>مگر خود رونقی بیند که بازش برسکولیدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگفتی گر به جان آیم نباشم با تو جز یک دل</p></div>
<div class="m2"><p>چو وقتِ امتحان آمد به غایت سست جنبیدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نکو کردی نکو کردی سزاوارم سزاوارم</p></div>
<div class="m2"><p>که دردِ دل بیفزودی و غم بر جان گماریدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بخستی دیده ی چشمم چو روی از پرده بگشادی</p></div>
<div class="m2"><p>ببستی پای امّیدم چو خار از راه برچیدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نزاری را به صد زاری رها کردی و برگشتی</p></div>
<div class="m2"><p>پس از چندین وفا داری چنین نقشی سگالیدی</p></div></div>