---
title: >-
    شمارهٔ ۶۳۰
---
# شمارهٔ ۶۳۰

<div class="b" id="bn1"><div class="m1"><p>بلای عشق تو ناگه ز در درآمد باز</p></div>
<div class="m2"><p>مدار دور سلامت مگر سرآمد باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حُبوب مهر تو در کشت زار سینهٔ من</p></div>
<div class="m2"><p>برست و تخم پراکند و در برآمد باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدنگ غمزهٔ تو بر دلم به نسبت حال</p></div>
<div class="m2"><p>هزار بار زهر بار خوش‌تر آمد باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم اگر چه برفت از ستیز تو یک چند</p></div>
<div class="m2"><p>ولی به عجز چو خسرو ز شکر آمد باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گمان مبر که ز کویت دل ستم‌کش من</p></div>
<div class="m2"><p>به ترّهات رقیب ستم گر آمد باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رقیب گو به ملامت مبالغت می‌کن</p></div>
<div class="m2"><p>مرا چه غم چو دل آرام در برآمد باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گذشت نوبت هجران نزاریا خوش باش</p></div>
<div class="m2"><p>که دور وصل و زمان طرف درآمد باز</p></div></div>