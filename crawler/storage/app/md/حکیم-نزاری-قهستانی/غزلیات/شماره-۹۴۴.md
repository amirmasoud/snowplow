---
title: >-
    شمارهٔ ۹۴۴
---
# شمارهٔ ۹۴۴

<div class="b" id="bn1"><div class="m1"><p>ای باد صبا رَو ز سپاهان به قهستان</p></div>
<div class="m2"><p>بگذر چو به قاین رسی از طرف گل¬ستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاران مرا در چمن باغ طلب کن</p></div>
<div class="m2"><p>از جام صبوحی شده مستان و چه مستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مستان که به یک جام دو عالم بفروشند</p></div>
<div class="m2"><p>وآن گه نخرند از فلکِ شعبده دستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در پای گل ایشان همه هم زانوی عشرت</p></div>
<div class="m2"><p>من در غم ایشان چو عنادل همه دستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی که درین واقعه بر من به شب آید</p></div>
<div class="m2"><p>بر دیده من روز نباشد که شب است آن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک همه آفاق جهان بر سر من باد</p></div>
<div class="m2"><p>گر دارم ازین غم سرِ باغ و دلِ بستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایشان همه دستان زده بر نغمه بربط</p></div>
<div class="m2"><p>من برسر از اندوهِ جدایی زده دستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیهات که چون می گذرانم شبِ اندوه</p></div>
<div class="m2"><p>خوش خفته و آسوده چه داند به شبِ¬ستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رویی دگرم نیست به هر حال نزاری</p></div>
<div class="m2"><p>هم دستِ مدد خواستن از دامن هستان</p></div></div>