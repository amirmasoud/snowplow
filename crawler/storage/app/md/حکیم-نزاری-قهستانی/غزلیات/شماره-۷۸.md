---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>قیامت بر انگیخت ما را ز خواب</p></div>
<div class="m2"><p>به محشر رسیدیم و خیر المآب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرافیل وحدت فرو کوفت صور</p></div>
<div class="m2"><p>ولی مرده دل در نیامد ز خواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آورد از سوزناکان دمار</p></div>
<div class="m2"><p>ز افسرده نه تف بر آمد نه تاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قیامت در این حال ما منتظر</p></div>
<div class="m2"><p>وگر بر نیندازد از رخ نقاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دیوان روز مظالم به حشر</p></div>
<div class="m2"><p>بماند خجل از سوال و جواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه آن جا به کارست از این جا ببر</p></div>
<div class="m2"><p>چو بردی ز فردوس بشنو خطاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به زلزال ارض و به طی سما</p></div>
<div class="m2"><p>چه حاجت تو را وقت خود بازیاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وگر هم بر افتد زمان و زمین</p></div>
<div class="m2"><p>مخور غم چو ایمن شدی از عذاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به زاری نزاری فرومانده ای</p></div>
<div class="m2"><p>چو مجرم میان ثواب و عقاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حساب ار به اعمال و کردار ماست</p></div>
<div class="m2"><p>خدایا مکن نا امید از ثواب</p></div></div>