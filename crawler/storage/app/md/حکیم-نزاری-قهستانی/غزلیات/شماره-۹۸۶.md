---
title: >-
    شمارهٔ ۹۸۶
---
# شمارهٔ ۹۸۶

<div class="b" id="bn1"><div class="m1"><p>ای دلِ درمانده به حبس وطن</p></div>
<div class="m2"><p>لافِ سرا پرده‌ی بالا مزن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما و منِ توست حُجُب در میان</p></div>
<div class="m2"><p>این حُجُباتِ من و ما برفکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ورنی در قطعِ‌طریقِ کمال</p></div>
<div class="m2"><p>نیست حجابی بتر از ما و من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دعویِ‌اخلاص و محبت مکن</p></div>
<div class="m2"><p>پس چو مرایی به ریا جان مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا برو و پس روی او مکن</p></div>
<div class="m2"><p>یا کمِ جان گیر و برستی ز تن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا به مقاماتِ محبت در آی</p></div>
<div class="m2"><p>یا سرِ خود گیر و زما بر شکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در رهِ اخلاص مبین کفر و دین</p></div>
<div class="m2"><p>در صفِ عشّاق مبین مرد و زن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوشِ عطا می‌کنی ای اصلِ‌بخل</p></div>
<div class="m2"><p>لاف صفا می‌زنی ای دُردِ دَن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شخص ز همسایگی‌ات در عذاب</p></div>
<div class="m2"><p>روح ز هم خانگی‌ات در محن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با که توان گفت که من سال و ماه</p></div>
<div class="m2"><p>در چه عذابم ز دلِ خویشتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مغز تهی کردم و رمز آشکار</p></div>
<div class="m2"><p>هیچ نه سِر ماند به من نه عَلَن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آری اگر چند صدف شد تهی</p></div>
<div class="m2"><p>کم نشود قیمتِ دُرّ عَدَن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قصّه چه گویم که به جان آمده‌ست</p></div>
<div class="m2"><p>کار نزاری ز دلِ پر فتن</p></div></div>