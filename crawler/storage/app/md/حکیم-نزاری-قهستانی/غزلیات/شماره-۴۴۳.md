---
title: >-
    شمارهٔ ۴۴۳
---
# شمارهٔ ۴۴۳

<div class="b" id="bn1"><div class="m1"><p>گر ز دل آهی برون خواهیم کرد</p></div>
<div class="m2"><p>عالمی را سرنگون خواهیم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوز هجران است بل کاین تیره شب</p></div>
<div class="m2"><p>رستخیز است آه چون خواهیم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در فراقت کان عذاب مطلق است</p></div>
<div class="m2"><p>دیده و دل هر دو خون خواهیم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو شفقت کم کنی سهل است ما</p></div>
<div class="m2"><p>مهربانی ها فزون خواهیم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اقتراح از طبع در خواهیم خواست</p></div>
<div class="m2"><p>اشتباه از دل برون خواهیم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق بی آلایشی خواهیم باخت</p></div>
<div class="m2"><p>نفس سرکش را زبون خواهیم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل اگر مانع شود ما دفع او</p></div>
<div class="m2"><p>از مقامات جنون خواهیم کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روی در محراب جان خواهیم داشت</p></div>
<div class="m2"><p>پشت بر دنیای دون خواهیم کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون نزاری هرکه درس از ما گرفت</p></div>
<div class="m2"><p>در جنونش ذوفنون خواهیم کرد</p></div></div>