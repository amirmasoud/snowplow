---
title: >-
    شمارهٔ ۱۰۶۹
---
# شمارهٔ ۱۰۶۹

<div class="b" id="bn1"><div class="m1"><p>این منم خود را چنین در دست غم بگذاشته</p></div>
<div class="m2"><p>پای مال محنت و جور و ستم بگذاشته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل به غربت داده و آرام جان در انتظار</p></div>
<div class="m2"><p>عهد او بشکسته و شرط قدم بگذاشته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قصه دردی از او هر شب صبا آرد به من</p></div>
<div class="m2"><p>حرف پنهان کرده و نقش قلم بگذاشته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده باشی نیک بختان ریاضت دیده را</p></div>
<div class="m2"><p>از پی اسلام تعظیم صنم بگذاشته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر صنم آن است من خواهم پرستش کردنش</p></div>
<div class="m2"><p>ملت و اسلام را بینی به هم بگذاشته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر زمان جانی دگر بخشد رقیبان را حبیب</p></div>
<div class="m2"><p>از وجود خویش ما را در عدم بگذاشته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق را فرمود تا بنمود قلب ما به خلق</p></div>
<div class="m2"><p>عقل را حیران چو سکه بردرم بگذاشته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لشکر سودا به غوغا بر سد ما تاخته</p></div>
<div class="m2"><p>در میان خلق ما را چون علم بگذاشته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این نه شرط دوستی باشد که دل بازش دهد</p></div>
<div class="m2"><p>چون نزاری هم دمی را در ندم بگذاشته</p></div></div>