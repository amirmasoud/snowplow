---
title: >-
    شمارهٔ ۲۱۷
---
# شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>ساقی سر خم زود برانداز که عید است</p></div>
<div class="m2"><p>پژمان منشین بزم فروساز که عید است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی ز نهان خوردن و در خفیه شمیدن</p></div>
<div class="m2"><p>با خلق بگو روشن و سرباز که عید است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بردار نقاب از خم ابروی هلالت</p></div>
<div class="m2"><p>بر بام شو و بانگ درانداز که عید است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو از افق طرف تتق ماه دوهفته</p></div>
<div class="m2"><p>بنمود خم ابروی طنّاز که عید است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امشب من و ابروی هلال و قدح می</p></div>
<div class="m2"><p>فردا تو به بازار فرو تاز که عید است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بن گاه حریفان و طرب خانه یاران</p></div>
<div class="m2"><p>از اقمشه روزه بپرداز که عید است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین پیش اگرت مصلحتی بودو حجابی</p></div>
<div class="m2"><p>زین بیش منه عذر و مکن ناز که عید است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از ما به نزاری برسان مژده که در حال</p></div>
<div class="m2"><p>خلوت گهت أراسته کن باز که عید است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگذشت مه روزه کنون مقدم شوّال</p></div>
<div class="m2"><p>دارید به اکرام و به اعزاز که عید است</p></div></div>