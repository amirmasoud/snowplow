---
title: >-
    شمارهٔ ۳۸۸
---
# شمارهٔ ۳۸۸

<div class="b" id="bn1"><div class="m1"><p>خوش است مجلس اخلاص امّتان مسیح</p></div>
<div class="m2"><p>شراب های مصفّا زساقیان ملیح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مباد مدرسه و خانقه که بیزارم</p></div>
<div class="m2"><p>ز عالمان شنیع و ز زاهدان قبیح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خطیب بر سری منبر چه ژاژ می خاید</p></div>
<div class="m2"><p>عجب که شرم نمی دارد از دروغ صریح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فسانه یی دو ز بهر فریب کرده زبر</p></div>
<div class="m2"><p>کجا کلام محقق کجا کلام صحیح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو علم داند و بر جهل می کند اصرار</p></div>
<div class="m2"><p>میان عالم و جاهل کجا بود ترجیح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فغان اهل دل از زاهدانِ معترض است</p></div>
<div class="m2"><p>دل از پی دو درم سیم و زر، به کف تسبیح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تیغ عشق مکش گردن ای فقیر که شیر</p></div>
<div class="m2"><p>به عجز سر بنهد هم چو گوسفند ذبیح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز کف منه چو نزاری مفرّحی که به حکم</p></div>
<div class="m2"><p>شوند بی دل و ابکم ازو شجاع و فصیح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غلام ساقی خویشم اگر غلام من است</p></div>
<div class="m2"><p>که مرده زنده کند باز هم چنان که مسیح</p></div></div>