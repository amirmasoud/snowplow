---
title: >-
    شمارهٔ ۵۶۵
---
# شمارهٔ ۵۶۵

<div class="b" id="bn1"><div class="m1"><p>بیا که مهر تو با جان ز جسم ما برود</p></div>
<div class="m2"><p>محبت ازلی کی چنین ز جا برود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میان اهل صفا گر ز آه خشم و عتاب</p></div>
<div class="m2"><p>کدورتی بود آن هم به ماجرا برود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دست عقل نباشد زمام عقل آری</p></div>
<div class="m2"><p>حجاب عقل شود عشق تا قضا برود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکایتی که مرا از تو و تو را از ماست</p></div>
<div class="m2"><p>همان به است که آخر کنیم تا برود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تحمل سخنت می کنم به دیده ی سخت</p></div>
<div class="m2"><p>که سست عهد بود هر که از جفا برود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار بار منادی به شهر دردادم</p></div>
<div class="m2"><p>که خاک بر سر آن کز سر وفا برود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو جمع باش که این خسته پریشان حال</p></div>
<div class="m2"><p>به سر به پیش تو آید اگر به پا برود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو قادری چه توان، اختیار باید کرد</p></div>
<div class="m2"><p>اگر هزار جفا بر سر از شما برود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز اعتقاد نزاری به صد پریشانی</p></div>
<div class="m2"><p>نعوذ بالله اگر هرگز این صفا برود</p></div></div>