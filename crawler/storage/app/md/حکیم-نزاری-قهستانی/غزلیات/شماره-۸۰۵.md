---
title: >-
    شمارهٔ ۸۰۵
---
# شمارهٔ ۸۰۵

<div class="b" id="bn1"><div class="m1"><p>یک امشبی که به خدمت به باده بنشستم</p></div>
<div class="m2"><p>مکن به بی ادبی سرزنش که بس مستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر اشارتِ بوسی کنم مکن منعم</p></div>
<div class="m2"><p>وگر به حلقۀ زلفت برم مبر دستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو بس غریب نوازی اگر نه باده به دست</p></div>
<div class="m2"><p>مرا چه زهره که هم زانوی تو بنشستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه حّدِ مرتبۀ هم چو من ضعیفی بود</p></div>
<div class="m2"><p>که دل به چون تو حریفی شگرف در بستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ورایِ این چه سعادت بود همین مقدار</p></div>
<div class="m2"><p>کفایت است که در دولتِ تو پیوستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اختیار شدم صید زلفِ مفتولت</p></div>
<div class="m2"><p>همان رمیده نی ام کز کمند می جستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز عشق باختن ار توبه کرده بودم شکر</p></div>
<div class="m2"><p>که بر محبّتِ رویِ تو توبه بشکستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر به دیدۀ من پای نهی شاید</p></div>
<div class="m2"><p>برو که در قدمت هم چو خاکِ ره پستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خلافِ رایِ نزاری مکن که کم یابی</p></div>
<div class="m2"><p>مطیع تر ز چنین بنده ای که من هستم</p></div></div>