---
title: >-
    شمارهٔ ۳۳۷
---
# شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>دریغ عمر که بی روی دوستان بگذشت</p></div>
<div class="m2"><p>چو باد صبح که بر طرف بوستان بگذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریغ سود ندارد چو اختیار از دست</p></div>
<div class="m2"><p>برفت هم چو خدنگی که از کمان بگذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهار عمر جوانی و بی غمی افسوس</p></div>
<div class="m2"><p>که هم چو قافله ی باد مهرگان بگذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو خود قیاس کن ای بی خبر که در دل ما</p></div>
<div class="m2"><p>چه آتش است که دودش ز آسمان بگذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسوخت حلق من از بس که برق آه دلم</p></div>
<div class="m2"><p>ز سینه هم چو براق سبک عنان بگذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنوز دیده به هم می نهم تعالی الله</p></div>
<div class="m2"><p>ز هر چه بر سرم از گردش زمان بگذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه حاصل از سفر بی مراد هیچ همین</p></div>
<div class="m2"><p>فسانه ای که فلان آمد و فلان بگذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نزاریا چه کنی چاره نیست تن درده</p></div>
<div class="m2"><p>به جور چرخ که کار تو زین و آن بگذشت</p></div></div>