---
title: >-
    شمارهٔ ۸۰۱
---
# شمارهٔ ۸۰۱

<div class="b" id="bn1"><div class="m1"><p>کوزه بر دوشِ راهب دیرم</p></div>
<div class="m2"><p>حلقه در گوش ساجد لاتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من که دردی کش خراباتم</p></div>
<div class="m2"><p>فارغ از طمطراق و طاماتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه از مشرکانِ توحیدم</p></div>
<div class="m2"><p>گاه از موقِنانِ غلّاتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه که در حوضِ کوثرِ تحقیق</p></div>
<div class="m2"><p>آب جاری بود مجاراتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه که در درجِ مدرجِ توحید</p></div>
<div class="m2"><p>در مکنون بود عباراتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خرد می سزد مفاخرتم</p></div>
<div class="m2"><p>به هنر می رسد مباهاتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل کلّی به اختیار دهد</p></div>
<div class="m2"><p>بوسه بر آستانِ ابیاتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفسِ قدسی به احتیاط نهد</p></div>
<div class="m2"><p>دست بر نبضِ نفی و اثباتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگر افسانه گوی پندارد</p></div>
<div class="m2"><p>که چو او راوی روایاتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیستم از معلّمانِ جهول</p></div>
<div class="m2"><p>نه که من ناسخِ مقالاتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیده کو تا ببیند اسرارم</p></div>
<div class="m2"><p>گوش تا بشنود اشاراتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه نزاری به هرزه لاف مزن</p></div>
<div class="m2"><p>بیش ازین برمگو محالاتم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه به دعوی به ابتدا گفتی</p></div>
<div class="m2"><p>فارغ از طمطراق و طاماتم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یک سخن راست گفته ای از خویش</p></div>
<div class="m2"><p>من که دردی کشِ خراباتم</p></div></div>