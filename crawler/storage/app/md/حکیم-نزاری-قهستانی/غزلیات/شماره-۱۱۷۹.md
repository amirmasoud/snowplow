---
title: >-
    شمارهٔ ۱۱۷۹
---
# شمارهٔ ۱۱۷۹

<div class="b" id="bn1"><div class="m1"><p>یاد باد آن که مرا با تو قراری بودی</p></div>
<div class="m2"><p>در میانِ من و تو واسطه یاری بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه از سعیِ رقیبان گله‌ها داشتمی</p></div>
<div class="m2"><p>گه‌گهم با رخ تو هم سر و کاری بودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم رقیبانِ تو صد بار جفا بردندی</p></div>
<div class="m2"><p>تا مرا یک نفسی پیشِ تو باری بودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش دوش کجا با که کشیدی باده</p></div>
<div class="m2"><p>هر چه در نرگسِ مستِ تو خماری بودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه شب زلفِ تو بر گردنِ من پیچیده</p></div>
<div class="m2"><p>هم چو از عنبر تر تافته ماری بودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلبلِ مستِ گلستانِ تو بودم هم‌وار</p></div>
<div class="m2"><p>خلق را با من از آن واسطه خاری بودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمی نیست مرا هیچ تعلّق با او</p></div>
<div class="m2"><p>این زمان معترفم معترف آری بودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کنارم همه شب تا به میان در خون است</p></div>
<div class="m2"><p>در میان کاش که با دوست کناری بودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناله ی عشق تو بر صد ره رساندی همه شب</p></div>
<div class="m2"><p>در جهان گر چو نزاری تو زاری بودی</p></div></div>