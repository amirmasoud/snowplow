---
title: >-
    شمارهٔ ۴۲۶
---
# شمارهٔ ۴۲۶

<div class="b" id="bn1"><div class="m1"><p>یکی را دوست می دارم که چون من سد رهی دارد</p></div>
<div class="m2"><p>ندانم تا ز حالِ من کم و بیش آگهی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرم سر می رود نتوانم از کویش گذر کردن</p></div>
<div class="m2"><p>خداوندست و بر جان و دلم فرمان دهی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیالِ محض بین باری کزو وصلی طمع دارم</p></div>
<div class="m2"><p>محال اندیش را سودا چنین بر ابلهی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو رشکِ آفتاب و غیرتِ سروست چون گویم</p></div>
<div class="m2"><p>جمالِ آفتاب و قامتِ سروِ سهی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که دیده ست آرزومندی چو من مظلوم کز هجران</p></div>
<div class="m2"><p>درونی از شکایت پر ولی مغزی تُهی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبا گر فرصتی یابی بگو آرامِ جانم را</p></div>
<div class="m2"><p>نزاری چشم بر راهت به امیّدِ بهی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وگر باور نمی داری بیا بنگر به چشمِ خود</p></div>
<div class="m2"><p>که از سودایِ شفتالوت رویِ چون بهی دارد</p></div></div>