---
title: >-
    شمارهٔ ۱۳۱۹
---
# شمارهٔ ۱۳۱۹

<div class="b" id="bn1"><div class="m1"><p>به قاین بگذر ای باد ار توانی</p></div>
<div class="m2"><p>زمین‌بوسی ببر آن جا که دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگوی آرام جانم را که جانم</p></div>
<div class="m2"><p>برآمد در غمت از ناتوانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دست باد صبح از نافه‌ی زلف</p></div>
<div class="m2"><p>چه باشد گر به من بویی رسانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تویی لیلی و من مجنونِ واله</p></div>
<div class="m2"><p>تویی شیرین و من فرهاد ثانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو مجنونم که در اندوه لیلی</p></div>
<div class="m2"><p>بریدم صحبت از انسی و جانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شدم پر از غمِ دوری دریغا</p></div>
<div class="m2"><p>که کردم در سرِ غربت جوانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو فرهادم که بر من تلخ کرده‌ست</p></div>
<div class="m2"><p>فراق روی شیرین زندگانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به حیرت می‌کنم صبری ضروری</p></div>
<div class="m2"><p>به غیرت می‌کشم دردی نهانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی‌میرد نزاری در فراقت</p></div>
<div class="m2"><p>به شوخی می‌کند الحق گرانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سبک روحی بود او در صفاهان</p></div>
<div class="m2"><p>تو در قاین زهی نامهربانی</p></div></div>