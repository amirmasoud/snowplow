---
title: >-
    شمارهٔ ۵۱۲
---
# شمارهٔ ۵۱۲

<div class="b" id="bn1"><div class="m1"><p>با دل از دست رفت و کار دگر شد</p></div>
<div class="m2"><p>بخت ز من برشکست و یار دگر شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی نمود از نقاب و باز نپوشید</p></div>
<div class="m2"><p>عهد نهان کرد و آشکار دگر شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با دل بد عهد چون کنم که برانداخت</p></div>
<div class="m2"><p>قاعده صلح و کارزار دگر شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم خلاصی که داشتم ز بلاها</p></div>
<div class="m2"><p>خود نه چنان بود و انتظار دگر شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من چه کنم بر وصال یار بد آموز</p></div>
<div class="m2"><p>کار دگر گشت و آن قرار دگر شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخت سرآسیمه باز شیوه دگر کرد</p></div>
<div class="m2"><p>با من سرگشته روزگار دگر شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقطه پرگار انتظار بگردید</p></div>
<div class="m2"><p>مرکز امید با مدار دگر شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم نخورم ار نزاریا ز زمانه</p></div>
<div class="m2"><p>با تو چو بخت ستیزه کار دگر شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این همه دل می کند نه بخت که با من</p></div>
<div class="m2"><p>از سر پیمان هزار بار دگر شد</p></div></div>