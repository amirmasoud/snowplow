---
title: >-
    شمارهٔ ۴۲۰
---
# شمارهٔ ۴۲۰

<div class="b" id="bn1"><div class="m1"><p>جمادست آن که دل داری ندارد</p></div>
<div class="m2"><p>یقین می دان که جان باری ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو آن منگر که صاحب دولت این جا</p></div>
<div class="m2"><p>به جز عیش و طرب کاری ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگرچه ملک و مال و جاه دارد</p></div>
<div class="m2"><p>ولی چون یارِ من یاری ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز من باور مکن گر چینِ زلفش</p></div>
<div class="m2"><p>دلی در زیرِ هر تاری ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز من مشنو اگر از غمزۀ او</p></div>
<div class="m2"><p>خرد در هر قدم خاری ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو از هر گوشه تُرکِ چشمِ مستش</p></div>
<div class="m2"><p>نظر بر خونِ هشیاری ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمند اندازِ گیسویش به هر جا</p></div>
<div class="m2"><p>رسن در حلقِ عیّاری ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو هم مشنو ز من گر این گواهی</p></div>
<div class="m2"><p>به نزدیکِ تو مقداری ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دروغ است این سخن گر از هر اعضا</p></div>
<div class="m2"><p>نزاری نالۀ زاری ندارد</p></div></div>