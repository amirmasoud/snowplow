---
title: >-
    شمارهٔ ۱۲۹۵
---
# شمارهٔ ۱۲۹۵

<div class="b" id="bn1"><div class="m1"><p>ای اگر بوی بری از نفسِ خرّمِ می</p></div>
<div class="m2"><p>معجزِ عیسیِ مریم به تو بنماید وَی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به جانت نرسد ذوق نیابی وه وه</p></div>
<div class="m2"><p>تا تجرّع نکنی قدر ندانی هَی هَی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معتقد باش به مستانِ برانداخته دین</p></div>
<div class="m2"><p>بشنو این خورده و طومارِ بزرگی کن طَی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محتسب گفت بیا برشکن از خمر و خمار</p></div>
<div class="m2"><p>بعد ازین توبه کن از مطرب و چنگ و دف و نی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم آری نخورم بیش می و خوردم و عقل</p></div>
<div class="m2"><p>معترض گشت و ز تشویر نشستم در خَوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز با عقل درافتادم و گفتم بی جان</p></div>
<div class="m2"><p>آدمی زنده محال است کجا کو که و کی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جانِ من زنده به جامی ست که باشد پُرجان</p></div>
<div class="m2"><p>حِرز من تازه به وردی ست که باشد با حی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه مستان خرابیم چنان می نخوریم</p></div>
<div class="m2"><p>که بود دردِ سر و رنجِ خمارش در پَی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راست چون دایره بر مرکز خم می گردیم</p></div>
<div class="m2"><p>خنب قطب است مگر گویی و مخمور جُدَی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا بود منطقه و خطّ و محیط و محور</p></div>
<div class="m2"><p>بر نگردم چو نزاری ز مدارِ خُمِ مَی</p></div></div>