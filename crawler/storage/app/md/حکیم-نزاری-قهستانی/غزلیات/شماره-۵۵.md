---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>خیز ای غلام باده درافکن به جام ما</p></div>
<div class="m2"><p>کز وصل توست گردش گردون غلام ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر لایق است چشمة خورشید را فلک</p></div>
<div class="m2"><p>خورشید باده را فلکی کن ز جام ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن قاصد است باده که جان است مقصدش</p></div>
<div class="m2"><p>جز وی به جان ما که رساند سلام ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بزم گه چو دست تو گردان کند قدح</p></div>
<div class="m2"><p>گردان شود سپهر سعادت به کام ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را ز عقل ما همه اندوه و آفت است</p></div>
<div class="m2"><p>جز می ز عقل ما که کشد انتقام ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می ده می ای غلام که از می در اوفتد</p></div>
<div class="m2"><p>صد لذت و نشاط به راحت به دام ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک ره که در جهان نتوان زیستن مقیم</p></div>
<div class="m2"><p>بی عشوه بی صواب نباشد مقام ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیکی کنیم و باده خوریم و عطا دهیم</p></div>
<div class="m2"><p>تا اقتدا کنند به ما خاص و عام ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وآنها که بد کنند نزاری چنین کند</p></div>
<div class="m2"><p>این شعر ما بس است بدیشان پیام ما</p></div></div>