---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>هرگزت روزی هوای ما نخاست</p></div>
<div class="m2"><p>آخر این نامهربانی تا کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما به تو مشتاق و تو از ما ملول</p></div>
<div class="m2"><p>عاشق تنها به حسرت مبتلاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل به دل گویند ره دارد بلی</p></div>
<div class="m2"><p>چون حجاب از راه برخیزد رواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیک شخص ناتوان را نیز هم</p></div>
<div class="m2"><p>از پی پیوند جسمانی رجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان اگر جان می‌فزاید طرفه نیست</p></div>
<div class="m2"><p>جسم باری در غم هجران بکاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوست کی دارد شکیبایی ز دوست</p></div>
<div class="m2"><p>من نمی‌دانم که این طاقت که راست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سنگ‌دل باشد به هرحالی صبور</p></div>
<div class="m2"><p>ور نه در هجران جان مانع چراست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با دلی چون آبگینه راستی</p></div>
<div class="m2"><p>صابری کردن نه کار جنس ماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک دمه آسایش دیدار دوست</p></div>
<div class="m2"><p>پیش دانا آفرینش را بهاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای نزاری تا به کی از قیل و قال</p></div>
<div class="m2"><p>گفت و گو اندر ره وحدت خطاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نام و ننگ و کفر و دین و هست و نیست</p></div>
<div class="m2"><p>هرچه غیر از دوست می‌خواهی هواست</p></div></div>