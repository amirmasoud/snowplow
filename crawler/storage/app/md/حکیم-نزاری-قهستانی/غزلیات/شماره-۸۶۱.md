---
title: >-
    شمارهٔ ۸۶۱
---
# شمارهٔ ۸۶۱

<div class="b" id="bn1"><div class="m1"><p>نیامدی و من از انتظار می سوزم</p></div>
<div class="m2"><p>در آرزویِ وصال تو زار می سوزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آبِ دیده ی من رحم کن اگر یاری</p></div>
<div class="m2"><p>که من بر آتشِ هجرانِ یار می سوزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بی قراریِ من بر قرار بی خبری</p></div>
<div class="m2"><p>ولی من از غم تو برقرار می سوزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسوختم ز فراقِ تو و ندانستم</p></div>
<div class="m2"><p>که از برایِ که بهرِ چه کار می سوزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سوختن خبری نیست همچو شمع مرا</p></div>
<div class="m2"><p>چه اختیار که بی اختیار می سوزم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب تر این که به هر انجمن ز غایتِ شوق</p></div>
<div class="m2"><p>ز شمع دورم و پروانه‌وار می سوزم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز چشمِ مستِ تو محروم عینِ مخمورست</p></div>
<div class="m2"><p>اگر چه مستِ تو ام در خمار می سوزم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو گُل‌عذاری و من بلبل و تو فارغ از آن</p></div>
<div class="m2"><p>که من بر آتشِ هجران چو خار می سوزم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قسم به آتشِ یاقوتِ آبدارِ لبت</p></div>
<div class="m2"><p>که با دو دیده ی یاقوت‌بار می سوزم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در انتظار نزاریِ زار می گوید</p></div>
<div class="m2"><p>نیامدی و من از انتظار می سوزم</p></div></div>