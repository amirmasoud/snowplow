---
title: >-
    شمارهٔ ۸۳۰
---
# شمارهٔ ۸۳۰

<div class="b" id="bn1"><div class="m1"><p>من همان مستم و شوریده کز اوّل بودم</p></div>
<div class="m2"><p>تا نبودم به تو مشغول معطّل بودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بریدم ز تو بی تو به خطا معذورم</p></div>
<div class="m2"><p>زان که موقوفِ محالاتِ مخیّل بودم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم اگر باز کنی باز شود از تو به تو</p></div>
<div class="m2"><p>من اگر جز به تو دیدم به تو احول بودم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نبودم به رخت ناظر و حاضر به وجود</p></div>
<div class="m2"><p>به خیالت به خیالت که مغفّل بودم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردمان گر ز تو گویند که بودم خرسند</p></div>
<div class="m2"><p>نه چنان است ولی معترفم بل بودم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من کی ام با تو و بی تو نتوانم بودن</p></div>
<div class="m2"><p>در میان هم نتوان گفت مزلزل بودم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز از حلقۀ عشّاق نبودم بیرون</p></div>
<div class="m2"><p>بلکه در سلسلۀ عشق مسلسل بودم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوش عشق آمد و در گوشِ نزاری می گفت</p></div>
<div class="m2"><p>که ز مبدایِ ازل بر تو موکَّل بودم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم آری من و تو هر دو ز یک معراجیم</p></div>
<div class="m2"><p>تو به وحی آمدی و من ز تو مرسل بودم</p></div></div>