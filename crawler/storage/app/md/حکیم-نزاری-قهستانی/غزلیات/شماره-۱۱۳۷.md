---
title: >-
    شمارهٔ ۱۱۳۷
---
# شمارهٔ ۱۱۳۷

<div class="b" id="bn1"><div class="m1"><p>ز ما برشکستی و از ما بجستی</p></div>
<div class="m2"><p>بر آنی که از زحمتِ ما برستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلِ ما ببردی سرِ ما نداری</p></div>
<div class="m2"><p>درست است اگر ماجرا برشکستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال تو از چشم ما نیست غایب</p></div>
<div class="m2"><p>بلی شیوه ی ما بود بت‌پرستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا تا بدیدم به تو بگرویدم</p></div>
<div class="m2"><p>دل از ما ببردی و در جان نشستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملامت مکن بر نزاریِ مسکین</p></div>
<div class="m2"><p>اگر پیش با خود نیاید ز مستی</p></div></div>