---
title: >-
    شمارهٔ ۱۸۰
---
# شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>اگر عنایت غم نیستی که یار من است</p></div>
<div class="m2"><p>که را غم من و اندوه بیشمار من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم تو یک نفس از من نمی شود غایب</p></div>
<div class="m2"><p>هم اوست در همه عالم که یارغار من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا نه یار و نه اغیار جز تو یاری نیست</p></div>
<div class="m2"><p>غمی دگر نخورم چون غم تو یار من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دام زلف تو افتادم و عجب تر این</p></div>
<div class="m2"><p>که من مقیدم و دام من شکار من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر به وصل نخواهی نواخت واویلا</p></div>
<div class="m2"><p>که مالک غم هجران در انتظار من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهاده ام سر بیچارگی و مسکینی</p></div>
<div class="m2"><p>همین دگر چه به بازوی اقتدار من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ترک هستی خود گفتن و به بودن نیست</p></div>
<div class="m2"><p>نه مرد کار چنینم اگر نه کار من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز پیر عشق شنیدم که گفت هر حلّاج</p></div>
<div class="m2"><p>نه رازدار انا الحق نه مرد دار من است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شدم ز دست وگر باورت نمی شود</p></div>
<div class="m2"><p>به خاک پای تو سوگند استوار من است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بس که خون دل از چشم من فرو ریزد</p></div>
<div class="m2"><p>گمان برند که یاقوت در کنار من است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رعایتی دگرم گر نمی کنی باری</p></div>
<div class="m2"><p>همین قدر که نزاری زار زار من است</p></div></div>