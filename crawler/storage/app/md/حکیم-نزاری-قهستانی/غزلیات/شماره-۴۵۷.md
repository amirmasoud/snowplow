---
title: >-
    شمارهٔ ۴۵۷
---
# شمارهٔ ۴۵۷

<div class="b" id="bn1"><div class="m1"><p>زمانه عهد و وفا عاقبت ز سر گیرد</p></div>
<div class="m2"><p>مفارقت ز میان من و تو برگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا تو جان و جهانی و خاک بر سر دل</p></div>
<div class="m2"><p>اگر به جای تو هرگز کسی دگر گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دل هر آینه در کار دوست خواهد شد</p></div>
<div class="m2"><p>چرا به هرزه کسی یار مختصر گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محب صادق هم خانه بلا باشد</p></div>
<div class="m2"><p>فدای دوست چرا از بلا حذر گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به وصل دست نمی گیردم فراق حبیب</p></div>
<div class="m2"><p>که داند آری عاقبت مگر گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب مدار که انسان کشته زنده شود</p></div>
<div class="m2"><p>که گر سرش برود دوستی ز سر گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حذر ز مشعله عشق کز حرارت شوق</p></div>
<div class="m2"><p>به یک شرر در و دیوار عقل در گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نزاری از صدف سینه هر زمان بی دوست</p></div>
<div class="m2"><p>چو ابر دامنِ افلاک در گهر گیرد</p></div></div>