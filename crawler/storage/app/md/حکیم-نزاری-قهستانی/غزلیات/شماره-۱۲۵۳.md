---
title: >-
    شمارهٔ ۱۲۵۳
---
# شمارهٔ ۱۲۵۳

<div class="b" id="bn1"><div class="m1"><p>خوشا که موسمِ گل بامداد برخیزی</p></div>
<div class="m2"><p>به باغ باده خوریّ و ز خلق بگریزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برو به روز جوانی جهان پیر بخور</p></div>
<div class="m2"><p>که آن به است که با روزگار نستیزی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازین نصیحت بیهوده‌ای فقیه ترا</p></div>
<div class="m2"><p>چه حاصل است که روغن به ریگ می‌ریزی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو نیز هم مکن از عشق احتراز که ذوق</p></div>
<div class="m2"><p>نباشدت مگر از خویشتن بپرهیزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ولی ترا نبود بخت آن که از سرِ شوق</p></div>
<div class="m2"><p>به دام زلف کمند افکنی برآویزی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنوز نقد تمنای دوست‌یابی باز</p></div>
<div class="m2"><p>پس از قیامت اگر خاک من بپرویزی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمانه خاک تو هم عاقبت به پرویزن</p></div>
<div class="m2"><p>فرو گذارد اگر ماورای پرویزی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حیل مکن که براق اجل عنان ترا</p></div>
<div class="m2"><p>چو باد گیرد اگر خود به سیر شبدیزی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برو دلت به کسی ده ز خویشتن به درآی</p></div>
<div class="m2"><p>به هرزه چند نشینی و چند برخیزی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نزاریا چه بلایی به گوشه ا‌ی بنشین</p></div>
<div class="m2"><p>که عاقبت ز میان فتنه‌ی برانگیزی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرین سخن که تو داری برون دهی خونت</p></div>
<div class="m2"><p>به گردنِ تو مگر نکته درهم آمیزی</p></div></div>