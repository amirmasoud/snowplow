---
title: >-
    شمارهٔ ۷۹۹
---
# شمارهٔ ۷۹۹

<div class="b" id="bn1"><div class="m1"><p>بیا کز تو نمی باشد شکیبم</p></div>
<div class="m2"><p>به جان آمد دل از چندین فریبم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن تدبیرم ای جان در جدایی</p></div>
<div class="m2"><p>که خون شد زهره آخر زین نهیبم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر نازی کنی آری و لیکن</p></div>
<div class="m2"><p>نباشد طاقت چندین عَتیبم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر صد ره عنان از من بپیچی</p></div>
<div class="m2"><p>منت تا زنده باشم در رکیبم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهل تا سر نهم بر پشت پایت</p></div>
<div class="m2"><p>چه می داری چو زلفت بر نشیبم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه با تو می توانم بُد نه با خود</p></div>
<div class="m2"><p>که خواهد برد بیرون زین حجیبم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدین زاری بریدن از نزاری</p></div>
<div class="m2"><p>بدین زودی نباشد در حسیبم</p></div></div>