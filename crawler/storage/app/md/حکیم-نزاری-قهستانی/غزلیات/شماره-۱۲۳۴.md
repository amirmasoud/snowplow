---
title: >-
    شمارهٔ ۱۲۳۴
---
# شمارهٔ ۱۲۳۴

<div class="b" id="bn1"><div class="m1"><p>بیش ازینم نشود از تو میسّر به صبوری</p></div>
<div class="m2"><p>گر ترا هست مرا نیست دگر طاقتِ دوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم‌چنان من ز نصیحت‌گر بی‌هوده ملولم</p></div>
<div class="m2"><p>که تو از صحبتِ بی‌حاصلِ این بنده نفوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من اگر با تو نباشم ز خیالم تو نباشی</p></div>
<div class="m2"><p>غایب ای دیده که چون باصره در عینِ حضوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر این‌اند رقیبان تو ما را چو غریبان</p></div>
<div class="m2"><p>کام و ناکام بباید شد از این شهر ضروری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر به بیغوله ما هیچ فرو نایدت آری</p></div>
<div class="m2"><p>منظرت رشک بهشت است و تو خود غیرت حوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقشِ دیوار به صورت که تو داری بفریبد</p></div>
<div class="m2"><p>هیچ عیبت نکنم گر به خود از حسن غیوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب چو بر بام برآیی و ز برقع به درآیی</p></div>
<div class="m2"><p>هم‌چو انوارِ تجلّی به صفت بر کُهِ‌ طوری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به تماشا به گلستان شو و چادر ز جمالت</p></div>
<div class="m2"><p>باز کن تا نزند لاف لطافت گل سوری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمع و شاهد به شبِ خلوت ما هر دو تو باشی</p></div>
<div class="m2"><p>که چو خورشید سراپایِ وجودت همه نوری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بوی دودِ نفست راحت روح است نزاری</p></div>
<div class="m2"><p>گو بسوزند چو عودت که بر آتش چو بخوری</p></div></div>