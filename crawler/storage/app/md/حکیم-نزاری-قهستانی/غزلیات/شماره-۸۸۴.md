---
title: >-
    شمارهٔ ۸۸۴
---
# شمارهٔ ۸۸۴

<div class="b" id="bn1"><div class="m1"><p>یک ام شبی که به خلوت جمال دوست ببینم</p></div>
<div class="m2"><p>به از ممالک روی زمین به زیر نگینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سماع بربط و جام خوش و حریف موافق</p></div>
<div class="m2"><p>به روی دوست برآنم که در بهشت برینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه مرد باشم و باشم سزای آتش دوزخ</p></div>
<div class="m2"><p>اگر بهشت برین بر سرای دوست گزینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر سعادت آنم بود که وقت شهادت</p></div>
<div class="m2"><p>به پیش دوست بمیرم زهی حیات پسینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو مهربانی من بین که از حوالی کویش</p></div>
<div class="m2"><p>برون نمی شوم ار می کشد رقیب به کینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرابخواره و شاهدپرست و عاشق ورندم</p></div>
<div class="m2"><p>و گر حیات بود تا مدار دور و برینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هیچ شهر نرفتم که برنخاست قیامت</p></div>
<div class="m2"><p>صلاح کار من است ار به گوشه ای بنشینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرا به شیفتگی کردنم معاف ندارند</p></div>
<div class="m2"><p>که آشنا شده ی مردمان رند لعینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برادران حقیقی و دوستان قدیمی</p></div>
<div class="m2"><p>برای مزد خدا رها کنید چنینم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به روی دوست درست است اعتقاد نزاری</p></div>
<div class="m2"><p>کجا روم چه کنم گو مباش دنیی و دینم</p></div></div>