---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>ای در نقاب حسن نهان کرده آفتاب</p></div>
<div class="m2"><p>خطّی بر آفتاب کشیده ز مشک ناب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش فکنده در جگر لاله عارضت</p></div>
<div class="m2"><p>وز برگ نسترن بر و رویت ببرده آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باد صبا ز بوی عرقچین نازکت</p></div>
<div class="m2"><p>چون روضه از روایح فردوس مستطاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاله ز غیرت رخ گل گون تو به داغ</p></div>
<div class="m2"><p>سنبل ز رشک گیسوی مفتول توبه تاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بگذرد ز بغلتاق تو نسیم</p></div>
<div class="m2"><p>بر گل ستان ز آتش غیرت شود گل آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عیبت همین که دیر به ما می رسی و زود</p></div>
<div class="m2"><p>بنیاد عهد می کنی و می کنی خراب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آرام نیست بی تو دل بی قرار ما</p></div>
<div class="m2"><p>دانی به خون خویش چرا می کند شتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشتاق را شکیب نباشد ز روی دوست</p></div>
<div class="m2"><p>سیماب را گریز نباشد ز اضطراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی وجه بود خطّ تو بر وجه خون من</p></div>
<div class="m2"><p>بر خون من چه حاجت خطی ست نا صواب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غوغای غمزه ی تو ز مغزم ببرد هوش</p></div>
<div class="m2"><p>سودای نرگس تو ز چشمم ربود خواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رنگ لب تو دارد برگ نشاط تو</p></div>
<div class="m2"><p>زان فتنه شد نزاری شوریده بر شراب</p></div></div>