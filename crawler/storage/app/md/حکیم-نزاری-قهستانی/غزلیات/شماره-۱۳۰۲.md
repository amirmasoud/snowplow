---
title: >-
    شمارهٔ ۱۳۰۲
---
# شمارهٔ ۱۳۰۲

<div class="b" id="bn1"><div class="m1"><p>ساقیا می بیار حالی می</p></div>
<div class="m2"><p>که ندارم سرِ کجا که و کی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلوتی ‌کن که عشق بپسندد</p></div>
<div class="m2"><p>نه چو نفرت گرفته عشق از وی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دردِ مخموری مرا دانی</p></div>
<div class="m2"><p>که نباشد علاج الا می</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ درمان دگر نخواهد بود</p></div>
<div class="m2"><p>گفته‌اند آخِرُ الدَوا الکی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز گردد به اصلِ خود هر چیز</p></div>
<div class="m2"><p>چند گویی بس از شی ولاشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خنب شخص من است و می جانم</p></div>
<div class="m2"><p>زنده بی جان کجا بود رگ و پی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سرم کن چنان شرابی پُر</p></div>
<div class="m2"><p>که ز پیشانیم بریزد خوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شرحِ ماء العنب نخوانده‌ای</p></div>
<div class="m2"><p>وِ مِن الماءِ کُلّ شِی ءِ حی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جامِ می بر کف نزاری نه</p></div>
<div class="m2"><p>تا برآرد دمار از دمِ دی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ورقِ نام و ننگ را طی کن</p></div>
<div class="m2"><p>تا کی از طمطراقِ حاتمِ طی</p></div></div>