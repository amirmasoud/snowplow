---
title: >-
    شمارهٔ ۱۱۶۲
---
# شمارهٔ ۱۱۶۲

<div class="b" id="bn1"><div class="m1"><p>هیچ یاری بود که برگردی</p></div>
<div class="m2"><p>بعدِ چندین که دوستی کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تو گو روزگار برخور ، ما</p></div>
<div class="m2"><p>برنخوردیم و خونِ ما خوردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلِ خلقی بسوختی آخِر</p></div>
<div class="m2"><p>تا کی ای شوخ نا جوان مردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو بدین جفتِ چشم و ابروی طاق</p></div>
<div class="m2"><p>رستخیز از جهان برآوردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دردِ بی چارگانِ سوخته دل</p></div>
<div class="m2"><p>چه شناسی که فارغ از دردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خونِ دل می دهی نزاری را</p></div>
<div class="m2"><p>کش به خونِ جگر بپروردی</p></div></div>