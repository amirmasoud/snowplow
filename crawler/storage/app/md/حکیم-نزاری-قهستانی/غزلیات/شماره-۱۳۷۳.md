---
title: >-
    شمارهٔ ۱۳۷۳
---
# شمارهٔ ۱۳۷۳

<div class="b" id="bn1"><div class="m1"><p>صبا گر توانی گذر کن به جایی</p></div>
<div class="m2"><p>سلامی به شاهی رسان از گدایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غریبی ستم دیده ی روزگاری</p></div>
<div class="m2"><p>گرفتار بی‌چاره ی مبتلایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرآسیمه بختی، به خون غرقه چشمی</p></div>
<div class="m2"><p>فروبسته دستی ، به گِل مانده پایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جگرخواره دل‌داده‌ ای نیم جانی</p></div>
<div class="m2"><p>جفا دیده بی‌حاصلی بی‌نوایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آزاد سروی به سوسن زبانی</p></div>
<div class="m2"><p>به گل‌برگ رویی ز بلبل نوایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به شیرین دهانی ز فرهاد مهری</p></div>
<div class="m2"><p>به لیلی صفاتی ز مجنون وفایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز لب تشنه جانی به آبِ حیاتی</p></div>
<div class="m2"><p>ز تن رفته روحی به عیسی صفایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آدم مثالی به باغ بهشتی</p></div>
<div class="m2"><p>ز یعقوب حالی به یوسف لقایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به یاقوت لعلی به ضحاک خشمی</p></div>
<div class="m2"><p>به خورشید رویی به جمشید رایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به لاله جبینی به سنبل نسیمی</p></div>
<div class="m2"><p>به نرگس کلاهی به غنچه قبایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به فربه سرینی به لاغر میانی</p></div>
<div class="m2"><p>به محکم ستیزی به مشکل گشایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به غمزه گدازی به ابرو نوازی</p></div>
<div class="m2"><p>به بیگانه‌رویی به چشم آشنایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگوی ای صبا کای ز دستِ تو هر دم</p></div>
<div class="m2"><p>مرا تازه بر سر گذشته بلایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روانم به بویِ تو در اضطرابی</p></div>
<div class="m2"><p>دلم با خیالِ تو در ماجرایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نزاری به زاری لگدکوب غربت</p></div>
<div class="m2"><p>نه رویش به راهی نه راهش به جایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرش جان به یک دم رسد تا بمیرد</p></div>
<div class="m2"><p>هنوزش به وصل تو باشد رجایی</p></div></div>