---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>دانی چه مصلحت را بل غاغ شد بخارا</p></div>
<div class="m2"><p>تا این ستیزه گاران بی دل کنند مارا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین قوم در خراسان الاّ بلا نخیزد</p></div>
<div class="m2"><p>شکلی کنید و دفعی بنشستن بلا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم از آن جماعت یاری به چنگم آید</p></div>
<div class="m2"><p>آن دل که داشتم نیز از دست شد قضا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاری چنان که باید دیدم ولی دریغا</p></div>
<div class="m2"><p>گر التفات کردی در عشق مبتلا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترکی که گر به دعوی از خانقه در آید</p></div>
<div class="m2"><p>از دین و دل بر آرد پیران پارسا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر زلف تاب داده باز افکند به گردن</p></div>
<div class="m2"><p>از نافه ی نغوله مشکین کند صبا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر فرق او ببیند بالای ماه رویش</p></div>
<div class="m2"><p>خط در کشد منجّم بر چرخ استوا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ملک خوب رویی آخر چه کم بباشد</p></div>
<div class="m2"><p>گر بوسه یی ببخشد خشنودی خدا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زر باید ای نزاری تا کی خروش و زاری</p></div>
<div class="m2"><p>تمکین نمی کند کس درویش بینوا را</p></div></div>