---
title: >-
    شمارهٔ ۴۸۰
---
# شمارهٔ ۴۸۰

<div class="b" id="bn1"><div class="m1"><p>من آزاد آمدم زان دوست کز دشمن بتر باشد</p></div>
<div class="m2"><p>به ظاهر معتبر گوید به باطن مختصر باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حضور و غیبتِ یاران مخلص مختلف نبود</p></div>
<div class="m2"><p>حذر اولاتر از یاری که در غیبت دگر باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه از بی دانشی بود این که دشمن دوست دانستم</p></div>
<div class="m2"><p>ازین بازی بسی افتد قضا را این قدر باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نباید عهد پیوستن به شوخی باز بشکستن</p></div>
<div class="m2"><p>دلِ صاحب نظر خستن خطایِ معتبر باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدان می آردم غیرت که دل پیش سگ اندازم</p></div>
<div class="m2"><p>که هر کو دل به ناکس داد از دشمن بتر باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محّبِ صادقِ یک دل دو چشم از غیر بر بندد</p></div>
<div class="m2"><p>به هر کس باز نگشاید مگر صاحب نظر باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حریفِ صورت از شاهد مرادِ خود کند حاصل</p></div>
<div class="m2"><p>چو بر صورت نظر دارد ز معنی بی خبر باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نزاری بعد ازین معنی به صورت در نپیوندی</p></div>
<div class="m2"><p>گرت از عالمِ صورت برین معنی گذر باشد</p></div></div>