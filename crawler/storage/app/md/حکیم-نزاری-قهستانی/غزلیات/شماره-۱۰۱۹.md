---
title: >-
    شمارهٔ ۱۰۱۹
---
# شمارهٔ ۱۰۱۹

<div class="b" id="bn1"><div class="m1"><p>کو شیفته ای کجاست چون من</p></div>
<div class="m2"><p>بر باد فراق داده خزمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذاشته جان و تن معطل</p></div>
<div class="m2"><p>بی جان چه کنم کجا برم تن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی جان چه کند سفر ، مجویید</p></div>
<div class="m2"><p>چیزی که کسی نیافت از من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کوره ی آتش جدایی</p></div>
<div class="m2"><p>نی من که چو موم گردد آهن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با دل گفتم که پرده راز</p></div>
<div class="m2"><p>یکباره ز روی بر میفکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت این چه حکایت است رفتم</p></div>
<div class="m2"><p>من هم چو تو نیستم تو جان کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من گرد دهان دوست گردم</p></div>
<div class="m2"><p>نی هم جو توام به کام دشمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می گرد به گرد سر چو گردن</p></div>
<div class="m2"><p>از طوق وفا کشید گردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای نور دو دیده ی نزاری</p></div>
<div class="m2"><p>بی روی تو نیست دیده روشن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فریاد رسم ز هجر فریاد</p></div>
<div class="m2"><p>مگذار چنینم آخر ای جَن</p></div></div>