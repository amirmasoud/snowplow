---
title: >-
    شمارهٔ ۱۱۶۳
---
# شمارهٔ ۱۱۶۳

<div class="b" id="bn1"><div class="m1"><p>نه قبول کرده بودی که ز عهد برنگردی</p></div>
<div class="m2"><p>چه گناه کردم آخر که خلافِ عهد کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کجا روم زکویت به که التجا نمایم</p></div>
<div class="m2"><p>که تو حیاتِ جانی که توم دوایِ دردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من و دانش و محبت تو و هرچنان که خواهی</p></div>
<div class="m2"><p>چه کری کند به خونم که تو آستین نوردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نتوان به حیله بردن نه محبت از دلِ من</p></div>
<div class="m2"><p>نه ز زلفِ شب سیاهی نه ز رویِ روز زردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه ملامتِ احبّا نه علامت اطبّا</p></div>
<div class="m2"><p>که نه آن حرارت است این که ز دل رود به سردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به وفا و عهد واجب شده سعی و جهد بر من</p></div>
<div class="m2"><p>مگر این قدر نتوانم که به سر برم به مردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دو چشم گفتی اول بخورم غمِ نزاری</p></div>
<div class="m2"><p>چو بدان رسید یک جو غمِ کارِ ما نخوردی</p></div></div>