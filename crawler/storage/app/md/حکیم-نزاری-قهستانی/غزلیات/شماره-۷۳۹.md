---
title: >-
    شمارهٔ ۷۳۹
---
# شمارهٔ ۷۳۹

<div class="b" id="bn1"><div class="m1"><p>ای پسر ار بگذری سوی خرابات عشق</p></div>
<div class="m2"><p>کشف شود بر دلت سر کرامات عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام می بیخودی تا نکشی باخودی</p></div>
<div class="m2"><p>مست شوی گم شود ذات تو در ذات عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده ی خفاش را طاقت خورشید نیست</p></div>
<div class="m2"><p>بر تو کفایت بود پرتو ذرات عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل تو جزوی بگشت لاف مزن بیش از این</p></div>
<div class="m2"><p>حل نکندعقل تو جزو کمالات عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهره مدزد از حریف بازی کودک مکن</p></div>
<div class="m2"><p>برد تو ناممکن است ناشده شه مات عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وسوسه آموز کیست طالب طامات عقل</p></div>
<div class="m2"><p>برق جهان سوز چیست پیک مهمات عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شش جهت شرق و غرب یک جهت عشق نیست</p></div>
<div class="m2"><p>وهم کسی چون رسد وصف مسافات عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دعوی دیوانگی کس نکند منقطع</p></div>
<div class="m2"><p>شرع نیارد سپرد راه مقالات عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شعر نزاری مخوان ورنه تعصب مکن</p></div>
<div class="m2"><p>تا بتوانی شنود حرف مقامات عشق</p></div></div>