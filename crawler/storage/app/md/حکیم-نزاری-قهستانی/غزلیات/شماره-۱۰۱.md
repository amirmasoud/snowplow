---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>دوش برقع ز روی باز انداخت</p></div>
<div class="m2"><p>گفت باید مرا به من بشناخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خودی خودت بباید سوخت</p></div>
<div class="m2"><p>با مراد منت بباید ساخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا درو جان جان نزول کند</p></div>
<div class="m2"><p>خانه باید ز خویشتن پرداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر تسلیم پیش گیر چو چنگ</p></div>
<div class="m2"><p>متغیر مشو ز ضربِ نواخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایمنش کرد و فارغ از دوزخ</p></div>
<div class="m2"><p>آتش عشق هر که را بنواخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نکند اعتراض بر مجنون</p></div>
<div class="m2"><p>هر که با عاقلان کند انداخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نزاری پیاده شو ز وجود</p></div>
<div class="m2"><p>تا توانی بر آفرینش تاخت</p></div></div>