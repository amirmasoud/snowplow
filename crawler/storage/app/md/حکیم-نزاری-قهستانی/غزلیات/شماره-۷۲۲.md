---
title: >-
    شمارهٔ ۷۲۲
---
# شمارهٔ ۷۲۲

<div class="b" id="bn1"><div class="m1"><p>همه را شادی و مارا غم جانانه خویش</p></div>
<div class="m2"><p>همه با همدم و ما با دل دیوانه خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مبتلای غم و محنت زده هجرانیم</p></div>
<div class="m2"><p>آشنا ناشده با دلبر بیگانه خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سر آتش و آبیم ز چشم و دل خود</p></div>
<div class="m2"><p>چند سوزیم در این تنگ قفس خانه خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دم این سوخته پروانه ما یعنی دل</p></div>
<div class="m2"><p>جان به کف بر نهد از همت مردانه ی خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راست چون جغد گرفتم به خرابی مسکن</p></div>
<div class="m2"><p>نه که چون گنج نهانیم به ویرانه خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر زمانی ز پس پرده برون آید دوست</p></div>
<div class="m2"><p>پیش آن شمع بسوزیم ز پروانه خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرد اگر معتکف خاک در دوست بود</p></div>
<div class="m2"><p>به که بر مسند تعظیم به کاشانه خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سال ها شد که به دریای عدم غواصیم</p></div>
<div class="m2"><p>تا وجودی به کف آریم ز دردانه خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هان نزاری مطلب صاف ز خم خانه دهر</p></div>
<div class="m2"><p>چونکه در دُرد زدیم اول پیمانه ی خویش</p></div></div>