---
title: >-
    شمارهٔ ۱۳۴۰
---
# شمارهٔ ۱۳۴۰

<div class="b" id="bn1"><div class="m1"><p>ای گر درآید از درم سرمست یار قاینی</p></div>
<div class="m2"><p>بر جان نهم، بر دل نهم، دستِ نگار قاینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم نمیگویم ز که مستم نمیگویم ز چه</p></div>
<div class="m2"><p>مشهور باشد در جهان چشم خمار قاینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرامِ جانی یافتم کز دل قرارم می برد</p></div>
<div class="m2"><p>دل بردن و جان سوختن این است کارِ قاینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخّ و نسیج انداخته شمع از میان برداشته</p></div>
<div class="m2"><p>یا رب توان خفتن چنین شب در کنارِ قاینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زهره ندارم کاین سخن پیش کسی پیدا کنم</p></div>
<div class="m2"><p>آری چه غم چون می برم غم یادگارِ قاینی</p></div></div>