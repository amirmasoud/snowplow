---
title: >-
    شمارهٔ ۱۱۳۲
---
# شمارهٔ ۱۱۳۲

<div class="b" id="bn1"><div class="m1"><p>ما را بده از کوثرِ خم‌خانه شرابی</p></div>
<div class="m2"><p>تعجیل کن ای دوست که داریم شتابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش آر سبک جامِ جم عشق و بزن زود</p></div>
<div class="m2"><p>بر آتشِ تیز جگر سوخته آبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن آب به اسم است و به فعل آتشِ سوزان</p></div>
<div class="m2"><p>زان آتش سوزان جگرم کرد کبابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما پاک بسوزیم که خود پاک بسوزند</p></div>
<div class="m2"><p>در کارگه عشقِ تو سد خام به تابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مرتبه ی عالم دنیا به ضرورت</p></div>
<div class="m2"><p>تقدیر توان کرد خطایی و صوابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عالم وحدت نبود حکم دو وجهی</p></div>
<div class="m2"><p>آن‌جا نتوان گفت جمالی و نقابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظّاره‌گه غیب حضورست و وزین جا</p></div>
<div class="m2"><p>محجوب نداند که جز او نیست حجابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دیده ی هر دیده که مستغرق نورست</p></div>
<div class="m2"><p>این گنبد فیروزه بود کم ز حبابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با عشق تشبّه نکند عقل و مثالش</p></div>
<div class="m2"><p>آن است که طاووسی سازد ز غرابی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حلاج نباشد نه به دعوی نه به معنی</p></div>
<div class="m2"><p>هر دزد که بر دار کشندش به طنابی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از سدره نشینان شده مخصوص محقّق</p></div>
<div class="m2"><p>هر لحظه به تشریفِ کمالی و ثوابی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا خود چه خلل عالم معمورِ جهان را</p></div>
<div class="m2"><p>گر مست برون شد ز خرابات خرابی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گویند نزاری ز پس توبه دگربار</p></div>
<div class="m2"><p>شد با سرِ می هست مرا طرفه جوابی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از صومعه با می‌کده رفتم چه شد آخر</p></div>
<div class="m2"><p>گر عشق برون برد خری را ز خلابی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دارد به مقیمانِ خرابات ارادت</p></div>
<div class="m2"><p>جای دگرش نیست نه ملجأ نه مآبی</p></div></div>