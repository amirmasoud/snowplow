---
title: >-
    شمارهٔ ۸۵۵
---
# شمارهٔ ۸۵۵

<div class="b" id="bn1"><div class="m1"><p>جان و دل بنهاده ام تا می خورم</p></div>
<div class="m2"><p>دین و دل بفروختم تا می خرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب به روز آخر کی آرم بی شراب</p></div>
<div class="m2"><p>بی می آخرروز با شب چون برم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی می اَم دم بر نمی آید ز حلق</p></div>
<div class="m2"><p>بل فرو می ریزد از هر پیکرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می توانم دید با می سرَ غیب</p></div>
<div class="m2"><p>گر به چشم استحالت بنگرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می شود بیضا کفم از نور می</p></div>
<div class="m2"><p>هر چه یاد از لن ترانی آورم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با خلیلم خوش تر آید از حریر</p></div>
<div class="m2"><p>گر بود بر فرش آتش بسترم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از حبیبم باز می دارد رقیب</p></div>
<div class="m2"><p>گاه و بی گه تا به کویش نگذرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر زمان الحمدلله العزیز</p></div>
<div class="m2"><p>روی در روی خیالش خوش ترم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوست خود می آید و او بی خبر</p></div>
<div class="m2"><p>بل که من هم، وین که دارد باورم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن که دارد در همه اکوان ظهور</p></div>
<div class="m2"><p>طرفه باشد گر درآید از درم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از ملامت گوی بستاند به حق</p></div>
<div class="m2"><p>داد من روز قیامت داورم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>معترض عیب نزاری گو مکن</p></div>
<div class="m2"><p>من چنین بیهوده غم تا کی خورم</p></div></div>