---
title: >-
    شمارهٔ ۶۳۴
---
# شمارهٔ ۶۳۴

<div class="b" id="bn1"><div class="m1"><p>چشمی که داشتم به امید وصال باز</p></div>
<div class="m2"><p>بر هم فتاده شد به ضرورت خیال باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برقع فرو گذاشت به رویم خیال دوست</p></div>
<div class="m2"><p>تا رغم من نقاب گشاد از جمال باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسیار چون سکندر محروم نا امید</p></div>
<div class="m2"><p>ناچاره تشنه گشت بر آب زلال باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در امتحان عشق ندانم که عقل و صبر</p></div>
<div class="m2"><p>تا طاقت آورند و کنند احتمال باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم دارم از عنایت حق چشم آن که زود</p></div>
<div class="m2"><p>روزی کند اعادت آن اتّصال باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجموع نیستم ز پریشانی فراق</p></div>
<div class="m2"><p>آری بس اتصال که شد انفصال باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌چاره حالیا به بلا مبتلا شده‌ست</p></div>
<div class="m2"><p>تا چون شود نزاری شوریده حال باز</p></div></div>