---
title: >-
    شمارهٔ ۱۱۰۳
---
# شمارهٔ ۱۱۰۳

<div class="b" id="bn1"><div class="m1"><p>تا قدم در ره مردان ننهی مردانه</p></div>
<div class="m2"><p>لافِ مردی مزن ای خواجهٔ نافرزانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حریمِ حرمِ عشق ترا ره ندهند</p></div>
<div class="m2"><p>تا که از خویش به کلّی نشوی بیگانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خویشتن بین بنبیند به جز از خود کس را</p></div>
<div class="m2"><p>به کسی بین نه به خود تا نبود افسانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل آن‌جا چه کند چون نتواند ره برد</p></div>
<div class="m2"><p>مرد باید که بود شیفته و دیوانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قبله از راه حقیقت نکند جز رخ دوست</p></div>
<div class="m2"><p>کعبه سازد ز سر صدق درِ می‌خانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسته ی توبه و پیمان مجازی نشود</p></div>
<div class="m2"><p>جان نهد از کف و از کف ننهد پیمانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرّ اسرار چو شمع است از او نور نیافت</p></div>
<div class="m2"><p>هر که بر شمع نشد سوخته چون پروانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نزاری اگر از دامِ بلا برگذری</p></div>
<div class="m2"><p>نبود در سرت از حرص هوایِ دانه</p></div></div>