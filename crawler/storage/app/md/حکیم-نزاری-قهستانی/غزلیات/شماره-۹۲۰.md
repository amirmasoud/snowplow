---
title: >-
    شمارهٔ ۹۲۰
---
# شمارهٔ ۹۲۰

<div class="b" id="bn1"><div class="m1"><p>ما که دیوانگانِ مدهوشیم</p></div>
<div class="m2"><p>عشق از مردمان نمی‌پوشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه با عشق برنمی‌آییم</p></div>
<div class="m2"><p>هم به نوعی که هست می‌کوشیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مقامات عشق می‌بازیم</p></div>
<div class="m2"><p>در خرابات دُرد می‌نوشیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنده‌ی شاهدانِ خوش چشمیم</p></div>
<div class="m2"><p>بل که هندویِ حلقه در گوشیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که را دوست داشتیم برو</p></div>
<div class="m2"><p>هر زمانی زمانه بفروشیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاه بینندگانِ بی‌چشمیم</p></div>
<div class="m2"><p>کاه گویندگانِ خاموشیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راه بی‌منزل است و می‌پوییم</p></div>
<div class="m2"><p>بحر بی‌ساحل است و می‌جوشیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حرفِ دیوانگان بیار که ما</p></div>
<div class="m2"><p>سخنِ عاقلان بننیوشیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون نزاریِ مستِ لایعقل</p></div>
<div class="m2"><p>واله و بی‌قرار و بی‌هوشیم</p></div></div>