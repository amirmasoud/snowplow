---
title: >-
    شمارهٔ ۲۳۶
---
# شمارهٔ ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>مرا از روی خوبان ناگزیرست</p></div>
<div class="m2"><p>نظر بر شاهدان چشمم منیرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برو خاطر به یاری ده که دایم</p></div>
<div class="m2"><p>دلِ صاحب نظر جایی اسیرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مریدی کو جوانی در بر آورد</p></div>
<div class="m2"><p>عجب گر دیگرش پروای پیرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نثارِ یک قدم در پایِ محبوب</p></div>
<div class="m2"><p>اگر صد جان برافشانی حقیرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو گر باور نمی داری که بی دوست</p></div>
<div class="m2"><p>بخواهی مرد ما را دل پذیرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر بی دوست خواهد بود فردوس</p></div>
<div class="m2"><p>هوایِ باغ طوبا زمهریرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جانان زندۀ باقی توان بود</p></div>
<div class="m2"><p>که جان ها را از آن جا ناگزیرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به صورت نقشِ شیرین داشت فرهاد</p></div>
<div class="m2"><p>بلی بر سنگ و ما را در ضمیرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نزاری دایه در خواب است و تو طفل</p></div>
<div class="m2"><p>بگریی خون گرت حاجت به شیرست</p></div></div>