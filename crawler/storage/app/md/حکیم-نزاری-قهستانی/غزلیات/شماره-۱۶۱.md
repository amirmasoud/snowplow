---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>آن را که در فراق صبوری مسلم است</p></div>
<div class="m2"><p>آه از دلش که سخت تر از سنگ محکم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیچاره من که از تف دود دلم ز حلق</p></div>
<div class="m2"><p>بر هر نفس که می‌رود آهی مقدّم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر سینه ام نه کورهٔ آهنگرست چیست؟</p></div>
<div class="m2"><p>کز سوز اندرون نفسم آتشین دم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرشب خیال روی تو در چشم های من</p></div>
<div class="m2"><p>چون عکس روز بر سر دریای قلزم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دربند عشق دوست به دربند چون بود</p></div>
<div class="m2"><p>دربند چه معاینه باب جهنم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویند هولناک بود روز رستخیز</p></div>
<div class="m2"><p>یعنی شب فراق به تشویش از آن کم است؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از غم اگر بمرد کسی در فراق دوست</p></div>
<div class="m2"><p>آن کس به اتفاق منم وان غم این غم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خرّم وجود آنکه ببیند تو را به خواب</p></div>
<div class="m2"><p>یا خود بدان رسد که وصالش مسلم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یادآور از نزاری محنت کشیده کو</p></div>
<div class="m2"><p>هر جا چنان که هست به یاد تو خرّم است</p></div></div>