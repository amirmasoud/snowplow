---
title: >-
    شمارهٔ ۶۱۵
---
# شمارهٔ ۶۱۵

<div class="b" id="bn1"><div class="m1"><p>یار با ما یک زمانی در میان آید مگر</p></div>
<div class="m2"><p>پرده از رویِ جهان‌آرای بگشاید مگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمِ آن دارم که هم‌چون روی شهرآرایِ خویش</p></div>
<div class="m2"><p>کلبۀ احزانِ ما یک شب بیاراید مگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با سرِ پیمانۀ فطرت شود هم عاقبت</p></div>
<div class="m2"><p>یک شبی تا روز با ما باده پیماید مگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محرمی می‌بایدم کز من پیامی می‌برد</p></div>
<div class="m2"><p>هم جوان‌مردی کند تشریف فرماید مگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قامتِ چالاکِ او آمد خرامان از درم</p></div>
<div class="m2"><p>و آن قیامت را که می‌گویند بنماید مگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصدِ جانم گر ندارد بر دلم رحم آورد</p></div>
<div class="m2"><p>همّتش را سر به خونِ من فرو ناید مگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نه زر داری و نه زور ای نزاری هم چو زیر</p></div>
<div class="m2"><p>زاریی می‌کن که دانم هم ببخشاید مگر</p></div></div>