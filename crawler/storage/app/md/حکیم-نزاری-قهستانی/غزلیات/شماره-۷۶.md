---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>کاش برون آمدی یوسف ما از نقاب</p></div>
<div class="m2"><p>تا به ملامت حسود بیش نکردی خطاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی چه حدیث است نی ما که و یوسف کدام</p></div>
<div class="m2"><p>شب پره و احتمال در نظر آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیست که انگیخت باز در همه آفاق شور</p></div>
<div class="m2"><p>چیست که افتاد باز در همه خلق اضطراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مطرب داوود لحن بر سر من کن سماع</p></div>
<div class="m2"><p>ساقی عیسی نفس بر کف من نه شراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیغ چو آب از قفا زود ببیند رقیب</p></div>
<div class="m2"><p>بی هده در خون ما چند نماید شتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاسد بدبخت را با روش ما چه کار</p></div>
<div class="m2"><p>گو قصب خویشتن دور بر از ماهتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که به تلقین عقل غیبت ما میکنی</p></div>
<div class="m2"><p>جهل مرکب تو را می فکند در عذاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دام شیاطین منه بر سر کوی ملک</p></div>
<div class="m2"><p>رو به فضولی مکن خانه ی بختت خراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیغ زبان می کشی در حرم پادشاه</p></div>
<div class="m2"><p>جان بدهی عاقبت سر نبری زین طناب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طاعت جزوی و بس کوثر و باقی طمع</p></div>
<div class="m2"><p>تشنه بسی شد فرو در طلب این سراب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما اگر از شرب مِی باز نداریم دست</p></div>
<div class="m2"><p>نزد تو باشد خطا زان که ندانی صواب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نعمت دارالبقا گر تو نخواهی مخواه</p></div>
<div class="m2"><p>دولت کشف الغطا گر تو نیابی میاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر چو منی گر شراب نیست حلال ای عجب</p></div>
<div class="m2"><p>بر تو وبال است نان بر تو حرام است آب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عاشق یوسف نه ای عیب زلیخا مکن</p></div>
<div class="m2"><p>آتش غیرت مدم جان نزاری متاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نغمه ی داوود بس گوش و حدیث رقیب</p></div>
<div class="m2"><p>یوسف بنشسته پیش چشم زلیخا و خواب</p></div></div>