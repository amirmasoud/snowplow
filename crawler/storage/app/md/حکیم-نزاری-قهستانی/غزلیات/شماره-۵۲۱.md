---
title: >-
    شمارهٔ ۵۲۱
---
# شمارهٔ ۵۲۱

<div class="b" id="bn1"><div class="m1"><p>مرا که جان به دهان از فراق یار برآمد</p></div>
<div class="m2"><p>هزار بار فرو شد هزار بار برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه می کنم ز چنین روزگار بی تو دریغا</p></div>
<div class="m2"><p>که در فراق ز جان و دلم دمار برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم ز مهر و وفا رفت خانه خانه هم چون مهر</p></div>
<div class="m2"><p>وفا ندید بسی گرد هر دیار برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنار تا به میان چون برآمده ست ز چشمم</p></div>
<div class="m2"><p>که از میان تو شوری هزار بار برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گره گره شود از خونِ بسته دل ریشم</p></div>
<div class="m2"><p>نفس نفس که ز حلقم به اضطرار برآمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرو شده ست مرا خار عشق بر رگ جانم</p></div>
<div class="m2"><p>دمار از رگ جانم ز خار خار برآمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر برآمد خار از کنار یاسمن تو</p></div>
<div class="m2"><p>بدیع نیست که گل هم ز نوکِ خار برآمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلای عشق تو ما را کمند عشق به گردن</p></div>
<div class="m2"><p>برهنه کرد و به بازار روزگار برآمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه هم نزاری مسکین به بحر عشق فرو شد</p></div>
<div class="m2"><p>که چون نزاری از این دست صدهزار برآمد</p></div></div>