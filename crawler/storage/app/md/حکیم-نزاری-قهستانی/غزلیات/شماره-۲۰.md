---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>ای با جفا در ساخته با ما نمی سازی چرا</p></div>
<div class="m2"><p>روزی ، شبی ، وقتی ، دمی ، با ما نپردازی چرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با غمزگان مست گو ، صلح است ما را با شما</p></div>
<div class="m2"><p>بر زه کمان کردن که چه، وین ناوک اندازی چرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نیستی در خون من ، خصم دل مجنون من</p></div>
<div class="m2"><p>از زلف طرّاری که چه ، وز غمزه طنازی چرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آشفته چون من بلبلی بر گل ستان روی تو</p></div>
<div class="m2"><p>افتاده در بند قفس با این خوش آوازی چرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر پیش حکمت بر زمین، داریم و نامت بر نگین</p></div>
<div class="m2"><p>بر عاجزان ممتحن این گردن افرازی چرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نا کرده از شهد لبت روزی به عمدا چاشنی</p></div>
<div class="m2"><p>شب های تا روزم چو شمع از سوز بگدازی چرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با آن که داری روز و شب ، با من مصاف و عربده</p></div>
<div class="m2"><p>من صلح را در می زنم تو جنگ آغازی چرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گه انتظارم می دهی گه نا امیدم می کنی</p></div>
<div class="m2"><p>با چون نزاری والهی این بل عجب بازی چرا</p></div></div>