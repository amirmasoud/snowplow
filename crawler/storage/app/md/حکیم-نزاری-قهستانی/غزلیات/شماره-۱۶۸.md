---
title: >-
    شمارهٔ ۱۶۸
---
# شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>رمضان میرسد اینک دهم شعبان است</p></div>
<div class="m2"><p>می بیارید و بنوشید که برغندان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور بمانیم به روزی که نشاید خوردن</p></div>
<div class="m2"><p>ساقیا باده بگردان که فلک گردان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن گه از صحبت نا اهل توان رست که می</p></div>
<div class="m2"><p>آشکارا بخورندی که چه خوش دوران است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راستی مجلس با مشغله بی ترتیب</p></div>
<div class="m2"><p>گر بهشت است به نزدیک خرد زندان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باده پنهان خور و از عربده جویان بگریز</p></div>
<div class="m2"><p>گوشه ای گیر که عیشی به فراغت آن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرت از رفتن و آوردن می باری هست</p></div>
<div class="m2"><p>سهل باشد که نه دردی است که بی درمان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من به نوک مژه نقبی بزنم تا سر خم</p></div>
<div class="m2"><p>خم هم سایه که در زیر زمین پنهان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من کی از ماه قدر دست بردارم یک ماه</p></div>
<div class="m2"><p>که میان من و او صحبت جانا جان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نکند توبه نزاری و اگر نیز کند</p></div>
<div class="m2"><p>شیشة توبه که بر سنگ زنند آسان است</p></div></div>