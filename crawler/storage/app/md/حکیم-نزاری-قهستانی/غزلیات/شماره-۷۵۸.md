---
title: >-
    شمارهٔ ۷۵۸
---
# شمارهٔ ۷۵۸

<div class="b" id="bn1"><div class="m1"><p>گر برون آیی و برقع بگشایی ز جمال</p></div>
<div class="m2"><p>از تو گیرند قیامت همه خلق استدلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نهی بر رهِ اسلام ز زلفت دامی</p></div>
<div class="m2"><p>عالمی خلق در افتند چو کافر به ضلال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر فشان عطفِ عرق چین و بهل تا گیرد</p></div>
<div class="m2"><p>نفسِ روحِ خدا رایحۀ بادِ شمال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تو عشّاق یکی جان نبرند ار تو تویی</p></div>
<div class="m2"><p>خو مگر باز کند غمزۀ مستت ز قتال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رحمت آرد مگر ای دیده ی پر خون بگری</p></div>
<div class="m2"><p>چاره ای نیست دگر ای دلِ پر درد بنال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دورم از غایتِ تعجیل و مسافت نزدیک</p></div>
<div class="m2"><p>چون بود بسته دهن تشنه بر اطرافِ زلال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر شکایت کنم از دوست ادب نیست که هست</p></div>
<div class="m2"><p>همه شب در برِ من خفته و لیکن به خیال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این همه تفرقه زان است که کم تر کردیم</p></div>
<div class="m2"><p>شکرِ جمعیّتِ احباب در ایّامِ وصال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صفحه ی سیمِ ورق جدولِ تقویم شود</p></div>
<div class="m2"><p>گر در آرم به قلم شمّه ای از صورتِ حال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صبر مفتاحِ نجات است نزاری خوش باش</p></div>
<div class="m2"><p>اخترِ طالعت آخر به درآید ز زوال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا نفس را حرکت باشد و دل را قوّت</p></div>
<div class="m2"><p>درِ امید زدن را بود امکان و مجال</p></div></div>