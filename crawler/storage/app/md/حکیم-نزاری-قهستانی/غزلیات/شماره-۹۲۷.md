---
title: >-
    شمارهٔ ۹۲۷
---
# شمارهٔ ۹۲۷

<div class="b" id="bn1"><div class="m1"><p>چنان آرزومندِ دیدارِ اویم</p></div>
<div class="m2"><p>که جان می‌رسد بر لب از آرزویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه ها می‌رسد بر سر از دستِ هجر</p></div>
<div class="m2"><p>به نامحرم این قصه چون باز گویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن گه که محرومم از رویِ خوبش</p></div>
<div class="m2"><p>فرو می‌رود اشکِ حسرت به رویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبی سدره از ژاله‌ی ارغوانی</p></div>
<div class="m2"><p>به خون صفحه‌ی ارغوانی بشویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه بمردم ز هجران ولیکن</p></div>
<div class="m2"><p>عرق چین او زنده دارد به بویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو کحل الجواهر کشم در دو دیده</p></div>
<div class="m2"><p>غباری گر آرند از آن خاک کویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جز جامِ می غم گساری ندارم</p></div>
<div class="m2"><p>اگر چند سر در کش از عیب گویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من و کوزه‌ی راح گو سنگ طعنه</p></div>
<div class="m2"><p>به هم در شکن توبه‌ی چون سبویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نزاری شکیبایی از می ندارد</p></div>
<div class="m2"><p>زِ من کی شود باز دیرینه خویم</p></div></div>