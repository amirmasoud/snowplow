---
title: >-
    شمارهٔ ۷۵۱
---
# شمارهٔ ۷۵۱

<div class="b" id="bn1"><div class="m1"><p>عشق است فراخ و سینه‌ای تنگ</p></div>
<div class="m2"><p>راهی ست دراز و مرکبی لنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک خاطر و صدهزار غصّه</p></div>
<div class="m2"><p>یک منزل و صدهزار فرسنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی یادِ تو کعبه‌ها خرابات</p></div>
<div class="m2"><p>بی نامِ تو نام‌ها همه ننگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جامی به هزار بیم در پیش</p></div>
<div class="m2"><p>شاخی به هزار حیله در چنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سجّاده فتاده در بُنِ خم</p></div>
<div class="m2"><p>قرّابه شکسته بر سرِ سنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درباخته نردِ دین و دنیا</p></div>
<div class="m2"><p>چون کم زدگان نشسته دل تنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از عشق منال ای نزاری</p></div>
<div class="m2"><p>رو صلح گزین و بگذر از جنگ</p></div></div>