---
title: >-
    شمارهٔ ۸۰۰
---
# شمارهٔ ۸۰۰

<div class="b" id="bn1"><div class="m1"><p>گر یک نفس از تو می شکیبم</p></div>
<div class="m2"><p>از معتقدان مکن حسیبم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بختم به وصالِ تو بشارت</p></div>
<div class="m2"><p>می آرد و من نمی فریبم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ساخته ام به نارِ سینه</p></div>
<div class="m2"><p>چون دست نمی رسد به سیبم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون کرد جگر شبِ فراقت</p></div>
<div class="m2"><p>چون روزِ قیامت از نهیبم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر ماه مپوش طرفِ برقع</p></div>
<div class="m2"><p>خود زلف تو بس بود حجیبم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای چشمۀ آفتابِ روشن</p></div>
<div class="m2"><p>حربا صفت از تو ناشکیبم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من خاکِ توم غباربردار</p></div>
<div class="m2"><p>مگذار چو آب سر به شیبم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیزاری و آن گه از نزاری</p></div>
<div class="m2"><p>هیهات مکش بدین عتیبم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو حاکمی ار عنان بپیچی</p></div>
<div class="m2"><p>من زنده و مرده در رکیبم</p></div></div>