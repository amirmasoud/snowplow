---
title: >-
    شمارهٔ ۳۲۰
---
# شمارهٔ ۳۲۰

<div class="b" id="bn1"><div class="m1"><p>مرا به صبر نمودن ، ثبات ممکن نیست</p></div>
<div class="m2"><p>که بی تو زنده دلان را حیات ممکن نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آستان تو تسلیم گشته ایم چو خاک</p></div>
<div class="m2"><p>سفر ز کوی تو در ممکنات ممکن نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی بگشتم و بسیار خوب رو دیدم</p></div>
<div class="m2"><p>ولی به لطف تو در کاینات ممکن نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سومنات اگر از صورت تو نقش برند</p></div>
<div class="m2"><p>دگر که سجده کند پیش لات ممکن نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلاص چشم نمیدارم ار بخواهی کشت</p></div>
<div class="m2"><p>که از کمند تو کس را نجات ممکن نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز سینه آتش عشق تو باز ننشید</p></div>
<div class="m2"><p>به آب سر که به آب فرات ممکن نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو فارغی و از این جانب آرزومندی</p></div>
<div class="m2"><p>به غایتی است که آن را صفات ممکن نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلاف عهد تو میگویدم عدو که بکن</p></div>
<div class="m2"><p>فریب من به چنین ترّهات ممکن نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گمان مبر که نزاری دل از تو بردارد</p></div>
<div class="m2"><p>نه در حیات که بعد از وفات ممکن نیست</p></div></div>