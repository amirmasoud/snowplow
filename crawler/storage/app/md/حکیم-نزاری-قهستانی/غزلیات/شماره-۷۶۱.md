---
title: >-
    شمارهٔ ۷۶۱
---
# شمارهٔ ۷۶۱

<div class="b" id="bn1"><div class="m1"><p>صبح دَم دوش برآورد منادی بلبل</p></div>
<div class="m2"><p>هین که باز از تُتُقِ غنچه برون آمد گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مطربِ زهره نوا ساقیِ خورشید لقا</p></div>
<div class="m2"><p>خوش بود خاصه که بر طلعتِ گل نوشی مُل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نازکان سرخوش و برطرفِ چمن طوف کنان</p></div>
<div class="m2"><p>مرغکان مست و در افکنده به بستان غلغل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت را باش به نقد و طمع از نسیه ببُر</p></div>
<div class="m2"><p>دیرگاه است که این سیل ببرده ست آن پُل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبلِ شیفته از جرعۀ جامِ میِ شوق</p></div>
<div class="m2"><p>مست باشد نه چو قمری ز شرابِ آمل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نروی در پیِ دل تا نشوی خوار چو من</p></div>
<div class="m2"><p>هر که او در پیِ دل رفت در افتاد به کل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر به شب طوق کنم زلفِ دل آرام به روز</p></div>
<div class="m2"><p>شحنه بر گردنِ من گو به غرامت نه غل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده پیش است اگر خار گزاید ور نیش</p></div>
<div class="m2"><p>سینه کردم سپر ار تیغ زند ور تاول</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر نزاریِ نزار این همه تشنیعِ رقیب</p></div>
<div class="m2"><p>باغ بان برد گل و شیفته بر گل بلبل</p></div></div>