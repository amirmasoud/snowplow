---
title: >-
    شمارهٔ ۱۰۴۴
---
# شمارهٔ ۱۰۴۴

<div class="b" id="bn1"><div class="m1"><p>آخر ای دوست کجایی که چنانم بی‌تو</p></div>
<div class="m2"><p>که سر از پای و شب از روز ندانم بی‌تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشتباهی بنماند که تویی هستی من</p></div>
<div class="m2"><p>چون که از هستیِ خود نیست گمانم بی‌تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه همان بلبلِ دیوانۀ عشقم یارب</p></div>
<div class="m2"><p>که چنین بسته لب و گنگ‌زبانم بی‌تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مُهرِ پیمان تو بر دیده و دل بنهادم</p></div>
<div class="m2"><p>خاک در چشمِ دل و دیده فشانم بی‌تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به خدمت نرسم باز نبینم رویت</p></div>
<div class="m2"><p>کافرم گر نفسی خوش‌گذرانم بی‌تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه عجب گر تو شبی زنده گذاری بی‌من</p></div>
<div class="m2"><p>عجب آن روز که من زنده بمانم بی‌تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شفقتی کن که دلم بر سرِ پا منتظرست</p></div>
<div class="m2"><p>مرحمت کن که روان است روانم بی‌تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناتوانم چه کنم بی تو نمی‌یارم بود</p></div>
<div class="m2"><p>چند گویم نتوانم نتوانم بی‌تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم به امّیدِ تو خواهم که بماند جانم</p></div>
<div class="m2"><p>که نه از مرگ بتر صحبتِ جانم بی‌تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از صبا پرس شبی حالِ نزاریِ نزار</p></div>
<div class="m2"><p>تا نشانی دهد از نام و نشانم بی‌تو</p></div></div>