---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>هر کو نظرش به جاه و مال است</p></div>
<div class="m2"><p>مستغرق قلزم ضلال است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود را به محیط در مینداز</p></div>
<div class="m2"><p>زیرا که خلاص از او محال است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نتوان به در آمدن ز گرداب</p></div>
<div class="m2"><p>ور زان که طمع کنی خیال است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن ها که به انزوا نشستند</p></div>
<div class="m2"><p>دانی که چرا درین سوال است؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا در نکشد به بحرشان حرص</p></div>
<div class="m2"><p>موجی که علاقه ی وبال است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن نیز هم از عنایت اوست</p></div>
<div class="m2"><p>ورنه که و چه که را مجال است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسکندر جست آب و شد خاک</p></div>
<div class="m2"><p>باری بنگر چه طرفه حال است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خضر از نظر مواهب حق</p></div>
<div class="m2"><p>خوش بر لب چشمه ی زلال است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر در یابی هنوز این راز</p></div>
<div class="m2"><p>از نوع مراتب رجال است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با این همه فصحت نزاری</p></div>
<div class="m2"><p>درشرح بیان هنوز لال است</p></div></div>