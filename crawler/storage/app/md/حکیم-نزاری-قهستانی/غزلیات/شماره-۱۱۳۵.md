---
title: >-
    شمارهٔ ۱۱۳۵
---
# شمارهٔ ۱۱۳۵

<div class="b" id="bn1"><div class="m1"><p>ما را ز دهانِ تو نباتی</p></div>
<div class="m2"><p>یعنی که به بوسه‌ ای براتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکرانه ی روزگارِ خود را</p></div>
<div class="m2"><p>از گوشه ی لب بده زکاتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیرین‌تر و نغزتر نباشد</p></div>
<div class="m2"><p>در مصر ازین شکر نباتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چشمه ی آفتاب نبود</p></div>
<div class="m2"><p>چون لعل تو چشمه ی حیاتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنشین برِ ما دمی که برخاست</p></div>
<div class="m2"><p>از چشمه ی چشم ما فراتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر وعده ی انتظار وصلت</p></div>
<div class="m2"><p>تا صبر کنیم کو ثباتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای یار به چشمِ پاک‌بازان</p></div>
<div class="m2"><p>از جانب ما کن التفاتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این عشق نه بر لب و دهانست</p></div>
<div class="m2"><p>به زین بشنو زمن صفاتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مستغرقِ عشق چیست دانی</p></div>
<div class="m2"><p>ذاتی شده متحد به ذاتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خودبینی و آرزو پرستی</p></div>
<div class="m2"><p>رهیابی و پروریده لاتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بشتاب نزاریا در آن کوش</p></div>
<div class="m2"><p>کز خویشتنت بود نجاتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زین نجد به وجد بر سر آیی</p></div>
<div class="m2"><p>مغرور مشو به مسکراتی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا زنده شوی بمیر بی‌مرگ</p></div>
<div class="m2"><p>خود بر همه واجب است ماتی</p></div></div>