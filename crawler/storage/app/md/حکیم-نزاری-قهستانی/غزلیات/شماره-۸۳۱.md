---
title: >-
    شمارهٔ ۸۳۱
---
# شمارهٔ ۸۳۱

<div class="b" id="bn1"><div class="m1"><p>یک شبی تا روز با تو خلوتی می بایدم</p></div>
<div class="m2"><p>زین طرف میل است زان سو رغبتی می بایدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رازها دارم که نتوان با کسی جز با تو گفت</p></div>
<div class="m2"><p>تا به خدمت عرضه دارم فرصتی می بایدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تو می گویم که می باید مرا کی گفته ام</p></div>
<div class="m2"><p>کز جهان آسایشی یا نعمتی می بایدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر بلایِ عشق می خواهم سلامت گو مباش</p></div>
<div class="m2"><p>بر سپاهِ هجر لیکن قدرتی می بایدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وعدۀ دیدار فرمودی و پیمانی برفت</p></div>
<div class="m2"><p>لیک تا آن وقت صبر و طاقتی می بایدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر تو من باری یقینم هر چه فرمایی کنی</p></div>
<div class="m2"><p>هیچ بر قول تو گفتم حجّتی می بایدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زار می نالد نزاری بر درِ غفرانِ تو</p></div>
<div class="m2"><p>گر گنه بر من بپوشی خلعتی می بایدم</p></div></div>