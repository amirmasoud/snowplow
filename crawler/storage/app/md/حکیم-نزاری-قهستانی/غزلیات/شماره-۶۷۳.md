---
title: >-
    شمارهٔ ۶۷۳
---
# شمارهٔ ۶۷۳

<div class="b" id="bn1"><div class="m1"><p>دلا سر پوشِ خوانِ سّرِ او باش</p></div>
<div class="m2"><p>مکن تا او نگوید سرِّ او فاش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چشمِ دوست رویِ دوست می بین</p></div>
<div class="m2"><p>همه تن دیده شو بی دیده می باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چشمِ خود چه خواهی دید خود را</p></div>
<div class="m2"><p>خودی از پیشِ خود برداری ای کاش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نهان شو چون صدف در بحرِ اسرار</p></div>
<div class="m2"><p>[گهی] می پوش گوهر گاه می پاش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دستی شمعِ اِلّا الله برافروز</p></div>
<div class="m2"><p>به دیگر دست لا را دیده بخراش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درِ او می زنی از خود به در شو</p></div>
<div class="m2"><p>کسِ او باش و ایمن شو ز اوباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نزاری طاقتِ نورِ خورت نیست</p></div>
<div class="m2"><p>برو سر در گریبان کش چو خفّاش</p></div></div>