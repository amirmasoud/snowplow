---
title: >-
    شمارهٔ ۹۰۰
---
# شمارهٔ ۹۰۰

<div class="b" id="bn1"><div class="m1"><p>ما چو از بَدْوِ ازل باده‌پرست آمده‌ایم</p></div>
<div class="m2"><p>پس یقین است که مستان ز الست آمده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قول تحیون و تَموتون نه نبی فرموده‌ست</p></div>
<div class="m2"><p>مست خواهیم شدن باز که مست آمده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مستی ما نه ز خمرست بیا تا بینی</p></div>
<div class="m2"><p>نیستانیم که در عالم هست آمده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاقبت صحبت خورشید ندارد خفاش</p></div>
<div class="m2"><p>ما روانیم نه از بهر نشست آمده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طاق ابروی بتان قبله‌گه اهل دل است</p></div>
<div class="m2"><p>لاجرم از پی بت لات‌پرست آمده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نزاری نزار از هوس مهرویان</p></div>
<div class="m2"><p>ماهیانیم که در شست به شست آمده‌ایم</p></div></div>