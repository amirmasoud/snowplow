---
title: >-
    شمارهٔ ۲۵۸
---
# شمارهٔ ۲۵۸

<div class="b" id="bn1"><div class="m1"><p>صباح بر سرم آمد خیالِ طلعتِ دوست</p></div>
<div class="m2"><p>چنان نمود مثالم که خود معاینه اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال بین که مرا بر خیال می‌‌دارد</p></div>
<div class="m2"><p>من آن نی‌ام که بدانستمی خیال از دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان ز خویش برفتم که در تصرّفِ من</p></div>
<div class="m2"><p>نه عقل ماند و نه هوش و نه مغز ماند و نه پوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین میانه شنیدم که گفت با ما باش</p></div>
<div class="m2"><p>ز خویشتن به درآ آخر این چه عادت و خوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگفته‌ایم که از هر چه غیرِ ما باز آی</p></div>
<div class="m2"><p>به دستِ وسوسه دادن زمامِ دل نه نکوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جحیم وسوسهء شیطن است پس ز جحیم</p></div>
<div class="m2"><p>ببر کآخر کم‌تر عطایِ ما مینوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دفع شیطنه لا حول گوی و لا قوّه</p></div>
<div class="m2"><p>به غیر الّا بالله دگر چه راه و چه روست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نزاریا همه از بهرِ ما و با ما گوی</p></div>
<div class="m2"><p>سخن سرای که از ما نگفت بی‌هده گوست</p></div></div>