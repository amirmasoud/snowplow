---
title: >-
    شمارهٔ ۱۹۳
---
# شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>تشنیعِ خلق برمن ازین خاک ساری است</p></div>
<div class="m2"><p>وین آب دیر شد که در این جوی جاری است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما خوف راملامتِ افسرده کرده ایم</p></div>
<div class="m2"><p>آری همیشه شیوه افسرده خواری است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما بی خبر که عینِ بقا در فنایِ ماست</p></div>
<div class="m2"><p>اصل جهاد قاعده جان سپاری است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهّاد را امیدِ ثواب از عبادت است</p></div>
<div class="m2"><p>معهودِ ما به دوست نیازست و زاری است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داند که از دیارِ که می آید این صبا</p></div>
<div class="m2"><p>آن را که برمقالتِ ما استواری است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اسرار فاش می کنم و قاصرم و لیک</p></div>
<div class="m2"><p>بی طاقتی نتیجه بی اختیاری است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الحق مقصّرم که قلم را به خدمتی</p></div>
<div class="m2"><p>رخصت دهم که حاصلِ آن شرم ساری است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرگز قبول کی کند از من سخن شناس</p></div>
<div class="m2"><p>عذری که در مقابله نابه کاری است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>معذور نیستم که به تاراج می دهم</p></div>
<div class="m2"><p>گنجی که در خرابه کُنجِ نزاری است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس مدّتی نماندکه بر تو نزاریا</p></div>
<div class="m2"><p>فتوی روان شود که چو حلّاج داری است</p></div></div>