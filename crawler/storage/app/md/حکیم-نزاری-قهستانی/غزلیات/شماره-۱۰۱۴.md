---
title: >-
    شمارهٔ ۱۰۱۴
---
# شمارهٔ ۱۰۱۴

<div class="b" id="bn1"><div class="m1"><p>ای عشق تو از بدو کن در جان من</p></div>
<div class="m2"><p>چشمی فکن بر دیده ی گریه گریان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک دم قدم بهر عیادت رنجه کن</p></div>
<div class="m2"><p>شوری برآر از کلبه ی احزان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا مرده ای زنده کند احسان تو</p></div>
<div class="m2"><p>یا روضه ای دیگر شود زندان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خون جانم بی گنه رفتن که چه</p></div>
<div class="m2"><p>زنهار قصد جان مکن ای جان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدنامیی باشد که عاشق می کشی</p></div>
<div class="m2"><p>بشنو نصیحت از من ای جانان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تو نمی دارم دریغ این نیم جان</p></div>
<div class="m2"><p>زیرا که هم درد تو شد درمان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچ آن نماید با تو از من آن تویی</p></div>
<div class="m2"><p>با من همان گو هیچ دیگر زآن من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جانا به جانت کز پی جور و جفا</p></div>
<div class="m2"><p>بی موجبی بر هم مزن پیمان من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باشد که بر من عاقبت رحم آوری</p></div>
<div class="m2"><p>خشمت نگردد موجب خذلان من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر لفظت ار نام نزاری بگذرد</p></div>
<div class="m2"><p>دشواری هجران شود آسان من</p></div></div>