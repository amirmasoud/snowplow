---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>چو به ترک تاز بردی دل مستمند ما را</p></div>
<div class="m2"><p>به کمینه بندهٔ خود به از این نگر خدا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر از سر عنایت به چو من نیاز مندی</p></div>
<div class="m2"><p>نظری کنی به رحمت چه زیان کند شما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگرت به خواب بینم نکنم دگر شکایت</p></div>
<div class="m2"><p>طمع وصال کردن به چه زهره و چه یارا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو چو غنچه زیر چادر به هزار ناز خفته</p></div>
<div class="m2"><p>من منتظر همه شب مترصدم صبا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشنو که از تو هرگز به جفا ملول گردم</p></div>
<div class="m2"><p>قدمی ندارد آن کو نبرد به سر وفا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از این قضای مبرم نرهم به زندگانی</p></div>
<div class="m2"><p>چه کند کسی که گردن بنهد چو من قضا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگرم شود میسر ز کنار تو میانی</p></div>
<div class="m2"><p>به خدا که از نزاری نکنی کرانه یارا</p></div></div>