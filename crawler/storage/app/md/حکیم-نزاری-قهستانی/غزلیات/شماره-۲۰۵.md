---
title: >-
    شمارهٔ ۲۰۵
---
# شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>هر که از می توبه خواهد کرد تایب شد نخست</p></div>
<div class="m2"><p>گو بیا تا من بیاموزم بدو شرط درست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویمش چون دست گیرد قاضی شهرت به عهد</p></div>
<div class="m2"><p>هم بر آن نیت که او کرده ست نیت کن نخست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توبه چون بر دست او کردی سبک بر پای خیز</p></div>
<div class="m2"><p>توبه ی من گو مقدر بر ادای شرط تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تو پنهان می خوری من نیز پنهان می خورم</p></div>
<div class="m2"><p>عهد محکم کرده ام بر سنت قاضی نه سست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حضور جمله در جامع به آواز بلند</p></div>
<div class="m2"><p>عهده کن در گردن قاضی بدین الزام رست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون برون آیی ز مسجد گر کسی پرسد بگو</p></div>
<div class="m2"><p>کز نزاری یافتم تعلیم این بازی چست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>توبه کاران را که با ما این قدم خواهند رفت</p></div>
<div class="m2"><p>اقتدا باید به قاضی کرد و دست از توبه شست</p></div></div>