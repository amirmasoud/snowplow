---
title: >-
    شمارهٔ ۳۴۵
---
# شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>آوخ آوخ که جگرگوشه دگر بار برفت</p></div>
<div class="m2"><p>دل به جان آمد از آن روز که دلدار برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رب این بار چنان رفت که باز آید باز</p></div>
<div class="m2"><p>یا دلش سیر شد از ما و به یک بار برفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا رب از کوفتگی های ره آسایش یافت</p></div>
<div class="m2"><p>وز تن نازک خود پرورش آزار برفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفسی با که زنم وه که فروبست دمم</p></div>
<div class="m2"><p>غم دل با که خورم آه که غمخوار برفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عکس رویش به خیال از دل من غایب نیست</p></div>
<div class="m2"><p>خواب سهل است که از دیده بیدار برفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبر بیچاره بسی دیده ی لک بازنهاد</p></div>
<div class="m2"><p>آخرالامر چو درماند به ناچار برفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمر درباختم افسوس که ایام گذشت</p></div>
<div class="m2"><p>بودنی بود چه تدبیر کنم کار برفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخت بربند نزاری که سر آمد مدت</p></div>
<div class="m2"><p>بخت برگشت و دل از دست شد و یار برفت</p></div></div>