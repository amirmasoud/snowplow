---
title: >-
    شمارهٔ ۱۳۹۲
---
# شمارهٔ ۱۳۹۲

<div class="b" id="bn1"><div class="m1"><p>برفتم از برت ای دیده را چو بینایی</p></div>
<div class="m2"><p>شدم به گرد جهان هر دری و هر جایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملول گشت دلت زان سبب سفر کردم</p></div>
<div class="m2"><p>مگر ز زحمتِ من هفته ای برآسایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان ضعیف ببودم به زیر بار فراق</p></div>
<div class="m2"><p>که نیست قوّت آهم ز ناتوانایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدم به مهرِ تو همچون هلال دست نمای</p></div>
<div class="m2"><p>چرا به من رخِ چون آفتاب ننمایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عقل دل به صبوری توان فریفت ولیک</p></div>
<div class="m2"><p>که راست عقل که من شهره ام به شیدایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دریغ فصلِ گل و من ز وصلِ تو محروم</p></div>
<div class="m2"><p>تو در شرابی و من در عذاب تنهایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهی که رغبت طرب کنی در باغ</p></div>
<div class="m2"><p>چو سرو و گل سر و پای چمن بیارایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظر چو بر گل زرد افکنی ز چهره ی من</p></div>
<div class="m2"><p>قیاس گیری و بر حالِ من ببخشایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کجاست بلبل مستت که در خروش آید</p></div>
<div class="m2"><p>ز گل سِتانِ رخت چون نقاب بگشایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترا که نوش و خوشت باد باده می پیمای</p></div>
<div class="m2"><p>که جز نصیبه ی من نیست باد پیمایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا به هرزه ملامت کنند دشمن و دوست</p></div>
<div class="m2"><p>که چند شیفته کاری و خویشتن رایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلِ نزاریِ بی دل ندارد آن طاقت</p></div>
<div class="m2"><p>که در فراق کند بیش از این شکیبایی</p></div></div>