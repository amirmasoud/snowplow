---
title: >-
    شمارهٔ ۹۸۷
---
# شمارهٔ ۹۸۷

<div class="b" id="bn1"><div class="m1"><p>آخر ای ظالم خدا را یاد کن</p></div>
<div class="m2"><p>بنده را بندِ غم آزاد کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا به مکتوبی روانم تازه‌دار</p></div>
<div class="m2"><p>یا به پیغامی دلم را شاد کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه با دست این سخن در گوشِ تو</p></div>
<div class="m2"><p>از دو زلفت حلقه‌ای برباد کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخرم روزی به شیرینی بپرس</p></div>
<div class="m2"><p>وآنگهم واله‌تر از فرهاد کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تو یاری گفته بود از راهِ‌عجز</p></div>
<div class="m2"><p>چاره‌ی این کُشته‌ی بی‌داد کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته[ای] من از کجا او از کجا</p></div>
<div class="m2"><p>هر که را دردی‌ست گو فریاد کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا طمع بگسل نزاری از وصال</p></div>
<div class="m2"><p>یا دلی از آهن و فولاد کن</p></div></div>