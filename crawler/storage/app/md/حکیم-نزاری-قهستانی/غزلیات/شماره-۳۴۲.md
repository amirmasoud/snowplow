---
title: >-
    شمارهٔ ۳۴۲
---
# شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>دوش با جمعی حواری باده خوردم در بهشت</p></div>
<div class="m2"><p>مجلسی دیدم همه عیسی دم و مریم جهشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمله شیرین پاسخ و شیرین لب و شیرین دهن</p></div>
<div class="m2"><p>جمله نیکو سیرت و نیکو دل و نیکو سرشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیب و نارنج و ترنج و نرگس از پیرامنش</p></div>
<div class="m2"><p>ماه رویان صبوحی بی نگهبانان زشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چار سوی بزم هم چون جنّت از بس فر و زیب</p></div>
<div class="m2"><p>بر ارم کرده تفاخر همچو کعبه بر کنشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوی بر رخساره ی اوراق گل هر یک چنان</p></div>
<div class="m2"><p>قطره ی شبنم بود بنشسته بر اطراف کشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بزمی القصه چو فردوس برین آراسته</p></div>
<div class="m2"><p>بیش از این بر صفحه ی کاغذ نمی یارم نوشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش کردم سر که بستانم ز حوری بوسه ای</p></div>
<div class="m2"><p>دست کردم پیش تا در گردنش آرم نهشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در هراسیدم ز خواب و خواب خوش بر چشم من</p></div>
<div class="m2"><p>آتش حسرت سبک بگداخت چون در آب خشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حاضر و غایب نزاری خواب و بیداری یکیست</p></div>
<div class="m2"><p>باز می گو با حواری دوش بودم در بهشت</p></div></div>