---
title: >-
    شمارهٔ ۱۱۸۵
---
# شمارهٔ ۱۱۸۵

<div class="b" id="bn1"><div class="m1"><p>باز جهان تازه کرد قدرتِ باری</p></div>
<div class="m2"><p>باده بده بر نسیمِ بادِ بهاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گُل بُنِ بشکفته و طراوت و زینت</p></div>
<div class="m2"><p>بلبلِ شوریده و شفاعت و زاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باد چو زلفِ بنفشه کرد به شانه</p></div>
<div class="m2"><p>گل چه کند در برابر آینه‌داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بویِ خوشِ لاله در تنوره ی آتش</p></div>
<div class="m2"><p>قاعده ی مجمرست و عودِ قِماری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاصه درین وقت کز خواص تناسخ</p></div>
<div class="m2"><p>بیدق شطرنج می‌کنند سواری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشتبهی گفته شد به عادتِ شاعر</p></div>
<div class="m2"><p>مذهبِ ما را از آن طرف نشماری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچت اگر از حیات هیچ نصیبی</p></div>
<div class="m2"><p>هست که حدّ ِ دواب باز گذاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در برِ گل خفته تا سحرگه و آن گه</p></div>
<div class="m2"><p>در شَمَرِ خُم روی و غسل برآری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور نه برون آیی از مراتب حیوان</p></div>
<div class="m2"><p>پس تو به فتوای عقل کم ز حماری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جنبشی اندر نبات هست و خروجی</p></div>
<div class="m2"><p>گر تو ز گل کم‌تری به مرتبه خاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بویِ ریاحین خلد باورت ار هست</p></div>
<div class="m2"><p>نیست جز از گل‌ستانِ طبعِ نزاری</p></div></div>