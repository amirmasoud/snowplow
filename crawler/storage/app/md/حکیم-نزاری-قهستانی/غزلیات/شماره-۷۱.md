---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>خوش بود بر طلوع صبح شراب</p></div>
<div class="m2"><p>زود بشتاب ساقیا بشتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیشتر زان که آفتاب کند</p></div>
<div class="m2"><p>کلّه ی ما چو کوره ی پرتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتاب قدح کند روشن</p></div>
<div class="m2"><p>کنج تاریک ما به برق شراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوست مطواع دوست در همه حال</p></div>
<div class="m2"><p>یار هم رنگ یار در همه باب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس بدین اعتبار باید بود</p></div>
<div class="m2"><p>در جهانِ خراب مست و خراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد خشک مغزِ تر دامن</p></div>
<div class="m2"><p>که نداند ره خطا زصواب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می خورد بنگ و می نمی نوشد</p></div>
<div class="m2"><p>از سرِ آب می رود به سراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای به رای خود اقتدا کرده</p></div>
<div class="m2"><p>کی کند هم حجاب رفع حجاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خویشتن را بدان که هیچ نه ای</p></div>
<div class="m2"><p>نص من عرف نفسه دریاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو نه ای هیچ و چون نباشی تو</p></div>
<div class="m2"><p>هم بماند پس از سوال و جواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وقت با بی خودان مست خوش است</p></div>
<div class="m2"><p>ای که طوبی لهم و حسن مآب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چاره ای کن نزاریا بی خویش</p></div>
<div class="m2"><p>به در آ زین محیط بی پایاب</p></div></div>