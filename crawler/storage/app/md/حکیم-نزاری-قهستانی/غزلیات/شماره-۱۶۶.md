---
title: >-
    شمارهٔ ۱۶۶
---
# شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>دریغ نیست وجودم که در عذاب الیم است</p></div>
<div class="m2"><p>دریغ صحبت یاران و دوستان قدیم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسی مفارقت افتد میان اهل مودت</p></div>
<div class="m2"><p>ولی شماتت اعدا ز هر کنار عظیم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو در حضور بود خاطر از فراق نترسم</p></div>
<div class="m2"><p>قتیل تیغ بلا را ز تیر مرگ چه بیم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرم دو دیده بدوزند روی دوست ببینم</p></div>
<div class="m2"><p>که اندرون مصفا چو دست پاک کلیم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حجاب عقل محال است عشق را که بپوشد</p></div>
<div class="m2"><p>چنان که نافة در جیب و طبل زیر گلیم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر صفا ز گریبان یوسفم به در آید</p></div>
<div class="m2"><p>که بوی پیرهن است آن که می دمد نه نسیم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بوستان همه کس مایل تفرج و بر ما</p></div>
<div class="m2"><p>نه بوستان که جهان بی وجود دوست جحیم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مزید دولت آن مقبلی که بخت مطیعش</p></div>
<div class="m2"><p>بر آستانه صاحب دلی چو خاک مقیم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رقیب را که چنین منع می میکند ز حبیبم</p></div>
<div class="m2"><p>دگر رقیب نمیخوانمش که دیو رجیم است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرا صبور نباشم مشنّعِ متهتّک</p></div>
<div class="m2"><p>محل راز نداند که مرد عشق حلیم است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز سنگ خون بچکد گر به دوست باز نمایم</p></div>
<div class="m2"><p>که در فراق چو خون میخورم خدای علیم است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز دوستان به ملامت نه ممکن است ملالت</p></div>
<div class="m2"><p>دوای درد نزاری خلاف رای حکیم است</p></div></div>