---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>گر گذر کردی بتِ ما بر دیارِ سومنات</p></div>
<div class="m2"><p>توبه کردی بت پرست از سجده ی عزّی ولات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ اگر کشف الغطایی کردی از اطرافِ سر</p></div>
<div class="m2"><p>التفاتی هم نکردندی به چندین ترّهات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از کمالِ نفسِ انسان حاصل ست این اقتباس</p></div>
<div class="m2"><p>آنکه استدلال می گیرند از روحانیات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم را گویی از اینجا لازم آمد انعکاس</p></div>
<div class="m2"><p>بی صفت را چون توان آورد در تحتِ صفات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سوادِ زلفِ یار ما طلب کن آبِ خضر</p></div>
<div class="m2"><p>تا بدان آبِ مصفّا یابی از ظلمت نجات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش بود می بر کنارِ چشمه ی خضرِ لبش</p></div>
<div class="m2"><p>خوش نباشد بر کنارِ چشمه ی حیوانِ نبات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خراباتِ مصافات آی تا بنمایمت</p></div>
<div class="m2"><p>صد هزاران خضر بر سر چشمه ی آبِ حیات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشمه ی خضر ای پسر با توست و تو در جست و جوی</p></div>
<div class="m2"><p>چون سکندر طالب آبِ حیات از شش جهات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جاودانی زنده گشتی شاه اسکندر چو خضر</p></div>
<div class="m2"><p>گر به مردی و جهان گیری بر ستندی ز مات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بشنو از اربابِ تاویل این همه تشبیه چیست</p></div>
<div class="m2"><p>آبِ حیوان باز نتوان بافت الّا از ندات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آری آری مقتدایِ عارفان قایم بود</p></div>
<div class="m2"><p>قایمی امّا که باشد ذاتِ او قایم به ذات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر تولّا می توانی کرد اینک مقتدا</p></div>
<div class="m2"><p>پس مسلّم شو تبرّا کن ز کلّ کاینات</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون نزاری والهی مستی بباید لامحال</p></div>
<div class="m2"><p>تا برون آرد مقاماتِ چنین از مسکرات</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خاصه چون سیمرغِ جانش از نشیمن گاهِ قدس</p></div>
<div class="m2"><p>بالِ حکمت بر گشاید بگذرد از ممکنات</p></div></div>