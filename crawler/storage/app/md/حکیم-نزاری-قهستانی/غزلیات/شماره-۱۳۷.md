---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>چون جمله تویی بهانه برخاست</p></div>
<div class="m2"><p>شکل دویی از میانه برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس من ز میان برون شوم به</p></div>
<div class="m2"><p>کاین فتنه هم از دوگانه برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق آمد و در میانه بنشست</p></div>
<div class="m2"><p>فریاد ز هر کرانه برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عکسی ز جمال دوست بنمود</p></div>
<div class="m2"><p>تا شور و شر از میانه برخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از زخمهٔ زهرهٔ غزل‌خوان</p></div>
<div class="m2"><p>این زمزمه‌ عاشقانه برخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کای بی‌خبرانِ خفته بلبل</p></div>
<div class="m2"><p>از بهر گل شبانه برخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرغ از پی کسب صید کردن</p></div>
<div class="m2"><p>شب‌گیر ز آشیانه برخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس طالب دوست را بباید</p></div>
<div class="m2"><p>یک صبح مولّهانه برخاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خود عاشق صادق از سر جان</p></div>
<div class="m2"><p>در حال محققانه برخاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مشنو که مجاورست هر کو</p></div>
<div class="m2"><p>از تربت آستانه برخاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشق از تو نزاریا چو بستد</p></div>
<div class="m2"><p>یک‌باره تو را بهانه برخاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از خاطر نازکت به کلّی</p></div>
<div class="m2"><p>اندیشهٔ احمقانه برخاست</p></div></div>