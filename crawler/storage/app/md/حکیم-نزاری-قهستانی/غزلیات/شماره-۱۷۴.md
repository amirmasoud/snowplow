---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>اگر چه هر چه تو گویی صواب من آن است</p></div>
<div class="m2"><p>ولی چو دل به خطا می رود چه درمان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به روای ترکم اگر ترک جان بیاد گفت</p></div>
<div class="m2"><p>ازو دریغ ندارم که خوشتر از جان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا امید به هشیاری و صلاح نماند</p></div>
<div class="m2"><p>که خاطر از پی ترکان مست چشمان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عمر چیزی باقی نماند و ما فیها</p></div>
<div class="m2"><p>هنوز تا نفس آخرین بر آن سان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلی ندارم و جمعیتی و غم خواری</p></div>
<div class="m2"><p>عجب نباشد اگر خاطرم پریشان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زغرقه بودن من فارغی درین گرداب</p></div>
<div class="m2"><p>ترا که بر لب جویی نشسته آسان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهر صفت که کنند آفتاب گردون را</p></div>
<div class="m2"><p>رخش هنوز به خوبی ،هزار چندان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز ناف آهوی تبّت دگر مگو که خطاست</p></div>
<div class="m2"><p>هزار چین اش در زیر زلف پنهان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غرض صلاح منش نیست مصلحت بین را</p></div>
<div class="m2"><p>حسد دمار بر آرد از او غرض آن است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نزاریا به بلایی که مبتلا شده ای</p></div>
<div class="m2"><p>اگر تو شرح دهی ورنه می توان دانست</p></div></div>