---
title: >-
    شمارهٔ ۱۳۶۲
---
# شمارهٔ ۱۳۶۲

<div class="b" id="bn1"><div class="m1"><p>من ز می کی توبه کردم این چه بهتان است هی</p></div>
<div class="m2"><p>توبه و من حاش لله توبه کی کردم ز می</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقیا برخیز و آبِ معنوی در جام ریز</p></div>
<div class="m2"><p>دیگران را تن به جان زنده‌ست و ما را جان به وی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پادشاه و ملک و درویش و قناعت ما و عشق</p></div>
<div class="m2"><p>گفته‌اند آری که هم با اصل گردد کُلّ شی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون کنم از صحبتِ اهلِ دل آخر احتراز</p></div>
<div class="m2"><p>با که پیوندم کزیشان بگسلم با اهلِ غی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاش لله توبه بر من کی توان بستن به زور</p></div>
<div class="m2"><p>خیره کی هم صحبتِ آتش تواند بود نی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ظلِّ طوبا سایة خُمّ است و در خم آفتاب</p></div>
<div class="m2"><p>من ز بهر آفتاب افتاده در پایش چو فی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آفتاب از رشک خورشیدِ قدح گیرد عرَق</p></div>
<div class="m2"><p>هم‌چنان کز قطره ی شبنم عذارِ لاله خَوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از صفایِ جوهرِ می کشف می‌شد سرِّ غیب</p></div>
<div class="m2"><p>این همه آوازه در دنیا فتاد از جامِ کی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محتسب گوید مرو بی ره‌ نزاری گردِ شهر</p></div>
<div class="m2"><p>احمق بیهوده گوی این‌جا غلط کرده‌ست پی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از پی تحصیلِ می دم دم بگردم دربه در</p></div>
<div class="m2"><p>هم‌چو مجنون در هوایِ وصل لیلی حی به حی</p></div></div>