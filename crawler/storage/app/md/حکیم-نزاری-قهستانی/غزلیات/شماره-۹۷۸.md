---
title: >-
    شمارهٔ ۹۷۸
---
# شمارهٔ ۹۷۸

<div class="b" id="bn1"><div class="m1"><p>گر باز میسّر شودم رویِ تو دیدن</p></div>
<div class="m2"><p>جان پیش کشم تا به کی از جور کشیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاقت برسیده‌ست ز طوفانِ فراقم</p></div>
<div class="m2"><p>فریاد ز من وز تو به فریاد رسیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر من چه ملامت اگرم طاقتِ آن نیست</p></div>
<div class="m2"><p>کز کوی تو باید به سفر راه بریدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویند که چون یارِ تو بسیار توان یافت</p></div>
<div class="m2"><p>آری همه جا هست بباید طلبیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سهل است بریدن سرِ عشّاق به شمشیر</p></div>
<div class="m2"><p>لیکن نتوانند ز احباب بریدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رویی دگرم نیست به جز راه سپردن</p></div>
<div class="m2"><p>کاری دگرم نیست به جز دست گزیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون خوردن و جان کندن و دل‌سوخته بودن</p></div>
<div class="m2"><p>سهل است بر امیدِ ملاقات تو دیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌روی‌ تو آرام نمی‌بودم و اکنون</p></div>
<div class="m2"><p>خرسندم از آوازه‌ی نامِ تو شنیدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش باش نزاری که ز عشّاق خوش آید</p></div>
<div class="m2"><p>نالیدن و بر دل زدن و جامه دریدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون بی‌دل و بی‌هوشی اگر می نخوری به</p></div>
<div class="m2"><p>آتش به همه حال برآید ز دمیدن</p></div></div>