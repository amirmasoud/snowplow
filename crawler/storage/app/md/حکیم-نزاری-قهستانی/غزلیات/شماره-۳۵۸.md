---
title: >-
    شمارهٔ ۳۵۸
---
# شمارهٔ ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>موذّن فالق الاصباح می گفت</p></div>
<div class="m2"><p>گمان بردم که هات الراح می گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر افکندم ز خواب آن دل ستان را</p></div>
<div class="m2"><p>که می را مونس الرواح می گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر ده را مسیح وقت میخواند</p></div>
<div class="m2"><p>طلوع صبح را صبّاح می گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو گفتم چه می گوید موذّن</p></div>
<div class="m2"><p>مگر رمزی در این اصلاح می گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدح پر کن که چون کردی قدح پر</p></div>
<div class="m2"><p>نباشد حاجت مصباح می گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به لفظی دیگرش می خواند جان بخش</p></div>
<div class="m2"><p>به دیگر قوّت اشباح می گفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وجوهی نقد می باید که بگشاد</p></div>
<div class="m2"><p>در می خانه بی مفتاح می گفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی گفت از طلوع صبح باری</p></div>
<div class="m2"><p>همه در لمعه ی اقداح می گفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در افکن کشتی ای در بحر مجلس</p></div>
<div class="m2"><p>که پندارم تورا ملّاح می گفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نزاری گفت قیدی هست در راه</p></div>
<div class="m2"><p>ولی مقری هوالفتاح می گفت</p></div></div>