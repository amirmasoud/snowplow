---
title: >-
    شمارهٔ ۵۴۵
---
# شمارهٔ ۵۴۵

<div class="b" id="bn1"><div class="m1"><p>یا خود همه کس فتنهء بالای بلندند</p></div>
<div class="m2"><p>بر عادتِ من چون گران مولعِ قندند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شمعِ جهان‌سوز به رغبت نظری کن</p></div>
<div class="m2"><p>با جانبِ جمعی که بر آتش چو سپندند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خیمه برون آی و ببین منتظران را</p></div>
<div class="m2"><p>دیوانه و عاقل زچپ و راست که چندند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانند که بودن نه صلاح است و نیارند</p></div>
<div class="m2"><p>از پیش تو رفتن که گرفتار کمندند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیف است که این روی به هرکس بنمودی</p></div>
<div class="m2"><p>تا بی‌نظران نیز نظر بر تو فکندند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویی همه سحر است سراپای وجودت</p></div>
<div class="m2"><p>کز دستِ تو خلقی به سراپای به بندند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این است قیامت که بگفتند و بدیدیم</p></div>
<div class="m2"><p>گو خلق ببینند گر از ما نپسندند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یوسف چو ببینی نکنی عیبِ زلیخا</p></div>
<div class="m2"><p>ای مدّعی آخر ز سرت دیده نکندند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یاران و رفیقانِ سفر بیش مگریید</p></div>
<div class="m2"><p>بر من که بر آشفتنِ دیوانه بخندند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با خلق مگویید کنون جز سخنِ دوست</p></div>
<div class="m2"><p>گرهم چو نزاری همه کس دشمنِ پندند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای باد پیامی ببر از ما به قهستان</p></div>
<div class="m2"><p>کایشان به فلان جای گرفتارِ کمندند</p></div></div>