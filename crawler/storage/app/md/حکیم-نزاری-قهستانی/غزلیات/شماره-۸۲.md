---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>مرا چو وصل تو تشریف بار داد امشب</p></div>
<div class="m2"><p>بیار باده و گوهر هرچه باد امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمند زلف تو وجام باده هر دو به کف</p></div>
<div class="m2"><p>رقیب را چه بماند به دست ، باد امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر زمانه ببخشود بر من مسکین</p></div>
<div class="m2"><p>که هم به وصل تو داد دلم بداد امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبا کجاست که یعقوب صبح را گوید</p></div>
<div class="m2"><p>که آفتاب چو یوسف به چه فتاد امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان ز مادر فطرت در آفرینش خاص</p></div>
<div class="m2"><p>جز از برای تمنای من نزاد امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر نه روز معادست و من بهشتی چیست</p></div>
<div class="m2"><p>که آسمان در فردوس برگشاد امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو هیچ حاصلت از نقد دیّ و فردا نیست</p></div>
<div class="m2"><p>نزاریا بستان از زمانه داد امشب</p></div></div>