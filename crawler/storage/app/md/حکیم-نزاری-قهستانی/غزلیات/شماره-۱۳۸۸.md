---
title: >-
    شمارهٔ ۱۳۸۸
---
# شمارهٔ ۱۳۸۸

<div class="b" id="bn1"><div class="m1"><p>ندارم در همه شهر آشنایی</p></div>
<div class="m2"><p>که پیغامی برد از من به جایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمِ دل با که گویم محرمی کو</p></div>
<div class="m2"><p>که بتوان گفت با او ماجرایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا دیر است تا از ماه رویی</p></div>
<div class="m2"><p>تقاضا میکند در سر هوایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیم نومید از این دولت که داند</p></div>
<div class="m2"><p>هنوزم هست در خاطر رجایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوشا گر دست دادی پای بوسش</p></div>
<div class="m2"><p>ندانم تا چنین افتد قضایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سری دارم فدای خاک پایش</p></div>
<div class="m2"><p>چه برخیزد ز دست بی نوایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبی گر اتّفاق افتد که با او</p></div>
<div class="m2"><p>به روز آریم در خلوت سرایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان باشد که بر مسند نشیند</p></div>
<div class="m2"><p>به شرکت پادشاهی با گدایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه زر نه زور کاری بر نیاید</p></div>
<div class="m2"><p>به دست عاجزان چبوَد دعایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دامِ عشق تا از عشق گویند</p></div>
<div class="m2"><p>نیفتد چون نزاری مبتلایی</p></div></div>