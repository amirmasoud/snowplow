---
title: >-
    شمارهٔ ۱۳۰۵
---
# شمارهٔ ۱۳۰۵

<div class="b" id="bn1"><div class="m1"><p>بماناد آن برو بالا که رشکِ سروِ بستانی</p></div>
<div class="m2"><p>چه رشکِ سرو بستان غیرتِ خورشید تابانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سهیلی یا مهی یا مشتری یا زهره یا مهری</p></div>
<div class="m2"><p>پری یا آدمی یا حور یا بُت با چه می‌مانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رخ‌سار آیتِ لطفی به دیدار آفتِ خلقی</p></div>
<div class="m2"><p>به دندان درِ مکنونی به لب لعلِ بدخشانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رفتن رشکِ طاووسی به گفتن غیرت طوطی</p></div>
<div class="m2"><p>به معنی روحِ مغلوبی به صورت ماهِ رخشانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان در دیده چالاکی که گویی حورِ فردوسی</p></div>
<div class="m2"><p>چنانی در دلم شیرین که گویی صورتِ جانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دورت هر چه می‌بینم دلم در جوش می‌آید</p></div>
<div class="m2"><p>نمی‌دانم مگر خیر است ما را با تو روحانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم بردی و خب کردی و خود با آن نمی‌آری</p></div>
<div class="m2"><p>بیا تا بر تو بر گویم چو می‌دانم که می‌دانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا گر محرمی بودی که رازم با تو بر گفتی</p></div>
<div class="m2"><p>نبایستی به خونِ دل جگر خوردن ز حیرانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غریبم خاطرم یارا مرنجان گر خدا ترسی</p></div>
<div class="m2"><p>به نوشی حاجتم یارا روا کن گر مسلمانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بخت خود نمی‌یابم که آن دولت دهد دستم</p></div>
<div class="m2"><p>که بر پایت نهم رویی و سر از من نگردانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نزاری کاش کی هرگز نظر بر تو نیفکندی</p></div>
<div class="m2"><p>چو نازل شد قضا اکنون چه مقصود از پشیمانی</p></div></div>