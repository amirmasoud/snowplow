---
title: >-
    شمارهٔ ۱۲۱۹
---
# شمارهٔ ۱۲۱۹

<div class="b" id="bn1"><div class="m1"><p>باز دل دادم به دستِ دل بری</p></div>
<div class="m2"><p>سرو قدّی گل رخی نسرین بری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ستانی دل گشایی دل کشی</p></div>
<div class="m2"><p>دل فریبی دل ربایی دل بری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاله رخساری بنفشه گیسویی</p></div>
<div class="m2"><p>سروبالایی صنوبر منظری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش نشینی خوش زبانی خوش دلی</p></div>
<div class="m2"><p>نازنینی نازکی بازی گری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تند خویی سرکشی عاشق کشی</p></div>
<div class="m2"><p>بی غمی سنگین دلی سلطان فری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتِ خلقی عذابِ عالمی</p></div>
<div class="m2"><p>فتنه ی شهری بلایِ کشوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل به زاری از نزاری برد و نیست</p></div>
<div class="m2"><p>حاصلی در عشق بی زور و زری</p></div></div>