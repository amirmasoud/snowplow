---
title: >-
    شمارهٔ ۱۱۵۴
---
# شمارهٔ ۱۱۵۴

<div class="b" id="bn1"><div class="m1"><p>مرا رمزی عجب نازل شد از تعالیم ِ استادی</p></div>
<div class="m2"><p>که سقفِ معرفت را ساختم زان نغز بنیادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تعالی ربنا تا در چه حیرت بوده ام ز اول</p></div>
<div class="m2"><p>به خود بر هم ز خود ظلمی همی کردیم و بی دادی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شنیدی حارث مْرَّه چه دید از خویشتن بینی</p></div>
<div class="m2"><p>اگر فرمان بری کردی به لعنت در نیفتادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبی با آن همه رتبت بدان محتاج شد کایزد</p></div>
<div class="m2"><p>بدو وحیی رسانیدی و جبریلی فرستادی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس اینجا ترک ِ تقلید ِ مقلد کردن اولا تر</p></div>
<div class="m2"><p>به دانایی سپردن کو رهی روشن نشان دادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مقصد کی رسیدی سالک این ره مگر وقتی</p></div>
<div class="m2"><p>که کلی اختیار خود به دست ِ صادقی دادی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر عاقل بدانستی که مجنون را چه حالت شد</p></div>
<div class="m2"><p>نگشتی معترض بر وی سر ِ تسلیم بنهادی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به تنهایی سویِ مقصد نمی یارست ره بردن</p></div>
<div class="m2"><p>ز لیلی هم رهی بر ساخت و ز عهد ِ وفا زادی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مسیح از نطفه ی امر آمد و شد مریم آبستن</p></div>
<div class="m2"><p>و گر نه بعد از او آبستنی روحی دگر زادی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر در جانش از عشقِ حقیقی جنبشی بودی</p></div>
<div class="m2"><p>به سنگ ِ بی ستونی کی شدی مشغول فرهادی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حجابِ راهِ خسرو بود شیرین مطلقا ور نه</p></div>
<div class="m2"><p>بر آن مسکین بدان تلخی کمان ِ کینه نگشادی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر نه پس رویِّ نفسِ بی آرام می کردی</p></div>
<div class="m2"><p>حجابِ خود شدی هرکس ز پیش خود بر استادی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نزاری تن مده در عجز ازین پس نیست وقت آن</p></div>
<div class="m2"><p>که بر سازی ز کنجِ عزلتِ خود خلوت آبادی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عجب گر یوسفِ ما زود بیرون ناید از پرده</p></div>
<div class="m2"><p>که برمی خیزد از هر گوشه ای لبیک و فریادی</p></div></div>