---
title: >-
    شمارهٔ ۴۵۹
---
# شمارهٔ ۴۵۹

<div class="b" id="bn1"><div class="m1"><p>میانه بخت دلم گر کرانه‌ای گیرد</p></div>
<div class="m2"><p>بلای صحبت خوبان بهانه‌ای گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلاص یابد و پیرانه‌سر بیاساید</p></div>
<div class="m2"><p>به طبع زاویه خنب خانه‌ای گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولی چگونه رود در رکاب سلطانی</p></div>
<div class="m2"><p>که عالمی به سرِ تازیانه‌ای گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمان چو می گذرد از گذشته غم نخورد</p></div>
<div class="m2"><p>محققانه ز نو سر زمانه‌ای گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظر گهی‌ش به عین الیقین شود حاصل</p></div>
<div class="m2"><p>که از خیال مصور نشانه‌ای گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز گنج طبع به هر دم دفینه‌ای بخشد</p></div>
<div class="m2"><p>ز کنج فقر به هر دم خزانه‌ای گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بال شوق برآید به اوج سدره عشق</p></div>
<div class="m2"><p>ورای قاف خرد آشیانه‌ای گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من و مجاورت آستان میخانه</p></div>
<div class="m2"><p>مراد آن که مرید آستانه‌ای گیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غلام همت آنم که چون نزاری مست</p></div>
<div class="m2"><p>پس از دوگانه واجب سه‌گانه‌ای گیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غنیمتی شمرد نقد وقت را امروز</p></div>
<div class="m2"><p>نوید حاصل فردا فسانه‌ای گیرد</p></div></div>