---
title: >-
    شمارهٔ ۱۱۸۱
---
# شمارهٔ ۱۱۸۱

<div class="b" id="bn1"><div class="m1"><p>دو سه روز است که دیدار به ما ننمودی</p></div>
<div class="m2"><p>مرحبا شاد رسیدی و کرم فرمودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه خطا رفت چه کردیم چه گفتیم چرا</p></div>
<div class="m2"><p>سر گرانی به سرت کز چه غبارآلودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بجستی که دگر باز نجُستی ما را</p></div>
<div class="m2"><p>به چه مشغول شدی با که به خلوت بودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درِ زندانم از آن شب که تو رفتی بسته‌ست</p></div>
<div class="m2"><p>تا به امروز که بازآمدی و بگشودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کس نگفته‌ست که مرده‌ست فلان یا زنده‌ست</p></div>
<div class="m2"><p>هم عفاالله که بر بی‌کسی ام بخشودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که مقبولِ ایازست غلامی کردن</p></div>
<div class="m2"><p>پیشِ او به بود از سلطنتِ محمودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو مکن قصد نزاری که به پایت ریزد</p></div>
<div class="m2"><p>جان به دستِ خود اگر طالب این مقصودی</p></div></div>