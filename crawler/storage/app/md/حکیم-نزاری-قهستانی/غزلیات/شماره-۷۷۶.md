---
title: >-
    شمارهٔ ۷۷۶
---
# شمارهٔ ۷۷۶

<div class="b" id="bn1"><div class="m1"><p>گیتی بهشت وار شد از روزگارِ گل</p></div>
<div class="m2"><p>در باغ بشکفید رخِ چون نگارِ گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد زاغ چون عطارد در باغ سوخته</p></div>
<div class="m2"><p>تا شد پدید چهرۀ خورشیدوارِ گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل جامه چاک زد چو بشد نرگس از چمن</p></div>
<div class="m2"><p>گویی بشد ز فرقتِ نرگس قرارِ گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانَد به چنگِ ساخته اکنون نوایِ مرغ</p></div>
<div class="m2"><p>ماند به عودِ سوخته اکنون بخارِ گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر خواست دارِ باده بود طبعِ ما کنون</p></div>
<div class="m2"><p>زیرا که بلبل است کنون خواست دارِ گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خانه گر کنیم کرانه کنون رواست</p></div>
<div class="m2"><p>زیرا که جایِ ما نشود جز کنارِ گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بوستان کنیم به دیدارِ دوستان</p></div>
<div class="m2"><p>تن ها فدایِ باده و دل ها نثارِ گل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اکنون که روزگار نزاری به کام ماست</p></div>
<div class="m2"><p>نتوان گذاشت جز به طرب روزگارِ گل</p></div></div>