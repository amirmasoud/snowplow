---
title: >-
    شمارهٔ ۴۷۶
---
# شمارهٔ ۴۷۶

<div class="b" id="bn1"><div class="m1"><p>چه محنت است که در عاشقی به ما نرسد</p></div>
<div class="m2"><p>کجا رویم که صد فتنه در قفا نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به رویِ ما همه رنجی رسید درغمِ دوست</p></div>
<div class="m2"><p>مگر که راحتِ رویش به رویِ ما نرسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فراغِ دل من از آن داشتم که یک چندی</p></div>
<div class="m2"><p>که عشقِ او به سر و جانِ مبتلا نرسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طمع به وصلِ چو اویی حماقتی ست عظیم</p></div>
<div class="m2"><p>که پادشاهیِ عالم به هر گدا نرسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر ز عشق ملامت به ما رسد چه عجب</p></div>
<div class="m2"><p>بلا و سرزنشِ عاشقی کجا نرسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز هجر و وصل چه نقصان کمالِ مجنون را</p></div>
<div class="m2"><p>اگر به خلوتِ لیلی رسد یا نرسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نزاریا چو تو رفتند رهروان بسیار</p></div>
<div class="m2"><p>که یک رونده در این ره به منتها نرسد</p></div></div>