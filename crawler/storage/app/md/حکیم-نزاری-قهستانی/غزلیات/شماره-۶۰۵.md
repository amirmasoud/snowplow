---
title: >-
    شمارهٔ ۶۰۵
---
# شمارهٔ ۶۰۵

<div class="b" id="bn1"><div class="m1"><p>غمِ جانانۀ ما بر دلِ ما اولاتر</p></div>
<div class="m2"><p>دلِ دیوانۀ ما بندِ بلا اولاتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر وفایی که نه با دوست به اخلاص کنی</p></div>
<div class="m2"><p>جور لایق‌تر از آن است و جفا اولاتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر مسلمانی و گر گبر ریا واجب نیست</p></div>
<div class="m2"><p>کافری کردنِ مطلق ز ریا اولاتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کعبه جستن به ریا کافریِ پنهانی‌ست</p></div>
<div class="m2"><p>بت پرستیدن پیدا به صفا اولاتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لوحِ وسواس چه خوانی چو قلم بشکستند</p></div>
<div class="m2"><p>حکم کز دستِ قضا رفته رضا اولاتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقان را که ز اسلام و سلام آزادند</p></div>
<div class="m2"><p>روی در قبلۀ جان پشت دوتا اولاتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل با عشق مصاحب نتواند بودن</p></div>
<div class="m2"><p>پنبه از آتشِ سوزنده جدا اولاتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ذوالفقارِ کفِ حیدر به شبان لایق نیست</p></div>
<div class="m2"><p>شک نباشد که شبان هم به عصا اولاتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زاهد و صومعه ورند و خراباتِ مغان</p></div>
<div class="m2"><p>عاشق و عشق‌سزا هم به سزا اولاتر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نقدِ هر کان به مکانی دگر اولا باشد</p></div>
<div class="m2"><p>زر به مخلوق و نزاری به خدا اولاتر</p></div></div>