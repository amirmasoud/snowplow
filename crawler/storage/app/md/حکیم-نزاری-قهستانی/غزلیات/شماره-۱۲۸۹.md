---
title: >-
    شمارهٔ ۱۲۸۹
---
# شمارهٔ ۱۲۸۹

<div class="b" id="bn1"><div class="m1"><p>آوازه درافتاد که باز آمدم از می</p></div>
<div class="m2"><p>بهتان صریح است من و توبه کجا کی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیهوده مرا پند دهد واعظ مشفق</p></div>
<div class="m2"><p>او وعظ کند آری و من نشنوم از وی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر من نتوان بست به مسمسار ملامت</p></div>
<div class="m2"><p>سندان نصیحت چه حدیث است چه هی هی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کو نبود کشته به شمشیر محبت</p></div>
<div class="m2"><p>هرگز به قیامت نه همانا که شود حی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مجنون ز حی کرده برون را چه تفاوت</p></div>
<div class="m2"><p>لیلیش درون رگ جان است نه در حی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سوخته گیرد نه در افسرده دم عشق</p></div>
<div class="m2"><p>بی هوده بود روح شمال از نفس دی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در مذهب ما زنده دلان باده پرستند</p></div>
<div class="m2"><p>آن جا که همه اوست نه شی است و نه لا شی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماییم و می و مظلمه ی عشق به گردن</p></div>
<div class="m2"><p>گو حور مده ساغر و طوبی مفکن فی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای یار بیا وین حجب از پیش برانداز</p></div>
<div class="m2"><p>ما را بده از کوثر وحدت قدحی می</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اسباب طرب جمع کن و بزم بیارای</p></div>
<div class="m2"><p>اطباق سماوات چه گسترده و چه طی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رخسار ترا با ورق گل چه تناسب</p></div>
<div class="m2"><p>آن کرده ز شبنم عرق و این ز حیا خوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن در قدح ماست که می جست سکندر</p></div>
<div class="m2"><p>هر کس به سر گنج گدایان نبرد پی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در عهد تو شک نیست که تا حشر بماند</p></div>
<div class="m2"><p>بر ران سخن های نزاری اثر کی</p></div></div>