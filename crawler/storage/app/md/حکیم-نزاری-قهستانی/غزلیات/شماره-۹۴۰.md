---
title: >-
    شمارهٔ ۹۴۰
---
# شمارهٔ ۹۴۰

<div class="b" id="bn1"><div class="m1"><p>بوی بهار آمده ست و وقت گل ستان</p></div>
<div class="m2"><p>باد خزان رفت و فتنه های زمستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باغ و چمن از نسیم گل شده بویا</p></div>
<div class="m2"><p>سرو سهی سایه اوفکنده به بستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی زمین از بهار غنچه گرفته ست</p></div>
<div class="m2"><p>سوی چنار آمده ست بلبل دستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت نشاط است و خرّمی و جوانی</p></div>
<div class="m2"><p>بی خبران غافل اند خفته چو مستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام و صراحی بَرَد به سایه گل بن</p></div>
<div class="m2"><p>مجمع آزادگان و باده پرستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطرب و ساقی ز باده خرّم و سرخوش</p></div>
<div class="m2"><p>خوش نظران در کنارشان شده سستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عهد چنان کرده ام که باز نگردم</p></div>
<div class="m2"><p>باقی پیمان ما که بود شکست آن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک دم عمرم اگرچه بی تو سرآید</p></div>
<div class="m2"><p>بر دل من روز نیست آن که شب است آن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیز نزاری ز نیستی به در آ تو</p></div>
<div class="m2"><p>دست فرو کن بگیر دامن هستان</p></div></div>