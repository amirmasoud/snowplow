---
title: >-
    شمارهٔ ۱۰۷۰
---
# شمارهٔ ۱۰۷۰

<div class="b" id="bn1"><div class="m1"><p>هستم ز جام عشقت مست و خراب گشته</p></div>
<div class="m2"><p>تن تاب مهر داده دل خون ناب گشته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خون نشسته دیده جانی به لب رسیده</p></div>
<div class="m2"><p>بی عقل و هوش مانده بی خورد و خواب گشته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا آفتاب رویت شد در غمام دارم</p></div>
<div class="m2"><p>رنگ رخی ز حسرت چون ماه تاب گشته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر آب رز نبودی پیوسته مونس من</p></div>
<div class="m2"><p>بودی محیط حکمت بر من سراب گشته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رویی بدان طراوت آورده خط ظلمت</p></div>
<div class="m2"><p>فرَ همای بوده پرَ غراب گشته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر بامداد ساقی پیش آر و بر کفم نه</p></div>
<div class="m2"><p>زان شیره ای که در خم لعل مذاب گشته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می خانه ی محبت معمور باد دایم</p></div>
<div class="m2"><p>گو باش ملک دنیا دایم خراب گشته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زهدی چنان مزور از غایت ندامت</p></div>
<div class="m2"><p>در حلق پارسایان هم چون طناب گشته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آلوده دامنان را با ما چه کار باری</p></div>
<div class="m2"><p>آیین پاک بازی دیرست تا بگشته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زین پیش بود کارم چون سرو راست قامت</p></div>
<div class="m2"><p>اکنون چو زلف خوبان پر پیچ و تاب گشته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وقتی دل نزاری گرد بلا نگشتی</p></div>
<div class="m2"><p>و اکنون چو نا بکاران بر ناصواب گشته</p></div></div>