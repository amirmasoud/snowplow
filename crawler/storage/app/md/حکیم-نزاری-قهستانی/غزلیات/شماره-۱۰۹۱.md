---
title: >-
    شمارهٔ ۱۰۹۱
---
# شمارهٔ ۱۰۹۱

<div class="b" id="bn1"><div class="m1"><p>به پای عشق درافتاده‌ام دگرباره</p></div>
<div class="m2"><p>عنان به دستِ بلا داده‌ام دگرباره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درِ ملامت اگر عاقلید مردم را</p></div>
<div class="m2"><p>خبر کنید که بگشاده‌ام دگرباره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار بار چو شوریدگان منادی عشق</p></div>
<div class="m2"><p>به گرد شهر فرستاده‌ام دگرباره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رواست گر ز پسم سرزنش کنند که باز</p></div>
<div class="m2"><p>به پیش عقل دراستاده‌ام دگرباره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آب چشم نزاری خلاص نتوان یافت</p></div>
<div class="m2"><p>ز آتشی که درافتاده‌ام دگرباره</p></div></div>