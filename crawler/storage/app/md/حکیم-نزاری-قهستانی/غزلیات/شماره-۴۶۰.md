---
title: >-
    شمارهٔ ۴۶۰
---
# شمارهٔ ۴۶۰

<div class="b" id="bn1"><div class="m1"><p>آتش عشق چو در سینه شرار اندازد</p></div>
<div class="m2"><p>مرد را از زبر تخت به دار اندازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق بر هر طرف از مملکت دل که زند</p></div>
<div class="m2"><p>همچو موجی ست که دریا به کنار اندازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق آن است که گر بر سر کویش محبوب</p></div>
<div class="m2"><p>بگذرد در قدمش سر به نثار اندازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پدرم گفت که هم زخم هلاکت بخورد</p></div>
<div class="m2"><p>خویشتن هرکه چنین بر سر نار اندازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم از مدعیان باک مدار ای بابا</p></div>
<div class="m2"><p>چه توان سوخت از آتش که چنار اندازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوستان بر سر دیوار سرا بستانش</p></div>
<div class="m2"><p>باغبان را مگذارید که خار اندازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه شود گر بگذارند رقیبان حرم</p></div>
<div class="m2"><p>که نزاری نظر از دور به یار اندازد</p></div></div>