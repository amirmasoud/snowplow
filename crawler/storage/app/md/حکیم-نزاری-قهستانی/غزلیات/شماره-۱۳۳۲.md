---
title: >-
    شمارهٔ ۱۳۳۲
---
# شمارهٔ ۱۳۳۲

<div class="b" id="bn1"><div class="m1"><p>شنیده ام که تو با دوستان وفا نکنی</p></div>
<div class="m2"><p>من اعتماد ندارم که عهد می شکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شیوه دگر افتاده ای ندانم دوش</p></div>
<div class="m2"><p>چه خواب دیده ای ام روز باز در چه فنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه خوانمت به که مانی جز این نمیدانم</p></div>
<div class="m2"><p>که آفت دل و دینی بلای جان و تنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هر جفا که توانی مرا زپیش بران</p></div>
<div class="m2"><p>که از تو تلخ نباشد بدین شکر دهنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که باشد آنکه تو را بیند و ندارد دوست</p></div>
<div class="m2"><p>ولی چنان نه که من دارمت چنان که جان منی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز غیر دوست بپرداختیم خانه دل</p></div>
<div class="m2"><p>نه هم تو شاهد مایی که صاحب الوطنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به شرط آن سپر انداختیم بر سر آب</p></div>
<div class="m2"><p>که از تو باز نگردیم اگر به تیغ زنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلاص چشم ندارد چو من گرفتاری</p></div>
<div class="m2"><p>از آن کمند که در گردن فلک فکنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نزایا نه تو را گفته ام که دیده ی شوخ</p></div>
<div class="m2"><p>سرت به باد دهد عاقبت نگر نکنی</p></div></div>