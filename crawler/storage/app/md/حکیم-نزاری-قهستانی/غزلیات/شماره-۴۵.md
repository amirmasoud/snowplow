---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>وقتی ز ما یاد آمدی هر هفته یی آن ماه را</p></div>
<div class="m2"><p>اکنون ملال خاطرش بر ما ببست آن راه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی جرم غیرت می‌کند ور نیز جرمی کرده ام</p></div>
<div class="m2"><p>هم چشم دارم کز کرم بردارد آن اکراه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر می کشد عین رضا ور نیز می بخشد روا</p></div>
<div class="m2"><p>بر خون و مال بندگان حکم است و فرمان شاه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق ببودی جان بده تسلیم گرد و سر بنه</p></div>
<div class="m2"><p>دست تصرف زین قبل در جان رسد دل خواه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با اعتماد صابری با عشق کردم کافری</p></div>
<div class="m2"><p>در پیش صرصر راستی وزنی نباشد کاه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق جهان آشوب را بر هر طرف کافتد گذر</p></div>
<div class="m2"><p>بانگ شبیخون برزند غارت کند بُن گاه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر مسندِ مصرِ دلم بنشست چون یوسف وشی</p></div>
<div class="m2"><p>یعنی که بی مسند نشین رونق نباشد گاه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قدرش نداند کس چو من یا چون زلیخا عاشقی</p></div>
<div class="m2"><p>آری نباشد حاصلی از قدرِ یوسف چاه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم نزاری را مکن با زورمندان امتحان</p></div>
<div class="m2"><p>بر شاخ بالا دست‌رس کمتر بود کوتاه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اکنون ندامت می‌برد جان می‌کند خون می‌خورد</p></div>
<div class="m2"><p>یارا ندارد لاجرم از دل برآرد آه را</p></div></div>