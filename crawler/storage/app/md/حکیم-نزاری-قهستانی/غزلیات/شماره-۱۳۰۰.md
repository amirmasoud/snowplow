---
title: >-
    شمارهٔ ۱۳۰۰
---
# شمارهٔ ۱۳۰۰

<div class="b" id="bn1"><div class="m1"><p>مهر ماه آمد بیار ای ماهِ مهرافزای می</p></div>
<div class="m2"><p>از رگِ جانِ صراحی هر زمان بگشای می</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وا نگر کز دست رفتم غمزه ای بر ما گمار</p></div>
<div class="m2"><p>پیش تر کز پا درآیم بر سرم پیمای می</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سحرِ مطلق می کند طبعِ مسیح انفاسِ من</p></div>
<div class="m2"><p>هر چه در مغزِ پرآشوبم بگیرد جای می</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دسته ی گل هر که را از باغِ طبعم آرزوست</p></div>
<div class="m2"><p>بی تقاضا گو مرا تشریف می فرمای می</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر فشانند آستینِ رقص تا دامانِ ماه</p></div>
<div class="m2"><p>گر نهد در محفلِ پرهیزگاران پای می</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ببینی خویشتن را چون دراندازد به جام</p></div>
<div class="m2"><p>امتحانی کن به خورشیدِ فلک پیمای می</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیل با او بس نیاید زآن که وقتِ امتحان</p></div>
<div class="m2"><p>کودکی بر سازد از پیرِ فلک فرسای می</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جام بر کف گیر و چون خم سینه صافی کن نخست</p></div>
<div class="m2"><p>تا نماید با تو جان در جامِ خون پالای می</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جام بی می هیچ ننماید بدان کز ابتدا</p></div>
<div class="m2"><p>می نمود احوال در جامِ جهان آرای می</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر نکت سرّیست کز غیب آمده اینک ببین</p></div>
<div class="m2"><p>می نماید معجز از طبعِ سخن پیرای می</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وین نه زآن می باشد ای بابا که هر کس می خورند</p></div>
<div class="m2"><p>از نزاری باز پرس ای وای می ای وای می</p></div></div>