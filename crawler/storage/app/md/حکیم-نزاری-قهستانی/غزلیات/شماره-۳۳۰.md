---
title: >-
    شمارهٔ ۳۳۰
---
# شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>ایبها العشّاق این کار به آسانی نیست</p></div>
<div class="m2"><p>عشق کآسودگی تن طلبد جانی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میل دل را نظر خاطر جان کی باشد</p></div>
<div class="m2"><p>نفس بل هوسی چون دم رحمانی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حجّتی نیست که خاصیت روحانی چیست</p></div>
<div class="m2"><p>آن که در علّت روحانی و انسانی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوهر عشق تو کی بی غرض آمد کز عشق</p></div>
<div class="m2"><p>کلّ مقصود تو جز شهوت شیطانی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از محمّد چه زنی لاف که در باطن تو</p></div>
<div class="m2"><p>روش بوذری و سیرت سلمانی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صورت کج نتوان گفت بود سیرت راست</p></div>
<div class="m2"><p>به مسلمانی کاین راه مسلمانی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گو نه خضرست هر آن کس که بود خضراپوش</p></div>
<div class="m2"><p>رند بودن به صفت زاهد یزدانی نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آخر ای دیو صفت فرش چه می آرایی</p></div>
<div class="m2"><p>هر سلیمانی را فرّ سلیمانی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوش کم کن که گر از کبر توان سلطان بود</p></div>
<div class="m2"><p>در سر هیچ گدا نیست که سلطانی نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش احمق مبر این رمز نزاری کان جا</p></div>
<div class="m2"><p>هیچ دانستنی یی با تو چو نادانی نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سخن عشق ترا حوصله ای می باید</p></div>
<div class="m2"><p>مرغ اسرا تو در هر قفس ارزانی نیست</p></div></div>