---
title: >-
    شمارهٔ ۵۳۸
---
# شمارهٔ ۵۳۸

<div class="b" id="bn1"><div class="m1"><p>یاد یارانی که با ما عهد یاری داشتند</p></div>
<div class="m2"><p>برشکستند از وفاداری و باد انگاشتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حق حرمت حق عزت حق نان حق نمک</p></div>
<div class="m2"><p>جمله ناحق بود پنداری همه بگذاشتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود وفا در اصل گویی بود معدوم الوجود</p></div>
<div class="m2"><p>بر صحیفه چرخ این صورت مگر بگذاشتند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا مرا سرسبز می دیدند در بستان جاه</p></div>
<div class="m2"><p>چون کدو در دوستی گردن همی افراشتند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا خزان طالعم شد از نحوست برگ ریز</p></div>
<div class="m2"><p>روی همچون باد شبگیری ز من برگاشتند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی بود در استحالت استقامت لاجرم</p></div>
<div class="m2"><p>پس محقق شد که مبطل را محق پنداشتند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دانه امیدشان در خوید و خرمن بر نداد</p></div>
<div class="m2"><p>زان که با ما تخم عهد بی وفایی کاشتند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سر حسرت نزاری همچنین می گوی باز</p></div>
<div class="m2"><p>یاد یارانی که که با ما عهد یاری داشتند</p></div></div>