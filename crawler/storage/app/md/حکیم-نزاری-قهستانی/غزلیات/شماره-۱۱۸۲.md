---
title: >-
    شمارهٔ ۱۱۸۲
---
# شمارهٔ ۱۱۸۲

<div class="b" id="bn1"><div class="m1"><p>وفا نکردی و از دوستی بگردیدی</p></div>
<div class="m2"><p>صلاحِ خویش مگر شیوه ی دگر دیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمانه داد بسی روزگار هجرانم</p></div>
<div class="m2"><p>تو چون زمانه مگر چشم تیره در دیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر زمانه خلافِ منت نمود به خواب</p></div>
<div class="m2"><p>که طرفه خوابی شیرین‌تر از شکر دیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون تُرُش منشین چون به عرض گه رفتی</p></div>
<div class="m2"><p>متاعِ نازک و بازارِ تیز و تر دیدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو دست داد به من در وفا چه گفت ای دل</p></div>
<div class="m2"><p>به چشم تجربه باری قدم بگردیدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نزاریا نه ترا گفته‌ام که از خوبان</p></div>
<div class="m2"><p>وفا و عهد نبینی بگو اگر دیدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگفتمت تو نزاری نزاریا زنهار</p></div>
<div class="m2"><p>که با قوی نکنی دست در کمر دیدی</p></div></div>