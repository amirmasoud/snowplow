---
title: >-
    شمارهٔ ۸۸۷
---
# شمارهٔ ۸۸۷

<div class="b" id="bn1"><div class="m1"><p>منم آخر که چنین بی تو جهان می بینم</p></div>
<div class="m2"><p>نه خیال است همانا که چنان می بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچه در آینه ی رغبت دل می نگرم</p></div>
<div class="m2"><p>نقش سودای تو بر صورت جان می بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو مپندار که آن روی ز چشمم برود</p></div>
<div class="m2"><p>بل که در هر چه نگه می کنم آن می بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهل مطلق بود از خانه به بستان رفتن</p></div>
<div class="m2"><p>تا گلستان تو بر سرو روان می بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر از دست رقیبت که سرش کوفته باد</p></div>
<div class="m2"><p>آشکارا نتوان دید نهان می بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی تو گر روشنی از چشم نزاری برود</p></div>
<div class="m2"><p>سهل باشد که به چشم تو جهان می بینم</p></div></div>