---
title: >-
    شمارهٔ ۲۹۴
---
# شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>چون جزو نیست ای همه پس چیست</p></div>
<div class="m2"><p>گر همه اوست پس همه کس کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برو و جان بدو سپار و بمان</p></div>
<div class="m2"><p>ورنه بی او چه گونه خواهی زیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه او نیست نقدِ وقت همه</p></div>
<div class="m2"><p>انتظارِ پریرو وعدۀ دی ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالمِ آفرینشِ مخلوق</p></div>
<div class="m2"><p>منزلِ جنّ و انس و دیو و پری ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما در آن عالمیم مستغرق</p></div>
<div class="m2"><p>کز دو عالم منزّه است و بری ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واحدِ مطلق است و هر چه جزو</p></div>
<div class="m2"><p>همه دیگر منافقی و دوئی ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وَحدهُ لاشریکَ له گفتند</p></div>
<div class="m2"><p>پس موحّد چه کارۀ ثنوی ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>علّتِ علمِ تست عادتِ تو</p></div>
<div class="m2"><p>ورنه چندین حجاب و شبهت چیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بت پرستی و بت گری یارا</p></div>
<div class="m2"><p>چیست دیگر همه منی و توئی ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>توبه زاری مبین نزاری را</p></div>
<div class="m2"><p>دل و پشتش به حبِ آل قوی ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن که [او] بی حجاب می گوید</p></div>
<div class="m2"><p>اعتقادش به اهلِ بیتِ نبی ست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پادشاه است بر ممالکِ فضل</p></div>
<div class="m2"><p>زان که مملوکِ خاندانِ علی ست</p></div></div>