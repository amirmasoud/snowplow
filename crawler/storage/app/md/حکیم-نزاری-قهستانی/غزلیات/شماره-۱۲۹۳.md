---
title: >-
    شمارهٔ ۱۲۹۳
---
# شمارهٔ ۱۲۹۳

<div class="b" id="bn1"><div class="m1"><p>خوش ترست از آبِ حیوان خاکِ می</p></div>
<div class="m2"><p>زندگی زهرست بی تریاکِ می</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روحِ راحِ خُلد می آید از او</p></div>
<div class="m2"><p>راحتِ روح است بویِ پاکِ می</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیر شد تا عقلِ روشن بین به عشق</p></div>
<div class="m2"><p>خویشتن را بسته بر فتراکِ می</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مور و آن گه طاقتِ پهلویِ پیل</p></div>
<div class="m2"><p>پیل و آن گه حملۀ ناباکِ می</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقیا کف گیره یی ساز از مژه</p></div>
<div class="m2"><p>از پیِ برچیدنِ خاشاکِ می</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیوۀ مستانِ چالاک است هین</p></div>
<div class="m2"><p>بر کفِ ما نه لبالب لاکِ می</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم چو من برقّدِ او پوشد قبا</p></div>
<div class="m2"><p>هر که داند شیوۀ چالاکِ می</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آبِ خضرِ معنوی دانی که چیست</p></div>
<div class="m2"><p>ورقسم خواهی ز من حقّا که می</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور قسم باور نداری هان گواه</p></div>
<div class="m2"><p>باز پرس از شیشۀ ضحّاکِ می</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سست می کوشد که می ترسد فقیه</p></div>
<div class="m2"><p>از خمارِ سختِ انده ناکِ می</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرگرانی با گرانان می کنند</p></div>
<div class="m2"><p>بر سبک روحان مبند امساکِ می</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناطق است آری نزاری ناطق است</p></div>
<div class="m2"><p>قاصر امّا قاصر از ادراکِ می</p></div></div>