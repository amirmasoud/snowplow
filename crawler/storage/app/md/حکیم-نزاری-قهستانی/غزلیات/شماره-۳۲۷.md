---
title: >-
    شمارهٔ ۳۲۷
---
# شمارهٔ ۳۲۷

<div class="b" id="bn1"><div class="m1"><p>گر تو بر کشتن من حکم کنی باکی نیست</p></div>
<div class="m2"><p>هیچ دلبستگی از بهر چو من خاکی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور به زهرم بکشی باز لبم بر لب نه</p></div>
<div class="m2"><p>بهتر از چشمه ی نوشین تو تریاکی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا سروقدی بود به عمدا دیدم</p></div>
<div class="m2"><p>در همه شهر به بالای تو چالاکی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محترز باش ز آلایش ادناس هوا</p></div>
<div class="m2"><p>پاک رو را بتر از صحبت ناپاکی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل اگر خواست که در موکب حسن تو رود</p></div>
<div class="m2"><p>هیچ کس نیست که بر بسته به فتراکی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه که من در غم عشق تو گرفتارم و بس</p></div>
<div class="m2"><p>از تو در هیچ مکان نیست که غم ناکی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه در حسن جمال تو نزاری گفته ست</p></div>
<div class="m2"><p>بیش از آنی و ازین برترش ادراکی نیست</p></div></div>