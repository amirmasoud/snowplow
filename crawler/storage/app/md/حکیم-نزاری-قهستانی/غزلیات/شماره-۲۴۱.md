---
title: >-
    شمارهٔ ۲۴۱
---
# شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>عشقِ تو از در درآمد در میانِ جان نشست</p></div>
<div class="m2"><p>دستِ بی رحمی گشاد و زیرِ پایم کرد پست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامِ غم در داد و مستم کرد و با دیوانگی</p></div>
<div class="m2"><p>چون بود دیوانه آن گاهی که باشد نیز مست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق مردِ پخته خواهد خام بودم من مگر</p></div>
<div class="m2"><p>آتشی زد در وجودم تا ز خام و پخته رست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خودم بیگانه کرد و با خودم کرد آشنا</p></div>
<div class="m2"><p>لاجرم جستم هم از خویش و هم از بیگانه جست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی خودم کردند کلّی زان که بودم پیش ازین</p></div>
<div class="m2"><p>خود فریب و خودنمای و خودشناس و خودپرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نام وننگ وفخروعار و ترّ وخشکم پاک سوخت</p></div>
<div class="m2"><p>آز وحرص وخوب وزشت ونیک وبد برهم شکست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چه برقی سوزناک است این که نامش هست عشق</p></div>
<div class="m2"><p>هستِ هستش نیستِ نیست ونیستِ نیستش هستِ هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشقِ تو عشق است و آن گه عشق این ها نیزعشق</p></div>
<div class="m2"><p>لذّتِ شکر تواند داشتن هرگز کبست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا نزاری با تو در پیوست بگسست از جهان</p></div>
<div class="m2"><p>با که بندد عهد چون با عشقِ تو جاوید بست</p></div></div>