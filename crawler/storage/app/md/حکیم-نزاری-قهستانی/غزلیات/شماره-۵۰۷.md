---
title: >-
    شمارهٔ ۵۰۷
---
# شمارهٔ ۵۰۷

<div class="b" id="bn1"><div class="m1"><p>مشنو که مرا جز تو یاری و کسی باشد</p></div>
<div class="m2"><p>باتو ز جهان خوشتر گر خوش نفسی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر مار سر زلفت بر پای کسی پیچد</p></div>
<div class="m2"><p>طاووس بهشت اینجا همچون مگسی باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من روی وفا از تو هرگز بنگردادم</p></div>
<div class="m2"><p>سر در قدمت بازم گر دست رسی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا رای تو چون باشد یا روی تو چون بیند</p></div>
<div class="m2"><p>از وصل تو در هر سر هرجا هوسی باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوزخ چه و جنت که هیچ اند همه بی تو</p></div>
<div class="m2"><p>گر بی تو بود جنت بر ما حرسی باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تو همه تو باید نه جنت و نه طوبی</p></div>
<div class="m2"><p>بیمار خسیسی کو موقوف خسی باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ارّان چه و شروان چه بیچاره نزاری را</p></div>
<div class="m2"><p>از هر مژه بر رویش بی تو ارسی باشد</p></div></div>