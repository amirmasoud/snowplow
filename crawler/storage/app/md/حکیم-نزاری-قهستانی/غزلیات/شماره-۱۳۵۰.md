---
title: >-
    شمارهٔ ۱۳۵۰
---
# شمارهٔ ۱۳۵۰

<div class="b" id="bn1"><div class="m1"><p>در دل نشسته ای اگر از دیده می روی</p></div>
<div class="m2"><p>موقوفِ وقت نیست ملاقاتِ معنوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دوستان مسافتِ شکلی حجاب نیست</p></div>
<div class="m2"><p>ای دوست جهد کن که از آن دوستان شوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چندان که ممکن است وفا کن به حسنِ عهد</p></div>
<div class="m2"><p>زنهار تا به گفتِ بد آموز نگروی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترسم خلافِ عهد کنی و أسّف خوری</p></div>
<div class="m2"><p>هان کوش تا نصیحتِ یارانه بشنوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محمود نیست عاقبتِ ناخدای ترس</p></div>
<div class="m2"><p>گویند ز آن که کاشته ای بیش ندروی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از عقده ی وبال بر اندیش و زخمِ نیش</p></div>
<div class="m2"><p>هر چند ماه طلعت و خورشید پرتوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باطل مکن امیدِ نزاری روا مدار</p></div>
<div class="m2"><p>آزارِ دوستان که گناهی بود قوی</p></div></div>