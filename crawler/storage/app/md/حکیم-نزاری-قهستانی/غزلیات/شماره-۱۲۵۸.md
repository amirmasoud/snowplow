---
title: >-
    شمارهٔ ۱۲۵۸
---
# شمارهٔ ۱۲۵۸

<div class="b" id="bn1"><div class="m1"><p>گر یاد روزگار فراغت کند کسی</p></div>
<div class="m2"><p>چون محنت از پس است تفاوت کند بسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با نقد وقت ساخته اند اهل معرفت</p></div>
<div class="m2"><p>این جا مجال نیست که طعنی کند کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن جا که ابر دانه کند بر صدف نثار</p></div>
<div class="m2"><p>غشّی بود که لاف کرامت زند خسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درّ سخن نه لایق گوش همه کس است</p></div>
<div class="m2"><p>چه ناطقی که بیهده گوید چه اخرسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مصنوع خلق و صنعت خالق قیاس کن</p></div>
<div class="m2"><p>دیری بود مقابل بیت المقدسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق وهوس وجود و عدم این چه علت است</p></div>
<div class="m2"><p>پیدا بود نهایت حد مهوسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خاک زود پست شود گرچه اوستاد</p></div>
<div class="m2"><p>بر آسمان کشد سر طاق مقرنسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با ناسزا مباش که هم جنس گفته اند</p></div>
<div class="m2"><p>پرهیز کن ز صحبت هر نا مجنّسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در بند آن مباش نزاری که هر گروه</p></div>
<div class="m2"><p>بیرون کند سری به تعصب ز هر پسی</p></div></div>