---
title: >-
    شمارهٔ ۲۰۰
---
# شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>تو در آ خانه کیا کیست دگر خانهٔ توست</p></div>
<div class="m2"><p>من کی‌ام هیچ به جز تو همه بیگانهٔ توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه پروانه‌صفت مولع شمعیم همه</p></div>
<div class="m2"><p>سوختن کار خلیل است که پروانهٔ توست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس و ارکان طبیعت به محل مجبورند</p></div>
<div class="m2"><p>عقل خود شاهد حال است که دیوانهٔ توست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من آشفته نی‌ام محرم اسرار رجال</p></div>
<div class="m2"><p>هرکه بیرون ندهد راز تو مردانهٔ توست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانه از پنبهٔ حلاج جدا می‌کردم</p></div>
<div class="m2"><p>پود و تارش همه آن است که در شانهٔ توست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتزاری‌ست جهان آب و زمینش همسان</p></div>
<div class="m2"><p>حق و باطل همه از ریختن دانهٔ توست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما نداریم دگر با دگری پیمانی</p></div>
<div class="m2"><p>خود نزاریِّ گدا مست ز پیمانهٔ توست</p></div></div>