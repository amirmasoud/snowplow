---
title: >-
    شمارهٔ ۶۲۸
---
# شمارهٔ ۶۲۸

<div class="b" id="bn1"><div class="m1"><p>هین که به جان آمده‌ام دست گیر</p></div>
<div class="m2"><p>رحم کن و بار دگر در پذیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه کنی من نکنم اعتراض</p></div>
<div class="m2"><p>بر من اگر رفت خطایی مگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیغ ز بازوی تو و سر ز من</p></div>
<div class="m2"><p>گردن تسلیم نپیچد اسیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس که کمان گوشهٔ ابروی تو</p></div>
<div class="m2"><p>بر دلم از غمزه روان کرد تیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد تُتُق سینهٔ من جعبه‌ای</p></div>
<div class="m2"><p>جعبه نکرده‌ست کسی از حریر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر دل پر آتش من رحم کن</p></div>
<div class="m2"><p>دل چه حدیث است تنور اثیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر مژه ی چشم پر آبم ببخش</p></div>
<div class="m2"><p>چشم غلط می‌کنم ابر مَطیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناله ی زارم بشنو هم چو نی</p></div>
<div class="m2"><p>گونه ی زردم بنگر چون زریر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل ز من گشت جدا از جنون</p></div>
<div class="m2"><p>خلق ز من گشت نفور از نفیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم نظری کن به نزاری زار</p></div>
<div class="m2"><p>چند کند ناله به زاری چو زیر</p></div></div>