---
title: >-
    شمارهٔ ۳۱۸
---
# شمارهٔ ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>محنت سرای دهر چو جای مقام نیست</p></div>
<div class="m2"><p>آسایشی در او ز حلال و حرام نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی سور و ماتم و غم و شادی در این جهان</p></div>
<div class="m2"><p>شامی به صبح رفته و صبحی به شام نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ملک کائنات به خورد کسی دهند</p></div>
<div class="m2"><p>از فرط حرص و آز هنوزش تمام نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم هیچ کار بهتر اگر نیک بنگری</p></div>
<div class="m2"><p>از خدمت حریف و صراحی و جام نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سنگ لاخ حرص مران توسن غرور</p></div>
<div class="m2"><p>آهسته تر که بر سر مرکب لگام نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش منزلیست عالم دنیا ولی درو</p></div>
<div class="m2"><p>دوران زندگانی ما بر دوام نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غبنا و حسرتا که به بازوی اجتهاد</p></div>
<div class="m2"><p>کاری چنان که دست دهد بر نظام نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرجا که شاهدیست رقیبیش ناظرست</p></div>
<div class="m2"><p>مه بی محاق نبود و خور بی غمام نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مردی برون ز خود شده مجنون صفت کجاست</p></div>
<div class="m2"><p>لیلی بسی است ، عاشق بی ننگ و نام نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی رحمی هلیل حجاب وصال شد</p></div>
<div class="m2"><p>گرنه شکیب کردن مزهر به کام نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این جا وصال دوست طمع چون کند کسی</p></div>
<div class="m2"><p>کز شنعت رقیب مجال سلام نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مشرک هنوز و دعوی وحدت نزاریا</p></div>
<div class="m2"><p>انصاف ده که سوخته ای چون تو خام نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو از کجا و شیوه ی تحقیق از کجا</p></div>
<div class="m2"><p>در گل ستان مرو که سرت بی زکام نیست</p></div></div>