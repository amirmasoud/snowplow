---
title: >-
    شمارهٔ ۷۷۹
---
# شمارهٔ ۷۷۹

<div class="b" id="bn1"><div class="m1"><p>کرد آشکار بادِ بهاری نهانِ گل</p></div>
<div class="m2"><p>وز گشتِ روزگار یقین شد گمانِ گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندوه و غم نهان شد و لهو و طرب یقین</p></div>
<div class="m2"><p>تا گشت آشکار جمالِ نهانِ گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا باز شد دو دیدۀ ابر از گریستن</p></div>
<div class="m2"><p>یک دم قرار نیست ز خنده دهانِ گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون گل ز کبر و ناز نگوید همی سخن</p></div>
<div class="m2"><p>بلبل بود به وقتِ سخن ترجمانِ گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویی نسیم دعوتِ عیسی همی دهد</p></div>
<div class="m2"><p>هر باد کان برون جهد از بادبانِ گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاخِ گل آسمان شد و از ابرِ نو بهار</p></div>
<div class="m2"><p>پر مشتری و زهره شود آسمانِ گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای من غلامِ خالِ رخ ماه رویِ خویش</p></div>
<div class="m2"><p>چون نقطه ای ز مشک زده بر میانِ گل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از لعل سیم دارد و از مشکِ ناب چشم</p></div>
<div class="m2"><p>آن در میانِ یاسمن این در میانِ گل</p></div></div>