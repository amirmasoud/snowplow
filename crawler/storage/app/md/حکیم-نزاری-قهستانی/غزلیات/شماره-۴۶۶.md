---
title: >-
    شمارهٔ ۴۶۶
---
# شمارهٔ ۴۶۶

<div class="b" id="bn1"><div class="m1"><p>خرد را که می ننگ و نامش بسوزد</p></div>
<div class="m2"><p>بده تا حلال و حرامش بسوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بد گفتن ما فقیه فسرده</p></div>
<div class="m2"><p>زبان در مکش گو که کامش بسوزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدام از پی ما چه تشنیع دارد</p></div>
<div class="m2"><p>بیا گو بخور تا مدامش بسوزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر عشق در جانش اندازد آتش</p></div>
<div class="m2"><p>رکوع و سجود و قیامش بسوزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر زآهن و سنگ باشد وجودش</p></div>
<div class="m2"><p>به یک شعله خود برق جامش بسوزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از این آب آتش صفت گو دو کاسه</p></div>
<div class="m2"><p>به افسرده ده تا تمامش بسوزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلاصش دهد از خیال مصور</p></div>
<div class="m2"><p>تمنای سودای خامش بسوزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نسیمی اگر از دماغش برآید</p></div>
<div class="m2"><p>ز تف حرارت مشامش بسوزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمانه اگر بر مرادم نگردد</p></div>
<div class="m2"><p>ز دود نفس صبح و شامش بسوزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به آهی که از اندرونم برآید</p></div>
<div class="m2"><p>تر و خشک ما لا کلامش بسوزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان در وجود نزاری زن آتش</p></div>
<div class="m2"><p>که خون در عروق و مشامش بسوزد</p></div></div>