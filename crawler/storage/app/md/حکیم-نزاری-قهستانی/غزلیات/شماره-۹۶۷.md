---
title: >-
    شمارهٔ ۹۶۷
---
# شمارهٔ ۹۶۷

<div class="b" id="bn1"><div class="m1"><p>توبه کردم که دگر توبه نخواهم کردن</p></div>
<div class="m2"><p>باش گو چون رگ جان خون رزم در گردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده گر رنگ جگر دارد جان پرورم است</p></div>
<div class="m2"><p>کار من چیست جگر خوردن جان پروردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من اگر خون نخورم از رگ جان و دهنش</p></div>
<div class="m2"><p>از لب جام به جز خون چه توانم خوردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقی پیشه ی من بود وگر تازه کنم</p></div>
<div class="m2"><p>بدعتی نیست که خواهم به جهان آوردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زخم عشق است کزو مرهم جان ساخته اند</p></div>
<div class="m2"><p>پس از این زخم به صد جان نتوان آزردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>علم و فضل و خرد و عقل ندانم که به عشق</p></div>
<div class="m2"><p>هیچ درمان دگر نیست به جز بسپردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه فرمود در اثنای حدیثی که بگوی</p></div>
<div class="m2"><p>توبه کردم که دگر توبه نخواهم کردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنده هم از ره الناسُ علی دین ملوک</p></div>
<div class="m2"><p>بو که این توبه تواند به قیامت بردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند گویند نزاری دل و دین داد به درد</p></div>
<div class="m2"><p>منم و دّردی درد ار نبود می دردَن</p></div></div>