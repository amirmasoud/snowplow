---
title: >-
    شمارهٔ ۹۱۹
---
# شمارهٔ ۹۱۹

<div class="b" id="bn1"><div class="m1"><p>ما که شنگولیانِ خوش باشیم</p></div>
<div class="m2"><p>بنده‌ی شاهدانِ قلّاشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت‌پرستی نمی‌کنیم اَرنه</p></div>
<div class="m2"><p>لعبتی بی‌نظیر بتراشیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اصل معناست ور نه نقش کنیم</p></div>
<div class="m2"><p>صورتی بی‌بدل که نقّاشیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاه دیوانه‌ایم و گه عاقل</p></div>
<div class="m2"><p>گاه پندان شویم و گه فاشیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دام در خاک و مرغ در افلاک</p></div>
<div class="m2"><p>دانه‌ای بر امید می‌پاشیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلق اگر دوست‌اند وگر دشمن</p></div>
<div class="m2"><p>ما نه مردانِ صلح و پرخاشیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایمنیم از خبر که خرسندیم</p></div>
<div class="m2"><p>فارغیم از خرد که اوباشیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کارِ‌ما با نزاری افتاده‌ست</p></div>
<div class="m2"><p>چون نزاری برفت ما باشیم</p></div></div>