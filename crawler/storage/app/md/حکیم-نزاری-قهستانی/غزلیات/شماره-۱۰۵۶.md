---
title: >-
    شمارهٔ ۱۰۵۶
---
# شمارهٔ ۱۰۵۶

<div class="b" id="bn1"><div class="m1"><p>من به جان آمده ام راه سویِ جانان کو</p></div>
<div class="m2"><p>سخت است دشوار طریقی ست رهی آسان کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوره ی سینه پرِ آتشِ هجران دارم</p></div>
<div class="m2"><p>درد دیرینه ی ما را دمِ آن درمان کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وصل خود عاقبت الامر به ما بازآید</p></div>
<div class="m2"><p>مرد برخاستن قاعده ی هجران کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر محبّت چه دلیلی ست فدا کردنِ جان</p></div>
<div class="m2"><p>عید درکیشِ قدیم است ولی قربان کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کسی را به سرِ خود سر و سامانی هست</p></div>
<div class="m2"><p>سر و سامانِ منِ بی سرو سامان کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من برانداختۀ عشقم و در باخته پاک</p></div>
<div class="m2"><p>هر چه آن بودم خود هیچ نبودم آن کو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای نزاری مکن از عالمِ تسلیم غلوّ</p></div>
<div class="m2"><p>گر مسلّم شده ای مرتبۀ سلمان کو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در پریشانی اگر مجتمعی مردی مرد</p></div>
<div class="m2"><p>استقامت ز کجا خاصه درین دوران کو</p></div></div>