---
title: >-
    شمارهٔ ۱۲۵۰
---
# شمارهٔ ۱۲۵۰

<div class="b" id="bn1"><div class="m1"><p>شبی گر اتّفاق افتد که با ما خلوتی سازی</p></div>
<div class="m2"><p>حجاب از راه برگیری نقاب از رخ براندازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه دولت بیش ازین ما را ولی در عقل کی گنجد</p></div>
<div class="m2"><p>که سلطانی کند در عشق با درویش انبازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به الطافت طمع داریم در هنگام بخشایش</p></div>
<div class="m2"><p>که گر وقتی برانی بندگان را باز بنوازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهانی از تو در غوغا و تو در حسن خود معجب</p></div>
<div class="m2"><p>همه کس در تو دارد روی و تو با کس نپردازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر نازی کنی وقتی و خشمی راهِ آن داری</p></div>
<div class="m2"><p>بحمداللّه چه شاید کرد اگر بر دوستان نازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلای عشق فرتوتت که پای از سر نمی داند</p></div>
<div class="m2"><p>به دعوا بر سر آمد هم چو زلفت در سرافرازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رهی می زن دلی بر شکاری هم چنین می کن</p></div>
<div class="m2"><p>که بی صیدی نباشد هر چه بر صاحب دلان تازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه گویم ای مسلمانان ولی گر بامیان آمد</p></div>
<div class="m2"><p>بیا گو مدّعی آن جا که جان بازی کند غازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میان جمع می باید که از تو هیچ ننماید</p></div>
<div class="m2"><p>نزاری هم چنین می سوز تا چون شمع بگدازی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر اهلیّتی داری ز نااهلان تحاشی کن</p></div>
<div class="m2"><p>کمال دوستی باشد که با هم با دوستان سازی</p></div></div>