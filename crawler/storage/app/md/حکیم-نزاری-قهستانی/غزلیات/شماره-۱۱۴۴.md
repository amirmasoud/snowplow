---
title: >-
    شمارهٔ ۱۱۴۴
---
# شمارهٔ ۱۱۴۴

<div class="b" id="bn1"><div class="m1"><p>نگارا یاد می‌داری که با ما عهد پیوستی</p></div>
<div class="m2"><p>چرا پیوند ببریدی چرا سوگند بشکستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو ما را ملک دل کردی و ملک جان زدی بر هم</p></div>
<div class="m2"><p>قبای عهد بگشادی کمر برخون ما بستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان یک‌باره ببریدی و ترک دوستی کردی</p></div>
<div class="m2"><p>که مکتوبات یاران را جوابی باز نفرستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قیامت می‌کند حسنت تعالی الله زهی فتنه</p></div>
<div class="m2"><p>فغان برخاست از مردم به هر محفل که بنشستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قبول دوستی کردی وداع وایه‌ی خود کن</p></div>
<div class="m2"><p>که ممکن نیست جان بردن ازین دعوی که پیوستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پای خم نشستی سر بنه گردن مکش یارا</p></div>
<div class="m2"><p>که پیل مست را با او خطا باشد زبردستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هنوز افسرده پندارد که خواهد مست شد وقتی</p></div>
<div class="m2"><p>اگر نقدی به دست آری ازین پس نسیه نپرستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی از من نمی‌گوید به هشیاران که ای خامان</p></div>
<div class="m2"><p>نزاری از الست آورد با خود این همه مستی</p></div></div>