---
title: >-
    شمارهٔ ۸۸۹
---
# شمارهٔ ۸۸۹

<div class="b" id="bn1"><div class="m1"><p>من اگر مست خرابم چه کنم مست توام</p></div>
<div class="m2"><p>خطر از نیستیم نیست اگر هست توم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توبه خود کرده ای از روز الستم گستاخ</p></div>
<div class="m2"><p>جرعه ی جام بلی خوردم از آن مست توم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صید دیرینه ی عشقم ز پس پنجه سال</p></div>
<div class="m2"><p>نیست ممکن که رهایی بود از شست توم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نداری سر من وای من و چشم امید</p></div>
<div class="m2"><p>دست من گیر که دیرست که بر دست توم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب حیوان و من بادیه پیموده چنین</p></div>
<div class="m2"><p>کی شود سیر دل تشنه ؟؟؟ست توم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه پیوسته کند سوز غمت بر دل من</p></div>
<div class="m2"><p>من همان شیفته ی خامش پیوست توم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز به نام تو زبانم نگشاید ورنی</p></div>
<div class="m2"><p>خود تو دانی که نزاری به زبان بست توم</p></div></div>