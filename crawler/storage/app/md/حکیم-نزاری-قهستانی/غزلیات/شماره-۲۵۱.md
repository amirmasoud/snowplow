---
title: >-
    شمارهٔ ۲۵۱
---
# شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>خرابم از آن نرگسِ نیم مست</p></div>
<div class="m2"><p>تمام اختیارم برون شد ز دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توقّع مکن کز کمان‌دار تیر</p></div>
<div class="m2"><p>اعادت کند چون برون شد زشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دیرِ مغان می‌پرستیده‌ام</p></div>
<div class="m2"><p>ز بس می پرستی شدم بت پرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بت پرستی در اسلام نیست</p></div>
<div class="m2"><p>کسی را ندانم در اسلام هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که رویِ بتی دید و پوشید چشم</p></div>
<div class="m2"><p>که مکروه دید و مقابل نشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا بگذر ای خواجه از زهدِ خشک</p></div>
<div class="m2"><p>موحّد ز تردامنی باز رست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دری هست بیش از مبادیِ عقل</p></div>
<div class="m2"><p>که بر عشق بازست تا بر که بست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز تو هیچ نگشاید از خود ببُر</p></div>
<div class="m2"><p>مجرّد ز هم راهِ بد بر شکست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو افسرده خام است و عاشق بسوخت</p></div>
<div class="m2"><p>ملامت مکن بر نزاریِ مست</p></div></div>