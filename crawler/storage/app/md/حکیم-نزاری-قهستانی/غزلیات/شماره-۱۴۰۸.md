---
title: >-
    شمارهٔ ۱۴۰۸
---
# شمارهٔ ۱۴۰۸

<div class="b" id="bn1"><div class="m1"><p>ساقی ز بامداد بیاور قِنینه‌ای</p></div>
<div class="m2"><p>بردار بانگِ قهقهه از آبگینه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقدینه بهشت مهیّا نمی‌شود</p></div>
<div class="m2"><p>لطفی کنی ز ما بستانی رهینه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حالی در این معامله مصرف نمی‌شود</p></div>
<div class="m2"><p>هر چند پر جواهر دارم خزینه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر نوع و هر چنان که توانی بساز هین</p></div>
<div class="m2"><p>هان زود اضطرابِ دلم را سکینه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چیزی چنان که حاصلِ وقتی بود سبک</p></div>
<div class="m2"><p>پیوند کن قرینه حال از قرینه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغِ مراد هم شود آخر به حیله صید</p></div>
<div class="m2"><p>بر رویِ دام تعبیه می پاش چینه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو ختمِ ساقیانی در بزمِ خلدِ اُنس</p></div>
<div class="m2"><p>ما در میان حلقه مجلس نگینه‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو در مکانِ حکمِ ریاست ممکَنی</p></div>
<div class="m2"><p>ما در صفِ نِعال کم از هر کمینه‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مستان که در ولایتِ حکمِ تو می‌روند</p></div>
<div class="m2"><p>هشدار تا ز خود نخراشند سینه‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خود کی بود به نیک و بد اصحابِ وجد را</p></div>
<div class="m2"><p>با هیچکس به شنقصه بغض و کینه‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر کس بر آن که بر صفتِ گنجِ کُنجِ ما</p></div>
<div class="m2"><p>حاصل کند ز ملکِ قناعت دفینه‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز این بحر خود برون نبرد هیچ ناخدا</p></div>
<div class="m2"><p>الّا ز دست کارِ نزاری سفینه‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان است می به کالبد آخر مگوی بیش</p></div>
<div class="m2"><p>چون جان برون شود چه فلاح از گلینه‌ای</p></div></div>