---
title: >-
    شمارهٔ ۱۰۵۷
---
# شمارهٔ ۱۰۵۷

<div class="b" id="bn1"><div class="m1"><p>آخر ای راحت جان دردِ دلِ ما بشنو</p></div>
<div class="m2"><p>امشب از بهرِ خدا مرحمتی کن بمرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امشبی باش که فردا به تو تسلیم کنم</p></div>
<div class="m2"><p>جان و دل هر دو به دستِ تو نهادیم گرو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم چنین کُنجِ من آراسته می دار چو گنج</p></div>
<div class="m2"><p>هر کجا شمع بود خانه نباشد بی ضو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نخواهی برِ ما بود و بخواهی رفتن</p></div>
<div class="m2"><p>وایِ من بر تو هلاکِ منِ مسکین به دو جو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ اگر داشته بودی خبر از عالمِ عشق</p></div>
<div class="m2"><p>قصدِ جان دادن فرهاد نکردی خسرو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برفکن برقع و بنمای رخ از مشرقِ بام</p></div>
<div class="m2"><p>تا مهِ نو سوی مغرب سرِ خود گیرد و دو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داغ حسرت حبشی وار فتد بر رخِ شان</p></div>
<div class="m2"><p>بر ختن گر فتد از عکسِ خیالت پرتو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر هوایِ سرِ کویِ تو فدا کردم جان</p></div>
<div class="m2"><p>به دو جو بر من اگر معتقدم هیچ به جو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قدحی ده به من و زنده ی جاویدم کن</p></div>
<div class="m2"><p>معنی چشمه ی حیوان چه بود زاده ی مو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گو درآیند به سرپنجه ی من بدگویان</p></div>
<div class="m2"><p>که نزاری ننهد پای به سستی درگو</p></div></div>