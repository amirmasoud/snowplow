---
title: >-
    شمارهٔ ۶۸۰
---
# شمارهٔ ۶۸۰

<div class="b" id="bn1"><div class="m1"><p>چو صرف شد همه اوقاتِ عمر در طلبش</p></div>
<div class="m2"><p>نه ممکن است که دل باز گردد از عقبش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو خضر خاصیتِ آبِ زندگی یابم</p></div>
<div class="m2"><p>اگر چنان که به عمدا لبم رسد به لبش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم نه بر پیِ آن می رود به تاریکی</p></div>
<div class="m2"><p>که هست چشمه درونِ دو زلفِ بُل عجبش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مقررّست که از زلفِ او برون ناید</p></div>
<div class="m2"><p>ز کنجِ سینه برون کرده ای بدین سببش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و گر چنان که به جایی دگر کند میلی</p></div>
<div class="m2"><p>خیالِ دوست به تلقینِ من کند ادبش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وگر شود ز خواصِ سواد سودایی</p></div>
<div class="m2"><p>طبیبِ عشق کند دفعِ احتراقِ تبش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برون از آن حَرَسِ خود کجا تواند شد</p></div>
<div class="m2"><p>که بسته اند به زنجیرِ زلف روز و شبش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گواه باش نزاری که دل دلِ من نیست</p></div>
<div class="m2"><p>درست نیست به من هیچ نسبت و حسبش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز ناشکیبی و بی طاقتی که بود دلم</p></div>
<div class="m2"><p>کسان به طعنه نهادند هر دری لقبش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرو نهادم و با عشق بر بساط نشست</p></div>
<div class="m2"><p>ببرده اند ز من هم در اوّلین نَدَبش</p></div></div>