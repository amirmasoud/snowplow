---
title: >-
    شمارهٔ ۸۸۳
---
# شمارهٔ ۸۸۳

<div class="b" id="bn1"><div class="m1"><p>هلاک می شوم از لعبت شکر دهنم</p></div>
<div class="m2"><p>نکرده چشمه ی خضر لب تو تر دهنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کس شکایت وصلت نمی توانم برد</p></div>
<div class="m2"><p>که دست مهر نهاده ست یار بر دهنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همین که نار دلم بر دهان رسید که آه</p></div>
<div class="m2"><p>ز جور دوست خیالت شکست در دهنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محبت تو چنان بر کشد زبانه ی شوق</p></div>
<div class="m2"><p>ز اندرون که همی سوزد از شرر دهنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آب روی از آن تازه روی می دارم</p></div>
<div class="m2"><p>که خشک می شود از آتش جگر دهنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فتاد بر لب پر خنده ی توی دی نظرم</p></div>
<div class="m2"><p>هنوز باز بمانده ست از آن نظر دهنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار گونه سخن ها که با تو دارم نیست</p></div>
<div class="m2"><p>به اتفاق ملاقات کارگر دهنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مکن ز فقر ذلیلم که مرد سایل حق</p></div>
<div class="m2"><p>کند چو گوش تو روزی پر از گهر دهنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز دست هجر تو خوردم هزار شربت تلخ</p></div>
<div class="m2"><p>لبت به بوسه شیرین کند مگر دهنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عجب که بی مزه باشد سخن نزاری را</p></div>
<div class="m2"><p>که از هلاهل زهر است بی خبر دهنم</p></div></div>