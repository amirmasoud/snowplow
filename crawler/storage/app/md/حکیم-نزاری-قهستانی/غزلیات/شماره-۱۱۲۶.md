---
title: >-
    شمارهٔ ۱۱۲۶
---
# شمارهٔ ۱۱۲۶

<div class="b" id="bn1"><div class="m1"><p>مردمان گویند از می توبه تا کی کرده‌ای</p></div>
<div class="m2"><p>باز لعبت بازی دیگر برون آورده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با که بر هم بسته‌ای این نقش و این شَین از کجاست</p></div>
<div class="m2"><p>نذر تا کی کرده‌ای سوگند تا کی خورده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه مقصّروار در بحث مراتب مانده‌ای</p></div>
<div class="m2"><p>گه علم چون غالیان بر بامِ عالم برده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاه در دعوت سخن با آسمان پیوسته‌ای</p></div>
<div class="m2"><p>گاه در وحدت بساطِ لامکان گسترده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در تن‌آسانی دل و دست از دو عالم شسته‌ای</p></div>
<div class="m2"><p>در مسلمانی خدا و خلق را آزرده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در فضولی هر چه تضییقی کنی دَه حرفتی</p></div>
<div class="m2"><p>در دورویی هر چه تخفیفی کنی صد مرده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خشک و تر در خرمنِ لاغیر بر هم سوختی</p></div>
<div class="m2"><p>در میان آتش کثرت هنوز افسرده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این همه هستم که می‌گویید و زین‌ها بیش‌تر</p></div>
<div class="m2"><p>چون کنم پروردگارا گرچنان پرورده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا نزاری از کجا پیدا کند خطِ جواز</p></div>
<div class="m2"><p>گر ز لوح حرفتش حرفِ نعم بسترده‌ای</p></div></div>