---
title: >-
    شمارهٔ ۳۵۴
---
# شمارهٔ ۳۵۴

<div class="b" id="bn1"><div class="m1"><p>ما دگر بوی کسی برنتوانیم گرفت</p></div>
<div class="m2"><p>هر زمان شیوه ی دیگر نتوانیم گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در وفای تو اگر سر برود از سر دست</p></div>
<div class="m2"><p>شرط عهدی دگر از سرنتوانیم گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ز هر گوشه هلیلی به درآید ماییم</p></div>
<div class="m2"><p>که چو از هر کم مزهر نتوانیم گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتابی تو و ما سایه از آن در عقبیم</p></div>
<div class="m2"><p>که قدم از قدمت برنتوانیم گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما ضعیفیم و سبک دل تو گرانی مشمر</p></div>
<div class="m2"><p>گر میانت چو کمر در نتوانیم گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار چون لشکری افتاد مسافر گشتیم</p></div>
<div class="m2"><p>ترک دنباله ی لشکر نتوانیم گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی در روی خیال تو کنم اولاتر</p></div>
<div class="m2"><p>که ازین روی نکوتر نتوانیم گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنگ رخ ساره چنین زرد از آن می داریم</p></div>
<div class="m2"><p>که بر سیم تو بی زر نتوانیم گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به گریبان هوای دل خود در زده چنگ</p></div>
<div class="m2"><p>دامن دوست عجب گر نتوانیم گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تکیه بر صبر نزاری نتوان کرد ای دل</p></div>
<div class="m2"><p>نه که آرام بر آذر نتوانیم گرفت</p></div></div>