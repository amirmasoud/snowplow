---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>هان کجایی که ز هجرانت قیامت برخاست</p></div>
<div class="m2"><p>فتنه بنشان که دگر باره ملامت برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی تو برخاست قیامت ز وجودم آری</p></div>
<div class="m2"><p>هر کجا عشقِ تو بنشست قیامت برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم آن عاشقِ بیچاره مسکین که مرا</p></div>
<div class="m2"><p>حاصلِ عمر ز عشقِ تو ندامت بر خاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم از کویِ تو برخیزم و کنجی گیرم</p></div>
<div class="m2"><p>دل چو بشنید به تشنیع و غرامت برخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دو ممکن نبود عشق و سلامت که شنید</p></div>
<div class="m2"><p>کز درِ عشق فلانی به سلامت برخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل جزوی به نصیحت چه کنم کز پیشم</p></div>
<div class="m2"><p>هر چه جز وی بُد و کلّی به تمامت برخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من که هرگز به ملامت نشوم با تو دگر</p></div>
<div class="m2"><p>خود گرفتم همه عالم به ملامت برخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قامتم پست چرا شد پس اگر در رهِ عشق</p></div>
<div class="m2"><p>سرو را از سرِ آزادی قامت برخاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به کرم یک نفسی پیشِ نزاری بنشین</p></div>
<div class="m2"><p>که نه آخر زجهان رسم کرامت برخاست</p></div></div>