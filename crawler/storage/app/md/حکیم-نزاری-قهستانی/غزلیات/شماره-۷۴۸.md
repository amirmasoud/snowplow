---
title: >-
    شمارهٔ ۷۴۸
---
# شمارهٔ ۷۴۸

<div class="b" id="bn1"><div class="m1"><p>اگر تو جهد کنی با تو می رود توفیق</p></div>
<div class="m2"><p>رفیقِ راه تو توفیق به علی التحقیق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر مشاهده خواهی برو مجاهده کش</p></div>
<div class="m2"><p>به پایِ جهد میسّر شده ست قطعِ طریق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سفینه واسطه ی مَخلص است بی ملاّح</p></div>
<div class="m2"><p>گمان مبر که توان شد برون ز بحرِ عمیق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به مهلکات در افتاده جهد باید کرد</p></div>
<div class="m2"><p>به کوششی که برآید ز دست و پایِ غریق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن فراخ روی در سخت که پنهان است</p></div>
<div class="m2"><p>رموزِ مردان در ضمنِ نکته هایِ دقیق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیال و وهم و قیاس از دماغ بیرون کن</p></div>
<div class="m2"><p>که استقامتِ تو نیست جز درین تفریق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدام باش طلبگارِ وقت بی تأخیر</p></div>
<div class="m2"><p>قیام کن به مهمّاتِ خیر بی تعویق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز راح دست مکش روح را صفایی ده</p></div>
<div class="m2"><p>صفایِ روح نباشد مگر به راحِ رحیق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سِیه کنند سفید اهلِ روزگار و مرا</p></div>
<div class="m2"><p>دقیقه ایست ز من بشنو ای رفیقِ شفیق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خضاب کاریِ من به که هر سپیده دمی</p></div>
<div class="m2"><p>رخِ زریرِ نزاری به مَی کنم چو عقیق</p></div></div>