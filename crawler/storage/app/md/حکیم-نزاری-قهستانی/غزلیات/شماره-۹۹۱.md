---
title: >-
    شمارهٔ ۹۹۱
---
# شمارهٔ ۹۹۱

<div class="b" id="bn1"><div class="m1"><p>ای عشق تو در جان من از بدو کُن</p></div>
<div class="m2"><p>جانا چنین بیگانگی با ما مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما از الست آورده‌ایم این اتصال</p></div>
<div class="m2"><p>اینجاست عشق تازه و عهد کُهُن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ما اگر چه زلّتی صادر شود</p></div>
<div class="m2"><p>تو بر مکن این اتصال از بیخ و بُن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خون مسکینان مرو بی موجبی</p></div>
<div class="m2"><p>بشنو خدا را از نزاری این سُخُن</p></div></div>