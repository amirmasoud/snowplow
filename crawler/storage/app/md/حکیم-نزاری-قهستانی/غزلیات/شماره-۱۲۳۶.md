---
title: >-
    شمارهٔ ۱۲۳۶
---
# شمارهٔ ۱۲۳۶

<div class="b" id="bn1"><div class="m1"><p>خوش است دردِ جداییّ و داغِ مهجوری</p></div>
<div class="m2"><p>اگر وصال میسّر شود پس از دوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوادِ ملکِ وجودم خراب کرد فراق</p></div>
<div class="m2"><p>خراب‌کرده عشق و امیدِ معموری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان امید که روزی به گوشِ دوست رسد</p></div>
<div class="m2"><p>به نظم آورم این عِقدهای منشوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دیده دانه دُر نظم می‌کنم همه شب</p></div>
<div class="m2"><p>به روشناییِ دل در شبانِ دیجوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معاف دار نگار اگر ضرورتِ حال</p></div>
<div class="m2"><p>شکایتی دو سه اِنها کنم به دستوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلامکی نفرستی پیامکی ندهی</p></div>
<div class="m2"><p>نه شرطِ عهدِ قدیم است اگر چه معذوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرت به همچو منی ار فرو نمی‌آید</p></div>
<div class="m2"><p>غریب نیست که در حسن خویش مغروری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شبی ز جام وصال تو گر شدم سرمست</p></div>
<div class="m2"><p>قیامتا که بدیدم ز روزِ مخموری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس از مجاهده انتظار خوش باشد</p></div>
<div class="m2"><p>اگر خلاص به صحّت بود ز رنجوری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نزاریا ز عذابِ فراق بیش منال</p></div>
<div class="m2"><p>که در مقابل نیش است نوشِ زنبوری</p></div></div>