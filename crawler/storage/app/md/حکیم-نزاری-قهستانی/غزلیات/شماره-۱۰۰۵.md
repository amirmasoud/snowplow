---
title: >-
    شمارهٔ ۱۰۰۵
---
# شمارهٔ ۱۰۰۵

<div class="b" id="bn1"><div class="m1"><p>یا رب آزاد کن مرا از من</p></div>
<div class="m2"><p>برهانم ز دستِ آهرمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرحی از شمامۀ رضوان</p></div>
<div class="m2"><p>فرجی از زمانۀ ریمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا شوم ایمن از خطا و زَلَل</p></div>
<div class="m2"><p>تا شوم فارغ از زمین و زمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکه پرسد ز واحد القهَار</p></div>
<div class="m2"><p>هم تو او را جواب ده که به من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیش زین ره مده که برخیزد</p></div>
<div class="m2"><p>این همه ما و من زما و زمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نظری کن که بشکفد بی‌خار</p></div>
<div class="m2"><p>گلِ امّیدِ ما ز طرفِ چمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیره‌زن می‌برد کلاوه و هست</p></div>
<div class="m2"><p>قدرِ یوسف برون ز حدِّ یمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما سبک‌بار و او گران‌کاوین</p></div>
<div class="m2"><p>ما روان‌ بی‌پناه و او ذوالمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست‌گیرا به فضلِ خویش بپوش</p></div>
<div class="m2"><p>بر سرِ سیِّئاتِ ما دامن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خودش خوان ز خود نزاری را</p></div>
<div class="m2"><p>بیش از ایینش مدار با دشمن</p></div></div>