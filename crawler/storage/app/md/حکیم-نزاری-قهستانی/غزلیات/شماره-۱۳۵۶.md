---
title: >-
    شمارهٔ ۱۳۵۶
---
# شمارهٔ ۱۳۵۶

<div class="b" id="bn1"><div class="m1"><p>بر من بگذشت دی پگاهی</p></div>
<div class="m2"><p>سروی و فرازِ سرو ماهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیبا بر و گردن و نغوله</p></div>
<div class="m2"><p>آراسته کاکل و کلاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آشوبِ دلی هلاکِ جانی</p></div>
<div class="m2"><p>بی چاره کنی ، ستیزه خواهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم که به بندگانِ مملوک</p></div>
<div class="m2"><p>از کبر نمی کنی نگاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتا که به جانبِ گدایان</p></div>
<div class="m2"><p>کم تر کند التفات شاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم که اگرچه راستی را</p></div>
<div class="m2"><p>در حسن تو نیست اشتباهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باشد که به دست مفلس افتد</p></div>
<div class="m2"><p>ناگه گهری ز جایگاهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتا که نه یوسفی بیابی</p></div>
<div class="m2"><p>هر جا که طلب کنی ز چاهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم که بترس اگر خداوند</p></div>
<div class="m2"><p>بخشید ترا جمال و جاهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کز آتشِ اندرون بسوزم</p></div>
<div class="m2"><p>خرمن گهِ آسمان به آهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت ار همه آسمان بسوزی</p></div>
<div class="m2"><p>بر من به جوی و جو به کاهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر اهلِ حقیقتی نزاری</p></div>
<div class="m2"><p>ساکن بنشین به خانقاهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یک رنگ شو از برایِ سالوس</p></div>
<div class="m2"><p>گه سبز مپوش و گه سیاهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مردان به غرض طمع نکردند</p></div>
<div class="m2"><p>رو توبه کن از چنین گناهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تسلیم کسی بباش و بر شو</p></div>
<div class="m2"><p>از گوشه ی گلخنی به گاهی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون کرد به راستی اقامت</p></div>
<div class="m2"><p>بر محضرِ من چنین گواهی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غیرت به وجودِ من درافتاد</p></div>
<div class="m2"><p>چون آتشِ تیز در گیاهی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتم به کسی نمای راهم</p></div>
<div class="m2"><p>کو را به خدای هست راهی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرموز به شیخ کرد اشارت</p></div>
<div class="m2"><p>کالّا درِ او دگر پناهی</p></div></div>