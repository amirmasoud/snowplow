---
title: >-
    شمارهٔ ۸۶۰
---
# شمارهٔ ۸۶۰

<div class="b" id="bn1"><div class="m1"><p>چو لطف کردی و برداشتی به اعزازم</p></div>
<div class="m2"><p>قبول کرده‌ای ای دوست رد مکن بازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن ز یار به آزار ترکِ دلداری</p></div>
<div class="m2"><p>چو برگرفتی و بنواختی می اندازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان ضعیف شدم در فراقِ تو که به جهد</p></div>
<div class="m2"><p>ز اندرون به دهان برنیاد آوازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان ز شوقِ تو مستغرقم که از حیرت</p></div>
<div class="m2"><p>دمی ز فکرِ تو با خویشتن نپردازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر موافق رایِ تو نیست می خوردن</p></div>
<div class="m2"><p>که مست می شوم و فاش می شود رازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه می کنم نخورم ترکِ دُردِ می کردم</p></div>
<div class="m2"><p>همان به است که با دردِ خویشتن سازم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نزاریِ توام آخر اگر چه هیچ نی‌ام</p></div>
<div class="m2"><p>ولی به قهر مرانم به لطف بنوازم</p></div></div>