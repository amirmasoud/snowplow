---
title: >-
    شمارهٔ ۱۱۷۵
---
# شمارهٔ ۱۱۷۵

<div class="b" id="bn1"><div class="m1"><p>دریغا گر شبی روزی تو را پروای ما بودی</p></div>
<div class="m2"><p>وگر خود یک زمان بودی گدایی پادشا بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر دانستمی کز تو توانستی به سر بردن</p></div>
<div class="m2"><p>به دست آوردمی یاری اگر از هر کجا بودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولیکن تو نه آن یاری که باشد کس به جای تو</p></div>
<div class="m2"><p>و گر بودی چو عقل از عشق و کفر از دین جدا بودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو از مبدای کون اضداد را از هم جدا کردن</p></div>
<div class="m2"><p>مقرر شد وگر نه این و آن روی و ریا بودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر بی درد درمانی میسر می شدی هرگز</p></div>
<div class="m2"><p>عذاب آباد دنیا سر به سر دارالشفا بودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دست کیست کاری یا که می داند به خود چیزی</p></div>
<div class="m2"><p>و گر بودی به حق بر یک دگر فرقی که را بودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو فرعونش بیفکندی به دست قهر در اسفل</p></div>
<div class="m2"><p>گرش هم چون کلیم الله عصایی اژدها بودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو روح الله گرش بودی دلی روشن دمی گیرا</p></div>
<div class="m2"><p>مقاماتش ز تحت الارض بر اوج سما بودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جای ارتقای خویش صاحب وهم خود را بین</p></div>
<div class="m2"><p>نیازی گر چو آه من روان بودی روا بودی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نزاری عاقبت روزی از این غرقاب بگذشتی</p></div>
<div class="m2"><p>اگر خوی تو با بیگانگان شهر آشنا بودی</p></div></div>