---
title: >-
    شمارهٔ ۱۱۷۷
---
# شمارهٔ ۱۱۷۷

<div class="b" id="bn1"><div class="m1"><p>گر آتشی چو من در میان جان بودی</p></div>
<div class="m2"><p>ز اندرون تو وقتی برآمدی دودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نصیحتی که مرا می کنی به دیده قبول</p></div>
<div class="m2"><p>بکردمی اگرم گوش خفته بشنودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز مطرب و می و چنگ ای پدر مکن منعم</p></div>
<div class="m2"><p>که نه بری برم از کوشش تو نه سودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسا وجود که مردار با عدم رفتی</p></div>
<div class="m2"><p>اگر نه عشق چنین قاتلی قوی بودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر موکّلِ فطرت نداشتی بر سر</p></div>
<div class="m2"><p>دلِ ستم گرِ ما را که کار فرمودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا نمی‌شودِ ای ماه روی رغبتِ آن</p></div>
<div class="m2"><p>که از وجود تو بی‌چاره‌یی بیاسودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هلاک می‌کنی و روی می‌نمایی باز</p></div>
<div class="m2"><p>سراب می‌کند از دور تشنه خشنودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دریغ اگر بصری داشتی ملامت‌گر</p></div>
<div class="m2"><p>که هم چو من نظری ناگه از تو بر بودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نزاریا به بلا گر نمی‌بدی لایق</p></div>
<div class="m2"><p>زمانه‌ات به سرِ عشق راه ننمودی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر مراد بدادی جهان به کس مجنون</p></div>
<div class="m2"><p>ز هجر لیلی بی‌چاره تن نفرسودی</p></div></div>