---
title: >-
    شمارهٔ ۳۷۵
---
# شمارهٔ ۳۷۵

<div class="b" id="bn1"><div class="m1"><p>ای حقه ی نقد جان بر طاق دو ابرویت</p></div>
<div class="m2"><p>وی حلقه ی جان و دل پیرامن گیسویت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تشویش خردمندان از باد صبا بودی</p></div>
<div class="m2"><p>گر هیچ گذر کردی بر سلسله ی مویت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پیرهن یوسف تا بیش نگوید کس</p></div>
<div class="m2"><p>بر باد فشان یک شب سامک چه ی خوش بویت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاید که کند دعوی کز خلد همی آیم</p></div>
<div class="m2"><p>آن را که گذر باشد بر خاک سر کویت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصف لب شیرینت چون من که کند الّا</p></div>
<div class="m2"><p>هنگام شکر خوردن طوطی سخن گویت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون جام به کف گیری با آن همه گیرایی</p></div>
<div class="m2"><p>گرمی نکند صهبا با نازکی خویت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چشم زدم بر هم بربود مرا از من</p></div>
<div class="m2"><p>ختم است فسون کردن بر غمزه ی جادویت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز با تو نمی باشم زیرا نتوان بودن</p></div>
<div class="m2"><p>در مظلمه ی هجرت بی مشعله ی رویت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی چاره نزاری را مرجع تو و ملجأ تو</p></div>
<div class="m2"><p>عیبش نتوان کردن گر میل کند سویت</p></div></div>