---
title: >-
    شمارهٔ ۴۶۱
---
# شمارهٔ ۴۶۱

<div class="b" id="bn1"><div class="m1"><p>یارِ ما ولوله در عالمِ راز اندازد</p></div>
<div class="m2"><p>گر نقابی که برانداخته باز اندازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشش آن قامت و بالا که خود استادِ ازل</p></div>
<div class="m2"><p>کسوتِ حسن به بالایِ دراز اندازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقیا باده دمادم ده و با چنگی گوی</p></div>
<div class="m2"><p>تا ز آهنگِ عراقم به حجاز اندازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دشمنِ سختِ من است آن که حدیثِ من و دوست</p></div>
<div class="m2"><p>نه به عکسِ روشِ عقل فراز اندازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بارها خواستمش گفت که یک حلقه از آن</p></div>
<div class="m2"><p>زلف در حلقِ ملامت گرِ راز اندازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیرتم باز پشیمان کند و داند عقل</p></div>
<div class="m2"><p>که خیالم به چنین فکرِ مجاز اندازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم بر هم مزن ای دیده که برخواهد خاست</p></div>
<div class="m2"><p>فتنۀ تازه به هر غمزه که باز اندازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>واعظی گفت به مقصد نرسد الّا آن</p></div>
<div class="m2"><p>که به مسجد رود و سر به نیاز اندازد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خود خیالِ تو نزاریِ خراباتی را</p></div>
<div class="m2"><p>نگذارد که مصلاً به نماز اندازد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که محمود بود بر همه عالم بندد</p></div>
<div class="m2"><p>درِ آن دیده که بر رویِ ایاز اندازد</p></div></div>