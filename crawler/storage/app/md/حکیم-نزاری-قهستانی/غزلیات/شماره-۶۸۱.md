---
title: >-
    شمارهٔ ۶۸۱
---
# شمارهٔ ۶۸۱

<div class="b" id="bn1"><div class="m1"><p>وَه وَه از جانِ به لب آمده بر بویِ لبش</p></div>
<div class="m2"><p>وزتنِ مانده خیالی ز خیالِ قصبش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف و رویش هبل ولاتِ منِ مجنون اند</p></div>
<div class="m2"><p>شب و روزِ منِ آسیمه سر از روز و شبش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ابتدا بی دلی و شیفته رایی کردن</p></div>
<div class="m2"><p>از کجا خاست مرا با تو بگویم سببش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوسه یی چند به من داد وز آن وقت چنین</p></div>
<div class="m2"><p>جگرم گرم شده ست از لبِ هم چون رطبش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زندگانیِ من و عمرِ گرامی آن است</p></div>
<div class="m2"><p>که بقایایِ بقا صرف کنم در طلبش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر دلم برد نگارا به سرِ زلفِ تو دست</p></div>
<div class="m2"><p>من نیارم تو بفرمای به واجب ادبش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه بی وجه بود عیب و ملامت کردن</p></div>
<div class="m2"><p>بی دلی را که بود آرزویت مستحبش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شرحِ حسنِ تو به تفضیل نمی یارم گفت</p></div>
<div class="m2"><p>گاه گاه از پیِ تخفیف کنم منتخبش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر جهان خلد برین است نزاری را بس</p></div>
<div class="m2"><p>سرِ کویِ تو مقاماتِ نشاط و طربش</p></div></div>