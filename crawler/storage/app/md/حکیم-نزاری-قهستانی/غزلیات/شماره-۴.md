---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>همت مجنون نکرد جز به حبیب التجا</p></div>
<div class="m2"><p>لاجرم اندر جهان نام ببرد از وفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی عرق چین تو روح مرا هم چنانک</p></div>
<div class="m2"><p>پیرهن نور دل دیده ی یعقوب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روضه ی جانم بسوخت آتش حسرت چو نی</p></div>
<div class="m2"><p>تا به کجا می کند آهوی چشمت چرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه نزاری بسوخت خرمن صبر و شکیب</p></div>
<div class="m2"><p>ابر صفت کلّه بست دود دلم در هوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محمل لیلی برفت طاقت مجنون نماند</p></div>
<div class="m2"><p>چند کند احتمال چون نرود بر قفا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داور من عشق بس مظلمه آن جا برم</p></div>
<div class="m2"><p>عشق برافتادگان ظلم ندارد روا</p></div></div>