---
title: >-
    شمارهٔ ۱۳۹۴
---
# شمارهٔ ۱۳۹۴

<div class="b" id="bn1"><div class="m1"><p>نشسته ام مترصّد به کنج تنهایی</p></div>
<div class="m2"><p>بدان امید که تشریف وصل فرمایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آرزوی دمی ام که هم دمی یابم</p></div>
<div class="m2"><p>مگر خلاص شوم از عذاب تنهایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهی سعادت و دولت اگر شبی، روزی</p></div>
<div class="m2"><p>ز در درآیی و از رخ نقاب بگشایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عفو تو نه غریب است جرم بخشیدن</p></div>
<div class="m2"><p>ز لطف تو نه بدیع است بنده بخشایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه خانه خانه ی خیلِ خیالِ طلعتِ توست</p></div>
<div class="m2"><p>به فضل خود چه شود خانه گر بیارایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر چنان که زمن بنده زلّتی رفته ست</p></div>
<div class="m2"><p>به لطف باز نوازی به صلح بازآیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نزاریا نه بر ازای حدِّ توست بگوی</p></div>
<div class="m2"><p>تو کیستی که به هم خانگیِ او شایی</p></div></div>