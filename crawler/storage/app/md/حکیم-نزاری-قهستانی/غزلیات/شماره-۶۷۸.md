---
title: >-
    شمارهٔ ۶۷۸
---
# شمارهٔ ۶۷۸

<div class="b" id="bn1"><div class="m1"><p>از آنم خلق می خوانند قلّاش</p></div>
<div class="m2"><p>که در شورم چو بینم زلفِ جَمّاش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگین در حلقه چون باشد گرفتار</p></div>
<div class="m2"><p>منم در حلقۀ رندانِ قلّاش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا با دوستان صلح است و با من</p></div>
<div class="m2"><p>همیشه دوستان را جنگ و پرخاش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قبولِ پندِ خودبینان ندارم</p></div>
<div class="m2"><p>ازین جا می کنند اسرارِ من فاش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رقیبم گو به سوزن دیده بر دوز</p></div>
<div class="m2"><p>حسودم گو به نشتر سینه بخراش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محبّت از دلم نتوان برون برد</p></div>
<div class="m2"><p>چه غم دارم ز بدگویانِ فحّاش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه حاصل عقل را از صحبتِ عشق</p></div>
<div class="m2"><p>شعاعِ آفتاب و چشمِ خفّاش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا این کار با عشق اوفتاده ست</p></div>
<div class="m2"><p>تو باری حالیا از عقل خوش باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرم پر شورِ عشق از ماه رویی ست</p></div>
<div class="m2"><p>که در بزمش سزد صد زُهره فرّاش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ولی او پادشازاده ست و من رند</p></div>
<div class="m2"><p>عجب گر سر فرود آرد به اوباش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منم دیوانۀ زنجیرِ زلفش</p></div>
<div class="m2"><p>که بودی حلقه یی در حلقِ من کاش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نزاری فخر کن بر سر فرازان</p></div>
<div class="m2"><p>اگر دستت دهد بوسیدنِ پاش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیاپی از نثارِ کلک و دیده</p></div>
<div class="m2"><p>گهر می ریز و مروارید می پاش</p></div></div>