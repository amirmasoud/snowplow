---
title: >-
    شمارهٔ ۱۰۲۱
---
# شمارهٔ ۱۰۲۱

<div class="b" id="bn1"><div class="m1"><p>گر به گوشت برسد درد من و زاری من</p></div>
<div class="m2"><p>زحمت آید مگرت بر شب بیداری من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تو ام ره ندهد بی تو نمی یارم بود</p></div>
<div class="m2"><p>از گران جانی بخت است سبک ساری من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من ترا دارم و بی تو نتوان داشت مرا</p></div>
<div class="m2"><p>جاودانیست در این قید گرفتاری من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صفت لیلی و مجنون که شنیدی بنگر</p></div>
<div class="m2"><p>تا بدان حسن کسی هست و بدین زاری من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من از آن جام نخوردم که به خود بازآیم</p></div>
<div class="m2"><p>عقل ازین پس نبرد راه به هشیاری من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو نه آنی که من از تو طمع این دارم</p></div>
<div class="m2"><p>که قدم رنجه کنی از پی دلداری من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کنم صبر و جفا می کشم و می گویم</p></div>
<div class="m2"><p>یادت آید مگر از دوستی و یاری من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزگار دل بی خویشتنم بر هم زد</p></div>
<div class="m2"><p>تا چه می خواست فراقت ز دل آزاری من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خود نگویی که نزاری چو ز حد در گذرد</p></div>
<div class="m2"><p>بر در شاه بنالد ز ستمگاری من</p></div></div>