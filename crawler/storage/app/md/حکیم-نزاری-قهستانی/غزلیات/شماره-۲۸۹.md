---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>حرام بر من و بر هر که بی تو می خورده ست</p></div>
<div class="m2"><p>بر آن که خورد حلالش حرام کی کرده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به حکمِ عقل حرام است نان و آب برو</p></div>
<div class="m2"><p>که ناحق از خود هر دم دلی بیازرده ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به غیرِ خمر و زنا و ربا و غیبت و قتل</p></div>
<div class="m2"><p>حلال داند وین رخصتش که آورده ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هنوز لحمِ خنازیر خوردن اولاتر</p></div>
<div class="m2"><p>بر آن که تن به طعام یتیم پرورده ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خمر خانه رو و خمر خواه و حالی خور</p></div>
<div class="m2"><p>که فرشِ عیش و بساطِ نشاط گسترده ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به رغمِ عادتِ بدگوی نیک بخت به طبع</p></div>
<div class="m2"><p>ز بامداد گرفته ست جامِ می بردست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوشی درآ به خراباتِ عشق و فارغ شو</p></div>
<div class="m2"><p>ز هرچه بر زبر و زیرِ این سرا پرده ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی که بوی نبرده ست از شمامۀ عشق</p></div>
<div class="m2"><p>گرش چو عود بسوزی هنوز افسرده ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لطیفه هایِ نزاری حقایقِ محض است</p></div>
<div class="m2"><p>تو عفو کن به بزرگی که در سخن خرده ست</p></div></div>