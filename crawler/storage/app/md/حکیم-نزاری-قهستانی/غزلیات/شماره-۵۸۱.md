---
title: >-
    شمارهٔ ۵۸۱
---
# شمارهٔ ۵۸۱

<div class="b" id="bn1"><div class="m1"><p>زهی مراد اگر از دست رفته بازآید</p></div>
<div class="m2"><p>به دست وانۀ امّیدِ من چو بازآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ستیزِ عیب کنان را وفایِ عهد کند</p></div>
<div class="m2"><p>خلافِ مدّعیان را به صلح بازآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا ز من بستاند زبس که بنوازد</p></div>
<div class="m2"><p>خیالِ او چو به بالینِ من فراز آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عذابِ روزِ قیامت همین بود که دلم</p></div>
<div class="m2"><p>هزار بار به جان از شبِ دراز آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر به بت کده ای در شود عجب نبود</p></div>
<div class="m2"><p>که پیشِ او هُبل از طاق در نماز آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل از طراوتِ رویش خجل شود در باغ</p></div>
<div class="m2"><p>چو سروِ قامتِ حسنش در اهتزاز آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن گروه که هم راه و هم وثاقِ وی اند</p></div>
<div class="m2"><p>مسافرانِ دگر را هزار ناز آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به کاروان گهِ حسنش چو خیمه بیرون زد</p></div>
<div class="m2"><p>رقیب ره ندهد هر که بی جواز آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یقین که زود به بی گانگی بر آرد سر</p></div>
<div class="m2"><p>کسی که هم رهِ او از سرِ مَجاز آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برون شوش ز بلا جز همین نمی دانم</p></div>
<div class="m2"><p>که اعتراض نیارد در احتراز آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به بد دلی نتوان بر بساطِ عشق نشست</p></div>
<div class="m2"><p>مگر در اوّلِ این باخت پاک بازآید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خلاصم از غمِ او بی خودی ست شادیِ آنک</p></div>
<div class="m2"><p>مرا صباح به یک چاره چاره ساز آید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>علاجِ دردِ نزاری همین و هیچ دگر</p></div>
<div class="m2"><p>نیازمند به درگاهِ بی نیاز آید</p></div></div>