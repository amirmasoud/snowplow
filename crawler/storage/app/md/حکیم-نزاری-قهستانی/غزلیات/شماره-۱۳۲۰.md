---
title: >-
    شمارهٔ ۱۳۲۰
---
# شمارهٔ ۱۳۲۰

<div class="b" id="bn1"><div class="m1"><p>مردم ز عشق رویت رحمی کن ار توانی</p></div>
<div class="m2"><p>جان بخش مرده‌ای را زان آبِ زندگانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چشمه حیاتت گر قطره‌ای بنوشم</p></div>
<div class="m2"><p>ایمن شوم ز مردن چون خضر جاودانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیسی به معجزِ دم می‌کرد مرده زنده</p></div>
<div class="m2"><p>تو گر قیاس گیری در خاصیت همانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو مریم حیاتی گر معجزات خواهی</p></div>
<div class="m2"><p>صد مرده در زمانی از لب به جان رسانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصلت ز پای جانم بندی نمی‌گشاید</p></div>
<div class="m2"><p>یک ره بگیر دستم چندم به سر دوانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شست جفا گشادی کردی نشانه گاهم</p></div>
<div class="m2"><p>به زین نشانه‌ای کن تا کی ز بی‌نشانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هستم سگی ز کویت جز دامنت نگیرم</p></div>
<div class="m2"><p>از پیش اگر برانی وز پس اگر بخوانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون آستین ببوسد دستت برایِ بویت</p></div>
<div class="m2"><p>گر دامن از نزاری صد ره فرو فشانی</p></div></div>