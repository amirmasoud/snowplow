---
title: >-
    شمارهٔ ۱۴۰۳
---
# شمارهٔ ۱۴۰۳

<div class="b" id="bn1"><div class="m1"><p>ساقیِ مجلسِ صفا چارهٔ ما به چاره‌ای</p></div>
<div class="m2"><p>چاره و بس که حالتی نیست جز این و چاره‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میر خمار هر چه شد تیره ز من زمان زمان</p></div>
<div class="m2"><p>می‌کشد از سپاهِ غم بر سرِ من هزاره‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فایض میکده به ما دیر به دیر می‌رسد</p></div>
<div class="m2"><p>طیره شده‌ست در میان کرده ز ما کناره‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راتبه‌مان نمی‌رسد مَشرِبه‌مان نمی‌دهد</p></div>
<div class="m2"><p>از من و روزگار من تیره شده‌ست پاره‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوختهٔ صبوح را تاب رسیده بر جگر</p></div>
<div class="m2"><p>روزِ جزا مگر شود واسطهٔ کفاره‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حَیَّ علی الصَلات من قامتِ ساقیان بود</p></div>
<div class="m2"><p>زنده شوم چو بر شود بانگ به هر مناره‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهد و ورع مگر نشد هم ره خوی و طبعِ ما</p></div>
<div class="m2"><p>مختلفند راستی طالعِ هر ستاره‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محتسبِ فراخ‌رو تنگ‌دلی چه می‌کند</p></div>
<div class="m2"><p>حوصله‌ای فراخ‌تر بایدش از غراره‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیش مگو نزاریا از حرکاتِ ناقصی</p></div>
<div class="m2"><p>لاشه خری کجا رسد در عقبِ سواره‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می‌خورد و خورنده را می‌کند احتساب و حدّ</p></div>
<div class="m2"><p>می‌زندش که که بر دلش بادزده کناره‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرزه‌سگانِ بدگمان غیبتِ ما چه می‌کنند</p></div>
<div class="m2"><p>باز بگوی ساقیا چارهٔ ما به چاره‌ای</p></div></div>