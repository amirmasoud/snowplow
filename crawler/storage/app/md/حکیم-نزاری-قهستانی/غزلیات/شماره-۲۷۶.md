---
title: >-
    شمارهٔ ۲۷۶
---
# شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>بار دگر هوایِ نشابورم آرزوست</p></div>
<div class="m2"><p>بر کف گرفته شیرۀ انگورم آرزوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش در کنارِ دوست میان نخ و نسیج</p></div>
<div class="m2"><p>تا روز خفته در شبِ دیجورم آرزوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او از پیِ عیادتِ من رنجه کرده پای</p></div>
<div class="m2"><p>بنهاده دست بر دلِ رنجورم آرزوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لولویِ زیرِ حُقّۀ لعلِ لبش نهان</p></div>
<div class="m2"><p>پیدا ز حقّه لولویِ منشورم آرزوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا گوش من بگیرد و در حلق ریزدم</p></div>
<div class="m2"><p>افتاده هالک و شده مخمورم آرزوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آوازۀ رقیب که دردِ سرم ازوست</p></div>
<div class="m2"><p>چون بانگِ نا خوشِ دهل از دورم آرزوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می خواهمش به حلق در آویخته ز دار</p></div>
<div class="m2"><p>نه نه دو نیم کرده به ساطورم آرزوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از قهستان شده به خراسان زمان زمان</p></div>
<div class="m2"><p>آوازۀ نزاریِ مهجورم آرزوست</p></div></div>