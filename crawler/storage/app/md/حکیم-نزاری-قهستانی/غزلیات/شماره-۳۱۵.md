---
title: >-
    شمارهٔ ۳۱۵
---
# شمارهٔ ۳۱۵

<div class="b" id="bn1"><div class="m1"><p>می ده که در خواص کم از سلسبیل نیست</p></div>
<div class="m2"><p>رغم جماعتی که بر ایشان سبیل نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی می مرو طریق محبت که هم چو می</p></div>
<div class="m2"><p>روشن دلی دگر سوی مقصد دلیل نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر می پرست ناخلف است و حرام زاد</p></div>
<div class="m2"><p>پس در همه قبیله ی آدم اصیل نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می شاهدست اگر نبود همدم خمار</p></div>
<div class="m2"><p>هرچند ازهرست ولی بی هلیل نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اقبال آن که دارد و خوش میخورد به طبع</p></div>
<div class="m2"><p>بدبخت چون کند که به دست بخیل نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی هرزه عمر میگذرانیم و حاصلی</p></div>
<div class="m2"><p>جز آفت دماغ و دل از قال و قیل نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چیز را به عمر توان یافتن بدل</p></div>
<div class="m2"><p>جز روزگار عمر که آن را بدیل نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دوست قاصدی که پیام آورد به دوست</p></div>
<div class="m2"><p>انصاف میدهم که کم از جبرئیل نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون مور اگر ضعیف نباشیم ممکن است</p></div>
<div class="m2"><p>بار فراغ دوست به بازوی پیل نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما را چه التفات به نمرودیان دهر</p></div>
<div class="m2"><p>مقصود ما از اینهمه الا خلیل نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مردار میرود به حقیقت نزاریا</p></div>
<div class="m2"><p>هرکو به تیر عشق چو مجنون قتیل نیست</p></div></div>