---
title: >-
    شمارهٔ ۱۰۸۰
---
# شمارهٔ ۱۰۸۰

<div class="b" id="bn1"><div class="m1"><p>ای به دیدار تو جانم آرزومند آمده</p></div>
<div class="m2"><p>پای تا سر همچو زلفت بند در بند آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیش تر فریاد رس جانا که از بس اشتیاق</p></div>
<div class="m2"><p>کار من در یک نفس با قطع و پیوند آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیش ازین طاقت ندارم در فراق روی تو</p></div>
<div class="m2"><p>یعلم الله نیز اگر حاجت به سوگند آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزگاری دیر باید تا توانم باز گفت</p></div>
<div class="m2"><p>آنچه بر من بنده زاندوه خداوند آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر نفس جانم رسد از آرزومندی به لب</p></div>
<div class="m2"><p>تا نپنداری نزاری از تو خرسند آمده</p></div></div>