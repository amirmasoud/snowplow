---
title: >-
    شمارهٔ ۱۱۶۶
---
# شمارهٔ ۱۱۶۶

<div class="b" id="bn1"><div class="m1"><p>دوش آمدی و خرمن ما آتشی زدی</p></div>
<div class="m2"><p>ام‌شب بیا و باز رهان بازم از خودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خالی کنیم خانه ز بهرِ نزولِ دوست</p></div>
<div class="m2"><p>لابد برون شود همه چون تو درآمدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشقِ تو آمد و رگِ مجنون فروگذشت</p></div>
<div class="m2"><p>این‌جا چه جای عاقلی و جایِ بخردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیلی و یک کرشمه و سد طعنه ی حسود</p></div>
<div class="m2"><p>ماییم و هیچ چون همه یک بار بستدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل در غمِ تو بستم و نگشاد از تو هیچ</p></div>
<div class="m2"><p>مهرِ تو برگرفتم و در خونِ من شدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آری نزاریا تو همه نیک بین نه بد</p></div>
<div class="m2"><p>ور عاشقی نُطُق مزن از نیکی و بدی</p></div></div>