---
title: >-
    شمارهٔ ۵۰۴
---
# شمارهٔ ۵۰۴

<div class="b" id="bn1"><div class="m1"><p>به لطف تو نبود گر بسی نکو باشد</p></div>
<div class="m2"><p>کسی نگفت که ترک فرشته خو باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عجایب از تو فرو مانده ام که تا شخصی</p></div>
<div class="m2"><p>بود که اینهمه اخلاق خوش درو باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمایل تو بیا گو ببین ملامت گر</p></div>
<div class="m2"><p>اگر چو من نشود حق به دست او باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا به سنگ ملامت نمی زنند که دل</p></div>
<div class="m2"><p>نه دل بوَد ، پس اگر بشکند سبو باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چاه گویِ زنخدانت ار نگاه کنند</p></div>
<div class="m2"><p>هزار دلشده سرگشته همچو گو باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر به دوش براندازی و نپوشانی</p></div>
<div class="m2"><p>جهان ز نافه زلف تو مشک بو باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نصیب من ز تو اندیشه ای تمام بود</p></div>
<div class="m2"><p>مرا چه زهره و یارای گفتگو باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نزاریا تو خود انصاف خود بده تا حیف</p></div>
<div class="m2"><p>بود که چون تو کسی را نظر برو باشد</p></div></div>