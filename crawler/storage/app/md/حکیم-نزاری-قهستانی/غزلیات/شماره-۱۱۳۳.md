---
title: >-
    شمارهٔ ۱۱۳۳
---
# شمارهٔ ۱۱۳۳

<div class="b" id="bn1"><div class="m1"><p>اگر خواهی که خود را بازیابی</p></div>
<div class="m2"><p>میان محرمان راز یابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غلط کردم در او شو محوِ مطلق</p></div>
<div class="m2"><p>چو گشتی محو آن‌جا بازیابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همایِ عشق را بر فرق تسلیم</p></div>
<div class="m2"><p>ز بامِ سدره در پرواز یابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر از ثعبان نفس ایمن بباشی</p></div>
<div class="m2"><p>چو موسی قدرتِ اعجاز یابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن بر بی ‌زبانان ظلم امروز</p></div>
<div class="m2"><p>که فرداشان سخن پرداز یابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به روز مویه یک یک موی بر خویش</p></div>
<div class="m2"><p>اگر مایی کنی غمّاز یابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهی شرکِ عظیم استغفرالله</p></div>
<div class="m2"><p>که با او خویش را انباز یابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یقین می‌دان که باشی در جهنم</p></div>
<div class="m2"><p>چو خود را در میان آز یابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نسازد با تو دنیا تا تو از حرص</p></div>
<div class="m2"><p>همه کارِ جهان ناساز یابی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نزاری محو شو فی‌الجمله در دوست</p></div>
<div class="m2"><p>اگر خواهی که خود را بازیابی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همان شرک است اگر یک ذره خود را</p></div>
<div class="m2"><p>در این چشم دوبینی بازیابی</p></div></div>