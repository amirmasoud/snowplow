---
title: >-
    شمارهٔ ۱۳۸۲
---
# شمارهٔ ۱۳۸۲

<div class="b" id="bn1"><div class="m1"><p>مرا ناگاه پیش آمد بلایی</p></div>
<div class="m2"><p>خلافِ عقل نازل شد قضایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم گم شد ندانستم کجا رفت</p></div>
<div class="m2"><p>نشانی میدهد هر کس به جایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تفحّص کردم از قوم مسافر</p></div>
<div class="m2"><p>مگر گفتم بود مشکل گشایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی میگفت دیدم در ختایش</p></div>
<div class="m2"><p>گرفتارِ کمند دل ربایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دگر یک گفتم پندارم به کشمیر</p></div>
<div class="m2"><p>چنینی دیده ام قیدِ بلایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر یک گفت من دیدم به رومش</p></div>
<div class="m2"><p>چنین افتاده کار مبتلایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دگر یک گفت نی در زنگبار است</p></div>
<div class="m2"><p>چنین دیوانه ی زنجیر سایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دگر یک گفت در بغداد دیدم</p></div>
<div class="m2"><p>چنین سرگشته ی شوریده رایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دگر یک گفت دیدم در عراقش</p></div>
<div class="m2"><p>ندیدم لیک کارش را نوایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دگر گفت از خراسانش طلب کن</p></div>
<div class="m2"><p>به هر موضع که خوش دارد هوایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی گفت از قهستان نیست بیرون</p></div>
<div class="m2"><p>طلب کن دل فروزی جان فزایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به غمزه ساحری عاشق فریبی</p></div>
<div class="m2"><p>به لب جان پروری معجز نمایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهان آرای رویی دارد الحق</p></div>
<div class="m2"><p>که خورشید است در جنبش سهایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نشسته دیدمش جایی که دارد</p></div>
<div class="m2"><p>چو فردوس برین خرم فضایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به عزّت بود بر طاق هلالی</p></div>
<div class="m2"><p>زخطِّ سبز کرده متّکایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو رمزش گوش کردم مطلقا بود</p></div>
<div class="m2"><p>صفات ابروان آشنایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نزاری وقت آن آمد که بادل</p></div>
<div class="m2"><p>درآیی از سرِ صلح و رضایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نشاید کرد بیزاری به یک بار</p></div>
<div class="m2"><p>گر از وی دروجود آید خطایی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرو مِن بعد بر دنبال دل بیش</p></div>
<div class="m2"><p>برو بنشین به کنج انزوایی</p></div></div>