---
title: >-
    شمارهٔ ۷۸۶
---
# شمارهٔ ۷۸۶

<div class="b" id="bn1"><div class="m1"><p>ای دلِ شوریده سرِ بُل فضول</p></div>
<div class="m2"><p>بیش مرو در پیِ ردّ و قبول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای برون می نهی از حّدِ خویش</p></div>
<div class="m2"><p>دست مزن رقص مکن بی اصول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساکن و آهسته و تن دار باش</p></div>
<div class="m2"><p>زود نگردد متغیّر ملول</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خانه ی اوباش تهی کن که شاه</p></div>
<div class="m2"><p>بی خبر آید کند آن جا نزول</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس روِ هادی شو لا حول کن</p></div>
<div class="m2"><p>تا نروی بیش ز دنبالِ غول</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دعویِ تصدیق و قبولِ مجاز</p></div>
<div class="m2"><p>زان طرفِ رود چه حاجت به پول</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چیست وفاداری و فرمان بری</p></div>
<div class="m2"><p>بر سخنِ دوست نجستن عدول</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غایب و در دوست شدن محو چیست</p></div>
<div class="m2"><p>هم چو نزاری شده از خود ملول</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رمزِ نزاری همه دیوانگی ست</p></div>
<div class="m2"><p>نیست به ادراکِ قیاسِ جهول</p></div></div>