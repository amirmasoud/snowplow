---
title: >-
    شمارهٔ ۱۹۶
---
# شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>عرق چین تو بویی بر صبا بست</p></div>
<div class="m2"><p>صبا بر جنبش شریان ما بست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزاران نافه ی آهوی تبّت</p></div>
<div class="m2"><p>ز چین زلف بر مشک ختا بست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزاران دل به صد ناز و کرشمه</p></div>
<div class="m2"><p>دو چشمت برد و بر زلف دوتا بست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدم آشفته بر فرق نگاری</p></div>
<div class="m2"><p>که خط راستی بر استوا بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عبای عاشقان معراج شوق است</p></div>
<div class="m2"><p>ولی مرد هوایی بر هوا بست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ملامت گر به ما ظن خطا برد</p></div>
<div class="m2"><p>خیالی بود کو بر جان ما بست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در این عالم که در بر ما گشادست</p></div>
<div class="m2"><p>تمنای تو مارا کرد پا بست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عنان عشق نبساید که خود را</p></div>
<div class="m2"><p>نه بر فتراک عشق از ابتدا بست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بهر چینه یی را آن چه مرغ است</p></div>
<div class="m2"><p>که او خود را نه بر دام بلا بست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که داند تا ز مبدا نقش لیلی</p></div>
<div class="m2"><p>قضا بر صورت مجنون چرا بست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به مسمار محبّت نعل عشقش</p></div>
<div class="m2"><p>قضا بر جبهت این مبتلا بست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به زاری کشت جلاد فراقش</p></div>
<div class="m2"><p>نزاری راو وصلش بر قضا بست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ولیکن از مبادی محبّت</p></div>
<div class="m2"><p>نزاری همچنان مست و خراب است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سخن پوشیده میگوید محقق</p></div>
<div class="m2"><p>ببین تا از کجا چون بر کجا بست</p></div></div>