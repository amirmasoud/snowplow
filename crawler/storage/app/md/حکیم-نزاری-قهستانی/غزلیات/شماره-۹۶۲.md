---
title: >-
    شمارهٔ ۹۶۲
---
# شمارهٔ ۹۶۲

<div class="b" id="bn1"><div class="m1"><p>تنم جایی و دل جایی ندارم زَهرهٔ گفتن</p></div>
<div class="m2"><p>دلم آن جا و تن زاین جا ندارد قوت رفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی کآشفته ی اویم ندانم با که بر گویم</p></div>
<div class="m2"><p>یکی محرم همی جویم که داند راز بنهفتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو ماه از پرده شد پیدا تحمل کی کند شیدا</p></div>
<div class="m2"><p>که را باشد درین سودا مجال خوردن و خفتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که را آن زهره و یارا که گوید رحم کن یارا</p></div>
<div class="m2"><p>وگر او رد کند ما را که خواهد در پذیرفتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شود از طعنه ی مردم امیدم گم مرادم گم</p></div>
<div class="m2"><p>اگر ممکن شود انجم ازین طارم فرو رفتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر در بحر نفتادی صدف را سینه نگشادی</p></div>
<div class="m2"><p>چنینش دست کی دادی نزاری را گهر سفتن</p></div></div>