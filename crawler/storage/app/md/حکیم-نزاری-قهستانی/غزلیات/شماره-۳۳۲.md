---
title: >-
    شمارهٔ ۳۳۲
---
# شمارهٔ ۳۳۲

<div class="b" id="bn1"><div class="m1"><p>آه و واویلا کم برگ شکیبایی نیست</p></div>
<div class="m2"><p>هیچ مشکل بتر از محنت تنهایی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پشتم از بار فراق تو دو تا شد چه عجب</p></div>
<div class="m2"><p>که ز بی قوّتی ام قوت یکتایی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من و سودای تو من بعد که جاهل باشد</p></div>
<div class="m2"><p>هر خردمند که عاشق شد و سودایی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاهد لاله رخ و یار صنوبر بالا</p></div>
<div class="m2"><p>همه جا هست و مرا خاطر هر جایی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپر تیر ملامت شده ام چتوان کرد</p></div>
<div class="m2"><p>با کمانی که به بازوی توانایی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق دردیست که در تربیت درمانش</p></div>
<div class="m2"><p>مرهمی نیست وگر هست به خودرایی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاقلان بار خدایا همه عاشق گردند</p></div>
<div class="m2"><p>تا بدانند که این کار به دانایی نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیب و بالا و نظر نقش خیالت چه شود</p></div>
<div class="m2"><p>یوسفم را چو غم سوز زلیخایی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از چنان روی به آوازه ی خوبی خرسند</p></div>
<div class="m2"><p>گر کسی هست نزاری به شکیبایی نیست</p></div></div>