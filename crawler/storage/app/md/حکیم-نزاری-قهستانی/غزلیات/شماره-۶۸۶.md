---
title: >-
    شمارهٔ ۶۸۶
---
# شمارهٔ ۶۸۶

<div class="b" id="bn1"><div class="m1"><p>خدایا ازین مسکراتم ببخش</p></div>
<div class="m2"><p>به مردانِ خود سیّئاتم ببخش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بس تشنۀ رحلتم ای کریم</p></div>
<div class="m2"><p>گران شربتی ز آن فراتم ببخش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو خاصانِ مخلص به آزادگی</p></div>
<div class="m2"><p>ز دیوانِ خود یک براتم ببخش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کشتیِ نوحِ عنایت درون</p></div>
<div class="m2"><p>ز طوفانِ غفلت نجاتم ببخش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو زین قلزمم داده باشی نجات</p></div>
<div class="m2"><p>ز گردابۀ مهلکاتم ببخش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو فرزین اگر چند کژ رفته ام</p></div>
<div class="m2"><p>تو رحمت کن از شاه ماتم ببخش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو آورده ای و تو پرورده ای</p></div>
<div class="m2"><p>خطای نفوس و ذواتم ببخش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلاصم ده از بت پرستیِ خود</p></div>
<div class="m2"><p>به الّا الله از لایِ لاتم ببخش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا خوش روان کن چو خوش داشتی</p></div>
<div class="m2"><p>به یک لمحۀ التفاتم ببخش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از آن پیش کز پس درآید اجل</p></div>
<div class="m2"><p>برون بر ازین مشکلاتم ببخش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قسم بر تو استغفرالله لک</p></div>
<div class="m2"><p>به فضلت که پیش از وفاتم ببخش</p></div></div>