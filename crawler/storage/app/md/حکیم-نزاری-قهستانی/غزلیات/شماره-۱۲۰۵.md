---
title: >-
    شمارهٔ ۱۲۰۵
---
# شمارهٔ ۱۲۰۵

<div class="b" id="bn1"><div class="m1"><p>خوش ایّامی و خرّم روزگاری</p></div>
<div class="m2"><p>که صحبت داشتم با تو یاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دمی کاندر فراقت می برآرم</p></div>
<div class="m2"><p>چه گویم زندگانی نیست باری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه راحت باشدش هیچ از جوانی</p></div>
<div class="m2"><p>کسی را کش نباشد غم‌گُساری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بادی گذر دارد به کویت</p></div>
<div class="m2"><p>ببین کز من نباشد بی‌غباری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و گر مرغی به بامت بر نشیند</p></div>
<div class="m2"><p>بود بر بالش از من نامه واری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرد بر صبر می‌دارد دلم را</p></div>
<div class="m2"><p>قراری می‌دهد با بی‌قراری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولیکن در هوای وصلِ خورشید</p></div>
<div class="m2"><p>نباشد ذرّه را بس اختیاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نزاری تا کی‌ای کوته‌نظر هان</p></div>
<div class="m2"><p>که عمرت صرف شد در انتظاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برو یاری به دست‌آور که دنیا</p></div>
<div class="m2"><p>ندارد پیش دنیا اعتباری</p></div></div>