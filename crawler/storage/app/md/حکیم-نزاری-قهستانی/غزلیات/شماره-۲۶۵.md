---
title: >-
    شمارهٔ ۲۶۵
---
# شمارهٔ ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>آرزویی بود و شد محو در اوصافِ دوست</p></div>
<div class="m2"><p>هیچ ز من مانده نیست هرچه بمانده­ست اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامِ می و کنج و یار هر چه دگر جمله هیچ</p></div>
<div class="m2"><p>شش جهتِ کاینات بر سرِ این چارسوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موسی و ثُعبان و چوب سامری و عجلِ زر</p></div>
<div class="m2"><p>او نکند التفات موسی اگر تندخوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیشِ من و پشت اگر نیک و بدی می­رود</p></div>
<div class="m2"><p>هرچه از آن­جا بود جمله بدی­ها نکوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی­هده سودا مپز از پیِ نام و حطام</p></div>
<div class="m2"><p>مغز تهی می­کنی خیز و برون آز پوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باطنت ار با خداست کعبه خراباتِ تست</p></div>
<div class="m2"><p>فقر چو نورِ درون خرقه چو مشتی رکوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاصر و غالی نیم راهِ مشنّع کجاست</p></div>
<div class="m2"><p>مرتبه حدِّ من پیشِ مقصّر غلوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیش نزاری مرو بر پیِ نفسِ دنی</p></div>
<div class="m2"><p>بر زن و در هم شکن سنگ سزایِ سبوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دولتِ من گر ز من باز ستانی مرا</p></div>
<div class="m2"><p>از تو همین التماس از تو همین آرزوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گنج به کُنجِ خراب می­نهی و در طلب</p></div>
<div class="m2"><p>انجمِ افلاک را کار همین گفت­وگوست</p></div></div>