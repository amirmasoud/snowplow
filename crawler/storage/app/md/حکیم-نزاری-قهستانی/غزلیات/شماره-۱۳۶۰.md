---
title: >-
    شمارهٔ ۱۳۶۰
---
# شمارهٔ ۱۳۶۰

<div class="b" id="bn1"><div class="m1"><p>بتی در خیمه ی دیدم چو ماهی</p></div>
<div class="m2"><p>که بازو کرده بر رخ تکیه گاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سهی سروی و بر سرو آفتابی</p></div>
<div class="m2"><p>قبایی در برو بر سر کلاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکر پاسخ لبی کوثر دهانی</p></div>
<div class="m2"><p>نشسته بر لبِ کوثر سیاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوادِ زلفِ دل گیرش همانا</p></div>
<div class="m2"><p>مرا روشن کند رویی و راهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر در قدرِ درویشی فزاید</p></div>
<div class="m2"><p>چه کم گردد ز حسنِ پادشاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شود حاصل ثوابی بی عظیمش</p></div>
<div class="m2"><p>به رحمت گر کند درمانگاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی ترسد ز دودِ سینه ی من</p></div>
<div class="m2"><p>که صد خرمن بسوزانم به آهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خیالِ ماهِ رویش ایستاده</p></div>
<div class="m2"><p>موکّل بر سرم چون وام خواهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نزاری را بخواهد کُشت آخر</p></div>
<div class="m2"><p>چه می خواهی ز خونِ بی گناهی</p></div></div>