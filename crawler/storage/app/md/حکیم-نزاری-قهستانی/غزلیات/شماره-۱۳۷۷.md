---
title: >-
    شمارهٔ ۱۳۷۷
---
# شمارهٔ ۱۳۷۷

<div class="b" id="bn1"><div class="m1"><p>ما از غم تو شدیم سودایی</p></div>
<div class="m2"><p>ای دوست چرا دمی نمی‌آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ما گذری نمیکنی گه گه</p></div>
<div class="m2"><p>ور می‌گذری دمی نمی‌پایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا تو نظری به ما و من کردی</p></div>
<div class="m2"><p>ماهیّت ما برون شد از مایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جایی که منیِّ عشق بنشیند</p></div>
<div class="m2"><p>برخیزد از آن منی همه مایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا امر نشد ز ابتدا فایض</p></div>
<div class="m2"><p>بر عقل، نشد سَمَر به دانایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیزار ز ما و من شدیم آخر</p></div>
<div class="m2"><p>یعنی ز منافقی و رعنایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاید که به طعنه دشمنان گویند</p></div>
<div class="m2"><p>ای دوست به دوستان نمی‌شایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منظورِ تو یار با تو بنماید</p></div>
<div class="m2"><p>گر آینه ی وجود بزدایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ارواح جنود شد جنود ارواح</p></div>
<div class="m2"><p>تو بر سرِ کارِ خویشتن رایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خم‌خانه ی می به خانقه گیری</p></div>
<div class="m2"><p>گر خانه ی معرفت بیارایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پالونه بیار و می به راوق کن</p></div>
<div class="m2"><p>تا چند به دیده خون بپالایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیرامنِ مصلحت نگردی بیش</p></div>
<div class="m2"><p>گر دامن معصیت نیالایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زنّارِ مغان اگر نمی‌بندم</p></div>
<div class="m2"><p>بی‌بهره‌ام از بتانِ یغمایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن قصّه‌ شنیده‌ام که بعد از حج</p></div>
<div class="m2"><p>بر بت صلیب شیخ صنعایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر شیوه ی او نزاریا کردی</p></div>
<div class="m2"><p>پیرانه سر اقتدا به برنایی</p></div></div>