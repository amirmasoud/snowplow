---
title: >-
    شمارهٔ ۳۰۹
---
# شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>ما را ز تو یک نفس به سر نیست</p></div>
<div class="m2"><p>الا در تو دری دگر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از منزل تو گذر ندارم</p></div>
<div class="m2"><p>گر هست برون شوی وگر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تو چه نشان دهد به وجهی</p></div>
<div class="m2"><p>آن را که ز خویشتن خبر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بی خبری به وجه دیگر</p></div>
<div class="m2"><p>سرّی است عجب که هست ور نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با بی خبرانِ با خبر باش</p></div>
<div class="m2"><p>تعلیمی از این شریف تر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بشتاب که سالک محقق</p></div>
<div class="m2"><p>موقوف ولایت بشر نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در معرکه ی سپاه اشواق</p></div>
<div class="m2"><p>جز تیغ نیاز کارگر نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر خیل خیال نازک دوست</p></div>
<div class="m2"><p>جز سوزن فکر را گذر نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خفاش و شعاع نور خورشید</p></div>
<div class="m2"><p>این مرتبه حد بی بصر نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جر محو شدن دگر چه تدبیر</p></div>
<div class="m2"><p>آنجا که مجال یک نظر نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با دوست مضایقت به جانی</p></div>
<div class="m2"><p>از جانب ما بدین قدر نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نی نی کششی بود از آنجا</p></div>
<div class="m2"><p>وین بیّنه زان دگر بتر نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی پرتو آفتاب نوری</p></div>
<div class="m2"><p>در صورت مظلم قمر نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از گلبُن امتحان نصیبت</p></div>
<div class="m2"><p>جز خار نزاریا مگر نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خوش باش که برفکندگان را</p></div>
<div class="m2"><p>میلی به جهان مختصر نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در پیش خدنگ غمزه ی دوست</p></div>
<div class="m2"><p>جز سینه ی بی دلان سپر نیست</p></div></div>