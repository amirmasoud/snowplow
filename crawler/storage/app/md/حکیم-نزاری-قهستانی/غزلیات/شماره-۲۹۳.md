---
title: >-
    شمارهٔ ۲۹۳
---
# شمارهٔ ۲۹۳

<div class="b" id="bn1"><div class="m1"><p>ما را سخنِ مولّهانه ست</p></div>
<div class="m2"><p>تو پنداری مگر فسانه ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه سخنی رود دو وجهی</p></div>
<div class="m2"><p>لیکن زدویی یکی یگانه ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این یک به اضافت است و کثرت</p></div>
<div class="m2"><p>وآن یک بنگر موحّدانه ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل ار چه مقدّم است لیکن</p></div>
<div class="m2"><p>او نیز مسخّرِ زمانه ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بشنو که مدارِ عشق بر چیست</p></div>
<div class="m2"><p>وین موعظه یی محقّقانه ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر نقطۀ امرو نقطۀ جان</p></div>
<div class="m2"><p>بر مرکزِ عمرِ جاودانه ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بحری متغیّرست و دروی</p></div>
<div class="m2"><p>نه عمق پدید و نه کرانه ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آرزویِ لب و کناری</p></div>
<div class="m2"><p>یک نکته عجب درین میانه ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هان تا بزنیم دست و پایی</p></div>
<div class="m2"><p>تا خود چه یقین درین گمانه ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق است و می و می و نزاری</p></div>
<div class="m2"><p>دیگر همه حیلت و بهانه ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر جا که زمرحلی برفتیم</p></div>
<div class="m2"><p>منزل گه ما شراب خانه ست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در سایۀ قصرِ او نشستیم</p></div>
<div class="m2"><p>هر مرغ مقیمِ آشیانه ست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ما لایقِ صدرِ او نباشیم</p></div>
<div class="m2"><p>آری سر ما و آستانه ست</p></div></div>