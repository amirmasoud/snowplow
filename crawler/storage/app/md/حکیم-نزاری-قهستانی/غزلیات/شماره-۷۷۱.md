---
title: >-
    شمارهٔ ۷۷۱
---
# شمارهٔ ۷۷۱

<div class="b" id="bn1"><div class="m1"><p>بیا دل ز دنیایِ دون بر گسل</p></div>
<div class="m2"><p>که بس رونقی نیست در آب و گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلوکِ تو از خود برون رفتن است</p></div>
<div class="m2"><p>شدن با دگر سالکان متّصل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دنبالِ آن سالکان کی رسی</p></div>
<div class="m2"><p>به احوالِ دنیا چنین مشتغل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گروهی گرفته رهِ فلسفه</p></div>
<div class="m2"><p>گروهی دگر مذهبِ معتزل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علی الجمله هر کس به خود مذهبی</p></div>
<div class="m2"><p>نهادند از یک دگر منفصل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مقصّر جدا گشته غالی جدا</p></div>
<div class="m2"><p>برون رفته از جاده ی معتدل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رهِ راستان است و فرمان بَران</p></div>
<div class="m2"><p>زری پاک و پاکیزه از غشّ و غل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قیامت که موقوف دارند خلق</p></div>
<div class="m2"><p>به نَطوی السّماءَ کطیِ السِجِل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر سّرِ این سَتر پیدا کنم</p></div>
<div class="m2"><p>کجا طاقت آرند کو محتمل</p></div></div>