---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>گر هیچ دلی داری دریاب دل مارا</p></div>
<div class="m2"><p>حال دل این بی دل میپسند چنین یارا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان نیست چنین کاسد کردیم بسی سودا</p></div>
<div class="m2"><p>هم نیز رهی باید بیرون شوِ سودا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر عاشق بی چاره از جور نمی نالد</p></div>
<div class="m2"><p>هم مرحمتی باید معشوقه ی زیبا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیهات که مظلومی در دامنت آویزد</p></div>
<div class="m2"><p>امروز کند عاقل اندیشه ی فردا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر یاد کنی از من هم حیف بود بر تو</p></div>
<div class="m2"><p>از وصل سخن گفتن کو زهره و کو یارا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای گل بن باغ من از من دو سخن بشنو</p></div>
<div class="m2"><p>یک بار نصیحت کن آن سرکش رعنا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گو بیش نزاری را شاید که نرنجانی</p></div>
<div class="m2"><p>گر سر ننهادستم آن بی سر و بی پا را</p></div></div>