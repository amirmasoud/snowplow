---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>کاش که من بودمی هم ره باد صبا</p></div>
<div class="m2"><p>تا گذری کردمی وقت سحر بر سبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نامه ی بلقیس جان سوی سلیمان دل</p></div>
<div class="m2"><p>کس نرساند مگر هدهد باد صبا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بگشاید ز هم چین سر زلف دوست</p></div>
<div class="m2"><p>بیش نبوید کسی نافه ی مشک ختا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی هده جان می کنم خون جگر می خورم</p></div>
<div class="m2"><p>این منم آخر چنین دوست کجا من کجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهر تو با جان من در ازل آمیختند</p></div>
<div class="m2"><p>هجر ، مرا و تو را کی کند از هم جدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آری اگر حاسدان تعبیه ای ساختند</p></div>
<div class="m2"><p>شکر که نومید نیست بنده ز فضل خدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور بستاند ز من دنیی و دین باک نیست</p></div>
<div class="m2"><p>بر همه چیز دگر غیر تو دارم رضا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یوسف جانم تویی زنده به بوی تو ام</p></div>
<div class="m2"><p>چند کنم پیرهن در غم هجرت قبا</p></div></div>