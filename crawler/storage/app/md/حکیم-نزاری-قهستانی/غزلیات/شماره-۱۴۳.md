---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>آه مِن حُبّکَ مِن حُبّکَ آه این چه قضاست</p></div>
<div class="m2"><p>بَلَغ السیل چه تدبیر کنم این چه بلاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش عشق به هر بیشه که در می‌افتد</p></div>
<div class="m2"><p>پیش او سنگ و گیا هر دو روان است و رواست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد بر دامن هر سوخته کز عشق نشست</p></div>
<div class="m2"><p>رستخیز آمد و طوفان ملامت برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم مستغرق دریا نخورد بر ساحل</p></div>
<div class="m2"><p>از کسی پرس که هم بستر خوابش دریاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کس نداند که مرا با تو چه کار افتاده ست</p></div>
<div class="m2"><p>گر بداند نکند عیب وگر کرد سزاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لا محال از عقب عشق ملامت خیزد</p></div>
<div class="m2"><p>از ملامت نگریزیم که خود شیوه ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ملامت گر و تشنیع زن و عیب نمای</p></div>
<div class="m2"><p>خاک بر فرقم اگر یک سر مویم پرواست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر مرا عشق ز خلقیّت خود برهاند</p></div>
<div class="m2"><p>چه بماند همه معشوق که در عین بقاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو حجاب است هم از پیش مگر برخیزد</p></div>
<div class="m2"><p>بیش از این هیچ ندارم ز نزاری درخواست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راز خاصان نتوان کرد چنین فاش خموش</p></div>
<div class="m2"><p>زان که در حوصله عام نمی گنجد راست</p></div></div>