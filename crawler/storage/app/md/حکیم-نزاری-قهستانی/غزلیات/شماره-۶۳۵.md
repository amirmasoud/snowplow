---
title: >-
    شمارهٔ ۶۳۵
---
# شمارهٔ ۶۳۵

<div class="b" id="bn1"><div class="m1"><p>مرا که دست به روی خرد نهادم باز</p></div>
<div class="m2"><p>به تَرهاتِ دگر مشتغل ندارد باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا ز من چه خبر در مقام وجد که درد</p></div>
<div class="m2"><p>به حضرت ملکی می‌برم که داند راز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفاذِ امر به جز عشق را مسلم نیست</p></div>
<div class="m2"><p>چه بانگ چنگ به نزدیک کر چه بانگ نماز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هرزه عمر گران مایه می‌کنی ضایع</p></div>
<div class="m2"><p>مکن که مرگ یقین بهتر از حیات مجاز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عنان به مرکب توسن مده مگر به حساب</p></div>
<div class="m2"><p>به چکسه باز نیاید چو اوج گیرد باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین نشیب نشیمن طلب گذر که مگر</p></div>
<div class="m2"><p>بر آشیانهٔ وحدت پری به بال نیاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر آستانی و با راستان نه‌ای هیهات</p></div>
<div class="m2"><p>اگر به سیم نه‌ای ملتفت چو زر مگداز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خویشتن چه شوی معجب ای مجاز پرست</p></div>
<div class="m2"><p>سری به دست که آنگاه گردنی بفراز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مصاحبی طلب اندر مسالک تحقیق</p></div>
<div class="m2"><p>که جانب تو رعایت کند به صد اعزاز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر ز پای در آیی ز دست نگذارد</p></div>
<div class="m2"><p>و گر ز چشم بیفتی نظر نگیرد باز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نزاریا نظر از اهل روزگار ببر</p></div>
<div class="m2"><p>به صبر خو کن و با درد خویشتن می‌ساز</p></div></div>