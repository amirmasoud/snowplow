---
title: >-
    شمارهٔ ۱۲۷۸
---
# شمارهٔ ۱۲۷۸

<div class="b" id="bn1"><div class="m1"><p>بر آن سری که دل مستمند ما بخلی</p></div>
<div class="m2"><p>اگر به دل برهم از تنم به جان بحلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو را رسد که اگر که بر قبیله ی آدم</p></div>
<div class="m2"><p>به حسن ناز کنی حق به دست توست بلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بوسه ای ز تو راضی نبوده ایم بیار</p></div>
<div class="m2"><p>که در صفا و کرم نوش لعل و بحر دلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا به جان عزیزت که روزگار مبر</p></div>
<div class="m2"><p>مجال گفت و شنو نیست تا کی از لک ولی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جان تو که ز پیکان تیر هجرانت</p></div>
<div class="m2"><p>جراحت است مرا بر میان جان اجلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا ولایت آن نیست کز تو بر بخورم</p></div>
<div class="m2"><p>غم فراق تو جان مرا بس است ولی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هدایت است غمت تا کجا فرود آید</p></div>
<div class="m2"><p>کفایت است نه کسبی بلی که لم یزلی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محبت تو نه آنست کس دلم برود</p></div>
<div class="m2"><p>محب معتقدم معتقد نه معتزلی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هنوز بعد قیامت همان محب تو ام</p></div>
<div class="m2"><p>کجا زوال پذیرد محبت ازلی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وفا و عهد تو محکم است در سینه چنان</p></div>
<div class="m2"><p>که اعتقاد نزاری به خاندان علی</p></div></div>