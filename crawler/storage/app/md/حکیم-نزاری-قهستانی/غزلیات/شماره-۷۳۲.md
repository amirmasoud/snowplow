---
title: >-
    شمارهٔ ۷۳۲
---
# شمارهٔ ۷۳۲

<div class="b" id="bn1"><div class="m1"><p>مدعیان می نهند پای برون از گزاف</p></div>
<div class="m2"><p>بی خبران می زنند از حرم عشق لاف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آری از آنجا که اوست هیچ دگر نیست دوست</p></div>
<div class="m2"><p>ما و خرابات و می حاج و منا و طواف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عین حیات است عشق غرق در آن عین شو</p></div>
<div class="m2"><p>در سخن می فشان چون قلم از شین و قاف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیش مباش ای پسر طالب درمان دل</p></div>
<div class="m2"><p>دردی دل آمده است داروی دلهای صاف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردن اخلاص نه زیر لگدکوب عشق</p></div>
<div class="m2"><p>سر چه کشی زین طناب جان نبری زین مصاف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای که به خود ناقصی دست بشوی از وجود</p></div>
<div class="m2"><p>مرد کمالی ولی کرده ازو طرح کاف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ ندانی خموش بیش به شوخی مکوش</p></div>
<div class="m2"><p>شرح مودت مگوی شعر محبت مباف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر ننشینی ز پای عاقبت آری به دست</p></div>
<div class="m2"><p>طالب مقصود را جنبش دردی کفاف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند کنم بی زبان چند زنم بی فغان</p></div>
<div class="m2"><p>ناله ی آهن گداز نعره ی گردون شکاف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرد دماغ جهان پر ز بخار و بخور</p></div>
<div class="m2"><p>کلک نزاری به حبر آهوی چینی زناف</p></div></div>