---
title: >-
    شمارهٔ ۱۲۴۷
---
# شمارهٔ ۱۲۴۷

<div class="b" id="bn1"><div class="m1"><p>همای وارم اگر سایه بر سر اندازی</p></div>
<div class="m2"><p>بر آفرینش عالم کنم سرافرازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عنایت تو اگر هم عنان ما باشد</p></div>
<div class="m2"><p>سبق بریم چو صاحب دلان به ممتازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تراست مملکت حسن و در مقابل تو</p></div>
<div class="m2"><p>زمانه را نرسد دل بر و طنّازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرشمه ای به عنایت ز غمزه ی توبسم</p></div>
<div class="m2"><p>کسم ز پیش نراند اگر تو بنوازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که دست گیرد و کی باز برتواند خاست</p></div>
<div class="m2"><p>ز التفات نظر هر که را بیندازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دست من چه زمین بوسه ای ز دور و همین</p></div>
<div class="m2"><p>به پای بوس مگر هم تو چاره ای سازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو راه ده به وصالم غم مشنّع نیست</p></div>
<div class="m2"><p>رقیب گوی ترا عادت است غمّازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نزاریا سر تسلیم نه سخن کوتاه</p></div>
<div class="m2"><p>زبان درازی تا چند و قصّه پردازی</p></div></div>