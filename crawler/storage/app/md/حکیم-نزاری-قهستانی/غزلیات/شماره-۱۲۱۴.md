---
title: >-
    شمارهٔ ۱۲۱۴
---
# شمارهٔ ۱۲۱۴

<div class="b" id="bn1"><div class="m1"><p>بیا یارا که آمد وقتِ یاری</p></div>
<div class="m2"><p>مرا بی خویشتن تا چند داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نومیدی چو من بی چاره ای را</p></div>
<div class="m2"><p>روا باشد که ضایع می گذاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جفا بردن ز دشمن بر امیدی</p></div>
<div class="m2"><p>بسی آسان تر از نومیدواری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسی جور از فراقت بردم ای دوست</p></div>
<div class="m2"><p>ز دشمن تا کی آخر بردباری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مباش آخر بدین نامهربانی</p></div>
<div class="m2"><p>که یاد از دوستان هرگز نیاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکیبایی مدار از من توقّع</p></div>
<div class="m2"><p>که من معذورم از آشفته کاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملاقاتی که رویت باز بینم</p></div>
<div class="m2"><p>همین میخواهم از مولا به زاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به رغبت جان به جانان واسپارم</p></div>
<div class="m2"><p>همین باشد طریقِ حق گزاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مقاماتی برون آرم که عشّاق</p></div>
<div class="m2"><p>بیاموزند از من جان سپاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو من در بی خودی مشهور دهرم</p></div>
<div class="m2"><p>ز من شینی نباشد بی قراری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>میازار آخر ای یار دل آزار</p></div>
<div class="m2"><p>نزاری تا به کی زار و نزاری</p></div></div>