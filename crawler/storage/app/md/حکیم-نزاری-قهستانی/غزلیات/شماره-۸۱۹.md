---
title: >-
    شمارهٔ ۸۱۹
---
# شمارهٔ ۸۱۹

<div class="b" id="bn1"><div class="m1"><p>به فلک می رسد از فرقتِ تو فریادم</p></div>
<div class="m2"><p>تا نگویی که من از بندِ غمت آزادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی تو بر رویِ همه خلقِ جهان بستم در</p></div>
<div class="m2"><p>لیکن از دیده بسی خونِ جگر بگشادم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل تو داری و هنوزم طمعِ وصلی هست</p></div>
<div class="m2"><p>اندرین صحبت از آن جان به تو بفرستادم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به زنجیرِ بلا بسته نبودی پایم</p></div>
<div class="m2"><p>به دو چشم آمد می باز نمی استادم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بارها در دلم اندیشه کنم تا توبه من</p></div>
<div class="m2"><p>چون فتادی و من آخر به تو چون افتادم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر من از بهرِ تو بی دادِ همه خلق رواست</p></div>
<div class="m2"><p>به قیامت بدهد قایم داور دادم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به پریشانیِ خاطر ز تو برگردم نه</p></div>
<div class="m2"><p>جمع می باش که بر جورِ تو دل بنهادم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شور در خاطرم افکند لبِ شیرینت</p></div>
<div class="m2"><p>ظاهر آن است که شوریده تر از فرهادم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست بی یادِ تو جام که بر کف گیرم</p></div>
<div class="m2"><p>بی تو گر باده خورم زهرِ هلاهل بادم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می زنم بر سر و می گویم و می گریم زار</p></div>
<div class="m2"><p>یادِ آن کس که نرفته ست دمی از یادم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به حدیثی که ز احوالِ نزاری پرسی</p></div>
<div class="m2"><p>گر چه می میرم از اندوهِ تو هم دل شادم</p></div></div>