---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>زهی قدر من گر وصال حبیب</p></div>
<div class="m2"><p>دگر باره روزی شود عن قریب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چلیپای زلفش کنم طوق حلق</p></div>
<div class="m2"><p>چنانش پرستم که رهبان صلیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو مجنون به عشق و چو لیلی به حسن</p></div>
<div class="m2"><p>من و او قریبیم و هر دو غریب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز حوران نگوید دگر وز بهشت</p></div>
<div class="m2"><p>اگر چشم مستش ببیند خطیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز روح عرق چین خوش بوی او</p></div>
<div class="m2"><p>ندارد گلاب نسیمی نصیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرق چین او هم چو انفاس روح</p></div>
<div class="m2"><p>دهد مرده را زندگانی به طیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به معجز مسیح است گویی که هست</p></div>
<div class="m2"><p>لبش را همین معجزات عجیب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا از مبادی فطرت مگر</p></div>
<div class="m2"><p>نیاموخت جز عشق بازی ادیب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مداوای عاشق عقاقیر عشق</p></div>
<div class="m2"><p>علاج نزاری چه داند طبیب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به عشق ار کنی نسبت خود درست</p></div>
<div class="m2"><p>چو تو کس نباشد حسیب و نسیب</p></div></div>