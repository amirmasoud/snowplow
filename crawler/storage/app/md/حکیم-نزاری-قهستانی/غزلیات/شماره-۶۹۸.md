---
title: >-
    شمارهٔ ۶۹۸
---
# شمارهٔ ۶۹۸

<div class="b" id="bn1"><div class="m1"><p>مرغِ دلم که زلفِ تو باشد نشیمنش</p></div>
<div class="m2"><p>کی آرزو کند هوسِ سینۀ منش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز وی التفات به زندانِ تن کند</p></div>
<div class="m2"><p>روحی که زیرِ سایۀ طوباست مسکنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید اگر ز گوشۀ برقع کند نگاه</p></div>
<div class="m2"><p>زلفت ز رشک تیره شود چشمِ روشنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور بت پرست رویِ تو بیند به اعتقاد</p></div>
<div class="m2"><p>لازم شود چو توبۀ من بت شکستنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روح الامین اگر تنِ او بیند از خیال</p></div>
<div class="m2"><p>هم در خیالِ آن چو خیالی شود تنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصلت که دید چون به خیالت نمی رسد</p></div>
<div class="m2"><p>کس تا نمی کند غمِ هجرت چو سوزنش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صیدِ کندِ زلفِ تو شد هر کجا دلی ست</p></div>
<div class="m2"><p>لطفی بکن به دوش دگربار مفکنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر ماه در کمندِ تو باشد غریب نیست</p></div>
<div class="m2"><p>خورشید نادرست کمندی به گردنش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رویت ز بیمِ فتنه که غوغا شود مگر</p></div>
<div class="m2"><p>با خلق از آن سبب بنمایی نهفتنش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اشکم ز شوقِ حلقۀ گوشت برون جهد</p></div>
<div class="m2"><p>از دیده هر زمان و بگیرم به دامنش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عکسِ خیالِ تست جگر گوشۀ دلم</p></div>
<div class="m2"><p>ز آن بر کنارِ دیده نشانم مُمکَنش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نقشِ توام ز دیدۀ ظاهر نمی رود</p></div>
<div class="m2"><p>الّا به می که بی خبرم می کند فنش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گلشن کند چو می به نزاری رسد ز شوق</p></div>
<div class="m2"><p>یادِ تو کلبۀ دلِ تاریک روشنش</p></div></div>