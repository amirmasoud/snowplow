---
title: >-
    شمارهٔ ۱۷۶
---
# شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>بتی فربه سرین لاغر میان است</p></div>
<div class="m2"><p>چه می گویم میانش بی نشان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن تا بر لبش ناید ندانند</p></div>
<div class="m2"><p>که در اصل آن شکر لب را دهان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لبش را چون توانم کرد نسبت</p></div>
<div class="m2"><p>به شیرینی که شیرین تر زجان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسیم لطف طبعش را چه گویم</p></div>
<div class="m2"><p>که با باد سحرگه هم عنان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمال عالم آرایش به خوبی</p></div>
<div class="m2"><p>نمودار بهشت جاودان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمان جز بر مراد او نگردد</p></div>
<div class="m2"><p>از این جا نادر دور زمان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر چه بس سبک روح است لیکن</p></div>
<div class="m2"><p>غم او بر دلم بار گران است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا سودای او در سر چو آتش</p></div>
<div class="m2"><p>که افتد در حریر و پرنیان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی یارم گذر کردن به کویش</p></div>
<div class="m2"><p>که سیلاب از کنارم تا میان است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خبر داری ز آتش دان گردون</p></div>
<div class="m2"><p>تنور سینه ی من همچنان است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پری دیده ست پنداری نزاری</p></div>
<div class="m2"><p>چنین با آدمی بیگانه ز آن است</p></div></div>