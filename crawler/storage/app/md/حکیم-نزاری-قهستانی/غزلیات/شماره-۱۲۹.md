---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>باد از طرفِ شمال برخاست</p></div>
<div class="m2"><p>معشوقه مشک خال برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشفته سهیلِ نیم خوابش</p></div>
<div class="m2"><p>ناگه ز خمِ هلال برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز آرزویِ صبوح کردش</p></div>
<div class="m2"><p>بازش هوسِ زلال برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سینه عودِ سوزناکش</p></div>
<div class="m2"><p>فریاد زگوش مال برخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیتی دو سه در بدیههء فکر</p></div>
<div class="m2"><p>ز آن طبعِ شکر مقال برخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باد از رخِ گل نقاب برداشت</p></div>
<div class="m2"><p>بلبل ز پیِ وصال برخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیرامن آب سبزه بنشست</p></div>
<div class="m2"><p>از طرفِ چمن نهال برخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خرّم تنِ آن که از پیِ عشق</p></div>
<div class="m2"><p>با صبحِ خجسته فال برخاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چندین شر و شور اهلِ دل را</p></div>
<div class="m2"><p>از فتنهء جاه و مال برخاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سوداست همه جهان و سودا</p></div>
<div class="m2"><p>از وسوسهء خیال برخاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در دستِ هوا زبون نزاری</p></div>
<div class="m2"><p>نتوان زِ سرِ محال برخاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زاهد که به پایِ چنگ بنشست</p></div>
<div class="m2"><p>از معرضِ احتمال برخاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنجا که اسیرِ عشق بنشست</p></div>
<div class="m2"><p>سلطان به صفِ نعال برخاست</p></div></div>