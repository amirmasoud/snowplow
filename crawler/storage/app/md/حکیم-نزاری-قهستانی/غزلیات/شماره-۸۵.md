---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>قیامت است سفر کردن از دیار حبیب</p></div>
<div class="m2"><p>مرا همیشه قضا را قیامت است نصیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ناز خفته چه داند که دردمند فراق</p></div>
<div class="m2"><p>به شب چه می گذراند علی الخصوص غریب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به قهر می روم و نیست آن مجال که باز</p></div>
<div class="m2"><p>به شهر دوست قدم در نهم ز دست رقیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پدر به صبر نمودن مبالغت می کرد</p></div>
<div class="m2"><p>که ای پسر بس از این روزگار بی ترتیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جواب گفتم از این ماجرا بس ای بابا</p></div>
<div class="m2"><p>که درد ما نپذیرد دوا به جهد طبیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدار توبه توقع ز من که در مسجد</p></div>
<div class="m2"><p>سماع چنگ تامّل کنم نه وعظ خطیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مکتب از چه فرستادی ام نکو ناید</p></div>
<div class="m2"><p>گرفت ناخن چنگی به ضرب چوب ادیب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هنوز بوی محبت زخاکم آید اگر</p></div>
<div class="m2"><p>جدا شود به لحد بند بندم از ترکیب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به اختیار نزاری سفر نکرد آری</p></div>
<div class="m2"><p>ستم غریب نباشد ز روزگار غریب</p></div></div>