---
title: >-
    شمارهٔ ۱۱۲۴
---
# شمارهٔ ۱۱۲۴

<div class="b" id="bn1"><div class="m1"><p>رفتی و صیدِ خاطر احباب کرده‌ای</p></div>
<div class="m2"><p>قلّاب شوق ر دلِ اصحاب کرده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل برده‌ای و تاختن آورده‌ای به جان</p></div>
<div class="m2"><p>آهسته‌تر که زهره ی ما آب کرده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس زاهدانِ خشک که در بحرِ زهدشان</p></div>
<div class="m2"><p>کشتی ز ره ببرده و غرقاب کرده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس ساکنانِ کنج خرابات را که باز</p></div>
<div class="m2"><p>خلوت‌نشین گوشه ی محراب کرده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب‌ها که ما ز شوقِ تو تا روز کرده‌ایم</p></div>
<div class="m2"><p>تو همچو بختِ خفته ی ما خواب کرده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را که یک دقیقه نصیب است ز آفتاب</p></div>
<div class="m2"><p>ساعت شناس‌تر ز سترلاب کرده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خصمی مسلّط است که بر ما گماشتی</p></div>
<div class="m2"><p>گفتم مگر رقیبی بوّاب کرده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نی عقل و نی فراغت و نی دل نزاریا</p></div>
<div class="m2"><p>اثبات عاشقی به چه اسباب کرده‌ای</p></div></div>