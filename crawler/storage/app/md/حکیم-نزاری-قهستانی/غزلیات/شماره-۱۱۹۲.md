---
title: >-
    شمارهٔ ۱۱۹۲
---
# شمارهٔ ۱۱۹۲

<div class="b" id="bn1"><div class="m1"><p>ختنی جمالی ای جام حبشی چه نام داری</p></div>
<div class="m2"><p>بجز از خطی و خالی ز حبش کدام داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ختنی است رنگ رویت حبشی است رنگ مویت</p></div>
<div class="m2"><p>به میان این دو کشور به کجا مقام داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حبشی سفید نبود ختنی نمک ندارد</p></div>
<div class="m2"><p>تو سفیدی و به غایت نمک تمام داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حبشی منم که بر تن همه سوخته ست جانم</p></div>
<div class="m2"><p>ختنی تویی که بر تن همه سیم خام داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن از حبش رها کن علم از ختن برآور</p></div>
<div class="m2"><p>که هزار چون نزاری حبشی غلام داری</p></div></div>