---
title: >-
    شمارهٔ ۱۸۱
---
# شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>نتیجه ای که زافکار نیم جان من است</p></div>
<div class="m2"><p>وبال چشم و دماغ و تن و توان من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به روزنامه ی سودای من چنین منگر</p></div>
<div class="m2"><p>مداد او همه از مغز استخوان من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روانیِ سخن از سرعت تفکّر نیست</p></div>
<div class="m2"><p>که قطره قطره ی خون از رگ روان من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عذوبت سخن ازآب سیل چشمم خاست</p></div>
<div class="m2"><p>مداد سوخته از خامه ی روان من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مراد من ز سخن طمطراق نیست بلی</p></div>
<div class="m2"><p>غرض تسلیِ مرغ ِدلِ تپانِ من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب مگر سر من در سر زبان نشود</p></div>
<div class="m2"><p>یقین نه ام که چنین است در گمان من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن که می شود از شیوه ی سخن معلوم</p></div>
<div class="m2"><p>که آفت سرمن در سر زبان من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین لطیف و گوارنده میوه ی سخنی</p></div>
<div class="m2"><p>غذای اهل دل است و بلای جان من است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به کنج سینه ی من گنج شایگان و ازو</p></div>
<div class="m2"><p>رسد به منفعتی هر کس و زیان من است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به شب چو خواب نمی یابم از هجوم خیال</p></div>
<div class="m2"><p>زحل قیاس گرفتم که پاسبان من است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به روز چون سرخودنیستم چنان پندار</p></div>
<div class="m2"><p>که زهره ی طرب انگیز میزبان من است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شنیده ای که شود در سرکسان سودا</p></div>
<div class="m2"><p>سری که در سر سودا شده است آن من است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کنون که در سر سودای فکر کردم سر</p></div>
<div class="m2"><p>چه سود اگر سر گردون برآستان من است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>امان نمیدهدم یک زمان تکلف عشق</p></div>
<div class="m2"><p>مگر خود این همه تکلیف در زمان من است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کمانِ ابروی خوبان کشیدمی وقتی</p></div>
<div class="m2"><p>کنون به قوت بازوی من کمان من است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جواهری که کمر وار برمیان بستم</p></div>
<div class="m2"><p>نثار کرده ی چشم گهرفشان من است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سواد کز پی شَعر نغوله شان کردم</p></div>
<div class="m2"><p>سیاه کرده ی انفاس پر دخان من است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسی به وصف لب لعل کانِ جان کندم</p></div>
<div class="m2"><p>هنوز عادت جان کندن امتحان من است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ازین سپس من و ترتیب مسکرات الوجد</p></div>
<div class="m2"><p>که گنج خانه ی سرّ دل نهان من است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر تفرّج مجنون به نجد بود اکنون</p></div>
<div class="m2"><p>تفرجی ست که در وجد داستان من است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خبرز نام و نشان درمقام وجدم نیست</p></div>
<div class="m2"><p>همین که نام نزاری برد نشان من است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>غم وجود و عدم نیست هر چه بود و نبود</p></div>
<div class="m2"><p>که داند آن که چه در سرّ کن فکان من است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به هرزه هم متلاشی نمی تواند شد</p></div>
<div class="m2"><p>وجود من که ز جود خدایگان من است</p></div></div>