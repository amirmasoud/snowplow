---
title: >-
    شمارهٔ ۳۰۵
---
# شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>عقل اگر گوید به وصل عشق حاجتمند نیست</p></div>
<div class="m2"><p>راست میگوید که ضدّان را به هم پیوند نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بندگی کن تا بود در حضرت عشقت قبول</p></div>
<div class="m2"><p>پادشاهان را به استحقاق خویشاوند نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امر و نهی عشق جاویدست در ملک وجود</p></div>
<div class="m2"><p>طمطراق عقل حالا بیش روزی چند نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوست چون از در درآمد خانه خالی شد ز غیر</p></div>
<div class="m2"><p>خانه ی دل غیر جای خلوت دلبند نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر شدم شوریده ی زنجیره ی زلفین دوست</p></div>
<div class="m2"><p>بند فرماییدش آن را کش قبول پند نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پالهنگ شوق باید گردن مشتاق را</p></div>
<div class="m2"><p>آهنی بر پای نادانی نهند این بند نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین شکر نی کز زمین قهستان برخاسته ست</p></div>
<div class="m2"><p>خوب تر در مصر اگر انصاف خواهی قند نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>الحق از جان هیچ شیرین تر بود شیرین تر است</p></div>
<div class="m2"><p>خود لبش میگوید آنک حاجت سوگند نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون نزاری تشنه ای وز چشمه های چشم سر</p></div>
<div class="m2"><p>در میان بحر غرقاب است و هم خرسند نیست</p></div></div>