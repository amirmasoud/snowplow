---
title: >-
    شمارهٔ ۱۹۵
---
# شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>با تو ما را اتّصالِ جانی است</p></div>
<div class="m2"><p>وین حدیث از عالمِ روحانی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل گفت او از کجا تو از کجا</p></div>
<div class="m2"><p>من چه دانم قدرتِ ربّانی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز به چینِ زلفِ تو اقرار من</p></div>
<div class="m2"><p>هر چه دانم غایت نادانی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز به سر گردیدن اندر پایِ تو</p></div>
<div class="m2"><p>هر چه دیگر هست سر گردانی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خویشتن بین گو مکن دعوی که کار</p></div>
<div class="m2"><p>نه به زورِ پنجه و پیشانی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر کسی دستی کند در خونِ ما</p></div>
<div class="m2"><p>زودیت خواهند کو قربانی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ضیافت گاهِ قتّالانِ عشق</p></div>
<div class="m2"><p>جمله جانِ عارفان سر خوانی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دینِ ما روشن به کفرِ زلفِ تست</p></div>
<div class="m2"><p>روزِ روشن در شبِ ظلمانی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست در فردوسِ اعلا چون تو حور</p></div>
<div class="m2"><p>وین بلاغت نیز هم انسانی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیش از این در وصفِ تو ادراک نیست</p></div>
<div class="m2"><p>غایتِ امکان چو بی امکانی است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خویشتن بینان مگر پنداشتند</p></div>
<div class="m2"><p>کاقتضایِ عاشقی کسلانی است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ازشتر مستی در آموز ای پسر</p></div>
<div class="m2"><p>خویشتن پرور خرِ کهدانی است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می کشد بار اشتر و در بی خودی</p></div>
<div class="m2"><p>فارغ از دشواری و آسانی است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون ندارد چاره یی در دستِ دوست</p></div>
<div class="m2"><p>محو شد وین هم ز بی درمانی است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دست گیری کو که عقلِ پای مرد</p></div>
<div class="m2"><p>چو نظر در معرضِ حیرانی است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کف ندارم خالی از جامِ الست</p></div>
<div class="m2"><p>گرچه می در جامِ جان پنهانی است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گوش بر قالوا بلی بنهاده ایم</p></div>
<div class="m2"><p>گرچه کارِ چشم خون افشانی است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیست چون معلول معلولان راه</p></div>
<div class="m2"><p>آری آری گوهرِ ما کانی است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>درّ این اسرار پنداری مگر</p></div>
<div class="m2"><p>ریزه ریزه گوهرِ عمّانی است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نی که در جنبش نباشد آفتاب</p></div>
<div class="m2"><p>ذرّه یی با آن که چون نورانی است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شیر رز را گروهی اهلِ دل</p></div>
<div class="m2"><p>گفته اند آری که روح ثانی است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جانِ جان است او فرو ریزش به حلق</p></div>
<div class="m2"><p>گربه حجّت بشنوی برهانی است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>روشن است اینک دلیلی روشن است</p></div>
<div class="m2"><p>جانِ یاران است و یارِ جانی است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر سخن هایِ نزاری جان بده</p></div>
<div class="m2"><p>جانِ شیرین هم به جان ارزانی است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>توگران باریِ جان او مبین</p></div>
<div class="m2"><p>نا نپنداری بدین ارزانی است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مسکرات الوجد چیزی دیگرست</p></div>
<div class="m2"><p>آن برون زین عالمِ حیوانی است</p></div></div>