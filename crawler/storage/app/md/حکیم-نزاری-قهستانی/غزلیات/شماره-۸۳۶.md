---
title: >-
    شمارهٔ ۸۳۶
---
# شمارهٔ ۸۳۶

<div class="b" id="bn1"><div class="m1"><p>روزها شد که برفتی و به خدمت نرسیدم</p></div>
<div class="m2"><p>هیچ کافر مَکَشاد آن چه من از هجر کشیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه نویسم که چه آمد بر سرم تا تو برفتی</p></div>
<div class="m2"><p>چه ملامت که نبردم چه قیامت که ندیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتابی تو و چون ذرّه سرآسیمه بماندم</p></div>
<div class="m2"><p>در پَی ات بس که بلافایده چون سایه دویدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کشیدم ز همه خلقِ جهان سر به خجالت</p></div>
<div class="m2"><p>که به دعوی زهمه خلق جهانت بگزیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لاجرم هر که به من می رسد انگشتِ ملامت</p></div>
<div class="m2"><p>می کشد در من ازین زهرِ ملامت که چشیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم چنان در سرم آشوبِ تمنّایِ وصال است</p></div>
<div class="m2"><p>تا نگویی که به کلّی ز تو امیّد بریدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مِهرِ دیرینه محال است که ازجان به در آید</p></div>
<div class="m2"><p>سخنِ معتبرست این مثل از هر که شنیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرگز اندیشه نکردم ز سرِ دستِ چو سیمت</p></div>
<div class="m2"><p>که به غیرت سرِ انگشتِ تحیّر نگزیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاقبت رفتی و پیوند بریدی ز نزاری</p></div>
<div class="m2"><p>من هم از اوّلِ عهد آخرِ این کار بدیدم</p></div></div>