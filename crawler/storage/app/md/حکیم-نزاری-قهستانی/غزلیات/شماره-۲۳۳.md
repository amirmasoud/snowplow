---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>نور رخ تو صاف‌تر از چشمهٔ خورست</p></div>
<div class="m2"><p>خاک در تو پاک‌تر از حوض کوثر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هیچ بوستان نبود چون قد تو سرو</p></div>
<div class="m2"><p>ور هست در زمانه مگر سرو کشمرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا رب بهشت تازه بود همچو روی دوست</p></div>
<div class="m2"><p>نادیده روی دوست یقینم که خوش‌ترست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مقصود من تویی ز بهشت ای بهشت روی</p></div>
<div class="m2"><p>بی روی خرم تو بهشتم چه در خورست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون دوست حاضر است به شمع احتیاج نیست</p></div>
<div class="m2"><p>آنجا که روی دوست بود شب منور است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشقت چو جان من بستد مهر برگرفت</p></div>
<div class="m2"><p>با جان مهرپرور من مهرپرور است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرمان عشق را چو قضا حکم نافذست</p></div>
<div class="m2"><p>لا بلکه عشق را ز قضا حکم برترست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر عمر حجتی که دبیر قضا نبشت</p></div>
<div class="m2"><p>گر بی‌نشان عشق بود هم مزور است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما را قضای عشق تو باری به سر رسید</p></div>
<div class="m2"><p>تا خود پس از نوشتهٔ عشقم چه بر سرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آری هزار جان نزاری فدای دوست</p></div>
<div class="m2"><p>جانی که خود به عشق دهی کار دیگر است</p></div></div>