---
title: >-
    شمارهٔ ۱۳۶۷
---
# شمارهٔ ۱۳۶۷

<div class="b" id="bn1"><div class="m1"><p>نه قرار داده بودی که شبی به خلوت آیی</p></div>
<div class="m2"><p>بگذشت روزگاری و نیامدی کجایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به وصال وعده کردی و دلی که بود ما را</p></div>
<div class="m2"><p>به امید در تو بستیم و دری نمی‌گشایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سرت که تا به رویت نظری ربوده کردم</p></div>
<div class="m2"><p>ز دو چشمِ بی‌قرارم بنرفت روشنایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چه خود نگاه دارم که نباشد اختیارم</p></div>
<div class="m2"><p>که تو آدمی به یک بار ز خود نمی‌ربایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگرت ندیده بودم به صفت شنیده بودم</p></div>
<div class="m2"><p>که دلِ من از تو می‌داد نشانِ آشنایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خرابه ی فقیران نفسی درآی روزی</p></div>
<div class="m2"><p>بنشین حکایتی کن که حیات می‌فزایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خلافِ دوستانی و به زعمِ دشمنانی</p></div>
<div class="m2"><p>که به حُسن بی‌نظیری و به عهد بی‌وفایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو خود از نزاری خود که ترا رسد نپرسی</p></div>
<div class="m2"><p>نه مکن که عیب باشد که به دوستان نشایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کم از آن که آشنایی به سلامِ ما فرستی</p></div>
<div class="m2"><p>اگرت مجال آن نیست که خویشتن بیایی</p></div></div>