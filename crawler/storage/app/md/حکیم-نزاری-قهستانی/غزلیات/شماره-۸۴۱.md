---
title: >-
    شمارهٔ ۸۴۱
---
# شمارهٔ ۸۴۱

<div class="b" id="bn1"><div class="m1"><p>ترا به جان و دل از جان و دل وفادارم</p></div>
<div class="m2"><p>که من خود از همه ملکِ جهان ترا دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی به جایِ تو باشد مرا چه می گویم</p></div>
<div class="m2"><p>نعوذبالله اگر هرگز این روا دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرم به تیغ بباید برید اگر به خطا</p></div>
<div class="m2"><p>ز طوقِ عهدِ تو گردن دمی جدا دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به آرزویِ دلم در نمی شود چه کنم</p></div>
<div class="m2"><p>ز پای بوسِ تو دستی که بر دعا دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیال را بفرست ار تو خود نمی آیی</p></div>
<div class="m2"><p>که با خیالِ تو صد گونه ماجرا دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دست بنده دعایی بود خدا داناست</p></div>
<div class="m2"><p>که روز و شب به دعا دست برخدا دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجب دلیست مرا در وفا چنان یکتا</p></div>
<div class="m2"><p>چو سرو اگرچه که قامت ز دل دوتا دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به رستخیز که فرزند را وفا نکنند</p></div>
<div class="m2"><p>به جست و جویِ تو جان بر میان وفا دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روا ندارم اگر دیده در جهان نگرد</p></div>
<div class="m2"><p>که از خیالِ تو آنی نظر جدا دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بریز خونِ نزاری که دولتی ست مرا</p></div>
<div class="m2"><p>که درعوض چو تویی را به خون بها دارم</p></div></div>