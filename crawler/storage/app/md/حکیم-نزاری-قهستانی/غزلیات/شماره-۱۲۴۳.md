---
title: >-
    شمارهٔ ۱۲۴۳
---
# شمارهٔ ۱۲۴۳

<div class="b" id="bn1"><div class="m1"><p>بر ما به گناهی که نکردیم نگیری</p></div>
<div class="m2"><p>ور نیز بکردیم شفاعت بپذیری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این قاعدۀ اهل کرم نیست که احباب</p></div>
<div class="m2"><p>از پای درآیند و توشان دست نگیری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دست رقیبم که بمیراد به خواری</p></div>
<div class="m2"><p>مگذار که کافر ببردمان به اسیری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از باطنِ مجروح گرفتم خبرت نیست</p></div>
<div class="m2"><p>در ظاهر آخر نظری کن که بصیری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرهاد به سنگی بر اگر صورت شیرین</p></div>
<div class="m2"><p>کرده ست چه باشد تو مرا نقش ضمیری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماهی، ملکی، حور بهشتی، چه وجودی</p></div>
<div class="m2"><p>مثل تو ندیدم به چه گویم که نظیری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاقل نکند عیب جوان را که اگر شیخ</p></div>
<div class="m2"><p>این روی ببیند بکند توبه ز پیری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگذار که دیوانه و شوریده و مستم</p></div>
<div class="m2"><p>مه نام و مه نسبت چه بزرگی چه امیری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوی گل و آتش به مشامت نرسیده ست</p></div>
<div class="m2"><p>گویی که چنین هم نفس دود عبیری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا دل نسپاری به کسی هم چو نزاری</p></div>
<div class="m2"><p>هرگز نشوی زنده به جانی که نمیری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گو خلق بکن سرزنش و عیب تو می باش</p></div>
<div class="m2"><p>در پای من ای خار که خوش تر ز حریری</p></div></div>