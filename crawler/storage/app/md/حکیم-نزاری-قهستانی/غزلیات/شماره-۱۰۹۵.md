---
title: >-
    شمارهٔ ۱۰۹۵
---
# شمارهٔ ۱۰۹۵

<div class="b" id="bn1"><div class="m1"><p>زین پیش اگر مجنون دیوانه بُد اکنون که</p></div>
<div class="m2"><p>از شبنم عشقِ او یک قطره و جیحون که</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجنون همه ی لیلی لیلی همه ی مجنون</p></div>
<div class="m2"><p>این جا نه دویی باشد لیلی چه و مجنون که</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون موج برانگیزد چون اسب برون تازد</p></div>
<div class="m2"><p>در معرض این و آن دریا چه و هامون که</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانم که نمی‌داند مفروغ ز مستأنف</p></div>
<div class="m2"><p>گر معرفتی داری مافوق که مادون که</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک جوهرِ فرد آمد در عالم کثرت عشق</p></div>
<div class="m2"><p>بر هم زدگان دانند افسرده که مجنون که</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیرینه حکایت‌ها با اوست نزاری را</p></div>
<div class="m2"><p>تا خود چه کند عرضه بر رایِ همایون که</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خونِ رز و خونِ دل تا چند خوری هر دو</p></div>
<div class="m2"><p>تا خود که برون آید از عهده ی این خون که</p></div></div>