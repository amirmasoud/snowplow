---
title: >-
    شمارهٔ ۱۰۶۰
---
# شمارهٔ ۱۰۶۰

<div class="b" id="bn1"><div class="m1"><p>اگر در عالمِ غیبم دهی راه</p></div>
<div class="m2"><p>شود بر من زبانِ خلق کوتاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جزین کز خود برون آری تمامم</p></div>
<div class="m2"><p>بحمدالله ندارم هیچ دل خواه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا از من اگر وا می ستانی</p></div>
<div class="m2"><p>تو می مانی و بس الحمدلله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درآ تا من شوم از خانه بیرون</p></div>
<div class="m2"><p>گدایی را نزیبد منصبِ شاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جز بر آستانِ دل ستانم</p></div>
<div class="m2"><p>نمی خواهم محلّ و منصب و جاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه جایِ جاه و منصب پیر زالی</p></div>
<div class="m2"><p>دو عالم را بسوزاند به یک آه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز مبدا فطرتی دارند هر کس</p></div>
<div class="m2"><p>که کس از فطرتِ خود نیست آگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جنبِ مهرِ یارِ مهربانم</p></div>
<div class="m2"><p>پشیزی بر نیاید مهر تا ماه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خود نتوان سپردن ره به مقصد</p></div>
<div class="m2"><p>دلالت باید ای خودبین درین راه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترا هر دیده کز عینِ یقین دید</p></div>
<div class="m2"><p>در آن دیده نگنجد شکّ و اشباه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه هر چ از تو می آید فتوح است</p></div>
<div class="m2"><p>در آیینِ نزاری نیست اکراه</p></div></div>