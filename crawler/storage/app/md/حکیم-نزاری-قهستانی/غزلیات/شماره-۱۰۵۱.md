---
title: >-
    شمارهٔ ۱۰۵۱
---
# شمارهٔ ۱۰۵۱

<div class="b" id="bn1"><div class="m1"><p>بیا که جانِ منی جانِ من بیا بمرو</p></div>
<div class="m2"><p>مکن به کامِ رقیبان مکن مرا بمرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر رضایِ تو در کشتنِ من است بکش</p></div>
<div class="m2"><p>رضا رضایِ تو دارم بدین رضا بمرو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر قفا که دهی شاکرم هلا باز آی</p></div>
<div class="m2"><p>به هر جفا که کنی راضی ام بیا بمرو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سوزِ سینه ی من مبتلا شوی بنشین</p></div>
<div class="m2"><p>روا مدار بترس آخر از خدا بمرو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر به مصلحت از دستِ من بخواهی رفت</p></div>
<div class="m2"><p>به رغمِ مدّعیان از سرِ وفا بمرو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر آن جفا که ز بیگانه می رود بر من</p></div>
<div class="m2"><p>رواست گو ز دلم داغِ آشنا بمرو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر از سرِ تو برفت آن که دست گیرت بود</p></div>
<div class="m2"><p>تو باری از سرِ پیمان نزاریا بمرو</p></div></div>