---
title: >-
    شمارهٔ ۴۵۲
---
# شمارهٔ ۴۵۲

<div class="b" id="bn1"><div class="m1"><p>تا عشق مرا گردِ خرابات برآورد</p></div>
<div class="m2"><p>از مسجد و محراب و مناجات برآورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با عقل به ضدیّت و انکار و تعصّب</p></div>
<div class="m2"><p>از جیب فتاوی و سجلاّت برآورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق از غضب و نخوت و غیرت چو نهنگی</p></div>
<div class="m2"><p>در تاب شد و بانگ به هیهات برآورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آتشِ غیرت که بر افروخت به گردون</p></div>
<div class="m2"><p>دودِ شغب از خرمنِ طاعات برآورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما پس روِ امریم نه غالی نه مقصّر</p></div>
<div class="m2"><p>قاصر نظر آشوب ز علّات برآورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون یک سرِ مویش خبر از صدق و صفا نیست</p></div>
<div class="m2"><p>سد چلّه گرفتم ز کرامات برآورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشغول به خود کرد مرا یارم و بی خود</p></div>
<div class="m2"><p>حاجات روا کرد و مهمّات برآورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در پایِ غمش هرکه لگد کوبِ عنا شد</p></div>
<div class="m2"><p>از خاکِ درش سر به سماوات برآورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا عقل بگسترد بساطِ لمن الملک</p></div>
<div class="m2"><p>پنداشت که دستی به مباهات برآورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق آمد و تا مهره فرو چید و فرو کرد</p></div>
<div class="m2"><p>در حال بزد نعره و شهمات برآورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یک نکته بیان کرد ز تسلیم و تسلّم</p></div>
<div class="m2"><p>فریاد ز اربابِ مقالات برآورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کز خودیِ خویش به در شد چو نزاری</p></div>
<div class="m2"><p>یک باره دمار از هُبل ولات برآورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>استادِ حقیقی به سرِ سوزنِ تعلیم</p></div>
<div class="m2"><p>از پایِ دلم خار محالات برآورد</p></div></div>