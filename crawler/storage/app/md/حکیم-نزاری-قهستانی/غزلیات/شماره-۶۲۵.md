---
title: >-
    شمارهٔ ۶۲۵
---
# شمارهٔ ۶۲۵

<div class="b" id="bn1"><div class="m1"><p>زان پیش کآفتاب ز مشرق کند ظهور</p></div>
<div class="m2"><p>آثار فیض حق متجلّی شود ز طور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ساقی‌ای که رشک برند و خجل شوند</p></div>
<div class="m2"><p>از قامت تو طوبی و از طلعت تو حور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآن آب ده کز آتش طبعش به خاصیت</p></div>
<div class="m2"><p>سقف دماغ پر کند از دودهٔ بخور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر آتشم زن آبی کز عکس برق او</p></div>
<div class="m2"><p>گردد سواد سینهٔ مخمور غرق نور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا من به کام دل غزل حسب حال خویش</p></div>
<div class="m2"><p>اینجا ادا کنم چو سرایندهٔ زبور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد خاطرم به تربیت عاقلان نفور</p></div>
<div class="m2"><p>دیوانه می‌شوم ز محال دل صبور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ناصحان مصلحت اندیش دوربین</p></div>
<div class="m2"><p>نزدیک شد کز اسلام افتم به کفر دور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از زاهدان عهد نیاموختم مگر</p></div>
<div class="m2"><p>فسق و فساد و فحش و فسون و فن و فجور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باری نصیحتی نکنندم جماعتی</p></div>
<div class="m2"><p>کافتاده‌اند و بوده چو من در چنین فتور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من در قیامتم ز ملامت‌گران عشق</p></div>
<div class="m2"><p>کو در دمید قصّهٔ من در جهان چو صور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا چند خاطر از می و مطرب فریفتن</p></div>
<div class="m2"><p>بر بوی آنک خیر بود آخر الامور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون اهل روزگار نی‌ام سخرهٔ مجاز</p></div>
<div class="m2"><p>نه راغب حواری و نه طالب قصور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آزادم از بهشت اضافی و فارغم</p></div>
<div class="m2"><p>از هر که دل فریفته گردد بدین غرور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر دوزخ و بهشت ندانی نزاریا</p></div>
<div class="m2"><p>تأویل آن زمن بشنو غیبت و حضور</p></div></div>