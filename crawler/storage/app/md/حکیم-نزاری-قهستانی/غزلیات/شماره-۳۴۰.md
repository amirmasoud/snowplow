---
title: >-
    شمارهٔ ۳۴۰
---
# شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>بر دست چون بود می و صبح و کنار کشت</p></div>
<div class="m2"><p>با دوستان همدم و یاران خوش جهشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بشنوی وگرنه به احکام معنوی</p></div>
<div class="m2"><p>اینک کنار کوثر و اینک در بهشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با جاهلان معامله در بحث معرفت</p></div>
<div class="m2"><p>باشد چنان که بر سر جیحون زنند خشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرم وجود ساقی شب خیز خوش نشین</p></div>
<div class="m2"><p>کز باقی شبانه به ما باقیی بهشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماییم و احتمال ملامت گر و وبال</p></div>
<div class="m2"><p>نیکو خصال را چه تعلق به بد سرشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر حق نکو کند به جهودان خیبری</p></div>
<div class="m2"><p>در اختصاص کعبه ی مطلق شود کنشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر عفو غافرست معوّل نه بر عمل</p></div>
<div class="m2"><p>نزدیک ما چه خیر و چه شرّ و چه خوب و زشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این نام بس مرا که خریدار یوسفم</p></div>
<div class="m2"><p>من پای مرد دارم و آن زال دست رشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آزاد کرده ایست نزاری ز ابتدا</p></div>
<div class="m2"><p>وین شرط نامه کاتبِ وحی از ازل نوشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز پهلوی نشاید و تسلیم عشق را</p></div>
<div class="m2"><p>در خورد پهلوی نبود مردم وبشت</p></div></div>