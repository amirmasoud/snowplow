---
title: >-
    شمارهٔ ۱۷۹
---
# شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>بده بیار که می مایه ی حیات من است</p></div>
<div class="m2"><p>ز بدو خلقت من متصل به ذات من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به وقت صبح درآیم زخواب و برخیزم</p></div>
<div class="m2"><p>به بانگ چنگ که قدقامت الصلات من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نزد هر کس از آنجا که ذات ناقص اوست</p></div>
<div class="m2"><p>هنوز زندقه از حیز صفات من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجال پس روی پیر عقل نیست که عشق</p></div>
<div class="m2"><p>فرو گرفته حوالی شش جهات من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گمان برم به خلاص از قوای نفسانی</p></div>
<div class="m2"><p>که نیست ممکن و این هم ز ممکنات من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهم گشاده شوم چون نفس فرو بندد</p></div>
<div class="m2"><p>عجب مدار که هم بند من نجات من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن کرا نکند با مقلدان گفتن</p></div>
<div class="m2"><p>هرآنچه گفته ام از محض ترهّات من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه می کنند ملامت مرا گناه از کیست</p></div>
<div class="m2"><p>شکال پای من از عقل بی ثبات من است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بس که برنظرم جلوه می کند اصنام</p></div>
<div class="m2"><p>اگر قبول کنی کعبه سومنات من است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو من به من همه هیچ ام چو با توام همه تو</p></div>
<div class="m2"><p>به حکم الا الله لا اله لات من است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به خواب دیده ام ای دوست در حلال و حرام</p></div>
<div class="m2"><p>که فاسقات به امر تو صالحات من است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا چه غم که معاند ترش نشیند و تلخ</p></div>
<div class="m2"><p>که زهر طعنه صاحب غرض نبات من است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حلاوت سخن من کجا نبات کجا</p></div>
<div class="m2"><p>که شور بر همه عالم ز مسکرات من است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز اسب نیک و بد خود چنان پیاده شوم</p></div>
<div class="m2"><p>که پیل مست بر این نطع شاه مات من است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گهی زباران رشک ارس بدی چشمم</p></div>
<div class="m2"><p>کنون زگریه ی وافر ارس فرات من است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عوام را به نزاری از آن تعلّق نیست</p></div>
<div class="m2"><p>که نام هستی او در مسلمات من است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من از نتیجه آدم نیم که فطرت من</p></div>
<div class="m2"><p>برون ز فطرت آبا و امهات من است</p></div></div>