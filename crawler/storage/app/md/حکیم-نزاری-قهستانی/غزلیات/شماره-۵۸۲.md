---
title: >-
    شمارهٔ ۵۸۲
---
# شمارهٔ ۵۸۲

<div class="b" id="bn1"><div class="m1"><p>آن بر شکسته از ما باشد که باز آید</p></div>
<div class="m2"><p>این جا دری گشاید آن جا رهی نماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما منتظر نشسته برخاسته قیامت</p></div>
<div class="m2"><p>روزی که بار باشد بر ما که در گشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هی هی چه گفته آخر با دوست در حضورم</p></div>
<div class="m2"><p>آن جا که دوست باشد چیزی دگر نباید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دنیا و آخرت را بر هم زدیم کلّی</p></div>
<div class="m2"><p>چیزی نمانده این جا حاسد چه بر فزاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تسلیم شو محقّ را پرهیز کن ز مبطل</p></div>
<div class="m2"><p>بی گانه چون برن شد خانه کیا درآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این کار را بباید گُردی و پهلوانی</p></div>
<div class="m2"><p>نازک مزاجِ بد دل تسلیم را نشاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منکر مباش چندین گر در مقامِ وحدت</p></div>
<div class="m2"><p>دوشیزه یی چو مریم روح اللّهی بزاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نَبوَد شگفت اگر چه بد گو نکو نگوید</p></div>
<div class="m2"><p>عادت بود که عقرب بر سنگ می گزاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شوریدنِ نزاری چون بلبل است عمدا</p></div>
<div class="m2"><p>کز فرطِ شوق چندی بر گل بُنی سراید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دو کَون در برابر یک ذرّه یی نسنجد</p></div>
<div class="m2"><p>آن جا که غمزۀ او ما را ز ما رباید</p></div></div>