---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>از دست بدادم دل شوریده خود را</p></div>
<div class="m2"><p>بر هم زدم احوال بشولیده خود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دوست به پرسیدن من رنجه کند پای</p></div>
<div class="m2"><p>در هر قدمی پیش کشم دیده خود را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دوست میازار دلم را و مینداز</p></div>
<div class="m2"><p>در پای جفا هم دم بگزیده خود را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لا یلتفتی کردن و بر دوست شکستن</p></div>
<div class="m2"><p>نادیده مکن دیده من دیده خود را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس تربیتی باشد و اعزازی و لطفی</p></div>
<div class="m2"><p>گر یاد کند یار نپرسیده خود را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم گوشه چشمی به عنایت سوی ما کن</p></div>
<div class="m2"><p>ضایع نگذارند پسندیده خود را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا خاک درت گل شود از خون نزاری</p></div>
<div class="m2"><p>خون بیش نده خاک نگردیده خود را</p></div></div>