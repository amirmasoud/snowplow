---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>چو عکس لعل تو بر دیده بگذرد ما را</p></div>
<div class="m2"><p>خیال خال تو در حالت آورد ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرامت می لعلت به معجزات خیال</p></div>
<div class="m2"><p>نکرده چاشنیی هوش می برد ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر به صیقل جام وصال ساقی عهد</p></div>
<div class="m2"><p>غبار از آینه ی سینه بسترد ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به غمزه ی تو نه این چشم داشتم کاخر</p></div>
<div class="m2"><p>به گوشه ی نظری باز بنگرد ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غمت مباد که بر شادی حمایت تو</p></div>
<div class="m2"><p>به جز غم تو کسی غم نمی خورد ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رقیب گفت مرا تا به کی ز بی خردی</p></div>
<div class="m2"><p>که از تهتّکّ تو پرده می درد ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جمال حور بهشت و حریم حرمت خلد</p></div>
<div class="m2"><p>رقیب را و نزاری بی خرد ما را</p></div></div>