---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>دل ز دست غم مده گر شادمانی بایدت</p></div>
<div class="m2"><p>مرد جانان باش اگر جان و جوانی بایدت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنج هجران کش ای اگر می وصل خواهی ای پسر</p></div>
<div class="m2"><p>چون زمین شو پست اگر می آسمانی بایدت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وصل جانان را به جان باید خریدن بی مکاس</p></div>
<div class="m2"><p>کی توانی یافتن تا رایگانی بایدت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اولین گامت قدم بر کام دل باید نهاد</p></div>
<div class="m2"><p>مشکلا گر کام دل در کامرانی بایدت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دو می ندهند آن جا پس همی یک رنگ شو</p></div>
<div class="m2"><p>این جهانی باز ده گر آن جهانی بایدت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دو بگذار و بمیر از پیش مرگ</p></div>
<div class="m2"><p>گر همی در زندگانی زندگانی بایدت</p></div></div>