---
title: >-
    شمارهٔ ۲۵۲
---
# شمارهٔ ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>مست آمدیم و باز چنان می‌رویم مست</p></div>
<div class="m2"><p>کز هر چه نیست بی‌خبرانیم و هر چه هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای مدّعی تو وهم‌پرستی و ما حبیب</p></div>
<div class="m2"><p>بنگر که از دوگانه کدامیم بت پرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ما مکش زبان به تعصّب ببند لب</p></div>
<div class="m2"><p>ای بی‌خبر میاز به دنبالِ مار دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشنو که مسکراتِ من از شیرهء رزست</p></div>
<div class="m2"><p>کاین مستیِ من است زخم خانهء الست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر دیگران ز شیرهء انگور سرخوش‌اند</p></div>
<div class="m2"><p>ما از شرابِ عشق چنین والهیم و مست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می را چه اعتبار به نزدیکِ ما اگر</p></div>
<div class="m2"><p>یک لحظه‌ای کند ز حرارت دماغ مست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می مونسی‌ست غم زدگان را که هم بدو</p></div>
<div class="m2"><p>یک دم توان ز محنتِ ایام باز رست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تشنیع می‌زدندی بر من که پیش ازین</p></div>
<div class="m2"><p>می‌کرد توبه باز و دگر بار می‌شکست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان می که ما به ساغرِ اَشواق می‌خوریم</p></div>
<div class="m2"><p>بر ما به ریسمان نتوانند توبه بست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن‌ها که پرورش ز میِ عشق یافتند</p></div>
<div class="m2"><p>اندر مذاق ایشان چه شهد و چه کبست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوش روزگارِ عمر عزیزان که هیچ وقت</p></div>
<div class="m2"><p>در روزگار عمر ازیشان دلی نخست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خرّم وجودِ آن‌که بشوید به آبِ عفو</p></div>
<div class="m2"><p>از ما اگر غباری بر خاطرش نشست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ما را نزاریا متواتر به وحیِ عشق</p></div>
<div class="m2"><p>از گنجِ کُنجِ خانه خود تحفه‌ای فرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با سوزِ خویش ساز که عیبی بود اگر</p></div>
<div class="m2"><p>گویند از ملامتِ افسردگان بجست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در ناقص الوجود نباشد کمالِ نفس</p></div>
<div class="m2"><p>این رمز پیشِ اهلِ حقایق مقرّرست</p></div></div>