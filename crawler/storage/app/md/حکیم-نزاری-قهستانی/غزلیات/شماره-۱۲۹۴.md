---
title: >-
    شمارهٔ ۱۲۹۴
---
# شمارهٔ ۱۲۹۴

<div class="b" id="bn1"><div class="m1"><p>خوش نیست بر طلیعه ی انوارِ بام می</p></div>
<div class="m2"><p>هست الله الله هست بیار ای غلام می</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاکیزه جوهری که به اسما مرکّب است</p></div>
<div class="m2"><p>صهبا ، رحیق ، راح ، مفرّح ، مدام ، می</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دفعِ همه غم است و دوایِ همه مرض</p></div>
<div class="m2"><p>معجونِ زندگانی مطلق کدام می</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر مست اعتراضِ خردمند شرط نیست</p></div>
<div class="m2"><p>کز پیلِ کامگار کشد انتقام می</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خُم جَهَد فقیه که پرهیز می کند</p></div>
<div class="m2"><p>گر ذوقِ عیش یابد از یک دو جام می</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقصانِ زهد و آفتِ توبه ست و عقل را</p></div>
<div class="m2"><p>فارغ کند به یک نفس از ننگ و نام می</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دانی چرا خواص به خلوت خورند از آنک</p></div>
<div class="m2"><p>حیفِ است و غبن و غصّه به دستِ عوام می</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مردم که می خورند کس از خود نیازرند</p></div>
<div class="m2"><p>ناکس خورد به عربده و ناتمام می</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز آنش محک نهند حکیمان که مست را</p></div>
<div class="m2"><p>آید پدید گوهرش از لعل فام می</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می بر کریم نیست به فتوایِ من حرام</p></div>
<div class="m2"><p>مطلق حلالِ محض بود بر کرام می</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از می گریز نیست نزاری که روزگار</p></div>
<div class="m2"><p>خونت خورد اگر نخوری بر دوام می</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می با حرام زاده مخور تا بود حلال</p></div>
<div class="m2"><p>خود بر حلال زاده نباشد حرام می</p></div></div>