---
title: >-
    شمارهٔ ۲۱۲
---
# شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>دلی که عاشق روی نگار دلبندست</p></div>
<div class="m2"><p>نه ممکن است که با صابریش پیوندست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی که او به صفت صابرست عاشق نیست</p></div>
<div class="m2"><p>به عشق و صبر نظر کن که چند در چندست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کدام عاشق صادق شنیده ای که ز هجر</p></div>
<div class="m2"><p>ز عاجزی سپرِ صابری نیفکنده ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چهارسوی نهادم ز رخت صبر تهی ست</p></div>
<div class="m2"><p>که شش جهات وجودم به عشق در بندست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز پند هیچ نیاید، نصیحتم مکنید</p></div>
<div class="m2"><p>که مرد عاشق دیوانه فارغ از پندست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمینه بنده ی اویم اگر قبول کند</p></div>
<div class="m2"><p>به هرچه خواهد و فرمان دهد خداوندست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار توبه شکستم هنوز مردم را</p></div>
<div class="m2"><p>طمع بود که مرا التفاتِ سوگندست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به مهر زن نکنم دامن دل آلوده</p></div>
<div class="m2"><p>که پایبندیِ واماندگان ز فرزندست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نزاریا ره دیوانگان عشق سپر</p></div>
<div class="m2"><p>طریق زهد ره مردم خردمند است</p></div></div>