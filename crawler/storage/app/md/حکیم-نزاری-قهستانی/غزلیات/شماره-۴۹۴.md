---
title: >-
    شمارهٔ ۴۹۴
---
# شمارهٔ ۴۹۴

<div class="b" id="bn1"><div class="m1"><p>شرابِ تلخ هنی تر ز انگبین باشد</p></div>
<div class="m2"><p>به خاصه کز کفِ سروِ سمن سرین باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه در بهشت شراب است و شاهدست اینک</p></div>
<div class="m2"><p>شراب و شاهد ازین خوب تر همین باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حریفِ مجلسِ ما بین به نقد و نسیه مگو</p></div>
<div class="m2"><p>که در بهشت تماشایِ حورِ عین باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدار چشمِ ریاضت ز ما برایِ بهشت</p></div>
<div class="m2"><p>مگر برای بهشتی که در زمین باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهشتِ مکتسبی گو خدا بدان کس ده</p></div>
<div class="m2"><p>که شکر و منّتِ او را به جان رهین باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا بس است که حالی علی المراد به نقد</p></div>
<div class="m2"><p>شرابکی مُعَد و یارکی قرین باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به صدق گفتم و دانم حسود خواهد گفت</p></div>
<div class="m2"><p>که مذهبِ حکما بر خلافِ دین باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولی مرا چه غم است از عدو که بد گوید</p></div>
<div class="m2"><p>بگوی گو روشِ عاشقان چنین باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صراطِ عشق سپردن به پایِ اعما نیست</p></div>
<div class="m2"><p>کسی نگفت که کژ دیده راست بین باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین مقام نگنجد قدم گران جان را</p></div>
<div class="m2"><p>وگر مثل به محل هم چو انگبین باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طریقِ عشق نشاید به چشمِ شک بردن</p></div>
<div class="m2"><p>مگر کسی چو نزاری علی الیقین باشد</p></div></div>