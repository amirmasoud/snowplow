---
title: >-
    شمارهٔ ۹۲۱
---
# شمارهٔ ۹۲۱

<div class="b" id="bn1"><div class="m1"><p>عیش ما داریم کز شادی و شیون فارغیم</p></div>
<div class="m2"><p>کار ما رانیم چون از کار کردن فارغیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راست چون سرویم آزاد و مجرّد لاجرم</p></div>
<div class="m2"><p>هم ز بستان ایمنیم و هم ز بهمن فارغیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواه گو آرام گیر و خواه گو بدرام باش</p></div>
<div class="m2"><p>هر چه هست از گردشِ گردونِ توسن فارغیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردن از بارِ سرِ ما جور بر جان می‌نهاد</p></div>
<div class="m2"><p>ما سر اندر باختیم از ننگِ گردن فارغیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هوایِ نورِ خورشیدِ محبّت ذرّه‌وار</p></div>
<div class="m2"><p>از مقام و جایْگاه و رکن و مسکن فارغیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفس را سر کوفتیم و عقل را بر در زدیم</p></div>
<div class="m2"><p>شکر حق را هم ز دزد و هم ز رهزن فارغیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردمان ما را به جهل از مرگ ترسانند و ما</p></div>
<div class="m2"><p>گرنه چون خضریم خصرآسا زِ مردن فارغیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یارِ ما جایی دگر دارد نشیمن سال‌هاست</p></div>
<div class="m2"><p>منّت ایزد را که از غم خانه‌ی تن فارغیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گلشن و گلخن به معنی دوزخ و خلدست و ما</p></div>
<div class="m2"><p>چون نزاری هم زِ گلشن هم زِ گلخن فارغیم</p></div></div>