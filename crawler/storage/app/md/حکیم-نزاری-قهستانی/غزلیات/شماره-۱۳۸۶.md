---
title: >-
    شمارهٔ ۱۳۸۶
---
# شمارهٔ ۱۳۸۶

<div class="b" id="bn1"><div class="m1"><p>وقت نیامد که روی باز نمایی</p></div>
<div class="m2"><p>پرده نبندی و خیمه بازگشایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یوسفِ در پرده ای و منتظرانت</p></div>
<div class="m2"><p>بر سر راه اند تا تو کی به درآیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیب نکن گر نیازمند ندارد</p></div>
<div class="m2"><p>طاقتِ دردِ فراق و داغ جدایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غایب و حاضر چه گویمت که ز پرده</p></div>
<div class="m2"><p>گر به در آیی وگرنه آفتِ مایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز نیاید به خویشتن به قیامت</p></div>
<div class="m2"><p>هر که تو او را زخویشتن بربایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گردنِ صیدی که در کمندِ تو آید</p></div>
<div class="m2"><p>چشم ندارد به هیچ روی رهایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دفع ندانند کرد و چاره اطبّا</p></div>
<div class="m2"><p>دردِ اَحبّات را که هم تو دوایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ذوقِ محبّت نیازمندِ تو داند</p></div>
<div class="m2"><p>عشق نداند که چیست مردِ هوایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا تو به درویش لقمه ای بفرستی</p></div>
<div class="m2"><p>دست برآورده ایم و سر به گدایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای شده شهری نزاریا ز تو پرشور</p></div>
<div class="m2"><p>تا به کی آخر چه فتنه ای چه بلایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مست شدی تا به روزِ حشر از این مِی</p></div>
<div class="m2"><p>باز نیایی به خود هنوز کجایی</p></div></div>