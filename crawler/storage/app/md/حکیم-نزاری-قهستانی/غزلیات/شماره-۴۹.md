---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>نظر از جانب ما کن زکات زندگانی را</p></div>
<div class="m2"><p>دلی ده باز ما را صدقة جان و جوانی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بوسی از سرم کردن توانی دفع صفرا را</p></div>
<div class="m2"><p>به بویی از دلم بردن توانی ناتوانی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه بادش گر به دلداری دمی با بی دلی داری</p></div>
<div class="m2"><p>نصیحت گوش کن عادت مکن نامهربانی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دندان می کنم دست از خلافِ وعده های تو</p></div>
<div class="m2"><p>نباشد اعتبار الحق حدیث سر زبانی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جان و دل فرود آمد بلای عشق تو ناگه</p></div>
<div class="m2"><p>نگرداند کسی از خود بلایِ آسمانی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل از من بستدی آری دل و جانم فدای تو</p></div>
<div class="m2"><p>تفاوت در دلی دادن چه حاجت یار جانی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نزاری گر به زاری خاک شد بر آستان تو</p></div>
<div class="m2"><p>نباشد اعتباری چارچوب استخوانی را</p></div></div>