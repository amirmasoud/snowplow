---
title: >-
    شمارهٔ ۲۹۵
---
# شمارهٔ ۲۹۵

<div class="b" id="bn1"><div class="m1"><p>آخر ای دوست بمردم ز غمت درمان چیست</p></div>
<div class="m2"><p>رازِ دل فاش کنم یا نکنم فرمان چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جور و بی داد مکن بیش و بترس از داور</p></div>
<div class="m2"><p>کشتۀ عشق توم خونِ مرا تاوان چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظاهرم می نگری مجتمعم می بینی</p></div>
<div class="m2"><p>بی خبر زان که ز هجران توم بر جان چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوسه یی بخش به من تا بدهم جان به عوض</p></div>
<div class="m2"><p>گرچه داند همه کس فرق ازین تا آن چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیرِ خون ریزِ مژه بر هدفِ جانم اگر</p></div>
<div class="m2"><p>نه تو انداخته ای بر جگرم پیکان چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بت پرستم به همه حال چو در بند توم</p></div>
<div class="m2"><p>پس چو کافر شدم اندیشه ام از ایمان چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر ز ادراکِ بشر نیست برون کعبۀ عشق</p></div>
<div class="m2"><p>عقلِ بی چاره فرو مانده چه و حیران چیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالمی خلق فرو رفت و نیامد بر سر</p></div>
<div class="m2"><p>کس ندانست که این بادیه را پایان چیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق با حاسدِ من گفت که ای بی معنی</p></div>
<div class="m2"><p>سرزنش کردن و تشنیع زدن هم سان چیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر به جانت رسد از دودِ نزاری اثری</p></div>
<div class="m2"><p>روشنت گردد کاین صاعقۀ سوزان چیست</p></div></div>