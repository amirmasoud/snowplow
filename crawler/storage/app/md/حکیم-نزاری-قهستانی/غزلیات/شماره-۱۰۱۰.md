---
title: >-
    شمارهٔ ۱۰۱۰
---
# شمارهٔ ۱۰۱۰

<div class="b" id="bn1"><div class="m1"><p>ای سرِ کویِ تو منزل گَهِ آسایشِ من</p></div>
<div class="m2"><p>خجل از مصقلۀ جودِ تو آرایشِ من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه امید به بخشودن و بخشیدنِ توست</p></div>
<div class="m2"><p>فضلِ تو کم نکند حصّه ی بخشایشِ من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم رضایِ تو خلاصم دهد از آتشِ قهر</p></div>
<div class="m2"><p>بس بود رحمت تو بوته ی پالایش من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیشِ طوفانِ قیامت چه محل خواهد داشت</p></div>
<div class="m2"><p>در دیوار عبادت ز گل اندایش من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دلیلِ کرمت هادی بختم نشود</p></div>
<div class="m2"><p>ره به مقصد نبرد مرحله پیمایشِ من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار تقدیرِ تو دارد من و امید به تو</p></div>
<div class="m2"><p>تا چه آید زمن و کاهش و افزایشِ من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاریِ زارِ نزاریِ ستم فرسوده</p></div>
<div class="m2"><p>تو شنو ورنه که دارد غمِ فرسایش من</p></div></div>