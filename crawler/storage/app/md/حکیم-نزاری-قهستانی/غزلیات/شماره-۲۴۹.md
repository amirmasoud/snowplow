---
title: >-
    شمارهٔ ۲۴۹
---
# شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>جماد بی خبرست از خروشِ بلبلِ مست</p></div>
<div class="m2"><p>به باغ باده خورد هر که را حیاتی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز عیش بهره ندارد کسی که وقتِ بهار</p></div>
<div class="m2"><p>به پایِ گل نگرفته ست جامِ باده به دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش آن خورد که به شب خیزد و به گه خفتد</p></div>
<div class="m2"><p>نه آن که روز برآید ز خواب باز نشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبوح سنّتِ اهلِ دل است خاصه چنان</p></div>
<div class="m2"><p>که آفتاب بر آید فرو شود سرمست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکستِ ما مکن ای عاقل آبگینۀ خود</p></div>
<div class="m2"><p>ز سنگِ عشق نگهدار کانِ ما بشکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیار ساقیِ مجلس که زهر نوش کنیم</p></div>
<div class="m2"><p>که انگبین بود از دستِ تیره روی کبست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی دو پایه فرود آی و عارفان را بین</p></div>
<div class="m2"><p>بلی بگفته و برکف گرفته جامِ الست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نزاریا نظر از بودِ خویشتن بردار</p></div>
<div class="m2"><p>به قبله معتقدی پس به جهل بت مپرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو مردِ لاف نیی هر درخت را نرسد</p></div>
<div class="m2"><p>که هم چو سرو بلندی کند به قامتِ پست</p></div></div>