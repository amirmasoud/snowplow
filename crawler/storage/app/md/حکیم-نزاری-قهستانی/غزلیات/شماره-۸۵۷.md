---
title: >-
    شمارهٔ ۸۵۷
---
# شمارهٔ ۸۵۷

<div class="b" id="bn1"><div class="m1"><p>ای بی خبر ز دردِ دلِ مهرپرورم</p></div>
<div class="m2"><p>عیبم مکن که عاشقم آخر نه کافرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون غافلی ز عشق چه دانی که حال چیست</p></div>
<div class="m2"><p>تو خویشتن پرستی و من عشق پرورم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صاحب‌نظر چو بنگرد انکار کی کند</p></div>
<div class="m2"><p>در قامتِ خمیده و در گونهی زرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نادیده رویِ دل بر او گویی گرفته اند</p></div>
<div class="m2"><p>از عکسِ رویش آینه ای در برابرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشتم بر آستان درش همچو خاک پست</p></div>
<div class="m2"><p>روزی مگر به سهو نهد پای بر سرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر در سرِ هواش کنم تا به رستخیز</p></div>
<div class="m2"><p>با آبِ روز خاکِ لحد سر برآورم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بنگرند روزِ قیامت ز مهرِ دوست</p></div>
<div class="m2"><p>بوی وفا دهد همه اجزایِ پیکرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا در وجودِ بی‌ غرضم عشق راه یافت</p></div>
<div class="m2"><p>زان پس دگر به هستیِ خود باز ننگرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی دیده راهْ بینم و بی سر کلاه‌ْدار</p></div>
<div class="m2"><p>بنشسته ی رونده و گنگِ سخنورم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه نه نه حدِّ مرتبه ی چون منی بود</p></div>
<div class="m2"><p>بر خود یقینم اَر به نزاری گمان برم</p></div></div>