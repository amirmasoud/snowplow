---
title: >-
    شمارهٔ ۶۸۳
---
# شمارهٔ ۶۸۳

<div class="b" id="bn1"><div class="m1"><p>درونِ سینه یی دارم پر آتش</p></div>
<div class="m2"><p>دلی از آتشی چون آبِ رز خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هم اضداد را چون ممتزج کرد</p></div>
<div class="m2"><p>تعالی الله به یک جا آب و آتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب خاصیّتی دارد از اوّل</p></div>
<div class="m2"><p>بود بیگانه امّا آشنا وش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به آخر می کند اضداد را جمع</p></div>
<div class="m2"><p>اگرچه ز ابتدا دارد مشوّش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به وجهِ می گرو کن هر چه داری</p></div>
<div class="m2"><p>کلاه و موزه و قربان و ترکش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه جز دوست کّلِ آفرینش</p></div>
<div class="m2"><p>حجابِ تست باید کرد ترکش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من و یک هم نفس کوهست و من نه</p></div>
<div class="m2"><p>زمین گو هفت می باش و جهت شش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هوایِ حرص و آز الحمدلله</p></div>
<div class="m2"><p>نزاری را ندارد در کشاکش</p></div></div>