---
title: >-
    شمارهٔ ۱۲۰۷
---
# شمارهٔ ۱۲۰۷

<div class="b" id="bn1"><div class="m1"><p>نیک‌بخت است که دارد چو تو یاری و نگاری</p></div>
<div class="m2"><p>من ندیدم به نکورویی و خوبیِ تو باری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرّم آن دل که بود در سر زلفِ تو قرارش</p></div>
<div class="m2"><p>گرچه در زلفِ پریشانِ تو خود نیست قراری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یوسفِ مصر گر این روی بدیدی چو زلیخا</p></div>
<div class="m2"><p>مصر در وجه نهادی عوضِ بوس و کناری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پادشاهیست گدایی به سرِ کویِ تو کردن</p></div>
<div class="m2"><p>تا که را دولتِ این کدیه میسّر شود آری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اتّصالاتِ احبّا چو ز مبداست پس این‌جا</p></div>
<div class="m2"><p>شاید ار بر پیِ یاری برود خاطرِ یاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد سرم پر ز بخارِ غمِ سودایِ تو آیا</p></div>
<div class="m2"><p>از میِ لعلِ لبت باز توان کرد بخاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر نه انگیزدم از کویِ تو طوفانِ قیامت</p></div>
<div class="m2"><p>تا نگویند که از کوی تو برخاست غباری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راستی دستِ تو آلوده دریغ است به خونم</p></div>
<div class="m2"><p>جهد کن هان که در انداخته‌ای طرفه شکاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صیدِ لاغر چه کشی مرحمتی کن که کم افتد</p></div>
<div class="m2"><p>چون نزاری به کمندِ تو گرفتار نزاری</p></div></div>