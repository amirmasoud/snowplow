---
title: >-
    شمارهٔ ۱۲۴۲
---
# شمارهٔ ۱۲۴۲

<div class="b" id="bn1"><div class="m1"><p>تا نیستی خود را از راه برنگیری</p></div>
<div class="m2"><p>هرگز طریق هستی از راه برنگیری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر از تو با تو بویی در نیستی بماند</p></div>
<div class="m2"><p>بویی دگر نیابی رنگی دگر نگیری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از رنگ و بوی بازآ وزخوب و زشت بگذر</p></div>
<div class="m2"><p>در خود اثر نبینی گر خود حذر نگیری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو آتشی و نفست بی خشک و تر نباشد</p></div>
<div class="m2"><p>تا هم چو برق آتش در خشک و تر نگیری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوریست عشق وحدت پس چون چراغ میری</p></div>
<div class="m2"><p>گر شمع زندگانی ز آن نور درنگیری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر در غلاف شبهت تا کی هوا گرفتن</p></div>
<div class="m2"><p>دنبال مرغ معنی بی بال و پر نگیری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنهار اگر درین عهد از شست پاک بازان</p></div>
<div class="m2"><p>صد تیر بر تو آید یک ره سپر نگیری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مُهر از زبان حیرت زنهار برنداری</p></div>
<div class="m2"><p>ستر از جمال دعوی زنهار برنگیری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دم بر مزن درین دم بی همدمی نزاری</p></div>
<div class="m2"><p>بی هم دمی درین ره ترک سفر نگیری</p></div></div>