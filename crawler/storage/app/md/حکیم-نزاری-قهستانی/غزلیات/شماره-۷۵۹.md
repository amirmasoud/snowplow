---
title: >-
    شمارهٔ ۷۵۹
---
# شمارهٔ ۷۵۹

<div class="b" id="bn1"><div class="m1"><p>صخره ای بر راهِ ما بود از خیال</p></div>
<div class="m2"><p>برگرفت آن صخره را از ره جمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمزه ای کردند از طرفِ نقاب</p></div>
<div class="m2"><p>عقلِ ما زان غمزه حالی کرد حال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه از آن شکل و شمایل آه آه</p></div>
<div class="m2"><p>دل ببرد از ما بدان غنج و دلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل ز بی صبری چو ذرّه مضطرب</p></div>
<div class="m2"><p>جان به نورالعین در عینِ وصال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل از آن زد دست در فتراکِ عشق</p></div>
<div class="m2"><p>تا نصیبی یابد از دورِ کمال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز این دورش نباشد بهره ای</p></div>
<div class="m2"><p>گردِ سر گردد چو دورانِ محال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل منه بر نسیۀ رای و قیاس</p></div>
<div class="m2"><p>نقد بطلب تا بیابی ارزِ حال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راویان انصاف را چون کرده اند</p></div>
<div class="m2"><p>هم چون کاه آن خر بطلان را در جوال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خلق را چون در ضلالت می برند</p></div>
<div class="m2"><p>وآن نگون ساران نترسند از وبال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هی نزاری چیست این ، دیوانه ای</p></div>
<div class="m2"><p>چشم می دار از دلیری گوش مال</p></div></div>