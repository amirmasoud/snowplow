---
title: >-
    شمارهٔ ۶۰۸
---
# شمارهٔ ۶۰۸

<div class="b" id="bn1"><div class="m1"><p>بی‌دوست این منم که چنین می‌برم به سر</p></div>
<div class="m2"><p>ای خاک بر سرِ من و خاکستر از زبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این است وبیش ازین و بتر زین سزایِ من</p></div>
<div class="m2"><p>از کویِ دوستان نکنم بعد از این سفر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل از کجا و من ز کجا کز دلِ فضول</p></div>
<div class="m2"><p>بیزارم از قبولِ نصیحت کند دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از غبن و غصّه خوردن و ناچاره دم زدن</p></div>
<div class="m2"><p>دیوانه می‌شود دل و خون می‌شود جگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمم به راه‌ و گوش بر آوازِ پیکِ دوست</p></div>
<div class="m2"><p>جانم فدایِ آن که ز جانان دهد خبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای باد قاصدی شو و پیغامِ او بیار</p></div>
<div class="m2"><p>وی بخت چاره‌ای کن و تیمارِ من ببر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باشد که باد لطف کند تا به سعیِ باد</p></div>
<div class="m2"><p>بویی بما رسد زِ عرق چینِ او مگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از من هزار خدمت و اخلاص می‌برد</p></div>
<div class="m2"><p>بادی که بر دیارِ نزاری کند گذر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خرّم وجودِ آن که ز تأثیرِ بختِ نیک</p></div>
<div class="m2"><p>بر آستانِ دوست چو خاک است بی‌سپر</p></div></div>