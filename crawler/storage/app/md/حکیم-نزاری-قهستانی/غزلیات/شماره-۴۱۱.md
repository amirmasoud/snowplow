---
title: >-
    شمارهٔ ۴۱۱
---
# شمارهٔ ۴۱۱

<div class="b" id="bn1"><div class="m1"><p>دلم ز جورِ تو خون گشت و برنمی‌گردد</p></div>
<div class="m2"><p>ز راهِ دیده برون گشت و برنمی‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه سخت‌جان است این آهنین‌صفت دلِ من</p></div>
<div class="m2"><p>که در فراقِ تو خون گشت و برنمی‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پایِ هجر در افتاد و برنمی‌افتد</p></div>
<div class="m2"><p>به دستِ عشق زبون گشت و برنمی‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز چاه محنتِ بختم خلاص روزی نیست</p></div>
<div class="m2"><p>که چرخِ وصل نگون گشت و برنمی‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرد ز حلقۀ زلفت که پای بندِ دل است</p></div>
<div class="m2"><p>جهان نمایِ جنون گشت و برنمی‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نزاریا دگر از دل مگوی و گر گویی</p></div>
<div class="m2"><p>جزین مگوی که خون گشت و برنمی‌گردد</p></div></div>