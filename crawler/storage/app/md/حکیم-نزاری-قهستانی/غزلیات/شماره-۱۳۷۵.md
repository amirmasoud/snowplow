---
title: >-
    شمارهٔ ۱۳۷۵
---
# شمارهٔ ۱۳۷۵

<div class="b" id="bn1"><div class="m1"><p>شبِ وصال نبردم گمانِ روز جدایی</p></div>
<div class="m2"><p>هلاک می‌شوم ای چشمه ی حیات کجایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز پای‌مالِ فراقت به هیچ وجه خلاصم</p></div>
<div class="m2"><p>نمی‌شود متصوّر مگر تو با سرم آیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسیم‌وار کشم جان به پیشِ رویِ تو روزی</p></div>
<div class="m2"><p>که بازآیی و بند بغل‌تر بگشایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دمِ پسینم اگر پیش از آن که قطع بباشد</p></div>
<div class="m2"><p>فرا رسی به سرم زندگانیم بفزایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز پیش چشم برفتی و در مقابلِ جانی</p></div>
<div class="m2"><p>کجا روی که به حکمِ ازل حواله به مایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا که ناله ی زارم محصّلیت فرستد</p></div>
<div class="m2"><p>که آن‌قدر ندهد مهلتت که خط بنمایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیار اگر همه بر خونِ من بود که نپیچم</p></div>
<div class="m2"><p>سر از خطِ خوشت ای رشکِ لعبتان ختایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر به شرح نویسم که بی تو در چه عذابم</p></div>
<div class="m2"><p>دلت بسوزد و رحم آوری و پیش بیایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگفته‌اند حکیمان که دل به دل کشد آخر</p></div>
<div class="m2"><p>چو مایلم به تو چندین ز من ملول چرایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من از تو جان نبرم عاقبت چنان که تو گفتی</p></div>
<div class="m2"><p>برای آنم اگر نیز هم بر آن سرِ رایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کمندِ عشق تو و گردنِ نزاری عاجز</p></div>
<div class="m2"><p>که را امید بماند به هیچ روی رهایی</p></div></div>