---
title: >-
    شمارهٔ ۹۳۳
---
# شمارهٔ ۹۳۳

<div class="b" id="bn1"><div class="m1"><p>ما گر چه ز خدمتت جداییم</p></div>
<div class="m2"><p>تا ظن نبری که بی‌وفاییم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان‌ها که وفا به سر نبردند</p></div>
<div class="m2"><p>زنهار گمان مبر که ماییم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما زَرق و ریا نمی‌فروشیم</p></div>
<div class="m2"><p>حال دل خویش می‌نماییم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نزدیک توییم گر چه دوریم</p></div>
<div class="m2"><p>بیگانه‌نمای آشناییم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این بیت ز گفته‌ی نظامی</p></div>
<div class="m2"><p>گوییم و ز دیده خون گشاییم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>«آیا تو کجا و ما کجاییم</p></div>
<div class="m2"><p>تو زانِ‌که‌ای که ما تراییم؟»</p></div></div>