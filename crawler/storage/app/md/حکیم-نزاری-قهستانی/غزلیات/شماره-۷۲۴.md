---
title: >-
    شمارهٔ ۷۲۴
---
# شمارهٔ ۷۲۴

<div class="b" id="bn1"><div class="m1"><p>مردانه وار بر گذر از آرزوی خویش</p></div>
<div class="m2"><p>دیگر مبین به دیده خود دیده سوی خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی دل کسان نتوان دید و روی دوست</p></div>
<div class="m2"><p>گر روی دوست خواهی منگر به روی خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور برگ ترک خویشتنت نیست در سلوک</p></div>
<div class="m2"><p>بیرون مشو ز جدول پرگار کوی خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دعوی دوستی تو در دوستی دوست</p></div>
<div class="m2"><p>آنگه مسلم است که باشی عدوی خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا از نسیم دوست شوی ممتلی دماغ</p></div>
<div class="m2"><p>باید که بر نتابی از باد بوی خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگذشتن از مراتب اکوان دیو چیست</p></div>
<div class="m2"><p>باز آمدن به خوی ملایک ز خوی خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برخوان نزاریا که صموت از جواب به</p></div>
<div class="m2"><p>تا وا رهی ز مظلمه گفتگوی خویش</p></div></div>