---
title: >-
    شمارهٔ ۵۹۵
---
# شمارهٔ ۵۹۵

<div class="b" id="bn1"><div class="m1"><p>باز اوفتاد در سرم از عشق خار خار</p></div>
<div class="m2"><p>ای دل ترا که گفت سر آخر به خار خار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاری دگر چه می کنم آخر مگر هنوز</p></div>
<div class="m2"><p>در پایِ دل شکسته ندارم هزار خار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارم به جایِ گل همه شب خار در کنار</p></div>
<div class="m2"><p>هرگز کسی نکرد چو گل در کنار خار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین پیش تر زمانه به ره بر نمی نهاد</p></div>
<div class="m2"><p>اکنون به عکس می نهدم روزگار خار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب هایِ تا به روز به عمدا همی کند</p></div>
<div class="m2"><p>بر خواب گاه و بستر و بالین نثار خار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوابم کجا که از مژه ها خار ساخته ست</p></div>
<div class="m2"><p>تا راهِ خواب کرد چنین استوار خار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حاجت به خار نیست که در خواب گاهِ من</p></div>
<div class="m2"><p>هر موی بر تنم شود از انتظار خار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موی از بخارِ مهر شود خار بر تنم</p></div>
<div class="m2"><p>تا دفعِ خواب می کند از چشم خار خار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سهل است گو ز پایِ من این خار بر میار</p></div>
<div class="m2"><p>آه ار برآید از رخِ آن گل عذار خار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مغرورِ حسن را چه عجب گر ز برگِ گل</p></div>
<div class="m2"><p>ناگاه بر دمد از پیِ اعتبار خار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گو از شبِ ضلالتِ زلفم مده خلاص</p></div>
<div class="m2"><p>ور نورِ دیده می خلدم گو بیار خار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جانا روا مدار که در نو بهارِ حسن</p></div>
<div class="m2"><p>از گلسِتان رسد به نزاریِ زار خار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو عمر صرف شد ندهد لذّتی حیات</p></div>
<div class="m2"><p>چون عهدِ گل گذشت نیاید به کار خار</p></div></div>