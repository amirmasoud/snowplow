---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>دوستان با جگرِ تشنه رسید آب حیات</p></div>
<div class="m2"><p>کوریِ مدّعیان را به محمّد صلوات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکرِ حق را که نمردیم و رسیدیم به کام</p></div>
<div class="m2"><p>عاقبت هم اثری روی نمود از دعوات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما به فروسِ ملاقات رسیدیم و حسود</p></div>
<div class="m2"><p>تا به جاوید بماناد ولی در درکات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حق تعالی به کرم باز به ما در نگرید</p></div>
<div class="m2"><p>وز سرِ مرحمت از دستِ بلا داد نجات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن کشیدیم ز ایّام که گر شرح دهیم</p></div>
<div class="m2"><p>نیست ممکن که مهندس کند ادراکِ صفات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دگر بی قدمان دست به هرکس نزدیم</p></div>
<div class="m2"><p>عهدِ محکم نشکستیم و نمودیم ثبات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کارِ ما با نفسِ باز پسین آمده بود</p></div>
<div class="m2"><p>همه دل ها بنهادیم ضرورت به ممات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خطِّ تسلیم بدادیم و طمع ببریدیم</p></div>
<div class="m2"><p>بارِ دیگر ملک الموت بدل کرد برات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگرت سیم و زری نیست نزاری به نثار</p></div>
<div class="m2"><p>سر به شکرانه در انداز و بده جان به زکات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکسی را طمعی هست ولیکن ما را</p></div>
<div class="m2"><p>چه به از دوست که آرند ز غربت سوغات</p></div></div>