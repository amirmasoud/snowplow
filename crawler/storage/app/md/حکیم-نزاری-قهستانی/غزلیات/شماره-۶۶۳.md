---
title: >-
    شمارهٔ ۶۶۳
---
# شمارهٔ ۶۶۳

<div class="b" id="bn1"><div class="m1"><p>رخ بنما ای صنم پرده فکن یک نفس</p></div>
<div class="m2"><p>بهر خدا رحم کن بر من و فریاد رس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هوس روی تو عمر به پایان رسید</p></div>
<div class="m2"><p>آه که جان می‌دهم در هوس این هوس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر شب خلوت مرا بار دهی باک نیست</p></div>
<div class="m2"><p>با رخ چون روز تو کار ندارد عسس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبهِ خطِ خوبِ تو ماه ندیده ست خلق</p></div>
<div class="m2"><p>شکل قد شَنگِ تو سرو ندیده ست کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند نصیحت کند دوستم از نیک و بد</p></div>
<div class="m2"><p>چند ملامت کند دشمنم از پیش و پس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌سبب درد نیست ناله و فریاد من</p></div>
<div class="m2"><p>جنبش چیزی بود موجبِ بانگِ جرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه نزاری زُدود آینهٔ روح را</p></div>
<div class="m2"><p>آینه گرچه که آه تیره کند از نفس</p></div></div>