---
title: >-
    شمارهٔ ۱۰۶۳
---
# شمارهٔ ۱۰۶۳

<div class="b" id="bn1"><div class="m1"><p>هر که را مهرِ تو در دل نبود بی جان به</p></div>
<div class="m2"><p>وان که جز عشقِ تو اش کیش بود قربان به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلِ من در سرِ میدانِ محبّت چون گوی</p></div>
<div class="m2"><p>در خمِ زلف چو چوگانِ تو سرگردان به</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قفلِ یاقوت که بر درجِ دُر انداخته ای</p></div>
<div class="m2"><p>سخت خوب است ولی پسته و گل خندان به</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من به مرجان نکنم نسبتِ لعل تو که هست</p></div>
<div class="m2"><p>بوسه ای زان لبِ لعلِ تو ز صد مرجان به</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگرم تیغ زنی سر نهم اندر قدمت</p></div>
<div class="m2"><p>سرِ چاکر چو رود در قدمِ سلطان به</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی بنمای که ابرویِ تو محرابِ من است</p></div>
<div class="m2"><p>باشدم کفرِ سرِ زلفِ تو از ایمان به</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه دردِ تو نزاری نپذیرد درمان</p></div>
<div class="m2"><p>عشق دردی ست که آن را نکنی درمان به</p></div></div>