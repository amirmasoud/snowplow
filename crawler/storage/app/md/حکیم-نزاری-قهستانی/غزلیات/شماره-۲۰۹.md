---
title: >-
    شمارهٔ ۲۰۹
---
# شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>پیر ما نعره زنان کوزه ی دردی در دست</p></div>
<div class="m2"><p>روز آدینه به بازار درآمد سرمست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با مریدان به سر کوی خرابات کشید</p></div>
<div class="m2"><p>با حریفان خرابات به مجلس بنشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدحی پر بستد تا سرو بر دست گرفت</p></div>
<div class="m2"><p>گفت چه فاسق و چه زاهد و چه نیست و چه هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبغه الله به کار آید نی ارزق زرق</p></div>
<div class="m2"><p>رنج بی هوده بود رنگ بر او نتوان بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بانگ برداشت که تا چند ز کوته نظری</p></div>
<div class="m2"><p>هان وهان عهد و وفا تازه کنید از سر دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بت بود بت زر و سیم و رز و باغ و زن و زه</p></div>
<div class="m2"><p>بت پرستی نکند هر که بود دوست پرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست بگشاد و حریفان همه را جامه بکند</p></div>
<div class="m2"><p>باده در داد و مریدان همه را توبه شکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که پیش آمد و تسلیم شد و بیعت کرد</p></div>
<div class="m2"><p>از بروت خود و از ریش همه خلق برست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرد از قاعده و عادت و رسم استغفار</p></div>
<div class="m2"><p>برد یاران همه را بر سر پیمان الست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دعوت پیر چو بشنید نزاری و بدید</p></div>
<div class="m2"><p>خود همین سنت و دین داشت نزاری پیوست</p></div></div>