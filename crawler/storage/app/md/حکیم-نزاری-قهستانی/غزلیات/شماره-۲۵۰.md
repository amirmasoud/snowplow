---
title: >-
    شمارهٔ ۲۵۰
---
# شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>دوش رفتم در خرابات از نماز شام مست</p></div>
<div class="m2"><p>مجلسی دیدم درو جمعی علی الاتمام مست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مست آدم مست حوّا مست فرزندان در او</p></div>
<div class="m2"><p>زاهد معصوم مست و رند درد آشام مست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مست دربان مست رهبان مست مریم مست روح</p></div>
<div class="m2"><p>ابن مست و بنت مست و اخت مست و مام مست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مست پرده مست ساقی مست مطرب مست رود</p></div>
<div class="m2"><p>خنب مست و کوزه مست و باده مست و جام مست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مست مجمر مست منقل مست عنبر مست عود</p></div>
<div class="m2"><p>شیخ مست و شاب مست و خاص مست و عام مست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مست چنگ و مست نای و مست عود و مست دف</p></div>
<div class="m2"><p>رفته مست و خفته مست و پخته مست و خام مست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل مست و نفس مست و صبر مست و هوش مست</p></div>
<div class="m2"><p>فخر مست و عار مست و ننگ مست و نام مست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب مست و نار مست و خاک مست و باد مست</p></div>
<div class="m2"><p>وقت مست و حال مست و صبح مست و شام مست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مست ظاهر مست باطن مست دنیا مست دین</p></div>
<div class="m2"><p>جمله مست القصه از آغاز تا انجام مست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاشق و مستی نزاری نیک و هر منکر که هست</p></div>
<div class="m2"><p>در صفات عشق مست ، اوصاف مست اوهام مست</p></div></div>