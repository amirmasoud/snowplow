---
title: >-
    شمارهٔ ۶۹۷
---
# شمارهٔ ۶۹۷

<div class="b" id="bn1"><div class="m1"><p>هر که ما را دوست دارد خلق گردد دشمنش</p></div>
<div class="m2"><p>ترک خود باید گرفت آن را که باید با منش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه گامی زد درین ره اختیارش شد ز دست</p></div>
<div class="m2"><p>وآن که سر پیچید ازین در خون خود در گردنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این قبول از دوست می باید که باشد قصّه چیست</p></div>
<div class="m2"><p>بخت چون برداردش گر دوست گوید بفکنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیسی از امّت چو شد بیزار ترسا را چه سود</p></div>
<div class="m2"><p>گر چو رشته بگذرد بر چشمۀ سوزن تنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر زبان در می کشد جاهل مشو در خشم ازو</p></div>
<div class="m2"><p>رحم کن بر چشمِ نابینا و طبعِ کودنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مفلس ار داند که این دُر از کدامین بحر خاست</p></div>
<div class="m2"><p>خوش کند کامِ نهنگ اندر به دست آوردنش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خشک مغز از همّتِ صاحب ولایت غافل است</p></div>
<div class="m2"><p>در شود وز موجِ دریا تر نگردد دامنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گنبد فیروزۀ گردون چو شب گردد سیاه</p></div>
<div class="m2"><p>دودِ آهِ عاشقان گر بگذرد بر روزنش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قمریِ طوبا نشین و دانۀ دنیایِ دون</p></div>
<div class="m2"><p>بالله ار جمله جهان در چشمِ جان یک ارزنش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل برین وحشت سرایِ دهر ننهد معتقد</p></div>
<div class="m2"><p>چون بود روح القدس را کنجِ گلخن مسکنش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که شد حاشا کُم الله زالِ دنیا را زبون</p></div>
<div class="m2"><p>مرد نَبوَد بل که کم دانند مردان از زنش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شیشۀ بایست و نا بایست را چون زلفِ دوست</p></div>
<div class="m2"><p>گر نمی خواهی که باشی زن صفت بر هم زنش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یار بت روی است و می گوید نزاری غیرِ من</p></div>
<div class="m2"><p>گر همه میثاقِ اسلام است چون بت بشکنش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مَرغ زارِ دهر پر شیرست و ناممکن بود</p></div>
<div class="m2"><p>بی سمومِ قاتل ار خواهی نسیمِ گلشنش</p></div></div>