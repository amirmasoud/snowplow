---
title: >-
    شمارهٔ ۸۷۷
---
# شمارهٔ ۸۷۷

<div class="b" id="bn1"><div class="m1"><p>جان برای تو که هم دردی و هم درمانم</p></div>
<div class="m2"><p>سر فدای تو که هم جانی و هم جانانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با تو چون قامت تو از دگران آزادم</p></div>
<div class="m2"><p>بی تو چون وصل تو از بی نظران پنهانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لوح سودای تو چون باد ز سر می گیرم</p></div>
<div class="m2"><p>در سر اندوه تو چون آب ز بر می خوانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق روحانی سوزان تر از این ممکن نیست</p></div>
<div class="m2"><p>کز تف آه جگر برق همی سوزانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر خیالی ست کسی را و تصور بندد</p></div>
<div class="m2"><p>کز تو برگردم و خو باز کنم نتوانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده بر قبضه ی ابروی کمان دارم و تیر</p></div>
<div class="m2"><p>بر جگر میخورم و روی نمی گردانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب هجران تو گر تا به قیامت باشد</p></div>
<div class="m2"><p>دیده بر هم نزنم ور مژه خون افشانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر سگم خوانی و کس باز نزاری خواند</p></div>
<div class="m2"><p>دهنش بشکنم و یا سه بدو برسانم</p></div></div>