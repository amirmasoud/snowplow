---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>چون بخواند حاسدم آتش پرست</p></div>
<div class="m2"><p>چون مرا بیند چنین آتش بدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماهم اندر آتشیم از آب رز</p></div>
<div class="m2"><p>گر خلیل اللّه در آتش نشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بود بی آبرویان را مجال</p></div>
<div class="m2"><p>از تعصّب در زنند آتش به مست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد روبَه طبیعت را چه قدر</p></div>
<div class="m2"><p>شیر با آن صولت از آتش بجست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نباشد دوزخی مست سخی</p></div>
<div class="m2"><p>پس از آن آتش به این آتش بر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقیا هین کز دم دی در دلم</p></div>
<div class="m2"><p>گر نمی خون بود بی آتش ببست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی چه میگویم که دور از روی او</p></div>
<div class="m2"><p>زار میسوزم ز بس آتش که هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوش گفتی می لبم مجروح کرد</p></div>
<div class="m2"><p>جان من یاقوت کی زأتش بخست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از نزاری رخ مپوش امشب که هست</p></div>
<div class="m2"><p>بت پرست ، اخترپرست ، آتش پرست</p></div></div>