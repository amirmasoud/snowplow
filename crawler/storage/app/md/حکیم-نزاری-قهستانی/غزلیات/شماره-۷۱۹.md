---
title: >-
    شمارهٔ ۷۱۹
---
# شمارهٔ ۷۱۹

<div class="b" id="bn1"><div class="m1"><p>خوش عالم عاقلان مدهوش</p></div>
<div class="m2"><p>خوش وقت سخن وران خاموش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با عشق خوش است خاصه مفرد</p></div>
<div class="m2"><p>با یار خوش است خاصه مدهوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردان که در آمدند بی چشم</p></div>
<div class="m2"><p>از خانه برون شدند بی گوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسوا شد و فاش هرکه برداشت</p></div>
<div class="m2"><p>از خوان چه‌ ی سرّ دوست سرپوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی طاقت بار عشق دارند</p></div>
<div class="m2"><p>نازک دلکان بی تن و توش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گو دور شو از مصاف بد دل</p></div>
<div class="m2"><p>ور نه چو دد بهیمه مخروش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در گردن دیگران مکن دست</p></div>
<div class="m2"><p>تا با تو شود وفا هم آغوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در باغ جهان ز نامرادی</p></div>
<div class="m2"><p>رز کار نزاریا و می نوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر آتش تیز عشق می باش</p></div>
<div class="m2"><p>تا پخته شوی تمام برجوش</p></div></div>