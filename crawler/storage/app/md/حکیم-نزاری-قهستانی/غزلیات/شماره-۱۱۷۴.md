---
title: >-
    شمارهٔ ۱۱۷۴
---
# شمارهٔ ۱۱۷۴

<div class="b" id="bn1"><div class="m1"><p>اگر زمام ارادت به دست ما بودی</p></div>
<div class="m2"><p>وجود معتکف حضرت شما بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآمدی رمقی کر حیات من باقیست</p></div>
<div class="m2"><p>اگر نه بوی تو در جنبش صبا بودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگر رقیب فضولی نمی شدی مانع</p></div>
<div class="m2"><p>ز آستان تو کی روی من جدا بودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا فراق نکشتی گرم شکیب استی</p></div>
<div class="m2"><p>تو را نظیر نبودی گرت وفا بودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز من حدیث نگویی و یاد من نکنی</p></div>
<div class="m2"><p>من ار چنان که چنین کردمی روا بودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ثبات عهد نباشد چه سود حسن و جمال</p></div>
<div class="m2"><p>دریغ اگر چو تویی را قدم به جا بودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین که دست برآورده ای به بد عهدی</p></div>
<div class="m2"><p>اگر ز من اثری دیده ای سزا بودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فریب میدهدم هم به عذر ها لکن</p></div>
<div class="m2"><p>نکوستی اگرت دل به حا ما بودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هر صفت که بود هم نگه توانی داشت</p></div>
<div class="m2"><p>دل نزاری مسکین گرت رضا بودی</p></div></div>