---
title: >-
    شمارهٔ ۹۰۱
---
# شمارهٔ ۹۰۱

<div class="b" id="bn1"><div class="m1"><p>ما ساغر الست به رغبت کشیده‌ایم</p></div>
<div class="m2"><p>قالوابلی به گوشِ ارادت شنیده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر مستی کنیم ز بی طاقتی رواست</p></div>
<div class="m2"><p>کآن روز کاسه هایِ لبالب کشیده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه نه قدح کشان دگران اند ما نه‌ایم</p></div>
<div class="m2"><p>ما جرعه یی ز جرعه ی ایشان چشیده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیرست تا نهالِ مودّت به مهرِ دل</p></div>
<div class="m2"><p>بر جویْ بارِ روضه ی جان پروده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در پیشِ شش جهات به کاوینِ هر دوکون</p></div>
<div class="m2"><p>خود را ز پیرِ زال جهان واخریده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر دل ز بارِ فاقه نداریم هیچ بار</p></div>
<div class="m2"><p>زیرا که ما سرِ طمع اول بریده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با کاورانیانِ مجرّد مصاحبیم</p></div>
<div class="m2"><p>افلاس و فقر و فاقه از آن برگزیده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌پای رهسپرده و بی نطق گفته‌ایم</p></div>
<div class="m2"><p>بی سامعه شنیده و بی دیده دیده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک هفته یی تعهدِ ما کن نزاریا</p></div>
<div class="m2"><p>ما را عزیز دار که مهمن رسیده‌ایم</p></div></div>