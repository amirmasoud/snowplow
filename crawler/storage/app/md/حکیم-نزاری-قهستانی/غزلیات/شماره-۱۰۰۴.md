---
title: >-
    شمارهٔ ۱۰۰۴
---
# شمارهٔ ۱۰۰۴

<div class="b" id="bn1"><div class="m1"><p>حذر کن ز آهِ جهان‌گیرِ من</p></div>
<div class="m2"><p>که ناگه خورد بر هدف تیرِ من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نترسی که روزی به درد آورد</p></div>
<div class="m2"><p>دلِ نازکت آهِ شب‌گیرِ من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم آن که آهش لقب می‌نهند</p></div>
<div class="m2"><p>جهانی بسوزد ز تأثیرِ من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملامت مکن بر من از بی‌خودی</p></div>
<div class="m2"><p>که ایزد چنین کرد تقدیرِ من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل هر که دیوانۀ زلفِ توست</p></div>
<div class="m2"><p>گرفتار باشد به زنجیرِ من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر از تو باشد قبولیّتی</p></div>
<div class="m2"><p>وگرنه محال است تدبیرِ من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه آخر نزاریِ زار تو‌ام</p></div>
<div class="m2"><p>ببخشای بر نالۀ زیرِ من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عذابی شدیدست هجران‌ِ تو</p></div>
<div class="m2"><p>خطایی عظیم است تأخیرِ من</p></div></div>