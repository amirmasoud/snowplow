---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>از دلم هیچ خبر نیست ندانم که کجاست</p></div>
<div class="m2"><p>هر کجا هست چه آید ز چنان دل که مراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خمِ زلفِ بتی مانده باشد محبوس</p></div>
<div class="m2"><p>هم مگر بادِ صبا زو خبری آرد راست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اعتمادی نبود بر دلِ هر جاییِ من</p></div>
<div class="m2"><p>راست گویم که دل غافلِ نا پا برجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به انصاف رود داوریِ دیده و دل</p></div>
<div class="m2"><p>گنه دیدهء شوخ است که دل قیدِ بلاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این دگر هست که مشّاطهء فطرت ز ازل</p></div>
<div class="m2"><p>نقشِ هر جنس به تقدیر چنان کرد که خواست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نمیخواست که مجنون شود آشفتهء عشق</p></div>
<div class="m2"><p>رویِ لیلی ز پی فتنه چرا میآراست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من خود از دستِ رقیبان شدهام دیوانه</p></div>
<div class="m2"><p>عاقلی کو که نه از عشق سرش پر سوداست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کجا بر سرِ کویی بنشستم عمدا</p></div>
<div class="m2"><p>رستخیز آمد و طوفانِ ملامت برخاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زاریی دارد و فریاد رسی میطلبد</p></div>
<div class="m2"><p>بر نزاری چه ملامت که رقیبش به قفاست</p></div></div>