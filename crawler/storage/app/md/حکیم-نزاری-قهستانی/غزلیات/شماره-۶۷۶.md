---
title: >-
    شمارهٔ ۶۷۶
---
# شمارهٔ ۶۷۶

<div class="b" id="bn1"><div class="m1"><p>دلا خاک در دیدۀ عقل پاش</p></div>
<div class="m2"><p>نه از خویشتن عاقلی بر تراش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر اهلِ دنیا زبون اند و خوار</p></div>
<div class="m2"><p>تو باری گدایی ازین خیل باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و بالِ تو در آفرینش تویی</p></div>
<div class="m2"><p>نبودی تویِ تو در اصل کاش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر افکنده را چون درو محو شد</p></div>
<div class="m2"><p>توان کرد تصدیقِ نسبت به ماش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جانی دگر زنده اند اهلِ دل</p></div>
<div class="m2"><p>غذایِ محقّق نه نان است و آش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ملامت گرِ مدّعی گو مدام</p></div>
<div class="m2"><p>به طعنه دلِ اهلِ دل می خراش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر پوستت چون نخود در کشند</p></div>
<div class="m2"><p>نشاید که صفرا کنی هم چو ماش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندانی ندانی که مردانِ حق</p></div>
<div class="m2"><p>نکردند اسرارِ پوشیده فاش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کجا گر درآید نشیند سروش</p></div>
<div class="m2"><p>مگر خانه خالی کنی از قماش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو مخلوقی و آفریننده را</p></div>
<div class="m2"><p>ندانی نبینی به عقلِ معاش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که را بینی ار او نگوید ببین</p></div>
<div class="m2"><p>که را باشی ار او نگوید بباش</p></div></div>