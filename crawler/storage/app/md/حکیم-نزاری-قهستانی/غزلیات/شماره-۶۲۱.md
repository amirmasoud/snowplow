---
title: >-
    شمارهٔ ۶۲۱
---
# شمارهٔ ۶۲۱

<div class="b" id="bn1"><div class="m1"><p>در جهان دبدبهٔ عشقِ من افتاد چو صور</p></div>
<div class="m2"><p>شد چنین قصّهٔ من در همه عالم مشهور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من چنین ساکن و آزاد و شکیبا و صبور</p></div>
<div class="m2"><p>متّهم گشته به تفلید و به بهتان و به زور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رؤیتی هست مرا راست بگویم با نور</p></div>
<div class="m2"><p>رؤیتی کز نظرِ اهلِ ریا باشد دور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر نظر طاقتِ آن نور ندارد به ظهور</p></div>
<div class="m2"><p>دیده‌ور دارد نادیده‌وران را معذور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناید از راهِ نظر در نظرِ ما منظور</p></div>
<div class="m2"><p>سَترِ ما پاره و او از نظرِ ما مستور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلقِ عالم شده مستغرقِ دریایِ غرور</p></div>
<div class="m2"><p>طالبِ گوهر و گنجور نهنگانِ غیور</p></div></div>