---
title: >-
    شمارهٔ ۳۱۰
---
# شمارهٔ ۳۱۰

<div class="b" id="bn1"><div class="m1"><p>مشتاق دوست را ره مقصد دراز نیست</p></div>
<div class="m2"><p>تسلیم کرده را زبلا احتراز نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچش ز روزگار نباشد تمتّعی</p></div>
<div class="m2"><p>آن کس که مبتلای بتی دلنواز نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرجا که در محامد محمود دم زنند</p></div>
<div class="m2"><p>بی آفت کرشمه ی حسن ایاز نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر گرچه نزد عقل شریف است و زر عزیز</p></div>
<div class="m2"><p>این بی شکنجه نبود و آن بی گداز نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کس را چه اختیار ،همه اوست، غیر او</p></div>
<div class="m2"><p>این لقمه جز به حوصله ی اهل راز نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش روزگار سوختگان نیازمند</p></div>
<div class="m2"><p>زاد طریق صدق و صفا جز نیاز نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من عاجز و حسود مسلط ولی چه سود</p></div>
<div class="m2"><p>روزی به زور بازوی گردن فراز نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیر قضا چو کن فیکون است در نفاذ</p></div>
<div class="m2"><p>هیچ اش تعلّقی به نشیب و فراز نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تدبیر بد نکرد کس از بهر خویشتن</p></div>
<div class="m2"><p>بیچاره چون کند که به خود چاره ساز نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر من شکایتی کنم از دشمنان به دوست</p></div>
<div class="m2"><p>ننگی چنین کشیدنم آخر ز ناز نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فریاد دف هر آینه از ضربتی بود</p></div>
<div class="m2"><p>عیبش مکن که ناله ی نی بر مجاز نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنشین نزاریا که به زاری و زور و زر</p></div>
<div class="m2"><p>از شهر بند عشق کسی را جواز نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در کعبه باش و قبله به هر سو که خواه کن</p></div>
<div class="m2"><p>هرجا که هست مرد خدا بی نماز نیست</p></div></div>