---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>ای پیک مشتاقان بگو این بی دل مشتاق را</p></div>
<div class="m2"><p>تا در چمن چون یافتی آن سرو سیمین ساق را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بر گلستان بگذری آنجا که دانی با منش</p></div>
<div class="m2"><p>اکنون به بستان بیشتر خاطر کند عشاق را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر باز بینیش ای صبا گو تا تو ما را دیده ای</p></div>
<div class="m2"><p>کردیم از سودای تو از سر قدم آفاق را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با خویشتن همراه کن یک آه من تا پیش او</p></div>
<div class="m2"><p>بر احتراق سینه ام شاهد بود مصداق را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دنیا و دین بر هم زدم تا نشکند پیمان من</p></div>
<div class="m2"><p>ترسم که بدعهدی کند بر هم زند میثاق را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرلحظه آتش می زند برق سخن بر دفترم</p></div>
<div class="m2"><p>با آن که از درد دلم دل پاره شد اوراق را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آری نزاری برمکن خاطر به دوری از وفا</p></div>
<div class="m2"><p>باشد که هم روزی ز ما یاد آرد استحقاق را</p></div></div>