---
title: >-
    شمارهٔ ۴۵۶
---
# شمارهٔ ۴۵۶

<div class="b" id="bn1"><div class="m1"><p>در دهر کسی که یار گیرد</p></div>
<div class="m2"><p>یاری چو من اختیار گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خوبی تو خرد به دندان</p></div>
<div class="m2"><p>انگشت به اعتبار گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک قدم تو سعد اکبر</p></div>
<div class="m2"><p>بر دیده به افتخار گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نا خورده می از دو چشم مستت</p></div>
<div class="m2"><p>هُشیاران را خمار گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن را که تو در میان جانی</p></div>
<div class="m2"><p>از کل جهان کنار گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با من نفسی درآی و بنشین</p></div>
<div class="m2"><p>تا خاطر من قرار گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگذار دل ستیزه خو را</p></div>
<div class="m2"><p>تا عادت روزگار گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن را که دماغ و دل نباشد</p></div>
<div class="m2"><p>هرگز سر انتظار گیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در چنگ فراق اگر نزاری</p></div>
<div class="m2"><p>آهنگ چو زیر زار گیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شک نیست که آتش دم نی</p></div>
<div class="m2"><p>در سوخته‌ء نزار گیرد</p></div></div>