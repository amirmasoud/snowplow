---
title: >-
    شمارهٔ ۸۷۵
---
# شمارهٔ ۸۷۵

<div class="b" id="bn1"><div class="m1"><p>اگر عشق تو شد در خون جانم</p></div>
<div class="m2"><p>قضا از خویشتن چون بگذرانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نداری مستیِ من هوشیاری</p></div>
<div class="m2"><p>که از مبدای فطرت هم چنانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرو بندم که هر عاقل که زلفت</p></div>
<div class="m2"><p>ببیند در نشورد من بمانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دستم برنمی خیزد نثاری</p></div>
<div class="m2"><p>ندانم تا چه در پایت فشانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم آویزشی دارد به زلفت</p></div>
<div class="m2"><p>غمت آمیزشی دارد به جانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طمع دارم زمین بوسی دگر بار</p></div>
<div class="m2"><p>اگر مهلت دهد دورِ زمانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امیدم باز میدارد خیالت</p></div>
<div class="m2"><p>بکوشم هم مگر ضایع نمانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمانه گرچه هم توسن رکاب است</p></div>
<div class="m2"><p>مگر بر بخت گرداند عنانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلم آبی کند هر روز در گِل</p></div>
<div class="m2"><p>وزین پس سر نخواهم شست دانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدان می آردم غیرت که نارد</p></div>
<div class="m2"><p>دگر نام نزاری بر زبانم</p></div></div>