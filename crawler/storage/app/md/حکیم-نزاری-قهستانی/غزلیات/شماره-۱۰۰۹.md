---
title: >-
    شمارهٔ ۱۰۰۹
---
# شمارهٔ ۱۰۰۹

<div class="b" id="bn1"><div class="m1"><p>جانِ من و عقلِ من و هوشِ من</p></div>
<div class="m2"><p>هر سه به یک ره شده فرتوشِ من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صاعقۀ عشق درآمد بسوخت</p></div>
<div class="m2"><p>خوابِ من وخوردِ من و توشِ من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر رهِ امید و ندای نجات</p></div>
<div class="m2"><p>چند بود چشمِ من و گوشِ من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقیِ خم خانۀ وحدت کجاست</p></div>
<div class="m2"><p>تا بنهد بر کفِ من نوشِ من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نکند دوست نظر ضایع است</p></div>
<div class="m2"><p>سعیِ من و جهدِ من و کوشِ من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آه که نتوان به کسی باز گفت</p></div>
<div class="m2"><p>زآن که ببرده‌است زمن هوشِ من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سروِ روانی که نگنجد ز قدر</p></div>
<div class="m2"><p>در همه عالم نه در آغوشِ من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قدرِ من امروز چه دانی که قدر</p></div>
<div class="m2"><p>باز ندانی ز شبِ دوشِ من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عیبِ نزاری چه کنی کاین عَلَم</p></div>
<div class="m2"><p>عشق ز مبداء زده بر دوشِ من</p></div></div>