---
title: >-
    شمارهٔ ۱۹۲
---
# شمارهٔ ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>می بده که در دادنِ می یاری‌هاست</p></div>
<div class="m2"><p>کار این است دگرها همه بیکاری‌هاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ آرامش و آسایش و آسانی نیست</p></div>
<div class="m2"><p>در عنا خانه دنیا همه دشواری‌هاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خلافِ فقها پیر خرابات منم</p></div>
<div class="m2"><p>که میانِ ورع و میکده بیزاری‌هاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو ملامت گرِ بی‌فایده خود را دریاب</p></div>
<div class="m2"><p>در دل‌آزاریِ عشّاق گرفتاری‌هاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیک‌بخت است که بر وی نبود پوشیده</p></div>
<div class="m2"><p>که عقوبت همه مشتق ز دل‌آزاری‌هاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درد و رنج و غم و اندوه و ملامت بر دل</p></div>
<div class="m2"><p>بارِ عشق است و بر او این همه سرباری‌هاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تلخیِ شربت هجران و ترش‌روییِ صبر</p></div>
<div class="m2"><p>شوربختا که چنین محتملِ خواری‌هاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر نزاری چو نزاری نبود بی‌زر و زور</p></div>
<div class="m2"><p>چه کند زاری و خود پیشه او زاری‌هاست</p></div></div>