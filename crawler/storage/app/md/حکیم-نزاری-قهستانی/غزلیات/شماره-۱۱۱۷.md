---
title: >-
    شمارهٔ ۱۱۱۷
---
# شمارهٔ ۱۱۱۷

<div class="b" id="bn1"><div class="m1"><p>گر هیچ به کویِ ما کنی رای</p></div>
<div class="m2"><p>بر خانه ی چشم ما فرود آی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه نبود چو من گدا را</p></div>
<div class="m2"><p>در خوردِ نزولِ پادشا جای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان روی که خانه خانه ی تست</p></div>
<div class="m2"><p>زنهار مضایقت مفرمای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا پیش کشی کنیم حالی</p></div>
<div class="m2"><p>از حقّه ی دیده ی گهرزای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پروای کسی دگر ندارم</p></div>
<div class="m2"><p>از توبه خودم کدام پروای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دریاب که شد ز دست کارم</p></div>
<div class="m2"><p>آخر گذری کن از سرِ پای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای سرو روان به موسم گل</p></div>
<div class="m2"><p>روزی به طوافِ باغ ما آی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم بلبل را چو من در آشوب</p></div>
<div class="m2"><p>هم باغ به روی خود بیارای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گو باد نزاری و به زاری</p></div>
<div class="m2"><p>آیا بود این سعادتم وای</p></div></div>