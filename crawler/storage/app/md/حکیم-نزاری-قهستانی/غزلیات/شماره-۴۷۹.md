---
title: >-
    شمارهٔ ۴۷۹
---
# شمارهٔ ۴۷۹

<div class="b" id="bn1"><div class="m1"><p>ایلچیِ بادِ صبا می‌رسد</p></div>
<div class="m2"><p>پیکِ سلیمان ز کجا می‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهره ندارم که برید صبا</p></div>
<div class="m2"><p>راست بگویم ز کجا می‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مُنهیِ عشق است که جبرئیل‌وار</p></div>
<div class="m2"><p>دم به دم از غیب فرا می‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به که دادند زمامِ مراد</p></div>
<div class="m2"><p>ورنه به تخصیص که را می‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیک به بد می‌نرسد بد به نیک</p></div>
<div class="m2"><p>زان که سزا هم به سزا می‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جاذبهٔ سابقهٔ فطرت است</p></div>
<div class="m2"><p>هرچه به اخوانِ صفا می‌رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر که عیان است بهشتِ برین</p></div>
<div class="m2"><p>تا که به سر وقتِ رضا می‌رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر نفس از سدره به گوشِ دلم</p></div>
<div class="m2"><p>بی‌لغت و حرف ندا می‌رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زار همی نال نزاری که زار</p></div>
<div class="m2"><p>نالهٔ عشقِ تو به ما می‌رسد</p></div></div>