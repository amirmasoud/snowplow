---
title: >-
    شمارهٔ ۴۴۵
---
# شمارهٔ ۴۴۵

<div class="b" id="bn1"><div class="m1"><p>هجر با روزگار من آن کرد</p></div>
<div class="m2"><p>که صفت جز به وصل نتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دو زلفت که روزگارِ مرا</p></div>
<div class="m2"><p>گاه مجموع و گه پریشان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دو چشمت که ملک پیمان را</p></div>
<div class="m2"><p>گاه معمور و گاه ویران کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کمان دو ابرویت که مرا</p></div>
<div class="m2"><p>بر دل از غمزه تیر باران کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دو لعل لبت که جوهر عقل</p></div>
<div class="m2"><p>لقبش رشکِ زاده کان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دهانت که معجز دو لبش</p></div>
<div class="m2"><p>خاک در چشمِ آب حیوان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نسیمت که همچو روح الله</p></div>
<div class="m2"><p>در تن مرده از نفس جان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که جهان بی وجود خرم تو</p></div>
<div class="m2"><p>بر نزاری زمانه زندان کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی جمال حیات بخش تو هجر</p></div>
<div class="m2"><p>مرگ بر خاطر من آسمان کرد</p></div></div>