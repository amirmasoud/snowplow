---
title: >-
    شمارهٔ ۱۰۴۰
---
# شمارهٔ ۱۰۴۰

<div class="b" id="bn1"><div class="m1"><p>بادِ صبا بر گرفت بویِ عرق چینِ تو</p></div>
<div class="m2"><p>نافۀ مشکِ تتار نیفۀ پرچینِ تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاش که من باد می تا چو صبا هر سحر</p></div>
<div class="m2"><p>راه گذر یابمی بر سرِ بالین تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بادِ صبا نرم‌نرم گه‌گه از آن می‌وزد</p></div>
<div class="m2"><p>تا ننشیند غبار بر گلِ نسرین تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیرتِ سرو و سمن قامت و سیمای تو</p></div>
<div class="m2"><p>رشکِ ختا و ختن نافۀ مشکینِ تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صفحه رویم شود از قطراتِ مژه</p></div>
<div class="m2"><p>راست مرّصع چنانک خوشۀ پروینِ تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتِ عقل است و هوش غمزۀ خون ریزِ تو</p></div>
<div class="m2"><p>راحت جان است و دل لعلِ دُر آگین تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس که نویسند باز تجربه را عاشقان</p></div>
<div class="m2"><p>از ورقِ روزگار مهرِ من و کینِ تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاقبت از شورِ من هیچ نشد حاصلی</p></div>
<div class="m2"><p>نیست نصیبم مگر از لبِ شیرینِ تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خونِ نزاری مریز تا به کی آخر ستیز</p></div>
<div class="m2"><p>گرچه انصاف و داد نیست در آیینِ تو</p></div></div>