---
title: >-
    شمارهٔ ۱۰۶۷
---
# شمارهٔ ۱۰۶۷

<div class="b" id="bn1"><div class="m1"><p>ای مرا در آتش هجران سراپا سوخته</p></div>
<div class="m2"><p>بعد ازین چیزی نماند از من بتا ناسوخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیش از این پروای کار خود ندارم چون کنم</p></div>
<div class="m2"><p>هستم از تشویش چون پروانه پروا سوخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر جمال شمع رویت در شبستان فراق</p></div>
<div class="m2"><p>مرغ جانم بال و پر پروانه آسا سوخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد دلم مایل به روی آتش گلرنگ تو</p></div>
<div class="m2"><p>زان که باشد قابل آتش در اصلا سوخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مقام صبر و کوره ی امتحان وقت حساب</p></div>
<div class="m2"><p>مطلقاً مِن ذلکُم شد غرقه منها سوخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند سوزد برق عالم سوز عشقت جان من</p></div>
<div class="m2"><p>طاقت آتش نمی آرد خصوصا سوخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه شرح آتش سودای عشقت می دهم</p></div>
<div class="m2"><p>در سرش بی حد از آن آتش قلم را سوخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در قلم گیرد چو آتش دوده ی سودای من</p></div>
<div class="m2"><p>لا جرم خاصیتی دارد قلم با سوخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روز و شب در موکب عشق تو بر سر می¬برم</p></div>
<div class="m2"><p>جانب کلکم نگه دارد همانا سوخته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که بیند دفتر سودای من گوید عجب</p></div>
<div class="m2"><p>مشک دارد در ورق ها یا جگر یا سوخته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درس مالیخولیا می گیرم از سر چون شده ست</p></div>
<div class="m2"><p>روزن سقف دماغ از دود سودا سوخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برق آهم چون به بالا بگذرد گر بنگری</p></div>
<div class="m2"><p>شعله های آتشین بینی شررها سوخته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درد عشق و جور یار و داغ هجر و سوز دل</p></div>
<div class="m2"><p>چون روا دارد نزاری را به صد جا سوخته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هم چو مجنون با خیال روی لیلی ساخته</p></div>
<div class="m2"><p>هم چو وامق در فراق روی عذرا سوخته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برق را سوی سیاهی میل باشد زآن قبل</p></div>
<div class="m2"><p>بر دلم زد ناگهان تا شد به عذرا سوخته</p></div></div>