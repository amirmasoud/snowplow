---
title: >-
    شمارهٔ ۱۱۵۱
---
# شمارهٔ ۱۱۵۱

<div class="b" id="bn1"><div class="m1"><p>ساقیا خیز و برافروز سبک مصباحی</p></div>
<div class="m2"><p>قفل اندیشه ی بی فایده را مفتاحی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد ِ سر دارم و سودایِ دل و رنج ِ دماغ</p></div>
<div class="m2"><p>بده این مشکل پیش آمده را اصلاحی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لنگرِ خم بگشایید که کشتی ز محیط</p></div>
<div class="m2"><p>نتوان برد برون بی مددِ ملاحی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من در افتادم و از من بستد عشق مرا</p></div>
<div class="m2"><p>غرقه ای را چه محل در دهن تمساحی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرهمِ عشق جراحت بود و به نشود</p></div>
<div class="m2"><p>که مداواش محال است به هر جراحی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روح ثانیش نهادند لقب زیرا شد</p></div>
<div class="m2"><p>هر زمان راحتِ روحی دگر از هر راحی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما به اقدام همم سیر سماوات کنیم</p></div>
<div class="m2"><p>سیرِ عشاق بدیع است ز هر سیاحی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کسی لازمه ای دارد و طرزی در شعر</p></div>
<div class="m2"><p>راح را هم چو نزاری نبود مداحی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه با نور ِ قدح حاجت آن نیست و لیک</p></div>
<div class="m2"><p>ساقیا خیر و برافروز سبک مصباحی</p></div></div>