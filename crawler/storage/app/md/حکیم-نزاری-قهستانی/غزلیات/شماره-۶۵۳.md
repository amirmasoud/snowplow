---
title: >-
    شمارهٔ ۶۵۳
---
# شمارهٔ ۶۵۳

<div class="b" id="bn1"><div class="m1"><p>خوش وقت صبوحیان شب‌خیز</p></div>
<div class="m2"><p>بر دست گرفته آتش تیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش نه که آب زندگانی</p></div>
<div class="m2"><p>آبی است ولیکن آتش انگیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تلخی و هزار جان شیرین</p></div>
<div class="m2"><p>جانی و هزار ملک پرویز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دوست بیا به‌رغم دشمن</p></div>
<div class="m2"><p>گر خون من است در قدح ریز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا می به معاد خود رسد باز</p></div>
<div class="m2"><p>با خون دل منش برآمیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناگه گیرد اجل گریبان</p></div>
<div class="m2"><p>از دامن دوستان درآویز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرهیز مکن ز می بیاموز</p></div>
<div class="m2"><p>عیش و طرب و ز خود بپرهیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان است به جانی آرزومند</p></div>
<div class="m2"><p>دریاب نزاریا و مستیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنشین پس کار خویش بنشین</p></div>
<div class="m2"><p>برخیز ز هرچه هست برخیز</p></div></div>