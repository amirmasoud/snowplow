---
title: >-
    شمارهٔ ۴۳۰
---
# شمارهٔ ۴۳۰

<div class="b" id="bn1"><div class="m1"><p>کس نمی دانم که پیغامی برد</p></div>
<div class="m2"><p>یک قدم با ما به یاری بسپرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بندگی ها عرضه دارد و آن گهی</p></div>
<div class="m2"><p>از دل آ [ رامم ] سلامی آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به وصلم کی مجالی می دهد</p></div>
<div class="m2"><p>باز پرسد روی و راهی بنگرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الله الله غفلتِ بی اختیار</p></div>
<div class="m2"><p>بر من از بی التفاتی نشمرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاریی می آورد از من به دوست</p></div>
<div class="m2"><p>مرغ اگر بالایِ بامش می پرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدمتی می آورد با اشتیاق</p></div>
<div class="m2"><p>گر برو بادِ سحر گه بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دفعِ سودا را نزاری تا به کی</p></div>
<div class="m2"><p>خونِ جان با آبِ رز برهم خورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در تعجّب مانده ام تا هیچ کس</p></div>
<div class="m2"><p>دشمنِ جان چون نزاری پرورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم گران باشد اگر او را کسی</p></div>
<div class="m2"><p>از غمِ دنیا به یک جو واخَرَد</p></div></div>