---
title: >-
    شمارهٔ ۵۴۶
---
# شمارهٔ ۵۴۶

<div class="b" id="bn1"><div class="m1"><p>آن‌ها که به دوست راه دارند</p></div>
<div class="m2"><p>اسرارِ نهان نگاه دارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سیر و سلوک شرطِ عشّاق</p></div>
<div class="m2"><p>آن است که سر به راه دارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌دوست اگر دمی برآرند</p></div>
<div class="m2"><p>آن دم سرِ هر گناه دارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جا که روند دوستان‌اند</p></div>
<div class="m2"><p>دانی به چه داغِ شاه دارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشهور بود جنودِ ارواح</p></div>
<div class="m2"><p>پیوسته از آن سپاه دارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کردند به ترک‌ ترکِ دنیا</p></div>
<div class="m2"><p>زان ترک چنین کلاه دارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر ترکِ حطامِ نام [و ناموس]</p></div>
<div class="m2"><p>سد محضر پرگواه دارند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن‌جا که به عرصۀ مظالم</p></div>
<div class="m2"><p>فریاد به دادخواه دارند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن روز مگر تو را نزاری</p></div>
<div class="m2"><p>از هاویه در پناه دارند</p></div></div>