---
title: >-
    شمارهٔ ۳۳۴
---
# شمارهٔ ۳۳۴

<div class="b" id="bn1"><div class="m1"><p>رجب آمد سه ماه باید داشت</p></div>
<div class="m2"><p>شرط طاعت نگاه باید داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست از ناسزا بباید شست</p></div>
<div class="m2"><p>با خدا سر به راه باید داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدمت شرع و حرمت اسلام</p></div>
<div class="m2"><p>هر دو بی اشتباه باید داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توبه ی تن درست باید کرد</p></div>
<div class="m2"><p>بر شکستن گناه باید داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پی آب روی در محشر</p></div>
<div class="m2"><p>گونه این جا چو کاه باید داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راه پهلوی چاه می گذری</p></div>
<div class="m2"><p>نظری هم به چاه باید داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش محراب بر زمین نیاز</p></div>
<div class="m2"><p>به تضرّع جباه باید داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم باید گرفت بر واعظ</p></div>
<div class="m2"><p>گوش بر انتباه باید داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گراز این ها نکرد خواهی هیچ</p></div>
<div class="m2"><p>سر ما گاه گاه باید داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زود رفع خمار باید کرد</p></div>
<div class="m2"><p>می به دست از پگاه باید داشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نزاری سپیده دم بر کف</p></div>
<div class="m2"><p>قدحی سر سیاه باید داشت</p></div></div>