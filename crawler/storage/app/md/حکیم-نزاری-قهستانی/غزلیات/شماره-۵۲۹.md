---
title: >-
    شمارهٔ ۵۲۹
---
# شمارهٔ ۵۲۹

<div class="b" id="bn1"><div class="m1"><p>مگر صبا به فلانی سلام ما برساند</p></div>
<div class="m2"><p>که راز ما نکند فاش چونکه نامه بخواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به قاصدی چه توان گفت خاصه قصه دری</p></div>
<div class="m2"><p>که گر به کوه بگویم ز غصه خون بچکاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی که درد جدایی ز دوستان نکشیده ست</p></div>
<div class="m2"><p>هنوز تا نکشد قدر اتصال نداند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اجل کجاست که بر جان ما زند به شبی خون</p></div>
<div class="m2"><p>مرا ز خویشتن و خلق را ز من برهاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا مگوی پدر کز حبیب باز ستان دل</p></div>
<div class="m2"><p>بلی که جان بدهم نیز اگرچه دل بستاند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محبتی که موکد بود میان دو مشفق</p></div>
<div class="m2"><p>هنوز بعد قیامت هزار سال بماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ضرورت است که فرزند پند بشنود آری</p></div>
<div class="m2"><p>ولی قبول پذیرفتن ای پدر نتواند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترا فراغت و مارا درون سینه جراحت</p></div>
<div class="m2"><p>به تیر غمزه‌ء شوخی که از سپر بجهاند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی که دل به جگر گوشه ای نداد حیاتش</p></div>
<div class="m2"><p>حرام باد که عمری به هرزه می گذراند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنار دوست کسی را میسر ست گرفتن</p></div>
<div class="m2"><p>که دامن از همه آلایش غرض بفشاند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ضرورت است نزاری جفای دوست کشیدن</p></div>
<div class="m2"><p>چو سایه بر عقبش میرو ار ز پیش براند</p></div></div>