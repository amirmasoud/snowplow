---
title: >-
    شمارهٔ ۲۴۶
---
# شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>بر یادِ صبوحیانِ سر مست</p></div>
<div class="m2"><p>خواهم سر و پایِ توبه بشکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما آدمی ایم و رختِ توبه</p></div>
<div class="m2"><p>باری ست که بر خران توان بست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرمایۀ پاک باز خمرست</p></div>
<div class="m2"><p>نتوان به قمار شد تهی دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو از سرِ نام و ننگ برخیز</p></div>
<div class="m2"><p>طوفانِ بلا و فتنه بنشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای خواجه دماغت آسمانی ست</p></div>
<div class="m2"><p>بر مرکزِ دل محیط پیوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بر شکنی زهر دو بینی</p></div>
<div class="m2"><p>چون نقطۀ مرکز آسمان پست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این است مطوبّات اطباق</p></div>
<div class="m2"><p>کس هم چون من این بیان نکرده ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جور از متعصّبان بی داد</p></div>
<div class="m2"><p>سهل است اگر قیامتی هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مالک ز پس و صراط در پیش</p></div>
<div class="m2"><p>عذرا ز میانه چون توان جست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کردی چو کمان نزاریا تیر</p></div>
<div class="m2"><p>هفتاد فزوده گیر بر شست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از حال نصیبِ خویش بردار</p></div>
<div class="m2"><p>وز پیش کفافِ نسیه بفرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هش یار مرو به خاک زنهار</p></div>
<div class="m2"><p>تا برخیزی ز خاک سر مست</p></div></div>