---
title: >-
    شمارهٔ ۲۵۹
---
# شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>بس است مونسِ جانم خیالِ طلعتِ دوست</p></div>
<div class="m2"><p>که قانعم به خیالش ببین که رخ چه نکوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر چه در نگرم رویِ دوست می‌بینم</p></div>
<div class="m2"><p>مگر به دیده درون است بل که دیده خود اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسیمِ دوست رساند به من صبا هر شب</p></div>
<div class="m2"><p>حیاتِ جانم از آن خوش نسیمِ عنبر بوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم ز شوقِ تو یک تاست در وفاداری</p></div>
<div class="m2"><p>ولیک قامتم از محنتِ فراق دو توست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گل نگه نکند باز بلبلِ عاشق</p></div>
<div class="m2"><p>ز پرده گر به در آید بتم چو غنچه ز پوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرا زخویِ بدش سرزنش کنند مرا</p></div>
<div class="m2"><p>بتی بدان همه خوبی چه باشد ار بدخوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عذابِ شیفتگان نیز مصلحت بین است</p></div>
<div class="m2"><p>که بی غرض نبود سرزنش ز دشمن و دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به هیچ وجه ندارم سرِ نصیحتِ خلق</p></div>
<div class="m2"><p>ملامتِ دلِ عشّاق نیز بی‌هده گوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز مردمان چه حکایت کنم به نا واجب</p></div>
<div class="m2"><p>که هر چه بر دل و جانِ نزاری است ازوست</p></div></div>