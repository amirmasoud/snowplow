---
title: >-
    شمارهٔ ۱۰۶۸
---
# شمارهٔ ۱۰۶۸

<div class="b" id="bn1"><div class="m1"><p>لبت چشمه و خضر گردش نشسته</p></div>
<div class="m2"><p>نبات است کز طرف کوثر برسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تعالی الله آخر که دیده ست لعلی</p></div>
<div class="m2"><p>میانش ز دردانه ها رسته رسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن رسته ها گویی افتاد در شک</p></div>
<div class="m2"><p>از آن است عقد ثریا گسسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در او می¬رسد آتش آه گرمم</p></div>
<div class="m2"><p>سر زلف از تاب آن شد شکسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمانه از او داد من باز خواهد</p></div>
<div class="m2"><p>هنوز از کمند حوادث نجسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهی بخت یاری اگر هیچ روزی</p></div>
<div class="m2"><p>شوم از بلایی چنان باز رسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کارم نظر کن که هم بر امیدی</p></div>
<div class="m2"><p>نزاری به جان دل درین کاربسته</p></div></div>