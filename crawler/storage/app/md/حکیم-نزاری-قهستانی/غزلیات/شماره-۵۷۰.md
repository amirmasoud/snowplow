---
title: >-
    شمارهٔ ۵۷۰
---
# شمارهٔ ۵۷۰

<div class="b" id="bn1"><div class="m1"><p>قضا ز عشق چو نازل شود گریز چه سود</p></div>
<div class="m2"><p>دلم مکابره از دست شد ستیز چه سود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گریه گر چه مدد می دهد بسی چشمم</p></div>
<div class="m2"><p>چو دیده می رود از اشک لاله ریز چه سود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر به وقت کند دوست رحمتی ور نی</p></div>
<div class="m2"><p>چو شد عظامم در خاک ریز ریز چه سود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به صبر وعده دهد ناصحم نمی دانی</p></div>
<div class="m2"><p>که خفتگان عدم را ز رستخیر چه سود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو نقد عشق نزاری به دار ضرب رسید</p></div>
<div class="m2"><p>پس از مبالغت عقل خاک بیز چه سود</p></div></div>