---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>اگر تو تازه کنی با من آشنایی را</p></div>
<div class="m2"><p>بر افکنی ز جهان رسم بی وفایی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز راه مرحمت آن دم که از وفا گویی</p></div>
<div class="m2"><p>بسوز همچو دلم برقع جدایی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چراغ نباشد شبِ وصال چه غم</p></div>
<div class="m2"><p>زشمعِ چهره برافروز روشنایی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیار ای بت ساقی می مغانه که من</p></div>
<div class="m2"><p>ز سر به در کنم این خرقة ریایی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملامتم مکن ای پارسا که در رهِ عشق</p></div>
<div class="m2"><p>به نیم جرعه فروشند پارسایی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خوانِ وصل تو هر کو نواله ای دارد</p></div>
<div class="m2"><p>به ملک جم ندهد ملکت گدایی را</p></div></div>