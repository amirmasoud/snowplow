---
title: >-
    شمارهٔ ۱۰۱۳
---
# شمارهٔ ۱۰۱۳

<div class="b" id="bn1"><div class="m1"><p>باد صبا هر نفس تازه کند جان من</p></div>
<div class="m2"><p>کز نفسش می دمد بوی گلستان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که به سر بر زدم دست به زاری نماند</p></div>
<div class="m2"><p>بلبل شوریده را طاقت دستان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فصل ریاحین و من حبس به زندان تن</p></div>
<div class="m2"><p>جلوه کنان در چمن سرو خرامان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رای سفر زد مرا عقل به دیوانگی</p></div>
<div class="m2"><p>ای که سرآسیمه باد عقل خطادان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زود هلاکم کند درد جدایی که عشق</p></div>
<div class="m2"><p>بازنگیرد چنین دست ز دامان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جامه درم تا به روز هر شب و هر بامداد</p></div>
<div class="m2"><p>تازه غمی برکُند سر زگریبان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شعله زند بر اثیر آتش هجران دوست</p></div>
<div class="m2"><p>دود برآرد ز سر سینه ی سوزان من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عیش و نشاط و طرب جمله فراموش کرد</p></div>
<div class="m2"><p>زین همه محنت که دید خوی تن آسان من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جمع نباشد دلم خواب نیابد سرم</p></div>
<div class="m2"><p>تا نکند دیده باز بخت پریشان من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آه نزاری بسوخت پرده ی ناموس و صبر</p></div>
<div class="m2"><p>فاش نکرد ای دریغ قصه ی پنهان من</p></div></div>