---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>کنار من ز سرشک دو دیده غرقاب است</p></div>
<div class="m2"><p>که از دو دیده سرشکم روان چو سیماب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وداع کردم و پوشیده نیست بر عشّاق</p></div>
<div class="m2"><p>که رستخیز قیامت وداع احباب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انیس شیفته هم صحبت است در غم دوست</p></div>
<div class="m2"><p>مرا بتر که عذاب از وجود اصحاب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر مصاحب هم رنگ خویشتن باشم</p></div>
<div class="m2"><p>چراغ خلوت من شمع دان مهتاب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی خورم می و وقتی علی الضروره اگر</p></div>
<div class="m2"><p>خورم کدام می آخر چه می که خوناب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب دراز و دماغ از خیال در تشویش</p></div>
<div class="m2"><p>به خواب نیز نمی‌بینمش که را خواب است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رقیب معتکف حضرت است گو می باش</p></div>
<div class="m2"><p>که مار بر در فردوس نیز بواب است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مجال نیست توقف مرا به فرصت و او</p></div>
<div class="m2"><p>ز تاب مهر من از من همیشه در تاب است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نماز من نبود جز به روی دوست روا</p></div>
<div class="m2"><p>نه قبله راست از آن سو بود که محراب است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مقلدان همه در اعتراض و غافل از آن</p></div>
<div class="m2"><p>که من کجا ام و در حلق من چه قلاب است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نزاریا طمع از یار معتقد بگسل</p></div>
<div class="m2"><p>شنیده ای که وفا در زمانه نایاب است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بساز با خود و گر مدعی زند دم صدق</p></div>
<div class="m2"><p>غلط مشو که چو صبح نخست کذاب است</p></div></div>