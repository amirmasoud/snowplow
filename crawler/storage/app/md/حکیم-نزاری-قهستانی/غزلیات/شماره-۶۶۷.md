---
title: >-
    شمارهٔ ۶۶۷
---
# شمارهٔ ۶۶۷

<div class="b" id="bn1"><div class="m1"><p>دریغ صحبت یاران و دوستان انیس</p></div>
<div class="m2"><p>دریغ عمر گرامی و روزگار نفیس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدارِ دور به هم برزد استقامتِ من</p></div>
<div class="m2"><p>ز گفته‌های شنیع و ز کرده‌های خسیس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر شنیده نبودم که در بهشت برین</p></div>
<div class="m2"><p>به مکر و شعبده با بوالبشر چه کرد ابلیس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوای هاویه بر من مگر مسلّط شد</p></div>
<div class="m2"><p>که شد مدبّر عقلم مسخّر تلبیس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمانه داشت مجنّس حساب ما یک‌چند</p></div>
<div class="m2"><p>کنون به تفرقه خطّی نهاد بر تجنیس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امید نیست که باز آورد به هیچ سبیل</p></div>
<div class="m2"><p>بریدِ باد نسیمی ز روضهٔ بلقیس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بی معامله سودا چه می‌پزند کسان</p></div>
<div class="m2"><p>که نیست نقد وفایی زمانه را در کیس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تراب و رمل به سر بر چه سود اگر ریزم</p></div>
<div class="m2"><p>چو در مقامه ی هفتم دوباره شد اِنکیس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو احتراق زحل جان من بسوخت چه سود</p></div>
<div class="m2"><p>ز استقامت تیر و سعادت برجیس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تراب بر سرِ من گر سرم فرود آید</p></div>
<div class="m2"><p>به هندویی که بر افلاک و انجم است رئیس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نزاریا که نگویم دگر ز انجم و چرخ</p></div>
<div class="m2"><p>خطی چنین به گواهی انجمن بنویس</p></div></div>