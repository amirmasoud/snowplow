---
title: >-
    شمارهٔ ۲۱۱
---
# شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>به روزگار شبی گر دهد وصالم دست</p></div>
<div class="m2"><p>خروس بانگ برارد سبک نباید جست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ستیزه ی شب وصل آمدست روز فراق</p></div>
<div class="m2"><p>مدارِ دور بر این نقطه می رود پیوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو هیچ ماهرخی نیست بی رقیب و ذنب</p></div>
<div class="m2"><p>چرا رضا ندهم بر قضا به حکم الست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان ما و غم دوست در خروج و دخول</p></div>
<div class="m2"><p>به جان دوست اگر هیچ امتناعی هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رقیب گفت برفتی و توبه بشکستی</p></div>
<div class="m2"><p>که دور چشم بد از توبه ی تو توبه پرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز درد طعنه ی او گفتمش تو را چه زیان</p></div>
<div class="m2"><p>اگر درست بود توبه ی من ار بشکست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهانیان بشنیدند و آفرین کردند</p></div>
<div class="m2"><p>زهی نزاری قلّاشِ رندِ عاشقِ مست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قلندری ام وشنگوَلی و خراباتی</p></div>
<div class="m2"><p>به زور، زهدی بر خود نمیتوانم بست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسا که در حقِ ما محتسب چو صبحِ نخست</p></div>
<div class="m2"><p>دروغ گفت ولیکن به راستی بنشست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همان به است که انصاف خویشتن بدهم</p></div>
<div class="m2"><p>که بی جواز ره از باژخواه نتوان رست</p></div></div>