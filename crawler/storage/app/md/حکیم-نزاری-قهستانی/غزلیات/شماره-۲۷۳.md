---
title: >-
    شمارهٔ ۲۷۳
---
# شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>به جان رسید دلم در فراق هان ای دوست</p></div>
<div class="m2"><p>ترحّمی کن اگر هیچ می توان ای دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر تو برشکنی دشمنان به کام رسند</p></div>
<div class="m2"><p>به دوستی که مکن ترکِ دوستان ای دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آن قرار برفتی که زود بازآیی</p></div>
<div class="m2"><p>بیا به قول وفا کن بدین نشان ای دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به وصل اگر زمیانِ توم کناری نیست</p></div>
<div class="m2"><p>کنارِ من ز سرشک است تا میان ای دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خبر چه گویمت از جانِ خسته و دلِ تنگ</p></div>
<div class="m2"><p>چو در کنارِ دلی در میانِ جان ای دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از میان بروم چون تو در کنار آیی</p></div>
<div class="m2"><p>من و تو مَجمعِ اضداد و یک مکان ای دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من و تو هر دو به هم شرک و وحدت اینت محال</p></div>
<div class="m2"><p>و گر به شکل بوم با تو توأمان ای دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همان نزاریِ شوریدۀ توم زنهار</p></div>
<div class="m2"><p>اگر مشابهتی گفتم الامان ای دوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درونِ جانی و بل جانِ جان و می کوشم</p></div>
<div class="m2"><p>که مدّعی نبرد بر من این گمان ای دوست</p></div></div>