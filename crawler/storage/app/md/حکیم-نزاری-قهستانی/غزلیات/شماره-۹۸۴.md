---
title: >-
    شمارهٔ ۹۸۴
---
# شمارهٔ ۹۸۴

<div class="b" id="bn1"><div class="m1"><p>ای عشق پروریده تو را درکنارِ حسن</p></div>
<div class="m2"><p>وی رسته سر و قدِّ تو بر جویبارِ حسن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملکِ جمال بر تو مقرّر شود به حکم</p></div>
<div class="m2"><p>گرپایْ مردِ لطف کنی دستْیارِ حسن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم عاقبت وصال میّسر شود که شد</p></div>
<div class="m2"><p>زان پس که حزن داشت به وصل انتظارِ حسن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با ما به حسنِ عهد وفا کن که روزگار</p></div>
<div class="m2"><p>تا بنگری خراب کند روزِگار حسن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تشنگان سوخته‌ی خود به شربتی</p></div>
<div class="m2"><p>گو تا مضایقت نکند آبْ‌دارِ حسن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>احسان کنند تا نشود حسن منقطع</p></div>
<div class="m2"><p>کاحسان بود به وجهِ حَسَن یادگارِ حسن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مانع مشو نزاری شاهد پرست را</p></div>
<div class="m2"><p>کو را قرار نیست مگر در جوار حسن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر طرفِ گل‌سِتان جهان تا بود گلی</p></div>
<div class="m2"><p>مشنو که از سرش برود خار خارِ حسن</p></div></div>