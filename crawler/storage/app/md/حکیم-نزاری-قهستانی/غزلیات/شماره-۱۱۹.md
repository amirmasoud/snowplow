---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>بر من شب فراق چو روز قیامت است</p></div>
<div class="m2"><p>در رستخیز عشق چه جای ملامت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون من گر آورد متنعّم شبی به روز</p></div>
<div class="m2"><p>داند که بر محال مشنّع غرامت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نا ایمن است راه ملامت گران عشق</p></div>
<div class="m2"><p>آنجا مرو که حاصل ظالم ندامت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تحویل ما ز کوی خرابات کی بود</p></div>
<div class="m2"><p>باری هنوز نیت من بر اقامت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنها نه بر من است به تخصیص می حرام</p></div>
<div class="m2"><p>آری به حکم شرع نبی بر تمامت است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون است مطلقا مثل میل من به می</p></div>
<div class="m2"><p>چون معتقد که پس رو نص امامت است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرهیز کن اخی ز خلاف منافقان</p></div>
<div class="m2"><p>دور از دری که عاقبتش بر وخامت است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عادت شده ست بی هده گفتن جهول را</p></div>
<div class="m2"><p>تقلید علتی ست که جهلش علامت است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم خانه ی نهنگ بلایی نزاریا</p></div>
<div class="m2"><p>در موج قلزمت چه امید سلامت است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ره پر حرامیان و بیابان عشق پیش</p></div>
<div class="m2"><p>جهل است هر که را هوس استقامت است</p></div></div>