---
title: >-
    شمارهٔ ۸۷۱
---
# شمارهٔ ۸۷۱

<div class="b" id="bn1"><div class="m1"><p>نکرد با سر زلف تو هیچ کار دلم</p></div>
<div class="m2"><p>ببرد در طلب وصل روزگار دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آتش است درونم چو کوره ی حداد</p></div>
<div class="m2"><p>چگونه بر سر آتش کند قرار دلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در انتظار خلاصی ز تنگنای وجود</p></div>
<div class="m2"><p>به گردِ سینه برآید هزار بار دلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان که مادر مشفق عزیز فرزندی</p></div>
<div class="m2"><p>غمت به مهر گرفته ست در کنار دلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو نیست منزلش اندر خورِ نزول غمت</p></div>
<div class="m2"><p>بود ز روی خیال تو شرم سار دلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کدام دل، ز کجا دل، که راست دل، کو دل</p></div>
<div class="m2"><p>که از دو دیده برون کردی ای نگار دلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو قطره قطره برون شد ز دیده چون گویم</p></div>
<div class="m2"><p>ز من مکابره بر بوده ای بیار دلم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر اختیار دل از دست پیش ازین رفته ست</p></div>
<div class="m2"><p>کنون ز دست بشد هم چو اختیار دلم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان مکن که به حسرت فرو شود جانم</p></div>
<div class="m2"><p>که بس به درد فرو شد به انتظار دلم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه سود اگرچه بگویی بسی ز بعد وفات</p></div>
<div class="m2"><p>که از وفات نزاری بسوخت زار دلم</p></div></div>