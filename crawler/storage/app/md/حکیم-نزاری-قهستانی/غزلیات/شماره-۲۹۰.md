---
title: >-
    شمارهٔ ۲۹۰
---
# شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>قدت بر ماهِ تابان سر کشیده ست</p></div>
<div class="m2"><p>چنین سروِ خرامان کس ندیده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تعالی الله خداوندی که از خاک</p></div>
<div class="m2"><p>چنین نازک وجودی آفریده ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خنک دستی که از طوبایِ قدّت</p></div>
<div class="m2"><p>به شفتالو گرفتن بر رسیده ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلی پرنارِ غیرت نارِ بُستان</p></div>
<div class="m2"><p>زرشکِ نارِ پستانت کفیده ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سودایِ خطِ سبزِ تو شب ها</p></div>
<div class="m2"><p>قلم در دستِ من بر سر دویده ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دهانت چشمۀ خضرست از آن روی</p></div>
<div class="m2"><p>که گردش سبزۀ خط بر دمیده ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کنارم عاقبت پر شد ز یاقوت</p></div>
<div class="m2"><p>ز بس خوناب کز چشمم چکیده ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نزاری را که بر تست از جهان چشم</p></div>
<div class="m2"><p>غبارِ خاکِ کویت کحل دیده ست</p></div></div>