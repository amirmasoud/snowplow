---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>ز من سرگشته در کویِ خرابات</p></div>
<div class="m2"><p>چه می خواهند اصحابِ کرامات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز فطرت نیست بیرون آفرینش</p></div>
<div class="m2"><p>یکی گنگ و یکی صاحب مقالات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندایِ لَن ترانی چون شنیدی</p></div>
<div class="m2"><p>مکن اصرار بر طورِ مناجات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مقامِ عاشقان جایی ست بی جای</p></div>
<div class="m2"><p>ز خود بیرون شو و رفتی به میقات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر عاشق نیی زفتی فسرده</p></div>
<div class="m2"><p>وگر هستی چه می خواهی ز طامات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دوران در میفکن خویشتن را</p></div>
<div class="m2"><p>وگر افتاده ای هیهات هیهات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندانم تا کی ای آسیمه سر باز</p></div>
<div class="m2"><p>برون آیی ز دورانِ سماوات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مجاریِ دماغ ار عقل داری</p></div>
<div class="m2"><p>بپرداز از محالات و خیالات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وگر در بندِ خویشی چاره یی کن</p></div>
<div class="m2"><p>برون آی از خیالات و محالات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وگر بیرون شدی یک باره از خویش</p></div>
<div class="m2"><p>شدی فی الجمله مستغرق در آن ذات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به اِلّا گر رسیدی رستی از لا</p></div>
<div class="m2"><p>ازین جا بازدانی نفی و اثبات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نزاری زنده دل را دوست دارد</p></div>
<div class="m2"><p>نه میّت را چون رهبان عزّی ولات</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دو چشم از هرچه غیرِ اوست بربند</p></div>
<div class="m2"><p>اگر با دوست می خواهی ملاقات</p></div></div>