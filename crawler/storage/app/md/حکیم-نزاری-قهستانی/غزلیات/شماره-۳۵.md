---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>برفت و بر سر آتش نشاند یار مرا</p></div>
<div class="m2"><p>به پای حادثه افکند روزگار مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر آشکار کند آب دیده راز دلم</p></div>
<div class="m2"><p>میان آتش سوزان چه اختیار مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان نکرد کمند بلای عشقم صید</p></div>
<div class="m2"><p>که قید عقل کند بعد از این شکار مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می فکن از نظر عزّتم چنین ای دوست</p></div>
<div class="m2"><p>که دوستان همه بگذاشتند خوار مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمانه را چه حسد بود در میانه ز من</p></div>
<div class="m2"><p>که کنار تو افکند بر کنار مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از تو هیچ دگر جز همین نمی خواهم</p></div>
<div class="m2"><p>مباش بی من و بی خویشتن مدار مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تویی مرادِ من از کاینات و موجودات</p></div>
<div class="m2"><p>به هر چه غیر تو باشد چه کار مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موکّلان خیالت نمی هلند دمی</p></div>
<div class="m2"><p>که بی وجود تو جایی بود قرار مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلی پر آتش و چشمی پر آب خواهم رفت</p></div>
<div class="m2"><p>شهیدم ار بکشد دردِ انتظار مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به شفقت تو نزاری امیدها دارد</p></div>
<div class="m2"><p>روا مدار چنین نا امیدوار مرا</p></div></div>