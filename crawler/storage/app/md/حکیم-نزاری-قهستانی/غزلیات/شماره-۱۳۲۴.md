---
title: >-
    شمارهٔ ۱۳۲۴
---
# شمارهٔ ۱۳۲۴

<div class="b" id="bn1"><div class="m1"><p>اگر ز قصه ی ما یک ورق فرو خوانی</p></div>
<div class="m2"><p>عجب ز صورتِ احوالِ ما فرو مانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شوق اگر چه دلم در جهان نمی‌گنجد</p></div>
<div class="m2"><p>ولی تو در دلِ تنگم نشسته چون جانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جان مضایقتی نیست بنده بنده ی تست</p></div>
<div class="m2"><p>همین بس است که از حالم این قدر دانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حدیثِ زلفِ تو گفتم به حلقه عشاق</p></div>
<div class="m2"><p>که در سواد وی است آفتابِ پنهانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قیامت از همه برخاست از تغلّبِ شوق</p></div>
<div class="m2"><p>میانِ جمع که دید این همه پریشانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تعجبی دگرست این که حلقه حلقه ی او</p></div>
<div class="m2"><p>به گردِ گویِ زنخ‌‌دان شده‌ست چوگانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز چشمِ مستِ تو گفتم حکایتی و شدند</p></div>
<div class="m2"><p>به حالتی که چه گویم ز فرطِ حیرانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجب تر این که سیاهی گرفت مسندِ ترک</p></div>
<div class="m2"><p>ز یک تبار به جسمانی و به روحانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز سیمِ دستِ تو کردم به رمز تشبیهی</p></div>
<div class="m2"><p>خرد به طعنه به من گفت جانی و کانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرِ بلا قدِ بالای تست اولا آن</p></div>
<div class="m2"><p>که از خدای بترسی و فتنه بنشانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شبی حکایتِ تشویشِ عشق می‌کردم</p></div>
<div class="m2"><p>خیال گفت مگر بی خبر ز طوفانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز صد شجاع یکی در مصافِ عشق هنوز</p></div>
<div class="m2"><p>ندیده‌ای چو ببینی عنان بگردانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>محیطِ عشق و تو بیگانه ز آشنا زنهار</p></div>
<div class="m2"><p>مرو دراو و براندیش از پشیمانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نزاریا سرِ خود گیر و از بلا بگریز</p></div>
<div class="m2"><p>تو با حریفِ قوی ، پنجه کرد نتوانی</p></div></div>