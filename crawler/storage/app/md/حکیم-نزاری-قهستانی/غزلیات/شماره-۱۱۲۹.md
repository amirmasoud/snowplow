---
title: >-
    شمارهٔ ۱۱۲۹
---
# شمارهٔ ۱۱۲۹

<div class="b" id="bn1"><div class="m1"><p>بازم افتاد دلِ ممتحن اندر تابی</p></div>
<div class="m2"><p>از غم ماه‌جبینی شده‌ام مهتابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز از آشوبِ جهانی که نمی‌یارم گفت</p></div>
<div class="m2"><p>قصّه‌ ای دارم و دارد صفتش اطنابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جفت ابروش که طاق است چو دیدم گفتم</p></div>
<div class="m2"><p>قبله ی خویش توان کرد چنین محرابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش بنمود به من گیسو و گفتم به حکیم</p></div>
<div class="m2"><p>کس پریشان‌تر ازین گفت نبیند خوابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود چه گویم ز دهانش که چو من تشنه بسی</p></div>
<div class="m2"><p>جان بدادند و از آن چشمه نخوردند آبی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گو چنین باش بقا باد غمِ هجران را</p></div>
<div class="m2"><p>گر میسّر نشود وصل به هیچ اسبابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌نهایت نبود هیچ بدایت الّا</p></div>
<div class="m2"><p>قلزمِ عشق که آن را نبود پایابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشقی بیش جگر خوردن و جان کندن نیست</p></div>
<div class="m2"><p>گر همه حیف و جفایی بود از بوّابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مهلتی باید و عمری که نزاری شرحی</p></div>
<div class="m2"><p>باز گوید که چه‌ها می‌کشد از هر بابی</p></div></div>