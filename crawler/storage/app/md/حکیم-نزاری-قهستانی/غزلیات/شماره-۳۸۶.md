---
title: >-
    شمارهٔ ۳۸۶
---
# شمارهٔ ۳۸۶

<div class="b" id="bn1"><div class="m1"><p>ساقی ز بامداد بیاور غذای روح</p></div>
<div class="m2"><p>ماییم و هر دو عالم و توزیع یک صبوح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر یاد دوستان حقیقی علی الخصوص</p></div>
<div class="m2"><p>می بر طلوع صبح علی الله زهی فتوح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویند رفت پنجه در شصت توبه کن</p></div>
<div class="m2"><p>من توبه کرده ام ز چه از توبه ی نصوح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرد آن گهی ز خود به در آید که مطلقا</p></div>
<div class="m2"><p>داند که چیست چشمه و کشتی و خضر و نوح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر مطلع اختصار کن این جا نزاریا</p></div>
<div class="m2"><p>ساقی ز بامداد بیاور غذای روح</p></div></div>