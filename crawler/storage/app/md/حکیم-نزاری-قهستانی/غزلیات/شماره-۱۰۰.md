---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>یاد باد آن شب که خوش کردم شبت</p></div>
<div class="m2"><p>گر چه ناخوش کرده بد آن شب تبت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاسه ی تب خال بر می داشتم</p></div>
<div class="m2"><p>نفیِ تهمت را به دندان از لبت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غایبی از چشم و در گوشم بماند</p></div>
<div class="m2"><p>هم چنان آوازِ یا رب یاربت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاد باد آن شب که مجلس روز کرد</p></div>
<div class="m2"><p>عکسِ نورِ با فروغِ غبغبت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم چو پروین اشک می ریزم ز چشم</p></div>
<div class="m2"><p>در فراقِ زلفِ هم چون عقربت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سروبستان را تفرّج می کنم</p></div>
<div class="m2"><p>از پی بالایِ شنگِ شبشبت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان نخواهد برد می دانم چو روز</p></div>
<div class="m2"><p>از شب هجران نزاری عاقبت</p></div></div>