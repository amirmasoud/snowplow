---
title: >-
    شمارهٔ ۱۱۳۱
---
# شمارهٔ ۱۱۳۱

<div class="b" id="bn1"><div class="m1"><p>ای صورت خیالت در پیش من شرابی</p></div>
<div class="m2"><p>وی پرتو جمالت در چشمم آفتابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چشم پرخمارت در مغزِ من بخاری</p></div>
<div class="m2"><p>وز زلفِ تاب‌دارت در جانم اضطرابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب‌های تا به روزم از دیده رفته سیلی</p></div>
<div class="m2"><p>وز تابِ آتش تب در دل فتاده تابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سوز سینه بسته وز درد دل گشاده</p></div>
<div class="m2"><p>بر هر نفس شراری وز هر مژه زهابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حلاج‌وار خواهم بر دار عشق خود را</p></div>
<div class="m2"><p>وز حلقه‌های زلفت در حلق من طنابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک ره نصیحتی کن خورشید حسنِ خود را</p></div>
<div class="m2"><p>تا فتنه برنخیزد گو برفکن نقابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوزِ تو در سرِ من صوری‌ست در قیامت</p></div>
<div class="m2"><p>مهرِ تو در دل من گنجی‌ست در خرابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آخر به کویِ وصلت دریوزه چند آرم</p></div>
<div class="m2"><p>یا زود کن ثوابی یا باز ده جوابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در کشتن نزاری تعجیل می‌نمایی</p></div>
<div class="m2"><p>آهسته‌تر نگارا گر نیست بس شتابی</p></div></div>