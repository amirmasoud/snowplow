---
title: >-
    شمارهٔ ۹۴۵
---
# شمارهٔ ۹۴۵

<div class="b" id="bn1"><div class="m1"><p>بنفشه زار برآمد ز طرفِ لاله¬ستان</p></div>
<div class="m2"><p>درونِ خطّ سیه چشمه زلال است آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدین قَدَر که کسی گفت بود و هست خدا</p></div>
<div class="m2"><p>کجا خدای¬شناسی بود خیال است آن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا و پرده برانداز تا نگاه کنند</p></div>
<div class="m2"><p>مقلّدان که بهشت است یا جمال است آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مُرید راه خدا گر طمع کند به خدا</p></div>
<div class="m2"><p>که بی دلیل به جایی رسد محال است آن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر التجا به کسی دارد و یقین داند</p></div>
<div class="m2"><p>که هیچ نیست به خود غایت کمال است آن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مرد راه که حبل الله است در زن دست</p></div>
<div class="m2"><p>به هرچه دست به خود درزنی جوال است آن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خون خلق گناهی حرام تر نبود</p></div>
<div class="m2"><p>اگر به قول برادر کنی حلال است آن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نزاری ار همه حج می کنی چو نشناسی</p></div>
<div class="m2"><p>که از کجا به کجا می روی وبال است آن</p></div></div>