---
title: >-
    شمارهٔ ۲۶۹
---
# شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>یارب آن خلدست یا رویِ جهان آرایِ دوست</p></div>
<div class="m2"><p>یارب آن سروِ خرامان است یا بالایِ دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب آن عشق است یا مهرست یا شوق است چیست</p></div>
<div class="m2"><p>بند بر بندم چنین در بندِ سر تا پایِ دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل دمی خرّم نباشد گر نبیند رویِ یار</p></div>
<div class="m2"><p>دیده در عالم نبیند گر نباشد رایِ دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوست را بر جانِ من حکم است گو در جان نشین</p></div>
<div class="m2"><p>کز میانِ جان کنم بر دیده و دل جایِ دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فارغم از وعدهٔ فردا چو حالی حاضر است</p></div>
<div class="m2"><p>کی بود دیوانگان را طاقت فردای دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر دمی ابرو ترش دارد به شیرینی رواست</p></div>
<div class="m2"><p>شورشی دارد به غایت تلخیِ صفرایِ دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دشمنم گو خونِ دل می خور که من در زیر چنگ</p></div>
<div class="m2"><p>باده خواهم خورد بر رویِ جهان آرایِ دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلق می گوید نزاری زندگی پر مشغله ست</p></div>
<div class="m2"><p>راستی پروایِ خلقم نیست جز پروای دوست</p></div></div>