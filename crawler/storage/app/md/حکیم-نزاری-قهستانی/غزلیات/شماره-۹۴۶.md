---
title: >-
    شمارهٔ ۹۴۶
---
# شمارهٔ ۹۴۶

<div class="b" id="bn1"><div class="m1"><p>ساقی فدای جان تو بادا هزار جان</p></div>
<div class="m2"><p>بر نیم جان تشنه ی ما زن بیار جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستغرق محیط خیالیم و کس نبرد</p></div>
<div class="m2"><p>زین بحر جز به کشتی می برکنار جان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار آن بود که چون دم اخلاص زد به صدق</p></div>
<div class="m2"><p>در دوستی دریغ ندارد ز یار جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود مستعد بود به وفا هم چو یار دوست</p></div>
<div class="m2"><p>خود معتقد کند چو در افتد نثار جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جامی به کف گرفته و جانی فدای دوست</p></div>
<div class="m2"><p>جز بهر دوست باز نیاید به کار جان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کو مجلسی که نبودش اغیار در کنار</p></div>
<div class="m2"><p>تا در میان نهیم به شکرانه وار جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساغر بیار ساقی و گو زود نوش کن</p></div>
<div class="m2"><p>بر باد و خاک و آب و هوای نهار جان</p></div></div>