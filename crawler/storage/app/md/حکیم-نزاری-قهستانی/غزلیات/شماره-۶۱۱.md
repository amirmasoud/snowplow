---
title: >-
    شمارهٔ ۶۱۱
---
# شمارهٔ ۶۱۱

<div class="b" id="bn1"><div class="m1"><p>یار با ما نه چنان بود که هر بارِ دگر</p></div>
<div class="m2"><p>ترک ما کرد گرفته‌ست مگر یارِ دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وعدۀ وصل همی‌داد و نمی‌کرد وفا</p></div>
<div class="m2"><p>داشت هر روز بیاراسته بازارِ دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته بود از منش این‌بار دری نگشاید</p></div>
<div class="m2"><p>گو برو از پیِ یاری دگر و کارِ دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خدا شرم ندارد که روا می‌دارد</p></div>
<div class="m2"><p>هر نفس بر تنِ رنجورِ من آزارِ دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌روم دامنِ دل‌چاک و گریبانِ وصال</p></div>
<div class="m2"><p>تا کجا و کی و چون دست دهد بارِ دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من به صد جور از او روی نپیچم گرچه</p></div>
<div class="m2"><p>هرکس از رویِ دگر می‌کند انکارِ دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاریِ زارِ نزاری بکند هم اثری</p></div>
<div class="m2"><p>زارتر خود ز نزاری نبود زارِ دگر</p></div></div>