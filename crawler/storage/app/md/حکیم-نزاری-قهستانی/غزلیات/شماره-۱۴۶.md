---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>زبان بربسته ای با ما که جنگ است</p></div>
<div class="m2"><p>نمی دانم دگر بار این چه ننگ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل صاحب دلان خون کردی آخر</p></div>
<div class="m2"><p>چه دل داری کدامین دل چه سنگ است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سگ آهوی وصلت می توان بود</p></div>
<div class="m2"><p>ولی خوی رقیب بد پلنگ است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شگفت است اینکه تو هرلحظه ما را</p></div>
<div class="m2"><p>به دم در می کشی و او چون نهنگ است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم در عالمی با این مسافت</p></div>
<div class="m2"><p>چرا گر چشم شوخت نیست تنگ است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به من گر زهر خواهی داد نوش است</p></div>
<div class="m2"><p>و گر شهدم دهد غیری شرنگ است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عوام الناس می گویند کان شیخ</p></div>
<div class="m2"><p>چرا مَی می خورد گر اهل رنگ است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به مسجد کی روم چون در خرابات</p></div>
<div class="m2"><p>شراب و شاهد و آواز چنگ است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر آهو چشم کز پیشم گذر کرد</p></div>
<div class="m2"><p>دلم در پی چو سگ در پالهنگ است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به هم برزن نزاری نام و ننگت</p></div>
<div class="m2"><p>که این چندین حجاب از نام و ننگ است</p></div></div>