---
title: >-
    شمارهٔ ۲۵۳
---
# شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>چه کنم با دلِ شوریدهٔ دیوانهٔ مست</p></div>
<div class="m2"><p>که دگر باره سرآسیمه شد و رفت از دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش از این گر به‌جوانی قدمی می‌رفتی</p></div>
<div class="m2"><p>گفتمی آری در طبعِ جوان شوری هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز پیرانه سرم واقعه‌ای پیش آورد</p></div>
<div class="m2"><p>که نخواهد ز بلایی که در افتاد برست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش توبه کن ای غافل از این دل‌بازی</p></div>
<div class="m2"><p>کرد و ناکرد همان است و همان باز شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>التفاتش نه و شرمش نه و تیمارش نه</p></div>
<div class="m2"><p>کارش این است و جزین کار ندارد پیوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر دلِ هر دلی آخر چه مُعوَّل باشد</p></div>
<div class="m2"><p>مگس است این مثلِ عام که بر مار نشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه کند در خمِ ابرویِ کمان افتاده</p></div>
<div class="m2"><p>هیچ دل جان نبرد عاقبت ارغمزه پرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غصّه‌ها دارم از آن رفته و برگشته زمن</p></div>
<div class="m2"><p>غبنِ صیّاد بود صید که از دام بجست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبغه‌الله نتوان کرد به تزویر دگر</p></div>
<div class="m2"><p>نقشِ آموخته از ما نتوان بر ما بست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر کسی هیچ حرج نیست نزاری خاموش</p></div>
<div class="m2"><p>همه مستیِّ قدیم است ز مبدایِ الست</p></div></div>