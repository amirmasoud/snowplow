---
title: >-
    شمارهٔ ۱۰۲۳
---
# شمارهٔ ۱۰۲۳

<div class="b" id="bn1"><div class="m1"><p>ای ز بهشت بی خبر رونق نو بهار بین</p></div>
<div class="m2"><p>رونق نو بهار چه جنت آشکار بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مریم باغ حامله عیسی غنچه در چمن</p></div>
<div class="m2"><p>مرده زنده دیده ای معجز نوبهار بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بید کشیده از میان تیغ چو آب و لاله را</p></div>
<div class="m2"><p>از رگ دیده خون دل ریخته در کنار بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوسن صد زبان نگر برگ شکوفه در دهن</p></div>
<div class="m2"><p>نرگس شوخ چشم را بر لب جویبار بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بر خار غنچه را دختر حامله نگر</p></div>
<div class="m2"><p>بر لب جو بنفشه را مادر سوگوار بین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ار صدف چمن نگر صورت آفتاب و پس</p></div>
<div class="m2"><p>بر سر سبزه زان صدف لولوی تر نثار بین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فاخته از فراز سرو آمده در سخن نگر</p></div>
<div class="m2"><p>بلبل گل پرست بر گل شده بی قرار بین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رغبت ار به بوستان نیست به گور خانه رو</p></div>
<div class="m2"><p>بازگشای دیده را در نگر اعتبار بین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محتسب دغا نگر بی خبر ست گو بیا</p></div>
<div class="m2"><p>بر کف صوفی صفا باده ی خوش گوار بین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روز نو و می کهن روزه کدام و توبه چه</p></div>
<div class="m2"><p>مفتی شهر گو بیا تایب روزه دار بین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در عمل طرب گران معنی ساحری نگر</p></div>
<div class="m2"><p>در قدح معاشران آتش آبدار بین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بربط عیش دیگران ساخته زهره طرب</p></div>
<div class="m2"><p>ناله من چو زیر چنگ از رگ سینه زار بین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مایه ی زندگانیم رفته و در فراق او</p></div>
<div class="m2"><p>بر سر ره دو چشم من باز بمانده زار بین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر چه خلاف قول خود کرد شکسته خاطرم</p></div>
<div class="m2"><p>رغم مرا و خصم را عهد من استوار بین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دیده تو نزاریا بر تو حجاب می شود</p></div>
<div class="m2"><p>کج نظری ز توست بی دیده به روی یار بین</p></div></div>