---
title: >-
    شمارهٔ ۴۵۵
---
# شمارهٔ ۴۵۵

<div class="b" id="bn1"><div class="m1"><p>دولتِ آن کس که ترکِ جاه بگیرد</p></div>
<div class="m2"><p>یوسفِ جان را ز قعرِ چاه بگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم چو من از طاعت و گناه نپرسد</p></div>
<div class="m2"><p>ترکِ خرابات و خانقاه بگیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل به جگر گوشه یی دهد که دو چشمش</p></div>
<div class="m2"><p>مملکتِ جان به یک نگاه بگیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که به جز غمزۀ تو باک ندارد</p></div>
<div class="m2"><p>زلفِ تو هر دل که در پناه بگیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند رقیب از برابرِ تو یه استد</p></div>
<div class="m2"><p>راست چو فرزین که پیشِ شاه بگیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او چو سپر گو حجاب شو که به فرصت</p></div>
<div class="m2"><p>تیرِ نظر خود نشانه گاه بگیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دام برافکن ز چشم و روی مپوشان</p></div>
<div class="m2"><p>رسم نباشد که روز ماه بگیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوسه به من می دهوبه گردنِ من کن</p></div>
<div class="m2"><p>هرچه خدایت بدین گناه بگیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می کُشی و می روی بترس که ناگه</p></div>
<div class="m2"><p>مظلمه ی عاجزی ات راه بگیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دامنِ آلودۀ تو خونِ نزاری</p></div>
<div class="m2"><p>روزِ قیامت به داد خواه بگیرد</p></div></div>