---
title: >-
    شمارهٔ ۱۰۵۸
---
# شمارهٔ ۱۰۵۸

<div class="b" id="bn1"><div class="m1"><p>آخر ای راحتِ جان دردِ دلِ ما بشنو</p></div>
<div class="m2"><p>سخنی چند از این بی دلِ شیدا بشنو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر چه می پیچی و گردن چه کشی از تسلیم</p></div>
<div class="m2"><p>طرفه پندی دهمت بی غرض از ما بشنو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوش بگشای ولی چشم ز نادیده ببند</p></div>
<div class="m2"><p>هر چه ز آن جا به تو گویند از این جا بشنو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نگویند ندانی و چو گفتند خموش</p></div>
<div class="m2"><p>حرف سربسته به الفاظ معمّا بشنو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک زمانی برِ ما آی و علی رغمِ رقیب</p></div>
<div class="m2"><p>اگرت هست حدیثی دو سه پروا بشنو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان مفرّح که ز یاقوتِ لبت ساخته اند</p></div>
<div class="m2"><p>دل بیمارِ مرا هست مداوا بشنو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم دارم که مرا دل دهی و گوش کنی</p></div>
<div class="m2"><p>زاریِ زار مرا بهرِ خدا را بشنو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه از دستِ من ای دوست فراغت داری</p></div>
<div class="m2"><p>نکته ای هست ازین عاشقِ تنها بشنو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناله ی مرغ که شب گیر به گوش تو رسد</p></div>
<div class="m2"><p>همه پیغام نزاری ست نگارا بشنو</p></div></div>