---
title: >-
    شمارهٔ ۹۳۶
---
# شمارهٔ ۹۳۶

<div class="b" id="bn1"><div class="m1"><p>مهر من ای ماه روی مهربان</p></div>
<div class="m2"><p>ای مَلَک بر بام حسنت سایه بان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر گناهی کرده ام بگشای لب</p></div>
<div class="m2"><p>تا چرا بربسته ای با من زبان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیش ازین ابرو ترش بر من مدار</p></div>
<div class="m2"><p>هم چرا این تلخ از آن شیرین دهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رحم کن بر مردم چشمم ببخش</p></div>
<div class="m2"><p>کز تو در خون است روزان و شبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بود مشتاق بی روی حبیب</p></div>
<div class="m2"><p>شوره ی گرم و برو ماهی تپان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من به جان معراج کردم گر رقیب</p></div>
<div class="m2"><p>برگرفت از بام وصلت نردبان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسن جانان می رباید دل ز ما</p></div>
<div class="m2"><p>حسن و دل آن کشتی و این بادبان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیب و شفتالو بسی چیدم ز باغ</p></div>
<div class="m2"><p>بی خبر در خواب غفلت باغبان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیمه بگشای از نزاری رخ مپوش</p></div>
<div class="m2"><p>بس بود زلف سیاهت سایه بان</p></div></div>