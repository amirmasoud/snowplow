---
title: >-
    شمارهٔ ۱۱۲۱
---
# شمارهٔ ۱۱۲۱

<div class="b" id="bn1"><div class="m1"><p>گر نداری خبر از چشمه ی حیوان به من آی</p></div>
<div class="m2"><p>تا به سرچشمه ی خضرت برم انگشت نمای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرت طاقت تشنیعِ جَهول است بیا</p></div>
<div class="m2"><p>در خرابات و درِ می‌کده بر خود بگشای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی غلط رفت که از خویش برون آمدن است</p></div>
<div class="m2"><p>وان جهانی‌ست که آن‌جا نه جهان است و نه جای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وان چه جای است و مقاماتِ برانداختگان</p></div>
<div class="m2"><p>که در آن‌جا نبود نه دل و دین نه سرو پای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور سرِ بی سر و سامانت به برگ است و نوا</p></div>
<div class="m2"><p>هرچه از خویش برون آمده باشی به من آی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناشری پوش نه کم‌خا و نخ و اطلس و خز</p></div>
<div class="m2"><p>نیستی کرمِ قَزین بیش به خود برمتنای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفته تا سایه ی پای علم عشق خوش است</p></div>
<div class="m2"><p>بیش هم پهلوی دیوارِ پی آزرده مپای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رای تو آفتِ دین تو و رنج دل تست</p></div>
<div class="m2"><p>جنبشی کن ز خودی برشکن ای صاحب رای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل مشاطه ی رای آمد و رای احول چشم</p></div>
<div class="m2"><p>چشم احول نپذیرد چو رخ خوب آرای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طمع خیر نزاری ببر از کون و مکان</p></div>
<div class="m2"><p>چه تمتع ز جهانی که بود حادثه‌زای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حب ذریه ی اولادِ حبیب‌الله بس</p></div>
<div class="m2"><p>هم برین باش و برین هیچ دگر برمفزای</p></div></div>