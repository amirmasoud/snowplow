---
title: >-
    شمارهٔ ۱۲۸۰
---
# شمارهٔ ۱۲۸۰

<div class="b" id="bn1"><div class="m1"><p>ای در بیابان غمت سرگشته هر جایی دلی</p></div>
<div class="m2"><p>وا مانده در ره صد هزار افتاده در هر منزلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چوب ثعبان کی شود شیطان سلیمان کی شود</p></div>
<div class="m2"><p>این ره به پایان کی شود الا به پای کاملی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انسان به رتبت از سمک چون رفت بر بام فلک</p></div>
<div class="m2"><p>ترتیب شیطان و ملک ز اول چه بود آب و گلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیطان که اصل شر بود کی با ملک هم بر بود</p></div>
<div class="m2"><p>حاشا کجا هم سر بود هرگز محق با مبطلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کس به کف آرد صدف لیکن کمرآید در به صف</p></div>
<div class="m2"><p>تا خود که باید این شرف آری که باید مقبلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسیار مخلوق خلق برد از زبر دستان سبق</p></div>
<div class="m2"><p>بی حاصل توفیق حق کوشش ندارد حاصلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود دیده مرد دین نشد بی دیده روشن بین نشد</p></div>
<div class="m2"><p>هر بیدقی فرزین نشد کسری نشد هر عادلی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای یار اگر اهل دلی دست از دو عالم بگسلی</p></div>
<div class="m2"><p>فرمان بری گر عاقلی بر سازی از خود عاقلی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خود رو و با خود نشین خود را مدان خود را مبین</p></div>
<div class="m2"><p>در عشق روشن تر ز این هرگز نباشد مشکلی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آیین نام و ننگ را بگذار و در ده جام را</p></div>
<div class="m2"><p>کاین بحر بی انجام را پیدا نیامد ساحلی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ره نیست هر دلتنگ را در عشق جز یک رنگ را</p></div>
<div class="m2"><p>کو ترک نام و ننگ را همچون نزاری مقبلی</p></div></div>