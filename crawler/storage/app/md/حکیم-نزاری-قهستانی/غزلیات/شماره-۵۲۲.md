---
title: >-
    شمارهٔ ۵۲۲
---
# شمارهٔ ۵۲۲

<div class="b" id="bn1"><div class="m1"><p>از این حیات چه حاصل که در فراق سرآمد</p></div>
<div class="m2"><p>بیا که جان نزاری ز اشتیاق برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر چه داری و رایِ کجا ، که از سر رحمت</p></div>
<div class="m2"><p>نیامدی به سرم باز و وعده ها به سر آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیازمندی جانم به التقای جمالت</p></div>
<div class="m2"><p>ز هرچه شرح توان کرد و وصف بیشتر آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه ابتهال و تضرع به حق نمودم و کردم</p></div>
<div class="m2"><p>هر اجتهاد که در وسع و طاقت بشر آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه سود جهد چو دولت مساعدت ننماید</p></div>
<div class="m2"><p>به هرچه فال زدم قرعه شیوه‌ء دگر آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز عمر و عیش ندارم نه لذتی و نه ذوقی</p></div>
<div class="m2"><p>چنین بود چو نحوست به روزگار درآمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظر به وجهه نکردم به روی کار چه گویم</p></div>
<div class="m2"><p>که هرچه با سرم آمد ز آفت نظر آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به هرکجا که نشستم برایستاد خیالت</p></div>
<div class="m2"><p>ز هر طرف که برفتم غم تو بر اثر آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر التفات نمایی همین بس است که گفتم</p></div>
<div class="m2"><p>بیا که جان نزاری ز اشتیاق برآمد</p></div></div>