---
title: >-
    شمارهٔ ۴۹۲
---
# شمارهٔ ۴۹۲

<div class="b" id="bn1"><div class="m1"><p>تماشایِ او کردن آسان نباشد</p></div>
<div class="m2"><p>مرا دیدۀ دیدنِ آن نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو خورشید طالع شود مرغِ شب را</p></div>
<div class="m2"><p>نظر قابلِ عکسِ لمعان نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من آن جا خود از حیرت افتاده باشم</p></div>
<div class="m2"><p>چو شخصی که در جسمِ او جان نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز چشمِ گهرپاشِ من هر سرشکی</p></div>
<div class="m2"><p>کم از دانۀ دُرِ عمّان نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا با پری کرده ای آشنایی</p></div>
<div class="m2"><p>شکیب از پری کردن آسان نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پشیمانم از کرده و مردِ عاقل</p></div>
<div class="m2"><p>ز بد کردنی چون پشیمان نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دعوی غلوّ می توان کرد امّا</p></div>
<div class="m2"><p>ز دعوی چه حاصل چو برهان نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان گنج کی در چنین کُنج افتد</p></div>
<div class="m2"><p>گدا پیشه را جایِ سلطان نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلا از تو تا با تو باشد پشیزی</p></div>
<div class="m2"><p>رهت در مقاماتِ مردان نباشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مباش ایمن از نفس معذور باشی</p></div>
<div class="m2"><p>که آخر ز دشمن هراسان نباشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز صورت برون شو که مجموعِ معنی</p></div>
<div class="m2"><p>دگر باره هرگز پریشان نباشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>معاذ الله ار علّتِ جهل داری</p></div>
<div class="m2"><p>که این درد را هیچ درمان نباشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نزاری فدایِ رهِ دوست کردن</p></div>
<div class="m2"><p>کم از نیم جانی فراوان نباشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ندانی [که] با دوستان دوستان را</p></div>
<div class="m2"><p>به یک جان سخن بل به صد جان نباشد</p></div></div>