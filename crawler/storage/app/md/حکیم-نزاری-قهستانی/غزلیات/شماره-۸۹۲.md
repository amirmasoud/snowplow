---
title: >-
    شمارهٔ ۸۹۲
---
# شمارهٔ ۸۹۲

<div class="b" id="bn1"><div class="m1"><p>اگر چه هیچ نی ام هر چه هستم آن توام</p></div>
<div class="m2"><p>مرا مران که سگ بر آستان توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به زلتی که رود از نظر میندازم</p></div>
<div class="m2"><p>که برگرفته ی الطاف بی کران توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر اعتماد قبول توام کز اول عهد</p></div>
<div class="m2"><p>عنایت توبه من گفت ضمان توام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نماند هیچ ز من و ز تغلب سودا</p></div>
<div class="m2"><p>هنوز بر سر بازار امتحان توام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توانم از سر جان در هوای تو برخاست</p></div>
<div class="m2"><p>غلط شدم نتوانم که ناتوان توام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه می کنم ز زمین و زمان و کون و مکان</p></div>
<div class="m2"><p>همین مراد مرا بس که در زمان توام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به عون مرحمتم زین صراط برگذران</p></div>
<div class="m2"><p>که بر سر آمده از پای نردبان توام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به آمدن چو خوشم داشتی به وقت رحیل</p></div>
<div class="m2"><p>خوشم روان کن ازین جا که میهمان توام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گهی نزاری زارم گهی به زاری زار</p></div>
<div class="m2"><p>به هر صفت که برون آوری همان توام</p></div></div>