---
title: >-
    شمارهٔ ۱۱۲۸
---
# شمارهٔ ۱۱۲۸

<div class="b" id="bn1"><div class="m1"><p>اگر عنانِ عنایت به سوی ما تابی</p></div>
<div class="m2"><p>مگر بود که مرا زنده باز دریابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عجب اگر اجلم مهلتِ وصال دهد</p></div>
<div class="m2"><p>مگر که دیر نیایی و زود بشتابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا کز آتشِ فرقت چو ژاله می‌بارد</p></div>
<div class="m2"><p>بر آبگینه ی رویم سرشک عنابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غمت به تیغ بلا پوست باز کرد ز من</p></div>
<div class="m2"><p>ندانم از که درآموخت رسم قصابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز روی هم‌چو زرم اشک سیم می‌سازد</p></div>
<div class="m2"><p>زهی فلان که چنین شهره شد به قلّابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بس که موج زد و غوطه داد مردمِ چشم</p></div>
<div class="m2"><p>گرفت مردمَکش خویِ مردمِ آبی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا که روی چو مهتابه ی نزاری شد</p></div>
<div class="m2"><p>ز آفتابِ رخت در حجاب مهتابی</p></div></div>