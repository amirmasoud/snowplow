---
title: >-
    شمارهٔ ۱۰۴۷
---
# شمارهٔ ۱۰۴۷

<div class="b" id="bn1"><div class="m1"><p>ماییم و نیم جان و جهانی و نیم‌جو</p></div>
<div class="m2"><p>جان از برایِ می که ستاند زما گرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای یار اگر نداری طاقت گریز کن</p></div>
<div class="m2"><p>چون از مصافِ عشق برآمد غریو و غو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میدانِ عشق و معرکۀ عشق و زخم‌ِ عشق</p></div>
<div class="m2"><p>گر هیچت اختیار بود در میان مرو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر برگِ جای دوست نداری و راه‌ِ دوست</p></div>
<div class="m2"><p>در تنگ‌نایِ عشق سرِ خویش گیر و دو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما با حواریانِ سماوات در گویم</p></div>
<div class="m2"><p>اینک نهاده‌ایم به دعوی قدم به گو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طوبا به چشمِ وهم مصوّر کند خطیب</p></div>
<div class="m2"><p>بی‌چاره سیاه می‌طلبد از نهالِ نو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر سیر شد ز مؤعظۀ عاقلان دلت</p></div>
<div class="m2"><p>تذکیرِ عاشقانه بیا و ز من شنو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>إلّا به پای‌مردیِ جبریلِ پای‌مرد</p></div>
<div class="m2"><p>بر طاقِ آسمان نتوانند بست خو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در کیشِ عاشقان برافکنده عقل و حکم</p></div>
<div class="m2"><p>در پیشِ آفتاب جهان‌تاب تیغ و ضو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در کشت‌زارِ عالمِ دنیا نزاریا</p></div>
<div class="m2"><p>إلّا به داسِ همّتِ مردان مکن درو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواهی بهشتِ عدن بگویم ترا عیان</p></div>
<div class="m2"><p>از حلقۀ ولایتِ عدنان برون مشو</p></div></div>