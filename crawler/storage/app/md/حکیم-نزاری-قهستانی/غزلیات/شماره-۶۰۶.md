---
title: >-
    شمارهٔ ۶۰۶
---
# شمارهٔ ۶۰۶

<div class="b" id="bn1"><div class="m1"><p>بهر لِلّه امشب ای بادِ سحر</p></div>
<div class="m2"><p>بر سوادِ شهرِ قاین کن گذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشنایی بخشِ جانم را ببین</p></div>
<div class="m2"><p>وز منش با خود زمین بوسی ببر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گو ز رویِ تربیت ما را بپرس</p></div>
<div class="m2"><p>گو به چشمِ مرحمت ما را نگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز اگر تشریف خواهی داد هان</p></div>
<div class="m2"><p>باز اگر دریافت خواهی بیش‌تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آرزومندم به غایت یک قدم</p></div>
<div class="m2"><p>سخت مشتاقم به رحمت یک نظر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو می‌آیی برم منَت به جان</p></div>
<div class="m2"><p>ور مرا فرمان دهی آیم به سر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خودم هرگز نخواهد بود خوش</p></div>
<div class="m2"><p>وز توام هرگز نخواهد شد به سر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان نگارم کس نمی‌آرد پیام</p></div>
<div class="m2"><p>زان دیارم کس نمی‌گوید خبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم به الطافِ تو ای بادِ صبا</p></div>
<div class="m2"><p>رونقی گیرد سر و کارم مگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شرمسارم از تو امّا چون کنم</p></div>
<div class="m2"><p>چون ندارم محرمِ رازی دگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیم‌جان دارم فدایِ راهِ تو</p></div>
<div class="m2"><p>بیش از این چیزی ندارم ماحضر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرچه کارِ عاشقان زاری بود</p></div>
<div class="m2"><p>زاریِ مسکین نزاری زارتر</p></div></div>