---
title: >-
    شمارهٔ ۱۴۵
---
# شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>جهان در سایه خورشید عشق است</p></div>
<div class="m2"><p>نهان در سایه خورشید عشق است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یقین در ذرّه ذرّه آفتاب است</p></div>
<div class="m2"><p>گمان در سایه خورشید عشق است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو جنّت را طلب گاری نشان خواه</p></div>
<div class="m2"><p>جنان در سایه خورشید عشق است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روان کردیم دانش را که دایم</p></div>
<div class="m2"><p>روان در سایه خورشید عشق است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگویید آب حیوان درسیاهی ست</p></div>
<div class="m2"><p>از آن در سایه خورشید عشق است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سکندر چشمه می جست و ندانست</p></div>
<div class="m2"><p>که آن در سایه خورشید عشق است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن روشن ز نور آفتاب است</p></div>
<div class="m2"><p>زبان در سایه خورشید عشق است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رکاب عقل اگرچه پای مال است</p></div>
<div class="m2"><p>عنان در سایه خورشید عشق است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به نورالعین حق بنگر که الحق</p></div>
<div class="m2"><p>عیان در سایه خورشید عشق است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گریز از آفتابِ ناتوانی</p></div>
<div class="m2"><p>توان در سایه خورشید عشق است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نزاری نیّرین طالعت را</p></div>
<div class="m2"><p>قِران در سایه خورشید عشق است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر نامت چو شمس آفاق بگرفت</p></div>
<div class="m2"><p>نشان در سایه خورشید عشق است</p></div></div>