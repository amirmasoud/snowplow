---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>وقف کردم هستی خود بر شراب</p></div>
<div class="m2"><p>نیستی از من بمان گو در حجاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر لب کوثر نشستن روز بعث</p></div>
<div class="m2"><p>ای مسلمانان از این خوش تر مآب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اهل فطرت مست از آن جا آمدند</p></div>
<div class="m2"><p>هم چنان مستند تا یوم الحساب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من هم از آن جام مستی می کنم</p></div>
<div class="m2"><p>تا نپندارید کز خمرم خراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب و خاک عشق بر هم کرده اند</p></div>
<div class="m2"><p>پس سرشت من از آن خاک است و آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش سودای عشقم آبم ببرد</p></div>
<div class="m2"><p>از چه از بس تاب سوز و سوز تاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم ما و طلعت دیدار دوست</p></div>
<div class="m2"><p>چشم خفّاش است و نور آفتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آفتاب آن جا اگر چه ذره ایست</p></div>
<div class="m2"><p>از ره تمثیل کردم انتساب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من چو حربا عاشقم بر عکس نور</p></div>
<div class="m2"><p>نی چو خفاشم زخور در احتجاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا شوند احباب در محبوب محو</p></div>
<div class="m2"><p>از وجود خویش کردند اجتناب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا که خواهد طاقت انوار داشت</p></div>
<div class="m2"><p>گر جمال از پیش بردارد نقاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چشم امید نزاری روشن است</p></div>
<div class="m2"><p>از طلوع نور نجل بوتراب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پرتوی بر جان من زان نور تافت</p></div>
<div class="m2"><p>ذره وار افتاده ام در اضطراب</p></div></div>