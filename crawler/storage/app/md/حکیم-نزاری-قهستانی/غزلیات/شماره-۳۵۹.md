---
title: >-
    شمارهٔ ۳۵۹
---
# شمارهٔ ۳۵۹

<div class="b" id="bn1"><div class="m1"><p>به جفتِ چشمِ سیاه و به ابروی طاقت</p></div>
<div class="m2"><p>که در فراق تو ام نیست بیش از این طاقت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هلاک می شوم آخر بیا و دستم گیر</p></div>
<div class="m2"><p>که پاد زهر دل است آن لب چو تریاقت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر هم ایلچیِ آه صبح گاهیِ من</p></div>
<div class="m2"><p>بر تو آید و باز آورد ز ییلاقت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به زندگانی خویش از تو راحتی نبود</p></div>
<div class="m2"><p>مرا اگر بنمیرد رقیب ایقاقت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پری وشا تو چه ترکی که کم فرشته بود</p></div>
<div class="m2"><p>به شکل و شیوه و خوی و سرشت و اخلاقت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر در آینه بینی وگرنه ممکن نیست</p></div>
<div class="m2"><p>به نیکویی که نظیری بود در آفاقت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به باغ سرو در آرد به پای بوس تو سر</p></div>
<div class="m2"><p>اگر ز موزه به عمدا برون کنی ساقت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کلاه بر نه و زلفت به دوش باز انداز</p></div>
<div class="m2"><p>نهان مکن که دریغ است زیر قلپاقت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوشا که حلقه بجنبانم و تو گویی کیست</p></div>
<div class="m2"><p>نزاری آه بر‌آرد که من زِ عشّاقت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به زینهار که با حسنِ طلعتی که توراست</p></div>
<div class="m2"><p>به چشمِ پاک نظر کی کنند عشّاقت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دفع چشم بد از روی بر مگیر نقاب</p></div>
<div class="m2"><p>که بس عجایب و خوب آفرید خلّاقت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر چه سرمه شود استخوانم اندر خاک</p></div>
<div class="m2"><p>بود روان نزاری هنو‌ز مشتاقت</p></div></div>