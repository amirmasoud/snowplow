---
title: >-
    شمارهٔ ۸۹۴
---
# شمارهٔ ۸۹۴

<div class="b" id="bn1"><div class="m1"><p>برون نمی رود از سر هوای روی توام</p></div>
<div class="m2"><p>به روز و شب به دل و جان مقیم کوی توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همین که روی نمودی به من فغان برخاست</p></div>
<div class="m2"><p>ز بند بندم و در بند بند موی توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه یاد می کنی از من نه باز می پرسی</p></div>
<div class="m2"><p>بیا که جان به لب آمد در آرزوی توام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه غرق شدم در محیط عشق هنوز</p></div>
<div class="m2"><p>بدان که نقطه ی جان و تن است سوی توام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و گر چه بهتر از اینم چه کار می آید</p></div>
<div class="m2"><p>ولیک عمر به سر شد به جست و جوی توام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب نبود که از زلف صولجان کردی</p></div>
<div class="m2"><p>عجب ترست که سرگشته گرد کوی توام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رقیب گفت نزاری مکن فضولی بیش</p></div>
<div class="m2"><p>تو دوستار فلانی و من عدوی توام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جواب گفتم آری تو باد می پیمای</p></div>
<div class="m2"><p>من ار تو دشمنی ار دوست خاک پای توام</p></div></div>