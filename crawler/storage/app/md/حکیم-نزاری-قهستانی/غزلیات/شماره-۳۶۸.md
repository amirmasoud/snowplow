---
title: >-
    شمارهٔ ۳۶۸
---
# شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>بکشم گر همه کوه است وز آهن ستمت</p></div>
<div class="m2"><p>بر ندارم به جفا چشم امید از کرمت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بمیرم زغمت روی نتابم هرگز</p></div>
<div class="m2"><p>وین تفاخر نه بس آخر که بمیرم ز غمت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در وفای تو نشینم که توانم بر خواست</p></div>
<div class="m2"><p>از سر سر که سرم باد فدای قدمت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیب گویند که در پای تو افتم آخر</p></div>
<div class="m2"><p>که به بت خانه روی سجده نماید صنمت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا سلامی به تو آرد به رقیبت فرمای</p></div>
<div class="m2"><p>تا صبا را نبرد ناز بسی از کرمت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قادری گر بزنی گر بنوازی ، اما</p></div>
<div class="m2"><p>آن کن ای دوست که از کرده نباشد ندمت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یاد داری که چه عهد است میان من و تو</p></div>
<div class="m2"><p>تا بدانی که فراموش نکردم قسمت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در کشیدم ز همه خلق جهان دامن دل</p></div>
<div class="m2"><p>بو که چون پیرهنت تنگ به بر در کشمت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهد کن در طلب وصل نزاری خوش باش</p></div>
<div class="m2"><p>عاقبت دست دهد دولت این نیز همت</p></div></div>