---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>در خرابات گدایان ز سر ناز میا</p></div>
<div class="m2"><p>نشنیدی و ندیدی برو و باز میا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زینهار از تو که با ساز مخالف نروی</p></div>
<div class="m2"><p>راست می ساز چو بازآیی ناساز میا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راه ما تا نکنی قطع مقامات مپوی</p></div>
<div class="m2"><p>کوی ما تا نشوی خانه برانداز میا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جای در باختگان است و بد انداختگان</p></div>
<div class="m2"><p>در مقامرکده بی حرمت و اعزاز میا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنج ما گنج نهان است در او بنهفته</p></div>
<div class="m2"><p>سوی ما تا نشوی معتمد راز میا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا سرافرازی و گردن کشی از ما دوری</p></div>
<div class="m2"><p>پس بنه گردن تسلیم و سرافراز میا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی در کعبة وحدت نتوان با خود رفت</p></div>
<div class="m2"><p>در چنین قافله با کثرت انباز میا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نخوانند به آن جا نتوانی رفتن</p></div>
<div class="m2"><p>بر پی بانگ دعا باز خوش آواز میا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فاش مرغی است نزاری و چو بلبل بر گل</p></div>
<div class="m2"><p>گو دگر بر سر این شاخ به پرواز میا</p></div></div>