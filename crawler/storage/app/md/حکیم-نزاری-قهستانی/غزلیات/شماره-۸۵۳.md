---
title: >-
    شمارهٔ ۸۵۳
---
# شمارهٔ ۸۵۳

<div class="b" id="bn1"><div class="m1"><p>توبه ای کردم و گفتم که دگر می نخورم</p></div>
<div class="m2"><p>تا منم باز دگر نام من و می نبرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ستم توبه ی مستحکم من بشکستند</p></div>
<div class="m2"><p>چه قضا بود که ناگاه درآمد به سرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طمعم بود که از غیب حضوری بخشند</p></div>
<div class="m2"><p>چون بدیدم من مسکین نه سزای حضرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محشر این است که من هر دم و هر لحظه درو</p></div>
<div class="m2"><p>بل که خود هر نفسی در عرصات حشرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من به خشنودی دل از سر جان آزادم</p></div>
<div class="m2"><p>بر مقامی چه نهم دل که به جان در خطرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خبر آن نیست که از کس خبری گویم باز</p></div>
<div class="m2"><p>خبر این است که از خویش نباشد خبرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر از اسرار نهان بی خبران بی خبرند</p></div>
<div class="m2"><p>ای مسلمانان من بی خبر بی خبرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من به امرالله در خلق اضافت بینم</p></div>
<div class="m2"><p>من به عین الله در عالم اشیا نگرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هوس دنیی و عقبی ز سرم بیرون شد</p></div>
<div class="m2"><p>تا به کی عشوه که خاطر بگرفت از سمرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سالکم مرحله بر مرحله می پردازم</p></div>
<div class="m2"><p>مسرعم مرتبه از مرتبه می برگذرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به جوان مردی مردان جهان باز که گر</p></div>
<div class="m2"><p>همه عالم به جوی نیست جوی غم نخورم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه بی من بود و هیچ نباشد بی او</p></div>
<div class="m2"><p>راستی آنچه صراط است که من می سپرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا کسی را طمع از من نبود ستر و صلاح</p></div>
<div class="m2"><p>هر زمان پرده ی پرهیز نزاری بدرم</p></div></div>