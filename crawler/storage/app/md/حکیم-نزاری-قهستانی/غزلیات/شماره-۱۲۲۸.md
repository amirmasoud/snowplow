---
title: >-
    شمارهٔ ۱۲۲۸
---
# شمارهٔ ۱۲۲۸

<div class="b" id="bn1"><div class="m1"><p>سواره هر چه به نظّاره گاه برگذری</p></div>
<div class="m2"><p>به یک کرشمه چه باشد که باز پس نگری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رقیب خود ز تو خالی نمی شود یک دم</p></div>
<div class="m2"><p>چنین یکی شده باهم که دید دیو و پری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دستِ باد غباری فرست از آن خاکی</p></div>
<div class="m2"><p>که بی دریغ به نعل سمند می سپری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عتاب و ناز به میزانِ عدل بر ما سنج</p></div>
<div class="m2"><p>سرش به ظلم گراید اگر ز حد ببری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مقابل تو سزد ز امّهات و از آبا</p></div>
<div class="m2"><p>که کس دگر نکند دعویِ پدر پسری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بی وفایی و بعد عهدی و جفا کاری</p></div>
<div class="m2"><p>به نام و ننگ ز دونان سزد که برگذری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قبول کن که نصیحت به فال میمون است</p></div>
<div class="m2"><p>تو خود مبارک رویی و نیک بخت اثری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می است مونس من در غمِ فراق تو می</p></div>
<div class="m2"><p>ولی نمی کند البتّه ترک پرده دری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نزاریا به ملامت گر التفات مکن</p></div>
<div class="m2"><p>خوش است مستی و دیوانگی و بی خبری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فسرده را اگرش بر خیال می گذرد</p></div>
<div class="m2"><p>که ترک شیفته کاری کنی چو می نخوری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ترا شراب زخم خانه ی الست دهند</p></div>
<div class="m2"><p>از آن که مستی شوریده کار و خیره سری</p></div></div>