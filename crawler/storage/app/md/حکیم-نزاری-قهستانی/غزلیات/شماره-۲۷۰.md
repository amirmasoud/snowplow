---
title: >-
    شمارهٔ ۲۷۰
---
# شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>آخر بدین صفت که منم مبتلایِ دوست</p></div>
<div class="m2"><p>ممکن بود ز من که نجویم رضای دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با عاشقان مجاز بود عشقِ عاشقی</p></div>
<div class="m2"><p>کو ترکِ هر دو کون نگیرد برایِ دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کردم به عشق زیر و زبر خان و مان دل</p></div>
<div class="m2"><p>تا هیچ کس دگر ننشیند به جایِ دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تسلیمِ راهِ دوست شوم چون به نزدِ من</p></div>
<div class="m2"><p>چیزی نیافرید خدا ماورایِ دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جاوید زنده مانم و باقی شوم چو خضر</p></div>
<div class="m2"><p>گر سجده ایی به من رسد از خاکِ پایِ دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دم قیامتی ز ملامت بدیده ام</p></div>
<div class="m2"><p>نادیده یک نفس به ارادت لقایِ دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا جان بود مرا بکشم از میان جان</p></div>
<div class="m2"><p>جنگ و ستیزِ دشمن و جور و جفایِ دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نی نی به عینِ صدق نزاری خطا مبین</p></div>
<div class="m2"><p>خالی شمر ز جور و جفا ماجرایِ دوست</p></div></div>