---
title: >-
    شمارهٔ ۱۳۲۸
---
# شمارهٔ ۱۳۲۸

<div class="b" id="bn1"><div class="m1"><p>مست و شوریده چنانم که ز بی خویشتنی</p></div>
<div class="m2"><p>من توام هیچ نمی‌دانم اگر خود تو منی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرطِ اخلاص چنان است ز مبدایِ وجود</p></div>
<div class="m2"><p>که نه من بر تو گزینم نه تو بر من شکنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وقت وقتی چه شود گر به سرِ ما گذری</p></div>
<div class="m2"><p>سایه ی سرو روان بر سرِ خاکی فکنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نسیمی ز تو خشنودم اگر بفرستی</p></div>
<div class="m2"><p>دلِ غمگینِ چو من سوخته‌ ای شاد کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیفه ی نافه ی پر چینِ عرق‌چین تو شد</p></div>
<div class="m2"><p>رشکِ ترکان ختایی و بتانِ ختنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب و دندانِ ترا من به چه تشبیه کنم</p></div>
<div class="m2"><p>حقه ی لعل درو رشتهء دُرِّ عَدَنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از نکو رویی و شیرینی و خوبی و کشی</p></div>
<div class="m2"><p>شهره ی شهری و انگشت کشِ مرد و زنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در جفا کاری و عاشق کشی و بی‌دادی</p></div>
<div class="m2"><p>آفتِ جانی و دردِ دلی و رنجِ تنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این همه هست که می‌گویم و زین هیچ نیی</p></div>
<div class="m2"><p>هر چه هستی چه کنم جانی و جانانِ منی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا نزاری لبِ شیرینِ تو بوسید افکند</p></div>
<div class="m2"><p>شور در عرصه ی آفاق به شیرین سخنی</p></div></div>