---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>مگر صبا برساند سلام یارو را</p></div>
<div class="m2"><p>وگرنه با که بگویم حکایت او را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو اعتماد نمانده ست جهل باشد اگر</p></div>
<div class="m2"><p>محل راز کنم دوستان بد گو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه یار با من و نه دل چگونه بی دل و یار</p></div>
<div class="m2"><p>توان برید به تکلیف راه اردو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا شبی و در آغوش و در کنارم گیر</p></div>
<div class="m2"><p>که بیش طاقت ازین نیست بی تو سعدو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی که جان و دل و هوش ما با اوست</p></div>
<div class="m2"><p>چگونه باز توانیم کرد ازو خو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جان تو که نزاری دگر به دیده و دل</p></div>
<div class="m2"><p>نه زشت را متقبل شود نه نیکو را</p></div></div>