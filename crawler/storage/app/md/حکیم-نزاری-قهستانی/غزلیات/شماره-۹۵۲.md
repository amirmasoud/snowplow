---
title: >-
    شمارهٔ ۹۵۲
---
# شمارهٔ ۹۵۲

<div class="b" id="bn1"><div class="m1"><p>وه که جهان در گرفت سوزِ دمِ عاشقان</p></div>
<div class="m2"><p>کون و مکان درکشید موجِ غمِ عاشقان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غلغلِ اِستبشرُوا در ملکوت اوفتاد</p></div>
<div class="m2"><p>باز نهان شد عیان آن صنمِ عاشقان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق به دستِ ازل تا به دوامِ ابد</p></div>
<div class="m2"><p>بر فلکِ مستقیم زد علمِ عاشقان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون متحرّک شود موکبِ رایاتِ عشق</p></div>
<div class="m2"><p>روحِ امین سر نهد در قدمِ عاشقان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محرم اگر نیستی پای درین ره منه</p></div>
<div class="m2"><p>از سرِ عَمیا مرو در حرمِ عشاقان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برگذر از ما و من بیش و کم خود مبین</p></div>
<div class="m2"><p>زان که کم و بیش نیست بیش و کمِ عاشقان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رنگ دو رنگی مکن کز ازل استادِ کار</p></div>
<div class="m2"><p>سکّه ی وحدت نهاد بر درمِ عاشقان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آینه ی آهنین صورتِ کج‌بین بود</p></div>
<div class="m2"><p>سینه ی یک دیگرست جامِ جمِ عاشقان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلکِ نزاری کند چهره ی معنی تراز</p></div>
<div class="m2"><p>خطِّ خطا کی روی بر قلمِ عاشقان</p></div></div>