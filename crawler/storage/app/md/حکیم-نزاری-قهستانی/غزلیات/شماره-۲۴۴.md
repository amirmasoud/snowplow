---
title: >-
    شمارهٔ ۲۴۴
---
# شمارهٔ ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>هم چو من هرگز خراباتِ الست</p></div>
<div class="m2"><p>مستِ لایعقل به دنیا آمده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا سرِ عقل از گریبان برکند</p></div>
<div class="m2"><p>دامنِ آشفتگان ندهد ز دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهم از دنیا برون شد هم چنان</p></div>
<div class="m2"><p>مست تا بازم بر انگیزند مست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انتظارِ صورِ محشر داشتن</p></div>
<div class="m2"><p>عمر ضایع کردن است ای بت پرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسیه کارِ مردِ صاحب وقت نیست</p></div>
<div class="m2"><p>هر که از خود برشکست از بت پرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طالعم بر بی دلی و بی خودی ست</p></div>
<div class="m2"><p>چون کنم نتوانم از طالع بجست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ایازی می دهندش در عوض</p></div>
<div class="m2"><p>هر که چون محمود بت در هم شکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بت که می گویند جز وهمِ تو نیست</p></div>
<div class="m2"><p>بت توئی چیزی دگر مشنو که هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا به موعودِ حواری و ظهور</p></div>
<div class="m2"><p>در نیارم بر می و معشوق بست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس نزاری و حریف و پایِ خم</p></div>
<div class="m2"><p>هم چنین بی کار نتواند نشست</p></div></div>