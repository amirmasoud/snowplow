---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>ای دل بدان که درد دل تو دوای تست</p></div>
<div class="m2"><p>بی درد دل مباش هم او مقتدای تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دنیا نه جای تست مکان تو دیگر است</p></div>
<div class="m2"><p>گرچه ز ابتدای مراتب سرای تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مقصود ز آفرینش کونین و عالمین</p></div>
<div class="m2"><p>گر بشنوی ز داعی مخلص برای تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین تنگنا بدر شو و حالی قرار گیر</p></div>
<div class="m2"><p>بر تخت گاه سدره که آرام جای تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنشین به چار بالش سرمد به کام دل</p></div>
<div class="m2"><p>گر زان که بر وساده ی حق متکای تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این جا هوای هاویه بیرون کنی ز سر</p></div>
<div class="m2"><p>آن جا هوای سدره نشینان هوای تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الا هوای نفس چه می دید جم ز جام</p></div>
<div class="m2"><p>اینک سفال ساغر گیتی نمای تست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساکن مباش می رو و ایمن مباش نیز</p></div>
<div class="m2"><p>بر ره هزار غول ز چون و چرای تست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا زین همه عذاب و خطر وارهی به طبع</p></div>
<div class="m2"><p>تسلیم کن به آن که به حق ماورای تست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آخر چرا متابعت دیو می کنی</p></div>
<div class="m2"><p>دیو ای فرشته زاده نگویی کهای تست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو پس رو اموری و او سخره ی غرور</p></div>
<div class="m2"><p>ابلیس ناسزا به چه حجت سزای تست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرگز به منتهای کمالات کی رسی</p></div>
<div class="m2"><p>تا هم ورای ناقص تو پیشوای تست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بنگر به دست کار نزاری که چون بساخت</p></div>
<div class="m2"><p>این داری مفید که شافی دوای تست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اخلاط حکمت است که ترکیب می کند</p></div>
<div class="m2"><p>حبی محققانه که محض شفای تست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با کافران غزا چه کنی آری از نخست</p></div>
<div class="m2"><p>دفع هوای نفس تو حد غزای تست</p></div></div>