---
title: >-
    شمارهٔ ۹۰۴
---
# شمارهٔ ۹۰۴

<div class="b" id="bn1"><div class="m1"><p>صبا بگوی به یارم که باز می‌آیم</p></div>
<div class="m2"><p>به جان رسیده سویِ دل نواز می‌آیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو حاجیان به سویِ کعبه ی زیارتِ دوست</p></div>
<div class="m2"><p>به دیده هر قدمی‌در نماز می‌آیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بر گرفته میانِ تو کی بود که چو خضر</p></div>
<div class="m2"><p>کنارِ چشمه ی حیوان فراز می‌آیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز در درآیی و صحنِ سرا بیارایی</p></div>
<div class="m2"><p>تو می‌خرامی‌و من پیش باز می‌آیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرشمه ی تو مرا می‌رباید از من و من</p></div>
<div class="m2"><p>به غمزه ی توبه صد اهتزاز می‌آیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم که در خمِ زلفِ تو مبتلاست گواست</p></div>
<div class="m2"><p>که من به جان ز شبانِ دراز می‌آیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همان نزاریِ محمودم ار قبول کنی</p></div>
<div class="m2"><p>که در مواکبِ حسنِ ایاز می‌آیم</p></div></div>