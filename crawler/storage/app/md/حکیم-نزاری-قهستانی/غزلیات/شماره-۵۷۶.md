---
title: >-
    شمارهٔ ۵۷۶
---
# شمارهٔ ۵۷۶

<div class="b" id="bn1"><div class="m1"><p>آهِ ندامت ز شیخ وشاب بر آید</p></div>
<div class="m2"><p>آری چندان که آفتاب بر آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق دمی در دمد به صورِ محبّت</p></div>
<div class="m2"><p>بو که سرِ خفتگان ز خواب برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جگرِ منتظر نوایرِ حسرت</p></div>
<div class="m2"><p>در جسدِ آلِ بوتراب بر آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ نمانده ست کز خزاینِ ظالم</p></div>
<div class="m2"><p>بانگ به تاراجِ انقلاب برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برقِ محبّت محیطِ عقل بسوزد</p></div>
<div class="m2"><p>آتشِ عشق از میانِ آب برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صاعقۀ عشق اگر شود متواتر</p></div>
<div class="m2"><p>دود ازین تودۀ خراب برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حاسد اگر قرعه یی زند به تفال</p></div>
<div class="m2"><p>در حقِ او آیۀ عذاب برآید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در حقِ ما هر که بد سگالد و گوید</p></div>
<div class="m2"><p>روزِ مکافات از ثواب برآید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آتشِ آهم اگر به دیر درافتد</p></div>
<div class="m2"><p>دود ز خُم خانۀ شراب برآید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما و خراباتِ عشق رغمِ عدو را</p></div>
<div class="m2"><p>جان ز تنش تا به تفت و تاب برآید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مجلسِ ما خوش تر از بهشت که هر دم</p></div>
<div class="m2"><p>حوروشی دیگر از نقاب برآید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیشِ رباب از فراقِ دعد بنالد</p></div>
<div class="m2"><p>زاریِ زیرش چو از رباب برآید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بخت دگر باره گر دری بگشاید</p></div>
<div class="m2"><p>کامِ نزاری ز فتحِ باب برآید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آری اگر فطرتش مناسبِ حال است</p></div>
<div class="m2"><p>دعوتِ مظلوم مستجاب برآید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دولت اگر خود مساعدت نکند هیچ</p></div>
<div class="m2"><p>فتنه ز تسکین به اضطراب برآید</p></div></div>