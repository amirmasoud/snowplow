---
title: >-
    شمارهٔ ۹۹۵
---
# شمارهٔ ۹۹۵

<div class="b" id="bn1"><div class="m1"><p>گر هیچ می‌توانی ای دل گزارشی کن</p></div>
<div class="m2"><p>با حامیِ عنایت ما را سپارشی کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنها نشین ندارد از عمر هیچ لذت</p></div>
<div class="m2"><p>در بار هر دو عالم ترتیب گردشی کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مملو شده‌ست طبعت از لقمه‌ی مخالف</p></div>
<div class="m2"><p>از ریزه‌ی محبت خود را جوارشی کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهی که از من و ما یک ره خلاص‌یابی</p></div>
<div class="m2"><p>از خویشتن برون آ جهدی و کوششی کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حامی کار ما شو یک‌باره یار ما شو</p></div>
<div class="m2"><p>میدان شده‌ست خالی برخیز چالشی کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیغ ظهور برکش آفاق کن مسلّم</p></div>
<div class="m2"><p>شاهانه لشکری کش مردانه جنبشی کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک رنگ شو نزاری در باز هر چه داری</p></div>
<div class="m2"><p>بر آستان مردان بنشین و پوزشی کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در کنجِ خویش ساکن بنشین و همچو مردان</p></div>
<div class="m2"><p>از خاک کعبه فرشی وز سنگ بالشی کن</p></div></div>