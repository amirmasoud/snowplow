---
title: >-
    شمارهٔ ۱۱۷۲
---
# شمارهٔ ۱۱۷۲

<div class="b" id="bn1"><div class="m1"><p>مرحبا مرحبا کجا بودی</p></div>
<div class="m2"><p>دیر شد تا جمال ننمودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک دمم در حضور ننشتی</p></div>
<div class="m2"><p>یک شبم در کنار نغنودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برشکستی و دیر پیوستی</p></div>
<div class="m2"><p>سیر گشتی ز ما بدین زودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درِ خلوت سرایِ ما اکنون</p></div>
<div class="m2"><p>قفل کردی و باز نگشودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خبری گوی صوفیانه بتا</p></div>
<div class="m2"><p>تا ز ابرام ما بیاسودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو خود از بدوِ فطرتِ اُولا</p></div>
<div class="m2"><p>در کنارِ خیالِ ما بودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخنی در تعارفِ ارواح</p></div>
<div class="m2"><p>گفته بودیم با تو نشنودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشرک ار خوانده‌‌ای نزاری را</p></div>
<div class="m2"><p>هم هنوزش به وجه نبسودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راست گفتی که پیشِ وجهِ خیال</p></div>
<div class="m2"><p>خونش از دیده‌ها بپالودی</p></div></div>