---
title: >-
    شمارهٔ ۴۱۳
---
# شمارهٔ ۴۱۳

<div class="b" id="bn1"><div class="m1"><p>به دست آورده ام یاری که رویی چون قمر دارد</p></div>
<div class="m2"><p>دهانی چون لبِ شیرین لبانی چون شکر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا شد عیسیِ مریم بیا گو معجزِ دم بین</p></div>
<div class="m2"><p>که در هر حرف پنداری نهان جانی دگر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظر تا بر وی افکندم ز سر تا پای در بندم</p></div>
<div class="m2"><p>سرِ او دارم از عالم ندانم اون چه سر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه داند هر هوس ناکی کمالِ حسنِ لیلی را</p></div>
<div class="m2"><p>کسی داند که چون مجنون دلی صاحب نظر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر صادق بود عاشق به تیرِ طعنۀ فاسق</p></div>
<div class="m2"><p>نه روی اندر گریز آرد نه پروایِ سپر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگو ای پیکِ مشتاقان بدان خورشید کاین مسکین</p></div>
<div class="m2"><p>همه شب دیدۀ بیدار و حیران بر سحر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لگد کوبِ فراقت گر مرا خاکی کند شاید</p></div>
<div class="m2"><p>مگر بازآورد بادی که بر کویت گذر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرا باید نزاری را نظر جز بر تو افکندن</p></div>
<div class="m2"><p>روا نبود جهان دیدن به چشمی کز تو بر دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی کش هم دمی باشد ازو چندی جدا ماند</p></div>
<div class="m2"><p>عجب می دارم ار هرگز دگر عزمِ سفر دارد</p></div></div>