---
title: >-
    شمارهٔ ۵۴۹
---
# شمارهٔ ۵۴۹

<div class="b" id="bn1"><div class="m1"><p>آن کدام‌اند و کیان‌اند و کجا می‌باشند</p></div>
<div class="m2"><p>کز خرد دور و برانگیخته با اوباش‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه آزرده و رنجور شود دل‌هاشان</p></div>
<div class="m2"><p>از جفای دگران سینۀ کس نخراشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برِ این کِشته گر ابلیس خورد گر آدم</p></div>
<div class="m2"><p>هم‌چنان مجتهدان دانۀ خود می‌پاشند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به دربانیِ دل نصب شوی قانع باش</p></div>
<div class="m2"><p>ماه و خورشید در این پرده‌سرا فرّاش‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست با مردمِ نادان سخنِ منکر حق</p></div>
<div class="m2"><p>که ندانی که گدایانِ خدا قلّاش‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خویشتن را ز خدا دان و منافق بشمار</p></div>
<div class="m2"><p>اغلب اربابِ حقایق به جهالت فاش‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واعظ آن است که اثبات کند نفیِ وجود</p></div>
<div class="m2"><p>گز ز من راست بپرسی دگران فحّاش‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صلح کرده‌ست و سپر بر سرِ آب افکنده‌است</p></div>
<div class="m2"><p>با نزاری همه زان در جدل و پرخاش‌اند</p></div></div>