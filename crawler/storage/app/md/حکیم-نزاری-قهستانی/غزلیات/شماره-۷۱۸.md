---
title: >-
    شمارهٔ ۷۱۸
---
# شمارهٔ ۷۱۸

<div class="b" id="bn1"><div class="m1"><p>درد عشق است ای ملامت گر خموش</p></div>
<div class="m2"><p>نیست این بحری که بنشیند ز جوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چاشنی کن گر نداری ذوقِ می</p></div>
<div class="m2"><p>طالب دردی درآ و دُرد نوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز و شب جانت به جان می پرورد</p></div>
<div class="m2"><p>بیش از این جرمی ندارد می فروش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که می گویی نصیحت کار بند</p></div>
<div class="m2"><p>هرگز او خود در نمی آید به گوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منزلی باشد که هوش آنجا بود</p></div>
<div class="m2"><p>گو کدام است این نشانم ده به هوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با که برگویم که زان حضرت به من</p></div>
<div class="m2"><p>هر زمان پیغام می آرد سروش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راست می گفتم وگرنه از کجاست</p></div>
<div class="m2"><p>در جهان افتاده چندینی خروش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم نمی گویم که مردم گفته اند</p></div>
<div class="m2"><p>سرّ سلطان تا توانی بازپوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل با من گفت کای کوته نظر</p></div>
<div class="m2"><p>بیش از این در کسب بدنامی مکوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق می گوید فضولی می کند</p></div>
<div class="m2"><p>گو نمی دانی زبان ما خموش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر نزاری را نمی دانی که چیست</p></div>
<div class="m2"><p>داغ او دارد ببین اینک دروش</p></div></div>