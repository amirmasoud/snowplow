---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>آدینه مست مرا دید محتسب خراب</p></div>
<div class="m2"><p>می آمدم برون ز خرابات مست بی نقاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آغاز کرد بی هده گفتن کی ای فلان</p></div>
<div class="m2"><p>از چون تویی خطاست چنین کار ناصواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آدینه چون به مجلس واعظ نیامدی</p></div>
<div class="m2"><p>نشنیدی از خطیب که چون می کند خطاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای فارغ از بهشت نمی بایدت نعیم</p></div>
<div class="m2"><p>وی غافل از جحیم نمی ترسی از عقاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که احتساب خراباتیان که کرد</p></div>
<div class="m2"><p>هرگز که دید کوی خرابات و احتساب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر راست بشنوی ز خراباتیان بسی</p></div>
<div class="m2"><p>راغب تری به شرب سر از راستی متاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر من چه اعتراض کنی دفع خویش کن</p></div>
<div class="m2"><p>کز خود برون نرفته به زهدت چه احتساب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اینک بهشت اگر به قضا داده ای رضا</p></div>
<div class="m2"><p>وینک جحیم اگر طمعی داری از شراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از مجلسی چه می کنم اینجا که می کنند</p></div>
<div class="m2"><p>هر دم روایتی درگر از آیت عذاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من در بهشتِ مجلسِ اصحاب می خورم</p></div>
<div class="m2"><p>بر بانگ چنگ و ضرب دف و نغمه ی رباب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن جا نه جای تست نه میخانه راه تو</p></div>
<div class="m2"><p>زهّاد را پدید بود مرجع ومآب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن ها که سر به دنیی و دین درنیاورند</p></div>
<div class="m2"><p>حلاج وار طوق ندانند از طناب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کردند اعتصام به حبل الله و زدند</p></div>
<div class="m2"><p>دست وفا به دامان اولاد بوتراب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بی بال مانده ای نتوانی پرید از آنک</p></div>
<div class="m2"><p>ناممکن است جلوه ی طاووس از غراب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مثل تو پاک تن نبد آن قوم پاکباز</p></div>
<div class="m2"><p>گنجشک را رسد که کند پنجه با عقاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ایشان منزّه اند و تو موقوف نام وننگ</p></div>
<div class="m2"><p>ای گاو لاغری مفگن خر در این خلاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>منگر به عاشقان به حقارت که می کنند</p></div>
<div class="m2"><p>شیران نر زه معرکه ی عشق اجتناب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>افسرده را چه غم که نزاری در آتش است</p></div>
<div class="m2"><p>ای دست گیر هان که ز سر درگذشت آب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یارب تو آگهی که محبّ ولایتم</p></div>
<div class="m2"><p>روز جزا به قدر محبان دهم ثواب</p></div></div>