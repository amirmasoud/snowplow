---
title: >-
    شمارهٔ ۹۶۵
---
# شمارهٔ ۹۶۵

<div class="b" id="bn1"><div class="m1"><p>نمیتوان دل یاری زخود بیازردن</p></div>
<div class="m2"><p>نه نیز هم دل خود را ز غیر آزردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میان این دو دلم نیست حاصلی دیگر</p></div>
<div class="m2"><p>مگر مناظره ای کردن و غمی خوردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر به چاره بر لب کشند جان مرا</p></div>
<div class="m2"><p>به هیچ وجه دگر نیست چاره یی کردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشنع متعضب مگر نمی داند</p></div>
<div class="m2"><p>که صبغت الله نتوان به حیله بستردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به لا نسلّم چیزی مسلّمت نشود</p></div>
<div class="m2"><p>چه سود آیت باطل به حجت آوردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا به عقل و گرعقل را به تو چون است</p></div>
<div class="m2"><p>کدام یک به دگر واجب است بسپردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه مرغ دانه ی دنیایم ای خطا بینان</p></div>
<div class="m2"><p>چه حاصل است شما را ز دام گستردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من از مشیمه ی فطرت وجود یافته ام</p></div>
<div class="m2"><p>به هرزه دایه ی عشقم نخواست پروردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه سود سنگ ملامت زدن نزاری را</p></div>
<div class="m2"><p>که مانده در گل عشقم ز پای تا گردن</p></div></div>