---
title: >-
    شمارهٔ ۴۶۴
---
# شمارهٔ ۴۶۴

<div class="b" id="bn1"><div class="m1"><p>گر مرا دل نواز بنوازد</p></div>
<div class="m2"><p>رایتِ دولتم برافرازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود دگر بار از آن همی ترسم</p></div>
<div class="m2"><p>که به دیدار ما نپردازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راستی را بر آفتابِ سپهر</p></div>
<div class="m2"><p>رسدش گر به حسن می نازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهرۀ مهر بر بساطِ نشاط</p></div>
<div class="m2"><p>نیست ممکن که کس چو او بازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیک یک عادتِ دگر دارد</p></div>
<div class="m2"><p>همه با رایِ خویشتن سازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقان را ز خویش بستاند</p></div>
<div class="m2"><p>خان و مان شان به کل براندازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوستان را بر آتشِ هجران</p></div>
<div class="m2"><p>چون نزاریِ زار بگدازد</p></div></div>