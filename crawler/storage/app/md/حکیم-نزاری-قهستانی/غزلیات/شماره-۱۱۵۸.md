---
title: >-
    شمارهٔ ۱۱۵۸
---
# شمارهٔ ۱۱۵۸

<div class="b" id="bn1"><div class="m1"><p>گر هیچ صبا به ما گذر کردی</p></div>
<div class="m2"><p>وز دوست به ما پیامی‌ آوردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان و دل اگر چه بی دل و جانم</p></div>
<div class="m2"><p>بستاندی و به دوست بسپردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بندگیی برد ز من جایی</p></div>
<div class="m2"><p>کو یاری و هم‌دمی و هم‌دردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با من چه فتاد مدّعی را کاشک</p></div>
<div class="m2"><p>هر کس غم کار خویشتن خوردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای قوم حُذر کنید از این طوفان</p></div>
<div class="m2"><p>ترسم که به دامنی رسد گردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر قدر فنای عشق دانستی</p></div>
<div class="m2"><p>افسرده به ناز جان نپروردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگریز نزاریا از آن میدان</p></div>
<div class="m2"><p>کت نیست مجال هیچ ناوردی</p></div></div>