---
title: >-
    شمارهٔ ۲۰۴
---
# شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>قبله ی روحانی ما روی تست</p></div>
<div class="m2"><p>کعبه ی دوجهانی ما کوی تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه ز شب راه بری نادرست</p></div>
<div class="m2"><p>رهبر ره گم شدگان موی تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانش جز وی چه بود ،عقل کل</p></div>
<div class="m2"><p>شیفته ی غمزه ی جادوی تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پنجه ی عقل من و ما از کجا</p></div>
<div class="m2"><p>مرتبه ی قوت بازوی تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دولت آن کس که به پیوند عشق</p></div>
<div class="m2"><p>حلق دلش بسته ی گیسوی تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هان جگر سوخته ی بی دلان</p></div>
<div class="m2"><p>زنده به بادی ست که از سوی تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شادی رویت که دل عاشقان</p></div>
<div class="m2"><p>جفت غم از طاق دو ابروی تست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ کس از بند تو آزاد نیست</p></div>
<div class="m2"><p>در همه آفاق هیاهوی تست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کیست نزاری که چو او صد هزار</p></div>
<div class="m2"><p>بنده ی آزاده ی هندوی تست</p></div></div>