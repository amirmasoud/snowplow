---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>جام پر جان کن بیاور ساقیا</p></div>
<div class="m2"><p>بین که می گردد سرم چون آسیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبع را اکسیر می پر می کنم</p></div>
<div class="m2"><p>می کنم حاصل از او این کیمیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم بدین اکسیر حاصل می شود</p></div>
<div class="m2"><p>کیمیای معنوی از اسخیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرب من ام الخبائث کی بود</p></div>
<div class="m2"><p>من به پاکی می خورم با ازکیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راستی را هم حرام و هم پلید</p></div>
<div class="m2"><p>هست و شد بر بی ثبات و بی حیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنگ را قومی به جای می خورند</p></div>
<div class="m2"><p>ظلمت شب را چه نسبت به ضیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آری آری هست آنجا نکته یی</p></div>
<div class="m2"><p>بیخ حیوانات باشد در گیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سود کی باشد کشیدن در بصر</p></div>
<div class="m2"><p>خاک کر در همچو گرد توتیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر کسی را ناخوش آید باک نیست</p></div>
<div class="m2"><p>راست می گوید نزاری بی ریا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منکران از جهل نشناسند باز</p></div>
<div class="m2"><p>اطلس و نخ از حریر و بوریا</p></div></div>