---
title: >-
    شمارهٔ ۱۰۵۵
---
# شمارهٔ ۱۰۵۵

<div class="b" id="bn1"><div class="m1"><p>برآوردند طوفان از جهان دیوان سلیمان کو</p></div>
<div class="m2"><p>ز سحرِ سامری پر شد همه آفاق ثعبان کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبیبان عاجز و مضطر فرو ماندند از این علّت</p></div>
<div class="m2"><p>بلی دردِ فراوان هست اندک مایه درمان کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان با دل چو نبود راست، ایمان کی بود صادق</p></div>
<div class="m2"><p>شهادت گوی بسیارند امّا اهلِ ایمان کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو دعوی می کنی با من که من در عالم رتبت</p></div>
<div class="m2"><p>مطیع حکم سلطانم و لیکن حکمِ سلطان کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر من با تو برگویم که حجّت نیست بی فرمان</p></div>
<div class="m2"><p>تو خواهی گفت اگر از حکمِ سلطان است فرمان کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منت گویم که کشفی می رود در عالمِ باطن</p></div>
<div class="m2"><p>تو خواهی حجّت آوردن که حجّت کیست برهان کو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مسلمانی اگر گویم به تسلیم است خواهی گفت</p></div>
<div class="m2"><p>درست است این سخن آری مقرّر شد مسلمان کو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا الزام خواهی کرد بر شرطِ مسلمانی</p></div>
<div class="m2"><p>سخن خود با سخن دان است امّا آن سخن دان کو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نزاری پیشِ پا بنگر مگو اسرار پنهانی</p></div>
<div class="m2"><p>سخن خود با سخن دان است سخن دانی چو سلمان کو</p></div></div>