---
title: >-
    شمارهٔ ۱۲۵۷
---
# شمارهٔ ۱۲۵۷

<div class="b" id="bn1"><div class="m1"><p>بمیر و تا نبود هم‌نفس مزن نفسی</p></div>
<div class="m2"><p>که مرگ بهتر از این زندگی بود به بسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو با کسی نبود عهد روزگار عزیز</p></div>
<div class="m2"><p>هباست گر همه عمری بود اگر نفسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو زاهدیّ و منم فاسقی چه میگویی</p></div>
<div class="m2"><p>کدام به تو به خود می روی و من به کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به صدر خاص به خاصی توان رسید بلی</p></div>
<div class="m2"><p>که عشق گوهر خاص است و عقل عام خسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گوش جان بشنو صور حق نه هم چو خران</p></div>
<div class="m2"><p>که مولعند به بانگ میان‌تهی جرسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن قبل هوس روی دوست می‌کندم</p></div>
<div class="m2"><p>که آدمی بچه را چاره نیست از هوسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا کی ام که تمنای وصل دوست بود</p></div>
<div class="m2"><p>کجا به دولت عنقا رسد چو من مگسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نزاریا برو و دامنی به دست آور</p></div>
<div class="m2"><p>که کس بدو نرسیده ست جز به دسترسی</p></div></div>