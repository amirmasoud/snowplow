---
title: >-
    شمارهٔ ۶۰۰
---
# شمارهٔ ۶۰۰

<div class="b" id="bn1"><div class="m1"><p>ای صنمِ خرگهی خیمه برون زد بهار</p></div>
<div class="m2"><p>گر ز جهان آگهی بادۀ نوشین بیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیز به بستان خرام باده همی خور مدام</p></div>
<div class="m2"><p>گشته جهانی به کام دست ز شادی مدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دلِ لاله ز باد عنبرِ سارا فتاد</p></div>
<div class="m2"><p>دستِ صبا چون گشاد نافۀ مشکِ تتار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای پسرِ سیم تن گشت شکفته سمن</p></div>
<div class="m2"><p>وز خوشی آخر چمن زار شد و مستعار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیز نزاری و شو باغ نو و یارِ نو</p></div>
<div class="m2"><p>نغمۀ مرغان شنو از سرِ هر شاخ سار</p></div></div>