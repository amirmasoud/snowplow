---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>ره نباشد در حریم عشق هر اوباش را</p></div>
<div class="m2"><p>طاقت خورشید ناممکن بود خفاش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پادشاهی نیست جز درویشی و آزادگی</p></div>
<div class="m2"><p>پس میسر نیست هفت اقلیم جز قلاش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تفرج می‌کنی باری بیا طوفی بکن</p></div>
<div class="m2"><p>عالم دردی کشان بی غم خوش باش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور باش از اهل دنیا زان که نا ایمن بود</p></div>
<div class="m2"><p>روزگار از خوف سلطان حاجب و فراش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیک خواه و نیک باش و نیک بین و نیک دان</p></div>
<div class="m2"><p>چون قلم رفته ست بر لوح ازل نقاش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم مزن با نفس ناقص مشورت با عقل کن</p></div>
<div class="m2"><p>مرد عاقل کی به نامحرم برد کنکاش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سینه خالی کن نزاری تا فرود آید ملک</p></div>
<div class="m2"><p>گر نه کی بتوان کشیدن کینه و پرخاش را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتش خشمت بریزد آب روی ایمن مباش</p></div>
<div class="m2"><p>آب جوی است آب رویت در نظر فحاش را</p></div></div>