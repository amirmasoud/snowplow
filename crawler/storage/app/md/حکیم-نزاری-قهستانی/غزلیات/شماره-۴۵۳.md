---
title: >-
    شمارهٔ ۴۵۳
---
# شمارهٔ ۴۵۳

<div class="b" id="bn1"><div class="m1"><p>امروز که باده می توان خورد</p></div>
<div class="m2"><p>یک هفته بهانه می توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هان تا به موافقت برآریم</p></div>
<div class="m2"><p>از مغزِ خمِ گرفته سر گرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کو سرِ پایِ خم ندارد</p></div>
<div class="m2"><p>گو هم چو زمانه گردِ سر گرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون لاله کنیم از پیاله</p></div>
<div class="m2"><p>رویی که شده ست چون گلِ زرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان می که ز عکسِ پرتوِ او</p></div>
<div class="m2"><p>از چوبِ سیه برون دَمَد ورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان می که پلنگِ هیبت او</p></div>
<div class="m2"><p>با عقل کند چو شیر ناورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سنگ زدیم شیشۀ نام</p></div>
<div class="m2"><p>از ننگِ وجودِ ناز پرورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون فایده نیست چند نالیم</p></div>
<div class="m2"><p>از جورِ رقیبِ ناجوان مرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیهات که صرصرِ ملامت</p></div>
<div class="m2"><p>طوفان ز وجودِ ما برآورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما پختۀ کارگاهِ عشقیم</p></div>
<div class="m2"><p>گو عقل مکوب آهنِ سرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیزار شدیم از نزاری</p></div>
<div class="m2"><p>ماییم و حریفِ دُردیِ درد</p></div></div>