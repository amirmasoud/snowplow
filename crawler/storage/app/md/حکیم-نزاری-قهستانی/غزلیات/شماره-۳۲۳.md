---
title: >-
    شمارهٔ ۳۲۳
---
# شمارهٔ ۳۲۳

<div class="b" id="bn1"><div class="m1"><p>رهِ عشّاق سپردن به دل آزاری نیست</p></div>
<div class="m2"><p>جز به دل سوزی و دل جویی و دل داری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه کنم با دل شوریده که از بدو وجود</p></div>
<div class="m2"><p>مست جامی ست که امّید به هشیاری نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون تبرّا و تولّا به مشعبد گهِ عشق</p></div>
<div class="m2"><p>بازیی نیست که از حقّه برون آری نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چاره تسلیم و رضا بیش مگو از من و ما</p></div>
<div class="m2"><p>هیچ تدبیر دگر تا که بنسپاری نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما به جان حاضر وقتیم و به دل ناظر دوست</p></div>
<div class="m2"><p>سیر عاشق به گرانی و سبک باری نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب و غربال بود دعوی بی معنی و هیچ</p></div>
<div class="m2"><p>خاک بر یاری یاری که همه یاری نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از نزاری تو به زاری نکنی بیزاری</p></div>
<div class="m2"><p>شرط آزار بر آن است که بیزاری نیست</p></div></div>