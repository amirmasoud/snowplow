---
title: >-
    شمارهٔ ۳۹۰
---
# شمارهٔ ۳۹۰

<div class="b" id="bn1"><div class="m1"><p>چه شور از آن لب شیرین که در جهان افتاد</p></div>
<div class="m2"><p>ز قامتت چه قیامت که در زمان افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میان ما و شما وعده ی کناری بود</p></div>
<div class="m2"><p>تو با کنار شدی فتنه در میان افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسوخت صفحه رویم ز آب گرم سرشک</p></div>
<div class="m2"><p>که آتشم ز تو در مغز استخوان افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر غم تو که یک دم نمی شود غایب</p></div>
<div class="m2"><p>به قرعه بر من مسکین ناتوان افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یک کرشمه که کردی ز گوشه برقع</p></div>
<div class="m2"><p>هزار بی دل بی چاره در گمان افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دریغ نام تو آلوده دهانِ خسان</p></div>
<div class="m2"><p>به خاص و عام رسد هر چه در زبان افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر ز حسن تو آوازه در جهان افکند</p></div>
<div class="m2"><p>به اختیار نزاری نبد چنان افتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوال کرد و به من گفت دوستی که بگو</p></div>
<div class="m2"><p>تویی که بویِ عبیر ِتو در جهان افتاد</p></div></div>