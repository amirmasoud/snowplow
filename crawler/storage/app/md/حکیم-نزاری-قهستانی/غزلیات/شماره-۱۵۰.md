---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>دل و جانم آنجا که جان و دل است</p></div>
<div class="m2"><p>به جانان که بی جان و دل مشکل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غلط میکنم چند گویم ز دل</p></div>
<div class="m2"><p>به جانان رسیدن نه کار دل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز مبدای فطرت برفته ست حکم</p></div>
<div class="m2"><p>همین است و بس جان به جان مایل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه دنیا بمانده ست و نه دین به من</p></div>
<div class="m2"><p>مرا از محبت همین حاصل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفته نشیمن گه جان من</p></div>
<div class="m2"><p>عقابی دلاور ولی بسمل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهنگی ست در قلزم آز من</p></div>
<div class="m2"><p>و لیکن چو لنگر دو پا در گل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر امید یک قطره از بحر عشق</p></div>
<div class="m2"><p>دو عالم به صد غصه بر ساحل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بن گاه تقلید بربند رخت</p></div>
<div class="m2"><p>که مقصد فراتر از آن منزل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندانی به خود هیچ دانا کسی ست</p></div>
<div class="m2"><p>کر بر جمله دانندگان فاضل است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندانست مستودع از مستقر</p></div>
<div class="m2"><p>هر آن کو درین امتحان داخل است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نزاری ز مردان حق گو سخن</p></div>
<div class="m2"><p>دگر هر چه گویی همه باطل است</p></div></div>