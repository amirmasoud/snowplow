---
title: >-
    شمارهٔ ۷۳۸
---
# شمارهٔ ۷۳۸

<div class="b" id="bn1"><div class="m1"><p>هر که نباشد چو من پس رو ارباب عشق</p></div>
<div class="m2"><p>خامی و افسرده ایست بی خبر از باب عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدعیان کرده اند پشت بر احکام عقل</p></div>
<div class="m2"><p>معتقدان کرده اند روی به محراب عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترک خور خواب کن برگ مشقت بساز</p></div>
<div class="m2"><p>نیست به آسودگی رخصت اصحاب عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که بهشت آرزو می کنی و غافلی</p></div>
<div class="m2"><p>روضه ی ما کوی دوست رضوان بوّاب عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردن عشاق بین قید به زنجیر شوق</p></div>
<div class="m2"><p>بر در مشتاق بین جذب به قلاب عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه مشوش شود جوف دماغ خرد</p></div>
<div class="m2"><p>گو به من آی و بخور شربت جلاب عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برق نفاقش بزد سوخته خرمن بماند</p></div>
<div class="m2"><p>هر که به رغبت نداد خانه به سیلاب عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقد نبهره نزد عشق چو قلاب عقل</p></div>
<div class="m2"><p>گشت روان لاجرم سکه ی ضراب عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کشتی تدبیر ما کی به درآید ز موج</p></div>
<div class="m2"><p>عشق محیط است و عقل غرقه به گرداب عشق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سوز و نیازی رسید قسم نزاری و بیش</p></div>
<div class="m2"><p>کلکی و فکری نداشت از همه اسباب عشق</p></div></div>