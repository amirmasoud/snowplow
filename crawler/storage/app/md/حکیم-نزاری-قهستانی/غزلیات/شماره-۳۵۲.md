---
title: >-
    شمارهٔ ۳۵۲
---
# شمارهٔ ۳۵۲

<div class="b" id="bn1"><div class="m1"><p>تا دل مجنون ما راه قلندر گرفت</p></div>
<div class="m2"><p>هر چه نه لیلیش بود از همه دل برگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر به گریبان عشق برزد و مردانه وار</p></div>
<div class="m2"><p>جان به میان برنهاد دامن دل برگرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق چو از کنج غیب راه کمین برگشاد</p></div>
<div class="m2"><p>دل ز سر جان برفت سر کم افسر گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز چو برخاستم از روش نام و ننگ</p></div>
<div class="m2"><p>بار دگر طعنه زن ولوله از سر گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی خبران را چه بیم از اثر برق عشق</p></div>
<div class="m2"><p>عشق نه آن است کو با همه کس درگرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حق هر بی هنر عشق نظر کی کند</p></div>
<div class="m2"><p>خضر به مقصد رسید رنج سکندر گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوز نزاری ز جان بس که علم برکشید</p></div>
<div class="m2"><p>قبّه ی مینا بسوخت گنبد اخضر گرفت</p></div></div>