---
title: >-
    شمارهٔ ۱۲۴۹
---
# شمارهٔ ۱۲۴۹

<div class="b" id="bn1"><div class="m1"><p>گرم به کینه بسوزی وگر به مهر بسازی</p></div>
<div class="m2"><p>ز بندگان مطیعم حقیقتی نه مجازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر در آتش سوزانم از تو باک نباشد</p></div>
<div class="m2"><p>که پاک تر شود آن زر که بیشتر بگدازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش آمدی که حیاتی به تازگی به وجودم</p></div>
<div class="m2"><p>درآمد از رخت ای غیرتِ بتانِ ترازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه سعادت و صحّت که آمدی به عیادت</p></div>
<div class="m2"><p>ندانم این چه خداوندی است و بنده نوازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز روی خوب نباشد غریب خوی خوش آری</p></div>
<div class="m2"><p>در آفرینش حق رمزها بود نه به بازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خُلق می سزد او بر فرشته فخر نمایی</p></div>
<div class="m2"><p>به حسن می رسدت گر بر آفتاب بنازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محلّ قدر تو چون داند از قیاس نزاری</p></div>
<div class="m2"><p>چو در اُمتیان عجم لفظ و استعارت تازی</p></div></div>