---
title: >-
    شمارهٔ ۹۲۵
---
# شمارهٔ ۹۲۵

<div class="b" id="bn1"><div class="m1"><p>وقتِ آن آمد که بر غندان زنیم</p></div>
<div class="m2"><p>جامِ می بر طلعتِ جانان زنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون مقارن می‌رسد ماهِ صیام</p></div>
<div class="m2"><p>هر چه باداباد بر شعبان زنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیش و کم یک هفته در پایانِ جنگ</p></div>
<div class="m2"><p>بر سماعِ مطربان دستان زنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طوقِ گردن گیسویِ پرچین کنم</p></div>
<div class="m2"><p>خاک در چشمِ خطابینان زنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شادیِ رندان و قلّاشانِ عشق</p></div>
<div class="m2"><p>ما دمِ اخلاص با ایشان زنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کفر و ایمان پای‌بندِ عاقل است</p></div>
<div class="m2"><p>ما به می بر کفر و بر ایمان زنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترکِ نام و ننگ و دین و دل کنیم</p></div>
<div class="m2"><p>سنگ بر قندیلِ جسم و جان زنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بارگاهِ فقر برگردون کشیم</p></div>
<div class="m2"><p>سایه‌بانِ فاقه بر کیوان زنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قصّه کوته کن نزاری بازگوی</p></div>
<div class="m2"><p>وقتِ آن آمد که بر غندان زنیم</p></div></div>