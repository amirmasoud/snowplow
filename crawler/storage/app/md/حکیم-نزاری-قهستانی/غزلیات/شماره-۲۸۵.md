---
title: >-
    شمارهٔ ۲۸۵
---
# شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>کار ما با نفسِ بازپسین افتاده ست</p></div>
<div class="m2"><p>آخر ای دوست همه مهرِ تو کین افتاده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا مگر رسمِ عیادت ز جهان برخیزد</p></div>
<div class="m2"><p>در میان این همه تعویق ازین افتاده ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه همانا که ز تأثیرِ مرض رنجه شود</p></div>
<div class="m2"><p>هر که را هم نفسی چون تو قرین افتاده ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندهد خاصیتِ چشمۀ نوشینِ لبت</p></div>
<div class="m2"><p>حوضِ کوثر که درو ماءِ معین افتاده ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر درِ روضه که دیده ست زمانی بوّاب</p></div>
<div class="m2"><p>با رقیبِ تو مرا راست همین افتاده ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من و بی داد کشیدن تو و شلتاق و عقاب</p></div>
<div class="m2"><p>آری از بدوِ ازل حکم چنین افتاده ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چارمیخِ غمِ عشقِ تو بمانده ست دلم</p></div>
<div class="m2"><p>راست در حلقۀ زلفِ تو نگین افتاده ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غمِ تنهایی و اندوهِ جدایی هیهات</p></div>
<div class="m2"><p>این همه قسمِ نزاریِ حزین افتاده ست</p></div></div>