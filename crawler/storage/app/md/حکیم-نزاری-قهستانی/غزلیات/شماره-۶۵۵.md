---
title: >-
    شمارهٔ ۶۵۵
---
# شمارهٔ ۶۵۵

<div class="b" id="bn1"><div class="m1"><p>خون رز بر خاک می‌ریزی مریز</p></div>
<div class="m2"><p>می‌کنی در خون خود با ما ستیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار باشی به گران‌جانی مکن</p></div>
<div class="m2"><p>بیش ازین منشین سبک‌تر باش خیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاضرِ فرزندِ وقتِ خویش باش</p></div>
<div class="m2"><p>گر نداری طاقت ای بابا گریز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر بساط بزم مستان الست</p></div>
<div class="m2"><p>چون نزاری پاک باز و خصل ریز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو چه دانی چون کند از هم جدا</p></div>
<div class="m2"><p>هالک و ناجی به روز رستخیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ز خاک تیره چون خیزد ز حشر</p></div>
<div class="m2"><p>استخوان پوده پوده ریز ریز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قارنِ میدانِ مردِ عشق باش</p></div>
<div class="m2"><p>نه چو قارون بر سر هم نه جهیز</p></div></div>