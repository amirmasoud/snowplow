---
title: >-
    شمارهٔ ۶۰۴
---
# شمارهٔ ۶۰۴

<div class="b" id="bn1"><div class="m1"><p>تا تو را عشق به کلّی نکند زیر و زبر</p></div>
<div class="m2"><p>کی شود جانِ تو از عالمِ ارواح خبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق اوّل ز تو این جان و جهان بستاند</p></div>
<div class="m2"><p>بعد از آنت بدهد جان و جهانی دیگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هستیی بخشدت آن گه که فنا نپذیرد</p></div>
<div class="m2"><p>در امانیت نشاند که نترسی ز خطر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق را مردِ قدم باید نه مردِ غلو</p></div>
<div class="m2"><p>عشق را مردِ سفر باید نه مردِ حضر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشقان ره به توکّلتُ علی‌الله سپرند</p></div>
<div class="m2"><p>پس تو هم جز به توکلت علی‌الله مسپر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو تدبیر کنی ور نکنی حکمِ قضا</p></div>
<div class="m2"><p>نه به خیر از تو بگردد به حقیقت نه به شر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور قبولی به تو وجهدِ تو حاجت نبود</p></div>
<div class="m2"><p>ور نه‌ای چون نکند سود حِیل رنج مبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوزنی را که بدان دیده بدوزند ترا</p></div>
<div class="m2"><p>چون نظر شد چه ز آهن زده سوزن چه ز زر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راست چون سرو مجّرد شو و یک تا می‌باش</p></div>
<div class="m2"><p>کز دوتایی‌ست گرفتارِ فنا حلقۀ در</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون ندادی خطِ تسلیم و ارادت مطلب</p></div>
<div class="m2"><p>کم ز پروانه توان بود به پروانه نگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون امانت بسپردی تو برو او داند</p></div>
<div class="m2"><p>عهده بیرون فکن از گردن و اندیشه مخور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به میان در نه تو بر هیچی و نه من بر هیچ</p></div>
<div class="m2"><p>کار تقدیم قضا دارد و تقدیرِ قدر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با تو بر گفت نزاری روشِ مذهبِ خویش</p></div>
<div class="m2"><p>راهِ دیوانگی این است تو دانی دیگر</p></div></div>