---
title: >-
    شمارهٔ ۹۴۷
---
# شمارهٔ ۹۴۷

<div class="b" id="bn1"><div class="m1"><p>باد بهار می وزد از طرف نهارجان</p></div>
<div class="m2"><p>ای که فدای آن چنان باد چنین هزار جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی عیسوی نفس چند ز انتظار بس</p></div>
<div class="m2"><p>تا به خلاف شرع و دین نوش کنیم بیار جان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان من است جام می خضر من و مدام من</p></div>
<div class="m2"><p>در طلب تو برمیان بسته ام استوار جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لایق اگرچه نیستم عاشق صادقم بلی</p></div>
<div class="m2"><p>ور نکنم نه صادقم در طلبت هزار جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دل اگر چشیده ای جرعه ی شوق بایدت</p></div>
<div class="m2"><p>کرد چو کوهکن فدا بهر وفای یار جان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می خور و غم مخور جوی ، نیست جز این برون شوی</p></div>
<div class="m2"><p>از پی نازنین گلی رنج مبر مخار جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منتظران نسیه را نیست ز نقد حاصلی</p></div>
<div class="m2"><p>صرف مکن نزاریا در سر انتظار جان</p></div></div>