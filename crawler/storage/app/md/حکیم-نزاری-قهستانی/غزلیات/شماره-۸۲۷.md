---
title: >-
    شمارهٔ ۸۲۷
---
# شمارهٔ ۸۲۷

<div class="b" id="bn1"><div class="m1"><p>چندان که در سلوک ز خود پیش تر شدم</p></div>
<div class="m2"><p>هر بار زنده باز به جانی دگر شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بازِ چشم دوخته بودم به دستِ شاه</p></div>
<div class="m2"><p>خوش خوش به روشنایی او دیده ور شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تنگ نایِ هستیِ خود چون مجال نیست</p></div>
<div class="m2"><p>تا پادشه نزول کند من به در شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود هیچ کس نگفت که آخر مگر کسی</p></div>
<div class="m2"><p>در پرده دیده ام که چنین پرده در شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آشفته مغز بودم و شوریده سر بسی</p></div>
<div class="m2"><p>در هر زمان به مستی و رندی سمر شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز به روزگارِ جوانی نبوده ام</p></div>
<div class="m2"><p>دیوانه تر ازین که به پیرانه سر شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم گذر کنم به کنارِ محیطِ عشق</p></div>
<div class="m2"><p>خود آب درگذشت ز سر تا خبر شدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بوده ام تتبّع عشّاق کرده ام</p></div>
<div class="m2"><p>نه بر مجاز پس رو عقل و نظر شدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لیلی ز در درآمد و مجنون ز هوش رفت</p></div>
<div class="m2"><p>گل با چمن رسید وز بلبل بتر شدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرهاد وار شورِ نزاری جهان گرفت</p></div>
<div class="m2"><p>شیرین سخن چنین ز لبِ چون شکر شدم</p></div></div>