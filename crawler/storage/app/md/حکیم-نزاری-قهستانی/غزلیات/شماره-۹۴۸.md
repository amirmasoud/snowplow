---
title: >-
    شمارهٔ ۹۴۸
---
# شمارهٔ ۹۴۸

<div class="b" id="bn1"><div class="m1"><p>بحر عشق است این و در وی موج بیم و جان</p></div>
<div class="m2"><p>گرنداری ترک جان باری سر خود گیر هان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقان در قلزم عشقند و کشتی بر کنار</p></div>
<div class="m2"><p>تا که را بیرون برد موج هدایت از میان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طاقت موجی ندارد بد دل و از بس غرور</p></div>
<div class="m2"><p>در دماغ افکنده چندان باد همچون بادبان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوستانش خوار بنمایند خود را و حقیر</p></div>
<div class="m2"><p>راز خود دارند از کوته نظر دایم نهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلطنت بگذشت کیخسرو جهان بدرود کرد</p></div>
<div class="m2"><p>هرکس از حکمت نداند تا کجا رفت او چنان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرد جانان دوست جان دشمن بود مالاکلام</p></div>
<div class="m2"><p>بایزیدی یا یزیدی هر دو بودن کی توان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موسی و چوب و شبان فرعون و چندان سلطنت</p></div>
<div class="m2"><p>تو ندانی لیک او را حکمتی باشد در آن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او تواند وانمود آثار صنع از سر غیب</p></div>
<div class="m2"><p>ورنه هر گز کی توان دادن نشان از بی نشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من نخواهم هرگز افکندن سپر از روی عجز</p></div>
<div class="m2"><p>گرچه هر کس در نزاری می کشد تیغ زبان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آتشی دارم که میسوزاندم ای خام طبع</p></div>
<div class="m2"><p>من به دریای محبت غرقه ام تو بر کران</p></div></div>