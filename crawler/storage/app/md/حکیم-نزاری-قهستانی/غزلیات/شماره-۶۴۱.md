---
title: >-
    شمارهٔ ۶۴۱
---
# شمارهٔ ۶۴۱

<div class="b" id="bn1"><div class="m1"><p>گر خدا دولت و بختم دهد و عمر دراز</p></div>
<div class="m2"><p>دیده روشن کنم از خاک سر کوی تو باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آستانت به تضرع ز خدا می‌خواهم</p></div>
<div class="m2"><p>که نکرده ست کسی جز به نیاز این در باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس ندانم که در این ورطه مرا چاره کند</p></div>
<div class="m2"><p>من و درگاه خداوند تعالی و نیاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی در روی تو می‌خواهم و کنجی همه عمر</p></div>
<div class="m2"><p>بر همه خلق جهان کرده در انس فراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من تعصب نکنم هرکه مرا عیب کند</p></div>
<div class="m2"><p>قول بدگوی یقینم که محال است و مجاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش سوخته از خام نپرسید آری</p></div>
<div class="m2"><p>منکر عشق ندارد خبر از عالم راز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جگرم خون شد از اندیشهٔ بی هم‌نفسی</p></div>
<div class="m2"><p>محرمی کو که بدو قصه توان کرد آغاز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلبلان را بود آخر که بنالند به حال</p></div>
<div class="m2"><p>من خود آن زهره ندارم که برآرم آواز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر خود نیز همم نیست که از سایهٔ خود</p></div>
<div class="m2"><p>هستم از بیم گریزنده و چو تیهو از باز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوش با دل ز پریشانی خود می‌گفتم</p></div>
<div class="m2"><p>تو بدین محتشم افکنده‌ای از نعمت و ناز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت آری که نه همواره شب وصل بود</p></div>
<div class="m2"><p>روزکی چند صبوری کن و با هجر بساز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کس نیارد به حیل دامن تقدیر به کف</p></div>
<div class="m2"><p>دست تدبیر چه کوتاه نزاری چه دراز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>میل مرغ از طرف دانه و غافل که قضا</p></div>
<div class="m2"><p>به سر دام بلا می‌بردش در پرواز</p></div></div>