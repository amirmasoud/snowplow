---
title: >-
    شمارهٔ ۱۰۲۹
---
# شمارهٔ ۱۰۲۹

<div class="b" id="bn1"><div class="m1"><p>دوش مرا گفت عقل از سر رای زرین</p></div>
<div class="m2"><p>کای پسر احباب را بر دو جهان بر گزین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز دم ایشان مزن جز درِ ایشان مرو</p></div>
<div class="m2"><p>جز دل ایشان مجوی جز رخِ ایشان مبین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس ز وجود و عدم تا به کی از کیف و کم</p></div>
<div class="m2"><p>خیز برون نه قدم از صفتِ کفر و دین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر قدم جان نهی در ره اخلاص شان</p></div>
<div class="m2"><p>در قدمت آسمان پست شود چون زمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عزم فلک باشدت مجلس ایشان طلب</p></div>
<div class="m2"><p>خلد برین بایدت در صف ایشان نشین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جام محبت بگیر از کف ساقی بنوش</p></div>
<div class="m2"><p>اینت شراب طهور در نظر حور عین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در طلب زندگی باز نمایی مگر</p></div>
<div class="m2"><p>از قدح عارفان چشمه ماءِ معین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روی بگردان ز خود راهِ بقا پیش گیر</p></div>
<div class="m2"><p>روی ندارد به حق راهِ بقا جز چنین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان نزاری فدا در قدم دوستان</p></div>
<div class="m2"><p>در روشِ عشق نیست مرتبه ای بیش از این</p></div></div>