---
title: >-
    شمارهٔ ۹۹۲
---
# شمارهٔ ۹۹۲

<div class="b" id="bn1"><div class="m1"><p>ای دل حصارِ همّتِ مردان پناه کن</p></div>
<div class="m2"><p>دنیا و دین به مرتبه تسلیمِ راه کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا طفلِ نفس خو کند از شیرِ حرص‌باز</p></div>
<div class="m2"><p>پستانِ حرص و آز و هوا سر سیاه کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آیینه است نفس و در او نقشِ آرزو</p></div>
<div class="m2"><p>هر گه که پیشِ رویِ تو برخاست آه کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عین الیقین معاینه دیدی بیار خاک</p></div>
<div class="m2"><p>در دیده‌هایِ شرک و شک و اشتباه کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردانه باش و هم چو دگر جاهلان مساز</p></div>
<div class="m2"><p>با گرگِ نفس یوسفِ دل را به چاه کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا در دریچه‌ی نظرت نگذرد خسی</p></div>
<div class="m2"><p>یعقوب‌وار چشم جهان‌بین تباه کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل با خدای دار و به بتخانه راز گوی</p></div>
<div class="m2"><p>در کعبه باش و قبله ز هر سو که خواه کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در شش در ست نردِ حیاتت نزاریا</p></div>
<div class="m2"><p>آخر به شش جهاتِ جهان در نگاه کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر جاهِ‌این جهانِ جهنده چه اعتماد</p></div>
<div class="m2"><p>چاهِ بلاست جاهِ جهان ترکِ جاه کن</p></div></div>