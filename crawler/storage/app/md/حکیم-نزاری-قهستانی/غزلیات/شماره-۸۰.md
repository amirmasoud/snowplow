---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>دوش مرا حالتی روی نمودی عجب</p></div>
<div class="m2"><p>در نظرم آفتاب تا سحر از نیمه شب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نادره تر این که شب ، کرد ظهور آفتاب</p></div>
<div class="m2"><p>من شده در پیش او ذره صفت مضطرب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوده بس از اضطراب بی خبر از خویشتن</p></div>
<div class="m2"><p>جاذبه ای بی کشش ، واسطه ای بی سبب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم من از نور خور حیران خفاش وار</p></div>
<div class="m2"><p>روح من از راح شوق غرق نشاط و طرب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خم تسنیم نوش کرده به تسلیم من</p></div>
<div class="m2"><p>وز سر تعظیم پیش چشم ز روی ادب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش طور و کلیم لازمه ی حال ما</p></div>
<div class="m2"><p>واقعه ای با هوس معجزه ای با طلب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رقعه بگسترد عشق مهره فروچید شوق</p></div>
<div class="m2"><p>تعبیه ای جمع شد تفرقه در یک ندب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این همه در باختم خانه برانداختم</p></div>
<div class="m2"><p>قصه کنم مختصر شرح کنم منتخب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچ دگر نیست هیچ جز همه او والسلام</p></div>
<div class="m2"><p>پیش نزاری مگو از گهر و از نسب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هیبت آن شب چنان برد دل من که شد</p></div>
<div class="m2"><p>بود من و حال من زار چو باد قصب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خلق زبان دراز کرده چو الماس تیز</p></div>
<div class="m2"><p>طعنه دهی از قفا سرزنشی از عقب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بار ملامتگران می کشم و می برم</p></div>
<div class="m2"><p>می گذرانم خوشی دور عنا و تعب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یافت نزاری به عشق از خودی خود خلاص</p></div>
<div class="m2"><p>عشق عطیت بود نه هنر مکتسب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حمداً لله که من یافته ام نقد وقت</p></div>
<div class="m2"><p>منتظر نسیه را هست خیالی عجب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ساقی وحدت بیار ، تا بزداید غبار</p></div>
<div class="m2"><p>زآینه ی روح ما صیقل آب عنب</p></div></div>