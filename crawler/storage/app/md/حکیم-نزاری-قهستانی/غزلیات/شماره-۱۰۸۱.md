---
title: >-
    شمارهٔ ۱۰۸۱
---
# شمارهٔ ۱۰۸۱

<div class="b" id="bn1"><div class="m1"><p>دوش بازم قاصدی از حضرت یار آمده</p></div>
<div class="m2"><p>نی غلط کردم کدامین دوش هم‌وار آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مملکت بخشیم و مملوکیم و در رتبت به ما</p></div>
<div class="m2"><p>از ملایک دم به دم الهامِ بسیار آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ز خود بیرون نیایی ره نیابی در جرم</p></div>
<div class="m2"><p>کی خرد در منزلِ عشّاق هشیار آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق ما بازی نباشد عزم سربازی مکن</p></div>
<div class="m2"><p>یار می باید که باشد چست و عیار آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قطعِ جان خوش کرده‌ام زآن شب که دیدم روی دوست</p></div>
<div class="m2"><p>سر برای این چنین روزی مرا کار آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خیال نرگس چشمان مستش تا به روز</p></div>
<div class="m2"><p>نوک مژگان هر شبم در دیده مسمار آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست در مصر قبولی دل عزیزی هم چو او</p></div>
<div class="m2"><p>هر چه یوسف نیست زان در چشمِ من خوار آمده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاشکی تانستمی گفتن که این درد از کجاست</p></div>
<div class="m2"><p>وز که و کی باز در جانم پدیدار آمده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برگ‌ها در باغ وحدت بر درختِ امتحان</p></div>
<div class="m2"><p>هر یکی در عشق حلّاجی‌ست بر دار آمده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاشقم عاشق به آواز بلند ای دوستان</p></div>
<div class="m2"><p>باش گو کوته نظر بر من به انکار آمده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من ز خود اقرار دارم حاجتِ انکار نیست</p></div>
<div class="m2"><p>جملهٔ اعضای من بر من به اقرار آمده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رفته بودم زین جهان بر بوی او باز آمدم</p></div>
<div class="m2"><p>هم چو بلبل کز برای گل به گل زار آمده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پای بندم گر نزاری بود در بازار عشق</p></div>
<div class="m2"><p>این زمان هستم ازو یک‌باره بیزار آمده</p></div></div>