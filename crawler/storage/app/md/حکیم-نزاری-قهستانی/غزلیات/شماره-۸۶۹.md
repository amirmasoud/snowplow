---
title: >-
    شمارهٔ ۸۶۹
---
# شمارهٔ ۸۶۹

<div class="b" id="bn1"><div class="m1"><p>ای امیدم به تو نومید مکن از خویشم</p></div>
<div class="m2"><p>ناشکیبم ز تو بردار حجاب از پیشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آرزومندی و بی صبری و مشتاقی و غم</p></div>
<div class="m2"><p>کم نمی گردد و هر دم به تو مایل بیشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی شیرین نفسی بی مگسی نادیده</p></div>
<div class="m2"><p>می زنم زار چو فرهاد سری بر تیشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر و جان جمله فدای قدمت خواهم کرد</p></div>
<div class="m2"><p>بیش از این دست رِسَم نیست که بس درویشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن از خویش ملولم سر خود کی دارم</p></div>
<div class="m2"><p>هم ز بیگانه ملالم زد و هم از خویشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به رقیبان که رسانند ز نزاری خبری</p></div>
<div class="m2"><p>که مکوشید به آزار دل بی خویشم</p></div></div>