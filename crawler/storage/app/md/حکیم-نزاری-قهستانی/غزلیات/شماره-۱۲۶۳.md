---
title: >-
    شمارهٔ ۱۲۶۳
---
# شمارهٔ ۱۲۶۳

<div class="b" id="bn1"><div class="m1"><p>گر به فرمانِ من سوخته خرمن باشی</p></div>
<div class="m2"><p>من غلام تو و تو خواجگی من باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ نقصان نکند مملکت حسنِ ترا</p></div>
<div class="m2"><p>که کم آزار و نکو خلق و فروتن باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاصل از کشتنِ من چیست همین هیچ دگر</p></div>
<div class="m2"><p>خون بی‌داد گری کرده به گردن باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دشمن جان منی پس منِ مسکین چه کنم</p></div>
<div class="m2"><p>دوست چون دارمش آن را که تو دشمن باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی وفایی و جفا کاری و بی‌داد گری</p></div>
<div class="m2"><p>خود تو پیوسته بدین کار معیّن باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسنت از حد و نهایت بگذشت احسان کن</p></div>
<div class="m2"><p>تا به اخلاق پسندیده مزیّن باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفس سوختگان در دلِ سخت تو رسد</p></div>
<div class="m2"><p>هم به دم نرم شوی گر همه آهن باشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقل شوریده رَوَد تا تو برین شیوه روی</p></div>
<div class="m2"><p>شهر پر فتنه بود تا تو در این فن باشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی بماند مگر از شعرِ نزاری بیتی</p></div>
<div class="m2"><p>گر تو در شهر چنین خانه برافکن باشی</p></div></div>