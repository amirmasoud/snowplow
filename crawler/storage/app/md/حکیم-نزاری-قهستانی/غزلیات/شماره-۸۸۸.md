---
title: >-
    شمارهٔ ۸۸۸
---
# شمارهٔ ۸۸۸

<div class="b" id="bn1"><div class="m1"><p>شد در سر کار تو هم دنیی و هم دینم</p></div>
<div class="m2"><p>ناداده هنوز از لب یک شربت شیرینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ناله ی زار من رحمت نکنی یک شب</p></div>
<div class="m2"><p>هم در تو رسد روزی سوز دل مسکینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر عمر بود روزی در بزم گه وصلت</p></div>
<div class="m2"><p>بی درد میی نوشم بی خار گلی چینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن جا گه تو نگذاری برگردم و ننشینم</p></div>
<div class="m2"><p>آن جا که تو فرمایی برخیزم و بنشینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر عنف کنی عمری مستوجب آن هستم</p></div>
<div class="m2"><p>ور لطف کنی روزی هم مستحق اینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندان که طلب کردم بسیار نظر بردم</p></div>
<div class="m2"><p>یک سرو نمی بینم کز قد تو بگزینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگذار نزاری را تا روی تو می بیند</p></div>
<div class="m2"><p>کاثار سعادت ها از روی تو می بینم</p></div></div>