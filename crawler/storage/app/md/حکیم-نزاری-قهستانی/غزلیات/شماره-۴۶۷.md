---
title: >-
    شمارهٔ ۴۶۷
---
# شمارهٔ ۴۶۷

<div class="b" id="bn1"><div class="m1"><p>دلم ز سوز جگر دم به دم بمی‌سوزد</p></div>
<div class="m2"><p>کدام دل که ز سر تا قدم بمی‌سوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عجب ز مردمک دیده مانده ام که مدام</p></div>
<div class="m2"><p>میان آب دو چشم است و هم بمی‌سوزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آه من دمِ باد صبا نمی فسرَد</p></div>
<div class="m2"><p>ز سوز من نفس صبح دم بمی‌سوزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز می که مونس جان بود بر شکست دلم</p></div>
<div class="m2"><p>که در عروق ز اندیشه دم بمی‌سوزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جام جم چه کند مفلسی که در شب تار</p></div>
<div class="m2"><p>به دود آه سحر ملک جم بمی‌سوزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به من نماند ز هستی و نیستی چیزی</p></div>
<div class="m2"><p>که برق عشق ، وجود و عدم بمی‌سوزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر بسوخت تنم از سموم غم چه عجب</p></div>
<div class="m2"><p>که آدمی ز بس افراط غم بمی‌سوزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسوختیم و دمی در رقیب ما نگرفت</p></div>
<div class="m2"><p>بلی بلی مگر افسرده کم بمی‌سوزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بس که شعله آهم به شعر در پیچد</p></div>
<div class="m2"><p>مداد وقت کتابت قلم بمی‌سوزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حذر ز سوز نذاری کز آتش سینه</p></div>
<div class="m2"><p>به چنگ بر نفس زیر و بم بمی‌سوزد</p></div></div>