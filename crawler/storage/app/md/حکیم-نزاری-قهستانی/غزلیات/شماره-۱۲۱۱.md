---
title: >-
    شمارهٔ ۱۲۱۱
---
# شمارهٔ ۱۲۱۱

<div class="b" id="bn1"><div class="m1"><p>آرزو می کندم با تو شبی بوس و کناری</p></div>
<div class="m2"><p>باز بر گردن من زلف تو پیچیده چو ماری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به گل زار وصالم نبود راه چه بودی</p></div>
<div class="m2"><p>کز صبا یافتمی بویِ عرق چین تو باری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر درت گر نگذارند که چون حلقه بپایم</p></div>
<div class="m2"><p>چه شود بر سر کویت بنشینم چو غباری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردِ شیرین نبود بی سببِ شور رقیبان</p></div>
<div class="m2"><p>دامنِ گل نبود بی ضرر زحمت خاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توبه بر دستِ نگارین تو خواهم که کنم من</p></div>
<div class="m2"><p>گر به دست چو منی بازفتد چون تو نگاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق اگر در سرِ کار تو کند دین و دلِ من</p></div>
<div class="m2"><p>گو بکن بهر خدا را به ازینم سر و کاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زر و زیور که در آویخته ای می نگذارد</p></div>
<div class="m2"><p>که به گوشِ تو درآید سحری ناله ی زاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاریِ زارِ نزاری بشنو مرحمتی کن</p></div>
<div class="m2"><p>وین قدر بس که برآید به زبانِ تو نزاری</p></div></div>