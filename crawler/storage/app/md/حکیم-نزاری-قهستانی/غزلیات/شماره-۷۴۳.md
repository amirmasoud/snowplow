---
title: >-
    شمارهٔ ۷۴۳
---
# شمارهٔ ۷۴۳

<div class="b" id="bn1"><div class="m1"><p>ای خطبۀ سعادتِ کلّی به نامِ عشق</p></div>
<div class="m2"><p>وی آفتابِ عالمِ هستی نظامِ عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشقم از سیاستِ سلطان نهیب نیست</p></div>
<div class="m2"><p>سلطان چه کس بود که نباشد غلامِ عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس را به دستِ عقل میسّر نمی شود</p></div>
<div class="m2"><p>پایِ دلِ ضعیف گشادن ز دامِ عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صاحب خرد بسا که جگر خورد و در نیافت</p></div>
<div class="m2"><p>چون و چرا و کو و کجا و کدامِ عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>استادِ کاملان همه عشق است و علم و عقل</p></div>
<div class="m2"><p>هستند در تمامیِ خود ناتمامِ عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیوانگان که مست الستند همچنان</p></div>
<div class="m2"><p>مستی کنند بعد قیامت ز جام عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مجنون که جرعه ای به مذاقش رسیده بود</p></div>
<div class="m2"><p>مست همیشگی شد و مست مدام عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما نیز هم ز جمله ی مستان عاشقیم</p></div>
<div class="m2"><p>بی نام و ننگ بی سر و سامان به کام عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مادر به نام و ننگ بپرورد و شیر داد</p></div>
<div class="m2"><p>استاد کار گشت نزاری به کام عشق</p></div></div>