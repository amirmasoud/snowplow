---
title: >-
    شمارهٔ ۹۲۳
---
# شمارهٔ ۹۲۳

<div class="b" id="bn1"><div class="m1"><p>ما که مستانِ الستیم چنان مستانیم</p></div>
<div class="m2"><p>که دگر حاجتِ آن نیست که می بستانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیستی چیست نماندن نفسی بر یک حال</p></div>
<div class="m2"><p>استحالت نکند هستی و ما هستانیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساکنانیم و همه طوفِ سماوات کنیم</p></div>
<div class="m2"><p>خامشانیم و چون بلبل همه سَدْدَستانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه لگد کوبِ رقیبان چو بساطِ چمنیم</p></div>
<div class="m2"><p>گه خوش و تازه و خندان چو گلِ بستانیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه در معرکه با تُرکِ فلک هم نیزه</p></div>
<div class="m2"><p>گاه با زهره و به خلوت‌کنده هم‌دستانیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن راست بیا تو زِ نزاری بشنو</p></div>
<div class="m2"><p>بشنود هر که بداند که زِ قوهستانیم</p></div></div>