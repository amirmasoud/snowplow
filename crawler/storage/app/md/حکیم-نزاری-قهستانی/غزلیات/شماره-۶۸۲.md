---
title: >-
    شمارهٔ ۶۸۲
---
# شمارهٔ ۶۸۲

<div class="b" id="bn1"><div class="m1"><p>ز گردِ چشمۀ حیوان برآمده ست نباتش</p></div>
<div class="m2"><p>درونِ چشمۀ حیوان لبالب آب حیاتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هلاکِ مطلقِ خود را کسی حیات نگوید</p></div>
<div class="m2"><p>وگرچه این دو صفت نیست از حسابِ صفاتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار تشنه بر آن چشمۀ حیات فرو شد</p></div>
<div class="m2"><p>به اختیار و یکی آرزو نکرد نجاتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه جهد کردم و هم از قضایِ عشق نجستم</p></div>
<div class="m2"><p>چه اختیار کسی را که رفت صبر و ثباتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی که دامنِ طاعات می کشید ز شبنم</p></div>
<div class="m2"><p>بیا ببین که ز سر درگذشت آبِ فراتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیالِ رویِ تو جانا بتِ من است بتِ من</p></div>
<div class="m2"><p>من و خیالِ تو و بت پرست و عزّی ولاتش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بت پرستیِ من در رسومِ شرع چه نقصان</p></div>
<div class="m2"><p>همین قدر که دریغا ثوابِ صوم و صلاتش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر تو خط بنمایی به خونِ خلق بدارند</p></div>
<div class="m2"><p>معبّدان همه تعظیم هم چنان که بر آتش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا بده ز لبت کامِ من به رغمِ عدو را</p></div>
<div class="m2"><p>به مستحق برسان حقّ که واجب است زکاتش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به کامِ خویش ببینم رقیبِ سوختنی را</p></div>
<div class="m2"><p>به دادخواه گریبان گرفته در عرصاتش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز دستِ او نفسی برنیامده ست ز من خوش</p></div>
<div class="m2"><p>که صد هزار بلا بر وجودِ ناقصِ ذاتش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بهشت رویا امشب اجازتم ده و فردا</p></div>
<div class="m2"><p>بسوز گو که وجودم دریغ نیست در آتش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به نقدِ وقت زمانی به حالِ ما نگران شو</p></div>
<div class="m2"><p>هزار یادِ نزاری چه سود بعدِ وفاتش</p></div></div>