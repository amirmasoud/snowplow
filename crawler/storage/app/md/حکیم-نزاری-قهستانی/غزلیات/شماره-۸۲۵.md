---
title: >-
    شمارهٔ ۸۲۵
---
# شمارهٔ ۸۲۵

<div class="b" id="bn1"><div class="m1"><p>شبانِ تا به سحر گردِ شهر می گردم</p></div>
<div class="m2"><p>کسی نکرد ازین بی خودی که من کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به اختیار گدا دل به پادشاه دهد</p></div>
<div class="m2"><p>به دستِ خود چه بلا با سر خود آوردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم نمی خورد آن کس که در محبّتِ او</p></div>
<div class="m2"><p>هزار شربتِ خونابۀ جگر خوردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دشمنم چه توقّع که بی گناه از دوست</p></div>
<div class="m2"><p>نمی کنم گله امّا بسی بیازردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خجل نمی شوم از طعنۀ فسرده دلان</p></div>
<div class="m2"><p>به زمهریر نمی باشدی عجب سردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلی چو آهن و چشمی چو سنگ می نگرید</p></div>
<div class="m2"><p>که زخمِ تیرِ ملامت نمی کند دردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بازداشتن از کویِ دوست رغمِ مرا</p></div>
<div class="m2"><p>رقیب دعویِ دفعی دگر کند هر دم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر وقوف ندارد که من به خلوتِ او</p></div>
<div class="m2"><p>چنان روم که نبیند مگر صبا گردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مخالفان به جفا گر مبالغت بکنند</p></div>
<div class="m2"><p>اگر وفا نکنم عهدِ دوست نامردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نمی توانم از او بازگشت اگر کارم</p></div>
<div class="m2"><p>به جان رسد که به خونِ دلش بپروردم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به اعتقاد نزاری که بر نگردم ازو</p></div>
<div class="m2"><p>وگر رضا دهد از اعتقاد برگردم</p></div></div>