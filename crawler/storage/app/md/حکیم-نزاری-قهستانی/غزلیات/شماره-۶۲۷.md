---
title: >-
    شمارهٔ ۶۲۷
---
# شمارهٔ ۶۲۷

<div class="b" id="bn1"><div class="m1"><p>گر مرا عقل کشد پای و سر اندر زنجیر</p></div>
<div class="m2"><p>نیست از کوی توام یک نفس ای دوست گزیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میل هر چیز به اصل و مرا میل به تو</p></div>
<div class="m2"><p>هم‌چنان است که آتش متعلق به اثیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه جز قامت تو گر همه اوج فلک است</p></div>
<div class="m2"><p>راستی در نظر همّت من هست قصیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این توان گفت که خورشید غلام رخ توست</p></div>
<div class="m2"><p>در نکویی به تو چیزی نتوان کرد نظیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر به دامن کشی‌اش تربیتی فرمایی</p></div>
<div class="m2"><p>هر شب از چرخ ببوسد قدمت بدر منیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سر زلف به عطّار صبا ده تاری</p></div>
<div class="m2"><p>تا دماغ من بی‌مغز کند پر ز عبیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناف آهوی خَتن خشک شود در شکمش</p></div>
<div class="m2"><p>گر به چین برگذرد باد ز کویت شب گیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرده از بوی عرق‌چین تو جان یابد باز</p></div>
<div class="m2"><p>قرطهٔ یوسف یعقوب نکرد این تأثیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم ز جایی بود آشوب نزاری آری</p></div>
<div class="m2"><p>بلبل شیفته بر هرزه نیاید به نفیر</p></div></div>