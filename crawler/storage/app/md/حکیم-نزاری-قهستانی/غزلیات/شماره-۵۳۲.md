---
title: >-
    شمارهٔ ۵۳۲
---
# شمارهٔ ۵۳۲

<div class="b" id="bn1"><div class="m1"><p>سر به سر رازی که با من گفته‌اند</p></div>
<div class="m2"><p>با که بر گویم که مردم خفته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاکبازان خانهٔ وهم و خیال</p></div>
<div class="m2"><p>پاک کردستند و بیرون رفته‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنگه دیوانگان عشق او</p></div>
<div class="m2"><p>زان سبب در هم دگر آشفته‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رغم جانان راست کین افسردگان</p></div>
<div class="m2"><p>جام جان‌پرور به کف بگرفته‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دولت باقی به مبطل کی دهند</p></div>
<div class="m2"><p>بر مغیلان نسترن نشکفته‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با کسی چون بازگویم کاولیا</p></div>
<div class="m2"><p>راز پنهانی ز خود بنهفته‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو حلاجند بر دار قبول</p></div>
<div class="m2"><p>هرکه را در ابتدا پذرفته‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوهر اسرار دور کشف را</p></div>
<div class="m2"><p>هم به الماس نزاری سفته‌اند</p></div></div>