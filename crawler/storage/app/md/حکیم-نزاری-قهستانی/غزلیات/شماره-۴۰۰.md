---
title: >-
    شمارهٔ ۴۰۰
---
# شمارهٔ ۴۰۰

<div class="b" id="bn1"><div class="m1"><p>دیر شد تا ز ما نکردی یاد</p></div>
<div class="m2"><p>طرفه رسمی نهاده ای بنیاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الله الله نمی کنی تقصیر</p></div>
<div class="m2"><p>آفرین آفرین مبارک باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توبه توبه ز یار بی آزارم</p></div>
<div class="m2"><p>آوخ آوخ ز یار سست نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب سر می زنم بر آتش دل</p></div>
<div class="m2"><p>خاک بر سر همی کنم چون باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر شکستی و عهد بشکستی</p></div>
<div class="m2"><p>چه توان گفت چشم بد مرساد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با تو باری به باد بر دادم</p></div>
<div class="m2"><p>جان شیرین به هرزه چون فرهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا دگر عاشقان چگونه رهند</p></div>
<div class="m2"><p>از جفای تو ظالم بیداد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کژ نشین راستگوی تا کی و کی؟</p></div>
<div class="m2"><p>از تو بودیم یک نفس دل شاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند خون ریزی از خدای بترس</p></div>
<div class="m2"><p>به تو جبریل وحی نفرستاد؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاشقان را به غمزه ی جادو</p></div>
<div class="m2"><p>چشم بربسته ای زهی استاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا ترا لا اله الا الله</p></div>
<div class="m2"><p>ای نزاری چه چشم زخم افتاد</p></div></div>