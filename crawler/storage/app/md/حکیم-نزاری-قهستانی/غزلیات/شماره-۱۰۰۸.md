---
title: >-
    شمارهٔ ۱۰۰۸
---
# شمارهٔ ۱۰۰۸

<div class="b" id="bn1"><div class="m1"><p>مرا که دوست تو باشی نترسم از دشمن</p></div>
<div class="m2"><p>اگر جهان به سرآید من و تو و تو و من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من و تو شرک بود آن تویی نه من غلطم</p></div>
<div class="m2"><p>ز رویِ لطف بپوشی برین خطا دامن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکش مرا تو بمان تا من از میان بروم</p></div>
<div class="m2"><p>به خونِ من نه دیت بر تو واجب و نه ثمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به منزلی که تو باشی مرا چه راهِ نزول</p></div>
<div class="m2"><p>که در مقامِ ملایک نگنجد آهرمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محبتّی که ز تو در درون سینۀ ماست</p></div>
<div class="m2"><p>نگنجد از رهِ انصاف در زمین و زمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به عهدِ حسنِ تو شد اهلِ راز را معلوم</p></div>
<div class="m2"><p>که هر که جز تو پرستید لات بود و وَثَن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهارِ عمر چو بگذشت و روزگارِ نشاط</p></div>
<div class="m2"><p>چنان بود که به هنگامِ برگ‌ریز چمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نزاریا چه کنی چاره نیست جز تسلیم</p></div>
<div class="m2"><p>چو سیل بر بُنه افتاد و برق در خرمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا برفت به غرب ز دست دامنِ دل</p></div>
<div class="m2"><p>کنار ارمن و گُرجم چه گُرج و چه ارمن</p></div></div>