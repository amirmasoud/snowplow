---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>گر بدانی لمن الملک این است</p></div>
<div class="m2"><p>اطلبوا العلم ولو بالصّین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچه در دایرۀ گردون نیست</p></div>
<div class="m2"><p>همه در یک دلِ روشن بین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تو را آن دلِ روشن باشد</p></div>
<div class="m2"><p>مطلعِ صبحِ قیامت این است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مست باش ای دلِ دیوانه که مست</p></div>
<div class="m2"><p>فارغ از عیب گر و تحسین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که با دخترِ دوشیزۀ رز</p></div>
<div class="m2"><p>متأهل نشود عِنّین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل اگر حکم کند من نکنم</p></div>
<div class="m2"><p>ترکِ می کآن سخنِ رنگین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عکسِ رخسارۀ جانانۀ ماست</p></div>
<div class="m2"><p>هر اشارت که به حورالعین است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هندوی ِرومیِ او یعنی خال</p></div>
<div class="m2"><p>نقطه یی بر ورقِ نسرین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زرهِ مُظلِم او یعنی زلف</p></div>
<div class="m2"><p>عقل را ظلمت و ما را دین است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حلقه بر حلقۀ زلفش گویی</p></div>
<div class="m2"><p>حبسِ خَم در خَمِ غسطنطین است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هیچ زندانی از او ره بیرون</p></div>
<div class="m2"><p>نبرد گرچه رهش تعیین است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ورچه تو بودۀ در زندانش</p></div>
<div class="m2"><p>حلقه در گوشِ درش مشکین است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر دلم هیچ ملامت مکنید</p></div>
<div class="m2"><p>گر خطایی رود اندر چین است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرچه دل گیر بود خوش باشد</p></div>
<div class="m2"><p>تن وطن گاهِ دلِ مسکین است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رمز می گویم و می گویم باز</p></div>
<div class="m2"><p>آن چه در ضمنِ سخن تضمین است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غایتِ کار محبّت دارد</p></div>
<div class="m2"><p>با نزاری همه را این کین است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پسِ دیوارِ قناعت بنشست</p></div>
<div class="m2"><p>گرچه صاحب قدمی پیشین است</p></div></div>