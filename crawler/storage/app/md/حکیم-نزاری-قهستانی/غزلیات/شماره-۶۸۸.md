---
title: >-
    شمارهٔ ۶۸۸
---
# شمارهٔ ۶۸۸

<div class="b" id="bn1"><div class="m1"><p>مسکین دلِ بی چاره کز دست بشد کارش</p></div>
<div class="m2"><p>در واقعه یی بینم هر روز گرفتارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توبه نکند هرگز ور نیز کند روزی</p></div>
<div class="m2"><p>شب را ز قضا باشد بشکسته دگر بارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر شوخ که پیش آید وز غمزه کند میلی</p></div>
<div class="m2"><p>باید شدنش بر پی بی چارۀ ناچارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم نتوان پیری بربست به برنایی</p></div>
<div class="m2"><p>تا چند ز دل بازی مِن بعد نگه دارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتا بشنو پندی کز عمر بری بهره</p></div>
<div class="m2"><p>پرهیز کن از عقلی کز عشق بودعارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عشق ریاضت کش وز راحت او برخور</p></div>
<div class="m2"><p>تا در نرسد میوه شیرین نبود بارش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاصر نظران باشند آشفتۀ صورت ها</p></div>
<div class="m2"><p>خودبین نتواند شد مستغرقِ دیدارش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلف و خط و خال و لب هست آیتی از قدرت</p></div>
<div class="m2"><p>آن جا همه او بیند گر عشق دهد بارش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی شمعِ شبِ تاری روشن نشود خانه</p></div>
<div class="m2"><p>اعما چو نمی بیند چه روشن و چه تارش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی چاره نزاری را در پختنِ این سودا</p></div>
<div class="m2"><p>کرده ست بدین زاری اندیشۀ بسیارش</p></div></div>