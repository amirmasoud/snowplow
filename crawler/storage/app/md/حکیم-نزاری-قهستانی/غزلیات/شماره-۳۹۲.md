---
title: >-
    شمارهٔ ۳۹۲
---
# شمارهٔ ۳۹۲

<div class="b" id="bn1"><div class="m1"><p>قد قامت الصّلات برآمد ز بامداد</p></div>
<div class="m2"><p>برخیز ساقیا بستان از مدام داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بر حلال زاده حرام است خون رز</p></div>
<div class="m2"><p>پس آب و نان حرام بود بر حرام زاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذار تا نماز کند اقضی القضات</p></div>
<div class="m2"><p>برنه پیاله ای به کفش تا سلام داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسیار در محامد رز شعر گفته اند</p></div>
<div class="m2"><p>من نیز هم ولیک ندارم تمام یاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهقان که در عمارت رز سعی می کند</p></div>
<div class="m2"><p>عمرش مدام باشد و بختش مدام باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خنب خانه می دمد این خوش نفس نسیم</p></div>
<div class="m2"><p>یا از بهشت می وزد این خوش خرام باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شادم به قرض دادن و دادن به وجه می</p></div>
<div class="m2"><p>چون من کسی که دید که باشد به وام شاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کلّی طمع ببر ز عوانان نزاریا</p></div>
<div class="m2"><p>رو کرد کان دهر نه این ها کدام زاد</p></div></div>