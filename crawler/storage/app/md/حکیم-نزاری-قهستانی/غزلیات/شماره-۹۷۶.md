---
title: >-
    شمارهٔ ۹۷۶
---
# شمارهٔ ۹۷۶

<div class="b" id="bn1"><div class="m1"><p>سخت کاری‌ست ریاضت‌کشِ هجران بودن</p></div>
<div class="m2"><p>خونِ دل خوردن و دور از برِ جانان بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه خلیلیم و نه ایّوب پس آخر تا چند</p></div>
<div class="m2"><p>صابری کردن و بر آتشِ سوزان بودن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای که با وصلِ تو پیوند گرفتم جاوید</p></div>
<div class="m2"><p>تا ابد بایدم از هجر هراسان بودن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیش دانی تو که بی‌رویِ تو نتوان کردن</p></div>
<div class="m2"><p>زنده دانیم که بی‌بویِ تو نتوان بودن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مُقبل آن کس که ترا بیند ازیرا که توان</p></div>
<div class="m2"><p>باغِ ریحانِ ترا بنده‌ی ریحان بودن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی جمعیّتِ خاطر نبود بی تو بلی</p></div>
<div class="m2"><p>که چو زلفت نتوان جز که پریشان بودن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاقلان عیب کنندم که بپرهیز از عشق</p></div>
<div class="m2"><p>عاشقی کردن از آن به که چو ایشان بودن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر به خوبان نظری هست حکیمان را نیست</p></div>
<div class="m2"><p>عیب در قدرتِ حق دیدن و حیران بودن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عارفان راغب و حورانِ بهشتی حاضر</p></div>
<div class="m2"><p>کی توان منتظر وعده‌ی غِلمان بودن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر فدایِ قدمِ دوست عفا الله رندان</p></div>
<div class="m2"><p>کارِ زاهد صفتان است تن آسان بودن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا کی از لاف نزاری به فصاحت مفریب</p></div>
<div class="m2"><p>یک قدم به که چو بلبل همه دستان بودن</p></div></div>