---
title: >-
    شمارهٔ ۸۳۸
---
# شمارهٔ ۸۳۸

<div class="b" id="bn1"><div class="m1"><p>اگر وصال میسّر شود دگر بارم</p></div>
<div class="m2"><p>فراق بیش فریبم دهد نپندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بس که خیلِ خیالت عذاب می دارد</p></div>
<div class="m2"><p>هزار جهد کنم تا شبی به روز آرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو ژاله بر ورقِ سرخ گل شقایقِ اشک</p></div>
<div class="m2"><p>ز ابرِ دیدۀ پرخون چنان فرو بارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که موج تا به گریبان برآید از دامن</p></div>
<div class="m2"><p>گر آستین زرهِ سیلِ دیده بردارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو با خیالش هم خانه می توانم بود</p></div>
<div class="m2"><p>رقیب گو به سرِ کویِ دوست مگذارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی رود نفسی چشم هاش از چشمم</p></div>
<div class="m2"><p>که بر ستارۀ بام است چشم بیدارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فغان ز دستِ ملامت گرانِ نا هموار</p></div>
<div class="m2"><p>که سر ز خجلتِ این در کشیده هم وارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگیر مدّتِ هجران نزاریا ز حیات</p></div>
<div class="m2"><p>که من عذابِ الیم از حیات نشمارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر قضایِ فراقم امان نخواهد داد</p></div>
<div class="m2"><p>گواه باش که من زین حیات بیزارم</p></div></div>