---
title: >-
    شمارهٔ ۹۷۱
---
# شمارهٔ ۹۷۱

<div class="b" id="bn1"><div class="m1"><p>به گدایان نرسد شاه سواری کردن</p></div>
<div class="m2"><p>عاقلان را نرسد شیفته کاری کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه چو حلاجی انا الحق نتوانی گفتن</p></div>
<div class="m2"><p>نه خدایی لمن الملک نیاری کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پادشاهی و جهان داری و فرمان رانی</p></div>
<div class="m2"><p>هیچ ازین ها نتوانی چونه یاری کردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مال پوشیدن و چون مار نشستن بر گنج</p></div>
<div class="m2"><p>هم چو اعما بود و آینه داری کردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>احمقی باشد و با مزبله در زیر بغل</p></div>
<div class="m2"><p>دعوی رایحه ی مشک ِ تتاری کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر ای یار عزیز آن چه نداری مطلب</p></div>
<div class="m2"><p>چیست با نفس شریف این همه خواری کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسد و بغض و تعصب نکنند اهل صفا</p></div>
<div class="m2"><p>دوستی باید و دل داری و یاری کردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگرت چشم به بخشایش بخشاینده ست</p></div>
<div class="m2"><p>بایدت گوش به تنبیهِ نزاری کردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زور و زر هر دو وبالند بنه گردن طوع</p></div>
<div class="m2"><p>چاره ای نیست نزاری تو و زاری کردن</p></div></div>