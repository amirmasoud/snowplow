---
title: >-
    شمارهٔ ۴۲۴
---
# شمارهٔ ۴۲۴

<div class="b" id="bn1"><div class="m1"><p>غمزۀ شوخِ تو شیرین حرکاتی دارد</p></div>
<div class="m2"><p>کشتۀ هر مژه فرهاد صفاتی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خورِ توتیِ جان در چمنِ باغِ جمال</p></div>
<div class="m2"><p>چشمۀ خضرِ لبت تازه نباتی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوسه یی گر بدهی خیر بود باز مزن</p></div>
<div class="m2"><p>این فقیری ز تو امّیدِ زکاتی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خازنِ وصل بگو تا بدهد دادِ دلم</p></div>
<div class="m2"><p>که ز دیوانِ ازل بر تو براتی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی تو بنشستن و خاطر ز تو باز آوردن</p></div>
<div class="m2"><p>کارِ آن است که صبری و ثباتی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواب در دیدۀ پر خونِ دلش کی گنجد</p></div>
<div class="m2"><p>هر که بر چهره ز هر چشم فراتی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم به صبر از شبِ یلدایِ فراقت روزی</p></div>
<div class="m2"><p>دلِ محنت کشم امّیدِ نجاتی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبرِ بی طاقتم از پای درآمد نی نی</p></div>
<div class="m2"><p>شادیِ غم که قدومش برکاتی دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل و هوش و دل و دین در سرِ زلفت کردم</p></div>
<div class="m2"><p>گو بفرمای دگر گر خدماتی دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از زبانِ من اگر گوش کنی بادِ صبا</p></div>
<div class="m2"><p>با تو یک لحظه به خلوت کلماتی دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از دهانِ تو سخن گفت نزاری چه عجب</p></div>
<div class="m2"><p>که حدیثش چو دهان ِتو حیاتی دارد</p></div></div>