---
title: >-
    شمارهٔ ۱۲۰۲
---
# شمارهٔ ۱۲۰۲

<div class="b" id="bn1"><div class="m1"><p>می‌روی می‌دهدت دل که مرا بگذاری</p></div>
<div class="m2"><p>برو اکنون تو و نامردمی و نایاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر من و زاری و تنهاییِ من رحمت کن</p></div>
<div class="m2"><p>مکن ای یار به آزار دلی بی‌زاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما نکردیم گناهی که بود موجبِ خشم</p></div>
<div class="m2"><p>آری آری تو ز ما سیر شدی پنداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلِ یاران بربایی و به غارت ببری</p></div>
<div class="m2"><p>از که آموختی این دل‌بری و عیّاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا برفتی نی‌ام از یادِ تو خالی نفسی</p></div>
<div class="m2"><p>منم و یادِ تو در مستی و در هش‌یاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمت عهد به پایان نبری روزِ نخست</p></div>
<div class="m2"><p>الحق الحق چه نکو عهد و وفایی داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ضعفِ دل بر منِ سودازده شد مستولی</p></div>
<div class="m2"><p>چیست تشویشِ دماغ آفتِ شب بیداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چند رفعِ غمِ بیهوده توان کرد به می</p></div>
<div class="m2"><p>نه همه عمر توان بود درین بی‌کاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دلم بود که هرگز نشوم از تو جدا</p></div>
<div class="m2"><p>در سرم بود که با ما قدمی بسپاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه چنان باز گرفتی قدم از ما که دگر</p></div>
<div class="m2"><p>رویِ آن است که دستی به سرم بازآری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفته بودی که نزاری همه زاری دارد</p></div>
<div class="m2"><p>چون زر و زور ندارد چه کند هم زاری</p></div></div>