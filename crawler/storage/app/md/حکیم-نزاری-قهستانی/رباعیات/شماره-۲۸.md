---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>گفتم که برای ما قرینی بفرست</p></div>
<div class="m2"><p>گر گندمیی نیست جوینی بفرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حالی چو ضرورت و مهم دارم زود</p></div>
<div class="m2"><p>تا زان برسد به نقد زینی بفرست</p></div></div>