---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>هنگام صبوح چون درآیی از خواب</p></div>
<div class="m2"><p>در خواه ز باقیات دوشینه شراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر کش و دست بر زن و پای بکوب</p></div>
<div class="m2"><p>عشرت به غنیمت کن و فرصت دریاب</p></div></div>