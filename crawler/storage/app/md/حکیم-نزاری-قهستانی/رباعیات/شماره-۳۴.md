---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>آب دهن و دلت اگر تاریکست</p></div>
<div class="m2"><p>روشن کردم ولی سخن باریکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عکسی ز بهشت در جهان آوردند</p></div>
<div class="m2"><p>اینک ز نزاری بشنو شاهیکست</p></div></div>