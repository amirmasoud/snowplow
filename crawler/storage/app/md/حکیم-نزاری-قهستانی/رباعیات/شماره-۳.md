---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>گفتم به وصال وعده دادست مرا</p></div>
<div class="m2"><p>اقبال دری دگر گشادست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمم به درست و گوش بر در و او خود</p></div>
<div class="m2"><p>چون حلقه برون در نهادست مرا</p></div></div>