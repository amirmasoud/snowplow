---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>مخموری ما ز مستی مستی اوست</p></div>
<div class="m2"><p>بی مایی ما و نیستی هستی اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تعجیل نمی کنیم دانی ز چه؟ زانک</p></div>
<div class="m2"><p>در هر سیلی که می رود پستی اوست</p></div></div>