---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>خون آمد و پیراهن چشمم بگرفت</p></div>
<div class="m2"><p>خوش یافت مگر مأمن چشمت بگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که به غمزه خون ناحق کردی</p></div>
<div class="m2"><p>خونی که چنین دامن چشمت بگرفت</p></div></div>