---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>در دامن دوستان دانا زن دست</p></div>
<div class="m2"><p>گر دانایی توان ز نادانی رست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با آنک سخن ز نیستی باید گفت</p></div>
<div class="m2"><p>هرگز نتوان گفت ز هستیت هست</p></div></div>