---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>این دست اگر چه زیر سرها بودست</p></div>
<div class="m2"><p>جز زیر سر او جمله سودا بودست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند نه حد ماست وصل چو تویی</p></div>
<div class="m2"><p>از بدو ازل حواله ما بودست</p></div></div>