---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>در مذهب عاشقان قراری دگر است</p></div>
<div class="m2"><p>این باده عشق را قراری دگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر علم که در مدرسه حاصل کردم</p></div>
<div class="m2"><p>کاری دگرست و عشق کاری دگرست</p></div></div>