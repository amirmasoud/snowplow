---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>آن کو زمی شبانه مخمور بود</p></div>
<div class="m2"><p>نزدیک خرد ز زندگی دور بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل سوخته آتش غم را مرهم</p></div>
<div class="m2"><p>خونیست که نامش آب انگور بود</p></div></div>