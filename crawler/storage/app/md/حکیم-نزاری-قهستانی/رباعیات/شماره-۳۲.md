---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>در عالم عقل مدخل عشق خوشست</p></div>
<div class="m2"><p>جان دادن عاشق بدل عشق خوشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در اول عمر عشق جهلست و جنون</p></div>
<div class="m2"><p>در آخر عمر اول عشق خوشست</p></div></div>