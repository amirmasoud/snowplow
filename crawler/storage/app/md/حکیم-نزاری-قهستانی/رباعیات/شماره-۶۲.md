---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>بیداد ز عقل و خرد و هوش و دل است</p></div>
<div class="m2"><p>فریاد ز باد و آتش و آب و گل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر می خورد و عقل و دل و خوش برد</p></div>
<div class="m2"><p>گر می بیند کو بتر از من بحل است</p></div></div>