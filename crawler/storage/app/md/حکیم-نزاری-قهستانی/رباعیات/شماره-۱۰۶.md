---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>این دم همه کس را نبود تا که زند</p></div>
<div class="m2"><p>در عشق منادی تولا که زند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما هیچ به خود نه‌ایم و با او همه‌ایم</p></div>
<div class="m2"><p>آری لِمَنِ المُلک به جز ما که زند؟</p></div></div>