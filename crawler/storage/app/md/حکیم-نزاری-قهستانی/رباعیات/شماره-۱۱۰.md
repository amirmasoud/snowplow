---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>ناهید زرشک پیرهن پاره کند</p></div>
<div class="m2"><p>کز دور نظر تیز درین باره کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید که صنعتش مرصّع کاریست</p></div>
<div class="m2"><p>زیبد که نگین‌هاش سیاره کند</p></div></div>