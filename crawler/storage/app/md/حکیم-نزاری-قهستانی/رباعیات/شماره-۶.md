---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ایزد که به عز و ناز پرورد مرا</p></div>
<div class="m2"><p>همواره به لطف خویش غم خورد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زد بر سر بالای جهان خیمه من</p></div>
<div class="m2"><p>و انگشت نمای عالمی کرد مرا</p></div></div>