---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>گر عزم شرایع و سن خواهم کرد</p></div>
<div class="m2"><p>ور قصد چمانه و چمن خواهم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مملکت وجود چه بیش و چه کم</p></div>
<div class="m2"><p>از طاعت و معصیت که من خواهم کرد</p></div></div>