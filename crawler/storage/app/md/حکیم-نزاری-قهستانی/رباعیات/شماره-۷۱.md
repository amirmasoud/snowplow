---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>زان طایفه نیستم که نازم به بهشت</p></div>
<div class="m2"><p>بازش که برون کرد و که بردش به بهشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوساله سامری سخن گوی که کرد</p></div>
<div class="m2"><p>موسی ز که خشم کرد و نعبان به که هشت</p></div></div>