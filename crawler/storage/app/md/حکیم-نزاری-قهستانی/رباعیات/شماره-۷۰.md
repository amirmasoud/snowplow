---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>جل صبح خمیر خاک آدم که سرشت</p></div>
<div class="m2"><p>زان قوم نیم که ترسم از دوزخ زشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیهوده چه غم خورم که معلومم نیست</p></div>
<div class="m2"><p>تا بر سر من در ازل ایزد چه نوشت</p></div></div>