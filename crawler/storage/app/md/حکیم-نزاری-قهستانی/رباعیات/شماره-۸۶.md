---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>از دوست مرا وانگری بس باشد</p></div>
<div class="m2"><p>وز گوشه‌ی چشمش نظری بس باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از منزل کثرتم به وحدت بردن</p></div>
<div class="m2"><p>بسیار نگویم قدری بس باشد</p></div></div>