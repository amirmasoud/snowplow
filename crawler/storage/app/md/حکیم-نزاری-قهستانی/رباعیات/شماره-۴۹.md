---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>چشم تو ز بس که کرد خون از چپ و راست</p></div>
<div class="m2"><p>در خون چو دلم غرق شد آنک پیداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خون دلم نه غمزه ی شوخ تو ریخت</p></div>
<div class="m2"><p>پس دامن چشم مستت آلوده است؟</p></div></div>