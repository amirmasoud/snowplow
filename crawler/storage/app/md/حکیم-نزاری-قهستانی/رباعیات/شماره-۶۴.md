---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>خودین چه کند چون نظرش کوتاه است</p></div>
<div class="m2"><p>او حب و غریب و بر صراطش راه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر رشته مده ز دست تا گم نشوی</p></div>
<div class="m2"><p>با حبل متین باش که ره بر چاه است</p></div></div>