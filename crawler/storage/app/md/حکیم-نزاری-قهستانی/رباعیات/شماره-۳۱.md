---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>ما کافر عشقیم مسلمان دگرست</p></div>
<div class="m2"><p>ما مور ضعیفیم سلیمان دگرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ما رخ زرد و جامه پاره طلب</p></div>
<div class="m2"><p>بازارچه قصب فروشان دگرست</p></div></div>