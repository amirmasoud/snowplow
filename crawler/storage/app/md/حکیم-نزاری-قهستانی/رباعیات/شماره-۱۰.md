---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>تن بود گرو کرده جان از مبدأ</p></div>
<div class="m2"><p>جان عاقبت الامر ز تن گشت جدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن رفت به خاک باز و جان شد بر عرش</p></div>
<div class="m2"><p>یعنی همه فانی اند و باقیست خدا</p></div></div>