---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>درد سر بنده را مداوا بفرست</p></div>
<div class="m2"><p>یعنی قدری می مصفا بفرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با او سخنی است به خلوت ما را</p></div>
<div class="m2"><p>آن یارک فیروزه قبا را بفرست</p></div></div>