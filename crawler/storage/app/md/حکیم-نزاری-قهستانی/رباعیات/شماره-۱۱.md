---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>ای ذات تو مطلقا همه چون و چرا</p></div>
<div class="m2"><p>از هستی خویش یک نظر بخش مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنجا که تویی کس دگر چون باشد</p></div>
<div class="m2"><p>من خود به در آیم از دگرها تو در آ</p></div></div>