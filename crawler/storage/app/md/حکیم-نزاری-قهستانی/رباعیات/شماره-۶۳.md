---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>تشنیع که می زنند بر من چهار است</p></div>
<div class="m2"><p>بار دل اهل دل هم از نااهل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بوته ز کارگاه برون آرند</p></div>
<div class="m2"><p>روشن بنماید محک زر سهل است</p></div></div>