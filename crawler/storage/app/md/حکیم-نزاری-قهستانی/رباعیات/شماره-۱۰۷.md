---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>آن‌ها که همیشه در قیامت باشند</p></div>
<div class="m2"><p>پیوسته کشنده‌ی غرامت باشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دل همه روزه در غرابت افتند</p></div>
<div class="m2"><p>وز بد همه ساله در ملامت باشند</p></div></div>