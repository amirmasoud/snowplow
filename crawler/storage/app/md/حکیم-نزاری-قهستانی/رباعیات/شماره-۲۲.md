---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>معموری روزگار ویرانی ماست</p></div>
<div class="m2"><p>جمعیت احباب پریشانی ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه از عدم عقل که در ملک وجود</p></div>
<div class="m2"><p>چندین خلل از بی سر و سامانی ماست</p></div></div>