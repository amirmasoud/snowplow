---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>ای دل چو بدو نیک جهان بر گذرست</p></div>
<div class="m2"><p>شادی کن و غم مخور که دنیا سمرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر گشتگی من و تو از چرخ مدان</p></div>
<div class="m2"><p>کو هم ز من و تو نیز سرگشته تر است</p></div></div>