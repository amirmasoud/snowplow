---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>هرگز دل من زبخت خشنود نبود</p></div>
<div class="m2"><p>می خواست ولی زعشق پر سود نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذشت شب عمر و در و حاصل من</p></div>
<div class="m2"><p>چون اول افسانه همه بود و نبود</p></div></div>