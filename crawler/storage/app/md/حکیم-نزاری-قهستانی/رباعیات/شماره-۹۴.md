---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>کی همچو تو مریم آستینی باشد</p></div>
<div class="m2"><p>ممکن نه که در هیچ زمینی باشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر گه که تو چین از آستین باز کنی</p></div>
<div class="m2"><p>در هر چینی خراج چینی باشند</p></div></div>