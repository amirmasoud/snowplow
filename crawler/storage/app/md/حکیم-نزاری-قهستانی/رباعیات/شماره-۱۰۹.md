---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>عشقی که چو عشق ماست نقصان نکند</p></div>
<div class="m2"><p>آن به که زیاده روی پنهان نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاصیّت عشقی که مجازی نبود</p></div>
<div class="m2"><p>دردیست که جستجوی درمان نکند</p></div></div>