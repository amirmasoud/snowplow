---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>از دست رقیبان مخالف چپ و راست</p></div>
<div class="m2"><p>با یارم اگر وصل نمی آید راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا خود چه خیال می پزد افسرده</p></div>
<div class="m2"><p>دیوار حجاب نیست جانان با ماست</p></div></div>