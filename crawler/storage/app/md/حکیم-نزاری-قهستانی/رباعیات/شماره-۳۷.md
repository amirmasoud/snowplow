---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>جام جم سعد دین نزاری اینست</p></div>
<div class="m2"><p>با هم نفسی که دم بر آری اینست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای یار طمع مکن بدین جام لطیف</p></div>
<div class="m2"><p>می نوش و خموش باش باری اینست</p></div></div>