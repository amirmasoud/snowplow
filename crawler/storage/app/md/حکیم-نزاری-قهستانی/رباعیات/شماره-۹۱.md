---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>با ما نه جهنم و نه مینو باشد</p></div>
<div class="m2"><p>نی بر غلطم که هر دو نیکو باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینجا که منم کس نبود الا من</p></div>
<div class="m2"><p>آنجا دگری نیست همه او باشد</p></div></div>