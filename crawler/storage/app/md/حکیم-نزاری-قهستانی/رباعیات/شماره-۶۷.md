---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>بس خلق که بر آمده (و) رفته گریست</p></div>
<div class="m2"><p>نا رفته در این گنبد درد آماده‌ کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستغرق این جهان فانی نشوی</p></div>
<div class="m2"><p>گر بشناسی کامدن و رفتن چیست</p></div></div>