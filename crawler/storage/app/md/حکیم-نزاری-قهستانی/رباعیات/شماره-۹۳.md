---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>روزی که مرا از تو جدایی باشد</p></div>
<div class="m2"><p>از چرخ نشان بی وفایی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاریک شود همه جهان بر چشمم</p></div>
<div class="m2"><p>بی نور مرا چه روشنایی باشد</p></div></div>