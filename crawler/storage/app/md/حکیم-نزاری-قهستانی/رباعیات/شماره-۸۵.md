---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>آنرا که سر و دل ملامت باشد</p></div>
<div class="m2"><p>پیوسته کشنده‌ی غرامت باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خویش کجا رسی به مقصد، هیهات!</p></div>
<div class="m2"><p>از خود بدر آمدن قیامت باشد</p></div></div>