---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>هر کز تو برید، کی کجا پیوندد؟</p></div>
<div class="m2"><p>بیگانه به آشنا کجا پیوندد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن حاصل جانست و از تن برود</p></div>
<div class="m2"><p>شک نیست که جان به جان ما بپیوندد</p></div></div>