---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>می آینه‌ی خاطر رخشان باشد</p></div>
<div class="m2"><p>جانست وز جان چه خوش‌ترست آن باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی که کجا بود چنین جان پرور</p></div>
<div class="m2"><p>در خدمت اتسزبن طوغان باشد</p></div></div>