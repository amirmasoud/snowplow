---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>می گر چه که طبع تلخ، شورانگیز ست</p></div>
<div class="m2"><p>موتی است حقیقت که حیات آمیزست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کندست ازو فهم فرو بسته دهان</p></div>
<div class="m2"><p>با عقلش از آن همیشه دندان تیزست</p></div></div>