---
title: >-
    غزل شمارهٔ ۹۳
---
# غزل شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>درد تو صدهزار جان ارزد</p></div>
<div class="m2"><p>گرد تو نور دیدگان ارزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه غمت را بها به جان بکنم</p></div>
<div class="m2"><p>که برآنم که بیش از آن ارزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه بر من یزید عشق غمت</p></div>
<div class="m2"><p>دل و عقل و تن و روان ارزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هجر تو بر امید وصل خوشست</p></div>
<div class="m2"><p>دزد مطبخ جزای خوان ارزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ظریفان به خاصه از چو تویی</p></div>
<div class="m2"><p>قصد جانی هزار جان ارزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درد از چاکرت دریغ مدار</p></div>
<div class="m2"><p>سگ کوی تو استخوان ارزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یاد کن بنده را به یاد کنی</p></div>
<div class="m2"><p>دزد دشنام پاسبان ارزد</p></div></div>