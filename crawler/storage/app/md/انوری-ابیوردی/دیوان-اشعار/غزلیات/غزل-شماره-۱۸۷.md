---
title: >-
    غزل شمارهٔ ۱۸۷
---
# غزل شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>آخر در زهد و توبه دربستم</p></div>
<div class="m2"><p>وز بند قبول آن و این رستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر پردهٔ چنگ پرده بدریدم</p></div>
<div class="m2"><p>وز بادهٔ ناب توبه بشکستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با آن بت کم‌زن مقامر دل</p></div>
<div class="m2"><p>در کنج قمارخانه بنشستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نوبت حسن پنج کرد آن بت</p></div>
<div class="m2"><p>زنار چهارگانه بربستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از رخصت عشق رخنه‌ای جستم</p></div>
<div class="m2"><p>وز عادت مادر و پدر جستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون پای بلا به جور بگشادم</p></div>
<div class="m2"><p>بی‌باده مباد یک نفس دستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بتکده گاه موئمن گبرم</p></div>
<div class="m2"><p>در مصطبه گاه عاقل مستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دستم ز زبان خصم کوته شد</p></div>
<div class="m2"><p>کامروز چنان که گویدم هستم</p></div></div>