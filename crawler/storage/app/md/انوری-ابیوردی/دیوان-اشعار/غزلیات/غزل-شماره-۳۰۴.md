---
title: >-
    غزل شمارهٔ ۳۰۴
---
# غزل شمارهٔ ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>ای غایت عیش این جهانی</p></div>
<div class="m2"><p>ای اصل نشاط و شادمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر روح بود لطیف روحی</p></div>
<div class="m2"><p>ور جان باشد عزیز جانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی که چگونه‌ای تو بی‌ما</p></div>
<div class="m2"><p>دور از تو بتا چنان که دانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از درد تو سخت ناتوانم</p></div>
<div class="m2"><p>رنجی برگیر اگر توانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردیم به پرسشی قناعت</p></div>
<div class="m2"><p>زین بیش همی مکن گرانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر دست‌رسی بدی به بوسی</p></div>
<div class="m2"><p>کاری بودی هزارگانی</p></div></div>