---
title: >-
    غزل شمارهٔ ۳۱۷
---
# غزل شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>ای همه دلبری و زیبایی</p></div>
<div class="m2"><p>بر دلم هیچ می‌نبخشایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل مسکین فدای رنج تو باد</p></div>
<div class="m2"><p>شاید اندی که تو برآسایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای سرم را ز دیده لایق‌تر</p></div>
<div class="m2"><p>خونم از دیده چند پالایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کارم از دست چرخ پرگرهست</p></div>
<div class="m2"><p>چرخ را دستبرد ننمایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بخواهی به حکم یک فرمان</p></div>
<div class="m2"><p>گره هفت چرخ بگشایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل به تو دادم و دهم جان نیز</p></div>
<div class="m2"><p>انوری را دگر چه فرمایی</p></div></div>