---
title: >-
    غزل شمارهٔ ۶۷
---
# غزل شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>مرا تا کی فلک رنجور دارد</p></div>
<div class="m2"><p>ز روی دلبرم مهجور دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یک باده که با معشوق خوردم</p></div>
<div class="m2"><p>همه عمرم در آن مخمور دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندانم تا فلک را زین غرض چیست</p></div>
<div class="m2"><p>که بی‌جرمی مرا رنجور دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو دست خود به خون دل گشادست</p></div>
<div class="m2"><p>مگر بر خون من منشور دارد</p></div></div>