---
title: >-
    غزل شمارهٔ ۲۴۱
---
# غزل شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>هم مصلحت نبینی رویی به ما نمودن</p></div>
<div class="m2"><p>زایینهٔ دل ما زنگار غم زدودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانجا که روی کارست خورشید آسمان را</p></div>
<div class="m2"><p>با روی تو چه رویست جز بندگی نمودن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر چیست این تکبر وین را همی چه خوانند</p></div>
<div class="m2"><p>آخر دلت نگیرد زین خویشتن ستودن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دولت تو آخر ما را شبی بباید</p></div>
<div class="m2"><p>زلف کژت بسودن قول خوشت شنودن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>احسنت والله الحق داری رخان زیبا</p></div>
<div class="m2"><p>کردم ترا مسلم در جمله دل ربودن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی که خون و جانت ما را مباح باشد</p></div>
<div class="m2"><p>فرمان تراست آری نتوان برین فزودن</p></div></div>