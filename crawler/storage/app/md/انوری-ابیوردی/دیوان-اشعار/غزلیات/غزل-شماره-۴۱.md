---
title: >-
    غزل شمارهٔ ۴۱
---
# غزل شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>دل بی‌تو به صدهزار زاریست</p></div>
<div class="m2"><p>جان در کف صدهزار خواریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق تو ز اشک دیده دل را</p></div>
<div class="m2"><p>الحق ز هزار گونه یاریست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در راه تو خوارتر ز حاکم</p></div>
<div class="m2"><p>ای بخت بد این چه خاکساریست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کردیم به کام دشمن ای دوست</p></div>
<div class="m2"><p>دانم که نه این ز دوستاریست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هجران سیه‌گر توام کشت</p></div>
<div class="m2"><p>این نیز هم از سپیدکاریست</p></div></div>