---
title: >-
    غزل شمارهٔ ۲۲۷
---
# غزل شمارهٔ ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>بی‌تو جانا زندگانی می‌کنم</p></div>
<div class="m2"><p>وز تو این معنی نهانی می‌کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرم باد از کار خویشم تا چرا</p></div>
<div class="m2"><p>بی‌تو چندین زندگانی می‌کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو نه و من در جهان زندگان</p></div>
<div class="m2"><p>راستی باید گرانی می‌کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبر گویم می‌کنم لیکن چه صبر</p></div>
<div class="m2"><p>حیلتی چونین که دانی می‌کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از غمم شادی و تا بشنیده‌ام</p></div>
<div class="m2"><p>از غم خود شادمانی می‌کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در همه راه تمنا کردمی</p></div>
<div class="m2"><p>بر سر ره دیده‌بانی می‌کنم</p></div></div>