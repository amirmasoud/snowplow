---
title: >-
    غزل شمارهٔ ۵۱
---
# غزل شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>باز کی گیرم اندر آغوشت</p></div>
<div class="m2"><p>کی بیارم به دست چون دوشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز آیا به خواب خواهم دید</p></div>
<div class="m2"><p>یک شبی دیگر اندر آغوشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بدیدم به زیر حلقهٔ زلف</p></div>
<div class="m2"><p>حلقهٔ گوش بر بناگوشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشت یکبارگی دل ریشم</p></div>
<div class="m2"><p>حلقهٔ گوش حلقه در گوشت</p></div></div>