---
title: >-
    غزل شمارهٔ ۱۲۹
---
# غزل شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>حسن تو عشق من افزون می‌کند</p></div>
<div class="m2"><p>عشق او حالم دگرگون می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمزه‌ای از چشم خونخوارش مرا</p></div>
<div class="m2"><p>زهره کرد آب و جگر خون می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خندهٔ آن لعل عیسی دم مرا</p></div>
<div class="m2"><p>هر دمی از گریه قارون می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر تنم یک موی ازو آزاد نیست</p></div>
<div class="m2"><p>من ندانم تا چه افسون می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن او در نرد خوبی داو خواست</p></div>
<div class="m2"><p>خطش اکنون داو افزون می‌کند</p></div></div>