---
title: >-
    غزل شمارهٔ ۳۰۸
---
# غزل شمارهٔ ۳۰۸

<div class="b" id="bn1"><div class="m1"><p>سر آن داری کامروز مرا شاد کنی</p></div>
<div class="m2"><p>دل مسکین مرا از غمت آزاد کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خانهٔ صبر دلم کز غم تو گشت خراب</p></div>
<div class="m2"><p>زان لب لعل شکربار خود آباد کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک پای توام و زاتش سودای مرا</p></div>
<div class="m2"><p>برزنی آب و همه انده بر باد کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخرت شرم نیاید که همه عمر مرا</p></div>
<div class="m2"><p>وعدهٔ داد دهی و همه بیداد کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد فراموش مرا راه سلامت ز غمت</p></div>
<div class="m2"><p>چو شود گر به سلامی دل من شاد کنی</p></div></div>