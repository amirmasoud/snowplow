---
title: >-
    غزل شمارهٔ ۴۷
---
# غزل شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>در همه مملکت مرا جانیست</p></div>
<div class="m2"><p>هر زمان پای‌بند جانانیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کنارم به جای دمسازی</p></div>
<div class="m2"><p>تا سحرگه ز دیده طوفانیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کجا می‌خورد مرا غم عشق</p></div>
<div class="m2"><p>در همه خانه‌ام یکی تا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک دم از درد عشق ناساید</p></div>
<div class="m2"><p>دادم انصاف رنج‌کش جانیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم او را که صبر کن که به صبر</p></div>
<div class="m2"><p>هر غمی را که هست پایانیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این همه هست کاشکی باری</p></div>
<div class="m2"><p>کار او را سری و سامانیست</p></div></div>