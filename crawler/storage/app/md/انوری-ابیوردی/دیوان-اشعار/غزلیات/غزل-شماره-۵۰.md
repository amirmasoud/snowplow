---
title: >-
    غزل شمارهٔ ۵۰
---
# غزل شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>یار با من چون سر یاری نداشت</p></div>
<div class="m2"><p>ذره‌ای در دل وفاداری نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقان بسیار دیدم در جهان</p></div>
<div class="m2"><p>هیچ‌کس کس را بدین خواری نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان به ترک دل بگفت از بیم هجر</p></div>
<div class="m2"><p>طاقت چندین جگرخواری نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا پدید آمد شراب عشق تو</p></div>
<div class="m2"><p>هیچ عاشق برگ هشیاری نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ز بی‌صبری همی زد لاف عشق</p></div>
<div class="m2"><p>گفت دارم صبر پنداری نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بار وصلش در جهان نگشاد کس</p></div>
<div class="m2"><p>کاندرو در هجر سرباری نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد چشم من فزون شد بهر آنک</p></div>
<div class="m2"><p>توتیای از صبر پنداری نداشت</p></div></div>