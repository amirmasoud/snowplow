---
title: >-
    غزل شمارهٔ ۱۰۴
---
# غزل شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>حسن تو بر ماه لشکر می‌کشد</p></div>
<div class="m2"><p>عشق تو بر عقل خنجر می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدمتش بر دست می‌گیرد فلک</p></div>
<div class="m2"><p>هر کرا دست غمت برمی‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست عشقت هرکرا دامن گرفت</p></div>
<div class="m2"><p>دامن از هر دو جهان درمی‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بر تو گر غمیم آرد رسول</p></div>
<div class="m2"><p>جان به صد شادیش در بر می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از همه بیش و کمی در مهر و حسن</p></div>
<div class="m2"><p>دل به هر معیار کت برمی‌کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه می‌گوید که از زلفت به تنگ</p></div>
<div class="m2"><p>باد شب تا روز عنبر می‌کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من که باری سر به رشوت می‌دهم</p></div>
<div class="m2"><p>زلف تو با این همه سر می‌کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>انوری بر پایهٔ تو کی رسد</p></div>
<div class="m2"><p>تا قبولت پایه بر تر می‌کشد</p></div></div>