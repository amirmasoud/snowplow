---
title: >-
    غزل شمارهٔ ۱۱
---
# غزل شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>خه‌خه به نام ایزد آن روی کیست یارب</p></div>
<div class="m2"><p>آن سحر چشم و آن رخ آن زلف و خال و آن لب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در وصف حسن آن لب ناهید چنگ مطرب</p></div>
<div class="m2"><p>بر چرخ حسن آن رخ خورشید برج کوکب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مسرور عیش او را این عیش عادتی غم</p></div>
<div class="m2"><p>بیمار هجر او را این مرگ صورتی تب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقشی نگاشت خطش از مشک سوده بر گل</p></div>
<div class="m2"><p>دامن فکند زلفش بر روز روشن از شب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دامیست چین زلفش عقل اندرو معلق</p></div>
<div class="m2"><p>جزعیست چشم شوخش سحر اندرو مرکب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه مشک می‌فشاند بر مه ز گرد موکب</p></div>
<div class="m2"><p>گه ماه می‌نگارد در ره ز نعل مرکب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پیش نور رویش گردون به دست حسرت</p></div>
<div class="m2"><p>بربست روی خود را بشکست نیش عقرب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بردارد ار بخواهد زلف و رخش به یک ره</p></div>
<div class="m2"><p>ترتیب کفر وایمان آیین کیش و مذهب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در من یزید وصلش جانی جوی نیرزد</p></div>
<div class="m2"><p>ای انوری چه لافی چندین ز قلب و قالب</p></div></div>