---
title: >-
    غزل شمارهٔ ۲۱۲
---
# غزل شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>دل رفت و این بتر بر دلبر نمی‌رسم</p></div>
<div class="m2"><p>کان می‌کنم ولیک به گوهر نمی‌رسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درویش حال کرد غم عشق او مرا</p></div>
<div class="m2"><p>زان در وصال یار توانگر نمی‌رسم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغ وصال را به همه حال‌ها درست</p></div>
<div class="m2"><p>گمره شدم ز هجر بدان در نمی‌رسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارد وصال یار یکی پایهٔ بلند</p></div>
<div class="m2"><p>آری مرا چه جرم بود بر نمی‌رسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هجران یار هست مرا گر وصال نیست</p></div>
<div class="m2"><p>با او بساختم چو به دیگر نمی‌رسم</p></div></div>