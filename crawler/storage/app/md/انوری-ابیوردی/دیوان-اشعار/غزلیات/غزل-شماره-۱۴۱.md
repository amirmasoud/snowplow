---
title: >-
    غزل شمارهٔ ۱۴۱
---
# غزل شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>آنچه بر من در غم آن نامسلمان می‌رود</p></div>
<div class="m2"><p>بالله ار با موئمن اندر کافرستان می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل به دلال غمش دادم به دستم باز داد</p></div>
<div class="m2"><p>گفت نقدی ده که این با خاک یکسان می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچنان بی‌معنیی کارم به جان آورد و رفت</p></div>
<div class="m2"><p>این سخن در یار بی‌معنی نه در جان می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم از بی‌آبی چشم زمانه‌ست این مگر</p></div>
<div class="m2"><p>پیشت آب من کنون تیره به دستان می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل کدامی سگ بود جایی که صد جان عزیز</p></div>
<div class="m2"><p>در رکاب کمترین شاگرد سگبان می‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در تماشاگاه زلفش از پی ترتیب حسن</p></div>
<div class="m2"><p>باد با فرمان روایی هم به فرمان می‌رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باد باری زلف او را چون به فرمان شد چنین</p></div>
<div class="m2"><p>دیو زلفش گرنه با مهر سلیمان می‌رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عید بودست آنچه در کشمیر می‌رفتست ازو</p></div>
<div class="m2"><p>کار این دارد که اکنون در خراسان می‌رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در میان آتش دل گرچه هر شب تا به روز</p></div>
<div class="m2"><p>جانم از یاد لبش در آب حیوان می‌رود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر زمان گوید چه خارج می‌رود اکنون ز من</p></div>
<div class="m2"><p>دم نمی‌یارم زدن ورنه فراوان می‌رود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آب لطف از جانب او می‌رود با انوری</p></div>
<div class="m2"><p>بلکه از انصاف و عدل و داد سلطان می‌رود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خسرو آفاق ذوالقرنین ثانی سنجر آنک</p></div>
<div class="m2"><p>قیصرش در تحت فرمان همچو خاقان می‌رود</p></div></div>