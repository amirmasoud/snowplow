---
title: >-
    غزل شمارهٔ ۱۰۹
---
# غزل شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>جانا دلم از غمت به جان آمد</p></div>
<div class="m2"><p>جانم ز تو بر سر جهان آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دولت این جهان دلی بودم</p></div>
<div class="m2"><p>آن نیز به دولتت گران آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آری همه دولتی گران آید</p></div>
<div class="m2"><p>چون پای غم تو در میان آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در راه تو کارها بنامیزد</p></div>
<div class="m2"><p>چونان که بخواستم چنان آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حجرهٔ دل خیال تو بنشست</p></div>
<div class="m2"><p>چون عشق تو در میان جان آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان بر در دل به درد می‌گوید</p></div>
<div class="m2"><p>دستوری هست در توان آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دست زمانه داستان گشتم</p></div>
<div class="m2"><p>چون پای دلم در آستان آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم که تو از زمانه به باشی</p></div>
<div class="m2"><p>خود هر دو نواله استخوان آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکباره سپر بر انوری مفکن</p></div>
<div class="m2"><p>با او همه وقت بر توان آمد</p></div></div>