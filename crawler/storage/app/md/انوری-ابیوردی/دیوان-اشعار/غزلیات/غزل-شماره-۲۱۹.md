---
title: >-
    غزل شمارهٔ ۲۱۹
---
# غزل شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>تو دانی که من جز تو کس را ندانم</p></div>
<div class="m2"><p>تویی یار پیدا و یار نهانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا جای صبر است و دانم که دانی</p></div>
<div class="m2"><p>ترا جای شکرست و دانی که دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برانی که خونم به خواری بریزی</p></div>
<div class="m2"><p>برای رضای تو من بر همانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا گویی که از من به جز غم نبینی</p></div>
<div class="m2"><p>همین است اگر راست خواهی گمانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر از وصل تو شاد گردم و گرنه</p></div>
<div class="m2"><p>به هرسان که باشد ز غم درنمانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان من و تو هم اندر هم آمد</p></div>
<div class="m2"><p>چو درجست و جوی تو جان بر میانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجب نیست کز انوری بر کرانی</p></div>
<div class="m2"><p>مرا بین که اویم و زو بر کرانم</p></div></div>