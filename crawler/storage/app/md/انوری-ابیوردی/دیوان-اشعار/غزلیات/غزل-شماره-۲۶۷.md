---
title: >-
    غزل شمارهٔ ۲۶۷
---
# غزل شمارهٔ ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>زردرویم ز چرخ دندان‌خای</p></div>
<div class="m2"><p>تیره‌رایم ز عمر محنت‌زای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه امیدی که سرخ دارم روی</p></div>
<div class="m2"><p>نه نوبدی که تازه دارم رای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با که گویم که حق من بشناس</p></div>
<div class="m2"><p>باکه گویم که بند من بگشای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از قیاسی که تکیه‌گاه منست</p></div>
<div class="m2"><p>باز جستم زمانه را سر و پای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روشنم شد که در بسیط زمین</p></div>
<div class="m2"><p>نیک عهدی نیافرید خدای</p></div></div>