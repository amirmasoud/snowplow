---
title: >-
    غزل شمارهٔ ۱۵۰
---
# غزل شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>صبر با عشق بس نمی‌آید</p></div>
<div class="m2"><p>یار فریادرس نمی‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ز کاری که پیش می‌نرود</p></div>
<div class="m2"><p>قدمی باز پس نمی‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق با عافیت نیامیزد</p></div>
<div class="m2"><p>نفسی هم‌نفس نمی‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌غمی خوش ولایتست ولیک</p></div>
<div class="m2"><p>زیر فرمان کس نمی‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داد در کاروان خرسندیست</p></div>
<div class="m2"><p>زان خروش جرس نمی‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه کنم عسکری که نی‌شکرش</p></div>
<div class="m2"><p>بی‌خروش مگس نمی‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویی از جانت می‌برآید پای</p></div>
<div class="m2"><p>چه حدیثست بس نمی‌آید</p></div></div>