---
title: >-
    غزل شمارهٔ ۱۷۲
---
# غزل شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>چارهٔ عشق تو نداند کس</p></div>
<div class="m2"><p>نامهٔ وصل تو نخواند کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقش هجران تو که مالد باز</p></div>
<div class="m2"><p>تو توانی اگر تواند کس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در رکابت فلک فرو ماند</p></div>
<div class="m2"><p>هم‌عنانی چگونه راند کس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به غمی چون دل بنستانی</p></div>
<div class="m2"><p>از تو انصاف چون ستاند کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تو هرچم بتر به روی رسید</p></div>
<div class="m2"><p>خود به روی کس این رساند کس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم برین دل اگر بخواهی ماند</p></div>
<div class="m2"><p>تا نه بس در جهان نماند کس</p></div></div>