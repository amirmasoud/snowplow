---
title: >-
    غزل شمارهٔ ۶۸
---
# غزل شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>با قد تو قد سرو خم دارد</p></div>
<div class="m2"><p>چون قد تو باغ، سرو کم دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصلت ز همه وجود به لیکن</p></div>
<div class="m2"><p>تا هجر تو روی در عدم دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شادم به تو و یقین همی دانم</p></div>
<div class="m2"><p>کین یک شادی هزار غم دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کار تو نیست عقل بر کاری</p></div>
<div class="m2"><p>کار آن دارد که یک درم دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دایم چو قلم به تارکم پویان</p></div>
<div class="m2"><p>زان قامت و قد که چون قلم دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در راه تو انوری تو خود دانی</p></div>
<div class="m2"><p>عمریست که تا ز سر قدم دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر سرزنش همه جهان خواهی</p></div>
<div class="m2"><p>آن نیز به دولت تو هم دارد</p></div></div>