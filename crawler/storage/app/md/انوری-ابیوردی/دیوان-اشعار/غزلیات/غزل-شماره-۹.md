---
title: >-
    غزل شمارهٔ ۹
---
# غزل شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>ای غارت عشق تو جهان‌ها</p></div>
<div class="m2"><p>بر باد غم تو خان و مان‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد بر سر کوی لاف عشقت</p></div>
<div class="m2"><p>سرها همه در سر زبان‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پیش جنیبت جمالت</p></div>
<div class="m2"><p>از جسم پیاده گشته جان‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کوکبهٔ رخ چو ماهت</p></div>
<div class="m2"><p>صد نعل فکنده آسمان‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظارگیان روی خوبت</p></div>
<div class="m2"><p>چون در نگرند از کران‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در روی تو روی خویش بینند</p></div>
<div class="m2"><p>زینجاست تفاوت نشان‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویم که ز عشوه‌های عشقت</p></div>
<div class="m2"><p>هستیم ز عمر بر زبان‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویی که ترا از آن زیان بود</p></div>
<div class="m2"><p>الحق هستی تو خود از آن‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا کی گویی چو انوری مرغ</p></div>
<div class="m2"><p>دیگر نپرد از آشیان‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داند همه‌کس که آن چه طعنه‌ست</p></div>
<div class="m2"><p>دندانست بتا در این دهان‌ها</p></div></div>