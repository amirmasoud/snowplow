---
title: >-
    غزل شمارهٔ ۲۱۴
---
# غزل شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>کار جهان نگر که جفای که می‌کشم</p></div>
<div class="m2"><p>دل را به پیش عهد وفای که می‌کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این نعره‌های گرم ز عشق که می‌زنم</p></div>
<div class="m2"><p>این آه‌های سرد برای که می‌کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر رضای دوست ز دشمن جفا کشند</p></div>
<div class="m2"><p>چون دوست نیست بهر رضای که می‌کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل در هوای او ز جهانی کرانه کرد</p></div>
<div class="m2"><p>آخر نگویدم که هوای که می‌کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای روزگار عافیت آخر کجا شدی</p></div>
<div class="m2"><p>باری بیا ببین که برای که می‌کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهریست انوری و شب و روز این غزل</p></div>
<div class="m2"><p>کار جهان نگر که جفای که می‌کشم</p></div></div>