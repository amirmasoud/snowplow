---
title: >-
    غزل شمارهٔ ۷۵
---
# غزل شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>آرزوی روی تو جانم ببرد</p></div>
<div class="m2"><p>کافریهای تو ایمانم ببرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جهان ایمان و جانی داشتم</p></div>
<div class="m2"><p>عشق تو هم این و هم آنم ببرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمزهات از بیخ وز بارم بکند</p></div>
<div class="m2"><p>عشوهات از خان و از مانم ببرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شحنهٔ عشقت دلم را چون بخواند</p></div>
<div class="m2"><p>از حساب جعل خود جانم ببرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل را گفتم که پنهان شو برو</p></div>
<div class="m2"><p>کین همه پیدا و پنهانم ببرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت اگر این بار دست از من بداشت</p></div>
<div class="m2"><p>باز باز آمد به دستانم ببرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انوری چند از شکایتهای عشق</p></div>
<div class="m2"><p>کو فلان بگذاشت و بهمانم ببرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این همه بگذار و می‌گوی انوری</p></div>
<div class="m2"><p>آرزوی روی تو جانم ببرد</p></div></div>