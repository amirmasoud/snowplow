---
title: >-
    غزل شمارهٔ ۳۸
---
# غزل شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>عشق تو دل را نکو پیرایه‌ایست</p></div>
<div class="m2"><p>دیده را دیدار تو سرمایه‌ایست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیر مژگان ترا خون ریختن</p></div>
<div class="m2"><p>در طریق عشق کمتر پایه‌ایست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از وفا فرزند اندوه ترا</p></div>
<div class="m2"><p>دل ز مادر مهربانتر دایه‌ایست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنده گشت از بهر تو دل دیده را</p></div>
<div class="m2"><p>گرچه دل را دیده بد همسایه‌ایست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان مرا وصلت به دست هجر داد</p></div>
<div class="m2"><p>کز پی هر آفتابی سایه‌ایست</p></div></div>