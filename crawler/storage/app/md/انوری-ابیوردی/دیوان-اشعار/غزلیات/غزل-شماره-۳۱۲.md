---
title: >-
    غزل شمارهٔ ۳۱۲
---
# غزل شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>ناز از اندازه بیرون می‌کنی</p></div>
<div class="m2"><p>وز جگر خوردن دلم خون می‌کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچه من از سرکشی کم می‌کنم</p></div>
<div class="m2"><p>در کله‌داری تو افزون می‌کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه رخسارت نه بس در میغ هجر</p></div>
<div class="m2"><p>نیز با این جور گردون می‌کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون به یک نوع از جفا تن دردهیم</p></div>
<div class="m2"><p>تازه صد نوع دگرگون می‌کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اینت دستی کاندرین بازی تراست</p></div>
<div class="m2"><p>نیک خار از پای بیرون می‌کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر زمان گویی که من نیک آورم</p></div>
<div class="m2"><p>این سخن باری بگو چون می‌کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حساب انوری هرگز نبود</p></div>
<div class="m2"><p>کز تو این آید که اکنون می‌کنی</p></div></div>