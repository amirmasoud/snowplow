---
title: >-
    غزل شمارهٔ ۱۴۰
---
# غزل شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>ای دلبر عیار ترا یار توان بود</p></div>
<div class="m2"><p>غمهای ترا با تو خریدار توان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با داغ تو تن در ستم چرخ توان داد</p></div>
<div class="m2"><p>با یاد تو اندر دهن مار توان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر بوی گل وصل تو سالی نه که عمری</p></div>
<div class="m2"><p>از دست گل وصل تو پر خار توان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آرزوی شکر و بادام تو صد سال</p></div>
<div class="m2"><p>بر بستر تیمار تو بیمار توان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد شب به تمنای وصال تو چو نرگس</p></div>
<div class="m2"><p>بی‌نرگس بیمار تو بیدار توان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنجا که مراد تو به جان کرد اشارت</p></div>
<div class="m2"><p>با خصم تو در کشتن خود یار توان بود</p></div></div>