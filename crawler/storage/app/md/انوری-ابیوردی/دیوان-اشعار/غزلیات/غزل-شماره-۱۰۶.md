---
title: >-
    غزل شمارهٔ ۱۰۶
---
# غزل شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>زلفت چو به دلبری درآمد</p></div>
<div class="m2"><p>بس کس که ز جان و دل برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم رایت خوشدلی نگون شد</p></div>
<div class="m2"><p>هم دولت بی‌غمی سر آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل گم نشود در آنچنان زلف</p></div>
<div class="m2"><p>کز فتنه جهان به هم برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاندیشه به حلقه‌ایش درشد</p></div>
<div class="m2"><p>کم گشت و چو حلقه بر در آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم سیه سپید کارت</p></div>
<div class="m2"><p>در کار چنان سیه‌گر آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کز کبر به دست التفاتش</p></div>
<div class="m2"><p>پهلوی زمانه لاغر آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان حذر من از غم تو</p></div>
<div class="m2"><p>آوخ که غم تو بهتر آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در موکب ترکتاز غمزه‌ت</p></div>
<div class="m2"><p>بشکست در دل و درآمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌رنگ رخ تو چون برد حسن</p></div>
<div class="m2"><p>ماه آمد و در برابر آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر خط که خریطه‌دار او داشت</p></div>
<div class="m2"><p>در حسن همه مزور آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حسن تو چو شعر انوری نیز</p></div>
<div class="m2"><p>گویی به مزاج دیگر آمد</p></div></div>