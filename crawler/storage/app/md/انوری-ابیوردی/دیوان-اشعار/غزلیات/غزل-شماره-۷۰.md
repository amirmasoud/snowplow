---
title: >-
    غزل شمارهٔ ۷۰
---
# غزل شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>یار با هرکسی سری دارد</p></div>
<div class="m2"><p>سر به پیوند من فرو نارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چنین شرط دوستی باشد</p></div>
<div class="m2"><p>که بخواند به لطف و بگذارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل و جانم به لابه بستاند</p></div>
<div class="m2"><p>پس به دست فراق بسپارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناز بسیار می‌کند لیکن</p></div>
<div class="m2"><p>نیک بنگر که جای آن دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان همی خواهد و کرا نکند</p></div>
<div class="m2"><p>که به جانی ز من بیازارد</p></div></div>