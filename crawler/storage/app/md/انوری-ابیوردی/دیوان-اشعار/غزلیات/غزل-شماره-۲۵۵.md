---
title: >-
    غزل شمارهٔ ۲۵۵
---
# غزل شمارهٔ ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>جرم رهی دوستی روی تو</p></div>
<div class="m2"><p>آفت سودای دلش موی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل نفس عشق تو تنها زند</p></div>
<div class="m2"><p>در همه دلها هوس روی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناوک غمزه مزن آندان که او</p></div>
<div class="m2"><p>کشتهٔ هر غمزدهٔ خوی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست بسی یوسف یعقوب رنگ</p></div>
<div class="m2"><p>پیرهنی کوست درو بوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از در خود عاشق خود را مران</p></div>
<div class="m2"><p>رحم کن انگار سگ کوی تو</p></div></div>