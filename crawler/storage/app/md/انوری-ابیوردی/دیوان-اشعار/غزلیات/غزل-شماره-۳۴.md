---
title: >-
    غزل شمارهٔ ۳۴
---
# غزل شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>عشق تو قضای آسمانست</p></div>
<div class="m2"><p>وصل تو بقای جاودانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسیب غم تو در زمانه</p></div>
<div class="m2"><p>دور از تو بلای ناگهانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دستم نرسد همی به شادی</p></div>
<div class="m2"><p>تا پای غم تو در میانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در زاویهای چین زلفت</p></div>
<div class="m2"><p>صد خردهٔ عشق در میانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این قاعده گر چنین بماند</p></div>
<div class="m2"><p>بنیاد خرابی جهانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با حسن تو در نوالهٔ چرخ</p></div>
<div class="m2"><p>رخسارهٔ ماه استخوانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وز عافیتی چنین مروح</p></div>
<div class="m2"><p>در عشق تو عمر بس گرانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با آنکه نشان نمی‌توان داد</p></div>
<div class="m2"><p>کز وصل تو در جهان نشانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل در غم انتظار خون شد</p></div>
<div class="m2"><p>بیچاره هنوز در گمانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم که به تحفه پیش وعده‌اش</p></div>
<div class="m2"><p>جان می‌نهم ار سخن در آنست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل گفت که بر در قبولش</p></div>
<div class="m2"><p>هرچه آن نرود به دست جانست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بازار سپید کاری تو</p></div>
<div class="m2"><p>اکنون به روایی آنچنانست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کانجا سر سبز بی‌زر سرخ</p></div>
<div class="m2"><p>چون سیم سیاه ناروانست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زر بایدت انوری وگر نیست</p></div>
<div class="m2"><p>غم خور که همیشه رایگانست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی‌مایه همی طلب کنی سود</p></div>
<div class="m2"><p>زان گاهی سود و گه زیانست</p></div></div>