---
title: >-
    غزل شمارهٔ ۱۱۹
---
# غزل شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>عشق تو ز دل برید نتواند</p></div>
<div class="m2"><p>وصل تو به جان خرید نتواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی تو اگر نه آفتاب آید</p></div>
<div class="m2"><p>چونست که درست دید نتواند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طرفه شکریست آن لبان تو</p></div>
<div class="m2"><p>هر طوطی ازو مزید نتواند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرجا که تو دام زلف گستردی</p></div>
<div class="m2"><p>یک پشه ازو پرید نتواند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهد که کند مر انوریت را</p></div>
<div class="m2"><p>تیغ غم تو شهید نتواند</p></div></div>