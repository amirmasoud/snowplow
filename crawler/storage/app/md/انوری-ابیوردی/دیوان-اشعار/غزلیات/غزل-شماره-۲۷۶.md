---
title: >-
    غزل شمارهٔ ۲۷۶
---
# غزل شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>ای دوست به کام دشمنم کردی</p></div>
<div class="m2"><p>بردی دل و زان پسم جگر خوردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دست ز عشق بر سر آوردم</p></div>
<div class="m2"><p>از دست شدی و سر برآوردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن دوستیی چنان بدان گرمی</p></div>
<div class="m2"><p>ای دوست چنین شود بدین سردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم که چو روزگار برگردد</p></div>
<div class="m2"><p>تو نیز چو روزگار برگردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی نکنم چنین معاذالله</p></div>
<div class="m2"><p>دیدی که به عاقبت چنان کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خورد تو نیست انوری آری</p></div>
<div class="m2"><p>لیکن به ضرورتش تو در خوردی</p></div></div>