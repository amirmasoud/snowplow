---
title: >-
    غزل شمارهٔ ۲۴۴
---
# غزل شمارهٔ ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>ای بت یغما دلم یغما مکن</p></div>
<div class="m2"><p>شادمان جان مرا شیدا مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی خوب از چشم من پیدا مدار</p></div>
<div class="m2"><p>راز پنهان مرا پیدا مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک زیبایی مسلم شد ترا</p></div>
<div class="m2"><p>شکر آنرا باز نازیبا مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سر کبر و جفا هر ساعتی</p></div>
<div class="m2"><p>با چو من سوداییی صفرا مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدهم ار امروز جان خواهی زمن</p></div>
<div class="m2"><p>چون با جام می فردا مکن</p></div></div>