---
title: >-
    غزل شمارهٔ ۵۳
---
# غزل شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>یار ما را به هیچ برنگرفت</p></div>
<div class="m2"><p>وانچه گفتیم هیچ درنگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پردهٔ ما دریده گشت و هنوز</p></div>
<div class="m2"><p>پرده از روی کار برنگرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درنیامد ز راه دیده به دل</p></div>
<div class="m2"><p>تا دل از راه سینه برنگرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدمت ما به جز هبا نشمرد</p></div>
<div class="m2"><p>صحبت ما به جز هدر نگرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز وفا سیرت دلم نگذاشت</p></div>
<div class="m2"><p>جز جفا عادتی دگر نگرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ روزی مرا به سر نامد</p></div>
<div class="m2"><p>که دلم عشق او از سر نگرفت</p></div></div>