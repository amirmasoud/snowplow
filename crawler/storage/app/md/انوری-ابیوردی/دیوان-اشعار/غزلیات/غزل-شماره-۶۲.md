---
title: >-
    غزل شمارهٔ ۶۲
---
# غزل شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>عشق تو بر هرکه عافیت به‌سر آرد</p></div>
<div class="m2"><p>هر دو جهانش به زیر پای درآرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل که در کوی روزگار نپاید</p></div>
<div class="m2"><p>بر سر کوی تو عمرها به‌سر آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبر که ساکن‌ترین عالم عشق است</p></div>
<div class="m2"><p>زلف تو هر ساعتش به رقص درآرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با توبه بیشئی صبر درنتوان بست</p></div>
<div class="m2"><p>زانکه به یک روزه غم شکم ز بر آرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوی تو باد ار شبی برد به طوافی</p></div>
<div class="m2"><p>جملهٔ عشاق را ز خاک برآرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم یارب چه عیشها کنمی من</p></div>
<div class="m2"><p>گر ز وصال توام کسی خبر آرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هجر ترا زین حدیث خنده برافتاد</p></div>
<div class="m2"><p>گفت که آری چنین بود اگر آرد</p></div></div>