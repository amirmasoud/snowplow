---
title: >-
    غزل شمارهٔ ۲۳۸
---
# غزل شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>ای بندهٔ روی تو خداوندان</p></div>
<div class="m2"><p>دیوانهٔ زلف تو خردمندان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بازار جمال روی خوبت را</p></div>
<div class="m2"><p>آراسته رسته رسته دلبندان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هر پس در مجاوری داری</p></div>
<div class="m2"><p>گریان و در انتظار دل خندان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چندین چه کنی به وعده دربندم</p></div>
<div class="m2"><p>ایام وفا نمی‌کند چندان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویی مشتاب تا که وقت آید</p></div>
<div class="m2"><p>گر خواهی وگرنه از بن دندان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خوی بدت شکایتی دارم</p></div>
<div class="m2"><p>کان نیست نشان نیک پیوندان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هجرت به جواب آن پدید آمد</p></div>
<div class="m2"><p>گفت اینت غم انوری سر و سندان</p></div></div>