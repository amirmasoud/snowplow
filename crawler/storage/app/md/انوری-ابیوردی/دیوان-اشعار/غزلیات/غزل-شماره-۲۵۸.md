---
title: >-
    غزل شمارهٔ ۲۵۸
---
# غزل شمارهٔ ۲۵۸

<div class="b" id="bn1"><div class="m1"><p>ای ایزد از لطافت محضت بیافریده</p></div>
<div class="m2"><p>واندر کنار رحمت و لطفت بپروریده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعلت به خنده توبهٔ کروبیان شکسته</p></div>
<div class="m2"><p>جزعت به غمزه پردهٔ روحانیان دریده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر گلبن اهل چو تو یک شاخ ناشکفته</p></div>
<div class="m2"><p>در بیشهٔ ازل چو تو یک مرغ ناپریده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشاطگان عالم علوی ز رشک خطت</p></div>
<div class="m2"><p>حوران خلد را به هوس نیل برکشیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای سایهٔ کمال تو بر شش جهت فتاده</p></div>
<div class="m2"><p>واوازهٔ جمال تو در نه فلک شنیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای از خیال روی تو اندر خیال هرکس</p></div>
<div class="m2"><p>ماه دگر برآمده صبحی دگر دمیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آرزوی سایهٔ قد تو هر سحرگه</p></div>
<div class="m2"><p>فریاد خاک کوی تو بر آسمان رسیده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما را به رایگان بخر از ما و داغ برنه</p></div>
<div class="m2"><p>ای درد و داغ عشق ترا ما به جان خریده</p></div></div>