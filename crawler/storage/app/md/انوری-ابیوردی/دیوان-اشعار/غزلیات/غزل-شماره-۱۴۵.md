---
title: >-
    غزل شمارهٔ ۱۴۵
---
# غزل شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>چون نیستی آنچنان که می‌باید</p></div>
<div class="m2"><p>تن در دادم چنانکه می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که از این بتر کنم خواهی</p></div>
<div class="m2"><p>الحق نه که هیچ درنمی‌باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با این همه غم که از تو می‌بینم</p></div>
<div class="m2"><p>گر خواب دگر نبینیم شاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با فتنهٔ روزگار تو عیدست</p></div>
<div class="m2"><p>هر فتنه که روزگار می‌زاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که دلم به بوسه خرسندست</p></div>
<div class="m2"><p>گفتی ندهم وگرچه می‌باید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین طرفه ترت حکایتی دارم</p></div>
<div class="m2"><p>دل بین که همی چه باد پیماید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوسی نه بدید و هر زمان گوید</p></div>
<div class="m2"><p>باشد که کناری اندر افزاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دستی برنه که انوری ای دل</p></div>
<div class="m2"><p>از دست تو پشت دست می‌خاید</p></div></div>