---
title: >-
    غزل شمارهٔ ۶۶
---
# غزل شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>تا ماه‌رویم از من رخ در حجیب دارد</p></div>
<div class="m2"><p>نه دیده خواب یابد نه دل شکیب دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم دست کامرانی دل از عنان گسسته</p></div>
<div class="m2"><p>هم پای زندگانی جان در رکیب دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پندار درد گشتم گویی که در دو عالم</p></div>
<div class="m2"><p>هرجا که هست دردی با من حسیب دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بفریفت آن شکر لب ما را به عشوه آری</p></div>
<div class="m2"><p>بس عشوه‌های شیرین کان دلفریب دارد</p></div></div>