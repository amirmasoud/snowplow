---
title: >-
    غزل شمارهٔ ۲۱۵
---
# غزل شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>نو به نو هر روز باری می‌کشم</p></div>
<div class="m2"><p>بار نبود چون ز یاری می‌کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناشکفته زو گلی هرگز مرا</p></div>
<div class="m2"><p>هر زمان زو رنج خاری می‌کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بلایش می‌کشم عیبم مکن</p></div>
<div class="m2"><p>کین بلا آخر به کاری می‌کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زحمت سرمای سرد از ماه دی</p></div>
<div class="m2"><p>بر امید نوبهاری می‌کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق هر دم در میانم می‌کشد</p></div>
<div class="m2"><p>گرچه خود را بر کناری می‌کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار من روزی شود همچون نگار</p></div>
<div class="m2"><p>کاین غم از بهر نگاری می‌کشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فخر وقت خویشتن دانم همی</p></div>
<div class="m2"><p>اینکه از خصمانش عاری می‌کشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بار او نتوان کشید از هجر و وصل</p></div>
<div class="m2"><p>پس مرا این بس که باری می‌کشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو مرا گویی کشیدی درد و غم</p></div>
<div class="m2"><p>من چه می‌گویم که آری می‌کشم</p></div></div>