---
title: >-
    غزل شمارهٔ ۱۸۶
---
# غزل شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>کس نداند کز غمت چون سوختم</p></div>
<div class="m2"><p>خویشتن در چه بلا اندوختم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدنی دیدم از آن رخسار تو</p></div>
<div class="m2"><p>جان بدان یک دیدنت بفروختم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برکشیدم جامهٔ شادی ز تن</p></div>
<div class="m2"><p>وز بلا دلقی کنون نو دوختم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچه دانش بود گم کردم همه</p></div>
<div class="m2"><p>در فراقت زرگری آموختم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زر براندودم برین رخسار سیم</p></div>
<div class="m2"><p>آتش اندر کورهٔ دل سوختم</p></div></div>