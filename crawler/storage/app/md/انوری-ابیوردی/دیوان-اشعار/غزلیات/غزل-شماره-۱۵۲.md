---
title: >-
    غزل شمارهٔ ۱۵۲
---
# غزل شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>یا وصل ترا عنایتی باید</p></div>
<div class="m2"><p>یا هجر ترا نهایتی باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد سورهٔ هجر می‌فرو خوانی</p></div>
<div class="m2"><p>در شان وصال آیتی باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل عمر به عشق می‌دهد رشوت</p></div>
<div class="m2"><p>آخر ز تو در حمایتی باید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوسی ندهی وگر طمع دارم</p></div>
<div class="m2"><p>گویی به بها ولایتی باید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الحق به از این بها به نتوان جست</p></div>
<div class="m2"><p>در هر کاری کفایتی باید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر ز تو در جهان پس از عمری</p></div>
<div class="m2"><p>جز جور و جفا حکایتی باید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وانگه ز منت چه عیب می‌جویی</p></div>
<div class="m2"><p>جز مهر و وفا شکایتی باید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خون منی چرا نیندیشی</p></div>
<div class="m2"><p>کین دل شده را جنایتی باید</p></div></div>