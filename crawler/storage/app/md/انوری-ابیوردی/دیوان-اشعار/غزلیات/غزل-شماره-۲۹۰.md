---
title: >-
    غزل شمارهٔ ۲۹۰
---
# غزل شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>ای کار غم تو غمگساری</p></div>
<div class="m2"><p>اندوه غم تو شادخواری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کبر نگاه کرد رویت</p></div>
<div class="m2"><p>در چشمهٔ خور به چشم خواری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تابش روی و تاب زلفت</p></div>
<div class="m2"><p>شب روشن گشت و روز تاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فقر غم تو ز باغ دلها</p></div>
<div class="m2"><p>برکند نهال کامگاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شربت بوسهٔ تو شافی</p></div>
<div class="m2"><p>وی ضربت غمزهٔ تو کاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داری سر آنکه بیش از اینم</p></div>
<div class="m2"><p>در بند فراق خود بداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویی بی‌من دل تو چونست</p></div>
<div class="m2"><p>چونست به صد هزار زاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزی که غم نوم نمایی</p></div>
<div class="m2"><p>آنرا به غنیمتی شماری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با یاران این کنند احسنت</p></div>
<div class="m2"><p>چشم بد دور نیک یاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امروز بر اسب جور با من</p></div>
<div class="m2"><p>هر گوشه همی کنی سواری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ترسم فردا گه مظالم</p></div>
<div class="m2"><p>تاب ثقةالملوک ناری</p></div></div>