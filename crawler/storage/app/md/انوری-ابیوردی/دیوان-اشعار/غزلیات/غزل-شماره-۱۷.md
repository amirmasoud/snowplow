---
title: >-
    غزل شمارهٔ ۱۷
---
# غزل شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>دل در آن یار دلاویز آویخت</p></div>
<div class="m2"><p>فتنه اینست که آن یار انگیخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل و دین و می و عهد و قوت</p></div>
<div class="m2"><p>رخت بر سر به یکی پای گریخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل من باز نمی‌یابد صبر</p></div>
<div class="m2"><p>همه آفاق به غربال تو بیخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور نمی‌یابد آن سلسله موی</p></div>
<div class="m2"><p>کار جانم به یکی موی آویخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل به سوی دل برفتم بر درش</p></div>
<div class="m2"><p>چشمم از اشک بسی چشم آویخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار گلرخ چو مرا بار ندارد</p></div>
<div class="m2"><p>گل عمرم همه از پای بریخت</p></div></div>