---
title: >-
    غزل شمارهٔ ۲۶۹
---
# غزل شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>گر مرا روزگار یارستی</p></div>
<div class="m2"><p>کار با یار چون نگارستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برنگشتی چو روزگار از من</p></div>
<div class="m2"><p>گرنه با روزگار یارستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برکنارم ز یار اگرنه مرا</p></div>
<div class="m2"><p>همه مقصود در کنارستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست در بوستان وصل گلی</p></div>
<div class="m2"><p>این چه ژاژست کاش خارستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هجر بر هجر می‌شمارم و هیچ</p></div>
<div class="m2"><p>بار یک وصل در شمارستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیش از این روی انتظارم نیست</p></div>
<div class="m2"><p>کاشکی روی انتظارستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزگارست مایهٔ همه کار</p></div>
<div class="m2"><p>ای دریغا که روزگارستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بارکش انوری حدیث مکن</p></div>
<div class="m2"><p>که اگر بر خریت بارستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در همه نامهات نامستی</p></div>
<div class="m2"><p>در همه کارهات کارستی</p></div></div>