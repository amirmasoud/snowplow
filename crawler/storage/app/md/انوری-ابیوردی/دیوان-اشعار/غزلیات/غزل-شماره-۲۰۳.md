---
title: >-
    غزل شمارهٔ ۲۰۳
---
# غزل شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>جز سر پیوند آن نگار ندارم</p></div>
<div class="m2"><p>گرچه ازو جز دل فکار ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نفسم یاد اوست گرچه ازو من</p></div>
<div class="m2"><p>جز نفس سرد یادگار ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاد بدانم که در فراق جمالش</p></div>
<div class="m2"><p>جز غم او هیچ غمگسار ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان نشوم رنجه از جفاش که در عشق</p></div>
<div class="m2"><p>سیرت عشاق روزگار ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وز غم هجران او به کاستن تن</p></div>
<div class="m2"><p>هیچ غم دیگر اعتبار ندارم</p></div></div>