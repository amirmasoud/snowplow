---
title: >-
    غزل شمارهٔ ۱۲۸
---
# غزل شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>دل به عشقش رخ به خون تر می‌کند</p></div>
<div class="m2"><p>جان ز جورش خاک بر سر می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌خورد خون دل و دل عشوهاش</p></div>
<div class="m2"><p>می‌خورد چون نوش و باور می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه پیش از وعده سوگندان خورد</p></div>
<div class="m2"><p>آنهم از پیشم فرا تر می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش بس می‌کند چشمت جفا</p></div>
<div class="m2"><p>گفت نیکو می‌کند گر می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل را چشم خوشش در نرد عشق</p></div>
<div class="m2"><p>می‌دهد شش ضرب و ششدر می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانکه تا دست سیاهش برنهند</p></div>
<div class="m2"><p>زلفش اکنون دست هم در می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زر ندارم لاجرم بی‌موجبی</p></div>
<div class="m2"><p>هر زمانم عیب دیگر می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت زر گفتم که جان، گفتا که خه</p></div>
<div class="m2"><p>الحق این نقدم توانگر می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم آخر جان به از زر گفت نه</p></div>
<div class="m2"><p>لاجرم کار تو چون زر می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون کنی خاکش همی بوس انوری</p></div>
<div class="m2"><p>گرچه با خاکت برابر می‌کند</p></div></div>