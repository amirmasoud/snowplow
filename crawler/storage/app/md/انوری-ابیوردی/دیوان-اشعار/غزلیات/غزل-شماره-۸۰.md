---
title: >-
    غزل شمارهٔ ۸۰
---
# غزل شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>روی تو آرام دلها می‌برد</p></div>
<div class="m2"><p>زلف تو زنهار جانها می‌خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا برآمد فتنهٔ زلف و رخت</p></div>
<div class="m2"><p>عافیت را کس به کس می‌نشمرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منهی عشق به دست رنگ و بوی</p></div>
<div class="m2"><p>راز دلها را به درها می‌برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت باشد بر سر بازار عشق</p></div>
<div class="m2"><p>کز تو یک غم دل به صد جان می‌خرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر کوی غمت چون دور چرخ</p></div>
<div class="m2"><p>پای کس جز بر سر خود نسپرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست دل در پردهٔ وصل لبت</p></div>
<div class="m2"><p>لاجرم زلف تو پرده‌اش می‌درد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پای در وصل لبت نتوان نهاد</p></div>
<div class="m2"><p>تا سر زلف تو در سر ناورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویمت وصلی مرا گویی که صبر</p></div>
<div class="m2"><p>تا دلم آن را طریقی بنگرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جمله در اندیشه سازی کار وصل</p></div>
<div class="m2"><p>تا تو بندیشی جهان می‌بگذرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وعده را بر در مزن چندین به عذر</p></div>
<div class="m2"><p>زندگانی را نگر چون می‌برد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گویی از من بگزران ای انوری</p></div>
<div class="m2"><p>چون کنم می‌نگزرد می‌نگزرد</p></div></div>