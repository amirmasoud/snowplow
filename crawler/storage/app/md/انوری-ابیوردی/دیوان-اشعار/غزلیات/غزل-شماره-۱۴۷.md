---
title: >-
    غزل شمارهٔ ۱۴۷
---
# غزل شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>دل در هوست ز جان برآید</p></div>
<div class="m2"><p>جان در غمت از جهان برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو جان و جهان مباش اندیک</p></div>
<div class="m2"><p>مقصود تو از میان برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سودیست تمام اگر دلی را</p></div>
<div class="m2"><p>یک غم ز تو رایگان برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همخانهٔ هرکه شد غم تو</p></div>
<div class="m2"><p>زودا که ز خان و مان برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وانکس که فرو شود به کویت</p></div>
<div class="m2"><p>دیرا که از او نشان برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویی که اگرچه هست کامم</p></div>
<div class="m2"><p>تا کام دل فلان برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لیکن ز زبان این و آنست</p></div>
<div class="m2"><p>هر طعنه که از زبان برآید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشنیدستی چنان توان مرد</p></div>
<div class="m2"><p>ای جان جهان که جان برآید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل طعنهٔ تو بدید بخرید</p></div>
<div class="m2"><p>تا دیدهٔ این و آن برآید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ارزان مفروش انوری را</p></div>
<div class="m2"><p>گر باز خری گران برآید</p></div></div>