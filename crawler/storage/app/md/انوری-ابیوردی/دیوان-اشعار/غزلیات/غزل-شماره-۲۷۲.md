---
title: >-
    غزل شمارهٔ ۲۷۲
---
# غزل شمارهٔ ۲۷۲

<div class="b" id="bn1"><div class="m1"><p>ای دیر به دست آمده بس زود برفتی</p></div>
<div class="m2"><p>آتش زدی اندر من و چون دود برفتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آرزوی تنگ‌دلان دیر رسیدی</p></div>
<div class="m2"><p>چون دوستی سنگ‌دلان زود برفتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان پیش که در باغ وصال تو دل من</p></div>
<div class="m2"><p>از داغ فراق تو برآسود برفتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناگشته من از بند تو آزاد بجستی</p></div>
<div class="m2"><p>ناکرده مرا وصل تو خشنود برفتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آهنگ به جان من دلسوخته کردی</p></div>
<div class="m2"><p>چون در دل من عشق بیفزود برفتی</p></div></div>