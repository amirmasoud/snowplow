---
title: >-
    غزل شمارهٔ ۱۶۱
---
# غزل شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>هیچ دانی که سر صحبت ما دارد یار</p></div>
<div class="m2"><p>سر پیوند چو من باز فرود آرد یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاشکی هیچ‌کسی زو خبری می‌دهدی</p></div>
<div class="m2"><p>تا از این واقعه خود هیچ خبر دارد یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو ببینی که مرا عشوه دهان خنداخند</p></div>
<div class="m2"><p>سالها زار بگریاند و بگذارد یار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یارت ار جو کند خود چکند چون به عتاب</p></div>
<div class="m2"><p>خون بریزد که همی موی نیازارد یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>انوری جان جهان گیر و کم انگار دلی</p></div>
<div class="m2"><p>پیش از آن کت به همین روز کم انگارد یار</p></div></div>