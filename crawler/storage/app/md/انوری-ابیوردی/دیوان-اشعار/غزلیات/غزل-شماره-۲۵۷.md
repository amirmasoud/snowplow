---
title: >-
    غزل شمارهٔ ۲۵۷
---
# غزل شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>ای برده دل من و جفا کرده</p></div>
<div class="m2"><p>بافرقت خویشم آشنا کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر به جفا مرا بیازردی</p></div>
<div class="m2"><p>در اول دوستی وفا کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی از تو بتا چگونه گردانم</p></div>
<div class="m2"><p>پشت از غم عشق تو دو تا کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر روز مرا هزار بد گویی</p></div>
<div class="m2"><p>من بر تو هزار شب دعا کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای رنج فراق روی و موی تو</p></div>
<div class="m2"><p>جان ودل من ز من جدا کرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وانگه من مستمند بی‌دل را</p></div>
<div class="m2"><p>در محنت عاشقی رها کرده</p></div></div>