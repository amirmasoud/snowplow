---
title: >-
    غزل شمارهٔ ۳
---
# غزل شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ای کرده خجل بتان چین را</p></div>
<div class="m2"><p>بازار شکسته حور عین را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنشانده پیاده ماه گردون</p></div>
<div class="m2"><p>برخاسته فتنهٔ زمین را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگذار مرا به ناز اگر چند</p></div>
<div class="m2"><p>خوب آید ناز نازنین را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منمای همه جفا گه مهر</p></div>
<div class="m2"><p>چیزی بگذار روز کین را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلداران بیش از این ندارند</p></div>
<div class="m2"><p>با درد قرین چو من قرین را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم یاد کنند گه گه آخر</p></div>
<div class="m2"><p>خدمتگاران اولین را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای گم شده مه ز عکس رویت</p></div>
<div class="m2"><p>در کوی تو لعبتان چین را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این از تو مرا بدیع ننمود</p></div>
<div class="m2"><p>من روز همی شمردم این را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیری نکند مرا ز جورت</p></div>
<div class="m2"><p>چونان که ز جود مجد دین را</p></div></div>