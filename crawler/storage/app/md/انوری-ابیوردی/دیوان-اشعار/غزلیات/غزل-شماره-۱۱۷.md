---
title: >-
    غزل شمارهٔ ۱۱۷
---
# غزل شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>درد تو دلا نهان نماند</p></div>
<div class="m2"><p>اندوه تو جاودان نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عشق مشو چنین شکفته</p></div>
<div class="m2"><p>کان روی نکو چنان نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آوازهٔ تو فرو نشیند</p></div>
<div class="m2"><p>وز محنت تو نشان نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر با همه کس چنین کند دل</p></div>
<div class="m2"><p>یک دلشده در جهان نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از درد تو دل نماند و بیمست</p></div>
<div class="m2"><p>کز بی‌رحمیت جان نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کار جهان کرانه‌ای دل</p></div>
<div class="m2"><p>کازار درین میان نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن سود بسم که تو بمانی</p></div>
<div class="m2"><p>بل تا همه سو زیان نماند</p></div></div>