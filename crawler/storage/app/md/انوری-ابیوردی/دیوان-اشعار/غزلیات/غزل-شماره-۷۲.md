---
title: >-
    غزل شمارهٔ ۷۲
---
# غزل شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>تا کار مرا وصل تو تیمار ندارد</p></div>
<div class="m2"><p>جز با غم هجر تو دلم کار ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌رونقی کار من اندر غم عشقت</p></div>
<div class="m2"><p>کاریست که جز هجر تو بر بار ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارد سر خون ریختنم هجر تو دانی</p></div>
<div class="m2"><p>هجر تو چنین کار به بیگار ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویی که ندارد به تو قصدی تو چه دانی</p></div>
<div class="m2"><p>این هست غم هجر تو نهمار ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با هجر تو گفتم که چه خیزد ز کسی کو</p></div>
<div class="m2"><p>از گلبن ایام نه گل خار ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی که چو دل جان بده انکار نداری</p></div>
<div class="m2"><p>جانا تو نگوییش که انکار ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون می‌ننیوشد سخن انوری آخر</p></div>
<div class="m2"><p>یک ره تو بگو گفت ترا خوار ندارد</p></div></div>