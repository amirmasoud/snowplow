---
title: >-
    غزل شمارهٔ ۲۳۷
---
# غزل شمارهٔ ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>درمان دل خود از که جویم</p></div>
<div class="m2"><p>افسانهٔ خویش با که گویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تخمی که نروید آن چه کارم</p></div>
<div class="m2"><p>چیزی که نیابم آن چه جویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آورد فراق زردرویی</p></div>
<div class="m2"><p>دور از رخت ای صنم به رویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای یوسف عصر بی‌رخ تو</p></div>
<div class="m2"><p>بیت‌الاحزان شدست کویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر ره حرص با دو همراه</p></div>
<div class="m2"><p>چون بیم و امید چند پویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من تشنه بر آن لبم وگر چند</p></div>
<div class="m2"><p>بر چهره همی رود دو جویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌سنگ شدم ز فرقت آری</p></div>
<div class="m2"><p>وقتست اگرنه سنگ و رویم</p></div></div>