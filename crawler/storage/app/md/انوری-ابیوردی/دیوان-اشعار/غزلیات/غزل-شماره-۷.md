---
title: >-
    غزل شمارهٔ ۷
---
# غزل شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>از دور بدیدم آن پری را</p></div>
<div class="m2"><p>آن رشک بتان آزری را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مغرب زلف عرض داده</p></div>
<div class="m2"><p>صد قافله ماه و مشتری را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر گوشهٔ عارض چو کافور</p></div>
<div class="m2"><p>برهم زده زلف عنبری را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جزعش به کرشمه درنوشته</p></div>
<div class="m2"><p>صد تختهٔ تازه کافری را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لعلش به ستیزه در نموده</p></div>
<div class="m2"><p>صد معجزهٔ پیمبری را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیر مژه بر کمان ابرو</p></div>
<div class="m2"><p>برکرده عتاب و داوری را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر دامن هجر و وصل بسته</p></div>
<div class="m2"><p>بدبختی و نیک‌اختری را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترسان ترسان به طنز گفتم</p></div>
<div class="m2"><p>آن مایهٔ حسن و دلبری را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کز بهر خدای را کرایی؟</p></div>
<div class="m2"><p>گفتا به خدا که انوری را</p></div></div>