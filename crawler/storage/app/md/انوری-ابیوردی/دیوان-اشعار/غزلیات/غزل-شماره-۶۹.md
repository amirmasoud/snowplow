---
title: >-
    غزل شمارهٔ ۶۹
---
# غزل شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>جان نقش رخ تو بر نگین دارد</p></div>
<div class="m2"><p>دل داغ غم تو بر سرین دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دامن دل به دست عشق تست</p></div>
<div class="m2"><p>صد گونه هنر در آستین دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم تو دلم ببرد و می‌بینم</p></div>
<div class="m2"><p>کاکنون پی جان و قصد دین دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وافکنده کمان غمزه در بازو</p></div>
<div class="m2"><p>تا باز چه فتنه در کمین دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویی که سخن مگوی و دم درکش</p></div>
<div class="m2"><p>انصاف بده که برگ این دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چند که پوستین به گازر ده</p></div>
<div class="m2"><p>خرم دل آنکه پوستین دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در باغ جهان مرا چه می‌بینی</p></div>
<div class="m2"><p>جز عشق تویی که در زمین دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خشک و تر انوری به صد حیلت</p></div>
<div class="m2"><p>در فرقت تو دلی حزین دارد</p></div></div>