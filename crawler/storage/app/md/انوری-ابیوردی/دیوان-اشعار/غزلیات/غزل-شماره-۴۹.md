---
title: >-
    غزل شمارهٔ ۴۹
---
# غزل شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>بی‌مهر جمال تو دلی نیست</p></div>
<div class="m2"><p>بی‌مهر هوای تو گلی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذشت زمانه وز تو کس را</p></div>
<div class="m2"><p>جز عمر گذشته حاصلی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا از چه گلی که از تو خالی</p></div>
<div class="m2"><p>در عالم آب و گل دلی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دائرهٔ جهان محدث</p></div>
<div class="m2"><p>چون حادثهٔ تو مشکلی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در تو که رسد که در ره تو</p></div>
<div class="m2"><p>جز منزل عجز منزلی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بحر تحیر تو پایاب</p></div>
<div class="m2"><p>کی سود کند که ساحلی نیست</p></div></div>