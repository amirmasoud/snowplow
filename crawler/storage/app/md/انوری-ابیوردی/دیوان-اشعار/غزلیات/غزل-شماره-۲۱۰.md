---
title: >-
    غزل شمارهٔ ۲۱۰
---
# غزل شمارهٔ ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>عمر بی‌تو به سر چگونه برم</p></div>
<div class="m2"><p>که همی بی‌تو روز و شب شمرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خونها از دو دیده پالودم</p></div>
<div class="m2"><p>رخنه رخنه شد از غمت جگرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو ز شادی و خرمی برخور</p></div>
<div class="m2"><p>که من از تو به جز جگر نخورم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر این بود بخششم ز فلک</p></div>
<div class="m2"><p>که ز دست غم تو جان نبرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند برتافتم ز کوی تو روی</p></div>
<div class="m2"><p>با قضا برنیامد آن حذرم</p></div></div>