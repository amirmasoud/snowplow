---
title: >-
    غزل شمارهٔ ۱۹۷
---
# غزل شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>هرچند به جای تو وفا دارم</p></div>
<div class="m2"><p>هم از تو توقع جفا دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سر ز تو همچنان هوس دارم</p></div>
<div class="m2"><p>در دل ز تو همچنان هوادارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از من چو جهان مبر که تو دانی</p></div>
<div class="m2"><p>کز دولت این جهان ترا دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیگانه مشو چو دین و دل با من</p></div>
<div class="m2"><p>چون با غم تو دل آشنا دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویی که مگوی راز با خصمان</p></div>
<div class="m2"><p>حاشا لله که این روا دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیکن به گل آفتاب چون پوشم</p></div>
<div class="m2"><p>چون پشت چو ماه نو دوتا دارم</p></div></div>