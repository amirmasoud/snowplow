---
title: >-
    غزل شمارهٔ ۲۰۱
---
# غزل شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>عشقت اندر میان جان دارم</p></div>
<div class="m2"><p>جان ز بهر تو بر میان دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا مرا بر سر جهان داری</p></div>
<div class="m2"><p>به سرت گر سر جهان دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویی از دست هجر جان نبری</p></div>
<div class="m2"><p>غافلم گرنه این گمان دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سرم هرچه عشق بنوشتست</p></div>
<div class="m2"><p>یک به یک بر سر زبان دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از اثرهای طالع عشقت</p></div>
<div class="m2"><p>چون قضاهای آسمان دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیش پای از قفای هجر منه</p></div>
<div class="m2"><p>من بیچاره نیز جان دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جانم اندر بهار وصل بخر</p></div>
<div class="m2"><p>گرچه بر هجر دل زیان دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویی از جان کسی حدیث کند</p></div>
<div class="m2"><p>چه کنم در کیایی آن دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر تو احوال انوری پیداست</p></div>
<div class="m2"><p>به تکلف چرا نهان دارم</p></div></div>