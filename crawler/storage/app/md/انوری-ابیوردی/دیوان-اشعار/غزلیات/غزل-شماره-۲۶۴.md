---
title: >-
    غزل شمارهٔ ۲۶۴
---
# غزل شمارهٔ ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>بر مه از عنبر عذار آورده‌ای</p></div>
<div class="m2"><p>بر پرند از مشک مار آورده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر حریر از قیر نقش افکنده‌ای</p></div>
<div class="m2"><p>بر گل از سنبل نگار آورده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه خوبان را به کار آید ز حسن</p></div>
<div class="m2"><p>در خط مشکین به کار آورده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیش رخ منمای کاندر کار تن</p></div>
<div class="m2"><p>روح را چون زیر و زار آورده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش می‌کردی حساب عاشقان</p></div>
<div class="m2"><p>انوری ار در شمار آورده‌ای</p></div></div>