---
title: >-
    غزل شمارهٔ ۳۷
---
# غزل شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>جمالت بر سر خوبی کلاهست</p></div>
<div class="m2"><p>بنامیزد نه رویست آن که ماهست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تویی کز زلف و رخ در عالم حسن</p></div>
<div class="m2"><p>ترا هم نیم شب هم چاشتگاهست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسا خرمن که آتش در زدی باش</p></div>
<div class="m2"><p>هنوزت آب خوبی زیر کاهست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پی عهدت نیاید جز در آن راه</p></div>
<div class="m2"><p>کز آنجا تا وفا صد ساله راهست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز عشوت روز عمرم در شب افتاد</p></div>
<div class="m2"><p>وزین غم بر دلم روز سیاهست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس از چندی صبوری داد باشد</p></div>
<div class="m2"><p>که گویم بوسه‌ای گویی پگاهست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبی قصد لبت کردم از آن شب</p></div>
<div class="m2"><p>سپاه کین چشمت در سپاهست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به تیر غمزه مژگانت انوری را</p></div>
<div class="m2"><p>بکشتند و برین شهری گواهست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لبت را گو که تدبیر دیت کن</p></div>
<div class="m2"><p>سر زلفت مبر کو بی‌گناهست</p></div></div>