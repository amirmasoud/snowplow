---
title: >-
    غزل شمارهٔ ۶۳
---
# غزل شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>یار دل در میان نمی‌آرد</p></div>
<div class="m2"><p>وز دل من نشان نمی‌آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سایه بر کار من نمی‌فکند</p></div>
<div class="m2"><p>تا که کارم به جان نمی‌آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وز بزرگی اگرچه در کارست</p></div>
<div class="m2"><p>خویشتن را بدان نمی‌آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی به پیمان من درآرد سر</p></div>
<div class="m2"><p>چون که سر در جهان نمی‌آرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز عمرم گذشت و وعدهٔ وصل</p></div>
<div class="m2"><p>شب هجرش کران نمی‌آرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمر سرمایه‌ایست نامعلوم</p></div>
<div class="m2"><p>تاب چندین زیان نمی‌آرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به سر او که عشق او به سرم</p></div>
<div class="m2"><p>یک بلا رایگان نمی‌آرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دروغی بر انوری همه عمر</p></div>
<div class="m2"><p>گر سر آرد توان نمی‌آرد</p></div></div>