---
title: >-
    غزل شمارهٔ ۱۸۱
---
# غزل شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>تا به مهر تو تولا کرده‌ام</p></div>
<div class="m2"><p>از همه خوبان تبرا کرده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر غمی کاید به روی من ز تو</p></div>
<div class="m2"><p>جای آن در سینه پیدا کرده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی فرود آید غمت جای دگر</p></div>
<div class="m2"><p>چون من اسبابی مهیا کرده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بهای هر غمی خواهی دلی</p></div>
<div class="m2"><p>وانگهی گویی محابا کرده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که در امید فردا در غمت</p></div>
<div class="m2"><p>با دل مسکین مدارا کرده‌ام</p></div></div>