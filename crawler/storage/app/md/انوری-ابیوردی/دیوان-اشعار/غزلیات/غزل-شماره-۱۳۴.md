---
title: >-
    غزل شمارهٔ ۱۳۴
---
# غزل شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>یارم این بار، بار می‌ندهد</p></div>
<div class="m2"><p>بخت کارم قرار می‌ندهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواب بختم دراز شد مگرش</p></div>
<div class="m2"><p>چرخ جز کوکنار می‌ندهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزگارم ز باغ بوک و مگر</p></div>
<div class="m2"><p>گل نگویم که خار می‌ندهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخت یاری نمی‌دهد نی‌نی</p></div>
<div class="m2"><p>این بهانه است یار می‌ندهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیک غمناکم از زمانه ازآنک</p></div>
<div class="m2"><p>جز غمم یادگار می‌ندهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این همه هست خود ولیکن اینک</p></div>
<div class="m2"><p>با غمم غمگسار می‌ندهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زانکه تا دل به گریه خوش نکنم</p></div>
<div class="m2"><p>اشک بی‌انتظار می‌ندهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>انوری دل ز روزگار ببر</p></div>
<div class="m2"><p>که دمی روزگار می‌ندهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچ‌کس را ز ساکنان زمین</p></div>
<div class="m2"><p>آسمان زینهار می‌ندهد</p></div></div>