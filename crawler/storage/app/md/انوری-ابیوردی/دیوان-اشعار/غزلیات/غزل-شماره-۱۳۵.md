---
title: >-
    غزل شمارهٔ ۱۳۵
---
# غزل شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>هرکه دل بر چون تو دلداری نهد</p></div>
<div class="m2"><p>سنگ بر دل بی‌تو بسیاری نهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانکه را محنت گلی خواهد شکفت</p></div>
<div class="m2"><p>روزگارش این چنین خاری نهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وانکه جانش همچو دل نبود به کار</p></div>
<div class="m2"><p>خویشتن را با تو در کاری نهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تحفه سازد گه گهم آن دل ظریف</p></div>
<div class="m2"><p>آرد و در دست خونخواری نهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیک می‌کوشد خدایش یار باد</p></div>
<div class="m2"><p>بو که روزی دست بر یاری نهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق گفت این هجر باری کیست و چیست</p></div>
<div class="m2"><p>خود کسی بر دل ازو باری نهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بار پای اندر میان خواهد نهاد</p></div>
<div class="m2"><p>تا به وصلت روز بازاری نهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هجر گفت از جانب تو راست شد</p></div>
<div class="m2"><p>اینت سودا و هوس آری نهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یار پای اندر میان ننهد ولیک</p></div>
<div class="m2"><p>انوری سر در میان باری نهد</p></div></div>