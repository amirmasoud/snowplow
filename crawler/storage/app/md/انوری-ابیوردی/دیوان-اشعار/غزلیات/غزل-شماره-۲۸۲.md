---
title: >-
    غزل شمارهٔ ۲۸۲
---
# غزل شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>دلم بردی نگارا وارمیدی</p></div>
<div class="m2"><p>جزاک‌الله خیرا رنج دیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جان چاکرت ار قصد کردی</p></div>
<div class="m2"><p>بحمدالله بدان نهمت رسیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خطا گفتم من از عشقت به حکمت</p></div>
<div class="m2"><p>معاذالله که از من این شنیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیابد بیش از این دانم غرامت</p></div>
<div class="m2"><p>که خط در دفتر جانم کشیدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنون باری به وصلت درپذیرم</p></div>
<div class="m2"><p>چون با این جمله عیبم درخریدی</p></div></div>