---
title: >-
    غزل شمارهٔ ۲۰۶
---
# غزل شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>اگر نقش رخت بر جان ندارم</p></div>
<div class="m2"><p>به زلف کافرت ایمان ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز تو یک درد را درمان مبادم</p></div>
<div class="m2"><p>اگر صد درد بی‌درمان ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عشقت رازها دارم ولیکن</p></div>
<div class="m2"><p>ز بی‌صبری یکی پنهان ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبوری را مگر معذور داری</p></div>
<div class="m2"><p>دلی می‌باید و من آن ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا گویی ز پیوندم چه داری</p></div>
<div class="m2"><p>چه دارم جز غم هجران ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر از تو بوسه‌ای خواهم به جانی</p></div>
<div class="m2"><p>تو گویی بوسهٔ ارزان ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لبت دندانم از جا برکشیدست</p></div>
<div class="m2"><p>چو گویی با لبت دندان ندارم</p></div></div>