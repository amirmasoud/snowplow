---
title: >-
    غزل شمارهٔ ۲۷۰
---
# غزل شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>همچون سر زلف خود شکستی</p></div>
<div class="m2"><p>آن عهد که با رهی ببستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بد عهد نخوانمت نگارا</p></div>
<div class="m2"><p>هرچند که عهد من شکستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس سیرت و خوی تو نداند</p></div>
<div class="m2"><p>من دانم و دل چنان که هستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شاخ وفا گلم ندادی</p></div>
<div class="m2"><p>وز خار جفا دلم بخستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از هجر تو در خمارم امروز</p></div>
<div class="m2"><p>نایافته‌ای ز وصل هستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با این همه میل من سوی تو</p></div>
<div class="m2"><p>چون رفتن سیل سوی پستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جان من ای عزیز چون جان</p></div>
<div class="m2"><p>کوتاه کن این درازدستی</p></div></div>