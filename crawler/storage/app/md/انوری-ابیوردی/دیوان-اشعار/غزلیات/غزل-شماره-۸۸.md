---
title: >-
    غزل شمارهٔ ۸۸
---
# غزل شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>مرا صورت نمی‌بندد که دل یاری دگر گیرد</p></div>
<div class="m2"><p>مرا بیکار بگذارد سر کاری دگر گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل خود را دهم پندی اگرچه پند نپذیرد</p></div>
<div class="m2"><p>که بگذارد هوای او هواداری دگر گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازو دوری نیارم جست ترسم زانکه ناگاهی</p></div>
<div class="m2"><p>خورد زنهار با جانم وفاداری دگر گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر زان لعل شکربار بفروشد به جان مویی</p></div>
<div class="m2"><p>رضای او بجوید جان خریداری دگر گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل باغ وصالش را رها کردم به نادانی</p></div>
<div class="m2"><p>به جای گل ز هجر او همی خاری دگر گیرد</p></div></div>