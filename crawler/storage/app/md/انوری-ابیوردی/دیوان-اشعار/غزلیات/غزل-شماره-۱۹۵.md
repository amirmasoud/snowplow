---
title: >-
    غزل شمارهٔ ۱۹۵
---
# غزل شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>دل باز به عاشقی درافکندم</p></div>
<div class="m2"><p>برداد به باد عهد و سوگندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیوست به عشق تا دگرباره</p></div>
<div class="m2"><p>ببرید ز خاص و عام پیوندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برکند به دست عشوه از بیخم</p></div>
<div class="m2"><p>تا بیخ صلاح و توبه برکندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پندم بدهد همی شود در سر</p></div>
<div class="m2"><p>این بار که نیک نیک دربندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بستهٔ بند عاشقی باشم</p></div>
<div class="m2"><p>کی سود کند نصیحت و پندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از مرهم وصل فارغم زیرا</p></div>
<div class="m2"><p>کز یار به درد هجر خرسندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آخر شب هجر بگذرد بر من</p></div>
<div class="m2"><p>گر بگذارند روزکی چندم</p></div></div>