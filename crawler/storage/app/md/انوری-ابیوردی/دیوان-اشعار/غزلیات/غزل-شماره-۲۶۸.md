---
title: >-
    غزل شمارهٔ ۲۶۸
---
# غزل شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>جانا به کمال صورتی‌ای</p></div>
<div class="m2"><p>در حسن و جمال آیتی‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصف رخ تو چگونه گویم</p></div>
<div class="m2"><p>می‌دان که به رخ قیامتی‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با وصل تو ملک جم نخواهم</p></div>
<div class="m2"><p>زیرا که تو به ز ملکتی‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انصاف اگر دهیم جانا</p></div>
<div class="m2"><p>آراسته خوب صورتی‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی که تراام انوری باش</p></div>
<div class="m2"><p>لیکن چه کنم که ساعتی‌ای</p></div></div>