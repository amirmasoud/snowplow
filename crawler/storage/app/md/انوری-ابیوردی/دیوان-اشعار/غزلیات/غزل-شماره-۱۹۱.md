---
title: >-
    غزل شمارهٔ ۱۹۱
---
# غزل شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>برآنم کز تو هرگز برنگردم</p></div>
<div class="m2"><p>به گرد دلبری دیگر نگردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل اندر عشق بستم، ور همه عمر</p></div>
<div class="m2"><p>جفا بینم هم از تو برنگردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا اسلام ماندست اندر آن کوش</p></div>
<div class="m2"><p>که از هجران تو کافر نگردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنانم من ز هجرانت نگارا</p></div>
<div class="m2"><p>کز این غم تا زیم بهتر نگردم</p></div></div>