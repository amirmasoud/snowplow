---
title: >-
    غزل شمارهٔ ۶
---
# غزل شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>گر باز دگرباره ببینم مگر اورا</p></div>
<div class="m2"><p>دارم ز سر شادی بر فرق سر او را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با من چو سخن گوید جز تلخ نگوید</p></div>
<div class="m2"><p>تلخ از چه سبب گوید چندین شکر او را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوگند خورم من به خدا و به سر او</p></div>
<div class="m2"><p>کاندر دو جهان دوست ندارم مگر او را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چندان که رسانید بلاها به سر من</p></div>
<div class="m2"><p>یارب مرسان هیچ بلایی به سر او را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر شب ز بر شام همی تا به سحرگه</p></div>
<div class="m2"><p>رخساره کنم سرخ ز خون جگر او را</p></div></div>