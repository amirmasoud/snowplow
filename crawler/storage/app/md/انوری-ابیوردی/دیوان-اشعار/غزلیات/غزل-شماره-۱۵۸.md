---
title: >-
    غزل شمارهٔ ۱۵۸
---
# غزل شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>طاقتم در فراق تو برسید</p></div>
<div class="m2"><p>صبر یکبارگی ز من برمید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا گرفتار عشق شد جانم</p></div>
<div class="m2"><p>بر دلم باد خرمی نوزید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرخ بر روزنامهٔ عمرم</p></div>
<div class="m2"><p>همه گویی نشان هجر کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل کوشید با غمت یک‌چند</p></div>
<div class="m2"><p>عاقبت هم طریق عجز گزید</p></div></div>