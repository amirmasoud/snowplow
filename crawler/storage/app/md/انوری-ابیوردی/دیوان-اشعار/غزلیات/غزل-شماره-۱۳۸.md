---
title: >-
    غزل شمارهٔ ۱۳۸
---
# غزل شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>آن روزگار کو که مرا یار یار بود</p></div>
<div class="m2"><p>من بر کنار از غم و او در کنار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزم به آخر آمد و روزی نزاد نیز</p></div>
<div class="m2"><p>زان گونه روزگار که آن روزگار بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امروز نیست هیچ امیدم به کار خویش</p></div>
<div class="m2"><p>بدرود دی که کار من امیدوار بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دایم شمار وصل همی برگرفت دل</p></div>
<div class="m2"><p>این هجر بی‌شمار کجا در شمار بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با روی چون نگار نگارم هزار شب</p></div>
<div class="m2"><p>کارم ز خرمی و خوشی چون نگار بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واکنون هزاربار شبی با دریغ و درد</p></div>
<div class="m2"><p>گویم که یارب آن چه نشاط و چه کار بود</p></div></div>