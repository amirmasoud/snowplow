---
title: >-
    غزل شمارهٔ ۱۵
---
# غزل شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>تا دل مسکین من در کار تست</p></div>
<div class="m2"><p>آرزوی جان من دیدار تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان و دل در کار تو کردم فدا</p></div>
<div class="m2"><p>کار من این بود دیگر کار تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با تو نتوان کرد دست اندر کمر</p></div>
<div class="m2"><p>هرچه خواهی کن که دولت یار تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل ترا دادم وگر جان بایدت</p></div>
<div class="m2"><p>هم فدای لعل شکربار تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شایدم گر جان و دل از دست رفت</p></div>
<div class="m2"><p>ایمنم اندی که در زنهار تست</p></div></div>