---
title: >-
    غزل شمارهٔ ۱۸۰
---
# غزل شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>مست از درم درآمد دوش آن مه تمام</p></div>
<div class="m2"><p>دربر گرفته چنگ و به کف برنهاده جام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر روز روشن از شب تیره فکنده بند</p></div>
<div class="m2"><p>وز مشک سوده بر گل سوری نهاده دام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آهنگ پست کرده به صوت حزین خویش</p></div>
<div class="m2"><p>شکر همی فشانده ز یاقوت لعل‌فام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی که لعل ناب و عقیق گداخته است</p></div>
<div class="m2"><p>درجام او ز عکس رخ او شراب خام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنشست بر کنار من و باده نوش کرد</p></div>
<div class="m2"><p>آن ماه سروقامت و آن سروکش خرام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت ای کسی که در همه عمر از جفاء چرخ</p></div>
<div class="m2"><p>با من شبی به روز نیاورده‌ای به کام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اینک من و تو و می لعل و سرود و رود</p></div>
<div class="m2"><p>بی‌زحمت رسول و فرستادن پیام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با چنگ بر کنار بد اندر کنار من</p></div>
<div class="m2"><p>مخمور تا به صبح سفید از نماز شام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در گوشه‌ای که کس نبد آگه ز حال ما</p></div>
<div class="m2"><p>زان عشرت به غایت و زان مستی تمام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه مطرب و نه ساقی و نه یار و نه حریف</p></div>
<div class="m2"><p>او بود و انوری و می لعل والسلام</p></div></div>