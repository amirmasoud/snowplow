---
title: >-
    غزل شمارهٔ ۵۶
---
# غزل شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>مرا با دلبری کاری بیفتاد</p></div>
<div class="m2"><p>دلم را روز بازاری بیفتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسلمانان مرا معذور دارید</p></div>
<div class="m2"><p>دلم را ناگهان کاری بیفتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قبای عشق مجنون می‌بریدند</p></div>
<div class="m2"><p>دلم را زان کله واری بیفتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم سجادهٔ عشقش برافشاند</p></div>
<div class="m2"><p>از آن سجاده زناری بیفتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم با عشق دست اندر کمر زد</p></div>
<div class="m2"><p>بسی کوشید و یکباری بیفتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا افتاد با بالای او کار</p></div>
<div class="m2"><p>نه بر بالای من کاری بیفتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان را چون دل من بر زمین زد</p></div>
<div class="m2"><p>کنون از دست دلداری بیفتاد</p></div></div>