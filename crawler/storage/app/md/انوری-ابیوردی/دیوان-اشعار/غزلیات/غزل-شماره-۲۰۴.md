---
title: >-
    غزل شمارهٔ ۲۰۴
---
# غزل شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>داری خبر که در غمت از خود خبر ندارم</p></div>
<div class="m2"><p>وز تو به جز غم تو نصیبی دگر ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستم به خاک‌پای و به جان و سرت به حالی</p></div>
<div class="m2"><p>کامروز در غم تو سر پای و سر ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منمای درد هجر از این بیشتر که دانی</p></div>
<div class="m2"><p>از حد گذشت و طاقت ازین بیشتر ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردا که بر امید وصال تو در فراقت</p></div>
<div class="m2"><p>از من اثر نماند و ز وصلت اثر ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای جان و دل ببرده و در پرده خوش نشسته</p></div>
<div class="m2"><p>هان تا ز روی راز نهان پرده برندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشک چو سیم دارم و روی چو زر ازین غم</p></div>
<div class="m2"><p>کاندر خور جمال و رخت سیم و زر ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دارم ز غم هزار جگر خون و انوری را</p></div>
<div class="m2"><p>شب نیست تا به خون جگر دیده تر ندارم</p></div></div>