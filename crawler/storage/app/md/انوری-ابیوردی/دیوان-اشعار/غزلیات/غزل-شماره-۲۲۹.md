---
title: >-
    غزل شمارهٔ ۲۲۹
---
# غزل شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>دل را به غمت نیاز می‌بینم</p></div>
<div class="m2"><p>کارت همه کبر و ناز می‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان جامه که دی وصل ما بودی</p></div>
<div class="m2"><p>اکنون نه بر آن طراز می‌بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد گونه زیان همی پدید آید</p></div>
<div class="m2"><p>سرمایهٔ دل چو باز می‌بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنرا که فلک همی کند نازش</p></div>
<div class="m2"><p>او را به تو هم نیاز می‌بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هین چند که زلف گردهٔ تو</p></div>
<div class="m2"><p>بر دست غمت دراز می‌بینم</p></div></div>