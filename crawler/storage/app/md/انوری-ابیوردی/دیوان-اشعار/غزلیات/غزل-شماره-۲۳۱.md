---
title: >-
    غزل شمارهٔ ۲۳۱
---
# غزل شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>روز دو از عشق پشیمان شوم</p></div>
<div class="m2"><p>توبه کنم باز و به سامان شوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز به یک وسوسهٔ دیو عشق</p></div>
<div class="m2"><p>بار دگر با سر دیوان شوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که ز عشق تو اگر من منم</p></div>
<div class="m2"><p>گبر شوم باز و مسلمان شوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلعجبی جان من از سر بنه</p></div>
<div class="m2"><p>کانچه کنی من به سر آن شوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوست تویی کاج بدانستمی</p></div>
<div class="m2"><p>کز تو به پیش که به افغان شوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من تو نگشتم که به هر خرده‌ای</p></div>
<div class="m2"><p>گه به فلان گاه به بهمان شوم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بن دندان بکشم جور تو</p></div>
<div class="m2"><p>بو که ترا بر سر دندان شوم</p></div></div>