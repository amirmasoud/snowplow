---
title: >-
    غزل شمارهٔ ۳۱۴
---
# غزل شمارهٔ ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>دوستا گر دوستی گر دشمنی</p></div>
<div class="m2"><p>جان شیرین و جهان روشنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سر کار تو کردم دین و دل</p></div>
<div class="m2"><p>انده جانست وان در می‌زنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برنیارم سر گرم در سرزنش</p></div>
<div class="m2"><p>ساعتی صد بار در پای افکنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا همی دانی که در کار توام</p></div>
<div class="m2"><p>رغم را پیوسته در خون منی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند گویی خونت اندر گردنت</p></div>
<div class="m2"><p>بس به سر بیرون مشو گر کردنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با منت چندین چه باید کارزار</p></div>
<div class="m2"><p>چون مصاف من ببوسی بشکنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون فلک با انوری توسن نگشت</p></div>
<div class="m2"><p>مردمی کن درگذر زین توسنی</p></div></div>