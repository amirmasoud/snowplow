---
title: >-
    غزل شمارهٔ ۱۶۹
---
# غزل شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>تختهٔ عشق برنوشتم باز</p></div>
<div class="m2"><p>برنویس ای نگار تختهٔ ناز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بر استاد عاشقی خوانیم</p></div>
<div class="m2"><p>روزکی چند باب ناز و نیاز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ورقی باز کن ز عهد قدیم</p></div>
<div class="m2"><p>باز کن خاک عشوه از سر آز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هین که روز و شب زمانه همی</p></div>
<div class="m2"><p>ورق عمرمان کنند فراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند گویی زمانه در پیش است</p></div>
<div class="m2"><p>بر وفای زمانه هیچ مناز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصه کوتاه کن که کوته کرد</p></div>
<div class="m2"><p>روز امید انتظار دراز</p></div></div>