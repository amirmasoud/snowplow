---
title: >-
    غزل شمارهٔ ۲۳۵
---
# غزل شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>ای روی خوب تو سبب زندگانیم</p></div>
<div class="m2"><p>یک روزه وصل تو طرب جاودانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز با جمال تو نبود شادمانیم</p></div>
<div class="m2"><p>جز با وصال تو نبود کامرانیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌یاد روی خوب تو ار یک نفس زنم</p></div>
<div class="m2"><p>محسوب نیست آن نفس از زندگانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردی نهانیست مرا از فراق تو</p></div>
<div class="m2"><p>ای شادی تو آفت درد نهانیم</p></div></div>