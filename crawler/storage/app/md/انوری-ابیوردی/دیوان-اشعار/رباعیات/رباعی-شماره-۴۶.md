---
title: >-
    رباعی شمارهٔ ۴۶
---
# رباعی شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>کون خر ملک ریش گاو افتادست</p></div>
<div class="m2"><p>چون استر بد لایق داو افتادست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صدر وزارتت که در عشق زرست</p></div>
<div class="m2"><p>چون از پس راء عمرو واو افتادست</p></div></div>