---
title: >-
    رباعی شمارهٔ ۱۸۵
---
# رباعی شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>قومی که در این سفر مرا همراهند</p></div>
<div class="m2"><p>از تعبیهٔ زمانه کم آگاهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما می‌کوشیم و آسمان می‌گوید</p></div>
<div class="m2"><p>نقش آن باشد که نقشبندان خواهند</p></div></div>