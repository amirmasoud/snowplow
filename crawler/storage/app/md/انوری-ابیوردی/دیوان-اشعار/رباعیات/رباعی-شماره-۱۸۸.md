---
title: >-
    رباعی شمارهٔ ۱۸۸
---
# رباعی شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>گفت آنکه مرا ره سلامت بنمود</p></div>
<div class="m2"><p>کان بت نکند وفا و برگردد زود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی آن همه گفتها یقین گشت و نبود</p></div>
<div class="m2"><p>وامروز نداردم پشیمانی سود</p></div></div>