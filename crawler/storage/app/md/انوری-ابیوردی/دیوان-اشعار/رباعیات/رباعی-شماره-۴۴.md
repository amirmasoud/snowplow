---
title: >-
    رباعی شمارهٔ ۴۴
---
# رباعی شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>در سایهٔ آن زلف مشوش که تراست</p></div>
<div class="m2"><p>ای بس دل سرگشتهٔ غمکش که تراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌بر دل و می ده غم و فارغ می‌رو</p></div>
<div class="m2"><p>دور از دل من زهی دل خوش که تراست</p></div></div>