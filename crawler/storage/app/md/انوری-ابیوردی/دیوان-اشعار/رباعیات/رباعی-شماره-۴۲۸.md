---
title: >-
    رباعی شمارهٔ ۴۲۸
---
# رباعی شمارهٔ ۴۲۸

<div class="b" id="bn1"><div class="m1"><p>شاها چو تو مادر زمان زاید نی</p></div>
<div class="m2"><p>بخشد چو تو هیچ شاه و بخشاید نی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا حشر چو تیغ و تازیانه‌ات پس از این</p></div>
<div class="m2"><p>یک ملک‌ستان و ملک‌بخش آید نی</p></div></div>