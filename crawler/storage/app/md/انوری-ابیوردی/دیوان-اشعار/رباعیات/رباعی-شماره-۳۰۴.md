---
title: >-
    رباعی شمارهٔ ۳۰۴
---
# رباعی شمارهٔ ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>در کوی غمت هزار منزل دارم</p></div>
<div class="m2"><p>وز دست تو پای صبر در گل دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در راه تو کار سخت مشکل دارم</p></div>
<div class="m2"><p>دل نیست پدید و صد غم دل دارم</p></div></div>