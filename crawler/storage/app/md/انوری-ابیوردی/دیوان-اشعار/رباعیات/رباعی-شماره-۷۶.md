---
title: >-
    رباعی شمارهٔ ۷۶
---
# رباعی شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>گر بنده دو روز خدمتت را بگذاشت</p></div>
<div class="m2"><p>نه نقش عیادت تو بر آب نگاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تقصیر از آن کرد که چشمی که بدان</p></div>
<div class="m2"><p>بیماری چون تویی توان دید نداشت</p></div></div>