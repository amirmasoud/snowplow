---
title: >-
    رباعی شمارهٔ ۳۷۰
---
# رباعی شمارهٔ ۳۷۰

<div class="b" id="bn1"><div class="m1"><p>دست تو که جود در سجود آید ازو</p></div>
<div class="m2"><p>سرمایهٔ نزهت وجود آید ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستارچه‌ای که یک دمش خدمت کرد</p></div>
<div class="m2"><p>تا نیست نگشت بوی عود آید ازو</p></div></div>