---
title: >-
    رباعی شمارهٔ ۴۱۹
---
# رباعی شمارهٔ ۴۱۹

<div class="b" id="bn1"><div class="m1"><p>دی درویشی به راز با همنفسی</p></div>
<div class="m2"><p>می‌گفت کریم در جهان مانده کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گوشهٔ چرخ هاتفی گفت خموش</p></div>
<div class="m2"><p>بوطالب نعمه را بقا باد بسی</p></div></div>