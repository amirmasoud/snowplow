---
title: >-
    رباعی شمارهٔ ۲۲۲
---
# رباعی شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>گویی که میفکن دبه در پای شتر</p></div>
<div class="m2"><p>تا من چو خران همی جهم بر آخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نه زندت صلاح قواد پسر</p></div>
<div class="m2"><p>من بر ... این سخن زنم ... ی پر</p></div></div>