---
title: >-
    رباعی شمارهٔ ۳۷۴
---
# رباعی شمارهٔ ۳۷۴

<div class="b" id="bn1"><div class="m1"><p>چون باز کنی ز زلف پرتاب گره</p></div>
<div class="m2"><p>احسنت کند چرخ و فلک گوید زه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر چشم جهانیان نگارا که و مه</p></div>
<div class="m2"><p>هر روز نکوتری و هر ساعت به</p></div></div>