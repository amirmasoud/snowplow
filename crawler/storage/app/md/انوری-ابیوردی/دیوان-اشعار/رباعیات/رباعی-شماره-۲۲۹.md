---
title: >-
    رباعی شمارهٔ ۲۲۹
---
# رباعی شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>ما با این همه غم با که گساریم آخر</p></div>
<div class="m2"><p>وین غصه دمی با که برآریم آخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس نیست که با او نفسی بتوان زد</p></div>
<div class="m2"><p>تنها همه عمر چون گذاریم آخر</p></div></div>