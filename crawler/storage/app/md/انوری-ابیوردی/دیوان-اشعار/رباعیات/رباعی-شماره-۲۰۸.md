---
title: >-
    رباعی شمارهٔ ۲۰۸
---
# رباعی شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>جان یک نفس از درد تو می‌ناساید</p></div>
<div class="m2"><p>وز دل نفسی بی‌تو همی برناید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکبار دگر وصل تو درمی‌باید</p></div>
<div class="m2"><p>وانگه پس از آن اگر نمانم شاید</p></div></div>