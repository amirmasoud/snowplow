---
title: >-
    رباعی شمارهٔ ۲۶۸
---
# رباعی شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>تا دست طمع بشستم از عالم خاک</p></div>
<div class="m2"><p>از گرد زمانه دامنی دارم پاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امید بقا یکی شد و بیم هلاک</p></div>
<div class="m2"><p>چون من ز جهان برفتم از مرگ چه باک</p></div></div>