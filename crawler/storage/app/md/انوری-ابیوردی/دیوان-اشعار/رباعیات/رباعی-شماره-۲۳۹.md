---
title: >-
    رباعی شمارهٔ ۲۳۹
---
# رباعی شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>جز بنده رفیق و عاشق و یار مگیر</p></div>
<div class="m2"><p>غمخوار توام عمر مرا خوار مگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کار تو کارم ار به جان یابد دست</p></div>
<div class="m2"><p>تو پای به کار برمنه کار مگیر</p></div></div>