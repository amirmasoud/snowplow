---
title: >-
    رباعی شمارهٔ ۲۷۱
---
# رباعی شمارهٔ ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>در منزل آبگینه هنگام درنگ</p></div>
<div class="m2"><p>چون بی‌تو دل شکسته را دیدم تنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که چگونه‌ای دلا گفت مپرس</p></div>
<div class="m2"><p>چونانک در آبگینه اندازی سنگ</p></div></div>