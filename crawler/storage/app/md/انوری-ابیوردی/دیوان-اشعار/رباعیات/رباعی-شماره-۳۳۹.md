---
title: >-
    رباعی شمارهٔ ۳۳۹
---
# رباعی شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>باغیست چو نوبهار از رنگ خزان</p></div>
<div class="m2"><p>عیشی که به عمرها توان گفت از آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاران همه انگشت زنان گرد رزان</p></div>
<div class="m2"><p>من در غم تو نشسته انگشت‌گزان</p></div></div>