---
title: >-
    رباعی شمارهٔ ۲۵۴
---
# رباعی شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>در منزل دل غم تو می‌آید و بس</p></div>
<div class="m2"><p>در سکنهٔ جان غم تو می‌باید و بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا صبح جمال فتنه‌زای تو دمید</p></div>
<div class="m2"><p>گویی که ز شب غم تو می‌زاید و بس</p></div></div>