---
title: >-
    رباعی شمارهٔ ۲۷۵
---
# رباعی شمارهٔ ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>آمیختم از بهر تو صد رنگ و حیل</p></div>
<div class="m2"><p>هم دست اجل قوی‌تر آمد به جدل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جان مرا قبول کردی به مثل</p></div>
<div class="m2"><p>پیش از اجلش کشیدمی پیش اجل</p></div></div>