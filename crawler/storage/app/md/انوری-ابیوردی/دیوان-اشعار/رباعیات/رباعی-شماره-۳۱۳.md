---
title: >-
    رباعی شمارهٔ ۳۱۳
---
# رباعی شمارهٔ ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>بینم دل خویش گر دهانت اندیشم</p></div>
<div class="m2"><p>یابم تن خویش گر میانت اندیشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یادم ناید ز سر به جان و سر تو</p></div>
<div class="m2"><p>الا که ز خاک آستانت اندیشم</p></div></div>