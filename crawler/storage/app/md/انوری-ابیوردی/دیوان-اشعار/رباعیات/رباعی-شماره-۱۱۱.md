---
title: >-
    رباعی شمارهٔ ۱۱۱
---
# رباعی شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>مریخ سلاح چاوشان تو برد</p></div>
<div class="m2"><p>گوی تو زحل به پاسبانی سپرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ملکت تو چه بیش و کم خواهد شد</p></div>
<div class="m2"><p>گر چاوش تو به پاسبان برگذرد</p></div></div>