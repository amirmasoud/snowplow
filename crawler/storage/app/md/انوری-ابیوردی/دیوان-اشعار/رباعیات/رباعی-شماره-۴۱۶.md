---
title: >-
    رباعی شمارهٔ ۴۱۶
---
# رباعی شمارهٔ ۴۱۶

<div class="b" id="bn1"><div class="m1"><p>بر جان منت نیست دمی دلسوزی</p></div>
<div class="m2"><p>بر وصل توام نیست شبی پیروزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق کسی بود بدین بد روزی</p></div>
<div class="m2"><p>وای من مستمند هجران روزی</p></div></div>