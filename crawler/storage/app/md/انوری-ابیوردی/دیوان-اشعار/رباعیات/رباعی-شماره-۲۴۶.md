---
title: >-
    رباعی شمارهٔ ۲۴۶
---
# رباعی شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>ای دل بخریدی دم آن شمع طراز</p></div>
<div class="m2"><p>وی دیده حدیث گریه کردی آغاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای عشق کهن ناشده نو کردی دست</p></div>
<div class="m2"><p>وی محنت ناگذشته آوردی باز</p></div></div>