---
title: >-
    رباعی شمارهٔ ۵۷
---
# رباعی شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>آن چیست که مقصود جهانی آنست</p></div>
<div class="m2"><p>آن طرفه که از جهانیان پنهانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دانش عقل و جان و تن حیرانست</p></div>
<div class="m2"><p>آن به که چنان بود که بتوان دانست</p></div></div>