---
title: >-
    رباعی شمارهٔ ۱۰۵
---
# رباعی شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>با هرکه زبان چرخ رازی بگشاد</p></div>
<div class="m2"><p>چون پای نداشت پای تا سر بنهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان داد سخن همی بنتوانم داد</p></div>
<div class="m2"><p>کابستن رازهابنتواند زاد</p></div></div>