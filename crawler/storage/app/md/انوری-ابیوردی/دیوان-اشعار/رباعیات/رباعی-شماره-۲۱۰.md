---
title: >-
    رباعی شمارهٔ ۲۱۰
---
# رباعی شمارهٔ ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>بس راه که پای همتم پیماید</p></div>
<div class="m2"><p>تا مشکل یک راز فلک بگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس روز سیه که از غلط پیش آید</p></div>
<div class="m2"><p>تا از شب شک صبح یقینی زاید</p></div></div>