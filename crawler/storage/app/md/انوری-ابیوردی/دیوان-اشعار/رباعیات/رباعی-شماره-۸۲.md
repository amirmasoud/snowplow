---
title: >-
    رباعی شمارهٔ ۸۲
---
# رباعی شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>با دل گفتم که آن بتم دوش نهفت</p></div>
<div class="m2"><p>جان خواست ز من چون گل وصلش بشکفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل گفت مضایقت مکن زود بده</p></div>
<div class="m2"><p>با او به محقری سخن نتوان گفت</p></div></div>