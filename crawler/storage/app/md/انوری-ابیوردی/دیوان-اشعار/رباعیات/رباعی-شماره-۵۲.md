---
title: >-
    رباعی شمارهٔ ۵۲
---
# رباعی شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>چون حسن تو رنج من به عالم سمرست</p></div>
<div class="m2"><p>کارم چو سر زلف تو زیر و زبرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدم ز غمت بسی جفاها لیکن</p></div>
<div class="m2"><p>نادیدن تو ز هرچه دیدم بترست</p></div></div>