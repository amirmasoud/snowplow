---
title: >-
    رباعی شمارهٔ ۲۸۲
---
# رباعی شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>صف زد حشم بهار پیرامن گل</p></div>
<div class="m2"><p>ابر آمد و پر کرد ز در دامن گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این همه جان نماند اندر تن گل</p></div>
<div class="m2"><p>گر تو به چمن درآیی ای خرمن گل</p></div></div>