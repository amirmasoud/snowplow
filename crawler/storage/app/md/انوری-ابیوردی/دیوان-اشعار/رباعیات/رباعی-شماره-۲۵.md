---
title: >-
    رباعی شمارهٔ ۲۵
---
# رباعی شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>جانا به تن شکسته و عزم درست</p></div>
<div class="m2"><p>عمریست که دل در طلب صحبت تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وامروز که نومید شد از وصل تو چست</p></div>
<div class="m2"><p>در صبر زد آن دست کز امید بشست</p></div></div>