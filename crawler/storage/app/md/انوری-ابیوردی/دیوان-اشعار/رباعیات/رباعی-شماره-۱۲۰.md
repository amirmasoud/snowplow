---
title: >-
    رباعی شمارهٔ ۱۲۰
---
# رباعی شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>گر یک شبه وصل بتم آواز آرد</p></div>
<div class="m2"><p>یکساله فراقش فلک آغاز آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد روز ارین که می‌گذارم بدهم</p></div>
<div class="m2"><p>گر دور فلک از آن شبی باز آرد</p></div></div>