---
title: >-
    رباعی شمارهٔ ۶۶
---
# رباعی شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>ای شاه نجیب کفشگر دانی کیست</p></div>
<div class="m2"><p>آنکس که ازو خزینت از مال تهیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیمت ز کل حبه طلب ورنه ازو</p></div>
<div class="m2"><p>سگ داند و کفشگر که در انبان چیست</p></div></div>