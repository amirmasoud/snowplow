---
title: >-
    رباعی شمارهٔ ۲۴۱
---
# رباعی شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>ای دست تو در جفا چو زلف تو دراز</p></div>
<div class="m2"><p>وی بی‌سببی گرفته پای از من باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی دست زاستین برون کرده به عهد</p></div>
<div class="m2"><p>وامروز کشیده پای در دامن ناز</p></div></div>