---
title: >-
    رباعی شمارهٔ ۲۸
---
# رباعی شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>کار تنم از دست دلم رفت ز دست</p></div>
<div class="m2"><p>بیچاره دلم به ماتم جان بنشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان دل ز جهان بربد و رخت اندر بست</p></div>
<div class="m2"><p>سازم همه این بود که در کار شکست</p></div></div>