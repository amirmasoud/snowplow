---
title: >-
    رباعی شمارهٔ ۵۶
---
# رباعی شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>دل در هوس شراب گلرنگ خوشست</p></div>
<div class="m2"><p>با بربط و با نای و دف و چنگ خوشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی ز کس فراخ نیکو نبود</p></div>
<div class="m2"><p>روزی فراخم از در تنگ خوشست</p></div></div>