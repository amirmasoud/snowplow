---
title: >-
    رباعی شمارهٔ ۴۰۹
---
# رباعی شمارهٔ ۴۰۹

<div class="b" id="bn1"><div class="m1"><p>مسعود قزل مست نه‌ای هشیاری</p></div>
<div class="m2"><p>یک دم چه بود که مطربی بگذاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زر بستانی ازارکی برداری</p></div>
<div class="m2"><p>ما را گل و باقلی و ریواس آری</p></div></div>