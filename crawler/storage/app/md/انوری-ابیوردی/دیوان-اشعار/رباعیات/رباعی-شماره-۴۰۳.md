---
title: >-
    رباعی شمارهٔ ۴۰۳
---
# رباعی شمارهٔ ۴۰۳

<div class="b" id="bn1"><div class="m1"><p>هر شب بت من به وقت باد سحری</p></div>
<div class="m2"><p>دل باز فرستدم به صاحب خبری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل با همه بی‌رحمی و بیدادگری</p></div>
<div class="m2"><p>آید بر من نشیند و زارگری</p></div></div>