---
title: >-
    رباعی شمارهٔ ۱۲۷
---
# رباعی شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>با آنکه غم عشق تو از من جان برد</p></div>
<div class="m2"><p>وان جان به هزار درد بی‌درمان برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دسترسی بود مرا در غم تو</p></div>
<div class="m2"><p>انگشت به هیچ شادیی نتوان برد</p></div></div>