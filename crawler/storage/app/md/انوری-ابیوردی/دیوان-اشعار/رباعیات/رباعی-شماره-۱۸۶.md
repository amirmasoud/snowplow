---
title: >-
    رباعی شمارهٔ ۱۸۶
---
# رباعی شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>گردون چو نشست و خاست تو می‌بیند</p></div>
<div class="m2"><p>با خلق همان شیوه چرا نگزیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بنشینی باد سخا برخیزد</p></div>
<div class="m2"><p>چون برخیزی گرد ستم بنشیند</p></div></div>