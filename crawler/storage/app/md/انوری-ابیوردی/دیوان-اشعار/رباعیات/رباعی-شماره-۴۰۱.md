---
title: >-
    رباعی شمارهٔ ۴۰۱
---
# رباعی شمارهٔ ۴۰۱

<div class="b" id="bn1"><div class="m1"><p>دیروز که در سرای عالی بودی</p></div>
<div class="m2"><p>رمزی گفتی اشارتی فرمودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر هست بده ورنه در آن بند مباش</p></div>
<div class="m2"><p>انگار که از من این سخن نشنودی</p></div></div>