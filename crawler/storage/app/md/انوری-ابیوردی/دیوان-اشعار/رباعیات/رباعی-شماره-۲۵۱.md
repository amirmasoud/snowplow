---
title: >-
    رباعی شمارهٔ ۲۵۱
---
# رباعی شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>ای ماه ز سودای تو در آتش تیز</p></div>
<div class="m2"><p>چون سوخته گشتم آبرویم بمریز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون چرخ ستیزه‌روی با من مستیز</p></div>
<div class="m2"><p>من در تو گریختم تو از من مگریز</p></div></div>