---
title: >-
    رباعی شمارهٔ ۲۷
---
# رباعی شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>با موزه به آب در دویدی به نخست</p></div>
<div class="m2"><p>تا خرمن من به باد بردادی چست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون تیز شد آتش دلم گشتی سست</p></div>
<div class="m2"><p>خاکش بر سر که او نه خاک در تست</p></div></div>