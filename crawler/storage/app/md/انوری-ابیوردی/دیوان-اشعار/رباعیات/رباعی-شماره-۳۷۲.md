---
title: >-
    رباعی شمارهٔ ۳۷۲
---
# رباعی شمارهٔ ۳۷۲

<div class="b" id="bn1"><div class="m1"><p>آن بت که به دست غم گرفتارم ازو</p></div>
<div class="m2"><p>وز دست همی درگذرد کارم ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیزار شدست از من و من زارم ازو</p></div>
<div class="m2"><p>دل نی و هزار درد دل دارم ازو</p></div></div>