---
title: >-
    رباعی شمارهٔ ۱۴۳
---
# رباعی شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>از عشق تو درجهان سمر خواهم شد</p></div>
<div class="m2"><p>وز دست غمت زیر و زبر خواهم شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانگه زپس هزار شب بی‌خوابی</p></div>
<div class="m2"><p>گریان گریان به خواب درخواهم شد</p></div></div>