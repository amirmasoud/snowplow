---
title: >-
    رباعی شمارهٔ ۳۸۵
---
# رباعی شمارهٔ ۳۸۵

<div class="b" id="bn1"><div class="m1"><p>ای فتنهٔ روزگار شب‌پوش منه</p></div>
<div class="m2"><p>و ابدالان را غاشیه بر دوش منه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلفی که هزار جان ازو در خطرست</p></div>
<div class="m2"><p>از چشم بدان بترس و برگوش منه</p></div></div>