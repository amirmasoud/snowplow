---
title: >-
    رباعی شمارهٔ ۳۸۷
---
# رباعی شمارهٔ ۳۸۷

<div class="b" id="bn1"><div class="m1"><p>پایی که مرا نزد تو بد راهنمای</p></div>
<div class="m2"><p>دستی که بدان خواستمت من ز خدای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن پای مرا چنین بیفکند از دست</p></div>
<div class="m2"><p>وآن دست مرا چنین درآورد ز پای</p></div></div>