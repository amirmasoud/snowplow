---
title: >-
    رباعی شمارهٔ ۳۳۸
---
# رباعی شمارهٔ ۳۳۸

<div class="b" id="bn1"><div class="m1"><p>ماییم و صراحی و شراب روشن</p></div>
<div class="m2"><p>مرغی دو و نان چند و زیشان دو سه تن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز میوه و ریحان قدری سیب و سمن</p></div>
<div class="m2"><p>برخیز و بیا چنانک دی نزد تو من</p></div></div>