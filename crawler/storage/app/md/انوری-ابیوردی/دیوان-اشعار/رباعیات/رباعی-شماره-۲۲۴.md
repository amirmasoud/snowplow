---
title: >-
    رباعی شمارهٔ ۲۲۴
---
# رباعی شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>ای عشق به جز غمم رفیقی دگر آر</p></div>
<div class="m2"><p>وی وصل غرض تویی سر از پیش برآر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وی هجر بگفته‌ای بریزم خونت</p></div>
<div class="m2"><p>گر وقت آمد بریز و عمرم به سر آر</p></div></div>