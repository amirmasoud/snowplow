---
title: >-
    رباعی شمارهٔ ۳۴۴
---
# رباعی شمارهٔ ۳۴۴

<div class="b" id="bn1"><div class="m1"><p>چون روی حیل نبود پایاب جهان</p></div>
<div class="m2"><p>یکباره ورق بشستم از تاب جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم چو مقیم نیست اسباب جهان</p></div>
<div class="m2"><p>خاکش بر سر که خوش خورد آب جهان</p></div></div>