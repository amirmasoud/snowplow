---
title: >-
    رباعی شمارهٔ ۱۴۴
---
# رباعی شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>رای تو به هیچ رای خرسند نشد</p></div>
<div class="m2"><p>تا بر همه خسروان خداوند نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رایات تو از پای‌فلک بنشیند</p></div>
<div class="m2"><p>تا ملک خراسان چو سمرقند نشد</p></div></div>