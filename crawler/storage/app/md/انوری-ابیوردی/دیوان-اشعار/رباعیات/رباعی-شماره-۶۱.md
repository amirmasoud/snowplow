---
title: >-
    رباعی شمارهٔ ۶۱
---
# رباعی شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>گر شرح نمی‌دهم که حالم چونست</p></div>
<div class="m2"><p>یا از تو مرا چه درد روزافزونست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیداست چو روز نزد هرکس که مرا</p></div>
<div class="m2"><p>با این لب خندان چه دل پر خونست</p></div></div>