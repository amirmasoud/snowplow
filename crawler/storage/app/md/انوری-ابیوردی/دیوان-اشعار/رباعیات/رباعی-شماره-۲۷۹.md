---
title: >-
    رباعی شمارهٔ ۲۷۹
---
# رباعی شمارهٔ ۲۷۹

<div class="b" id="bn1"><div class="m1"><p>منزل دوردست و روز بی‌گاه ای دل</p></div>
<div class="m2"><p>زین رو مکش انتظار همراه ای دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشتاب که منقطع فراوان هستند</p></div>
<div class="m2"><p>زین راه دراز و روز کوتاه ای دل</p></div></div>