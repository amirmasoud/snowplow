---
title: >-
    رباعی شمارهٔ ۲۹۷
---
# رباعی شمارهٔ ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>روزی که به حیلت به شب تیره برم</p></div>
<div class="m2"><p>می‌گویم شکر و باز پس می‌نگرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنگر که ز عمر در چه خون جگرم</p></div>
<div class="m2"><p>تا روز گذشته را غنیمت شمرم</p></div></div>