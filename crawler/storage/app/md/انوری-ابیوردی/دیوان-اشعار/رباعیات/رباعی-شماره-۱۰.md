---
title: >-
    رباعی شمارهٔ ۱۰
---
# رباعی شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>بوطالب نعمه ای سپهرت طالب</p></div>
<div class="m2"><p>بر تابش آفتاب رایت غالب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دور زمانه یادگاری نگذاشت</p></div>
<div class="m2"><p>بهتر ز تو گوهری علی بوطالب</p></div></div>