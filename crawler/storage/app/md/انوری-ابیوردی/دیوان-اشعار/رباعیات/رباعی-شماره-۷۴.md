---
title: >-
    رباعی شمارهٔ ۷۴
---
# رباعی شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>گر درخور قدر همتم سیمی نیست</p></div>
<div class="m2"><p>چون من به هنر کس اندر اقلیمی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیبی نبود گر فلکم سیم نداد</p></div>
<div class="m2"><p>چونان که ز نان استدنم بیمی نیست</p></div></div>