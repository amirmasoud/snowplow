---
title: >-
    رباعی شمارهٔ ۱۲۹
---
# رباعی شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>موری که به چاه شست بازی گذرد</p></div>
<div class="m2"><p>بی‌تو شب من بدان درازی گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان شب که مرا با تو به بازی گذرد</p></div>
<div class="m2"><p>گویی که همی بر اسب تازی گذرد</p></div></div>