---
title: >-
    رباعی شمارهٔ ۳۵۶
---
# رباعی شمارهٔ ۳۵۶

<div class="b" id="bn1"><div class="m1"><p>چشمم ز همه جهان فرازست اکنون</p></div>
<div class="m2"><p>وین دیده به دیدار تو بازست اکنون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتار همه جهان مجازست اکنون</p></div>
<div class="m2"><p>ما را به جمال تو نیازست اکنون</p></div></div>