---
title: >-
    رباعی شمارهٔ ۴۰۵
---
# رباعی شمارهٔ ۴۰۵

<div class="b" id="bn1"><div class="m1"><p>ای شب چو ز نالهای من بی‌خبری</p></div>
<div class="m2"><p>بر خیره کنون چند کنم نوحه‌گری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای روز سپید وقت نامد که مرا</p></div>
<div class="m2"><p>از صحبت این شب سیه باز خری</p></div></div>