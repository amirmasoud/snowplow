---
title: >-
    رباعی شمارهٔ ۹۱
---
# رباعی شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>آن بت که به انصاف نکو بود برفت</p></div>
<div class="m2"><p>حورا صفت و فرشته‌خو بود برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسایش عمرم همه او داشت ببرد</p></div>
<div class="m2"><p>آرایش جانم همه او بود برفت</p></div></div>