---
title: >-
    رباعی شمارهٔ ۳۵۰
---
# رباعی شمارهٔ ۳۵۰

<div class="b" id="bn1"><div class="m1"><p>ای دل ز سر نهاد پرواز مکن</p></div>
<div class="m2"><p>فرجام نگر حدیث آغاز مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک از سر این راز نهان باز مکن</p></div>
<div class="m2"><p>خود را و مرا در سر این راز مکن</p></div></div>