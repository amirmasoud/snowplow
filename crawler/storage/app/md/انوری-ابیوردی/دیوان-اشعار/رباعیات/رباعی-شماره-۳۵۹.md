---
title: >-
    رباعی شمارهٔ ۳۵۹
---
# رباعی شمارهٔ ۳۵۹

<div class="b" id="bn1"><div class="m1"><p>شاهان ممالک تو مودود و معین</p></div>
<div class="m2"><p>دارند خزانها نهان در ثمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوهر که همین بر سر گنجست و همین</p></div>
<div class="m2"><p>باهر که همان از در تیغست و همین</p></div></div>