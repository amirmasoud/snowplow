---
title: >-
    رباعی شمارهٔ ۳۳۳
---
# رباعی شمارهٔ ۳۳۳

<div class="b" id="bn1"><div class="m1"><p>دی یک دو قدح شراب صافی خوردیم</p></div>
<div class="m2"><p>با همنفسی شبی به روز آوردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز چنان شد که به ناچار دو دست</p></div>
<div class="m2"><p>در گردن درد و رنج و هجران کردیم</p></div></div>