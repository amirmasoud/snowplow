---
title: >-
    رباعی شمارهٔ ۹۹
---
# رباعی شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>تا روز به شب چو سوسنم بی‌رویت</p></div>
<div class="m2"><p>بیدار چو نرگسم به گرد کویت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون لاله شوم سوخته‌دل گر بنهم</p></div>
<div class="m2"><p>مانند گل دو رویه رو بر رویت</p></div></div>