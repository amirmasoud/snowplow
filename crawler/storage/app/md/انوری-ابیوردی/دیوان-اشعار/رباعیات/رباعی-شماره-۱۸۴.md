---
title: >-
    رباعی شمارهٔ ۱۸۴
---
# رباعی شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>این طایفه گر مروت آیین نکنند</p></div>
<div class="m2"><p>زیشان نه بس اینکه بخل را دین نکنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت آنکه به نظم و شعر احسان کردی</p></div>
<div class="m2"><p>امروز همی به سحر تحسین نکنند</p></div></div>