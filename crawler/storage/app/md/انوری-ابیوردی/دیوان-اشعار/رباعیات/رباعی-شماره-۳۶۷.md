---
title: >-
    رباعی شمارهٔ ۳۶۷
---
# رباعی شمارهٔ ۳۶۷

<div class="b" id="bn1"><div class="m1"><p>آن صبر که حامی منست از غم تو</p></div>
<div class="m2"><p>مویی نبرد ز عهد نامحکم تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وین وصل که قبله‌ایست در عالم عشق</p></div>
<div class="m2"><p>از گمشدگان یکیست در عالم تو</p></div></div>