---
title: >-
    رباعی شمارهٔ ۳۶۸
---
# رباعی شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>دورم ز قرار و خواب از دوری تو</p></div>
<div class="m2"><p>وز پرده برون شدم به مستوری تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی که کراست برگ مهجوری من</p></div>
<div class="m2"><p>انگشت به خود کشم به دستوری تو</p></div></div>