---
title: >-
    رباعی شمارهٔ ۴۱۷
---
# رباعی شمارهٔ ۴۱۷

<div class="b" id="bn1"><div class="m1"><p>هرکو به مواظبت بخواند چیزی</p></div>
<div class="m2"><p>با او به همه حال بماند چیزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر پس از آن، از آن به چیزی برسد</p></div>
<div class="m2"><p>چیزی نبود هر که نداند چیزی</p></div></div>