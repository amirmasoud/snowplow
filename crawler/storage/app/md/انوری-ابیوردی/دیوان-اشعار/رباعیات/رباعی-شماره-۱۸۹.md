---
title: >-
    رباعی شمارهٔ ۱۸۹
---
# رباعی شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>دستت به سخا چون ید بیضا بنمود</p></div>
<div class="m2"><p>از جود تو در جهان جهانی بفزود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس چون تو سخی نه هست نه خواهد بود</p></div>
<div class="m2"><p>گو قافیه دال شو زهی عالم جود</p></div></div>