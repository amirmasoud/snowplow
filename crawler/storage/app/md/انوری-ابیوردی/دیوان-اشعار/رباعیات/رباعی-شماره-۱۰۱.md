---
title: >-
    رباعی شمارهٔ ۱۰۱
---
# رباعی شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>عمری جگرم خورد ز بدخویی چرخ</p></div>
<div class="m2"><p>یک روز نرفت راه دلجویی چرخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آورد و به دست جور مریخم داد</p></div>
<div class="m2"><p>با زهره گرفتست مرا گویی چرخ</p></div></div>