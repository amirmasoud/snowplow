---
title: >-
    رباعی شمارهٔ ۲۹۶
---
# رباعی شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>دی کرد وداع بر جناح سفرم</p></div>
<div class="m2"><p>تا دست فراق کرد زیر و زبرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او می‌شد و جان نعره همی زد ز پی‌اش</p></div>
<div class="m2"><p>آهسته ترک تاز که من بر اثرم</p></div></div>