---
title: >-
    رباعی شمارهٔ ۵۱
---
# رباعی شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>فرمان تو بر جهان قضای دگرست</p></div>
<div class="m2"><p>کلک تو گره‌گشای بند قدرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نامه که در نظم امور بشرست</p></div>
<div class="m2"><p>توقیع برو ابوالمعالی عمرست</p></div></div>