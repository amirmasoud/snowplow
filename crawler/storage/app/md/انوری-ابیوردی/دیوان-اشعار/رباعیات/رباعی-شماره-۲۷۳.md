---
title: >-
    رباعی شمارهٔ ۲۷۳
---
# رباعی شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>ای گوهر تو خلاصهٔ عالم گل</p></div>
<div class="m2"><p>باد از تو دو قوم را دو معنی حاصل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آب نکوخواه ترا حکم روان</p></div>
<div class="m2"><p>چون لوله بداندیش ترا سوخته‌دل</p></div></div>