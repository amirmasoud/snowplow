---
title: >-
    رباعی شمارهٔ ۳۴۵
---
# رباعی شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>آیا گهر وصل تو یارم سفتن</p></div>
<div class="m2"><p>راه تو امیدوار یارم رفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌روشن و حجره خالی و موسم گل</p></div>
<div class="m2"><p>ای گلبن نو شکفته یارم گفتن</p></div></div>