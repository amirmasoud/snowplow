---
title: >-
    رباعی شمارهٔ ۸۳
---
# رباعی شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>با گل گفتم شکوفه در خاک بخفت</p></div>
<div class="m2"><p>گل دیده پر آب کرد از باران گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آری نتوان گرفت با گیتی جفت</p></div>
<div class="m2"><p>بنمای گلی که ریختن را نشکفت</p></div></div>