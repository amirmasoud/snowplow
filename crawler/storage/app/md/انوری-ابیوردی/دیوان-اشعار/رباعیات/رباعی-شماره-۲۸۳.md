---
title: >-
    رباعی شمارهٔ ۲۸۳
---
# رباعی شمارهٔ ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>تاب رخ یار من نداری ای گل</p></div>
<div class="m2"><p>جامه چه دری رنگ چه آری ای گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودت نکند تا که به خواری ای گل</p></div>
<div class="m2"><p>از بار خجل فرو نیاری ای گل</p></div></div>