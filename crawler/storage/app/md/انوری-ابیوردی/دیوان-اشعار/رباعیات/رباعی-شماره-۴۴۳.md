---
title: >-
    رباعی شمارهٔ ۴۴۳
---
# رباعی شمارهٔ ۴۴۳

<div class="b" id="bn1"><div class="m1"><p>صورت‌گر فطرت ننگارد چو تویی</p></div>
<div class="m2"><p>دوران فلک برون نیارد چو تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند همه جهان تو داری لیکن</p></div>
<div class="m2"><p>ای صدر جهان جهان ندارد چو تویی</p></div></div>