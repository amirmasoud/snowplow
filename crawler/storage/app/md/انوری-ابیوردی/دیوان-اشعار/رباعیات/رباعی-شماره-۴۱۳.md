---
title: >-
    رباعی شمارهٔ ۴۱۳
---
# رباعی شمارهٔ ۴۱۳

<div class="b" id="bn1"><div class="m1"><p>در بنده به دیدهٔ دگر می‌نگری</p></div>
<div class="m2"><p>با این همه خوش دلم چو درمی‌نگری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روز سپس ترست کارم با تو</p></div>
<div class="m2"><p>در من نه به چشم پیشتر می‌نگری</p></div></div>