---
title: >-
    رباعی شمارهٔ ۸۸
---
# رباعی شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>معشوق مرا عهد من از یاد برفت</p></div>
<div class="m2"><p>وان عهد و وفا به باد برداد و برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پایم به حیل ببست و آزاد برفت</p></div>
<div class="m2"><p>آتش به من اندر زد و چون باد برفت</p></div></div>