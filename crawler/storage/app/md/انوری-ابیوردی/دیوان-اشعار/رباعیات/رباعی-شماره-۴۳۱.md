---
title: >-
    رباعی شمارهٔ ۴۳۱
---
# رباعی شمارهٔ ۴۳۱

<div class="b" id="bn1"><div class="m1"><p>ای دل طمعم زان همه سرگردانی</p></div>
<div class="m2"><p>نومیدی و درد بود و بی‌درمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این کار نه بر امید آن می‌کردم</p></div>
<div class="m2"><p>باری تو که در میان کاری دانی</p></div></div>