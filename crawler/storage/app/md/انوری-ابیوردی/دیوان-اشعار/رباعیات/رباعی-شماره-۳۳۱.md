---
title: >-
    رباعی شمارهٔ ۳۳۱
---
# رباعی شمارهٔ ۳۳۱

<div class="b" id="bn1"><div class="m1"><p>چون پای همی تحفه برد هر جایم</p></div>
<div class="m2"><p>وز پای به پای آمدنی می‌آیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستم شکند فلک من این را شایم</p></div>
<div class="m2"><p>آری چو گزیز نیست باری پایم</p></div></div>