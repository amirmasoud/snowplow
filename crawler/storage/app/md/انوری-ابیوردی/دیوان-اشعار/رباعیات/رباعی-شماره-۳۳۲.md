---
title: >-
    رباعی شمارهٔ ۳۳۲
---
# رباعی شمارهٔ ۳۳۲

<div class="b" id="bn1"><div class="m1"><p>ای عشق در آفاق بسی تاختیم</p></div>
<div class="m2"><p>تا از دل و دلدار برانداختیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر حق صحبتی که با تست مرا</p></div>
<div class="m2"><p>بشناس و همان گیر که نشناختیم</p></div></div>