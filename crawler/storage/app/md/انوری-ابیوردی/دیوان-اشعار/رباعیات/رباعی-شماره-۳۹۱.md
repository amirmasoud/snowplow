---
title: >-
    رباعی شمارهٔ ۳۹۱
---
# رباعی شمارهٔ ۳۹۱

<div class="b" id="bn1"><div class="m1"><p>عمزاد و عمزاد خریدند بری</p></div>
<div class="m2"><p>عمزادگکی قدیمشان اندر پی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینک چو دو نوبهار بین با یک دی</p></div>
<div class="m2"><p>عمزاد همی رود دو عمزاد ز پی</p></div></div>