---
title: >-
    رباعی شمارهٔ ۳۴۷
---
# رباعی شمارهٔ ۳۴۷

<div class="b" id="bn1"><div class="m1"><p>زرق است جهان تو زرق کن از هر فن</p></div>
<div class="m2"><p>که می‌خور و که می‌کن و لوتی می‌زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش خور تو جهان و یاد می‌آر از من</p></div>
<div class="m2"><p>تا روزی چند جمله را سر کن زن</p></div></div>