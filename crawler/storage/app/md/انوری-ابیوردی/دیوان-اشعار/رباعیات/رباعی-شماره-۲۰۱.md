---
title: >-
    رباعی شمارهٔ ۲۰۱
---
# رباعی شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>آنرا که خرد مصلحت‌آموز شود</p></div>
<div class="m2"><p>کی در غم عید و بند نوروز شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیدی شمرد که روز نوروز شود</p></div>
<div class="m2"><p>هر شب به عافیت بر او روز شود</p></div></div>