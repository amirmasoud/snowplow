---
title: >-
    رباعی شمارهٔ ۳۹
---
# رباعی شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>عدل تو زمانه را نگهدار بس است</p></div>
<div class="m2"><p>تایید تو دین و ملک را یار بس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کار جهان کلک تو می‌دارد راست</p></div>
<div class="m2"><p>تا هست جهان کلک تو بر کار بس است</p></div></div>