---
title: >-
    رباعی شمارهٔ ۱۵۶
---
# رباعی شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>آن روز که جان نامهٔ عشق تو بخواند</p></div>
<div class="m2"><p>دل دست زجان بشست و دامن بفشاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان صبر که خادمت بدان آسودی</p></div>
<div class="m2"><p>آن نیز بقای عمر تو باد نماند</p></div></div>