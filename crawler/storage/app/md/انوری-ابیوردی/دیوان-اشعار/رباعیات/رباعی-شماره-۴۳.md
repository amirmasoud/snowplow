---
title: >-
    رباعی شمارهٔ ۴۳
---
# رباعی شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>ای شاه جهان ملک جهان حسب تراست</p></div>
<div class="m2"><p>وز دولت و اقبال شهی کسب تراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز به یک حمله هزار اسب بگیر</p></div>
<div class="m2"><p>فردا خوارزم و صدهزار اسب تراست</p></div></div>