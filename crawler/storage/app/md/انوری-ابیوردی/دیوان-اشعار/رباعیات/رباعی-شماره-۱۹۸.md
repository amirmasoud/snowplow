---
title: >-
    رباعی شمارهٔ ۱۹۸
---
# رباعی شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>هر کو نه به خدمت تو خرسند شود</p></div>
<div class="m2"><p>آفاق برو حبس و زمین بند شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان را که به بندگی پذیری یک روز</p></div>
<div class="m2"><p>شب را به همه حال خداوند شود</p></div></div>