---
title: >-
    رباعی شمارهٔ ۲۲۸
---
# رباعی شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>بر من شب هجر تو سرآید آخر</p></div>
<div class="m2"><p>این صبح وصال تو برآید آخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستی که ز هجران تو بر سر دارم</p></div>
<div class="m2"><p>از وصل به گردنت درآید آخر</p></div></div>