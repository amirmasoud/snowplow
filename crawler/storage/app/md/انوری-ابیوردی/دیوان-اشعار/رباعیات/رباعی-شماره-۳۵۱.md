---
title: >-
    رباعی شمارهٔ ۳۵۱
---
# رباعی شمارهٔ ۳۵۱

<div class="b" id="bn1"><div class="m1"><p>جانا لبم از شراب غم خشک مکن</p></div>
<div class="m2"><p>چشمم ز سرشک هیچ دم خشک مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق گران رکاب صبری داری</p></div>
<div class="m2"><p>زنهار نمد زین ستم خشک مکن</p></div></div>