---
title: >-
    رباعی شمارهٔ ۱۹۶
---
# رباعی شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>دل درخور صحبت دل‌افروز نبود</p></div>
<div class="m2"><p>زان بر من مستمند دلسوز نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان شب که برفت و گفت خوش‌باد شبت</p></div>
<div class="m2"><p>هرگز شب محنت مرا روز نبود</p></div></div>