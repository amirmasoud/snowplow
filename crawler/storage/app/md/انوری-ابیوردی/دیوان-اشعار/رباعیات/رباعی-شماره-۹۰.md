---
title: >-
    رباعی شمارهٔ ۹۰
---
# رباعی شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>دلبر چو دلم به عشوه بربود برفت</p></div>
<div class="m2"><p>غمهای مرا به غمزه بفزود برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس دیر به دست آمد و بس زود برفت</p></div>
<div class="m2"><p>آتش به من اندر زد و چون دود برفت</p></div></div>