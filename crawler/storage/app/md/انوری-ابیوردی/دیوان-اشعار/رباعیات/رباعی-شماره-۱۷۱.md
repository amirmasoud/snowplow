---
title: >-
    رباعی شمارهٔ ۱۷۱
---
# رباعی شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>یاران به جهان چشم چو گل بگشادند</p></div>
<div class="m2"><p>هر یک دو سه روز رنگ و بویی دادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون راست که بر بهار دل بنهادند</p></div>
<div class="m2"><p>ازبار یگان یگان فرو افتادند</p></div></div>