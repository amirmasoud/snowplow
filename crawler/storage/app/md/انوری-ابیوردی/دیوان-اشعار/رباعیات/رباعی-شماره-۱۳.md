---
title: >-
    رباعی شمارهٔ ۱۳
---
# رباعی شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>بس شب که به روز بردم اندر طلبت</p></div>
<div class="m2"><p>بس روز طرب که دیدم از وصل لبت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتی و کنون روز و شب این می‌گویم</p></div>
<div class="m2"><p>کای روز وصال یار خوش باد شبت</p></div></div>