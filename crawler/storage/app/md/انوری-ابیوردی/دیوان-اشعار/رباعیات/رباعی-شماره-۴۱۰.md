---
title: >-
    رباعی شمارهٔ ۴۱۰
---
# رباعی شمارهٔ ۴۱۰

<div class="b" id="bn1"><div class="m1"><p>گفتی که به هر قطعه مرا هر باری</p></div>
<div class="m2"><p>از خواجه به تازگی برآید کاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوران شماست ای برادر آری</p></div>
<div class="m2"><p>ما را به سه چار و پنج خدمت داری</p></div></div>