---
title: >-
    رباعی شمارهٔ ۱۱
---
# رباعی شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>هرچند که بر جزو بود کل غالب</p></div>
<div class="m2"><p>باشد همه جزو کل خود را طالب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جزویست که کل خویش را ماند راست</p></div>
<div class="m2"><p>بوطالب نعمه از علی بوطالب</p></div></div>