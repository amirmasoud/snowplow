---
title: >-
    رباعی شمارهٔ ۹۸
---
# رباعی شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>ای روزی خصم پیش خورد حشمت</p></div>
<div class="m2"><p>جزویست قیامت از نبرد حشمت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندیشهٔ پل مکن که جیحون شاها</p></div>
<div class="m2"><p>انباشته شد جمله ز گرد حشمت</p></div></div>