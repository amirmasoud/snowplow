---
title: >-
    رباعی شمارهٔ ۳۸۲
---
# رباعی شمارهٔ ۳۸۲

<div class="b" id="bn1"><div class="m1"><p>دی طوف چمن کرده سه چاری خورده</p></div>
<div class="m2"><p>آهنگ حزین و پرده حزان کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او چون گل و سرو و گرد او عاشق‌وار</p></div>
<div class="m2"><p>گل جامه دریده سرو حال آورده</p></div></div>