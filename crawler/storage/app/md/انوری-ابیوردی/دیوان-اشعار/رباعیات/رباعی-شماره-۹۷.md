---
title: >-
    رباعی شمارهٔ ۹۷
---
# رباعی شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>با یار مرا زور و ستم درنگرفت</p></div>
<div class="m2"><p>زاری و فغان و لابه هم درنگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شعر ترم چو سنگ نم درنگرفت</p></div>
<div class="m2"><p>تدبیر درم کنم که دم درنگرفت</p></div></div>