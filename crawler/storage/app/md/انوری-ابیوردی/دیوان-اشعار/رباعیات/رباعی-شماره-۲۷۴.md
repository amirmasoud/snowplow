---
title: >-
    رباعی شمارهٔ ۲۷۴
---
# رباعی شمارهٔ ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>آخر شب دوش بی‌تو ای شمع چگل</p></div>
<div class="m2"><p>بگذشت و گذاشت در غمم خوار و خجل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو فارغ و من به وعده تا روز سپید</p></div>
<div class="m2"><p>در بند تو بنشسته و برخاسته دل</p></div></div>