---
title: >-
    رباعی شمارهٔ ۱۰۲
---
# رباعی شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>از چرخ که کامی به مرادم ننهاد</p></div>
<div class="m2"><p>وز بخت که بندی ز امیدم نگشاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیروز شه طغان تکین دادم داد</p></div>
<div class="m2"><p>پیروز شه طغان تکین باقی باد</p></div></div>