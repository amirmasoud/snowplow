---
title: >-
    رباعی شمارهٔ ۱۴۲
---
# رباعی شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>نه مشکل روزگار حل خواهد شد</p></div>
<div class="m2"><p>نه دور فلک همی بدل خواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین پس من و عشق و می که این روزی دو</p></div>
<div class="m2"><p>تا روز دو بر باد اجل خواهد شد</p></div></div>