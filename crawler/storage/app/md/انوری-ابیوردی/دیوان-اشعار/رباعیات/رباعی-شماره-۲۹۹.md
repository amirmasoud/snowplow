---
title: >-
    رباعی شمارهٔ ۲۹۹
---
# رباعی شمارهٔ ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>سودای تو بیرون شده یکسر ز سرم</p></div>
<div class="m2"><p>وز کوی تو ببرید خرد رهگذرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست طلب تو باز در کوفت درم</p></div>
<div class="m2"><p>تا با سر کار برد بار دگرم</p></div></div>