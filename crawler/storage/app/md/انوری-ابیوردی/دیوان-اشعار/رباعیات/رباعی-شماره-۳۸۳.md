---
title: >-
    رباعی شمارهٔ ۳۸۳
---
# رباعی شمارهٔ ۳۸۳

<div class="b" id="bn1"><div class="m1"><p>آیا که مرا تو دست گیری یا نه</p></div>
<div class="m2"><p>فریادرسی در این اسیری یا نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که ترا به بندگی بپذیرم</p></div>
<div class="m2"><p>خدمت کردم اگر پذیری یا نه</p></div></div>