---
title: >-
    رباعی شمارهٔ ۳۹۸
---
# رباعی شمارهٔ ۳۹۸

<div class="b" id="bn1"><div class="m1"><p>در کفر گریزم ار تو ایمان گردی</p></div>
<div class="m2"><p>با درد بسازم ار تو درمان گردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون از سر این حدیث برخاست دلم</p></div>
<div class="m2"><p>دل برکنم از توگر مثل جان گردی</p></div></div>