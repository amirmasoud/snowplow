---
title: >-
    رباعی شمارهٔ ۳۲۶
---
# رباعی شمارهٔ ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>آن دیده ندارم که به خوابت بینم</p></div>
<div class="m2"><p>یا آن رخ همچو آفتابت بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شرم رخ تو در تو نتوان نگریست</p></div>
<div class="m2"><p>می‌ریزم اشک تا در آبت بینم</p></div></div>