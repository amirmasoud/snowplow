---
title: >-
    رباعی شمارهٔ ۸۵
---
# رباعی شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>از گردش این هفت مخالف بر هفت</p></div>
<div class="m2"><p>هر هفت در افتیم به هفتاد آگفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می ده که چو گل جوانیم در گل خفت</p></div>
<div class="m2"><p>تا کی غم عالمی که چون رفتی رفت</p></div></div>