---
title: >-
    رباعی شمارهٔ ۴۳۷
---
# رباعی شمارهٔ ۴۳۷

<div class="b" id="bn1"><div class="m1"><p>هر روز به نویی ای بت سلسله‌موی</p></div>
<div class="m2"><p>جای دگری به دوستی در تک و پوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماهی تو و ماه را چنین باشد خوی</p></div>
<div class="m2"><p>هر روز به منزلی دگر دارد روی</p></div></div>