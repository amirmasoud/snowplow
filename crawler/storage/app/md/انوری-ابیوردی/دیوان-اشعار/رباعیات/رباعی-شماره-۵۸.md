---
title: >-
    رباعی شمارهٔ ۵۸
---
# رباعی شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>با دل گفتم چو یار بی فرمانست</p></div>
<div class="m2"><p>این صبر هوس پختن بی‌پایانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل گفت نفس مزن که تدبیر آنست</p></div>
<div class="m2"><p>هم پختن این هوس که نتوان دانست</p></div></div>