---
title: >-
    رباعی شمارهٔ ۲۱۹
---
# رباعی شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>با گل گفتم ابر چرا می‌گرید</p></div>
<div class="m2"><p>ماتم‌زده نیست بر کجا می‌گرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل گفت اگر راست همی باید گفت</p></div>
<div class="m2"><p>بر عمر من و عهد شما می‌گرید</p></div></div>