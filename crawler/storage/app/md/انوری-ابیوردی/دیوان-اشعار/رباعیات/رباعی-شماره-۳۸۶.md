---
title: >-
    رباعی شمارهٔ ۳۸۶
---
# رباعی شمارهٔ ۳۸۶

<div class="b" id="bn1"><div class="m1"><p>مریخ به خنجر تو جوید فتوی</p></div>
<div class="m2"><p>ناهید به ساغر تو پوید ماوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانست که می‌کند به عید اضحی</p></div>
<div class="m2"><p>از بهر ترا آن حمل این ثور فدی</p></div></div>