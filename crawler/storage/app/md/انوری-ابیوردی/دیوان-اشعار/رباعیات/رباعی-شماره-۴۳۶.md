---
title: >-
    رباعی شمارهٔ ۴۳۶
---
# رباعی شمارهٔ ۴۳۶

<div class="b" id="bn1"><div class="m1"><p>رو رو که تو یار چو منی کم بینی</p></div>
<div class="m2"><p>وین پس همه مرد جلد محکم بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من با تو وفا کردم از آن غم دیدم</p></div>
<div class="m2"><p>با اهل جفا وفا کنی غم بینی</p></div></div>