---
title: >-
    رباعی شمارهٔ ۲۳۲
---
# رباعی شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>خورشید ز رای مقتفی دارد نور</p></div>
<div class="m2"><p>وز دولت سنجریست گیتی معمور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز رایت این رایت دین شد منصور</p></div>
<div class="m2"><p>احسنت زهی خلیفه سلطان دستور</p></div></div>