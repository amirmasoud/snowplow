---
title: >-
    رباعی شمارهٔ ۱۵۵
---
# رباعی شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>پست افکندم غم تو ای سرو بلند</p></div>
<div class="m2"><p>شادم که مرا غمت بدین روز افکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داد من و بیداد تو آخر تا کی</p></div>
<div class="m2"><p>عذر من و آزار تو آخر تا چند</p></div></div>