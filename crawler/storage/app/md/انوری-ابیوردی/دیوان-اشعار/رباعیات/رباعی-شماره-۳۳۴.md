---
title: >-
    رباعی شمارهٔ ۳۳۴
---
# رباعی شمارهٔ ۳۳۴

<div class="b" id="bn1"><div class="m1"><p>سبحان‌الله غمی به پایان نبریم</p></div>
<div class="m2"><p>الا که ازو در دگری می‌نگریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن شد که ستاره می‌شمردیم به روز</p></div>
<div class="m2"><p>اکنون همه روز و شب نفس می‌شمریم</p></div></div>