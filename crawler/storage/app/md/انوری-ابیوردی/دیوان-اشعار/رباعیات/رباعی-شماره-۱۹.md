---
title: >-
    رباعی شمارهٔ ۱۹
---
# رباعی شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>سیاره به خدمت سپرد خاک درت</p></div>
<div class="m2"><p>خورشید که باشد که بود تاج سرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد هر دو جهان به بندگی تو مقر</p></div>
<div class="m2"><p>چونان که به بندگی جد و پدرت</p></div></div>