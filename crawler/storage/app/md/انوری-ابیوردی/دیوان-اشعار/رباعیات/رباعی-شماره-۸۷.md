---
title: >-
    رباعی شمارهٔ ۸۷
---
# رباعی شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>عیشی که نمودم از جوانی همه رفت</p></div>
<div class="m2"><p>عهدی که خریدم از جهان دمدمه رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هین ای بز لنگ آفرینش بشتاب</p></div>
<div class="m2"><p>وین سبزهٔ عاریت رها کن رمه رفت</p></div></div>