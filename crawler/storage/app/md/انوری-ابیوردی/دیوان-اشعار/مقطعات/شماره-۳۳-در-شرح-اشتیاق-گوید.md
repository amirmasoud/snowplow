---
title: >-
    شمارهٔ ۳۳ - در شرح اشتیاق گوید
---
# شمارهٔ ۳۳ - در شرح اشتیاق گوید

<div class="b" id="bn1"><div class="m1"><p>به خدایی که از میان دو حرف</p></div>
<div class="m2"><p>هفت چرخ و چهار طبع انگیخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی کافور و عود و مشک آورد</p></div>
<div class="m2"><p>رنگ طاوس و کبک و زاغ آمیخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که مرا درد هجر تو بر سر</p></div>
<div class="m2"><p>خاک اندوه و آتش غم بیخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از برم دل به خدمت تو رسید</p></div>
<div class="m2"><p>وز تنم جان ز فرقت تو گریخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این چنین کارها زمانه کند</p></div>
<div class="m2"><p>با زمانه نمی‌توان آویخت</p></div></div>