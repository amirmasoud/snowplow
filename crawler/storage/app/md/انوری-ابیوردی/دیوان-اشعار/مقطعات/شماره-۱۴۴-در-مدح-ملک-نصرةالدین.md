---
title: >-
    شمارهٔ ۱۴۴ - در مدح ملک نصرةالدین
---
# شمارهٔ ۱۴۴ - در مدح ملک نصرةالدین

<div class="b" id="bn1"><div class="m1"><p>مبشر آمد و اخبار فتح ختلان داد</p></div>
<div class="m2"><p>نشاط باده کن ای خسرو خراسان شاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درخت رقص‌کنان گشت و مرغ نعره‌زنان</p></div>
<div class="m2"><p>چو برد مژدهٔ فتحت به باغ و بستان باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تویی که هرچ بخواهی خدات آن بدهد</p></div>
<div class="m2"><p>بدان دلیل کزو هرچه خواستی آن داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تویی که تیغ تو چون سیل خون برانگیزد</p></div>
<div class="m2"><p>کنند انجم و ارکان ز روز طوفان یاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عون عدل تو از شیر و یوز بستانند</p></div>
<div class="m2"><p>گوزن و آهو در بیشه و بیابان داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز سنگ ریز در تست دست دریا پر</p></div>
<div class="m2"><p>ز فتح باب کف تست ابر نیسان راد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان ز خصم تو مخذول‌تر نیابد کس</p></div>
<div class="m2"><p>مگر ز مادر محنت برای خذلان زاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنانکه نصرت دین می‌کنی ز رایت و رای</p></div>
<div class="m2"><p>به هرچه روی نهی ناصر تو یزدان باد</p></div></div>