---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>نیامدست مرا خویشتن دگر مردم</p></div>
<div class="m2"><p>از آن زمان که بدانسته‌ام که مردم چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرم نشان دهی از روی مردمی چه شود</p></div>
<div class="m2"><p>چو بخت نیک نشانت دهم که مردم کیست</p></div></div>