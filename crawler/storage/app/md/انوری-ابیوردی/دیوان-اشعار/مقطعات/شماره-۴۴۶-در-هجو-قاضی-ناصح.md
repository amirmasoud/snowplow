---
title: >-
    شمارهٔ ۴۴۶ - در هجو قاضی ناصح
---
# شمارهٔ ۴۴۶ - در هجو قاضی ناصح

<div class="b" id="bn1"><div class="m1"><p>آنکه سایه‌اش کس ندید از غایت ستر و صلاح</p></div>
<div class="m2"><p>باصلاح صالحی شد آفتاب از واضحی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه رای هوشیارت ناصح احوال تست</p></div>
<div class="m2"><p>یک نصیحت گوش دار از بنده قاضی ناصحی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه بر درگاه و اندر مجلس تست از خدم</p></div>
<div class="m2"><p>در صلاح کار تست الا صلاح صالحی</p></div></div>