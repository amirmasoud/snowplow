---
title: >-
    شمارهٔ ۴۹۱ - نصیحت
---
# شمارهٔ ۴۹۱ - نصیحت

<div class="b" id="bn1"><div class="m1"><p>تو اگر شعر نگویی چه کنی خواجه حکیم</p></div>
<div class="m2"><p>بی‌وسیلت نتوانی که بدرها پویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من اگر شعر نگویم پی کاری گیرم</p></div>
<div class="m2"><p>که خلاصی دهد از جاهلی و بدخویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من همه شب ورق زرق فرو می‌شویم</p></div>
<div class="m2"><p>تو همه روز رخ آز به خون می‌شویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قیمت عمر من و عمر تو یکسان نبود</p></div>
<div class="m2"><p>کانچه من جویم از این عمر تو آن کی جویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد رنگین بدل عمر که در خانه نهند</p></div>
<div class="m2"><p>بوی آن می‌برم الحق تو همانا اویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ضایع از عمر من آنست که شعری گویم</p></div>
<div class="m2"><p>حاصل از عمر تو آنست که شعری گویی</p></div></div>