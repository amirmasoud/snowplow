---
title: >-
    شمارهٔ ۲۵۳
---
# شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>بر کار جهان دل منه ایرا که نشاید</p></div>
<div class="m2"><p>کین خوبی و ناخوبی هم دیر نپاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان که بگفتم مهل کاخر روزی</p></div>
<div class="m2"><p>آن سیم سیه گردد و آن حلقه بساید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پندم نپذیرفتی و خوکی شدی آخر</p></div>
<div class="m2"><p>وامروز در این شهر کسی خوک نگاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم با دل پر دردی و هم با رخ پر موی</p></div>
<div class="m2"><p>ای سرو لقا محنت از این بیش نیاید</p></div></div>