---
title: >-
    شمارهٔ ۴۲۸ - از لالابک تقاضایی کند
---
# شمارهٔ ۴۲۸ - از لالابک تقاضایی کند

<div class="b" id="bn1"><div class="m1"><p>ای جهان را دفین به دست تو در</p></div>
<div class="m2"><p>چون معادن هزار سرمایه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دولتت را دوام همخانه</p></div>
<div class="m2"><p>مدتت را زمانه همسایه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردن و گوش آفرینش را</p></div>
<div class="m2"><p>رسمهای تو گشته پیرایه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جود را پروریده همت تو</p></div>
<div class="m2"><p>راست چونان که طفل را دایه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملکی در محاسن و اخلاق</p></div>
<div class="m2"><p>زان نداری محاسن و خایه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتابی و در مراتب جاه</p></div>
<div class="m2"><p>آفتابت فروترین پایه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چیست کز تابش تو در نورند</p></div>
<div class="m2"><p>همه آفاق و بنده در سایه</p></div></div>