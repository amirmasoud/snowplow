---
title: >-
    شمارهٔ ۵۷ - اظهار اشتیاق کند
---
# شمارهٔ ۵۷ - اظهار اشتیاق کند

<div class="b" id="bn1"><div class="m1"><p>به خدایی که در پرستش خویش</p></div>
<div class="m2"><p>آسمان را رکوع فرمودست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست حکمش به کیلهٔ خورشید</p></div>
<div class="m2"><p>خرمن روزگار پیمودست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که ز چشمم به عشق خدمت تو</p></div>
<div class="m2"><p>جان به عرض سرشک پالودست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این سخن را عزیز دار که دوش</p></div>
<div class="m2"><p>چرخ با من در این سخن بودست</p></div></div>