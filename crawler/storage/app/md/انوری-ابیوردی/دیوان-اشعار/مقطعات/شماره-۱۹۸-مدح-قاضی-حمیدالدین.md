---
title: >-
    شمارهٔ ۱۹۸ - مدح قاضی حمیدالدین
---
# شمارهٔ ۱۹۸ - مدح قاضی حمیدالدین

<div class="b" id="bn1"><div class="m1"><p>با جلال تو ای حمیدالدین</p></div>
<div class="m2"><p>رونق ماه و آفتاب نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طلعت فضل و چهرهٔ دانش</p></div>
<div class="m2"><p>از ضمیر تو در نقاب نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌تو ما را به حق نعمت تو</p></div>
<div class="m2"><p>در دل و چشم صبر و خواب نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا من از تو جدا شدم به خطا</p></div>
<div class="m2"><p>در دلم فکرت صواب نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جامهٔ عیش را طراز برفت</p></div>
<div class="m2"><p>خیمهٔ لهو را طناب نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شخص اقبال را حیات بشد</p></div>
<div class="m2"><p>جام لذات را شراب نماند</p></div></div>