---
title: >-
    شمارهٔ ۲۹۱
---
# شمارهٔ ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>سر زلفت به جز دست تو حیفست</p></div>
<div class="m2"><p>لب لعلت به بوس جز تو افسوس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر زلف تو باری هم تو می‌کش</p></div>
<div class="m2"><p>لب لعل تو باری هم تو می‌بوس</p></div></div>