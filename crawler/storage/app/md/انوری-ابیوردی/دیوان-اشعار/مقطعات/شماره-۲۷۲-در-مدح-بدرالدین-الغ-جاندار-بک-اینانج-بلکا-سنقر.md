---
title: >-
    شمارهٔ ۲۷۲ - در مدح بدرالدین الغ جاندار بک اینانج بلکا سنقر
---
# شمارهٔ ۲۷۲ - در مدح بدرالدین الغ جاندار بک اینانج بلکا سنقر

<div class="b" id="bn1"><div class="m1"><p>خداوندا تو آنی کافرینش</p></div>
<div class="m2"><p>به کلی هست چون دریا و تو در</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان را پهلوان چون تو نباشد</p></div>
<div class="m2"><p>زهی از تو جهان را صد تفاخر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارد بیشهٔ دولت چو تو شیر</p></div>
<div class="m2"><p>نزاید مادر گیتی چو تو حر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گیتی فتنه کی بنشستی از پای</p></div>
<div class="m2"><p>اگز نه تیغ تو گفتیش التر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک با اختران گفتا که آن کیست</p></div>
<div class="m2"><p>که هست از خیل او چشم ظفر پر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رکاب تو ببوسیدند و گفتند</p></div>
<div class="m2"><p>الغ جاندار بک اینانج سنقر</p></div></div>