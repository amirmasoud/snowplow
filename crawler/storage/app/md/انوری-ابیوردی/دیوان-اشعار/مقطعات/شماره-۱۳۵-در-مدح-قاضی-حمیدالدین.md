---
title: >-
    شمارهٔ ۱۳۵ - در مدح قاضی حمیدالدین
---
# شمارهٔ ۱۳۵ - در مدح قاضی حمیدالدین

<div class="b" id="bn1"><div class="m1"><p>ای به تو مخصوص اعجاز سخن</p></div>
<div class="m2"><p>چون به وترای وتر در معنی قنوت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سمت درگاهت سعود چرخ را</p></div>
<div class="m2"><p>گشته در دوران کل خیرالسموت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزگاری در کمال ناقصان</p></div>
<div class="m2"><p>روزگار اطلس کند ز برگ توت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما چو قرص ارزن و حوت غدیر</p></div>
<div class="m2"><p>تو چو قرص آفتا و برج حوت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صعوهٔ ما مرد سیمرغ تو نیست</p></div>
<div class="m2"><p>تو قوی بازو به فضلی ما به قوت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش نظم چون نسیج الوحد تو</p></div>
<div class="m2"><p>چیست نظم ما نسیج النعکبوت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه در تالیف این ابیات نیست</p></div>
<div class="m2"><p>بی‌سمین غثی و قسبی بی‌کروت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رای عالی در جواب این مبند</p></div>
<div class="m2"><p>لایق اینجا السکوتست السکوت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای به حق بخت تو حی لاینام</p></div>
<div class="m2"><p>بادی اندر حفظ حی لایموت</p></div></div>