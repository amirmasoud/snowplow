---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>انوری را ز حرص خدمت تو</p></div>
<div class="m2"><p>چون بر آتش بود قدم پیوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نتواند که زحمتت ندهد</p></div>
<div class="m2"><p>گاه و بی‌گه چه هوشیار و چه مست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست اینک ندیم حلقهٔ در</p></div>
<div class="m2"><p>ای جهان بر در تو بارش هست</p></div></div>