---
title: >-
    شمارهٔ ۶۴ - در مدح اقضی‌القضاة قاضی حمیدالدین
---
# شمارهٔ ۶۴ - در مدح اقضی‌القضاة قاضی حمیدالدین

<div class="b" id="bn1"><div class="m1"><p>قطعهٔ صدر اجل قاضی قضاة شرق و غرب</p></div>
<div class="m2"><p>آنکه بر عالم نفاذ او قضای دیگرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواجهٔ ملت حمیدالدین که از روی قوام</p></div>
<div class="m2"><p>دین و ملت را مکانش چون عرض را جوهرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه قاضی فلک یعنی که جرم مشتری</p></div>
<div class="m2"><p>روز بارش از عداد پرده‌داران درست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چاکران حضرتش نزد من آوردند دی</p></div>
<div class="m2"><p>چاکران حضرتی کو را چو من صد چاکرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نهادم بر سر و بر دیده آن تشریف را</p></div>
<div class="m2"><p>کز عزیزی راست همچون دیدگانم در سرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده از حیرت همی گفت این چه کحل و توتیاست</p></div>
<div class="m2"><p>تارک از دهشت همی گفت این چه تاج و افسرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر زبانم رفت کین درج سراسر نکته‌بین</p></div>
<div class="m2"><p>عقل گفت ای هرزه‌گو این درج تا سر گوهرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان سخن پروردنم یکبارگی معلوم شد</p></div>
<div class="m2"><p>کانچه عالی رای ملک‌آرای معنی پرورست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاطر وقادش اندر نسبت آب سخن</p></div>
<div class="m2"><p>آتشی آمد که دودش جمله آب کوثرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عالم معنیش خواندم عالمم خاموش کرد</p></div>
<div class="m2"><p>گفت عامل چون بود آن کو ز عالم برترست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مهر و کینش موجب بدبختی و نیک‌اختریست</p></div>
<div class="m2"><p>چون از این بدبخت شد انصاف از آن نیک‌اخترست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از خط شیرینش اندر فکرتم کایا مگر</p></div>
<div class="m2"><p>آهوان چین و ماچین را چراگه عسکرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با خرد گفتم توانی گفت این اعجوبه چیست</p></div>
<div class="m2"><p>گفت پندارم که بحری پر ز مشک و شکرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عشق ازو به گفت گفتا نیک دور افتاده‌اند</p></div>
<div class="m2"><p>یادگاری از لب معشوق و زلف دلبرست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دیر زی ای آنکه بعد از پانصد و پنجاه سال</p></div>
<div class="m2"><p>نظم و خطت بر نبوت حجت پیغامبرست</p></div></div>