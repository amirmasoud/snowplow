---
title: >-
    شمارهٔ ۳۲ - در مدح مجدالدین ابوطالب نعمه
---
# شمارهٔ ۳۲ - در مدح مجدالدین ابوطالب نعمه

<div class="b" id="bn1"><div class="m1"><p>گره عهد آسمان سست است</p></div>
<div class="m2"><p>گره کیسهٔ عناصر سخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه بگشاد هیچ وقت نبست</p></div>
<div class="m2"><p>گره عهد و بندگیش ز بخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیست بحری که موج بخشش اوی</p></div>
<div class="m2"><p>کیسهٔ بحر و کان کند پردخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میر بوطالب آنکه او ثمرست</p></div>
<div class="m2"><p>اسدالله باغ و نعمه درخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پادشاهیست نسبت او را تاج</p></div>
<div class="m2"><p>شهریاریست همت او را تخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جرم ماه از اشارت جدش</p></div>
<div class="m2"><p>هم به دو نیمه گشت و هم یک لخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عرش می‌گفت در احد تکبیر</p></div>
<div class="m2"><p>پدرش تیغ فتح می‌آهخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ترازوی همتش هرگز</p></div>
<div class="m2"><p>حاصل روزگار هیچ نسخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست او سایه بر جهان افکند</p></div>
<div class="m2"><p>با عدم برد تنگدستی رخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باد دستش قوی و از دستش</p></div>
<div class="m2"><p>دشمنش لخت لخت گشته به لخت</p></div></div>