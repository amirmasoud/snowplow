---
title: >-
    شمارهٔ ۲۹۸
---
# شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>ای کریمی که از سخاوت تو</p></div>
<div class="m2"><p>روید از سنگ خاره مرزنگوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا جهان اسب دولتت زین کرد</p></div>
<div class="m2"><p>چرخ را هست غاشیه بر دوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه او تای خدمتت نزند</p></div>
<div class="m2"><p>چون ربابش فلک بمالد گوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنگ مدح تو ساختم چه شود</p></div>
<div class="m2"><p>که چو بربط شوم عتابی‌پوش</p></div></div>