---
title: >-
    شمارهٔ ۱۲۳ - فی‌الحکمة والنصیحة
---
# شمارهٔ ۱۲۳ - فی‌الحکمة والنصیحة

<div class="b" id="bn1"><div class="m1"><p>در حدود ری یکی دیوانه بود</p></div>
<div class="m2"><p>سال و مه کردی به سوی دشت گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تموز و دی به سالی یک دو بار</p></div>
<div class="m2"><p>آمدی در قلب شهر از طرف دشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی ای آنان کتان آماده بود</p></div>
<div class="m2"><p>زیر قرب و بعد ازین زرینه طشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قاقم و سنجاب در سرما سه چار</p></div>
<div class="m2"><p>توزی و کتان به گرما هفت و هشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر شما را با نوایی بد چه شد</p></div>
<div class="m2"><p>ورچه ما را بود بی‌برگی چه گشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راحت هستی و رنج نیستی</p></div>
<div class="m2"><p>بر شما بگذشت و بر ما هم گذشت</p></div></div>