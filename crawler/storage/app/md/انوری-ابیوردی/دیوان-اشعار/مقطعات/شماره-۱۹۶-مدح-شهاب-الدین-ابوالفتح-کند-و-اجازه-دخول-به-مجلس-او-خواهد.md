---
title: >-
    شمارهٔ ۱۹۶ - مدح شهاب‌الدین ابوالفتح کند و اجازهٔ دخول به مجلس او خواهد
---
# شمارهٔ ۱۹۶ - مدح شهاب‌الدین ابوالفتح کند و اجازهٔ دخول به مجلس او خواهد

<div class="b" id="bn1"><div class="m1"><p>ای آنکه لقب تاش ثاقب تو</p></div>
<div class="m2"><p>هر شب ز فلک اهرمن رماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موئمن به زبان بر پس اذاجاء</p></div>
<div class="m2"><p>نام پسر و کنیت تو خواند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید جهان را به هر وظیفت</p></div>
<div class="m2"><p>نور دگر از رای تو ستاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر چهرهٔ گیتی اگر بخواهی</p></div>
<div class="m2"><p>خالی ز سیاهی شب نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیتی به لب خشک نامرادان</p></div>
<div class="m2"><p>بی‌دست تو آبی نمی‌رساند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز معرکهٔ آز بی‌محابا</p></div>
<div class="m2"><p>بی‌وجود تو کس را نمی‌رهاند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وز قدر تو اندر حروف معجم</p></div>
<div class="m2"><p>کلک تو نهد زانکه او تواند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منشی فلک با فنون انشاء</p></div>
<div class="m2"><p>پیش قلمت هر ز بر نداند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر سدهٔ تو کاسمان به رغبت</p></div>
<div class="m2"><p>آن خواهد کانجم برو فشاند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون سایهٔ نشاندست انوری را</p></div>
<div class="m2"><p>عشق تو وزین گونه او نشاند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر نیست اجازه به ادخلوها</p></div>
<div class="m2"><p>باز آیت الراحلون بخواند</p></div></div>