---
title: >-
    شمارهٔ ۲۱۳ - در وصل سرای مجدالدین ابوالحسن
---
# شمارهٔ ۲۱۳ - در وصل سرای مجدالدین ابوالحسن

<div class="b" id="bn1"><div class="m1"><p>ای نمودار آفتاب بلند</p></div>
<div class="m2"><p>گشته ایمن چو آسان ز گزند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورت فتح و قبهٔ ظفری</p></div>
<div class="m2"><p>این‌چنین دلگشای دشمن بند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساحتت آب قندهار ببرد</p></div>
<div class="m2"><p>صنعتت بیخ نوبهار بکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سقف تو با سپهر همسایه</p></div>
<div class="m2"><p>صحن تو با بهشت خویشاوند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسمانی که نیستت همتا</p></div>
<div class="m2"><p>یا بهشتی که نیستت مانند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تو آباد باد و فرخ باد</p></div>
<div class="m2"><p>آنکه بنیاد فرخ تو فکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مجد دین بوالحسن هست عقیم</p></div>
<div class="m2"><p>مادر عالم از چو او فرزند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه دستش به دادن روزی</p></div>
<div class="m2"><p>آمد اندر زمانه روزی مند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا ز تاریخها شود معلوم</p></div>
<div class="m2"><p>کز فلان چند شد ز بهمان چند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عدد سالهای عمرش باد</p></div>
<div class="m2"><p>همچو تاریخ پانصد و چل و اند</p></div></div>