---
title: >-
    شمارهٔ ۷۹ - مطایبه
---
# شمارهٔ ۷۹ - مطایبه

<div class="b" id="bn1"><div class="m1"><p>دی گفت به طنز نجم قوال</p></div>
<div class="m2"><p>کای بنده سپهر آبنوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در زنگولهٔ نشید دانی</p></div>
<div class="m2"><p>گفتم چه دهند از این فسوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پردهٔ راست راه دانم</p></div>
<div class="m2"><p>وانگاه به خانهٔ عروست</p></div></div>