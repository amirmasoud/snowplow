---
title: >-
    شمارهٔ ۲۱ - ایضا درخواست شراب کند
---
# شمارهٔ ۲۱ - ایضا درخواست شراب کند

<div class="b" id="bn1"><div class="m1"><p>ایا دقیق نظر مهتری که گاه سخا</p></div>
<div class="m2"><p>توانی ار بچکانی همی از آتش آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پیش دست سخی تو از خجالت و شرم</p></div>
<div class="m2"><p>به جای قطرهٔ باران عرق چکد ز سحاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سه کس به زاویه‌ای در نشسته مخموریم</p></div>
<div class="m2"><p>به یاد بادهٔ دوشینه هرسه مست و خراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ذروهٔ فلک و ماه برکشیده سرود</p></div>
<div class="m2"><p>ز چهرهٔ طرب و لهو برگرفته نقاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امید ما پس از ایزد به جود تست که نیست</p></div>
<div class="m2"><p>ز ساز مجلس ما هیچ جز کباب و رباب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مصاف عشرت ما بشکند زمانه اگر</p></div>
<div class="m2"><p>تو نشکنی بتفضل خمار ما به شراب</p></div></div>