---
title: >-
    شمارهٔ ۲۶۹
---
# شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>من و سه شاعر و شش درزی و چهار دبیر</p></div>
<div class="m2"><p>اسیر و خوار بماندیم در کف دو سوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دبیر و درزی و شاعر چگونه جنگ کنند</p></div>
<div class="m2"><p>اگر چه چارده باشند وگر چهار هزار</p></div></div>