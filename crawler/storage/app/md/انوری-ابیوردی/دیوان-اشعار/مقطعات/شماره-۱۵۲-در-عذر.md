---
title: >-
    شمارهٔ ۱۵۲ - در عذر
---
# شمارهٔ ۱۵۲ - در عذر

<div class="b" id="bn1"><div class="m1"><p>تو آن کریمی کز التفات خاطر تو</p></div>
<div class="m2"><p>نیاز تا به ابد در نعیم و ناز افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرد سزای تو نا معنییی به دست آرد</p></div>
<div class="m2"><p>هزار سال در اندیشهٔ دراز افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بیست بیت مدیح تو در کرم بینی</p></div>
<div class="m2"><p>چنان فتد که به اصلاح آن نیاز افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب مدار که اندر سرای عالم کون</p></div>
<div class="m2"><p>گهی نشیب فتد کار و گه فراز افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز حرص مدح تو باشد که از درخت سخن</p></div>
<div class="m2"><p>لطیفه‌ای مثلا نیم پخته باز افتد</p></div></div>