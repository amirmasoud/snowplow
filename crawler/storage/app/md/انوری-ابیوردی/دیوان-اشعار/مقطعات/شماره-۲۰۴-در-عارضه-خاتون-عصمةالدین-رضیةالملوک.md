---
title: >-
    شمارهٔ ۲۰۴ - در عارضهٔ خاتون عصمةالدین رضیةالملوک
---
# شمارهٔ ۲۰۴ - در عارضهٔ خاتون عصمةالدین رضیةالملوک

<div class="b" id="bn1"><div class="m1"><p>گر خداوند عصمةالدین را</p></div>
<div class="m2"><p>عارضه رنجه داشت روزی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن بدان از بد ستارهٔ نحس</p></div>
<div class="m2"><p>یا جفای سپهر بد پیوند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دولتی داشت بس به غایت تیز</p></div>
<div class="m2"><p>چون قضا قادر و چو چرخ بلند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخت بیدار مهربانش گفت</p></div>
<div class="m2"><p>که بود در کمال بیم گزند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دفع چشم بد جهانی را</p></div>
<div class="m2"><p>همچنین نرم نرم و خنداخند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داشت از روی مصلحت دو سه روز</p></div>
<div class="m2"><p>دل او را که شاد باد نژند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور تو کفارتی نهی آنرا</p></div>
<div class="m2"><p>من نباشم بدان سخن خرسند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کادمیزاده‌ای که بی‌گنه است</p></div>
<div class="m2"><p>کی به کفار تست حاجتمند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وانکه معصوم هست دست گناه</p></div>
<div class="m2"><p>پای او را نیارد اندر بند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>معصیت را به عالم عصمت</p></div>
<div class="m2"><p>وهم هم درنیاورد به کمند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس چه کفارت این چه کفر بود</p></div>
<div class="m2"><p>یا چه بیهوده باشد و ترفند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لفظ کفارت ای سلیم القلب</p></div>
<div class="m2"><p>بپذیر از من مسلمان پند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هیچ معصوم را چو نپسندی</p></div>
<div class="m2"><p>عصمت صرف را مکن به پسند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای ز آباء و امهات وجود</p></div>
<div class="m2"><p>چون تو هرگز نزاده یک فرزند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به خدایی که نیست مانندش</p></div>
<div class="m2"><p>گرچه مستغنیم از این سوگند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که ز انصاف روزگار امروز</p></div>
<div class="m2"><p>همه چیزیت هست جز مانند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وانکه در عرضگاه کون و فساد</p></div>
<div class="m2"><p>چرخ را نیست هیچ خویشاوند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نظم پروین نداد کاری را</p></div>
<div class="m2"><p>تا به شکل بنات نپراکند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر نگاری نگاشت باز بشست</p></div>
<div class="m2"><p>ور نهالی نشاند باز بکند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باری از طوبی تو طوبی لک</p></div>
<div class="m2"><p>سالها رفت و بر گلی نفکند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روزگارت جگر نخواهد داد</p></div>
<div class="m2"><p>خصم گو روز و شب جگر می‌رند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر گشاید زمانه ور بندد</p></div>
<div class="m2"><p>دل به جز در خدای هیچ مبند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پایت اندر رکاب تاییدست</p></div>
<div class="m2"><p>درنیتفی از این سیاه و سمند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو که در حفظ ایزدی چه کنی</p></div>
<div class="m2"><p>حرز و تعویذ اهل جند و خجند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حرف و صوت ار قضا بگرداند</p></div>
<div class="m2"><p>مرحبا زند و حبذا پازند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از که کرد آتش حوادث دور</p></div>
<div class="m2"><p>در سرای سپنج دود سپند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا که در نطع دهر در بازیست</p></div>
<div class="m2"><p>رخ بهرام و اسب مار اسفند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باد فرزین عز و عمرت را</p></div>
<div class="m2"><p>از پیاده دوام فرزین بند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شخص و دینت ودیعت ایزد</p></div>
<div class="m2"><p>بی‌نیاز از طبیب و دانشمند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عدد سالهای مدت تو</p></div>
<div class="m2"><p>همچو تاریخ پانصد و چل و اند</p></div></div>