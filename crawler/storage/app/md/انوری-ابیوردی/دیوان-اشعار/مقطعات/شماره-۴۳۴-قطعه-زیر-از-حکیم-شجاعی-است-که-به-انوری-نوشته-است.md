---
title: >-
    شمارهٔ ۴۳۴ - قطعهٔ زیر از حکیم شجاعی است که به انوری نوشته است
---
# شمارهٔ ۴۳۴ - قطعهٔ زیر از حکیم شجاعی است که به انوری نوشته است

<div class="b" id="bn1"><div class="m1"><p>ای انوری تویی که به فضل و هنر سزند</p></div>
<div class="m2"><p>احرار روزگار و افاضل ترا رهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بودند در قدیم امیران و شاعران</p></div>
<div class="m2"><p>واکنون شدت مسلم بر شاعران شهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هستت خبر که هستم دور از تو ناتوان</p></div>
<div class="m2"><p>اشکم چو ناردانه و رخسار چون بهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشغول بوده‌ای که نکردی عیادتم</p></div>
<div class="m2"><p>یا خود مرا محل عیادت نمی‌نهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی‌نی ز ابلهی است مرا از تو این طمع</p></div>
<div class="m2"><p>خیزد چنین طمع به حقیقت ز ابلهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با رنج ناتوانی ای دوستان مرا</p></div>
<div class="m2"><p>دل گشت پر ز انده و از صبر شد تهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوید طبیب بهتری امروز غم مخور</p></div>
<div class="m2"><p>اینک برفت علت و آغاز شد بهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم این غمست و بس که ز من فوت می‌شود</p></div>
<div class="m2"><p>در بزم صدر عالم رسم سه‌شنبهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن جنت نعیم اگر در جهان بود</p></div>
<div class="m2"><p>ممکن ظهور جنت ماوی، فتلک هی</p></div></div>