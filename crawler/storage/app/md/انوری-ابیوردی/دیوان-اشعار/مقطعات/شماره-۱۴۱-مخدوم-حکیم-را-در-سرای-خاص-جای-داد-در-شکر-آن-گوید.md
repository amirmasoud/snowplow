---
title: >-
    شمارهٔ ۱۴۱ - مخدوم حکیم را در سرای خاص جای داد در شکر آن گوید
---
# شمارهٔ ۱۴۱ - مخدوم حکیم را در سرای خاص جای داد در شکر آن گوید

<div class="b" id="bn1"><div class="m1"><p>ای مقر عز تو از خرمی دارالقرار</p></div>
<div class="m2"><p>دایم از اقبال چون دارالقرار آباد باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن مکان کز تو فلک قدر و زمین بسطت شده است</p></div>
<div class="m2"><p>در نهاد خود فلک سقف و زمین بنیاد باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته‌ای از روی آزادی نزولی کن درو</p></div>
<div class="m2"><p>جاودان جانت ز بند حادثات آزاد باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وانکه گفتی طبع ما را شاد گردان گاهگاه</p></div>
<div class="m2"><p>گاه و بی‌گاهت دل صافی و طبع شاد باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پایهٔ شعر از عذوبت برده‌ای بر آسمان</p></div>
<div class="m2"><p>آشمان را کمترین شاگرد تو استاد باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باد شهرت را که دارد نسبت از باد بهشت</p></div>
<div class="m2"><p>بر سر از تشویر طبعت خاک و در کف باد باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمترین بندگان از بندگان خاص تو</p></div>
<div class="m2"><p>ای خداوندیت عام از بندگانت یاد باد</p></div></div>