---
title: >-
    شمارهٔ ۴۱۷ - در طلب هیزم
---
# شمارهٔ ۴۱۷ - در طلب هیزم

<div class="b" id="bn1"><div class="m1"><p>ای ز دست تجاسر خادم</p></div>
<div class="m2"><p>شربهای ملال نوشیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اختلالی که من دارد</p></div>
<div class="m2"><p>نیست بر خاطر تو پوشیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو ایام بیض و من صایم</p></div>
<div class="m2"><p>وز خطا در صواب کوشیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیم جوشیده دیگکی دارم</p></div>
<div class="m2"><p>قلقلش گوش نانیوشیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از طریق کرم توانی کرد</p></div>
<div class="m2"><p>بدو چوبش تمام جوشیده</p></div></div>