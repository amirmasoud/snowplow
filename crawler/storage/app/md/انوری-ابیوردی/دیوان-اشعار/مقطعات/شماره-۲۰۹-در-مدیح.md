---
title: >-
    شمارهٔ ۲۰۹ - در مدیح
---
# شمارهٔ ۲۰۹ - در مدیح

<div class="b" id="bn1"><div class="m1"><p>صاحبا دین و ملک بی‌تو مباد</p></div>
<div class="m2"><p>کز جهان کار این و آن دارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانکه این دو ودیعتند که خلق</p></div>
<div class="m2"><p>از خدای و خدایگان دارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک و دین را زمان زمان تو باد</p></div>
<div class="m2"><p>کاب و رونق درین زمان دارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تویی آنکس که ذکر مدت تست</p></div>
<div class="m2"><p>تا که گویندگان زبان دارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالمی دئر پناه نعمت تو</p></div>
<div class="m2"><p>شکر شکر در دهان دارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امتی در وفای خدمت تو</p></div>
<div class="m2"><p>کمر عهد بر میان دارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دامن عرصه‌ایست جاه ترا</p></div>
<div class="m2"><p>این که این چار قهرمان دارند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوشهٔ طارمی است قدر ترا</p></div>
<div class="m2"><p>این که این هفت پاسبان دارند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوستان از تواتر کرمت</p></div>
<div class="m2"><p>خانه چون راه کهکشان دارند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دشمنان از تراکم سخطت</p></div>
<div class="m2"><p>فتنه در مغز استخوان دارند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ضبط عالم به تیغ و کلک کنند</p></div>
<div class="m2"><p>که اثرهای بی کران دارند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کلک فرزانگان کارگزار</p></div>
<div class="m2"><p>تیغ ترکان کاردان دارند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زین کروه آنکه اهل انعامند</p></div>
<div class="m2"><p>همه از نعمت تو جان دارند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زان گروه آنکه اهل اقطاعند</p></div>
<div class="m2"><p>همه از دست تو جهان دارند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جود می‌گفت با کرم روزی</p></div>
<div class="m2"><p>که کسانی که این مکان دارند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر جهانداری به شرط کنند</p></div>
<div class="m2"><p>چه نکوتر که بر چه سان دارند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کرم از سوی تو اشارت کرد</p></div>
<div class="m2"><p>که کریمان جهان چنان دارند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کیسه پرداز بحر و کان کف تست</p></div>
<div class="m2"><p>که بدو خرج جاودان دارند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طاعت آموز انس و جان در تست</p></div>
<div class="m2"><p>کش همه سر بر آستان دارند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه در مهر خازنت بادا</p></div>
<div class="m2"><p>هرچ اضافت به بحر و کان دارند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه با داغ طاعتت بادند</p></div>
<div class="m2"><p>هرکه نسبت به انس و جان دارند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پای بر خاک هر زمین که نهی</p></div>
<div class="m2"><p>منتی تا بر آسمان دارند</p></div></div>