---
title: >-
    شمارهٔ ۷۰ - در نصیحت
---
# شمارهٔ ۷۰ - در نصیحت

<div class="b" id="bn1"><div class="m1"><p>اعتقادی درست دار چنانک</p></div>
<div class="m2"><p>اعتمادت بدان نباشد سست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنده را بی‌شک از عذاب خدای</p></div>
<div class="m2"><p>نرهاند جز اعتقاد درست</p></div></div>