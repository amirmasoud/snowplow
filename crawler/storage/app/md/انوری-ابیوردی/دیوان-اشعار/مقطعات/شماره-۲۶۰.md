---
title: >-
    شمارهٔ ۲۶۰
---
# شمارهٔ ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>اگر انوری خواهد از روزگار</p></div>
<div class="m2"><p>که یک لحظه بی‌زاء زحمت زید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگس را پدید آورد روزگار</p></div>
<div class="m2"><p>که تا بر سر راء رحمت رید</p></div></div>