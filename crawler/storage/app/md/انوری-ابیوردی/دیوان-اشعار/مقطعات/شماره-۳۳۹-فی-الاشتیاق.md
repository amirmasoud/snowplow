---
title: >-
    شمارهٔ ۳۳۹ - فی‌الاشتیاق
---
# شمارهٔ ۳۳۹ - فی‌الاشتیاق

<div class="b" id="bn1"><div class="m1"><p>به خدایی که عقل کلی را</p></div>
<div class="m2"><p>بر درش سر بر آستان دیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پی وصف حضرت عزش</p></div>
<div class="m2"><p>دهن نطق بی‌زبان دیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که من از دوری تو دور از تو</p></div>
<div class="m2"><p>بی‌تکلف هلاک جان دیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌تو تاریک شد جهان بر من</p></div>
<div class="m2"><p>که به روئیت همه جهان دیدم</p></div></div>