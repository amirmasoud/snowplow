---
title: >-
    شمارهٔ ۳۶۹ - در ریاضت خاطر
---
# شمارهٔ ۳۶۹ - در ریاضت خاطر

<div class="b" id="bn1"><div class="m1"><p>چون من به ره سخن فراز آیم</p></div>
<div class="m2"><p>خواهم که قصیده‌ای بیارایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایزد داند که جان مسکین را</p></div>
<div class="m2"><p>تا چند عنا و رنج فرمایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد بار به عقده در شوم تا من</p></div>
<div class="m2"><p>از عهدهٔ یک سخن برون آیم</p></div></div>