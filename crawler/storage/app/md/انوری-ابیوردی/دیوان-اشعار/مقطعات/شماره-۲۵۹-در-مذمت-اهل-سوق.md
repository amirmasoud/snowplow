---
title: >-
    شمارهٔ ۲۵۹ - در مذمت اهل سوق
---
# شمارهٔ ۲۵۹ - در مذمت اهل سوق

<div class="b" id="bn1"><div class="m1"><p>روزی پسری با پدر خویش چنین گفت</p></div>
<div class="m2"><p>کان مردک بازاری از آن زرق چه جوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا چه تفحص کنی احوال گروهی</p></div>
<div class="m2"><p>کز گند طمعشان سگ صیاد نبوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاقل به چنان طایفهٔ دون نگراید</p></div>
<div class="m2"><p>مردم به سوی مزبله و جیفه نپوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بازار یکی مزرعهٔ تخم فسادست</p></div>
<div class="m2"><p>زان تخم در آن خاک چه پاشی که چه روید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امید مکن راستی از پشت بنفشه</p></div>
<div class="m2"><p>تا روی تو چون لاله به خونابه نشوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قولی نبود راست‌تر از قول شهادت</p></div>
<div class="m2"><p>زان در همه بازار یکی راست نگوید</p></div></div>