---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>ایمنی را و تندرستی را</p></div>
<div class="m2"><p>آدمی شکر کرد نتواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جهان این دو نعمت است بزرگ</p></div>
<div class="m2"><p>داند آن کس که نیک و بد داند</p></div></div>