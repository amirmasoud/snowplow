---
title: >-
    شمارهٔ ۱۷۷ - در صفت کسب کمال ومذمت ابناء عصر
---
# شمارهٔ ۱۷۷ - در صفت کسب کمال ومذمت ابناء عصر

<div class="b" id="bn1"><div class="m1"><p>هر که به ورزیدن کمال نهد روی</p></div>
<div class="m2"><p>شیوهٔ نقصان ز هیچ روی نورزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلزلهٔ حرص اگر زهم ببرد کوه</p></div>
<div class="m2"><p>گرد قناعت بر آستانش نلرزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفعت اهل زمانه کسب کند زانک</p></div>
<div class="m2"><p>صحبت اهل زمانه هیچ نه ارزد</p></div></div>