---
title: >-
    شمارهٔ ۲۶۲
---
# شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>ای برادر پند من بشنو اگر خواهی صلاح</p></div>
<div class="m2"><p>در معاش خویش بر قانون من کن یک مدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور قرارت نیست بر گفتم یقین دان کز اسف</p></div>
<div class="m2"><p>بر فوات آن نگردی ناصبور و بی قرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرد باش و ترک زن کن کاندرین ایام ما</p></div>
<div class="m2"><p>زن نخواهد هیچ مرد باتمیز و هوشیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باشد اندر اصل خود خر پس شود تصحیف حر</p></div>
<div class="m2"><p>آنکه خواهد اصل هر اندوه مر تیماردار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور اسیر شهوتی باری کنیزک خر به زر</p></div>
<div class="m2"><p>سروقدی ماه رویی سیم ساقی گلعذار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این قدردانی که چون خیزی به وقت بامداد</p></div>
<div class="m2"><p>روی مال خویش بینی نه روی وام دار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور به کس رغبت نداری برگذر زو برحقی</p></div>
<div class="m2"><p>کاندرو یک نفع بینی و کدورت صد هزار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیوهٔ اهل زمانه پیش کن بگزین غلام</p></div>
<div class="m2"><p>در حضر بی‌بی و خاتون در سفر اسفندیار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر زند از بهر تو دامن به وقت کاه زیر</p></div>
<div class="m2"><p>بر زند خود را به صف کین به گاه کارزار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روز و شب دوزندهٔ خصم و عدو باشد به تیر</p></div>
<div class="m2"><p>سال ومه باشد جماع و بوسه را پیشت چو پار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم حریف و هم قرین و هم ندیم و هم رفیق</p></div>
<div class="m2"><p>هم غلام و هم کنیزک هم پیاده هم سوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا بود بر طبع تو باری بزی با سنگ و سیم</p></div>
<div class="m2"><p>ور ز دل گردد مزاجت هست او زر عیار</p></div></div>