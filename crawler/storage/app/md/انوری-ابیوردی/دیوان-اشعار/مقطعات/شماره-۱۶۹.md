---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>به خدایی که آب حکمت او</p></div>
<div class="m2"><p>از دل خاک می‌دماند ورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست تقدیر او ز دامن شب</p></div>
<div class="m2"><p>بر رخ روز می‌فشاند گرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که رهی در فراق وصلت تو</p></div>
<div class="m2"><p>زندگانی نمی‌تواند کرد</p></div></div>