---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>هزار مدح شکر طعم وصف تو گفتم</p></div>
<div class="m2"><p>کزو نگشت مرا تازه یک صبوح فتوح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برادرم که دو تن تاک را نهد نیرو</p></div>
<div class="m2"><p>همی گسسته نگردد غبوق او ز صبوح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درست شد که دو تن تاک به ز صد ممدوح</p></div>
<div class="m2"><p>یقین شدم که دو ممدوح به ز صد ممدوح</p></div></div>