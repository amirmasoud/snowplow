---
title: >-
    شمارهٔ ۲۴۷ - در قضا و قدر
---
# شمارهٔ ۲۴۷ - در قضا و قدر

<div class="b" id="bn1"><div class="m1"><p>خدای کار چو بر بنده‌ای فرو بندد</p></div>
<div class="m2"><p>به هرچه دست زند رنج دل بیفزاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر به طبع شود زود نزد همچو خودی</p></div>
<div class="m2"><p>ز بهر چیزی خوار و نژند باز آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو اعتقاد کند کز کسش نباید چیز</p></div>
<div class="m2"><p>خدای قدرت والای خویش بنماید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دست بنده ز حل و ز عقد چیزی نیست</p></div>
<div class="m2"><p>خدای بندد کار و خدای بگشاید</p></div></div>