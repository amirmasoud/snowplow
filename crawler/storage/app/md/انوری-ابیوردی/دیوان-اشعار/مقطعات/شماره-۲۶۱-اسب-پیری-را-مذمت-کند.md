---
title: >-
    شمارهٔ ۲۶۱ - اسب پیری را مذمت کند
---
# شمارهٔ ۲۶۱ - اسب پیری را مذمت کند

<div class="b" id="bn1"><div class="m1"><p>خسرو از اصطبل معمورت که آن معمور باد</p></div>
<div class="m2"><p>کام‌ور اعمار اسبان شیخ ابوعامر رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرکب میمون ادام الله توفیقه که هست</p></div>
<div class="m2"><p>یادگار نوح پیغمبر که در کشتی کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم ای پیر مبارک خیر مقدم مرحبا</p></div>
<div class="m2"><p>قصهٔ آن کو که گوش و چشم تو دید وشنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خبرهای صریر آسمان گوشت چه یافت</p></div>
<div class="m2"><p>وز خطرهای سپهری دیدهٔ سرت چه دید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر آن وقتی که عالم جمله اسبان داشتند</p></div>
<div class="m2"><p>مجلس شیخ‌الشیوخی سبزها چون می‌چرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حال آدم گوی و نوح و قصهٔ ذبح خلیل</p></div>
<div class="m2"><p>ناقهٔ صالح چه بود و رخش رستم چون دوید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهسوار سر اسری در شبی هفت آسمان</p></div>
<div class="m2"><p>بر براق تیز تک ره چون بپیمود و برید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیعت بوبکر و آن فضل اقیلونی چه بود</p></div>
<div class="m2"><p>مصلحت دید علی وان فتنها چون خوابنید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حیدر کرار حرب عمرو عنتر چون شکست</p></div>
<div class="m2"><p>رستم دستان صف گردان لشکر چون درید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اسب اندر خشم شد الحق ندانی تا چه گفت</p></div>
<div class="m2"><p>پشت دست از غبن من آنجا به دندان می‌گزید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت ای استغفرالله این سؤال از چون منی</p></div>
<div class="m2"><p>وه وه این اشکال بین کاین بر سر من آورید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتمش اسبا قدیما خرنه‌ای آخر بگوی</p></div>
<div class="m2"><p>تا مبارک مقدمت در دور عالم کی رسید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت تو بسیار ماندی هیچ می‌دانی کدام</p></div>
<div class="m2"><p>آن نخستین جانور کایزد تعالی آفرید</p></div></div>