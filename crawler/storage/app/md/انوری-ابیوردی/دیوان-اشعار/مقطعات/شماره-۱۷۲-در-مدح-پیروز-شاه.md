---
title: >-
    شمارهٔ ۱۷۲ - در مدح پیروز شاه
---
# شمارهٔ ۱۷۲ - در مدح پیروز شاه

<div class="b" id="bn1"><div class="m1"><p>آنکه او دست و دلت را سبب روزی کرد</p></div>
<div class="m2"><p>درگهت را در پیروزی و بهروزی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یافت از دست اجل جان گرامیش خلاص</p></div>
<div class="m2"><p>هر کرا خدمت جان‌پرور تو روزی کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای ولی‌نعمت احرار سوی نعمت و ناز</p></div>
<div class="m2"><p>آز را داعی جود تو ره‌آموزی کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با جهانی کفت آن کرد که با خاک و نبات</p></div>
<div class="m2"><p>باد نوروزی و باران شبانروزی کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فضلهٔ بزم توفراش به نوروز برفت</p></div>
<div class="m2"><p>باغ را مایه به دست آمد و نوروزی کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخت پیروز ترا گنبد فیروزهٔ چرخ</p></div>
<div class="m2"><p>تاقیامت سبب نصرت و پیروزی کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبدهٔ گوهر آن شاه که از گوشهٔ تخت</p></div>
<div class="m2"><p>سالها گوهر تاجش فلک‌افروزی کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پاسبانی جهان گر تو بگویی بکند</p></div>
<div class="m2"><p>فتنه بی‌عدل کزین پیش جانسوزی کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وز سراپردهٔ آن شاه کز انگشت نفاذ</p></div>
<div class="m2"><p>ماه را پرده دری کرد و قبادوزی کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از شب و روز میندیش که با تست بهم</p></div>
<div class="m2"><p>آنکه از زلف شبی کرد و ز رخ روزی کرد</p></div></div>