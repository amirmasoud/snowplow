---
title: >-
    شمارهٔ ۸۵ - در مدح سعدالدین و کیفیت سقطه و حسب حال خود
---
# شمارهٔ ۸۵ - در مدح سعدالدین و کیفیت سقطه و حسب حال خود

<div class="b" id="bn1"><div class="m1"><p>ای سعد سپهر دین کجایی</p></div>
<div class="m2"><p>کاثار سعادتت نهانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بازم ز زمانه کم گرفتی</p></div>
<div class="m2"><p>وین هم ز کیادت زمانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این عادت قلةالمبالات</p></div>
<div class="m2"><p>آیین کدام دوستانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین گونه بضاعت مودت</p></div>
<div class="m2"><p>در حمل کدام کاروانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را باری غم تو هر شب</p></div>
<div class="m2"><p>همخوابهٔ مغز استخوانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان روی که روزی از فراقت</p></div>
<div class="m2"><p>با سال تمام توامانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سالیست که دیدهٔ پر آبم</p></div>
<div class="m2"><p>بر طرف دریچه دیدبانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخسارهٔ کاه‌رنگم از اشک</p></div>
<div class="m2"><p>در هجر تو راه کهکشانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روزم سیهست از آنکه چشمم</p></div>
<div class="m2"><p>از آتش سینه پر دخانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خود صحبت اندساله بگذار</p></div>
<div class="m2"><p>گو مرد غریب ناتوانست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرچه زدهٔ سپهر پیرست</p></div>
<div class="m2"><p>آخر نه چو بخت ما جوانست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برخیزم و بنگرم که حالش</p></div>
<div class="m2"><p>در حبس تکبر از چه سانست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از دست مشو ز سقطهٔ من</p></div>
<div class="m2"><p>پای تو اگرچه در میانست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سری دارم که گر بگویم</p></div>
<div class="m2"><p>گویی بحقیقت آن چنانست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن شب که دو عالم از حوادث</p></div>
<div class="m2"><p>گویی که دو محنت آشیانست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>و اجرام نحوس را به یکبار</p></div>
<div class="m2"><p>در طالع عافیت قرانست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وز عکس شفق هوای گیتی</p></div>
<div class="m2"><p>یک معرکه لمعهٔ سنانست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتم که چو شب گران‌رکابست</p></div>
<div class="m2"><p>تدبیر می سبک عنانست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مهمان تو آمدیم یالیت</p></div>
<div class="m2"><p>یالیتم از آن دو میهمانست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا از در مجلست که خاکش</p></div>
<div class="m2"><p>همتای بهشت جاودانست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سر در کردم اشارتت گفت</p></div>
<div class="m2"><p>در صدر نشین که جایت آنست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من نیز به حکم آنکه حکمت</p></div>
<div class="m2"><p>بر جان و روان من روانست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بنشستم و گفتم ارچه صدر اوست</p></div>
<div class="m2"><p>عیبی نبود که میزبانست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>القصه چو جای خود بدیدم</p></div>
<div class="m2"><p>کز منطقه نیک بر کرانست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>با خود گفتم که انوری هی</p></div>
<div class="m2"><p>هرچند که خانهٔ فلانست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لیکن به حضور او که حدش</p></div>
<div class="m2"><p>حاضر شدن همه جهانست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دانی که تصدری بدین حد</p></div>
<div class="m2"><p>نه حد تو خام قلتبانست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فی‌الجمله ز خود خجل شدم نیک</p></div>
<div class="m2"><p>خود موجب خجلتم عیانست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اندازهٔ رسم دانی من</p></div>
<div class="m2"><p>داند آن کس که رسم دانست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر پای نشستم آخرالامر</p></div>
<div class="m2"><p>چونان که گمان همگنانست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پی کورکنان حریف جویان</p></div>
<div class="m2"><p>زانگونه که هیچکس ندانست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گفتم که چو شب سبکترک شد</p></div>
<div class="m2"><p>اکنون گه ساغر گرانست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون تو به سه گانه دست بردی</p></div>
<div class="m2"><p>برجستم و این سخن نشانست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از گوشهٔ طارمت که سمکش</p></div>
<div class="m2"><p>معیار عیار آسمانست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر خاک درت نثار کردم</p></div>
<div class="m2"><p>شخصی که برو نثار جانست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یعنی که گرم ز روی تمکین</p></div>
<div class="m2"><p>بر سدرهٔ منتهی مکانست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>درگاه سپهر صورتت را</p></div>
<div class="m2"><p>تا حشر سرم بر آستانست</p></div></div>