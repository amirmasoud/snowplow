---
title: >-
    شمارهٔ ۱۴۵ - در هجا
---
# شمارهٔ ۱۴۵ - در هجا

<div class="b" id="bn1"><div class="m1"><p>آن خداوندی که سال و ماه را</p></div>
<div class="m2"><p>تکیه بر اجزای روز و شب نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مر موالید جهان را سیزده</p></div>
<div class="m2"><p>اصل و فرع و منشاء و مطلب نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چار سفلی را از آن ام نام کرد</p></div>
<div class="m2"><p>نام آن نه علویان را آب نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچه از عالم بخیلی جمع کرد</p></div>
<div class="m2"><p>یک مکان‌شان مطعم و مشرب نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن بخیل آباد ممسک خانه را</p></div>
<div class="m2"><p>روز فطرت نام او نخشب نهاد</p></div></div>