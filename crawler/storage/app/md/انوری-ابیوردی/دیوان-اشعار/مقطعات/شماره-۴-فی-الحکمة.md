---
title: >-
    شمارهٔ ۴ - فی‌الحکمة
---
# شمارهٔ ۴ - فی‌الحکمة

<div class="b" id="bn1"><div class="m1"><p>نگر تا حلقهٔ اقبال ناممکن نجنبانی</p></div>
<div class="m2"><p>سلیما ابلها لابلکه مرحوما و مسکینا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنایی گرچه از وجه مناجاتی همی گوید</p></div>
<div class="m2"><p>به شعری در ز حرص آنکه یابد دیدهٔ بینا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که یارب مر سنایی را سنایی ده تو در حکمت</p></div>
<div class="m2"><p>چنان کز وی به رشک آید روان بوعلی سینا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ولیکن از طریق آرزو پختن خرد داند</p></div>
<div class="m2"><p>که با تخت زمرد بس نیاید کوشش مینا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برو جان پدر تن در مشیت ده که دیر افتد</p></div>
<div class="m2"><p>ز یاجوج تمنی رخنه در سد ولوشینا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به استعداد یابد هرکه از ما چیزکی یابد</p></div>
<div class="m2"><p>نه اندر بدو فطرت پیش از کان الفتی طینا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلی از جاهدوا یکسر به دست تست این رشته</p></div>
<div class="m2"><p>ولیک از جاهدوا هم برنخیزد هیچ بی‌فینا</p></div></div>