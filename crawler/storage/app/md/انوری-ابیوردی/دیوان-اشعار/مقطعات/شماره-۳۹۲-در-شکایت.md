---
title: >-
    شمارهٔ ۳۹۲ - در شکایت
---
# شمارهٔ ۳۹۲ - در شکایت

<div class="b" id="bn1"><div class="m1"><p>من از تاثیر این گردنده گردون</p></div>
<div class="m2"><p>بر این ساکن نیم یک لحظه ساکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا گویی جهان اینست خوش باش</p></div>
<div class="m2"><p>همی کوشم که خوش باشم ولیکن</p></div></div>