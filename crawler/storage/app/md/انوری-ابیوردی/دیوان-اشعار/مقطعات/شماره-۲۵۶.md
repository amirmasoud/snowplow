---
title: >-
    شمارهٔ ۲۵۶
---
# شمارهٔ ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>صاحبا سقطهٔ مبارک تو</p></div>
<div class="m2"><p>نه ز آسیب حادثات رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوش این واقعه چو حادث شد</p></div>
<div class="m2"><p>منهیی زاسمان به بنده دوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماجرایی از آن حکایت کرد</p></div>
<div class="m2"><p>بنده برگویدت چنان که شنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت دی خواجهٔ جهان زچمن</p></div>
<div class="m2"><p>ناگهانی چو سوی قصر چمید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر اندر میان آن حرکت</p></div>
<div class="m2"><p>چین دامن زخاک ره برچید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک در پایش اوفتاد وبه درد</p></div>
<div class="m2"><p>روی در کفش او همی مالید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یعنی از بنده در مکش دامن</p></div>
<div class="m2"><p>آسمان انبساط خاک بدید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غیرت غیر برد بر پایش</p></div>
<div class="m2"><p>قوت غیرتش چو درجنبید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخ ترش کرد و آستین بر زد</p></div>
<div class="m2"><p>سیلی خصم‌وار باز کشید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاک مسکین زبیم سیلی او</p></div>
<div class="m2"><p>مضطرب گشت و جرم در دزدید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پای میمونش از تزلزل خاک</p></div>
<div class="m2"><p>مگر از جای خویشتن بخزید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم از این بود آنکه وقت سحر</p></div>
<div class="m2"><p>دوش گیسوی شب زبن ببرید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم از این بود آنکه زاول روز</p></div>
<div class="m2"><p>صبح برخویشتن قبا بدرید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا ربش هیچ تلخییی مچشان</p></div>
<div class="m2"><p>که از این سهل شربتی که چشید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نور بر جرم آفتاب فسرد</p></div>
<div class="m2"><p>خوی ز اندام آسمان بچکید</p></div></div>