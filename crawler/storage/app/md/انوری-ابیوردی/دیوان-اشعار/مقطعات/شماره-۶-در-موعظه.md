---
title: >-
    شمارهٔ ۶ - در موعظه
---
# شمارهٔ ۶ - در موعظه

<div class="b" id="bn1"><div class="m1"><p>هرکه سعی بد کند در حق خلق</p></div>
<div class="m2"><p>همچو سعی خویش بد بیند جزا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچنین فرمود ایزد در نبی</p></div>
<div class="m2"><p>لیس للانسان الا ما سعی</p></div></div>