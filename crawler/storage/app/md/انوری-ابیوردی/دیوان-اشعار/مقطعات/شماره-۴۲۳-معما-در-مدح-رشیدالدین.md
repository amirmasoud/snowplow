---
title: >-
    شمارهٔ ۴۲۳ - معما در مدح رشیدالدین
---
# شمارهٔ ۴۲۳ - معما در مدح رشیدالدین

<div class="b" id="bn1"><div class="m1"><p>خرد دوش از من بپرسید و گفتا</p></div>
<div class="m2"><p>که ای پیش نطق تو منطق فسانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگو چیست آن طرفه صیاد دلها</p></div>
<div class="m2"><p>که از لفظ و معنیش دامست و دانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم گفت خاموش تا من بگویم</p></div>
<div class="m2"><p>که من حاکم عدلم اندر میانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوا و نفاق از میان برگرفتم</p></div>
<div class="m2"><p>کلام رشید آن خداوند خانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رشید اختیار زمانه است و طبعش</p></div>
<div class="m2"><p>در این فن چو در زلف ژولیده شانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قوی باشد اندر زمان تو الحق</p></div>
<div class="m2"><p>که گردد کسی اختیار زمانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زه تربیت بر کمانی نهادی</p></div>
<div class="m2"><p>که آمد همه تیر او بر نشانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بمانید با یکدگر تا جهان را</p></div>
<div class="m2"><p>چهار آستانست و نه آسمانه</p></div></div>