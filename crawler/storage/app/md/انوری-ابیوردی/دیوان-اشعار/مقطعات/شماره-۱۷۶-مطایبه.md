---
title: >-
    شمارهٔ ۱۷۶ - مطایبه
---
# شمارهٔ ۱۷۶ - مطایبه

<div class="b" id="bn1"><div class="m1"><p>جهان گر مضطرب شد گو همی شو</p></div>
<div class="m2"><p>من و می تا جهان آرام گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم را انده امروز بس نیست</p></div>
<div class="m2"><p>که می اندوه فردا وام گیرد</p></div></div>