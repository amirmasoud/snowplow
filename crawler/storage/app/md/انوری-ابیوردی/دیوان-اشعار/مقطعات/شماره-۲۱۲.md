---
title: >-
    شمارهٔ ۲۱۲
---
# شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>بیخ دو غماز برانداختند</p></div>
<div class="m2"><p>اصل بشد فرع چه تن می‌زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسعد بندار به دوزخ رسید</p></div>
<div class="m2"><p>مخلص غزال چه فن می‌زند</p></div></div>