---
title: >-
    شمارهٔ ۶۸ - در هجا
---
# شمارهٔ ۶۸ - در هجا

<div class="b" id="bn1"><div class="m1"><p>شمس را چیزکی است بر گردن</p></div>
<div class="m2"><p>واندرور چیزها نه یک چیزست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ دانی درو چه شاید بود</p></div>
<div class="m2"><p>باش در زیر ریش او تیزست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچه بر گردن است ترکاج است</p></div>
<div class="m2"><p>وانچه در زیر ریش تر تیزست</p></div></div>