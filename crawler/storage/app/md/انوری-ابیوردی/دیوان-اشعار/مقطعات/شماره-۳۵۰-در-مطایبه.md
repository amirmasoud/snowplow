---
title: >-
    شمارهٔ ۳۵۰ - در مطایبه
---
# شمارهٔ ۳۵۰ - در مطایبه

<div class="b" id="bn1"><div class="m1"><p>عقل صد مسهل به طبعم بیش دارد</p></div>
<div class="m2"><p>تا چنین در نظم و نثرش کرد نرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بدانستم که بی اسهال او</p></div>
<div class="m2"><p>مجلس سردان نخواهد گشت گرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کافرم گر قطره‌ای زین پس ریم</p></div>
<div class="m2"><p>در دهانشان جز به آزرم و به شرم</p></div></div>