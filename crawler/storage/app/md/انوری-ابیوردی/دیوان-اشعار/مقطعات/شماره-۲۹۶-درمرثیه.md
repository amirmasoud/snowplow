---
title: >-
    شمارهٔ ۲۹۶ - درمرثیه
---
# شمارهٔ ۲۹۶ - درمرثیه

<div class="b" id="bn1"><div class="m1"><p>آن خواجه کز آستین رغبت</p></div>
<div class="m2"><p>دست کرم بزرگوارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برداشت زخاک عالمی را</p></div>
<div class="m2"><p>در خاک نهاد روزگارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ننشست نظیر او ولیکن</p></div>
<div class="m2"><p>بنشاند عزای پایدارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صدگونه چو من یتیم احسان</p></div>
<div class="m2"><p>برخاک دریغ یادگارش</p></div></div>