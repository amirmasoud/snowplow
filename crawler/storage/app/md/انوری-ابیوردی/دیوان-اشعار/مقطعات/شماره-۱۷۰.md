---
title: >-
    شمارهٔ ۱۷۰
---
# شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>به خدایی که درسپهر بلند</p></div>
<div class="m2"><p>اختر و مهر و مه مرکب کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دایهٔ صنع و لطف قدرت او</p></div>
<div class="m2"><p>رونق حسن تو مرتب کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که جهان بر من غریب اسیر</p></div>
<div class="m2"><p>اشتیاق جمال تو شب کرد</p></div></div>