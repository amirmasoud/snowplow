---
title: >-
    شمارهٔ ۲۵۷ - فی الاشتیاق
---
# شمارهٔ ۲۵۷ - فی الاشتیاق

<div class="b" id="bn1"><div class="m1"><p>به خدایی که دست قدرت او</p></div>
<div class="m2"><p>نیل شب برعزار روز کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کین برادر ندید یک لحظه</p></div>
<div class="m2"><p>بی‌شما راحت و نخواهد دید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌شما هیچ بر گل دل او</p></div>
<div class="m2"><p>باد شبگیری صبا نوزید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ یک از دریچهٔ جانش</p></div>
<div class="m2"><p>مرغ لذات و عیش خوش نپرید</p></div></div>