---
title: >-
    شمارهٔ ۳۶۸
---
# شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>به خدایی که قائمست به ذات</p></div>
<div class="m2"><p>نه چو ما بلکه قایم و قیوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که مرا در فراق خدمت تو</p></div>
<div class="m2"><p>جان ز غم مظلمست و تن مظلوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز مرحوم روزگار شدم</p></div>
<div class="m2"><p>تا که گشتم ز خدمتت محروم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکه محروم شد ز خدمت تو</p></div>
<div class="m2"><p>روزگارش چنین کند مرحوم</p></div></div>