---
title: >-
    شمارهٔ ۲۳۶ - در نکوهش روزگار
---
# شمارهٔ ۲۳۶ - در نکوهش روزگار

<div class="b" id="bn1"><div class="m1"><p>یک چند روزگار نه از راه مکرمت</p></div>
<div class="m2"><p>بر ما دری ز نعمت گیتی گشاده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون چیز اندکی به هم افتاد باز برد</p></div>
<div class="m2"><p>گفتی که نزد ما به امانت نهاده بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وامروز هرکه گویدم آن نیم ثروتی</p></div>
<div class="m2"><p>کز مادر زمانه به تدریج زاده بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون با تو نیست گویمش آن بازخواست زود</p></div>
<div class="m2"><p>گویی دهنده از سر جودی نداده بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردون چو سگ به فضلهٔ خود بازگشت کرد</p></div>
<div class="m2"><p>بیچاره او که کارش با این فتاده بود</p></div></div>