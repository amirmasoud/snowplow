---
title: >-
    شمارهٔ ۵۸ - حساد او را به تهمتی منسوب کردند قسمیات در نفی تهمت و ذم شاعری و استغفار گوید
---
# شمارهٔ ۵۸ - حساد او را به تهمتی منسوب کردند قسمیات در نفی تهمت و ذم شاعری و استغفار گوید

<div class="b" id="bn1"><div class="m1"><p>بدان خدای که در جست و جوی قدرت او</p></div>
<div class="m2"><p>مسافران فلک را قدم بفرسودست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دست احمد مرسل به کافران قریش</p></div>
<div class="m2"><p>هزار معجزهٔ رنگ رنگ بنمودست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ناودان قضا آب حکم بگشادست</p></div>
<div class="m2"><p>به لاژورد بقا بام چرخ اندودست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمال لم یزل و ذات لایزالی اوی</p></div>
<div class="m2"><p>ز هرچه نسبت نقصان بود برآسودست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مقدسی است که آسیب دامن امکان</p></div>
<div class="m2"><p>بساط بارگه کبریاش نبسودست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز راه حکمت و رحمت عموم اشیا را</p></div>
<div class="m2"><p>طریق کسب کمالات خاص بنمودست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشاعل فلکی را ز کارخانهٔ صنع</p></div>
<div class="m2"><p>بهین و خوبترین رنگ و شکل فرمودست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان که طرهٔ شب را به قهر شانه زدست</p></div>
<div class="m2"><p>به لطف آینهٔ جرم ماه بزدودست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز عدل شاملش اندر مقام حیز خاک</p></div>
<div class="m2"><p>نهاده هریکی از چار طبع و نغنودست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خمیرمایهٔ بخشش به خاک بخشیدست</p></div>
<div class="m2"><p>برآنکه مرجع او خاک شد نبخشودست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوار روح به چوگان یای نسبت او</p></div>
<div class="m2"><p>ز کوی گردون گوی کمال بربودست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درازدستی ادراک و تیزگامی وهم</p></div>
<div class="m2"><p>طناب نوبتی حضرتش نه پیمودست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جناب قدرت او را به قدر وسعت نطق</p></div>
<div class="m2"><p>زبان سوسن و طوطی همیشه بستودست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کمین سلطنتش در مصاف کون و فساد</p></div>
<div class="m2"><p>سنان لاله به خون دلش بیالودست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سیاه روی سپهر کبود کسوت را</p></div>
<div class="m2"><p>رخش ز زنگ کدورت نخست بزدودست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پس از خزانهٔ حسن و جمال خورشیدش</p></div>
<div class="m2"><p>کفاف حسن و زکوة جمال فرمودست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیاض روز به پالونهٔ هوای مشف</p></div>
<div class="m2"><p>هزار سال بر این تیره خاک پالودست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گهی به خرج بخار از بحار کم کردست</p></div>
<div class="m2"><p>گهی به دخل دخان بر اثیر بفزودست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ترا که میر خراسانی از ره تقدیم</p></div>
<div class="m2"><p>بر آسمان و زمین قدر و جاه افزودست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که انوری را بی‌خدمت مبارک تو</p></div>
<div class="m2"><p>هرآنچه دیده ندیدست و گوش نشنودست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در این سه سال چه در خواب و چه به بیداری</p></div>
<div class="m2"><p>خیال رایت و آواز نوبتت بودست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شکستهای امانی به عشوه می‌بسته است</p></div>
<div class="m2"><p>درشتهای حوادث به حیله می‌بودست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کنون حواشی جانش از قدوم فرخ تو</p></div>
<div class="m2"><p>چو برگ گل همه شادیش توده بر تودست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که صورتی که ز من بنده آشنایی کرد</p></div>
<div class="m2"><p>نه آنکه از لب من هیچ گوش نشنودست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه بر زبان گذرانیده‌ام نه بر خاطر</p></div>
<div class="m2"><p>نه بر عقیدت من بنده هرگز این بودست</p></div></div>