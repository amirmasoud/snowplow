---
title: >-
    شمارهٔ ۷۶ - لطیفه
---
# شمارهٔ ۷۶ - لطیفه

<div class="b" id="bn1"><div class="m1"><p>ای سروری که کوکبهٔ کبریات را</p></div>
<div class="m2"><p>کمتر جنیبت ابلق ایام سرکشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رای تو در نظام ممالک براستی</p></div>
<div class="m2"><p>تیری که جیب گنبد گردونش ترکشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اکنون که از گشاد فلک بر مسام ابر</p></div>
<div class="m2"><p>پیکان باد را گذر تیر آرشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وز برف ریزه گوشهٔ هر ابر پاره‌ای</p></div>
<div class="m2"><p>تیغست گوییا که به گوهر منقشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برحسب حال مطلع شعری گزیده‌ام</p></div>
<div class="m2"><p>واورده‌ام به صورت تضمین و بس خوشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویم کسی که چهرهٔ روزی چنین بدید</p></div>
<div class="m2"><p>خاصه چنین که طرهٔ شبها مشوشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر خاطرش هر آینه این شعر بگذرد</p></div>
<div class="m2"><p>کامروز وقت باده و خرگاه و آتشست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چندان بقات باد ز تاثیر نه سپهر</p></div>
<div class="m2"><p>کاندر زمانه طبع چهار و جهت ششست</p></div></div>