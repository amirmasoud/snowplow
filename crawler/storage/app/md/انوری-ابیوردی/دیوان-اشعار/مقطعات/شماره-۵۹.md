---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>عاقلا از سر جهان برخیز</p></div>
<div class="m2"><p>که نه معشوقهٔ وفادارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیر کامروز بر سر گنجی</p></div>
<div class="m2"><p>پا نه فردات بر دم مارست</p></div></div>