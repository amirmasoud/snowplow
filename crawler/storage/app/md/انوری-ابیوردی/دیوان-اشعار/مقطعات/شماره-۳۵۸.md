---
title: >-
    شمارهٔ ۳۵۸
---
# شمارهٔ ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>ای از برادر و پدر افزون دوبار صد</p></div>
<div class="m2"><p>وز تیر آسمان بتازی چهار کم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بفرست حورزاده به حکم دو سه ستیز</p></div>
<div class="m2"><p>با چنبر مصحف و بیخی بدان به هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بادا بقای نام تو چندان به روزگار</p></div>
<div class="m2"><p>کاید برون ز صورت بی‌دو دویست کم</p></div></div>