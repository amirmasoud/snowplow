---
title: >-
    شمارهٔ ۴۰۰ - در مدیح
---
# شمارهٔ ۴۰۰ - در مدیح

<div class="b" id="bn1"><div class="m1"><p>به خدایی که ذات لم یزلش</p></div>
<div class="m2"><p>باشد از سر بندگان آگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست صنعتش ز اقتدار نهد</p></div>
<div class="m2"><p>بر سر آفتاب و ماه کلاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زر فشاند ز صبح هر روزی</p></div>
<div class="m2"><p>در خم این زمردین خرگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رسولی که بد سبابهٔ او</p></div>
<div class="m2"><p>سبب جامه خرقه کردن ماه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به امینی که آورید بدو</p></div>
<div class="m2"><p>ز اسمان امر و نهی بی‌اکراه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کتابی که تا بدو داریم</p></div>
<div class="m2"><p>از گناهان به روز حشر گواه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کلامی که مهر ایمانست</p></div>
<div class="m2"><p>چیست آن لا اله الا الله</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که اگر هست یا بخواهد بود</p></div>
<div class="m2"><p>ملک و دین را نظیر همچو تو شاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا جهان باشد از تو نازان باد</p></div>
<div class="m2"><p>رایت و چتر و تخت و تاج و کلاه</p></div></div>