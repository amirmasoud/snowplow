---
title: >-
    شمارهٔ ۱۶۲ - کتاب و کلاهی نزد بزرگی داشت در تقاضای آن گوید
---
# شمارهٔ ۱۶۲ - کتاب و کلاهی نزد بزرگی داشت در تقاضای آن گوید

<div class="b" id="bn1"><div class="m1"><p>به کلاهی بزرگ کرد مرا</p></div>
<div class="m2"><p>آنکه گیتی به چشمشس آمد خرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه آب کلاهداری چرخ</p></div>
<div class="m2"><p>آب دستار خواجگیش ببرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که پیشش کمر به خدمت بست</p></div>
<div class="m2"><p>بر کله گوشهٔ زمانه سپرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>... در زهرهٔ سپهر نمود</p></div>
<div class="m2"><p>تا کلاهه بخورد و لب بسترد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس چو از قلهٔ‌المبالاتش</p></div>
<div class="m2"><p>پس از آن کس مرا به کس نشمرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست از صحبتم چنان بکشید</p></div>
<div class="m2"><p>پای بر فرق من چنان بفشرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که نه محرم شدم به شادی و غم</p></div>
<div class="m2"><p>نه حریف آمدم به صافی و درد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم آن را کله چگونم نهم</p></div>
<div class="m2"><p>که کلاهی ببایدش زد و برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیز پیرا که راه ما غلط است</p></div>
<div class="m2"><p>به سر راه باز گرد چو کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن جوان بخت را بپرس و بگوی</p></div>
<div class="m2"><p>که سفینه بده کلاه بمرد</p></div></div>