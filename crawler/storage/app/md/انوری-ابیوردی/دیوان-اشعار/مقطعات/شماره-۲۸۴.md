---
title: >-
    شمارهٔ ۲۸۴
---
# شمارهٔ ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>روزم از روز بهتر است اکنون</p></div>
<div class="m2"><p>از مراعات شمس دین فیروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جاودان از فلک خطابش این</p></div>
<div class="m2"><p>کی بر اعدا و اولیا پیروز</p></div></div>