---
title: >-
    شمارهٔ ۴۴۹ - در مدح امیر فخرالدین ابوالمفاخر آبی
---
# شمارهٔ ۴۴۹ - در مدح امیر فخرالدین ابوالمفاخر آبی

<div class="b" id="bn1"><div class="m1"><p>ای به تدبیر قطب آن گردون</p></div>
<div class="m2"><p>که ز تقدیر ساختست جدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وی ز تشویر خاطرت خورشید</p></div>
<div class="m2"><p>غوطها خورده در تموج خوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه مکنون خطهٔ اشیاست</p></div>
<div class="m2"><p>همه با مکنت تو ادنی شییء</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حکمت اندر نفاذ گشته چنان</p></div>
<div class="m2"><p>که نگنجد در انقیادش کی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظل جاهت از آن کشیده‌ترست</p></div>
<div class="m2"><p>که کند دور روزگارش طی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیر حکمت از آن سریع‌ترست</p></div>
<div class="m2"><p>که برد مسرع ضمیرش پی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر تقلد کنی عمارت عصر</p></div>
<div class="m2"><p>نشود هیچ‌کس خراب از می</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آدم از نسبت وجود تو یافت</p></div>
<div class="m2"><p>اختصاص خلقته بیدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون عنان قلم روان کردی</p></div>
<div class="m2"><p>آب گردد روان صاحب ری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون رکاب کرم گران کردی</p></div>
<div class="m2"><p>خاک بوسد عظام حاتم طی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قدرتت گفت روز عرض الست</p></div>
<div class="m2"><p>چون جدا کرد اخطل از اخطی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کای علی خرج این حشم برگیست</p></div>
<div class="m2"><p>همتت گفت قد ضمنت علی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دوش با آسمان همی گفتم</p></div>
<div class="m2"><p>بر سبیل سؤال مطلب ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که مدار حیات عالم کیست</p></div>
<div class="m2"><p>روی سوی تو کرد و گفتا وی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتم این را دلیل باید گفت</p></div>
<div class="m2"><p>هیچ دانی که می چه گویی هی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>میر آبست و حق همی گوید</p></div>
<div class="m2"><p>و من الماء کل شیء حی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا که نی را چو سرو نیست قوام</p></div>
<div class="m2"><p>در بهار و تموز و آذر و دی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باد پیشت جهان چو سرو به پای</p></div>
<div class="m2"><p>پای تا سر کمر ببسته چو نی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پوست بر دشمنت کفن گشته</p></div>
<div class="m2"><p>همچو بر کرم قز تراکم قی</p></div></div>