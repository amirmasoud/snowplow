---
title: >-
    شمارهٔ ۲۴۴ - شراب خواهد
---
# شمارهٔ ۲۴۴ - شراب خواهد

<div class="b" id="bn1"><div class="m1"><p>شاهدی دارم ای بزرگ چنانک</p></div>
<div class="m2"><p>چاکرش آفتاب می‌باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دلم تل سیم او بیند</p></div>
<div class="m2"><p>یک جهان زر ناب می‌باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشود راست تا بود هشیار</p></div>
<div class="m2"><p>گند مستی خراب می‌باید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ستونم رسد به خیمهٔ او</p></div>
<div class="m2"><p>سه قدح می طناب می‌باید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقل و اسباب و لوت حاصل شد</p></div>
<div class="m2"><p>یک صراحی شراب می‌باید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو بده تا ترا ثواب بود</p></div>
<div class="m2"><p>گر دلت را ثواب می‌باید</p></div></div>