---
title: >-
    شمارهٔ ۸۹ - ناصرالدین طاهر را درد دندان عارض گشته انوری این قطعه هنگام عیادت او گفته و در هر بیت‌التزام لفظ دندان نموده است
---
# شمارهٔ ۸۹ - ناصرالدین طاهر را درد دندان عارض گشته انوری این قطعه هنگام عیادت او گفته و در هر بیت‌التزام لفظ دندان نموده است

<div class="b" id="bn1"><div class="m1"><p>ای به دندان دولت آمده خوش</p></div>
<div class="m2"><p>درد دندانت هیچ بهتر هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارد از غصه آسمان دندان</p></div>
<div class="m2"><p>بر که بر نفس همتت پیوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زانکه هرگز به هیچ دندان مزد</p></div>
<div class="m2"><p>بر سر خوان آسمان ننشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیز دندانی حرارت می</p></div>
<div class="m2"><p>درد دندانت چون به خیره بخست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز بنمود آسمان دندان</p></div>
<div class="m2"><p>تا الم باز پس کشیدی دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر دندان سپید کرد قضا</p></div>
<div class="m2"><p>گفتش ای جور خوی عشوه‌پرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب دندان حریفی آوردی</p></div>
<div class="m2"><p>کوش تا رایگان توانی جست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از چنین صید برمکش دندان</p></div>
<div class="m2"><p>مرغ چربست و آشیانی پست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من نگویم که جامه در دندان</p></div>
<div class="m2"><p>زانتقامش به جان بخواهی رست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خیز و دندان‌کنان به خدمت شو</p></div>
<div class="m2"><p>آسمان دیرتر میان دربست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت هم عشوه پشت دست بزد</p></div>
<div class="m2"><p>دو سه دندان آسمان بشکست</p></div></div>