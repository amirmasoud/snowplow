---
title: >-
    شمارهٔ ۳۵۱ - کیسه‌ای به حکیم وعده کرده‌اند آن را با کاردی طلب می‌کند
---
# شمارهٔ ۳۵۱ - کیسه‌ای به حکیم وعده کرده‌اند آن را با کاردی طلب می‌کند

<div class="b" id="bn1"><div class="m1"><p>ای کمال زمان بیا و ببین</p></div>
<div class="m2"><p>که ز عشقت چگونه می‌سوزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با بهار رخت تواند گفت</p></div>
<div class="m2"><p>شب یلداکه روز نوروزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در فراق رخ چو خورشیدت</p></div>
<div class="m2"><p>روشنایی نمی‌دهد روزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کیسه‌ای دادیم در این شبها</p></div>
<div class="m2"><p>که همی وام صحبت اندوزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزها رفت و من نمی‌دانم</p></div>
<div class="m2"><p>که بر آن کیسه کیسه‌ای دوزم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یارب از کاردی بود با آن</p></div>
<div class="m2"><p>که بدان کین دشمنان توزم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر چو سرو از نشاط بفرازم</p></div>
<div class="m2"><p>رخ ز شادی چو گل برافروزم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وگر این کار هست بیهوده</p></div>
<div class="m2"><p>تن زن آنگاه کاسهٔ یوزم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سایه بر کار این سخن مفکن</p></div>
<div class="m2"><p>زانکه چون سایه بر تو آموزم</p></div></div>