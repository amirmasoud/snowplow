---
title: >-
    شمارهٔ ۴۵۶ - در شکایت و تقاضای الطاف صاحب
---
# شمارهٔ ۴۵۶ - در شکایت و تقاضای الطاف صاحب

<div class="b" id="bn1"><div class="m1"><p>ای صاحبی که صدر وزارت ز جاه تو</p></div>
<div class="m2"><p>با اوج آفتاب زند لاف برتری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرمان تو که زیر رکابش رود جهان</p></div>
<div class="m2"><p>با روزگار سوده عنان در برابری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر هر که ابر عاطفتت سایه افکند</p></div>
<div class="m2"><p>تا حشر باقیست چو دریا توانگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست تو رازقست و ضمیر تو غیب‌دان</p></div>
<div class="m2"><p>بی‌دعوی خدایی و لاف پیمبری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>احوال مبرمی و گدایی شاعران</p></div>
<div class="m2"><p>دانند همگان که مه شعر و مه شاعری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد مدتی که عزم زمین‌بوس تازه کرد</p></div>
<div class="m2"><p>در خدمت مبارک میمونت انوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واکنون بر آستانهٔ عالیت روز و شب</p></div>
<div class="m2"><p>کش آسمانه باد پر از ماه و مشتری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از لطف شامل تو طمع دارد این قدر</p></div>
<div class="m2"><p>کاخر چه می‌کنی و کجایی چه می‌خوری</p></div></div>