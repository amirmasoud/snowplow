---
title: >-
    شمارهٔ ۴۲۵ - لطیفه
---
# شمارهٔ ۴۲۵ - لطیفه

<div class="b" id="bn1"><div class="m1"><p>مرا دی یاسمن پیغام دادست</p></div>
<div class="m2"><p>به تو ای صاحب و صدر یگانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هر نوعی سخن گفتست پنهان</p></div>
<div class="m2"><p>غرض را درج کرده در میانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه فرمایی کنون پیغام او را</p></div>
<div class="m2"><p>به سمع تو رساند بنده یانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا گفتست فردا کاتش صبح</p></div>
<div class="m2"><p>زند از کورهٔ مشرق زبانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگو او را که می‌گوید فلانی</p></div>
<div class="m2"><p>که ای خلقت چو جودت بی‌کرانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو در سالی مرا ده روز افزون</p></div>
<div class="m2"><p>نباشد نوبت از گشت زمانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس از ده روز خود تاخیر کردم</p></div>
<div class="m2"><p>شوم تا سال دیگر آفسانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون درخواستی دارم ز خلقت</p></div>
<div class="m2"><p>همانا ناورد با من بهانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو روزک نیز در صحن چمن آی</p></div>
<div class="m2"><p>بگو تا مطرب آرند و چغانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به زیر سایهٔ گل شادمان باش</p></div>
<div class="m2"><p>مرا از لطف خود کن شادمانه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون من بهر تو آیم خوب نبود</p></div>
<div class="m2"><p>من اندر باغ و تو در تاب خانه</p></div></div>