---
title: >-
    شمارهٔ ۳۴۹ - در بیان هنرهای خود و جهل ابناء عصر
---
# شمارهٔ ۳۴۹ - در بیان هنرهای خود و جهل ابناء عصر

<div class="b" id="bn1"><div class="m1"><p>گرچه دربستم در مدح و غزل یکبارگی</p></div>
<div class="m2"><p>ظن مبر کز نظم الفاظ و معانی قاصرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلکه در هر نوع کز اقران من داند کسی</p></div>
<div class="m2"><p>خواه جزوی گیر آن را خواه کلی قادرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منطق و موسیقی و هیات بدانم اندکی</p></div>
<div class="m2"><p>راستی باید بگویم با نصیب وافرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وز الهی آنچه تصدیقش کند عقل صریح</p></div>
<div class="m2"><p>گر تو تصدیقش کنی بر شرح و بسطش ماهرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وز ریاضی مشکلی چندم به خلوت حل شده است</p></div>
<div class="m2"><p>واندر آن جز واهب از توفیق کس نه یاورم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز طبیعی رمز چند ار چند بی‌تشویر نیست</p></div>
<div class="m2"><p>کشف دانم کرد اگر حاسد نباشد ناظرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیستم بیگانه از اعمال و احکام نجوم</p></div>
<div class="m2"><p>در بیان او به غایت اوستاد و ماهرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ز لقمان و فلاطون نیستم کم در حکم</p></div>
<div class="m2"><p>ور همی باور نداری رنجه شو من حاضرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با بزرگان مستفیدم با فرودستان مفید</p></div>
<div class="m2"><p>عالم تحصیل را هم وارد و هم صادرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غصه ها دارم ز نقصان از همه نوعی ولیک</p></div>
<div class="m2"><p>زین یکی آوخ که نزدیک تو مردی شاعرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این همه بگذار با شعر مجرد آمدم</p></div>
<div class="m2"><p>چون سنایی هستم آخر گرنه همچون صابرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هریکی آخر از ایشان بی‌کفافی نیستند</p></div>
<div class="m2"><p>این منم کز مفلسی چون روز روشن ظاهرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خود هنر در عهد ما عیب است اگرنه این سخن</p></div>
<div class="m2"><p>می‌کند برهان که من شاعر نیم بل ساحرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خاطرم در ستر دیوان دختران دارد چو حور</p></div>
<div class="m2"><p>زهره‌شان پرورده در آغوش طبع زاهرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر ز یک خاطب یکی را روز تزویج و قبول</p></div>
<div class="m2"><p>برتر از احسنت کابین یافتستم کافرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در چنین قحط مروت با چنین آزادگان</p></div>
<div class="m2"><p>وای من گر نان خورندی دختران خاطرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اینکه می‌گویم شکایت نیست شرح حالتست</p></div>
<div class="m2"><p>شکر یزدان را که اندر هرچه هستم شاکرم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در غرض از آفرینش غایتم بس اولم</p></div>
<div class="m2"><p>گرچه در سلک وجود از روی صورت آخرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قدر من صاحب قوام‌الدین حسن داند از آنک</p></div>
<div class="m2"><p>صدر او را یادگار از ناصرالدین طاهرم</p></div></div>