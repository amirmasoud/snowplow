---
title: >-
    شمارهٔ ۱۹۹
---
# شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>بسا سخن که مرا بود وان نگفته بماند</p></div>
<div class="m2"><p>ز من نخواست کس آنرا و آن نهفته بماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن که گفته بود همچو در سفته بود</p></div>
<div class="m2"><p>مرا رواست گر این در من نسفته بماند</p></div></div>