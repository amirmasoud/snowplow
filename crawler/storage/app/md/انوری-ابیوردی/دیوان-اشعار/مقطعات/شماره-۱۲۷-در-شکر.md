---
title: >-
    شمارهٔ ۱۲۷ - در شکر
---
# شمارهٔ ۱۲۷ - در شکر

<div class="b" id="bn1"><div class="m1"><p>من به الماس طبع تا بزیم</p></div>
<div class="m2"><p>گوهر مدحت تو خواهم سفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو عطا گر دهی و گر ندهی</p></div>
<div class="m2"><p>بالله ار جز ثنات خواهم گفت</p></div></div>