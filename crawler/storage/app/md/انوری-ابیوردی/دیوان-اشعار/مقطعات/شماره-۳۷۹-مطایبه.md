---
title: >-
    شمارهٔ ۳۷۹ - مطایبه
---
# شمارهٔ ۳۷۹ - مطایبه

<div class="b" id="bn1"><div class="m1"><p>چو غزنینی به محشر زنده گردد</p></div>
<div class="m2"><p>بسنجد طاعتش ایزد به میزان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کم آید طاعتش گوید خدایا</p></div>
<div class="m2"><p>ترازو چشمه دارد سر بگردان</p></div></div>