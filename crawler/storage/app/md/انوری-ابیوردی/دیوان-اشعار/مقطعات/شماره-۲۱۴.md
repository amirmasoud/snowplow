---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>به خدایی که دست قدرت او</p></div>
<div class="m2"><p>ناوک مجری قدر فکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست قهرش مگر ز وعد و وعید</p></div>
<div class="m2"><p>جوز در مغز معصیت شکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کز ملافات مردک جاهل</p></div>
<div class="m2"><p>بیخ شادی ز جان و دل بکند</p></div></div>