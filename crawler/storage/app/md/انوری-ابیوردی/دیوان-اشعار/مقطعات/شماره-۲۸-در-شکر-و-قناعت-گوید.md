---
title: >-
    شمارهٔ ۲۸ - در شکر و قناعت گوید
---
# شمارهٔ ۲۸ - در شکر و قناعت گوید

<div class="b" id="bn1"><div class="m1"><p>درین دو روزه توقف که بو که خود نبود</p></div>
<div class="m2"><p>درین مقام فسوس و درین سرای فریب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا قبول کنم از کس آنکه عاقبتش</p></div>
<div class="m2"><p>ز خلق سرزنشم باشد از خدای عتیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا خدای تعالی ز آسیای فراز</p></div>
<div class="m2"><p>که عقل حاصل آنرا نیاورد به حسیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو می‌دهد همه چیزی به قدر حاجت من</p></div>
<div class="m2"><p>چنان که بی‌خبر سیب ماه رنگ به سیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بهر حفظ حیات آنچه بایدم ز کفاف</p></div>
<div class="m2"><p>ز بهر کسب کمال آنچه بایدم ز کتیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار سال اگر عمر من بود به مثل</p></div>
<div class="m2"><p>مرا نیاز نیاید به آسیای نشیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دو نعمتست مرا کان ملوک را نبود</p></div>
<div class="m2"><p>به روز راحت شکر و به روز رنج شکیب</p></div></div>