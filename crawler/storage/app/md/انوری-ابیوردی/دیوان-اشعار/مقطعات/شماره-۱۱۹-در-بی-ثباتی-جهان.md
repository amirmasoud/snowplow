---
title: >-
    شمارهٔ ۱۱۹ - در بی ثباتی جهان
---
# شمارهٔ ۱۱۹ - در بی ثباتی جهان

<div class="b" id="bn1"><div class="m1"><p>جریده‌ایست نهاد سیه سپید جهان</p></div>
<div class="m2"><p>که روزگار درو جز قضای بد ننوشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان نثار گل تیره کرد آب سیاه</p></div>
<div class="m2"><p>وزان زمانه نهفت آنکه سالها بسرشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمانه روزی چند از طریق عشوه گری</p></div>
<div class="m2"><p>دهد بهار بقای ترا جمال بهشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ولیک باد خزانش چو شاخ عمر شکست</p></div>
<div class="m2"><p>به موت بستر و بالین کند ز خاک و ز خشت</p></div></div>