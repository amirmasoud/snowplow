---
title: >-
    شمارهٔ ۲۲۷ - ایضا طلب شراب کند
---
# شمارهٔ ۲۲۷ - ایضا طلب شراب کند

<div class="b" id="bn1"><div class="m1"><p>ای بزرگی که کلک وهمت تو</p></div>
<div class="m2"><p>روی امید را چو لاله کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از یک احسان تو شکسته‌دلان</p></div>
<div class="m2"><p>جبر کسر هزار ساله کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نماز در تو بگرایند</p></div>
<div class="m2"><p>آن کسان کز نیاز ناله کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قحط فرموده قلتبانی چند</p></div>
<div class="m2"><p>که خری را به یک نواله کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در وثاق من آمدند امروز</p></div>
<div class="m2"><p>تا بلا را به من حواله کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دفع ایشان نمی‌توانم کرد</p></div>
<div class="m2"><p>جز به چیزی که در پیاله کنند</p></div></div>