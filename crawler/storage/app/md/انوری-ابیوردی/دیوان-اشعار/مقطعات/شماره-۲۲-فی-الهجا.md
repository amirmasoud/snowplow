---
title: >-
    شمارهٔ ۲۲ - فی‌الهجا
---
# شمارهٔ ۲۲ - فی‌الهجا

<div class="b" id="bn1"><div class="m1"><p>گفته بودی که کاه و جو بدهم</p></div>
<div class="m2"><p>چون ندادی از آن شدم در تاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ستوران و اقربات مدام</p></div>
<div class="m2"><p>کاه کهتاب باد و جو کشکاب</p></div></div>