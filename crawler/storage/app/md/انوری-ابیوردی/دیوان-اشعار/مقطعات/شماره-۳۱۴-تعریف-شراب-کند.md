---
title: >-
    شمارهٔ ۳۱۴ - تعریف شراب کند
---
# شمارهٔ ۳۱۴ - تعریف شراب کند

<div class="b" id="bn1"><div class="m1"><p>غذای روح بود بادهٔ رحیق‌الحق</p></div>
<div class="m2"><p>که لون او کند از لون دور گل راوق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به طعم تلخ چو پند پدر ولیک مفید</p></div>
<div class="m2"><p>به نزد مبطل باطل به نزد دانا حق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلال گشته به احکام عقل بر دانا</p></div>
<div class="m2"><p>حرام گشته به فتوی شرع بر احمق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رنگ زنگ زداید ز جان اندهگین</p></div>
<div class="m2"><p>همای گردد اگر جرعه‌ای بیابد بق</p></div></div>