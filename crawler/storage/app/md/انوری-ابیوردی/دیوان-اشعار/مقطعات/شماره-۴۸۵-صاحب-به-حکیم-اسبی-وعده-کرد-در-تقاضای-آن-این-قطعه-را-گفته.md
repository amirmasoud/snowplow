---
title: >-
    شمارهٔ ۴۸۵ - صاحب به حکیم اسبی وعده کرد در تقاضای آن این قطعه را گفته
---
# شمارهٔ ۴۸۵ - صاحب به حکیم اسبی وعده کرد در تقاضای آن این قطعه را گفته

<div class="b" id="bn1"><div class="m1"><p>زهی نفاذ تو در سر کارهای ممالک</p></div>
<div class="m2"><p>گرفته نسبت اسرار حکمهای الهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقال رفعت قدر تو پیش رفعت گردون</p></div>
<div class="m2"><p>حدیث پایهٔ ما هست پیش پستی ماهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو وقفنامهٔ دولت قضا به نام تو بنوشت</p></div>
<div class="m2"><p>چهار عنصر و نه چرخ برزدند گواهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تویی که مسرع امرت ندید وهن توقف</p></div>
<div class="m2"><p>تویی که عرصهٔ جاهت ندید ننگ تباهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز رشک رای منیر تو هیچ روز نباشد</p></div>
<div class="m2"><p>که صبح جامه ندرد بر آسمان ز پگاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر به رنج نداری که هیچ رنج مبادت</p></div>
<div class="m2"><p>ز حسب واقعه بنویس چند بیت کماهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به یاد تست همانا حدیث بخشش اسبی</p></div>
<div class="m2"><p>که کهرباش چو بیند کند عزیمت کاهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برون نمی‌شود از گوشم آن حدیث و تو دانی</p></div>
<div class="m2"><p>حدیث اسب نیاید برون ز گوش سپاهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و گربها بود آنرا بها پدید نباشد</p></div>
<div class="m2"><p>پیادگی و فراغت به از عقیله و شاهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به عون تست پناهم که از عنایت گردون</p></div>
<div class="m2"><p>چنانت باد که هرگز به هیچ‌کس نپناهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا ز صورت حالی که هست قصه غصه</p></div>
<div class="m2"><p>روا بود که بگویم به ناخوشی و تباهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدان خدای که اندر زمانه روز و شب آرد</p></div>
<div class="m2"><p>اگرچه روز تمنی شبی بود به سیاهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا ز حادثه حالیست آنچنانکه نخواهم</p></div>
<div class="m2"><p>توانی ار به عنایت چنان کنی که بخواهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به بذل کوش که از مال و جاه حاتم طی را</p></div>
<div class="m2"><p>اثر نماند به جز بذلهای مالی و جاهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بقات باد که تا مهر آسمان گیه‌گون</p></div>
<div class="m2"><p>به خاصیت بنماید ز شوره مهر گیاهی</p></div></div>