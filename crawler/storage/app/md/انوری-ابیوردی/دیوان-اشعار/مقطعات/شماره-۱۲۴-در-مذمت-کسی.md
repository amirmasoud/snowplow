---
title: >-
    شمارهٔ ۱۲۴ - در مذمت کسی
---
# شمارهٔ ۱۲۴ - در مذمت کسی

<div class="b" id="bn1"><div class="m1"><p>سراجی ای ز مقیمان حضرت ترمد</p></div>
<div class="m2"><p>رسید نامهٔ تو همچو روضه‌ای ز بهشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حدیث فخری منحول اندرو کرده</p></div>
<div class="m2"><p>که دست و طبعش جز دوک آن حدیث نرشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غرض چه یعنی دزدیست بی‌حیا آخر</p></div>
<div class="m2"><p>من این ندانم کز ماده گاو ناید کشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کعبهٔ سخن اندر چه ذکر او رانی</p></div>
<div class="m2"><p>که ذکر او نکند هیچ کافری به کنشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گواهیش که گواهی خود در این محضر</p></div>
<div class="m2"><p>ز ننگ او به همه شهر خود دو کس ننوشت</p></div></div>