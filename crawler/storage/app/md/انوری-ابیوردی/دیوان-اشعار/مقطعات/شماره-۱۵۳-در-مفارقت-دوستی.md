---
title: >-
    شمارهٔ ۱۵۳ - در مفارقت دوستی
---
# شمارهٔ ۱۵۳ - در مفارقت دوستی

<div class="b" id="bn1"><div class="m1"><p>به خدایی که از شب تیره</p></div>
<div class="m2"><p>روز روشن همی پدید آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌قلم بر بساط آینه فام</p></div>
<div class="m2"><p>صورت آفتاب بنگارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کز غمت انوری ز آتش دل</p></div>
<div class="m2"><p>آب حسرت ز دیده می‌بارد</p></div></div>