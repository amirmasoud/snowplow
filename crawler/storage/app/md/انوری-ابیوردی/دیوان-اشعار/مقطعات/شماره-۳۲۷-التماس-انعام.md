---
title: >-
    شمارهٔ ۳۲۷ - التماس انعام
---
# شمارهٔ ۳۲۷ - التماس انعام

<div class="b" id="bn1"><div class="m1"><p>ای ترا آفتاب حاجب بار</p></div>
<div class="m2"><p>حشمتت را ستارگان در خیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرخ جاه ترا معالی برج</p></div>
<div class="m2"><p>بحر جود ترا مکارم سیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوده در وقت فطرت عالم</p></div>
<div class="m2"><p>گوهرت را وجود جمله طفیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرر شعلهٔ سیاست تست</p></div>
<div class="m2"><p>از سهاء سپهر تا به سهیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سدهٔ ساحت تو منبع امن</p></div>
<div class="m2"><p>خانهٔ دشمن تو معدن ویل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرمن جود تو نپیماید</p></div>
<div class="m2"><p>گر قضا از سپهر سازد کیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنده گستاخیی نخواهد کرد</p></div>
<div class="m2"><p>گر ترا سوی عفو باشد میل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ دانی که یاد هست امروز</p></div>
<div class="m2"><p>رای عالیت را کلام اللیل</p></div></div>