---
title: >-
    شمارهٔ ۴۴۷ - در وصف بزرگی و کرم صاحب ترمد
---
# شمارهٔ ۴۴۷ - در وصف بزرگی و کرم صاحب ترمد

<div class="b" id="bn1"><div class="m1"><p>دی ز من پرسید معروفی ز معروفان بلخ</p></div>
<div class="m2"><p>از شما پوشیده چون دارم عزیز شادخی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت گیتی را سه دریا داد گیتی آفرین</p></div>
<div class="m2"><p>هریکی زیشان محیط از غایت بی‌برزخی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن به ترمدوان به موصول وان سه دیگر در هرات</p></div>
<div class="m2"><p>کیست بهتر زین سه عالی موج دریای سخی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم او را حاش لله این تساوی شرط نیست</p></div>
<div class="m2"><p>لاله هرگز کی کند رمحی و سوسن ناچخی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این میان صوفیان باشد که هنگام خطاب</p></div>
<div class="m2"><p>شیخ هدهد را اخی خواند سلیمان را اخی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانکه اندر خدمت این صاحب صاحب‌قران</p></div>
<div class="m2"><p>مدحتی گویم که حکمش طاعتست از فرخی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منتظم گردد ز ملک موصل و حصن هرات</p></div>
<div class="m2"><p>امتحان را این بهشتی غصه را آن دوزخی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مجلسش را میوه‌کش باشد جمال موصلی</p></div>
<div class="m2"><p>مطبخش را دیگ شو گردد اثیر مطبخی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شادمان زی ای قدر قدرت خداوندی که هست</p></div>
<div class="m2"><p>جای مغلوبی فلک را گر کنون با او چخی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از متانت حبل اقبالت چو شعر بوالفرج</p></div>
<div class="m2"><p>وز عذوبت مشرب عیشت چو نظم فرخی</p></div></div>