---
title: >-
    شمارهٔ ۳۷۸ - در طلب عفو
---
# شمارهٔ ۳۷۸ - در طلب عفو

<div class="b" id="bn1"><div class="m1"><p>بزرگا گر خطایی کرده آمد</p></div>
<div class="m2"><p>مگیر از من اگر باشد بزرگ آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خطای بندگان باید به هر حال</p></div>
<div class="m2"><p>که تا پیدا شود عفو بزرگان</p></div></div>