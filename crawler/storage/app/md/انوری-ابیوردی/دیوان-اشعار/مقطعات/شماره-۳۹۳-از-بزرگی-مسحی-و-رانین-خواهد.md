---
title: >-
    شمارهٔ ۳۹۳ - از بزرگی مسحی و رانین خواهد
---
# شمارهٔ ۳۹۳ - از بزرگی مسحی و رانین خواهد

<div class="b" id="bn1"><div class="m1"><p>حسام دولت و دین ای خدای داده ترا</p></div>
<div class="m2"><p>جمال احمد و جود علی و نام حسین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهاد آدم لفظ و تو چون مراد از لفظ</p></div>
<div class="m2"><p>سواد عالم عین و تو چون سواد از عین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عنایت ازلی صورت تو چون بنگاشت</p></div>
<div class="m2"><p>نوشت نسخهٔ روشن ز حاصل کونین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سعادت فلکی طینت تو چون بسرشت</p></div>
<div class="m2"><p>نمود از دل و دست تو مجمع‌البحرین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخ تو آب حیاتست و تشنه‌تر هر روز</p></div>
<div class="m2"><p>به دیدن تو خداوند صد چو ذوالقرنین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو ذکر جاه تو کردند آسمان من هو</p></div>
<div class="m2"><p>چو عرض قدر تو دادند اختران من این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز حسب حال در این قطعه رمزکی بشنو</p></div>
<div class="m2"><p>چنان که بینک رفتست دی پریر، وبین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا که طوطی نظمم در این چنین وحلی</p></div>
<div class="m2"><p>چو چوژه پای به گل درنباشد آخر شین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگرچه بط و همایم کند کرامت تو</p></div>
<div class="m2"><p>بچه به زیور مسحی و زینت رانین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شوم چو هیات کبک دری سراسر زیب</p></div>
<div class="m2"><p>شوم چو پیکر طاوس نر سراسر زین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنم چو فاخته در گردن از سپاس تو طوق</p></div>
<div class="m2"><p>از آنکه هست درین گردن آفرین تو دین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرایمت همه جایی به شکر بلبل‌وار</p></div>
<div class="m2"><p>وگرنه نایبه‌کش بادم از غراب‌البین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بقات باد بخوبی و خرمی چندان</p></div>
<div class="m2"><p>که ابجدش ننهد بار جز به منزل غین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حسود جاه ترا آن الم که در همه عمر</p></div>
<div class="m2"><p>حنین او نکند کم علاجهای حنین</p></div></div>