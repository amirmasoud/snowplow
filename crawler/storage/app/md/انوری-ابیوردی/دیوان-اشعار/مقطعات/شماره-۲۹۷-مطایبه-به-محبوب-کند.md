---
title: >-
    شمارهٔ ۲۹۷ - مطایبه به محبوب کند
---
# شمارهٔ ۲۹۷ - مطایبه به محبوب کند

<div class="b" id="bn1"><div class="m1"><p>شعرم به همه جهان رسیدست</p></div>
<div class="m2"><p>مانند کبوتران مرعش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوخ آن باشد که وقت پاسخ</p></div>
<div class="m2"><p>ما را بدهد جواب ناخوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکر ز لبش چو خواستم گفت</p></div>
<div class="m2"><p>بگذر ز سر حدیث زرکش</p></div></div>