---
title: >-
    شمارهٔ ۲۶۳ - قبا از بزرگی خواسته
---
# شمارهٔ ۲۶۳ - قبا از بزرگی خواسته

<div class="b" id="bn1"><div class="m1"><p>ای برقد تو راست قبای سخا و جود</p></div>
<div class="m2"><p>حرفیست در لباس مرا با تو گوش دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تن مراست کهنه قبایی که پاره‌اش</p></div>
<div class="m2"><p>دارد ز بخیه کاری ادریس یادگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آدم به دست جود خودش پنبه کاشته</p></div>
<div class="m2"><p>حوا به سعی دوک خودش رشته پود و تار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوراخهای او کندم وام ریشخند</p></div>
<div class="m2"><p>از هر طرف که پیش گروهی کنم گذار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لطفی نما که هست به راه قبای تو</p></div>
<div class="m2"><p>سوراخها به هر طرفی چشم انتظار</p></div></div>