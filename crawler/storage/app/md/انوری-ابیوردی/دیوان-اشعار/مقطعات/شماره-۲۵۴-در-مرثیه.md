---
title: >-
    شمارهٔ ۲۵۴ - در مرثیه
---
# شمارهٔ ۲۵۴ - در مرثیه

<div class="b" id="bn1"><div class="m1"><p>در مرثیهٔ موئیدالدین</p></div>
<div class="m2"><p>هرکس اثری همی نماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که تشبهی کنم نیز</p></div>
<div class="m2"><p>باشد که تسلیی فزاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لیکن پس از آن جهان معنی</p></div>
<div class="m2"><p>خود طبع سخن همی نزاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با این همه شرح حال شرطست</p></div>
<div class="m2"><p>شرطی نه که طبع هرزه لاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جوف سپهر تنگدل بود</p></div>
<div class="m2"><p>عنقا به قفس درون نیاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌گفت کجاست باد فضلی</p></div>
<div class="m2"><p>کم زین سر خاک در رباید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یزدان که گره‌گشای فضلش</p></div>
<div class="m2"><p>بند قدر و قضا گشاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بشنید به استماع لایق</p></div>
<div class="m2"><p>چونان که جز آنچنان نشاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لطفش به رسالت اجل گفت</p></div>
<div class="m2"><p>کاین زبدهٔ صنع می چه باید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر شاخ مزاج بلبل جانت</p></div>
<div class="m2"><p>تا چند نوای غم سراید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر مختصرست عالم کون</p></div>
<div class="m2"><p>رای تو بدو نمی‌گراید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بخرام که سکنهٔ دگر هست</p></div>
<div class="m2"><p>تا آن دگرت چگونه آید</p></div></div>