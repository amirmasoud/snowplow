---
title: >-
    شمارهٔ ۴۵۴ - فی‌الموعظة
---
# شمارهٔ ۴۵۴ - فی‌الموعظة

<div class="b" id="bn1"><div class="m1"><p>چهار چیزست آیین مردم هنری</p></div>
<div class="m2"><p>که مردم هنری زین چهار نیست بری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی سخاوت طبعی چو دستگاه بود</p></div>
<div class="m2"><p>به نیکنامی آن را ببخشی و بخوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو دیگر آنکه دل دوستان نیازاری</p></div>
<div class="m2"><p>که دوست آینه باشد چو اندرو نگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سه دیگر آنکه زبان را به گاه گفتن زشت</p></div>
<div class="m2"><p>نگاه داری تا وقت عذر غم نخوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چهارم آنکه کسی کو به جای تو بد کرد</p></div>
<div class="m2"><p>چو عذر خواهد نام گناه او نبری</p></div></div>