---
title: >-
    شمارهٔ ۳۷۳ - از زبان پسران میرداد که یکی طوطی به یک ملقب به ناصرالدین و دیگر عضدالدین است گفته و آنها را ستایش کرده است
---
# شمارهٔ ۳۷۳ - از زبان پسران میرداد که یکی طوطی به یک ملقب به ناصرالدین و دیگر عضدالدین است گفته و آنها را ستایش کرده است

<div class="b" id="bn1"><div class="m1"><p>گیتی به سر سنان گشادیم</p></div>
<div class="m2"><p>پس از سر تازیانه دادیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک همه خسروان گرفتیم</p></div>
<div class="m2"><p>سد همه دشمنان گشادیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنیاد جهان اگر کهن بود</p></div>
<div class="m2"><p>از عدل جهان نو نهادیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قایم به وجود ماست گیتی</p></div>
<div class="m2"><p>بس آتش و آب و خاک و بادیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شادند به عدل ما جهانی</p></div>
<div class="m2"><p>ما لاجرم از زمانه شادیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ظن نبری که ما به شاهی</p></div>
<div class="m2"><p>امروز به تازگی فتادیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کز مادر خویش روز اول</p></div>
<div class="m2"><p>شایستهٔ تخت و تاج زادیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سنجر که جهان سراسر او داشت</p></div>
<div class="m2"><p>از ماست و ما از آن نژادیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مسمار سه ملک برکشیدیم</p></div>
<div class="m2"><p>جایی که دو دم بایستادیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر عادل و راد بود سنجر</p></div>
<div class="m2"><p>شکرست که عادلیم و رادیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیداد و ستم نیاید از ما</p></div>
<div class="m2"><p>کاخر پسران میردادیم</p></div></div>