---
title: >-
    شمارهٔ ۹۴ - لطیفه
---
# شمارهٔ ۹۴ - لطیفه

<div class="b" id="bn1"><div class="m1"><p>صاحبا ماجرای دشمن تو</p></div>
<div class="m2"><p>که کسش در جهان ندارد دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفته‌ام در سه بیت چار لطیف</p></div>
<div class="m2"><p>زان چنانها که خاطرم را خوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طنز می‌کرد با جهان کهن</p></div>
<div class="m2"><p>در جهان گفتیی که تازه و نوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنگ او با زمانه درنگرفت</p></div>
<div class="m2"><p>رونق رنگ با قیاس رکوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزگارش گلی شکفت و برو</p></div>
<div class="m2"><p>همچو بر باقلی کفن شد پوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسمان در تنعمش چو بدید</p></div>
<div class="m2"><p>گفت اسراف بیش از این نه نکوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو ریواج پروریده شدست</p></div>
<div class="m2"><p>وقت از بیخ برکشیدن اوست</p></div></div>