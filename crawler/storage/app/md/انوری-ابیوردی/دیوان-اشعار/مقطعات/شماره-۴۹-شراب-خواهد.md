---
title: >-
    شمارهٔ ۴۹ - شراب خواهد
---
# شمارهٔ ۴۹ - شراب خواهد

<div class="b" id="bn1"><div class="m1"><p>فریدالدین کاتب دام عزه</p></div>
<div class="m2"><p>مگر چون ده منی سیکیش بردست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گرمایی چنین در چار طاقش</p></div>
<div class="m2"><p>به دست چار خوارزمی سپردست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنتوانی شنید آخر که گویند</p></div>
<div class="m2"><p>که آن صافی سخن محبوس دردست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به آبی چند آبش باز روی آر</p></div>
<div class="m2"><p>اگر دانی که آن آتش نمردست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مصون باد از حوادث نفس عالیت</p></div>
<div class="m2"><p>الا تا نقش گیتی ناستردست</p></div></div>