---
title: >-
    شمارهٔ ۱۴۶ - در ذم طمع
---
# شمارهٔ ۱۴۶ - در ذم طمع

<div class="b" id="bn1"><div class="m1"><p>مذلت از طمع خیزد همیشه</p></div>
<div class="m2"><p>وجودش در جهان نامنتفع باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طمع آرد به روی مرد زردی</p></div>
<div class="m2"><p>که لعنتهای رکنی بر طمع باد</p></div></div>