---
title: >-
    شمارهٔ ۴۰۷ - سلطان سنجر را گوید
---
# شمارهٔ ۴۰۷ - سلطان سنجر را گوید

<div class="b" id="bn1"><div class="m1"><p>ای جهان را عدل تو آراسته</p></div>
<div class="m2"><p>باغ ملک از خنجرت پیراسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حلقهٔ شب رنگ زلف پرچمت</p></div>
<div class="m2"><p>روزها رخسار فتح آراسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دو دم بنشانده از باران تیر</p></div>
<div class="m2"><p>هر کجا گرد خلافی خاسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خسروان نقش نگین خسروی</p></div>
<div class="m2"><p>نام را جز نام تو ناخواسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنجها خواهان ز دستت زان شدند</p></div>
<div class="m2"><p>کز پی خواهنده داری خواسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بلاد ملک تو با خاک بیز</p></div>
<div class="m2"><p>راستی ناید ز خاک آراسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای به قدر و رای چرخ و آفتاب</p></div>
<div class="m2"><p>باد ماه دولتت ناکاسته</p></div></div>