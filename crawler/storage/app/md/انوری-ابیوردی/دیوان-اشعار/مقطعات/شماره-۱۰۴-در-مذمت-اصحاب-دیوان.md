---
title: >-
    شمارهٔ ۱۰۴ - در مذمت اصحاب دیوان
---
# شمارهٔ ۱۰۴ - در مذمت اصحاب دیوان

<div class="b" id="bn1"><div class="m1"><p>خسروا این چه حلم و خاموشیست</p></div>
<div class="m2"><p>صاحبا این چه عجز و مایوسیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر افسوستان نیاید از آنک</p></div>
<div class="m2"><p>ملک در دست مشتی افسوسیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اولا نایبی که نیست به کار</p></div>
<div class="m2"><p>راست چون پیر کافر روسیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ثانیا این کمال مستوفی</p></div>
<div class="m2"><p>نیک سیاح روی و سالوسیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ثالثا این قوام رعنا ریش</p></div>
<div class="m2"><p>بر سر منهی و جاسوسیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رابعا این کریم گنده دهن</p></div>
<div class="m2"><p>مردکی حیلتی و ناموسیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خامسا این محمد رازی</p></div>
<div class="m2"><p>بتر از رهزنان چپلوسیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سادسا این ثقیل مفسد عز</p></div>
<div class="m2"><p>کز گرانی چو کوه بعلوسیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه ناز و کرشمه و کبرست</p></div>
<div class="m2"><p>گوییا از نژاد کاووسیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سابعا این فرید عارض لنگ</p></div>
<div class="m2"><p>از در صدهزار طرطوسیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ثامن‌القوم آن یمین سرخس</p></div>
<div class="m2"><p>راست چون میل گور قابوسیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کیست تاسع نتیجهٔ مخلص</p></div>
<div class="m2"><p>که به رخ همچو زر بر موسیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مردکی اشقراست و رومی روی</p></div>
<div class="m2"><p>گویی از راهبان ناقوسیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عاشر آن اکرم معاشر شر</p></div>
<div class="m2"><p>گویی از گبرکان ناووسیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اکرم اکرم نعوذ بالله ازو</p></div>
<div class="m2"><p>هیکل مدبری و منحوسیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چاکر خام قلتبانی اوست</p></div>
<div class="m2"><p>هیچ گویی کمال عبدوسیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ما فرحنا معین حدادی</p></div>
<div class="m2"><p>هست محبوس و اهل محبوسیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>احمد لیث آن مخنث فش</p></div>
<div class="m2"><p>که همه خز و توزی وسوسیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از کمال خری و بی‌خردی</p></div>
<div class="m2"><p>جل اسبش کتان قبروسیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هریکی را از این رهی مذهب</p></div>
<div class="m2"><p>کفر محض این نجیبک طوسیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه از روزگار معکوسست</p></div>
<div class="m2"><p>هرچه در کار ملک معکوسیست</p></div></div>