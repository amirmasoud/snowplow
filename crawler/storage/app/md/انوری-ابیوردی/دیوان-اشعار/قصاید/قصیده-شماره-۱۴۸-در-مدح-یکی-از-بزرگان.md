---
title: >-
    قصیدهٔ شمارهٔ ۱۴۸ - در مدح یکی از بزرگان
---
# قصیدهٔ شمارهٔ ۱۴۸ - در مدح یکی از بزرگان

<div class="b" id="bn1"><div class="m1"><p>ای ز کلک توراست کار جهان</p></div>
<div class="m2"><p>صاحب و صدر و افتخار جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوهرت روی کائنات وجود</p></div>
<div class="m2"><p>مسندت پشت شهریار جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظرت حافظ نظام امور</p></div>
<div class="m2"><p>قلمت محور مدار جهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مسرع عزم تو برید قضا</p></div>
<div class="m2"><p>بارهٔ حزم تو حصار جهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار معمار عدل شامل تست</p></div>
<div class="m2"><p>حفظ بیان استوار جهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هردم از جاه نو شوندهٔ تو</p></div>
<div class="m2"><p>نومرادیست در کنار جهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خارج از ظل رایت تو نماند</p></div>
<div class="m2"><p>هیچ دیار در دیار جهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از وقوفت نهان نیارد شد</p></div>
<div class="m2"><p>نه نهان و نه آشکار جهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جنبش رایت تو داند داد</p></div>
<div class="m2"><p>بکم از هفته‌ای قرار جهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر محک جلالت تو زدند</p></div>
<div class="m2"><p>مهر تا کم شد از عیار جهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر جهان خواستار تو نبدی</p></div>
<div class="m2"><p>نشدی امن خواستار جهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر بداند که اختیار تو چیست</p></div>
<div class="m2"><p>همه آن باشد اختیار جهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رو که سیمرغ همت تو نشد</p></div>
<div class="m2"><p>به فریب امل شکار جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر نظر کردیی به آفاقش</p></div>
<div class="m2"><p>در میان آمدی کنار جهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کم کند گر خدای چرخ سخات</p></div>
<div class="m2"><p>بکم از مدتی کنار جهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دشمنت کز عداد مردم نیست</p></div>
<div class="m2"><p>ناردش چرخ در شمار جهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کیست او تا چو مردمان بندد</p></div>
<div class="m2"><p>ناقهٔ خویش در قطار جهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا سپهر از مدار خالی نیست</p></div>
<div class="m2"><p>بر تو بادا مدار کار جهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر مراد تو دار و گیر قضا</p></div>
<div class="m2"><p>بر بساط تو کار و بار جهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حافظت باد هرکجا باشی</p></div>
<div class="m2"><p>گاه و بیگاه کردگار جهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بودن اندر جهان شعار تو باد</p></div>
<div class="m2"><p>تا گذشتن بود شعار جهان</p></div></div>