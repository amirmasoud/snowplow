---
title: >-
    قصیدهٔ شمارهٔ ۸۶ - در مدح شمس‌الدین اغل بیک
---
# قصیدهٔ شمارهٔ ۸۶ - در مدح شمس‌الدین اغل بیک

<div class="b" id="bn1"><div class="m1"><p>دوش در هجر آن بت عیار</p></div>
<div class="m2"><p>تا به روزم نبود خواب و قرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه با ماه و زهره بودم انس</p></div>
<div class="m2"><p>همه با آه و ناله بودم کار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه کسی یک زمان مرا مونس</p></div>
<div class="m2"><p>نه کسی یک نفس مرا غمخوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه بستر ز اشک من رنگین</p></div>
<div class="m2"><p>همه کشور ز آه من بیدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخم از خون چو لالهٔ خودرنگ</p></div>
<div class="m2"><p>اشکم از غم چو لؤلؤ شهوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر و رویم ز زخم دست کبود</p></div>
<div class="m2"><p>دل و جانم به تیر هجر فکار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رخم از رنج زرد همچو ترنج</p></div>
<div class="m2"><p>دلم از درد پاره همچو انار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفسم سرد و سینه آتشگاه</p></div>
<div class="m2"><p>دهنم خشک و دیده طوفان‌بار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گاه چون شمع قوت آتش تیز</p></div>
<div class="m2"><p>گاه چون زیر جفت نالهٔ زار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست بر سر زنان همی گفتم</p></div>
<div class="m2"><p>کای فلک دست از این ضعیف بدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تن بفرسود چند ازین محنت</p></div>
<div class="m2"><p>جان بپالود چند از این آزار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا کی این جور کردن پیوست</p></div>
<div class="m2"><p>چند از این نحس بودن هموار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برگذر از ره جفا و مرا</p></div>
<div class="m2"><p>روزکی چند بی‌غمی بگذار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طاقتم نیست از خدای بترس</p></div>
<div class="m2"><p>بیش ازینم به دست غم مسپار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این همی گفتم و همی کردم</p></div>
<div class="m2"><p>خاک بر سر ز گنبد دوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یار چون نالهای من بشنید</p></div>
<div class="m2"><p>گفت با من به سر در آن شب تار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مکن ای انوری خروش و جزع</p></div>
<div class="m2"><p>که شدت بخت جفت و دولت یار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بار انده مکش که بار دگر</p></div>
<div class="m2"><p>برهانیدت ایزد از غم و بار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بند بگشود چرخ، تنگ مباش</p></div>
<div class="m2"><p>راه بنمود بخت، باک مدار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به تو آورد سعد گردون روی</p></div>
<div class="m2"><p>روی زی درگه خداوند آر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شمس دین پهلوان لشکر شاه</p></div>
<div class="m2"><p>پشت اسلام و قبلهٔ احرار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خاص سلطان اغلبک آنکه کفش</p></div>
<div class="m2"><p>در سخا هست همچو ابر بهار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>موی بر سایلان زبان خواهد</p></div>
<div class="m2"><p>طبعش از بهر بخشش دینار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نظر لطف او بر آنکه فتاد</p></div>
<div class="m2"><p>باز رست از زمانهٔ غدار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زیر پر همای دولت او</p></div>
<div class="m2"><p>چه یکی تن چه صدهزار هزار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>روز هیجا بر اسب که‌پیکر</p></div>
<div class="m2"><p>چو برون آید از پی پیکار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرکب زهره طبع مه نعلش</p></div>
<div class="m2"><p>که تن باد پای خوش رفتار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گه زمین را کند ز پویه هوا</p></div>
<div class="m2"><p>گه هوا را زمین کند ز غبار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برباید شهاب ناوک او</p></div>
<div class="m2"><p>انجم از چرخ و نقش از دیوار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پیش او مار و مرغ در صف جنگ</p></div>
<div class="m2"><p>تحفه و هدیه از برای نثار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مهر آرد گرفته در دندان</p></div>
<div class="m2"><p>دیده آرد گرفته در منقار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سایهٔ رمح و عکس شمشیرش</p></div>
<div class="m2"><p>بگر بیفتد بر جبال و بحار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سنگ این خاک گردد از انده</p></div>
<div class="m2"><p>آب آن قیر گردد از تیمار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ای به ملکت چو وارث داود</p></div>
<div class="m2"><p>ای به مردی چو حیدر کرار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای چو چرخت هزار مدحت‌گوی</p></div>
<div class="m2"><p>وی چو دهرت هزار خدمتگار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا چو تیرست کار دولت تو</p></div>
<div class="m2"><p>بی‌زبانست خصم چون سوفار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو بشادی نشین که گشت فلک</p></div>
<div class="m2"><p>خود برآرد ز دشمن تو دمار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بس ترا پشت نصرت یزدان</p></div>
<div class="m2"><p>بس ترا یار دولت دادار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آنکه در دیدهٔ تو دارد قدر</p></div>
<div class="m2"><p>وانکه بر درگه تو یابد بار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>رفعت این را همی دهد تشریف</p></div>
<div class="m2"><p>دولت آنرا همی نهد مقدار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بنده نیز ار به حکم اومیدی</p></div>
<div class="m2"><p>مدحتی گفت ازو عجب مشمار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>عالمی را چو از تو شاکر دید</p></div>
<div class="m2"><p>گشت در دام خدمت تو شکار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ور ز اقبال قربتی یابد</p></div>
<div class="m2"><p>پیش تخت تو چون صغار و کبار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جست از جور عالم جافی</p></div>
<div class="m2"><p>رست از مکر گیتی مکار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کرد در منزل قبول نزول</p></div>
<div class="m2"><p>گشت بر مرکب مراد سوار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تا نباشد به رنگ روز چو شب</p></div>
<div class="m2"><p>تا نباشد به فعل نور چو نار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شب اعدات را مباد کران</p></div>
<div class="m2"><p>روز شادیت را مباد کنار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>پای بدگوی حاسدت در بند</p></div>
<div class="m2"><p>سر بدخواه و دشمنت بر دار</p></div></div>