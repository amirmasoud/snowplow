---
title: >-
    قصیدهٔ شمارهٔ ۷ - در مدح خواجه ناصرالدین طاهر
---
# قصیدهٔ شمارهٔ ۷ - در مدح خواجه ناصرالدین طاهر

<div class="b" id="bn1"><div class="m1"><p>نصر فزاینده باد ناصر دین را</p></div>
<div class="m2"><p>صدر جهان خواجهٔ زمان و زمین را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صاحب ابوالفتح طاهر آنکه ز رایش</p></div>
<div class="m2"><p>صبح سعادت دمید دولت و دین را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه قضا در حریم طاعتش آورد</p></div>
<div class="m2"><p>رقص کنان گردش شهور و سنین را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وانکه قدر در ادای خدمتش افکند</p></div>
<div class="m2"><p>موی‌کشان گردن ینال و تگین را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وانکه به سیر و سکون یمین و یسارش</p></div>
<div class="m2"><p>نطق و نظر داده‌اند کلک و نگین را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قلزم و کان را نه مستفید نخست‌اند</p></div>
<div class="m2"><p>کلک و نگین آن یسار و اینت یمین را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پای نظر پی کند بلندی قدرش</p></div>
<div class="m2"><p>رغم اشارت‌کنان شک و یقین را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قفل قدر بشکند تفحص حزمش</p></div>
<div class="m2"><p>کفش نهان خانهاء غث وسمین را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غوطه توان داد روز عرض ضمیرش</p></div>
<div class="m2"><p>در عرق آفتاب چرخ برین را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حسرت ترتیب عقد گوهر کلکش</p></div>
<div class="m2"><p>در ثمین کرده اشک در ثمین را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی‌شرف مهر خازنش ننهادست</p></div>
<div class="m2"><p>در دل کان آفتاب هیچ دفین را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی‌مدد عزم قاهرش نگشادست</p></div>
<div class="m2"><p>کوکبهٔ روزگار هیچ کمین را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>واهب روح ازپی طفیل وجودش</p></div>
<div class="m2"><p>قابل ارواح کرده قالب طین را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جز به در جامه خانه کرم او</p></div>
<div class="m2"><p>کسوت صورت نمی‌دهند جنین را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا افق آستانش راست نکردند</p></div>
<div class="m2"><p>شعله نزد روز نیک هیچ حزین را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بی‌دم لطفش به خاک در بنشاندند</p></div>
<div class="m2"><p>باد صبا را نه بلکه ماء معین را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فاتحهٔ داغش از زمانه همی خواست</p></div>
<div class="m2"><p>شیر سپهر از برای لوح سرین را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت قضا کز پی سباع نوشتست</p></div>
<div class="m2"><p>کاتب تقدیر حرز روح امین را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای ز پی آب ملک و رونق دولت</p></div>
<div class="m2"><p>دافعهٔ فتنه کرده رای رزین را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وز پی احیای دین خزان و بهاری</p></div>
<div class="m2"><p>بر سر خر زین ندیده خنگ تو زین را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رای تو بود آنکه در هوای ممالک</p></div>
<div class="m2"><p>رایحهٔ صلح داد صرصر کین را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رحم تو بود آنکه فیض رحمت سلطان</p></div>
<div class="m2"><p>بدرقه شد یک جهان حنین و انین را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ورنه تو دانی که شیر رایت قهرش</p></div>
<div class="m2"><p>مثله کند شیر چرخ و شیر عرین را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حصن هزار اسب اگرچه بر سر آن ملک</p></div>
<div class="m2"><p>سد قدیمست حصنهای حصین را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کعبهٔ دهلیز شه چو دید فصیلش</p></div>
<div class="m2"><p>سجده‌کنان بر زمین نهاد جبین را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خود مدد تیغ پادشا چه بکارست</p></div>
<div class="m2"><p>خاصه تهیاء کارهای چنین را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سیر سریع شهاب کلک تو بس بود</p></div>
<div class="m2"><p>رجم چنان صد هزار دیو لعین را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>غیبت خوارزم شاه چون پس شش ماه</p></div>
<div class="m2"><p>چشمهٔ خون دید چشم حادثه‌بین را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دست به فتراک اصطناع تو در زد</p></div>
<div class="m2"><p>معتصم ملک ساخت حبل متین را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شاد زی، ای در ظهور معجز تدبیر</p></div>
<div class="m2"><p>روی‌سیه کرده رسم سحر مبین را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ناصر تو خیر ناصرست و معین است</p></div>
<div class="m2"><p>طاعت تو خیر طاعتست معین را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>باغ وجود از بهار عدل تو چونانک</p></div>
<div class="m2"><p>رشک فزاید نگارخانهٔ چین را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ملت و ملک از تو در لباس نظامند</p></div>
<div class="m2"><p>بی‌تو نه آنرا نظام باد و نه این را</p></div></div>