---
title: >-
    قصیدهٔ شمارهٔ ۷۱ - در مدح ناصرالدین ابوالفتح طاهر
---
# قصیدهٔ شمارهٔ ۷۱ - در مدح ناصرالدین ابوالفتح طاهر

<div class="b" id="bn1"><div class="m1"><p>مست شبانه بودم افتاده بی‌خبر</p></div>
<div class="m2"><p>دی در وثاق خویش که دلبر بکوفت در</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون اصطکاک و قرع هوا از طریق صوت</p></div>
<div class="m2"><p>داد از ره صماخ دماغ مرا خبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر عادتی که باشد گفتم که کیست این</p></div>
<div class="m2"><p>گفت آنکه نیست در غم و شادیت ازو گذر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جستم چنان ز جای که جانم خبر نداشت</p></div>
<div class="m2"><p>کان دم به پای می روم از عشق یا به سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در باز کرد و دست ببوسید و در کشید</p></div>
<div class="m2"><p>تنگش چو خرمن گل و تنگ شکر ببر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>القصه اندر آمد و بنشست و هر سخن</p></div>
<div class="m2"><p>گفت و شنید از انده و شادی و خیر و شر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس در ملامت آمد کین چیست می‌کنی</p></div>
<div class="m2"><p>یزدانت به کناد که کردست خود بتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا در خمار مانده‌ای از صبح تا به شام</p></div>
<div class="m2"><p>یا در شراب خفته‌ای از شام تا سحر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو سر به نای و نوش فرو برده‌ای و من</p></div>
<div class="m2"><p>خاموش و سرفکنده که هین بوک و هان مگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل گرم کرده‌ای ز تف عشق من به سست</p></div>
<div class="m2"><p>سردی مکن که گرم کنی همچو دل جگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باری ز باده خوردن و عشرت چو چاره نیست</p></div>
<div class="m2"><p>در خدمت بساط خداوند خواجه خور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صدر زمانه ناصر دین طاهر آنکه هست</p></div>
<div class="m2"><p>در شان ملک آیتی از نصرت و ظفر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا حضرتی ببینی بر چرخ کرده فخر</p></div>
<div class="m2"><p>تا مجلسی بیابی از خلد برده فر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بربسته پیش خدمت اسبان رتبتش</p></div>
<div class="m2"><p>رضوان میان کوثر و تسنیم را کمر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتم که پایمرد و وسیلت که باشدم</p></div>
<div class="m2"><p>گفتا که بهتر از کرم او کسی دگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فردا که ناف هفته و روز سه‌شنبه است</p></div>
<div class="m2"><p>روزی که هست از شب قدری خجسته‌تر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روزی چنان که گویی فهرست عشرتست</p></div>
<div class="m2"><p>یک حاشیه به خاور و دیگر به باختر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آثار او چو عدت ایام بر قرار</p></div>
<div class="m2"><p>و اوقات او چو صورت افلاک بر گذر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بی هیچ شک نشاط صبوحی کند به‌گاه</p></div>
<div class="m2"><p>دانی چه کن و گرچه تو دانی خود این قدر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کاری دگر نداری بنشین و خدمتی</p></div>
<div class="m2"><p>ترتیب کن هم امشب و فردا به گه ببر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دوش آنچنان که از رگ اندیشه خون چکید</p></div>
<div class="m2"><p>نظمی چنان که دانی رفتست مختصر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر زحمتت نباشد از آن تا ادا کنم</p></div>
<div class="m2"><p>آهسته همچنین به همین صورت پرده‌در</p></div></div>