---
title: >-
    قصیدهٔ شمارهٔ ۱۰۷ - در مدح ناصرالدین طاهربن مظفر
---
# قصیدهٔ شمارهٔ ۱۰۷ - در مدح ناصرالدین طاهربن مظفر

<div class="b" id="bn1"><div class="m1"><p>زهی دست تو بر سر آفرینش</p></div>
<div class="m2"><p>وجود تو سر دفتر آفرینش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فضا خطبه‌ها کرده در ملک و ملت</p></div>
<div class="m2"><p>به نام تو بر منبر آفرینش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چهل سال مشاطه کون کرده</p></div>
<div class="m2"><p>رسوم ترا زیور آفرینش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طرازی نه چون طاهربن مظفر</p></div>
<div class="m2"><p>به عهد تو در ششتر آفرینش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر فضلهٔ گوهر تو نبودی</p></div>
<div class="m2"><p>حقیر آمدی گوهر آفرینش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشاد نفاذ تو گردون فطرت</p></div>
<div class="m2"><p>بپردازد از دفتر آفرینش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وگر اختر تو نبودی نگشتی</p></div>
<div class="m2"><p>سعادت‌رسان اختر آفرینش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به باد عدم بردهد گر بخواهد</p></div>
<div class="m2"><p>خلاف تو خاکستر آفرینش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فنا بارها کرد عزم مصمم</p></div>
<div class="m2"><p>که تا بشکند چنبر آفرینش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکوه تو دریافت آن کار اگرنه</p></div>
<div class="m2"><p>بکردی فنا در خور آفرینش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دیوان جاهت گذارند انجم</p></div>
<div class="m2"><p>خراج نهم کشور آفرینش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وز اقطاع جودت رسانند ارکان</p></div>
<div class="m2"><p>وجوب همه لشکر آفرینش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو ای سرور آفرینش نبینی</p></div>
<div class="m2"><p>که هر دم قضا مادر آفرینش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به زجر تمام از طبیعت بپرسد</p></div>
<div class="m2"><p>که هم به نشد سرور آفرینش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترا کردگار از برای تحفظ</p></div>
<div class="m2"><p>موکل کند بر سر آفرینش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تکسر چه باشد که با چون تو شحنه</p></div>
<div class="m2"><p>بگردد به گرد در آفرینش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حوادث چرا بستری گستردکان</p></div>
<div class="m2"><p>به معنی بود بستر آفرینش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گوا می‌کنم بر تو هان ای طبیعت</p></div>
<div class="m2"><p>درین داوری داور آفرینش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که تا گرم و سردی برویش نیاری</p></div>
<div class="m2"><p>که این است خشک و تر آفرینش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>الا تا مزاج عناصر به نسبت</p></div>
<div class="m2"><p>زیادت کند پیکر آفرینش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو بادی که جز با تو نیکو نیاید</p></div>
<div class="m2"><p>قبای بقا در بر آفرینش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دوام ترا بیخ در آب و خاکی</p></div>
<div class="m2"><p>کزو رست برگ و بر آفرینش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بقای تو چندان که در طول و عرضش</p></div>
<div class="m2"><p>نشاید به جز محور آفرینش</p></div></div>