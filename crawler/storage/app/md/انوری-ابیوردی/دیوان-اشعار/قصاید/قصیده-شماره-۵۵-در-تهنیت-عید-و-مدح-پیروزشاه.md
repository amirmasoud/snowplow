---
title: >-
    قصیدهٔ شمارهٔ ۵۵ - در تهنیت عید و مدح پیروزشاه
---
# قصیدهٔ شمارهٔ ۵۵ - در تهنیت عید و مدح پیروزشاه

<div class="b" id="bn1"><div class="m1"><p>ای عید دین و دولت عیدت خجسته باد</p></div>
<div class="m2"><p>ایامت از حوادث ایام رسته باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلزار باغ چرخ که پژمردگیش نیست</p></div>
<div class="m2"><p>در انتظار مجلس تو دسته دسته باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بازار مصر جامع ملک از مکان تو</p></div>
<div class="m2"><p>تا بارهٔ نهم ز جهان رسته رسته باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الا ز شست عزم تو تیر قدر قضا</p></div>
<div class="m2"><p>بر هر نشانه‌ای که زند باز جسته باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نشو بیخ امن بود جز به باغ تو</p></div>
<div class="m2"><p>از شاخهاش در تبر فتنه دسته باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور آبروی ملک رود جز به جوی تو</p></div>
<div class="m2"><p>زاب فساد کل ورق کون شسته باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در هیچ کار بی‌تو فلک را مباد خوض</p></div>
<div class="m2"><p>پس گر بود نخست رضای تو جسته باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کیوان موافقان ترا گر جگر خورد</p></div>
<div class="m2"><p>نسرین چرخ را جگر جدی مسته باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور مشتری جوی ز هوای تو کم کند</p></div>
<div class="m2"><p>یکباره مرغزار فلک خوشه رسته باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مریخ اگر به خون حسود تو تشنه نیست</p></div>
<div class="m2"><p>زنگار خورده خنجر و جوشن گسسته باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ور در شود بر وزن بدخواهت آفتاب</p></div>
<div class="m2"><p>گرد کسوف گرد جمالش نشسته باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور زهره جز به بزم تو خنیاگری کند</p></div>
<div class="m2"><p>جاوید دف دریده و بربط شکسته باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ور نامه‌ای دهد نه به پروانهٔ تو تیر</p></div>
<div class="m2"><p>شغلش فرو گشاده و دستش ببسته باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ماه ار نخواهد آنکه وبد نعل مرکبت</p></div>
<div class="m2"><p>از ناخن محاق ابد چهره خسته باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>واندر هرآنچه رای تو کرد اقتضای آن</p></div>
<div class="m2"><p>تقدیر جز به عین رضا ننگرسته باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا رسم تهنیت بود اندر جهان بعید</p></div>
<div class="m2"><p>هر بامداد بر تو چو عیدی خجسته باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بادام‌وار چشم حسود تو آژده</p></div>
<div class="m2"><p>وز ناله بازمانده دهان همچو پسته باد</p></div></div>