---
title: >-
    قصیدهٔ شمارهٔ ۱۸۷ - مدح ابوالمفاخر امیر فخرالدین میرآب مرو معروف به آبی
---
# قصیدهٔ شمارهٔ ۱۸۷ - مدح ابوالمفاخر امیر فخرالدین میرآب مرو معروف به آبی

<div class="b" id="bn1"><div class="m1"><p>ای قبلهٔ کوی خاکی و آبی</p></div>
<div class="m2"><p>وی فخر همه قبیلهٔ آبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای یافته هرچه جسته از گیتی</p></div>
<div class="m2"><p>جز مثل که این یکی نمی‌یابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اجرام ز رشک پایهٔ قدرت</p></div>
<div class="m2"><p>پوشیده لباسهای سیمابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عدل تو ز روی خاصیت کرده</p></div>
<div class="m2"><p>با آتش فتنه سالها آبی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر چرخ ز بهر اختیاراتت</p></div>
<div class="m2"><p>خورشید همی کند سطر لابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرده صف اختران گردون را</p></div>
<div class="m2"><p>درگاه تواند سال محرابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دارالضربی است کرد و گفت تو</p></div>
<div class="m2"><p>ایمن شده از مجال قلابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون خاک به گاه خشم بشکیبی</p></div>
<div class="m2"><p>چون باد به وقت عفو بشتابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درگاه تو باب اعظم عدلست</p></div>
<div class="m2"><p>مهدی شده نامزد به بوابی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز آسیب تو از فلک فرو ریزند</p></div>
<div class="m2"><p>انجم چو کبوتران مضرابی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از کار عدوت چون روان گردد</p></div>
<div class="m2"><p>تعلیم توان ستد رسن تابی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از سیم مخالفت سخا ناید</p></div>
<div class="m2"><p>نشنیدستی ز سیم اعرابی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تاریخ تفاخرست تشریفت</p></div>
<div class="m2"><p>هم اسلافی مرا هم اعقابی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زوداکه به دلوشان فرو دادست</p></div>
<div class="m2"><p>این گنبد زود گرد دولابی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای چشم نیازیان ز جود تو</p></div>
<div class="m2"><p>چون بخت مخالفت به خوش خوابی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفتم که به شکر آن پدید آیم</p></div>
<div class="m2"><p>رخ کرده جلالت تو عنابی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفتا ز گرانی رکاب من</p></div>
<div class="m2"><p>زودا که عنان به عجز برتابی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فتح‌البابی بکردم آخر هم</p></div>
<div class="m2"><p>با آنکه تو از ورای این بابی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا هست ز شصت دور در سرعت</p></div>
<div class="m2"><p>ایام چو تیرهای پرتابی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خصم تو و دور چرخ او بادا</p></div>
<div class="m2"><p>طینت قصبی و طبع مهتابی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون دانهٔ نار اشک بدخواهت</p></div>
<div class="m2"><p>وز غصه رخش چو چهرهٔ آبی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اسباب بقات ساخته گردون</p></div>
<div class="m2"><p>در جمله نه صنعتی نه اسبابی</p></div></div>