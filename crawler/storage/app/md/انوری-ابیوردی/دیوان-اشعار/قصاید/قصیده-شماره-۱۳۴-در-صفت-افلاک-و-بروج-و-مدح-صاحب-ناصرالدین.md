---
title: >-
    قصیدهٔ شمارهٔ ۱۳۴ - در صفت افلاک و بروج و مدح صاحب ناصرالدین
---
# قصیدهٔ شمارهٔ ۱۳۴ - در صفت افلاک و بروج و مدح صاحب ناصرالدین

<div class="b" id="bn1"><div class="m1"><p>دوش سلطان چرخ آینه فام</p></div>
<div class="m2"><p>آنکه دستور شاه راست غلام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کنار نبردگاه افق</p></div>
<div class="m2"><p>چون به دست غروب داد زمام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدم اندر سواد طرهٔ شب</p></div>
<div class="m2"><p>گوشوار فلک ز گوشهٔ بام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم آن نعل خنگ دستورست</p></div>
<div class="m2"><p>قرةالعین و فخر آل نظام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسمان گفت کاشکی هستی</p></div>
<div class="m2"><p>که نهد خنگ او به ما بر گام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم آن چیست پس بگو برهان</p></div>
<div class="m2"><p>آسمان با دریغ و درد تمام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت ربی و ربک الله گوی</p></div>
<div class="m2"><p>گفتم آوخ هلال ماه صیام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت آری مدام نتوان کرد</p></div>
<div class="m2"><p>بر بساط وزیر شرب مدام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شبکی چند احتباس شراب</p></div>
<div class="m2"><p>روزکی چند احتماء طعام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو انعام تا کی از خور و خواب</p></div>
<div class="m2"><p>نوبت فاتحه است والانعام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طیره گشتم ازو والحق بود</p></div>
<div class="m2"><p>جای آن طیرگی در آن هنگام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ماه چون در حجاب می‌نوشد</p></div>
<div class="m2"><p>از سرای سپهر مینافام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خیمه‌ای دیدم از زمانه برون</p></div>
<div class="m2"><p>واندران خیمه درج کرده خیام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مجمعی از مخدرات درو</p></div>
<div class="m2"><p>همه آتش لباس و آب اندام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سکنه‌شان را مدار بی‌آغاز</p></div>
<div class="m2"><p>ساکنان را مسیر بی‌فرجام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تیر در هجر چهرهٔ زهره</p></div>
<div class="m2"><p>گشته از اشتیاق بی‌آرام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زهره در پیش چشم بهمن و دی</p></div>
<div class="m2"><p>به کفی بربط و به دیگر جام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تیغ مریخ پیش صیقل قلب</p></div>
<div class="m2"><p>تخت خورشید زیر سایهٔ شام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دلو کیوان در اوفتاده به چاه</p></div>
<div class="m2"><p>ماهی مشتری بجسته ز دام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>توامان در ازاء ناوک قوس</p></div>
<div class="m2"><p>منع را خصم‌وار کرده قیام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حدی مفتون خوشهٔ گندم</p></div>
<div class="m2"><p>بره مذبوح خنجر بهرام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اسد اندر کمین کینهٔ ثور</p></div>
<div class="m2"><p>کام بگشاده تا بیابد کام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در ترازوی چرخ چیزی نه</p></div>
<div class="m2"><p>جز مراد لئام و غبن کرام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جویبار مجره را سرطان</p></div>
<div class="m2"><p>زیر پی درکشیده بود و خرام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر زمانی مسیر کلک شهاب</p></div>
<div class="m2"><p>بر زبان رقم به وجه پیام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ساکنان سواد مسکون را</p></div>
<div class="m2"><p>دادی از راز روزگار اعلام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>راست همچون مسیر کلک وزیر</p></div>
<div class="m2"><p>که دهد ملک را قرار و نظام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صاحب آن ذوالجلالتین که هست</p></div>
<div class="m2"><p>بر ازو ذوالجلال والاکرام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>افتخار انام ناصر دین</p></div>
<div class="m2"><p>صدر اسلام و اختیار انام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>صاهربن مظفر آنکه ظفر</p></div>
<div class="m2"><p>رایتش را ملازمست مدام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آنکه از بهر خدمتش بندد</p></div>
<div class="m2"><p>نقش تصویر نطفه در ارحام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آنکه از بهر مدحتش زاید</p></div>
<div class="m2"><p>گوهر نظم و نثر در اوهام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آن تمامی که روز استغناش</p></div>
<div class="m2"><p>نه ز نقصان نشان گذاشت نه نام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>متصل مدتی که باقی شد</p></div>
<div class="m2"><p>به طفیل بقای او ایام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آنکه خشمش طلایهٔ زحمت</p></div>
<div class="m2"><p>وانکه عفوش بهانهٔ انعام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آنکه خورشید آسمان بگزارد</p></div>
<div class="m2"><p>سایه‌ها را ز نور رایش وام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ژاله خورشید شعله بارد اگر</p></div>
<div class="m2"><p>درجهد برق خاطرش به غمام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آسمان در ازاء حکم روانش</p></div>
<div class="m2"><p>خط باطل کشید بر احکام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دور او آنگه آسمان را حکم</p></div>
<div class="m2"><p>آسمان باری از کجا و کدام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای ز پاس تو تیره آب ستم</p></div>
<div class="m2"><p>وز شکوه تو نان حادثه خام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تیغ باس تو تا کشیده شدست</p></div>
<div class="m2"><p>حادثه خنجرست و حبس نیام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چون جلای خدای جای تو خاص</p></div>
<div class="m2"><p>چون عطای خدای جود تو عام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اصطناعت چو آب جان‌پرور</p></div>
<div class="m2"><p>انتقامت چو خاک خون‌آشام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شاکر نعمتت وضیع و شریف</p></div>
<div class="m2"><p>عاشق خدمتت خواص و عوام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زیر طوق تو گردون شب و روز</p></div>
<div class="m2"><p>لوح داغ تو شانهٔ دد و دام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بی‌زمین بوس نور و سایه نداد</p></div>
<div class="m2"><p>سدهٔ ساحت ترا ابرام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که بود دهر کت نبوسد خاک</p></div>
<div class="m2"><p>چکند چرخ کت نباشد رام</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جذب عدلت به خاصیت بکشد</p></div>
<div class="m2"><p>با عرق راز مجرمان ز مسام</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بر دوام تو عدل تست دلیل</p></div>
<div class="m2"><p>عدل باشد بلی دلیل دوام</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بانفاذت ز گرگ بستاند</p></div>
<div class="m2"><p>دیت کشتگان خود اغنام</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تشنگان زلال لطفت را</p></div>
<div class="m2"><p>نکند تلخ ناامیدی کام</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کشتگان سموم قهر ترا</p></div>
<div class="m2"><p>حشر ناممکن است روز قیام</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خون خصمت حلال دارد چرخ</p></div>
<div class="m2"><p>ور بود در حریم بیت حرام</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خاضع آید کلاه گوشهٔ عرش</p></div>
<div class="m2"><p>گوشهٔ بالش ترا به سلام</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>فیض عقلت نفوس انجم را</p></div>
<div class="m2"><p>به سعادت همی کنند الهام</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>عالیا پایهٔ مدیح تو وای</p></div>
<div class="m2"><p>که چه پرها بریختند اوهام</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>من کیم تا به آستانش رسد</p></div>
<div class="m2"><p>دست نطقم ز آستین کلام</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>انوری هم حدیث لااحصی</p></div>
<div class="m2"><p>بس دلیری مکن لکل مقام</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سخنت چون الف ندارد هیچ</p></div>
<div class="m2"><p>چه کشی از پی قبولش لام</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ای جوادی که ازدحام سحاب</p></div>
<div class="m2"><p>با کفت هست التیام لئام</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تا به اجسام قائمند اعراض</p></div>
<div class="m2"><p>تا به اعراض باقیند اجسام</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بی‌تو اجسام را مباد بقا</p></div>
<div class="m2"><p>بی‌تو اعراض را مباد قیام</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گل عز تو در بهار وجود</p></div>
<div class="m2"><p>تازه باد و عدم گرفته ز کام</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>با مرادت سپهر سست مهار</p></div>
<div class="m2"><p>با حسودت زمانه سخت لگام</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>درگهت را سیاست از حجاب</p></div>
<div class="m2"><p>حضرتت را سیادت از خدام</p></div></div>