---
title: >-
    قصیدهٔ شمارهٔ ۱۷۲ - مدح خاقان‌المعظم طفقاج خان
---
# قصیدهٔ شمارهٔ ۱۷۲ - مدح خاقان‌المعظم طفقاج خان

<div class="b" id="bn1"><div class="m1"><p>شاها صبوح فتح و ظفر کن شراب خواه</p></div>
<div class="m2"><p>نرد و ندیم و مطرب و چنگ و رباب خواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دست آنکه غیرت ماهست و آفتاب</p></div>
<div class="m2"><p>در جام ماه نو می چون آفتاب خواه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وز خد آنکه قطرهٔ آبست و برگ گل</p></div>
<div class="m2"><p>تا گرد رزمگه بزدایی گلاب خواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاقوت ناب و آب فسرده است جام می</p></div>
<div class="m2"><p>آب طرب روان کن و یاقوت ناب خواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازکام شیر ملک چو کردی برون به تیغ</p></div>
<div class="m2"><p>فارغ ز گرد ران گوزنان کباب خواه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز مصاف خصم به جیش خطاشکن</p></div>
<div class="m2"><p>وقت صلاح ملک ز رای صواب خواه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبها که دشمن تو ز بیم تو نغنود</p></div>
<div class="m2"><p>گردون به طعنه گویدش از بخت خواب خواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر پایه‌ای که خصم ترا برکشد سپهر</p></div>
<div class="m2"><p>گوید قضا تمام شد اکنون طناب خواه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روزی که رجم دیو کنی بر سپهر فتح</p></div>
<div class="m2"><p>از ترکش گهرکش خود یک شهاب خواه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وقتی که حکم جزم کنی بر بسیط خاک</p></div>
<div class="m2"><p>از منشیان حضرت خود یک خطاب خواه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر گشت عافیت چو بخیلی کند سپهر</p></div>
<div class="m2"><p>از چتر و تیغ خویش سپهر و سحاب خواه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در موقف جزای مطیعان و عاصیان</p></div>
<div class="m2"><p>از لطف و قهر خویش ثواب و عقاب خواه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نی نی که انتقام خواهد خود آسمان</p></div>
<div class="m2"><p>روزی شکار کن تو و روزی شراب خواه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در شان داد آیت حق بود میر داد</p></div>
<div class="m2"><p>او باب تست زندگی از نام باب خواه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ایام گر بکرد خطایی در آن مبند</p></div>
<div class="m2"><p>خوش باش و انتقام ز رای صواب خواه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنجا که تاب حمله ندارد زمین رزم</p></div>
<div class="m2"><p>از رخش و رمح خویش توان جوی و تاب خواه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون خاک بی‌درنگ شود چرخ بی‌شتاب</p></div>
<div class="m2"><p>از حزم و عزم خویش درنگ و شتاب خواه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دنیا خراب و دین به خلل بود و عدل تو</p></div>
<div class="m2"><p>آباد کرد هر دو کنون طشت و آب خواه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کاهی که از جهان ببرد کهربا به غصب</p></div>
<div class="m2"><p>در عهد عدل تست ز عدلت جواب خواه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بی‌عدل مستجاب نگردد دعای شاه</p></div>
<div class="m2"><p>شاها دعای خویش همه مستجاب خواه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آباد دار ملک زمین خسروا به داد</p></div>
<div class="m2"><p>طوفان باد ملک هوا گو خراب خواه</p></div></div>