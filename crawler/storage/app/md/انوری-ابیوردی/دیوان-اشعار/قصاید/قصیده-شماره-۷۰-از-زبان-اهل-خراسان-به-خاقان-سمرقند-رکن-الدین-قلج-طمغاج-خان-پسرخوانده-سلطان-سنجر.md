---
title: >-
    قصیدهٔ شمارهٔ ۷۰ - از زبان اهل خراسان به خاقان سمرقند رکن‌الدین قلج طمغاج خان پسرخواندهٔ سلطان سنجر
---
# قصیدهٔ شمارهٔ ۷۰ - از زبان اهل خراسان به خاقان سمرقند رکن‌الدین قلج طمغاج خان پسرخواندهٔ سلطان سنجر

<div class="b" id="bn1"><div class="m1"><p>به سمرقند اگر بگذری ای باد سحر</p></div>
<div class="m2"><p>نامهٔ اهل خراسان به بر خاقان بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نامه‌ای مطلع آن رنج تن و آفت جان</p></div>
<div class="m2"><p>نامه‌ای مقطع آن درد دل و سوز جگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نامه‌ای بر رقمش آه عزیزان پیدا</p></div>
<div class="m2"><p>نامه‌ای در شکنش خون شهیدان مضمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش تحریرش از سینهٔ مظلومان خشک</p></div>
<div class="m2"><p>سطر عنوانش از دیدهٔ محرومان تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ریش گردد ممر صوت از او گاه سماع</p></div>
<div class="m2"><p>خون شود مردمک دیده از او وقت نظر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کنون حال خراسان و رعایا بوده‌ست</p></div>
<div class="m2"><p>بر خداوند جهان خاقان پوشیده مگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی نبوده‌ست که پوشیده نباشد بر وی</p></div>
<div class="m2"><p>ذره‌ای نیک و بد نه فلک و هفت اختر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کارها بسته بود بی‌شک در وقت و کنون</p></div>
<div class="m2"><p>وقت آن است که راند سوی ایران لشکر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو عادل خاقان معظم کز جد</p></div>
<div class="m2"><p>پادشاه است و جهاندار به هفتاد پدر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دائمش فخر به آن است که در پیش ملوک</p></div>
<div class="m2"><p>پسرش خواندی سلطان سلاطین سنجر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باز خواهد ز غزان کینه که واجد باشد</p></div>
<div class="m2"><p>خواستن کین پدر بر پسر خوب‌سیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون شد از عدلش سرتاسر توران آباد</p></div>
<div class="m2"><p>کی روا دارد ایران را ویران یکسر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای کیومرث‌بقا پادشه کسری‌عدل</p></div>
<div class="m2"><p>وی منوچهرلقا خسرو افریدون‌فر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قصهٔ اهل خراسان بشنو از سر لطف</p></div>
<div class="m2"><p>چون شنیدی ز سر رحم به ایشان بنگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این دل‌افکار جگرسوختگان می‌گویند</p></div>
<div class="m2"><p>کای دل و دولت و دین را به تو شادی و ظفر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خبرت هست که از هرچه در او چیزی بود</p></div>
<div class="m2"><p>در همه ایران امروز نمانده‌ست اثر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خبرت هست کز این زیروزبر شوم‌غزان</p></div>
<div class="m2"><p>نیست یک پی ز خراسان که نشد زیروزبر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر بزرگان زمانه شده خردان سالار</p></div>
<div class="m2"><p>بر کریمان جهان گشته لئیمان مهتر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر در دونان احرار حزین و حیران</p></div>
<div class="m2"><p>در کف رندان ابرار اسیر و مضطر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاد الا به در مرگ نبینی مردم</p></div>
<div class="m2"><p>بکر جز در شکم مام نیابی دختر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مسجد جامع هر شهر ستورانشان را</p></div>
<div class="m2"><p>پایگاهی شده نه سقفش پیدا و نه در</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خطبه نکْنند به هر خطه به نام غز ازآنک</p></div>
<div class="m2"><p>در خراسان نه خطیب است کنون نه منبر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کشته فرزند گرامی را گر ناگاهان</p></div>
<div class="m2"><p>بیند، از بیم خروشید نیارد مادر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن‌که را صد ره غز زر ستد و باز فروخت</p></div>
<div class="m2"><p>دارد آن جنس که گوییش خریده‌ست به زر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر مسلمانان زآن نوع کنند استخفاف</p></div>
<div class="m2"><p>که مسلمان نکند صدیک از آن با کافر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هست در روم و خطا امن مسلمانان را</p></div>
<div class="m2"><p>نیست یک ذره سلامت به مسلمانی در</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خلق را زین غم فریاد رس ای شاه‌نژاد</p></div>
<div class="m2"><p>ملک را زین ستم آزاد کن ای پاک‌سیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به خدایی که بیاراست به نامت دینار</p></div>
<div class="m2"><p>به خدایی که بیفراخت به فرت افسر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که کنی فارغ و آسوده دل خلق خدا</p></div>
<div class="m2"><p>زین فرومایه‌غز شوم‌پی غارتگر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وقت آن است که یابند ز رمحت پاداش</p></div>
<div class="m2"><p>گاه آن است که گیرند ز تیغت کیفر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زن و فرزند و زر جمله به یک حمله چو پار</p></div>
<div class="m2"><p>بردی امسال روانشان به دگر حمله ببر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آخر ایران که از او بودی فردوس به رشک</p></div>
<div class="m2"><p>وقف خواهد شد تا حشر بر این شوم‌حشر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سوی آن حضرت کز عدل تو گشته‌ست چو خلد</p></div>
<div class="m2"><p>خویشتن زاینجا کز ظلم غزان شد چو سقر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هرکه پایی و خری داشت به حیلت افکند</p></div>
<div class="m2"><p>چه کند آن‌که نه پای است مر او را و نه خر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>رحم‌ کن رحم بر آن قوم که نبود شب و روز</p></div>
<div class="m2"><p>در مصیبتشان جز نوحه‌گری کار دگر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رحم‌ کن رحم بر آن قوم که جویند جوین</p></div>
<div class="m2"><p>از پس آنکه نخوردندی از ناز شکر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رحم‌ کن رحم بر آن‌ها که نیابند نمد</p></div>
<div class="m2"><p>از پس آنکه ز اطلسشان بودی بستر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رحم‌ کن رحم بر آن قوم که رسوا گشتند</p></div>
<div class="m2"><p>از پس آنکه به مستوری بودند سمر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گرد آفاق چو اسکندر برگرد از آنک</p></div>
<div class="m2"><p>تویی امروز جهان را بدل اسکندر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از تو رزم ای شه و از بخت موافق نصرت</p></div>
<div class="m2"><p>از تو عزم ای ملک و از ملک‌العرش ظفر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همه پوشند کفن گر تو بپوشی خفتان</p></div>
<div class="m2"><p>همه خواهند امان چون تو بخواهی مغفر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ای سرافرازجهانبانی کز غایت فضل</p></div>
<div class="m2"><p>حق سپرده‌ست به عدل تو جهان را یکسر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بهره‌ای باید از عدل تو نیز ایران را</p></div>
<div class="m2"><p>گرچه ویران شد بیرون ز جهانش مشمر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تو خور روشنی و هست خراسان اطلال</p></div>
<div class="m2"><p>نه بر اطلال بتابد چو بر آبادان خور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هست ایران به مثل شوره تو ابری و نه ابر</p></div>
<div class="m2"><p>هم برافشاند بر شوره چو بر باغ مطر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بر ضعیف و قوی امروز تویی داور حق</p></div>
<div class="m2"><p>هست واجب غم حق ضعفا بر داور</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کشور ایران چون کشور توران چو تو راست</p></div>
<div class="m2"><p>از چه محروم است از رأفت تو این کشور</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گر نیاراید پای تو بدین عزم رکاب</p></div>
<div class="m2"><p>غز مدبر نکشد باز عنان تا خاور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کی بود کی که ز اقصای خراسان آرند</p></div>
<div class="m2"><p>از فتوح تو بشارت بر خورشید بشر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پادشاه علما صدر جهان خواجهٔ شرع</p></div>
<div class="m2"><p>مایهٔ فخر و شرف قاعدهٔ فضل و هنر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شمس اسلام فلک‌مرتبه برهان‌الدین</p></div>
<div class="m2"><p>آن‌که مولیش بود شمس و فلک فرمان‌بر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آن‌که از مهر تو تازه‌ست چو از دانش روح</p></div>
<div class="m2"><p>وآن‌که بر چهر تو فتنه‌ست بر شمس قمر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>یاورش بادا حق عزوجل در همه کار</p></div>
<div class="m2"><p>تا در این کار بود با تو به همت یاور</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چون قلم گردد این کار گر آن صدر بزرگ</p></div>
<div class="m2"><p>نیزه‌کردار ببندد ز پی کینه کمر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به تو ای سایهٔ حق خلق جگرسوخته را</p></div>
<div class="m2"><p>او شفیع است چنان کامّت را پیغمبر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>خلق را زین حشر شوم اگر برهانی</p></div>
<div class="m2"><p>کردگارت برهاند ز خطر در محشر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>پیش سلطان جهان سنجر کاو پروردت</p></div>
<div class="m2"><p>ای چنو پادشه دادگر حق‌پرور</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>دیده‌ای خواجهٔ آفاق کمال‌الدین را</p></div>
<div class="m2"><p>که نباشد به جهان خواجه از او کامل‌تر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نیک دانی که چه و تا به کجا داشت بر او</p></div>
<div class="m2"><p>اعتماد آن شه دین‌پرور نیکومحضر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هست ظاهر که بر او هرگز پوشیده نبود</p></div>
<div class="m2"><p>هیچ اسرار ممالک چه ز خیر و چه ز شر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>روشن است آنکه بر آن جمله که خور گردون را</p></div>
<div class="m2"><p>بود ایران را رایش همه عمر اندر خور</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>واندر آن مملکت و سلطنت و آن دولت</p></div>
<div class="m2"><p>چه اثر بود از او هم به سفر هم به حضر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>با کمال‌الدین ابنای خراسان گفتند</p></div>
<div class="m2"><p>قصهٔ ما به خداوند جهان خاقان بر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چون کند پیش خداوند جهان از سر سوز</p></div>
<div class="m2"><p>عرضه این قصهٔ رنج و غم و اندوه و فکر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>از کمال کرم و لطف تو زیبد شاها</p></div>
<div class="m2"><p>کز کمال‌الدین داری سخن ما باور</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>زو شنو حال خراسان و غزان ای شه شرق</p></div>
<div class="m2"><p>که مر او را همه حال است چو الحمد ازبر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>تا کشد رای چو تیر تو در آن قوم کمان</p></div>
<div class="m2"><p>خویشتن پیش چنین حادثه‌ای کرد سپر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>آنچه او گوید محض شفقت باشد از آنک</p></div>
<div class="m2"><p>بسطت ملک تو می‌خواهد نه جاه و خطر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خسروا در همه انواع هنر دستت هست</p></div>
<div class="m2"><p>خاصه در شیوهٔ نظم خوش و اشعار غرر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گر مکرر بود ایطاء در این قافیتم</p></div>
<div class="m2"><p>چون ضروری‌ست شها پردهٔ این نظم مدر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هم بر آن‌گونه که استاد سخن عمعق گفت</p></div>
<div class="m2"><p>خاک خون‌آلود ای باد به اصفاهان بر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بی‌گمان خلق جگرسوخته را دریابد</p></div>
<div class="m2"><p>چون ز درد دلشان یابد از این‌گونه خبر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>تا جهان را بفروزد خور گیتی‌پیمای</p></div>
<div class="m2"><p>از جهان‌داری ای خسرو عادل برخور</p></div></div>