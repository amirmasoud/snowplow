---
title: >-
    قصیدهٔ شمارهٔ ۴۴ - در مدح ملک بدرالدین سنقر
---
# قصیدهٔ شمارهٔ ۴۴ - در مدح ملک بدرالدین سنقر

<div class="b" id="bn1"><div class="m1"><p>عید بر بدر دین مبارک باد</p></div>
<div class="m2"><p>سنقر آن آفتاب دولت و داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه شغل نظام عالم را</p></div>
<div class="m2"><p>چرخ از عدل او نهد بنیاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وانکه قصر خراب دولت را</p></div>
<div class="m2"><p>دهر از دست او کند آباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برق تیغش چو برق روشن و تیز</p></div>
<div class="m2"><p>ابر جودش چو ابر معطی و راد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنگ حلمش ببرده سنگ از خاک</p></div>
<div class="m2"><p>سیر حکمش ربوده گوی از باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همتش آنچنان که از سر عجز</p></div>
<div class="m2"><p>امر او را زمانه گردن داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در شجاعت به روز حرب و مصاف</p></div>
<div class="m2"><p>آنکه شاگرد اوست هست استاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پای چون بر فلک نهاد ز قدر</p></div>
<div class="m2"><p>عدل او بر زمانه دست گشاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای ترا رام بوده هر توسن</p></div>
<div class="m2"><p>وی ترا بنده گشته هر آزاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنده را گرنه حشمتت بودی</p></div>
<div class="m2"><p>کاندرین حادثه شفیع افتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که گشادیش در زمانه ز بند</p></div>
<div class="m2"><p>که رسیدیش در زمین فریاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کاندر اطراف خاوران از وی</p></div>
<div class="m2"><p>هیچ‌کس را همی نیاید یاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرنه عدل تو داد او دادی</p></div>
<div class="m2"><p>آه تا کی برستی از بیداد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چکنم از شب جهان که جهان</p></div>
<div class="m2"><p>این نخستین جفا نبود که زاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همتت چون گشاد دست به عدل</p></div>
<div class="m2"><p>قدر تو بر سپهر پای نهاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا بود ز اختلاف جنبش چرخ</p></div>
<div class="m2"><p>یکی اندوهناک و دیگر شاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هیچ شادیت را مباد زوال</p></div>
<div class="m2"><p>هیچ اندوهت از زمانه مباد</p></div></div>