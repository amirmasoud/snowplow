---
title: >-
    قصیدهٔ شمارهٔ ۷۲ - در مدح میرآب مرو صاحب سعید صدر الدین نظام‌الملک
---
# قصیدهٔ شمارهٔ ۷۲ - در مدح میرآب مرو صاحب سعید صدر الدین نظام‌الملک

<div class="b" id="bn1"><div class="m1"><p>نماز شام چو کردم بسیج راه سفر</p></div>
<div class="m2"><p>درآمد از درم آن سرو قد سیمین‌بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز تف آتش دل وز سرشک دیده شده</p></div>
<div class="m2"><p>لب چو قندش خشک و رخ چو ماهش تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آب دیده همی گشت زلف مشکینش</p></div>
<div class="m2"><p>چو شاخ سنبل سیراب در می احمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا دلی ز غریوش چو اندر آتش عود</p></div>
<div class="m2"><p>مرا تنی ز وداعش چو اندر آب شکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه گفت، گفت نه سوگند خورده‌ای به سرم</p></div>
<div class="m2"><p>که هرگز از خط عشق تو برندارم سر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنوز مدت یک هجر نارسیده به پای</p></div>
<div class="m2"><p>هنوز وعدهٔ یک وصل نارسیده به سر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهانهٔ سفر و عزم رفتن آوردی</p></div>
<div class="m2"><p>دلت ز صحبت یاران ملول گشت مگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه وقت رفتن و هنگام کردن سفرست</p></div>
<div class="m2"><p>سفر مکن که شود بر دلم جهان چو سقر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا درین غم وتیمار ودرد دل مگذار</p></div>
<div class="m2"><p>ز عهد و بیعت و سوگند خویشتن مگذر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وگر به رغم دل من همی بخواهی رفت</p></div>
<div class="m2"><p>از آن دیار خبرده مرا وزان کشور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کجاست مقصد و تا چند خواهی آنجا ماند</p></div>
<div class="m2"><p>کجا رسیم دگر بار و کی به یکدیگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو این بگفت به بر در گرفتمش گفتم</p></div>
<div class="m2"><p>که جان جان و قرار دلی و نور بصر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سفر مربی مردست و آستانهٔ جاه</p></div>
<div class="m2"><p>سفر خزانهٔ مالست و اوستاد هنر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به شهر خویش درون بی‌خطر بود مردم</p></div>
<div class="m2"><p>به کان خویش درون بی‌بها بود گوهر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>درخت اگر متحرک شدی ز جای به جای</p></div>
<div class="m2"><p>نه جور اره کشیدی و نه جفای تبر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به جرم خاک و فلک در نگاه باید کرد</p></div>
<div class="m2"><p>که این کجاست ز آرام و آن کجا ز سفر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز دست فتنهٔ این اختران بی‌معنی</p></div>
<div class="m2"><p>ز دام عشوهٔ این روزگار دون‌پرور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی به خدمت آن صدر روزگار شوم</p></div>
<div class="m2"><p>که روزگار ازو یافتست قدر و خطر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نظام ملکت سلطان و صدر دین خدای</p></div>
<div class="m2"><p>خدایگان وزیران وزیر خوب سیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>محمد آنکه ز جاهش گرفت ملت و ملک</p></div>
<div class="m2"><p>همان نظام که دین ز ابتدا به عدل عمر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بزرگواری کاندر بروج طاعت اوست</p></div>
<div class="m2"><p>مدبران فلک را مدار گرد مدر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر شمایل حلمش نموده کوه سبک</p></div>
<div class="m2"><p>بر بسایط طبعش نموده بحر شمر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه دست او به سخا در چه ابر در نیسان</p></div>
<div class="m2"><p>چه طبع او به سخن در چه بحر بی‌معبر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شمر ز تربیت جود او شود دریا</p></div>
<div class="m2"><p>عرض به تقویت جاه او شود جوهر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز بیم او نچشد شیر شرزه طعم وسن</p></div>
<div class="m2"><p>ز عدل او نبرد شور و فتنه رنج سهر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو باز او شکرد صید او چه شیر و چه گرگ</p></div>
<div class="m2"><p>چو اسب او گذرد راه او چه بحر و چه بر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سعادت ابدی در هوای او مدغم</p></div>
<div class="m2"><p>نوایب فلکی در خلاف او مضمر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر به وجه عنایت کند به شوره نگاه</p></div>
<div class="m2"><p>وگر ز روی سیاست کند به خاره نظر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شود به دولت او خاک شوره مهر گیا</p></div>
<div class="m2"><p>شود ز هیبت او سنگ خاره خاکستر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به ابر بهمن اگر دست جود بنماید</p></div>
<div class="m2"><p>عرق چکد ز مسامش به جای قطر مطر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو دست دولت او بر زمانه بگشودند</p></div>
<div class="m2"><p>کشید پای به دامن درون قضا و قدر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ایا به جاه و شرف با ستاره سوده عنان</p></div>
<div class="m2"><p>و یا به جود و سخا گشته در زمانه سمر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ببرده نام ز فرزانگان به قدر و به جاه</p></div>
<div class="m2"><p>ربوده گوی ز سیارگان به فخر و به فر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به روز بار ترا مهر بالش ومسند</p></div>
<div class="m2"><p>به روز جشن ترا ماه مشرب و ساغر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کند نسیم رضای تو کاه را فربه</p></div>
<div class="m2"><p>کند سموم خلاف تو کوه را لاغر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به حضرت تو درون تیر کلک مستوفی</p></div>
<div class="m2"><p>به مجلس تودرون زهره ساز خنیاگر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز تیر حادثه ایمن شد و سنان بلا</p></div>
<div class="m2"><p>هر آفریده که کرد از حمایت تو سپر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به زیر سایهٔ عدل تو نیست خوف و رجا</p></div>
<div class="m2"><p>ورای پایهٔ قدر تو نیست زیر و زبر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بجز در آینهٔ خاطر تو نتوان دید</p></div>
<div class="m2"><p>ز راز چرخ نشان و ز علم غیب خبر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگر ز حلم تو یک ذره بر سپهر نهند</p></div>
<div class="m2"><p>قرار یابد ازو همچو کشتی از لنگر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نسیم لطف تو ار بگذرد به آتش تیز</p></div>
<div class="m2"><p>ز شعلهاش گشاید به خاصیت کوثر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>حسام قهر تو شخص اجل زند به دو نیم</p></div>
<div class="m2"><p>چنان که ماه فلک را بنان پیغمبر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به نیش کژدم قهرت اگر قضا بزند</p></div>
<div class="m2"><p>عدوت را که سیه روز باد و شوم اختر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به هیچ داروی و تریاک برنیارد خاست</p></div>
<div class="m2"><p>ز خاک جز که به آواز صور در محشر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>قدر ز شست تو بر اختران رساند تیر</p></div>
<div class="m2"><p>قضا ز دست تو بر آسمان گشاید در</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چه باره‌ایست به زیر تو در بنامیزد</p></div>
<div class="m2"><p>که منزلیش بود باختر دگر خاور</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هلال نعل فلک قامت ستاره مسیر</p></div>
<div class="m2"><p>زمین‌نوردی دریا گذار که پیکر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به زور چرخ و به آواز رعد و جستن برق</p></div>
<div class="m2"><p>به قد کوه و تن پیل و پویهٔ صرصر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گه درنگ ازو طیره خورده پای خیال</p></div>
<div class="m2"><p>گه شتاب درو خیره مانده مرغ به پر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گه تحرک او منقطع صبا و دبور</p></div>
<div class="m2"><p>بر تحمل او مضطرب حدید و حجر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>درخش نعلش سندان و سنگ را در خاک</p></div>
<div class="m2"><p>فروغ و شعله دهد همچو اختر و اخگر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بزرگوارا دریا دلا خداوندا</p></div>
<div class="m2"><p>ترا سپهر سریرست و آفتاب افسر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز شوق خدمت تو عمرها گذشت که من</p></div>
<div class="m2"><p>چو شکرم در آب و چو عود بر آذر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بدان عزیمت و اندیشه‌ام که تا ننهد</p></div>
<div class="m2"><p>قضابه دست اجل بر به حنجرم خنجر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بجز مدیح توام برنیاید از دیوان</p></div>
<div class="m2"><p>بجز ثنای توام بر نیاید از دفتر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز نظم و نثر مدیح تو اندر آویزم</p></div>
<div class="m2"><p>ز گوش و گردم ایام عقدهای گهر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نه نظم بلکه ازین درجهای پر ز نکت</p></div>
<div class="m2"><p>نه نثر بلکه ازین درجهای پر ز درر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>همیشه تا که بروید ز خاکها زر و سیم</p></div>
<div class="m2"><p>همیشه تا که بتابد ز آسمان مه و خور</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>علو و رفعت تو همچو ماه باد و چو مهر</p></div>
<div class="m2"><p>سرشک و چهرهٔ خصمت چو سیم باد و چو زر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تو بر میان کمر ملک بسته و جوزا</p></div>
<div class="m2"><p>به پیش طالع سعدت همیشه بسته کمر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>جهان مطیع و فلک تابع و ستاره حشم</p></div>
<div class="m2"><p>زمان غلام و قضا بنده و قدر چاکر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>درخت بخت حسود ترانه بیخ و نه شاخ</p></div>
<div class="m2"><p>چو شاخ دولت خصم ترانه بار و نه بر</p></div></div>