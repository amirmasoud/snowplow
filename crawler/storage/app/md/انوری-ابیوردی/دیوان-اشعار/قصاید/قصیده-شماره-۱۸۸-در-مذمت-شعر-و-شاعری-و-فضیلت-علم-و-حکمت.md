---
title: >-
    قصیدهٔ شمارهٔ ۱۸۸ - در مذمت شعر و شاعری و فضیلت علم و حکمت
---
# قصیدهٔ شمارهٔ ۱۸۸ - در مذمت شعر و شاعری و فضیلت علم و حکمت

<div class="b" id="bn1"><div class="m1"><p>ای برادر بشنوی رمزی ز شعر و شاعری</p></div>
<div class="m2"><p>تا ز ما مشتی گداکس را به مردم نشمری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دان که از کناس ناکس در ممالک چاره نیست</p></div>
<div class="m2"><p>حاش لله تا نداری این سخن را سرسری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زانکه گر حاجت فتد تا فضله‌ای را کم کنی</p></div>
<div class="m2"><p>ناقلی باید تو نتوانی که خود بیرون بری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار خالد جز به جعفر کی شود هرگز تمام</p></div>
<div class="m2"><p>زان یکی جولاهگی داند دگر برزیگری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز اگر شاعر نباشد هیچ نقصانی فتد</p></div>
<div class="m2"><p>در نظام عالم از روی خرد گر بنگری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آدمی را چون معونت شرط کار شرکتست</p></div>
<div class="m2"><p>نان ز کناسی خورد بهتر بود کز شاعری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن شنیدستی که نهصد کس بباید پیشه‌ور</p></div>
<div class="m2"><p>تا تو نادانسته و بی‌آگهی نانی خوری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ازاء آن اگر از تو نباشد یاریی</p></div>
<div class="m2"><p>آن نه نان خوردن بود دانی چه باشد مدبری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو جهان را کیستی تا بی‌معونت کار تو</p></div>
<div class="m2"><p>راست می‌دارند از نعلین تا انگشتری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون نداری بر کسی حقی حقیقت دان که هست</p></div>
<div class="m2"><p>هم تقاضا ریش گاوی هم هجا کون خری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از چه واجب شد بگو آخر بر این آزادمرد</p></div>
<div class="m2"><p>اینکه می‌خواهی ازو وانگه بدین مستکبری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>او ترا کی گفت کاین کلپترها را جمع کن</p></div>
<div class="m2"><p>تا ترا لازم شود چندین شکایت گستری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عمر خود خود می‌کنی ضایع ازو تاوان مخواه</p></div>
<div class="m2"><p>هم تو حاکم باش تا هم زانکه بفروشی خری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عقل را در هر چه باشی پیشوای خویش ساز</p></div>
<div class="m2"><p>زانکه پیدا او کند بدبختی از نیک‌اختری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خود جز از بهر بقای عدل دیگر بهر چیست</p></div>
<div class="m2"><p>این سیاستها که موروثست از پیغمبری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من نیم در حکم خویش از کافریهای سپهر</p></div>
<div class="m2"><p>ورنه در انکار من چه شاعری چه کافری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دشمن جان من آمد شعر چندش پرورم</p></div>
<div class="m2"><p>ای مسلمانان فغان از دست دشمن‌پروری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شعر دانی چیست دور از روی تو حیض‌الرجال</p></div>
<div class="m2"><p>قایلش گو خواه کیوان باش و خواهی مشتری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا به معنی های بکرش ننگری زیرا که نیست</p></div>
<div class="m2"><p>حیض را در مبدا فطرت گزیر از دختری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر مرا از شاعری حاصل همین عارست و بس</p></div>
<div class="m2"><p>موجب توبه است و جای آنکه دیوان بستری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اینکه پرسد هر زمان آن کون خر این ریش گاو</p></div>
<div class="m2"><p>کانوری به یا فتوحی در سخن یا سنجری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>راستی به بوفراس آمد به کار از شاعران</p></div>
<div class="m2"><p>وان نه از جنس سخن یا از کمال قادری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وانکه او چون دیگران مدح و هجا هرگز نگفت</p></div>
<div class="m2"><p>پس مرنج ار گویدت من دیگرم تو دیگری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آمدم با این سخن کز دست بنهادم نخست</p></div>
<div class="m2"><p>زانکه بی‌داور نیارم کرد چندین داوری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای به جایی در سخندانی که نظمت واسطه است</p></div>
<div class="m2"><p>هرکجا شد منتظم عقدی ز چه از ساحری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون ندارد نسبتی با نظم تو نظم جهان</p></div>
<div class="m2"><p>در سخن خواهی مقنع باش و خواهی سامری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گنج اتسز گنج قارون بود اگر نی کی شدی</p></div>
<div class="m2"><p>از یکی منحول چندان کم بهارا مشتری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مهتران با شین شعرند ارنه کی گشتی چنین</p></div>
<div class="m2"><p>منتشر با قصهٔ محمود ذکر عنصری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کو رییس مرو منصور آنکه در هفتاد سال</p></div>
<div class="m2"><p>شعر نشنید و نگفت اینک دلیل مهتری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا نپنداری که باعث بخل بود او را بدان</p></div>
<div class="m2"><p>در کسی چون ظن بری چیزی کزان باشد بری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زانکه امثال مرا بی‌شاعری بسیار داد</p></div>
<div class="m2"><p>کاخهای چارپوشش باغهای چل‌گری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرد را حکمت همی باید که دامن گیردش</p></div>
<div class="m2"><p>تا شفای بوعلی بیند نه ژاژ بحتری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عاقلان راضی به شعر از اهل حکمت کی شوند</p></div>
<div class="m2"><p>تا گهر یابند، مینا کی خرند از گوهری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یارب از حکمت چه برخوردار بودی جان من</p></div>
<div class="m2"><p>گر نبودی صاع شعر اندر جوالم بر سری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>انوری تا شاعری از بندگی ایمن مباش</p></div>
<div class="m2"><p>کز خطر درنگذری تا زین خطا درنگذری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گرچه سوسن صد زبان آمد چو خاموشی گزید</p></div>
<div class="m2"><p>خط آزادی نبشتش گنبد نیلوفری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خامشی را حصن ملک انزوا کن ور به طبع</p></div>
<div class="m2"><p>خوش نیاید نفس را گو زهرخند و خون‌گری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کشتیی بر خشک می‌ران زانکه ساحل دور نیست</p></div>
<div class="m2"><p>گو مباشت پیرهن دامن نگهدار از تری</p></div></div>