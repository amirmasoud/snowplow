---
title: >-
    قصیدهٔ شمارهٔ ۲۴ - در مدح مجدالدین محمدبن نصر احمد
---
# قصیدهٔ شمارهٔ ۲۴ - در مدح مجدالدین محمدبن نصر احمد

<div class="b" id="bn1"><div class="m1"><p>گرچرخ را در این حرکت هیچ مقصدست</p></div>
<div class="m2"><p>از خدمت محمدبن نصر احمدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرزانه ای که بابت گاهست وبالشست</p></div>
<div class="m2"><p>آزاده‌ای که درخور صدرست ومسندست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با بذل دست بخشش او ابر مدخلست</p></div>
<div class="m2"><p>با سیر برق خاطر او ابر مقعدست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عزم او طلایه تقدیر منهزم</p></div>
<div class="m2"><p>با رای او زبانهٔ خورشید اسودست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون حرف آخرست ز ابجد گه سخن</p></div>
<div class="m2"><p>وز راستی چو حرف نخستین ابجدست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ملک ز اهتمام تو تمهید یافتست</p></div>
<div class="m2"><p>شغل ملوک و کار ممالک ممهدست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای سروری که حزم تو تسدید ملک را</p></div>
<div class="m2"><p>هنگام دفع حادثه سد مسددست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از عادت حمید تو هر دم به تازگی</p></div>
<div class="m2"><p>رسمیست در جهان که جهانی مجددست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تادست تو گشاده شد اندر مکاتبت</p></div>
<div class="m2"><p>از خجلت تو دست عطارد مقیدست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اصل جهان تویی و ازو پیشی آنچنانک</p></div>
<div class="m2"><p>اصل عدد یکیست ولی نامعددست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم نیاز پیش کف تو چنان بود</p></div>
<div class="m2"><p>گویی که چشم افعی پیش زمردست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خصم ترا به فرق برست از زمانه دست</p></div>
<div class="m2"><p>تاپای تو ز مرتبه بر فرق فرقدست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اسب فلک جواد عنان تو شد چنانک</p></div>
<div class="m2"><p>ماه و مجره اسب ترا نعل و مقودست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا شکل گنبد فلک و جرم آفتاب</p></div>
<div class="m2"><p>چون درقهٔ مکوکب و درع مزردست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تیغ فلک ز تیغ تو اندر نیام باد</p></div>
<div class="m2"><p>تا بر فلک مجره چو تیغ مهندست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چشم بد از تو دور که در روزگار تو</p></div>
<div class="m2"><p>چشم بلا و فتنهٔ ایام ارمدست</p></div></div>