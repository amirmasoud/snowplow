---
title: >-
    قصیدهٔ شمارهٔ ۱۹۱ - در صفت بزم و مدح ملک اعظم عماد الدین فیروزشاه و دستور بزرگ
---
# قصیدهٔ شمارهٔ ۱۹۱ - در صفت بزم و مدح ملک اعظم عماد الدین فیروزشاه و دستور بزرگ

<div class="b" id="bn1"><div class="m1"><p>حبذا بزمی کزو هردم دگرگون زیوری</p></div>
<div class="m2"><p>آسمان بر عالمی بندد زمین بر کشوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشوری و عالمی را هم زمین هم آسمان</p></div>
<div class="m2"><p>از چنین بزمی تواند داد هردم زیوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجلس کو دعوی فردوس را باطل کند</p></div>
<div class="m2"><p>گر میان هر دو بنشانند عادل داوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با هوای سقف او رونق نبیند نافه‌ای</p></div>
<div class="m2"><p>با زمین صحن او قیمت نیابد عنبری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خیال نقش بت‌رویان او واله شوند</p></div>
<div class="m2"><p>گر ز دور هر گریبان سر برآرد آزری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جنتست آن عرصه گر بی‌وعده یابی جنتی</p></div>
<div class="m2"><p>کوثرست آن باده گر مستی فزاید کوثری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساغرش پر بادهٔ رنگین چنان آید به چشم</p></div>
<div class="m2"><p>کز میان آب روشن برفروزی آذری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتش سیال دیدستی در آب منجمد</p></div>
<div class="m2"><p>گر ندیدستی بخواه از ساقیانش ساغری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست مصر جامع هستی از آن خارج نیافت</p></div>
<div class="m2"><p>روزگار از عرصهٔ او یک عرض را جوهری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آسمان دیگر است از روی رتبت گوییا</p></div>
<div class="m2"><p>واندرو هر ساکنی قایم مقام اختری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آفتاب و ماه او پیروزشاه و صاحبند</p></div>
<div class="m2"><p>شه سلیمان عنصری دستور آصف گوهری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیر مان ای حضرتی کز سعی بنای سپهر</p></div>
<div class="m2"><p>خاک را حاصل نخواهد گشت مثلث دیگری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا چه عالی حضرتی کاین آفتاب خسروی</p></div>
<div class="m2"><p>هر زمان از سدهٔ قصر تو سازد خاوری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آفتابی گر بخواهد برگشاید نور اوی</p></div>
<div class="m2"><p>جاودان از نیم‌روز اندر شب گیتی دری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر کواکب را مسلم گشتی این عالی سپهر</p></div>
<div class="m2"><p>هریکی بودندی اندر فوج دیگر چاکری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جرم کیوان آن معمر هندوی باریک‌بین</p></div>
<div class="m2"><p>پاسبان تو نشاندی هر شبی بر منظری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مشتری اندر ادای خطبهٔ این خسروی</p></div>
<div class="m2"><p>معتکف بنشسته بودی روز و شب بر منبری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>والی عقرب ز بهر منع و رد حادثات</p></div>
<div class="m2"><p>بر درش بودی به هر دستی کشیده خنجری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زهره اندر روزهای عیش و خلوتهای شب</p></div>
<div class="m2"><p>بسته بودی خویشتن بر دامن خنیاگری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تیر مستوفی به دیوان در چو شاگردان او</p></div>
<div class="m2"><p>می‌بریدی کاغذی یا می‌شکستی دفتری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای خداوندی که تا بیخ صنایع شاخ زد</p></div>
<div class="m2"><p>شاخ هستی را ندادند از تو کاملتر بری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آسمان قدری که صاحب افسر گردون نیافت</p></div>
<div class="m2"><p>ملک آب و خاک را همچون تو صاحب افسری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون لب ساغر بخندد هر ندیمت صاحبی</p></div>
<div class="m2"><p>چون سر خنجر بگرید هر غلامت قیصری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جام و خنجر چون تو یک صاحب قران هرگز ندید</p></div>
<div class="m2"><p>بزم را سائل نوازی رزم را کین‌آوری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بوستان ملک را چه از شبیخون خزان</p></div>
<div class="m2"><p>تا چو چشم بخت تو بیدار دارد عبهری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر شود پاس تو در ملک طبیعت محتسب</p></div>
<div class="m2"><p>آسمان انگشت ننهد تا ابد بر منکری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ور نشاندی نائبی بر چارسوی آسمان</p></div>
<div class="m2"><p>زهره هرگز درنیاید نیز جز با چادری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ابر می‌بارید روزی پیش دستت بی‌خبر</p></div>
<div class="m2"><p>برق می‌خندید و می‌گفت اینت عاقل مهتری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ابر اگر از فتحباب دستت آبستن شود</p></div>
<div class="m2"><p>قطرهٔ باران کند از هر حشیشی عرعری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>معن و حاتم گر بدیدندی دل و دست ترا</p></div>
<div class="m2"><p>هریکی بر بخل آن دیگر نوشتی محضری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در چنان دوران که عمری در سه کشور بلکه بیش</p></div>
<div class="m2"><p>ز ایمنی زادن سترون شد چو گردون مادری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بالش عالیت سد فتنه شد ورنه کجا</p></div>
<div class="m2"><p>پهلویی در ایمنی هرگز نسودی بستری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دختران روزگارند این حوادث وین بتر</p></div>
<div class="m2"><p>کو چو زاید دختری دخترش زاید دختری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>روز هیجا کز خروش و گرد جیشت سایه را</p></div>
<div class="m2"><p>تا سوار خویش را یابد بباید رهبری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از پس گرد سپه برق سنان آبدار</p></div>
<div class="m2"><p>همچنان باشد که اندر پردهٔ شب اخگری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آسمان ابریق شریان را گشاید نایژه</p></div>
<div class="m2"><p>تا بشوید روزگار از گرد هیجا خنجری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هر کمان ابری بود بارنده پیکان ژاله‌وار</p></div>
<div class="m2"><p>هر سنان برقی شود هر بارگیری صرصری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون بجنبانی عنان صرصر که پیکرت</p></div>
<div class="m2"><p>بانگ شب خوش باد جان برخیزد از هر پیکری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>لشکری را هیزم دوزخ کنی در ساعتی</p></div>
<div class="m2"><p>ای تو تنها هم پناه لشکر و هم لشکری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اژدهای رمح تو خلقی به یک دم درکشد</p></div>
<div class="m2"><p>وانگهی فربه نگردد اینت معجز لاغری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عقل با رمح تو فتوی می‌دهد اکنون که چوب</p></div>
<div class="m2"><p>شاید ار ثعبان شود بی‌معجز پیغمبری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خنجرت سبابهٔ پیغمبرست از خاصیت</p></div>
<div class="m2"><p>زان به هر ایما چو مه از هم بدرد مغفری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>با چنین اعجاز کاندر خنجر تو تعبیه است</p></div>
<div class="m2"><p>بر سر خصم لعین چه مغفری چه معجری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بر زبان خنجرت روزی به طنازی برفت</p></div>
<div class="m2"><p>کاسمان چون من نیارد هیچ نصرت پروری</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گفت نصرت نی مرا بازوی شه می‌پرورد</p></div>
<div class="m2"><p>لاجرم هر ذوالفقاری را بباید حیدری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خسروا من بنده را در مدت این هفت ماه</p></div>
<div class="m2"><p>گر میسر گشتی اندر هفت کشور یاوری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا مرا از لجهٔ دریای حرمان دوست‌وار</p></div>
<div class="m2"><p>فی‌المثل بر تخته‌ای بردی کشان یا معبری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هستمی از بس که سر بر آستانت سودمی</p></div>
<div class="m2"><p>چون دگر ابنای جنس خویش اکنون سروری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>لیکن از بس قصد این ناقص عنایت روزگار</p></div>
<div class="m2"><p>مانده‌ام در قعر دریای عنا چون لنگری</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>روزگار این جنس با من بس که دارد قصدها</p></div>
<div class="m2"><p>آن چنان بی‌رحمتی نامهربانی کافری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هم توانستی گرم شاکر ترک زین داشتی</p></div>
<div class="m2"><p>تا نبودی چون منش باری شکایت گستری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تا صبا از سر جهان را هر بهاری بی‌دریغ</p></div>
<div class="m2"><p>در کنار دایهٔ گردون نهد چون دلبری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بی‌دریغت باد ملک اندر کنار خسروی</p></div>
<div class="m2"><p>تا نیاید گردش ایام را پیدا سری</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خصم چون پرگار سرگردان و رای صایبت</p></div>
<div class="m2"><p>استوای کارهای ملک را چون مسطری</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>آسمان ملک را دایم تو بادی آفتاب</p></div>
<div class="m2"><p>از سعود آسمان گردت مجاور معشری</p></div></div>