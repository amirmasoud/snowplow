---
title: >-
    قصیدهٔ شمارهٔ ۷۵ - در مدح وزیر علاء الدین بوبویه
---
# قصیدهٔ شمارهٔ ۷۵ - در مدح وزیر علاء الدین بوبویه

<div class="b" id="bn1"><div class="m1"><p>چو زیر مرکز چرخ مدور</p></div>
<div class="m2"><p>نهان شد جرم خورشید منور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه عید از فلک رخسار بنمود</p></div>
<div class="m2"><p>نه پیدایی تمام و نه مستر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو تیغ ناخنی بر چرخ مینا</p></div>
<div class="m2"><p>چو شست ماهیی در بحر اخضر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در اجسام زمین سیرش مؤثر</p></div>
<div class="m2"><p>وز اجرام فلک ذاتش مؤثر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دبیری بود از او برتر بفکرت</p></div>
<div class="m2"><p>چو فکرت بی‌نیاز از کلک و دفتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسی اسرار جزوی کرده معلوم</p></div>
<div class="m2"><p>بسی احکام کلی کرده از بر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزاران پیکر جنی و انسی</p></div>
<div class="m2"><p>ز نور پیکر او در دو پیکر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بتی بر غرفهٔ دیگر خرامان</p></div>
<div class="m2"><p>چو بت‌رویان چین زیبا و دلبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز فرقش تا قدم در ناز و کشی</p></div>
<div class="m2"><p>ز پایش تا به سر در زر و زیور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دستی بربطی با صوت موزون</p></div>
<div class="m2"><p>به دیگر ساغری پر خمر احمر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برازوی صحن دیگر بود خالی</p></div>
<div class="m2"><p>چو لشکرگاه بی‌سلطان ولشکر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گمانی آمدم کانجا کسی نیست</p></div>
<div class="m2"><p>به ظاهر از مجاور یا مسافر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خرد گفت این حریم پادشاهیست</p></div>
<div class="m2"><p>به شاهی برتر از خاقان و قیصر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز عدل او همی بارد هوا نم</p></div>
<div class="m2"><p>ز فیض او همی زاید زمین زر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنان کامل که نه گرم است و نه سرد</p></div>
<div class="m2"><p>چنان عادل که نه خشک است و نه تر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ولیکن دیدن او نیست ممکن</p></div>
<div class="m2"><p>که شب ممکن نباشد دیدن خور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وزین بربود دیوانی و در وی</p></div>
<div class="m2"><p>دلاور قهرمانی ترک اشقر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به روز جنگ با دستان رستم</p></div>
<div class="m2"><p>به پیش خصم با پیکار حیدر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>درآرد از عدم عنقا به ناوک</p></div>
<div class="m2"><p>ببرد خاصیت ز اشیا به خنجر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برازوی خواجهٔ چونان ممکن</p></div>
<div class="m2"><p>که تمکین بودش از تمکین مسخر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز عونش از عنایت چار عنصر</p></div>
<div class="m2"><p>ز سیرش با سعادت هفت کشور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>غنی و نعمت او دانش ودین</p></div>
<div class="m2"><p>سخی و بخشش او حشمت وفر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وزو بر پیر دیگر بود هندی</p></div>
<div class="m2"><p>بزرگ اندیشه‌ای چونان معمر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که ذاتش داشت بر آرام پیشی</p></div>
<div class="m2"><p>که زادش بود با جنبش برابر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وفاق او صلاح اهل عالم</p></div>
<div class="m2"><p>خلاف او فساد کون و جوهر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خیالات ثوابت در خیالم</p></div>
<div class="m2"><p>چنان آمد همی بی‌حد و بی‌مر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که اندر چرخ کحلی کرده ترکیب</p></div>
<div class="m2"><p>هزاران در و مروارید و گوهر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شهاب تیزرو چون بسدین تیر</p></div>
<div class="m2"><p>گذاره کرده از پیروزه مغفر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مجره گفتیی تیغ گهردار</p></div>
<div class="m2"><p>نهادستی بزنگاری سپر بر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به شاخ ثور بر شکل ثریا</p></div>
<div class="m2"><p>چو مرواریدگون بار صنوبر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بنات‌النعش گرد قطب گردان</p></div>
<div class="m2"><p>گهی از جرم زیر و گاه از بر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو گرد مرکز رای خداوند</p></div>
<div class="m2"><p>قضای ایزد دادار داور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وزیر ملک سلطان معظم</p></div>
<div class="m2"><p>نصیر دین یزدان و پیمبر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جهان حمد محمود آنکه از جاه</p></div>
<div class="m2"><p>جهان حمدش گرفت از پای تا سر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مؤخر عهد و در دانش مقدم</p></div>
<div class="m2"><p>مقدم عقل و در رتبت مؤخر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به جنب رایش اجرام سماوی</p></div>
<div class="m2"><p>چو با خورشید اجرام مکدر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه اوج قدر او را هیچ پستی</p></div>
<div class="m2"><p>نه بحر طبع او را هیچ معبر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ندارد عقل بی‌عونش هدایت</p></div>
<div class="m2"><p>نگیرد باز بی‌سعیش کبوتر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یقینی چون گمان او نباشد</p></div>
<div class="m2"><p>نباشد دیدهٔ احوال چو احور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به وهمش قدرت آن هست کز دهر</p></div>
<div class="m2"><p>بگرداند بد و نیک مقدر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به قدرش قوت آن هست کز سهم</p></div>
<div class="m2"><p>کشد پیش قضا سد سکندر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کفش بحرست و موجش جود و بخشش</p></div>
<div class="m2"><p>خطش تارست و پودش مشک و عنبر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگرنه نهی کردستی ز اسراف</p></div>
<div class="m2"><p>خدای و نهی او نهیی است منکر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز افراط سخای او شدستی</p></div>
<div class="m2"><p>جهان درویش و درویشی توانگر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سموم قهرش اندر لجهٔ بحر</p></div>
<div class="m2"><p>نسیم لطفش اندر شورهٔ بر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>برآرد از مسام ماهی آتش</p></div>
<div class="m2"><p>برآرد از غبار تیره عرعر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نه با آرام حلمش خاک را صبر</p></div>
<div class="m2"><p>نه با تعجیل امرش باد را پر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به جنب آن خفیف، اثقال مرکز</p></div>
<div class="m2"><p>به پیش این کسل، اعجال صرصر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گرش بهتان نهد خصم بداندیش</p></div>
<div class="m2"><p>ورش عصیان کند چرخ ستمگر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>لعاب آن شود چون آب افیون</p></div>
<div class="m2"><p>نجوم این شود چون جرم اخگر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگرنه کلک او شد ناف آهو</p></div>
<div class="m2"><p>وگرنه طبع او شد ابر آذر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چرا بارد به نطق آن در دریا</p></div>
<div class="m2"><p>چرا ساید به نوک این مشک اذفر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در این جنبش اگر جز قوت نفس</p></div>
<div class="m2"><p>فلک را علتی یابند دیگر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نظام کار او باشد که او را</p></div>
<div class="m2"><p>همی از باختر تازد به خاور</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ایا طبع تو بر احسان موفق</p></div>
<div class="m2"><p>و یا بخت تو بر اعدا مظفر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تویی آن‌کس که گر کوشی، برآری</p></div>
<div class="m2"><p>به قهر از صبح عالم شام محشر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تویی آن‌کس که گر خواهی برانی</p></div>
<div class="m2"><p>به لطف از دود دوزخ آب کوثر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نیاوردست پوری بهتر از تو</p></div>
<div class="m2"><p>جهان از نه پدر وز چار مادر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تو عقلی بوده‌ای در بدو ابداع</p></div>
<div class="m2"><p>هدایت را چنان لابد و درخور</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>که جز نور تو تااکنون نبودست</p></div>
<div class="m2"><p>هیولی را به صورت هیچ رهبر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>زمین پیش وقار تو مجوف</p></div>
<div class="m2"><p>جهان پیش کمال تو محقر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>خرد جز در دماغ تو شمیده</p></div>
<div class="m2"><p>سخن جز در ثنای تو مزور</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تو بیش از عالمی گرچه درویی</p></div>
<div class="m2"><p>چو رمز معنوی در لفظ ابتر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کند با لطف تو دوران گردون</p></div>
<div class="m2"><p>چنان چون با سمندر طبع آذر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بود با تو هدر وسواس شیطان</p></div>
<div class="m2"><p>چنان چون با پسر تعلیم آزر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>حوادث چون به درگاهت رسیدند</p></div>
<div class="m2"><p>نزاید بیش از ایشان فتنه و شر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>که شب را تیرگی چندان بماند</p></div>
<div class="m2"><p>که رخ پیدا کند خورشید ازهر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>جهان از فتنه طوفانست و در وی</p></div>
<div class="m2"><p>پناه و حلم تو کشتی و لنگر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>اگر پیروزیی بینی ز خود دان</p></div>
<div class="m2"><p>بزیر دور این پیروزه چادر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>وگر من بنده را حرمان من داشت</p></div>
<div class="m2"><p>دو روز از خدمتت مهجور و مضطر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو دارم حلقهٔ عهد تو در گوش</p></div>
<div class="m2"><p>به یک جرمم مزن چون حلقه بر در</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>تو مخدوم قدیمی انوری را</p></div>
<div class="m2"><p>چنان چون بوالفرج را بوالمظفر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>مرا درگاه تو قبله است و در وی</p></div>
<div class="m2"><p>اگر کفران کنم چه من چه کافر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نمی‌گویم که تقصیری نرفته است</p></div>
<div class="m2"><p>درین مدت که نتوان کرد باور</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ولیکن اختیار من نبودست</p></div>
<div class="m2"><p>که مجبور فلک نبود مخیر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>از این بی‌پا و سر گردون گردان</p></div>
<div class="m2"><p>به سرگردانیی بودستم اندر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>که گر تقریر آن بودی در امکان</p></div>
<div class="m2"><p>زبانم اندکی کردی مقرر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>به ابرامی که دادم عذر نه زانگ</p></div>
<div class="m2"><p>بود گستاخ‌تر دیرینه چاکر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>همیشه تا بود دی پیش از امروز</p></div>
<div class="m2"><p>همیشه تا بود دی بعد آذر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>همه آذرت با دی باد مقرون</p></div>
<div class="m2"><p>همه امروز از دی باد خوشتر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>به هر چت رای بگراید مهیا</p></div>
<div class="m2"><p>به هر چت کام روی آرد میسر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>حساب عمر تو چون دور گردون</p></div>
<div class="m2"><p>به تکراری که سر ناید مکرر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چنان چون مرجع اجزا سوی کل</p></div>
<div class="m2"><p>چو کان بادست رادت مرجع زر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>نکوخواهت نکونام و نکوبخت</p></div>
<div class="m2"><p>بداندیشت بدآیین و بداختر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>همه روزت چو روز عیداضحی</p></div>
<div class="m2"><p>همه سالت نشاط جام و ساغر</p></div></div>