---
title: >-
    قصیدهٔ شمارهٔ ۵۶ - در مدح ابوالحسن مجدالدین علی عمرانی
---
# قصیدهٔ شمارهٔ ۵۶ - در مدح ابوالحسن مجدالدین علی عمرانی

<div class="b" id="bn1"><div class="m1"><p>اکنون که ماه روزه به نقصان در اوفتاد</p></div>
<div class="m2"><p>آه از حجاب حجرهٔ دل بر در اوفتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هجران ماه روزه پیام وصال داد</p></div>
<div class="m2"><p>اینک نهیب او به جهان اندر اوفتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوید به چند روز دگر طبع نفس را</p></div>
<div class="m2"><p>دیدی که رسم توبه ز عالم بر اوفتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن شد که از تقرب مصحف به اختیار</p></div>
<div class="m2"><p>از دست پایمرد طرب ساغر اوفتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن مرغ را که بال و پر از شوق توبه بود</p></div>
<div class="m2"><p>هم بال ریخت از خلل و هم پر اوفتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق و سرور و لهو مرا در نهاد رست</p></div>
<div class="m2"><p>سودای جام و باده مرا در سر اوفتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن‌کس که از دو کون به یکباره دل بشست</p></div>
<div class="m2"><p>او را دو چشم بر دو رخ دلبر اوفتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرماندهٔ زمین و زمان مجد دین که مجد</p></div>
<div class="m2"><p>با طینت مطهر او در خور اوفتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن ملجا ملوک و سلاطین که شخص را</p></div>
<div class="m2"><p>از کارها عبادت او خوشتر اوفتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر وسعت ممالک جاهش گواه شد</p></div>
<div class="m2"><p>صیتی که در زمانه ز خشک و تر اوفتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون کین او ز مرکز علوی سفر نمود</p></div>
<div class="m2"><p>از بیم لرزه بر فلک و اختر اوفتاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در باختر سیاست او چون کمان کشید</p></div>
<div class="m2"><p>تیرش سپر سپر شد ودر خاور اوفتاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای صاحبی که صورت جان عدوی ملک</p></div>
<div class="m2"><p>از قهر تو در آینهٔ خنجر اوفتاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دریا دلی و غرقهٔ دریای نیستی</p></div>
<div class="m2"><p>از اعتماد جود تو بر معبر اوفتاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جایی که عرضه کرد جهان بر و داد ملک</p></div>
<div class="m2"><p>افسار در مقابلهٔ افسر اوفتاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روزی که عنف و خشم شد از یاد چرخ را</p></div>
<div class="m2"><p>آتش ز کارزار تو در چنبر اوفتاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرگ از برای دادن دارو طبیب شد</p></div>
<div class="m2"><p>بیمار هیبت تو چو بر بستر اوفتاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در موضعی که جود تو پرواز کرد زود</p></div>
<div class="m2"><p>در پیش ز ایران تو زر بر زر اوفتاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در درج گوشها به نظاره عقود را</p></div>
<div class="m2"><p>از لفظ تو نظر همه بر گوهر اوفتاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دریای انتقام تو آنجا که موج زد</p></div>
<div class="m2"><p>از کشتی حیات و بقا لنگر اوفتاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قصد جبین ماه و رخ آفتاب کرد</p></div>
<div class="m2"><p>حرفی که از مدیح تو بر دفتر اوفتاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از یک صریر کلک تو در نوبت نبرد</p></div>
<div class="m2"><p>از صد هزار سر به فزع مغفر اوفتاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اقبال تو به چشم رضا روی ملک دید</p></div>
<div class="m2"><p>خورشید بر سرادق نیلوفر اوفتاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیغام تو به فکر درافکند اضطراب</p></div>
<div class="m2"><p>از مرتضی نه زلزله در خیبر اوفتاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از نسل آدم آنکه یقین بود مهر او</p></div>
<div class="m2"><p>بر خدمت تو در شکم مادر اوفتاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از شاخ خدمت تو که طوبی است بیخ او</p></div>
<div class="m2"><p>هر میوه‌ای به خاصیت دیگر اوفتاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>الحق محال نیست که بنده چو دیگران</p></div>
<div class="m2"><p>از عشق خدمت تو بدین کشور اوفتاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>او را که شکرها ز شکرریز شعرهاست</p></div>
<div class="m2"><p>زهری به دست واقعه در شکر اوفتاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از حضرتی حشر به درش حاضر آمدند</p></div>
<div class="m2"><p>نادیده مرگ در فزع محشر اوفتاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تیمارش از تعرض هر بی‌خبر فزود</p></div>
<div class="m2"><p>دستارش از عقیلهٔ مه معجر اوفتاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بشنو که در عذاب چگونه رسید صبر</p></div>
<div class="m2"><p>بنگر که در خلاب چگونه خر اوفتاد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>با منکران عقل در این خطه کار او</p></div>
<div class="m2"><p>داند همی خدای که بس منکر اوفتاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کافور در غذاش به افطار هر شبی</p></div>
<div class="m2"><p>از جور این دو سنگدل کافر اوفتاد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از بس که بار داوری این و آن کشید</p></div>
<div class="m2"><p>او را سخن به حضرت این داور اوفتاد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا آگه است عقل که از خامهٔ قضا</p></div>
<div class="m2"><p>نقش وجود قابل نفع و ضر اوفتاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بادا همیشه طالب آزرم تو سپهر</p></div>
<div class="m2"><p>گرچه ازو عدوی تو در آذر اوفتاد</p></div></div>