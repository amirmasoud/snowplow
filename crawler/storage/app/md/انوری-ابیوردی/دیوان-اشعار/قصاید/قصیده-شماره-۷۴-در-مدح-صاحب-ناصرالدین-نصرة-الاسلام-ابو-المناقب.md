---
title: >-
    قصیدهٔ شمارهٔ ۷۴ - در مدح صاحب ناصرالدین نصرة الاسلام ابو المناقب
---
# قصیدهٔ شمارهٔ ۷۴ - در مدح صاحب ناصرالدین نصرة الاسلام ابو المناقب

<div class="b" id="bn1"><div class="m1"><p>چو از دوران این نیلی دوایر</p></div>
<div class="m2"><p>زمانه داد ترکیب عناصر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمین شد چون سپهر از بس بدایع</p></div>
<div class="m2"><p>خزان شد چون بهار از بس نوادر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درخت مفلس از گنج طبیعت</p></div>
<div class="m2"><p>توانگر شد به انواع جواهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان شد باغ کز نظارهٔ او</p></div>
<div class="m2"><p>همی خیره بماند چشم ناظر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنور دانهٔ نار کفیده</p></div>
<div class="m2"><p>ببیند در دل آبی همی سر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو گویی برگ سیب و سیب الوان</p></div>
<div class="m2"><p>سپهرست و برو اجرام زاهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز شکل بربط و از دستهٔ او</p></div>
<div class="m2"><p>اگر فکرت کند مرد مفکر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همان هیات که از امرود و شاخش</p></div>
<div class="m2"><p>به خاطر اندرست آید به خاطر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگرنه برج ثور و شاخ انگور</p></div>
<div class="m2"><p>دو موجودند از یک مایه صادر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرا پس خوشهٔ انگور و پروین</p></div>
<div class="m2"><p>یکی صورت پذیرفت از مصور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وگرنه شاخها را جام نرگس</p></div>
<div class="m2"><p>به باغ اندر شرابی داد مسکر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چرا چونان که مستان شبانه</p></div>
<div class="m2"><p>توان و سرنگونسارند و فاتر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چمن را شاخ چندان زر فرستاد</p></div>
<div class="m2"><p>ز دارالضرب وی پنهان و ظاهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که هر ساعت چمن گوید که هر شاخ</p></div>
<div class="m2"><p>کف خواجه است با این بخشش و بر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ظهیر دین یزدان بوالمناقب</p></div>
<div class="m2"><p>نصیر ملت اسلام ناصر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کمال فضل و او با فضل کامل</p></div>
<div class="m2"><p>وفور علم و او با علم وافر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به تقدیم قضا رایش مقدم</p></div>
<div class="m2"><p>به تقدیر قدر حکمش مدبر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بود در پیش حلمش خاک عاجل</p></div>
<div class="m2"><p>بود در جنب حکمش برق صابر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به کلکش در فتوت را خزاین</p></div>
<div class="m2"><p>به طبعش در مروت را ذخایر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>امور شرع را عدلش مربی</p></div>
<div class="m2"><p>رموز غیب را حلمش مفسر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ندارد هیچ حاصل عقل کلی</p></div>
<div class="m2"><p>که نه در ذهن او آن هست حاضر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خطابش منهی آمال عاقب</p></div>
<div class="m2"><p>عتابش داعی آجال قاهر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز سهمش گوئیا اقرار حشوست</p></div>
<div class="m2"><p>به دیوانش اندرون انکار منکر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دهد پیشش گواهی در مظالم</p></div>
<div class="m2"><p>رگ و پی بر فجور مرد فاجر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قضا تاویل سهم او ندارد</p></div>
<div class="m2"><p>حریف خویش بشناسد مقامر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر از گردون تاسع کرد مفروض</p></div>
<div class="m2"><p>ز قدر او خرد گردون عاشر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قدر تقدیر قدر او نداند</p></div>
<div class="m2"><p>مقدر کی بود هرگز مقدر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ایا آرام خاکت در نواهی</p></div>
<div class="m2"><p>و یا تعجیل بادت در اوامر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بیان از وصف انعام تو عاجز</p></div>
<div class="m2"><p>زبان از شکر اکرام تو قاصر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ره درگاه تو گویی مجره است</p></div>
<div class="m2"><p>ز سیم سایلت وز زر زایر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر از جود تو گیتی دانه سازد</p></div>
<div class="m2"><p>به دام او درآید نسر طایر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ور از لطف تو تن مایه پذیرد</p></div>
<div class="m2"><p>چو روحش درنیابد حس باصر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نیارد چون تو گردون مدور</p></div>
<div class="m2"><p>نزاید چون تو ایام مسافر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به فرمان بردن اندر شرع مامور</p></div>
<div class="m2"><p>به فرمان دادن اندر حکم آمر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عمارت یافت از عدلت زمانه</p></div>
<div class="m2"><p>زمانه هست معمور و تو عامر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>فرو خورد آب عدلت آتش ظلم</p></div>
<div class="m2"><p>چنان چون مار موسی سحر ساحر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر مسعود ناصر تربیت داد</p></div>
<div class="m2"><p>عیاضی را به خلعتهای فاخر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرا آن داد جاهت کان ندادست</p></div>
<div class="m2"><p>عیاضی را دو صد مسعود ناصر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>وگر چند اندرین مدت ندیدست</p></div>
<div class="m2"><p>کسم در خدمتت الا بنادر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به یاد آن حقوق مکرماتت</p></div>
<div class="m2"><p>زبانها دارم از خلق تو شاکر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>وگر عمرم بر آن مقصور دارم</p></div>
<div class="m2"><p>به آخر هم نمیرم جز مقصر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به شعر آنرا مقابل کی توان کرد</p></div>
<div class="m2"><p>ولیکن شعر نیکوتر ز شاعر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو خاموشی بود کفران نعمت</p></div>
<div class="m2"><p>در این معنی چه خاموش و چه کافر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همیشه تا بود ارکان مؤثر</p></div>
<div class="m2"><p>همیشه تا بودگردون مؤثر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو ارکانت مبادا هیچ نقصان</p></div>
<div class="m2"><p>چو گردونت مبادا هیچ آخر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز چرخت باد عمری در تزاید</p></div>
<div class="m2"><p>ز بختت باد عزمی بر تواتر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بر احکام قضا حکم تو قاضی</p></div>
<div class="m2"><p>بر اسرار قدر علم تو قادر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سعادت همنشینت در مجالس</p></div>
<div class="m2"><p>هدایت هم حریفت بر منابر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ترا در شرع امری باد جاری</p></div>
<div class="m2"><p>مرا در شعر طبعی باد ماهر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو عیدی بگذرد تا عید دیگر</p></div>
<div class="m2"><p>به عید دیگرت هر شب مبشر</p></div></div>