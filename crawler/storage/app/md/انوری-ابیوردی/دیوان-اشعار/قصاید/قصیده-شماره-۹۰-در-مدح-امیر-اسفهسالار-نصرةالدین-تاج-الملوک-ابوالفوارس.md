---
title: >-
    قصیدهٔ شمارهٔ ۹۰ - در مدح امیر اسفهسالار نصرةالدین تاج‌الملوک ابوالفوارس
---
# قصیدهٔ شمارهٔ ۹۰ - در مدح امیر اسفهسالار نصرةالدین تاج‌الملوک ابوالفوارس

<div class="b" id="bn1"><div class="m1"><p>ای در نبرد حیدر کرار روزگار</p></div>
<div class="m2"><p>وی راست کرده خنجر تو کار روزگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معمور کرده از پی امن جهانیان</p></div>
<div class="m2"><p>معمار حزم تو در و دیوار روزگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دهر جز خرابی مستی نیافتند</p></div>
<div class="m2"><p>زان دم که هست حزم تو معمار روزگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>واضح به پیش رای تو اشکال حادثات</p></div>
<div class="m2"><p>واسان به نزد عزم تو دشوار روزگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رای تو از ورای ورقهای آسمان</p></div>
<div class="m2"><p>تکرار کرده دفتر اسرار روزگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان سوی آسمان به تصرف برون شدی</p></div>
<div class="m2"><p>گر قدر و قدرت تو شدی یار روزگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قدرت برون بماند چون بنای کن فکان</p></div>
<div class="m2"><p>بنهاد اساس دایره کردار روزگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور در درون دائره ماندی ز رفعتش</p></div>
<div class="m2"><p>درهم نیامدی خط پرگار روزگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بعد از قبای قدر تو ترکیب کرده‌اند</p></div>
<div class="m2"><p>این هفت و هشت پاره کله‌وار روزگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جزوی ز ملک جاه تو اقطاع اختران</p></div>
<div class="m2"><p>نوعی ز رسم جود تو آثار روزگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با خرج جود تو نه همانا وفا کند</p></div>
<div class="m2"><p>این مختصر خزانه و انبار روزگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیش تو بر سبیل خراج آورد قضا</p></div>
<div class="m2"><p>هرچ آورد ز اندک و بسیار روزگار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زانها نه‌ای که همت تو چون دگر ملوک</p></div>
<div class="m2"><p>تن دردهد به بخشش و ادرار روزگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای وقف کرده دولت موروث و مکتسب</p></div>
<div class="m2"><p>بر تو قضا و بستده اقرار روزگار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تزویر این و آن نه همانا به دل کند</p></div>
<div class="m2"><p>اقرار روزگار به انکار روزگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زیرا که روزگار ترا نیک بنده‌ایست</p></div>
<div class="m2"><p>احسنت ای خدای نگهدار روزگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا بندگیت عام شد آزاد کس نماند</p></div>
<div class="m2"><p>الا که سرو و سوسن از احرار روزگار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جودت چو در ضمان بهای وجود شد</p></div>
<div class="m2"><p>بگشاد کاروان قدر بار روزگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طبعت به چارسوی عناصر چو برگذشت</p></div>
<div class="m2"><p>آویخت بخل را عدم از دار روزگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای در جوال عشوه علی‌وار ناشده</p></div>
<div class="m2"><p>از حرص دانگانه به گفتار روزگار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تیغ جهادت از پی تمهید اقتداش</p></div>
<div class="m2"><p>ایمن چو ذوالفقار ز زنگار روزگار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>روزی که زلف پرچم از آشوب معرکه</p></div>
<div class="m2"><p>پنهان کند طراوت رخسار روزگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باشد ز بیم شیر علم شیر بیشه را</p></div>
<div class="m2"><p>دل قطره قطره گشته در اقطار روزگار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در کر و فر ز غایت تعجیل گشته چاک</p></div>
<div class="m2"><p>ز انگشت پای پاچهٔ شلوار روزگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>واندر گریزگاه هزیمت به پای در</p></div>
<div class="m2"><p>از بیم سرکشان شده دستار روزگار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو چون نمک به آب فرو برده از ملوک</p></div>
<div class="m2"><p>یک دشت خصم را به نمکسار روزگار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ترجیح داده کفهٔ آجال خصم را</p></div>
<div class="m2"><p>از دانگ سنگ چرخ تو معیار روزگار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زور تو در کشاکش اگر بر فلک خورد</p></div>
<div class="m2"><p>زاسیب او گسسته شود تار روزگار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بیرون کند چو تیغ تو گلگون شود به خون</p></div>
<div class="m2"><p>دست قدر ز پای ظفر خار روزگار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون باد حملهٔ تو به دشمن خبر برد</p></div>
<div class="m2"><p>کای جان و تن سپرده به زنهار روزگار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>القاب و کنیت تو در اینست زانکه نیست</p></div>
<div class="m2"><p>القاب و کنیتت شده تذکار روزگار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در نظم این قصیده ادب را نگفته‌ام</p></div>
<div class="m2"><p>القابت ای خلاصهٔ اخیار روزگار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هرچند نام و کنیت تو نیست اندرو</p></div>
<div class="m2"><p>ای بد نکرده حیدر کرار روزگار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دانی که جز به حال تو لایق نباشد این</p></div>
<div class="m2"><p>کای در نبرد حیدر کرار روزگار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کرتر بود ز جذر اصم گر بپرسمش</p></div>
<div class="m2"><p>کامثال این قصیده ز اشعار روزگار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در مدحتت که زیبد گوید به صد زبان</p></div>
<div class="m2"><p>تاج‌الملوک صفدر و صف‌دار روزگار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کس را به روزگار دگر ی اد کی بود</p></div>
<div class="m2"><p>وز گرم و سرد شادی و تیمار روزگار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا زاختلاف بیع و شرای فساد و کون</p></div>
<div class="m2"><p>باشد همیشه رونق بازار روزگار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بادا همیشه رونق بازار ملک تو</p></div>
<div class="m2"><p>تا کاین است و فاسد از ادوار روزگار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دست دوام دامن جاه تو دوخته</p></div>
<div class="m2"><p>بر دامن سپهر به مسمار روزگار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در عرصه‌گاه موکب میمون کبریات</p></div>
<div class="m2"><p>کمتر جنیبت ابلق رهوار روزگار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در زینهار عدل تو ایام و بس ترا</p></div>
<div class="m2"><p>حفظ خدای داده به زنهار روزگار</p></div></div>