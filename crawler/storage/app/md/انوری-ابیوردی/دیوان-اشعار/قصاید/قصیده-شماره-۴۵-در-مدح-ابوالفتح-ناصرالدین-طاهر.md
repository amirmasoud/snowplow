---
title: >-
    قصیدهٔ شمارهٔ ۴۵ - در مدح ابوالفتح ناصرالدین طاهر
---
# قصیدهٔ شمارهٔ ۴۵ - در مدح ابوالفتح ناصرالدین طاهر

<div class="b" id="bn1"><div class="m1"><p>آفرین بر حضرت دستور و بر دستور باد</p></div>
<div class="m2"><p>جاودان چشم بد از جاه و جلالش دور باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک را از رایت اقبال و رای روشنش</p></div>
<div class="m2"><p>تا که نور و سایه باشد سایه باد و نور باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رایت و رایش که در نظم ممالک آیتی است</p></div>
<div class="m2"><p>تا نزول آیت نصرت بود منصور باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من نگویم کز پی تفویض ملک روم و چین</p></div>
<div class="m2"><p>بر درش دایم رسول قیصر و فغفور باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویم از بهر نظام ملک سلطان سپهر</p></div>
<div class="m2"><p>در رکابش ز اختران پیوسته صد مذکور باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه همچون دانهٔ انگور با او شد دودل</p></div>
<div class="m2"><p>ریخته خونش چو خون خوشهٔ انگور باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیغ زنگ از آب گیرد ملک نقصان از غرور</p></div>
<div class="m2"><p>زین سپس رایش به ملک و جاه نامغرور باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از برای پاسبان قصر او یعنی زحل</p></div>
<div class="m2"><p>در نه اقلیم فلک تا روز هر شب سور باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مشتری را از شرف دولت‌سرای طالعش</p></div>
<div class="m2"><p>چون کلیم‌الله را خلوت سرای طور باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در کنار بارگاهش در صف حجاب بار</p></div>
<div class="m2"><p>والی عقرب کمر بربسته چون زنبور باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آفتاب ار کلبهٔ بدخواه او روشن کند</p></div>
<div class="m2"><p>روز دوران از کسوف کل شب دیجور باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زهره گر در مجلس بزمش نباشد بربطی</p></div>
<div class="m2"><p>در میان اختران چون زاد فی الطنبور باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر وزیر آفتاب از خدمتش گردن کشد</p></div>
<div class="m2"><p>از جمالی کافتابش می دهد مهجور باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>منشی ملک فلک در هرچه منشوری نوشت</p></div>
<div class="m2"><p>کلکش اندر عهدهٔ توقیع آن منشور باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در زوایای عدم گر بر خلافش واردیست</p></div>
<div class="m2"><p>همچنان در طی ستر نیستی مستور باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرچه در الواح گردونست از اسرار غیب</p></div>
<div class="m2"><p>در ورقهای وقوفش بر ولا مسطور باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آسمان از نیک و بد هر آیتی کامل کند</p></div>
<div class="m2"><p>شان او بر اقتضای رای او مقصور باد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای به تدبیر آصف ملک سلیمان دوم</p></div>
<div class="m2"><p>جبر امرت را چو انس و جان فلک مجبور باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ملک معمورست تا معمار او تدبیر تست</p></div>
<div class="m2"><p>تا جهان باقیست این معمار و آن معمور باد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در عمارتهای عالم کز تو خواهد شد تمام</p></div>
<div class="m2"><p>هرکجا رایت مهندس آسمان مزدور باد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نعمت جاه تو عالم را مهنا نعمتیست</p></div>
<div class="m2"><p>حظ برخورداری عالم ازو موفور باد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فتنه را بخت بداندیشت نکو همخوابه‌ایست</p></div>
<div class="m2"><p>هر دو را امکان بیداری به نفخ صور باد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هرکجا گنجی نهد در کان و دریا آفتاب</p></div>
<div class="m2"><p>مه که بیت‌المال او دارد ترا گنجور باد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر به جز کام تو زاید شب که آبستن بود</p></div>
<div class="m2"><p>شب عزب ورنه سقنقور قدر کافور باد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرکرا در سر نه از جام وفاقت مستی است</p></div>
<div class="m2"><p>جانش از درد اجل تا جاودان مخمور باد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خواستم گفتن جهان مامور امرت باد و باز</p></div>
<div class="m2"><p>گفتم او مامور و آنگه گویمش مامور باد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وهم با وصف تو چون خورشید و خفاشند راست</p></div>
<div class="m2"><p>در چنین حیرت گرش سهوی فتد معذور باد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خصم بد عهدت که کهف ملک را هشتم کسست</p></div>
<div class="m2"><p>گر کند خدمت همش جل باد و هم ساجور باد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ورنه دایم چار چشمش در غم یک استخوان</p></div>
<div class="m2"><p>بر در قصاب جان اندر سرش ساطور باد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شاعران از دشمن ممدوح چون ذکری کنند</p></div>
<div class="m2"><p>رسم را گویند کز قهر اجل مقهور باد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بنده می‌گوید مبادش مرگ بل عمر دراز</p></div>
<div class="m2"><p>همچنان مقهور این دارالغرور زور باد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>لیکن از جاه تو هر دم زیر بار غصه‌ای</p></div>
<div class="m2"><p>کاندران راحت شمارد مرگ را رنجور باد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>باغ دولت را که آب آن لعاب کلک تست</p></div>
<div class="m2"><p>با نمای عهد نیسان حاصل باحور باد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وین چهار آزاد سروت را که تعیین شرط نیست</p></div>
<div class="m2"><p>از جمال هریکی هردم دلت مسرور باد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تاکه بر هر هفت کشور سایه‌شان شامل شود</p></div>
<div class="m2"><p>نشو در بلخ و هری و مرو و نیشابور باد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا که «المقدورکائن» شرط کار عالمست</p></div>
<div class="m2"><p>کلک و رایت کار ساز کائن و مقدور باد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پیش صدر و مسند عالیت هر عیدی چنین</p></div>
<div class="m2"><p>از فحول شاعران صد شاعر مشهور باد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>وانگه از پیرایهٔ عدل تو تا عید دگر</p></div>
<div class="m2"><p>گردن و گوش جهان پر لؤلؤ منثور باد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بارگاهت کعبه، مردم حاج و درگاهت حرم</p></div>
<div class="m2"><p>مجلست فردوس و کوثر جام و ساقی حور باد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>احتیاجی نیست جاهت را به سعی روزگار</p></div>
<div class="m2"><p>ور کند نوعی بود از بندگی مشکور باد</p></div></div>