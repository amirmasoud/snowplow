---
title: >-
    قصیدهٔ شمارهٔ ۲۱ - در صفت خزان و مدح ناصرالدین ابوالفتح طاهر
---
# قصیدهٔ شمارهٔ ۲۱ - در صفت خزان و مدح ناصرالدین ابوالفتح طاهر

<div class="b" id="bn1"><div class="m1"><p>روز می خوردن و شادی و نشاط و طرب است</p></div>
<div class="m2"><p>ناف هفته است اگر غرهٔ ماه رجب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برگ‌ریزان به همه حال فرو باید ریخت</p></div>
<div class="m2"><p>به قدح آنچه از او برگ و نوای طرب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مادر باغ سترون شد و زادن بگذاشت</p></div>
<div class="m2"><p>چکند نامیه عنین و طبیعت عزب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دختر رز که تو بر طارم تاکش دیدی</p></div>
<div class="m2"><p>مدنی شد که بر آونگ سرش در کنب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موی بر خیک دمیده ز حسد تیغ زنست</p></div>
<div class="m2"><p>تا به خلوت لب خم بر لب بنت‌العنب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرنه صراف خزان کیسه‌فشان رفت ز باغ</p></div>
<div class="m2"><p>چون چمن‌ها ز ذهابش همه یکسر ذهب است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این عجب نیست بسی کز اثر لاله و خوید</p></div>
<div class="m2"><p>گفتی آهوبره میناسم و بیجاده لب است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یارب الماس لبش باز که کرد و شبه سم</p></div>
<div class="m2"><p>بینی این گنبد فیروه که چون بلعجب است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این همان سکنه و صحراست که گفتی ز سموم</p></div>
<div class="m2"><p>تربت آن خزف و رستنی این حطب است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خیز از سعی دخان بین و ز تاثیر بخار</p></div>
<div class="m2"><p>تا در این هر دو کنون چند رسوم عجب است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روزن این همه پر ذرهٔ زرین زره است</p></div>
<div class="m2"><p>عرصهٔ آن همه پر پشهٔ سیمین سلب است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لمعه در سکنهٔ کانون شده بر خود پیچان</p></div>
<div class="m2"><p>افعی کاه‌ربا پیکر مرجان عصب است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دود حلقه شده بر سطح هوا خم در خم</p></div>
<div class="m2"><p>سطرهاییست که مکتوب بنان لهب است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شعلهٔ آتش از این روی که گفتم گویی</p></div>
<div class="m2"><p>در مقادیر کتابت قلم منتجب است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر زمان لرزه بر آب شمر افتد مگرش</p></div>
<div class="m2"><p>در مزاج از اثر هیبت دستور تب است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صاحب عادل ابوالفتح که در جنبش فتح</p></div>
<div class="m2"><p>جنبش رایت عالیش قویتر سبب است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>طاهر آن ذات مطهر که سپهرش گوید</p></div>
<div class="m2"><p>صدر طاهر گهر و صاحب طاهر نسب است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آنکه در شش جهت از فضلهٔ خوان کرمش</p></div>
<div class="m2"><p>هیچ دل نیست که از آز در آن دل کرب است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وانکه در نه فلک ار برق کمالی بجهد</p></div>
<div class="m2"><p>همه از بارقهٔ خاطر او مکتسب است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ساحت بارگهش مولد ملک عجمست</p></div>
<div class="m2"><p>عدل فریادرسش داور دین عرب است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ضبط ملک فلک اندیشه همی کرد شبی</p></div>
<div class="m2"><p>زانشب اوراد مقیمان فلک قد وجب است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صاحبانه ملکا، هم نه چرا زانکه ترا</p></div>
<div class="m2"><p>مدحت از حرف برونست چه جای لقب است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نام سلطان نه بدانست که تا خوانندش</p></div>
<div class="m2"><p>بل برای شرف سکه و فخر خطب است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گوشهٔ بالش تو چیست کله گوشهٔ ملک</p></div>
<div class="m2"><p>وندرو هم ز نسب رفعت و هم از حسب است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مسندت برتر از آنست که در صد یک از آن</p></div>
<div class="m2"><p>چرخ را گنج تمنا و مجال طلب است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>غرض از کون تو بودی که ز پروردن نخل</p></div>
<div class="m2"><p>گرچه از خار گذر نیست غرض هم رطب است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آسمان دگری زانکه به همت جنبی</p></div>
<div class="m2"><p>جنبش چرخ نه از شهوت و نه از غضب است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مه به نعل سم اسب تو تشبه می‌کرد</p></div>
<div class="m2"><p>خاک فریاد برآورد که ترک ادب است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گرد جیش تو بشد بر همه اعضاش نشست</p></div>
<div class="m2"><p>تاکه اجرب شد وانک همه سالش جرب است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چرخ چون گوز شکستست از آن روی که ماه</p></div>
<div class="m2"><p>چهره چون چهرهٔ بادام از آن پر ثقب است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خصم اگر لاف تقابل زند از روی حسد</p></div>
<div class="m2"><p>حق شناسد که که بوالقاسم و که بولهب است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ور مقابل نهمش نیز به یک وجه رواست</p></div>
<div class="m2"><p>تو چو خورشید به راس او چو قمر در ذنب است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>رتبت شرکت قدرش نشود لازم ازآنک</p></div>
<div class="m2"><p>دار او از خشب و تخت تو هم از خشب است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آخر از رابطهٔ قهر کجا داند شد</p></div>
<div class="m2"><p>سرعت سیر نفاذت نه به پای هرب است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ور کشد سد سکندر به مثل گرد بقاش</p></div>
<div class="m2"><p>این مهندس که در افعال ورای تعب است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عقل داند که چو مهتاب زند دست به تیغ</p></div>
<div class="m2"><p>رد تیغش نه به اندازهٔ درع قصب است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همه در ششدر عجزند و ترا داو به هفت</p></div>
<div class="m2"><p>ضربه بستان و بزن زانکه تممی ندب است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تاکه تبدیل بد و نیک به سال و به مهست</p></div>
<div class="m2"><p>تاکه ترتیب مه و سال به روزست و شب است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بی‌تو ترتیب شب و روز و مه و سال مباد</p></div>
<div class="m2"><p>که ز سر جملهٔ آن مدت تو منتخب است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به می و مطرب خوش‌نغمه شعف بیش نمای</p></div>
<div class="m2"><p>که ز انصاف تو اقطار جهان بی شغب است</p></div></div>