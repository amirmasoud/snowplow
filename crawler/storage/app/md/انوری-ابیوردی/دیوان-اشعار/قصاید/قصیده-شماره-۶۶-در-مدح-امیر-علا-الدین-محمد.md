---
title: >-
    قصیدهٔ شمارهٔ ۶۶ - در مدح امیر علاء الدین محمد
---
# قصیدهٔ شمارهٔ ۶۶ - در مدح امیر علاء الدین محمد

<div class="b" id="bn1"><div class="m1"><p>هرکرا در دور گردون ذکر مقصد می‌رود</p></div>
<div class="m2"><p>یا سخن در سر این صرح ممرد می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا حدیث آن بهشتی چهره کز بدو وجود</p></div>
<div class="m2"><p>همچو خاتونان درین فیروزه مرقد می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا در آن حورا نسب کودک شروعی می‌کند</p></div>
<div class="m2"><p>کز تصنع گه مخطط گاه امرد می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا همی گوید چرا در کل انسان بر دوام</p></div>
<div class="m2"><p>از تحرک میل و تحریک مجدد می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر زبان دور گردون در جواب هرکه هست</p></div>
<div class="m2"><p>ذکر دوران علاء الدین محمد می‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه پیش سایهٔ او سایهٔ خورشید را</p></div>
<div class="m2"><p>در نشستن گفت‌وگوی صدر و مسند می‌رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وانکه جز در موکب رایش نراند آفتاب</p></div>
<div class="m2"><p>رایتش بر چرخ منصور و مید می‌رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه از تاثیر نه گردون به دست روزگار</p></div>
<div class="m2"><p>ساکنان خاک را انعام بی‌حد می‌رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچه رفتست از عطیتهای ایشان تاکنون</p></div>
<div class="m2"><p>حاطه‌الله زو به یک احسان مفرد می‌رود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عقل کل کو تا ببیند نفس خاکی گوهری</p></div>
<div class="m2"><p>کز دو عالم گوهرافشانان مجرد می‌رود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طبعش استقبال حاجتها بدان سرعت کند</p></div>
<div class="m2"><p>کاندر آن نسبت زمان گویی مقید می‌رود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست اورا در سخا تشبیه می‌کردم به ابر</p></div>
<div class="m2"><p>عقل گفت این اصل باری ناممهد می‌رود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیش دست او هنوز اندر دبیرستان جود</p></div>
<div class="m2"><p>بر زبان رعد او تکرار ابجد می‌رود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خاک پایش را ز غیرت آسمان بر سنگ زد</p></div>
<div class="m2"><p>تا به گاه چرخ موزون نامعدد می‌رود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت صراف قضا ای شیخ اگر ناقد منم</p></div>
<div class="m2"><p>در دیار ما تصرف فرق فرقد می‌رود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وصف می‌کردم سمندش را شبی با آسمان</p></div>
<div class="m2"><p>گفتم این رفتار بین کان آسمان قد می‌رود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت دی بر تیغ کوهی بود پویان گفتیی</p></div>
<div class="m2"><p>آفتابستی که سوی بعد ابعد می‌رود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ماه بشنید این سخن آسیب زد با منطقه</p></div>
<div class="m2"><p>گفت آیا تا حدیث نعل و مقود می‌رود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای جوان دولت خداوندی که سوی خدمتت</p></div>
<div class="m2"><p>دولت من سروقد یاسمین خد می‌رود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جانم از یک ماهه پیوند تو عیشی یافتست</p></div>
<div class="m2"><p>کز کمالش طعنه در عیش مخلد می‌رود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ختم شد بر گوهر تو همچو مردی مردمی</p></div>
<div class="m2"><p>در تو این دعوی به صد برهان مکد می‌رود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دور نبود کین زمان در مجلس حکم قضا</p></div>
<div class="m2"><p>بر زبان چرخ و اختر لفظ اشهد می‌رود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نعت تو کی گنجد اندر بیت چندی مختصر</p></div>
<div class="m2"><p>راستی باید سخن در صد مجلد می‌رود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چشم بد دور از تو خود دورست کز بس باس تو</p></div>
<div class="m2"><p>فتنه اکنون همچو یاجوج از پس سد می‌رود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دانی از بهر تو با چشم بد گردون چه رفت</p></div>
<div class="m2"><p>آنچه آن با چشم افعی از زمرد می‌رود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا عروس روزگار اندر شبستان سپهر</p></div>
<div class="m2"><p>در حریر ابیض و در شعر اسود می‌رود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وقف بادا بر جمال و جاه و عمرت روزگار</p></div>
<div class="m2"><p>زانکه در اوقاف احکام مبد می‌رود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حاجب بارت سپه‌داری که در میدان چرخ</p></div>
<div class="m2"><p>حزم را پیوسته با تیغ مهند می‌رود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ساقی بزمت سمن ساقی که بر قصر سپهر</p></div>
<div class="m2"><p>لهو را همواره با صرف مورد می‌رود</p></div></div>