---
title: >-
    قصیدهٔ شمارهٔ ۱۷۶ - مدح سلطان سعید سنجربن ملکشاه
---
# قصیدهٔ شمارهٔ ۱۷۶ - مدح سلطان سعید سنجربن ملکشاه

<div class="b" id="bn1"><div class="m1"><p>ای ز یزدان تا ابد ملک سلیمان یافته</p></div>
<div class="m2"><p>هرچه جسته جز نظیر از فضل یزدان یافته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ز رشک رونق بزمت سلیمان را خدای</p></div>
<div class="m2"><p>از تضرع کردن هب‌لی پشیمان یافته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منبر از یادت جناب خطبه عالی داشته</p></div>
<div class="m2"><p>دولت از نامت دهان سکه خندان یافته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچه دعوی کرده از رتبت امیرالمؤمنین</p></div>
<div class="m2"><p>روزگار از پایهٔ تخت تو برهان یافته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اختران را شوکتت بر سمت طاعت رانده</p></div>
<div class="m2"><p>آسمان را همتت در تحت فرمان یافته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بارها از شرم رایت آسمان خورشید را</p></div>
<div class="m2"><p>زیر سیلاب عرق در موج طوفان یافته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش چوگان مرادت گوی گردون را قضا</p></div>
<div class="m2"><p>بی‌تصرف سالها چون گوی میدان یافته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرده موزن حل و عقد آفرینش را قدر</p></div>
<div class="m2"><p>تا ز عدل شاملت معیار و میزان یافته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منهیان ربع مسکون زاب روی عدل تو</p></div>
<div class="m2"><p>فتنه را پنجاه ساله نان در انبان یافته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در میان دولتی با حلق ملکی گشته سخت</p></div>
<div class="m2"><p>هر کمندی کز کف عزم تو دوران یافته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بارها آحاد فراشانت شیر چرخ را</p></div>
<div class="m2"><p>در پناه شیر شادروان ایوان یافته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حادثه در نرد درد و فتنه در شطرنج رنج</p></div>
<div class="m2"><p>بدسگالت را حریف آب دندان یافته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زلف‌وارش سر ز تن ببریده جلاد اجل</p></div>
<div class="m2"><p>بر دل هرک از خلافت خال عصیان یافته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از مصافت قایل تکبیر حیران مانده باز</p></div>
<div class="m2"><p>وز نفاذت نامهٔ تقدیر عنوان یافته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم ز بیم لمعهٔ تیغ تو جاسوس ظفر</p></div>
<div class="m2"><p>مرگ را در چشمهٔ تیغ تو پنهان یافته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جرم خاک از بس و حل کز خون خصمت ساخته</p></div>
<div class="m2"><p>ابلق ایام را افتان و خیزان یافته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زان اثرها کز سنانت یاد دارد روزگار</p></div>
<div class="m2"><p>یک نشان معجز از موسی عمران یافته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ناقهٔ صالح، عصای موسی و روح پدر</p></div>
<div class="m2"><p>هرسه را در بطن مادر دیده بی‌جان یافته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سالها بر خوان رزم از میزبان تیغ تو</p></div>
<div class="m2"><p>وحش و طیر و دام ودد را چرخ مهمان یافته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هرکجا طی کرده یک پی نعل اسبت خاک رزم</p></div>
<div class="m2"><p>اژدهای رایت از باد ظفر جان یافته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آفتاب از سمت رزمت چون به مغرب آمده</p></div>
<div class="m2"><p>چهره چون قوس قزح پر اشک الوان یافته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وز گشادت روز دیگر چون به خود پرداخته</p></div>
<div class="m2"><p>دیده چون رخسار مه پر زخم پیکان یافته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وز بخار خون خصمانت هوای معرکه</p></div>
<div class="m2"><p>بی‌مزاج انجم استعداد باران یافته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پس به مدتها ز خاک رزمگاهت روزگار</p></div>
<div class="m2"><p>رستنی را صورت و ترکیب مرجان یافته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خسروا من بنده در اثناء این خدمت که هست</p></div>
<div class="m2"><p>گوش و هوش از گوهرش سرمایهٔ کان یافته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قصد آن کردم که ذوالقرنین ثانی گویمت</p></div>
<div class="m2"><p>هر غلامت از تو در هر مکرمت آن یافته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شاد باش ای مصطفی سیرت خداوند این منم</p></div>
<div class="m2"><p>کز قبول حضرتت اقبال حسان یافته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا توان گفتن همی با خسرو سیارگان</p></div>
<div class="m2"><p>کای زکیوان پاسبان وز ماه دربان یافته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بادت اندر خسروی سیاره از فوج حشم</p></div>
<div class="m2"><p>ای مه منجوق چترت قدر کیوان یافته</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هرچه پنهان قضا حزم تو پیدا داشته</p></div>
<div class="m2"><p>هرچه دشوار قدر عزم تو آسان یافته</p></div></div>