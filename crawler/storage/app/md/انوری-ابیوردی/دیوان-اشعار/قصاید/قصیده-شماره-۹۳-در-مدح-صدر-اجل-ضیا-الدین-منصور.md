---
title: >-
    قصیدهٔ شمارهٔ ۹۳ - در مدح صدر اجل ضیاء الدین منصور
---
# قصیدهٔ شمارهٔ ۹۳ - در مدح صدر اجل ضیاء الدین منصور

<div class="b" id="bn1"><div class="m1"><p>رییس مشرق و مغرب ضیاء الدین منصور</p></div>
<div class="m2"><p>که هست مشرق و مغرب ز عدل او معمور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به اصطناع بیاراست دستگاه وجود</p></div>
<div class="m2"><p>به استناد بیفزود پایگاه صدور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپهر قدری کاندر ازای قدرت او</p></div>
<div class="m2"><p>شکوه گردون دونست و روز انجم زور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرفته مکنت او عرصهٔ صباح و مسا</p></div>
<div class="m2"><p>ببسته طاعت او گردن صبا و دبور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوایب فلکی در خلاف او مضمر</p></div>
<div class="m2"><p>سعادت ابدی بر هوای او مقصور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قضا نسازد کاری ز عزم او پنهان</p></div>
<div class="m2"><p>قدر ندارد رازی ز حزم او مستور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضالهٔ سخطش نیش گشته بر کژدم</p></div>
<div class="m2"><p>حلاوت کرمش نوش گشته بر زنبور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>توان گریخت اگر حاجت اوفتد مثلا</p></div>
<div class="m2"><p>به پشتی حرم حرمتش ز سایه و نور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهی موافق احمام تو زمین و زمان</p></div>
<div class="m2"><p>خهی متابع فرمان تو سنین و شهور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مجاهدان نفاذ تو همچو باد عجول</p></div>
<div class="m2"><p>مجاهزان وقار تو همچو خاک صبور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به جود اگرچه کفت همچو ابر معروفست</p></div>
<div class="m2"><p>به لاف هرزه چو رعدت زبان نشد مشهور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کف تو قدرت آن دارد ارچه ممکن نیست</p></div>
<div class="m2"><p>که خلق را برهاند ز روزی مقدور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه چشمهاست که آن نیست از مکارم تو</p></div>
<div class="m2"><p>زهی کریم به واجب که چشم بد ز تو دور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به تیغ قهر تو آنرا که سخته کرد قضا</p></div>
<div class="m2"><p>چو وحش و طیر نیابد به نفخ صور نشور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به آب لطف تو آنرا که تشنه کرد امید</p></div>
<div class="m2"><p>سپهر برشده ننمایدش سراب غرور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بزرگوارا من خادم و توابع من</p></div>
<div class="m2"><p>همیشه جفت نفیریم از جهان نفور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا نه در خور ایام همتی است بلند</p></div>
<div class="m2"><p>همی به پرده دریدن نداردم معذور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا نه در خور احوال عادتی است جمیل</p></div>
<div class="m2"><p>همی به راز گشادن نباشدم دستور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زمانه هرچه بزاید به عرصه نتوان برد</p></div>
<div class="m2"><p>که مادریست فلک بر بنات خویش غیور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرا فلک عملی داد در ولایت غم</p></div>
<div class="m2"><p>که دخل آن نپذیرد به هیچ خرج قصور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به خیره عزل چه جویم که می‌رسد شب و روز</p></div>
<div class="m2"><p>به دست حادثه منشور در دم منشور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من از فلک به تو نالم که از دشمن و دوست</p></div>
<div class="m2"><p>چو از فلک به مصیبت همی رسند و به سور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همیشه تا که کند نور آفتاب فلک</p></div>
<div class="m2"><p>زمانه تیره و روشن به غیب و به حضور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شبت چو روز جهان باد و روز دشمن تو</p></div>
<div class="m2"><p>ز گرد حادثه تاریک چون شب دیجور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حساب عمر حسود ترا اگر به مثل</p></div>
<div class="m2"><p>زمانه ضرب کند باد همچو ضرب کسور</p></div></div>