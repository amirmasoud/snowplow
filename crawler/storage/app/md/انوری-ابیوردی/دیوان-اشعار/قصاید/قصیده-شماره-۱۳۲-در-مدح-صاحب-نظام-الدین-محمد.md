---
title: >-
    قصیدهٔ شمارهٔ ۱۳۲ - در مدح صاحب نظام‌الدین محمد
---
# قصیدهٔ شمارهٔ ۱۳۲ - در مدح صاحب نظام‌الدین محمد

<div class="b" id="bn1"><div class="m1"><p>ای گرفته عالم از عدلت نظام</p></div>
<div class="m2"><p>ای نظام ابن النظام ابن النظام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک اقبال تو ملک لایزال</p></div>
<div class="m2"><p>بخت بیدار تو حی لاینام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی تقدیر از شکوهت در حجاب</p></div>
<div class="m2"><p>تیغ مریخ از نهیبت در نیام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملک را بی‌کلک تو بازار کند</p></div>
<div class="m2"><p>عقل را بی‌رای تو اندیشه خام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشتگان خنجر قهر ترا</p></div>
<div class="m2"><p>حشر ناممکن بود روز قیام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرخ برتابد زمام روزگار</p></div>
<div class="m2"><p>هر کجا عزم تو برتابد زمام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رایض اقبال تو کردست و بس</p></div>
<div class="m2"><p>توسن ایام را یکباره رام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لاجرم در زیر ران رای تو</p></div>
<div class="m2"><p>ابلقش اکنون همی خاید لگام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر ترا یزدان و سلطان برکشید</p></div>
<div class="m2"><p>از جهانی تا جهانت شد غلام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حکم یزدان از غرض خالی بود</p></div>
<div class="m2"><p>تا کرا پوشد لباس احتشام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رای سلطان از غرض صافی بود</p></div>
<div class="m2"><p>تا کرا بیند سزای احترام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روز هیجاکز خروش کوس و اسب</p></div>
<div class="m2"><p>آب گردد مغز گردان در عظام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زهرها در بر بجوشد وز نهیب</p></div>
<div class="m2"><p>با عرق بیرون ترابد از مسام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نوک پیکانها چو پیکان قضا</p></div>
<div class="m2"><p>از اجل آرند خصمان را پیام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کوس همچون رعد و شمشیر چو برق</p></div>
<div class="m2"><p>تیر چون باران و گرد چون غمام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زرد گردد روی چرخ نیلگون</p></div>
<div class="m2"><p>سرخ گردد روی تیغ سبزفام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در بر شیر فلک شیر علم</p></div>
<div class="m2"><p>از پی خون عدو بگشاده کام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>معرکه مجلس بود ساقی اجل</p></div>
<div class="m2"><p>رمح ریحان خون شراب و خود جام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرکسی نصرت همی خواهد ز چرخ</p></div>
<div class="m2"><p>وز تو نصرت چرخ می‌خواهد به وام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رایتت بافتح چون همبر شود</p></div>
<div class="m2"><p>کس نداند این کدامست آن کدام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای جهان را حزم تو حصن حصین</p></div>
<div class="m2"><p>ملک ودین را رای تو پشت تمام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دی نه آن چندان تهاون کرده‌ام</p></div>
<div class="m2"><p>کان بدین خدمت پذیرد التیام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هستم از تشویر آن یک خارجی</p></div>
<div class="m2"><p>تا ابد با خویشتن در انتقام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هست خونم زان گنه بر تو حلال</p></div>
<div class="m2"><p>هست عمرم زین سبب بر من حرام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>با لبی بر هم بر خرد و بزرگ</p></div>
<div class="m2"><p>با سری در پیش پیش خاص و عام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حق همی داند کز آن دم تاکنون</p></div>
<div class="m2"><p>نیز برناورده‌ام یکدم به کام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آن گنه‌کارم که نتواند نمود</p></div>
<div class="m2"><p>آسمان در عذر جرم من قیام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر مرا اندر نیابد عفو تو</p></div>
<div class="m2"><p>ماندم با این ندامتها مدام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گرچه گشتستم ز خذلانی که رفت</p></div>
<div class="m2"><p>درخور صدگونه تادیب و ملام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون همی دانی که می‌کرد آن نه من</p></div>
<div class="m2"><p>عفو فرمای و کرم کن چون کرام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>من چه کردم آنچه آن آمد ز من</p></div>
<div class="m2"><p>تو چه کن آنچ از تو آید والسلام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا نباشد شام را آثار صبح</p></div>
<div class="m2"><p>باد دایم صبح بدخواهت چو شام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>قدرت از گردون گردان بردهقدر</p></div>
<div class="m2"><p>رایت از خورشید تابان برده نام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بخت را دست نکوخواهت به دست</p></div>
<div class="m2"><p>چرخ را پای بداندیشت به دام</p></div></div>