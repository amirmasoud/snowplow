---
title: >-
    قصیدهٔ شمارهٔ ۱۵۶ - در مدح ناصرالدین طاهربن مظفر
---
# قصیدهٔ شمارهٔ ۱۵۶ - در مدح ناصرالدین طاهربن مظفر

<div class="b" id="bn1"><div class="m1"><p>صاحب روزگار و صدر زمین</p></div>
<div class="m2"><p>نصرت کردگار ناصر دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاهربن المظفر آنکه ظفر</p></div>
<div class="m2"><p>هست در کلک و خاتمش تضمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه بی‌داغ طاعتش تقدیر</p></div>
<div class="m2"><p>ناید از آسمان به هیچ زمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وانکه بی‌مهر خازنش در خاک</p></div>
<div class="m2"><p>ننهد آفتاب هیچ دفین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدرش را بر سپهر تکیه زند</p></div>
<div class="m2"><p>قاب قوسین را دهد تزیین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور قلم در جهان کشد قهرش</p></div>
<div class="m2"><p>بارز کون را کند ترقین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رای او چون در انتظام شود</p></div>
<div class="m2"><p>دختر نعش را کند پروین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نهی او چون در اعتراض آید</p></div>
<div class="m2"><p>حدثان را قفا کند ز جبین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بشکند امتداد انعامش</p></div>
<div class="m2"><p>به موازین قسط بر شاهین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آسمان چون نگینش پیروزه‌ست</p></div>
<div class="m2"><p>دهر از آن آمدش به زیر نگین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر عنان فلک فرو گیرد</p></div>
<div class="m2"><p>به خط استوا در افتد چین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور زمام زمانه باز کشد</p></div>
<div class="m2"><p>شبش از روز بگسلد در حین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر کجا حلم او گذارد پی</p></div>
<div class="m2"><p>پی کند شعلهای آتش کین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر کجا امن او کشد باره</p></div>
<div class="m2"><p>نکشد بار قفلها زرفین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باس او دست چون دراز کند</p></div>
<div class="m2"><p>دست یابد تذرو بر شاهین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای ترا حکم بر زمین و زمان</p></div>
<div class="m2"><p>وی ترا امر بر شهور و سنین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از یسار تو دهر برده یسار</p></div>
<div class="m2"><p>به یمین تو چرخ خورده یمین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر در کبریای تو شب و روز</p></div>
<div class="m2"><p>اشهب روز و ادهم شب زین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نوک کلک تو رازدار قضا</p></div>
<div class="m2"><p>نوز ظن تو رهنمای یقین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>طوق و داغ ترا نماز برند</p></div>
<div class="m2"><p>فلک از گردن و جهان ز سرین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آسمان را زبان کلک تو داد</p></div>
<div class="m2"><p>در مقادیر کارها تلقین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آفتاب از بهشت بزم تو برد</p></div>
<div class="m2"><p>ساز صورتگران فروردین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قدرت تو به عینه قدرست</p></div>
<div class="m2"><p>خود خردشان نمی‌کند تعیین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نتواند که گوید آنک آن</p></div>
<div class="m2"><p>نتواند که گوید اینک این</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون تو صاحب‌قران نباشد ازانک</p></div>
<div class="m2"><p>همه چیزیت هست جز که قرین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لاف نسبت زند حسود ولیک</p></div>
<div class="m2"><p>شیر بالش نشد چو شیر عرین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به جسد کی شود ضعیف قوی</p></div>
<div class="m2"><p>به ورم کی شود نزار سمین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صاحبا بنده را در این یکسال</p></div>
<div class="m2"><p>در مدیح تو شعرهاست متین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>واندر ابیات آن معانی بکر</p></div>
<div class="m2"><p>چون خط و زلف تو خوش و شیرین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هرکه او را وسیلتی است چنان</p></div>
<div class="m2"><p>نه همانا که حالتیست چنین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گه ز خاک تحیرش بستر</p></div>
<div class="m2"><p>گه ز خشت تحسرش بالین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سخنش چون دهد نتیجه که هست</p></div>
<div class="m2"><p>سخنش بکر و دولتش عنین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همه از روزگار باید دید</p></div>
<div class="m2"><p>شادی شادمان و حزن حزین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شاه‌مات عنا شدم که نکرد</p></div>
<div class="m2"><p>یک پیاده عنایتش فرزین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چه کنم گو کشیده دار کمان</p></div>
<div class="m2"><p>چه کنم گو گشاده دار کمین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آخر این روزگار جافی را</p></div>
<div class="m2"><p>که به جاه تو دارد این تمکین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خود نپرسی یکی ز روی عتاب</p></div>
<div class="m2"><p>تا چه می‌خواهد از من مسکین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فلک تند را نگویی هان</p></div>
<div class="m2"><p>دولت کند را نگویی هین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>وقت کوچ است و عرصه تنگ و مرا</p></div>
<div class="m2"><p>دل به تیمار چرخ راه رهین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نیست در سکنهٔ زمانه کسی</p></div>
<div class="m2"><p>کاضطراب مرا دهد تسکین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو کن احسان که هرکه جز تو بود</p></div>
<div class="m2"><p>ننهد پای زانسوی تحسین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا زمین را طبیعت است آرام</p></div>
<div class="m2"><p>تا زمان را گذشتن است آیین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از زمانت به خیر باد دعا</p></div>
<div class="m2"><p>وز زمینت به طبع باد آمین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ساحت بارگاه عالی تو</p></div>
<div class="m2"><p>برتر از بارگاه علیین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>یمن و یسری که از زمان زاید</p></div>
<div class="m2"><p>دایمت بر یسار باد و یمین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>روزگار آفرین شب و روزت</p></div>
<div class="m2"><p>حافظ و ناصر و مغیث و معین</p></div></div>