---
title: >-
    قصیدهٔ شمارهٔ ۱۲۱ - در مدح صدرالامم کمال‌الدین محمود
---
# قصیدهٔ شمارهٔ ۱۲۱ - در مدح صدرالامم کمال‌الدین محمود

<div class="b" id="bn1"><div class="m1"><p>ای به هستی داده گیتی را کمال</p></div>
<div class="m2"><p>ملک را فرخنده هر روز از تو فال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدر دنیایی و دنیا را به تو</p></div>
<div class="m2"><p>هست هر ساعت کمالی بر کمال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون وزارت آسمان رفعت شود</p></div>
<div class="m2"><p>هر کرا جاه تو افزاید جلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخت بیدار تو حی لاینام</p></div>
<div class="m2"><p>ملک تایید تو ملک لایزال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مقاتب آفتابت زیردست</p></div>
<div class="m2"><p>در معالی آسمانت پایمال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اوج جاهت را ثوابت در جوار</p></div>
<div class="m2"><p>غور حزمت را حوادث در جوال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملک را حزم تو دفع چشم بد</p></div>
<div class="m2"><p>فتنه را دور تو دور گوشمال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اصل اوتاد زمین شد حزم تو</p></div>
<div class="m2"><p>زان چنین ثابت اساس آمد جبال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چیده گوش از نطق تو در ثمین</p></div>
<div class="m2"><p>دیده چشم از کلک تو سحر حلال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناله از کلکت به عدوی شد به خصم</p></div>
<div class="m2"><p>کلک را گو کار خود کردی منال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر کجا امرت سبک دارد عنان</p></div>
<div class="m2"><p>چرخ بستاند رکاب امتثال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کجا قهرت گران دارد رکاب</p></div>
<div class="m2"><p>کوه برتابد عنان احتمال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون گره بر ابروی قهرت زدند</p></div>
<div class="m2"><p>آسمان گوید کفی‌الله القتال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیستی یزدان، چرا هست ای عجب</p></div>
<div class="m2"><p>مثل و مانند ترا هستی محال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عفو تو تعیین کند عذر گناه</p></div>
<div class="m2"><p>جود تو تلقین کند حسن سؤال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای جوانمردی که در ایام تو</p></div>
<div class="m2"><p>هست کمتر ثروت آمال مال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آز را از کثرت برت گرفت</p></div>
<div class="m2"><p>در طباع اکنون ز استغنا ملال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر شود محسوس دریای دلت</p></div>
<div class="m2"><p>اخترش گوهر بود طوبیش نال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اختران را سعیت ار حامی شود</p></div>
<div class="m2"><p>فارغ آیند از هبود و از وبال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آسمان را نهیت ار منعی کند</p></div>
<div class="m2"><p>منفصل گردد زمان را اتصال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ور کند خورشید رای روشنت</p></div>
<div class="m2"><p>سوی چارم چرخ رای انتقال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از سواد شب نماند گرد روز</p></div>
<div class="m2"><p>آن قدر کاید رخش را زلف و خال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اختران کز علمشان خارج نجست</p></div>
<div class="m2"><p>بر جهان بادی و کی بودی محال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جمله اکنون چون به درگاهت رسند</p></div>
<div class="m2"><p>این از آن می‌پرسد آیا چیست حال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای بجایی کز تحیر وصف تو</p></div>
<div class="m2"><p>طوطی نطق مرا کردست لال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون فلک نسگالدت جز نیکویی</p></div>
<div class="m2"><p>بدسگالت را بدی گو می‌سگال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون روان بر آفرینش قول تست</p></div>
<div class="m2"><p>قیل گو چندان که خواهی باش و فال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>طبل را کی سود دارد ولوله</p></div>
<div class="m2"><p>چون باول نافریدندش دوال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ذره گر پنهان کند روی از شعاع</p></div>
<div class="m2"><p>نام هستی هم بر او آید زوال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>صاحبا تا شمع و تا پروانه هست</p></div>
<div class="m2"><p>این غرورانگیز و آن صاحب جمال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برنخیزد گفت و گوی و جست و جوی</p></div>
<div class="m2"><p>گرچه سوزد خویشتن را پر و بال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گوش را از انفعال این سخن</p></div>
<div class="m2"><p>باز خر گو ایهاالساقی تعال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جام مالامال نوش از دست آن</p></div>
<div class="m2"><p>کو به سیارات ننماید جمال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جرعهٔ رخسار او از روی عکس</p></div>
<div class="m2"><p>پر می رنگین کند جام هلال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا که باشد سمت میل آفتاب</p></div>
<div class="m2"><p>گه جنوب از روی دوران گه شمال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سال و مه دورانت اندر سایه باد</p></div>
<div class="m2"><p>ای طفیل دور عمرت ماه و سال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جاودان محروس و محفوظ از هموم</p></div>
<div class="m2"><p>زانکه معصوم آمدستی از همال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سرو اقبال تو تر وز عرق او</p></div>
<div class="m2"><p>باغ دولت را نهال اندر نهال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سد دشمن رخنه چون دندان سین</p></div>
<div class="m2"><p>پشت حاسد کوز چون بالای دال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>معتدل اقبال بادی کو چرا</p></div>
<div class="m2"><p>زانکه بنیاد بقا شد اعتدال</p></div></div>