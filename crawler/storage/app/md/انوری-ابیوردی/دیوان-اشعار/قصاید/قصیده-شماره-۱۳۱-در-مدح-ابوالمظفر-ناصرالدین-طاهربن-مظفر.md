---
title: >-
    قصیدهٔ شمارهٔ ۱۳۱ - در مدح ابوالمظفر ناصرالدین طاهربن مظفر
---
# قصیدهٔ شمارهٔ ۱۳۱ - در مدح ابوالمظفر ناصرالدین طاهربن مظفر

<div class="b" id="bn1"><div class="m1"><p>شرف گوهر اولاد نظام</p></div>
<div class="m2"><p>ملک را باز شرف داد و نظام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صاحب مملکت و خواجهٔ عصر</p></div>
<div class="m2"><p>ناصر دین و نصیر اسلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوالمظفر که به عون ظفرش</p></div>
<div class="m2"><p>عدل شد ظلم و ضیا گشت ظلام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن پس از مبدع و پیش از ابداع</p></div>
<div class="m2"><p>آن به از جنبش و پیش از آرام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیر امرش ببرد کوی صبا</p></div>
<div class="m2"><p>ابر جودش ببرد آب غمام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهد ار قصد کند همت او</p></div>
<div class="m2"><p>بر محیط فلک اعظم گام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عدلش ار چیره شود بر عالم</p></div>
<div class="m2"><p>دیدهٔ باشه شود جای حمام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امنش ار خیمه زند بر صحرا</p></div>
<div class="m2"><p>گرگ را صلح دهد با اغنام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای قضا داده به حکم تو رضا</p></div>
<div class="m2"><p>وی قدر داده به دست تو زمام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کند ار جهد کند دولت او</p></div>
<div class="m2"><p>بر سر توسن افلاک لگام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از پی کثرت خدام تو شد</p></div>
<div class="m2"><p>حامل نطفه طباع ارحام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای ترا گردش افلاک مطیع</p></div>
<div class="m2"><p>وی ترا خواجهٔ اجرام غلام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بنده را بنده خداوندانند</p></div>
<div class="m2"><p>تا که در حضرت تست از خدام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به قبولی که ز اقبال تو دید</p></div>
<div class="m2"><p>مقصد خاص شد و قبلهٔ عام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا قیامت شرفی یافت ز تو</p></div>
<div class="m2"><p>که به جایش نتوان کرد قیام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرچه از خدمت دیرینهٔ او</p></div>
<div class="m2"><p>حاصلی نیست ترا جز ابرام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر به درگاه تو آبی بودش</p></div>
<div class="m2"><p>نام او پخته شود حکمت خام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>علم شعر زند بر شعری</p></div>
<div class="m2"><p>در مدیح تو زند نظم نظام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون ریاضت ز تو یابد نشگفت</p></div>
<div class="m2"><p>توسن طبعش اگر گردد رام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هم در ایام تو جایی برسد</p></div>
<div class="m2"><p>اگر انصاف بیابد ز ایام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر به جز پیش تو تا روز اجل</p></div>
<div class="m2"><p>برکشد تیغ فصاحت ز نیام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کشتهٔ تیغ اجل باد چنان</p></div>
<div class="m2"><p>که نشورش نبود روز قیام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تابد از روی حسام تو ظفر</p></div>
<div class="m2"><p>راست همچون گهر از روس حسام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وتد قاف ترا میخ طناب</p></div>
<div class="m2"><p>اوج خورشید ترا ساق خیام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پست با قدر تو قدر کیوان</p></div>
<div class="m2"><p>کند با تیغ تو تیغ بهرام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پیش حکم تو کشد کلک قضا</p></div>
<div class="m2"><p>خط طغیان و خطا بر احکام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شایدت روز سواری و شکار</p></div>
<div class="m2"><p>آسمان مرکب و مه طرف ستام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روز عیش تو نهد دست قدر</p></div>
<div class="m2"><p>بر کف جان و خرد جام مدام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زیبدت روز تماشا و شراب</p></div>
<div class="m2"><p>زهره خنیاگر و ماه نو جام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر به انگشت ذکا بنمایی</p></div>
<div class="m2"><p>نقطه چون جسم پذیرد اقسام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ور در آیینهٔ خاطر نگری</p></div>
<div class="m2"><p>دهد از راز سپهرت اعلام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرکز عالمی از غایت حلم</p></div>
<div class="m2"><p>هفت اقلیم ترا هفت اندام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خواهد از رای منیرش هر روز</p></div>
<div class="m2"><p>جرم خورشید فلک تابش وام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کاهد از کلک و بنانش هردم</p></div>
<div class="m2"><p>دفتر و کلک عطارد را نام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>واله حکم تو دور افلاک</p></div>
<div class="m2"><p>تابع رای تو سیر اجرام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اول فکرتی و آخر فعل</p></div>
<div class="m2"><p>که جهان شد به وجود تو تمام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وز پی شرح رسوم سیرت</p></div>
<div class="m2"><p>قابل نظم و عروضست کلام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>روز کین نفس نفیس تو کند</p></div>
<div class="m2"><p>چون در اوهام عمل در اجسام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا بود از پی هر شامی صبح</p></div>
<div class="m2"><p>باد بدخواه ترا صبح چو شام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گشته بر خصم تو چون کام نهنگ</p></div>
<div class="m2"><p>همه آفاق وزو یافته کام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر چه تقدیر کنی بی‌مهلت</p></div>
<div class="m2"><p>وانچه آغاز کنی بی‌انجام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مسند صدر مقام تو مقیم</p></div>
<div class="m2"><p>شربت عیش مدام تو مدام</p></div></div>