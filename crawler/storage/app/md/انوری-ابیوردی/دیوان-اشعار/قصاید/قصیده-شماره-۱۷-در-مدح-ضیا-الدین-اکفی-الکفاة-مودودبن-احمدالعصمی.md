---
title: >-
    قصیدهٔ شمارهٔ ۱۷ - در مدح ضیاء الدین اکفی الکفاة مودودبن احمدالعصمی
---
# قصیدهٔ شمارهٔ ۱۷ - در مدح ضیاء الدین اکفی الکفاة مودودبن احمدالعصمی

<div class="b" id="bn1"><div class="m1"><p>آخر ای خاک خراسان داد یزدانت نجات</p></div>
<div class="m2"><p>از بلای غیرت خاک ره گرگانج و کات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در فراق خدمت گرد همایون موکبی</p></div>
<div class="m2"><p>کاندر نعل از هلالست اسب را میخ از نبات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موکب صدر جهان پشت هدی روی ظفر</p></div>
<div class="m2"><p>خواجهٔ دنیی ضیاء دین حق اکفی الکفاة</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاجرم بادت نسیمی یافت چون باد مسیح</p></div>
<div class="m2"><p>لاجرم آبت مزاجی یافت چون آب حیات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه گردون را برو ترجیح نتواند نهاد</p></div>
<div class="m2"><p>عقل کل در هیچ معنی جز که در تقدیم ذات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داده کلک بی‌قرارش کار عالم را قرار</p></div>
<div class="m2"><p>داده رای با ثباتش ملک دنیا را ثبات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچه در گیتی برو نام عطا افتد کفش</p></div>
<div class="m2"><p>جمله را گفتست خذ جام و قلم را گفته هات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در غنایی خواهد افتاد از کفش گیتی چنانک</p></div>
<div class="m2"><p>بر مساکین طرح باید کرد اموال زکات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای ز شرم جاه تو سرگشته اوج اندر فلک</p></div>
<div class="m2"><p>وی ز رشک دست تو نالنده موج اندر فرات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آمدی اندر هنر اقصی نهایات الکمال</p></div>
<div class="m2"><p>چون محیط آسمان اعلی نهایات الجهات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از خداوندی جدا هرگز نبودستی چنانک</p></div>
<div class="m2"><p>نفس موجود از وجود و ذات موصوف از صفات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بعد از آن والی که بنیاد وجود از جود اوست</p></div>
<div class="m2"><p>بر خلایق چون تو والی کس نبودست از ولات</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دست انصاف تو بر بدعت‌سرای روزگار</p></div>
<div class="m2"><p>دست محمودست بر بتخانه‌های سومنات</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر حرم را چون حریم حرمتت بودی شکوه</p></div>
<div class="m2"><p>در درون کعبه هرگز نامدی عزی و لات</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر کرا در دل هوای تست ایمن از هوان</p></div>
<div class="m2"><p>هر که را در جان وفای تست فارغ از وفات</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خود صلاح اهل عالم نیست اندر شرع و رسم</p></div>
<div class="m2"><p>اعتصام الا به حبل طاعتت بعد از صلات</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زانکه امروز از اولوالامری و یزدان در نبی</p></div>
<div class="m2"><p>همچنین گفتست و حق اینست و دیگر ترهات</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خون دل یابد ز باس تو چو گردون بشکند</p></div>
<div class="m2"><p>در عظام دشمن ملک ار همه باشد رفات</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صد عنایت‌نامهٔ گردون حنا بر کرده گیر</p></div>
<div class="m2"><p>چون ز دیوانت به جان کردند خصمی را برات</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خصم را گو هرچه خواهی کن تو و تدبیر ملک</p></div>
<div class="m2"><p>این خبر دانم خداوندا که دانی کل شات</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صاحبا صدرا خداوندا کریما بنده گر</p></div>
<div class="m2"><p>یابد از حرمان عالی بارگاه تو نجات</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بعد از این در خدمت از سر پای سازد چون قلم</p></div>
<div class="m2"><p>زانکه گشتست از فراق تو سیه‌دل چون دوات</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در قضای خدمت ماضیش قوتها دهد</p></div>
<div class="m2"><p>آنکه حسرتهاش میداده است هردم بر فوات</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اندرین خدمت که دارد بنده از تشویر آن</p></div>
<div class="m2"><p>پیش فتیان خراسان دست بر رخ چون فتات</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرچه بعضی شایگانست از قوافی باش گو</p></div>
<div class="m2"><p>عفو کن وقت ادا دانی ندارم بس ادات</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بود الحق تاء چند دیگر از وجدان ولیک</p></div>
<div class="m2"><p>چون ممات و چون قنات و چون روات و چون عدات</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفتم آخر شایگان خوش به از وجدان بد</p></div>
<div class="m2"><p>فی‌المثل چون حادثاتی از ورای حادثات</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هیچ‌کس در یک قوافی بنده را یاری نکرد</p></div>
<div class="m2"><p>هرکه بیتی شعر دانست از رعیت وز رعات</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جز جمال‌الدین خطیب ری که برخواند از نبی</p></div>
<div class="m2"><p>مسلمات مؤمنات قانتات تایبات</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا کند تقطیع این یک وزن وزان سخن</p></div>
<div class="m2"><p>فاعلاتن فاعلاتن فاعلاتن فاعلات</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جیش تو بادا به بلخ و جشن تو بادا به مرو</p></div>
<div class="m2"><p>بارگاهت در نشابور و مقام اندر هرات</p></div></div>