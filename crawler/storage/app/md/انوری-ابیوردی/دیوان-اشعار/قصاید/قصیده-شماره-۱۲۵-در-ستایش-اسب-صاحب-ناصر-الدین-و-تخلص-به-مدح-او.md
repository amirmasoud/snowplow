---
title: >-
    قصیدهٔ شمارهٔ ۱۲۵ - در ستایش اسب صاحب ناصر الدین و تخلص به مدح او
---
# قصیدهٔ شمارهٔ ۱۲۵ - در ستایش اسب صاحب ناصر الدین و تخلص به مدح او

<div class="b" id="bn1"><div class="m1"><p>ای زرین نعل آهنین سم</p></div>
<div class="m2"><p>ای سوسن گوش خیزران دم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای باد صبا گرفته در گل</p></div>
<div class="m2"><p>با آتش تو چو ساق هیزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیر تو به گرد خط ناورد</p></div>
<div class="m2"><p>چون گرد سپهر سیر انجم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر دامن کسوت بهیمه‌ات</p></div>
<div class="m2"><p>بربسته قضا خواص مردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با نرمی حشوهای شانه‌ات</p></div>
<div class="m2"><p>برکنده قدر بروت قاقم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ره گم نکنی و در تحرک</p></div>
<div class="m2"><p>چون گوی ز پای سر کنی گم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مضطر نشوی ز بستن نعل</p></div>
<div class="m2"><p>دردی ندهی ز اول خم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وقت جو اگر ز عجلت طبع</p></div>
<div class="m2"><p>بر گوشهٔ آسمان زنی سم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بهر قضیم تو شود جو</p></div>
<div class="m2"><p>در سنبلهٔ سپهر گندم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در خدمت داغ و طوق صاحب</p></div>
<div class="m2"><p>بس تجربهات بی‌تعلم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن عالم کبریا که عامست</p></div>
<div class="m2"><p>چون رحمت ایزدش ترحم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وهم از پی کبریاش می‌رفت</p></div>
<div class="m2"><p>تا غایت این رونده طارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون عاجز شد به طیره برگشت</p></div>
<div class="m2"><p>یعنی که نمی‌کنم تبرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زان پس خبرش نیافت آری</p></div>
<div class="m2"><p>آنجا که برد پی تسنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای پایهٔ کبریات فارغ</p></div>
<div class="m2"><p>از ننگ تصرف توهم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای حکم ترا قضا پیاپی</p></div>
<div class="m2"><p>وی امر ترا قدر دمادم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صدر تو به پایه تخت جمشید</p></div>
<div class="m2"><p>اسب تو به سایه رخش رستم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با رای تو ذره‌ایست خورشید</p></div>
<div class="m2"><p>با طبع تو قطره‌ایست قلزم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گردون به سر تو خورد سوگند</p></div>
<div class="m2"><p>سر سبزی یافت از تراکم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیدار نشد سپیده‌دم تاش</p></div>
<div class="m2"><p>رای تو نگفت لاتنم قم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فرمان ترا که باد نافذ</p></div>
<div class="m2"><p>جایز شده بر قضا تقدم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عهد تو و در زمانه تقدیم</p></div>
<div class="m2"><p>آب آمده وانگهی تیمم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با دست تو از ترشح ابر</p></div>
<div class="m2"><p>دایم لب برق با تبسم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از لطف تو زاده نوش زنبور</p></div>
<div class="m2"><p>وز عنف تو رسته نیش کژدم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فتنه نکند همی تجاسر</p></div>
<div class="m2"><p>تا عدل تو می‌کند تجشم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از جملهٔ کاینات کانست</p></div>
<div class="m2"><p>کز دست تو می‌کند تظلم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خالی نگذاشتست هرگز</p></div>
<div class="m2"><p>ای عزم تو خالی از تلعثم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مدح تو ضمیری از تفکر</p></div>
<div class="m2"><p>شکر تو زبانی از ترنم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا شکر مزید نعمت آرد</p></div>
<div class="m2"><p>بادی همه ساله در تنعم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا حکم نه آسمان روانست</p></div>
<div class="m2"><p>بر هفت زمین ترا تحکم</p></div></div>