---
title: >-
    قصیدهٔ شمارهٔ ۲۰۰ - در مدح مجدالدین ابوالحسن‌العمرانی اذا شرفه السطان بالتشریف
---
# قصیدهٔ شمارهٔ ۲۰۰ - در مدح مجدالدین ابوالحسن‌العمرانی اذا شرفه السطان بالتشریف

<div class="b" id="bn1"><div class="m1"><p>اختیار سکندر ثانی</p></div>
<div class="m2"><p>زبدهٔ خاندان عمرانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجد دین خواجهٔ جهان که سزاست</p></div>
<div class="m2"><p>اگرش خواجهٔ جهان خوانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار دولت چنان بساخت که نیست</p></div>
<div class="m2"><p>جز که در زلف شب پریشانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیخ بدعت چنان بکند که دیو</p></div>
<div class="m2"><p>ملکی می‌کند نه شیطانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه از رای کرد خورشیدی</p></div>
<div class="m2"><p>وانکه از قدر کرد کیوانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه فیض ترحم عامش</p></div>
<div class="m2"><p>بر جهان رحمتیست یزدانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوبهار نظام عالم را</p></div>
<div class="m2"><p>دست او ابرهای نیسانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشت‌زار بقای دشمن را</p></div>
<div class="m2"><p>قهر او ژالهای طوفانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکه زندان پاس او دارد</p></div>
<div class="m2"><p>چون حوادث هزار زندانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رسم او کرده روی باطل و حق</p></div>
<div class="m2"><p>سوی پوشیدگی و عریانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا نه بس روزگار خواهی دید</p></div>
<div class="m2"><p>فتنه در عهدهٔ جهانبانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نکند آسمان به دشواری</p></div>
<div class="m2"><p>آنچه عزمش کند بسانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نامهای نفاذ حکمش را</p></div>
<div class="m2"><p>حکم تقدیر کرده عنوانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در چنان کف عجب مدار که چوب</p></div>
<div class="m2"><p>از عصایی رسد به ثعبانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قلمش معجزیست حادثه خوار</p></div>
<div class="m2"><p>خاصه در کارهای دیوانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نکند مست طافح کینش</p></div>
<div class="m2"><p>جرعه از دردی پشیمانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدسگالش ز حرص مرگ بمرد</p></div>
<div class="m2"><p>چون طفیلی ز حرص مهمانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرگ جانش همی به جو نخرد</p></div>
<div class="m2"><p>از چه از غایت گران‌جانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای جهان از عنایت تو چنانک</p></div>
<div class="m2"><p>جغد را یاد نیست ویرانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عدل تو راعی مسلمانان</p></div>
<div class="m2"><p>جاه تو حامی مسلمانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بارگاه تو کرده فردوسی</p></div>
<div class="m2"><p>پرده‌دار تو کرده رضوانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو در آن منصبی که گر خواهی</p></div>
<div class="m2"><p>روز بگذشته باز گردانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو در آن پایه‌ای که گر به مثل</p></div>
<div class="m2"><p>کار بر وفق کبریا رانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نایبی را بجای هر کوکب</p></div>
<div class="m2"><p>بر سپهری بری و بنشانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون بجنبی ز گوشهٔ مسند</p></div>
<div class="m2"><p>مسند ملکها بجنبانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>محسنی لاجرم ز قربت شاه</p></div>
<div class="m2"><p>دایم‌الدهر غرق احسانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گرچه ارکان ملک یافته‌اند</p></div>
<div class="m2"><p>عز تشریفهای سلطانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آن نه آنست با تو گویم چیست</p></div>
<div class="m2"><p>آصف و کسوت سلیمانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای چهل سال یک زمان کرده</p></div>
<div class="m2"><p>مصطفی معجز و تو حسانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وانکه من بنده خواستم که کشم</p></div>
<div class="m2"><p>اندرین عقد گوهر کانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بیتکی چند حسب و در هریک</p></div>
<div class="m2"><p>رمزکی شاعرانه پنهانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از تو وز پادشاه و از تشریف</p></div>
<div class="m2"><p>عقل درهم کشیده پیشانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گفت تشریف پادشا وانگه</p></div>
<div class="m2"><p>تو به وصفش رسی و بتوانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هان و هان تا ترا عمادی‌وار</p></div>
<div class="m2"><p>از سر ابلهی و نادانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>درنیفتد حدیث مصحف و بند</p></div>
<div class="m2"><p>کان مثل نیست نیک تا دانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>این همی گوی کای ز کنه ثنات</p></div>
<div class="m2"><p>خاطرم در مضیق حیرانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وی ز لطف خدایگان و خدا</p></div>
<div class="m2"><p>به چنین صد لطیفه ارزانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>وی در این تهنیت بجای نثار</p></div>
<div class="m2"><p>از در جان که بر تو افشانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بنده از جان‌نثاری آوردست</p></div>
<div class="m2"><p>همه گوهر ولیک روحانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>او چو از جان ترا ثنا گوید</p></div>
<div class="m2"><p>جان‌فشانی بود ثناخوانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا که در من‌یزید دور بود</p></div>
<div class="m2"><p>روی نرخ امل به ارزانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دور تو عمر باد و چندان باد</p></div>
<div class="m2"><p>کز امل داد بخت بستانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بلکه از بی‌نهایتی چو ابد</p></div>
<div class="m2"><p>که نگنجد درو دو چندانی</p></div></div>