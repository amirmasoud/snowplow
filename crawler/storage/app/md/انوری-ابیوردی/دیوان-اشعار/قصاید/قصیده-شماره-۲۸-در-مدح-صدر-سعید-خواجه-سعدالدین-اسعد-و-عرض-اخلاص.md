---
title: >-
    قصیدهٔ شمارهٔ ۲۸ - در مدح صدر سعید خواجه سعدالدین اسعد و عرض اخلاص
---
# قصیدهٔ شمارهٔ ۲۸ - در مدح صدر سعید خواجه سعدالدین اسعد و عرض اخلاص

<div class="b" id="bn1"><div class="m1"><p>منت از کردگار دادگرست</p></div>
<div class="m2"><p>که ترا کار با نظام‌ترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدرآفاق وسعد دین که ز قدر</p></div>
<div class="m2"><p>قدمش جای تارک قمرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این مراتب کنون که می بینی</p></div>
<div class="m2"><p>اثر جزو کلی قدرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باش تا صبح دولتت بدمد</p></div>
<div class="m2"><p>کین لطایف نتیجهٔ سحرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای جوادی که دست و طبع ترا</p></div>
<div class="m2"><p>کان دعاگوی و بحر سجده برست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش دست و دل تو ناچیزست</p></div>
<div class="m2"><p>هرچه در بحر و کان زر و گهرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دم و کلک تو در بیان و بنان</p></div>
<div class="m2"><p>گرچه بر یار و خضم نفع و ضرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غیرت روح عیسی است این یک</p></div>
<div class="m2"><p>خجلت چوب موسی آن دگرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچه در زیر چرخ داناییست</p></div>
<div class="m2"><p>راستی پرتوی از آن هنرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رانده‌ای بر جهان تو آن احکام</p></div>
<div class="m2"><p>کز خجالت رخ زمانه ترست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیش دست تو ابر چون دودست</p></div>
<div class="m2"><p>بر طبع تو بحر چون شمرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ذهن پاک تو ناطق وحی است</p></div>
<div class="m2"><p>نوک کلک تو منشی ظفرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در حصار حمایت حزمت</p></div>
<div class="m2"><p>مرگ چون حلقه از برون درست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مابقی را ز خوان خود پندار</p></div>
<div class="m2"><p>هرچه بر خوان دهر ماحضرست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مه و خورشید شوخ و بی‌شرمند</p></div>
<div class="m2"><p>تا چرا بر سر توشان گذرست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جود تو آن شنیده این دیده</p></div>
<div class="m2"><p>مه مگر کور و آفتاب کرست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به حقیقت بدان که مثل تو نیست</p></div>
<div class="m2"><p>زیر گردون مگر که بر زبرست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آمدم با حدیث سیرت خویش</p></div>
<div class="m2"><p>که نمودار مردمان سیرست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به خدایی که در دوازده برج</p></div>
<div class="m2"><p>هفت پیکش همیشه در سفرست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عمل کارگاه صنعت اوست</p></div>
<div class="m2"><p>که سواد مه و بیاض خورست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به صفای صفی حق آدم</p></div>
<div class="m2"><p>که سر انبیا و بوالبشرست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به دعایی که کرد نوح نجی</p></div>
<div class="m2"><p>که در آفاق از آن هنوز اثرست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به رضای خلیل ابراهیم</p></div>
<div class="m2"><p>که به تسلیم در جهان سمرست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حق داود و لطف نعمت او</p></div>
<div class="m2"><p>که ترا در بهشت منتظرست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به نماز و نیاز یعقوبی</p></div>
<div class="m2"><p>در غم یوسفی کش او پسرست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به کف موسی کلیم کریم</p></div>
<div class="m2"><p>به دم عیسیی که زنده‌گرست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به سر مصطفی شریف قریش</p></div>
<div class="m2"><p>که ز جمع رسل عزیرترست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به صفا و وفا و صدق عتیق</p></div>
<div class="m2"><p>که ز دل جان فروش و شرع خرست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به دلیری و هیبت عمری</p></div>
<div class="m2"><p>که ظهور شریعت از عمرست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به حیا و حیات ذوالنورین</p></div>
<div class="m2"><p>که حقیقت مؤلف سورست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به کف و ذوالفقار مرتضوی</p></div>
<div class="m2"><p>که به حرب اندرون چو شیر نرست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حرمت جبرئیل روح امین</p></div>
<div class="m2"><p>که به عصمت جهانش زیرپرست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حق میکال خواجهٔ ملکوت</p></div>
<div class="m2"><p>که ز کروبیان مهینه‌ترست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به صدا و ندای اسرافیل</p></div>
<div class="m2"><p>که منادی و منهی حشرست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به کمال و جلال عزرائیل</p></div>
<div class="m2"><p>که کمین‌دار جان جانورست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به صلوة و صیام و حج و جهاد</p></div>
<div class="m2"><p>کاصل اسلام از این چهار درست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به حق کعبه و صفا و منی</p></div>
<div class="m2"><p>حق آن رکن کش لقب حجرست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به کلام خدای عز و جل</p></div>
<div class="m2"><p>که هر آیت ازو دو صد عبرست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>حرمت روضه و قیامت و خلد</p></div>
<div class="m2"><p>حق حصنی که نام آن سقرست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به عزیزی و حق نعمت تو</p></div>
<div class="m2"><p>که زیادت ز قطرهٔ مطرست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به کریمی و لطف و رحمت حق</p></div>
<div class="m2"><p>که گنه‌کار را امیدورست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که مرا در وفای خدمت تو</p></div>
<div class="m2"><p>نه به شب خواب و نه به روز خورست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چمن بوستان نعت ترا</p></div>
<div class="m2"><p>خاطرم آن درخت بارورست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که ز مدح و ثنا و شکر و دعا</p></div>
<div class="m2"><p>دایمش بیخ و شاخ و برگ و برست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آنچه گفتند حاسدان به غرض</p></div>
<div class="m2"><p>به سر تو که جملگی هدرست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خاک نعل ستور تو بر من</p></div>
<div class="m2"><p>بهتر از توتیای چشم سرست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زانکه دانم که پیش همت تو</p></div>
<div class="m2"><p>آفرینش به جمله بی‌خطرست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سبب خدمت تو از دل پاک</p></div>
<div class="m2"><p>جان من بسته بر میان کمرست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>پس اگر ز اعتماد در مستی</p></div>
<div class="m2"><p>حالتی اوفتاد کان سیرست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تو پسندی که رد کنی سخنم</p></div>
<div class="m2"><p>چون منی را به چون تویی نظرست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چکنم بازگیرم از تو مدیح</p></div>
<div class="m2"><p>بنده را آخر این قدر بصرست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چه حدیث است از تو برگردم</p></div>
<div class="m2"><p>الله الله دو قول مختصرست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چون به عالم تویی مرا مقصود</p></div>
<div class="m2"><p>از در تو بگو دگر گذرست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پس بگویند بنده را حاشاک</p></div>
<div class="m2"><p>مردکی ریش گاو کون خرست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ای جوادی که خاک پایت را</p></div>
<div class="m2"><p>بوسه ده گشته هرکه تاجورست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>عفو فرمای گر مثل گنهم</p></div>
<div class="m2"><p>خون شپیر و کشتن شپرست</p></div></div>