---
title: >-
    قصیدهٔ شمارهٔ ۵ - در مدح امیر سید مجدالدین ابوطالب
---
# قصیدهٔ شمارهٔ ۵ - در مدح امیر سید مجدالدین ابوطالب

<div class="b" id="bn1"><div class="m1"><p>زان پس که قضا شکل دگر کرد جهان را</p></div>
<div class="m2"><p>وز خاک برون برد قدر امن و امان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بلخ چه پیری و جوانی بهم افتاد</p></div>
<div class="m2"><p>اسباب فراغت بهم افتاد جهان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بخت جوان و خرد پیر گشادند</p></div>
<div class="m2"><p>بر منفعت خلق در دست و زبان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیوسته ثنا گفت فلک همت این را</p></div>
<div class="m2"><p>همواره دعا گفت ملک دولت آن را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این مزرعهٔ تخم سخا کرد زمین را</p></div>
<div class="m2"><p>وان دفتر آیات ثنا کرد زبان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن دید جهان از کرم هر دو که هرگز</p></div>
<div class="m2"><p>در حصر نیاید نه یقین را نه گمان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نزد تو اگر صورت این حال نهانست</p></div>
<div class="m2"><p>بر رای تو پیدا کنم این راز نهان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوطالب نعمه چو شهاب زکی از جود</p></div>
<div class="m2"><p>یک چند کم آورد چه دریا و چه کان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون دست حوادث در این هر دو فروبست</p></div>
<div class="m2"><p>دربست جهان‌باز ز امساک میان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن بود که بحر کرمش زود برانگیخت</p></div>
<div class="m2"><p>از لجهٔ کف ابر چو دریای روان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا بر دهن خشک جهان نایژه بگشاد</p></div>
<div class="m2"><p>وز بیخ بزد شعلهٔ نار حدثان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ورنه که به تن باز رسانیدی از این قوم</p></div>
<div class="m2"><p>باکتم عدم رفته دو صد قافله جان را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>القصه از این طایفه کز روی مروت</p></div>
<div class="m2"><p>آسان گذرانند جهان گذران را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زیر فلک پیر ز پیران و جوانان</p></div>
<div class="m2"><p>او ماند و تو دانی که نماند دگران را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بختیست جوان اهل جهان را به حقیقت</p></div>
<div class="m2"><p>یارب تو نگهدار مر این بخت جوان را</p></div></div>