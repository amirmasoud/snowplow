---
title: >-
    قصیدهٔ شمارهٔ ۱۶۱ - در مدح پیروزشاه عادل
---
# قصیدهٔ شمارهٔ ۱۶۱ - در مدح پیروزشاه عادل

<div class="b" id="bn1"><div class="m1"><p>ای شمس دین و شمس فلک آسمان تو</p></div>
<div class="m2"><p>ای صدر ملک و صدر جهان آستان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای چرخ پست همبر رای رفیع تو</p></div>
<div class="m2"><p>وی ابر زفت همبر بذل بنان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرام خاک تابع پای رکاب تست</p></div>
<div class="m2"><p>تعجیل باد والهٔ دست و عنان تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اسباب دهر دادهٔ دست سخای تو</p></div>
<div class="m2"><p>اشکال عقل سخرهٔ کشف و بیان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذات مقدس تو جهانیست از کمال</p></div>
<div class="m2"><p>یک جزو نیست کل کمال از جهان تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر لامکان روا بودی جای هیچ‌کس</p></div>
<div class="m2"><p>از قدر و از مکان تو بودی مکان تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور بر قضا روان شودی امر هیچ‌کس</p></div>
<div class="m2"><p>راه قضا ببستی امر روان تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رازی که از زمانه نهان داشت آسمان</p></div>
<div class="m2"><p>راند در این زمانه همی بر زبان تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر با زمانه کلک تو گوید که در زمین</p></div>
<div class="m2"><p>منظور کیست حکم قضا گوید آن تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اسرار عالمش به حقیقت شود یقین</p></div>
<div class="m2"><p>هرکو کند مطالعهٔ لوح گمان تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مریخ رابه خنجر تو سرزنش کند</p></div>
<div class="m2"><p>گر دیدهٔ سپهر ببیند سنان تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شکل هلال و بدر ز تاثیر شمس نیست</p></div>
<div class="m2"><p>این هست عکس جام تو وان ظل خوان تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جوزا به پیش طالع سعدت کمر ببست</p></div>
<div class="m2"><p>چون دست تو شده است مگر بر میان تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>واندر مراتب هنر ابنای ملک را</p></div>
<div class="m2"><p>آیین وسان دگر شد از آیین وسان تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر ذروهٔ وجود رساند خدنگ خویش</p></div>
<div class="m2"><p>شست شهاب اگر به کف آرد کمان تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا شاخ را ز باد صبا تربیت بود</p></div>
<div class="m2"><p>بیخ فنا برآمده از بوستان تو</p></div></div>