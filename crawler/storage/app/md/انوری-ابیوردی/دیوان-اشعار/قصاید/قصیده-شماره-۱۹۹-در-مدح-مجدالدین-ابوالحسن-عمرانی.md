---
title: >-
    قصیدهٔ شمارهٔ ۱۹۹ - در مدح مجدالدین ابوالحسن عمرانی
---
# قصیدهٔ شمارهٔ ۱۹۹ - در مدح مجدالدین ابوالحسن عمرانی

<div class="b" id="bn1"><div class="m1"><p>دلم ای دوست تو داری دانی</p></div>
<div class="m2"><p>جان ببر نیز که می‌بتوانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دلی صحبت تو نیست گران</p></div>
<div class="m2"><p>چه حدیثست به جان ارزانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویمت بوسه مرا گویی جان</p></div>
<div class="m2"><p>این بده تا مگر آن بستانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویم این نیست بدان دشواری</p></div>
<div class="m2"><p>گویی آن نیست بدین آسانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی گرم بوسه دهی جان منی</p></div>
<div class="m2"><p>که گرم جان ببری هم جانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاهم از عشوره‌گری می‌خوانی</p></div>
<div class="m2"><p>گاهم از طیره‌گری می‌رانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه در پای تو افتم چه شود</p></div>
<div class="m2"><p>گر سری در سخنم جنبانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با فلک یار مشو در بد من</p></div>
<div class="m2"><p>ای به هر نیکویی ارزانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که چو از حد ببری فاش کنم</p></div>
<div class="m2"><p>قصهٔ درد ز بی‌درمانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا ترا از سر من باز کند</p></div>
<div class="m2"><p>مجد دین بوالحسن عمرانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه از رای کند خورشیدی</p></div>
<div class="m2"><p>وانکه از قدر کند کیوانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنکه لطفش مدد آبادی</p></div>
<div class="m2"><p>وانکه قهرش سبب ویرانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنکه در حبس سیاست دارد</p></div>
<div class="m2"><p>فتنه و جور و ستم زندانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بندهٔ نعمت او هر انسی</p></div>
<div class="m2"><p>بستهٔ طاعت او هر جانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ابرهای کرمش آذاری</p></div>
<div class="m2"><p>موجهای سخطش طوفانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صورت مجلس او فردوسی</p></div>
<div class="m2"><p>سیرت حاجب او رضوانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نز پی منع بود دربانش</p></div>
<div class="m2"><p>کز پی رسم بود دربانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای هنرهای تو افریدونی</p></div>
<div class="m2"><p>وی اثرهای تو نوشروانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تویی آن‌کس که اگر قصد کنی</p></div>
<div class="m2"><p>خاک بر تارک چرخ افشانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مایه از جود تو دارد نه ز طبع</p></div>
<div class="m2"><p>نامی و معدنی و حیوانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تویی آن‌کس که اگر منع کنی</p></div>
<div class="m2"><p>باد را از حرکت بنشانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اول فکرتی و آخر فعل</p></div>
<div class="m2"><p>آنی از هرچه توان گفت آنی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه ز آسیب قضاکوب خوری</p></div>
<div class="m2"><p>نه به اشکال قدر درمانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به سر کوی کمالت نرسد</p></div>
<div class="m2"><p>پای اندیشه ز سرگردانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر کجا نام وقار تو برند</p></div>
<div class="m2"><p>خاک بر خاک نهد پیشانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هرکجا شرح صفای تو دهند</p></div>
<div class="m2"><p>آب آبی شود از حیرانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در شکار از پی سائل تازی</p></div>
<div class="m2"><p>در نماز آیت احسان خوانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آفتابی که رسد منفعتت</p></div>
<div class="m2"><p>به خرابی و به آبادانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>معنی از کلک تو گیرد نه ز عقل</p></div>
<div class="m2"><p>قوت ناطقهٔ انسانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>انتقامت نه ز پاداش و جزا</p></div>
<div class="m2"><p>همه کس داند و تو هم دانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که نه آزردهٔ یک مکروهی</p></div>
<div class="m2"><p>که نه آلودهٔ یک احسانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پیشی از دور به تمکین و جواز</p></div>
<div class="m2"><p>گرچه در دایرهٔ دورانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برتر از نه فلکی در رفعت</p></div>
<div class="m2"><p>گرچه در حیز چار ارکانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دامن امن تو دارد پنهان</p></div>
<div class="m2"><p>صدهزاران صفت شیطانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کرم طبع تو دارد پیدا</p></div>
<div class="m2"><p>صد هزاران ملک روحانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>حزم سنگین تو دولت راهست</p></div>
<div class="m2"><p>بارهٔ محکم ناجسمانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عرض پاک تو جهان ثالث</p></div>
<div class="m2"><p>عزم جزم تو قضای ثانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ای نمودار حیات باقی</p></div>
<div class="m2"><p>روی بازار جهان فانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بنده روزی دو گر از خدمت تو</p></div>
<div class="m2"><p>مانده محروم ز بی‌سامانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به روانی و نفاذ فرمانت</p></div>
<div class="m2"><p>کان نرفتست ز نافرمانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>حکمها بود که مانع بودند</p></div>
<div class="m2"><p>بیشتر طالعی و یزدانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر بدین عذر نداری معذور</p></div>
<div class="m2"><p>دیگری دارم و آن کم دانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا که نقاش فلک ننگارد</p></div>
<div class="m2"><p>روز روشن چو شب ظلمانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همه عمر از اثر دور فلک</p></div>
<div class="m2"><p>باد چون روز شبت نورانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مدت عمر تو چون مدت دور</p></div>
<div class="m2"><p>بی‌کران از مدد نفسانی</p></div></div>