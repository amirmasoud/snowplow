---
title: >-
    قصیدهٔ شمارهٔ ۸۴ - در مدح امیر ناصرالدین قتلغ شاه
---
# قصیدهٔ شمارهٔ ۸۴ - در مدح امیر ناصرالدین قتلغ شاه

<div class="b" id="bn1"><div class="m1"><p>شب و شمع و شکر و بوی گل و باد بهار</p></div>
<div class="m2"><p>می و معشوق و دف و رود و نی و بوس و کنار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبزه و آب گل‌افشان و صبوحی در باغ</p></div>
<div class="m2"><p>نالهٔ بلبل و آواز بت سیم عذار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش بود خاصه کسی را که توانایی هست</p></div>
<div class="m2"><p>وای بر آنکه دلی دارد و آنهم افکار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوبهار آمد و هنگام طرب در گلزار</p></div>
<div class="m2"><p>چه بهاری که ز دلها ببرد صبر و قرار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقیا خیز که گل رشک رخ حورا شد</p></div>
<div class="m2"><p>بوستان جنت و می کوثر و طوبیست چنار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرده خواهد که بجنبد به چنین وقت از جا</p></div>
<div class="m2"><p>کشته خواهد که ز خون لاله کند با گلنار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار می‌ساز که بی‌می نتوان رفت به باغ</p></div>
<div class="m2"><p>مست رو سوی چمن تات کند باغ نثار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلبل شیفته مست است و گل و سرو و سمن</p></div>
<div class="m2"><p>نپسندند که او مست بود ما هشیار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باد نوروز سحرگه چو به بستان بگذشت</p></div>
<div class="m2"><p>گل صد برگ برون رست ز پیرامن خار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرب‌دستی فلک بین تو که بی‌خامه و رنگ</p></div>
<div class="m2"><p>کرد اطراف چمن را همه پر نقش و نگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نقش‌بندی هوا باز نگه کن بر گل</p></div>
<div class="m2"><p>که دو صد دایره بر دایره زد بی‌پرگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شکل غنچه است چو پیکان که بود بر آتش</p></div>
<div class="m2"><p>برگ بیدست چو تیغی که برآرد زنگار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گل نارست درخشنده چو یاقوتین جام</p></div>
<div class="m2"><p>دانهٔ نار چو لل و چو در جست انار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طفل غنچه عرق آورده ز تب بر رخ از آن</p></div>
<div class="m2"><p>مادر ابر همی اشک برو بارد زار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دی گل سرخ و سهی سرو رسیدند به هم</p></div>
<div class="m2"><p>در میان آمدشان گفت و شنودی بسیار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گل همی گفت ترا نیست بر من قیمت</p></div>
<div class="m2"><p>سرو می‌گفت ترا نیست بر من مقدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گل ازو طیره شد و گفت که ای بی‌معنی</p></div>
<div class="m2"><p>دم خوبی زنی آخر به کدام استظهار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گویی آزادم و بر یک قدمی پیوسته</p></div>
<div class="m2"><p>دعوی رقص نمایی و نداری رفتار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سرو لرزان شد و زان طعنه به گل گفت که من</p></div>
<div class="m2"><p>پای برجایم و همچون تو نیم دست‌گذار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سالها بودم در باغ و ندیدم رخ شهر</p></div>
<div class="m2"><p>تو که دوش آمدی امروز شدی در بازار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گل دگربار برآشفت و بدو گفت که من</p></div>
<div class="m2"><p>هر به یک سال یکی هفته نمایم دیدار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه پس از یازده مه بودن من در پرده</p></div>
<div class="m2"><p>که کنون نیز بپوشم رخ و بنشینم زار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سوی شهر از پی آن رفتم تا دریابم</p></div>
<div class="m2"><p>بزم خورشید زمین سایهٔ حق فخر کبار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نازش ملک و ملک ناصر دین قتلغ شاه</p></div>
<div class="m2"><p>که بدو فخر کند تخت به روزی صدبار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن جوان بخت شه پاکدل پاک‌سرشت</p></div>
<div class="m2"><p>آن نکوسیرت نیکوسیر نیکوکار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آن خردمند هنردوست که کردست خجل</p></div>
<div class="m2"><p>بحر و کان را به گه بذل یمینش ز یسار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کف او ضامن ارزاق وحوشست و طیور</p></div>
<div class="m2"><p>در او قبلهٔ ارکان بلادست و دیار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خه‌خه ای قدر ترا طارم گردون کرسی</p></div>
<div class="m2"><p>زه زه ای رای ترا صبح منیر آینه‌دار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هرچه گویم به مدیح تو و گویند کسان</p></div>
<div class="m2"><p>تو از آن بیشتری نیست در آن هیچ انکار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>منکران همه عالم چو رسیدند به تو</p></div>
<div class="m2"><p>بر تمیز و خرد و خلق تو کردند اقرار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>احتشام تو درختی است به غایت عالی</p></div>
<div class="m2"><p>که نشاط و طرب و ناز و نعیم آرد بار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو سلیمانی و زیر تو فرس تخت روان</p></div>
<div class="m2"><p>تخت از معجزه بر باد نشسته چو غبار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون کدو خصم تو گردنکش اگر شد چه شود</p></div>
<div class="m2"><p>هم تواش باز کنی پوست ز تن همچو خیار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>با همه سرکشی توسن گردون چو شتر</p></div>
<div class="m2"><p>دست حکم تو ببینیش درون کرد مهار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نیست جز کلک تو گر کلک بود مشک‌فشان</p></div>
<div class="m2"><p>نیست جز طبع تو گر طبع بود گوهربار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همچو باران به نشیب افتد بدخواه تو باز</p></div>
<div class="m2"><p>گر به بالاکشدش چرخ دو صد ره چو بخار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دشمنت را چو خرد نیست اگر گنج نهد</p></div>
<div class="m2"><p>نشود مالک دینار به ملک و دینار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نشود مشک اگر چند فراوان ماند</p></div>
<div class="m2"><p>جگر سوخته در نافهٔ آهوی تتار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>علم دولت تو میخ زمین است و زمان</p></div>
<div class="m2"><p>عزت ذات شریفت شرف لیل و نهار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ده ره از نه فلک ایام شنیدست صریح</p></div>
<div class="m2"><p>که تویی واسطهٔ هفت و شش و پنج و چهار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر چو فرعون لعین خصم تو در بحر شود</p></div>
<div class="m2"><p>موکب موسویت گرد برآرد ز بحار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>باز تمکین تو هرجا که به پرواز آید</p></div>
<div class="m2"><p>سر فرو دزدد بدخواه تو چون بوتیمار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گرد نبندد کمر مهر تو چون مور عدوت</p></div>
<div class="m2"><p>زود از پوست برون آردش ایام چو مار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تو چنانی که در آفاق ترا نیست نظیر</p></div>
<div class="m2"><p>به صفا و به حیا و به ثبات و به وقار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>باز اخوان خردمند ترا چتوان گفت</p></div>
<div class="m2"><p>زیرک و فاضل و دشمن‌شکن و کارگذار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سرورا، پاکدلا، زین فلک بی‌سر و پا</p></div>
<div class="m2"><p>زندگانی رهی گشت به غایت دشوار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نقد می‌بایدم امروز ز خدمت صد چیز</p></div>
<div class="m2"><p>نقدتر از همه حالی فرجی و دستار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بندگانند فراوان ز تو با نعمت و ناز</p></div>
<div class="m2"><p>بنده را نیز چه باشد هم از ایشان انگار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>وقت آنست که خواهی ز کرم کلک و دوات</p></div>
<div class="m2"><p>بدری پارهٔ کاغذ ز کنار طومار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بر هر آن کس که براتم بنویسی شاید</p></div>
<div class="m2"><p>به کمال‌الدین باری ننویسی زنهار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زانکه آن ظالم بی‌رحم یکی حبه نداد</p></div>
<div class="m2"><p>زان زر و جامه و کرباس و کتان من پار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آن کمالی که چو نقصان من آمد در پیش</p></div>
<div class="m2"><p>زان ندیدم من از آن هدیهٔ شاهی آثار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هجو کی خواستمش گفت ولی ترسیدم</p></div>
<div class="m2"><p>که نه بر طبع ملک راست بود آن گفتار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بحلش کردم اگر چند که او ظالم بود</p></div>
<div class="m2"><p>با ویم بیش از این نیز مبادا سر و کار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تا جهان ماند، ماناد وجودت به جهان</p></div>
<div class="m2"><p>بادی از بخت و جوانی و جهان برخوردار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دوستان جمع و ندیمان خوش و دولت باقی</p></div>
<div class="m2"><p>سر تو سبز و دلت شاد و تنت بی‌آزار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>عید فرخنده و در عید به رسم قربان</p></div>
<div class="m2"><p>سر بریده عدویت همچو شتر زار و نزار</p></div></div>