---
title: >-
    قصیدهٔ شمارهٔ ۷۹ - در مدح امیرکبیر ضیاء الدین مودود احمد عصمی
---
# قصیدهٔ شمارهٔ ۷۹ - در مدح امیرکبیر ضیاء الدین مودود احمد عصمی

<div class="b" id="bn1"><div class="m1"><p>دوش از درم درآمد سرمست و بی‌قرار</p></div>
<div class="m2"><p>همچون مه دو هفته و هر هفت کرده یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با زلف تابدار دلاویز پر شکن</p></div>
<div class="m2"><p>با چشم نیم‌خواب جهان‌سوز پرخمار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جستم ز جای و پیش دوید و سلام کرد</p></div>
<div class="m2"><p>واوردمش چو تنگ شکر تنگ در کنار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت از کجات پرسم و خود کی رسیده‌ای</p></div>
<div class="m2"><p>چونی بماندگی و چگونست حال و کار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که حالم از غم تو بس تباه بود</p></div>
<div class="m2"><p>لیکن کنون ز شادی روی تو چون نگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا همچون چنگ تو به کنارم نیامدی</p></div>
<div class="m2"><p>بودم چو زیر چنگ تو با ناله‌های زار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنشست و ماجرای فراق از نخست روز</p></div>
<div class="m2"><p>آغاز کرد و قصهٔ آن گوی و اشکبار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌گفت و می‌گریست که آخر چو درگذشت</p></div>
<div class="m2"><p>بی‌تو ز حد طاقت من بار انتظار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منت خدای را که به هم باز یک نفس</p></div>
<div class="m2"><p>دیدار بود بار دگرمان در این دیار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>القصه از سخن به سخن شد چو یک زمان</p></div>
<div class="m2"><p>گفتیم از این حدیث و گرفتیم اعتبار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>افتاد در معانی و تقطیع شاعری</p></div>
<div class="m2"><p>بر وزنهای مشکل و الفاظ مستعار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتا اگرچه مست و خرابم سؤال کن</p></div>
<div class="m2"><p>رمزی دو زین نمط نه نهان بل به آشکار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتم که چیست آنکه پس دور چرخ ازوست</p></div>
<div class="m2"><p>گر زیر دور چرخ یمین است یا یسار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در بزم رشک برده برو شاخ در خزان</p></div>
<div class="m2"><p>در بذل شرم خورده از او ابر در بهار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اصل وجود اوست که از بیخ فرع اوی</p></div>
<div class="m2"><p>دارد همان نظام که از هفت و از چهار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفتا که دست نایب سلطان شرق و غرب</p></div>
<div class="m2"><p>آن از جهان گزیده و دستور شهریار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مودود احمد عصمی کز نفاذ امر</p></div>
<div class="m2"><p>دارد زمام گیتی در دست اختیار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتم که چیست آن تن بی‌جان که در صبی</p></div>
<div class="m2"><p>بودی صباش دایه و مادرش جویبار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زو موج فتنه ساکن و او روز و شب دوان</p></div>
<div class="m2"><p>زو ملک شاه فربه و او سال و مه نزار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گه در مزاج حرف نهد نفس ناطقه</p></div>
<div class="m2"><p>گه در کنار نطق کند در شاهوار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفتا که کلک نایب دستور شرق و غرب</p></div>
<div class="m2"><p>آن لطف گاه بر و سیاست به روز بار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مودود احمد عصمی کز مکان اوست</p></div>
<div class="m2"><p>بنیاد دین و قاعدهٔ دولت استوار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفتم قصیده‌ای اگرت امتحان کنم</p></div>
<div class="m2"><p>در مدح این خلاصهٔ مقصود روزگار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>طبعت بدان قیام تواند نمود گفت</p></div>
<div class="m2"><p>کم‌گوی قصه، خیز دوات و قلم بیار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برخاستم دوات و قلم بردمش به پیش</p></div>
<div class="m2"><p>آن یار ناگزیر و رفیق سخن‌گزار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>برداشت کلک و کاغذ و فرفر فرونوشت</p></div>
<div class="m2"><p>بر فور این قصیدهٔ مطبوع آبدار</p></div></div>