---
title: >-
    قصیدهٔ شمارهٔ ۱۳۷ - قال فی‌التفاخر و شکایة الزمان
---
# قصیدهٔ شمارهٔ ۱۳۷ - قال فی‌التفاخر و شکایة الزمان

<div class="b" id="bn1"><div class="m1"><p>تا آمد از عدم به وجود اصل پیکرم</p></div>
<div class="m2"><p>جز غم نبود بهره ز چرخ ستمگرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون شد دلم در آرزوی آنکه یک نفس</p></div>
<div class="m2"><p>بی‌خار غم ز گلشن شادی گلی برم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیموده گشت عمر به پیمانهٔ نفس</p></div>
<div class="m2"><p>گویی به کام دل نفسی کی برآورم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کردم نظر به فکر در احکام نه فلک</p></div>
<div class="m2"><p>جز نو عروس غم نشد از عمر همسرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هستم یقین که در چمن باغ روزگار</p></div>
<div class="m2"><p>بی‌بر بود نهال امیدی که پرورم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بزمگاه محنت گیتی به جام عمر</p></div>
<div class="m2"><p>جز خون دل ز دست زمانه نمی‌خورم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زیرا که تا برآرم از اندیشه یک نفس</p></div>
<div class="m2"><p>پر خون دل شود ز ره دیده ساغرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کحل شب چو دیدهٔ ناهید شب گمار</p></div>
<div class="m2"><p>روشن شود چو اختر طبع منورم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خورشید غم ز چشمهٔ دل سر برآورد</p></div>
<div class="m2"><p>تا کان لعل گردد بالین و بسترم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حالم مخالف آمد از آن در جهان عمر</p></div>
<div class="m2"><p>درویشم از نشاط و زانده توانگرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دست زمانه جدول انده به من کشید</p></div>
<div class="m2"><p>زیرا که چون قلم به صفت سخت لاغرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناچیز شد وجودم از اشکال مختلف</p></div>
<div class="m2"><p>گویی عرض گشاده شد از بند جوهرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از روشنان شب که چو سیماب و اخگرند</p></div>
<div class="m2"><p>پیوسته بی‌قرار چو سیماب و اخگرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وز بازی سپهر سبکبار بوالعجب</p></div>
<div class="m2"><p>بر تخته نرد رنج و بلا در مششدرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی‌آب شد چو چشمهٔ خورشید روزگار</p></div>
<div class="m2"><p>در عشق او رواست که بنشیند آذرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر من در حوادث و انده از آن گشاد</p></div>
<div class="m2"><p>کز خانهٔ حوادث چون حلقه بر درم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خواندم بسی علوم ولیکن به عاقبت</p></div>
<div class="m2"><p>علمم وبال شد که فلک نیست یاورم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کوته کنم سخن چو گواه دل منند</p></div>
<div class="m2"><p>چشم عقیق بارم و روی مزعفرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صحرای عمر اگر چه خوش آمد به چشم عقل</p></div>
<div class="m2"><p>از رنج دل به پای نفس زود بسپرم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کین چرخ سرکشست و نباشد موافقم</p></div>
<div class="m2"><p>وین دهر توسن است و نگردد مسخرم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای چرخ سفله‌پرور دلبند جان‌شکر</p></div>
<div class="m2"><p>شد زهر با وجود تو در کام شکرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>واقف نمی‌شوی تو بر اسرار خاطرم</p></div>
<div class="m2"><p>فاسد شدست اصل مزاجت گمان برم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر خشک شد دماغ نهادت عجب مدار</p></div>
<div class="m2"><p>در حلق و در مشام تو چون مشک اذفرم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای بی‌وفا جهان دلم از درد خون گرفت</p></div>
<div class="m2"><p>دریاب پیش از آنکه رسد جان به غرغرم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یکتا شدم به تاب هوای تو تاکنون</p></div>
<div class="m2"><p>از بار غم دوتا شده بر شکل چنبرم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای روزگار شیفته چندین جفا مکن</p></div>
<div class="m2"><p>آهسته‌تر که چرخ جفا را نه محورم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون آمدم بر تو که پایم شکسته باد</p></div>
<div class="m2"><p>راه وفا سپر که جفا نیست درخورم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در آب فتنه خفته چو نیلوفرم مدار</p></div>
<div class="m2"><p>بر آتش نهیب مسوزان چو عنبرم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وز ثقل رنج و خفت ضعف تنم مکن</p></div>
<div class="m2"><p>چون خاک خیره طبعم و چون باد مضمرم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون روشن است چشم جهان از وجود من</p></div>
<div class="m2"><p>تاری چرا شود ز تو این چشم اخترم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در عیش اگر کم آمدم از طبع ناخوشست</p></div>
<div class="m2"><p>در علم هر زمان به تفکر فزون‌ترم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زان کز برای دیدن گلهای معرفت</p></div>
<div class="m2"><p>در باغ فکر دیده گشاده چو عبهرم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ملک خرد چو نیست مقرر به نام من</p></div>
<div class="m2"><p>هستم ذلیل گر ملک هفت کشورم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از شرم آفتاب رخ خاک زرد شد</p></div>
<div class="m2"><p>بادی گرفت در سر یعنی که من زرم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اوتاد هفت کشور اگر کان زر شوند</p></div>
<div class="m2"><p>همت در آن نبندم و جز خاک نشمرم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گشتم غلام همت خویش از برای آنک</p></div>
<div class="m2"><p>با روشنان چرخ به همت برابرم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چرخ ار نمود بر چمن باغ روزگار</p></div>
<div class="m2"><p>بی‌بار چون چنارم و بی‌بر چو عرعرم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در صفهٔ دل از پی آزادی جهان</p></div>
<div class="m2"><p>هر ساعتی بساط قناعت بگسترم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>روح آرزو کند که چون این چرخ لاجورد</p></div>
<div class="m2"><p>بندد ز اختران خردبخش زیورم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>لیکن چو زهره بر شرف چرخ چون شوم</p></div>
<div class="m2"><p>کز باد و خاک و آتش و آبست پیکرم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا از حد جهان ننهم پای خود برون</p></div>
<div class="m2"><p>گردون به بندگی ننهد دست بر سرم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>حوران همه گشاده نقاب از جمال خویش</p></div>
<div class="m2"><p>من چون خیال بستهٔ تمثال آزرم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در آرزوی لفظ فلکسای من جهان</p></div>
<div class="m2"><p>بر فرق خود نهاده ز افلاک منبرم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>با من سپهر آینه کردار چند بار</p></div>
<div class="m2"><p>گفت این سخن ولیک نمی‌گشت باورم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گیرم کنون چو صبح گریبان آسمان</p></div>
<div class="m2"><p>در عالم خیال چه باشد چو بنگرم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در مکتب ادب ز ورای خرد، نهاد</p></div>
<div class="m2"><p>استاد غیب تختهٔ تهدید در برم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون خواستم که ثبت کنم بر بیاض دل</p></div>
<div class="m2"><p>فهرست نه فلک ز خرد کرد مسطرم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>داند که از مکارم اخلاق در صفا</p></div>
<div class="m2"><p>چون طوبی از بهشتم و چون جان ز کشورم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بر کارگاه پنج حواس و چهار طبع</p></div>
<div class="m2"><p>با دست کار گردش چرخ مدورم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از من بدی نیامد و ناید ز من بدی</p></div>
<div class="m2"><p>کز عنصر لطیف وز پاکیزه گوهرم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بر آسمان مکرمت از روشنان علم</p></div>
<div class="m2"><p>چون مشتری به نور خرد سعد اکبرم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از بهر دیدنم همه تن چشم شد فلک</p></div>
<div class="m2"><p>چون بنگرم به عقل فلک را چو دلبرم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در دیدهٔ جهان ز لطافت چو لعبتم</p></div>
<div class="m2"><p>بر تارک زمان ز فصاحت چو افسرم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>در آشیان عقل چو عنقای مغربم</p></div>
<div class="m2"><p>بر آسمان فضل چو خورشید ازهرم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>روحست هم عنانم اگرچه مرکبم</p></div>
<div class="m2"><p>عقل است هم‌نشینم اگرچه مصورم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در مجلس مذاکره علمست مونسم</p></div>
<div class="m2"><p>در منزل محاوره فضلست رهبرم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>از خلق روزگار نیاید چو من پسر</p></div>
<div class="m2"><p>در پرده‌ام چه دارد آخر نه دخترم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>از اختران فضل چو مهرم جدا کنند</p></div>
<div class="m2"><p>در پردهٔ جهان چو حوادث مسترم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>داند یقین که از نظر آفتاب عقل</p></div>
<div class="m2"><p>در چشم کان فضل چو یاقوت احمرم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>در دانشی که آن خردم را زیان شدست</p></div>
<div class="m2"><p>بر آسمان جان چو عطارد سخنورم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گلهای بوستان سخن را چو گلبنم</p></div>
<div class="m2"><p>عنقای آشیان خرد را چو شهپرم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از باغ فضل با لطف دستهٔ گلم</p></div>
<div class="m2"><p>وز بحر طبع با صدف لؤلؤ ترم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ماه سخن شده است ز من روشن ای عجب</p></div>
<div class="m2"><p>گویی بر آسمان سخن چشمهٔ خورم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>زاول به پای فکر شدم در جهان علم</p></div>
<div class="m2"><p>تا مضمر آنچه بود کنون گشت مظهرم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بر من چو باز شد در بستان‌سرای جان</p></div>
<div class="m2"><p>زین نظم جانفزای جهان گشت چاکرم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بادهٔ لطیف نظم مرا بین که کلک چون</p></div>
<div class="m2"><p>سرمست می‌خرامد بر روی دفترم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>معشوق دلبرم چو خط دلبرم بدید</p></div>
<div class="m2"><p>سوگند خورد و گفت به زلف معنبرم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کز خط روزگار چنین خط دلربای</p></div>
<div class="m2"><p>پیدا نشد ز عارض خورشید پیکرم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>با این کفایت و هنرم در نهاد عمر</p></div>
<div class="m2"><p>اسباب یک مراد نگردد میسرم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>هم بگذرد مدار غم ای جان چو عاقبت</p></div>
<div class="m2"><p>بگذارم این سرای مجازی و بگذرم</p></div></div>