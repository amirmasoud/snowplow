---
title: >-
    قصیدهٔ شمارهٔ ۱۶۷ - در مدح صدراعظم زین‌الدین عبد الله  و شکر صحت یافتن او از بیماری
---
# قصیدهٔ شمارهٔ ۱۶۷ - در مدح صدراعظم زین‌الدین عبد الله  و شکر صحت یافتن او از بیماری

<div class="b" id="bn1"><div class="m1"><p>از محاق قضا برون شد ماه</p></div>
<div class="m2"><p>وز عرای خطر برون شد شاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز فراش عافیت طی کرد</p></div>
<div class="m2"><p>بستری غم‌فزای و شادی‌کاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز برداشت وهن ملت و ملک</p></div>
<div class="m2"><p>باز بفزود قدر مسند و گاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زینت ملک پادشاه جهان</p></div>
<div class="m2"><p>زین دین خدای عبدالله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه از دامن جلالت اوست</p></div>
<div class="m2"><p>دست تاثیر آسمان کوتاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وانکه در طول و عرض همت اوست</p></div>
<div class="m2"><p>رای سلطان اختران گمراه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش پاسش قضا گشاده کمر</p></div>
<div class="m2"><p>پیش قدرش قدر نهاده کلاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز بی حرز دولتش تیهو</p></div>
<div class="m2"><p>شیر بی‌طوق طاعتش روباه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وانکه از چتر دولتش آموخت</p></div>
<div class="m2"><p>عکس مهتاب شکل خرمن ماه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عزمش از سر اختران منهی</p></div>
<div class="m2"><p>حزمش از راز روزگار آگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه از رای روشنش بگزارد</p></div>
<div class="m2"><p>نور خورشید وام سایهٔ چاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عرصهٔ همتش چو گنبد چرخ</p></div>
<div class="m2"><p>یک جهان خیمه دارد و خرگاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای ز رسم تو پر سمر اقوال</p></div>
<div class="m2"><p>وی ز شکر تو پر شکر افواه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آسمانت زمین طارم قدر</p></div>
<div class="m2"><p>وافتابت نگین خاتم جاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زین سپس در حمایت جاهت</p></div>
<div class="m2"><p>طاعت کهربا ندارد کاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حرمی شد حمایت تو چنانک</p></div>
<div class="m2"><p>باشد از آفتاب و سایه پناه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ملک را ز آفتاب رای تو هست</p></div>
<div class="m2"><p>ابدالدهر بامداد پگاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جز به درگاه عالی تو فلک</p></div>
<div class="m2"><p>ننبشته است عبده و فداه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جز به عین رضا نخواهد کرد</p></div>
<div class="m2"><p>دیدهٔ روزگار در تو نگاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شد مطیع ترا زمانه مطیع</p></div>
<div class="m2"><p>شد سپاه ترا ستاره سپاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هست بر وقف‌نامهٔ شرفت</p></div>
<div class="m2"><p>نه سپهر و چهار طبع گواه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خشم و خصم تو آتشست و حشیش</p></div>
<div class="m2"><p>مهر و کین تو طاعتست و گناه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر دماند ز شعلهٔ آتش</p></div>
<div class="m2"><p>فتح باب کف تو مهر گیاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کرده‌ای از دراز دستی جود</p></div>
<div class="m2"><p>از جهان دست خواستن کوتاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در هنر خود چنین تواند بود</p></div>
<div class="m2"><p>بشری لا اله الا الله</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای به تو زنده سنت پاداش</p></div>
<div class="m2"><p>وی ز تو تازه رسم باد افراه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بنده زین سقطهٔ چو آتش تیز</p></div>
<div class="m2"><p>بر سر آتش است بی‌گه و گاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حاش لله چو روز سقطهٔ تو</p></div>
<div class="m2"><p>شب گیتی نزاد روز سیاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شکر ایزد که باز روشن شد</p></div>
<div class="m2"><p>به تو صدر وزیر و حضرت شاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نشد از سقطه قربتت ساقط</p></div>
<div class="m2"><p>بلکه بفزود بر یکی پنجاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا کند اختلاف جنبش چرخ</p></div>
<div class="m2"><p>نقش بی‌رنگ روزگار تباه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هرکه نبود به روزگار تو شاد</p></div>
<div class="m2"><p>روزگارش مباد نیکی خواه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>امر و نهیت روان چو حکم قضا</p></div>
<div class="m2"><p>بر نشابور و مرو و بلخ و هراه</p></div></div>