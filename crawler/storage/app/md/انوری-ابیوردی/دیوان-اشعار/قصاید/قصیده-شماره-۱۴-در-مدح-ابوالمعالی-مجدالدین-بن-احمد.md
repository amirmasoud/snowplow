---
title: >-
    قصیدهٔ شمارهٔ ۱۴ - در مدح ابوالمعالی مجدالدین بن احمد
---
# قصیدهٔ شمارهٔ ۱۴ - در مدح ابوالمعالی مجدالدین بن احمد

<div class="b" id="bn1"><div class="m1"><p>ای از کمال حسن تو جزوی در آفتاب</p></div>
<div class="m2"><p>خطت کشیده دائرهٔ شب بر آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف چو مشک ناب ترا بنده مشک ناب</p></div>
<div class="m2"><p>روی چو آفتاب ترا چاکر آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنجا که زلف تست همه یکسره شب است</p></div>
<div class="m2"><p>وانجا که روی تست همه یکسر آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باغیست چهره تو که دارد ستاره‌بار</p></div>
<div class="m2"><p>سرویست قامت تو که دارد بر آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر ماه مشک داری و بر سرو بوستان</p></div>
<div class="m2"><p>در لاله نوش داری و در عنبر آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر حور و آفتاب نهم نام تو رواست</p></div>
<div class="m2"><p>کاندر کنار حوری و اندر بر آفتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از چهره آفتابی و از بوسه شکری</p></div>
<div class="m2"><p>بس لایق است با شکرت همبر آفتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>انگیختست حسن تو گل با مه تمام</p></div>
<div class="m2"><p>وامیخته است لفظ تو با شکر آفتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نایب سپهر نشد زلف تو چرا</p></div>
<div class="m2"><p>در حلقه ماه دارد و در چنبر آفتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خالیست بر رخ تو بنامیزد آنچنانک</p></div>
<div class="m2"><p>خواهد همی به خوبی ازو زیور آفتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گویی که نوک خامهٔ دستور پادشاه</p></div>
<div class="m2"><p>ناگه ز مشک شب نقطی زد بر آفتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مخدوم ملک‌پرور و صدر جهان که هست</p></div>
<div class="m2"><p>در پیش بارگاهش خدمتگر آفتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرزانه مجد دولت و دین کز برای فخر</p></div>
<div class="m2"><p>داد ز رای روشن او رهبر آفتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عالی ابوالمعالی بن احمد آنکه اوست</p></div>
<div class="m2"><p>از مخبر آسمانی و از منظر آفتاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لشکرکشی که هستش لشکرگه آسمان</p></div>
<div class="m2"><p>فرمان‌دهی که هستش فرمانبر آفتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر طالع قویش دعاگوی مشتری</p></div>
<div class="m2"><p>بر طلعت شهیش ثناگستر آفتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر صبحدم بسوزد بهر بخور او</p></div>
<div class="m2"><p>مشک سیاه شب را در مجمر آفتاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کامل ز ذات اوست خردپرور آدمی</p></div>
<div class="m2"><p>قاهر ز جود اوست گهرپرور آفتاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر منبری که خطبهٔ مدحش ادا کنند</p></div>
<div class="m2"><p>بوسد ز فخر پایهٔ آن منبر آفتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زیبد زمانه را که کند بهر مدح او</p></div>
<div class="m2"><p>خامه شهاب و نقش شب و دفتر آفتاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای صاحبی که دایم بر آسمان ملک</p></div>
<div class="m2"><p>دارد ز رای روشن تو مفخر آفتاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای از محل چنانکه زهر آفریده جان</p></div>
<div class="m2"><p>وی از شرف چنانکه زهر اختر آفتاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آنجا برد که رای تو باشد دل آسمان</p></div>
<div class="m2"><p>و آنجا نهد که پای تو باشد سر آفتاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از گرد موکب تو کشد سرمه حور عین</p></div>
<div class="m2"><p>وز ماه رایت تو کند افسر آفتاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نام شب از صحیفهٔ ایام بسترد</p></div>
<div class="m2"><p>از رای تو اجازت یابد گر آفتاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر عزم آنکه ریزد خون عدوی تو</p></div>
<div class="m2"><p>هر روز بامداد کشد خنجر آفتاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا کیمیای خاک درت بر نیفکند</p></div>
<div class="m2"><p>در صحن هیچ کان ننهد گوهر آفتاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سیمرغ صبح را ندهد مژدهٔ صباح</p></div>
<div class="m2"><p>تا نام تو نبندد بر شهپر آفتاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون تیغ نصرت تو برآرد سر از نیام</p></div>
<div class="m2"><p>گویی همی برآید از خاور آفتاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>با بندگانت پای ندارند سرکشان</p></div>
<div class="m2"><p>میرد سپاه شب چو کشد لشکر آفتاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آنجا که رزم جویی و لشکرکشی به فتح</p></div>
<div class="m2"><p>در بحر خون بتابد بی‌معبر آفتاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از تف و تاب خنجر مردان لشکرت</p></div>
<div class="m2"><p>در سر کشد به شکل زنان چادر آفتاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای آفتاب دولت عالیت بی‌زوال</p></div>
<div class="m2"><p>وی در ضمیر روشن تو مضمر آفتاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ای چاکری جاه ترا لایق آسمان</p></div>
<div class="m2"><p>وی بندگی رای ترا در خور آفتاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر شعر آفتاب که نبود بر این نمط</p></div>
<div class="m2"><p>خصمی کند هر آینه در محشر آفتاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آیینه‌ای که جلوه‌گه روی تو بود</p></div>
<div class="m2"><p>می‌زیبدش هر آینه خاکستر آفتاب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نشگفت اگر نویسد این شعر انوری</p></div>
<div class="m2"><p>بر روی روزگار به آب زر آفتاب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا نوبهار سبز بود و آسمان کبود</p></div>
<div class="m2"><p>تا لاله سایه جوید و نیلوفر آفتاب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سر سبز باد ناصحت از دور آسمان</p></div>
<div class="m2"><p>پژمرده لاله‌وار حسودت در آفتاب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در جشن آسمان‌وش تو ریخته به ناز</p></div>
<div class="m2"><p>ساقی ماه روی تو در ساغر آفتاب</p></div></div>