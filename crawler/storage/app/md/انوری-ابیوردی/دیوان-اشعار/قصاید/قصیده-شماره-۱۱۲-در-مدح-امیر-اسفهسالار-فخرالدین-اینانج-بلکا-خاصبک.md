---
title: >-
    قصیدهٔ شمارهٔ ۱۱۲ - در مدح امیر اسفهسالار فخرالدین اینانج بلکا خاصبک
---
# قصیدهٔ شمارهٔ ۱۱۲ - در مدح امیر اسفهسالار فخرالدین اینانج بلکا خاصبک

<div class="b" id="bn1"><div class="m1"><p>ای سپاهت را ظفر لشکرکش و نصرت یزک</p></div>
<div class="m2"><p>نه یقین بر طول و عرض لشکرت واقف نه شک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسته گرد موکبت صد پرده بر روی سماک</p></div>
<div class="m2"><p>کرده نعل مرکبت صد رخنه در پشت سمک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکجا حزم تو ساکن موج فوجی از ملوک</p></div>
<div class="m2"><p>هرکجا عزم تو جنبان جوشی جیشی از ملک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون رکاب تو گران گردد عنان تو سبک</p></div>
<div class="m2"><p>روز هیجا ای سپاهت انجم و میدان فلک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قابل تکبیر فتح از آسمان گوید که هین</p></div>
<div class="m2"><p>القتال ای حیدر ثانی که النصرة معک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیر چرخ از بیم شیر رایتت افغان‌کنان</p></div>
<div class="m2"><p>کالامان ای فخر دین اینانج بلکا خاصبک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمهٔ تیغ تو هم پر آب و هم پر آتش است</p></div>
<div class="m2"><p>چشمه‌ای دیدی میان آب و آتش مشترک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان و جاه خصم سوزان و گدازان روز و شب</p></div>
<div class="m2"><p>چون به آتش در حشیش و چون به آب اندر نمک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فتنه را رایت نگون کن هین که اقرار قضا</p></div>
<div class="m2"><p>ایمنی را تا قیامت کرد بر تیغ تو چک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر ترا یزدان بزرگی داد و راضی نیست خصم</p></div>
<div class="m2"><p>خصم را گو دفتر تقدیر باید کرد حک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عالم و آدم نبودستند کاندر بدو کار</p></div>
<div class="m2"><p>زید از اهل درج شد عمرو از اهل درک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور به یزدان اقتدا کردست سلطان واجبست</p></div>
<div class="m2"><p>شاه والا برنهد چون حق نکو کردست دک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حذ و قدر بندگان نیکو شناسد پادشاه</p></div>
<div class="m2"><p>خود تفاوت در عیار زر که داند جز محک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پایهٔ قدرت نشان می‌خواست گردون از قضا</p></div>
<div class="m2"><p>گفت آنک زآفرینش پاره‌ای آنسوترک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ملک بخشاینده در حرمان میمون خدمتت</p></div>
<div class="m2"><p>چون خلافت بی‌علی بودست و بی‌زهرا فدک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آسمان از مجلست بفکندش از روی حسد</p></div>
<div class="m2"><p>تا ز ناکامی نفس در حلق او شد چون خسک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>او به تاراج قضا در چون غنیمت در مصاف</p></div>
<div class="m2"><p>زو صبایع در جدل کان جز ولی آن عضو لک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پای چون هیزم شکسته دل چو آتش بی‌قرار</p></div>
<div class="m2"><p>مانده در اطوارد و دودم چو ماهی در شبک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دوستان با یک جگر پر خون که اینک قد مضی</p></div>
<div class="m2"><p>دشمنان با یک دهن پر خنده کانک قد هلک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آسمان خود سال و مه با بنده این دستان کند</p></div>
<div class="m2"><p>در دی اش با خیش دارد در تموزش با فنک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شکر یزدان را که این یک دست بوسش داد دست</p></div>
<div class="m2"><p>تا کند خار سپهر از پای بیرون یک به یک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا نباشد همچو عنقا خاصه در عزلت غراب</p></div>
<div class="m2"><p>تا نباشد همچو شاهین خاصه در قدرت کرک</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جان خصم از تیر سیمرغ افکنت بر شاخ عمر</p></div>
<div class="m2"><p>باد لرزان در برش چون جان گنجشک از پفک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ساختت از شاعران پر اخطل و فضل و جریر</p></div>
<div class="m2"><p>مجلست از ساقیان پر اخطی و رای و یمک</p></div></div>