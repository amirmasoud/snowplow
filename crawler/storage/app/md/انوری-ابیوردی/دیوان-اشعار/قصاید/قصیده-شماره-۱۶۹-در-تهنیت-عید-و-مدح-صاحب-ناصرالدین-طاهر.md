---
title: >-
    قصیدهٔ شمارهٔ ۱۶۹ - در تهنیت عید و مدح صاحب ناصرالدین طاهر
---
# قصیدهٔ شمارهٔ ۱۶۹ - در تهنیت عید و مدح صاحب ناصرالدین طاهر

<div class="b" id="bn1"><div class="m1"><p>ای سراپردهٔ سپید و سیاه</p></div>
<div class="m2"><p>ای بلند آفتاب و والا ماه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعلهٔ صبح روزگار دو رنگ</p></div>
<div class="m2"><p>در زد آتش به آسمان دوتاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از افق برکشید شیر علم</p></div>
<div class="m2"><p>در جهان اوفتاد شور سپاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هین که برکرد مرغ و ماهی را</p></div>
<div class="m2"><p>شغب از خوابگاه و خلوتگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد یکی را سبک عنان شتاب</p></div>
<div class="m2"><p>دیگری را گران رکاب شناه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بخار بحار کله ببند</p></div>
<div class="m2"><p>وی عروس بهار حله بخواه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای مرصع دوات و مصری کلک</p></div>
<div class="m2"><p>وی همایون بساط و میمون‌گاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز عیدست و تهنیت شرطست</p></div>
<div class="m2"><p>عید را تهنیت کنند به گاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به ملاقات بزم صاحب عصر</p></div>
<div class="m2"><p>به زمین بوس صدر ثانی شاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناصرالدین که نوک خامهٔ اوست</p></div>
<div class="m2"><p>چهره‌پرداز نصر دین اله</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طاهربن المظفر آنکه ظفر</p></div>
<div class="m2"><p>جز پی رایتش نداند راه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنکه در زیر سایهٔ عدلش</p></div>
<div class="m2"><p>طاعت کهربا ندارد کاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وانکه در جنب سایهٔ قدرش</p></div>
<div class="m2"><p>خواجهٔ اختران نجوید جاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وانکه او یونس است و گردون حوت</p></div>
<div class="m2"><p>وانکه او یوسف است و گیتی چاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رای او را مگر ملاقاتی</p></div>
<div class="m2"><p>خواست افتاد با فلک ناگاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اتفاقا به وجه گستاخی</p></div>
<div class="m2"><p>سوی او آفتاب کرد نگاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هرچه این می‌گشاد بند قبا</p></div>
<div class="m2"><p>آن فرو می‌کشید پر کلاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای غلامت به طبع بی‌اجبار</p></div>
<div class="m2"><p>وی مطیعت به طوع بی‌اکراه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرچه در زیر دور چرخ کبود</p></div>
<div class="m2"><p>هرچه بر پشت جرم خاک سیاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قدرتت گشته در ازاء قدر</p></div>
<div class="m2"><p>حملهٔ شیر و حیلهٔ روباه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دست عدلی دراز کردستی</p></div>
<div class="m2"><p>هم به پاداش و هم به بادافراه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که نه بس روزگار می‌باید</p></div>
<div class="m2"><p>ای قضاه قهر روزگار پناه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا کنی از تصرفات زمین</p></div>
<div class="m2"><p>دست تاثیر آسمان کوتاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عدل دایم بود گواه دوام</p></div>
<div class="m2"><p>بر دوام تو عدل تست گواه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فتنه در عهد حزم تو نزدست</p></div>
<div class="m2"><p>یک نفس خالی از دوکار آگاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دهر در دور دست تو نگذاشت</p></div>
<div class="m2"><p>هفت اقلیم را دو حاجت خواه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دست تو فتح باب بارانیست</p></div>
<div class="m2"><p>که برآرد ز شوره مهر گیاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای خلایق به جمله جزو و توکل</p></div>
<div class="m2"><p>و آفرینش همه پیادهٔ تو شاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه خدایی و داشتست خدای</p></div>
<div class="m2"><p>جاودانت از شریک و شبه نگاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شبهت از خواب و آب و آینه خاست</p></div>
<div class="m2"><p>ورنه آزاد بودی از اشباه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زین فراتر نمی‌توانم شد</p></div>
<div class="m2"><p>خاطرم تیره شد دماغ تباه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عاجزم در ثنای تو عاجز</p></div>
<div class="m2"><p>آه اگر همچنین بمانم آه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یک دلیری کنم قرینهٔ شرک</p></div>
<div class="m2"><p>نکنم لا اله الا الله</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تاکه ذکر گناه و طاعت هست</p></div>
<div class="m2"><p>سال و ماه اوفتاده در افواه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از مقامات بندگی خدای</p></div>
<div class="m2"><p>هرچه جز طاعت تو باد گناه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سوی تدبیر تو نوشته قضا</p></div>
<div class="m2"><p>گاه تقدیر عبده و فداه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همتت ملک‌بخش و ملک‌ستان</p></div>
<div class="m2"><p>دولتت دوستکام و دشمن‌کاه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یک نفس حاسدان بی‌نفست</p></div>
<div class="m2"><p>برنیاورده جز که وا اسفاه</p></div></div>