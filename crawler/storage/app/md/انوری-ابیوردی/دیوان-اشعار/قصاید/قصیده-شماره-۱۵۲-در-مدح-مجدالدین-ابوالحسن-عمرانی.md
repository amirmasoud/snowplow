---
title: >-
    قصیدهٔ شمارهٔ ۱۵۲ - در مدح مجدالدین ابوالحسن عمرانی
---
# قصیدهٔ شمارهٔ ۱۵۲ - در مدح مجدالدین ابوالحسن عمرانی

<div class="b" id="bn1"><div class="m1"><p>ای جهان را جمال و جاه تو زین</p></div>
<div class="m2"><p>اسم و رسم تو اسم و رسم حسین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در و دست تو مقصد آمال</p></div>
<div class="m2"><p>دل و طبع تو مجمع‌البحرین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عرصهٔ همتت چنان واسع</p></div>
<div class="m2"><p>که در آن عرصه گم شود کونین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نزد عهدت وفا برابر دین</p></div>
<div class="m2"><p>پیش طبعت عطا برابر دین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حال من بنده و حوالت من</p></div>
<div class="m2"><p>گشت آب حیات و ذوالقرنین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای چو الیاس و خضر بر سر کار</p></div>
<div class="m2"><p>عزم تزویج کن مگو من این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انتظارم مده بده ز کرم</p></div>
<div class="m2"><p>گر همه نقد نیست بین‌البین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من نگویم که می‌نخواهم جنس</p></div>
<div class="m2"><p>تو مگو نیز من ندارم عین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خود چو معطی تویی و سایل من</p></div>
<div class="m2"><p>بیش از این عشوه شین باشد شین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای چو سیمرغ جفت استغنا</p></div>
<div class="m2"><p>بیش از این باش با غراب‌البین</p></div></div>