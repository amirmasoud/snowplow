---
title: >-
    قصیدهٔ شمارهٔ ۸۸
---
# قصیدهٔ شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>ای روزگار دولت تو روز روزگار</p></div>
<div class="m2"><p>وی بر زمانه سایهٔ تو فضل کردگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قادر به حکم بر همه‌کس آسمان صفت</p></div>
<div class="m2"><p>فائض به جود بر همه خلق آفتاب‌وار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حزم تو دام و دانهٔ امروز دیده دی</p></div>
<div class="m2"><p>جود تو نقد و نسیهٔ امسال داده پار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افلاک را به عز و جلال تو اهتزاز</p></div>
<div class="m2"><p>وایام را به جاه و جمال تو افتخار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آب تف هیبت تو برکشد دخان</p></div>
<div class="m2"><p>وز سنگ جذب همت تو برکشد بخار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا سد حزم تو نکشیدند در وجود</p></div>
<div class="m2"><p>عالم نیافت عافیت عام را حصار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقلی گه ذکا و سحابی گه سخا</p></div>
<div class="m2"><p>بحری گه کفایت و کوهی گه وقار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم عقل پیش نطق تو شخصی است بی‌روان</p></div>
<div class="m2"><p>هم نطق پیش کلک تو نقدیست کم‌عیار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ابر اگر ز دست تو یک خاصیت نهند</p></div>
<div class="m2"><p>دست تهی برون ندمد هرگز از چنار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا در ضمان رزق خلایق نشد کفت</p></div>
<div class="m2"><p>ترکیب معده را نه به پیوست پود و تار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حکم تو همچو باد دهد خاک را مسیر</p></div>
<div class="m2"><p>علم تو همچو خاک دهد باد را قرار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نی چرخ را به سرعت امر تو ره‌نورد</p></div>
<div class="m2"><p>نه وهم را به پایهٔ قدر تو رهگذار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از خاک زور بازوی امرت برد شکیب</p></div>
<div class="m2"><p>وز آب نعل مرکب عزمت کند غبار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنجا که یک پیاده فرو کرد عزم تو</p></div>
<div class="m2"><p>ملکی توان گرفت به نیروی یک سوار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مهر تو دوستان را در دل شکفته گل</p></div>
<div class="m2"><p>کین تو دشمنان را در جهان شکسته خار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون مور هرکه با کمر طاعت تو نیست</p></div>
<div class="m2"><p>بیرون کشد قضای بد از پوستش چو مار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هم غور احتیاط ترا دهر در جوال</p></div>
<div class="m2"><p>هم اوج بارگاه ترا چرخ در جوار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چندین سوابق از پی کام تو آفرید</p></div>
<div class="m2"><p>از تر و خشک عالم خاک آفریدگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ورنه چو ذات کامل تو کل عالمست</p></div>
<div class="m2"><p>کردی بر آفرینش ذات تو اختصار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا نیست اختران را آسایش از مسیر</p></div>
<div class="m2"><p>تا نیست آسمان را آرامش از مدار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بادا مسیر امر تو چون چرخ بی‌فتور</p></div>
<div class="m2"><p>بادا مدار عمر تو چون دور بی‌شمار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هم فتنه را به دست شکوه تو گوشمال</p></div>
<div class="m2"><p>هم چرخ را ز نعل سمند تو گوشوار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو بر سریر رفعت و اعدا چو خاک پست</p></div>
<div class="m2"><p>تو در مقام عزت و حاسد چو خاک خوار</p></div></div>