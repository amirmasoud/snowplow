---
title: >-
    قصیدهٔ شمارهٔ ۱۴۹ - در مدح مجدالدین ابوالحسن عمرانی
---
# قصیدهٔ شمارهٔ ۱۴۹ - در مدح مجدالدین ابوالحسن عمرانی

<div class="b" id="bn1"><div class="m1"><p>درآمد موکب عید همایون</p></div>
<div class="m2"><p>که بر صاحب مبارک باد و میمون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپهر مجد مجدالدین که شاهان</p></div>
<div class="m2"><p>ز مجدش ملک را کردند قانون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عدو بندی که کلکش در دهاده</p></div>
<div class="m2"><p>کند گل را ز خون فتنه گلگون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بکاهد وقت خشمش عمر در مرگ</p></div>
<div class="m2"><p>بغلطد گاه کینش مرگ در خون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازو دشمن چو دارا از سکندر</p></div>
<div class="m2"><p>ازو حاسد چو ضحاک از فریدون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهی جود از تو در قوت چو قارن</p></div>
<div class="m2"><p>زهی آز از تو در نعمت چو قارون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عتابش بر زمین بارد صواعق</p></div>
<div class="m2"><p>نهیبش بر زبان آرد شبیخون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امیران تو جباران گیتی</p></div>
<div class="m2"><p>مطیعان تو بیداران گردون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمانه تیره و رای تو روشن</p></div>
<div class="m2"><p>خلایق تشنه و دست تو جیحون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غلط را سوخت حکمت بر در سهو</p></div>
<div class="m2"><p>چرا را کشت امرت بر در چون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه عالی همتی یارب که هردم</p></div>
<div class="m2"><p>یکی در آفرینش بینی افزون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندادی دل به دنیی و به عقبی</p></div>
<div class="m2"><p>نبستی وهم در والا و در دون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قضا تدبیر دور چرخ می‌کرد</p></div>
<div class="m2"><p>که بر ذات تو گشت اقبال مفتون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قدر ساز وجود دهر می‌ساخت</p></div>
<div class="m2"><p>که بر عرش تو شد اقبال مقرون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو گیرد آتش خشم تو بالا</p></div>
<div class="m2"><p>نیابد از دو عالم نیم کانون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو از تو بگذری نزدیک آن قوم</p></div>
<div class="m2"><p>نبیند کس مگر محرور و مدفون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه خیزد آخر از قومی که هستند</p></div>
<div class="m2"><p>غلام آلتی مولای التون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به مردی و مروت کی رسیدند</p></div>
<div class="m2"><p>در انگشت تو این یک مشت مرهون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در آن موقف که از مصروع پیکار</p></div>
<div class="m2"><p>زبان رمح گردان خواند افسون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رساند آتش کوشش حرارت</p></div>
<div class="m2"><p>به ایوان مسیح و جیش ذوالنون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز پشته پشته گشته ناظران را</p></div>
<div class="m2"><p>نماید کوه کوه اطراف هامون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز اشک بیدل و خون دلاور</p></div>
<div class="m2"><p>همه میدان کنی جیحون و سیحون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خداوندا ز مدح تست حاصل</p></div>
<div class="m2"><p>رخ رنگ مرا رنگ طبر خون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شنیدستم که پیش تخت اعلی</p></div>
<div class="m2"><p>بزرگی خواند شعر قافیه خون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه بر وجهی که باشد رونق او</p></div>
<div class="m2"><p>در آخر کرد ذکر آب و صابون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جهان داند که معزولی نیابد</p></div>
<div class="m2"><p>ربیع نطق را در ربع مسکون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هنوز از استماع شعر نیکوست</p></div>
<div class="m2"><p>خرد را گوش درج در مکنون</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سزای افتخار آن شعر باشد</p></div>
<div class="m2"><p>که افزون باشدش راوی موزون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز شعر باطل هر کس زبانم</p></div>
<div class="m2"><p>نمی‌گفته است حقی تا به‌اکنون</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همیشه تا که حسن و عشق باشد</p></div>
<div class="m2"><p>مثلها شاهد از لیلی و مجنون</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جناب دوستانت باد جنت</p></div>
<div class="m2"><p>طعام دشمنانت باد طاعون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شبت فرخنده و روزت خجسته</p></div>
<div class="m2"><p>خزانت خرم و عهدت همایون</p></div></div>