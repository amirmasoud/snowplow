---
title: >-
    قصیدهٔ شمارهٔ ۱۵۷ - در مدح ملک عضدالدین طغرل تکین
---
# قصیدهٔ شمارهٔ ۱۵۷ - در مدح ملک عضدالدین طغرل تکین

<div class="b" id="bn1"><div class="m1"><p>ای جهان را ایمنی از دولت طغرلتکین</p></div>
<div class="m2"><p>جاودان منصور بادا رایت طغرل تکین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نعمت انصاف عالم را ز عدل عام اوست</p></div>
<div class="m2"><p>کیست آنکو نیست اندر نعمت طغرل تکین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو و ظلمت از حضور و غیبت خورشید دان</p></div>
<div class="m2"><p>امن و تشویش از حضور و غیبت طغرل تکین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خسروان دل برقرار ملک آن گاهی نهند</p></div>
<div class="m2"><p>کاوردشان آسمان در بیعت طغرل تکین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پهلوانان دل ز جان و جاه آنگه برکنند</p></div>
<div class="m2"><p>کافکندشان روزگار از طاعت طغرل تکین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اختیار تاج و تختش نیست ورنه چیست کم</p></div>
<div class="m2"><p>از دگر شاهان شکوه و شوکت طغرل تکین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کو فریدون گو بیا نظاره کن اندر جهان</p></div>
<div class="m2"><p>تا ببینی خویشتن در نسبت طغرل تکین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملک اگر در دولت سنجر به آخر پیر شد</p></div>
<div class="m2"><p>شد جوان بار دگر در نوبت طغرل تکین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هفت کشور زیر فرمان کرد و هم نوبت سه زد</p></div>
<div class="m2"><p>صبر کن تا پنج گردد نوبت طغرل تکین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قدرت طغرل تکین نوعی است گویی از قدر</p></div>
<div class="m2"><p>بر جهان زان غالب آمد قدرت طغرل تکین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چرخ را گفتم دلیری می‌کنی در کارها</p></div>
<div class="m2"><p>گفت از خود نه ولی از صولت طغرل تکین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کهربا در کاه نتواند تصرف کرد نیز</p></div>
<div class="m2"><p>بی‌اجازت نامه‌ای از حضرت طغرل تکین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لشکر طغرل تکین بر هم زنندی خاک و آب</p></div>
<div class="m2"><p>گرنه ساکن داردیشان هیبت طغرل تکین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تنگ میدان ماندی فتح و نگون رایت طفر</p></div>
<div class="m2"><p>گر نباشندی طفیل نصرت طغرل تکین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از پی آسایش خلقست و آرام جهان</p></div>
<div class="m2"><p>هرچه هست از آلت و از عدت طغرل تکین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ورنه آخر ملک عالم کیست با این طول و عرض</p></div>
<div class="m2"><p>تا بدو مغرور گردد رغبت طغرل تکین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با خرد گفتم که بیرون سپهر احوال چیست</p></div>
<div class="m2"><p>گفت دانی از که پرس از همت طغرل تکین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باز گفتم عادت طغرل تکین در ملک چیست</p></div>
<div class="m2"><p>گفت انصافست و بخشش عادت طغرل تکین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رحمتی دیدی که جویای گنه باشد مدام</p></div>
<div class="m2"><p>رحمت یزدان‌شناس و رحمت طغرل تکین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حاجت از طغرل تکین شاید که خواهی بهر آنک</p></div>
<div class="m2"><p>جز به یزدان نیست هرگز حاجت طغرل تکین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیست کس را بر جهان منت جز او را گرچه نیست</p></div>
<div class="m2"><p>در عطا منت نهادن سیرت طغرل تکین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قربت طغرل تکین را نیکبختی لازمست</p></div>
<div class="m2"><p>نیک بختا انوری از قربت طغرل تکین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون خداوندی از این خدمت همی حاصل شود</p></div>
<div class="m2"><p>ما و زین پس آستان و خدمت طغرل تکین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر جهان چون سایهٔ ابرست و نور آفتاب</p></div>
<div class="m2"><p>بخشش بی‌وعده و بی‌منت طغرل تکین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون جهان از دولت طغرل تکین دارد نظام</p></div>
<div class="m2"><p>تا جهان باقیست بادا دولت طغرل تکین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مدت طغرل تکین چندان که دوران سپهر</p></div>
<div class="m2"><p>وام خواهد روزگار از مدت طغرل تکین</p></div></div>