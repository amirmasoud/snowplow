---
title: >-
    قصیدهٔ شمارهٔ ۲۰۵ - در مدح سید سادات
---
# قصیدهٔ شمارهٔ ۲۰۵ - در مدح سید سادات

<div class="b" id="bn1"><div class="m1"><p>با خاک در تو آشنایی</p></div>
<div class="m2"><p>خوشتر ز هزار پادشایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده رخ راز مه ببیند</p></div>
<div class="m2"><p>بر عارض تو ز روشنایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نکتهٔ طوطی لب تو</p></div>
<div class="m2"><p>سیمرغ گزید پارسایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جایی که زلب حیات بخشی</p></div>
<div class="m2"><p>عیسی بود از در گدایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهر تو و سینهٔ چو من کس</p></div>
<div class="m2"><p>طاوس و سرای روستایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خدمت عشق تست ما را</p></div>
<div class="m2"><p>دل عاریتی و جان بهایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بردی ز پری و آدمی هوش</p></div>
<div class="m2"><p>یک راه بگوی تا کرایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خانهٔ صبر فرقت تو</p></div>
<div class="m2"><p>افکند هزار بی‌نوایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دعوی حسن خود سخن‌گوی</p></div>
<div class="m2"><p>تا ماه دهد بر آن گوایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از کوی چو آفتاب از کوه</p></div>
<div class="m2"><p>در خدمت تاج دین برآیی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صورتگر عز پناه دولت</p></div>
<div class="m2"><p>معبرده دولت علایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن جان خرد که مر خرد را</p></div>
<div class="m2"><p>با طاعت اوست آشنایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در نسبت آن شرف توان دید</p></div>
<div class="m2"><p>چون فضل خدای در خدایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه چرخ گرفت و هفت اختر</p></div>
<div class="m2"><p>یک فکرت او به تیزپایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای دیدهٔ ناظر نبوت</p></div>
<div class="m2"><p>در ذات تو دیده مصطفایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون روی خلقت نخواندت عقل</p></div>
<div class="m2"><p>شاید که ز پشت مرتضایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خود عقل ترا کمال هرگز</p></div>
<div class="m2"><p>داند که ز جاه تا کجایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پیش در تو قبول کرده</p></div>
<div class="m2"><p>پیشانی سدره خاک پایی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرغ دل جبرئیل گیرد</p></div>
<div class="m2"><p>در مدحت تو سخن سرایی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اولاد بزرگ مرتضا را</p></div>
<div class="m2"><p>یارب چه بزرگ پیشوایی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کبر تو کم است و کبریا بیش</p></div>
<div class="m2"><p>از کبر نه‌ای ز کبریایی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن روز که عمر در غم مرگ</p></div>
<div class="m2"><p>معزول بود ز خوش لقایی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نیلوفر تیغ چشمها را</p></div>
<div class="m2"><p>چون لاله کند به کم بقایی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از نسبت فعل سایه گیرد</p></div>
<div class="m2"><p>در صدمت صور صوت نایی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از ساغر خوف تشنهٔ جنگ</p></div>
<div class="m2"><p>سیراب شود ز بی‌رجایی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جانهای مبارزان ز تنها</p></div>
<div class="m2"><p>بینند ز تیغ تو جدایی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این خاطر من ز غیبت تو</p></div>
<div class="m2"><p>محروم ز پادشا ستایی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دل در غم خدمت تو یک دم</p></div>
<div class="m2"><p>نایافته از عنا رهایی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا آمد مرگ جان غمگین</p></div>
<div class="m2"><p>گشته ز هوای تو هوایی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زنهار مرا مگو که رو رو</p></div>
<div class="m2"><p>تو در خور شهر و بوریایی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در غیبت تو خوش است ما را</p></div>
<div class="m2"><p>آن به که بدین طرف نیایی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آخر به طریق لطف یکبار</p></div>
<div class="m2"><p>بنویس که خیز چند پایی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در خدمت دیگران چه کوشی</p></div>
<div class="m2"><p>چون بندهٔ خاندان مایی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در جستن کرده گرد عالم</p></div>
<div class="m2"><p>گردنده چو سنگ آسیایی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در شکر علاء دین و دولت</p></div>
<div class="m2"><p>پیوسته چرا شکر نخایی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از حضرت ما که روی کونست</p></div>
<div class="m2"><p>دوری ز چه روی می‌نمایی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا فائدهٔ نبات یابند</p></div>
<div class="m2"><p>اشکال زمینی و سمایی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حکم تو گسسته باد یارب</p></div>
<div class="m2"><p>ار علت چونی و چرایی</p></div></div>