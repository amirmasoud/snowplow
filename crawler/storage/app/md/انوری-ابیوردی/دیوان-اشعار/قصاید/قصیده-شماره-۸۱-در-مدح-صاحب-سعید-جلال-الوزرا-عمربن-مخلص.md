---
title: >-
    قصیدهٔ شمارهٔ ۸۱ - در مدح صاحب سعید جلال‌الوزرا عمربن مخلص
---
# قصیدهٔ شمارهٔ ۸۱ - در مدح صاحب سعید جلال‌الوزرا عمربن مخلص

<div class="b" id="bn1"><div class="m1"><p>هندویی کز مژگان کرد مرا لاله قطار</p></div>
<div class="m2"><p>سوخت از آتش غم جان مرا هندووار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لاله راندن به دم و سوختن اندر آتش</p></div>
<div class="m2"><p>هندوان دست ببردند بدین هر دو نگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هندوانه دو عمل پیش گرفت او یارب</p></div>
<div class="m2"><p>داری از هر دو عمل یار مرا برخوردار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هندوان را چه اگر گرم و تر آمد به مزاج</p></div>
<div class="m2"><p>عشقشان در دل از آن گرمتر آمد صدبار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق هندو به همه حال بود سوزان‌تر</p></div>
<div class="m2"><p>که در انگشت بود عادت سوزانی نار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اتفاق فلکی بود و قضای ازلی</p></div>
<div class="m2"><p>عشق را بر سر من رفته یکایک سر و کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیدم از پنجرهٔ حجرهٔ نخاس او را</p></div>
<div class="m2"><p>او به کاشانه بد و من به میان بازار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم بر آن‌گونه که از پنجرهٔ ابر به شب</p></div>
<div class="m2"><p>رخ رخشندهٔ مه بیند مرد نظار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کشی و چابکیش دیدم و با خود گفتم</p></div>
<div class="m2"><p>اینت افسونگر هندو نسب جادو سار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به فسون بین که بدانگونه مسخر کردست</p></div>
<div class="m2"><p>هم به بالای خود از عنبر و از مشک دو مار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه دلال دو گیسوی پر از عطر ویست</p></div>
<div class="m2"><p>نیست دلال درین مرتبه هست او عطار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زنخش چیست یکی گوی بلورین در مشک</p></div>
<div class="m2"><p>ابرویش چیست دو چوگان طلی کرده نگار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دمچهٔ چشم کدامست و دماوند کدام</p></div>
<div class="m2"><p>حلقهٔ زلف کدامست و کدامست تتار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنکه آن حور که او را دل احرار بهشت</p></div>
<div class="m2"><p>وانکه آن بت که ورا جان عزیزان فرخار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گو بیا روی ببین اینک وانگه به دو دست</p></div>
<div class="m2"><p>زو نگهدار به دل و دین خود ای صومعه‌دار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من در آن صورت او عاجز و حیران مانده</p></div>
<div class="m2"><p>دیده در وی نگران و دل از اندیشه فکار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هندوانه عملی کرد وی و من غافل</p></div>
<div class="m2"><p>دلم از سینه برآورده و از فرق دمار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جادویی کردن جادو بچه آسان باشد</p></div>
<div class="m2"><p>نبود بط بچه را اشنهٔ دریا دشوار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون به ناگاه فرود آمد از آن حجره به شیب</p></div>
<div class="m2"><p>همچو کبکی که خرامنده شود از کهسار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پای من خشک فرومانده ز رفتار و مرا</p></div>
<div class="m2"><p>نیست بر خشک زمین پای من و گل ستوار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفتم ای رشک بتان عشق مبارک بادم</p></div>
<div class="m2"><p>که گرفتم غم عشق تو به صد مهر کنار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خنده می‌آمدش و بسته همی داشت دو لب</p></div>
<div class="m2"><p>کانچنان خنده نبینی ز گل هیچ بهار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفت اگر زر بودت عشق مبارک بادت</p></div>
<div class="m2"><p>که به زر پای رسد بر سر نجم سیار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از خداوند مرا گر بخری فردا شب</p></div>
<div class="m2"><p>برخوری از من و از وصل من اندوه مدار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفتم ار زر نبود پس چه بود تدبیرم</p></div>
<div class="m2"><p>گفت یک بدرهٔ زر فکر کن و ریش مخار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دلم از جا بشد ناگه و بخروشیدم</p></div>
<div class="m2"><p>جامه بدریدم و اشک از مژگان کرد نثار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نوحهٔ زار همی کردم و می‌گفتم وای</p></div>
<div class="m2"><p>اینت بی‌سیمی و با سیم همی آید یار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دلش از زاری و از نوحهٔ من باز بسوخت</p></div>
<div class="m2"><p>به نوازش بگشاد آن دو لب شکربار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفت مخروش ترا راه نمایم که چه کن</p></div>
<div class="m2"><p>رو بر خواجهٔ خود شعر برو سیم بیار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خواجهٔ عادل عالم خلف حاتم طی</p></div>
<div class="m2"><p>معطی دهر جلال‌الوزرا شمع دیار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آنکه آسان به کم از تو مثلا داده بود</p></div>
<div class="m2"><p>ده به از من به یکی راه ترا نه صدبار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه بسنجد چهل از من به جوی در چشمش</p></div>
<div class="m2"><p>نه بهای چو منی بگذرد از چل دینار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>رو میندیش که از بهر توام بخریدی</p></div>
<div class="m2"><p>به مثل قیمت من گر بگذشتی ز هزار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفتم ای دوست نکوراه نمودی تو ولی</p></div>
<div class="m2"><p>با خداوند کرا زهره از این سان گفتار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گفت لا حول و لا قوة الا بالله</p></div>
<div class="m2"><p>این چه گل بود که بشکفت میانش پرخار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>او چو برگشت و خرامان شد از آنجای وداع</p></div>
<div class="m2"><p>که نحوست کند از چرخ بر آنجای نثار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>درد بی‌سیمیم آورد به سوی خانه</p></div>
<div class="m2"><p>چو گنه کاری حاشا که برندش سوی دار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در ببستم بدو زنجیر هم از اول شب</p></div>
<div class="m2"><p>پشت کردم سوی در روی به سوی دیوار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گفتم امشب بسزا بر سر بی‌سیمی خویش</p></div>
<div class="m2"><p>تا گه صبح یکی ناله کنم زارازار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اشک راندم که همی غرقه شدی کشتی نوح</p></div>
<div class="m2"><p>آه کردم که همی خیمه بیفکندی نار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر شراری که برانداخت دل از روی رهی</p></div>
<div class="m2"><p>بر فلک دیدم رخشان شده انجم کردار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>من درین دمدمهٔ کار که سیمرغ سحر</p></div>
<div class="m2"><p>به یکی جوی پر از شیر فرو زد منقار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گرمی و تری آن شیر همانا که مرا</p></div>
<div class="m2"><p>به سوی مغز همان لحظه برآورد بخار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا زدم چشم ولی نعمت خود را دیدم</p></div>
<div class="m2"><p>بر نهالی به زر بر طرف صفهٔ بار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گفت ای انوری آخر چه فتادست ترا</p></div>
<div class="m2"><p>که فرو رفته‌ای و غمزده چون بوتیمار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>پیشتر رفتم و با خواجه به یکبار به شرح</p></div>
<div class="m2"><p>قصهٔ عشق کنیزک همه کردم تکرار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خوش بخندید و مرا گفت سیه‌کار کسی</p></div>
<div class="m2"><p>گفتم از خواجه سیه به نبود رنگ‌نگار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هم در آن لحظه بفرمود یکی را که برو</p></div>
<div class="m2"><p>بخر این بدره بیار و به ثناگوی سپار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>رفت و بخرید و بیاورد و به من بنده سپرد</p></div>
<div class="m2"><p>دست دلدار گرفتم شدم آنگه بیدار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نه ولی‌نعمت من بود و نه معشوقهٔ من</p></div>
<div class="m2"><p>راست من با تن خود خفته چو با سگ شنغار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>وز همه نادره‌تر آنکه عطا خواست عطا</p></div>
<div class="m2"><p>تا بر خواب گزارنده گرو شد دستار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ویحک ای چرخ منم مانده سری پر سودا</p></div>
<div class="m2"><p>از جهان این سر و سودا به من ارزانی دار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دور ادبار تو تا چند به پایان آرم</p></div>
<div class="m2"><p>دور اقبال اگر هست بیار ای دیار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ای کریمی و حلیمی که ز نسل آدم</p></div>
<div class="m2"><p>کرم و حلم ترا آمده بی‌استغفار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>از کریمی و حلیمی است که می بنیوشی</p></div>
<div class="m2"><p>نعرهٔ زاغ و زغن چون نغم موسیقار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گرچه از قصه درازی ببرد شیرینی</p></div>
<div class="m2"><p>کی بود از بر هفتاد ترش بوالغنجار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همه به قدر تو که کوتاه نخواهم کردن</p></div>
<div class="m2"><p>تا ببینم که دهی تا شب قدرم دیدار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ناز بنده که کشد جز که خداوند کریم</p></div>
<div class="m2"><p>ناز حسان که کشد جز که رسول مختار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>من برآنم که مدیح تو بخوانم برخاک</p></div>
<div class="m2"><p>تا شود خاک سیه کن‌فیکون زر عیار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>وانگهی زر بدهم کار چو زر خوب کنم</p></div>
<div class="m2"><p>بیش چون زر نکنم در طلب زر رخسار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>راست گویم چو کف راد گهربار تو هست</p></div>
<div class="m2"><p>منت زر شدن خاک سیاهم به چکار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آفتاب فلک‌آرای تو بر جای بود</p></div>
<div class="m2"><p>جای باشد که جهان را ز چراغ آید عار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تا به نزدیک سر و صدر اطبا آفاق</p></div>
<div class="m2"><p>عشق بیماری دل باشد و عاشق بیمار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دل من باد گرفتار چنین بیماری</p></div>
<div class="m2"><p>تو خداوند مرا داشته هردم تیمار</p></div></div>