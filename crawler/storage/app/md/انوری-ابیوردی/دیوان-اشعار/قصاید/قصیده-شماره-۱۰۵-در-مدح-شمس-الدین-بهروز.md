---
title: >-
    قصیدهٔ شمارهٔ ۱۰۵ - در مدح شمس‌الدین بهروز
---
# قصیدهٔ شمارهٔ ۱۰۵ - در مدح شمس‌الدین بهروز

<div class="b" id="bn1"><div class="m1"><p>ای بر اعدا و اولیا پیروز</p></div>
<div class="m2"><p>در مکافات این و آن‌شب و روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر یکی جود فایضت غالب</p></div>
<div class="m2"><p>وز دگر جاه قاهرت کین‌توز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بذل نزدیک همت تو چو وام</p></div>
<div class="m2"><p>کرمت وام تو ز شکر اندوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داده بی‌میل و کرده بی‌کینه</p></div>
<div class="m2"><p>دور این مایه‌ساز صورت‌سوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قالب دوستانت را دل شیر</p></div>
<div class="m2"><p>حال دشمنانت را سگ و یوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بحق هر دو تصرف تو</p></div>
<div class="m2"><p>مالک هر دوی بدر و بدفوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زانکه اقبال خویش را دیدم</p></div>
<div class="m2"><p>با رخ دلگشای جان‌افروز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتمش هان چگونه داری حال</p></div>
<div class="m2"><p>زیراین ورطه تاب حادثه‌سوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت ویحک خبر نداری تو</p></div>
<div class="m2"><p>که بگو بازگشت آخر گوز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حدثان کرد رای پای‌افزار</p></div>
<div class="m2"><p>آسمان گشت مرغ دست‌آموز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شب محنت به آخر آمد و شد</p></div>
<div class="m2"><p>شب من روز و روز من نوروز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روزم از روز بهترست اکنون</p></div>
<div class="m2"><p>از مراعات شمس دین بهروز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باد عمرش چو جاه روز افزون</p></div>
<div class="m2"><p>عمر اعداش عمر روز سپوز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حاسدانش همیشه سرگردان</p></div>
<div class="m2"><p>غم بر ایشان ز بخت بد فیروز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وقت بر آبریز سبلتشان</p></div>
<div class="m2"><p>آنکه گویند صوفیانش گوز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جاودان از فلک خطابش این</p></div>
<div class="m2"><p>کای بر اعداد و اولیا پیروز</p></div></div>