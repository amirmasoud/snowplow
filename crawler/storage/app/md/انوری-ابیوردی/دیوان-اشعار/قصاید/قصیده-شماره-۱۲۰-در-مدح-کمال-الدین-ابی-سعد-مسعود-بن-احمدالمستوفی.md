---
title: >-
    قصیدهٔ شمارهٔ ۱۲۰ - در مدح کمال‌الدین ابی‌سعد مسعود بن احمدالمستوفی
---
# قصیدهٔ شمارهٔ ۱۲۰ - در مدح کمال‌الدین ابی‌سعد مسعود بن احمدالمستوفی

<div class="b" id="bn1"><div class="m1"><p>خدای خواست که گیرد زمانه جاه و جلال</p></div>
<div class="m2"><p>جمال داد جهان را به جود و جاه و کمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپهر معنی مسعود کز قران سعود</p></div>
<div class="m2"><p>نزاد مادر گیتی چو او ستوده خصال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قضا توان و قدر قدرت و ستاره محل</p></div>
<div class="m2"><p>زمانه بخشش و کان دستگاه و بحر نوال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جنب قدر رفیعش مدار انجم پست</p></div>
<div class="m2"><p>به پیش رای مصیبش زبان حجت لال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نوک حامه ببندد ره قضا و قدر</p></div>
<div class="m2"><p>به تیر نکته بدوزد لب صواب و محال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ابر خاطر او قطره بر زمین بارد</p></div>
<div class="m2"><p>به جای برگ زبان بردمد ز شاخ نهال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو رای روشن او باشد آفتاب سپهر</p></div>
<div class="m2"><p>گر آفتاب امان یابد ز کسوف و زوال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هلال چرخ معالیش منخسف نشود</p></div>
<div class="m2"><p>از آنکه راه نباشد خسوف را به هلال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپر برشده را رای او به خدمت خواند</p></div>
<div class="m2"><p>کمر ببست به جوزا چو بندگان به دوال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز حرص خدمت او سرنگون همی آیند</p></div>
<div class="m2"><p>به وقت مولد از ارحام مادران اطفال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز شاخ بادرم آید کف چنار برون</p></div>
<div class="m2"><p>گر از مهب کف او وزد نسیم شمال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ترازویی که بدان بار بر او سنجند</p></div>
<div class="m2"><p>سپهر کفهٔ او زیبد و زمین مثقال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز حرص آنکه ازو سائلان سؤال کنند</p></div>
<div class="m2"><p>همی سؤال بخواهد ز سائلان به سؤال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ایا محامد تو نقش گشته در اوهام</p></div>
<div class="m2"><p>و یا مؤثر تو وقف گشته بر اقوال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خطر ندید هر آنکو ندید از تو قبول</p></div>
<div class="m2"><p>شرف نیافت هر آنکو نجست با تو وصال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو آن کسی که سپهرت نپرورید نظیر</p></div>
<div class="m2"><p>تو آن کسی که خدایت نیافرید همال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زمانه سال و مه از خدمت تو جوید نام</p></div>
<div class="m2"><p>ستاره روز و شب از طلعت تو گیرد فال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو آدمی و همه دشمنان تو ابلیس</p></div>
<div class="m2"><p>تو مهدیی و همه حاسدان تو دجال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به دست حزم بمالی همی مخالفت را</p></div>
<div class="m2"><p>زمانه نیز نبیند چو تو مخالف مال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگرنه کین تو کفرست پس چرا دارد</p></div>
<div class="m2"><p>سپهر خصم ترا خون مباح و مال حلال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عدو حرارت بیم تو دارد اندر دل</p></div>
<div class="m2"><p>ز دست مردمک دیده زان زند قیفال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بزرگوارا شد مدتی که من خادم</p></div>
<div class="m2"><p>به خدمتت نرسیدم ز گردش احوال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه آنکه از دل و جان مخلصت نبودستم</p></div>
<div class="m2"><p>گواه دارم، وان کیست ایزد متعال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز مجلس تو گر ابرام دور داشته‌ام</p></div>
<div class="m2"><p>نه از فراغت من بود بل ز بیم ملال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وگرنه در دو سه موسم ز طبع چون آتش</p></div>
<div class="m2"><p>قصیده‌هات بیاورد می چو آب زلال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به جای دیگر اگر اول التجا کردم</p></div>
<div class="m2"><p>بدیدم آنچه مبیناد هیچ کس به خیال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خدای داند و کس چون خدای نیست که کس</p></div>
<div class="m2"><p>به عمر خویش ندیدست از آن سمج‌تر حال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ثنا قبول به همت کنند اهل ثنا</p></div>
<div class="m2"><p>بلی که مرد به همت پرد چو مرغ به بال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدین دلیل تویی خواجهٔ به استحقاق</p></div>
<div class="m2"><p>وزین قیاس تویی مهتر به استقلال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه هرکرا به لقب با کسی مشابهت است</p></div>
<div class="m2"><p>شبیه اوست چنان چون یمین شبیه شمال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که دال نیز چو ذال است در کتابت لیک</p></div>
<div class="m2"><p>به ششصد و نود و شش کمست دال از ذال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ببین که میر معزی چه خوب می‌گوید</p></div>
<div class="m2"><p>حدیث هیات بینو و شکل کعب غزال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در این مقابله یک بیت ازرقی بشنو</p></div>
<div class="m2"><p>نه بر طریق تهجی به وجه استدلال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زمرد و گیه سبز هر دو همرنگند</p></div>
<div class="m2"><p>ولیک زین به نگین‌دان کشند از آن به جوال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همیشه تا که بود نعت زلف در ابیات</p></div>
<div class="m2"><p>همیشه تا که بود وصف خال در امثال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سری که از تو بپیچد بریده باد چو زلف</p></div>
<div class="m2"><p>دلی که از تو بگردد سیاه باد چو خال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هزار سال تو مخدوم و دهر خدمتگار</p></div>
<div class="m2"><p>هزار جای تو ممدوح و بنده مدح سگال</p></div></div>