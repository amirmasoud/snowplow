---
title: >-
    قصیدهٔ شمارهٔ ۵۲ - در مدح صاحب ناصرالدین طاهر
---
# قصیدهٔ شمارهٔ ۵۲ - در مدح صاحب ناصرالدین طاهر

<div class="b" id="bn1"><div class="m1"><p>صاحبا جنبشت همایون باد</p></div>
<div class="m2"><p>عید و نوروز بر تو میمون باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طالع اختیار مسعودت</p></div>
<div class="m2"><p>زبدهٔ شکلهای گردون باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صولت و سرعت زمین و زمان</p></div>
<div class="m2"><p>با رکاب و عنانت مقرون باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در زوایای ظل رایت تو</p></div>
<div class="m2"><p>فتنه بر خواب امن مفتون باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دفع سؤ المزاج دولت را</p></div>
<div class="m2"><p>لطف تدبیرهات معجون باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک و خاشاک منزلت ز شرف</p></div>
<div class="m2"><p>طور سینین و تین و زیتون باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تراکم غبار موکب تو</p></div>
<div class="m2"><p>حصن سکان ربع مسکون باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وز پی غوطهٔ حوادث را</p></div>
<div class="m2"><p>موج فوجت چو موج جیحون باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرد جیشت که متصل مددست</p></div>
<div class="m2"><p>مدد سمک و کوه و هامون باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روز خصمت که منفصل عقبست</p></div>
<div class="m2"><p>متصل بر در شبیخون باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تن که بی‌داغ طاعتت زاید</p></div>
<div class="m2"><p>از مراعات نشو بیرون باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زر که بی مهر خازنت روید</p></div>
<div class="m2"><p>قسم میراث‌خوار قارون باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرنه لاف از دلت زند دریا</p></div>
<div class="m2"><p>گوهرش در دل صدف خون باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ورنه بر امر تو رود گردون</p></div>
<div class="m2"><p>همچو گردون بارکش دون باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دست سرو ار دعای تو نکند</p></div>
<div class="m2"><p>الف استقامتش نون باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ور کمر جز به خدمتت بندد</p></div>
<div class="m2"><p>نیشکر آبش آب افیون باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وقت توجیه رزق آدمیان</p></div>
<div class="m2"><p>آسمان را کف تو قانون باد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جادوان از ترازوی عدلت</p></div>
<div class="m2"><p>حل و عقد زمانه موزون باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در مصاف قضا به خون عدوت</p></div>
<div class="m2"><p>تا به شمشیر بید گلگون باد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در کمین عدم گرت خصمیست</p></div>
<div class="m2"><p>دهر در انتقامش اکنون باد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در جهان تا کمی و افزونیست</p></div>
<div class="m2"><p>کمی دشمنت بر افزون باد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به ضمان خزینه‌دار ابد</p></div>
<div class="m2"><p>عز و عمرت همیشه مخزون باد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اجر اعمال صالح بنده</p></div>
<div class="m2"><p>از ایادیت غیر ممنون باد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وز قبول تو پیش آب سخنش</p></div>
<div class="m2"><p>خاک در چشم در مکنون باد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ور مشرف شود به تشریفی</p></div>
<div class="m2"><p>قصبش پای‌مزد اکسون باد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صاحبا بنده را اجازت ده</p></div>
<div class="m2"><p>تا بگویم که دشمنت چون باد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خار در چشم و کلک در ناخن</p></div>
<div class="m2"><p>تیز در ریش و کیر در کون باد</p></div></div>