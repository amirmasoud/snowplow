---
title: >-
    قصیدهٔ شمارهٔ ۳ - در مدح شاهزاده عمادالدین
---
# قصیدهٔ شمارهٔ ۳ - در مدح شاهزاده عمادالدین

<div class="b" id="bn1"><div class="m1"><p>ای داده به دست هجر ما را</p></div>
<div class="m2"><p>خود رسم چنین بود شما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر گوش نهاده‌ای سر زلف</p></div>
<div class="m2"><p>وز گوشهٔ دل نهاده ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کی ز دروغ راست مانند</p></div>
<div class="m2"><p>زین درد امید کی دوا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر لحظه کجی نهی دگرگون</p></div>
<div class="m2"><p>کس درندهد تن این دغا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بردی دل و عشوه دادی ای جان</p></div>
<div class="m2"><p>پاداش جفا بود وفا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما عافیتی گرفته بودیم</p></div>
<div class="m2"><p>دادی تو به ما نشان بلا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن روز که گنج حسن کردی</p></div>
<div class="m2"><p>این کنج وثاق بی‌نوا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم که کنون ز درگه دل</p></div>
<div class="m2"><p>امید عیان کند وفا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک‌دم دو سخن به هم بگوییم</p></div>
<div class="m2"><p>زان کام دلی بود هوا را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در حجرهٔ وصل نانشسته</p></div>
<div class="m2"><p>هجر آمد و در بزد قضا را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان گفت که کیست گفت بگشای</p></div>
<div class="m2"><p>بیگانه مدار آشنا را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گستاخ برآمد و درآمد</p></div>
<div class="m2"><p>تهدیدکنان جدا جدا را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با وصل به خشم گفت آری</p></div>
<div class="m2"><p>گر من نکشم تو ناسزا را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ناری تو به دامن وفا دست</p></div>
<div class="m2"><p>اندر زده آستین جفا را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خواهی که خبر کنم هم‌اکنون</p></div>
<div class="m2"><p>زین حال کسان پادشا را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شهزاده عماد دین که تیغش</p></div>
<div class="m2"><p>صد باره پذیره شد وغا را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>احمد که ز محمدت نشانیست</p></div>
<div class="m2"><p>هم نامی ذات مصطفا را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن کو چو به حرب تاخت بیند</p></div>
<div class="m2"><p>بر دلدل تند مرتضی را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرد سپهش به حکم رد کرد</p></div>
<div class="m2"><p>از حجرهٔ دیده توتیا را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خاک قدمش به فخر بنشاند</p></div>
<div class="m2"><p>در گوشهٔ گوش کیمیا را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای کرده خجل نسیم خلقت</p></div>
<div class="m2"><p>در ساحت بوستان صبا را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>طبع تو که ابر ازو کشد در</p></div>
<div class="m2"><p>یک تعبیه کرده صد سخا را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دست تو که کوه او برد کان</p></div>
<div class="m2"><p>صد گنج نهاده یک عطا را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در بزم امل ز بخشش تو</p></div>
<div class="m2"><p>محروم ندیده جز ریا را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در رزم اجل ز کوشش تو</p></div>
<div class="m2"><p>زنهار نخواست جز وبا را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در عالم معدلت صبا یافت</p></div>
<div class="m2"><p>از عدل تو معتدل هوا را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از غیرت رایتت فلک دید</p></div>
<div class="m2"><p>در خط شده خط استوا را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روزی که فتد خس کدورت</p></div>
<div class="m2"><p>در دیده هوای با صفا را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در گرد ز مرد باز دارد</p></div>
<div class="m2"><p>چون ظلمت چشمهٔ ضیا را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از رمح چو مار کرده پیچان</p></div>
<div class="m2"><p>چون کرده به دیده اژدها را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از لعل حجاب سازد الماس</p></div>
<div class="m2"><p>رخسارهٔ همچو کهربا را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گه حسرت سر بود کله را</p></div>
<div class="m2"><p>گه فرقت تن بود قبا را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در دیدهٔ فتح جای سازد</p></div>
<div class="m2"><p>از کوری دشمنان لوا را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پیش تو زمین اگر نبوسد</p></div>
<div class="m2"><p>منکر المی رسد فنا را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عکس سپر سهیل شکلت</p></div>
<div class="m2"><p>از پای درآورد سها را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا روی به خطهٔ خراسان</p></div>
<div class="m2"><p>آوردی و مانده مر ختا را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اینجا ز صواب رای عالیت</p></div>
<div class="m2"><p>یک شغل نمی‌رود خطا را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون نیک نظر کنم نزیبد</p></div>
<div class="m2"><p>چون نام تو زیوری ثنا را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از کعبه چو بگذری نباشد</p></div>
<div class="m2"><p>چون سده‌ت قبلهٔ دعا را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از تیغ تو ای بقای دولت</p></div>
<div class="m2"><p>ناموس تبه شود قضا را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آراسته نظم من عروسیست</p></div>
<div class="m2"><p>شایسته کنار کبریا را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آخر ز برای او نگهدار</p></div>
<div class="m2"><p>این پر هنر نکو ادا را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یک دم منه از کنار فکرت</p></div>
<div class="m2"><p>این خوب نهاد خوش لقا را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا هیچ سبب بود ز ایمان</p></div>
<div class="m2"><p>در دیدهٔ مردمی حیا را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آن معجزه بادت از بزرگی</p></div>
<div class="m2"><p>در جاه که بود انبیا را</p></div></div>