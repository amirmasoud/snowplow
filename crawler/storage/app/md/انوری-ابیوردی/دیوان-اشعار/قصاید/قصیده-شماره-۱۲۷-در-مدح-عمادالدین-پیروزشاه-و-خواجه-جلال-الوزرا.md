---
title: >-
    قصیدهٔ شمارهٔ ۱۲۷ - در مدح عمادالدین پیروزشاه و خواجه جلال‌الوزرا
---
# قصیدهٔ شمارهٔ ۱۲۷ - در مدح عمادالدین پیروزشاه و خواجه جلال‌الوزرا

<div class="b" id="bn1"><div class="m1"><p>ای رایت رفیعت بنیاد نظم عالم</p></div>
<div class="m2"><p>وی گوهر شریفت مقصود نسل آدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برنامهٔ وجودت شد چار حرف عنوان</p></div>
<div class="m2"><p>کان چار حرف آمد پس چار طبع عالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم نام فرخت را زی نامه برد عیسی</p></div>
<div class="m2"><p>کین بود از آن دگرها فضلش فزون عدد کم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر پنج عمده بودی دین را اساس و اکنون</p></div>
<div class="m2"><p>تا تو عماد دینی شد شش همه معظم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای آفتاب رایت بر آفتاب غالب</p></div>
<div class="m2"><p>وی آسمان قدرت بر آسمان مقدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر نامهٔ وجودت نام رسول عنوان</p></div>
<div class="m2"><p>بر طینت نهادت حفظ خدای مدغم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در عرصهٔ ممالک پیش نفاذ امرت</p></div>
<div class="m2"><p>هم دست‌جور کوته هم پای عدل محکم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دین از تو چون ارم شد ذات عماد ربی</p></div>
<div class="m2"><p>زین بیش می تو گفتی هستی به کنه طارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باست فروگشاید از خاک صبر و صولت</p></div>
<div class="m2"><p>حفظت نگاه دارد بر آب نقش خاتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خال جمال دولت بر نامهات نقطه</p></div>
<div class="m2"><p>زلف عروس نصرت بر نیزهات پرچم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در شیر رایت تو باد هوای هیجا</p></div>
<div class="m2"><p>روح‌الله است گویی در آستین مریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لطف سبک عنانت کوثر کند ز دوزخ</p></div>
<div class="m2"><p>قهر گران رکابت آتش کند ز زمزم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تکبیر فتح گوید سیاره چون برانی</p></div>
<div class="m2"><p>با فکرت مصور با نصرت مجسم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از حرفهای تیغت آیات فتح خیزد</p></div>
<div class="m2"><p>تالیف آیت آری هست از حروف معجم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی‌رونقا که باشد بی‌باس تو سیاست</p></div>
<div class="m2"><p>بی‌هیزما که باشد بی‌تیغ تو جهنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از بوستان بزمت شاخی درخت طوبی</p></div>
<div class="m2"><p>بر آستان جاهت گردی سپهر اعظم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیش شمال امرت پای شمال در گل</p></div>
<div class="m2"><p>پیش سحاب دستت دست سحاب بر هم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آنجا در زه آرد دستت کمان بخشش</p></div>
<div class="m2"><p>ابر از حسد ببرد زه بر کمان رستم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دست چنار هرگز بی‌زر برون نیاید</p></div>
<div class="m2"><p>گر از محیط دستت بردارد آسمان نم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در شاهراه دوران با عزم تیزگامت</p></div>
<div class="m2"><p>گردون چه گفت گفتا من تابعم تقدم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در مشکلات گیتی با رای پیش بینت</p></div>
<div class="m2"><p>اختر چه گفت گفتا من عاجزم تکلم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صایب‌تر از کمانت یک راه رو نزد پی</p></div>
<div class="m2"><p>صادق‌تر از کلامت یک صبحدم نزد دم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از خلوت ضمیرت بویی نبرد هرگز</p></div>
<div class="m2"><p>جاسوس وهم کانجا بر وهم گم شود شم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در هر سخن که گویی گوید قضا پیاپی</p></div>
<div class="m2"><p>ای ملک طفل اسمع ای پیر چرخ اعلم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زودا که داغ حکمت خواهد گرفت یکسر</p></div>
<div class="m2"><p>از گوش صبح اشهب تا نعل شام ادهم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>با آسمان چه گفتم گفتا که هست ممکن</p></div>
<div class="m2"><p>دستی ورای دستت در کارهای عالم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سوی تو کرد اشارت گفتا که دست حکمش</p></div>
<div class="m2"><p>حکمی چگونه حکمی همچون قضای مبرم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آن قدرتست او را بر حل و عقد گیتی</p></div>
<div class="m2"><p>کان تا ابد نگردد هرگز مرا مسلم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفتم نفاذ حکمش در تو مؤثر آید</p></div>
<div class="m2"><p>گفتا که می چه گویی در ماورای من هم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا روز چند بینی سگبانش برنهاده</p></div>
<div class="m2"><p>شیر مرا قلاده همچون سگ معلم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای یادگار دولت، دولت به تو مشرف</p></div>
<div class="m2"><p>وی حقگزار ملت، ملت به تو مکرم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در مدتی که بودی غایب ز دار دولت</p></div>
<div class="m2"><p>ای در حضور و غیبت شان تو شان معظم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آن ورطه دید حاشا دولت که کنه آنرا</p></div>
<div class="m2"><p>غایت خدای داند والله جل اعظم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تقریر حال دولت چندا که کم کنی به</p></div>
<div class="m2"><p>زان فتنهٔ پیاپی زان آفت دمادم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در دی مه حوادث از بیخ و بن برآمد</p></div>
<div class="m2"><p>ملکی که بود عمری چون نوبهار خرم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>الحق نبود درخور با آنچنان دو وقعت</p></div>
<div class="m2"><p>این نیمهٔ رجب را وان آخر محرم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حالی که رای عالی داند چو روز روشن</p></div>
<div class="m2"><p>من بنده چند گویم چندین صریح و مبهم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در جمله ملک و دین را با آن دو زخم مهلک</p></div>
<div class="m2"><p>هر روز تازه گشتی دیگر جراحتی ضم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یارب کجا رسیدی پایان کار ایشان</p></div>
<div class="m2"><p>گر جاه تو نکردی این سودمند مرهم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گیتی خراب گشتی گر در سرای گیتی</p></div>
<div class="m2"><p>سوری چینن نبودی بعد از چنان دو ماتم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همواره تا که باشد در جلوه‌گاه بستان</p></div>
<div class="m2"><p>پیش زبان بلبل سوسن زبان ابکم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در باغ آفرینش از حرص خدمت تو</p></div>
<div class="m2"><p>همچون بنفشه هرگز پشتی مباد بی‌خم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هم خانه با سعادت بختت چو راز با دل</p></div>
<div class="m2"><p>هم گوشه با زمانه عمرت چو زیر بابم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دست گهرفشانت تا صبح حشر باقی</p></div>
<div class="m2"><p>جان خردنگارت تا شام دهر بی‌غم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>روزت چو عید فرخ عیدت چو روز میمون</p></div>
<div class="m2"><p>وز روزهٔ تنفس بربسته خصم را دم</p></div></div>