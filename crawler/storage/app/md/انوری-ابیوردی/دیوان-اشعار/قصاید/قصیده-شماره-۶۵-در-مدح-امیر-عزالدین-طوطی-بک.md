---
title: >-
    قصیدهٔ شمارهٔ ۶۵ - در مدح امیر عزالدین طوطی بک
---
# قصیدهٔ شمارهٔ ۶۵ - در مدح امیر عزالدین طوطی بک

<div class="b" id="bn1"><div class="m1"><p>خراب کرد به یکبار بخل کشور جود</p></div>
<div class="m2"><p>نماند در صدف مکرمات گوهر جود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وبال گشت همه فضل و علم و راحت و مال</p></div>
<div class="m2"><p>شرنگ گشت همه نوش و شهد و شکر جود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برفت باد مروت بگشت خاک وفا</p></div>
<div class="m2"><p>ببست آب فتوت بمرد آدر جود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخفت فتنه و بی‌جفت خفت شخص هنر</p></div>
<div class="m2"><p>نماند همت و بی‌شوی ماند دختر جود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک به مهر نشد یک نفس مطیع خرد</p></div>
<div class="m2"><p>جهان به کام نشد یک زمان مسخر جود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دریده گشت به زوبین ناکسی دل لطف</p></div>
<div class="m2"><p>بریده گشت به شمشیر ممسکی سر جود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی‌دمد به مشامم نسیم سنبل عدل</p></div>
<div class="m2"><p>نمی‌دهد به دماغم بخار عنبر جود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به صدق نیست در این عهد بخت ناصر جاه</p></div>
<div class="m2"><p>به طبع نیست در این عصر ملک غمخور جود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هلاک گشت عقات امل ز گرسنگی</p></div>
<div class="m2"><p>مگر نماند به برج شرف کبوتر جود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرا فروغ نیابد هوای سال امید</p></div>
<div class="m2"><p>که آفتاب هنر رفت در دو پیکر جود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وجود جود عدم گشت و نیست هیچ شکی</p></div>
<div class="m2"><p>که در جهان کرم کس ندید منظر جود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنون که صبح خساست به شرق بخل دمید</p></div>
<div class="m2"><p>درون پرده شود آفتاب خاور جود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سهیل عدل نتابد به طرف قطب شرف</p></div>
<div class="m2"><p>سپهر ملک نگردد به گرد محور جود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در این هوس که خرامنده ماه من برسید</p></div>
<div class="m2"><p>به شکل عربده بر من کشید خنجر جود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لبش به نوش بیاکنده لطف صانع لطف</p></div>
<div class="m2"><p>رخش به مشک نگاریده صنع داور جود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به خشم گفت که چندین به رسم بی‌ادبان</p></div>
<div class="m2"><p>مگوی مرثیهٔ جود در برابر جود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>امید جود مبر از جهان کنون که گشاد</p></div>
<div class="m2"><p>فلک به طالع فرخنده بر جهان در جود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به عون همت سلطان عصر و شاه جهان</p></div>
<div class="m2"><p>شجاع دولت و سالار ملک و صفدر جود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خدایگان سلاطین ستوده عزالدین</p></div>
<div class="m2"><p>کمال ملت و دیهیم عدل و مفخر جود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جهانگشای ولی نعمتی که همت او</p></div>
<div class="m2"><p>همیشه هست به انعام روح‌پرور جود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>طری به مکرمت جود اوست سوسن ملک</p></div>
<div class="m2"><p>قوی به تقویت کلک اوست لشکر جود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به فهم حکمت او حاصل است مشکل علم</p></div>
<div class="m2"><p>به وهم همت اوظاهر است مضمر جود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نهفته در دل داهیش بخت ذات کرم</p></div>
<div class="m2"><p>سرشته در کف کافیش طبع جوهر جود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به یمن دولت او گشت چرخ خادم ملک</p></div>
<div class="m2"><p>به عون همت او هست دهر چاکر جود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زهی به حزم و فراست کمال رتبت و جاه</p></div>
<div class="m2"><p>خهی به عزم و سیاست کمال و زیور جود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تویی به طالع میمون مدام بابت ملک</p></div>
<div class="m2"><p>تویی به رای همایون همیشه در خور جود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به احتشام تو فرخنده گشت طالع سعد</p></div>
<div class="m2"><p>به احترام تو رخشنده گشت اختر جود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز عکس تیغ تو تایید یافت بازوی عدل</p></div>
<div class="m2"><p>به نوک کلک تو تشریف یافت محضر جود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>غلام ملک تو بر سر نهاد تاج شرف</p></div>
<div class="m2"><p>عروس بخت تو بر روی بست معجر جود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ندید مثل تو هنگام عدل چشم خرد</p></div>
<div class="m2"><p>نزاد شبه تو هنگام لطف مادر جود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بنازنید ترا افتخار بر سر تخت</p></div>
<div class="m2"><p>بپرورید ترا روزگار بر بر جود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صفات حمد تو در ابتدای مصحف مجد</p></div>
<div class="m2"><p>مثال نعت تو در انتهای دفتر جود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز هول جود تو لاغر شدست فربه بخل</p></div>
<div class="m2"><p>ز امن بر تو فربه شدست لاغر جود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شدست نام تو مجموع بر وجود کرم</p></div>
<div class="m2"><p>بدین صفات شدی در زمانه سرور جود</p></div></div>