---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>یار رقیب دیده، سزایش ندیدن است</p></div>
<div class="m2"><p>دندان کرم خورده، علاجش کشیدن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باید که عضو عضو تو کوشند بهر شکر</p></div>
<div class="m2"><p>آن سانکه شکر گوش، نصیحت شنیدن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاداش یک دقیقه که غافل شوی ز حق</p></div>
<div class="m2"><p>یک عمر پشت دست بدندان گزیدن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برخیز و باش از گذر عمر بهره مند</p></div>
<div class="m2"><p>وقت سحر که باد صبا در وزیدن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چیزی که کفر محض بود در طریق عشق</p></div>
<div class="m2"><p>غافل ز یاد خسته دلان آرمیدن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جائیکه عندلیب صفت صبر می توان</p></div>
<div class="m2"><p>(صابر) چو گل چه جای گریبان دریدن است</p></div></div>