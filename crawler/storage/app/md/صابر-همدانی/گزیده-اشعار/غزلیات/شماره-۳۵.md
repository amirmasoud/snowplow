---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>ای ساحت قدس همدان، ای چمن عشق</p></div>
<div class="m2"><p>جان برخی خاک تو، که هستی وطن عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر قطعه ای از خاک تو خلدی است مصفا</p></div>
<div class="m2"><p>کز خاطر عشاق زداید محن عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تل و دمن خرم و سبزت زده پهلو</p></div>
<div class="m2"><p>در عالم اندیشه بتل و دمن عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر لاله که از دامن الوند تو روید</p></div>
<div class="m2"><p>زیبنده بود بر تن او پیرهن عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا منظر زیبای تو شد از نظرم دور</p></div>
<div class="m2"><p>نشنیده کسی از دهن من سخن عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از فرقت یاران هم آهنگ و وفا کیش</p></div>
<div class="m2"><p>دور از تو شدم ساکن بیت الحزن عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر یک ز معاریف تو در عالم عرفان</p></div>
<div class="m2"><p>خون خورده و نو کرده حدیث کهن عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آثار بزرگان و اساتید علومت</p></div>
<div class="m2"><p>جاری است بهر عصر و زمانی سنن عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندر کنفت (عین قضاة) این شرفت بس</p></div>
<div class="m2"><p>کامروز توئی مدفن خونین کفن عشق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آرامگه (بوعلی) و بقعه ی (بابا)</p></div>
<div class="m2"><p>آن مخزن حکمت بود، این انجمن عشق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شعر شعرای تو زند راه دل خلق</p></div>
<div class="m2"><p>آنسان که بود حسن بتان راهزن عشق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رفت از همدان (صابر)و، گوئی که برون شد</p></div>
<div class="m2"><p>از هند سخن طوطی شکرشکن عشق</p></div></div>