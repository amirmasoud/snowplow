---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>به حکم آنکه نادان همسر دانا نخواهد شد</p></div>
<div class="m2"><p>برابر قطرهٔ ناچیز با دریا نخواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمین تا آسمان فرق است بین عارف و عامی</p></div>
<div class="m2"><p>که هرگز ذره خورشید جهان آرا نخواهد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشاید گفت استاد سخن طفل دبستان را</p></div>
<div class="m2"><p>مگس، هم بال و هم پرواز با عنقا نخواهد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندیدم هیچکس عیب کسی را روبرو گوید</p></div>
<div class="m2"><p>دگر آئینه وش روشندلی پیدا نخواهد شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر اهل دلی، اهل ادب را روی خوش بنما</p></div>
<div class="m2"><p>که بی آئینه نطق طوطیان گویا نخواهد شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیفشان دامن از گرد تعلق اندرین صحرا</p></div>
<div class="m2"><p>دراین ره گر چه یکتن گرد باد آسا نخواهد شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگو با مدعی (صابر) که بنشیند به جای خود</p></div>
<div class="m2"><p>حریف هر کسی باشد حریف ما نخواهد شد</p></div></div>