---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>دیدم چو باغ دل را، بی عارضت صفا نیست</p></div>
<div class="m2"><p>گل گفتمت ولیکن گل چون تو بیوفا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنشین ببزم اغیار چون گل که در بر خار</p></div>
<div class="m2"><p>زیرا که از تو ای یار، این شیوه خوشنما نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من نیستم چو بلبل، کز غم کنم تحمل</p></div>
<div class="m2"><p>زیرا که موسم گل، گلچین یکی دو تا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من آنچه از غم عشق دیدم بعالم عشق</p></div>
<div class="m2"><p>جز نزد محرم عشق، اظهار آن بجا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>(صابر) وصال یاران حیف است مغتنم دان</p></div>
<div class="m2"><p>همواره در گلستان، گل زیردست و پا نیست</p></div></div>