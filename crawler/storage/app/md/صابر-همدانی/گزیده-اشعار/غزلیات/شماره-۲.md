---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>تا ز خاک مقدمت کردیم روشن دیده را</p></div>
<div class="m2"><p>چشم ما حاجت ندارد سرمهٔ ساییده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود توانی با دل من آتش عشقت چه کرد</p></div>
<div class="m2"><p>دیده باشی فی المثل گر موم آتش دیده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو بخواب ناز و من بیدار و دانند اهل دل</p></div>
<div class="m2"><p>وای اگر بیدار باشد در قفا خوابیده را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس نخواهد داد دلها را، که گلچین در جهان</p></div>
<div class="m2"><p>کی بشاخ آویزد از نو غنچه های چیده را؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>(صابر) آسا می توان در صبر کوشد، گر کسی</p></div>
<div class="m2"><p>نرم سازد رد کف دست آهن تفتیده را</p></div></div>