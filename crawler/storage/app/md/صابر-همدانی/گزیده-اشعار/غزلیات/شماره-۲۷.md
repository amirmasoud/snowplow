---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>چون همدم اغیار منافق نتوان بود</p></div>
<div class="m2"><p>دور از بر یاران موافق نتوان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نهار ز دیدار بد اندیش بپرهیز</p></div>
<div class="m2"><p>زیرا که بر این منظره شایق نتوان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با سیرت بد، فایده ی صورت خوش چیست؟</p></div>
<div class="m2"><p>مانند گل سرخ شقایق نتوان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از صدق و صفا هیچ نکوتر عملی نیست</p></div>
<div class="m2"><p>هر چند زمانی است که صادق نتوان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر خلق خدا ظلم روا می نتوان داشت</p></div>
<div class="m2"><p>هم مسلک یک سلسله سارق نتوان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنانکه بسر منزل مقصود رسیدند</p></div>
<div class="m2"><p>گفتند که پا بست علایق نتوان بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از (صابر) دلخسته بگوئید بیاران</p></div>
<div class="m2"><p>صامت بنشینید که ناطق نتوان بود</p></div></div>