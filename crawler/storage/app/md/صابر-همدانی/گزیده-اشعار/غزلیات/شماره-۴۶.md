---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>دیشب، مه من! انجمن آرای که بودی؟</p></div>
<div class="m2"><p>خلقی بتو مجنون و، تو لیلای که بودی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هنگام تبسم بدهان تو، که پی برد؟</p></div>
<div class="m2"><p>ز آن لعل لبت حل معمای که بودی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خونا به فشان از بن مژگان که گشتی؟</p></div>
<div class="m2"><p>با لشگر حسن از پی یغمای که بودی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لوح دل من، صورت غیری نپذیرفت</p></div>
<div class="m2"><p>ای آفت دل! شاهد معنای که بودی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با دیده ی خود بر رخ خویشت نظری بود</p></div>
<div class="m2"><p>در مردمک دیده ی بینای که بودی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من اشک فشان بودمت از غم، تو نگفتی</p></div>
<div class="m2"><p>ز آن زلف دوتا سلسلهٔ پای که بودی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>(صابر)! بود امروز دلت خرم و روشن</p></div>
<div class="m2"><p>دوش آینه دار رخ زیبای که بودی؟</p></div></div>