---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>از بس که آفریده گلت را خدا ملیح</p></div>
<div class="m2"><p>پا تا به سر ملیحی و سر تا به پا ملیح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم و لب و دهان و بناگوش و غبغبت</p></div>
<div class="m2"><p>هر یک به شیوه‌ای خوش و هر یک به جا ملیح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرم و حیا، تو را به ملاحت فزوده است</p></div>
<div class="m2"><p>خوش آنکه شد ز دولت شرم و حیا ملیح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا عندلیب نغمه سراید ز عشق گل</p></div>
<div class="m2"><p>باشد بیاد روی تو، گفتار ما ملیح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>(صابر) چو غنچه شو متبسم، چو گل مخند</p></div>
<div class="m2"><p>زیرا که نیست خندهٔ دندان‌نما ملیح</p></div></div>