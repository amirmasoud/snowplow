---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>هر که آمد در جهان پست و رفت</p></div>
<div class="m2"><p>رشتهٔ الفت ز ما بگسست و رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرگ صیاد است و، صیدش آدمی است</p></div>
<div class="m2"><p>کو که زین صیاد، سالم جست و رفت؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر ما، همچون نسیم صبحگاه</p></div>
<div class="m2"><p>آمد و از پا دمی ننشست و رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آدمی را، تن طلسم جان بود</p></div>
<div class="m2"><p>عاقبت میبایدش بشکست و رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرم از سر منزل کثرت گذشت</p></div>
<div class="m2"><p>آنکه شد از جام وحدت مست و رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچکس با خویشتن چیزی نبرد</p></div>
<div class="m2"><p>هر که بودش هر چه داد از دست و رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظم را عقد گهر بگسسته بود</p></div>
<div class="m2"><p>لیکنش (صابر) به هم پیوست و رفت</p></div></div>