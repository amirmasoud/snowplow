---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ندهد دست اگر دولت دیدار مرا</p></div>
<div class="m2"><p>آخر الامر کشد دوری دلدار مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش نگرفتم اگر راه بیابان جنون</p></div>
<div class="m2"><p>زلف زنجیر وشت بود نگهدار مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نزدم بیهده اندر ره عشق تو قدم</p></div>
<div class="m2"><p>سیرها گشته در این راه پدیدار مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد چو منصور دگر راز من از پرده برون</p></div>
<div class="m2"><p>دار کوتا که رها سازد از این دار مرا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>(صابرا) چون دهن یار شد این قافیه تنگ</p></div>
<div class="m2"><p>نه عجب پاره شد ار پرده ی پندار مرا</p></div></div>