---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>عاشق صادق ای جوان، بند هوس نمی‌شود</p></div>
<div class="m2"><p>طایر قدس آشیان، صید مگس نمی‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند اسیر این و آن؟ دل به یکی ده ای جوان!</p></div>
<div class="m2"><p>چون همه کس در این جهان بهر تو کس نمی‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داشت کسی که عاطفت، نیست انیس بدصفت</p></div>
<div class="m2"><p>گوهر بحر معرفت، همسر خس نمی‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوالهوسی و بوالهوس، هست چو طایر و قفس</p></div>
<div class="m2"><p>خواهی اگر زنی نفس، کنج قفس نمی‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل ضعیف و ناتوان، نیست چو عشق کاردان</p></div>
<div class="m2"><p>نه خر لنگ را که آن همچو فَرَس نمی‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون من (صابر) ای صنم، با تو کسی که زد قدم</p></div>
<div class="m2"><p>کیست که همچو صبحدم، تازه‌نفس نمی‌شود</p></div></div>