---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>در دارِ جهان تا بُوَدَت جان به جسد</p></div>
<div class="m2"><p>پیدا و نهان گریز از مردمِ بد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌ترس از آن دم که شود عیب تو فاش</p></div>
<div class="m2"><p>چون جوجه همیشه نیست در زیر سبد</p></div></div>