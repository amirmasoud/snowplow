---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>بی علم کسی بود که نا اهل بود</p></div>
<div class="m2"><p>دانستن علم و معرفت سهل بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این نکته مبرهن است نزد همه کس</p></div>
<div class="m2"><p>دانستن هر چیز به از جهل بود</p></div></div>