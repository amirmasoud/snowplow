---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>هر کاو به جمال سروری مشتاق است</p></div>
<div class="m2"><p>سرمایهٔ او مکارم الاخلاق است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>استحقاقی است سروری را بر او</p></div>
<div class="m2"><p>مردم داری دلیل استحقاق است</p></div></div>