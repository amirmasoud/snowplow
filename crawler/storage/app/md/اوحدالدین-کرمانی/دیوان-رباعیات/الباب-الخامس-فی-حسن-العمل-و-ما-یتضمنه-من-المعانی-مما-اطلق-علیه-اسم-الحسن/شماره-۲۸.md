---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>بامدعیان چو آب و آتش مامیز</p></div>
<div class="m2"><p>چون باد زخاک تا توانی برخیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که چو خاک آب رویت نبرند</p></div>
<div class="m2"><p>چون باد سبک مباش و چون آتش تیز</p></div></div>