---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>هر زخم که بر سینهٔ غمناک آید</p></div>
<div class="m2"><p>از تیغ زبانِ نفس ناپاک آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب سخن است آنک بدو دل شویند</p></div>
<div class="m2"><p>دل پاک بود اگر سخن پاک آید</p></div></div>