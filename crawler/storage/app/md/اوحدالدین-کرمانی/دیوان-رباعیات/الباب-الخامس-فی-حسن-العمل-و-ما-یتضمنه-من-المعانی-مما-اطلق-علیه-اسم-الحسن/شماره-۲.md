---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای دل زنفاق درگذر تا برهی</p></div>
<div class="m2"><p>بر صدق همی دار نظر تا برهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم می خوری و نان نگه می داری</p></div>
<div class="m2"><p>رو غم مخور و نان بخور تا برهی</p></div></div>