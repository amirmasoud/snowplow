---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>در خوی خوش است عیش خوش کز جان است</p></div>
<div class="m2"><p>ور عیشی هست غیر ازین سرد آن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با بدخویی تو را جهان تنگ آید</p></div>
<div class="m2"><p>خو خوش کن و چشم سوزنی میدان است</p></div></div>