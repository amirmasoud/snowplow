---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>گر قرب خدا می طلبی خوش خو باش</p></div>
<div class="m2"><p>وندر حق جمله خلق نیکو گو باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که چو صبح صادق القول شوی</p></div>
<div class="m2"><p>خورشید صفت با همه کس یک رو باش</p></div></div>