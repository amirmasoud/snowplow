---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>ای دوست اگر بهشت را داری دوست</p></div>
<div class="m2"><p>یک نکته بیاموز که آن سخت نکوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلق خوش تو تو را رساند به بهشت</p></div>
<div class="m2"><p>تنگی و فراخی بهشت تو ازوست</p></div></div>