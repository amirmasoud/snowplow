---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>زان پیش که من شیفته رسوا گردم</p></div>
<div class="m2"><p>وندر مجلس چو نقل رسوا گردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بر دل جمع زحمتی هست زمن</p></div>
<div class="m2"><p>هرچند که جا خوش است تا واگردم</p></div></div>