---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>بیشی مطلب زهیچ کس بیش مباش</p></div>
<div class="m2"><p>چون مرهم و موم باش و چون ریش مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که به تو زهیچ کس بد نرسد</p></div>
<div class="m2"><p>بدخواه و بدآموز و بداندیش مباش</p></div></div>