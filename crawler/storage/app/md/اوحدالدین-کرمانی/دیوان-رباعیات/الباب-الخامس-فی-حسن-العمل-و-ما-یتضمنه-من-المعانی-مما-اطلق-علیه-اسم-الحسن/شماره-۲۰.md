---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>دل گرچه به بد گرایدت، نیکی کن</p></div>
<div class="m2"><p>از بد چه گره گشایدت، نیکی کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیکی و بدی مونس گور تو شوند</p></div>
<div class="m2"><p>گر مونس گور بایدت، نیکی کن</p></div></div>