---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>هان تا نکنی هرآنچ بتوانی کرد</p></div>
<div class="m2"><p>بس کینه کش است روزگار ای سره مرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خصم چو یافتی ظفر ای سره مرد</p></div>
<div class="m2"><p>چندان زنش آن زمان که بتوانی خورد</p></div></div>