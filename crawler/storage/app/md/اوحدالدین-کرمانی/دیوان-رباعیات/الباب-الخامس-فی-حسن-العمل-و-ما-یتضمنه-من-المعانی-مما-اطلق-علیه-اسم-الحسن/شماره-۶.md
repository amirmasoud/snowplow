---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>چون گل به میان خار می باید زیست</p></div>
<div class="m2"><p>با دشمن دوست وار می باید زیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که سخن زپرده بیرون نشود</p></div>
<div class="m2"><p>در پردهٔ روزگار می باید زیست</p></div></div>