---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>گر عاقلی آزاد شو از بند هوس</p></div>
<div class="m2"><p>در راه خدا خرج کن این یک دو نفس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر دو روزه دولت عاریتی</p></div>
<div class="m2"><p>عاقل نه برنجد نه برنجاند کس</p></div></div>