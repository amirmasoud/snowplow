---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>مه را به بناگوش تو نسبت کردم</p></div>
<div class="m2"><p>زنهار که این سخن به گوش تو رسد</p></div></div>