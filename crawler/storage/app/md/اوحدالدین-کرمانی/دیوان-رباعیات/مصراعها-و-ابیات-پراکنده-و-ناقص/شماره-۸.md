---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>گفتم که تو را کجا توانم دیدن</p></div>
<div class="m2"><p>گفتا که مرا کجا توانی دیدن</p></div></div>