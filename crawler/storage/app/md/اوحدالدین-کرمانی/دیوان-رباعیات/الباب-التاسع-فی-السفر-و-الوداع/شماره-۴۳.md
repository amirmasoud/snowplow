---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>سبحان الله چه سخت کاری غم تو</p></div>
<div class="m2"><p>از خسته دلم عظیم کاری غم تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که غمم می خوری آری غم تو</p></div>
<div class="m2"><p>از غم چو گزیر نیست باری غم تو</p></div></div>