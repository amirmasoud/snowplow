---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>هر کس که زدردی به دوایی برسد</p></div>
<div class="m2"><p>گر صدق نباشد به ریایی برسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انصاف بده به کاهلی هرگز کس</p></div>
<div class="m2"><p>شاید که به چیزی و به جایی برسد؟</p></div></div>