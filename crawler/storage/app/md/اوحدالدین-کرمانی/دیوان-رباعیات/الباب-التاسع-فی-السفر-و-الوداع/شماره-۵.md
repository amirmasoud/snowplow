---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>هر بازو را زور کمان تو کجاست</p></div>
<div class="m2"><p>وین سخت کمان چه درخور بازوی ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ما زر و زور و بازو البته مجوی</p></div>
<div class="m2"><p>کانجا زر و زور و بازو ار هست تو راست</p></div></div>