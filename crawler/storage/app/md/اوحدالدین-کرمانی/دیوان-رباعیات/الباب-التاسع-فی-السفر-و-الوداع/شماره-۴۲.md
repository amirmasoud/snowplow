---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>بیگانهٔ جان شد دل و خویش غم تو</p></div>
<div class="m2"><p>قربان دل من است کیش غم تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلطان جهان پیش غمت مسکین است</p></div>
<div class="m2"><p>مسکینان را چه قدر پیش غم تو</p></div></div>