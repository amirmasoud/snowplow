---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>غم گفت کزو دو دیده خون باید کرد</p></div>
<div class="m2"><p>بازو علم صبر نگون باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سر که نه در پای غمش گردد پست</p></div>
<div class="m2"><p>از مملکت دلش برون باید کرد</p></div></div>