---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>رویی نه که پشت جان قوی ماند ازو</p></div>
<div class="m2"><p>پشتی نه که روی دل بگرداند ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پایی نه کزو به دست آرد مقصود</p></div>
<div class="m2"><p>دستی نه که پای عقل برهاند ازو</p></div></div>