---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>هر کس که سفر کرد پسندیده شود</p></div>
<div class="m2"><p>پیش همه کس چو مردم دیده شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آب لطیف تر نباشد چیزی</p></div>
<div class="m2"><p>لیکن چو مقام کرد گندیده شود</p></div></div>