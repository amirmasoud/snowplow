---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>کس مشکل اسرار حقیقت نگشاد</p></div>
<div class="m2"><p>کس یک قدم از نهاد بیرون ننهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون در نگری زمبتدی و استاد</p></div>
<div class="m2"><p>عجز است به دست هر که از مادر زاد</p></div></div>