---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>او را طلبی و تو منی اینت غلط</p></div>
<div class="m2"><p>می پنداری که چون منی اینت غلط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که صلاح نیکنامی ورزی</p></div>
<div class="m2"><p>وآنگه دم عاشقی زنی اینت غلط</p></div></div>