---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>در هر نفسی درد سری آرد غم</p></div>
<div class="m2"><p>یک لحظه مرا زدست نگذارد غم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل خون شد و از دیده ام افتاد برون</p></div>
<div class="m2"><p>دست از من بیچاره نمی دارد غم</p></div></div>