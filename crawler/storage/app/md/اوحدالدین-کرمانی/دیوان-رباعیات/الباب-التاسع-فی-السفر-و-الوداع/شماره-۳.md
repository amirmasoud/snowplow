---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>اینجا اگرم کار به فرهنگ نشد</p></div>
<div class="m2"><p>آخر قدم روان من لنگ نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من نیز به جانبی دگر رخت کشم</p></div>
<div class="m2"><p>مردم بنمردند و جهان تنگ نشد</p></div></div>