---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>هر جا که غم است زنگ آیینهٔ ماست</p></div>
<div class="m2"><p>هر تیر بلا که هست در سینهٔ ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادی زبرم بزود برمی گردد</p></div>
<div class="m2"><p>هم درد که او حریف دیرینهٔ ماست</p></div></div>