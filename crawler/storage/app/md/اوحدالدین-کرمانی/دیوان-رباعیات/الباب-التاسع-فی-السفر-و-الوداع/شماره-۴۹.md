---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>ناکس که به کس شود نمازی نبود</p></div>
<div class="m2"><p>و این سیرت خواجگی به بازی نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جمله خران دهر مرکب گردند</p></div>
<div class="m2"><p>خر را حرکات اسب تازی نبود</p></div></div>