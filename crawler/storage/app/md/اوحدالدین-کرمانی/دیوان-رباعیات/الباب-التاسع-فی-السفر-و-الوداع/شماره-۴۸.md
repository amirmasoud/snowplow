---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>ناجنس حلاوت جوانی بُبَرد</p></div>
<div class="m2"><p>عیش خوش و حظّ زندگانی بُبَرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم نیک بود آنک میان قومی</p></div>
<div class="m2"><p>داند که گران است گرانی بُبَرد</p></div></div>