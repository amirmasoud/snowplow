---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>نارنج و ترنج بر سر خار که دید</p></div>
<div class="m2"><p>در لانهٔ بنجشک سر مار که دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آهو به کنار یوز در خواب که دید</p></div>
<div class="m2"><p>از صد زنگی یکی وفادار که دید</p></div></div>