---
title: >-
    شمارهٔ ۱۸ - العبودیة
---
# شمارهٔ ۱۸ - العبودیة

<div class="b" id="bn1"><div class="m1"><p>جز با تو حوالتت نباشد فردا</p></div>
<div class="m2"><p>جز فعل تو آلتت نباشد فردا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنگر که خدا با تو چه کرده است امروز</p></div>
<div class="m2"><p>آن کن که خجالتت نباشد فردا</p></div></div>