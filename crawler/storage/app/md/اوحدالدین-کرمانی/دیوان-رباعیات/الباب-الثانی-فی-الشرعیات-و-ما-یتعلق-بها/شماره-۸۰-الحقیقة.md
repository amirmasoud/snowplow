---
title: >-
    شمارهٔ ۸۰ - الحقیقة
---
# شمارهٔ ۸۰ - الحقیقة

<div class="b" id="bn1"><div class="m1"><p>این کار تو را کارگزار دگر است</p></div>
<div class="m2"><p>نیک و بد تو به اختیار دگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علم و عمل و ریاضت و جهد و ثبات</p></div>
<div class="m2"><p>راه طلب است و یافت کار دگر است</p></div></div>