---
title: >-
    شمارهٔ ۷۲ - الحقیقة
---
# شمارهٔ ۷۲ - الحقیقة

<div class="b" id="bn1"><div class="m1"><p>جز نیستی تو نیست هستی به خدا</p></div>
<div class="m2"><p>ای هشیاران خوش است مستی به خدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بت زبر ای حق پرستی روزی</p></div>
<div class="m2"><p>حقّا که رسی زبت پرستی به خدا</p></div></div>