---
title: >-
    شمارهٔ ۱۳۵ - الصفا
---
# شمارهٔ ۱۳۵ - الصفا

<div class="b" id="bn1"><div class="m1"><p>رسمی است میان اهل دل دیرینه</p></div>
<div class="m2"><p>کز کینه تهی کنند دایم سینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دل همه حلم و بردباری باید</p></div>
<div class="m2"><p>صاحبدل ریش سینه اندر کینه</p></div></div>