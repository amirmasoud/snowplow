---
title: >-
    شمارهٔ ۳۴ - العبودیة
---
# شمارهٔ ۳۴ - العبودیة

<div class="b" id="bn1"><div class="m1"><p>رو در پی درد او که درمان گردی</p></div>
<div class="m2"><p>گر جز در او زنی پشیمان گردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاج سر دیگران نیرزد خاکی</p></div>
<div class="m2"><p>خاک در او باش که سلطان گردی</p></div></div>