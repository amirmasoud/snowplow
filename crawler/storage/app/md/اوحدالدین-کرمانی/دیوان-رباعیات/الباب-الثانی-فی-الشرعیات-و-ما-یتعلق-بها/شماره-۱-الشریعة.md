---
title: >-
    شمارهٔ ۱ - الشریعة
---
# شمارهٔ ۱ - الشریعة

<div class="b" id="bn1"><div class="m1"><p>اصل همه اوست و هر چه جز او فرع است</p></div>
<div class="m2"><p>هر کس که جز این داند او را صرع است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تارکیت شمع و چراغی باید</p></div>
<div class="m2"><p>جسمت شمع است و آن چراغت شرع است</p></div></div>