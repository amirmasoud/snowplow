---
title: >-
    شمارهٔ ۱۰۵ - الحقیقة
---
# شمارهٔ ۱۰۵ - الحقیقة

<div class="b" id="bn1"><div class="m1"><p>آتش نزند در دل ما الّا او</p></div>
<div class="m2"><p>کوته نکند منزل ما الّا او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جمله جهانیان طبیبم گردند</p></div>
<div class="m2"><p>حل می نکند مشکل ما الّا او</p></div></div>