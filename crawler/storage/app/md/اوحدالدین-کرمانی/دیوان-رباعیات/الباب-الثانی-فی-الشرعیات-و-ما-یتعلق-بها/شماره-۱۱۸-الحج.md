---
title: >-
    شمارهٔ ۱۱۸ - الحج
---
# شمارهٔ ۱۱۸ - الحج

<div class="b" id="bn1"><div class="m1"><p>گر عُجب زموقف دلت دور شود</p></div>
<div class="m2"><p>حج تو و عمرهٔ تو مبرور شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک وقفه طواف کعبهٔ امرش کن</p></div>
<div class="m2"><p>تا سعی تو در طواف مشکور شود</p></div></div>