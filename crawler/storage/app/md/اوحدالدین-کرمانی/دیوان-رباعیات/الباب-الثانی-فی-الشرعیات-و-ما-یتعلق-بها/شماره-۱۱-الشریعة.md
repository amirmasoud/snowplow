---
title: >-
    شمارهٔ ۱۱ - الشریعة
---
# شمارهٔ ۱۱ - الشریعة

<div class="b" id="bn1"><div class="m1"><p>چون دیدهٔ عقل راهرو بگشاید</p></div>
<div class="m2"><p>در ظلمت شب همی چراغش باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم ارچه که روشن است از نور بصر</p></div>
<div class="m2"><p>جز شرع ره راست بد و ننماید</p></div></div>