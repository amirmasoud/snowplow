---
title: >-
    شمارهٔ ۱۲۵ - طلب الآخرة
---
# شمارهٔ ۱۲۵ - طلب الآخرة

<div class="b" id="bn1"><div class="m1"><p>ترسم که اگر در طلبش نشتابی</p></div>
<div class="m2"><p>بر آتش حسرت دل خود را تابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا اینجایی ترک خوش آمد می کن</p></div>
<div class="m2"><p>تا هر چه به آمده است آنجا یابی</p></div></div>