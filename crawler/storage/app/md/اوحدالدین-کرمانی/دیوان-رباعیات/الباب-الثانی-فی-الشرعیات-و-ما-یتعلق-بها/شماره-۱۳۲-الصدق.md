---
title: >-
    شمارهٔ ۱۳۲ - الصدق
---
# شمارهٔ ۱۳۲ - الصدق

<div class="b" id="bn1"><div class="m1"><p>از صدق رهانی دل خود را از حیف</p></div>
<div class="m2"><p>وز صدق رهانی سر خود را از سیف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاید که تو حدّ صدق از من پرسی</p></div>
<div class="m2"><p>دانی چه بود صدق نگویی کم و کیف</p></div></div>