---
title: >-
    شمارهٔ ۵۰ - الطریقة
---
# شمارهٔ ۵۰ - الطریقة

<div class="b" id="bn1"><div class="m1"><p>چندان برو این ره که دوی برخیزد</p></div>
<div class="m2"><p>ور هست دوی به رهروی برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو او نشوی ولی اگر جهد کنی</p></div>
<div class="m2"><p>جایی برسی کز تو توی برخیزد</p></div></div>