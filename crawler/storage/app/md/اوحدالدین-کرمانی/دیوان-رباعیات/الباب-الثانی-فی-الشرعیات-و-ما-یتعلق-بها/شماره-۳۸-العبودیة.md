---
title: >-
    شمارهٔ ۳۸ - العبودیة
---
# شمارهٔ ۳۸ - العبودیة

<div class="b" id="bn1"><div class="m1"><p>آن کس که به بندگی قرارش باشد</p></div>
<div class="m2"><p>با چون و چرای او چه کارش باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بنده ای اختیار در باقی کن</p></div>
<div class="m2"><p>آن خواجه بود که اختیارش باشد</p></div></div>