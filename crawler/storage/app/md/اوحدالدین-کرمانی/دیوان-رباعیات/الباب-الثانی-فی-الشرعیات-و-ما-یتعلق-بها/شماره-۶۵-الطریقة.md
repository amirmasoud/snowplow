---
title: >-
    شمارهٔ ۶۵ - الطریقة
---
# شمارهٔ ۶۵ - الطریقة

<div class="b" id="bn1"><div class="m1"><p>تا بتوانی در صف مردان می باش</p></div>
<div class="m2"><p>در بزم طرب حریف سلطان می باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پردهٔ اسلام چه باشی کافر</p></div>
<div class="m2"><p>در پردهٔ کافری مسلمان می باش</p></div></div>