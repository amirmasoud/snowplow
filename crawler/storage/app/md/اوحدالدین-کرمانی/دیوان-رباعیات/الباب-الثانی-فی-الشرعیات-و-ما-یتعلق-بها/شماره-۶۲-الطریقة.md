---
title: >-
    شمارهٔ ۶۲ - الطریقة
---
# شمارهٔ ۶۲ - الطریقة

<div class="b" id="bn1"><div class="m1"><p>خیزم در دلدار زنم بوک دبو</p></div>
<div class="m2"><p>خود را به درش در افکنم بوک دبو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خود دانم که او قبولم نکند</p></div>
<div class="m2"><p>با این همه جانی بکنم بوک دبو</p></div></div>