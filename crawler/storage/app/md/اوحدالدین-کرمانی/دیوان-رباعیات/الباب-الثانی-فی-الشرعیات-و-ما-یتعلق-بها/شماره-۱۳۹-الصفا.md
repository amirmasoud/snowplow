---
title: >-
    شمارهٔ ۱۳۹ - الصفا
---
# شمارهٔ ۱۳۹ - الصفا

<div class="b" id="bn1"><div class="m1"><p>از راه صفا هر که نصیبی یابد</p></div>
<div class="m2"><p>هرگز به جواب هیچ بد نشتابد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگه که زقلّتین فزون گردد آب</p></div>
<div class="m2"><p>هر جا که کدورتی بود برتابد</p></div></div>