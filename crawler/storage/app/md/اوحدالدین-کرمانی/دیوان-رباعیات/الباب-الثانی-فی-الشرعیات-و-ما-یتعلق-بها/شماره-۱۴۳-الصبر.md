---
title: >-
    شمارهٔ ۱۴۳ - الصّبر
---
# شمارهٔ ۱۴۳ - الصّبر

<div class="b" id="bn1"><div class="m1"><p>بر مردم اهل گر بد و نیک آید</p></div>
<div class="m2"><p>گه بسته شود کار و گهی بگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم در دل تو حامله ورجای گرفت</p></div>
<div class="m2"><p>دلتنگ مشو بوک به شادی زاید</p></div></div>