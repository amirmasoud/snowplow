---
title: >-
    شمارهٔ ۵۲ - الطریقة
---
# شمارهٔ ۵۲ - الطریقة

<div class="b" id="bn1"><div class="m1"><p>هر دل شده ای چهره به خون می شوید</p></div>
<div class="m2"><p>هر غمزده ای ترانه ای می گوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر راهی که رهروی می پوید</p></div>
<div class="m2"><p>چون نیک نگه کنم تو را می جوید</p></div></div>