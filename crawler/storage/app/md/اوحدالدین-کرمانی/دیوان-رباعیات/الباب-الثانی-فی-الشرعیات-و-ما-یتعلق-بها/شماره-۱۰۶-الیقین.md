---
title: >-
    شمارهٔ ۱۰۶ - الیقین
---
# شمارهٔ ۱۰۶ - الیقین

<div class="b" id="bn1"><div class="m1"><p>هر چیز که او گفت چنان است همه</p></div>
<div class="m2"><p>آن است همه دگر گمان است همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این قدر یقین بدان که هر سود که هست</p></div>
<div class="m2"><p>گر نیست سزای او زیان است همه</p></div></div>