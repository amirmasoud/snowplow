---
title: >-
    شمارهٔ ۹۳ - الحقیقة
---
# شمارهٔ ۹۳ - الحقیقة

<div class="b" id="bn1"><div class="m1"><p>در باغ طلب اگر نباتی یابی</p></div>
<div class="m2"><p>هر لحظه ازو تازه نباتی یابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که تو بی نفاذ ذاتی یابی</p></div>
<div class="m2"><p>بی مرگ بمیر تا حیاتی یابی</p></div></div>