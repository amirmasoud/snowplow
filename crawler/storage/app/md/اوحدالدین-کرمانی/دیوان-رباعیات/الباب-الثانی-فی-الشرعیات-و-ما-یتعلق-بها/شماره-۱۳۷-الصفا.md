---
title: >-
    شمارهٔ ۱۳۷ - الصفا
---
# شمارهٔ ۱۳۷ - الصفا

<div class="b" id="bn1"><div class="m1"><p>نه هر که میان ببندد از کفار است</p></div>
<div class="m2"><p>یا هر زاهد زسبحه برخوردار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دل به صفای حق نباشد روشن</p></div>
<div class="m2"><p>در گردن شیخ طیلسان زنّار است</p></div></div>