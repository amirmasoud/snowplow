---
title: >-
    شمارهٔ ۷۵ - الحقیقة
---
# شمارهٔ ۷۵ - الحقیقة

<div class="b" id="bn1"><div class="m1"><p>از کوی و مکان گذشت آب و گل ما</p></div>
<div class="m2"><p>وز وصف و بیان گذشت حال دل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را زقبول و [ردّ کس] باکی نیست</p></div>
<div class="m2"><p>چون دلبر ما همره و هم منزل ما</p></div></div>