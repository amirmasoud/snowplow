---
title: >-
    شمارهٔ ۱۴۶ - الصّبر
---
# شمارهٔ ۱۴۶ - الصّبر

<div class="b" id="bn1"><div class="m1"><p>دل خوش کن و بر صبر گمار اندیشه</p></div>
<div class="m2"><p>یعنی که دگر به دل مدار اندیشه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو صبر و کدام دل، چه می گویی تو</p></div>
<div class="m2"><p>یک قطرهٔ خون است و هزار اندیشه</p></div></div>