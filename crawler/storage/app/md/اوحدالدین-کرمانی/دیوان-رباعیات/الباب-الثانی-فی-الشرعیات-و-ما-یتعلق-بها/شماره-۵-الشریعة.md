---
title: >-
    شمارهٔ ۵ - الشریعة
---
# شمارهٔ ۵ - الشریعة

<div class="b" id="bn1"><div class="m1"><p>هر چند به عقل راه می شاید دید</p></div>
<div class="m2"><p>بی رهبر شرع کس به جایی نرسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرگردان بود عقل بی رهبر شرع</p></div>
<div class="m2"><p>سردار وجود شد چو در شرع رسید</p></div></div>