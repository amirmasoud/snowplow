---
title: >-
    شمارهٔ ۳۶ - العبودیة
---
# شمارهٔ ۳۶ - العبودیة

<div class="b" id="bn1"><div class="m1"><p>در بندگیش اگر تو نیکو باشی</p></div>
<div class="m2"><p>فرمان ده این طارم نُه تو باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول قدم آن است که او را طلبی</p></div>
<div class="m2"><p>آخر قدم آن بود که تو او باشی</p></div></div>