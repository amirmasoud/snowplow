---
title: >-
    شمارهٔ ۷۷ - الحقیقة
---
# شمارهٔ ۷۷ - الحقیقة

<div class="b" id="bn1"><div class="m1"><p>از عالم کفر تا به دین یک نفس است</p></div>
<div class="m2"><p>وز منزل شک تا به یقین یک نفس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این یک نفس عزیز را خوار مدار</p></div>
<div class="m2"><p>چون حاصل عمر ما همین یک نفس است</p></div></div>