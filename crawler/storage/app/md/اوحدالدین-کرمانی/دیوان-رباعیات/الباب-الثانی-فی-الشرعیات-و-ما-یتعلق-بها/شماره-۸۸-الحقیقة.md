---
title: >-
    شمارهٔ ۸۸ - الحقیقة
---
# شمارهٔ ۸۸ - الحقیقة

<div class="b" id="bn1"><div class="m1"><p>ای مؤمن محض بودنت مطلق گبر</p></div>
<div class="m2"><p>گاهی به قَدَر دست بزن گاه به جبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر کثرت حرص تست و بر قلّت صبر</p></div>
<div class="m2"><p>گر خندهٔ برق است و گر گریهٔ ابر</p></div></div>