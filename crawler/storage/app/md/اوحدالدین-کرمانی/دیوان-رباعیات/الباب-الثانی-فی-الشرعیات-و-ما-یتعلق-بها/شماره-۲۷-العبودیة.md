---
title: >-
    شمارهٔ ۲۷ - العبودیة
---
# شمارهٔ ۲۷ - العبودیة

<div class="b" id="bn1"><div class="m1"><p>می دان که اگر او دمی آن تو شود</p></div>
<div class="m2"><p>کار دل تو به کام جان تو شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود را باشی تو را به خود باز هلد</p></div>
<div class="m2"><p>او را شو تا او همه آن تو شود</p></div></div>