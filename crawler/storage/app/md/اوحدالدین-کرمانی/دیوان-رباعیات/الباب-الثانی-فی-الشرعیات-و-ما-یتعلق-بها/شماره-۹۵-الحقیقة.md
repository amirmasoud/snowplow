---
title: >-
    شمارهٔ ۹۵ - الحقیقة
---
# شمارهٔ ۹۵ - الحقیقة

<div class="b" id="bn1"><div class="m1"><p>یاری که وجود و عدمت اوست همه</p></div>
<div class="m2"><p>سرمایهٔ شادی و غمت اوست همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو دیده نداری که بدو درنگری</p></div>
<div class="m2"><p>ورنه زسرت تا قدمت اوست همه</p></div></div>