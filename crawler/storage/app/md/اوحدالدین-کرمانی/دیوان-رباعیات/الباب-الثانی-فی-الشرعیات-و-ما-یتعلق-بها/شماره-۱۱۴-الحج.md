---
title: >-
    شمارهٔ ۱۱۴ - الحج
---
# شمارهٔ ۱۱۴ - الحج

<div class="b" id="bn1"><div class="m1"><p>در راه خدا دو کعبه آمد حاصل</p></div>
<div class="m2"><p>یک کعبهٔ صورتی و یک کعبهٔ دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بتوانی زیارت دلها کن</p></div>
<div class="m2"><p>کافزون زهزار کعبه آید یک دل</p></div></div>