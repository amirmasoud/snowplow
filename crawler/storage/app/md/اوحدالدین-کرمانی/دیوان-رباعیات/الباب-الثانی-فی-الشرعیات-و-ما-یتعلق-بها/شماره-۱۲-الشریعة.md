---
title: >-
    شمارهٔ ۱۲ - الشریعة
---
# شمارهٔ ۱۲ - الشریعة

<div class="b" id="bn1"><div class="m1"><p>از عقل مجرّد به دوایی نرسی</p></div>
<div class="m2"><p>بی شرع به برگی و نوایی نرسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرع است که آن تو را رساند به خدا</p></div>
<div class="m2"><p>ورنی تو بدین عقل به جایی نرسی</p></div></div>