---
title: >-
    شمارهٔ ۱۱۲ - التوبه
---
# شمارهٔ ۱۱۲ - التوبه

<div class="b" id="bn1"><div class="m1"><p>گر تو به سر راه خرد وانرسی</p></div>
<div class="m2"><p>در درد بمیری به مداوا نرسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر شب گویی که توبه فردا بکنم</p></div>
<div class="m2"><p>توبه که کند گر تو به فردا نَرسی</p></div></div>