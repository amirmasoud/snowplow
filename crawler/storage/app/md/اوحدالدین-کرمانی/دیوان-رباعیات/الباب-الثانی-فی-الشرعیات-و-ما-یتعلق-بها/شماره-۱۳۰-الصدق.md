---
title: >-
    شمارهٔ ۱۳۰ - الصدق
---
# شمارهٔ ۱۳۰ - الصدق

<div class="b" id="bn1"><div class="m1"><p>صدق است که مرد را همی بخشد جان</p></div>
<div class="m2"><p>از صدق بود همیشه دشوار آسان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دوست در آن کوش که صادق باشی</p></div>
<div class="m2"><p>از صدق ملک شود حقیقت انسان</p></div></div>