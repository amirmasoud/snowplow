---
title: >-
    شمارهٔ ۷۶ - الحقیقة
---
# شمارهٔ ۷۶ - الحقیقة

<div class="b" id="bn1"><div class="m1"><p>بی می همه نوبهار عالم دی توست</p></div>
<div class="m2"><p>در صحبت می دو کون ادنی شی توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از می همه لعل آب روان فهم مکن</p></div>
<div class="m2"><p>هر چه از تو تو را بازستاند می توست</p></div></div>