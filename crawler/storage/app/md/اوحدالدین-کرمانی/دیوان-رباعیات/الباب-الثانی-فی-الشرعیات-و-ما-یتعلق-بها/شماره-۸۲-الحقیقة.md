---
title: >-
    شمارهٔ ۸۲ - الحقیقة
---
# شمارهٔ ۸۲ - الحقیقة

<div class="b" id="bn1"><div class="m1"><p>این مردمک دیده سحرگه برخاست</p></div>
<div class="m2"><p>برخاست صَلای عاشقان از چپ و راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که تیمم کنم از خاک درش</p></div>
<div class="m2"><p>دل گفت که غسل کن کنار دریاست</p></div></div>