---
title: >-
    شمارهٔ ۱۲۳ - طلب الآخرة
---
# شمارهٔ ۱۲۳ - طلب الآخرة

<div class="b" id="bn1"><div class="m1"><p>زان پیش که گویند که جا واپرداز</p></div>
<div class="m2"><p>جا واپرداز و توشهٔ راه بساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون می دانی که خانه پرداختنی است</p></div>
<div class="m2"><p>چندین غم خانه چیست با خود پرداز</p></div></div>