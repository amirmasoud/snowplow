---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>تا بستهٔ جان و خستهٔ تن باشم</p></div>
<div class="m2"><p>در دوستی ات به کام دشمن باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خود چو برون شوم تو را می بینم</p></div>
<div class="m2"><p>پس پرده میان من و تو من باشم</p></div></div>