---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>از آتش حرص و آز تا چند نفیر</p></div>
<div class="m2"><p>ای آب ز روی رفته پندی بپذیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خوار چو خاک راه تا چند امیر</p></div>
<div class="m2"><p>ای عمر به باد داده میری کم گیر</p></div></div>