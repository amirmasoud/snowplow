---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>گر نفس وجود خویشتن استردی</p></div>
<div class="m2"><p>یکباره ازین گلخن تن جان بردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش از مردن بمیر و جاوید بمان</p></div>
<div class="m2"><p>ورنه پس از آن مرگ چو مردی مردی</p></div></div>