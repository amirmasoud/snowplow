---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>دین داری را ز بت پرستی بشناس</p></div>
<div class="m2"><p>هشیاری را اگر نه مستی بشناس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانم که مرا نمی شناسی به یقین</p></div>
<div class="m2"><p>باری خود را چنانک هستی بشناس</p></div></div>