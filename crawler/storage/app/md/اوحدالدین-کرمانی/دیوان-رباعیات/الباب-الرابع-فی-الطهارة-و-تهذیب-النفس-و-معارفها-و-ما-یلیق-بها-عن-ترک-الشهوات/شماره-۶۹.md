---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>بوی دم مقبلان چو گل خوش باشد</p></div>
<div class="m2"><p>بدبخت چو خار تیز و سرکش باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از صحبت گل خار زآتش برهد</p></div>
<div class="m2"><p>وز صحبت خار گل در آتش باشد</p></div></div>