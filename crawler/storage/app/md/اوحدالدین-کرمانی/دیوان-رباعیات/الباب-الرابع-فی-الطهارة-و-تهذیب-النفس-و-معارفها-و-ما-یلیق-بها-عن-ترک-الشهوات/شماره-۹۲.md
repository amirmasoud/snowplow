---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>ای دل همه کار تو به بالا شده گیر</p></div>
<div class="m2"><p>اسباب تو یک هفته مهیّا شده گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تخت ثری تا به ثریّا شده گیر</p></div>
<div class="m2"><p>امروز چو دی گذشت فردا شده گیر</p></div></div>