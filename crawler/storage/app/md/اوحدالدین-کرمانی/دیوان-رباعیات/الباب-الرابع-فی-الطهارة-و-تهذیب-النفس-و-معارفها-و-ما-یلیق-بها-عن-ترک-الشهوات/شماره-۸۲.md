---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>دنیا مطلب تا همه دینت باشد</p></div>
<div class="m2"><p>دنیا طلبی نه آن نه اینت باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر روی زمین زیر زمین وار بزی</p></div>
<div class="m2"><p>تا زیر زمین روی زمینت باشد</p></div></div>