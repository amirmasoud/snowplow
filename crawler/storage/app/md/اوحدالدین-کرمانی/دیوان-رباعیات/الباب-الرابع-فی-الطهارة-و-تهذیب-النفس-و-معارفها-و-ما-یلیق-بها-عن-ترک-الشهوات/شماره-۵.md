---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>سبحان الله که چه زیانم خود را</p></div>
<div class="m2"><p>بر باد هوا همی نشانم خود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیکان خود را هنوز بد دانستند</p></div>
<div class="m2"><p>من نیک بدم و نیک دانم خود را</p></div></div>