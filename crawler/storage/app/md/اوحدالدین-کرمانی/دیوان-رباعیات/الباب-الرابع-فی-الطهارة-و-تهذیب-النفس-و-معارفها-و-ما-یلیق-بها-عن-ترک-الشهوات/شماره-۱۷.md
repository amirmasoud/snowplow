---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>فارغ منشین ز راه و اندر ره باش</p></div>
<div class="m2"><p>غافل زتو نیست کردگار آگه باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن باش که هستی و جز آنگه باشی (؟)</p></div>
<div class="m2"><p>لیکن تو بدان که چیستی آنگه باش</p></div></div>