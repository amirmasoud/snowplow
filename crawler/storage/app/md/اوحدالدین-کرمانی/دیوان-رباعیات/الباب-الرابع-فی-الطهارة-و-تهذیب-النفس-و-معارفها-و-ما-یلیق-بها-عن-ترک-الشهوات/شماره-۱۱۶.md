---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>هرگه کآید زبحر ربّانی سیل</p></div>
<div class="m2"><p>دیگر نکند این سگ نفسانی میل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حقّا که به لب رسید این روح عزیز</p></div>
<div class="m2"><p>زین سگ که هزار خوک دارد در خیل</p></div></div>