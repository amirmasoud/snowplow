---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>جهدی بکن ای خواجه درین عالم دون</p></div>
<div class="m2"><p>بیرون افتی که نیست این جای سکون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور زانک به اختیار بیرون نشوی</p></div>
<div class="m2"><p>دست اجلت کند به سیلی بیرون</p></div></div>