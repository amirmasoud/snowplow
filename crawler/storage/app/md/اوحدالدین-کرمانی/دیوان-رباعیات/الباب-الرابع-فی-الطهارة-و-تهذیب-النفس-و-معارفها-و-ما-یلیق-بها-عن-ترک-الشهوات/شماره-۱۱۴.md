---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>جز قطع نظر به کام رهرو نکند</p></div>
<div class="m2"><p>واین کوی وصال غیر او هو نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پروانهٔ فقر را ندیده است کسی</p></div>
<div class="m2"><p>تا قطع نظر زکهنه و نو نکند</p></div></div>