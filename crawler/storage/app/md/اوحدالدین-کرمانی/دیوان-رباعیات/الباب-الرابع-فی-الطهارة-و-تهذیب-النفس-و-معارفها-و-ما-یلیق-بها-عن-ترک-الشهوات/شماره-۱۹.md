---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>چون یک نفس از وجود خود برگذرم</p></div>
<div class="m2"><p>خود را به دمی هزار منزل بُبَرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس باز به یک نظر که با خود نگرم</p></div>
<div class="m2"><p>یک ساعته راه را به سالی سپَرم</p></div></div>