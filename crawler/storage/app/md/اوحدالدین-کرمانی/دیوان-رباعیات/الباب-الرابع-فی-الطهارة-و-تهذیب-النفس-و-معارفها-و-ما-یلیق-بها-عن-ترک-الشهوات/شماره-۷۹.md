---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>زین مرتبه و قاعدهٔ بَردابَرد</p></div>
<div class="m2"><p>ایمن منشین ز دولت کرداکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل شاد بزی به کام دل مردامرد</p></div>
<div class="m2"><p>چیزی که کنی کزو شوی مردامرد</p></div></div>