---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>کامل زیکی هنر ده و صد بیند</p></div>
<div class="m2"><p>ناقص همه جا معایب خود بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلق آینهٔ چشم و دل همدگرند</p></div>
<div class="m2"><p>در آینه نیک نیک و بد بد بیند</p></div></div>