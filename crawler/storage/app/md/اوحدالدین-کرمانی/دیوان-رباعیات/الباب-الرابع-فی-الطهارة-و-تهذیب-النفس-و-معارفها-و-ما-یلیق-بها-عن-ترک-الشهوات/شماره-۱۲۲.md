---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>من پیرو طبعم این ضلالت زآن است</p></div>
<div class="m2"><p>بی حاصلم از عمر ملالت زآن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بی سودی نمی خورم چندین غم</p></div>
<div class="m2"><p>سرمایه زیان است خجالت زآن است</p></div></div>