---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>ای خواجَه اگر تو را سَعادت خویش است</p></div>
<div class="m2"><p>ایمن منشین زآنچ تو را در پیش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زینها که تو مال و ملک می پنداری</p></div>
<div class="m2"><p>جز مرداری و مرد ریگی بیش است</p></div></div>