---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>این ره نبرد مگر به سر ناپاکی</p></div>
<div class="m2"><p>شوخی، شنگی، قلندری، بی باکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکت بر سر حدیث سر چند کنی</p></div>
<div class="m2"><p>آنجا که هزار سر نیرزد خاکی</p></div></div>