---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>گر عالم را زبهر تو آرایند</p></div>
<div class="m2"><p>مگر ای که عاقلان بدو نگرایند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسیار چو تو روند و بسیار آیند</p></div>
<div class="m2"><p>بر بای نصیب خویش کت بربایند</p></div></div>