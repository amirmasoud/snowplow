---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>بگذار بدی که در من از وی صد نیست</p></div>
<div class="m2"><p>چد بد که مرا امید نیکی خود نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افسوس که خلق را امید نیک است</p></div>
<div class="m2"><p>اندر من و سرمایهٔ من جز بد نیست</p></div></div>