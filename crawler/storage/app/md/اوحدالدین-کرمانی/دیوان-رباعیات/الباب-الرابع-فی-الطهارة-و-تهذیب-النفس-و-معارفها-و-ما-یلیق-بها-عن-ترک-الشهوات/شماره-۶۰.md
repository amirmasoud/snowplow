---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>چون آتش شهوت آبرویت را برد</p></div>
<div class="m2"><p>در معرض هر بزرگ ماندی تو چو خرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کوش که باد نفس را خاک کنی</p></div>
<div class="m2"><p>هر زنده که آن نکرد در عقبی مُرد</p></div></div>