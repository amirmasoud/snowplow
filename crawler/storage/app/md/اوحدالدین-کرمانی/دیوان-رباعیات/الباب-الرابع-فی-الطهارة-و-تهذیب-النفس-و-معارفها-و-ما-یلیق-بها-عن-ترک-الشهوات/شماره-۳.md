---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>مرد آن نبود که ظاهر آرای بود</p></div>
<div class="m2"><p>تا در دل و جان مردمش جای بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردانه درآی و باطن آرایی کن</p></div>
<div class="m2"><p>کآن زن باشد که ظاهر آرای بود</p></div></div>