---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>علم عُلوی و سُفلی آموخته گیر</p></div>
<div class="m2"><p>واموال جهان جمله تو اندوخته گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناگاه اجلی آتش افروخته گیر</p></div>
<div class="m2"><p>آموخته و اندوخته را سوخته گیر</p></div></div>