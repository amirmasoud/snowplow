---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>ای دل دل خسته بر جهان بیش منه</p></div>
<div class="m2"><p>وای کاه ضعیف کوه بر خویش منه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوته تر از آن است که می دانی عمر</p></div>
<div class="m2"><p>چندان امل دراز در پیش منه</p></div></div>