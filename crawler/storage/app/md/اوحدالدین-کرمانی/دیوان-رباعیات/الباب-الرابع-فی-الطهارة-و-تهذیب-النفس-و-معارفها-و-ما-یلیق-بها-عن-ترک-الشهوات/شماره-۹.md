---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>می باید ساختن گرت برگ صفاست</p></div>
<div class="m2"><p>با نیک و بد و خرد و بزرگ و کژ و راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با آتش و آب و باد باید بودن</p></div>
<div class="m2"><p>واندر حرکت چو گرد باید برخاست</p></div></div>