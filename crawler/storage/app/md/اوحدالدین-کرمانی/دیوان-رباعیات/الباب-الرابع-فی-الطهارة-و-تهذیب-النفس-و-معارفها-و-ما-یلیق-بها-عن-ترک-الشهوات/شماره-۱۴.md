---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>آنها که مرا بنیک می پندارند</p></div>
<div class="m2"><p>تخم سره را به جای بد می کارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر پرده ز روی کار من بردارند</p></div>
<div class="m2"><p>در هیچ خرابه ای مرا نگذارند</p></div></div>