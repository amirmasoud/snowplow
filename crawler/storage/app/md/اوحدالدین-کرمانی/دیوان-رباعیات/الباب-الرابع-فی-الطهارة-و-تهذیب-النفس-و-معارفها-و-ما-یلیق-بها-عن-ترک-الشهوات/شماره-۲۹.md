---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>تو آلت فعل و در میان هیچ نئی</p></div>
<div class="m2"><p>وز فاعل و فعل جز نشان هیچ نئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو عالمی و مراد از عالم تو</p></div>
<div class="m2"><p>چون درنگری درین میان هیچ نئی</p></div></div>