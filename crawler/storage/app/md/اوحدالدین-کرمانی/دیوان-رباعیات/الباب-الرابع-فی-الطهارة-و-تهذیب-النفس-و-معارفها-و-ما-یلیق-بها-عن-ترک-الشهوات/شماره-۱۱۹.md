---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>فریاد از آنچ نیست و می خوانندم</p></div>
<div class="m2"><p>زاهد نیم و بزهد می دانندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زانک درون من برون گردانند</p></div>
<div class="m2"><p>مستوجب آنم که بسوزانندم</p></div></div>