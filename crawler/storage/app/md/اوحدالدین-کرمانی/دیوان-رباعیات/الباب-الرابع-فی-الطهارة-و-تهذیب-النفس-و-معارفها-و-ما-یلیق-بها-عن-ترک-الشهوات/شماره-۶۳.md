---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>ای دل می وصل بی خمارت ندهند</p></div>
<div class="m2"><p>بی زحمت دِی هیچ بهارت ندهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر با تو هوای سوزنی خواهد بود</p></div>
<div class="m2"><p>گر عیسی مریمی که بارت ندهند</p></div></div>