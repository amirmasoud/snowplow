---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>از بَهر جهانی که تو هیچی دَروی</p></div>
<div class="m2"><p>آزار کسی چرا بسیچی دَر وی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فی الجمله به جملگی تو را گیر جهان</p></div>
<div class="m2"><p>بگذاری و بگذری چه پیچی دَروی</p></div></div>