---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>تا بتوانی به طبع خود کار مکن</p></div>
<div class="m2"><p>البته رفیق بد به خود یار مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی که رفیق بد که را می گویم</p></div>
<div class="m2"><p>نفس تو به قول نفس تو کار مکن</p></div></div>