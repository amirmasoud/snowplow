---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>دنیا که جوی وفا ندارد در پوست</p></div>
<div class="m2"><p>هر لحظه هزار مغز سرگشتهٔ اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندین که خدای دشمنش می دارد</p></div>
<div class="m2"><p>گر دشمن حق نئی چرا داری دوست</p></div></div>