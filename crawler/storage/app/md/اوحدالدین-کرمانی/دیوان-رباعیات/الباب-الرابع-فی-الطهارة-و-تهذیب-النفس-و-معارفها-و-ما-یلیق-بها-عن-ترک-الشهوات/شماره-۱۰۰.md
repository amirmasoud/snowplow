---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>تا دل زعلایق جهان حُر نشود</p></div>
<div class="m2"><p>هرگز شبه وجود ما دُر نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر می نشود کاسهٔ سرها زهوس</p></div>
<div class="m2"><p>هر کاسه که سرنگون بود پر نشود</p></div></div>