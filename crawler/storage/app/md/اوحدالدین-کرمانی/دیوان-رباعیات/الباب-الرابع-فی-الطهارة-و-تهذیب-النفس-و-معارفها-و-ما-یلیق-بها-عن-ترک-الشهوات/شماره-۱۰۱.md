---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>دنیا کَزِ تست بَهر بیشی و کمی</p></div>
<div class="m2"><p>خواهیش به شادی گذ[ر]ان خوه به غمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین منزلت البته چو می باید رفت</p></div>
<div class="m2"><p>خواهی به هزار سال خواهی به دمی</p></div></div>