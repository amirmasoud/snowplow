---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>عمر از پی افزودن زر کاسته گیر</p></div>
<div class="m2"><p>گنجی به هزار حیله آراسته گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو بر سر آن گنج چو در صحرا برف</p></div>
<div class="m2"><p>روزی دو سه بنشسته و برخاسته گیر</p></div></div>