---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>آنها که زدام بُت پرستی جستند</p></div>
<div class="m2"><p>بردل در نیستی و هستی بستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پا بر سر و روی جمله اسباب زدند</p></div>
<div class="m2"><p>وز تنگ دلی و تنگ دستی رستند</p></div></div>