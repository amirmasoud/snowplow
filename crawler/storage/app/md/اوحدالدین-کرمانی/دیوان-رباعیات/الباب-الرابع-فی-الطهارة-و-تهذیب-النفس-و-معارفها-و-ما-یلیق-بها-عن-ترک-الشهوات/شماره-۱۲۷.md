---
title: >-
    شمارهٔ ۱۲۷
---
# شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>دل از پی آب و نان در آتش نبود</p></div>
<div class="m2"><p>چون حال پریشان و مشوش نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیرانه به کنجی به سکونت بنشین</p></div>
<div class="m2"><p>کز موی سپید کودکی خوش نبود</p></div></div>