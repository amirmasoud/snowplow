---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>عمری به غلط سوخته خرمن بودم</p></div>
<div class="m2"><p>در دوستی ات به کام دشمن بودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون چشم من از خاک درت روشن شد</p></div>
<div class="m2"><p>دیدم به یقین حجاب من من بودم</p></div></div>