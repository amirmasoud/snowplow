---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>هر چند چو خاک ره عَناکش باشی</p></div>
<div class="m2"><p>ور باد جفای دَهر ناخوش باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار زدست ناکسان آب حیات</p></div>
<div class="m2"><p>بر لب مچکان گرچه در آتش باشی</p></div></div>