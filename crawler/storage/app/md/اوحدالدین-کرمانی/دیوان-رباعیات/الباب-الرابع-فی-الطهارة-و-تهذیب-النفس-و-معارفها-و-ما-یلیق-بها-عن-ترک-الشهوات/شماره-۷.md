---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>تا ظن نبری که خوی دد نیست مرا</p></div>
<div class="m2"><p>یا آلت جنگ یک دو صد نیست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بد زآن نکنم که بد کنم بد باشد</p></div>
<div class="m2"><p>واین عادت بد که نیست بد نیست مرا</p></div></div>