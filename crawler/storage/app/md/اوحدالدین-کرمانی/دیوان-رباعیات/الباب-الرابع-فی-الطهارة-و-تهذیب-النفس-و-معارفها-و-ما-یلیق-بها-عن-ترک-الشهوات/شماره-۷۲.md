---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>در راه یقین گمان نباشد کس را</p></div>
<div class="m2"><p>با شک و یقین امان نباشد کس را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دنیاطلبان زآخرت محرومند</p></div>
<div class="m2"><p>ای دوست همین و آن نباشد کس را</p></div></div>