---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>صد بار بگفتم این دل سوخته را</p></div>
<div class="m2"><p>کآبی برزن آتش افروخته را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشنید و به باد خاکساری برداد</p></div>
<div class="m2"><p>این جانِ به صد خون دل اندوخته را</p></div></div>