---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>آن را که دل از راهِ صفا پر نور است</p></div>
<div class="m2"><p>از طبع و هوا و خشم و خشیت دور است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وَر از سبکی کند کسی بی ادبی</p></div>
<div class="m2"><p>او نیز در آن بی ادبی معذور است</p></div></div>