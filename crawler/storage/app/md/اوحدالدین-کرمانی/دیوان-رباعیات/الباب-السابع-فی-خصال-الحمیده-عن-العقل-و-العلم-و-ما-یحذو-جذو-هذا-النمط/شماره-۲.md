---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>در جُستن راه شرع عقل اولی تر</p></div>
<div class="m2"><p>وز منزل طبع خویش نقل اولی تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شک نیست که چون رسد ودادی باشد</p></div>
<div class="m2"><p>شاهد باشد ولیک عقل اولی تر</p></div></div>