---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>شکرانهٔ آنک خواجهٔ بنده نئی</p></div>
<div class="m2"><p>وندر پی رزق خود پراکنده نئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خواهندت بده که ملکی است عظیم</p></div>
<div class="m2"><p>آخر تو چو او نیز تو خواهنده نئی</p></div></div>