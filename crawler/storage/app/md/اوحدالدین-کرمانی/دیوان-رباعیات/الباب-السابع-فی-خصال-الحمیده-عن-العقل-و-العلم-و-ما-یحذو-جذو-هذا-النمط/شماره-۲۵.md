---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>ای دل اگرت بصیرت حق بین است</p></div>
<div class="m2"><p>پیوسته براق همّتت در زین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون مور میان ببند در خدمت خلق</p></div>
<div class="m2"><p>کان ملک سلیمان که شنیدی این است</p></div></div>