---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>این راه طریقت نه به پای عقل است</p></div>
<div class="m2"><p>خاک قدم عشق ورای عقل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرّی که زسر فرشته زان بی خبر است</p></div>
<div class="m2"><p>ای عقلک بی عقل چه جای عقل است</p></div></div>