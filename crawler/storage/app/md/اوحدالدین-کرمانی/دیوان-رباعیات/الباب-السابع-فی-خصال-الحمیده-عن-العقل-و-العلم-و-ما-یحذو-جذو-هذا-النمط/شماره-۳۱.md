---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>از علم همه حلم و تواضع زاید</p></div>
<div class="m2"><p>وز جهل همه گَند دماغ افزاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذار دماغ اگر که علمت باید</p></div>
<div class="m2"><p>پوسیده دماغ علم را کی شاید</p></div></div>