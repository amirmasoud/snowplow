---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>تا بتوانی خسته مگردان کس را</p></div>
<div class="m2"><p>بر آتش خشم خویش منشان کس را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر راحت جاوید طمع می داری</p></div>
<div class="m2"><p>می رنج همیشه و مرنجان کس را</p></div></div>