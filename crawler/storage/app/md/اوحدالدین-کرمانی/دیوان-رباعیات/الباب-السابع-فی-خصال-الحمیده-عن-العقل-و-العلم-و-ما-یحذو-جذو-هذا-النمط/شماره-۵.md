---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>با اهل خیال اگر در آویزد عقل</p></div>
<div class="m2"><p>شاید که همیشه خون جان ریزد عقل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با عاشق گرم رو کجا دارد پای</p></div>
<div class="m2"><p>چون رخت نهاد عشق بگریزد عقل</p></div></div>