---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>ای دوست من از هیچ مشوّش گردم</p></div>
<div class="m2"><p>وز نیمهٔ نیم ذرّه دلخوش گردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آب لطیف تر مزاجی دارم</p></div>
<div class="m2"><p>دریاب مرا وگرنه آتش گردم</p></div></div>