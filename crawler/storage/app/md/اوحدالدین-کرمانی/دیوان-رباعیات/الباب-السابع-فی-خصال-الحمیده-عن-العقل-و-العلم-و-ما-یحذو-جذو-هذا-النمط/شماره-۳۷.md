---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>در راه تواضع ار سرافکنده شوی</p></div>
<div class="m2"><p>سردار سلاطین شوی اربنده شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زنده دلی به دستت افتد روزی</p></div>
<div class="m2"><p>در پاش بمیر تا مگر زنده شوی</p></div></div>