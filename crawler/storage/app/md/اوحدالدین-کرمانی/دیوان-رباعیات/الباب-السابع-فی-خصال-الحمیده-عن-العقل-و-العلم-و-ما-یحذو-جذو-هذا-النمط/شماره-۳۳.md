---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>شادی طلبی برو گدای همه باش</p></div>
<div class="m2"><p>بیگانه خویش و آشنای همه باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که تو را چو دیده بر سر دارند</p></div>
<div class="m2"><p>دست همه بوس و خاک پای همه باش</p></div></div>