---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>هرگز نبود که در دلم جان نشوی</p></div>
<div class="m2"><p>از گریهٔ زار من تو خندان نشوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آری پس از این جهان جهانی دگر است</p></div>
<div class="m2"><p>با دوست چنان زی که پشیمان نشوی</p></div></div>