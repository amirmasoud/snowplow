---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>علمی که ازو گره گشاید بطلب</p></div>
<div class="m2"><p>زان پیش که جان و تن برآید بطلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این نیست که هست می نماید بگذار</p></div>
<div class="m2"><p>وان هست که نیست می نماید بطلب</p></div></div>