---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>گر بر عملند خلق اگر معزولند</p></div>
<div class="m2"><p>در می نگرم جمله بدو مشغولند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن مذهب تست به گزینی کردن</p></div>
<div class="m2"><p>زینجا که منم جمله جهان مقبولند</p></div></div>