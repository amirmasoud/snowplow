---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>هر کس که زمام نفس محکم گیرد</p></div>
<div class="m2"><p>باید که شراب وصل آن دم گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کس که بزرگ جمع خواهد خود را</p></div>
<div class="m2"><p>باید که زجمله خویش را کم گیرد</p></div></div>