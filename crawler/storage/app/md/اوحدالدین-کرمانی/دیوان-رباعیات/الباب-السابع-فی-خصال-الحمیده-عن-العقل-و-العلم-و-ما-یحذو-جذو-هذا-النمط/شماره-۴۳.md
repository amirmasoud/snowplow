---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>گفتم که یکی روز بپرسم خبرش</p></div>
<div class="m2"><p>تابوک برون رود تکبّر زسرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون گشت کرشمه هر زمان افزونش</p></div>
<div class="m2"><p>اکنون من و زاری و شفیعان درش</p></div></div>