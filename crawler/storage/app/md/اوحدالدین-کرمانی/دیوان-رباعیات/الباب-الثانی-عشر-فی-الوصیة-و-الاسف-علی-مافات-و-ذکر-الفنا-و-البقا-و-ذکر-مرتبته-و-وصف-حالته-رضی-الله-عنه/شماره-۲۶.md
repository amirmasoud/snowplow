---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>با دست تهی پرهوسان را چه دهند</p></div>
<div class="m2"><p>پیداست که بی دسترسان را چه دهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند مرا که با تو دلدار چه کرد</p></div>
<div class="m2"><p>من هیچ کسم هیچ کسان را چه دهند</p></div></div>