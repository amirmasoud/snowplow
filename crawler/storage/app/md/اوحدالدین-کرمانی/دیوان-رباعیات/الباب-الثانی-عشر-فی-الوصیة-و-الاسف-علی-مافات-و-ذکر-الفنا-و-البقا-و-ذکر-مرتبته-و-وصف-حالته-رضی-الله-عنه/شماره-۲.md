---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>پیوسته چو باشی تو به بازی مشغول</p></div>
<div class="m2"><p>هرگز بر حق نباشدت هیچ قبول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انگار که امروز قیامت برخاست</p></div>
<div class="m2"><p>گویند چه کرده ای، چه گویی تو فضول</p></div></div>