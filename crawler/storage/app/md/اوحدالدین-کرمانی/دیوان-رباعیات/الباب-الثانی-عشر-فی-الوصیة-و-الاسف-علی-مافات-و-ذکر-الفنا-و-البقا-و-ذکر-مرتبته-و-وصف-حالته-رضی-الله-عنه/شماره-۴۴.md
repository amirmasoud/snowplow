---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>چون معترفم بدانچ سرمستی هست</p></div>
<div class="m2"><p>در حضرتت افلاس و تهیدستی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من آن توم تو را چه باشد هیچی</p></div>
<div class="m2"><p>تو آن منی مرا همه هستی هست</p></div></div>