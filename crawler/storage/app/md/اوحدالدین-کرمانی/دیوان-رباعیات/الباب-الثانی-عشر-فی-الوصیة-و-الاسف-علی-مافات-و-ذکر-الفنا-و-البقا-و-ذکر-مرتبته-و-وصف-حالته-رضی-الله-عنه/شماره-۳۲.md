---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>بر سبزه چو چشم ابر نوروز گریست</p></div>
<div class="m2"><p>بی وصل رخ یار نمی شاید زیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد لاله زخاک دیگران مجلس ما</p></div>
<div class="m2"><p>تا سبزهٔ گور ما تماشاگه کیست</p></div></div>