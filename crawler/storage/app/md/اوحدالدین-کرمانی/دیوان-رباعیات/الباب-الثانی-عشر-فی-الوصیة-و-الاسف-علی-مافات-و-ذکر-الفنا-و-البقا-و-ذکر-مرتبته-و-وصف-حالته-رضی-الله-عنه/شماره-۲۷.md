---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>از دست اجل جان نبرد زاینده</p></div>
<div class="m2"><p>بر کس بنماند این جهان پاینده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر باد نهاده شد بنای من و تو</p></div>
<div class="m2"><p>بر باد بنا کجا بود پاینده</p></div></div>