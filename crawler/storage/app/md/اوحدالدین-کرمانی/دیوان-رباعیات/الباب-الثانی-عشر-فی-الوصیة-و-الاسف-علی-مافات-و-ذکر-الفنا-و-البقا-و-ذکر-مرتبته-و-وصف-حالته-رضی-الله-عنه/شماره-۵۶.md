---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>دل بر طرب و عیش نهادن بهتر</p></div>
<div class="m2"><p>از جان گره غمان گشادن بهتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دست چو هر چه هست خواهد رفتن</p></div>
<div class="m2"><p>آخر نه به دست خویش دادن بهتر</p></div></div>