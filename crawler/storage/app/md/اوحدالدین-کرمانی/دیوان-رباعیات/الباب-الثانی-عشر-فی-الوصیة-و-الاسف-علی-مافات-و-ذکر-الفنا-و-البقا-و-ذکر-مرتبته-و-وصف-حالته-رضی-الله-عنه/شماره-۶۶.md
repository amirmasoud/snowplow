---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>در دست زمانه سخت مظلومم من</p></div>
<div class="m2"><p>ورنه چه سزای خطّهٔ رومم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با صد هنرم هزار غم باید خورد</p></div>
<div class="m2"><p>یا رب که چه محروم و چه محرومم من</p></div></div>