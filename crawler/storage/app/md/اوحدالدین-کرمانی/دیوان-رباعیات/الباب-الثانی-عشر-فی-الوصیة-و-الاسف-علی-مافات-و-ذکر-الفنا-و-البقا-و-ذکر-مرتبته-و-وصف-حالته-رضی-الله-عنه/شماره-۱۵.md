---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>عمری که به یاد این و آن می‌گذرد</p></div>
<div class="m2"><p>گویی باد است در خزان می‌گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برمی‌گذری تو از جهان چون شب و روز</p></div>
<div class="m2"><p>می‌پنداری که این جهان می‌گذرد</p></div></div>