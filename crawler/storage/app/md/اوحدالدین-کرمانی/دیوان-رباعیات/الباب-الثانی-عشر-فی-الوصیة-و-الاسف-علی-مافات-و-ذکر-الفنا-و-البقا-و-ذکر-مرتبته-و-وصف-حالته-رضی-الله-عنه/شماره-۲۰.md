---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>بر اهل هنر کار پریشان بهتر</p></div>
<div class="m2"><p>اومید کمال نیست نقصان بهتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک لقمهٔ نان خشک نزد عقلا</p></div>
<div class="m2"><p>بی درد سر از ملک سلیمان بهتر</p></div></div>