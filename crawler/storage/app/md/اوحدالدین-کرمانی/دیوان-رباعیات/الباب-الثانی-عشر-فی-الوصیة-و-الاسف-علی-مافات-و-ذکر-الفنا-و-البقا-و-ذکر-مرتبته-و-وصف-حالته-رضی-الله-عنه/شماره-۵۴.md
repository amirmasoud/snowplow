---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>تا یوسف دل را نکنی از بن چاه</p></div>
<div class="m2"><p>یعقوب خرد ضریر باشد در راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که عزیز مصر باشی در جاه</p></div>
<div class="m2"><p>از عشق کمر ببند وز صدق کلاه</p></div></div>