---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>چون رفت رقیب عمر دریاب ای دل</p></div>
<div class="m2"><p>زین بیش مکن به لهو اشتاب ای دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دست برفت عمر دریاب ای دل</p></div>
<div class="m2"><p>ور مرده نئی درآی از خواب ای دل</p></div></div>