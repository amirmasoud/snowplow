---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>قیصر که زمین به پای حشمت فرسود</p></div>
<div class="m2"><p>قصرش به بلندی زفلک برتر بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کیخسرو که جاش داری بنگر</p></div>
<div class="m2"><p>کو قصر کجا قیصر گویی که نبود</p></div></div>