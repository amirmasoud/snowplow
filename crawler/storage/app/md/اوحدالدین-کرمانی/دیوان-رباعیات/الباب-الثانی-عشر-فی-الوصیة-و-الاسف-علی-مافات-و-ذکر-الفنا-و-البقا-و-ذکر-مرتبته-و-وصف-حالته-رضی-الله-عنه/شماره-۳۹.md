---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>اسباب فلک چو مهره بشمرد به خاک</p></div>
<div class="m2"><p>کس را ننواخت تاش نسپرد به خاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر شاخ که برگ او بلند است امروز</p></div>
<div class="m2"><p>از آب برآورد و فروبرد به خاک</p></div></div>