---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>گر می خواهی که روز و شب گردی شاه</p></div>
<div class="m2"><p>باید که زننگ خلق گردی آزاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زینها نشود هیچ خرابی آباد</p></div>
<div class="m2"><p>مشتی دونند که خاکشان بر سر باد</p></div></div>