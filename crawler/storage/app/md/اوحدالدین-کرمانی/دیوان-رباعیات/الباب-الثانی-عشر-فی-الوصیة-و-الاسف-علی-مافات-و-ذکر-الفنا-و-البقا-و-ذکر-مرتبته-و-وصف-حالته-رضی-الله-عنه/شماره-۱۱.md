---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>ماهی امید عمرم از شست برفت</p></div>
<div class="m2"><p>بی فایده عمرم چو شب مست برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری که ازو دمی جهانی ارزید</p></div>
<div class="m2"><p>افسوس که رایگانم از دست برفت</p></div></div>