---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>خواهی که رسی به کام برگیر دو گام</p></div>
<div class="m2"><p>یک گام زکونین و دگر گام زکام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر ره حق خواجه کم آید زغلام</p></div>
<div class="m2"><p>یک بندهٔ پخته به که صد خواجهٔ خام</p></div></div>