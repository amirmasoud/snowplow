---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>تا حد طلبی به وصل بی حد نرسی</p></div>
<div class="m2"><p>توحید نورزیده به «اوحد» نرسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاید که تمنّای رسیدن داری</p></div>
<div class="m2"><p>لکن به تمنّای مجرّد نرسی</p></div></div>