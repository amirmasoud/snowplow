---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>هر جا که سری است پر زسودای شماست</p></div>
<div class="m2"><p>هر جا که دلی است پر تمنای شماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنجا که جهان عالم آرای شماست</p></div>
<div class="m2"><p>اندر دل جان و جان و دل جای شماست</p></div></div>