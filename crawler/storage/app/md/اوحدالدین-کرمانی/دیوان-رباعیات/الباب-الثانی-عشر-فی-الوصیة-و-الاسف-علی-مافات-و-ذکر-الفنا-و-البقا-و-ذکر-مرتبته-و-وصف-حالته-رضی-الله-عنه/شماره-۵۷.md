---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>هر چند که عقل داری و دیده و هوش</p></div>
<div class="m2"><p>ایمن مشو و به دیگران پرده بپوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنجا که قضا بر تو کمین بگشاید</p></div>
<div class="m2"><p>نه دیده به کار آید آنجا و نه گوش</p></div></div>