---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>آدم که همی زد دم بی درمانی</p></div>
<div class="m2"><p>ترسم که تو آن دم بزنی درمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار که درماندهٔ هر در نشوی</p></div>
<div class="m2"><p>گر درمانی به که زره درمانی</p></div></div>