---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>آن شاه که او ملک تواند بخشید</p></div>
<div class="m2"><p>جز اشرف دین ملک که داند بخشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهی که به سهو می ببخشد شهری</p></div>
<div class="m2"><p>از بهر خدا دهی تواند بخشید</p></div></div>