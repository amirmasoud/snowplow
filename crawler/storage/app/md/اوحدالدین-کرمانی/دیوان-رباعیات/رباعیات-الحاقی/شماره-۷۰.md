---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>ای دل به در دوست تولّا می کن</p></div>
<div class="m2"><p>از دور به درگهش تمنّا می کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نومید مشو از در او باز نگرد</p></div>
<div class="m2"><p>در می زن و سر نیز تقاضا می کن</p></div></div>