---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>دانم که بتم چو لؤلؤی مکنون است</p></div>
<div class="m2"><p>رنگ دو رخش به رنگ آذرگون است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدّ و خد و خال و زلف و اندام و تنش</p></div>
<div class="m2"><p>سرو و گل و مشک و قیر و عاج و خون است</p></div></div>