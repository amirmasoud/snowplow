---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>هر چند که در خورد توام می دانی</p></div>
<div class="m2"><p>خون مژه پرورد توام می دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلسوختهٔ عشق توام می بینی</p></div>
<div class="m2"><p>ماتم زدهٔ درد توام می دانی</p></div></div>