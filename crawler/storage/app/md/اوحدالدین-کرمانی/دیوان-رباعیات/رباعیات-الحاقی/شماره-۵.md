---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>آن را که زبان و سینه یکتاست کجاست</p></div>
<div class="m2"><p>بر شرع وفا و سیرت راست کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن چشم که عیب دیگران بیند هست</p></div>
<div class="m2"><p>چشمی که به عیب خویش بیناست کجاست</p></div></div>