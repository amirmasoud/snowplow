---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>عشق آمد و گرد فتنه بر جانم بیخت</p></div>
<div class="m2"><p>دل خون شد و عقل رفت و صبرم بگریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاین واقعه هیچ دوست دستم نگرفت</p></div>
<div class="m2"><p>جز دیده که هر چه داشت در پایم ریخت</p></div></div>