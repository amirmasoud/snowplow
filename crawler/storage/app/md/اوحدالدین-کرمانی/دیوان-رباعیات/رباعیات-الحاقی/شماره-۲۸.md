---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>یار آمد و گفت خسته می دار دلت</p></div>
<div class="m2"><p>دایم به امید بسته می دار دلت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را به شکستگان نظرها باشد</p></div>
<div class="m2"><p>ما را خواهی شکسته می دار دلت</p></div></div>