---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>بر برگ گلت مورچه ره خواهد کرد</p></div>
<div class="m2"><p>بر لاله بنفشه تکیه گه خواهد کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آتش رخسار تو می دانی چیست</p></div>
<div class="m2"><p>دودی که هزار جان سیه خواهد کرد</p></div></div>