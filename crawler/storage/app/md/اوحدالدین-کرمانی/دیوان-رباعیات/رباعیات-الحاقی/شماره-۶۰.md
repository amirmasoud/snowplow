---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>از دوست به هر رهگذری می پرسم</p></div>
<div class="m2"><p>وز هر که بیابم خبری می پرسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دشمن بدسگال واقف نشود</p></div>
<div class="m2"><p>در دل وی و من از دگری می پرسم</p></div></div>