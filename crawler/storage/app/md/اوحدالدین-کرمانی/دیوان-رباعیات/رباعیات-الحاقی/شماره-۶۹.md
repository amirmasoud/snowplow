---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>سهل است مرا بر سر خنجر بودن</p></div>
<div class="m2"><p>در پای مراد خویش بی سر بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو آمده ای که ملحدی را بکشی</p></div>
<div class="m2"><p>غازی چو تویی رواست کافر بودن</p></div></div>