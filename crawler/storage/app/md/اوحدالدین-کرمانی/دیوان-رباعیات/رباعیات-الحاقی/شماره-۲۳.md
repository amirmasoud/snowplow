---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>در عالم دون دلِ کسی یافته نیست</p></div>
<div class="m2"><p>کاندر تف غم به سالها تافته نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی گویی سیه گلیم است فلان</p></div>
<div class="m2"><p>مسکین چه کند به دست خود بافته نیست</p></div></div>