---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>در عشق تو دل رفت و زجان می ترسم</p></div>
<div class="m2"><p>وز هجر و زمرگِ ناگهان می ترسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زار کُشی مرا نمی ترسم از آن</p></div>
<div class="m2"><p>بیزار زمن شوی از آن می ترسم</p></div></div>