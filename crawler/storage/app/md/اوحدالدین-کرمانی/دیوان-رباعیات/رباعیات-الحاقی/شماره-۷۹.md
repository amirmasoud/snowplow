---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>گر یک نفس از نیستی آگاه شوی</p></div>
<div class="m2"><p>بر هستی خود [به] نیستی شاه شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو حاضر غایبی از آن بی خبری</p></div>
<div class="m2"><p>گر غایب حاضر شوی آگاه شوی</p></div></div>