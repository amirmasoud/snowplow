---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>بگذار که تا زلف تو گیرم یک بار</p></div>
<div class="m2"><p>یا در کف پای تو بمالم رخسار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انگار که سنگ پایمال است رخم</p></div>
<div class="m2"><p>یا دست مرا شانهٔ چوبین پندار</p></div></div>