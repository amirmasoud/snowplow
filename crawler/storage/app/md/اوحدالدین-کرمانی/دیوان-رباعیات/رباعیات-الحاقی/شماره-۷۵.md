---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>با دل گفتم چو از مطر شاد نیی</p></div>
<div class="m2"><p>وز بند زمانه یک دم آزاد نیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تجربه های دهر استادانند</p></div>
<div class="m2"><p>شاگردی کن کنون که استاد نیی</p></div></div>