---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>من بندهٔ آنم که دلی برباید</p></div>
<div class="m2"><p>یا دل به کسی دهد که جان افزاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وآن کس که نه عاشق و نه معشوق کسی است</p></div>
<div class="m2"><p>در ملک خدا اگر نباشد شاید</p></div></div>