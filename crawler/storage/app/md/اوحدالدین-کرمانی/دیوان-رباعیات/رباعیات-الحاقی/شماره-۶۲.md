---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>من عشق تو را به صد ملامت بکشم</p></div>
<div class="m2"><p>گر آه کنم به جان غرامت بکشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عمر وفا کند جفاهای تو را</p></div>
<div class="m2"><p>آخر کم از آن که تا قیامت بکشم</p></div></div>