---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>مِهر تو چو مُهر از نگینم نرود</p></div>
<div class="m2"><p>سودای تو از دل حزینم نرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خود رفتم ولیک خونابهٔ چشم</p></div>
<div class="m2"><p>تا دامن عمر زآستینم نرود</p></div></div>