---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>مجروحان را دوا و مرهم تو دهی</p></div>
<div class="m2"><p>محرومان را ملک مسلّم تو دهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تو کششی هست یقین می دانم</p></div>
<div class="m2"><p>تقصیر زکوشش است آن هم تو دهی</p></div></div>