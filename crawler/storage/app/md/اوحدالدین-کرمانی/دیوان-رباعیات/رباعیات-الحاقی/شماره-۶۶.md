---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>نه ما به سر رشته شدن بتوانیم</p></div>
<div class="m2"><p>نه رشته به دیگری سپردن دانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر یک به بهانه ای فرو می مانیم</p></div>
<div class="m2"><p>قصه چه کنم که جمله سرگردانیم</p></div></div>