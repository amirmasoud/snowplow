---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>گر بنوازی بندهٔ مقبول توَم</p></div>
<div class="m2"><p>گر ننوازی چاکر معزول توَم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با ردّ و قبول تو مرا کاری نیست</p></div>
<div class="m2"><p>زیرا که به هر دو کار مشغول توَم</p></div></div>