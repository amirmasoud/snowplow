---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ای آمدهٔ به وعده باز آمده راست</p></div>
<div class="m2"><p>بر دیده نشین که جات بر دیدهٔ ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکر تو به سالها کجا دانم گفت</p></div>
<div class="m2"><p>عذر تو به عمرها کجا دانم خواست</p></div></div>