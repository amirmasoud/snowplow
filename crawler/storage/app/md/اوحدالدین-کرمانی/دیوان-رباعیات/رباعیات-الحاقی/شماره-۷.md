---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>ذاتم ز ورای حرف و بیرون زحد است</p></div>
<div class="m2"><p>وز چشمهٔ لطف آب حیاتم مدد است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علّت زاحد به اوحد آمد حرفی</p></div>
<div class="m2"><p>علت بگذار کاینک اوحد احد است</p></div></div>