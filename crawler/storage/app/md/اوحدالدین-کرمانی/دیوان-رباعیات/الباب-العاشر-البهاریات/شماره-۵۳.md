---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>نی بر سر آب و جویها می روید</p></div>
<div class="m2"><p>واندر طلب عشق خدا می پوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی زن چو زعشق یک دم او را بدمد</p></div>
<div class="m2"><p>بنگر که چگونه سرّها می گوید</p></div></div>