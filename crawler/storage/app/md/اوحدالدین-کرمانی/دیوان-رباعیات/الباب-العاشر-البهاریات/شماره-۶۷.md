---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>از جهل بود زیره به کرمان بردن</p></div>
<div class="m2"><p>یا قطره به نزد آب عمّان بردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لکن چو مروّت است فرمود خرد</p></div>
<div class="m2"><p>پای ملخی نزد سلیمان بردن</p></div></div>