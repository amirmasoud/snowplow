---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>خواهی که بری تو گوی میدان سماع</p></div>
<div class="m2"><p>بی حال مزن دست به چوگان سماع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا از عُجبت تو برنخیزی به خدا</p></div>
<div class="m2"><p>هرگز نرسد برات فرمان سماع</p></div></div>