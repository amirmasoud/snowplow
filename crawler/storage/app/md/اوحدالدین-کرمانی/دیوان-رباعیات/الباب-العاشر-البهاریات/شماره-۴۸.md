---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>بشنو که نی اسرار نهان می گوید</p></div>
<div class="m2"><p>سوزی که بود درون جان می گوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد جمله دهان و راز دل می گوید</p></div>
<div class="m2"><p>از نی بشنو که بی زبان می گوید</p></div></div>