---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>امشب زطرب هیچ اثر پیدا نیست</p></div>
<div class="m2"><p>ور هست مگر در دگری در ما نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حظّی زسماع امشب آن ما نیست</p></div>
<div class="m2"><p>کان مونس روزگار ما اینجا نیست</p></div></div>