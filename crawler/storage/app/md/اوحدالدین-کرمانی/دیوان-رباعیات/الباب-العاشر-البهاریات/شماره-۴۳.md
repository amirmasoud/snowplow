---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>ای دل به طبیعت نفسی یکتا شو</p></div>
<div class="m2"><p>وانگه به نظاره ای تو بر بالا شو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوهرطلبی خوش است چون پروانه</p></div>
<div class="m2"><p>رقصی کن و بر آتش وحدت لاشو</p></div></div>