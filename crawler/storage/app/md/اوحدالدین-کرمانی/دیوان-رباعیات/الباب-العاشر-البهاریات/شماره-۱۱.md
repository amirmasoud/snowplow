---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>رو باده و بنگ را بکن در باقی</p></div>
<div class="m2"><p>تا چند ازین دو سفرهٔ زراقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستی خواهی گردِ درِ معنی گرد</p></div>
<div class="m2"><p>تا مست شوی و هم بمانی باقی</p></div></div>