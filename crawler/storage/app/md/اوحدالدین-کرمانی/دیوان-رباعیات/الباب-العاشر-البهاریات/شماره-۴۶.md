---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>بی روی تو دل کیست چه کار آید ازو</p></div>
<div class="m2"><p>جز ناله که هر دمی هزار آید ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می گرید تا خاک شود وز گل او</p></div>
<div class="m2"><p>نی روید و ناله های زار آید ازو</p></div></div>