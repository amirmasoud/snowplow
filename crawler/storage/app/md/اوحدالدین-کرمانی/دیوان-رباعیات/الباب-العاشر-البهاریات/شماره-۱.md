---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>هنگام بهار آمد و من بی رخ یار</p></div>
<div class="m2"><p>رخساره به خون دیدگان کرده نگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون چشم و رخ و لبش به پیشم نبود</p></div>
<div class="m2"><p>مَه نرگس و مه لاله و مه گل مه بهار</p></div></div>