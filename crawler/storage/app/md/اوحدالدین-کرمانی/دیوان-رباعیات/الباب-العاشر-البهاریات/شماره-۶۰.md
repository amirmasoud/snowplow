---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>ای شمع هوای دلفروزی داری</p></div>
<div class="m2"><p>شب زنده هم از برای روزی داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا صبح از آرزوی شیرین لب او</p></div>
<div class="m2"><p>از گریه میاسای که سوزی داری</p></div></div>