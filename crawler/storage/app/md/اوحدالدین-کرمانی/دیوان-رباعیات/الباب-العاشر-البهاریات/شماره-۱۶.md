---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>این خوش پسران خوی پلنگ آوردند</p></div>
<div class="m2"><p>روی چو مه و دل چو سنگ آوردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بیم پدر باده نمی یارند خورد</p></div>
<div class="m2"><p>ناچار همه روی به بنگ آوردند</p></div></div>