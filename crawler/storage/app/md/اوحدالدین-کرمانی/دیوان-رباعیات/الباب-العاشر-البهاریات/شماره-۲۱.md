---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>در عشق زدیده اشک باید سفتن</p></div>
<div class="m2"><p>دل را زغبار نفس باید رُفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در رقص به قوّال کسی گوید بیت</p></div>
<div class="m2"><p>کاو معنی حرف بیت داند گفتن</p></div></div>