---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>شمعی که مبارز است و تمکین دارد</p></div>
<div class="m2"><p>بر پشت لگن زچابکی زین دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که چرا زرد رخی؟ گفت مرا</p></div>
<div class="m2"><p>فرهاد دلم فراق شیرین دارد</p></div></div>