---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>عقّال به جز پیروی دل نکنند</p></div>
<div class="m2"><p>در عشق به کوی طبع منزل نکنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنها که سماع را حقیقت دانند</p></div>
<div class="m2"><p>عیش خوش را به هزل باطل نکنند</p></div></div>