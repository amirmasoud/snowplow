---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>نظمی که به راستی چو وحی اش دانند</p></div>
<div class="m2"><p>نتوان کردن به شعر او را مانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرق است میان آنک از خود گویی</p></div>
<div class="m2"><p>یا آنک زدیوان کسانش خوانند</p></div></div>