---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>از کار برفته چونک با کار شوی</p></div>
<div class="m2"><p>از هر چه تو کرده ای تو بیدار شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز تو خفته ای از آنی فارغ</p></div>
<div class="m2"><p>فردات کند غصّه که بیدار شوی</p></div></div>