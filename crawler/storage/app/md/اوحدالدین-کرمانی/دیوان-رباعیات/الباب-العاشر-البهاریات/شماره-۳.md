---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>شد زنده زمین مرده از لطف بهار</p></div>
<div class="m2"><p>وقت طرب است ساقیا باده بیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فصل گل و وصل یار و عاشق هشیار</p></div>
<div class="m2"><p>حیفی باشد عظیم یا رب زنهار</p></div></div>