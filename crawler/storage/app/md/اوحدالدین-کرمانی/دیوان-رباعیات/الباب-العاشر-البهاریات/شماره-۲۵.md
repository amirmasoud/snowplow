---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>جانا چو نئی نیک، بدآموز مباش</p></div>
<div class="m2"><p>هر لحظه جگر خواره و دلسوز مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون هست حضور شاهد و شمع و سماع</p></div>
<div class="m2"><p>گو که امشب ما را به جهان روز مباش</p></div></div>