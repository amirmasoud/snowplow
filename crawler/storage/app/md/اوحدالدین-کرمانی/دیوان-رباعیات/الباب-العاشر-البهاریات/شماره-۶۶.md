---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>جا[ئ]ت سلیمان یوم العرض قبّرةٌ</p></div>
<div class="m2"><p>اتت برجل جراد کان فی فیها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترنّمت بلطیف القول اذ نطقت</p></div>
<div class="m2"><p>انّ الهدایا علی مقدار مُهدیها</p></div></div>