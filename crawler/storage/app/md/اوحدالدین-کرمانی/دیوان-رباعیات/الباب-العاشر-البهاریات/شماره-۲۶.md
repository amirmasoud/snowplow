---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>این سکّهٔ زربین که به پول افتاده است</p></div>
<div class="m2"><p>اوصاف ملک بین که به غول افتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افشاندن هر دو دست مردان ز دو کون</p></div>
<div class="m2"><p>امروز نگر که در کچول افتاده است</p></div></div>