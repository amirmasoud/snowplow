---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>در هستی اگر به عمر نوحی برسی</p></div>
<div class="m2"><p>در هر نفسی زو به فتوحی برسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری باید که شب به روز آری تو</p></div>
<div class="m2"><p>باشد که تو صبحی به صبوحی برسی</p></div></div>