---
title: >-
    شمارهٔ  ۶۰
---
# شمارهٔ  ۶۰

<div class="b" id="bn1"><div class="m1"><p>آنها که محققان این درگاهند</p></div>
<div class="m2"><p>اهل دل خاص خاص شاهنشاهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باقی همه هرچ هست خرج راهند</p></div>
<div class="m2"><p>نزد دل اهل دل چو برگ کاهند</p></div></div>