---
title: >-
    شمارهٔ  ۲۵۲
---
# شمارهٔ  ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>آن یار که در سینه جنون دارم ازو</p></div>
<div class="m2"><p>در هر مژه صد قطرهٔ خون دارم ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنجی و دمی و محرمی می طلبم</p></div>
<div class="m2"><p>تا شرح دهم که حال چون دارم ازو</p></div></div>