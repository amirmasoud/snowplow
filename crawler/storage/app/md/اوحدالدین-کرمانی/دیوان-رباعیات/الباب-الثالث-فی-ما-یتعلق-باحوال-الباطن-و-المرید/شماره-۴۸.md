---
title: >-
    شمارهٔ  ۴۸
---
# شمارهٔ  ۴۸

<div class="b" id="bn1"><div class="m1"><p>آنها که سرانند به سرگردانند</p></div>
<div class="m2"><p>با سر زسری خویش سرگردانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سررشتهٔ مقصود به دست کس نیست</p></div>
<div class="m2"><p>در پای مراد جمله سرگردانند</p></div></div>