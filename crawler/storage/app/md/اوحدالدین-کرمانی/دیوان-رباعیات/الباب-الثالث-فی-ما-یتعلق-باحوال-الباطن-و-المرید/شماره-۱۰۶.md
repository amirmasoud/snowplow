---
title: >-
    شمارهٔ  ۱۰۶
---
# شمارهٔ  ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>با دل گفتم نئی زمردان غمش</p></div>
<div class="m2"><p>بیهوده متاز گردِ میدان غمش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل گفت به من که رو سر انداز و مپرس</p></div>
<div class="m2"><p>مانندهٔ گوی پیش چوگان غمش</p></div></div>