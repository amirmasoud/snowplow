---
title: >-
    شمارهٔ  ۱۴۱
---
# شمارهٔ  ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>ایزد زقناعت چو مرا گنجی داد</p></div>
<div class="m2"><p>از میر و وزیر جمله گشتم آزاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلق من و حُلّه های زربفت ملوک</p></div>
<div class="m2"><p>کفش من و تاج سر کسری و قباد</p></div></div>