---
title: >-
    شمارهٔ  ۱۵۱
---
# شمارهٔ  ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>در کوی قناعت ار سپنجی داری</p></div>
<div class="m2"><p>در هر قدم آراسته گنجی داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز هر چه نه بر مراد تو خواهد بود</p></div>
<div class="m2"><p>رنجیده شوی دراز رنجی داری</p></div></div>