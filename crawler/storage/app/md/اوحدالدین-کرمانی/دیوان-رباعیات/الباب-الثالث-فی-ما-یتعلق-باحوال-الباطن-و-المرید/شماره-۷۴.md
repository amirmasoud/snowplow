---
title: >-
    شمارهٔ  ۷۴
---
# شمارهٔ  ۷۴

<div class="b" id="bn1"><div class="m1"><p>چون بر سر و پای من نگه کرد او دوش</p></div>
<div class="m2"><p>هم از سر پای گفت ای زرق فروش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی سر پایداریم در غم هست</p></div>
<div class="m2"><p>گر بر سر پایداریی پس مخروش</p></div></div>