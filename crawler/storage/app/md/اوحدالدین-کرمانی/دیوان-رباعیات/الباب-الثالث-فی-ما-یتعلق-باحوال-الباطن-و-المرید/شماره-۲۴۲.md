---
title: >-
    شمارهٔ  ۲۴۲
---
# شمارهٔ  ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>زنهار دلا بکوش اگر باخبری</p></div>
<div class="m2"><p>کز دست تکلّف تو مگر جان بُبَری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مادام که در بند تکلّف باشی</p></div>
<div class="m2"><p>از عمر خود و عیش جهان بی خبری</p></div></div>