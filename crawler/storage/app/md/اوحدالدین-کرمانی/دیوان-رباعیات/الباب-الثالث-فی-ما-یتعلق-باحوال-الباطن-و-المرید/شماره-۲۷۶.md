---
title: >-
    شمارهٔ  ۲۷۶
---
# شمارهٔ  ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>ای دل زشراب جهل مستی تاکی</p></div>
<div class="m2"><p>وای مست شونده لاف هستی تاکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وای غرقهٔ بحر غفلت ار ابر نئی</p></div>
<div class="m2"><p>تر دامنی و هواپرستی تاکی</p></div></div>