---
title: >-
    شمارهٔ  ۸۸
---
# شمارهٔ  ۸۸

<div class="b" id="bn1"><div class="m1"><p>روزی زقضای آسمانی ای دل</p></div>
<div class="m2"><p>باشد که نکو شود چه دانی ای دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا در غم رنج بی کرانی ای دل</p></div>
<div class="m2"><p>خوش باش که آن چنان نمانی ای دل</p></div></div>