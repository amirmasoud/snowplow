---
title: >-
    شمارهٔ  ۲۸۶
---
# شمارهٔ  ۲۸۶

<div class="b" id="bn1"><div class="m1"><p>با آنچ گسستنی است در پیوستی</p></div>
<div class="m2"><p>وآنجا که گذشتنی است خوش بنشستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز گل است و خار از پس بینی</p></div>
<div class="m2"><p>فردات کند خمار کامشب مستی</p></div></div>