---
title: >-
    شمارهٔ  ۲۸۰
---
# شمارهٔ  ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>گر عمر بود تو را فزون از پانصد</p></div>
<div class="m2"><p>افسانه شوی عاقبت از روی خرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باری چو فسانه می شوی ای بخرد</p></div>
<div class="m2"><p>افسانهٔ نیک شو نه افسانهٔ بد</p></div></div>