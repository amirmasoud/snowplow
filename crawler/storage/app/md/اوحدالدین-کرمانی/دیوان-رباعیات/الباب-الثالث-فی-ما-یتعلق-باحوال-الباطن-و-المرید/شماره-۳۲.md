---
title: >-
    شمارهٔ  ۳۲
---
# شمارهٔ  ۳۲

<div class="b" id="bn1"><div class="m1"><p>چون بی خبران مگرد هر دم به دری</p></div>
<div class="m2"><p>پادار و زسر مرو به هر درد سری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تلخی و خوشی جمله عالم خوابی است</p></div>
<div class="m2"><p>بیدار شوی از تو نماند اثری</p></div></div>