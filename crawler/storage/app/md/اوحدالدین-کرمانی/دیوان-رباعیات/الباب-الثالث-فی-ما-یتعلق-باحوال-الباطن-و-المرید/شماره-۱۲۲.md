---
title: >-
    شمارهٔ  ۱۲۲
---
# شمارهٔ  ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>اندر طلبت گرچه به سر می پویم</p></div>
<div class="m2"><p>رخساره به خوناب جگر می شویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون در نگرم تو با منی من غلطم</p></div>
<div class="m2"><p>سررشتهٔ خود جای دگر می جویم</p></div></div>