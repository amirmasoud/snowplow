---
title: >-
    شمارهٔ  ۱۰۰
---
# شمارهٔ  ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>از دیده چه گویم که ازو دارم غم</p></div>
<div class="m2"><p>وز دل چه خبر دهم که بودش همدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>القصّه دل و دیده فتادند به هم</p></div>
<div class="m2"><p>تا درد مرا هیچ نباشد مرهم</p></div></div>