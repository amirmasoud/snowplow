---
title: >-
    شمارهٔ  ۷۶
---
# شمارهٔ  ۷۶

<div class="b" id="bn1"><div class="m1"><p>دم درکش و در خویش سیاحی می کن</p></div>
<div class="m2"><p>در عالم ذات خود ملاحی می کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خود به خودیّ خویش حاصل کردی</p></div>
<div class="m2"><p>با خود بنشین و پادشاهی می کن</p></div></div>