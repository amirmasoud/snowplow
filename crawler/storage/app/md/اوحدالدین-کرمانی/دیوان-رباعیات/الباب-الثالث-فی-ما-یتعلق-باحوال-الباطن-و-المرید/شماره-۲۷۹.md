---
title: >-
    شمارهٔ  ۲۷۹
---
# شمارهٔ  ۲۷۹

<div class="b" id="bn1"><div class="m1"><p>از سود و زیان خود به در نتوان بود</p></div>
<div class="m2"><p>بر مایهٔ کس زیر و زبر نتوان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بد بودن و حال خویش نیکو دیدن</p></div>
<div class="m2"><p>آن بد باشد کز آن بتر نتوان بود</p></div></div>