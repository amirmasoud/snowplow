---
title: >-
    شمارهٔ  ۶۱
---
# شمارهٔ  ۶۱

<div class="b" id="bn1"><div class="m1"><p>آنجا که نه کون و نه مکان در گنجد</p></div>
<div class="m2"><p>کی زحمت عقل و دل جان در گنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آنجا که زاسرار خدا گوید راز</p></div>
<div class="m2"><p>نه حرف و نه کام و نه دهان در گنجد</p></div></div>