---
title: >-
    شمارهٔ  ۲۷۸
---
# شمارهٔ  ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>ای دل تو بدین حال چرایی خشنود</p></div>
<div class="m2"><p>کز عمر گذشته هیچ سود تو نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود را دریاب اگر نه چون مایه برفت</p></div>
<div class="m2"><p>بسیار بگویی تو که افسوس چه سود</p></div></div>