---
title: >-
    شمارهٔ  ۵
---
# شمارهٔ  ۵

<div class="b" id="bn1"><div class="m1"><p>اصحاب طلب چون به صفایی برسند</p></div>
<div class="m2"><p>خواهند کز آنجا به رضایی برسند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست از سر پای وامگیرند از جهد</p></div>
<div class="m2"><p>یا سر بنهند یا به جایی برسند</p></div></div>