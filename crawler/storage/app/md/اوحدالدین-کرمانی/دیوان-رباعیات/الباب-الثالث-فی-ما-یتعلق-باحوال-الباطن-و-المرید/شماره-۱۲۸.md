---
title: >-
    شمارهٔ  ۱۲۸
---
# شمارهٔ  ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>ای تن خواهی که احتشامت باشد</p></div>
<div class="m2"><p>حکمی و تصرّفی تمامت باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنبیل طلب بر سر جان و دل نه</p></div>
<div class="m2"><p>دریوزه کن از درش که گامت باشد</p></div></div>