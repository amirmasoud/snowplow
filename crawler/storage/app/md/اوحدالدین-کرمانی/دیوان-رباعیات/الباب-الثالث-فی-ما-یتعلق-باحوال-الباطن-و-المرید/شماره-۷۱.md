---
title: >-
    شمارهٔ  ۷۱
---
# شمارهٔ  ۷۱

<div class="b" id="bn1"><div class="m1"><p>جان از قبل زبان به بیم خطر است</p></div>
<div class="m2"><p>کم گفتن مرد هم به جای سپر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانا که سخن نگوید آن از هنر است</p></div>
<div class="m2"><p>گر گوید بد و گر نگوید گهر است</p></div></div>