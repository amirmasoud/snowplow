---
title: >-
    شمارهٔ  ۲۱۴
---
# شمارهٔ  ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>آنها که به تقلید عبارات کنند</p></div>
<div class="m2"><p>واندر طلب خدا ریاضات کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دل نکنی پاک ز دنیا و هوا</p></div>
<div class="m2"><p>در راه به یک پیاده شان مات کنند</p></div></div>