---
title: >-
    شمارهٔ  ۳۴
---
# شمارهٔ  ۳۴

<div class="b" id="bn1"><div class="m1"><p>عیشی که مهیّاست رها نتوان کرد</p></div>
<div class="m2"><p>سر در سر یار بی وفا نتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری که تو راهست غنیمت می دان</p></div>
<div class="m2"><p>کان را چو نمازها قضا نتوان کرد</p></div></div>