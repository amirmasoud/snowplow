---
title: >-
    شمارهٔ  ۲۷۵
---
# شمارهٔ  ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>گر زانک تو صاحبدل و صاحبنظری</p></div>
<div class="m2"><p>باید که به گل به چشم عبرت نگری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیف است عظیم شاهدی چون گل را</p></div>
<div class="m2"><p>در زیر لگد سپرده از بی خبری</p></div></div>