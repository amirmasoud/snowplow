---
title: >-
    شمارهٔ  ۲۴۱
---
# شمارهٔ  ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>در دست تکلّف چو اسیری ای دل</p></div>
<div class="m2"><p>بر طبع خودت نیست امیری ای دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهدی بکن از سر تکلّف برخیز</p></div>
<div class="m2"><p>در پای تکلف به چه میری ای دل</p></div></div>