---
title: >-
    شمارهٔ  ۱۶۹
---
# شمارهٔ  ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>شاهان جهان چاکر درویشانند</p></div>
<div class="m2"><p>عالم همه خاک در درویشانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معصومانی که ساکنان قدسند</p></div>
<div class="m2"><p>با این همه اجرا خور درویشانند</p></div></div>