---
title: >-
    شمارهٔ  ۱۶۴
---
# شمارهٔ  ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>گفتی عالم به پای درویشان است</p></div>
<div class="m2"><p>عالم همه از برای درویشان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار مخوان تو هر گدا را درویش</p></div>
<div class="m2"><p>سلطان جهان گدای درویشان است</p></div></div>