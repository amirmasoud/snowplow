---
title: >-
    شمارهٔ  ۲۱۸
---
# شمارهٔ  ۲۱۸

<div class="b" id="bn1"><div class="m1"><p>هرگه که ببینی دو سه سرگردان را</p></div>
<div class="m2"><p>عیب ره مردان نتوان کرد آن را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تقلیدِ دو سه مقلّد بی معنی</p></div>
<div class="m2"><p>بدنام کند راه جوامردان را</p></div></div>