---
title: >-
    شمارهٔ  ۱۰۸
---
# شمارهٔ  ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>چندانک دلم جان کند اندر سر و کار</p></div>
<div class="m2"><p>هرگز نشود به وصل کس برخوردار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوهر دارد دل من وزآن خوانند</p></div>
<div class="m2"><p>عشّاق جهان دل مرا جوهر دار</p></div></div>