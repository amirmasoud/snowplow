---
title: >-
    شمارهٔ  ۲۳۸
---
# شمارهٔ  ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>هر شیخ که او علم ندارد در تن</p></div>
<div class="m2"><p>او نتواند مرید را پروردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این شیخی را علم و عمل می باید</p></div>
<div class="m2"><p>بی علم چه لایق است شیخی کردن</p></div></div>