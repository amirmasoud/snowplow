---
title: >-
    شمارهٔ  ۱۳
---
# شمارهٔ  ۱۳

<div class="b" id="bn1"><div class="m1"><p>این راه به شش پنج نشاید رفتن</p></div>
<div class="m2"><p>با راحت و بی رنج نشاید رفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورت در باز تا به معنی برسی</p></div>
<div class="m2"><p>آسان به سر گنج نشاید رفتن</p></div></div>