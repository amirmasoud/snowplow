---
title: >-
    شمارهٔ  ۲۷
---
# شمارهٔ  ۲۷

<div class="b" id="bn1"><div class="m1"><p>جز حق تو بکن جمله فراموش امشب</p></div>
<div class="m2"><p>وز جام وصال باده می نوش امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بوک به وصل جاودانی برسی</p></div>
<div class="m2"><p>منشین و به جان و دل همی کوش امشب</p></div></div>