---
title: >-
    شمارهٔ  ۱۱۰
---
# شمارهٔ  ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>آنجا که صفای دل بود دایهٔ عیش</p></div>
<div class="m2"><p>برسود بود مدام سرمایهٔ عیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افسوس که کار خلق جایی نرسید</p></div>
<div class="m2"><p>کز مایهٔ غم شوند همسایهٔ عیش</p></div></div>