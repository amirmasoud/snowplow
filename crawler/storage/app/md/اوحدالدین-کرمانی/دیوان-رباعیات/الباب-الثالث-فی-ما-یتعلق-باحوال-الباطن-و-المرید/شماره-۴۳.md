---
title: >-
    شمارهٔ  ۴۳
---
# شمارهٔ  ۴۳

<div class="b" id="bn1"><div class="m1"><p>دردا که درون صفه ما را ره نیست</p></div>
<div class="m2"><p>و افسوس که سوی وصل ره کوته نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بس که گذشت صبحها از من و تو</p></div>
<div class="m2"><p>وز سرّ صبوح این دل ما آگه نیست</p></div></div>