---
title: >-
    شمارهٔ  ۱
---
# شمارهٔ  ۱

<div class="b" id="bn1"><div class="m1"><p>با همنفسان دلا دمت همدم نیست</p></div>
<div class="m2"><p>در راه حقیقت قدمت محکم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موی از سر بوالفضول کم کردی تو</p></div>
<div class="m2"><p>لیکن سر مویی زفضولی کم نیست</p></div></div>