---
title: >-
    شمارهٔ  ۱۴۲
---
# شمارهٔ  ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>عالی نسبا چرا بننشینی پست</p></div>
<div class="m2"><p>وز ملک جهان پاک نیفشانی دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بود تو با بود قناعت پیوست</p></div>
<div class="m2"><p>خواهی همه نیست گیر و خواهی همه هست</p></div></div>