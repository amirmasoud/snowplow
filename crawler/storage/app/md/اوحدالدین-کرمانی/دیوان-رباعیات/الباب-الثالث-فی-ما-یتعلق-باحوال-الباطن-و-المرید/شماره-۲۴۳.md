---
title: >-
    شمارهٔ  ۲۴۳
---
# شمارهٔ  ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>امروز درین زمانه بی یاری به</p></div>
<div class="m2"><p>در خلق وفا نماند تنهایی به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون رونق علم نیست جهل اولی تر</p></div>
<div class="m2"><p>چون قیمت عقل نیست سَودایی به</p></div></div>