---
title: >-
    شمارهٔ  ۱۹۰
---
# شمارهٔ  ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>خود دمدمه ای است آدمی از دم او</p></div>
<div class="m2"><p>عالم همه سایه است از عالم او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاج سر کیقباد و جمشید ارزد</p></div>
<div class="m2"><p>خاک قدم سوختگانِ غم او</p></div></div>