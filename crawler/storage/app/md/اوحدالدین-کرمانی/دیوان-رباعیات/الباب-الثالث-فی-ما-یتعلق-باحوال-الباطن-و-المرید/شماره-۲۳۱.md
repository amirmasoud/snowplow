---
title: >-
    شمارهٔ  ۲۳۱
---
# شمارهٔ  ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>خود را چو دمی ز دهر خرّم یابی</p></div>
<div class="m2"><p>از عُمر نصیب خویش آن دم یابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار که ضایع نکنی آن دم را</p></div>
<div class="m2"><p>باشد که چنان دمی دگر کم یابی</p></div></div>