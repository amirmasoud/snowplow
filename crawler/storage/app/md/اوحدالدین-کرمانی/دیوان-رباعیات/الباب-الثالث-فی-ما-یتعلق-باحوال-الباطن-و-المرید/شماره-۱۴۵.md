---
title: >-
    شمارهٔ  ۱۴۵
---
# شمارهٔ  ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>در کوی قناعت ارچه دیر آمده ایم</p></div>
<div class="m2"><p>بر نیستی خویش دلیر آمده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ناخوش و گر خوش است این باقی عمر</p></div>
<div class="m2"><p>باری به سر آمدی که سیر آمده ایم</p></div></div>