---
title: >-
    شمارهٔ  ۱۴۰
---
# شمارهٔ  ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>مسپار به عشوهٔ جهان خویشتنت</p></div>
<div class="m2"><p>مگذار که گردد دو یکی پیرهنت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دشوار مکن جمع که باشد روزی</p></div>
<div class="m2"><p>بسیار سخنها رود اندر کفنت</p></div></div>