---
title: >-
    شمارهٔ  ۱۳۵
---
# شمارهٔ  ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>حاشا که من از خاک درت برخیزم</p></div>
<div class="m2"><p>وز لعل لب چون شکرت برخیزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آرزوی زلف خم اندر خم تو</p></div>
<div class="m2"><p>چون موی شدم کی زسرت برخیزم</p></div></div>