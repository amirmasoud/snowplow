---
title: >-
    شمارهٔ  ۲۵۴
---
# شمارهٔ  ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>جز بادهٔ نیستی دلا نوش مکن</p></div>
<div class="m2"><p>جز سلسلهٔ نیاز در گوش مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی که به همّت ز فلک برگذری</p></div>
<div class="m2"><p>بیچارگی خویش فراموش مکن</p></div></div>