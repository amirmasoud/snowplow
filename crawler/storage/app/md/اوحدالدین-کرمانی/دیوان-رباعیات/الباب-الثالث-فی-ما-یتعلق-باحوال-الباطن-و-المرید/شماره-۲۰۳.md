---
title: >-
    شمارهٔ  ۲۰۳
---
# شمارهٔ  ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>افکند بتی به بت پرستی ما را</p></div>
<div class="m2"><p>او راست خبر که نیست هستی ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان می که شب وصال با هم خوردیم</p></div>
<div class="m2"><p>تا روز قیامت است مستی ما را</p></div></div>