---
title: >-
    شمارهٔ  ۱۷۷
---
# شمارهٔ  ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>درویش به غم همیشه خرّم باشد</p></div>
<div class="m2"><p>اندر ره فقر زخم مرهم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر درویشی تو بابلا خوش می باش</p></div>
<div class="m2"><p>کس جای حدیث عافیت کم باشد</p></div></div>