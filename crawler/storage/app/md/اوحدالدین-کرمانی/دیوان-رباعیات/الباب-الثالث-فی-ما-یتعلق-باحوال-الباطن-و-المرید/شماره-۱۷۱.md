---
title: >-
    شمارهٔ  ۱۷۱
---
# شمارهٔ  ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>بادی که زکوی فقر گرد انگیزد</p></div>
<div class="m2"><p>بر آتش کبر آب تواضع ریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حقّا که هزار تاج کسری ارزد</p></div>
<div class="m2"><p>گردی که زپای این گدایان خیزد</p></div></div>