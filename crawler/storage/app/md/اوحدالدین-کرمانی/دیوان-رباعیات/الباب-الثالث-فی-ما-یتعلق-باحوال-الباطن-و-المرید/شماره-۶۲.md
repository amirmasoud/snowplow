---
title: >-
    شمارهٔ  ۶۲
---
# شمارهٔ  ۶۲

<div class="b" id="bn1"><div class="m1"><p>این ره به قدم چون بر وی برخیزد</p></div>
<div class="m2"><p>بند منی از راهِ توی برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکبار مرا ز زحمت من برهان</p></div>
<div class="m2"><p>تا من تو شوم تو من دوی برخیزد</p></div></div>