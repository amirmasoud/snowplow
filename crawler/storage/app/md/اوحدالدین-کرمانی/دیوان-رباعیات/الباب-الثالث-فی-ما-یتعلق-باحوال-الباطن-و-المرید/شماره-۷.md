---
title: >-
    شمارهٔ  ۷
---
# شمارهٔ  ۷

<div class="b" id="bn1"><div class="m1"><p>چون از سر جد پای نهادی در کار</p></div>
<div class="m2"><p>سررشتهٔ خود به دست عشقش بسپار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کوش به قدر جهد و دل خوش می دار</p></div>
<div class="m2"><p>کاو چارهٔ کار تو بسازد ناچار</p></div></div>