---
title: >-
    شمارهٔ  ۹۷
---
# شمارهٔ  ۹۷

<div class="b" id="bn1"><div class="m1"><p>با دل گفتم مشکلت آسان نشود</p></div>
<div class="m2"><p>با یار سر تو هرگز آسان نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باری سر خویش گیر ازو دست بدار</p></div>
<div class="m2"><p>دل گفت همه شود ولی آن نشود</p></div></div>