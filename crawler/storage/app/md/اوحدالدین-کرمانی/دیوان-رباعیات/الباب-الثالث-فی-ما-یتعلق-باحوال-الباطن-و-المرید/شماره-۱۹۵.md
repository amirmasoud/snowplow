---
title: >-
    شمارهٔ  ۱۹۵
---
# شمارهٔ  ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>درویش ز درویشی از آن می نرهد</p></div>
<div class="m2"><p>کاین دهر درم به جای خود می ننهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن را که دل است هیچ درویشی نیست</p></div>
<div class="m2"><p>و آن را که به دست است دلش می ندهد</p></div></div>