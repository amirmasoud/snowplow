---
title: >-
    شمارهٔ  ۱۶۵
---
# شمارهٔ  ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>سرمایهٔ ملک جاودان درویشی است</p></div>
<div class="m2"><p>درویش تن است و جان آن درویشی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افلاس و گدایی نبود درویشی</p></div>
<div class="m2"><p>پرداختن دل زجهان درویشی است</p></div></div>