---
title: >-
    شمارهٔ  ۲۲۹
---
# شمارهٔ  ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>تخمی دو سه بی وقت بپاشیم مگر</p></div>
<div class="m2"><p>حالی به دروغ بر تراشیم مگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری به هوس با دگران ما کردیم</p></div>
<div class="m2"><p>روزی دو سه نیز با تو باشیم مگر</p></div></div>