---
title: >-
    شمارهٔ  ۸۱
---
# شمارهٔ  ۸۱

<div class="b" id="bn1"><div class="m1"><p>هرگه که غمی ملازم دل شودت</p></div>
<div class="m2"><p>یا تنگ دلی تمام مایل شودت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حال کسی دگر نباید پرسید</p></div>
<div class="m2"><p>تا خوش دلی تمام حاصل شودت</p></div></div>