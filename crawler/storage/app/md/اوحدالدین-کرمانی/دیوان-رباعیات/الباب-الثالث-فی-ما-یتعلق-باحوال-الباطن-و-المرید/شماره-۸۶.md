---
title: >-
    شمارهٔ  ۸۶
---
# شمارهٔ  ۸۶

<div class="b" id="bn1"><div class="m1"><p>قفلی زده ام زمهر تو بر سر دل</p></div>
<div class="m2"><p>تا مهر دگر کسی نگنجد در دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این طرفه تر است کار ماند مشکل</p></div>
<div class="m2"><p>نه دل سرما دارد و نه ما سر دل</p></div></div>