---
title: >-
    شمارهٔ  ۱۹۴
---
# شمارهٔ  ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>با فقر نشین اگر تو همدم خواهی</p></div>
<div class="m2"><p>فقر است اگر ملک مُسلَّم خواهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک کف پای این گدایان را خواه</p></div>
<div class="m2"><p>گر افسر سروران عالم خواهی</p></div></div>