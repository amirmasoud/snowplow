---
title: >-
    شمارهٔ  ۲۵۱
---
# شمارهٔ  ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>ای دل پس زنجیر چو دیوانه نشین</p></div>
<div class="m2"><p>بر دامن درد خویش مردانه نشین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآمد شد بیهوده تو خود را پی کن</p></div>
<div class="m2"><p>معشوقه چو خانگی است در خانه نشین</p></div></div>