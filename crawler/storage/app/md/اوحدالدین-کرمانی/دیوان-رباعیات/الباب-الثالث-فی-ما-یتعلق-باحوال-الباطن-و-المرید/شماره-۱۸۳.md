---
title: >-
    شمارهٔ  ۱۸۳
---
# شمارهٔ  ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>هرگه که تو آوری بیانی از فقر</p></div>
<div class="m2"><p>در حال تو را دهند جانی از فقر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ترک وجود هر دو عالم نکنی</p></div>
<div class="m2"><p>معلوم نگرددت نشانی از فقر</p></div></div>