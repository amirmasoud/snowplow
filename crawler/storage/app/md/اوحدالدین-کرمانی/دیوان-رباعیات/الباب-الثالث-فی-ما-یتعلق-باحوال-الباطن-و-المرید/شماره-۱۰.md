---
title: >-
    شمارهٔ  ۱۰
---
# شمارهٔ  ۱۰

<div class="b" id="bn1"><div class="m1"><p>هر چند که تو چارهٔ بهبود کنی</p></div>
<div class="m2"><p>آن به که هر آنچ می کنی زود کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان می ترسم که چون پشیمان گردی</p></div>
<div class="m2"><p>آن مایه نماند که بدان سود کنی</p></div></div>