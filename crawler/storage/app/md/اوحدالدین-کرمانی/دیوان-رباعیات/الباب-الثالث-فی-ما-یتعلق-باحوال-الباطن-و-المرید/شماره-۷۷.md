---
title: >-
    شمارهٔ  ۷۷
---
# شمارهٔ  ۷۷

<div class="b" id="bn1"><div class="m1"><p>یا قلب ترید وصله مجّانا</p></div>
<div class="m2"><p>هذا هوس و لیته ما کانا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فی النار ولَو بجنّة یلقانا</p></div>
<div class="m2"><p>دع یلقانا لعله یلقانا</p></div></div>