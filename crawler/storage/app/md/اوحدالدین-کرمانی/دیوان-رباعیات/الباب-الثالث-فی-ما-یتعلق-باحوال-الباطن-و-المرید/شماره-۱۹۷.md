---
title: >-
    شمارهٔ  ۱۹۷
---
# شمارهٔ  ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>جانی که زمهر زیر میغش داری</p></div>
<div class="m2"><p>باید که همیشه زیر تیغش داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان و دل تو که هردوان بخشش اوست</p></div>
<div class="m2"><p>خود آن ارزد کزو دریغش داری</p></div></div>