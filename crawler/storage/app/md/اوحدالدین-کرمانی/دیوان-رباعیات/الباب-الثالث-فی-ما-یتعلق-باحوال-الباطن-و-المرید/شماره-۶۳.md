---
title: >-
    شمارهٔ  ۶۳
---
# شمارهٔ  ۶۳

<div class="b" id="bn1"><div class="m1"><p>روزی که جمال تو مرا دیده شود</p></div>
<div class="m2"><p>از فرق سرم تا به قدم دیده شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در من نگری همه تنم جان گردد</p></div>
<div class="m2"><p>در تو نگرم همه تنم دیده شود</p></div></div>