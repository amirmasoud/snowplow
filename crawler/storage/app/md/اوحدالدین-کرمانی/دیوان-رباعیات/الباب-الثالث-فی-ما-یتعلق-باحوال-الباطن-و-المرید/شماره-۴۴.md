---
title: >-
    شمارهٔ  ۴۴
---
# شمارهٔ  ۴۴

<div class="b" id="bn1"><div class="m1"><p>ای دل صفت جمال او نتوان گفت</p></div>
<div class="m2"><p>وز مرتبهٔ کمال او نتوان گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند مقرّبی ولی از هیبت</p></div>
<div class="m2"><p>هرگز سخن وصال او نتوان گفت</p></div></div>