---
title: >-
    شمارهٔ  ۱۰۲
---
# شمارهٔ  ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>ای دل دَرِ غم گشاده ای می بینم</p></div>
<div class="m2"><p>در دام بلا فتاده ای می بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از یار کناره کرده ای می دانم</p></div>
<div class="m2"><p>دل در دگری نهاده ای می بینم</p></div></div>