---
title: >-
    شمارهٔ  ۲۸۱
---
# شمارهٔ  ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>زین سان که مراست از تو در سینه سرور</p></div>
<div class="m2"><p>می ترسم از آنک خواجه گردد مغرور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مغرور مشو که شاهد ما معنی است</p></div>
<div class="m2"><p>نزدیک میا تا نکنی صورت دور</p></div></div>