---
title: >-
    شمارهٔ  ۱۷۸
---
# شمارهٔ  ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>میل دل ما جز به فقیری نبود</p></div>
<div class="m2"><p>خرسند به میر جز اسیری نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از صدق اثری در دل شخصی دیدم</p></div>
<div class="m2"><p>ورنه دل ما منزل میری نبود</p></div></div>