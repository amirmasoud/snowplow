---
title: >-
    شمارهٔ  ۵۰
---
# شمارهٔ  ۵۰

<div class="b" id="bn1"><div class="m1"><p>دل رفته و عشق آمده تا خود چه کند</p></div>
<div class="m2"><p>جان داده و غم بستده تا خود چه کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حالت خود نشان نمی دانم داد</p></div>
<div class="m2"><p>کاری است به هم برشده تا خود چه کند</p></div></div>