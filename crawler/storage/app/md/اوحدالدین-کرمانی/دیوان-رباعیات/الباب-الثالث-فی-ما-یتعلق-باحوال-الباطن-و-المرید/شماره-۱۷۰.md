---
title: >-
    شمارهٔ  ۱۷۰
---
# شمارهٔ  ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>اندر ره فقر دیده نادیده کنند</p></div>
<div class="m2"><p>هرچ آن نه حدیث اوست نشنیده کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک در او باش که شاهان جهان</p></div>
<div class="m2"><p>خاک در تو چو سرمه در دیده کنند</p></div></div>