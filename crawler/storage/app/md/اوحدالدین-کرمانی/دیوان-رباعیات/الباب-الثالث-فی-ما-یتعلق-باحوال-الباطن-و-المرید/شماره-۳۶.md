---
title: >-
    شمارهٔ  ۳۶
---
# شمارهٔ  ۳۶

<div class="b" id="bn1"><div class="m1"><p>از شمع وصال کمترک یابی نور</p></div>
<div class="m2"><p>ما کنت مقیداً بتیه و غرُور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریاب فتوح وز رعونت شو دور</p></div>
<div class="m2"><p>احضر نفساً و کل کاین مقدور</p></div></div>