---
title: >-
    شمارهٔ  ۱۵
---
# شمارهٔ  ۱۵

<div class="b" id="bn1"><div class="m1"><p>ای دل به غم فراق رایی می زن</p></div>
<div class="m2"><p>بر چنگ امید او نوایی می زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار هزیمت مشو ار خسته شوی</p></div>
<div class="m2"><p>افتاده و خسته دست و پایی می زن</p></div></div>