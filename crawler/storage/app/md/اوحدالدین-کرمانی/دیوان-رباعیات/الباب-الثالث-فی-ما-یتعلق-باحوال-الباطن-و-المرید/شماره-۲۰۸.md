---
title: >-
    شمارهٔ  ۲۰۸
---
# شمارهٔ  ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>مستی زمی عشق و نه مستی زمی است</p></div>
<div class="m2"><p>وآن کس که زمی مست بود مست کی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوانه بهار دید گفتا که دَی است</p></div>
<div class="m2"><p>جنبیدن هر کسی از آنجا که وی است</p></div></div>