---
title: >-
    شمارهٔ  ۲۴۹
---
# شمارهٔ  ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>این آزادی هزار جان بیش ارزد</p></div>
<div class="m2"><p>و این تنهایی ملک جهان بیش ارزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خلوت یک زمان با خود بودن</p></div>
<div class="m2"><p>از جان و جهان این و آن بیش ارزد</p></div></div>