---
title: >-
    شمارهٔ  ۲۱۵
---
# شمارهٔ  ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>قلّاش و قلندری و عاشق بودن</p></div>
<div class="m2"><p>می خواره و بت پرست و فاسق بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کنج خرابات موافق بودن</p></div>
<div class="m2"><p>به زانک به خرقه در منافق بودن</p></div></div>