---
title: >-
    شمارهٔ  ۸۲
---
# شمارهٔ  ۸۲

<div class="b" id="bn1"><div class="m1"><p>جان بر سر عشق پای فرسود ای دل</p></div>
<div class="m2"><p>بر ما در هر حادثه بگشود ای دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون که فراق روی بنمود ای دل</p></div>
<div class="m2"><p>زنده به کدام جان توان بود ای دل</p></div></div>