---
title: >-
    شمارهٔ  ۲۶۲
---
# شمارهٔ  ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>سرمایهٔ عمر ابن آدم نفسی است</p></div>
<div class="m2"><p>پیرایهٔ ملک جمله عالم هوسی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاری بکن اکنون که تو را دسترسی است</p></div>
<div class="m2"><p>در زیر زمین شاه جهاندار بسی است</p></div></div>