---
title: >-
    شمارهٔ  ۲۱
---
# شمارهٔ  ۲۱

<div class="b" id="bn1"><div class="m1"><p>گر می خواهی بقا و پیروز مخسب</p></div>
<div class="m2"><p>بر آتش عشق دوست می سوز مخسب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد شب خفتی و حاصلت آن دیدی</p></div>
<div class="m2"><p>از بهر خدا امشب تا روز مخسب</p></div></div>