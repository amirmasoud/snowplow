---
title: >-
    شمارهٔ  ۴۲
---
# شمارهٔ  ۴۲

<div class="b" id="bn1"><div class="m1"><p>آنی که فلک با تو درآید به طرب</p></div>
<div class="m2"><p>گر آدمئی شیفته گردد چه عجب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز بندگی تو من نخواهم کردن</p></div>
<div class="m2"><p>خواهی بطلب مرا و خواهی مطلب</p></div></div>