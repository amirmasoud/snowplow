---
title: >-
    شمارهٔ  ۱۵۶
---
# شمارهٔ  ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>الفقر اذا ابعدکم یدنیکم</p></div>
<div class="m2"><p>و الفقر اذا اماتکم یُحییکم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا اخوانی بفقرکم اوصیکم</p></div>
<div class="m2"><p>الفقر عناً و ذلکم یکفیکُم</p></div></div>