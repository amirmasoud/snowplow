---
title: >-
    شمارهٔ  ۲۵۵
---
# شمارهٔ  ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>عمری به مراد خود رسیدی، بس کن</p></div>
<div class="m2"><p>یکچند به کام خود دویدی، بس کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نامه سیه کردن خود شرمت باد</p></div>
<div class="m2"><p>چون موی سیه سپید دیدی، بس کن</p></div></div>