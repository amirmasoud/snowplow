---
title: >-
    شمارهٔ  ۱۳۹
---
# شمارهٔ  ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>تا ظن نبری که خان و مان محتشمی است</p></div>
<div class="m2"><p>یا خواسته و حکم روان محتشمی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در درویشی اگر تو قانع باشی</p></div>
<div class="m2"><p>حقّا و به جان تو که آن محتشمی است</p></div></div>