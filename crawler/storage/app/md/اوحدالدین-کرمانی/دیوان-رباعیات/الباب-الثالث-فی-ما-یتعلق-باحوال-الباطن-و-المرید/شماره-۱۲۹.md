---
title: >-
    شمارهٔ  ۱۲۹
---
# شمارهٔ  ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>از بهر چه حل نمی کنی مشکل خود</p></div>
<div class="m2"><p>وز بند هوس نمی رهانی دل خود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینجا تو برای حاصلی آمده ای</p></div>
<div class="m2"><p>ای بی حاصل باز طلب حاصل خود</p></div></div>