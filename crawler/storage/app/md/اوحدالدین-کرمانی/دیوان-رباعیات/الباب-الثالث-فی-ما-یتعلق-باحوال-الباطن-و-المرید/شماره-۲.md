---
title: >-
    شمارهٔ  ۲
---
# شمارهٔ  ۲

<div class="b" id="bn1"><div class="m1"><p>باید که زجمله خلق تنها گردی</p></div>
<div class="m2"><p>آنگه به طریق خرقه پیدا گردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگه که به لبس خرقه گردی قانع</p></div>
<div class="m2"><p>چون خرقه کفن شود تو رسوا گردی</p></div></div>