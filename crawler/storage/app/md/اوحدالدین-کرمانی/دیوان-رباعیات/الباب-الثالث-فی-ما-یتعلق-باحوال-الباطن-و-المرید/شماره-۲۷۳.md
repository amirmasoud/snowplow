---
title: >-
    شمارهٔ  ۲۷۳
---
# شمارهٔ  ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>هان تا تو ببندی به مراعاتش پشت</p></div>
<div class="m2"><p>کاو با گل نرم پرورد خار درشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هان تا نشوی غزّه به دریای کرم</p></div>
<div class="m2"><p>کاو بر لب بحر تشنه بسیار بکشت</p></div></div>