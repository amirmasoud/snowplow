---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>جانا به جز از یاد تو در سر هوسم نیست</p></div>
<div class="m2"><p>سوگند خورم من که به جای تو کسم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز منم خسته و بی مونس و بی یار</p></div>
<div class="m2"><p>فریاد همی خواهم و فریادرسم نیست</p></div></div>