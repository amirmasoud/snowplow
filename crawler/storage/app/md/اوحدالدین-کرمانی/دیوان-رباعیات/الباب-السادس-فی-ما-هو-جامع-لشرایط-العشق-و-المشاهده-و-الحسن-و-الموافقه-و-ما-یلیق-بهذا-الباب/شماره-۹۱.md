---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>هر دل که غم عشق تو را گشت شکار</p></div>
<div class="m2"><p>با کعبه و بتخانه ندارد پیکار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون در ره عشق کفر و دین یک رنگ اند</p></div>
<div class="m2"><p>بتخانه و کعبه را در آن راه چه کار</p></div></div>