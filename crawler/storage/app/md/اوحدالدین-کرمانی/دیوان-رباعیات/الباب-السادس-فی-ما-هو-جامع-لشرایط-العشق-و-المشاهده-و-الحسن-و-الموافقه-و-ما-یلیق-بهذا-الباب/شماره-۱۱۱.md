---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>ما جز به غم عشق تو سرنفرازیم</p></div>
<div class="m2"><p>تا سرداریم در غمت سربازیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تو سرما بی سر و پایان داری</p></div>
<div class="m2"><p>ماییم و سری، در قدمت اندازیم</p></div></div>