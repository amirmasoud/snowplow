---
title: >-
    شمارهٔ ۲۶۲
---
# شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>یاری که به وقت کار در کار آید</p></div>
<div class="m2"><p>وی را چو طلب کنی دل افگار آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این یار که بار تو کشد کم یابی</p></div>
<div class="m2"><p>گر بار کشی جمله جهان یار آید</p></div></div>