---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>درمان چه کنی اگر تو درمانی سوز</p></div>
<div class="m2"><p>حاصل طلب ار طالب درمانی سوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوز است که ساز عالم است ای مسکین</p></div>
<div class="m2"><p>تو سوخته نیستی کجا دانی سوز</p></div></div>