---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>ای دل عَلَم عشق برافراز و مترس</p></div>
<div class="m2"><p>وز سر کُلَه کبر برانداز و مترس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوری همه دشمنان بی معنی را</p></div>
<div class="m2"><p>با ما همه همچو خویش در ساز و مترس</p></div></div>