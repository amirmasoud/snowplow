---
title: >-
    شمارهٔ ۱۶۶
---
# شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>در عشق حمول و حمله کش می باشم</p></div>
<div class="m2"><p>وندر صف عاشقان کش می باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با نیک و بد جهان مرا کاری نیست</p></div>
<div class="m2"><p>با آنک خوش است نیک خوش می باشم</p></div></div>