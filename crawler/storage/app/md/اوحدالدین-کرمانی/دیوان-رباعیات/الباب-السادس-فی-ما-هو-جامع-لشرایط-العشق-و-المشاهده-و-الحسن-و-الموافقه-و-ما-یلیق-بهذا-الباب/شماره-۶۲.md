---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>در عالم عشق کفر ایمان باشد</p></div>
<div class="m2"><p>آنجای گناه و توبه یکسان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جایی که عبادت می و مستی دانند</p></div>
<div class="m2"><p>آنجای نماز و روز عصیان باشد</p></div></div>