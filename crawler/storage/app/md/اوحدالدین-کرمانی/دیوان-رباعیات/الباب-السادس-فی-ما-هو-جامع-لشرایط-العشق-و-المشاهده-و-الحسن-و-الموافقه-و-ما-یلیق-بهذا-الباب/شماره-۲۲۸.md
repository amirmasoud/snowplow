---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>زان روز که چشم من به رویت نگریست</p></div>
<div class="m2"><p>نگذشت شبی که از غمت خون نگریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشتاب که دل بی تو نمی داند زیست</p></div>
<div class="m2"><p>دریاب که جان بی تو نمی داند زیست</p></div></div>