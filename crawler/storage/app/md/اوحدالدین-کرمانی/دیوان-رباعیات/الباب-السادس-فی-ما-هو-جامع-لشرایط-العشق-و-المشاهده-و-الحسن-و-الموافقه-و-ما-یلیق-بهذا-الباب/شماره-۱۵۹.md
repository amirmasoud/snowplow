---
title: >-
    شمارهٔ ۱۵۹
---
# شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>با عشق اگرت رای بود همرازی</p></div>
<div class="m2"><p>باید که دل از مراد وا پردازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چیز که بر مراد طبع تو بود</p></div>
<div class="m2"><p>خواهیش نماز گیر و خواهش بازی</p></div></div>