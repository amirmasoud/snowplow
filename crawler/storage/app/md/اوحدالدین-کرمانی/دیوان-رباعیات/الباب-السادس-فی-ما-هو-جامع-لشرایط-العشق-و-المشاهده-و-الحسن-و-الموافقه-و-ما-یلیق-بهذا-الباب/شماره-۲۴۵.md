---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>یارم به سفر چو راه رفتن بگزید</p></div>
<div class="m2"><p>نرگس دیدم ازو روان مروارید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیچاره دلم در پی او می نگرید</p></div>
<div class="m2"><p>می گفت که الوداع و خون می بارید</p></div></div>