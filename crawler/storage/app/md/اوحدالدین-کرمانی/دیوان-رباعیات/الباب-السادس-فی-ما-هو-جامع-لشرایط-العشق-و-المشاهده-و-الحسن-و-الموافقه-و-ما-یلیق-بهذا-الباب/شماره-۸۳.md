---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>مخلوق زعاشقی نشان چون یابد</p></div>
<div class="m2"><p>کز روح سبک شخص گران چون یابد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق آتش تیز است و تو کاه [و] کبریت</p></div>
<div class="m2"><p>کبریت و کَه از آتش امان چون یابد</p></div></div>