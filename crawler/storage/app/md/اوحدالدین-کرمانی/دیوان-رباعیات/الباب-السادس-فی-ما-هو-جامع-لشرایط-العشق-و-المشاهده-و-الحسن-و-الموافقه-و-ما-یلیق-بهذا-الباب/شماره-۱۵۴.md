---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>ای دل چو تو از دامن حُسن آویزی</p></div>
<div class="m2"><p>باید که زهیچ زحمتی نگریزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرط است که چون تو پای در عشق نهی</p></div>
<div class="m2"><p>اول گامی زکام خود برخیزی</p></div></div>