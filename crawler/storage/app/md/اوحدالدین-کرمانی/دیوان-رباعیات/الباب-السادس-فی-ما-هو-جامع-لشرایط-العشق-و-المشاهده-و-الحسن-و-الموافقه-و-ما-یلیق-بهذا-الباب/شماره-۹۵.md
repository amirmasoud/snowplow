---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>شمع است رخ خوب تو پروانه طِراز</p></div>
<div class="m2"><p>سودات مفرح است دیوانه فراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق تو زآن نای مرا نیست که هست</p></div>
<div class="m2"><p>شب کوته و تو ملول و افسانه دراز</p></div></div>