---
title: >-
    شمارهٔ ۲۰۹
---
# شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>گه بوی خوشت زپیرهن می شنوم</p></div>
<div class="m2"><p>گه شرح غمت ز مرد و زن می شنوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور هیچ نباشد کسکی بنشانم</p></div>
<div class="m2"><p>کاو نام تو می گوید و من می شنوم</p></div></div>