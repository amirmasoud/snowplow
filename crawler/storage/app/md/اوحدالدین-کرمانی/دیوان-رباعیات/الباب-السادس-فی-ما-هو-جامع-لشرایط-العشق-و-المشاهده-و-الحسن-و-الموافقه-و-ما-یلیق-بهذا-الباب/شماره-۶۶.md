---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>دل در پی عشق دوست سودا بینید</p></div>
<div class="m2"><p>جان طالب وصل است، تمنّا بینید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود را بر خاص و عام رسوا کردم</p></div>
<div class="m2"><p>از بهر خدا عاشق رسوا بینید</p></div></div>