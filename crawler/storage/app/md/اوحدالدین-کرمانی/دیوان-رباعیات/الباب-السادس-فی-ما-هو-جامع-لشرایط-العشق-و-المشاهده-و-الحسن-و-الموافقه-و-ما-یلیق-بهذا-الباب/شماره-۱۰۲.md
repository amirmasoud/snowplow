---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>تا بر لگن عشق سواریم چو شمع</p></div>
<div class="m2"><p>نقش همه کس فرا پذیریم چو شمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشّاق قلندریم و شرط است که ما</p></div>
<div class="m2"><p>آن شب که نسوزیم بمیریم چو شمع</p></div></div>