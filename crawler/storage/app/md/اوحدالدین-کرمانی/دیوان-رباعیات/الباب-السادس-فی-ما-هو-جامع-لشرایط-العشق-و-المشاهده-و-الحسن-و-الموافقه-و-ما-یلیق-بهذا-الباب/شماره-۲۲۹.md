---
title: >-
    شمارهٔ ۲۲۹
---
# شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>تا دست وصال تو نگیرم در دست</p></div>
<div class="m2"><p>وز دولت مسکونت نگردم سرمست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی لب روزی به خنده خواهم بگشاد</p></div>
<div class="m2"><p>نه چشم شبی زگریه خواهم در بست</p></div></div>