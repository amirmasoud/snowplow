---
title: >-
    شمارهٔ ۲۲۵
---
# شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>یا حاسب هل تعلم ماذا تَصنَع</p></div>
<div class="m2"><p>ترجو و ترومُ ما لمثلک یُمنع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکفیک هواهُ وصلهُ لا تطمع</p></div>
<div class="m2"><p>من این الی این تادّب وَاقنع</p></div></div>