---
title: >-
    شمارهٔ ۱۵۵
---
# شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>عشق تو فزون است زبینایی من</p></div>
<div class="m2"><p>راز تو برون است زدانایی من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق تو انتهاست تنهایی من</p></div>
<div class="m2"><p>در دست تو عاجز است توانایی من</p></div></div>