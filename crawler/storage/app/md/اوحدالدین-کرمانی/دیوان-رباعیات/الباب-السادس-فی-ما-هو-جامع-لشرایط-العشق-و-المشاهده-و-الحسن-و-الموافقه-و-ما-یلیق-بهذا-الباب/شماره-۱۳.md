---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>عشق تو همه دینی و دنیاوی ماست</p></div>
<div class="m2"><p>در عشق تو گر هیچ نداریم رواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق تو هر گدای سلطان باشد</p></div>
<div class="m2"><p>سلطان که ندارد غم عشق تو گداست</p></div></div>