---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>از عشق شود ادیب عاقل مجنون</p></div>
<div class="m2"><p>وز عشق شود عافیت از پرده برون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار به عشق در ملامت نکنی</p></div>
<div class="m2"><p>چون عشق آمد نه صبر ماند نه سکون</p></div></div>