---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>ای دل چو غم عشق برای من و تُست</p></div>
<div class="m2"><p>سر بر خط او نه که سزای من و تُست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو چاشنی درد نداری ورنه</p></div>
<div class="m2"><p>یک دم غم یار خون بهای من و تُست</p></div></div>