---
title: >-
    شمارهٔ ۲۴۱
---
# شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>بی دیدن تو بیم هلاک است مرا</p></div>
<div class="m2"><p>پیراهن صبرم همه چاک است مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز فرقت دیدار تو ای جان و جهان</p></div>
<div class="m2"><p>یا رب چه عظیم دردناک است مرا</p></div></div>