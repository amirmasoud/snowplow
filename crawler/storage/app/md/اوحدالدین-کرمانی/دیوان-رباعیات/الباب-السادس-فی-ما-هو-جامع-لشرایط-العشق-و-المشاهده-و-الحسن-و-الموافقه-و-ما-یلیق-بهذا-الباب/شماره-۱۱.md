---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>ای خوش پسران که عقل مدهوش شماست</p></div>
<div class="m2"><p>دل چاکر آن عارض گل پوش شماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زر را چه محل که سر فدا باید کرد</p></div>
<div class="m2"><p>آن را که سر سیم بناگوش شماست</p></div></div>