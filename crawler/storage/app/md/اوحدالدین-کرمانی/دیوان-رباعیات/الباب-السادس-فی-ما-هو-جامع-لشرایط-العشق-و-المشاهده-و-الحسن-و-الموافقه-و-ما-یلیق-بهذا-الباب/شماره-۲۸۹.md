---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>با دشمن و با دوست نه صلح است و نه حرب</p></div>
<div class="m2"><p>گاهم زند این طعنه گه آن دیگر ضرب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غصّهٔ همنشین ناهموار آب</p></div>
<div class="m2"><p>معذور بود گر رود از شرق به غرب</p></div></div>