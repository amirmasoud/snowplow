---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>در کوی تو سر بر سر خنجر بنهم</p></div>
<div class="m2"><p>چون مهرهٔ جان عشق تو در بر بنهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نا مردم اگر عشق تو از دل بکنم</p></div>
<div class="m2"><p>سودای تو کافرم گر از سر بنهم</p></div></div>