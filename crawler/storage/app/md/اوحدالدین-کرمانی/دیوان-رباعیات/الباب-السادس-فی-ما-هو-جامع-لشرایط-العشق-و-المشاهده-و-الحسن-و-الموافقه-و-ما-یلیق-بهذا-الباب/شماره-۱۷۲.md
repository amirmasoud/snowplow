---
title: >-
    شمارهٔ ۱۷۲
---
# شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>عاشق شوی و از دل و جان اندیشی</p></div>
<div class="m2"><p>دُردی کشی و زپاسبان اندیشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دعوی محبّت کنی و لاف زنی</p></div>
<div class="m2"><p>وانگه ز زبان این و آن اندیشی</p></div></div>