---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>در عشق حدیث کفر و ایمان نکند</p></div>
<div class="m2"><p>بر در دربند و بام درمان نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنجا که شه عشق فرو آرد سر</p></div>
<div class="m2"><p>در پای غمش نثار جز جان نکند</p></div></div>