---
title: >-
    شمارهٔ ۲۵۷
---
# شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>سرمایهٔ عمر عاقلان یک نفس است</p></div>
<div class="m2"><p>پس همنفسی جو که جهان یک نفس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با همنفسی گر نفسی دست دهد</p></div>
<div class="m2"><p>مجموع حساب عمر آن یک نفس است</p></div></div>