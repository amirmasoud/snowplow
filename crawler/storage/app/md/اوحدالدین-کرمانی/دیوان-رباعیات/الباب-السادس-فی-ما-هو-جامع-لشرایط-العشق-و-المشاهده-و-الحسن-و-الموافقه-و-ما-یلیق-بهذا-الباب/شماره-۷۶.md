---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>عشّاق کجا زبوی و رنگ اندیشند</p></div>
<div class="m2"><p>یا از غم هجر و دل تنگ اندیشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که شود نام نکو در سر عشق</p></div>
<div class="m2"><p>کی دل شدگان زنام و ننگ اندیشند</p></div></div>