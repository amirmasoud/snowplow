---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>ما شربت عشقت نه به بازی خوردیم</p></div>
<div class="m2"><p>سودای تو را نه از هوس پروردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود را هدف تیر ملامت کردیم</p></div>
<div class="m2"><p>گر بر گردیم ازین سخن نامردیم</p></div></div>