---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>ما عشق تو را به جان و دل بخریدیم</p></div>
<div class="m2"><p>وز بهر تو از جمله جهان ببُریدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را زملامت پس ازین باکی نیست</p></div>
<div class="m2"><p>چون پردهٔ خود به دست خود بدریدیم</p></div></div>