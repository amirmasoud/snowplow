---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>ای دل بر یار گر نمی یابی بار</p></div>
<div class="m2"><p>پادار وزو تو سر مگردان زنهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاندر ره عشق چون ثباتت باشد</p></div>
<div class="m2"><p>ناچار به مقصود رسی آخر کار</p></div></div>