---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>عشقت که زجمله خلق هستی بربود</p></div>
<div class="m2"><p>هشیاری ما به بوی مستی بربود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بودم و نیم دل به صد خون جگر</p></div>
<div class="m2"><p>وآن نیز غمت به چربدستی بربود</p></div></div>