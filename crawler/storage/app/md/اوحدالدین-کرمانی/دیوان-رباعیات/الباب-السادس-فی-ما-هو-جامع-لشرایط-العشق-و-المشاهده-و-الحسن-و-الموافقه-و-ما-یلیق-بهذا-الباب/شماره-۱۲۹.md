---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>در عشق درآی و خانه پردازی کن</p></div>
<div class="m2"><p>مانندهٔ پروانه سراندازی کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور زانک زعشق عافیت می طلبی</p></div>
<div class="m2"><p>پس عشق نه کار تست رو بازی کن</p></div></div>