---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>تا با خودم از عشق خبر نیست مرا</p></div>
<div class="m2"><p>جز بر در دل هیچ گذر نیست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون من به میان نیم تویی حاصل من</p></div>
<div class="m2"><p>جز من به تو مانعی دگر نیست مرا</p></div></div>