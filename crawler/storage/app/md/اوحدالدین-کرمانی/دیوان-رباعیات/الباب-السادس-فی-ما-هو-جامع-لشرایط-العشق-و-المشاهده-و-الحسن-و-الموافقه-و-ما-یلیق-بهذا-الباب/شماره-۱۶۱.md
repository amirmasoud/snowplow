---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>اینجا پر طاوس به کرکس ندهند</p></div>
<div class="m2"><p>خود را چُو پلاس سازد اطلس ندهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>«اوحد» تو هوای نفس را عشق مخوان</p></div>
<div class="m2"><p>کاین عشق عزیز است به هر خَس ندهند</p></div></div>