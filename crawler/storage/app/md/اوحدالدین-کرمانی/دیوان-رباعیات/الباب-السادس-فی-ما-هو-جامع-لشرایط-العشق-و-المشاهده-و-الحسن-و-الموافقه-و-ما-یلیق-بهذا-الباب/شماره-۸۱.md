---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>در کوی تو هیچ کس ره آسان نبرد</p></div>
<div class="m2"><p>جز شیفتهٔ بی سر و سامان نبرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آن کس که به دام عشق تو پای نهاد</p></div>
<div class="m2"><p>تا سر ندهد زدست تو جان نبرد</p></div></div>