---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>در عشق تو گر کشته شوم باکی نیست</p></div>
<div class="m2"><p>کم دامن عشق است بر او چاکی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلقی زپی تو دوست دشمن گشتند</p></div>
<div class="m2"><p>با این همه چون تو دوستی باکی نیست</p></div></div>