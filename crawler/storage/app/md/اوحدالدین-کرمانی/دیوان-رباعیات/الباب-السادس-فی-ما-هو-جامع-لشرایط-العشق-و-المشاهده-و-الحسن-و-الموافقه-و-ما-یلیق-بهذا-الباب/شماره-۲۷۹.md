---
title: >-
    شمارهٔ ۲۷۹
---
# شمارهٔ ۲۷۹

<div class="b" id="bn1"><div class="m1"><p>خوبان همه گردن نفرازند آخر</p></div>
<div class="m2"><p>یکباره چنین به بر نتازند آخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که دلفریب و دلبر باشند</p></div>
<div class="m2"><p>با خسته دلان نیز نسازند آخر</p></div></div>