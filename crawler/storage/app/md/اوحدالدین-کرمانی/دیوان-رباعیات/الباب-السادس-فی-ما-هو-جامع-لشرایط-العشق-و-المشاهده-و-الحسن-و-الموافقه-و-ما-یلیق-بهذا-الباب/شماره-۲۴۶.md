---
title: >-
    شمارهٔ ۲۴۶
---
# شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>روزی دو سه از وصل تو بودم دلشاد</p></div>
<div class="m2"><p>وز بند هوس شبی دو، دل گشت آزاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهد که دهد فراق، عیشم بر باد</p></div>
<div class="m2"><p>آری که فراق تو به روز من باد</p></div></div>