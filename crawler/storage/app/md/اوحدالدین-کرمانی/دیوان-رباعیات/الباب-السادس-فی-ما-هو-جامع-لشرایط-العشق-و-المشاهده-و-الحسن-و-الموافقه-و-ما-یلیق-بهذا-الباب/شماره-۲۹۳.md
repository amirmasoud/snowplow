---
title: >-
    شمارهٔ ۲۹۳
---
# شمارهٔ ۲۹۳

<div class="b" id="bn1"><div class="m1"><p>با من بت من هیچ نکو عهد نشد</p></div>
<div class="m2"><p>زو حاجت من روا به صد جهد نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تلخ سخنهاش عجب می دارم</p></div>
<div class="m2"><p>کان بر لب او گذشت چون شهد نشد</p></div></div>