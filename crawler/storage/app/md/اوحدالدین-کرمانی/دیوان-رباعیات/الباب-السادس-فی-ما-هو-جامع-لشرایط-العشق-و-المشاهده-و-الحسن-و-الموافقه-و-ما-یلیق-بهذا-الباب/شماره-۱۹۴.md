---
title: >-
    شمارهٔ ۱۹۴
---
# شمارهٔ ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>تا بر رخ چون گلت پدید است عرق</p></div>
<div class="m2"><p>از شرم رخت زگل چکیده است عرق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ابر شنیده ام که باران باشد</p></div>
<div class="m2"><p>بر چهرهٔ خورشید که دیده است عرق</p></div></div>