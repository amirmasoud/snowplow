---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>در عشق فدای دلبران باید بود</p></div>
<div class="m2"><p>با هر چه جز اوست سرگران باید بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن را که سری به دست ناید که نهد</p></div>
<div class="m2"><p>خاک کف پای سروران باید بود</p></div></div>