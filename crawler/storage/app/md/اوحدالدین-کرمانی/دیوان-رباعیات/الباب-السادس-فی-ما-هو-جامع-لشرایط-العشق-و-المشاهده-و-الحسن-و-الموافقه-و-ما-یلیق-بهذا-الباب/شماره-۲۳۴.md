---
title: >-
    شمارهٔ ۲۳۴
---
# شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>بر من سپه هجر تو پیروز مباد</p></div>
<div class="m2"><p>جز سوز تو در دلم دگر سوز مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن شب که مرا با تو وصالی باشد</p></div>
<div class="m2"><p>تا صبح قیامت ندمد روز مباد</p></div></div>