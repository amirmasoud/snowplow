---
title: >-
    شمارهٔ ۲۸۴
---
# شمارهٔ ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>هر بد که توان کاشت تو آن کاشته ای</p></div>
<div class="m2"><p>آزرم به هیچ روی نگذاشته ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این همه هم منم که دارم سر صلح</p></div>
<div class="m2"><p>هر چند که جای صلح نگذاشته ای</p></div></div>