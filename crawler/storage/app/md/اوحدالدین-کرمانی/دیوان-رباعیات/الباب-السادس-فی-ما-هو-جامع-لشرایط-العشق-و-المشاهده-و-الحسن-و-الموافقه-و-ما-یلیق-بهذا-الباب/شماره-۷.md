---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>تا بر سر کوی عشق شد منزل ما</p></div>
<div class="m2"><p>فریاد برآمد از نهاد دل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جستن خاک عشق از بس که شدیم</p></div>
<div class="m2"><p>خون شد دل ما و حل نشد مشکل ما</p></div></div>