---
title: >-
    شمارهٔ ۲۶۳
---
# شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>ای دل همه آن بکن که رایت باشد</p></div>
<div class="m2"><p>جایی منشین که آن نه جایت باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بتوانی رفیق درویش گزین</p></div>
<div class="m2"><p>کاو در همه عمر خاک پایت باشد</p></div></div>