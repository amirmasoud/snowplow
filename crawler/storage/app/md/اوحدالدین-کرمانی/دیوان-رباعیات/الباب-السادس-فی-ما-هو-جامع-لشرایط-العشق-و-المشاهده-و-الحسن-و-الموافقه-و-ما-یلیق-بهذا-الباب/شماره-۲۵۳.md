---
title: >-
    شمارهٔ ۲۵۳
---
# شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>ماییم زدل دور و ز دلبر مهجور</p></div>
<div class="m2"><p>در درد بمانده ایم وز درمان دور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبحان الله این چه پریشانیهاست</p></div>
<div class="m2"><p>جان خسته و دل سوخته و تن رنجور</p></div></div>