---
title: >-
    شمارهٔ ۱۴۰
---
# شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>ای دل اگرت هنوز می باید ازو</p></div>
<div class="m2"><p>باید که کشید هرچه می زاید ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق شده ای وفا طلب می داری</p></div>
<div class="m2"><p>دیوانَه ندانی که وفا ناید ازو</p></div></div>