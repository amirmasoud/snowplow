---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>بر باد اگر تو عشق شهوت دانی</p></div>
<div class="m2"><p>خاکت بر سر که سخت سرگردانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق آب حیات هر دو عالم باشد</p></div>
<div class="m2"><p>تو آتش شهوتش چرا می خوانی</p></div></div>