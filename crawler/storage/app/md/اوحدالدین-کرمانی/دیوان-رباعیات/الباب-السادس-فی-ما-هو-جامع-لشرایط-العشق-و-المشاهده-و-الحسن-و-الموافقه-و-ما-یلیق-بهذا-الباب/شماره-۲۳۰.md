---
title: >-
    شمارهٔ ۲۳۰
---
# شمارهٔ ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>امروز که یار من مرا مهمان است</p></div>
<div class="m2"><p>بخشیدن جان و دل مرا پیمان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را خطری نیست، سخن در جان است</p></div>
<div class="m2"><p>جان افشانم که وقت جان افشان است</p></div></div>