---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>داری سر آنک عشق بازی با ما</p></div>
<div class="m2"><p>ببُری زهمه خلق و بسازی با ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار دو جهان در سر کار تو کنیم</p></div>
<div class="m2"><p>گر شرط کنی که کژ نبازی با ما</p></div></div>