---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>گر در ره عشق او نباشی سرباز</p></div>
<div class="m2"><p>زنهار مکن حدیث عشقش آغاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر روشنیی می طلبی همچون شمع</p></div>
<div class="m2"><p>پروانه صفت تو خویشتن را درباز</p></div></div>