---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>از عشق تو بوی مختصر نتوان برد</p></div>
<div class="m2"><p>جز درد دل و سوز جگر نتوان برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی عشق تو هرک می برد عمر به سر</p></div>
<div class="m2"><p>ضایع تر از آن عمر به سر نتوان برد</p></div></div>