---
title: >-
    شمارهٔ ۲۵۶
---
# شمارهٔ ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>ای دلبر قصّاب نه سر می دهیَم</p></div>
<div class="m2"><p>نه شاخ امید هیچ بر می دهیَم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناخورده زگرد رانِ وصل تو هنوز</p></div>
<div class="m2"><p>در هجر چه گردن و جگر می دهیَم</p></div></div>