---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>بگریختم از عشق تو ای سیمین تن</p></div>
<div class="m2"><p>باشد که زغم باز رهم مسکین من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق آمد وزنیمه رهم باز آورد</p></div>
<div class="m2"><p>مانندهٔ خونیان رسن در گردن</p></div></div>