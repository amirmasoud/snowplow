---
title: >-
    شمارهٔ ۲۵۴
---
# شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>او را چه تمتّع از جوانی باشد</p></div>
<div class="m2"><p>کش بی تو حیات و زندگانی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیزارم از آنک سر برآرم بی تو</p></div>
<div class="m2"><p>گر خود همه عمر جاودانی باشد</p></div></div>