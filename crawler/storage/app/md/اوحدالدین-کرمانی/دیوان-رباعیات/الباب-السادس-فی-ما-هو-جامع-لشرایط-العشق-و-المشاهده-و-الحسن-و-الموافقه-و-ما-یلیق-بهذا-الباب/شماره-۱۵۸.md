---
title: >-
    شمارهٔ ۱۵۸
---
# شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>اندر ره عشق اگر تو هستی غازی</p></div>
<div class="m2"><p>با خون و رگ و پوست چه می پردازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شاهد شاهدی دگر پنهان است</p></div>
<div class="m2"><p>با آن شاهد خوش است شاهد بازی</p></div></div>