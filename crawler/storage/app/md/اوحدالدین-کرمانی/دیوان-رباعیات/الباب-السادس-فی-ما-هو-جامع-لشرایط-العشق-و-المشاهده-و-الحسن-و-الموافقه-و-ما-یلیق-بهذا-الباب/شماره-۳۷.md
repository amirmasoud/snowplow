---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>جز در دل و جان عاشقان جای تو نیست</p></div>
<div class="m2"><p>واندر سر و عقل جز تمنّای تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سوختم از آتش سودات رواست</p></div>
<div class="m2"><p>خامی است که در پختن سودای تو نیست</p></div></div>