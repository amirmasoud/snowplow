---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>از شربت عشق تست دل مست شده</p></div>
<div class="m2"><p>در پای هوای تست جان پست شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بر سر لطف خود ببستی ما را</p></div>
<div class="m2"><p>از پای فتاده گیر وزدست شده</p></div></div>