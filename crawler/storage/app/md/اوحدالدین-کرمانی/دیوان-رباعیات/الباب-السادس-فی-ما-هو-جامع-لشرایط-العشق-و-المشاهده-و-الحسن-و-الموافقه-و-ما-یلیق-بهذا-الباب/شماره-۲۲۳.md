---
title: >-
    شمارهٔ ۲۲۳
---
# شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>العمر مضی وفاتنی المطلوبُ</p></div>
<div class="m2"><p>لَا القلبُ اطاعنی و لا المحبوبُ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دمعی و دمی کلاهما مسلوبُ</p></div>
<div class="m2"><p>یا یوسُف صل فانّنی یعقوبُ</p></div></div>