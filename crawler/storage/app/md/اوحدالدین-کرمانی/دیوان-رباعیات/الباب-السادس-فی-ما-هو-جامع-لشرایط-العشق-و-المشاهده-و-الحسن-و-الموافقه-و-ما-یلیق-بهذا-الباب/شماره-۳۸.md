---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>از لذت عشق در جهان خوش تر چیست</p></div>
<div class="m2"><p>جز جان دادن درین میان خوش تر چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من دست ندارم از تو گر سر ببُرند</p></div>
<div class="m2"><p>چو پای به عشق در نهادم سر چیست</p></div></div>