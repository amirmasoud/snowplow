---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>زاول که مرا عشق نگارم بُر بود</p></div>
<div class="m2"><p>همسایهٔ من زنالهٔ من نغنود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و اکنون کم شد ناله و عشقم افزود</p></div>
<div class="m2"><p>آتش که همه گرفت کم گردد دود</p></div></div>