---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>عشق تو زخاص و عام پنهان چه کنم</p></div>
<div class="m2"><p>دردی که زحد گذشت درمان چه کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم که دلم به دیگری میل کند</p></div>
<div class="m2"><p>من خواهم و دل نخواهد ای جان چه کنم</p></div></div>