---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>در عشق بسی زیر و زبرها باشد</p></div>
<div class="m2"><p>مر عاشق را بسی خطرها باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پادار به هر دردسر از دست مرو</p></div>
<div class="m2"><p>کاندر ره عشق دردسرها باشد</p></div></div>