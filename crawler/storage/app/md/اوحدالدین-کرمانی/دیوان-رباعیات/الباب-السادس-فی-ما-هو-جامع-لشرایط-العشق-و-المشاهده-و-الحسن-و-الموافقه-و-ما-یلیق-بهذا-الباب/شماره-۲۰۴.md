---
title: >-
    شمارهٔ ۲۰۴
---
# شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>از عشق چنان است دل مسکینم</p></div>
<div class="m2"><p>کز عشق تو با جان خود اندر کینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبحان الله به هر چه در می نگرم</p></div>
<div class="m2"><p>از غایت آرزو تو را می بینم</p></div></div>