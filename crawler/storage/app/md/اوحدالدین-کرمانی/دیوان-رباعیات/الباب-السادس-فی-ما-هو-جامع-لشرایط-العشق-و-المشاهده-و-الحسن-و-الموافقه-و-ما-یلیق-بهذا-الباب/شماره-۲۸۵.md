---
title: >-
    شمارهٔ ۲۸۵
---
# شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>کو عقل که بندی زهوس بگشاید</p></div>
<div class="m2"><p>یا صبر که هنگام بلا برناید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا راهبری که راهکی بنماید</p></div>
<div class="m2"><p>یا همراهی که همدمی را شاید</p></div></div>