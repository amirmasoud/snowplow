---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>عشق تو و بس همنفس من این است</p></div>
<div class="m2"><p>واندر همه عالم هوس من این است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خود دانم که گفت و گو بیهوده است</p></div>
<div class="m2"><p>لیکن چه کنم دسترس من این است</p></div></div>