---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>در عشق تو جان بازم خود سر چه بود</p></div>
<div class="m2"><p>چون نیست غم تو سرسری سر چه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که به ترک سر توانی گفتن</p></div>
<div class="m2"><p>گر زآنک تو سر درآوری سر چه بود</p></div></div>