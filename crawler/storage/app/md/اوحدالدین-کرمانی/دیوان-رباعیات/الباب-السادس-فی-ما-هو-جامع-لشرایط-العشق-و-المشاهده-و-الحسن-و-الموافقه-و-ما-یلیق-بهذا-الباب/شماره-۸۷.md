---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>با عشق تو چون فتاده ما را سروکار</p></div>
<div class="m2"><p>گو بر سر ما تیر ملامت می بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست من و دامن تو امشب تا روز</p></div>
<div class="m2"><p>امشب من [و] وصل یار و فردا سر[و] دار</p></div></div>