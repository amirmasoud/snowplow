---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>بی روی تو رای استقامت نکنم</p></div>
<div class="m2"><p>در جستن وصل تو اقامت نکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس را به هوای تو ملامت نکنم</p></div>
<div class="m2"><p>وز عشق تو توبه تا قیامت نکنم</p></div></div>