---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>تا هستی خود را نکنی دامن چاک</p></div>
<div class="m2"><p>ثابت نشود تو را قدم بر افلاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند منی و من، زخود شرمت باد</p></div>
<div class="m2"><p>تو خود چه کسی، که ای، چه ای، مشتی خاک</p></div></div>