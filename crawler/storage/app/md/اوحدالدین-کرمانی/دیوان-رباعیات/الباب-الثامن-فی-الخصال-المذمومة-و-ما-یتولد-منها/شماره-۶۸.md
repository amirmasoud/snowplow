---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>می میرم ازو و صورت جان در پیش</p></div>
<div class="m2"><p>بر آتشم و روضهٔ رضوان در پیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عالم عشق طرفه حالی که مراست</p></div>
<div class="m2"><p>تشنه جگر و چشمهٔ حیوان در پیش</p></div></div>