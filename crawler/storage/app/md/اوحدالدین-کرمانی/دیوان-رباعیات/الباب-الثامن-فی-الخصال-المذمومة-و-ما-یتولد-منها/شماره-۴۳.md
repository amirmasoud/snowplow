---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>آن کس که سرشته باشد از آب منی</p></div>
<div class="m2"><p>او را نرسد که او کند کبر و منی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این است حدیث مصطفای مدنی</p></div>
<div class="m2"><p>من اکرمَ عالماً فقَد اکرمَنی</p></div></div>