---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>از خوی بد تو زان همی رنجد کس</p></div>
<div class="m2"><p>کاندر نظرت هیچ نمی سنجد کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک پوست فزون نیست تو را در همه تن</p></div>
<div class="m2"><p>چون از تو پُر است کی درو گنجد کس</p></div></div>