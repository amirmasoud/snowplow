---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>یکچند دویدیم نه بر راه صواب</p></div>
<div class="m2"><p>برداشته از روی خرد پاک نقاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون که همی باز کنم دیده به خواب</p></div>
<div class="m2"><p>هم نامه سیه بینی و هم عمر خراب</p></div></div>