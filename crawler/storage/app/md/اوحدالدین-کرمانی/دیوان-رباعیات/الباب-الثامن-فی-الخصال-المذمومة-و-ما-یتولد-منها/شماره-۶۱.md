---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>تا گوش دلت به غفلت است آکنده</p></div>
<div class="m2"><p>دل را تو مپندار که گردد زنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرمت ناید از آنک از خون تو بود</p></div>
<div class="m2"><p>سلطان باشد تو را تو او را بنده</p></div></div>