---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>بیگانگی ات چو با دل خویش آید</p></div>
<div class="m2"><p>هر جای که مرهمی زنی نیش آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد زخم خوری به تیغ بر تن به از آنک</p></div>
<div class="m2"><p>یک زخم زتو بر دل درویش آید</p></div></div>