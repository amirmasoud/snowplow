---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>در دل زغم زمانه باری دارم</p></div>
<div class="m2"><p>در دیدهٔ هر مراد خاری دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه همنفسی نه غمگساری دارم</p></div>
<div class="m2"><p>شوریده دلی و روزگاری دارم</p></div></div>