---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>هر شه که زعدل شد شعار و شانش</p></div>
<div class="m2"><p>نافذ باشد به ملک در فرمانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک چو گوی و عدل چوگان، به مثل</p></div>
<div class="m2"><p>گوی آن نبود که باشد آن چوگانش</p></div></div>