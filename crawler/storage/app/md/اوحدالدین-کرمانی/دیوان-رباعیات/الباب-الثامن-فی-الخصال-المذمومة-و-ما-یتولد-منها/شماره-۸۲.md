---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>تا چند ازین خلق خدا آزردن</p></div>
<div class="m2"><p>زین عالم فانی چه توانی بردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بر فلک از کبر کشیده گردن</p></div>
<div class="m2"><p>آخر نه خدایی! نه بخواهی مُردن؟</p></div></div>