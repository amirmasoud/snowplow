---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>هرگز دل من واقف اسرار نشد</p></div>
<div class="m2"><p>روزی به صفا و صدق بر کار نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس پند که بشنید و یکی گوش نکرد</p></div>
<div class="m2"><p>بس عبرتها که دید و بیدار نشد</p></div></div>