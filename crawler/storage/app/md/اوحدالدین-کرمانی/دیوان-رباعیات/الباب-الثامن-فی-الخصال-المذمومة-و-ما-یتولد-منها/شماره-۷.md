---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>رازت همه دارای فلک می داند</p></div>
<div class="m2"><p>کز موی به موی و رگ به رگ می داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرم که به زرق خلق را بفریبی</p></div>
<div class="m2"><p>با او چه کنی که یک به یک می داند</p></div></div>