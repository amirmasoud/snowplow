---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>الورد یقول بعد ما کنت اناس</p></div>
<div class="m2"><p>قد صرت من العجب مهانا واداس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>العجب دعوا فاعتبروا یا جلّاس</p></div>
<div class="m2"><p>طیبوا و تواضعوا و خلّوا الوسواس</p></div></div>