---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>از کبر مدار هیچ در دل هوسی</p></div>
<div class="m2"><p>از کبر به جایی نرسیده است کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون زلف بتان شکستگی عادت کن</p></div>
<div class="m2"><p>تا صید کنی هزار دل در نفسی</p></div></div>