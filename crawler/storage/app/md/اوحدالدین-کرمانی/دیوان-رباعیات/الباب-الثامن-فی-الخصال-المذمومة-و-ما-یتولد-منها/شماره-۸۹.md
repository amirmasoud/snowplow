---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>هان تا تو چو ظالمان ستمها نکنی</p></div>
<div class="m2"><p>وندر دل خستگان المها نکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می دان که به مقبلان تو همسر نشوی</p></div>
<div class="m2"><p>تا تو قدمت پی قدمها نکنی</p></div></div>