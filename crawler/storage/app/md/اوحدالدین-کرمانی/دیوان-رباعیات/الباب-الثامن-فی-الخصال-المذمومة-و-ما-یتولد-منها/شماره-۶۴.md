---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>صد زخم چشید نفس و افگار نشد</p></div>
<div class="m2"><p>صد تجربه کرد عقل و بر کار نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گردش چرخ صد هزاران عبرت</p></div>
<div class="m2"><p>این دیده بدید و هیچ بیدار نشد</p></div></div>