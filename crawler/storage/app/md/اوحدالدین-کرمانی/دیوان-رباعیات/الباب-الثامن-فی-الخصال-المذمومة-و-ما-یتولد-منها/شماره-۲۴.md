---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>هان ای «واحد» زباد حرصی تو اسیر</p></div>
<div class="m2"><p>زان نیست تو را زآتش حرص گزیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکت بر سر صوفی و دریوزهٔ زر</p></div>
<div class="m2"><p>ای رفته زرویت آب درویش و امیر</p></div></div>