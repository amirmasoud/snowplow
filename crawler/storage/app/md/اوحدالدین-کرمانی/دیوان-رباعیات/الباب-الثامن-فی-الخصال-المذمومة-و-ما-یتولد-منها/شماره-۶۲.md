---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>زین سان که تُوی دیده به خاک آکنده</p></div>
<div class="m2"><p>دشوار توان کرد تو را بیننده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیدار شود خفته به یک بانگ ولیک</p></div>
<div class="m2"><p>مرده نشود به هیچ بانگی زنده</p></div></div>