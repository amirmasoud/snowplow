---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>آزار چو باز و آز چون بط منمای</p></div>
<div class="m2"><p>چون بوم سوی سلامت طبع گرای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآنند دراز عمر و فرخنده لقای</p></div>
<div class="m2"><p>کازار نجست کرکس و آز همای</p></div></div>