---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>تا در سر تو مایهٔ مایی و منی است</p></div>
<div class="m2"><p>آگه نشوی که مایهٔ کار تو چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مایی و منی در می و مستی گم کن</p></div>
<div class="m2"><p>تا دریابی که مایه ت از کیسهٔ کیست</p></div></div>