---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>هر کاو درمی به خون دل جمع آرد</p></div>
<div class="m2"><p>می نگذاری تا به تو می نسپارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون می گذری و می گذاری همه را</p></div>
<div class="m2"><p>باری بگذار تا همو می دارد</p></div></div>