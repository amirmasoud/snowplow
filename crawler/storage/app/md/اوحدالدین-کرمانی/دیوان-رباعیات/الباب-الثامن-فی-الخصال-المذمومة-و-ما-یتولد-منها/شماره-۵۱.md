---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>از غایت خودپسندی و خودبینی</p></div>
<div class="m2"><p>عالم همه نیکند و تو شان بدبینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس نیست که کس نیست همه کس داند</p></div>
<div class="m2"><p>گر باز کنی دیدهٔ دل خودبینی</p></div></div>