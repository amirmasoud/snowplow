---
title: >-
    شمارهٔ  ۱۳۲ - نعت النبوة
---
# شمارهٔ  ۱۳۲ - نعت النبوة

<div class="b" id="bn1"><div class="m1"><p>جز در غم تو شادی من نفزاید</p></div>
<div class="m2"><p>جز در طلبت جان و دلم ناساید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک در تو چو سرمه در چشم کشم</p></div>
<div class="m2"><p>ملک دو جهان به چشمم اندر ناید</p></div></div>