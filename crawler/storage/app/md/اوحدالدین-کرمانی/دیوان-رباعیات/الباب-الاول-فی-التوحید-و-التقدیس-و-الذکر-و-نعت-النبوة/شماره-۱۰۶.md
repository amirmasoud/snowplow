---
title: >-
    شمارهٔ  ۱۰۶
---
# شمارهٔ  ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>چون من به تو دادم دل و دین بس باشد</p></div>
<div class="m2"><p>نفرین توم از آفرین بس باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من می گویم جمله تویی من هیچم</p></div>
<div class="m2"><p>تسبیح بزرگ من همین بس باشد</p></div></div>