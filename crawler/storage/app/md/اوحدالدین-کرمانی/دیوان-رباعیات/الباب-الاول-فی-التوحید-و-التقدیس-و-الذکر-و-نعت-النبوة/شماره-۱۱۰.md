---
title: >-
    شمارهٔ  ۱۱۰
---
# شمارهٔ  ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>ای از تو خرابی سبب آبادی</p></div>
<div class="m2"><p>وای در غم تو هزار جان را شادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بندگیت از دو جهان آزادم</p></div>
<div class="m2"><p>هرگز دیدی بنده بدین آزادی</p></div></div>