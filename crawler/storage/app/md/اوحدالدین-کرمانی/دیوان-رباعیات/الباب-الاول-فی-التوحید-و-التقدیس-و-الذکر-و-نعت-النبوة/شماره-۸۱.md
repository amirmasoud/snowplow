---
title: >-
    شمارهٔ  ۸۱
---
# شمارهٔ  ۸۱

<div class="b" id="bn1"><div class="m1"><p>تا خاک تو کحل دیدهٔ ما گردد</p></div>
<div class="m2"><p>در دیدهٔ ما سرّ تو پیدا گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رب تو به فضل خویش جایی برسان</p></div>
<div class="m2"><p>زان پیش که این دو رشته یکتا گردد</p></div></div>