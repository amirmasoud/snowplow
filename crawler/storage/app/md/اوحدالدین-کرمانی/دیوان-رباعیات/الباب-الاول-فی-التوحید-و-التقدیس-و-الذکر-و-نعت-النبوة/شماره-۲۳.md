---
title: >-
    شمارهٔ  ۲۳
---
# شمارهٔ  ۲۳

<div class="b" id="bn1"><div class="m1"><p>خواهی که ببینی دل کارآگه را</p></div>
<div class="m2"><p>و از خود به خدا عیان ببینی ره را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر تخت درون نشان به شمشیر زبان</p></div>
<div class="m2"><p>شاهنشه لا اله الّا الله را</p></div></div>