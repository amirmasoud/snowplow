---
title: >-
    شمارهٔ  ۴۰
---
# شمارهٔ  ۴۰

<div class="b" id="bn1"><div class="m1"><p>لطف تو و قهر تو همیشه به هم است</p></div>
<div class="m2"><p>لکن چو ضعیفیم به جان در ستم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای آنکه ز هیچ هر چه خواهی بکنی</p></div>
<div class="m2"><p>با من همه آن کن که طریق کرم است</p></div></div>