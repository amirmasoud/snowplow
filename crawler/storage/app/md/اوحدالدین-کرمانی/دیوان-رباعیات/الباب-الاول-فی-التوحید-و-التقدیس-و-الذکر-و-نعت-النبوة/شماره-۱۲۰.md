---
title: >-
    شمارهٔ  ۱۲۰
---
# شمارهٔ  ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>از درد سر خویش ندانم چونم</p></div>
<div class="m2"><p>وز دایرهٔ وجود خود بیرونم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رب تو مرا از سر و گردن برهان</p></div>
<div class="m2"><p>کز خود به سری زگردنی افزونم</p></div></div>