---
title: >-
    شمارهٔ  ۲۲۸ - الخوف
---
# شمارهٔ  ۲۲۸ - الخوف

<div class="b" id="bn1"><div class="m1"><p>قد کنت اقول لا ابالی بجفا</p></div>
<div class="m2"><p>کردیم چنانک می بنوشم زوفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الآن اذاصبّ من الحبّ صفا</p></div>
<div class="m2"><p>ترسم که کدورتیش باشد زقفا</p></div></div>