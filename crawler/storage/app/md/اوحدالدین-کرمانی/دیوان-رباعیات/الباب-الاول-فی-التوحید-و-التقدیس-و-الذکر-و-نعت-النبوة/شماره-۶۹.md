---
title: >-
    شمارهٔ  ۶۹
---
# شمارهٔ  ۶۹

<div class="b" id="bn1"><div class="m1"><p>ای آنک دوای دردمندان دانی</p></div>
<div class="m2"><p>درمان [و] علاج مستمندان دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچ از دل ریش خویش گویم با تو</p></div>
<div class="m2"><p>ناگفته تو صد هزار چندان دانی</p></div></div>