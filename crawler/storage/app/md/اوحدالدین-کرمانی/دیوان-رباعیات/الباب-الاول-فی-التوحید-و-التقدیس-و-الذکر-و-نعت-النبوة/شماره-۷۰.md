---
title: >-
    شمارهٔ  ۷۰
---
# شمارهٔ  ۷۰

<div class="b" id="bn1"><div class="m1"><p>ای راهنمای راه خوبان ازل</p></div>
<div class="m2"><p>وای طعمهٔ حضرتت نه علم و نه عمل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی علم و عمل به حضرتت راهی نیست</p></div>
<div class="m2"><p>وانگه به بر محققان هر دو زَلَل</p></div></div>