---
title: >-
    شمارهٔ  ۵۹
---
# شمارهٔ  ۵۹

<div class="b" id="bn1"><div class="m1"><p>از عشق تو هر لحظه فغان در بندم</p></div>
<div class="m2"><p>بیم است که شوری به جهان در بندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رب تو مرا به لطف توفیقی ده</p></div>
<div class="m2"><p>تا باز کنم چشم و زبان در بندم</p></div></div>