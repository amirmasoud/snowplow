---
title: >-
    شمارهٔ  ۵۸
---
# شمارهٔ  ۵۸

<div class="b" id="bn1"><div class="m1"><p>یا رب به خودم هیچ نفس وامگذار</p></div>
<div class="m2"><p>بر من در طبع بوالهوس وامگذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که بر درت کم از هیچ کسم</p></div>
<div class="m2"><p>از خویشتنم به هیچ کس وامگذار</p></div></div>