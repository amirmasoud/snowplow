---
title: >-
    شمارهٔ  ۲۳۵ - الخوف
---
# شمارهٔ  ۲۳۵ - الخوف

<div class="b" id="bn1"><div class="m1"><p>نی همچو منت به عمر یاری خیزد</p></div>
<div class="m2"><p>یاری چو منت به روزگاری خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خاک توم تو می دهی بر بادم</p></div>
<div class="m2"><p>ترسم که میان ما غباری خیزد</p></div></div>