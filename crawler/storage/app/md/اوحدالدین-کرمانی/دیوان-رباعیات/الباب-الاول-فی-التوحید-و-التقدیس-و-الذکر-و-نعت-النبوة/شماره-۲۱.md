---
title: >-
    شمارهٔ  ۲۱
---
# شمارهٔ  ۲۱

<div class="b" id="bn1"><div class="m1"><p>گفتم که زرخ پردهٔ عزّت بردار</p></div>
<div class="m2"><p>بسیار کس اند منتظر آن دیدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیکو سخنی بگفت آن زیبایار</p></div>
<div class="m2"><p>دیدار قدیم است برو دیده بیار</p></div></div>