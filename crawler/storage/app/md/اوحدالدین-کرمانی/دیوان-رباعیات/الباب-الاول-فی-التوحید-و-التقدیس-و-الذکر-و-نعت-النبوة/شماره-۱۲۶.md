---
title: >-
    شمارهٔ  ۱۲۶
---
# شمارهٔ  ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>استاد چو صانع آمد و چابک دست</p></div>
<div class="m2"><p>آسان باشد به نزد او بست و شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صنعت او چنانک خواهد پیوست</p></div>
<div class="m2"><p>گه هست کند زنیست و گه نیست زهست</p></div></div>