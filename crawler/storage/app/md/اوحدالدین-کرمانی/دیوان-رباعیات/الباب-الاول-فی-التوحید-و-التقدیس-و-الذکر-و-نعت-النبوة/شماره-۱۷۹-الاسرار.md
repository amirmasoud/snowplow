---
title: >-
    شمارهٔ  ۱۷۹ - الاسرار
---
# شمارهٔ  ۱۷۹ - الاسرار

<div class="b" id="bn1"><div class="m1"><p>در خود نگر ار نئی تو واقف زجهان</p></div>
<div class="m2"><p>و اندر تن خود بین همه پیدا و نهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زانک نمی رسد یقینت به گمان</p></div>
<div class="m2"><p>پس خواندن علمها چه سود و چه زیان</p></div></div>