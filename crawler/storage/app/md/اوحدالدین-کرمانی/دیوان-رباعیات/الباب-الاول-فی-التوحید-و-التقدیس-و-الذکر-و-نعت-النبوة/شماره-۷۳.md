---
title: >-
    شمارهٔ  ۷۳
---
# شمارهٔ  ۷۳

<div class="b" id="bn1"><div class="m1"><p>ای خدمت تو سعادت و پیروزی</p></div>
<div class="m2"><p>مولای تو بودن سبب بهروزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خدمت تو دست ندارم که زتو</p></div>
<div class="m2"><p>هم عمر فزون می شود و هم روزی</p></div></div>