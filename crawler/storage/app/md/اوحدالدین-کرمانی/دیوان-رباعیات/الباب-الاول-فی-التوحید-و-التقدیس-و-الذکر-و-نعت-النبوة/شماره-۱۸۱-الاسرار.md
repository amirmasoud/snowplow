---
title: >-
    شمارهٔ  ۱۸۱ - الاسرار
---
# شمارهٔ  ۱۸۱ - الاسرار

<div class="b" id="bn1"><div class="m1"><p>روزت بستودم و نمی دانستم</p></div>
<div class="m2"><p>شب با تو غنودم و نمی دانستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ظن برده بُدم به من که من من باشم</p></div>
<div class="m2"><p>من جمله تو بودم و نمی دانستم</p></div></div>