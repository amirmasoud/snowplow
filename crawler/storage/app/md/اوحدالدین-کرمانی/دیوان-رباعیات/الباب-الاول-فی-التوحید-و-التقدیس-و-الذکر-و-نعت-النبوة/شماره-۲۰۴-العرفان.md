---
title: >-
    شمارهٔ  ۲۰۴ - العرفان
---
# شمارهٔ  ۲۰۴ - العرفان

<div class="b" id="bn1"><div class="m1"><p>از بهر شناختن نکو کن خود را</p></div>
<div class="m2"><p>زیرا که سزا نکو بود نیکو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس نادره رسمی است که در راه طلب</p></div>
<div class="m2"><p>تا بی تو نباشی نشناسی او را</p></div></div>