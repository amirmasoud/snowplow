---
title: >-
    شمارهٔ  ۱۳۰ - نعت النبوة
---
# شمارهٔ  ۱۳۰ - نعت النبوة

<div class="b" id="bn1"><div class="m1"><p>صدری که چو بدر کرد عالم انور</p></div>
<div class="m2"><p>بگذشت وی از نُه فلک و هفت اختر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین عالم شش جهت برآمد برتر</p></div>
<div class="m2"><p>از فضل خدا تاج لعمرک بر سر</p></div></div>