---
title: >-
    شمارهٔ  ۸۸
---
# شمارهٔ  ۸۸

<div class="b" id="bn1"><div class="m1"><p>ای در عالم عیان تر از هر چه عیان</p></div>
<div class="m2"><p>در بی چونی نهان تر از هر چه نهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نزدیک تری به بندگان از ره جان</p></div>
<div class="m2"><p>ای دورتر از هر چه بود عقل گمان</p></div></div>