---
title: >-
    شمارهٔ  ۲۰۱ - الاسرار
---
# شمارهٔ  ۲۰۱ - الاسرار

<div class="b" id="bn1"><div class="m1"><p>چون آینه کرد صفّه ای را نقّاش</p></div>
<div class="m2"><p>تا نقش سه صفّه رو نماید زصفاش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستی تو چهار صفّهٔ چین علوم</p></div>
<div class="m2"><p>یا قابل نقش باش یا آینه باش</p></div></div>