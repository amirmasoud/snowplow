---
title: >-
    شمارهٔ  ۱۲۱
---
# شمارهٔ  ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>ای از پی دیدنت منوّر چشمم</p></div>
<div class="m2"><p>نور تو گرفته است سراسر چشمم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خاک در تو سرمه ای بخش مرا</p></div>
<div class="m2"><p>تا جز تو کسی نماند اندر چشمم</p></div></div>