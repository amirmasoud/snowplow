---
title: >-
    شمارهٔ  ۶۸
---
# شمارهٔ  ۶۸

<div class="b" id="bn1"><div class="m1"><p>ای از ره لطف راعی هر رَمه تو</p></div>
<div class="m2"><p>مقصود جهانیان زهر دمدمه تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز تو همه هر چه هست تشویش ره است</p></div>
<div class="m2"><p>ما را زهمه باز رهان ای همه تو</p></div></div>