---
title: >-
    شمارهٔ  ۲۳۰ - الخوف
---
# شمارهٔ  ۲۳۰ - الخوف

<div class="b" id="bn1"><div class="m1"><p>انعام تو هر گرسنه را می پرورد</p></div>
<div class="m2"><p>هر تشنه زجوی فضلت آبی می خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل در پی اومید تو شادی می کرد</p></div>
<div class="m2"><p>هم بخت بد از امید نومیدم کرد</p></div></div>