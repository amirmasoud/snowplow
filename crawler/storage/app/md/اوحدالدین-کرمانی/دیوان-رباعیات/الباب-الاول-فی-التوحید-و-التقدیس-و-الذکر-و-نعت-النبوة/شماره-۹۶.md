---
title: >-
    شمارهٔ  ۹۶
---
# شمارهٔ  ۹۶

<div class="b" id="bn1"><div class="m1"><p>ای آنک به دوست جان دشمن بخشی</p></div>
<div class="m2"><p>مسکینان را هزار مسکن بخشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر درگه تو پیر شدم گرچه بدم</p></div>
<div class="m2"><p>شاید که مرا به پیری من بخشی</p></div></div>