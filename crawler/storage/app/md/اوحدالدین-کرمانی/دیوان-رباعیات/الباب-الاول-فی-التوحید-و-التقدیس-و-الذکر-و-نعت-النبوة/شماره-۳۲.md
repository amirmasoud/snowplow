---
title: >-
    شمارهٔ  ۳۲
---
# شمارهٔ  ۳۲

<div class="b" id="bn1"><div class="m1"><p>خود را تو قباپوش کن و ذاکر باش</p></div>
<div class="m2"><p>وین جام بقانوش کن و ذاکر باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر می خواهی طریق اسرار خدا</p></div>
<div class="m2"><p>در سینهٔ خود گوش کن و ذاکر باش</p></div></div>