---
title: >-
    شمارهٔ  ۱۰۸
---
# شمارهٔ  ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>مقصود میان من و تو پنهان است</p></div>
<div class="m2"><p>دل را سببی هست که سرگردان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زینجا که منم حدیث بس دشوار است</p></div>
<div class="m2"><p>زآنجا که قبول تست بس آسان است</p></div></div>