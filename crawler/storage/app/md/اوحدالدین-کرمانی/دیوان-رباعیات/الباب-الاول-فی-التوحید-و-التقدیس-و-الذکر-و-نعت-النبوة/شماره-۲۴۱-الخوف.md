---
title: >-
    شمارهٔ  ۲۴۱ - الخوف
---
# شمارهٔ  ۲۴۱ - الخوف

<div class="b" id="bn1"><div class="m1"><p>بردار نظر زدیگران تا خود باش</p></div>
<div class="m2"><p>وز مکر خدا حذر کن و با خود باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور زانک نجات آخرت می طلبی</p></div>
<div class="m2"><p>تو نیک شو و جمله جهان گو بد باش</p></div></div>