---
title: >-
    شمارهٔ  ۹۱
---
# شمارهٔ  ۹۱

<div class="b" id="bn1"><div class="m1"><p>ای آنک تو را همه صفت احسان است</p></div>
<div class="m2"><p>با عفو تو طاعت و گنه یکسان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان کرده نگاهی که دلم ترسان است</p></div>
<div class="m2"><p>گر عفو کنی به نزد تو آسان است</p></div></div>