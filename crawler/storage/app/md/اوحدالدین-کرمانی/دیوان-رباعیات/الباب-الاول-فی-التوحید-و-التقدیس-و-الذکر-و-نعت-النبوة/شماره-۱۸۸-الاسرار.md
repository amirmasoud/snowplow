---
title: >-
    شمارهٔ  ۱۸۸ - الاسرار
---
# شمارهٔ  ۱۸۸ - الاسرار

<div class="b" id="bn1"><div class="m1"><p>هر دل نبود قابل اسرار خدا</p></div>
<div class="m2"><p>در هر گوشی نگنجد اسرار خدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستند عقول یکسر ار درنگری</p></div>
<div class="m2"><p>سرگشته و واله شده در کار خدا</p></div></div>