---
title: >-
    شمارهٔ  ۱۴۹ - الرّضا و التّسلیم
---
# شمارهٔ  ۱۴۹ - الرّضا و التّسلیم

<div class="b" id="bn1"><div class="m1"><p>گر کِشتهٔ ما غم آورد غم دِرَویم</p></div>
<div class="m2"><p>گر بهرهٔ ما درد بود درد خوریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کار خدا مرا تصرّف نرسد</p></div>
<div class="m2"><p>امروز درآمدیم و فردا برویم</p></div></div>