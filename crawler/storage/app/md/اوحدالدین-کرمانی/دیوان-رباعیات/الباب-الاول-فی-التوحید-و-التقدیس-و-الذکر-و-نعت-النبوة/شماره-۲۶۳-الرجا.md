---
title: >-
    شمارهٔ  ۲۶۳ - الرجاء
---
# شمارهٔ  ۲۶۳ - الرجاء

<div class="b" id="bn1"><div class="m1"><p>بنگر که چنین ما به چه تدبیر شدیم</p></div>
<div class="m2"><p>گشتیم گدای دَرِ او میر شدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما زانک به طاعتش نکردیم قیام</p></div>
<div class="m2"><p>با حلقهٔ بندگیّ او پیر شدیم</p></div></div>