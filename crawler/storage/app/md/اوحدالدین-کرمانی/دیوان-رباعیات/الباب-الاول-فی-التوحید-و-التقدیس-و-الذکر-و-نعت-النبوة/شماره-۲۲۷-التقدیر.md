---
title: >-
    شمارهٔ  ۲۲۷ - التّقدیر
---
# شمارهٔ  ۲۲۷ - التّقدیر

<div class="b" id="bn1"><div class="m1"><p>ای دل اگرت هست خرد راهنما</p></div>
<div class="m2"><p>در هر سوری که آیدت بیش نما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساکن شو و بر متپ که هرگز نشود</p></div>
<div class="m2"><p>تدبیر من و تو دفع تقدیر خدا</p></div></div>