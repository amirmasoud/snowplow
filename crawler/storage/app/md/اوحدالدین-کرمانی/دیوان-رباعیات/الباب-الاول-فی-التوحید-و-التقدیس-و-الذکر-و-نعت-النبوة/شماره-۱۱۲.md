---
title: >-
    شمارهٔ  ۱۱۲
---
# شمارهٔ  ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>از خاک ره خود آبم ارزانی دار</p></div>
<div class="m2"><p>یا رؤیت خود به خوابم ارزانی دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون هستی تست کنج دلهای خراب</p></div>
<div class="m2"><p>یا رب تو دل خرابم ارزانی دار</p></div></div>