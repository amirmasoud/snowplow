---
title: >-
    شمارهٔ  ۶۳
---
# شمارهٔ  ۶۳

<div class="b" id="bn1"><div class="m1"><p>عمری گشتم شیفته و آواره</p></div>
<div class="m2"><p>نومید شدم زخویشتن یکباره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای آنک به هیچ چاره محتاج نئی</p></div>
<div class="m2"><p>دریاب کسی را که ندارد چاره</p></div></div>