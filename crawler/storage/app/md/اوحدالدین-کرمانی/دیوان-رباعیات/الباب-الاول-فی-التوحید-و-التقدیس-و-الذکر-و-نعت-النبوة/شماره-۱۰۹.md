---
title: >-
    شمارهٔ  ۱۰۹
---
# شمارهٔ  ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>ای جان مرا امید جاوید به تو</p></div>
<div class="m2"><p>روشن دل من چو روی خورشید به تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فارغ کنم از امید و بیم دگران</p></div>
<div class="m2"><p>چون بیم دلم زتست و اومید به تو</p></div></div>