---
title: >-
    شمارهٔ  ۱۱۸
---
# شمارهٔ  ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>یا رب مگذارم اینچنین بی حاصل</p></div>
<div class="m2"><p>نه دین به سلامت و نه دنیا حاصل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنمای رهی کزو میسّر گردد</p></div>
<div class="m2"><p>بی منّت دعوی همه معنی حاصل</p></div></div>