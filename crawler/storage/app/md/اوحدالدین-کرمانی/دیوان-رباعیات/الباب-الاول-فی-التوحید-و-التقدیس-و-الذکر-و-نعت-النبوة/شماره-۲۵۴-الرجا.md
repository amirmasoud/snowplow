---
title: >-
    شمارهٔ  ۲۵۴ - الرجاء
---
# شمارهٔ  ۲۵۴ - الرجاء

<div class="b" id="bn1"><div class="m1"><p>با رنگ ز تیغ او دلم نزداید</p></div>
<div class="m2"><p>می باشم از آن گونه که او فرماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نومید نیم ز فضل او زیرا کاو</p></div>
<div class="m2"><p>هرگه که دری ببست صد بگشاید</p></div></div>