---
title: >-
    شمارهٔ  ۶۷
---
# شمارهٔ  ۶۷

<div class="b" id="bn1"><div class="m1"><p>از خلق نه کاهد نه فزاید کاری</p></div>
<div class="m2"><p>الّا به خدای برنیاید کاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای آنک گشانیدهٔ هر کار تویی</p></div>
<div class="m2"><p>تا تو نگشایی نگشاید کاری</p></div></div>