---
title: >-
    شمارهٔ  ۲۳۸ - الخوف
---
# شمارهٔ  ۲۳۸ - الخوف

<div class="b" id="bn1"><div class="m1"><p>دستی نه که از دل گرهی برگیرد</p></div>
<div class="m2"><p>پایی نه که بار گنهی برگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم که زبی دلی سرانجام دلم</p></div>
<div class="m2"><p>از سینه برون رود رهی برگیرد</p></div></div>