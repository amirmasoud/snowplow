---
title: >-
    شمارهٔ  ۲۶۵ - الرجاء
---
# شمارهٔ  ۲۶۵ - الرجاء

<div class="b" id="bn1"><div class="m1"><p>ماییم به عشق تو تولّا کرده</p></div>
<div class="m2"><p>وز طاعت و معصیت تبرّا کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن را که عنایت تو باشد باشد</p></div>
<div class="m2"><p>ناکرده چو کرده، کرده چون ناکرده</p></div></div>