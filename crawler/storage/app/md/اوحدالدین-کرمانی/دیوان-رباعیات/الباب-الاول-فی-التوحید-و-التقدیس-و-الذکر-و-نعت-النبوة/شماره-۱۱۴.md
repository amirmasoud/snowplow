---
title: >-
    شمارهٔ  ۱۱۴
---
# شمارهٔ  ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>ای از همه بی نیاز و ای بنده نواز</p></div>
<div class="m2"><p>و از تست که کار من نمی گیرد ساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد مشغله در راه من انداخته ای</p></div>
<div class="m2"><p>آنگه گویی تمام با من پرداز</p></div></div>