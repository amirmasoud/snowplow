---
title: >-
    شمارهٔ  ۲۳۹ - الخوف
---
# شمارهٔ  ۲۳۹ - الخوف

<div class="b" id="bn1"><div class="m1"><p>تا رهبر تو طبع بدآموز بود</p></div>
<div class="m2"><p>بخت تو مپندار که پیروز بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو خفته به عیش و شب عمرت کوتاه</p></div>
<div class="m2"><p>ترسم که چو بیدار شوی روز بود</p></div></div>