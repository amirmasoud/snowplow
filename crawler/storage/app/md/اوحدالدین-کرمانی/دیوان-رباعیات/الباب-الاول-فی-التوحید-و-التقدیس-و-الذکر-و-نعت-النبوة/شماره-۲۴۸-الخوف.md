---
title: >-
    شمارهٔ  ۲۴۸ - الخوف
---
# شمارهٔ  ۲۴۸ - الخوف

<div class="b" id="bn1"><div class="m1"><p>رخسار به خون دیده باید شستن</p></div>
<div class="m2"><p>کانجا گل بی خار نخواهد رستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با نیک و بد زمانه می باید ساخت</p></div>
<div class="m2"><p>تا خود به چه زاید این شب آبستن</p></div></div>