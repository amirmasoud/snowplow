---
title: >-
    شمارهٔ  ۱۶۷ - الرّضا و التّسلیم
---
# شمارهٔ  ۱۶۷ - الرّضا و التّسلیم

<div class="b" id="bn1"><div class="m1"><p>من خاک تو در چشم خرد می آرم</p></div>
<div class="m2"><p>عذرت نه یکی نه ده که صد می آرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر خواسته ای به دست کس نتوان داد</p></div>
<div class="m2"><p>می آیم و بر گردن خود می آرم</p></div></div>