---
title: >-
    شمارهٔ  ۱۹۸ - الاسرار
---
# شمارهٔ  ۱۹۸ - الاسرار

<div class="b" id="bn1"><div class="m1"><p>هر دل که خبردار شد از اسرارش</p></div>
<div class="m2"><p>گر نور بود سوخته شد در نارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر شبه سلامتی کسی را بینی</p></div>
<div class="m2"><p>زان است که او بی خبر است از کارش</p></div></div>