---
title: >-
    شمارهٔ  ۱۹۴ - الاسرار
---
# شمارهٔ  ۱۹۴ - الاسرار

<div class="b" id="bn1"><div class="m1"><p>عقل از ره تو حدیث افسانه برد</p></div>
<div class="m2"><p>در کوی تو ره مردم بیگانه برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر لحظه چو من هزار دل سوخته را</p></div>
<div class="m2"><p>سودای تو از کعبه به بتخانه برد</p></div></div>