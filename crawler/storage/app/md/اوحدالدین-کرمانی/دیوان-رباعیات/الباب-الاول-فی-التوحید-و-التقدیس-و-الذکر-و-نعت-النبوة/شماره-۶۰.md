---
title: >-
    شمارهٔ  ۶۰
---
# شمارهٔ  ۶۰

<div class="b" id="bn1"><div class="m1"><p>در سایهٔ رحمت تو خورشید شویم</p></div>
<div class="m2"><p>وز لطف تو نیکبخت جاوید شویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز لطف تو اومید نداریم دگر</p></div>
<div class="m2"><p>مپسند که از لطف تو نومید شویم</p></div></div>