---
title: >-
    شمارهٔ  ۲۶۴ - الرجاء
---
# شمارهٔ  ۲۶۴ - الرجاء

<div class="b" id="bn1"><div class="m1"><p>دعوی طلب گرچه مجاز است از تو</p></div>
<div class="m2"><p>سرنامهٔ دهر بی نماز است از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر معصیتت هزار چندان باشد</p></div>
<div class="m2"><p>نومید مشو چو نی نیاز است از تو</p></div></div>