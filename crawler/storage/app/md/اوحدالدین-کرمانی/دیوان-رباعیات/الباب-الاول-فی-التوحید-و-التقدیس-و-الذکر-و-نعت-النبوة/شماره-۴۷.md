---
title: >-
    شمارهٔ  ۴۷
---
# شمارهٔ  ۴۷

<div class="b" id="bn1"><div class="m1"><p>یا رب تو مرا زخواب بیداری ده</p></div>
<div class="m2"><p>وز مستی غفلتم تو هشیاری ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریافتن آنچ مرا به بودَه است</p></div>
<div class="m2"><p>من عاجزم ای خدا توَم یاری ده</p></div></div>