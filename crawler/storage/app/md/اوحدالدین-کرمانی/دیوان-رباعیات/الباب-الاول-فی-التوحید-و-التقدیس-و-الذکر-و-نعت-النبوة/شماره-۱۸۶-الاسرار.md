---
title: >-
    شمارهٔ  ۱۸۶ - الاسرار
---
# شمارهٔ  ۱۸۶ - الاسرار

<div class="b" id="bn1"><div class="m1"><p>شبهای دراز با غمت می سازم</p></div>
<div class="m2"><p>پوشیده چنانک کس نداند رازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میدان بلا و من درو می تازم</p></div>
<div class="m2"><p>دل رفته و می روم نه جان در بازم</p></div></div>