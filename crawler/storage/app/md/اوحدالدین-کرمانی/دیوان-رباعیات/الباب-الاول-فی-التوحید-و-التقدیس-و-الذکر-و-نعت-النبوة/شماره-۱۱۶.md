---
title: >-
    شمارهٔ  ۱۱۶
---
# شمارهٔ  ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>ای آنک تو را به هیچ کس نیست نیاز</p></div>
<div class="m2"><p>کوتاه کن این قصّه که شد کار دراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما درخور عجز خویشتن می نالیم</p></div>
<div class="m2"><p>تو درخور لطف خویشتن چاره بساز</p></div></div>