---
title: >-
    شمارهٔ  ۷۹
---
# شمارهٔ  ۷۹

<div class="b" id="bn1"><div class="m1"><p>بیچاره دل شکسته چون بستهٔ تست</p></div>
<div class="m2"><p>بی مرهم وصل هر زمان خستهٔ تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون می گسلی از آنک پیوستهٔ تست</p></div>
<div class="m2"><p>گر بستهٔ تست دل نه بشکستهٔ تست</p></div></div>