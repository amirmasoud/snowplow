---
title: >-
    شمارهٔ  ۲۲
---
# شمارهٔ  ۲۲

<div class="b" id="bn1"><div class="m1"><p>من گرچه سزای راه درگاه نیم</p></div>
<div class="m2"><p>جز بر در سایهٔ هوالله نیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون من توام و تو من، توام راه نمای</p></div>
<div class="m2"><p>تو آگهی از من و من آگاه نیم</p></div></div>