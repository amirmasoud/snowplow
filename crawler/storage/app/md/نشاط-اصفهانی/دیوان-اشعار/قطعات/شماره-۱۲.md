---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>در وصلم و به هجر برم رشک غیر را</p></div>
<div class="m2"><p>از نامه‌ای چو می‌شنوم یاد می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز اضطراب دگر داشت مرغ دل</p></div>
<div class="m2"><p>صیادش از قفس مگر آزاد می‌کند</p></div></div>