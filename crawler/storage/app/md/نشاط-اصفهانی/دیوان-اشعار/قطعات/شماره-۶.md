---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>گفتم چو دیده دید چسان منع دل کنم</p></div>
<div class="m2"><p>گفتا که منع دیده ز دیدار بایدت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرم رسید و حال تو دید و عنان کشید</p></div>
<div class="m2"><p>آخر نشاط جرأت گفتار بایدت</p></div></div>