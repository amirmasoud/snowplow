---
title: >-
    شمارهٔ ۱ - خون تو در گردن ماست
---
# شمارهٔ ۱ - خون تو در گردن ماست

<div class="b" id="bn1"><div class="m1"><p>دوش میگفت کسی گفت فلان خواجه مرا</p></div>
<div class="m2"><p>که فلان از پی جاه و خطر و مسکن ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم ار باز ببینیش بگو کای خواجه</p></div>
<div class="m2"><p>مال و جاهت چه بود خون تو در گردن ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواجه هشدار و میندیش و میاسا که فلان</p></div>
<div class="m2"><p>با چنین بی زر و سیمی چه غم از دشمن ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زر و سیمی که بدان جیب و دل آراسته ای</p></div>
<div class="m2"><p>مشت گردیست که بر خواسته از دامن ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرمنی چند گر از زرع ضعیفان داری</p></div>
<div class="m2"><p>حاصل هر دو جهان خوشه ای از خرمن ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جامه و فرش نوت قدر بیفزود ولی</p></div>
<div class="m2"><p>اطلس عرش برین کهنه لباس تن ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه دگر بر فرس و استر خود رشک بری</p></div>
<div class="m2"><p>کاشهب چرخ روان بر اثر توسن ماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راست تر خواهی ازین خواجه مرا با تو چه کار</p></div>
<div class="m2"><p>آنچه در وهم تو گلزار تو شد گلخن ماست</p></div></div>