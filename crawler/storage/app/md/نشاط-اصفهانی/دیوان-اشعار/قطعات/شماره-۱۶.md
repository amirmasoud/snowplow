---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>از بیم او نگه نکنی سوی من خوش آن</p></div>
<div class="m2"><p>کز شرم من نگاه نکردی بسوی غیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیند بغیر یارو بمن غیر و من ببرم</p></div>
<div class="m2"><p>چشمی بروی یارم و چشمی بسوی غیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یارب چه ظلم بود که گلشن تهی نگشت</p></div>
<div class="m2"><p>از بانک زاغ و بزم تو از گفتگوی غیر</p></div></div>