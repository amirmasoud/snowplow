---
title: >-
    شمارهٔ ۲۳ - برخاستی و کناره کردی
---
# شمارهٔ ۲۳ - برخاستی و کناره کردی

<div class="b" id="bn1"><div class="m1"><p>پای دل خسته بستی آنگاه</p></div>
<div class="m2"><p>سر رشته ی عهد پاره کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دل چو نشستی از کنارم</p></div>
<div class="m2"><p>بر خاستی و کناره کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم شاد شد از تو غیر و هم من</p></div>
<div class="m2"><p>مکتوب مرا چو پاره کردی</p></div></div>