---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>بر لاله ژاله میچکد از ابر مشکفام</p></div>
<div class="m2"><p>خوشتر ز ژاله باده و بهتر زلاله جام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح است و بزم عید و می و مطرب و نبید</p></div>
<div class="m2"><p>دولت مدید و بخت سعید و جهان بکام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلزار را طراوت و ایام را نشاط</p></div>
<div class="m2"><p>افلاک را سعادت و آفاق را نظام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در زلف روی ساقی و در شیشه عکس می</p></div>
<div class="m2"><p>کالبدر فی الدجیه و الشمس فی الغمام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باشد حلال تو به نباشد دگر ز می</p></div>
<div class="m2"><p>باشد حرام باده نباشد اگر بجام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باید فروخت سبحه اگر کس خرد به هیچ</p></div>
<div class="m2"><p>باید خرید باده اگر کس دهد بوام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از طرف جوی میگذرد یار سرو قد</p></div>
<div class="m2"><p>یا داده اعتدال هوا سرو را خرام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از فیض باد و لطف هوا جاودان زید</p></div>
<div class="m2"><p>نقشی اگر بر آب نگارند در منام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جذب صبا بگوش رساند صدای آن</p></div>
<div class="m2"><p>بگذارد ار پری بچمن در خیال گام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اجزای بوستان نه چنان التیام یافت</p></div>
<div class="m2"><p>کاجسام را بو هم توان داد انقسام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گلزار را بر گویی معشوق و عاشقند</p></div>
<div class="m2"><p>کاین تا بگرید آن دگر آید در ابتسام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دوشیزگان باغ مگر آگهند ازین</p></div>
<div class="m2"><p>کامروز شاه را شده در گلستان مقام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کار است باد گلشن و گسترد سبزه فرش</p></div>
<div class="m2"><p>آورد ژاله باده و پر کرد لاله جام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برخاست سرو و بید فرو برد سر بزیر</p></div>
<div class="m2"><p>بگشود دیده نرگس و بر بست غنچه کام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تعظیم پیشگاه حضور شهنشه است</p></div>
<div class="m2"><p>شمشاد را که گاه رکوع است و گه قیام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن بوستان مکرمت آن آسمان جود</p></div>
<div class="m2"><p>خورشید سایه خسرو جمشید احتشام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خاقان دهر فتحعلی شاه کز ازل</p></div>
<div class="m2"><p>جودش رهین کف شد و فتحش قرین نام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای از پی وجود تو اجسام را نظام</p></div>
<div class="m2"><p>اجرام در سجود وجود تو صبح و شام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آفاق را زپاس تو گیرند احتساب</p></div>
<div class="m2"><p>ارزاق را زجود تو یابند خاص و عام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سود از تو برد عالم و گنج تو بی زبان</p></div>
<div class="m2"><p>آفاق شد مسخر و تیغ تو در نیام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از حضرت تو رفته بهند وی چرخ پیک</p></div>
<div class="m2"><p>وز سطوت تو داده بترک فلک پیام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از عدل و فضل و رأفت و سطوت سرشته اند</p></div>
<div class="m2"><p>ارکان دولتت که مصون باد از انهدام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ملکت مزاج دید ز اضداد معتدل</p></div>
<div class="m2"><p>نبود عجب پذیرد اگر تا ابد قوام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آری در اعتدال حقیقی وجود نیست</p></div>
<div class="m2"><p>ورهست ایمن است ز آسیب انعدام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هنگام احتیاج توان دید دست تو</p></div>
<div class="m2"><p>کز جرم آفتاب توان دید در ظلام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ابر کفت بریزش و آنگه بقای آز</p></div>
<div class="m2"><p>خورشید پرتو افکن و آنگه لقای شام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آسوده است خصم تو از خصمی سپهر</p></div>
<div class="m2"><p>صید زبون نبیند هرگز زیان ز دام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر رتبت تو دست که یابد بپای سعی</p></div>
<div class="m2"><p>آری بر آسمان نتوان شد باهتمام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مستی نیاورد دگر آب رزان اگر</p></div>
<div class="m2"><p>افتد ز عکس رای تو یک لعمه برغمام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر چار چیز باد تو را وقف چارچیز</p></div>
<div class="m2"><p>تا وقف راست شرط که دارند مستدام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر ذات تو ستایش و برجود تو سپاس</p></div>
<div class="m2"><p>بر گنج تو فزایش و بر ملک تو دوام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شوق تو در روانم و ذوق تو در وجود</p></div>
<div class="m2"><p>نام تو بر زبانم و مدح تو در کلام</p></div></div>