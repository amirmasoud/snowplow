---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>ای مایه روح و راحت جان</p></div>
<div class="m2"><p>ای خاک تو به ز آب حیوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ساحت دلگشای اشرف</p></div>
<div class="m2"><p>ای روضه ی جانفزای رضوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای غنچه گلبنت سخن سنج</p></div>
<div class="m2"><p>ای سوسن گلشنت زبان دان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بادت چه عجب اگر برد دل</p></div>
<div class="m2"><p>آبت چه عجب اگر دهد جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاکت چه عجب اگر شود لعل</p></div>
<div class="m2"><p>از مقدم آفتاب تابان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای باد صبا نخست بگذر</p></div>
<div class="m2"><p>بر ساحت باغ و طرف بستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از لاله بکف پیاله بر گیر</p></div>
<div class="m2"><p>وز ژاله عرق برخ بیفشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از جعد بنفشه عطر بردار</p></div>
<div class="m2"><p>وز عارض گل گلاب بستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منشین که رسید موکب شاه</p></div>
<div class="m2"><p>بر خیز و غبار راه بنشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وانگاه بسوی شهر بگذر</p></div>
<div class="m2"><p>بر عارض دلفریب خوبان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلها برهان زجعد گیسو</p></div>
<div class="m2"><p>جانها بستان ز نوک مژگان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در مقدم اشرفش بیفکن</p></div>
<div class="m2"><p>بر پای مبارکش بیفشان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاقان معظم مکرم</p></div>
<div class="m2"><p>دارای جهان خدیو دوران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن معنی لفظ آفرینش</p></div>
<div class="m2"><p>آن حاصل کارگاه امکان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا شادی جسم باشد از روح</p></div>
<div class="m2"><p>تا شادی روح باشد از جان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو شاد نشین و شاد عالم</p></div>
<div class="m2"><p>در ظل تو ای تو ظل یزدان</p></div></div>