---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>باد نوروزی مگر از کوی جانان می‌رسد</p></div>
<div class="m2"><p>کز شمیمش بر تن افسردگان جان می‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز فراش صبا در مقدم سلطان گل</p></div>
<div class="m2"><p>از پی آرایش بستان شتابان می‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سبزه تا آرد خبر از گل به بلبل در چمن</p></div>
<div class="m2"><p>چون شتابان پیکی از شبنم خَوی‌افشان می‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رشک گردون شد چمن از گل کنون بر چرخ پیر</p></div>
<div class="m2"><p>سدهزاران طعنه از اطفال بستان می‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که باد افشانده بر وی لاله‌های آتشین</p></div>
<div class="m2"><p>آب جو را طعنه بر خاک بدخشان می‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گلشن از گل طبعم از معنی‌ست گنج شایگان</p></div>
<div class="m2"><p>درج نظمم را قوافی شایگان زان می‌رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در گلستان یارب این آشفتگی از عشق کیست</p></div>
<div class="m2"><p>گل گریبان می‌درد سنبل پریشان می‌رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق را دست تصرف بین که در ملک وجود</p></div>
<div class="m2"><p>حکم او هم بر نبات و هم به حیوان می‌رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سروها را مانده چون من پا به گل یارب که گفت</p></div>
<div class="m2"><p>در چمن آن سرو قد اینک خرامان می‌رسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم نرگس شد سفید از انتظار مقدمی</p></div>
<div class="m2"><p>گویی آگاه است کو با چشم فتّان می‌رسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گل به بلبل مهربان آمد همانا آن نگار</p></div>
<div class="m2"><p>با رخی رشک گل اکنون در گلستان می‌رسد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس کن ای بلبل فغان کاینک بپوشد گل نقاب</p></div>
<div class="m2"><p>ای دل افغان کن که باز آن آفت جان می‌رسد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او به فکر این که افزاید به دردم دردها</p></div>
<div class="m2"><p>من به این خوش کرده‌ام خاطر که درمان می‌رسد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آمد و در گلستان دیدم ز خط عارضش</p></div>
<div class="m2"><p>گلستانی دیگر از نسرین و ریحان می‌رسد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتم ای زیب گلستان بر گل و بلبل ببین</p></div>
<div class="m2"><p>تا چه سان دلبر به درد دردمندان می‌رسد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت حاشا درد را درمان کجا باشد که گفت</p></div>
<div class="m2"><p>کار عاشق هرگز از جانان به سامان می‌رسد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زخم کز یار است آساید هم از زخم دگر</p></div>
<div class="m2"><p>درد کز عشق است افزاید چون درمان می‌رسد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتم اینک روز نوروز و جلوس شهریار</p></div>
<div class="m2"><p>گر رسد سد قرن کی روزی بدین سان می‌رسد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روز نوروز است امروز ار چه هر روز نوی</p></div>
<div class="m2"><p>در جهان کهنه از بخت جهانبان می‌رسد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صبح عید و هر کسی را بهره از انعام شاه</p></div>
<div class="m2"><p>جز مرا کز تو نصیبم جمله حرمان می‌رسد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>افتخار خسروان فتحعلی‌شه آنکه او</p></div>
<div class="m2"><p>آستانش را شرف بر اوج کیوان می‌رسد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از حسب تا بنگری برتر ز برتر می‌رود</p></div>
<div class="m2"><p>وز نسب تا بشمری سلطان به سلطان می‌رسد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>منتش بر چرخ ازو چندان که خدمت می‌برد</p></div>
<div class="m2"><p>خدمتش بر دهر ازو چندان که فرمان می‌رسد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا پدید آمد وجودش ز امتزاج چار طبع</p></div>
<div class="m2"><p>فخرها بر هفت چرخ از چار ارکان می‌رسد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر خلاف عهد دوران شکر کاندر عهد او</p></div>
<div class="m2"><p>فخرها امروز دانا را به نادان می‌رسد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>روز هیجا کز خروش نای و غوغای درای</p></div>
<div class="m2"><p>منکران را بر ثبوت حشر برهان می‌رسد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از غبار توسنان و ز لمعهٔ تیغ و سنان</p></div>
<div class="m2"><p>روز چون شب شب چو روز این هر دو یکسان می‌رسد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باطل آمد لا ملا نزد حکیم از بس همی</p></div>
<div class="m2"><p>بر فراز سطح گردون گرد میدان می‌رسد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>باز ماند از تحرک رمح‌ها را نوک و بن</p></div>
<div class="m2"><p>از دو جانب بس که بر گردون گردان می‌رسد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تیر از آن سان در شتاب آمد که گویی عاشقی</p></div>
<div class="m2"><p>بر وصال یار خود اینک ز هجران می‌رسد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تیغ اگر معشوق آمد از چه خون گرید چو ابر</p></div>
<div class="m2"><p>ور بود عاشق چرا چون برق خندان می‌رسد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تیره‌بختان را بپوشاند لباس نیستی</p></div>
<div class="m2"><p>گرچه خود با پیکری رخشان و عریان می‌رسد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون بر آید بر سمند دیو شکل بادپای</p></div>
<div class="m2"><p>هدهد نصرت همی‌گوید سلیمان می‌رسد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آسمانی بر زمین پیدا ازو گاه خرام</p></div>
<div class="m2"><p>از زمین بر آسمان ناگه به جولان می‌رسد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر برانگیزدش یک ره از حدود امتناع</p></div>
<div class="m2"><p>تا به سر حد وجوب ار خواهد آسان می‌رسد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رزم او سیارگان دیدند گفتند الحذر</p></div>
<div class="m2"><p>ز آتش خشمش کنون آفت به دروان می‌رسد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مشتری ترسان همی نا پیش کیوان شد دوان</p></div>
<div class="m2"><p>ماه را با زهره دید از ره هراسان می‌رسد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گفت کیوان چون شد آن ترک جفاجو زهره گفت</p></div>
<div class="m2"><p>مانده از سستی به ره افتادن و خیزان می‌رسد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گفت با مه هیچ دانی تا چرا ماندست مهر</p></div>
<div class="m2"><p>گفت آن را نسبتی بارای سلطان می‌رسد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مشتری گفتا همانا تیر ماندستی به جای</p></div>
<div class="m2"><p>کز دبیران خدمتی او را به دیوان می‌رسد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هم ثنایش واجب و هم ممتنع شد چون کنم</p></div>
<div class="m2"><p>زانکه در ذاتش سخن برتر ز امکان می‌رسد</p></div></div>