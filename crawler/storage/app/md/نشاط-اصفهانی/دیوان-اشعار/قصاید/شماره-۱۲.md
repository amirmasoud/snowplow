---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>زیباترین اشیا فرخ‌ترین اعیان</p></div>
<div class="m2"><p>از هرچه هست پیدا وز هرچه هست پنهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مرغ‌ها هزار است از وقت‌ها سحرگه</p></div>
<div class="m2"><p>از فصل‌ها بهار است از نوع‌هاست انسان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عهدها شباب است از آب‌ها شراب است</p></div>
<div class="m2"><p>از انجم آفتاب است از ماه‌هاست نیسان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سنگ‌ها دل دوست از عیش‌ها غم اوست</p></div>
<div class="m2"><p>از تیغ‌هاست ابرو از دشنه‌هاست مژگان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از زیب‌هاست افسر از طیب‌هاست عنبر</p></div>
<div class="m2"><p>از عضوهاست دیده از خلق‌هاست احسان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از اولیاست حیدر از حوض‌هاست کوثر</p></div>
<div class="m2"><p>از شاخ‌هاست طوبا از باغ‌هاست رضوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از انبیا محمد از شهرها مدینه</p></div>
<div class="m2"><p>از خسروان شهنشه از ملک‌هاست ایران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بحرهاست آن دل از ابرهاست آن کف</p></div>
<div class="m2"><p>از روح‌هاست آن تن از عقل‌هاست آن جان</p></div></div>