---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>سوی تهران خویش را از اصفهان آورده‌ام</p></div>
<div class="m2"><p>یا که از گلخن مکان در گلستان آورده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا که از دارالحوادث بار رحلت بسته‌ام</p></div>
<div class="m2"><p>رخت هستی جانب دارالامان آورده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا که گویی از بلای زاهدان جان برده‌ام</p></div>
<div class="m2"><p>نیم‌جانی بر در پیر مغان آورده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راست گویم داشتم یک چند در دوزخ مقام</p></div>
<div class="m2"><p>وین زمان جا در بهشت جاودان آورده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جنت از قهر شه ار دوزخ شود نبود عجب</p></div>
<div class="m2"><p>نه به تهمت این مثل بر اسفهان آورده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قهر شاه است آنچه او را نام دوزخ کرده‌ام</p></div>
<div class="m2"><p>لطف شاه است آنچه نام او را جنان آورده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه گردون مرتبت فتحعلی‌شه آنکه من</p></div>
<div class="m2"><p>از نخستین تا زبان اندر دهان آورده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست جز حرف مدیحش بر زبانم گوییا</p></div>
<div class="m2"><p>مدح او آموخته آنگه زبان آورده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوش دیدم چرخ را می‌گفت با سیارگان</p></div>
<div class="m2"><p>خویش را در سایهٔ آن آستان آورده‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت کیوان قدر من بالاتر آمد زآن که من</p></div>
<div class="m2"><p>روز و شب خود را بر آن در پاسبان آورده‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشتری گفتا سعادت آنچه اندر قرن‌هاست</p></div>
<div class="m2"><p>دوستانش را قرین در یک قران آورده‌ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت مریخ از کمال آسمان تیر بلا</p></div>
<div class="m2"><p>هرچه آید دشمنانش را نشان آورده‌ام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مهر گفتا روزها در سایهٔ رایش شدم</p></div>
<div class="m2"><p>این همه نور و ضیا از فیض آن آورده‌ام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زهره گفتا بودم اندر بزمش از خنیاگران</p></div>
<div class="m2"><p>چند روزی بخت بد در آسمان آورده‌ام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت مه گویم چرا گاهی هلالم گاه بدر</p></div>
<div class="m2"><p>خلق را تا چند ازین ره در گمان آورده‌ام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا به بزم ار کنم گه ساغری گاهی دفی</p></div>
<div class="m2"><p>خویش را گاهی چنین گاهی چنان آورده‌ام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با عطارد گفتم از کلکش نداری شرم گفت</p></div>
<div class="m2"><p>پس چرا مهر خموشی بر زبان آورده‌ام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت عنصر با فلک اَلفَخرُ لی لا لَک که من</p></div>
<div class="m2"><p>زامتزاجی این چنین صاحب قران آورده‌ام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ناگه از فوج ملک بانگی بر آمد کای گروه</p></div>
<div class="m2"><p>تا به کی گویید این آورده آن آورده‌ام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفت حق کاو را برای مظهر اسماء خویش</p></div>
<div class="m2"><p>از فراز لامکان سوی مکان آورده‌ام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شهریارا زیبدت گویی اگر از عزم خویش</p></div>
<div class="m2"><p>ترجمان سر لوح کن فکان آورده‌ام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بار گاهت را سزد الحق که گوید گاه بار</p></div>
<div class="m2"><p>بر زمین از خویش پیدا آسمان آورده‌ام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا بسوزم زآتش رشک آفتاب چرخ را</p></div>
<div class="m2"><p>آفتاب طلعت شاه جهان آورده‌ام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چرخ بهر حل و عقد آورد اگر سیارگان</p></div>
<div class="m2"><p>من دبیران شه گیتی ستان آورده‌ام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آسمان را هر طرف خیلی اگر از انجم است</p></div>
<div class="m2"><p>من سپاه بیکران از هر کران آورده‌ام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از هجوم سرکشان ز آمدشد گردن‌کشان</p></div>
<div class="m2"><p>راه این درگاه را چون کهکشان آورده‌ام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خسروا عمری به سر سودای این در داشتم</p></div>
<div class="m2"><p>تا نگوید کس کز این سودا زیان آورده‌ام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بندگان را قابل خدمت نبودم خویش را</p></div>
<div class="m2"><p>با هزار امید در سلک سگان آورده‌ام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کی بود یا رب فرستم مژده سوی اصفهان</p></div>
<div class="m2"><p>کز عنایات شه این آورده آن آورده‌ام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خستگان را مرهم از داروی لطفش کرده‌ام</p></div>
<div class="m2"><p>مجرمان را از خط عفوش امان آورده‌ام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ابر آزاری‌ست عفو شه گلستان اصفهان</p></div>
<div class="m2"><p>ابر آزاری به طرف گلستان آورده‌ام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>لطف شه خورشید تابان اصفهان کان گهر</p></div>
<div class="m2"><p>تابش خورشید تابان سوی کان آورده‌ام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر مسیح از باد و خضر از آب بخشیدی حیات</p></div>
<div class="m2"><p>من ز خاک پای شه بر مرده جان آورده‌ام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جرم‌های بی‌نهایت عفوهای بی‌شمار</p></div>
<div class="m2"><p>بر در شاه جهان این برده آن آورده‌ام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کامکارا آسمان با بخت تو گوید مرنج</p></div>
<div class="m2"><p>یک دو روز از دشمنت را کامران آورده‌ام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آفتاب دولتت اول فروزان کرده‌ام</p></div>
<div class="m2"><p>پس چو شمع صبحگاهش در میان آورده‌ام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا به دوران تو هرکس باز داند قدر خویش</p></div>
<div class="m2"><p>این شگفتی‌ها برای امتحان آورده‌ام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دولتت را با ابد پیوند الفت داده‌ام</p></div>
<div class="m2"><p>مدتت را با نهایت سرگران آورده‌ام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر زمان بادا خطابت از قضا کای شهریار</p></div>
<div class="m2"><p>بلعجب نقشی به دورانت عیان آورده‌ام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دوستت را گرچه در می زعفران افکنده‌ام</p></div>
<div class="m2"><p>عارضش را همچو شاخ ارغوان آورده‌ام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دشمنت را گرچه هردم خون به ساغر کرده‌ام</p></div>
<div class="m2"><p>چهره‌اش را همچو برگ زعفران آورده‌ام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>باشد ار انصاف کس عیبم نگوید زین که من</p></div>
<div class="m2"><p>هم مکرر قافیه هم شایگان آورده‌ام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هست این نظمی که گوید انوری از افتخار</p></div>
<div class="m2"><p>این قصیده از برای امتحان آورده‌ام</p></div></div>