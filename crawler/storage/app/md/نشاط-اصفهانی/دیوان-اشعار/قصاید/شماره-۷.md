---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>طلع الصبح و فاضت الانوار</p></div>
<div class="m2"><p>یکی از خفتگان نشد بیدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پند گیرید چند ازین غفلت</p></div>
<div class="m2"><p>شرم دارید تا کی این پندار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بس آزادگان سرو خرام</p></div>
<div class="m2"><p>پای خجلت بگل درین گلزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بسا زیرکان پرمایه</p></div>
<div class="m2"><p>دست حسرت بسر درین بازار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می ندانید یا ذوی الالباب</p></div>
<div class="m2"><p>می نبینید یا اولی الابصار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مانده از رهروان درین وادی</p></div>
<div class="m2"><p>ز اشک خونین و آه آتشبار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شعله های نهفته در دل سنگ</p></div>
<div class="m2"><p>غنچه های شکفته بر سر خار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد کمال آیت زوال ای دل</p></div>
<div class="m2"><p>عسعس اللیل کادت الاسحار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا درنگت بود شتابی کن</p></div>
<div class="m2"><p>تا توانی برفت ره بسپار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا که نشکسته شیشه، سنگ مجوی</p></div>
<div class="m2"><p>تا نیفتاده پرده شرم بدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا توانی گسست عهد ببند</p></div>
<div class="m2"><p>تا توانی شکست تو به بیار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاکساری کزین نه سنگدلی</p></div>
<div class="m2"><p>کاید از خاک گل ز سنگ شرار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کوش تا نقد دل بدست آری</p></div>
<div class="m2"><p>که بجز دل نمی ستاند یار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنکه سرمایه ی دو کونش بود</p></div>
<div class="m2"><p>غیر حسرت نبرد ازین بازار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جیب جان چاک شد ز دست هوس</p></div>
<div class="m2"><p>آخر ای عشق سر زجیب بر آر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آخر ای کشت دل گیاه بروی</p></div>
<div class="m2"><p>آخر ای ابر دیده قطره ببار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آخر ای نفس یک نفس بشکیب</p></div>
<div class="m2"><p>آخر ای عقل یک قدم بگذار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مانده ای از قفا صدایی زن</p></div>
<div class="m2"><p>گمرهی گوش بر درایی دار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سست منشین مگر توانی جست</p></div>
<div class="m2"><p>رهبری چست و مرکبی رهوار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرکبت نیست غیر فضل یکی</p></div>
<div class="m2"><p>رهبرت چیست مهر هشت و چهار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چند بر پرده نقش می فکنی</p></div>
<div class="m2"><p>دع الا و ثان واکشف الاستار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پرده بردار تا عیان نگری</p></div>
<div class="m2"><p>لیس فی الدار غیره دیار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شهر ها بینی اندر آن یکسان</p></div>
<div class="m2"><p>مسجد و دیر و سبحه و زنار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بزمها بینی اندر آن یکرنگ</p></div>
<div class="m2"><p>عاشق و یار و بیدل و دلدار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زخمه زن مطربان بیک آهنگ</p></div>
<div class="m2"><p>هم نوا چنگ و بربط و مزمار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بی لب و گوش گرم گفت و شنید</p></div>
<div class="m2"><p>مست بی باده بی خرد هشیار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ذکر آموز ذاکران طیور</p></div>
<div class="m2"><p>راقدا بالعشی و الابکار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این ز خاموشیش بلب تسبیح</p></div>
<div class="m2"><p>آن فراموشیش بدل اذکار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تاجداران کشور معنی</p></div>
<div class="m2"><p>شهریاران عالم اسرار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ره بری گر بسویشان نگری</p></div>
<div class="m2"><p>کبریائی بری ز استکبار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ملکها بینی اندر آن ملکان</p></div>
<div class="m2"><p>رانده بیگاه و گه زخود سد بار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تخت خاقان چو گردی از پایش</p></div>
<div class="m2"><p>تاج قیصر چو تابی از دستار</p></div></div>