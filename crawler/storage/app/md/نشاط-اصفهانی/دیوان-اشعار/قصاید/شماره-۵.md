---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>بزم غیب از شمع ذاتش چون منور داشتند</p></div>
<div class="m2"><p>پرده داران صفاتش پرده بر در داشتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواست بر نامحرمان پیدا شود حسن ازل</p></div>
<div class="m2"><p>محرمانش صد ره از اول نهان تر داشتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهدان غیب را دادند اطوار ظهور</p></div>
<div class="m2"><p>رویشان بس در ظهور خویش مضمر داشتند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خامه ی اظهار چون بر لوح امکان نقش بست</p></div>
<div class="m2"><p>از نخستین صورت نوری مصور داشتند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه خواندندش محمد گاه گفتندش علی</p></div>
<div class="m2"><p>گه بعقل اولین او را معبر داشتند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفس کل کز سایه اش طبع هیولا پایه یافت</p></div>
<div class="m2"><p>مقتبس از نور آن فرخنده جوهر داشتند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وندر آن نور آنچه از نقصان و پستی یافتند</p></div>
<div class="m2"><p>عرش نامیدند و زان کرسی فروتر داشتند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وز کف و دود هیولا از پس بگداختن</p></div>
<div class="m2"><p>چرخ اخضر بر فراز ارض اغبر داشتند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با زلال عشق پس آن جمله را آمیختند</p></div>
<div class="m2"><p>وانگه از وی طبنت آدم مخمر داشتند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بوالبشر را بر بشر گر برتری دادند لیک</p></div>
<div class="m2"><p>پایه ی خیرالبشر برتر ز برتر داشتند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ذات او واجب نشاید گفت و ممکن هم از آنک</p></div>
<div class="m2"><p>از وجوبش کمتر از امکان فزونتر داشتند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گه دم عیسی ز فیضش روح پرور یافتند</p></div>
<div class="m2"><p>گاه دست موسی از نورش منور داشتند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جودی از بحر سخایش شامل آمد نوح را</p></div>
<div class="m2"><p>کشتیش را کوه جودی جای لنگر داشتند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قهر مهر آمیز او را مظهری جستند باز</p></div>
<div class="m2"><p>آذر نمرود از ابراهیم آذر داشتند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر جمالش پرده بستند از جمال یوسفی</p></div>
<div class="m2"><p>پرده ی عصمت زلیخا را ز رخ برداشتند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وز جلال او چو مرآت وجودش عکس یافت</p></div>
<div class="m2"><p>تخت دارا عرضه بر تخت سکندر داشتند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز اختلاف روزن اندر تابش یک آفتاب</p></div>
<div class="m2"><p>سایه را از هر طرف بر شکل دیگر داشتند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عاشق میخواره را کردند سرمست جنون</p></div>
<div class="m2"><p>واعظ بیچاره را پا بست منبر داشتند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قد سرو و نارون دادند خوبان را ولی</p></div>
<div class="m2"><p>عاشقان را پای در گل دست بر سر داشتند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پیشکاران ازل کز پیشگاه لم یزل</p></div>
<div class="m2"><p>نفعها هر سو روان در دفع هرضر داشتند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا نگویی خیر و شر بی عزمشان آمد پدید</p></div>
<div class="m2"><p>یا نپنداری که بی موجب سرشر داشتند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فعلشان بر مقتضای قابل آمد در وجود</p></div>
<div class="m2"><p>زان ستمکش خواستند آن و ین ستمگر داشتند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قوه ها را راه سوی فعل دادند ارنه کی</p></div>
<div class="m2"><p>آنکه را مؤمن توانستند کافر داشتند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>می نبینی سایه ها را بیش و کم نزدیک و دور</p></div>
<div class="m2"><p>در خور خود پرتوی از تابش خور داشتند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>انبساطات وجود از اعتبارات حدود</p></div>
<div class="m2"><p>همچو ظل در قرب و بعد مهر انور داشتند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ور بگویی ز اعتباری کی اثر آمد پدید</p></div>
<div class="m2"><p>گویم این آثار هم اوهام مظهر داشتند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون در انسان عالم معنی و صورت را پدید</p></div>
<div class="m2"><p>زامتزاج خاک و آب و باد و آذر داشتند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از پی نظم دو عالم از پی هم یک بیک</p></div>
<div class="m2"><p>شاه بر شاه و پیمبر بر پیمبر داشتند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در ظهور احمدی ختم نبوت خواستند</p></div>
<div class="m2"><p>سلطنت را ختم بر شاه مظفر داشتند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تاج فرق خسروی فتح علی شه کز شرف</p></div>
<div class="m2"><p>خسروان خاک رهش را زیب افسر داشتند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بی قضای او قدر را کی مقرر یافتند</p></div>
<div class="m2"><p>بی رضای او قدر را کی مقدر داشتند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وقف بر اوقات دانی از چه شد حکم خلود</p></div>
<div class="m2"><p>حنجر بدخواه او را وقف خنجر داشتند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گفتمی فردا ودی گر مجتمع گشتی بهم</p></div>
<div class="m2"><p>چرخ را در سیر با عزمش برابر داشتند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در مشاهد حادث فردا چو دی شد گفتمی</p></div>
<div class="m2"><p>مهر را از نور رای او منور داشتند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کی فری یابد که یابد کیفر خصم ترا</p></div>
<div class="m2"><p>از مکافات ایمن و فارغ ز کیفر داشتند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کشورت را ایمن از آفات لشکر ساختند</p></div>
<div class="m2"><p>لشکرت را آفت سد گونه کشور داشتند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون بعزم رزمگه ترتیب لشکر ساختی</p></div>
<div class="m2"><p>هم زنامت فتح پیشاپیش لشکر داشتند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زیر رانت آسمان آسا ز عنصر پیکری</p></div>
<div class="m2"><p>کامتزاج او همین از باد و آذر داشتند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>لوحش الله باد پایی مسرعان فکر و وهم</p></div>
<div class="m2"><p>سرعتش با سرعت عزم تو همسر داشتند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از خرامش چرخی اندر ارض اغبر یافتند</p></div>
<div class="m2"><p>وزغبارش ارضی اندر چرخ اخضر داشتند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>این نه مهر است و نه ماه آن کار پردازان دهر</p></div>
<div class="m2"><p>چون بنای طرح این فرخنده نظر داشتند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از پی نعل سمش جسمی منور ساختند</p></div>
<div class="m2"><p>وز پی گوی دمش جرمی مدور داشتند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اسب تازی رزم سازی دست یازی بی دریغ</p></div>
<div class="m2"><p>موی تیغ آن کش ظفر از وی مصور داشتند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>رزم جویی مفرد آری در تصاریف قتال</p></div>
<div class="m2"><p>کثرت خصم ترا جمع مکسر داشتند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دشمنت را جای درد دوزخ شد اکنون باز گرد</p></div>
<div class="m2"><p>مقدمت را بزم از جنت نکوتر داشتند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>حبذا زان بزم خلد آسا که در هر شامگاه</p></div>
<div class="m2"><p>خادمانش از صباح عید خوشتر داشتند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در هوایش طبع عنصر با فلک آمیختند</p></div>
<div class="m2"><p>کافتاب و ماه بر سرو و صنوبر داشتند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>یا عزایم خوان شدندی مطربان کز هر طرف</p></div>
<div class="m2"><p>در فضایش از پری فوجی مسخر داشتند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مجمر آسا عارض خوبان فروزان و ندر آن</p></div>
<div class="m2"><p>جای عود از خط مشکین عنبر تر داشتند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ساقیان را دعوی اعجاز اگر باشد رواست</p></div>
<div class="m2"><p>زانکه در ساغر عیان با آب آذر داشتند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هوش بردند و روان دادند گفتی ساقیان</p></div>
<div class="m2"><p>آب خضرو آتش موسی بساغر داشتند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مهوشان در رقص از نزدیکی و دوری بهم</p></div>
<div class="m2"><p>راست رفتار دو شعر او دو پیکر داشتند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نیستند ار دشمن جان جراحت دیدگان</p></div>
<div class="m2"><p>جای دلها از چه در زلف معنبر داشتند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ور علاج ناتوانانشان نبودی در نظر</p></div>
<div class="m2"><p>پس چرا از چشم و لب بادام و شکر داشتند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نقشبندان قدم در کارگاه حادثات</p></div>
<div class="m2"><p>امتحان را هر زمانی نقش دیگر داشتند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گاه تمثالی زجم گه از فریدون ساختند</p></div>
<div class="m2"><p>گاه نقشی از ملکشه گه ز سنجر داشتند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نیک و بد آموختند آنگاه نقش روی تو</p></div>
<div class="m2"><p>کار بستند از سیه کاری قلم بر داشتند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تا ابد نقش است بر رخسار عالم بخت تو</p></div>
<div class="m2"><p>نقش بستندی جز این خوشتر از این گر داشتند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شاد باش و شادمان تا شاد باشد عالمی</p></div>
<div class="m2"><p>کانده و شادی بعالم از تو مصدر داشتند</p></div></div>