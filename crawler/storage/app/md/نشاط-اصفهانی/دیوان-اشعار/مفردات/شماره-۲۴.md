---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>شادمان غیر به الطاف تو، من شادم ازین</p></div>
<div class="m2"><p>که یقینت به وفاداری او نیست هنوز</p></div></div>