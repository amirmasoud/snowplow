---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>سید کونین سبط مصطفا</p></div>
<div class="m2"><p>بهترین فرزند خیرالاولیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پروریده حق در آغوش بتول</p></div>
<div class="m2"><p>زیب دامان، زینت دوش رسول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جبرئیلش مهد جنبان صبا</p></div>
<div class="m2"><p>شیر او را مایه از شیر خدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منبع هستی ست آن فرخنده ذات</p></div>
<div class="m2"><p>رشحه رشحه زو رسد بر کائنات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قوه ها را سوی فعل آورد او</p></div>
<div class="m2"><p>نیک را ممتاز از بد کرد او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دشمن از وی دشمن آمد، دوست دوست</p></div>
<div class="m2"><p>بد از او بد گشت و نیکو زو نکوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم وجود دشمن از جود وی است</p></div>
<div class="m2"><p>هم زیانش از پی سود وی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رهنمونش کرد خود بر قتل خویش</p></div>
<div class="m2"><p>پس بیفکندش سر تسلیم پیش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در قیامت نیز حاضر سازدش</p></div>
<div class="m2"><p>پس در آتش هم خود او اندازدش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هان مگو جبر این خطاب مستطاب</p></div>
<div class="m2"><p>فهم کن والله اعلم بالصواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه کنون زین فعل بد میسوزد او</p></div>
<div class="m2"><p>از ازل خود تا ابد میسوزد او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مصطفای دودمان ارتضا</p></div>
<div class="m2"><p>مرتضای خاندان اصطفا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جمله هستیها طفیل هست اوست</p></div>
<div class="m2"><p>زور بازوی یداله دست اوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کی سگی او را تواند بست دست</p></div>
<div class="m2"><p>شیر را روبه نداند دست بست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرنه خود از زندگی سیر آمدی</p></div>
<div class="m2"><p>عاجز از روباه کی شیر آمدی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ابن سعادت از ازل اندوخته است</p></div>
<div class="m2"><p>این شهادت از علی آموخته است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون پیام دوست از دشمن شنفت</p></div>
<div class="m2"><p>زیر زخم تیغ دشمن قرب گفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر که را از دوستانش خواند دوست</p></div>
<div class="m2"><p>زیر تیغ دشمنان بنشاند دوست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از نخست افتاد چون مقبول عشق</p></div>
<div class="m2"><p>لاجرم شد عاقبت مقتول عشق</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر حدیث ما تو را آمد عجب</p></div>
<div class="m2"><p>گفت حق خود در حدیث من طلب</p></div></div>