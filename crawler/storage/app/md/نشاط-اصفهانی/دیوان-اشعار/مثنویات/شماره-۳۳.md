---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>باز این دیوانه ی بگسسته بند</p></div>
<div class="m2"><p>فاش میگوید بآواز بلند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در همه عالم نبینم غیر دوست</p></div>
<div class="m2"><p>چیست عالم، نیست عالم گرنه اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کافر است این عاشق شوریده حال</p></div>
<div class="m2"><p>ای مسلمانان کافر کش تعال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اقتلونی کیف ماشاء الحبیب</p></div>
<div class="m2"><p>والطرحونی اینما جاء الحبیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق اگر کفر است بیشک کافرم</p></div>
<div class="m2"><p>گر کشی کافر بکش من حاضرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طایری را از قفس آزاد کن</p></div>
<div class="m2"><p>خاطر غمدیده ای را شاد کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرغ دامی را سوی بستان فرست</p></div>
<div class="m2"><p>تشنه کامی را بر عمان فرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من نمیگویم که عاشق کافراست</p></div>
<div class="m2"><p>عاشقی از کافری آنسوتر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کافرم ترسم اگر از کشتنم</p></div>
<div class="m2"><p>بنده ی شاهم نه در بند تنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این تن خاکی قرین خاک به</p></div>
<div class="m2"><p>دور ازین ناپاک جان پاک به</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این سرادر خورد ویران کردن است</p></div>
<div class="m2"><p>این قفس شایسته ی بشکستن است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرغ را خوشتر چه باشد از چمن</p></div>
<div class="m2"><p>زندگی تن بود زندان من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان سلیمان است و این دل خاتم است</p></div>
<div class="m2"><p>که بر او نفشی ز اسم اعظم است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وین تن مشؤومم آن دیو لعین</p></div>
<div class="m2"><p>کز سلیمان در ربودستی نگین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن حواس باطن و ظاهر همه</p></div>
<div class="m2"><p>امر وی را گشته فرمانبر همه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرگ کو تاداد جان گیرد ز تن</p></div>
<div class="m2"><p>خاتم جم را ستاند ز اهرمن</p></div></div>