---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>عشق از نو باز دستان‌ساز گشت</p></div>
<div class="m2"><p>عکس سوی اصل آخر بازگشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هجرها رفتند و آمد وصل‌ها</p></div>
<div class="m2"><p>عکس‌ها رفتند سوی اصل‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغی افتاده سوی دام از چمن</p></div>
<div class="m2"><p>بس عجب گر گیرد آرام از چمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور گرفتاری او بسیار شد</p></div>
<div class="m2"><p>مدتی مهجور از گلزار شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طبع او با دام و دانه یار گشت</p></div>
<div class="m2"><p>خاطر او فارغ از گلزار گشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با هم آوازان بطرف گلستان</p></div>
<div class="m2"><p>گاه در پرواز و گه در آشیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بدان غایت برون از یاد کرد</p></div>
<div class="m2"><p>کو همی خود را گمان آزاد کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باورش نامد که گلزاریش بود</p></div>
<div class="m2"><p>با گل و گلشن سرو کاریش بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوی گل رو در چمن بنمایدش</p></div>
<div class="m2"><p>رهنما جذب گلستان آیدش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق از نو باز دستان ساز کرد</p></div>
<div class="m2"><p>مرغ سوی آشیان پرواز کرد</p></div></div>