---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>زافرینش بیشتر حق بود و بس</p></div>
<div class="m2"><p>هستی از هستی مطلق بود و بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذات واجب بود و هستی کمال</p></div>
<div class="m2"><p>ایمن از هر نیستی و هر زوال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواست تا سازد جهانی از عدم</p></div>
<div class="m2"><p>نیستی را داد در هستی قدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیستی با هستی آمیزش گرفت</p></div>
<div class="m2"><p>با بلندی پستی آمیزش گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مایه ی هستی ممکن نیستی ست</p></div>
<div class="m2"><p>کس ز هستی غیر واجب هست نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیستی را گر بهستی ره نبود</p></div>
<div class="m2"><p>هستیی جز هستی اله نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نگشتی نقص پیدا با کمال</p></div>
<div class="m2"><p>کس نبودی غیر ذات ذوالجلال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده بگشا از سمک تا بر سماک</p></div>
<div class="m2"><p>از فراز عرش در قعر مغاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیک بنگر تا که در هر ذره ای</p></div>
<div class="m2"><p>از کمال و نقص بینی بهره ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نعمت و نقمت بهم آمیختند</p></div>
<div class="m2"><p>محنت و راحت زهم انگیختند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عقل اول کز دو عالم برتر است</p></div>
<div class="m2"><p>غیر حق از هر چه گویم سرور است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از غم تجدید و ظل احتیاج</p></div>
<div class="m2"><p>ممکن است و نیستی ممکن علاج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاک ره گر خارت آید در نظر</p></div>
<div class="m2"><p>فخرها دارد ز یک ره بر بشر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر چه اندوزی نبینی هیچ سود</p></div>
<div class="m2"><p>گر ندارد هیچ خود دارد وجود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر که باشد جز خدای لایزال</p></div>
<div class="m2"><p>هم در او نقص است و هم در وی کمال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ذلتی دارد رهین عزتی</p></div>
<div class="m2"><p>نعمتی دارد قرین نقمتی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اولیا و انبیا و رهنما</p></div>
<div class="m2"><p>خازنان گنج اسرار خدا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر کمالی رو نمودی سویشان</p></div>
<div class="m2"><p>سوی دیگر نقص باید رویشان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ور بدیدی وقتی آسیب از نقم</p></div>
<div class="m2"><p>شکر میگفتند بر دیگر نعم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه ملول از آن و نه معذور از این</p></div>
<div class="m2"><p>نه ز شادی شاد و نه از غم غمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من که سد شادی بهر کامیم هست</p></div>
<div class="m2"><p>شهدها بر لب زهر جامیم هست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از غمی کی تلخ سازم کام خویش</p></div>
<div class="m2"><p>تلخ بگذارم بخود ایام خویش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این غمم را هم نشاطی از پی است</p></div>
<div class="m2"><p>امشب و فردا نمیدانم کی است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر که دارد غمگساری چون خدا</p></div>
<div class="m2"><p>گر غمین باشد کجا باشد روا</p></div></div>