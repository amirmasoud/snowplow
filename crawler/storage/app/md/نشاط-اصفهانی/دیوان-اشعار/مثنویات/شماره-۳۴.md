---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>یارب بر لب ولی دل غافل است</p></div>
<div class="m2"><p>کز لبت تا دل هزاران منزل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زبان با جان و لب با دل یکی ست</p></div>
<div class="m2"><p>از دعا تا مدعا حاصل یکی ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وانکه سازد با دعا لب را قرین</p></div>
<div class="m2"><p>نی دعا با مدعا شد همنشین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه جان پیوسته دارد با خدا</p></div>
<div class="m2"><p>از دعا لب بست و دل از مدعا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنده ام وز بندگی شرمنده ام</p></div>
<div class="m2"><p>شرم بادم زین که گویم بنده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من کیم شایسته ی جرگ سگان</p></div>
<div class="m2"><p>کی شمارندم ز جمع بندگان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت آن شاهنشه ملک یقین</p></div>
<div class="m2"><p>مفخر الکونین امیرالمؤمنین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وقت آنکس خوش که در آن زندگی</p></div>
<div class="m2"><p>وقت خود را بگذراند چون سگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زانکه ده حال نکو در وی بود</p></div>
<div class="m2"><p>کانکه در وی نیست مؤمن کی بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در میان خلق او را قدر نیست</p></div>
<div class="m2"><p>هر که مسکین است با این حال زیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دور او را هست فقر و نیست مال</p></div>
<div class="m2"><p>وندرین حالت مجرد را همال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سوم آن باشد که او هرگز نجست</p></div>
<div class="m2"><p>مسکنی معلوم و مأوایی درست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خود بساط اوست سرتاسر زمین</p></div>
<div class="m2"><p>از علامات توکل باشد این</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چارم اکثر وقت را جایع بود</p></div>
<div class="m2"><p>این صفت از صالحان شایع بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پنجم او را صاحب او گر زند</p></div>
<div class="m2"><p>باز بر درگاه او خوش می تند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حاش لله ترک آن در گوید او</p></div>
<div class="m2"><p>یا دری غیر از در او جوید او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از علامات مرید اینست کو</p></div>
<div class="m2"><p>از جفا هرگز نگردد غیر جو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سادس او شب را نخوابد جز قلیل</p></div>
<div class="m2"><p>وین صفت باشد محبان را دلیل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سابع آنکس را که شد فرمانبرش</p></div>
<div class="m2"><p>گرزند سد بار و راند از درش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون بخواند باز آید شاد و خوش</p></div>
<div class="m2"><p>نه دلی پر حقد و نه رویی ترش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>راندن و خواندن بود یکسان بر آن</p></div>
<div class="m2"><p>باشد این حال از خصال خاشعان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ثامن اکثر حال او باشد سکوت</p></div>
<div class="m2"><p>تاسع او خشنود از صاحب بقوت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این سکوتش از علامات رضاست</p></div>
<div class="m2"><p>وین رضایش از قناعت مقتضاست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عاشر آن باشد که چون میرد سگی</p></div>
<div class="m2"><p>نیست میراثیش پر یا اندکی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وین نباشد جز ممات زاهدین</p></div>
<div class="m2"><p>رب الحقنی بهم فی الغابرین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای خدای من، من آنم کز کرم</p></div>
<div class="m2"><p>سوی هستی دادیم ره از عدم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ذات بیچونت چو کرد آهنک جود</p></div>
<div class="m2"><p>هیچ را داد از کرامت هر چه بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جود تو چون هیچتر از من ندید</p></div>
<div class="m2"><p>تا کمال و فضل تو آرد پدید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر چه مخزون بود در گنج عدم</p></div>
<div class="m2"><p>یک بیک را زد بنام من رقم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قدرتت از نیستی هستیم داد</p></div>
<div class="m2"><p>از تهیدستی زبر دستیم داد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چیستم من؟ نیستم، هستی تر است</p></div>
<div class="m2"><p>آستینم من زبردستی تر است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>راست گویم من هنوز آن نیستم</p></div>
<div class="m2"><p>ور نگویم من تو دانی چیسیتم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای ز بودت ظلمت ما را ظهور</p></div>
<div class="m2"><p>ظلمت ما پرده ی رخسار نور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ای تو ذاتت عدل وای و صفت کرم</p></div>
<div class="m2"><p>من نبودم قابل چندین کرم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نه همین بر مستحقان کافیی</p></div>
<div class="m2"><p>عدل را عین و کرم را وافیی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خیر تو نازل بسوی ما همی</p></div>
<div class="m2"><p>شر ما صاعد بسویت هر دمی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نعمتت بر من فزون شد از شمار</p></div>
<div class="m2"><p>وانچه پیدا شد یکی بود از هزار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ای بسا نعمت که از من شد نهان</p></div>
<div class="m2"><p>یا که خود پیدا و من غافل از آن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>این یکی نعمت که ای دادار غیب</p></div>
<div class="m2"><p>بر من از رحمت شدی ستارعیب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هستی و نیکوییم بود و نبود</p></div>
<div class="m2"><p>لطفت آن پیدا و این پنهان نمود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زشتیم را دادی آیین جمال</p></div>
<div class="m2"><p>ظلمتم را نور و نقصم را کمال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پای تا سر عیب چون دیدی همی</p></div>
<div class="m2"><p>عیب من از خلق پوشیدی همی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هم ز تو دارم امید ای ذوالمنن</p></div>
<div class="m2"><p>که ز من پنهان نماند عیب من</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آزمایش را گهی در ابتلا</p></div>
<div class="m2"><p>کرد و سد نعمت نهان در یک بلا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر جفا کردم وفا دیدم زتو</p></div>
<div class="m2"><p>گر خطا کردم عطا دیدم ز تو</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جای شکر از من نه عصیان یافته</p></div>
<div class="m2"><p>من بپاداش تو احسان یافته</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>من کجا و ذکر منتهای تو</p></div>
<div class="m2"><p>من کجا و شکر نعمتهای تو</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شاعران را شکر نعمت مدحت است</p></div>
<div class="m2"><p>خادمان را شکر نعمت خدمت است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نعمتی را شکر اگر هم کرده ام</p></div>
<div class="m2"><p>جای خدمت مدحتی آورده ام</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نعمتت افزونتر آمد از قیاس</p></div>
<div class="m2"><p>چون قیاسش نیست چون بتوان سپاس</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ره نمودی برده ره از غفلتم</p></div>
<div class="m2"><p>بند دادی سخت تر شد قسوتم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نیکویی کردی باعطای جمیل</p></div>
<div class="m2"><p>من بعصیان سر کشیدم ای خلیل</p></div></div>