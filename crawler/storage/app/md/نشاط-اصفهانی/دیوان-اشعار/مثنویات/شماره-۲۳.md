---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>فتنه از ملک شهنشه دور شد</p></div>
<div class="m2"><p>بود هر جا دشمنی مقهور شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر این دل نیز زان شاه ماست</p></div>
<div class="m2"><p>تا بکی مقهور نفس فتنه زاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای خدا تا کی بباید زیستن</p></div>
<div class="m2"><p>گه اسیر نفس و گه مقهور تن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قاصد جانی و مقصود دلی</p></div>
<div class="m2"><p>خالق جان و دل از آب و گلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چیست جان مرغی و کویت گلشنی</p></div>
<div class="m2"><p>چیست دل از تن بسویت روزنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرگ کو تا رخنه در روزن کند</p></div>
<div class="m2"><p>از بن این دیوار غم را بر کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این نه مرگ من بود مرگ تن است</p></div>
<div class="m2"><p>تن قفس جان مرغ و جانان گلشن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من قفس را جا بگلشن دیده ام</p></div>
<div class="m2"><p>بر قفس سد گونه روزن دیده ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوش بر آواز مرغان چمن</p></div>
<div class="m2"><p>چشم بر شاخ کل و سرو و سمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گه ازین رخنه گه از آن روزنم</p></div>
<div class="m2"><p>منتظر تا کی قفس را بشکنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرگ تن در تن حیات جان شود</p></div>
<div class="m2"><p>مشکلات من ز مرگ آسان شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرگ تن سهل است جان پاینده باد</p></div>
<div class="m2"><p>ور شود جان نیز جانان زنده باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من ز مرگ اندیشم ای بس ابلهی</p></div>
<div class="m2"><p>شرح این قصه که گوید کو تهی</p></div></div>