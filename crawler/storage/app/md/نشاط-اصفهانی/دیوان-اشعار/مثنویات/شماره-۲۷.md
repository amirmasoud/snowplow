---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>این امام و رهنمای هشتمین</p></div>
<div class="m2"><p>هم صراط حق و هم نور مبین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای فروغ هفتم از نور دوم</p></div>
<div class="m2"><p>انظرونا نقتبس من نورکم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انت قلب القلب قلاب النفوس</p></div>
<div class="m2"><p>انت نور النوریا شمس الشموس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو سرا پا عدلی و نوری تمام</p></div>
<div class="m2"><p>من ز پا تا سر همه ظلم و ظلام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظلمتی را رو بسوی نور بین</p></div>
<div class="m2"><p>صبح پایان شب دیجور بین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ضیا ظلمت چه جوید جز فنا</p></div>
<div class="m2"><p>تا رود ظلمت نماند جز ضیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چیست ظلمت نیست ظلمت جز عدم</p></div>
<div class="m2"><p>هم تو بودی هم تو خواهد بود هم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من گرفتم رو نهادم سوی تو</p></div>
<div class="m2"><p>با کدامین رو ببینم روی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سینه ی من در خور مهر تو نیست</p></div>
<div class="m2"><p>دیده ی من لایق چهر تو نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه سری دارم سزای درگهت</p></div>
<div class="m2"><p>نه رخی شایسته ی خاک رهت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روی من شایسته ی آن خاک نیست</p></div>
<div class="m2"><p>در خور آن پاک، این ناپاک نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر سرم از لطف اگر آری گذر</p></div>
<div class="m2"><p>افکنی از مهر اگر سویم نظر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اولم دستی بباید دادنت</p></div>
<div class="m2"><p>تا توانم زان بگیرم دامنت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس دلی سوزان و چشمی غرق خون</p></div>
<div class="m2"><p>طاقتی اندک، غمی از حد فزون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس زبانی کاشف هر گونه راز</p></div>
<div class="m2"><p>پس بیانی سر بسر عجز و نیاز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زان سپس گوشی به قیل و قال من</p></div>
<div class="m2"><p>جای رحم است آن زمان بر حال من</p></div></div>