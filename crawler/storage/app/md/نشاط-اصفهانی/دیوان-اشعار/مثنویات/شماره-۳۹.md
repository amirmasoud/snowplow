---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>نفس شوم تو چاه تاریک است</p></div>
<div class="m2"><p>راه شرع ار چه راست باریک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل و علم آن چراغ و این روغن</p></div>
<div class="m2"><p>بشب تیره راه از آن روشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق پوینده مرکبی رهجو</p></div>
<div class="m2"><p>باشد از ذکر تازیانه ی او</p></div></div>