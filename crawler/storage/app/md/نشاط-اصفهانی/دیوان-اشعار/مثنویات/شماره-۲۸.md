---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>این منم کاینسان خجل از خاک توس</p></div>
<div class="m2"><p>میبرندم! این دریغ وای فسوس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیخود و درمانده و سر گشته ام</p></div>
<div class="m2"><p>خشک لب از طرف جوبر گشته ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کسی کز طرف جو کامی گرفت</p></div>
<div class="m2"><p>بر مراد کام خود جامی گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خور جامی نیامد کام من</p></div>
<div class="m2"><p>لایق سنگی نشد هم جام من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرح اوصاف گلم رهزن شده</p></div>
<div class="m2"><p>لیک بسته چشم و در گلشن شده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بازگشته از گلستان خوار و زار</p></div>
<div class="m2"><p>خود چه یا بد کور از گل غیر خار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشته یوسف را خریدار از کلاف</p></div>
<div class="m2"><p>رانده با رخش اسب چو بین در مصاف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کیستم من رهروی بی راحله</p></div>
<div class="m2"><p>کیستم من واپسی از قافله</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنده ای در کار خود درمانده ای</p></div>
<div class="m2"><p>از در صاحب بخواری رانده ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنده ی بیشرم و گستاخ و جسور</p></div>
<div class="m2"><p>با که آوخ با خداوندی غیور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مستمندی خسته مسکینی غریب</p></div>
<div class="m2"><p>از صلای عام سلطان بی نصیب</p></div></div>