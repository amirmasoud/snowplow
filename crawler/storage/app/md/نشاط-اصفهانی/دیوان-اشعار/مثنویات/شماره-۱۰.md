---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>ای یگانه گوهر سلک وجود</p></div>
<div class="m2"><p>دومین نقش خوش کلک وجود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می ندانم اولی یا آخری</p></div>
<div class="m2"><p>جز یکی از هر که گویم برتری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دو چشم منکرانت کور بود</p></div>
<div class="m2"><p>ورنه ذاتت را دو عالم نور بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهر با هر ذره پرتو افکن است</p></div>
<div class="m2"><p>کوری هر کور را ببیند روشن است</p></div></div>