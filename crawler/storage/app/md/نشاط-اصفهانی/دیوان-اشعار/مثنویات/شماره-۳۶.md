---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>ملک چاکر خدیوا پادشاها</p></div>
<div class="m2"><p>جهان داور شها عالم پناها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر گردنگشان و سرفرازان</p></div>
<div class="m2"><p>بدرگاهت نیاز بی نیازان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشانی آسمان از پایه ی تو</p></div>
<div class="m2"><p>فروغی اختران از سایه ی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریدون حشمت اسکندر خصالی</p></div>
<div class="m2"><p>غلط گفتم که بی شبه و مثالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آهنگر فریدون راست لافی</p></div>
<div class="m2"><p>تو از فولاد تیغ آهن شکافی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بساط خسروی رایت چو گسترد</p></div>
<div class="m2"><p>سکندر نیست جز پیکی جهانگرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلیمان گر ندادی خاتم از دست</p></div>
<div class="m2"><p>تو را گفتم ز شاهان همسری هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملک بر درگهت خدمتگزاری</p></div>
<div class="m2"><p>فلک در پیشگاهت پیشکاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خرابی آسمان از کشور تو</p></div>
<div class="m2"><p>ثوابت ماندگان لشکر تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمین مشتی غبار از آستانت</p></div>
<div class="m2"><p>حجابی چند بر در آسمانت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بجز تاج از تو کس برتر نباشد</p></div>
<div class="m2"><p>بجز افسر ترا همسر نباشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهان یکسر گزید آسایش از تو</p></div>
<div class="m2"><p>جهانداری گرفت آرایش از تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهان جسم است و حکم تو روانست</p></div>
<div class="m2"><p>جدایی جسم از جان کی توانست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زذاتت جز خدا برتر که باشد</p></div>
<div class="m2"><p>گر این شاهی خداوندی چه باشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بمیزان سخن مدحت نسنجد</p></div>
<div class="m2"><p>چو باشد لفظ در معنی نگنجد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فزون ز اندیشه بیرون از گمانی</p></div>
<div class="m2"><p>چه گویم کانیچنین یا آنچنانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حکیم گنجه دانای سخن سنج</p></div>
<div class="m2"><p>که دارد گنج گوهر از سخن پنچ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بوقتی گفت بهر عذر تقصیر</p></div>
<div class="m2"><p>که گردیر آمدم شیر آمدم شیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گذارش گر بدین درگاه بودی</p></div>
<div class="m2"><p>اگر شیر آمدی روباه بودی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه تنها بردرت دیر آمدستم</p></div>
<div class="m2"><p>که با سد گونه تقصیر آمدستم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ولی روباهی و شیری ندانم</p></div>
<div class="m2"><p>همین دانم سگ این آستانم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گشایی گر نظر یک ره بسویم</p></div>
<div class="m2"><p>گشاید سد در دولت برویم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دلی کو را با خلاصت نیازاست</p></div>
<div class="m2"><p>زبانی کو بمدحت نکته ساز است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پریشان سازدش انده روانیست</p></div>
<div class="m2"><p>زغم خاموش بنشیند سزانیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر آتش گر ببینی گل بروید</p></div>
<div class="m2"><p>به آب ار بنگری پستی نجوید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر یابد ز راهت باد گردی</p></div>
<div class="m2"><p>ز سر بگذارد این بیهوده گردی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گذارد سر بپایت هر که چون بخت</p></div>
<div class="m2"><p>سزای تاج گردد در خور تخت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زبانها بی ثنایت چاک بادا</p></div>
<div class="m2"><p>روانها بی هوایت خاک بادا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لبی فارغ مبادا از دعایت</p></div>
<div class="m2"><p>دلی طالب مبادا جز رضایت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سپهر اندر حساب کشورت باد</p></div>
<div class="m2"><p>کواکب در شمار لشکرت باد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جهان بین جهان پر نور از تو</p></div>
<div class="m2"><p>فلک خرم زمین معمور از تو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زهی تمثال جان پرور که آرد</p></div>
<div class="m2"><p>به تن جان گر چه جان در تن ندارد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از آن بی پرده نوری آشکار است</p></div>
<div class="m2"><p>که در نه پرده پنهان پرده داراست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عجب نبود مثالش گر محال است</p></div>
<div class="m2"><p>مثال پادشاه بی مثال است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تعالی الله زهی شاه جوان بخت</p></div>
<div class="m2"><p>طراز افسر و آرایش تخت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خیالی آسمان از پایه ی او</p></div>
<div class="m2"><p>مثالی آفتاب از سایه ی او</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کواکب عکس نقش خاک راهش</p></div>
<div class="m2"><p>جهان تمثالی از تصویر جاهش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز عدلش پای کبک کوهساری</p></div>
<div class="m2"><p>خضاب از خون مرغان شکاری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>قضا چون آهوی سر در کمندش</p></div>
<div class="m2"><p>سر گردون لگد کوب سمندش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>برون ز اندیشه بیرون از گمان است</p></div>
<div class="m2"><p>چه گویم کاینچنین یا آنچنان است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو زین معنی نشاید راز گویم</p></div>
<div class="m2"><p>همان به شرح صورت باز گویم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>حجابی کانجمن ساز نقوش است</p></div>
<div class="m2"><p>مثال صید گاه کالپوش است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز گرگان چون هژ بران برگذشتند</p></div>
<div class="m2"><p>بشادی کوه و هامون در نوشتند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>صباحی جانفزا روزی دل افروز</p></div>
<div class="m2"><p>چو تخت و بخت شه میمون و فیروز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شمیمش راحت تن مایه ی جان</p></div>
<div class="m2"><p>نسیمش همچو جان پیدا و پنهان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چمن خرم زابر نو بهاران</p></div>
<div class="m2"><p>ولی چندان ترشحهای باران</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کزان پر لاله را ساغر نگشتی</p></div>
<div class="m2"><p>وزان دامان زاهد تر نگشتی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>صبا چندان که گل دفتر نریزد</p></div>
<div class="m2"><p>شراب لاله از ساغر نریزد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>پریشان زان شود زلف نکویان</p></div>
<div class="m2"><p>نسازد لیک دلها را پریشان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شهنشه با غلامان صید جویان</p></div>
<div class="m2"><p>در این نخجیر گه گشتند پویان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>وشاقان صف بصف رومی و چینی</p></div>
<div class="m2"><p>چگویم من به آیینی که بینی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>پریوش چاکران صف برکشیده</p></div>
<div class="m2"><p>ملک بر پشت دیوی جا گزیده</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سمندی چون برانگیزد برزمش</p></div>
<div class="m2"><p>بر آن پیشی نگیرد غیر عزمش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کمندی رشته گویی روزگارش</p></div>
<div class="m2"><p>قضا را با قدر در پود و تارش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سنانی ز آفت جانها سرشته</p></div>
<div class="m2"><p>بر آن توقیع خونریزی نوشته</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کمانی سخت چون سلک عطایش</p></div>
<div class="m2"><p>بر آن تیری چو رای بی خطایش</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خدنگی همچو اخگر تاب داده</p></div>
<div class="m2"><p>تو گویی زاتش قهر آب داده</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>قدوم شاه را مرغان نوا ساز</p></div>
<div class="m2"><p>ز خرسندی گو زنان در تک و تاز</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چنان بستند خود را بر کمندش</p></div>
<div class="m2"><p>که نگشاید کسی از صید بندش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز پیشش بسملی گر گام برداشت</p></div>
<div class="m2"><p>زکیشش حسرت تیر دگر داشت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>اگر شیری رسیدی در کمینش</p></div>
<div class="m2"><p>ندیدی زخم او جز بر سرینش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>غزالی پشت کردی گر بجنگش</p></div>
<div class="m2"><p>بجز بردیده کی دیدی خدنگش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ز گردون و زمین هر دم صدایی</p></div>
<div class="m2"><p>که ای تیر و سنان آخر خطایی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سنایی را خطا بر گورا زین داشت</p></div>
<div class="m2"><p>که در دل حسرتی گاو زمین داشت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>اگر بر طایری تیری خطا رفت</p></div>
<div class="m2"><p>بصید نسر طایر بر سما رفت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو جان اندر جهان حکمش روان باد</p></div>
<div class="m2"><p>جهان تا هست او جان جهان باد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سرگردنکشان فتراک جویش</p></div>
<div class="m2"><p>روان تاجداران خاک کویش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>مرادش را قضا زین هفت پرده</p></div>
<div class="m2"><p>بر آرد صورتی هر هفت کرده</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>پردگر طایری بی شوق دامش</p></div>
<div class="m2"><p>بود ذوق پر افشانی حرامش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>زمانه یارو گردون یاورش باد</p></div>
<div class="m2"><p>نشاط از خاکبوسان درش باد</p></div></div>