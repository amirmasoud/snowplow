---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>ای عشق آخر سخن گذارت کردم</p></div>
<div class="m2"><p>آسوده و عاجز و نزارت کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چهل سال شنیده ام ز تو لاف و گزاف</p></div>
<div class="m2"><p>آخر دردست عقل خوارت کردم</p></div></div>