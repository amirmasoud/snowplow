---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>رفتی بسر عشق تو پاینده هنوز</p></div>
<div class="m2"><p>از دست غم تو دل پراکنده هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو جان منی و بی تو ای جان جهان</p></div>
<div class="m2"><p>شرمم بادا که بی توام زنده هنوز</p></div></div>