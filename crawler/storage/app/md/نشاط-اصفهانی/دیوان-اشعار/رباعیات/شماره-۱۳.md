---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>این جان که زتن هر دمش آزاری هست</p></div>
<div class="m2"><p>گفتم که مگر ترا بوی کاری هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ورنه بقفس چرا بماند مرغی</p></div>
<div class="m2"><p>کز هر طرفش راه بگلزاری هست</p></div></div>