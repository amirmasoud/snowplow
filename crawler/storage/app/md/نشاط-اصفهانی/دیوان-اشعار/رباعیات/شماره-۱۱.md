---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>ای قهر ازل سرشته با شمشیرت</p></div>
<div class="m2"><p>وی حبس ابد نوشته بر زنجیرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تبلیغ قضا فاتحه ی یرلیغت</p></div>
<div class="m2"><p>تقدیر خدا خاتمه ی تدبیرت</p></div></div>