---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>عمرم همه جز بکام خاطر بگذشت</p></div>
<div class="m2"><p>یک روز مرا چو روز دیگر بگذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی نگذشت بر من از دولت عشق</p></div>
<div class="m2"><p>کز روز دگر مرا نکوتر نگذشت</p></div></div>