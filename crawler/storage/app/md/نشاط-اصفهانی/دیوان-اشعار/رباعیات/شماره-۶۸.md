---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>گر دل داری بدست جز یار مجوی</p></div>
<div class="m2"><p>ورنه بجز از رضای دلدار مجوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دل بکسی دهی ز جان هم بگذر</p></div>
<div class="m2"><p>چون یار بجستی دگر اغیار مجوی</p></div></div>