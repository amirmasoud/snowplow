---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>پیوند غمت تا بدل و جان بستم</p></div>
<div class="m2"><p>از دل ببریدم وز جان بگسستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندوه تو را چه شکر گویم کزوی</p></div>
<div class="m2"><p>از شادی و اندوه دو عالم رستم</p></div></div>