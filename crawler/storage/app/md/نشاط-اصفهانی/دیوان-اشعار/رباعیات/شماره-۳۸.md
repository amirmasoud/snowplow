---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>گر بار دگر گذر بکویت فکنم</p></div>
<div class="m2"><p>این دیده ی غمدیده برویت فکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا دل که بجان رسیده گیرم ز تو باز</p></div>
<div class="m2"><p>یا جان بلب رسیده سویت فکنم</p></div></div>