---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>روی تو نگاه خویش دیدن نتوان</p></div>
<div class="m2"><p>وز دیدن تو طمع بریدن نتوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی دیده ببیندت که در دیده ی من</p></div>
<div class="m2"><p>تو نوری و نور دیده دیدن نتوان</p></div></div>