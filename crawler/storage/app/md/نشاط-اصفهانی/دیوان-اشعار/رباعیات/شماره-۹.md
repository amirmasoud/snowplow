---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>پایی که بجان هر قدمی نقشی بست</p></div>
<div class="m2"><p>زلفی که دلی از آن بهر تارش بست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردا نگذارد که بدان سایم رخ</p></div>
<div class="m2"><p>آوخ نپسندد که بدان آرم دست</p></div></div>