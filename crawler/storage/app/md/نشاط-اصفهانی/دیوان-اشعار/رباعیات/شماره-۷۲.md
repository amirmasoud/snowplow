---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>وقتست که بر من ای نسیم سحری</p></div>
<div class="m2"><p>رحم آری و بر ساحل رودی گذری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان خاک بدین چشم غباری آری</p></div>
<div class="m2"><p>زین چشم بدان رود درودی ببری</p></div></div>