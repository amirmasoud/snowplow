---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>گر ره بخدا جویی در گام نخست</p></div>
<div class="m2"><p>نقش خودی از صفحه ی جان باید شست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گم گشته ز تو گوهر مقصود و تو خود</p></div>
<div class="m2"><p>تا گم نشوی گمشده نتوانی جست</p></div></div>