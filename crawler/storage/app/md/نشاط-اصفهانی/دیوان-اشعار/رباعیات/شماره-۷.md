---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>گر با تو بود کس همه عالم راه است</p></div>
<div class="m2"><p>ور بی تو رود جهان سراسر چاه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خاک سرو چاک گریبان پیوست</p></div>
<div class="m2"><p>آن دست که از دامن تو کوتاه است</p></div></div>