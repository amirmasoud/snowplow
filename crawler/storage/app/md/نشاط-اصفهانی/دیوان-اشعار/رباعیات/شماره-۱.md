---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ناکامی من بود ز خودکامی‌ها</p></div>
<div class="m2"><p>این سوختگی‌ها همه از خامی‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کام دل دوست طلب کردم، شد</p></div>
<div class="m2"><p>کام دل من روا ز ناکامی‌ها</p></div></div>