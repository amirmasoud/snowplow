---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>من از کرمت بسی حکایت دارم</p></div>
<div class="m2"><p>کی از ستمت دگر شکایت دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز ستمی ندیده ام از تو بلی</p></div>
<div class="m2"><p>ز اندازه فزون چشم عنایت دارم</p></div></div>