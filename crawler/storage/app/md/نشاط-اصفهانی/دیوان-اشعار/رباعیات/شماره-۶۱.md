---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>ما هیچ نداریم پسند دل تو</p></div>
<div class="m2"><p>جز یک دل و آن نیز بود منزل تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر هیچ نداریم بغیر تو، خوشیم</p></div>
<div class="m2"><p>غیر از تو چه باشد که بود قابل تو</p></div></div>