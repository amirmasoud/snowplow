---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>ای خواجه ی جان وای خداوند دلم</p></div>
<div class="m2"><p>از یاد تو بیشتر ز رویت خجلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از من بحلی هر چه کنی یا نکنی</p></div>
<div class="m2"><p>ای وای بمن اگر نسازی بحلم</p></div></div>