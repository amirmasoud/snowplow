---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>گفتم رویش گفت نهان خواهد بود</p></div>
<div class="m2"><p>در مویش و مویش بمیان خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم سر ما و خنجر او گفتا</p></div>
<div class="m2"><p>آن نیز نصیب دشمنان خواهد بود</p></div></div>