---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>سر گر نه بپای اوست بی تن خوشتر</p></div>
<div class="m2"><p>پا گر نه براه او بدامن خوشتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن ره که نه سوی اوست گمگشته نکوست</p></div>
<div class="m2"><p>آن سوی که نه بکوی اوست بی من خوشتر</p></div></div>