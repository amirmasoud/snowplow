---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>جانی که اسیر دست هجران دارم</p></div>
<div class="m2"><p>خواهم که فدای پای جانان دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کاش بدا منش در آرم روزی</p></div>
<div class="m2"><p>دستی کامشب سوی گریبان دارم</p></div></div>