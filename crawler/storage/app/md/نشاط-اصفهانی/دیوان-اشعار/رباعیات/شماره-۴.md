---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>بزم طرب آخر شد و پایان شب است</p></div>
<div class="m2"><p>با نغمه ی چنگ و نی نویدی عجب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب رفت و صباح دولت اندر عقب است</p></div>
<div class="m2"><p>شادی پی شادی طرب اندر طرب است</p></div></div>