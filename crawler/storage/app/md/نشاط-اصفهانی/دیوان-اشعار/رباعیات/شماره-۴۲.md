---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>از میکده میآیم و چندان مستم</p></div>
<div class="m2"><p>کاگاه نیم که نیستم یا هستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خلوت عشق تو بدیوان خرد</p></div>
<div class="m2"><p>سد جای فتم اگر نگیری دستم</p></div></div>