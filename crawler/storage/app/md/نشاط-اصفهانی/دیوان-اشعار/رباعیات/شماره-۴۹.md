---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>از دوری تو تن نزاری دارم</p></div>
<div class="m2"><p>جانی غمگین، دل فکاری دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در رهگذرت نشسته جان بر سر دست</p></div>
<div class="m2"><p>برخیز و بیا که با تو کاری دارم</p></div></div>