---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>کیوانت ستاده بر در ایوان باد</p></div>
<div class="m2"><p>بهرام فتاده بر سر میدان باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناهید درون بزم و برجیس برون</p></div>
<div class="m2"><p>مه بر سر مهر و تیر در فرمان باد</p></div></div>