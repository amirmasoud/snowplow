---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>ای جمالت شمع هر جا محفلی ست</p></div>
<div class="m2"><p>از خیالت پرتوی با هر دلی ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون منی را با تو بودن مشکل است</p></div>
<div class="m2"><p>ورنه آسان با تو هرجا مشکلی ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برفشان اشکی بخاک راه دوست</p></div>
<div class="m2"><p>گل از آنجا سرزند کانجا گلی ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رو بسویش نه که رویش سوی تست</p></div>
<div class="m2"><p>بی قبولش نیست هرجا مقبلی ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیستیمان آیت هستی اوست</p></div>
<div class="m2"><p>دلبری باشد بهرجا بیدلی ست</p></div></div>