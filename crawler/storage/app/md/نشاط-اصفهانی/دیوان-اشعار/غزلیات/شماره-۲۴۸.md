---
title: >-
    شمارهٔ ۲۴۸
---
# شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>ای شیفته ی روی نکوی تو جهانی</p></div>
<div class="m2"><p>نیکو نتوان گفت که نیکو تر از آنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پیکر من روحی و در دیده ی من نور</p></div>
<div class="m2"><p>نزدیکی و دوری و عیانی و نهانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آشوب سر، آسیب خرد، آفت هوشی</p></div>
<div class="m2"><p>آرام دل، آسایش تن، راحت جانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خاطر آگاه دلان معنی عقلی</p></div>
<div class="m2"><p>در دیده ی صاحب نظران صورت جانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنرا که بنظاره ی روی تو فتد کار</p></div>
<div class="m2"><p>هر بار دانی باید و هر لحظه روانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وانرا که در اوصاف تو باشد سر گفتار</p></div>
<div class="m2"><p>هر عضو لبی باید و هر موی زبانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بد عهدی و جور از تو نکو روی نیاید</p></div>
<div class="m2"><p>یا از اثر عهد شهنشاه جهانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دارای جهان فتحعلی شه که مبادا</p></div>
<div class="m2"><p>از خدمت او دور نشاط ارچه زمانی</p></div></div>