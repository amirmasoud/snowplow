---
title: >-
    شمارهٔ ۲۰۶
---
# شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>بی تو میل گل و گلشن نکنم</p></div>
<div class="m2"><p>هوس سوری و سوسن نکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامن از خون دلم گلگون شد</p></div>
<div class="m2"><p>ورنه گل بی تو بدامن نکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرسد شام که در خلوت دل</p></div>
<div class="m2"><p>شمعی از یاد تو روشن نکنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشود صبح که در منظر چشم</p></div>
<div class="m2"><p>بر سر راه تو مسکن نکنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من که غارت زده ی ملک شهم</p></div>
<div class="m2"><p>دگر اندیشه ی رهزن نکنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من که سد دشت نهفتم در مرد</p></div>
<div class="m2"><p>حذر از کودک برزن نکنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من که سد تیغ فشردم در دل</p></div>
<div class="m2"><p>ناله از کاوش سوزن نکنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سر بام تو بر خاسته ام</p></div>
<div class="m2"><p>جز ببام تو نشیمن نکنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترک جان در ره دلدار نشاط</p></div>
<div class="m2"><p>که نکرده ست که تا من نکنم</p></div></div>