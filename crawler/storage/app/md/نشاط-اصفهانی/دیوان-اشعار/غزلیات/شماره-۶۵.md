---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>اگرت دیده و دل شیفته و گریان نیست</p></div>
<div class="m2"><p>برو ای خواجه که در عشق ترا فرمان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منظر دوست چرا از نظری اشک فشان</p></div>
<div class="m2"><p>نوبهاریست که دروی اثر از باران نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی بیگانه چو روز است ولی روز فراق</p></div>
<div class="m2"><p>طره ی یار چو شب لیک شب هجران نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار باز آمد و آشفتگی از دل نه برفت</p></div>
<div class="m2"><p>این چه دردیست که دروی اثر از درمان نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که روی تو ندیدست زگفتار نشاط</p></div>
<div class="m2"><p>عجبی نیست که دروی اثری چندان نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناله ی بلبلش از جا نبرد دل چه عجب</p></div>
<div class="m2"><p>هر کرا دیده به دیدار گلی حیران نیست</p></div></div>