---
title: >-
    شمارهٔ ۲۲۱
---
# شمارهٔ ۲۲۱

<div class="b" id="bn1"><div class="m1"><p>صبح باز آمد و شب گشت نهان</p></div>
<div class="m2"><p>موکب روز بسر آراست جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز از هر طرف اصحاب بهار</p></div>
<div class="m2"><p>غارت آورد بر افواج خزان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موکب شاه جهان در جنبش</p></div>
<div class="m2"><p>توسن فتح و ظفر در جولان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملک در ملک جهان زیر نگین</p></div>
<div class="m2"><p>جیش در جیش سپه در فرمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهر در شهر خراج است و منال</p></div>
<div class="m2"><p>دشت در دشت رکابست و عنان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گنج در گنج یمین تا به یسار</p></div>
<div class="m2"><p>خیل در خیل کران تا بکران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملت از احمد و آیین ز علی</p></div>
<div class="m2"><p>همت از شاه و ظفر از یزدان</p></div></div>