---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>صبح شد برخیز و برزن دامن خرگاه را</p></div>
<div class="m2"><p>تاز سر بیرون کنیم این خفتن بیگاه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی گلچهره شاهد بین و غایب شمع را</p></div>
<div class="m2"><p>مهر عالمتاب طالع بین و غارت ماه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آبی از ساغر بزن بر عشق و در مجمر بسوز</p></div>
<div class="m2"><p>حاصل این عقل غم افزای شادی کاه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرمی خواهی زمستی خواه و از بیدانشی</p></div>
<div class="m2"><p>کاسمان بی غم نماند خاطر آگاه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل فکر آموز در عالم نشان از خود ندید</p></div>
<div class="m2"><p>هم نبیند عشق عالم سوز جز الاه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده ناپاک است تا شویی روان کن اشک را</p></div>
<div class="m2"><p>پرده افلاک است تا سوزی بر افروز آه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود حجاب عکس ماهی چند داری سر بچاه</p></div>
<div class="m2"><p>سر بر آراز چاه تا بر چرخ بینی ماه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آبش از سر برگذشت ای همرهان آگه کنید</p></div>
<div class="m2"><p>هم ملامتگوی عاشق هم سلامت خواه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر سر زلف درازش عمر بگذارم نشاط</p></div>
<div class="m2"><p>بو که پیوندی کنم این رشتهٔ کوتاه را</p></div></div>