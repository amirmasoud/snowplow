---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>قرارگاه جهان بر مدار تقدیر است</p></div>
<div class="m2"><p>عجب زخواجه که درگیر و دار تدبیر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر بلطف بخوانند کبک صیاد است</p></div>
<div class="m2"><p>و گر بقهر برانند باز نخجیر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه لطف خاصه ی طاعت نه خشم لازم جرم</p></div>
<div class="m2"><p>خدای را چه طلسم است این چه تأثیر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهی کمال کشی ابروان بر چینش</p></div>
<div class="m2"><p>که هر طرف نگرد دیده، تیر بر تیر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و زان لبان شکر بار او تعالی الله</p></div>
<div class="m2"><p>که گر بزهر دهد حکم شهد با شیر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بقید زلف ببستی دل مراد و دلی</p></div>
<div class="m2"><p>که در تو شیفته نبود سزای زنجیر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زناله بس مکن ای دل در آن سلاسل زلف</p></div>
<div class="m2"><p>که هست اگر اثر از ناله های شبگیر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خمار دوش نماند از نشاط اثر ساقی</p></div>
<div class="m2"><p>بیار باده که گر زود میرسی دیر است</p></div></div>