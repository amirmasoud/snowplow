---
title: >-
    شمارهٔ ۲۴۶
---
# شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>نشاید ار چو تویی در کنار من باشی</p></div>
<div class="m2"><p>همین بس است که گویند یار من باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا بیک نگه از خود خجل توانی کرد</p></div>
<div class="m2"><p>مباد کز ستمی شرمسار من باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو کز میان دل من قدم برون ننهی</p></div>
<div class="m2"><p>نمیشود که دمی در کنار من باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بباغ مشک فشان میوزد نسیم بهار</p></div>
<div class="m2"><p>بیا که مرهم جان فکار من باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو عکس سرو بن از جویبار باغ عیان</p></div>
<div class="m2"><p>بدیده از مژه ی اشکبار من باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شاهد ظفر اندر وصال موکب شاه</p></div>
<div class="m2"><p>گه از یمین و گهی از یسار من باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو خاک درگه شاه جهان ردیده ی ما</p></div>
<div class="m2"><p>فروغ مردمک چشم تار من باشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یگانه فتحعلی شه که نیست در عهدش</p></div>
<div class="m2"><p>غمی اگر تو دمی غمگسار من باشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه غم که نیست سزاوار بند گیت نشاط</p></div>
<div class="m2"><p>تو را سزد که خداوندگار من باشی</p></div></div>