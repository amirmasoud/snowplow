---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>هر چه جویند ز ما در طلب آن باشیم</p></div>
<div class="m2"><p>ما نه نیکیم و نه بد بندهٔ فرمان باشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه زشتیم ولی در کنف نیکانیم</p></div>
<div class="m2"><p>گر چه خاریم ولی خار گلستان باشیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرسامان منت نیست ولی چتوان کرد</p></div>
<div class="m2"><p>قسمت اینست که ما بی سرو سامان باشیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز سیلی رسد از راه ندانم یارب</p></div>
<div class="m2"><p>تا که ویران تر از این چیست که ویران باشیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان بیفشانم و دامن بفشانی ترسم</p></div>
<div class="m2"><p>آید آن روز کزین هر دو پشیمان باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاقبت کیست که با زلف تواش کاری نیست</p></div>
<div class="m2"><p>وقت ما خوش که زآغاز پریشان باشیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد و درمان همه در ماست نداریم نشاط</p></div>
<div class="m2"><p>دردی از کس که ز کس طالب درمان باشیم</p></div></div>