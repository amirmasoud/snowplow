---
title: >-
    شمارهٔ ۱۵۷
---
# شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>اگر چه ناصح ما مشفق است و خیر اندیش</p></div>
<div class="m2"><p>به تندرست چه گویم من از جراحت ریش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهیچ حادثه ما را غمین نشاید داشت</p></div>
<div class="m2"><p>که از وجود تو شادیم نی زهستی خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمش نهفته نشاید بدل که مقدم شاه</p></div>
<div class="m2"><p>نهان ز خلق نماند بکلبه ی درویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به همعنانی طفلان نی سوار بماند</p></div>
<div class="m2"><p>چه تیغها به نیام و چه تیرها در کیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو در دل من و سد بار از دلم افزون</p></div>
<div class="m2"><p>بعالم اندر و زاندازه ی دو عالم بیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یگانه فتحعلی شه خدیو نیک نهاد</p></div>
<div class="m2"><p>خداش نیکی بخش و قضاش نیک اندیش</p></div></div>