---
title: >-
    شمارهٔ ۱۷۳
---
# شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>سلطان ملک فقرم و عشق است لشکرم</p></div>
<div class="m2"><p>ترک دو کون تاجم و کونین کشورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم غرق بحر نیستیم ساخت عشق و هم</p></div>
<div class="m2"><p>در حفظ فلک هستی کونین لنگرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آلایشی بظاهرم ارهست باک نیست</p></div>
<div class="m2"><p>زیرا که اصل پاکم و از نسل حیدرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حق را ولی مطلق و دین را صراط حق</p></div>
<div class="m2"><p>گر غیر حق بدانمش الحق که کافرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افکنده بودم از ره ازین پیش دیو نفس</p></div>
<div class="m2"><p>ظل خدا براه هدا گشت رهبرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فردا که پرده دور شود از جمال قرب</p></div>
<div class="m2"><p>یا رب مدار دور زآل پیمبرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچ آن سزای آل علی نیست در جهان</p></div>
<div class="m2"><p>گر گنج عالم است مبادا میسرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز با هوای دوست نهم سر اگر نشاط</p></div>
<div class="m2"><p>از خاک سر بر آورم ای خاک بر سرم</p></div></div>