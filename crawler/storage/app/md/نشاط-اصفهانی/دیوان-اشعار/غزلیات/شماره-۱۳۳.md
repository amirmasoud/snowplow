---
title: >-
    شمارهٔ ۱۳۳
---
# شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>ناصح اگر بر آن رخ نیکو نظر کند</p></div>
<div class="m2"><p>بندد زبان زپند و سخن مختصر کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مینالم از غم تو و اینهم غم دگر</p></div>
<div class="m2"><p>کاین ناله دانم آخرنا گه اثر کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر من چگونه میگذرد بی تو صبح و شام</p></div>
<div class="m2"><p>داند کسی که با تو شبی را سحر کند</p></div></div>