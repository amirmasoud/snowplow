---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>دیوانه و مست و باده نوشیم</p></div>
<div class="m2"><p>پرورده ی دست می فروشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم زیور ساعد جنونیم</p></div>
<div class="m2"><p>هم ساعد آستین هوشیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم در صف زاهدان مسجد</p></div>
<div class="m2"><p>سجاده نشین و خرقه پوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم از پی ساقیان محفل</p></div>
<div class="m2"><p>پیمانه کش و سبو بدوشیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از مستی باده هوش بخشیم</p></div>
<div class="m2"><p>وز ساغر عقل می فروشیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کی طلبند و باز خواهند</p></div>
<div class="m2"><p>جان بر لب و گوش بر سروشیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم نغمه ی بلبلان عرشیم</p></div>
<div class="m2"><p>در منطق زاهدان خموشیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کوتاه زبان شویم شاید</p></div>
<div class="m2"><p>تا مستمع دراز گوشیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما طایر بوستان قدسیم</p></div>
<div class="m2"><p>با مرغ هم آشیان خروشیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با گوش سخن نیوش نطقیم</p></div>
<div class="m2"><p>با نطق گهر فروش گوشیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا خواست قضا رضای ما خواست</p></div>
<div class="m2"><p>بیهوده نشاط از چه کوشیم</p></div></div>