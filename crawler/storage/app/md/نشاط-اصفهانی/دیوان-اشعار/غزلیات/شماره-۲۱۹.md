---
title: >-
    شمارهٔ ۲۱۹
---
# شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>خرم آنان کافرید از نور خود یزدانشان</p></div>
<div class="m2"><p>آفرینش تابشی از طلعت تابانشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز گردانند مهر از غرب و شق سازند ماه</p></div>
<div class="m2"><p>آسمان گوییست گویی در خم چوگانشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون به حکم آیند و تمکین خاک تنشان خوابگاه</p></div>
<div class="m2"><p>چون براق عزم در زین آسمان میدانشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تشنه لب در رزم دشمن لیک اندر بزم دوست</p></div>
<div class="m2"><p>چشمه ی خور دردی از ته جرعهٔ دورانشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیستیشان تا بقوت شام وانگه کاینات</p></div>
<div class="m2"><p>از ازل بر خوان هستی تا ابد مهمانشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در قضای حق رضاشان راستی خواهی، قضا</p></div>
<div class="m2"><p>هست اجمالی که تفصیلش بود فرمانشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فارق حقند و باطل خون نا حق کشتگان</p></div>
<div class="m2"><p>از لب هر زخم انا الحق میسراید جانشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه شادی بخش کونین است غمگینش مخوان</p></div>
<div class="m2"><p>دیده شان گریان مبین بنگر دل خندانشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نور یزدانند اینان بس عجب نبود اگر</p></div>
<div class="m2"><p>باشد از رحمت نظر بر سایهٔ یزدانشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من به خود قالبی بی‌جان نمی‌بینم نشاط</p></div>
<div class="m2"><p>جان عالم سر به سر بادا فدای جانشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نا امید از ابر رحمت نیستم من کیستم</p></div>
<div class="m2"><p>خاکی از ایوانشان یا خاری از بستانشان</p></div></div>