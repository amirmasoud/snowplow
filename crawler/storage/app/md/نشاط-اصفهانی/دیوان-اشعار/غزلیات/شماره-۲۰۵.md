---
title: >-
    شمارهٔ ۲۰۵
---
# شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>بر آن سرم که به پیمانه دست بگشایم</p></div>
<div class="m2"><p>غبار، عقل ز رخسار عشق بزدایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی بطره ی ساقی کهن بگیسوی چنگ</p></div>
<div class="m2"><p>گره ببندم و از کار بسته بگشایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا نه دست ستیزی بود نه پای گریز</p></div>
<div class="m2"><p>اگر بخشم بر آیی بعجز باز آیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا بدست خضیبت چه جای پنجه، ولی</p></div>
<div class="m2"><p>بخون خویش توانم که پنجه آلایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پی قبول تو آراست هر کسی خود را</p></div>
<div class="m2"><p>من از قبول تو خود را مگر بیارایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار بادیه پیموده ام بدین امید</p></div>
<div class="m2"><p>که در سرای مغان جرعه ای بپیمایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفتم اینکه نعیم جهان بکام من است</p></div>
<div class="m2"><p>روان بکاهم تا چند و تن بیفزایم</p></div></div>