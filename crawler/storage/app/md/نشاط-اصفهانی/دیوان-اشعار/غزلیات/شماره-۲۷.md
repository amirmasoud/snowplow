---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>فرخنده پیکریست که سر در هوای تست</p></div>
<div class="m2"><p>فرخنده تر سریست که بر خاکپای تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودای زاهدان همه شوق بهشت و حور</p></div>
<div class="m2"><p>غوغای عارفان همه ذوق لقای تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امروز اگر بباد رود در رهت چه باک</p></div>
<div class="m2"><p>فردا که سر زخاک بر آید بپای تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر خدمتیست از تو بما بار نعمتیست</p></div>
<div class="m2"><p>کاری نکرد بنده که گوید برای تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را بقدر خویش خطائیست لاجرم</p></div>
<div class="m2"><p>چندان که بیش باشد کم از عطای تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عفو تو دیده ایم و گنه کرده ایم، اگر</p></div>
<div class="m2"><p>بر جرم ما نبینی و بخشی سزای تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر بر مراد دوست نهادی به تیغ خصم</p></div>
<div class="m2"><p>ای کشته غم مدار که خود خونبهای تست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آهسته تر نمیروی ای میر کاروان</p></div>
<div class="m2"><p>ای بس ضعیف و خسته که اندر فقای تست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تن خسته، دل شکسته، نظر بسته، لب خموش</p></div>
<div class="m2"><p>ای عشق کار ما همه بر مدعای تست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر کس نشاط رشک ندارد ز راحتی</p></div>
<div class="m2"><p>الا بر آن دلی که بغم مبتلای تست</p></div></div>