---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>عمر بگذشت و نماندست جز ایامی چند</p></div>
<div class="m2"><p>به که با یاد کسی صبح شود شامی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به حقیقت نبود در همه عالم جز عشق</p></div>
<div class="m2"><p>زهد و رندی و غم و شادی از او نامی چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زحمت بادیه حاجت نبود در ره دوست</p></div>
<div class="m2"><p>خواجه برخیز و برون آی ز خود گامی چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طبع خاکی بنه و چاک بر افلاک انداز</p></div>
<div class="m2"><p>مرغ کز دام بر آید چه بود بامی چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ دل در طلب دانهٔ خالت مشغول</p></div>
<div class="m2"><p>من چه باکم بود از سرزنش عامی چند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خم زلفت به بناگوش سرافکنده بماند</p></div>
<div class="m2"><p>کز دل غم‌زده بودش به تو پیغامی چند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتشی بر سر این کوی برافروخت نشاط</p></div>
<div class="m2"><p>در نگیرد ولی از شعلهٔ او خامی چند</p></div></div>