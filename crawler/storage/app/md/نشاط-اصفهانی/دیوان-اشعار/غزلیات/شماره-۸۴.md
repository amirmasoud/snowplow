---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>مثال هستی با نیستی روان و تن است</p></div>
<div class="m2"><p>روان حقیقت هستی و نیستی بدن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه صرف هست هویدا نه صرف نیست پدید</p></div>
<div class="m2"><p>نمایش خوش از آمیزش دو مقترن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه هست نیست شدستی نه نیست هست شود</p></div>
<div class="m2"><p>نه حق جهان نه جهان حق نه جای این سخن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا چه حد که بگویند آن من است و من او</p></div>
<div class="m2"><p>هر آنچه هست وی است و هر آنچه نیست من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه عکس شخص و نه ظل و نه موج بحر و نه یم</p></div>
<div class="m2"><p>که ظل و شخص و نم و بحر جمله خویشتن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگوش کس نرود این حدیث نغز نشاط</p></div>
<div class="m2"><p>بدل بگو، نه بهر دل، دلی که ممتحن است</p></div></div>