---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>مگو مرگ است بی او زندگانی</p></div>
<div class="m2"><p>که این ناکامی است آن کامرانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لبم بست از شکایت عشق و آموخت</p></div>
<div class="m2"><p>نگاهش را زبان بی زبانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز رشک خضر میمیرم که دانم</p></div>
<div class="m2"><p>نمی بخشد جز آن لب زندگانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غمش با ناتوانان سازگار است</p></div>
<div class="m2"><p>توانایی مجو تا میتوانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن گلشن چه دل بندم که باشد</p></div>
<div class="m2"><p>پی گل چیدن آنجا باغبانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جزای رنج یک نظاره بر شیخ</p></div>
<div class="m2"><p>عجب نبود بهشت جاودانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در این گلشن مرا داد الفت برق</p></div>
<div class="m2"><p>فراق از زحمت هم آشیانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا پایان کار جان سپردن</p></div>
<div class="m2"><p>تو را آغاز عهد دلستانی</p></div></div>