---
title: >-
    شمارهٔ ۲۳۰
---
# شمارهٔ ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>او میرود ز پیش و من اندر قفای او</p></div>
<div class="m2"><p>او فارغ است از من و من مبتلای او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشکین کمند گیسویش افتاده از قفا</p></div>
<div class="m2"><p>هر جا دلیست میکشد اندر قفا ی او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم که از خطای من افزون چه میشود</p></div>
<div class="m2"><p>شرمنده تر شدم چو بدیدم عطای او</p></div></div>