---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>دوشینه بر مراد دل آمد بسر شبم</p></div>
<div class="m2"><p>ذکر خدا و شکر خداوند بر لبم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی بریز باده بر آیین گریه ام</p></div>
<div class="m2"><p>مطرب بساز نغمه بآهنگ یاربم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا رب تو آگهی تو که در سر نبود و نیست</p></div>
<div class="m2"><p>هرگز هوای حشمت دنیا و منصبم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم نهم بخاک دری سر و گر نه چیست</p></div>
<div class="m2"><p>از احتمال کشمکش دهر مطلبم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که ساغری کشم از صاف بندگی</p></div>
<div class="m2"><p>دست زمانه خاک فشاند بمشربم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیوانه را چه حاصلی از رایء قلان</p></div>
<div class="m2"><p>باید بعشق خواند حدیثی زمذهبم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آخر برون زخود قدمی مینهم نشاط</p></div>
<div class="m2"><p>بر در زخیل شاه ستاده ست مرکبم</p></div></div>