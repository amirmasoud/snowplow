---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>دل از سر کویت هوس خانه ندارد</p></div>
<div class="m2"><p>دیوانهٔ عشقت سر ویرانه ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز محنت و غم راه به این خانه ندارد</p></div>
<div class="m2"><p>این خانه مگر راه به میخانه ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیمانه چه غم گر شکند محتسب شهر</p></div>
<div class="m2"><p>مستیم از آن باده که پیمانه ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل را هوس الفت ما نیست ببینید</p></div>
<div class="m2"><p>دیوانه سر صحبت دیوانه ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستند دو عالم همه از ساغر وحدت</p></div>
<div class="m2"><p>خوش باش درین بزم که بیگانه ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آتش معشوق شراری بود این عشق</p></div>
<div class="m2"><p>شمعی که نیفروخته پروانه ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک بار ندیدیم نشاط آید ازین راه</p></div>
<div class="m2"><p>این کوچه مگر راه به میخانه ندارد</p></div></div>