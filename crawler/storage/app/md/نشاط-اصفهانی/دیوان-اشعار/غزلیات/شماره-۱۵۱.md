---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>مردود خلق گشتم و گشتم پسند خویش</p></div>
<div class="m2"><p>رستم ز بند غیر و فتادم به بند خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند درد زهر بکامم زجام غیر</p></div>
<div class="m2"><p>زین پس من و مذاق خوش از صاف قند خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را بتلخکامی خود ذوق دیگر است</p></div>
<div class="m2"><p>چندین مدار پاس لب نوشخند خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توفان ز دیده آرم و بندم لب از سخن</p></div>
<div class="m2"><p>تنگ آمدم ز دعوت نا سودمند خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در باغ جعد سنبل و در بزم زلف یار</p></div>
<div class="m2"><p>هرجا بصورتی دگر آرد کمند خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر عنان عشق سپردم بدست عقل</p></div>
<div class="m2"><p>این عرصه ای نبود که تازم سمند خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خامی ز سر بنه که بخاک اوفتی نشاط</p></div>
<div class="m2"><p>ای میوه خام باش بشاخ بلند خویش</p></div></div>