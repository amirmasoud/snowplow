---
title: >-
    شمارهٔ ۲۷۱
---
# شمارهٔ ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>ای جم از این می اگر جامی زنی</p></div>
<div class="m2"><p>دست یازد بر تو کی اهریمنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رکابی گر بر انگیزی کمیت</p></div>
<div class="m2"><p>باز داری چرخ را از توسنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست بر کاری زدن بیحاصلی ست</p></div>
<div class="m2"><p>دست باید زد ولی بر دامنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق اگر از عقل خیزد رهبر است</p></div>
<div class="m2"><p>لیک اگر از نفس زاید رهزنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این جهان ز آمیزش عقل است و عشق</p></div>
<div class="m2"><p>مرد کی فرزند آرد بی زنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موکب شاه است بیرون سرای</p></div>
<div class="m2"><p>گر برون نایی ببین از روزنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز دل غمگین مسکینان نشاط</p></div>
<div class="m2"><p>جان جانها را نباشد مسکنی</p></div></div>