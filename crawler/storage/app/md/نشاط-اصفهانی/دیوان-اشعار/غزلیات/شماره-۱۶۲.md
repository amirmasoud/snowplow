---
title: >-
    شمارهٔ ۱۶۲
---
# شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>بگیر دست دل و سر بر آور از افلاک</p></div>
<div class="m2"><p>چه خواهی از تن خاکی که باز گردد خاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بکوش تا مگر این خار گل ببار آرد</p></div>
<div class="m2"><p>و گرنه بار نیابد ببزم شه خاشاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باشک دیده بشوی و بخاک چهره بسای</p></div>
<div class="m2"><p>کز آب و خاک توان کرد پاک هر ناپاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملول شد دلم از تن، خدای را در شهر</p></div>
<div class="m2"><p>کراست خنجر خونریز و بازوی چالاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر تو زخم زنی درد یابم از مرهم</p></div>
<div class="m2"><p>اگر تو زهر دهی رنج بینم از تریاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سزای من ز تو آهنگ طاعتی هیهات</p></div>
<div class="m2"><p>بجای من ز تو تغییر نعمتی حاشاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو پرسش است بمحشر مگر ز ما پرسند</p></div>
<div class="m2"><p>حساب دامن پر خون و جامه ی سد چاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ظهور خلق بحق بین، ظهور حق در خلق</p></div>
<div class="m2"><p>فداک عینک حقا وانت لست نراک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس است حاصل ادراک این دقیقه نشاط</p></div>
<div class="m2"><p>که ره بسوی حقیقت نمیبرد ادراک</p></div></div>