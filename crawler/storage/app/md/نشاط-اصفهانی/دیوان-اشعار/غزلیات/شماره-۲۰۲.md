---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>غم عالم نبرد ره به دلم</p></div>
<div class="m2"><p>که سرشتند به میخانه گلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من چه دارم که ز احسان تو نیست</p></div>
<div class="m2"><p>جان اگر بر تو فشانم خجلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش دزدیده نگاهی به رخت</p></div>
<div class="m2"><p>کردم، آوخ نکنی گر بحلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌برم حسرت روی تو به خاک</p></div>
<div class="m2"><p>تا چه گل‌ها که بروید ز گلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گو فرود آکه گشایش با تست</p></div>
<div class="m2"><p>مکن اندیشه ز تنگی دلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرح دل کار زبان نیست نشاط</p></div>
<div class="m2"><p>کاش بیرون فتد از سینه دلم</p></div></div>