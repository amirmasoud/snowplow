---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>گر بسوزیم بآتش همه گویند سزاست</p></div>
<div class="m2"><p>در خور جورم و از فضل توام چشم عطاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بخوانی زعطا سر زخطا در پیش است</p></div>
<div class="m2"><p>ور برانی بجفا روی امیدم بقفاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بخود هر چه کنم گر کرم است آن ستم است</p></div>
<div class="m2"><p>تو جفا می نکنی ور بکنی عین وفاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بسا لطف که در چشم بصیرت قهر است</p></div>
<div class="m2"><p>باز قهریست که در پیش نظر لطف نماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش دوزخ و آن چشمه ی جان بخش بهشت</p></div>
<div class="m2"><p>شعله ای از دل من، رشحه ای از دست شماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دفتر عشق سراسر همه خواندیم ولی</p></div>
<div class="m2"><p>آنچه در یاد بماندست فراموشی ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شادمانی جهان است که فانی گردد</p></div>
<div class="m2"><p>غم بدان دل نبرد ره که نشاطش به خداست</p></div></div>