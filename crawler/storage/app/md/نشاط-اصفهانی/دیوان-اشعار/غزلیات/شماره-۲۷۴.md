---
title: >-
    شمارهٔ ۲۷۴
---
# شمارهٔ ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>این شهد نمیرسد بکامی</p></div>
<div class="m2"><p>این صید نمی فتد بدامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سد جو برهش ز دیده بستم</p></div>
<div class="m2"><p>این سرو نمیکند خرامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دستم رسد ار بچین زلفش</p></div>
<div class="m2"><p>سد صبح بر آورم ز شامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را که بهیچ میفروشند</p></div>
<div class="m2"><p>ای خواجه نمیخری غلامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز آن رخ آتشین بر افروز</p></div>
<div class="m2"><p>یک شعله چه میکند بخامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارم ز تو چشم یک نگه باز</p></div>
<div class="m2"><p>من مست نمیشوم ز جامی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی عشق چه خاصیت دهد عقل</p></div>
<div class="m2"><p>بی تیغ چه آید از نیامی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی عزم چه سود آورد حزم</p></div>
<div class="m2"><p>بی شیر چه خیزد از کنامی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از خویش برون شو اول آنگاه</p></div>
<div class="m2"><p>بگذار براه دوست گامی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رسوای غمت نشاط و غم نیست</p></div>
<div class="m2"><p>این ننگ نمیدهد بنامی</p></div></div>