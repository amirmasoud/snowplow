---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>زبان بر بند ای ناصح زپندم</p></div>
<div class="m2"><p>اگر دستت رسد بگشای بندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمان ابروی من بازو مرنجان</p></div>
<div class="m2"><p>که من خود آهوی سردر کمندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از بند تو هرگز سر نپیچم</p></div>
<div class="m2"><p>اگر ریزند از هم بند بندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر فضل است و بخشش تا بخواهی</p></div>
<div class="m2"><p>فقیر و بینوا و مستمندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر عدل است و پرسش، تا شماری</p></div>
<div class="m2"><p>گنهکارم، مپرس از چون و چندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر گلشن که کردم عزم شاخی</p></div>
<div class="m2"><p>رسید از خار گلبن سد گزندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر صحرا که دیدم نیش خاری</p></div>
<div class="m2"><p>دلیلی شد بیاد نوشخندم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در این ره دست من گیرند روزی</p></div>
<div class="m2"><p>سری امروز بر پایی فکندم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باین سستی که میرانم در این دشت</p></div>
<div class="m2"><p>نشاط افتد کجا صیدی به بندم</p></div></div>