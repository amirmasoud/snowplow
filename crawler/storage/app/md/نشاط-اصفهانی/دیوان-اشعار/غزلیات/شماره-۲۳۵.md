---
title: >-
    شمارهٔ ۲۳۵
---
# شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>دیدیم کرانه تا کرانه</p></div>
<div class="m2"><p>غیر از تو نبود در میانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم دست هزار آستینی</p></div>
<div class="m2"><p>هم صدر هزار آستانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک گلبن و سد هزار گلشن</p></div>
<div class="m2"><p>یک شاهد و سد هزار خانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شادی زمانه جاودان نیست</p></div>
<div class="m2"><p>اندوه تو عیش جاودانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جرم دگر است طاعت ما</p></div>
<div class="m2"><p>عفو تو نجوید ار بهانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسوده تر آنکه غرقه شد زود</p></div>
<div class="m2"><p>کاین بحر نباشدش کرانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست ار نرسد بر آستینش</p></div>
<div class="m2"><p>بگذار سری بر آستانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب را بنشاط خوش بصبح آر</p></div>
<div class="m2"><p>تا صبح چه آورد زمانه</p></div></div>