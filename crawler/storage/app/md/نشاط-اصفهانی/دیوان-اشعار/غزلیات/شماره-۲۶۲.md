---
title: >-
    شمارهٔ ۲۶۲
---
# شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>تو بدین شکل و شمایل که به خود می‌نگری</p></div>
<div class="m2"><p>جای آن است که بر ما به تکبر گذری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نه خود جان منی از چه برون می نایی</p></div>
<div class="m2"><p>گر نه خود عمر منی از چه بغفلت گذری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من چنان رفته ام از خود که زخود بی خبرم</p></div>
<div class="m2"><p>نتوان عیب تو گفتن که ز من بی خبری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکنی بر شکن طره ی پرتاب فکن</p></div>
<div class="m2"><p>تا کی ای شوخ شکر لب دل ما میشکری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیگران بیخبرند از نظر چالاکت</p></div>
<div class="m2"><p>نگهت سوی من و دیده بسوی دگری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه با زلف پریشان دل مجموعش هست</p></div>
<div class="m2"><p>در همه جمع ندارد ز من آشفته تری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باده در دست از آن به که بود باد بدست</p></div>
<div class="m2"><p>می بخور تا غم بیهوده ی دنیا نخوری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه شب دیده براه تو نهادست نشاط</p></div>
<div class="m2"><p>تا به خاک قدمت خشک کند چشم تری</p></div></div>