---
title: >-
    شمارهٔ ۲۳۹
---
# شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>ز ما گمگشتگان پرسید از آن کوی</p></div>
<div class="m2"><p>سراغ تشنگان جویید از جوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میی بی غش، بتی دلکش، دلی خوش</p></div>
<div class="m2"><p>لب ساقی، لب ساغر، لب جوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فسونگر نو گلی، جزعی نظر باز</p></div>
<div class="m2"><p>زبان دان بلبلی، لعلی سخنگوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر از هر چه گویی، لب فروبند</p></div>
<div class="m2"><p>دگر از هر چه جویی، دل فروشوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و گر زین جمله جز غم حاصلت نیست</p></div>
<div class="m2"><p>نشاط آسا دل دیوانه ای جوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در ویرانه ی دل کوب و زانجا</p></div>
<div class="m2"><p>سراغ بیدلان می پرس و میپوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سری پر فتنه و کاری خطرناک</p></div>
<div class="m2"><p>دلی بی باک و یاری مصلحت جوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشاط از یمن خاک پای خسرو</p></div>
<div class="m2"><p>که آراید ملک زان لب فلک روی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از این صحرا مگر بیرون کشی رخت</p></div>
<div class="m2"><p>ازین میدان مگر بیرون بری گوی</p></div></div>