---
title: >-
    شمارهٔ ۲۶۰
---
# شمارهٔ ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>من و اندیشه ی باری که ندارد یاری</p></div>
<div class="m2"><p>نگشاید دل از آن گل که بود با خاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عجب از مفلس بی خانه که مهمان خواند</p></div>
<div class="m2"><p>دل بدست آر و پس آنگه بطلب دلداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راحت هر دو جهان پاکی دل از هوس است</p></div>
<div class="m2"><p>زر چو پاک ست بود رایج هر بازاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیخ شهر از من دیوانه حذر می نکند</p></div>
<div class="m2"><p>من که مستم چه حذر میکنم از هشیاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل آیینه صفت جو به نثار رخ دوست</p></div>
<div class="m2"><p>ورنه اینجا سر و زر را نبود مقداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سایه افتاد هم از گام نخستین بر خاک</p></div>
<div class="m2"><p>تا چرا با قد او لاف زد از رفتاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم بر اندازه ی غمخوار فرستند نشاط</p></div>
<div class="m2"><p>غم فزون داراز آنجا که تواش غمخواری</p></div></div>