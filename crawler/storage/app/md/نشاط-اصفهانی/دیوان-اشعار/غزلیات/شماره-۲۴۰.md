---
title: >-
    شمارهٔ ۲۴۰
---
# شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>هوسی میبردم سوی کسی</p></div>
<div class="m2"><p>تا چه بازم بسر آرد هوسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خبری نیستم از راه هنوز</p></div>
<div class="m2"><p>ناله ای میشنوم از جرسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذوق پرواز چه داند مرغی</p></div>
<div class="m2"><p>کامد از ببضه برون در قفسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق نگذاشت کر از من اثری</p></div>
<div class="m2"><p>عیب عاشق نتوان گفت بسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ عاقل ننهد جرم بوی</p></div>
<div class="m2"><p>بر سر آتش اگر سوخت خسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق و فرمان خرد کی باشد</p></div>
<div class="m2"><p>شاهبازی بمراد مگسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زیر پا تا ننهی سر نبود</p></div>
<div class="m2"><p>بسر زلف ویت دسترسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با که گوید سخن دوست نشاط</p></div>
<div class="m2"><p>که ندارد بجز از دوست کسی</p></div></div>