---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>تا به کی این صبح و این شام مکرر بگذرد</p></div>
<div class="m2"><p>حیف باشد عمر اگر زینسان سراسر بگذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خوشا آن صبح کز رویی منور بر دمد</p></div>
<div class="m2"><p>وان شب دلکش که با مویی معنبر بگذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترسمت ای خفته در دامان کوهی سیل خیز</p></div>
<div class="m2"><p>خواب نگذاری ز سر تا آبت از سر بگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوش تا جاوید در زحمت نمانی ورنه عمر</p></div>
<div class="m2"><p>بگذرد آخر چه سود از آن که خوشتر بگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیمه برتر زد ز دل سلطان عشق او ولی</p></div>
<div class="m2"><p>سالها ماند خراب آنجا که لشکر بگذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باورم ناید که آبی جان ببخشد جاودان</p></div>
<div class="m2"><p>چشمه ی حیوان مگر از خاک آن در بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چاک سازد عاشق اول سینه وانگه جامه را</p></div>
<div class="m2"><p>تیغ عشق اول بسر آنگه ز مغفر بگذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زندگی بی جان نشاید کرد در عالم نشاط</p></div>
<div class="m2"><p>بگذر از عمری که دور از روی دلبر بگذرد</p></div></div>