---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>من نه آنم که دل آزرده ز بیداد شوم</p></div>
<div class="m2"><p>هر ستم را کرمی بینم و دلشاد شوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا توانی بخرابی من ای عشق بکوش</p></div>
<div class="m2"><p>من نه آنم که از این پس دگر آباد شوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عتابی که بیادش دهدم خوشتر چیست</p></div>
<div class="m2"><p>ستم آنست که یکباره من از یاد شوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرش جانب گلزار گذاری بفتد</p></div>
<div class="m2"><p>سر کنم نغمه و تا خانه ی صیاد شوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آبروی دو جهان جویم و از آتش عشق</p></div>
<div class="m2"><p>خاک سازم تن و در راه تو بر باد شوم</p></div></div>