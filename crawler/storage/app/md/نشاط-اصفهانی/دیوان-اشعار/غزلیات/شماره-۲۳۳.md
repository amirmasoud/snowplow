---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>توانایی چه جویی، خستگی به</p></div>
<div class="m2"><p>بدین تندی مران آهستگی به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفتاران زلف پر شکن را</p></div>
<div class="m2"><p>پریشان حالی واشکستگی به</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلا گر مهر پیوستی به یک سو</p></div>
<div class="m2"><p>از این سوی دگر بگسستگی به</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گره مگشا از آن گیسوی پر چین</p></div>
<div class="m2"><p>که ما را از گشایش بستگی به</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشاط گه به گه کمتر ز غم نیست</p></div>
<div class="m2"><p>اگر خود غم بود پیوستگی به</p></div></div>