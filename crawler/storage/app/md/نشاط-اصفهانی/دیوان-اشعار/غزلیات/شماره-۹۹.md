---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>این نکویان که بلای دل اهل نظرند</p></div>
<div class="m2"><p>دشمن جان و دل و از دل و جان خوبترند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقان را نتوان داد دل غمزده داد</p></div>
<div class="m2"><p>ورنه خوبان نه ستم پیشه مه بیداد گرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاک کن دل زهر آلایش و آنگه بدر آی</p></div>
<div class="m2"><p>که مقیمان در میکده صاحبنظرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پای بر فرق جهان سر بکف پای حبیب</p></div>
<div class="m2"><p>تا نگویی تو که این طایفه بی پاو سرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم کاریت بباید که در آن شادی اوست</p></div>
<div class="m2"><p>ورنه شادی و غم کار جهان در گذرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خط بگرد رخش آید بشبیخون روزی</p></div>
<div class="m2"><p>عاشقان بی‌خبر از فتنهٔ دور قمرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من و باد سحر از بوی تو سر گشته همین</p></div>
<div class="m2"><p>یا همه شیفتگان تو چنین در بدرند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غارت آورد بر افواج خزان خیل بهار</p></div>
<div class="m2"><p>بلبلان نغمه سرا زآیت فتح و ظفرند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آب در جنبش و تبدیل و تو خود پنداری</p></div>
<div class="m2"><p>عکس سرو و گل نسرین و چمن ره سپرند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خبر از هستی خود خلق چه جویند نشاط</p></div>
<div class="m2"><p>آب و آیینه نه در خورد خبر از صورند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باغ در سایه ی سرو سهی و دولت و دین</p></div>
<div class="m2"><p>شاد در سایهٔ شاهنشه خورشید فرند</p></div></div>