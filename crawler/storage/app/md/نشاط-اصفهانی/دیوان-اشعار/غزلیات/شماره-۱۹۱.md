---
title: >-
    شمارهٔ ۱۹۱
---
# شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>آخر این روز بشب میرسد این صبح بشام</p></div>
<div class="m2"><p>عاقل آنست که خاطر ننهد بر ایام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخت شد کار و دریغا که هوسها همه سست</p></div>
<div class="m2"><p>سوخت جان از غم و آوخ که طعمها همه خام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ره بپایان شد و دردا که ندانیم هنوز</p></div>
<div class="m2"><p>بکجا میرود این اشتر بگسسته زمام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توسن عمر ازین دشت سراسر بگذشت</p></div>
<div class="m2"><p>تا زنی چشم بهم بگذرد این یک دو سه گام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرتو مهر که در ساحت این خانه نماند</p></div>
<div class="m2"><p>شک نباشد که دوامی نکند بر لب بام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این گل تازه که سر بر زده امروز ز شاخ</p></div>
<div class="m2"><p>یک دو روز دگرش بر سر خاک است مقام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس از این انجمن حادثه سودی نبرد</p></div>
<div class="m2"><p>که ذهاب است و ایاب است و قعود است و قیام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بر باد دمادم نکند شمع ثبات</p></div>
<div class="m2"><p>در ره سیل پیاپی نکند خانه دوام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آخر این ریشه به بن آید و این تیشه بسنگ</p></div>
<div class="m2"><p>آخر این می زسبو ریزد و این شهد زجام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خیز و بفروز چراغ خرد از آتش عشق</p></div>
<div class="m2"><p>آبی از اشک بزن بر رخ و برشو زمنام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل یکی مرکب ره جوست رکابش بطلب</p></div>
<div class="m2"><p>راه این سوست نشاط از اثر دل بخرام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کوش کاین جان مقدس رهد از محبس تن</p></div>
<div class="m2"><p>تا کی این طایر فرخنده بماند در دام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کوش کاین مهر فروزان که نهان است بمیغ</p></div>
<div class="m2"><p>همچو تیغ شه آفاق بر آید ز نیام</p></div></div>