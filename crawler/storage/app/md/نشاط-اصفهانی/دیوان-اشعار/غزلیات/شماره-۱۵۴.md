---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>جوی شهد است لعل سیرابش</p></div>
<div class="m2"><p>تشنگی میفزاید از آبش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز ما نذر طره ی سیهش</p></div>
<div class="m2"><p>بخت ما وقف چشم پر خوابش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل مسکین و جعد مشکینش</p></div>
<div class="m2"><p>جان بی تاب و زلف پرتابش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیف باشد بدین لطایف حسن</p></div>
<div class="m2"><p>که نباشد بکام احبابش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشقی و ملامت این نشود</p></div>
<div class="m2"><p>که شود سیر ماهی از آبش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اولین احتمال عاشق چیست</p></div>
<div class="m2"><p>جور احباب و طعن اصحابش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل عاشق قرار کی گیرد</p></div>
<div class="m2"><p>بمتاع جهان و اسبابش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غرقه در بحر و باز مستسقی</p></div>
<div class="m2"><p>کی نماید سراب سیرابش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواجه بیهوده تن همی پرورد</p></div>
<div class="m2"><p>گاه با شهد و گه بجلابش</p></div></div>