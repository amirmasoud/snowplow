---
title: >-
    شمارهٔ ۲۳۲
---
# شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>دوش آمد ببرم می زده خواب آلوده</p></div>
<div class="m2"><p>چهره افروخته، خوی کرده، عتاب آلوده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیشه در دست و قدح بر کف و بگشوده نظر</p></div>
<div class="m2"><p>لب شکر شکن آن لعل شراب آلوده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت ای خفته ی آشفته ز اندوه جهان</p></div>
<div class="m2"><p>حیف نبود چو تویی غمزده خواب آلوده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدحی در کش و از دیده ی عفوش بنگر</p></div>
<div class="m2"><p>تا ببینی چه گنه های ثواب آلوده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر در پیر خرابات نگیرند بهیچ</p></div>
<div class="m2"><p>خرقه ای را که نباشد بشراب آلوده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رازم از پرده برافتاد و دریغا که هنوز</p></div>
<div class="m2"><p>نتوان گفت بدان طفل حجاب آلوده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سحری بیخبری گفت بگو چرخ چراست</p></div>
<div class="m2"><p>چاک بر سینه و رخساره تراب آلوده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم آمد ز در شاه نبینی که همی</p></div>
<div class="m2"><p>همچو دهشت زد گانست شتاب آلوده</p></div></div>