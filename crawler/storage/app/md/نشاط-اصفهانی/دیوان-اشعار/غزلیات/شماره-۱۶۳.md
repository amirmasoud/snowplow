---
title: >-
    شمارهٔ ۱۶۳
---
# شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>بی عشق کس بدوست نیابد ره وصول</p></div>
<div class="m2"><p>سبحان من تحیر فی ذاته العقول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر مرد این دری بدرآ کاندر این سرای</p></div>
<div class="m2"><p>دربان برای منع خروج است نی دخول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پیشگاه عشق مجال محال نیست</p></div>
<div class="m2"><p>عاشق نباشد آنکه نشیند دمی ملول</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تعجیل نیست شرط طریقت بصبر کوش</p></div>
<div class="m2"><p>کم یظفر الصبور بما یهزم العجول</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر غایبی تو هر چه ملامت کشم رواست</p></div>
<div class="m2"><p>وانجا که حاضری که دهد گوش بر عذول</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قل للعذول ویلک عنی و لا تلم</p></div>
<div class="m2"><p>ما قلت مادریت ولم تدر ما تقول</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انکار ذوق عشق ز عاقل عجیب نیست</p></div>
<div class="m2"><p>عاشق مگر روایت عاقل کند قبول</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هردم بجانبی کشدت نفس مضطرب</p></div>
<div class="m2"><p>والقلب لا یزال محبا لما یزول</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ره در مقام خلد نیایی و جای امن</p></div>
<div class="m2"><p>تا در سرای دل ندهی خویش را نزول</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کوتاه شد فسانه ی هستی ما بعشق</p></div>
<div class="m2"><p>ناصح دراز کرده سخن همچنان فضول</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من حالتی که خود بتصور نیارمش</p></div>
<div class="m2"><p>در نامه چون نویسم و گویم چه بارسول</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتی هلاک میکنم از کین نشاط را</p></div>
<div class="m2"><p>روحی فداک قد سبق الحب ما تقول</p></div></div>