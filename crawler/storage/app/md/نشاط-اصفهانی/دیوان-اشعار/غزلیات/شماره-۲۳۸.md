---
title: >-
    شمارهٔ ۲۳۸
---
# شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>دارد شب نومیدی ما صبح امیدی</p></div>
<div class="m2"><p>باد سحری میدهد از غیب نویدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غازی ز پی دشمن و ما را برخ دوست</p></div>
<div class="m2"><p>هر لحظه نگاهی و در آن اجر شهیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از لطف بنالیم و ز بیداد ننالیم</p></div>
<div class="m2"><p>کز دوست نداریم بجز دوست امیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نشکنی آگاه نگردی ز دل ما</p></div>
<div class="m2"><p>قفلیست که در وی نفتد هیچ کلیدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر تیر زنی دیده نپوشیم که باشد</p></div>
<div class="m2"><p>هر تیر بریدی ز تو هر زخم نویدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکچند نشاط از سخن بیهده بس کن</p></div>
<div class="m2"><p>ای بس که همی گفتی و ای بس که شنیدی</p></div></div>