---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>فرخنده طایری که گرفتار دام تست</p></div>
<div class="m2"><p>فرخنده تر از آن که گذارش ببام تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسوده بیدلی کخ بکویت کند مقام</p></div>
<div class="m2"><p>آسوده تر دلی که در آنجا مقام تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پای تا بفرق بکام منی ولی</p></div>
<div class="m2"><p>شادی نصیب کام کسی کو بکام تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تشریف نیستی ز تو خاصان گرفته اند</p></div>
<div class="m2"><p>هستی کاینات ز انعام عام تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با شاهدان قدس بر آیید در سماع</p></div>
<div class="m2"><p>امشب که ذکر مجلسیان از کلام تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این حسن دلفروز فروغی زبزم تو</p></div>
<div class="m2"><p>وین عشق خانه سوز شرابی زجام تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز و شب تو تا چه بود ای دیار عشق</p></div>
<div class="m2"><p>کان روی و موی آیتی از صبح و شام تست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان شب که من نویدی از آن لب شنیده ام</p></div>
<div class="m2"><p>هر جا حکایتیست بگوشم پیام تست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سد بار بیش فال زدستم بامتحان</p></div>
<div class="m2"><p>هرجا که قرعه ایست زدولت بنام تست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برخیز تا بساط نشاط است در میان</p></div>
<div class="m2"><p>جامی بزن که ساقی دوران بکام تست</p></div></div>