---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>روز آسایش و آرایش و دین و دنیاست</p></div>
<div class="m2"><p>روز افزایش و فضل و کرم و بذل و عطاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر طرف میگذری مجلس شکر است و سپاس</p></div>
<div class="m2"><p>هر کجا مینگری حلقه ی ذکر است و دعاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا دل همه خرسندی و وجداست و نشاط</p></div>
<div class="m2"><p>هر کجا دیده همه روشنی و نور و ضیاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من همه جرم و چه غم نوبت عفو است و گذشت</p></div>
<div class="m2"><p>من همه درد و چه غم وقت علاج است و دواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بجهان دگر از قید تو گفتم بر هم</p></div>
<div class="m2"><p>می ندانستم کاین سلسله از زلف دوتاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان بدشواری میدادم، غافل که همی</p></div>
<div class="m2"><p>کشتگان نگهش را غم او جان افزاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما کجا عشق کجا! ای دل و ای جان حذری</p></div>
<div class="m2"><p>ما سفاهانی و او ترک وش و بی پرواست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از جمالت بخیال تو تولا جستم</p></div>
<div class="m2"><p>کاین همی خواب رباینده و آن هوش رباست</p></div></div>