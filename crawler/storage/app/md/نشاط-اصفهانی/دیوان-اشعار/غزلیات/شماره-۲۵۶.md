---
title: >-
    شمارهٔ ۲۵۶
---
# شمارهٔ ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>در بلورین خانه عکس طلعت داراستی</p></div>
<div class="m2"><p>یا که آن نور حق استی آن دل داناستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی شاه است اینکه عکس افکنده بر کاخ بلور</p></div>
<div class="m2"><p>یا سپهر است این و آن مهر جهان آراستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سد هزاران عکس بینی چون نظر پوشی ز اصل</p></div>
<div class="m2"><p>باز چون بر اصل بینی ذات بی همتاستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عکس دیهیم و نطاق استی در این فرخنده طاق</p></div>
<div class="m2"><p>یا سپهر است این و آن اکلیل و آن جوزاستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساعد شاهد حمایل دار زیب پیکریست</p></div>
<div class="m2"><p>یا ببرج ماهی امشب ماه نور افزاستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم مست ساقی بزم است و زلف پرخمش</p></div>
<div class="m2"><p>یا که ترک چرخ برج عقربش مأواستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطرب بزم است و جزوی بر کف از شعر نشاط</p></div>
<div class="m2"><p>یا بچرخ امشب قرین ناهید با شعراستی</p></div></div>