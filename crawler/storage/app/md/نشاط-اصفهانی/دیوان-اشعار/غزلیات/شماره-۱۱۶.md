---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>راز ما خلوتیان بر سر بازار افتاد</p></div>
<div class="m2"><p>پرده بگشا ز در خانه که دیوار افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار در خلوت ما بود بسد پرده نهان</p></div>
<div class="m2"><p>پرده برداشت چواز خانه ببازار افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن خرامیدن دلجوی نگر بر لب جوی</p></div>
<div class="m2"><p>سرو آن روز بدیدش که ز رفتار افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم ایام و لیالی ندهد راه بدل</p></div>
<div class="m2"><p>هر که را کار بدان طره و رخسار افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده دور از تو نیا سوددمی، خواب چسان</p></div>
<div class="m2"><p>آید اکنون که بدیدار تواش کار افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خورشکر توام نیست بیانی چه زیان</p></div>
<div class="m2"><p>که زبانم بلقای تو زگفتار افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دام تزویر چه سازد دگر امروز نشاط</p></div>
<div class="m2"><p>سبحه ای داشت که در خانه ی خمار افتاد</p></div></div>