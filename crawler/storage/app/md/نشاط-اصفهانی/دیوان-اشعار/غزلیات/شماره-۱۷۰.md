---
title: >-
    شمارهٔ ۱۷۰
---
# شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>وقت شد وقت کزین جمع کناری گیریم</p></div>
<div class="m2"><p>برد دوست نشینیم و قراری گیریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزکی چند نظر بر رخ یاری فکنیم</p></div>
<div class="m2"><p>شبکی چند سر زلف نگاری گیریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرزوی سر از آن پا بقدومی طلبیم</p></div>
<div class="m2"><p>مقصد دیده از آن در بغباری گیریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند بیهوده توان برد بسر عمر عزیز</p></div>
<div class="m2"><p>جهدی ای دل که ازین پس پی کاری گیریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل نگذاشت تنی را بسپاهی شکنیم</p></div>
<div class="m2"><p>عشق کو عشق که ملکی بسواری گیریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صید گاهی خوش و یاران همه غافل بشتاب</p></div>
<div class="m2"><p>تا سمندی بجهانیم و شکاری گیریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوست گر دست به بیداد کند باز نشاط</p></div>
<div class="m2"><p>عشق را با هوس آنگاه عیاری گیریم</p></div></div>