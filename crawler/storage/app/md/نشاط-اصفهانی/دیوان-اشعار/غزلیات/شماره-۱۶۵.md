---
title: >-
    شمارهٔ ۱۶۵
---
# شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>این خیال خودپرستانند غافل کان جمال</p></div>
<div class="m2"><p>تا به چشمی درنیاید برنیاید در خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل گوید رب ارنی عشق می‌گوید که هی!</p></div>
<div class="m2"><p>کَیفَ اَعبُد؟ گاه من معبودم و گه ذوالجلال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کَیفَ اَعبُد را شنیدی کوش تا بینی رُخَش</p></div>
<div class="m2"><p>دیدهٔ ربّی اری گر هم طلب کن زان جمال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک خطاب آمد به عقل و عشق از دربار دوست</p></div>
<div class="m2"><p>در صماخ این تجنب در سماع آن یقال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل می‌نالد ز خویش و عشق می‌نالد ز دوست</p></div>
<div class="m2"><p>این همی‌جوید وصال و او همی‌گوید محال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من سیه‌بخت جهانم لیک در دوران شاه</p></div>
<div class="m2"><p>همچو زلف دل‌سِتانم کز وی افزاید جمال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان ستاند جان دهد جزع سیه از یک نگاه</p></div>
<div class="m2"><p>وان لب لعلی هنوز آسوده از رنج دلال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر پریشان و سیه‌بختم همی‌بینی چه باک</p></div>
<div class="m2"><p>زلف مشکین توام کز وی فزایی بر جمال</p></div></div>