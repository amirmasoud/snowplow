---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>وقت است که تن جان شود و جان همه دلدار</p></div>
<div class="m2"><p>ای خون شده دل خانه بپرداز زاغیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا شمع براهش برم ای سینه بر افروز</p></div>
<div class="m2"><p>تا گنج نثارش کنم ای دیده فروبار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر یک من و زاهد شده خرسند بکاری</p></div>
<div class="m2"><p>تا غیرت داور چه کند عاقبت کار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من پای تو میبوسم و او پایه ی منبر</p></div>
<div class="m2"><p>من دست بسر میزنم او دسته ی دستار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخ منظر غیب است بهر عیب مپوشان</p></div>
<div class="m2"><p>لب مخزن گنج است بهر رنج میازار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم از پی نظاره ی رویی ست فرو بند</p></div>
<div class="m2"><p>پا از پی سیر سرکویی ست نگه دار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل خلوت یاریست درین غمکده مپسند</p></div>
<div class="m2"><p>جان از پی کاریست چنین بیهده مگذار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا چند نشاط اینهمه بیهوده سرایی</p></div>
<div class="m2"><p>گر مرد رهی گام بنه کام بدست آر</p></div></div>