---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>چه شتابی از پی من تو که رنجه شد سمندت</p></div>
<div class="m2"><p>که من آمدم در این دشت که اوفتم به بندت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو نکو پسندی و من چه کنم که تا پسندت</p></div>
<div class="m2"><p>نشوم، نگو نگردم من و لایق کمندت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو اگر ملولی از من، سر خویشتن بگیرم</p></div>
<div class="m2"><p>من و چشم اشکبارم، تو و لعل نوشخندت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل زاهدان توانی ببری از آن نبردی</p></div>
<div class="m2"><p>که نبود صید غافل زتو، در خور کمندت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو که خسرو کریمی زمن گدا چه پرسی</p></div>
<div class="m2"><p>من و دست کوته من تو و همت بلندت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر ای دل اوفتادی به بساط لعب طفلان</p></div>
<div class="m2"><p>که بلطف می ستانند و بقهر می دهندت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو چه غمفرا نشاطی و چه بیهنر غلامی</p></div>
<div class="m2"><p>که بهیچ میفروشیم وز ما نمیخرندت</p></div></div>