---
title: >-
    شمارهٔ ۲۶۸
---
# شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>من فاش کنم غم نهانی</p></div>
<div class="m2"><p>حاشا نکنم که خود تو دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با تو بزبان چه راز گویم</p></div>
<div class="m2"><p>هم راز منی تو هم زبانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیک و بد من تو میشناسی</p></div>
<div class="m2"><p>بد راهم و نیک میتوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر شهد رواست میفرستی</p></div>
<div class="m2"><p>گر زهر سزاست میچشانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانم ببری و غم ندارم</p></div>
<div class="m2"><p>زیرا که تو خوبتر زجانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خصم تنی و حبیب روحی</p></div>
<div class="m2"><p>درد دلی و طبیب جانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کز دل شکنی تو جای شکر است</p></div>
<div class="m2"><p>کارام دل شکستگانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر گونه که خواهیم چنان کن</p></div>
<div class="m2"><p>زانگونه که خواهمت چنانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غمخوار نشاط جز تو کس نیست</p></div>
<div class="m2"><p>آخر نه تو یار بی‌کسانی</p></div></div>