---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>یا رب که چشم بد نرسد آن نگاه را</p></div>
<div class="m2"><p>وان طرز بازدیدن بیگاه و گاه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن خم بخم سلاسل مشکین پرشکن</p></div>
<div class="m2"><p>کاندر شکنج هر خمی افکنده ماه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن آستین فشاندن و آن جامه برزدن</p></div>
<div class="m2"><p>آن رسم برشکستن طرف کلاه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر دسته دسته زلف معنبر در آینه</p></div>
<div class="m2"><p>بیند چنانکه شاه مظفر سپاه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیبا میفکنید براهش که عاشقان</p></div>
<div class="m2"><p>از نقش چشم و چهره بپوشند راه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شرح دوستی بر او لب گشاده ام</p></div>
<div class="m2"><p>چون عاصیی که عذر بگوید گناه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاصان بارگاه رقیبان مدعی</p></div>
<div class="m2"><p>حال گدا که عرضه دهد پادشاه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز در گه تو راه بجایی نبرده اند</p></div>
<div class="m2"><p>از آستان خویش مران داد خواه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر لب قرین شکر تو ذکری نیاورم</p></div>
<div class="m2"><p>الا دعای خسرو گیتی پناه را</p></div></div>