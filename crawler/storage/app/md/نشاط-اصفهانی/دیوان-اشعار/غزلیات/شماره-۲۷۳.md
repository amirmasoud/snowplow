---
title: >-
    شمارهٔ ۲۷۳
---
# شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>شب تیره وره سخت، چنین سست چرایی</p></div>
<div class="m2"><p>بشتاب اگر بر اثر ناقه ی مایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاندیشه ی رهزن بود این راهبران را</p></div>
<div class="m2"><p>در دست نه شمعی و نه بر ناقه درایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رهبر ز پس قافله و راهرو از پیش</p></div>
<div class="m2"><p>تا عقل نماند نرسد عشق بجایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فردا که سر از خاک برآرند خلایق</p></div>
<div class="m2"><p>ترسم نتواند که برد راه بجایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن پا که نپیموده رهی بر سر کویی</p></div>
<div class="m2"><p>وان سر که نیاسوده دمی بر کف پایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر درد بود هست دوا، دکه ی عطار</p></div>
<div class="m2"><p>باز است و یکی نیست خریدار دوایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر هر که ستم رفت بباید کرمی کرد</p></div>
<div class="m2"><p>غم نیست اگر دید نشاط از تو جفایی</p></div></div>