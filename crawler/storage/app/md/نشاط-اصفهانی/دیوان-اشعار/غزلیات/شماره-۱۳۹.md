---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>دل از قید دو عالم رسته خوشتر</p></div>
<div class="m2"><p>بر آن زلف مسلسل بسته خوشتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بده دل با یکی پس دیده بر بند</p></div>
<div class="m2"><p>چویار آمد درون، در بسته خوشتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از این ره چون بباید باز گشتن</p></div>
<div class="m2"><p>بدین چستی مران، آهسته خوشتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توانایی تن سستی جان است</p></div>
<div class="m2"><p>قوی گو باش جان، تن خسته خوشتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنا دام دل و زندان جانی</p></div>
<div class="m2"><p>سراپایت بهم بشکسته خوشتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصال دوستان جوییم و یاران</p></div>
<div class="m2"><p>چو آن نبود، برخ در بسته خوشتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه تن،جان هم بر آن منظر حجابیست</p></div>
<div class="m2"><p>نشاط این پرده هم بگسسته خوشتر</p></div></div>