---
title: >-
    شمارهٔ ۲۱۱
---
# شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>زتست پای گریزم ز تست دست ستیزم</p></div>
<div class="m2"><p>هم از تو با تو ستیزم هم از تو در تو گریزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعجز خویش نبردیم هست با تو و نازت</p></div>
<div class="m2"><p>وگر نه خشم تو داند که من نه مرد ستیزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس از نگاه بسویم اشارتی کن از ابرو</p></div>
<div class="m2"><p>فکندیم چو به تیری به تیغ زن که نخیزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیال طره ی تو اژدهای حادثه خوار است</p></div>
<div class="m2"><p>بپاس گنج دل از رنج نفس حادثه خیزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حکایتی مگر آرم بنامه از خم زلفت</p></div>
<div class="m2"><p>ز سطر سلسله سازم ز نقطه غالیه بیزم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا ببزم حریفان ببین کرامت ساقی</p></div>
<div class="m2"><p>بجام صاف شراب و بکام خنجر تیزم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشاط را چه ملامت کنی زمهر نکویان</p></div>
<div class="m2"><p>بگو ملامت آنکس که عقل داد و تمیزم</p></div></div>