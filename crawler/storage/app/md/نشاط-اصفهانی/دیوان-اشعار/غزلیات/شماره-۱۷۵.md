---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>نوید لطف همی میرسد نهفته بگوشم</p></div>
<div class="m2"><p>چه مژده ها که همی میدهد زغیب سروشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجال نطق نمیداد دوش بانگ سروشم</p></div>
<div class="m2"><p>گمان انجمن این کز غمی ملول و خموشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرا خموش نباشم میان جمع که هر سو</p></div>
<div class="m2"><p>خیال اوست بچشمم حدیث اوست بگوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبود اثر زمن و کوششم که داد زجودش</p></div>
<div class="m2"><p>مرا وجود کنون هم روا بود که نکوشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آنچه دوست پسندد خلاف آن نپسندم</p></div>
<div class="m2"><p>اگر بر آتش سوزان نشاندم نخروشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وجود من همه چشمیست بر وجود تو حیران</p></div>
<div class="m2"><p>زبیم مدعیان گو که دیده از تو بپوشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجب مدار بپوشم نشاط اگر غم عشقش</p></div>
<div class="m2"><p>تمام سوخته ام با هزار شعله نجوشم</p></div></div>