---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>گذری باد بر آن زلف معنبر دارد</p></div>
<div class="m2"><p>بازیارب دل آشفته چه بر سر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دفتر معرفت آن به که بشوییم بجوی</p></div>
<div class="m2"><p>که درخت چمن اوراق دی از بر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در نظر بازی مژگان تو احوال دلم</p></div>
<div class="m2"><p>داند آن خسته که دل بر سر خنجر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رندبی پاو سر از کوی خرابات چه دید</p></div>
<div class="m2"><p>که جهان را بنظر سخت محقر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اختر طالع من روی فروزان تو بود</p></div>
<div class="m2"><p>هر که بینی نظری جانب اختر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ستم از لطف چسان فرق توان کرد که دوست</p></div>
<div class="m2"><p>هم به کف خنجر و هم دست به ساغر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به طبیبان جفاپیشه چه گوییم نشاط</p></div>
<div class="m2"><p>درد ما را به جز او کیست که باور دارد</p></div></div>