---
title: >-
    شمارهٔ ۲۷۷
---
# شمارهٔ ۲۷۷

<div class="b" id="bn1"><div class="m1"><p>ما را که جام نبود رنجیم کی ز سنگی</p></div>
<div class="m2"><p>آن کس که نام دارد گو رنجه شو ز ننگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ابنای دهر ما را غیر از ستم طمع نیست</p></div>
<div class="m2"><p>دیوانه‌ایم و سرخوش از کودکان به سنگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بوستان چو مزکوم در گلستان چو اعمی</p></div>
<div class="m2"><p>ماندیم روزگاری فارغ ز بو و رنگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ره فتادگانیم تا صبح سر بر آرد</p></div>
<div class="m2"><p>ای آسمان شتابی ای کاروان درنگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صید توام من ای شوخ از این و آن چه خواهی</p></div>
<div class="m2"><p>هرسو به امتحانی ضایع مکن خدنگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین پس نشاط یک چند آسوده می‌توان بود</p></div>
<div class="m2"><p>کس را بمانه صلحی ما را به کس نه جنگی</p></div></div>