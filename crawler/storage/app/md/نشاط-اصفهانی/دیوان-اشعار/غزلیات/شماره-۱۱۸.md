---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>طاعت از دست نیاید گنهی باید کرد</p></div>
<div class="m2"><p>در دل دوست به هر حیله رهی باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منظر دیده قدمگاه گدایان شده است</p></div>
<div class="m2"><p>کاخ دل درخور اورنگ شهی باید کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیغ عشق و سر این نفس مقنع بخرد</p></div>
<div class="m2"><p>زین سپس خدمت صاحب کلهی باید کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روشنان فلکی را اثری در ما نیست</p></div>
<div class="m2"><p>حذر از گردش چشم سیهی باید کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب که خورشید جهان‌تاب نهان از نظر است</p></div>
<div class="m2"><p>قطع این مرحله با نور مهی باید کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش همی می‌روی ای قافله‌سالار به راه</p></div>
<div class="m2"><p>گذری جانب گم کرده رهی باید کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه همین صف زده مژگان سیه باید داشت</p></div>
<div class="m2"><p>به صف دلشدگان هم نگهی باید کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جانب دوست نگه از نگهی باید داشت</p></div>
<div class="m2"><p>کشور خصم تبه از سپهی باید کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر مجاور نتوان بود به میخانه نشاط</p></div>
<div class="m2"><p>سجده از دور بهر صبحگهی باید کرد</p></div></div>