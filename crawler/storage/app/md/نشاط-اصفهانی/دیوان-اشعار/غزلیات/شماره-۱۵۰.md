---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>نتوان داشت نگه باز زچشم سیهش</p></div>
<div class="m2"><p>دار از چشم بد خلق خدایا نگهش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جای رحم است بر آن بندهٔ مسکین فقیر</p></div>
<div class="m2"><p>که برانند و ندانند چه باشد گنهش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگهی جانب ما دلشدگان می نکنی</p></div>
<div class="m2"><p>دل چرا امیبری از کس چو نداری نگهش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قاصد یارم و با تیرگی بخت نشاط</p></div>
<div class="m2"><p>حرفی از جعد خطش دارم و چشم سیهش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنده ی شاهم و بر روشنی چشم امید</p></div>
<div class="m2"><p>خطی از گرد رهش دارم و گرد سپهش</p></div></div>