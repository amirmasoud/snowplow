---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>ای فروغ ماه از شمع شبستان شما</p></div>
<div class="m2"><p>چشمهٔ خور جرعه‌ای در بزم مستان شما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق دارد صیدگاهی نغز و دلکش کاندر آن</p></div>
<div class="m2"><p>صید شیران می‌کند آهوی چشمان شما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف مشکین خم به خم بر طرف رو چوگان‌صفت</p></div>
<div class="m2"><p>ای دل عشاق مسکین گوی چوگان شما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل از راهی برفت و صبر در کنجی نشست</p></div>
<div class="m2"><p>آری آری عشق باشد مرد میدان شما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیل کفر و جیش اسلام آشتی جستند و باز</p></div>
<div class="m2"><p>صف به خون عاشقان بستست مژگان شما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزگار آشفتگی از سر نهادستی دگر</p></div>
<div class="m2"><p>تا چه بر سر دارد این زلف پریشان شما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتنه از ملک شهنشه رخت بیرون می‌برد</p></div>
<div class="m2"><p>پس چه خواهد کرد ازین پس چشم فتان شما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از اجل چندان امان خواهد که برگیرد نشاط</p></div>
<div class="m2"><p>بهر رضوان تحفه‌ای از خار بُستان شما</p></div></div>