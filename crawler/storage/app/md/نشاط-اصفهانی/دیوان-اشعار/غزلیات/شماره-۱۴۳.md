---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>زلف بر پا فکنده از سر ناز</p></div>
<div class="m2"><p>ما گرفتار این شبان دراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظری داشت با نظر بازان</p></div>
<div class="m2"><p>لعبت شوخ و شاهد طناز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپر از دیده بایدش آورد</p></div>
<div class="m2"><p>هر که از غمزه گشت تیرانداز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منع عاشق توان ز شاهد لیک</p></div>
<div class="m2"><p>حذر از شاهدان عاشقباز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این تذروان شوخ چشم دلیر</p></div>
<div class="m2"><p>جلوه آرند در گذر گه باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه از دوست هر چه هست نکوست</p></div>
<div class="m2"><p>ستم و لطف و خواری و اعزاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جور بر بندگان روا نمود</p></div>
<div class="m2"><p>خاصه در عهد شاه بنده نواز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنهایت حدیث ما نرسید</p></div>
<div class="m2"><p>که ملالت رسیدت از آغاز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>را ز ما بود آنکه در عالم</p></div>
<div class="m2"><p>ماند در پرده با دو سد غماز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راز دار جهانیان خاکست</p></div>
<div class="m2"><p>خاک گشتیم و ماند در دل راز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این پریشان سخن ز نظم نشاط</p></div>
<div class="m2"><p>گر قبول شهنشه افتد باز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شکر و شعرش آورند نثار</p></div>
<div class="m2"><p>توتی از هند و سعدی از شیراز</p></div></div>