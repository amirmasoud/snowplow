---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>شکرانهٔ بازوان پر زور</p></div>
<div class="m2"><p>رحمی به شکستگان رنجور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیچاره و مستمند و مسکین</p></div>
<div class="m2"><p>شاد از ستم و زجور مسرور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان‌ها به محبت تو مخلوق</p></div>
<div class="m2"><p>دل‌ها به ارادت تو مفطور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باطره ی دلفریب طرار</p></div>
<div class="m2"><p>با غمزه ی می پرست مخمور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما احسنک از تو وقت ما خوش</p></div>
<div class="m2"><p>ما الطفک از تو چشم بد دور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مفتون بتو روزگار و فتنه</p></div>
<div class="m2"><p>در دولت شهریار مقهور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاقان مؤید مظفر</p></div>
<div class="m2"><p>شاهنشه کامکار منصور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن معنی لفظ آفرینش</p></div>
<div class="m2"><p>آن حاصل کارگاه مقدور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نهیش ز قدر گرفته توقیع</p></div>
<div class="m2"><p>امرش بفلک نوشته منشور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گردونی و در جلالتش سیر</p></div>
<div class="m2"><p>خورشیدی و از عدالتش نور</p></div></div>