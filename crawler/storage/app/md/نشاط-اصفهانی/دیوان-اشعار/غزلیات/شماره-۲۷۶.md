---
title: >-
    شمارهٔ ۲۷۶
---
# شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>در عشق روانیست نه دعوی نه گواهی</p></div>
<div class="m2"><p>فرسوده دلی باید و آسوده نگاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدیم چو روی تو دگر هیچ ندیدیم</p></div>
<div class="m2"><p>بستیم نظر از همه عالم به نگاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دوزخ عشقم مگر آرند عقوبت</p></div>
<div class="m2"><p>جز هستی من نیست مرا هیچ گناهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مرحله ی عشق بسی سال که بگذشت</p></div>
<div class="m2"><p>بر ما و دریغا نرسیدیم بماهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا باز چه آرد بسرم میبرد امروز</p></div>
<div class="m2"><p>باز این دل سر گشته مرا بر سر راهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را بجز از بیکسی ای دوست کسی نیست</p></div>
<div class="m2"><p>آنرا که پناهی نبود هست پناهی</p></div></div>