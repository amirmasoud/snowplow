---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>پنجه از خون دل ماست که رنگین دارد</p></div>
<div class="m2"><p>آنکه با دست بلورین دل سنگین دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سالک اندیشه نه از کفر و نه از دین دارد</p></div>
<div class="m2"><p>وادی عشق بهر گام سد آیین دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل با عشق به بیهوده زند لاف مصاف</p></div>
<div class="m2"><p>اسب تازی چه زیان از خر چو بین دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواجه راهست غم دشمن و ما را غم دوست</p></div>
<div class="m2"><p>هر که بینی دل ار اندیشه ی غمگین دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تلخکامان غمش را نگذارد دل تنگ</p></div>
<div class="m2"><p>آنکه سد تنگ شکر در لب شیرین دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناله ی بلبلش ار خواب رباید چه عجب</p></div>
<div class="m2"><p>هر که در خانه درخت گل و نسرین دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باغبان گر در بستان بگشاید بیگاه</p></div>
<div class="m2"><p>باز گویم حذر از غارت گلچین دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا نهم سر بکف پای تو یا بر سر تیغ</p></div>
<div class="m2"><p>بی تو خاکش بسر آنکو سر بالین دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>او بر اشکم نگران من نگران بر رویش</p></div>
<div class="m2"><p>من نظر سوی مه، او جانب پروین دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نظر خواجه بر اندازه ی خدمت باشد</p></div>
<div class="m2"><p>ورنه با بنده کجا مهر و چراکین دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در همه شهر همین صاحب ما بود نشاط</p></div>
<div class="m2"><p>که بزحمت نظری با من مسکین دارد</p></div></div>