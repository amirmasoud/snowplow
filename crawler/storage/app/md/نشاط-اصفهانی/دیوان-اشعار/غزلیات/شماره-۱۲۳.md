---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>بوی جان از نفس باد صبا می‌آید</p></div>
<div class="m2"><p>یارب این باد بهاری ز کجا می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ره عاشقی اندیشه ز گمراهی نیست</p></div>
<div class="m2"><p>کز پی گمشدگان راهنما می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رحمت خواجه به تقصیر دلیرت نکند</p></div>
<div class="m2"><p>گر به پاداش خطا باز عطا می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع بردار که مه حلقه‌زنان بر در ما</p></div>
<div class="m2"><p>امشب از روی تو جویای ضیا می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاجتی دارد ازین دلشده پرسید که کیست</p></div>
<div class="m2"><p>که به هرجا که روی او ز قفا می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منزل دوست از آن سوست که می‌رفت نشاط</p></div>
<div class="m2"><p>منعمی هست به هرجا که گدا می‌آید</p></div></div>