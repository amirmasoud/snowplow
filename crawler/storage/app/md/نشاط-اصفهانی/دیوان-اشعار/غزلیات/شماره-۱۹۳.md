---
title: >-
    شمارهٔ ۱۹۳
---
# شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>من بدین ساعد سیمین که تو داری دانم</p></div>
<div class="m2"><p>که اگر تیغ زنی از تو حذر نتوانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرم تلخ فرستی به حلاوت نوشم</p></div>
<div class="m2"><p>اگرم غیب نویسی به ارادت خوانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگرم تاج دهی چاکر این درگاهم</p></div>
<div class="m2"><p>اگرم سر طلبی شاکر این فرمانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر از غم نگریزم که تویی غمخوارم</p></div>
<div class="m2"><p>دگر از درد ننالم که تویی درمانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر برانی تو یکی بند بپا مسکینم</p></div>
<div class="m2"><p>گر بخوانی تو یکی چشمه طلب عطشانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو دهقان منی گلبن رنگارنگم</p></div>
<div class="m2"><p>گر تو بستان منی بلبل خوش الحانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذوق دیدار تو بس هم دل و هم دلدارم</p></div>
<div class="m2"><p>خاک در بار تو بس هم سرو هم سامانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بروم تا بکجا لطمه ی چوگان غمت</p></div>
<div class="m2"><p>حالیا گوی صفت بر سر این میدانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سوی جانان چو نظر میفکنم جز جان نیست</p></div>
<div class="m2"><p>چون بجان مینگرم نیست بجز جانانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناصح از گفتن بیهوده مبر وقت نشاط</p></div>
<div class="m2"><p>هر چه گویی تو چنانم من و سد چندانم</p></div></div>