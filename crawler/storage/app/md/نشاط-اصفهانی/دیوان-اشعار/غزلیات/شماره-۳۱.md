---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>کشور دل از جهانی دیگر است</p></div>
<div class="m2"><p>این زمین را آسمانی دیگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جهان از راه ما بردار دام</p></div>
<div class="m2"><p>طایر ما زآشیانی دیگر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای فلک از بخت ما بر گیر رخت</p></div>
<div class="m2"><p>کوکب ما ز آسمانی دیگر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما در این راه ایمنیم از رهزنان</p></div>
<div class="m2"><p>نقد ما با کاروانی دیگر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تو خاموشم ولی با یاد دوست</p></div>
<div class="m2"><p>هر سر مویم زبانی دیگر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من نیم آن من که بودم یا مرا</p></div>
<div class="m2"><p>هر زمان از عشق جایی دیگر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد جهان بر من دگرگون یا که من</p></div>
<div class="m2"><p>این که می بینم جهانی دیگر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق دارد سد زبان و هر زمان</p></div>
<div class="m2"><p>بر زبانش داستانی دیگر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می ندانم ره بجایی برده ام</p></div>
<div class="m2"><p>یا که بازم امتحانی دیگر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما بجانان خوشدل و یاران بجان</p></div>
<div class="m2"><p>هر دلی را دلستانی دیگر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مردن ما از تن و یاران بجان</p></div>
<div class="m2"><p>هر بهاری را خزانی دیگر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میزنی از عاشقی لافی نشاط</p></div>
<div class="m2"><p>عشقبازان را نشانی دیگر است</p></div></div>