---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ای دوست بیا دمی به بالین</p></div>
<div class="m2"><p>این خون دو دیدهٔ ترم بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی که شده مقرنس عنین</p></div>
<div class="m2"><p>داماد زمانه راست کابین</p></div></div>