---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>دلا از خویشتن چون درگذشتی</p></div>
<div class="m2"><p>شوی اندر وجود دوست فانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم از غیرت ز وی کامی نجویی</p></div>
<div class="m2"><p>هم از حیرت ز وی نامی ندانی</p></div></div>