---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>حکایتیست مرا از که از کسی که بود او</p></div>
<div class="m2"><p>چه کار داری برگو بکن سوال بفرما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز اسم گویمش آری ز رسم نیز بدیده</p></div>
<div class="m2"><p>که‌ باشد او علی عسکر کنون ز شغلش‌ بسرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حجابم آ‌ید غربیله خوب نیست بیان کن</p></div>
<div class="m2"><p>برد لحاف برای که هرکه زر دهد او را</p></div></div>