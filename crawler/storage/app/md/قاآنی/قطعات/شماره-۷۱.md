---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>اگر خاموش بینی عارفی را</p></div>
<div class="m2"><p>مزن طعنش که هست آسوده از ذکر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان از پای تا سر غرق یارست</p></div>
<div class="m2"><p>که هم ذکرش فراموشست و هم فکر</p></div></div>