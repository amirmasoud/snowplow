---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>یکی به چشم تامل نگر بدین تمثال</p></div>
<div class="m2"><p>که تات مات شود دیدگان ز حیرانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی درست بدین نوجوان نگر ز نخست</p></div>
<div class="m2"><p>که‌راست ماه دو هفته است و یوسف ثانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به زلفکانش چندان که چشم کار کند</p></div>
<div class="m2"><p>همی نبیند چیزی به جز پریشانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپید سیم سرینش چو کوه بلّورست</p></div>
<div class="m2"><p>که می‌بلغزد در وی نگاه انسانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان عودش برپا بودکه پنداری</p></div>
<div class="m2"><p>ستاده گرز به کف رستم سجستانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فکنده رخش در آن عرصه‌ای که می‌بینی</p></div>
<div class="m2"><p>فشرده میخ در آن ثقبه‌ای که می‌دانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زن نجیب کهن‌سالش از قفا نگران</p></div>
<div class="m2"><p>چو پاسبان که کند دزد را نگهبانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو صرفه‌جویی و امساک عادت نجباست</p></div>
<div class="m2"><p>نجیب‌وار کند شرفهای پنهانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به شوهرش ز نجابت جماع می‌ندهد</p></div>
<div class="m2"><p>که از نجیب عجیبست فعل شهوانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قضیب شوی نخواهد به فرج خویش تمام</p></div>
<div class="m2"><p>که مال شوی نسازد تلف به نادانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غرض چه‌گویم زن از قفا چو حلقه به‌در</p></div>
<div class="m2"><p>ز پیش شوی جوان گرم حلقه‌ جنبانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان کنیزک زن راگرفته است به کار</p></div>
<div class="m2"><p>که هرکه بیند گردد ز دور شیطانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کنیزکی شهدالله ز شهد شیرین‌تر</p></div>
<div class="m2"><p>لطیف و دلکش و موزون چو شعر قاآنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تبارک‌الله فرجی دو مغزه چون بادام</p></div>
<div class="m2"><p>به شرط آنکه به بادام شکر افشانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زن نجیب وی اندر قفا یساول‌وار</p></div>
<div class="m2"><p>گرفته چوب و درافکنده چین به پیشانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنیز مطبخی از خشم نیم‌سوز به دست</p></div>
<div class="m2"><p>ستاده بر طرفی همچو دیو ظلمانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کنیزک دگر استاده گرم شکرخند</p></div>
<div class="m2"><p>زکار زانیه و فعل شوهر زانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به شهوت و غضب طبع آدمی ماند</p></div>
<div class="m2"><p>اگر تو معنی این نقشها فروخوانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو شهوت از طرفی دست عقل برتابد</p></div>
<div class="m2"><p>سپه کشد ز دگر سو قوای روحانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو نقش فانی دنیا ببین و عبرت گیر</p></div>
<div class="m2"><p>که این ستوده سخن حکمتیست لقمانی</p></div></div>