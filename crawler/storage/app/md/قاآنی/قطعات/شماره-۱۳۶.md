---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>نفس با عقل آشنا نشود</p></div>
<div class="m2"><p>زاع را نفرتست از طوطی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سفله راگر هزارگنج دهی</p></div>
<div class="m2"><p>نشود رام جز که با لوطی</p></div></div>