---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>ای برادر گرت خطایی رفت</p></div>
<div class="m2"><p>متمسک مشو به عذر دروغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کان دروغت بود خطای دگر</p></div>
<div class="m2"><p>که برد بار دیگر از تو فروغ</p></div></div>