---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>کار خود را به کردگار گذار</p></div>
<div class="m2"><p>تا ترا مصلحت بیاموزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لطف او بی‌سبب سبب سازد</p></div>
<div class="m2"><p>قهر او با سبب سبب سوزد</p></div></div>