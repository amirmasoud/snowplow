---
title: >-
    شمارهٔ ۱۳۴
---
# شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>دلاکنون چو نداری به عرش وکرسی راه</p></div>
<div class="m2"><p>کمال همت تو عرش هست یاکرسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ولی به کرسی و عرشت اگر اجازه دهند</p></div>
<div class="m2"><p>سراغ کرسی و عرش دگر همی پرسی</p></div></div>