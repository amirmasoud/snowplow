---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>هزاران مکر و فن باشد زنان را</p></div>
<div class="m2"><p>که نتواند یکی را چاره ابلیس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شود کاری چو بر ابلیس مشکل</p></div>
<div class="m2"><p>بر او آسان کنند ایشان به تلبیس</p></div></div>