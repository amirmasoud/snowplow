---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>استرم را اگر فرستادی</p></div>
<div class="m2"><p>نکنم جز به مردمی یادت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معنی آن فلان تحیاتست</p></div>
<div class="m2"><p>وان فلان روح پاک اجدادت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ورنه‌گویم که آن فلان ذکرست</p></div>
<div class="m2"><p>وان فلان مقعد پر از بادت</p></div></div>