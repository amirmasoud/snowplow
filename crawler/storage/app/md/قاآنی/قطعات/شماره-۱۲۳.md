---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>گلستانی که هر برگ گلش را</p></div>
<div class="m2"><p>هزاران گلشن خلدست بنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روان اهل معنی تا قیامت</p></div>
<div class="m2"><p>به بوی روح‌بخش اوست زنده</p></div></div>