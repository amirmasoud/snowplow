---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>کلام عاقل و جاهل به گوش یکدیگر</p></div>
<div class="m2"><p>چو نیک بنگری از روی تجربت بادست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همین به باغ ننالند بلبلان از زاغ</p></div>
<div class="m2"><p>که زاغ نیز هم از بلبلان به فریادست</p></div></div>