---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>محققست که دنیا مثال مرداریست</p></div>
<div class="m2"><p>حرام صرف بر آن کس که هست برخوردار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ولی به حکم ضرورت به سالکان طریق</p></div>
<div class="m2"><p>حلال گشته به هنگام نیستی مردار</p></div></div>