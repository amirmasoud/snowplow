---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>چون زبان راز دل نمی‌داند</p></div>
<div class="m2"><p>چیستش چاره غیر دلتنگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نداند زبان رومی را</p></div>
<div class="m2"><p>از حسد تنگدل شود زنگی</p></div></div>