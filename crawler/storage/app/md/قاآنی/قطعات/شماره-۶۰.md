---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>آوخ آوخ که مرگ نگذارد</p></div>
<div class="m2"><p>که کس اندر جهان زید جاوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه ز بهمن گذشت نز دارا</p></div>
<div class="m2"><p>نه فریدون گذاشت نه جمشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون وزد باد او به گلشن بود</p></div>
<div class="m2"><p>نخل تن بی‌ثمر شود چون بید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپس رفتگان بسی دیدیم</p></div>
<div class="m2"><p>جنبش تیر و گردش‌ ناهید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیز بی‌ ما بسی بخواهد تافت</p></div>
<div class="m2"><p>جرم مهتاب و قرصهٔ خورشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکر یزدان که مهر آل رسول</p></div>
<div class="m2"><p>دهدم بر خلود نفس نوید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به امید بزرگ بارخدای</p></div>
<div class="m2"><p>بگسلانیده‌ام ز خلق امید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه ازینم که روزگار سیاه</p></div>
<div class="m2"><p>نامه گو باش روز حشر سپید</p></div></div>