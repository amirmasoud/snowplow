---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>شهی که پردهٔ امکان اگر براندازد</p></div>
<div class="m2"><p>شناخت می‌نتواند جز ز دادارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرشته و فلک و عرن و فرش و لوح و قلم</p></div>
<div class="m2"><p>بر او سلام فرستند و آل اطهارش</p></div></div>