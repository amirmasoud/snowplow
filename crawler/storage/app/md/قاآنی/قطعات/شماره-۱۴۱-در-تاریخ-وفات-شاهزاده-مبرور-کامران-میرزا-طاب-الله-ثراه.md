---
title: >-
    شمارهٔ ۱۴۱ - در تاریخ وفات شاهزادهٔ مبرور کامران میرزا طاب‌الله ثراه
---
# شمارهٔ ۱۴۱ - در تاریخ وفات شاهزادهٔ مبرور کامران میرزا طاب‌الله ثراه

<div class="b" id="bn1"><div class="m1"><p>داد از سپهر غدّار آه از جهان فانی</p></div>
<div class="m2"><p>کان‌ حاسدیست‌ مکار وین دشمنیست‌ جانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دزد مردم‌ آزار در زیّ اهل بازار</p></div>
<div class="m2"><p>این گرک آدمی‌خوار در کسوت شبانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هریک‌ چو مار قتال زیبا و خوش خط و خال</p></div>
<div class="m2"><p>ما بیخبر ازین حال وز حیلت نهانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن هردو مار خفته ما نرم نرم رفته</p></div>
<div class="m2"><p>سرشان به‌برگرفته از روی مهربانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما بیخبرکه ناگاه نیشی زنند جانکاه</p></div>
<div class="m2"><p>کان لحظه طاقت آه نبود ز ناتوانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز انسان که‌ یکدومه‌ پیش ‌آن ‌هر دو خصم ‌بد کیش</p></div>
<div class="m2"><p>کردند سینها ریش از نیش ناگهانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صیت بلا فکندند در ری وبا فکندند</p></div>
<div class="m2"><p>سروی ز پا فکندند چون سرو بوستانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشتند کامران را شهزادهٔ جوان را</p></div>
<div class="m2"><p>کز داغ او جهان را مرگیست جاودانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم آهوی رمیده رخ میوهٔ رسیده</p></div>
<div class="m2"><p>خط سنبل دمیده لب آب زندگانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل گوهر شهامت کف لجهٔ کرامت</p></div>
<div class="m2"><p>فد معنی قیامت رخ صورت معانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خط یک سفینه عنبر لب یک خزینه‌‌گوهر</p></div>
<div class="m2"><p>تن رحمت مصوٌر رخ کوکب یمانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاقان ز فرط جودش کامی لقب نمودش</p></div>
<div class="m2"><p>کاو رنگ و مهد بودش در عهد کامرانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او رفت‌و مهد و اورنگ ‌از غم‌نشسته دلتنگ</p></div>
<div class="m2"><p>رخساره کرده گلرنگ از اشک ارغوانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون‌ در غمش ز هر تن برخاست شور و شیون</p></div>
<div class="m2"><p>چون وقت کوچ کردن غوغای کاروانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قاآنی از هلاکش شد سینه چاک چاکش</p></div>
<div class="m2"><p>گفتا برم به خاکش تاریخی ارمغانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زان‌ پس که‌ خون‌ دل‌ خورد این مصرع‌ ارمغان برد</p></div>
<div class="m2"><p>شهزاده کامران مرد نومید در جوانی</p></div></div>