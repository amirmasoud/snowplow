---
title: >-
    شمارهٔ ۱۴۴
---
# شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>چو کفر و دین حجاب رهست ای رفیق راه</p></div>
<div class="m2"><p>بگذار هر دو بگذرد ازین مایی و منی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمشیر عشق برکش و از خویش برآی</p></div>
<div class="m2"><p>آن را به دوستی کش و این را به دشمنی</p></div></div>