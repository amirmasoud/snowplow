---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>لاف طاعت چند در پیری زنی</p></div>
<div class="m2"><p>ای نکرده در جوانی هیچ کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه را در روز روشن کس‌ نجست</p></div>
<div class="m2"><p>چون توانی جست در شبهای تار</p></div></div>