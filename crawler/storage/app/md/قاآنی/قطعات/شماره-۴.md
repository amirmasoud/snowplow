---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>ای پسر درکار دنیا تا توانی دل مبند</p></div>
<div class="m2"><p>کز پی هر سود او چندین زیان آید تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند گویی شب بهل کز می دماغی تر کنم</p></div>
<div class="m2"><p>صبحدم ترسم خماری ناگهان آید تو را</p></div></div>