---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>آدمی راکاو نباشد تجربت</p></div>
<div class="m2"><p>بر چنان آدم شرف دارد ستور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌خورد مسکین نمک بر جای قند</p></div>
<div class="m2"><p>طعم شیرین را نمی‌داند ز شور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مختصر گویم به هر کاری که هست</p></div>
<div class="m2"><p>کور بینا بهتر از بینای کور</p></div></div>