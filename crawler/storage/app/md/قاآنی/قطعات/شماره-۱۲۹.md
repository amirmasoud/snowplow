---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>دایماً چون دو دست اهل دعا</p></div>
<div class="m2"><p>هر دو پایش بر آسمان بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غالباً جز به گاه وجد و سماع</p></div>
<div class="m2"><p>کف پا بر زمین نمی‌سودی</p></div></div>