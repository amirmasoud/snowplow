---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>درین‌کتاب پریشان نبینی از تربیتت</p></div>
<div class="m2"><p>عجب مدار که چون حال من پریشانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار شکر که با یک جهان پریشانی</p></div>
<div class="m2"><p>چو تار طرهٔ دلدار عنبرافشانست</p></div></div>