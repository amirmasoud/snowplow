---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>دل و جان مرد عاشق دوست دارد</p></div>
<div class="m2"><p>ولی با این دو مهرش هست چندان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که دل بگذارد اندر دست دلبر</p></div>
<div class="m2"><p>که جان بسپارد اندر پای جانان</p></div></div>