---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>طلعت مقصود چون ز پرده درآید</p></div>
<div class="m2"><p>خلق جهان را تمام پرده‌ در آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوست مگو جلوه گر شود به قیامت</p></div>
<div class="m2"><p>هست قیامت چو دوست جلوه گر آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدهٔ ما تاب آفتاب ندارد</p></div>
<div class="m2"><p>گر فکند پرده یا ز پرده برآید</p></div></div>