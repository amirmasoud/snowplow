---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>نفس شریر بدرگ غدار خیره را</p></div>
<div class="m2"><p>ازکار بد چو منع نمایی بترکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نف شریر چیست شراری که هرکجا</p></div>
<div class="m2"><p>افتاد سوز او به دگر جا اثرکند</p></div></div>