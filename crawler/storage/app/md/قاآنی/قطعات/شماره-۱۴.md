---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>مردکز عیب خویش بیخبرست</p></div>
<div class="m2"><p>هنر دیگران شمارد عیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام بیچارگان چرا شکند</p></div>
<div class="m2"><p>آنکه مینای می نهد در جیب</p></div></div>