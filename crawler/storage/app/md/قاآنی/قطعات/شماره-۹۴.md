---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>چنان بیغوله دشتی آدمی‌کش</p></div>
<div class="m2"><p>که نگذشتی در آن اندیشه از هول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تعالی‌الله بدانسان وحشت‌انگیز</p></div>
<div class="m2"><p>که شیطان اندرو می‌گفت لاحول</p></div></div>