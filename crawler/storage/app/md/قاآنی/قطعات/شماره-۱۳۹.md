---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>چون کاسه و کیسه‌‌ گشت هر دو</p></div>
<div class="m2"><p>ار باده و زرّ و سیم خالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز زهد و ورع چه چاره دارد</p></div>
<div class="m2"><p>دردی کش رند لاابالی</p></div></div>