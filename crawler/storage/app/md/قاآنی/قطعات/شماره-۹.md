---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>در سخن گفتن چو ماه و آفتاب</p></div>
<div class="m2"><p>رهنمای خلق هر صبح و مسا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدح او در گوش نادان ناگوار</p></div>
<div class="m2"><p>چون شمیم گل به مغز خنفسا</p></div></div>