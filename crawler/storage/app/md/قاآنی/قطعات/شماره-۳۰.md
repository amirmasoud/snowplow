---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>زینگونه که امروز کند خواجه تغافل</p></div>
<div class="m2"><p>گویی خبرش نیست ز فردای قیامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز مگر توبه کند چاره و گرنه</p></div>
<div class="m2"><p>فردا نپذیرند ازو عذر ندامت</p></div></div>