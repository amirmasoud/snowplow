---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>امید عیش مدار از جهان بوقلمون</p></div>
<div class="m2"><p>که هر دمش چو مخنث طبیعتان رنگیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ولی تو سخت ازین غافلی که از هر رنگ</p></div>
<div class="m2"><p>بسان مرد مخنث به دامنت ننگیست</p></div></div>