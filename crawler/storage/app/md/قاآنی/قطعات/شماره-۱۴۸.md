---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>هر آن دیار که باشد ز اهل دل خالی</p></div>
<div class="m2"><p>بود چو گوشهٔ ویرانه بدترین جایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به اختیار به ویرانه عاقلان نروند</p></div>
<div class="m2"><p>جز آن زمان که طبیعت کند تقاضایی</p></div></div>