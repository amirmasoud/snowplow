---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>صحن فلک شد سیاه بسکه ز غبرا</p></div>
<div class="m2"><p>گرد به گردون گردگرد برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشت هوا زمهریر بسکه ز هر سو</p></div>
<div class="m2"><p>از جگر گرم آه سرد برآمد</p></div></div>