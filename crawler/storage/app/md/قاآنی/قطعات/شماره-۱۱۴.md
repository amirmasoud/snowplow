---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>ای برادر جامهٔ عوری طلب</p></div>
<div class="m2"><p>کز دریدن وارهی وز دوختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم بیفشان آبی از بحرین چشم</p></div>
<div class="m2"><p>تا امان یابی به حشر از سوختن</p></div></div>