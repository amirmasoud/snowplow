---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>بخیل چون زر قلبست و پند چون آتش</p></div>
<div class="m2"><p>نه زرّ قلب ز آتش سیاه‌ترگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز حرص مال بخیلا مگو به ترک مآل</p></div>
<div class="m2"><p>از آن بترس که روزیت بخت برگردد</p></div></div>