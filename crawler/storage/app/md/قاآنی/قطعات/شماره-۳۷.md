---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>نفس کافر زنی است زانیّه</p></div>
<div class="m2"><p>که به بیگانه رام می گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسته از روزی حلال نظر</p></div>
<div class="m2"><p>پی رزق حرام می گردد</p></div></div>