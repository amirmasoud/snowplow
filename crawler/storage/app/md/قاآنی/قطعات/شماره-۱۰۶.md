---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>پیرکی لال سحرگاه به طفلی الکن</p></div>
<div class="m2"><p>می‌شنیدم که بدین نوع همی راند سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کای ز زلفت صصصبحم شاشاشام تاریک</p></div>
<div class="m2"><p>وی ز چهرت شاشاشامم صصصبح روشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تتتریاکیم و بی شششهد للبت</p></div>
<div class="m2"><p>صصصبر و تاتاتابم رررفت از تتتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طفل گفتا مَمَمَن را تُتُو تقلید مکن</p></div>
<div class="m2"><p>گگگم شو ز برم ای کککمتر از زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ممی‌خواهی مممشتی به ککلت بزنم</p></div>
<div class="m2"><p>که بیفتد مممغزت ممیان ددهن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیرگفتا وووالله که معلومست این</p></div>
<div class="m2"><p>که که زادم من بیچاره ز مادر الکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هههفتاد و ههشتاد و سه سالست فزون</p></div>
<div class="m2"><p>گگگنگ و لالالالم به‌ خخلاق زمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طفل گفتا خخدا را صصصدبار ششکر</p></div>
<div class="m2"><p>که برستم به جهان از مملال و ممحن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مممن هم گگگنگم مممثل تتتو</p></div>
<div class="m2"><p>تتتو هم گگگنگی مممثل مممن</p></div></div>