---
title: >-
    شمارهٔ ۵ - و لهُ ایضاً فی مدحه
---
# شمارهٔ ۵ - و لهُ ایضاً فی مدحه

<div class="b" id="bn1"><div class="m1"><p>بت سادهٔ رفیق بط بادهٔ رحیق</p></div>
<div class="m2"><p>مرا به ز صد حشم مرا به ز صد فریق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخواهم غذای روح به جز بادهٔ رقیق</p></div>
<div class="m2"><p>نجویم انیس دل به جز سادهٔ رفیق</p></div></div>
<div class="b2" id="bn3"><p>جو دولت یکی جوان چو دانش یکی عتیق</p></div>
<div class="b" id="bn4"><div class="m1"><p>بحمدالله از بتان مرا هست دلبری</p></div>
<div class="m2"><p>به طلعت فرشته‌ای به قامت صنوبری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رخ ماه نبخشی به قد سرو کشمری</p></div>
<div class="m2"><p>به دل سنگ خاره‌ای به تن کوه مرمری</p></div></div>
<div class="b2" id="bn6"><p>به هر آفرین سزا به هر نیکویی حقیق</p></div>
<div class="b" id="bn7"><div class="m1"><p>خطش‌ یک ‌قبیله مور رخش یک حدیقه‌ گل</p></div>
<div class="m2"><p>تنش یک دریچه نور لبش یک قنینه مل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خطش ماه را ز مشک به گردن فکنده غل</p></div>
<div class="m2"><p>لبش بر چَه ِ عدم ز یاقوت بسته پل</p></div></div>
<div class="b2" id="bn9"><p>به سرخی لبش شفق به یاران دلش شفیق</p></div>
<div class="b" id="bn10"><div class="m1"><p>خرامنده‌تر ز کبک سیه چشم‌تر ز وعل</p></div>
<div class="m2"><p>دهان نیستش وزو سخن‌ها کنند جعل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز عشق وی ابرویش در آتش فکنده نعل</p></div>
<div class="m2"><p>رخش از نژاد گل لبش از نتاج لعل</p></div></div>
<div class="b2" id="bn12"><p>یکی یک چمن شقیق یکی یک یمن عقیق</p></div>
<div class="b" id="bn13"><div class="m1"><p>نخواهم کسی گزید ازین پس به جای او</p></div>
<div class="m2"><p>که هرگز ندیده‌ام بتی با وفای او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو جاوید زنده است دلم در هوای او</p></div>
<div class="m2"><p>سزد گر به زندگی بمیرم برای او</p></div></div>
<div class="b2" id="bn15"><p>که نادر فتد ز خلق نگاری چنین خلیق</p></div>
<div class="b" id="bn16"><div class="m1"><p>چو خواهم ازو شراب دوَد گرم در وثاق</p></div>
<div class="m2"><p>صراحیّ و جام را فرود آورد ز طاق</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بریزد ز دست خویش‌ می از شیشه در ایاق</p></div>
<div class="m2"><p>پس‌ آنگاه به دست من دهد با صد اشتیاق</p></div></div>
<div class="b2" id="bn18"><p>که بر یاد لعل من بنوش این می رحیق</p></div>
<div class="b" id="bn19"><div class="m1"><p>چو من درکشم قدح سراید که نوش‌ باد</p></div>
<div class="m2"><p>به قول قلندران همه جزو هوش باد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هزار آفرین ترا به جان از سروش باد</p></div>
<div class="m2"><p>به جز در ثنای تو زبان‌ها خموش باد</p></div></div>
<div class="b2" id="bn21"><p>که شهزاده را به صدق تویی داعی صدیق‌</p></div>
<div class="b" id="bn22"><div class="m1"><p>فلک فر علیقلی که جودش بود فره</p></div>
<div class="m2"><p>برویش‌ ندیده کس مگر روز کین گره</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز سهم خدنگ او چو بیرون جهد ز زه</p></div>
<div class="m2"><p>کند ماه آسمان چو ماهی به تن زره</p></div></div>
<div class="b2" id="bn24"><p>بخندد همی ببرق سر تیغش از بریق</p></div>
<div class="b" id="bn25"><div class="m1"><p>دلش بیتی از کرم مکارم نجود او</p></div>
<div class="m2"><p>فلک رفته در رکوع ز بهر سجود او</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نماید در جهان همه شکر جود او</p></div>
<div class="m2"><p>تنی هست روزگار روانش وجود او</p></div></div>
<div class="b2" id="bn27"><p>چه در هند برهمن چه در روم جاثلیق</p></div>
<div class="b" id="bn28"><div class="m1"><p>ز رایش به مویه ماه ز جودش به ناله نیل</p></div>
<div class="m2"><p>هم از فضل بی‌منال هم از عدل بی‌عدیل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سخن‌های او بلند سخایای او جمیل</p></div>
<div class="m2"><p>کرم‌های او بزرگ عطاهای او جزیل</p></div></div>
<div class="b2" id="bn30"><p>هنرهای او شگرف نظرهای او دقیق</p></div>
<div class="b" id="bn31"><div class="m1"><p>ز رخسار شاملش زمین روضهٔ ارم</p></div>
<div class="m2"><p>ز انصاف کاملش جهان حوزهٔ حرم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به یکره چو آفتاب کفش پاشد از کرم</p></div>
<div class="m2"><p>به قدر ستارگان اگر باشدش درم</p></div></div>
<div class="b2" id="bn33"><p>محیطیست جود او دو عالم درو غریق</p></div>
<div class="b" id="bn34"><div class="m1"><p>زهی بخت حاسدت شب و روز در رقود</p></div>
<div class="m2"><p>به میزان خشم او تن دشمنان وقود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کمان از تو ممتحن چنان کز محک نقود</p></div>
<div class="m2"><p>سزد عقد جو ز هر کمند ترا عقود</p></div></div>
<div class="b2" id="bn36"><p>سزد برج سنبله دواب ترا علیق</p></div>
<div class="b" id="bn37"><div class="m1"><p>پرد تا به عون پر همی طیر در هوا</p></div>
<div class="m2"><p>دود تا بزورگام همی رخش در چرا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دمد تا به فروردین همی از زمین گیا</p></div>
<div class="m2"><p>رسد تا به بندگان ز شاهان همی عطا</p></div></div>
<div class="b2" id="bn39"><p>جهد تا به زخم نیش همی خون ز باسلیق</p></div>
<div class="b" id="bn40"><div class="m1"><p>ترا یسر در یسار ترا یمین در یمین</p></div>
<div class="m2"><p>به‌ ارزاق خاص و عام دل و دست تو ضمین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ملک گویدت ثنا فلک بوسدت زمین</p></div>
<div class="m2"><p>جهان با همه جلال ترا بندهٔ کمین</p></div></div>
<div class="b2" id="bn42"><p>خدا و رسول آل ترا هادی طریق</p></div>