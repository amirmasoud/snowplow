---
title: >-
    قصیدهٔ شمارهٔ ۹۵ - د‌ر ستایش نور حدیقه احمدی فاطمه اخت علی‌بن‌موسی علیه‌السلام
---
# قصیدهٔ شمارهٔ ۹۵ - د‌ر ستایش نور حدیقه احمدی فاطمه اخت علی‌بن‌موسی علیه‌السلام

<div class="b" id="bn1"><div class="m1"><p>ای به جلالت ز آفرینش برتر</p></div>
<div class="m2"><p>ذات تو تنها به هرچه هست برابر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زادهٔ خیرالوری رسول مکرم</p></div>
<div class="m2"><p>بضعهٔ خیرالنسا بتول مطهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تو تسلی گرفته خاطر گیتی</p></div>
<div class="m2"><p>وز تو تجلی نموده ایزد داور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم جانی و عالم دو جهانی</p></div>
<div class="m2"><p>اخت رضایی و دخت موسی جعفر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فاطمه‌ات نام و از سلالهٔ زهرا</p></div>
<div class="m2"><p>کز رخ او شرم داشت زهرهٔ ازهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای تو به حوا ز افتخار مقدم</p></div>
<div class="m2"><p>لیک ز حوا به روزگار موخر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاج ویستی و از نتاج ویستی</p></div>
<div class="m2"><p>وین نه محالست نزد مرد هنرور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای بس بابا کزو به آید فرزند</p></div>
<div class="m2"><p>ای بس ماما کز او به آید دختر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمس‌که او را عروس عالم خوانند</p></div>
<div class="m2"><p>به بود از خاوران‌که هستش مادر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوهر ناسفته‌کاوست دخترکی بکر</p></div>
<div class="m2"><p>مر صدفش مادریست دخترپرور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مادر آن را زنان برند به حمام</p></div>
<div class="m2"><p>دختر این را شهان نهند به افسر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سیم به از سنگ‌ هست و خیزد از سنگ</p></div>
<div class="m2"><p>لاله به از اغبرست و روید ز اغبر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منبر و تخت ار چه تخته‌اند ولیکن</p></div>
<div class="m2"><p>تخته نه با تخت برزند نه به منبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا که ترا نافریده بود خداوند</p></div>
<div class="m2"><p>شاهد هستی نداشت زینت و زیور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهر وجود توکرد خلقت‌گیتی</p></div>
<div class="m2"><p>کز پی روحست آفرینش پیکر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دانه نکارند جز که از پی میوه</p></div>
<div class="m2"><p>حقه نسازند جزکه از پی‌گوهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چیست مراد از سپهر گردش انجم</p></div>
<div class="m2"><p>چیست غرض از درخت میوهٔ نوبر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>علت ایجاد اگر عفاف تو بودی</p></div>
<div class="m2"><p>نقش جهان نامدی به چشم مصور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عصمتت ار پیش چرخ پرده کشیدی</p></div>
<div class="m2"><p>بر به زمین نامدی قضای مقدر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پیر خرد بد طفیل ذات تو گرچه</p></div>
<div class="m2"><p>کشت به طفلی ترا سپهر معمر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صبح صفت ناکشیده یک نفس از دل</p></div>
<div class="m2"><p>روز تو شد تیره‌تر ز شام مکدر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چشم و دل عالم و زمانه تو بودی</p></div>
<div class="m2"><p>شخص ‌تو زان خرد بود و شکل تو لاغر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لیکن چون چشم و دل بدان همه خردی</p></div>
<div class="m2"><p>هر دو جهان بود در وجود تو مضمر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عمر تو چون لفظ کاف و نون مشیت</p></div>
<div class="m2"><p>کم بد و زو زاد هرچه زاد سراسر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>صورت‌ کن را نظر مکن ‌که به معنی</p></div>
<div class="m2"><p>بود دو عالم در آن دو حرف مستر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هست ز یگ نور پاک ایزد ذوالمن</p></div>
<div class="m2"><p>ذ‌ات تو و حیدر و بتول و پیمبر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر ز یکی شمع صد چراغ فروزند</p></div>
<div class="m2"><p>نور نخستین بود که ‌گشته مکرر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ورنه چرا نورها ز هم نکنی فرق</p></div>
<div class="m2"><p>چون شود از صد چراغ خانه منور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دانه نگردد دو از تکثر خوشه</p></div>
<div class="m2"><p>شعله نگردد دو از تعدد اخگر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا تو به خاک سیاه رخ بنهفتی</p></div>
<div class="m2"><p>هیچکس این حرف را نکردی باور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کز در قدرت خدای هر دو جهان را</p></div>
<div class="m2"><p>جای دهد در دوگز زمین مقعر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چرخ شنیدم‌که خاک در برگیرد</p></div>
<div class="m2"><p>خاک ندیدم که چرخ گیرد در بر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر به ‌گل اندوده می‌نگردد خورشید</p></div>
<div class="m2"><p>چون به گل اندودت این سپهر بد اختر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پیشتر از آنکه رخ به خاک بپوشی</p></div>
<div class="m2"><p>جملهٔ ‌گلها شکفته بود معطر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون تو برفتی و رخ به‌گل بنهفتی</p></div>
<div class="m2"><p>حالت‌گلها به رنگ و بو شد دیگر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تیره شد از بسکه سوخت سینهٔ لاله</p></div>
<div class="m2"><p>خیره شد از بس‌گریست دیدهٔ عبهر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جامهٔ ماتم‌ کبود کرد بنفشه</p></div>
<div class="m2"><p>پیرهن از غصه چاک زد گل احمر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>طرهٔ سنبل شد از کلال پریشان</p></div>
<div class="m2"><p>گونهٔ خیری شد از ملال معصفر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون علوی‌زادگان به سوک تو در باغ</p></div>
<div class="m2"><p>غنچه به سر چاک زد عمامهٔ اخضر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>وز پی خدمت چو خادمان به مزارت</p></div>
<div class="m2"><p>بر سر یک پای ایستاده صنوبر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فاخته کو‌کو زنان ‌که ‌کو به‌ کجا رفت</p></div>
<div class="m2"><p>سرو دلارای باغ حیدر صفدر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گرچه نمردی و هم نمیری ازیراک</p></div>
<div class="m2"><p>‌جانی و جان را هلاک نیست مقرر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>لیک چو نامحرمست دیدهٔ عامی</p></div>
<div class="m2"><p>بکر سخن به نهفته در پس چادر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بس کن قاآنیا ثنای‌کسی را</p></div>
<div class="m2"><p>گثن ملک‌العرش مادحست و ثناگر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>عرصهٔ بحر محیط نتوان پیمود</p></div>
<div class="m2"><p>ماهیک خرد اگرچه هست شناور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>رو ببراین شعر را به رسم هدیت</p></div>
<div class="m2"><p>نزد مشیر جهان امیر مظفر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>صدر مؤید مهین اتابک اعظم</p></div>
<div class="m2"><p>کاو به ‌شرف خضر هست‌ و شاه سکندر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>عمر وی و بخت بی‌زوال شهنشه</p></div>
<div class="m2"><p>باقی و پاینده باد تا صف محشر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هم ز دعا دم مزن‌که اصل دعا اوست</p></div>
<div class="m2"><p>کش همه آمال بی‌دعاست میسر</p></div></div>