---
title: >-
    قصیدهٔ شمارهٔ ۳۴۷ - د‌ر مدح اسدالله الغالب علی‌بن ابیطالب علیه السلام و ستایش محمد شاه مرحوم
---
# قصیدهٔ شمارهٔ ۳۴۷ - د‌ر مدح اسدالله الغالب علی‌بن ابیطالب علیه السلام و ستایش محمد شاه مرحوم

<div class="b" id="bn1"><div class="m1"><p>سروش غیبم‌گوید به‌گوش پنهانی</p></div>
<div class="m2"><p>که جهل دونان خوشتر ز علم یونانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا ز حکمت یونان جز این چه حاصل شد</p></div>
<div class="m2"><p>که شبهه ‌کردی در ممکنات قرآنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو نفس علم شو از نقش علم دست بشوی</p></div>
<div class="m2"><p>که نفس علم قدیمست و نقش او فانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شناختن نتوانی هگرز یزدان را</p></div>
<div class="m2"><p>چو خود شناختن نفس خویش نتوانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در این بدن که تو داری دلی نهفته خدای</p></div>
<div class="m2"><p>که‌گنج خانهٔ عشقست و عرش رحمانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکوب حلقهٔ در را که عاقبت ز رای</p></div>
<div class="m2"><p>سری برآید چون حلقه را بجنبانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولی به گنج دلت راه نیست تا نرهی</p></div>
<div class="m2"><p>ز جهل ‌کافری و نخوت مسلمانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به‌گنج دل رسی آنگه‌که تن شود ویران</p></div>
<div class="m2"><p>که‌گنج را نتوان یافت جز به ویرانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فضول عقل رها کن‌ که با فضایل عشق</p></div>
<div class="m2"><p>اصول حکمت دانایی است نادانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به ملک عشق چه خیزد ز کدخدابی عقل</p></div>
<div class="m2"><p>کجا رسد خر باری به اسب جولانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عنان قافلهٔ دل به دست آز مده</p></div>
<div class="m2"><p>که می‌نیاید هرگز ز گرگ چوپانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بقین عشق چو آمد گمان عقل خطاست</p></div>
<div class="m2"><p>بکش چراغ چو خندید صبح نورانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرفتم آنکه نتیجه است عشق و عقل دلیل</p></div>
<div class="m2"><p>دلیل را چه کنی چون نتیجه را دانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو خود نتیجهٔ عشقی پی دلیل مگرد</p></div>
<div class="m2"><p>که نزد اهل دل این دعوی است برهانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>امل سراب غرورست زینهار بترس</p></div>
<div class="m2"><p>که نفس‌ گول تو غولی بود بیابانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مشو ز دعوت نفس شریر خود ایمن</p></div>
<div class="m2"><p>که‌ گرگ می‌نبرد گله را به مهمانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جهان دهست ‌و خرد دهخدای خرمن دوست</p></div>
<div class="m2"><p>که منتظم شود از وی اساس دهقانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>راکه دعوی شاهی بود همان بهر</p></div>
<div class="m2"><p>که روی ازین ده و این دهخدا بگردانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به هر دوکون قناعت مکن ‌کزین دو برون</p></div>
<div class="m2"><p>هزار عالم بی‌منتهاست پنهانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گمان بری که هستی کران‌پذیر بود</p></div>
<div class="m2"><p>گر این مسلم هستی به هستی ارزانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ولی من از در انصاف بی‌ستیزهٔ جهل</p></div>
<div class="m2"><p>سرایمت سخنی فهم ‌کن به‌ آسانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کران‌هستی اگر هستی است چیست سخن</p></div>
<div class="m2"><p>وگر فناست فنا را عدم چرا خوانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو ملک هستی‌ گردد به نیستی محضور</p></div>
<div class="m2"><p>نکوتر آنکه عنان سوی نیستی رانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز چهرشاهد هستی اگر نقاب افتد</p></div>
<div class="m2"><p>به یکدگر نزنی مژه را ز حیرانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر آستانهٔ عشق آن زمان دهندت بار</p></div>
<div class="m2"><p>که بر زمین و زمان آستین برافشانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مقام بوذر و سلمان گرت بود مقصود</p></div>
<div class="m2"><p>خلاص بوذر بمای و صدق سلمانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برهنه‌پا و سرانند در ولایت عشق</p></div>
<div class="m2"><p>که‌قوتشان‌همه‌جوعست و جامه عریانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همه برهنه و چون مهر عور عریان ‌پوش</p></div>
<div class="m2"><p>همه ‌گرسنه و چون علم قوت روحانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مبین بر آنکه چو زلف بتان پریشانند</p></div>
<div class="m2"><p>که همچو گیسوی جمعند در پریشانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>غلام درگه شاه ولایتند همه</p></div>
<div class="m2"><p>که در ولایت جان می‌کنند سلطانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کمال قدرت داور وصیّ پیغمبر</p></div>
<div class="m2"><p>ولیّ خالق اکبر علیّ عمرانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شهنشهی ‌که ز واجب ‌کسش نداند باز</p></div>
<div class="m2"><p>اگر برافکند از رخ حجاب امکانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از آن ‌گذشته‌ که مخلوق اولش‌ گویی</p></div>
<div class="m2"><p>بدان رسیده که خلاق ثانیش دانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به شخص قدرش هجده هزار عالم صنع</p></div>
<div class="m2"><p>بود چو چشمهٔ سوزن ز تنگ میدانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر خلیفهٔ چارم در اولش دانند</p></div>
<div class="m2"><p>من اولیش شناسم‌که نیستش ثانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>لوای ‌کوکبهٔ ذات او چوگشت پدید</p></div>
<div class="m2"><p>وجود مغترف آمد به تنگ سامانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شها تویی‌ که ندانم به دهر مانندت</p></div>
<div class="m2"><p>جز این صفت که بگویم به خویش می‌مانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به‌ گاه عفو تو عصیان بود سبکباری</p></div>
<div class="m2"><p>به وقت خشم تو طاعت بود پشیمانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چسان جهانت خوانم‌که خواجهٔ اینی</p></div>
<div class="m2"><p>کجا سپهرت دانم‌که خالق آنی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز حسن طلعت خلاق جرم خورشیدی</p></div>
<div class="m2"><p>ز فرط همت رزاق ابر نیسانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به پای عزم محیط فلک بپیمایی</p></div>
<div class="m2"><p>به دست امر عنان قضا بگردانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نه آفتاب و مهست اینکه چرخ روز شبان</p></div>
<div class="m2"><p>به طوع داغ ترا می‌نهد به پیشانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نسیم خلت تو بر دل خلیل وزید</p></div>
<div class="m2"><p>که ‌کرد آتش سوزان بر او گلستانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شد از ولای تو یوسف عزیز مصر ارنه</p></div>
<div class="m2"><p>هنوز بودی در قعر چاه زندانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نه‌گر به جودی جودت پناه بردی نوح</p></div>
<div class="m2"><p>بدی سفینهٔ او تا به حشر طوفانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>امیر خیل ملایک کجا شدی جبریل</p></div>
<div class="m2"><p>اگر نکردی بر درگه تو دربانی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ازین قبل ‌که چو خشم تو هست شورانگیز</p></div>
<div class="m2"><p>حرام گشته در اسلام راح ریحانی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>وزان‌سب‌که‌چو مهر توهست‌راحت‌بخ</p></div>
<div class="m2"><p>به دل قرارگرفتست روح حیوانی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز موی موی عرق ریزدم به مدحت تو</p></div>
<div class="m2"><p>که خجلت آرد در مدح تو سخندانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چنان به مهر تو مسظهرم‌که شاه جهان</p></div>
<div class="m2"><p>به ذات پاک تو آثار صنع یزدانی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خدایگان ملوک جهان محمد شاه</p></div>
<div class="m2"><p>که در محامد او عقل‌کرده حسّانی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به روز کینه‌ که پیکان ز خون نماید لعل</p></div>
<div class="m2"><p>ز خاک خیزد تا حشر لعل پیکانی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شها تویی‌ که از آن‌سوی طاق‌ کیوانست</p></div>
<div class="m2"><p>رواق شوکت تو از بلند ایوانی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به طلعت تو کند خاک تیره خورشیدی</p></div>
<div class="m2"><p>به هیبت تو کند آب صاف سوهانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به روز میدان ببر زمانه او باری</p></div>
<div class="m2"><p>به صدر ایوان ابر ستاره بارانی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هماره تاکه برونست از تصّور عقل</p></div>
<div class="m2"><p>کمال قدرت یزدان و صنع سبحانی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بدوست ملک‌سپاریّ و مملکت‌بخشی</p></div>
<div class="m2"><p>ز خصم ‌گنج بگیری و مال بستانی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به خوبش حتم‌کند آسمان‌که ختم‌کند</p></div>
<div class="m2"><p>سخا به شاه و سخن بر حکیم قاآنی</p></div></div>