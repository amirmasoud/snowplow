---
title: >-
    قصیدهٔ شمارهٔ ۱۵۲ - د‌ر ستایش شیراز صانه‌لله عن الاعواز و اعیان آن و تخلص به مدح معتمدالدوله منوچهرخان طاب ثراه گوید
---
# قصیدهٔ شمارهٔ ۱۵۲ - د‌ر ستایش شیراز صانه‌لله عن الاعواز و اعیان آن و تخلص به مدح معتمدالدوله منوچهرخان طاب ثراه گوید

<div class="b" id="bn1"><div class="m1"><p>تبارک‌الله از فارس آن خجسته دیار</p></div>
<div class="m2"><p>که می‌نبیند چون آن دیار یک دیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به زیر بقعهٔ‌ گردون به روی رقعهٔ خاک</p></div>
<div class="m2"><p>ندیده دیدهٔ بینا چنان خجسته دیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی ندیده در آفاق اینچنین معمور</p></div>
<div class="m2"><p>به هیچ عصری از اعصار مصری از امصار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسیم او همه دلکش‌تر از نسیم بهشت</p></div>
<div class="m2"><p>هوای او همه خرم‌تر از هوای بهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز لاله هر دمن اوست ‌کوهی از یاقوت</p></div>
<div class="m2"><p>ز سبزه هر چمن اوست کانی از زنگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حدایقش زده پهلو بهشت باغ بهشت</p></div>
<div class="m2"><p>ز گونه گونه فواکه ز گونه گونه ثمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بسکه زمزمهٔ سار خیزد از هامون</p></div>
<div class="m2"><p>ز بسکه قهقههٔ کبک آید از کهسار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فضای دشت پر از صوتهای موسیقی</p></div>
<div class="m2"><p>هوای‌کوه پر از لحنهای موسیقار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز رنگ‌ریزی ابر بهار در هامون</p></div>
<div class="m2"><p>ز مشک‌ بیزی باد ربیع درگلزار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هزار طعنه دمن را به دکهٔ صباغ</p></div>
<div class="m2"><p>هزار خنده چمن را به‌کلبهٔ عطار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز هرکرانه پری‌ پیکران ‌گروه ‌گروه</p></div>
<div class="m2"><p>ز هر کنار قمرطلعتان قطار قطار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو جسم وامق در تاب زلفشان ز نسیم</p></div>
<div class="m2"><p>چو بخت‌عاشق‌درخواب‌چشمشان‌ز خمار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز رشک خامهٔ صورتگران شیرازش</p></div>
<div class="m2"><p>روان مانی و لوشاست جفت عیب و عوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز هر چه عقل تصور کند در او موجود</p></div>
<div class="m2"><p>ز هرچه وهم تفکرکند در آن بسیار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه صنایع چینش به صحن هر دکان</p></div>
<div class="m2"><p>همه طرایف رومش به طرف هر بازار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به صدهزار چمن نیست یک‌هزار و در او</p></div>
<div class="m2"><p>به شاخ هرگل در هر چمن هزار هزار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به خاک او نتوان پا نهاد زانکه بود</p></div>
<div class="m2"><p>ز انبیا و رسل اندرو هزار هزار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زهی سفید حصارش ‌که نافریده خدای</p></div>
<div class="m2"><p>چنان حصاری در زیر این‌ کود حصار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به‌گرمسیر نخیلات او به وقت ثمر</p></div>
<div class="m2"><p>بسان پیران خم‌گشته از گرانی بار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز هر نهال برومندش آشکار ترنج</p></div>
<div class="m2"><p>بسان‌ گوی زنخ بر فراز قامت یار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نهال گوی زر آورده بار از نارنج</p></div>
<div class="m2"><p>حدیقه‌ کرده روان جوی سیم از انهار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکی به شکل چو بر خط استوا خورشید</p></div>
<div class="m2"><p>یکی به وضع چو در صحن آسمان سیار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جبال شامخه‌اش با سپهر نجوی گوی</p></div>
<div class="m2"><p>چو عاشقی‌که‌کند راز دل به یار اظهار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به باغ و راغش هر گوشه صد بساط نشاط</p></div>
<div class="m2"><p>-‌- ماه و مهرش هر “یو هزار جام عقار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز عکس ساقی و رنگ شراب و طلعت ‌گل</p></div>
<div class="m2"><p>پیاله‌ گشته به هرگوشه مطلع الانوار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز بس قلاع و صیاصی ز بس بقاع و قصور</p></div>
<div class="m2"><p>ز بس مراع و مواشی ز بس ضیاع و عقار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به ساحتش نبود شخص را مجال‌ گذر</p></div>
<div class="m2"><p>به عرصه‌اش نبود مرد را طریق‌گذار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صوامعش چو ارم‌ گشته ‌کعبهٔ اشراف</p></div>
<div class="m2"><p>مساجدش چو حرم‌گشته قبلهٔ ابرار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>منابرش چو فلک مرتقای خیل ملک</p></div>
<div class="m2"><p>معابرش چو افق ملتقای لیل و نهار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز بسکه عارف و عامی بر آن کنند صعود</p></div>
<div class="m2"><p>ز بسکه رومی و زنگی درین شوند دوچار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>منجمانش بی‌رنج زیج و اسطرلاب</p></div>
<div class="m2"><p>ز ارتفاع تقاویم و اختران هشیار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ندیده نبض حکیمانش ازکمال وقوف</p></div>
<div class="m2"><p>خبر دهند ز رنج نهان هر بیمار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>محاسبانش زآغاز آفرینش خلق</p></div>
<div class="m2"><p>شمار خلق توانند تا به روز شمار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز لحن مرثیه‌خوانان او گدازد سنگ</p></div>
<div class="m2"><p>چو جسم عاشق بیدل ز دوری دلدار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هزار محفل و در هر یکی هزار ادیب</p></div>
<div class="m2"><p>هزار مدرس و در هریکی هزار اسفار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز صرف و نحو و بدیع و معانی و امثال</p></div>
<div class="m2"><p>بیان و فقه و اصول و ریاضی و اخبار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز جفر و منطق و تجوید و رمل و اسطرلاب</p></div>
<div class="m2"><p>نجوم و هیات و تفسیر و حکمت و آثار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یکی نکات طبیعی همی‌کند تعلیم</p></div>
<div class="m2"><p>یکی رموز الهی همی‌کند تکرار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یکی نوشته بر اشکال هندسی برهان</p></div>
<div class="m2"><p>یکی نموده ز قانون فلسفی اظهار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یکی سراید کاینست رای اقلیدس</p></div>
<div class="m2"><p>یکی نگارد کاینست گفت بهمنیار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بویژه حضرت نواب آسمان بواب</p></div>
<div class="m2"><p>محیط دانش و کان سخا و کوه وقار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به هر هنر بود از اهل هر هنر ممتاز</p></div>
<div class="m2"><p>چو ازگروه بنی هاشم احمد مختار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تبارک از اسدالله خان جهان هنر</p></div>
<div class="m2"><p>که هست اهل هنر را به ذاتش استظهار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گرش دو دیدهٔ ظاهرنگر برون آورد</p></div>
<div class="m2"><p>به نوک ‌گزلک تقدیر چرخ بد هنجار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به نور مردمک چشم معرفت بیند</p></div>
<div class="m2"><p>سواد سرّ سویدای مور در شب تار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هزار چشم نهان‌بین خدای داده بدو</p></div>
<div class="m2"><p>که خیره‌اند ز بیناییش الوالابصار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زهی وزیر سخندان‌ که نوک خامهٔ او</p></div>
<div class="m2"><p>مشیر ملک بود بی‌زبان و بی‌گفتار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>قلمش را دو زبانست و صدهزار زبان</p></div>
<div class="m2"><p>به یک زبانی او یک‌زبان‌کنند اقرار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بود دو گوهر بکتاش در یسار و یمین</p></div>
<div class="m2"><p>چو مهر و ماه روان بالعشی و الابکار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>یکی یگانه به تدبیر همچو آصف جم</p></div>
<div class="m2"><p>یکی‌ گزیده به شمشیر همچو سام سوار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زکلک لاغر آن نیکخواه‌گشته سمین</p></div>
<div class="m2"><p>ز گرز فربه این بدسگال گشته نزار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هم از عنایت داماد او عروس سخن</p></div>
<div class="m2"><p>هزار طعنه زند بر عرایس ابکار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به دست اوست گه جود خامه در جنبش</p></div>
<div class="m2"><p>بدان مثابه که ماهی شنا کند به بحار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خهی وصال سخندان‌ که گشته نقد سخن</p></div>
<div class="m2"><p>به سعی صیرفی طبع او تمام عیار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گذشته نثرش از نثره شعرش از شعری</p></div>
<div class="m2"><p>ولی نه نثر دثارش بود نه شعر شعار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نه یک شعیر به شعرش کسی فشانده صله</p></div>
<div class="m2"><p>نه یک پشیز به نثرش کسی نموده نثار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به ‌هفت‌ خط جهان ‌رفته صیت هفت خطش</p></div>
<div class="m2"><p>ولی ز هفت خطش‌ نست حظّ یک دینار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کلامش آب روانست و طبعش از حیرت</p></div>
<div class="m2"><p>نشسته بر لب آب روان چو بوتیمار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اگر کمال بود عیب ‌کاش می‌افزود</p></div>
<div class="m2"><p>به عیب او و به عیب من ایزد دادار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز ایلخان نکنم وصف زانکه بحر محیط</p></div>
<div class="m2"><p>شناورش به شنا ره نمی‌برد به‌ کنار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز دود مطبخ جودش سپهر گشته‌ کبود</p></div>
<div class="m2"><p>ز گرد توسن قهرش هوا گرفته غبار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گرش به من نبود التفات باکی نیست</p></div>
<div class="m2"><p>که نیست در بر خورشید ذره را مقدار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>برادر و پسرش را چگونه وصف‌کنم</p></div>
<div class="m2"><p>که مرگ خواهد از بیم تیغشان زنهار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>یکی به یمن بمبنن زمانه خورده یمین</p></div>
<div class="m2"><p>یکی ز یسر یسارش ستاره برده یسار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>یک از هزار نگویم به صدهزار زبان</p></div>
<div class="m2"><p>ثنای حضرت به گلبرگی خطهٔ لار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ز بسکه لؤلؤ ریزد ز طبع لؤلؤ خیز</p></div>
<div class="m2"><p>ز بسکه ‌گوهر ریزد ز دست‌ گوهربار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>حساب آن نتوان ‌کرد تا به روز حساب</p></div>
<div class="m2"><p>شمار آن نتوان یافت تا به روز شمار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>زهی‌ کلانتر دانا که طوطی قلمم</p></div>
<div class="m2"><p>به ‌گاه شکرش شکر فشاند از منقار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چه مدح‌گویم از میر بهبهان‌که بود</p></div>
<div class="m2"><p>به خوان همت او روزگار خوان ‌سالار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>اگرچه دیر بپیوست با امیر جهان</p></div>
<div class="m2"><p>ولی ز خدمت او زود نگسلد چون تار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ز شیخ بندر هستم به ناله چون تندر</p></div>
<div class="m2"><p>که داردم ز حقارت وقار آن چو حقار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>دو دست اوست دو دریا و من ز حسرت آن</p></div>
<div class="m2"><p>همی ز دیده دو دریا روان‌کنم به‌کنار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>زهی وکیل‌ که چون نفخ صور موتی را</p></div>
<div class="m2"><p>دهد ز صیت سخا جان به جسم دیگربار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ز خان جهرم اگر باشدم هزار زبان</p></div>
<div class="m2"><p>یک از هزارکنم‌وصف و اندک از بسیار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز فیض صحبت خان نفر نفور نیم</p></div>
<div class="m2"><p>که زنگ غم بزداید به صیقل افکار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چه مدح‌ گویم از حکمران حومه ‌که هست</p></div>
<div class="m2"><p>یگانه‌گوهری از صلب حیدرکرار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>محمد آنکه ورا بود عاقبت محمود</p></div>
<div class="m2"><p>به عون احمد مختار و سید ابرار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ز قدح فارس مرا قدح کرد و گفت مگرد</p></div>
<div class="m2"><p>به‌گرد دایرهٔ عیب یک جهان احرار</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به عرق خویش ازین بیش نیش طعن مزن</p></div>
<div class="m2"><p>که آخرت عرق شرم ریزد از رخسار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>کلامت آب روان است و این عجب که مرا</p></div>
<div class="m2"><p>نشست ز آب روانت به دل غبار نقار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ز قدح پارس چو بر گردنت بود تقصیر</p></div>
<div class="m2"><p>ز درّ مدحش بر گردنت سزد تقصار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بویژه اکنون‌ کز عدل حکمران جهان</p></div>
<div class="m2"><p>شدس حیرت‌کشمبر و غیرت فرخار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>جناب معتمدالدوله‌ کز سحاب‌ کفش</p></div>
<div class="m2"><p>بود هماره در آزار ابر در آذار</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ز بحر جودش جوییست لجهٔ عمّان</p></div>
<div class="m2"><p>ز جیب حلمش گویی‌ست گنبد دوار</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>سپهر و هرچه درآن نقطه حکم او چنبر</p></div>
<div class="m2"><p>جهان و هرکه درو بنده قدر او سالار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ستاره کیست که از امر او کند اعراض</p></div>
<div class="m2"><p>زمانه چیست ‌که بر حکم او کند انکار</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>زهی ز صاعقهٔ تیغ آسمان رنگت</p></div>
<div class="m2"><p>بسان رعد خروشان پلنگ درکهسار</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>به مهد عدل‌تو در خواب امن رفته جهان</p></div>
<div class="m2"><p>ولیک بخت تو چون پاسبان بود بیدار</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>خلاف با تو بود آن گنه که توبهٔ آن</p></div>
<div class="m2"><p>قبول می‌نشود با هزار استغفار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بزرگوارا امیرا مرا یکی خانه است</p></div>
<div class="m2"><p>که تنگ‌تر بود از چشم مور و دیدهٔ مار</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>به سطح آن نتوان‌ کرد رسم دایره زانک</p></div>
<div class="m2"><p>ز بسکه تنگ نگردد به هیچ سو پرگار</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>شود چو پای ملخ رویشان خراشیده</p></div>
<div class="m2"><p>اگر دو پشه نمایند اندر آن پیکار</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>از آن سبب‌که ز ضیق فضا و تنگی جای</p></div>
<div class="m2"><p>همی خورند ز هر گوشه بر در و دیوار</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>درو دو موش ملاقی شوند اگر با هم</p></div>
<div class="m2"><p>ز هم‌گذشت نیارند از یمین و یسار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>به جایگاه ملاقات جان دهند آخر</p></div>
<div class="m2"><p>کشان نه راه‌گریزست و نه مجال‌گذار</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>وگر دو مور در او از دو سوکنغد عبور</p></div>
<div class="m2"><p>زنند قرعه و بر یکدگر شوند سوار</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>از آن سبب که در آن تنگنایشان نبود</p></div>
<div class="m2"><p>نه رهگذار فرار و نه جایگاه قرار</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چهارده تن در خانه‌یی بدین تنگی</p></div>
<div class="m2"><p>که نیک تنگ‌ترست از دهان ترک تتار</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>به روی یکدگر افتاده‌ایم پیر و جوان</p></div>
<div class="m2"><p>چنانکه چین به رخ پیر و خم به زلف نگار</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ولی دو خانه بود در جوار آن خانه</p></div>
<div class="m2"><p>که زنده دارد ما را به یمن قرب جوار</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>وسیع ‌چون‌ دل دانا‌ گشاده چون رخ دوست</p></div>
<div class="m2"><p>به خرمی چو بهشت و به تازگی چو نگار</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>گر آن دو خانه یکی را به نقد بستانم</p></div>
<div class="m2"><p>به نقد می‌نشوم با هزار غصه دوچار</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>بزرگوارا کردم شکایتی زین پیش</p></div>
<div class="m2"><p>ز اهل فارس که شادان زیند و برخوردار</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>به هجو و هذیان بستند بر من این بهتان</p></div>
<div class="m2"><p>کسان کشان نبود فهم معنی اشعار</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>کنون به عذر هجای نکرده بسرودم</p></div>
<div class="m2"><p>مر این قصیده‌ که دارد به مدحشان اشعار</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>قسم به حشمت و جاه توگر همی جویم</p></div>
<div class="m2"><p>ز هیچ‌کس به جهان عیب خاصه از اخیار</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>ولی ز هرکه‌ گزندی رسد به خاطر من</p></div>
<div class="m2"><p>به‌تیغ هجو برآرم زجسم و جانش دمار</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>بود به‌کام تو یارب مدار هفت سپهر</p></div>
<div class="m2"><p>کند به‌گرد مدر تا سپهر پیر مدار</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>تبارک الله از فکر بکر قاآنی</p></div>
<div class="m2"><p>که جان حاسد از ابکار او بود افکار</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>خطای شعرش چون صبر عاشقان اندک</p></div>
<div class="m2"><p>قبول نظمش چون جور دلبران بسیار</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>قوافی سخنش هست چون ثنای امیر</p></div>
<div class="m2"><p>که طبع را ننماید ملول از تکرار</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>و یا عطای امیرست‌کز اعادهٔ او</p></div>
<div class="m2"><p>ز جان سائل مسکین برون برد تیمار</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>جهان جود موچهر خان‌که انگیزد</p></div>
<div class="m2"><p>به‌ گاه خشم ز آب آتش و ز باد بخار</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>همیشه خرگه اقبال و شوکتش را باد</p></div>
<div class="m2"><p>امل طناب و فلک قبه و زمین مسمار</p></div></div>