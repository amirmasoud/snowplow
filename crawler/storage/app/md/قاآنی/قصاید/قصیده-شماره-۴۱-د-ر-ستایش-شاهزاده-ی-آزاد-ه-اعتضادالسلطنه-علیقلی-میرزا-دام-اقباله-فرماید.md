---
title: >-
    قصیدهٔ شمارهٔ ۴۱ - د‌ر ستایش شاهزاده ی آزاد‌ه اعتضادالسلطنه علیقلی میرزا دام اقباله فرماید 
---
# قصیدهٔ شمارهٔ ۴۱ - د‌ر ستایش شاهزاده ی آزاد‌ه اعتضادالسلطنه علیقلی میرزا دام اقباله فرماید 

<div class="b" id="bn1"><div class="m1"><p>تا لاله به باغ و گل به ‌گلزارست</p></div>
<div class="m2"><p>میخواره ز زهد و توبه بیزارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر لاله به بانگ چنگ می‌خوردن</p></div>
<div class="m2"><p>عصیان‌گذشته را ستغفارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امروز نشاط مل به از دی بود</p></div>
<div class="m2"><p>و امسال صفای ‌گل به از پارست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوروز و جنون من به یک فصلست</p></div>
<div class="m2"><p>نیسان و نشاط من به یکبارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درکام‌ کهینه جرعه‌ام رطلست</p></div>
<div class="m2"><p>بر نام مهینه قرعه‌ام یارست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایمان بِهِلم‌ که نوبت ‌کفرست</p></div>
<div class="m2"><p>سبحه بدرم که وقت زنارست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی جامی ‌که عشرتم خامست</p></div>
<div class="m2"><p>مطرب زیری‌ که حالتم زارست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می از چه نمی‌خوری مگر ننگست</p></div>
<div class="m2"><p>بوس از چه نمی‌دهی مگر عارست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من شیخ نوان بدل ندارم دوست</p></div>
<div class="m2"><p>تا شوخ جوان ماه رخسارست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تسبیح ببر که در کفم بندست</p></div>
<div class="m2"><p>دستار مهل که بر سرم بارست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می ده ‌که نسیم سبزه در مغزم</p></div>
<div class="m2"><p>مشکین نفحات زلف دلدارست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برخیز و‌ یکی به بوستان بخرام</p></div>
<div class="m2"><p>کش سبزه بهشت و جوی انهارست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برگرد سمن بنفشگان بینی</p></div>
<div class="m2"><p>پیرامن رو‌ز از شب تارست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گل دایره‌یی ز لعل و بلبل را</p></div>
<div class="m2"><p>دو پای برو به شکل پرگارست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن بلبلکان نگرکشان در حلق</p></div>
<div class="m2"><p>بی‌صنعت خلق بربط و تارست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وان بربط و تار ایزدیشان را</p></div>
<div class="m2"><p>حاجت نه به زیر و بم او تارست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>و آن قمریکان‌ که شغلشان بر سرو</p></div>
<div class="m2"><p>چون موزونان نشید اشعارست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وان سنبلکان‌که بویشان در مغز</p></div>
<div class="m2"><p>گویی به دل گلاب عطارست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وان نرگسکان چو حوضی از بلور</p></div>
<div class="m2"><p>کش زرد فواره‌یی ز دینارست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یاگرد یکی طبقچهٔ زرین</p></div>
<div class="m2"><p>کوبیده ز نقره هفت مسمارست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>و آن شاخهٔ ارغوان‌ که ترکیبش</p></div>
<div class="m2"><p>چون مژهٔ عاشقان خونبارست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یا پاره‌یی از عقیقکان خرد</p></div>
<div class="m2"><p>کز ساعد شاهدی پدیدارست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وان نیلوف که چون رسن بازان</p></div>
<div class="m2"><p>بی‌لنگر بر رسنش رفتارست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر بام رود به ریسمان‌گویی</p></div>
<div class="m2"><p>دزدست و ‌کمندگیر ‌و طرارست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>و آن خیری زردبین که از خردیش</p></div>
<div class="m2"><p>رنج یرقان عیان ز رخسارست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نرگس از ساق خود عصا گیرد</p></div>
<div class="m2"><p>مسکین چکند هنوز بیمارست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وان غنچه به طفل هاشمی ماند</p></div>
<div class="m2"><p>کاو را ز حریر سبز دستارست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از بیم همی به زیر لب خندد</p></div>
<div class="m2"><p>کش خار رقیب سان پرستارست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شَعیای پیمبرست پنداری</p></div>
<div class="m2"><p>کش اره به سر نهاده از خارست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یا طوطیکی به خاربن خفته</p></div>
<div class="m2"><p>کش زمرد بال و لعل منقارست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بیرنگ ز صنع خامهٔ قدرت</p></div>
<div class="m2"><p>بس صورت گونگون نمودارست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه سرخی لالگان ز شنگرفست</p></div>
<div class="m2"><p>نه سبزی سبزگان ز زنگارست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای ترک به فصلی این چنین ما را</p></div>
<div class="m2"><p>دانی که شراب و بوسه درکارست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در خوردن باده این‌چه تعطیلست</p></div>
<div class="m2"><p>در دادن بوسه این چه انکارست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ها باده بخور بهار در پیش است</p></div>
<div class="m2"><p>هی بوسه بده خدای غفارست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پرسی همه دم ‌که بوسه می‌خواهی</p></div>
<div class="m2"><p>می‌خواهم آخر این چه اصرارست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گویی همه دم‌ که باده می‌نوشی</p></div>
<div class="m2"><p>می‌نوشم آری این چه تکرارست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>می ده که شبست و جمله در خوابند</p></div>
<div class="m2"><p>جز بخت خدایگان که بیدارست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شهزاده علیقلی‌ که از فرهنگ</p></div>
<div class="m2"><p>قاموس علوم و کنز اسرارست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فخریست ازان سبب لقب او را</p></div>
<div class="m2"><p>کش فخر به نه سپهر دوارست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چرخ ارچه بلند پیش او پستست</p></div>
<div class="m2"><p>سیم ار چه عزیز نزد او خوارست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جز آنکه به بذل‌ گنج مجبورست</p></div>
<div class="m2"><p>در هرچه‌گمان برند مختارست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>روحیست کش از عقول اجسامست</p></div>
<div class="m2"><p>نوریست‌ کش از قلوب ابصار ست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بیند به سرایر آنچه آمالست</p></div>
<div class="m2"><p>داند به ضمایر آنچه افکارست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>رویش به بها چو لمعهٔ نورست</p></div>
<div class="m2"><p>رایش به ذکا چو شعلهٔ نارست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ای جان جهان که خنجرت جسمیست</p></div>
<div class="m2"><p>کش نصرت و فتح و فال و مقدارست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گویی که ز صلب آسمان زاده</p></div>
<div class="m2"><p>شمشیر کج تو بس که خو‌نخوارست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>آنانکه سفر کنند در دریا</p></div>
<div class="m2"><p>گو‌یند به بحر کوه بسیارست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>من گر ز تو چون به دست تو دیدم</p></div>
<div class="m2"><p>دانستم کاین حدیث ستوارست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>لیکن نشنیده بودم از مردم</p></div>
<div class="m2"><p>بحری که مقام او به‌ کهسارست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بر کوههٔ زین چو دیدمت‌ گفتم</p></div>
<div class="m2"><p>بر کوه نشسته بحر زخارست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گر خصم ترا بود سرافرازی</p></div>
<div class="m2"><p>یا بر سر نیزه یا سر دارست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بازست پی سوال در پیشت</p></div>
<div class="m2"><p>هر دستی اگر چه برگ اشجارست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>قوس است و بال تیر و تیر تو</p></div>
<div class="m2"><p>در قول و بال خصم غدارست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>وین ط‌رفه‌ که قطب ساکنست و او</p></div>
<div class="m2"><p>قطب ظفرست و نیک سیارست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بزم تو سزد مقام قاآنی</p></div>
<div class="m2"><p>علیین جایگاه ابرارست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تا بار خدا یکست و عالم دو</p></div>
<div class="m2"><p>تا دخترکان‌ سه مامکان چارست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>پنج و شش نرد حکم هفت اقلیم</p></div>
<div class="m2"><p>چون هشت جنان ترا سزاوارست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نه‌ گردون وقف ده حواست باد</p></div>
<div class="m2"><p>تا سهلترین‌کسوری اعشارست</p></div></div>