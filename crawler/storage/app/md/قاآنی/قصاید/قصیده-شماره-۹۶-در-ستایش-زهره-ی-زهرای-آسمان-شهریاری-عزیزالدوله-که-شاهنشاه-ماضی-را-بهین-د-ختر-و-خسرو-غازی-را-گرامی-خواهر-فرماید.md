---
title: >-
    قصیدهٔ شمارهٔ ۹۶ - در ستایش زهره ی زهرای آسمان شهریاری عزیزالدوله که شاهنشاه ماضی را بهین د‌ختر و خسرو غازی را گرامی خواهر فرماید
---
# قصیدهٔ شمارهٔ ۹۶ - در ستایش زهره ی زهرای آسمان شهریاری عزیزالدوله که شاهنشاه ماضی را بهین د‌ختر و خسرو غازی را گرامی خواهر فرماید

<div class="b" id="bn1"><div class="m1"><p>ای طرهٔ مشکین تو همشیرهٔ قنبر</p></div>
<div class="m2"><p>وی خال سیه‌ فام تو نوباوهٔ عنبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دنبالهٔ ابروی تو در چنبر گیسو</p></div>
<div class="m2"><p>چون قبضهٔ شمشیر علی درکف قنبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر چهرهٔ تو طرهٔ مشکین تو گویی</p></div>
<div class="m2"><p>استاده بلال حبشی پیش پیمبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من چشم به زلفت نکنم باز که ترسم</p></div>
<div class="m2"><p>چشمم چو زره پر شود از حلقه و چنبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیسوی تو بر قامت رعنای تو گویی</p></div>
<div class="m2"><p>ماری سیه آویخته از شاخ صنوبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنهار که ‌گوید که پری بال ندارد</p></div>
<div class="m2"><p>اینک رخ خوب تو پری زلف تواش پر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرسی همی از من‌ که لب من به چه ماند</p></div>
<div class="m2"><p>قندست لب لعل توگفتیم مکرر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواهم شبکی با تو به‌ کنجی بنشینم</p></div>
<div class="m2"><p>جایی ‌که در آنجا نبود جز می و ساغر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر کف قدحی باده‌ که امی ز فروغش</p></div>
<div class="m2"><p>برخواند از الفاظ معانی همه یکسر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وز پرتو جامش بتوان دید در ارحام</p></div>
<div class="m2"><p>هر بچه‌ که زاید پس ازین تا صف محشر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنقدر بنوشیم‌که می در عوض خوی</p></div>
<div class="m2"><p>بیرون جهد از هرچه مسام است به پیکر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من خنده‌کنان خیزم و بر روی تو افتم</p></div>
<div class="m2"><p>چون ماه تو در زیر و چو مریخ من از بر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هی بویمت و هی زنم از بوی تو عطسه</p></div>
<div class="m2"><p>هی بوسمت و هی خورم از بوس تو شکر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چندان زنمت بوسه ‌که سر تاقدمت را</p></div>
<div class="m2"><p>از بوسه نمایم چو رخ خویش مجدر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای طرهٔ مشکین تو با مشک پسرعم</p></div>
<div class="m2"><p>وی چهرهٔ سیمین تو با سیم برادر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چشم و مژه‌ات هیچ نگویم به چه ماند</p></div>
<div class="m2"><p>ترکی ‌که شو‌د مست و برد دست به خنجر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مسکین‌دلکم چون رهد از چنبر زلفت</p></div>
<div class="m2"><p>در پنجهٔ شاهین چه برآید ز کبوتر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رفتم به میان تو کنم رخنه چو یأجوج</p></div>
<div class="m2"><p>بستی ز سرین در ره من سد سکندر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پیوسته زمین تر شدی از آب رخ تو</p></div>
<div class="m2"><p>گر آب رخت را نبدی شعلهٔ آذر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رخساره نمودی و دلم بردی و رفتی</p></div>
<div class="m2"><p>مانا صنما از پریان داری‌ گوهر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زلف تو به روی تو سر افکنده ز خجلت</p></div>
<div class="m2"><p>بنیوش دلیلی ‌که نکو داری باور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زنگی چو در آیینه رخ خویش ببیند</p></div>
<div class="m2"><p>شرم آیدش از خویش و به زانو فکند سر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جز بر رخ زردم مفکن چشم ازیراک</p></div>
<div class="m2"><p>بیمار غذایی نخورد غیر مزعفر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر صورت بازی شدی از حسن مجسم</p></div>
<div class="m2"><p>مژگان تو چنگش بدی و زلف تو شهپر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرگه فکنم چشم بر آن ‌کاکل پیچان</p></div>
<div class="m2"><p>هرگه که زنم دست بر آن زلف معنبر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زین ‌یک شودم مشت پر از کژدم اهواز</p></div>
<div class="m2"><p>زان یک شودم چشم پر از افعی حَمیَر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یک روز اگرت تنگ در آغوش بگیرم</p></div>
<div class="m2"><p>تا صبح قیامت نفسم هست معطر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>منگر به حقارت سوی قاآنی‌ کز مهر</p></div>
<div class="m2"><p>شد مشتری دانش او زهرهٔ‌کشور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دخت ملک ملک‌ستان آسیه سلطان</p></div>
<div class="m2"><p>کش عصمت و عفت بود از آسیه برتر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>او جان شه و مردمک دیدهٔ شاهست</p></div>
<div class="m2"><p>زانروست عزیزش لقب از شاه مظفر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جز دامن شاهش نبود جایگه آری</p></div>
<div class="m2"><p>جز در دل دریا نبود مسکن گو‌هر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون چهره نهد شاه به رخسارش‌گویی</p></div>
<div class="m2"><p>از چرخ درآمد به زمین برج دو پیکر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر صبح که رخسار خود از آب بشوید</p></div>
<div class="m2"><p>هر قطره از آن آب شود مهر منور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فربه شود از قرب شهنشاه اگرچه</p></div>
<div class="m2"><p>نزدیکی خورشید کند مه را لاغر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای زینت آغوش و بر داور دوران</p></div>
<div class="m2"><p>کزصورت تو معنی جان‌گشته مصور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خیزد پی تعظیم رخ خوب تو هر روز</p></div>
<div class="m2"><p>خورشید ز گردون چو سپند از سر مجمر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از نور تو در پردهٔ اصلاب توان دید</p></div>
<div class="m2"><p>ایمان ز رخ مومن وکفر از دل‌کافر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو مرکز حسنی و ملک دایرهٔ جود</p></div>
<div class="m2"><p>زان است تو را جا به دل شاه دلاور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شه را تو به برگیری و بسیار عجیبست</p></div>
<div class="m2"><p>مرکز که همی دایره را گیرد در بر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گویند ملک می نخورد پس ز چه بوسد</p></div>
<div class="m2"><p>لبهای تو کش نشوه ز می هست فزونتر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در دفتر اگر وصف عفاف تو نگارند</p></div>
<div class="m2"><p>همچون پری از دیده نهان ‌گردد دفتر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>انصاف ده امروز به غیر از تو که دارد</p></div>
<div class="m2"><p>مهتاب به پیراهن و خورشید به معجر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مامت بود آن شمسهٔ ایوان جلالت</p></div>
<div class="m2"><p>کز بدر رخش جای عرق می‌چکد اختر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وز بس‌ که بر او عفت او پرده ‌کشیدست</p></div>
<div class="m2"><p>عاجز بود از مدحت او وهم سخنور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تنها نه همین پوشد رخساره ز مردان</p></div>
<div class="m2"><p>کز غایت عصمت ز زنانست مستر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از حجره برون ناید الا به شب تار</p></div>
<div class="m2"><p>تا سایه همش نیز نبیند به ره اندر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در آینه هرگه نگرد عکس رخ خویش</p></div>
<div class="m2"><p>بیگانه شماردش رود در پس چادر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جز او که بر او پرده‌ کشد عصمت زهرا</p></div>
<div class="m2"><p>مردم همگی عور درآیند به محشر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در بطن مشیت‌که خلایق همه بودند</p></div>
<div class="m2"><p>نامحرم و محرم بر هم خفته سراسر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>او درکنف فاطمه دور از همه مردم</p></div>
<div class="m2"><p>محجوب بد اندر حجب رحمت داور</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گویی‌ که خدیجه است هم آغوش محمد</p></div>
<div class="m2"><p>زیراکه بتولی چو ترا آمده مادر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ای دخت شه ای مردمک چشم شهنشاه</p></div>
<div class="m2"><p>ای همچو خردکامل و چون روح مطهر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بی پرده برون آ که کست روی نبیند</p></div>
<div class="m2"><p>بنیوش دلیل و مشو از بنده مکدر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گویند حکیمان‌ که رود خط شعاعی</p></div>
<div class="m2"><p>از چشم سوی آنچه به چشمست برابر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تا خط شعاعی به بصر باز نگردد</p></div>
<div class="m2"><p>در باصره حاصل نشود صورت مبصر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>حسن تو به حدیست‌که آن خط ز رخ تو</p></div>
<div class="m2"><p>برگشتنش از فرط وله نیست میسر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مشاطهٔ حسن تو بود سلطان آری</p></div>
<div class="m2"><p>هم مهر بباید که کند مه را زیور</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چون شانه کند موی ترا جیب و کنارش</p></div>
<div class="m2"><p>تا روز دگر پر بود از نافهٔ اذفر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چون‌ روی ‌ترا شو‌ید و ساید به رخت دست</p></div>
<div class="m2"><p>فی‌الحال بروید ز کفش لالهٔ احمر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از جنت و کوثر نکند یاد که او را</p></div>
<div class="m2"><p>رخسار و لب تست به از جنت و کوثر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تا از اثر نامیه هر سال به نوروز</p></div>
<div class="m2"><p>بر فرق نهد لاله ‌کله ‌گوشهٔ قیصر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آغوش ملک باد شب و روز و مه و سال</p></div>
<div class="m2"><p>از چهره و چشم تو پر از لاله و عبهر</p></div></div>