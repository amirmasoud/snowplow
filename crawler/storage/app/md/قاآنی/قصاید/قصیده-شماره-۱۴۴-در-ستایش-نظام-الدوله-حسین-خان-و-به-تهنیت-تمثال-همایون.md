---
title: >-
    قصیدهٔ شمارهٔ ۱۴۴ - در ستایش نظام الدوله حسین خان و به تهنیت تمثال همایون
---
# قصیدهٔ شمارهٔ ۱۴۴ - در ستایش نظام الدوله حسین خان و به تهنیت تمثال همایون

<div class="b" id="bn1"><div class="m1"><p>ای همایون صورت میمون شاه‌ کامگار</p></div>
<div class="m2"><p>یک جهان جانی‌ که جان یک جهان بادت نثار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورت روح‌الامینی یا که تمثال وجود</p></div>
<div class="m2"><p>روضهٔ خلد برینی یا که نقش نوبهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماهتابی زان فروغت افتد اندر هر زمین</p></div>
<div class="m2"><p>آفتابی زان شعاعت تابد اندر هر دیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماه می‌گفتم ترا گر ماه بودی تاجور</p></div>
<div class="m2"><p>مهر می‌خواندم ترا گر مهر بودی تاجدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرخ بودی چرخ اگر بر خاک می‌گشتی مقیم</p></div>
<div class="m2"><p>عرش‌بودی عرش اگر بر فرش می‌جستی قرار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکجا نقشی اش از هستی نماید فخر و تو</p></div>
<div class="m2"><p>هستی آن نقشی‌که هستی از تودارد افتخار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقش‌آن‌شاهی‌که‌از جان‌خانه‌زاد مرتضی‌است</p></div>
<div class="m2"><p>نقش تیغش هم به معنی خانه‌زاد ذوالفقار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عارف معنی‌پرست ار صورتی بیند چو تو</p></div>
<div class="m2"><p>هم در آن باعت‌کند صورت‌پرستی اختیار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواجهٔ اعظم پس از یزدان پرستد مر ترا</p></div>
<div class="m2"><p>وندرین رمزیست کش صورت‌پرستی اختیار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواجه ‌را چشمیست‌ معنی ‌بین‌ به ‌هر صورت که ‌هست</p></div>
<div class="m2"><p>زانکه ما صورت همی بینیم و او صورت‌نگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای مهین تمثال هستی ای بهین تصویر عقل</p></div>
<div class="m2"><p>تا چه نقشی ‌کز تو جوید عقل و هستی اعتبار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیک می‌تابی مگر مهتاب داری در بغل</p></div>
<div class="m2"><p>نور می‌باری مگر خورشید داری درکنار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نفس ‌و روح و عقل‌ و معنی را همی‌گوید حکیم</p></div>
<div class="m2"><p>کس نمی‌بیند به چشم و من ندارم استوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زانکه تا نقش همایون ترا دیدم به چشم</p></div>
<div class="m2"><p>نفس‌ و روح‌ و عقل‌ و معنی شد مصور هر چهار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عارفت ار نقشت عیان بیند به مرآت وجود</p></div>
<div class="m2"><p>در ظهور هستی غیبش نماید انتظار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پرده‌ات را از ازل‌گویی فلک نساج بود</p></div>
<div class="m2"><p>کز جلالش‌کرد پود و از جمالش بافت تار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صورت شاهیّ و پیدا معنی شاهی ز تو</p></div>
<div class="m2"><p>نقش هر معنی شود آری ز صورت آشکار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرکجا هستی‌تو شاه آنجا به‌معنی حاضرست</p></div>
<div class="m2"><p>زان که تو سایه ی شهی شه سایه ی پروردگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زان ستادستند میران و بزرگان بر درت</p></div>
<div class="m2"><p>هم بدان آیین ‌که بر دربار خسرو روزبار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عیش دایم پیش روی و عمر جاوید از قفا</p></div>
<div class="m2"><p>یمن دولت بر یمین و یسر شوکت بر یسار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یک طرف سرهنگ و سرتیپان گروه اندر گره</p></div>
<div class="m2"><p>یک طرف تابین و سربازان قطار اندر قطار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زیر دست چاکران شاه ماه و آفتاب</p></div>
<div class="m2"><p>زیر دست آفتاب و ماه چرخ و روزگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یک‌دو صف باترک زین چار میر ملک جم</p></div>
<div class="m2"><p>کهترین سؤباز شاهنشاه صاحب اختیار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روزگار و چرخ و مهر و ماه آری‌کیستند</p></div>
<div class="m2"><p>تا شوند از قدر با سرباز خسرو همقطار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هم به دست خیلی از خدام جام‌گوهرین</p></div>
<div class="m2"><p>هم به دوش فوجی از سرباز مار مورخوار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جام آن شربت دهد احباب شه را روز عید</p></div>
<div class="m2"><p>مار این ضربت زند خصم ملک را روزکلر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آن نماید خنگ عشرت را به جام خود لجام</p></div>
<div class="m2"><p>وین برآرد خصم خسرو را به مار خود دمار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هم ز جام آن مصور صورت جمشید و جام</p></div>
<div class="m2"><p>هم به مار این محول حالت ضحاک و مار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زان جوان و پیر می رقصند امروز از نشاط</p></div>
<div class="m2"><p>کاب ششپیر آمد از بخت جوان شهریار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون ‌که این آب روان از راه خلّر ‌آمدست</p></div>
<div class="m2"><p>چون شراب خلری زان مست‌گردد هوشیار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن ششپیرست آن یا آب شمشیر ملک</p></div>
<div class="m2"><p>دوستان را دلپذیر و دشمنان را ناگوار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جود شاهنشه مگر سرچشمهٔ این آب بود</p></div>
<div class="m2"><p>کاب می جوشد همی ‌از کوه و دشت و مرغزار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از نشاط آنکه این آب آید از بخت ملک</p></div>
<div class="m2"><p>شعر قاآنی چو تیغ شاه‌گشتست آبدار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گو مغنی لحن شهرآشوب ننوازد از آنک</p></div>
<div class="m2"><p>شهر بی‌آشوب‌ گشت از بخت شاه بختیار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا به ‌دهر اندر حصار ملک‌ گیتی هست چرخ</p></div>
<div class="m2"><p>حزم شه چون چرخ بادا ملک ‌گیتی را حصار</p></div></div>