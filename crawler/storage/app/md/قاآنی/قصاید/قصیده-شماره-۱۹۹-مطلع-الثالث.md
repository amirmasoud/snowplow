---
title: >-
    قصیدهٔ شمارهٔ ۱۹۹ - مطلع الثالث
---
# قصیدهٔ شمارهٔ ۱۹۹ - مطلع الثالث

<div class="b" id="bn1"><div class="m1"><p>همانا فصل تابستان سرآمد عهد تسعینش</p></div>
<div class="m2"><p>که مایل شد به‌ کفهٔ شب ترازو باز شاهینش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو پرّ باز بود اسپید روز از روشنی آوخ</p></div>
<div class="m2"><p>که ابر تیره تاری‌تر نمود از چشم شاهینش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فلک از ابر ایدون آبنوسی گشته خورشیدش</p></div>
<div class="m2"><p>چمن از باد ایدر سندروسی گشته نسرینش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قمر بدگوهری رخثباکه‌گردون بود عمانثن</p></div>
<div class="m2"><p>سمن بدعنبری بویا که هامون بود نسرینش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به‌کام‌اندر کشید این را زمین از بیم بدگویش</p></div>
<div class="m2"><p>به ابر اندر نهفت آن را فلک از چشم بدبیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرآن‌کانون‌که‌مهرافروخت‌درمرداد و شهریور</p></div>
<div class="m2"><p>عیان در آسمان دود از چه در آبان و تشرینش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مر آن درّاعهٔ سندس ‌که بیضا دوخت در جوزا</p></div>
<div class="m2"><p>به‌اکسون‌وشساب‌ایدر جهان‌را عزم تردینش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مر آن بارانی قاقم‌ که خود آراست در سرطان</p></div>
<div class="m2"><p>به قندزگون غمام اینک فلک را رای تبطینش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مر آن آتش که شید افروخت اندر بیشهٔ ضیغم</p></div>
<div class="m2"><p>ز آب ابر اینک آسمان را قصد تسکینش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زره سازد ز آب برکه باد و می‌نپاید بس</p></div>
<div class="m2"><p>که در هر خرگهی روشن بود نیران تفتینش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>توگویی تخم بید انجییر خوردست ابر آبانی</p></div>
<div class="m2"><p>که از رشح پیاپی ظاهرست آثار تلیینش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نک از باد خزان برک رزان لرزان تو پنداری</p></div>
<div class="m2"><p>فلک ‌در حضرت‌صدر جهان‌کردست‌ تو خینش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مکان جود و کان جود ابوالقاسم ‌که در سینه</p></div>
<div class="m2"><p>نهان چون کین اهل‌کفر مهر آل‌یاسینش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مخمّر زآب و خاک و باد و نارستش بدن اما</p></div>
<div class="m2"><p>حیا آبش وفا نارش رضا بادش عطا طینش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر از گردون سخن‌رانی بود شوکت ‌دوچندانش</p></div>
<div class="m2"><p>ور از عمان سمرخوانی بود همت دوچندینش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیان او که با آیات فرقانست توشیحش</p></div>
<div class="m2"><p>کلام او که با اصوات داودست تضمینش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مکن بوجهل سان ای حاسد بدگوی انکارش</p></div>
<div class="m2"><p>مکن جالوت‌وار ای دشمن بدگوی تلحینش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به‌کاخ اندر کهین شبری فضای هند و بلغارش</p></div>
<div class="m2"><p>به‌گنج اندرکمین‌فلسی خراج چین و ماچینش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فنا رنجی بود محتوم و لطف اوست تدبیرش</p></div>
<div class="m2"><p>قضا گنجی‌ بود مکتوم و حزم اوست زرفینش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به سر دست ‌آورد هرگه ‌نظر بر روی محتاجش</p></div>
<div class="m2"><p>بپا چشم افکند هر گه‌ گذر در کوی مسکینش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بلی پژمان اگر بخشد خراج چین و سقلابش</p></div>
<div class="m2"><p>بلی غمگین اگر بدهد منال روم و سقسینش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به درّ و گوهر آمودست نثر نثره مانندش</p></div>
<div class="m2"><p>به ‌مشک و عنبر آکندست شعر شعری آیینش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو سحبان ‌العرب ‌شنود دمان ‌سوزد تصانیفش</p></div>
<div class="m2"><p>چو حسان‌العجم بیند روان شوید دواوینش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>محیطی‌هست‌جود اوکه‌ممکن‌نیست‌تقدیرش</p></div>
<div class="m2"><p>جهانی هست جاه او که یارا نیست تخمینش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به ‌وهمش ‌گر بپیمایی ‌خجل ‌گردی ‌ز تشخیصش</p></div>
<div class="m2"><p>به فهمش‌ گر بینگاری‌ کسل مانی ز تعیینش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ندانی نیل و طوفان را بود خود پایه زان برتر</p></div>
<div class="m2"><p>که پیمایی به باع یام و صاع ابن‌یامینش‌</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ازو چون ‌منحرف‌ شد خصم‌ لازم‌ طعن و تو بیخش</p></div>
<div class="m2"><p>چنال‌چون‌منصرف‌شد اسم‌واجب‌جرّ و تنوینش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زهی فرخنده آن دیوان که نام اوست عنوانش</p></div>
<div class="m2"><p>خهی پاینده آن ایوان‌ که نقش اوست آذینش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وثاق او دبستانی‌که هفت اجرام اطفالش</p></div>
<div class="m2"><p>رواق او گلستانی که نه افلاک پرچینش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه انبازست در هوش و کیاست پور قحطانش</p></div>
<div class="m2"><p>نه همرازست در فر و فراست ابن‌یقطینش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بلی آن روضهٔ مینو مشاکل نیست رضوانش</p></div>
<div class="m2"><p>بلی این دوحهٔ طوبی مشابه نیست یقطینش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به عالم ‌گر درون از عالم افزون نی عجب ایرا</p></div>
<div class="m2"><p>که‌ نون ‌یک ح‌رف ‌در صورت ولی ‌معنیست خمسینش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به‌نزدش‌چرخ‌صفری‌لیک‌از ‌چرخش فزاید فر</p></div>
<div class="m2"><p>ز یک صفر آری آری پایه ‌گردد سبع سبعینش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خهی قدر تو کیالی ‌که ‌گردونست مکیالش</p></div>
<div class="m2"><p>زهی فرّ تو میزانی‌ که ‌گیهانست شاهینش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جهان مقصورهٔ ویران ز سعی تست تعمیرش</p></div>
<div class="m2"><p>زمان معشوقهٔ عریان ز فرّ تست تزیینش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جلال تست آن خرگه‌ که اجرامست اوتادش</p></div>
<div class="m2"><p>شکوه تست آن صفه ‌که افلاکست خرزینش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فلک نهمار دون‌پرور سزانی با تو تشبیهش</p></div>
<div class="m2"><p>جهان بسیار کین‌گستر روانی با تو تزکینش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>عنودی ‌کز تو رخ تابد به دوزخ قوت زقّومش</p></div>
<div class="m2"><p>حسودی ‌کز تو سر پیچد به‌ نیران‌ سجن ‌سجینش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز فرت فر آن دارا که فرمان بر ممالیکش</p></div>
<div class="m2"><p>ز بختت‌ بخت‌ آن ‌خسرو که ‌سلطان‌ بر سلاطینش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>غیاث‌الملک و المله فلک‌فر حشمت‌الدوله</p></div>
<div class="m2"><p>که بر نُه چرخ و هفت اختر بود نافذ فرامینش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جهان آشفته‌دل روز نبرد از برق صمصامش</p></div>
<div class="m2"><p>سپهر آسیمه‌سر گاه جدال از بانگ سرغینش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>عطای اوست آن مطبخ که مهر آمد عقاقیرش</p></div>
<div class="m2"><p>سخای‌ اوست ‌آن ‌مصنع ‌که ‌چرخ ‌آمد طواحینش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو بر ختلی گذارد کام باج آرند از رومش</p></div>
<div class="m2"><p>چو از هندی زداید زنگ ساو آرند از چینش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به‌زنگ‌ اندر زلازل‌ چون‌ که‌ بر عارض بود زنگش</p></div>
<div class="m2"><p>به چین‌ اندر هزاهز چون‌ که بر ابروفتد چینش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به‌ گاه کینه حدادی ‌که البرزست فطیسش</p></div>
<div class="m2"><p>به وقت وقعه قصابی ‌که مریخت سکینش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به نطع رزم هر بیدق‌ که از مکمن برون راند</p></div>
<div class="m2"><p>برد در ملکت بدخواه و بخشد فرّ فرزینش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو ‌در کین ‌طلعت ‌افروزد دنیایش گوی خرّادش</p></div>
<div class="m2"><p>چو بر زین قامت افرازد ستایش جوی بر زینش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به صولت پیل ‌کوشنده به دولت نیل جوشنده</p></div>
<div class="m2"><p>نه بل صولت دو چندانش ‌نه ‌بل ‌دولت دو چندینش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گرفتم ‌خصم رویین‌تن سرودم حصن رویین‌دز</p></div>
<div class="m2"><p>زبون دیوانه‌یی آنش نگون ویرانه‌یی اینش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>یکی‌شیرست ‌آتش‌خوی‌و آهن‌دل که در هیجا</p></div>
<div class="m2"><p>نماید خشک چوبی در نظر بهرام چوبینش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو گاه کینه لشکر بر سما شور هیاهویش</p></div>
<div class="m2"><p>چو وقت وقعه موکب بر سها بانگ هیاهینش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کم از برفینه پیلی صدهزاران ریو و رهامش</p></div>
<div class="m2"><p>کم از گرینه شیری صد هزاران ‌گیو و گرگینش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>پدرش آن گرد عمان‌بخش گردون‌رخش دولتشه</p></div>
<div class="m2"><p>که با این فر و مکنت آسمان می‌کرد نمکیش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>برفت و ماند ازو نامی‌ که ماند تا جهان ماند</p></div>
<div class="m2"><p>زهی احسان‌که تا روز جزا باقیست تحسینش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>برفت و ماند ازو پوری ‌که پیر عقل را قائد</p></div>
<div class="m2"><p>تبارک آن پدر کز فر و دانش پور چونینش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نیاش آن خسرو صاحبقران ‌کز فرهٔ ایزد</p></div>
<div class="m2"><p>روان چونان‌ که جان و جسم فرمانبر خواقینش</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به کاخ اندر چو رویین صدهزاران گرد نویانش</p></div>
<div class="m2"><p>به‌ جیش اندر چو زوبین‌ صدهزاران نیو نوئینش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز شوق جان‌فشانی در صف هیجا دهد بوسه</p></div>
<div class="m2"><p>به خنجر حنحر سنجر به زوبین نای زوبیش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>زند با راستان از بهر طاعت رای چیپالش‌</p></div>
<div class="m2"><p>نهد بر آستان از بهر خدمت روی رویینش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چوزی ایوان نماید رای و سازد جای بر صدرش</p></div>
<div class="m2"><p>جو بر یکران نماید روی و آرد پای بر زینش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو بر عرش برین بینی یکی فرخنده جبریلش</p></div>
<div class="m2"><p>چو بر باد بزین یابی یکی سوزنده بر زینش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ملک با خوی این دارا چرا نازد به اخلاقش</p></div>
<div class="m2"><p>فلک با خام این خسرو چرا بالد به تنینش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ملک‌ کی با ملک همسر فلک‌ کی با کیا همبر</p></div>
<div class="m2"><p>ز فضل آن فایده یابش ز بذل این زایده چینش</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>فلک گر بالد از هوری ملک نازد به دستوری</p></div>
<div class="m2"><p>که صد خورر باستین دارد نهال رای جهان‌بنیش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>سمی مصطفی آن صاحب صاحب لقب ‌کامد</p></div>
<div class="m2"><p>امل آسوده از مهرش اجل فرسوده از کینش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ز حزم اوست دین ایزدی جاری تکالیفش</p></div>
<div class="m2"><p>ز رای اوست شرع احمدی نافذ قوانینش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بیانش‌کز رشاقت پایه بر جوزا و عیوقش</p></div>
<div class="m2"><p>کلامش‌کز براعت طعنه بر بیضا و پروینش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تو گویی کلک مانی بوده نقاش عباراتش</p></div>
<div class="m2"><p>تو گویی نطق عیسی بوده قوّال مضمامینش</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>اگر دشمن شود فربه ز کلک اوست تهزیلش</p></div>
<div class="m2"><p>وگر ملکت شد لاغر ز عزم اوست تسمینش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>فصاحت‌چیست مجنونی ‌که لفظ اوست لیلاین</p></div>
<div class="m2"><p>بلاغت کیست فرهادی‌که‌کلک اوست شیرینش</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>در اشعار بلاغت بس بود اشعار شیوایش</p></div>
<div class="m2"><p>در اثبات رشاقت بس بود ابیات رنگینش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>مقام مصطفی خواهی بخوان اخبار معراجش</p></div>
<div class="m2"><p>نبرد مرتضی جویی ببین آثار صفینش</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>وزیرا صاحبا صدرا درین ابیات جان‌پرور</p></div>
<div class="m2"><p>دو نقصانست پنهانی‌که ناچارم ز تبیینش</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>یکی در چند جا تکرار جایز در قوافیش</p></div>
<div class="m2"><p>نه تکراری ‌که دیوان را رسد نقصان ز تدوینش</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>یکی در چند شعر ایطا نه ایطایی چنان روشن</p></div>
<div class="m2"><p>که باشد بیمی از غمّاز و باکی از سخن چینش</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>نپنداری ندانستم بدانستم نتانستم</p></div>
<div class="m2"><p>شکر تب‌خیز و دانا ناگزیر از طعم شیرینش</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>وگر برخی قوافیش خشن نشگف‌کز فاقه</p></div>
<div class="m2"><p>پلاسین ‌پو شد آنکو نیست سنجان و پرندینش</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>قوافی نیست‌کژدم تا دو خشت تر نهم برهم</p></div>
<div class="m2"><p>پس از روزی دو بتوانم بدین تدبیر تکوینش</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>قوافی را لغت باید لغت را من نیم واضع</p></div>
<div class="m2"><p>که ‌رانم طبع ‌را کاین ‌لفظ ‌شایسته‌ است بگزیش</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>زهی حسان سحر آرای سِحرانگیز قاآنی</p></div>
<div class="m2"><p>که حسان‌العجم احسنت‌گو از خاک شروینش</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>تبارک از عباراتش تعالی ز استعاراتش</p></div>
<div class="m2"><p>زهی شایسته تبیانش خهی بایسته تقنینش</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>حکایت گه ز جانانش شکایت گه ز دورانش</p></div>
<div class="m2"><p>نگارش گه ز نیسانش‌ گزارش‌ گه ز تشرینش</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ز جانان مدح و تعریفش ز آبان وصف و توصیفش</p></div>
<div class="m2"><p>به‌ گیهان ‌سبّ و تقریعش به ‌گردون ذم و تلعینش</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>گهی بر لب ز بوالقاسم ثنا و بر رخ آزرمش</p></div>
<div class="m2"><p>گهی بردم ز حشت شه دعا و در دل آمینش</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>گهی از یاد دولتشه ز محنت لکنه در دالش</p></div>
<div class="m2"><p>گهی‌از ذکر حشمت‌شه ز عسرت لثغه در شینش</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>گه از صاحب ‌ثنا گفتن ولی با شرم بسیارش</p></div>
<div class="m2"><p>گه از هریک دعا گفتن ولی با قصد تأمینش</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گهی در شعر گفتن آن همه اصرار و تعجیلش</p></div>
<div class="m2"><p>گهی‌ در شعر خواندن این‌ همه‌ انکار و تهدینش</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>گهی عذر قوافی خواستن وانطور تبیانش</p></div>
<div class="m2"><p>گهی برد امانی بافتن وان طرز تضمینش</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>کنون از بارور نخل ضمیرم یک ثمر باقی</p></div>
<div class="m2"><p>همایون‌باد نخلی ‌کاین رطب باشد پساچینش</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>دو مه زین پیش‌ کم یا بیش بودن چاکر میری</p></div>
<div class="m2"><p>که‌ کوه بیستون را رخنه بر تن از تبرزینش</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>مرا با خواجه‌ تاشی دیو دیدن داد آمیزش</p></div>
<div class="m2"><p>که صحن‌چهره قیرآگین‌بدی‌از رای تارینش</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ز می آموده اندر آستان هر شب صراحیش</p></div>
<div class="m2"><p>به بنج آلوده اندر آستین هردم معاجینش</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>گهی از بی‌نبیذی‌ کیک وحشت در سراویلش</p></div>
<div class="m2"><p>گهی از بی‌حشیشی‌سنگ‌محنت در تساخینش</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>چو جوکی موی‌سر انبوه‌و ناخنهای‌دست و پا</p></div>
<div class="m2"><p>دراز و زفت و ناهنجار چون بیل دهاقینش</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>اگر لاحول پاس من نبودی حافظ و حارس</p></div>
<div class="m2"><p>ز شب تا چاشتگه نهمار گادندی شیاطینش</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>به‌من چون‌دیو در ریمن ولی‌من‌از ش‌ش ایمن</p></div>
<div class="m2"><p>بلی چون مهر نورانی‌کرا یارای تبطینش</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>نهانی خواجه با او رام چونان نفس با شهوت</p></div>
<div class="m2"><p>ولی‌وحشت‌ز من چون معده از حب‌السلاطینش</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ضرورت را بریدم زوکه تا در عرصهٔ محشر</p></div>
<div class="m2"><p>بپیوندم ابا پیغمبر و آل میامینش</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>خلاف امر یزدان بود و شرع پاک پیغمبر</p></div>
<div class="m2"><p>رضای‌ خواجه‌ای‌ چو نان که‌ چونین زسم و آیینش</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>گرفتم خواجه‌ کوثر بود کوثر ناگوار آید</p></div>
<div class="m2"><p>چو آمیزش به‌ غسّاقش چو آلایش به‌ غسلینش</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>ازین پس مادح پیغمبر و دارای دورانم</p></div>
<div class="m2"><p>که ستوار ست پغمبر ز دارا ملت و دینش</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>الا تا آب نبود کار جز ترطیب و تبریدش</p></div>
<div class="m2"><p>الا تا نار نبود فعل جز تجفیف و تسخینش</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ملک پیوسته با چرخ برین انباز اورنگش</p></div>
<div class="m2"><p>کیان همواره با مهر فلک همراز گرزینش</p></div></div>