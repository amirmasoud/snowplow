---
title: >-
    قصیدهٔ شمارهٔ ۱۳۷ - در منقبت مولانا اسدالله الغالب علی بن ابیطالب علیه السلام و ستایش شاهنشاه ناصرالدین شاه خلدالله ملکه گوید
---
# قصیدهٔ شمارهٔ ۱۳۷ - در منقبت مولانا اسدالله الغالب علی بن ابیطالب علیه السلام و ستایش شاهنشاه ناصرالدین شاه خلدالله ملکه گوید

<div class="b" id="bn1"><div class="m1"><p>اسم شد مشیّد و دین ‌گشت استوار</p></div>
<div class="m2"><p>از بازوی یدالله و از ضرب ذوالفقار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن رحمت خدای ‌که از لطف عام اوست</p></div>
<div class="m2"><p>شیطان هنوز با همه عصیان امیدوار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن اولین نظر که ز رحمت نمود حق</p></div>
<div class="m2"><p>وان آخرین طلب‌که ز حق‌کرد روزگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای برترین عطیهٔ ایزد که امر تو</p></div>
<div class="m2"><p>بر رد و منع حکم قضا دارد اقتدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کن غرض تو بودی و پیش از خطاب حق</p></div>
<div class="m2"><p>بودی نهفته درتتق نور کردگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نابوده را خطاب به بودن نکرد حق</p></div>
<div class="m2"><p>وین نغز نکته‌ گوش خرد راست ‌گوشوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معنیّ امر کن به تو این بود در نهان</p></div>
<div class="m2"><p>کای بوده جنبشی‌کن و نابوده را بیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>معنی هر درخت ‌که ‌کاری به خاک چیست</p></div>
<div class="m2"><p>جز اینکه باش و میوهٔ پنهان‌کن آشکار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ذات خود چو نور تراکردگار دید</p></div>
<div class="m2"><p>با تو خطاب‌کرد ز الطاف بی شمار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کای دانهٔ مشیت و ای ریشهٔ وجود</p></div>
<div class="m2"><p>باش این زمان‌که از تو پدید آورم شمار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از حزم تو زمین‌ کنم از عزمت آسمان</p></div>
<div class="m2"><p>از رحمت تو جنت و از هیبت تو نار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عنفت‌کنم مجسم و نامش نهم خزان</p></div>
<div class="m2"><p>لطفت ‌کنم مصور و نامش نهم بهار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از طلعت تو لاله برویانم از زمین</p></div>
<div class="m2"><p>از سطوت تو موج برانگیزم از بحار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نقش دوکون راکه نهان در وجود تست</p></div>
<div class="m2"><p>بیرون‌ کشم چو گوهر از آن بحر بی‌کنار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو‌ عکس ‌ذات ‌حقی ‌و حق ‌عاکس است ‌و نیست</p></div>
<div class="m2"><p>فرقی در این میان به جز از جبر و اختیار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عاکس به اختیار چو بیند در آینه</p></div>
<div class="m2"><p>بیخود فتد در آینه عکسش به اضطرار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مر سایه را نگر که به جبر از قفا رود</p></div>
<div class="m2"><p>هرجا به اختیار بود شخص راگذار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یک جنبشست خامه و انگشت را ولی</p></div>
<div class="m2"><p>فرقیست در میانه نهان پاس آن بدار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با هم اگر چه خیزند از کام حرف و صوت</p></div>
<div class="m2"><p>لیکن به اصل صوت بود حرف استوار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آوخ‌ که نقد معنی پاکست در ضمیر</p></div>
<div class="m2"><p>چون بر زبان رسد شود آن نقد کم‌عیار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بس مغز معنیاکه به دل پخته است و نغز</p></div>
<div class="m2"><p>چون قشر لفظ‌گیرد خامست و ناگوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لیکن‌گه بیان معانی ز حرف و صوت</p></div>
<div class="m2"><p>از وی طبع چاره ندارد سخن‌گذار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از بهر آنکه سیم‌کند سکه را قبول</p></div>
<div class="m2"><p>بر سیم لازمست‌ که از مس زنند بار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>باری تو از خدا به حقیقت جدا نیی</p></div>
<div class="m2"><p>گرچه تو آفریده‌یی او آفریدگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون از ازل تو بودی با کردگار جفت</p></div>
<div class="m2"><p>هم تا ابد تو باشی باکردگار یار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زانسان‌که خط دایره در سیر همبرست</p></div>
<div class="m2"><p>با مرکزی‌که دایره بر وی‌کند مدار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فردست‌ کردگار تویی جفت ذات او</p></div>
<div class="m2"><p>لیکن نه آنچنان‌که بود پود جفت تار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با اویی و نه اویی و هم غیر او نیی</p></div>
<div class="m2"><p>کاثبات و نفی هست در اینجا به اعتبار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یک شخص را کنی به مثل‌‌ گر هزار وصف</p></div>
<div class="m2"><p>ذاتش همان یکست و نخواهد شدن هزار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وحدت ز ذات یک نشود دور اگر تواش</p></div>
<div class="m2"><p>هفتاد بار برشمری یا هزار بار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خواهد کس ار ز روی حقیقت‌ کند بیان</p></div>
<div class="m2"><p>در یک نفس مدیح دو عالم به اختصار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نام ترا برد به زبان زانکه نام تست</p></div>
<div class="m2"><p>دیباچهٔ مدایح و فهرست افتخار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر مدح و منقبت که بود کاینات را</p></div>
<div class="m2"><p>در نام تو نهفته چو در دانه برگ نار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زیراکه هرچه بود نهان در دو حرف‌کن</p></div>
<div class="m2"><p>هم بر سه حرف نام تو جستست انحصار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زان ضربتی‌که بر سر مرحب زدی هنوز</p></div>
<div class="m2"><p>آواز مرحباست که خیزد ز هر دیار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دادی رواج شرع نبی را ز قتل عمرو</p></div>
<div class="m2"><p>کاو را ز پا فکندی و دین‌گشت پایدار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بعد از نبی رسید خلافت به چار تن</p></div>
<div class="m2"><p>بودی تو یک خلیفهٔ برحق از آن چهار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>متصود میوه‌ایست‌که آخر دهد درخت</p></div>
<div class="m2"><p>نز برگها که پیش بروید ز شاخسار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مدح تو چون شعاع خور از مشرق لبم</p></div>
<div class="m2"><p>ناجسته در بسیط زمین یابد انتشار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو ابر رحمتی و ملک کشت عمر ملک</p></div>
<div class="m2"><p>برکشت عمر ملک ز رحمت یکی ببار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ختم ولایتی تو سزدکز ولای تو</p></div>
<div class="m2"><p>یکباره ختم‌ گردد شاهی به شهریار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شاهی‌که هرچه بود ز عدلش قرار یافت</p></div>
<div class="m2"><p>غیر از دلش‌ که ماند ز مهر تو بیقرار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فرمانروای عصر ابوالنصر تاج‌بخش</p></div>
<div class="m2"><p>جمشید ملک ناصر دین شاه کامگار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای رمح تو ستون سراپردهٔ ظفر</p></div>
<div class="m2"><p>وی حلم تو سجل نسب‌نامهٔ وقار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دانی چه وقت یابد خصم تو برتری</p></div>
<div class="m2"><p>روزی‌که خاک‌گردد خاکش شود غبار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چنگال شیر مرگ مگرهست تیغ تو</p></div>
<div class="m2"><p>کز وی عدوی ملک چو روبه‌ کند فرار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هرگه‌ که وصف تیغ تو گویمُ زبان من</p></div>
<div class="m2"><p>گردد بسان ‌کورهٔ حداد پر شرار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شیرازهٔ صحیفهٔ من خواست بگسلد</p></div>
<div class="m2"><p>دیشب که‌ گشتم از صفت وی سخن‌گذار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هی سوخت دفتر من از اوصاف او و من</p></div>
<div class="m2"><p>هی آب می‌زدم به وی از شعر آبدار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>امشب به محدت تو به غواصی ضمیر</p></div>
<div class="m2"><p>آرم ز بحر طبع‌گهرهای شاهوار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا صبح بهرپیشکش عید جمله را</p></div>
<div class="m2"><p>در مجلس اتابک اعظم ‌کنم نثار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از دانه ریشه تا دمد از ریشه شاخ و برک</p></div>
<div class="m2"><p>شاخ نشاط بنشان تخم طرب بکار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چون بخت تو ز بخت تو اعدای تو سمین</p></div>
<div class="m2"><p>چون تیغ تو ز تیغ تو اعدای تو نزار</p></div></div>