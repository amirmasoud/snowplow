---
title: >-
    قصیدهٔ شمارهٔ ۲۷۹ - بر سبیل تغزل و ترکیب‌بند گوید
---
# قصیدهٔ شمارهٔ ۲۷۹ - بر سبیل تغزل و ترکیب‌بند گوید

<div class="b" id="bn1"><div class="m1"><p>گر خضر دهد آب بقایت به زمستان</p></div>
<div class="m2"><p>مستان بستان جام می از ساقی مستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بستان به شبستان قدح از دست نگارین</p></div>
<div class="m2"><p>کز روی دلارا شکند رونق بستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترکی‌که به خوناب جگر دارد معجون</p></div>
<div class="m2"><p>در هر نظری اشک تر زهدپرستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لعل لب دلدار گز و خون رزان مز</p></div>
<div class="m2"><p>در خرقهٔ سنجاب خز و کنج شبستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درکش می چون خون سیاووش به بهمن</p></div>
<div class="m2"><p>کز نیرویش از دست رود رستم دستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خمر عنبی خواهم و بستانی کاو را</p></div>
<div class="m2"><p>نارنج غیب سیب زنخ نار دو پستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اینست علاج دل بیمار طبیبا</p></div>
<div class="m2"><p>سودم ندهد شیرهٔ عناب و سپستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بادهٔ ‌گلگون بودت‌ گو نبود گل</p></div>
<div class="m2"><p>فرخنده بهارست به میخواره زمستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خستی دلم ای دوست به دستگان نگارین</p></div>
<div class="m2"><p>دستان تو ای بس‌که بگویند به دستان</p></div></div>
<div class="b2" id="bn10"><p>بیرحمی و یک ذره وفا در دل تو نیست</p>
<p>تخمیست مروت ‌که در آب و گل تو نیست</p></div>