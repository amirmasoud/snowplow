---
title: >-
    قصیدهٔ شمارهٔ ۱۵۱ - در مدح صاحب ا‌ختیار
---
# قصیدهٔ شمارهٔ ۱۵۱ - در مدح صاحب ا‌ختیار

<div class="b" id="bn1"><div class="m1"><p>تا چه معجز کرده امشب باز عدل شهریار</p></div>
<div class="m2"><p>کاتش سوزنده با آب روان گشتست یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب و آتش بسکه از عدلش بهم آمیختند</p></div>
<div class="m2"><p>کس ترشح را نیارد فرق‌کردن از شرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از چراغان خاک پنداری سپهری دیگرست</p></div>
<div class="m2"><p>یا فلک پروین و مه راکرده برگیتی نثار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حزم صاحب اختیاری بین ‌که از عزم ملک</p></div>
<div class="m2"><p>آب و آتش را بهم‌کردست امشب سازگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اختیار از جبر خیزد ور همی خواهی دلیل</p></div>
<div class="m2"><p>حال میر ملک جم بنگر به چشم اعتبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نشد مجبول و مجبورش روان از مهر شه</p></div>
<div class="m2"><p>شاه دریادل نکردش نام صاحب‌اختیار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب ششپیر آمد این آتش ازان افروخت میر</p></div>
<div class="m2"><p>تا میان آب و آتش هم نماند گیرودار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا نه چون آن آب از دیر آمدن دلگیر بود</p></div>
<div class="m2"><p>خواست دلگرمش‌کند زالطاف شاه بختیار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راست ‌گویم معجز حزم شهنشاهست و بس</p></div>
<div class="m2"><p>کاتش سوزنده را زاب روان سازد حصار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرخ‌رو گشت‌آب‌ششپیر امشب‌از بخت‌ملک</p></div>
<div class="m2"><p>زانکه از دیر آمدن شرمنده بود و خاکسار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یا نه باز از هجر خاکپای شه شرمنده است</p></div>
<div class="m2"><p>زان‌رخش سرخست زآتش‌همچو روی‌شرمسار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آتش اندر ابر می‌بارند امشب یا به طبع</p></div>
<div class="m2"><p>آتش سوزنده همچون تیغ شه شد آبدار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یا خیال تیغ شه اندر دل آتش‌گذشت</p></div>
<div class="m2"><p>پای تا سر آب شد از شرم تیغ شهریار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا چو مهر و کین شه خلاق آب و آتشند</p></div>
<div class="m2"><p>مهر و کین شه بهم‌ گشتند امشب سازگار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>راست‌ گویی آتشین گلها درون موج آب</p></div>
<div class="m2"><p>هست‌چون‌عکس‌می گلگون‌ به ‌سیمین‌چهریار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یا نشان آتش موسی است اندر آب خضر</p></div>
<div class="m2"><p>یا نه شاخ ارغوان رستست زآب جویبار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یا میان حقهٔ الماس یاقوت مذاب</p></div>
<div class="m2"><p>یا درون بوتهٔ سیماب زر خوش‌عیار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آب امشب شعله‌انگیزست و آتش رشحه‌ریز</p></div>
<div class="m2"><p>عدل شه را بین‌ کزو شد نار آب و آب نار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دود آتش پیچد اندر آب‌گویی در نهفت</p></div>
<div class="m2"><p>لشکر دیو و پری دارند با هم‌کارزار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وادی طورست ‌گویی باغ تخت امشب از آنک</p></div>
<div class="m2"><p>آتشی موسی شدست از هر درختی آشکار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مارهای آتشین بنگر شتابان در هوا</p></div>
<div class="m2"><p>با وجود اینکه از آتش‌گریزانست مار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در به باغ تخت از بس آتش افتد لخت لخت</p></div>
<div class="m2"><p>سبزه‌هایش را چو برگ لاله بینی داغدار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>راست‌گویی‌ باغ را صد داغ حسرت بر دلست</p></div>
<div class="m2"><p>از فراق طلعت میمون شاه‌ کامگار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بسکه اخترها ز اخگرها همی ریزد در آب</p></div>
<div class="m2"><p>از شمار اختران عاجز بود اخترشمار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بس که تیر آتشین در باغ آید از هوا</p></div>
<div class="m2"><p>خشم شه ‌گویی درون خُلق شه دارد قرار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یا نه‌گویی باژگون‌ گشتست دوزخ در بهبشت</p></div>
<div class="m2"><p>تا عیان‌گردد به مردم قدرت پروردگار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تیر تخش اندر هوا ماند به سروی بارور</p></div>
<div class="m2"><p>کز شعاعش ‌هست ‌برگ ‌و از شر‌ارش هست بار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یا پی رجم شیاطین از سپهر آید شهاب</p></div>
<div class="m2"><p>کیست می‌دانی شیاطین خصم شاه نامدار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای‌ که دیدستی بسی فوارهای موج‌خیز</p></div>
<div class="m2"><p>اینک اندر آب بین فوارهای شعله‌بار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از چنارکهای آتش دیدم امشب آنچه را</p></div>
<div class="m2"><p>می‌شنیدم‌کاتش سوزنده خیزد از چنار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آب ز آتش رنگ خون دارد تو گویی آب نیل</p></div>
<div class="m2"><p>بر گروه قبطیان خون شد به امر کردگار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شاه آری موسی است و آب ششپیر آب نیل</p></div>
<div class="m2"><p>سبطی احباب ملک قبطی عدوی نابکار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سبطیان را بهره از آن نهر آب روح‌بخش</p></div>
<div class="m2"><p>قبطیان را قسمت از آن رود خون ناگوار</p></div></div>