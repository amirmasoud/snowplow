---
title: >-
    قصیدهٔ شمارهٔ ۸۲ - د‌ر ستایش پا‌دشاه ماضی محمد شاه غازی طاب ا‌لله ثراه ‌گوید
---
# قصیدهٔ شمارهٔ ۸۲ - د‌ر ستایش پا‌دشاه ماضی محمد شاه غازی طاب ا‌لله ثراه ‌گوید

<div class="b" id="bn1"><div class="m1"><p>هر دل اسیر زلف تو بیدادگر بود</p></div>
<div class="m2"><p>کارش ز تار زلف تو آشفته‌تر بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشوب ملک شاهی و بیدادکار تست</p></div>
<div class="m2"><p>ترکی و ترک لابد بیدادگر بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ملک حسن شاهی زان شور و شرکنی</p></div>
<div class="m2"><p>شک نیست حس چونین با شور و شر بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمشاد مهرچهری و خورشید مه‌جبین</p></div>
<div class="m2"><p>مانات مهر مادر و ماهت پدر بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باور نیفتدم ‌که بدین حسن و دلبری</p></div>
<div class="m2"><p>نقشی به چین و سروی در غاتفر بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چین و کاشغر ز پی چون تو دلفریب</p></div>
<div class="m2"><p>همواره پای اهل نظر رهسپر بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ورنه چو بست صورت با چون تویی وصال</p></div>
<div class="m2"><p>خواهم نه چین بماند و نه‌کاشغر بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرجاکه جلوه سازکنی‌گشت قندهار</p></div>
<div class="m2"><p>هر جا خرام ناز کنی‌ کاشمر بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرگه به زلف شانه زنی تبتست ‌کوی</p></div>
<div class="m2"><p>ور برکشی نقاب سرا شوشتر بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رویت به نور با مه‌ گردون برابرست</p></div>
<div class="m2"><p>زلفت به رنگ دایهٔ مشک تتر بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ماه فلک نه حاشاکی مشک پرورد</p></div>
<div class="m2"><p>مشک تتر نه‌ کلاکی با قمر بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ر‌وی تو ماه باشد و طرفه بود که ماه</p></div>
<div class="m2"><p>برجرم روشنش زره از مشک تر بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چندان ‌که وصف خوبی یوسف نموده‌اند</p></div>
<div class="m2"><p>ستوار نایدم‌که ز تو خوبتر بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یوسف اگر به چاهی وقتی نهفت چهر</p></div>
<div class="m2"><p>چاهی ترا به‌گرد زنخ مستتر بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یاقوت را به‌ گونه همی ماند آن دو لب</p></div>
<div class="m2"><p>الّا که در میانش دو رشته‌ گهر بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پر حلقه طرهٔ تو کتاب مجسطی است</p></div>
<div class="m2"><p>سر داده بسکه دایره یک با دگر بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کژدم سپر به سالی یک مه شد آفتاب</p></div>
<div class="m2"><p>دایم بر آفتاب تو کژدم سپر بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>(:‌ر حیرتم‌که چشم تو ماند از چه رو سقیم</p></div>
<div class="m2"><p>با اینهمه‌که در لب تو نیشکر بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>داند دل جریح ‌که ‌گاه نگه ترا</p></div>
<div class="m2"><p>درنوک مژه تعبیه صد نیشتر بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>د‌ر زیر دام زلف تو از خال دانه‌ایست</p></div>
<div class="m2"><p>کاین دانه دام مردم صاحبنظر بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قدت صنوبرست و ندیدم صنوبری</p></div>
<div class="m2"><p>کوهیش بر به زیر و مهی بر زبر بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>باشد به حکم عادت سیم و کمر به‌کوه</p></div>
<div class="m2"><p>چونست‌کوه سیم ترا درکمر بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پیمای نیست‌کوه سرین تو در خرم</p></div>
<div class="m2"><p>لرزان مدام از چه سبب اینقدر بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بلور ساده است‌ که چونین ز عکس او</p></div>
<div class="m2"><p>روشن سر او بام و در و بوم و بر بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اندر ازار سرخ بجای سرین تو</p></div>
<div class="m2"><p>نسرین به بار و سیم به خروار در بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مسکین دلم‌که در طلب سیم تو مدام</p></div>
<div class="m2"><p>همچون گدای گرسنه دل دربه‌در بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بی‌زر به‌کف نیاید سیم تو مر مرا</p></div>
<div class="m2"><p>اشکی بسان سیم و رخی همچو زر بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با زر چهر و سیم سرشکم بود محال</p></div>
<div class="m2"><p>کم بر مراد خاطر هرگز ظفر بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من آن زمان‌ که دادم تن در بلای عشق</p></div>
<div class="m2"><p>گشتم یقین‌ که جان و تنم در خطر بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون نیست درکنارم سروقدت چسود</p></div>
<div class="m2"><p>گر بی‌تو از سرشک‌کنارم شمر بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای غیرت ستاره ز هجر تو تا به‌ کی</p></div>
<div class="m2"><p>شب تا به صبح چشمم اختر شمر بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یک ره در آ به‌کلبه مسکین اگرچه تو</p></div>
<div class="m2"><p>قدت بزرگ وکلبهٔ ما مختصر بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چندین متاز توسن و دل را مکن خراب</p></div>
<div class="m2"><p>زین فتنه ترسمت‌که در آخر ضرر بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آخر نه خانهٔ دل ما ملک پادشاست</p></div>
<div class="m2"><p>دانی‌که شاه از همه جا باخبر بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شاهنشه زمانه محمد شه آنکه مهر</p></div>
<div class="m2"><p>هر صبح از سجود درش مفتخر بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گیهان خدای آنکش در حل و عقد ملک</p></div>
<div class="m2"><p>دستی قضا به قدرت و دستی قدر بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ظل خدا خدیو بشر کز طریق حق</p></div>
<div class="m2"><p>دارای ملک و ملت خیرالبشر بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>د‌ر روزکین به نهب روان‌ گفتئی اجل</p></div>
<div class="m2"><p>تیغ خمیده قامت او را پسر بود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گردون به‌کاخ‌دولت او چیست قبه‌ایست</p></div>
<div class="m2"><p>گیتی ز ملک شوکت او یک اثر بود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جوییست از محیط عطایش هرآنچه یم</p></div>
<div class="m2"><p>خشک و ترش به خوان کرم ماحضر بود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از مهر او بهشت برینست یک ورق</p></div>
<div class="m2"><p>وز قهر او لهیب سقر یک شرر بود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>صدره به چرخ نازد خاک از برای آنک</p></div>
<div class="m2"><p>رامش در او گزیده چنین تاجور بود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>د‌ر روز رزم و بزم ز شمشیر و جام می</p></div>
<div class="m2"><p>دستش هماره حامله خیر و شر بود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وقتی‌که جام جوید گوهرفشان شود</p></div>
<div class="m2"><p>وقتی‌ که تیغ‌ گیرد دشمن شکر بود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هرجا به عودسوزی رامش طلب ‌کند</p></div>
<div class="m2"><p>هرجا به‌کینه‌توزی پرخاشخر بود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جامش‌ موالیان را کوثر شود به طعم</p></div>
<div class="m2"><p>تیغش مخالفان را سوزان سقر بود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا از پس شکوفه شجر بارور شود</p></div>
<div class="m2"><p>یارب نهال دولت او بارور بود</p></div></div>