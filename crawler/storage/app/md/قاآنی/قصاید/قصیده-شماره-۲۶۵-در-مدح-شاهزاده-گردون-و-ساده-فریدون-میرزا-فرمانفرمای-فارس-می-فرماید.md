---
title: >-
    قصیدهٔ شمارهٔ ۲۶۵ - در مدح شاهزادهٔ گردون و ساده فریدون میرزا فرمانفرمای فارس می‌فرماید
---
# قصیدهٔ شمارهٔ ۲۶۵ - در مدح شاهزادهٔ گردون و ساده فریدون میرزا فرمانفرمای فارس می‌فرماید

<div class="b" id="bn1"><div class="m1"><p>به عزم پارس دل پارسایم از کرمان</p></div>
<div class="m2"><p>سفر گزید که حب‌الوطن من‌الایمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا عقیده‌ که روزی دوبار در شیراز</p></div>
<div class="m2"><p>به دوستان‌کهن بهینه نوینم پیمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گمانم آنکه چو در چشمشان شوم نزدیک</p></div>
<div class="m2"><p>چه‌نور چشم دهندم به‌چشم خویش مکان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ولیک غافل ازین ماجرا که مردم چشم</p></div>
<div class="m2"><p>ز چشم مردم هست ازکمال قرب نهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به صدهزار سکندر که ره‌نوردم خورد</p></div>
<div class="m2"><p>رهی سپردم چون عُمر خضر بی‌پایان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رهی ز بسکه درو جوی و جر به هر طرفش</p></div>
<div class="m2"><p>چو آسیا شده جمعی ز آب سرگردان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رهی نشیبش چندان‌که حادثات سپهر</p></div>
<div class="m2"><p>رهی فرازش چندان که نایبات زمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه بر شواهق او پرگشوده مرغ خیال</p></div>
<div class="m2"><p>نه در صحاری او پا نهاده پیک‌گمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عروج ختم رسل را به جسم زی معراج</p></div>
<div class="m2"><p>شدن بر اوج جبالش نکوترین برهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو جا به فارس ‌گزیدم دلم ‌گرفت ملال</p></div>
<div class="m2"><p>چو مومنی‌ که به دوزخ رود ز باغ جنان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا به کُنهِ شناسا ولی ز غایت بخل</p></div>
<div class="m2"><p>همه ز روی تحیر به روی من نگران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی به خنده‌ که این واعظیست از قزوین‌</p></div>
<div class="m2"><p>یکی به طعنه‌که ای فاضلیست از همدان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من از فراست فطری ز رازشان آگه</p></div>
<div class="m2"><p>ولی چه‌سود ز تشخیص درد بی‌درمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هزار گونه تذلل به جای آوردم</p></div>
<div class="m2"><p>یکی نکرد اثر در مناعت ایشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بلی دو صد ره اگر آبگینه نرم شود</p></div>
<div class="m2"><p>تفاوتی نکند سخت‌رویی سندان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به هر تنی ‌که نمودم سلام ‌گفت علیک</p></div>
<div class="m2"><p>ولی علیکی همچون علی مفید زیان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو حال اهل وط شد به م چنب عالی</p></div>
<div class="m2"><p>که می‌زنند ز حیلت بر آتشم دامان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگفتم ار همه از بهر دادخواهی محض</p></div>
<div class="m2"><p>قصیده‌یی بسرایم به مدحت سلطان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خدیو کشور جم مالک رقاب امم</p></div>
<div class="m2"><p>کیای ملک عجم داور زمین و زمان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سپهرکوکبه فرمانروای فارس ‌که هست</p></div>
<div class="m2"><p>تنش ز فرط لطافت نظیر آب روان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قصیده‌ گفتم و هر آفرین‌ که فرمودند</p></div>
<div class="m2"><p>مرا به جای صلت بود به زگنج روان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صلت نداد مرا زان سبب‌که خواست دلش</p></div>
<div class="m2"><p>که آشکار شود این لطیفهٔ پنهان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که درّ درّیِ نظم دریّ قاآنی</p></div>
<div class="m2"><p>چنان بهی‌که ادای بهای او نتوان</p></div></div>