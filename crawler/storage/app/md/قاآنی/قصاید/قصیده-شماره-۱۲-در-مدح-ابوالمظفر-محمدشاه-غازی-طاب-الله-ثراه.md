---
title: >-
    قصیدهٔ شمارهٔ ۱۲ - در مدح ابوالمظفر محمدشاه غازی طاب‌الله ثراه
---
# قصیدهٔ شمارهٔ ۱۲ - در مدح ابوالمظفر محمدشاه غازی طاب‌الله ثراه

<div class="b" id="bn1"><div class="m1"><p>چه مایه مایلی ای ترک ترک و خفتان را</p></div>
<div class="m2"><p>یکی بیاو میازار چهر الوان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوای جنگ چه داری نوای چنگ شنو</p></div>
<div class="m2"><p>به یک دو جام می‌کهنه تازه‌کن جان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شور و طیش چه‌دیدی به‌سور و عیش‌گرای</p></div>
<div class="m2"><p>که حاصلی به ازین نیست دور دوران را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سینه‌کینه بپرداز وکار آب بساز</p></div>
<div class="m2"><p>مزن بر آتش‌کین همچو باد دامان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چهارماهه نه‌بس‌ بود شور و فتنه و جنگ</p></div>
<div class="m2"><p>که باز زین زنی از بهرکینه یکران را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به زلفکان سیاهت به جای مشک و عبیر</p></div>
<div class="m2"><p>چه بینم این همه‌گرد و غبار میدان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از‌ین قبل‌که به بر بینمت سلیح نبرد</p></div>
<div class="m2"><p>گمان برم‌که خلف مر تویی نریمان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو فتنه‌کردی و تاجیک و ترک متهمند</p></div>
<div class="m2"><p>که ره به فتنه‌گشودند ملک سلطان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه از نبایر سلمی نه از نتایج تور</p></div>
<div class="m2"><p>تراکه‌گفت‌که ویران نمایی ایران را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کمان و تیرت اگر نفس آرزو دارد</p></div>
<div class="m2"><p>کمان ابرو بنمای و تیر مژگان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ورت به خود و زره دل‌کشد یکی بگذار</p></div>
<div class="m2"><p>چو خود بر سر آن‌گیسوی زره‌سان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس است آن زنخ و زلف‌گوی و چوگانت</p></div>
<div class="m2"><p>چه مایلی هله این‌قدرگوی و چوگان را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی ز بند حوادث‌گشایش ار طلبی</p></div>
<div class="m2"><p>درآ به حجره و بگشای بند خفتان را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ورت هواست‌که در فارس فتنه بنشیند</p></div>
<div class="m2"><p>یکی ز خلق بپوش آن دو چشم فتان را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیار از آن می چون ارغوان‌که مدحت آن</p></div>
<div class="m2"><p>میان جمع به رقص آورد سخندان را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو در شود به‌گلوی خورنده از دل جام</p></div>
<div class="m2"><p>ز دل برون فکند رازهای پنهان را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از آن شراب‌که‌گر بیندش‌کسی شب تار</p></div>
<div class="m2"><p>کند نظاره به ظلمات آب حیوان را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بده بگیر بنوشان بنوش تا ز طرب</p></div>
<div class="m2"><p>تو عشوه سازکنی من مدیح سلطان را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خدیو راد محمد شه آن‌که ملکت او</p></div>
<div class="m2"><p>ز هرکرانه محیط‌است ملک امکان را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ندانما به چه بستایمش‌که شوکت او</p></div>
<div class="m2"><p>گشاده ز آن سوی بازار وهم دکان را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به خلق پارس بس این رحمتش‌که برهانید</p></div>
<div class="m2"><p>ز چنگ حادثه یک مملکت مسلمان را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگرچه حاکم و محکوم را نبودگناه</p></div>
<div class="m2"><p>که‌کس نداند علت قضای یزدان را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سخن درازکشد عفو شه بس اینکه سپرد</p></div>
<div class="m2"><p>زمام ملک سلیمان امیر دیوان را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بزرگوار امیری‌که با سیاست او</p></div>
<div class="m2"><p>به چار رکن جهان نام نیست طغیان را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز موشکافی تدبیر موکشان آرد</p></div>
<div class="m2"><p>به خاک تیره ز هفتم سپهرکیوان را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به جامه خانهٔ جودش ندیده چشم جهان</p></div>
<div class="m2"><p>جز آفتاب جهانتاب هیچ عریان را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نظام‌کار جهان پیرو عزیمت تست</p></div>
<div class="m2"><p>چنانکه حس عمل تابع است ایمان را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به‌عهد عدل توصبحست وبس اگربه‌مثل</p></div>
<div class="m2"><p>تنی به دست تظلم دردگریبان را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سبب وجود تو بود ارنه بر فریشتگان</p></div>
<div class="m2"><p>هگرز برنگزیدی خدای انسان را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کشند صورت شمشیرت ار به باغ بهشت</p></div>
<div class="m2"><p>بهشتیان همه مایل شوند نیران را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز روی صدق‌گواهی دهدکه خلد اینست</p></div>
<div class="m2"><p>اگر به بزم تو حاضرکنند رضوان را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خدانمونه‌یی‌ازطول‌وعرض‌جاه توخواست</p></div>
<div class="m2"><p>که آفرید به یک امرکن دوکیهان را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جنایتی‌که به‌کیهان رسد زکید سپهر</p></div>
<div class="m2"><p>کف‌کریم تو آماده است تاوان را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ترشح کرمت گرد آز بزداید</p></div>
<div class="m2"><p>چنان‌که آب ستغفار لوث عصیان را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زمانه بی‌مدد حزم تو ندارد نظم</p></div>
<div class="m2"><p>که بی‌خرد اثر نطق نیست حیوان را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به آب و آینه ماند ضمیر روشن تو</p></div>
<div class="m2"><p>که آشکارکند رازهای پنهان را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به دست راد تو بیچاره ابرکی ماند</p></div>
<div class="m2"><p>چه جرم‌کرده‌که مستوجبست بهتان را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کدام ابر شنیدی‌که فیض یک‌دمه‌اش</p></div>
<div class="m2"><p>دهد به در وگهر غوطه ملک امکان را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برنده تیغ تو ویحک چگونه الماسیست</p></div>
<div class="m2"><p>که روز معرکه آبستن است مرجان را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بسان آتش سوزنده صارم قهرت</p></div>
<div class="m2"><p>جداکند ز موالید چهار ارکان را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بتابد ازکف رخشنده‌ات به روز مصاف</p></div>
<div class="m2"><p>بسان برق‌که بشکافد ابر نیسان را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تبارک‌الله از آن خنگ‌کوه‌کوههٔ تو</p></div>
<div class="m2"><p>که بر نطاق نهم چرخ سوده‌کوهان را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>پیش ز پویه دهانش زکف تنش ز عرق</p></div>
<div class="m2"><p>نمونه‌ایست عجب باد و برف وباران را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گمان بری‌که معلق نموده‌اند به سحر</p></div>
<div class="m2"><p>ز چارگوشهٔ البرز چار سندان را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به غیر شخص‌کریمت برو نیافته‌کس</p></div>
<div class="m2"><p>فرازکوه دماوند بحر عمان را</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مطیع تست به هرحال در شتاب و درنگ</p></div>
<div class="m2"><p>چنان‌که باد مطاوع بدی سلیمان را</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مگر نمونهٔ وی خواست آفرید خدای</p></div>
<div class="m2"><p>که آفرید دماوند وکوه ثهلان را</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>قوی قوایم او خاک را بتوفاند</p></div>
<div class="m2"><p>چنانکه باد به‌گرداب لجه طوفان را</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بزرگوار امیرا تویی‌که همت تو</p></div>
<div class="m2"><p>زیاد برده عطایای معن و قاآن را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دوسال و پنج‌مه ایدون رودکه بنده‌به‌فارس</p></div>
<div class="m2"><p>شنوده در عوض مدح قدح نادان را</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>متاع من همه شعرست و او بس ارزانست</p></div>
<div class="m2"><p>یکی بگو چکنم این متاع ارزان را</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کسش ز من نخرد ور خرد بنشناسد</p></div>
<div class="m2"><p>ز پشک مشک وز خرمهره در غلطان را</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تویی‌که قدر سخن دانی و عیار هنر</p></div>
<div class="m2"><p>برآن صفت‌که پیمبر رموز قرآن را</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ولی تو نظم پریشانم آن زمان شنوی</p></div>
<div class="m2"><p>که نظم بخشی یک مملکت پریشان را</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چه‌باشد این دو سه مه تا تو نظم‌کار دهی</p></div>
<div class="m2"><p>ببنده بار دهی خاکبوس خاقان را</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مرا مگو چو ترا نیست سازو برگ سفر</p></div>
<div class="m2"><p>هلا چگونه‌کنی جزم عزم طهران را</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز ساز و برگ سفر یک اراده دارم و بس</p></div>
<div class="m2"><p>که هست حامله صدگونه برگ و سامان را</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بدان ارادهٔ تنها اگر خدا خواهد</p></div>
<div class="m2"><p>نبشت خواهم‌کوه و در و بیابان را</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به‌جز تو از تونخواهم‌که‌نافریده خدای</p></div>
<div class="m2"><p>عظیم‌تر ز وجود تو هیچ احسان را</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زوال و نقص مبیناد عز و جاه امیر</p></div>
<div class="m2"><p>چنانکه فضل خداوندگار پایان را</p></div></div>