---
title: >-
    قصیدهٔ شمارهٔ ۲۷۴ - فی‌المدیحه ایضاً
---
# قصیدهٔ شمارهٔ ۲۷۴ - فی‌المدیحه ایضاً

<div class="b" id="bn1"><div class="m1"><p>دوش چون شد رشتهٔ پروین عیان از آسمان</p></div>
<div class="m2"><p>دیده‌ام پروین‌فشان شد دامنم پروین‌نشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر زمین از بس هجوم آورد اشکم چون نجوم</p></div>
<div class="m2"><p>می‌نیارستم زمین را فرق‌ کرد از آسمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برق آهم مشعلی افروخت درگیتی‌که‌گشت</p></div>
<div class="m2"><p>از برون جامه راز خاطر مردم عیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسکه‌ گرداگرد من صف‌صف هجوم آورد غم</p></div>
<div class="m2"><p>جهد می‌کردم‌که خود را بازجویم از میان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاهی از بس زردی رخساره بودم بیم آنک</p></div>
<div class="m2"><p>سایدم بر جبهه هندویی به جانی زعفران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الغرض بودم درین حالت‌که ناگه دررسید</p></div>
<div class="m2"><p>بر سرم آن سرو بالا چون بلای ناگهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی خطا گفتم بلایی به ز عیش مستدام</p></div>
<div class="m2"><p>نی غلط گفتم فنایی به ز عمر جاودان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلف یک خروار سنبل چهره یک ‌گلزار گل</p></div>
<div class="m2"><p>لعل یک انبار مل‌گیسوش یک مِضمار جان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فتنهٔ یک خانقه تقوی ز چشم دلفریب</p></div>
<div class="m2"><p>دشمن یک صومعه طاعت ز خال دلسنان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آفت یک روم ترسا از دو پرچین سلسله</p></div>
<div class="m2"><p>غارت یک دیر راهب از دو مشکین طیلسان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زلف‌ چون‌ شام‌ محرم چهره همچون صبح عید</p></div>
<div class="m2"><p>صبح عیدش را شده شام محرم سایبان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در دهان او سخن چونان و‌جودی در عدم</p></div>
<div class="m2"><p>بر میان او کمر چونان یقینی بر گمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روی سیمینش سپر گیسوی مشکینش کمند</p></div>
<div class="m2"><p>زلف پرچینش زره مژگان خون‌ریزش سنان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر قدش گیسو چو ماری بر فراز نارون</p></div>
<div class="m2"><p>در لبش دندان چو دری در میان ناردان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم رخش در زیر زلف و هم خطش بر گرد لب</p></div>
<div class="m2"><p>غاتفر در زنگبار و نوبه در هندوستان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از فسون چشم بربستم زبان آری به سحر</p></div>
<div class="m2"><p>ساحر از بادام مردم را کند عقد اللسان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رویش اندر طرّهٔ مشکین قمر در سنبله</p></div>
<div class="m2"><p>خالش اندر چهرهٔ سیمین زحل بر فرقدان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عشق دارد مار بر سرو روان ‌گر منکری</p></div>
<div class="m2"><p>زلف چون مارش ببین بر قد چون سرو روان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با دو لعل نوشخندش می‌ننوشم نیشکر</p></div>
<div class="m2"><p>با دو زلف درع‌پوشش می‌نبویم ضیمران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غیر زلف چون دخانش بر رخان آتشین</p></div>
<div class="m2"><p>می‌ندیدم کز هوا سوی زمین یازد دخان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زلف او بر روی سیمین عقربی در ماهتاب</p></div>
<div class="m2"><p>جعد او بر چهر رنگین‌ سنبلی بر ارغوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زلف بر دوشش عزازیلی به دوش جبرئیل</p></div>
<div class="m2"><p>دل در آغوشش دماوندی میان پرنیان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عشق‌او را هفت وادی بود و من در هر یکش</p></div>
<div class="m2"><p>زحمتی دیدم‌که دید اسفندیار از هفتخان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آتشین ‌رویش ‌چو دیدم‌ جستم ‌از جا چون سپند</p></div>
<div class="m2"><p>وز سپندش عقل را آتش زدم در دودمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفتمش ای ترک غارتگر که در اقلیم حسن</p></div>
<div class="m2"><p>نیکوان را شهریاری دلبران را قهرمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کوه را دزدی و پوشی در قصب‌ کاینم سرین</p></div>
<div class="m2"><p>موی را آری و بندی درکمر کاینم میان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تاکی از دردت بمیرم‌گفت بخ‌بخ‌ گو بمیر</p></div>
<div class="m2"><p>تاکی از هجرت نمانم ‌گفت هی‌هی‌ گو ممان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفتمش یارم ‌که باشد در غمت ‌گفتا اجل</p></div>
<div class="m2"><p>گفتمش ‌کارم چه باشد بی‌رخت‌ گفتا فغان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفتمش شب بی‌تو ناید خواب اندر چشم من</p></div>
<div class="m2"><p>گفت آری خواب می‌ناید به چشم پاسبان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفتم از وصل دهانت تا به‌ کی جویم اثر</p></div>
<div class="m2"><p>گفت تا آن گه ‌که جویی از دهان من نشان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گفتم آخر بر رخ من از چه خندی شرم دار</p></div>
<div class="m2"><p>گفت هی‌هی می‌ندانی خنده آرد زعفران</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گفتم ای‌گلچهره چون من باغبانی بایدت</p></div>
<div class="m2"><p>گفت رو رو من نیم آن‌ گل ‌که خواهد باغبان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گفتمش ای ترک چون من ترجمانی شایدت</p></div>
<div class="m2"><p>گفت بخ‌بخ من نه آن ترکم‌ که جوید ترجمان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفتم آخر چند ماند راز جورت سر به مهر</p></div>
<div class="m2"><p>مهر بردار از ضمیر و قفل بگشا از زبان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گفت ای ابله‌ ندانی اینقدرکز وصل تو</p></div>
<div class="m2"><p>من همان بینم‌که بیندگلشن از باد خزان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بی‌نشانی چون تو را چون من نشاید همنشین</p></div>
<div class="m2"><p>میزبانی ‌چون ترا چون من نباید میهمان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>طره‌ام‌ ماری نه‌ کش چنگ‌تو باشد مارگیر</p></div>
<div class="m2"><p>غبغبم‌گویی نه کش دست تو باشد صولجان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو ب ه‌قامت چو‌ن ‌کمانی من به‌ قامت همچو تیر</p></div>
<div class="m2"><p>تیر پران بگذرد چون جفت‌ گردد باکمان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>با چنین رخسار منکر با چنین اندام زشت</p></div>
<div class="m2"><p>اینقدر حجت مجوی و اینقدر طیبت مران</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>منظر زیبا نداری یار زیبارو مخواه</p></div>
<div class="m2"><p>منطق شیرین نداری شوخ شیرین‌لب مخو‌ان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>روی زشت خود ندیدستی مگر در آینه</p></div>
<div class="m2"><p>تا به‌جهد از خود گریزی قیروان تا قیروان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>صورت زشت ترا صورتگری ‌گر برکشد</p></div>
<div class="m2"><p>کلکش از تأثیر آن صورت بخوشد در بنان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بر رخ زردت ز هر جانب نشان آبله</p></div>
<div class="m2"><p>پشهٔ خاکیست مانان بر برازی پرفشان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بینیت چون ناودان و آب ازو جاری چنانک</p></div>
<div class="m2"><p>روز بارانش نشاید فرق‌ کرد از ناودان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>روی زشتت ‌گر شود در صورت بت جلوه‌گر</p></div>
<div class="m2"><p>کافرم‌ گر هیچ ‌کافر بت پرستد در جهان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ورکسی نامت‌کند بر درهم و دینار نقش</p></div>
<div class="m2"><p>درهم و دینار راکس می‌نگیرد رایگان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر نمایی روی من با روی زشت خود قیاس</p></div>
<div class="m2"><p>آزمون آیینه را برگیر و در شبهت ممان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مار را نسبت‌گنه باشد به طاووس ارم</p></div>
<div class="m2"><p>خار را شبهت خطا باشد به ‌گلزار جنان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ور توگویی وصل‌من بس دلکشست و دلپذیر</p></div>
<div class="m2"><p>یک‌ نفس با چون خودی بنشین ز روی امتحان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تا چه‌ کردستم‌ گنه تا با تو باشم همنشین</p></div>
<div class="m2"><p>یا چه ‌کردستم خطا تا با تو باشم در غمان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مر ترا طاعت چه باشد تا خدایت در جزا</p></div>
<div class="m2"><p>از وصال چون منی بخشد حیات جاودان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یا مرا عصیان چه باشد تا به‌کیفر کردگار</p></div>
<div class="m2"><p>از جمال چون تویی‌گوید به دوزخ ‌کن مکان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گاه خوانی سست‌مهرم هستم آری اینچنین</p></div>
<div class="m2"><p>گاه خوانی سخت رویم هستم آری آن چنان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سخت‌رویستم‌ولی‌با ون‌تو یاری سست‌طبع</p></div>
<div class="m2"><p>سست ‌مهرستم ‌ولی ‌با چون ‌تو خاری سخت ‌جان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>راستی را در شگفتستم ز اطوار سپهر</p></div>
<div class="m2"><p>راستی را در شگرفستم ز ادوار جهان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کز چه هرجا غرچه‌یی دنگی دبنگی دیورنگ</p></div>
<div class="m2"><p>ابلهی گولی فضولی ناقبولی قلتبان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>الکنی کوری کری لنگی شلی زشتی کلی</p></div>
<div class="m2"><p>بدسرشتی ا‌حولی زشتی نحیفی ناتوان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ساده‌یی گیرد صبیح‌و دلبری‌خواهد ملیح</p></div>
<div class="m2"><p>همسری‌ خو‌اهد جمیل‌ و شاهدی جوید جوان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کوبکو تازان‌که گردد با نگاری همنشین</p></div>
<div class="m2"><p>دربدر یازان‌ که گردد با ظریفی رایگان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گر تجنب بیند از یاری بگرید ابروار</p></div>
<div class="m2"><p>ور تقرب بیند از شوخی بخندد برق‌سان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گاه با معشوق‌ گوید اینت جور بی‌حساب</p></div>
<div class="m2"><p>گاه با منظور گوید اینت ظلم بی‌کران</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دلبر مظلوم از خجلت بنسراید سخن</p></div>
<div class="m2"><p>شاهد محجوب‌از حسرت بنگشاید زبان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>خود نماید جور و از معشوق نالد هر نفس</p></div>
<div class="m2"><p>خود اید ظلم و از محبوب موید هر زمان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>جور آن این ببن‌ که ‌گردد با نگاری مقترن</p></div>
<div class="m2"><p>ظلم آن این بس‌که جوید با جوانی اقتران</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>آن ازین جفت نشاط و این ازان یار محن</p></div>
<div class="m2"><p>این ازان اندر جحیم و آن ازین اندر جنان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>راستی را دلبری دیوانه باید همچو من</p></div>
<div class="m2"><p>تا مگر با زشت‌رویی چون تو گردد توأمان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چشم خیره‌ خشم چیره‌ روی تیره‌ خوی زشت</p></div>
<div class="m2"><p>رخ‌گره نخوت فره صورت زره قامت‌کمان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بخت لاغر رنج فربه مغز خالی جهل پر</p></div>
<div class="m2"><p>غم‌ فراوان دل‌نوان دانش سبک خاطرگران</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>آه سرد و اشک ‌گرم و روح زار و تن نزار</p></div>
<div class="m2"><p>روی‌سخت ‌و طبع‌سست ‌و جان‌نژند و دل‌نوان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>قامت پست تو بینم یا رخ پر آبله</p></div>
<div class="m2"><p>هیکل زفت تو بینم یا دل نامهربان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تو چه بینی از من آن بینی‌ که راغ از فرودین</p></div>
<div class="m2"><p>من چه یابم از تو آن یابم ‌که باغ از مهرگان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>تو مرا باب ملالی من ترا آب زلال</p></div>
<div class="m2"><p>تو مرا رنج روانی من ترا گنج روان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>من ترا دار نعیمم تو مرا نار جحیم</p></div>
<div class="m2"><p>من ترا باغ جنانم تو مرا داغ جنان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تو مرای دشمن جان م‌ن مرایی همنشین</p></div>
<div class="m2"><p>من ترایم راحت تن چون ترایم همعنان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>من چه بینم از تو آن بینم ‌که از صرصر چراغ</p></div>
<div class="m2"><p>تو چه بینی از من آن بینی که از راح روان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تو مرا آن‌زحمتی‌کش وصف بیرون از حدیث</p></div>
<div class="m2"><p>من ترا آن رحمتم‌کش مدح بیرون از بیان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>نه ترا یزدان فرستد رحمتی برتر ازین</p></div>
<div class="m2"><p>نه مراگیهان پسندد زحمتی برتر از آن</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>وصل تو مرگست و مرگ از عمر نگذارد اثر</p></div>
<div class="m2"><p>روی تو رنجست و رنج از شخص برباید توان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>عشقبازی ‌چون ‌تو زشت و شاهدی ‌زیبا چو من</p></div>
<div class="m2"><p>فی‌المثل دانی چه باشد آسمان و ریسمان</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>این بود انصاف یارب ‌کز وصال چون تویی</p></div>
<div class="m2"><p>من بباشم ناامید و من بباشم ناتوان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>وین روا باشد خدا را کز وصال چون منی</p></div>
<div class="m2"><p>تو بپایی شادکام و تو بمانی شادمان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>با تو چون باشم نباشد هیچم از شادی اثر</p></div>
<div class="m2"><p>با تو چون مانم نماند هیچم از عشرت نشان</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>رنج بیند پادشا چون با گدا گردد قرین</p></div>
<div class="m2"><p>نحس گردد مشتری چون‌ با زحل جوید قران</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>خوشدلی را مایه‌یی باید مرا بسرای هین</p></div>
<div class="m2"><p>‌نیکویی را آیتی شاید مرا بنمای هان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ای‌دریغاکاکی سمای خود دیدی به چشم</p></div>
<div class="m2"><p>تا به پای ‌خویشتن از خویشتن جستی‌ کران</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>تو اگر بوسی مرا بوسیده‌یی مه را جبین</p></div>
<div class="m2"><p>من اگر بوسم ترا بوسیده‌ام خر را فلان</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گر مرا خواهی دعایی کرد باری کن چنین</p></div>
<div class="m2"><p>کز وصال چون تویی دارد خدایم در امان</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>گفتم ای سرو قباپوش اینهمه توسن متاز</p></div>
<div class="m2"><p>گفتم ای ماه ‌کله‌دار اینقدر مرکب مران</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>غمزهای دلبران را رمزها باشد نهفت</p></div>
<div class="m2"><p>نازهای نیکوان را رازها باشد نهان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>حسن بامی‌هست‌عالی‌نردبانثن چیست عشق</p></div>
<div class="m2"><p>هیچکس بر بام می‌نتوان شدن بی‌نردبان</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>عشق خسرو کرد شکر را به شیرینی مثل</p></div>
<div class="m2"><p>ورنه شکر نام بسیارستی اندر اصفهان</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>هم عرب را بوده چون لیلی هزاران دلفریب</p></div>
<div class="m2"><p>هم عجم را بوده چون شیرین هزاران دلستان</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>شور مجنونی مر او راکرد معروف زمن</p></div>
<div class="m2"><p>شوق فرهادی مر این را ساخت مشهور زمان</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>از زلیخا یوسف اندر خوبرویی شد مثل</p></div>
<div class="m2"><p>ازکثیر عزه‌ا عزت یافت در ملک جهان</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>گر نبودی وامق از عذرا که پرسیدی اثر</p></div>
<div class="m2"><p>ور نبودی‌عروه از عفراکه دانستی نشان</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>هندویی خورشید رخشان ‌را ستایش می‌نکرد</p></div>
<div class="m2"><p>تا نه زاول حیرت حربا فکندش درگمان</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>شمع از جانبازی پروانه آمد سرفراز</p></div>
<div class="m2"><p>ویس از دل بردن رامین مثل شد در جهان</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>سروکی بالد به بستان‌گر ننالد فاخته</p></div>
<div class="m2"><p>گُل‌کجا خندد به‌ گلزار ار نزارد زندخوان</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>گر نبودی داستان توبه و لیلی مثل</p></div>
<div class="m2"><p>از حد اوهام نامی می‌نبودی در میان</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ور جمیل از دل نبودی طالب حسن جمال</p></div>
<div class="m2"><p>کافرم ‌گر هیچ راندی از بُثینه‌ داستان</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>شاعر ماهر چو فردوسی ببایستی همی</p></div>
<div class="m2"><p>تا به دهر اندر خبر ماندی زگرد سیستان</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>مفلقی دانا چو خاقانی بشایستی همی</p></div>
<div class="m2"><p>تا به دوران داستان‌گویدکس از شاه اخستان</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>لاجرم باید چو قاآنی ادیبی هوشمند</p></div>
<div class="m2"><p>تا به‌ گیتی داستان ماند ز شاه راستان</p></div></div>