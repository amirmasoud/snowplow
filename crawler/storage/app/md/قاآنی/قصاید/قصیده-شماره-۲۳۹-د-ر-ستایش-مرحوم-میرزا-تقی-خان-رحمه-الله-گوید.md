---
title: >-
    قصیدهٔ شمارهٔ ۲۳۹ - د‌ر ستایش مرحوم میرزا تقی خان رحمه‌الله‌ گوید
---
# قصیدهٔ شمارهٔ ۲۳۹ - د‌ر ستایش مرحوم میرزا تقی خان رحمه‌الله‌ گوید

<div class="b" id="bn1"><div class="m1"><p>هرآنچه هست مه و سال و هفته و ایام</p></div>
<div class="m2"><p>خجسته بادا چون عید بر امیر نظام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهین اتابک اعظم خدایگان صدور</p></div>
<div class="m2"><p>قوام ملت بدر زمانه صدر انام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهی رسیده به جایی‌ که با جلالت تو</p></div>
<div class="m2"><p>جهان و صد چو جهان را کسی نپرسد نام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به عهد عدل تو آهو خیال لاله‌ کند</p></div>
<div class="m2"><p>اگر به دشت نهد پا به دیدهٔ ضرغام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر که کلک تو مهدست و ملک طفل رضیع</p></div>
<div class="m2"><p>که تا نجنبد این‌یک نگیردی آلام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بس حروف و معانی بهم سبق جویند</p></div>
<div class="m2"><p>به وقت مدح وام لکنت اوفتد به‌ کلام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هنوز بر شب و روز زمانه مشتبهست</p></div>
<div class="m2"><p>که مهر و ماه‌ کدامست و طلعت تو کدام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نهفته راز دو گیتی به چشم فکرت تو</p></div>
<div class="m2"><p>بر آن صفت‌ که دو مغز اندرون یک بادام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به روز باد چسان پشه می‌شود عاجز</p></div>
<div class="m2"><p>به‌گاه مدح تو آنگونه عاجزند اوهام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عروس ملک‌جهان چون به عقد دائم تست</p></div>
<div class="m2"><p>حلال بر تو و بر هر که غیر تست حرام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز اختیار خود آن‌دم زمانه دست بشست</p></div>
<div class="m2"><p>که در کف تو نهاد آسمان زمام مهام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مسلمست ‌که انجم سپر بیندازند</p></div>
<div class="m2"><p>چو تیغ زرین خورشید برکشد ز نیام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهان اگر به توگیرد سبق ملول مشو</p></div>
<div class="m2"><p>که هم ز پیشی صفرست بیشی ارقام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو چون در آخر دور زمانه خلق شدی</p></div>
<div class="m2"><p>همی حسد برد آغاز دهر بر انجام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه هر که تیر و کمان برگرفت و گرز و کمند</p></div>
<div class="m2"><p>به وقعه گردد زال و به حمله گردد سام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به زور مرد مبارز بُرنده ‌گردد تیغ</p></div>
<div class="m2"><p>به شور بادهٔ‌ گلرنگ مستی آرد جام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نشان بازوی شیر خدا ز مرحب پرس</p></div>
<div class="m2"><p>کزو بنالد نز ذوالفقار خون‌آشام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر نه قوت بازوی حیدری بودی</p></div>
<div class="m2"><p>ز ذوالفقار بدی شهره‌تر هزار حسام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپهر عالی خواهد چو خاک پست شود</p></div>
<div class="m2"><p>بدین امید که روزی ببوسدت اقدام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که‌ گفت‌ کام تو می‌بخشد آسمان و زمین</p></div>
<div class="m2"><p>که آسمان و زمین هر دو را تو بخشی‌کام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر نه مهر تو پیوند جان به تن دادی</p></div>
<div class="m2"><p>گسسته بودی جان را علاقه از اجسام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر فضایل حلمت به‌کوه برخوانند</p></div>
<div class="m2"><p>همی ز شرم چو ابرش عرق چکد ز مسام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جهان و هرچه درو هست با جلالت تو</p></div>
<div class="m2"><p>چو رود نزد محیطست و دود پیش غمام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز همّت تو جمادات نیز در طربند</p></div>
<div class="m2"><p>جماد را نبود گرچه روح در اندام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنانکه‌ روح نباشد عظام را لیکن</p></div>
<div class="m2"><p>به تن هم از اثر روح زنده‌اند عظام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>میان اینهمه رایات‌ کفر در عالم</p></div>
<div class="m2"><p>تو برفراشتی اعلام دولت اسلام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو شیر غژمان تب داشتم مگیر آهو</p></div>
<div class="m2"><p>برین قصیده‌که طبعم بدیهه ‌کرد تمام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>منم پیمبر نظم و پیمبران را نیست</p></div>
<div class="m2"><p>به فکرکردن حاجت چو دررسد الهام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرا بپرور کاین شعرها که می‌شنوی</p></div>
<div class="m2"><p>بود چو عمر تو پاینده تا به روز قیام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همیشه تا که پری را ز آهنست‌ گریز</p></div>
<div class="m2"><p>هماره تاکه عرض را به جوهرست قوام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به طلعت تو شود شام ‌دوستان تو صبح</p></div>
<div class="m2"><p>به هیبت تو بود صبح دشمنان تو شام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ترا خدای معین باد و پادشاه ناصر</p></div>
<div class="m2"><p>ترا سعود قرین باد و روزگار غلام</p></div></div>