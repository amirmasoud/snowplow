---
title: >-
    قصیدهٔ شمارهٔ ۱۸۴ - د‌ر تهنیت عید غدیر و ستایش وزریر بی‌نظیر صدراعظم میرزا آقاخان دا‌م اقباله
---
# قصیدهٔ شمارهٔ ۱۸۴ - د‌ر تهنیت عید غدیر و ستایش وزریر بی‌نظیر صدراعظم میرزا آقاخان دا‌م اقباله

<div class="b" id="bn1"><div class="m1"><p>شراب تاک ننوشم دگر ز خمّ عصیر</p></div>
<div class="m2"><p>شراب پاک خورم زین سپس ز خم غدیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مهر ساقی‌کوثر از آن شراب خورم</p></div>
<div class="m2"><p>که دُرد ساغر او خاک را کند اکسیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن شراب‌کزان هرکه قطره‌یی بچشد</p></div>
<div class="m2"><p>شود ز ماحصل سرّ کاینات خبیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جان خواجه چنان مست آل‌یاسینم</p></div>
<div class="m2"><p>که آید از دهنم جای باده بوی عبیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوصد قرابه شراب ار به یک نفس بخورم</p></div>
<div class="m2"><p>که مست‌تر شوم اصلا نمی‌کند توفیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب مدار که‌ گوهرفشان شوم امروز</p></div>
<div class="m2"><p>که صد هزارم دریا ست در درون ضمیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دمیده صبح جنونم چنانکه بروی دم</p></div>
<div class="m2"><p>ز قل اعوذ برب الفلق دمد زنجیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر آن مبین ‌که چو خورشید چرخ عریانم</p></div>
<div class="m2"><p>بر آن نگر که جهان را دهم لباس حریر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نهفته مهر نبی‌گنج فقر در دل من</p></div>
<div class="m2"><p>که گنج نقره نیرزد برش به نیم نقیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فقیر را به زر و سیم و گنج چاره‌ کنند</p></div>
<div class="m2"><p>ولی علاج ندارد چو گنج‌ گشت فقیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر چه عید غدیرست و هر گنه‌ که‌ کنند</p></div>
<div class="m2"><p>ببخشد از کرم خویش‌ کردگار قدیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ولیک با دهن پاک و قلب پاک اولی است</p></div>
<div class="m2"><p>که نعت حیدر کرّار را کنم تقریر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نسیم رحمت یزدان قسیم جنت و نار</p></div>
<div class="m2"><p>خدیو پادشهان پادشاه عرش سریر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دروغ باشد اگر گویمش نظیری هست</p></div>
<div class="m2"><p>ولیک شرک اگر گویمش ‌که نیست نظیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لباس واجبی از قامتش بلندترست</p></div>
<div class="m2"><p>ولیک جامهٔ امکان ز قد اوست قصیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر بگویم حق نیست ‌گفته‌ام ناحق</p></div>
<div class="m2"><p>وگر بگویم حقست ترسم از تکفیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بزرگ آینه‌یی هست در برابر حق</p></div>
<div class="m2"><p>که‌هرچه هست سراپا دروست عکس‌پذیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نبد ز لوح مشیت بزرگتر لوحی</p></div>
<div class="m2"><p>که نقش‌بند ازل صورتش‌کند تصویر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دمی‌ که رحمتش از خلق سایه برگیرد</p></div>
<div class="m2"><p>هماندم از همه اشیا برون رود تاثیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زهی به درگه امر تو کاینات مطیع</p></div>
<div class="m2"><p>زهی به ربقهٔ حکم تو ممکنات اسیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه جای قلعهٔ خیبر که روز حملهٔ تو</p></div>
<div class="m2"><p>به عرش زلزله افتد چو برکشی تکبیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تویی یدالله و آدم صنیع رحمت تست</p></div>
<div class="m2"><p>که‌کرده‌ای‌گل او را چهل صباح خمیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گمانم افتد کابلیس هم طمع دارد</p></div>
<div class="m2"><p>که عفو عام تو آخر ببخشدش تقصیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به هیچ خصم نکردی قفا مگر آندم</p></div>
<div class="m2"><p>که عمروعاص قفا برزد از ره تزویر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شد از غلامی تو صدر شه امیر جهان</p></div>
<div class="m2"><p>بلی غلام تو بر کاینات هست امیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خجسته خواجهٔ اعظم جمال دولت و دین</p></div>
<div class="m2"><p>که‌ کمترین اثر قدر اوست چرخ اثیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به دل رؤوف و به دین کامل و به عدل تمام</p></div>
<div class="m2"><p>به ‌کف جواد و به رخ ثاقب و به رای بصیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هزار ملک منظم‌کند به یک‌گفتار</p></div>
<div class="m2"><p>هزار شهر مسخر کند به یک تدبیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نظیر ضرب ‌کسورست سعی حاسد او</p></div>
<div class="m2"><p>که هر چه‌ کوشد تقلیل یابد از تکثیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به خواب صدرا دیشب بهشت را دیدم</p></div>
<div class="m2"><p>بهشت روی تو بودش سحرگهان تعبیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به مصحف آیت یحیی‌ العظام برخواندم</p></div>
<div class="m2"><p>به زنده‌ کردن جود تو کردمش تعبیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مدیح رای منیرت زبر توانم خواند</p></div>
<div class="m2"><p>ولی نیارم خواندن‌گرش‌کنم تحریر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از آن‌ سبب‌ که‌ چو خورشید سطر مدحت آن</p></div>
<div class="m2"><p>به هیچ چشم نیاید ز بسکه هست منیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به عید قربان از حال این فدایی خویش</p></div>
<div class="m2"><p>چرا خبر نشدی ای ز راز دهر خبیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو آفتابی و بر آفتاب عاری نیست</p></div>
<div class="m2"><p>که هم به ذره بتابد اگر چه هست حقیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همیشه تا که به پیری مثل بود عالم</p></div>
<div class="m2"><p>فدای بخت جوان تو باد عالم پیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هماره پیش سریر ملک دو کار بکن</p></div>
<div class="m2"><p>به دوستان سریر و به دشمنان شریر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بگو بیار بیاور بده ببخش و بپاش</p></div>
<div class="m2"><p>بکش بکوب بسوزان بزن ببند بگیر</p></div></div>