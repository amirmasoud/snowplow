---
title: >-
    قصیدهٔ شمارهٔ ۲۸۶ - در ستایش شاهزادهٔ آزاد شجاع السلطنه مرحوم حسنعلی میرزا طاب‌ لله ثراه فرماید
---
# قصیدهٔ شمارهٔ ۲۸۶ - در ستایش شاهزادهٔ آزاد شجاع السلطنه مرحوم حسنعلی میرزا طاب‌ لله ثراه فرماید

<div class="b" id="bn1"><div class="m1"><p>آوخ آوخ‌که شد پسرعم من</p></div>
<div class="m2"><p>مایهٔ رنج و محنت و غم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من شده شادی مجرّد او</p></div>
<div class="m2"><p>او شده غصهٔ مجسم من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم ز من عشرت پیاپی او</p></div>
<div class="m2"><p>هم از و غصهٔ دمادم من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچه از من به دیگرن بخشد</p></div>
<div class="m2"><p>شده از خرج‌کیسه حاتم من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من چو سهرابم اوفتادهٔ او</p></div>
<div class="m2"><p>گشته او چیره دست رستم من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او ستمکار و من ستمکش او</p></div>
<div class="m2"><p>من عزادار و او محرم من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پای من ایستاده تا هرجا</p></div>
<div class="m2"><p>گر بسورست اگر به ماتم من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیوهٔ من خلاف شیوهٔ او</p></div>
<div class="m2"><p>عالم او ورای عالم من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر دم از باد او پریشانست</p></div>
<div class="m2"><p>یک جهان خاطر فراهم من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لیک با این هه عزیزترست</p></div>
<div class="m2"><p>از دل و دیدهٔ مکرم من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دست ازو برنمی‌توانم داشت</p></div>
<div class="m2"><p>کاو بهر حال هست محرم من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خجلم زانکه خدمتی نشدست</p></div>
<div class="m2"><p>به وی از عزم نامصمّم من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چشم دارم‌ که خوانمش‌ سگ خویش</p></div>
<div class="m2"><p>شاه دوران خدیو اعظم من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شیر اوژن حسن شه آنکه ازوست</p></div>
<div class="m2"><p>درفشان نطق عیسوی دم من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه گوید قضا نموده مدام</p></div>
<div class="m2"><p>فتح ونصرت قرین پرچم من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاه سیاره در خوی خجلت</p></div>
<div class="m2"><p>از چه از شرم رای محکم من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عقل موسی و ذات من هرون</p></div>
<div class="m2"><p>جود عیسی و طبع مریم من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گردن‌ گردنان هفت اقلیم</p></div>
<div class="m2"><p>بستهٔ خم خام پرخم من</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون سلیمان تمام روی زمین</p></div>
<div class="m2"><p>زیر خضرا نگین خاتم من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آسمان زی حریم من پوید</p></div>
<div class="m2"><p>کعبه درگاه و لطف زمزم من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نی خدایم ولی خداوندم</p></div>
<div class="m2"><p>ملک دوران فضای عالم من</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نفخهٔ ‌لطف‌ من ‌بهشت‌ برین</p></div>
<div class="m2"><p>شعلهٔ قهر من جهنم من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قدرم حکم محکمست ولی</p></div>
<div class="m2"><p>تیغ هندی قضای مبرم من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خسروا ایدر از ستایش تو</p></div>
<div class="m2"><p>قاصر آمد بیان ابکم من</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به‌ که باشد دعای دولت تو</p></div>
<div class="m2"><p>شیوهٔ خاطر مسلم من</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>باد یار تو تا به روز قیام</p></div>
<div class="m2"><p>لطف پروردگار اعلم من</p></div></div>