---
title: >-
    قصیدهٔ شمارهٔ ۲۴۵ - در مدح امیرالامرا‌‌ء العظام شاهرخ خان قاجار می‌فرماید
---
# قصیدهٔ شمارهٔ ۲۴۵ - در مدح امیرالامرا‌‌ء العظام شاهرخ خان قاجار می‌فرماید

<div class="b" id="bn1"><div class="m1"><p>انجمن پر انجمست از مهر چهر ماه من</p></div>
<div class="m2"><p>خیز ای خادم برون بر شمع را از انجمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الله الله چیست انجم آفتاب آمد برون</p></div>
<div class="m2"><p>شمع را بگذار تا بیهوده سوزد همچو من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌نسوزد شمع راکس زود برخیز ای ندیم</p></div>
<div class="m2"><p>جمع را گردن فراز و شمع را گردن بزن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمع را آشفته دارد شمع موم از دمع شوم</p></div>
<div class="m2"><p>خیز و این گردنکش ناکام را گردن فکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از شبستان شو به بستان ای ترا بستان غلام</p></div>
<div class="m2"><p>تا سمن پیشت نماز آرد چو پیش بت شمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماه می‌گفتم ترا گر ماه بودی مشکبوی</p></div>
<div class="m2"><p>سرو می‌خواندم‌تراگر سرو بودی‌سیمتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماه را کی ریشه سرو و سرو در سیمین قبا</p></div>
<div class="m2"><p>سرو رایی میوه ماه و ماه در مشکین‌ رسن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نخل آرد خار و خرما نحل آرد نیش و نوش</p></div>
<div class="m2"><p>از چه این هر چار دارد آن لب چون بهرمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نوش و خرما از تبسم خار و نیش از سر زنش</p></div>
<div class="m2"><p>آن دو دایم بهر غیر و این دو دایم بهر من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شهد می‌ریزد به جای خنده زان شبرین‌لبان</p></div>
<div class="m2"><p>قند می‌بارد به جای حرف زان نوشین ‌دهن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌خراشد سینه‌ام را ناخن از عشق لبت</p></div>
<div class="m2"><p>چون ز بهر نقش شرین بیستون راکوهکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو لبی‌ داری چو لعل‌ و من‌ سرشکی چون عقیق</p></div>
<div class="m2"><p>نه ترا باید بدخشان نه مرا باید یمن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خال و رخسار تو با هم چیست دانی زاغ و باغ</p></div>
<div class="m2"><p>زاغ یک خروار عنبر باغ یک دامان سمن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خنده یک بنگاله شکر لعل یک عمان‌گهر</p></div>
<div class="m2"><p>زلف یک اهو از عقرب طره یک عالم‌ شکن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عشوه یک‌کابل‌سماع و غمزه یک بابل‌فسون</p></div>
<div class="m2"><p>ناز یک شیراز شوخی چهره یک‌کشمیرفن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن زنخدان‌یک سپاهال سیب‌سیمینست‌و هست</p></div>
<div class="m2"><p>صدهزار آسیب ازان سیبم نصیب جان و تن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یک بیابان سنبلست آن زلفکان مشکبار</p></div>
<div class="m2"><p>یک خراسان فتنه است آن چشمکان راهزن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همچو نارکفته‌ام دل زان لب چون ناردان</p></div>
<div class="m2"><p>پر ز نار تفته‌ام جان زان قد چون نارون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خال مشکت به رخ یا هندویی آتش‌پرس</p></div>
<div class="m2"><p>خط سبزت‌گرد لب یا طوطیی شکرشکن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صو‌رت‌و خط‌خال‌و عارض زلف‌و چشمت پیش هم</p></div>
<div class="m2"><p>ماه و هاله داغ و لاله مشک و آهوی ختن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا شدستی ای پری پیدا پری پنهان شدست</p></div>
<div class="m2"><p>ور شوی پیدا شود پنهان ز طعن مرد و زن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مهرچهر روشنت در موی همچون جوشنت</p></div>
<div class="m2"><p>نور یزدانست در تاریک جان اهرمن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سجده آرد پیش رویت هردم آن زلف ساه</p></div>
<div class="m2"><p>چون بر خورشد هندو چون بر بت برهمن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ماه نخشب چاه نخشب‌گر ندیدستی ببین</p></div>
<div class="m2"><p>ماه‌نخشب زان‌عذار و چاه نخشب زان ذقن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بذلهٔ شیرین ز قاآنی به‌ گوش آید غریب</p></div>
<div class="m2"><p>چون نوای خارکن از بینوای خارکن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>می‌کندگه دل چکار افغان چرا از غم چسان</p></div>
<div class="m2"><p>همچو قمری‌کی بهاران بر چه بر سرو چمن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ترک من‌کوه از چه آویزی به موکاینم سرین</p></div>
<div class="m2"><p>آنچنان‌کوهی‌که در ایران نگنجد از سمن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چشم وگیسوی تو چون بینم به یاد آید مرا</p></div>
<div class="m2"><p>حالت افراسیاب اندرکمند تهمتن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چهره ات فردوسی از حسنست و مژگانت در او</p></div>
<div class="m2"><p>راست مانند سنان‌گیو در جنگ پشن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زلف تو چون پشت‌ من شد پشت‌من چون زلف تو</p></div>
<div class="m2"><p>وین‌ دو چون‌ چرخ ‌از پی‌ تعظیم‌ خورشد زمن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شاهرخ‌خان‌ کش رود گردون پیاده در رکاب</p></div>
<div class="m2"><p>با فر فرزین نشیند چون بر اسب پیلتن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صدر و قدر او جلیل و طول و نول او جزیل</p></div>
<div class="m2"><p>رای و روی او جمیل و خلق و خوی او حسن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از هراس بأس او گوی زمین را ارتعاش</p></div>
<div class="m2"><p>از نهیب گرز او چرخ مهین را بو مهن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در نیام نیلگون شمشیر گوهربار او</p></div>
<div class="m2"><p>یا نهان در ظلمت شب موج دریای عدن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جوهرش در تیغ و تیغش در نیام‌گوهرین</p></div>
<div class="m2"><p>آن پرن اندر هلالست این هلال اندر پرن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تیر در شستش ‌عقابی ‌مانده‌چون‌ماهی بشست</p></div>
<div class="m2"><p>تیغ در دستش نهنگی‌ کرده در عمان وطن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مهر لامع نزد رایش‌کوکبی در احتراق</p></div>
<div class="m2"><p>نسر واقع بر سنانش صعوه‌یی بر بابزن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خنجر رخشنده‌ش از کوههٔ توسن عیان</p></div>
<div class="m2"><p>یا روان از قله ی کهسار سیلی موج‌زن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای چو جنت خلقت اندر جانفروزی مشتهر</p></div>
<div class="m2"><p>ای ‌چو دوزخ‌ خشمت اندر کفر سوزی ممتحن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کلک لاغر در بنانت ماهی و بحر محیط</p></div>
<div class="m2"><p>شکل جوهر بر سنانت ‌گوهر و بحر عدن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>با رخی‌ پرچین زنی‌ چون زین به رخش از بهر کین</p></div>
<div class="m2"><p>تاختن از چین‌کند رخشت بیکدم تاختن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جامهٔ جاه تو و معمار ایوان تو را</p></div>
<div class="m2"><p>عرش اطلس پروزست و چرخ هشتم پروزن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>روی‌تو مهریست‌رخشان‌کش زمین‌آمد سپهر</p></div>
<div class="m2"><p>رای تو شمعیست تابان‌کش جهان آمد لگن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همچو معماری مهندس هر سحرگه آفتاب</p></div>
<div class="m2"><p>با شعاع خود ز بام قصرت آویزد رسن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پیش ‌تیغت چون‌ بود یکسان چه ‌آهن چه حریر</p></div>
<div class="m2"><p>لاجرم بر پیکر خصمت چه خفتان چه‌کفن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بر هلاکت مرگ قادر نیست لیک از فرط جود</p></div>
<div class="m2"><p>خود نثار مرگ سازی نقد جان خویشتن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زانکه‌ چون‌ جان‌ از تو او خواهد ز فرط مکرمت</p></div>
<div class="m2"><p>ننگ داری در جواب او زگفت لا و لن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>الله الله مرحبا قاآنیا زین فکر تو</p></div>
<div class="m2"><p>کز سماع آن به رقص آید روان اندر بدن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>صاحبا صدرا خداوندا روا داری ‌که چرخ</p></div>
<div class="m2"><p>ماه بخت چون منی با کید دارد مقترن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چشم آن دارم‌که با فرمانروای اصفهان</p></div>
<div class="m2"><p>بازگویی‌کای ملک خصلت امیر موتمن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ای خداوندی‌ که دارد از عطای عام تو</p></div>
<div class="m2"><p>منتی بر هرکه درگیتی خدای ذوالمنن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>این همان قاآنی دانا که ازگفتار او</p></div>
<div class="m2"><p>سنگ آید در سماع وکوه آید در سخن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>این همان قاآنی بخرد که ماند جاودان</p></div>
<div class="m2"><p>مدح او اندر زمان و قدح او اندر زمن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مدح او زنده است تا هر زنده‌ای‌گردد هلاک</p></div>
<div class="m2"><p>قدح او تازه است تا هر تازه‌یی ‌گردد کهن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تو عزیز مصر احسانی و او یوسف‌صفت</p></div>
<div class="m2"><p>خستهٔ‌ گرگ شجون و بستهٔ سجن شجن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چند چون ایوب باشد همدم رنج و عنا</p></div>
<div class="m2"><p>چند چون یعقوب ماند ساکن بیت‌الحزن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نی بود ننگ سلیمان‌گر سخن‌گوید به مور</p></div>
<div class="m2"><p>یا چه از سیمرغ‌ کاهد گر نشیند با زغن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>مدح او چون درپذیرفتی عطایی لازمست</p></div>
<div class="m2"><p>اینچنین بودست تا بودست میران را سنن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>رفتگان را نام نیکو زنده دارد ورنه هست</p></div>
<div class="m2"><p>سالیان‌تا از جهان‌رفتست سیف ذوالیزن</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تا به‌کی قاآنیا زین عجزکردن شرم دار</p></div>
<div class="m2"><p>عجز در نزد کریمان نیک دورست از فطن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>عجز ‌چون تو کهتری در نزد چون او مهتری</p></div>
<div class="m2"><p>راستی‌گویم دلیل ضنت اش و سوء ظن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هر کرا طول و نوالی ننگش از طول نوال</p></div>
<div class="m2"><p>هرکرا فضل و سخایی شرمش از فضل سخن</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ابر نیسان را نگوید هیچکس‌گوهرفشان</p></div>
<div class="m2"><p>مهر رخشان‌را نگوید هیچ کس پرتوفکن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>تا قیامت باد خصمت یار لیکن با ملال</p></div>
<div class="m2"><p>تا به محشر باد یارت خصم لیکن با محن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>هان بیا قاآنیا ترک طمع‌کن از مهان</p></div>
<div class="m2"><p>تیشهٔ همت بیار و ریشهٔ ذلت بکن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>یاد آور داستان‌ گربه‌ای‌ کز بهر عیش</p></div>
<div class="m2"><p>سوی قصر تیرزن شد از سرای پیرزن</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>عزت ار خواهی قناعت‌کن‌که نقد آبرو</p></div>
<div class="m2"><p>جنس عزت را شود از بی‌نیازی مرتهن</p></div></div>