---
title: >-
    قصیدهٔ شمارهٔ ۲۵۳ - د‌ر تهنیت خبر بهبو‌دی محمدشاه غازی به فارس و شادمانی حسین‌خان نظام‌الدوله فرماید
---
# قصیدهٔ شمارهٔ ۲۵۳ - د‌ر تهنیت خبر بهبو‌دی محمدشاه غازی به فارس و شادمانی حسین‌خان نظام‌الدوله فرماید

<div class="b" id="bn1"><div class="m1"><p>زآستان خواجهٔ اعظم چراغ انجمن</p></div>
<div class="m2"><p>پیکی آمد تیزگام و نیکام و خوش سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نامه یی ‌از خواجه‌ بر کف داشت ‌کز عنوان او</p></div>
<div class="m2"><p>بُد هویدا آیت الطاف حقّ ذوالمنن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زانکه اندر نامه بود این مژده کز تأیید حق</p></div>
<div class="m2"><p>یافت بهبودی ز تب طبع شهنشاه زمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه‌شیرس‌ت و شیر شرزه تب‌ دارد از آنک</p></div>
<div class="m2"><p>مر شجاعت را حرارت لازم آمد در بدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا نه خسرو آفتابست و تب اندر آفتاب</p></div>
<div class="m2"><p>لازم تابیست کاو با جرمش آمد مقترن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا نه دارا شمع و هستی انجم‌ آفاق ش</p></div>
<div class="m2"><p>شمع را سوزد به شب تا برفروزد انجمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه‌نارست‌ازبرای‌خصم‌و نور ازبهر دوست</p></div>
<div class="m2"><p>گرمی اندر نار و تف در نور باید مختزن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راستی پرسی خلایق از حقایق غافلند</p></div>
<div class="m2"><p>هست ‌سرّی اندر این معنی ‌که ‌گویم بر علن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قرب و بعد فهم ما ما را بر آن دارد که‌ گاه</p></div>
<div class="m2"><p>شاه را سالم همی بینیم وگاهی ممتحن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ورنه‌شه‌جانست‌و جان‌دارد حیات جاودان</p></div>
<div class="m2"><p>نقص جان نبود اگر گاهی نفور آید ز تن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زین فزون‌ خواهی دلیلی چند گویم معنوی</p></div>
<div class="m2"><p>تا ترا بیرون برد لختی ازین تخییل و ظن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاه ماهست و به نفس خود به یک جا است ماه</p></div>
<div class="m2"><p>قرب و بعد ماست کش گه تیغ بیند گه مجن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاه خورشید و تو نابینا گرش بینی کدر</p></div>
<div class="m2"><p>این کدورت از تو دارد نی ز نفس خویشتن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خود تو لرزی‌ در زمستان‌ زانکه‌ دوری ز آفتاب</p></div>
<div class="m2"><p>ورنه او دایم به یک حالت بود پرتوفکن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ور به تابستان ز گرما تب کنی این هم ز تست</p></div>
<div class="m2"><p>کت شعاع مهر از نزدیکی آید تیغ‌زن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاه ‌سر تا پا بهشتست ‌و چو دوریم از بهشت</p></div>
<div class="m2"><p>آنچنان دانیم کاندر وی بود رنج و محن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ور نه گر چون خواجه ما را چشم معنی‌بین بدی</p></div>
<div class="m2"><p>جنتی آسوده می‌دیدیم بی‌کرب و حزن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شه سپهرس وکدورت ره نیابد بر سپهر</p></div>
<div class="m2"><p>گرچه دامانش کدر بینی گه از گرد دمن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لیک با این جمله ما را لازمست ایدون نشاط</p></div>
<div class="m2"><p>چون به قدر فهم ما باید تکالیف و سنن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شکر بهبود ملک را ای نگار می‌گسار</p></div>
<div class="m2"><p>شیشهٔ می تیشه ساز و ریشهٔ انده بکن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ساقیا جامی بیار و شاهدا کامی بده</p></div>
<div class="m2"><p>خادما عودی بسوز و مطربا رودی بزن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زاهدا امروز منع باده‌خواران گو مکن</p></div>
<div class="m2"><p>بزم شیادی میارا تار زراقی متن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مفتیا امروز فتوی ده‌ که می نوشند خلق</p></div>
<div class="m2"><p>زانکه نبود درد تن را چاره‌یی جز دُردِ دن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>باده اکنون لازمست از ساقیان سیم‌ساق</p></div>
<div class="m2"><p>بوسه اکنون جایزست از گلرخان سبمتن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خانه را باید ز چهر شاهدان‌ کردن بهشت</p></div>
<div class="m2"><p>حجره را باید ز موی‌گلرخان‌کردن چمن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گه ز زلف این به دامن برد می‌باید عبیر</p></div>
<div class="m2"><p>گه ز روی آن به خرمن چید می‌باید سمن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خاصه قاآنی‌که او را با نگاری سرخوشست</p></div>
<div class="m2"><p>دلفریب و دلنشین و دلنواز و دل‌شکن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>غیر او کز چاک پیراهن نماید روی خوب</p></div>
<div class="m2"><p>مشرق خورشید نشنیدم ز چاک پیرهن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چاه‌نخشب ماه‌نخشب هردو دارد کش بود</p></div>
<div class="m2"><p>ماه نخشب بر عذار و چاه نخشب در ذقن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نرخ بوس او مگر از نقد جان بالاترست</p></div>
<div class="m2"><p>کاو بهای بوسه خواهد مدح سلطان زَمَن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خسرو دوران محمد شه شهنشاهی‌که هست</p></div>
<div class="m2"><p>روی و رای او جمیل و خلق و خلق او حسن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کلک لاغر در بنانش ماهی بحر محیط</p></div>
<div class="m2"><p>شکل جوهر بر سنانش‌ گوهر بحر عدن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مهر لامع نزد رایش‌ کوکبی در احتراق</p></div>
<div class="m2"><p>نسر واقع با سنانش طایری بر بابزن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جوهرش در تیغ و تیغ اندر نیام‌گوهرین</p></div>
<div class="m2"><p>آن پرن اندر هلالست این هلال اندر پرن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تیر در شستش عقابی مانده چون ماهی به‌شست</p></div>
<div class="m2"><p>تیغ در دستش نهنگی ‌کرده در دریا وطن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عرصه ی هستی به نزد داستان جاه او</p></div>
<div class="m2"><p>چون به جنب‌ کاخ نوشروان و ثاق پیرزن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خسروا تا مژدهٔ بهبودیت آمد به فارس</p></div>
<div class="m2"><p>جان به‌تن می‌رقصد از شادی وتن در پیرهن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خاصه‌ کز فیروزی این مژده صاحب‌اختیار</p></div>
<div class="m2"><p>شد چنان‌شادان‌ که‌جانش ‌‌می‌نگنجد در بدن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کرد عشی آنچنان ‌کز خار خار عیش او</p></div>
<div class="m2"><p>زهرهٔ چنگی به‌گردون زد نوای خارکن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بوم‌ و بر آمد به وجد و کوه و در آمد به رقص</p></div>
<div class="m2"><p>رند و عارف‌ پای‌ کوبان‌ شیخ‌ و عامی دست‌زن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ماهی از دریا نیایش ‌گفت و ماه از آسمان</p></div>
<div class="m2"><p>وحش در هامون ستایش ‌کرد و طیر اندر وکن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در زمستان نوبهار آمد توگفتی‌کز نشاط</p></div>
<div class="m2"><p>گل دمید از بوستان و لاله سر زد از دمن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>پای‌کوبان شد ز عشرت خوشهای ضیمران</p></div>
<div class="m2"><p>دست‌افشان شد ز شادی برگهای نسترن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وز نشاط این بشارت مردگان را زیر خاک</p></div>
<div class="m2"><p>باغ جنّت شد قبور و زلف حورا شد کفن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>وز فتوح آب خعر در زلف خوبان هم نماند</p></div>
<div class="m2"><p>چنبر و پیچ ‌و شکنج و عقده و چین‌ و شکن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وز نسیم این اثر در دکهٔ سلاخ شهر</p></div>
<div class="m2"><p>گشت ‌خون ‌گوسفندان غیرت مشک ختن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زین بشارت در میان عید اضحی و غدیر</p></div>
<div class="m2"><p>عید دیگر شد عیان از امر میر موتمن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>عید قربان و غدیری را که بود از هم جدا</p></div>
<div class="m2"><p>هم ملفق هم مثنی ‌کرد در یک انجمن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>عید قربان‌شد بدی‌ن معنی مث‌کز خلون</p></div>
<div class="m2"><p>هر تنی قربان خسرو کرد جان خویشتن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هم‌ دو شد عبد غدیر از آن سبب ‌کز هر کنار</p></div>
<div class="m2"><p>دست بوس عید را الحمدخوان شد مرد و زن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هر تنی شکرانه را جان‌کرد قربانی‌که باز</p></div>
<div class="m2"><p>شد بر اورنگ خلافت جانشین بوالحسن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>وز چراغان در شب تاریک سرزد آفتاب</p></div>
<div class="m2"><p>چون‌گل سوری‌که روز ابر تابد بر چمن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شادمان‌شد جان‌خلق‌‌و بوستان‌شد ملک‌فارس</p></div>
<div class="m2"><p>نوجوان شد چرخ پیر و تازه شد دیرکهن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تا سمر باشد به عالم داستان تخت جم</p></div>
<div class="m2"><p>تا مثل باشد به گیتی فر و برز تهمتن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تا قیامت خصم خسرو یار لیکن با ملال</p></div>
<div class="m2"><p>تا به محشر یار سلطان خصم امّا با محن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در ضمیرش باد هر نقشی به جز نقش ملال</p></div>
<div class="m2"><p>در دیارش باد هر چیزی به جز شور و فتن</p></div></div>