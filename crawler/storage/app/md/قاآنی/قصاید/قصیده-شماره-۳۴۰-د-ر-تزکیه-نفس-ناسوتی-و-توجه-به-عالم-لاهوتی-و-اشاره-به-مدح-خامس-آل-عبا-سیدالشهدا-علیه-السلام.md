---
title: >-
    قصیدهٔ شمارهٔ ۳۴۰ - د‌ر تزکیهٔ نفس ناسوتی و توجه به عالم لاهوتی و اشاره به مدح خامس آل ‌عبا سیدالشهدا علیه السلام
---
# قصیدهٔ شمارهٔ ۳۴۰ - د‌ر تزکیهٔ نفس ناسوتی و توجه به عالم لاهوتی و اشاره به مدح خامس آل ‌عبا سیدالشهدا علیه السلام

<div class="b" id="bn1"><div class="m1"><p>ای دل چو تو حالی صفت خویش ندانی</p></div>
<div class="m2"><p>بیهوده سخن از صفت غیر چه رانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با آنکه تو غایب نشوی یک نفس از خویش</p></div>
<div class="m2"><p>خود را نشناسی‌ که چنین یا که چنانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چند سرایی‌ که چنینست و چنانست</p></div>
<div class="m2"><p>آن را که به جز نام دگر هیچ ندانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این‌ گرد که بر دامنت از عجب نشسته</p></div>
<div class="m2"><p>آید عجبم ‌کز چه ز دامن نفشانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن را که به تقلید کسان زشت شماری</p></div>
<div class="m2"><p>گر مصحف آرد ز خداوند نخوانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو‌ن‌ خود همه‌ عیبی چه‌ کنی عیب‌ کسان فاش</p></div>
<div class="m2"><p>بر غیر چه خندی چو تو خود بدتر از آنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر عیب تو چون پرده بپوشید خداوند</p></div>
<div class="m2"><p>ظ‌لمست اگر پردهٔ مردم بدرانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد قافلهٔ عمر تو وامانده ز دنبال</p></div>
<div class="m2"><p>بشتاب مگر لاشه به منزل برسانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون همسفرانت همه از خویش گذشتند</p></div>
<div class="m2"><p>انصاف نباشد که تو در خویش بمانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان تو سبک جانب لاهوت سفر کرد</p></div>
<div class="m2"><p>تو مانده به صحرای طبیعت ز گرانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوش باش به نیک و بد ایام‌ که ما را</p></div>
<div class="m2"><p>نادیده خبر نیست ز اسرار نهانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگشا نظر عقل و ببین صورت مقصود</p></div>
<div class="m2"><p>زیراکه‌گنجد به عیان راز عیانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پرهیز مکن از لقب زشت ‌که موسی</p></div>
<div class="m2"><p>قدرش نشود کاسته از وصف شبانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای نفس به پیری نبری را غم یار</p></div>
<div class="m2"><p>کان بار توان برد به نیروی جوانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قاآنی اگر مرد رهی بار بیفکن</p></div>
<div class="m2"><p>تا از دو جهان توسن همت بجهانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در ماتم شاه شهدا اشک بیفشان</p></div>
<div class="m2"><p>زان آب مگر آتش دوزخ بنشانی</p></div></div>