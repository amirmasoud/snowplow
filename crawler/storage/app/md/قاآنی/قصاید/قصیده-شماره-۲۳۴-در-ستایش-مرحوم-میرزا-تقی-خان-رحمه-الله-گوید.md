---
title: >-
    قصیدهٔ شمارهٔ ۲۳۴ - در ستایش مرحوم میرزا تقی خان رحمه‌الله گوید
---
# قصیدهٔ شمارهٔ ۲۳۴ - در ستایش مرحوم میرزا تقی خان رحمه‌الله گوید

<div class="b" id="bn1"><div class="m1"><p>پی نظارهٔ فرخ هلال عید صیام</p></div>
<div class="m2"><p>هلال ابروی من دوش رفت بر لب بام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو دید مه دو سرانگشت بر دو چشم نهاد</p></div>
<div class="m2"><p>بدان نمط‌ که دو فندق نهی به دو بادام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به من ز گوشهٔ ابرو هلال را بنمود</p></div>
<div class="m2"><p>نیافتم‌ که از آن هر دو ماه عید کدام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو در رخش نگرستم شگفتم آمد زانک</p></div>
<div class="m2"><p>کسی ندیده در آغاز ماه ماه تمام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غرض چو دید مه عید را به‌ گوشهٔ چشم</p></div>
<div class="m2"><p>اشاره‌کردکه برخیز و باده ریز به جام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن شراب ‌که چون شیر خورد سرخ شود</p></div>
<div class="m2"><p>ز عکس او همه نیهای زرد در آجام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به سر جهد عوض مغز نارسیده به لب</p></div>
<div class="m2"><p>به دل دود بَدَل روح ناچکیده به‌کام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هنوز ناشده در جام بسکه هست لطیف</p></div>
<div class="m2"><p>همی بپرد همراه بوی خود به مشام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هنوز ناشده از شیشه در درون قدح</p></div>
<div class="m2"><p>چو خون و مغز جهد تند در عروق و عظام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز جای جستم و آوردمش از آن باده</p></div>
<div class="m2"><p>که عکس او در و دیوار راکندگلفام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو خورد یک دو سه پیمانه از حرارت می</p></div>
<div class="m2"><p>دو چشم تیغ‌زنش شد دو ترک خون‌آشام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به خشم ‌گفت چرا می ‌نمی‌خوری‌ گفتم</p></div>
<div class="m2"><p>من از دو چشم تو هستم مدام مست مدام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به پیش نشوهٔ چشم تو می چه تاب آرد</p></div>
<div class="m2"><p>به اشکبوس‌ کشانی چه در فتد رهّام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به دور چشم تو دور قدح بدان ماند</p></div>
<div class="m2"><p>که با تجلی یزدان پرستش اصنام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کسی که مست ‌شد امروز از دو نرگس‌ تو</p></div>
<div class="m2"><p>به هوش باز نیاید مگر به روز قیام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نهفته نرمک نرمک به زیر لب خندید</p></div>
<div class="m2"><p>چنانکه‌گفتی رنگش زگل دهد پیغام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به عشوه‌گفت‌که الحق شگفت صیادی</p></div>
<div class="m2"><p>که ‌پخته‌پخته ‌بری‌ دل به‌رنگ و صورت خام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بهار اگر به‌ گل و لاله رنگ و بوی دهد</p></div>
<div class="m2"><p>تو ای بهار هنر رنگ و بو دهی به‌ کلام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سزد کزین‌ دم تا نفخ صور اسرافیل</p></div>
<div class="m2"><p>ز رشک ‌کلک تو ‌کُتّاب بشکنند اقلام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من و تو گر چه به‌ انگیز می نه محتاجیم</p></div>
<div class="m2"><p>که بی‌مدام همان مست الفتیم مدام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ولی چو باده چنان مرد را ز هوش برد</p></div>
<div class="m2"><p>که می‌نداند کاغاز چیست یا انجام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه هیچ بالد از مدح ناقدان بصیر</p></div>
<div class="m2"><p>نه هیچ نالد از قدح ناکسان لئام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو نور مهر درخشان تفاوتی نکند</p></div>
<div class="m2"><p>گرش به صفّ نعالست یا به صدر مقام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر به خاک شود تا بهار فیض ازل</p></div>
<div class="m2"><p>ازو دماند گلهای تازه از ابهام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شراب خوردن و بیخود شدن از آن خوشتر</p></div>
<div class="m2"><p>که آب نوشی و در راه دین ‌گذاری دام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شراب را چو بری نام می‌توان دانست</p></div>
<div class="m2"><p>که هست آب شر انگیز هم به شرع حرام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نه آب نیل‌ که بر سبطیان حلال نمود</p></div>
<div class="m2"><p>حرام بود بر قبطیان نافرجام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نه در مصاف حسین تیغ آبدار اولیست</p></div>
<div class="m2"><p>ز آب در گلوی کافران کوفه و شام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه سگ‌گزیده‌گرش آب پیش چشم برند</p></div>
<div class="m2"><p>چنان ز هول بلرزد که روبه از ضرغام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شراب اگر نکند شر بسی حلالترست</p></div>
<div class="m2"><p>ز آب برکه و باران ز شیر دایه و مام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شراب اگر نکند شر بود مباح از آنک</p></div>
<div class="m2"><p>مدام پخته ازو دیده‌اند عشرت خام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حلال هست می ‌امّا به آزموده خواص</p></div>
<div class="m2"><p>حرام هست وی امّا به‌کور دیده عوام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شراب با تو همان می‌کند که روح به تن</p></div>
<div class="m2"><p>نه روح هرچه قوی‌تر قوی‌ترست اندام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بخور شراب ‌و مده نقد حال‌ خویش ز دست</p></div>
<div class="m2"><p>که دلنشین‌تر ازین‌ کمتر اوفتد ایام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شهی نشسته چو یک عرش نور یزدانی</p></div>
<div class="m2"><p>فراز تخت و ملوکش غلام و ملک به‌کام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نعیم هر دو جهانش به‌کام دل حاصل</p></div>
<div class="m2"><p>ز یمن طاعت صدر مهین امیر نظام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قوام عالم و تاریخ آفرینش جود</p></div>
<div class="m2"><p>که آفرینش عالم بدوگرفت قوام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کتاب حکمت دیباچهٔ صحیفهٔ فیض</p></div>
<div class="m2"><p>جمال دولت بازوی ملت اسلام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سپهر مجد و علا میرزا تقی خان آنک</p></div>
<div class="m2"><p>امورکشور و لشکر بدوگرفته قوام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>درنگ حزمش بخشیده تخت را جنبش</p></div>
<div class="m2"><p>شتاب عزمش افزوده ملک را آرام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کفایتش زده سرپنجه با قضا و قدر</p></div>
<div class="m2"><p>سیاستش نهد اشکنجه بر صدور و عظام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به بزم او نتوان رفت بی‌رکوع و سجود</p></div>
<div class="m2"><p>ثنای او نتوان‌گفت بی‌درود و سلام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بدان رسید که اندیشه خون شود در مغز</p></div>
<div class="m2"><p>ز شرم آنکه به مدحش چسان ‌کند اقدام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چنان ارادت شاهش دویده در رگ و پی</p></div>
<div class="m2"><p>که خون و مغز همه خلق در عروق و عظام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زهی ز هیبت تو جسم چرخ را رعشه</p></div>
<div class="m2"><p>خهی ز سطوت تو مغز مرگ را سرسام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به عقل مبهمی ار رو دهد برون ا ید</p></div>
<div class="m2"><p>به یک اشارهٔ سبابهٔ تو از ابهام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز طیب خلق تو نبود عجب‌ که مردم را</p></div>
<div class="m2"><p>به جای موی همه مشک روید از اندام</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به هر که سایهٔ خورشید همت تو فتد</p></div>
<div class="m2"><p>همه ستاره فشاند بجای خوی ز مسام</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به یمن رای رزین تو بس عجب نبود</p></div>
<div class="m2"><p>که‌ کودکان همه بالغ شوند در ارحام</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به عقل دیدهٔ اوهام را کنی خیره</p></div>
<div class="m2"><p>به حزم توسن اجرام را نمایی رام</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گهر فشانی یک‌ روزهٔ تو بیشترست</p></div>
<div class="m2"><p>ز هرچه قطره‌ که تا حشر می‌چکد ز غمام</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نهاده فایض نهیت به پای حکم رسن</p></div>
<div class="m2"><p>نموده رایض امرت به فرق باد لجام</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خدا یگانا آب زلال مستغنیست</p></div>
<div class="m2"><p>که تشنگان دل‌آزرده را بپرسد نام</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>همین بس است ‌که سیراب می کند همه را</p></div>
<div class="m2"><p>اگر سکندر رومست اگر قلندر جام</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز فیض خویش سپاس و ثنا طمع دارد</p></div>
<div class="m2"><p>که این سپاس بس او را که هست رحمت عام</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به پیش رحمت عامش تفاوتی نکند</p></div>
<div class="m2"><p>ز کام تشنه‌لبان ‌گر دعاست ور دشنام</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هزار بار گرش تشنه مدح و قدح کند</p></div>
<div class="m2"><p>نه کم کند نه فزاید به بخشش و انعام</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به قدر تشنگی هر کسی فشاند فیض</p></div>
<div class="m2"><p>اگر فقیر حقیرست اگر ملوک ‌کرام</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کنون تو آبی و ما تشنه‌لب ببخش و ببین</p></div>
<div class="m2"><p>به قدر رتبت ما والسلام والاکرام</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو در اجابت مسؤول جود تو دارد</p></div>
<div class="m2"><p>هزار بار فزونتر ز سائلان ابرام</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بیان صورت حال آنقدر مرا کافیست</p></div>
<div class="m2"><p>کنون تو دانی و روزی‌دهندهٔ دد و دام</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به هرچه روزی مقسوم هست خشنودم</p></div>
<div class="m2"><p>ز دل بپرس ‌که ایزد چسان نهاد اقسام</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ز حکم بارخدایی عنان نخواهم تافت</p></div>
<div class="m2"><p>به‌حکم آنکه بران‌نسخه‌جاری‌است احکام</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>هزار بارگرم فقر ریز ریز کند</p></div>
<div class="m2"><p>زبان دق نگشایم به ایزد علّام</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو او ببیند دیگر چرا دهم عرضه</p></div>
<div class="m2"><p>چو اوبداند دیگر چراکنم اعلام</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>خدا به جود تو ارزاق ما حوالت ‌کرد</p></div>
<div class="m2"><p>وگرنه بر تو چه افتاده بود رنج انام</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چنان‌ کریم و رحیمی‌ که می‌ندانندت</p></div>
<div class="m2"><p>ز شوهر و پدر خود ارامل و ایتام</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>قضا عنان ‌کش خلقست سوی رحمت تو</p></div>
<div class="m2"><p>وگرنه اینهمه ‌گستاخ هم نیند عوام</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>سخن چو عمر تو خوشتر اگر دراز کشید</p></div>
<div class="m2"><p>که خوش فتد بر حق از کلیم طول ‌کلام</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>همیشه تا چو دو معنی ز یک سخن خیزد</p></div>
<div class="m2"><p>سخنوران بلیغش‌ کنند نام ایهام</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>زبان هرکه چو نشتر ترا بیازارد</p></div>
<div class="m2"><p>دلش پر از خون بادا چو شیشهٔ حجام</p></div></div>