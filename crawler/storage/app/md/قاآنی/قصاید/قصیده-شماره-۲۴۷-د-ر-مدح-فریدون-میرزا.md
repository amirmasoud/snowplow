---
title: >-
    قصیدهٔ شمارهٔ ۲۴۷ - د‌ر مدح فریدون میرزا
---
# قصیدهٔ شمارهٔ ۲۴۷ - د‌ر مدح فریدون میرزا

<div class="b" id="bn1"><div class="m1"><p>ای به مشکین موی تو مسکین دلم‌کرده وطن</p></div>
<div class="m2"><p>چون غم آن موی مشکن در دل مسکین من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه میان انجم از خجلت نگردد آشکار</p></div>
<div class="m2"><p>آشکارت‌گر ببیند در میان انجمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر فرو ریزد اگر طلعت فروزی در بهار</p></div>
<div class="m2"><p>سرو بنشیند اگر قامت فرازی در چمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای نه اندامست زیر جامه‌ات کاموده‌ای</p></div>
<div class="m2"><p>پیرهن از یک چمن نسرین و یک بستان سمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاش لله نیست نسرین را چنین فر و بهار</p></div>
<div class="m2"><p>روح پاکست اینکه دادی جای اندر پیرهن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این‌چه مشکین زلف دلبند رسا باشدکز او</p></div>
<div class="m2"><p>یک جهان دل را اسیر آورده‌یی در یک رسن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یعلم‌الله هیچکس زینسان رسن هرگز ندید</p></div>
<div class="m2"><p>حلقه اندر حلقه خم در خم شکن اندر شکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاتش دل سوزم و سازم ‌چو شمعت در حضور</p></div>
<div class="m2"><p>خواهیم گردن فراز و خواهیم گردن بزن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور توام‌ گردن زنی من تازه‌جان ‌گردم چو شمع</p></div>
<div class="m2"><p>زانکه جان تازه یابد شمع از گردن زدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خود چه باشد گر درآیی درکنار من شبی</p></div>
<div class="m2"><p>همچو جانی در بدن یا همچو شمعی در لگن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نی چو قرص آفتابی من چرغ صبحگاه</p></div>
<div class="m2"><p>در وصالت نیست الا جان سپردن‌ کار من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خرم آنشب ‌کز رخ و زلف تو باشد تا سحر</p></div>
<div class="m2"><p>دوش من پر سنبل و آغوش من پر یاسمن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با من مسکین نگردی یار و جای آن بود</p></div>
<div class="m2"><p>ای بت سیمین‌بر و سیمین‌تن و سیمین‌ذقن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لیک ازینسان هم نخواهد ماند روزی چند باش</p></div>
<div class="m2"><p>تا ز جود خسروم بینی قرین خویشتن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در بر شه عرضه خو‌اهم داشت حال خویش و شاه</p></div>
<div class="m2"><p>از کرم نپسنددم در این غم و رنج و محن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باش تا بینی که خسرو دوش و آغوش مرا</p></div>
<div class="m2"><p>پر در وگوهر نماید از سخا و از سخن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باش تا بینی به من از بحر دست و کان طبع</p></div>
<div class="m2"><p>گوهر افشاند به خروار و زر افشاند به من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای بت قامت قیامت وی مه بالا بلا</p></div>
<div class="m2"><p>ای غلام قامت و بالات سرو و نارون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا به‌کی تابم بری زان زلفکان پر ز تاب</p></div>
<div class="m2"><p>تا به‌ کی راهم زنی زان چشمکان پرفتن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لعل تو چون بهر من لیکن بود از بهر غیر</p></div>
<div class="m2"><p>وه چه بود ار بهر من بود آن لب چون بهرمن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روی‌داری چون‌سهیل و لعل داری چون عقیق</p></div>
<div class="m2"><p>هرکرا باشی به دامن بی‌نیازست از یمن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چثبم‌و مغز من ز عکس‌لعل‌و بوفا زلف تست</p></div>
<div class="m2"><p>این پر از لعل بدخشان آن پر از مشک ختن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گل نبویم می ننوشم ‌که نباشند این و آن</p></div>
<div class="m2"><p>این به طعم آن دهان و آن به بوی این بدن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مغز من‌پر نکهتست از بسکه بویم آن دو زلف</p></div>
<div class="m2"><p>کام من پر شکرست از بسکه بوسم آن دهن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بوسهٔ لعل تو گر باشد به نرخ جان رواست</p></div>
<div class="m2"><p>خاصه آن ساعت که خواند مدحت شاه زمن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شاه فرّخ‌ رخ‌ که یابد فرّ فرزینی ازو</p></div>
<div class="m2"><p>هر پیاده‌کش دود در پای اسب پیلتن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خسروگیتی فریدونشه‌که باشد بر جهان</p></div>
<div class="m2"><p>با وجودش منّت و فضل از خدای ذوالمنن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای جوان بختی ‌که بی‌شیرینی اوصاف تو</p></div>
<div class="m2"><p>هیچ‌ کودک برنگیرد در جهان لب از لبن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گردش گردون به‌قدر و جاه شخصت معترف</p></div>
<div class="m2"><p>گردن دوران به جود و شکر مدحت مرتهن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای به عالم بی‌همال از فطرت و اصل و گهر</p></div>
<div class="m2"><p>وی به ‌گیتی بی‌مثال از فکرت و فهم و فطن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چرخ برپیچد عنان چون توسنت بیند دمان</p></div>
<div class="m2"><p>خصم برپوشد کفن چون جوشنت بیند به تن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اژدر رمحت بیوبارد ود خشک و تر</p></div>
<div class="m2"><p>تیر تو گوید برافروزد شرار مر زغن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کلک تو ریزد لآل نغز بی‌دست و درون</p></div>
<div class="m2"><p>تیر تو گوید جواب خصم بی‌کام و دهن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون‌به‌دست ‌آری قلم‌اندیشه‌ گوید ای‌شگفت</p></div>
<div class="m2"><p>ابر نیسان را بود اندر محیط ایدر وطن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کف ‌گشودی در سخا بحر عمان شد در غمان</p></div>
<div class="m2"><p>لب‌گشادی در سخن درَ ثمین شد بی‌ثمن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ای نهاده یک جهان سر بر خط فرمان تو</p></div>
<div class="m2"><p>همچنان‌که مهر را هندو و بت را برهمن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گرنه بهر بذل تو چه سیم چه خاک سیاه</p></div>
<div class="m2"><p>ورنه بهر جود تو چه ریگ چه در عدن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از پی مدح تو باشد ورنه خاصیت چه بود</p></div>
<div class="m2"><p>منطق شیرین ودیعت در دهان مرد و زن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا غم معشوق ‌گیرد در دل عاشق قرار</p></div>
<div class="m2"><p>تا دل عشاق جوید در بر جانان سکن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>حاسد جاه تو در قعر زمین‌گیرد سکون</p></div>
<div class="m2"><p>پایهٔ قدر توگیرد جای در اوج پرن</p></div></div>