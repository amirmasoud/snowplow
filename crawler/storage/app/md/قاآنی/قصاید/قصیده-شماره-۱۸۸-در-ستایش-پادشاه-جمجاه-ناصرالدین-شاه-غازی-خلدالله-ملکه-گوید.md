---
title: >-
    قصیدهٔ شمارهٔ ۱۸۸ - در ستایش پادشاه جمجاه ناصرالدین شاه غازی خلدالله ملکه گوید
---
# قصیدهٔ شمارهٔ ۱۸۸ - در ستایش پادشاه جمجاه ناصرالدین شاه غازی خلدالله ملکه گوید

<div class="b" id="bn1"><div class="m1"><p>ناصرالدین شاه ‌گیتی را منظم‌ کرد باز</p></div>
<div class="m2"><p>معنی اقبال و نصرت را مجسم‌ کرد باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رموز خسروی یک نکته باقی مانده بود</p></div>
<div class="m2"><p>ملهم غیبش به آن یک نکته ملهم ‌کرد باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فال شه نصر من الله بود اینک‌کردگار</p></div>
<div class="m2"><p>آیهٔ انا فتحنا را بر او ضم‌ کرد باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشکبوسی را به یک تیر عذاب از پا فکند</p></div>
<div class="m2"><p>راستی ‌کیخسرو ماکار رستم‌ کرد باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواست کین ایرج دین‌ را ز سلم و تور کفر</p></div>
<div class="m2"><p>این منوچهر مؤید کار نیرم ‌کرد باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منت ایزد را که صد ره بیشتر از پیشتر</p></div>
<div class="m2"><p>ملک و دین را هم معظم هم منظم کرد باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کردگاری شه ‌که در باغ جنان روح ملک</p></div>
<div class="m2"><p>سجده بر خاک ره حوا و آدم‌ کرد باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راست گویی ‌خیمهٔ‌دولت به مویی بسته بود</p></div>
<div class="m2"><p>ایزدش با رشتهٔ تقدیر محکم ‌کرد باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صدهزاران عقده بود از حلم شه درکارها</p></div>
<div class="m2"><p>جمله را سرپنجهٔ عزمش به یکدم کرد باز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاه پنداری سلیمان بود کز انگشت او</p></div>
<div class="m2"><p>اهرمن خویی به حیلت قصد خاتم کرد باز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صدراعظم خلق را چون آصف‌ بن برخیا</p></div>
<div class="m2"><p>آگه از کردار دیو و حالت جم‌ کرد باز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اسم شه را خواند و بر آن دیو بد گوهر دمید</p></div>
<div class="m2"><p>قصه ‌کوته هرچه‌ کرد آن اسم اعظم‌ کرد باز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قالب بی‌روح دولت را ملک بخشید روح</p></div>
<div class="m2"><p>آشکارا معجز عیسی بن مریم‌کرد باز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنکه از عجب‌ پلنگی قصد چندین شیر کرد</p></div>
<div class="m2"><p>خسروش ضایع‌تر ازکلب معلم‌کرد باز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کید خصم‌ خانگی‌ را هر‌چه خسرو در سه سال</p></div>
<div class="m2"><p>خواست‌کردن فاش عفو شاه مدغم ‌کرد باز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون نبودش گوشمال سال اول سودمند</p></div>
<div class="m2"><p>چرخش اسباب‌پریشانی فراهم کرد باز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاخ عمرش راکه می‌بالید در بستان ملک</p></div>
<div class="m2"><p>آخر از باد نهیب پادشه خم کرد باز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زهره ء شیر فلک شد آب ازاین جرأت‌ که شه</p></div>
<div class="m2"><p>پنجه اندر ینجهٔ این چیره ضیغم ‌کرد باز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عالمی راکرد مات درد در شطرنج و نرد</p></div>
<div class="m2"><p>زان دغلها کان حریف بد دمادم‌ کرد باز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باغ‌ملک از صولت‌وی چون‌بدی آشفته بود</p></div>
<div class="m2"><p>فرّ شه زان رو درش پیچیده در هم کرد باز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دست قدرت‌گوبی اندر آستین شاه بود</p></div>
<div class="m2"><p>کاستین برچید و از نو خلق عالم‌ کرد باز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر دل‌دشمن زد و بر حلقه‌های‌زلف دوست</p></div>
<div class="m2"><p>دست شه هر عقده‌کز دلهای پر غم‌کرد باز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از پریشان زلف پرچم با هزار آشفتگی</p></div>
<div class="m2"><p>رایت هرگوشه جمعی را پریشان‌کرد باز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زادهٔ خسرو هلاکوخان هم از بخت نیا</p></div>
<div class="m2"><p>قتل عام از مرز خنج تا شکیبان کرد باز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وز در بیغاره ‌گردون خندهٔ دندان‌نما</p></div>
<div class="m2"><p>از بن دندان به خصم آب دندان‌ کرد باز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>باد هر روزش ز نو فتحی‌ که گویند نه سپهر</p></div>
<div class="m2"><p>الله الله شه عجب فتحی نمایان ‌کرد باز</p></div></div>