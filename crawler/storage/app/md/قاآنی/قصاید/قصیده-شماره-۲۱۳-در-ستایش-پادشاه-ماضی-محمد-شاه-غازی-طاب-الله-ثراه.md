---
title: >-
    قصیدهٔ شمارهٔ ۲۱۳ - در ستایش پادشاه ماضی محمد شاه غازی طاب الله ثراه
---
# قصیدهٔ شمارهٔ ۲۱۳ - در ستایش پادشاه ماضی محمد شاه غازی طاب الله ثراه

<div class="b" id="bn1"><div class="m1"><p>خسروا ای ‌کت ایزد متعال</p></div>
<div class="m2"><p>نافریدست در زمانه حمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دولتی بیکران ترا داده</p></div>
<div class="m2"><p>کش همه چیز هست غیر زوال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بحر در جنب جود تو شبنم</p></div>
<div class="m2"><p>کوه در نزد حلم تو مثقال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیم و زر را به دور دولت تو</p></div>
<div class="m2"><p>نشناسد کسی ز سنگ و سفال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مر مرا هست چاکری‌ که بود</p></div>
<div class="m2"><p>در مدیحش زبان ناطقه لال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب او ساغریست از یاقوت</p></div>
<div class="m2"><p>از می ‌لعل رنگ مالامال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر خورد خون من حلالش باد</p></div>
<div class="m2"><p>خوردن خون ا‌گر‌چه نیست حلال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راستی سرو و ماه را ماند</p></div>
<div class="m2"><p>قد و رخسارش ازکمال و جمال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم و مژگان او بهم دانی</p></div>
<div class="m2"><p>به چه ماند به تیر خورده غزال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خلقی از فکر موی او شب و روز</p></div>
<div class="m2"><p>خیلی از یاد خال او مه و سال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با تنی همچو موی مویاموی</p></div>
<div class="m2"><p>با قدی همچو نال نالانال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خال در طاق ابرویش ‌گویی</p></div>
<div class="m2"><p>جا به محراب ‌کعبه‌ کرده بلال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عقل گفت از خیال او بگذر</p></div>
<div class="m2"><p>تا نگردی اسیر خیل خیال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عشق‌گفتا زهی فراست عقل</p></div>
<div class="m2"><p>که تصورکند خیال محال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روی او کرده مر مرا حیران</p></div>
<div class="m2"><p>بر چه بر صنع قادر متعال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ورنه یکتا خدای داند و بس</p></div>
<div class="m2"><p>که نیم پای‌بست طره و خال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به خدایی‌که صبح و شام‌کنند</p></div>
<div class="m2"><p>شکر آلای او نساء و رجال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به کریمی که گسترد شب و روز</p></div>
<div class="m2"><p>بر سیاه و سفید خوان نوال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که بود مر مرا ز پاکی اصل</p></div>
<div class="m2"><p>پاس شرع رسول در همه حال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هست القصه زان سهی بالا</p></div>
<div class="m2"><p>مر مرا از بلا فراغت بال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من و او هر دو بیهمالستیم</p></div>
<div class="m2"><p>او به حسن و جمال و من به ‌کمال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شَعر او مشک و شِعر من شکر</p></div>
<div class="m2"><p>آن مبرا ز مثل و این ز مثال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شَعر او بر بنای شرع ‌کمند</p></div>
<div class="m2"><p>شعر من بر به ‌پای عقل عقال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>او چو برقع ز رخ براندازد</p></div>
<div class="m2"><p>تا که بفریبدم به غنج و دلال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من چنان ساز شعر ساز کنم</p></div>
<div class="m2"><p>که دگرگون شود ورا احوال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تنگ بر خدمتم میان بسته</p></div>
<div class="m2"><p>چون به قصر تو قیصر و چیپال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>من نخواهم ز بخت الا او</p></div>
<div class="m2"><p>او نخواهد ز شاه الا شال</p></div></div>