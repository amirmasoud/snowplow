---
title: >-
    قصیدهٔ شمارهٔ ۳۳۷ - و له من‌ کلامه
---
# قصیدهٔ شمارهٔ ۳۳۷ - و له من‌ کلامه

<div class="b" id="bn1"><div class="m1"><p>ترک کشتی‌گیر من میل شنا دارد همی</p></div>
<div class="m2"><p>وانچه بی‌میلی بود با آشنا دارد همی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگذرد بر لب ز میل آشنایانش حدیث</p></div>
<div class="m2"><p>ور حدیثی دارد از میل و شنا دارد همی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌ندارم زهره تاگویم به هنگام شنا</p></div>
<div class="m2"><p>زهره را مایل به خط استوا دارد همی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازکمر بگذشته زلف تابدارش ای ‌شگفت</p></div>
<div class="m2"><p>می‌ندانم ‌کز کمر قصد کجا دارد همی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنج سیم اندرکمر مانا مگر دارد سراغ</p></div>
<div class="m2"><p>تا زگنج سیم ‌کام دل روا دارد همی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلفش آری اژدرست و گنج بیند در کمر</p></div>
<div class="m2"><p>هر کمر کاو گنج دارد اژدها دارد همی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پهلوانی می‌کند با اهل دل ‌گیسوی او</p></div>
<div class="m2"><p>بنگر آن افتاده اندر سر چها دارد همی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌رباید زلف مشکینش دل از خوبان مگر</p></div>
<div class="m2"><p>زلف او خاصیت آهن‌ربا دارد همی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با سر زلفش ‌‌که یک ‌اقلیم دل پابست اوست</p></div>
<div class="m2"><p>روز و شب مسکین دل ‌من ماجرا دارد همی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون نماید میل‌ کشتی‌ کِشتی صبر مرا</p></div>
<div class="m2"><p>زآب چشمان غرقهٔ بحر فنا دارد همی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>میل ‌چون جنبد به‌ دستش ‌‌میل ‌من‌ جنبد چنانک</p></div>
<div class="m2"><p>تا دوصد فرسنگم از دانش جدا دارد همی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون به چرخ آید بتابد روی هر ساعت زمن</p></div>
<div class="m2"><p>نسبتی مانا به چرخ بی‌وفا دارد همی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رند و قلاشست در ظاهر ولیکن در نهفت</p></div>
<div class="m2"><p>پاکدامن خویش را چون پوریا دارد همی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیکرش یک‌ ‌توده نسرینست ‌و یک‌خروار سیم</p></div>
<div class="m2"><p>سیم و نسرین را دریغ از ما چرا دارد همی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سیم‌و نسرینش ‌ز اشک‌لاله‌گون‌و ضعف دل</p></div>
<div class="m2"><p>سیم و نسرینم عقیق و کهربا دارد همی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یاسمینست آن نه ‌پیکر ارغوانست ‌آن ‌نه خط</p></div>
<div class="m2"><p>روی و پیکر کی چنین فرّ و بها دارد همی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هیچ دیدی یاسمین را سخت سندان در بغل</p></div>
<div class="m2"><p>یا شنبدی ‌کارغوان مشک ختا دارد همی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر فراز نخل قد سیمای سیمینش عیان</p></div>
<div class="m2"><p>یا نه بر سرو روان بدرالدجی دارد همی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چشم ‌و ابرو خال ‌و گیسو قامت ‌و رو زلف ‌و لب</p></div>
<div class="m2"><p>در کمین خلق دزدی جابجا دارد همی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دولت وصلی‌ که شاهان جهان را آرزوست</p></div>
<div class="m2"><p>وقف قلّاشان و رندان ‌کرده تا دارد همی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تخت ‌عاجش را نه‌دیدست و نه‌بیند هیچکس</p></div>
<div class="m2"><p>تا نگار پارسی‌دل پارسا دارد همی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گاه گاهی بوسه‌ای‌‌ گر می دهد عیبش مکن</p></div>
<div class="m2"><p>اینقدر بر خلق بخشایش روا دارد همی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>غیر وی از وی نخواهد هرکه باشد پاکباز</p></div>
<div class="m2"><p>پاکباز از هرچه جز جانان ابا دارد همی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ویحک از بالای دلبندش که چون پوشد قبا</p></div>
<div class="m2"><p>صد خیابان نارون در یک قبا دارد همی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وقف خوبان ‌کرده قاآنی مگر گفتار خویش</p></div>
<div class="m2"><p>کاینهمه زیشان به لب مدح و ثنا دارد همی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا نه‌ پنداری هوسناکست و هر جا شاهدیست</p></div>
<div class="m2"><p>خویش رادزدیده‌بر جورش رضا دارد همی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>طبع را می‌آزماید در مضامین شگرف</p></div>
<div class="m2"><p>وزسخن سنجان امید مرحبا دارد همی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ورنه ‌هم یکتا خدا داند که ‌اندر شرق ‌و غرب</p></div>
<div class="m2"><p>روی دل در هرچه دارد در خدا دارد همی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>او به‌ یاری ‌بسته ‌دل کش ‌‌نیست‌ هستی ‌ز آب‌ و گل</p></div>
<div class="m2"><p>در وجودش آب و‌ گل نشو و نما دارد همی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون‌ ولا خواهد بلا خواهد از آنرو روز و شب</p></div>
<div class="m2"><p>خاطر از بالای خوبان در بلا دارد همی</p></div></div>