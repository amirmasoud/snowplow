---
title: >-
    قصیدهٔ شمارهٔ ۲۶۰ - در ستایش یکی از سرداران ولیعهد مبرور فرماید
---
# قصیدهٔ شمارهٔ ۲۶۰ - در ستایش یکی از سرداران ولیعهد مبرور فرماید

<div class="b" id="bn1"><div class="m1"><p>امین داور و دارا معین ملت و ایمان</p></div>
<div class="m2"><p>یمین کشور و لشکر ضمین ملکت و هامان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قوام ملت احمد نظام مذهب جعفر</p></div>
<div class="m2"><p>معاذکشور دارا ملاذ لشکر خاقان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگین خاتم دولت مکین مسد شوکت</p></div>
<div class="m2"><p>تکین‌کشور همت‌طغان ملکت احسان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قوام‌کشور صاحبقران و قائدگیتی</p></div>
<div class="m2"><p>ظام لشکر عباس شاه و ناظم ‌گیهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هجوم لشکر او را علامت آمده محشر</p></div>
<div class="m2"><p>زمان دولت او را قیامت آمده پایان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قطاس رایت او را که‌ کلاله ساخته حورا</p></div>
<div class="m2"><p>عقاص پرچم او را غلاله ساخته غلمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقاب صول او را نوایب آمده مخلب</p></div>
<div class="m2"><p>هژبر سطوت او را حوادث آمده دندان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به‌صحن گلشن جودش نرسته غنچه ی ضنت</p></div>
<div class="m2"><p>به ‌گرد مرکز ذاتش نگشته پرگر عصیان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کمند چینی او را ستاره آمده چنبر</p></div>
<div class="m2"><p>سمند ختلی او را زمانه آمده میدان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به پیش صارم برٌان او چه خار و چه خاره</p></div>
<div class="m2"><p>به نزد بیلک پرّان او چه برد و چه خفتان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پرند حادثه سوزش فنای خرمن فتنه</p></div>
<div class="m2"><p>خدنگ نایبه‌توزش بلای دودهٔ طغیان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حسام هندی او را منیه آمده جوهر</p></div>
<div class="m2"><p>سهام‌توزی او را بلیّه آمده پیکان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به وقعه خنجر قهرش بریده حنجر ضیغم</p></div>
<div class="m2"><p>به پهنه دهرهٔ خشمش دریده زهرهٔ ثعبان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سپاه شوکت او را ستاره مهچهٔ رایت</p></div>
<div class="m2"><p>سرای دولت او را مجره شمسهٔ ایوان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جهان‌ دانش و جود ای ز وصف‌ ذات تو عاجز</p></div>
<div class="m2"><p>ضمیر اخطل و اعشی روان صابی و حسان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز ابر دیدهٔ‌ کلگ تو صفحه مخزن ‌گوهر</p></div>
<div class="m2"><p>ز برق خندهٔ تیغ تو پهنه معدن مرجان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غلام عزم تو صرصر مطیع رای تو اختر</p></div>
<div class="m2"><p>یتیم دست تو گوهر اسیر طبع تو عمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نسیم‌گلشن مهرت فنای‌ گلشن جنت</p></div>
<div class="m2"><p>سموم آتش قهرت بلای ساحت نیران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر آنچه حاصل‌ گیتی به پیش جود تو اندک</p></div>
<div class="m2"><p>هرآنچه مشکل عالم به نزد رای تو آسان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کمینه خادم خدمتگران بزم تو زهره</p></div>
<div class="m2"><p>کهینه چاکر خنجرکشان رزم تو کیوان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سموم صرصر قهرت خمود آتش دوزخ</p></div>
<div class="m2"><p>زلال‌ کوثر لطفت زوال چشمه ی حیوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کف تو آفت گوهر لب تو آتش شکر</p></div>
<div class="m2"><p>رخ تو قتنهٔ اختر دل تو مظهر ایمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برنده تیغ تو مهر و عدوی جاه تو شبنم</p></div>
<div class="m2"><p>درنده رمح تو ماه و حسود قدر تو کتان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چه لابه پیش تو آرم ز جور اختر ریمن</p></div>
<div class="m2"><p>چه شکوه پیش تو آرم ز دورگنبدگردان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز بخت ‌خو‌د شده‌شاکی به‌روز خو‌د شده باکی</p></div>
<div class="m2"><p>ز رنج خود شده حاکی به حال خو‌د شده حیران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه زخم ‌کلفت او را بغیر مهر تو مرهم</p></div>
<div class="m2"><p>نه درد محنت او را به غیر لطف تو درمان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ولی قدر تو بادا هماره همسر شادی</p></div>
<div class="m2"><p>عدوی جاه تو بادا همیشه پیرو خذلان</p></div></div>