---
title: >-
    قصیدهٔ شمارهٔ ۱۰۹ - د‌ر ستایش مهد کبری و ستر عظمی کافلهٔ‌الملک عاقلهٔ‌الدوله مام پادشاه
---
# قصیدهٔ شمارهٔ ۱۰۹ - د‌ر ستایش مهد کبری و ستر عظمی کافلهٔ‌الملک عاقلهٔ‌الدوله مام پادشاه

<div class="b" id="bn1"><div class="m1"><p>دلکا هیچ خبر داری‌ کان ترک پسر</p></div>
<div class="m2"><p>دوشم از ناز دگر بار چه آورد به سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با لب نوش آمد شب دوشین به سرای</p></div>
<div class="m2"><p>حلقه بر در زد و برجستم و بگشودم در</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنگ بگرفتمش اندر بر و بر تنگ دهانش</p></div>
<div class="m2"><p>آنقدر بوسه زدم‌کز دو لبم ریخت شکر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت قاآنیا تا کی خسبی به سرای</p></div>
<div class="m2"><p>خیز کز روزه شد اوضاع جهان زیر و زبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غالباً مست چنان خفته‌یی اندر شعبان</p></div>
<div class="m2"><p>کز مه روزه و از روزه ترا نیست خبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم ای ترک دلارام مگر بازآمد</p></div>
<div class="m2"><p>رمضان آن مه شاهد کش زاهد پرور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت آری رمضان آمد و گوید که به خلق</p></div>
<div class="m2"><p>رقم از بار خدا دارم و از پیغمبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راست‌ گویی‌ که ز نزد ملک‌الموت رسید</p></div>
<div class="m2"><p>که ز ره نامده روح از تن من‌ کرد سفر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رمضان کاش نمی‌آمد هرگز به جهان</p></div>
<div class="m2"><p>تا نمی‌رفت مرا روح روان از پیکر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مر مرا روزه یک ‌روزه درآورد ز پای</p></div>
<div class="m2"><p>تا دگر روزهٔ سی‌روزه چه آرد بر سر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من شکر بودم و بگداختم از بی‌آبی</p></div>
<div class="m2"><p>گرچه رسمست که بگدازد از آب شکر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من‌ گهر بودم و آوردم دریا ز دو چشم</p></div>
<div class="m2"><p>گرچه شک نیست‌ که از دریا آرند گهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می‌شنیدم‌ که ز همسایه به همسایه رسد</p></div>
<div class="m2"><p>گه‌ گه آسیب و نمی‌کردم از آن‌کار حذر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دیدی آخر که ز همسایگی زلف و میان</p></div>
<div class="m2"><p>شد چسان رویم باریک و سرینم لاغر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مردم دیده‌ام از جنبش صفرای صیام</p></div>
<div class="m2"><p>صبح تا شب یرقان دارد همچون عبهر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شام زاندوه علایق شودم تیره روان</p></div>
<div class="m2"><p>صبح زانبوه خلایق شودم خیره بصر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بَدَل بانگ نیم بانگ مؤذن درگوش</p></div>
<div class="m2"><p>عوض خون رزم خون دل اندر ساغر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خلق گویند در آتش نگدازد یاقوت</p></div>
<div class="m2"><p>بالله این حرف دروغست و ندارم باور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زانکه یاقوت لبم ز آتش صفرای صیام</p></div>
<div class="m2"><p>صاف بگداخت بدانسان ‌کهازو نیست اثر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غُصّه ‌ها دارم نا گفتنی از دور سپهر</p></div>
<div class="m2"><p>قصه‌ها دارم نشنفتنی از جور قدر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وقت آن آمد کان واعظک از بعد نماز</p></div>
<div class="m2"><p>همچو بوزینه به یکبار جهد بر منبر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آسیا سنگی بر فرق نهد از دستار</p></div>
<div class="m2"><p>ناو آن آس شود نایش و گردن محور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>من ‌که بی‌ غمزه نمی‌خواندم یک روز نماز</p></div>
<div class="m2"><p>ورد بوحمزه چسان خوانم هر شب به سحر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفتم ای روی تو بر قد چو به طوبی فردوس‌</p></div>
<div class="m2"><p>گفتم ای زلف تو بر رخ چو بر آتش عنبر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خط تو برجی از مشک و د ر ‌آن برج سهیل</p></div>
<div class="m2"><p>لب تو دُرجی از لعل و در آن درج ‌گهر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زلف چون غالیه‌ات غالی اگر نیست چرا</p></div>
<div class="m2"><p>نرسد زآتش روی تو بدو هیچ ضرر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زهر چشم تو چرا زان حط مشکن افزود</p></div>
<div class="m2"><p>راستی دافع زهرست اگر سیسنبر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از دل سخت تو شد چهره‌ام از اشکم سیم</p></div>
<div class="m2"><p>وین عجب نی که زر و سیم برآید ز حجر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دل من رهرو و زلفت شب و رخسارت ماه</p></div>
<div class="m2"><p>شب همان به‌ که به مهتاب نمایند سفر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز لفکانت دو غلامند سیه‌ کاره و دزد</p></div>
<div class="m2"><p>که نهادستند از خجلت بر زانو سر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یا دوگبرند سیه‌چرده‌که آرند سجود</p></div>
<div class="m2"><p>چون براهیم زراتشت همی بر آذر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یا نه هستند دو هندو که به بتخانهٔ‌ گنگ</p></div>
<div class="m2"><p>پشت ‌کردستند از بهر ریاضت چنبر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یا نه دو زنگی جادوگر آتشبازند</p></div>
<div class="m2"><p>که همی بر زبر سرو فروزند اخگر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یا نه بنشسته به زانو بر ماه مدنی</p></div>
<div class="m2"><p>از سوی راست بلال از طرف چپ قنبر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ان‌ا عجب نیست به هر خانه‌که تویر بود</p></div>
<div class="m2"><p>گر در آن خانه ملک را نبود هیچ گذر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عجب آنست که هر جا تو ملک‌وار روی</p></div>
<div class="m2"><p>خلق حیرت‌زده مانند به مانند صور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>غم‌مخور زآنکه‌ به‌ یک‌حال ‌نماندست جهان</p></div>
<div class="m2"><p>شادی آید ز پس غصه و خیر از پی شر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به‌کسوف اندر پیوسته نپاید خورشید</p></div>
<div class="m2"><p>به وبال اندر همواره نماند اختر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>رمضان عمر ملک نیست ‌که ماند جاوید</p></div>
<div class="m2"><p>بلکه چون خصم ولیعهد بود زودگذر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ماه شوال ز نزدیکی دورست چنانک</p></div>
<div class="m2"><p>مردم چشم ز نزدیکی ناید به نظر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اینک از غرهٔ غرارگره بازگشای</p></div>
<div class="m2"><p>که بر آن طرهٔ طرارگره اولی‌تر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نذرکردم صنما چون مه شوال آید</p></div>
<div class="m2"><p>نقل و می آرم و طنبور و نی و رامشگر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>صبح عید آن‌گه کز کوه برآید خورشید</p></div>
<div class="m2"><p>کوه را جامهٔ زربفت نماید در بر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وام یک‌ماهه‌کت از بوسه به من باید داد</p></div>
<div class="m2"><p>همه را بازستانم ز تو بی‌بوک و مگر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بوسه‌ائی‌که در آن تگ‌دهان جمع‌شدست</p></div>
<div class="m2"><p>بشمار از تو بگیرم سپس یکدیگر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همی همی بوسمت از شوق و تو چون ناز کنی</p></div>
<div class="m2"><p>به ادب گویمت ای ماه غلط شد بشمر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا تو هم وارهی از زحمت یک‌ماه صیام</p></div>
<div class="m2"><p>مدح مستورهٔ آفاقت خوانم از بر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مهد علیا ملک دهر در درج وجود</p></div>
<div class="m2"><p>سترکبری فلک جود مه برج هنر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>قمر زهره بها زهرهٔ خورشید شرف</p></div>
<div class="m2"><p>هاجر ساره لقا سارهٔ بلقیس‌گهر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شمس‌‌خوانث به‌عفت نه قمرکاهل لغت</p></div>
<div class="m2"><p>مهر را ماده شمارند همه مه را نر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همچو خورشید عیانست‌و ز خلقست‌نهان</p></div>
<div class="m2"><p>که هم از پرتو خویشست مر او را معجر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ای به هرحال ترا بوده ز باری یاری</p></div>
<div class="m2"><p>وی به هر کار ترا آمده داور یاور</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>عکسی ار افتد زآیینهٔ حسن تو به زنگ</p></div>
<div class="m2"><p>می‌نماند ز سیاهی به همه زنگ اثر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>در ازل آدم اگر مدح تو می‌کردی ‌گوش</p></div>
<div class="m2"><p>هیچ کس تا ابد از مام نمی‌زادی کر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ور به ظلمات جمال تو فکندی پرتو</p></div>
<div class="m2"><p>ایمن از وحشت ظلمات شدی اسکندر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گر زنان حبشی روی تو آرند به یاد</p></div>
<div class="m2"><p>بجز از حور نزایند همی تا محشر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>واجب آمدکه مشیت نهمت نام از آنک</p></div>
<div class="m2"><p>آفرینش ز توگردید عیان سرتاسر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>آفرینش ز تو پیدا شد ها منکرکیست</p></div>
<div class="m2"><p>تاش گویم به سراپای ولیعهد نگر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ثانی رابعه‌یی در ورع و زهد و عفاف</p></div>
<div class="m2"><p>تالی آمنه‌یی درکرم و حسن سیر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>عیسی از چرخ زند عطسه اگر روح‌القدس</p></div>
<div class="m2"><p>عوض عود نهد موی ترا بر‌ مجمر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مگر از عصمت تو روح و خرد خلق شدند</p></div>
<div class="m2"><p>که به آثار عیانند و به صورت مضمر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گر در آن‌دم‌که خلیل‌الله بتها بشکست</p></div>
<div class="m2"><p>نقش رخسار تو بر بت بکشیدی آزر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>من برانم‌که براهیم ستغفارکنان</p></div>
<div class="m2"><p>بت بنشکستی و برگشتی زی‌ کیش پدر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بس ‌عجب‌نیست‌که از یمن عفافت تا حشر</p></div>
<div class="m2"><p>مادر فکرت من بکر بزاید دختر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>عصمتت‌بر خون‌ گر پرده‌کشیدی به عروق</p></div>
<div class="m2"><p>خون برون نامدی از رگ به هزاران نشتر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>وندر اوهام اگر عفت تو جستی جای</p></div>
<div class="m2"><p>نام مردان جهان راه نبردی به فکر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نسلها قطع شدی ورنه پس از زادن تو</p></div>
<div class="m2"><p>نطفه‌یی در رحم مام نمی‌گشت پسر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>سدی از عصمت تو گر به ره بادکشند</p></div>
<div class="m2"><p>تا به شام ابد از جای نجبند صرصر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تا دمد نیلوفر افتان خیزان به چمن</p></div>
<div class="m2"><p>باد افتان خیزان خصم تو چون نیلوفر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>لاله‌سان لال بود خصمت و بادا شب و روز</p></div>
<div class="m2"><p>خون سرخش به ‌رخ و داغ سیاهش به جگر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>شعر قاآنی اگر نطفه به زهدان شنود</p></div>
<div class="m2"><p>از طرب رقص نماید به مشیمهٔ مادر</p></div></div>