---
title: >-
    قصیدهٔ شمارهٔ ۱۶۶ - در ستایش امیرالامراءالعظام نظام‌الدوله حسین خان حکمران فارس فرماید
---
# قصیدهٔ شمارهٔ ۱۶۶ - در ستایش امیرالامراءالعظام نظام‌الدوله حسین خان حکمران فارس فرماید

<div class="b" id="bn1"><div class="m1"><p>صبح چون خورشید رخشان رخ نمود از کوهسار</p></div>
<div class="m2"><p>ماه من از در درآمد با رخی خورشیدوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بربجای شانه در زلفش همه پیچ و شکن</p></div>
<div class="m2"><p>بربجای سرمه در چشمش همه خواب و خمار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مژّهای چشم او‌ گیرنده چون چنگال شیر</p></div>
<div class="m2"><p>حلقهای زلف او پیچنده چون اندام مار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من همی گوهر فشاندم او همی عنبر فشاند</p></div>
<div class="m2"><p>من ز چشم اشکبار و او ز زلف مشکبار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت چشمت را همانا برلب من سوده‌اند</p></div>
<div class="m2"><p>کاینچنین ریزد ازو هرلحظه در شاهوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر فرا بردم به‌گوشش تا ببویم زلف او</p></div>
<div class="m2"><p>آمد از زلفش بگوشم نالهٔ دلهای زار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حلقهای زلف او را هر چه بگشودم ز هم</p></div>
<div class="m2"><p>هی دل وجان بود در هریک قطار اندر قطار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سایه و خورشیدگر باهم ندیدستی ببین</p></div>
<div class="m2"><p>زلفکان تابدار او بروی آبدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا سرین فربهش دیدم به وجد آمد دلم</p></div>
<div class="m2"><p>کبک آری می‌بخندد چون ببیند کوهسار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست بر زلفش‌کشیدم ناگهان از نکهتش</p></div>
<div class="m2"><p>مشت من پر مشک شد چون ناف آهوی تتار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسکه بوسیدم دهانش را لبم شد پر شکر</p></div>
<div class="m2"><p>بسکه بوییدم دو زلفش‌ را دلم شد بیقرار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا ندیدم زلف او افعی ندیدم مشکبوی</p></div>
<div class="m2"><p>تا ندیدم چشم او آهو ندیدم زهردار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتمش بنشین ‌که چین زلفکانت بشمرم</p></div>
<div class="m2"><p>گفت چین زلف من تا حشرناید در شمار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفتمش چین دو زلفت را اگر نتوان شمرد</p></div>
<div class="m2"><p>نسبتی دارد یقین با جود صاحب اختیار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غیث ساکب لیث‌ساغب صدر دی بدر امم</p></div>
<div class="m2"><p>حکمران ملک جم میر مهان فخر کبار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ناظم لشکر حسین خان آسمان داد و دین</p></div>
<div class="m2"><p>نامدار خطهٔ ایران امین شهریار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روی او ماهست و چشم دوستانش آسمان</p></div>
<div class="m2"><p>رمح او سروست و قد دشمنانش جویبار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وصف تیغ آتشینش بر لبم روزی‌ گذشت</p></div>
<div class="m2"><p>گشت حال چون دل دوزخ دهانم پر شرار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یاد رمحش کرد وقتی در خیال من خطور</p></div>
<div class="m2"><p>رست‌ حالی از بن هر موی من یک ‌بیشه خار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هیچ دانی از چه مالد روز کین گوش کمان</p></div>
<div class="m2"><p>زانکه ببیند پشت بر دشمن‌ کند در کارزار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سرو را ده سال افزونست تا از روی صدق</p></div>
<div class="m2"><p>در خلوص حضرتت مانند کوهم استوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>روزگاری مهرت از خاطر فراموشم نشد</p></div>
<div class="m2"><p>سخت ‌می‌ترسم فراموشم‌ کنی چون روزگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نیستم زر از چه افکندی چنینم از نظر</p></div>
<div class="m2"><p>نیستم سیم از چه فرمودی مرا اینگونه خوار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نی سپهرم تا مرا قدرت کند بی‌احترام</p></div>
<div class="m2"><p>نه جهانم تا مرا جاهت‌ کند بی‌اعتبار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قدر من باری بدان و شعر من گاهی بخوان</p></div>
<div class="m2"><p>نام من روزی بپرس و کام من وقتی برآر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شعر قاآنی تو پنداری شراب خلرست</p></div>
<div class="m2"><p>هر که از وی مست شد بس دیر گردد هوشیار</p></div></div>