---
title: >-
    قصیدهٔ شمارهٔ ۲۹۶ - فی المدیحه
---
# قصیدهٔ شمارهٔ ۲۹۶ - فی المدیحه

<div class="b" id="bn1"><div class="m1"><p>حبذا تشریف شاهشاه دریا آستین</p></div>
<div class="m2"><p>مرحبا اندام جان‌افروز صدر راستین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لو حش‌الله خلعتی بر یک فلک شوکت محیط</p></div>
<div class="m2"><p>مرحباالله پیکری با یک جهان رحمت عجین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلعتی تهلیل‌گو از حیرتش مهر منیر</p></div>
<div class="m2"><p>پیکری تسبیح‌خوان از عزتش چرخ برین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلعتی رایات نورش بر یمین و بر یسار</p></div>
<div class="m2"><p>پیکری آیات مجدش بر یسار و بر یمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلعتی ‌کز بس ضیابر آفتاب آرد شکست</p></div>
<div class="m2"><p>پیکری ‌کز بس بها بر آسمان نازد زمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلعتی خورشیدوار آرایش ملک جهان</p></div>
<div class="m2"><p>پیکری طوبی صفت پیرایهٔ خلد برین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلعتی از نور او بدر فروزان شرمسار</p></div>
<div class="m2"><p>پیکری از نور او مهر درخشان شرمگین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلعتی از رشک ار در پیکر ناهید تاب</p></div>
<div class="m2"><p>پیکری از تاب او بر چهرهٔ خورشید چین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خلعتی از فرهی خجلت ده بدر منیر</p></div>
<div class="m2"><p>پیکری از روشنی رونق بر درّ ثمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خلعتی نه حجّتی از رحمت پروردگار</p></div>
<div class="m2"><p>پیکری نه آیتی از قدرت جان‌آفرین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خلعتی نه سایه‌یی از شهپر روح‌القدس</p></div>
<div class="m2"><p>پیکری نه مایه‌یی از طینت روح‌الامین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خلعتی کش پیکری شایسته شاید آنچنان</p></div>
<div class="m2"><p>پیکری‌کش خلعتی بایسته باید این چنین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خلعت شاهنشه‌گیهان فریدون جهان</p></div>
<div class="m2"><p>پیکر فرمانده‌ کشور منوچهر مهین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>داور اقلیم جم فرمانده ملک عجم</p></div>
<div class="m2"><p>غوث‌ ملت، کهف ‌دولت، صدر دنیا، بدر دین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرکجابادی ز خشمش‌مهرگان درمهرگان</p></div>
<div class="m2"><p>هرکجاذکری ز لطفش فرودین در فرودین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از هراسش یک جهان دشمن نفیر اندر نفیر</p></div>
<div class="m2"><p>از نهیبش یک زمین لشکر حنین اندر حنین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از قدش وصفی خیابان در خیابان نارون</p></div>
<div class="m2"><p>از رخش مدحی گلستان در گلستان یاسمین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>موکبش در دشت هیجا چون‌ کمان اندر کمان</p></div>
<div class="m2"><p>لشکرش در روز غوغا چون‌کمین اندرکمین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قیروان تا قیروان ترکان غریو اندر غریو</p></div>
<div class="m2"><p>باختر تا باختر گردان انین اندر انین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بسته خم‌ کمندش در وغا یال ینال</p></div>
<div class="m2"><p>خسته نوک پرندش روزکین ترگ تکین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرز او در چنگ او البرز در بحر محیط</p></div>
<div class="m2"><p>برز او بر خنگ او الوند بر باد بزین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>با خطابش صبح صادق تابد از شام سیاه</p></div>
<div class="m2"><p>باعتابش نار سوزان خیزد از ماء معین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هرکجا شستش به تیر دال پریابد قران</p></div>
<div class="m2"><p>هرکجا دستش به تیغ جان شکر گردد قرین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درفلک از سهم ‌گردد چون سها پنهان‌ سهیل</p></div>
<div class="m2"><p>در رحم ‌از بیم ‌گردد چون جرس نالان جنین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خاک راهش مر قمر را در فلک خاک عذار</p></div>
<div class="m2"><p>داغ مهرش مر جبین را در رحم نقش جبین‌</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نی به‌غبراز سیم‌و زر یک‌تن درایامش ملول</p></div>
<div class="m2"><p>نی به ‌غیر از بحر و کان یک‌دل در ایامش حزین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون به خشم آید نماید قهر جان‌فرسای او</p></div>
<div class="m2"><p>بیش از جدوار و نیش از نوش و زهر از انگبین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>قدر او قصری رفیع و حزم او حصنی منیع</p></div>
<div class="m2"><p>جاه او ملکی وسیع و فکر او سوری متین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مهر از آن برگنبد خاکستری دارد مقام</p></div>
<div class="m2"><p>کاو همی از شرم رایش گشته خاکستر نشین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر پناهدحاسد از خشمش به صد حصن بلند</p></div>
<div class="m2"><p>ور گریزد دشمن از قهرش به صد سور رزین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از کمندش سر نیارد تافت در میدان رزم</p></div>
<div class="m2"><p>از پرندش جان نخواهد برد در مضمار کین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>می‌نبخشد نفع در دفع اجل سدّ سدید</p></div>
<div class="m2"><p>می‌ندارد سود در طرد قضا حصن حصین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>داد بخشاد او را ای آنکه افتد روز جنگ</p></div>
<div class="m2"><p>از غریو کوست اندر گنبد گردان طنین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>صدرهٔ بخت ترا بی‌جادهٔ خورشیدگوی</p></div>
<div class="m2"><p>خاتم قدر ترا فیروزه ‌گردون نگین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مر به شکر آنکه شد از یمن بخت آراسته</p></div>
<div class="m2"><p>قامت موزونت از تشریف شاه راستین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز اقتضای جود عام وز اختصاص لطف خاص</p></div>
<div class="m2"><p>هم به‌ تشریفی‌ رهی ‌را می‌توان‌ کردن رهین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خلعتت ‌را زیب‌ تن ‌سازند خلق از فخر و من</p></div>
<div class="m2"><p>سازمش تعویذ جان از هول روز واپسین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا که راز سرمدی را درک نتواند گمان</p></div>
<div class="m2"><p>تا که ذات ایزدی را فهم نتواند یقین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آنی ازساعات عمرت‌هرچه درگیتی شهور</p></div>
<div class="m2"><p>روزی از ایام بختت هرچه در عالم سنین</p></div></div>