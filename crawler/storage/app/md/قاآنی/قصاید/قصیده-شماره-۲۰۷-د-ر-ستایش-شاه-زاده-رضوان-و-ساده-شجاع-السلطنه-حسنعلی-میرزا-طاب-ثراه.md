---
title: >-
    قصیدهٔ شمارهٔ ۲۰۷ - د‌ر ستایش شاه‌زادهٔ رضوان و ساده شجاع‌السلطنه حسنعلی میرزا طاب‌ثراه
---
# قصیدهٔ شمارهٔ ۲۰۷ - د‌ر ستایش شاه‌زادهٔ رضوان و ساده شجاع‌السلطنه حسنعلی میرزا طاب‌ثراه

<div class="b" id="bn1"><div class="m1"><p>چیست آن اژدها نهاد نهنگ</p></div>
<div class="m2"><p>که ز پیریش چهره پر آژنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم ازو در ایاق دوست شراب</p></div>
<div class="m2"><p>هم ازو در مذاق خصم شرنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم به‌ کابل ازو نهیب و خروش</p></div>
<div class="m2"><p>هم به زابل ازو غریو و غرنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم ازو ویله در اراضی روم</p></div>
<div class="m2"><p>هم ازو مویه در نواحی زنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم ولاول ازو به خلخ و چین</p></div>
<div class="m2"><p>هم زلازل ازو به تبت و تنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاه آردگذر به تارک شیر</p></div>
<div class="m2"><p>گاه سازد مقر به‌کام پلنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رنگ مرآت‌گون او به مصاف</p></div>
<div class="m2"><p>جز به خون عدو نگیرد زنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گردن شیر تابد از پیکار</p></div>
<div class="m2"><p>ز نخ دیو پیچد از نیرنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر به خرچنگ دیده‌یی مه نو</p></div>
<div class="m2"><p>در مه نو نظاره‌ کن خرچنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حامی دین چنان‌که یارد ساخت</p></div>
<div class="m2"><p>کعبه را درکلیسیای فرنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کسوت جان نگیرد از دشمن</p></div>
<div class="m2"><p>تا نگردد برهنه در صف جنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از شررباریش گریزانست</p></div>
<div class="m2"><p>پیل از میل و شیر از فرسنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان شیرین ز خصم‌گیرد از آن</p></div>
<div class="m2"><p>فوج موران درو زنند کُرنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مسکنش دست خسروست آری</p></div>
<div class="m2"><p>بحر زیبد قرارگاه نهنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خسرو راستین حسن‌شه راد</p></div>
<div class="m2"><p>که خرد را ز رای او فرهنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه از فرط عدل او شاهین</p></div>
<div class="m2"><p>لب پر از شکوه دارد از تو رنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شیر عزمش به چرخ داده شتاب</p></div>
<div class="m2"><p>وقر حزمش به‌خاک داده درنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فرق ناکرده بزم را از رزم</p></div>
<div class="m2"><p>می‌ندانسته جشن را از جنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نال نایش به‌گوش نالهٔ نای</p></div>
<div class="m2"><p>شور شورش به مغر نغمهٔ چنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سطوت او کند ثریا را</p></div>
<div class="m2"><p>بس پراکنده‌تر ز هفت اورنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>داده جودش حشیش بُخل بر آب</p></div>
<div class="m2"><p>زده عدلش زجاج فتنه به سنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون برد دست بر به ‌گرز گران</p></div>
<div class="m2"><p>چون زند شست بر به تیر خدنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تن بشوید به آب مرگ فرود</p></div>
<div class="m2"><p>رخ بپوشد به خاک تیره پشنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مدحت آرد به محرمان دارا</p></div>
<div class="m2"><p>بذله‌گوید به پیلتن ارژنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خسروا ای ز یمن معدلتت</p></div>
<div class="m2"><p>روی‌گیتی سراچهٔ ارژنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مُلک را از نگار رأفت تو</p></div>
<div class="m2"><p>طعنها بر نگارخانهٔ‌گنگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با توان تو دست دوران شل</p></div>
<div class="m2"><p>با سمند تو پای‌ گردون لنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون نهی‌پای‌، در چه در میدان</p></div>
<div class="m2"><p>چون‌ کنی جای‌،‌بر چه بر اورنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر یکی اشقری دو صد کاموس</p></div>
<div class="m2"><p>بر یکی مسندی دوصد هوشنگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>روزکین‌کز خروش شندف و نای</p></div>
<div class="m2"><p>کر شود گوش روزگار از عنگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه به سرها ز ترس ماند هوش</p></div>
<div class="m2"><p>نه به تنها ز بیم ماند هنگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر هژبری عیان به‌کوههٔ دیو</p></div>
<div class="m2"><p>هر نهنگی نهان به چرم پلنگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون تو بیرون خرامی از مکمن</p></div>
<div class="m2"><p>شیرسان بر نشسته بر شبرنگ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سفته یاقوت را به مروارید</p></div>
<div class="m2"><p>تیغ الماس گون ‌گرفته به چنگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در زمین وغا ز خون یلان</p></div>
<div class="m2"><p>رود نیل آوری به یک آهنگ</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خاک را لعل سازی از الماس</p></div>
<div class="m2"><p>چرخ را پر وزن ‌کنی ز پرنگ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خسروا ای‌که زهره در بزمت</p></div>
<div class="m2"><p>به نوای طرب زند آهنگ</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>عقل اگر با تو لاف فهم زند</p></div>
<div class="m2"><p>کودکانش همی زنند به سنگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شاهی اندر قفای تو پویان</p></div>
<div class="m2"><p>ورنه شخص ترا ز شاهی ننگ</p></div></div>