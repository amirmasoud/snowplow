---
title: >-
    قصیدهٔ شمارهٔ ۱۸۷ - در ستایش امیرالامراء العظام مبرزا نبی خان رحمه الله فرماید
---
# قصیدهٔ شمارهٔ ۱۸۷ - در ستایش امیرالامراء العظام مبرزا نبی خان رحمه الله فرماید

<div class="b" id="bn1"><div class="m1"><p>محمود ماه من‌که غلامش بود ایاز</p></div>
<div class="m2"><p>دیشب دعای میر بدینگونه‌کرد ساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر کف‌ گرفت زلف ‌که یارب به موی من</p></div>
<div class="m2"><p>عمر امیر کن چو سر زلف من دراز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خشمش چو هجر طلعت من باد دلشکن</p></div>
<div class="m2"><p>مهرش چو ماه عارض من باد دلنواز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مال کس چو خواجه ی من باد بی‌طمع</p></div>
<div class="m2"><p>درکار دین چو عاشق من باد پاکباز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خصمش چو زلف تیرهٔ من باد سرنگون</p></div>
<div class="m2"><p>بختش چو سرو قامت من باد سرفراز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گیتی چو من به‌ حضرت جاهش برد سجود</p></div>
<div class="m2"><p>گردون چو من به درگه قدرش برد نماز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در کار خصم و چهر حسودش زند سپهر</p></div>
<div class="m2"><p>هر عقده‌ای‌ که من کنم از زلف خویش باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اخلاق او چو موی من از طبع مشک‌بیز</p></div>
<div class="m2"><p>اقبال او چو حسن من از وصف بی‌نیاز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خصم وی و دهان من این هر دو بی‌نشان</p></div>
<div class="m2"><p>خشم وی و فراق من این هر دو جانگداز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در تیر او چو مژهٔ باد تعبیه</p></div>
<div class="m2"><p>دندان شیر شرزه و چنگال شاهباز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در چنگ او چو طرهٔ من خام شصت خم</p></div>
<div class="m2"><p>در دست او چو قامت من رُمح هشت‌باز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آوازهٔ جلال وی و صیت حسن من</p></div>
<div class="m2"><p>باد از عراق رفته همه روز تا حجاز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گنجش چوگنج فکر تو لبریز ازگهر</p></div>
<div class="m2"><p>ملکش چو ملک حسن من ایمن زترکتاز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پرورده همچو طبع تو اندر وفا و مهر</p></div>
<div class="m2"><p>آسوده همچو شخص من اندر نعیم و ناز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ممتاز باد شخص وی از والیان عصر</p></div>
<div class="m2"><p>چونان‌که من ز خیل بتان دارم امتیاز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیدا بر او چو نقش جمالم وجود جود</p></div>
<div class="m2"><p>پنهان بر او چو سرّ دهانم نشان آز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>محمود باد عاقبت او چو نام من</p></div>
<div class="m2"><p>با طالعی خجسته‌تر از طلعت ایاز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وآخر چه‌گفت‌گفت‌که قاآنیا. چو شمع</p></div>
<div class="m2"><p>در عشق من بسوز و به سودای من بساز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا خواجهٔ منستی در بندگی بکوش</p></div>
<div class="m2"><p>تا بندهٔ امیری بر خواجگان بناز</p></div></div>