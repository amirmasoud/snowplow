---
title: >-
    قصیدهٔ شمارهٔ ۱۶۱ - مطلع ثانی
---
# قصیدهٔ شمارهٔ ۱۶۱ - مطلع ثانی

<div class="b" id="bn1"><div class="m1"><p>مژده که شد در چمن رایت گل آشکار</p></div>
<div class="m2"><p>مژده ‌که سر زد سمن از دمن و مرغزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وجد کنان شاخ‌ گل از اثر باد صبح</p></div>
<div class="m2"><p>رقص ‌کنان سرو ناز بر طرف جویبار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاله به‌ کف جام می‌ گشته مهیای عشق</p></div>
<div class="m2"><p>گر چه ز نقصان عمر هست به دل داغدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوش فراداده ‌گل تا به چمن بشنود</p></div>
<div class="m2"><p>از دهن عندلیب شرح غم بی‌شمار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان به زبان فصیح ‌کرده روایات شوق</p></div>
<div class="m2"><p>قصه ز هجران‌ گل شکوه ز بیداد خار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وقت سحر گشت باز دیدهٔ نرگس ز خواب</p></div>
<div class="m2"><p>تاکه صبوحی زند از پی دفع خمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غنچه‌ گشاید دهن تا که ز پستان ابر</p></div>
<div class="m2"><p>از قطرات مطر شیر خورد طفل‌وار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باد به رخسار باغ غالیه‌سایی ‌کند</p></div>
<div class="m2"><p>زلف سمن را دهد نفحهٔ مشک تتار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چهر ریاحین رود در عرق از آفتاب</p></div>
<div class="m2"><p>مِروَحه زانرو دهد باد به دست چنار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لاله به سان صدف ابر در او چون گهر</p></div>
<div class="m2"><p>شاخ شود بارور باد شود مشک‌بار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوسن از آن رو شدست شهره به آزادگی</p></div>
<div class="m2"><p>کز دل و جان می‌کند مدح شه ‌کامگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاه بهادر لقب میر سکندر نسب</p></div>
<div class="m2"><p>داور دارا حسب هرمز کسری شعار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهمن جم احتشام ‌کاوست حسن شه به نام</p></div>
<div class="m2"><p>مهر سپهرش غلام عقد نجومش نثار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنکه به ایوان بزم آمده جمشید عزم</p></div>
<div class="m2"><p>وانکه به میدان رزم هست چو سام سوار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شعلهٔ تیغش در آب ‌گر فکند عکس خویش</p></div>
<div class="m2"><p>زآب چو آتش جهد جای ترشح شرار</p></div></div>