---
title: >-
    قصیدهٔ شمارهٔ ۱۷۴ - در ستایش‌امیرالامر‌اء‌العظام‌نظام‌لدوله حسین‌خان حکمران فارس فرماید
---
# قصیدهٔ شمارهٔ ۱۷۴ - در ستایش‌امیرالامر‌اء‌العظام‌نظام‌لدوله حسین‌خان حکمران فارس فرماید

<div class="b" id="bn1"><div class="m1"><p>همتی مردانه می‌خواهم‌که اسمعیل‌وار</p></div>
<div class="m2"><p>بر خلیل خویشتن امروز جان سازم نثار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عید قربانست و من قربان آن عیدی‌ که هست</p></div>
<div class="m2"><p>کوی او دایم بهشت و روی او دایم بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان سبب قربان اسمعیل باید شدکه او</p></div>
<div class="m2"><p>گشت قربان ‌کسی ‌کاو را ز قربانیست عار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عار دارد آری از قربانی آن یاری که هست</p></div>
<div class="m2"><p>نور هستی از فروغ ذات پاکش مستعار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در چنین روزی ‌که اسمعیل شد قربان دوست</p></div>
<div class="m2"><p>بهتر از امروز روزی نبود اندر روزگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من به حق قربان اسمعیل خواهم شدکه او</p></div>
<div class="m2"><p>عاشق حق بود و عاشق راست قربانی شعار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشتهٔ‌کوی محبت را دعا نفرین بود</p></div>
<div class="m2"><p>زین دعا بالله‌ کز اسمعیل هستم شرمسار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من چه حد دارم شوم قربان قربانی‌که او</p></div>
<div class="m2"><p>بس امام پاک‌زاد و بس خلیفهٔ نامدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچ‌ر ابمعیل منهم جان‌کنم قربان دوست</p></div>
<div class="m2"><p>گو مرا دشمن در آذر افکن ابراهیم‌وار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مردم اسمعیلیم خوانند و حق دارند از آنک</p></div>
<div class="m2"><p>نام اسمعیل رانم بر زبان بی‌اختیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اختیاری نیست عاشق را به ذکر نام دوست</p></div>
<div class="m2"><p>عشق اول اختیارست عشق آخر اضطرار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا نپنداری که اسمعیل جان قربان نکرد</p></div>
<div class="m2"><p>کاو گذشت از جال شیرین در حقیقت چند بار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وقت گفتن وقت رفتن وقت خفتن زیر تیغ</p></div>
<div class="m2"><p>کرد جان تسلیم و در سر باختن بد پایدار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور دلش را رای آن بودی‌ که بهراسد ز مرگ</p></div>
<div class="m2"><p>هفت ره ابلیس را در ره نکردی سنگسار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کار عاشق این بود کز جان شیرین بگذرد</p></div>
<div class="m2"><p>وان دگر معشوق داند کشتنش یا زینهار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همچو اسمعیل ‌کاو جان ‌داد اگر یارش نکشت</p></div>
<div class="m2"><p>می نباید کشت اسمعیل را بر رغم یار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>او‌ به‌معنی جان فداکرد ارچه در صورت خدا</p></div>
<div class="m2"><p>کرد میش او را فدا کاین‌ کیش ماند برقرار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حرمت ‌او راست ‌کاندر عید قربان تا به‌ حشر</p></div>
<div class="m2"><p>این همه قربان ‌کنند از بهر قرب ‌کردگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>راستی را عید قربان بهترین عیدست از آنک</p></div>
<div class="m2"><p>در نشاط آیند جانبازان عشق از هر کنار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>میش ‌را عامی‌ کند قربان‌ و مقصودش ریا</p></div>
<div class="m2"><p>خویش را عارف کند قربان و عزمش‌ انکسار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن به بیع‌کشتهٔ‌خود خونبها خواهد ز دوست</p></div>
<div class="m2"><p>آن به ریع ‌کشته خود برخورد از کشتزار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>راستی گویم کسی تا سر نبازد پیش دوست</p></div>
<div class="m2"><p>دشمن یارست اگر خود را شمارد دوستدار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عشق طغیان کرد باز ای دل فروکش سر به جیب</p></div>
<div class="m2"><p>یا اگر بر صدق دعوی حجتی داری بیار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یا بیا چون شیر مردان سر بنه در پیش تیغ</p></div>
<div class="m2"><p>یا برو چون نوعروسان یا بکش از نیش خار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رستم ‌کاموس‌ بند اشکبوس افکن رسید</p></div>
<div class="m2"><p>جنگ را گر مرد جنگی زاستین دستی برآر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عشق ‌سهرابست‌بر وی حمله‌کم‌کن ای هجیر</p></div>
<div class="m2"><p>رود غرقابست در وی باره‌کم ران ای سوار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پشه‌یی در کاهدان خز خرطم پیلان مگز</p></div>
<div class="m2"><p>روبهی در لانه بنشین‌ گردن شیران مخار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>راستی گر عاشقی جان آشکارا ده به دوست</p></div>
<div class="m2"><p>پیش از آن‌ کت مرگ موعود از کمین‌ سازد شکار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>‌گر نه مفتی جهولی پیش از استفتا بگو</p></div>
<div class="m2"><p>و‌رنه ابر خشک‌سالی پیش‌ از استسقا ببار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عقل را بنیان بکن چون عشق شد فرمانروا</p></div>
<div class="m2"><p>شمع را‌ردن بزن چون صبح‌‌ردید آشکار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رنج‌ و راحت هر دو همسنگند در میزان عشق</p></div>
<div class="m2"><p>شیر و قطران هر دو همرنگند در شبهای تار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پشک را عنبر شمر چون‌ گشت با مغز آشنا</p></div>
<div class="m2"><p>زهر را شکر شمر چون گشت با تن سازگار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرد افیون‌ خوار می‌نندیشد از افیون تلخ</p></div>
<div class="m2"><p>شخص افسون‌کار می‌ نهراسد از دندان مار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زشت و زیبا هر دو مطبوعست نزد حق‌پرست</p></div>
<div class="m2"><p>شور و شیرین هر دو ممدوحند نزد حق گزار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عیب‌ مردم پیش ازین می‌گفتم‌ اندر چشم خلق</p></div>
<div class="m2"><p>و‌رقتیم آیینه‌گفتا آخر از خود شرم‌دار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>با چنین پستی ‌که داری لاف رعنایی مزن</p></div>
<div class="m2"><p>با چنین زشتی که داری تخم زیبایی مکار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عیب‌جویی را بهل هیچ ار هنر داری بگو</p></div>
<div class="m2"><p>غیب‌گویی را بنه هیچ ار خبر داری بیار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یک خبر دارم بلی یزدان بود پوزش پذیر</p></div>
<div class="m2"><p>یک هنر دارم بلی هستم به ‌حق امیدوار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای دل از سر باختن‌ گردن مکش در پیش دوست</p></div>
<div class="m2"><p>کانکه بر جانان سپارد جان عوض گیرد هزار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>میش‌ قربانی‌ کش اینک ‌کشته بینی هر طرف</p></div>
<div class="m2"><p>باز هر لقمه از آن‌ گردد روانی هوشیار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>لقمهٔ او سنگ را ماند کز اول تیره است</p></div>
<div class="m2"><p>چون‌گدازد آینهٔ روشن شود انجام‌کار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>قدر سربازی شناسد آن‌کسی‌کز روی شوق</p></div>
<div class="m2"><p>جان‌فشاند همچو میرملک جم‌بر شهریار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>میر دریا دل حسین‌خان آسمان مکرمت</p></div>
<div class="m2"><p>صدر دین بدر هدی بحر کرم‌ کوه وقار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دست گوهربخش او هرگه که بنشیند به رخش</p></div>
<div class="m2"><p>بحر عمانست‌گویی بر فرازکوهسار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شش جهت‌ از ساحت جاهش یکی‌کوته ارش</p></div>
<div class="m2"><p>نه سپهر از کشتی جودش یکی تاری بخار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>با سر پیکان تیرش چون بود اندک شبیه</p></div>
<div class="m2"><p>رم‌ کند از تکمه ی پستان مادر شیرخوار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چهر او تن را توان و مهر او دل را نوان</p></div>
<div class="m2"><p>جود او جان را امان و تیغ او دین‌ را حصار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کوه با فکرش‌ بود در دانهٔ ارزن نهان</p></div>
<div class="m2"><p>چرخ با حزمش ‌کند در چشمهٔ سوزن مدار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گر خیال عزم او گیرد محاسب در ضمیر</p></div>
<div class="m2"><p>جمع‌ و خرج هر دو گیتی یک دم آرد در شمار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>قدرش ار گشتی مجسم جا در او کردی جهان</p></div>
<div class="m2"><p>جودش ار بودی مصور موج او بودی بحار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>روزی اندر باغ گفتم بخت او پاینده باد</p></div>
<div class="m2"><p>دانه زیر خاک آمین گفت و برگ از شاخسار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>وقتی آمد بر زبانم از سخای او سخن</p></div>
<div class="m2"><p>ماهی از دریا ستایش ‌کرد و مرغ از مرغزار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نام قهر او تو پنداری‌ که باد صرصرست</p></div>
<div class="m2"><p>تا برم بر لب زمین و آسمان‌ گیرد غبار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دوش دیدم ساحری را بر کنار جوی خشک</p></div>
<div class="m2"><p>خواند چیزی کاب جاری گشت اندر جویبار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گفتم این افسون که بر خواندی چه بود ای بوالحیل</p></div>
<div class="m2"><p>کاب جاری‌ گشت و طغیان‌ کرد سیل از هر‌ کنار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گفت حکم میر ملک جم ز بس جاری بود</p></div>
<div class="m2"><p>چون حدیثش بر لب آرم آب جوشد از قفار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گفتم افسون دگر دانی‌ که بخشد این اثر</p></div>
<div class="m2"><p>گفت آری شعر قاآنی ز بس هست آبدار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چون فرو خوانی همانا شعر او بر کوه و دشت</p></div>
<div class="m2"><p>راست گویی سیل‌خیز آمد مدرگاه مدار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گفتمم‌ا ج‌ری روان را هم ت‌رانی‌ر‌رد خشک</p></div>
<div class="m2"><p>گفت می‌سوزم مپرس این حرف‌کلا زینهار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>عجز‌ کردم لابه کردم کاین سخن سهلست سهل</p></div>
<div class="m2"><p>این عمل را نیز حواهم‌کز تو ماند یادگار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>عجزمن‌ چون دید حرزی خواند و از هر سو دمید</p></div>
<div class="m2"><p>رو به‌ گردون‌ کرد کم حافظ شو ای پروردگار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>وانگهی آهسته چون موری‌ کز او خیزد نفس</p></div>
<div class="m2"><p>گفت درگوشم که نام تیغ میر کامگار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هرکجا نهریست بی‌پایان و بحری بیکران</p></div>
<div class="m2"><p>چون بری این نام آبش سر به‌ سر‌ گردد بخار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ای کهین سرباز خسرو ای مهین سالار دهر</p></div>
<div class="m2"><p>ای ز تو دولت قویم وای ز تو دین پایدار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>با رشاد حزم تو هشیاری آرد جام می</p></div>
<div class="m2"><p>باسهاد بخت تو بیداری آرد کوکنار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بس که از هرسو ‌گر‌یزد مرگ بیند پیش ‌روی</p></div>
<div class="m2"><p>شاید از میدان ‌کینت خصم ننماید فرار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دربیابان دی نوشتم نام حلمت بر زمین</p></div>
<div class="m2"><p>ناگهم از پیش رو برجست‌ کوهی استوار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دوش گفتم وضعی از جودت نمایم مختصر</p></div>
<div class="m2"><p>عقل گفتا شرمی آخر جودش آنگه اختصار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چون به حشر اعمال نیکوی ترا نتوان شمرد</p></div>
<div class="m2"><p>پس چرا خواند عجم آن روز را روز شمار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>هر کجا نامی ز نطقت قند و شکر تنگ تنگ</p></div>
<div class="m2"><p>هر کجا یادی ز خلقت مشک و عنبر باربار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>وصف جودت زان کنم پیش از همه اوصاف تو</p></div>
<div class="m2"><p>تا به وصفش نیز سامع را نماند انتظار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>حیلتی کردم که تا شد صیت فضلم مشتهر</p></div>
<div class="m2"><p>نامی از جود تو بردم یافت فضلم اشتهار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>تا بود رمحت نزار و تا بود گرزت سمین</p></div>
<div class="m2"><p>دین ازین بادا سمین وکفر از آن بادا نزار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>شعر قاآنی برین نسبت اگر بالا رود</p></div>
<div class="m2"><p>یا به‌کرسی می‌نشیند یا به عرش کردگار</p></div></div>