---
title: >-
    قصیدهٔ شمارهٔ ۳۲۳ - در مدح شاهزاده حسنعلی میرزای شجاع السلطنه فرماید
---
# قصیدهٔ شمارهٔ ۳۲۳ - در مدح شاهزاده حسنعلی میرزای شجاع السلطنه فرماید

<div class="b" id="bn1"><div class="m1"><p>ای دفتر گل از ورق حسن تو بابی</p></div>
<div class="m2"><p>با آب رخت چشمهٔ خورشید سرابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نالان دلم ار برده دو چشمت عجبی نیست</p></div>
<div class="m2"><p>در دست دو مست از پی تفریح ربابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با دیدهٔ تر برد ز فکر تو مرا خواب</p></div>
<div class="m2"><p>بی‌روی تو نقشی زدم امروز بر آبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وصف دهنت زان ننوشتیم به دیوان</p></div>
<div class="m2"><p>کان نقطهٔ موهوم نگنجد به کتابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بو که کند نرگس مست تو تمنا</p></div>
<div class="m2"><p>از لخت جگرکرده‌ام امروزکبابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتاگذرم بر سر خاک تو پس از مرگ</p></div>
<div class="m2"><p>ترسم‌ که ز یادش رود ای مرگ شتابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک لعل تو جان برد و دگر لعل تو جان داد</p></div>
<div class="m2"><p>وین طرفه ‌که هر یک به دگرگونه عتابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وقتست که دل رشوه برد بوسه ز هر یک</p></div>
<div class="m2"><p>اکنون ‌که میانشان شده پیدا شکرآبی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از خجلت منظور شه ار نیست چرا هست</p></div>
<div class="m2"><p>بر چهرهٔ چون ماه تو پیوسته نقابی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن مهتر فرخنده‌که ازکاخ رفیعش</p></div>
<div class="m2"><p>برتر نبود در همه آفاق جنابی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رشحی ز سحاب ‌کف او یا که محیطی</p></div>
<div class="m2"><p>موجی ز محیط دل او یاکه سحابی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنگونه رفیعست رواقش ‌که نماندست</p></div>
<div class="m2"><p>مابین وی و عرش برین هیچ حجابی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنجاکه سجاب ‌کف او ژاله‌فشانست</p></div>
<div class="m2"><p>بالله‌که اگر ابر درآید به حسابی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بذل وکف رادش‌کرم و طبع جوادش</p></div>
<div class="m2"><p>این ویسه و رامینی و آن دعد و ربابی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با ریزش ابرکف او ابر دخانی</p></div>
<div class="m2"><p>با بخشش بحر دل او بحر حبابی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای ساقی مجلس زیرم جام شرابی</p></div>
<div class="m2"><p>لب‌تشنهٔ دل‌سوخته را جرعهٔ آبی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زان آب‌ که از شعلهٔ او برق فروغی</p></div>
<div class="m2"><p>زان آب‌ که از تابش او صاعقه تابی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زان آب‌که خود آتش سردست ولیکن</p></div>
<div class="m2"><p>در ملک جهان نیست از آن‌ گرم‌تر آبی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زان آب‌که آید به پیش روح چو آدم</p></div>
<div class="m2"><p>گر قطره‌ای از وی بچکانی به ترابی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زان آب‌ که بی‌منت اکسیر ز تاثیر</p></div>
<div class="m2"><p>مس را کند از نیم ترشح زر نابی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آبی‌که چو بر قبرگنهکار فشانند</p></div>
<div class="m2"><p>نبود به دلش واهمه از روز حسابی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آبی‌ که اگر نوشد پیری‌ کند ادراک</p></div>
<div class="m2"><p>ایام پسندیده‌تر از عهد شبابی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آبی ‌که چو بر جبههٔ بیمار فشانند</p></div>
<div class="m2"><p>با فایده‌تر دردسرش را زگلابی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آبی‌که اگر صعوه‌کند رشحی از آن نوش</p></div>
<div class="m2"><p>بی‌ شبهه شکارش نکند هیچ عقابی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آبی‌که چو آقانی اگر نوش‌کندکس</p></div>
<div class="m2"><p>یابد ز پی مدح ملک فکر مصابی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دارای جوانبخت حسن شاه‌ که او را</p></div>
<div class="m2"><p>گردون نکند جز به ابوالسیف خطابی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آن خسرو عادل که به جز کاخ ستم نیست</p></div>
<div class="m2"><p>ز آبادی عدلش به جهان جای خرابی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رمحش بود آن افعی پیچان‌که بنابش</p></div>
<div class="m2"><p>از خون بداندیش بود سرخ لعابی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بختش بود آن شاخ برومندکه طوبی</p></div>
<div class="m2"><p>در نسبت او خردتر از برگ سدابی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در خدمتش آنان که سر از پای شناسند</p></div>
<div class="m2"><p>در دیدهٔ ارباب عقول‌اند دوابی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مشکل‌که شود با سخطش در دل اصداف</p></div>
<div class="m2"><p>یک قطره از این پس به شبه درّ خوشابی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ای آنکه ز کیمخت فلک ساخته ز آغاز</p></div>
<div class="m2"><p>سرّاج قضا تیغ تو را سبز قرابی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از غایت ابذال نعم سایل نعمت</p></div>
<div class="m2"><p>الا نعم از لفظ تو نشنیده جوابی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>با فرهٔ شهباز جلال تو به گیتی</p></div>
<div class="m2"><p>سیمرغ ‌کم از خادی و عنقا ز ذبابی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خون بی‌مدد خلق تو زنهار که گردد</p></div>
<div class="m2"><p>در ناف غزالان ختن نافهٔ نابی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هنگام رضا بر صفت عفو خداوند</p></div>
<div class="m2"><p>صد سیئه را عفو تو بخشد به ثوابی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یا فتح شود فتنهٔ تیغ تو چو داماد</p></div>
<div class="m2"><p>از خون عدوکرده عروسانه خضابی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شمشیر جهانسوز تو در تیره قرابش</p></div>
<div class="m2"><p>رخشنده هلالیست به تاریک سحابی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یا خیره نهنگیست تن اوبار به نیلی</p></div>
<div class="m2"><p>یا شرزه هژبریست عدو خوار بغابی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در ملک جهان دیدهٔ نُه چرخ ندیده</p></div>
<div class="m2"><p>چون‌ دانش تو شیخی ‌و چون بخت تو شابی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اقبال تو فرسوده مدار فلک از عمر</p></div>
<div class="m2"><p>وین طرفه‌که چون او نبود تازه مشابی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از ماه چو یکران تو را بست فلک نعل</p></div>
<div class="m2"><p>پنداشت‌که صادر شده زو فعل صوابی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از قدر تفاخر به قدرکرد و قضا دید</p></div>
<div class="m2"><p>غژمان ز سر خشم بدوکرد عتابی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کاین نعل تباهی ز چه بستی به سمندی</p></div>
<div class="m2"><p>کش حلقهٔ خورشید نیرزد به رکابی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>برگردن خفاش صفت خصم تو بندد</p></div>
<div class="m2"><p>هر روز خور از شعشعهٔ خویش طنابی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جز تیغ توکز تن چکدش خون بداندیش</p></div>
<div class="m2"><p>حاشاکه ز الماس چکد لعل مذابی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون موج زند لجهٔ جود تو نماید</p></div>
<div class="m2"><p>بر ساحت او قبهٔ نه چرخ حبابی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خیاط ازل دوخته از جامهٔ نه چرخ</p></div>
<div class="m2"><p>بر قامت اقبال تو کوتاه ثیابی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جستند و ندیدند حوادث پی ملجا</p></div>
<div class="m2"><p>چون درگه انصاف تو فرخنده‌مآبی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بدخواه تو گر مانده سلامت عجبی نیست</p></div>
<div class="m2"><p>اندر خور بادافره او نیست عقابی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا نیز رخ حادثه در خواب نبیند</p></div>
<div class="m2"><p>هرگز نرود دیدهٔ بخت تو به خوابی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در رجم شیاطین عدو بر فلک رزم</p></div>
<div class="m2"><p>آمد ز ازل تیر تو دلدوز شهابی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>‌تا خلق سرایند که در عرصهٔ محشر</p></div>
<div class="m2"><p>اندر خور هر معصیتی هست عذابی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از قهر تو بدخواه و ز لطف تو نکوخواه</p></div>
<div class="m2"><p>این‌یک به نصیبی رسد آن‌یک به نصابی</p></div></div>