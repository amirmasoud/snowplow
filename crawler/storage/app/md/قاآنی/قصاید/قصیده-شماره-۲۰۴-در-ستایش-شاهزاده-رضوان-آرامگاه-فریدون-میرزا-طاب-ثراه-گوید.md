---
title: >-
    قصیدهٔ شمارهٔ ۲۰۴ - ‌در ستایش شاهزاده رضوان آرامگاه فریدون میرزا طاب ثراه گوید
---
# قصیدهٔ شمارهٔ ۲۰۴ - ‌در ستایش شاهزاده رضوان آرامگاه فریدون میرزا طاب ثراه گوید

<div class="b" id="bn1"><div class="m1"><p>ای زلف نگار ای حبشی‌زادهٔ شبرنگ</p></div>
<div class="m2"><p>ای‌ اصل تو از نو به و ای نسل تو از زنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای مادر اهریمن و ای خواهر عفریت</p></div>
<div class="m2"><p>ای دایهٔ پتیاره و ای مایهٔ نیرنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ریحان مگرت بوده پدر غالیه مادر</p></div>
<div class="m2"><p>کت مانده به میراث از آن بوی و ازین رنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جادوی سیه‌کاری و جاسوس شب تار</p></div>
<div class="m2"><p>دربان رخ یاری و درمان دل تنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک حلقه پریشانی و یک سلسله شیدا</p></div>
<div class="m2"><p>یک ‌گله پرستویی و یک بادیه سارنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک مملکت آشوبی و یک معرکه غوغا</p></div>
<div class="m2"><p>یک طایفه ریحانی و یک قافله شبرنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میلاد تو در بربر و میعاد تو در روم</p></div>
<div class="m2"><p>جولان تو در خلخ و میدان تو در گنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تخمهٔ ریحانی و از دودهٔ سنبل</p></div>
<div class="m2"><p>همشیرهٔ قطرانی و نوباوهٔ ارژنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اسپهبد زنگی و ولیعهد نجاشی</p></div>
<div class="m2"><p>دارندهٔ چینی و طرازندهٔ ارتنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تاری ز تو وز نافهٔ تاتار دوصد تار</p></div>
<div class="m2"><p>بویی ز تو و سنبل خودروی دوصد تنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون دام همه پیچی و چون خام همه چین</p></div>
<div class="m2"><p>چون دیو همه ریوی و چون زاغ همه رنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با عود پسر عمی و با مشک برادر</p></div>
<div class="m2"><p>با غالیه‌همرنگی و با سلسله همسنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جادوی رسن‌سازی و هندوی رسن‌باز</p></div>
<div class="m2"><p>دیوان را سالاری و دزدان را سرهنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آویخته با ماهی و آمیخته با گل</p></div>
<div class="m2"><p>سوداگر سودانی و همسایهٔ افرنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم سرکشی ای زلف سیه هم متواضع</p></div>
<div class="m2"><p>با نخوت ‌گلچهری و با لابهٔ اورنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صوفی صفتی ساخته از کبر و تواضع</p></div>
<div class="m2"><p>باطن همه نیرنگی و ظاهر همه بیرنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر ماه سراپرده زدستی مگر از عجب</p></div>
<div class="m2"><p>خواهی‌ که چو نمرود به معبود کنی جنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حامی تو به نفرین پدرگشته سیه‌روی</p></div>
<div class="m2"><p>تا حشر نگونساری از آلایش این رنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حلق دل خلقیت به هر حلقه‌ گرفتار</p></div>
<div class="m2"><p>چون طایر پر ریخته ‌کاویخته از چنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آیینهٔ رخسار نگار از تو صفا یافت</p></div>
<div class="m2"><p>با آنکه سیه‌روی شود آینه از زنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اندام مهم نخل بلندست و تو عرجون</p></div>
<div class="m2"><p>بالای بتم تاک ستاکست و تو پاشنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زنگی بچه فرهنگ و ادب هیچ نداند</p></div>
<div class="m2"><p>چون شد که تو نهمار ادب گشتی و فرهنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صبر دل عشاق همی سنجی ازیراک</p></div>
<div class="m2"><p>چون‌ کفهٔ میزان ز دو سو بینمت آونگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بالا زده‌یی ساق چو زاهد که ز وسواس</p></div>
<div class="m2"><p>دامان ز پس و پیش بگیرد به سر چنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یا چون دو غلام حبشی ‌کز پی ‌کشتی</p></div>
<div class="m2"><p>سرپاچه بمالند و برند از دو سو آهنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از مردمک دیده اگر دوده نساید</p></div>
<div class="m2"><p>نقاش نیارد که زند نقش تو بیرنگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ما دردسر عشق تو داریم اگرچه</p></div>
<div class="m2"><p>آسوده شود دردسر خلق ز شبرنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون چنگ نکیسایی و هر موی تو از تو</p></div>
<div class="m2"><p>آویخته چون تار بریشم ز بر چنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای‌ طرفه‌ که نالان دل من در تو شب و روز</p></div>
<div class="m2"><p>چون‌ زیر و بم چنگ‌ کشد هر نفس آهنگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>میزان رخ یاری و درکفهٔ تارت</p></div>
<div class="m2"><p>صد تبت و تاتار نسنجند به جو سنگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تقویم مه رویی و آویخته مویت</p></div>
<div class="m2"><p>چون خط جداول به رصد نامهٔ جیسنگ‌</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مانا که دل و جسم منت عاریه دادند</p></div>
<div class="m2"><p>تاب و گره و عقده و پیچ و شکن و گنگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تابد رخ یار از تو چو خورشید ز روزن</p></div>
<div class="m2"><p>یا از شکن زلف شب تیره شباهنگ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یا تافته شمعی ز بر تافته فانوس</p></div>
<div class="m2"><p>یا ساخته تاجی ز یکی سوخته اورنگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یا برگ ‌گل از غالیه یا نور ز سایه</p></div>
<div class="m2"><p>یا مشتری از پنجره یا ماه ز پاچنگ</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یا طینت دینی‌ که برو حلقه زند کفر</p></div>
<div class="m2"><p>یاگوهر فخری‌که برو پرده‌کشد ننگ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مانی به غرابی‌ که بود جفت حواصل</p></div>
<div class="m2"><p>یا بچهٔ زاغی‌که به شهباز زند چنگ</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یا هندوی عریان‌ که نشیند به دو زانو</p></div>
<div class="m2"><p>از بهر ریاضت ز بر بتکدهٔ‌ گنگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یا زنگی حیران ‌که نشیند بر مهتاب</p></div>
<div class="m2"><p>یک دست به پیشانی و یک دست به آرنگ</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یا طفل سبق‌خوان ‌که بر پیر معلم</p></div>
<div class="m2"><p>گردد گه تعلیم گهی راست گهی چنگ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یا عود قماری ز بر مجمر سیمین</p></div>
<div class="m2"><p>یا مشک تتاری ز بر لالهٔ خود رنگ</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یاگرد سپاه شه ‌گیتی ‌که ‌گه ‌کین</p></div>
<div class="m2"><p>بر چهرهٔ خود پرده‌کشد تا دو سه فرسنگ</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شهزاده فریدون ملک باذل عادل</p></div>
<div class="m2"><p>کش بارخدا بر دو جهان‌ کرده‌ کنارنگ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دیوان ادب فرد کرم دفتر دانش</p></div>
<div class="m2"><p>اکسیر خرد جوهر جان عنصر فرهنگ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تعویذ زمان حرز امان جوشن ایمان</p></div>
<div class="m2"><p>اکلیل سخا تاج سخن افسر اورنگ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ای‌کز اثر عدل تو در موسم‌گرما</p></div>
<div class="m2"><p>از شهپر شهباز کند مروحه تورنگ</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آسایش ملک تو رسیدست به جایی</p></div>
<div class="m2"><p>کز بأ‌س تو در قافله افغان نکند زنگ</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>آمال ببالد چو تو بر تخت بری رخت</p></div>
<div class="m2"><p>آجال بنالد چو تو بر رخش‌ کشی تنگ</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چون‌ قلب ‌همه‌ روحی چون ‌روح‌ همه ‌عقل</p></div>
<div class="m2"><p>چون‌ عقل‌ همه‌ هوشی و چون‌ هوش همه سنگ</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>با صولت کاموسی و با دولت کاووس</p></div>
<div class="m2"><p>با شوکت جمشیدی و با حشمت هوشنگ</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گرکودک بخت توکند میل ترازو</p></div>
<div class="m2"><p>نه گنبد گردون سزدش ‌کفه ی نارنگ</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آسیمه شود چرخ چو خنگ توکند خوی</p></div>
<div class="m2"><p>دیوانه شود عقل چوکوس توکشد غنگ</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>درکاخ تو بر ابروی حاجب نبود چین</p></div>
<div class="m2"><p>در قصر تو بر حاجب دربان نفتد زنگ</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>وین طرفه ‌که ‌گر حاجب ‌کاخ تو شود پیر</p></div>
<div class="m2"><p>از چهرهٔ او جود تو بیرون برد آژنگ</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>از جوهر رای توکس ار آینه سازد</p></div>
<div class="m2"><p>آن آینه تا حشر مصفا بود از زنگ</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>با راستی عدل تو در عهد تو نقاش</p></div>
<div class="m2"><p>از بیم نیارد که‌ کشد صورت خرچنگ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>با مهر تو نسرین دمد از پنجهٔ ضیغم</p></div>
<div class="m2"><p>با عدل تو شاهین رمد از سایهٔ سارنگ</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>جود تو ز بسیاری بخشش نشودکم</p></div>
<div class="m2"><p>چون دل ‌که ز افزونی دانش نشود تنگ</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>با پنجهٔ حزم تو بود دست یقین شل</p></div>
<div class="m2"><p>با جنبش عزم تو بود پای خرد لنگ</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>با تیغ درخشان تو آتش جهد از آب</p></div>
<div class="m2"><p>با دست درافشان توگوهر دمد از سنگ</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چون تیغ به دست توبود ولوله در روم</p></div>
<div class="m2"><p>چون گرز به چنگ تو بود زلزله در زنگ</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هرجاکه سنان تو به‌کین شعله فروزد</p></div>
<div class="m2"><p>خاک از تف او سوزد تا چندین فرسنگ</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در حیّز اقبال تو امکان شده پنهان</p></div>
<div class="m2"><p>در چنبر فتراک توگردون بود آونگ</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>از هستی تو زیب برد صورت امکان</p></div>
<div class="m2"><p>بر منطق تو فخر کند دانش و فرهنگ</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نصرت نشود جز به‌ خم خام تو مفتون</p></div>
<div class="m2"><p>دشمن نزید در بر فر تو به نیرنگ</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>فتحست پدیدار به هرجا زنی اختر</p></div>
<div class="m2"><p>دولت دود از پیش به هر سو کنی آهنگ</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>از بأس تو بر جبههٔ افلاک فتد چین</p></div>
<div class="m2"><p>وز بیم تو از چهرهٔ خورشید رود رنگ</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بی‌حکم تو جریان قضا را نبود روی</p></div>
<div class="m2"><p>با قدر تو گردون‌ کهن را نبود سنگ</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>در دولت تو واصل دهرست همه فخر</p></div>
<div class="m2"><p>وزکینه تو حاصل خصمست همه ننگ</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>نوباوهٔ عمرست بنانت به‌گه بزم</p></div>
<div class="m2"><p>همشیرهٔ مرگست سنانت به صف جنگ</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نیوان وغا را شکنی برز به یک‌گرز</p></div>
<div class="m2"><p>دیوان دغا را گسلی چنگ به یک هنگ</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>رمحت خلف عوج نماید به درازی</p></div>
<div class="m2"><p>کش لجهٔ خون موج زند تا به شتالنگ</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ابر ازکف جود تو اگر حامله‌گردد</p></div>
<div class="m2"><p>سنبل شکفاند ز زمینهای زراغنگ</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>در عهد تو شهباز بود مضحکهٔ‌کبک</p></div>
<div class="m2"><p>وز عدل تو ضرغام بود مسخرهٔ زنگ</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>شاها ملکا دادگرا ملک ستانا</p></div>
<div class="m2"><p>دور از تو به جان هست مرا انده آونگ</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تن خوار و روان زار و اجل یار و امل خصم</p></div>
<div class="m2"><p>جان تفته و دل‌کفته و قد جفته و سر دنگ</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>با این همه از دور دهد چهر توام نور</p></div>
<div class="m2"><p>چون مهر که از چرخ به یاقوت دهد رنگ</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ابری تو و من خاک‌که با ‌بعد مسافت</p></div>
<div class="m2"><p>مست از تو مرا زیب و فر و زینت اورنگ</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گر قرب عیان نیست ولی قرب نهان هست</p></div>
<div class="m2"><p>با قرب نهان قرب عیان را نبود سنگ</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>دوریت ز من دوری معنی بود از لفظ</p></div>
<div class="m2"><p>کز دیدهٔ سر دوری وز دیدهٔ سِرّ تنگ</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>هجر تو ز من هجرت دانش بود از مغز</p></div>
<div class="m2"><p>هم در منی آنگه‌ که به وصلت‌ کنم آهنگ</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>جانی تو و من جسم‌که با دوری صوری</p></div>
<div class="m2"><p>هست از تو مرا هوش و حواس و هنر و سنگ</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>دورستی و نزدیک نهانستی و پیدا</p></div>
<div class="m2"><p>زانسان‌که‌به‌تن‌توش وبه‌سرهوش وبه‌دل سنگ</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>یا چون شرف عقل به‌گفتار خردمند</p></div>
<div class="m2"><p>یا چون اثر عشق در آهنگ شباهنگ</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>تا پیل و رخ و اس و شه و بیدق و فرزین‌ا</p></div>
<div class="m2"><p>دارندکشاکش همه در عرصهٔ شترنگ</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بادا به سرت چتر زگیسوی مهی شوخ</p></div>
<div class="m2"><p>بادا به‌کفت تیغ ز ابروی بتی شنگ</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>احباب تو پیوسته رهین طرب و عیش</p></div>
<div class="m2"><p>اعدای تو همواره قرین کرب و رنگ</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>سالی دو سه قاآنی اگر زنده بمانی</p></div>
<div class="m2"><p>بیغاره بمانی زنی و طعنه به ارژنگ</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ورکلک‌. تو زیغگونه همی نقش نگارد</p></div>
<div class="m2"><p>زوداکه ز خجلت بدرد پردهٔ ارژنگ</p></div></div>