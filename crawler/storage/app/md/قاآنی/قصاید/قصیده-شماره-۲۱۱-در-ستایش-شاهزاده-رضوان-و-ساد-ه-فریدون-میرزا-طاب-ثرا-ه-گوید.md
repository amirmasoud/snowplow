---
title: >-
    قصیدهٔ شمارهٔ ۲۱۱ - در ستایش شاهزادهٔ رضوان و ساد‌ه فریدون میرزا طاب ثرا‌ه گوید
---
# قصیدهٔ شمارهٔ ۲۱۱ - در ستایش شاهزادهٔ رضوان و ساد‌ه فریدون میرزا طاب ثرا‌ه گوید

<div class="b" id="bn1"><div class="m1"><p>ای رخش ره‌نورد من ای مرغ تیزبال</p></div>
<div class="m2"><p>کز دودمان برقی و از تخمهٔ خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در طبع سیر تست سبکباری نسیم</p></div>
<div class="m2"><p>در جیب نعل تست نسب‌نامهٔ شمال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه مغز که بدرّی بی‌جهد گاز و چنگ</p></div>
<div class="m2"><p>گه در هوا بپری بی‌سعی پر و بال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تاکی هوای آخور آخر برون خرام</p></div>
<div class="m2"><p>تات از پی رحیل به کوهان نهم رحال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بشتاب و مغز باد مشوّش‌ کن از مسیر</p></div>
<div class="m2"><p>بخرام و لوح خاک منقش کن از نعال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم بر فراز و مغز فلک را یکی بکوب</p></div>
<div class="m2"><p>سیم برفشان و ناف زمین را یکی بمال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز اصطبل طبل عزم فروکوب و شو برون</p></div>
<div class="m2"><p>تا ز آب دیده‌ گرد فرو شویمت ز یال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای نایب براق بپیماره عراق</p></div>
<div class="m2"><p>کایدون مرا به فارس اقامت بود محال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا چند خورد باید اندوه آب و نان</p></div>
<div class="m2"><p>تا چند برد باید تیمار عمّ و خال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لا ترجلنّ یا ملک الخیل و اعجلن</p></div>
<div class="m2"><p>کم عجله ینال بها المرء لاینال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آهنگ شهر قم ‌کن ‌گم ‌کن ز پارس پی</p></div>
<div class="m2"><p>قم قبل ان یضیق لنا الوقت و المجال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رو کن به حضرتی که ندانسته جود او</p></div>
<div class="m2"><p>در از صدف‌ گهر ز خزف‌ گوهر از سفال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کهن امان پناه زمان ‌گوهر شرف</p></div>
<div class="m2"><p>غیث‌کرم غیاث امم جوهر نوال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فهرست آفرینش و سرمایهٔ وجود</p></div>
<div class="m2"><p>عنوان حکمرانی و دیباچهٔ جلال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برهان دین و داد فریدون شه آنکه هست</p></div>
<div class="m2"><p>قسطاس فهم و فکرت مقیاس فرّ و فال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بی ‌عون مهر او نبود بخت را اثر</p></div>
<div class="m2"><p>بی‌زیب عدل او نبود مُلک را جمال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خاک از نهیب خنجر او یابد ارتعاش</p></div>
<div class="m2"><p>آب از نهیب ناچخ او دارد اشتعال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با مهر او ضلال مخلّد بود رشاد</p></div>
<div class="m2"><p>با قهر او رشاد موید بود ضلال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جز از طریق وهم نیابد کسش نظیر</p></div>
<div class="m2"><p>جز بر سبیل فرض نیابد کسش همال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای‌ کت به تحفه تاج سپارد همی تکین</p></div>
<div class="m2"><p>ای‌ کت به هدیه باج فرستد همی ینال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روزی دهد عطای تو بی ‌دعوت امید</p></div>
<div class="m2"><p>پاسخ دهد سخای تو بی‌سقبت سوال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>منشور روی و رای تو در جیب مهر و ماه</p></div>
<div class="m2"><p>توقیع امر و نهی تو در دست ماه و سال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>امر ترا به طوع قدر دارد استماع</p></div>
<div class="m2"><p>حکم ترا به طبع قضا دارد امتثال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در پیش ابر اگر ز سخایت رود سخن</p></div>
<div class="m2"><p>پیشانیش عرق‌کند از فرط انفعال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ور بر زگال تیره فتد عکس تیغ تو</p></div>
<div class="m2"><p>از تف آن چو دوزخ سوزان شود زگال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آنجا که شخص تست مجسم بود هنر</p></div>
<div class="m2"><p>آنجاکه طبع تست مصوٌر شود کمال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رسوا شود حسود تو در هرکجا که هست</p></div>
<div class="m2"><p>چون دزد شب که ناگه درگیردش سعال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با ترکتاز جود تو نشگفت اگر ز بیم</p></div>
<div class="m2"><p>پنهان‌کند پشیزهٔ خود را به بحر وال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گیهان محیط تست و به معنی محاط تو</p></div>
<div class="m2"><p>برسان جامه ‌کاو به بدن دارد اشتمال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چرخ از غبار خنگ تو تاریک چون جحیم</p></div>
<div class="m2"><p>کوه از نهیب رمح تو باریک چون خلال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از بسکه بار فتح و ظفر می‌کشد به دوش</p></div>
<div class="m2"><p>تیغت خمیده پشت نماید به شکل دال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روز وغا که از دم شمشیر سرفشان</p></div>
<div class="m2"><p>درگام اکدشان متوقٌد شود نعال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ازگرذ ره چو تودهٔ قطران شود سپهر</p></div>
<div class="m2"><p>از رنگ خون چو سودهٔ مرجان شود رمال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زانبوه‌گرد رخش محدب شود وهاد</p></div>
<div class="m2"><p>زاسیب نعل اسب مقعر شود تلال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنگال شیر شرزه نداند کس از سیوف</p></div>
<div class="m2"><p>دندان مارگرزه نداند کس از نبال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از نعل اسبها متحرک شود زمین</p></div>
<div class="m2"><p>بر چوب نیزها متوقد شود نصال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هرگه‌که میغ تیغ تو آتش‌فشان شود</p></div>
<div class="m2"><p>کس پور زال را نشناسد ز پیر زال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مغز ستاره بر دری از تیغ فتنه‌سوز</p></div>
<div class="m2"><p>کتف زمانه بشکنی از گرز مرد مال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فرماندها مها ملکا ملک‌پرورا</p></div>
<div class="m2"><p>آن‌کیست غیر حق‌که قدیمست و لایزال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ایدون گرت ز چرخ گزندی رسد مرنج</p></div>
<div class="m2"><p>ایدر گرت ز دهر ملالی رسد منال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بس عیش و عشر تا که نماند به هیچ روی</p></div>
<div class="m2"><p>بس رنج و اندها که نماند به هیچ حال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مه را به چرخ‌گاه فرازست وگه نشیب</p></div>
<div class="m2"><p>جان را به جسم گاه نشاطست و گه ملال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نی زار نالد آنگه از جان برد محن</p></div>
<div class="m2"><p>می تلخ‌ گردد آن‌ گه از دل برد کلال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خورشیدسان زوالی اگر یافتی مرنج</p></div>
<div class="m2"><p>جاوید می‌نماند خورشید را زوال</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ور کوکبت قرین وبالست غم مخور</p></div>
<div class="m2"><p>وقتی به سوی خانهٔ خویش آید از وبال</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سلطان برای مصلحتی بود اگر ترا</p></div>
<div class="m2"><p>روزی دو ساخت معتکف ‌کنج اعتزال</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زر آن زمان عزیزتر آید که ناقدی</p></div>
<div class="m2"><p>بگدازدش به بوته و بگذاردش بقال</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>فولاد راگداز دهند از برای آنک</p></div>
<div class="m2"><p>شمشیر از آن‌کنند پی دفع بدسگال</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تو تیر شست شاهی از آنت رها نمود</p></div>
<div class="m2"><p>تا خصم را دهد ز نهیب توگوشمال</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>وافکند چون شهاب ترا از سپهر ملک</p></div>
<div class="m2"><p>تا سوزد از تو دیوصفت خصم بدفعال</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>پیراست شاخ و برگ ترا چون نهال از آنک</p></div>
<div class="m2"><p>ریزد چو شاخ و برگ قوی‌تر شود نهال</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شه آفتاب مملکتست و تو ماه نو</p></div>
<div class="m2"><p>هم بدر ازو شوی اگر از وی شدی هلال</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>حکم ملک قضاست رضا ده به حکم او</p></div>
<div class="m2"><p>هم خیر ازو رسد اگر از وی رسد نکال</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شاه آنچه می‌کند همه از روی حکمتست</p></div>
<div class="m2"><p>حالی مباش رنجه‌که نیکو شود مآل</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ای بس جراحتا که برو نیشتر زنند</p></div>
<div class="m2"><p>تا خون مرده خیزد و بپذیرد اندمال</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شاگرد کاوستادش سیلی زند به روی</p></div>
<div class="m2"><p>خواهد معذبش ‌که مهذّب‌ کند خصال</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>داروی‌تلخ را نخورد خسته جز به‌عنف</p></div>
<div class="m2"><p>وان تلخ با حلاوت جان دارد اتصال</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نشتر زند پزشک به قیفال دردمند</p></div>
<div class="m2"><p>کز دفع خون مزاج ‌گراید به اعتدال</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>آید به چشم من‌که مهی بیش نگذرد</p></div>
<div class="m2"><p>کت شه به حکمرانی ملکی دهد مثال</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ای‌کز هوای مدح تو در حالتند و رقص</p></div>
<div class="m2"><p>افکار در ضمایر و ابکار در حجال</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>داند خدا که بود جدا از تو حال من</p></div>
<div class="m2"><p>چون حال تشنه‌بی که جدا ماند از زلال</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ای بس‌که قامتم از مویه همچو موی بود</p></div>
<div class="m2"><p>ای بس ‌که ‌گشت پیکرم از ناله همچو نال</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>خونم بریخت دست فراقت اگرچه نیست</p></div>
<div class="m2"><p>الا به‌ کیش تیر تو خون ریختن حلال</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>جز من‌ که بار هجر تو بردم به جان و دل</p></div>
<div class="m2"><p>کاهی شنیده‌ای‌ که ‌کند کوهی احتمال</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>منت خدای را که رسیدم به‌ کام دل</p></div>
<div class="m2"><p>زان نقمت فراق بدین نعمت وصال</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>حالی چو اخرسی ‌که اشارت‌ کند به دست</p></div>
<div class="m2"><p>با صد زبان زبان من از مدح تست لال</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ارجو که مدح من بگزینی به مدح غیر</p></div>
<div class="m2"><p>کز اشهد فصیح بهست اسهد بلال</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>سیم و زرم نبود که آرمت هدیه‌ای</p></div>
<div class="m2"><p>بپذیر جای هدیهٔ من باری این مقال</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دانی‌ که از تو بود گرم بود سیم و زر</p></div>
<div class="m2"><p>دانی ‌که از تو بود گرم بود جاه و مال</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>تا راه دل زنند نکویان به روی و موی</p></div>
<div class="m2"><p>تا صید جان‌کنند نکویان به خط و خال</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چون روی یار یار ترا تازه باد عیش</p></div>
<div class="m2"><p>چون خال دوست خصم ترا تیره ‌باد حال</p></div></div>