---
title: >-
    قصیدهٔ شمارهٔ ۲۲۴ - د‌ر ستایش مرزا ابوالقاسم قایم‌مقام رحمه‌لله فرماید
---
# قصیدهٔ شمارهٔ ۲۲۴ - د‌ر ستایش مرزا ابوالقاسم قایم‌مقام رحمه‌لله فرماید

<div class="b" id="bn1"><div class="m1"><p>شاعری امروز مر مراست مسلم</p></div>
<div class="m2"><p>از شرف مدحت اتابگ اعظم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حضرت قایم مقام صدر قدر قدر</p></div>
<div class="m2"><p>احمد عیسی خصال میر خضر دم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه به رای رزین مربی‌گردون</p></div>
<div class="m2"><p>وانکه به فکر متین مقوم عالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلق روان سیرتش روان مصور</p></div>
<div class="m2"><p>خوی بهشت آیتش بهشت مجسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساحت گیتی ز جود اوست مزین</p></div>
<div class="m2"><p>جبهت‌ گردون به داغ اوست موسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرمن خرمن شکر زگفتش پیدا</p></div>
<div class="m2"><p>دریا دریاگهر به‌کلکش مدغم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دولت ایران برای اوست مخلد</p></div>
<div class="m2"><p>ملکت سلطان به سعی اوست منظم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مجمرهٔ بزمش آفتاب منور</p></div>
<div class="m2"><p>مشربه ی ‌کاخش آسمان معظّم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رایتی از رای اوست بیضهٔ بیضا</p></div>
<div class="m2"><p>آیتی از نطق اوست چشمهٔ زمزم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تربیتش سنگ را به مایه ‌کند دُرّ</p></div>
<div class="m2"><p>تقویتش مور را به پایه‌کند جم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از می انعام اوست روی امل سرخ</p></div>
<div class="m2"><p>از پی اکرام اوست پشت فلک خم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>علت غائی بود وجود جهان را</p></div>
<div class="m2"><p>گرچه مؤخّر ولی به رتبه مقدّم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طبع‌کریمش به جود و جاه مخمّر</p></div>
<div class="m2"><p>ذات سلیمش به روی و رای مسلم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازکرمش آفتاب و‌کُرتهٔ زرین</p></div>
<div class="m2"><p>از سخطش آسمان وکسوت ماتم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دوزخ با مهر اوست روضهٔ رضوان</p></div>
<div class="m2"><p>جنت با قهر اوست قعر جهنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با رخ او گل به رنگ تیره‌تر از گِل</p></div>
<div class="m2"><p>با کف اویم به سنگ طعنه‌بر از یم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای به‌گهر مهترین نتیجهٔ حوّا</p></div>
<div class="m2"><p>وی به شرف اولین سلالهٔ آدم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اینت اشارت زکردگار پیاپی</p></div>
<div class="m2"><p>اینت بشارت ز کردگار دمادم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کز تو یک اقدام و صد دیار مسخر</p></div>
<div class="m2"><p>وز تو یک اقبال و صد اساس فراهم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شیر فلک امتثال امر ترا هست</p></div>
<div class="m2"><p>روز و شب آماده‌تر زکلب معلم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چرخ به چنگال قدرتت به چه ماند</p></div>
<div class="m2"><p>روبهکی خسته در مخالب ضیغم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنبر آفاق را جلال تو مرکز</p></div>
<div class="m2"><p>قسمت ارزاق را نوال تو مقسم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ساعد مجد تراست‌گیهان یاره</p></div>
<div class="m2"><p>رایت رای تراست گردون پرچم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خصم تشبّه ‌کند به شخص تو لیکن</p></div>
<div class="m2"><p>سفله نگرددکیا به‌کسوت ملحم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پیر نگردد جوان به غازه و زیور</p></div>
<div class="m2"><p>زشت نگردد نکو به یاره و خاتم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>طینت احمد کجا و فکرت بوجهل</p></div>
<div class="m2"><p>دعوت عیسی ‌کجا و دعوی بلعم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>باقل‌ هرگز بهش نگردد حسان</p></div>
<div class="m2"><p>مادر هرگز به ‌بر نگردد حاتم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کوه دماوندکی چو حزم تو متقن</p></div>
<div class="m2"><p>پشهٔ الوند کی چو حکم تو محکم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تاج سخا را کنوز کلک تو گوهر</p></div>
<div class="m2"><p>بام سخن را رموز فکر تو سلّم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>صدرا کس جز تو قدر من نشناسد</p></div>
<div class="m2"><p>رومی داند بهای دیبهٔ معلم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رای تو میزان دانشست ولیکن</p></div>
<div class="m2"><p>کوه بر سنگ او ز کاه بود کم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شکر خدا را که هستم از کرم تو</p></div>
<div class="m2"><p>صاحب قدر منیع و صدر مکرم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>منت بیمر خدای راکه ز جودت</p></div>
<div class="m2"><p>خاطر درهم ندارم از پی دِرهم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کیسه پر آموده‌ام ز لؤلؤ لالا</p></div>
<div class="m2"><p>کاسه بپیموده‌ام ز بادهٔ درغم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گه ز بت ساده خانه سازم بستان</p></div>
<div class="m2"><p>گه ز بط باده خاطر آرم خرّم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چیست بط باده شعر بیغش شیوا</p></div>
<div class="m2"><p>کیست بت ساده یار مونس همدم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هیچ کسم نیست جز ولای تو مونس</p></div>
<div class="m2"><p>هیچکسم نیست جز ثنای تو همدم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حضرت دستور نیز ازکرم عام</p></div>
<div class="m2"><p>در حق چاکرکند متابعت عم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مجلسش آموده از سران معزز</p></div>
<div class="m2"><p>محفلش آکنده از مهان مخفم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>صف به صف استاده پیر و کودک و برنا</p></div>
<div class="m2"><p>کش بکش آماده ترک و تازی و دیلم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نیست‌برش نام‌ من چو وصف‌تو مجهول</p></div>
<div class="m2"><p>نیست برش قدر من چو نعت تو مبهم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آری در وصف تست عاقله جاهل</p></div>
<div class="m2"><p>آری در نعت تست ناطقه ابکم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بالله ازین به ‌کسی سخن نسراید</p></div>
<div class="m2"><p>جز که شود خاطرش به معجزه ملهم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خاصه‌که از فرّ آفتاب قبولت</p></div>
<div class="m2"><p>گشته کنون آسمان‌گرای چو شبنم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا به جهان نام از جلالت سهراب</p></div>
<div class="m2"><p>تا به زبان یاد از شجاعت رستم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>رایض امر ترا به ساحت‌گیتی</p></div>
<div class="m2"><p>تا ابد از صح و شام اشهب وادهم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>عزم تو چون خنگ چرخ سایر و ساری</p></div>
<div class="m2"><p>حزم تو چون‌کوی خاک ثابت و مبرم</p></div></div>