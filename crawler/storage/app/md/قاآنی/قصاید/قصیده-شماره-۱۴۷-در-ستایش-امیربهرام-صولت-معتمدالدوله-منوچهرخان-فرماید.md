---
title: >-
    قصیدهٔ شمارهٔ ۱۴۷ - در ستایش امیربهرام صولت معتمدالدوله منوچهرخان فرماید
---
# قصیدهٔ شمارهٔ ۱۴۷ - در ستایش امیربهرام صولت معتمدالدوله منوچهرخان فرماید

<div class="b" id="bn1"><div class="m1"><p>با فال نیک و حال خوش و بخت‌کامگار</p></div>
<div class="m2"><p>از ملک جم به عزم سپاهان شدم سوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در زیر ران من فرسی ‌کافریده بود</p></div>
<div class="m2"><p>اوهام را ز پویهٔ او آفریدگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شخ ‌برّ و که‌نورد و جهانگرد و گرم‌سیر</p></div>
<div class="m2"><p>کم‌خسب و پرتوان و زمین‌کوب و رهسپار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کز پی نگارم آمد و تنگم عنان گرفت</p></div>
<div class="m2"><p>با چشم اشکبار و دوگیسوی مشکبار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در زیر مه فراشته از سیم ساده سرو</p></div>
<div class="m2"><p>بر برگ ‌گل‌ گذاشته از مشک سوده تار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مویی به بوی سنبل و رویی به رنگ‌ گل</p></div>
<div class="m2"><p>قدّی به لطف طوبی و خدّی به نور نار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گیسوی تابدارش همسایه ی بهشت</p></div>
<div class="m2"><p>زلفین عنبرینش پیرایهٔ بهار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لعلش پر آب بی‌مدد نور آفتاب</p></div>
<div class="m2"><p>چشمش به خواب بی‌اثر برگ کو کنار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر سرو ماه هشته و بر ماه غالیه</p></div>
<div class="m2"><p>بر رخ ستاره بسته و بر پشت‌کوهسار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر زهرهٔ رخش مه و خورشید مشتری</p></div>
<div class="m2"><p>از حسرت خطش شبه و مشک سوگوار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در روی و موی او چو اسیران روم و زنگ</p></div>
<div class="m2"><p>دلهای داغ دیده قطار از پی قطار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گیسو گشود و مغزم از آن‌ گشت عنبرین</p></div>
<div class="m2"><p>عارض نمود و چشم از آن گشت لاله‌زار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنگی زدم به زلفش و از تار تار او</p></div>
<div class="m2"><p>چون‌ تار چنگ خاست بسی نالهای زار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وز هر مکنج او که گشودم به خاک ریخت</p></div>
<div class="m2"><p>چندین هزار سلسله دلهای بیقرار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وانگشتهای من چو زره‌گشت پرگره</p></div>
<div class="m2"><p>از پیچ و تاب و حلقهٔ زلفین آن نگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>القصه نارسیده لب شکوه باز کرد</p></div>
<div class="m2"><p>وآن طبله طبله مشک پریشید بر عذار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت ای نکرده یاد ز یاران و دوستان</p></div>
<div class="m2"><p>این بود حق صحبت یاران حق‌گزار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باری چه روی داد ندانم‌که بی‌سبب</p></div>
<div class="m2"><p>مسکین دلم شکستی و بستی ز شهریار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این‌گفت و از تگرگ بپوشید لاله برگ</p></div>
<div class="m2"><p>وز نرگسش چکید به‌ گل دانهای نار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیجاده راگزید به الماس شکرین</p></div>
<div class="m2"><p>یاقوت را مزید به لولوی شاهوار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از ده هلال مرّیخ انگیخت از قمر</p></div>
<div class="m2"><p>وز خون دیده بست ده انگشت را نگار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از جزع بست دجلهٔ سیماب بر سمن</p></div>
<div class="m2"><p>وز اشک ریخت سودهٔ الماس در کنار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفتم بتا مموی و پریشان مساز موی</p></div>
<div class="m2"><p>کز مویه ترسمت‌که چو مویی شوی نزار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اشک‌تو انجمست و رخت مهر وکس ندید</p></div>
<div class="m2"><p>کانجا که هست مهر شود انجم آشکار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دیدم بسی ‌که خیزد از جویبار سرو</p></div>
<div class="m2"><p>نشنیده‌ام‌که خیزد از سرو جویبار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پروین بروز می‌ننماید ترا چه شد</p></div>
<div class="m2"><p>کایدون بروز خوشهٔ پروین‌کنی نثار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جراره از چه پوشی بر ماه نوربخش</p></div>
<div class="m2"><p>سیاره از چه پاشی بر مهر نور بار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باری قسم به جوشن داود و مهر جم</p></div>
<div class="m2"><p>یعنی به زلفکان تو وان لعل آبدار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کز هرچه در جهان‌گذرم در هوای تو</p></div>
<div class="m2"><p>الا ز خاکبوسی صدر بزرگوار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سالار دهر معتمدالدوله آنکه هست</p></div>
<div class="m2"><p>دیباچهٔ جلالت و عنوان اقتدار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صدری ‌که بر یسار وی افلاک را یمین</p></div>
<div class="m2"><p>بدری‌که از یمین وی آفاق را یسار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر چیز در زمانه به هستیت مفتخر</p></div>
<div class="m2"><p>جز ذات وی ‌که هستی از آن دارد افتخار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر خاک شوره تابد اگر نور روی او</p></div>
<div class="m2"><p>خور جای خار روید از خاک شوره‌زار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یک ناامید در همه‌گیتی ندیده چرخ</p></div>
<div class="m2"><p>کاو را نکرده فضل عمیمش امیدوار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دوران به دور دولت او جوید اختتام</p></div>
<div class="m2"><p>گیهان ز فر شوکت او خواهد اعتبار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گیتی به عدل شامل اوگشته معتصم</p></div>
<div class="m2"><p>هستی به ذات‌ کامل او جسته انحصار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ای چون سپهر قصر جلال تو بی‌قصور</p></div>
<div class="m2"><p>وی چون وجود لجهٔ جود تو بی‌کنار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تن را هوای مهر تو چون عمر سودمند</p></div>
<div class="m2"><p>جان راسموم قهر تو چون مرگ ناگوار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون ذات عقل پایهٔ جاهت بر از جهت</p></div>
<div class="m2"><p>چون فیض روح مایهٔ جودت بر از شمار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بذل تو بی‌قیاس چو ادوار آسمان</p></div>
<div class="m2"><p>فضل تو بی‌حساب چو اطوار روزگار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در پیش خصم تیغ تو سدیست آهنین</p></div>
<div class="m2"><p>برگرد ملک حزم تو حصنیست استوار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>عدلت به‌ کتف ماه ز کتان نهد رسن</p></div>
<div class="m2"><p>حزمت به گرد آب ز آتش کشد حصار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ناگفته دانی آرزوی طفل در رحم</p></div>
<div class="m2"><p>نادیده یابی آبخور وحش در قفار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از خاک‌گاه جود تو زرین دمد شجر</p></div>
<div class="m2"><p>وز آب روز مهر تو مشکین جهد بخار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خصم ترا به دهر محالست برتری</p></div>
<div class="m2"><p>جز آنکه خاک‌ گردد و خاکش شود غبار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ای بر زمین طاعت تو چرخ را سجود</p></div>
<div class="m2"><p>وی در نگین خاتم تو ملک را مدار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>وقتی بران شدم‌ که به دیوان رقم ‌کنم</p></div>
<div class="m2"><p>ز اوصاف تیغ‌جان‌شکرت بیتکی سه‌چار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ننوشته نام تیغ توکز نوک‌کلک من</p></div>
<div class="m2"><p>جست آتشی‌ که تا به فلک رفت ازان شرار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هی سوخت دفتر من از اوصاف او و من</p></div>
<div class="m2"><p>هی آب می‌زدم به وی از شعر آبدار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زاندام اهل زنگ سیاهی برون رود</p></div>
<div class="m2"><p>گر آفتاب تیغ تو تابد به زنگبار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>روزی نسیم خلق تو بر مغز من وزید</p></div>
<div class="m2"><p>پر شد کنار و دامنم از نافهٔ تتار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چون نام همت تو برم از زبان من</p></div>
<div class="m2"><p>در خوشه خوشه ریزد و دینار بار بار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چون وصف مجلس تو کنم خیزد از لبم</p></div>
<div class="m2"><p>آواز چنگ و نغمهٔ نای و نوای تار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کوهیست همتت ‌که چو بحرست موج‌خیز</p></div>
<div class="m2"><p>بحریست رحمتت‌که چوکوهیست پایدار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>یا حبذا ز تیغ تو آن پاسبان بخت</p></div>
<div class="m2"><p>کز وی اساس دولت و دینست استوار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گاهش چو عقل در سرگردنکشان مقر</p></div>
<div class="m2"><p>گاهش چو روح در تن کند او زان قرار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نبود شگفت اگر ملک‌الموت خوانمش</p></div>
<div class="m2"><p>از بسکه‌ هست‌ چون‌ ملک‌الموت جان‌شکار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>جز مور جوهرش که به ‌کین اژدها کش است</p></div>
<div class="m2"><p>نادیده در زمانه کسی مور مارخوار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ویحک ز چارباغ سپاهان‌ که سعی تو</p></div>
<div class="m2"><p>کردش چنان ‌که آیدش از هشت خلد عار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>داغ جنان و باغ جنانست ساحتش</p></div>
<div class="m2"><p>ز ازهار گونه ‌گونه وز اشجار پر ثمار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>باغ زرشک تا تو درویی ز رشک خلد</p></div>
<div class="m2"><p>روی از سرشک خونین دارد ز رشک‌وار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>خون ‌گردد از زرشک مصفا و خون چرخ</p></div>
<div class="m2"><p>در دل ز داغ باغ زرشک تو گشت تار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>صدرا خدایگانا ده سال بی‌توام</p></div>
<div class="m2"><p>جان بود دردمند و جگرخون و دل‌فکار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>منت خدای را که بدیدم به ‌کام دل</p></div>
<div class="m2"><p>بازت به صدر قدر ظفرمند و بختیار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تا خاص و عام‌ گاه بلندند و گاه پست</p></div>
<div class="m2"><p>تا شیخ و شاب ‌گاه عزیزند و گاه خوار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>از چهر نیکخواه تو بادا شکفته‌ گل</p></div>
<div class="m2"><p>در چشم بدسگال تو بادا خلیده خار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>تا چار ربع شانزدهست و سه ثلث نه</p></div>
<div class="m2"><p>تا هفت نصف چاردهست و دو جذر چار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>هر کاو که هفت و هشت‌ کند با تو در جهان</p></div>
<div class="m2"><p>باکید نه سپهر سه روحش بود دو چار</p></div></div>