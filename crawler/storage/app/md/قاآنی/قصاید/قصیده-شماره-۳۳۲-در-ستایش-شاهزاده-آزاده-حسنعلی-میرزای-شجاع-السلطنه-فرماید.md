---
title: >-
    قصیدهٔ شمارهٔ ۳۳۲ - در ستایش شاهزاده آزاده حسنعلی میرزای شجاع السلطنه فرماید
---
# قصیدهٔ شمارهٔ ۳۳۲ - در ستایش شاهزاده آزاده حسنعلی میرزای شجاع السلطنه فرماید

<div class="b" id="bn1"><div class="m1"><p>دوش درآمد از درم آن مه برج دلبری</p></div>
<div class="m2"><p>سود بر آسمان سرم از در ذرّه‌پروری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دو کمند گیسوان وز دو کمان ابروان</p></div>
<div class="m2"><p>بسته‌ دو دست جاودان داده به چرخ چنبری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به دو زلفکان او شاه طغان نظر کند</p></div>
<div class="m2"><p>همچو‌ کبوتران زند بر در او کبوتری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سینهٔ صاف چون سمن، عارض تر چو یاسمن</p></div>
<div class="m2"><p>مقصد شیخ و برهمن رشک بتان آزری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماه فلک ز روی او خاک‌نشین ‌کوی او</p></div>
<div class="m2"><p>سنگ سیه ز موی او جسته رواج عنبری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیرت سو و یاسمن آفت جان مرد و زن</p></div>
<div class="m2"><p>غارت عقل و هوش من حسرت ماه و مشتری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت ‌که ای اسیر تب خسته ی محنت و کرب</p></div>
<div class="m2"><p>چند به پویهٔ تعب پایهٔ مرگ بسپری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکوه بر از غم زمان پیش سکندر جهان</p></div>
<div class="m2"><p>تا نخوری ز بیم جان هر قدمی سکندری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاه جهان حسنعلی فارس عرصه ی یلی</p></div>
<div class="m2"><p>غازی دشت پر دلی مهر سپهر سروری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنکه به‌ گاه حشمتش شمس نموده شمه‌ای</p></div>
<div class="m2"><p>وآنگه به بزم عشرتش‌کرده هلال ساغری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وآنکه چو پور آتبین کرده زگرز گاوسر</p></div>
<div class="m2"><p>مغز سر ده‌آک‌ را طعمهٔ مار حمیری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آهوی چرخ رام او شیر فلک به دام او</p></div>
<div class="m2"><p>ملک فلک به‌کام او بر ملکش بهادری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آتش زارتشت اگر ، قبله ی خاص و عام شد</p></div>
<div class="m2"><p>خاک سرای شاه بین معبد آدم و پری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رومی روز در برش همچو غلام خلخی</p></div>
<div class="m2"><p>زنگی شام بر درش همچو سیاه بربری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بود اگر به طوس در اژدر اهرمن شکر</p></div>
<div class="m2"><p>تا به حسام سام یل زود نمودش اسپری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاه‌به‌طو‌س اندرون‌ بست و درید و ریخت خون</p></div>
<div class="m2"><p>هر که ز طالع زبون کرد ز کینه اژدری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رستم یل ز خستگی تافت ز روی تن عنان</p></div>
<div class="m2"><p>بر لب رود هیرمند با همهٔ دلاوری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت‌ که نیست ‌کارگر تیر و سنانش بر بدن</p></div>
<div class="m2"><p>زانکه نموده بر تنش زار دهشت ساحری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هان به‌ کجاست روی تن تا ز خدنگ پادشه</p></div>
<div class="m2"><p>کالبدش زره شود با همه روی پیکری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای شه آسمان حثبم‌ کارگشای ملک جم</p></div>
<div class="m2"><p>داور کشور عجم وارث تاج نوذری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چرخ به پیش موکبت غاشیه برکتف‌کشد</p></div>
<div class="m2"><p>ماه نوت شود عنان چرخ‌ کند تکاوری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خصم تو مار جانگزا تیر تو آتشین قبا</p></div>
<div class="m2"><p>شن تو هوشهنگ‌سا جن چرا نگستری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تات چو مرکز آسمان جا به‌کنار خود دهد</p></div>
<div class="m2"><p>زاوٌل شکل خویشتن خواست به هیأت ‌کری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نی غلطم ‌که آسمان پیش تو هست نقطه‌سان</p></div>
<div class="m2"><p>وز پی صولجان تو کرده چو گو مدوری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پادشهی ترا سزد ورنه بغیر لاغ نه</p></div>
<div class="m2"><p>کوکبهٔ ملکشهی حشمت و جاه سنجری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دست ‌کریمت از کرم غیرت ابر بهمنی</p></div>
<div class="m2"><p>طبع همیمت از همم رشک سحاب آذری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مهرهٔ بخت درکفت داو به روی داوکش</p></div>
<div class="m2"><p>تا ببری به دس خون‌ داو فلک به شثدری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رونق دین جعفری‌ گرچه به تیغ داده‌ای</p></div>
<div class="m2"><p>لیک ز بذل برده‌یی رونق جود جعفری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مهر ز شر‌م رای تو از عرق جبین شود</p></div>
<div class="m2"><p>غرقه به بحر چارمین گر نکند شناوری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خصم تو گر درین زمان لاف اناللهی زند</p></div>
<div class="m2"><p>جملهٔ خلق آگهند از حرکات سامری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پادشها حبیب تو چون ز ثنات دم زند</p></div>
<div class="m2"><p>نیست عجب ‌گر از سخن فخر کند بر انوری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>لیک به جانش ز آسمان هر نفسی غمی رسد</p></div>
<div class="m2"><p>چون شد ار ز مرحمت غم ز روانش بستری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جنس هنر کجا برد پیش توگر نیاورد</p></div>
<div class="m2"><p>دانی کاندرین بلد تنگ شدست شاعری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا که نجات هر تنی هست ز دین احمدی</p></div>
<div class="m2"><p>تا که صفای هر دلی هست ز مهر حیدری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>باد مخالف ترا غی و ضلال بولهب</p></div>
<div class="m2"><p>باد موالف ترا جاه و مقام بوذری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چهرهٔ دوستان تو گونهٔ دشمنان تو</p></div>
<div class="m2"><p>این ز فرح معصفری وآن ز الم مزعفری</p></div></div>