---
title: >-
    قصیدهٔ شمارهٔ ۲۹۷
---
# قصیدهٔ شمارهٔ ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>خوش بود خاصه فصل فروردین</p></div>
<div class="m2"><p>بادهٔ تلخ و بوسهٔ شیرین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوسهٔ‌ گرم کز حلاوت آن</p></div>
<div class="m2"><p>یک طبق انگبین چکد به زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بادهٔ تلخ‌کز حرارت او</p></div>
<div class="m2"><p>مور گیرد مزاج شیر عرین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو گویی ‌کدام ازین دو بهست</p></div>
<div class="m2"><p>گویمت هر دو به همان و همین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن یک از دست‌ گلرخی زیبا</p></div>
<div class="m2"><p>وین یک از لعل شاهدی نوشین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاصه چون ترک پاکدامن من</p></div>
<div class="m2"><p>مهوشی دلکشی درست آیین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیم خد سرو قد فرشته همال</p></div>
<div class="m2"><p>مشک مو ماهرو ستاره جبین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدل سرمه در دو چشمش ناز</p></div>
<div class="m2"><p>عوض شانه در دو زلفش چین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باد در زلفکانش حلقه شمار</p></div>
<div class="m2"><p>ناز در چشمکانش‌ گوشه‌نشین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سنبلش را ز ارغوان بستر</p></div>
<div class="m2"><p>سوسنش را ز ضیمران بالین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسته بر مژه چنگل شهباز</p></div>
<div class="m2"><p>هشته در طره پنجهٔ شاهین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رشته‌یی را لقب نهاده میان</p></div>
<div class="m2"><p>پشته‌یی را صفت نهاده سرین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>علم جرالثقیل داند از آنک</p></div>
<div class="m2"><p>بسته کوهی چنان به موی چنین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ساق او ماهی سقنقورست</p></div>
<div class="m2"><p>که تقاضا کند بدو عنین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از جبینش اگر سوال ‌کنی</p></div>
<div class="m2"><p>علم الله یک طبق نسرین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صبح هنگام آنکه باد سحر</p></div>
<div class="m2"><p>غم زداید ز سین‌های حزین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ترکم از ره رسید خنداخند</p></div>
<div class="m2"><p>با تنی پای تا به سر تمکین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت چونستی السلام علیک</p></div>
<div class="m2"><p>ای ترا عون‌ کردگار معین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جستم ‌از جای و گفتمش به ‌جواب</p></div>
<div class="m2"><p>و علیک‌السلام فخرالدین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفت قاآنیا به‌ گیسوی من</p></div>
<div class="m2"><p>شعر بافی مکن بهل تضمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باده پیش آر از آنکه درگذرد</p></div>
<div class="m2"><p>عیش نوروز و جشن فروردین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکی از حجره سوی باغ بچم</p></div>
<div class="m2"><p>یکی از غرقه سوی راغ ببین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عوض سبزه بر چمن ‌گویی</p></div>
<div class="m2"><p>زلف و گیسو گشاده حورالعین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زان میم ده‌ که‌ کور اگر نوشد</p></div>
<div class="m2"><p>بیند از ری حصار قسطنطین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باده‌ای ‌کز نسیم او تا حشر</p></div>
<div class="m2"><p>کوه و صحرا شود عبیر آگین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ور به آبستنی بنوشانی</p></div>
<div class="m2"><p>می برقصد به بچه دانش جنین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قصه ‌کوتاه از آن میش دادم</p></div>
<div class="m2"><p>که برد روح را به علیّین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خورد چندانکه پیکرش ز نشاط</p></div>
<div class="m2"><p>متمایل شد از یسار و یمین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نازهایی ‌که شرم پنهان داشت</p></div>
<div class="m2"><p>جنبشی‌ کرد کم کمک ز کمین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ناگه از جای جست و بیرون ریخت</p></div>
<div class="m2"><p>از کله زلف و کاکل مشکین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وان‌ گران‌ کوه را که می‌دانی</p></div>
<div class="m2"><p>گاه بالا فکند و گه پایین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>متفاوت نمود گردش او</p></div>
<div class="m2"><p>چون در آفاق سیر چرخ برین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آسیاوار گه نمودی سیر</p></div>
<div class="m2"><p>چون فلک در اراضی تسعین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفتئی‌گردشش چوگردش چرخ</p></div>
<div class="m2"><p>نگسلد تا به روز بازپسین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>من به نظاره تا سرینش را</p></div>
<div class="m2"><p>به قیاس نظر کنم تخمین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عقل آهسته گفت در گوشم</p></div>
<div class="m2"><p>نقب بیجا مبر به حصن حصین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گفتم ای ترک رقص تاکی و چند</p></div>
<div class="m2"><p>بوسه‌یی باگلاب و قند عجین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بوسه‌یی ده ‌که از دهان به ‌گلو</p></div>
<div class="m2"><p>عذب و آسان رود چو ماء معین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بوسه‌یی ده‌ که شهد ازو بچکد</p></div>
<div class="m2"><p>کام را چون شکر کند شیرین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به شکرخنده‌ گفت قاآنی</p></div>
<div class="m2"><p>در بهار این‌قدر مکن تسخین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گفتم ای ترک وقت طیبت نیست</p></div>
<div class="m2"><p>با کم و کیف بوسه ‌کن تعیین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چند بوسم دهی بفرما هان</p></div>
<div class="m2"><p>بچه نسبت دهی بیاور هین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>رخ ترش ‌کرد کاین دلیری تو</p></div>
<div class="m2"><p>هان و هان از کجاست ای مسکین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گفتمش زانکه مادح ملکم</p></div>
<div class="m2"><p>روز و شب سال و ماه صبح و پسین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>غبغب خویش راگرفت به مشت</p></div>
<div class="m2"><p>شرمگین‌ گفت‌ کای خجسته قرین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به زنخدان من بخور سوگند</p></div>
<div class="m2"><p>که نگویی به ترک من پس ازین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا ز بهر دوام دولت شاه</p></div>
<div class="m2"><p>تو نمایی دعا و من آمین</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شاه‌گیتی ستان محمدشاه</p></div>
<div class="m2"><p>که جهانش بود به زیر نگین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خصم او همچو تیغ اوست نزار</p></div>
<div class="m2"><p>گرز او همچو بخت اوست سیمین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>عدل او عرق ظلم را نشتر</p></div>
<div class="m2"><p>خشم او چشم خصم را زوبین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>عهد او چون اساس شرع قویم</p></div>
<div class="m2"><p>عدل او چون قیاس عقل متین</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سایهٔ دستش ار به‌کوه افتد</p></div>
<div class="m2"><p>سنگ‌گیرد بهای در ثمین</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نفخهٔ خلقش ار به دشت وزد</p></div>
<div class="m2"><p>خاک یابد نسیم نافهٔ چین</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>رایت قدر او چو چرخ بلند</p></div>
<div class="m2"><p>آیت جاه او چو مهر مبین</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>عقل در گوش او گشاید راز</p></div>
<div class="m2"><p>که ازو خوبتر ندید امین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>جان به بازوی او خورد سوگند</p></div>
<div class="m2"><p>که ازین سخت‌تر نیافت یمین</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ناصر ملتست و کاسر کفر</p></div>
<div class="m2"><p>ماحی بدعتست و حامی دین</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>فتح در ره ستاده دست بکش</p></div>
<div class="m2"><p>تا که او بر جهد به خانهٔ زین</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مرگ در ره نشسته گوش‌به‌حکم</p></div>
<div class="m2"><p>تا کی او در شود به عرصهٔ‌ کین</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زهره جو دهره‌اش ز قلب قباد</p></div>
<div class="m2"><p>تشنه ‌لب دشنه‌اش به ‌کین تکین</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>شعله‌یی ‌کز حسام او خیزد</p></div>
<div class="m2"><p>ندهد آب قلزمش تسکین</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>شبهتی ‌کز خلاف او زاید</p></div>
<div class="m2"><p>نکند عقل ‌کاملش تبیین</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>علم در عهد او بود رایج</p></div>
<div class="m2"><p>چون شب جمعه سورهٔ یاسین</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>خبر عسدل او چنان مشهور</p></div>
<div class="m2"><p>که در آفاق غزوهٔ صفین</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>خسروا ای‌که بر مخالف تو</p></div>
<div class="m2"><p>وحش و طیر جهان‌ کند نفرین</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بشکفد خاطر از عنایت تو</p></div>
<div class="m2"><p>چون ضمیر سخنور از تحسین</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بسفرد پیکر از مهابت تو</p></div>
<div class="m2"><p>چون روان منافق از تهجین</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>باره‌یی چون حصار دولت تو</p></div>
<div class="m2"><p>در دو گیتی نیافتند رزین</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بقعه‌یی چون بنای شوکت تو</p></div>
<div class="m2"><p>در دو گیهان نساختند متین</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>رخنه افتد به ‌کوه از سخطت</p></div>
<div class="m2"><p>چون ز نوک قلم به مدّهٔ سین</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بشکفد تا شکوفه در نیسان</p></div>
<div class="m2"><p>بفسرد تا بنفشه در تشرین</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>باد مقصور مدت تو شهور</p></div>
<div class="m2"><p>باد محصور دولت تو سنین</p></div></div>