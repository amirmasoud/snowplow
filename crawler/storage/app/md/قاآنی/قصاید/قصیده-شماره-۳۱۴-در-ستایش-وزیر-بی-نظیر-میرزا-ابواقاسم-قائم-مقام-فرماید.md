---
title: >-
    قصیدهٔ شمارهٔ ۳۱۴ - در ستایش وزیر بی نظیر میرزا ابواقاسم قائم مقام فرماید
---
# قصیدهٔ شمارهٔ ۳۱۴ - در ستایش وزیر بی نظیر میرزا ابواقاسم قائم مقام فرماید

<div class="b" id="bn1"><div class="m1"><p>مگوگناه بود بر رخ نگار نگاه</p></div>
<div class="m2"><p>که بر شمایل غلمان نگاه نیست‌گناه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرشک ریز دم از دیده هر زمان ‌که ‌کنم</p></div>
<div class="m2"><p>در آفتاب جمال تو خیره خیره نگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخت زداید گرد رخم چو آب روان</p></div>
<div class="m2"><p>خطت فزاید مهر دلم چو مهر گیاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو چهرهٔ تو بود چهر من ز اشک سفید</p></div>
<div class="m2"><p>چو طرهٔ تو بود روز من ز آه سیاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز عشق روی منیر تو روز من تاریک</p></div>
<div class="m2"><p>ز فکر زلف دراز تو عمر من‌کوتاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا شکنج به‌گیسو مرا شکنجه به جان</p></div>
<div class="m2"><p>مراکلال به خاطر تراکلاله به ماه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تراست چشم‌کحیل و مراست جسم علیل</p></div>
<div class="m2"><p>تراست خال سیاه و مراست حال تباه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر نه چشم تو افراسیاب تُرک چرا</p></div>
<div class="m2"><p>به ‌گردش از مژه صف بسته از دو روی سپاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شدست حاجب سلطان چهره ابرویت</p></div>
<div class="m2"><p>که بی اشارهٔ این کس بدو نجویند راه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا ز هجر تو جیحون‌شدست‌دیده ز اشک</p></div>
<div class="m2"><p>مرا ز عشق تو کانون شدست سینه ز آه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز تیر ‌زلف دلم را مخوان به سوی زنخ</p></div>
<div class="m2"><p>مباد آنکه درافتد شبان تیره به چاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>‌و یا نقاب درافکن ز چهره تا بیند</p></div>
<div class="m2"><p>شبان تیره به ره چاه را ز تابش ماه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گشاده رویت ای مه به تاب می‌ماند</p></div>
<div class="m2"><p>به دشت همت دستور آسمان درگاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سپهر فضل و هنر میرزا ابوالقاسم</p></div>
<div class="m2"><p>که فضل او زده بر اوج آسمان خرگاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خدایگان وزیران‌که خور ز رشگ رخش</p></div>
<div class="m2"><p>به چرخ مات شود چون ز فر فرزین شاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دلیل دعوی یکتاییش بس اینکه سپهر</p></div>
<div class="m2"><p>کند ز بحر سجودش هماره ‌پشت دوتاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به دعوت نعمش هرکه در زمانه مزیل</p></div>
<div class="m2"><p>به دعوی‌کرمش هرچه در جهان آگاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به جود دست و دلش فقر کان و بحر دلیل</p></div>
<div class="m2"><p>به نور رای و رخش‌ خسف ماه و مهر گواه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زهی‌ گذشته ترا از کمال عز و شرف</p></div>
<div class="m2"><p>ز جبهه نور جبین وز طرفه طرف ‌کلاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به جنب جاه تو هیچست آسمان بلند</p></div>
<div class="m2"><p>ولی عجب نه گر او مر ترا فزاید جاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنانکه صفر بود هیچ بر سبیل مثل</p></div>
<div class="m2"><p>چو پیش پنج نهی پنج ازو شود پنجاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که مثل تست‌که تاگویمت بر از امثال</p></div>
<div class="m2"><p>که شبه تست‌که تا دانمت به از اشباه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز دیده بسکه ببارند حاسدان تو خون</p></div>
<div class="m2"><p>ز سینه بسکه برآرند دشمنان تو آه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شفاهشان شده از دود آن به رنگ جفون</p></div>
<div class="m2"><p>جفونشان شده از رنگ این به لون شفاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو شهد عهد تو در کام دوستان شیرین</p></div>
<div class="m2"><p>چو زهر قهر تو در جان دشمنان جانکاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز حسرت‌ دل ‌و دست‌ تو بحر و کان شب و روز</p></div>
<div class="m2"><p>به مهر و ماه رسانند بانگ و اغوثاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روان به مهر تو پیوند جسته با اجسام</p></div>
<div class="m2"><p>زبان به مدح تو میثاق بسته با افواه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پی نظارهٔ تو خلق‌کرده‌اند عیون</p></div>
<div class="m2"><p>ز بهر سجدهٔ تو آفریده‌اند جباه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قلم به دست تو هنگام جود در جنبش</p></div>
<div class="m2"><p>بدان مثابه‌که ماهی‌کند به بحر شناه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر به چشم تعنت‌ کنی به‌ کوه نظر</p></div>
<div class="m2"><p>اگر به عین عنایت‌کنی به‌کاه نگاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شود ز خشم‌ تو چون جسم بدسگال تو کوه</p></div>
<div class="m2"><p>شود ز مهر تو چون بخت نیکخواه تو کاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بزرگوارا هستم من از تو سخت دژم</p></div>
<div class="m2"><p>ولی چه سود که قادر نیم به باد افراه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه بحر و کانم تا همچو بحر و کان بشوم</p></div>
<div class="m2"><p>ز جود دست و دلت خوار و زار بیگه و گاه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نه بحرم آبروی من ز جود خویش مبر</p></div>
<div class="m2"><p>نه ‌کانم ازکرمت خاک من به باد مخواه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نه روزگارم تا همچو روزگار کنی</p></div>
<div class="m2"><p>ز ذیل قدرت خود دست جور من ‌کوتاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نه آفتاب حرورم نه آسمان غرور</p></div>
<div class="m2"><p>که رای و قدر تو بنشاندم به خاک سیاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه دهرم از غضبت جان من چو دهر مسوز</p></div>
<div class="m2"><p>نه‌کوهم از سخطت جسم‌ من چوکاه مخواه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه بخلم از چه ز من خاطر ترا اعراض</p></div>
<div class="m2"><p>نه ظلمم از چه ز من طینت ترا اکراه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بخوان بخوان نوالم ‌که ‌کم نخواهد شد</p></div>
<div class="m2"><p>زکاسه‌لیسی درویش خوان نعمت شاه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>الا به ‌گیتی تا در طبیعت محرور</p></div>
<div class="m2"><p>هم فزایدکافور بر به قوهٔ باه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به دهر امر تو قاهر چو باز بر تیهو</p></div>
<div class="m2"><p>به چرخ حکم تو غالب چو شیر بر روباه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سزد که مدح‌ کنم این مدیح دلکش را</p></div>
<div class="m2"><p>به مدح خاتم پیغمبران جعلت فداه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کمال مطلق فیض بسیط عقل نخست</p></div>
<div class="m2"><p>محیط امکان مصداق‌کان حبیب‌الله</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وجود آگهش از سر هر وجود خبیر</p></div>
<div class="m2"><p>ضمیر روشنش از فکر هر ضمیر آگاه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به خاک بندگی او مزینست خدود</p></div>
<div class="m2"><p>به داغ پیره‌ری از موسَمست جباه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ولای او بود از هر بلا وقایهٔ تن</p></div>
<div class="m2"><p>ز بیم آنکه اجل تاختن‌ کند ناگاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کمند وهم به بام جلال او نرسد</p></div>
<div class="m2"><p>زهی کمال شرف لا اله الا الله</p></div></div>