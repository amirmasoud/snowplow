---
title: >-
    قصیدهٔ شمارهٔ ۳۱۵ - ایضاً د‌ر مدح شاهنشاه ماضی محمدشاه قاضی طاب ثراه
---
# قصیدهٔ شمارهٔ ۳۱۵ - ایضاً د‌ر مدح شاهنشاه ماضی محمدشاه قاضی طاب ثراه

<div class="b" id="bn1"><div class="m1"><p>شاها ز ساغر لب ساقی شراب خواه</p></div>
<div class="m2"><p>آخر سکندری تو ازین چشمه آب خواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از لعل یار بوسهٔ همچون شکرستان</p></div>
<div class="m2"><p>ز الماس جام جوهر یاقوت ناب خواه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی بخواه باده و بوس وکنار جوی</p></div>
<div class="m2"><p>مطرب بخوان و بربط و چنگ و رباب خواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیشب هلال عید ز بام افق نمود</p></div>
<div class="m2"><p>از دست مهوشی می چون آفتاب خواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آب تیغ در دل آتش شرر فکن</p></div>
<div class="m2"><p>وز خاک‌ کوی خویش شکست گلاب خواه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اقبال و بخت و شوکت و فر همعنان طلب</p></div>
<div class="m2"><p>تایید و عون و فتح و ظفر همرکاب خواه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از عزم خود شتاب و ز‌ گردون درنگ جوی</p></div>
<div class="m2"><p>از حزم خود درنگ و ز غبرا شتاب خواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدخواه را ز چشمهٔ رخشان تیغ خویش</p></div>
<div class="m2"><p>سیراب ساز و چشمهٔ عمرش سراب خواه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از روی و رای خویش مه و آفتاب جوی</p></div>
<div class="m2"><p>از قدر و بذل خویش سپهر و سحاب خواه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از لطف خود به جان مؤالف ثواب بخش‌</p></div>
<div class="m2"><p>وز قهر خود به جای مخالف عقاب خواه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا ناورد ز حکم تو گردن‌ کشد برون</p></div>
<div class="m2"><p>از کهکشان به گردن گردون طناب خواه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا صدهزار کشتی جان از بلا رهد</p></div>
<div class="m2"><p>پنهان نهنگ تیغ به بحر قراب خواه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جز بخت خود که قرعهٔ بیداریش زدند</p></div>
<div class="m2"><p>از امن عدل خویش ‌جهان را به‌خواب خواه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بادا دوام عمر تو تا روز رستخیز</p></div>
<div class="m2"><p>یارب دعای بندهٔ خود مستجاب خواه</p></div></div>