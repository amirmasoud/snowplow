---
title: >-
    قصیدهٔ شمارهٔ ۶۸ - در مدح شاهزاده شجاع السلطنه حسنعلی میرزا
---
# قصیدهٔ شمارهٔ ۶۸ - در مدح شاهزاده شجاع السلطنه حسنعلی میرزا

<div class="b" id="bn1"><div class="m1"><p>ای صفاهان مژده کاینک شاه دوران می رسد</p></div>
<div class="m2"><p>جسم بیجان ترا از نو به تن جان می‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غصه را بدرود کن‌کاید مسرت این زمان</p></div>
<div class="m2"><p>درد را پیغام‌ ده‌ کاین‌ لحظه درمان می‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد نعل توسنش بنشست بر اندام ما</p></div>
<div class="m2"><p>خاک راه موکبش تا چرخ‌گردان می‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ظل چتر رایتش ‌‌گسترده تا ترشم برین</p></div>
<div class="m2"><p>دور باش حضرتش‌‌تاکاخ‌کیوان می‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با جلال‌کیقباد و شوکت افراسیاب</p></div>
<div class="m2"><p>با شکوه قیصر و فرّ سلیمان می‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خسره پرویز آ‌ید زی مداین این زمان</p></div>
<div class="m2"><p>یا سوی کابلستان سام نریمان می‌رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا نه پور زادشم پوید به حصن گنگ دژ</p></div>
<div class="m2"><p>یا نه‌گرد زابلی سوی سجستان می‌رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا نه تیمور دوم‌ گردد سمرقندش‌ مکان</p></div>
<div class="m2"><p>یا نه قاآن نخسش زی‌کلوران می‌رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا نه سلطان آتسز روزی هزار اسب آورد</p></div>
<div class="m2"><p>یا مگر شاه اخستان نزد شروان می‌رسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اردوان کاردان اکنون شتابد سوی ری</p></div>
<div class="m2"><p>اردشیر شیردل نک سوی‌کرمان می‌رسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یا به سوی بارهٔ استخر تازد جم شید</p></div>
<div class="m2"><p>یا به سوی‌ کشور تبریز غازان می‌رسد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یا مگر سنجر به نیشابور راند بادپای</p></div>
<div class="m2"><p>یا مگر سلطان جلال‌الدین به‌ ملتان می‌رسد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یا اتابک جانب شیراز فرماید نزول</p></div>
<div class="m2"><p>یا حسن شاه بهادر زی سپاهان می‌رسد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن‌جهانداری ‌که از خاک ره جان‌پرورش</p></div>
<div class="m2"><p>سرزنش‌ها هر زمان بر آب حیوان می‌رسد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن جهانجویی ‌که از بوی نسیم رافتش</p></div>
<div class="m2"><p>هر نفس بیغارها بر باغ رضوان می‌رسد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه از یاقوت‌باریهای نوک تیغ او</p></div>
<div class="m2"><p>طعنها هر لحظه برکوه بدخشان می‌رسد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نسبت رایش نخواهم داد با تابنده مهر</p></div>
<div class="m2"><p>زانکه راث را آریا تشبیه نقصان می‌رسد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آشکارا هر زمان از جانب بخت سعید</p></div>
<div class="m2"><p>بر روان او اشارت‌های پنهان می‌رسد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا به‌کی قاآنیا بیهوده می‌رانی سخن</p></div>
<div class="m2"><p>کی‌از ای‌ت‌توصیف‌اوصاف‌به‌پایان می‌رمبد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باد تابان اخترت تا هر سحر از خاوران</p></div>
<div class="m2"><p>سوی ملک باختر خورشید تابان می‌رسد</p></div></div>