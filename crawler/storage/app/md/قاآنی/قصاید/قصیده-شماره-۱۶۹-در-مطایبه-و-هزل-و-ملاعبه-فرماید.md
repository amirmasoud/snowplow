---
title: >-
    قصیدهٔ شمارهٔ ۱۶۹ - در مطایبه و هزل و ملاعبه فرماید
---
# قصیدهٔ شمارهٔ ۱۶۹ - در مطایبه و هزل و ملاعبه فرماید

<div class="b" id="bn1"><div class="m1"><p>کوهی به قفا بسته‌ای ای شوخ دلازار</p></div>
<div class="m2"><p>با خویش کشانیش به هر کوچه و بازار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان ‌کوه‌ گران ترسمت آزرده شود تن</p></div>
<div class="m2"><p>خود را عبث ای شوخ دلازار میازار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو کاه‌ کشیدن نتوانی چه‌ کشی ‌کوه</p></div>
<div class="m2"><p>تو نرم‌تر و تازه‌تری ازگل بربار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نور مه چارده ماند به رخت رنگ</p></div>
<div class="m2"><p>وز برگ ‌گل تازه خلد بر قدمت خار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر لاله نهی پای شود پای تو رنجور</p></div>
<div class="m2"><p>بر سایه نهی گام شود گام تو آزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با حالتی این‌گونه مرا بس عجب آید</p></div>
<div class="m2"><p>کاین ‌کوه ‌کشیدن نبود نزد تو دشوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مزدور نیی اینهمه آخر چه‌ کشی رنج</p></div>
<div class="m2"><p>حمال نیی این‌ همه آخر چه بری بار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من بار تو بر سینه نهم ای بت شنگول</p></div>
<div class="m2"><p>کز بردن بار تو مرا می‌نبود عار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن بار گران را که ‌کشند ار بتر ازو</p></div>
<div class="m2"><p>شک نیست‌که در و‌زن بچربد زد و خروار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چونست‌که آویخته داریش به مویی</p></div>
<div class="m2"><p>این جرّ ثقیل از که بیاموختی ای یار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>موییست میان تو میاویز بدین ‌کوه</p></div>
<div class="m2"><p>ترسم‌که‌گسسته شود آن موی به یکبار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یارب چه بخیلی توکه اندر قصب سرخ</p></div>
<div class="m2"><p>پیوسته‌ کنی سیم سپید ای همه انبار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیم از پی دادن بود و عقده‌گشادن</p></div>
<div class="m2"><p>نز بهر نهادن ‌که تبه‌ گردد و مردار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زان سیم بپرهیزکه روزی ببرد دزد</p></div>
<div class="m2"><p>رندان تو ندانی ‌که چه چستند و چه طرار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من در بغل خویش‌ کنم سیم تو پنهان</p></div>
<div class="m2"><p>تا راه به سیمت نبرد دزد ستمکار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مردم همه دانندکه من طرفه امینم</p></div>
<div class="m2"><p>در کار امانت به خیانت نشوم یار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن سیم مرا ده که نگهدارمش از دزد</p></div>
<div class="m2"><p>پنهان‌کنم اندر شکن جبه و دستار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ور مشورت از من‌کنی و رای تو باشد</p></div>
<div class="m2"><p>در سیم تو الا به تجارت نکنم‌کار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سیم تو دهم وام به اعیان ولایت</p></div>
<div class="m2"><p>باسوده ده و شانزده چون مرد رباخوار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شک نیست‌که سیم از پی سودا بود و سود</p></div>
<div class="m2"><p>تا مایهٔ امسال فزونتر شود از پار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ور رسم تجارت نبود سیم بکاهد</p></div>
<div class="m2"><p>در مدت اندک برود مایهٔ بسیار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ور نیز به تنها نکنی رای تجارت</p></div>
<div class="m2"><p>من با تو شراکت‌کنم ای دوست به ناچار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>من بر زبر سیم تو از چهره نهم زر</p></div>
<div class="m2"><p>وایین شراکت بگذاریم چو تجّار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زر من و سیم تو هرآن سودکه بخشد</p></div>
<div class="m2"><p>تقسیم نماییم به آیین و به هنجار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دو بهره مرا باشد و یک بهره ترا زانک</p></div>
<div class="m2"><p>بر سیم بچربد ز در قیمت دینار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نی نی که من این حرف به انصاف نگفتم</p></div>
<div class="m2"><p>دینار مرا نیست بر سیم تو مقدار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دینار مرا کس ز من امروز نخرّد</p></div>
<div class="m2"><p>وان سیم ترا جمله بجانند خریدار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>امروز بتا شرح دهم قصهٔ دوشین</p></div>
<div class="m2"><p>کان قصه ترا غصه زداید ز دل زار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دوشینه شدم جانب آن خانه‌ که دانی</p></div>
<div class="m2"><p>جایی‌که به شب چرخ برین را نبود بار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خود را بدو صد حیله در آن خانه فکندم</p></div>
<div class="m2"><p>پنهان به‌ کمینی شده چون روبه مکار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برخی نشد از شب‌ که ز جا مرغ صراحی</p></div>
<div class="m2"><p>برجست و همی لعل روان ریخت ز منقار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون ماه فروزنده ز هر حجره درآمد</p></div>
<div class="m2"><p>حوری بچه‌یی سرو به قد کبک به رفتار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یک جوق پری از پی دیوانگی خلق</p></div>
<div class="m2"><p>از چهر نکو پرده فکندند به یکبار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حوری نسبانی همه چون سرو قباپوش</p></div>
<div class="m2"><p>غلمان بچگانی همه چون ماه‌کله‌دار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>قد همه چون فکرت من آمده موزون</p></div>
<div class="m2"><p>زلف همه چون طالع من گشته نگونسار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دوری دو سه چون باده ببردند و بخوردند</p></div>
<div class="m2"><p>برخاست خروش دهل و چنگ و دف و تار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در رقص فتادند و سرین‌های مدور</p></div>
<div class="m2"><p>در چرخ زدن آمد چون‌گنبد دوِار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آوازه فکندند بهم مالک و مملوک</p></div>
<div class="m2"><p>شلوار بکندند ز پا بنده و سالار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دامن به کمر بر زده هر یک ز پس و پیش</p></div>
<div class="m2"><p>چون زاهد وسواسی در کوچهٔ خمار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا چشم همی‌رفت سرین بود به خرمن</p></div>
<div class="m2"><p>تا دیده همی دید سمن بود به خروار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گفتی‌ که بود کارگه دنبه‌ فروشان</p></div>
<div class="m2"><p>کانجا به سلم دنبه فروشند به قنطار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یا طایفهٔ پنبه‌فروشان ز پس سود</p></div>
<div class="m2"><p>آورده همی پنبهٔ محلوج به بازار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بازار حلب بود توگفتی‌که ز هر سوی</p></div>
<div class="m2"><p>گردیده یکی آینهٔ صاف پدیدار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گفتی‌که سرین همه قندیل بلورست</p></div>
<div class="m2"><p>کاویخته از بهر چراغان به شب تار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مانا مگر از عهدکیومرث بهر شهر</p></div>
<div class="m2"><p>سیمین ‌کفلی بوده در آنجا شده انبار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>القصه بخوردند و بخفتند ز مستی</p></div>
<div class="m2"><p>بر روی هم افتاده ز هرگوشه ملخ‌وار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از پیش قضیب همه چون دانهٔ خرما</p></div>
<div class="m2"><p>وز پشت سرین همه چون تل سمن‌زار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زینسوی همه شمع و زانسو همه قندیل</p></div>
<div class="m2"><p>زین روی همه‌ گنج وزان رو همه چون مار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>من چابگ و چالاک برفتم زکمینگاه</p></div>
<div class="m2"><p>زانگونه‌که‌کفتار رود بر سر مردار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آنان همه سرمست و مرا فرصت دردست</p></div>
<div class="m2"><p>آنان همه در خواب و مرا طالع بیدار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>در ساق یکی نرم فرو بردم انگشت</p></div>
<div class="m2"><p>وز پای یکی‌گرم برون ‌کردم شلوار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گه‌ کام من از بوسهٔ این معدن شکّر</p></div>
<div class="m2"><p>گه مغز من از طرهٔ آن طبلهٔ عطار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بر دمّل آن‌گاه فرو بردم نشتر</p></div>
<div class="m2"><p>در ثقبهٔ این‌گاه فرو کردم مسمار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تیغم به سپر رفت فرو تا بن قبضه</p></div>
<div class="m2"><p>تیرم به هدف‌گشت نهان تا پر سوفار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>در چشم فرودین همه را میل‌کشیدم</p></div>
<div class="m2"><p>نه خواجه به جا باز نهادم نه پرستار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>القصه بدین قدّ کمان‌وار همه شب</p></div>
<div class="m2"><p>حلاج صفت پنبه‌زدن بود مراکار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>من تکیه چو بهمن زده بر تخت ‌کیانی</p></div>
<div class="m2"><p>وانان چو فرامرز شده بر زبر دار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تا زان تل و ماهور برون رانم شبدیز</p></div>
<div class="m2"><p>مهمیز زدم بر فرس نفس ستمکار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نردیک‌‌ اذان سحر از جای بجستم</p></div>
<div class="m2"><p>گفتم بهلم نقشی ازین نادره‌ کردار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از جیب قلمدان به‌در آوردم چابک</p></div>
<div class="m2"><p>مانند دبیری‌ که بود کاتب اسرار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بر صفحهٔ سیمین سرینشان بنوشتم</p></div>
<div class="m2"><p>نام و لقب خویش‌ که النار ولاالعار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>وانگه ز پی توشهٔ ره بوسهٔ چندی</p></div>
<div class="m2"><p>برداشتم از ساق و سرین و لب و رخسار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>وایدون به یقینم‌ که بر الواح سرینشان</p></div>
<div class="m2"><p>باقی بود آن نقش چو بر آینه زنگار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چون نام مرا صبح ببینند نوشته</p></div>
<div class="m2"><p>گویند زهی شاعرک شبرو عیار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>باری همه را داغ غلامی بنهادم</p></div>
<div class="m2"><p>کز صحبت منشان نبود زین سپس انکار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>و یدون همه را در عوض جامه و جیره</p></div>
<div class="m2"><p>طومار غزل می‌دهم وکاغذ اشعار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>لیکن به سر و جان تو ای ترک که امروز</p></div>
<div class="m2"><p>کردم بدل از هرگنه رفته ستغفار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>زیراکه دلی تا زگنه پاک نگردد</p></div>
<div class="m2"><p>آورد نیارد به زبان مدح جهاندار</p></div></div>