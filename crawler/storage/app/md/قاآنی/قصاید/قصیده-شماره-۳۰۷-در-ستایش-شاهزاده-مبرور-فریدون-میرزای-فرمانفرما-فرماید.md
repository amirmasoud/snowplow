---
title: >-
    قصیدهٔ شمارهٔ ۳۰۷ - در ستایش شاهزاده مبرور فریدون میرزای فرمانفرما فرماید
---
# قصیدهٔ شمارهٔ ۳۰۷ - در ستایش شاهزاده مبرور فریدون میرزای فرمانفرما فرماید

<div class="b" id="bn1"><div class="m1"><p>دوش چو بنهفت نوعروس ختن رو</p></div>
<div class="m2"><p>شاهد زنگی‌ گره‌ گشاد ز ابرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترک من آمد ز ره چو شعلهٔ آتش</p></div>
<div class="m2"><p>گرم و دم آهنج و تند و توسن و بدخو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سر زلف دو صد شکنج به عارض</p></div>
<div class="m2"><p>چون خم جعدش دو صد ترنج بر ابرو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خم خم و چین چین گره ‌گره سر زلفش</p></div>
<div class="m2"><p>از بر دوش اوفتاده تا سر زانو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تاب به مویش چنانکه بوی به عنبر</p></div>
<div class="m2"><p>تاب به رویش چنانکه رنگ به لولو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلف پریشیده بر عذارش چو نانک</p></div>
<div class="m2"><p>بال ‌گشاید در آفتاب پرستو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چهرهٔ رخشنده از میان دو زلفش</p></div>
<div class="m2"><p>تافت بدانسان‌ که ‌گرد مه ز ترازو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا نه تو گفتی به نزد خواجهٔ رومی</p></div>
<div class="m2"><p>زایمن و ایسرستاده‌اند دو هندو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جستم و بنشاندمش به صدر و فشاندم</p></div>
<div class="m2"><p>گرد رهش به آستین ز طلعت نیکو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مانا نگذشت یک‌دو لمحه‌ که بگذشت</p></div>
<div class="m2"><p>آهش از آسمان و اشک ز مشکو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چهرش بغدادگشت و مژگان دجله</p></div>
<div class="m2"><p>رویش خوارزم‌ گشت و دیده قراسو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در عوض مویه چشمه راند ز هر چشم</p></div>
<div class="m2"><p>بر صفت دیده مویه‌کرد ز هر مو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گشت بدانگونه موی موی‌ که‌ گفتی</p></div>
<div class="m2"><p>در بن هر موی‌کرده تعبیه آمو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چهر سپیدش ز اشک چشم سیاهش</p></div>
<div class="m2"><p>یاد ز خوارزم کرد و آب قراسو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتمش ای مه به جان من ز چه مویی</p></div>
<div class="m2"><p>گفت ز بیداد شهریار جفا جو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفتمش ای ترک ترک هذیان می‌کن</p></div>
<div class="m2"><p>خیز و صداعم مده وداعم می‌گو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مهلا مهلا سخن مگو به درشتی</p></div>
<div class="m2"><p>کت خرده خرده دان ندارد معفو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نام ستم بر شهی منه‌که به عهدش</p></div>
<div class="m2"><p>بازگریزد زکبک و شیر ز راسو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طعن جفا بر شهی مزن‌که به‌دورش</p></div>
<div class="m2"><p>بیضه نهد درکنام شاهین تیهو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفت زمانی زمام منع فروکش</p></div>
<div class="m2"><p>دست ز تقلید ناصواب فروشو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ظلم فراتر ازین‌که شاه جهانم</p></div>
<div class="m2"><p>ساخته رسوا به هر دیار و به هرکو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جور ازین بین‌کاو ز درگه خویشم</p></div>
<div class="m2"><p>نیک به چوگان قهر راند چون ‌گو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سرو بود برکنار جوی و من اینک</p></div>
<div class="m2"><p>سروم و جاریست درکنار مراجو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گرچه به شه مایلم ازو بهراسم</p></div>
<div class="m2"><p>اینت شگبفتی اخاف منه و ارجو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرچه به شه عاشقم ازو به ملالم</p></div>
<div class="m2"><p>اینت عجب‌کز وی استغیث وادنو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شه ز چه هر مه برون رود پی نخجیر</p></div>
<div class="m2"><p>آهو اگر باید دو چشم من آهو</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گو نچمد از قفای‌گور به هر دشت</p></div>
<div class="m2"><p>گو ندود در هوای ‌کبک به هر سو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بهرگوزنان به دشت وکه نبرد راه</p></div>
<div class="m2"><p>بهر تذروان به راغ و کو ننهد رو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کبک و تذروش منم به خنده و رفتار</p></div>
<div class="m2"><p>رنج‌ کمان‌ گو مخواه و زحمت بازو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گور وگوزنش منم به دیده و دیدار</p></div>
<div class="m2"><p>گو منما در فراز و شیب تکاپو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گورکمند افکنم‌گوزن‌کمان‌کش</p></div>
<div class="m2"><p>کبک قدح خواره‌ام تذر و سخنگو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گفتمش ای ترک حق به سوی تو بینم</p></div>
<div class="m2"><p>چون تو بسی شاکی‌اند از ستم او</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سیم‌کند ناله زر نماید فریاد</p></div>
<div class="m2"><p>بحر کند نوحه‌ کان نماید آهو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>لیک ز روی ادب به شاه جهاندار</p></div>
<div class="m2"><p>مرد خردمند می‌نگیرد آهو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ظلم چنین خوش‌تر از هزاران انصاف</p></div>
<div class="m2"><p>درد چنین بهتر از هزاران دارو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شاه فریدون خدایگان جهانست</p></div>
<div class="m2"><p>اوست‌ که قدرش بر آسمان زده پهلو</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گنج نبالد چو او به تخت دل‌افروز</p></div>
<div class="m2"><p>ملک ببالد چو او به رخش جهان‌پو</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حزمش مبرم‌تر از هزاران باره</p></div>
<div class="m2"><p>رایش محکم‌تر از هزاران بارو</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بر در قصرش هزار بنده چو ارغون</p></div>
<div class="m2"><p>در بر بارش هزار برده چو منکو</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>صولت چنگیزخان شکسته به یاسا</p></div>
<div class="m2"><p>پردهٔ تیمور شه دریده به یرغو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تیغ تو هنگام وقعه‌کرد به دشمن</p></div>
<div class="m2"><p>تیر تو در وقت‌ کینه‌ کرد به بدگو</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آنچه فرامرز یل نمود به سرخه</p></div>
<div class="m2"><p>آنچه نریمان‌گو نمود به‌کاکو</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ای‌که بنالد ز زخم‌گرز تو رستم</p></div>
<div class="m2"><p>ویکه به موید ز بیم بر ز تو برزو</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خشم تو از شاخ ارغوان ببرد رنگ</p></div>
<div class="m2"><p>مهر تو از برگ ضیمران ببرد بو</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>رنگین‌گردد ز تاب روی تو محفل</p></div>
<div class="m2"><p>مشکین ‌گردد ز بوی خلق تو مشکو</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بس‌ که به مدحت رقم زدند دفاتر</p></div>
<div class="m2"><p>قیمت عنبر گرفت دوده و مازو</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>برق حسامت به هر دمن‌ که بتابد</p></div>
<div class="m2"><p>روید از آن تا به حشر لالهٔ خودرو</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ابر عطایت به هر چمن‌که ببارد</p></div>
<div class="m2"><p>خوشهٔ خرما دمد ز شاخهٔ ناژو</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نقش توانی زدن بر آب به قدرت</p></div>
<div class="m2"><p>کوه توانی ز جای ‌کند به نیرو</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چرخ بود همچو بزم عیش تو هیهات</p></div>
<div class="m2"><p>راغ و چمن دیر وکعبه‌گلخن و مینو</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یا چو ضمیرت بود ستاره علی‌الله</p></div>
<div class="m2"><p>مهر و سها لعل و خاره شکر و مینو</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شاخی‌ گوهر دهد چو کلک تو نه‌ کی</p></div>
<div class="m2"><p>حاشا کلّا چسان چگوهه ‌کجا کو</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>عزم تو بر آب ریخت آب سکندر</p></div>
<div class="m2"><p>حزم تو بر باد داد خاک ارسطو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گو نفرازد عدو به بزم تو رایت</p></div>
<div class="m2"><p>گو نکند خصم در بر تو هیاهو</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مرغ نیی‌کت بود هراس زمحندار</p></div>
<div class="m2"><p>طفل نیی ‌کت بود نهیب ز لولو</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>پیکر گردون شود ز تیر تو غربال</p></div>
<div class="m2"><p>سینهٔ‌ گردان شود ز تیر تو ماشو</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دادگر تا مراست مدح تو آیین</p></div>
<div class="m2"><p>بس‌ که‌ کنم سخره بر امامی و خواجو</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>خواجهٔ خواجویم و امام امامی</p></div>
<div class="m2"><p>شاعر سحارم و سخنور و جادو</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نیست شگفتی‌که همچو صیت نوالت</p></div>
<div class="m2"><p>صیت‌کمالم فتد به طارم نه تو</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بس کن قاآنیا چه هرزه درایی</p></div>
<div class="m2"><p>رو که به درگاه شه ‌کم از همه‌یی تو</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مدحت خسرو چه ‌گویی ای همه‌‌ گستاخ</p></div>
<div class="m2"><p>چرخ نیاید به ذرع و بحر به مشکو</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>اهل جهان را به‌ گوش تا عجب آید</p></div>
<div class="m2"><p>واقعهٔ اندروس‌اا و قصهٔ هارو</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>خصم ز بأس تو بیند آنچه همی دید</p></div>
<div class="m2"><p>دولت مستعصم از نهیب هلاکو</p></div></div>