---
title: >-
    قصیدهٔ شمارهٔ ۲۲۶ - در ستایش جناب جلالت مآب صدراعظم گوید
---
# قصیدهٔ شمارهٔ ۲۲۶ - در ستایش جناب جلالت مآب صدراعظم گوید

<div class="b" id="bn1"><div class="m1"><p>ای بت سیمین بناگوش ای به تن چون سیم خام</p></div>
<div class="m2"><p>ای دو زنگی طره‌ات را عنبر و ریحان غلام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه نمایی ازگریبان سرو پوشی در حریر</p></div>
<div class="m2"><p>گل‌گذاری زیر سنبل نور بندی در ظلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پستهٔ خندان تو چون تُنگ شکر دلفریب</p></div>
<div class="m2"><p>رستهٔ دندان تو چون سلک‌گوهر با نظام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسکه سر تا پا لطیفی هیچ عضوت را ز هم</p></div>
<div class="m2"><p>می‌نشاید فرق ‌کردن ‌کاین ‌کدامست آن‌ کدام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قامتست این یا قیامت عارضست این یا قمر</p></div>
<div class="m2"><p>صورتست این یا معانی شکرست این یا کلام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ها بجنبان زلف تا باد صبا آید به رقص</p></div>
<div class="m2"><p>هی بیفشان موی تا مرغ هوا افتد به دام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موی بگشا تا دگر هرگز نگردد شام صبح</p></div>
<div class="m2"><p>روی بنما تا دگر هرگز نگردد صبح شام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طرهٔ تو مغربست و چهرهٔ تو آفتاب</p></div>
<div class="m2"><p>. چهره بنما سهل باشدگو قیامت‌کن قیام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا به‌کی در حجره پنهانی چو غلمان در بهشت</p></div>
<div class="m2"><p>آخر ای نوباوهٔ حورا یکی بیرون خرام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فکر ننگ و نام تاکی چنگ و جام آور به‌کف</p></div>
<div class="m2"><p>چنگ و جام ار هست باقی‌گو نباشد ننگ و نام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عیش می‌روید به جای لاله امروز از زمین</p></div>
<div class="m2"><p>وجد می‌بارد به جای ژاله امروز از غمام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روز مولود شهنشاهست و در روزی چنین</p></div>
<div class="m2"><p>هرکه غمگینست بر وی زندگی بادا حرام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در چنین روزی ‌که خون از وجد می‌جوشد به تن</p></div>
<div class="m2"><p>در چنین روزی‌که می از شوق می رقصد به جام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در چنین روزی که می‌جنبد ز وصل دوست دل</p></div>
<div class="m2"><p>در چنین روزی‌که می‌پرد ز شوق جام‌کام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باده باید آنقدر خوردن ‌که جای خون و خوی</p></div>
<div class="m2"><p>می‌دود اندر عروق و می‌تراود از مسام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لیک من از تنگدستی چون ندارم وجه می</p></div>
<div class="m2"><p>مست سازم خویش را از مدحت صدر انام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آفتاب دین و دولت حکمران شرق و غرب</p></div>
<div class="m2"><p>آسمان ملک و ملت اعتضاد خاص و عام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صدر اعظم بدر عالم شمس ملت تاج ملک</p></div>
<div class="m2"><p>غیث دولت غوث دین کان کرم کهف کرام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آنکه‌ کاخش از حوادث دهر را دارالامان</p></div>
<div class="m2"><p>وانکه بزمش از سوانح خلق را دارالسلام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نامهٔ اقبال و دولت را به نامش افتتاح</p></div>
<div class="m2"><p>دفتر اجلال و شوکت را به تیغش اختتام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روز مهرش سرو و سنبل روید از صحرا وکوه</p></div>
<div class="m2"><p>گاه جودش سیم وگوهر ریزد از دیوار و بام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سنگ را بیجاده سازد حزمش از یک التفات</p></div>
<div class="m2"><p>خاک را فیروزه سازد عزمش از یک اهتمام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خامهٔ او ظم صد لشکر دهد از یک صریر</p></div>
<div class="m2"><p>خاطر او فتح صدکشورکند از یک مسام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خلق را نگذاشتی یک لحظه جودش‌گرسنه</p></div>
<div class="m2"><p>گر ز امر حق نبودی فرض بر مردم صیام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پشه‌یی را باد اگر در عهد او سیلی زند</p></div>
<div class="m2"><p>خشم او تا روز حشر از بادگیرد انتقام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا نظام ملک و دین را گشت ‌کلک او کفیل</p></div>
<div class="m2"><p>تیرها درکیش ماند و تیغها اندر نیام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای دل و دست ترا دریا وکان نایب مناب</p></div>
<div class="m2"><p>ای رخ و رای ترا خورشید و مه قایم مقام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر جبینی راکه نبود داغ مهرت بر جبین</p></div>
<div class="m2"><p>باز زی پشت پدر برگردد از زهدان مام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گرمی مهر تو مور و مار را کردست صید</p></div>
<div class="m2"><p>نرمی نطق تو وحش و طیر راکردست رام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عاجزی از مالش موری اگرچه قادری</p></div>
<div class="m2"><p>کز دو تار مو نمایی بر سر شیران لجام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برگها با نظم می‌رویند از اطراف شاخ</p></div>
<div class="m2"><p>نوبهار عدلت از بس داده‌گیتی را نظام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مهر تو در هیچ دل نگذاشت جای آرزو</p></div>
<div class="m2"><p>بسکه شادی بر س شادی همی جست ازدحام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زر ز جودت خوار شد چندانکه زال زر ز خشم</p></div>
<div class="m2"><p>زانزجار این لقب نفرین‌کند بر جان سام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>صاحبا صدرا حدیثی طرفه دارم‌گوش‌کن</p></div>
<div class="m2"><p>زار و پژمان زال زر را دوش دیدم در منام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گفتمش زار از چه‌ای‌؟‌گفتا شنیدستم‌که زر</p></div>
<div class="m2"><p>از سخای خواجه شد چون خاک ره بی‌احترام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وینک اندر دخمهٔ تاری ز ننگ این لقب</p></div>
<div class="m2"><p>هر زمان از خشم نفرینها کنم بر جان سام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بر کمال قدرت یزدان بس این برهان تو</p></div>
<div class="m2"><p>بر یکی مسندکنی جا با دو عالم احتشام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فقر را زافراط جودت بر گلو گیرد فواق</p></div>
<div class="m2"><p>خلق را از بوی خلقت در مشام افتد زکام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا حکیمان را حکایت از حدوثست و قدم</p></div>
<div class="m2"><p>تا فقیهان را روایت از حلالست و حرام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ناصرت بادا شهنشه یاورت بادا خدای</p></div>
<div class="m2"><p>کشورت بادا به فرمان اخترت بادا به ‌کام</p></div></div>