---
title: >-
    قصیدهٔ شمارهٔ ۲۴۴ - د‌ر مدحت جغتای خان بن ارغون میرزا می‌فرماید
---
# قصیدهٔ شمارهٔ ۲۴۴ - د‌ر مدحت جغتای خان بن ارغون میرزا می‌فرماید

<div class="b" id="bn1"><div class="m1"><p>آمد برم سحرگه آن ترک سیمتن</p></div>
<div class="m2"><p>با طره‌یی سیاه‌تر از روزگار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مویش فراز رویش آزرم غالیه</p></div>
<div class="m2"><p>رویش به زیر مویش بیغارهٔ سمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مویی چگونه مویی یک راغ ضیمران</p></div>
<div class="m2"><p>رویی چگونه رویی یک باغ نسترن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماهی فراز سروش وه‌وه قرار جان</p></div>
<div class="m2"><p>سروی نشیب ماهش به‌به بلای تن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماهی چه ماه هی‌هی منظور خاص و عام</p></div>
<div class="m2"><p>بروی چه سرو بخ‌بخ مقصود مرد و زن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در تاب طره‌اش‌ که‌ گره از پی‌ گره</p></div>
<div class="m2"><p>در چین ‌گیسویش‌ که شکن از پی شکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک شهر دل به‌ بند کمند از پی‌ کمند</p></div>
<div class="m2"><p>یک ملک جان اسیر رسن از پی رسن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک خنده از لبانش و تا بنگری عقیق</p></div>
<div class="m2"><p>یک جلوه از رخانش و تا بگذری چمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون توده‌های ریگ‌ که از جنبش نسیم</p></div>
<div class="m2"><p>سیمین سرینش موج زند گفتی از سمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گو چهره‌اش نگه‌ کن از حلقهای زلف</p></div>
<div class="m2"><p>یزدان اگر ندیدی در بند اهرمن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنگر کلاله‌اش ز بر چهرهٔ لاله‌رنگ</p></div>
<div class="m2"><p>گر ضیمران ندیدی بر برگ یاسمن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنگر فراز نارونش لعل نارگون</p></div>
<div class="m2"><p>گر ناردان ندیدی بر شاخ نارون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر سو چمان و شهری پویانش از قفا</p></div>
<div class="m2"><p>هر سو روان و خلقی بر گردش انجمن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون دیدمش دویدم و در برکشیدمش</p></div>
<div class="m2"><p>خوشدل چنان شدم‌ که ز دیدار بت شمن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بنشستم و نشاندمش از مهر در کنار</p></div>
<div class="m2"><p>بر هیاتی ‌که شمع فروزنده در لگن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لختی ‌چو رفت ‌چهره ‌دژم ‌کرد و جبهه ترش</p></div>
<div class="m2"><p>چونان کسی که نوشد جام می کهن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفتم‌که تنگدل به چه‌گشتی بسان جام</p></div>
<div class="m2"><p>گفتا از آنکه نبود صاحبدلی چودن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتم‌ خم‌ش که صاحبدل در جهان بسیست</p></div>
<div class="m2"><p>گ‌فتا مگو که صرف‌‌ گمانست و محض ظن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفتم‌که‌ای حدیث من و تو به روزگار</p></div>
<div class="m2"><p>منسوخ ‌کرده قصهٔ شیرین و کوهکن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صاحبدل از چه مسلک‌ گفتا ز شاعران</p></div>
<div class="m2"><p>گفتم پی چه خدمت ‌گفتا مدیح من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مدحم نه اینکه ماه منیرم بود عذار</p></div>
<div class="m2"><p>وصفم نه اینکه چاه نگونم بود ذقن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بستایدم به اینکه هواخواه حضرتیست</p></div>
<div class="m2"><p>کامد به عهد مهد صف‌آرای و صف‌شکن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تابان در محیط جلالت جهان مجد</p></div>
<div class="m2"><p>جغتای‌خان بن ارغون خان بن حسن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شیرانش طعمه‌اند نبسته دهن ز شیر</p></div>
<div class="m2"><p>پیرانش سخره‌اند نشسته لب از لبن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خردست و خرده‌گیر به میران خرده‌دان</p></div>
<div class="m2"><p>طفلست و طعنه‌گوی به پیران پر فطن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خردست و شیرخوار ولی گرد شیرخوار</p></div>
<div class="m2"><p>از شیرزنش طعمه ولی مرد شیرزن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از خوی او شمیمی تا بنگری ختا</p></div>
<div class="m2"><p>از موی او نسیمی تا بگذری ختن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روزی رسد که بینی بر نوک خطیش</p></div>
<div class="m2"><p>نه چرخ را چو مرغی به فراز بابزن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>روزی رسد که بینی بر دشت‌ کارزار</p></div>
<div class="m2"><p>از آهنش‌ کلاه و ز پولاد پیرهن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>روزی رسد که بینی بر نوک نیزه‌اش</p></div>
<div class="m2"><p>بدخواه را چو پیلی بر شاخ ‌کرگدن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>روزی رسد که بینی بر ایمنش پرند</p></div>
<div class="m2"><p>وقتی رسد که بینی بر ایسرش مجن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>این در نظر سپهری آکنده از نجوم</p></div>
<div class="m2"><p>آن در صفت هلالی آموده از پرن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>روزی رسد که بینی بر جبهه‌اش ترنج</p></div>
<div class="m2"><p>وقتی شود که یابی بر چهره‌اش شکن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از آن ترنج خلقی دمساز با شکنج</p></div>
<div class="m2"><p>وزان شکن گروهی همراز با شجن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>طبعش ز بس‌ گهرخیز اندر گه سخا</p></div>
<div class="m2"><p>لطفش ز بس شکرریز اندرگه سخن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون نام این بری‌ گهرت خیزد از زبان</p></div>
<div class="m2"><p>چون وصف آن کنی شکرت ریزد از دهن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>این شبل آن غضنفر کز گاز و چنگ او</p></div>
<div class="m2"><p>بر پیکر تهمتن ببر بیان ‌کفن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>این مهر آن سپهر که از مهر و کین او</p></div>
<div class="m2"><p>یک‌ ملک را مسرت و یک ملک را محن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>این در آن صدف ‌که ز آزرم ‌گوهرش</p></div>
<div class="m2"><p>بیغاره از شبه شنود لؤلؤ عدن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>این پور آن ‌کیا که به میمند و اندخوذ</p></div>
<div class="m2"><p>خود گوان شکست ز کوپال‌ که‌ شکن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>این شبل آن اسد که ازو پیل را هراس</p></div>
<div class="m2"><p>این پور آن بدرگه ازو شیر را شکن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آخر نه این نبیرهٔ آن کز خدنگ او</p></div>
<div class="m2"><p>در پهنه جسم‌ گردان آزرم پر وزن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آخر نه این ز دودهٔ آن ‌کاتش حسامش</p></div>
<div class="m2"><p>در دودمان افغان افروخت مرزغن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آخر نه این ز تخمهٔ شاهی‌که بوقبیس</p></div>
<div class="m2"><p>گردد ز زخم‌ گرزش چون تخم پر پهن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آخر نه این نبیرهٔ شاهی ‌کزو گریخت</p></div>
<div class="m2"><p>کابل خدا چنانکه ز لاحول اهرمن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کابل خدا نه دهری آبستن از فساد</p></div>
<div class="m2"><p>کابل خدا نه چرخی آموده از فتن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>با لشکری فره همه در عزم مشتهر</p></div>
<div class="m2"><p>با موکبی ‌گران همه در رزم ممتحن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از سیستان و کابل و کشمیر و قندهار</p></div>
<div class="m2"><p>وز دیرجات هند بل از دهلی و دکن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آمد به مرز خاور و خاورمهان همه</p></div>
<div class="m2"><p>با یکدگر ز یاریش از ریو رایزن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خسرو شنید و رفت و درید و برید وکف</p></div>
<div class="m2"><p>بست و شکست و خست از آن لشکر کشن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از رمح و تیغ و خنجر و فتراک و گرز و تیر</p></div>
<div class="m2"><p>اندام و ترک و تارک و بازو و برز و تن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بس‌ تن که کوفت از چه ز کوپال جان‌شکر</p></div>
<div class="m2"><p>بت سر که‌ کفت از چه ز صمصام سرفکن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از بسکه‌ کشته پشته ‌گرانبار شد زمین</p></div>
<div class="m2"><p>از بسکه خسته بسته به زنهار شد زمن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هرکس که بود یارش شد خصم با ملال</p></div>
<div class="m2"><p>هرکس که بود خصمش یار با محن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مسروق پور ابرهه با صدهزار مرد</p></div>
<div class="m2"><p>شد از یمن به چالش زی سیف ذو‌الیزن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>وان پنج ره هزار بدش مرد کینه‌جوی</p></div>
<div class="m2"><p>با ششصد از عجم همه در رزم شیرون</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>رفت و شکست موکب مسروق را و گشت</p></div>
<div class="m2"><p>هم در یمن شهیر و همش خلق مفتتن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>آن رزم را بسنجد اگر کس به رزم شاه</p></div>
<div class="m2"><p>چون‌ کین ‌کودکست بر کینه پشن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تنها همین نه لشکر کابل خدا شکست</p></div>
<div class="m2"><p>از تیغ کُه شکاف و ز کوپال کُه‌شکن</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بس ملکها گرفت به بازوی ملک گیر</p></div>
<div class="m2"><p>بس حصنها گشود ز چنگال خاره‌کن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>شاهان ز خصم خویش ستانند ملک و او</p></div>
<div class="m2"><p>بخشد به‌ خصم خویش ‌همی‌ ملک خویشتن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آری چو خصم ازو کند از ملک او سوال</p></div>
<div class="m2"><p>ننگ آیدش ز فرط عطا گفت لا و لن</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>شاها مباش رنجه گر از کید روزگار</p></div>
<div class="m2"><p>سالی دو ماه بختت باکید مقترن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ایوب مر نه تنش به اسقام مبتلا</p></div>
<div class="m2"><p>یعقوب مر نه جانش به آلام مرتهن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>آن آخر از بلا جست از آب چشمه‌سار</p></div>
<div class="m2"><p>این آخر از عمی رست از بوی پیرهن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>یونس مگر نبودش در بطن نون سکون</p></div>
<div class="m2"><p>یوسف مگر نه‌گشتش در قعر چَه سکن</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>آن شد رسول قوم و شد آزاد از بلا</p></div>
<div class="m2"><p>این شد عزیز مصر و شد آزاد از حزن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>مر مصطفی نکرد نهان تن به تیره غار</p></div>
<div class="m2"><p>جولاهه مر نگشت به آن غار تار تن</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بگذر ز انبیا چه بزرگان‌که روزگار</p></div>
<div class="m2"><p>پیوسه‌شان قرین شجن داشت در سجن</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>مر کیقباد و بیژن و کاووس هر سه را</p></div>
<div class="m2"><p>زالبرز و چاه و کوری برهاند تهمتن</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>سنجر مگر نه در قفس غُز اسیر بود</p></div>
<div class="m2"><p>واخر به چاربالش فر گشت تکیه‌زن</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>اکنون تو نیز گرت مر این چرخ ‌کج‌نهاد</p></div>
<div class="m2"><p>دارد قرین تیمار از ریمن و شکن</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بشکیب ‌کز شکیب شود قطره پاک دُر</p></div>
<div class="m2"><p>بشکیب‌ کز شکیب شود خاره بهر من</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نی زار نالد آنگه از جان برد ملال</p></div>
<div class="m2"><p>می تلخ ‌گردد آنگه از جان برد محن</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>آسوده‌دل نشین‌ که چو دیماه بگذرد</p></div>
<div class="m2"><p>بلبل ‌کشد ترانه و خامش شود زغن</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>دلتنگ‌تر ز غنچه‌ کسی نی ولی به صبر</p></div>
<div class="m2"><p>بینی‌کزان شکفته‌تری نیست در چمن</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ملکی ستد خدای ‌که تا ملک دگرت</p></div>
<div class="m2"><p>بخشد همی نکوترهاگوش‌کن ز من</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>معمار خانهای کهن را کند خراب</p></div>
<div class="m2"><p>تا نو نهد اساس ‌که نو بهتر از کهن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>هرکس به قدر پایه ببایدش جایگاه</p></div>
<div class="m2"><p>عنقا کند به قاف وکبوتر بچه و کن</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>قدرت بلند و پست بسی تودهٔ زمین</p></div>
<div class="m2"><p>شخصیت عظیم و تنگ بسی فسحت زمن</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>گو ملک رو چو هست بجا تیغ ملک گیر</p></div>
<div class="m2"><p>گو بلخ شو خراب چو زنده است روی تن</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>روزی رسد تیغ یمانیت در یمین</p></div>
<div class="m2"><p>آرد زمین معرکه چون ساحت یمن</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>روزی رسد که چونان محمود زاولی</p></div>
<div class="m2"><p>در سومنات بت‌شکنی بر سر شمن</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>روزی رسد که از مدد تیغ‌ کفرسوز</p></div>
<div class="m2"><p>نه نام دیر شنوی نه نام برهمن</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>روزی رسد که بر تو شود فتنه روزگار</p></div>
<div class="m2"><p>چون نل‌که بود واله بر طلعت دمن</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>روزی رسد که خصم تو سر افکند به زیر</p></div>
<div class="m2"><p>چونان کسی که ناگه درگیردش وسن</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>شاها یک آفرین تو صد گنج‌ گوهر ست</p></div>
<div class="m2"><p>باورگرت نه لب بگشا از پی سخن</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بر این چکامه‌ گر بفشانی هزار گنج</p></div>
<div class="m2"><p>جز آفرینی از تو نخواهم ورا ثمن</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>لیکن یک آرزویم از دیرگه به دل</p></div>
<div class="m2"><p>زانم هماره بینی محزون و ممتحن</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>دارم یکی برادر در پارس پارسا</p></div>
<div class="m2"><p>کاو اندر آن دیار اویسست در قرن</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>جان‌گویدم ابی او خلد ار بود مرو</p></div>
<div class="m2"><p>دل راندم ابی او سور ار بود مزن</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بی‌او زیم چنانکه ابی ‌سرخ ‌گل‌گیا</p></div>
<div class="m2"><p>بی‌او بوم چنانکه ابی پاک جان بدن</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>گریم چو ابر بی او در شام و در سحر</p></div>
<div class="m2"><p>نالم چو رعد بی‌او در سر و در علن</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بی‌او دل از خروشم تفتیده چون تنور</p></div>
<div class="m2"><p>بی‌او رخ از خراشم آژیده چون سفن</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بی‌او ز غم ‌گزیر ندارم به هیچ مکر</p></div>
<div class="m2"><p>بی‌او ز رنج چاره ندارم به هیچ فن</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>جز چار مه نه بیش و نه ‌کم‌کم خدایگان</p></div>
<div class="m2"><p>فرمان دهدکه رخت‌کشم جانب وطن</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>گر گویدم ملک که بود راهزن به ‌راه</p></div>
<div class="m2"><p>گویم برهنه باک ندارد ز راهزن</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ور گویدم‌ که نیست ترا باره ی چمان</p></div>
<div class="m2"><p>گویم‌ که پای راهسپر بس مرا چمن</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>اینها تمام طیبت محضست اگرچه نیست</p></div>
<div class="m2"><p>طیبت ز بندگان به ملوک ای ملک حسن</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>منت خدای راکه مرا از عطای تو</p></div>
<div class="m2"><p>حاجت به کس نه‌جز به خداوند ذوالمن</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>منت خدای راکه ز بس جود بیحساب</p></div>
<div class="m2"><p>در زیر در و گوهر بنهفتیم بشن</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>قاآنیا توگرم بیانیّ و قافیه</p></div>
<div class="m2"><p>تکرار جست و دورست ابن معنی از فطن</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>صاحب ‌که با جوازش هذیان بود فصیح</p></div>
<div class="m2"><p>صاحب‌که با قبولش ابکم بود لسن</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>صدری‌ که در قلمرو شرع رسول‌ گشت</p></div>
<div class="m2"><p>کلکش چو تیغ شاه جهان محیی سنن</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>شاه زمانه فتحعلی شه‌که روز رزم</p></div>
<div class="m2"><p>درگوش بانگ شاد غرش لحن خارکن</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>دستش نه‌ گر مخالف با گوهر عمان</p></div>
<div class="m2"><p>طبعش نه ‌گر معاند با لؤلؤ عدن</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بهر چه بخشد آن یک‌گوهر همی به‌کیل</p></div>
<div class="m2"><p>بهر چه ریزد این یک لولو همی به من</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>تابان ز حلقهای زره جسم روشنش</p></div>
<div class="m2"><p>چون نور آفتاب‌که تابد ز آژگن</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>دستش چو یار خطی زلزال در خطا</p></div>
<div class="m2"><p>پایش چو جفت ختلی ولوال در ختن</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>اجراخور از عطایش پیوسته خاص و عام</p></div>
<div class="m2"><p>روزی‌بر از سخایش همواره مرد و زن</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>چونان‌که ختم آمد بر نام وی از سخا</p></div>
<div class="m2"><p>من نیز ختم‌کردم بر نام او سخن</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>تا دهر گاه محنت زاید گهی نشاط</p></div>
<div class="m2"><p>یارش قرین رامش و خصمش قرین رن</p></div></div>