---
title: >-
    قصیدهٔ شمارهٔ ۱۷۰ - در ستایش میرزا آقاخان صدراعظم
---
# قصیدهٔ شمارهٔ ۱۷۰ - در ستایش میرزا آقاخان صدراعظم

<div class="b" id="bn1"><div class="m1"><p>گفتم به یار فصل بهار آمد ای نگار</p></div>
<div class="m2"><p>گفتا که وصل یار نگارین به از بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم‌ که بار یافت هزاران به‌ گلستان</p></div>
<div class="m2"><p>گفتا زگلستان رخ من به هزار بار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم‌که لاله داغ بدل دارد از چه روی</p></div>
<div class="m2"><p>گفتا ز روی من دل لاله است داغدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم چو سرو کی به کنارم قدم نهی</p></div>
<div class="m2"><p>گفت آن زمان‌که رانی از دیده جویبار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم به زیر سایهٔ گیسو رخ تو چیست</p></div>
<div class="m2"><p>گفت ار به‌کس نگونی خورشید سایه‌دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم مگر بقد تو زلف تو عاشقست</p></div>
<div class="m2"><p>گفتا بلی به سرو روان عاشقست مار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم‌که زلفکان تو بر چهره چیستند</p></div>
<div class="m2"><p>گفتا به روم طایفه‌یی ز اهل زنگبار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم‌که اختیارکنم جز تو دلبری</p></div>
<div class="m2"><p>گفتاکه عاشقی نکندکس به اختیار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم از آن بترس‌که آهن دلی‌کنم</p></div>
<div class="m2"><p>گفت آن پری نیم‌که ز آهن کنم فرار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم ‌غزال چشم تو هست از چه شیر مست</p></div>
<div class="m2"><p>گفتا ز بس ‌که شیر دلان را کند شکار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم به آهوان دو چشم تو عاشقم</p></div>
<div class="m2"><p>گفتا خموش‌گردن شیر ژیان مخار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتم رسید جان به لبم ز انتظار تو</p></div>
<div class="m2"><p>گفت آن قدر بمان‌ که برآید ز انتظار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتم ببخش‌کام دلم ازکنار و بوس</p></div>
<div class="m2"><p>گفتا به جان خواجه ‌کزین ‌کام جو کنار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفتم مگر ندانی مداح خواجه‌ام</p></div>
<div class="m2"><p>گفتا اگر چنینست این بوس و این ‌کنار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتم‌ که صدر اعظم خواندش پادشه</p></div>
<div class="m2"><p>گفتاکه َبدرِ عالم دانَدش روزگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفتم نپروریده چنان خواجه آسمان</p></div>
<div class="m2"><p>گفتا نیافریده چنان بنده‌کردگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفتم بسیط ملک او هست بیکران</p></div>
<div class="m2"><p>گفتا محیط همت او هست بی‌کنار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتم به‌گاه جود عجو لست و بی‌سکون</p></div>
<div class="m2"><p>گفتا به‌گاه حلم حمولست و بردبار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفتم قرار هرچه تو بینی به دست اوست</p></div>
<div class="m2"><p>گفت از چه زر ندارد در دست او قرار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفتم‌که افتخار وی از فرّ و شو کتست</p></div>
<div class="m2"><p>گفتا که فر و شوکت ازو دارد افتخار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفتم‌که اشتهار وی از مال و دو لتست</p></div>
<div class="m2"><p>گفتاکه مال و دولت ازو جوید اشتهار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفتم توان ز سطوت وی زینهار جست</p></div>
<div class="m2"><p>گفتا به هیچ‌کس ندهد مرگ زینهار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفتم ‌که بر َیسارش ‌گردون خورد یمین</p></div>
<div class="m2"><p>گفتا ستم ز عدل سمینش بود نزار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفتم‌که هست فکرت او تار و عقل پود</p></div>
<div class="m2"><p>گفتاکه اعتماد بود پود را بتار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفتم‌ که هست دولت او بار و ملک برگ</p></div>
<div class="m2"><p>گفتا که افتخار بود برگ را به بار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفتم‌که موج بحرکفش را شماره چیست</p></div>
<div class="m2"><p>گفتا که موج بحر برونست از شمار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفتم عیارگیرد حزمش همی ز عقل</p></div>
<div class="m2"><p>گفتاکه عقل‌گیرد از حزم او عیار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفتم چه وقت پایهٔ خصمش شود بلند</p></div>
<div class="m2"><p>گفت آن زمان‌که خاک وجودش شود غبار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفتم بود ز مهرش هر هوشیار مست</p></div>
<div class="m2"><p>گفتا بود ز عدلش هر مست هوشیار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفتم سوارگان را قهرش پیاده‌کرد</p></div>
<div class="m2"><p>گفتا پیادگان را لطفش ‌کند سوار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گفتم حصار امن دو عالم وجود اوست</p></div>
<div class="m2"><p>گفتا به جز بلا که برونست از آن حصار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گفتم‌که اعتبار مرا نیست نزدکس</p></div>
<div class="m2"><p>گفتا به نزد خواجه بسی داری اعتبار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گفتم به عید پارم تشریف داد و زر</p></div>
<div class="m2"><p>گفتا به عید امسال افزون دهد ز پار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفتم نکو نیارم‌کاو را ثناکنم</p></div>
<div class="m2"><p>گفت ار ثنا نیاری دست دعا برآر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گفتم‌که عمر و دولت او باد مستدام</p></div>
<div class="m2"><p>گفتاکه جاه و شوکت او باد پایدار</p></div></div>