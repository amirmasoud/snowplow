---
title: >-
    قصیدهٔ شمارهٔ ۱۳۶ - در ستایش شاهزاده ی رضوان آرامگاه نواب فریدون میرزا طاب ثراه گوید
---
# قصیدهٔ شمارهٔ ۱۳۶ - در ستایش شاهزاده ی رضوان آرامگاه نواب فریدون میرزا طاب ثراه گوید

<div class="b" id="bn1"><div class="m1"><p>از سر دوش دو ضحاک درآویخت دو مار</p></div>
<div class="m2"><p>کان دو مار از همه آفاق برآورد دمار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مار آن عمرگزا چون نفس دیو لعین</p></div>
<div class="m2"><p>مار این روح‌فزا چون اثر باد بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مار آن چون به‌کمر سایه‌یی از ابر سیاه</p></div>
<div class="m2"><p>مار این چون به قمر خرمنی از عود قمار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مار آن‌آفت‌جان‌ود و ز جان‌جست قصان</p></div>
<div class="m2"><p>مار این فتنهٔ دل ‌گشت و ز دل برد قرار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مار آن مغز سر خلق بخوردی پیوست</p></div>
<div class="m2"><p>مار این خون دل زار بنوشد هموار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مار آن‌کرده به‌گوش از زبر دوش‌گذر</p></div>
<div class="m2"><p>مار این‌ کرده به دوش از طرف ‌گوش گذار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن دمید از زبر دوش و به‌ گوش آمد جفت</p></div>
<div class="m2"><p>این خمید از طرف گوش و به دوش آمد یار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن به بالا شده چون خشم‌گرفته تنین</p></div>
<div class="m2"><p>این به شیب آمده چون نیم‌‌گشوده طومار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مار آن ضحاک آهیخته چون‌گازگراز</p></div>
<div class="m2"><p>مار این ضحاک آمیخته با مشک تتار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کشوری از دم آن مار به تیمار قرین</p></div>
<div class="m2"><p>عالمی با غم این مار بناچار دوچار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر از آن مار شدی خیلی بی‌حد بیهوش</p></div>
<div class="m2"><p>هم از این مار شود خلقی بیمر بیمار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر از آن مار شدی ‌کشته به هر روز دو تن</p></div>
<div class="m2"><p>هم از این مار شود کشته به هر روز هزار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باشد این مار به خون دل عاشق تشنه</p></div>
<div class="m2"><p>آمد آن مار به مغز سر مردم ناهار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ویلک آن ضحاک از چرخ بیاموخت ستم</p></div>
<div class="m2"><p>ویحک ‌این ‌ضحاک‌ از حسن برافروخت ‌شرار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنک آن را ز بزرگان عرب بوده نژاد</p></div>
<div class="m2"><p>اینک این را ز نکویان تتارست تبار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنک آن دشمن جمشید و ربودش افسر</p></div>
<div class="m2"><p>اینک‌ این حاسد خورشید و شکستش بازار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دیدی از فتنهٔ آن اسم‌کیان شد ز میان</p></div>
<div class="m2"><p>بنگر از کینهٔ این جسم‌ کیان رفت ز کار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چیره بر کشور جمشید شد آن یک به سپاه</p></div>
<div class="m2"><p>طعنه بر طلعت خورشید زد این یک به عذار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دو فریدون به جهان نیز برافراخت علم</p></div>
<div class="m2"><p>یکی از دودهٔ جمشید و یکی از قاجار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن فریدون اگرش‌گاو زمین دادی شیر</p></div>
<div class="m2"><p>این فریدون ‌گه ‌کین شیر فلک‌ کرد شکار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن فریدون اگرش کاوه نشاندی به سریر</p></div>
<div class="m2"><p>این فریدون ببرش ‌کاوه نمی‌یابد بار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن فریدون به دماوند اگر برد پناه</p></div>
<div class="m2"><p>این فریدون ز دماند برانگیخت غبار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن فریدون همه جادوگریش بود شیم</p></div>
<div class="m2"><p>این فریدون همه دانشوریش هست شعار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زان فریدون همه‌گوییم به تقلید سخن</p></div>
<div class="m2"><p>زین فریدون همه رانیم به تحقیق آثار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن فریدون شد و این شاه جهانست به نقد</p></div>
<div class="m2"><p>بس همین فرق ‌که این زنده بود آن مردار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آن به عون علم‌کاوه‌گشودی‌کشور</p></div>
<div class="m2"><p>این به نوک قلم خویش‌ گشاید امصار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای فریدون‌شه راد ای ملک ملک‌ستان</p></div>
<div class="m2"><p>که فریدون به بزرگی تو دارد اقرار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو فریدونی و در عرصهٔ‌پیکار ز رمح</p></div>
<div class="m2"><p>بر سر دوش تو ضحاک‌صفت بینم مار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو فریدونی و شمشیر تو ضحاک بود</p></div>
<div class="m2"><p>بسکه بر حال عدو خنده‌ کند در پیکار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو فریدونی و افواج نظام تو به رزم</p></div>
<div class="m2"><p>مارشان بر زبر کتف نماید به قطار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو فریدونی و در عهد تو ضحاک‌صفت</p></div>
<div class="m2"><p>شاهدی پنجه به خون دل ماکرده نگار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو فریدونی و افکنده چو ضحاک به دوش</p></div>
<div class="m2"><p>دو سیه مار به دوران تو ترکی خونخوار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو فریدونی و ضحاک لبی خنداخند</p></div>
<div class="m2"><p>دو سیه مار نماید ز یمین و ز یسار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو فریدونی و اینها همه ضحاک آخر</p></div>
<div class="m2"><p>پرسشی‌گیرکه ضحاک چرا شد بسیار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو خود اول بنه آن نیزهٔ چون مار ز دوش</p></div>
<div class="m2"><p>تات ماری ز کتف برندمد بیور وار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تیغ را نیز بده پند که بسیار مخند</p></div>
<div class="m2"><p>تات زین معنی ضحاک نخوانند احرار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چارهٔ فوج نظام تو ندانم ایراک</p></div>
<div class="m2"><p>چارهٔ آن همه ضحاک نماید دشوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زان‌همه مارکشان‌رسته چو ضحاک به‌دوش</p></div>
<div class="m2"><p>مار زاریست همه بوم و بر و دشت و دیار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>باری این جمله بهل داد دل من بستان</p></div>
<div class="m2"><p>زان دو ماری که بود روز و شبان غالیه‌بار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هوش من چند برد شاهد ضحاک شیم</p></div>
<div class="m2"><p>خون من چند خورد دلبر ضحاک دثار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چند چند از لب ضحاک مرا ریزد خون</p></div>
<div class="m2"><p>چند چند از دل بی‌باک مرا خواهد خوار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گاو سرگرز بکش‌ گردن ضحاک بکوب</p></div>
<div class="m2"><p>تیشهٔ عدل بزن ریشهٔ ضحاک برآر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خون ضحاک بدان صارم خونریز بریز</p></div>
<div class="m2"><p>مغز ضحاک بدان ناوک خونخوار به خار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>موی ضحاک بکش غبغب ضحاک بگیر</p></div>
<div class="m2"><p>همچو آن شیر که ‌گیرد سر آهو به ‌کنار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نی خطاگفتم ای شاه فریدون‌که مرا</p></div>
<div class="m2"><p>وصل آن شاهد بی‌باک بباید ناچار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>این نه ضحاکی‌کز صحبت آن جان غمگین</p></div>
<div class="m2"><p>این نه ضحاکی کز الفت آن دل بیزار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>این نه ضحاکی کز کینهٔ او نفس دژم</p></div>
<div class="m2"><p>این نه ضحاکی‌ کز وی دل و دی‌ را انکار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>این نه ضحاک که او چاکر افریدونست</p></div>
<div class="m2"><p>کاو‌یانی علم افراخته از طرهٔ تار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>من به ضحاک چنین نقد روان‌کرده فدا</p></div>
<div class="m2"><p>من به ضحاک چنین هر دو جهان‌کرده نثار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>این نه ضحاک‌ که او هر شب و هر روز کند</p></div>
<div class="m2"><p>دمبدم از دل و جان مدح فریدون تکرار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دل قاآنی از آن برده و بربسته به زلف</p></div>
<div class="m2"><p>تاش در گوش کند مدح فریدون تکرار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شه به ضحاک چنین به‌که نماید یاری</p></div>
<div class="m2"><p>شه به ضحاک چنین به‌ که فشاند دینار</p></div></div>