---
title: >-
    قصیدهٔ شمارهٔ ۱۷۵ - در مدح آقامحمد حسن پیشخدمت خاصه ی خاقان خلد آشیان
---
# قصیدهٔ شمارهٔ ۱۷۵ - در مدح آقامحمد حسن پیشخدمت خاصه ی خاقان خلد آشیان

<div class="b" id="bn1"><div class="m1"><p>یار نیکوتر از آنست‌ که من دیدم پار</p></div>
<div class="m2"><p>باش تا سال دگر خوبترک گردد یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پار یک بوسه به صد عجز نمی‌داد به من</p></div>
<div class="m2"><p>خود به خود می‌دهد امسال به من بوسه هزار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس‌که بوسیده‌ام امسال لب نازک او</p></div>
<div class="m2"><p>از لبش جای سخن بوسه چکد ازگفتار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پار می‌جست ‌کنار از من و امسال همی</p></div>
<div class="m2"><p>بوسها رشوه دهد تاش در آرم به ‌کنار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زانسوی بوسه مرا کار کشیدست‌ کنون</p></div>
<div class="m2"><p>بس‌ که می‌بینم‌ کز بوسه ندارد انکار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شعر کردست شعار خود و زینرو با من</p></div>
<div class="m2"><p>رام ‌گشتست بدانگونه‌ که ‌گویند اغیار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یارب این آبله رو ابلهک مفلس زشت</p></div>
<div class="m2"><p>بچه تدبیر به شیرین پسران ‌گردد یار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کجا هست غزلگوی غزالی در شهر</p></div>
<div class="m2"><p>پی صیدش همه دم دام نهد از اشعار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لب خوبان مگس نحل و ندیدم جز او</p></div>
<div class="m2"><p>عنکبوتی‌که نماید مگس نحل شکار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راست گویند حکیمان جهان دیده که نیست</p></div>
<div class="m2"><p>لاله بی‌داغ و شکر بی‌مگس وگل بی‌خار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشود شاهد زیبارو جز همدم زشت</p></div>
<div class="m2"><p>نخورد خربزهٔ شیرین الّا کفتار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>الغرض پار اگر یار مرا دادی بوس</p></div>
<div class="m2"><p>از سر خشم یکی را دو همی‌ کرد شمار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وینک امسال چو بر روی و لبش بوسه زنم</p></div>
<div class="m2"><p>شصت را شش شمرد سی را سه چل را چار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هی همی شعر ز من گیرد و هی بوسه دهد</p></div>
<div class="m2"><p>خرم آنکو چو منش شعر فروشیست شعار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرکه یک شعر مرا بیند اندر بر او</p></div>
<div class="m2"><p>حالی‌اندر عوض او دهدش بوسه هزار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کاغذ شعر مرا پار اگر می‌بردند</p></div>
<div class="m2"><p>به یکی کاغذ دارو نخریدی عطار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>لیکن امسال به تقلید بت سادهٔ من</p></div>
<div class="m2"><p>کمترین شعر مرا هست رواج دینار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یار تنها نه چنینست‌ که هر جا صنمی است</p></div>
<div class="m2"><p>از پی شعر و غزل در بر من جوید بار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر پریرو که بدو شعر مرا برخوانی</p></div>
<div class="m2"><p>به تو مشتاق بود چون به ‌گل سرخ هزار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شعر من همچو عزایم شده افسون پری</p></div>
<div class="m2"><p>که پری‌وار کند ساده رخان را احضار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شعر من‌ گر به سر زلف نکویان بندی</p></div>
<div class="m2"><p>با تو آنگونه شود رام ‌که با افسون مار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر کسی شعر من امروز فروشد به سلم</p></div>
<div class="m2"><p>ده دو افزون خرد از نقرهٔ خالص تجار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خادم خانه همی شعر مرا می‌دزدد</p></div>
<div class="m2"><p>کش فروشد عوض سیم و طلا در بازار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هرشب آید بر من دوست چو یک خرمن گل</p></div>
<div class="m2"><p>وز لب خود دهدم قند و شکر یک خروار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من‌کنون ‌کرم قزم آن لب یاقوتی توت</p></div>
<div class="m2"><p>زان خورم توت و ز اشعار تنم هر دم تار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شعر من راست به ابریشم‌ گیلان ماند</p></div>
<div class="m2"><p>که خرندش به‌سلف ‌پیله‌وران در امصار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>غالبآ شعر من اینگونه از آن رایج شد</p></div>
<div class="m2"><p>که پسند افتاد در حضرت مخدو‌م‌ کبار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آن حسن اسم و حسن رسم‌ که‌گویی ز ازل</p></div>
<div class="m2"><p>خلق‌گشتست ز خلق خوش او باد بهار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آنکه یارد ز پی منع حوادث شب و روز</p></div>
<div class="m2"><p>گرد بر گرد جهان را کشد از حزم حصار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ابر نیسان اگر از همت او جوید فیض</p></div>
<div class="m2"><p>عوض‌گل همه یاقوت دمد از گلزار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کف او گویی آتش بود و سیم سپند</p></div>
<div class="m2"><p>زان نگیرد نفسی در بر او سیم قرار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پنج ماهیست به دریای‌ کفش پنج انگشت</p></div>
<div class="m2"><p>گر چه ماهی نشنیدم ‌که بود گوهربار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در سه ماهیش یکی مار بود نامش‌کلک</p></div>
<div class="m2"><p>لیک ماری‌ که از و مشک بود در رفتار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مار دیدی‌ که‌ گهر بارد بر صفحهٔ سیم</p></div>
<div class="m2"><p>یا شنیدی ‌که ‌کند مشک به‌ کافور نثار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مار دیدی‌که فشاند به دل زهر شکر</p></div>
<div class="m2"><p>یا خورد در عوض خاک سیه مشک تتار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مار دیدستی چون نحل فرو ریزد شهد</p></div>
<div class="m2"><p>مار دیدستی چون نخل رطب آرد بار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نی نه مارس یکی طوطی شکر شکنست</p></div>
<div class="m2"><p>زان دمادم به سوی هند پرد طوطی‌وار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>طوطی ار پرّش سبزستی و منقارش سرخ</p></div>
<div class="m2"><p>او بود طوطی زرین پر مشکین منقار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>عنبر آرد اگر از بحر کفش نیست عجب</p></div>
<div class="m2"><p>عنبر آرند بلی مردم از دریا بار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای ‌که ‌گر آیت حزم تو بر اعدا بدمند</p></div>
<div class="m2"><p>در نهانخانهٔ تقدیر ببینند اسرار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا که‌ کالای وجود تو به بازار آمد</p></div>
<div class="m2"><p>آسمان بر در دکان عدم زد مسمار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کلک سحار تو چون شعر نویسدگویی</p></div>
<div class="m2"><p>صورت روح‌ کند بر پر جبریل نگار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر تو گویی نبی استم من و شعرم معجز</p></div>
<div class="m2"><p>بر به پیغمبریت من‌ کنم اوّل اقرار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>عوض ‌کوزه همه جام جم آرد بیرون</p></div>
<div class="m2"><p>گر مثل‌کوزه‌یی از فخر تو سازد فخار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>صاحبا خواستم از شاه تیولی در فارس</p></div>
<div class="m2"><p>پیش از آنی‌ که به شیراز ز ری بندم بار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شاه فرمود تیول تو بود ملک سخن</p></div>
<div class="m2"><p>مر ترا همچو رعیت شعرا باج‌گزار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چه تیولست ازین به ‌که محوّل داریم</p></div>
<div class="m2"><p>وجه مرسوم تو بر صنفی از اصناف دیار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از قضا زنده بد آن روز مهین مستوفی</p></div>
<div class="m2"><p>کش بیامرزاد از فضل فراوان دادار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گفت آن به‌که به قصابانش فرمان بدهیم</p></div>
<div class="m2"><p>تا همی چرب زبانتر شود اندر اشعار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شاه پذرفت و از آن پس‌ که ‌گرفتم فرمان</p></div>
<div class="m2"><p>از پی آمدن فارس ز شه جستم بار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چون به شیراز رسیدم در هرجایی من</p></div>
<div class="m2"><p>گشت مایل به بتی سنگدلی سیم عذار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دلبری ساده ‌که بد موی سیه بر رویش</p></div>
<div class="m2"><p>چون یکی دستهٔ سنبل‌ که دمد از گلنار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>لب او با همه گلشکر و گلقند که داشت</p></div>
<div class="m2"><p>در شگفتم‌ که چرا بود دو چشمش بیمار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جز خطش در شکن زلف ندیدم‌که روند</p></div>
<div class="m2"><p>فوجی از مورچگان در شب تاری به قطار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>جز رخش در خم‌گیسو نشنیدم‌ که ‌کسی</p></div>
<div class="m2"><p>روز رخشنده‌ کند تعبیه اندر شب تار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>اطلسی جز رخ زیباش ندیدم همه عمر</p></div>
<div class="m2"><p>کز ملاحت بودش پود وز نیکویی تار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>زلف پیچانش طومار صفت خم در خم</p></div>
<div class="m2"><p>ثبت ‌کرده غم دلها همه در آن طومار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>الغرض از پی مرسوم نرفتم دیگر</p></div>
<div class="m2"><p>زانکه دیوانهٔ خوبان نرود از پی‌کار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>لیکن امسال‌که شدکیسه ام از زر خالی</p></div>
<div class="m2"><p>من شدم بی‌زر و مهروی من از من بیزار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سرو گلچهرهٔ من غنچه صفت شد دلتنگ</p></div>
<div class="m2"><p>تا شد ازسیم تهی پنجهٔ من همچو چنار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خویش را گفتم لاقیدی و رندی تاکی</p></div>
<div class="m2"><p>زین محبت بگذر انده و محنت بگذار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چون حوالت شده مرسوم تو بر میش ‌‌کشان</p></div>
<div class="m2"><p>اینک امضا را شو خویش‌کشان زی سالار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>خویشتن در عوض میش فدا کن بر میر</p></div>
<div class="m2"><p>تا مگر از کرم میر شوی برخوردار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ناظم‌کشور جم میر عجم شیر اجم</p></div>
<div class="m2"><p>خصم یم ‌کان همم بحر کرم ‌کوه وقار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>رفتم وگفتم و پذرفت و هماندم فرمود</p></div>
<div class="m2"><p>به مهین منشی عبدالله توقیع نگار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>که ز قاآنی فرمان مبارک بستان</p></div>
<div class="m2"><p>بهمان نوع‌ که خواهد دلش امضا میدار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>او قلم قط زد و زانو زد و فرفر بنوشت</p></div>
<div class="m2"><p>نامه‌یی چون پر طاووس پر از نقش و نگار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>برد زی میرش و زد مهر وز مهر آمد و داد</p></div>
<div class="m2"><p>زود بگرفتم و بوسیدمش از جان صدبار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>لیک بازم زعنا بار گرانیست بدل</p></div>
<div class="m2"><p>باری از یاری تو بو که سبک ‌گردد بار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>عشر آن راتبه هر سال‌کند کم دیوان</p></div>
<div class="m2"><p>هست از آن کم شدنم بر دل رنجی بسیار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>دارم امیدکه بخشد به تو آن عشر امیر</p></div>
<div class="m2"><p>تو به من بخشی و من نیز به طفلان صغار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خواهش دیگرم آنست ‌که آن امضا را</p></div>
<div class="m2"><p>میر از خامهٔ خود زیب‌ دهد چون فرخار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به خط خویش نماید به کلانتر مرقوم</p></div>
<div class="m2"><p>که تو مرسوم فلان را بده و عذر میار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بدو قسط اول سال آن را از میش کشان</p></div>
<div class="m2"><p>بستان وجه بکن سعی و محصل بگمار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>هم بدینسان بدهش نقد به هر سال دگر</p></div>
<div class="m2"><p>تا کند از دل و جان مدح شهنشاه شعار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>هم مرا بود بهر ساله ز شه انعامی</p></div>
<div class="m2"><p>که نه امسال رسیدست و نه پیرار و نه پار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>میر فرمود تو بنویسی و خود بنویسد</p></div>
<div class="m2"><p>نامه‌یی چند به دربار شه شیرشکار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>تا مگر عاطفت خواجهٔ اعظم‌ گردد</p></div>
<div class="m2"><p>مر مرا یمن یمینش سبب یسر یسار</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بر به مرسوم من انعام من افزوده شود</p></div>
<div class="m2"><p>تنم از رنج شود ایمن و جان از تیمار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>یا مرخص‌ کندم میر که در خدمت تو</p></div>
<div class="m2"><p>به ری آیم مگرم‌ کار شود همچو نگار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>این سه‌ کار ار شود از لطف عمیم تو درست</p></div>
<div class="m2"><p>به سر و جان تو کز چرخ برین دارم عار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>هیچ دانی چکنم مختصری شرح دهم</p></div>
<div class="m2"><p>تا ز طول سخنت می‌نشود طبع فکار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بخرم خانئکی همچو یکی باغ بهشت</p></div>
<div class="m2"><p>صورت ساده‌ رخان نقش‌کنم بر دیوار</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>شاهدی غضبان گیرم ‌که زند سیلی و مشت</p></div>
<div class="m2"><p>نه‌که هرلحظه‌گشاید ز میان بند ازار</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>گلرخ و سرو قد و لاله لب و نسرین بر</p></div>
<div class="m2"><p>دلکش ومهو‌ش مشکین خط و سیمین رخسار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>لب میگونش چو بر مه نقطی از شنگرف</p></div>
<div class="m2"><p>گرد آن نقطه خطش دایره‌یی از زنگار</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>همه اسباب طرب‌ گرد کنم در خانه</p></div>
<div class="m2"><p>از می و بربط و رود و نی و عود و دف و تار</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>صد خم‌ کهنه ستانم همه قیر اندوده</p></div>
<div class="m2"><p>قرب صد خروار انگور خرم از خلار</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>آنگه انگورکنم دانه و ریزم در خم</p></div>
<div class="m2"><p>هی همی لب زنمش بیگه وگه لیل و نهار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>تا بدان گه ‌که چو دیوانه ‌کف آرد بر لب</p></div>
<div class="m2"><p>و آب انگور شود سرخ‌تر از آب انار</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>زان شوم مست بدانگونه‌که در بیداری</p></div>
<div class="m2"><p>می ندانم‌که به شیراز درم یا بلغار</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>هر زمانی‌که خورم باده به یاد تو خورم</p></div>
<div class="m2"><p>هم به‌جای تو زنم بوسه به رخسار نگار</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>هی زنم‌ ساغر و هی بوسه‌ زنم بر رخ‌ دوست</p></div>
<div class="m2"><p>هی خورم باده و هی نقل خورم از لب یار</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بر سر تخت سرینتث‌ن بکشم هرشب رخت</p></div>
<div class="m2"><p>هم بدانسان ‌که رود کبک دری بر کهسار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>تا خدایم به صف حشر بیامرزد جرم</p></div>
<div class="m2"><p>همه مدح تو کنم در عوض استغفار</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>سال عمر تو چو تضعیف بیوت شطرنج</p></div>
<div class="m2"><p>باد چندانکه به صد جهد درآید به شمار</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>فرخی گرچه بدین وزن و قوافی گفته</p></div>
<div class="m2"><p>شهر غزنین نه‌همانست‌ که‌ من دیدم پار</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>لیک بر تربتش این شعر کس ار بر خواند</p></div>
<div class="m2"><p>آفرین گوید و از وجد بجنبد به مزار</p></div></div>