---
title: >-
    قصیدهٔ شمارهٔ ۲۴ - در تهنیت نظام‌الدوله هنگام آوردن خلعت شاهنشاه غازی در هنگام ولیعهدی
---
# قصیدهٔ شمارهٔ ۲۴ - در تهنیت نظام‌الدوله هنگام آوردن خلعت شاهنشاه غازی در هنگام ولیعهدی

<div class="b" id="bn1"><div class="m1"><p>صبحدم کز جانب مشرق برآمد آفتاب</p></div>
<div class="m2"><p>همچو بخت پادشه بیدار شد چشمم ز خواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی ناشسته ز دم جامی مئی کز بوی او</p></div>
<div class="m2"><p>تا لب گور آید از لبهای من بوی شراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان مئی کز جام کیخسرو جهان‌بین‌تر شود</p></div>
<div class="m2"><p>گر چکد یک قطره درکاسهٔ سر افراسیاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دماغم تر شد از می دیدم ازطرف شمال</p></div>
<div class="m2"><p>تافت خورشیدی که شد خورشید زو در احتجاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم مالیدم که مستم یا به خوابستم هنوز</p></div>
<div class="m2"><p>واندرین معنی دلم در شبهه جان در ارتیاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاه میگفتم که خورشید است گردون راز اصل</p></div>
<div class="m2"><p>باز می‌گفتم نه حاشا انه شیئی عجاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز میگفتم شنیدستم ز مستان پیش ازین</p></div>
<div class="m2"><p>کادمی یک را دو بیند چون فزون نوشد شراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من درین حیرت که آمد ماه من ناگه ز در</p></div>
<div class="m2"><p>با دو چشمی همچو حال عاشقان مست و خراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سر هر موی مژگانش دوصد ترکش خدنگ</p></div>
<div class="m2"><p>در خم هر تار گیسویش دو صد چین مشک ناب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روی او را صد خزینه حسن در هر آب و رنگ</p></div>
<div class="m2"><p>موی او را صد صحیفه سحر در هر پیچ و تاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آب روی و تاب موی برد آب و تاب من</p></div>
<div class="m2"><p>این ز جانم برده آب و آن ز جسمم برده تاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چهرش اندر زلف حوری خفته در دامان دیو</p></div>
<div class="m2"><p>یا حواصل بچهیی آسوده در پر غراب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حرمت گیسو و چشمش را بر آنستم که نیست</p></div>
<div class="m2"><p>هیچ کافر را عذاب و هیچ ساحر را عقاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون مرا زان گونه پژمان دید غژمان شد ز خشم</p></div>
<div class="m2"><p>چنگ پیش آورد تاگوشم بمالد چون رباب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتم ای غلمان دنیا ای بهشت خاکیان</p></div>
<div class="m2"><p>ای ستارهٔ نازپرور ای فرشتهٔ بی‌نقاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای دو رنگین عارضت دارالخلافهٔ دلبری</p></div>
<div class="m2"><p>وی دو مشکین طره‌ات دارالامارهٔ ماهتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مهر نورافروز امروزم دومی آید به چشم</p></div>
<div class="m2"><p>من درین احوال حیران‌کاحولستم یا مُصاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آفتابی از شمال آید به چشمم جلوه‌گر</p></div>
<div class="m2"><p>وافتابی دیگر اندر مشرق از وی نور تاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نرم نرمک خنده‌یی فرمود و برقع برگشود</p></div>
<div class="m2"><p>گفت ما را هم نظرکن تا سه بینی آفتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفتم از حال تو و خورشید گردون واقفم</p></div>
<div class="m2"><p>اینک این خورشید دیگر چیست گفتا در جواب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آفتابی ‌کز شمال پارس بینی جلوه‌گر</p></div>
<div class="m2"><p>هست تشریف ولیعهد شه مالک رقاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بوالمظفر ناصرالدّین‌ کز نسیم عفو او</p></div>
<div class="m2"><p>در دهان مار تریاق اجل‌گردد لعاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفتم آن تشریف آرند ازکجاگفتا ز ری</p></div>
<div class="m2"><p>گفتم از بهرکه‌گفت از بهر میرکامیاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جانفشان سرباز شاهنشه حسین‌خان آنکه هست</p></div>
<div class="m2"><p>ناخن و تیغش ز خون دشمنان شه خضاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفتم از سعی‌که صاحب اختیار ملک جم</p></div>
<div class="m2"><p>شد چنین وافر نصیب و شد چنان‌کامل نصاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفت از فضل عمیم خواجهٔ اعظم‌ که هست</p></div>
<div class="m2"><p>هرچه در هستی قشور و جسم و جان اولباب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفتم آیا تهنیت را هیچ‌گویم‌گفت نه</p></div>
<div class="m2"><p>گفت من خوشتر که دوشم زآسمان آمد خطاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کز برای تهنیت فردا ز قول قدسیان</p></div>
<div class="m2"><p>در حضور میر برخوان این قصیدهٔ مستطاب</p></div></div>