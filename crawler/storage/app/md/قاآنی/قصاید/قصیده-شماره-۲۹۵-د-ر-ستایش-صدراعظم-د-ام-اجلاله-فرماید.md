---
title: >-
    قصیدهٔ شمارهٔ ۲۹۵ - د‌ر ستایش صدراعظم د‌ام اجلاله فرماید
---
# قصیدهٔ شمارهٔ ۲۹۵ - د‌ر ستایش صدراعظم د‌ام اجلاله فرماید

<div class="b" id="bn1"><div class="m1"><p>به راغ و باغ گذرکرد ابر فروردین</p></div>
<div class="m2"><p>شراره ریخت بر آن و ستاره ریخت براین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن شراره همه راغ گشت پر لاله</p></div>
<div class="m2"><p>وزین ستاره همه باغ‌گشت پر نسرین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چمن از آن شده پرنور وادی ایمن</p></div>
<div class="m2"><p>دمن ازین شده پر نار آذر برزین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر چمن زگل آتش‌ گرفت ‌کز باران</p></div>
<div class="m2"><p>زند بر آتش آن آب ابر فروردین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین بهار مرا شیر گیر آهو کی است</p></div>
<div class="m2"><p>گوزن چشم و پلنگینه خشم وگور سرین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان عقل و جنون داده عشق او پدید</p></div>
<div class="m2"><p>میان چشم و نظرکرده حسن او تفتین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دو طٌره‌اش چو دو برگشته چنگل شهباز</p></div>
<div class="m2"><p>دو مژه‌اش چو دوگیرنده پنجهٔ شاهین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قدش به قاعده موزون نه ‌کوته و نه بلند</p></div>
<div class="m2"><p>تنش به حد متناسب نه لاغر و نه سمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوچشم زیر دوابرو و دوخال زیر دوچشم</p></div>
<div class="m2"><p>گمان بری ‌که همی در نگارخانهٔ چین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دو ترک خفته و در زیر سر نهان ‌کمان</p></div>
<div class="m2"><p>دو بچه هندو ی بیدار هردو را به‌ کمین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شب ‌گذشته ‌کز آیینه پارهای نجوم</p></div>
<div class="m2"><p>سیه عماری شب را سپهر بست آیین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رسید بی‌خبر از راه و من ز رنج رمد</p></div>
<div class="m2"><p>به چهره بسته نقابی چو زلف او مشکین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دو عبهرم شده از خون دو لالهٔ نعمان</p></div>
<div class="m2"><p>دمیده از بر هر لاله یک چمن نسرین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شده دو جزع یمانی دو لعل و از هریک</p></div>
<div class="m2"><p>چکیده ز اشک‌ روان خوشه‌خوشه درّ ثمین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ندیده طلعت او دیدم از جوارح من</p></div>
<div class="m2"><p>ز هرکرانه همی خاست نالهای حزین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مژه به چشمم همی خار زد که ها بنگر</p></div>
<div class="m2"><p>جنون به مغزم هی بانگ زد که ها منشین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز جای جستم و با صد تعب‌ گشودم چشم</p></div>
<div class="m2"><p>رخی معاینه دیدم به از بهشت برین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شعاع نور جبینش ز سطح خاک نژند</p></div>
<div class="m2"><p>رسیده تا فلک زهره همچو ظلّ زمین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به‌ کف بطی ز میش لعل رنگ و مشکین بوی</p></div>
<div class="m2"><p>بسان آتش موسی به آب خضر عجین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از ‌آن شراب‌ که با نور او توان دیدن</p></div>
<div class="m2"><p>نزاده در شکم مادر آرزوی جنین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه دید دید مرا همچو باز دوخته چشم</p></div>
<div class="m2"><p>دو لاله‌ گشته عیان از دو نرگس مسکین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چه‌ گفت‌ گفت‌ که ای آسمان فضل و هنر</p></div>
<div class="m2"><p>ز فرقدین تو چندین چرا چکد پروین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه سوزی این همه نارت که ریخت بر بستر</p></div>
<div class="m2"><p>چه پیچی این همه مارت ‌که هشت بر بالین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مگر خیال سر زلف من نمودی دوش</p></div>
<div class="m2"><p>که‌بر تنت همه‌تابست و بر رخت همه چین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگفتمش به شبی ‌کابر پیلگون از برف</p></div>
<div class="m2"><p>همی فشاند ز خرطوم پنبهٔ سیمین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز بس که سودهٔ کافور بر زمانه فشاند</p></div>
<div class="m2"><p>زمین ز حمل سترون شد آسمان عنین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به چشم من دو سه الماس سوده ریخت ز برف</p></div>
<div class="m2"><p>سحرگهان ‌که ز مشرق وزید باد بزین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز درد چشم چنانم‌کنون‌که پنداری</p></div>
<div class="m2"><p>به چشم من مژه از خشم می‌زند زوبین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو این شنید ز جا جست و نام خواجه دمید</p></div>
<div class="m2"><p>بهر دو چشمم و پذرفت درد من تسکین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فروغ چشم معالی نظام ملت و ملک</p></div>
<div class="m2"><p>جمال چهر مکارم قوام دولت و دین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خدایگان امم صدراعظم ابر کرم</p></div>
<div class="m2"><p>که صدر بدر نشانست و بدر صدر نشین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به یک نفس همه انفاس خلق را شمرد</p></div>
<div class="m2"><p>ز صبح روز ازل تا به شام بازپسین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به یک نظر همه اسرار دهر را نگرد</p></div>
<div class="m2"><p>ز اولین دم ایجاد تا به یوم‌الدین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زهی ز یمن یمینت زمانه برده یسار</p></div>
<div class="m2"><p>خهی به یسر یسارت ستاره خورده یمین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مداد خامهٔ تو خال چهر روح‌القدس</p></div>
<div class="m2"><p>سواد نامهٔ توکحل چشم حورالعین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز بهر پاس ممالک به عون عزم قوی</p></div>
<div class="m2"><p>برای امن مسالک به یمن رای رزین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز بال پشّه نهی پیش باد سد سدید</p></div>
<div class="m2"><p>ز نار تفته‌کشی‌گرد آب حصن حصین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ستاره با همه رفعت ترا برد سجده</p></div>
<div class="m2"><p>زمانه با همه قدرت تراکند تمکین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از آن زمان‌که مکان و مکین شدند ایجاد</p></div>
<div class="m2"><p>ندید هیچ مکان چون تو در زمانه مکین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو جزو عالمی و به ز عالمی چون آن</p></div>
<div class="m2"><p>که جزو خاتم و هم به زخاتمست نگین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به نور رای تو ناگشته نطفه خون به رحم</p></div>
<div class="m2"><p>توان نمود معین بنات را ز بنین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پی فزونی عمر تو دهر باز آرد</p></div>
<div class="m2"><p>هرآنچه رفته ازین پیش از شهور و سنین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز بیم عدل تو نقاش را بلرزد دست</p></div>
<div class="m2"><p>کشد چو نقش‌کبوتر به پنجهٔ شاهین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در آفرینش عالم تو ز آن عزیزتری</p></div>
<div class="m2"><p>که در میان بیابان تموز ماء مین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>وجود را نبد ار ذات چون تویی زیور</p></div>
<div class="m2"><p>هزار مرتبه کردی عدم بر او نفرین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زمین به قوت حکم تو حکمران سپهر</p></div>
<div class="m2"><p>گمان بیاری رای تو اوستاد یقین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خزان‌گلشن تو نوبهار باغ بهشت</p></div>
<div class="m2"><p>زمین درگه تو آسمان چرخ برین</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گرت هزار ملامت‌ کند حسود عنود</p></div>
<div class="m2"><p>بدو نگیری خشم و بدو نورزی‌کین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از آنکه پایهٔ سیمرغ از آن رفیع‌تر است</p></div>
<div class="m2"><p>که التفات ‌کند گر کشد ذباب طنین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به‌ کفهٔ‌ کرمت چرخ و خاک همسنگند</p></div>
<div class="m2"><p>اگر چه آن یک بالا فتاده این پایین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بلند و پستی دو کفه را مکن مقیاس</p></div>
<div class="m2"><p>بدان نگرکه همی راست ایستد شاهین</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شنیده بودم مارست‌کاژدهاگردد</p></div>
<div class="m2"><p>چو چند قرن بگردد بر او سپهر برین</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز خامهٔ تو شد این حرف مر مرا باور</p></div>
<div class="m2"><p>از آنکه خامهٔ تو مار بود شد تنین</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به‌حکم آنکه چوثعبان موسوی نگذاشت</p></div>
<div class="m2"><p>به هیچ رو اثر از سحر ساحران لعین</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>برون ز ربقهٔ حکم تونیست خشک و تری</p></div>
<div class="m2"><p>درست شدکه تویی معنی‌کتاب مبین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همیشه تا نشود جهل با خرد همسر</p></div>
<div class="m2"><p>هماره تا نبود زهر چون شکر شیرین</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خرد به روی تو مجنون چو قیس از لیلی</p></div>
<div class="m2"><p>هنر ز شور تو شیدا چو خسرو از شیرین</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کف ‌گشاده روانت ستوده جان بی‌غم</p></div>
<div class="m2"><p>دلت شکفته تنت بی‌گزنده و بخت سیمین</p></div></div>