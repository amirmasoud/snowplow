---
title: >-
    قصیدهٔ شمارهٔ ۲۷۳ - در ستایش امیرالامراء العظام حسین خان نظام‌الدوله فرماید
---
# قصیدهٔ شمارهٔ ۲۷۳ - در ستایش امیرالامراء العظام حسین خان نظام‌الدوله فرماید

<div class="b" id="bn1"><div class="m1"><p>دوش اندر خواب دیدم بر قد سروی جوان</p></div>
<div class="m2"><p>سایه‌ گستر گشت خورشید از فراز آسمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با معبّر صبح چون گفتم بگفت از ملک ری</p></div>
<div class="m2"><p>شه فرستد خلعتی از بهر سالار زمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسمان ملک ریست و آفتابش پادشاه</p></div>
<div class="m2"><p>سایه تشریف ملک سرو جوان صدر جهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما درین صحبت که ناگه از در آمد ماه من</p></div>
<div class="m2"><p>با لبی همرنگ خون و با تنی همسنگ جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلشن چهرش شکفته فرودین‌ در فرودین‌</p></div>
<div class="m2"><p>سبزهٔ خطش دمیده بوستان در بوستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جستم و بگرفتم و تنگش‌ کشیدم در بغل</p></div>
<div class="m2"><p>بر شمار چین زلفش بوسه دادم بر دهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لاجرم چون چین زلفش بوسه‌ام شد بیشمار</p></div>
<div class="m2"><p>آری آری چین زلفش را شمردن کی توان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد ز عکس چهرهٔ او چشم من پر آفتاب</p></div>
<div class="m2"><p>شد ز بوی طرهٔ او مغز من پر ضیمران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سرای من ز قدش رست گفتی نارون</p></div>
<div class="m2"><p>وز دو چشم من ز لعلش ریخت گفتی ناردان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زلف او بوییدم و هی عطسه کردم بیشمار</p></div>
<div class="m2"><p>لعل او بوسیدم و هی نکته‌ گفتم دلستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گشت در موی میانش عقل من باریک‌بین</p></div>
<div class="m2"><p>عقل و من مانند مویی هر دو رفتیم از میان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بوسه دادن بر دهانش غصه را زایل ‌کند</p></div>
<div class="m2"><p>آری آری‌کرده‌ام این نکته را من امتحان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>راستی را حیرت آوردم چو دیدم قد او</p></div>
<div class="m2"><p>زانکه بر سرو روان هرگز ندیدم گلستان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا ندیدم بردمد از شاخ طوبایی بهشت</p></div>
<div class="m2"><p>یا ندیدم بشکفد بر شاخ شمشاد ارغوان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در دندان در دهان او چو در عمان‌ گهر</p></div>
<div class="m2"><p>زلف تاری بر رخان او چو بر آتش دخان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت قاآنی ترا گر مژده‌یی نیکو دهم</p></div>
<div class="m2"><p>مژدگانی را چه خواهی داد گفتم نقد جان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت فردا بهر صاحب‌اختیار ملک جم</p></div>
<div class="m2"><p>خلعتی فرخنده آید از خدیو کامران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خلعتی چون زیور انجم بر اندام سپهر</p></div>
<div class="m2"><p>خلعتی چون جامهٔ هستی به بالای جهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خلعتی همچون لباس آفرینش بی‌قصور</p></div>
<div class="m2"><p>خلعتی همچون بساط آسمان گوهر نشان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خلعتی در روشنی چون پرتو نور ازل</p></div>
<div class="m2"><p>خلعتی از نیکویی چون طلعت حور جنان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شمسهٔ الماس آن چون بنگری‌‌ گویی همی</p></div>
<div class="m2"><p>شمس خود را تعبیه‌ کردست در وی آسمان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفتم آن خلعت مبارک باد بر میر عجم</p></div>
<div class="m2"><p>بدر دین صدر هدی غیث زمین غوث زمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آسمان رفعت و شوکت حسین خان آنکه هست</p></div>
<div class="m2"><p>تیغ او جوهرنشان و دست اوگوهرفشان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن فلک قدر و ملک صدری که با یکران اوست</p></div>
<div class="m2"><p>بخت و دولت همرکاب و فتح و نصرت همعنان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>راستی را دوست دارد آنقدر کاندر وغا</p></div>
<div class="m2"><p>با سنان‌و تیر جنگ آرد نه با تیغ وکمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فتنه‌یی گر هست در عهدش منم در شاعری</p></div>
<div class="m2"><p>با دو چشم‌ دوست کان هم‌ هست‌ درخواب گران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جزکتاب نثر من‌کانرا پریشانست نام</p></div>
<div class="m2"><p>در به عهد او نماندست از پریشانی نشان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رزق و مرگ عالم از تیغ و قدح در دست اوست</p></div>
<div class="m2"><p>روز رزم و بزم وین راکرده‌ام بس امتحان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زانکه چون تیغ و قدح بگرفت گاه رزم و بزم</p></div>
<div class="m2"><p>آید از این‌رزق‌مردم زاید از آن‌مرک جان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سرورا صدرا بزرگا داورا فرماندها</p></div>
<div class="m2"><p>ای‌که از آن برتری‌کاو صافت آید در گمان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا چه ‌کردستی ‌که ‌هر روزت برافرازد خدای</p></div>
<div class="m2"><p>بسی نیاید کت بساید سر به فرق فرقدان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خواس‌ یزدان ‌کت ‌کند در صورت ‌و معنی ‌بلند</p></div>
<div class="m2"><p>زان به قد سرو روانی وز شرف روح روان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گاه تعریفت نماید شهریار بی‌قرین</p></div>
<div class="m2"><p>گاه تشریفت فرستد خسرو صاحبقران</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آصف عهدت گهی مهر سلیمانی دهد</p></div>
<div class="m2"><p>تا شوی زان مهر در ملک سلیمان‌کامران</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مهر او شد از شرف مَهر عروس بخت تو</p></div>
<div class="m2"><p>وه چه مهری وه چه مَهری مِهرها در وی نهان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تاکه از سیار و ثابت هست در آفاق نام</p></div>
<div class="m2"><p>باد در آفاق عمرت ثابت و امرت روان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هم بنالد بدسگالت هم ببالد چاکرت</p></div>
<div class="m2"><p>تا بنالد ارغنون و تا ببالد ارغوان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جاودان تا جلوهٔ هستی بماند برقرار</p></div>
<div class="m2"><p>در جهان چون جلو‌هٔ هستی بمانی جاودان</p></div></div>