---
title: >-
    قصیدهٔ شمارهٔ ۲۴۱ - مطلع ثانی
---
# قصیدهٔ شمارهٔ ۲۴۱ - مطلع ثانی

<div class="b" id="bn1"><div class="m1"><p>منم‌ که ازکف زربخش آفت‌ کانم</p></div>
<div class="m2"><p>جهان عزّ و علا را چهار ارکانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به وقعه پیلم وکوبنده گرز خرطومم</p></div>
<div class="m2"><p>به‌ کینه شیرم و درنده تیغ دندانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمانه چنبری از تاب خورده فتراکم</p></div>
<div class="m2"><p>ستاره جوهری از آب داده پیکانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زره شود سپر آسمان ز شمشیرم</p></div>
<div class="m2"><p>قبا شود کمر کهکشان زکیوانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمانه گسسته طنابی به میخ خرگاهم</p></div>
<div class="m2"><p>زمین شکسته‌ کلوخی به خاک ایوانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بزم عشرت رودک ز نیست ناهیدم</p></div>
<div class="m2"><p>به بام شوکت چوبک ز نیست‌کیوانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکدرست ضمیر از نیاز فغفورم</p></div>
<div class="m2"><p>مجدرست زمین از نماز خاقانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو عزم رزم‌کنم ضیغم زره‌پوشم</p></div>
<div class="m2"><p>چو رای بزم کنم قلزم سخندانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به روز قهر اجل را رواج بازارم</p></div>
<div class="m2"><p>به‌گاه مهر امل راکساد دکانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خوان فضل چو از آستین برآرم دست</p></div>
<div class="m2"><p>کمینه لقمه بود صدهزار لقمانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به‌گاه نظم چو از ابر،‌خامه پاشم آب</p></div>
<div class="m2"><p>کهینه قطره بود صدهزار قطرانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درون درع چو در آب عکس خورشیدم</p></div>
<div class="m2"><p>فراز رخش چو برکوه ابر نیسانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به باغ لاله و ریحان‌ گرم بجوشد مهر</p></div>
<div class="m2"><p>مصاف باغ و سنان لاله تیغ ریحانم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به آب و سبزه و بستان‌گرم بجنبد دل</p></div>
<div class="m2"><p>خدنگ آب و خسک سبزه دشت بستانم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شمامه‌یی بود از بویِ خلق فردوسم</p></div>
<div class="m2"><p>شراره‌یی بود از تف تیغ نیرانم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به‌گرد رزم چو در زنگبار خورشیدم</p></div>
<div class="m2"><p>به پشت رخش چو بر بوقبیس عمّانم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>محیط قهرم و شمشیر وگرز امواجم</p></div>
<div class="m2"><p>سحاب‌کینم وکوپال و تیغ بارانم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به تیغ شیر شکر ملک را پرستارم</p></div>
<div class="m2"><p>به رمح مارصفت ‌گنج را نگهبانم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شدس پرّ مگس همچو پر طوطی سبز</p></div>
<div class="m2"><p>ز رنگ زهرهٔ گرگان دشت گرگانم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هنوز از دلم الماس زمردین‌ گوهر</p></div>
<div class="m2"><p>ز خون خصم چکد لخت لخت مرجانم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هنوز تیغ درخشان من به خود نازد</p></div>
<div class="m2"><p>که من ز خون عدو معدن بدخشانم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مراست عرضی شاها که‌ گر قبول افتد</p></div>
<div class="m2"><p>دهد بهار امل بار شاخ حرمانم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دو هفته رفت‌که ازفاقه در قلمرو فارس</p></div>
<div class="m2"><p>نژند و خوار چو مصحف به‌کافرستانم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از آنکه زلف پریشان به طبع دارم دوست</p></div>
<div class="m2"><p>چو زلف دوست پریشان شدست سامانم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>علی‌الخصوص ‌که در فرق می‌بتوفد مغز</p></div>
<div class="m2"><p>ز شوق حضرت فرمانروای ایرانم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بجز اراده مرا نیست ساز و برگ سفر</p></div>
<div class="m2"><p>به ساز و برگ چنین طیّ راه نتوانم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گرم وظیفهٔ امساله التفات رود</p></div>
<div class="m2"><p>ز شوق بر دو جهان آستین برافشانم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنان به شکر تو گویا شوم ‌که گ‌‌ویی چرخ</p></div>
<div class="m2"><p>نموده تعبیه بر لب هزاردستانم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شها چو سیم و زرم بیش ازین نژند مدار</p></div>
<div class="m2"><p>چه جرم‌ کرده‌ام آخر چه بوده عصیانم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به من ستم چه‌کنی خسروا نه من سیمم</p></div>
<div class="m2"><p>ز من چه‌کینه‌کشی داورا نه من‌ کانم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به دولت تو که نه من پسر عم اینم</p></div>
<div class="m2"><p>به افسر توکه نه من برادر آنم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه آسمانم چندین مساز پامالم</p></div>
<div class="m2"><p>نه روزگارم چندین مخواه خسرانم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه همچو صبح ز دستم به پیش رای تو لاف</p></div>
<div class="m2"><p>که تا ز دست سخط بردری‌گریبانم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دوام عمر تو چندانکه آسمان‌گوید</p></div>
<div class="m2"><p>مدار عمر سر آمد به امر یزدانم</p></div></div>