---
title: >-
    قصیدهٔ شمارهٔ ۱۱۶ - در ستایش پا‌دشاه جمجاه محمدشاه غازی و فتح خوارزم‌ گوید
---
# قصیدهٔ شمارهٔ ۱۱۶ - در ستایش پا‌دشاه جمجاه محمدشاه غازی و فتح خوارزم‌ گوید

<div class="b" id="bn1"><div class="m1"><p>رسید؛ چه‌؟ خبر فتح؛ کی رسید؟ سحر</p></div>
<div class="m2"><p>کجا؟ به نزد مالک؛ از چه ملک؟ از خاور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خبر چه بود؟ شکست عدو؛ که‌ گفت‌؟ بشیر</p></div>
<div class="m2"><p>عدو شکست چِسان خورده؟ ‌گشت زیر و زبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مصافگاه ‌کجا بود؟ ساحت بسطام</p></div>
<div class="m2"><p>که بر شکست عدو را؟ سمی بن آزر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر که ناصر او بود؟ نصرت‌الدوله</p></div>
<div class="m2"><p>چه بود منصبش از شه‌؟ امارت لشکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کدام لشکر؟ آن لشکری‌ که رفت زری</p></div>
<div class="m2"><p>کجا؟ به طوس‌، چرا؟ بهر نظم آن‌ کشور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپاه را که فرستاد؟ خواجه، ‌‌کی‌؟ شعبان</p></div>
<div class="m2"><p>کدام خواجه‌؟ مهین خواجهٔ عطاگستر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دگر سپاه فرستد؟ بلی؛ چه مه‌؟ شوال</p></div>
<div class="m2"><p>چه روز؟ عید؛ کی آن روز می‌رسد؟ ایدر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گذشت روزه‌؟ بلی؛ ماه نو نمود؟ نعم</p></div>
<div class="m2"><p>چه‌وقت‌؟ دوش؛ کجا؟ در جنوب ‌چون لاغر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون چه باید؟ ساغر؛ چگونه باید؟ پر</p></div>
<div class="m2"><p>پر از چه باشد؟ از می؛ چه می‌؟ می خلر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قدح چه باشد؟ نقره؛ چه نقره‌؟ نقرهٔ خام</p></div>
<div class="m2"><p>قدح‌گسار که‌؟ ترکی؛ چگونه‌؟ سیمین‌بر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قدح به یاد که بخشد؟ به یاد روی ملک</p></div>
<div class="m2"><p>قدح نخست که نوشد؟ حکیم دانشور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرآن حکیم‌ که باشد؟ حکیم قاآنی</p></div>
<div class="m2"><p>چو خورد می‌چکند؟ مدح شاه‌ کیوان فر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کدام شه‌؟ شه ایران؛ چه‌کس‌؟ محمدشاه</p></div>
<div class="m2"><p>ورا لقب چه‌؟ ابوالسیف؛ از که‌؟ از داور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز نسل کیست‌؟ ز ترک؛ از چه ترک‌؟ از قاجار</p></div>
<div class="m2"><p>شهش ‌که ‌کرد؟ نیا؛ جانشینش‌ کیست‌؟ پسر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کشد که‌؟ حزمش‌؛ ‌چه‌؟ باره؛ ‌از چه‌؟ از انصاف</p></div>
<div class="m2"><p>کجا؟ به ملک؛ چرا؟ بهر دفع فتنه و شر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود چه تیغش؟ چون پاسبان دولت و دین</p></div>
<div class="m2"><p>رود چه‌ رخشش‌؟ چون همعنان فتح و ظفر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مسلمست‌؟ بلی؛ در چه‌؟ در سخا و سخن‌</p></div>
<div class="m2"><p>مقدم است‌؟ نعم؛ بر که‌؟ بر قضا و قدر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گذشته چه‌؟ صیتش؛ تا کجا؟ ‌به شرق و غرب</p></div>
<div class="m2"><p>رسیده چه‌؟ نامش؛ تا کجا؟ به بحر و به بر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بود که دشمن او؟ چون رمیده کی‌؟ شب و روز</p></div>
<div class="m2"><p>ز چه‌؟ ز سایهٔ خود؛ در کجا؟ به سنگ و مدر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دهد چه‌؟ زر؛ کی‌؟ دایم؛ چگونه‌؟ بی‌منت</p></div>
<div class="m2"><p>به که‌؟ به عارف و عامی؛ چه‌قدر؟ بی‌حد و مر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نظیر اوست چه‌؟ عکسش؛ کجا؟ در آیینه</p></div>
<div class="m2"><p>به معنی است نظیرش؟ نه از طریق نظر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به دور وی که خورد خون‌؟ دو کس؛ کجا؟ به دو جا</p></div>
<div class="m2"><p>به دشت رزمش تیغ و به مجلسش ساغر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همی‌گشاید چه‌؟ تیغ او؛ چه چیز؟ حصار</p></div>
<div class="m2"><p>همی فشاند چه‌؟ رمح او؛ چه چیز؟ شرر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز فر او شده کاسد چه چیز؟ ذلّ و هوان</p></div>
<div class="m2"><p>ز جود شده رایج چه چیز؟ فضل و هنر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گشاید آسان چه‌؟ رمح او؛ چه‌؟ بارهٔ سخت</p></div>
<div class="m2"><p>دهد فراوان چه‌؟ دست او؛ چه‌؟ بدرهٔ زر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کسی به عهدش پیچد به خویشتن‌؟ آری</p></div>
<div class="m2"><p>کمند در کف او زلف بر رخ دلبر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دلی ز جودش نالد به روزگار؟ بلی</p></div>
<div class="m2"><p>به کوه سیم و به دریا در و به کان گوهر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تنی گدازد در مجلسش به عید؟ نعم</p></div>
<div class="m2"><p>درون مجمر عود و میان آب شکر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به هیچ ‌کشور سرباز او شود حاکم‌؟</p></div>
<div class="m2"><p>بلی کجا؟ همه‌جا کیستند؟ ها بشمر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به ملک فارس حسین‌خان به مرز چین خاقان</p></div>
<div class="m2"><p>به ارض زنگ نجاشی به رومیان قیصر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همیشه تا که دمد چه‌؟‌ گل؛ از کجا؟ ز چمن</p></div>
<div class="m2"><p>شکفته باد چه‌؟ بختش‌؛ چگونه‌؟ چون عبهر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بود چه؟ یارش؛ ‌که‌؟ حق؛ دگر که؟ احمد و آل</p></div>
<div class="m2"><p>کجا؟ به هر دو سرا؛ تا چه روز؟ تا محشر</p></div></div>