---
title: >-
    قصیدهٔ شمارهٔ ۱۱۱ - مطلع ثانی
---
# قصیدهٔ شمارهٔ ۱۱۱ - مطلع ثانی

<div class="b" id="bn1"><div class="m1"><p>شبی به عادت روز شباب عیش آور</p></div>
<div class="m2"><p>شبی به سیرت صبح وصال جان‌پرور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شی ز بسکه زمین روشن از فروغ نجوم</p></div>
<div class="m2"><p>چو برک لاله عیان از درون سنگ شرر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبی زگنبد نیلوفری عیان پروین</p></div>
<div class="m2"><p>چو هفت نرگس شهلا ز شاخ نیلوفر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبی به ‌گونهٔ مشاطگان به‌ گرد عروس</p></div>
<div class="m2"><p>هجوم‌کرده ز هر سو نجوم‌ گرد قمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسول امّی مشگوی ام هانی را</p></div>
<div class="m2"><p>نموده از رخ و لب رشک جنت و کوثر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که جبرئیل امین فر خجسته پیک خدای</p></div>
<div class="m2"><p>به امر ایزد دادار حلقه زد بر در</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بانگ حلقه سر حلقهٔ انام ز شوق</p></div>
<div class="m2"><p>بسان حلقه ندانست پای را ازسر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو حلقه ساخت دل از یاد ماسوا خالی</p></div>
<div class="m2"><p>که تا ز حلقه جیب فنا برآرد سر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درون حلقهٔ امکان نماند هیچ مقام</p></div>
<div class="m2"><p>کزو چو رشته نکرد از درون حلقه‌ گذر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خطاب‌ کرد به جبریل‌ کای امین خدای</p></div>
<div class="m2"><p>بگو پیام چه داری ز حضرت داور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جواب دادش جبریل‌ کای پیمبر پاک</p></div>
<div class="m2"><p>تو خود پیام‌دهی و تو خود پیام‌آور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سخن ز دل به زبان وز زبان به دل گذرد</p></div>
<div class="m2"><p>درین میانه زبان منهی است و فرمان‌بر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگرچه آینه خالی بود ز صورت شخص</p></div>
<div class="m2"><p>بود به واسطهٔ شخص شخص را مظهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر از شکوفه برون آید و شکوفه ز شاخ</p></div>
<div class="m2"><p>گمان خلق چنان کز شکوفه خیزد بر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ثمر نهفته ز اصل است و آشکار ز فرع</p></div>
<div class="m2"><p>کنون تو اصلی و من فرع و سرّ وحی ثمر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرت هوس‌ که ز من بشنوی‌ حکایت خویش</p></div>
<div class="m2"><p>درون آینهٔ حق‌نمای من بنگر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ولی چو آینهٔ من محیط ذات تو نیست</p></div>
<div class="m2"><p>حکایتش ز تو ناقص نماید و ابتر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من و ملایک سکان آسمان و زمین</p></div>
<div class="m2"><p>تمام مظهر ذات توییم ای سرور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هزار آینه بنهاده است خرد و بزرگ</p></div>
<div class="m2"><p>درین هزار یکی را هزارگونه صور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکیست عین هزار ارچه هست غیر هزار</p></div>
<div class="m2"><p>که مختلف به ظهورند و متفق به‌گهر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یکیست ساقی و هر لحظه در یکی مجلس</p></div>
<div class="m2"><p>یکیست شاهد و هر لحظه در یکی زیور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کنون مجال سخن نیست برنشین به براق</p></div>
<div class="m2"><p>کز انتظار تو بس دیده است در معبر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همی برآمد چون برق بر براق و نخست</p></div>
<div class="m2"><p>به بیت مقدس چون پیک وهم‌کردگذر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وزان به مسجد اقصی چمید و شد ز کرم</p></div>
<div class="m2"><p>خجسته روح رسل را به سوی حق رهبر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فزود پایه و بخشید مایه داد فروغ</p></div>
<div class="m2"><p>به هر فرشته به هر آسمان به هر اختر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به سدره ماند ز ره جبرئیل و زانگونه</p></div>
<div class="m2"><p>که بازماند از پیک عقل پیک نظر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رسول‌ گفتش‌ کای طایر حظیرهٔ قدس</p></div>
<div class="m2"><p>سبب چه بود که‌ کردی به شاخ سدره مقر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جواب دادش‌ کای محرم حریم وصال</p></div>
<div class="m2"><p>من ار فراتر پرم بسوزدم شهپر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تویی که داری در کاخ لی مع‌الله جای</p></div>
<div class="m2"><p>تویی‌که داری از تاج لا به سر افسر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو شه‌نشانی و ما شه تو شاه و ما بنده</p></div>
<div class="m2"><p>تو آفتابی و ما مه تو ماه و ما اختر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو نیز هستی خویش اندرین محل بگذار</p></div>
<div class="m2"><p>بسیج بزم بقا کن وزین فنا بگذر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>براق عقل رها کن برآ به رفرف عشق</p></div>
<div class="m2"><p>که عقل را نبود با فروغ عشق اثر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به پشت رفرف برشد نبی ز پشت براق</p></div>
<div class="m2"><p>چنان‌که مرغ ز شاخ نگون به شاخ زبر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز سدره شد به مقامی‌که بود بیگانه</p></div>
<div class="m2"><p>در آن مقام تن از جان و جانش از پیکر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>صعود کرد به اوجی کز آن نمود هبوط</p></div>
<div class="m2"><p>ر*‌ع یافت به ملکی‌‌ز. آن نمود س‌حر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز سدره صد ره برتر چمید از پی آنک</p></div>
<div class="m2"><p>ز سدره آید و از جیب لا برآرد سر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دو قوس دایره در ملتقای نقطهٔ امر</p></div>
<div class="m2"><p>سر از دوسو بهم آورد چون خط پرگر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به عالمی شد کانجا نه اسم بود و نه رسم</p></div>
<div class="m2"><p>به‌محفلی شد‌ کانجا نه خواب بود و نه خور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>وجود شاهد و مشهود اتحادگزید</p></div>
<div class="m2"><p>چو اتحاد فروغ بصر به ذات بصر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نه اتحاد و حلولی که رای سوفسطا</p></div>
<div class="m2"><p>بود به نزد خر‌دمند زشت و ژاژ و هدر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بل اتحاد وجودی که نیست هستی وصف</p></div>
<div class="m2"><p>بغیر هستی موصوف هیچ چیز دگر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>میان‌هستی‌موصوف و وصف فرق این بس‌</p></div>
<div class="m2"><p>که متحد به وجودند و مختلف به صور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یکیست‌ اصل ‌و حقیقت ‌یکیست ‌فرع‌و مجاز</p></div>
<div class="m2"><p>یکیست عین و هویت یکیست تیغ و اثر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کمال و نقصان کرد از یکی مقام ظهور</p></div>
<div class="m2"><p>وجوب و امکان کرد از یکی گریبان سر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به یک خزینه درآمیخت قرصهٔ زر و سیم</p></div>
<div class="m2"><p>ز یک دریچه عیان‌گشت تابش مه و خور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نشسته ناظر و منظور در یکی بالین</p></div>
<div class="m2"><p>غنوده عاشق و معشوق در یکی بستر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دو ماهتاب فروزنده از یکی مطلع</p></div>
<div class="m2"><p>دو آفتاب درخشنده از یکی خاور</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دو تاجدار مکان کرده در یکی اورنگ</p></div>
<div class="m2"><p>دو گلعذار نهان گشته در یکی چادر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شنیده‌ام که نبی آن شب از ورای حجاب</p></div>
<div class="m2"><p>به گو‌شش آمد آواز حیدر صفدر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>و دیگر آنکه به هنگام بازگشت بدو</p></div>
<div class="m2"><p>نمود حمله یکی شرزه شیر اژدر در</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به ‌کام شیر سلیمان فکند خاتم و داد</p></div>
<div class="m2"><p>پس از نزول علی را از آن حدیث خبر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز گفت خاتم پیغمبران ز خاتم لعل</p></div>
<div class="m2"><p>فشاند حیدر کرار تنگ تنگ شکر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>یس از تبسم جان‌بخش خاتمی ‌که سپهر</p></div>
<div class="m2"><p>بود چو حلقه حاتم ز شرم او چنبر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زکان جیب برآورد و کرد گوهروار</p></div>
<div class="m2"><p>نثار خاتم پیغمبران بشیر بشر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز نعت حیدر کرار لب فروبندم</p></div>
<div class="m2"><p>ز بیم آنکه مسلمان نخواندم ‌کافر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>منم ثناگر آل رسول و حاسد من</p></div>
<div class="m2"><p>خرست اگر بفروشد هزار عشوه مخر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مرا ز کین خران باک نیست زانکه بود</p></div>
<div class="m2"><p>سه گز فسار و دو چنبر چدار چارهٔ خر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به پیش دشمن یاجوج‌ خو کشیدستم</p></div>
<div class="m2"><p>ازین قصیدهٔ ستوار سدّ اسکندر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>برین صحیفهٔ دلکش به جای نظم دری</p></div>
<div class="m2"><p>ز نوک خامه برافشانده‌ام عقود دُرر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>اگر قبول ملک افتد این چکامهٔ نغز</p></div>
<div class="m2"><p>به آب سیم نگارمش بر صحیفهٔ زر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>پسند حاسد اگر نیست گو مباش که هست</p></div>
<div class="m2"><p>گنه به شرع نگارنده نی به شعر اندر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به خالقی که دماند به سعی باد بهار</p></div>
<div class="m2"><p>ز ناف صخرهٔ صمّا شقایق احمر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به قادری‌ که ز پستان ابر نیسانی</p></div>
<div class="m2"><p>به کام کودک دُر دایه‌سان نماید دَر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بدانکه گشته ز صنعش دو فلک چرخ و زمین</p></div>
<div class="m2"><p>روان و ساکن بی‌بادبان و بی‌لنگر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به جان شاه هلاگو که هر دوگیتی را</p></div>
<div class="m2"><p>بیافریده خداوند در یکی پیکر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>که‌ گر خدیو جهان التفات ننماید</p></div>
<div class="m2"><p>برین قصیده‌که پیرایه بر عروس هنر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دگر نه نظم نگارم زکلک در دیوان</p></div>
<div class="m2"><p>دگر نه نثر نویسم ز خامه در دفتر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شنیده‌ام‌ که حسودی به شه چنین‌ گفته</p></div>
<div class="m2"><p>که بسته است رهی بر هجای شاه‌کمر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چگونه منکر باشم‌که در محامد تو</p></div>
<div class="m2"><p>ثنای ناقص من چون هجا بود منکر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>هر آن مدیح‌ که ممدوح را سزا نبود</p></div>
<div class="m2"><p>به کیش من ز دو صد قدح ناسزاست بتر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چگونه‌ کور کند مدح چشمهٔ خورشید</p></div>
<div class="m2"><p>چگونه‌ کر شمرد وصف نالهٔ مزهر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>همیشه تا نبود جسم را ز روح‌گزیر</p></div>
<div class="m2"><p>هماره تا نبود مست راز راح‌گذر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به قلب‌ گیتی امرت چو روح در قالب</p></div>
<div class="m2"><p>به جسم گیهان حکمت چو راح در ساغر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>هوای خدمت تو همچو روح راحت‌بخش</p></div>
<div class="m2"><p>سپاس حضرت تو همچو راح انده‌بر</p></div></div>