---
title: >-
    قصیدهٔ شمارهٔ ۶۶ - د‌ر ستایش شاهزاده‌ ی کیوان سریر اردشیر میرزا ‌دام‌ اقباله فرماید
---
# قصیدهٔ شمارهٔ ۶۶ - د‌ر ستایش شاهزاده‌ ی کیوان سریر اردشیر میرزا ‌دام‌ اقباله فرماید

<div class="b" id="bn1"><div class="m1"><p>صبح آفتاب چون ز فلک سر زد</p></div>
<div class="m2"><p>ماهم به خشم سندان بر در زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جستم ز جاگشودم درگفتی</p></div>
<div class="m2"><p>خورشید از کنار افق سر زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بس‌ که حنده خندهٔ نوشینش</p></div>
<div class="m2"><p>بر بسته بسته قند مکرّر زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ننشسته بردرید گریبان را</p></div>
<div class="m2"><p>پهلو ز تن به صبح من‌رر زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون داغ دیدان به ملامت جنگ</p></div>
<div class="m2"><p>در حلقهای زلف معنبر زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی به قهر پنجه یکی شاهین</p></div>
<div class="m2"><p>غافل به پرّ و بال ‌کبوتر زد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر روی خویش نازده یک لطمه</p></div>
<div class="m2"><p>از روی خشم لطمهٔ دیگر زد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای بسکه خنده صفحهٔ‌ کافورش</p></div>
<div class="m2"><p>زان لطمه بر لطیمهٔ عنبر زد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیلی‌تر از بنفشه‌ستان آمد</p></div>
<div class="m2"><p>از بس طپانچه بر گل احمر زد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتی به عمد شاخهٔ نیلوفر</p></div>
<div class="m2"><p>پیرایه را به فرق صنوبر زد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در خون دیده طرهٔ او گفتی</p></div>
<div class="m2"><p>زاغی به خون خویش همی پر زد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از دانه دانه اشک دو رخسارش‌</p></div>
<div class="m2"><p>بس‌ طعنه بر نجوم دو پیکر زد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در لب ‌گرفته زلف سیه‌ گفتی</p></div>
<div class="m2"><p>دزدی به بارخانهٔ‌ گوهر زد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر هر رگم ز خشم دو چشم او</p></div>
<div class="m2"><p>از هر نگه هزاران نشتر زد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر جان همه شرنگ ز شکر ریخت</p></div>
<div class="m2"><p>بر دل همه خدنگ ز عنبر زد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر مژه‌اش ز قهر به هر عضوم</p></div>
<div class="m2"><p>چندین هزار ناوک و خنجر زد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هم نرگسش به ‌کینم ترکش بست</p></div>
<div class="m2"><p>هم عبهرش به جانم آذر زد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیلی شدش ز بسکه رخ از سیلی</p></div>
<div class="m2"><p>گفتی به نیل دیبهٔ ششتر زد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگداخت شکّرین لب نوشینش</p></div>
<div class="m2"><p>از بس ز دیده آب به شکّر زد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>افروخت زیر زلف رخش گفتی</p></div>
<div class="m2"><p>دوزخ زبانه در دل کافر زد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در موج اشک مردمک چشمش</p></div>
<div class="m2"><p>بس دست و پا چو مرد شناور زد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سر تا قدم چون نیل شدش نیلی</p></div>
<div class="m2"><p>از بس طپانچه بر سر و پیکر زد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زد دست و زلف و کاکل مشکین را</p></div>
<div class="m2"><p>چون‌ کار رو‌زگار بهم بر زد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بگشود چین ز جعد و گره از زلف</p></div>
<div class="m2"><p>بر روی پاک و قلب مکدّر زد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چونان‌ که مار حلقه زند بر گنج</p></div>
<div class="m2"><p>مویش به‌ گرد رویش چنبر زد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شد چون بنات نعش پراکنده</p></div>
<div class="m2"><p>از بسکه چنگ بر زر و زیور زد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر زرد چهره سیلی پی در پی</p></div>
<div class="m2"><p>گفتی چو سکه بود که بر زر زد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چندان ‌که باد سرد کشید از دل</p></div>
<div class="m2"><p>اشکش ز دیده موج فزون تر زد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>موج از قفا‌ی ‌موج همی‌ گفتی</p></div>
<div class="m2"><p>بحر دمان ز جنبش صرصر زد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفتی ز خون دیده سِتبرَق را</p></div>
<div class="m2"><p>صباغ سان به خم معصفر زد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بیهوش‌ گشت عبهر فتانش</p></div>
<div class="m2"><p>زاشکش‌ به رخ گلاب همی برزد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گفتی ‌کسوف یافت مگر خورشید</p></div>
<div class="m2"><p>از بس‌ طپانچه بر مه انور زد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گفتمش ناله از چه‌ کنی چندین</p></div>
<div class="m2"><p>کافغانت بر به جان من‌ آذر زد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفتا ز دوری تو همی مویم</p></div>
<div class="m2"><p>کاتش‌ به موی موی من اندر زد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ایدون مر آن غلامک دیرینت</p></div>
<div class="m2"><p>زین باز بر به پشت تکاور زد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گفتم خمش‌ که صاعقهٔ آهت</p></div>
<div class="m2"><p>آتش به‌کشت جان من اندر زد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یک سال بیش رفت‌ که هجرانم</p></div>
<div class="m2"><p>آتش به جان مام و برادر زد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در ری ازین فزون بنیارم ماند</p></div>
<div class="m2"><p>کاهم به جان زبانه چو اخگر زد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>این‌گفت و سفت لعل به مروارید</p></div>
<div class="m2"><p>وز خشم سنگریزه به ساغر زد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گفت از پی علاج‌ کنون باید</p></div>
<div class="m2"><p>دست رجا به دامن داور زد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مظلوم ‌وش ز بهر تظلم چنگ</p></div>
<div class="m2"><p>در دامن‌ خدیو مظفّر زد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شهزاده اردشیر که جودش طعن</p></div>
<div class="m2"><p>بر ‌فضل معن و همت جعفر زد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فرماندهی که خادم قصر او</p></div>
<div class="m2"><p>بیغاره از جلال به قیصر زد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>رایش بها به مهر منور داد</p></div>
<div class="m2"><p>قهرش‌ قفا به چرخ مدور زد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خود او به‌رزم‌یک‌تنه‌چون‌خورشید</p></div>
<div class="m2"><p>با صد هزار بیشه غضنفر زد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کس دیده غیر او ‌که به یک حمله</p></div>
<div class="m2"><p>بر صد هزار بادیه لشکر زد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اختر بدند دشمن و او خورشید</p></div>
<div class="m2"><p>خورشیدوش‌ به یک فلک اختر زد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از خون زمین رزم بدخشان شد</p></div>
<div class="m2"><p>در کین چو او نهیب بر اشقر زد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بر عرق حلقِ خصم سنان او</p></div>
<div class="m2"><p>پنداشتی ز پیکان نشتر زد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زد برگره‌ره دشم‌ا دین تنها</p></div>
<div class="m2"><p>چون ‌مرتضی‌ که ‌بر صف کافر زد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دیگر نشان ‌کسی بنداد از او</p></div>
<div class="m2"><p>کوپال هرکرا که به مغفر زد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در رزم تیغ ‌کینه چو بهمن آخت</p></div>
<div class="m2"><p>در بزم جام زر چو سکندر زد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ساغر به‌ بزم‌ عیش چو خسرو خورد</p></div>
<div class="m2"><p>صارم به رزم خصم چو نوذر زد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جمشیدوار تخت چو بر بپراست</p></div>
<div class="m2"><p>خورشید وار بادهٔ احمر زد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بر بام آسمان برین قدرش</p></div>
<div class="m2"><p>ای‌بس‌که پنج نوبه چو سنجر زد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>جز تیر او عقاب شنیدستی</p></div>
<div class="m2"><p>کاندر طوافگاه اجل پر زد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>جز تیغ او نهنگ شنیدستی</p></div>
<div class="m2"><p>کاو همچو لجه موج ز جوهر زد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>خرگاه عز و رایت دولت را</p></div>
<div class="m2"><p>بر فرق چرخ و تارک اختر زد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نعلین جاه و مقدم حشمت را</p></div>
<div class="m2"><p>بر ارج ماه و فرق دو پیکر زد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>با برق‌ گویی ابر قرین آمد</p></div>
<div class="m2"><p>چون دست او به قبضهٔ خنجر زد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کفران نمود بر نعمش دشمن</p></div>
<div class="m2"><p>او تیغ‌ کینه از پی ‌کیفر زد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نشکفت اگر به طاعت ما چربد</p></div>
<div class="m2"><p>ضربی‌ که شه به دشمن ابتر زد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>کافزون ز طاعت ثقلین آمد</p></div>
<div class="m2"><p>آن ضربتی‌که حیدر صفدر زد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>شیر خدا علی‌ که حسام او</p></div>
<div class="m2"><p>آتش به جان فرقهٔ‌ کافر زد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>او بود ماشطهٔ صور خلقت</p></div>
<div class="m2"><p>دست ازل چو خامه به دفتر زد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>لا بلکه نیست دست صور پیرا</p></div>
<div class="m2"><p>گر نقش دست خالق اکبر زد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>جز او که اوست دست خدا آری</p></div>
<div class="m2"><p>دست خدا به دفتر زیور زد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>جز او پی شکستن بتها در</p></div>
<div class="m2"><p>کی پای‌کس به دوش پیمبر زد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>از راست جز به عون و لای او</p></div>
<div class="m2"><p>نتوان قدم به عرصهٔ محشر زد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>کوته ‌کنم سخن ‌که سزای او</p></div>
<div class="m2"><p>نتوان دم از ستایش درخور زد</p></div></div>