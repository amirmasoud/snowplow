---
title: >-
    قصیدهٔ شمارهٔ ۱۱۰ - در ستایش شاهزاده رضوان و ساده شجاع السلطنه حسنعلی میرزا طاب ثراه و تخلص به معراج نبی صلعم گوید
---
# قصیدهٔ شمارهٔ ۱۱۰ - در ستایش شاهزاده رضوان و ساده شجاع السلطنه حسنعلی میرزا طاب ثراه و تخلص به معراج نبی صلعم گوید

<div class="b" id="bn1"><div class="m1"><p>دو سال بیش ندانم‌گذشت یاکمتر</p></div>
<div class="m2"><p>که دور ماندم از ایوان شاه‌کیوان‌فر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا دو سال که هر روز آن دو سال بود</p></div>
<div class="m2"><p>ز روز خمسین الفم‌ هزار بار بتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از ملک نشدم دور دورکرد مرا</p></div>
<div class="m2"><p>سپهر کشخان‌ کش خانه باد زیر و زبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر عنایت شه یاریم کند امسال</p></div>
<div class="m2"><p>ازین‌ کبود کهن پشته برکشم‌ کیفر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپهر ازرق داند که من چو کین ورزم</p></div>
<div class="m2"><p>به روی هرمز وکیوان همی‌کشم خنجر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگرچه‌کرد مرا آسمان ز خدمت دور</p></div>
<div class="m2"><p>نگشت دور ز من مهر شاه دین‌ پرور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو هست قرب نهان گو مباش قرب عیان</p></div>
<div class="m2"><p>که نیست قرب عیان را به نزد عقل خطر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر نه مهر به چارم سپهر دارد جای</p></div>
<div class="m2"><p>و زو فروزان هر روز تودهٔ اغبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگر نه عقل‌ کزان سوی حیزست و مکان</p></div>
<div class="m2"><p>جدا نماند لختی ز مغز دانشور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگر نه یزدان‌کز فکرت و قیاس برون</p></div>
<div class="m2"><p>به ماست صدره نزدیکتر و سمع و بصر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غلام قرب نهانم‌که از دو صد فرسنگ</p></div>
<div class="m2"><p>کند مجسم منظور را به پیش نظر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ملک به خطهٔ کرمان و من به طوس برش</p></div>
<div class="m2"><p>ستاده دست بکش همچو چاکران دگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه سود قرب ملک خصم راکه نفزاید</p></div>
<div class="m2"><p>ز قرب احمد مختار جایگاه عمر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا به قرب عیان‌ گوش هوش نگراید</p></div>
<div class="m2"><p>که هست قرب عیان را هزارگونه خطر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مگر نبینی کز قرب آفتاب منیر</p></div>
<div class="m2"><p>همی چگونه به هر مه شود هلال قمر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مگر نبینی کز قرب آتش سوزان</p></div>
<div class="m2"><p>همی چگونه شود چوب خشک خاکستر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مگر نبینی‌ کز قرب شمع بزم‌افروز</p></div>
<div class="m2"><p>همی چگونه پروانه را بسوزد پر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من آن نیم‌که به من هرکسی شود چیره</p></div>
<div class="m2"><p>بجز خدا و خداوند آسمان چاکر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرآن جنین‌که ورا داغ‌کین من به جبین</p></div>
<div class="m2"><p>دریده چشم و نگونسار زاید از مادر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من آن گران سر سندان آهنینستم</p></div>
<div class="m2"><p>که برده سختی من آب پتک آهنگر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کس ار به دندان خاید ز ابلهی سندان</p></div>
<div class="m2"><p>به سعی خویش ‌رساند همی به‌ خویش ضرر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا خدای نگهبان و چارده تن پاک</p></div>
<div class="m2"><p>که رفته‌ گویی یک جان به چارده پیکر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یکی خورست درخشان ز چارده روزن</p></div>
<div class="m2"><p>یکی مهست فروزان ز چارده منظر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یکیست‌چشمه‌و جاری‌از آن چهارده جوی</p></div>
<div class="m2"><p>یکیست خانه و برگرد آن چهارده در</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز آب هر جو نوشی‌کند ز چشمه حدیث</p></div>
<div class="m2"><p>به نزد هر در پویی دهد ز خانه خبر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پس از عنایت یزدان و چارده تن پاک</p></div>
<div class="m2"><p>خجسته خسرو آفاق به مرا یاور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ابوالشجاع حسن شه جهان مجد که هست</p></div>
<div class="m2"><p>به نزد بحر کفش بحر در شمار شمر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به جنب حلمش‌ گوییست‌ گنبد مینا</p></div>
<div class="m2"><p>به نزد جودش جوییست لجهٔ اخضر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به راغ شوکت او چرخ سبزهٔ خضرا</p></div>
<div class="m2"><p>به باغ دولت او مهر لالهٔ احمر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به هرچه جزم‌ کند کردگار یاری‌بخش</p></div>
<div class="m2"><p>به هر چه عزم‌ کند روزگار فرمان‌بر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز ابر دستش رشحیست ابر فرو‌ردین</p></div>
<div class="m2"><p>به بحر طبعش موجیست بحر پهناور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به سنگ اگر نگرد سنگ راکند لولو</p></div>
<div class="m2"><p>به خاک اگر گذرد خاک را کند عنبر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مطیع خدمت او هرچه بر فلک انجم</p></div>
<div class="m2"><p>رهین طلعت او هرچه بر زمین ‌کشور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زمانه چیست‌ که از امر او بتابد روی</p></div>
<div class="m2"><p>ستاره ‌کیست‌ که از حکم او بپیچد سر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به‌ گرد معرکه شمشیر او بدان ماند</p></div>
<div class="m2"><p>که تیغ حیدرکرار در دل‌ کافر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو رخ نماید گیهان شود پر از خورشید</p></div>
<div class="m2"><p>چو لب‌ گشاید گیتی شود پر از گوهر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به روزگار نماند مگر به روز وغا</p></div>
<div class="m2"><p>که‌ کینه توزد چون روزگار کین‌گستر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به بحر ماند اگر بحر پر شود لبریز</p></div>
<div class="m2"><p>به مهر ماند اگر مهر برنهد افسر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که دیده بحر که در بر همی‌ کند خفتان</p></div>
<div class="m2"><p>که دیده مهر که بر سر همی نهد مغفر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>حسام او ملک الموت را همی ماند</p></div>
<div class="m2"><p>که جان ستاند تنها ز یک جهان لشکر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بسان روح خدنگش مکان‌ کند در دل</p></div>
<div class="m2"><p>به جای هوش حسامش نهان شود در سر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگر ندیدی خورشید را به‌گاه خسوف</p></div>
<div class="m2"><p>نهفته بین رخ رخشانش را به زیر سپر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فنای هرچه به‌ گیتی به قهر او مدغم</p></div>
<div class="m2"><p>بقای هرچه به‌گیهان به مهر او مضمر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شگفت آیدم از ابلهی‌ که رزم ترا</p></div>
<div class="m2"><p>همی بیند و انکار دارد از محشر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اگرچه از در انصاف جای عذرش هست</p></div>
<div class="m2"><p>که این مقام شهودست و آن مقام خبر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>من آنچه دیدم از خنگ برق رفتارت</p></div>
<div class="m2"><p>به هر که ‌گویم نادیده نیستش باور</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به صدهزاران مصحف اگر خورم سوگند</p></div>
<div class="m2"><p>همی فسانه شمارد حدیث من یکسر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چگونه آری باور کند که کوه‌ گرن</p></div>
<div class="m2"><p>به گاه پویه همی باج گیرد از صرصر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بود خیال مجسم وگرنه همچو خیال</p></div>
<div class="m2"><p>چگونه آسان می‌بگذرد به بحر و به بر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بود گمان مصور و گرنه همچو گمان</p></div>
<div class="m2"><p>چگونه یکسان می‌بسپرد نشیب و زبر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به‌گرد نقطهٔ پرگار چون خط پرگار</p></div>
<div class="m2"><p>همی بگردد و ساکن نمایدت به نظر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از آنکه چون خط پرگار بر یکی نقطه</p></div>
<div class="m2"><p>به‌ گردش آید و بر وی‌ کند سریع‌ گذر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز چابکی‌ که ورا هست خلق پندارند</p></div>
<div class="m2"><p>که قطب‌سان به یکی نقطه ساکنست ایدر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اگر به سمت فلک سیر او بدی مقدور</p></div>
<div class="m2"><p>به عون تربیت رایض قضا و قدر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مجال شبهه نبودی ‌که از سمک به سماک</p></div>
<div class="m2"><p>شدی چگونه به یکدم براق پیغمبر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مجال شبهه‌ کسی راست در عروج براق</p></div>
<div class="m2"><p>که‌ چشم‌ عقلش‌ کورست و گوش هوشش کر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>عنان خیل خیالم‌ گرفت رایض طبع</p></div>
<div class="m2"><p>که از حکایت معراج مصطفی مگذر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بگو که شاه جهان را خوش آید این گفتار</p></div>
<div class="m2"><p>چنان‌که خاطر پرویز را حدیث شکر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو ابتدای ثناکردی از مدیح رسول</p></div>
<div class="m2"><p>در انتهای سخن آبروی نظم مبر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>اگر قریحهٔ نظمت بود ز غصه مرنج</p></div>
<div class="m2"><p>بخوان زگفتهٔ من این قصیده را از بر</p></div></div>