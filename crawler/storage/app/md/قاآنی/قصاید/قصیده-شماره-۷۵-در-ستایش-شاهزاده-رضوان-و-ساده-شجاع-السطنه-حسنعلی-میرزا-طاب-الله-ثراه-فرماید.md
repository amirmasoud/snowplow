---
title: >-
    قصیدهٔ شمارهٔ ۷۵ - در ستایش شاهزادهٔ رضوان و ساده شجاع السطنه حسنعلی میرزا طاب‌الله ثراه فرماید
---
# قصیدهٔ شمارهٔ ۷۵ - در ستایش شاهزادهٔ رضوان و ساده شجاع السطنه حسنعلی میرزا طاب‌الله ثراه فرماید

<div class="b" id="bn1"><div class="m1"><p>غم و شادیست‌که با یکدگر آمیخته‌اند</p></div>
<div class="m2"><p>یا مه روزه به نوروز درآمیخته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درکفی رشتهٔ تسبیح و کفی ساغر می</p></div>
<div class="m2"><p>راست با عقد ثریا قمر آمیخته‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تردماغ از می شب خشک‌لب از روزهٔ روز</p></div>
<div class="m2"><p>ورع خشک به دامان تر آمیخته‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کف شیخ‌ عصا در کف میخواره قدح</p></div>
<div class="m2"><p>اژدها با ید بیضا اثر آمیخته‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه را چهره چو صندل شده از ره‌زه ولی</p></div>
<div class="m2"><p>صندلی هست‌ که با دردسر آمیخته‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطرب و ناله نی واعظ و آوازه وعظ</p></div>
<div class="m2"><p>لحن داود به صوت بقر آمیخته‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چرا روزه به نوره,ز درآمیخته است</p></div>
<div class="m2"><p>خلق با وی ز سرکینه درآمیخته‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه با روزه بجنگند و علاجش نکنند</p></div>
<div class="m2"><p>روبهانندکه با شیر ن‌ر آمیخته‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باز نوروز شود چیره هم آخرکه‌کنون</p></div>
<div class="m2"><p>نیمی از خلق بدو بیخبر آمیخته‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رو‌رزه‌کس را ندهد چیز و کند منع ز خور</p></div>
<div class="m2"><p>ابله آنان‌که بدو بی‌ثمر آمیخته‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرچه بر روزه به شورند هم آخر که سپاه</p></div>
<div class="m2"><p>با ملوک از پی تحصیل خور آمیخته‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خوان نوروز پر از نعمت الوان با او</p></div>
<div class="m2"><p>زین سبب مردم صاحب هن‌ر آمیخته‌اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منع می هم ند زانرو با اه سپهی</p></div>
<div class="m2"><p>همچو رندان جهان معتبر آمیخته‌اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زاهدان را اگر از سبحه‌کرامت اینست</p></div>
<div class="m2"><p>که یکی رشته به صد عقده‌ بر آمیخته‌اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ساقیان ‌راست ازین معجزه‌ کز ساغر می</p></div>
<div class="m2"><p>آب و آتش را با یکدگر آمیخته‌اند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کرده در جام بلورین می چون لعل روان</p></div>
<div class="m2"><p>نی نی الماس به یاقوت تر آمیخته‌اند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آتش طور عجین با ید بیضاکردند</p></div>
<div class="m2"><p>نار نمرود به آب خضر آمیخته‌اند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باده درکام فروریخته از زرّین جام</p></div>
<div class="m2"><p>خاو‌ران ‌گو‌یی با باختر آمیخته‌اند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سرخ مرجان تر آمیخته با لؤلؤ خشک</p></div>
<div class="m2"><p>تا به ساغر می مرجان گهر آمیخته‌اند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رنگ و بو داده به‌می لاله‌رخان از لب و زلف</p></div>
<div class="m2"><p>یا شفق را به نسیم سحر آمیخته‌اند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کرده در جام هلالی می خورشید مثال</p></div>
<div class="m2"><p>یا هلالیست‌که با قرص خور آمیخته‌اند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قطره‌یی آب بهم بسته ‌که هیچش‌ نم نیست</p></div>
<div class="m2"><p>با روان آتش نمناک درآمیخته اند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آب بی‌نم نگر و آتش پر نم‌ که به طبع</p></div>
<div class="m2"><p>هر نمش را به هزاران شرر آمیخته‌اند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اشک می پاک‌کند خون جگر را گرچه</p></div>
<div class="m2"><p>رنگ آن اشک به ‌خون جگر آمیخته‌اند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نی خبر می‌دهد از عشق و خبردار مباد</p></div>
<div class="m2"><p>گوش و هوشی که نه با آن خبر آمیخته‌اند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شکل ماریست‌ که باده دهش نیست زبان</p></div>
<div class="m2"><p>طبع زهرش به مزاج شکر آمیخته‌اند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنگ در چنگ خوش‌آهنگی کز آهنگش</p></div>
<div class="m2"><p>هوش شنوایی با گوش کر آمیخته‌اند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شاهدان بسته کمر کوه‌کشی را به میان</p></div>
<div class="m2"><p>زان سرینهاکه به موی‌کمر آمیخته‌اند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هنت سین کز پی تحویل گذارند به خوان</p></div>
<div class="m2"><p>گلرخان رنگی از آن تازه‌تر آمیخته‌اند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ساعد و سینه و سیما و سر و ساق و سرین</p></div>
<div class="m2"><p>هفت سین‌آسا با سیم بر آمیخته‌اند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گویی از لخلخهٔ عود و سراییدن رود</p></div>
<div class="m2"><p>بوی‌ گل با دم مرغ سحر آمیخته‌اند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مهوشان قرص تباشیر ز اندام سفید</p></div>
<div class="m2"><p>از پی راحت قلب‌کدر آمیخته‌اند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا همی از زر و یاقوت مفرح سازند</p></div>
<div class="m2"><p>می یاقوتی با جام زر آمیخته‌اند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گلعذاران شکرلب به علاج دل خلق</p></div>
<div class="m2"><p>هر زمان از رخ و لب گلشکر آمیخته‌اند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همه‌مشکین‌خط وشیرین‌لب‌و سیمین ‌عارض</p></div>
<div class="m2"><p>نوبه و هند عجب با خزر آمیخته‌اند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نقشبندان قضا بر ز بر دیبهٔ خاک</p></div>
<div class="m2"><p>نقشها تازه‌تر از شوشتر آمیخته‌اند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جعد سنبل جو زره عارض نسرین چو سپر</p></div>
<div class="m2"><p>از پی کینه زره با سپر آمیخته‌اند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مقدم اهل خرد غالیه‌بو بسکه به باغ</p></div>
<div class="m2"><p>عطرگل در قدم پی سپر آمیخته‌اند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شجر باغ چمان از چه ز تحریک صبا</p></div>
<div class="m2"><p>گرنه روح حیوان با شجر آمیخته‌اند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>حجر از فرط لطافت ز چه ناید به نظر</p></div>
<div class="m2"><p>گرنه جان ملکی با حجر آمیخته‌اند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چشم نرگس ز چه بر طرف چمن حادثه‌بین</p></div>
<div class="m2"><p>گرنه چشمش به خواص نظر آ‌میخته‌اند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از مطر زنده چرا پیکر بیجان نبات</p></div>
<div class="m2"><p>دم عیسی نه اگر با مطر آمیخته‌اند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شاهد گل شده بازاری و از مقدم آن</p></div>
<div class="m2"><p>نکهت نافه به هر رهگذر آمیخته‌اند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آب همرنگ زمرّد شده از بسکه به باغ</p></div>
<div class="m2"><p>حشر سبزه بهر جوی و جر آمیخته‌اند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بسکه در نشو و نمایند ریاحین‌گویی</p></div>
<div class="m2"><p>طبعشان زاب و گل بوالشر آمیخته‌اند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سوسن و عبهر و گل لاله و ریحان و سمن</p></div>
<div class="m2"><p>رسته در رسته حشر در حشر آمیخته‌اند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گویی از خیل خدیوان معظم گه بار</p></div>
<div class="m2"><p>نقش بزم ملک دادگر آمیخته‌اند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خسرو راد حسن‌شاه که از غایت لطف</p></div>
<div class="m2"><p>روح پاکانش با خاک درآمیخته‌اند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جرأت‌انگیز ز بس موقف رزمش‌گویی</p></div>
<div class="m2"><p>خاکش از زهرهٔ شیران نر آمیخته‌اند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>یک الف ترهٔ خشکیست به خوان‌کرمش</p></div>
<div class="m2"><p>هر تر و خشک‌ که در بحر و بر آمیخته‌اند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اجر یک‌روزهٔ سگبان جلالش نبود</p></div>
<div class="m2"><p>هرچه در خوان بقا ماحضر آمیخته‌اند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ابر و دریا نه ز خود اینهمه‌گوهر دارند</p></div>
<div class="m2"><p>باکف داور فرخنده‌فر آمیخته‌اند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دوست ‌سازست‌ و عدو سوز همانا زنخست</p></div>
<div class="m2"><p>طینتش را ز بهشت و سقر آمیخته‌اند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خاک راه تو شد اکسیر ز بس شاهانش</p></div>
<div class="m2"><p>با بصر از پی ‌کحل بصر آمیخته‌اند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>روزی از گلشن خُلقت اثری‌ گشت پدید</p></div>
<div class="m2"><p>هشت جنت را زان یک اثر آمیخته‌اند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>رقتی ار آتش قهرت شرری شد روشن</p></div>
<div class="m2"><p>هفت دوزخ را زان یک شرر آمیخته‌اند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ظفر از جیش تو هرگز نشود دور مگر</p></div>
<div class="m2"><p>طینت جیش ترا از ظفر آمیخته‌اند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>پاس ایوان ترا شب همه شب انجم چرخ</p></div>
<div class="m2"><p>دیده تا وقت سحر با سهر آمیخته‌اند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>صارمت صاعقهٔ خرمن عمرست مگر</p></div>
<div class="m2"><p>جوهرش با اجل جان‌شکر آمیخته‌اند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نیزه از بسکه‌گشاید رگ جان پنداری</p></div>
<div class="m2"><p>با سنانش اثر نیشتر آمیخته‌اند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>یابد آمیزش جان جسم یلان با جوشن</p></div>
<div class="m2"><p>گویی ارواح بود با صور آمیخته‌اند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بسکه در خود یلان نیفکند جاگویی</p></div>
<div class="m2"><p>خود ابطال به تیغ و تبر آمیخته‌اند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تیرها بسکه نشیند به زره پنداری</p></div>
<div class="m2"><p>عاشقان با صنمی سیمبر آمیخته‌اند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>پدران خنجر خونریز ز مغلوبی جنگ</p></div>
<div class="m2"><p>روستم‌وار به خون پسر آمیخته‌اند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پسران دشنهٔ فولاد ز سرگرمی‌کین</p></div>
<div class="m2"><p>همچو شیرویه به خون پدر آمیخته‌اند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>تیغت آنگاه‌که بر فرق عدوگیرد جای</p></div>
<div class="m2"><p>ماه نو گویی با باختر آمیخته‌اند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گاو سرگرز به دریای‌کفت پنداری</p></div>
<div class="m2"><p>کوه البرز به بحر خزر آمیخته‌اند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>گوهر نظم دلارای ترا قاآنی</p></div>
<div class="m2"><p>راستی‌گرچه به سلک‌گهر آمیخته‌اند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خازنان ملک از بهر خریداری آن</p></div>
<div class="m2"><p>هر دو سطرش‌ به دو مثقال زر آمیخته‌اند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>کم شود قیمت‌کالا چو فراوان‌گردد</p></div>
<div class="m2"><p>با فراوانی کالا ضرر آمیخته‌اند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به دل و دست ملک بین‌که دُرّ و گوهر را</p></div>
<div class="m2"><p>بسکه بخشیده چسان با مدر آ‌میخته‌اند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>تاکه همواره ز همواری و ناهمواری</p></div>
<div class="m2"><p>که به نیک و بد دور قمر آمیخته‌اند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>تلخی‌ کام بود لازم شیرینی عیش</p></div>
<div class="m2"><p>شهد با زهر و صفا با کدر آمیخته‌اند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تلخی‌ کام تو دشنام تو بادا به عدو</p></div>
<div class="m2"><p>گرچه دشنام تو هم با شکر آمیخته‌اند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>و‌انچنان عیش‌ تو شیرین که خود اقرار کنی</p></div>
<div class="m2"><p>که ازو شربت جان بشر آمیخته‌اند</p></div></div>