---
title: >-
    قصیدهٔ شمارهٔ ۲۲۲ - در ستایش ولیعهد رضوان مهد عباس شاه غازی طاب‌الله ثراه گوید
---
# قصیدهٔ شمارهٔ ۲۲۲ - در ستایش ولیعهد رضوان مهد عباس شاه غازی طاب‌الله ثراه گوید

<div class="b" id="bn1"><div class="m1"><p>الحمد خدا راکه ولیعهد معظم</p></div>
<div class="m2"><p>باز آمد و شد زامدنش ملک منظم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بازآمد و بگرفت همه ملک خراسان</p></div>
<div class="m2"><p>وز یاری یزدان شدش آن ملک مسلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امسال به فیروزی و اقبال خداداد</p></div>
<div class="m2"><p>از طوس بری شد بر شاهنشه اعظم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تشریف شهیٌ و لقب ملک‌ستانی</p></div>
<div class="m2"><p>بگرفت به پاداش فتوحات دمادم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پار آمدش از زیر نگین ملک خراسان</p></div>
<div class="m2"><p>امسال مسّخر شودش عرصهٔ عالم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر پار سپه راند پی فتح خبوشان</p></div>
<div class="m2"><p>امسال به تسخیر بخاراست مصمّم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملکی‌ که به صد جهد به صد عهد نگیرند</p></div>
<div class="m2"><p>بستد ز عدو جمله به یک حمله به یکدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امسال به خوارزم ز آهنگ سپاهش</p></div>
<div class="m2"><p>بینی به‌بر پیر و جوان‌کسوت ماتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امسال به‌ گاه سخط از صدمهٔ‌ گرزش</p></div>
<div class="m2"><p>بیرون رود از جنبرهٔ چرخ برین خم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امسال ‌کند از فزع چین جبینش</p></div>
<div class="m2"><p>خاقان خطا همچو غزال ختنی رم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>امسال بلاهور شود بی‌مدد صور</p></div>
<div class="m2"><p>غوغای نشور از غو شیپور مجسّم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از مهرهٔ زنبوره مشبک شود امسال</p></div>
<div class="m2"><p>چون خانهٔ زنبوران این برشده طارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از غلغلهٔ فوج زند بحر بلا موج</p></div>
<div class="m2"><p>چندانکه نماند اثر از عالم و آدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از طنطنه ‌کوس شود کاس فنا پر</p></div>
<div class="m2"><p>چندانکه نه‌ کس را خبر از بیش و نه از کم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرمانرو افغان به فلک برکشد افغان</p></div>
<div class="m2"><p>از بیم روان بسکه سنان بیند و صارم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رنجیده شود خاطر رنجیده به‌کشمیر</p></div>
<div class="m2"><p>از هستی خود بسکه علم بیند و پرچم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از زهرهٔ‌گردان‌که درآمیخته با خاک</p></div>
<div class="m2"><p>تا حشر زمین سبزتر از برگ سپر غم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وز خون دلیران‌که زند موج به‌گردون</p></div>
<div class="m2"><p>مینای فلک پر شود از بادهٔ در غم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زآوازهٔ پیکارش با دشمن مطعون</p></div>
<div class="m2"><p>ازیاد رود دردبهٔ وقعهٔ نیرم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>با پهلوی بدخواه‌ کند خنجر قهرش</p></div>
<div class="m2"><p>کاری‌ که به سهراب شد از خنجر رستم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جمشید زمانست و ولیعهد هم آخر</p></div>
<div class="m2"><p>از دیو بگیرد به سنان مملکت جم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تسخیر کند عزمش خوارزم و بخارا</p></div>
<div class="m2"><p>اقطاع شود چینش از آنگونه‌ که دیلم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای ساحت آفاق به جود تو مزّین</p></div>
<div class="m2"><p>وی جبههٔ افلاک به داغ تو موسّم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بعد از همه شاهانی و پیش از همه آری</p></div>
<div class="m2"><p>به بود محمدکه سپس بود ز آدم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>روید سمن از خاک و می از تاک ولیکن</p></div>
<div class="m2"><p>آن هر دو برین هر دو ز قدرند مقدم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گیتی همه از جود تو دلشاد به جز کان</p></div>
<div class="m2"><p>کیهان همه از فضل تو آباد به جز یم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مانا کف درپاش تو پنداری دریاست</p></div>
<div class="m2"><p>از بسکه پراکنده‌کندگوهر ودرهم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نی نی که به دست تو گه ریزش دریا</p></div>
<div class="m2"><p>مضمر بود آنسان‌که بود نیسان مدغم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شاید که ‌کند رزم تو و بزم تو منسوخ</p></div>
<div class="m2"><p>مردانگی رستم و بخشایش حاتم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هرگاه ‌که تیغ از پی پیکار بگیری</p></div>
<div class="m2"><p>در چشم عدو جلوه‌ کند مرگ مجسّم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مغزی ‌که پریشان شود از صدمهٔ‌ گرزت</p></div>
<div class="m2"><p>اجزای وجودش به قیامت نشود ضم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نبود عجب ار از تف شمشیر تو دریا</p></div>
<div class="m2"><p>چون‌کوزهٔ بی‌آب برون می‌ندهد نم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شیر فلک وگاو زمین از زبر و زیر</p></div>
<div class="m2"><p>در ربقهٔ فرمان تو چون‌کلب معلم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نه چنبر افلاک در انگشت گزینت</p></div>
<div class="m2"><p>گردان ز حقارت چو یکی حلقهٔ خاتم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدخواه نیارد به جهان تاب عنانت</p></div>
<div class="m2"><p>هر موی به تن‌ گر شودش افعی و ارقم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هگام وغا خصم دغا از توگریزان</p></div>
<div class="m2"><p>مانند گرازان که گریزند ز ضیغم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون غنچه ز سهم تو بدرند گریبان</p></div>
<div class="m2"><p>چون‌گل شود ار رسته ز گل بهمن و رستم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون لاله نمایند ز تیغ تو کفن سرخ</p></div>
<div class="m2"><p>چون سبزه‌ گر از خاک دمد آرش و نیرم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از پویهٔ رخش تو غباریست دماوند</p></div>
<div class="m2"><p>از آتش قهر تو شراریست جهنم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از دور بقای تو دمی دورهٔ گردون</p></div>
<div class="m2"><p>از بحر عطای تو نمی چشمهٔ زمزم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از عنف تو در رزم دو صد جیش پریشان</p></div>
<div class="m2"><p>از لطف تو در بزم دوصد عیثثن فراهم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جانبخش نعیمی چوکنی جای به دیهیم</p></div>
<div class="m2"><p>جانسوز جحیمی چو نهی پای بر ادهم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون غنچه‌که هردم شود از آب شکفته</p></div>
<div class="m2"><p>بدخواه ترا تازه شود زخم ز مرهم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر بر دم‌ کژدم نگرد مهر تو ناگه</p></div>
<div class="m2"><p>ور بر دم افعی‌گذرد مهر تو در دم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زآن در دم‌کژدم همه پازهر شود زهر</p></div>
<div class="m2"><p>زین در دَم افعی همه تریاق شود سم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نخلی شود این بسکه رطب ریزدش از دم</p></div>
<div class="m2"><p>نحلی شود این بسکه عسل خیزدش از دَم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ماریست سنانت‌که به افسون نشود رام</p></div>
<div class="m2"><p>الاکه برو راقی عفو تو دمد دم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از جنبش صد زلزله سستی نپذیرد</p></div>
<div class="m2"><p>کوهی‌که چو حکم تو بود ثابت و محکم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گلبن شود از صرصر قهر تو ورق‌ریز</p></div>
<div class="m2"><p>دوزخ شود از تربیت مهر تو خرم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هرجا که سنان تو جهانیست مسخر</p></div>
<div class="m2"><p>وانجا که ‌کفت عیش جهانیست مسلم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا تقویت روح دهد راح مروق</p></div>
<div class="m2"><p>تا تربیت جسم‌کند روح مکرم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خرّم ز تو اخیار چو از نام تو دینار</p></div>
<div class="m2"><p>در هم ز تو اشرار چو از جود تو در هم</p></div></div>