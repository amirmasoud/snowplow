---
title: >-
    قصیدهٔ شمارهٔ ۱۷۹ - د‌ر مدح شجاع‌لسلطنه حسنعلی میرزا
---
# قصیدهٔ شمارهٔ ۱۷۹ - د‌ر مدح شجاع‌لسلطنه حسنعلی میرزا

<div class="b" id="bn1"><div class="m1"><p>حبذا از هوای نیشابور</p></div>
<div class="m2"><p>که بود مایهٔ نشاط و سرور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح او اصل نزهتست و صفا</p></div>
<div class="m2"><p>شام او فرع عشرتست و حبور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پی انقطاع نسل محن</p></div>
<div class="m2"><p>صبح او را طبیعت‌کافور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طرب از خاک و خشت او ظاهر</p></div>
<div class="m2"><p>کرب اندر سرشت او مستور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باشد از یمن خاک او طاعن</p></div>
<div class="m2"><p>نیش عقرب به فضلهٔ زنبور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ثواب مرمّت ملکش</p></div>
<div class="m2"><p>شده شادان به مرزغن شاپور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حدودش ز ازدحام طرب</p></div>
<div class="m2"><p>نتوان جز بعون غصه عبور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزی از مصدر حوادث یافت</p></div>
<div class="m2"><p>رقم صادرات غصه صدور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و اصل از اهل او نشد که نبود</p></div>
<div class="m2"><p>ذره‌یی زان متاعشان مقدور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر دیارش ندارد از اشراق</p></div>
<div class="m2"><p>ذرّ‌هٔ من ز آفتاب حرو‌ر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زانکه در رستهٔ نزاهت او</p></div>
<div class="m2"><p>هم ترازوست نرخ سایه و نور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روح‌ پرور هوای او دارد</p></div>
<div class="m2"><p>اعتدال بهار در باحور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرده‌گویی نشاط‌ گیتی را</p></div>
<div class="m2"><p>آسمان بر زمین او مقصور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در چنین مأمنی به بستر رنج</p></div>
<div class="m2"><p>چون منی خفته روز و شب رنجور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چشمم از اشک آبگون دریا</p></div>
<div class="m2"><p>دلم از آه آتشین تنور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن یک از دوری حضور ملک</p></div>
<div class="m2"><p>این یک از هجر ناظر منظور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کلبه‌ام برده سیل اشک آری</p></div>
<div class="m2"><p>ژاله طوفان بود به خانهٔ مور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وای بر من اگر نمی کردم</p></div>
<div class="m2"><p>خویش را از خیال شه مسرور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاه غازی ابوالشجاع‌ که هست</p></div>
<div class="m2"><p>طبع‌ گیتی ز تیغ او محرور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنکه خوالیگرش نهد بر خوان</p></div>
<div class="m2"><p>کاسهٔ چینی از سر فغفور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>طوق خدمت فکنده فرمانش</p></div>
<div class="m2"><p>بر چه بر گردن وحوش و‌ طیور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نیل طاعت کشیده اقبالش</p></div>
<div class="m2"><p>بر چه بر جبههٔ اناث و ذکور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دل و دستش به‌گاه بذل و کرم</p></div>
<div class="m2"><p>گنج ارزاق خلق را گنجور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر به مغرب زمین سپاه‌کشد</p></div>
<div class="m2"><p>لرزه افتد ز هول در لاهور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حکم او حاکم و قضا محکوم</p></div>
<div class="m2"><p>امر او آمر و قدر مأمور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آنی از روزگار دولت او</p></div>
<div class="m2"><p>مایهٔ مدت سنین و شهور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای به‌کاخ تو چاکری چیپال</p></div>
<div class="m2"><p>وی به قصر تو خادمی فغفور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ذات پاکت ز ریمنی ایمن</p></div>
<div class="m2"><p>همچو میثاق عاشقان ز فتور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در زمانت به جغد رفته ستم</p></div>
<div class="m2"><p>گرچه هستی درین ستم معذور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زانکه معمار عدل توکرده</p></div>
<div class="m2"><p>هرچه ویرانه در جهان معمور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو نتاج جهانی و چه عجب</p></div>
<div class="m2"><p>گر به دست تو حلّ و عقد امور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>لذت نشوه ز آب انگورست</p></div>
<div class="m2"><p>گرچه آن هم نتاجی از انگور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا کفت ‌گشته در عطا معروف</p></div>
<div class="m2"><p>تا دلت گشته در سخا مشهور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ابر را دردها به تن مبرم</p></div>
<div class="m2"><p>بحر را زخم‌ها به دل ناسور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در صف حشرکارزارکه هست</p></div>
<div class="m2"><p>کوست از غو همال نفخهٔ صور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خلق را آنچنان ‌کند ز فزع</p></div>
<div class="m2"><p>که زنده نگردد به روز نشور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدسگال ار ز چنبر امرت</p></div>
<div class="m2"><p>یال طاعت برون‌کند ز غرور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>باش تا شیر آسمان فکند</p></div>
<div class="m2"><p>چون سگ لاس بر سرش ساجور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زانکه هرکس ازو حمایت خواست</p></div>
<div class="m2"><p>شد به ‌گیتی مظفر و منصور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نشود بی‌کفایت‌کف تو</p></div>
<div class="m2"><p>برکسی نزل روزی مقدور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نشود بی‌حصانت دل تو</p></div>
<div class="m2"><p>فتنه در حصن نیستی محصور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تاب گرزت نیاورد البرز</p></div>
<div class="m2"><p>طاقت نور حق نیارد طور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آنکه مدح تو و کسان ‌گوید</p></div>
<div class="m2"><p>سخنش را تفاوتی موفور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>قایل هر دو قول‌ گرچه یکیست</p></div>
<div class="m2"><p>لیک مصحف فصیح‌تر ز زبور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>عدد مدت مدار سپهر</p></div>
<div class="m2"><p>نزد عمر تو در شمار کسور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شیر فربه تن از مهابت تو</p></div>
<div class="m2"><p>خزد از لاغری به دیدهٔ مور</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>روز هیجا که در بسیط زمین</p></div>
<div class="m2"><p>افتد از بانگ‌ کوس شور نشور</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هر زمان بر صدور حادثه‌ای</p></div>
<div class="m2"><p>منشی آسمان دهد منشور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بر صماخ تو مشتبه‌ گردد</p></div>
<div class="m2"><p>غو شندف به نغمهٔ طنبور</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خون بدخواه را شماری می</p></div>
<div class="m2"><p>عرصهٔ جنگ را سرای سرور</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نشوهٔ جام حادثات کند</p></div>
<div class="m2"><p>شاهد خنجر ترا مخمور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ای‌که با شکل شیر رایت تو</p></div>
<div class="m2"><p>شیر گردون ردیف کلب عقور</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نور رای تو و بصیرت عقل</p></div>
<div class="m2"><p>جلوهٔ آفتاب و دیدهٔ ‌کور</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خسروا مادح تو قاآنی</p></div>
<div class="m2"><p>که نمی‌شد دمی جدا ز حضور</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>روزکی چند شدکنون‌که شدس</p></div>
<div class="m2"><p>ظاهر از قرب آستان تو دور</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هست موسی صفت به‌طور ملال</p></div>
<div class="m2"><p>در سرش خواهش تجلی نور</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ور نه دانی‌که لحظه‌یی نشود</p></div>
<div class="m2"><p>از حریم عنایتت مهجور</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>آرم از انوری دو بیت ‌که هست</p></div>
<div class="m2"><p>هریکی همچو لولو منثور</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به خدایی‌که از مشیت اوست</p></div>
<div class="m2"><p>رنج رنجور و شادی مسرور</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>که مرا از همه جهان جانیست</p></div>
<div class="m2"><p>وان ز حرمان خدمتت رنجور</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تا که از فعل حرف جر گردد</p></div>
<div class="m2"><p>آخر اسم منصرف مجرور</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آن هر لحظه‌یی ز عمر تو باد</p></div>
<div class="m2"><p>هم ترازوی امتداد دهور</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>صبح ایام عیش دشمن تو</p></div>
<div class="m2"><p>تالی شام تاری دیجور</p></div></div>