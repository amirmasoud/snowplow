---
title: >-
    قصیدهٔ شمارهٔ ۳۳۰ - و له فی المدیحه
---
# قصیدهٔ شمارهٔ ۳۳۰ - و له فی المدیحه

<div class="b" id="bn1"><div class="m1"><p>آوخا کز کین چرخ چنبری</p></div>
<div class="m2"><p>رنج را بر عیش دادم برتری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوی دیر از کعبه یازیدم عنان</p></div>
<div class="m2"><p>بر مسلمانی گزیدم کافری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نحس را بر سعد کردم اختیار</p></div>
<div class="m2"><p>کردم آهنگ زحل از مشتری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از در نابخردی گشتم روان</p></div>
<div class="m2"><p>جانب انگشت‌گر از عنبری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رو سوی بوجهل جهلان تافتم</p></div>
<div class="m2"><p>از حریم حرمت پیغمبری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر در یاجوجیان‌کردم‌گذار</p></div>
<div class="m2"><p>از رواق شوکت اسکندری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بردم از موسی بهارونی پیام</p></div>
<div class="m2"><p>جانب گوسالگان سامری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یعنی از درگاه دارا زی سرخس</p></div>
<div class="m2"><p>اسب‌ راندم‌ سوی سالو از خری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از برای دیدن خفاش چند</p></div>
<div class="m2"><p>دیده بربستم ز مهر خاوری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خسرو خاور حسن شه آنکه هست</p></div>
<div class="m2"><p>دست جودش رشک ابر آذری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حیدری کز نیروی بازوی خویش</p></div>
<div class="m2"><p>کرده در روز محابا صفدری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صفدری‌کز ذوالفقار تیغ تیز</p></div>
<div class="m2"><p>کرده اندر دشت هیجا حیدری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنکه خط استوا و خط قطب</p></div>
<div class="m2"><p>کرده چرخ حشمتش را محوری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باشد از تاثیر نوش رافتش</p></div>
<div class="m2"><p>زهر را خاصت سیسنبری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تفّ تیغش‌گر به دریا بگذرد</p></div>
<div class="m2"><p>آب را بخشد خواص آذری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کرده فربه ملک را شمشیر او</p></div>
<div class="m2"><p>گرچه همتا نیستش در لاغری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خسروا ای سطح درگاه ترا</p></div>
<div class="m2"><p>با فراز عرش اعظم برتری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون سلیمان عالمت زیر نگین</p></div>
<div class="m2"><p>لیک بی‌خاصیت انگشتری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روزکین‌ کز شورش‌کند آوران</p></div>
<div class="m2"><p>گسترد دوران بساط محشری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرد راه‌و بانگ‌کوس و شور نای</p></div>
<div class="m2"><p>بر ثریا راه یابد از ثری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چرخ رویاند ز خاک‌کشتگان</p></div>
<div class="m2"><p>گونه‌گونه لالهای احمری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وانگهی زان لالها احمر شود</p></div>
<div class="m2"><p>لونهای احمری گون اصفری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از غبار ره هوای‌کارزار</p></div>
<div class="m2"><p>عزم‌ گردونی کند از اغبری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر فریدون فرّه‌یی ضحاک‌وار</p></div>
<div class="m2"><p>نیزه برگیرد چو مار حمیری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وزگرن پتک عمودگاوسر</p></div>
<div class="m2"><p>کاوه‌وش هر تن‌ کند آهنگری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون ‌تو بیرون‌ تازی از مکمن سمند</p></div>
<div class="m2"><p>لرزه افتد در روان لشکری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز آب شمشیر شرربارت زمین</p></div>
<div class="m2"><p>یابد از زلزال طبع صرصری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باست اندر پیکر بدخواه ملک</p></div>
<div class="m2"><p>گه نماید ناچخی‌گه خنجری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خسروا ای دست احسان ترا</p></div>
<div class="m2"><p>در سخاوت دعوی پیغمبری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>این منم قاآنی دوران‌که هست</p></div>
<div class="m2"><p>در فنون نظم و نثرم ماهری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون نیوشد نظم من در زیر خاک</p></div>
<div class="m2"><p>آفرین گوید روان انوری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ور ببیند عنصری اشعار من</p></div>
<div class="m2"><p>دفتر دانش بشوید عنصری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در سخن پیغمبرم وز کینه خصم</p></div>
<div class="m2"><p>متهم سازد مرا در ساحری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا بریزد برگها از شاخسار</p></div>
<div class="m2"><p>ز اهتزاز بادهای آذری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>باد ذاتت همچوذات لایزال</p></div>
<div class="m2"><p>از زوال و شرکت و نقصان بری</p></div></div>