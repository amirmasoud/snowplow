---
title: >-
    قصیدهٔ شمارهٔ ۶ - در مدح ابوالمظفر محمدشاه غازی طاب‌لله ثراه گوید
---
# قصیدهٔ شمارهٔ ۶ - در مدح ابوالمظفر محمدشاه غازی طاب‌لله ثراه گوید

<div class="b" id="bn1"><div class="m1"><p>گسترد بهار در زمین دیبا</p></div>
<div class="m2"><p>چون چهر نگار شد چمن زیبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آثار پدید آب شد پنهان</p></div>
<div class="m2"><p>اسرار نهان خاک شد پیدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابر آمد و سیم ریخت بر هامون</p></div>
<div class="m2"><p>باد آمد و مشک بیخت بر صحرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این تعبیه‌کرده نافه در دامن</p></div>
<div class="m2"><p>آن عاریه کرده‌گوهر از دریا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سبزه چمن چو روضهٔ رضوان</p></div>
<div class="m2"><p>از لاله دمن چو سینهٔ سینا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن مایهٔ سوز سینهٔ غمگین</p></div>
<div class="m2"><p>وین سرمهٔ نور دیدهٔ بینا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این را به سر است‌کلّه از یاقوت</p></div>
<div class="m2"><p>آن را به بر است حلّه از مینا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای عید من ای بهار روحانی</p></div>
<div class="m2"><p>ای ماه من ای نگار بی‌همتا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نوروز تویی و نوبهاران تو</p></div>
<div class="m2"><p>کز طلعت تو جوان شود دنیا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از روح روان سرشته‌یی گویی</p></div>
<div class="m2"><p>بر روی ز من فرشته‌یی مانا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از لعل تو نعل روح در آتش‌</p></div>
<div class="m2"><p>از عشق تو مغز عقل پر سودا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون از خم زلف چهره بنمایی</p></div>
<div class="m2"><p>خورشید برآید از شب یلدا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون سلسله زلف تست پر حلقه</p></div>
<div class="m2"><p>چون زلزله عشق تست پر غوغا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این زلزله‌کوه راکند از بن</p></div>
<div class="m2"><p>این سلسله عقل راکند شیدا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بنما رخ تا ز شوق بی‌معجر</p></div>
<div class="m2"><p>از خلد برین برون دود حورا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بنشین و ببار خندهٔ شیرین</p></div>
<div class="m2"><p>برخیز و بیار بادهٔ حمرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگشای‌کمرکه تاکمربندد</p></div>
<div class="m2"><p>در خدمت تو در آسمان جوزا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لبهای تو بهر بوسه خلقت‌کرد</p></div>
<div class="m2"><p>از حکمت خویش خالق یکتا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عاطل مگذار خلقت باری</p></div>
<div class="m2"><p>باطل مشمار حکمت دانا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو موی نموده‌یی‌کمند آیین</p></div>
<div class="m2"><p>من پشت نموده‌ام‌کمان‌آسا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون تیر تو ازکمان ما عاجل</p></div>
<div class="m2"><p>چون تار من ازکمند تو دروا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای ترک به‌عید بوسه آیین است</p></div>
<div class="m2"><p>در شرع رسول و ملت بیضا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حالی بنه این طبیعت غره</p></div>
<div class="m2"><p>شرمی بکن از شریعت غرا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زان پس‌که مرا مباح شد بوسه</p></div>
<div class="m2"><p>پیش آی‌که تا ببوسمت عمدا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از بوسه مکن دریغ تات ای ترک</p></div>
<div class="m2"><p>صد بوسه‌زنم برآن رخ رخشا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هل تا بگزم لبان شیرینت</p></div>
<div class="m2"><p>خوش خوش‌مزم آن دودانهٔ خرما</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زان روی چنم ورق ورق سوری</p></div>
<div class="m2"><p>زان لعل خورم طبق طبق حلوا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زان‌گرد زنخ‌که‌گوی را ماند</p></div>
<div class="m2"><p>در رقص آیم چوگوی سر تا پا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نی نیست به بوسه حاجتم امروز</p></div>
<div class="m2"><p>گر عمر بود ببوسمت فردا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کامروز بس است لب مرا شیرین</p></div>
<div class="m2"><p>از شکر شکر خسرو والا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دارای جهان ستان محمد شاه</p></div>
<div class="m2"><p>کز هردو جهان فزون بود تنها</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اجزای وی است هرچه درگیتی</p></div>
<div class="m2"><p>باکل چه برابری‌کند اجزا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اعضای وی است هرکه در عالم</p></div>
<div class="m2"><p>با روح چه همسری‌کند اعضا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>افلاک مطاوعش به یک فرمان</p></div>
<div class="m2"><p>آفاق مسخرش به یک ایما</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کوهی‌که خورد قفای قهر او</p></div>
<div class="m2"><p>آسیمه دود چو باد در بیدا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بادی‌که بود مطیع حزم او</p></div>
<div class="m2"><p>همواره بود چوکوه پابرجا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ای خشم تو همچو مرگ بی‌تاخیر</p></div>
<div class="m2"><p>وی قهر تو همچو زهر جان‌فرسا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خیل تو چو سیل‌کوه بنیان‌کن</p></div>
<div class="m2"><p>فوج تو چو موج بحر طوفان‌زا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در جانسوزی چو چرخ بی‌مهلت</p></div>
<div class="m2"><p>درکین‌توزی چو دهر بی‌پروا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نه ملک مخلد ترا مقطع</p></div>
<div class="m2"><p>نه ذات مؤید ترا مبدا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>صد جمله به حمله‌یی زنی برهم</p></div>
<div class="m2"><p>صد بقعه به وقعه‌یی‌کنی یغما</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از دشنهٔ توکه تشنهٔ خون است</p></div>
<div class="m2"><p>بس‌کشته‌که پشته‌گشته در هیجا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>باطلعت رای‌گیتی افروزت</p></div>
<div class="m2"><p>خورشید برآید از شب یلدا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>با نکهت خلق عنبرافشانت</p></div>
<div class="m2"><p>عنبر خیزد زکام اژدرها</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>توقیع ترا قدر برد فرمان</p></div>
<div class="m2"><p>فرمان ترا قضاکند امضا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>انکار تو نیست دهر را ممکن</p></div>
<div class="m2"><p>پیکار تو نیست چرخ را یارا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>انجم تار است و رای تو روشن</p></div>
<div class="m2"><p>گردون پستست و قدر تو والا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شیر است به روز جنگ تو روبه</p></div>
<div class="m2"><p>موم است ز زور چنگ تو خارا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>فوجی ز صف سپاه تو انجم</p></div>
<div class="m2"><p>موجی زکف نوال تو دریا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خلق تو زکام شیر انگیزد</p></div>
<div class="m2"><p>چون ناف غزال نافهٔ سارا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مهر تو ز صلب سنگ رویاند</p></div>
<div class="m2"><p>چون باد بهار لالهٔ حمرا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خورشیدی و برخلاف خورشیدی</p></div>
<div class="m2"><p>کز ابر شود به چرخ ناپیدا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>زیراکه هماره باکفی چون ابر</p></div>
<div class="m2"><p>خورشید صفت بتابدت سیما</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چون باد قلم دود در انگشتم</p></div>
<div class="m2"><p>گر مدح تکاورت‌کنم املا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چون برق‌کشد ضمیر من شعله</p></div>
<div class="m2"><p>گر وصف بلارکت‌کنم انشا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گر خشم‌کنی به چشمهٔ خورشید</p></div>
<div class="m2"><p>چون شب‌پره زو حذرکند حربا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ور چشم زنی به جانب ناهید</p></div>
<div class="m2"><p>سوی تو چمد زگنبد خضرا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اخلاق تو آبگینه یارد ساخت</p></div>
<div class="m2"><p>از نرم دلی ز صخرهٔ صما</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گرد سپهت به چشم بدخواهان</p></div>
<div class="m2"><p>یک بادیه افعی است و اژدرها</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>شخص تو جهان پیر برناکرد</p></div>
<div class="m2"><p>از دانش پیرو طالع برنا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>رخسار تو آیینه است و خصمت دیو</p></div>
<div class="m2"><p>زان در تو چو بنگرد شود رسوا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تا لمعه و نور خیزد از خورشید</p></div>
<div class="m2"><p>تا فتنه و شور زاید از صهبا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>دارم دو هزار شکوه از طالع</p></div>
<div class="m2"><p>لیک آن دو هزار شکوه باشد تا</p></div></div>