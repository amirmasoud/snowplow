---
title: >-
    قصیدهٔ شمارهٔ ۱۱ - در مدح سلالة‌السادت میرزا سلیمان 
---
# قصیدهٔ شمارهٔ ۱۱ - در مدح سلالة‌السادت میرزا سلیمان 

<div class="b" id="bn1"><div class="m1"><p>اگر مشاهده خواهی فروغ یزدان را</p></div>
<div class="m2"><p>به صدر فضل نگر میرزا سلیمان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چراغ دودهٔ خیرالبشرکه طاعت او</p></div>
<div class="m2"><p>ز لوح دهر فروشسته نقش عصیان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کلیم‌وار عیان بین به طور سینهٔ او</p></div>
<div class="m2"><p>چو نور وادی ایمن فروغ ایمان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرآنکه بیند بر سفت او ردای ورع</p></div>
<div class="m2"><p>به یک ردا نگرد صدهزار سلمان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کف‌کریمش‌ از بس فشانده در یتیم</p></div>
<div class="m2"><p>یتیم ساخته پروردگار عمان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرآن نشاط بود روح را ز صحبت او</p></div>
<div class="m2"><p>کز آب چشمهٔ زمزم روان عطشان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خوان فضلش اگر توشه‌یی برد عاصی</p></div>
<div class="m2"><p>به خوشه‌یی نخرد هفت باغ رضوان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نوع انسان آنسان بود مباهاتش</p></div>
<div class="m2"><p>که بر بسایر انواع نوع انسان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلام او همه وحی است لاجرم دانا</p></div>
<div class="m2"><p>زگفت او نکند فرق هیچ فرقان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز آب چشمهٔ آتش فروغ حکمت او</p></div>
<div class="m2"><p>فلک به باد فنا داده خاک یونان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زبان او به‌سخن صارمیست خاره شکاف</p></div>
<div class="m2"><p>که بر دو سندس داند پرند و سندان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زمانه اشهدبالله به ملک هستی او</p></div>
<div class="m2"><p>به عمر خود نشنیده است نام پایان را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهرکوکبه صدرا تویی‌که‌کوکب تو</p></div>
<div class="m2"><p>شکسته‌کوکبه هفت آسمان‌گردان را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پی تذکر مدح تو شسته حافظ روح</p></div>
<div class="m2"><p>ز لوح حافظهٔ ناس نقش عصیان را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به باغ مجد تو سیسنبریست چرخ‌کبود</p></div>
<div class="m2"><p>چه افتخار به سیسنبری‌گلستان را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سپهر رای ترا آفتاب تابان خواند</p></div>
<div class="m2"><p>چو نیک دید ستغفارگفت بهتان را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از آن سپس ز در شرم زیب بزم تو ساخت</p></div>
<div class="m2"><p>چو آفتابهٔ زر آفتاب تابان را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ترا به ملک هنر شاه دید و با خودگفت</p></div>
<div class="m2"><p>که آفتابهٔ زر لایق است سلطان را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نبود آگه ازین ماجراکه اندر شرع</p></div>
<div class="m2"><p>ز زر و سیم نسازند آب دستان را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ضعیف پیکر تو یک دو مشت ستخوانست</p></div>
<div class="m2"><p>کزوست توشهٔ هستی همای امکان را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر آنکه دید تنت خیره ماندکز چه خدای</p></div>
<div class="m2"><p>گزیده بردو جهان یک‌دو مشت ستخوان را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به راه یزد چو یعقوب دیده‌گشت سفید</p></div>
<div class="m2"><p>ز شوق خاک رهت سرمهٔ سپاهان را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز نور رای توگر دم زد آفتاب مرنج</p></div>
<div class="m2"><p>که التهاب تبش موجبست هذیان را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز هجر احمد مرسل حنین حنانه</p></div>
<div class="m2"><p>اگر قرین انین ساخت عرش یزدان را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شب فراق تو نیز این زمان ز نالهٔ یزد</p></div>
<div class="m2"><p>نموده حنان بر اهل یزد حنان را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بزرگوارا از روی شوق قاآنی</p></div>
<div class="m2"><p>دهد به مدح تو زیور عروس دیوان را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که تا به روز قیامت بزرگ بار خدای</p></div>
<div class="m2"><p>ز وی دریغ ندارد عطا و احسان را</p></div></div>