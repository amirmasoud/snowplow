---
title: >-
    قصیدهٔ شمارهٔ ۱۴۱ - د‌ر مدح فریدون میرزا گوید
---
# قصیدهٔ شمارهٔ ۱۴۱ - د‌ر مدح فریدون میرزا گوید

<div class="b" id="bn1"><div class="m1"><p>ای ترک می فروش ای ماه میگسار</p></div>
<div class="m2"><p>بنشین و می بنوش برخیز و می بیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه خطا مرو ترک عطا مکن</p></div>
<div class="m2"><p>بیخ وفا مَکَن، تخم جفا مکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بستان بده بنوش بنشین بگو بجوش</p></div>
<div class="m2"><p>چندت ‌زبان خموش چندت روان فکار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش آر چنگ و نی بردار جام می</p></div>
<div class="m2"><p>بفشان ز چهره خوی بنشان ز سر خمار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیور چه می‌نهی زیور تراست ننگ</p></div>
<div class="m2"><p>زینت چه می‌کنی زینت تراست عار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زیور ترا بس است آن موی چون عبیر</p></div>
<div class="m2"><p>زیث ترا ببب‌ن اس آن روی چون نگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برگیر چنگ و جام درده صلای عام</p></div>
<div class="m2"><p>خوشتر از آن‌ کدام بهتر ازین چه‌کار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پایی ز روی وجد بر آستان بکوب</p></div>
<div class="m2"><p>دستی برای رقص از آستین برآر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنشین به دامنم تا از لب و رخت</p></div>
<div class="m2"><p>پر مل‌کنم دهان پرگل‌کنم‌کنار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می ده مرا چنانک هر دم ز بیخودی</p></div>
<div class="m2"><p>آویزمت به جهد در زلف مشکبار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هی‌گویمت سخن هی‌گیرمت به بر</p></div>
<div class="m2"><p>هی بویمت دهان هی بوسمت عذار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای در مذاق من دشنام تلخ تو</p></div>
<div class="m2"><p>چون‌صبر سودمندچون‌پند سازگار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گویند از جهان هر تن که بست رخت</p></div>
<div class="m2"><p>در بند مار و مورگردد تنش دوچار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من در حیات خویش از خط و زلف تو</p></div>
<div class="m2"><p>افتاده‌ام اسیر در بند مور و مار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای‌’‌ترک‌کاشغر ای شمع غاتفر</p></div>
<div class="m2"><p>ای سرو کاشمر ای ماه قندهار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رو ترک کن ادب دیوانگی طلب</p></div>
<div class="m2"><p>از روی اختیار در عین اقتدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چند از پی هنر پوییم دربدر</p></div>
<div class="m2"><p>چند از پی خطر موییم زار زار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خاموشی آورد گفتار بی‌ثمر</p></div>
<div class="m2"><p>بیهوشی آورد سودای هوشیار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دانش به پای طبع بندیست آهنین</p></div>
<div class="m2"><p>فکرت به راه نفس دامیست استوار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن بند درشکن این دام درگسل</p></div>
<div class="m2"><p>زین بند شو برون زین دام‌ کن فرار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نی نی ز هوش و عقل ما را‌ گزیر نیست</p></div>
<div class="m2"><p>کاین هر دو لازمست در مدح شهریار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دیباچهٔ مهی فهرست فرهی</p></div>
<div class="m2"><p>عنوان آگهی دیوان افتخار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دریای مکرمت دنیای معدلت</p></div>
<div class="m2"><p>گیهان منزلت‌گردون اقتدار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سلطان بحر و بر دارای خشک و تر</p></div>
<div class="m2"><p>نقاد خیر و شر قلاب نور و نار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فرخ شه آنکه هست فرخنده ذات او</p></div>
<div class="m2"><p>بر خلق آیتی از فضل ‌کردگار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نطقش همه‌گهر رایش همه هنر</p></div>
<div class="m2"><p>بختش همه ظفر شخصش همه وقار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جان بی‌ولای او در پیکرست ننگ</p></div>
<div class="m2"><p>سر بی‌رضای او برگردنست بار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گیهان ز بخت او جون بخت او سمین</p></div>
<div class="m2"><p>دشمن ز رمح او چون رمح او نزار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر جنبشی‌که هست مقدور آسمان</p></div>
<div class="m2"><p>تاند که طی ‌کند عزمش به یک مدار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شخصش ‌ببین ‌به‌ رخش ‌‌بادست گنج‌بخش</p></div>
<div class="m2"><p>ابر ار ندیده‌یی بر فرق‌ کوهسار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بنگربه روز جنگ‌گرزش درون چنگ</p></div>
<div class="m2"><p>کوه ار ندیده‌یی در بحر بی‌کنار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از ترکتاز مرگ ایمن بود روان</p></div>
<div class="m2"><p>از حزمش ار کشد بر گرد تن حصار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مهرش سرشته‌اند در جان آدمی</p></div>
<div class="m2"><p>ورنه نیافتی جان در بدن قرار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر نام خسروان یکباره حک‌کنند</p></div>
<div class="m2"><p>آثار او بس است زآن جمله یادگار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>محصور عمر اوست ادوار آسمان</p></div>
<div class="m2"><p>مقصور امر اوست اطوار روزگار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ای چون بنای چرخ ‌کاخ تو دیرپای</p></div>
<div class="m2"><p>وی چون اساس فضل ملک تو پایدار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از سهم تیر تو در وقت دار و گیر</p></div>
<div class="m2"><p>از بیم تیغ تو در روز گیرودار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بر پیکرگوان خفتان شود کفن</p></div>
<div class="m2"><p>بر تارک مهان افسر شود افسار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چندین هزار قرن یک لحظه طی ‌کند</p></div>
<div class="m2"><p>خورشید اگر شود بر توسنت سوار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مانا که در چنار قهرت نهفته‌ اند</p></div>
<div class="m2"><p>کز اصل خویشتن آتش دهد چنار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سرویست رمح تو در جویبار رزم</p></div>
<div class="m2"><p>مرگ گوانش بر ترگ یلانش یار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>قصرت ز خسروان چرخیست پر نجوم</p></div>
<div class="m2"><p>کاخت ز نیکوان باغیست پر نگار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شاها خدای من داند که روز و شب</p></div>
<div class="m2"><p>شکرانه‌گویمت هر دم هزار بار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>روزی ‌که نگذرد نام تو بر لبم</p></div>
<div class="m2"><p>نفرین‌ کنم به خویش از فرط انزجار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>برهان قاطعست بر پاکی سخن</p></div>
<div class="m2"><p>تا شعر من شدست چون تیغت آبدار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ای شاه پیش ازین معروض داشتم</p></div>
<div class="m2"><p>کز فضل بی‌قیاس وز جود بی‌شمار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>باری طلب‌کنی اجرای بنده را</p></div>
<div class="m2"><p>افزاید ازکرم دارای نامدار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بالله اشارتی‌گر از تو سر زند</p></div>
<div class="m2"><p>کامم روا شود ز الطاف شهریار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>وانگه شود مرا از لطف عام تو</p></div>
<div class="m2"><p>امروز به زدی امسال به ز پار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تا غنچه بشکفد در صحن بوستان</p></div>
<div class="m2"><p>تا لاله بردمد در طرف لاله‌زار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بادا خلیل تو چون غنچه شادمان</p></div>
<div class="m2"><p>بادا عدوی تو چون لاله داغدار</p></div></div>