---
title: >-
    قصیدهٔ شمارهٔ ۳ - در ستایش محمّد شاه 
---
# قصیدهٔ شمارهٔ ۳ - در ستایش محمّد شاه 

<div class="b" id="bn1"><div class="m1"><p>دوش که این گرد گرد گنبد مینا</p></div>
<div class="m2"><p>آبله‌گون شد چو چهر من ز ثریا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تند و غضبناک و سخت و سرکش و توسن</p></div>
<div class="m2"><p>از در مجلس درآمد آن بت رعنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه ختن شاه روم شاهد کشمر</p></div>
<div class="m2"><p>فتنهٔ چین شور خلخ آفت یغما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تاجکی از مشک تر گذاشته بر سر</p></div>
<div class="m2"><p>غیرت تاج قباد و افسر دارا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خم خم و چین چین شکن شکن سر زلفش</p></div>
<div class="m2"><p>کرده ز هر سو پدید شکل چلیپا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی سپیدش برادر مه گردون</p></div>
<div class="m2"><p>موی سیاهش پسر عم شب یلدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم مگو یک قبیله زنگی جنگی</p></div>
<div class="m2"><p>تیر و کمان برگرفته از پی هیجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلفش از جنبش نسیم چو رقاص</p></div>
<div class="m2"><p>گاه به پایین فتاد وگاه به بالا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم مگو یک قرابه بادهٔ خلر</p></div>
<div class="m2"><p>زلف مخوان یک لطیمه عنبر سارا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حلقهٔ زلفش کلید نعمت جاوید</p></div>
<div class="m2"><p>مژدهٔ وصلش نوید دولت دنیا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مات شدم در رخش چنانکه تو گفتی</p></div>
<div class="m2"><p>او همه خورشید گشت و من همه حربا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چین نپسندیدمش به چهره اگر چه</p></div>
<div class="m2"><p>شاهد غضبان بود ز عیب مبرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتمش ای شوخ چین به چهره میفکن</p></div>
<div class="m2"><p>خوش نبود پیچ و خم به چهرهٔ برنا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چین و شکن بایدت به زلف نه بر روی</p></div>
<div class="m2"><p>جور و ستم شایدت به غیر نه بر ما</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرکه فروشی مکن ز چهره که در عشق</p></div>
<div class="m2"><p>هیچم از آن سرکه گم نگردد صفرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاهد باید گشاده روی و سخنگوی</p></div>
<div class="m2"><p>دلبر و دلجوی و دلفریب و دلارا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دلبر باید که هر دم از در شوخی</p></div>
<div class="m2"><p>بوسه نماید لبش‌ به طبع‌ تقاضا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سیب زنخدانش وقف عارف و عامی</p></div>
<div class="m2"><p>تنگ نمکدانش نذر جاهل و دانا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کرد شکرخنده‌یی که حکمت مفروش</p></div>
<div class="m2"><p>زشت چه داند رموز طلعت زیبا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>«لعبت شیرین اگر ترش ننشیند</p></div>
<div class="m2"><p>مدعیانش طمع کنند به حلوا»</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حاجب بار ملوک اگر نکند منع</p></div>
<div class="m2"><p>خوان شهان مفلسان برند به یغما</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خار اگر پاسبان نخل نباشد</p></div>
<div class="m2"><p>بر زبر نخلی کس‌نبیند خرما</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زشت به هرجا رود در است به خواری</p></div>
<div class="m2"><p>گر همه باشد ز نسل شاه بخارا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خود نشنیدی مگرکه مایهٔ عشرت</p></div>
<div class="m2"><p>طلعت زیبا بود نه خلعت دیبا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفتمش احسنت ای نگار سخنگوی</p></div>
<div class="m2"><p>وه‌که شکیبم ربودی از لب‌گویا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پیشترک آی تا لب تو ببوسم</p></div>
<div class="m2"><p>کز لب لعل توگشت حل معما</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همچو یکی شیر خشمگین بخروشید</p></div>
<div class="m2"><p>لرزه فتادش ز فرط خشم بر اعضا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت‌که ای مفلس این چه بی‌ادبی بود</p></div>
<div class="m2"><p>خیز و وداعم‌کن و صداع میفزا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر تو بدین مایه دانش از بشرستی</p></div>
<div class="m2"><p>نفرین بادت به جان ز آدم و حوا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کاش‌که سیلی زمین تمام بشوید</p></div>
<div class="m2"><p>کز تو ملوث شده است تودهٔ غبرا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>این‌قدر ای بی‌ادب هنوز ندانی</p></div>
<div class="m2"><p>کز لب من‌کوتهست دست تمنا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هیچ شنیدی به عمر خودکه‌گدایی</p></div>
<div class="m2"><p>تار طمع افکند به‌گردن جوزا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کس لب لعل مرا نیارد بوسید</p></div>
<div class="m2"><p>جزکه ثناگوی شهریار توانا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جستم و از وجد آستین بفشاندم</p></div>
<div class="m2"><p>یک دو معلق زدم چو مردم شیدا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گفتمش الحمد پس توزان منستی</p></div>
<div class="m2"><p>دم مزن ای خوب چهر از نعم ولا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مهتر قاآنی آن منم‌که ز دانش</p></div>
<div class="m2"><p>در همه‌گیتی‌کسم نبیند همتا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مادح خاص خدایگان ملوکم</p></div>
<div class="m2"><p>مدحت او خوانده صبح و شام به هرجا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نرمک‌‌نرمک لبان‌گشوده به خنده</p></div>
<div class="m2"><p>وز لبکانش چکید شهد مهنا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خندان خندان دوید و پیش من آمد</p></div>
<div class="m2"><p>دوخت دو لب بر لبم‌که بوسه بزن‌ها</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>الحق شرم آمدم بدین لب منکر</p></div>
<div class="m2"><p>بوسه زدن بر لبی چو لالهٔ حمرا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کاین لب همچون ز لوی من نه سزا بود</p></div>
<div class="m2"><p>بر لبکی سرخ تر ز خون مصفا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گفتمش ای ترک داده‌گیرد و صد بوس</p></div>
<div class="m2"><p>کز لب لعل تو قانعم به تماشا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>روی ترش‌کرد وگفت‌کبر فروهل</p></div>
<div class="m2"><p>کز تو تولا نکو بود نه تبرا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شاعر و آنگاه رد بوسهٔ شیرین</p></div>
<div class="m2"><p>کودک و آنگاه ترک جوز منقا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مادح شاهی ترا رسدکه بروبد</p></div>
<div class="m2"><p>خاک رهت را به زلف تافته حورا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بوسه بزن مرمرا ز لطف وگرنه</p></div>
<div class="m2"><p>نزد بتان سرشکسته‌گردم و رسوا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در همه عضوم مخیری پی بوسه</p></div>
<div class="m2"><p>از سرم اینک بگیر بوسه بزن تا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>روی و لبم هردو نیک درخور بوسند</p></div>
<div class="m2"><p>این من و اینک تو یا ببوس لبم یا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گفتمش ای ترک ترک این سخنان‌گوی</p></div>
<div class="m2"><p>بس‌کا ازین غمز و رمز و عشوه و ایما</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>با تو خیانت‌کنم هلا بچه زهره</p></div>
<div class="m2"><p>با تو جسارت‌کنم الا بچه یارا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خصلت دزدان و خوی راهزنانست</p></div>
<div class="m2"><p>چشم طمع دوختن به جانب‌کالا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گفت اگرکام من نبخشی امشب</p></div>
<div class="m2"><p>نزد ملک از تو شکوه رانم فردا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گفتم رو روکه‌کار اگر به شه افتد</p></div>
<div class="m2"><p>شاه مرا برگزیند از همه دنیا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شه نخرد شعر دلکش تو به مویی</p></div>
<div class="m2"><p>چون‌کند از روی لطف شعر من اصغا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گفت مزن لاف و عشوه‌کم‌کن از یراک</p></div>
<div class="m2"><p>مایهٔ شعر تو از منست سراپا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گر نکشد سرخ‌گل نقاب ز چهره</p></div>
<div class="m2"><p>بلبل مسکین چگونه برکشد آوا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>شادی خسرو بود ز طلعت شیرین</p></div>
<div class="m2"><p>نالهٔ وامق بود ز الفت عذرا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چهرهٔ یوسف به خواب دیدکه در مصر</p></div>
<div class="m2"><p>ترک وصال عزیزگفت زلیخا</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گفتمش ای ترک در لبان توگویی</p></div>
<div class="m2"><p>رحل اقامت فکنده است مسیحا</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>خنده‌کنان‌گفت‌کاین تعلل تاکی</p></div>
<div class="m2"><p>خیز و بگو مدحی از شهنشه دارا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>غرهٔ او را به چشم‌کردم و در مدح</p></div>
<div class="m2"><p>غره صفت خواندم این قصیدهٔ غرا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تا ز زوالست لایزال مبرا</p></div>
<div class="m2"><p>ملک ملک باد از زوال معرا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>راد محمد شه آنکه آتش قهرش</p></div>
<div class="m2"><p>می بگدازد چو موم صخرهٔ صمّا</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دولت او را نه اولست و نه آخر</p></div>
<div class="m2"><p>شوکت او را نه مقطعست و نه مبدا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شعله‌کشد خنجرش اگر به زمستان</p></div>
<div class="m2"><p>خلق به سرداب‌ها روند زگرما</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کلک‌گهر سلک او چه معجزه دارد</p></div>
<div class="m2"><p>کز شبه آرد پدید لؤلؤ لالا</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نی غلطم نبود این عجب‌که نماید</p></div>
<div class="m2"><p>در شب تاریک جلوه نجم ثریا</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>حفظ تو پوشد ز آب سقف بر آتش</p></div>
<div class="m2"><p>حزم تو بندد ز باد جسر به دریا</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خلق تو خیری‌ دماند از تف آتش</p></div>
<div class="m2"><p>جود تو الماس سازد ازکف دریا</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>حزم تو یارد مدینه ساخت به جیحون</p></div>
<div class="m2"><p>عزم تو تاند سفینه تاخت به صحرا</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>عون تو سازد ز موم جوشن داود</p></div>
<div class="m2"><p>رای تو آرد ز دودگنبد خضرا</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چون ز عدوی تو نام هست و نشان نیست</p></div>
<div class="m2"><p>شاید اگر خوانمش نبیرهٔ عنقا</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>عفو تو ناخوانده است وصف سیاست</p></div>
<div class="m2"><p>قهر تو نشنیده است نام مدارا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>شاها در این قصیده ژرف نگه‌کن</p></div>
<div class="m2"><p>نظم تو آیین ببین و شیوهٔ شیوا</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>هزل من از جد دیگران بود اولی</p></div>
<div class="m2"><p>خاصه چو افتد قبول شاه معلا</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>شعر نشایدش خواندن از در معنی</p></div>
<div class="m2"><p>هرچه به صورت مردفست و مقفا</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>مرثیه دانش نه شعر آنکه چو خوانند</p></div>
<div class="m2"><p>پیچ و خم افتد ز رنج و غصه در امعا</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چهر حسودت ز سیم اشک مفضض</p></div>
<div class="m2"><p>اشک عدویت ز زر چهره مطلا</p></div></div>