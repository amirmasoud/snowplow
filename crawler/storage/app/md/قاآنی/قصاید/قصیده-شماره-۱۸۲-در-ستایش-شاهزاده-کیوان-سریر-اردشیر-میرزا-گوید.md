---
title: >-
    قصیدهٔ شمارهٔ ۱۸۲ - در ستایش شاهزادهٔ کیوان سریر اردشیر میرزا گوید
---
# قصیدهٔ شمارهٔ ۱۸۲ - در ستایش شاهزادهٔ کیوان سریر اردشیر میرزا گوید

<div class="b" id="bn1"><div class="m1"><p>دوش از بر شهزاده اردشیر</p></div>
<div class="m2"><p>آورد مرا نامه‌یی بشیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگرفتم و بوسیدمش وز آن</p></div>
<div class="m2"><p>شد مغز من آکنده از عبیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سیم پراکنده بود مشک</p></div>
<div class="m2"><p>بر شیر پریشیده بود قیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شنوا شده از لفظ او اصم</p></div>
<div class="m2"><p>بینا شده از خط او ضریر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی سر زلفین خویش حور</p></div>
<div class="m2"><p>بگسسته و پیچیده در حریر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا ماهیکی چند مشک رنگ</p></div>
<div class="m2"><p>افتاده به سیمابی آبگیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بشنوم آن لفظ دلپسند</p></div>
<div class="m2"><p>تا بنگرم آن خط دلپذیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون دل شده اعضای من سمیع</p></div>
<div class="m2"><p>چون ‌جان ‌شده اجزای‌ من بصیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هی خواندی و هی‌کردم آفرین</p></div>
<div class="m2"><p>بر کلک ملک‌زاده اردشیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از هر ستمی دهر را پناه</p></div>
<div class="m2"><p>از هر فزعی خلق را مجیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون بحر به همت دلش عمیق</p></div>
<div class="m2"><p>چون ابر به بخشش‌کفش مَطیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ملکش ز سمک بود تا سماک</p></div>
<div class="m2"><p>صیتش ز ثری رفته تا اثیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جودش پی بخشش بهانه‌جو</p></div>
<div class="m2"><p>عزمش پی‌کوشش بهانه‌گیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در خصم عتابش جهنده‌تر</p></div>
<div class="m2"><p>از آتش تنور در فطیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در سنگ سهامش دونده‌تر</p></div>
<div class="m2"><p>از پنجهٔ خباز در خمیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درکوه سنانش خلنده‌تر</p></div>
<div class="m2"><p>از سوزن خیاط در حریر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دنیا بر ملکش‌کم از طسوج</p></div>
<div class="m2"><p>دریا بر جودش کم از نفیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در چنبر حکمش نه آسمان</p></div>
<div class="m2"><p>زانگونه‌که تدویر در مدیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر درگه قدرش فلک غلام</p></div>
<div class="m2"><p>در ربقهٔ حکمش جهان اسیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ترسد ز جهانسوز تیغ او</p></div>
<div class="m2"><p>زانست‌ که دوزخ ‌کشد زفیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه چرخ ز سهمش چنان نفور</p></div>
<div class="m2"><p>کز هستی خود می‌کشد نفیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درگوش مخاطب جهد ز حرص</p></div>
<div class="m2"><p>بی‌سعی زبان وصفش از ضمیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای چرخ به عون تو مستعین</p></div>
<div class="m2"><p>ای دهر به لطف تو مستجیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صیت قلمت بحر و برگرفت</p></div>
<div class="m2"><p>با آنکه‌کسش نشنود صریر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مهری‌ که سنی‌تر ازو نبود</p></div>
<div class="m2"><p>با رای تو چون ذره شد حقیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بحری‌که غنی‌تر ازو نبود</p></div>
<div class="m2"><p>با جود تو چون قطره شد فقیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>منظورش از آن جزو نام تست</p></div>
<div class="m2"><p>زان طفل‌کندگریه بهر شیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نبود پس نه پردهٔ فلک</p></div>
<div class="m2"><p>رازی‌که نه رایت بر آن خبیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گویی که مجسم شود سرور</p></div>
<div class="m2"><p>آنگه‌که‌کنی جای بر سریر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در مغز خرد یک جهان شعور</p></div>
<div class="m2"><p>باحزم توهمسنگ یک‌شعیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جنبد همه اعضایش از نشاط</p></div>
<div class="m2"><p>چون مدح تو انشاکند دبیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>لرزان تن دوزخ ز تیغ تو</p></div>
<div class="m2"><p>چون پیکر عریان به زمهریر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا حوزهٔ‌ گیهان بود وسیع</p></div>
<div class="m2"><p>تا روضهٔ رضوان بود نضیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عمر ابد و نصرت ازل</p></div>
<div class="m2"><p>آن باد نصیب این یکت نصیر</p></div></div>