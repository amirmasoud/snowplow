---
title: >-
    قصیدهٔ شمارهٔ ۲۰۹ - در ستایش پادشاه اسلام‌پناه ناصرالدین شاه غازی
---
# قصیدهٔ شمارهٔ ۲۰۹ - در ستایش پادشاه اسلام‌پناه ناصرالدین شاه غازی

<div class="b" id="bn1"><div class="m1"><p>ای زلف تو پیچیده‌تر از خط ترسل</p></div>
<div class="m2"><p>بر دامن زلف تو مرادست توسل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریحان خط از زلف شکستهٔ تو نماید</p></div>
<div class="m2"><p>چون عین رقاع از خم طغرای ترسل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلفین تو زاغیست سیه‌کز زبر سرو</p></div>
<div class="m2"><p>بگرفته نگون بچهٔ بازی به دو چنگل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابروی تو بر چهرهٔ خورشید کشد تیغ</p></div>
<div class="m2"><p>گیسوی تو بر گردن ناهید نهد غل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد لب میگون خط خضرای تو گویی</p></div>
<div class="m2"><p>از غالیه بر آب بقا خضرکشد پل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز زلف تو بر رخ نشنیدیم‌ که هرگز</p></div>
<div class="m2"><p>در روم‌ گشاید حبشی دست تطاول</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیچ و خم زلف علی‌رغم حکیمان</p></div>
<div class="m2"><p>تا چشم گشایی همه دورست و تسلسل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلفین تو بر چهر توگویی‌که ستادست</p></div>
<div class="m2"><p>بر درگه قیصر ز نجاشی دو قراول</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای ترک بهارست و دلم سخت فکارست</p></div>
<div class="m2"><p>درمانش چهارست: نی و چنگ و‌ گل و مل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یاد آمدم از حالت مستان به ‌گه رقص</p></div>
<div class="m2"><p>هرگه‌که‌گل از باد درافتد به تمایل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لختی به چمن بگذر و بنگر که چگونه</p></div>
<div class="m2"><p>صلصل به سر سرو درانداخته غلغل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از سبزه و گل سرو بپا سلسله دارد</p></div>
<div class="m2"><p>کافغان‌ کند از دیدن آن سلسله صلصل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گل بلبلهٔ باده به‌کف دارد از شوق</p></div>
<div class="m2"><p>در جوش و خروش آمد زان بلبله بلبل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بالله به چنین فصل مباحست نشستن</p></div>
<div class="m2"><p>با طرفه غزالان ز پی عیش و تغازل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مطرب چه ستادستی بنشین و بزن چنگ</p></div>
<div class="m2"><p>ساقی چه نشستستی برخیز و بده مل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نایی چه شد امروزکه نی می‌نزنی هی</p></div>
<div class="m2"><p>خادم‌که تراگفت‌که می می‌ندهی قل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا نی نزنی می نخورم چند تانی</p></div>
<div class="m2"><p>تا می ندهی خوش نزیم چند تأمل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ترکا تو هم از چهرهٔ خود مجمری افروز</p></div>
<div class="m2"><p>در زلف بر او عود نه از خال قرنفل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر عقده که بینی به دل تنگ من امروز</p></div>
<div class="m2"><p>بگشای و بزن بر خم آن طره وکاکل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برخیز و بده باده بنه ناز و تفرعن</p></div>
<div class="m2"><p>بنشین و بده بوسه بهل ناز و تدلل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نقل می تلخم چه به از بوسهٔ شیرین</p></div>
<div class="m2"><p>کردیم تعقل به ازین نیست تنقل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ها بوسه بده جان پدر چند تحاشی</p></div>
<div class="m2"><p>هی باده بخور جان پسر چند تعلل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>می نوش و مخور غصه که با مشعلهٔ می</p></div>
<div class="m2"><p>از مشغلهٔ دهر توان‌کرد تغافل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر سنبل و نسرین بچم امروز که روزی</p></div>
<div class="m2"><p>ترسم‌ که چو من روید نسرینت ز سنبل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آوخ ‌که جوانی به هنر صرف نمودیم</p></div>
<div class="m2"><p>تا بو که به پیری‌ کندم بخت تکفل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفتم به فلک چون زنم اعلام فصاحت</p></div>
<div class="m2"><p>در خاک چو قارون رودم گنج تمول</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کی بودگمانم‌که چو فوارهٔ آبم</p></div>
<div class="m2"><p>آغاز ترقی بود انجام تنزل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کی داشتم این ظن که به من ‌عجب فروشند</p></div>
<div class="m2"><p>آن قوم که عنصر نشناسند ز غنصل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نی‌نی که همی پَستَیم از قوّت هستیست</p></div>
<div class="m2"><p>چون میوه‌که از شاخ درافتد ز تثاقل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سیلم که چو انبوه شود بر ز بر کوه</p></div>
<div class="m2"><p>از قلهٔ‌ کهسار کند قصد تسفل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن اشتر مستم که مهارم کند ار چرخ</p></div>
<div class="m2"><p>از فرط تدلّل نگریم به تذلّل</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر چیز که تا روز و شب آید برود باز</p></div>
<div class="m2"><p>باقی نزید هیچ اگر عز و اگر ذل</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هرکارکه مشکل شود از جهل جهانم</p></div>
<div class="m2"><p>حالی به خود آسان ‌کنم آن را به تجاهل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>الحمد که از همت پاکان جهان نیست</p></div>
<div class="m2"><p>چون جوهر جان جسم مرا بیم تحول</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون شیر دهد طعمه‌ام از مغز پلنگان</p></div>
<div class="m2"><p>تا بسته مرا عشق به زنجیر توکل</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>قاآنی مهراس ازین چرخ ستمکار</p></div>
<div class="m2"><p>کز لاشهٔ عصفور بنهراسد طغرل</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بر دامن اجلال ولیعهد بزن دست</p></div>
<div class="m2"><p>تا وارهی از چنگ غم و ننگ تملّل</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فهرست بقا معنی جان صورت اقبال</p></div>
<div class="m2"><p>قاموس خرد کنز ادب‌ گنج تفضل</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سلطان جهان ناصر دین خسرو منصور</p></div>
<div class="m2"><p>سالار جهان فخر زمان شاه تناسل</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای دایرهٔ چرخ نهم خنگ ترا تنگ</p></div>
<div class="m2"><p>وی اطلس‌گردون برین رخش ترا جل</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بگرفته به ‌کف چرخ عصا از خط محور</p></div>
<div class="m2"><p>تا بو که شود در صف بار تو یساول</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ارواح حقایق همه عضوند و تویی روح</p></div>
<div class="m2"><p>اشباح دقایق همه جزوند و تویی کل</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا کوکبهٔ ناصریت گشت پدیدار</p></div>
<div class="m2"><p>هر روز به نام تو زند بخت تفال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر حزم رزین تو شود حافظ اجسام</p></div>
<div class="m2"><p>اجسام جهان وارهد از ننگ تخلخل</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ور پرتو تیغ تو بر اصلاب بتابد</p></div>
<div class="m2"><p>تا حشر ز ارحام شود قطع تناسل</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>حزمت دو جهان را به یکی دانه دهد جای</p></div>
<div class="m2"><p>با آنکه در اجسام روا نیست تداخل</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هاروت به عزم تو اگر معتصم آید</p></div>
<div class="m2"><p>پران به سوی عرش چمد از چه بابل</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تیغت شده مدقوق ز آسایش‌ کشور</p></div>
<div class="m2"><p>زان چو مه نو بینیش از رنج تضایل</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شخص تو ز انداد برد گوی فضیلت</p></div>
<div class="m2"><p>عدل تو در اضداد نهد رسم تعادل</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>حزمت بسزا داد جهان داده و اینک</p></div>
<div class="m2"><p>در فکر که چون وارهد از ننگ تعطل</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>توحید موحد را انصاف توکافیست</p></div>
<div class="m2"><p>کاشیا همه یکسان شده از فرط تشاکل</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از مشرق و مغرب همه شاهان جهان را</p></div>
<div class="m2"><p>سهم تو درافکنده به تهدین و تراسل</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اصل همه شاهان تویی و هرکه به جز تست</p></div>
<div class="m2"><p>ناخوانده غریبیست‌ که آید به تطفل</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زانسان ‌که مراد شعرا مدح ملوک ست</p></div>
<div class="m2"><p>هرچند مقدم به مدیحست تغزل</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>در عهد تو اضداد به انداد شبیهند</p></div>
<div class="m2"><p>از بسکه فکندی به میان رسم تماثل</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>از مشرق و مغرب همه را دست درازست</p></div>
<div class="m2"><p>کز خوان نوال تو نمایند تناول</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تا طیّ جدل‌ کرده‌یی از راه‌ کفایت</p></div>
<div class="m2"><p>تا راه طلب بسته‌یی از دست تطاول</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در نحو نخواند دگر باب تنازع</p></div>
<div class="m2"><p>در صرف نبیتتد دگر وزن تفاعل</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>هر چیز که محدود بود شکل پذیرد</p></div>
<div class="m2"><p>زان جاه تو بیرون بود از حد تشکل</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>در نظم عناصر شود ار حزم تو ناصر</p></div>
<div class="m2"><p>قاصر شود از دامنشان دست تبدل</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>آن‌گونه پلیدست رویت‌ که ز نصرت</p></div>
<div class="m2"><p>از کشتن او طبع ترا هست تکاهل</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چون عورت عمرو است‌ تو گویی که به صفین</p></div>
<div class="m2"><p>بنمود که رست از سخط فارس دلدل</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>حزم تو اگر مانع عزم تو نبودی</p></div>
<div class="m2"><p>نه مه نبدت در رحم مام تمهل</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>حیرانم از آن درج عفافی که به نه مه</p></div>
<div class="m2"><p>حمل دو جهان روح همی‌ کرد تحمّل</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>احسنت بر آن اختر عفت که جهان را</p></div>
<div class="m2"><p>از طالع مولود تو بخشید تجمّل</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>آن عصمت عظمی‌که ز مستوری و دانش</p></div>
<div class="m2"><p>اوصاف جمیلش نکند عقل تعقل</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ور فی‌المثل آید به تخیّل صفت او</p></div>
<div class="m2"><p>صد پرده‌ کشد دست عفافش به تخیّل</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>در حافظه ‌گر عصمت او نقش پذیرد</p></div>
<div class="m2"><p>در حافظه نسیان نبرد ره به تمحل</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>برکوه اگر نقش عفافش بنگارند</p></div>
<div class="m2"><p>آن کوه ز صد زلزله ناید به تزلزل</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>تا طی مسالک نتوان‌کرد به ایدی</p></div>
<div class="m2"><p>تا کسب صنایع نتوان کرد به ارجل</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>احکام تو را با قلم خط شعاعی</p></div>
<div class="m2"><p>بر دیده نگاراد خور از بحر تجلل</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بر هرچه ‌کند رای تو ایما به دو ابرو</p></div>
<div class="m2"><p>بر دیده نهدکلک تو انگشت تقبل</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>تا هست تساوی دو خط شرط توازی</p></div>
<div class="m2"><p>دو زاویه‌یی را که بهم هست تبادل</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>از چار‌جهت باد مقابل به تو نصرت</p></div>
<div class="m2"><p>از چار جهت تاکه برون نیست تقابل</p></div></div>