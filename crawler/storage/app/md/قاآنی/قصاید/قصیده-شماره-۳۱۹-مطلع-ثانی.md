---
title: >-
    قصیدهٔ شمارهٔ ۳۱۹ - مطلع ثانی
---
# قصیدهٔ شمارهٔ ۳۱۹ - مطلع ثانی

<div class="b" id="bn1"><div class="m1"><p>جشنی ز نوروز عجم‌ کاراستش جمشید جم</p></div>
<div class="m2"><p>جشنی که با کون و علم شاه جهاندار آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یعنی شجاع‌السلطنه آنکو ز قلب و میمنه</p></div>
<div class="m2"><p>همرزم صد تن یک‌تنه در دشت پیکار آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اسکندر دارا خدم دارای اسکندر حشم</p></div>
<div class="m2"><p>سالار افریدون علم سلم سپهدار آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از لطف ‌و قهرش ‌این ‌زمان ‌شد آشکارا در جهان</p></div>
<div class="m2"><p>زان مرکز آب روان زین مرکز نار آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لرزان تن ‌کاووس ازو ترسان روان طوس ازو</p></div>
<div class="m2"><p>در رزمگه ‌کاموس ازو چون نقش دیوار آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آرش ‌فکار از تیر او گرشاسب از شمشیر او</p></div>
<div class="m2"><p>در حیطه ‌تسخیر او هفت و شش ‌و چار آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگه‌که شمشیر آخته روی زمین پرداخته</p></div>
<div class="m2"><p>گردون سپر انداخته عاجز ز پیکار آمده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گردان ستوه از رزم اوگردون خجل از بزم او</p></div>
<div class="m2"><p>ثابت به پیش عزم او هر هفت سیار آمده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تاگیردش اندر جهان مانند مرکز در میان</p></div>
<div class="m2"><p>ز آغاز شکل آسمان بر شکل پرگار آمده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گردون‌ کباب مهر او مست شراب مهر او</p></div>
<div class="m2"><p>فیض سحاب مهر او بر کشت احرار آمده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مه نعل سم مرکبش ‌گردون روان در موکبش</p></div>
<div class="m2"><p>تابنده نورکوکبش مرآت انوار آمده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای‌ کاخ‌ کیوان جای تو مه سوده سر بر پای تو</p></div>
<div class="m2"><p>تابنده روز از رای تو همچون شب تار آمده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زانصاف تو جان زمان هستند در خواب امان</p></div>
<div class="m2"><p>جز بخت تو کاندر جهان پیوسته بیدار آمده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اجرام انجم نیت این تابنده هر ساعت چنین</p></div>
<div class="m2"><p>رشحیست بر چرخ برین‌کز ابر آذار آمده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر قطره‌یی کاندر هوا باریده از ابر عطا</p></div>
<div class="m2"><p>از شرم جودت قهقرا بر چرخ دوّار آمده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>الای گردون پست تو هستی جود از هست تو</p></div>
<div class="m2"><p>غمگین ‌ز فیض ‌دست‌ تو صد همچو زخار آمده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاها به قاآنی نگر خاقانی ثانی نگر</p></div>
<div class="m2"><p>نی روح خاقانی نگر اینک به‌گفتار آمده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نا برزند از کوه سر خورشید خاور هر سحر</p></div>
<div class="m2"><p>در شرق‌وغرب‌و بحر و بر نورش‌نمودار آمده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تابنده بادا اخترت بر سر ز خورشید افسرت</p></div>
<div class="m2"><p>زان‌رو که رای انورت خورشید آثار آمده</p></div></div>