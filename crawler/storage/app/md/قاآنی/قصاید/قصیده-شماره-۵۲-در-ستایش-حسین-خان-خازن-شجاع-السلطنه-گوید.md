---
title: >-
    قصیدهٔ شمارهٔ ۵۲ - در ستایش حسین خان خازن شجاع السلطنه گوید
---
# قصیدهٔ شمارهٔ ۵۲ - در ستایش حسین خان خازن شجاع السلطنه گوید

<div class="b" id="bn1"><div class="m1"><p>تا ابد چشم بد ازگنجور دارا دور باد</p></div>
<div class="m2"><p>بحر وکان خالی زگنج همت‌گنجور باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن حسین اسم حسن رسمی ‌که چشم روزگار</p></div>
<div class="m2"><p>از می مینای مهرش جاودان مخمور باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه ‌چون معمار جودش قصد آبادی‌ کند</p></div>
<div class="m2"><p>آسمان در آستانش‌کمترین مزدور باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر فروغ طلعتش هرگه‌که بگشایند چشم</p></div>
<div class="m2"><p>دیدهٔ احباب روشن چشم اعدا کور باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسمان را هست مهر و مُهر شه در دست او</p></div>
<div class="m2"><p>تا ابد از لطف شه ‌کارش بدین‌ دستور باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیر این ‌کش پشکارانند با این هر نفس</p></div>
<div class="m2"><p>از شهش بر منصبی از منشیان منشور باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیشکارانی‌ که بر خرم روانشان از سپهر</p></div>
<div class="m2"><p>هر زمان فرخ نوید سعیکم مشکور باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر نوایی‌ کار غنون‌ساز فلک آرد پدید</p></div>
<div class="m2"><p>با نوای سازبختش زاد فی‌الطنبور باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باد دایم محرم درگاه دارای جهان</p></div>
<div class="m2"><p>آنکه تا جاوید جیش ناصرش منصور باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خسرو غازی بهادر شه حسن آنکو مدام</p></div>
<div class="m2"><p>در شبستانش عروس عافیت مستور باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاک راه باد پایش توتیای چشم چرخ</p></div>
<div class="m2"><p>نعل سم ابرشش تاج سر فغفور باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای جهانداری‌ که درکریاس جاهت پاسبان</p></div>
<div class="m2"><p>قیصر و رای‌و نجاشی‌و تکین‌ و فور باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تیغت ارچه هست چون‌سیماب لیکن در مصاف</p></div>
<div class="m2"><p>از برای قطع نسل دشمنان ‌کافور باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چند روزی چون اتابک ‌گر نمودی عزم فارس</p></div>
<div class="m2"><p>بازگشتت باز چون سنجر به نیشابور باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جاودان در چنگل شاه و در چنگال شیر</p></div>
<div class="m2"><p>ز احتسابت جای غرم و لانهٔ‌ عصفور باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیکخواه از ظل چتر رایتت آسوده حال</p></div>
<div class="m2"><p>بدسگال از فر بخت قاهرت مقهور باد</p></div></div>