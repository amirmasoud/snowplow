---
title: >-
    قصیدهٔ شمارهٔ ۲۶۳ - د‌ر مدح شاهنشاه مبرور محمد شاه مغفور انارلله برهانه می‌فرماید
---
# قصیدهٔ شمارهٔ ۲۶۳ - د‌ر مدح شاهنشاه مبرور محمد شاه مغفور انارلله برهانه می‌فرماید

<div class="b" id="bn1"><div class="m1"><p>بارهاگفته‌ام ای ری به تو این راز نهان</p></div>
<div class="m2"><p>ای ری و راز ز نستوده نباید پژمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که ملک روح و تویی دل نزید دل بی‌روح</p></div>
<div class="m2"><p>که‌ کیا جان و تویی تن نزید تن بی‌جان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرودینست شنهشاه و تو بستان لیکن</p></div>
<div class="m2"><p>فرودین چون برود فر برود از بستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حلم شه لنگر و تو کشتی و گیهان دریا</p></div>
<div class="m2"><p>ناخدا دهر و بلا موج و حوادث طوفان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناخدا کشتی بی‌لنگر را چون آرد</p></div>
<div class="m2"><p>ایمن از موجه و طوفان و بلا و حدثان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود گرفتم ‌که تو گیهانی انصاف بده</p></div>
<div class="m2"><p>که ابی بار خدا هیچ نپاید گهیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای ری هیچ مدان هیچ نیاری به خیال</p></div>
<div class="m2"><p>یاد آن سال‌که شاه همه‌دان در همدان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که زبر زیر شدت زیر زبر از زلزال</p></div>
<div class="m2"><p>یعنی ایوانت درگه شد و درگه ایوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زیبق آکندی در گوش و بنشنیدی پند</p></div>
<div class="m2"><p>ناز زلزال تنت لرزان شد زیبق‌سان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وینک امسال از آن رنج‌ که نامش نبرم</p></div>
<div class="m2"><p>نبودت نامی از نام و نشانی ز نشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بارها گفتم از دامن شه دست مدار</p></div>
<div class="m2"><p>که‌گریبان ز تحسّر ندری تا دامان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرچه ‌گفتم همه را ژاژ شمردی و مزیح</p></div>
<div class="m2"><p>هی سرودی که مکن طیبت و مسرا هذیان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که مکینست شهنشاه و مکانستم من</p></div>
<div class="m2"><p>واحتیاجست به‌ناچار مکین را به مکان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ژاژها گفتی ای ری ‌که اگر شرح دهم</p></div>
<div class="m2"><p>همه‌گویند مگو در حق ری این بهتان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لاغها راندی ای ری‌که‌گر انصاف بدی</p></div>
<div class="m2"><p>به دهانت اندر ننهاد میی یک دندان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مثل شاه و تو دانی به چه ماند ای ری</p></div>
<div class="m2"><p>مثل مغز و خرد چشم و ضیا جسم و روان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یونسست این شه و بارهٔ تو چو بطن ماهی</p></div>
<div class="m2"><p>یوسفست این شه و قلعهٔ تو چوکنج زندان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شه چمد زی تو بلی نبود بی‌مصلحتی</p></div>
<div class="m2"><p>مصطفی در غار ار وقتی‌گردد پنهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای ری این گفته ملال آرد صد شکر که باز</p></div>
<div class="m2"><p>شه گرایید از اسپاهان سوی تو عنان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باز چون خاطر احباب ملک ‌گشت آباد</p></div>
<div class="m2"><p>بر و بوم تو که بد چون دل دشمن ویران</p></div></div>