---
title: >-
    قصیدهٔ شمارهٔ ۸۹ - در ستایش‌ کهف ا‌لا‌دانی و الاقاصی وزیر بی‌نظیر جناب حاجی آقاسی
---
# قصیدهٔ شمارهٔ ۸۹ - در ستایش‌ کهف ا‌لا‌دانی و الاقاصی وزیر بی‌نظیر جناب حاجی آقاسی

<div class="b" id="bn1"><div class="m1"><p>از شب نرفته دوش پاسی دو بیشتر</p></div>
<div class="m2"><p>من‌ پاسدار آنک آن مه ‌کند گذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هردم به خویشتن‌گویان به زیر لب</p></div>
<div class="m2"><p>کایدون شب مرا طالع شود سحر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بربوی آنکه ‌کی خورشید سر زند</p></div>
<div class="m2"><p>می‌رفت وقت من با بوک و با مگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسته روان دو چشم بر چرخ تیره جرم</p></div>
<div class="m2"><p>وز روشنان چرخ در چشم من سهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس فکرها که‌ کرد اندر دلم‌ گذار</p></div>
<div class="m2"><p>بر طمع اینکه یار بر من‌ کند گذر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گردون باژگون بر من نمود عِرض</p></div>
<div class="m2"><p>از سیر دم به دم بس‌اگونگو صور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تمثالهای نغز بارو‌ی تابناک</p></div>
<div class="m2"><p>آورد نو به نو از پشت یکدگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتی نشسته‌اند در آبگون غراب</p></div>
<div class="m2"><p>خوبان قندهار ترکان غاتفر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کیوان نموده چهر چون پیر منحنی</p></div>
<div class="m2"><p>بهرام تفته رخ چون ترک کینه‌ور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناهید و مشتری چون اهل زهد و لهو</p></div>
<div class="m2"><p>آن ارغنون به ‌کف این طیلسان بسر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ماهی و گاو را جایی شده مقام</p></div>
<div class="m2"><p>خرچنگ و شیر را سویی شده مقر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم خوشه هم بره بی‌دانه و سُروی</p></div>
<div class="m2"><p>هم‌کژدم و کمان بی‌چشم و بی وتر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نسر و سماک او بد جفت و بر خلاف</p></div>
<div class="m2"><p>آن رامح این به‌عزل آن ساکن این‌ بپر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گردان بنات نعش ‌گر‌د جدی چنانک</p></div>
<div class="m2"><p>افلاک را مدار پیرامن مدر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتی‌که آسمان‌گردیده آسکون</p></div>
<div class="m2"><p>زو ماهیان سیم آورده سر به‌در</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یا نی یکی ارم آکنده از سمن</p></div>
<div class="m2"><p>یا نی یکی صدف آموده از دُرر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من بر مدار چرخ بردوخته دو چشم</p></div>
<div class="m2"><p>تاکی زمان هجر آید همی به‌سر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تاگاه آنکه ماه بنشست بر زمین</p></div>
<div class="m2"><p>ناگاه بر فلک برخاست بانگ در</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زان سهمگین صدا جستم فرا ز جا</p></div>
<div class="m2"><p>آسیمه سر دوان رفتمش ‌بر اثر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هم برگمان غیر اندر دلم هراس</p></div>
<div class="m2"><p>هم با خیال یار اندر سرم بط‌ر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با خوف و با رجا گفتم‌ کیی هلا</p></div>
<div class="m2"><p>کاین ‌وقت ‌شب ‌گذشت ‌نتوان به بوم و بر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دزدی و یا قرین در صلح یا به‌ کین</p></div>
<div class="m2"><p>باری‌ که‌یی چه‌یی بنمای و برشمر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با خشم‌ گفت هی هوش حکیم بین</p></div>
<div class="m2"><p>کا-‌ه‌از آشنا نشناسد از دگر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بگش‌ای در مایست تا بنگری‌که‌کیست</p></div>
<div class="m2"><p>ای دلت منتظر ای جانت محتضر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در باز کردمش حیران و تن زده</p></div>
<div class="m2"><p>تا بنگرم‌ که‌ کیست آن دزد خانه‌بر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون بنگریستم دزدیده زیر چشم</p></div>
<div class="m2"><p>دیدم ‌که بود یار آن ترک سیمبر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از شو‌ق مقدمش چرخی زدم سه چار</p></div>
<div class="m2"><p>می‌خواست از تنم ‌کردن روان سفر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفتم به چشم من بخ‌بخ درآ درآ</p></div>
<div class="m2"><p>ای شمع کاشغر ای سرو کاشمر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بردمش در وثاق‌ گفتمش از وفاق</p></div>
<div class="m2"><p>هان برفکن‌کله هین برگشاکمر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بنشست و برفکند از روی دلبری</p></div>
<div class="m2"><p>زان چهر دلستان آن زلف دل‌شکر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گفتی طلوع‌کرد در آن فضای تنگ</p></div>
<div class="m2"><p>یک چرخ مشتری یک آسمان قمر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خالش به تیرگی آزرم زنگبار</p></div>
<div class="m2"><p>چهرش به روشنی آشوب‌ کاشغر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>قد یک بهشت سرو رخ یک سپهر ماه</p></div>
<div class="m2"><p>این ماه سرو چرخ آن سرو ماه‌بر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از زلف خم به‌خم یک شهربند ه دام</p></div>
<div class="m2"><p>از چشم باسقم یک دهر شور و شر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سنگیش در بغل باغیش در رخان</p></div>
<div class="m2"><p>کوهیش در ازار موییش در کمر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>لب یک ‌بدخش لعل خط یک تتار مشک</p></div>
<div class="m2"><p>لعلی‌ گهرفشان مشکی قمر سپر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رخسار و زلف او جبریل و اهرمن</p></div>
<div class="m2"><p>گفتار و لعل او یاقوت و نیشکر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یاقوت را بود گر نیشکر بدل</p></div>
<div class="m2"><p>جبریل را بودگر اهرمن به بر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چشمش‌ گه نگه‌ گفتی‌ که بسته است</p></div>
<div class="m2"><p>در هر سر مژه صد جعبه نیشتر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مطبوع و دلربا از فرق تا قدم</p></div>
<div class="m2"><p>منظور و دلنشین از پای تا به سر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شاید که تاجری از شرم پیکرش</p></div>
<div class="m2"><p>در پارس‌ ناورد دیبای شوشتر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>باری نگار من ننشسته بر بساط</p></div>
<div class="m2"><p>گفتا شراب سرخ آور به جام زر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>داری به چهر من تاکی نظر هلا</p></div>
<div class="m2"><p>برخیز و برفکن درکار می نظر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بی‌نقل و بی‌نبید دل را رسد حزن</p></div>
<div class="m2"><p>بی‌جام و بی‌قدح جان را بود خط‌ر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گرچه بودگنه مندیش‌ و می بده</p></div>
<div class="m2"><p>با فضل‌ کردگار جرمست مُغتفر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>برجسته در زمان آوردمش به پیش</p></div>
<div class="m2"><p>زان جوهر خرد زان پایهٔ ظفر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زان می‌که مور ازو گر قطره‌یی خورد</p></div>
<div class="m2"><p>در حمله برکند چنگال شیر نر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زان می ‌که گر فروغش‌ افتد به شوره‌زار</p></div>
<div class="m2"><p>خاکش شود سمن سنگش شود گهر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>زان می ‌که جسم ازو یکسر خرد شود</p></div>
<div class="m2"><p>نارفته در گلو نگذشته در جگر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>وان رشک حور عین از شیشه ی بلور</p></div>
<div class="m2"><p>در جام زر فکند آن لعل معصفر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چون خورد ساغری پر کرد دیگری</p></div>
<div class="m2"><p>بر من بداد و گفت ای مرد هوشور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از می شدن خراب آید نکوترم</p></div>
<div class="m2"><p>چون منقلب بود اوضاع دهر در</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بگذشته زان ‌که مرد اندر طریق فقر</p></div>
<div class="m2"><p>مقبول‌تر بود چندان‌که بی‌خبر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مظور چون یکیست از این همه برون</p></div>
<div class="m2"><p>با این رمه چری تاکی به جوی و جر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تن خانه فناست ویران شدنش به</p></div>
<div class="m2"><p>جان آیت بقاست آباد خوبتر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در پیش عاشقان هستی بود و بال</p></div>
<div class="m2"><p>درکیش بیدلان مستی بود هنر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تن کوی خواهشست دل کاخ آرزو</p></div>
<div class="m2"><p>زین کوی شو برون زین کاخ رو بدر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در عالم بقا بس عیشها کنی</p></div>
<div class="m2"><p>بتوانی ارگذشت زین عیش مختصر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>از خویش درگذرگر یار بایدت</p></div>
<div class="m2"><p>تا هستی تو هست یارست مستتر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>در جلوه‌‌گاه دوست بود توشد حجاب</p></div>
<div class="m2"><p>این پرده برفکن آن جلوه درنگر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>از هٔد هست و نیست وارسته شو هلا</p></div>
<div class="m2"><p>گر در حریم دوست بایدت مستقر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>وارستگی بهست از قید کفر و دین</p></div>
<div class="m2"><p>وارستگی خوشست از فکر نفع و ضر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>زین چار مادرت باید گریختن</p></div>
<div class="m2"><p>خواهی‌مسیح‌وش‌ گر رفت زی پدر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>هرکس طلب‌ کند با یار خرگهی</p></div>
<div class="m2"><p>وصل مدام را در شام و در سحر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>سودای عم و خال دارد همی وبال</p></div>
<div class="m2"><p>برخیز و از جهان بگریز و از پسر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>وارستگان نهند بر فرق چرخ پای</p></div>
<div class="m2"><p>آزادگان زنند با آفتاب بر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>وارسته در جهان دانی‌کنون ‌کی است</p></div>
<div class="m2"><p>مولای نامدار دستور نامور</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>گردون هنگ و هش دریای عز و مجد</p></div>
<div class="m2"><p>گیهان داد و دین دنیای فال و فر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>آقاسی آنکه هست شخصش درین جهان</p></div>
<div class="m2"><p>چون روح در بدن چون نور در بصر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>جودش چو فیض ‌ابر نازل به ‌خار و گل</p></div>
<div class="m2"><p>فیضش‌چو نور مهر شامل‌به خشک و تر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ازکاخ قدر او طاقیست نه رواق</p></div>
<div class="m2"><p>از ملک جاه او شبریست بحر و بر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>نفس شریف اوست گر هیچ جلوه‌کرد</p></div>
<div class="m2"><p>تأیید آسمان در کسوت بشر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>هرچند بوالبشر نسرایمش ولیک</p></div>
<div class="m2"><p>امروز خلق را باشد همی پدر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بر یاد قهر او سم زاید از عسل</p></div>
<div class="m2"><p>وز باد مهر او گل روید از حجر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>با ابر دست او ابرست چون دخان</p></div>
<div class="m2"><p>با بحر طبع‌ او بحرست چون شمر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>در حفظ مملکت ‌کلکش قو‌یترست</p></div>
<div class="m2"><p>از رمح سام یل از تیر زال زر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>او قطب وقت و دهر گردان به‌گرد او</p></div>
<div class="m2"><p>چونان‌ نه فلک پیراهن مدر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>دل در هوای او نیندیشد از جنان</p></div>
<div class="m2"><p>جان با ولای او نهراسد از سفر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بر هرچه امر اوست اجرا دهد قضا</p></div>
<div class="m2"><p>بر هرچه حکم اوست اذعان‌کند قدر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>آنجا که قدر اوست‌ گردون بود زمین</p></div>
<div class="m2"><p>آنجاکه قهر اوست دوزخ بود شرر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>با عزم ثاقبش‌ صرصر بود گران</p></div>
<div class="m2"><p>با رای روشنش‌ انجم بود کدر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>در حفظ تن بود نامش به روز کین</p></div>
<div class="m2"><p>بهتر ز صد سپاه افزون ز صد سپر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>آنجاکه تیغ اوست از امن نی نشان</p></div>
<div class="m2"><p>آنجاکه‌کلک اوست از ظلم نی خبر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>در عهد عدل او اندر تمام ملک</p></div>
<div class="m2"><p>جایی نمانده است از ظلم وکین اثر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>کلب ه‌گ‌ثث‌ «- است تا روز وایسیا</p></div>
<div class="m2"><p>میزان داد و دین رزّاق رزق بر</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ای صدر راستین ای بدر راستان</p></div>
<div class="m2"><p>کز وصف ذات تو عاجز بود فکر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ایدون‌که درکف یزدان ودیعه هشت</p></div>
<div class="m2"><p>آمال انس و جان ارزاق جانور</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>دورست‌ چون منی‌ ،‌ هشیار نکته‌ دان</p></div>
<div class="m2"><p>در عهد چون تویی بردن چنین خطر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>با آن‌که در سخن همواره ‌کلک من</p></div>
<div class="m2"><p>ریزد به یک نفس یک آسکون غرر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>گاه حساب مال صفرست دست من</p></div>
<div class="m2"><p>بر عیش سالیان زان نبودم ظفر</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>ارجو که جود تو آسوده داردم</p></div>
<div class="m2"><p>از فکر آب و نان از یاد خواب و خور</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>تا در جهان رود از مهر و مه سخن</p></div>
<div class="m2"><p>تا در زمین بود از آب و گل ثمر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>جان عدوی تو از اشک دیده‌ گل</p></div>
<div class="m2"><p>جاه حبیب تو از اوج ماه بر</p></div></div>