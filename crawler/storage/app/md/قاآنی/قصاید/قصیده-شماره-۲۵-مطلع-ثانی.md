---
title: >-
    قصیدهٔ شمارهٔ ۲۵ - مطلع ثانی
---
# قصیدهٔ شمارهٔ ۲۵ - مطلع ثانی

<div class="b" id="bn1"><div class="m1"><p>در همایون ساعتی فرخنده چون عهد شباب</p></div>
<div class="m2"><p>در بهین روزی چو روز وصل خوبان دیریاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مبارکتر دمی‌کز اتصالات سعود</p></div>
<div class="m2"><p>تا ابد در عرصهٔ‌ گیتی نبینی انقلاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلعتی آمدکه‌گویی‌کرده نساج ازل</p></div>
<div class="m2"><p>تارش ازگیسوی حور و پودش از نور شهاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوهر آگین خلعتی ‌کز نور گوهرهای او</p></div>
<div class="m2"><p>نقش هر معنی توان دید از ضمایر بی‌حجاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلعتی‌گر فی‌المثل آن را به دریا افکنند</p></div>
<div class="m2"><p>تا قیامت زو گهر خیزد به جای موج آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمد از ری‌ کش خدا آباد دارد تا به حشر</p></div>
<div class="m2"><p>جانب شیرازکش‌گردون نگرداند خراب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازکه از نزد ولیعهد خدیو راستین</p></div>
<div class="m2"><p>آنکه بادا تا قیامت کامجوی و کامیاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از برای افتخار میر ملک جم‌ که هست</p></div>
<div class="m2"><p>زآتش تیغش دل اعدای شاهنشه کباب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یارب آن تشریف ده را مملکت ده بی‌شمار</p></div>
<div class="m2"><p>یارب این تشریف بر را مرتبت ده بی‌حساب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راستی‌ گویم ندیدست و نه بیند آسمان</p></div>
<div class="m2"><p>هیچ شاهی را ولیعهد چنین نایب مناب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ملک او با انتظام و بخت او با احتشام</p></div>
<div class="m2"><p>باس او با انتقام و عدل او با احتساب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با ولایش هیچ‌کس را نیست پروای‌ گنه</p></div>
<div class="m2"><p>با خلافش هیچ دل را نیست توفیق ثواب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر وزد بر ساحت دوزخ نسیم عفو او</p></div>
<div class="m2"><p>در مذاق اهل دوزخ عَذب گرداند عذاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روزی اندر باغ‌ گفتم از سخای او سخن</p></div>
<div class="m2"><p>برگ هر شاخش زمرد گشت و بارش زر ناب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یاد رای روشنش در خاطرم یک شب گذشت</p></div>
<div class="m2"><p>از بن هر موی من سرزد هزاران آفتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وز خیال جود او برکف‌گرفتم جام می</p></div>
<div class="m2"><p>جام در دشم‌گهر شد می در آن لعل مذاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روز بزمش خاک چون‌ گردون بجنبد از طرب</p></div>
<div class="m2"><p>گاه رزمش آب چون آتش بجوشد زالتهاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نام جودش چون بری یاقوت روید از زمین</p></div>
<div class="m2"><p>یاد تیغش چون‌ کنی الماس بارد از سحاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>التفاتش گر کسی را دست گیرد چون عنان</p></div>
<div class="m2"><p>گردش گردون نسازد پایمالش‌چون رکاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خصم او‌ گفتا خدایا سرفرازم کن به دهر</p></div>
<div class="m2"><p>رُمح او گفتا من این دعوت نمایم مستجاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بحر از جاه وسیع او اگر جوید مدد</p></div>
<div class="m2"><p>هفت دریا را ز وسعت جا دهد در یک حباب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر سراب ار قطره‌یی بارد سحاب جود او</p></div>
<div class="m2"><p>تا قیامت جوی شهد و شیر خیزد از سراب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>روز طوفان ناخدا گر نام پاک او برد</p></div>
<div class="m2"><p>بحر را چون طبع قاآنی نماند اضطراب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رشک جودش بر دل دریا گره بندد ز موج</p></div>
<div class="m2"><p>پاس عدلش بر تن ماهی زره پوشد در آب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گاه خشمش موج دریا خیزد از موج حریر</p></div>
<div class="m2"><p>روز مهرش فر عنقا زاید از پر ذباب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خلقش آن جنّت بود کز یاد آن در هر نفس</p></div>
<div class="m2"><p>عطسهای عنبرین خیزد ز مغز شیخ و شاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا غم آرد تنگدستی خاصه در عهد مشیب</p></div>
<div class="m2"><p>تا طرب خیزد ز مستی خاصه در عهد شباب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بخت او بادا جوان و حکم او بادا روان</p></div>
<div class="m2"><p>رای او بادا مصیب و خصم او بادا مصاب</p></div></div>