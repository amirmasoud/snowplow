---
title: >-
    قصیدهٔ شمارهٔ ۱۲۰ - مطلع ثانی
---
# قصیدهٔ شمارهٔ ۱۲۰ - مطلع ثانی

<div class="b" id="bn1"><div class="m1"><p>زهی‌گرفته تیغ و سنان چه بحر و چه بر</p></div>
<div class="m2"><p>زهی‌گشو‌ده به‌کلک و بنان چه‌خشک و چه‌تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عطای دمبدمت کاروان ملک وجود</p></div>
<div class="m2"><p>کمند خم به خمت نردبان بام ظفر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان تیغ تو ضرغام مرگ را ناخن</p></div>
<div class="m2"><p>جناح چتر تو سیمرغ‌ بخت را شهپر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو نام خنگ ترا بر زبان برد نراد</p></div>
<div class="m2"><p>برون جهد اگرش مهره‌ایست در ششدر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و گر به کان نگرد دشمن ترا آهن</p></div>
<div class="m2"><p>برد گلویش ناگشته ناوک و خنجر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو چون به باغ چمی بهر کندن گل و سرو</p></div>
<div class="m2"><p>به باغبانان چشمک زند همی عبهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به رزم و بزم تو داند مگر به کار آید</p></div>
<div class="m2"><p>که نی نروید از خاک جز که بسته کمر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حدیث تیغ تو تا بر زبان خلق گذشت</p></div>
<div class="m2"><p>بریده ‌گشت حروف هجا ز یکدیگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حلولی ار نه جمال تو دید پس ز چه گفت</p></div>
<div class="m2"><p>حلول‌ کرده خداوند در نهاد بشر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترا و شاه جهان را مگر نصاری دید</p></div>
<div class="m2"><p>که‌گفت روح‌الله مر خدای راست پسر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حکیم گوید جان را به چشم نتوان دید</p></div>
<div class="m2"><p>نکرده است مگر بر شمایل تو نظر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نسیم حزم تو گر بر مشام نطفه وزد</p></div>
<div class="m2"><p>شگفت نیست‌که بالغ شود به پشت پدر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به عقل گفتم با جود ناصری عجبست</p></div>
<div class="m2"><p>که بچه خون خورد اندر مشیمهٔ مادر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جواب دادکه خون خو‌ردنش ز فرقت اوست</p></div>
<div class="m2"><p>غذای مردم مهجور چیست خون جگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قلوب خلق ز مهرت چنان لبالب گشت</p></div>
<div class="m2"><p>که در ضمیر بر اندیشه تنگ شد معبر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خطیب نام ترا چون برد ز وجد و سماع</p></div>
<div class="m2"><p>بر آن شود که بر افلاک پرّد از منبر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ادیب مدح ترا چون کند ز شور نشاط</p></div>
<div class="m2"><p>گمان بری‌که بود مست بادهٔ خلر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به وصف خنگ تو غواص خامه‌ام دی خواست</p></div>
<div class="m2"><p>ز بحر طبع فشاند به نامه سلک درر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نقوش وصفش ازان پیشتر که جنبد کلک</p></div>
<div class="m2"><p>ز بس روانی از دل بجست در دفتر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عجبتر آنکه ز بس چابکست توسن تو</p></div>
<div class="m2"><p>حروف نامش جنبد به نامه چون جانور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنان فضای جهان را گرفته هیبت تو</p></div>
<div class="m2"><p>که می‌نیارد بیرون شدن نگه ز بصر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شها مها ملکا دادگسترا مَلَکا</p></div>
<div class="m2"><p>منم‌که مدح تو شعر مرا بود زیور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سخن به مدح تو گویی ز آسمان آرم</p></div>
<div class="m2"><p>که می‌نریزد از خامه‌ام به جز اختر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو آفتابی و تا گشتی از دو چشمم دور</p></div>
<div class="m2"><p>سیاه شد به جهان بین من جهان یکسر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنان ضعیف شدستم‌که صفحه راکاتب</p></div>
<div class="m2"><p>ز استخوان تن من همی‌کشد مسطر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چه راحتست مرا بی‌حضور حضرت تو</p></div>
<div class="m2"><p>چه هستیست عرض را به طبع بی‌جوهر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کمم ز خاک ‌گرفتی‌ که چون غبار مرا</p></div>
<div class="m2"><p>نبردی افتادن خیزان به همره لشکر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به خاکپای تو کز طعن دشمنان و شب و روز</p></div>
<div class="m2"><p>بحیرتستم و گویم چه روی داد مگر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که شاه ناصردین را ز یاد قاآنی</p></div>
<div class="m2"><p>شود فرامش فالله خالقی اکبر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همیشه تا که رسن تاب از پس آید پیش</p></div>
<div class="m2"><p>که تا رسن را آرد ز حلقه در چنبر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر آنکه سرکشد از چنبر ولای تو باد</p></div>
<div class="m2"><p>قدش چو حلقه نگون جسم چون رسن لاغر</p></div></div>