---
title: >-
    قصیدهٔ شمارهٔ ۲۲۵ - در ستایش پادشاه رضوان ارامگاه محمد شاه غازی طاب‌الله ثراه گوید
---
# قصیدهٔ شمارهٔ ۲۲۵ - در ستایش پادشاه رضوان ارامگاه محمد شاه غازی طاب‌الله ثراه گوید

<div class="b" id="bn1"><div class="m1"><p>عید آمد و عیش ‌آمد و شد روزه و شد غم</p></div>
<div class="m2"><p>زین آمد و شد جان و دلی دارم خرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه رمضان‌گرچه مهی بود مبارک</p></div>
<div class="m2"><p>شوال نکوتر که مهی هست مکرّم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الحمدکه آن واعظک امروز به‌کنجی</p></div>
<div class="m2"><p>چون‌حرف نخشین مضاعف شده مدغم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وان زاهدک از طعنهٔ اوباش خلایق</p></div>
<div class="m2"><p>چون دزد عسس دیده به‌کنجی نزند دم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفت آنکه رود شیخ خرامان سوی مسجد</p></div>
<div class="m2"><p>وز پیش و پسش خیل مریدان معمّم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کبر ز هم برنکند چشم چو اکمه</p></div>
<div class="m2"><p>وز عجب به‌کس می‌نزند حرف چو ابکم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفت آنکه مر آن موذن موذی به مناجات</p></div>
<div class="m2"><p>چون‌گاوکشد نعره‌گهی زیر وگهی بم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وان واعظ و مفتی چو درآیند به مسجد</p></div>
<div class="m2"><p>این عجب مصور شود آن‌کبر مجسم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن باد به حلق افکند این باد به دستار</p></div>
<div class="m2"><p>آن مشک منفخ شود این خیک مورم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وان قاری عاری به‌گه غنه و ادغام</p></div>
<div class="m2"><p>خیشوم بر از باد کند همچو یکی دم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وانگونه ز هم حنجره و حلق‌گشاید</p></div>
<div class="m2"><p>کش پیچ و خم روده هویدا شود از فم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خیز ای بیت و امروز به‌رغم دل واعظ</p></div>
<div class="m2"><p>هی بوسه پیاپی ده و هی باده دمادم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ماه رمضان برنگرفتم ز لبت بوس</p></div>
<div class="m2"><p>کز روزه دلی داشتم آشفته و درهم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بس بوسه‌که درکنج لبت جمع شدستند</p></div>
<div class="m2"><p>چون شهد که گردد به یکی‌ گوشه فراهم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زان لب نکنی بازکه از فرط حلاوت</p></div>
<div class="m2"><p>چون تنگ شکر هر دو لبت دوخته برهم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا بر لب لعل تو ز من وام نماند</p></div>
<div class="m2"><p>برخیز و بده بوسهٔ یک‌ماهه به یکدم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای طرهٔ تو تیره‌تر از دیدهٔ شاهین</p></div>
<div class="m2"><p>وی مژهٔ تو چیره‌تر از ناخن ضیغم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جور تو وفا خار توگل درد تو درمان</p></div>
<div class="m2"><p>رنج تو شفا زهر تو مل زخم تو مرهم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در حلقهٔ زلفین تو تا چثبم‌کندکار</p></div>
<div class="m2"><p>بندست و شکنج و گره و دایره و خم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جز چشم‌ تو کز وی دل من هست هراسان</p></div>
<div class="m2"><p>آهو نشنیدم که ازو شیرکند رم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با یاد سر زلف تو شب تا به سحرگاه</p></div>
<div class="m2"><p>در بستر و بالین چمدم افعی و ارقم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای پستهٔ خندان تو زان رستهٔ دندان</p></div>
<div class="m2"><p>چون حقهٔ یاقوت پر از عقد منظم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در زلف سیاهت همه‌کس ناظر و من نیز</p></div>
<div class="m2"><p>بر ساق سپیدت همه‌کسن مایل و من هم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون ‌حسن ‌تو هر روز شود عشق ‌من افزون</p></div>
<div class="m2"><p>زانست‌که چون حسن تو عشقم نشودکم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بی‌ساعد سیمین توام حال تباهست</p></div>
<div class="m2"><p>بی‌سیم ‌گدا را نبود عیش مسلّم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در سیم سرینت ز طمع دوخته‌ام چشم</p></div>
<div class="m2"><p>کز فقر ندارم به جز اندیشهٔ درهم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زان سیم بخیلی مکن ای ترک ازیراک</p></div>
<div class="m2"><p>ازدادن سیمست همه بخشش حاتم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای ترک برآنم که در این عهد همایون</p></div>
<div class="m2"><p>مردانه شبیخون فکنم بر سپه غم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از زلف تو پوشم زره از جعد تو خفتان</p></div>
<div class="m2"><p>از قد تو سازم علم از موی تو پرچم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وانگه ز پی خطبهٔ این فتح نمایان</p></div>
<div class="m2"><p>شعری‌کنم انشاء به مدح شه اعظم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دارای عجم وارث جم سایهٔ یزدان</p></div>
<div class="m2"><p>خورشید زمین ماه زمان شاه معظّم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شاهنشه آفاق محمد شه غازی</p></div>
<div class="m2"><p>کز پایه براز کی بود از مایه براز جم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای ساحت آفاق ز رای تو منور</p></div>
<div class="m2"><p>وی جبهت افلاک به داغ تو موسم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مهتاب بود مهر تو و حادثه‌کتان</p></div>
<div class="m2"><p>خورشید بود چهر تو و نایبه شبنم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>روی قمر از طعنهٔ رمح تو بود ریش</p></div>
<div class="m2"><p>پشت فلک از صدمهٔ‌گرز تو بود خم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زاید نعم از جود تو چون حرف مشدّد</p></div>
<div class="m2"><p>ناقص ستم از عدل تو چون اسم مرخم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بعد از همه شاهانی و پیش از همه آری</p></div>
<div class="m2"><p>به بود محمدکه سپس بود ز آدم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ذات تو مگر علت غائیست جهان را</p></div>
<div class="m2"><p>کز عهد موخر بود از رتبه مقدم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مانند سلیمان همه عالم بگرفتی</p></div>
<div class="m2"><p>با قوت بازو نه به خاصیت خاتم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بر ذرّهٔ خاک قدمت سجده برد چرخ</p></div>
<div class="m2"><p>در قطرهٔ ابر کرمت غوطه‌ خوردیم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از خیر و شر دور زمان رای تو آگه</p></div>
<div class="m2"><p>بر نیک و بدکار جهان جان تو ملهم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>با لطف تو تریاک دهد چاشنی قند</p></div>
<div class="m2"><p>با قهر تو پازهر دهد خاصیت سم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از ضعف عدد ضعف عدوی تو فزاید</p></div>
<div class="m2"><p>چون‌کسرکز افزونی تربیع شودکم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر حور به جنت شرر تیغ تو بیند</p></div>
<div class="m2"><p>باگیسوی آشفته‌گریزد به جهنم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در معرکهٔ رزم تو از زهرهٔ شیران</p></div>
<div class="m2"><p>تا حشر نروید به جز از شاخ سپر غم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>با جاه تو پستست نهایات نه افلاک</p></div>
<div class="m2"><p>با قدر تو تنگست فراخای دو عالم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شاها به سرم‌گر ز فلک تیغ ببارد</p></div>
<div class="m2"><p>در مهر تو الا به ارادت نزنم دم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تو چشمهٔ حیوانی و من همچو سکندر</p></div>
<div class="m2"><p>از چهر تو محرومم و با مهر تو محرم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دیریست‌که آسوده‌ام از خلق به کنجی</p></div>
<div class="m2"><p>هم مدح توام مونس و هم یاد تو همدم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نه شاکر ازینم‌که خلیلی‌کندم مدح</p></div>
<div class="m2"><p>نه شاکی از آنم‌که حسودی‌کندم ذم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>درکیسهٔ من‌گو نبود درهم و دینار</p></div>
<div class="m2"><p>بر آخور من ‌گو نبود ابرش و ادهم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>با مهر تو بر دوش من این خرقهٔ خلقان</p></div>
<div class="m2"><p>صد بار نکوتر بود از دیبهٔ معلم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کامم همه اینست چو شمشیر تو قاطع</p></div>
<div class="m2"><p>تا حکم فضا هست چو تدبیر تو محکم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>احباب ترا باد به‌کف ساغر عشرت</p></div>
<div class="m2"><p>اعدای ترا باد به‌برکسوت ماتم</p></div></div>