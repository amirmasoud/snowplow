---
title: >-
    قصیدهٔ شمارهٔ ۵۱ - و له ایضاً فی مدحه
---
# قصیدهٔ شمارهٔ ۵۱ - و له ایضاً فی مدحه

<div class="b" id="bn1"><div class="m1"><p>بهادر شه ای شهریاران ‌غلا‌مت</p></div>
<div class="m2"><p>قضا و قدر هردو در اهتمامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خاصان درافتاده غوغای عامی</p></div>
<div class="m2"><p>ز ادراک خاص و ز انعام عامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان آفرین زافرینش ندارد</p></div>
<div class="m2"><p>مرادی دگر جز حصول مرامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برون بود نه چرخ از جمع امکان</p></div>
<div class="m2"><p>اگر بود همپایه با احتشامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دوزخ‌ گریزند ارباب تقوی</p></div>
<div class="m2"><p>کشند ار به فردوس شکل حسامت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پیش رواق توگردون خضرا</p></div>
<div class="m2"><p>گیاهیست روینده از طرف بامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیم قایل شرک لیکن درآید</p></div>
<div class="m2"><p>پس از نام یزدان بهر خطبه نامت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به ایوان طرف را به میدان شغب را</p></div>
<div class="m2"><p>قیام از قعودت قعود از قیامت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز رفعت‌ کند منع تدویر گردون</p></div>
<div class="m2"><p>سنان رماح و قباب خیامت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به عالم درونی و از عالم افزون</p></div>
<div class="m2"><p>چو مضمون وافر زمو جزکلامت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو در حضرت قدس صف ملایک</p></div>
<div class="m2"><p>صفوف سلاطین به صفّ سلامت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر هفت دریا شود جمله ‌گوهر</p></div>
<div class="m2"><p>به هنگام بخشش نیابد تمامت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وگر دست رادت عطا وام دادی</p></div>
<div class="m2"><p>زمین و زمان بود در زیر وامت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کجاگشت عزمت مصمّم به یاری</p></div>
<div class="m2"><p>که حالی نگردید گردون به کامت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کجا آهوی رافتت ‌کرد جولان</p></div>
<div class="m2"><p>که حالی نشد شیر درنده رامت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ژحل لحظه‌یی دورگردون‌کند طی</p></div>
<div class="m2"><p>دهد سیرش از اَبَرَش تیز کامت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تعالی‌الله ای برق تک خنگ دارا</p></div>
<div class="m2"><p>که‌نقش‌است نصرت به زرین ستامت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو آن باد سیری ‌که هنگام جولان</p></div>
<div class="m2"><p>بود درکف باد صرصر زمامت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدانسان‌که روی زمین می‌نوردی</p></div>
<div class="m2"><p>اگر سوی‌ گردون شود یک خرامت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به یک لحظه پویی ز نه چرخ برتر</p></div>
<div class="m2"><p>اگر دست دارا نگیرد لجامت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به هر قطره‌کالای صدگنج بخشد</p></div>
<div class="m2"><p>به گاه کرم دست همچون غمامت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هنوز آسمان پنبه درگوش دارد</p></div>
<div class="m2"><p>ز افغانِ افغان به غوغای جامت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هنوز از وغازان زمین لاله روید</p></div>
<div class="m2"><p>ز خونریزی خنجر لعل فامت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هنوز است صحرا و هامون مغربل</p></div>
<div class="m2"><p>ز آسیب پولاد پیکان سهامت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر پای عفوت نبُد در میانه</p></div>
<div class="m2"><p>برانگیخت دود از جهان انتقامت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بود بر یمین مایهٔ مرگ تیغت</p></div>
<div class="m2"><p>بود بر یسار آیت عیش جامت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خرد فتنه اندر زوایای عالم</p></div>
<div class="m2"><p>برآید چو نیلی پرند از نیامت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>الا تا مُدام آورد شادمانی</p></div>
<div class="m2"><p>بود شادمانی مدام از مدامت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه جز در رواق ریاست نشستت</p></div>
<div class="m2"><p>نه جز بر سریر کیاست مقامت</p></div></div>