---
title: >-
    قصیدهٔ شمارهٔ ۶۱ - در ستایش امیر دیوان میرزا نبی خان فرماید
---
# قصیدهٔ شمارهٔ ۶۱ - در ستایش امیر دیوان میرزا نبی خان فرماید

<div class="b" id="bn1"><div class="m1"><p>آن کیست که باز آمد و در بزم نظر کرد</p></div>
<div class="m2"><p>جان و دل ما از نظری زیر و زبر کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن برق یمانست‌که افتاد به خرمن</p></div>
<div class="m2"><p>یا صاعقه‌یی بود که بر کوه ‌گذر کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیزید و بگیرید و بیارید و بپرسید</p></div>
<div class="m2"><p>زان فتنه که ناگاه سر از خانه بدر کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی هیچ مگویید و مپویید و مجویید</p></div>
<div class="m2"><p>من یافتم آن شعبده ‌کان شعبده‌گر کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن‌یار منست‌ آن و همانست‌ و جز این نیست</p></div>
<div class="m2"><p>صدبار چنین‌کرد و فزون‌کرد و بتر کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این است همان یار که هر روز دو صد بار</p></div>
<div class="m2"><p>ناکرده یکی‌کار ز نوکار د‌رکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه‌آمد و گه خست و گهی ‌رفت و گهی ‌بست</p></div>
<div class="m2"><p>گه ساز سفرکرد و گه آهنگ حضرکرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گه‌صلح وگهی جنگ وگهی نوش و گهی نیش</p></div>
<div class="m2"><p>گه شد ز میان بی‌خبر و گاه خبر کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گاه از بر من رفت و دو صد نوع دغل باخت</p></div>
<div class="m2"><p>گه بر سر من آمد و صدگونه حشر کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گه خادم و گه خائن و گه دشمن و گه دوست</p></div>
<div class="m2"><p>گه دست به خنجر زد و گه سینه سپر کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گه‌ گفت نیم خادم و صدگونه قسم خورد</p></div>
<div class="m2"><p>گه گفت نیم چاکر و صد شورش‌ و شر کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گه خانه‌نشین گشت و گهی خانه نشان داد</p></div>
<div class="m2"><p>گه‌ خون ز رخم شست و گهی خون به ‌جگر کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گه رفت به اصطبل و گهی‌‌ گشت نمدپوش</p></div>
<div class="m2"><p>گاهی ز قضا شکوه وگاهی ز قدرکرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گاهی به فلان برد امان گاه به بهمان</p></div>
<div class="m2"><p>گاهی به علی تکیه و گاهی به عمر کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از فضل امیرالامراء آمد و این بار</p></div>
<div class="m2"><p>از بوسئکی چند لبم پر ز شکرکرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یک روز چو بگذشت به ره دخترکی دید</p></div>
<div class="m2"><p>مانند سگ عوعو زد و آهنگ قمرکرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گاهی ز پی هدیه ز من شعر و غزل خواست</p></div>
<div class="m2"><p>گاهی طلب جامه و آویزگهرکرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گه موی سر زلف فرستاد به معشوق</p></div>
<div class="m2"><p>وانرا ز گرفتاری خود نیک خبر کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گه نقل فرستاد وگهی جوزی بوان</p></div>
<div class="m2"><p>گه بهر عرایض طلب‌ کاغذ زر کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گه نعل فکند از پی معشوق در آتش</p></div>
<div class="m2"><p>گه زآتش عشقش‌ دل خود زیر و زبر کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گه شد به منجم ز پی ساعت تزویج</p></div>
<div class="m2"><p>گه مشت به حمدان زد و نفرین به پدر کرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گه خواست صد اندر صد و گه خواند عزیمه</p></div>
<div class="m2"><p>گه از پی تحبیب دو صد فکر دگر کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گه‌گفت خداکاش مرا چشم نمی‌داد</p></div>
<div class="m2"><p>کاو دید و دلم را هدف تیر خطر کرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گه‌گفت مرا از همه آفاق دلی بود</p></div>
<div class="m2"><p>دیدار نکویان دلم از دست بدر کرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گه‌ گفت ‌که دیوانه شوم‌ گر نشد این ‌کار</p></div>
<div class="m2"><p>وندر رخ من خیره چو دیوانه نظر کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من‌ گاه پی تسلیه‌ گفتم مکن این‌ کار</p></div>
<div class="m2"><p>هشدار کزین حادثه بایست حذر‌ کرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عشق چه و کشک چه و پشم چه فرو هل</p></div>
<div class="m2"><p>وسواس تو عرض من و خون تو هدر کرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رو جان پدر جلق زن و دلق به سر کش</p></div>
<div class="m2"><p>هر دم به بتی دست نشاید به کمر کرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خندید که این جان پدر جان پدر چند</p></div>
<div class="m2"><p>هر چیز به من کرد همین جان پدر کرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>این جان پدر از وطن افکند مرا دور</p></div>
<div class="m2"><p>این‌ جان پدر بین ‌که چه بر جان پسر کرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>قا آنیا تن زن و انصاف ده آخر</p></div>
<div class="m2"><p>با یار خود اینقدر توان بوک و مگر کرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من یار تو باشم تو به کارم نکنی میل</p></div>
<div class="m2"><p>یزدان دل سختت مگر از روی و حجر کرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>این ‌گفت و خراشید رخ از ناخ‌ و پاشید</p></div>
<div class="m2"><p>اشکی که به یک رشحه زمین را همه تر کرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفتم چکنم نیست مرا برگ عروسی</p></div>
<div class="m2"><p>خود حاضرم ار هیچ توانی خر نر کرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برتافت زنخدان مرا با سرانگشت</p></div>
<div class="m2"><p>وندر رخ من ژرف نگاهی به عبر کرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گفتا تو عروس منی ای خواجه بدین‌ حسن</p></div>
<div class="m2"><p>کز روی تو زنگی به شب تار حذر کرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خر گایم و نر گایم و آنگاه چنین زشت</p></div>
<div class="m2"><p>ویحک که ترا بار خدا این همه خر کرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گویند حکیمی تو که آباد شود فارس</p></div>
<div class="m2"><p>خرتر ز تو آن کس که تو را نام بشر‌ کرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گفتم به خدا هرچه‌‌ کنم فکر نیارم</p></div>
<div class="m2"><p>کاری‌ که توان بر طلب سیم ظفر کرد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گفتا نه چنینست به یک روز توانی</p></div>
<div class="m2"><p>یزدان نه مگر شخص ترا زاهل هنر کرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شعری دو سه در مدح امیرالامرا‌ گوی</p></div>
<div class="m2"><p>میری ‌که ترا صاحب این جاه و خط‌ر کرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گفتم‌که من این قصه نگارم به علیخان</p></div>
<div class="m2"><p>کش بار خدا پاک دل و نیک سیر کرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شعر از من و سور از تو و سیم از کرم میر</p></div>
<div class="m2"><p>نصرت ز خدایی که معانی به صور کرد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا صورت این حال دهد عرضه بر میر</p></div>
<div class="m2"><p>میری ‌که خدایش به سخا نام سمر کرد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گفتاکه نکوگفتی و تحقیق همین بود</p></div>
<div class="m2"><p>وین ‌گفتهٔ حق در دل من نیک اثر کرد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>محمود بود عاقبت میر که دایم</p></div>
<div class="m2"><p>از همت او کشته‌ آمال شمر کرد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>قاآنی ازین نوع سخن گفتن شیرین</p></div>
<div class="m2"><p>بالله‌که توان‌ کام تو پر درّ و گهر کرد</p></div></div>