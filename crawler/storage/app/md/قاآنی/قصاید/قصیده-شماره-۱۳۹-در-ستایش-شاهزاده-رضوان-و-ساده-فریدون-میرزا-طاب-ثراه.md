---
title: >-
    قصیدهٔ شمارهٔ ۱۳۹ - در ستایش شاهزادهٔ رضوان و ساده فریدون میرزا طاب ثراه
---
# قصیدهٔ شمارهٔ ۱۳۹ - در ستایش شاهزادهٔ رضوان و ساده فریدون میرزا طاب ثراه

<div class="b" id="bn1"><div class="m1"><p>امروز از دوکعبه جهان دارد افتخار</p></div>
<div class="m2"><p>کز فر آن دو کعبه بود ملک برقرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن مَضجع ملایک و این مرجع ملوک</p></div>
<div class="m2"><p>آن دافع‌کبایر و این رافع‌کبار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن‌کعبه در عرب بود این‌کعبه در عجم</p></div>
<div class="m2"><p>آن ‌کعبه نامور بود این‌کعبه نامدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حاجی شود هر آنکه بدانجا کشید رخت</p></div>
<div class="m2"><p>ناجی شود هرآنکه درینجا گشود بار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن‌کعبه‌ایست شرع بدان‌گشته محترم</p></div>
<div class="m2"><p>این‌کعبه‌ایست عدل بدوگشته استوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن‌ کعبه‌ بی‌ که شخص بدو می خورد یمین</p></div>
<div class="m2"><p>این کعبه‌یی که مرد از او می‌خورد یسار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن ‌کعبه ناف خاک و همش خاک نافه‌ خیز</p></div>
<div class="m2"><p>این ‌‌کعبه‌ کعب مجدو همش مجد کعبه دار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن کعبهٔ امانی و این کعبهٔ امان</p></div>
<div class="m2"><p>وین قبلهٔ اخایر و آن قبلهٔ خیار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن‌کعبه همچو زلف نکویان سیاه‌پوش</p></div>
<div class="m2"><p>این‌کعبه همچو اهل سعادت سپیدکار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن‌کعبه‌ایست‌کش عرفاتست درکنف</p></div>
<div class="m2"><p>این کعبه‌ای است کش غرفاتست بر کنار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن‌کعبهٔ خلیلست این‌کعبهٔ جلیل</p></div>
<div class="m2"><p>آن خاص‌ کردگارست این خاص شهریار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن‌کعبه راست سنگی آورده از بهشت</p></div>
<div class="m2"><p>این ‌کعبه راست خاکی آورده از تتار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن سنگ جای بوس امینان حق‌پرست</p></div>
<div class="m2"><p>این خاک سجده‌گاه امیران‌کامگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نتوان شکارکرد در آن‌کعبه‌ای عجب</p></div>
<div class="m2"><p>کاین کعبه روز و شب دل دانا کند شکار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن زمزمش به زمزمه در طعن سلسبیل</p></div>
<div class="m2"><p>وین زمزمش ز زمزم و تسنیم یادگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صید اندران حرام به فرمان دادگر</p></div>
<div class="m2"><p>عیش اندرین حلال به یاسای باده‌خوار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>احرام واجب آمده آن را به‌ گاه حج</p></div>
<div class="m2"><p>اجرام حاجب آمد این را به روز بار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در آن نمازکرده‌گروه از پی‌گروه</p></div>
<div class="m2"><p>در این نیاز برده قطار از پی قطار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر بام آن ز امن ‌کبوتر کند وطن</p></div>
<div class="m2"><p>در صحن این ز بیم غضفرکند فرار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یک مشعرست آن را معمور در کنف</p></div>
<div class="m2"><p>صد مشعرست این را مسرور در جوار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اندر فنای این شده الماس سنگریز</p></div>
<div class="m2"><p>وندر منای او شود ابلیس سنگسار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن کعبه‌یی که فدیه برندش ز هر طرف</p></div>
<div class="m2"><p>این‌کعبه‌ای‌که هدیه نهندش به هرکنار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قربان برند بر در آن کعبه بیش و کم</p></div>
<div class="m2"><p>قربان کنند بر در این کعبه بی‌شمار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قربان او همه حملست و همه جمل</p></div>
<div class="m2"><p>قربان این روان و دل مرد هوشیار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>واجب در آن طواف به سالی سه چار روز</p></div>
<div class="m2"><p>لازم درین سجود به روزی هزار بار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آن از خدای عالم و این از خدایگان</p></div>
<div class="m2"><p>کش بنده‌اند بارخدایان روزگار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آن مروهٔ مروت و این زمزم صفا</p></div>
<div class="m2"><p>این مشعر مشاعر و آن‌کعبهٔ فخار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بازوی عدل دست‌کرم پیکر شکوه</p></div>
<div class="m2"><p>پهلوی امن جان خرد هیکل وقار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تاج‌الملوک شاه فریدون که حزم او</p></div>
<div class="m2"><p>بر گرد او ز صخرهٔ صما کشد حصار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آنجا که تیغ او اجل و خنده قاه‌قاه</p></div>
<div class="m2"><p>وانجا که رمح او امل وگریه زار زار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>با بخت فربهش همهٔ لاغران سمین</p></div>
<div class="m2"><p>با رمح لاغرش همهٔ فربهان نزار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رایش چو نور مهر فروزان به هر زمین</p></div>
<div class="m2"><p>حزمش چو سیر باد شتابان به هر دیار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مانا ز جوهر ملک‌الموت در ازل</p></div>
<div class="m2"><p>یزدان ‌دو تیغ ‌ساخت جهان سوز و ذوالفقار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن را نهاد درکف حیدر که ها بگیر</p></div>
<div class="m2"><p>این را نهاد در بر خسرو که هین بدار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آن ‌یک‌ یهودکش شد و این‌ یک حسودکش</p></div>
<div class="m2"><p>آن طرفه ژاله‌بار شد این طرفه لاله‌زار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر شیر نر ندیده‌یی اندر قفای‌ گور</p></div>
<div class="m2"><p>شه را یکی ببین سپس خصم نابکار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ور منکری‌که بادکشد ابر درکتف</p></div>
<div class="m2"><p>شه را نظاره‌ کن ز بر خنگ راهوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تیغ برانش از بر یکران به روز رزم</p></div>
<div class="m2"><p>ماند به ماه نوکه نماید زکوهسار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در چشم اشکبار عدو عکس نیزه‌اش</p></div>
<div class="m2"><p>ماند به سرو ناز که روید ز جویبار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در پیش روی او چو عدو برکشد غریو</p></div>
<div class="m2"><p>ماند همی به رعدکه نالد به نوبهار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>قاآنیا عجب نه اگر ترزبان شوی</p></div>
<div class="m2"><p>کت آب می‌چکد همی از شعر آبدار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا جیب بوستان شود از ابر پر درم</p></div>
<div class="m2"><p>تا صحن‌ گلستان شود از باد پرنگار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از باد نعل خنگ ملک فتح را مسیر</p></div>
<div class="m2"><p>در زیر ابر رایت شه چرخ را مدار</p></div></div>