---
title: >-
    قصیدهٔ شمارهٔ ۳۰۹ - د‌ر ستایش پادشاه رضوا‌ن جایگاه مغفور محمدشاه مبرور طاب ثراه فرماید
---
# قصیدهٔ شمارهٔ ۳۰۹ - د‌ر ستایش پادشاه رضوا‌ن جایگاه مغفور محمدشاه مبرور طاب ثراه فرماید

<div class="b" id="bn1"><div class="m1"><p>دو چشم باز و دوگوشم فراز مانده به راه</p></div>
<div class="m2"><p>که‌کی بشارت فتح آید از معسکر شاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندانم از چه به راه اندرون بشیر بماند</p></div>
<div class="m2"><p>گمان برم که به شیری دوچار شد ناگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و یا ز پویه سم بارگیش کوفته شد</p></div>
<div class="m2"><p>پیاده ماند و نبودش پیاده طاقت راه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و یا ز شدت باران و برف و برد هوا</p></div>
<div class="m2"><p>به نیمه راه به جایی بماند خواه مخواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و یا چو روی منش دست و پا پر آبله شد</p></div>
<div class="m2"><p>ز بسکه بوسه زدندش زمان زمان به شفاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه شد چرا سفرش این قدر دراز کشید</p></div>
<div class="m2"><p>مگر نه عمر سفر بود غالباً کوتاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>علی‌الله از چه سبب دور ماند و دیر آمد</p></div>
<div class="m2"><p>مگر شکار بتی‌گشت شوخ و خاطرخواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرا نیامد یارب‌کجا اقامت‌کرد</p></div>
<div class="m2"><p>به حیرتم‌ که چه شد لا اله الا الله</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همین دم آمده ور نامدست می‌آید</p></div>
<div class="m2"><p>خدای را ز قدوم ویم‌ کنید آگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همی معاینه بینم ‌که مژده را بت من</p></div>
<div class="m2"><p>دوان دوان خوش و خرم درآید از درگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به جهد رانده ز تک مانده تنگ بسته‌کمر</p></div>
<div class="m2"><p>نفس‌ گسیخته خوی ‌کرده ‌کج نهاده ‌کلاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عرق نشسته به رویش چو بر سمن باران</p></div>
<div class="m2"><p>غبار مانده به چهرش چو بر ثواب‌گناه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپید گرد رهش برد و زلف غالیه‌گون</p></div>
<div class="m2"><p>بسان سودهٔ‌ کافور تر به مشک سیاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خطش به چهرهٔ رنگین چو مشک بر شنجرف</p></div>
<div class="m2"><p>تنش به جامهٔ فاخر چو نقره در دیباه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو پشت گردون در سجدهٔ خدیو جهان</p></div>
<div class="m2"><p>به پیش رویش آن زلف‌ کرده پشت دوتاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به غیر خط سیاهش برآن سپید رخان</p></div>
<div class="m2"><p>ز مشک سوده ندیدم حصار خرمن ماه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نشسته از بریکران باد پای چو برق</p></div>
<div class="m2"><p>دو اسبه تاخته ناگه دمان رسد از راه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بشارت آرد کآمد بشیر و برّه زدند</p></div>
<div class="m2"><p>به گردش از دو طرف جوق جوق بنده و داه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز بس به روی بشیر از در نیاز عیون</p></div>
<div class="m2"><p>ز بس به راه برید از در نماز جباه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تمام جبهه بود هرکجا نهند قدم</p></div>
<div class="m2"><p>تمام دیده بود هر کجا کنند نگاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لبش پر آبله‌گردیده چون سپهر به شب</p></div>
<div class="m2"><p>ز بس که بومه زدندش ز هرطرف به شفاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکیش ساغر می داده‌ کای بشیر بنوش</p></div>
<div class="m2"><p>یکیش نقد روان بر ده‌ کای برید بخواه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز هر کرانه‌ گروهی‌ گرفته دامن او</p></div>
<div class="m2"><p>که ای بشیر چه داری خبر ز فتح هراه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به روزگار زمستان‌که آبها همه سنگ</p></div>
<div class="m2"><p>چسان ز آب هری رود عبره‌ کرد سپاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به فصل دی که ز سردی بنیم راه سخن</p></div>
<div class="m2"><p>به سمع‌ کس نتواند رسیدن از افواه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز بس برودت در طبع روزگار حرون</p></div>
<div class="m2"><p>که منجمد شده قوهٔ نما به طبع‌گیاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هرات راکه سپهری است بر فراز زمین</p></div>
<div class="m2"><p>چسان ‌گرفت شهنشاه آسمان خرگاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به مان آذر وکانون ‌که شعله درکانون</p></div>
<div class="m2"><p>چنان فسرده نماید که شاخ سرخ ‌گیاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هرات را که جهانیست در میان جهان</p></div>
<div class="m2"><p>چسان ‌گشود مهین شهریار ملک پناه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به وقت بهمن‌ کز تیره جرم ابر مطیر</p></div>
<div class="m2"><p>سپهر نیلی در بر کند پرند سیاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هرات راکه بود قلعهٔ ستاره‌گرای</p></div>
<div class="m2"><p>چسان نمود مسخر شه ستاره سپاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بشیرگوید ای قوم تا نبیند کس</p></div>
<div class="m2"><p>خبر فسانه شمارد به صدهزار گواه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مگر نه خسرو گیتی‌ستان محمدشاه</p></div>
<div class="m2"><p>به سرش تاج سعادت بود ز فر آله</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شکوه شاه همین بس ‌که از مهابت او</p></div>
<div class="m2"><p>ز سومنات به عیوق رفت بانگ صلوه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نبرد شاه همین بس‌ که از صلابت او</p></div>
<div class="m2"><p>فغان افغان بررفت تا به طارم ماه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نه شاه عرضهٔ شطرنج بود شاه هری</p></div>
<div class="m2"><p>که می ز جای بجنبد ز بانگ شاهاشاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چه مایه رنج و خطر برد شاه تا آورد</p></div>
<div class="m2"><p>بر اوج تختهٔ دارش ز شیب تختهٔ‌ گاه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به مال و جاه عدو غره‌‌ گشت و غافل ازین</p></div>
<div class="m2"><p>که مال او همه مارست و جاه او همه چاه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بلی چو بخت قرین نیست مال‌ گردد مار</p></div>
<div class="m2"><p>بلی چو چرخ معین نیست جاه‌ گردد چاه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>غریو توپ دژ آشوب از محال هری</p></div>
<div class="m2"><p>گمان برم که فراتر شد از دیار فراه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نهیب شاه چنان تنگ‌کرد سینهٔ خصم</p></div>
<div class="m2"><p>که می‌نداشت ز تنگی مجال‌گفتن آه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز بس‌که بهر تماشای رزم خم شد چرخ</p></div>
<div class="m2"><p>چو چرخ چاچی شاهش‌ نماند پشت دوتاه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همی به فرق ملک خود آهنین گفتی</p></div>
<div class="m2"><p>فکنده سایه بلند آسمان به خرمن ماه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ستاره‌گریان از بیم مرگ هایاهای</p></div>
<div class="m2"><p>زمانه خندان بر کار خصم قاهاقاه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>عدو ز مرگ دل آسوده بود و غافل ازین</p></div>
<div class="m2"><p>که نوک نیزهٔ شه مرگ را بود بنگاه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مجال جنبش از هیچ سو نداشت نسیم</p></div>
<div class="m2"><p>ز بس هوا متراکم ز بانگ واویلاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز بیم شاه پر از نقش شاه بود جهان</p></div>
<div class="m2"><p>به چشم خصم ولی بود در جهان یکتاه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چنان ز بیم ملک زردگشت چهر عدو</p></div>
<div class="m2"><p>که‌کهرباش نیارست فرق‌کرد ازکاه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز گرز شاه شد آشفته مغز خصم چنانک</p></div>
<div class="m2"><p>نسیم ناخوش او مغز چرخ‌ کرد تباه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>عجبترآنکه ز مغزش به خاک تخمی‌کاشت</p></div>
<div class="m2"><p>که تا قیامت مجنون دمد به جای‌ گیاه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خدنگ شاه چنان خود دوخت بر سر خصم</p></div>
<div class="m2"><p>که‌ گفتی آنکه به فرقش شدست پوست‌ کلاه</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز بس‌که تندی شمشیر شاه جسم عدو</p></div>
<div class="m2"><p>دوپاره‌‌گشت به یک ضرب و می نبود آگاه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مصاف بس که در آن پهنه ‌‌گرم بود نداشت</p></div>
<div class="m2"><p>همی خبر پدر از پور و همره از همراه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سپاهیان ملک بر عدو چنان چیره</p></div>
<div class="m2"><p>که شرزه شیردژ آگه به حمله بر روباه</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز تیر شاه‌که ده ده به یکدگر می‌دوخت</p></div>
<div class="m2"><p>کسی نیافت‌ که پنجست خصم یا پنجاه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سپهر قلزم خوناب‌گشت و تیر ملک</p></div>
<div class="m2"><p>در او به قوت بازو همی نمود شناه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چنان نهیب ملک‌کار تنگ‌کرد به خصم</p></div>
<div class="m2"><p>که جز به سایهٔ تیغ اجل نیافت پناه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز تیغ شاه مکافات یافت خصم آری</p></div>
<div class="m2"><p>گناه را نه مگر دوزخست باد افراه</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بلی به دوزخ تفتیده می‌بسوزد مرد</p></div>
<div class="m2"><p>چو بنگریش جری بر به ارتکاب‌ گناه</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز چیره دستی شه خیره مرزبان هری</p></div>
<div class="m2"><p>چنانکه غیرامانش نه روی ماند و نه راه</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>زمان زمان پی پوزش به بارگاه ملک</p></div>
<div class="m2"><p>دوان دوان زهری صف به صف سپید و سیاه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>وزیر شه بدل اسب داد پیل دمان</p></div>
<div class="m2"><p>به هر بیاده ‌که آورد رخ به درگه شاه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>جهانستان ملکا بدسگال سوز شها</p></div>
<div class="m2"><p>تویی‌که پشت فلک در سجود تست دوتاه</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>هزار شکر خدا راکه از عنایت تو</p></div>
<div class="m2"><p>جهانیان همه انباز راحتند و رفاه</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به ویژه فارس که گویی بهشت را ماند</p></div>
<div class="m2"><p>از آنکه راه ندارد به هیچ دل اکراه</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>یکی منم‌ که به میدان مدح‌گوی سخن</p></div>
<div class="m2"><p>به صولجان بلاغت ربودم از اشباه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سوارگشته سرانگشت من به پشت قلم</p></div>
<div class="m2"><p>بدان مثابه ‌که رویینه تن بر اسب سیاه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>اگر نه خامهٔ من بود نظم عنین بود</p></div>
<div class="m2"><p>هم او بسان سقنقور بر فزودش باه</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>شها جدا ز جنابت به حیرتم‌که مرا</p></div>
<div class="m2"><p>چگونه روز شود هفته هفته ‌گردد ماه</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چنان سپاه محن بر دلم هجوم آرد</p></div>
<div class="m2"><p>که‌ گم شود تنم اندر میانه ‌گاه بگاه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ثنای شاه نیاری نمود قاآنی</p></div>
<div class="m2"><p>به هرزه باد مپیما به خیره عمر مکاه</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>به هر بهار الا تا همی به قوت طبع</p></div>
<div class="m2"><p>چو خون روان شود اندر عروق شاخ میاه</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>قوام بخت تو چندانکه در بسیط زمین</p></div>
<div class="m2"><p>کهین غلام تو بر آسمان زند خرگاه</p></div></div>