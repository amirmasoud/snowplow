---
title: >-
    قصیدهٔ شمارهٔ ۲۲۷ - در ستایش از اهل پارس و ستایش بعضی از آنها
---
# قصیدهٔ شمارهٔ ۲۲۷ - در ستایش از اهل پارس و ستایش بعضی از آنها

<div class="b" id="bn1"><div class="m1"><p>ای رخش ره‌نورد من ای اسب تیزگام</p></div>
<div class="m2"><p>تا چند بند آخوری آخر برون خرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاه خسان چه می‌خوری ای رخش ره‌نورد</p></div>
<div class="m2"><p>بار خران چه می‌بری ای اسب تیزگام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگز نبوده آب تو از منهل خسان</p></div>
<div class="m2"><p>هرگز نبوده‌ کاه تو از آخور لئام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ده ماه شد که خوی‌ گرفتی به نای و نوش</p></div>
<div class="m2"><p>وندر طویله خوردی و خفتی علی‌الدوام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر شام داده‌ کاه و جوت را به امتنان</p></div>
<div class="m2"><p>هر روز شسته یال و دمت را به احترام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بس‌که آب دادم و تیمارکردمت</p></div>
<div class="m2"><p>نه زبن زدم به پیشت و نه بر بستمت لجام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آبت ‌گهی ز چاه ‌کشیدم‌ گهی ز جوی</p></div>
<div class="m2"><p>کاهت ‌گهی به نقد گرفتم‌ گهی به وام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرگز به تازیانه بنشخودمت سرین</p></div>
<div class="m2"><p>وز چنبر چدار نیفکدمت به دام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گاهت به‌گاه دادم و آب و علف به وقت</p></div>
<div class="m2"><p>غافل نبودم از تو به یک عمر صبح و شام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک‌یک حقوق رفته اگر بازگویمت</p></div>
<div class="m2"><p>حالی فروچکد عرق شرمت از مسام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تازی نژاد اسب من آخر حمیتی</p></div>
<div class="m2"><p>یک ره چو تازیان به حمیت برآر نام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون شد حمیت عربی‌کت ز پیش بود</p></div>
<div class="m2"><p>ز اصطبل سر برآر چو شمشیر از نیام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خیز ای سیاه‌روی ترا ز رخش روستهم</p></div>
<div class="m2"><p>از سم بسای مردمک دیدهٔ خصام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اسبا حقوق من به عقوق ار بدل ‌کنی</p></div>
<div class="m2"><p>ترسم ‌که روزگار کشد از تو انتقام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اسبا زمان یاری و هنگام یاوریست</p></div>
<div class="m2"><p>لختی برون خرام و مکن رنج من حرام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از سمّ ره‌نورد بجنبان همی زمین</p></div>
<div class="m2"><p>وز نعل خاره‌کوب بسنبان همی رخام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برزن خروش تا بمرد مار در شکفت</p></div>
<div class="m2"><p>برکش صهیل تا برمد شیر درکنام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از دم به چشم شیر فلک در فکن غبار</p></div>
<div class="m2"><p>از سم به جسم ‌گاو زمین برشکن عظام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اسباگرم ز پارس رسانی به ملک ری</p></div>
<div class="m2"><p>زرین‌ کنم رکابت و سیمین‌ کنم ستام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از حلقهٔ ستاره همی سازمت رکیب</p></div>
<div class="m2"><p>وز رشتهٔ مجره همی آرمت لجام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>میخت‌کنم ستاره و نعلت‌کنم هلال</p></div>
<div class="m2"><p>زینت ز زر پخته ستامت ز سیم خام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هم پای‌بند بافمت از ریش ابلهان</p></div>
<div class="m2"><p>هم پاردم نمایمت از سبلت عوام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو زیر رانم آیی چون زیر ابرکوه</p></div>
<div class="m2"><p>من بر تو خود نشینم چون بر سمند سام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از پارس بهر کسب معالی سفرکنم</p></div>
<div class="m2"><p>راحت ‌کنم حرام‌ که حاصل شود مرام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هم چهرهٔ ستاره برندم به نوک تیر</p></div>
<div class="m2"><p>هم‌گردن زمانه ببندم به خم خام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گه چون‌ عجم به‌دست همی چین کنم‌کمند</p></div>
<div class="m2"><p>گه چون عرب به چهره همی برنهم لثام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اقبال و بخت و عز و معالی به‌گرد من</p></div>
<div class="m2"><p>از چارسو بجهد همی جوید ازدحام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حیرت ‌کند ز جنبش من در هوا عقاب</p></div>
<div class="m2"><p>غیرت برد به رحمت من در زمین هوام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قانع شوم به بیش وکمی‌کم دهد خدای</p></div>
<div class="m2"><p>راضی شوم به خیر و شری ‌کاید از انام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر دهر سخره رانم چون رند بر فقیه</p></div>
<div class="m2"><p>بر مرگ حمله آرم چون باز بر حمام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نفرین‌ کنم به پارس‌ که از ساکنان او</p></div>
<div class="m2"><p>واصل نگشت نعمت و حاصل نگشت کام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه ریش‌ کس ز مرهمشان جسته اندمال</p></div>
<div class="m2"><p>نه زخم ‌کس ز داروشان دیده التیام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همواره در شقاق و ستمشان مدار سیر</p></div>
<div class="m2"><p>پیوسته در نفاق و جفاکرده اقتحام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون‌ من ‌کسی به ‌ساحت آن‌خوار و مستمند</p></div>
<div class="m2"><p>چون من ‌کسی به عرصهٔ آن زار و مستهام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>میران آن به‌ گاه تواضع چنان ثقیل</p></div>
<div class="m2"><p>کز جا قیامشان ندهد دست تا قیام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جز باد عجبشان ندمد هیچ در دماغ</p></div>
<div class="m2"><p>جز بوی‌ کبرشان نرسد هیچ بر مشام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جز چند تنگه از گهر پاک زاده‌اند</p></div>
<div class="m2"><p>از دودهٔ مکارم و از دوحهٔ‌ کرام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون‌ لاله روز و شب همه با عیش و انبساط</p></div>
<div class="m2"><p>چون غنچه دمبدم همه با وجد و ابتسام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ژاژی ز هیچکس نشنیدم به جز مدیح</p></div>
<div class="m2"><p>لغوی ز هیچیک نشنیدم به جز سلام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بر من زحام آنان چون عام بر امیر</p></div>
<div class="m2"><p>بر من هجوم ایشان چون خاص بر امام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زان چند تن‌گذشته ملولم ز شیخ و شاب</p></div>
<div class="m2"><p>زان چند تن‌‌ گذشته خمولم ز خاص و عام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رنجی مرا کز ایشان ‌گر زانکه بشمرم</p></div>
<div class="m2"><p>آن رنج ناشمرده سخن می‌شود تمام</p></div></div>