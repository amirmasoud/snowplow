---
title: >-
    قصیدهٔ شمارهٔ ۳۴۱ - در ستایش امیر بی نظیر الله قلیخان ایلخانی قاجار فرماید
---
# قصیدهٔ شمارهٔ ۳۴۱ - در ستایش امیر بی نظیر الله قلیخان ایلخانی قاجار فرماید

<div class="b" id="bn1"><div class="m1"><p>ای روی تو فهرست شادمانی</p></div>
<div class="m2"><p>وصل تو به از فصل نوجوانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چشم تو صد جور آشکارا</p></div>
<div class="m2"><p>در زلف تو صد فتنهٔ نهانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کویت به حقیقت بهشت دنیا</p></div>
<div class="m2"><p>رویت به صفت عیش جاودانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گیسوی تو طومار دلفریبی</p></div>
<div class="m2"><p>ابروی تو طغرای دل‌ستانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر بوسه‌یی از لعل روح‌بخشت</p></div>
<div class="m2"><p>سرمایهٔ یک عمر زندگانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر فاخته قد ترا ببند</p></div>
<div class="m2"><p>نشناسدش از سرو بوستانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر شب رود از شرم طلعت تو</p></div>
<div class="m2"><p>در زیر زمین ماه آسمانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشکم جهد از مغز جای عطسه</p></div>
<div class="m2"><p>هر گه که سر زلف برفشانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در هجر تو ای دوست زنده ماندم</p></div>
<div class="m2"><p>شاید که بنالم ز سخت‌جانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواهم شبکی بی‌حضور اغیار</p></div>
<div class="m2"><p>سرمست شوی از می مغانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون روح روان دربرم نشینی</p></div>
<div class="m2"><p>وز آب دو رخ آتشم نشانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گه زلف تو بویم چنان‌که دانم</p></div>
<div class="m2"><p>گه لعل تو بوسم چنان که دانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا صبح نمایم ز بیم دزدان</p></div>
<div class="m2"><p>برگنج سرین تو پاسبانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای ترک سرین توکان نقره است</p></div>
<div class="m2"><p>زان سیم بریز تا توانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترسم‌که بر آن‌کان نقرهٔ تو</p></div>
<div class="m2"><p>خود را بزند دزد ناگهانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرچند کس ار سیم تو بدزدد</p></div>
<div class="m2"><p>زر در عوض نقره می‌ستانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بسپار به من سیم خویش اگرچه</p></div>
<div class="m2"><p>از گرک ندیدست ‌کس شبانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ترکا علم‌الله مهت نخوانم</p></div>
<div class="m2"><p>مه را نبود قد خیزرانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شوخا شهدالله‌گلت ندانم</p></div>
<div class="m2"><p>گل را نبود زلف ضیمرانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر نکته‌ که در دلبری به‌ کارست</p></div>
<div class="m2"><p>دانی همه الا که مهربانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر فن به‌عاشق کشی ضرورست</p></div>
<div class="m2"><p>داری همه الّا که خوش‌زبانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زنهار کجا می‌بری به تنها</p></div>
<div class="m2"><p>این بار سرین را بدین‌ گرانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از بسکه سرین تو گشته فربه</p></div>
<div class="m2"><p>برخاستن از جا نمی‌توانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن بارگرن را فروهل از دوش</p></div>
<div class="m2"><p>خود را به زمین چند می کشانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من بار تو بر دوش خود گذارم</p></div>
<div class="m2"><p>با این همه پیری و ناتوانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای دوست چو می‌بگذرد زمانه</p></div>
<div class="m2"><p>آن به که تو با دوست بگذرانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>راحت برسان تا رسی به راحت</p></div>
<div class="m2"><p>کان چیز که بخشی همان ستانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با عیش و طرب بگذران جهان را</p></div>
<div class="m2"><p>زان پبش که رخت از جهان جهانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون مرگ در آید ز کس نپرسد</p></div>
<div class="m2"><p>کز نسل اعالیست یا ادانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زان بادهٔ رنگین بخورکه جامش</p></div>
<div class="m2"><p>سرچشمهٔ عیشست و شادمانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وز جام به‌کام تو نارسیده</p></div>
<div class="m2"><p>حالی شودت چهره ارغوانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بینا شوی آنسان که در شب تار</p></div>
<div class="m2"><p>بی‌نقش صور بنگری معانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از وجد زمین را به جنبش آرد</p></div>
<div class="m2"><p>گرد دردی از آن بر زمین چکانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر جرم سها گر فتد شعاعش</p></div>
<div class="m2"><p>فی‌الحال سهلی شود یمانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از وجد بپرد دلت چو سیماب</p></div>
<div class="m2"><p>گر قطره‌یی از وی به لب رسانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زان باده علی‌رغم جان دشمن</p></div>
<div class="m2"><p>نوشیم به آیین دوستگانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گه ساقی مجلس دهد پیاله</p></div>
<div class="m2"><p>گه مطرب محفل زند اغانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گاهی تو پی تردماغی من</p></div>
<div class="m2"><p>بوسی دو سه بخشی به رایگانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گه من به تو از مدحت خداوند</p></div>
<div class="m2"><p>ایثار کنم ‌گنج شایگانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خورشید عجم شمع بزم قاجار</p></div>
<div class="m2"><p>الله قلیخان ایلخانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آنکو نظر حزم دوربینش</p></div>
<div class="m2"><p>در دل نگرد صورت امانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا تیغ هلالیش دیده خورشید</p></div>
<div class="m2"><p>افکنده سپر در جهان‌ستانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یکبارگی از چشم مردم افتاد</p></div>
<div class="m2"><p>با خاک رهش‌ کحل اصفهانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای رای تو مشکوهٔ عقل اول</p></div>
<div class="m2"><p>وی روی تو مصباح صبح ثانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>رایات تو آیات ملک‌گیری</p></div>
<div class="m2"><p>احکام تو اعلام‌کامرانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در صورت تو سیرت ملایک</p></div>
<div class="m2"><p>در غرهٔ تو فرهٔ کیانی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از فرّ تو عالی زمین سافل</p></div>
<div class="m2"><p>وز بخت تو باقی جهان فانی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گر روح مجسم شود تو اینی</p></div>
<div class="m2"><p>ور عقل مصور شود تو آنی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در تیره شب از رای روشن تو</p></div>
<div class="m2"><p>اسرار نهانی شود عیانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سروی‌ که نشینی به سایهٔ او</p></div>
<div class="m2"><p>بر وی نوزد باد مهرگانی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>باغی‌ که خرامی به ساحت او</p></div>
<div class="m2"><p>ایمن بود از صرصر خزانی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تیغ تو به دشتی‌ که خون فشاند</p></div>
<div class="m2"><p>تا حشر بود خاکش ارغوانی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بر چهرهٔ خصمت اجل بخندد</p></div>
<div class="m2"><p>کز هیبت توگشته ارغوانی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پیشانی رخش ترا ببوسد</p></div>
<div class="m2"><p>گر زنده شود گرد سیستانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گر وصف سمندت به‌ کوه خوانند</p></div>
<div class="m2"><p>کُه باد شود در سبک‌عنانی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ور قصهٔ عزمت به بحر رانند</p></div>
<div class="m2"><p>لنگر کند آهنگ بادبانی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خشم تو به تدبیر برنگردد</p></div>
<div class="m2"><p>زانگونه ‌که تقدیر آسمانی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اوصاف تو در وهم ما نگنجد</p></div>
<div class="m2"><p>از ما ارنی از تو لن‌ترانی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ای چرخ هنر را دل تو محور</p></div>
<div class="m2"><p>وی‌کاخ‌کرم راکف تو بانی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بی‌سعی قلم حکم نافذ تو</p></div>
<div class="m2"><p>در نامه شود ثبت از روانی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>آیات قضا نارسیده بینی</p></div>
<div class="m2"><p>احکام قدر نانوشته خوانی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>اندام معانی برهنه بیند</p></div>
<div class="m2"><p>ادراک تو در کسوت مبانی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مفتاح فتوحست رایت تو</p></div>
<div class="m2"><p>همچون علم نطع کاویانی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>هستی به طفیل تو یافت مایه</p></div>
<div class="m2"><p>زانسان‌ که طفیلی به میهمانی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ای‌کرده به بام رواق جاهت</p></div>
<div class="m2"><p>نه پایهٔ افلاک نردبانی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>از فرط ارادت به حضرت تو</p></div>
<div class="m2"><p>این شعر فرستادم ارمغانی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هر نقطه ی او خال چهر جانست</p></div>
<div class="m2"><p>گر نکته‌ گیرد عدوی جانی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>من نای معانی چنین نوازم</p></div>
<div class="m2"><p>گو خصم تو بهتر زن ار توانی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ختمست در اقلیم دانش امروز</p></div>
<div class="m2"><p>بر من لقب صاحب‌القرانی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>خوارم ز جهان ‌گرچه خواری من</p></div>
<div class="m2"><p>بر عزت من بس بود نشانی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>خواری کشد از گاز و پتک و کوره</p></div>
<div class="m2"><p>زآنرو که عزیزست زر کانی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>طوطی به قفس‌کی شدی‌گرفتار</p></div>
<div class="m2"><p>گر شهره نبودی به خوش زبانی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>از دام بلا ایزدت رهاند</p></div>
<div class="m2"><p>از دام بلاگر مرا رهانی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تا ملک بقا جاودان بماند</p></div>
<div class="m2"><p>در ملک بقا جاودان بمانی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>هر کاو نرود راست با تو چون تیر</p></div>
<div class="m2"><p>پشتش‌ کند از بار غم‌ کمانی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>مفعول مفاعیل فاعلاتن</p></div>
<div class="m2"><p>تقطیع چنین‌ کن ز نکته ‌دانی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>تا مطرب مجلس به رقص خواند</p></div>
<div class="m2"><p>تن‌تن تنناتن تنن تنانی</p></div></div>