---
title: >-
    قصیدهٔ شمارهٔ ۲۵۶ - در مدح شاهزاهٔ آزاد‌ه هلاکوخان بن شجاع السلطنه
---
# قصیدهٔ شمارهٔ ۲۵۶ - در مدح شاهزاهٔ آزاد‌ه هلاکوخان بن شجاع السلطنه

<div class="b" id="bn1"><div class="m1"><p>مخسب ای صنم امشب بخواه بادهٔ روشن</p></div>
<div class="m2"><p>بیار شمع به مجلس بریز نقل به دامن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بکش ترانهٔ دلکش بنه سپند بر آتش</p></div>
<div class="m2"><p>بسوز عود به مجمر بسای مشک به هاون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مخور چمانه‌چمانه ‌سبوسبو خور و خم‌خم</p></div>
<div class="m2"><p>مده پیاله پیاله قدح قدح ده و من من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی ز روزنهٔ حجره در سراچه نظرکن</p></div>
<div class="m2"><p>ببین چگونه برقصند بام و خانه و برزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چگونه مست و خرابند گلرخان سمن‌سا</p></div>
<div class="m2"><p>چگونه گرم سماعند شاهدان پری‌ون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دن ‌ارچه داشت ‌دلی‌ پر ز خون ز توبهٔ مستان</p></div>
<div class="m2"><p>به خنده خنده برون‌کرد جام می ز دل دن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه چهره روح مجسم چه چهره چهرهٔ ساقی</p></div>
<div class="m2"><p>نه ناله عیش مصور چه ناله نالهٔ ارغن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی‌ گرفته به ‌بر دلبری چو دلبر یغما</p></div>
<div class="m2"><p>یکی‌ کشیده بکش شاهدی چو شاهد ار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمین زمین چمن از فروش اطلس و دیبا</p></div>
<div class="m2"><p>هوا هوای بهشت از بخور عنبر و لادن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی ز بهر تماشا نظرگشوده چو نرگس</p></div>
<div class="m2"><p>یکی‌ ز بهر خوشآمد زبان‌‌گشاده‌ چو سوسن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جو‌ن و پبر و زن و مرد و روستایی و شهری</p></div>
<div class="m2"><p>پذیره را همه از روی شوق برزده دامن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو نیز ای بت چین ای به چهره آذر برزین</p></div>
<div class="m2"><p>پی پذیره بیا تا که زین زنیم به توسن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که بامداد ز خاور چو آفتاب برآید</p></div>
<div class="m2"><p>برآید از طرف خاور آفتابی روشن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ابوالشجاع هلاکوی بن حسن شه غازی</p></div>
<div class="m2"><p>که خاک معرکه از تیغ اوست منبت روین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو او به عرصه به درعی نهان هزار نریمان</p></div>
<div class="m2"><p>چو او به پهنه به رخشی عیان هزار تهمن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو بزم خواهد روحی مصوّرست در ایوان</p></div>
<div class="m2"><p>چو رزم جوید مرگی مجسمست به جوشن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شراب نوشد اما ز خون عرق مخالف</p></div>
<div class="m2"><p>پیاله ‌گیرد اما ز کاسهٔ سر دشمن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز حلقه حلقهٔ جوشن عیان به عمرصه تن او</p></div>
<div class="m2"><p>چنان‌که نور درخشنده آفتاب ز روزن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به ‌وقعه فوجش موجی چه‌ موج موج بلاجو</p></div>
<div class="m2"><p>به ‌کینه خیلش سیلی چه سیل سیل بناکن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کمند و جوشن‌گردان ز امن عهدش دایم</p></div>
<div class="m2"><p>یکی به ‌کاسهٔ شیر و یکی به ‌کیسهٔ ارزن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به روز رزمگه آهن‌دلان آهن‌خفتان</p></div>
<div class="m2"><p>بسان آتش سوزان نهان شوند در آهن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به جای سبزه بروید ز خاک ناوک آرش</p></div>
<div class="m2"><p>به جای قطره ببارد ز ابر نیزهٔ قارن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شود جنون مجسم خمرد ز وسوسه در سر</p></div>
<div class="m2"><p>شود هلاک مصور روان ز ولوله در تن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کمان و تیر چو یاران نورسیده ز هرسو</p></div>
<div class="m2"><p>پی معانقه با هم شوند دست به ‌گردن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه میل‌ها که‌ کشد آسمان به چشم سلامت</p></div>
<div class="m2"><p>ز نیزه‌ها که نشیند فرو به چشمهٔ جوشن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو او به نیزه زند دست روح قارن و مویه</p></div>
<div class="m2"><p>چو او به تیر بردشست جان آرش و شیون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جهان ز سهم جهانسوز تیغ شعله‌فشانش</p></div>
<div class="m2"><p>به چشم خصم شود تنگتر ز چشمهٔ سوزن</p></div></div>