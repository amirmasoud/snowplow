---
title: >-
    قصیدهٔ شمارهٔ ۳۰۰ - وله ایضاً
---
# قصیدهٔ شمارهٔ ۳۰۰ - وله ایضاً

<div class="b" id="bn1"><div class="m1"><p>دوش ‌که شاه اختران والی چرخ چارمین</p></div>
<div class="m2"><p>کرد ز اوج آسمان میل به مرکز زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ز پس ادای فرض اندر خانهٔ خدا</p></div>
<div class="m2"><p>بر نهجی ‌که واردست از در شرع و ره دین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کردم زی سرای خود میل و زدم قدم برون</p></div>
<div class="m2"><p>گشنه چمان به کوی و درگه به یسار و گه یمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم به پای و پا به ره نرم گرا و کند رو</p></div>
<div class="m2"><p>دل ز خیال گه بگه تفته و درهم و غمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه هوای فال و فرگه به خیال سم و زر</p></div>
<div class="m2"><p>گاه اندیشهٔ خطر گاهی فکرت دفین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفس به فکر عزّ و شان تن به هوای آب و نان</p></div>
<div class="m2"><p>دل به وصال دلستان لب به خیال ساتکین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمزمه هردمم به لب از پی جام پر ز می</p></div>
<div class="m2"><p>وسوسه بی‌حدم بدل از غم یار نازنین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کآیا آن فرشته‌خو در چه مکانش گفتگو</p></div>
<div class="m2"><p>ایدر با که همنفس ایدون با که همنشین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من دل در برم‌ کنون زین غم‌ ‌گشته بحر خون</p></div>
<div class="m2"><p>تا که ببوسدش غبب یا که بمالدش سرین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یابد چون پس ازخورش ساده ز باده‌ پرورش</p></div>
<div class="m2"><p>تاکه برد بدو یورش یا که ‌کند براو کمین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرکشی‌ او چو سر کند میل به‌ شور و شر کند</p></div>
<div class="m2"><p>از پی رام‌ کردنش یاد کند دو صد یمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مانا با چه دوزخی رام شد آن بهشت‌رو</p></div>
<div class="m2"><p>کز لب‌ کوثر آیتش نوش نماید انگبین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حالی ازدو چهر او و آندو کمند خم‌به‌خم</p></div>
<div class="m2"><p>چیند شاخ ضیمران بوید برگ یاسمین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پاس دگر چو بگذرد بستر خواب‌ گسترد</p></div>
<div class="m2"><p>تا به فراش خوابگه تن دهد آن بلای دین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس ز در ملاعبت آید وگیردش ببر</p></div>
<div class="m2"><p>سخت فشاردش بدن‌گرم ببوسدش جبین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این همه سهل بشمرم گ‌رنه به تخت عاج او</p></div>
<div class="m2"><p>دیو هوس نمایدش از اثر شبق مکین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زیرا چون به تخت جم دست بیابد اهرمن</p></div>
<div class="m2"><p>بی‌شک برسپوزد انگشت به حلقهٔ نگین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یابد چون به تخت سیم آری ناکسی ظفر</p></div>
<div class="m2"><p>دست ستم ‌کند دراز ار همه خود بود تکین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آنگاه از غضب مرا هرسر مو شود به تن</p></div>
<div class="m2"><p>همچو سنان‌‌گستهم راست به زیر پوستین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غیرت عصمتم بدان دارد تا کشم به خون</p></div>
<div class="m2"><p>لاشهٔ خود ز تیر غم پیکر او به تیغ‌ کین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باری‌بس‌خیال‌ها بگذشت اندرم به دل</p></div>
<div class="m2"><p>تا بگذشت ساعتی ز اول شب به هان و هین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>طیره هنوز من در آن اول شب‌ که ناگهم</p></div>
<div class="m2"><p>گشت ز خم‌کوچه‌یی طالع صبح دومین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در شب تیره‌ای عجب بنمود آفتاب‌رو</p></div>
<div class="m2"><p>گرچه بر آفتاب نی ‌کژدم هیچگه قرین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ماند چو من دو چشم من خیره ز فرط روشنی</p></div>
<div class="m2"><p>کاین شب نی‌ کلیم چون‌بیضاش اندرآستین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون سوی او پس از وله نیکو بنگریستم</p></div>
<div class="m2"><p>دیدم یار می‌رسد با دو رخان آتشین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چشمش یک تتار فن چهرش یک‌بهارگل</p></div>
<div class="m2"><p>جعدش یک جهان شکن زلفش‌ یک سپهر چین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قدش یک چمن نهال امّا بر سرش ارم</p></div>
<div class="m2"><p>لعلش یک یمن عقیق امّا با شکر عجین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نازک چون خیال من نقش میانش در کمر</p></div>
<div class="m2"><p>زیر کمرش کوه سان شکل سرین ز بس سمین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آیت حسن و دلبری از خم طره‌اش عیان</p></div>
<div class="m2"><p>راست چو نقش نصرت از رایت پور آتبین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بس که مهیب و جان شکر چشمش درگه نگه</p></div>
<div class="m2"><p>گفتی در دو چشم او شیر ژیان بود مکین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هرچه شکنج و پیچ و خم بو‌د به زلف او نهان</p></div>
<div class="m2"><p>هرچه فریب و رنگ و فن بود به چشم او ضمین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چشمم بر جمال او روشن گشت و گفتمش</p></div>
<div class="m2"><p>لعل تو چیست‌گفت هی شادی یک‌جهان حزین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گفتمش ای بدیع رخ اهلا مرحبا بیا</p></div>
<div class="m2"><p>کت به روان ز جان من باد هزار آفرین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زان سپسش ز رهگذر بردم تا وثاق در</p></div>
<div class="m2"><p>تنگ ‌کشیدمش به بر راست چو خازن امین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زان پس ای بسا فسون خواندم تا که رام شد</p></div>
<div class="m2"><p>همچو تکاوری حرون ‌کآوریش به زیر زین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هرچه غلط‌ گمان مرا رفت به جای دیگران</p></div>
<div class="m2"><p>بعد کنار و بوس شد آن همه با ویم یقین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وایدون خیره مانده‌ام تا چه‌ دهم جواب اگر</p></div>
<div class="m2"><p>شرحی زین حکایتم پرسد خسرو گزین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آنکه بر آستان او بوسه همی دهد ینال</p></div>
<div class="m2"><p>آنکه به خاک راه او سجده همی برد تکین</p></div></div>