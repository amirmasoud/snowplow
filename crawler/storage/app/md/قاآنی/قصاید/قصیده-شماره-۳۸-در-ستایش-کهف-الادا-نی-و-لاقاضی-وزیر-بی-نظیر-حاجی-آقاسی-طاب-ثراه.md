---
title: >-
    قصیدهٔ شمارهٔ ۳۸ - ‌در ستایش کهف الادا‌نی و‌لاقاضی وزیر بی‌نظیر حاجی آقاسی طاب ثراه
---
# قصیدهٔ شمارهٔ ۳۸ - ‌در ستایش کهف الادا‌نی و‌لاقاضی وزیر بی‌نظیر حاجی آقاسی طاب ثراه

<div class="b" id="bn1"><div class="m1"><p>بر دلم صدهزار نیشترست</p></div>
<div class="m2"><p>بلکه از صدهزار بیشترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرح یک ماجرا ز دردسرم</p></div>
<div class="m2"><p>موجب صدهزار درد سرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیکرم آنچنان شدست ضعیف</p></div>
<div class="m2"><p>که نهان همچو روح از نظرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین سبب درکفم ز غایت ضعف</p></div>
<div class="m2"><p>خشک چوبی به ‌گاه پویه درست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لاجرم ‌گاه پویه پندارند</p></div>
<div class="m2"><p>که عصایی به سحر ره سپرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر هلال این‌چنین ضعیف شود</p></div>
<div class="m2"><p>عاطل از سیر و جنبش و اثرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوه اگر بیند اینچنین آسیب</p></div>
<div class="m2"><p>لرزه‌اش تا به حشر در کمرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش اشک دو چشم خونبارم</p></div>
<div class="m2"><p>قلزم اندر شمارهٔ شمرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قامتم خم شدست همچو کمان</p></div>
<div class="m2"><p>لیک در پیش تیر غم سپرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تن افسرده‌ام ز غایت ضعف</p></div>
<div class="m2"><p>چون یکی چوب خشک بی‌ثمرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>موی از تاب تب بر اندامم</p></div>
<div class="m2"><p>بتر از نیش ناچخ و تبرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در و بام سرایم از شیشه</p></div>
<div class="m2"><p>راست‌گویی دکان شیشه‌گرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه لبریز از آن قبیل عرق</p></div>
<div class="m2"><p>کش به چارم مزاج سرد و ترست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آه از آن شیشه‌ای‌ که چون‌ کژدم</p></div>
<div class="m2"><p>هیأتش دل شکاف زهره درست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لاطئی هست ‌کاب شهوت آن</p></div>
<div class="m2"><p>رافع رنج و دافع خطرست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دوستانم زنند دست به دست</p></div>
<div class="m2"><p>که فلان ای دریغ محتضرست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنچنان لاغرم که پنداری</p></div>
<div class="m2"><p>پوستم زیر و استخوان زبرست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لاجرم هرکه مر مرا بیند</p></div>
<div class="m2"><p>فاش‌ گوید که این چه جانورست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حجرهٔ من زمین یونانست</p></div>
<div class="m2"><p>بس‌که در وی حکیم چاره‌گرست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دهنم از حرارت صفرا</p></div>
<div class="m2"><p>از عفونت چوکام شیر نرست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لرز لرزان تنم ز شدت ضعف</p></div>
<div class="m2"><p>چون دل خصم صدر نامورست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حاجی آقاسی آن جهان جلال</p></div>
<div class="m2"><p>که جهانش به چشم مختصرست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آنکه رایش مدبر فلکست</p></div>
<div class="m2"><p>وآنکه قدرش مربی قدرست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آنکه از مهر و کین او زاید</p></div>
<div class="m2"><p>هرچه اندر زمانه خیر و شرست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جنبش خامه‌اش چو گردش چرخ</p></div>
<div class="m2"><p>پایمرد صدور نفع و ضرست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لیک سیرش خلاف سیر سپهر</p></div>
<div class="m2"><p>دوست را نفع‌ و خصم را ضررست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>طبع او بحر و گفت او گوهر</p></div>
<div class="m2"><p>دست او ابر و جود او مطرست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آنچه ز آثار خلق نیک در اوست</p></div>
<div class="m2"><p>از گمان و قیاس و وهم برست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ملکی هست در لباس بشر</p></div>
<div class="m2"><p>کاین خلایق نه لایق بشرست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر از خود بُدی فروغ قمر</p></div>
<div class="m2"><p>گفتمی‌کاو برای و رو قمر‌ست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>روی او نیست آفتاب سپهر</p></div>
<div class="m2"><p>لیک چون آفتاب مشتهرست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خامهٔ او چو خام خسرو عهد</p></div>
<div class="m2"><p>مادر فتح و دایهٔ ظفرست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>با عتابش‌ که هست مایهٔ مرگ</p></div>
<div class="m2"><p>خون و جان جهانیان هدرست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دل و دستش به‌گاه جود وکرم</p></div>
<div class="m2"><p>غارت‌گنج و آفت‌گهرست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون غزالی رمیده از صیاد</p></div>
<div class="m2"><p>حزم او پیش بین و پس نگرست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>لطف او روح‌بخش و روح‌افزا</p></div>
<div class="m2"><p>قهر او جان‌ستان و جان شکرست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ای بهشت جهانیان ‌که جحیم</p></div>
<div class="m2"><p>زاتش سطوت تو یک شررست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر سخن ‌کز لبت برون آید</p></div>
<div class="m2"><p>خوشتر از آب چشمهٔ خضرست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جامهٔ شوکت و جلالت را</p></div>
<div class="m2"><p>دیبهٔ نه سپهر آسترست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نوش درکام دشمنت نیش است</p></div>
<div class="m2"><p>زهر درکام دوستت شیرست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>صاحبا بندهٔ تو قاآنی</p></div>
<div class="m2"><p>که خداوند دانش و هنرست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گله‌ها دارد از تغافل تو</p></div>
<div class="m2"><p>لیک دلش از زبانش بی‌خبرست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هیچ‌ گفتی‌ کهینه چاکر من</p></div>
<div class="m2"><p>مدتی شدکه غایب از نظرست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هیچ‌گفتی‌که درکدام محل</p></div>
<div class="m2"><p>به ‌کدامین سراچه‌اش مقرست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جد پاک تو مصطفی ‌که بقدر</p></div>
<div class="m2"><p>ذاتش از هرچه جز خدای برست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به سرای فلان یهود شتافت</p></div>
<div class="m2"><p>دید چون خسته‌حال و خون جگرست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زادگان را مگر نه درگیتی</p></div>
<div class="m2"><p>شیوهٔ جد و عادت پدرست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دوش‌گفتم‌که پاکشم چندی</p></div>
<div class="m2"><p>ز آستانت‌که از سپهر برست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بازگفتم‌که بنده در همه حال</p></div>
<div class="m2"><p>از تولای خواجه ناگزرست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سایه جز پیروی‌گزیرش نیست</p></div>
<div class="m2"><p>هرکجا کافتاب درگذرست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زبر و زیر زیر فرمانت</p></div>
<div class="m2"><p>تا زمین زیر و آسمان زبرست</p></div></div>