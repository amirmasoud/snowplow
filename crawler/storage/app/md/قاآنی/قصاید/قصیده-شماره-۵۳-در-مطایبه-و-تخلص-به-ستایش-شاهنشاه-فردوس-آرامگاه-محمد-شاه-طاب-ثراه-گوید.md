---
title: >-
    قصیدهٔ شمارهٔ ۵۳ - در مطایبه و تخلص به ستایش شاهنشاه فردوس آرامگاه محمد شاه طاب ثراه گوید
---
# قصیدهٔ شمارهٔ ۵۳ - در مطایبه و تخلص به ستایش شاهنشاه فردوس آرامگاه محمد شاه طاب ثراه گوید

<div class="b" id="bn1"><div class="m1"><p>هر زمانم‌ که به آن ترک سر و کار افتد</p></div>
<div class="m2"><p>صلح خیزد ز میان‌ کار به پیکار افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من به عمد از پی صلح همی جویم جنگ</p></div>
<div class="m2"><p>کز پی صلحم با بوسه سر وکار افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفسم بر دو یک افتد ز سبکروحی شوق</p></div>
<div class="m2"><p>عدد بوسهٔ من چون به سه و چار افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر میانش چو کمر آورم از شوق دو دست</p></div>
<div class="m2"><p>نقطه را ماند کاندر خط پرگار افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای خوش آن وقت که خیزد بت من از پی رقص</p></div>
<div class="m2"><p>از طرب رعشه برآن‌گنبد دوار افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساعد و ساق چو بالا زند آن ترک پسر</p></div>
<div class="m2"><p>دختر طبع مراکیک به شلوار افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوشتر آن‌ وقت‌ که از غایت مستیش سخن</p></div>
<div class="m2"><p>همچو سرما زده درکام به تکرار افتد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه بنشیند و از جای به یک‌پا خیزد</p></div>
<div class="m2"><p>گاه برخیزد و از پای به یک‌بار افتد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفتاب خردش روی نماید به غروب</p></div>
<div class="m2"><p>بس‌که چون سایه هم بر در ودیوار افتد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مژه‌اش از طرف چشم فتد بر رخسار</p></div>
<div class="m2"><p>راست مانند عصا کز کف بیمار افتد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مست در بستر من خسبد و رندان دانند</p></div>
<div class="m2"><p>حالت مست‌ِ که در بستر هشیار افتد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا به صبح آنقدرش بوسه زنم بر رخسار</p></div>
<div class="m2"><p>که چو منش آبله از بوسه به رخسار افتد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صبح اگر حالت شب عرضه نماید بر شاه</p></div>
<div class="m2"><p>کارم از بیم به سوگند و به انکار افتد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور به خاک قدم شاهم سوگند دهد</p></div>
<div class="m2"><p>ناگزیرم‌که مراکار به اقرار افتد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم به خاک قدم شه‌که قسم می‌نخورم</p></div>
<div class="m2"><p>گرنه اول به‌کفم خاتم زنهار افتد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاه زنهار اگرم بدهد اقرارکنم</p></div>
<div class="m2"><p>ورنه حاشا زنم و مسأله دشوار افتد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نی خطاگفتم شاه از همه‌حال آگاهست</p></div>
<div class="m2"><p>می نخواهد که همی پرده ز اسرار افتد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هم‌ خدا داند و هم‌ شاه‌ که هر شب در شهر</p></div>
<div class="m2"><p>زین نمط رندی و قلاشی بسیار افتد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون بر ابنای جهان بار خدا ستارست</p></div>
<div class="m2"><p>لاجرم سایهٔ او باید ستار افتد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>می‌خوران را شه اگر خواهد بر دار زند</p></div>
<div class="m2"><p>گذر عارف و عامی همه بر دار افتد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ور به دژخیم‌ کند حکم کشان گوش به رند</p></div>
<div class="m2"><p>همه گوشست که در کوچه و بازار افتد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این همه طیبت محضست‌که در دولت شاه</p></div>
<div class="m2"><p>گر همه کافر حربیست نکوکار افتد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شعرا را بود این قاعده از عهد قدیم</p></div>
<div class="m2"><p>که حدیث از می و معشوق در اشعار افتد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون خور این نظم دلاویز جهانگیر شود</p></div>
<div class="m2"><p>گر به خاک در شه درخور ایثار افتد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شاه آزاد جوانبخت محمد شه راد</p></div>
<div class="m2"><p>که جهان با سخن خلقش فرخار افتد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آنکه گر نام عطایش ببری بر لب بحر</p></div>
<div class="m2"><p>ریزهٔ سنگ به قعرش ذر شهوار افتد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خنجر برّان در پنجهٔ او روز عزا</p></div>
<div class="m2"><p>همچو برقیست‌ که در قلزم زخار افتد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رزمگاهی‌ که درو یک ره شمشیر زند</p></div>
<div class="m2"><p>تا به جاوید ز خون خاکش در آغار افتد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دور بین حزمش بر موم چو تایید دهد</p></div>
<div class="m2"><p>موم چون بیضهٔ پولادین ستوار افتد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خار ناچیز چو گلبن همه‌ گل‌ آرد برگ</p></div>
<div class="m2"><p>نظر مهرش اگر روزی بر خار افتد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پرچم رایتش اینسان‌ که بود شقه ‌گشای</p></div>
<div class="m2"><p>زود باشد که درش سایه به بلغار افتد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا بر اقطار زمین دور فلک سلطانست</p></div>
<div class="m2"><p>این چنین‌ کمتر سلطان جهاندار افتد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا ز اسلام وز کفرست نشان خنجر شاه</p></div>
<div class="m2"><p>از پی قوت دین قاطع ‌گفتار افتد</p></div></div>