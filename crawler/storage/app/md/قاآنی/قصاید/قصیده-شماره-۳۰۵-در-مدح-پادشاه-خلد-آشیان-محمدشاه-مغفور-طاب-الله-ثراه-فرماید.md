---
title: >-
    قصیدهٔ شمارهٔ ۳۰۵ - در مدح پادشاه خلد آشیان محمدشاه مغفور طاب الله ثراه فرماید
---
# قصیدهٔ شمارهٔ ۳۰۵ - در مدح پادشاه خلد آشیان محمدشاه مغفور طاب الله ثراه فرماید

<div class="b" id="bn1"><div class="m1"><p>الحمدکه آمد ز سفر موکب خسرو</p></div>
<div class="m2"><p>وز موکب او کوکب دین یافته پرتو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هر لب پژمرده به یمن قدم شاه</p></div>
<div class="m2"><p>در هر دل افسرده به فر رخ خسرو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برخاست به جای دم ناخوش نفس خوش</p></div>
<div class="m2"><p>بنشست به جای غم دیرین طرب نو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینک چو سحابست هوا حاملهٔ رعد</p></div>
<div class="m2"><p>از نالهٔ زنبوره و آوای شواشو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنجاب به دوش فلگ از گرد عساکر</p></div>
<div class="m2"><p>سیماب به‌گوش ملک از بانگ روا رو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمد ملکی ‌کز فزع ‌گرد سپاهش</p></div>
<div class="m2"><p>در چشمهٔ‌خورشعد سراسیمه‌شود ضو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دارای جوانبخت محمد شه غازی</p></div>
<div class="m2"><p>شاهی که سمندش چو خیالست سبکرو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در چنبر چوگانش فلک همچو یکی ‌گوی</p></div>
<div class="m2"><p>در ساحت میدانش زمین همچو یکی‌گو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون بر سر او رنگ نهد پای چو جمشید</p></div>
<div class="m2"><p>چون از بر شبرنگ‌ کند جای چو خسرو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گنجی‌شود از جودش هر سایل‌و مسکین</p></div>
<div class="m2"><p>مرغی شود از تیرش هر ترکش و پهلو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای خستهٔ صمصام تو هر پیل‌تنی یل</p></div>
<div class="m2"><p>ای بستهٔ فتراک تو هر تیغ زنی‌گو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رخش تو بنی عمّ براقست ازیراک</p></div>
<div class="m2"><p>میدان همی از چرخ‌کندگاه تک و دو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون رفرف اگر بر زبر عرش نهدگام</p></div>
<div class="m2"><p>مهماز زند قدر تو بازش‌ که همی دو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آتش زده خشم تو به معمورهٔ عالم</p></div>
<div class="m2"><p>زانگونه‌که ناپلیون در خطهٔ مسکو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با بخت عدو بخت تو گوید به تمسخر</p></div>
<div class="m2"><p>بیدارم و می‌دارم من پاس تو به غنو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گلزر سماحت شده در عهد تو بیخار</p></div>
<div class="m2"><p>فالیز عدالت شده از جهد تو بی‌خو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو مهر جهانبانی از آن سایل جودت</p></div>
<div class="m2"><p>دامانش چو کان آمده از جود تو محشو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وقتی شرر دوزخ می‌کرد صدایی</p></div>
<div class="m2"><p>قهر تو بدوگفت یکی‌گوی و دو بشنو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جاه وخطر آنجاست‌که بخت تو برد رخت</p></div>
<div class="m2"><p>فتح و ظفر آنجاست‌ که‌ کوی تو کند غو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خالی شود ار ساحت دنیا ز تر و خشک</p></div>
<div class="m2"><p>حالی بلآلی ‌کندش جود تو مملو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ازکینه و پرخاش عدو نیست ترا باک</p></div>
<div class="m2"><p>مه را چه هراس از سگ و آن حملهٔ عوعو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هم پیل بنهراسد اگر پشه ‌کند بانگ</p></div>
<div class="m2"><p>هم شیر نیندیشد اگر گربه‌ کند مو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خودروی بود خصم تو در مزرع هستی</p></div>
<div class="m2"><p>ای شاه بدان خنجر چون داسش بدرو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نه بذل ترا واهمهٔ نفی لن ولا</p></div>
<div class="m2"><p>نه جود ترا وسوسهٔ شرط ان ولو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرگندم ذات تو در آن خوشه نبستی</p></div>
<div class="m2"><p>کس حاصل هستی نخریدی به یکی جو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در قالب بی‌روح عدو دهر دمد دم</p></div>
<div class="m2"><p>چون نافه‌که از جهل‌گرید به سوی بو</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اجرام بر رای تو چون ذره بر مهر</p></div>
<div class="m2"><p>افلاک بر قدر تو چون قطره بر زو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در سایه ی قدر تو اگر ماه و اگر مهر</p></div>
<div class="m2"><p>در پایهٔ صدر تو اگر زاب و اگر زو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا نفس بنالد چو خطا گردد امید</p></div>
<div class="m2"><p>تا طبع ببالد چو روا گردد مدعوّ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نالنده عدویت ز خطا دیدن مسؤول</p></div>
<div class="m2"><p>بالنده حبیبت ز روا گشتن مرجو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا دیبه ز روم آید و سنجاب ز بلغار</p></div>
<div class="m2"><p>تا نافه ز چین خیزد و کافور ز جوجو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عز و شرف از ماهیت قدر تو خیزد</p></div>
<div class="m2"><p>ز انسان‌که ز گل بوی و ز می رنگ و ز مه تو</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بی‌غرهٔ اقبال تو شامی نشود صبح</p></div>
<div class="m2"><p>بی‌طرهٔ اعلام تو صبحی نشود شو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا بخت تو برنا بود و تخت تو برپا</p></div>
<div class="m2"><p>ای شاه به داد و دهش و نیکی بگرو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از امر قدر درکنف حفظ خداپوی</p></div>
<div class="m2"><p>با حکم قضا معتکف ‌کاخ رضا شو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>قاآنی صد شکرکه رستیم ز اندوه</p></div>
<div class="m2"><p>والحمدکه آمد ز سفر موکب خسرو</p></div></div>