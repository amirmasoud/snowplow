---
title: >-
    قصیدهٔ شمارهٔ ۲۶۸ - و من افکار طبعه فی‌المدیحه
---
# قصیدهٔ شمارهٔ ۲۶۸ - و من افکار طبعه فی‌المدیحه

<div class="b" id="bn1"><div class="m1"><p>تاج دولت رکن دین غیث زمین غوث زمان</p></div>
<div class="m2"><p>شاه عادل خسرو باذل شهنشاه جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرگ را در مشت‌گیرد اینک این تیغش دلیل</p></div>
<div class="m2"><p>مار در انگشت دارد وینک آن رمحش نشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خشم او یارد ز هم بگسستن اعضای سپهر</p></div>
<div class="m2"><p>حزم او تاند بهم پیوستن اجزای زمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نماید یاد تیغش آتشین‌ گردد خیال</p></div>
<div class="m2"><p>چون سراید وصف گرزش آهنین گردد زبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسکه اسرار نهان از نور رایش روشنست</p></div>
<div class="m2"><p>آرزو از دل پدیدارست و معنی از بیان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ملک ملک اوست تا هر جا که تابد آفتاب</p></div>
<div class="m2"><p>دور دور اوست تا هرجا که‌ گردد آسمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناخدا تا داستان عزم و حزم او شنید</p></div>
<div class="m2"><p>گفت زین پس مرمرا این لنگرست آن بادبان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حقه‌باز و ساحرم خوانند مردم زانکه من</p></div>
<div class="m2"><p>در مدیح شه ‌کنم هردم گفتیها عیان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یاد تیغ اوکنم دوزخ فشانم از ضمیر</p></div>
<div class="m2"><p>نام خشم او برم آتش برآرم از زبان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رعد غرّد گر بگویم ‌کوس او هست اینچنین</p></div>
<div class="m2"><p>کوه برّد گر بگویم رخش او هست آنچنان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نام خُلقِ او برم خیزد ز خاک شوره ‌گل</p></div>
<div class="m2"><p>وصف جود او کنم ‌بخشم به‌سنگ خاره جان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نام حزمش بر زبان آرم فلک ماند ز سیر</p></div>
<div class="m2"><p>ذکر عزمن در مبان آرم زمین‌گردد روان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شر‌ح رزم او دهم ‌گردد جوان از غصه پیر</p></div>
<div class="m2"><p>یاد بزم اوکنم پیر از طرب‌گردد جوان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای سنین عمر تو چون دور اختر بیشمار</p></div>
<div class="m2"><p>وی رسول ‌عدل ‌تو چون ‌صنع داور بیکران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسکه در عهد تو شایع ‌گشته رسم راستی</p></div>
<div class="m2"><p>شاید ار مرد کمانگر ساخت نتواند کمان</p></div></div>