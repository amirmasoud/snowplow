---
title: >-
    قصیدهٔ شمارهٔ ۲۹۲ - د‌ر مدحت عمدهٔ الخوانین العظام شمس الدین خان افغان می‌فرماید
---
# قصیدهٔ شمارهٔ ۲۹۲ - د‌ر مدحت عمدهٔ الخوانین العظام شمس الدین خان افغان می‌فرماید

<div class="b" id="bn1"><div class="m1"><p>آفتاب زمانه شمس‌الدین</p></div>
<div class="m2"><p>ای قدر قدر آسمان تمکین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهر بارای روشن توسها</p></div>
<div class="m2"><p>چرخ با اوج درگه تو زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوه با عزم تو چو کاه سبک</p></div>
<div class="m2"><p>کاه با حزم تو چوکوه متین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیغ تو عزم‌ فتنه‌ را نشتر</p></div>
<div class="m2"><p>خشم تو چشم خصم را زوبین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نامی از جود تست ابر بهار</p></div>
<div class="m2"><p>گامی از کاخ تست چرخ برین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاتمی هست حکم محکم تو</p></div>
<div class="m2"><p>کش بود آفتاب زیر نگین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرورا حسب حال من بشنو</p></div>
<div class="m2"><p>گرچه مستغنی است از تبیین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ز شیراز آمدم به عراق</p></div>
<div class="m2"><p>مرمرا بود هشت اسب‌گزین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر یکی‌گاه حمله چون صرصر</p></div>
<div class="m2"><p>هریکی روز وقعه چون تنین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وندر اینجا به قحطی افتادند</p></div>
<div class="m2"><p>که مبیناد چشم عبرت بین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همگی همچو مرغ جلاله</p></div>
<div class="m2"><p>گشته قانع به خوردن سرگین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون من از بهر جو دعاکردم</p></div>
<div class="m2"><p>همه‌ گفتند ربنا آمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر من و بخت من همی‌ کردند</p></div>
<div class="m2"><p>صبح تا شام هریکی نفرین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه مرا زهره‌ای‌ که‌ گویم هان</p></div>
<div class="m2"><p>نه مرا جرأتی‌ که ‌گویم هین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قصه کوتاه هفته‌یی نگذشت</p></div>
<div class="m2"><p>که گذشتند با هزار انین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وینک از بهر هریکی خوانم</p></div>
<div class="m2"><p>هر شب جمعه سوره یاسین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بنده را حال اسبکی باید</p></div>
<div class="m2"><p>نرم‌ دُم‌ گِرد سُم‌ گوزن سرین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تیزبین آنچنانک در شب تار</p></div>
<div class="m2"><p>بیند از ری حصار قسطنطین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون باستد به پهنه کوه گران</p></div>
<div class="m2"><p>چون بپوید به وقعه باد بزین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رعد کردار چونکه شیهه کشد</p></div>
<div class="m2"><p>می‌ نخسبد به بیشه شیر عرین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون سلیمان که هشت تخت بباد</p></div>
<div class="m2"><p>از بر پشت او گذارم زین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چند پنهان کنم بگویم راست</p></div>
<div class="m2"><p>چون مرا راستی بود آیین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرمرا شوخ و شنگ شاهدکی است</p></div>
<div class="m2"><p>سیم خد سرو قد فرشته جبین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مژه‌اش همچو چنگل شهباز</p></div>
<div class="m2"><p>طره‌اش همچو پنجه ی شاهین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زلفکانش ورق ورق سنبل</p></div>
<div class="m2"><p>چهرگانش طبق طبق نسرین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قامتش همچو طبع من موزون</p></div>
<div class="m2"><p>طره‌اش همچو چهر من پرچین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ابرویش همچو تیغ تو بران</p></div>
<div class="m2"><p>گیسویش همچو خلق تو مشکین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وجناتش چو طبع تو خرم</p></div>
<div class="m2"><p>حرکاتش چو شعر من شیرین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چبا بد دور چشمکی دارد</p></div>
<div class="m2"><p>که درو ناز گشته ‌گوشه نشین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ساق او را اگر نظاره‌ کند</p></div>
<div class="m2"><p>پای تا سر شبق شود عنین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تاری از زلفش ار به باد رود</p></div>
<div class="m2"><p>کوه و صحرا شود عبیرآگین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چشمش از فتنه یک جهان لشکر</p></div>
<div class="m2"><p>رویش ‌از جلوه یک فلک پروین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>روز تا شب سرین‌ گردش‌ را</p></div>
<div class="m2"><p>به نگاه نهان کنم تخمین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در دل از بهر عارض و لب او</p></div>
<div class="m2"><p>بوس ها می‌کنم همی تعیین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>او پیاده است و زین سبب نهلد</p></div>
<div class="m2"><p>که سوارش شوم من مسکین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر دو را می توان سوار نمود</p></div>
<div class="m2"><p>به یکی اسب ای فرشته قر‌ین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>‌آسیاوار تا نماید سیر</p></div>
<div class="m2"><p>آسمان در ارضی تسعین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آنی از دور مدت تو شهور</p></div>
<div class="m2"><p>روزی از سال دولت توسنین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آفرین بر روان قاآنی</p></div>
<div class="m2"><p>کش روش راستست ورای رزین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در دل و‌رای این ‌چنین دارد</p></div>
<div class="m2"><p>یاد و مهر جناب شمس‌‌الدین</p></div></div>