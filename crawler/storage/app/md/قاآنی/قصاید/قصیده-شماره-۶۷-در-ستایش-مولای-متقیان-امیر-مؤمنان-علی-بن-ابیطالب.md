---
title: >-
    قصیدهٔ شمارهٔ ۶۷ - در ستایش مولای متقیان امیر مؤمنان علی بن ابیطالب
---
# قصیدهٔ شمارهٔ ۶۷ - در ستایش مولای متقیان امیر مؤمنان علی بن ابیطالب

<div class="b" id="bn1"><div class="m1"><p>بجز لب تو کزو گفت شکرین خیزد</p></div>
<div class="m2"><p>که دیده لعل کزو جوی انگبین خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عجب ز سادگی سرو بوستان دارم</p></div>
<div class="m2"><p>که پیش قامت موزونت از زمین خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قد تو سرو بود طرّهٔ تو مشک اگر</p></div>
<div class="m2"><p>ز سرو ماه بروید ز مشک چین خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کند به دوزخ اگر جای چو تو غلمانی</p></div>
<div class="m2"><p>بهشتی از سر سودای حور عین خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز هر زمین ‌که فتد عکس عارض تو برو</p></div>
<div class="m2"><p>قسم به جان تو یک عمر یاسمین خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه خدای‌پرستان سفر کنند به چین</p></div>
<div class="m2"><p>چو ترک ‌کافر من ‌گر بتی ز چین خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>«هزار بیشه هژبرم چنان نترساند</p></div>
<div class="m2"><p>که آن غزال غزلخوانم از کمین خیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>و‌لی به آهوی چشمت قسم ‌که نگریزم</p></div>
<div class="m2"><p>هزار لجه نهنگم ‌گر ازکمین خیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدا به حالت ابلیس ‌‌کاو نمی‌دانست</p></div>
<div class="m2"><p>که گوهری چو تو از کان ماء و طین خیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر آستان تو ترسم فرشته رشک برد</p></div>
<div class="m2"><p>به ناله‌یی که مرا از دل حزین خیزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شرح‌ گوهر اشکم دهد به جای حروف</p></div>
<div class="m2"><p>ز نوک خامه همی گوهر ثمین خیزد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به قد همچو کمانم مبین ‌که هردم ازو</p></div>
<div class="m2"><p>چو تیر ناز صد آه دلنشین خیزد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه قرنها گذرد تا قران زهره و ماه</p></div>
<div class="m2"><p>اثر کند که قران تو بی‌قرین خیزد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز رشک نازکی و نوبهار طلعت تو</p></div>
<div class="m2"><p>طراوت و طرب از طبع فرودین خیزد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مدام از نی‌ کلکم‌ که رشک نیشکرست</p></div>
<div class="m2"><p>به وصف لعل تو گفتار شکرین‌ خیزد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدان رسیده‌ که بر طبع خویش رشک برم</p></div>
<div class="m2"><p>کزان سفینه چسان گوهری چنین خیزد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سزد که سجده برم پیش طبع‌ قاآنی</p></div>
<div class="m2"><p>کزو نهفته همی مدح شاه دین خیزد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>علی‌که‌ گر کندش‌ مدح طفل ابجدخوان</p></div>
<div class="m2"><p>ز آسمان و زمین بانگ آفرین خیزد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شهی که خاتم قدرت کند چو در انگشت</p></div>
<div class="m2"><p>هزار ملک سلیمانش از نگین خیزد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر بر ادهم ‌گردون ‌کند به خشم نگاه</p></div>
<div class="m2"><p>نشان داغ مه و مهرش از سرین خیزد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به روی زین جو نشیند‌ گمان بری‌ که مگر</p></div>
<div class="m2"><p>هزار بیشه غضنفر ز پشت زین خیزد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شبیه پیکر یکران اوست‌ کوه ‌گران</p></div>
<div class="m2"><p>زکوه اگر روش صرصر بزین خیزد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شها دوبینی ذات و رسول خدای</p></div>
<div class="m2"><p>نه از دو دیده که از دیدهٔ دوبین خیزد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به روز عرض سخا صد هزار گنج‌ گهر</p></div>
<div class="m2"><p>ز آستین تو ای شاه راستین خیزد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به جای موج ز رشک کف تو بحر محیط</p></div>
<div class="m2"><p>زمان زمان عرق شرمش از جبین خیزد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به روز رزم تو هر خون که خورده در زهدان</p></div>
<div class="m2"><p>ز بیم خشم تو از چشم هر جنین خیزد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به نزد شورش رزم تو شور و غوغایی</p></div>
<div class="m2"><p>کز آسمان و زمین روز واپسین خیزد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هزار بار به نسبت از آن بود کمتر</p></div>
<div class="m2"><p>که روز معرکه از پشه‌یی طنین خیزد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برای آنکه ترا روز و شب سلام‌کنند</p></div>
<div class="m2"><p>ز جن و انس و ملایک صفیر سین خیزد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مخالفان ترا هر زمان به جای نفس</p></div>
<div class="m2"><p>ز سینه ناله برآید ز دل انین خیزد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز من که غرق گناهم ثنای حضرت تو</p></div>
<div class="m2"><p>چنان غریب‌ که ‌گوهر ز پارگین خیزد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو آن شهی‌ که ‌گدایان آستان ترا</p></div>
<div class="m2"><p>هزار دامن ‌گوهر ز آستین خیزد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گدای راه‌نشینم ولی به همت تو</p></div>
<div class="m2"><p>یسار گنج‌ گهربارم از یمین خیزد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شها ثناگر خود را ممان به درگه خلق</p></div>
<div class="m2"><p>که شرمسار کند جان و شرمگین خیزد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنان به یک نظر لطف بی‌نیازش‌کن</p></div>
<div class="m2"><p>که از سر دو جهان از سر یقین خیزد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هزار سال بقا باد دوستان ترا</p></div>
<div class="m2"><p>به شرط آنکه ز هر آنش صد سنین خیزد</p></div></div>