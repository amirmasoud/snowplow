---
title: >-
    قصیدهٔ شمارهٔ ۲۲۱ - در ستایش ولیعهد رضوان‌مهد، عباس شاه غازی طاب الله ثراه و وزیر بوزرجمهر تدبیر قایم‌مقام
---
# قصیدهٔ شمارهٔ ۲۲۱ - در ستایش ولیعهد رضوان‌مهد، عباس شاه غازی طاب الله ثراه و وزیر بوزرجمهر تدبیر قایم‌مقام

<div class="b" id="bn1"><div class="m1"><p>از تقویت رای دو سالار معظم</p></div>
<div class="m2"><p>امروز همه روی زمینست منظم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن آصف آصف‌حسب و صدر جم‌آیین</p></div>
<div class="m2"><p>این مهدی مهدی‌نسب و میر خضر دم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن آصف و بر خواری عفریت مهیا</p></div>
<div class="m2"><p>این مهدی و برگشتن دجال مصمّم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن ضارب سیف آمد و این صاحب خانه</p></div>
<div class="m2"><p>آن فتح مصور شد و این جود مجسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در صارم آن خواری صد سلسله مضمر</p></div>
<div class="m2"><p>در خامهٔ این یاری صد طایفه مدغم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خامهٔ این تا نگری نیست به جز نوش</p></div>
<div class="m2"><p>در صارم آن تا گذری نیست به جز سمّ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خامهٔ این‌گاو زمین عجل سخنگوی</p></div>
<div class="m2"><p>از صارم آن شیر فلک‌ کلب معلّم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از صارم آن طعنه زند سام به دستان</p></div>
<div class="m2"><p>از خامهٔ این لعن‌کند معن به حاتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با خامهٔ این یافته بود نافهٔ آهو</p></div>
<div class="m2"><p>با صارم آن رنجه بود پنجهٔ ضیغم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای بر سرگنج ‌کفتان جان سخن‌سنج</p></div>
<div class="m2"><p>آسوده چو عطشان به لب چشمهٔ زمزم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طبعم به یکی قرصه جو خواست قناعت</p></div>
<div class="m2"><p>تا بو که چو خامان به ارادت نزند خم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جوع البقر لولی ‌کرمان نپسندید</p></div>
<div class="m2"><p>کان بحر عطا کوزه‌صفت بازدهد نم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در غم مگذارید کسی را که بیانش</p></div>
<div class="m2"><p>صد ره طرب‌انگیزترست از می درغم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در هم مپسندید تنی راکه وجودش</p></div>
<div class="m2"><p>در دور ملک خوارترست از در و درهم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مهری چو مرا در کف عفریت ممانید</p></div>
<div class="m2"><p>ای مرتبهٔ آصفتان از قبل جم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زی‌گاه ولیعهد مرا راه نمایید</p></div>
<div class="m2"><p>ای رهبرتان فضل شهنشاه معظّم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عمان بود آن دولت پاینده و من مور</p></div>
<div class="m2"><p>گو غوطه زند مورکه عمّان نشودکم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر کس ز عطا‌تان به غناییست مگر کان</p></div>
<div class="m2"><p>هرکس ز یمشان به یساریست مگر یم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من کان نیم آخر که نخواهیدم خشنود</p></div>
<div class="m2"><p>من یم نیم آخرکه نسازیدم خرم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یزدان به نبی ‌گفته‌ که در عسر بود یسر</p></div>
<div class="m2"><p>وین نکته بر نفس سلیمست مسلم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زی یُسر مرا راه نمایید ازین عسر</p></div>
<div class="m2"><p>تا یسر مؤخر ببرد عسر مقدم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>الحمد خدا را که به دوران ولی‌عهد</p></div>
<div class="m2"><p>جز بر تن اعدا نبود کسوت ماتم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>روزی نه که از طنطنهٔ ‌کوس بشارت</p></div>
<div class="m2"><p>آوازهٔ فتحش نرود در همه عالم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روزی نه‌ که تیرش نکند روز یلان تار</p></div>
<div class="m2"><p>روزی نه ‌که خامش نکند پشت ‌گوان خم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دی بودکه سالار خبوشان به خبوشان</p></div>
<div class="m2"><p>می‌کرد همی فخر چون عفریت به خاتم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>امروز یکی پشتهٔ خاکست حصارش</p></div>
<div class="m2"><p>از ناوک و فتراک پر از افعی و ارقم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دی بود که از کنگرهٔ حصن حصینش</p></div>
<div class="m2"><p>می‌دید سراشیب برین بر شده طارم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>امروز به دوزخ شده زان باره نگونسار</p></div>
<div class="m2"><p>مانندهٔ پیری‌که درافتاد ز سُلُّم‌</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دی بودکه از باره خروش دف اوباش</p></div>
<div class="m2"><p>زی زهره و مه بودگه از زیر وگه از بم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>امروز چو دف از تف خمیازهٔ توپش</p></div>
<div class="m2"><p>در جوش و خروشندکه طوبی لجهنم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>امروز چو خوکی شده با خنگ ملک رام</p></div>
<div class="m2"><p>آن دیو که دی داشت غزالانه همی رم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>امروز خبوشان شده بنگاه خموشان</p></div>
<div class="m2"><p>وینک به جز از دام دروکس نزند دم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از توپ دز آشوب کنون هرکف خاکش</p></div>
<div class="m2"><p>گردیده پریشان و به ملکی شده منضم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آری به روش فی‌المثل از مصر به بغداد</p></div>
<div class="m2"><p>هر خانه‌که آید به ره سیل دمادم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر قطره‌که سیل ازکتف‌کوه براند</p></div>
<div class="m2"><p>از چار کران در به دیاری کندش ضم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آن باره‌کش ازکنگره یک خشت نکندی</p></div>
<div class="m2"><p>گر روی زمین پر شدی از بهمن و رستم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از چار طرف توپ دژ آهنج ز خاکش</p></div>
<div class="m2"><p>در چار محل چار که آورده فراهم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یک‌کوه به خوارزم و دگرکوه به‌کرمان</p></div>
<div class="m2"><p>یک‌کوه به‌کشمیر و دگرکوه به دیلم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برکنگرهٔ حصن هزار اسب و هری زد</p></div>
<div class="m2"><p>هر خشت‌که برکند از آن بارهٔ معظم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>امروز به خوارزم و هری مشت غباریست</p></div>
<div class="m2"><p>هر خشت ‌که دی بود بر آن باروی مبرم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شاهان عجم رزم بدینگونه نکردند</p></div>
<div class="m2"><p>ها دفتر شهنامه و ها نامهٔ معجم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>فرداست ‌که از رایت او ساخت نخشب</p></div>
<div class="m2"><p>پر ماه مقنّع شود از مهچهٔ پرچم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فرداست‌که بر مه رود از خاک سراندیب</p></div>
<div class="m2"><p>سور و شغب از دخمه ‌گرشاسب و نیرم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فرداست‌ که غوغای تفضّل لبنینی</p></div>
<div class="m2"><p>وارحم لبنانی به فلک برکشد آدم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نالد ز سر سوزکه یا بضعتی اغفر</p></div>
<div class="m2"><p>موید ز در عجز که یا مهجتی ارحم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>فرداست‌ که یا قاهر ارحم لعبادی</p></div>
<div class="m2"><p>جبریل پیام آردش از خالق اعظم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>فرداست که شاهان به ولیعهد سرایند</p></div>
<div class="m2"><p>کای نیروی بازوی شهنشاه مکرّم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>قد فضلک الله علینا فتفضل</p></div>
<div class="m2"><p>قد سلطک الله علینا فترحم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>فرداست که زی ساحت ری رای نهد روی</p></div>
<div class="m2"><p>با جبهتی از داغ شهنشاه موسّم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شار آید و مار آید و خان آید و خاقان</p></div>
<div class="m2"><p>با ماره و با یاره و با شاره و ملحم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فرداست‌ که آواز من وکوس بشارت</p></div>
<div class="m2"><p>هر دم رود از خاک برین برشده طارم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>او نعره برآرد ز پی فتح پیاپی</p></div>
<div class="m2"><p>من چامه سرایم ز پی نصر دمادم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تا هست جهان‌شاه جهان‌شاه جهان باد</p></div>
<div class="m2"><p>نی شاه رعیت‌ که شهنشاه شهان هم</p></div></div>