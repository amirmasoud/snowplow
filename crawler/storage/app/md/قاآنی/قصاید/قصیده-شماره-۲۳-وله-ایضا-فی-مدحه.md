---
title: >-
    قصیدهٔ شمارهٔ ۲۳ - وله ایضاً فی مدحه
---
# قصیدهٔ شمارهٔ ۲۳ - وله ایضاً فی مدحه

<div class="b" id="bn1"><div class="m1"><p>بدا به حالت آن مجرمی‌که روز حساب</p></div>
<div class="m2"><p>به قدر یک شب هجر تواش‌کنند عذاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشا به حالت آن زاهدی‌که در محشر</p></div>
<div class="m2"><p>به قدر یک دم وصل تواش دهند ثواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمند زلف خم اندر خمت ز هر تاری</p></div>
<div class="m2"><p>به‌گردن دلم افکند صدهزار طناب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حرارت تب شوقم شد از لب تو فزون</p></div>
<div class="m2"><p>اگرچه‌گرمی تب برطرف‌کند عناب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زیر ابروی پیوسته چشم رهزن تو</p></div>
<div class="m2"><p>چوکافریست‌که‌سرمست خفته در محراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دهان تنگ تو آن نقطه‌یی بود موهوم</p></div>
<div class="m2"><p>که می نگنجد وصفش به صد هزارکتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبی ز لعل لبش بوسه‌یی طلب‌کردم</p></div>
<div class="m2"><p>اشاره‌کرد به ابروکه در طلب بشتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو رفتم از دو لبش ذوق بوسه دریابم</p></div>
<div class="m2"><p>رضا به بوسه ندادند آن دو لعل خوشاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنانکه‌هرلب لعلش به‌عذر رنجش خویش</p></div>
<div class="m2"><p>ز بهر بوسه به لعل دگر نمود خطاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خطابشان چو به اندازهٔ عتاب رسید</p></div>
<div class="m2"><p>فتاد لاجرم اندر میانشان شکرآب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مکش به‌گوش من ای پارسا ز خلد سخن</p></div>
<div class="m2"><p>که خلد را نخرم من به نیم جرعه شراب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به سوی خلدکشیدی دلم اگر بودی</p></div>
<div class="m2"><p>دروکباب و می و ساقی و سماع و رباب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز ضرب ناخن من از چه برکشد آهنگ</p></div>
<div class="m2"><p>اگر نه سینه ربابست و ناخنم مضراب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فراهم آمده در من ز جور هفت سپهر</p></div>
<div class="m2"><p>جدا ز طرهٔ آشفتهٔ تو چار اسباب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز وصل باد به دستم ز هجر خاک به سر</p></div>
<div class="m2"><p>ز ناله سینه بر آتش زگریه دیده پر آب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به بزم هردو ز شرم محبتیم خموش</p></div>
<div class="m2"><p>کجاست باده‌که بردارد از میانه حجاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به‌مستی ار عرق‌افشانی از جبین چه عجب</p></div>
<div class="m2"><p>خمار دردسری هست و به شود زگلاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دهان تنگ تو را نیست‌گنج آنکه‌کند</p></div>
<div class="m2"><p>بیان اجر شهیدان خود بروز حساب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به پارهای‌کباب دلم نمک پاشند</p></div>
<div class="m2"><p>دو جرعه نوش لبت وقت خوردن می ناب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بلی عجب نبود زانکه رسم مستانست</p></div>
<div class="m2"><p>که از برای‌گزک شور می‌کنندکباب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرت هواست‌که جان آفرین ببخشاید</p></div>
<div class="m2"><p>بر آن‌گروه‌که هستند مستحق عذاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به روز حشر بدان حالتی‌که می‌دانی</p></div>
<div class="m2"><p>برافکن از رخ عالم فریب خویش نقاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز نشتر مژه ایما نماکه تا بزنند</p></div>
<div class="m2"><p>به یک‌کرشمه رگ خواب مالکان عقاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به عهد عدل ملک این قدر همی دانم</p></div>
<div class="m2"><p>که ملک دل نسزد از تطاول تو خراب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ابوالشجاع بهادر شه آنکه از سخطش</p></div>
<div class="m2"><p>به خواب می نرود شیر شرزه اندر غاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تهمتنی‌که ز یک جلوهٔ بلارک او</p></div>
<div class="m2"><p>فتد به خاک هلاکت هزار چون سهراب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تکی ز خنگ وی وگرد و دوله در دهلی</p></div>
<div class="m2"><p>غوی ز سنج وی و شور و ناله در سنجاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر آستانش اگر سنجرست اگر سلجوق</p></div>
<div class="m2"><p>به بارگاهش اگر بهمنست اگر داراب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که نشمردشان‌گردون ز جرگهٔ خدام</p></div>
<div class="m2"><p>نیاوردشان‌گیتی به حلقهٔ حجاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به‌کام اژدر اگر رأفتش دمی بدمد</p></div>
<div class="m2"><p>عموم خلق خورند از لغت او جلاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شها تویی‌که پس ازکار ساز بنده نواز</p></div>
<div class="m2"><p>کف‌کریم تو آمد مسبب الاسباب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تویی‌که هست به همدستی‌کلید ظفر</p></div>
<div class="m2"><p>پرند قلعه‌گشایت مفتح الابواب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر عدوی تو را پرورش دهدگردون</p></div>
<div class="m2"><p>همان حکایت‌میش است و صرفه‌جو قصاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سنان خطیت آن‌گرزه مار عقرب نیش</p></div>
<div class="m2"><p>پرنگ هندیت آن اژدهای افعی ناب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یکی بدرد ناف سمک به‌گاه طعان</p></div>
<div class="m2"><p>یکی ببرد فرق فلک به وقت ضراب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو آن به چنگل خشم تو، ویله در لاهور</p></div>
<div class="m2"><p>چو این به پنجهٔ قهر تو، مویه در پنجاب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عجب نباشد اگر صید شاهبازکند</p></div>
<div class="m2"><p>به پشت‌گرمی شاهین همت تو ذباب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز خون دیدهٔ خصم تو می‌شدی لبریز</p></div>
<div class="m2"><p>اگر نه دروا می‌بودی این‌کهن دولاب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ستارگان همه شب تا به صبح بیدارند</p></div>
<div class="m2"><p>ز بیم آنکه نبیند سطوت تو به خواب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز ملک دفع نماید خدنگت اعدا را</p></div>
<div class="m2"><p>چنانکه رجم شیطان‌کند ز چرخ شهاب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عیان ز ماهچهٔ اخترت مطالع فتح</p></div>
<div class="m2"><p>چو ارتفاع نجوم از خطوط اسطرلاب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگر ز تیغ تو برقی‌گذرکند به محیط</p></div>
<div class="m2"><p>محیط در خوی خجلت‌رود ز شم تراب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به حجله‌گاه وغا خنجر تو دامادیست</p></div>
<div class="m2"><p>که‌کرده است ز خون دست‌و پای خویش خضاب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ولیک تا ندهد روگشا ز خون عدو</p></div>
<div class="m2"><p>عروس فتح ز رخ بر نیفکند جلباب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو نام عزم تو شنود همی سپهر و دزنگ</p></div>
<div class="m2"><p>چو سوی حزم تو بیند همی زمین و شتاب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زمانه را نبود خز به خدمت تو رجوع</p></div>
<div class="m2"><p>سپهر را نسزد جز به حضرت تو ایاب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اگرچه شکل حبابست چرخ لیکن نیست</p></div>
<div class="m2"><p>به نزد لجهٔ جود تو در شمار حباب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به سیم و زر چوکند سکه نام نیک تو را</p></div>
<div class="m2"><p>ز فر نام تو صاحبقران شود ضراب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به چرخ خواست‌کند دود مطبخ تو صعود</p></div>
<div class="m2"><p>خرد به سهو سرودش به ره قرین سحاب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چنان به‌گرد خود از ننگ این سخن پیچعد</p></div>
<div class="m2"><p>که نارسیده به‌گردون شد از خجالت آ‌ب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شبی ز روی تفاخر هلال‌گفت به چرخ</p></div>
<div class="m2"><p>که باد پای ملک را منم خجسته رکاب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>جواب دادش‌کای هرزه‌گرد هرجایی</p></div>
<div class="m2"><p>که از لقای تو دیوانه می‌شود بیتاب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هزار همچو تو یک لحظه نقش می‌بندد</p></div>
<div class="m2"><p>ز نیم جنبش خنگ ملک به لوح تراب</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به روز رزمگه از خون پردلان‌گردد</p></div>
<div class="m2"><p>فضای معرکه آزرم بحر بی‌پایاب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>زمین شود متلاطم ز موج خون یلان</p></div>
<div class="m2"><p>بدان مثابه‌که افتد سفینه درگرداب</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>درون لجهٔ خون دست و پا زندگردون</p></div>
<div class="m2"><p>ز بیم غرقه شدن چون غریق در غرقاب</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>زمین بتابد از تاب تیغ چون‌کوره</p></div>
<div class="m2"><p>فلک بجنبد از بادگرز چون سیماب</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز اشک چشم عدو لجه‌یی شود هامون</p></div>
<div class="m2"><p>که ساق عرش‌کند تر ز جیش خیزاب</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>زمانه‌جفت‌کند موزه پیش پای اجل</p></div>
<div class="m2"><p>پرند جانشکرت چون برون شود ز قراب</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نهنگ سبز تو بر خویشتن سیه شمرد</p></div>
<div class="m2"><p>که سرخ‌گردد از خون سرخه و سرخاب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خدنگ‌دال پرت چون ز چرخ دال مثال</p></div>
<div class="m2"><p>به‌صید نسر فلک‌بال‌و پر زند چو عقاب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>شوند بی‌پر ازان لاجرم ز لانهٔ چرخ</p></div>
<div class="m2"><p>دو نسر طایر و واقع ز بیم جان پرتاب</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>پرنگ هندی رومی تنت همی‌گیرد</p></div>
<div class="m2"><p>مزاج زنگی از قتل خصم چون سقلاب</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>شود ز تربیت آفتاب شمشیرت</p></div>
<div class="m2"><p>فضای عرصهٔ پیکارکان لعل مذاب</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شها ز بزم حضور تو تا شدم غایب</p></div>
<div class="m2"><p>رسد به‌گوشم من صار غایباً قدخاب</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>جدا ز خاک درت هرزمان خورم افسوس</p></div>
<div class="m2"><p>به طرز پیر دل افسرده ز آرزوی شباب</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>کفی شهیداً بالله‌که من به هستی خویش</p></div>
<div class="m2"><p>نه لایقم به خطاب و نه در خورم به عتاب</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بلی‌گزیر جز این نی‌که طفل بگریزد</p></div>
<div class="m2"><p>ز باب جانب مام و زمام در بر باب</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گرم بسوزی و خاکسترم به باد دهی</p></div>
<div class="m2"><p>به هیچ‌جا نکنم جز به درگه تو مآب</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سزدکه فخرکنم بر امام خاقانی</p></div>
<div class="m2"><p>به یمن تربیتت ای خدیو عرش جناب</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به چند باب مرا برتری مسلم ازو</p></div>
<div class="m2"><p>به شرط آنکه ز انصاف دم زنند احباب</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>نخست آنکه نیای من آن مهندس راد</p></div>
<div class="m2"><p>که پیر عقل بدش طفل مکتب آداب</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>هزار مرتبه هست از نیای او افضل</p></div>
<div class="m2"><p>که بود نادان جولاهکی قرین دواب</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نیای من همه بحثش به صدر صفهٔ علم</p></div>
<div class="m2"><p>ز شش‌جهات وچهار اسطقس وهفت‌حجاب</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>نیای او همه‌گفتش به شیب دکهٔ جهل</p></div>
<div class="m2"><p>ز آبگیره و ماشو و میخ‌کوب و طناب</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>دویم‌گزیده پدرم آن مهین سخنور عصر</p></div>
<div class="m2"><p>که فکر بکرش مستغنی است از القاب</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سخن چه‌رانم درباب باب خویش‌که‌بود</p></div>
<div class="m2"><p>کمال بابش و از باب او بر از همه باب</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>از آنکه بودی‌گفت پدرم پیوسته</p></div>
<div class="m2"><p>ز ابرو مخزن و دریاو لؤلؤ خوشاب</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به عکس بابک نجار اوکه بد سخنش</p></div>
<div class="m2"><p>ز رند و مثقب و معل وکمان و دولاب</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>سیم‌که مامک عیسی‌پرست او بودی</p></div>
<div class="m2"><p>ز بی عفافی طباخ مطبخ احزاب</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>عفیفه مام من آن زن‌که پشت پایش را</p></div>
<div class="m2"><p>ندیده‌طلعت‌خورشید و تابش مهتاب</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>گذشتم از نسب اکنون‌کنم بیان حسب</p></div>
<div class="m2"><p>برای آنکه نکو نی پژوهش انساب</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نخست اینکه ازوکم نیم به فضل ارچه</p></div>
<div class="m2"><p>هزار مرتبه زو برترم ز فکر مصاب</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چو سوی نظم مجرد نظرکنی بینی</p></div>
<div class="m2"><p>که نظم من زر پاکست و نظم او قلاب</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>به‌ویژه آنکه‌گر او مدح اخستان کردی</p></div>
<div class="m2"><p>که بود چون شه شطرنج خالی از اسباب</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>من از ثنای شهی دم زنم‌که هست او را</p></div>
<div class="m2"><p>هزار بنده چو شاه اخستان‌کهین بواب</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ور او مسلسل از قهر اخستان بودی</p></div>
<div class="m2"><p>به‌حبس وکنده وزنجیر و بند وقید وعذاب</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>من از عنایت خاورخدای تن ندهم</p></div>
<div class="m2"><p>که‌اوج عرش برینم شود حضیض جناب</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>زبان زگفتهٔ بیجا ببند قاآنی</p></div>
<div class="m2"><p>که خود ستایی دور است از طریق ثواب</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>الا به دور جهان تا مدام طعنه رسد</p></div>
<div class="m2"><p>به فکر خاطی جهان از اولوالالباب</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>شمار عمر ملک آنقدرکه نتوانند</p></div>
<div class="m2"><p>محاسبین جهان ضبط او به‌هیچ حساب</p></div></div>