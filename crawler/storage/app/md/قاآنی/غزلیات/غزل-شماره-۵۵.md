---
title: >-
    غزل شمارهٔ ۵۵
---
# غزل شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>آن سنگدل که شیشهٔ جانهاست جای او</p></div>
<div class="m2"><p>آتش زند در آب و گل ما هوای او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوگند خورده‌ام که ببوسم هزار بار</p></div>
<div class="m2"><p>هرجا رسیده است به یکبار پای او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز کاندر آب و آیینه دیدم جمال وی</p></div>
<div class="m2"><p>بر هیچ کس نظر نگشودم به جای او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق که آرزو نکند جز رضای دوست</p></div>
<div class="m2"><p>این عجز او بتر بود ازکبریای او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر مدعی نبود ز خود خواهشی نداشت</p></div>
<div class="m2"><p>او را چه کار تا طلبد مدعای او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر زیرکی بهل که همین عین آرزوست</p></div>
<div class="m2"><p>کز دوست آرزو بکند جز رضای او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاآنی ار ز پای فتادست عیب نیست</p></div>
<div class="m2"><p>نیکو قویست دست توانا خدای او</p></div></div>