---
title: >-
    غزل شمارهٔ ۲۵
---
# غزل شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>رفتند دوستان و کم از بیش و کم نماند</p></div>
<div class="m2"><p>روزم سیاه گشت و برم سایه هم نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون صبح از آن سبب نفس سرد می کشم</p></div>
<div class="m2"><p>کان صبح چهره چون نفس صبحدم نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با من ستم نمی‌کند ار یار من رواست</p></div>
<div class="m2"><p>چندان ستم نمودکه دیگر ستم نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویی دلت چرا نشد از هجر من غمین</p></div>
<div class="m2"><p>آن قدر تنگ شدکه درو جای غم نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ابر در فراق تو از بس گریستم</p></div>
<div class="m2"><p>در چشم من چو چشمهٔ خورشید نم نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می ده که وقت آمدن و رفتن از جهان</p></div>
<div class="m2"><p>کس محتشم نیامد وکس محتشم نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای خواجه عمر جام سفالین دراز باد</p></div>
<div class="m2"><p>کاو بهر باده هست اگر جام جم نماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قاآنیا دل تو حرم خانهٔ خداست</p></div>
<div class="m2"><p>منت خدای راکه بتی در حرم نماند</p></div></div>