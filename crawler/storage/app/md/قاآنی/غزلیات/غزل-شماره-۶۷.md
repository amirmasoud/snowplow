---
title: >-
    غزل شمارهٔ ۶۷
---
# غزل شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>دوست دارم که مرا در بر خود بنشانی</p></div>
<div class="m2"><p>شیشه را آن طرف دیگر خود بنشانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه نزدیک‌تر از من بتو زو رشک برم</p></div>
<div class="m2"><p>شیشه را باید آنسوتر خود بنشانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زینطرف جام دهی زانطرفم بوس و لبم</p></div>
<div class="m2"><p>در میان لب جان‌پرور خود بنشانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چهره گلگون کنی از جام و ز رشک آتش را</p></div>
<div class="m2"><p>زار و افسرده به خاکستر خود بنشانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نسیم سحرم ده شبکی اذن دخول</p></div>
<div class="m2"><p>چند چون حلقه مرا بر در خود بنشانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به کی اسب به میدان وصالت تازد</p></div>
<div class="m2"><p>مدعی را چه شود بر خر خود بنشانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماه گردون سزدت تاج کله را چه محل</p></div>
<div class="m2"><p>که ز اکرام به فرق سر خود بنشانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کعبتین چشمی و من مهره چو نراد مرا</p></div>
<div class="m2"><p>می‌زنی مهره که در ششدر خود بنشانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مادرت حور بود غیرتم آید که به خلد</p></div>
<div class="m2"><p>صالحان را ببر مادر خود بنشانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دامن پاک وی آلوده شود قاآنی</p></div>
<div class="m2"><p>ترسم او را تو به ‌چشم تر خود بنشانی</p></div></div>