---
title: >-
    غزل شمارهٔ ۶۲
---
# غزل شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>مگر دریچهٔ نوری تو یا نتیجهٔ حوری</p></div>
<div class="m2"><p>که فرق تا به قدم غرق در لطافت و نوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا تو مردم چشمی چه غم که غایبی از من</p></div>
<div class="m2"><p>حضور عین چه‌ حاجت بود که عین حضوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گمان برند خلایق که حور بچه نزاید</p></div>
<div class="m2"><p>خلاف من که یقین دانمت که بچهٔ حوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو عکس ماه که افتد درون چشمهٔ روشن</p></div>
<div class="m2"><p>به‌ چشم من همه‌ نزدیکی و ز من همه‌ دوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به لطف آب حیاتی به طیب باد بهاری</p></div>
<div class="m2"><p>به‌ بوی خاک بهشتی به نور آتش‌ طوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو عشق رهزن عقلی چو عقل زینت روحی</p></div>
<div class="m2"><p>چو روح زیور عمری چو عمر مایهٔ سوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بتی نه لعبت چینی تنی نه باد بهاری</p></div>
<div class="m2"><p>گلی نه باغ بهشتی مهی نه حور قصوری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز شرم روی تو شاید که آفتاب بگیرد</p></div>
<div class="m2"><p>کنون که عنبر سارا دمیدت از گل سوری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به عشق دوست کنم ناز بر ملالت دشمن</p></div>
<div class="m2"><p>که‌ عشق‌ را نتوان کرد چاره‌ای‌ به صبوری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به یک دو جام که قاآنیا ز دوست گرفتی</p></div>
<div class="m2"><p>چو جام باده سراپا همه نشاط و سروری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر آستان ولیعهد این جلال ترا بس</p></div>
<div class="m2"><p>که روز و شب چو سعادت ز واقفان حضوری</p></div></div>