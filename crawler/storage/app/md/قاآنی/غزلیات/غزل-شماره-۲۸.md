---
title: >-
    غزل شمارهٔ ۲۸
---
# غزل شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>ای رفیقان امشب اسماعیل غوغا می‌کند</p></div>
<div class="m2"><p>چنگ را ز آواز شورانگیز رسوا می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسمان امشب ز حیرانی سراپا گشته چشم</p></div>
<div class="m2"><p>صنع حق را در وجود او تماشا می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راه گوش عاشقان از لحن دلکش می‌زند</p></div>
<div class="m2"><p>صید چشم ناظران از روی زیبا می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نغمهٔ شیرین او گویی غذای روح ماست</p></div>
<div class="m2"><p>کز لطافت در دل و مغز و جگر جا می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حلق داودست گویی در گلویش تعبیه</p></div>
<div class="m2"><p>زان مزامیرش اثر در سنگ خارا می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم در خمیازه می‌افتد ز شوق روی او</p></div>
<div class="m2"><p>خاصه‌ آن دم کز پی خواندن دهن وا می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخت می‌ترسد ز تنهایی دلش‌ گردد ملول</p></div>
<div class="m2"><p>زان سبب در کشتن عاشق مدارا می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرد او آشفتگان جمعند و گویی ساحری‌ست</p></div>
<div class="m2"><p>کز بنات‌النعش ترکیب ثریا می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون لب ساغر لب شیرین شورانگیز او</p></div>
<div class="m2"><p>بس که جان بخش است بوسیدن تقاضا می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاهد و شمع‌ و شراب و شهد و شکر گو مباش</p></div>
<div class="m2"><p>کار آن هر پنج را او خود به تنها می‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وقت خواندن گر لب شیرین او بیند مگس</p></div>
<div class="m2"><p>بر لب او می‌نشیند ترک حلوا می‌کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس که سرتا پای شیرینست اگر آید به باغ</p></div>
<div class="m2"><p>باغبان او را خیال نخل خرما می‌کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر فلاطون الهی آید از یونان به فارس</p></div>
<div class="m2"><p>او به ‌یک لحن عراقش مست و شیدا می‌کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر بدانم در بهشتم اینچنین غلمان دهند</p></div>
<div class="m2"><p>خاطرم پیش از اجل مردن تمنا می‌کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرکجا کآواز شورانگیز او گردد بلند</p></div>
<div class="m2"><p>شادی از دنیا و عقبی رو بدان جا می‌کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در وجودش از هجوم حسن هرسو محشرست</p></div>
<div class="m2"><p>با چنین زیبایی از محشر چه پروا می‌کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر خردمندی بکاود تا قیامت زلف او</p></div>
<div class="m2"><p>زیر هر چینش دلی دیوانه پیدا می‌کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرکه از اهل وطن روزی صدای او شنید</p></div>
<div class="m2"><p>روز دیگر چون مسافر سر به صحرا می‌کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وین عجب‌تر گر مسافر بیندش در ملک فارس</p></div>
<div class="m2"><p>از وطن دل می‌کَنَد در فارس مأوا می‌کند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سر به دوش هم‌نشینان چون نهد وقت سرود</p></div>
<div class="m2"><p>ماه را مانَد که جا در برج جوزا می‌کند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بار منت می‌نهد بر دوش یاران زان سبب</p></div>
<div class="m2"><p>وقت خواندن تکیه بر دوش احبا می‌کند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سینهٔ او چون به درد آید به ‌درد آید دلم</p></div>
<div class="m2"><p>کز احبا رو چرا سوی اطبا می‌کند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>روز مردم تی*‌راهد ورنه چشمت تار نیست</p></div>
<div class="m2"><p>سرمه در چشم سیاه خود به عمدا می‌کند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هیچ کحالی ندیدم بهتر از رخسار او</p></div>
<div class="m2"><p>زان که چشمش‌ هرکجا کوری‌ست بینا می‌کند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دل به مستی یک شب از دستم به عیاری ربود</p></div>
<div class="m2"><p>هرچه می‌گویم بده امرو‌ز و فردا می‌کند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بوسهٔ جان‌بخش و چشم ‌جان‌سِتانش هر نَفَس</p></div>
<div class="m2"><p>کار عزرائیل و اعجاز مسیحا می‌کند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زان خدای عاشقان دارد لقب کز چشم و لب</p></div>
<div class="m2"><p>می‌کشد هر لحظه خلقی را و احیا می‌کند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از جمال او شرف دارد زمین و آسمان</p></div>
<div class="m2"><p>حس او گویی جهان را زیر و بالا می‌کند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گو نشیند ترش و گوید تلخ و‌ گردد تند و تیز</p></div>
<div class="m2"><p>شوربخت است آنکه با شیرین معادا می‌کند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جو‌شن داود دزدیدست کاین‌ موی منست</p></div>
<div class="m2"><p>با وجود آنکه از دزدی تبرا می‌کند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ماه‌ را در مشک‌ پنهان کرده کاین روی منست</p></div>
<div class="m2"><p>ور کسی گوید که این ماهست، حاشا می‌کند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بس عجب دارم که زلف او چرا دیوانه است</p></div>
<div class="m2"><p>با وجود آنکه عقل و هوش یغما می‌کند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در جمال اوست قاآنی چنین شیرین‌زبان</p></div>
<div class="m2"><p>جلوهٔ آیینه طوطی را شکرخا می‌کند</p></div></div>