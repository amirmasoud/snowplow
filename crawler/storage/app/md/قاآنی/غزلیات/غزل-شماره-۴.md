---
title: >-
    غزل شمارهٔ ۴
---
# غزل شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>ضحاک ‌وار کشته بسی بی گناه را</p></div>
<div class="m2"><p>بر دوش تا فکنده دو مار سیاه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قصد ذقن نمودمش از زلف عنبرین</p></div>
<div class="m2"><p>چشمم ندید در شب تاریک چاه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوش از سرم به چابکی آن شوخ کج‌کلاه</p></div>
<div class="m2"><p>برد آنچنان که دزد شب از سر کلاه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیران زاهدم که بر آن روی چون بهشت</p></div>
<div class="m2"><p>از ابلهی گناه شمارد نگاه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می خوردنم به مجلس جانان گناه نیست</p></div>
<div class="m2"><p>آسوده در بهشت چه داند گناه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صوفی نشد ریاضت چل ساله سودمند</p></div>
<div class="m2"><p>یک دم بیا و میکده کن خانقاه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کو بادهٔ دو ساله و ماه دو هفته‌ای</p></div>
<div class="m2"><p>تا شب به عیش روز کنم سال و ماه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر روز و شب به یاد جمال جمیل تو</p></div>
<div class="m2"><p>نظاره می‌کنم رخ خورشید و ماه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در گیسوی سیاه تو دلها چو شبروان</p></div>
<div class="m2"><p>گم کرده‌اند در شب تاریک راه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دارم دلی گرفته و مشکل که شاه عشق</p></div>
<div class="m2"><p>در این فضای تنگ زند بارگاه را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وقتست کز تطاول آن چشم فتنه‌جوی</p></div>
<div class="m2"><p>آگه کنیم لشکر عباس شاه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاهی که خاک درگه گردون اساس او</p></div>
<div class="m2"><p>تاج زر است تارک خورشید و ماه را</p></div></div>