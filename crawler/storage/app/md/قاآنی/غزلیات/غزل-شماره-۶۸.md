---
title: >-
    غزل شمارهٔ ۶۸
---
# غزل شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>ای شوخ نازپرور آشوب عقل و دینی</p></div>
<div class="m2"><p>طیب بهار خلدی زیب نگار چینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کم مهر و زود خشمی گلچهر و شوخ چشم</p></div>
<div class="m2"><p>طرار و دلفریبی طناز و نازنینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیدی از آن شر‌یفی روحی از آن لطیفی</p></div>
<div class="m2"><p>حوری از آن جمیلی نوری از آن مبینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سروی ولی روانی جانی ولی عیانی</p></div>
<div class="m2"><p>ماهی ولی تمامی مایی ولی معینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حلق تشنه کامان یک جرعه سلسبیلی</p></div>
<div class="m2"><p>درکام تلخ عیشان یک کوزه انگبینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آهوی مشک مویی طاووس بذله گویی</p></div>
<div class="m2"><p>شمشاد سروقدی خورشید مه جبینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پروردهٔ بهشتی همشیرهٔ سهیلی</p></div>
<div class="m2"><p>نوباوهٔ بهاری فرزند فرودینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک جویبار سروی یک بوشان تذروی</p></div>
<div class="m2"><p>یک باغ لاله برگی یک دسته یاسمینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک مشرق آفتابی یک خانه ماهتابی</p></div>
<div class="m2"><p>یک عرش روح قدسی یک خلد حور عینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون طعنهٔ رقیبان در هجر جانگدازی</p></div>
<div class="m2"><p>چون نکتهٔ ادیبان در وصل دلنشینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همزاد روح پاکی گرچه زآب و خاکی</p></div>
<div class="m2"><p>عم زاد حور عینی گرچه ز ماء و طینی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از حلقهای گیسو داود درع سازی</p></div>
<div class="m2"><p>وز لعل روح‌پرور عیسای جم نگینی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تشویر نار نمرود از چهر پرفروغی</p></div>
<div class="m2"><p>تصویر مار ضحاک از زلف پر ز چینی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باک از خزان نداری گویی گل بهشی</p></div>
<div class="m2"><p>ارزان به کف نیایی مانا دُر ثمینی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بوسیدن لب تو فرضست برخلایق</p></div>
<div class="m2"><p>تا شاه راستان را مداح راستینی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فرمانده سلاطین جمجاه ناصرالدین</p></div>
<div class="m2"><p>آن کش سپهر گوید تو پور آتبینی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای کز سنان سر پخش‌ آجال را ضمانی</p></div>
<div class="m2"><p>وی کز بنان زر بخش آمال را ضمینی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شاهنشه جهانی فرمانده مهانی</p></div>
<div class="m2"><p>آسایش زمانی آرایش زمینی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در رزم بی‌مثالی در بزم بی‌همالی</p></div>
<div class="m2"><p>در عزم بی‌نظیری در حزم بی‌قرینی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مسجود شرق‌ و غربی‌ محسود روم‌ و روسی</p></div>
<div class="m2"><p>بنیان عقل و شرعی برهان داد و دینی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دارای تاج و گنجی داروی درد و رنجی</p></div>
<div class="m2"><p>منشور دین و دادی منشار کفر و کینی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کوهی چو بر سمندی شیری چو با کمندی</p></div>
<div class="m2"><p>چرخی چو باکمانی دهری چو درکمینی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در حمله روز ناورد چابکتر ازگمانی</p></div>
<div class="m2"><p>در وقعه پیش دشمن ثابت‌تر از یقینی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تندر چگونه غرّد تو گاه کین چنانی</p></div>
<div class="m2"><p>خنجر چگونه برّد در نظم دین چنینی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون حزم زودیابی چون حلم دیر خشمی</p></div>
<div class="m2"><p>چون فکر دورسنجی چون عقل پیش‌بینی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>با قدرت قبادی بافرهٔ فرودی</p></div>
<div class="m2"><p>با شوکت ینالی با مکنت تکینی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با صولت کیانی با دولت جوانی</p></div>
<div class="m2"><p>با همت بلندی با فکرت متینی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شاه ملک شعاری شیر فلک شکاری</p></div>
<div class="m2"><p>ایام را یساری اسلام را یمینی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هم عقل را قوامی هم عدل را نظامی</p></div>
<div class="m2"><p>هم شرع را امانی هم ملک را امینی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هم مکرمت شعاری هم مملکت طرازی</p></div>
<div class="m2"><p>هم مسألت پذیری هم معدلت گزینی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بحر سحاب خیزی چون از بر سریری</p></div>
<div class="m2"><p>بدر شهاب تیری چون بر فراز زینی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ملک ترا هماره حق ناصر و معین باد</p></div>
<div class="m2"><p>زانسان که دین حق را تو ناصر و معینی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پیوسته بر سراپات از عرش آفرین‌ باد</p></div>
<div class="m2"><p>زآنروکه پای تا سر یک عرش آفرینی</p></div></div>