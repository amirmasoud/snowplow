---
title: >-
    غزل شمارهٔ ۷۱
---
# غزل شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>به هر چه وصف نمایم ترا به زیبایی</p></div>
<div class="m2"><p>جمیل‌تر ز جمالی چو روی بنمایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صفت کنند نکویان شهر را به جمال</p></div>
<div class="m2"><p>تو با جمال چنین در صفت نمی‌آیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ناتوانی من بین ترحّمی فرما</p></div>
<div class="m2"><p>که نیست با تو مرا پنجهٔ توانایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر معاینه‌ات بنگرند و بشناسند</p></div>
<div class="m2"><p>که چون ز چشم روی در صفت نمی‌آیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حد حس تو زیور نمی‌رسد ترسم</p></div>
<div class="m2"><p>که زشت‌تر شوی ار خویشتن بیارایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تفاوت شب و روز از برای ماست نه تو</p></div>
<div class="m2"><p>از آن سبب که تو خود مهر عالم‌آرایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب وصال تو دانستم از چه کوتاهست</p></div>
<div class="m2"><p>تو خود ستارهٔ روزی چو پرده بگشایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگس ز سر ننهد شوق عشق شیرینی</p></div>
<div class="m2"><p>بابرویی که ترش کرده است حلوایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خاکپای عزیز تو بر ندارم سر</p></div>
<div class="m2"><p>که نیست از تو مرا طاقت شکیبایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به قول مدعیان از تو برندارم دست</p></div>
<div class="m2"><p>وگر ز عشق توکارم کشد به رسوایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگر تو با رخ خود بعد ازین بورزی عشق</p></div>
<div class="m2"><p>از آنکه هم گل و هم عندلیب گویایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به سرو و ماه از آن عاشقست قاآنی</p></div>
<div class="m2"><p>که ماه سروقد و سرو ماه‌سیمایی</p></div></div>