---
title: >-
    غزل شمارهٔ ۱۲
---
# غزل شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>دل هرجایی من آفت جانست و تنست</p></div>
<div class="m2"><p>آتش عمر خود و برق تن و جان منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سر زلف بتانش نتوان کردن فرق</p></div>
<div class="m2"><p>در تن تیره‌اش از بس که شکنج و شکنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاصل وقتم از آن نیست به جز رنج و بلا</p></div>
<div class="m2"><p>نه دلست این به حقیقت که بلا و فتنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده آ‌زادی خود را به گرفتاری خویش</p></div>
<div class="m2"><p>زین سبب عشق نکویانش شعارست و فنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ره غمزهٔ مهرویان از تیر نگاه</p></div>
<div class="m2"><p>راست مانندهٔ مرغیست که بر بابزن‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاه با اژدر زلفست چو بهمنش مدار</p></div>
<div class="m2"><p>بیژن‌آسا گهی افتاده به چاه ذقنست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکجا صارم ابرویی آنجا سپرست</p></div>
<div class="m2"><p>هرکجا ناوک مژگانی آنجا مجنست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه چون قمری بر سرو قدی نغمه‌ سراست</p></div>
<div class="m2"><p>گاه دهقان و به پیرایش باغ سمنست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گه چو بیند صنمی گلرخ و سیمین اندام</p></div>
<div class="m2"><p>عندلیب‌آسا بر شاخ گلش نغمه زنست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکجا روی بتی بیند در سجدهٔ او</p></div>
<div class="m2"><p>قد دو تا کرده چو در سجدهٔ بت برهمنست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در پرستیدن بت‌رویان از بس مولع</p></div>
<div class="m2"><p>راست پنداری آن ‌یک صنم این یک شمنست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سال و مه عشق بتان و زرد و رنجه نشود</p></div>
<div class="m2"><p>عیش او مانا از رنج وگداز و محنست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در ره دانش و دین کاهل و خیره است و زبون</p></div>
<div class="m2"><p>لیک در کار هوس چیره‌تر از اهرمنست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روز اگر شام کند بی‌رخ یوسف چهری</p></div>
<div class="m2"><p>خلوت سینه بر او ساحت بیت‌الحزنست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرچه گویمش دلا توبه کن و عشق مورز</p></div>
<div class="m2"><p>که سر‌انجام هوس سخرهٔ مردم شدنست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غیر ناکامی و بدنامی ازین عشق نزاد</p></div>
<div class="m2"><p>ابله آنکش سر فانی شدن خویشتنست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فهم گردآر و خرد پیشه کن و دانش‌جوی</p></div>
<div class="m2"><p>کانکه عقل و خردش نی به سفه مفتتنست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دل به‌ خشم آید و بخروشد و راند به جواب</p></div>
<div class="m2"><p>حبذا رای حکیمی که بدینسان حسنست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باد بر حکمت نفرین اگر اینست حکیم</p></div>
<div class="m2"><p>که حکیمان را آماده به هجو سننست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حاصل هستی ما هستی عشق آمد و او</p></div>
<div class="m2"><p>منعم از عشق فراگوید کاین نزفطنست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای حکیم خرد اندوز سبک تاز که من</p></div>
<div class="m2"><p>عشق می‌بازم و این قاعده رسمی کهنست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حکما متفقستند که خلق از پی عشق</p></div>
<div class="m2"><p>خلق گشتند و درین کس را کی لاولنست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عشق اگر می نبود نفس مهذب نشود</p></div>
<div class="m2"><p>عشق زی بام کمالات روانرا رسنست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز آتش عشق بنگدازد تا هیکل جسم</p></div>
<div class="m2"><p>کی بر افلاک شود جان که ترا در بدنست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بی‌ریاضت نشود جان تو با فر و بها</p></div>
<div class="m2"><p>شمع را فر و بها جمله ز گردن زدنست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>متفاوت بود این عشق به ذرات وجود</p></div>
<div class="m2"><p>ور نه پیدا ز کجا فرق لجین از لجنست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>متفاوت شد از آن روی مقامات کمال</p></div>
<div class="m2"><p>که به مقدار نظر هرکه خبیر از سخنست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پرتو عشق بود یکسره از تابش مهر</p></div>
<div class="m2"><p>هان و هان بشمر تا شمع که اندر لگنست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فهم این نکته نیارد همه کس کرد مگر</p></div>
<div class="m2"><p>خواجهٔ عصر که در عشق دلش ممتحنست</p></div></div>