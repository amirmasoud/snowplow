---
title: >-
    غزل شمارهٔ ۱۹
---
# غزل شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>دوش‌ رندی خلوتی‌ خوش خالی از اغیار داشت</p></div>
<div class="m2"><p>حورش از فردوس و غلمانش ز جنت عار داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهدش خوشتر ز غلمان زانکه‌ غلمان دربهشت</p></div>
<div class="m2"><p>ذکر استغفار و آن الحان موسیقار داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حورالقدوس والقدوس و آن زیبا سرشت</p></div>
<div class="m2"><p>الصبوح والصبوح اوراد در اسحار داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندر افتادند حالی آندو سیمین تن بهم</p></div>
<div class="m2"><p>‌کاین شغب بسیار و آن دیگر شبق بسیار داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب همی سودند برهم آری آن را این‌ سزد</p></div>
<div class="m2"><p>کاین به‌لب‌شنگرف وآن برپشت‌لب‌زنگار داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نغمهای آوخ آوخ خاست زان حورا سرشت</p></div>
<div class="m2"><p>کانچنان دلکش‌ نوایی زخمهٔ مزمار داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمش در عین وصل این ناله و فریاد چیست</p></div>
<div class="m2"><p>گفت ما را جلوهٔ معشوق در این کار داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>الغرض با آب غلمان چشمه‌سار حور را</p></div>
<div class="m2"><p>شیوهٔ جنات تجری تحتهاالانهار داشت</p></div></div>