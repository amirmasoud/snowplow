---
title: >-
    غزل شمارهٔ ۳۲
---
# غزل شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>خلق را قصهٔ حسن پری از یاد رود</p></div>
<div class="m2"><p>هرکجا ذکری از آن شوخ پریزاد رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر شکایت که مرا از تو بود در دل تنگ</p></div>
<div class="m2"><p>چون کنم یاد وصالت همه از یاد رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکجا کز رخ و بالای تو گویند سخن</p></div>
<div class="m2"><p>ظلم باشدکه حدیث ازگل و شمشاد رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت آنست که تا سنبلهٔ چرخ مرا</p></div>
<div class="m2"><p>از غم سنبل گیسوی تو فریاد رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از طرب عارف و عامی همه در رقص آیند</p></div>
<div class="m2"><p>هرکجا ذکری از آن حسن خداداد رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون شود دجله ز اشک از خبر گریهٔ من</p></div>
<div class="m2"><p>وقتی از خطهٔ کرمان سوی بغداد رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن نه بالاست بلاییست که از رفتن او</p></div>
<div class="m2"><p>دل و دین و سر و سامان همه بر باد رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با زبان چو منی خاصه که در مدحت شاه</p></div>
<div class="m2"><p>ستمست ار سخن از سوسن آزاد رود</p></div></div>