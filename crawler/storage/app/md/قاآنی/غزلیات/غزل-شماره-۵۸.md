---
title: >-
    غزل شمارهٔ ۵۸
---
# غزل شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>یارکی هست مرا به لطافت ملکو</p></div>
<div class="m2"><p>به حلاوت شکر و به ملاحت نمکو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی مرا گفت به طیش غم برانگیخته جیش</p></div>
<div class="m2"><p>از پی موکب عیش ساخت باید یزکو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیز و آن باده بنوش‌ که روی پاک ز هوش‌</p></div>
<div class="m2"><p>رودت جوش و خروش‌ بسماک از سمکو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پشه زو پیل شود قطره زو نیل شود</p></div>
<div class="m2"><p>زو ابابیل شود باز سیمین پر کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جرعهٔ می هاتوا که جم و کی ماتوا</p></div>
<div class="m2"><p>جملگی قد فاتوا همگی قد هلکو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیخنا بهر عوام ساخته دانه و دام</p></div>
<div class="m2"><p>دانه‌اش‌ سبحهٔ خام دام تحت‌الحنکو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر دیبای طراز تا کیت جان بگداز</p></div>
<div class="m2"><p>شادمان باش‌ و بساز با قبای قدکو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هله قاآنی هان نقد خود دار نهان</p></div>
<div class="m2"><p>که شد از غیب عیان نقدها را محکو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمع شیراز منم نکته‌پرداز منم</p></div>
<div class="m2"><p>همه تن ناز منم تو چه گویی کلکو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فعلاتن فعلن فعلاتن فعلن</p></div>
<div class="m2"><p>هست تقطیع سخن دک دکادک دککو</p></div></div>