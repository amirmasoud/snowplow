---
title: >-
    غزل شمارهٔ ۱۰
---
# غزل شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>دل دیوانه که خود را به سر زلف تو بستست</p></div>
<div class="m2"><p>کس بر او دست نیابد که سر زلف تو بستست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چکند طالب چشمت که ز جان دست نشوید</p></div>
<div class="m2"><p>بوی خون آید از آن مست‌ که شمشیر به دست است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به امیدی که شبی سرزده مهمان من آیی</p></div>
<div class="m2"><p>چشم در راه و سخن بر لب و جان بر کف دست است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من و وصل تو خیالیست که صورت نپذیرد</p></div>
<div class="m2"><p>که ترا پایه بلندست و مرا طالع پستست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم از دست تو روزی بنهم سر به بیابان</p></div>
<div class="m2"><p>دست در زلف زد و گفت‌ کیت پای ببستست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاش لله که رهایی دلم از زلف تو بیند</p></div>
<div class="m2"><p>که دلم ماهی بسمل بود و زلف تو شستست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرد آن دانهٔ خال تو سیه موی تو دامست</p></div>
<div class="m2"><p>دل شناسد که تنی هرگز ازین دام نجستست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل قاآنی ازینسان که به زلف تو گریزد</p></div>
<div class="m2"><p>چ‌ون برآشفته یکی رومی هندوی پرستست</p></div></div>