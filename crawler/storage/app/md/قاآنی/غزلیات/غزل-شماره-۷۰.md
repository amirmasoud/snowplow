---
title: >-
    غزل شمارهٔ ۷۰
---
# غزل شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>دلبران اخترند و تو ماهی</p></div>
<div class="m2"><p>نیکوان لشکرند و تو شاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندگویی دلت چگونه بود</p></div>
<div class="m2"><p>تو درون دلی خود آگاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس درازستی ای شب یلدا</p></div>
<div class="m2"><p>لیک با زلف دوست کوتاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اول از دشمنان برآورگرد</p></div>
<div class="m2"><p>آخر از دوستان چه می‌خواهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماه نو خوانمت از آنکه به حسن</p></div>
<div class="m2"><p>می‌فزایی همی نیمکاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یوسف ار با تو لاف حسن زند</p></div>
<div class="m2"><p>گو تو هرچند صاحب جاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لیک من چاه بر زنخ دارم</p></div>
<div class="m2"><p>کف به زیر زنخ تو در چاهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لاف طاقت مزن دلاکه ترا</p></div>
<div class="m2"><p>شیر پنداشتیم و روباهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتی از طاقتم چوکوه گرن</p></div>
<div class="m2"><p>چون بدیدم سبک‌تر ازکاهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پنجه با باد کمترک می‌زن</p></div>
<div class="m2"><p>ای که از ضعف کمتر ازکاهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چونی از هجر دوست قاآنی</p></div>
<div class="m2"><p>تن پر از زخم و دل پر از آهی</p></div></div>