---
title: >-
    غزل شمارهٔ ۳۰
---
# غزل شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>شب دوشین‌ که مرا لب به لب نوشین بود</p></div>
<div class="m2"><p>شب که از عمر شمردیم شب دوشین بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاه لب بر لب جانانه و گه بر لب جام</p></div>
<div class="m2"><p>تا دم صبح مرا کار به شب دوش این بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوعروسیست جهیزش‌ همه شادی و نشاط</p></div>
<div class="m2"><p>دختر زر نتوان گفت گران کابین بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوق آن ماه روان از مژه‌ام پروین داشت</p></div>
<div class="m2"><p>کار چشمم همه شب با مه و با پروین بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کس‌ نداند که چه دیدم من از آن گردش چشم</p></div>
<div class="m2"><p>مگر آن صعوه که در صیدگه شاهین بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاه در دامن و آغوش من آن خرمن گل</p></div>
<div class="m2"><p>گاه در گردنم آن سلسلهٔ مشکین بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ریخت خونم به جفا یار و خوشم قاآنی</p></div>
<div class="m2"><p>که مرا کامی اگر بود به عالم این بود</p></div></div>