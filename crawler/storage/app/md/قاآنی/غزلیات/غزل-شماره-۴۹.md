---
title: >-
    غزل شمارهٔ ۴۹
---
# غزل شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>بکش ار کشی به تیغم، بزن ار زنی به تیرم</p></div>
<div class="m2"><p>بکن آنچه می‌توانی که من از تو ناگزیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه شرط عاشق آنست که کام دوست جوید</p></div>
<div class="m2"><p>بکن ار کنی قبولم، ببر ار بری اسیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر من فرو نیاید به کمند پهلوانان</p></div>
<div class="m2"><p>تو کنی به تار مویی همه روزه دستگیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظر ار ز دوست پوشم که برون رود ز چشمم</p></div>
<div class="m2"><p>به چه اقتدار گویم که برون شو از ضمیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جهان کناره کردم که تو در کنارم آیی</p></div>
<div class="m2"><p>مگر ای جوان رهانی ز غم جهان پیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو به راه باد گویا سر زلف خود گشودی</p></div>
<div class="m2"><p>که ز مغز جای عطسه همه می‌جهد عبیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طلب از خدای کردم که بمیرم ار نیایی</p></div>
<div class="m2"><p>تو نیامدی و ترسم که درین طلب بمیرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگرم نظر بدوزی به خدنگ جور ورنه</p></div>
<div class="m2"><p>همه تا حیات دارم نظر از تو برنگیرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هوای مهر محمود چو ذره در نشاطم</p></div>
<div class="m2"><p>که چو آفتاب روزی به فلک برد امیرم</p></div></div>