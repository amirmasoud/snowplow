---
title: >-
    غزل شمارهٔ ۱۱
---
# غزل شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>قوت من باده قوتم یارست</p></div>
<div class="m2"><p>وآدمی را همین دو درکارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیش آدم بود به قوت و قوت</p></div>
<div class="m2"><p>قوت و قوت نیست مردارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر ولایت که خوبرویی هست</p></div>
<div class="m2"><p>هرکه جز اوست نقش دیوارست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که گفتی مبین به صورت خوب</p></div>
<div class="m2"><p>صورت خوب بهر دیدارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوش اگر نشنود حکایت یار</p></div>
<div class="m2"><p>بر بناگوش مردمان بارست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم اگر ننگرد به صورت خوب</p></div>
<div class="m2"><p>پیشه بر روی آدمی عارست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل به مستی ربود نرگس دوست</p></div>
<div class="m2"><p>به خدا مست نیست هشیارست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم یار ار چه هست خواب‌آلود</p></div>
<div class="m2"><p>اندرو هرچه فتنه بیدارست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دستم ای همسفر ز دست بدار</p></div>
<div class="m2"><p>که مرا پای دل گرفتارست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خودکشم رنج و خودکنم شکوه</p></div>
<div class="m2"><p>درد عشق ای رفیق بسیارست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر من مست چند طعنه زنی</p></div>
<div class="m2"><p>آخر ای زاهد این چه آزارست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر عبادت به مردم آزاریست</p></div>
<div class="m2"><p>زان عبادت خدای بیزارست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من ز دریا روم تو از خشکی</p></div>
<div class="m2"><p>به سوی کعبه راه بسیارست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نفس بیدار گفت دارم شیخ</p></div>
<div class="m2"><p>نه چنانست نقش پندارست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>موشکافست طبع قاآنی</p></div>
<div class="m2"><p>از چنین طبع جای زنهارست</p></div></div>