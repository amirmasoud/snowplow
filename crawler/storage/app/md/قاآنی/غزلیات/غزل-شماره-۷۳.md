---
title: >-
    غزل شمارهٔ ۷۳
---
# غزل شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>تو را رسمست اول دلربایی</p></div>
<div class="m2"><p>نخستین مهر و آخر بی‌ وفایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در اول می‌نمایی دانهٔ خال</p></div>
<div class="m2"><p>در آخر دام گیسو می گشایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو کوته می‌نمودی زلف گفتم</p></div>
<div class="m2"><p>یقین کوته شود شام جدایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندانستم کمند طالع من</p></div>
<div class="m2"><p>ز بام وصل یابد نارسایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برآن بودم که از آهن کنم دل</p></div>
<div class="m2"><p>ندانستم که تو آهن‌ربایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من آن روز از خرد بیگانه گشتم</p></div>
<div class="m2"><p>که با عشق توکردم آشنایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نپندارم که باشد تا دم مرگ</p></div>
<div class="m2"><p>گرفتار محبت را رهایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا شاهی چنان لذت نبخشد</p></div>
<div class="m2"><p>که اندر کوی مه رویان گدایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سحر جانم برآمد بی‌تو از لب</p></div>
<div class="m2"><p>گمان بردم تویی از در درآیی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو دیدم جان محزون بود گفتم</p></div>
<div class="m2"><p>برو دانم که بی‌جانان نپایی</p></div></div>