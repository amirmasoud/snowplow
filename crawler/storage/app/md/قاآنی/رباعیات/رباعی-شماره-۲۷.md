---
title: >-
    رباعی شمارهٔ ۲۷
---
# رباعی شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>تو مردمک چشم من مهجوری</p></div>
<div class="m2"><p>زان با همه نزدیکیت از من دوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی نی غلطم تو جان شیرین منی</p></div>
<div class="m2"><p>زان با منی و ز چشم من مستوری</p></div></div>