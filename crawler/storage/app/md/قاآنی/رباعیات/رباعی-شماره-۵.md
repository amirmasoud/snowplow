---
title: >-
    رباعی شمارهٔ ۵
---
# رباعی شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>ابروی کجت که دل برو مشتاقست</p></div>
<div class="m2"><p>محراب شهان و قبلهٔ آفاقست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاقست ولی به دلنشینی جفتست</p></div>
<div class="m2"><p>جفتست ولی ز بیقرینی طاقست</p></div></div>