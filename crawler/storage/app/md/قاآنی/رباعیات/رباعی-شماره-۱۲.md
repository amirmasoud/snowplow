---
title: >-
    رباعی شمارهٔ ۱۲
---
# رباعی شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>یک عمر شهان تربیت جیش کنند</p></div>
<div class="m2"><p>تا نیم نفس عیش به صد طیش کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نازم به جهان همت درویشان را</p></div>
<div class="m2"><p>کایشان به یکی لقمه دوصد عیش کنند</p></div></div>