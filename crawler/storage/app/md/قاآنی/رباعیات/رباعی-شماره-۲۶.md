---
title: >-
    رباعی شمارهٔ ۲۶
---
# رباعی شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>شوخی که بیاض گردن روشن او</p></div>
<div class="m2"><p>آغشته به صندل شده پیرامن او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبحست و به سرخی شفق آلوده</p></div>
<div class="m2"><p>یا خون خلایقست در گردن او</p></div></div>