---
title: >-
    رباعی شمارهٔ ۲۰
---
# رباعی شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>صدرا دیشب به باغ نواب شدم</p></div>
<div class="m2"><p>امروز به حضرتت شرفیاب شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن باغ چو روی ناکسان آب نداشت</p></div>
<div class="m2"><p>از خجلت بی‌آبی او آب شدم</p></div></div>