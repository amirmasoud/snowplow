---
title: >-
    رباعی شمارهٔ ۱۸
---
# رباعی شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>با آنکه هنوز از می دوشین مستم</p></div>
<div class="m2"><p>در مهد طرب به خواب نوشین هستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دست خدا بگیر لختی دستم</p></div>
<div class="m2"><p>کز سخت‌دلی و سست‌بختی رستم</p></div></div>