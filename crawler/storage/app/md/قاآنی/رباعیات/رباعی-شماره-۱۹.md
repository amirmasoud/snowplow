---
title: >-
    رباعی شمارهٔ ۱۹
---
# رباعی شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>تا دل به هوای وصل جانان دادم</p></div>
<div class="m2"><p>لب بر لب او نهادم و جان دادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خضر ار ز لب چشمهٔ حیوان جان یافت</p></div>
<div class="m2"><p>من جان به لب چشمهٔ حیوان دادم</p></div></div>