---
title: >-
    رباعی شمارهٔ ۲۵
---
# رباعی شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>تاکی غم زید و گه غم عمرو خوریم</p></div>
<div class="m2"><p>آن به که به جای غم ز خم خمر خوریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش باش به نیش‌ و نوش کز نخل حیات</p></div>
<div class="m2"><p>فرضست که گه خار و گهی تمر خوریم</p></div></div>