---
title: >-
    رباعی شمارهٔ ۲۸
---
# رباعی شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>نه باده نه جام باده ماند باقی</p></div>
<div class="m2"><p>نه ساده نه نام ساده ماند باقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما زادهٔ مام روزگاریم ولی</p></div>
<div class="m2"><p>نه زاده نه مام‌زاده ماند باقی</p></div></div>