---
title: >-
    رباعی شمارهٔ ۲۳
---
# رباعی شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>تا دست ارادت به تو دادست دلم</p></div>
<div class="m2"><p>دامان طرب زکف نهادست دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ره یافته در زلف دلاویز کجت</p></div>
<div class="m2"><p>القصه به راه کج فتادست دلم</p></div></div>