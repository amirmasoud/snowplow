---
title: >-
    رباعی شمارهٔ ۱۰
---
# رباعی شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>زلفین سیه که بر بناگوش تواند</p></div>
<div class="m2"><p>سر بر سر هم نهاده همدوش تواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساید سر از ادب به پایت شب و روز</p></div>
<div class="m2"><p>آری دو سیاه حلقه در گوش تواند</p></div></div>