---
title: >-
    رباعی شمارهٔ ۲
---
# رباعی شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>دوشینه فتادم به رهش مست و خراب</p></div>
<div class="m2"><p>از نشوهٔ عشق او نه از بادهٔ ناب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانست که عاشقم ولی می‌پرسید</p></div>
<div class="m2"><p>این کیست کجاییست چرا خورده شر‌اب</p></div></div>