---
title: >-
    شمارهٔ ۱۳ - وله ایضاً
---
# شمارهٔ ۱۳ - وله ایضاً

<div class="b" id="bn1"><div class="m1"><p>ای کرده سیه چشم تو تاراج دل و جان</p></div>
<div class="m2"><p>از فتنهٔ ترک تو جهانی شده ویران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی با تن سهراب کند خنجر رستم</p></div>
<div class="m2"><p>کاری که کند با دلم آن خنجر مژگان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آشفته مکن چون دل من کار جهانی</p></div>
<div class="m2"><p>بر باد مده یعنی آن زلف پریشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از گوی زنخدانت و چوگان سر زلف</p></div>
<div class="m2"><p>آسیمه سرم دایم چون گوی ز چوگان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گریهٔ من نرم نگردد دل سختت</p></div>
<div class="m2"><p>هرگز نکند باران تاثیر به سندان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نقطه و چون موی شد از غم تن و جانم</p></div>
<div class="m2"><p>در فهم میان و دهنت ای بت خندان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر وهم میان تو نهادستی تهمت</p></div>
<div class="m2"><p>بر هیچ دهان تو ببستستی بهتان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر و هم کسی هیچ ندیدم که کمر بست</p></div>
<div class="m2"><p>وز هیچ بیفشانده کسی گوهر غلطان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سروی تو و غیر از تو از آن چهرهٔ رنگین</p></div>
<div class="m2"><p>بر سرو ندیدم که کسی بست گلستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زلف تو کمندست و دو صد یوسف دل را</p></div>
<div class="m2"><p>آویخته دارد ز بر چاه زنخدان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر یاد لب لعل تو ای گفت تو لؤلؤ</p></div>
<div class="m2"><p>تا کی همی از جزع فرو ریزم مرجان</p></div></div>
<div class="b2" id="bn12"><p>در خوبی تو نقصان یک موی نبینم</p>
<p>اینست که با مهر کست روی نبینم</p></div>
<div class="b" id="bn13"><div class="m1"><p>بی‌ روی تو در شام فراق ای بت ارمن</p></div>
<div class="m2"><p>آهم ز فلک بگذرد و اشک ز دامن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیش نظرم نقش جمال تو مصور</p></div>
<div class="m2"><p>هرجا نگرم بام و در و خانه و برزن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای فتنهٔ عالم چه بلایی تو که شهری</p></div>
<div class="m2"><p>گشت از تو ندیم ندم و همدم شیون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از جوشن جان درگذرد تیر نگاهت</p></div>
<div class="m2"><p>هرگه به رخ آرایی آن زلف چو جوشن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از دوستیت آنچه به من آمده هرگز</p></div>
<div class="m2"><p>نامد به فرامرز یل ازکینهٔ بهمن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پیدا ز عذار تو بود لاله به خروار</p></div>
<div class="m2"><p>پنهان ز بازار تو بود نقره به خرمن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از لالهٔ تو رفته مرا خاری در پا</p></div>
<div class="m2"><p>از نقرهٔ تو مانده مرا باری بر تن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زین بار مرا کاسته چون که تن چون کوه</p></div>
<div class="m2"><p>زان خار مرا آمده دل روزن روزن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باریک‌تر از رشتهٔ سوزن بود آن لب</p></div>
<div class="m2"><p>سودای توام پیشه بود عشق توام فن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>با اینهمه‌ام دیدن روی تو پری‌شان</p></div>
<div class="m2"><p>با اینهمه‌ام جستن وصل تو پریون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون می‌نگرم بستن با دست به چنبر</p></div>
<div class="m2"><p>چون می‌شمرم سودن آبست به هاون</p></div></div>
<div class="b2" id="bn24"><p>هیهات که از وصل تو من طرف نبندم</p>
<p>از دیده به رخ گر همه شنگرف ببندم</p></div>
<div class="b" id="bn25"><div class="m1"><p>ای زلف تو پر حلقه‌تر از جوشن داود</p></div>
<div class="m2"><p>ای روی تو تابنده‌تر از آتش نمرود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>با جام و قدح زین‌ سپسم عمر شود صرف</p></div>
<div class="m2"><p>بگزیدم چون مشرب آن لعل می‌آلود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای سیمبر از جای فزا خیز و فروریز</p></div>
<div class="m2"><p>در ساغر زرین یکی آن آتش بی‌دود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پیش آر می و جام به رغم غم دیرین</p></div>
<div class="m2"><p>بی‌داروی می درد مرا نبود بهبود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز آن می که از آن هر دل غمگین شد خرم</p></div>
<div class="m2"><p>زآن می که از آن خاط‌ر پژمان شد خشنود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>می سیرت و هنجار حکیمست و تو دانی</p></div>
<div class="m2"><p>بیهوده حکیم این همه اصرار نفرمود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>با دختر زر تا نبود کس را سودا</p></div>
<div class="m2"><p>هیهات که برگیرد ازکار جهان سود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز آن باده که تابنده‌تر از چهر ایازست</p></div>
<div class="m2"><p>درده که شود عاقبت کارم محمود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مقصود من از باده تویی بو که به مستی</p></div>
<div class="m2"><p>آورد توان بوسه زنم بر رخ مقصود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از بوسه تو با من ز چه‌رو بخل بورزی</p></div>
<div class="m2"><p>از اشک چون من با تو نورزم بمگر جود</p></div></div>
<div class="b2" id="bn35"><p>بردی به فسون دل زکف عشق‌پرستان</p>
<p>دستان تو ای بس که بگویند به دستان</p></div>
<div class="b" id="bn36"><div class="m1"><p>ای تنگتر از سینهٔ عشاق دهانت</p></div>
<div class="m2"><p>باریکتر از فکر خردمند میانت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همسنگ قلل شد غمم از فکر سرینت</p></div>
<div class="m2"><p>همراز عدم شد تنم از عشق دهانت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>صد خار جفا در دلم از حسرت بشکست</p></div>
<div class="m2"><p>آن باغ که شد تعبیه بر سرو روانت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>قد تو بود تیر و کمان ‌آسا ابروت</p></div>
<div class="m2"><p>من جفته قد از حسرت آن تیر و کمانت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بگرفته سنان ترک نگاه تو مژگان</p></div>
<div class="m2"><p>می بگذرد از جوشن جان نوک سنانت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>با آنکه خورد خون جهان خاتم لعلت</p></div>
<div class="m2"><p>در زیر نگین آمده ملک دو جهانت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دیگر به پشیزی نخرم سرو چمن را</p></div>
<div class="m2"><p>گردد سوی ما مایل اگر سرو چمانت</p></div></div>
<div class="b2" id="bn43"><p>حسنی نه که آن را تو دل آزار نداری</p>
<p>صد حیف که پروای دل‌ زار نداری</p></div>