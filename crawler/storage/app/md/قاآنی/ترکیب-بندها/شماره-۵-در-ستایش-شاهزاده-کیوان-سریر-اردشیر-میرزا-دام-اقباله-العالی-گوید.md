---
title: >-
    شمارهٔ ۵ - در ستایش شاهزادهٔ کیوان سریر اردشیر میرزا دام اقباله‌العالی گوید
---
# شمارهٔ ۵ - در ستایش شاهزادهٔ کیوان سریر اردشیر میرزا دام اقباله‌العالی گوید

<div class="b" id="bn1"><div class="m1"><p>خیزید و یک دو ساغر صهبا بیاورید</p></div>
<div class="m2"><p>ساغر کمست یک دو سه مینا بیاورید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مینا به کار ناید کشتی کنید پر</p></div>
<div class="m2"><p>کشتی کفاف ندهد دریا بیاورید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوبان شهر را همه یک‌جا کنید جمع</p></div>
<div class="m2"><p>جایی که من نشسته‌ام آنجا بیاورید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را اگر به جام سفالین دهید می</p></div>
<div class="m2"><p>خاکش ز کاسهٔ سر دارا بیاورید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ملک ری به ساحت یغما سپه کشید</p></div>
<div class="m2"><p>هرجا پری رخیست به یغما بیاورید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز روم هر کجا بچه ترسای مهوشست</p></div>
<div class="m2"><p>ور خود بود کشیش کلیسا بیاورید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بزم عیشم از لب و دندان مهوشان</p></div>
<div class="m2"><p>یک آسمان سهیل و ثریا بیاورید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا من به یاد چشم نکویان خورم شراب</p></div>
<div class="m2"><p>یک جویبار نرگس شهلا بیاورید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا من به بوی زلف بتان تر کنم دماغ</p></div>
<div class="m2"><p>یک مرغزار سنبل بویا بیاورید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گیرید گوش زهره و او را کشان کشان</p></div>
<div class="m2"><p>از آسمان به ساحت غبرا بیاورید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تابید زلف حوری و او را دوان دوان</p></div>
<div class="m2"><p>سوی من از بهشت به دنیا بیاورید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا من کنم ثنای خداوند خود رقم</p></div>
<div class="m2"><p>کلک و مداد وکاغذ و انشا بیاورید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اول به جای صفحه ز بال فرشتگان</p></div>
<div class="m2"><p>پری سه چار دلکش و زیبا بیاورید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور از دو ساق غلمان ناید قلم به دست</p></div>
<div class="m2"><p>از ساعدین آن بت ترسا بیاورید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس جای دو ده مردمک دیدگان حور</p></div>
<div class="m2"><p>سایید و هرسه چیز به یکجا بیاورید</p></div></div>
<div class="b2" id="bn16"><p>تا بر پر فرشته ز آن حبر و آن قلم</p>
<p>در مدح اردشیرکنم چامه‌ای رقم</p></div>
<div class="b" id="bn17"><div class="m1"><p>ترکا مگر تو بچهٔ حور جنانیا</p></div>
<div class="m2"><p>کاندر جهان پیری و دایم جوانیا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>معجون جان و جوهر دل کس ندیده بود</p></div>
<div class="m2"><p>اینک تو جوهر دل و معجون جانیا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سوگند می‌خورم که به‌دنیا بهشت نیست</p></div>
<div class="m2"><p>ور هست در زمانه بهشتی تو آنیا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سیم از پی ذخیرهٔ تن می‌نهند خلق</p></div>
<div class="m2"><p>تو سیمتن ذخیرهٔ روح روانیا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شادی دهد به ‌دل رخ خوب تو ای‌ عجب</p></div>
<div class="m2"><p>کز رنگ ارغوان به اثر زعفرانیا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هنگام رقص چونکه به چرخ افتدت سرین‌</p></div>
<div class="m2"><p>پندارمت به روی زمین آسمانیا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دل را به نسیه گرچه دهی وعده‌ها ولیک</p></div>
<div class="m2"><p>جان را به نقد زندگی جاودانیا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هرگه که تشنه گردم خواهم بنوشمت</p></div>
<div class="m2"><p>پندارم از لطافت آب روانیا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سهراب‌وار خنجر عشقت دلم شکافت</p></div>
<div class="m2"><p>ترکا مگر تو رستم زاولستانیا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گویند جان ز فرط لطافت نهان بود</p></div>
<div class="m2"><p>جانی تو در لطافت و اینک عیانیا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>معلوم شد که مردم چشم منی از آنک</p></div>
<div class="m2"><p>در چشم من نشسته و از من نهانیا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بنگر در آب و آینه منگر که ترسمت</p></div>
<div class="m2"><p>عاشق شوی به خویش و درانده بمانیا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>روزی بپرس از دهن تنگ خود که تو</p></div>
<div class="m2"><p>عاشق نگشته‌ای ز چه رو بی‌نشانیا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در عضو عضو پیکر من نقش روی تست</p></div>
<div class="m2"><p>یک تن فزون نیی و به چندین مکانیا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>الله اکبر ای سر زلفین یار من</p></div>
<div class="m2"><p>خود مایه چیست کاینهمه عنبر فشانیا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اول ضعیف و زار نمودی به چشم من</p></div>
<div class="m2"><p>وآخر بدیدمت که عجب پهلوانیا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از تار تار موی تو آید شمیم مشک</p></div>
<div class="m2"><p>گویی که خلق والی مازندرانیا</p></div></div>
<div class="b2" id="bn34"><p>شهزاده‌ای که شاهش فرمانروای کرد</p>
<p>بازش ز مرحمت طبرستان خدای کرد</p></div>
<div class="b" id="bn35"><div class="m1"><p>ای زلف دانم از چه بدینسان خمیده‌ای</p></div>
<div class="m2"><p>عمری به دوش بار دل ما کشیده‌ ای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زینسان که بینمت مه و خورشید در بغل</p></div>
<div class="m2"><p>دارم گمان که چرخی از آنرو خمیده‌ای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شیطان شنیده‌ام که برون شد ز خلد و تو</p></div>
<div class="m2"><p>شیطانی و هنوز به خلد آرمیده‌ای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مانی به زاغ خلد که عمری به باغ خلد</p></div>
<div class="m2"><p>خوش خوش‌ به‌ ‌گرد کوثر و طوبی چریده‌ای</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>رضوان چه کرد با تو و حورا ترا چه گفت</p></div>
<div class="m2"><p>کاشفته‌ای و با پر و بال شمیده‌ای</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>غلمان مگر به شوخی سنگی زدت به بال</p></div>
<div class="m2"><p>کز خلد قهر کرده به دنیا پریده‌ای</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نزدیک گوش یاری و آشفته‌ای مگر</p></div>
<div class="m2"><p>آشفته حالی من از آنجا شنیده‌ای</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چنبر نموده پشت و به زانو نهاده سر</p></div>
<div class="m2"><p>مانند اهل حال به کنجی خزیده‌ای</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نوری از آن به دیدهٔ مردم مکرٌمی</p></div>
<div class="m2"><p>حوری از آن به باغ جنان جا گزیده‌ای</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پس دیو دل چرایی اگر حور طینتی</p></div>
<div class="m2"><p>پس تیره جان چرایی اگر نور دیده‌ای</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دامن ز پیش برزده چون مرد پهلوان</p></div>
<div class="m2"><p>در روی ماه از پی کشتی دویده‌ای</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خال نگار من مگس است و تو عنکبوت</p></div>
<div class="m2"><p>کز بهر صید تار به گردش تنیده‌ای</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>وی خالک سیاه تو هم زان شکنج زلف</p></div>
<div class="m2"><p>بنمای رخ که شبروکی شوخ دیده‌ای</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>متواریک چو دانه نظر می کنی ز دام</p></div>
<div class="m2"><p>در انتظار صید شکار رمیده‌ای</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مانند زاغ بچهٔ نارسته پرّ و بال</p></div>
<div class="m2"><p>تن گرد کرده در دل مادر طپیده‌ای</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دزدیده‌ای دل من و از دیده گشته دور</p></div>
<div class="m2"><p>در زیر پرده پردهٔ مردم دریده‌ای</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دزد دل منی ز چه جان بخشمت به مزد</p></div>
<div class="m2"><p>جز خویش دزد مزدستان هیچ دیده‌ای</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تاریک و روشنست ز تو چشم من ازانک</p></div>
<div class="m2"><p>چون مردمک ز ظلمت و نور آفریده‌ای</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر خود سواد مردم چشم منی چرا</p></div>
<div class="m2"><p>پیوند الفت از نظر من بریده‌ای</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>یا قطرهٔ مرکب خشکی که بر حریر</p></div>
<div class="m2"><p>از نوک کلک والی والا چکیده‌ای</p></div></div>
<div class="b2" id="bn55"><p>فر‌ماندهی که ‌مهرش نرمست وکین درشت</p>
<p>دینار بدره بدره دهد سیم مشت مشت</p></div>
<div class="b" id="bn56"><div class="m1"><p>شد وقت آ‌نکه رو سوی ساری کند همی</p></div>
<div class="m2"><p>فرمان شه به ساری جاری کند همی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>زانسان که سار نغمه سراید به شاخسار</p></div>
<div class="m2"><p>بر شاخسار دولت ساری کند همی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>رو سوی ساری‌آرد و آنگه به قول ترک</p></div>
<div class="m2"><p>رخسار دشمنان را ساری کند همی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ساری کنون ز وجد چو سوریست سرخ روی</p></div>
<div class="m2"><p>بیچاره نام خود ز چه ساری کند همی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ساریست رنگ زرد بترکی و زین لغت</p></div>
<div class="m2"><p>ساری شودگر آگه زاری کند همی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نی باز شادمان شود ار بشنودکه ترک</p></div>
<div class="m2"><p>رخشنده نام یزدان تاری کند همی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>باری سزدکه ساری از وجد این خبر</p></div>
<div class="m2"><p>تا حشر شکر نعمت باری کند همی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>وقتست کاردشیر برآید به پشت رخش</p></div>
<div class="m2"><p>برکه نشسته طی صحاری کند همی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>وقتست کاردشیر به اقبال شهریار</p></div>
<div class="m2"><p>از چرخ و ماه پیل و عماری کند همی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چرخش ز پی علم کشد از خط استوا</p></div>
<div class="m2"><p>مهرش ز پیش غاشیه‌داری کند همی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>یزدان هوای طاعت او را به سان روح</p></div>
<div class="m2"><p>در عضو عضو هستی ساری کند همی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>خورشید رایش از افق دل کند طلوع</p></div>
<div class="m2"><p>صد روز روشن از شب تاری کند همی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>در هرنفس‌ که برکشد از صدق همچو صبح</p></div>
<div class="m2"><p>باری هزار بارش یاری کند همی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>آدم به خلد بیند اگر فرّ و جاه او</p></div>
<div class="m2"><p>فخر از علوّ شأن ذراری کند همی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>هرشب به‌شرط آنکه کند یاد ازین غلام</p></div>
<div class="m2"><p>بالین ز زلف ترک تتاری کند همی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هرگه که دست همت او دُرفشان شود</p></div>
<div class="m2"><p>دامان چرخ پر ز دراری کند همی</p></div></div>
<div class="b2" id="bn72"><p>گوینده را مدیحش ابکم نمایدا</p>
<p>یک تن چگونه مدح دو عالم نمایدا</p></div>
<div class="b" id="bn73"><div class="m1"><p>ای آسمان به طوع و ارادت زمین تو</p></div>
<div class="m2"><p>گنجینهٔ یسار جهان در یمین تو</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گردون در افق نگشاید بر آفتاب</p></div>
<div class="m2"><p>تا هر سحر چو سایه نبوسد زمین تو</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>الحق بجاست گر همه اجزای روزگار</p></div>
<div class="m2"><p>یکسر زبان شود ز پی آفرین تو</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>با صدهزار چشم به چندین هزار قرن</p></div>
<div class="m2"><p>گردون ندیده در همه گیتی قرین تو</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>عکست درآب و آینه مشکل فتدکه نیست</p></div>
<div class="m2"><p>کس در جهان به صورت و معنی قرین تو</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>زآنرو به نحل وحی فرستاد کردگار</p></div>
<div class="m2"><p>کش موم بود قابل نقش نگین تو</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>تا جمله کاینات ببینند نقش خویش</p></div>
<div class="m2"><p>حق ساختست آینه‌ای از جبین تو</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>نزدیک آن رسیده که بینی ضمیر خلق</p></div>
<div class="m2"><p>ای من فدای این نظر دوربین تو</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>نبود عجب که دعوی پیغمبری کند</p></div>
<div class="m2"><p>روزی که بدسگال تو آید به کین تو</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>کانروز خصم سایه ندارد که سایه‌اش</p></div>
<div class="m2"><p>پنهان شود ز هیبت چین جبین تو</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>و اعضای او متابعت او نمی کند</p></div>
<div class="m2"><p>گر دشمنی بود به مثل درکمین تو</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>از دست تست معجز روح‌الله آشکار</p></div>
<div class="m2"><p>دامان مریمست مگر آستین تو</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>اهل هنر به کُنه کمالت کجا رسند</p></div>
<div class="m2"><p>خرمن ‌تراست‌وین‌دگران‌ خوشه‌چین تو</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>قاآنی از برِ تو به جایی نمی‌رود</p></div>
<div class="m2"><p>تو انگبینی او مگس انگبین تو</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>تا آن‌ زمان ‌بمان که ز پی شاهدی به خلد</p></div>
<div class="m2"><p>تنگت به برکشدکه منم حورعین تو</p></div></div>
<div class="b2" id="bn88"><p>محمود باد عاقبت روزگار تو</p>
<p>صد چون ایاز و بهتر ازو میگسار تو</p></div>