---
title: >-
    شمارهٔ ۹ - در بعضی از فتوحات شاهزاده شجاع‌السلطنه گوید
---
# شمارهٔ ۹ - در بعضی از فتوحات شاهزاده شجاع‌السلطنه گوید

<div class="b" id="bn1"><div class="m1"><p>خلق موتی را همین تنها نه احیا ساختند</p></div>
<div class="m2"><p>هر گیاهی را ز شادی خضر گویا ساختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هوای مهرگان هنگامه را کردند گرم</p></div>
<div class="m2"><p>نوشدارویی برای دفع سرما ساختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا شود صادر به هر ملکی مسرت قدسیان</p></div>
<div class="m2"><p>ز آفتاب و آسمان توقیع و طغرا ساختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ترازو از پی سنجیدن وزن نشاط</p></div>
<div class="m2"><p>کفهٔ جان را پر از کیل تمنا ساختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای عجبتر آنکه بی‌تأثیر نفس ناطقه</p></div>
<div class="m2"><p>آنچه در خورد بهار از صنع والا ساختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پی تفریح جان‌ها ساقیان سیم‌ساق</p></div>
<div class="m2"><p>بدر ساغر را پر از خورشید صهبا ساختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا ید بیضای موسای کلیم‌الله را</p></div>
<div class="m2"><p>مشرق اشراق نور طور سینا ساختند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر دفع ساحران غصه و غم گلرخان</p></div>
<div class="m2"><p>از سر زلف سیه ثعبان موسی ساختند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خط و قد و خد و زلف پریرویان شهر</p></div>
<div class="m2"><p>سنبل و سرو و گل و ریحان بویا ساختند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو مریخ از هلال تیغ دژخیمان شاه</p></div>
<div class="m2"><p>خصم جوزن را به میزان شکل جوزا ساختند</p></div></div>
<div class="b2" id="bn11"><p>شرزه شیر بیشهٔ مردی شجاع‌السلطنه</p>
<p>کز هراسش خون خورد ارغنده شیر ارژنه</p></div>
<div class="b" id="bn12"><div class="m1"><p>بوالعجب هنگامه ای خلق جهان آراستند</p></div>
<div class="m2"><p>طرفه جشنی جانفزا پیر و جوان آراستند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر نشد بیت‌الشرف بیت‌الهبوط آفتاب</p></div>
<div class="m2"><p>جشن نوروزی چرا در مهرگان آراستند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا ز تنشان روح نگریزد ز شادی در عروق</p></div>
<div class="m2"><p>رشته‌ها هر یک ز بهر حبس جان آراستند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جان به تنشان تازه شد از تنگ ظرفی لاجرم</p></div>
<div class="m2"><p>جای اول روح را در استخوان آراستند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا حَمَل را باز نشناسد ز جدی آهوی چرخ</p></div>
<div class="m2"><p>جشن نوروزی دو مه پیش از کمان آراستند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر نه افریدون فری بر بیوراسبی‌، چیره شد</p></div>
<div class="m2"><p>مهرگان جشن از چه رو در هر کران آراستند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یا فکند آرش‌ کمانی تیری از آمل به مرو</p></div>
<div class="m2"><p>کز طرف فرخنده جشنی تیرگان آراستند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یا نه امطار مطر شد بعد چندین سال قحط</p></div>
<div class="m2"><p>جشن شایانی به روز مهرگان آراستند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یا مقید ساخت خصم نامقید را ملک</p></div>
<div class="m2"><p>کز فرح جشنی فره در جاودان آراستند</p></div></div>
<div class="b2" id="bn21"><p>ابن همان خصمی که مغلوبش ملک زین پیش کرد</p>
<p>پس خلاصش از پی اظهار عفو خویش کرد</p></div>
<div class="b" id="bn22"><div class="m1"><p>عافیت اکنون چو تیغ شاه عالم‌گیر شد</p></div>
<div class="m2"><p>کان دَدِ پتیارهٔ دیوانه در زنجیر شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تیغ خونریز ملک از کشتن او عار داشت</p></div>
<div class="m2"><p>تا نپنداری که در پاداش او تأخیر شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفته‌بود اختر شناسش تاج ورخواهی شدن</p></div>
<div class="m2"><p>حکم ازین بهتر که تاج تارکش شمشیر شد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خوشهٔ عمرش از آنرو احتراق تیر سوخت</p></div>
<div class="m2"><p>کاو به برج خوشه زاد و کوکب او تیر شد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نوجوان‌تر گشت بخت شه به ‌عالم ای شگفت</p></div>
<div class="m2"><p>کز مدار مدت او چرخ گردان پیر شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دید خم خام شه بر یال خود در خواب خصم</p></div>
<div class="m2"><p>خم خام اکنون به بند آهنین تعبیر شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>قهر شاه آمد چو یزدان دیر گیر و سخت گیر</p></div>
<div class="m2"><p>سخت بگرفتش‌ چه‌غم گر چند روزی دیر شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خصم در دل صورت قهر ملک تصویر کرد</p></div>
<div class="m2"><p>صورتی بی‌جان بسان صورت تصویر شد</p></div></div>
<div class="b2" id="bn30"><p>تا ابد تیغ ملک بر فرق اعدا تندباد</p>
<p>در ثنای تیغ او تیغ زبان‌ها کند باد</p></div>
<div class="b" id="bn31"><div class="m1"><p>ای پس از داور خداگیهان خدای راستین</p></div>
<div class="m2"><p>شاه گردون آستان دارای دریا آستین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قابض ارواح را تیغت بود بئس‌البدل</p></div>
<div class="m2"><p>واهب نصرت سپاهت را بود نعم‌المعین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>لفظ شمشیرت نگارند ار به فرق بدسگال</p></div>
<div class="m2"><p>ارّه بر فرقش نهد دندانهای حرف شین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در رحم گر نام تیغ جانستانت بشنود</p></div>
<div class="m2"><p>از هراس جان به سوی نطفه بر‌گردد جنین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای که اندر نسبت کاخ رفیعت آمدست</p></div>
<div class="m2"><p>پایمال گاو و ماهی پیکر عرش برین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر شتابد از پی اخبار ماضی توسنت</p></div>
<div class="m2"><p>داستان نوح و آدم را نگارد بر سرین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا بنای آستانت بر زمین شد آسمان</p></div>
<div class="m2"><p>در توهّم کز چه ساکن عرش اعظم بر زمین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر مدد از شاهباز همتت یابد ذناب</p></div>
<div class="m2"><p>افکند درکاسهٔ گردون طناطن از طنین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر به دوزخ جاکند لطف گنهکاران زنند</p></div>
<div class="m2"><p>طعن‌ها بر آنکه اندر روضهٔ رضوان مکین</p></div></div>
<div class="b2" id="bn40"><p>باد یارب بدسگالت اندرین دار سپنج</p>
<p>ششدر اندر نرد درد و مات در شطرنج رنج</p></div>
<div class="b" id="bn41"><div class="m1"><p>بخل را تنها به به ذلت معن باذل ساخته</p></div>
<div class="m2"><p>فتنه را عدالت انوشروان عادل ساخته</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا بخوابد فتنه‌ در عهدت‌ به‌خواب نیستی</p></div>
<div class="m2"><p>دایهٔ گردون ز مهر و مه جلاجل ساخته</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>حلقهای نجم را درهم کشیدست آسمان</p></div>
<div class="m2"><p>از برای گردن خصمت سلاسل ساخته</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بس که از رشک ضمیرت گریه کردست آفتاب</p></div>
<div class="m2"><p>اشک چشمش رهگذار چرخ را گل ساخته</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>طعنه بر رایت مگر زد کز مدار آفتاب</p></div>
<div class="m2"><p>سایر سیاره را قهر تو مایل ساخته</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدسگال اکنون به قانون عرب رفعش رواست</p></div>
<div class="m2"><p>کش به فعل بغض تو آفاق فاعل ساخته</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>لطفت از زهر هلاهل نوش نحل آرد ولیک</p></div>
<div class="m2"><p>قهرت از قند مکرر سمّ قاتل ساخته</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>وانگهی چون تیر رانی درکمان‌ گویند خلق</p></div>
<div class="m2"><p>نک عطارد بین به برج قوس منزل ساخته</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چون سپر بر سرکشی هنگام کین گویند بدر</p></div>
<div class="m2"><p>خویش را بر پیکر خورشید حایل ساخته</p></div></div>
<div class="b2" id="bn50"><p>رفعت کاخت اگر می‌دید چرخ چنبری</p>
<p>از ازل در دل نمی‌آورد فکر برتری</p></div>
<div class="b" id="bn51"><div class="m1"><p>چون زری شبدیز راندی زی خراسان ای ملک</p></div>
<div class="m2"><p>گشت ز آهنگت دوتاری دل هراسان ای ملک</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هردو را بر تیره دل اندیشهٔ رزمت‌ گذشت</p></div>
<div class="m2"><p>نز پی گردنکشی ز اندیشهٔ جان ای ملک</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چهرهٔ اقبالشان در ششدر خواری فتاد</p></div>
<div class="m2"><p>زانکه بودندی حریف آب‌دندان‌ ای‌ ملک</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زان سپس هر یک فرستادند زی خوارزم شاه</p></div>
<div class="m2"><p>هدیهای وافر و پیک فراوان ای ملک</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>آن دد ناپاک زاد از هیبتت جان داد از آنک</p></div>
<div class="m2"><p>بود در گوشش هنوز افغان افغان ای ملک</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زان سپس با چار‌گرد از خاوران راندی به‌قهر</p></div>
<div class="m2"><p>زی دز با خزر و مرز زاوه یکران ای ملک</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>قومی از افغان دون یاری ده خصم زبون</p></div>
<div class="m2"><p>بسته با هم از پی کین تو پیمان ای ملک</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>قصه کوته کشتی از آن ناکسان چندانکه گشت</p></div>
<div class="m2"><p>تا دو صد فرسنگ سنگ مرج مرجان ای ملک</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>لاجرم زآن هردو تاری دل یکی را کرد چرخ</p></div>
<div class="m2"><p>چون برهمن بستهٔ‌ زنجیر رُهبان ای ملک</p></div></div>
<div class="b2" id="bn60"><p>بس کن ای قاآنی آخر از ثنای شهریار</p>
<p>از ثنا چون عاجزی برگو دعای شهریار</p></div>
<div class="b" id="bn61"><div class="m1"><p>تا ابد یارب ملک در ملک گیتی شاه باد</p></div>
<div class="m2"><p>بر رعیت شاه و بر هر شاه شاهنشاه باد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تا نگردد چار مادر بر عدویش حامله</p></div>
<div class="m2"><p>شوی نه افلاک را زین پس عنن درباه باد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تا قیامت بر لبش از فرط بخشش حرف لا</p></div>
<div class="m2"><p>نگذرد ور بگذرد با لفظ الاالله باد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گر نیندازد به گردن ماه طوق بندگیش</p></div>
<div class="m2"><p>رنج سرطانی ز سرطانش به باد افراه باد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>خدمتش را گر عطارد بندد از جوزا کمر</p></div>
<div class="m2"><p>خوشه‌چین خرمنش مهر ار نباشد ماه باد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ور به میزان سعادت زهره سنجد طالعش</p></div>
<div class="m2"><p>تا قیامت گاوش اندر خرمن بدخواه باد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گر به خاک آستانش رخ نساید آسمان</p></div>
<div class="m2"><p>تا ابد اندام شیرش طعمهٔ روباه باد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بهر خوانش برّه را مریخ اگر بریان کند</p></div>
<div class="m2"><p>نیش عقرب درمذاقش نوش خاطرخواه‌ باد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گر کمان خویش را پیشش نیارد مشتری</p></div>
<div class="m2"><p>جسم‌حوتش صید قلاب ستم ناگاه باد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ور زحل در چرخ دولایی ز بهر مطبخش</p></div>
<div class="m2"><p>جدی را بریان نسازد دلوش اندر چاه باد</p></div></div>
<div class="b2" id="bn71"><p>تا قیامت شه مکان برتخت عرش آیین کناد</p>
<p>بی‌ریاکردم دعا روح‌الامین آمین کناد</p></div>