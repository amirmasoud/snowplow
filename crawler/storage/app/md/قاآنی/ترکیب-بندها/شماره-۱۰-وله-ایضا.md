---
title: >-
    شمارهٔ ۱۰ - وله ایضاً
---
# شمارهٔ ۱۰ - وله ایضاً

<div class="b" id="bn1"><div class="m1"><p>ای زلف نگار من از بس که پریشانی</p></div>
<div class="m2"><p>سرتا به قدم مانا سامان مرا مانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون زنگیکی عریان زانو به زنخ برده</p></div>
<div class="m2"><p>در تابش مهر اندر بنشسته و عریانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هندو چو سپارد جان در آذرش اندازند</p></div>
<div class="m2"><p>تو به‌آتش‌سوزان‌در چون‌هندوی بیجانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افعی‌زده را مانی از بس که به‌خود پیچی</p></div>
<div class="m2"><p>با آنکه تو خود از شکل ‌چون افعی پیچانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افعی به بهار اندر از خاک برآرد سر</p></div>
<div class="m2"><p>زآن چهر بهار آیین زین روی گرایانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسیار به شب کژدم از لانه برون آید</p></div>
<div class="m2"><p>تو کژدمی و پیوست در روز نمایانی</p></div></div>
<div class="b2" id="bn7"><p>آن چهره بدین خوبی ‌آشوب جهانستی</p>
<p>گویند بهشتی‌هست گر هست همانستی</p></div>
<div class="b" id="bn8"><div class="m1"><p>زی کوی مغان ما راگاهی دو سه می‌باید</p></div>
<div class="m2"><p>وز چنگ مغان ما را جامی دوسه می‌باید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیوانه و ژولیده آشفته و شوریده</p></div>
<div class="m2"><p>مشتاق نکویان را نامی دو سه می‌باید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهاد ریایی را انکار بود از می</p></div>
<div class="m2"><p>بر گردن این خامان خامی دو سه می‌باید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم بد بدخواهان از هرطرفی بازست</p></div>
<div class="m2"><p>بر چهر نگار از نیل لامی دو سه می‌باید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در جان و دل و دیده جاکرده خیال دوست</p></div>
<div class="m2"><p>آن طایر قدسی را با می دو سه می‌باید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از تاک به خم و زخم در شیشه از آن در جام</p></div>
<div class="m2"><p>دوشیزهٔ صهبا را مامی دو سه می‌باید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زلف و خط و‌گیسو را زیب رخ جانان بین</p></div>
<div class="m2"><p>وان صبح همایون را شامی دو سه می‌باید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خواهی شودت ای دل کام دو جهان حاصل</p></div>
<div class="m2"><p>زی بارگه خسروگامی دو سه می‌باید</p></div></div>
<div class="b2" id="bn16"><p>شاهی که بر او ختمست آیات جهانداری</p>
<p>و آمد به صفت رایش مرآت جهانداری</p></div>
<div class="b" id="bn17"><div class="m1"><p>من بندهٔ خاقانم از دهر نیندیشم</p></div>
<div class="m2"><p>تریاق به کف دارم از زهر نیندیشم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر چرخ زند ناچخ ور دهرکشد خنجر</p></div>
<div class="m2"><p>از چرخ نپرهیزم وز دهر نیندیشم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دوشیزهٔ صهبا را من عقد بخواهم بست</p></div>
<div class="m2"><p>مهرش همه گر جانست از مهر نیندیشم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر تیغ کشد خورشید ور قهرکند بهرام</p></div>
<div class="m2"><p>زان تیغ نتابم رو زان قهر نیندیشم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شهری به‌خلاف من گر تبغ کشدچون بید</p></div>
<div class="m2"><p>با حرز ولای آن زان شهر نیندیشم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون نی ز فلک باکم بادیست کرهٔ خاکم</p></div>
<div class="m2"><p>در بحر زنم غوطه از نهر نیندیشم</p></div></div>
<div class="b2" id="bn23"><p>شاهی که ولای او داروی غمانستی</p>
<p>دست گهر انگیزش آشوب عمانستی</p></div>