---
title: >-
    بخش ۷۱ - پاسخ دادن کید هندی،فرامرز را(و) دین یزدان پذیرفتن او
---
# بخش ۷۱ - پاسخ دادن کید هندی،فرامرز را(و) دین یزدان پذیرفتن او

<div class="b" id="bn1"><div class="m1"><p>چنین گفت کید ای جهاندار گرد</p></div>
<div class="m2"><p>سرافراز و دانای با دستبرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر گنج خواهی،بیاریم گنج</p></div>
<div class="m2"><p>بدان تا روان ها ندارم به رنج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشایم در گنج های پدر</p></div>
<div class="m2"><p>وز آن پس ببخشم تو را سر به سر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدین سان کس از دین خود برنگشت</p></div>
<div class="m2"><p>ز راه نیاکان نباید گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا یک خدا هست ما صدخدا</p></div>
<div class="m2"><p>همان پاک وپاکیزه ماند به جا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به زشتی، کس از دین خود نگذرد</p></div>
<div class="m2"><p>وگر بگذرد نیز کیفر برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولی چون تو فرمان دهی انجمن</p></div>
<div class="m2"><p>بیارم به هندوستان برهمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شما نیز زین انجمن مهتری</p></div>
<div class="m2"><p>بیارید داننده دانشوری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگویید گفتار و پاسخ دهید</p></div>
<div class="m2"><p>بدین کار بر رای فرخ نهید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به گفتار هرکس فرو ماند او</p></div>
<div class="m2"><p>دو چشم خرد را نخواباند ائو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر هم تهی شد زایران،هنر</p></div>
<div class="m2"><p>نشایدش گفتن به تیغ و سپر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هزارای دانش زفرهنگ شد</p></div>
<div class="m2"><p>به کوپال و شمشیر و نیرنگ شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرآن کو که آید به دانشوری</p></div>
<div class="m2"><p>خدای همان انجمن شد سری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی خرقه سال خورده به بر</p></div>
<div class="m2"><p>فروبسته زنار ترکی به سر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کشیده ریاضت بسی سال وماه</p></div>
<div class="m2"><p>همه موی،برف وهمه رخ چو کاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو مرغی به یاد قفس برشدی</p></div>
<div class="m2"><p>زدیدار مردم فزون تر شدی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرآن قوم را نام بد جوکیان</p></div>
<div class="m2"><p>خردمند یک سر قوی راکیان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیامد چنان پیر زنار بند</p></div>
<div class="m2"><p>به گفتار دانش یکی کاربند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سؤالی که داری به شکل زره</p></div>
<div class="m2"><p>بگو و زایرانیان بر فره</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فرامرز گفتا به کینه منم</p></div>
<div class="m2"><p>به دانشوری تا به هم برزنم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگو آنچه داری زدانش به یاد</p></div>
<div class="m2"><p>که نفرین بدین دانش وهوش باد</p></div></div>