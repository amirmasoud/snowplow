---
title: >-
    بخش ۴۲ - خواندن نصیحت نامه نوش زاد بن جمشید
---
# بخش ۴۲ - خواندن نصیحت نامه نوش زاد بن جمشید

<div class="b" id="bn1"><div class="m1"><p>یکی لوح زرین به بالین اوی</p></div>
<div class="m2"><p>نوشته یکی نامه کای جنگجوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو من بس فراخست و لشکرپناه</p></div>
<div class="m2"><p>جهان پر زر و سیم در سنگ گاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو باری فروزان میان گوان</p></div>
<div class="m2"><p>که پاکی تن جان شدیم و روان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پدر کرد نام مرا نوش زاد</p></div>
<div class="m2"><p>زجمشید دارم به گیتی نژاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیای تو یک سر پدر بر پدر</p></div>
<div class="m2"><p>پدر ما وزاده تویی ای پسر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو تکیه به عمر جوانی مکن</p></div>
<div class="m2"><p>به گیتی چو ما زندگانی مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به روز جوانی رسیدم به مرگ</p></div>
<div class="m2"><p>نگه کن به بالین من تیغ و ترک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیالوده بر من همین مشک ناب</p></div>
<div class="m2"><p>شده صید مرگ و رسیده به خواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سمن بین که هست از نهفته تهی</p></div>
<div class="m2"><p>شقایق شده چون یکی پر بهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منه دل بر ای روزگار درشت</p></div>
<div class="m2"><p>که او بشکند مهره سند وپشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که گیتی سپنج است و ما در گذر</p></div>
<div class="m2"><p>به غفلت مبر عمر در وی به سر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سهی سرو بینی که زین سان بلند</p></div>
<div class="m2"><p>دو رخساره همچون دل دردمند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسی گشته ام بر لب جویبار</p></div>
<div class="m2"><p>بغلطید چون آب در مرغزار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شکار آهوان و گوزنان و گور</p></div>
<div class="m2"><p>بسی دیده وهم شده بخت شور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به هامون چو رفتی به وقت شکار</p></div>
<div class="m2"><p>زده حلقه در گرد من صدهزار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به هر چند چون شادمان آمدم</p></div>
<div class="m2"><p>سرانجام بینم چه سان آمدم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به روز جوانی فرو خفته زار</p></div>
<div class="m2"><p>تو در پی ز گیتی همین چشم دار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چریدم در این پهن گیتی دویست</p></div>
<div class="m2"><p>چه سازم در این عمر کوته نویست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به ملک جهان دل مدارید شاد</p></div>
<div class="m2"><p>نگه کن ببین پیکر نوش زاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پدرمان که پرمایه جمشید بود</p></div>
<div class="m2"><p>به بالین من خفته چون بید بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیارست منع چنین روز کرد</p></div>
<div class="m2"><p>به ناکام ترک دل افروز کرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدین سان پریدم ز تخت بهی</p></div>
<div class="m2"><p>بکنده دل از جایگاه مهی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شنیدم زگفتار هندوستان</p></div>
<div class="m2"><p>که آیی تو از زابل و سیستان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نهاده ز بهر تو این برده رنج</p></div>
<div class="m2"><p>رهاکن مرو را تو بردار گنج</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نهان کن سردخمه ای نامجو</p></div>
<div class="m2"><p>به ایران رو این استان را بگو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زنهصد فزونست کاین دیو گرگ</p></div>
<div class="m2"><p>زمن پاسبان شد به گنج بزرگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زما باد بر پهلوان آفرین</p></div>
<div class="m2"><p>دلیر و جهانگیر و پاکیزدین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فرامرز چون تخته زر بخواند</p></div>
<div class="m2"><p>سرشکش زدیده به رخ برفشاند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>روان شد زچشم جهانجوی جوی</p></div>
<div class="m2"><p>درآن چهره زر بمالید روی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دل پاکش از چهره او بسوخت</p></div>
<div class="m2"><p>بنالید زار و دلش برفروخت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بفرمود تا گوهر و زر ناب</p></div>
<div class="m2"><p>صراحی که بد پر چه در خوشاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زچیزی که بد یک سره بار کرد</p></div>
<div class="m2"><p>جهان پر زر و رخت و دینار کرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نهفته برون کرد وآن شه بماند</p></div>
<div class="m2"><p>بسی مشک و عنبربر او برفشاند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همان رخت وآن جامه خسروی</p></div>
<div class="m2"><p>گرانمایه کوپال و تیغ گوی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همان تاج زرین و پرمایه تخت</p></div>
<div class="m2"><p>بماندند با شاه زیبا درخت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دگر هرچه بد گنج برداشتند</p></div>
<div class="m2"><p>به روی شرف در بپرداختند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جهانی به بالین گنج آمدند</p></div>
<div class="m2"><p>زبردن سراسر به رنج آمدند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو گیتی تهی شد ز درنده گرگ</p></div>
<div class="m2"><p>به شهر آمدند آن سپاه بزرگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو لشکر ستوه آمد از خواسته</p></div>
<div class="m2"><p>همه شاد زان گنج آراسته</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به می برنشستند چنگ و رباب</p></div>
<div class="m2"><p>گران شد سرمی خوران از شراب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همین بر ببخشید گنج و بره</p></div>
<div class="m2"><p>وزو لشکری شاد شد یک سره</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دو تیغ سمنکش به الماس پاک</p></div>
<div class="m2"><p>که مانند خورشید بد تابناک</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یکی زان به گستهم گودرز داد</p></div>
<div class="m2"><p>دل شیر شد زان چنان تیغ شاد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زراسب گرانمایه را زان یکی</p></div>
<div class="m2"><p>ببخشید تریاق پاک اندکی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به گرگین میلاد گرزم کمر</p></div>
<div class="m2"><p>بفرمود آن گرد پرخاشخر</p></div></div>