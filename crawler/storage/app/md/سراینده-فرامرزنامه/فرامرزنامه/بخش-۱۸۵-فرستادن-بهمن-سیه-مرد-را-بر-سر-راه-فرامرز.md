---
title: >-
    بخش ۱۸۵ - فرستادن بهمن،سیه مرد را بر سر راه فرامرز
---
# بخش ۱۸۵ - فرستادن بهمن،سیه مرد را بر سر راه فرامرز

<div class="b" id="bn1"><div class="m1"><p>چو بر زهره،تیره شب الماس کرد</p></div>
<div class="m2"><p>هوا روز روشن،شب تار کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هردو سپاه اندرآمد کمی</p></div>
<div class="m2"><p>زهم بازگشتند هردو غمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیه مرد را شاه ایران بخواند</p></div>
<div class="m2"><p>ابا او زهر در سخن ها براند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدوگفت امشب برو با سپاه</p></div>
<div class="m2"><p>برایشان زهر سو بگیرید راه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که دانم که امشب مر آن بدنژاد</p></div>
<div class="m2"><p>همی روی بر راه خواهدنهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین رنج کامروز بر وی رسید</p></div>
<div class="m2"><p>نیارد برین بوم و برآرمید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیه مرد با نامور سی هزار</p></div>
<div class="m2"><p>برفت و سر راه کرد استوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرامرز از آن روی با خواهران</p></div>
<div class="m2"><p>چنین گفت کین رنج ما شد گران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمانه به ما دست بد برگشاد</p></div>
<div class="m2"><p>ازین بیش دیگر نباشیم شاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نبینیم یکدیگران را دگر</p></div>
<div class="m2"><p>مگر پیش دادار فیروزگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شما را همان به که بیرون شوید</p></div>
<div class="m2"><p>سر خویش گیرید واکنون روید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه خوش داد فرزانه را پند راز</p></div>
<div class="m2"><p>که امروز بگذشت و آینده باز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پدر کشتی و تخم کین کاشتی</p></div>
<div class="m2"><p>پدر کشته را کی بود آشتی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به دشمن مرا بازدارید دست</p></div>
<div class="m2"><p>نخواهم من از دست این دیو رست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زهرکس برآمد همی خون به جوش</p></div>
<div class="m2"><p>زهر دل برآمد به زاری،خروش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بکندند پس عنبر مشکبوی</p></div>
<div class="m2"><p>به پیشش نهادند بر خاک،روی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غریوان فرامرز و آن سرکشان</p></div>
<div class="m2"><p>همه باد سرد از جگر برکشان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی گفت هر کس که ای بخت ما</p></div>
<div class="m2"><p>به دشمن سپردی سر وتخت ما</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کنون روی از ماه پیکر متاب</p></div>
<div class="m2"><p>چو برداشتی بی کران برشتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فرامرز را گفت با نو گشسب</p></div>
<div class="m2"><p>که هرگز نبیند مرا پشت اسب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو با دشمن اندر نبرد و ستیز</p></div>
<div class="m2"><p>چگونه نماییم رو در گریز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حیاتم زبهر تو باید همی</p></div>
<div class="m2"><p>روانم زمهر تو باشد همی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو را گر یکی آسیب بر تن رسد</p></div>
<div class="m2"><p>روا دارم آن به که برمن رسد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به جان گر هزاران نهیب آیدم</p></div>
<div class="m2"><p>دل از مهر تو ناشکیب آیدم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بمان تا به پیش تو کشته شویم</p></div>
<div class="m2"><p>به خاک و به خون در سرشته شویم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زگفتار او جان ودل خسته شد</p></div>
<div class="m2"><p>زنوک مژه،درد،پیوسته شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فرامرز سوگندشان داد باز</p></div>
<div class="m2"><p>به روز سفید و شب دیرباز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به آذر گشسب و به وستا و زند</p></div>
<div class="m2"><p>به جان و سر پهلوان بلند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که آغاز رفتن نمایید تیز</p></div>
<div class="m2"><p>میارید پاسخ،مرا هیچ چیز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنین گفت مر شاه را رهنمان</p></div>
<div class="m2"><p>تن خویش با دشمنت برگران</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به زور و بزرگی و مردی و هوش</p></div>
<div class="m2"><p>چه با او برابر نباشی،مکوش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من امروز بی لشکر و رنج وساز</p></div>
<div class="m2"><p>چگونه شوم پیش دشمن فراز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همانا مرا بهتر آید بسی</p></div>
<div class="m2"><p>که از تخم رستم بماند کسی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بود کآورد روزدگار دگر</p></div>
<div class="m2"><p>جهان را یکی داد دارد دگر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یکی بچه شیر دل پهلوان</p></div>
<div class="m2"><p>پدید آید از کشور هندوان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو باید که بر دست این دیو زاد</p></div>
<div class="m2"><p>به خیره بدادیم جان ها به باد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ره هندوان پیش باید گرفت</p></div>
<div class="m2"><p>جز آن راه دیگر نشاید گرفت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که آن بوم بر دوستان من اند</p></div>
<div class="m2"><p>به کشور،همه بوستان من اند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شما را همه کس بود یاوری</p></div>
<div class="m2"><p>چو رفتید کوته شود داوری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>من و دشمن وگرز گردنکشان</p></div>
<div class="m2"><p>از این رزم مانم به گیتی نشان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مرا تا یکی آلت کارزار</p></div>
<div class="m2"><p>بماند کجا ترسم از شهریار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کنون گر زمانم سرآید همی</p></div>
<div class="m2"><p>سرانجام گل بسته آید همی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کجا جاودانه به گیتی نماند</p></div>
<div class="m2"><p>وگر ماند ازو داستان کس نراند</p></div></div>