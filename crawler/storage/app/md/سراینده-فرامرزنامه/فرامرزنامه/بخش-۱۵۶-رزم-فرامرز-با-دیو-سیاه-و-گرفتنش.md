---
title: >-
    بخش ۱۵۶ - رزم فرامرز با دیو سیاه و گرفتنش
---
# بخش ۱۵۶ - رزم فرامرز با دیو سیاه و گرفتنش

<div class="b" id="bn1"><div class="m1"><p>درین گفتگو بد که از ناگهان</p></div>
<div class="m2"><p>خروشی برآمد که ای پهلوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو آنی که گویی به مردی منم</p></div>
<div class="m2"><p>که کوه گران را زبن برکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم شیردل نامدار از مهان</p></div>
<div class="m2"><p>به مردی همی پویم اندر جهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خنجر،کلان کوه را بستدم</p></div>
<div class="m2"><p>سپاهی برآن گونه برهم زدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر مرد رزمی تو ای پهلوان</p></div>
<div class="m2"><p>یکی پای داری چو کندآوران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببینی کنون زخم شمشیر تیز</p></div>
<div class="m2"><p>تو را گویم از گردش دار وگیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مردی کنون پای بر جای بردار</p></div>
<div class="m2"><p>که بر تو کنم روز رخشنده تار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منم شیر جنگی سیه دیو فام</p></div>
<div class="m2"><p>بیابم هم اکنون ز کار تو کام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپهبد چو بشنید برجست زود</p></div>
<div class="m2"><p>بپوشید جوشن به کردار دود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر افکند بر گستوان بر سمند</p></div>
<div class="m2"><p>به فتراک بربست پیچان کمند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرو برد گرز گران را به زین</p></div>
<div class="m2"><p>به بالا بر آمد چون کوهی به کین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خروشید کای دیو ناسازگار</p></div>
<div class="m2"><p>همه آوردت آمد برآرای کار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به خنجر من این دشت،دریا کنم</p></div>
<div class="m2"><p>زبالای تو کوه،پهنا کنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنانت بپیچم من ای بدنژاد</p></div>
<div class="m2"><p>که ناری از آن پس،کلان کوه یاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خروشید دیو اندر آن تیره شب</p></div>
<div class="m2"><p>زتندی به گفتار،نابسته لب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به زیر اندرش بادپایی چو ابر</p></div>
<div class="m2"><p>بیامد به نزدیک غران هژبر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی سهمگین کوه بد بدگمان</p></div>
<div class="m2"><p>تو گفتی زمین بود زیرش نوان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زپولاد،ترگ و ز آهن،قبای</p></div>
<div class="m2"><p>بپوشیده اندام،سرتا به پای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چوآمد به نزدیک گرد دلیر</p></div>
<div class="m2"><p>دو گرد دلاور چو غرنده شیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در آن تیره شب در هم آمیختند</p></div>
<div class="m2"><p>تو گفتی به همشان در آمیختند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به زخم تبرزین برآشوفتند</p></div>
<div class="m2"><p>سران را به خنجر همی کوفتند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو چندی بکوشید گرد دلیر</p></div>
<div class="m2"><p>سوی چپ بتازید چو نره شیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کشید از میان،تیغ والاگهر</p></div>
<div class="m2"><p>فروکوفت بر تارک دیو نر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شب تیره و تیغ زهرآبدار</p></div>
<div class="m2"><p>چنان بود بر ترک جنگی سوار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو گفتی که خورشید برآبنوس</p></div>
<div class="m2"><p>فشاند همی خورده سندروس</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدان گونه تا مهر،زرین چراغ</p></div>
<div class="m2"><p>برافروخت بر روی هامون وراغ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همی حمله کردند بر یکدگر</p></div>
<div class="m2"><p>یکی را زبد سر نپیچید وبر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سرانجام،گرد دلاور زجای</p></div>
<div class="m2"><p>برآمد یکی تند بفشرد پای</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>درآمد سوی راستش همچو گرد</p></div>
<div class="m2"><p>سنان وار نیزه براو راست کرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بزد بر کمربند آن بد گهر</p></div>
<div class="m2"><p>تو گفتی بدرید کوه وکمر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برآوردش از جا چو کوه سیاه</p></div>
<div class="m2"><p>بیفکند خوارش بدان رزمگاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دو دست از پس پشت بربست سخت</p></div>
<div class="m2"><p>بدو گفت کای دیو برگشته بخت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کجات آن همه مردی وکام ولاف</p></div>
<div class="m2"><p>بدادی سر خویشتن از گزاف</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نبرم سرت زان که ننگ آیدم</p></div>
<div class="m2"><p>اگر چون تو سیصد به چنگ آیدم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دو گوشش به خنجر همانگه بسفت</p></div>
<div class="m2"><p>دو نعل گران اندرو کرد وگفت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که اکنون برلشکرم رهنمای</p></div>
<div class="m2"><p>چو خواهی که ماند سرت را به جای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سیه دیو گفت ای گو بی همال</p></div>
<div class="m2"><p>خداوند کوپال و اورنگ ویال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به مردی،کسی دست برمن نیافت</p></div>
<div class="m2"><p>همان شیر نر،گرد اسبم نیافت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بسی نامور کاردیده سوار</p></div>
<div class="m2"><p>که ازمن رمیدند در کارزار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کسی تاب شمشیر و گرزم نداشت</p></div>
<div class="m2"><p>سپهر از نبردم همی سر بگاشت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گمانم چنین بد که اندر زمین</p></div>
<div class="m2"><p>نباشد کسی کو به هنگام کین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>براسب من افشاند از باد گرد</p></div>
<div class="m2"><p>وگر باد گردد به روز نبرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زبس مردی و نیروی و یال تو</p></div>
<div class="m2"><p>همان بنده فر و گردی تو</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به هر جا که گویی تو را ره برم</p></div>
<div class="m2"><p>زمانی ز رای تو برنگذرم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ولیکن مرا اندر این کوهسار</p></div>
<div class="m2"><p>بسی گنج وتاج است و هم گوشوار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>غلام و پرستار و هم زیر دست</p></div>
<div class="m2"><p>همان اسب و اسباب و فرش نشست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که گرد آوریدم به روز دراز</p></div>
<div class="m2"><p>کشیدم بدین کوه خارا فراز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بمان تا بیارم همه پیش تو</p></div>
<div class="m2"><p>که پردرد بادا بداندیش تو</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سپهبد چو زین گونه گفتار دید</p></div>
<div class="m2"><p>دل دیو بدخو پرستار دید</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پراندیشه شد گفت تو جادویی</p></div>
<div class="m2"><p>به مکر وفریب و بد وبدخویی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بدین گفت شیرین و چربی زبان</p></div>
<div class="m2"><p>زمن برد خواهی سرت رایگان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سیه دیو برجست و سوگند خورد</p></div>
<div class="m2"><p>به روز سفید و شب لاجورد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به مردی و گردی و تخت وکلاه</p></div>
<div class="m2"><p>به شمشیر و گرز و به خورشید وماه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>که من از ره داد گویم سخن</p></div>
<div class="m2"><p>از این گفت با من تو دل بد مکن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>فرامرز دانست کو راست گفت</p></div>
<div class="m2"><p>که سوگند با مردمی بود جفت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ودیگر که از خویشتن داد داد</p></div>
<div class="m2"><p>ازو بد نشانی گرفتن به یاد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>اگر دیو بینی اگر آدمی</p></div>
<div class="m2"><p>چو در وی بود مایه مردمی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سخن هاش بپذیر و دل برشکن</p></div>
<div class="m2"><p>به ویژه که از داد راند سخن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>جوان سرافراز فرمانروا</p></div>
<div class="m2"><p>ازو بند بگشاد و کردش رها</p></div></div>