---
title: >-
    بخش ۶۲ - سؤال کردن فرامرز(و) پاسخ دادن برهمن
---
# بخش ۶۲ - سؤال کردن فرامرز(و) پاسخ دادن برهمن

<div class="b" id="bn1"><div class="m1"><p>بپرسید زان پس دل آشفته گرد</p></div>
<div class="m2"><p>کزین دار شش در چه خواهیم برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درو گنج و گوهر فراز آوریم</p></div>
<div class="m2"><p>تهیدست زین پس چرا بگذریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از این رنج و این گنج، انجام چیست</p></div>
<div class="m2"><p>وزین پادشاهی،همه کام چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پاسخ بدو گفت کز شش دری</p></div>
<div class="m2"><p>برد زین همه نام نیک اختری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در این پادشاهی وآباد گنج</p></div>
<div class="m2"><p>که داری نداری بجز درد و رنج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به منزل چو شد رسته کوس و رحیل</p></div>
<div class="m2"><p>نه تیغت به کارست و نه ژنده پیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الف چون شمرده شد این جان پاک</p></div>
<div class="m2"><p>بپرده همه تن فتد در مغاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نماند بجز نام و نیکی و دروی</p></div>
<div class="m2"><p>بنالید زان نامور جنگجوی</p></div></div>