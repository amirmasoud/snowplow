---
title: >-
    بخش ۱۵۸ - نامه نوشتن فرامرز به فرطورتوش
---
# بخش ۱۵۸ - نامه نوشتن فرامرز به فرطورتوش

<div class="b" id="bn1"><div class="m1"><p>بفرمود تا شد نویسنده پیش</p></div>
<div class="m2"><p>نشاندش بر تخت،نزدیک خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی نامه فرمود با رای وهوش</p></div>
<div class="m2"><p>پراز مهر،نزدیک فرطورتوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چوماهی برآمد زدریای مشک</p></div>
<div class="m2"><p>گذر کرد بر روی کافور خشک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببست از بر کاغذ،ابری چو قار</p></div>
<div class="m2"><p>ببارید ازو گوهر شاهوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخستین که بنهاد سر برزمین</p></div>
<div class="m2"><p>گرفت آفرین بر جهان آفرین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خداوند پیدا و راز نهفت</p></div>
<div class="m2"><p>خداوند بی یار و انباز و جفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به شش روز،نه چرخ بر پای کرد</p></div>
<div class="m2"><p>جهان را بدو اندرو جای کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زبرهان او ذره مهر وماه</p></div>
<div class="m2"><p>به ایشان شماره دهد سال وماه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زدیو و دد وآدمی وپری</p></div>
<div class="m2"><p>زخورشید تا ذره ای بنگری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه بندگانند جویندگان</p></div>
<div class="m2"><p>درین بندگی نیز پویندگان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ورا در جهان پادشاهی رواست</p></div>
<div class="m2"><p>که او آفریننده پادشاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وزو بر شهنشاه فرطورتوش</p></div>
<div class="m2"><p>جهاندار و بینا دل و پاک هوش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سزاوار باشد بدو آفرین</p></div>
<div class="m2"><p>که هم با نژاد است و هم پاک دین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هم از خسروان جهان برتر است</p></div>
<div class="m2"><p>سرافراز و بر مهتران مهتر است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خداوند گنجست و تاج وکمر</p></div>
<div class="m2"><p>خداوند با زیب و با مهر و فر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زما نیز بروی فراوان درود</p></div>
<div class="m2"><p>درودی که مهرش بود تار و پود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رسیدم به خوبی بدین بوم وبر</p></div>
<div class="m2"><p>سر مرز آن خسرو نامور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ابا لشکر و بوق و کوس وسپاه</p></div>
<div class="m2"><p>ابا نامداران زرین کلاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چواز گردش چرخ ناسازگار</p></div>
<div class="m2"><p>مرا بهره این بد در این روزگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که حیران شوم چند گه درجهان</p></div>
<div class="m2"><p>ببینم بسی آشکار و نهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به نزدیک مرز شما آمدیم</p></div>
<div class="m2"><p>بدین بوم و بر چند گه دم زدیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زگفتار و کردار و فرهنگ تو</p></div>
<div class="m2"><p>زمردی و دیدار و نیرنگ تو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شنیدیم هرگونه از هرکسی</p></div>
<div class="m2"><p>که دارید بهره ز دانش بسی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چنین آرزو خاست در جان ما</p></div>
<div class="m2"><p>برینست جاوید پیمان ما</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که با تو مرا آشنایی بود</p></div>
<div class="m2"><p>زدیدار تو روشنایی بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>منم پهلوان زاده بافرین</p></div>
<div class="m2"><p>سرافراز گردی ز ایران زمین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جهانگیر پور جهان پهلوان</p></div>
<div class="m2"><p>سپهبد سرافراز روشن روان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو دستان نیا و چو رستم پدر</p></div>
<div class="m2"><p>جهاندار و گردنکش و نامور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نبیره سرافراز سام سوار</p></div>
<div class="m2"><p>که از جنگ شیران ربودی شکار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدین گونه از زخم شمشیر تیز</p></div>
<div class="m2"><p>بماندست ازو نام تا رستخیز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کنون زو منم در جهان یادگار</p></div>
<div class="m2"><p>به هرکار،شایسته کارزار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شنیدم که اندر شبستان تو</p></div>
<div class="m2"><p>پس پرده خوب رویان تو</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی ماه روییست نام آرزوی</p></div>
<div class="m2"><p>مرا خاست در دل بسی آرزوی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که با تو یکی نیز خویشی کنم</p></div>
<div class="m2"><p>به خوبی بسی رای بیشی کنم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به من ده به آئین،تو آن ماه را</p></div>
<div class="m2"><p>نگار سمن بوی دلخواه را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدین گفتگو اندرون جنگ نیست</p></div>
<div class="m2"><p>تو را هم به پیوند ما ننگ نیست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چه گفت آن خردمند پاکیزه مغز</p></div>
<div class="m2"><p>چو بگشاد لب را به گفتار نغز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که گیتی به پیوند،خرم بود</p></div>
<div class="m2"><p>به بیگانگی،دل پر از غم بود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نبینی که بر شاخ های درخت</p></div>
<div class="m2"><p>به پیوند بنهد یکی نیک بخت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو سر برکشد شاخ آید به بر</p></div>
<div class="m2"><p>دهد میوه را بخش تو خوب تر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همیدون به آب روان درنگر</p></div>
<div class="m2"><p>که چون یافت پیوند آب دگر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یکی چشمه ای همچو دریا شود</p></div>
<div class="m2"><p>جهانش درازی و پهنا شود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو گشت از نوشتن،سخن اسپری</p></div>
<div class="m2"><p>برآراست دیو سیه رهبری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فرستاد با او دلاور سوار</p></div>
<div class="m2"><p>زگردان شمشیر زن،یک هزار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ابا هدیه و اسپ آراسته</p></div>
<div class="m2"><p>زدینار و ا زگنج و از خواسته</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>روان گشت دیو از بر راه دور</p></div>
<div class="m2"><p>بتازید در راه یک ماهه بور</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بیامد سیه دیو با تاب وتوش</p></div>
<div class="m2"><p>به نزدیکی شاه فرطورتوش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>یکی را فرستاد از پیشتر</p></div>
<div class="m2"><p>که گوید بدان شاه با مهر وفر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>که آمد فرستاده نامدار</p></div>
<div class="m2"><p>برشاه فرطور پاکیزه وار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو بشنید آن خسرو سرفراز</p></div>
<div class="m2"><p>پذیره فرستاد پیشش فراز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گوی نامور بود وارود نام</p></div>
<div class="m2"><p>به نزدیک شه یافته رای وکام</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>روان گشت و آمد پذیره به راه</p></div>
<div class="m2"><p>خود و نامداران با دستگاه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چوآمد به نزد سیه دیو گرد</p></div>
<div class="m2"><p>سوار سرافراز با دستبرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>فرود آمد از اسب و پرسید دیر</p></div>
<div class="m2"><p>سواری برافکند از آن پس چو شیر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>که تا شاه را زان دهد آگهی</p></div>
<div class="m2"><p>از این شیروش مرد بافرهی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سیه دیو شیراوژن و سرکشست</p></div>
<div class="m2"><p>تو گویی که بر زین،که آتش است</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو بشنید خسرو برآراست کار</p></div>
<div class="m2"><p>به نزدیک آن انجمن شهسوار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بیامد سیه دیو مانند کوه</p></div>
<div class="m2"><p>زگردان ایران ابا او گروه</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چوآمد در ایوان آن سرفراز</p></div>
<div class="m2"><p>درودش رسانید و بردش نماز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>یکی هدیه و اسپ آراسته</p></div>
<div class="m2"><p>ابا گنج و دینار و هم خواسته</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بیاورد بسپرد و نامه بداد</p></div>
<div class="m2"><p>بسی از جهان آفرین کرد یاد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ستایش نمود از فرامرز گرد</p></div>
<div class="m2"><p>زمردی و فرهنگ واز دستبرد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>از آن کارهایی که او کرده بود</p></div>
<div class="m2"><p>به هندوستان و به چین رفته بود</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به نزدیک شاه پری باز گفت</p></div>
<div class="m2"><p>شهنشا پذیرفت واندر شکفت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>از آن پس بفرمود فرطورتوش</p></div>
<div class="m2"><p>وزآن پیشکاران با رای وهوش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>یکی کاخ پرمایه پرداختند</p></div>
<div class="m2"><p>زبهر سیه دیو یل ساختند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>زگردان چو پرداخت آن بارگاه</p></div>
<div class="m2"><p>بیامد نشست از بر تخت شاه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بیاورد آن نامه دل پسند</p></div>
<div class="m2"><p>وزو مهر برداشت بگشاد بند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چو برخواند نامه دبیر جوان</p></div>
<div class="m2"><p>شگفتی نمودند پیرو جوان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>از آن جستن مهر وپیوند او</p></div>
<div class="m2"><p>پراندیشه شد خسرو نامجو</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>زیک ره به پیوند او شاد گشت</p></div>
<div class="m2"><p>ز روی دگر پر زفریاد گشت</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>که با آدمی نبود از بن وفا</p></div>
<div class="m2"><p>گرفتار خشمند و کین و جفا</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به آغاز اگر چند نیکی کنند</p></div>
<div class="m2"><p>سرانجام،عهد و وفا بشکنند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نباشند پیوسته بر یک نهاد</p></div>
<div class="m2"><p>نجنبد زهر جا به هر خیره باد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به جای نکویی،بدی آورند</p></div>
<div class="m2"><p>به هر راستی در کژی آورند</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>وگر سر بپیچم ازین گفتگوی</p></div>
<div class="m2"><p>نخواهم که دختر شود جفت اوی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>پر ازدرد گردد دل پهلوان</p></div>
<div class="m2"><p>زساغر گراید به تیرو کمان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>به ناکام با او بباید زدن</p></div>
<div class="m2"><p>از آن پس ندانم چه شاید بدن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>یکی کینه پیدا شود در نهان</p></div>
<div class="m2"><p>بسی برسر آید سر اندر زمان</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ازآن پس که داند بجزکردگار</p></div>
<div class="m2"><p>که فیروز برگردد از کارزار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چنین گفت داننده پیش بین</p></div>
<div class="m2"><p>که هرکس که پیدا کند تخم کین</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>زکردار خود زود پیچان شود</p></div>
<div class="m2"><p>سر پادشاهیش ویران شود</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>به گیتی درش رنج وسختی بود</p></div>
<div class="m2"><p>به محشر همان شوربختی بود</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>پس آن به که با داد،داد آوریم</p></div>
<div class="m2"><p>به پاسخ همه مهر،یاد آوریم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>که گیتی فسونست و پر درد و رنج</p></div>
<div class="m2"><p>به یکسان نماند سرای سپنج</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>همه مهتری باد فرمان ما</p></div>
<div class="m2"><p>نکو کاری و رای و پیمان ما</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چو زاندیشه بیکران شد ستوه</p></div>
<div class="m2"><p>برآورد سرکرد زی رخ ستوه</p></div></div>