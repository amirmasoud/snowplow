---
title: >-
    بخش ۳۰ - عقد بستن بانو گشسب به جهت گیو (و) بردن بانو به خانه (و) ناقبول کردن بانو گشسب، گیو را واحوال آن
---
# بخش ۳۰ - عقد بستن بانو گشسب به جهت گیو (و) بردن بانو به خانه (و) ناقبول کردن بانو گشسب، گیو را واحوال آن

<div class="b" id="bn1"><div class="m1"><p>ببستند مه را به مریخ عقد</p></div>
<div class="m2"><p>به مفلس بدادند آن گنج نقد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بودش به زور و هنر دستبرد</p></div>
<div class="m2"><p>زمیدان جهان گوی خوبی ببرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببردند مه را به خلوت سرای</p></div>
<div class="m2"><p>چو شد بسته کابین آن دلگشای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو در خلوت خاص شد گیو گرد</p></div>
<div class="m2"><p>بیامد بر ماه با دستبرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همین خواست مانند گستاخ وار</p></div>
<div class="m2"><p>درآرد مر آن ماه را در کنار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زتندی برآشفت بانوی گرد</p></div>
<div class="m2"><p>نمود آن جهانجوی را دستبرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزد بر بناگوش او مشت سخت</p></div>
<div class="m2"><p>بدان سان که افتاد از روی تخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو دست و دو پایش به خم کمند</p></div>
<div class="m2"><p>ببست و به یک کنجش اندرفکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خود غره بودن هم از جاهلیست</p></div>
<div class="m2"><p>که بهتر مطاعی هم از عاقلیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خدایی که بالا و پست آفرید</p></div>
<div class="m2"><p>زبردست هر زیر دست آفرید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به گستاخی خویش دلخسته شد</p></div>
<div class="m2"><p>زدلخستگی تنگ بربسته شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به هرکاری چون بنگری در نهان</p></div>
<div class="m2"><p>همانا کسی از تو به درجهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که بود اندر این بوستانش نوا</p></div>
<div class="m2"><p>که به زو نبد بلبل خوش نوا</p></div></div>