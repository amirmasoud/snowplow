---
title: >-
    بخش ۹۸ - رزم فرامرز با ده پهلوان و کشتن ایشان را
---
# بخش ۹۸ - رزم فرامرز با ده پهلوان و کشتن ایشان را

<div class="b" id="bn1"><div class="m1"><p>سپهبد برآمد به کردار شیر</p></div>
<div class="m2"><p>بغرید چون اژدهای دلیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دست اندرون گرزه سرگرای</p></div>
<div class="m2"><p>به زیر اندرون باره بادپای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو تنگ اندر آمد بدان هندیان</p></div>
<div class="m2"><p>دمنده به کردار شیرژیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان ده مبارز،یکی حمله برد</p></div>
<div class="m2"><p>به اسب نبردی یکی پی فشرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی را کمربند بگرفت و پشت</p></div>
<div class="m2"><p>برآورد بر دیگری زد بکشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی را کمربند بگرفت و سر</p></div>
<div class="m2"><p>بپیچید و زد بر سرآن دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دم اسب دیگر سواری گرفت</p></div>
<div class="m2"><p>بگرداند بر گرد سراز شگفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به سان فلاخن بینداخت تیز</p></div>
<div class="m2"><p>ابر دیگری آن یل پرستیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به یک زخم دو کشته شد زان چهار</p></div>
<div class="m2"><p>دو دیگر بیامد بر نامدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به شبگیر تا گشت خورشید راست</p></div>
<div class="m2"><p>گهی تاختند از چپ و گه ز راست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان ده دلاور که با ده هزار</p></div>
<div class="m2"><p>برابر بدندی گه کارزار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه از پی مردی و نام و ننگ</p></div>
<div class="m2"><p>یکایک بدادند سر را به جنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زپیمان گری شاه بی هوش وداد</p></div>
<div class="m2"><p>چنان نامداران ابر باد داد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل رای،پرکین بدی از فراز</p></div>
<div class="m2"><p>نگهدار پیمان شد از پیش باز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرامرز گفتا به رای گزین</p></div>
<div class="m2"><p>ابا شاه با دانش و بافرین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به پیمان کنون باج و بپذیر و ساو</p></div>
<div class="m2"><p>چو بگریخت از شیر درنده گاو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو بشنید از او شاه هندی سخن</p></div>
<div class="m2"><p>غمی گشت از آن پهلو پیلتن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سگالید تدبیر تا چون کند</p></div>
<div class="m2"><p>کز ایرانیان دشت پرخون کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به کار اندر آورد پند وفریب</p></div>
<div class="m2"><p>دلش رفت بر سوی مکر ونهیب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به شیرین زبان گفت با پهلوان</p></div>
<div class="m2"><p>که ای شیر دل گرد روشن روان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فرود آی و بنشین و رامش پذیر</p></div>
<div class="m2"><p>بدین فرخی باده و جام گیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگروم به پیمان و وعده درست</p></div>
<div class="m2"><p>بدان ره روم من که فرمان توست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو روشن شود کشور از آفتاب</p></div>
<div class="m2"><p>سرنامداران درآید به خواب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شود تازه دل ها به دیدار تو</p></div>
<div class="m2"><p>بسازیم برآرزو کار تو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به گفتار،شیرین،به دل پر دروغ</p></div>
<div class="m2"><p>درونش بدان نامور بی فروغ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فرودآمد از بارگی شیردل</p></div>
<div class="m2"><p>به دست اندرون،خنجر جان گسل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ندانست کز مکر،روباه پیر</p></div>
<div class="m2"><p>همی دام سازد ابر نره شیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه گفت آن خردمند بیدار دل</p></div>
<div class="m2"><p>چو دشمنت گشتست آزرده دل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>منه دل به گفتار شیرین او</p></div>
<div class="m2"><p>کزآن خرمی،تلخی آرد به روی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نشستند با رای هندی به هم</p></div>
<div class="m2"><p>فرامرز و نام آوران،بیش و کم</p></div></div>