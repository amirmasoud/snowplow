---
title: >-
    بخش ۸ - ملاقات کردن فرامرز و بانوگشسب
---
# بخش ۸ - ملاقات کردن فرامرز و بانوگشسب

<div class="b" id="bn1"><div class="m1"><p>چو دریا دل بانو آمد به جوش</p></div>
<div class="m2"><p>فرامرز چون شیر برزد خروش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به آواز گفتش که ای بدنژاد</p></div>
<div class="m2"><p>که باشی چنین گفته آری به یاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان برکشم من زبان از دهن</p></div>
<div class="m2"><p>که دیگر نگویی بدین سان سخن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خنجر جدا سازم از تن سرت</p></div>
<div class="m2"><p>بکوبم به گرزگران پیکرت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدوزم به تیرت چو سوزن حریر</p></div>
<div class="m2"><p>که تا پر بر آری تو از پر تیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نوک سنان چشمت آرم برون</p></div>
<div class="m2"><p>بریزم هم اکنون در این دشت خون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چگونه مرا برد خواهی بگوی</p></div>
<div class="m2"><p>که از خون تو گل کنم خاک روی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>توانگر تو از زخم خنجر شوی</p></div>
<div class="m2"><p>بدین خنجر من تو بی سر شوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همانا ندانی که من کیستم</p></div>
<div class="m2"><p>در این سرزمین از پی چیستم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر نام من بشنود گوش تو</p></div>
<div class="m2"><p>هم اکنون برآید زتن هوش تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منم بار آن تازه فرخ درخت</p></div>
<div class="m2"><p>کزو تازه گردد سر تاج و تخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر سرفرازان گو پیلتن</p></div>
<div class="m2"><p>شهنشه نشان سرور انجمن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دلیر و هژبر افکن و سرفراز</p></div>
<div class="m2"><p>سوار صف آرای دشمن گداز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرامرز گردنکش نامدار</p></div>
<div class="m2"><p>پدر رستم زال سام سوار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرا خواهر است این گو کامیاب</p></div>
<div class="m2"><p>که بانوگشسبش همی خواند باب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به شمشیر شیران شکار از ویند</p></div>
<div class="m2"><p>به نیزه دلیران شکار از ویند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همانا اجل با تو آورده زور</p></div>
<div class="m2"><p>که از پای خود آمدستی به گور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگفت این و بر کوه پیکر نشست</p></div>
<div class="m2"><p>چو بر کوهه پیل نر شیر مست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنین گفت رستم که ای جاهلان</p></div>
<div class="m2"><p>به خود غره از بخت بی حاصلان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همانا که تا هست گردون سپهر</p></div>
<div class="m2"><p>به من مهر از این گونه ننمود چهر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برمتان کنون پیش افراسیاب</p></div>
<div class="m2"><p>برشه بیفزایدم جاه و آب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا سرفرازی دهد در جهان</p></div>
<div class="m2"><p>به پیش کهان و به نزد مهان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کنون گر بخواهید جان در بدن</p></div>
<div class="m2"><p>خود آیید آسان به نزدیک من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که تا من ببندم شما را دو دست</p></div>
<div class="m2"><p>برم پیش آن شاه یزدان پرست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو بشنید بانو بخندید سخت</p></div>
<div class="m2"><p>بدو گفت کای ترک بر گشته بخت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنین تا به کی ژاژ خواهی کنی</p></div>
<div class="m2"><p>بر ره همی خودنمایی کنی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ترا بخت بر گشته زین آمدن</p></div>
<div class="m2"><p>ره بازگشتن نخواهی شدن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>من آن رستم زال را دخترم</p></div>
<div class="m2"><p>فروزنده در برج چون اخترم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو از گوهر او بود گوهرم</p></div>
<div class="m2"><p>به هر سروری در جهان سرورم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گرفتم که هستی چو دیو سفید</p></div>
<div class="m2"><p>زنم بر زمینت چو یک شاخ بید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مشو غره بر زور و بازوی خویش</p></div>
<div class="m2"><p>بر این برز و بازوی و هم خوی خویش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگر کوه باشی چو کاهت کنم</p></div>
<div class="m2"><p>به یک گرز چون خاک راهت کنم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر کوه باشی و گر اژدها</p></div>
<div class="m2"><p>که از تیغ تیزم نیابی رها</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مگر آن که گفتار من بشنوی</p></div>
<div class="m2"><p>از این کوه پیکر پیاده شوی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دهی بوسه نعل سمند مرا</p></div>
<div class="m2"><p>ز خود دور داری گزند مرا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بغرید باز آن سوار دلیر</p></div>
<div class="m2"><p>برآشفت مانند چون نره شیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدو گفت کای هرزه و یاوه گوی</p></div>
<div class="m2"><p>چه گویی سخن های سردم به روی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ندانی که چون من کنم رای جنگ</p></div>
<div class="m2"><p>ز بیمم گریزد به دریا نهنگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز نیروی بازوی خاراشکاف</p></div>
<div class="m2"><p>شکاف افکنم در دل کوه قاف</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به سرپنجه آهنی روز جنگ</p></div>
<div class="m2"><p>بدرم دل شیر و چرم پلنگ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو با من همیدون شوی در نبرد</p></div>
<div class="m2"><p>ببینی تو پیکار مردان مرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بگفتم نیارم به جانتان گزند</p></div>
<div class="m2"><p>نگردید از تیغ من دردمند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بدو گفت بانو که ای دیو دون</p></div>
<div class="m2"><p>به چنگال و بازوی گردی زبون</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بیا تا بگردیم جنگ آوریم</p></div>
<div class="m2"><p>در این دشت تا کی درنگ آوریم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چنان نیزه برزد کمربند او</p></div>
<div class="m2"><p>که لرزید از آن بند پیوند او</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ازو در دل رستم آمد نهیب</p></div>
<div class="m2"><p>زبیمش بشد هر دو پا از رکیب</p></div></div>