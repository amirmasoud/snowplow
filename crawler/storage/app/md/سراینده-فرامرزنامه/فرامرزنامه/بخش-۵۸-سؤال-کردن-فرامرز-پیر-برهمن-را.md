---
title: >-
    بخش ۵۸ - سؤال کردن فرامرز،پیر برهمن را
---
# بخش ۵۸ - سؤال کردن فرامرز،پیر برهمن را

<div class="b" id="bn1"><div class="m1"><p>دگر گفت کای پیر با آفرین</p></div>
<div class="m2"><p>به همت جهان کرده زیر نگین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمیدان و وادی و بستان و کاخ</p></div>
<div class="m2"><p>چه باشد کزین پیش باشد فراخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بزرگ و فراخی گره هیچ چیز</p></div>
<div class="m2"><p>نباشد به گیتی چه دانیم نیز</p></div></div>