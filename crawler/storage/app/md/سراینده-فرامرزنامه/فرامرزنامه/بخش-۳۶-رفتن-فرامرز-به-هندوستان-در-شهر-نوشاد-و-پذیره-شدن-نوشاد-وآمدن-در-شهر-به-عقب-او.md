---
title: >-
    بخش ۳۶ - رفتن فرامرز به هندوستان در شهر نوشاد و پذیره شدن نوشاد وآمدن در شهر به عقب او
---
# بخش ۳۶ - رفتن فرامرز به هندوستان در شهر نوشاد و پذیره شدن نوشاد وآمدن در شهر به عقب او

<div class="b" id="bn1"><div class="m1"><p>چو رستم سخن ها سراسر براند</p></div>
<div class="m2"><p>زمژگان بسی خون دل برفشاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفتند مریکدیگر را به بر</p></div>
<div class="m2"><p>بسی بوسه دادند بر چشم و سر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وز آن جا فرامرز یل شد روان</p></div>
<div class="m2"><p>بیامد خرامان سوی هندوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نوشاد آنگه خبرشد از وی</p></div>
<div class="m2"><p>که آید یل نامور جنگجوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پذیره شدش تا به فرسنگ چند</p></div>
<div class="m2"><p>سری پر زتاب و دلی مستمند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به شادی ز نوشاد زان مرز شد</p></div>
<div class="m2"><p>یکایک به روی فرامرز شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ورا دید با چتر و کوس و درای</p></div>
<div class="m2"><p>نهاده به سر تاج و بر پیل جای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فروبسته برپیل جنگیش تخت</p></div>
<div class="m2"><p>یکی تخت زیبا چو زرین درخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیاده شد او را ستایش گرفت</p></div>
<div class="m2"><p>ابر بندگی ها ستایش گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدو گفت کای گرد فیروزگر</p></div>
<div class="m2"><p>مبارک پی تو براین بوم و بر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدان را دو چشم از رخت دور باد</p></div>
<div class="m2"><p>جهان بنده و چرخ مزدور باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بپرسید از آن کار و از روزگار</p></div>
<div class="m2"><p>چو بگریست زان کار نوشاد زار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز کناس نالید و برگفت اوی</p></div>
<div class="m2"><p>کزآن دخترانم چه آمد به روی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرامرز گفتا که دل شادکن</p></div>
<div class="m2"><p>وز اندوه دختر دل آزاد کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که من مغز او زان سرتیرگون</p></div>
<div class="m2"><p>به نیروی یزدان برآرم برون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سپارم ترا دختران سر به سر</p></div>
<div class="m2"><p>به فر جهاندار فیروزگر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زمین را ببوسید نوشاد وگفت</p></div>
<div class="m2"><p>که از تخم سام این نباشد شگفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فرامرز چون شد به شهر اندرش</p></div>
<div class="m2"><p>شد از بام او زرفشان افسرش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زرافشان برآمد زهر بام و کوی</p></div>
<div class="m2"><p>شد از مشک و عنبر جهان چارسوی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ابر درگه کاخ نوشاد پیل</p></div>
<div class="m2"><p>فکنده همه فرش دیبا دو میل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه بام در زیور آراسته</p></div>
<div class="m2"><p>چو فردوس شد گیتی آراسته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برآمد زرافشان به کاخ بزرگ</p></div>
<div class="m2"><p>برآن نامور تخت پیل سترگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برآمد دم مشکبویان زکوی</p></div>
<div class="m2"><p>جهان مجمر و عود شد چارسوی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فرود آمد از پیل جنگی دلیر</p></div>
<div class="m2"><p>سوی تخت نوشاد شه شد چو شیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو دید اندر آن حقه دلپذیر</p></div>
<div class="m2"><p>زعاج و زبرجد نهاده سریر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به لعل و بلورین برآموده تاج</p></div>
<div class="m2"><p>همه فرش زر اوفتاده سراج</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همه سقف او نقطه زر و سیم</p></div>
<div class="m2"><p>زصندل همان چوب ایوان مقیم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نگاریده این نقش های سپهر</p></div>
<div class="m2"><p>چو هرمزد و بهرام و کیوان و مهر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زبرج حمل تا به حوت اندران</p></div>
<div class="m2"><p>برآورده یک یک همه پیکران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همه نقش شب کرده پرآبنوس</p></div>
<div class="m2"><p>همه روز برچهره سندروس</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همان هفت کشور زمین و خیال</p></div>
<div class="m2"><p>چو ایام سیبوع و با ماه و سال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همان پایه تخت بر پشت شیر</p></div>
<div class="m2"><p>تو گویی که زندست و غرو دلیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فرامرز بر شد بر آن تخت زر</p></div>
<div class="m2"><p>به کرسی دلیران والاگهر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کمر بسته نوشاد چون سروری</p></div>
<div class="m2"><p>شده خیره در ماه سرو سهی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>درآن برز بالا و آهستگی</p></div>
<div class="m2"><p>در آن گرز و کوپال و شایستگی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بفرمود هرگونه خوان و خورش</p></div>
<div class="m2"><p>که آرد همی مرد خوالیگرش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فرامرز و نوشاد و چندین دلیر</p></div>
<div class="m2"><p>نشستند و خوردند چو گشتند سیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نهادند زان پس می و چنگ پیش</p></div>
<div class="m2"><p>به جام بلورین زده چنگ خویش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فرامرز یل چون شد از باده مست</p></div>
<div class="m2"><p>چنین گفت کای مرد یزدان پرست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چرا می نگویی سخن های کید</p></div>
<div class="m2"><p>زما هر دو گویی کدامیم صید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بدو گفت کای مهتر نیمروز</p></div>
<div class="m2"><p>اگر بر شمارم شود نیم روز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زکید بداندیش ناکس سخن</p></div>
<div class="m2"><p>همانا که آن هم نیاید به بن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سپاهش دلیر است و لشکر بسی</p></div>
<div class="m2"><p>ولی چون تو با او ندیدم کسی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فزون صدهزار است جنگ آورش</p></div>
<div class="m2"><p>زپیلان جنگی نترسد دلش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به هرسال بفرستد او باج خواه</p></div>
<div class="m2"><p>نیارم بدو نیز کردن نگاه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مرا چل هزار است مرد نبرد</p></div>
<div class="m2"><p>نیارم برابر شد آورد کرد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زر و گنج او را که داند شمار</p></div>
<div class="m2"><p>به هر گوشه گنجش بود صدهزار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دو دختست او را چو خرم بهار</p></div>
<div class="m2"><p>گرانمایه گرد جنگی سوار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شنیدم که در روز آورد و کین</p></div>
<div class="m2"><p>هرآن کس که پشتش نهد بر زمین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نباشد جز او جفت آن دخترم</p></div>
<div class="m2"><p>دو بهره ورا شاهی و لشکرم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بسا رزم سازان گران نامور</p></div>
<div class="m2"><p>به کشتی گری در نهادند سر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بکوشید با او چو شیر ژیان</p></div>
<div class="m2"><p>ولیکن نشد هیچ عاجز از آن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سمن رخ یکی و سمن بر یکی</p></div>
<div class="m2"><p>که پیدا نه رخشان زماه اندکی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سمن رخ بسی گرد و چابک تر است</p></div>
<div class="m2"><p>دلیران شه را یکایک سر است</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>فرامرز گفت ای شه نامدار</p></div>
<div class="m2"><p>دو چشمت سوی باده و جام دار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>که من کید را با سمن رخ که هست</p></div>
<div class="m2"><p>بیارم به پشت فروبسته دست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بیارم همان گنج هندوستان</p></div>
<div class="m2"><p>فرستم سوی زابل و سیستان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نباید مرا لشکر و ساز وکوس</p></div>
<div class="m2"><p>نه پیلان که گیتی کنند آبنوس</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تو بنشین و با لشکری باده نوش</p></div>
<div class="m2"><p>من و گرز و میدان کید و خروش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>مرا دو هزار است گردن فراز</p></div>
<div class="m2"><p>سپاه تو را من ندارم نیاز</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سپاه فراوان چه سازم همین</p></div>
<div class="m2"><p>چه خود گرز کین برفرازم به کین</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بدین سان یکی هفته در چنگ و نوش</p></div>
<div class="m2"><p>نشستند و برشد به کیوان خروش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به یاد جهاندار کاوس کی</p></div>
<div class="m2"><p>ببودند با رامش و رود و می</p></div></div>