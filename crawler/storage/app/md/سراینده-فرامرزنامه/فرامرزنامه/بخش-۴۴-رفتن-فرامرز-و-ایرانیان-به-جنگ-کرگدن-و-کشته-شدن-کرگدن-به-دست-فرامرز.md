---
title: >-
    بخش ۴۴ - رفتن فرامرز و ایرانیان به جنگ کرگدن و کشته شدن کرگدن به دست فرامرز
---
# بخش ۴۴ - رفتن فرامرز و ایرانیان به جنگ کرگدن و کشته شدن کرگدن به دست فرامرز

<div class="b" id="bn1"><div class="m1"><p>چو یک هفته مستان شدند انجمن</p></div>
<div class="m2"><p>جهان پهلوان گفت زان کرگدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نوشاد گفت ای شه بافرین</p></div>
<div class="m2"><p>کجا باشد آن کرگدن در زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدوگفت زایدر به فرسنگ بیست</p></div>
<div class="m2"><p>چنان شد که کس را نشانی نه زیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سه فرسنگ بینی یکی مرغزار</p></div>
<div class="m2"><p>گله گشته به هر سویی ده هزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی بیشه بینی سه فرسنگ راه</p></div>
<div class="m2"><p>بدان بیشه کردن که آرد نگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه کرگدن گردن افراخته</p></div>
<div class="m2"><p>زمین را به دندان برافراخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکایک همه ژنده پیلان به تن</p></div>
<div class="m2"><p>به هر بیشه و دشت و غار،انجمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه غم دارد از گرز و کوپالشان</p></div>
<div class="m2"><p>که شاید بریدن پی یالشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرامرز گفت ای شه پرخرد</p></div>
<div class="m2"><p>اگر کرگدن تیغ من بنگرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زبیمش بریزند هریک سروی</p></div>
<div class="m2"><p>یکایک شود مست و افتد به روی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بفرمود تا برکشیدند کوس</p></div>
<div class="m2"><p>برآمد زاسب گران مایه طوس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه گستهم و گرگین و فرزند گیو</p></div>
<div class="m2"><p>جهان سوز گر سم کناد بود نیو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بفرمود کاید سواری هزار</p></div>
<div class="m2"><p>دلیران با آلت کارزار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بشد سوی آن دشت با انجمن</p></div>
<div class="m2"><p>به جایی که بد بیشه و کرگدن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زیک دامن دشت تا کوهسار</p></div>
<div class="m2"><p>زهر سو بفرمود کنده هزار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چوشد کنده آنگه مغاک بلند</p></div>
<div class="m2"><p>زخاشاک بر وی بیفکند چند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به خاک و به خاشاک چون شد نهان</p></div>
<div class="m2"><p>زدیگر کران پهلوان جهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درآورد آن لشکر جنگجوی</p></div>
<div class="m2"><p>کران تا کران یک به یک سو به سوی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دهل های بسیار و کوس ودرای</p></div>
<div class="m2"><p>دف و چنگ و افغان شیپور ونای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکایک همه نعره برداشتند</p></div>
<div class="m2"><p>همان کرگدن پشت برگاشتند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زهر سو دوان کرگدن ده هزار</p></div>
<div class="m2"><p>گله کشته بر دامن کوهسار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در آمد به هامون یکی رستخیز</p></div>
<div class="m2"><p>زافغان همی کردن در گریز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به هرسو که آمد یکی درمیان</p></div>
<div class="m2"><p>زتیغ دلیران شدی پرنیان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه بیشه را آتش اندر زدند</p></div>
<div class="m2"><p>چو زینشان همه بیشه ها بستندند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برفتند هریک به کردار کوه</p></div>
<div class="m2"><p>گریزان به هامون گروها گروه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زهامون چو لشکر برآمد به تنگ</p></div>
<div class="m2"><p>از آن دو گرویی برون شد به جنگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به روی بیابان بکرده مغاک</p></div>
<div class="m2"><p>همه ژنده پیلان کشیدند به خاک</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گله گشته یک روی و بگریختند</p></div>
<div class="m2"><p>برآن چاهساران فرو ریختند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همانا نشد زآن دوان بی شمار</p></div>
<div class="m2"><p>رهایی یکی زان دلیران کار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به هرجا از آن گله بگریختند</p></div>
<div class="m2"><p>برآن چاهساران فرو ریختند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همانا نشد زآن دوان بی شمار</p></div>
<div class="m2"><p>رهایی یکی زان دلیران کار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به هر جا از آن گله بگریختند</p></div>
<div class="m2"><p>به هر شوره ای گل برانگیختند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همه چاهساران پر از کرگدن</p></div>
<div class="m2"><p>به هم درفتاده شدند انجمن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چه در چه فتاده چه کشته چه خست</p></div>
<div class="m2"><p>چنان شد که یک تن ازیشان نرست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زشمشیرشیران خنجر گذار</p></div>
<div class="m2"><p>تهی گشت از کرگدن دشت وغار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یکی زان میان برکرانه نشد</p></div>
<div class="m2"><p>خطایی به دست زمانه نشد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو زان دد تهی شد همی دشت وغار</p></div>
<div class="m2"><p>دویدند هریک بدان چاهسار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زپیلان بجوشید کوس و مغاک</p></div>
<div class="m2"><p>گرفته ز پیش و زپس چاک چاک</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خروشان به چاهسار سیصدهزار</p></div>
<div class="m2"><p>جهان گشته چون ساز درنده وار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بکشتند برهر سر چاهسار</p></div>
<div class="m2"><p>به هر جایکی بد بکشتند زار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چنان دان که در دشت کین چند میل</p></div>
<div class="m2"><p>همه لاشه کرگدن شد دو میل</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به هر جا سرشیر پست آمده</p></div>
<div class="m2"><p>به هر سوتن پیل خست آمده</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نشد تیز دندان دد کارگر</p></div>
<div class="m2"><p>که از تیغ شیران پرخاشخر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو دد شد یکایک به گیتی نهان</p></div>
<div class="m2"><p>سوی چشمه شد پهلوان جهان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به هر جا که پیلی درانداختند</p></div>
<div class="m2"><p>کشیدند هردو پی ای ساختند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همه هفته در چاهساران بماند</p></div>
<div class="m2"><p>فرستاد نوشاد لشکر بخواند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نگه کرد نوشاد تا چند میل</p></div>
<div class="m2"><p>بیفکنده کشته شده ژنده پیل</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بر ایرانیان از جهان آفرین</p></div>
<div class="m2"><p>زنو خواند هردم بسی آفرین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ددان را که بد بر تل انداختند</p></div>
<div class="m2"><p>گروها گروه انجمن ساختند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شمردن بفرمود پس شهریار</p></div>
<div class="m2"><p>شماره برآمد چهل شش هزار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شه هندوان شاد دل گشت ازین</p></div>
<div class="m2"><p>به ایرانیان کرد صدآفرین</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همی گفت کین رخت آمیخته</p></div>
<div class="m2"><p>به تدبیر ایرانیان ریخته</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نه از دیو پیچد نه از تیره گرگ</p></div>
<div class="m2"><p>نه از کرگدن اژدهای بزرگ</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سرانجام این بد چو ایدون بود</p></div>
<div class="m2"><p>ندانم که تاکید،ره چون بود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ببینم سر و مغز شیران زکید</p></div>
<div class="m2"><p>پراکنده و دختران کرد صید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>فرامرز گفت ای سرموبدان</p></div>
<div class="m2"><p>چوبینی همین کار وبارددان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همه این چنین تا به هند اندرون</p></div>
<div class="m2"><p>به ایران شمارند خوار وزبون</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تو گفتی که لشکر نیاید به کار</p></div>
<div class="m2"><p>بدان ژنده پیلان ناهوشیار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ندیدی تو گردان ایرانیان</p></div>
<div class="m2"><p>که بندند هنگام کین چون میان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چنین یک به یک درچه این آورند</p></div>
<div class="m2"><p>به نیزه فلک بر زمین آورند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به پاسخ چنین داد کای نامدار</p></div>
<div class="m2"><p>زگرشسب واطرط تویی یادگار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>شنیدم که چون رو به آورد زد</p></div>
<div class="m2"><p>به کینه همی مرد بر مرد زد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هرآن گه که گشتی به آورد مست</p></div>
<div class="m2"><p>نبردی سوی تیغ پیکار دست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>پیاده دویدی به کردار گرد</p></div>
<div class="m2"><p>یکی را گرفتی زدی بردو مرد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>سپاسم به دادار جان آفرین</p></div>
<div class="m2"><p>که چون تو کسی سوی ما شد قرین</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>که ایمن کنی مرز هند از بدی</p></div>
<div class="m2"><p>چو خورشید سازی ره ایزدی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>یکی هفته زان پس در آن مرغزار</p></div>
<div class="m2"><p>نشستند جویان زبهر شکار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به هر مرز کو بود سرسرنهاد</p></div>
<div class="m2"><p>بیامد بدید آن دلیران راد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چو دیدی بسی آفرین خواندی</p></div>
<div class="m2"><p>برآن لشکر و جیش درماندی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو نوشاد از آن نامور شادشد</p></div>
<div class="m2"><p>از آن چاره پتیاره آزاد شد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>سران های آن کرگدن سه هزار</p></div>
<div class="m2"><p>بریده بیاورد بهر نظار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>همه هندوان شاد وخرم شدند</p></div>
<div class="m2"><p>از آن برجهان جمله بی غم شدند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چنان بد همه کار وبار جهان</p></div>
<div class="m2"><p>وز آن پس تو را باشد اندر جهان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ازین پس کنون رسم کید آوریم</p></div>
<div class="m2"><p>عقابی برانیم و صیدآوریم</p></div></div>