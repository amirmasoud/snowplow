---
title: >-
    بخش ۹۰ - رفتن فرامرز به هندوستان ونشاندن دستور در مرز خرگاه و نامه نوشتن به رای هند
---
# بخش ۹۰ - رفتن فرامرز به هندوستان ونشاندن دستور در مرز خرگاه و نامه نوشتن به رای هند

<div class="b" id="bn1"><div class="m1"><p>سرماه فرمود تا کرنای</p></div>
<div class="m2"><p>دمیدند و برداشت لشکر زجای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوانی سرافراز با رای وکام</p></div>
<div class="m2"><p>ابا پهلوان، خویش و دستور نام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه مرز خرگاه، او را سپرد</p></div>
<div class="m2"><p>یکی لشکرش داد مردان و گرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو گفت پیوسته بیدار باش</p></div>
<div class="m2"><p>سپه را زدشمن نگهدار باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرآرند ترکان یکی تاختن</p></div>
<div class="m2"><p>نباید تو را جای پرداختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به زابل به دستان فرست آگهی</p></div>
<div class="m2"><p>کزویست پاینده تخت مهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هرکار،یاری فرستد برت</p></div>
<div class="m2"><p>برآرد به گردون گردان سرت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور ایدون که باشد جهان پهلوان</p></div>
<div class="m2"><p>به زابل تو را خود چه بیم از بدان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگفت این و پس سرسوی راه کرد</p></div>
<div class="m2"><p>از آن ره برآورد از ماه،گرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیامد دمان سوی هندوستان</p></div>
<div class="m2"><p>بدید آن بر و بوم و جادوستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی مرد بودی سپهدار هند</p></div>
<div class="m2"><p>که حکمش روان بود تا مرز سند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به نام ونشان بود آن مرد، رای</p></div>
<div class="m2"><p>یکی دانش افروز پاکیزه رای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همش لشکر وپیل و اسباب بود</p></div>
<div class="m2"><p>شب و روز با جنگ وبا تاب بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی گفت چون من به روی زمین</p></div>
<div class="m2"><p>نباشد به مردی گه رزم وکین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زهندوستان،سی و شش پادشاه</p></div>
<div class="m2"><p>ورا تاج دادی به سال و به ماه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شمار سپاهش نیامد پدید</p></div>
<div class="m2"><p>از اندازه بگذشت گفت و شنید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرامرز یل چون بدان جا رسید</p></div>
<div class="m2"><p>دبیری خردمند پیش آورید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی نامه فرمود نزدیک رای</p></div>
<div class="m2"><p>بدان تا بداند به پاکیزه رای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نخست ا زجهان آفرین کرد یاد</p></div>
<div class="m2"><p>در داد و دانش برو برگشاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خداوند بی یار و انباز و جفت</p></div>
<div class="m2"><p>کزویست پیدا نهان و نهفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جهان آفریننده بی نیاز</p></div>
<div class="m2"><p>به فرمان او دان نشیب وفراز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به قدرت زناچیز،چیزآفرید</p></div>
<div class="m2"><p>هم از خاک،گوهر پدید آورید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به فرمان اویند خورشید و ماه</p></div>
<div class="m2"><p>وزو دارد آرام، خاک سیاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خداوند،اویست و ما بندگان</p></div>
<div class="m2"><p>به فرمان و رایش سرافکندگان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دگرگفت ای شاه با دستگاه</p></div>
<div class="m2"><p>همت مهر و گنجست و تخت وکلاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تویی شاه گردنفرازان هند</p></div>
<div class="m2"><p>به فرمانت از مرز چین تا به سند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همانا شنیدی که از آب و خاک</p></div>
<div class="m2"><p>زباد و زآتش جهاندار پاک</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همی تا جهان و مکان آفرید</p></div>
<div class="m2"><p>که تا آدمی از درش برکشید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو کیخسرو پاک دل در جهان</p></div>
<div class="m2"><p>نبد پادشاهی پدید از مهان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به چهر و به مهر و به خوبی و داد</p></div>
<div class="m2"><p>به نیرو و فرهنگ و فر ونژاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز فرش جهان گشت پرایمنی</p></div>
<div class="m2"><p>شده پست کردار اهریمنی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شهان جهانش همه بنده اند</p></div>
<div class="m2"><p>به فرمان او گردن افکنده اند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از آنگه که پروردگار جهان</p></div>
<div class="m2"><p>ورا برگزید از میان مهان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به شاهی،زمین یکسر او را سپرد</p></div>
<div class="m2"><p>کسی نام دیگر به شاهی نبرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بزرگان و شاهان روی زمین</p></div>
<div class="m2"><p>سراسر بدو خواندند آفرین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جهاندار،تخم سیاوش بود</p></div>
<div class="m2"><p>نژاد فریدون باهش بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که ضحاک بدگوهر بدنژاد</p></div>
<div class="m2"><p>بکشت و برآورد تخمش به باد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو هرگز نرفتی بدان بوم و بر</p></div>
<div class="m2"><p>نکردی به درگاه او برگذر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر سر به فرمان درآری رواست</p></div>
<div class="m2"><p>که او در جهان سر به سر پادشاست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فرستی به درگاه او باج وساو</p></div>
<div class="m2"><p>بدانی که با او تو را نیست تاو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>وگرنه من این مرز هندوستان</p></div>
<div class="m2"><p>بر وبوم این دشت جادوستان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زشمشیر تیز آتش اندر زنم</p></div>
<div class="m2"><p>بن وبیخ هندی زبن برکنم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یکی داستان دارم از شاه یاد</p></div>
<div class="m2"><p>به اندرز بر من زبان برگشاد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که مرد خردمند پاکیزه دین</p></div>
<div class="m2"><p>کرانه پذیرد زبیداد و کین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بدان گفتم این تا نگویی که من</p></div>
<div class="m2"><p>به هند آورم بهر رزم و شکن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وگر نشنوی هر چه در نامه است</p></div>
<div class="m2"><p>پشیمان شوی باد ماند به دست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کنون گر خردمندی و هوشیار</p></div>
<div class="m2"><p>به هرکار باشد تورا هوش دار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زجنگ و ز پیکار یکسو شوی</p></div>
<div class="m2"><p>به آسودگی در جهان بغنوی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بماند به تو مرز و گنج و سپاه</p></div>
<div class="m2"><p>بزرگی و شاهی و تخت و کلاه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خردمند داننده پیش بین</p></div>
<div class="m2"><p>بداند که نفرین به از آفرین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همان به که دل را به اندوه و رنج</p></div>
<div class="m2"><p>کنی شادمان در سرای سپنج</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو از باد بر روی کافور مشک</p></div>
<div class="m2"><p>نوشتند شد نامه یکباره خشک</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>فرامرز،مهری برآن برنهاد</p></div>
<div class="m2"><p>یکی دانشی خواند با رای وداد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>یکی نامور بد کیانوش نام</p></div>
<div class="m2"><p>جهان دیده و گرد وبا رای وکام</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چنین گفت مهتر،کیانوش را</p></div>
<div class="m2"><p>که از زهر،کردن جدا نوش را</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ببر نامه من به رای برین</p></div>
<div class="m2"><p>شه هند و کشمیر و والی چین</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سخن ها از آنی که رای آیدت</p></div>
<div class="m2"><p>پس نامه برگو چو کارآیدت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کیانوش باهوش،ره ساز کرد</p></div>
<div class="m2"><p>در دانش و زیرکی باز کرد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بسیچید وآمد به هندوستان</p></div>
<div class="m2"><p>شتابان بر رای روشن روان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به قنوج بودی نشستنگهش</p></div>
<div class="m2"><p>همان گنج و لشکرگه و بنگهش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>از آنجا که بد پهلوان سپاه</p></div>
<div class="m2"><p>کشیدی به قنوج یک ماهه راه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دلاور کیانوش فرخنده رای</p></div>
<div class="m2"><p>بیامد به درگاه،نزدیک رای</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>یکی را فرستاده نامور</p></div>
<div class="m2"><p>زنزد فرامرز والاگهر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو در نزد سالار،پیغام گفت</p></div>
<div class="m2"><p>دلاور کیانوش پاکیزه جفت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بشد پرده دار و بگفتا به شاه</p></div>
<div class="m2"><p>که آمد فرستاده با کلاه</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چوآگه ازو گشت رای گزین</p></div>
<div class="m2"><p>زکار کیانوش پاکیزه دین</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به آیین بفرمود تا کوس وپیل</p></div>
<div class="m2"><p>برآراستند از زمین،پنج میل</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>پذیره شدندش دلیران هند</p></div>
<div class="m2"><p>دلاور سواران و شیراه هند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ببردند پیلان و رویینه خم</p></div>
<div class="m2"><p>همان تاج زرین و زرین شرار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به شهراندر آمد جهاندیده مرد</p></div>
<div class="m2"><p>بیامد به درگاه،با دار و برد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به نزدیک رای اندر آمد دوان</p></div>
<div class="m2"><p>ثنا خواند بر پیشگاه و ردان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بدو داد پس نامه پهلوان</p></div>
<div class="m2"><p>از آن پس که بنشست روشن روان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>درودش رسانید و بردش نماز</p></div>
<div class="m2"><p>ستایش نمودش زمانی دراز</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>یکی تخت همچون سپهری ز زر</p></div>
<div class="m2"><p>نهاده مرصع به در و گهر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>همی پایه تخت زرین،بلور</p></div>
<div class="m2"><p>برو پیکر شیر و آهو و گور</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>نشسته بدو خسرو هندوان</p></div>
<div class="m2"><p>ابا یاره و طوق شاه جوان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>یکی کرسی زر بفرمود شاه</p></div>
<div class="m2"><p>زبهر کیانوش در بارگاه</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>نهادند بر کرسی زر نشست</p></div>
<div class="m2"><p>کمر برمیان، دست بر نشست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>دبیر خردمند روشن روان</p></div>
<div class="m2"><p>به خود خواند آن شهریار جوان</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>دبیرآمد و نامور رای هند</p></div>
<div class="m2"><p>به او داد آن نامه دل پسند</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>سرنامه چون برگشاد آن دبیر</p></div>
<div class="m2"><p>تو گفتی که بد مشک و عود و عبیر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>فروخواند نامه بدان سان که بود</p></div>
<div class="m2"><p>به هندی زبان گفت و خسرو شنود</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>زمانی برآشفت رای برین</p></div>
<div class="m2"><p>به ابرو ز خشم اندر آورد چین</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به هندی زبان گفت با ترجمان</p></div>
<div class="m2"><p>که هرگز که دیدست کایرانیان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>به هندوستان این دلیری کنند</p></div>
<div class="m2"><p>سترگی نمایند و شیری کنند</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>به ایران اگر شاه،کیخسرو است</p></div>
<div class="m2"><p>نه تاج من اندر زمانه نو است</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>پدر به پدر،شاه کشمیر و هند</p></div>
<div class="m2"><p>به شاهی منم در جهان بی گزند</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>زتهدید و اندرز و بیم و امید</p></div>
<div class="m2"><p>زهندوستان،باج دارم امید</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>همانا نداند که من کیستم</p></div>
<div class="m2"><p>بدین بوم و بر از پی چیستم</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>اگر لشکرآرم سوی کارزار</p></div>
<div class="m2"><p>زپیلان ایران برآرم دمار</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>کیانوش،پاسخ چنین داد باز</p></div>
<div class="m2"><p>که تندی نه خوب آید از سرفراز</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>سخن گویمت بشنو از راه داد</p></div>
<div class="m2"><p>بود گاه،کین گفتن آید به یاد</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>تو شاهی و برهندوان مهتری</p></div>
<div class="m2"><p>ولی کارها را مدان سرسری</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>تو دانی و بشنیده باشی مگر</p></div>
<div class="m2"><p>که کیخسرو آن شاه فیروزگر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>جهانگیر و شاه بلند اختر است</p></div>
<div class="m2"><p>زدانش ز چرخ برین برتر است</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>به تنها تن خود ز توران زمین</p></div>
<div class="m2"><p>چو شیری بیامد به ایران زمین</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>دو لشکر زتوران بیامد پیش</p></div>
<div class="m2"><p>جز از بخت،یاری نبودی کسش</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ازو آن دو لشکر چنان بازگشت</p></div>
<div class="m2"><p>که گریان شد آهو بدیشان به دشت</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>به فر بزرگی و شاهنشهی</p></div>
<div class="m2"><p>زمین،بنده شد آسمانش رهی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>گذر کرد از آب جیحون بر اسب</p></div>
<div class="m2"><p>به ایران زمین شد چو آذرگشسب</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>دژی بود در شهر آبادکان</p></div>
<div class="m2"><p>از آن بد غم ورنج آزادگان</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>کجا جاودان را بر آن برزکوه</p></div>
<div class="m2"><p>بدی جایگاشان و مردم ستوه</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>یکی آدمی اندر آن بوم و بر</p></div>
<div class="m2"><p>به کاری نیارست کردن گذر</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>به فر کیانی و نیک اختری</p></div>
<div class="m2"><p>به مردی و گردی و کند آوری</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>همه بوم و برکرد از ایشان تهی</p></div>
<div class="m2"><p>به گردون برافراخت تاج مهی</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>سراسر همه جاودان را بکشت</p></div>
<div class="m2"><p>به مردی و شمشیر وبا گرز و مشت</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>زخاور زمین تا در باختر</p></div>
<div class="m2"><p>همه بسته دارند پیشش کمر</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>سپهدار او توس نوذر نژاد</p></div>
<div class="m2"><p>سپه کش چو گودرز با فر وداد</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>چو لهراسب و اشکش دو گرد دلیر</p></div>
<div class="m2"><p>برفتند مانند نره شیر</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>به کین پدر تا بسی روزگار</p></div>
<div class="m2"><p>برآورد از شهر توران دمار</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>زبس دیرکاید شما را خبر</p></div>
<div class="m2"><p>که آن مرز کردند زیر وزبر</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بدین مرز،پور جهان پهلوان</p></div>
<div class="m2"><p>سپهبد فرامرز روشن روان</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>به خوبی فرستاد نزدت پیام</p></div>
<div class="m2"><p>مگر تیغ کین ماند اندر نیام</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>که مرد خردمند باهوش و هنگ</p></div>
<div class="m2"><p>نجوید زبیداد،پیکار و جنگ</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>تو با وی به پرخاش گویی سخن</p></div>
<div class="m2"><p>نه سر بینم این گفتگو را نه بن</p></div></div>