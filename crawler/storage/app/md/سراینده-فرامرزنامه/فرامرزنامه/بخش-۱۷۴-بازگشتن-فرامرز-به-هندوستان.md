---
title: >-
    بخش ۱۷۴ - بازگشتن فرامرز به هندوستان
---
# بخش ۱۷۴ - بازگشتن فرامرز به هندوستان

<div class="b" id="bn1"><div class="m1"><p>چو یک چند نزدیک شاه جهان</p></div>
<div class="m2"><p>ببود آن سرافراز روشن روان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوی بازگشتن به هندوستان</p></div>
<div class="m2"><p>همی زد به نزدیک سرداستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نوعی یکی عهد بنوشت شاه</p></div>
<div class="m2"><p>زبهر فرامرز لشکر پناه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه کشور سند و هندوستان</p></div>
<div class="m2"><p>زقنوج کشمیر چون بوستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همان منزل مرغ با مرز چین</p></div>
<div class="m2"><p>بدو داد پیوسته شاه گزین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی تاج با طوق و با گوشوار</p></div>
<div class="m2"><p>همان تخت فیروزه زرنگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرامرز را داد شاه جهان</p></div>
<div class="m2"><p>بدو گفت کای گرد روشن روان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگر تا نیاری به بیداد،دست</p></div>
<div class="m2"><p>که دنیا نباشد سرای نشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرآن کس که باتو کند کارزار</p></div>
<div class="m2"><p>برآور ز مرز و سپاهش دمار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل زیردستان میازار هیچ</p></div>
<div class="m2"><p>کزین پس ببینی بسی درد و پیچ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به هر جایگه یار درویش باش</p></div>
<div class="m2"><p>به دهقان بیچاره چون خویش باش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان کن که هر جا که نامت برند</p></div>
<div class="m2"><p>به نیکی به تن آفرین گسترند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زمین را ببوسید در پیشگاه</p></div>
<div class="m2"><p>بدوگفت کای نامور پادشاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زپیمان شاه جهان نگذرم</p></div>
<div class="m2"><p>به هر جا که باشم کهین چاکرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ده ودوهزار از دلیران گرد</p></div>
<div class="m2"><p>گزین کرد شاه و مر او را سپرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سپیده دمان برخروشید نای</p></div>
<div class="m2"><p>دلاور نشست از بر بادپای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سوی هندوان تیز لشکر براند</p></div>
<div class="m2"><p>بیامد به زابل دوهفته بماند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی بود با او گو پیل تن</p></div>
<div class="m2"><p>بسی پند دادش به هر انجمن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که در کار،هشیار باید بدن</p></div>
<div class="m2"><p>نباید که دشمن کند تاختن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دگر نامور زال سام سوار</p></div>
<div class="m2"><p>بدو گفت ای پهلو نامدار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به هر کار باموبدان گفت کن</p></div>
<div class="m2"><p>پس آنگه بدان رای خود جفت کن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نباید که آسیب یابی ز دهر</p></div>
<div class="m2"><p>همان خرمی بادت از دهر،بهر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>و دیگر ز هر نیک و بد کایدت</p></div>
<div class="m2"><p>فرستاده نزدیک ما بایدت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پذیرفت گفت نیا وپدر</p></div>
<div class="m2"><p>زمین را ببوسید وآمد به در</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وز آن جا به قنوج شد با سپاه</p></div>
<div class="m2"><p>جهان گشته از دل،ورا نیکخواه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چوآمد،برآسود وخرم بزیست</p></div>
<div class="m2"><p>به نخجیر و رامش همی کرد زیست</p></div></div>