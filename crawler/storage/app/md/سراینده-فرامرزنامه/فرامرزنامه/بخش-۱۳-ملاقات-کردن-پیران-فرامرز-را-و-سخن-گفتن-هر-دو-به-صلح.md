---
title: >-
    بخش ۱۳ - ملاقات کردن پیران، فرامرز را و سخن گفتن هر دو به صلح
---
# بخش ۱۳ - ملاقات کردن پیران، فرامرز را و سخن گفتن هر دو به صلح

<div class="b" id="bn1"><div class="m1"><p>که گر می پذیری درین صیدگاه</p></div>
<div class="m2"><p>کنی سربلندم در این جایگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک امروز و فردا هم اینجا بیا</p></div>
<div class="m2"><p>تو مهمان مایی و ما کدخدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو روزه در این دشت با یکدگر</p></div>
<div class="m2"><p>بباشیم با شادی و نوش خور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر بیخ کین از جهان برکنیم</p></div>
<div class="m2"><p>ز دل دوستی در میان آوریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابا همدمان باده نوشیم شاد</p></div>
<div class="m2"><p>زکین گذشته نیاریم یاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلاور چو گفتار پیران شنید</p></div>
<div class="m2"><p>پیاده شد و آفرین گسترید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نزدیک او رفت پیران گرد</p></div>
<div class="m2"><p>بدید آن سرفراز با دستبرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو دید آن بر و پیکر دلپذیر</p></div>
<div class="m2"><p>ز دیدار او شد جوان مرد پیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدان حسن و مردی دیدار او</p></div>
<div class="m2"><p>به جان هر کسش شد خریدار او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوش آمدش گفتار و بالای او</p></div>
<div class="m2"><p>به خوبی سخن گفتن و رای او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ببوسید روی فرامرز شیر</p></div>
<div class="m2"><p>سرافراز پیران گرد دلیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فرامرز گفتش که ای پهلوان</p></div>
<div class="m2"><p>سزدگر بیایی به روشن روان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نخستین شمایید مهمان ما</p></div>
<div class="m2"><p>که من دوستداری کنم با شما</p></div></div>