---
title: >-
    بخش ۱۷۵ - زادن سام از دختر فرطورتوش و زادن آذربرزین از دختر شاه کهیلا
---
# بخش ۱۷۵ - زادن سام از دختر فرطورتوش و زادن آذربرزین از دختر شاه کهیلا

<div class="b" id="bn1"><div class="m1"><p>چو یک سال بگذشت از روزگار</p></div>
<div class="m2"><p>بدو داد جان آفرین کردگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن دختر شاه فرطورتوش</p></div>
<div class="m2"><p>یکی پورش آمد چو فرخ سروش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ورا نام کردند سام دلیر</p></div>
<div class="m2"><p>سرافراز وفرخ پی ودلپذیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو گفتی مگر زنده شد سام گرد</p></div>
<div class="m2"><p>بزرگی و گردی مر او را سپرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس از سال دیگر ورا همچنین</p></div>
<div class="m2"><p>همان پاک یزدان جهان آفرین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازآن دخت شه کهیلا یکی</p></div>
<div class="m2"><p>پدید آمدش خوب رخ کودکی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو گویی که گرشسب یل زنده شد</p></div>
<div class="m2"><p>فلک پیش آن نامور بنده شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>و یا رستم زال،مردی و زور</p></div>
<div class="m2"><p>ببخشید بر نامور گرد پور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به آذار برزینش کردند نام</p></div>
<div class="m2"><p>هم از فر او یافت آرام وکام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرامرز چون نام اوکرد زود</p></div>
<div class="m2"><p>فرستاده ای کرد مانند دود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به نزدیک زال زر پهلوان</p></div>
<div class="m2"><p>یک نامه بنوشت روشن روان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که دارای جان وجهان آفرین</p></div>
<div class="m2"><p>دو بنده فزود از شما پاک دین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یک آذاربرزین دگر سام گرد</p></div>
<div class="m2"><p>بدانم که باشند با دستبرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>امیدم به دادار روز شمار</p></div>
<div class="m2"><p>که زیبند بر درگه شهریار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو نامه بر نامداران رسید</p></div>
<div class="m2"><p>زشادی روانشان به جان آرمید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فراوان به درگاه جان آفرین</p></div>
<div class="m2"><p>ستایش نمودند رخ بر زمین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کزان سان دو فرزند گرد سترگ</p></div>
<div class="m2"><p>پدید آمد از پهلوان بزرگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چوازآفرین گشته پرداخته</p></div>
<div class="m2"><p>نشستند با رود ومی ساخته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپهبد یل نامور پیل تن</p></div>
<div class="m2"><p>نگهدار گیتی،سرانجمن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی عهد بنوشت با رای وداد</p></div>
<div class="m2"><p>به نام دو فرزند والانژاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه بهره از شهر کابلستان</p></div>
<div class="m2"><p>هم ازکاخ وایوان زابلستان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدان دو گرامی پس داد وگفت</p></div>
<div class="m2"><p>که همواره با کام باشند جفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فرستاده را خلعت افکند و رفت</p></div>
<div class="m2"><p>به پیش سپهبد فرامرز،تفت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرآن نامه بوسید و بنهاد پیش</p></div>
<div class="m2"><p>برآن نامور آفرین کرد بیش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فرامرز درهندوان بازماند</p></div>
<div class="m2"><p>همه روز با رامش وناز ماند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به دیدار آن هر دو پور جوان</p></div>
<div class="m2"><p>همی بود شادان و روشن روان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ابر هندوان،شاه بد شصت سال</p></div>
<div class="m2"><p>که درگیتی اندر نبودش همال</p></div></div>