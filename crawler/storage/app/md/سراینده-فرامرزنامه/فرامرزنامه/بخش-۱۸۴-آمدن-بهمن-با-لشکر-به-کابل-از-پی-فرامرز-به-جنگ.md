---
title: >-
    بخش ۱۸۴ - آمدن بهمن با لشکر به کابل از پی فرامرز به جنگ
---
# بخش ۱۸۴ - آمدن بهمن با لشکر به کابل از پی فرامرز به جنگ

<div class="b" id="bn1"><div class="m1"><p>دگرباره هردوبرابرشدند</p></div>
<div class="m2"><p>همه پیش زوبین وخنجرشدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان اندکی لشکرپهلوان</p></div>
<div class="m2"><p>یکایک فداکرده پیشش روان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرامرزدرپیش صف بانگ زد</p></div>
<div class="m2"><p>که ای نامداران پاکیزه قد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدانید کامروز روزیست بد</p></div>
<div class="m2"><p>اگر نام مانیم وگر سر رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرآن کس که برگردد از کارزار</p></div>
<div class="m2"><p>ازو دشمن دون برآرد دمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفت این و لشکر برآمدبه جوش</p></div>
<div class="m2"><p>تو گفتی زمین گشت پولاد پوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی ابر پیوست خونبار گشت</p></div>
<div class="m2"><p>رخ بددلان همچو بیمار گشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هوا گشت از تیغ،زنگارگون</p></div>
<div class="m2"><p>زمین را زخون رنگ گلنارگون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان گشت لرزه سران همچو پیل</p></div>
<div class="m2"><p>روان از برخاک،رودی چو نیل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز زوبین،هوا گشت پربال وار</p></div>
<div class="m2"><p>زجوشن،زمین گشت پولادوار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زکشته،درو ودشت انبوه شد</p></div>
<div class="m2"><p>زجوشن،زمین چون یکی کوه شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپه بازگردید آرام کرد</p></div>
<div class="m2"><p>تکاور به زیر اندرون رام کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهبد سوی لشکر شاه شد</p></div>
<div class="m2"><p>وزآن لشکر شاه، آگاه شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنین گفت کای نامداران کین</p></div>
<div class="m2"><p>دلیران روم و سواران چین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدانید کین رزم ما بی گمان</p></div>
<div class="m2"><p>همی بر بزرگان سرآید زمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فراوان به ما سالیان برگذشت</p></div>
<div class="m2"><p>نیاسود لشکر زپیکار دشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>میان من وبهمن کینه ساز</p></div>
<div class="m2"><p>چنین کار یکبارگی شد دراز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سپاه سه کشور همه کشته شد</p></div>
<div class="m2"><p>جهانی زخون،چون گل آغشته شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به هر خون که شد اندرین روزگار</p></div>
<div class="m2"><p>گرفتار باشد به روز شمار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که مردم همه بی گناهند ازین</p></div>
<div class="m2"><p>ندارد کس از من به دل،رنج کین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ودیگر که با ما سپاه اندکیست</p></div>
<div class="m2"><p>چنان کز شما پانصد،ازما یکیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همانا که داند به جز دادگر</p></div>
<div class="m2"><p>که کوشش بدیشان نباشد هنر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه مردی بود خیره خون ریختن</p></div>
<div class="m2"><p>دو صد با سواری برآویختن؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدین رزم اگر داد خواهید داد</p></div>
<div class="m2"><p>ابا شاه،پیمان بباید نهاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که ما هر دو بیرون شویم از میان</p></div>
<div class="m2"><p>نیاید بدین نامداران زیان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به آوردگه آزمایش کنیم</p></div>
<div class="m2"><p>به مردی،هنرها نمایش کنیم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ببینیم تا چرخ ناسازگار</p></div>
<div class="m2"><p>که را زین دوگانه کند کامکار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر امروز باشد مرا دسترس</p></div>
<div class="m2"><p>کنم با شما آنچه کردید پس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سواری نیازارم از روم وچین</p></div>
<div class="m2"><p>نه از جنگجویان ایران زمین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همه در پناه خداوند هور</p></div>
<div class="m2"><p>سرخویش گیرید مرد و ستور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وگرشاه را دست بر من بود</p></div>
<div class="m2"><p>سپه زیر فرمان بهمن بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بگو هر چه خواهی بکن با سپاه</p></div>
<div class="m2"><p>ببخشای ورنه فروبند راه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سپه را پسندیده آمد سخن</p></div>
<div class="m2"><p>همی گفت با یکدیگر تن به تن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که گفتار این مرد،بیهوده نیست</p></div>
<div class="m2"><p>درین سالیان شکر انبوه نیست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر کینه کش بهمنت ای شگفت</p></div>
<div class="m2"><p>یکی راه میدانش باید گرفت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدیشان همی گفت لشکر همه</p></div>
<div class="m2"><p>چو بشنید شاه آن چنان از رمه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همن گاه پوشید ساز نبرد</p></div>
<div class="m2"><p>برون شد ز لشکر پس آهنگ کرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو دستور فرزانه دید آن چنان</p></div>
<div class="m2"><p>چوآتش بجست و گرفتش عنان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدو گفت ای شاه خورشید فش</p></div>
<div class="m2"><p>تن خویشتن پیش آتش مکش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فرامرز را خوار دادی همی</p></div>
<div class="m2"><p>به دست سبک بر گراهی همی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فراوانش دیدی به هنگام کین</p></div>
<div class="m2"><p>همی نعل اسبش بدوزد زمین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سپاهی به نزدیک او یک تن است</p></div>
<div class="m2"><p>زتیغ و زنیروی خود ایمن است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خدنگش بدوزد دل آفتاب</p></div>
<div class="m2"><p>کمندش درآرد زگردون عقاب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ندارد دمان پیل جنگش به جای</p></div>
<div class="m2"><p>درآرد گران کوه،گرزش ز پای</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گه رزم،ازو دیو گردد دژم</p></div>
<div class="m2"><p>ز ده پیل،نیرو نیایدش کم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو با اژدهایی تو هم سر شوی</p></div>
<div class="m2"><p>ازآن به که با او برابر شوی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چنین داد پاسخ ورا شهریار</p></div>
<div class="m2"><p>که چون او بود مر مرا خواستار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مرا گر به رفتن درنگی بود</p></div>
<div class="m2"><p>گه نام جستن چو ننگی بود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>عنان بست از دست داننده مرد</p></div>
<div class="m2"><p>بزد اسب و آهنگ آورد کرد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ازآن کار،داننده آگاه بود</p></div>
<div class="m2"><p>سپهبد نه اندر خور شاه بود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همانگه رهام گودرز را</p></div>
<div class="m2"><p>بخواند آن دلیران آن مرز را</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو سقلاب روم ودگر رزم یار</p></div>
<div class="m2"><p>بهان رود ودیلم دلاور سوار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بدیشان چنین گفت کای سرکشان</p></div>
<div class="m2"><p>مدارید گفتار پیران گوان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بدانید کین شاه ما سرکش است</p></div>
<div class="m2"><p>هم آورد او چون یکی آتش است</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چوبا او برابر نیاید به کین</p></div>
<div class="m2"><p>نباید که آسیب یابد چنین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سزد گر شما نزد ایشان شوید</p></div>
<div class="m2"><p>به یاری بر شاه ایران شوید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو دانید کان اژدهای دژم</p></div>
<div class="m2"><p>به شاه جهان اندرآید به دم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>شما حمله آرید و اندر نهید</p></div>
<div class="m2"><p>سپاسی به شاه جهان برنهید</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>برفتند پرمایگان هر چهار</p></div>
<div class="m2"><p>نهانی به نزدیکی شهریار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همه بر سپهبد کمین ساختند</p></div>
<div class="m2"><p>همه تیغ کین از میان آختند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو چشم سپهبد به بهمن رسید</p></div>
<div class="m2"><p>فرودآمد و آفرین گسترید</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>وزآن پس بدو گفت کای شهریار</p></div>
<div class="m2"><p>به گیتی همه تخم زشتی مکار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چنان دان که گیتی سراییست تنگ</p></div>
<div class="m2"><p>نباشد درو برکسی را درنگ</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گر از پهلوان کینه ای داشتی</p></div>
<div class="m2"><p>کنون سر زگردون برافراشتی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به کام تو شد کشور نیمروز</p></div>
<div class="m2"><p>شب آمد که ما خود نبینیم روز</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>زما کینه پهلوان آختی</p></div>
<div class="m2"><p>زمین از بزرگان بپرداختی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گرانمایه زالی که هنگام کین</p></div>
<div class="m2"><p>ستوه آمد از سم اسبش زمین</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کنون چون اسیران به بند اندر است</p></div>
<div class="m2"><p>به پیری به دام گزند اندراست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>زتخم نریمان سام سوار</p></div>
<div class="m2"><p>نماندست جز من کسی یادگار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سزدگر به جایی که اکنون مرا</p></div>
<div class="m2"><p>نریزی بدین خیرگی خون مرا</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>یکی باشم از چاکران سپای</p></div>
<div class="m2"><p>اگر کینه از دل بریزی به جای</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>وگر خود نخواهی که بینی رخم</p></div>
<div class="m2"><p>به خون یکی بازده پاسخم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بمان تا زهندوستان بگذرم</p></div>
<div class="m2"><p>دگر باره ایران زمین نگذرم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>اگر دشمنم،دشمن،آواره به</p></div>
<div class="m2"><p>زخون،دست کوتاه،یکباره به</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بدان سر چه سختست بازار خون</p></div>
<div class="m2"><p>مباد هیچ مردم،گرفتار خون</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چنین داد پاسخ ورا شهریار</p></div>
<div class="m2"><p>که سوگند دارم به پروردگار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>که از تخم رستم نمانم یکی</p></div>
<div class="m2"><p>نه از کشور و کاخ او اندکی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>وگرنه به جانت ببخشودمی</p></div>
<div class="m2"><p>زخون،روزگاری برآسودمی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بیاور کنون تا چه داری به بر</p></div>
<div class="m2"><p>کجا روزگار تو آمد به سر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>زگفتار بیهوده،دم بسته به</p></div>
<div class="m2"><p>به پیکان، تن دشمنان خسته به</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>سپهبد چو از شاه نومید گشت</p></div>
<div class="m2"><p>تن از خشم، لرزنده چون بید گشت</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چنین گفت کز مردم بدنژاد</p></div>
<div class="m2"><p>همانا بماند همی عدل و داد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>هر آن کز خرد،مغز دارد تهی</p></div>
<div class="m2"><p>نباشد درو شادی وفرهی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>تو شاهی،کنون پیش دستی نمای</p></div>
<div class="m2"><p>ببینیم تا چرخ را چیست رای</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>برانگیخت شبرنگ را شهریار</p></div>
<div class="m2"><p>به دست اندرون گرزه گاوسار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>درآمد به کردار کوه گران</p></div>
<div class="m2"><p>بزد گرز چون پتک آهنگران</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>نه دست سپهبد شد از زخم،خم</p></div>
<div class="m2"><p>نه نیرو شد از چنگ پیروز،کم</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>سپهبد خروشید کای شیردل</p></div>
<div class="m2"><p>نیای تواز زخم تو شد خجل</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>تو را کاندرآورد، زخم این بود</p></div>
<div class="m2"><p>ره کینه جستن نه آیین بود</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ببینی کنون زخم مردان جنگ</p></div>
<div class="m2"><p>که گردد زخونت زمین،لعل رنگ</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بگفت این و پس جنگ را زان نمود</p></div>
<div class="m2"><p>برآمد زجا اسب،مانند دود</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>برافراخت گرز صد و شصت من</p></div>
<div class="m2"><p>چوکوهی برافکند بر شاه تن</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چوسقلاب روم و دلیران شاه</p></div>
<div class="m2"><p>بدیدند کامد چو کوه سیاه</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>یکی حمله کردند با دار وگیر</p></div>
<div class="m2"><p>نهادند هریک درو تیغ و تیر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>سپهبد چو آن دید برگشت ازو</p></div>
<div class="m2"><p>بدان پنج سردار بنهاد روی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>در افکندشان پیش،همچون رمه</p></div>
<div class="m2"><p>گریزان ازو نامداران همه</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>چوباد اندر آمد به سقلی رسید</p></div>
<div class="m2"><p>خروشی به چرخ برین برکشید</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>بزد گرز برگردن بادپای</p></div>
<div class="m2"><p>روانش تو گفتی به تن در نماندش روان</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>سپهبد به زخم دگر کرد دست</p></div>
<div class="m2"><p>به هوش آمد وزود بر پای جست</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>پشوتن زقلب سپه چون بدید</p></div>
<div class="m2"><p>خروشید و تیغ از میان برکشید</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>بیامد و برآویخت با پیل تن</p></div>
<div class="m2"><p>برایشان نظاره شدند انجمن</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>چو این پنج با وی برآویختند</p></div>
<div class="m2"><p>ازو شاه و سقلاب بگریختند</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>شدند از میانه میان سپاه</p></div>
<div class="m2"><p>نهیب آمدش پیش سقلاب شاه</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چنین نازنین تیره گون گشته دشت</p></div>
<div class="m2"><p>سپهبد از آن پنج تن برنگشت</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>یکی گرز زد بر سر رزم یار</p></div>
<div class="m2"><p>تو گفتی که شد خاک را خواستار</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>گریزان شدند آن چهار دگر</p></div>
<div class="m2"><p>بدان خاک خشک و پر از خون،جگر</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>ببود آن شب و بامدادان دگر</p></div>
<div class="m2"><p>سرکوه بگرفت زرین سپر</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>غوکوس برخاست وآواز نای</p></div>
<div class="m2"><p>سپاه اندرآمد چو دریا زجای</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>جهانی کجا بود دینارگون</p></div>
<div class="m2"><p>زگرد سپه گشت زنگارگون</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>زمین را نمودند چون لاله زار</p></div>
<div class="m2"><p>پر از کشته کردند از خسته،خار</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>هوا گفتی آهن بپوشد همی</p></div>
<div class="m2"><p>زمین گفتی از خون بجوشد همی</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>زنوک سنان ها میان هوا</p></div>
<div class="m2"><p>زخاک پی اسب فرمان روا</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>ستاره تو گفتی که ریزان شده است</p></div>
<div class="m2"><p>وگر هور تابان گریزان شده است</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بدان گه که بر دشت برکینه خاست</p></div>
<div class="m2"><p>فرستاد بهمن سوی چپ و راست</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>که یکسر سپه را به جنگ آورید</p></div>
<div class="m2"><p>نخواهم کسی کو درنگ آورید</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>بکوشید تا یک تن از دشمنان</p></div>
<div class="m2"><p>نمانند زنده از این بدنشان</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>زگفتار شاه،آن سپاه بزرگ</p></div>
<div class="m2"><p>یکی گشت چون شیر و دیگر چوگرگ</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>یکی حلقه کرد آن شگفت اژدها</p></div>
<div class="m2"><p>کزآن جان مردم نیابد رها</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>یکی مارجان گیر را برفراشت</p></div>
<div class="m2"><p>یکی تیر دلدوز را برگماشت</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>چنان حلقه کردند بر سگزیان</p></div>
<div class="m2"><p>که بیرون سواری نماند از میان</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>نهادند بر تیغ برنده دست</p></div>
<div class="m2"><p>زخون یلان،خاک کردند پست</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>فرامرز با چند خویشان خویش</p></div>
<div class="m2"><p>نهادند یکبارگی پای پیش</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>بنا آخت خود از سران نامدار</p></div>
<div class="m2"><p>زخویشان پس پشت او ده سوار</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>درافتاد اندر میان سپاه</p></div>
<div class="m2"><p>چو در خرمن افتاد باد سیاه</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>بدان روی کو خنگ را ره نمود</p></div>
<div class="m2"><p>برآورد از آن روی،یکباره دود</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>از ایران سپه،مرد چندان بکشت</p></div>
<div class="m2"><p>که در دسته تیغش افشرد مشت</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>چو بگذشت یک نیمه از تیره روز</p></div>
<div class="m2"><p>زگردون فروگشت گیتی فروز</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>ازآن رو سیه مرد،هشتصد هزار</p></div>
<div class="m2"><p>درآمد یکایک چو دریای قار</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>زبس چاک چاک و زبس دار وگیر</p></div>
<div class="m2"><p>بنالید مریخ و کیوان پیر</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>تو گفتی نماندست برچرخ،هور</p></div>
<div class="m2"><p>نه در مرد،نیرو نه در باره،زور</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>زکشته چنان گشت هر دو سپاه</p></div>
<div class="m2"><p>که بر زندگان تنگ شد جایگاه</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>یکی تیغ بر کتف بانو گشسب</p></div>
<div class="m2"><p>بیامد ولیکن نیفتاد از اسب</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>تخواره چو او را بدان سان بدید</p></div>
<div class="m2"><p>باستاد بر سرش زخمی رسید</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>فراوان بکشتند از آن سرکشان</p></div>
<div class="m2"><p>چنان روز را کس ندارد نشان</p></div></div>