---
title: >-
    بخش ۴۶ - گفتار اندر نامه نوشتن فرامرز برکید هندی(و) رفتن گستهم در هند
---
# بخش ۴۶ - گفتار اندر نامه نوشتن فرامرز برکید هندی(و) رفتن گستهم در هند

<div class="b" id="bn1"><div class="m1"><p>چو بشنید از من جهان پهلوان</p></div>
<div class="m2"><p>به گل زد گلابی زنرگس روان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدادش بسی خواسته دلپذیر</p></div>
<div class="m2"><p>به نزدیک خود خواند گویا دبیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گفتار من سوی آن شهریار</p></div>
<div class="m2"><p>یکی نامه بنویس الماس وار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دبیر آنکه کافور و عنبر گرفت</p></div>
<div class="m2"><p>به کید آنگهی نامه ای برگرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخست آفرین برجهاندار کرد</p></div>
<div class="m2"><p>وز آن پس سرکید بیدارکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که این نامه از مهتر سیستان</p></div>
<div class="m2"><p>نوشته سوی کید هندوستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنزد فرامرز گرد دلیر</p></div>
<div class="m2"><p>نژاد وی از رستم زال شیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زجنگی نریمان سام سوار</p></div>
<div class="m2"><p>پدر بر پدر پهلو ونامدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کمربسته شاه کاوس کی</p></div>
<div class="m2"><p>که دارد خجسته همین یال و پی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان داور و پاک و یزدان پرست</p></div>
<div class="m2"><p>که بر شهریاران گیتی سرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دین نیاکان سر افراخته</p></div>
<div class="m2"><p>بتان را سراسر تبه ساخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهان تیره از بند و شیران اوی</p></div>
<div class="m2"><p>زپیلان فزون تر دلیران اوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نوشته چنان رفت فرمان کی</p></div>
<div class="m2"><p>که من بسپرم هند را زیر پی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر بدسگالان به راه آورم</p></div>
<div class="m2"><p>گرفته همه نزد شاه آورم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرت تاج باید که داری به سر</p></div>
<div class="m2"><p>ببایدت گردن نهادن به در</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برابر شوی جم و ضحاک را</p></div>
<div class="m2"><p>ویا سرنهی چون رهی خاک را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرت آشتی رای خیزد همی</p></div>
<div class="m2"><p>و گر دل به کینه ستیزد همی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خبرده کزین دو کدامی یکی</p></div>
<div class="m2"><p>گذر کن سوی نیک نامی یکی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرآن کو در آشتی کوبد او</p></div>
<div class="m2"><p>در کینه از وی نیاشوبد او</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دو چشم بدی را بخواباند اوی</p></div>
<div class="m2"><p>درخت بلا را نجنباند اوی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرا شاه با او نفرمود رزم</p></div>
<div class="m2"><p>همانا که او پیش سازد ز بزم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هرآن کو بلا را بجنبد زجای</p></div>
<div class="m2"><p>به نیروی دارار گیتی خدای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زتیغ نریمانش پاره کنم</p></div>
<div class="m2"><p>به کوپال گرشاسب چاره کنم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>من این دیو از لشکر بی شمار</p></div>
<div class="m2"><p>نیندیشم از دولت شهریار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من از دیو کناس وارونه گرگ</p></div>
<div class="m2"><p>وز آن کرگدن ها و ماربزرگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زنیروی یزدان نپیچیده ام</p></div>
<div class="m2"><p>چنین رزم من بی شمر دیده ام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نشسته دو روزم درین مرغزار</p></div>
<div class="m2"><p>تفرج کنان بر لب جویبار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زمانت دهم تا پیمبر رسد</p></div>
<div class="m2"><p>چوآمد تو را تیغ بر سر رسد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو نامه به سر شد دلاور به هم</p></div>
<div class="m2"><p>بفرمود کآید برش گستهم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدو داد گفتا برکید بر</p></div>
<div class="m2"><p>بگویش سخن ها همه سر به سر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو پاسخ کند زود زو بازگرد</p></div>
<div class="m2"><p>نگهدارش یئین وساز نبرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ستد نامه گستهم و بر زین نشست</p></div>
<div class="m2"><p>به بور نکو خسرو آیین نشست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بشد روز و شب تا به مرز کلیو</p></div>
<div class="m2"><p>به نیروی یزدان کیهان خدیو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خبرشد برکید هندی روان</p></div>
<div class="m2"><p>که آمد فرستاده پهلوان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گروهی پذیره فرستاد شاه</p></div>
<div class="m2"><p>فرستاده چون شد هم از گرد را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بیامد به نزدیک سالار بار</p></div>
<div class="m2"><p>یکایک ببردش سوی شهریار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چوآمد برکاخ و تخت بلند</p></div>
<div class="m2"><p>ستایش گرفت آنگهی هوشمند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زمین را ببوسید گستهم شیر</p></div>
<div class="m2"><p>بدادش پیام دلیران دلیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نوشته همان نامه دلپذیر</p></div>
<div class="m2"><p>بداد و بخواندش یکایک دلیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زریری شد از نامه رخسار او</p></div>
<div class="m2"><p>چو گل کاه شد روی گلنار او</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به گستهم گفت ای نبرده سوار</p></div>
<div class="m2"><p>چه مایه مر او را دلیران کار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زنسل که این نام بردار گرد</p></div>
<div class="m2"><p>مرو را زتخم که باید شمرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دلیران کدامند و شیران که اند</p></div>
<div class="m2"><p>به رزم اندرون شیر مردان که اند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدو گفت کای خسرو هندبار</p></div>
<div class="m2"><p>مر او را دلیران بود ده هزار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همه گرد و شیرند و شمشیر زن</p></div>
<div class="m2"><p>زهند و همه چل هزار انجمن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گرش مرد جنگی بود ده هزار</p></div>
<div class="m2"><p>همه گرد مرد وهمه نامدار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بدارد مر او را به کردار کوه</p></div>
<div class="m2"><p>به تنها زند خویش تن برگروه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مر او را زششصد من افزون عمود</p></div>
<div class="m2"><p>که آرد نبردش دمی آزمود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>یکی تیغ دارد به هنگام جنگ</p></div>
<div class="m2"><p>زالماس بران دو ده من به سنگ</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ورا نیزه آهنینش سراست</p></div>
<div class="m2"><p>سمندش یکی کوه چون پیکر است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>که او کره رخش رستم بود</p></div>
<div class="m2"><p>به گیتی سپرکش چنو کم بود</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سیه ابر شش تازی تیزتک</p></div>
<div class="m2"><p>به میدان چهل گز جهد دیورگ</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به دریا چو باد است و غران به راغ</p></div>
<div class="m2"><p>به هامون چو شاهین و طاوس باغ</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نگوید سخن نیک داند همی</p></div>
<div class="m2"><p>زخندق چهل گز جهاند همی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>یکی کوه خاراش بینی سوار</p></div>
<div class="m2"><p>بدان گرز وآن آلت کارزار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دو روز و دو شب را گر افتد به جنگ</p></div>
<div class="m2"><p>به جز گرز،چیزی ندارد به چنگ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به تنها زند لشکر صدهزار</p></div>
<div class="m2"><p>سپاهی به یک حمله زو تارمار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>فراسان دیوان گر از اسپروز</p></div>
<div class="m2"><p>رسیدند گیتی بشد تیره روز</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>فراسان بکشت و فراهان گرفت</p></div>
<div class="m2"><p>وزو این چنین ها نباید شگفت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>در آن مرز نوشاد و کناس بود</p></div>
<div class="m2"><p>همی جنگ و مکر همه یاس بود</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چنان گرگ گویا و آن کرگدن</p></div>
<div class="m2"><p>چنان مارجوشا به یال و بدن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بکشت و جهان گشت زان بد تهی</p></div>
<div class="m2"><p>وز آن پس تو را کرد خواهد رهی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نژاد وی از رستم زال زر</p></div>
<div class="m2"><p>زسام نریمان والاگهر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>زگرشسب و ز اطرط و جمشید</p></div>
<div class="m2"><p>به شادی و کندی چو تابنده شید</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پذیره شدش ناگهان نوشدار</p></div>
<div class="m2"><p>برابر کشیدش سپه ده هزار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>جهان پهلوان بود اندر سپاه</p></div>
<div class="m2"><p>به نیزه ز زین برگرفتش ز راه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به تیغ جهان گیر سام سوار</p></div>
<div class="m2"><p>بدان لشکری کرد یک یادگار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>که گویندش از یلمه تیغ تیز</p></div>
<div class="m2"><p>به آسایش رزم تا رستخیز</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>کینه جو نباشی تو با پهلوان</p></div>
<div class="m2"><p>مرنجان تن خویش و دیگر سران</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>کسی را که با او نیایی به جنگ</p></div>
<div class="m2"><p>اگر صبح سازی نباشدت ننگ</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>که خورشید با تیغ ایرانیان</p></div>
<div class="m2"><p>نبندد همانا به کینه میان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>دلیران لشکر چو بیژن دلیر</p></div>
<div class="m2"><p>زبیمش نتازد برآهوی،شیر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>زراسب جهانگیر کز تیغ اوی</p></div>
<div class="m2"><p>هژبر ژیان زو گریزد به کوی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چو گرزم که گر تیغ گیرد به چنگ</p></div>
<div class="m2"><p>به میدان او کس ندارد درنگ</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>سپاهش همه یک به یک نره شیر</p></div>
<div class="m2"><p>به تن،ژنده پیل و به زهره،دلیر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو آهو رباید سپاه تو را</p></div>
<div class="m2"><p>چو گلزارشان رزمگاه تو را</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سری زان برآید که صد مرد ازین</p></div>
<div class="m2"><p>بگویم که برخیزدت درد ازین</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو بینی تواو را بدانی که من</p></div>
<div class="m2"><p>دروغی نگفتم در این انجمن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>تو دانی که هرکو بگوید دروغ</p></div>
<div class="m2"><p>نگیرد سخن هاش در دل فروغ</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو بشنید کید دلاور سخن</p></div>
<div class="m2"><p>برآشفت با نامور انجمن</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بدو گفت اگر کوه خارا بود</p></div>
<div class="m2"><p>چو سنگ آورد آشکارا بود</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>مرا نیز چندان دلیران بود</p></div>
<div class="m2"><p>که شیران از ایشان گریزان بود</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>به هنگام جنگ و به روز شکار</p></div>
<div class="m2"><p>به میدان شتابد چو باد بهار</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>فلک سرنپیچد ز زوبین من</p></div>
<div class="m2"><p>زمین و زمان هم ز آیین من</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>من امروزت از روز فرخ کنم</p></div>
<div class="m2"><p>که از صبح آن پاسخ نو کنم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>خروش شبستان به هامون بریم</p></div>
<div class="m2"><p>به دشت خوش و پاک گلگون کنیم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>وز آن پس نگه کن که گردان سپهر</p></div>
<div class="m2"><p>زبالای سر بر که گردد به مهر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بفرمود آورده شد خوان ومی</p></div>
<div class="m2"><p>پس از خوان خورش آمد از چنگ ونی</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چوگستهم شد مست کاخ بلند</p></div>
<div class="m2"><p>سزاوار آن مهتر هوشمند</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بفرمود تا راست کردند جای</p></div>
<div class="m2"><p>گروهی نشستند با کدخدای</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>چو طهمور و دستور پاکیزه تن</p></div>
<div class="m2"><p>به اندیشه ها مهتر رای زن</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چوفیروز و بهروز شمشیر زن</p></div>
<div class="m2"><p>گلنگوی زنگی که بد از یمن</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>سمن رخ که بد پهلوان سپاه</p></div>
<div class="m2"><p>سمن بر که بد کهتر دخت شاه</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>فلارنگ شیر اوژن پیلتن</p></div>
<div class="m2"><p>چو شم دیگر آن گرد لشکرشکن</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>همه پهلوانان شه مرد شیر</p></div>
<div class="m2"><p>به هنگام کین اژدهای دلیر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>شدند انجمن آن شب دیرباز</p></div>
<div class="m2"><p>سخن رفت زان پهلو سرفراز</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>همه شب سخن های آورد بود</p></div>
<div class="m2"><p>دلیران بگفتند و خسرو شنود</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>سرانجامشان هم زکین شد سخن</p></div>
<div class="m2"><p>به رزم گران داستان شد به بن</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چو شد زعفرانی در و دشت هند</p></div>
<div class="m2"><p>چو الماس شد خاک دریای سند</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>همان گشت سیماب سرزان زکوه</p></div>
<div class="m2"><p>برآمد شب تیره زان شب ستوه</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>بفرمود شه تادر آمد دبیر</p></div>
<div class="m2"><p>زگل گشت مشکین به روی حریر</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>نوندی شب آسای و گوهرفشان</p></div>
<div class="m2"><p>دوانید و ماند از پی او نشان</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>که ای پهلوان،سرفراز ستخر</p></div>
<div class="m2"><p>که کاوس دارد به روی تو فخر</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>درودت زما باد کندآوران</p></div>
<div class="m2"><p>بزرگان سند ودلاور سران</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>وز آن پس بدان ای سر سیستان</p></div>
<div class="m2"><p>به آسان گرفتی تو هندوستان</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>مشو غره برلشکر نوشدار</p></div>
<div class="m2"><p>زنوشاد کناس مردارخوار</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>به گیتی چنان دان که مرز کلیو</p></div>
<div class="m2"><p>نشاید گرفتی به رای وبه ریو</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>سپاهم فراوان و پیلان جنگ</p></div>
<div class="m2"><p>یکایک چو گرگند وشیر وپلنگ</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>چوهندی کم آید زایرانیان</p></div>
<div class="m2"><p>سزد گر ببندد از ایشان میان</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>چنانم که ترسم من از گفتگوی</p></div>
<div class="m2"><p>هم اکنون به هامون شدم جنگجوی</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>پلنگ و دهل گور وآهو دمد</p></div>
<div class="m2"><p>زگردان که چون شیر جنگی دمد</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>درخت بلاها به جای آوریم</p></div>
<div class="m2"><p>درآن گه که در رزم پای آوریم</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>زضحاک تازی که بد ماردوش</p></div>
<div class="m2"><p>که گرشاسب برزد زهندو خروش</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>ندارد کسی با شه هند تاو</p></div>
<div class="m2"><p>زدریا نیابد همی باژ و ساو</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>کنون گر تو ساز نو آیین کنی</p></div>
<div class="m2"><p>بترسم که سردر سرکین کنی</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>بدین سان سخن های نا دلپسند</p></div>
<div class="m2"><p>نه فرخ بود زان چنان هوشمند</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>زبان خردمند گویا بود</p></div>
<div class="m2"><p>سخن یکسره مشک بویا بود</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چو بیهوده گفتن نیاید به کار</p></div>
<div class="m2"><p>همین مغز گوید سخن هوش دار</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چو سرشد همان نامه با قدر وغم</p></div>
<div class="m2"><p>بخواندند از آن پس همین گستهم</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>رسول گرانمایه را خواستند</p></div>
<div class="m2"><p>تنش را به خلعت بیاراستند</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>برون شد ز درگاه او گستهم</p></div>
<div class="m2"><p>دژم چهره بستی بر ابروش خم</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>هم ا زگرد ره شد سوی نامدار</p></div>
<div class="m2"><p>یکایک بدو راست کرد آشکار</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>سراسر چو دانسته شد کار کید</p></div>
<div class="m2"><p>بفرمود رفتن به پروا به صید</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>براند آن گرانمایه لشکر چو باد</p></div>
<div class="m2"><p>به مرز کلیو آمد آن پاکزاد</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>وز آن پس بفرمود کید بزرگ</p></div>
<div class="m2"><p>پذیره شدندش به کردار گرگ</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>به فرمان او نامور صدهزار</p></div>
<div class="m2"><p>پذیره شدن را بر آراست کار</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>چنان پیل جنگی و آشفته مرد</p></div>
<div class="m2"><p>بیامد خروشان به دشت نبرد</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>زپیلان همانا که ششصد فزون</p></div>
<div class="m2"><p>همه تن در آهن شده نیلگون</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>بیابان یکایک سپر بر سپر</p></div>
<div class="m2"><p>همه نیزه ها در هوا کرد سر</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>جهان پر شد از ناله گاودم</p></div>
<div class="m2"><p>زشیپور وآوای رویینه خم</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>زمین شد یکایک چو دریای موج</p></div>
<div class="m2"><p>به هر سو دلیران همه فوج فوج</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>چپ وراست،لشکر بیاراستند</p></div>
<div class="m2"><p>عقابان زهرگونه برخاستند</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>رده برکشیدند یک یک خروش</p></div>
<div class="m2"><p>بلرزید دشت و بگردید جوش</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>فرامرز زآن سو صفی برکشید</p></div>
<div class="m2"><p>که کیوان،زمین را به دیده ندید</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>گرانمایه بیژن سوی میمنه</p></div>
<div class="m2"><p>ابا شاه نوشاد چندین بنه</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>سوی میسره هوش ور گستهم</p></div>
<div class="m2"><p>ابا نوشدار و دلیران به هم</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>زرسب گرانمایه دلبند توس</p></div>
<div class="m2"><p>به قلب اندرون با علم بود و کوس</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>فرامرز هر سو صف آرای بود</p></div>
<div class="m2"><p>به هرگوشه چون شیر بر پای بود</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>وز آن سو صف آرای شد کید هند</p></div>
<div class="m2"><p>جهان نیلگون شد چو دریای سند</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>سوی راست طهمور اروند شاه</p></div>
<div class="m2"><p>زمین شد چو دریای جوشان سیاه</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>چو فیروز و بهروز در رزمگاه</p></div>
<div class="m2"><p>چو شه مرد چندین دلیران شاه</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>سمن رخ به پیش سپه بد به در</p></div>
<div class="m2"><p>جهان سرخ و زرد و زمین سربه سر</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>ابا جوشن و تیغ کین و سپر</p></div>
<div class="m2"><p>همی بانگ برزد چو پر خاشخر</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>نهاده سریر زبرجد به پیل</p></div>
<div class="m2"><p>زپیلان، هوا و زمین پر زنیل</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>برآن تخت برشاه هندوستان</p></div>
<div class="m2"><p>نظاره کنان لشکر سیستان</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>نخستین سمن رخ به میدان جنگ</p></div>
<div class="m2"><p>بیامد به کردا غران پلنگ</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>چو دیدش به قلب اندر آمد زرسب</p></div>
<div class="m2"><p>چو آتش سوی او جهانید اسب</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>بدو گفت کای هندی بد سگال</p></div>
<div class="m2"><p>چه نامی بدین تیغ و کوپال و یال</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>چه تازی به میدان ایران زمین</p></div>
<div class="m2"><p>ندیدی تو رزم دلیران همین</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>بپوشم تو را جامه پرنیان</p></div>
<div class="m2"><p>کزین خود نبندی به مردی میان</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>سمن رخ بدو گفت کای پارسی</p></div>
<div class="m2"><p>چو تو گشته ام نیزه در بار سی</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>سمن رخ منم دختر شاه کید</p></div>
<div class="m2"><p>که افکنده ام چون تو بسیار صید</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>به مردی کسی پشت من بر زمین</p></div>
<div class="m2"><p>نیارد به میدان و هنگام کین</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>زرسب از سخن های او بردمید</p></div>
<div class="m2"><p>زکوهه عمود گران برکشید</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>سمن رخ برآورد با او عمود</p></div>
<div class="m2"><p>زمین شد پرآتش، هوا پر زدود</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>چکاچاک برشد به هرمزد و ماه</p></div>
<div class="m2"><p>زدو روی کردند گردان نگاه</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>خمیده زکوپال شد پایشان</p></div>
<div class="m2"><p>برآمد سنان،رفت کوپالشان</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>نیامد سنان نیزه هم کارگر</p></div>
<div class="m2"><p>برآمد یکی تیغ پرخاشخر</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>به شمشیر بران برآمد نبرد</p></div>
<div class="m2"><p>چرنگیدن تیغ و آشوب مرد</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>چو تنگ اندر آمد دلاور زرسب</p></div>
<div class="m2"><p>گریز اندر آورد و پیچید اسب</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>کمند اندر آورد و زد خم به خم</p></div>
<div class="m2"><p>سمن رخ دوان در پی او دژم</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>چو بفکنده شد حلقه در چین به چین</p></div>
<div class="m2"><p>به بند اندر افتاد دخت گزین</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>بپیچید وافکند بر روی خاک</p></div>
<div class="m2"><p>درآمد به خاک آن تن تابناک</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>ندم گرانمایه جنگی زرسب</p></div>
<div class="m2"><p>فرو جست مانند آذر گشسب</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>ببستش دو بازی گرد جوان</p></div>
<div class="m2"><p>کشان آوریدش بر پهلوان</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>برآمد ز ایران سپه بانگ و جوش</p></div>
<div class="m2"><p>زکوس ودهل ها برآمد خروش</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>فرامرز گفتش سمن رخ تویی</p></div>
<div class="m2"><p>که داری به میدان رگ پهلوی</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>کله خودش از سرفکندند خوار</p></div>
<div class="m2"><p>پدیدن آمد آن گوهر آبدار</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>رخی چون بهار ولبی همچو نوش</p></div>
<div class="m2"><p>به گرد سنان لشکرستان به جوش</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>بفرمود کین را بسازید بند</p></div>
<div class="m2"><p>ولیکن چو جانش کنید ارجمند</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>زاندوه دختر،دل هندوان</p></div>
<div class="m2"><p>بپیچید هر یک به جایی نوان</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>گلنگوی جنگی چو دریای قیر</p></div>
<div class="m2"><p>دوان شد زلشکر به صد دارو گیر</p></div></div>