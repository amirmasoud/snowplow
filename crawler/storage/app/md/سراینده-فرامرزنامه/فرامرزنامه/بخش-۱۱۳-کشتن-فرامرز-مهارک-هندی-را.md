---
title: >-
    بخش ۱۱۳ - کشتن فرامرز، مهارک هندی را
---
# بخش ۱۱۳ - کشتن فرامرز، مهارک هندی را

<div class="b" id="bn1"><div class="m1"><p>مهارک چو دید آن سپاه نبرد</p></div>
<div class="m2"><p>گریزا شد آهنگ دروازه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی خواست کان شهر سازد حصار</p></div>
<div class="m2"><p>مگر رسته گردد از آن گیر ودار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زکارش کسی با فرامرز گفت</p></div>
<div class="m2"><p>سپهدار از این قصه اندر شگفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عنان را به جنگ فرامرزداد</p></div>
<div class="m2"><p>بتازید و شد از پی بدنژاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان شد که بگذشت ازو نامدار</p></div>
<div class="m2"><p>سر راه بگرفت شیر شکار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهارک چو تنگ اندر آمد به روی</p></div>
<div class="m2"><p>برآویخت با او یل جنگجوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سنان را همانگه برو کرد راست</p></div>
<div class="m2"><p>خروش از سوار دلاور بخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بزد نیزه بر پشت اسب نبرد</p></div>
<div class="m2"><p>به زیر اندر آورد او را به گرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همیدون به نیزه برافراختش</p></div>
<div class="m2"><p>چو بر باب زن مرغ برداشتش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو سوگند یادآمدش در زمان</p></div>
<div class="m2"><p>زدش بر زمین همچو کوه گران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سنان از روانش برآورد دود</p></div>
<div class="m2"><p>تو گفتی مهارک به گیتی نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین است کردار این تیز گرد</p></div>
<div class="m2"><p>یکی تاج بخشد یکی دار برد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خردمند اندر جهان دل نبست</p></div>
<div class="m2"><p>که آخر زود بی گمانش زدست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مهارک چو شد کشته و ناپدید</p></div>
<div class="m2"><p>خبر زو به شهر بزرگان رسید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی انجمن نامداران شهر</p></div>
<div class="m2"><p>کسی را کجا از خرد بود بهر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به زنهار پیش سپهبد شدند</p></div>
<div class="m2"><p>به رای هشیوار وبخرد شدند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به زاری بگفتند با پهلوان</p></div>
<div class="m2"><p>که ای شیردل مهتر کامران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تودانی که ما سخت بیچاره ایم</p></div>
<div class="m2"><p>همه بر تن خود ستمکاره ایم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ببخشا و ترسان شو از روزگار</p></div>
<div class="m2"><p>مباش ایمن از چرخ ناسازگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که گاهیت شادی دهد گاه غم</p></div>
<div class="m2"><p>گهی داد پیش آورد گه ستم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فرامرز بشنید و بنواختشان</p></div>
<div class="m2"><p>به خوبی همه کار برساختشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکی هندوی بود نامش تهون</p></div>
<div class="m2"><p>که از رای او شیر گشتی زبون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ورا کرد بر شهر کشمیر شاه</p></div>
<div class="m2"><p>برآورد نام بزرگی به ماه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دو هفته در آن مرز بد شادکام</p></div>
<div class="m2"><p>شب وروز با باده و رود و جام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وزآن جا به قنوج شد با سپاه</p></div>
<div class="m2"><p>به شاهی به سربر نهاده کلاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پذیره شدش با بزرگان خویش</p></div>
<div class="m2"><p>ابا کوس وپیلان زاندازه بیش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نبیره زنان هفت منزل به راه</p></div>
<div class="m2"><p>برفتند پیش یل رزمخواه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو روی فرامرز یل را بدید</p></div>
<div class="m2"><p>پیاده شد و آفرین گسترید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بزرگان زاسبان فرود آمدند</p></div>
<div class="m2"><p>زبان ها همه پر درود آمدند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سپهبد فرودآمداز بارگی</p></div>
<div class="m2"><p>چو با هم رسیدند یکبارگی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گرفتند مر یکدگر را کنار</p></div>
<div class="m2"><p>نشستند با رامش و میگسار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به شادی بر ایشان گذرکرد روز</p></div>
<div class="m2"><p>چو درکه نهان گشت گیتی فروز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بخفتند خرم،بی اندوه ودرد</p></div>
<div class="m2"><p>چو خورشید بر گنبد لاجورد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زشادی بیاراست گیتی به رنگ</p></div>
<div class="m2"><p>زرنگش همه کوه شد لاله رنگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نشستند بر باره راه جوی</p></div>
<div class="m2"><p>سوی شهر قنوج کردند روی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همه شهر قنوج و بی راه راه</p></div>
<div class="m2"><p>ببستند آذین به آیین و گاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به شادی سوی بارگاه آمدند</p></div>
<div class="m2"><p>به دل،خرم و نیکخواه آمدند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یکی بزمگه ساخت رای گزین</p></div>
<div class="m2"><p>که بر وی حسد برد خلد برین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سمن برنگاران خورشید روی</p></div>
<div class="m2"><p>بتان پری چهره مشک بوی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به رامش همه عود و ساغر به دست</p></div>
<div class="m2"><p>دو نرگس همه دل ربا نیم مست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هوا پرخروش و زمین پر درود</p></div>
<div class="m2"><p>همی داد ناهید،جان را درود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گل ونرگس و سنبل و نسترن</p></div>
<div class="m2"><p>به هر جای بر توده بد یاسمن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سپهر اندر آن بزمگه خیره ماند</p></div>
<div class="m2"><p>زبس خرمی،جان همی برفشاند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گرفتند و خوردند و گشتند مست</p></div>
<div class="m2"><p>خوش این زندگانی گرآید به دست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بدین گونه یک ماه با کام دل</p></div>
<div class="m2"><p>ببودند شادان به آرام دل</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سر مه، جهان پهلوان کرد ساز</p></div>
<div class="m2"><p>زهندوستان آنچه آید فراز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زپیل و زفیروزه و تخت و تاج</p></div>
<div class="m2"><p>زدیبا و اطلس هم از عاج وساج</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زلؤلؤ واز گوهر و بوی مشک</p></div>
<div class="m2"><p>زیاقوت و عنبر زکافور خشک</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کنیزان کشمیری و خلخی</p></div>
<div class="m2"><p>غلامان مه روی با فرخی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زچینی و هندی واز بربری</p></div>
<div class="m2"><p>برآراسته خوبی وبرتری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زداور واز زعفران،بی حساب</p></div>
<div class="m2"><p>زهندی همه جام ها مشک ناب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هم از تیغ هندی و گرز گران</p></div>
<div class="m2"><p>زپرمایه اسپان و از گوهران</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همان خود و بر گستوان بی شمار</p></div>
<div class="m2"><p>زپیلان جنگی ده و دو هزار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چنین باج هندوستان سر به سر</p></div>
<div class="m2"><p>دو ساله بر شاه فیروزگر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>فرستاد با صدهزار آفرین</p></div>
<div class="m2"><p>یل شیردل پهلوان گزین</p></div></div>