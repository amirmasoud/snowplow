---
title: >-
    بخش ۱۸۳ - خبر یافتن فرامرز از گرفتن بهمن،شهر سیستان و گرفتار شدن زال
---
# بخش ۱۸۳ - خبر یافتن فرامرز از گرفتن بهمن،شهر سیستان و گرفتار شدن زال

<div class="b" id="bn1"><div class="m1"><p>فرامرز را آگهی شد که زال</p></div>
<div class="m2"><p>بماندست چون مرغ بی پر وبال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفتار بر دست بهمن چنان</p></div>
<div class="m2"><p>که مرگ آرزو آیدش هر زمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مر او را زپولاد،زندان و بند</p></div>
<div class="m2"><p>به تن،سوگوار و به دل،درد مند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد و شصت من بند برپای او</p></div>
<div class="m2"><p>یکی آهنین قفس جای او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شده سیستان سر به سر چون غبار</p></div>
<div class="m2"><p>سرای بزرگان شده کشت زار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرامرز یل پیرهن چاک کرد</p></div>
<div class="m2"><p>زدیده سرشک از برخاک کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همی گفت کای چرخ ناسازگار</p></div>
<div class="m2"><p>برآوردی از ماه پیکر دمار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی را کجا بخت بنمود پشت</p></div>
<div class="m2"><p>شود روزگارش به یک ره درشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی را که آید زمانه فراز</p></div>
<div class="m2"><p>نگردد به مردی ونیرنگ باز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه چاره سگالم که لشکر نماند</p></div>
<div class="m2"><p>همان مایه و گنج و گوهر نماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدین مایه مردم کجا با من است</p></div>
<div class="m2"><p>چه کوشم که گیتی پر از دشمن است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو خویشان ازو آگهی یافتند</p></div>
<div class="m2"><p>خروشان همه تیز بشتافتند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه پهلوان زادگان زمان</p></div>
<div class="m2"><p>غریوان و مرجان به گل بر زنان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سه روز اندر آن سوگ بودند و درد</p></div>
<div class="m2"><p>کزیشان کسی را نماند به خورد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چهارم که روی هوا تیره گشت</p></div>
<div class="m2"><p>وز آن تیرگی دیده ها خیره گشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سگالش چنین کرد با خواهران</p></div>
<div class="m2"><p>زخویشان،تنی چند نام آوران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که از ما کنون بخت برگاشت رو</p></div>
<div class="m2"><p>چنین خیره شد دشمن کینه جو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برین دشت،یک باره کوشش کنیم</p></div>
<div class="m2"><p>زمین را زخون باز جوشش کنیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو یکبارگی کشتن آراستن</p></div>
<div class="m2"><p>زهرکس همی یاوری خواستن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو دستان فرخ گرفتارشد</p></div>
<div class="m2"><p>گل زندگی را همی خارشد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیابم بدین روی گیتی درنگ</p></div>
<div class="m2"><p>بمیرم به نام و نمانم به ننگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چورفت آفتاب از جهان،شب بود</p></div>
<div class="m2"><p>که هم مرگ را پیش او تب بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگفت این ولشکر برآمد به هم</p></div>
<div class="m2"><p>جهان،پر ستم گشت گردون،دژم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدان چندگه کو با بابل بماند</p></div>
<div class="m2"><p>زهر سو سواری که بودش بخواند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فرامرز یل را بدان روزگار</p></div>
<div class="m2"><p>زلشکر که او داشت هفده هزار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زکابل شب تیره برخاست غو</p></div>
<div class="m2"><p>برون رفت با نامداران گو</p></div></div>