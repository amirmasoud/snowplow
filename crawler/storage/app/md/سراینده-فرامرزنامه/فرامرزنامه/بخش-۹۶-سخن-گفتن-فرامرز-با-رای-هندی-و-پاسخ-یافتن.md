---
title: >-
    بخش ۹۶ - سخن گفتن فرامرز با رای هندی و پاسخ یافتن
---
# بخش ۹۶ - سخن گفتن فرامرز با رای هندی و پاسخ یافتن

<div class="b" id="bn1"><div class="m1"><p>چنین گفت با خسرو هندوان</p></div>
<div class="m2"><p>که ای نامور شاه روشن روان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرامرز رستم یل بافرین</p></div>
<div class="m2"><p>مرا گفت رو نزد شاه گزین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگویش که ای نامداربزرگ</p></div>
<div class="m2"><p>سزد گر نه تندی به گرد سترگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که ما را شهنشاه ایران زمین</p></div>
<div class="m2"><p>جهاندار کیخسرو پاک دین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین داد فرمان که برهند وسند</p></div>
<div class="m2"><p>گذر کن به نیروی چینی پرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نخستین که رفتی بگو رای را</p></div>
<div class="m2"><p>که از مردمی برمکش پای را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تودانی که شاهان ایران زمین</p></div>
<div class="m2"><p>زجم و ز تهمورث بافرین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زشاه آفریدون فرخ نژاد</p></div>
<div class="m2"><p>چنین تا منوچهر و تا کی قباد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که بودند یکسر نیاکان من</p></div>
<div class="m2"><p>بزرگان گیتی به هر انجمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روان بوده فرمانشان در جهان</p></div>
<div class="m2"><p>چه بر چین و روم و چه بر هندوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به فرجهاندار کیهان خدای</p></div>
<div class="m2"><p>جهان آفریننده رهنمای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من امروز از ایشان همه برترم</p></div>
<div class="m2"><p>برای بزرگان تواناترم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فزونم به گنج و به فر وهنر</p></div>
<div class="m2"><p>همیدون به گردان پرخاشخر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کنون ای شهنشاه ودارای هند</p></div>
<div class="m2"><p>منم بر همه سروران ارجمند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرآیی به درگاه حق بنده وار</p></div>
<div class="m2"><p>خرد کار بندی و ادراک،یار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زسر بفکنی بیشی و سروری</p></div>
<div class="m2"><p>گزینی برین مهتری چاکری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به تو ماند این مرز و گنج روان</p></div>
<div class="m2"><p>زتو دور شد خنجر پهلوان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وگرنه سپاه من از مرز هند</p></div>
<div class="m2"><p>هم از بوم کشمیر تا مرز سند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به نیروی دادار یزدان پاک</p></div>
<div class="m2"><p>برآرنده چرخ گردنده خاک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ببرم پیت را به هندوستان</p></div>
<div class="m2"><p>نماند یکی گل در این بوستان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زجان سپاهت برآرم دمار</p></div>
<div class="m2"><p>پشیمانی آنگه نیاید به کار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از آنجا چومن آمدم پیش تو</p></div>
<div class="m2"><p>چنان چون سزد نامور خویش تو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فرستادم اول کیانوش را</p></div>
<div class="m2"><p>خردمند و بیدار و خاموش را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به گفتار چرب و سخن های نرم</p></div>
<div class="m2"><p>به اندرز شیرین وآوای نرم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که گر زین بزرگی بگرداندت</p></div>
<div class="m2"><p>زپرخاش واز کینه برهاندت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نگفتی سخن هیچ بر راه راست</p></div>
<div class="m2"><p>به پاسخ بدان گونه دادی که خاست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از آن پس که برگشت از پیش تو</p></div>
<div class="m2"><p>به دل رنج از گفته و کیش تو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سپه کردی و کینه آراستی</p></div>
<div class="m2"><p>بدیدی همی آنچه خود خواستی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو را گویم ای مهتر هندوان</p></div>
<div class="m2"><p>که اندیشه دارم به روشن روان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سبکباری و تندی از شهریار</p></div>
<div class="m2"><p>نه خوب آید از مردم هوشیار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شنیدی همانا که بر لشکرت</p></div>
<div class="m2"><p>همان بوم و برشهر و بر کشورت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چه آمد ز گرز جهان پهلوان</p></div>
<div class="m2"><p>وزآن نامداران فرخ گوان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چوآمد بدیدی همه نیک وبد</p></div>
<div class="m2"><p>کنون آن گزین کت پسندد خرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو را گاو مردی به چرم اندر است</p></div>
<div class="m2"><p>از آنت چنین داوری در سر است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>روا باشد از کین و رزم آوری</p></div>
<div class="m2"><p>پس از کین بسیار رزم آوری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از آن رزم ها دل بپرداختی</p></div>
<div class="m2"><p>همه کار بر آرزو ساختی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که از نو دگر لشکر آورده ای</p></div>
<div class="m2"><p>درفش بزرگی برآورده ای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زکار تو اندیشه ننمود چهر</p></div>
<div class="m2"><p>نبینم تو را برتن خویش مهر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنین گفت شیر ژیان با پلنگ</p></div>
<div class="m2"><p>که بر غرم چون روز شد تار وتنگ</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به نیک و بد کار خود ننگرد</p></div>
<div class="m2"><p>بیاید روان پیش ما بگذرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو را هم بدان گونه بینم همی</p></div>
<div class="m2"><p>خرد در سر تو نبینم همی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پس اندیشه او بدین کار کرد</p></div>
<div class="m2"><p>به پاسخ فروبایدت یاد کرد</p></div></div>