---
title: >-
    بخش ۱۲۶ - رفتن فرامرز به جزیره فیل گوشان
---
# بخش ۱۲۶ - رفتن فرامرز به جزیره فیل گوشان

<div class="b" id="bn1"><div class="m1"><p>دگر باره افکند کشتی درآب</p></div>
<div class="m2"><p>روان کرد مانند باد از شتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سه روز و سه شب ماند کشتی روان</p></div>
<div class="m2"><p>فرامرز گردنکش پهلوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چهارم سوی فیل گوشان رسید</p></div>
<div class="m2"><p>سپه را به سوی جزیره کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جزیره بد آن هم نکوتر به جای</p></div>
<div class="m2"><p>بسی اندرو باغ و کشت و سرای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گروهی کجا فیل گوشان بدند</p></div>
<div class="m2"><p>به مردانگی سخت کوشان بدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یلانی به بالا به مانند ساج</p></div>
<div class="m2"><p>به تن،آبنوس و به رخسار عاج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زسر تاقدم،گوش همچون سپر</p></div>
<div class="m2"><p>بدن ها همه همچو پیلان نر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه دیده هاشان به کردار نیل</p></div>
<div class="m2"><p>ازیشان گریزان شده ژنده پیل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکایک ابا آلت و ساز زرم</p></div>
<div class="m2"><p>به نزدیکشان رزم، خوش تر ز بزم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سپهدارشان نامداری بزرگ</p></div>
<div class="m2"><p>بداندیش و خودکام مردی سترگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زنانشان به خوبی به کردارماه</p></div>
<div class="m2"><p>فروهشته از سرو،جعد سیاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به رخ،ارغوان و به نرگس،دژم</p></div>
<div class="m2"><p>به ابرو،کمان و به بینی،قلم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بتان سهی قد خورشید روی</p></div>
<div class="m2"><p>نگاران سیمین تن مشک بوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جهان پهلوان گرد گیتی گشای</p></div>
<div class="m2"><p>ابا لشکر و کوس و هندی درای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو تنگ اندرآمد سوی آن زمین</p></div>
<div class="m2"><p>بدو گفت ملاح کای پاک دین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از این مرز،مارا بباید گذشت</p></div>
<div class="m2"><p>نباید غنودن بدین کوه ودشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که این سهمگین جایگاهی بود</p></div>
<div class="m2"><p>مگرمان به یزدان پناهی بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کزین مرز،آسوده دل بگذریم</p></div>
<div class="m2"><p>سزد گر بدین بوم وبر نگذریم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فرامرز گفتش که این غم ز چیست</p></div>
<div class="m2"><p>بدین گونه بیم تو از ترس کیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنین گفت با او جهان دیده پیر</p></div>
<div class="m2"><p>که ای گرد پیل افکن و شیر گیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جزیریست این چارصد میل بیش</p></div>
<div class="m2"><p>چو پهنا درازیش آید به پیش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سپاهی بدو اندرون بی شمار</p></div>
<div class="m2"><p>همی رزم جویان ناپاک وار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به چهر و به دیدار مانند دیو</p></div>
<div class="m2"><p>همه ساله با کوشش و با غریو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جهاندیده کانجا بگسترده کام</p></div>
<div class="m2"><p>کجا فیل گوشانشان گفت نام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به بالا زکوه سیه برترند</p></div>
<div class="m2"><p>زباد شمالی تکاورترند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همه نیزه هاشان بودآهنین</p></div>
<div class="m2"><p>زکوپال ایشان بلرزد زمین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به تن،باد،پاشان بود همچو کوه</p></div>
<div class="m2"><p>که از لعلشان کوه گردد ستوه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنین مردمانی زمردم،بری</p></div>
<div class="m2"><p>رمنده زمردم چو دیو و پری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه مرغ هوا را بدین جا گذر</p></div>
<div class="m2"><p>نه پیل و نه دیو ونه شیران نر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>توبا این سپه نزد ایشان شوی</p></div>
<div class="m2"><p>همان دم زکرده پشیمان شوی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به هریک از ین لشکر نامدار</p></div>
<div class="m2"><p>همانا کزیشان بود ده هزار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو در خون چندین سر سرفراز</p></div>
<div class="m2"><p>شوی گر به بیشی نداری نیاز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زمن بشنواین پند را کار بند</p></div>
<div class="m2"><p>نشاید که یابی در اینجا گزند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زکاری که رنج دل آید پدید</p></div>
<div class="m2"><p>خردمند از آن کار دوری گزید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سپهبد چو بشنید از وی سخن</p></div>
<div class="m2"><p>چنین داد پاسخ به مرد کهن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که ای پیر پاکیزه پاک دین</p></div>
<div class="m2"><p>اگر یار باشد جهان آفرین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به فر جهاندار کیهان خدای</p></div>
<div class="m2"><p>سرانشان چنان اندر آرم زپای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که شیر ژیان گور پی خسته را</p></div>
<div class="m2"><p>ویا باز بر کبک پر بسته را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدان تا به گیتی بسی روزگار</p></div>
<div class="m2"><p>بماند زما این سخن یادگار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مرا ایزد از بهر رنج آفرید</p></div>
<div class="m2"><p>نه از بهر بیشی و گنج آفرید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بدان تا که از رنج،نام آورم</p></div>
<div class="m2"><p>هم از رنج وازداد،کام آورم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>منم تا بوم زنده،جویای نام</p></div>
<div class="m2"><p>به مردی برآورده ازنام،کام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خردمند گوید که انجام کار</p></div>
<div class="m2"><p>برآید اگر کردن از روزگار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ببینم نکورو که تن، مرگ راست</p></div>
<div class="m2"><p>نترسد زمرگ آن که او نام خواست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>وزین نامداران گردان من</p></div>
<div class="m2"><p>سرافراز شیران و مردان من</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نمیرد کسی تا نیاید زمان</p></div>
<div class="m2"><p>بدان کار خیره مگردان زبان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>توای پیر ملاح پاکیزه هوش</p></div>
<div class="m2"><p>به گیتی سوی من همی دار گوش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>که بی کام وامر خداوند بخت</p></div>
<div class="m2"><p>نیفتند یکی برگ سبز ازدرخت</p></div></div>