---
title: >-
    بخش ۱۰۸ - گذشتن فرامرز از آب و رزم کردن با مهارک
---
# بخش ۱۰۸ - گذشتن فرامرز از آب و رزم کردن با مهارک

<div class="b" id="bn1"><div class="m1"><p>چو خورشید بر چرخ گشت آشکار</p></div>
<div class="m2"><p>جهان کرد روشن به رنگ بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرامرزیل ساز رفتن گرفت</p></div>
<div class="m2"><p>به دل اندر اندیشه کردن گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که بر ما گر ایشان کمین آورند</p></div>
<div class="m2"><p>به آب اندرون رخ به چین آورند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نباشد بجز کامه هندوان</p></div>
<div class="m2"><p>به نامردی از ما بپرد روان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خردمند گوید به هنگام جنگ</p></div>
<div class="m2"><p>شتاب آوریدن به جای درنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه انده و رنج بارآورد</p></div>
<div class="m2"><p>پشیمانی و غم به کار آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به اندیشه بفکند نزدیک رای</p></div>
<div class="m2"><p>بدان تا ببیند در آن کار رای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدوگفت رای ای سرافراز گرد</p></div>
<div class="m2"><p>تواین جاودان را مپندار خورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که ایشان بداندیش و بد گوهرند</p></div>
<div class="m2"><p>به افسون ونیرنگ کین آورند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در این کار،اندیشه باید بسی</p></div>
<div class="m2"><p>ره آب پرسیدن از هرکسی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سگالید باید یکی چاره ای</p></div>
<div class="m2"><p>کزان ناید از پیش بیغاره ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگر کان سپاه گران پرشتاب</p></div>
<div class="m2"><p>فراتر شود از لب رود آب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو آنگه گذرکن بدین آب تیز</p></div>
<div class="m2"><p>که دشمن زپیش تو گیرد گریز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سپهبد چنین پاسخش داد باز</p></div>
<div class="m2"><p>که ای شاه دانای گردن فراز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنین دان که هرکس که او نام جست</p></div>
<div class="m2"><p>به خون شست رخسار خود از نخست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه از آب و آتش بپرهیزد او</p></div>
<div class="m2"><p>زتیغ اجل نیز نگریزد او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو گر در آید زمانه فراز</p></div>
<div class="m2"><p>نگردد به مردی و پرهیز باز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنین گفت روشن دل رهنما</p></div>
<div class="m2"><p>که گر نام جویی مترس از بلا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من این رزم را چاره پیش آورم</p></div>
<div class="m2"><p>کزین آب،بی رنج دل بگذرم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بفرمود تا زان درخت بلند</p></div>
<div class="m2"><p>فراوان ببرند وگردآورند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از آن پس ببندند بر یکدگر</p></div>
<div class="m2"><p>فشاندند خاشاکشان بر زبر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فراوان ازین گونه برساختند</p></div>
<div class="m2"><p>چو کشتی به آب اندر انداختند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نشستند بر هر یکی زان هزار</p></div>
<div class="m2"><p>کمان ور سپردار و مردان کار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به اسپان برافکنده بر گستوان</p></div>
<div class="m2"><p>دوان گشته مانند کوه روان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فرامرز گردافکن نامدار</p></div>
<div class="m2"><p>چو تنگ اندرآمد سوی رودبار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زپشته،مهارک زمین برگشاد</p></div>
<div class="m2"><p>بیامد سپاهش به کردار باد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به تیر و به زوبین زهر آبدار</p></div>
<div class="m2"><p>بکشتند از ایرانیان،بی شمار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زخون،آب دریا چو شنگرف گشت</p></div>
<div class="m2"><p>بدرید بانگ یلان کوه ودشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سپهبد چنین گفت با سرکشان</p></div>
<div class="m2"><p>که یکسر برآرید بر زه کمان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر ایشان ببارید همچون تگرگ</p></div>
<div class="m2"><p>زابر کمان،تیر باران مرگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سپه چون به ترکش درآورد چنگ</p></div>
<div class="m2"><p>رها گشت از شست تیر خدنگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زپیکان پولاد جوشن گذار</p></div>
<div class="m2"><p>زمین بر لب آب شد لاله زار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو از تیر،آن رزم را ساختند</p></div>
<div class="m2"><p>لب آب از ایشان بپرداختند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به هامون برآمد سپهبد زآب</p></div>
<div class="m2"><p>سپاه از پی او همه با شتاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو شیران نشستند بر بارگی</p></div>
<div class="m2"><p>کشیدند خنجر به یکبارگی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سپاه از دورویه رده برزدند</p></div>
<div class="m2"><p>به هم نیزه و تیر و خنجر زدند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به هندوسپه بد چو مور و ملخ</p></div>
<div class="m2"><p>کشیده به هامون چو بر کوه یخ</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فرامرز گردنکش رزمخواه</p></div>
<div class="m2"><p>به نیزه برآمد به آوردگاه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به هر حمله زان نیزه جان ستان</p></div>
<div class="m2"><p>فکندی دو صد شیردل را روان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فراوان بیفکند مرد و ستور</p></div>
<div class="m2"><p>برآورد از لشکر هند شور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زهندو یکی شیر دل پیش صف</p></div>
<div class="m2"><p>به سان هیون بر لب آورده کف</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دلاور سواری به کردار کوه</p></div>
<div class="m2"><p>زمین از گرانیش گشته ستوه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خروشید چون پهلوان را بدید</p></div>
<div class="m2"><p>یکی خنجر آبگون برکشید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بیامد بگردید با پهلوان</p></div>
<div class="m2"><p>چو پیل دمان یا هژبر ژیان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بینداخت زهر آبگون خنجرش</p></div>
<div class="m2"><p>که از تن ببرد همه پیکرش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سپر بر سر آورد آن نامدار</p></div>
<div class="m2"><p>بزد آن چنان تیغ زهرآبدار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سپرگشت از ضرب آن تیغ تیز</p></div>
<div class="m2"><p>دو نیمه به روز نبرد و ستیز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سپهبد یکی تیغ زد بر سرش</p></div>
<div class="m2"><p>به سختی بیفتاد خود از سرش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>برآهیخت هندو ازین گونه گرز</p></div>
<div class="m2"><p>فروهشت چون گرز بالای برز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پیاده درآویخت با پهلوان</p></div>
<div class="m2"><p>ستیزنده هندی تیره روان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بیامد به نزد سپهدار گرد</p></div>
<div class="m2"><p>گرفت آن کمربند جنگی گرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کش از پشت زین در رباید مگر</p></div>
<div class="m2"><p>سپهبد گرفتش بر ویال و سر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بپیچد چون گردن گوسفند</p></div>
<div class="m2"><p>ویاگور در جنگ شیران،نژند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بکند از تنش سر به کردار گوی</p></div>
<div class="m2"><p>دو لشکر بمانده شگفت اندروی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بینداخت آن سرسری هندوان</p></div>
<div class="m2"><p>زهولش بترسید جمله روان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مهارک چو دید آنچنان دستبرد</p></div>
<div class="m2"><p>به ایران سپه بر یکی حمله برد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>زگرد سپه تیره شد روی روز</p></div>
<div class="m2"><p>گریزان شد از چرخ،گیتی فروز</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>همانگاه تیره شب اندر رسید</p></div>
<div class="m2"><p>جهان،چادر تا بر سرکشید</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>زهم بازگشتند هردو سپاه</p></div>
<div class="m2"><p>نبد گاه وبیگاه آوردگاه</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>مهارک چو تیره شب آمد پدید</p></div>
<div class="m2"><p>سپه را برآن کوه گرد آورید</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>برآن بد که آن شب شبیخون کند</p></div>
<div class="m2"><p>زدشمن،در ودشت پرخون کند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>سواری فرستاد زان سو به راه</p></div>
<div class="m2"><p>به کارآگهی نزد ایران سپاه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>طلایه بداند که چنداست و چیست</p></div>
<div class="m2"><p>زلشکر که خفتست و بیدارکیست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بیامد نگه کرد هرسو بسی</p></div>
<div class="m2"><p>طلایه ندید او زایران کسی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>فرامرز،تنها لب رود و دشت</p></div>
<div class="m2"><p>همی از پاس لشکر بگشت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>پژوهنده هندو از آن سرفراز</p></div>
<div class="m2"><p>نبود آگه اندر شب دیرباز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شبی بود بس تیره چون آبنوس</p></div>
<div class="m2"><p>نه آوای اسب و نه آواز کوس</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>سپهبد چو نزدیک هندو رسید</p></div>
<div class="m2"><p>سمندش خروشید واندر چمید</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>همیدون زبالای هندو خروش</p></div>
<div class="m2"><p>بیامد فرامرز بگشاد گوش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>خدنگی به هنجار آواز اسب</p></div>
<div class="m2"><p>بینداخت برسان آذرگشسب</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بزد بر سر هندو تیره جان</p></div>
<div class="m2"><p>همانگه برون رفتش از تن روان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>مهارک زمانی همی بود دیر</p></div>
<div class="m2"><p>زکار فرستاده برگشت سیر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چه هندو نیامد به نزدیک او</p></div>
<div class="m2"><p>پر از درد شد جان تاریک او</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بدانست کو را از ایران سپاه</p></div>
<div class="m2"><p>بدآمد به سر،کار گشتش تباه</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>سرش گشت پردرد و دل پر زکین</p></div>
<div class="m2"><p>به ابرو درآورد از خشم،چین</p></div></div>