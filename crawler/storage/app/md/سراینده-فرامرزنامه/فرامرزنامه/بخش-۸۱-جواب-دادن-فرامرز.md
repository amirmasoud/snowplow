---
title: >-
    بخش ۸۱ - جواب دادن فرامرز
---
# بخش ۸۱ - جواب دادن فرامرز

<div class="b" id="bn1"><div class="m1"><p>فرامرز گفت این درخت آدمیست</p></div>
<div class="m2"><p>همین چار گوهر تو را همدمیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم از آتش وآب و وز باد و خاک</p></div>
<div class="m2"><p>درختی بدین گونه رستست پاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه شاخ او رنگ رنگست نیز</p></div>
<div class="m2"><p>چو تازی و ترکی و زنگست نیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو رومی چو کشمیری و دلبر است</p></div>
<div class="m2"><p>گر ایزد پرست است گر بت پرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که قیوم پاکست پرودگار</p></div>
<div class="m2"><p>بدین سان درختی بیاوردبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه عقل وهوش وهمه تاب ونوش</p></div>
<div class="m2"><p>دهد دست و پای و دگر چشم وگوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی را چو من دل ببخشد به خود</p></div>
<div class="m2"><p>سری چون تو هرگز نیارد به خود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدان تا به عقبی مرا خوش کند</p></div>
<div class="m2"><p>تو را دیده ودل برآتش کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترا چاره گفتم به رنج آمدی</p></div>
<div class="m2"><p>بدین گفته خود به رنج آمدی</p></div></div>