---
title: >-
    بخش ۱۴۵ - نهادن فرامرز،پر سیمرغ را برآتش
---
# بخش ۱۴۵ - نهادن فرامرز،پر سیمرغ را برآتش

<div class="b" id="bn1"><div class="m1"><p>بیاورد پس مهتر تیز ویر</p></div>
<div class="m2"><p>از آن پر و لختی به پیکان تیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برافروخت با عود آن را بسوخت</p></div>
<div class="m2"><p>زناگاه روی هوا برفروخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دید از هوا آتش و تیره دود</p></div>
<div class="m2"><p>بیامد به نزد فرامرز زود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشست از بر خاک و پوزش گرفت</p></div>
<div class="m2"><p>فرامرز و مانده اندر شگفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو دیدش دژم روی و دل مستمند</p></div>
<div class="m2"><p>بپرسید از او کای یل هوشمند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه آمد به رویت زگیتی ستم</p></div>
<div class="m2"><p>چه بودت کزین گونه گشتی دژم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه کار است اکنون به من بازگو</p></div>
<div class="m2"><p>که از غم،تو را شادی آرم به رو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که من مهربانم به زال دلیر</p></div>
<div class="m2"><p>همان رستم پیلتن نره شیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دگر هرکه از تخم ایشان بود</p></div>
<div class="m2"><p>به نزدیک من همچو خویشان بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرامرز بر وی نیایش گرفت</p></div>
<div class="m2"><p>فراوان مر او را ستایش گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه کار لشکر بدو کرد یاد</p></div>
<div class="m2"><p>هم از موج دریا واز تندباد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که از من پراکنده شد لشکرم</p></div>
<div class="m2"><p>ازاین درد شاید که اندوه خورم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدو گفت سیمرغ از این باک نیست</p></div>
<div class="m2"><p>کشد زهر،جایی که تریاک نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مدار از چنین کارها دل به رنج</p></div>
<div class="m2"><p>روان شو به سوی جزیره مرنج</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که کام تو آنجا برآرم همه</p></div>
<div class="m2"><p>سپه را به نزد تو آرم همه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>میندیش ای پهلوان سپاه</p></div>
<div class="m2"><p>که یک تن نگشته از ایشان تباه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو گفتار سیمرغ زان سان شنید</p></div>
<div class="m2"><p>دلش چون کبوتر به بر برتپید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شناکرد با مرغ روشن روان</p></div>
<div class="m2"><p>وزآنجا سپهبد یل پهلوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به آب اندر افکند کشتی ورفت</p></div>
<div class="m2"><p>به سوی جزیره خرامید تفت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همی رفت با مرد بازارگان</p></div>
<div class="m2"><p>زبالای او مرغ فرخ،روان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چوآمد به سوی جزیره به رنج</p></div>
<div class="m2"><p>ازو دور شد درد واندوه و رنج</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برآمد زآب و سوی بیشه رفت</p></div>
<div class="m2"><p>برآن مرز فرخنده پویید تفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یکی جای خرم تر از نوبهار</p></div>
<div class="m2"><p>پر از لاله وگل،لب جویبار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درو کاخ و ایوان شاهنشهی</p></div>
<div class="m2"><p>بیاراست با خوبی وفرهی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یکی خوب کاخ از در پهلوان</p></div>
<div class="m2"><p>برافراخته همچو خرم جنان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که آن کاخ و ایوان دستان بدی</p></div>
<div class="m2"><p>همه جای مهتر پرستان بدی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به هنگام مردی که دستان سام</p></div>
<div class="m2"><p>بدش نزد سیمرغ،آرام وکام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>درآن کاخ بد زال را پرورش</p></div>
<div class="m2"><p>همه هر چه بایست بودی برش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کجا مرغ فرخ بدش اوستاد</p></div>
<div class="m2"><p>یکی خسروی بود با کام وداد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از آن کاخ،هرکو سخن راندی</p></div>
<div class="m2"><p>همی کاخ زال زرش خواندی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>درآن کاخ شد گرد خورشید فر</p></div>
<div class="m2"><p>ابا ماه رخ دلبر سیم بر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بخفت و بخورد و برآسود شاد</p></div>
<div class="m2"><p>تو گفتی نبد در دلش رنج یاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از آن پش بشد مرغ فرمان روا</p></div>
<div class="m2"><p>همی گشت پوینده اندر هوا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به هر بیشه و کوه و دریا رسید</p></div>
<div class="m2"><p>پراکنده لشکر به هر سو بدید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گروهی به هر گوشه افتاده خوار</p></div>
<div class="m2"><p>همه تن پر از درد و دل سوگوار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پریشان و سرگشته و خسته دل</p></div>
<div class="m2"><p>غریوان و از درد وغم،تیره دل</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بیامد بر پهلوان باز گفت</p></div>
<div class="m2"><p>که لشکرت با درد و رنجست جفت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر چه نزارند و فریاد خواه</p></div>
<div class="m2"><p>از ایشان نگشتست یک تن تباه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فرامرز گفت ای خداوند فر</p></div>
<div class="m2"><p>یکی چاره ای کن که تا درنگر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از ایدر بدان نامداران رسیم</p></div>
<div class="m2"><p>بدان پرخرد نیک یاران رسیم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بدو گفت سیمرغ،من کام تو</p></div>
<div class="m2"><p>بجویم دهم دل به آرام تو</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همانگه بیاورد سه پاره سنگ</p></div>
<div class="m2"><p>چو خورشید تابنده از رنگ رنگ</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بدو داد و گفت ای گو رزمزن</p></div>
<div class="m2"><p>تواین را همی دار با خویشتن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چوباشی به دریا به کار آیدت</p></div>
<div class="m2"><p>همان روز سختی به یار آیدت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کنون بشنو از خاصیت هایشان</p></div>
<div class="m2"><p>که هرجا ببینی به گیتی نشان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یکی آنکه چون باد،تندی کند</p></div>
<div class="m2"><p>سپهر روان با تو تندی کند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مر این مهره را بر سر نیزه کن</p></div>
<div class="m2"><p>مگو با کس از هیچ گونه سخن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>که اندر زمان گردد آسوده باد</p></div>
<div class="m2"><p>از آن پس زتندی نیایدش یاد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دگر آنکه چون باد ناید پدید</p></div>
<div class="m2"><p>چو بی باد،دریا نشاید برید</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به آب اندر انداز و آنگه ببین</p></div>
<div class="m2"><p>خروشید بادی ز روی زمین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>برآید که کشتی بگردد روان</p></div>
<div class="m2"><p>بدان سان که حیران شود پهلوان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>فرامرز از آن گشت خندان و شاد</p></div>
<div class="m2"><p>بشد رنج بگذشته او را به یاد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دگر مهره بد به رنگ بلور</p></div>
<div class="m2"><p>که از دیدنش در دل افتاد شور</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زصد رنگ با هم برآمیخته</p></div>
<div class="m2"><p>درو پیکر ماهی انگیخته</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بداد و بگفتش که این را بدار</p></div>
<div class="m2"><p>که این مهره ات بهتر آید به کار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به بحر و به خشکی به هر جا شدن</p></div>
<div class="m2"><p>بدین مر تو را فال باید زدن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به جایی که آنجا ندانی تو راه</p></div>
<div class="m2"><p>به ناچار باید شد آن جایگاه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به آب اندرافکن شگفتی نگر</p></div>
<div class="m2"><p>که آن سوی پیچاند از بادسر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به کام تو آن راه بنمایدت</p></div>
<div class="m2"><p>زدل زنگ اندیشه بزدایدت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>برافروخت رخسار آن ارجمند</p></div>
<div class="m2"><p>تو گفتی برآمد به چرخ بلند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>فراوان ثنا گفت سیمرغ را</p></div>
<div class="m2"><p>که باشی همیشه به نیک اخترا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تویی پروراننده زال زر</p></div>
<div class="m2"><p>تویی افسر ما همه سر به سر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تویی رهنمای پراکندگان</p></div>
<div class="m2"><p>تویی بهتر از جمله دانندگان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بدو گفت سیمرغ کای نامدار</p></div>
<div class="m2"><p>یکی بنده ام کهتر کردگار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>مرا با شما حق بود دیرباز</p></div>
<div class="m2"><p>ابا زال و رستم گو سرفراز</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به هر کار باید که از نیک وبد</p></div>
<div class="m2"><p>مرا آگهی بخشی از کار خود</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>که من هر زمان آیم اندر برت</p></div>
<div class="m2"><p>بدان سان که باشد یکی چاکرت</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بسازم تو را کار،اگر جنگ و کین</p></div>
<div class="m2"><p>به فرمان یزدان جان آفرین</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>یکی پر کشید از بر خویشتن</p></div>
<div class="m2"><p>بدادش بدان سرور انجمن</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بدو گفت این یادگار منست</p></div>
<div class="m2"><p>همه پیش تو یادگار منست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هر آنگه که خواهی که آیم برت</p></div>
<div class="m2"><p>برآتش نه از عود با مجمرت</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ثنا خواند بر وی یل نامدار</p></div>
<div class="m2"><p>به پدرود کردن گرفتش کنار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ببوسید روی وبر روشنش</p></div>
<div class="m2"><p>زره را بپوشید با جوشنش</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به کشتی نشست و روان شد در آب</p></div>
<div class="m2"><p>زبهر سپه جان ودل در شتاب</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>مرآن مهره را برد با خویشتن</p></div>
<div class="m2"><p>همی آزمون کرد در انجمن</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به هر جایگاهی که رای آمدی</p></div>
<div class="m2"><p>به کردار مهره به جای آمدی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>به یک ماه در گرد دریا بگشت</p></div>
<div class="m2"><p>به هر بیشه و کوه اندر گذشت</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>به چندین زمان زان سپاه بزرگ</p></div>
<div class="m2"><p>زدریای پر موج و باد سترگ</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نبد یک تن آزرده از هیچ روی</p></div>
<div class="m2"><p>نبد رنج ودردی از آن نامجوی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>همه لشکر خویشتن دید شاد</p></div>
<div class="m2"><p>به دل خرمی پهلو پاک نزاد</p></div></div>