---
title: >-
    بخش ۳۵ - وداع کردن فرامرز از نزد کاوس و تأسف خوردن رستم و پند دادن رستم،فرامرز را
---
# بخش ۳۵ - وداع کردن فرامرز از نزد کاوس و تأسف خوردن رستم و پند دادن رستم،فرامرز را

<div class="b" id="bn1"><div class="m1"><p>پسر بار کرد و چو بربست کوس</p></div>
<div class="m2"><p>سطخر گزین گشت چون آبنوس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ماه دی برگ ریزان خروش</p></div>
<div class="m2"><p>برآمد بدین سان که بدرید گوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهاندار با تهمتن یک دو روز</p></div>
<div class="m2"><p>برفتند با گرد گیتی فروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیوم چون برآمد غوغای نی</p></div>
<div class="m2"><p>بخواند آن دلاور گو نیک پی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدو گفت کای مرد آسان نبرد</p></div>
<div class="m2"><p>ندیده هنوز از جهان گرم و سرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بزرگ و قوی گرچه هستی به زور</p></div>
<div class="m2"><p>کیاتر تویی چشم گیتی فروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو به دانی آیین آوردگاه</p></div>
<div class="m2"><p>ولی بشنو از من همی رسم شاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به گیتی کسی کو بود تا جور</p></div>
<div class="m2"><p>چو خورشید تابد همی نور و فر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دهد نور نزدیک را نور خویش</p></div>
<div class="m2"><p>نه هر کس که نزدیک تر نور خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درختی بود نامور شهریار</p></div>
<div class="m2"><p>همیشه ورا حنظل و شهدبار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کسی را که داند سزاوار زهر</p></div>
<div class="m2"><p>ز شهدش نبخشد همی هیچ بهر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر آن کس که داند سزاوار نوش</p></div>
<div class="m2"><p>زشهدش به تن درنسازد خروش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرت کید پیش آید و سرنهد</p></div>
<div class="m2"><p>به شکل رهی دست بر سر نهد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ببخش و مکش آن شه نامور</p></div>
<div class="m2"><p>که شاخ بریده نروید دگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به شب پاسدار هشیوار جفت</p></div>
<div class="m2"><p>چو گل در میان دوصد خوار خفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زبیگانه شربت چنان که ز زهر</p></div>
<div class="m2"><p>نخستین ز جامش بفرمای بهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرت جفت باید به هندوستان</p></div>
<div class="m2"><p>نگه کن بدان مرز جادوستان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زکید ارچه خورشید تابد به مهر</p></div>
<div class="m2"><p>چنان دان که دشمن ترین از سپهر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زنوشاد اگر دیو باشد نکوست</p></div>
<div class="m2"><p>که او یار هست و بود یار دوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز دخت کسان دیده بربند دل</p></div>
<div class="m2"><p>که نیکو نباشد چو آن دلگسل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه ناپارسا بر سر تخت عاج</p></div>
<div class="m2"><p>چه در گلخن افتاد زرینه تاج</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تورا پارسایی و جنگی بهست</p></div>
<div class="m2"><p>به هر انجمن نیکنامی بهست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه باید کشی تنگی از مردمی</p></div>
<div class="m2"><p>چه در دیو بستن به خون آدمی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو دردی که پر مغز گلشن نهی</p></div>
<div class="m2"><p>چه بایسته رازی که بارش تهی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو گنجت فزونست بیشش مکن</p></div>
<div class="m2"><p>خدا را نهان جز پرستش مکن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو هر نیکنامی زیزدان شناس</p></div>
<div class="m2"><p>شناسا چو گشتی همی کن سپاس</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گرت دادخواهی زند بانگ تند</p></div>
<div class="m2"><p>نباید که با او شوی هیچ کند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر بر تکاور سواری مپوی</p></div>
<div class="m2"><p>بپرس و بده داد با او بگوی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ببر اندران رنج کشور ببخش</p></div>
<div class="m2"><p>کمربستگان را همی زر ببخش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به هنگام کین هیچ گونه مخند</p></div>
<div class="m2"><p>چو خندد شود کار شاهی به بند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سخن های شاهی همیدون بود</p></div>
<div class="m2"><p>دلیری تو دانی که آن چون بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بگفت این و بوسید چشمش به مهر</p></div>
<div class="m2"><p>فرامرز بگریست پرآب چهر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جهاندار برگشت پس پیلتن</p></div>
<div class="m2"><p>بدو گفت ای دیده انجمن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز تیزی یکی آتش افروختی</p></div>
<div class="m2"><p>دو چشم من و زال زر سوختی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو یابد از این آگهی زال پیر</p></div>
<div class="m2"><p>زرنج دل خفته او مرده گیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ولی چون به نزدیک کاوس کی</p></div>
<div class="m2"><p>زبانت چنین گفته افکند پی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نیارم که گویم زره بازگرد</p></div>
<div class="m2"><p>مبادا که بخت اندر آید به گرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از این پس که رستم به زابلستان</p></div>
<div class="m2"><p>نبیند تو را با می و گلستان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شود زعفران رنگ گلگون اوی</p></div>
<div class="m2"><p>از آن غم شود خاک بالین اوی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نبینم تو را چون به بزم شکار</p></div>
<div class="m2"><p>چه گویم چه سازم بدان روزگار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همان مادرت بی دل و مستمند</p></div>
<div class="m2"><p>شب و روز ترسان ز بیم و گزند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>امیدم چنانست که زنده به مهر</p></div>
<div class="m2"><p>رساند تو با من خدای سپهر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نگهدار این رای فیروز من</p></div>
<div class="m2"><p>به رزم و به بزم ای دل افروز من</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ستیزه مکن با بلایی بزرگ</p></div>
<div class="m2"><p>بیندیش زان مار وآن تیره گرگ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به اندیشه و رای بربند کار</p></div>
<div class="m2"><p>مگر بگذرد برتو این روزگار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>حذرکن ز کناس و با او مگرد</p></div>
<div class="m2"><p>مبادا که هور اندر آید به گرد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دگر چون به ناکام جنگ آوری</p></div>
<div class="m2"><p>به پیکار ایشان درنگ آوری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کمر سخت کن گرز را پیش دار</p></div>
<div class="m2"><p>دلیری و فرهنگ با خویش دار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نگهدار مر بیژن و گیو را</p></div>
<div class="m2"><p>جهاندیده گرگین یل نیو را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دلیری چو گرزم که در روز کین</p></div>
<div class="m2"><p>ندارد کسی تاب او بر زمین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو گرزم که لرزد زمین زیر او</p></div>
<div class="m2"><p>زمین نالد از گرز و شمشیر او</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>پدر بر پدر چاکر سام شیر</p></div>
<div class="m2"><p>هنرمند گرد سوار دلیر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دلیری چو گستهم که در روز کین</p></div>
<div class="m2"><p>سنان برندارد ز روشن زمین</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زراسب گرانمایه فرزند توس</p></div>
<div class="m2"><p>که باشد به هرجا سزاوار کوس</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نگه دار دلشان که هنگام کار</p></div>
<div class="m2"><p>از ایشان یکی به که هندی هزار</p></div></div>