---
title: >-
    بخش ۴۱ - آمدن فرامرز در غار و دیدن گنج نوش زاد بن جمشید و سرگذشت آن
---
# بخش ۴۱ - آمدن فرامرز در غار و دیدن گنج نوش زاد بن جمشید و سرگذشت آن

<div class="b" id="bn1"><div class="m1"><p>در آن گه که برشد خروش خروس</p></div>
<div class="m2"><p>برآمد یکی چهره سندروس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شادی، فرامرز یل شد سوار</p></div>
<div class="m2"><p>به پیش اندرون بیژن نامدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درآمد در آن غار تار ارجمند</p></div>
<div class="m2"><p>شه و لشکر و پهلوان بلند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درآمد فرامرز آنجا فرود</p></div>
<div class="m2"><p>همی داد بر بیژن یل درود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخستین در آن گنج شد پهلوان</p></div>
<div class="m2"><p>یکایک نشستند با آن گوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیامد یکایک سوی نردبان</p></div>
<div class="m2"><p>دری دید بسته همی مرزبان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه بند و زنجیر او برشکست</p></div>
<div class="m2"><p>برآن مرمری بر فراکرد دست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درون شد فرامرز با آن سران</p></div>
<div class="m2"><p>یکی هشت صفه بدید اند آن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هر صفه گنجی نوآراسته</p></div>
<div class="m2"><p>دگر کرد دالان ز بس خواسته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نهاده زعاج و زبرجد سریر</p></div>
<div class="m2"><p>زپنجا و ده تاج خوش دلپذیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زهر سو سلاح و کمند یلان</p></div>
<div class="m2"><p>نهاده بسی جامه هندوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زر و مشک و کافور و عود وعبیر</p></div>
<div class="m2"><p>فتاده چو خرمن شمارش بگیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه خوشه زیرکران تاکران</p></div>
<div class="m2"><p>وزان خیره شد دیده پهلوان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدان تخت زرین یکی خفته بود</p></div>
<div class="m2"><p>به دیباش در چهره بنهفته بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جهان پهلوان زان در افکند رخت</p></div>
<div class="m2"><p>کیانی تنی دید همچون درخت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیالوده عنبر به روی سمن</p></div>
<div class="m2"><p>چو سرو اوفتاده به روی چمن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو گفتی زجانش کنون شسته اند</p></div>
<div class="m2"><p>به شیر و می و مشک و خون شسته اند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گشاده دو ابروی نرگس به هم</p></div>
<div class="m2"><p>به سینه همین بازوان کرده غم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تنش تازه همچون گل اندر بهار</p></div>
<div class="m2"><p>دژم رویش از گردش روزگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فراوان به انگشتش انگشتری</p></div>
<div class="m2"><p>به چهره سهیل و به رخ مشتری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سفیدش تن و چهره زرد آمده</p></div>
<div class="m2"><p>تو گویی کنون از نبردآمده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نهاده به بالین کمان وکمند</p></div>
<div class="m2"><p>همان ترکش و تیر وتیغ و پرند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زکوپال تیغش جهان پرنهیب</p></div>
<div class="m2"><p>تو گویی زگیتی ندارد شکیب</p></div></div>