---
title: >-
    بخش ۷۴ - سؤال کردن برهمن
---
# بخش ۷۴ - سؤال کردن برهمن

<div class="b" id="bn1"><div class="m1"><p>دگر گفت کی پهلو دلپذیر</p></div>
<div class="m2"><p>به سالی جوان و به اندیشه پیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سواری شنیدم که چون برنشست</p></div>
<div class="m2"><p>شد از دست وافتاد بر سر نشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخن گوید او هیچ تو نشنوی</p></div>
<div class="m2"><p>بگوید همین تازی وپهلوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به میدان چو خورشید چون دم زند</p></div>
<div class="m2"><p>همه گوهر افشاند از لب زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبانش به پیری نگوید سخن</p></div>
<div class="m2"><p>مراین داستان را توپاسخ بکن</p></div></div>