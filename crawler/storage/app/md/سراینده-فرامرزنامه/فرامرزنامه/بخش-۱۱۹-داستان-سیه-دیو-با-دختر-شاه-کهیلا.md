---
title: >-
    بخش ۱۱۹ - داستان سیه دیو با دختر شاه کهیلا
---
# بخش ۱۱۹ - داستان سیه دیو با دختر شاه کهیلا

<div class="b" id="bn1"><div class="m1"><p>چنان بودآیین آن شهریار</p></div>
<div class="m2"><p>که نوروز و در موسوم نوبهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر از سبزه ولاله گشتی جهان</p></div>
<div class="m2"><p>زباد بهاری شدی نوجوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بزرگان لشکر چه مرد و چه زن</p></div>
<div class="m2"><p>به هامون شدندی همه انجمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان گل و سنبل و لاله زار</p></div>
<div class="m2"><p>بدین سان گذشتی به ایشان بهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی دختری داشت آن شهریار</p></div>
<div class="m2"><p>به خوبی به سان بت قندهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بالا چو سرو و به رخسار ماه</p></div>
<div class="m2"><p>زبالاش مشکین کمند سیاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مسلسل فروبسته همچون زره</p></div>
<div class="m2"><p>زمشک و زعنبر گره تا گره</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو چشمانش چون نرگس نیم مست</p></div>
<div class="m2"><p>زغمزه دل جادوان کرد پست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زلعلش دو عقد گهر در نهفت</p></div>
<div class="m2"><p>شده طاق ابروش با ماه جفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرشتش زبس نیکویی از خرد</p></div>
<div class="m2"><p>تو گفتی که جان را همی پرورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدان نیکویی دختر پرهنر</p></div>
<div class="m2"><p>به دانش بیاراسته سر به سر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خردمند و با ناز وبا زیب وشرم</p></div>
<div class="m2"><p>زبان چرب کرده و شیرین و آوای نرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به دل،مهربان بود بر وی پدر</p></div>
<div class="m2"><p>که فرزند جز او نبودش دگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به آیین سوی جشنگه رفته بود</p></div>
<div class="m2"><p>میان گل و یاسمن خفته بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی دیو بود اندر آن مرز وبوم</p></div>
<div class="m2"><p>که در جنگ او خاره گشتی چو موم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از این دیو چهری به بالا چو ساج</p></div>
<div class="m2"><p>دو دندان به ماننده دار و عاج</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دو دیده به مانند دو چشمه خون</p></div>
<div class="m2"><p>دو بینی چو دو کشتی سرنگون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دهانش چو غاری بد از سنگ،پر</p></div>
<div class="m2"><p>درآویخته لب چو لنج شتر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به چنگال ماننده نره گرگ</p></div>
<div class="m2"><p>سرانگشت چون شاخ داری بزرگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زانگشت او ناخنان دراز</p></div>
<div class="m2"><p>برسته فزون تر زنیش گراز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از آن زشت پتیاره تیره روی</p></div>
<div class="m2"><p>جزیره همه ساله در گفتگوی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به هر روز در گرد آن شهر ودشت</p></div>
<div class="m2"><p>کسی را که بودی برو ره گذشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرفتی و خوردی هم آن جایگاه</p></div>
<div class="m2"><p>بدین سان همی رستی از سال وماه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در آن روز،آن دیو ناسازگار</p></div>
<div class="m2"><p>همی گشت در دشت بهر شکار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گذر شد مر او را در آن جشنگاه</p></div>
<div class="m2"><p>کجا خفته بود اندر آن دخت شاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پری چهره را همچنان خفته دید</p></div>
<div class="m2"><p>دوان گشت نزدیکی او رسید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرآن خوب رخ را به بر درگرفت</p></div>
<div class="m2"><p>ز ره بازگشت و ره از سر گرفت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کنیزان گلرخ فرستندگان</p></div>
<div class="m2"><p>همه خوب رخ دل ربا یندگان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگفتند باشاه یکسرسخن</p></div>
<div class="m2"><p>که آن دیووارون چه افکندبن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چوپیل دژآگاه مست ودژم</p></div>
<div class="m2"><p>بیامد همی سوخت گیتی به دم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زناگه پری چهره رادرربود</p></div>
<div class="m2"><p>ببرد وز دل ها برآورد دود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو بشنیدشاه این سخن شد غمین</p></div>
<div class="m2"><p>زسرتاج برداشت زدبرزمین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خروشی برآمد ز ایوان شاه</p></div>
<div class="m2"><p>جهان گشت برنیکخواهان سیاه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>غلام وکنیزان خورشید روی</p></div>
<div class="m2"><p>برفتند بامویه وهای هوی</p></div></div>