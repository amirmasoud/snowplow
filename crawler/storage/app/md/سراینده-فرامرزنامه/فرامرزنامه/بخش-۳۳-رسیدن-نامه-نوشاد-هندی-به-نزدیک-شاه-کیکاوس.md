---
title: >-
    بخش ۳۳ - رسیدن نامه نوشاد هندی به نزدیک شاه کیکاوس
---
# بخش ۳۳ - رسیدن نامه نوشاد هندی به نزدیک شاه کیکاوس

<div class="b" id="bn1"><div class="m1"><p>که ناگه در آمد یکی نامدار</p></div>
<div class="m2"><p>که می بار خواهد بر شهریار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دربان چنین گفت کای نامدار</p></div>
<div class="m2"><p>خبر کن ز من یک بر شهریار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که پیغام آوردم از شاه هند</p></div>
<div class="m2"><p>سخن های چندین در او مستمند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همان گه ز در رفت دربان شاه</p></div>
<div class="m2"><p>زمین را ببوسید در پیشگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به شه گفت که آمد یکی نامدار</p></div>
<div class="m2"><p>زهندوستان بر در شهریار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن دارد از شاه هندوستان</p></div>
<div class="m2"><p>همان کشور هند و جادوستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بفرمود کاید بر شهریار</p></div>
<div class="m2"><p>درون شد ز در هندوی نامدار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ستایش بسی کرد و نامه بداد</p></div>
<div class="m2"><p>ز هندوستان کرد بسیار یاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز نوشاد هندی بدو آفرین</p></div>
<div class="m2"><p>بسی کرد و بوسید روی زمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دبیر آمد و نامه بگشاد سر</p></div>
<div class="m2"><p>نخست آفرین کرد بر دادگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خداوند جوزا و بهرام و هور</p></div>
<div class="m2"><p>که بدهد به قسمت خور پیل و مور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وزو آفرین بر شه نیک پی</p></div>
<div class="m2"><p>جهاندار فرخنده کاوس کی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهانجوی با دانش و دین و داد</p></div>
<div class="m2"><p>سرافراز از تخمه کیقباد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس از  آفرین جهاندار کی</p></div>
<div class="m2"><p>غم و درد فرزند بفکند پی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نوشته به زاری و درد و غریو</p></div>
<div class="m2"><p>بلا و غم و رنج کناس دیو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدان ای سرافراز دیهیم و گنج</p></div>
<div class="m2"><p>که جانم ستوه آمد از درد و رنج</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنان دان که در وادی مرغزار</p></div>
<div class="m2"><p>یکی چشمه ساریست خوش جویبار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درختی در آن مرز شاخ بلند</p></div>
<div class="m2"><p>به نزدیک آن شاخ کاخ بلند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>درو گنبد سال خورده بزرگ</p></div>
<div class="m2"><p>در آن تیره گنبد بلایی بزرگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی دیو در وی سیاه و بلند</p></div>
<div class="m2"><p>ستبر و سیه روی و ناهوشمند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دلیر و قوی بال چون برف موی</p></div>
<div class="m2"><p>دوان ببر و شیرش همه چار سوی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در آید به هر سال در خان من</p></div>
<div class="m2"><p>بسوزد بسی مرز و ایوان من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نگردد ز من تا ز من دختری</p></div>
<div class="m2"><p>ستاند به کردار نیک اختری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سه دختر بدم چون سه خرم بهار</p></div>
<div class="m2"><p>ز من بستد آن دیو نا هوشیار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو کاوس مان شهریاری کند</p></div>
<div class="m2"><p>سزد گر در این رنج یاری کند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دگر آنکه در خطه هند بار</p></div>
<div class="m2"><p>یکی شهریار است بس نامدار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرو را به هندوستان کید نام</p></div>
<div class="m2"><p>دلیرو سرافراز و گسترده کام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به هنگام کین تیغ آهن گذار</p></div>
<div class="m2"><p>به پیش سپاهست نهصدهزار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شماره ز پیلان جنگی مکن</p></div>
<div class="m2"><p>قلم درکش و بار سنگین مکن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به گردون همی برفرازد کلاه</p></div>
<div class="m2"><p>فرستد بر هر سالمان باج خواه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدان نامور ما نداریم تاو</p></div>
<div class="m2"><p>چو کاوس جوید ز ما باژ و ساو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نخستین ز ما باج بستاند اوی</p></div>
<div class="m2"><p>وز آن پس ز ما باج نستاند اوی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وز آن پس طلب دارد آن ساو و باج</p></div>
<div class="m2"><p>وگرنه دگر ره نجوید خراج</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دگر آنکه در بیشه مرزغون</p></div>
<div class="m2"><p>یکی گرگ پیدا شده پرفسون</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو گوران دو شاخ و چو پیلان دو نیش</p></div>
<div class="m2"><p>دل عالمی گشته زان گرگ ریش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تبه شد بسی لشکر جنگجوی</p></div>
<div class="m2"><p>نیارد کسی کردن آهنگ اوی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یکی ژنده بینی چو یک ژنده پیل</p></div>
<div class="m2"><p>همه سر چو برف و همه تن چو نیل</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز سر تا به پایش کشیده رقم</p></div>
<div class="m2"><p>خط نازنین سرخ همچون بقم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به دندان یکی پیل بردارد اوی</p></div>
<div class="m2"><p>به یک دم جهان را بسوزاند اوی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همی نام او گرگ گویا بود</p></div>
<div class="m2"><p>سخنگوی بیداد پویا بود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دگر آنکه در دامن شهر هند</p></div>
<div class="m2"><p>یکی کوه بینی دراز و بلند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یکی دره در کوه خارا بود</p></div>
<div class="m2"><p>که از دیدنش خیره برنا شود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>درو اژدهایی بلند و پلید</p></div>
<div class="m2"><p>کزان سان همی اژدها کس ندید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فتاده تن خیره اش خم به خم</p></div>
<div class="m2"><p>جهان را همی برفروزد به دم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نروید گیاه از دو فرسنگیش</p></div>
<div class="m2"><p>بلرزد جهان از تن جنگیش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جهان از دم او شود سوخته</p></div>
<div class="m2"><p>ازو بس جوانی شد افروخته</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مهیبست گویی همه کام او</p></div>
<div class="m2"><p>خردمندجو تا کند نام او</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نبرد ورا کس نبندد میان</p></div>
<div class="m2"><p>نزیبد کس او را جز ایرانیان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گر از تخمه سام جنگی سوار</p></div>
<div class="m2"><p>به ایران بود جنگی نامدار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بپیماید این ره به فرمان کی</p></div>
<div class="m2"><p>ببرد مر این جمله را پای و پی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دگر آن که در بیشه خوم سار</p></div>
<div class="m2"><p>پدید آمده کرگدن سی هزار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو شیران که از بند گردد یله</p></div>
<div class="m2"><p>به کردار گوران به هر سو گله</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به هر مرز بومی که پی بسپرند</p></div>
<div class="m2"><p>زمین برشکافند و خارا درند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو پیلان به زورند وآهو به تک</p></div>
<div class="m2"><p>چو شیران بغرند دیوان به رگ</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به گردن ستبر و به سینه فراخ</p></div>
<div class="m2"><p>ز پیشانی آنکه برون کرده شاخ</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همه گشته زین مرز تا مرز دود</p></div>
<div class="m2"><p>شگفتی دو گوشت توان کی شنود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو کاوس کی یار باشد بدین</p></div>
<div class="m2"><p>همه هند خوانند بدو آفرین</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بدین پنج اگر رنج فرماید او</p></div>
<div class="m2"><p>وز آن پس ز ما گنج پیماید او</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو ایرانیان پای بینم به رنج</p></div>
<div class="m2"><p>به پاداش آن برگشاییم گنج</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>وگر چون ز ایران تهی شد هنر</p></div>
<div class="m2"><p>چه باید مر داد این گنج و زر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چرا داد باید به ایران خراج</p></div>
<div class="m2"><p>فرستادن تاج با تخت عاج</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هر آ ن کس که خواهد که گردد بلند</p></div>
<div class="m2"><p>از این نام گرد آید و ارجمند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>دبیر گرانمایه چون این سخن</p></div>
<div class="m2"><p>فرو خواند آن نامه آمد به بن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دل پیر کاوس زان بردمید</p></div>
<div class="m2"><p>زجام و زباده دم اندر کشید</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به گردان چنین گفت پرمایه شاه</p></div>
<div class="m2"><p>که ای نامداران زرین کلاه</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هرآن کو بدین کار بندد میان</p></div>
<div class="m2"><p>برافروزد او نام ایرانیان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ببخشم مر او را کلاه و کمر</p></div>
<div class="m2"><p>دو بهره ز گیتی همه سر به سر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>رخ نامداران دگر شد به رنگ</p></div>
<div class="m2"><p>شد از خشم گردان دل شاه تنگ</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>زکرسی برآمد فرامرز گو</p></div>
<div class="m2"><p>بگفتا منم هند را پیش رو</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سر کید هندی به شمشیر تیز</p></div>
<div class="m2"><p>ببرم نمایم ورا رستخیز</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>وز آن پس بیایم سوی مرغزار</p></div>
<div class="m2"><p>بگیرم همان دیو ناسازگار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>همان دخت نوشاد هندی ز بند</p></div>
<div class="m2"><p>رهانم به فرمان شه ارجمند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>همان گرگ گویا به کوپال سام</p></div>
<div class="m2"><p>بکوبم زنم آتش اندر کنام</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>همان مار جوشان به تدبیر و رای</p></div>
<div class="m2"><p>بسوزم به فر جهانبان خدای</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>وز آن پس به تنها من و کرگدن</p></div>
<div class="m2"><p>بگیرم کنم بی روانشان بدن</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>وگر یک هزار است و گر صد هزار</p></div>
<div class="m2"><p>چو من بر شم تیغ سام سوار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>به نیروی شاه و به فر پدر</p></div>
<div class="m2"><p>تهی سازم از کرگدن بوم و بر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو شد گفته فرمان دهد پور زال</p></div>
<div class="m2"><p>ازین پنج گونه برآرم دمار</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>رخ شاه زان گرد جنگی سرشت</p></div>
<div class="m2"><p>چنان شد که گل در بساط بهشت</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چنین گفت با رستم زال زر</p></div>
<div class="m2"><p>که پنهان نماند نژاد و گهر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>فرامرز یل روی من زنده کرد</p></div>
<div class="m2"><p>مرا خرم و انجمن بنده کرد</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>همش رای و فرهنگ و هم هوش و سنگ</p></div>
<div class="m2"><p>همش دست و بازوی و هم گرز جنگ</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نژاد وی از تخم جمشید شاه</p></div>
<div class="m2"><p>به گیتی به کردار تابنده ماه</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>پدر بر پدر هر دو گرد و دلیر</p></div>
<div class="m2"><p>جهانگیر و جنگ آور و مرد شیر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>هم او را سزد شاهی هندبار</p></div>
<div class="m2"><p>که جنگ آورست و قوی نامدار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>همانا جز او هرکه پوید به هند</p></div>
<div class="m2"><p>نباشد بر کید هندی پسند</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به کناس دیو و به مار و به گرگ</p></div>
<div class="m2"><p>نشاید بجز پهلوان بزرگ</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بفرمود تا شاهی مرز هند</p></div>
<div class="m2"><p>همیدون چنان تا به دریای سند</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>نوشتند منشور بر نام اوی</p></div>
<div class="m2"><p>ز هر در بجست آن زمان جنگجوی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>جهانجوی چون مهر منشور کرد</p></div>
<div class="m2"><p>چو ماه آن دو رخ سوی گنجور کرد</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بفرمود تا تاج و انگشتری</p></div>
<div class="m2"><p>یکی تخت و پیرایه آذری</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بیاورد تاجش به سر برنهاد</p></div>
<div class="m2"><p>جهاندار ازو شاد و او نیز شاد</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بگفتا ز گردان هر آن نامور</p></div>
<div class="m2"><p>که خواهی یکایک به همراه بر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>نگه کرد بیژن سوی شهریار</p></div>
<div class="m2"><p>که جاوید زی تا بود روزگار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>به جایی که خورشید زابلستان</p></div>
<div class="m2"><p>به پیروزی رفتند در گلستان</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>به هند و به دریا و هر گرم و سرد</p></div>
<div class="m2"><p>پس ایدر بگو ما چه خواهیم کرد</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>دلیران به می خوردن اندر ستخر</p></div>
<div class="m2"><p>چو باشند ما زان نداریم فخر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>سر ما به جایی که در پی نهند</p></div>
<div class="m2"><p>بویژه که فرمان بدو کی نهند</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>گرانمایه خسرو به گنجور گفت</p></div>
<div class="m2"><p>که یک دست جامه برآر از نهفت</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>کلاه و کمر یکسره شاهوار</p></div>
<div class="m2"><p>یکی تیغ پرمایه گوهر نگار</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>سه اسب گرانمایه با زین زر</p></div>
<div class="m2"><p>سه ریدک همه خوب و زرین کمر</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>گرانمایه گنجور چون آن ببرد</p></div>
<div class="m2"><p>جهان کدخدایش به بیژن سپرد</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ز ایران و وز لشکر نامدار</p></div>
<div class="m2"><p>فرامرز بستد همی ده هزار</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>ز زابل گزین کرد صد نامور</p></div>
<div class="m2"><p>زکابل دو صد گرد والاگهر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>یکی هفته در کار هندوستان</p></div>
<div class="m2"><p>به سرکرده شد سوی جادوستان</p></div></div>