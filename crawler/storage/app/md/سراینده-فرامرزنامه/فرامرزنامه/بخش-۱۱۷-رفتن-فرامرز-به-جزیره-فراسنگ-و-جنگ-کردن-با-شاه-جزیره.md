---
title: >-
    بخش ۱۱۷ - رفتن فرامرز به جزیره فراسنگ و جنگ کردن با شاه جزیره
---
# بخش ۱۱۷ - رفتن فرامرز به جزیره فراسنگ و جنگ کردن با شاه جزیره

<div class="b" id="bn1"><div class="m1"><p>از آن پس جزیری به پیش آمدش</p></div>
<div class="m2"><p>که فرسنگ از صد به پیش آمدش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فراسنگ میخواندندش به نام</p></div>
<div class="m2"><p>درو خسروی بود با رای وکام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اباپیل و با کوس و گنج و سپاه</p></div>
<div class="m2"><p>همش نام شاهی،همش تاج و گاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو آگه شد از لشکر پهلوان</p></div>
<div class="m2"><p>که آمد بدان مرز،روشن روان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپاهی برآراست آن نامدار</p></div>
<div class="m2"><p>همه گرد و شایسته کارزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیاورد نزدیک دریا کنار</p></div>
<div class="m2"><p>بدان تا برآرد ز دشمن دمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوانی به تن همچو کوه سیاه</p></div>
<div class="m2"><p>همه جنگجوی و همه کینه خواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زدندان ماهی و پیل استخوان</p></div>
<div class="m2"><p>به دست اندرون تیغ و گرز گران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گروهی فلاخن گرفته به دست</p></div>
<div class="m2"><p>که از زخم ایشان شدی کوه،پست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به بالا چو کوه و به تن،همچو باد</p></div>
<div class="m2"><p>زدیو است گفتی مگرشان نژاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به تن،سهمگین زورمندان بدند</p></div>
<div class="m2"><p>به جنگ اندرون،پیل دندان بدند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به تک در ربودندی از پشت اسب</p></div>
<div class="m2"><p>پیاده به کردار آذرگشسب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو در دستشان اوفتادی کسی</p></div>
<div class="m2"><p>از آن پس زمانشان نماندی بسی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به دندان بکندند آن را چوچرم</p></div>
<div class="m2"><p>بخوردندی اندر زمان گرم گرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرامرز رستم چو آمد زآب</p></div>
<div class="m2"><p>برآن جنگ و کینه گرفته شتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زکشتی برآمد سراسر سپاه</p></div>
<div class="m2"><p>برآمد به خورشید گرد سپاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برآراستند از پی نام و ننگ</p></div>
<div class="m2"><p>گرفتند کوپال و خنجر به چنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بغرید کوس و بنالید نای</p></div>
<div class="m2"><p>سپهبد برانگیخت لشکر زجای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برآن نره دیوان ببارید تیغ</p></div>
<div class="m2"><p>چو باران نوروز از تیره میغ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سپاه جزیره برآورد جوش</p></div>
<div class="m2"><p>به مغز سپهر اندرآمد خروش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زسنگ فلاخن به ایرانیان</p></div>
<div class="m2"><p>زدندان ماهی واز استخوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زمین تنگ شد آسمان تیره گشت</p></div>
<div class="m2"><p>سر سرفرازان از آن خیره گشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فلاخن ببارید همچون تگرگ</p></div>
<div class="m2"><p>به ایرانیان،سنگ وباران مرگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چواز سنگ ازهوا آمدی بر سپر</p></div>
<div class="m2"><p>شدی همچو ناوک ز اسپر گذر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سپردار از آسیب سنگ سیاه</p></div>
<div class="m2"><p>به چرخ آمدی اندرآوردگاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بخستند از ایرانیان بی شمار</p></div>
<div class="m2"><p>به سنگ فلاخن در آن کارزار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به سختی رسید آن سپاه بزرگ</p></div>
<div class="m2"><p>از آن نره دیوان گرد سترگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سپهبد چو آن دید برداشت گرز</p></div>
<div class="m2"><p>برانگیخت آن تند بالای برز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به ایشان همی کوفت گرز گران</p></div>
<div class="m2"><p>چو خایسک و سندان آهنگران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فراوان از آن دیو چهران بکشت</p></div>
<div class="m2"><p>به گرز گران و به زخم درشت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یکی نره دیوی بیامد برش</p></div>
<div class="m2"><p>تو گفتی که بر چرخ ساید سرش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ازین هولناکی روان خواره ای</p></div>
<div class="m2"><p>دژآگاه دیوی و پتیاره ای</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی استخوانی به دست اندرون</p></div>
<div class="m2"><p>که بودی درازیش چل رش فزون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>برآویخت با پهلوان جهان</p></div>
<div class="m2"><p>بینداخت بر پهلوان،استخوان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سپر بر سرآورد گرد دلیر</p></div>
<div class="m2"><p>فرو برد چنگال چون نره شیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گرفتش کمر بند واز زین بکند</p></div>
<div class="m2"><p>برآوردش از جا چو کوه بلند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بزد بر زمینش چو یک لخت کوه</p></div>
<div class="m2"><p>زدو رویه بر وی نظاره گروه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بفرمود کز تن بریدن سرش</p></div>
<div class="m2"><p>فکندند بر نامور لشکرش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دگر نیزه بگرفت مانند باد</p></div>
<div class="m2"><p>درآن بدمنش نره دیوان فتاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نگه کرد تا جای خسرو کجاست</p></div>
<div class="m2"><p>به پیش سپه دیدش از دست راست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بتازید تا پیش خسرو رسید</p></div>
<div class="m2"><p>جهان شد به گرد اندرون ناپدید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بزد نیزه ای بر کمرگاه اوی</p></div>
<div class="m2"><p>ز زین برگرفتش به کردار گوی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بیامد دمان نزد ایران سپاه</p></div>
<div class="m2"><p>بیفکند آن شاه و کردش تباه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چوافکنده شد خسرو بی همال</p></div>
<div class="m2"><p>رمه بی شبان گشت و شد پایمال</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به تاراج دادان بر وبوم و رست</p></div>
<div class="m2"><p>پدر بر پسر بر همی راه جست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بسی بدره و برده تخت عاج</p></div>
<div class="m2"><p>زکرسی زرین و زرینه تاج</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به دست آمد ایرانیان را زشهر</p></div>
<div class="m2"><p>از آن رزم هرکس بسی یافت بهر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زن و کودکان زینهاری شدند</p></div>
<div class="m2"><p>به نزد سپهبد به زاری شدند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>فرامرز یکسر ببخشیدشان</p></div>
<div class="m2"><p>ز راه محبت نوازید شان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دو ماهی در آنجا به شادی بماند</p></div>
<div class="m2"><p>زماه سیم لشکری برنشاند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یکی را برآن شهر،سالارکرد</p></div>
<div class="m2"><p>زکشتی ودریا برآورد گرد</p></div></div>