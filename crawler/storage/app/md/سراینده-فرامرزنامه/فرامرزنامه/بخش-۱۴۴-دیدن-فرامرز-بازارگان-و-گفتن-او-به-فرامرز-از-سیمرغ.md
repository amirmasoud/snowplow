---
title: >-
    بخش ۱۴۴ - دیدن فرامرز،بازارگان و گفتن او به فرامرز از سیمرغ
---
# بخش ۱۴۴ - دیدن فرامرز،بازارگان و گفتن او به فرامرز از سیمرغ

<div class="b" id="bn1"><div class="m1"><p>ببندد دری کردگار جهان</p></div>
<div class="m2"><p>که بگشایدت صد در اندر نهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زداد هرگز مشو نا امید</p></div>
<div class="m2"><p>دل راست را سوی او ده نوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که فیروز بخت است وفیروز گر</p></div>
<div class="m2"><p>نماند تو را روز سختی زبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همانگه پدیدآمد از ناگهان</p></div>
<div class="m2"><p>یکی پر خرد مرد بازارگان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیامد به پیش سپهدار گرد</p></div>
<div class="m2"><p>به رخ پیش او مر زمین را سترد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوازید و بنواختش نامدار</p></div>
<div class="m2"><p>بدو گفت ای مرد پاکیزه کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو چون اوفتادی بدین جایگاه</p></div>
<div class="m2"><p>کجا یافتی اندرین مرز راه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه سرگذشتت بگو پیش من</p></div>
<div class="m2"><p>یکی تازه گردان دل ریش من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنین داد پاسخ بدو مرد باز</p></div>
<div class="m2"><p>که ای شیروش گرد گردن فراز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان دان که بازارگانی بدم</p></div>
<div class="m2"><p>یکی مایه ور کاروانی بدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زبهر فزونی و سود وزیان</p></div>
<div class="m2"><p>برآراستم بر سوی زنگیان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زناگاه باد کج آمد پدید</p></div>
<div class="m2"><p>همه مال مردم بشد ناپدید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیفتادم از ناگهان بی گزاف</p></div>
<div class="m2"><p>به یک پاره تخته سوی کوه قاف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وزآن جای بر خشک رفتم بسی</p></div>
<div class="m2"><p>برآن راه یارم نبودی کسی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زمینی پر از سختی و رنج وآز</p></div>
<div class="m2"><p>پر از هول وبی آب وراه دراز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به دریای مغرب شدم بی درنگ</p></div>
<div class="m2"><p>رسیدم از آن پس به شهر فرنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وزآن جا به کشتی نشستم دگر</p></div>
<div class="m2"><p>به جان،راه جوی و به دل،چاره گر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو چندی برفتیم بر روی آب</p></div>
<div class="m2"><p>سربختمان اندرآمد به خواب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به راهی برون رفت کشتی که کس</p></div>
<div class="m2"><p>نبود اندر آن راه،فریاد رس</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زناگاه،کشتی زباد بلا</p></div>
<div class="m2"><p>بپیچید و شد در دم اژدها</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین گفت داننده پیر کهن</p></div>
<div class="m2"><p>چواز بند بگشاد پای سخن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که اندر همه کارها شکر گوی</p></div>
<div class="m2"><p>که از بد،بتر هست کاید به روی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چوشد غرق کشتی زباد دمان</p></div>
<div class="m2"><p>به یک تخته ماندم به دل با غمان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدیدم به دریا درختی بلند</p></div>
<div class="m2"><p>بزرگیش بگذشته از چون و چند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درختی کزآن شاخ گفتی چهان</p></div>
<div class="m2"><p>شدست از بر سایه او نهان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همی بر کشیده سر اندر سپهر</p></div>
<div class="m2"><p>زشاخش خراشیده رخسار مهر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنین گفت دانادل برهمن</p></div>
<div class="m2"><p>کزآن جا فروزد سهیل یمن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی رشته بالای او ده کمند</p></div>
<div class="m2"><p>زابریشم خام کرده به بند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همی داشتم با خودم بی گمان</p></div>
<div class="m2"><p>که روزی به کار آیدم در جهان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو تخته بیامد به زیر درخت</p></div>
<div class="m2"><p>بینداختم رشته ناگه زبخت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به شاخ اندر انداختم آن کمند</p></div>
<div class="m2"><p>بپیچید و بر شاخ شد سخت بند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زدم اندرو دست بر سان باد</p></div>
<div class="m2"><p>روان بر شدم از بر شاخ شاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چه خوش گفت داننده پیش بین</p></div>
<div class="m2"><p>که اندر همه کار،یزدان گزین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>برآن شاخ بودم نشسته سه روز</p></div>
<div class="m2"><p>چهارم چو خورشید گیتی فروز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برآمد جهان گشت روشن ازو</p></div>
<div class="m2"><p>زمین گشت یکباره گلشن ازو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یکی مرغ دیدم چو کوه گران</p></div>
<div class="m2"><p>که از هیبتش خیره گشتی روان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جهانی دراز است پهنای او</p></div>
<div class="m2"><p>سیه گشته گیتی ز پهنای او</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برآمد نشست از برآن درخت</p></div>
<div class="m2"><p>به شاخ اندرون کرد چنگال سخت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نگه کردم از نامور پای او</p></div>
<div class="m2"><p>فراخی بر و چنگ و پهنای او</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چنان بد که گر سی و چل آدمی</p></div>
<div class="m2"><p>برو بر نشستی نگشتی غمی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>من اندیشه کردم بسی اندر آن</p></div>
<div class="m2"><p>که یابم رهایی از آنجا به جان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>برفتم نشستمش بر پشت پای</p></div>
<div class="m2"><p>درآمد دمان مرغ پران به جای</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به پرواز بر شد سوی تیره ابر</p></div>
<div class="m2"><p>زپرش خروش آمدی چون هژبر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدان گونه بر شد به چرخ برین</p></div>
<div class="m2"><p>که چون بنگریدم به روی زمین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زمین پیش چشمم یکی مهره بود</p></div>
<div class="m2"><p>زمهره تو گفتی که کمتر نمود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز پرواز او دیده ام خیره شد</p></div>
<div class="m2"><p>زمین و زمان پیش من تیره شد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زبالا به سوی زمین کرد سر</p></div>
<div class="m2"><p>چو کشتی بیامد سوی رهگذر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بیامد روان تا بدین جایگاه</p></div>
<div class="m2"><p>که می بینی ای گرد لشکر پناه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هنوز از هوا تا به روی زمین</p></div>
<div class="m2"><p>فزون مانده بودی ارش ای گزین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>که من خویشتن را بینداختم</p></div>
<div class="m2"><p>جز این چاره ای نیز نشناختم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فتادم به تن خسته بر روی خاک</p></div>
<div class="m2"><p>به خشنود دارنده یزدان پاک</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تن مرده را زندگی باز داد</p></div>
<div class="m2"><p>از آن خاک برخاستم همچو باد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ستایش کنان راه را بر ساختم</p></div>
<div class="m2"><p>دل از رنج رفته بپرداختم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>رسیدم بدین شهر،بیچاره وار</p></div>
<div class="m2"><p>بدین سان که بینی به بد روزگار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دو سالست شاها فزون تر که من</p></div>
<div class="m2"><p>گرفتار گشتم در این انجمن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چنین زار و بیچاره و سوگوار</p></div>
<div class="m2"><p>پریشان و سر گشته و دل فکار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نه راه و نه یار و نه کس رهنمون</p></div>
<div class="m2"><p>دلم پر زدرد و جگر پر ز خون</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>مگر پاک یزدان فرمان روا</p></div>
<div class="m2"><p>مرا داد خواهد ز سختی رها</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>که همچون تو گردی ز کشتی به باد</p></div>
<div class="m2"><p>به ناگه بدین مرز اندر فتاد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بدان تا من از روزگار بلا</p></div>
<div class="m2"><p>به فرو به بخت تو گردم رها</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>فرامرز یل مانند ازو در شگفت</p></div>
<div class="m2"><p>از آن گفته او شگفتی گرفت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ازآن پس بدو گفت دلشاد وار</p></div>
<div class="m2"><p>همه رنج بگذشته را باد دار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>فراوان سپاه من و سرکشان</p></div>
<div class="m2"><p>که هستند هر یک به گیتی نشان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به دریا هم از موج واز باد تیز</p></div>
<div class="m2"><p>زمن گم شدستند چون رستخیز</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پذیرفتم از پاک جان آفرین</p></div>
<div class="m2"><p>که گر من به فر جهان آفرین</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ببینم رخ پهلوانان خویش</p></div>
<div class="m2"><p>ستوده گوان و جوانان خویش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به بخت فروزنده فرخ شوم</p></div>
<div class="m2"><p>جهانبان مگر یاوری بخشدم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>از این ژرف دریا بیابم گذار</p></div>
<div class="m2"><p>به من بازگردد همان روزگار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نگهدارمت همچو یاران خویش</p></div>
<div class="m2"><p>مگر کت رسانم به آرام خویش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو بشنیدآن مرد بس آفرین</p></div>
<div class="m2"><p>برو خواند و بنهاد سر بر زمین</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بدو گفت از آن پس یل پهلوان</p></div>
<div class="m2"><p>که ای مرد دانای روشن روان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>برو پیش آن مردمان وگروه</p></div>
<div class="m2"><p>سخن گو کز اندیشه گشتم ستوه</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ازیشان سخن بازپرس اندکی</p></div>
<div class="m2"><p>مگر چاره دانند زی مایکی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>که ما جستجوی سپه چون کنیم</p></div>
<div class="m2"><p>مگر کز دل اندیشه بیرون کنیم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>دوان آمد از پیش آن نامدار</p></div>
<div class="m2"><p>بشد تا بر مردم آن دیار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بپرسید پس مرد بازارگان</p></div>
<div class="m2"><p>از آن اسب چهران بی مایگان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بیان کرد رازی که بود از نهفت</p></div>
<div class="m2"><p>همه کارلشکر بدان ها بگفت</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چنین پاسخ آورده شد زان گروه</p></div>
<div class="m2"><p>کز ایدر به ده روز یک برزکوه</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به پیش آیدت کز بلندی ندید</p></div>
<div class="m2"><p>بدان گونه کوهی نه کس هم شنید</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>برو بر یکی مرغ با رای وکام</p></div>
<div class="m2"><p>جهان دیده،سیمرغ گوید به نام</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چنین کارها زوبرآید مگر</p></div>
<div class="m2"><p>که او نیست گمراه را راهبر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>شما را بر مرغ باید شدن</p></div>
<div class="m2"><p>چنین داستان ها بر او زدن</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>که او سخت دانا و زیرک دلست</p></div>
<div class="m2"><p>همه دانشی نزد او حاصل است</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چو بشنید ازیشان هم اندر زمان</p></div>
<div class="m2"><p>بیامد بر پهلوان جهان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>سخن های ایشان بدو بازگفت</p></div>
<div class="m2"><p>زکوه و ز سیمرغ و راز نهفت</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>سپهبد پراندیشه شد زین سخن</p></div>
<div class="m2"><p>به یاد آمدش داستان کهن</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>که گرد جهان پهلوان زال زر</p></div>
<div class="m2"><p>ز سیمرغ بستد هم او چند پر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>که روزی که از روزگار دژم</p></div>
<div class="m2"><p>به پیش آیدش سختی و درد وغم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>به هنگام کوشش که با روزگار</p></div>
<div class="m2"><p>همی گنج و لشکر نیاید به کار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>یکی لخت از آن مو به آتش زند</p></div>
<div class="m2"><p>مگر کز زمانه خلاصی دهد</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>از آن پر سیمرغ،مر زال زر</p></div>
<div class="m2"><p>دو پر داده بودی بدان نامور</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>که روزی که کاری بود سخت تر</p></div>
<div class="m2"><p>به عود اندر آتش نه ودرنگر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>نگه کن که آن مرغ گیتی فروز</p></div>
<div class="m2"><p>بیاید برت با دل مهرسوز</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>برآرد همی هرچه باشدت کام</p></div>
<div class="m2"><p>کند توسن چرخ،نزد تو رام</p></div></div>