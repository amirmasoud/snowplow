---
title: >-
    بخش ۱۹۱ - مناجات شهریار مرحوم خدابخش
---
# بخش ۱۹۱ - مناجات شهریار مرحوم خدابخش

<div class="b" id="bn1"><div class="m1"><p>ایا قادر پاک ودانای راز</p></div>
<div class="m2"><p>تویی خالق وقادر کارساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تواز قدرت خویش گشتی پدید</p></div>
<div class="m2"><p>تو دادی در بسته ها را کلید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو کردی مراین طاق زرین،عیان</p></div>
<div class="m2"><p>منقش نموده بر او اختران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درخشان شده هریک اندر سپهر</p></div>
<div class="m2"><p>چو مریخ و ناهید و برجیس ومهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خور دادی این پرتو و نور وسوز</p></div>
<div class="m2"><p>زصنعت شده ماه،عالم فروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به شب،نور بدر وبه روز،آفتاب</p></div>
<div class="m2"><p>زمشرق به مغرب گرفته شتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زدریای صنعت یکی قطره ای</p></div>
<div class="m2"><p>نیارم که وصفش کنم ذره ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زصنعت چه گویم ایا بی نیاز</p></div>
<div class="m2"><p>کریم جهان داور و چاره ساز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زحکم تو انسان و وحش وطیور</p></div>
<div class="m2"><p>ابر خاک گشتند هریک ظهور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمهر تو هریک به رنگ دگر</p></div>
<div class="m2"><p>شده شادمان و برآورده سر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زصنع تو دانندگان جهان</p></div>
<div class="m2"><p>همه تاب گشتند وبی تاب وجان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زلطف تو یک چوب خشک از زمین</p></div>
<div class="m2"><p>شود سرو آزاده نازنین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زیک قطره آب منی،پیکری</p></div>
<div class="m2"><p>بسازی که گردد به مه،دلبری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شود راست قامت چو سرو سهی</p></div>
<div class="m2"><p>ابا هوش و با زیب وبا فرهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به جز تو که آرد به یک قطره آب</p></div>
<div class="m2"><p>دهد جان که گردد ابا نوش تاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نباتات کانی و وحش وطیور</p></div>
<div class="m2"><p>چه انسان چه حیوان چه از مارومور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ثناخوان شده دایم از قدرتت</p></div>
<div class="m2"><p>همه چشم دارند از رحمتت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به ویژه مرین بنده پرگناه</p></div>
<div class="m2"><p>که در جرم سختی شد عمرم تباه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ببودم همیشه گرفتارآز</p></div>
<div class="m2"><p>زرشک وطمع بود ما را نیاز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه پایی نهادم ابر راه تو</p></div>
<div class="m2"><p>به بودم به یک ذره آگاه تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زحکم تو گردن برون تافتم</p></div>
<div class="m2"><p>به راه پیمبر نه بشتافتم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه گفتم دمی ذکرت ای کردگار</p></div>
<div class="m2"><p>نه بشناختم مرد طاعت گذار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به گفتار دانای با فر وهوش</p></div>
<div class="m2"><p>نه بگشادم از غفلت و خواب،گوش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو کارم به چون وچرا در گذشت</p></div>
<div class="m2"><p>گناهم زاندازه ها درگذشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به هر ره که رفتم،درم بسته شد</p></div>
<div class="m2"><p>روان توانم زتن خسته شد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شبی خفتم از بهر آرام وخواب</p></div>
<div class="m2"><p>شد از کار زشتم دو دیده پرآب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نشد چیره بر من همی خواب ناز</p></div>
<div class="m2"><p>زخوابم همی داشت اندیشه باز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در این فکر بودم که پیش خدا</p></div>
<div class="m2"><p>چه عذرآورم تا ببخشد گناه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به بیچارگی سوی درمان شدم</p></div>
<div class="m2"><p>چواز کار خود زاروحیران شدم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به آخر چنین گشت راهم قرار</p></div>
<div class="m2"><p>که باید زجانم برآرم دمار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بیاوردم آن زهر قاتل به دست</p></div>
<div class="m2"><p>که آرد ابر جان آدم شکست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بخوردم که تا جان شیرین زتن</p></div>
<div class="m2"><p>برآید رود رویم اندر چمن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زصنعت نشد زهر کاریگرم</p></div>
<div class="m2"><p>روانم برون نامد از پیکرم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زحکم تو ای کردگار جهان</p></div>
<div class="m2"><p>هرآنچه بخواهی نباشد جزآن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در اندیشه بودم همی بهرآن</p></div>
<div class="m2"><p>چه خواهد شدن حال واحوالمان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که خواب از روانم همی چیره گشت</p></div>
<div class="m2"><p>به چشمم همی روشنی تیره گشت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زهاتف رسیدم ندایی به گوش</p></div>
<div class="m2"><p>به نزدیکم آمد خجسته سروش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یکی مرد دیدم چو سرو سهی</p></div>
<div class="m2"><p>ابا فرو آئین شاهنشهی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو گویی که خورشید رخشنده بود</p></div>
<div class="m2"><p>ویا آنکه ناهید تابنده بود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به دستش یکی جام بد آهنین</p></div>
<div class="m2"><p>نهاد آنگهی جام را بر زمین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زمین گشت لرزان و جانم نماند</p></div>
<div class="m2"><p>به تن،تاب وتوش وروانم نماند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زهیبت فرورفت پایم به گل</p></div>
<div class="m2"><p>همی بودم از حال خود منفعل</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در این فکر بودم که تا گفتگو</p></div>
<div class="m2"><p>کند با من آن سرو خوش رنگ و بو</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که ناگه به من بانگ زد آن چنان</p></div>
<div class="m2"><p>که از تن پریدم روان وتوان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>یکی کار کردی که تا جادوان</p></div>
<div class="m2"><p>به دنیا و عقبی بمانی نهان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شدستی زجان،سیر و خوردی چو زهر</p></div>
<div class="m2"><p>گرفت از تو یزدان بخشنده، قهر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همی زیر این جام، جای تو گشت</p></div>
<div class="m2"><p>رهایی نیابی از این کوه ودشت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو رفتی تو بر راه اهریمنی</p></div>
<div class="m2"><p>بود روزی تو همه ریمنی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نکردی تو کاری که یابی رها</p></div>
<div class="m2"><p>گرفتار گشتی به دام بلا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نه عقبی شناسی و نه مال کسان</p></div>
<div class="m2"><p>نبودی شناسای پیغمبران</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نه حق را شناسی و نه بنده را</p></div>
<div class="m2"><p>نگشتی ره راست،پوینده را</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به حال ضعیفان و بیچارگان</p></div>
<div class="m2"><p>ستم کردی ای مرد نامهربان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نکردی تو کاری که یارآیدت</p></div>
<div class="m2"><p>به سختی در اینجا به کار آیدت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>درآن دم که آرند حساب تو پیش</p></div>
<div class="m2"><p>نبینی به جز کار وکردار خویش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چه گویی جواب و سئوال،آن زمان</p></div>
<div class="m2"><p>پس وپیش تو گشته موج گران</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نه راه گریز است ونه زور وزر</p></div>
<div class="m2"><p>نباشد کسی مر تو را راهبر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>جهانت دمادم خبر می دهد</p></div>
<div class="m2"><p>خبر از رحیل سفر می دهد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>از این خواب،یک دم برون آرهوش</p></div>
<div class="m2"><p>همی از دل خود برآور خروش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شب وروز،ذکر خدای جهان</p></div>
<div class="m2"><p>بخوان تا ببخشد چو تو گمرهان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بگفت این وگشت از دو چشمم نهان</p></div>
<div class="m2"><p>پدیدآمد آنگه یکی بد نشان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بسی زشت و بد شکل و پتیاره بود</p></div>
<div class="m2"><p>به دستش یکی تیغ خونخواره بود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از آن سهمگین پیکر وروی او</p></div>
<div class="m2"><p>ازآن هیبت و دست و بازوی او</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به لرزه درآمد همی پیکرم</p></div>
<div class="m2"><p>بپرید هوش وروان از سرم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>زجا جستم و بر نشستم دمی</p></div>
<div class="m2"><p>نبودی کسی نزد من آدمی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>درآن دم بسی سخت و ترسان شدم</p></div>
<div class="m2"><p>به کردار خود سخت پیچان شدم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همی باشم از عفوت امیدوار</p></div>
<div class="m2"><p>چو مجرم که خواهد به جان،زینهار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ببخشای بر مابه روز حساب</p></div>
<div class="m2"><p>نبیند همی روح و جانم عذاب</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به پادافره این گناهم مگیر</p></div>
<div class="m2"><p>تو ای داور پاک پوزش پذیر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>پشیمان شدم من زکردار خویش</p></div>
<div class="m2"><p>زشرمت همی سرفکندم به پیش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>زدریای عصیان تنم غرق گشت</p></div>
<div class="m2"><p>چوموری که افتاده باشد به طشت</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>خدایا فتادم،تویی دستگیر</p></div>
<div class="m2"><p>تویی یار بیچارگان و فقیر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چو افتادم همچون خری در وحل</p></div>
<div class="m2"><p>زمهرت گشادم همی چشم ودل</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>که گیری تو دستم زلطف و ز رحم</p></div>
<div class="m2"><p>تو گر دست گیری،نداریم وهم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ایا نور چشم جهان بین پسر</p></div>
<div class="m2"><p>رضاباش بر سرنوشت وقدر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>گر از دهر،محنت بیابی و رنج</p></div>
<div class="m2"><p>مشو هیچ غمگین زکار سپنج</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بکن صبر ودل بر خداوند بند</p></div>
<div class="m2"><p>نشیب ونگون را کند حق بلند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>دوصد وای بر حال آن مردمان</p></div>
<div class="m2"><p>که از زهر،خود را کشند از نهان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ویا آن که خود را به دریا وچاه</p></div>
<div class="m2"><p>کند غرق تا گرددش جان،تباه</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>و یا آن کسی کز فراز و نشیب</p></div>
<div class="m2"><p>تن خویشتن بفکند از نهیب</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>که از خوار و زاری برآیدش جان</p></div>
<div class="m2"><p>نباشد به تقدیر خود شادمان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>هرآن کس بدین گونه خود را کشد</p></div>
<div class="m2"><p>سرش همت خالی زهوش وخرد</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>به دنیای او شوربختی بود</p></div>
<div class="m2"><p>به عقبای او رنج وسختی بود</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>روانش گرفتار باشد مدام</p></div>
<div class="m2"><p>به زیر یکی آهنین تیره جام</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>نیابد به تا روز محشر رها</p></div>
<div class="m2"><p>همیشه بود در دم اژدها</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>اگر غم ببینی به دنیا،مرنج</p></div>
<div class="m2"><p>که ما راست دایم نگهبان گنج</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>صبوری کن ودل به یزدان ببند</p></div>
<div class="m2"><p>که او پادشاهست ونیکی پسند</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>صبوری،کلید در بسته است</p></div>
<div class="m2"><p>صبوری،دوای دلی خسته است</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>گر از کارها صبر پیش آوری</p></div>
<div class="m2"><p>بسی بر نیاید کزو برخوری</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>شتابی چو تیغ و نبرد زره</p></div>
<div class="m2"><p>صبوری به آسان گشاید گره</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>مکن چشم حسرت به جاه کسان</p></div>
<div class="m2"><p>نه بر مال و لبس وکلاه کسان</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>مبر رشک بر ناز وشادی کس</p></div>
<div class="m2"><p>اگر نیست بر عشرتت دسترس</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>غم و شادمانی بود همچوباد</p></div>
<div class="m2"><p>مکن ای پسر از غم وناز،یاد</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>اگر چرخ گردون وگردان سپهر</p></div>
<div class="m2"><p>به قهر از تو برد به یکباره مهر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>مبادا که از تن،برون جان پاک</p></div>
<div class="m2"><p>کنون تا شوی غیب در زیرخاک</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>مخور زهر و تریاک وتن را به چاه</p></div>
<div class="m2"><p>میفکن که گردد روانت تباه</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>ایا مرد دانای به روزگار</p></div>
<div class="m2"><p>نیوش این نصیحت تو از شهریار</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>خدایا ابر درگهت شهریار</p></div>
<div class="m2"><p>دو چشمش گشاده همی ز انتظار</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>که از بحر رحمت ببخشی گناه</p></div>
<div class="m2"><p>به عقبی نگردد روانش تباه</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ابر روی نیکان بود پاک روز</p></div>
<div class="m2"><p>چوپاکان بود شاد و محشر فروز</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>تو بخشی گنهکارگان اثیم</p></div>
<div class="m2"><p>تویی قادر و کردگار رحیم</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>ایا خالق عقل وادراک وهوش</p></div>
<div class="m2"><p>ابر آستانت برآرم خروش</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>به حق بزرگیت ای لایزال</p></div>
<div class="m2"><p>که بر شاه دادی تو جاه وجلال</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>به حق زراتشت و اسفنتمان</p></div>
<div class="m2"><p>که آورد دین بهی در جهان</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>ازو دور شد کژی و کاستی</p></div>
<div class="m2"><p>بکرد آشکارا ره راستی</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>به دستور آذارباد گزین</p></div>
<div class="m2"><p>که بر روح پاکش هزار آفرین</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>به اسفندیار،آن شه نوجوان</p></div>
<div class="m2"><p>که بربست بر راه یزدان میان</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>پرستندگان بت هندو چین</p></div>
<div class="m2"><p>تهی کرد از جان ایشان زمین</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>به زاری طفلان بی مام وباب</p></div>
<div class="m2"><p>به زجر ضعیفان بی تو ش وتاب</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>به آه فقیران بی آب ونان</p></div>
<div class="m2"><p>به تیمار اندوه بیچارگان</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>به تیر و به کیوان و ناهید ومهر</p></div>
<div class="m2"><p>به بهرام و برجیس و ماه وسپهر</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>به دستور نیکو منش شهردین</p></div>
<div class="m2"><p>که او بسته دارد کمر بهر دین</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>به بهرامش آن موبد راستگو</p></div>
<div class="m2"><p>به کیخسرو وآفریدون گو</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>به سی وسه امشاسفندان پاک</p></div>
<div class="m2"><p>به نارو به باد و به آب وبه خاک</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>که از جرم و سختی مر او را رهان</p></div>
<div class="m2"><p>به عقبی روانم شود شادمان</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>در این دهرم از رنج،آزاد دار</p></div>
<div class="m2"><p>به گنج قناعت دلم شاد دار</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>زبعد وفاتم زمن یادگار</p></div>
<div class="m2"><p>بمانند فرزند شایسته کار</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>زنور عبادت شکوفد دلم</p></div>
<div class="m2"><p>نبینم زمانی ملال والم</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>به تا آن دمی کآیدم مرگ،پیش</p></div>
<div class="m2"><p>نباشد دمی قلبم ازدرد،ریش</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چو گردد برون ازتنم جان پاک</p></div>
<div class="m2"><p>گسسته عنصر آب وخاک</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>به آسانم از تن برآید روان</p></div>
<div class="m2"><p>روانم رود شادمان زان جهان</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>به مینو حضور زراتشت پاک</p></div>
<div class="m2"><p>روانم بود همچو خور،تابناک</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>کریما به هردر تویی یارما</p></div>
<div class="m2"><p>به هر در کنی شاد،بازارما</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>اگر یارگردی ویاری دهی</p></div>
<div class="m2"><p>مرا در جهان کامکاری دهی</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>منم بی کس وبنده بی هنر</p></div>
<div class="m2"><p>تویی خالق و قادر جان وسر</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>منم بنده سوگوار علیل</p></div>
<div class="m2"><p>طبیبی توای کردگار جلیل</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>زجرم و گنه شد دلم سوگوار</p></div>
<div class="m2"><p>چو شخصی که شد جان و چشمش نزار</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>طبیبی تو ای داور کبریا</p></div>
<div class="m2"><p>زلطفت یکی درد ما را دوا</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>دل ومغز و جانم شد از جرم،ریش</p></div>
<div class="m2"><p>ز رحمت یکی مرهم آور به پیش</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>که از مرهم عفو تو درد من</p></div>
<div class="m2"><p>شفا یابد ای داور ذوالمنن</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>زرحمت تو کن درد مارا دوا</p></div>
<div class="m2"><p>کنی شاد وخرم به هردو سرا</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>به غیر از تو یاری نخواهم زکس</p></div>
<div class="m2"><p>تو بیچارگان را دهی دسترس</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>مناجات این بنده خاکسار</p></div>
<div class="m2"><p>هرآن کس که خواند مرادش برآر</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>نویسنده را شاد و بی غم بدار</p></div>
<div class="m2"><p>وجودش زآلام،سالم بدار</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>هم آن کو خدا مرزی و روحش شاد</p></div>
<div class="m2"><p>ابر شهریار خدابخش داد</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>درین دار دنیا دلش شاد باد</p></div>
<div class="m2"><p>ز پیشش غم و درد،دوری کناد</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>روانش به مینو بود سرفراز</p></div>
<div class="m2"><p>امیدم چنین باشد ای بی نیاز</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>به خرداد روز و به خرداد ماه</p></div>
<div class="m2"><p>به وجد آمدم دل زعشق اله</p></div></div>