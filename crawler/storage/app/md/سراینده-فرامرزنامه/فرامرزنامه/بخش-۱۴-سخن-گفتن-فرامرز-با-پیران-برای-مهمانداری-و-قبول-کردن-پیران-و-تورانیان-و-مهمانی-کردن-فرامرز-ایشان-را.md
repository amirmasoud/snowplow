---
title: >-
    بخش ۱۴ - سخن گفتن فرامرز با پیران برای مهمانداری و قبول کردن پیران و تورانیان و مهمانی کردن فرامرز، ایشان را
---
# بخش ۱۴ - سخن گفتن فرامرز با پیران برای مهمانداری و قبول کردن پیران و تورانیان و مهمانی کردن فرامرز، ایشان را

<div class="b" id="bn1"><div class="m1"><p>شما را اگر دوستی در سر است</p></div>
<div class="m2"><p>می و جام اینجا مهیاتر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بپذرفت پیران از آن پیلتن</p></div>
<div class="m2"><p>بیامد بر نامدار انجمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گردان توران سراسر بگفت</p></div>
<div class="m2"><p>بماندند گردان از آن در شگفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زتورانیان بود هفتاد گرد</p></div>
<div class="m2"><p>فرامرزشان سوی آن خیمه برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برفتند در خیمه پور زال</p></div>
<div class="m2"><p>نشستند شادان و فرخنده فال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرامرز و بانو به تخت بلند</p></div>
<div class="m2"><p>نشستند شادان و دل ارجمند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر آن تخت فیروز چون ماه و خور</p></div>
<div class="m2"><p>نشستند آن هر دو آزاده سر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همین کرد آهو بر آتش کباب</p></div>
<div class="m2"><p>بخوردند با هم شراب و کباب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از این خوردنی ها که در خورد بود</p></div>
<div class="m2"><p>بیاورد و خوان ها بگسترد زود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کشیدند شایسته خوان سره</p></div>
<div class="m2"><p>زحلوا و هم نان و مرغ و بره</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برنجی معطر سرافشان به قند</p></div>
<div class="m2"><p>طبق ها فزون از چه و چون و چند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو از خوردنی ها بسی خورده شد</p></div>
<div class="m2"><p>دگرگونه خوان ها بگسترده شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می و رود و مجلس بیاراستند</p></div>
<div class="m2"><p>به هر گونه رامشگری خواستند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پری چهره ترکان صراحی به دست</p></div>
<div class="m2"><p>چو چشم خود از باده ناب مست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز می روی ساقی شده لاله رنگ</p></div>
<div class="m2"><p>نی اندر فغان بود و در ناله چنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرفته در و بام دود کباب</p></div>
<div class="m2"><p>به هم کرده آهنگ عود و رباب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نشسته دو آزاده با می به بزم</p></div>
<div class="m2"><p>ولیکن زره در بر و ساز رزم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تن هر دو بد در سلیح گران</p></div>
<div class="m2"><p>چنین گفت پیران بدان سروران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که امروز در دست با جام و بزم</p></div>
<div class="m2"><p>نیاید به تن خوشترین ساز رزم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شما گر ز می چهره گلگون کنید</p></div>
<div class="m2"><p>ز تن جامه جنگ بیرون کنید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فرامرز گفتا که باشد صواب</p></div>
<div class="m2"><p>برون آمد از ابر چون آفتاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همان زود بانو زره دور کرد</p></div>
<div class="m2"><p>چو خورشید آن خانه پر نور کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شد آن بزم روشن ز دیدار او</p></div>
<div class="m2"><p>به جان هر کسی شد خریدار او</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو بانو زره کرد بیرون زتن</p></div>
<div class="m2"><p>فرو ماند بیچاره شاه ختن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو شیده بدان روی او بنگرید</p></div>
<div class="m2"><p>دلش چون کبوتر زتن بر تپید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قدی دید چون سرو آزاده است</p></div>
<div class="m2"><p>رخی دید چون لب شکر داده است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خرد با همه خورد دانی که بود</p></div>
<div class="m2"><p>نیارست هیچ از دهانش ستود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دو ابروی او نقش بستم خیال</p></div>
<div class="m2"><p>چو بر ماه تابنده شکل هلال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از اندیشه ابرویش پیش من</p></div>
<div class="m2"><p>خیال کج آمد کج اندیش من</p></div></div>