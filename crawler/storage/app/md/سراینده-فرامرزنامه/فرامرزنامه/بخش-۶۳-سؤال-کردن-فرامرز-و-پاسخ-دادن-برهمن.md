---
title: >-
    بخش ۶۳ - سؤال کردن فرامرز(و) پاسخ دادن برهمن
---
# بخش ۶۳ - سؤال کردن فرامرز(و) پاسخ دادن برهمن

<div class="b" id="bn1"><div class="m1"><p>دگر گفت کای نامور مرد شنگ</p></div>
<div class="m2"><p>شنیدم به گیتیست پنهان نهنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه پیدا به مرد است و همراه مرد</p></div>
<div class="m2"><p>دوان مرد،او درپی آید چو گرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب و روز تازان و در پی دوان</p></div>
<div class="m2"><p>چو دیدش نبخشد زمانی زمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا وتو را درپی است این نهنگ</p></div>
<div class="m2"><p>نه جای درنگ ونه یارای جنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به پاسخ بدو گفت کای پهلوان</p></div>
<div class="m2"><p>حذرکن که ناگه بپرد روان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهنگ اندرون هفت خوانست و مرگ</p></div>
<div class="m2"><p>که برد یکایک همه شاخ و برگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو پویان و نازان و اندرز پی</p></div>
<div class="m2"><p>چه درویش با او چه سالار کی</p></div></div>