---
title: >-
    بخش ۱۲ - اندر شکار رفتن فرامرز با بانو گشسب
---
# بخش ۱۲ - اندر شکار رفتن فرامرز با بانو گشسب

<div class="b" id="bn1"><div class="m1"><p>پس آنگاه چون بخت و دولت به هم</p></div>
<div class="m2"><p>برفتند با کوس و چتر و علم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابا باز و شاهین و با چرخ و یوز</p></div>
<div class="m2"><p>برفتند آن هر دو یل کینه توز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکار افکنان تا به توران زمین</p></div>
<div class="m2"><p>که از چرخشان دل نبودی غمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گزیدند سرچشمه ای دلپذیر</p></div>
<div class="m2"><p>کشیدند از آن خسروانی سریر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همان سبز خیمه برافراختند</p></div>
<div class="m2"><p>چو شیران به نخجیرگه تاختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکشتند بسیار آهو و گور</p></div>
<div class="m2"><p>دویدند از چشمه نزدیک و دور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز هر گونه اسپان برانگیختند</p></div>
<div class="m2"><p>به آهو و گور اندر آویختند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به شمشیر و تیر و کمان و کمند</p></div>
<div class="m2"><p>بکشتند بسیار و کردند بند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنخجیر گشتند و گشتند باز</p></div>
<div class="m2"><p>به نزدیک آن خیمه دلنواز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز شبگیر بانو به صحرا و دشت</p></div>
<div class="m2"><p>فکندند صید اندر آن پهن دشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس آنگه نشستند با ناز و نوش</p></div>
<div class="m2"><p>برآورد جام از سرای سروش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدین گونه بودند تا قرص مهر</p></div>
<div class="m2"><p>فرو شد در این کارگاه سپهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به مغرب چو خورشید نزدیک شد</p></div>
<div class="m2"><p>ز گرد سپه روز تاریک شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو بانو به گرد اندران بنگرید</p></div>
<div class="m2"><p>سپاهی گران دید کامد پدید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سپاهی کم و بیش تا سی هزار</p></div>
<div class="m2"><p>همه دشت از ایشان گرفته غبار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فرامرز گفتا که این ها که اند</p></div>
<div class="m2"><p>به نزدیک ما آمده از چه اند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپهدار این لشکران کسیتند</p></div>
<div class="m2"><p>گذرشان بدین جانب از چیستند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنین داد پاسخ که توران سپاه</p></div>
<div class="m2"><p>گزینند هم اندرین شاه را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همانا درین نامدار انجمن</p></div>
<div class="m2"><p>سپهدار گردیست شمشیرزن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ولیکن تو دل را زغم دور کن</p></div>
<div class="m2"><p>به مرگ سپهدارشان سور کن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از این خیل لشکر میندیش هیچ</p></div>
<div class="m2"><p>ز بس تاب نادیده چندین مپیچ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که گر کینه ورزند ما آن کنیم</p></div>
<div class="m2"><p>که از آمدنشان پشیمان کنیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کشیدند بر مرکبان تنگ تنگ</p></div>
<div class="m2"><p>سواره ستادند بر عزم جنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وز آن سان چو لشکر کشیدند تنگ</p></div>
<div class="m2"><p>بدیدند آن خیمه سبز رنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عنان را کشیدند توران سپاه</p></div>
<div class="m2"><p>بدان خیمه کردند مردم نگاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سپهدار بد شیده شیرمرد</p></div>
<div class="m2"><p>چو پیران و هومان و فرشیدورد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو لهاک و گرسیوز تیزجنگ</p></div>
<div class="m2"><p>چو کلباد ویسه دلاور نهنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گروی و دمور و سپهرم دگر</p></div>
<div class="m2"><p>همان نیز نستیهن نامور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دگر پیلسم آن گو شیر نر</p></div>
<div class="m2"><p>دگر بود بسیار پرخاشخر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همانا که بودند هفتاد مرد</p></div>
<div class="m2"><p>دلیران و مردان روز نبرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دگرگونه با هریکی بد درفش</p></div>
<div class="m2"><p>چو سرخ و چو سبز و چو زرد و بنفش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از آن خیمه شان در دل آمد نهیب</p></div>
<div class="m2"><p>سر سرفرازان از آن در نشیب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بگفتند که امروز روز بلاست</p></div>
<div class="m2"><p>غم افزود ازو شادمانی بکاست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همانا که این خیمه رستمست</p></div>
<div class="m2"><p>که چون او نبرده به گیتی کم است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به عزم شکار اندر این مرغزار</p></div>
<div class="m2"><p>کمین کرده چون شیر در رهگذار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در این گفتگو بود توران سپاه</p></div>
<div class="m2"><p>که آن هر دو گرد از پس گرد راه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو گفتی به کین جنگ ساز آمدند</p></div>
<div class="m2"><p>چو نزدیک ایشان فراز آمدند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو پیران بدید آن دو آذرگشسب</p></div>
<div class="m2"><p>بیامد به نزدیک بانو گشسب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بپرسید کای نامور سرکشان</p></div>
<div class="m2"><p>بگویید با من زنام و نشان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چه نامید از خیل نام آوران</p></div>
<div class="m2"><p>کدام از دلیران و گندآوران</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فرامرز گفت ای سرانجمن</p></div>
<div class="m2"><p>منم پور رستم گو پیلتن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دگر نامداری چو آذر گشسب</p></div>
<div class="m2"><p>مرا خواهر و نام بانو گشسب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شکارافکنان هر طرف در گذار</p></div>
<div class="m2"><p>که جایی گرفتیم در مرغزار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شما باز گویید نام و نژاد</p></div>
<div class="m2"><p>به جنگ اندر آیید یا صلح و داد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو بشنید پیران فرامرز راد</p></div>
<div class="m2"><p>فرود آمد و خاک را بوسه داد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>که ای پهلوان زاده صلحست پیش</p></div>
<div class="m2"><p>عزیزید از مردم دیده بیش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به اولاد رستم منم چون رهی</p></div>
<div class="m2"><p>تو را بنده ام هر چه فرمان دهی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نباشد به از صلح در راه دین</p></div>
<div class="m2"><p>که گوید که نفرین به از آفرین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>فرامرز گفتا چه نامی به نام</p></div>
<div class="m2"><p>که هستی چو طوطی شیرین کلام</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به گفتار نیکو بگفتی سخن</p></div>
<div class="m2"><p>ولیکن نهانت ندانم زبن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به پاسخ چنین گفت پیران منم</p></div>
<div class="m2"><p>که بر دشمنان شما دشمنم</p></div></div>