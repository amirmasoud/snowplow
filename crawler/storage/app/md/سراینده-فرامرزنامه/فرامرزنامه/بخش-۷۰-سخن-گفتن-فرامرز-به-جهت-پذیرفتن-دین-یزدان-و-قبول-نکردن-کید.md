---
title: >-
    بخش ۷۰ - سخن گفتن فرامرز به جهت پذیرفتن دین یزدان و قبول نکردن کید
---
# بخش ۷۰ - سخن گفتن فرامرز به جهت پذیرفتن دین یزدان و قبول نکردن کید

<div class="b" id="bn1"><div class="m1"><p>پس از هفته بنشست با انجمن</p></div>
<div class="m2"><p>چو پروین گرانمایگان تن به تن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بفرمود تا هندوان سر به سر</p></div>
<div class="m2"><p>یکایک بیایند بسته کمر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شد انجمن خیل هندوستان</p></div>
<div class="m2"><p>چنین گفت پس مهتر سیستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که ای کید هندی بگردان خدای</p></div>
<div class="m2"><p>که او قادر و داور ورهنمای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان دان که او از مکان برتر است</p></div>
<div class="m2"><p>جهان بخش روزی ده و داور است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرآن کو ازین گفته آید فرود</p></div>
<div class="m2"><p>تنش را روان داد خواهد درود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به شمشیر گرشاسبی پیکرش</p></div>
<div class="m2"><p>ببرم به هامون نشانم سرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرآن کس که او گشت یزدان شناس</p></div>
<div class="m2"><p>جهید از بن تیغ و رست از هراس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرآن کس که او بت نخواهد شکست</p></div>
<div class="m2"><p>نخواهد ز تیغ فرامرز رست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نخستین چو با دین هویدا کنم</p></div>
<div class="m2"><p>رخ دین چو خورشید پیدا کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از آن پس بگوییم چیزی که هست</p></div>
<div class="m2"><p>به جیپال سازیم شمشیر دست</p></div></div>