---
title: >-
    بخش ۹ - جنگ کردن رستم با فرامرز و بانو گشسب (و) آزمودن رستم، ایشان را پنهان داشتن خود را در جنگ
---
# بخش ۹ - جنگ کردن رستم با فرامرز و بانو گشسب (و) آزمودن رستم، ایشان را پنهان داشتن خود را در جنگ

<div class="b" id="bn1"><div class="m1"><p>بدانست رستم که او سرکش است</p></div>
<div class="m2"><p>که در جنگ همچون که آتش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز آن پس به کین سوی او حیله کرد</p></div>
<div class="m2"><p>برآورد بر چرخ گردنده گرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو یل نیزه بر نیزه انداختند</p></div>
<div class="m2"><p>چو آتش به پیکار هم تاختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زره حلقه حلقه ز یکدیگران</p></div>
<div class="m2"><p>به نیزه ربودند آن سروران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نیزه ربودند از هم زره</p></div>
<div class="m2"><p>زره را گشودند از هم گره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرامرز از دور نظاره کرد</p></div>
<div class="m2"><p>همی دید آن هر دو یل در نبرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میان صد و شصت طعنه زنان</p></div>
<div class="m2"><p>ببودند نیامد یکی در زیان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرانجام هر دو بر آشوفتند</p></div>
<div class="m2"><p>بن نیزه را بر زمین کوفتند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نخستین برآورد بانو عمود</p></div>
<div class="m2"><p>پدر را یکی پیش دستی نمود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به پا ایستاد اندر آن صدر زین</p></div>
<div class="m2"><p>فرو کوفت بر پهلوان گرز کین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که شد رخش تا سینه اندر زمین</p></div>
<div class="m2"><p>بخوابید لب را به چشم و به کین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپر خورد گشت و بشد دست خم</p></div>
<div class="m2"><p>ولیکن فرو شد به دریای غم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل رخش را خون درآمد به جوش</p></div>
<div class="m2"><p>برآورد چون شیر شرزه خروش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدانست رستم که رخش دلیر</p></div>
<div class="m2"><p>بنالید از ضرب آن گرز چیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به دل گفت بانو هم آورد را</p></div>
<div class="m2"><p>برآورده ام از تنش گرد را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو مرکب سر خود بپیچید باز</p></div>
<div class="m2"><p>بدید او هم آورد با اسب و ساز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به جولان در افکند که کوب را</p></div>
<div class="m2"><p>به میدان درافکند آشوب را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به دل گفت رستم که اینست گرد</p></div>
<div class="m2"><p>که یارد به پیکار این دستبرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو از کینه نزدیک بانو رسید</p></div>
<div class="m2"><p>بزد دست گرز گران بر کشید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگفتش که ای دختر نامدار</p></div>
<div class="m2"><p>یکی ضرب دست مرا پای دار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو بفراخت او گرز بالای سر</p></div>
<div class="m2"><p>نهان گشت بانو به زیر سپر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پشیمان شده رستم از کین او</p></div>
<div class="m2"><p>نم آورد چشم جهان بین او</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه مردیست فرزند کشتن به جنگ</p></div>
<div class="m2"><p>که در پیش داناست این کار ننگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگرشان برآرم به خم کمند</p></div>
<div class="m2"><p>که شاید ازین بند گیرند پند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به زین اندر افکند گرز نبرد</p></div>
<div class="m2"><p>کمربند آن گرد بگرفت مرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به سر پنجه می خواست کو را ز زین</p></div>
<div class="m2"><p>رباید به مردی زند بر زمین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به پشت اندر افکند بانو سپر</p></div>
<div class="m2"><p>گرفت او کمربند آن شیر نر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همی زور کرد آن بر این این بر آن</p></div>
<div class="m2"><p>هوا بود جنبان و لرزان زمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سما چون زمین زردگون شد ز گرد</p></div>
<div class="m2"><p>زمین زیر پای یلان پر ز درد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همه خاک میدان ز خون نم گرفت</p></div>
<div class="m2"><p>ز زور دو پر دل زمین خم گرفت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دل هر دو در سینه آمد تپان</p></div>
<div class="m2"><p>شده خشک آن هر دو یل را دهان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز کینه دهانشان پر از گرد شد</p></div>
<div class="m2"><p>ز غصه روانشان پر از درد شد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بسی چیرگی کرد بانوگشسب</p></div>
<div class="m2"><p>که باب اندر آرد به نیروی اسب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نبودش توان و رخش زرد شد</p></div>
<div class="m2"><p>ازین غصه جانش پر از درد شد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سرانجام رستم بغرید سخت</p></div>
<div class="m2"><p>برآورد از زین به نیروی بخت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز انده رخش زرد و بیرنگ شد</p></div>
<div class="m2"><p>چو غنچه دل ماه دلتنگ شد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رخش گونه زعفرانی گرفت</p></div>
<div class="m2"><p>تنش لرزه و خیره زانی گرفت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو افکند مه را به خاک نژند</p></div>
<div class="m2"><p>فرود آمد آن پهلوان بلند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز فتراک بگشاد خم کمند</p></div>
<div class="m2"><p>که بندد دو بازوی سرو بلند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در آن دم فرامز یل در رسید</p></div>
<div class="m2"><p>درخشان یکی تیغ کین بر کشید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به بالای سر برد تیغ از فراز</p></div>
<div class="m2"><p>که تا برزند بر سر سرفراز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تهمتن نبودش مجال درنگ</p></div>
<div class="m2"><p>به زیر سپر شد نهان مرد جنگ</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنان بر سپر زد یل نامور</p></div>
<div class="m2"><p>که چون پرنیان شد دو نیمه سپر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدزدید رستم سر از روی کار</p></div>
<div class="m2"><p>وگرنه سر از تیغ گشتی فکار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زجا جست بانو چو یار آمدش</p></div>
<div class="m2"><p>برادر به مردی به کار آمدش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نشست از بر اسب بانو گشسب</p></div>
<div class="m2"><p>به یک سو کشید از بر باب اسب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تهمتن به جنگ فرامرز شیر</p></div>
<div class="m2"><p>درآمد چو غرنده شیر دلیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گرفت او کمربند فرزند را</p></div>
<div class="m2"><p>بیازرد فرزند دلبند را</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>یکی زور کرد آن یل زورمند</p></div>
<div class="m2"><p>کشیدش فرود از فراز سمند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>فرامرز بر جست از روی خاک</p></div>
<div class="m2"><p>گرفت او کمربند بی ترس و باک</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو پیلان آشفته اندر سماع</p></div>
<div class="m2"><p>همی بود با یکدگرشان نزاع</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همی بود از کینه همچون پلنگ</p></div>
<div class="m2"><p>یکی از ستیزه بسان نهنگ</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تو گفتی دو پیل اند اندر زمین</p></div>
<div class="m2"><p>بپیچید خرطوم درهم به کین</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زگردش فروماند چرخ فلک</p></div>
<div class="m2"><p>شده تیره از گرد چشم ملک</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به دل گفت رستم که بدکار شد</p></div>
<div class="m2"><p>که ما را به فرزند پیکار شد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مبادا شوم عاجز از جنگ او</p></div>
<div class="m2"><p>زیانی کشم آخر از چنگ او</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>که باشد هم آورد من پور من</p></div>
<div class="m2"><p>خردمند جنگی و دستور من</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو شیر است بانو گشسبم به رزم</p></div>
<div class="m2"><p>که باشد در پیش رزمم چو بزم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>فرامرز هم با دل اندیشه کرد</p></div>
<div class="m2"><p>از اندیشه دل را یکی تیشه کرد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>که ما را همانا بشد اندیشه کرد</p></div>
<div class="m2"><p>از اندیشه دل را یکی تیشه کرد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>که ما را همانا بشد بخت کور</p></div>
<div class="m2"><p>شده آب در پنجه سخت شور</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>درآورد برزه فلک چرخ کین</p></div>
<div class="m2"><p>که تیرم زند ناگهان از کمین</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>که گویند پور تهمتن ز درد</p></div>
<div class="m2"><p>زبون گشت از یک دلیر نبرد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سرم گر بگردد به خاک و به خون</p></div>
<div class="m2"><p>از آن به که گردم ز دشمن زبون</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>که گر آب دریا بپوشد تنم</p></div>
<div class="m2"><p>همان به که شادان شود دشنم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گهی پور پشت پدر داد خم</p></div>
<div class="m2"><p>گهی از پدر بد پسر پر زغم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بنالید رستم به یزدان پاک</p></div>
<div class="m2"><p>کای آفریننده آب و خاک</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>مکن پایمالم به دست پسر</p></div>
<div class="m2"><p>که پیش تو به باشد افکند سر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بگفت این و از دادگر خواست زور</p></div>
<div class="m2"><p>به نیروی دادار کیهان و هور</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گرفت آن کمربند پور جوان</p></div>
<div class="m2"><p>یکی زور کرد آن یل پهلوان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>دو زانو کشیدش به خاک نژند</p></div>
<div class="m2"><p>برو چیره شد پهلوان بلند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>همی خواست بازوی او داد خم</p></div>
<div class="m2"><p>که بانو درآمد چو شیر دژم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به چنگ اندرون تیغ آتش شرار</p></div>
<div class="m2"><p>کزو اژدها خواستی زینهار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تهمتن نبودش مجال درنگ</p></div>
<div class="m2"><p>چو با تیغ بانو در آید به تنگ</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بدو گفت کای دختر پهلوان</p></div>
<div class="m2"><p>شما را امانست امشب به جان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو فردا برآید خور از کوهسار</p></div>
<div class="m2"><p>بیارم به یاری خود کوهیار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>به گوهر مرا او برادر بود</p></div>
<div class="m2"><p>که در جنگ با من برابر بود</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>من او را بیارم فردا به گاه</p></div>
<div class="m2"><p>مگرتان بیابیم این جایگاه</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>در آرم شما را به خم کمند</p></div>
<div class="m2"><p>به توران برم پیش شه مستمند</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بدو گفت بانو که لافی مزن</p></div>
<div class="m2"><p>مرادی که هرگز نیابی ز من</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>که فردا بیاییم همراه هم</p></div>
<div class="m2"><p>برآریم جانت به باران غم</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>مگر آنکه نایی بدین ره گذر</p></div>
<div class="m2"><p>وگر آیی آرم دو چشمت به در</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>تهمتن یکی سخت سوگند خورد</p></div>
<div class="m2"><p>برآرنده گنبد لاجورد</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>که فردا برآرد خور از کوهسار</p></div>
<div class="m2"><p>بیایم بدین جای با کوه یار</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>به سوگند بانو زبان برگشاد</p></div>
<div class="m2"><p>که اینجا بود وعده بامداد</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بگفت این و از جا برانگیخت اسب</p></div>
<div class="m2"><p>دلیر و جهانگیر بانوگشسب</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>فرامرز را گفت رو تا رویم</p></div>
<div class="m2"><p>به آسایش خود به منزل رویم</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>برانگیخت رستم زجا تند رخش</p></div>
<div class="m2"><p>به ایوان در آمد یل تاج بخش</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ز تن دور کرد آن سلاح نبرد</p></div>
<div class="m2"><p>به درگه شد آن پهلوان شیر مرد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>به مغرب چو تنگ اندر آورد هور</p></div>
<div class="m2"><p>ز شب چون شبه گشت رنگ بلور</p></div></div>