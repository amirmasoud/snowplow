---
title: >-
    بخش ۱۶ - لاف زدن تمرتاش پیش افراسیاب برای گرفتن بانو گشسب و دلخوش نمودن گرسیوز افراسیاب او را
---
# بخش ۱۶ - لاف زدن تمرتاش پیش افراسیاب برای گرفتن بانو گشسب و دلخوش نمودن گرسیوز افراسیاب او را

<div class="b" id="bn1"><div class="m1"><p>ز رستم چه داری تو دل پر هراس</p></div>
<div class="m2"><p>مرا زو به مردی فزون تر شناس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گزینم ز لشکر ده و دو هزار</p></div>
<div class="m2"><p>همه پهلوانان خنجر گذار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از این جا روم تا به کابلستان</p></div>
<div class="m2"><p>بسوزم بر و بوم زابلستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه رستم بمانم نه دستان پیر</p></div>
<div class="m2"><p>ببارم در آن زهره باران تیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خون لعل سازم روان نعل اسب</p></div>
<div class="m2"><p>ز پرده کشم جعد بانو گشسب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز ایوان به گیسوش بیرون کشم</p></div>
<div class="m2"><p>ز تخت بزرگی به هامون کشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کام دل شاه و بخت بلند</p></div>
<div class="m2"><p>به خم کمان و به خام کمند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخواهم از او خون گردان تمام</p></div>
<div class="m2"><p>ز رستم نماند در آفاق نام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدو گفت گرسیوز بدفعال</p></div>
<div class="m2"><p>که ای پهلوان زاده بی همال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نباشد نیازت به کابلستان</p></div>
<div class="m2"><p>نه با رستم و جنگ زابلستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که بانو بود نز ره منزلش</p></div>
<div class="m2"><p>خوش افتاده در مرز توران دلش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سراپرده اش هست کوی سپهر</p></div>
<div class="m2"><p>فرامرز و بانو دو چون ماه و مهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه حاجت که تا زابلستان شوی</p></div>
<div class="m2"><p>به کینه بر پور دستان شوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ره دور بر خویش کوتاه کن</p></div>
<div class="m2"><p>بزودی تو اندیشه راه کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تمرتاش از این گفته دل شاد کرد</p></div>
<div class="m2"><p>روان را از اندیشه آزاد کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنین گفت کای نامداران تور</p></div>
<div class="m2"><p>کجا شیر ترسد ز یک دشت گور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>منم پشت این نامدار انجمن</p></div>
<div class="m2"><p>ندیده کسی در جهان پشت من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به هم پشت تیغ من از آفتاب</p></div>
<div class="m2"><p>کند روی کشور چو دریای آب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ستاره نشان سنان من است</p></div>
<div class="m2"><p>خمیده سپهر از کمان من است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شفق دامن آن روز در خون کشید</p></div>
<div class="m2"><p>که تیغم نگون شعله بیرون کشید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گشاده جهان از کمند من است</p></div>
<div class="m2"><p>سران را سر اندر به بند من است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سمندم چه جولان کند روز کین</p></div>
<div class="m2"><p>ز مسمار نعلم بجنبد زمین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به جان و سر و افسر و تخت شاه</p></div>
<div class="m2"><p>من اکنون بگیرم بر آن ماه راه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به نیروی بازوی گرز گران</p></div>
<div class="m2"><p>نمایم بدو دستبردی چنان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زخیمه به گیسوش بیرون کشم</p></div>
<div class="m2"><p>به خواریش بر روی هامون کشم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شهش آفرین کرد و آنگاه گفت</p></div>
<div class="m2"><p>که گرز تو با کوه خاراست جفت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر اینکه گفتی به جای آوری</p></div>
<div class="m2"><p>زر سرخ را زیر پای آوری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بیابی ز من تاج و تخت و نگین</p></div>
<div class="m2"><p>سرافراز گردی به توران زمین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرا گنج آباد و شهر آن تست</p></div>
<div class="m2"><p>ز دریا و خشکی دو بهر آن تست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تمرتاش گفتا که فردا پگاه</p></div>
<div class="m2"><p>سراپرده بیرون زنم با سپاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به گرز گران دست بیرون کنم</p></div>
<div class="m2"><p>ز خون دامن کوه هامون کنم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ببودند در آن شب در این گفتگو</p></div>
<div class="m2"><p>بسی لاف زد مرد پرخاشجوی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو خورشید بر خاور آورد روز</p></div>
<div class="m2"><p>سر از کان فیروزه برزد تموز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مر آن ترک گردنکش نامدار</p></div>
<div class="m2"><p>سلیح بر تن خویش کرد استوار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کمر بست در دست تیغ و تبر</p></div>
<div class="m2"><p>روان گشته با او عمود و سپر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به تندی سپه را به هامون کشید</p></div>
<div class="m2"><p>ز گرد سپه کوه شد ناپدید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>برآورده از ره تمرتاش گرد</p></div>
<div class="m2"><p>ز جوشن درون روی پرخاش کرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نشسته ابر خنگ پولادسم</p></div>
<div class="m2"><p>به برگستوان گشته از دیده گم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یکی ترک پولاد گوهر نگار</p></div>
<div class="m2"><p>به سر بر نهاده دلاور سوار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به تن در یکی جوشن خسروی</p></div>
<div class="m2"><p>دو تیغ گرانمایه پهلوی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یکی زان حمایل یکی در میان</p></div>
<div class="m2"><p>به بالای خفتانش ببر بیان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بیامد دمان تا بدان جا رسید</p></div>
<div class="m2"><p>بدان خیمه با ساز زر بنگرید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز مرکب فرود آمد از روی کین</p></div>
<div class="m2"><p>فرو زد بن نیزه را بر زمین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ببست اندر آن نیزه اسب نبرد</p></div>
<div class="m2"><p>پس آهنگ سوی همان خیمه کرد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زره دامنش را بزد بر میان</p></div>
<div class="m2"><p>به خیمه درون شد چو شیر ژیان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بغرید بر سان شیر و پلنگ</p></div>
<div class="m2"><p>گرفته یکی تیغ رخشان به چنگ</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو رخسار بانو نکو بنگرید</p></div>
<div class="m2"><p>تو گفتی دگر خویشتن را ندید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مهی دید رخشان بر افراز تخت</p></div>
<div class="m2"><p>بشد بیدل آن ترک برگشته بخت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بیفتاد شمشیر تیزش ز دست</p></div>
<div class="m2"><p>ز جام می عشق گردید مست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بگفتا که ای دختر پیلتن</p></div>
<div class="m2"><p>به یک ره که بردی دل از دست من</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به کین آمدم من ز درگاه شاه</p></div>
<div class="m2"><p>کنون مهر دارم به رخسار ماه</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تو را شیده شیدای دیدار شد</p></div>
<div class="m2"><p>به جان گوهرت را خریدار شد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بدان آمدم تا کنونت اسیر</p></div>
<div class="m2"><p>رسانم بدان خسروانی سریر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به چشمم کنون دید دیدار تو</p></div>
<div class="m2"><p>به یکبار جان شد خریدار تو</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همان به که خواهم چو بانوی شوی</p></div>
<div class="m2"><p>انیس دل و قوت جانم شوی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به چین اندرم هست فرماندهی</p></div>
<div class="m2"><p>همه چینیان پیش تختم رهی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به زور هنر اژدها پشت من</p></div>
<div class="m2"><p>نهنگ ژیان خوار در مشت من</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اگر دست بر کوه خارا زنم</p></div>
<div class="m2"><p>گران کوه را من ز جا برکنم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کنون ای نگار سمن بوی من</p></div>
<div class="m2"><p>مگردان بر آشفته این خوی من</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>که هر چند گردی و نام آوری</p></div>
<div class="m2"><p>به مردی نیابی گهر داوری</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ندانم که بیمم ز رستم بود</p></div>
<div class="m2"><p>که با زور او زور من کم بود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>که گر جنگ رستم به زخم و رکیب</p></div>
<div class="m2"><p>ز بالا فرود آورم سر نشیب</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به نام تمرتاش چینی لقب</p></div>
<div class="m2"><p>فزونست ز افراسیابم نسب</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به من رام شو چون که رام توام</p></div>
<div class="m2"><p>بدین سربلندی غلام توام</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>وگرنه چو من دست یازم به کار</p></div>
<div class="m2"><p>مبادا که غمگین شوی ای نگار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>برآشفت از این گفته بانو گشسب</p></div>
<div class="m2"><p>زجا جست مانند آذرگشسب</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو بانو بدان ترک نزدیک شد</p></div>
<div class="m2"><p>جهان بر جهانجوی تاریک شد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>زبیمش نهان شد به زیر سپر</p></div>
<div class="m2"><p>روان تیغ تیز از سپهرش به در</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ببرید سر تا به سر پیکرش</p></div>
<div class="m2"><p>روان کرد از تیغ خود در سرش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بریده چو برقی برون شد ز میغ</p></div>
<div class="m2"><p>که از خون نشانی نبودش به تیغ</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>دو پاره تنش کشته افتاد خوار</p></div>
<div class="m2"><p>ز خون شد چو گویی بر چشمه سار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>سراسر سرا پرده پر موج خون</p></div>
<div class="m2"><p>تمرتاش در موج خون سرنگون</p></div></div>