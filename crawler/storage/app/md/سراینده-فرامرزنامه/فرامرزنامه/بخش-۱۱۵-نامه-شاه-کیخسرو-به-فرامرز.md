---
title: >-
    بخش ۱۱۵ - نامه شاه کیخسرو به فرامرز
---
# بخش ۱۱۵ - نامه شاه کیخسرو به فرامرز

<div class="b" id="bn1"><div class="m1"><p>یکی پاسخ نامه فرمود شاه</p></div>
<div class="m2"><p>نخستین که بنوشت بر نیکخواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نام خداوند فیروزگر</p></div>
<div class="m2"><p>خداوند نیروی و فر وهنر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خداوند کیوان و بهرام وهور</p></div>
<div class="m2"><p>خداوند پیل و خداوند مور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازویست فیروزی و برتری</p></div>
<div class="m2"><p>کمی و فزونی و نیک اختری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازو بر فرامرز باد آفرین</p></div>
<div class="m2"><p>سپهدار شیر اوژن پاک دین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر افراز و پور جهان پهلوان</p></div>
<div class="m2"><p>خداوند شمشیر و گرز گران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرانمایه و گرد وباهوش و فر</p></div>
<div class="m2"><p>جوانمرد نیک اختر و شیرنر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به بزم اندرون،ابر گوهر نثار</p></div>
<div class="m2"><p>به رزم اندرون،ابر شمشیر بار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تویی شوکت و فر وبرزکیان</p></div>
<div class="m2"><p>به مردی و گردی کمر بر میان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رسید آنچه گفتی به نزدیک ما</p></div>
<div class="m2"><p>وزآن تازه شد رای باریک ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دل شاد گشتم زگفتار تو</p></div>
<div class="m2"><p>وزآن پرهنر جان بیدار تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فراوان سپاس از جهان آفرین</p></div>
<div class="m2"><p>پذیرفتم ای مهتر پاک دین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که چون تو سواری هژبر افکنی</p></div>
<div class="m2"><p>سرافرازگردی و شیر اوژنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به من داد تا من به کوپال تو</p></div>
<div class="m2"><p>به بازوی گردافکن و یال تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ببرم پی دشمنان از زمین</p></div>
<div class="m2"><p>بسوزم تن بددلان روز کین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تویی یادگار از نریمان و سام</p></div>
<div class="m2"><p>بمانی به گیتی همی شادکام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کنون ای سرافراز کشور گشای</p></div>
<div class="m2"><p>نوشتیم یک عهد پاکیزه رای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه مرز خرگاه و کشمیر هند</p></div>
<div class="m2"><p>سراسر چنان تا به دریای سند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپردیم یکسر به شاهی تو را</p></div>
<div class="m2"><p>پرستش کند مرغ و ماهی تو را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به شادی بباش و به خوبی بمان</p></div>
<div class="m2"><p>به فرخندگی بر خور و کامران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زمین یکسر از داد آباد کن</p></div>
<div class="m2"><p>خرد را به هر کار بنیاد کن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>میازار هرگز دل رادمرد</p></div>
<div class="m2"><p>به گرد درآز و انده مگرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زرنج کم آزار،پرهیز جوی</p></div>
<div class="m2"><p>سخن ها به نیکی وآزرم گوی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به دهقان و بازارگان رنج و کین</p></div>
<div class="m2"><p>نسازی که آید شکستت بدین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدین گونه بنوشت منشور شاه</p></div>
<div class="m2"><p>همی پند و اندرز پیش سپاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فرستاده را داد چندی درم</p></div>
<div class="m2"><p>نوازش بسی کرد از بیش و کم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فرستاده با خرمی بازگشت</p></div>
<div class="m2"><p>کبوتر بد از فر شه بازگشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چوآمد بر پهلوان شادکام</p></div>
<div class="m2"><p>یکایک بدادش درود و پیام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همان نامه و تاج و منشور شاه</p></div>
<div class="m2"><p>نهاد از بر تخت آن رزمخواه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنان گشت خوشنود شیرژیان</p></div>
<div class="m2"><p>که گویی برافشاند خواهد روان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زدادار بر شاه خواند آفرین</p></div>
<div class="m2"><p>که او شاد بادا به ایران زمین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدو تا جهانست آباد باد</p></div>
<div class="m2"><p>دل وبخت او خرم و شاد باد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زرنج و زتیمار،پرداخته</p></div>
<div class="m2"><p>همه کارگیتی بدو ساخته</p></div></div>