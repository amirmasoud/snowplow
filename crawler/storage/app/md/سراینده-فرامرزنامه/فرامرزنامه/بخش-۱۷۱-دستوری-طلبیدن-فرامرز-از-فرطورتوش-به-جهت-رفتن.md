---
title: >-
    بخش ۱۷۱ - دستوری طلبیدن فرامرز از فرطورتوش به جهت رفتن
---
# بخش ۱۷۱ - دستوری طلبیدن فرامرز از فرطورتوش به جهت رفتن

<div class="b" id="bn1"><div class="m1"><p>یکی روز شد پهلو نامور</p></div>
<div class="m2"><p>بر دادگر خسرو تاجور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدو گفت کای شاه با داد و راه</p></div>
<div class="m2"><p>بسی وقت باشد زسال و زماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برون آمدستم ز پیش پدر</p></div>
<div class="m2"><p>همان شاه کیخسرو تاجور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به من بر شبی نگذرد بی شتاب</p></div>
<div class="m2"><p>که من باب خود را نبینم به خواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه شهنشاه با هوش وفر</p></div>
<div class="m2"><p>جهاندار و گردنکش و نامور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسی شاد باشد که من زین دیار</p></div>
<div class="m2"><p>بوم شاد با رامش و میگسار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولیکن بسی روزگار دراز</p></div>
<div class="m2"><p>بباید شدن سوی آرام وناز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو این گفته بشنید فرطورتوش</p></div>
<div class="m2"><p>دلش زانده دختر آمدبه جوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به ناکام بایست دادن جواز</p></div>
<div class="m2"><p>فراوان بیاراستش برگ و ساز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زاسب و ز اشتر فزون از شمار</p></div>
<div class="m2"><p>بفرمود تا جمله کردند بار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زهرگونه آلت که بد در خورش</p></div>
<div class="m2"><p>زبهر جوان مرد و از لشکرش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زگنج و زدینار و از تاج وتخت</p></div>
<div class="m2"><p>بفرمود چندان شه نیک بخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که مرد مهندس شمارش ندید</p></div>
<div class="m2"><p>نه از نامداران پیشین شنید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو پر حواصل برآورد راغ</p></div>
<div class="m2"><p>برافروخت کیوان زنیکی،چراغ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سپهبد فرامرز روشن روان</p></div>
<div class="m2"><p>برون رفت با نامور سروران</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه شهر،پرناله ودرد شد</p></div>
<div class="m2"><p>رخ نیکخواهان زغم،زرد شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برفتند هرکس زخورد وبزرگ</p></div>
<div class="m2"><p>به همراه آن شیرمرد سترگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خروشان بپیمود فرسنگ بیست</p></div>
<div class="m2"><p>همی هرکس از بهر او خون گریست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ازو بازگشتند از آن پس به درد</p></div>
<div class="m2"><p>همه با غم و ناله و آه سرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو زو بازگردید فرطورتوش</p></div>
<div class="m2"><p>جوان سرافراز با رای وهوش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرازان به راه اندر آورد سر</p></div>
<div class="m2"><p>چو شیر ژیان در پی گورنر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به ره بر نکردش فراوان درنگ</p></div>
<div class="m2"><p>چو با مرز چین اندر آمد به تنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که پیوسته هندوان بود چین</p></div>
<div class="m2"><p>به قنوج نزدیک بود آن زمین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به زودی همی خواست مرد جوان</p></div>
<div class="m2"><p>کز آن ره گراید به هندوستان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کجا پانزده سال بگذشته بود</p></div>
<div class="m2"><p>کز ایشان سپهدار برگشته بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به هر سال،یک ره زگرد گزین</p></div>
<div class="m2"><p>فرستاده رفتی به ایران زمین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدو نیک،هر چش گذشتی به سر</p></div>
<div class="m2"><p>نمودی به باب و شه دادگر</p></div></div>