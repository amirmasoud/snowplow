---
title: >-
    بخش ۱۸۰ - صفت جنگ کردن خورشید
---
# بخش ۱۸۰ - صفت جنگ کردن خورشید

<div class="b" id="bn1"><div class="m1"><p>گروهی به خورشید یل بازخورد</p></div>
<div class="m2"><p>برانگیخت او اسب و برخاست گرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیفشرد خورشید یل نامجوی</p></div>
<div class="m2"><p>فرودآمدش هریک از پیش رو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو مرد از دلیران دشمن بکشت</p></div>
<div class="m2"><p>سرانجام،زخمی رسیدش درشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر رنجش آمد بر اسب دژم</p></div>
<div class="m2"><p>یکی سنگ نیز آمدش بر شکم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیفتاد خورشید و بر پای خاست</p></div>
<div class="m2"><p>کمرگاه برزد کمین کرد راست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو تیری به وی دشمن انداختی</p></div>
<div class="m2"><p>دگر باره ترکش نپرداختی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیاده گریزان و دشمن هزار</p></div>
<div class="m2"><p>به هر بیشه ای بر یکی کارزار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو دشمن بر او حمله انگیختی</p></div>
<div class="m2"><p>دگر باره ترکش فروریختی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو خورشید بگذشت بر نیمروز</p></div>
<div class="m2"><p>درافتاد خورشید در نیمروز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به زال آگهی شد که دشمن چه کرد</p></div>
<div class="m2"><p>زگردان برآورد یکباره گرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزد بر زمین خویش را همچو قاف</p></div>
<div class="m2"><p>بدرید جامه زبر تا به ناف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به چنگال،برکند موی سفید</p></div>
<div class="m2"><p>همی گفت زار ودریغا امید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو نیلوفر از دیدگان آب داد</p></div>
<div class="m2"><p>به دندان،سرانگشت را تاب داد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شدند انجمن در سرایش زنان</p></div>
<div class="m2"><p>همه دست ها بر سر و رو زنان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بتان هریکی کند گلنارون</p></div>
<div class="m2"><p>میان اندرون شاخ شاخ سمن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه مویه کردند بر یال او</p></div>
<div class="m2"><p>برآن پهلوانی بر و یال او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه خویشتن جنگ بد خوی تو</p></div>
<div class="m2"><p>کجا رفت گفتند نیروی تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همان پهلوانی بر و یال تو</p></div>
<div class="m2"><p>سر نیزه و گرز وکوپال تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عقاب از کمندت نگشتی رها</p></div>
<div class="m2"><p>گریزان زکید تو نراژدها</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گلت باد برد و نهالت بریخت</p></div>
<div class="m2"><p>تهمتن بمرد وزواره گریخت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دریغ از فرامرز وآن سرکشان</p></div>
<div class="m2"><p>که هرگز نیابی از ایشان نشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدین دودمان هرگز انده نبود</p></div>
<div class="m2"><p>به گردون رسیدست این درد و دود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سرایی که گردون ورا بنده بود</p></div>
<div class="m2"><p>به دست ستمکار، وی را ربود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سرایی کجا بود دیوان شکار</p></div>
<div class="m2"><p>زدشمن بباید کنون زینهار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بیا ای تهمتن ببین خوان تو</p></div>
<div class="m2"><p>که پرورده تست مهمان تو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بپروردی او را به رنج و نیاز</p></div>
<div class="m2"><p>کنون بر تو چنگال او شد دراز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بکشت این همه سروران ومهان</p></div>
<div class="m2"><p>فرامرز، پویان به گرد جهان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گروهی زده دست بر روی خویش</p></div>
<div class="m2"><p>همه مویه کردند بر شوی خویش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گروهی زنان دست بر چهره بر</p></div>
<div class="m2"><p>همه مویه کردند بر یکدگر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو شب گشت،دستان به خورشید گفت</p></div>
<div class="m2"><p>که مارا سرخویش باید نهفت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برو تا زدروازه بیرون شویم</p></div>
<div class="m2"><p>وزین شهر کنده به هامون شویم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یکی راه دانم به زیر زمین</p></div>
<div class="m2"><p>نداند کسی جز جهان آفرین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مگر جان از ایدر به شهری کشیم</p></div>
<div class="m2"><p>که تا جان سپاریم دشمن کشیم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدو گفت خورشید،خیره مگوی</p></div>
<div class="m2"><p>به چاه اندرون روشنایی مجوی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تورا دیده شد تیره و پشت،خم</p></div>
<div class="m2"><p>به شمشیر و گرز اندر آیی دژم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چگونه شوی سوی شهر دگر</p></div>
<div class="m2"><p>که نه اسب ماندست ما را نه زر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همه گرد بر گرد ما لشکرست</p></div>
<div class="m2"><p>جهان سر به سر پر ز اهریمنست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بگیرند ما را وباز آورند</p></div>
<div class="m2"><p>سر ما به دندان وکاز آورند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همان به که ایدر نهانی شویم</p></div>
<div class="m2"><p>به جان کسی بی گمانی شویم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شب آمد برفتند هردو به هم</p></div>
<div class="m2"><p>عنان پیش و اندر قفاشان ستم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یکی مرد بد دوست خورشید بود</p></div>
<div class="m2"><p>فراوان به خورشیدش امید بود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سوی خان او رفت با زال پیر</p></div>
<div class="m2"><p>کشاورز را گفت مهمان پذیر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همانگه به خانه درآوردشان</p></div>
<div class="m2"><p>همان زیر هیزم نهان کردشان</p></div></div>