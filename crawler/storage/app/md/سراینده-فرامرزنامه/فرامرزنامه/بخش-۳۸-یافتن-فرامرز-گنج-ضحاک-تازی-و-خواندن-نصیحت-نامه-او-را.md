---
title: >-
    بخش ۳۸ - یافتن فرامرز،گنج ضحاک تازی و خواندن نصیحت نامه او را
---
# بخش ۳۸ - یافتن فرامرز،گنج ضحاک تازی و خواندن نصیحت نامه او را

<div class="b" id="bn1"><div class="m1"><p>نوشته یکی تخته دیدند سنگ</p></div>
<div class="m2"><p>بدو داستانی پراز بوی و رنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوشته بسی گفته پند ارجمند</p></div>
<div class="m2"><p>ز ضحاک،زی پهلوان بلند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان ای فرامرز رستم که من</p></div>
<div class="m2"><p>به گیتی شده برتر از انجمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نهادم به گیتی بسی گنج و زر</p></div>
<div class="m2"><p>کشیده به خاک این سر تاجور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبهر تو ای سرور سیستان</p></div>
<div class="m2"><p>نهادم من این گنج هندوستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز زابل چو آیی بدین سرزمین</p></div>
<div class="m2"><p>ببری سر دیو بی آفرین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به فرت طلسمات دیو نژند</p></div>
<div class="m2"><p>همین گنج را کرده ام پای بند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببینی همین گنبد و جای من</p></div>
<div class="m2"><p>بدانی همین شاهی و رای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان دان که من در همین مرغزار</p></div>
<div class="m2"><p>به شادی نشستم بسی روزگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به مکر و به تدبیر واز رای وریو</p></div>
<div class="m2"><p>به دام آوریدیم کناس دیو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدین گنج کردم ورا پاسبان</p></div>
<div class="m2"><p>که تا گوش دارد به روز وشبان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وز آن صفه دیو بی جان کنی</p></div>
<div class="m2"><p>همان مرمر کوچه پیچان کنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بکن آن سر گنج و شیران بود</p></div>
<div class="m2"><p>هرآن کو برد گنج شیر آن بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ببر هرچه خواهی به کاوس کی</p></div>
<div class="m2"><p>که فرخنده باشی بر این بوم پی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرامرز رستم چو نامه بخواند</p></div>
<div class="m2"><p>از آن پند واندرزها خیره ماند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بفرمود کندن همان سنگ را</p></div>
<div class="m2"><p>بدید آن نشانی کنارنگ را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چهل نردبان دید کرده زعاج</p></div>
<div class="m2"><p>همه فرش دیبا و با تخت و تاج</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درازی صفه ز ده تیر بیش</p></div>
<div class="m2"><p>نگاریده دیوار بر گاومیش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نهاده چهل خم ز زر هر سویی</p></div>
<div class="m2"><p>زلعل و ز لولو بر پهلویی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مکلل به دیوار او شب چراغ</p></div>
<div class="m2"><p>فروزان یکایک به مثل چراغ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زشمشیر هندی و برگستوان</p></div>
<div class="m2"><p>به هر سویکی جامه پهلوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عقیق و زبرجد برو بر نگار</p></div>
<div class="m2"><p>همان لعل و فیروزه بد صدهزار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زبرجد سریر و زبیجاده تاج</p></div>
<div class="m2"><p>نهاده به هرگوشه کرسی زعاج</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گرانمایه هر گونه انگشتری</p></div>
<div class="m2"><p>نگینش چو رخساره مشتری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برون کرد گوهر که بود از نهفت</p></div>
<div class="m2"><p>همه لشکر هند شد زان شگفت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یکایک کشیدند زان گونه گنج</p></div>
<div class="m2"><p>زبردن،شه و لشکرآمد به رنج</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چوآمد سوی شهر نوشاد شاد</p></div>
<div class="m2"><p>همان تاج ضحاک بر سرنهاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سریر زبرجد چو بنهاد زیر</p></div>
<div class="m2"><p>پراکنده اندر جهان نام شیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ببخشید گنج و بپیمود رنج</p></div>
<div class="m2"><p>چنین باشد اندر سرای سپنج</p></div></div>