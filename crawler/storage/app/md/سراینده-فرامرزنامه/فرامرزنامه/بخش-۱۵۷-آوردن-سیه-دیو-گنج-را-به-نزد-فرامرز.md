---
title: >-
    بخش ۱۵۷ - آوردن سیه دیو،گنج را به نزد فرامرز
---
# بخش ۱۵۷ - آوردن سیه دیو،گنج را به نزد فرامرز

<div class="b" id="bn1"><div class="m1"><p>سیه دیو،پویان و تازان برفت</p></div>
<div class="m2"><p>خرامان سوی کوه تازید و رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زتاج و زتخت و ز در وگهر</p></div>
<div class="m2"><p>غلام و فرستنده و سیم وزر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم از اسب و از اشترو گوسفند</p></div>
<div class="m2"><p>زگاوان که برکوه و در می روند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیاورد چندان که هرکس که دید</p></div>
<div class="m2"><p>سرانگشت حیرت به دندان گزید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپهبد بدان گنج،خیره بماند</p></div>
<div class="m2"><p>نهانی همه نام یزدان بخواند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بپرسید از دیو کین خواسته</p></div>
<div class="m2"><p>کزو روی کشور شد آراسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگویی که چون گرد کردی همه</p></div>
<div class="m2"><p>چنین گوسفندان و اسب و رمه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیه دیو گفت ای جهان پهلوان</p></div>
<div class="m2"><p>زهنگام ضحاک تا این زمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدین کوه و این دشت دارم نشست</p></div>
<div class="m2"><p>جهانم چو یک مهره بد زیردست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدین کوه سر هر شب آتش کنم</p></div>
<div class="m2"><p>دل گمرهان را بدین خوش کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زنزدیک واز دور،هرکس که دید</p></div>
<div class="m2"><p>که آتش به گردون زبانه کشید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نباشند آگه زنزدیک من</p></div>
<div class="m2"><p>زتیغ وسنان و زآهنگ تن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیایند نزدیک من بی گمان</p></div>
<div class="m2"><p>اگر لشکری باشد ار کاروان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به یکبارشان پاک بی جان کنم</p></div>
<div class="m2"><p>دل و دیده شان زار و پیچان کنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه چارپا و همه خواسته</p></div>
<div class="m2"><p>نمانم که مویی شود کاسته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیارم نهم از برکوهسار</p></div>
<div class="m2"><p>تو این کوه را خوار مایه مدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که امروز اگر تا به صد سالیان</p></div>
<div class="m2"><p>ببینند این کوه کیشانیان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیابند از این کوه،دینار وگنج</p></div>
<div class="m2"><p>شوند از کشیدن سراسر به رنج</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زدینار و گنجم خود اندازه نیست</p></div>
<div class="m2"><p>همان گوهر و لعل از اینجا بسیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فرامرز از آن ماند اندر شگفت</p></div>
<div class="m2"><p>زگفتارش اندرزها برگرفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که این دیو چندین بپیمود آز</p></div>
<div class="m2"><p>بسی مردمان آوریده به کاز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بفرمود تا بارکرد آن همه</p></div>
<div class="m2"><p>به پیش اندر افکند گنج ورمه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سوی لشکر خویشتن کرد روی</p></div>
<div class="m2"><p>سیه دیو،پیش اندرون راه جوی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو خور از برباختر زد درفش</p></div>
<div class="m2"><p>همی گشت رنگ زمانه بنفش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سپهبد به لشکر گه خود رسید</p></div>
<div class="m2"><p>یکایک سران سپه را بدید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همه برنهادند برخاک،سر</p></div>
<div class="m2"><p>به نزد جهاندار فیروز گر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که دیدند او خرم و تندرست</p></div>
<div class="m2"><p>نبایست رخشان به خوناب شست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سپهبد نشست از بر تخت زر</p></div>
<div class="m2"><p>نشستند گردان بسته کمر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همه تازه روی وهمه شاد دل</p></div>
<div class="m2"><p>از اندوه بگذشته آزاد دل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یکی جشنگه ساخت آن پهلوان</p></div>
<div class="m2"><p>که گفتی برافشاند گردون روان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زشادی بخندید می در قدح</p></div>
<div class="m2"><p>زعکسش هوا پر زقوس وقزح</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پری چهرگان،دسته گل به دست</p></div>
<div class="m2"><p>به کف،ساغر می همه نیم مست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>رخ لاله گون از عرق پر گلاب</p></div>
<div class="m2"><p>چو بر برگ نسرین چکیده شراب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بنالید ابریشم زبر زار</p></div>
<div class="m2"><p>ز آواز او مست شد هوشیار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو از باده سرمست شد سرفراز</p></div>
<div class="m2"><p>به دل اندرش مهر آن دلنواز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شکیب از دل و جانش دوری گزید</p></div>
<div class="m2"><p>همی هر زمان لب به دندان گزید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه برآرزو خورد جامی که خورد</p></div>
<div class="m2"><p>همی برزدی هر نفس باد سرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بزرگان لشکر همه تن به تن</p></div>
<div class="m2"><p>یکایک بگفتند بر انجمن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کیانوش دستور جنگی تخوار</p></div>
<div class="m2"><p>چو شیروی و اشکش یل نامدار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>که این شیر دل مهتر نامور</p></div>
<div class="m2"><p>سرافراز و با دانش و پرهنر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از این گونه آمد کزیدر برفت</p></div>
<div class="m2"><p>ندانیم تا بر سر او چه رفت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نبینیم رنگ رخش تازه رو</p></div>
<div class="m2"><p>همی برکشد هر زمان سردهو</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از آن نامداران که با او بدند</p></div>
<div class="m2"><p>همه راز داران دلجو بدند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گشادند جمله سراسر زبان</p></div>
<div class="m2"><p>بگفتند با نامور پهلوان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زکار بیابان و آهو و گرگ</p></div>
<div class="m2"><p>همان آتش وکار دیو سترگ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در آن مرغزاران و آب روان</p></div>
<div class="m2"><p>وزآن رفتن نامور پهلوان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سوی چشمه ساران که در وی پری</p></div>
<div class="m2"><p>نشسته چو بر آسمان،مشتری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بدان ماه رخ،پهلوان شیفتست</p></div>
<div class="m2"><p>همانا کش آن دیو بفریفتست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گوان چون شنیدند گفتارشان</p></div>
<div class="m2"><p>زاندیشه شد تیز بازارشان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>می چند خوردند از آن بزمگاه</p></div>
<div class="m2"><p>برفتند چون رنگ شب شد سیاه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>جهان چادر تیره بر سرکشید</p></div>
<div class="m2"><p>شد از تیرگی رنگ خور ناپدید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>برفتند گردان به آیین خویش</p></div>
<div class="m2"><p>سپهبد،سیه دیو را خواند پیش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بپرسید ازو شاه فرطورتوش</p></div>
<div class="m2"><p>همان شاه بینای با رای وهوش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گرت هیچ هست آگهی بازجوی</p></div>
<div class="m2"><p>وگر چارهای نیز دانی بگوی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بدو دیو گفت ای گرانمایه مرد</p></div>
<div class="m2"><p>سرافراز جنگی به روز نبرد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به گیتی نگوید کسی نام اوی</p></div>
<div class="m2"><p>نیارد کسی جستن آرام روی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>که او بر پری سر به سر پادشاست</p></div>
<div class="m2"><p>سپهدار و با داد و فرمانرواست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>همان کرگساران مازندران</p></div>
<div class="m2"><p>به فرمان او از کران تا کران</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همین مرز کاینجاست ما را نشست</p></div>
<div class="m2"><p>سر مرز آن نامور خسرو است</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>روارو چنان تا در باختر</p></div>
<div class="m2"><p>همه بسته دارند پیشش کمر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>جهانی برآورده زیر اندرون</p></div>
<div class="m2"><p>سپاهش ز ریگ بیابان فزون</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ازیدر که ماییم تا پیش او</p></div>
<div class="m2"><p>به یک ماه پویان رود راه جو</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو فرمان دهی با سواری هزار</p></div>
<div class="m2"><p>ازیدر ببندم کمر بنده وار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>رسانم بدان شاه،فرمان تو</p></div>
<div class="m2"><p>ازآن جا برآرم مگر کام تو</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>یکی نامه بنویس با مهر وداد</p></div>
<div class="m2"><p>چنان چون سزاوار مردم بواد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به نامه سخن های بی رزم گوی</p></div>
<div class="m2"><p>همه گفتنی های با بزم جوی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>که او شهریار است و با کام و رای</p></div>
<div class="m2"><p>بزرگ و سپرده جهان زیر پای</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به گفتار شیرین و آوای نرم</p></div>
<div class="m2"><p>دلش رام گردان زمهر و زشرم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>سپهبد چو بشنید ازو شادگشت</p></div>
<div class="m2"><p>زتیمار آن دلبر آزاد گشت</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بخفت و برآسود تا روز پاک</p></div>
<div class="m2"><p>چو از شید،رخشیده شد روی خاک</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>یکی معجز از زر و یاقوت ناب</p></div>
<div class="m2"><p>بگسترد بر روی خاک،آفتاب</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>سپهدار بنشیت و نام آوران</p></div>
<div class="m2"><p>سواران گردنکش و مهتران</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>برفتند پیشش پراندیشه دل</p></div>
<div class="m2"><p>زاندوه آن نامور پا به گل</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نهانشان بدانست آن پهلوان</p></div>
<div class="m2"><p>به ایشان چنین گفت کای سروران</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>همانا که از کار من در به در</p></div>
<div class="m2"><p>به گوش بزرگان رسیده خبر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>که در دل چه دارم همی آرزوی</p></div>
<div class="m2"><p>سزد گر شوندم یکی چاره جوی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ابا آن که این آرزو اندکیست</p></div>
<div class="m2"><p>بکوشیم تا درتن و جان رگیست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>زدیو و زجادو واز اژدها</p></div>
<div class="m2"><p>زدریای ژرف و دژ واز بلا</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گذشتم زهر کشور گرم وسرد</p></div>
<div class="m2"><p>کشیدیم و دیدیم تیمار ودرد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>سپاسم زدادار فیروزگر</p></div>
<div class="m2"><p>که مارا بدادست مردی وفر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ودیگر کزین نامداران من</p></div>
<div class="m2"><p>یکی گم نگشتند از آن انجمن</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>سپهدار اگر چند نام آورست</p></div>
<div class="m2"><p>زمردی زنام آوران برتر است</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>یکی کار ماندستمان با پری</p></div>
<div class="m2"><p>بساییم دست اندرین داوری</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>که این کام دیگر در این روزگار</p></div>
<div class="m2"><p>به فیروزی دادگر کردگار</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>برآید به ما داستانی شود</p></div>
<div class="m2"><p>که هرکس که این داستان بشنود</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>زما مهر یزدان بیارد به یاد</p></div>
<div class="m2"><p>بمانیم خرم دل و بخت شاد</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>پس از هرکه نامی بود در جهان</p></div>
<div class="m2"><p>که هرگز به گیتی نگردد نهان</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>همه پهلوانان سرافراختند</p></div>
<div class="m2"><p>همه پاسخش را بیاراستند</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>که برماتو را کام،فرمانرواست</p></div>
<div class="m2"><p>مراد تو یکسر همه کام ماست</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>اگر کوه و دریا شود پر زتیغ</p></div>
<div class="m2"><p>و گر تیغ بارد زبارنده میغ</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>زبهر مراد تو ای پهلوان</p></div>
<div class="m2"><p>نباشد دریغ از بزرگان روان</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>سپهبد زگفتارشان تازه گشت</p></div>
<div class="m2"><p>به دل،آرزوها بی اندازه گشت</p></div></div>