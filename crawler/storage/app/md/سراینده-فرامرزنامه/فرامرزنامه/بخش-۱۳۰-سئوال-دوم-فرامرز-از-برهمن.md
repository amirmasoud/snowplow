---
title: >-
    بخش ۱۳۰ - سئوال دوم فرامرز از برهمن
---
# بخش ۱۳۰ - سئوال دوم فرامرز از برهمن

<div class="b" id="bn1"><div class="m1"><p>دگر گفت کای مرد پاکیزه رای</p></div>
<div class="m2"><p>مرا سون دانش یکی ره نمای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گیتی چه نیکوست نزد خرد</p></div>
<div class="m2"><p>که دانا بدان جان همی پرورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همان بد به گیتی چه چیز است نیز</p></div>
<div class="m2"><p>کجا با خرد باشد اندر ستیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برهمن بدو گفت کای هوشیار</p></div>
<div class="m2"><p>چو پاسخ بیابی زمن گوش دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکویی،پرستیدن دادگر</p></div>
<div class="m2"><p>جز او را مدان پادشاهی دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازو دان شب وروز و رخشنده ماه</p></div>
<div class="m2"><p>بزرگی و فیروزی و دستگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان دان که بی امر یزدان پاک</p></div>
<div class="m2"><p>نه آب و نه آتش نه باد ونه خاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به سوزندگی وبه سازندگی</p></div>
<div class="m2"><p>بدو بر بکردند یکبارگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندارند با کام خود دسترس</p></div>
<div class="m2"><p>به فرمان اویند جاوید و بس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه او را بخوانی بدان سان که اوست</p></div>
<div class="m2"><p>تو را از پرسیدنش بس نکوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پرسیدن او بگویم تو را</p></div>
<div class="m2"><p>روان از بدی ها بشویم تو را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زیزدان بیندیش و روز شمار</p></div>
<div class="m2"><p>زپادافره دوزخش بیم دار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زکاری که خوشنودی ایزد است</p></div>
<div class="m2"><p>از آن رخ متاب ای که آنت بهست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نگهدار دین باش و جویای دین</p></div>
<div class="m2"><p>زدیندار بر دل مدار هیچ کین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو بر کام باشد تو را دسترس</p></div>
<div class="m2"><p>میازار از آن پس دل هیچ کس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ابا نیکمردان،سخن نرم گوی</p></div>
<div class="m2"><p>به گفتن،ره شرم و آزرم جوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به دل،نیکویی دار و زفتی مکن</p></div>
<div class="m2"><p>ابا دیو وارونه جفتی مکن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که زفتی زداد خداوند نیست</p></div>
<div class="m2"><p>جهاندار از این کار خرسند نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرآنگه که نیکی کنی با کسی</p></div>
<div class="m2"><p>مکن یاد از کار نیکی بسی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که از گفتنت دل شکسته شود</p></div>
<div class="m2"><p>در نیکویی بر تو بسته شود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زبان را زگفتار بد بند کن</p></div>
<div class="m2"><p>دل ودیده سوی خردمند کن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زگفتار نیکو گریزان مشو</p></div>
<div class="m2"><p>به ناکامی اندر غریوان مشو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو کارتو ناید به کام تو راست</p></div>
<div class="m2"><p>برآن رو که فرمان و امر خداست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کجا آفریننده گرم و سرد</p></div>
<div class="m2"><p>جهان را نه برآرزوی تو کرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زنیکو هر آنچت جهاندار داد</p></div>
<div class="m2"><p>همی باش خشنود و خرسند وشاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زخرسندی،ایزدت بیشی دهد</p></div>
<div class="m2"><p>دلت را چو باشکر خویشی دهد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وزو دار پیوسته شکر وسپاس</p></div>
<div class="m2"><p>همیشه همی باش اندر هراس</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همان چون رسیدی زیزدان به کام</p></div>
<div class="m2"><p>برآمد تو را گنج و خوبی ونام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ببخش و بخور دیگرت خود دهد</p></div>
<div class="m2"><p>یکی را جهان آفرین صد دهد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به دل،مهربان و به کف،راد باش</p></div>
<div class="m2"><p>زغم خوردن گیتی آزاد باش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به بی کامی کس مشو شادمان</p></div>
<div class="m2"><p>که نزدیک تو راه دارد همان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زبدها بیندیش و مپسند بد</p></div>
<div class="m2"><p>که گردی پشیمان تو نزد خرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دروغ از بنه هیچ گونه مگوی</p></div>
<div class="m2"><p>که تیره شود در جهان آبروی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به هرکار در راستی پیشه کن</p></div>
<div class="m2"><p>مگردان زبان را به کج در سخن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز رشک و حسد جاودان دور باش</p></div>
<div class="m2"><p>به آزادی از دهر کن نام فاش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>حسد چیره رشک آورد دل گسل</p></div>
<div class="m2"><p>بود نزد نیکان همیشه خجل</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زدور حسد مرد پرهیز جوی</p></div>
<div class="m2"><p>سخن نزد او از بنه خود مگوی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دلت را به گفتار بد گوش دار</p></div>
<div class="m2"><p>ولیکن به کردار خود هوش دار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به جای تو آن کس که جوید بدی</p></div>
<div class="m2"><p>تو با او مکن بد اگر بخردی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گرت دست باشد نکویی نمای</p></div>
<div class="m2"><p>که نیکی برآید به نزد خدای</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گراو بدکند پس توهم بدکنی</p></div>
<div class="m2"><p>بر نیک مردی به چاه افکنی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو نزد تواز نیک و بد فرق نیست</p></div>
<div class="m2"><p>بدان رای و دانش بباید گریست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کم آزاری از هرچه جویی بهست</p></div>
<div class="m2"><p>کم آزار بر مهتران هم مهست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به هنگام خشم ار ببخشی گناه</p></div>
<div class="m2"><p>پسندیده تر زان مدان هیچ راه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ازین گونه چندان که گویم سخن</p></div>
<div class="m2"><p>زگفتار،یک شمه ناید به بن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زپیر جهان دیده می نوش پند</p></div>
<div class="m2"><p>دل اندر فریب زمانه مبند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که گیتی نماند به کس جاودان</p></div>
<div class="m2"><p>همان به که رادی بود در میان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو گفت برهمن به پایان رسید</p></div>
<div class="m2"><p>دل پهلوان از خرد بردمید</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بسی آفرین کرد بر برهمن</p></div>
<div class="m2"><p>ستایش نمودش در آن انجمن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>که یزدان زداننده خوشنود باد</p></div>
<div class="m2"><p>همه مزد او نیکویی ها دهاد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بیاسود جانم زگفتار تو</p></div>
<div class="m2"><p>دلم گشت تازه به دیدارتو</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>فراوان زیاقوت و در و گهر</p></div>
<div class="m2"><p>هم ا زجامه و اسب وهم سیم وزر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سپهبد به گنجور گفتا بیار</p></div>
<div class="m2"><p>بدان تا ببخشد به آموزگار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو آورد دانا نپذرفت ازوی</p></div>
<div class="m2"><p>بدو گفت کای مهتر رزمجوی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>برهمن به این ها ندارد نیاز</p></div>
<div class="m2"><p>به دینار و گوهر نیاید فراز</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>اگرمن بدین آرزو دارمی</p></div>
<div class="m2"><p>جهان را به گوهر بانبارمی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همه کوهساران ما این بود</p></div>
<div class="m2"><p>مرا دل زدینار،پرکین بود</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>خرد گر به دینار بسته شود</p></div>
<div class="m2"><p>دل از کام و امید خسته شود</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>زیزدان بدین بازماند دلم</p></div>
<div class="m2"><p>پس آنگاه دل را زتن بگسلم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ازآن پس برآمد به جای نشست</p></div>
<div class="m2"><p>ابا او سپهدار،دستش به دست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به دریا کنار وبه هامون و کوه</p></div>
<div class="m2"><p>بسی گشت تا شد زگشتن ستوه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>یکایک نمودش همه دشت ودر</p></div>
<div class="m2"><p>پر از لعل و یاقوت و در و گهر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>شگفت آمدش پهلوان کان بدید</p></div>
<div class="m2"><p>پر اندیشه لب را به دندان گزید</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>از آنجا بیامد سوی بارگاه</p></div>
<div class="m2"><p>سخن گفت با موبدان و سپاه</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو بنشست در بارگه نامور</p></div>
<div class="m2"><p>برهمن بیاورد با خویش بر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سپهدار برتختش بنشاختش</p></div>
<div class="m2"><p>زخوبی بسی آفرین ساختش</p></div></div>