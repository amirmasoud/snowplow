---
title: >-
    بخش ۲۲ - جنگ کردن بانوگشسب با جیپورشاه (و) شکست خوردن جیپورشاه در صف جنگ
---
# بخش ۲۲ - جنگ کردن بانوگشسب با جیپورشاه (و) شکست خوردن جیپورشاه در صف جنگ

<div class="b" id="bn1"><div class="m1"><p>به جیپور گردید بانو دوچار</p></div>
<div class="m2"><p>درآمد به پیکار آن نامدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برو بریکی نیزه زد کز نهیب</p></div>
<div class="m2"><p>شدش از بدن جان و پا از رکیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چنگال جیپال را دست برد</p></div>
<div class="m2"><p>کمربند او را گرفت و فشرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی کشته گشت و دگر را بخست</p></div>
<div class="m2"><p>چو رای آن چنان دید زآنجا بجست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نزدیک زال آمد از رزمگاه</p></div>
<div class="m2"><p>زبانو بر زال برد او پناه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زجا جست بانو گرفتش به بر</p></div>
<div class="m2"><p>بدو گفت اکنون سپاهت ببر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپردم به تو ملک هندوستان</p></div>
<div class="m2"><p>به تخت بهی باش با دوستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو را شاه جیپال کهتر بود</p></div>
<div class="m2"><p>ز هر سه تو را پایه بهتر بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببوسید رای گزین تخت او</p></div>
<div class="m2"><p>زجان آفرین کرد بر تخت او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس آنگه بزودی سپه را ببرد</p></div>
<div class="m2"><p>دگر بر زبان نام بانو نبرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز روم و ز چین و ز ترک و تتار</p></div>
<div class="m2"><p>هرآن کس که بروی شد خواستار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو با وی به کشتی بشد بازپس</p></div>
<div class="m2"><p>نبد مرد میدان او هیچ کس</p></div></div>