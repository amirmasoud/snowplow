---
title: >-
    بخش ۵۵ - جواب دادن پیر برهمن، فرامرز را
---
# بخش ۵۵ - جواب دادن پیر برهمن، فرامرز را

<div class="b" id="bn1"><div class="m1"><p>بدو پاسخ آورد پس هوشمند</p></div>
<div class="m2"><p>خرد خوانم آن دار شاخ بلند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا شاخ آن مهتر از گنبد است</p></div>
<div class="m2"><p>هر آن کو بدان بر شود موبد است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ورا برگ پیرایه ای بر سخن</p></div>
<div class="m2"><p>همی شاخ او می نگردد کهن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زچیزی که دانی خرد مهتر است</p></div>
<div class="m2"><p>زحرفی که داری خرد بهتر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیرزد به چیزی سر بی خرد</p></div>
<div class="m2"><p>که دانا مرو را به کس نشمرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خردمندی آموز و تدبیر جوی</p></div>
<div class="m2"><p>نه جنگ و سواری ومیدان و گوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وجود تو شهریست پرنیک وبد</p></div>
<div class="m2"><p>تو سلطان و دستور دانا خرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی باغ بینی همه سبز و خوش</p></div>
<div class="m2"><p>درختان، همه تازه و خوب و کش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سراسر همه پرگل ونسترن</p></div>
<div class="m2"><p>همه طوطی و قمری است وزغن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه میوه تلخ و شیرین و بوی</p></div>
<div class="m2"><p>شود نامی از دیدنش جنگجوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرآن باغ را نام بینی خرد</p></div>
<div class="m2"><p>که دانا مرو را به صد جان خرد</p></div></div>