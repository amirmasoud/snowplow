---
title: >-
    بخش ۵۰ - جنگ گردن کید و همه هندیان با فرامرز و ایرانیان و گریختن کید و لشکراو
---
# بخش ۵۰ - جنگ گردن کید و همه هندیان با فرامرز و ایرانیان و گریختن کید و لشکراو

<div class="b" id="bn1"><div class="m1"><p>چو دریا به موج اندر آمد سپاه</p></div>
<div class="m2"><p>شد از میمنه بیژن نیک خواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپه را بفرمود یکسر روید</p></div>
<div class="m2"><p>همه از میان تیغ هندی کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دریا به قلب اندر آمد سپاه</p></div>
<div class="m2"><p>زچپ گستهم شد گو رزمخواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرامرز،پیش و دلیران ز پس</p></div>
<div class="m2"><p>چو مردار برکرکسان را هوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو رویه سپه اندر آمیختند</p></div>
<div class="m2"><p>زمین را زخواب خوش انگیختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شه از نیزه چو نیستان و سپهر</p></div>
<div class="m2"><p>نهان شد به گرد اندرون ماه ومهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بشورید گیتی چو دریای قیر</p></div>
<div class="m2"><p>فلک، مه سپرکرد زآشوب تیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زنیزه شد نیستان دشت هند</p></div>
<div class="m2"><p>زمین شور بد او چو دریای سند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روان شد درآن دشت کین، جوی خون</p></div>
<div class="m2"><p>شکسته سر ودست گردان فزون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به لرزه درآمد زمین از ستور</p></div>
<div class="m2"><p>زالماس شیران بترسید هور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر گاو ماهی زنعل سوار</p></div>
<div class="m2"><p>بتوفید بر شد به گردان غبار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیفشاند خون از تن ژنده پیل</p></div>
<div class="m2"><p>به کردار خون بر سر رود نیل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرامرز یل همچو درنده گرگ</p></div>
<div class="m2"><p>درآمد به قلب سپاه بزرگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به الماس ضحاک بنهاد دست</p></div>
<div class="m2"><p>بسا پیکر مرد زو گشت پست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از آن تیغ آتش وش آبدار</p></div>
<div class="m2"><p>بسی پشت شد دست خنجرگذار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به فیروز و بهروز ناگه رسید</p></div>
<div class="m2"><p>دل هردو زان شیر نر بردمید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو آتش دمیدند زی پهلوان</p></div>
<div class="m2"><p>برآمد ده و گیر زان هردوان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به شمشیر هندی و کوپال و خشت</p></div>
<div class="m2"><p>زد و خون روان شد درآن پهن دشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به یاری بیامد سمن بر چو شیر</p></div>
<div class="m2"><p>سیه مرد و شه مرد و چندین دلیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زکوهه چو برشد به گردون عمود</p></div>
<div class="m2"><p>برآمد زهر تارکی مغز و دود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سیه مرد را دست و پهلو شکست</p></div>
<div class="m2"><p>سمن بر بیفتاد برخاک پست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه پشت دادند و بگریختند</p></div>
<div class="m2"><p>بدان اژدها کس نیاویختند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وز آن پس بشد بیژن دیو بند</p></div>
<div class="m2"><p>فتاد اندر آن لشکر شاه هند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو پیلی که او مست گردد یله</p></div>
<div class="m2"><p>خروشد به کین و فتد در گله</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ابا او دلیران ایران هزار</p></div>
<div class="m2"><p>همه گردمرد ودلیر و سوار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سرهند باری زخنجر به بار</p></div>
<div class="m2"><p>فکندند برکید و گشتند خوار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بزد گستهم تیز بر میمنه</p></div>
<div class="m2"><p>شکست و ببردش یکایک بنه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همه دشت کین سربد و یال و گوش</p></div>
<div class="m2"><p>برآمد به گردون به کینه خروش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گریزان بشد لشکر کید شاه</p></div>
<div class="m2"><p>بسا سر کزان رزمگه شد تباه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بشد کید و بگرفت راه گریز</p></div>
<div class="m2"><p>برآورد با لشکران رستخیز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چنین گفتشان کای همه زن، نه مرد</p></div>
<div class="m2"><p>نزیبد شما را سلاح نبرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدانید کاین تیرباران بود</p></div>
<div class="m2"><p>خروش نبرد سواران بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نخستین نبایست رفتن به جنگ</p></div>
<div class="m2"><p>چو رفتید چه باید زمانی درنگ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دواند کسی سوی آن رستخیز</p></div>
<div class="m2"><p>که نوشد سنان و نگیرد گریز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بجای کباب و شرابست و رود</p></div>
<div class="m2"><p>که گیرد می و در فرازد سرود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بگفت این وآوردشان سوی جنگ</p></div>
<div class="m2"><p>برآمد غو پیل و شیر و پلنگ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دگر باره از نو برآمد نبرد</p></div>
<div class="m2"><p>دورویه برافراشته پیل ومرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یکایک زخون شد زمین لاله زار</p></div>
<div class="m2"><p>برآمد زهر خسته ای ناله زار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو رخشنده شد تیغ گردان کین</p></div>
<div class="m2"><p>زجا اندرآمد تو گفتی زمین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نگون شد به هر دشت و وادی درفش</p></div>
<div class="m2"><p>به خون غرقه شاهان زرینه کفش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فکنده سر ودست و پای دلیر</p></div>
<div class="m2"><p>بشد روز از کینه شب اسیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به گستهم گفت ای یل نامدار</p></div>
<div class="m2"><p>ایا پهلوان دلیرو سوار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تو پیشم نگه دار مردانه شو</p></div>
<div class="m2"><p>چو من تند گشتم تو دیوانه شو</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که من تاج ومهد سرافرازشاه</p></div>
<div class="m2"><p>هم اکنون فرو پیچم از گرد راه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بگفت و بشد از پسش گستهم</p></div>
<div class="m2"><p>بیامد به کردار شیب دژم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در آن موج دریای بی دین رسید</p></div>
<div class="m2"><p>بسا سر کزان شیر شد ناپدید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>رسید اندر آن پیل و چترو علم</p></div>
<div class="m2"><p>به یک ضرب تیغش علم شد قلم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بیفکند چتر ودرآمد به شاه</p></div>
<div class="m2"><p>جهان شد به چشم دلیران سیاه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>یکی رزم شد گرد آن مهد پیل</p></div>
<div class="m2"><p>که گیتی سراسر شد از گرد نیل</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بدان گرز گرشاسبی حمله برد</p></div>
<div class="m2"><p>بسا سر به میدان از او گشت خورد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کشیده دوان گستهم تیغ تیز</p></div>
<div class="m2"><p>نموده بر ایشان یکی رستخیز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>که ناگه به سینه درآمد ستور</p></div>
<div class="m2"><p>جداشد گرانمایه از پشت بور</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>فلارنگ و شه مرد و چندین سوار</p></div>
<div class="m2"><p>دویدند بالین آن نامدار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پیاده درآمد دوان گستهم</p></div>
<div class="m2"><p>به دست اندران تیغ و گشته دژم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به هرسو پیاده نبرد آورید</p></div>
<div class="m2"><p>فرامرز یل چون بدو بنگرید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به شمشیر تیز اندرآورد چنگ</p></div>
<div class="m2"><p>بدان سان که خیزد زدریا نهنگ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>رسید اندر آن نامداران دلیر</p></div>
<div class="m2"><p>دو تن چارکرده به یک زخم شیر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>دلیران رمیدند زان نامور</p></div>
<div class="m2"><p>برون برد گستهم پرخاشخر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سوار شکوفت بدل برفشاند</p></div>
<div class="m2"><p>بشد کید وخون بر دوزخ برنشاند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>فرامرز زین سو و زان سو دوید</p></div>
<div class="m2"><p>وز انبوه لشکر مراو را ندید</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>در این بد که گرگین درآمد چو باد</p></div>
<div class="m2"><p>بدو گفت کای شیر فرخ نژاد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>کزان سو فلارنگ با سی هزار</p></div>
<div class="m2"><p>سوی قلبگه شد چو باد بهار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>برآمد ز ایران سپه دارو گیر</p></div>
<div class="m2"><p>زمین وهوا شد چو دریای قیر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ندانم چه باشد سرانجام کار</p></div>
<div class="m2"><p>مبادا کز ایشان زراسب سوار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بپیچد به غارت رود مهد وپیل</p></div>
<div class="m2"><p>نبینی زمین شد به کردار نیل</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو بشنید زو پهلوان سپاه</p></div>
<div class="m2"><p>بتازید و آمد سوی قلبگاه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>زراسب گرانمایه آشفته دید</p></div>
<div class="m2"><p>که هرگه بتازید و می بردمید</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>زکشته در آن قلبگاه بزرگ</p></div>
<div class="m2"><p>به هم برفتاده سرو دست گرگ</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>پراکنده هر سو سر هندوان</p></div>
<div class="m2"><p>زخون جوی گشته به هر سو روان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به میدان همی ابرش کین فکند</p></div>
<div class="m2"><p>بسا سرکه در خاک مشکین فکند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به یک لحظه آن لشکر بی شمار</p></div>
<div class="m2"><p>زتیغ فرامرز شد تارومار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چه افکنده چه کشته شد چه اسیر</p></div>
<div class="m2"><p>بسا نوجوان گشته زان درد، پیر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>دوان هر سویی پهلوان شیرمرد</p></div>
<div class="m2"><p>فلارنگ جویان زدشت نبرد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چو زان بستگان باز دانست کار</p></div>
<div class="m2"><p>نشان جست و آمد سوی کارزار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>یکی دید مانند سرو چمن</p></div>
<div class="m2"><p>دلیران به گردش بسی انجمن</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چوآتش فرامرز با صد سوار</p></div>
<div class="m2"><p>بدو زد برآمد یکی کارزار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>شد از خون به هرگوشه ای آبگیر</p></div>
<div class="m2"><p>زآهنگ پیل دمان نره شیر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>درآمد،نهادند رخ در گریز</p></div>
<div class="m2"><p>پس آن سواران فکندند نیز</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>فلارنگ و گرزم به هم باز خورد</p></div>
<div class="m2"><p>زد و خون برآمد به دشت نبرد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>نخستین به شمشیر کین جنگ بود</p></div>
<div class="m2"><p>پس از وی به کوپال،آهنگ بود</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>وزآن پس شگفتی ز دست ستور</p></div>
<div class="m2"><p>کشیدند و بستند و کردند زور</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بپیچید گرزم میانش به کین</p></div>
<div class="m2"><p>گرفت و ربودش بزد برزمین</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>زابلق گرانمایه گرگین بجست</p></div>
<div class="m2"><p>دو دستش به نیرو بپیچید و بست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به بالین او هندوان تاختند</p></div>
<div class="m2"><p>به کینه همه گردن افراختند</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>فرامرز آمد چو سرو بلند</p></div>
<div class="m2"><p>همه لشکر هند برهم فکند</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>از آن پس هزیمت گرفتند پیش</p></div>
<div class="m2"><p>گرفتند هریک ره شهر خویش</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>فرامرز و گردان به دنبالشان</p></div>
<div class="m2"><p>همه ره بکردند بیچارشان</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>وز آن پس به لشکرگه خویش باز</p></div>
<div class="m2"><p>فرود آمدند آن یلان سرفراز</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بدین می گذارم بس روزگار</p></div>
<div class="m2"><p>چنین داد تعلیم آموزگار</p></div></div>