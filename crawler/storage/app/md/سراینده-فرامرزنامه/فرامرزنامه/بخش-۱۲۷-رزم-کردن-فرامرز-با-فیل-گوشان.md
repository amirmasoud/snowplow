---
title: >-
    بخش ۱۲۷ - رزم کردن فرامرز با فیل گوشان
---
# بخش ۱۲۷ - رزم کردن فرامرز با فیل گوشان

<div class="b" id="bn1"><div class="m1"><p>برآمد زدریا گو نره شیر</p></div>
<div class="m2"><p>پی او گوان ویلان دلیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سراسر در و دشت و دریا کنار</p></div>
<div class="m2"><p>سراپرده و خیمه زد نامدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شه فیل گوشان چوآگاه شد</p></div>
<div class="m2"><p>که هامون پر از اسب و خرگاه شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپه برنشاند وبزد بوق و کوس</p></div>
<div class="m2"><p>زمانه شد از گرد چون آبنوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپاهی کز آسیب ایشان زمین</p></div>
<div class="m2"><p>بلرزید مانند دریای چین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جزیره بشد جنب جنبان زتاب</p></div>
<div class="m2"><p>تو گفتی همی غرقه گردد زآب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یلانشان به کردار دیو سفید</p></div>
<div class="m2"><p>دمان همچو بر چرخ گردنده شید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به آورد ایران سپاه آمدند</p></div>
<div class="m2"><p>بسیچیده و رزمخواه آمدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپهبد چو گرد سپه دید گفت</p></div>
<div class="m2"><p>که امروز مردی نشاید نهفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا گر جهاندار یزدان پاک</p></div>
<div class="m2"><p>بدین مرز و این بوم سازد هلاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمیرم همانا به جای دگر</p></div>
<div class="m2"><p>نگردد مرا زندگی بیشتر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیامد به دلش اندرون ترس و بیم</p></div>
<div class="m2"><p>بفرمود کردن سپه را دو نیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی نیمه در راه دریا بداشت</p></div>
<div class="m2"><p>دگر نیمه بر روی دشمن گماشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدان تا که از دشمن بد گمان</p></div>
<div class="m2"><p>سپاهی نیاید ز راه نهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خود اندر برابر صفی برکشید</p></div>
<div class="m2"><p>بدان سان که از مردی او سزید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به گردان لشکر چنین گفت شیر</p></div>
<div class="m2"><p>که ای نامداران گرد و دلیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپه چون به تنگ اندرآید نخست</p></div>
<div class="m2"><p>بباید به تیر وکمان دست جست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو از تیر تان ترکش آید تهی</p></div>
<div class="m2"><p>سوی نیزه آرید دست مهی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به نیزه چو از جایشان برکنید</p></div>
<div class="m2"><p>سوی خنجر و گرز دست افکنید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دگر روز،شمشیر زهر آبدار</p></div>
<div class="m2"><p>برآرید ازین فیل گوشان دمار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگفت این و برخاست مانند گرد</p></div>
<div class="m2"><p>سوی دشمن خیره آهنگ کرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به دست اندرون آهنین نیزه ای</p></div>
<div class="m2"><p>به پای ایستاده ابا فرهی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به تیزی برآمد به جای نشست</p></div>
<div class="m2"><p>به کردار باد دمنده بجست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بن نیزه زد از هوا بر زمین</p></div>
<div class="m2"><p>زبالا چو باد اندر آمد به زین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ابا جوشن و خود و ببر بیان</p></div>
<div class="m2"><p>همان ترکش و خنجرش بر میان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گوان وبزرگان ایران زمین</p></div>
<div class="m2"><p>گرفتند یکسر بروآفرین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نشستند بر باد پایان همه</p></div>
<div class="m2"><p>چو بنشست براسب،شیر ورمه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همه تیر بر دست و بر زه،کمان</p></div>
<div class="m2"><p>دل آکنده از کینه بدگمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چوتنگ آمد از فیل گوشان سپاه</p></div>
<div class="m2"><p>همی گرد پرخاش بر شد به ماه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فرامرز گردافکن شیر گیر</p></div>
<div class="m2"><p>کمان بر زه و ترکشش پر زتیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به چاچی کمان اندر آورد چنگ</p></div>
<div class="m2"><p>زترکش برآهیخت تیره خدنگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نهاد از بر چرخ و اندر کشید</p></div>
<div class="m2"><p>زتیرش همی آتش آمد پدید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو سوفار در زه آمیختی</p></div>
<div class="m2"><p>به چرخ اندرافکندش آهیختی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو پیکان درون قبضه آورد سنگ</p></div>
<div class="m2"><p>ببرد از رخ ماه و خورشید رنگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گره شست آویخت زه باز کرد</p></div>
<div class="m2"><p>خدنگ از بر چرخ پرواز کرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بزد بر کمربند گرد دلیر</p></div>
<div class="m2"><p>گذر کرد از او تیز پیکان تیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>برآمد به پهلوی گردی دگر</p></div>
<div class="m2"><p>وز آن پهلوی دیگر آمد به سر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همه نامداران چو ابر بهار</p></div>
<div class="m2"><p>ببارید تیر اندر آن کارزار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به هر تیر کز شستشان جسته شد</p></div>
<div class="m2"><p>تن نامداران از آن خسته شد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چه خسته چه کشته شدی در زمان</p></div>
<div class="m2"><p>روانش برون رفتی اندر زمان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زدشمن به تیر اندر آن کارزار</p></div>
<div class="m2"><p>فکندند از آن بیشه پنجه هزار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>فکندند وکشتند در دشت کین</p></div>
<div class="m2"><p>که دریای خون گشت روی زمین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چواز تیر بر دشمن آمد شکست</p></div>
<div class="m2"><p>سوی نیزه بردند آنگاه دست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سپاه اندر آمد به نیزه چو کوه</p></div>
<div class="m2"><p>از ایشان بشد فیل گوشان ستوه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زنیزه شد آوردگه نیستان</p></div>
<div class="m2"><p>همان روی کشور زخون،می ستان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سپهدار بگرفت نیزه به دست</p></div>
<div class="m2"><p>به جنگ اندرآمد چو یک پیل مست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هرآنگه که او نیزه بگذاردی</p></div>
<div class="m2"><p>سپه را چنان تنگ بفشاردی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چونیزه بر سینه یک زدی</p></div>
<div class="m2"><p>زپشت دگر کس همی سر زدی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>همه باز از ایشان یکان و دوگان</p></div>
<div class="m2"><p>به زین در ربودی به نوک سنان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو مرغان فکندند بر باب زن</p></div>
<div class="m2"><p>برافراختی اندرآن انجمن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یکی فیل گوشی چو باد دمان</p></div>
<div class="m2"><p>بیامد به تندی بر پهلوان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سنان برتن پهلوان کرد راست</p></div>
<div class="m2"><p>خروشان تو گفتی یکی اژدهاست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به دل گفت شیرژیان پهلوان</p></div>
<div class="m2"><p>که آمد روانم همی را زمان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چوتنگ اندر آمد بر شیرمرد</p></div>
<div class="m2"><p>یکی حمله آورد با دار و برد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بزد نیزه ای بر بر روشنش</p></div>
<div class="m2"><p>بدرید در بر همه جوشنش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زره بود زیرش سنان بربتافت</p></div>
<div class="m2"><p>تن روشنش زان گزندی نیافت</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سپهبد برآورد یک تیغ سخت</p></div>
<div class="m2"><p>بزد نیزه اش را که شد لخت لخت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>برآورد گرز گران نره دیو</p></div>
<div class="m2"><p>درافکند بر چرخ گردان غریو</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>برآورد و بنمود حمله به شیر</p></div>
<div class="m2"><p>سپر برسرآورد گرد دلیر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سپر بر سر پهلوان گشت خرد</p></div>
<div class="m2"><p>سپهبد به شمشیر کین دست برد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کشید از میانش یکی تیغ تیز</p></div>
<div class="m2"><p>برآورد از جان او رستخیز</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چنان زد که یک نیمه از اسب ودیو</p></div>
<div class="m2"><p>بماند و دگر نیمه بد در غریو</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو او کشته شد جوشن زابلی</p></div>
<div class="m2"><p>بپوشید با خنجر کابلی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بیامد گرازان به دشت نبرد</p></div>
<div class="m2"><p>دگر باره آهنگ آورد کرد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>در آن رزمگه داد مردی بداد</p></div>
<div class="m2"><p>چوآتش در آن فیل گوشان فتاد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>یکی را کمر بند بگرفت خوار</p></div>
<div class="m2"><p>برآورد اندر صف کارزار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چنان بر زمین زد که اندام وی</p></div>
<div class="m2"><p>فرو ریخت مهره چو بگسست پی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>یکی را زپس یال بگرفت و گوش</p></div>
<div class="m2"><p>برآورد و زد بر زمین با خروش</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>یکی رابه بال ویکی را به مشت</p></div>
<div class="m2"><p>یکی را به گرز و به زخم درشت</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بکشت و در آن رزمگه توده کرد</p></div>
<div class="m2"><p>به مغز وبه خون،خاک آلوده کرد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>زکردار او خیره مانده سپاه</p></div>
<div class="m2"><p>همه آفرین خوان بدان رزمخواه</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>نگه کرد تا خسرو آن سپاه</p></div>
<div class="m2"><p>کجا برفرازد درفش وکلاه</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو دیدش به جنگ اندر آمد به تنگ</p></div>
<div class="m2"><p>که بد نیزه جان ستانش به چنگ</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بزد تند برکوهه زینش به بر</p></div>
<div class="m2"><p>کزآن کوشه دیگر آمد به در</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>دو زانوش برکوهه زین بدوخت</p></div>
<div class="m2"><p>روانش به نوک سنان برفروخت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو کشته شد آن شهریار رمه</p></div>
<div class="m2"><p>سپه هر چه ماندند با دمدمه</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>گریزان به بیشه نهادند روی</p></div>
<div class="m2"><p>روان تیره و با غم و گفتگوی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>زشمشیر آن شیرمردان کین</p></div>
<div class="m2"><p>تهی گشت از آن فیل گوشان زمین</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به تاراج بستند از آن پس کمر</p></div>
<div class="m2"><p>چه گردان و چه مهتر نامور</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>فراوان بت خوب رخ یافتند</p></div>
<div class="m2"><p>به جستن چو رفتند و بشتافتند</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بسی گوهر و زر وتاج وکمر</p></div>
<div class="m2"><p>همان پیل واسبان با زین وبر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>زهرگونه آلات گستردنی</p></div>
<div class="m2"><p>ببردند چندان که بد بردنی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>از آن پس به دریا نهادند روی</p></div>
<div class="m2"><p>همه با دل شادمان بزم جوی</p></div></div>