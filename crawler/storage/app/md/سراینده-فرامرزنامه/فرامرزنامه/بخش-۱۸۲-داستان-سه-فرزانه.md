---
title: >-
    بخش ۱۸۲ - داستان سه فرزانه
---
# بخش ۱۸۲ - داستان سه فرزانه

<div class="b" id="bn1"><div class="m1"><p>یکی داستانی کنون در خور است</p></div>
<div class="m2"><p>که دانش فراوان بدو اندر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سه فرزانه بودند جایی به هم</p></div>
<div class="m2"><p>نشسته زگردون گردان دژم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی گفت کز راه باریک من</p></div>
<div class="m2"><p>بتر نیست از درد،نزدیک من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه دردمندی شود تیره خوی</p></div>
<div class="m2"><p>بود بی گمانیش از مرگ روی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دگر گفت ما این نخوانیم بد</p></div>
<div class="m2"><p>به جایی که نیکو به مردم رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بد آن دان که مردم بود گرسنه</p></div>
<div class="m2"><p>سر خویش نشناسد از پاشنه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سه دیگر بدان هردو آورد روی</p></div>
<div class="m2"><p>چنین گفت کای مرد،یاوه نگوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندانم از آن بیم با هول تر</p></div>
<div class="m2"><p>که آن آورد زندگانی به سر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بجوشد همی زهره از ترس وبیم</p></div>
<div class="m2"><p>دل ار چه دلیرست گردد دو نیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکیبا بدان هردو بودن توان</p></div>
<div class="m2"><p>بدان هردو دارو خریدن توان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به سیم آیدت نان و دارو پدید</p></div>
<div class="m2"><p>به سیم این دوگیتی توانی خرید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیا تا یکی آزمایش کنیم</p></div>
<div class="m2"><p>کرا راست گوید ستایش کنیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو ناسازگار آمد این چند رای</p></div>
<div class="m2"><p>درست آیدت آزمایش به جای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیاورد هریک یکی گوسفند</p></div>
<div class="m2"><p>نمودند بیچارگان را گزند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از ایشان یکی را شکستند پای</p></div>
<div class="m2"><p>فکندند اورابه خانه به جای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نهادند سبزی به پیش اندرش</p></div>
<div class="m2"><p>همان آب روشن که بودی خورش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به زندان یکی را دگر باز داشت</p></div>
<div class="m2"><p>ببردند نزدیک او شام و چاشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به خانه درون کرد میش بزرگ</p></div>
<div class="m2"><p>ببست از برابرش گرگی سترگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سیم را ببستند بی آب ونان</p></div>
<div class="m2"><p>ببستند در را به بیچارگان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنان بود پیمن آن هرسه کس</p></div>
<div class="m2"><p>که یک هفته آنجا نکردند کس</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پدید آید آن هفت روز تمام</p></div>
<div class="m2"><p>که زنده کدامست، مرده کدام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به هشتم سه فرزانه رفتند تیز</p></div>
<div class="m2"><p>زبان پر ز گفتار و دل پر ستیز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سوی خانه دردمندان شدند</p></div>
<div class="m2"><p>بدان خانه مستمندان شدند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدیدند خفته شکسته دو پای</p></div>
<div class="m2"><p>گیا خورده وآب،زنده به جای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دوم را به زندان شدند آن سه تن</p></div>
<div class="m2"><p>به لب ناچران زنده ماند به تن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سه دیگر ز بیم گزاینده گرگ</p></div>
<div class="m2"><p>بمرده چنان گوسفند بزرگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یقین شد که ترس از همه برترست</p></div>
<div class="m2"><p>به هردو جهان،ایمنی خوشتر است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کشیده ستم دیده زال این سه چیز</p></div>
<div class="m2"><p>به دل،درد،نان خوردنی،بیم نیز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سرانجام پیری چو نیکو بود</p></div>
<div class="m2"><p>همه زندگانی بی آهو بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نشسته کشاورز خورشیدپیش</p></div>
<div class="m2"><p>زمانه زده بر دل هردو نیش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کشاورز را گفت بیچاره وار</p></div>
<div class="m2"><p>که لختی سیاهی وکاغذ بیار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برفت و بیاورد دستان زدرد</p></div>
<div class="m2"><p>به دستور بهمن یکی نامه کرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کزین زندگانی که هستم دروی</p></div>
<div class="m2"><p>سزد گر نتابم من از شاه،روی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سرآید به من این هم از روزگار</p></div>
<div class="m2"><p>به گیتی نماند کسی پایدار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>من وشاه و تو هرسه تن بگذریم</p></div>
<div class="m2"><p>زکاری که کردیم کیفر بریم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سرانجام ما بازگشتن به خاک</p></div>
<div class="m2"><p>زمردی چه بیم و زکشتن چه باک</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدان گیتی افکندم اکنون سخن</p></div>
<div class="m2"><p>بگو شاه را هرچه خواهی مکن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که من سیرم از زندگانی کنون</p></div>
<div class="m2"><p>ببخشای خواه و بریزی تو خون</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بیامد کشاورز و نامه بداد</p></div>
<div class="m2"><p>چو جاماسب آن نامه را برگشاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سر راستان اندر آمد به اسب</p></div>
<div class="m2"><p>برون رفت مانند آذر گشسب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به تند استری را نهادند زین</p></div>
<div class="m2"><p>زبهر سرافراز زال گزین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دو تا گشته و سرفکنده به پیش</p></div>
<div class="m2"><p>تن ازدرد،نالان،دل از درد،ریش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زباد خزان،گل فرو ریخته</p></div>
<div class="m2"><p>زخون،زعفران،ژاله بربیخته</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو شاخی که بی بار باشد خزان</p></div>
<div class="m2"><p>چو باغی کزو بگسلد ارغوان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زبس سال ومه رفته گردان سرش</p></div>
<div class="m2"><p>چو سرو سمن خم شده پیکرش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدو گفت برخیز کین رای نیست</p></div>
<div class="m2"><p>که با رنج گردون تو را پای نیست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مر او را ابا خویشتن رادمرد</p></div>
<div class="m2"><p>نهانی زخویش و زبیگانه برد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نهادند خوان پیش آن هردوان</p></div>
<div class="m2"><p>بخوردند و کردند تازه روان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>همان گاه برخاست جاماسب زود</p></div>
<div class="m2"><p>بیامد به نزدیک بهمن چودود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بدو گفت ای نامور شهریار</p></div>
<div class="m2"><p>تو را کرد یزدان،چنین کامکار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بیا تا بتازیم ایدر به بلخ</p></div>
<div class="m2"><p>به خود روز شادی نسازیم تلخ</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چنین گفت بهمن که پنجاه سال</p></div>
<div class="m2"><p>نشینم درین مرز از بهر زال</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خود از بهر زال است برخاک،جنگ</p></div>
<div class="m2"><p>نخواهم شدن تا نیاید به چنگ</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بدو گفت ایدر درنگ آوری</p></div>
<div class="m2"><p>چه خواهیش کرد ار به چنگ آوری</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بکوبم سرش گفت در زیرسنگ</p></div>
<div class="m2"><p>خورم زاستخوانش با می لعل رنگ</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کنم سیستان را یکی ساده دشت</p></div>
<div class="m2"><p>سراسر به ارزن بخواهیم کشت</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>فرامرز را زیر پای آورم</p></div>
<div class="m2"><p>هرآنچه که گفتم به جای آورم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>وز آن پس سوی دخمه لشکر کشم</p></div>
<div class="m2"><p>از این مرز کنده به خاور کشم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بدان تا جهانی بدانند کار</p></div>
<div class="m2"><p>که یاوه نشد خون اسفندیار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بدو گفت داننده،فرمان توراست</p></div>
<div class="m2"><p>ولیکن سگالش چنین نارواست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چوپیروز گشتی بزرگی نمای</p></div>
<div class="m2"><p>همان به که بخشایش آری به جای</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>سر راستی،داد وبخشایش است</p></div>
<div class="m2"><p>از این هر دوگیتی برآسایش است</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>اگر رایت این است گفتار،این</p></div>
<div class="m2"><p>نیابی تو مر زال را در زمین</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گر زال درد هر، بیچاره گشت</p></div>
<div class="m2"><p>فرامرز در گیتی آواره گشت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بکندی همه کاخ وایوانشان</p></div>
<div class="m2"><p>به تاراج دادی شبستانشان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سرایی که گرشاسب کردی نماز</p></div>
<div class="m2"><p>پراز نامداران گردنفراز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چنان گشت گویی که هرگز نبود</p></div>
<div class="m2"><p>نه نامت ازین شهریارا نبود</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>زگفتار آن شاه گیتی گشا</p></div>
<div class="m2"><p>چو خیره فروماند گفتی به جای</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بدو گفت با سخن گفتنت</p></div>
<div class="m2"><p>چه چیز است برخیره آشفتنت</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>نه من دشمنم گر تورا هست دوست</p></div>
<div class="m2"><p>زداننده داد درستی نکوست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بگو مر مرا تا هوای تو چیست</p></div>
<div class="m2"><p>زگفتار بیهوده رای تو چیست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بدوگفت داننده کای شهریار</p></div>
<div class="m2"><p>تو را بازگویم که چون است کار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>اگر زال خواهی که آید به دست</p></div>
<div class="m2"><p>یکی سخت پیمانت باید ببست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>زسوگند،چون شاه چاره ندید</p></div>
<div class="m2"><p>زگفتار دانا کرانه ندید</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بیاورد داننده،وستا و زند</p></div>
<div class="m2"><p>به سوگند مر شاه را کرد بند</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>از آن پس که او را بسی پند داد</p></div>
<div class="m2"><p>مراو رایکی سخت سوگند داد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>به یزدان که امید مردم وراست</p></div>
<div class="m2"><p>به پیغمبر دین ابر راه راست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>که من زال را خون نریزم زتن</p></div>
<div class="m2"><p>نه کس را بفرمایم از انجمن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نه بد خواهمش نه گزندش کنم</p></div>
<div class="m2"><p>نه زندان نمایم نه بندش کنم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>به دل شاد،فرزانه بر پای جست</p></div>
<div class="m2"><p>که دستان زدار و زکشتن برست</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بیامد بر نزدیک دستان سام</p></div>
<div class="m2"><p>به مژده که تنداژدها گشت رام</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>یکی رنجه شو تابه نزدیک شاه</p></div>
<div class="m2"><p>تو را چون ببیند ببخشد گناه</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بدو گفت زال ای سرافراز پیر</p></div>
<div class="m2"><p>به کشتن دهی مر مرا خیره خیر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>که شه بی وفایست واندک خرد</p></div>
<div class="m2"><p>به جز بر بدی سویمان ننگرد</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>خرد راه دیده بدوزد بروی</p></div>
<div class="m2"><p>همان آتش کین برافروزد اوی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بدو گفت اندیشه بد مدار</p></div>
<div class="m2"><p>که شه نشکند با من این زینهار</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به یزدان هرآن کس که سوگند خورد</p></div>
<div class="m2"><p>اگر بشکند کس نخواندش مرد</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>برفتند جاماسب،خورشید و زال</p></div>
<div class="m2"><p>گرفته دو تن زال را سخت یال</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>به خورشید گفت ای نکو رای مرد</p></div>
<div class="m2"><p>تو با من چه باشی برو بازگرد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>یک آرزو کار تو دل ناخوش است</p></div>
<div class="m2"><p>به جان تو چون آتش سرکش است</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>نیاید که دیوش به جایی کشد</p></div>
<div class="m2"><p>که جان تو از وی بلایی کشد</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چنین داد پاسخ که او پادشاست</p></div>
<div class="m2"><p>به گیتی وی امروز فرمان رواست</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چنان دان که چون آفتابست شاه</p></div>
<div class="m2"><p>که دارد کنون زآفتابم نگاه؟</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>دگر کز تو برگشت آیین من</p></div>
<div class="m2"><p>نباشد روا نیست در دین من</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>چو من با تو بودم به هنگام ناز</p></div>
<div class="m2"><p>نگردم به گاه بلا از تو باز</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>زگفتار او هرسه گریان شدند</p></div>
<div class="m2"><p>وزآنجا سوی شاه ایران شدند</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>چو زال اندر آمد به نزدیک شاه</p></div>
<div class="m2"><p>رخان کرده چون کاه،بالا دوتاه</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>به لب بر رخ از خاک بر رمیم زد</p></div>
<div class="m2"><p>زخون،نقطه بر تخته سیم زد</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چنین گفت کای شاه فرخنده روز</p></div>
<div class="m2"><p>به کام تو شد کشور نیمروز</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>چه خواهی از این پیر برگشته بخت</p></div>
<div class="m2"><p>از آن پس که دیدم زتو درد سخت</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>نه من پروراننده بودم تورا</p></div>
<div class="m2"><p>نه من مهربانی نمودم تو را؟</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>کنون از بزرگان روا این بود</p></div>
<div class="m2"><p>چنان نیکویی را جزا این بود؟</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چه باید تو را خواند مرد اسیر</p></div>
<div class="m2"><p>که پیش نیاکان تو گشت سیر</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>به ما پنج روز دگر تا جهان</p></div>
<div class="m2"><p>سرآرد بدین پیرسر ناگهان</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>بزرگی و نیکو دلی پیشه کن</p></div>
<div class="m2"><p>به کار جهان اندر اندیشه کن</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>که تا شهریاری سوی تو رسید</p></div>
<div class="m2"><p>جهان چند از این شهریاران بدید</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>کجا شد کیومرث آن سرکشان</p></div>
<div class="m2"><p>که کس را به گیتی نمانده نشان</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>کجا رفته کیخسرو پاک زاد</p></div>
<div class="m2"><p>که لهراسب را نام شاهی نهاد</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>سرانجام،نزدیک ایشان شوی</p></div>
<div class="m2"><p>که در پیش دادار یزدان شوی</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>بکندی مرا خانه وبوم وبر</p></div>
<div class="m2"><p>بخستی دلم را به مرگ پسر</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>بکشتی همه نامداران من</p></div>
<div class="m2"><p>دلیران این مرز ویاران من</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>فرامرز،آواره و دختران</p></div>
<div class="m2"><p>زگیتی بسی مرده نام آوران</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>نبوده به جایی شما را گزند</p></div>
<div class="m2"><p>زما یافته تاج و تخت بلند</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>گراین داد بینی،زهی دادگر</p></div>
<div class="m2"><p>همین برد و برگیرد این ره پسر</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>زگفتار او شاه شد همچو قیر</p></div>
<div class="m2"><p>زختمش بترسید جاماسب پیر</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>به دل گفت داننده اکنون بود</p></div>
<div class="m2"><p>که پیرامن زال،پرخون شود</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>همان گاه بهمن برآورد سر</p></div>
<div class="m2"><p>به دژخیم فرمود کو را ببر</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>که نتوانم او را به دو دیده دید</p></div>
<div class="m2"><p>سرش بی گمانی بباید برید</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>هنوزش زبان بین که چون خنجر است</p></div>
<div class="m2"><p>یکی خنجر تیزش اندر خور است</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>کشانش ببردند وکردند بند</p></div>
<div class="m2"><p>وزآن پس بفرمود شاه بلند</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>کز آهن یکی تنگ گونه قفس</p></div>
<div class="m2"><p>که زندان ندید آن چنان هیچ کس</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>در آن بندکردند مر زال را</p></div>
<div class="m2"><p>چو مرغی مر آن هشتصد سال را</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>جهان را چو دیدی،سرانجام بین</p></div>
<div class="m2"><p>چنین رنگ بین و چنان دام بین</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>مباش ایمن از گردش روزگار</p></div>
<div class="m2"><p>که ناپایدار است و ناسازگار</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>سرانجام کارت به جایی رسید</p></div>
<div class="m2"><p>کت اندر مقامی بباید خزید</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>به دستان فرستاد پیغام،شاه</p></div>
<div class="m2"><p>که این است تا زنده ای جایگاه</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>یکی ژنده پیلی تو را باد وبس</p></div>
<div class="m2"><p>که بر پشت او باشی اندر قفس</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>بسی گنج از ایوان او برگرفت</p></div>
<div class="m2"><p>بسی افسر و تخت و گوهر گرفت</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>از آن پس به ویرانی آورد رای</p></div>
<div class="m2"><p>درآورد کاخ بلندش زپای</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>یکی آتش سهمگین برفروخت</p></div>
<div class="m2"><p>همه سیستان را سراسر بسوخت</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>وزآن پس برافکند تخم برست</p></div>
<div class="m2"><p>همیشه چنین بودگفتن درخت</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>چنان شد که هرکس که آنجا گذشت</p></div>
<div class="m2"><p>همه ساله گفتی که بوده است شت</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>چو از شهر دستان بپرداخت شاه</p></div>
<div class="m2"><p>سوی کابل آورد او با سپاه</p></div></div>