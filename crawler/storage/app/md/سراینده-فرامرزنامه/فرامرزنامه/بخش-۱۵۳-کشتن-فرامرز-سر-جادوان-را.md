---
title: >-
    بخش ۱۵۳ - کشتن فرامرز، سر جادوان را
---
# بخش ۱۵۳ - کشتن فرامرز، سر جادوان را

<div class="b" id="bn1"><div class="m1"><p>زجا اندر آمد چو کوه گران</p></div>
<div class="m2"><p>یکی سنگ انداخت بر پهلوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپر در سر آورد آن چیره دست</p></div>
<div class="m2"><p>نیامد از آن سنگ بر وی شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان جو سوی خنجر آورد دست</p></div>
<div class="m2"><p>بدو تاخت مانند آذرگشسب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزد بر کمر گاه دیو سیاه</p></div>
<div class="m2"><p>به دو نیمه کردش در آن جایگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خروشی درآمد در آن تیره غار</p></div>
<div class="m2"><p>تو گفتی بدرید آن کوهسار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرآن دیو و جادو که بر دژ بدند</p></div>
<div class="m2"><p>از آن قصه دیو، آگه شدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوی خانه شه نهادند روی</p></div>
<div class="m2"><p>پرازخشم وکینه همه جنگجوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برون آمد از غار شیر ژیان</p></div>
<div class="m2"><p>برآویخت با لشکر جاودان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به تنها تن خویشتن بی سپاه</p></div>
<div class="m2"><p>همی رزم جست آن گو کینه خواه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به شمشیر وگرزوبه سنگ و به مشت</p></div>
<div class="m2"><p>زجادو از دیو چندان بکشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که از خون آن بد گهر جاودان</p></div>
<div class="m2"><p>درآن کوه،سیلاب خون شد روان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین تا که شد روز،آن شیرمرد</p></div>
<div class="m2"><p>زجادو و دیوان برآورد گرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو بر تیغ فیروزه گون رفت مهر</p></div>
<div class="m2"><p>بتابید رخشان زچارم سپهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گوان و فرامرز برخاستند</p></div>
<div class="m2"><p>به آیین،سپه را بیاراستند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سوی بارگاه جهان پهلوان</p></div>
<div class="m2"><p>برفتند شادان و روشن روان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سپهبد ندیدند در بارگاه</p></div>
<div class="m2"><p>برنامداران،جهان شد سیاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به جستن گرفتند چون بی هشان</p></div>
<div class="m2"><p>به هر گوشه پویان و جویان نشان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پراکنده سوی حصار آمدند</p></div>
<div class="m2"><p>جهان پهلوان خواستار آمدند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خروشی شنیدند از آن کوهسار</p></div>
<div class="m2"><p>غریوان مردان درون حصار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خروش فرامرز هم زان نشان</p></div>
<div class="m2"><p>شنیدند گردان گردنکشان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سپه بازدانست آواز او</p></div>
<div class="m2"><p>همانگه شدند آگه از راز او</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شگفتی همی گفت هرکس که شیر</p></div>
<div class="m2"><p>بدین سان دلاور نباشد دلیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که تنها زمردی به دژ درشدست</p></div>
<div class="m2"><p>یلی از دژ جادوان برشدست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از آن پس برفتند نزدیک دژ</p></div>
<div class="m2"><p>پر ازخون و پر کشته بد راه دژ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به درگاه دژ آتش اندر زدند</p></div>
<div class="m2"><p>به جنگ اندرون تیر وخنجر زدند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به دژ در شد آن لشکر نامدار</p></div>
<div class="m2"><p>بدیدند پهلو در آن کارزار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برو هرکسی از جهان آفرین</p></div>
<div class="m2"><p>بخواندند بر پهلوان زمین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از آن پس کشیدند تیغ و تبر</p></div>
<div class="m2"><p>یلان سرافراز پرخاشخر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بکشتند چندان در آن کوهسار</p></div>
<div class="m2"><p>که شد ژرف دریای خون آشکار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زبس خون درآن کوه ریزان برفت</p></div>
<div class="m2"><p>خور از چرخ گردان گریزان برفت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدین گونه تا خور که فیروز بخت</p></div>
<div class="m2"><p>سوی باختر برد بنگاه رخت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از آن نره دیوان و جادو سران</p></div>
<div class="m2"><p>نماندند یک تن ز نام آوران</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همه کشته و خسته و دلفکار</p></div>
<div class="m2"><p>تو گویی نباشد کسی آشکار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نه برنا بماندند ونه مرد پیر</p></div>
<div class="m2"><p>زن وبچه ها نیز کردند اسیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به تاراج دادند دژیکسره</p></div>
<div class="m2"><p>بجستند هامون وکوه و دره</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بسی گوهر وسیم و دیبا و گنج</p></div>
<div class="m2"><p>کجا گرد کردند دیوان به رنج</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به دست آمد ایرانیان را زکوه</p></div>
<div class="m2"><p>شدند از کشیدن یکایک ستوه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ازآن پس چو پرداخت آن پهلوان</p></div>
<div class="m2"><p>زدیو بداندیش و از جادوان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سوی مرز چین اندر آورد روی</p></div>
<div class="m2"><p>همی رفت خرم دل و راه جوی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو شش مه برفتند پویان به راه</p></div>
<div class="m2"><p>زبسیار رفتن،دژم شد سپاه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یکی دشت پیش آمدش چون بهشت</p></div>
<div class="m2"><p>پر از گلشن وباغ پاکیزه کشت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در آن خرم آباد روی زمین</p></div>
<div class="m2"><p>نبد هیچ پیدا کس اندر زمین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همه دشت، آهو و نخجیر بود</p></div>
<div class="m2"><p>جهان پهلوان زان شگفتی نمود</p></div></div>