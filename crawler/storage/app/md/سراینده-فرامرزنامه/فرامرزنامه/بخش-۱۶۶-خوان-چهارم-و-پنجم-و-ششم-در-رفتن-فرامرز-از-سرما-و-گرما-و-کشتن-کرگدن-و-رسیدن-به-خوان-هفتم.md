---
title: >-
    بخش ۱۶۶ - خوان چهارم و پنجم و ششم در رفتن فرامرز از سرما و گرما و کشتن کرگدن و رسیدن به خوان هفتم
---
# بخش ۱۶۶ - خوان چهارم و پنجم و ششم در رفتن فرامرز از سرما و گرما و کشتن کرگدن و رسیدن به خوان هفتم

<div class="b" id="bn1"><div class="m1"><p>بیاورد صد کاروان شتر</p></div>
<div class="m2"><p>زآب وعلف کرده یکباره پر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پیش اندر افکند و پویان برفت</p></div>
<div class="m2"><p>برآن ریگ تاریک،جویان برفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زگرمی همی سوخت تن در سلاح</p></div>
<div class="m2"><p>نبد روز پیکار وگاه مزاح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبان ها برون اوفتاده زکام</p></div>
<div class="m2"><p>همه یاد کردی زقوم و مقام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تن بارگی گشته از خوی پرآب</p></div>
<div class="m2"><p>برآن دشت بی آب ودل پرشتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یزدان بنالید هرکس به درد</p></div>
<div class="m2"><p>از آن راه تاریک پربار و گرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدین گونه ببرید سه روزه راه</p></div>
<div class="m2"><p>میان دو کوه اندر آمد سپاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو آورد لشکر میان دو کوه</p></div>
<div class="m2"><p>خود ونامداران پس اندر گروه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سراپرده زد بر لب جویبار</p></div>
<div class="m2"><p>پس وپشت او لشکر نامدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شب آمد بخفتند و دم بر زدند</p></div>
<div class="m2"><p>یکی بر لب خشک،نم بر زدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز رنج و غم راه دور ودراز</p></div>
<div class="m2"><p>برآسود آن لشکر رزم ساز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو پاسی ازآن تیره شب درگذشت</p></div>
<div class="m2"><p>زابر سیه آسمان تیره گشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برآمد یکی ابر مانند غار</p></div>
<div class="m2"><p>سراسر بپیوست بر کوهسار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازآن ابر تاریک و باد دمان</p></div>
<div class="m2"><p>جهان گشت پردام اهریمنان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ببارید برفی به کردار او</p></div>
<div class="m2"><p>کزآن شد دل نامداران ستوه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برآمد به بالا یکی تیره برف</p></div>
<div class="m2"><p>پراز برف شد کوهسار شگرف</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سرا پرده و خیمه ها پر ز یخ</p></div>
<div class="m2"><p>کشیده شد از برف از دشت نخ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نبد دست و بازو کسی را به کار</p></div>
<div class="m2"><p>پر از برف و سرما شد آن کوهسار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چواز برف و سرما به بیچارگی</p></div>
<div class="m2"><p>رسیدند لشکر به یکبارگی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سپهبد چنین گفت با بخردان</p></div>
<div class="m2"><p>که ای نامداران و فرخ ردان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زبد دست خواهش به یزدان بریم</p></div>
<div class="m2"><p>زخود بینی وکبر، دل برکنیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدین رنج،رخ سوی او آوریم</p></div>
<div class="m2"><p>زچشم، آب حسرت به رو آوریم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مگرمان ببخشد از این سخت جای</p></div>
<div class="m2"><p>که اویست بیچاره را رهنمای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بزرگان و گردان لشکر همه</p></div>
<div class="m2"><p>سپاه آنچه بودند یکسر همه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به زاری همه دست برداشتند</p></div>
<div class="m2"><p>زاندازه فریاد بگذاشتند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که ای برتر از دانش و عقل و جان</p></div>
<div class="m2"><p>تویی آفریننده انس وجان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدین جای بی دسترس،دست گیر</p></div>
<div class="m2"><p>نیاز همه بندگان درپذیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چوکردند از این گونه زاری بسی</p></div>
<div class="m2"><p>زسرما نپرداخت با خود کسی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ببخشود بخشنده داد ومهر</p></div>
<div class="m2"><p>همان گاه شد تازه روی سپهر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همان گه بیامد یکی باد تند</p></div>
<div class="m2"><p>ببرد از رخ آسمان ابر کند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو بخشایش و داد یزدان بود</p></div>
<div class="m2"><p>بهار و دی وتیر،یکسان بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بیامددل مهتران باز جای</p></div>
<div class="m2"><p>نیایش کنان پیش یزدان به پای</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سراپرده و خیمه پربرف ویخ</p></div>
<div class="m2"><p>فکندند برتن برآن کوه،شخ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چوشد خشک،خرگاه و پرده سرای</p></div>
<div class="m2"><p>بزرگان برفتند یکسر زجای</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سبک بار کردند چیزی که بود</p></div>
<div class="m2"><p>وزآنجا برفتند مانند دود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سه روز وسه شب بود در راه برف</p></div>
<div class="m2"><p>برفتند از آن راه برف شگرف</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چهارم چو آمد میان دو کوه</p></div>
<div class="m2"><p>سراسر همه لشکرش بد ستوه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بدیدند یک دشت پرآب وگل</p></div>
<div class="m2"><p>همان جای رامش بد و رود ومل</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گرفتند بر دادگر آفرین</p></div>
<div class="m2"><p>خداوند فیروز جان آفرین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگر چند بسیار دیدند رنج</p></div>
<div class="m2"><p>به هر بهر زان خرمی بود گنج</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نماند به مردم،غم و رنج ودرد</p></div>
<div class="m2"><p>نه خوبی و آسانی و گرم وسرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نه سود و زیان ونه نیک و نه بد</p></div>
<div class="m2"><p>خردمند مردم چرا غم خورد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>برآن دشت پرگل فرود آمدند</p></div>
<div class="m2"><p>ابا رامش و نای و رود آمدند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>یکی بزم خرم بیاراستند</p></div>
<div class="m2"><p>همی جام زرین بپیراستند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو خوردند با شادمانی سه روز</p></div>
<div class="m2"><p>چهارم زگردون چو گیتی فروز</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>برآورد رخشنده،زرین درفش</p></div>
<div class="m2"><p>بدرید شب،پرنیانی بنفش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دلیران به رفتن سر افراختند</p></div>
<div class="m2"><p>دل از رنج و سختی بپرداختند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همی رفت پیش اندرون،پهلوان</p></div>
<div class="m2"><p>سیه دیو،همراه او با ردان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دگر باره آن پهلوان بزرگ</p></div>
<div class="m2"><p>بپرسید از نره دیو سترگ</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>که دیگر شگفتی چه بینم به راه</p></div>
<div class="m2"><p>یکایک بگو ای گو نیک خواه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بدو گفت کز کرگدن دیو زوش</p></div>
<div class="m2"><p>هم اکنون به گوش آیدت یک خروش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کزآن گونه پتیاره دیو ژیان</p></div>
<div class="m2"><p>ندیده است هرگز کس اندر جهان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بدرد زآواز او کوه وسنگ</p></div>
<div class="m2"><p>بخاید ز بیمش ژیان شیر،چنگ</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تن پیل دارد سرکرگدن</p></div>
<div class="m2"><p>سرون بر سرش چون درخت گشن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به تک باد را زیر پی بسپرد</p></div>
<div class="m2"><p>به دندان چو پیل ژیان بشکند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سر و پای پیل ژیان بر زند</p></div>
<div class="m2"><p>چو بادش ز روی زمین برکند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نباشد مر او را یکی پشه سنگ</p></div>
<div class="m2"><p>ازوکوه،پیچان شود روز جنگ</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>فرامرز فرمود تا رزم ساز</p></div>
<div class="m2"><p>بیارند در پیش آن سرفراز</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>زبهر نبرد آنچه بد ناگزیر</p></div>
<div class="m2"><p>زتیغ و زگرز و کمان و زتیر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بپوشید یکسر همان ساز جنگ</p></div>
<div class="m2"><p>برون تاخت مانند شیر و پلنگ</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سپه رفت و خود ماند پیش اندرون</p></div>
<div class="m2"><p>تو گفتی روان شد که بیستون</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو خورشید از باختر کرد روی</p></div>
<div class="m2"><p>به منزل رسید آن گو نامجوی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به دست اندرون خنجر دد فکن</p></div>
<div class="m2"><p>چو دانست مأوای آن کرگدن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>یکی نعره زد پهلوان دلیر</p></div>
<div class="m2"><p>که از نعره او بلرزید شیر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو بشنید آن دد، برآشفت سخت</p></div>
<div class="m2"><p>که از نعره گرد با زیب و بخت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بیامد برش کرگدن دیو زوش</p></div>
<div class="m2"><p>برآورد بر چرخ گردون خروش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دمنده ز ابر اندر آورد سر</p></div>
<div class="m2"><p>شد از هیبتش کوه،زیر و زبر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو از دور دیدش نبرده سوار</p></div>
<div class="m2"><p>بغرید مانند شیر شکار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ببارید بر وی زتیر خدنگ</p></div>
<div class="m2"><p>زپیکان بر وی جهان کرد تنگ</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>دد تیز دندان بیامد چو باد</p></div>
<div class="m2"><p>به پای سمند جوان در فتاد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>سرونی بزد بر زهار سمند</p></div>
<div class="m2"><p>به یک زخم بر تیره خاکش فکند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>سپهبد بجست از بر بادپای</p></div>
<div class="m2"><p>چوپیل دمان اندر آمد زجای</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>پیاده درآویخت با کرگدن</p></div>
<div class="m2"><p>یکی تیغ در چنگ آن پیل تن</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بزد بر میان سرش تیغ تیز</p></div>
<div class="m2"><p>به مردی برآورد از او رستخیز</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به دو نیمه شد پیل وش پیکرش</p></div>
<div class="m2"><p>به خاک اندر افکند یال وبرش</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بیامد خروشان به پیش خدای</p></div>
<div class="m2"><p>خداوند نیروده رهنمای</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>خروشید بسیار و کرد آفرین</p></div>
<div class="m2"><p>بمالید رخسارها بر زمین</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>همان گاه دیو سیه در رسید</p></div>
<div class="m2"><p>مراو را به جای پرستش بدید</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>فتاده به نزدیک او کرگدن</p></div>
<div class="m2"><p>به خنجر به دو نیمه گشتست تن</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بدو آفرین خواند دیو دلیر</p></div>
<div class="m2"><p>ابر بازوی نامور نره شیر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>سپه نیز آمد ز راه دراز</p></div>
<div class="m2"><p>ببردند یک یک براو نماز</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بسی آفرین کرد هر کس بر او</p></div>
<div class="m2"><p>که جاوید بادا یل نامجوی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>همان جا بر سبزه خرگه زدند</p></div>
<div class="m2"><p>سراپرده نزد یکی ره زدند</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به رامش نشستند و می خواستند</p></div>
<div class="m2"><p>دل از خرمی ها برآراستند</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چو شد مست هرکس سوی خوابگاه</p></div>
<div class="m2"><p>برفتند آسوده یکسر سپاه</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>دگر روز چون گشت خورشید،زرد</p></div>
<div class="m2"><p>بگسترد زرآب بر لاجورد</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>برآراست راه،آن یل پهلوان</p></div>
<div class="m2"><p>همه نامداران روشن روان</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>دگر باره با آن سیه دیو گفت</p></div>
<div class="m2"><p>که در کار دانش مکن در نهفت</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چه بینم دگر باره از دیو ودد</p></div>
<div class="m2"><p>در این ره چه پیش آیدم نیک وبد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>دگر گفت با او سیه دیو گرد</p></div>
<div class="m2"><p>که ای مرد با دانش و دستبرد</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>یکی دیگرت کار ماندست و بس</p></div>
<div class="m2"><p>کز آن سهمگین تر ندیدست کس</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چو زین بگذری هیچ رنجت نماند</p></div>
<div class="m2"><p>بجز کشور و تاج وگنجت نماند</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>یکی اژدها است بر رهگذر</p></div>
<div class="m2"><p>کزو چرخ گردنده جوید حذر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>چو کوهی به تن باشد وتف و تاب</p></div>
<div class="m2"><p>گریزد ازو بر سپهر،آفتاب</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>دو چشمش چو دو طاس هم پر زخون</p></div>
<div class="m2"><p>زکام و دمش آتش آید برون</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>نفس همچو سوزنده آذرگشسب</p></div>
<div class="m2"><p>زمیلی به دم در کشد پیل واسب</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>به سر بر دو شاخش بود تیر سخت</p></div>
<div class="m2"><p>ستبری فزون تر زشاخ درخت</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>همی دست و پا دارد ویال وبر</p></div>
<div class="m2"><p>به چنگال،ماننده شیر نر</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>گرایدون که او را نگون آوری</p></div>
<div class="m2"><p>به مردی تن او به خون آوری</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>چنان دان که داننده خوب وزشت</p></div>
<div class="m2"><p>به نام تو منشور مردی نوشت</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>سیه دیو را گفت شیرژیان</p></div>
<div class="m2"><p>که ای مرد دانای شیرین زبان</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بسی دیده ام زین نشان اژدها</p></div>
<div class="m2"><p>ازین سخت تر گاه کین وبلا</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>که هرگز نپیچیده ام سر زجنگ</p></div>
<div class="m2"><p>نه در رزم جستن نمودم درنگ</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>به زور جهاندار ازین اژدها</p></div>
<div class="m2"><p>برآرم دمار ونیابد رها</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>بگفت این وبرگستوان بر سیاه</p></div>
<div class="m2"><p>برافکند آن پهلو رزمخواه</p></div></div>