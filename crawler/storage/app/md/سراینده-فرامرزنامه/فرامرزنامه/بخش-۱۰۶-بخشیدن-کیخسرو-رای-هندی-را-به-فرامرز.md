---
title: >-
    بخش ۱۰۶ - بخشیدن کیخسرو،رای هندی را به فرامرز
---
# بخش ۱۰۶ - بخشیدن کیخسرو،رای هندی را به فرامرز

<div class="b" id="bn1"><div class="m1"><p>سپیده چو از باختر زد درفش</p></div>
<div class="m2"><p>چوکافور شد روی چرخ بنفش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمین،تازه شد کوه چون سندروس</p></div>
<div class="m2"><p>زدرگاه برخاست آواز کوس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرامرز با رستم پهلوان</p></div>
<div class="m2"><p>برفتند نزدیک شاه جهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرامرز در باره شاه شد</p></div>
<div class="m2"><p>سخن گفتن شاه همراه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که با من نکویی بسی کرد رای</p></div>
<div class="m2"><p>هنر در دل خویشتن کرد جای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدان گه که من اوفتادم برش</p></div>
<div class="m2"><p>یکی نامداری ببد لشکرش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسی نیکویی ها از او دیده ام</p></div>
<div class="m2"><p>به دانش مر او را پسندیده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون چشم دارم زشاه جهان</p></div>
<div class="m2"><p>که بخشد برو ملک هندوستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدان کو مرا دوستداری نمود</p></div>
<div class="m2"><p>نباید بدو رنج و خواری نمود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بشنید شاهنشه دادگر</p></div>
<div class="m2"><p>ورا گفت بخشیدمش سربه سر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ببر همچنانش به هندوستان</p></div>
<div class="m2"><p>به سوی بر و بوم جادوستان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به خوبیش بر تخت شاهی نشان</p></div>
<div class="m2"><p>از ایدر فراوان ببر سرکشان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که آن بوم و بر تا به دریای چین</p></div>
<div class="m2"><p>به شاهی تو را دادم ای پاک دین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به خوبی بساز و میازار کس</p></div>
<div class="m2"><p>نه از کارداران برنجید بس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کشاورز را نیکی آور به جای</p></div>
<div class="m2"><p>زتو نام باید که ماند به جای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فرامرز،روی زمین داد بوس</p></div>
<div class="m2"><p>بدو گفت ای شاه با پیل وکوس</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی بنده ام پیش تختت به پای</p></div>
<div class="m2"><p>چنان چون بفرمایی آرم به جای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شه هندوان را طلب کردشاه</p></div>
<div class="m2"><p>بدو خلعتی داد زیبای گاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نوازید بسیار و اندرزکرد</p></div>
<div class="m2"><p>سپهدار هندی آن مرز کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدو گفت من شاه را بنده ام</p></div>
<div class="m2"><p>به فرمان و رایش سرافکنده ام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نپیچم من از چاکران تو سر</p></div>
<div class="m2"><p>گرم دیده خواهند ای نامور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زمین را ببوسید وآمد به در</p></div>
<div class="m2"><p>ره هند را تنگ بسته کمر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از آن پس که آمد برون شاه هند</p></div>
<div class="m2"><p>سپهبد فرامرز نیکی پسند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ورا بر در خیمه خویشتن</p></div>
<div class="m2"><p>ابا نامداران یکی انجمن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به می خوردن اندر نشستند شاد</p></div>
<div class="m2"><p>یکی شب ببودند تا بامداد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چوخورشید بر چرخ بگذارد پای</p></div>
<div class="m2"><p>خروش جرس خاست با بانگ نای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روان شد فرامرز با رای هند</p></div>
<div class="m2"><p>سوی شهر خود از ره مرز سند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو تنگ اندرآمد سوی هندوان</p></div>
<div class="m2"><p>یکی آگهی آمد از پهلوان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که بر هندوان دیگری خسرو است</p></div>
<div class="m2"><p>شهنشاهی ونامداری گو است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سرافراز مردی مهارک به نام</p></div>
<div class="m2"><p>سپهدار و گردنکش وخویش کام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از آن گه که آن پهلوان سترگ</p></div>
<div class="m2"><p>ازیدر بشد نزد شاه بزرگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بزرگان هندوستان همچنان</p></div>
<div class="m2"><p>گزیدند شاهی دلیر و جوان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نشاندند بر تخت و بر تاج زر</p></div>
<div class="m2"><p>به فرمانش بستند یکسر کمر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همه عهد کردند مردان هند</p></div>
<div class="m2"><p>بزرگان و گردان و شیران سند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که گر تیغ بارد به ما از سپهر</p></div>
<div class="m2"><p>نسازیم با رای از روی مهر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همانا که کیخسرو از راه کین</p></div>
<div class="m2"><p>ورا کرده باشد نهان در زمین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وگر زنده باشد در این بارگاه</p></div>
<div class="m2"><p>نه دیهیم یابد نه تخت و نه گاه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو این گفته بشنید مردی ژیان</p></div>
<div class="m2"><p>شگفتی نمودش بیامد دمان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بر رای هند این سخن بازگفت</p></div>
<div class="m2"><p>چو بشنید از او رای پاسخ بگفت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>که آن بدرگ بدتن بدنژاد</p></div>
<div class="m2"><p>مهارک که بر نخت من کرده یاد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یکی بنده ای بود باب مرا</p></div>
<div class="m2"><p>پرستنده خاک و آب مرا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زفرمان من شاه کشمیر بود</p></div>
<div class="m2"><p>بدان کشور و مرز،او میربود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>من از جان گرامی ترش داشتم</p></div>
<div class="m2"><p>سراو زهرکس برافراشتم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کنون او ز بدخویی و بد تنی</p></div>
<div class="m2"><p>پدیدار کرده است اهریمنی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ره ایزدی هشت و از راه شد</p></div>
<div class="m2"><p>چو بد گوهری کرده گمراه شد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نکو گفت دانای آموزگار</p></div>
<div class="m2"><p>که از بدگهر،چشم نیکی مدار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به نوش ار کسی زهر را پرورد</p></div>
<div class="m2"><p>مه و سال ها رنج و سختی برد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سرشتش دهد از می واز انگبین</p></div>
<div class="m2"><p>به کام اندرش شیر و ماء معین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سرانجام،راز آشکارا کند</p></div>
<div class="m2"><p>همان گونه خویش پیدا کند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>فرامرز گفت ای جهان دیده شاه</p></div>
<div class="m2"><p>توزان بدکنش،دل مگردان ز راه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به توفیق دادار فیروزگر</p></div>
<div class="m2"><p>زتختش نگون اندرآرم به سر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یکی نامه باید نوشتن بدو</p></div>
<div class="m2"><p>به نامه شود گونه این گفتگو</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اگر رام گردد بدین بارگاه</p></div>
<div class="m2"><p>بیاید سپارد تو را جایگاه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>وگرنه به گرز وبه شمشیر تیز</p></div>
<div class="m2"><p>برانگیزم از جان او رستخیز</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بدو رای گفتا که فرمان،توراست</p></div>
<div class="m2"><p>دلم بسته رای وفرمان،توراست</p></div></div>