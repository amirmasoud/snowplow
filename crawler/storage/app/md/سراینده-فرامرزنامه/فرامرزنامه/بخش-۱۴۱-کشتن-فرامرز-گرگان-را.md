---
title: >-
    بخش ۱۴۱ - کشتن فرامرز،گرگان را
---
# بخش ۱۴۱ - کشتن فرامرز،گرگان را

<div class="b" id="bn1"><div class="m1"><p>دگر روز چون خسرو آسمان</p></div>
<div class="m2"><p>برآمد زگردون سپیده دمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز چرخ چهارم چو بنمود چهر</p></div>
<div class="m2"><p>بیاراست گیتی سراسر به مهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپهبد بیامد به تن بر زره</p></div>
<div class="m2"><p>زپولاد بسته زره را گره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآراسته دل به پیکار و جنگ</p></div>
<div class="m2"><p>کمان بر زه و ترکشش پر خدنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زور جهان داور رهنمای</p></div>
<div class="m2"><p>به اسپ سیاه اندر آورد پای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خروشان و جوشان سوی رزم گرگ</p></div>
<div class="m2"><p>دمان زیر او بادپای سترگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چوآمد یکی برخروشید سخت</p></div>
<div class="m2"><p>زبانگش فرو ریخت برگ درخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین تا به نزدیک گرگان رسید</p></div>
<div class="m2"><p>یکی مرغزاری خوشاینده دید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو گرگان شنیدند آواز اوی</p></div>
<div class="m2"><p>زبیشه سوی او نهادند روی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو گرگان که دیوان مازندران</p></div>
<div class="m2"><p>خروشان زمین را به دندان کنان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به نعره دل کوه بشکافتند</p></div>
<div class="m2"><p>زنراژدها سرنه بر تافتند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به سرشان سرونی به مانند عاج</p></div>
<div class="m2"><p>به تن،همچو یلان،همه رنگ ساج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به رزم فرامرز نیو آمدند</p></div>
<div class="m2"><p>خروشان به کردار دیو آمدند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو نزد سپهبد رسیدند تنگ</p></div>
<div class="m2"><p>جوان بر کمان راند تیر خدنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی تیر پیکان زهرآبدار</p></div>
<div class="m2"><p>به چرخ اندرون راند گرد سوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بزد بر برو سینه گرگ نر</p></div>
<div class="m2"><p>دد از زخم تیر اندر آمد به سر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیفتاد بر جایگه،جان بداد</p></div>
<div class="m2"><p>خروشید جفتش درآمد چوباد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برانگیخت از رزمگه،تیره گرد</p></div>
<div class="m2"><p>یکی بر فرامرز یل حمله کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به پای تکاور درافتاد گرگ</p></div>
<div class="m2"><p>سرون بر سرش همچو دار بزرگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سیه را سرونی بزد بر زهار</p></div>
<div class="m2"><p>شکم بردریدش به یک زخم خوار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به خاک اندرآمد تکاور زدرد</p></div>
<div class="m2"><p>جداشد زپشتش یل شیر مرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بزد دست و کوپال را برکشید</p></div>
<div class="m2"><p>چو گرگ اندرآمد دلش بردمید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برآورد و زد بر سرش کرد پست</p></div>
<div class="m2"><p>سرویال و گردنش درهم شکست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو افکند گرگان،یل با شکوه</p></div>
<div class="m2"><p>از آنجا بیامد دمان زی گروه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پیاده سوی چشمه آمد به آب</p></div>
<div class="m2"><p>به رخ،ارغوان و به دل کامیاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بخورد آب وروی و سرو تن نشست</p></div>
<div class="m2"><p>به اندیشه خوب ورای درست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به پیش جهاندار،زاری نمود</p></div>
<div class="m2"><p>نیایش سزاوار او برفزود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که ای برتر از جایگاه و مکان</p></div>
<div class="m2"><p>تو بودی مرا یار براین ددان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همی گفت از این سان شگفتی که دید</p></div>
<div class="m2"><p>بدین گونه دد در جهان کس ندید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چواز آفرین جهان آفرین</p></div>
<div class="m2"><p>بپرداخت،برگشت از دشت کین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سوی لشکر خود بتازید تفت</p></div>
<div class="m2"><p>گرازان و نخجیر جویان برفت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وزآن سو که بد لشکر نامدار</p></div>
<div class="m2"><p>غمی گشته از پهلوان سوار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همی گفته هرکس که شد دیرگاه</p></div>
<div class="m2"><p>جهان پهلوان مهتر رزمخواه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زآوردگاه بازماند همی</p></div>
<div class="m2"><p>نداند کسی تا چه باشد همی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مبادا که چشم بد روزگار</p></div>
<div class="m2"><p>رساند گزندی به آن نامدار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به چندان نبرده دم رستخیز</p></div>
<div class="m2"><p>که پیش آمدش روزگار ستیز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همه باره فیروز باز آمدی</p></div>
<div class="m2"><p>جهان را به فرش نیاز آمدی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کنون چون که نامد به دشت نبرد</p></div>
<div class="m2"><p>بتازیم واز ره برآریم گرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شویم وببینیم تا حال چیست</p></div>
<div class="m2"><p>مبادا که برخود بباید گریست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>برفتند گردان همه هم زمان</p></div>
<div class="m2"><p>غریوان وپردردو زاری کنان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو یک میل تازان برفتندپیش</p></div>
<div class="m2"><p>بدیدند آن گردپاکیزه کیش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پیاده به تن برسلاح گران</p></div>
<div class="m2"><p>خوی از تن روان همچوآب روان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خروشان برفتند بالای او</p></div>
<div class="m2"><p>چودیدندبرخاک ره پای او</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بزرگان بروآفرین خواندند</p></div>
<div class="m2"><p>سراسر بدوگوهرافشاندند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به گردان چنین گفت پس پهلوان</p></div>
<div class="m2"><p>که شایدکه مردان روشن روان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گرایند زی دشت آوردگاه</p></div>
<div class="m2"><p>کننداندران آن تندگرگان نگاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که هرگزبدین سان دوگرگ بزرگ</p></div>
<div class="m2"><p>دلیران وپرخاشجوی سترگ</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ندیدند و نشینده باشند نیز</p></div>
<div class="m2"><p>که سر شد زمانشان به گاه ستیز</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>برفتند گردان و دیدند زود</p></div>
<div class="m2"><p>زگرگان برآورده از تیغ،دود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بدان زخم بازوی گرد دلیر</p></div>
<div class="m2"><p>همی آفرین خواند برنا وپیر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بکندند دندان هاشان ز بن</p></div>
<div class="m2"><p>پراز آفرینشان سراسر سخن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به لشگر گه خویش رفتند باز</p></div>
<div class="m2"><p>شده ایمن از رنجو سوز و گداز</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به کشور پراکنده شد داستان</p></div>
<div class="m2"><p>میان بزرگان کشورستان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>که از گرگ و از اژدها و زشیر</p></div>
<div class="m2"><p>به تیغ جهانجوی گرد دلیر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سراسر تهی گشت روی زمین</p></div>
<div class="m2"><p>بزرگان هر مرز با آفرین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>برفتند نزدیک او با نثار</p></div>
<div class="m2"><p>ببردند هر چیز کامد به کار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>در آن مرز خرم،شه و پهلون</p></div>
<div class="m2"><p>ابا نامداران و فرخ گوان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ببودند یک چند با نای ونوش</p></div>
<div class="m2"><p>ز رامش همه مست و سرپرخروش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گهی در شکار و نیستان بدند</p></div>
<div class="m2"><p>گهی با حضور و میستان بدند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بدین گونه شش ماه دیگر گذشت</p></div>
<div class="m2"><p>چه در جویبار و چه در کوه و دشت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>از آن پس فرامرز روشن روان</p></div>
<div class="m2"><p>ابا شاه ولشکر،چه پیر و جوان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از آنجا سوی شهر بشتافتند</p></div>
<div class="m2"><p>همه کام و امید دریافتند</p></div></div>