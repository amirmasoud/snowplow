---
title: >-
    بخش ۹۱ - پاسخ دادن رای هند،فرستاده فرامرز را
---
# بخش ۹۱ - پاسخ دادن رای هند،فرستاده فرامرز را

<div class="b" id="bn1"><div class="m1"><p>شه هندوان پاسخش دادباز</p></div>
<div class="m2"><p>که هستم ازین گفته ها بی نیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو برگرد از ایدر بزودی برو</p></div>
<div class="m2"><p>ببر پاسخ از من به سالار نو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که تو نورسیده سپهدار گرد</p></div>
<div class="m2"><p>تو از خویشتن دیده ای دستبرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو بازوی مردان نپیموده ای</p></div>
<div class="m2"><p>از آن بازوی خویش بستوده ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو با آهوان ساختی کارزار</p></div>
<div class="m2"><p>نه من شیر دشتم نه گور شکار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبینی مرا روزگار نبرد</p></div>
<div class="m2"><p>بدانگه که مرد اندرآید به مرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به تخت مهی برنسازم درنگ</p></div>
<div class="m2"><p>من و تیغ هندی و میدان جنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی برگرایم فرامرز را</p></div>
<div class="m2"><p>سوران و گردان آن مرز را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کیانوش را خلعت آراستند</p></div>
<div class="m2"><p>کلاه و قبا و کمر خواستند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همانگاه بیرون شد از پیش شاه</p></div>
<div class="m2"><p>دلاور کیانوش زرین کلاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از ایوان به میدان گذر کرد رای</p></div>
<div class="m2"><p>بفرمود تا دردمیدند نای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپه گرد شد بر در رای هند</p></div>
<div class="m2"><p>ز چین و زکشمیر و از مرز سند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بفرمود کز شهر بیرون شوند</p></div>
<div class="m2"><p>دلاور سوی دشت وهامون شوند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برون شد زهندوستان لشکری</p></div>
<div class="m2"><p>که هر فوج از ایشان بد از کشوری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سواران نیزه ور نامدار</p></div>
<div class="m2"><p>همانا که بودند سیصدهزار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی پهلوان بد تجانو به نام</p></div>
<div class="m2"><p>به مردی بگسترده در هند نام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو دیوی بد آن سهمگین دیو زشت</p></div>
<div class="m2"><p>ز دود تف دوزخ او را سرشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به بالا زسی رش بدی او فزون</p></div>
<div class="m2"><p>چو پیلانش دندان،دو دیده چو خون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به بازوی پیل وبه نیروی شیر</p></div>
<div class="m2"><p>به چنگال،همچون هژبر دلیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به تک همچو باد وبه تن،همچو کوه</p></div>
<div class="m2"><p>زمین از کشیدندش گشتی ستوه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زلشکر بدو داد پنجه هزار</p></div>
<div class="m2"><p>سواران گرد ودلیران کار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فرامرز را تا پذیره شود</p></div>
<div class="m2"><p>ابا پیل و کوس وتبیره شود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدو رای گفت ای یل نیکخواه</p></div>
<div class="m2"><p>شب وروز باید طلایه به راه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که ایرانیان اندرآیند زود</p></div>
<div class="m2"><p>به تیغ و به زوبین برآرند دود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرآن کس که دارد بدین ملک رای</p></div>
<div class="m2"><p>نباید که مانی یکی را به جای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بیامد تجانوی،مانند باد</p></div>
<div class="m2"><p>پرازکینه سر سوی لشکر نهاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زغریدن کوس واسب نبرد</p></div>
<div class="m2"><p>زآواز گردان و از خاک گرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو گفتی پدیدار شد رستخیز</p></div>
<div class="m2"><p>سم اسب با گرد گفتا که خیز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همی گرد بر شد به کردار باد</p></div>
<div class="m2"><p>جهان گشت پرکینه و پرفساد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پر از غلغل و گفتگو شد جهان</p></div>
<div class="m2"><p>روان گشت از تن،دل بدنهان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدین گونه می راند لشکر چو کوه</p></div>
<div class="m2"><p>چنین تا رسید اندر ایران گروه</p></div></div>