---
title: >-
    بخش ۱۱۴ - نامه نوشتن فرامرز به شاه کیخسرو
---
# بخش ۱۱۴ - نامه نوشتن فرامرز به شاه کیخسرو

<div class="b" id="bn1"><div class="m1"><p>به نامه درون سر به سر یاد کرد</p></div>
<div class="m2"><p>ز رزم و ز پرخاش روز نبرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخستین که بر نامه بنهاد دست</p></div>
<div class="m2"><p>زعنبر سر خامه را کرد مست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ستایش نمودش زیزدان پاک</p></div>
<div class="m2"><p>خداوند کیوان و خورشید وخاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدایی که جان و روان آفرید</p></div>
<div class="m2"><p>به یک مشت خاکی توان آفرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازوباد بر شاه ایران درود</p></div>
<div class="m2"><p>مبادا کم و کاست آن تار و پود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسیدم به قنوج ای شهریار</p></div>
<div class="m2"><p>ابا رای هندی شه نامدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی مرد بودی مهارک به نام</p></div>
<div class="m2"><p>بداندیش و خیره سر و ناتمام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همان جای رای آمدش آرزوی</p></div>
<div class="m2"><p>چنان ابله و ناکس و خیره روی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بخت شهنشاه ایران زمین</p></div>
<div class="m2"><p>بپرداختم بیخ او از زمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشاندم به تخت خود آن نامدار</p></div>
<div class="m2"><p>به فیروزی ودولت شهریار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنون بنده ام شهریار جهان</p></div>
<div class="m2"><p>چه فرمایدم آشکار و نهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فرستاده برجست و آمد به راه</p></div>
<div class="m2"><p>چنین تا بیامد بر تخت شاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کسی گفت نزدیک شاه جهان</p></div>
<div class="m2"><p>که آمد فرستاده پهلوان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شهنشاه ایران ورا پیش خواند</p></div>
<div class="m2"><p>به نزدیکی پیشگاهش نشاند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همان نامه نزدیک شاه زمین</p></div>
<div class="m2"><p>نهاد و برو خواند بس آفرین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بپرسید ازو خسرو نامدار</p></div>
<div class="m2"><p>زکار فرامرز و وز کارزار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرستاده با شه یکایک بگفت</p></div>
<div class="m2"><p>زنیک و بد آشکار ونهفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سپرد آنچه آورده بد سر به سر</p></div>
<div class="m2"><p>به گنجور شاهنشه دادگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شهنشه پسندید و کردآفرین</p></div>
<div class="m2"><p>بدان شیر دل پهلوان زمین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فرستاده را خلعت و اسب داد</p></div>
<div class="m2"><p>برو بر بسی آفرین کرد یاد</p></div></div>