---
title: >-
    بخش ۳۲ - آغاز داستان فرامرز نامه و سرگذشت آن گوید
---
# بخش ۳۲ - آغاز داستان فرامرز نامه و سرگذشت آن گوید

<div class="b" id="bn1"><div class="m1"><p>ایا دوستداران والا گهر</p></div>
<div class="m2"><p>ز من بشنوید این چنین سر به سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نو آورم داستانی ز پی</p></div>
<div class="m2"><p>ز هندوستان آورم ملک ری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که روزی ز ایام کاوس کی</p></div>
<div class="m2"><p>ندایی به هند آمدش پی ز پی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نام خداوند روزی رسان</p></div>
<div class="m2"><p>یکی قصه آرم برون از نهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به توفیق آن قادر کردگار</p></div>
<div class="m2"><p>کنم نظم ها چون در شاهوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز مردی و جنگ فرامرز گرد</p></div>
<div class="m2"><p>ز گیتی چنان گوی دولت ببرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی روز با رامش و میگسار</p></div>
<div class="m2"><p>نشسته دلیران بر شهریار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شبان و رمه سربه سر انجمن</p></div>
<div class="m2"><p>فریبرز و توس گو پیلتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرامرز و گودرز و بهرام و گیو</p></div>
<div class="m2"><p>چه گستهم و رهام و گرگین نیو</p></div></div>