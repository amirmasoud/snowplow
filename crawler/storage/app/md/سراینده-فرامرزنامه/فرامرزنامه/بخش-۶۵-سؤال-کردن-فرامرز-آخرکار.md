---
title: >-
    بخش ۶۵ - سؤال کردن فرامرز،آخرکار
---
# بخش ۶۵ - سؤال کردن فرامرز،آخرکار

<div class="b" id="bn1"><div class="m1"><p>به پاسخ بدو گفت کای سرفراز</p></div>
<div class="m2"><p>ببایدت بردن مرو را نماز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو فرزند گشتاسب را سر نهی</p></div>
<div class="m2"><p>بماند به تو تاج و تخت مهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگر سر بپیچی گزند آیدت</p></div>
<div class="m2"><p>سرنامور زیر بند آیدت</p></div></div>