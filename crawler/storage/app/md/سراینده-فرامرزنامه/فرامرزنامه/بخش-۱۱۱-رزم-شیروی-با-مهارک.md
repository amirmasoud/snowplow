---
title: >-
    بخش ۱۱۱ - رزم شیروی با مهارک
---
# بخش ۱۱۱ - رزم شیروی با مهارک

<div class="b" id="bn1"><div class="m1"><p>سپه را برانگیخت شیروی گرد</p></div>
<div class="m2"><p>برایشان هم از گرد ره حمله برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزد بر سپاه مهارک سپاه</p></div>
<div class="m2"><p>یکی رود خون خاست زآوردگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپه را به یک حمله از جا بکند</p></div>
<div class="m2"><p>به دروازه شهرشان درفکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فراوان زهندو سپه کشته شد</p></div>
<div class="m2"><p>زکشته در شهر چون پشته شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهارک چو افکند خود را به شهر</p></div>
<div class="m2"><p>زبیشی نمودن،غم آمدش بهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببستند دروازه شهر،تنگ</p></div>
<div class="m2"><p>زباره فراوان ببارید سنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپهدار شیر یل نامجوی</p></div>
<div class="m2"><p>ندید ایستادن به دروازه روی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپاه از درشهر بیرون کشید</p></div>
<div class="m2"><p>بیامد سوی دشت وهامون کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کس از شهر،بیرون نیامد به جنگ</p></div>
<div class="m2"><p>به مردی نجستند وهم نام وننگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خردمند شیروی آزاده مرد</p></div>
<div class="m2"><p>به سوی فرامرز یل نامه کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نویسنده را خواند نزدیک خود</p></div>
<div class="m2"><p>بدو گفت ای موبد پر خرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان چون سزای جهان پهلوان</p></div>
<div class="m2"><p>یکی نامه بنویس در دم روان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نخستین که بر نامه بنهاد دست</p></div>
<div class="m2"><p>سر نامه بر نام یزدان ببست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازآن پس فراوان ستایش گرفت</p></div>
<div class="m2"><p>فرامرز یل را نیایش گرفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به جنگ مهارک به روز نخست</p></div>
<div class="m2"><p>به دروازه شهر کو راه جست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنون شهر کشمیر دارد حصار</p></div>
<div class="m2"><p>ندارد همی مایه کارزار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من ایدون در شهر دارم نشست</p></div>
<div class="m2"><p>مگر بد سگال آیدم پیش دست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر رای بیند جهان پهلوان</p></div>
<div class="m2"><p>سوی شهر کشمیر پیچد عنان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که من درگمانم که در شهر چین</p></div>
<div class="m2"><p>پر از لشکر و پر سلاح گزین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدین مایه لشکر نیاید به جنگ</p></div>
<div class="m2"><p>همان بوم وبر از عدو هست تنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کنون برهیونی جگردار باد</p></div>
<div class="m2"><p>فرستاد زی گرد فرخ نژاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وز آن سو به روز چهارم پگاه</p></div>
<div class="m2"><p>چو خورشید از گرد شد لاجورد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برون برد از شهر، مردان کار</p></div>
<div class="m2"><p>سوی رزم شیرو ده ودو هزار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سپهدار شیروی با هوش و هنگ</p></div>
<div class="m2"><p>شتابش بیامد به جای درنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنان دان که سالار با هوش وتاب</p></div>
<div class="m2"><p>کجا بازداند درنگ از شتاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به هوشی شکیب و جوانی خرد</p></div>
<div class="m2"><p>سربخت دشمن به چنگ آورد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدین سان همی بود شیروی گرد</p></div>
<div class="m2"><p>سپه را به تیزی سوی کس نبرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ولیکن برابر صفی برکشید</p></div>
<div class="m2"><p>بدان تا که دشمن به جا آورید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نبودش مر آن رزم را آرزوی</p></div>
<div class="m2"><p>مگر پهلوان در رسد اندروی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وز آن سو فرامرز با رای هند</p></div>
<div class="m2"><p>بیامد به قنوج از راه سند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به شهر اندر آورد رای برین</p></div>
<div class="m2"><p>چنان چون سزا بود با آفرین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به مردیش بنشاند بر تخت عاج</p></div>
<div class="m2"><p>نهادش به سر بر دل افروز تاج</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مسخر شدش بوم هندوستان</p></div>
<div class="m2"><p>برو راست شد ملک جادوستان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به رامش نشستند با فرهی</p></div>
<div class="m2"><p>برآراسته بزم شاهنشهی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شب و روز با گور و نخجیر و می</p></div>
<div class="m2"><p>همه شادمانی فکندند پی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>فرستاده ای کرد شیرو به شب</p></div>
<div class="m2"><p>به نزد فرامرز شد با ادب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چوآن نامه را بر فرامرز داد</p></div>
<div class="m2"><p>سپهبد روانی به خواننده داد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زشیرو چو آگاه شد پهلوان</p></div>
<div class="m2"><p>زرامش سوی جنگ شد در زمان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نباید که با رامش و رود و می</p></div>
<div class="m2"><p>نشیند ورا دشمن آید زپی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدان گه نشین خرم و شادمان</p></div>
<div class="m2"><p>که نبود زگیتی به دشمن نشان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همانگه بفرمود تا کوس جنگ</p></div>
<div class="m2"><p>ببستند بر اسب و بر پیل،تنگ</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چوآمد به گوش سپه بانگ کوس</p></div>
<div class="m2"><p>شد از گرد،رخسار مهر آبنوس</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو شبرنگ مه لعل در زین کشید</p></div>
<div class="m2"><p>در ابرو زخشم اندرون چین کشید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همی راند روز وشبان با شتاب</p></div>
<div class="m2"><p>چو اندر هوا مرغ و ماهی درآب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زمنزل شبانگه چو برداشتی</p></div>
<div class="m2"><p>دو منزل زیک روز بگذاشتی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چنان در شب تیره کردی گذر</p></div>
<div class="m2"><p>که از نامده روز رفتی به در</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همی تاخت زین گونه شیرژیان</p></div>
<div class="m2"><p>نخورد ونیاسود روز و شبان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو تنگ اندرآمد به شیروی گرد</p></div>
<div class="m2"><p>هم از گرد ره بر عدو حمله برد</p></div></div>