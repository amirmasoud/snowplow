---
title: >-
    بخش ۳۴ - این چند سخن در باب شکایت روزگار هنگام پیری خود گوید
---
# بخش ۳۴ - این چند سخن در باب شکایت روزگار هنگام پیری خود گوید

<div class="b" id="bn1"><div class="m1"><p>چو سالم بشد سی و شش این زمان</p></div>
<div class="m2"><p>زپیری رسیده به سر مر زبان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زباد خزانی رخم زرد شد</p></div>
<div class="m2"><p>گل ارغوان رخم گرد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنفشه سمن گشت گل شد تهی</p></div>
<div class="m2"><p>شدم چنبری شاخ سرو سهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنم خم گرفت و دلم غم گرفت</p></div>
<div class="m2"><p>دو دیده بشوریده رخ نم گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خورشید بر من نیامد تفی</p></div>
<div class="m2"><p>وزین خرمنم چون نیامد کفی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه گرگی زمیشم بشد در کنار</p></div>
<div class="m2"><p>گلی هم نیامد نصیب از بهار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان پر ز گنجست و ما پر ز رنج</p></div>
<div class="m2"><p>شکوفه به هر سوی ما در شکنج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان را همه باده هست و نوا</p></div>
<div class="m2"><p>مرا باد در دست و خود بینوا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز نرگس کفم بازو جام شد</p></div>
<div class="m2"><p>نظرگاه زو نقره خام شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه شاخ با زور و سیمست چیز</p></div>
<div class="m2"><p>زرافشان همه ساله و نقره ریز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا چهره زرد و مو سیم شد</p></div>
<div class="m2"><p>وزین نیستی دل به دو نیم شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کسی را که بردست گیتی نیاز</p></div>
<div class="m2"><p>بدان سان که بینی گرفتار آز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرانمایه گنجش به هر چند بیش</p></div>
<div class="m2"><p>دلش آز بگرفت در بند نیش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به بخشش بیارای و بگشای دل</p></div>
<div class="m2"><p>زبیشی برون کش همی پای دل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نگویم مرا ده که سخت آیدت</p></div>
<div class="m2"><p>گران سنگ زان بر درخت آیدت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همی گویمت شاد و خرم بزی</p></div>
<div class="m2"><p>بیا شاد و خوش باش بی غم بزی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ببخش و بیارام و بگشای مهر</p></div>
<div class="m2"><p>که ناگه زبالای چهرت سپهر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگردد بگرداندت سرنگون</p></div>
<div class="m2"><p>به کاری بیابی فریب و فسون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سرتاجداران به گرد اندراست</p></div>
<div class="m2"><p>ترا دل بدین روز حرص اندر است</p></div></div>