---
title: >-
    بخش ۱۷۶ - نامه نوشتن زال به فرامرز از کار بهمن و جنگ کردن
---
# بخش ۱۷۶ - نامه نوشتن زال به فرامرز از کار بهمن و جنگ کردن

<div class="b" id="bn1"><div class="m1"><p>سر نامه از زال بسیار سال</p></div>
<div class="m2"><p>که گردون ورا کرد بی پر و بال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازو چرخ بستد همه گرد و مال</p></div>
<div class="m2"><p>شد از گشت گردون،بی پر وبال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نزد نبیره فرامرز گو</p></div>
<div class="m2"><p>که درهند،شاه است و هم پیشرو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان ای پسر کین جهان دیده پیر</p></div>
<div class="m2"><p>کهن گشته از عهد نوروز دیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمانه درآوردش اکنون زپای</p></div>
<div class="m2"><p>نه زورش بماندست نه هوش و رای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنش لرزه وناتوانی گرفت</p></div>
<div class="m2"><p>دلش رای دیگر جهانی گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هر روز کز چرخ می بگذرد</p></div>
<div class="m2"><p>مرا جان و نیرو به تن بفسرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابا این چنین ناتوانی و رنج</p></div>
<div class="m2"><p>مرا خوش نیاید سرای سپنج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که دشمن چنین بی کران بر دراست</p></div>
<div class="m2"><p>زمین،شصت فرسنگ پرلشکر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه راه گریز ونه روی رها</p></div>
<div class="m2"><p>بماندم چنین در دل اژدها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به شهر اندرون مردم لشکری</p></div>
<div class="m2"><p>فدا کرده جان را به فرمانبری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شب وروز پیکار جویند و جنگ</p></div>
<div class="m2"><p>بکوشیده اند ازپی نام وننگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کنون توشه ای هم نماند ای پسر</p></div>
<div class="m2"><p>نه یک دانه گندم نه یک دانه زر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در این شهر اگر باشد از بیش وکم</p></div>
<div class="m2"><p>برابر بخوردند نان با درم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو از مردم شهر گشتم خجل</p></div>
<div class="m2"><p>نوشتم من این نامه از درد دل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو فریاد مردم به گردون رسید</p></div>
<div class="m2"><p>شکم گرسنه بیش از این نارمید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدیشان بدادم یک امشب امید</p></div>
<div class="m2"><p>که تا خود چه آید به روز سفید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو بدرود باش ای گرامی پسر</p></div>
<div class="m2"><p>نبینی از این پس رخ زال زر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که فردا به جایی رسد کاخ شهر</p></div>
<div class="m2"><p>مرا خاک بیزاست زین هردو بهر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سرایی که از گاه گرشاسب شاه</p></div>
<div class="m2"><p>بدی خسروان جهان را پناه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کنون کرد خواهند با خاک راست</p></div>
<div class="m2"><p>چه مایه زدشمن به مایه بر بلاست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شب تیره کین نامه بنوشته ام</p></div>
<div class="m2"><p>زمین را به خون دل آغشته ام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنانم که گاه پرستش نمای</p></div>
<div class="m2"><p>به ده مرد،پایم برآرد زجای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تبه گشتم از گردش ماه وسال</p></div>
<div class="m2"><p>کنون مرغ عمرم بیفکند بال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اجل،تیغ کین بر سرم آخته</p></div>
<div class="m2"><p>جهان خواهد از زال پرداخته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو از من برآرد زمانه دمار</p></div>
<div class="m2"><p>زمن باد برگردنت زینهار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که در کابل و زابل ای جان باب</p></div>
<div class="m2"><p>نجویی تو آرامش و خورد وخواب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به هندوستان خوی آرامگاه</p></div>
<div class="m2"><p>سراندیب شمشیر گیری پناه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که با شاه ایران نتابی به جنگ</p></div>
<div class="m2"><p>گریزان زپیشش تو را نیست ننگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جوانی مکن،پند من یاد دار</p></div>
<div class="m2"><p>مکن خیره با جان خود زینهار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جهاندار بهمن،هزاران هزار</p></div>
<div class="m2"><p>سپه دار وساز و مردان کار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زخاور مراو راست تا باختر</p></div>
<div class="m2"><p>جهان،زیر فرمان او سر به سر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو را باب،کشته نیا پرور است</p></div>
<div class="m2"><p>زخویشان تو نیست کس تندرست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سپاهت همه گشته پردخته پاک</p></div>
<div class="m2"><p>برآورد از کشورت تیره خاک</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چوبا دشمن امروز در خور نه ای</p></div>
<div class="m2"><p>جوانی مکن چون برابر نه ای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سرخویش گیر وبه آنجا ممان</p></div>
<div class="m2"><p>همان نامه با سپاسی بخوان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو را گر بدی پشت،یاور به کار</p></div>
<div class="m2"><p>زواره بدی زنده،سام سوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ابا شاه پیکار در خور بدی</p></div>
<div class="m2"><p>چو باب و پسر، هردو یاور بدی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کنون ای فرامرز،تدبیر پیر</p></div>
<div class="m2"><p>تو بیهوده جان را مده خیر خیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فراوان از این در بسی در نوشت</p></div>
<div class="m2"><p>به خون دل ودیده اندر سرشت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به پوینده ای داد بر سان باد</p></div>
<div class="m2"><p>رسانید نزد فرامرز راد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>درآن جایگه،زال غمگین برفت</p></div>
<div class="m2"><p>غریوان،راه پرستش گرفت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خروشان وگریان شب دیرباز</p></div>
<div class="m2"><p>همی بودتاگاه بانک نماز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خمیده گهی چون کمان پشت او</p></div>
<div class="m2"><p>گهی برزمین بد سرانگشت او</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گهی بر هوا روی،گه بر زمین</p></div>
<div class="m2"><p>گهی خوانده برکردگار آفرین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سحرگه به خواب اندرآمد سرش</p></div>
<div class="m2"><p>یکی مرد را دید کامد برش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چنین گفت مر زال را کای پسر</p></div>
<div class="m2"><p>زبهر خورش درد چندی مبر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نیای تو خود گرد گرشاسب نام</p></div>
<div class="m2"><p>زبهر تو رنجی کشیدست سام</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در این پیشگاهست کاخ بلند</p></div>
<div class="m2"><p>برآید سرش شصت تار کمند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تبرخواه و بیلی،سرش بازکن</p></div>
<div class="m2"><p>سپه را از آن دادن آغازکن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گرانمایه دستان درآمد زخواب</p></div>
<div class="m2"><p>زشادی،روانش گرفته شتاب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همی گفت کین کار اهریمنست</p></div>
<div class="m2"><p>که او آدمی را به دل،دشمنست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هر آن کو کند پیشه،فرمان دیو</p></div>
<div class="m2"><p>شود گمره از راه کیهان خدیو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ندارد بجز باد،چیزی به دست</p></div>
<div class="m2"><p>خنک هر که از دیو دوزخ برست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گهی گفت گفتار گرشسب گرد</p></div>
<div class="m2"><p>به بازی همانا که نتوان شمرد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بفرمودپس تا به بیل وتبر</p></div>
<div class="m2"><p>مرآن خانه را بازکردند سر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>یکی لوح دید از برش لاجورد</p></div>
<div class="m2"><p>نوشته که این کاخ،گرشسب کرد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو دیدم که این روزگاراست پیش</p></div>
<div class="m2"><p>پراز دانه کردم من از رنج خویش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>یک امروزت این دانه اندرخور است</p></div>
<div class="m2"><p>که هر دانه ای دانه گوهر است</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به شهراندرون بانگ زد زال پیر</p></div>
<div class="m2"><p>که گفتار پیران مدارید خیر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بیایید روزی به خانه برید</p></div>
<div class="m2"><p>وزین کاخ گرشاسب،دانه برید</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>همه شهر از این کار،پرگفتگو</p></div>
<div class="m2"><p>به درگاه دستان نهادند روی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به خورشید فرمود دستان سام</p></div>
<div class="m2"><p>که بنویس مرا همگنان را تو نام</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بده هریکی را تویک من خورش</p></div>
<div class="m2"><p>که تنشان بیاید ازین پرورش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>از آن گوشت کاری زدند آن برون</p></div>
<div class="m2"><p>نه کس زورگیرد نه نیرو فزون</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چنین کرد وبگذشت یک چند روز</p></div>
<div class="m2"><p>چوشد خورده آن دانه دلفروز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بپوشید مردم همه ساز جنگ</p></div>
<div class="m2"><p>یکی همچو شیر و یکی چون پلنگ</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>گشادند دروازه بی آگهی</p></div>
<div class="m2"><p>شده مغز،خشک و شکم ها تهی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به بهمن نهادند یکباره روی</p></div>
<div class="m2"><p>نه آگاه از ایشان یل نامجوی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو دستان چنان دید،خیره بماند</p></div>
<div class="m2"><p>همان گاه خورشید را پیش خواند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نبینی بدو گفت این بی بنان</p></div>
<div class="m2"><p>گرفتند کردار اهریمنان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>تو بیرون خرام و سر پل بگیر</p></div>
<div class="m2"><p>که سرها بدادند بر خیره خیر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>یکی جوشن نامداران بپوش</p></div>
<div class="m2"><p>اگر جنگ پیش آیدت هم بکوش</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بشد ماه پیکر،سرپل گرفت</p></div>
<div class="m2"><p>همه لشکر،آشوب وغلغل گرفت</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به بازار شد مردم گرسنه</p></div>
<div class="m2"><p>همان لشکرش درمیان بنه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>کشیدند شمشیر و زوبین جنگ</p></div>
<div class="m2"><p>شهنشه ندید هیچ روی درنگ</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سراپرده بگذاشت شد سوی کوه</p></div>
<div class="m2"><p>سپه پیش او شد گروهاگروه</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>نظاره شدند اندر آن مردمان</p></div>
<div class="m2"><p>ازایشان بسی را سرآمد زمان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بخوردند چیزی که بد خوردنی</p></div>
<div class="m2"><p>ببردند چیزی که بد بردنی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چوبازارگه خوردنی تنگ کرد</p></div>
<div class="m2"><p>سوی شهر هرکس شد آهنگ کرد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>به گردان چین گفت شاه زمین</p></div>
<div class="m2"><p>که هرگز ندیدیم تنگی چنین</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>که چون گرسنه مردم انبوه شد</p></div>
<div class="m2"><p>زبیمش سپه بر سرکوه شد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چنان شد که شهری بروخاسته</p></div>
<div class="m2"><p>سپاهی روان را زغم کاسته</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چنین گفت هنگام خنده بود</p></div>
<div class="m2"><p>مبادا سپاهی که زنده بود</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>شما زنده و دشمنم بیم مرگ</p></div>
<div class="m2"><p>به شهراندر آرد همی تیغ و ترگ</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>سپه شد زگفتار بهمن خجل</p></div>
<div class="m2"><p>همی هرکس تیزتر شد به دل</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چو خورشید مه پیکر او را بدید</p></div>
<div class="m2"><p>خروشی به چرخ برین برکشید</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چوبر تیغ بفشرد انگشت کین</p></div>
<div class="m2"><p>از ایشان تنی چند بر زمین</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چوپران شد از یال خورشید،خشت</p></div>
<div class="m2"><p>زخون دلیران،زمین،لاله کشت</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>وزآنجا به شهر اندرون شد دژم</p></div>
<div class="m2"><p>از آن خوردنی،بهر او رنج وغم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بپرسید خروشید را زال پیر</p></div>
<div class="m2"><p>که ای دخت پورگو شیرگیر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>تو بودی بدین رزم،فریادرس</p></div>
<div class="m2"><p>همه شهر،جان از تو دارند وبس</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>سپه را نبایست بیرون شدن</p></div>
<div class="m2"><p>زبهر شکم در پی خون شدن</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بدوگفت خورشید کای مهربان</p></div>
<div class="m2"><p>شکم گرسنه کسی شکیبد ز نان</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>نبیند همی موج دریا دمن</p></div>
<div class="m2"><p>نجوشد شکم گر نباشد دهن</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>وزآن روی،بهمن به فرزانه گفت</p></div>
<div class="m2"><p>که شاید از ایدر بمانی شگفت</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بدین خیرگی مردم زیردست</p></div>
<div class="m2"><p>ندیدم به دوران چنین تنددست</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>دلم خیره ماند اندر آن یک سوار</p></div>
<div class="m2"><p>که چندین هنر کرد در کارزار</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>گرفته سرپیل نیزه به دست</p></div>
<div class="m2"><p>ببین نامداران ما کرد پست</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>همی حمله آورد برسان باد</p></div>
<div class="m2"><p>ندیدم که گامی به پس تر نهاد</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چنین داد پاسخ،خردمند مرد</p></div>
<div class="m2"><p>که شاه جهان خیره اندیشه کرد</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بکوشد همی هرکس از بهرآن</p></div>
<div class="m2"><p>بسا کز پی نان فدا کرد جان</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>زکوشش گه رزم وکین چاره نیست</p></div>
<div class="m2"><p>سپه را زپیکار،بیغاره نیست</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>سرافراز خورشید مینو نمای</p></div>
<div class="m2"><p>گریزان براند لشکری را زجای</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>اگر شاه ازو گیرد امروز کین</p></div>
<div class="m2"><p>نباشد ره داد و آیین دین</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>چوشاه جهان،دل پر از کین کنید</p></div>
<div class="m2"><p>بروبخت بیدار،نفرین کنید</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چنان نامداری بر شهریار</p></div>
<div class="m2"><p>بهست از سپاهی که ناید کار</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>هم اکنون بسازم یکی پای دام</p></div>
<div class="m2"><p>کزین چاره شاها برآیدت کام</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>شکم گرسنه چون خورش یابد او</p></div>
<div class="m2"><p>نگرداند از تیغ برنده رو</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>شب تیره،بازاریان را بخواند</p></div>
<div class="m2"><p>زهر در سخن ها فراوان براند</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>بفرمود تا زود برخاستند</p></div>
<div class="m2"><p>دکان ها به آیین بیاراستند</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>زماهی و از مرغ بریان گرم</p></div>
<div class="m2"><p>همان چرب و شیرین و از نان نرم</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>چه نار و چه انگور وبادام و سیب</p></div>
<div class="m2"><p>کزآن گرسنه را نبودی شکیب</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>سیه مرد را گفت تا سی هزار</p></div>
<div class="m2"><p>سپه دار و زوبین وهم نیزه دار</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>بسازید در خیمه هاشان کمین</p></div>
<div class="m2"><p>نباید که باشد کس آگاه ازین</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چو شد روز،آمد به پای سپاه</p></div>
<div class="m2"><p>به بازار کردند هرکس نگاه</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>بهشت برین بود گویی درست</p></div>
<div class="m2"><p>همه شهریاران پایشان گشت سست</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>سوی نیستان آمد از بوی نوش</p></div>
<div class="m2"><p>همی رفت از آن بوی،مردم زهوش</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>همان گاه دروازه کردند باز</p></div>
<div class="m2"><p>خبر شد به دستان گردن فراز</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>فرستاد نزدیک ایشان پیام</p></div>
<div class="m2"><p>که روبه همی دنبه بیند نه دام</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>زیزدان کجا دیو را دشمنست</p></div>
<div class="m2"><p>که آن خوردنی،دام اهریمن است</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>اگر بازگردید،بهتر بود</p></div>
<div class="m2"><p>که مرگ از پس بسته افسر بود</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>به شهر اندرون گر بمیرید پاک</p></div>
<div class="m2"><p>از آن به که دشمن نماید هلاک</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>نه کس بازگشت و نه پذرفت پند</p></div>
<div class="m2"><p>همه پند پیران بود سودمند</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>زپیران سخن ها بباید شنید</p></div>
<div class="m2"><p>چواو را گهر یک به یک برگزید</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>سخن های نیکو نکو داشتی</p></div>
<div class="m2"><p>نبهره همی خوار بگذاشتی</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>سخن هر چه از زرگرامی ترست</p></div>
<div class="m2"><p>سخن بر سخن،خود گرامی ترست</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>اگر بشنوی،بشنوانم سخن</p></div>
<div class="m2"><p>وگرنه زبان هیچ رنجه مکن</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>خرد،همچودریا،سخن،گوهر است</p></div>
<div class="m2"><p>چنان است که گوهر به دریا در است</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>کرانه زدریا نیاید پدید</p></div>
<div class="m2"><p>چو یزدان خرد بی گمان او بدید</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>کسی کز خرد،مغزدارد تهی</p></div>
<div class="m2"><p>ندارد زهر دوجهان آگهی</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>چو فرمان دستان نکردند گوش</p></div>
<div class="m2"><p>به خورشید گفتا که اکنون بکوش</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>برو تا سر پل نگهدارشان</p></div>
<div class="m2"><p>به دشمن به یکباره مسپارشان</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>سرپل چو بگرفت باردگر</p></div>
<div class="m2"><p>نهادند مردم به بازار،سر</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>نماند اندر آن شهر،برنا و پیر</p></div>
<div class="m2"><p>به لشکرگه آمد همی خیره خیر</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>به تاراج خوردن گشادند دست</p></div>
<div class="m2"><p>ببردند همی هرکسی گشت پست</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>همی گفت بازاری خیره سر</p></div>
<div class="m2"><p>چو خوردی فراوان از اندر مبر</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>سیه مرد با لشکر پر زکین</p></div>
<div class="m2"><p>زناگه برون آمدند از کمین</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>نهادند بر مردمان،تیغ تیز</p></div>
<div class="m2"><p>برآمد از ایشان یکی رستخیز</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>چوآن شهر از دست دستان برفت</p></div>
<div class="m2"><p>شتابید نزد فرامرز تفت</p></div></div>