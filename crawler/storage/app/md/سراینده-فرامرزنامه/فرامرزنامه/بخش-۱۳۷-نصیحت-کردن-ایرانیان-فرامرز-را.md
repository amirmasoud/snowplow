---
title: >-
    بخش ۱۳۷ - نصیحت کردن ایرانیان،فرامرز را
---
# بخش ۱۳۷ - نصیحت کردن ایرانیان،فرامرز را

<div class="b" id="bn1"><div class="m1"><p>چو بشنید گردنکش دیو بند</p></div>
<div class="m2"><p>سخن های پردرد از آن مستمند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همان گه برآمد به جای نشست</p></div>
<div class="m2"><p>به بر زد بدین سهمگین کار،دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین گفت کاین کار،کار من است</p></div>
<div class="m2"><p>همان این ددان هم شکار من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به زور جهان آفرین دادگر</p></div>
<div class="m2"><p>به فرشهنشاه فیروزگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بر زین کشم تنگ شبرنگ را</p></div>
<div class="m2"><p>بشویم به خون ددان چنگ را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو این گفت، گردان پرخاشجوی</p></div>
<div class="m2"><p>بپیچید هرکس زگفتار اوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهانی بگفتند با یکدگر</p></div>
<div class="m2"><p>کزین شیردل گرد پرخاشخر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدآید سپه را به سرهر زمان</p></div>
<div class="m2"><p>که هر دم رود در دم بدگمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیندیشد از شیر و نراژدها</p></div>
<div class="m2"><p>نه پیل دمان یابد از وی رها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زتیغش دل گرگ،پیچان شود</p></div>
<div class="m2"><p>زگرزش تن ببر،بی جان شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدین گونه سه رزم بر خود گرفت</p></div>
<div class="m2"><p>سزد گر بمانیم ازو در شگفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از این پیش با شیر و دیو وپلنگ</p></div>
<div class="m2"><p>بسی رزم جست آن یل تیز چنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که فیروزگر بود و مشهور گشت</p></div>
<div class="m2"><p>زدیدار او چشم بد دور گشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ولیکن سه جنگ چنین هولناک</p></div>
<div class="m2"><p>که برکوه و صحرا ودر آب و خاک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ندید و نبیند جهان دیده مرد</p></div>
<div class="m2"><p>خردمند،خود را هزیمت نکرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جوانی و گردی و فر وهنر</p></div>
<div class="m2"><p>نژاد و بزرگی ونام وگهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نماند ورا تا تن آسان شود</p></div>
<div class="m2"><p>که از روز سختی هراسان شود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به هر رزم بر خیزدش دل زجای</p></div>
<div class="m2"><p>سوی جنگ دارد همه روزه رای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ولیکن همه روزه نبود چنان</p></div>
<div class="m2"><p>که خود جوید اندر جهان پهلوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چه سازیم و تدبیر این کار چیست</p></div>
<div class="m2"><p>ابا این جوان راه گفتار چیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مبادا کزین جنگ،بی جان شود</p></div>
<div class="m2"><p>دل ودیده ما غریوان شود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر چه هنرمند باشدجوان</p></div>
<div class="m2"><p>نباید بدو بودن ایمن به جان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که هر بار فیروزه باشد به جنگ</p></div>
<div class="m2"><p>بود روزکاید سرش زیرسنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به جنگ اندر اندیشه باید نخست</p></div>
<div class="m2"><p>که ناید سبو یکسر از جو درست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کسی کوکند رای آوردگاه</p></div>
<div class="m2"><p>نباید سوی بازگشتن به راه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سرانجام،ایرانیان،هم زبان</p></div>
<div class="m2"><p>ستایش گرفتند بر پهلوان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گشادند یکسر زبان ها به پند</p></div>
<div class="m2"><p>به خواهش بر مهتر دیو بند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که از فر تو چشم بد دور باد</p></div>
<div class="m2"><p>شب و روز وسال ومهت سور باد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ازآن گه از پیش فرخ پدر</p></div>
<div class="m2"><p>همان از بر شاه فیروزگر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برون آمدی تا کنون روزگار</p></div>
<div class="m2"><p>بود سال بگذشته دو پنج وچار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که یک دم به آرام ننشسته ای</p></div>
<div class="m2"><p>همه ساله پرخاش و کین جسته ای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه جنگ جستی به نام بلند</p></div>
<div class="m2"><p>گهی با کمان و گهی با کمند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جهاندار بخشیده فیروزیت</p></div>
<div class="m2"><p>به هرکار بادا دل افروزیت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سپهر ستیزنده رام تو گشت</p></div>
<div class="m2"><p>نبرد دلیران به کام تو گشت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدان رزم ها سخت دیدیمشان</p></div>
<div class="m2"><p>شب وروز دیدیم در جنگشان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بگفتیم کامد به سر،درد وغم</p></div>
<div class="m2"><p>نبینیم دیگر بلا و ستم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به هنگام شادی و جان پروری</p></div>
<div class="m2"><p>غم اندر دل دوستان آوری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ببندی کمر،این سه رزم گران</p></div>
<div class="m2"><p>پذیرفتی از خسرو قیروان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چه شیر و چه گرگ و چه نراژدها</p></div>
<div class="m2"><p>کز ایشان زمین و زمان در بلا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چوخورشید در هفت چرخ برین</p></div>
<div class="m2"><p>به پرهیز باشد در این رزم و کین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مکن پهلوانا از این درگذر</p></div>
<div class="m2"><p>که کاری بدست این وازبد،بتر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگر جنگ با او همی بایدت</p></div>
<div class="m2"><p>که از نیکنامی بیفزایدت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مکن تیره بر دوستان،روزگار</p></div>
<div class="m2"><p>تن هود بدین رزم،رنجه مدار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جوانی و گردی و نیروی و فر</p></div>
<div class="m2"><p>چرا خوار داری ایا نامور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>که ما رزم گرشسب وسام دلیر</p></div>
<div class="m2"><p>نبرد تهمتن گو نره شیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چه با دیو و شیر و چه با پیل و گرگ</p></div>
<div class="m2"><p>چه با اژدها و یلان سترگ</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شنیدیم و بسیار هم دیده ایم</p></div>
<div class="m2"><p>به نام نکوشان پسندیده ایم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سه رزم چنین در جهان کس ندید</p></div>
<div class="m2"><p>نه از گرزداران گیتی شنید</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خردمند نام آور و تیز ویر</p></div>
<div class="m2"><p>به پای خود اندر دم نره شیر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نرفتست و هرگز نیارد روا</p></div>
<div class="m2"><p>همیدون شدن در دم اژدها</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نه سنگی نه آهن نه پولاد و روی</p></div>
<div class="m2"><p>چه با این ددان کرد خواهی بگوی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نباشیم یک تن بدین داستان</p></div>
<div class="m2"><p>تواین گفته بپذیر از این راستان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گرت دور از ایدر گزندی رسد</p></div>
<div class="m2"><p>به یک موی بر تنت بندی رسد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به نزدیک رستم چه پوزش بریم</p></div>
<div class="m2"><p>چگونه رخ زال زر بنگریم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چه گوییم نزدیک شاه جهان</p></div>
<div class="m2"><p>نکوهش بود از کهان ومهان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نماند یکی زنده از ما به جای</p></div>
<div class="m2"><p>نبینیم این رزم را هیچ پای</p></div></div>