---
title: >-
    بخش ۱۶۰ - سخن گفتن سیه دیو از نژاد فرامرز
---
# بخش ۱۶۰ - سخن گفتن سیه دیو از نژاد فرامرز

<div class="b" id="bn1"><div class="m1"><p>چو هرکس سخن گفت از پهلوان</p></div>
<div class="m2"><p>همان شاه گردان و نام آوران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس آن دیو گویا زبان برگشاد</p></div>
<div class="m2"><p>همی کرد کار فرامرز،یاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخستین ز راه نژاد و گهر</p></div>
<div class="m2"><p>سخن گفت ا زآن پهلو نامور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نژادش یکایک بدیشان شمرد</p></div>
<div class="m2"><p>ز رستم بشد تا نریمان گرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زکورنگ و اترط همی کرد یاد</p></div>
<div class="m2"><p>چنین تا به جمشید بردش نژاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم از مادرش دخت گودرز شیر</p></div>
<div class="m2"><p>که با فر وبرز است و با دار و گیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدین سان که گفتم نژادش کنون</p></div>
<div class="m2"><p>هنرهاش باشد از این در فزون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خردمندی و فر و اورنگ او</p></div>
<div class="m2"><p>مهی و جوانی و آهنگ او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چند گویم از این در سخن</p></div>
<div class="m2"><p>به گفتار هرگز نیاید به بن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنون سیزده سال باشد کنون</p></div>
<div class="m2"><p>که تا این هنر مند با رهنمون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همی پوید اندر جهان نامجوی</p></div>
<div class="m2"><p>گهی نام جوی و گهی کام جوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسا گرگ و شیر و پلنگ و نهنگ</p></div>
<div class="m2"><p>که از گرز او پست شد روز جنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسا دیو جنگی و نر اژدها</p></div>
<div class="m2"><p>که از تیغ تیزش نیابد رها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه مایه حصاری که از کوه و دشت</p></div>
<div class="m2"><p>که از سم اسبش همی تازه گشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسا کارزاری نبرده سوار</p></div>
<div class="m2"><p>بسا شیر دل پهلو نامدار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که از جنگ او روی برگاشتند</p></div>
<div class="m2"><p>بدو کام و امید بگذاشتند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پدرش آن سپهبد جهان پهلوان</p></div>
<div class="m2"><p>نگهدار ایران و پشت گوان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زهنگام شاه جهان کی قباد</p></div>
<div class="m2"><p>کمر بسته دارد به مردی وداد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه کشور ترک وماچین و چین</p></div>
<div class="m2"><p>بدان گونه تا مصر و بر برزمین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگردید یکسر به نعل ستور</p></div>
<div class="m2"><p>برافکند از آن انجمن گرد و شور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زدیوان مازندران در نبرد</p></div>
<div class="m2"><p>زخنجر به گردون برافشاند گرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به تنها تن خویشتن رزم جست</p></div>
<div class="m2"><p>نجست از کسی یاری اندر نخست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زجادو از دیو و از اژدها</p></div>
<div class="m2"><p>یکی را نیامد زتیغش رها</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نماندست اندر جهان هیچ کس</p></div>
<div class="m2"><p>که بر رزم او باشدش دسترس</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همان سام نیرم که بودش نیا</p></div>
<div class="m2"><p>سرافراز وبا رای وبا کیمیا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به هنگام عهد فریدون شاه</p></div>
<div class="m2"><p>به مردی کمر بست بر کرد گاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همان کرگساران مازندران</p></div>
<div class="m2"><p>بپرداخت آن یل به گرز گران</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هزاروصد و شصت دیو دمان</p></div>
<div class="m2"><p>که بودند هریک چو شیر ژیان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که در کرگساران بزرگان بدند</p></div>
<div class="m2"><p>سرافراز و تند و سترگان بدند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گرفتار بند کمندش شدند</p></div>
<div class="m2"><p>به خواری همه زیر بندش شدند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چه با اژدها و چه با نره دیو</p></div>
<div class="m2"><p>چه با شیر و دیو و چه با گرد نیو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یکی بد به رزم اندرون شیرمرد</p></div>
<div class="m2"><p>که بر وی برافشاند گرد نبرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدین مرز کاکنون تو داری نشست</p></div>
<div class="m2"><p>نشان پی اسب و گرزش بسست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به خویشان ضحاک در بوم چین</p></div>
<div class="m2"><p>به خنجر سیه کرد روی زمین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نریمان که بد باب سام سوار</p></div>
<div class="m2"><p>همان است آوازه اش در دیار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چوگرشسب خود در جهان کس نبود</p></div>
<div class="m2"><p>که با او توانست رزم آزمود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر گویم از کار آن نامدار</p></div>
<div class="m2"><p>به چندان بود ناید اندر شمار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همانا شنیده بود تاجور</p></div>
<div class="m2"><p>زکردار مردی آن پرهنر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که با منهراس و همان اژدها</p></div>
<div class="m2"><p>چه کرد آن دلاور به تیغ جفا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کنون این جوان از نژاد چنین</p></div>
<div class="m2"><p>خردمند و با گرد وبا آفرین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به رخ،همچو ماه و به بالا چو سرو</p></div>
<div class="m2"><p>به تن،ژنده پیل و به رفتن،تذرو</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دو بازوش ماننده ران پیل</p></div>
<div class="m2"><p>بجوشد زآوازه او رود نیل</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هنر با نژادش که گفتم همه</p></div>
<div class="m2"><p>به نزد تو ای شهریار رمه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همانا که از صد، یکی بیش نیست</p></div>
<div class="m2"><p>چو او نامور در جهان کس نزیست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو این گفته بشنید فرطورتوش</p></div>
<div class="m2"><p>به دلش اندر آمد از آن کار،هوش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>پدید آمد آن شاه را آرزو</p></div>
<div class="m2"><p>که بیند یکی چهر آن نامجو</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو از باد شب پشت خور خم گرفت</p></div>
<div class="m2"><p>رخ هور چادر فراهم گرفت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>برآمد یکی دیو گرد از جهان</p></div>
<div class="m2"><p>همه آشکارا بشد زو نهان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>برفتند هرکس به دل تندرست</p></div>
<div class="m2"><p>چو خور چهره روز روشن بشست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>یکی تیغ زرآبگون در گرفت</p></div>
<div class="m2"><p>جهان را بدان تیغ در زرگرفت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بیامد سیه دیو نزدیک شاه</p></div>
<div class="m2"><p>ابا ودلیران ایران سپاه</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چنین گفت با شاه فرطورتوش</p></div>
<div class="m2"><p>که ای شاه بینای با رای وهوش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ببودن در این شهر،بسیار گشت</p></div>
<div class="m2"><p>دل مهتر از ما پرآزار گشت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کنون پاسخ نامه باید نوشت</p></div>
<div class="m2"><p>به خوبی و نیکی چو حور از بهشت</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تو نیز از بزرگی وداد وخرد</p></div>
<div class="m2"><p>همان کن که در مردمی می سزد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چنین داد پاسخ بدو شهریار</p></div>
<div class="m2"><p>که ای نیک دل سرور نامدار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>یک امروز دیگر بر ما بپای</p></div>
<div class="m2"><p>که تا پاسخ نامه آرم به جای</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تو با شادمانی به ایوان خرام</p></div>
<div class="m2"><p>ز رفتن،مبر هیچ امروز نام</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>که ما رای پیوند او ساختیم</p></div>
<div class="m2"><p>ز بیگانگی،دل بپرداختیم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سیه دیو،آن گفته بشنید ورفت</p></div>
<div class="m2"><p>ابا نامداران،سوی کاخ،تفت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نشستند با او دلیران به هم</p></div>
<div class="m2"><p>به می تازه کردند روی دژم</p></div></div>