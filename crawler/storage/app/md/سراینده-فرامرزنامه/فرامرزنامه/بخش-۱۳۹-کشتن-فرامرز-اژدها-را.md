---
title: >-
    بخش ۱۳۹ - کشتن فرامرز،اژدها را
---
# بخش ۱۳۹ - کشتن فرامرز،اژدها را

<div class="b" id="bn1"><div class="m1"><p>بگفت و برانگیخت شبرنگ را</p></div>
<div class="m2"><p>برافراخت یال وکئی چنگ را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو تنگ اندرآمد سوی کارزاز</p></div>
<div class="m2"><p>بغرید مانند شیر شکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون آن نعره در گوش اژدر رسید</p></div>
<div class="m2"><p>چو شیر ژیان ویله ای برکشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خود بربجنبید نراژدها</p></div>
<div class="m2"><p>پراکنده شد زهرش اندر هوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلرزید زآسیب او کوه و دشت</p></div>
<div class="m2"><p>زدودش همه آسمان تیره گشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شبرنگ سرکش مر او را بدید</p></div>
<div class="m2"><p>چو خورشید جوشید واندر جهید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رمید و نشد پیش نراژدها</p></div>
<div class="m2"><p>زگردش نهان گشت روی هوا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به تندی زدش پهلوان سوار</p></div>
<div class="m2"><p>که تنگ اندرآید سوی کارزار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنراژدها نیز برگاشت روی</p></div>
<div class="m2"><p>چو رفتن نیارست نزدیک اوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سپهبد شد از بارگی تنگدل</p></div>
<div class="m2"><p>چو او سوی اژدر نیاورد دل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرود آمد از خشم و کردش رها</p></div>
<div class="m2"><p>پیاده بیامد بر اژدها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهان دید یکسر پر از درد وتاب</p></div>
<div class="m2"><p>سیه گشته از دود او آفتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به یزدان پناهید گرد دلیر</p></div>
<div class="m2"><p>ازآن پس برآویخت چون نره شیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به قربان برآورد چاچی کمان</p></div>
<div class="m2"><p>یکی تیر پولاد پیکان گران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نهاده بدو اندر آورد شست</p></div>
<div class="m2"><p>گرفت از بر قبضه چرخ،دست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به ابروی چرخ اندرآورد چین</p></div>
<div class="m2"><p>زگوشه برآمد خروشی به کین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چوبا گوش،گوشه برآورد تنگ</p></div>
<div class="m2"><p>رها کرد از شست تیر خدنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بزد بر میان و دهان و زفر</p></div>
<div class="m2"><p>برآمد یکی جوی خون از جگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خدنگی دگر بر میان و سرش</p></div>
<div class="m2"><p>بزد رفت یکسر به مغز اندرش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو تنگ اندرآمد بدو اژدها</p></div>
<div class="m2"><p>همی جست شیر ژیان زو رها</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زدش دیگری بر میان دو چشم</p></div>
<div class="m2"><p>برافزود بر اژدها درد چشم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دگر خنجر چار شاخ از میان</p></div>
<div class="m2"><p>کشید و بیامد برش تازیان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بزدبر سراو چو نزدیک شد</p></div>
<div class="m2"><p>جهان از تف ودود،تاریک شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دهن باز کرده چو غاری فراخ</p></div>
<div class="m2"><p>شهید ازیل و خنجر چارشاخ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرفت و به کام اندرش در بسوخت</p></div>
<div class="m2"><p>چو دریا ازو خون روان برفروخت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زخنجر دهانش گشاده بماند</p></div>
<div class="m2"><p>همی زهر در چرخ گردون فشاند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به کام اندرش تیغ چون گشت سخت</p></div>
<div class="m2"><p>جوان سرافراز بیدار بخت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برآورد الماسگون تیغ تیز</p></div>
<div class="m2"><p>سرش پر زکین و دلش پر ستیز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دو دستی همی کوفت بر سر ش تیغ</p></div>
<div class="m2"><p>نشد بازویش خم از آن دد دریغ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو از رزم خنجر به سیری رسید</p></div>
<div class="m2"><p>عمود گران از میان برکشید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برآورد و زد بر سرش کرد خورد</p></div>
<div class="m2"><p>ندیده بدان گونه کس دستبرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو او کشته شد جوشن از دود زهر</p></div>
<div class="m2"><p>زبس کز تف وتاب او یافت بهر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فروریخت زاندام شیر ژیان</p></div>
<div class="m2"><p>همان مغفر و درع و ببر بیان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از آنجا بیامد برهنه تنان</p></div>
<div class="m2"><p>به نزدیک یک چشمه آب روان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بیفتاد لرزان برآن سردآب</p></div>
<div class="m2"><p>زگرمی نمانده ورا توش و تاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدان گه که از خشم و کین،پهلوان</p></div>
<div class="m2"><p>زاسب اندرآمد به روشن روان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دوان شد همی بارگی باز جای</p></div>
<div class="m2"><p>بدیدند گردان ستاده به پای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برو بر نگونسار زین پلنگ</p></div>
<div class="m2"><p>سراسر گسسته سلاح و کمند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برآمد خروشی از ایران به زار</p></div>
<div class="m2"><p>سپاهش ببارید خون در کنار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گمان بودشان کان یل تیز چنگ</p></div>
<div class="m2"><p>که با اژدها اندرآمد به تنگ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از او اژدها در گه کارزار</p></div>
<div class="m2"><p>زناگه برآورده باشد دمار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>فغان بزرگان چو پیوسته شد</p></div>
<div class="m2"><p>ز دردش دل هرکسی خسته شد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یکی گفت از آن نامداران شهر</p></div>
<div class="m2"><p>کش از مردی و زیرکی بود بهر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که ای نامداران و کندآوران</p></div>
<div class="m2"><p>زمن بشنوید از کران تا کران</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شما یک زمان مویه کمتر کنید</p></div>
<div class="m2"><p>وزین پایه و کوچه سر برکنید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>که من رفت خواهم بر پهلوان</p></div>
<div class="m2"><p>ببینم من او را به روشن روان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر از چنگ اژدر،جوان کشته شد</p></div>
<div class="m2"><p>سر بخت ما جمله برگشته شد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بیایم شما را دهم آگهی</p></div>
<div class="m2"><p>از آن نیزه کش گرد با فرهی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گر ایدون که یزدان فیروزگر</p></div>
<div class="m2"><p>ببخشیده باشد بدین بوم بر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به تیغ اژدها آن گو نره شیر</p></div>
<div class="m2"><p>زبالا درآورده باشد به زیر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بیایم دهم مژده از کار او</p></div>
<div class="m2"><p>به پیکار و پرخاش و کردار او</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بزرگان بدین گفته خشنو شدند</p></div>
<div class="m2"><p>زرای وی ازمویه یکسو شدند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>جوان دلاور،کمر سخت کرد</p></div>
<div class="m2"><p>سواره شد از ره برآورده گرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>وزآن سو جهانجوی گردن فراز</p></div>
<div class="m2"><p>به آب اندرون بد زمانی دراز</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو تن پاک گشتش برآمد زآب</p></div>
<div class="m2"><p>به جای پرستش بیامد به تاب</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به پیش جهاندار یزدان پاک</p></div>
<div class="m2"><p>بغلطید بسیار در تیره خاک</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همی گفت ای پاک پروردگار</p></div>
<div class="m2"><p>تو دادی مرا بهره از روزگار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سپاس از تو دارم نه از خویشتن</p></div>
<div class="m2"><p>نه از مردی و تیغ و نیروی تن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>فراوان از این گونه زاری نمود</p></div>
<div class="m2"><p>از آن پس ز جا اندرآمد چو دود</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو خورشید بر خاوران زرد گشت</p></div>
<div class="m2"><p>شتابان سوی لشکر آمد زدشت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گرازان همی رفت بر سان مست</p></div>
<div class="m2"><p>به کوپال کرده دل کوه،پست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>سواری دلاور که با فرهی</p></div>
<div class="m2"><p>همی آمد آنجا سوی آگهی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو از دور،مر پهلوان را بدید</p></div>
<div class="m2"><p>خروشی به ایران سپه برکشید</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>فغان کرد کای پر هنر بخردان</p></div>
<div class="m2"><p>سواران گردنکش و موبدان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شمارا به شیر ژیان مژده باد</p></div>
<div class="m2"><p>که از جنگ،برگشته فیروز و شاد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به دل در مگیرید تیمار و غم</p></div>
<div class="m2"><p>روان را مدارید از غم،دژم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ستایش کنان برگرفتند راه</p></div>
<div class="m2"><p>چو آمد به دیدار شاه و سپاه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>همی خواندند آفرین خدای</p></div>
<div class="m2"><p>ابر نامور پهلو نیک رای</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>برو هر کسی کرد گوهر نثار</p></div>
<div class="m2"><p>چو لشکر چو دستور و چون شهریار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چواز آفرین،دل بپرداختند</p></div>
<div class="m2"><p>دلیران همه سر بر افراختند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به رامش نشستند در قیروان</p></div>
<div class="m2"><p>زن و کودک و پیر وجوان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>زبانشان شب وروز پر آفرین</p></div>
<div class="m2"><p>بدان پهلوان زاده پاک دین</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بدیشان نبد یاد تیمار وغم</p></div>
<div class="m2"><p>نماند اندر آن مرز،یک تن دژم</p></div></div>