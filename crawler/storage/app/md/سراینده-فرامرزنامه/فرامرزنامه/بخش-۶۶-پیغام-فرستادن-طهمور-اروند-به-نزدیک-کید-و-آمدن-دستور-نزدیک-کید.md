---
title: >-
    بخش ۶۶ - پیغام فرستادن طهمور اروند به نزدیک کید و آمدن دستور، نزدیک کید
---
# بخش ۶۶ - پیغام فرستادن طهمور اروند به نزدیک کید و آمدن دستور، نزدیک کید

<div class="b" id="bn1"><div class="m1"><p>در این بود دستور کید بزرگ</p></div>
<div class="m2"><p>که آمد فرستاده همچو گرگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنزدیک طهمور اروند شاه</p></div>
<div class="m2"><p>سخن ها بسی گفت زان بارگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که زنهار تا دل نپیچی ز درد</p></div>
<div class="m2"><p>مکن جنگ و پیرامن بد مگرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که خورشید با او نتابد به جنگ</p></div>
<div class="m2"><p>سپهدار،شیر و سپه چون پلنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکشت و ببخشید مان سر به سر</p></div>
<div class="m2"><p>کلاه و قبا داد و زرین کمر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به شادی همه شب بدو می خوریم</p></div>
<div class="m2"><p>ابا بربط و رود و رامشگریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر او را بدین سان ببینی به بزم</p></div>
<div class="m2"><p>بدانی که او با نه خوبست رزم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پیش آی وخواهشگری پیشه کن</p></div>
<div class="m2"><p>وز آن پیش کار وی اندیشه کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل پاکش از بد تهی است و جنگ</p></div>
<div class="m2"><p>خرد دارد و مردی و هوش و سنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر او را چو نوشاد بندی کمر</p></div>
<div class="m2"><p>همت گنج ماند همت تاج وزر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وگر سر بپیچی،در افتی به رنج</p></div>
<div class="m2"><p>نه سرماند و تاج و نه تخت و گنج</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مکن رزم با پهلوان کم ستیز</p></div>
<div class="m2"><p>که در دودمان افتدت رستخیز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکایک چو بشنید کید این پیام</p></div>
<div class="m2"><p>به جا ماند شمشیرش اندر نیام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به خواهشگری مهتران را بخواند</p></div>
<div class="m2"><p>در گنج بگشود و زر برفشاند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرستاد زین گونه برگ بزرگ</p></div>
<div class="m2"><p>به نزدیک آن پهلوان سترک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز دیبا و دینار و خز و حریر</p></div>
<div class="m2"><p>زر و گوهر و مشک و تاج و سریر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به پیش اندرون پاک دستور شاه</p></div>
<div class="m2"><p>بیامد بر پهلوان سپاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زگردن کفن ها درآویخته</p></div>
<div class="m2"><p>زنرگس به گل خون دل ریخته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه دل نهاده به خواهشگری</p></div>
<div class="m2"><p>زسر دور کرده یکایک سری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فرامرز بخشید وبنواختشان</p></div>
<div class="m2"><p>به نزدیک خود جایگه ساختشان</p></div></div>