---
title: >-
    بخش ۲ - آغاز سخن
---
# بخش ۲ - آغاز سخن

<div class="b" id="bn1"><div class="m1"><p>طرح نوی در سخن انداختم</p></div>
<div class="m2"><p>طرح سخن نوع دگر ساختم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر این کوی جز این خانه نیست</p></div>
<div class="m2"><p>رهگذر مردم دیوانه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساخته‌ام من به تمنای خویش</p></div>
<div class="m2"><p>خانه‌ای اندر خور کالای خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ کسم نیست به همسایگی</p></div>
<div class="m2"><p>تا زندم طعنه ز بی‌مایگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بانی مخزن که نهاد آن اساس</p></div>
<div class="m2"><p>مایه او بود برون از قیاس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خانه پر از گنج خداداد داشت</p></div>
<div class="m2"><p>عالمی از گنج خود آباد داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از مدد طبع گهر سنج خویش</p></div>
<div class="m2"><p>مخزنی آراست پی گنج خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود در او گنج فراوان به کار</p></div>
<div class="m2"><p>مخزن سد گنج چه، سد سد هزار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوهر اسرار الاهی در او</p></div>
<div class="m2"><p>آنقدر اسرار که خواهی در او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که به همسایگی او شتافت</p></div>
<div class="m2"><p>غیرت شاهی جگرش را شکافت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شرط ادب نیست که پهلوی شاه</p></div>
<div class="m2"><p>غیر شهان را بود آرامگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من که در گنج طلب می‌زنم</p></div>
<div class="m2"><p>گام در این ره به ادب می‌زنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم ادبم راه به جایی دهد</p></div>
<div class="m2"><p>در طلبم قوت پایی دهد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جهد کنم تا به مقامی رسم</p></div>
<div class="m2"><p>گام نهم پیش و به کامی رسم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کام من اینست که فیاض جود</p></div>
<div class="m2"><p>انجمن آرای بساط وجود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرحمت خویش کند یار من</p></div>
<div class="m2"><p>کم نکند مرحمت از کار من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن که به ما قوت گفتار داد</p></div>
<div class="m2"><p>گنج گهر داد و چه بسیار داد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کرد به ما لطف ز لطف عمیم</p></div>
<div class="m2"><p>نادره گنجی و چه گنج عظیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن که از این گنج نشد بهره‌مند</p></div>
<div class="m2"><p>قیمت این گنج چه داند که چند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دخل جهان گشته مهیا از این</p></div>
<div class="m2"><p>بلکه دو عالم شده پیدا از این</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بود جهان بر سر کوی عدم</p></div>
<div class="m2"><p>بی‌خبر از وضع جهان قدم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه سخن کون و نه ذکر مکان</p></div>
<div class="m2"><p>نه ز هیولا وز صورت نشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نام سما و لقب ارض نه</p></div>
<div class="m2"><p>عمق نه وطول نه و عرض نه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون نه ز ابعاد نشان بود و نام</p></div>
<div class="m2"><p>قابل ابعاد که بود و کدام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>غیر برون بود ز ملک وجود</p></div>
<div class="m2"><p>غیر یکی ذات مقدس نبود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بود یکی ذات و هزاران صفات</p></div>
<div class="m2"><p>واحد مطلق صفتش عین ذات</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زنده باقی احد لایزال</p></div>
<div class="m2"><p>حی توانا صمد ذوالجلال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بیند و گوید نه به چشم و زبان</p></div>
<div class="m2"><p>زو شده موجود هم این و هم آن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن که از او دیده فروزد چراغ</p></div>
<div class="m2"><p>وز مدد باصره دارد فراغ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وان که دهد کام و زبان را بیان</p></div>
<div class="m2"><p>هست چه محتاج به کام و زبان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آنچه نه او بود نمودی نداشت</p></div>
<div class="m2"><p>محض عدم بود و وجودی نداشت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خلوتیان جمله به خواب عدم</p></div>
<div class="m2"><p>در تتق غیب فرو بسته دم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تیره شبی بود، درآن تیره شب</p></div>
<div class="m2"><p>ما همه در خواب فرو بسته لب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شام سیاهی که دو عالم تمام</p></div>
<div class="m2"><p>گم شده بودند در آن تیره شام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>موج برآورد محیط قدم</p></div>
<div class="m2"><p>ابر بقا خاست ز بحر کرم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گشت از آن ابر که شد درفشان</p></div>
<div class="m2"><p>حامله در صدف کن فکان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شعشعهٔ آن گهر شب فروز</p></div>
<div class="m2"><p>کرد شب تار جهان همچو روز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>صبح دل افروز عنایت دمید</p></div>
<div class="m2"><p>باد روان بخش هدایت وزید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کوکبهٔ مهر پدیدار شد</p></div>
<div class="m2"><p>هر دو جهان مطلع انوار شد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از اثر گرمی آن آفتاب</p></div>
<div class="m2"><p>دیده گشودند جهانی ز خواب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عقل جنیبت ز همه تاخت پیش</p></div>
<div class="m2"><p>رایت خویش از همه افراخت پیش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>فوج به فوج از پی هم می‌رسید</p></div>
<div class="m2"><p>خیل و حشم بود که صف می‌کشید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جیش عدم سوی وجود آمدند</p></div>
<div class="m2"><p>بر سر میدان شهود آمدند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تاخت برون لشکری از هر طرف</p></div>
<div class="m2"><p>پیش جهاندند و کشیدند صف</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>لشکر حسن از طرفی در رسید</p></div>
<div class="m2"><p>عشق و سپاهش ز برابر رسید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از طرف حسن برون تاخت ناز</p></div>
<div class="m2"><p>وز طرف عشق در آمد نیاز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>عشق و سپاهی ز کران تا کران</p></div>
<div class="m2"><p>حسن و وفا بود جهان تا جهان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>محنت و درد سپه بی‌شمار</p></div>
<div class="m2"><p>آمد و صف زد ز یمین و یسار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سوز و گداز آمده در قلبگاه</p></div>
<div class="m2"><p>زد علم خویش به قلب سپاه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از صف خود عشق جدا گشت فرد</p></div>
<div class="m2"><p>تاخت به میدان و طلب کرد مرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>پر جگر آن مرد که شد مرد عشق</p></div>
<div class="m2"><p>آمد و نگریخت ز ناورد عشق</p></div></div>