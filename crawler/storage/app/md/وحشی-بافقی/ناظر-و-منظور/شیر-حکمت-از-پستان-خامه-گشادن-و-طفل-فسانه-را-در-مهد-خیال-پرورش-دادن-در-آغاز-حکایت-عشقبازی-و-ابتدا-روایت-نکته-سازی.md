---
title: >-
    شیر حکمت از پستان خامه گشادن و طفل فسانه را در مهد خیال پرورش دادن در آغاز حکایت عشقبازی و ابتداء روایت نکته سازی
---
# شیر حکمت از پستان خامه گشادن و طفل فسانه را در مهد خیال پرورش دادن در آغاز حکایت عشقبازی و ابتداء روایت نکته سازی

<div class="b" id="bn1"><div class="m1"><p>نوا پرداز قانون فصاحت</p></div>
<div class="m2"><p>چنین زد چنگ بر تار حکایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بود اقلیم چین را شهریاری</p></div>
<div class="m2"><p>به تخت شهریاری کامکاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تاج نامداری سربلندی</p></div>
<div class="m2"><p>به زنجیر عدالت ظلم بندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چین در دور عدل آن جهاندار</p></div>
<div class="m2"><p>نبود آشفته‌ای جز طره یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جز چشم نکویان در سوادی</p></div>
<div class="m2"><p>به دورش کس نداد از فتنه یادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز عدلش هم‌سرا گنجشک با مار</p></div>
<div class="m2"><p>به دورش چرغ آهو را هوادار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظر چون بر رخش دوران گشاده</p></div>
<div class="m2"><p>نظر نام شه دوران نهاده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وزیری بود بس عالی مقامش</p></div>
<div class="m2"><p>نظیر از مادر ایام نامش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حصار ملک رای محکم او</p></div>
<div class="m2"><p>بهار عدل روی خرم او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از آن چیزی که بر دل بندشان بود</p></div>
<div class="m2"><p>همین نومیدی فرزندشان بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پی صیدافکنی یک روز دلتنگ</p></div>
<div class="m2"><p>وزیر و شه برون راندند شبرنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وزیر و پادشاه و خادمی چند</p></div>
<div class="m2"><p>ز دیگر لشکری بگسسته پیوند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از آنجا روی در صحرا نهادند</p></div>
<div class="m2"><p>بسان سیل در صحرا فتادند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به زیر ران هر یک تیز گامی</p></div>
<div class="m2"><p>سمند بادپایی، خوشخرامی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شدندی صد بیابان بیش در پیش</p></div>
<div class="m2"><p>به تندی از صدای سینه خویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زد آتش گرمی خور در جگرشان</p></div>
<div class="m2"><p>یکی ویرانه آمد در نظرشان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دوانی سوی آن ویرانه راندند</p></div>
<div class="m2"><p>به سرعت خویش را آنجا رساندند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در او دیدند پیری با صفایی</p></div>
<div class="m2"><p>ز عالم نور او ظلمت زدایی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زبان او کلید گنج عرفان</p></div>
<div class="m2"><p>بسان گنج در ویرانه پنهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر در دل گذشتی طیلسانش</p></div>
<div class="m2"><p>فلک در پا فکندی کهکشانش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>محیط معرفت دل در بر او</p></div>
<div class="m2"><p>کف دریای دین موی سر او</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به قدی چون کمان در چله دایم</p></div>
<div class="m2"><p>بنای گوشه گیری کرده قایم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو رخ بنمود آن پیر فتاده</p></div>
<div class="m2"><p>ز اسب خویشتن شه شد پیاده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شه و دستور در پایش فتادند</p></div>
<div class="m2"><p>نقاب از روی راز خود گشادند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به و ناری برون آورد درویش</p></div>
<div class="m2"><p>از آنها داشت هر یک را یکی پیش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نظر زان نار خرم گشت بسیار</p></div>
<div class="m2"><p>که روشن دید شمع بخت از آن نار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پس آنگه داد ایشان را بشارت</p></div>
<div class="m2"><p>که بر چیزیست آن هر یک اشارت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وزیر از به بسی چون نار خندید</p></div>
<div class="m2"><p>که درد خویشتن را زان بهی دید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به خسرو مژدهٔ آن می‌دهد نار</p></div>
<div class="m2"><p>که گردد گلبن بختش گران یار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به تخت دور در کم روزگاری</p></div>
<div class="m2"><p>از و سر بر فرازد تاجداری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خدا بخشد به دستور خداوند</p></div>
<div class="m2"><p>در این گلزار یک نخل برومند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ولی باشد چو به با چهره زرد</p></div>
<div class="m2"><p>ز آه عاشقی رخسار پر گرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دل دستور خرم بود از آن به</p></div>
<div class="m2"><p>که دردش می‌شود گویا از آن به</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ولی در نار حرف پیرش انداخت</p></div>
<div class="m2"><p>چو شمع از بار غم دلگیرش انداخت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بلی بوی بهی نبود در آن باغ</p></div>
<div class="m2"><p>ز نارش نیست یک دل خالی از داغ</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در این گلشن که خندان گشت چون نار</p></div>
<div class="m2"><p>که چشم از خون نگشتش ناردان بار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به نزدیکش دمی چون آرمیدند</p></div>
<div class="m2"><p>دعا گویان از او دوری گزیدند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سوی بستانسرای خویش راندند</p></div>
<div class="m2"><p>برای میوه نخل نو نشاندند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از آن مدت چو شد نه ماه و نه روز</p></div>
<div class="m2"><p>شبی سرزد و مهر عالم افروز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>وزیر و شاه را زان مژده دادند</p></div>
<div class="m2"><p>ز گنج سیم قفل زر گشادند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چنان دادند سیم و زر به مردم</p></div>
<div class="m2"><p>که در زیر غنیمت شد جهان گم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نظر از خرمی سوی پسر تاخت</p></div>
<div class="m2"><p>رخ فرزند را مد نظر ساخت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنین فرمود شاه نیک فرجام</p></div>
<div class="m2"><p>که منظورش کنند اهل نظر نام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به دستوری که باشد رفت دستور</p></div>
<div class="m2"><p>نظر را گوهر خود داشت منظور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>که فرمان شه روی زمین چیست</p></div>
<div class="m2"><p>بفرماید شهنشه نام این چیست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو پر می‌دید سوی شاه ایام</p></div>
<div class="m2"><p>نظر فرمود ناظر باشدش نام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به سوی هر یکی یک دایه بردند</p></div>
<div class="m2"><p>به دست دایه ایشان را سپردند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز هجر آن لبان روح پرور</p></div>
<div class="m2"><p>چو ماتم دار شد پستان مادر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به رسم مادری بنهاد دوران</p></div>
<div class="m2"><p>دهانشان را بجای شیر دندان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به ملک حسن چون از ده گذشتند</p></div>
<div class="m2"><p>ز ماه چارده صد ره گذشتند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به خوبی شد چنان شهزاده منظور</p></div>
<div class="m2"><p>که در عالم چو خور گردیده مشهور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>قدش سروی ز بستان نکویی</p></div>
<div class="m2"><p>گل رویش ز باغ تازه رویی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>پی مرغ دل هر هوشیاری</p></div>
<div class="m2"><p>ز کاکل بر سر آن سرو ماری</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دل کس با وجود هوشیاری</p></div>
<div class="m2"><p>نبردی جان از او با رستگاری</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>فکنده فتنهٔ او در جهان شور</p></div>
<div class="m2"><p>مدامش نرگس بیمار مخمور</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>صف مژگان او کز هم گذشته</p></div>
<div class="m2"><p>کمینگاه هزاران فتنه گشته</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>پی خون خوردن عشاق جانباز</p></div>
<div class="m2"><p>دو لعل او دو خونی گشته همراز</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در دندان او در خنده تا دید</p></div>
<div class="m2"><p>دل گوهر ز غم سوراخ گردید</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گهر کو دست پرورد صدف بود</p></div>
<div class="m2"><p>بدان دندان کیش لاف شرف بود</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زنخدانش بر آن رخسار دلکش</p></div>
<div class="m2"><p>معلق کرده آبی را در آتش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز زر بر گردنش طوقی فتاده</p></div>
<div class="m2"><p>به گنج سیم ماری تکیه داده</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بری از سیم خام آن نخل تر داشت</p></div>
<div class="m2"><p>عجب نخلی که سیم خام برداشت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>جهانی بسته بود از شوق هر سو</p></div>
<div class="m2"><p>چو بازو بند دل در بازوی او</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>فروغ ساعدش از آستینها</p></div>
<div class="m2"><p>چو نور شمع از فانوس پیدا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به خوبی داد آن خورشید پایه</p></div>
<div class="m2"><p>ز سیم دست سیمین دست مایه</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کمر پیچید عمری بر میانش</p></div>
<div class="m2"><p>نگشته آگه از سر نهانش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دلا در فکر آن موی میان پیچ</p></div>
<div class="m2"><p>طلب کن فکر باریکی در آن پیچ</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>مگر حرف از میان آن فزون‌تر</p></div>
<div class="m2"><p>حکایت در میان بگذار و بگذر</p></div></div>