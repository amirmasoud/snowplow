---
title: >-
    رسیدن آن گل نودمیدهٔ چمن رعنایی و سرو تازه رسیدهٔ گلشن زیبایی به مرغزاری که پنجهٔ چنارش شاخ بیداد شکستی و آفتاب بلند پایه در  سایه بیدش نشستی
---
# رسیدن آن گل نودمیدهٔ چمن رعنایی و سرو تازه رسیدهٔ گلشن زیبایی به مرغزاری که پنجهٔ چنارش شاخ بیداد شکستی و آفتاب بلند پایه در  سایه بیدش نشستی

<div class="b" id="bn1"><div class="m1"><p>سمند ره نورد این بیانان</p></div>
<div class="m2"><p>بزد راه سخن زینسان به پایان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چون منظور دور از لشکری گشت</p></div>
<div class="m2"><p>خروشان همچو سیل افتاد در دشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دل می‌کرد آه سرد و می‌رفت</p></div>
<div class="m2"><p>دو منزل را یکی می‌کرد و می‌رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسان همزبان را یاد می‌کرد</p></div>
<div class="m2"><p>ز درد بی‌کسی فریاد می‌کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش آن بیکس که صحرایی گزیند</p></div>
<div class="m2"><p>که غیر از سایه همپایی نبیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کند چندان فغان از جان ناشاد</p></div>
<div class="m2"><p>که آید آه از افغانش به فریاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نماند در مقام خسته حالی</p></div>
<div class="m2"><p>دل پر سازد از فریاد خالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیا وحشی که عنقایی گزینیم</p></div>
<div class="m2"><p>وطن در قاف تنهایی گزینیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو مه با خور بود نقصان پذیر است</p></div>
<div class="m2"><p>می از تنها نشستن شیر گیر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز تنهاییست می را در فرح روی</p></div>
<div class="m2"><p>چو یارش پشه شد گردد ترش روی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو سرکه همسرای پشه افتاد</p></div>
<div class="m2"><p>نیاید از سرایش غیر فریاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو زر با نقره یکچندی نشیند</p></div>
<div class="m2"><p>دگر خود را به رنگ خود نبیند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مشو دمساز با کس تا توانی</p></div>
<div class="m2"><p>اگر می‌بایدت روشن روانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو آیینه که با هرکس مقابل</p></div>
<div class="m2"><p>ز تأثیر نفس گردد سیه دل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو روزی چند شد القصه منظور</p></div>
<div class="m2"><p>به چشمش مرغزاری آمد از دور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو شد نزدیک جای خرمی دید</p></div>
<div class="m2"><p>عجب آب و هوای بی‌غمی دید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در او هر سو چکاوک خانه کرده</p></div>
<div class="m2"><p>چو هدهد کاکل خود شانه کرده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز جا برجسته طفل سبزه از باد</p></div>
<div class="m2"><p>به آهو نیزه بازی کرده بنیاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز زخم خار گلها را تکسر</p></div>
<div class="m2"><p>ز زخم سنگ مشت یاسمین پر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گشودی ماهیش مقراض از دم</p></div>
<div class="m2"><p>به قصد آب می‌بردید قاقم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بیان می‌کرد هر سو غنچه با گل</p></div>
<div class="m2"><p>به سر گوشی حدیث خون بلبل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>میان سبزه آب افتاده بیهوش</p></div>
<div class="m2"><p>کشیده سبزه تنگ او را در آغوش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پی راحت فرود آمد ز شبرنگ</p></div>
<div class="m2"><p>به طرف سبزه‌زاری کرد آهنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به آسایش به روی سبزه افتاد</p></div>
<div class="m2"><p>سمند خویش را سر در چرا داد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فتادی همچو گل از دست بر دست</p></div>
<div class="m2"><p>که شد در خواب نازش نرگس مست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو مست خواب شد آن مایه ناز</p></div>
<div class="m2"><p>سمندش ناگه آمد در تک و تاز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز آواز سم اسب رمیده</p></div>
<div class="m2"><p>ز جا جست و گشود از خواب دیده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نظر چون کرد شیری دید از دور</p></div>
<div class="m2"><p>در و دشت از غریوش گشته پر شور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز چنبر شیر گردون را جهانده</p></div>
<div class="m2"><p>نشان ناخنش بر ثور مانده</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خروشش مرده را بردی ز سر خواب</p></div>
<div class="m2"><p>به زهر چشم کردی زهره‌ها آب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پی جستن زدی چون بر زمین پای</p></div>
<div class="m2"><p>نمودی کوههٔ گاو زمین جای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کشید آن شیردل بر شیرشمشیر</p></div>
<div class="m2"><p>چو شیری حمله آور گشت بر شیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هژبر تیغ زن تیغ آنچنان راند</p></div>
<div class="m2"><p>که زخم تیغ بر گاو زمین ماند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جدا کرد آن بلا را از سر خویش</p></div>
<div class="m2"><p>نمود از سبزه و گل بستر خویش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به روی سبزه می‌غلطید چون آب</p></div>
<div class="m2"><p>که شد بر روی گل آهوش در خواب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سفر سازندهٔ شهر فسانه</p></div>
<div class="m2"><p>زند بر رخش زینسان تازیانه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که چون منظورگشت از خواب بیدار</p></div>
<div class="m2"><p>برآمد بر سمند باد رفتار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو بیرون شد از آن دلکش نشیمن</p></div>
<div class="m2"><p>به روی پشته‌ای برراند توسن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نظر چون کرد شهری در نظر دید</p></div>
<div class="m2"><p>سوادش از نظر پر نورتر دید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>حصار او زدی بر چرخ پهلو</p></div>
<div class="m2"><p>کواکب سنگها بر کنگر او</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>حصارش زلف زهره شانه کرده</p></div>
<div class="m2"><p>ز کنگر شانه را دندانه کرده</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کشیده خندقش از غرب تا شرق</p></div>
<div class="m2"><p>در آب خندقش چوب فلک غرق</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سواد شهر کردش دیده پرنور</p></div>
<div class="m2"><p>چو گل از خرمی بشکفت منظور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز روی خرمی میراند توسن</p></div>
<div class="m2"><p>که تا گشتش در دروازه روشن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بر او دروازه‌بان چون دیده بگشاد</p></div>
<div class="m2"><p>به پای توسنش چون سایه افتاد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بگفتا کای جوان نورسیده</p></div>
<div class="m2"><p>که از مهرت به ما پرتو رسیده</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چسان جان برده‌ای زین بیشه بیرون</p></div>
<div class="m2"><p>که شیرش بسته ره بر گاو گردون</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کنون عمریست تا این راه بسته</p></div>
<div class="m2"><p>به راه رهروان از کین نشسته</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز نیش خویش شیر این گذرگاه</p></div>
<div class="m2"><p>نهاده رهروان را خار در راه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ازو این حرف چون منظور بشنید</p></div>
<div class="m2"><p>ز کار رفته گوهر بار گردید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بر او پیر از تعجب دیده بگشاد</p></div>
<div class="m2"><p>به منزلگاه خویشش برد و جا داد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو دید آن گنج در ویرانهٔ خویش</p></div>
<div class="m2"><p>به پیش آورد درویشانهٔ خویش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>پس آنگه رفت سوی درگه شاه</p></div>
<div class="m2"><p>بگفت این حال با خاصان درگاه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ازو چون شرح این معنی شنفتند</p></div>
<div class="m2"><p>به خسرو صورت احوال گفتند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>زد از روی تعجب دست بر دست</p></div>
<div class="m2"><p>که یک تن چون ز دست این بلا رست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به جمعی داد خلعت‌ها و فرمود</p></div>
<div class="m2"><p>که باتشریف تشریف آورد زود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سوی منظور از آنجا رو نهادند</p></div>
<div class="m2"><p>زمین از دور پیشش بوسه دادند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>پی تعظیم تشریف از زمین خاست</p></div>
<div class="m2"><p>بدن از خلعت شاهانه آراست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به آنها گشت همره بی‌توقف</p></div>
<div class="m2"><p>سوی بازار مصر آمد چو یوسف</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ازو دل داده خلقی از کف خویش</p></div>
<div class="m2"><p>هجوم بی‌دلانش از پس و پیش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>فتاده پیش و خلقی گشته پیرو</p></div>
<div class="m2"><p>چنین می‌رفت تا درگاه خسرو</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بیاوردند نزدیکان درگاه</p></div>
<div class="m2"><p>به تعظیم تمامش جانب شاه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>زمین بوسید آنطوری که شاید</p></div>
<div class="m2"><p>دعایش کرد آن نوعی که باید</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به میدان سخن افکند گویی</p></div>
<div class="m2"><p>ز هر جا کرد با او گفتگویی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو از هر بحث گوهر بار گردید</p></div>
<div class="m2"><p>به تقریبی حدیث شیر پرسید</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>زمین بوسید منظور ادب کیش</p></div>
<div class="m2"><p>به خسرو گفت یک یک قصه خویش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چنین در بزم شه تا شام جا کرد</p></div>
<div class="m2"><p>سخن از هر دری با شه ادا کرد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شهنشه گفت تا کردند تعیین</p></div>
<div class="m2"><p>مقامی از پی شهزادهٔ چین</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>پی رفتن زمین بوسید منظور</p></div>
<div class="m2"><p>به دستوری ز بزم شاه شد دور</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو جست از مجلس خسرو کرانه</p></div>
<div class="m2"><p>ببردندش به بزم خسروانه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به روی نیم تختی جاش دادند</p></div>
<div class="m2"><p>به مجلس نقل خوشحالی نهادند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چو پاسی از شب دیجور بگذشت</p></div>
<div class="m2"><p>سپاه خواب بر منظور بگذشت</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>برای پاس آن پاکیزه گوهر</p></div>
<div class="m2"><p>گروهی حلقهٔ سان ماندند بر در</p></div></div>