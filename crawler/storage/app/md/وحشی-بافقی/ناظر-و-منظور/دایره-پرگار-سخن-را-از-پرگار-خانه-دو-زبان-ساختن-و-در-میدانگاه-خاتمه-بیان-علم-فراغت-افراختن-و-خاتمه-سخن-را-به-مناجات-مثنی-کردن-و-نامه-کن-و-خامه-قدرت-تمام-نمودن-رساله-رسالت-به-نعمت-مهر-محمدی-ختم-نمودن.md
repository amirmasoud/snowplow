---
title: >-
    دایرهٔ پرگار سخن را از پرگار خانهٔ دو زبان ساختن و در میدانگاه خاتمهٔ بیان علم فراغت افراختن و خاتمه سخن را به مناجات مثنی کردن و نامهٔ کن و خامهٔ قدرت تمام نمودن رسالهٔ رسالت به نعمت مهر محمدی ختم نمودن
---
# دایرهٔ پرگار سخن را از پرگار خانهٔ دو زبان ساختن و در میدانگاه خاتمهٔ بیان علم فراغت افراختن و خاتمه سخن را به مناجات مثنی کردن و نامهٔ کن و خامهٔ قدرت تمام نمودن رسالهٔ رسالت به نعمت مهر محمدی ختم نمودن

<div class="b" id="bn1"><div class="m1"><p>بحمدالله که گر دیدیم رنجی</p></div>
<div class="m2"><p>در آخر یافتیم این طور گنجی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در او ناسفته گوهرها نهاده</p></div>
<div class="m2"><p>طلسمش تا به اکنون ناگشاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نام ایزد چه گنج شایگانی</p></div>
<div class="m2"><p>کز او گردید پر جوهر جهانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگو آسان طلسمش را گشادم</p></div>
<div class="m2"><p>که پر جانی در این اندیشه دادم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دشواری چنین گنجی توان یافت</p></div>
<div class="m2"><p>بلی کی گنج بی‌رنجی توان یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دماغم تیره شد چون خامه بسیار</p></div>
<div class="m2"><p>که تا کردم رقم این نقش پرگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز مو اندیشه را کردم قلم ساز</p></div>
<div class="m2"><p>شدم این لعبتان را چهره پرداز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسی همچون بخورم سوخت ایام</p></div>
<div class="m2"><p>که تا گشتند این روحانیان رام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سحر خیزی بسی کردم چو خورشید</p></div>
<div class="m2"><p>که زر گردید خاک راه امید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بوته پر فرو رفتم به آتش</p></div>
<div class="m2"><p>که آخر این طلا گردید بی‌غش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که مشتی خاک ره گر برگرفتم</p></div>
<div class="m2"><p>روانش در لباس زر گرفتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگر شد خاطر من مهر جان تاب</p></div>
<div class="m2"><p>کزو گردید خاک ره زر ناب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برون آورده‌ام از کان امید</p></div>
<div class="m2"><p>زر لایق به زیب تاج خورشید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنین بی‌غش زری از کان برآید</p></div>
<div class="m2"><p>چه کان کز مادر امکان بزاید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در این معدن که زر سیماب گردید</p></div>
<div class="m2"><p>بسان کیمیا نایاب گردید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پریشانی بسی دیدم چو سیماب</p></div>
<div class="m2"><p>که تا شد جمع این مشتی زر ناب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زر نابم ز کان دیگری نیست</p></div>
<div class="m2"><p>بدین در هم نشان دیگری نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز هر آلایشی دل پاک کردم</p></div>
<div class="m2"><p>گذر بر حجلهٔ افلاک کردم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که این بکران معنی رو نمودند</p></div>
<div class="m2"><p>نقاب غیب از طلعت گشودند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سخن کاو بکر خلوتگاه غیب است</p></div>
<div class="m2"><p>نهان گردیده در خرگاه عیب است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به هر آلوده‌ای کی رو نماید</p></div>
<div class="m2"><p>نقاب غیب کی از رو گشاید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کسی کاین نظم دور اندیشه خواند</p></div>
<div class="m2"><p>اگر تاریخ تصنیفش نداند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شمارد پنج نوبت سی به تضعیف</p></div>
<div class="m2"><p>که با شش باشدش تاریخ تصنیف</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نداند گر به این قانون که شد فکر</p></div>
<div class="m2"><p>بجوید از همه ابیات پر فکر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گزیدم گر طریق خود ستایی</p></div>
<div class="m2"><p>بیان کردم سخنهای هوایی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بنا بر سنت اهل سخن بود</p></div>
<div class="m2"><p>و گر نه این سخن کی حد من بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کسی کاین نظم بی‌مقدار خواند</p></div>
<div class="m2"><p>ز سد بیت ار یکی پرکار داند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز عیب آن دگرها دیده دوزد</p></div>
<div class="m2"><p>چراغ وصف این را برفروزد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه رسم عیب جویی پیشه سازد</p></div>
<div class="m2"><p>حیات خود در این اندیشه بازد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همان به کاین حکایتها نگویم</p></div>
<div class="m2"><p>که باشم من که باشد عیب جویم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خدایا پرده‌ای بر عیب من کش</p></div>
<div class="m2"><p>زبان حرف گیران در دهن کش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کلامم را بده آن حالت خاص</p></div>
<div class="m2"><p>کزو گردند اهل حال رقاص</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بنه مهری بر این قلب زر اندود</p></div>
<div class="m2"><p>که در ملک جهان رایج شود زود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به این زیبا عروس نورسیده</p></div>
<div class="m2"><p>که از نو پرده از طلعت کشیده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بده بختی که عالمگیر گردد</p></div>
<div class="m2"><p>نه از بی‌طالعیها پیر گردد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در ناسفتهٔ این گنج معنی</p></div>
<div class="m2"><p>که در معنی ندارد رنج دعوی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز دست خائنانش در امان دار</p></div>
<div class="m2"><p>به ملک حفظ خویشش جاودان دار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>قبول خاص و عامش ساز یارب</p></div>
<div class="m2"><p>به خاطرها مقامش ساز یارب</p></div></div>