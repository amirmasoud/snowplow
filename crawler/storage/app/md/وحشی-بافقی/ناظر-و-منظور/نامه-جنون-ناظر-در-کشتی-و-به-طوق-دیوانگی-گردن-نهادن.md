---
title: >-
    نامه جنون ناظر در کشتی و به طوق دیوانگی گردن نهادن
---
# نامه جنون ناظر در کشتی و به طوق دیوانگی گردن نهادن

<div class="b" id="bn1"><div class="m1"><p>سلاسل ساز این فرخنده تحریر</p></div>
<div class="m2"><p>کشد زینگونه مطلب را به زنجیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که ناظر داشت در کشتی نشیمن</p></div>
<div class="m2"><p>ز ابر دیده دریا کرد دامن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شدی هر روز افزون شوق یارش</p></div>
<div class="m2"><p>که آخر با جنون افتاد کارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریبان می‌درید و آه می‌زد</p></div>
<div class="m2"><p>ز آه آتش به مهر و ماه می‌زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آتش یافتی بیتاب خود را</p></div>
<div class="m2"><p>دویدی کافکند در آب خود را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو همراهان ازو این حال دیدند</p></div>
<div class="m2"><p>در آن کشتی به زنجیرش کشیدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به زنجیر جنون چون گشت پا بست</p></div>
<div class="m2"><p>سری بر زانوی اندوه بنشست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو آیین جنونش برد از کار</p></div>
<div class="m2"><p>به زنجیر از جنون آمد به گفتار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که ای چون زلف خوبان دلارا</p></div>
<div class="m2"><p>اسیر حلقه‌هایت اهل سودا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسی منت بگردن از تو دارم</p></div>
<div class="m2"><p>که یادم می‌دهی از زلف یارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منم در راه تو از پا فتاده</p></div>
<div class="m2"><p>به طوق خدمتت گردن نهاده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تویی سر رشتهٔ هر عیش و شادی</p></div>
<div class="m2"><p>عجب نیکو به پای من فتادی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم آوازی کنی از روی یاری</p></div>
<div class="m2"><p>مرا شبها به کنج بیقراری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز قید عقل از یمن تو رستم</p></div>
<div class="m2"><p>عجب سررشته ای دادی به دستم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نزد مار غمی برسینه‌ات نیش</p></div>
<div class="m2"><p>چرا پیچی بسان مار برخویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا بر سینه روزنها از آنست</p></div>
<div class="m2"><p>که جسم ناوک غم را نشانست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ترا در سینه این سوراخها چیست</p></div>
<div class="m2"><p>وجودت زخمدار ناوک کیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا چشمی‌ست زان هر دم به راهی</p></div>
<div class="m2"><p>که دارم انتظار وصل ماهی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نمی‌دانم تو باری در چه کاری</p></div>
<div class="m2"><p>که بر ره حلقه‌های دیده داری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>درین زندان نه‌ای دیوانه چون من</p></div>
<div class="m2"><p>بگو کز چیست این طوقت به گردن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه طوق است این رکاب رخش خواریست</p></div>
<div class="m2"><p>گریبان لباس بیقراریست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لب چاه مصیبت را نشانیست</p></div>
<div class="m2"><p>برای حرف نومیدی دهانیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فغان کاین طوق پامال غمم ساخت</p></div>
<div class="m2"><p>عجب کاری مرا در گردن انداخت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>منم زین طوق چون قمری فغان ساز</p></div>
<div class="m2"><p>به یاد قدت ای سرو سرافراز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بیا ای کاکلت زنجیر سودا</p></div>
<div class="m2"><p>که زنجیر غمم انداخت از پا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به زنجیر غمم پامال مگذار</p></div>
<div class="m2"><p>بیا وز پایم این زنجیر بردار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز هجر آن خم زلف گره گیر</p></div>
<div class="m2"><p>ندارم دستگیری غیر زنجیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به کنج بیکسی اینگونه دربند</p></div>
<div class="m2"><p>به کارم سد گرده زنجیر مانند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو زنجیرم بود گر سد دهن بیش</p></div>
<div class="m2"><p>بیان نتوان نمودن یک غم خویش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به غیر از کنج غم جایی ندارم</p></div>
<div class="m2"><p>بجز زنجیر همپایی ندارم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرا کاین است همپا چون نیفتم</p></div>
<div class="m2"><p>ز اشک خویش چون در خون نیفتم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز دل برمی‌کشید آه از سردرد</p></div>
<div class="m2"><p>چنین تا بر کنار نیل جا کرد</p></div></div>