---
title: >-
    پایهٔ سریر معانی بر عرش نهادن و گام فکر در عرصهٔ سپهر گشادن در مدح شهسواری که فضای هستی گویی از اقلیم اوست
---
# پایهٔ سریر معانی بر عرش نهادن و گام فکر در عرصهٔ سپهر گشادن در مدح شهسواری که فضای هستی گویی از اقلیم اوست

<div class="b" id="bn1"><div class="m1"><p>چو این گنج هنر ترتیب دادم</p></div>
<div class="m2"><p>ز هر جوهر در او درجی نهادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شدم جویندهٔ زیبنده اسمی</p></div>
<div class="m2"><p>که حفظ گنج را سازم طلسمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کام فکر ملکی چند گشتم</p></div>
<div class="m2"><p>به اکثر نامداران بر گذشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ناگه پیشم آمد پیر دانش</p></div>
<div class="m2"><p>که ای کار تو بر تدبیر و دانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نام نامداری شد گهر سنج</p></div>
<div class="m2"><p>که تیغش ملک را ماریست بر گنج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شه انجم سپاه آسمان تخت</p></div>
<div class="m2"><p>جهانگیر و جهاندار و جوانبخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهالی از گلستان پیمبر</p></div>
<div class="m2"><p>گلی از بوستان باغ حیدر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بر او رنگ دارایی نهد گام</p></div>
<div class="m2"><p>شود آیین اطلس بخشش عام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل خورشید لرزد بر سر خاک</p></div>
<div class="m2"><p>که بخشد ناگهان دیبای افلاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صدف آبستن از ابر سخایش</p></div>
<div class="m2"><p>گهر بی‌قیمت از دست عطایش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دارالضرب احسان چون قدم زد</p></div>
<div class="m2"><p>کرم را سکه نو بر درم زد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر زین بیشتر در کشور جود</p></div>
<div class="m2"><p>کرم زا نام حاتم بر درم بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سرانگشت سخا ز آنگونه افشرد</p></div>
<div class="m2"><p>که نقش نام حاتم را از آن برد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به تخت خسروی چون کرد آهنگ</p></div>
<div class="m2"><p>به قانون عدالت زد چنان چنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که در بزم جهان از شاه درویش</p></div>
<div class="m2"><p>بجز نی نیست کس را باد در خویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنان دورش به صحبت خانهٔ داد</p></div>
<div class="m2"><p>ز امنیت صلای عیش در داد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به دور او که ناامنی‌ست محبوس</p></div>
<div class="m2"><p>مگر یکباره راه جنگ زد کوس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که می‌پیچند سر تا پا کمندش</p></div>
<div class="m2"><p>به نوبت چوب بر سر می‌زنندش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از آنرو زخمهٔ مطرب خورد چنگ</p></div>
<div class="m2"><p>که مانند است نام چنگ با چنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو معموری ده ملک جهان شد</p></div>
<div class="m2"><p>جهان از گنج آسایش جنان شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که جای خشت زن بزم شراب است</p></div>
<div class="m2"><p>به جای قالب خشتش رباب است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کشد چون آتش خشمش زبانه</p></div>
<div class="m2"><p>برآرد دود از چشم زمانه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به روز جنگ چون بر پشت شبرنگ</p></div>
<div class="m2"><p>کند او عزم میدان تیغ در چنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز هر جانب برآید نعره کوس</p></div>
<div class="m2"><p>دهد سوفار ناوک جمله را بوس</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نفیر سرکشان افتد به عالم</p></div>
<div class="m2"><p>خورد مرغ حیات بیدلان رم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دلیران را به خون گلگون تبر زین</p></div>
<div class="m2"><p>پلنگی چند ناخن کرده خونین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پی پرواز مرغ روح لشکر</p></div>
<div class="m2"><p>ز هر جانب شود شمشیر شهپر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برآرد تیغ چون مهر جهانسوز</p></div>
<div class="m2"><p>شود در عرصهٔ کین آتش افروز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گهی بر غرب راند گاه بر شرق</p></div>
<div class="m2"><p>به شرق و غرب از تیغش جهد برق</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گریزد لشکر خصم از صف کین</p></div>
<div class="m2"><p>بدانسان کز شهب خیل شیاطین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زهی کشور گشا دارای دوران</p></div>
<div class="m2"><p>جهانگیر و جهاندار و جهانبان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تویی آن آفتاب عرش پایه</p></div>
<div class="m2"><p>که افتد چرخ در پایت چو سایه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ترا هر کس به قدر رتبهٔ خویش</p></div>
<div class="m2"><p>پی ایثار چیزی آورد پیش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کشیدم پیش منهم گوهری چند</p></div>
<div class="m2"><p>ز درج طبع رخشان جوهری چند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو آن دانا دل گوهر شناسی</p></div>
<div class="m2"><p>که نیکو گوهر از گوهر شناسی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نیم از قسم هر گوهر فروشی</p></div>
<div class="m2"><p>به سوی گوهر من دار گوشی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چه می‌گویم چه گوهر چند مهره</p></div>
<div class="m2"><p>به شهر بی‌وجودی گشته شهره</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه آن مقدارها چیزیست دلکش</p></div>
<div class="m2"><p>که افتد طبع دانا را به آن خوش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز سد بیت ار فتد یک بیت پرکار</p></div>
<div class="m2"><p>ز طبع من بود آن نیز بسیار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>الاهی تا در این میدان انبوه</p></div>
<div class="m2"><p>کشد خورشید خنجر بر سرکوه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کسی کاو هست کینت در نهادش</p></div>
<div class="m2"><p>اگر کوه است بر سر تیغ بادش</p></div></div>