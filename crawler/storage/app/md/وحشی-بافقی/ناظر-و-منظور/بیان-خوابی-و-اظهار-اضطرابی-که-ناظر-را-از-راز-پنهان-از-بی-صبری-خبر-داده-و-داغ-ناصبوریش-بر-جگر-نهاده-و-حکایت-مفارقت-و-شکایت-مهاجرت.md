---
title: >-
    بیان خوابی و اظهار اضطرابی که ناظر را از راز پنهان از بی‌صبری خبر داده و داغ ناصبوریش بر جگر نهاده و حکایت مفارقت و شکایت مهاجرت
---
# بیان خوابی و اظهار اضطرابی که ناظر را از راز پنهان از بی‌صبری خبر داده و داغ ناصبوریش بر جگر نهاده و حکایت مفارقت و شکایت مهاجرت

<div class="b" id="bn1"><div class="m1"><p>چنین گفت آن ادیب نکته پرداز</p></div>
<div class="m2"><p>که درس عاشقی می‌کرد آغاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که منظور از وفا چون گل شکفتی</p></div>
<div class="m2"><p>حکایتهای مهر آمیز گفتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نوشین لعل آن شوخ شکر خند</p></div>
<div class="m2"><p>دل مسکین ناظر ماند در بند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حدیث خوش‌ادا گلزار یاریست</p></div>
<div class="m2"><p>نهال بوستان دوستاریست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حدیث ناخوش از اهل مودت</p></div>
<div class="m2"><p>به پای دل نشاند خار نفرت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسا یاران که بودی این گمانشان</p></div>
<div class="m2"><p>که بی هم صبر نبود یک زمانشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به حرف ناخوشی کز هم شنیدند</p></div>
<div class="m2"><p>چنان پا از ره یاری کشیدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که مدتها برآمد زان فسانه</p></div>
<div class="m2"><p>نشد پیدا صفایی در میانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش آن صحبت که در آغاز یاریست</p></div>
<div class="m2"><p>در او سد گونه لطف و دوستداریست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کمال لطف جانان آن مجال است</p></div>
<div class="m2"><p>که روز اول بزم وصال است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسا لطفی که من از یار دیدم</p></div>
<div class="m2"><p>به ذوق بزم اول کم رسیدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به عیش بزم اول حالتی هست</p></div>
<div class="m2"><p>که حالی آن چنان کم می‌دهد دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو گویی عیش عالم وام کردند</p></div>
<div class="m2"><p>نخستین بزم وصلش نام کردند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به عاشق لطف معشوق است بسیار</p></div>
<div class="m2"><p>ولی چندان که شد عاشق گرفتار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بلی صیاد چندان دانه ریزد</p></div>
<div class="m2"><p>که مرغ از صیدگاهی برنخیزد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو گردد مرغ اندک چاشنی خوار</p></div>
<div class="m2"><p>بود در سلک مرغان گرفتار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه خوش می‌گفت در کنج خرابات</p></div>
<div class="m2"><p>به دختر شاهدی شیرین حکایات</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر خواهی که با جور تو سازند</p></div>
<div class="m2"><p>حیات خویش در جور تو بازند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به آغاز محبت در وفا کوش</p></div>
<div class="m2"><p>وفا کن تا بری زاهل وفا هوش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بنای مهر چون شد سخت بنیاد</p></div>
<div class="m2"><p>تو خواهی لطف میکن خواه بیداد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو شمعی را که میداری به آتش</p></div>
<div class="m2"><p>نگه دارش که گردد شعله سرکش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چراغی را که از آتش شراریست</p></div>
<div class="m2"><p>کجا بر پرتو او اعتباریست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنین القصه لطف آن وفا کیش</p></div>
<div class="m2"><p>شدی هر روز از روز دگر بیش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دمی بی یکدگر آرامشان نه</p></div>
<div class="m2"><p>به غیر ازدیدن هم کارشان نه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر یک لحظه می‌بودند بی هم</p></div>
<div class="m2"><p>برون می‌رفت افغانشان ز عالم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شدی هر روز افزون شوق ناظر</p></div>
<div class="m2"><p>به مکتب بیشتر می‌گشت حاضر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو بی‌منظور یک دم جا گرفتی</p></div>
<div class="m2"><p>به همدرسان ره غوغا گرفتی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که قرآن کردم از دست شما بس</p></div>
<div class="m2"><p>نمی‌خواهم که همدرسم شود کس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرا دیوانه کرد این درس خواندن</p></div>
<div class="m2"><p>نمی‌دانم چه می‌خواهید از من</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به یکدیگر دریدی دفتر خویش</p></div>
<div class="m2"><p>که این مکتب نمی‌خواهم از این بیش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نظر از راه مکتب بر نمی‌داشت</p></div>
<div class="m2"><p>بدین اندوه و این رنج عالمی داشت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دمی سد ره برون رفتی ز مکتب</p></div>
<div class="m2"><p>که شاه من کجا رفتست یا رب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گذشته آفتاب از جای هر روز</p></div>
<div class="m2"><p>کجا رفتست آن مهر جهانسوز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ازین مکتب گرفتندش مگر باز</p></div>
<div class="m2"><p>و گر نه کو که با من نیست دمساز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گهی کردی به جای خویش مسکن</p></div>
<div class="m2"><p>کشیدی سر به جیب و پا به دامن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شدی منظور چون از دور پیدا</p></div>
<div class="m2"><p>ز روی خرمی می‌جست از جا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که ای جای تو چشم خون فشانم</p></div>
<div class="m2"><p>بیا کز داغ دوری سوخت جانم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خوشا عشق و بلای عشقبازی</p></div>
<div class="m2"><p>دل ما و جفای عشقبازی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خوش آن راحت که دارد زحمت عشق</p></div>
<div class="m2"><p>مبادا هیچ دل بی‌زحمت عشق</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در او غم را خواص شادمانی</p></div>
<div class="m2"><p>ازو مردن حیات جاودانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نهان در هر بلایش سد تنعم</p></div>
<div class="m2"><p>به هر اندوه او سد خرمی گم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به جام او مساوی شهد با زهر</p></div>
<div class="m2"><p>در او یکسان خواص زهر و پازهر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فراغت بخشد از سودای غیرت</p></div>
<div class="m2"><p>رهاند خاطر از غوغای غیرت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نشاند در مقام انتظارت</p></div>
<div class="m2"><p>که کی آید برون از خانه یارت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دمی گر دیرتر آید برون یار</p></div>
<div class="m2"><p>ز دل بیرون رود طاقت به یکبار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شود وسواس عشقت رهزن صبر</p></div>
<div class="m2"><p>کنی سد چاک در پیراهن صبر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>لباس صبر تا دامن دریدن</p></div>
<div class="m2"><p>گریبان چاک هر جانب دویدن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>در آن راهش که روزی دیده باشی</p></div>
<div class="m2"><p>ز مهرش گرد سر گردیده باشی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>روی آنجا به تقریبی نشینی</p></div>
<div class="m2"><p>سراغش گیری از هر کس که بینی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>که گردد ناگهان از دور پیدا</p></div>
<div class="m2"><p>نگاهش جانب دیگر به عمدا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به شوخی دیده را نادیده کردن</p></div>
<div class="m2"><p>به تندی از بر عاشق گذردن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به هر دیدن هزاران خنده پنهان</p></div>
<div class="m2"><p>تغافل کردنی سد لطف با آن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بدینسان مدتی بودند دمساز</p></div>
<div class="m2"><p>دلی فارغ ز چرخ حیله پرداز</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شبی چون طرهٔ منظور ناظر</p></div>
<div class="m2"><p>به کنجی داشت جا آشفته خاطر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>درآن آشفتگی خواب غمش برد</p></div>
<div class="m2"><p>غم عالم به دیگر عالمش برد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>میان بوستانی جای خود دید</p></div>
<div class="m2"><p>چه بستان، جنتی مأوای خود دید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چنار و سرو را در دست بازی</p></div>
<div class="m2"><p>لباس سبزه از شبنم نمازی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به زیر سایهٔ سرو و صنوبر</p></div>
<div class="m2"><p>به یک پهلو فتاده سبزه تر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>صنوبر صوف سبز افکنده بر دوش</p></div>
<div class="m2"><p>درخت بید گشته پوستین پوش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>در آن گلشن نظر هر سو گشادی</p></div>
<div class="m2"><p>که ناگه ز آن میان برخاست بادی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بسان خس ربود از جای خویشش</p></div>
<div class="m2"><p>بیابانی عجب آورده پیشش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بیابان غمی ، دشت بلایی</p></div>
<div class="m2"><p>کشنده وادیی ، خونخوار جایی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>عیان از گردباد آن بیابان</p></div>
<div class="m2"><p>ز هر سو اژدری بر خویش پیچان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز موج پشته‌های ریگ آن بر</p></div>
<div class="m2"><p>نمایان گشته نقش پشت اژدر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>زبان اژدها برگ گیاهش</p></div>
<div class="m2"><p>خم و پیچ افاعی کوره راهش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>عیان از کاسه‌های چشم اژدر</p></div>
<div class="m2"><p>ز هر سو لالهٔ سیراب از آن بر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شده زهر مصیبت سبزه زارش</p></div>
<div class="m2"><p>ز خون بیدلان گل کرده خارش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کدوی می شده خر زهره در وی</p></div>
<div class="m2"><p>به زهر او داده از جام فنا می</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>پی گمگشتهٔ آن دشت اندوه</p></div>
<div class="m2"><p>شد آتش چشم اژدر بر سر کوه</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به غایت کرد هولی در دلش کار</p></div>
<div class="m2"><p>ز روی هول شد از خواب بیدار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به خود می‌گفت این خوابی که دیدم</p></div>
<div class="m2"><p>وزان در جیب محنت سر کشیدم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>به بیداری نصیبم گر شود وای</p></div>
<div class="m2"><p>چه خواهم کرد با جان غم افزای</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>از آن خواب گران کوه غمی داشت</p></div>
<div class="m2"><p>چه کوه غم که بار عالمی داشت</p></div></div>