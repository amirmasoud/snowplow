---
title: >-
    رفتن شاهزاده منظور به شکار و باز را بر کبک انداختن و شام فراق ناظر را به صبح وصال مبدل ساختن
---
# رفتن شاهزاده منظور به شکار و باز را بر کبک انداختن و شام فراق ناظر را به صبح وصال مبدل ساختن

<div class="b" id="bn1"><div class="m1"><p>برد ره نکته ساز معنی اندیش</p></div>
<div class="m2"><p>چنین ره بر سر گم کردهٔ خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که در نزدیک آن دلکش نشیمن</p></div>
<div class="m2"><p>بدان کوهی که ناظر داشت مسکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به قصد کبک منظور دل افروز</p></div>
<div class="m2"><p>گشود از بند پای باز یک روز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز ره شد از خرام کبک بازش</p></div>
<div class="m2"><p>ز پی شد کورد با خویش بازش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیامد باز و او می‌رفت از پی</p></div>
<div class="m2"><p>بیابان از پی او ساختی طی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین تا کرد جا بر طرف کهسار</p></div>
<div class="m2"><p>ز تاب تشنگی افتاد از کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برای آب می‌گردید در کوه</p></div>
<div class="m2"><p>ره افتادش سوی آن غار اندوه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مقامی دید در وی دام و دد جمع</p></div>
<div class="m2"><p>در او هر جانور از نیک و بد جمع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میان جمعشان ژولیده مویی</p></div>
<div class="m2"><p>وجود لاغرش پیچیده مویی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پریشان کرده بر سرموی سودا</p></div>
<div class="m2"><p>چو شمع مرده‌ای بنشسته از پا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تنش در موی سر گردیده پنهان</p></div>
<div class="m2"><p>ز سوز دل به خاک تیره یکسان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پر از خونش دو چشم ناغنوده</p></div>
<div class="m2"><p>چو اخگرها ز خاکستر نموده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو بوی غیردام و دد شنیدند</p></div>
<div class="m2"><p>ز جا جستند و از دورش رمیدند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز دام و دد چو دورش گشت خالی</p></div>
<div class="m2"><p>خروشان شد ز درد خسته حالی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که از اندوه و هجران آه و سد آه</p></div>
<div class="m2"><p>مرا جان کاست، آه از هجر جانکاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>منم با وحشیان گردیده همدم</p></div>
<div class="m2"><p>گرفته گوشه‌ای ز ابنای عالم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا با چشم آهو زان خوش افتاد</p></div>
<div class="m2"><p>کز آن آهوی وحشی می‌دهد یاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیا ای آهوی وحشی کجایی</p></div>
<div class="m2"><p>ببین حالم به دشت بینوایی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بیا کز هجر روز خسته حالان</p></div>
<div class="m2"><p>سیه گردیده چون چشم غزالان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو در بتخانه چین با بتان یار</p></div>
<div class="m2"><p>به غار مصر من چون نقش دیوار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به دشت چین تو با مشکین غزالان</p></div>
<div class="m2"><p>به کوه مصر من چون شیر نالان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چه کم گردد که از چشم فسونساز</p></div>
<div class="m2"><p>کنی در ساحری افسونی آغاز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که چون بر هم زنم چشم جهان بین</p></div>
<div class="m2"><p>ترا با خویش بینم عشرت آیین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خوش آن روزی که در چین منزلم بود</p></div>
<div class="m2"><p>مراد دل ز جانان حاصلم بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به هر جایی که بودم یار من بود</p></div>
<div class="m2"><p>به هر غم مونس و غمخوار من بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گهی با هم به مکتبخانه بودیم</p></div>
<div class="m2"><p>دمی با هم به یک کاشانه بودیم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فلک روزی که طرح این غم انداخت</p></div>
<div class="m2"><p>که نومیدم ز روز وصل او ساخت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دگر خود را ندیدم شاد از آن روز</p></div>
<div class="m2"><p>چه روزی بود خرم یاد از آن روز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرا این داغ از آنها بیشتر سوخت</p></div>
<div class="m2"><p>که چون چرخ آتش محرومی افروخت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گره دیدم به دل این آرزو را</p></div>
<div class="m2"><p>ندیدم بار دیگر روی او را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وداع او مرا روزی نگردید</p></div>
<div class="m2"><p>ازو کارم به فیروزی نگردید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرا از خویش باید ناله کردن</p></div>
<div class="m2"><p>که خود کردم نه کس این جور با من</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر بی‌روی آن شمع شب افروز</p></div>
<div class="m2"><p>به مکتب می‌نمودم صبر یک روز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>معلم را نمی‌آزردم از خویش</p></div>
<div class="m2"><p>صبوری می‌نمودم پیشهٔ خویش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ندیدی کس چنین ناشادم از هجر</p></div>
<div class="m2"><p>به این محنت نمی‌افتادم از هجر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو منظور این سخنها کرد ازو گوش</p></div>
<div class="m2"><p>خروشی بر کشید و گشت بیهوش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از آن فریاد ناظر از زمین جست</p></div>
<div class="m2"><p>زد از روی تعجب دست بر دست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که شوقم برد از جا این صدا چیست</p></div>
<div class="m2"><p>به گوشم این صدای آشنا چیست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ازین آواز دل در اضطراب است</p></div>
<div class="m2"><p>رگ جان زین صدا در پیچ و تاب است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دلم رقاص شد این بیغمی چیست</p></div>
<div class="m2"><p>به راه دیده اشک خرمی چیست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به شادی می‌دود اشکم چه دیده‌ست</p></div>
<div class="m2"><p>نوید وصل پنداری شنیده‌ست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>قد من راست شد بارش که برداشت</p></div>
<div class="m2"><p>دلم خوش گشت آزارش که برداشت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>لبم با خنده همراز است چونست</p></div>
<div class="m2"><p>دلم با عشق دمساز است چونست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>برآمد بخت خواب آلوده از خواب</p></div>
<div class="m2"><p>سرشک شادیم زد خانه را آب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نمی‌دانم که خواهد آمد از راه</p></div>
<div class="m2"><p>که رفت از دل به استقبال او آه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چه بوی امروز همراه صبا بود</p></div>
<div class="m2"><p>که جانم تازه گشت و روحم آسود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همان راحت از آن بو جان من یافت</p></div>
<div class="m2"><p>که یعقوب از نسیم پیرهن یافت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>صبا گفتی که بوی یارم آورد</p></div>
<div class="m2"><p>که جانی در تن بیمارم آورد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز ره ای باد مشک افشان رسیدی</p></div>
<div class="m2"><p>مگر از کشور جانان رسیدی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز مشک افشانیت این خسته جان یافت</p></div>
<div class="m2"><p>ز دشت چین چنین بویی توان یافت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از این بو گر چه جانم یافت راحت</p></div>
<div class="m2"><p>ولیکن تازه شد جان را جراحت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو کرد از پیش رو موی جنون دور</p></div>
<div class="m2"><p>ستاده در برابر دید منظور</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز شوق وصل آن خورشید پایه</p></div>
<div class="m2"><p>به خاک افتاد و بیخود شد چو سایه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خوشا صحرای عشق و وادی او</p></div>
<div class="m2"><p>خوشا ایام وصل و شادی او</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خوشا تاریکی شام جدایی</p></div>
<div class="m2"><p>که بخشد صبح وصلش روشنایی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کسی کاو را فزونتر درد هجران</p></div>
<div class="m2"><p>فزونتر شادیش در وصل جانان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کنند از آب چون لب تشنگان تر</p></div>
<div class="m2"><p>کند ذوق آنکه باشد تشنه جانتر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چنان هجری که وصل انجام باشد</p></div>
<div class="m2"><p>بود خوش گر چه خون آشام باشد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کجا صاحب خرد آشفته حال است</p></div>
<div class="m2"><p>در آن هجران که امید وصال است</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>مرا هجری‌ست ناپیدا کرانه</p></div>
<div class="m2"><p>که داغ اوست با من جاودانه</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چه غم بودی در این هجران جانکاه</p></div>
<div class="m2"><p>اگر بودی امید وصل را راه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>فغان زین تیره شام ناامیدی</p></div>
<div class="m2"><p>که در وی نیست امید سفیدی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>قیامت صبح این شام سیاه است</p></div>
<div class="m2"><p>شب ما را قیامت صبحگاه است</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>خوشا ایام وصل مهرکیشان</p></div>
<div class="m2"><p>کجا رفتند ایشان ، یاد از ایشان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همه رفتند و زیر خاک خفتند</p></div>
<div class="m2"><p>بسان گنج یک یک رو نهفتند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به جامی سر به سر رفتند از هوش</p></div>
<div class="m2"><p>همه زین بزمشان بردند بر دوش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چنانشان خواب مستی کرد بیتاب</p></div>
<div class="m2"><p>که تا صبح جزا ماندند در خواب</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>اجل یا رب چه مرد افکن شرابی‌ست</p></div>
<div class="m2"><p>که در هر جانبی او را خرابی‌ست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>فغان کز خواری چرخ جفاکار</p></div>
<div class="m2"><p>همه رفتند یاران وفادار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>مگر ملک فنا جاییست دلکش</p></div>
<div class="m2"><p>که هرکس رفت کرد آنجا فروکش</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نیامد کس کز ایشان حال پرسیم</p></div>
<div class="m2"><p>ز دمسازان خود احوال پرسیم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>که در زیر زمین احوالشان چیست</p></div>
<div class="m2"><p>جدا از دوستداران حالشان چیست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>مرا حال برادر چیست آنجا</p></div>
<div class="m2"><p>رفیق و مونس او کیست آنجا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>برادر نی که نور دیده من</p></div>
<div class="m2"><p>مراد جان محنت دیدهٔ من</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>مرادی خسرو ملک معانی</p></div>
<div class="m2"><p>سرافراز سریر نکته دانی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>سمند عزم تا زین خاکدان راند</p></div>
<div class="m2"><p>هزاران بکر معنی بی‌پدر ماند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>هزاران بکر فکرت دوش بر دوش</p></div>
<div class="m2"><p>نشسته در عزای او سیه پوش</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ز روشان گرد ماتم آشکاره</p></div>
<div class="m2"><p>در این ماتم دل هر یک دو پاره</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بیا وحشی بس است این نوحهٔ غم</p></div>
<div class="m2"><p>مگو در بزم شادی حرف ماتم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>که باشد هر کلامی را مقامی</p></div>
<div class="m2"><p>مقام خاص دارد هر کلامی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>به هوش خود چو آمد شاهزاده</p></div>
<div class="m2"><p>بدید از دور ناظر اوفتاده</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>سرش را بر سر زانوی خود ماند</p></div>
<div class="m2"><p>به روی او خروشان روی خود ماند</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>که ای بیمار غم حال دلت چیست</p></div>
<div class="m2"><p>به روز بیدلی در منزلت کیست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ز تنهایی چو خواهی راز گویی</p></div>
<div class="m2"><p>بگو تا با که حالت بازگویی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>به شبها شمع بزم تیره‌ات چیست</p></div>
<div class="m2"><p>چو گویی حرف روی حرف درکیست</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>به غیر از آه گرمت کیست دمساز</p></div>
<div class="m2"><p>بجز کوهت که می‌گردد هم آواز</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بگو جز دود آه بیقراری</p></div>
<div class="m2"><p>به روز بی‌کسی بر سر چه دار ی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>به غیر ازقطره اشک دمادم</p></div>
<div class="m2"><p>که می‌گردد به گردت در شب غم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چو خود را افکنی از کوه دلتنگ</p></div>
<div class="m2"><p>ترا بر سر که می‌آید به جز سنگ</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>چو باز آمد به حال خویش ناظر</p></div>
<div class="m2"><p>به پیش دیده جانان دید حاضر</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>سر خود بر سر زانوی او دید</p></div>
<div class="m2"><p>رخ پر گرد خود بر روی او دید</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ز جای خویشتن برخاست خوشحال</p></div>
<div class="m2"><p>ز درد و رنج دوری فارغ البال</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>خروشان شد که آیا کیستی تو</p></div>
<div class="m2"><p>ملک یا حور آیا چیستی تو</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>منم این وان تویی اندر برابر</p></div>
<div class="m2"><p>نمی‌آید مرا این حال باور</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>تویی این یا پری آیا کدام است</p></div>
<div class="m2"><p>بگو با من ترا آخر چه نام است</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>به شادی دست یکدیگر گرفتند</p></div>
<div class="m2"><p>نوای خرمی از سر گرفتند</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>روان گشتند شادان چنگ در چنگ</p></div>
<div class="m2"><p>نوای خوشدلی کردند آهنگ</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چه خوشتر زانکه بعد از مدتی چند</p></div>
<div class="m2"><p>دو یار همدم بگسسته پیوند</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>نبوده آگهی از یکدگرشان</p></div>
<div class="m2"><p>نه از جاه و مقام هم خبرشان</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>فلک ناگه کند افسونگری ساز</p></div>
<div class="m2"><p>رساند بی‌خبرشان پیش هم باز</p></div></div>