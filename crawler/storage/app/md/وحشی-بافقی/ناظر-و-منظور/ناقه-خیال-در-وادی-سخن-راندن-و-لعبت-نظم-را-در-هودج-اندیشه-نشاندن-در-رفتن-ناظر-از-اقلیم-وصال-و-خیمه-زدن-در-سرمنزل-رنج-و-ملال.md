---
title: >-
    ناقهٔ خیال در وادی سخن راندن و لعبت نظم را در هودج اندیشه نشاندن در رفتن ناظر از اقلیم وصال و خیمه زدن در سرمنزل رنج و ملال
---
# ناقهٔ خیال در وادی سخن راندن و لعبت نظم را در هودج اندیشه نشاندن در رفتن ناظر از اقلیم وصال و خیمه زدن در سرمنزل رنج و ملال

<div class="b" id="bn1"><div class="m1"><p>سفر سازندهٔ این طرفه صحرا</p></div>
<div class="m2"><p>به عزم کارسازی زد چنین پا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چون دستور از آن راز آگهی یافت</p></div>
<div class="m2"><p>رخ از ذوق بساط خرمی تافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خود زد رأی در تغییر فرزند</p></div>
<div class="m2"><p>که گر بگذارمش در خانه یک چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رسوایی شود ناگه فسانه</p></div>
<div class="m2"><p>فتد افسانهٔ او در میانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جنون از خانه اندارد برونش</p></div>
<div class="m2"><p>به گوش شه رسد حرف جنونش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو خسرو پرسد از من شرح حالش</p></div>
<div class="m2"><p>بگویم چیست باعث بر ملالش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسی در چارهٔ آن کار کوشید</p></div>
<div class="m2"><p>چنین در کارش آخر مصلحت دید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که همره سازدش با کاردانی</p></div>
<div class="m2"><p>رفیق او کند بسیار دانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تجارت کردنش سازد بهانه</p></div>
<div class="m2"><p>به شهری دیگرش سازد روانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که شاید درد عشق او شود کم</p></div>
<div class="m2"><p>چو یک چندی برآید گرد عالم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر خواهی در این دیر مجازی</p></div>
<div class="m2"><p>دوایی بهر درد عشقبازی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنه بهر سفر رو در بیابان</p></div>
<div class="m2"><p>که درد عشق را اینست درمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وزیر دانش اندوز خردمند</p></div>
<div class="m2"><p>چو کرد این فکر در تدبیر فرزند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طلب فرمود و پیش خود نشاندش</p></div>
<div class="m2"><p>به گوش از هر دری حرفی رساندش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس آنگه گفت کای تابنده خورشید</p></div>
<div class="m2"><p>جهان را از تو روشن صبح امید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مثل باشد درین دیرینه مسکن</p></div>
<div class="m2"><p>جهان گشتن به از آفاق خوردن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرت باید به فر سروری دست</p></div>
<div class="m2"><p>سفر کن زانکه این فر در سفر هست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو لعل از خاک کان گردد سفر ساز</p></div>
<div class="m2"><p>دهد زینت به تاج هر سرافراز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز یکجا آب چون نبود مسافر</p></div>
<div class="m2"><p>شود یکسان بخاک تیره آخر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بنه سر در سفر ، منشین به یک جا</p></div>
<div class="m2"><p>گرت باید ز اسفل شد ، به اعلا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در نامی شود هر قطره باران</p></div>
<div class="m2"><p>ز ابرش چون سفر باشد به عمان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به کار خویش حیران ماند ناظر</p></div>
<div class="m2"><p>بسی ز آن حرف شد آشفته خاطر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه روی آنکه گوید «نی» جوابش</p></div>
<div class="m2"><p>نه رای آنکه سازد «با» خطابش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برو درماند پیشش آخر کار</p></div>
<div class="m2"><p>جوابش گفت چون شد حرف بسیار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که مقصود پدر چون رفتن ماست</p></div>
<div class="m2"><p>ز ما بودن به جای خویش بیجاست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز سر سازم به راه مدعا پای</p></div>
<div class="m2"><p>به جان خدمت کنم خدمت بفرمای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پدر زان گفتگو گردید خوشحال</p></div>
<div class="m2"><p>ز فکر کار او شد فارغ‌البال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>طلب فرمود مرد کاردانی</p></div>
<div class="m2"><p>به غایت زیرکی بسیار دانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز گرم و سردعالم بوده آگاه</p></div>
<div class="m2"><p>جفای راه دیده گاه و بیگاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به تاج خویش دادش سر بلندی</p></div>
<div class="m2"><p>به تشریف شریفش ارجمندی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پس آنگه گفت کای از کار آگاه</p></div>
<div class="m2"><p>ز دامان تو دست فتنه کوتاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نماند بر تو پنهان این حکایت</p></div>
<div class="m2"><p>که ناظر راست سودای تجارت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چه باشد گر بود در خدمت تو</p></div>
<div class="m2"><p>به کام خود رسد از دولت تو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جوابش گفت مرد کار دیده</p></div>
<div class="m2"><p>که او را در قدم باشم به دیده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وزیر آماده کرد اسباب رهشان</p></div>
<div class="m2"><p>میسر شد وداع پادشهشان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پس آنگه بهر رفتن بار بستند</p></div>
<div class="m2"><p>به مرکبهای تازی برنشستند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز شهر آورد ناظر روی در راه</p></div>
<div class="m2"><p>ز پس می‌دید و از دل می‌کشید آه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نظر سوی سواد شهر می‌کرد</p></div>
<div class="m2"><p>ز دل پر می‌کشید آه از سر درد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو آن کش وقت رحلت کردن آید</p></div>
<div class="m2"><p>به عالم دیدهٔ حسرت گشاید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بیا وحشی کزین دیر غم آباد</p></div>
<div class="m2"><p>به رفتن گام بگشاییم چون باد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چنین تا چند در یکجا نشینیم</p></div>
<div class="m2"><p>ز حد شد تا به کی از پا نشینیم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به یک جا خانه آن مقدار کردیم</p></div>
<div class="m2"><p>که خود را پیش مردم خوار کردیم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز ما دلگیر گردیدند یاران</p></div>
<div class="m2"><p>به جان گشتند دشمن دوستداران</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خوش آنکس را که یکجا نیست مسکن</p></div>
<div class="m2"><p>نه کس را دوست می‌بیند نه دشمن</p></div></div>