---
title: >-
    آمدن ناظر و منظور به لشگرگاه اقبال و آگاهی شاه جهان‌پناه از صورت احوال و استقبال ایشان کردن و شرایط اعزاز بجای آوردن
---
# آمدن ناظر و منظور به لشگرگاه اقبال و آگاهی شاه جهان‌پناه از صورت احوال و استقبال ایشان کردن و شرایط اعزاز بجای آوردن

<div class="b" id="bn1"><div class="m1"><p>دلا بر عکس ابنای زمان باش</p></div>
<div class="m2"><p>به روز بینوایی شادمان باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم خود خور به روز شادمانی</p></div>
<div class="m2"><p>که دارد مرگ در پی زندگانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبیند بی‌خزان کس لاله زاری</p></div>
<div class="m2"><p>خزان تا نگذرد ناید بهاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بی‌برگی چو سازد شاخ یکچند</p></div>
<div class="m2"><p>کند سر سبزش این شاخ برومند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشد چون ژاله در جیب صدف سر</p></div>
<div class="m2"><p>شود آخر شهان را زیب افسر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گهر گر زخم مثقب برنتابد</p></div>
<div class="m2"><p>به بازوی بتان کی دست یابد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نباشد غنچه تا یکچند دلتنگ</p></div>
<div class="m2"><p>ز دل کی خنده‌اش از خود برد زنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلی هر کار وقتی گشته تعیین</p></div>
<div class="m2"><p>چو خرما خام باشد نیست شیرین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز ناکامی چه می‌نالی در این کاخ</p></div>
<div class="m2"><p>ثمر چون پخته شد خود افتد از شاخ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به سنگ از شاخ افتد میوهٔ خام</p></div>
<div class="m2"><p>ولیکن تلخ سازد خوردنش کام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شود از غوره دندان کند چندان</p></div>
<div class="m2"><p>که از حلوا بباید کند دندان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دهد درد شکم حلوای خامت</p></div>
<div class="m2"><p>ز دارو تلخ باید کرد کامت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنین می‌گوید آن از کار آگه</p></div>
<div class="m2"><p>چو با ناظر بشد منظور همره</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به سوی دشت شد منظور با یار</p></div>
<div class="m2"><p>دلی پرخنده و لب پر ز گفتار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عنان رخش در دستی گرفته</p></div>
<div class="m2"><p>به دستی دست پا بستی گرفته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز هجر و وصل می‌گفتند با هم</p></div>
<div class="m2"><p>گهی بودند خندان گاه خرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که سرکردند نا گه خیل منظور</p></div>
<div class="m2"><p>ز غوغاشان جهان گردید پر شور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نظر کردند سوی شاهزاده</p></div>
<div class="m2"><p>ز اسب خویش دیدندش پیاده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به دستش دست مجنون غریبی</p></div>
<div class="m2"><p>عجب ژولیده مو شخصی عجیبی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بهم گفتند کاین شخص عجب کیست</p></div>
<div class="m2"><p>به دستش دست منظور از پی چیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو شد نزدیک ایشان شاهزاده</p></div>
<div class="m2"><p>همه گشتند از توسن پیاده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز روی عجز در پایش فتادند</p></div>
<div class="m2"><p>به عجزش رو به خاک ره نهادند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اشارت کرد تا رخشی گزیدند</p></div>
<div class="m2"><p>به تعظیمش سوی ناظر کشیدند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به ناظر همعنان گردید منظور</p></div>
<div class="m2"><p>ز حیرت در میان لشکری دور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به هم منظور و ناظر گرم گفتار</p></div>
<div class="m2"><p>چنین تا طرف آن فرخنده گلزار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به طرف چشمه‌ای بنشست ناظر</p></div>
<div class="m2"><p>به پیشش سر تراشی گشت حاضر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز سر موی جنون بردش به پا کی</p></div>
<div class="m2"><p>به بردش پاک چرک از جرم خاکی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدن آراست از تشریف جانان</p></div>
<div class="m2"><p>چو گل آمد سوی منظور خندان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یکی از جملهٔ خاصان منظور</p></div>
<div class="m2"><p>بگفت ای دیده را از دیدنت نور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چه باشد گر گشایی پرده زین راز</p></div>
<div class="m2"><p>به ما گویی حدیث این جوان باز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از او منظور چون این حرف بشنید</p></div>
<div class="m2"><p>ز درج لعل گوهر بار گردید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حدیث خویش و شرح حال ناظر</p></div>
<div class="m2"><p>بیان فرمود ز اول تا به آخر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نمی‌دانست لشکر تا به آن روز</p></div>
<div class="m2"><p>که در چین شهریار است آن دل افروز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز حال هر دو چون گشتند آگاه</p></div>
<div class="m2"><p>یکی بهر نوید آمد سوی شاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شنید آن مژده چون شاه جهانبان</p></div>
<div class="m2"><p>به استقبال آمد با بزرگان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دعای شاه ناظر بر زبان راند</p></div>
<div class="m2"><p>به او شاه جهاندان آفرین خواند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به پوزش رفت خسرو سوی منظور</p></div>
<div class="m2"><p>که گر بیراهیی شد دار معذور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رخ خود ماند بر در شاهزاده</p></div>
<div class="m2"><p>که‌ای در عرصه‌ات شاهان پیاده</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چسان عذر کرمهایت توان خواست</p></div>
<div class="m2"><p>چه می‌گویم نه جای این سخنهاست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در آنجا چند روز القصه بودند</p></div>
<div class="m2"><p>وطن در بزم عشرت می‌نمودند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اشارت کرد شاه مصر کشور</p></div>
<div class="m2"><p>کز آنجا رو نهد بر شهر لشکر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به عزم مصر گردیدند راهی</p></div>
<div class="m2"><p>شه و منظور و ناظر با سپاهی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>برای خود در شادی گشودند</p></div>
<div class="m2"><p>به بزم شادمانی جا نمودند</p></div></div>