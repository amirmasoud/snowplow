---
title: >-
    بخش ۳۵ - در ستایش پنهان نمودن راز نهانی که آسایش دو جهانی‌ست
---
# بخش ۳۵ - در ستایش پنهان نمودن راز نهانی که آسایش دو جهانی‌ست

<div class="b" id="bn1"><div class="m1"><p>اگر خواهی بماند راز پنهان</p></div>
<div class="m2"><p>به دل آن راز پنهان ساز چو جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن راز آشکارا تا توانی</p></div>
<div class="m2"><p>که اندر محنت و اندوه مانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حکیم این راز را خود پرده در شد</p></div>
<div class="m2"><p>که رازی کن دو بیرون شد سمر شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که گل چون راز خویش از پرده بگشاد</p></div>
<div class="m2"><p>به اندک فرصتی در آتش افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در اول نکهت و تابش ببردند</p></div>
<div class="m2"><p>در آخر ز آتشی آبش ببردند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو کان از کیسه بیرون یک گهر داد</p></div>
<div class="m2"><p>تن خود را به راه سد خطر داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نخستش پیکر از پولاد سودند</p></div>
<div class="m2"><p>وزان پس گوهرش یغما نمودند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو راز کوهکن چون کوه شد فاش</p></div>
<div class="m2"><p>به سر افکنده خسرو فکر یغماش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که آن گوهر که در خورد شهان بود</p></div>
<div class="m2"><p>چودل در سینهٔ پاکش نهان بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین گویید کز شیرین و فرهاد</p></div>
<div class="m2"><p>خبر در محفل پرویز افتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که از چین چابک استادی قوی دست</p></div>
<div class="m2"><p>که در فرسودن سنگش بود دست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رسیده در بر بانوی ارمن</p></div>
<div class="m2"><p>سر شیرین لبان شیرین پرفن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گشاده دست در کار آزمایی</p></div>
<div class="m2"><p>نموده سحر در صنعت نمایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز دست و تیشهٔ آن مرد فسون ساز</p></div>
<div class="m2"><p>شده پولادسای و خاره پرداز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تهی از بیستون کرده‌ست طاقی</p></div>
<div class="m2"><p>چو چرخ بی‌ستون عالی رواقی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز تیشه نقشهابربسته بر سنگ</p></div>
<div class="m2"><p>که مانی را ز خاطر برده ارتنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنان در کار برده هندسی را</p></div>
<div class="m2"><p>که شسته نامهٔ اقلیدسی را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در این صنعت به شوق زر نبوده‌ست</p></div>
<div class="m2"><p>که با شوق دگر بازو گشوده‌ست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه بر سیم است چشم او نه بر زر</p></div>
<div class="m2"><p>که افشاند ز نوک تیشه گوهر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو مزدوران نداند زر پرستی</p></div>
<div class="m2"><p>که هست از باده دیگر به مستی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین گویند با آن کس که گفته</p></div>
<div class="m2"><p>نباشد اعتمادی بر شنفته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که شیرین گوشهٔ چشمی نموده‌ست</p></div>
<div class="m2"><p>به کلی خاطر او را ربوده‌ست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدان هم نیز می‌ماند از آن رو</p></div>
<div class="m2"><p>که کرد او آنچه در یک مه به نیرو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بود چون خسروی گر کارفرما</p></div>
<div class="m2"><p>نیاید او ز چندین خاره فرسا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به حدی خاطر شیرین برآشفت</p></div>
<div class="m2"><p>که نه خوردش به خاطر ماند و نه خفت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنانش آتش غیرت بر افروخت</p></div>
<div class="m2"><p>که یاقوتی که بودش بر کمر سوخت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر چه غیرت اندرهرتنی هست</p></div>
<div class="m2"><p>برد بر خسرو آتش بیشتر دست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که درویش ارچه غیرتمند باشد</p></div>
<div class="m2"><p>به عجز خویشتن در بند باشد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ولی غیرت چو با قدرت کند زور</p></div>
<div class="m2"><p>حریف ار چرخ باشد نیست معذور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو شه غیرت کند با قدرت خویش</p></div>
<div class="m2"><p>جهان سوزد ز سوز غیرت خویش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به خلوت شد شه و شاپور را خواند</p></div>
<div class="m2"><p>فزودش قدر و پیش خویش بنشاند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به خود پیچید و گفت ای دانش اندوز</p></div>
<div class="m2"><p>چه گویی چون کنم با این غم و سوز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چه سازم با چنین نا آشنائی</p></div>
<div class="m2"><p>که بگزیده‌ست بر شاهی گدایی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چه گویم با چنین بی روی و راهی</p></div>
<div class="m2"><p>که خوی افکنده با ظلمت ز ماهی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همانا آن پری را برده دیوی</p></div>
<div class="m2"><p>که پردازد به دیوی از خدیوی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نبودم واقع از طبع زبونش</p></div>
<div class="m2"><p>که آگاهی نبودم از درونش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بر آزادگان نبود ستوده</p></div>
<div class="m2"><p>که بندی دل به کس ناآزموده</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کسی با ناسزایی چون دهد دست</p></div>
<div class="m2"><p>سزایش عهد و پیمانی که بشکست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چه خوش گفت آنکه با نا اهل شد خویش</p></div>
<div class="m2"><p>که هرکس خویش کاهد قیمت خویش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به دشمن شهد و با ما چون شرنگ است</p></div>
<div class="m2"><p>تو بینی تا کجا شیرین دو رنگ است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زمین با خصم و با ما آسمان است</p></div>
<div class="m2"><p>تو بینی تا کجا نا مهربان است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تو آنرا بین که با شاهان نپراخت</p></div>
<div class="m2"><p>به نطع خسروی بازی در انداخت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بگویم تا که خونش را بریزید</p></div>
<div class="m2"><p>که با شاهان گدایان کم ستیزند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زمین را بوسه زد فرزانه شاپور</p></div>
<div class="m2"><p>که رای شاه باد از هر بدی دور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مبادا آسمان از خدمتت سیر</p></div>
<div class="m2"><p>همه کارت به وفق رای و تدبیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جهان را روشنی از اخترت باد</p></div>
<div class="m2"><p>سرگردن کشان خاک درت باد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>یکی گستاخ خواهم گفت شه را</p></div>
<div class="m2"><p>به شرط آنکه شه بخشد گنه را</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خطا در خدمت شاهان روا نیست</p></div>
<div class="m2"><p>ولی گویم که شیرین را خطا نیست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مگر شیرین نه بهر خدمت شاه</p></div>
<div class="m2"><p>سفر ازمنزل خود کرده چون ماه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مگر نه شهره شد در شهر و بازار</p></div>
<div class="m2"><p>به مهر و الفت شاه جهان دار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مگر نه رنجها در راه شه دید</p></div>
<div class="m2"><p>مگر نه طعنه‌ها از خلق بشنید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به هر چیزی که دید از نیک و از بد</p></div>
<div class="m2"><p>قدم کی بر خلاف دوستی زد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به جرم آنکه بی پیوند و آیین</p></div>
<div class="m2"><p>نیامد با شه او را سر به بالین</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به یک ره خسرو از وی دل بپرداخت</p></div>
<div class="m2"><p>ترش رو شد به شیرین، با شکر ساخت</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همین جرم آن نگار سیمبر داشت</p></div>
<div class="m2"><p>که از الطاف شاه اندر نظر داشت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>که همچون خاصگان شاهش نبیند</p></div>
<div class="m2"><p>چو خاصانش به بانویی گزیند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو شاه از لطف خود کردش گرامی</p></div>
<div class="m2"><p>ز شکر داد او را تلخکامی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نشاید پیش شاهان گفت جز راست</p></div>
<div class="m2"><p>گر اینجا نیست شیرین خسرو اینجاست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همین با این روشها باورم نیست</p></div>
<div class="m2"><p>که شیرین لحظه‌ای بی شه کند زیست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گمانم کاین حدیث آوازهٔ اوست</p></div>
<div class="m2"><p>هم از نیرنگهای تازهٔ اوست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>که خسرو را در اندازد به تشویش</p></div>
<div class="m2"><p>تهی سازد دل پر اندوه خویش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>کجا همچون جهانداری جهان را</p></div>
<div class="m2"><p>که شیرین خوش کند جان غمین را</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گمانم آنکه آن بیچاره مزدور</p></div>
<div class="m2"><p>بود محنت کشی از خانمان دور</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز سختی لختی آسوده‌ست جانش</p></div>
<div class="m2"><p>که خسرو را کند حق مهربانش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دگر در کشتن آن بی گنه مرد</p></div>
<div class="m2"><p>چه کوشی چون ندانی او چه بد کرد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ز مسکینی که آگاهیت نبود</p></div>
<div class="m2"><p>برو آن به که بد خواهیت نبود</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>مکن در خون مسکینان دلیری</p></div>
<div class="m2"><p>ز مسکینی بترس و دستگیری</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>صلاح آن بینم ای شاه جهانگیر</p></div>
<div class="m2"><p>که بفرستی یکی با رای و تدبیر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>فرستی نامه‌ای همراه او نیز</p></div>
<div class="m2"><p>عباراتی سراسر شکوه‌آمیز</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>هم از آخر نمایی عذرخواهی</p></div>
<div class="m2"><p>دهی امیدش از الطاف شاهی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>توقع دارد او نیز ای شهنشاه</p></div>
<div class="m2"><p>کز او یادآوری در گاه و بیگاه</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>نگویی عهد شیرین بی ثبات است</p></div>
<div class="m2"><p>ز شه موقوف اندک التفات است</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>که دلگیر از حریم شه برون رفت</p></div>
<div class="m2"><p>دل او داند و او خود که چون رفت</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چو آزردیش باشی عذر خواهش</p></div>
<div class="m2"><p>ور از ره رفت باز آری به رامش</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به افسون رای خسرو را بر آن داشت</p></div>
<div class="m2"><p>که می‌باید به شیرین نامه بنگاشت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>دبیر آمد به کف بگرفت خامه</p></div>
<div class="m2"><p>پرند چین گشوده بهر نامه</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>طراز پرنیان نام خدا کرد</p></div>
<div class="m2"><p>که چرخ بی‌ستون را او بپاکرد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>فلک از زینت افزا شد ز انجم</p></div>
<div class="m2"><p>خرد در وی چو وهم اندر خرد گم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>جهان افروز از خورشید و از ماه</p></div>
<div class="m2"><p>درون آزار عقل و جان آگاه</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>سر گردن کشان در چنبر او</p></div>
<div class="m2"><p>رخ شاهان عالم بر در او</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ادب فرمای عشاق از نکویان</p></div>
<div class="m2"><p>بساط آرای خاک از لاله رویان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بلا پیدا کن از بالا بلندان</p></div>
<div class="m2"><p>خرد شیدا کن از مشکین کمندان</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>شهت اما نه چون من بندهٔ عشق</p></div>
<div class="m2"><p>دهنده عشق نی افکندهٔ عشق</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>برون آرا ز عقل عافیت ساز</p></div>
<div class="m2"><p>درون پیرا ز عشق خانه پرداز</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>یکی را سر نهد در دامن دوست</p></div>
<div class="m2"><p>یکی را خون کند در گردن دوست</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>به این درد و به آن درمان فرستد</p></div>
<div class="m2"><p>به هر کس هر چه شاید آن فرستد</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>وزان پس از شه با داد و آیین</p></div>
<div class="m2"><p>سوی بیدادگر بانوی شیرین</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>نگار زود رنج تلخ پاسخ</p></div>
<div class="m2"><p>بت دیر آشتی، شیرین فرخ</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>قدح پیمای بزم بی‌وفایی</p></div>
<div class="m2"><p>نوا پرداز قانون جدایی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>به دل سنگ افکن مینای طاقت</p></div>
<div class="m2"><p>به خوی آتش زن کشت محبت</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>به صورت نازنین و شوخ و چالاک</p></div>
<div class="m2"><p>به دل دور از همه خوبان هوسناک</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>خریداری شنیدم کردت آهنگ</p></div>
<div class="m2"><p>که نبود در ترازویش به جز سنگ</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>تو هم دل در هوای او نهادی</p></div>
<div class="m2"><p>گرفتی سنگی و سنگیش دادی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بجز رسوایی خود زین چه بینی</p></div>
<div class="m2"><p>که بر شاهی گدایی برگزینی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>خوش است این رسم با شاهان گرانی</p></div>
<div class="m2"><p>به مسکینان بی‌دل مهربانی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>نه با شاهی که از شاهی گذشته‌ست</p></div>
<div class="m2"><p>به پیشت خط به مسکینی نوشته‌ست</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>خوش است این شیوه با عالم بگویی</p></div>
<div class="m2"><p>به یک جانب نهادن زشت خویی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>نه دل پرداختن از شاه عالم</p></div>
<div class="m2"><p>نشستن با گرانی شاد و خرم</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>مرا از خلق عالم خود یکی گیر</p></div>
<div class="m2"><p>ز افزونی گذشتم اندکی گیر</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>خوش است این ره به طبع خلق بودن</p></div>
<div class="m2"><p>مدارا با همه عالم نمودن</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>نه از سر بازکردن سروری را</p></div>
<div class="m2"><p>گزیدن رند بی پا و سری را</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>چو شه را گوهری ارزنده باشی</p></div>
<div class="m2"><p>گدایی را نیرزد بنده باشی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>از این بگذشته از یاران جدایی</p></div>
<div class="m2"><p>به هر بیگانه کردن آشنایی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>خلل آرد به ملک خوبرویی</p></div>
<div class="m2"><p>گرفتم من نگفتم خود نکویی</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>گرفتم کز شکر آزرده بودی</p></div>
<div class="m2"><p>که از رشکش بسی خون خورده بودی</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>نشاید در هلاک خویش کوشی</p></div>
<div class="m2"><p>چنین از رشک شکر زهر نوشی</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چو غیرت دامنت ناچار بگرفت</p></div>
<div class="m2"><p>به رغم گل نشاید خار بگرفت</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>مرا کام دل و جان از شکر نیست</p></div>
<div class="m2"><p>به غیر از شهوت تن بیشتر نیست</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>از آن آتش که عشقت در من افروخت</p></div>
<div class="m2"><p>وجودم جمله از سر تا قدم سوخت</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>تو خود نفشانی و نپسندیم نیز</p></div>
<div class="m2"><p>که خویش آبی زنم بر آتش تیز</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>چو شیرین همچو فرهادیش باید</p></div>
<div class="m2"><p>چرا پرویز را شکر نشاید</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>چرا دست و دل از انصاف شویی</p></div>
<div class="m2"><p>مرا فرمایی و خود را نگویی</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>تو تا در فکر خویش و کام خویشی</p></div>
<div class="m2"><p>نه خصم من که خصم نام خویشی</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>به رغم من به هر کس آشنایی</p></div>
<div class="m2"><p>به من گر دشمنی با خود چرایی</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>ز من از بیم بدنامی گذشتی</p></div>
<div class="m2"><p>به نام دیگران بدنام گشتی</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>نیالودی گرفتم دامن پاک</p></div>
<div class="m2"><p>چه سازی زین که خوانندت هوسناک</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>دو رویی گر چه خوی نیکوان است</p></div>
<div class="m2"><p>ولیکن خوبرویی را زیان است</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>به کام دوستان بد نام بودن</p></div>
<div class="m2"><p>از آن بهتر که دشمن کام بودن</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>کنون با شکوه‌های من چه سازی</p></div>
<div class="m2"><p>به طعن و خنده دشمن چه سازی</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>مرا گر چون تو طبعی بیوفا بود</p></div>
<div class="m2"><p>کنونم جای چندین طعنه‌ها بود</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>ولیکن چون مرا آن طبع و خو نیست</p></div>
<div class="m2"><p>اگر حرف بدی گویم نکو نیست</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>اگر چه تا مرا این طبع و خو بود</p></div>
<div class="m2"><p>سپهرم برخلاف آرزو بود</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>کجا در دوستی برخود پسندم</p></div>
<div class="m2"><p>که همچون دشمنانت بردوست خندم</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>به نیکویی بدت را می‌شمارم</p></div>
<div class="m2"><p>به شیرینی به زهرت رغبت آرم</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>نهم بر خویش جرمی کز تو بینم</p></div>
<div class="m2"><p>گل افشانم به خاری کز تو چینم</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>فریبم خاطر خود گاه و بیگاه</p></div>
<div class="m2"><p>که باشد در دل سنگ توام راه</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>به صورت گر چه تلخی می‌فزایی</p></div>
<div class="m2"><p>نهانم کام جان شیرین نمایی</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>به عین دلبری دل مینوازی</p></div>
<div class="m2"><p>بری در آتش اما پخته سازی</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>مثل زد دلبری دیوانه‌ای را</p></div>
<div class="m2"><p>که ماند عشق مکتب خانه‌ای را</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>نخست استاد با طفلی کند خوی</p></div>
<div class="m2"><p>که از طفلی به دانش آورد روی</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>کند در دامن او قند و بادام</p></div>
<div class="m2"><p>که یکسر تلخ نتوان کردنش کام</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>چو اندک خو به دانش کرد کودک</p></div>
<div class="m2"><p>کند تلخی فزون شیرینی اندک</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>به دانش هر چه آنرا میل جان خواست</p></div>
<div class="m2"><p>به سختی این فزود از مرحمت کاست</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>چو یکسر خو به دانش کرد و فرهنگ</p></div>
<div class="m2"><p>بدل گردد به صلح و دوستی جنگ</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>بتان را نیز با دل داستانهاست</p></div>
<div class="m2"><p>به فرهنگ محبت ترجمانهاست</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>دهند اول ز عیاری فریبش</p></div>
<div class="m2"><p>از آن چشم و ذقن بادام و سیبش</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>ز راه و رسم دلداری در آیند</p></div>
<div class="m2"><p>چو میل افزود بر خواری فزایند</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>وفا چندان که ورزد عاشق زار</p></div>
<div class="m2"><p>شود بی مهرتر دلدار عیار</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>چو یکسر خاطرش با خویشتن دید</p></div>
<div class="m2"><p>چو یک جان با خود او را در دو تن دید</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>به کلی جانب او آورد روی</p></div>
<div class="m2"><p>به کام او ز عالم برکند خوی</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>مرا نیز از جفایش شکوه‌ها بود</p></div>
<div class="m2"><p>چو نیکو دیدم آن عین وفا بود</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>اگر چه هر چه را نیکو بر آن خوست</p></div>
<div class="m2"><p>به حکم آنکه را نیکوست نیکوست</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>ولیکن من نگویم خوش میندیش</p></div>
<div class="m2"><p>که شه را فرقها باشد ز درویش</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>بر آن سنگین دلت از بس فغان کرد</p></div>
<div class="m2"><p>دلم گفتی که کوبد آهن سرد</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>گدایی تا چه حیلت کار فرمود</p></div>
<div class="m2"><p>که آهن نرم گشتش همچو داود</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>نه عارت بود ای ناسفته گوهر</p></div>
<div class="m2"><p>که شاهان بر نشانندت بر افسر</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>چرا ننگت نمی‌آید بدین حال</p></div>
<div class="m2"><p>که مسکینی در آوردت به خلخال</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>اگر رخش هوس زینگونه دانی</p></div>
<div class="m2"><p>به رسوایی کشد کار تو دانی</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>قلمزن چون به کار نامه پرداخت</p></div>
<div class="m2"><p>شه از خاصان غلامی را روان ساخت</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>بدادش نامه و گفت برانگیز</p></div>
<div class="m2"><p>دل مجروح شیرین را نمک ریز</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>اگر خواهی که آساید دل شاه</p></div>
<div class="m2"><p>نباید هیچت آسودن در این راه</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>گرفت از شاه و چون سیلی برانگیخت</p></div>
<div class="m2"><p>بنای طاقت شیرین ز هم ریخت</p></div></div>