---
title: >-
    بخش ۴۰ - نازل شدن شیرین به دلجویی فرهاد مسکین در دامنهٔ کوه بیستون
---
# بخش ۴۰ - نازل شدن شیرین به دلجویی فرهاد مسکین در دامنهٔ کوه بیستون

<div class="b" id="bn1"><div class="m1"><p>چو نازل شد به فرش سبزه چون گل</p></div>
<div class="m2"><p>به گل افشاند زلف همچو سنبل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خود خواند آن آواره دل را</p></div>
<div class="m2"><p>برایش نرم کرد آن خاره دل را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشاندش رو به روی و پرده برداشت</p></div>
<div class="m2"><p>که دیدش کام خشک و چشم تر داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ساقی گفت آن مینای می کو</p></div>
<div class="m2"><p>نشاط محفل جمشید و کی کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیار و در قدح ریز و به من ده</p></div>
<div class="m2"><p>گلم افسرده بین آب چمن ده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بت ساقی قدح از باده پر کرد</p></div>
<div class="m2"><p>هلال جام را از می چو خور کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزد زانو به خدمت پیش شیرین</p></div>
<div class="m2"><p>به دستش داد بدری پر ز پروین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرفت از دست او شیرین خود کام</p></div>
<div class="m2"><p>به شوخی بوسه‌ای زد بر لب جام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس آنگه گفت با فرهاد مسکین</p></div>
<div class="m2"><p>که بستان این قدح از دست شیرین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بخور از دستم این جان داروی هوش</p></div>
<div class="m2"><p>که غمهای کهن سازد فراموش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگرخسرو به شکر کرده پیوند</p></div>
<div class="m2"><p>تو هم از لعل شیرین نوش کن قند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به کوری شکر قند مکرر</p></div>
<div class="m2"><p>مکرر بخشمت از لب نه شکر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شکر در کام خسرو خوش گواراست</p></div>
<div class="m2"><p>کز این قند مکرر روزه داراست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرفت از دست شیرین جام و نوشید</p></div>
<div class="m2"><p>چو خم از آتش آن آب جوشید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روان شد گرمی می در دماغش</p></div>
<div class="m2"><p>فروزان شد ز برق می چراغش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خرد یکباره بیرون شد ز دستش</p></div>
<div class="m2"><p>حجاب افکند یک سو چشم مستش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پی نظاره پرده شرم شق کرد</p></div>
<div class="m2"><p>ز تاب دیدنش شیرین عرق کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به برگ گل نشستن خوی چو شبنم</p></div>
<div class="m2"><p>گلش را تازگی افزود در دم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز لب چون غنچه خندان گشت و بشکفت</p></div>
<div class="m2"><p>به دلداری یار مهربان گفت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیا چون دل برم بنشین زمانی</p></div>
<div class="m2"><p>که برخوان وصالم میهمانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نظر بگشا به رخساری که خسرو</p></div>
<div class="m2"><p>بود محروم از آن ز آن دلبر نو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز کام قندم از شکر گذشته</p></div>
<div class="m2"><p>ز بدر نامم از اختر گذشته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز ارمن کان قندم را طلبکار</p></div>
<div class="m2"><p>شد و با شکرش شد گرم بازار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مگس طبعی یار بلهوس بین</p></div>
<div class="m2"><p>به هر جا شکر او را چون مگس بین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو فرهاد این سخن‌ها کرد از او گوش</p></div>
<div class="m2"><p>برفت از کار او یکباره سرپوش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز جا برجست و در پهلوش بنشست</p></div>
<div class="m2"><p>سخن بشنید از او خاموش بنشست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سراپا دیده شد تا بیندش روی</p></div>
<div class="m2"><p>شود همدم به آن لعل سخنگوی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ولی از شرم سر بالا نمی‌کرد</p></div>
<div class="m2"><p>نظر بر آن رخ زیبا نمی کرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مراد خویشتن با او نمی‌گفت</p></div>
<div class="m2"><p>سخن در آن رخ نیکو نمی‌گفت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو شیرین اینچنینش دید ، در دم</p></div>
<div class="m2"><p>به ساقی گفت می درده دمادم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دمی از باده ما را آزمون آر</p></div>
<div class="m2"><p>ز وسواس خردمندی برون آر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حکیمان را براین گفت اتفاق است</p></div>
<div class="m2"><p>که اندر بزم هشیاران نفاق است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز عقل دوربین دوریم ازعیش</p></div>
<div class="m2"><p>ز دانش سخت مهجوریم از عیش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خوشا مستی و صدق می پرستان</p></div>
<div class="m2"><p>که نی سالوس دانند و نه دستان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شنید از وی چو ساقی جام پر کرد</p></div>
<div class="m2"><p>قدح را پخته باز از خام پر کرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گرفت و خورد دردیهای آن جام</p></div>
<div class="m2"><p>نصیب کوهکن آمد سرانجام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو سور یار شیرین خورد فرهاد</p></div>
<div class="m2"><p>ز قید خو بکلی گشت آزاد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه یاد خویش ، نی بیگانه ماندش</p></div>
<div class="m2"><p>نه صبر اندر دل دیوانه ماندش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به روی یار شیرین شد غزلخوان</p></div>
<div class="m2"><p>کتاب عشق را بگشود عنوان</p></div></div>