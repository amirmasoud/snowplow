---
title: >-
    بخش ۳۸ - در بیان وصل و هجران نکویان و رفتن شیرین به تماشای بیستون
---
# بخش ۳۸ - در بیان وصل و هجران نکویان و رفتن شیرین به تماشای بیستون

<div class="b" id="bn1"><div class="m1"><p>بهر جا وصل از دوری نکوتر</p></div>
<div class="m2"><p>بجز یک جا که مهجوری نکوتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رهد عطشان ز مردن آب خوردن</p></div>
<div class="m2"><p>بجز یک جا که بهتر تشنه مردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه جا آنجا که یار آید ز در باز</p></div>
<div class="m2"><p>برای آنکه بر دشمن کند ناز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز یاران رنج به کاو بر تن آید</p></div>
<div class="m2"><p>که بهر گوشمال دشمن آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غذا به گر خورم از پهلوی خویش</p></div>
<div class="m2"><p>کز آن گسترده خوان بهر بداندیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ار خون جگر باشد به جامم</p></div>
<div class="m2"><p>که ریزد ساغر غیری به کامم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز شبهای سیه چندان نسوزم</p></div>
<div class="m2"><p>که شمع از آتش غیری فروزم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولی غیرت هم از راهی نه نیکوست</p></div>
<div class="m2"><p>کدام است آنکه بربندیم بر دوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو آمد یار خوش بر روی اوباش</p></div>
<div class="m2"><p>به رغم هر که خواهد باش گو باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به کام تشنه وانگه آب حیوان</p></div>
<div class="m2"><p>هلاک آن دل کز او برگیری آسان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به ساغر کوثر و دلدار ساقی</p></div>
<div class="m2"><p>حرام آن قطره‌ای کاو مانده باقی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو عمر رفته را بخت آورد باز</p></div>
<div class="m2"><p>از آن بدبخت‌تر کو کورد باز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز شیرین کوهکن را جام لبریز</p></div>
<div class="m2"><p>بهانه گو شکر گو باش پرویز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به کوه این نامراد سنگ فرسای</p></div>
<div class="m2"><p>به نقش پای شیرین چشم تر سای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز درد جان گداز و آه دل سوز</p></div>
<div class="m2"><p>ز شب روزش بتر بودی شب از روز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه شب از غم جانان نخفتی</p></div>
<div class="m2"><p>خیالش پیش چشم آورده گفتی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که او از یاد ناشادم نرفته</p></div>
<div class="m2"><p>ز چشم ار رفته از یادم نرفته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز جان از تاب زلفم تاب برده</p></div>
<div class="m2"><p>ز چشم ار چشم مستم خواب برده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نگفتی چون برفتم کیم از ناز</p></div>
<div class="m2"><p>نگفتم عمر رفته نایدم باز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نگفتی با وفا طبعم قرین است</p></div>
<div class="m2"><p>نگفتم عادت بختم نه این است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نگفتی گشت خواهم آشنامن</p></div>
<div class="m2"><p>نگفتم راست است اما نه بامن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نگفتی دل ستانم جانت به خشم</p></div>
<div class="m2"><p>نگفتی این نبخشی و آنت به خشم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نگفتی راز تو با کس نگویم</p></div>
<div class="m2"><p>نگفتم گویی اما پیش رویم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نگفتی خسروان از من به تابند</p></div>
<div class="m2"><p>نگفتم ره نشینان تا چه یابند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نگفتی یکدلم با ره نشینان</p></div>
<div class="m2"><p>نگفتم پیش آنان وای اینان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نکردی آنچه نیرنگت بیاراست</p></div>
<div class="m2"><p>بیا تا آنچه گفتم بنگری راست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به وصل خود نگشتی رهنمونم</p></div>
<div class="m2"><p>بیا بنگر که از هجر تو چونم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو بنشستی به دلخواهی به پیشم</p></div>
<div class="m2"><p>بیا بنگر به دلخواهی خویشم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ببین از درد هجرم در تب و تاب</p></div>
<div class="m2"><p>ز چشم و دل درون آتش و آب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مرا گفتی چو دل در عشق بندی</p></div>
<div class="m2"><p>دهد عشقت به آخر سر بلندی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بلندی داده عشق ارجمندم</p></div>
<div class="m2"><p>ولی تنها به این کوه بلندم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرا از بهر سختی آفریدند</p></div>
<div class="m2"><p>نخست این جامه را بر تن بریدند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شدم چون از بر مادر به استاد</p></div>
<div class="m2"><p>سر و کارم به سنگ افتاد و پولاد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همی بر سختیم سختی فزودند</p></div>
<div class="m2"><p>به بدبختیم بدبختی فزودند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدان سختی چو لختی چاره کردم</p></div>
<div class="m2"><p>ز آهن رخنه‌ها در خاره کردم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>فتادم با دلی سنگین سر و کار</p></div>
<div class="m2"><p>که آسان کرد پیشم هر چه دشوار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کجا آهن که با این سخت جانم</p></div>
<div class="m2"><p>اگر کوشم در او راهی ندانم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بسی خارا به آهن سوده کردم</p></div>
<div class="m2"><p>از این خارا روان فرسوده کردم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نگارا وقت دمسازیست بازآ</p></div>
<div class="m2"><p>مرا هنگام جانبازیست بازآ</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>که از جان طاقت از تن تاب رفته</p></div>
<div class="m2"><p>در این جو مانده ماهی آب رفته</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بر این کهسار تاب ای ماهتابم</p></div>
<div class="m2"><p>فرو نارفته از کوه آفتابم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همی ترسم که ای جان جهانم</p></div>
<div class="m2"><p>نیایی ور رود بر باد جانم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر از جان دادنم بیمی‌ست زان است</p></div>
<div class="m2"><p>که جان بهر نثار دلستان است</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به سختی با اجل زان می‌ستیزم</p></div>
<div class="m2"><p>که باز آیی و جان بر پات ریزم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به هجران سخت باشد زندگانی</p></div>
<div class="m2"><p>به امید تو کردم سخت جانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>اجل را می‌دهم هر دم فریبی</p></div>
<div class="m2"><p>مگر یابم ز دیدارت نصیبی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به حیلت روزگاری می‌گذارم</p></div>
<div class="m2"><p>که جان در پای دلداری سپارم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چه بودی طالعم دمساز گشتی</p></div>
<div class="m2"><p>که جان رفته از تن بازگشتی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>زمانی روی گلگون کن بدین سوی</p></div>
<div class="m2"><p>ز گردش بخت را گلگونه کن روی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>براین کوه ار شدی آن برق رفتار</p></div>
<div class="m2"><p>چو برقی کاو فرود آید ز کهسار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>وگر از نعل او فرسودی این کوه</p></div>
<div class="m2"><p>ز من برخاستی این کوه اندوه</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نمی گویم کزین کارم نفور است</p></div>
<div class="m2"><p>به کار سخت همدستی ضرور است</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گرم همدست سازی پای گلگون</p></div>
<div class="m2"><p>کنم این کوه را یک لحظه هامون</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خیالت گر چه‌ای بیگانه کیشم</p></div>
<div class="m2"><p>نخست آمد به همدستی خویشم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ولی چندان فریب و ناز دارد</p></div>
<div class="m2"><p>که از شوخی ز کارم باز دارد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چنین می‌گفت و خون دیده باران</p></div>
<div class="m2"><p>از آن کهسار چون سیل بهاران</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>زمانی دیده بست و بیخود افتاد</p></div>
<div class="m2"><p>چو دید آن دم که از هم دیده بگشاد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به نام ایزد یکی دشت از غزالان</p></div>
<div class="m2"><p>همه بالا بلندان خردسالان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همه در زیر چتر از تابش خور</p></div>
<div class="m2"><p>چو تاووسان چتر آورده بر سر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>در فردوس را گفتی گشادند</p></div>
<div class="m2"><p>که آن حورا وشان بیرون فتادند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>همه صید افکنان در راه و بیراه</p></div>
<div class="m2"><p>کمند زلفشان بر گردن ماه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>همه گلچهرگان با زلف پرچین</p></div>
<div class="m2"><p>از ایشان دشت چون دامان گلچین</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سگ افکن در پی آهو به هر سو</p></div>
<div class="m2"><p>همه در پویه چون سگ دیده آهو</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز مژگان چنگل شاهین گشاده</p></div>
<div class="m2"><p>چو شاهین در پی کبکان فتاده</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شراب لاله گونشان در پیاله</p></div>
<div class="m2"><p>همه صحرا تو گفتی رسته لاله</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>زمین از رویشان همچون گلستان</p></div>
<div class="m2"><p>هوا از مویشان چون سنبلستان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بت گلگون سوار اندر میانه</p></div>
<div class="m2"><p>روان را آرزو دل را بهانه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز مژگان رخنه کن در خانهٔ دل</p></div>
<div class="m2"><p>ز صورت شعله زن در خانه زین</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خرد زنجیری زلف بلندش</p></div>
<div class="m2"><p>سر زنجیر مویان در کمندش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>قمر از پیشکاران جمالش</p></div>
<div class="m2"><p>جنون از دستیاران خیالش</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بلا را دیده بر فرمان بالاش</p></div>
<div class="m2"><p>اجل را گوش بر حکم تقاضاش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>نگاه فتنه بر چشمان مستش</p></div>
<div class="m2"><p>فلک را دست بیرحمی به دستش</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>دل آشوبی ز همکاران مویش</p></div>
<div class="m2"><p>جهانسوزی ز همدستان خویش</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>شه از گنج گهر او را خریدار</p></div>
<div class="m2"><p>فقیر از آه شبگیرش طلبکار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به آن از زلف طوق بندگی نه</p></div>
<div class="m2"><p>به این از لب شراب زندگی ده</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو چشم افتاد به روی کوهکن را</p></div>
<div class="m2"><p>همی مالید چشم خویشتن را</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>به خود می‌گفت کاین آن سرونازست</p></div>
<div class="m2"><p>که شاهان را به وصل او نیاز است ؟</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>که شد سوی گدایان رهنمونش</p></div>
<div class="m2"><p>که ره بنمود سوی بیستونش ؟</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>کدام استاد این افسونگری کرد ؟</p></div>
<div class="m2"><p>که این افسون به کار آن پری کرد ؟</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>که راهش زد که اندر راهش آورد ؟</p></div>
<div class="m2"><p>به من چون دولت ناگاهش آورد ؟</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>کرا تاب کمند آمد بر افلاک ؟</p></div>
<div class="m2"><p>که ماه آسمان افکند بر خاک ؟</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>مگر راه سپهر خویش دارد</p></div>
<div class="m2"><p>که ره بر این بلندی پیش دارد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>در این بد کآمد از آن دلفریبان</p></div>
<div class="m2"><p>بتی چون سوی رنجوران طبیبان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>پی آگاهی فرهاد مسکین</p></div>
<div class="m2"><p>فرستادش مگر بانوی شیرین</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>سخنهایی که بود از بیش و کم گفت</p></div>
<div class="m2"><p>برهمن را ز آهنگ صنم گفت</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>حدیث نامهٔ شاه جهان را</p></div>
<div class="m2"><p>جواب نامهٔ سرو روان را</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گر از خود یا از آن شیرین دهن گفت</p></div>
<div class="m2"><p>تمامی را به گوش کوهکن گفت</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>از آن گفت و شنو بیچاره فرهاد</p></div>
<div class="m2"><p>به جایی شد که چشم کس مبیناد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>تنش گفتی ز بس تاب و تب آورد</p></div>
<div class="m2"><p>نثار پای گلگون بر لب آورد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>چو سیلاب از سر کوه آن یگانه</p></div>
<div class="m2"><p>به استقبال شیرین شد روانه</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>شکر لب یافت اندر نیمه راهش</p></div>
<div class="m2"><p>به سد شیرینی آمد عذر خواهش</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>به کوه آمد نگار لاله رخسار</p></div>
<div class="m2"><p>چو خورشیدی که او تابد به کهسار</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>رسید آنجا که مرد آهنین دست</p></div>
<div class="m2"><p>به کوه آن نقشهای طرفه بر بست</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>رسید آنجا که عشق سخت بازو</p></div>
<div class="m2"><p>به کوه افکنده بد غارت به نیرو</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>شده سد پاره کوه از عشق پر زور</p></div>
<div class="m2"><p>بدانسان کز تجلی سینه طور</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>چو پیش آمد رواقی دید عالی</p></div>
<div class="m2"><p>که کردش دست عشق از سنگ خالی</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>شکسته طاق چرخ دیر بنیاد</p></div>
<div class="m2"><p>به زیرش طاق دیگر بسته فرهاد</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>همی شد تا به سنگی شد مقابل</p></div>
<div class="m2"><p>که بر تمثال آن شیرین شمایل</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>بگفت این سینهٔ فرهاد زار است</p></div>
<div class="m2"><p>که در وی نقش شیرین آشکار است</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>به زلف خویش دستی زد پریوش</p></div>
<div class="m2"><p>نگشت از حال خود آن نقش دلکش</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>از آنجا یافت کان تمثال خویش است</p></div>
<div class="m2"><p>که احوالش نه چون احوال خویش است</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>و یا استاد چینی کرده نیرنگ</p></div>
<div class="m2"><p>یکی آیینه بنموده‌ست از سنگ</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>تبسم را درون سینه ره داد</p></div>
<div class="m2"><p>به صنعت پیشه مزد از یک نگه داد</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>به شوخی گفت کای مرد هنرور</p></div>
<div class="m2"><p>تو گویی بوده شیرینت برابر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>مرا خود یک نظر افزون ندیدی</p></div>
<div class="m2"><p>چسان این صورت دلکش کشیدی</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>اگر گویم هنر بود این هنر نیست</p></div>
<div class="m2"><p>چنین تمثال کار یک نظر نیست</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بگفت آن یک نظر از چشم دل بود</p></div>
<div class="m2"><p>از آنش دست هجران محو ننمود</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>چو دیدم بر رخت از دیدهٔ دل</p></div>
<div class="m2"><p>از آن دارم شب و روزت مقابل</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>بگفت این نقش بد گو را بهانه‌ست</p></div>
<div class="m2"><p>به بی پروایی شیرین بهانه‌ست</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>همی گوید که آن کاین نقش بسته‌ست</p></div>
<div class="m2"><p>چو دل شیرین به پهلویش نشسته‌ست</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>که کس نادیده نقش کس نپرداخت</p></div>
<div class="m2"><p>و گر پرداخت چو اصلش کجا ساخت</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بگفتا داند این کاندیشد این راز</p></div>
<div class="m2"><p>که این صورت که بر مه زیبدش ناز</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>برهر کس که جای از ناز دارد</p></div>
<div class="m2"><p>ز بس شوخی زکارش باز دارد</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>دلی از سنگ باید جانی از روی</p></div>
<div class="m2"><p>که پردازد به سنگ و تیشه زین روی</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چو شیرینش چنین بی خویشتن دید</p></div>
<div class="m2"><p>به بیهوشی صلاح کوهکن دید</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>بگفتا بایدش جامی که پیمود</p></div>
<div class="m2"><p>به مستی چند حرفی گفت و بشنود</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>اگر حرفی زند مستی بهانه‌ست</p></div>
<div class="m2"><p>توان گفت او به بد مستی نشانه‌ست</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>وزین غافل که عاشق چون شود مست</p></div>
<div class="m2"><p>لب از اسرار عشقش چون توان بست</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>مگر می‌خواست وصف نوگل خویش</p></div>
<div class="m2"><p>عیان تر بشنود از بلبل خویش</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>به دور آمد شرابی چون دل پاک</p></div>
<div class="m2"><p>روان افروز دور از هر هوسناک</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>میی سرمایه عشق جوانی</p></div>
<div class="m2"><p>کمین تعریفش آب زندگانی</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>به صافی چون عذار دلنوازان</p></div>
<div class="m2"><p>به تلخی روزگار عشقبازان</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>سراپا حکمت و آداب گشته</p></div>
<div class="m2"><p>فلاتونی‌ست در خم آب گشته</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>ادبها دیده از خردی زدهقان</p></div>
<div class="m2"><p>شده در خورد بزم پادشاهان</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>نخست آن مه به لعل آلوده یاقوت</p></div>
<div class="m2"><p>نمود از لعل تر یاقوت را قوت</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>از آن رو جام می جان پرور آمد</p></div>
<div class="m2"><p>که روزی بر لب آن دلبر آمد</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>چو جام از لعل او شد شکر آلود</p></div>
<div class="m2"><p>به آن تلخی کش ایام پیمود</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>چو جوش باده هوش از دل ربودش</p></div>
<div class="m2"><p>که چندان گشت آشوبی که بودش</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>جنون کش با خرد گرگ آشتی بود</p></div>
<div class="m2"><p>چو فرصت یافت بر وی دست بگشود</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>که بیرون شو ز سرکاین خانهٔ ماست</p></div>
<div class="m2"><p>نیاید صحبت عقل و جنون راست</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>خرد عشق و جنون را دید همدست</p></div>
<div class="m2"><p>از آن هنگامه رخت خویش بر بست</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>ادب را رفت گستاخی به سر نیز</p></div>
<div class="m2"><p>که گستاخی‌ست جا ننگ است برخیز</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>حجاب این کشمکش چون دید شد راست</p></div>
<div class="m2"><p>به او کس تا نگوید خیز برخاست</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>خرد با پیشکاران تا برون راند</p></div>
<div class="m2"><p>جنون با دستیاران در درون ماند</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>حجاب عقل رفت و جای آن بود</p></div>
<div class="m2"><p>حجاب عشق بر جا همچنان بود</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>حجاب عشق اگر از پیش خیزد</p></div>
<div class="m2"><p>به مردی کاب مردان را بریزد</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>چه غم گر عشق داور پرده رو نیست</p></div>
<div class="m2"><p>که خورشید است و چشم بد بر او نیست</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>ولی عشقی که نبود پرده‌اش پیش</p></div>
<div class="m2"><p>زیان بیند هم از چشم بد خویش</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>که عاشق چون نظر پرورده نبود</p></div>
<div class="m2"><p>همان بهتر که او بی‌پرده نبود</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>چو آتش عاشق آنگه رخ برافروخت</p></div>
<div class="m2"><p>که اول خویش و آنگه پرده را سوخت</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>از آتش سوختن از پرده پیش است</p></div>
<div class="m2"><p>که او خود پردهٔ سیمای خویش است</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>چو شیرین کوهکن را پرده در دید</p></div>
<div class="m2"><p>به شیرینی از او در پرده پرسید</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>که ای چینی نسب مرد هنرمند</p></div>
<div class="m2"><p>به چین با کیستت خویشی و پیوند</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>در آن شهری ز تخم سر بلندان</p></div>
<div class="m2"><p>و یا از خاندان مستمندان</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>تو با فرهنگ و رای مهترانی</p></div>
<div class="m2"><p>نپندارم که تخم کهترانی</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>نخستین روز کت پرسیدم از بوم</p></div>
<div class="m2"><p>نگردید از نژادت هیچ معلوم</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>همی خواهم که دست از شرم شویی</p></div>
<div class="m2"><p>نژاد خویشتن با من بگویی</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>دگر گفتش تو گویی بت پرستی</p></div>
<div class="m2"><p>کت اندر بت تراشی هست دستی</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>بسی نقش است در این کوه خارا</p></div>
<div class="m2"><p>نباشد همچو این صورت دل آرا</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>بدو فرهاد گفت آری چنین است</p></div>
<div class="m2"><p>ز چینم بت پرستی کار چین است</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>تو ای بت‌گر به چین منزل گزینی</p></div>
<div class="m2"><p>به غیر از بت پرستی می نبینی</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>چنین می رفت در اندیشهٔ من</p></div>
<div class="m2"><p>کز اول روز دانی پیشهٔ من</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>ولی معذوری ای سرو سمن سا</p></div>
<div class="m2"><p>که یک سرداری و سد گونه سودا</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>صنم ازناز دستی برد بر روی</p></div>
<div class="m2"><p>به سد ناز و کرشمه گفت با اوی</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>که ای از تیشه رکش کلک مانی</p></div>
<div class="m2"><p>ترا بینم به مزدوران نمانی</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>غریبی پیشه ور از کارفرما</p></div>
<div class="m2"><p>ز سودای زر و نه فکر کالا</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>اگر روی زمین گردد پر از در</p></div>
<div class="m2"><p>ترا بینم که چشم دل بود پر</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>همه گوهر ز نوک تیشه داری</p></div>
<div class="m2"><p>نخواهی زر چه در اندیشه داری</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>چنین بی‌مزد این زحمت کشیدن</p></div>
<div class="m2"><p>مرا بار آورد خجلت کشیدن</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>کشی رنج و هوای زر نداری</p></div>
<div class="m2"><p>اگر رنج دو روزه بود باری</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>کرا داری بگو در کشور خویش</p></div>
<div class="m2"><p>که نه داری سر او نه سر خویش</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>به حق آشنایی ها که پیشم</p></div>
<div class="m2"><p>سراسر شرح ده احوال خویشم</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>از این گفتار فرهاد هنرمند</p></div>
<div class="m2"><p>به خود پیچد و خامش ماند یکچند</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>وزان پس شرح غم با نازنین گفت</p></div>
<div class="m2"><p>چنین شیرین نگفت اما چنین گفت</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>که ای لعلت زبانم برده از کار</p></div>
<div class="m2"><p>زبانت بازم آورده به گفتار</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>چه می‌پرسی که تاب گفتنم نیست</p></div>
<div class="m2"><p>و گر چه هم دل بنهفتنم نیست</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>شنیدم ای نگار لاله رخسار</p></div>
<div class="m2"><p>دلی داری غمین جانی پرآزار</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>گلت پژمرده و طبعت فسرده‌ست</p></div>
<div class="m2"><p>که سودا در مزاجت راه برده‌ست</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>به حیلت کوه و صحرا می‌سپاری</p></div>
<div class="m2"><p>که یک دم خاطری مشغول داری</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>چه باید بر سر غم غم نهادن</p></div>
<div class="m2"><p>به فکر غم کشی چون من فتادن</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>به چنگ و باده ده خود را شکیبی</p></div>
<div class="m2"><p>نه از درد دل چون من غریبی</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>ولی گویم به پیشت مشکل خویش</p></div>
<div class="m2"><p>به امیدی که بگشایی دل خویش</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>مگو از غم، ره غم چون توان بست</p></div>
<div class="m2"><p>که می گویند خون با خون توان بست</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>نگویم کز غمم آزاد سازی</p></div>
<div class="m2"><p>که از غم خاطر خود شاد سازی</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>بدان ای گل عذار مه جبینم</p></div>
<div class="m2"><p>که من شهزادهٔ اقلیم چینم</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>من از چینم همه چین بت پرستند</p></div>
<div class="m2"><p>چو من یک تن ز دام بت نرستند</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>مرا مادر پدر بودند خرسند</p></div>
<div class="m2"><p>ز هر کام از جهان الا ز فرزند</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>پدر گفته‌ست روزی با برهمن</p></div>
<div class="m2"><p>که گر بت سازدم این دیده روشن</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>به فرزندی نماید سرفرازم</p></div>
<div class="m2"><p>مر او را خادم بت خانه سازم</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>چنان گفت و چنان گشت و چنان کرد</p></div>
<div class="m2"><p>مرا شش ساله در بتخانه آورد</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>یکی بتگر در آنجا رشک آذر</p></div>
<div class="m2"><p>مرا افتاد خو با مرد بتگر</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>چو بت می کردم از جان خدمت او</p></div>
<div class="m2"><p>که بد میل دلم با صنعت او</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>از آن خدمت روان او برافروخت</p></div>
<div class="m2"><p>هر آن صنعت که بودش با من آموخت</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>برهمن بت تراشی داد یادم</p></div>
<div class="m2"><p>بماند آن خوی طفلی در نهادم</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>چو از چشم محبت سوی من دید</p></div>
<div class="m2"><p>چنان گشتم که استادم پسندید</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>بتی باری به سنگی نقش بستم</p></div>
<div class="m2"><p>ربود آن بت عنان دل ز دستم</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>شب و روزم سر اندر پای او بود</p></div>
<div class="m2"><p>سرم پیوسته پر سودای او بود</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>بسی گشتم که او را زنده بینم</p></div>
<div class="m2"><p>به جان آن گوهر ارزنده بینم</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>ندیدم در همه چین همچو اویی</p></div>
<div class="m2"><p>شدم شیدایی و آشفته خویی</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>از آن آشوب بی اندازه من</p></div>
<div class="m2"><p>همه چین گشت پرآوازه من</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>همه گفتند شادان نیک بختی</p></div>
<div class="m2"><p>زباغ خسروی خرم درختی</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>کش اول بت می صورت چشاند</p></div>
<div class="m2"><p>به معنی بازش از صورت کشاند</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>همه بامن نیاز آغاز کردند</p></div>
<div class="m2"><p>مرا از همگنان ممتاز کردند</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>برهمن چون مرا بی خویشتن دید</p></div>
<div class="m2"><p>مرا همچون صنم خود را شمن دید</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>من از سودای بت ز آنگونه گشته</p></div>
<div class="m2"><p>که فرش بت پرستی در نوشته</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>هجوم خلق و عشق بت چنان کرد</p></div>
<div class="m2"><p>که دورم عاقبت از خانمان کرد</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>سفر کردم ز صورت سوی معنی</p></div>
<div class="m2"><p>ترا دیدم بدیدم روی معنی</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>چه بودی باز چشمش بازگشتی</p></div>
<div class="m2"><p>هم از صورت به معنی بازگشتی</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>وصال از دیدهٔ جانت گشاده‌ست</p></div>
<div class="m2"><p>ترا نیز اینچنین کاری فتاده‌ست</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>هوس‌های دل دیوانه تو</p></div>
<div class="m2"><p>همه بت بوده در بتخانهٔ تو</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>خیال منصب و ملک و زن ومال</p></div>
<div class="m2"><p>هوای عزت و سلمن و اقبال</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>هنرهایی که بود آخر و بالت</p></div>
<div class="m2"><p>سراسر نقص می‌دیدی کمالت</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>همه چون بت پرستی‌های خامه</p></div>
<div class="m2"><p>سیاه از وی چو بختت روی نامه</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>چو با عشق بتان افتاد کارت</p></div>
<div class="m2"><p>شرابی شد پی دفع خمارت</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>ز صورت های بی معنی رمیدی</p></div>
<div class="m2"><p>چنان دیدی که در معنی رسیدی</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>بسی از سخت گوییهای اغیار</p></div>
<div class="m2"><p>به سنگ و آهن افتادت سر و کار</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>بسی آه نفس را گرم کردی</p></div>
<div class="m2"><p>که تا سنگین دلی را نرم کردی</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>بر دلها بسی رفتی به زاری</p></div>
<div class="m2"><p>که نقش مهر بر سنگی نگاری</p></div></div>
<div class="b" id="bn209"><div class="m1"><p>جفاها دیدی از بیگانه و خویش</p></div>
<div class="m2"><p>ز جور دلبر و کین بداندیش</p></div></div>
<div class="b" id="bn210"><div class="m1"><p>که گردیدی و سنجیدی کنونش</p></div>
<div class="m2"><p>فزودن دیدی زکوه بیستونش</p></div></div>
<div class="b" id="bn211"><div class="m1"><p>لبی دیدی که از شیرین کلامی</p></div>
<div class="m2"><p>شکر را داده فتوا بر حرامی</p></div></div>
<div class="b" id="bn212"><div class="m1"><p>رخی دیدی که خورشید سحر تاب</p></div>
<div class="m2"><p>چو نیلوفر ز عشقش رفته در تاب</p></div></div>
<div class="b" id="bn213"><div class="m1"><p>بدیدی مویی آتش پرور عشق</p></div>
<div class="m2"><p>هزاران خسرو اندر چنبر عشق</p></div></div>
<div class="b" id="bn214"><div class="m1"><p>قدی دیدی خرام آهو زشمشاد</p></div>
<div class="m2"><p>به رعنایی غلامش سرو آزاد</p></div></div>
<div class="b" id="bn215"><div class="m1"><p>تذروی دیدی از وی باغ رنگین</p></div>
<div class="m2"><p>خضاب چنگلش از خون شاهین</p></div></div>
<div class="b" id="bn216"><div class="m1"><p>غزالی دیدی از وی دشت را زیب</p></div>
<div class="m2"><p>و زو بر پهلوی شیران سد آسیب</p></div></div>
<div class="b" id="bn217"><div class="m1"><p>بهشتی دیدی از وی کلبه معمور</p></div>
<div class="m2"><p>سرا پا رشک غلمان ، غیرت حور</p></div></div>
<div class="b" id="bn218"><div class="m1"><p>اگر چه آن هم از صورت اثر داشت</p></div>
<div class="m2"><p>ولیکن ره بمعنی بیشتر داشت</p></div></div>
<div class="b" id="bn219"><div class="m1"><p>اگر چه نقش آن صورت زدت راه</p></div>
<div class="m2"><p>ولی جانت ز معنی بود آگاه</p></div></div>
<div class="b" id="bn220"><div class="m1"><p>ترا گر نی دل و گردیده بودی</p></div>
<div class="m2"><p>چو فرهادش به معنی دیده بودی</p></div></div>
<div class="b" id="bn221"><div class="m1"><p>برو شکری کن ار دردی رسیدت</p></div>
<div class="m2"><p>که آخر چاره از مردی رسیدت</p></div></div>
<div class="b" id="bn222"><div class="m1"><p>که معنی‌های مردم صورت اوست</p></div>
<div class="m2"><p>جنون سرمست جام حیرت اوست</p></div></div>
<div class="b" id="bn223"><div class="m1"><p>هر آن معنی که صورت را مقابل</p></div>
<div class="m2"><p>کجا بند صور بگشاید از دل</p></div></div>
<div class="b" id="bn224"><div class="m1"><p>چو بحر معنی آید در تلاطم</p></div>
<div class="m2"><p>شود این صورت معنی در او گم</p></div></div>
<div class="b" id="bn225"><div class="m1"><p>در این معنی کسی کاو را نه دعوی‌ست</p></div>
<div class="m2"><p>یقین داند که صورت عین معنی‌ست</p></div></div>
<div class="b" id="bn226"><div class="m1"><p>به نام خالق پیدا و پنهان</p></div>
<div class="m2"><p>که پیدا و نهان داند به یکسان</p></div></div>
<div class="b" id="bn227"><div class="m1"><p>در گنج سخن را می‌کنم باز</p></div>
<div class="m2"><p>جهان پر سازم از درهای ممتاز</p></div></div>
<div class="b" id="bn228"><div class="m1"><p>حدیثی را که وحشی کرده عنوان</p></div>
<div class="m2"><p>وصالش نیز ناورده به پایان</p></div></div>
<div class="b" id="bn229"><div class="m1"><p>به توفیق خداوند یگانه</p></div>
<div class="m2"><p>به پایان آرم آن شیرین فسانه</p></div></div>
<div class="b" id="bn230"><div class="m1"><p>که کس انجام آن نشیند از کس</p></div>
<div class="m2"><p>که در ضمن سخن گفتندشان بس</p></div></div>
<div class="b" id="bn231"><div class="m1"><p>حکایتها میان آن دو رفته‌ست</p></div>
<div class="m2"><p>که نه آن دیده کس ، نی آن شنفته‌ست</p></div></div>
<div class="b" id="bn232"><div class="m1"><p>شبی در خواب فرهاد آن به من گفت</p></div>
<div class="m2"><p>که چشمم زیر کوه بیستون خفت</p></div></div>
<div class="b" id="bn233"><div class="m1"><p>که آن افسانه کس نشنیده از کس</p></div>
<div class="m2"><p>که من خواهم که بنیوشند از این پس</p></div></div>
<div class="b" id="bn234"><div class="m1"><p>ز وحشی دید یاری روی یاری</p></div>
<div class="m2"><p>وصالش داشت از یاری به کاری</p></div></div>
<div class="b" id="bn235"><div class="m1"><p>بسی در معانی هردو سفتند</p></div>
<div class="m2"><p>به مقداری که بد مقدور ، گفتند</p></div></div>
<div class="b" id="bn236"><div class="m1"><p>به نام خسرو و فرهاد و شیرین</p></div>
<div class="m2"><p>بیان عشق را بستند آیین</p></div></div>
<div class="b" id="bn237"><div class="m1"><p>ولی ز آن قصه چیزی بود باقی</p></div>
<div class="m2"><p>که پرشد ساغر هر دو ز ساقی</p></div></div>
<div class="b" id="bn238"><div class="m1"><p>ز دور جام مردافکن فتادند</p></div>
<div class="m2"><p>سخن از لب ، ز کف خامه نهادند</p></div></div>
<div class="b" id="bn239"><div class="m1"><p>شدند اندر هوای وصل جانان</p></div>
<div class="m2"><p>به گیتی یادگاری ماند از آنان</p></div></div>
<div class="b" id="bn240"><div class="m1"><p>کنون آن خامه در دست من افتاد</p></div>
<div class="m2"><p>که آرد قصه‌ای شیرین ز فرهاد</p></div></div>
<div class="b" id="bn241"><div class="m1"><p>چو شرح حال خود را کوهکن گفت</p></div>
<div class="m2"><p>ندانی پاسخش چون زان دهن گفت</p></div></div>
<div class="b" id="bn242"><div class="m1"><p>وصال اینجا سخن را بس نموده‌ست</p></div>
<div class="m2"><p>نقاب از چهرهٔ جان بس نموده‌ست</p></div></div>
<div class="b" id="bn243"><div class="m1"><p>ز صابر بشنو آن پاسخ که او داد</p></div>
<div class="m2"><p>که بس کام از لبش زان گفتگو داد</p></div></div>