---
title: >-
    بخش ۸ - حکایت
---
# بخش ۸ - حکایت

<div class="b" id="bn1"><div class="m1"><p>به حربا گفت خفاشی که تا چند</p></div>
<div class="m2"><p>سوی خورشید بینی دیده دربند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازین پیکر که سازد چشم خیره</p></div>
<div class="m2"><p>چرا عالم کنی بر خویش تیره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز نشترهاش کاو الماس دیده‌ست</p></div>
<div class="m2"><p>به غیر از تیرگی چشمت چه دیده‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه دیدی کاینچنین بی‌تابی از وی</p></div>
<div class="m2"><p>تپان چون ماهی بی‌آبی از وی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا جا در مغاک ، او را در افلاک</p></div>
<div class="m2"><p>برو کوتاه کن دستش ز فتراک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو پروانه طلب یاری که آن یار</p></div>
<div class="m2"><p>گهی پیرامن خویشت دهد بار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نیلوفر از این سودای باطل</p></div>
<div class="m2"><p>نمی‌دانم چه خواهی کرد حاصل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگفتش کوتهی افسوس افسوس</p></div>
<div class="m2"><p>تو پا می‌بینی و من پر تاووس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو شبهای سیه دیدی چه دانی</p></div>
<div class="m2"><p>فروغ این چراغ آسمانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرت روشن شدی یک چشم سوزن</p></div>
<div class="m2"><p>بر او می‌دوختی سد دیده چون من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو می پیما سواد شام دیجور</p></div>
<div class="m2"><p>نداری کفه میزان این نور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ترازویی که باشد بهر انگشت</p></div>
<div class="m2"><p>بود سنجیدن کافور از او زشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همین بس حاصلم زین شغل سازی</p></div>
<div class="m2"><p>که با خورشید دارم عشقبازی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازین به دولتی خواهم در ایام</p></div>
<div class="m2"><p>که تا خورشید باشد باشدم نام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیا وحشی ز حربایی نیی کم</p></div>
<div class="m2"><p>که شد این نسبت و نامش مسلم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به خورشید سخن نه دیدهٔ دل</p></div>
<div class="m2"><p>مشو خفاش ظلمت خانه گل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر این نسبت بیابی تا به جاوید</p></div>
<div class="m2"><p>بماند سکه‌ات بر نقد خورشید</p></div></div>