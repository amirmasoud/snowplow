---
title: >-
    بخش ۳۳ - در حکایت گفتگوی آن بی‌خبر از مقامات عشق با مجنون و جواب دادن مجنون
---
# بخش ۳۳ - در حکایت گفتگوی آن بی‌خبر از مقامات عشق با مجنون و جواب دادن مجنون

<div class="b" id="bn1"><div class="m1"><p>شنیدم عاقلی گفتا به مجنون</p></div>
<div class="m2"><p>که برخود عشق را بستی به افسون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که عاشق لاغر است و زرد و دلتنگ</p></div>
<div class="m2"><p>ترا تن فربه است و چهره گلرنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوابش داد آن دلدادهٔ عشق</p></div>
<div class="m2"><p>به غرقاب فنا افتادهٔ عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که بینی هرکجا رنجور عاشق</p></div>
<div class="m2"><p>نباشد عشق با طبعش موافق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا این عاشقی دلکش فتاده‌ست</p></div>
<div class="m2"><p>محبت با مزاحم خوش فتاده‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به طبع آتشین ناخوش نماید</p></div>
<div class="m2"><p>که عشق آبست اگر آتش نماید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو من در عاشقی چون خاک پستم</p></div>
<div class="m2"><p>کجا از آب عشق آید شکستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چهرم چو گل بینی چه باک است</p></div>
<div class="m2"><p>نبینی کاصل گل از آب و خاک است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو نیز ای در خمار از بادهٔ عشق</p></div>
<div class="m2"><p>مزاج خویش کن آماده عشق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که چون عشق گرامی سرخوش افتد</p></div>
<div class="m2"><p>به طبعت سرکشیهایش خوش افتد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سخن را تاکنون پیرایه‌ای بود</p></div>
<div class="m2"><p>که با صاحب سخن سرمایه‌ای بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن گفتار شیرین میسرودم</p></div>
<div class="m2"><p>کزان لبهای شیرین می‌شنودم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کنون می‌بایدم خاموش بنشست</p></div>
<div class="m2"><p>که دلدارم لب از گفتار بربست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>و گر گویم هم از خود باز گویم</p></div>
<div class="m2"><p>حدیث از طالع ناساز گویم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز دلبر گویم و ناسازگاریش</p></div>
<div class="m2"><p>هم از دل گویم و افغان و زاریش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز جانان گویم و پیوند سستش</p></div>
<div class="m2"><p>هم از دل گویم و عهد درستش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که دیده‌ست اینچنین یار جفاکیش</p></div>
<div class="m2"><p>جفای او همه با بیدل خویش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که دیده‌ست اینچنین ماه دل آزار</p></div>
<div class="m2"><p>ستیز او همه با عاشق زار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برید از خلق پیوندم به یکبار</p></div>
<div class="m2"><p>که جای مست دل با غیر مگذار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو دل خالی شد از هر خویش و پیوند</p></div>
<div class="m2"><p>بگفتا هم تو رخت خویش بربند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که من خوش دارم از تنها نشینی</p></div>
<div class="m2"><p>که تنها باشم اندر نازنینی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فریب او ز خویش آواره ام ساخت</p></div>
<div class="m2"><p>چنین بی خانمان بیچاره‌ام ساخت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کنون با هر که بینم سازگار است</p></div>
<div class="m2"><p>ز پیوند منش ننگ است و عار است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو گل با هر خس و خاری قرین است</p></div>
<div class="m2"><p>چو با من می‌رسد خلوت نشین است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به من سرد است و با دشمن به جوش است</p></div>
<div class="m2"><p>باو در گفتگو، با من خموش است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نمی‌پرسد ز شبهای درازم</p></div>
<div class="m2"><p>نمی‌بیند به اندوه و گدازم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نمی‌گوید اسیری داشتم کو</p></div>
<div class="m2"><p>به حرمان دستگیری داشتم کو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نپرسد تا ز من بیند خبر نیست</p></div>
<div class="m2"><p>نجوید تا ز من یابد اثر نیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نبیند تا ببیند غرق خونم</p></div>
<div class="m2"><p>نگوید تا بگویم بی تو چونم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نخواند تا بخوانم شرح هجران</p></div>
<div class="m2"><p>نیاید تا زنم دستش به دامان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه چون مینا درآید در کنارم</p></div>
<div class="m2"><p>نه چون ساغر کند دفع خمارم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه چون چنگم نوازد تا خروشم</p></div>
<div class="m2"><p>نه چون بربط خروشد تا بجوشم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>لبش برلب نه تا چون نی بنالم</p></div>
<div class="m2"><p>ز اندوه و فراق وی بنالم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نه دستی تا که خار از پا در آرم</p></div>
<div class="m2"><p>نه پایی تا ره کویش سپارم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نه دینی تا باو در بند باشم</p></div>
<div class="m2"><p>دمی از طاعتی خرسند باشم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کنون این بی دل و دینم که بینی</p></div>
<div class="m2"><p>حکایت مختصر اینم که بینی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عجب تر آنکه گر غیرت گذارد</p></div>
<div class="m2"><p>که دل شرحی ز جورش برشمارد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز بیم رنجش آن طبع سرکش</p></div>
<div class="m2"><p>زنم از دل به کلک و دفتر آتش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همان بهتر که باز افسانه خوانم</p></div>
<div class="m2"><p>ز حال خود سخن در پرده رانم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بیا ساقی از آن صهبای دلکش</p></div>
<div class="m2"><p>بزن آبی بر این جان پرآتش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که طبع آتشین چون خوش فروزد</p></div>
<div class="m2"><p>مبادا در جهان آتش فروزد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شرابی ده چو روی خرم دوست</p></div>
<div class="m2"><p>به دل شادی فزا یعنی غم دوست</p></div></div>