---
title: >-
    بخش ۱۷ - گفتار در رفتار خادمان شیرین به طلب نزهتگاه دلنشین و پیدا نمودن دشت بیستون و خبردادن شیرین را
---
# بخش ۱۷ - گفتار در رفتار خادمان شیرین به طلب نزهتگاه دلنشین و پیدا نمودن دشت بیستون و خبردادن شیرین را

<div class="b" id="bn1"><div class="m1"><p>خوشا خاکی و خوش آب و هوایی</p></div>
<div class="m2"><p>که افتد قابل طرح وفایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشا سرمنزلی خوش سرزمینی</p></div>
<div class="m2"><p>که باشد لایق مسند نشینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب جایی بباید بهجت‌انگیز</p></div>
<div class="m2"><p>که بر شیرین سرآرد هجر پرویز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملال خاطر شیرین چو دیدند</p></div>
<div class="m2"><p>پرستاران جنیبت‌ها کشیدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کوه و دشت میراندند ابرش</p></div>
<div class="m2"><p>مراد خاطر شیرین عنان کش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر آهویی بدیدندی به راغی</p></div>
<div class="m2"><p>از آن آهو گرفتندی سراغی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کبکی گر رسیدندی به دشتی</p></div>
<div class="m2"><p>بپرسیدند از وی سرگذشتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به هر سر چشمه‌ای، هر مرغزاری</p></div>
<div class="m2"><p>همی‌کردند بودن را شماری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدین هنجار روزی چند گشتند</p></div>
<div class="m2"><p>که تا آخر به دشتی برگذشتند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صفای نوخطان با سبزه زارش</p></div>
<div class="m2"><p>صفای وقت وقف چشمه سارش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هوایش اعتدال جان گرفته</p></div>
<div class="m2"><p>نم از سرچشمهٔ حیوان گرفته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز کس گر سایه بر خاکش فتادی</p></div>
<div class="m2"><p>ز جا جستی و برپا ایستادی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر مرغی به شاخش آرمیدی</p></div>
<div class="m2"><p>گشادی سایه‌اش بال و پریدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گلش چون گلرخان پروردهٔ ناز</p></div>
<div class="m2"><p>نوای بلبلانش عشق پرداز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو گفتی حسن خیزد از فضایش</p></div>
<div class="m2"><p>فتوح عشق ریزد از هوایش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به شیرین آگهی دادند از آنجای</p></div>
<div class="m2"><p>از آن آب و هوای رغبت افزای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که در دامان کوه و کوهساری</p></div>
<div class="m2"><p>که تا کوه است از آنجا نعره‌داری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی صحراست پیش او گشاده</p></div>
<div class="m2"><p>فضای او سد اندر سد زیاده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر بر سبزه‌اش پویی به فرسنگ</p></div>
<div class="m2"><p>سر برگی نیابی زعفران رنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رسیده سبزه‌هایش تا کمرگاه</p></div>
<div class="m2"><p>درختانش زده بر سبزه خرگاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گشاده چشمه‌ای از قلهٔ کوه</p></div>
<div class="m2"><p>گل و سنبل به گرد چشمه انبوه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فرو ریزد چو بر دامان کهسار</p></div>
<div class="m2"><p>رگ ابریست پنداری گهر بار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خورد بر کوه و کوبد سنگ بر سنگ</p></div>
<div class="m2"><p>صدای آن رود فرسنگ فرسنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پر اندر پر زده مرغابیانش</p></div>
<div class="m2"><p>به جای موجه بر آب روانش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زمینهایش ز آب ابر شسته</p></div>
<div class="m2"><p>در او گلهای رنگارنگ رسته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بساطش در نقاب گل نهفته</p></div>
<div class="m2"><p>گل و لاله‌ست کاندر هم شکفته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر گلگون در آن گردد عنان کش</p></div>
<div class="m2"><p>وگر آنجا بود نعلش در آتش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نسیمش را مذاق باده در پی</p></div>
<div class="m2"><p>همه جایش برای صحبت می</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر شیرین در او بزمی نهد نو</p></div>
<div class="m2"><p>دگر یادش نیاید بزم خسرو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز کنج چشم شیرین اشک غلتید</p></div>
<div class="m2"><p>به بخت خود میان گریه خندید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که گویا بخت شیرین را ندانند</p></div>
<div class="m2"><p>که بر وی اینهمه افسانه خوانند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شکر تلخی دهد از بخت شیرین</p></div>
<div class="m2"><p>زهی شیرین و جان سخت شیرین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چه شیرین تلخ بهری، تلخ کامی</p></div>
<div class="m2"><p>ز شیرینی همین قانع به نامی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اگر سوی ارم شیرین نهد روی</p></div>
<div class="m2"><p>ز لاله رنگ بگریزد ز گل بوی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به باغ خلد اگر شیرین کند جای</p></div>
<div class="m2"><p>نهد عیش از در دیگر برون‌های</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر چین است اگر بتخانهٔ چین</p></div>
<div class="m2"><p>بود زندان چو خوشدل نیست شیرین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دل خوش یاد می‌آرد ز گلزار</p></div>
<div class="m2"><p>چو دل خوش نیست گل خار است و مسمار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر دل خوش بود می‌خوشگوار است</p></div>
<div class="m2"><p>شراب تلخ در غم زهر مار است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دلی دارم که گر بگشایمش راز</p></div>
<div class="m2"><p>به سد درد از درون آید به آواز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>غمی‌دارم که گر گیرم شمارش</p></div>
<div class="m2"><p>بترسم از حساب کار و بارش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کدامین دل کدامین خاطر شاد</p></div>
<div class="m2"><p>که آید از گل و از گلشنم یاد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مرا گفتند خوش جاییست دلکش</p></div>
<div class="m2"><p>هوا خوش، دست خوش، کهسار او خوش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بلی اطراف کوه و دامن دشت</p></div>
<div class="m2"><p>بود خوش گر به ذوق خود توان گشت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو دامان ماند زیر کوه اندوه</p></div>
<div class="m2"><p>چه فرق از طرف دشت و دامن کوه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چه خرسندی در آن مرغ غم انجام</p></div>
<div class="m2"><p>که باغ و راغ باید دیدش از دام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دگر گفتند جای می‌گساریست</p></div>
<div class="m2"><p>که دشتی پر ز گلهای بهاریست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بلی می‌خوش بود در دشت و کهسار</p></div>
<div class="m2"><p>ولی گر یار باشد لیک کو یار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بود بر بلبل گل آتشین داغ</p></div>
<div class="m2"><p>کش افتد در قفس نظارهٔ باغ</p></div></div>