---
title: >-
    بخش ۲۱ - گفتار اندر گفت و شنید غلامان شیرین با فرهاد و بردن او را به نزد شیرین مه جبین
---
# بخش ۲۱ - گفتار اندر گفت و شنید غلامان شیرین با فرهاد و بردن او را به نزد شیرین مه جبین

<div class="b" id="bn1"><div class="m1"><p>حریص گنج بنای گهر سنج</p></div>
<div class="m2"><p>بگفت این کار ممکن نیست بی‌گنج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بباید گنجی از گوهر گشادن</p></div>
<div class="m2"><p>گره از سیم و قفل از زر گشادن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود بر زر مدار کار عالم</p></div>
<div class="m2"><p>به زر آسان شود دشوار عالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر خواهی هنر را سخت بازو</p></div>
<div class="m2"><p>زر بی سنگ باید در ترازو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خلق و لطف خاطرها شود رام</p></div>
<div class="m2"><p>زر و سیم است دام، آن دانهٔ دام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو چیز آمد کمند هوشمندان</p></div>
<div class="m2"><p>کز آن بندند پای ارجمندان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی جودی که بی‌منت دهد کام</p></div>
<div class="m2"><p>یکی خلقی که بی‌نفرت زند گام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برو گر زین دو در ذاتت یکی نیست</p></div>
<div class="m2"><p>که در دستت کمند زیرکی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگفتندش که ما صنعت شناسیم</p></div>
<div class="m2"><p>هنر را پایهٔ قیمت شناسیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو صنعت کن که زر خود بی‌شمار است</p></div>
<div class="m2"><p>به پیش ما هنر را اعتباراست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هنر کمیاب باشد زر بسی هست</p></div>
<div class="m2"><p>هنر چیزیست کان با کم کسی هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر آن جوهر که نایابست کانش</p></div>
<div class="m2"><p>چو پیدا شد بود نرخ گرانش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به زر نرخ هنر هست از هنر دور</p></div>
<div class="m2"><p>چه نیکو گفت آن استاد مشهور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر آن صنعت که برسنجی به مالی</p></div>
<div class="m2"><p>بهای گوهری باشد سفالی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به گنج سیم و زر بنواختندش</p></div>
<div class="m2"><p>به شغل خویش راضی ساختندش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به تعریف و به تحسین و به تعظیم</p></div>
<div class="m2"><p>به انعام و به احسان زر و سیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به مرد تیشه سنج سخت بازو</p></div>
<div class="m2"><p>چو زر کردند گوهر در ترازو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز کار کارفرمایان بر آشفت</p></div>
<div class="m2"><p>گره بر گوشهٔ ابرو زد و گفت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مگر از بهر زر ما کار سنجیم</p></div>
<div class="m2"><p>ز میل طبع خود زینسان به رنجیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چه مایه زر که ما بر باد دادیم</p></div>
<div class="m2"><p>از آن روزی که بازو بر گشادیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به ذوق کارفرما کار سازیم</p></div>
<div class="m2"><p>ز مزد کارفرما بی‌نیازیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بلی گفتید در پیشانی مرد</p></div>
<div class="m2"><p>نوشته حالت پنهانی مرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برای صورت باطن نمایی</p></div>
<div class="m2"><p>چنین آیینه‌ای باشد خدایی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز گنج آسوده باشد آن هنر سنج</p></div>
<div class="m2"><p>که پنهانش به هر بازوست سد گنج</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تهی دستی خروشد از غم قوت</p></div>
<div class="m2"><p>که او را نیست بازو بند یاقوت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به ناخن تنگدستی گو بکن کان</p></div>
<div class="m2"><p>که الماسش نباشد در نگین دان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ترا دانیم محتاجی به زر نیست</p></div>
<div class="m2"><p>که سد گنجت به پای یک هنر نیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به ذوق کارفرما پیش نه پای</p></div>
<div class="m2"><p>که خیزد ذوق کار از کارفرما</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر تو کارفرما را بدانی</p></div>
<div class="m2"><p>چو نقش سنگ در کارش بمانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بگفت این کارفرما خود کدام است</p></div>
<div class="m2"><p>که درهر نسبتی کارش تمام است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بگفتندش که آن شیرین مشهور</p></div>
<div class="m2"><p>کزو پرویز را شوریست در شور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز نام او قیاس کار او کن</p></div>
<div class="m2"><p>حلاوت سنجی گفتار او کن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه تنها دیده جاسوس جمال است</p></div>
<div class="m2"><p>که راه گوش هم راه خیال است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به کامش درنشست آن نام چون نوش</p></div>
<div class="m2"><p>چنان کش تلخکامی شد فراموش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از آن نامش که جنبش در زبان بود</p></div>
<div class="m2"><p>اثر در حل و عقد استخوان بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از آن جنبش که در ارکان فتادش</p></div>
<div class="m2"><p>تزلزل در بنای جان فتادش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از آن نامش به جان میلی درآمد</p></div>
<div class="m2"><p>چه میلی کز درش سیلی درآمد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از آن سیلش که در رفت از ره گوش</p></div>
<div class="m2"><p>نگون شد سقف و طاق خانهٔ هوش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به استادی ره آن سیل می‌بست</p></div>
<div class="m2"><p>دل خود را گذر بر میل می‌بست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بگفت آنگه بدین شغلم فتد رای</p></div>
<div class="m2"><p>که افتد چشم من بر کارفرمای</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بگفتندش چنین باشد بلی خیز</p></div>
<div class="m2"><p>بس است این نازهای صنعت‌آمیز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گرت حسن هنر پرناز دارد</p></div>
<div class="m2"><p>که یارد تا از آنت باز دارد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز حسن آنجا که باشد نسبتی عام</p></div>
<div class="m2"><p>بود نازی، چنین شد رسم ایام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ولی این ناز هر جا درنگیرد</p></div>
<div class="m2"><p>بود کس کش به کاهی بر نگیرد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سخی را پرده زینسان می‌گشادند</p></div>
<div class="m2"><p>غرض از پرده بیرون می‌نهادند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>عبارت با کنایت یار می شد</p></div>
<div class="m2"><p>به نکته مدعا اظهار می‌شد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از آن تخمی که می‌کردند در گل</p></div>
<div class="m2"><p>وفا می‌رستش از جان، مهر از دل</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چنانش مهر غالب شد در آن کام</p></div>
<div class="m2"><p>که ره می‌خواست طی سازد به یک گام</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هوای دل چو گردد رغبت‌انگیز</p></div>
<div class="m2"><p>ز جان فریاد برخیزد که هان خیز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تقاضای دل امید پرورد</p></div>
<div class="m2"><p>تن از جان طاق سازد جان ز تن فرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هوس را در گریبان اخگر افتاد</p></div>
<div class="m2"><p>صبوری را خسک در بستر افتاد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دلی‌پر آرزو، جانی هوا خواه</p></div>
<div class="m2"><p>سراپای وجود آمادهٔ راه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به ایشان گفت اگر رفتن ضرور است</p></div>
<div class="m2"><p>توقف از صلاح کار دور است</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کسی کش عزم را بی‌حزم شد پیش</p></div>
<div class="m2"><p>چو محبوسان بود در خانهٔ خویش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به زندان گر رود از باغ و بستان</p></div>
<div class="m2"><p>درنگ بوستان بند است زندان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو دیدندش به رفتن استواری</p></div>
<div class="m2"><p>در آن ناسازگاری سازگاری</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ستودندش به تعریف و به تحسین</p></div>
<div class="m2"><p>به ظاهر از خود و پنهان ز شیرین</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>طلب را کفش پیش پا نهادند</p></div>
<div class="m2"><p>غرض را رخت در صحرا نهادند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>جهانیدند بر صحرا ز انبوه</p></div>
<div class="m2"><p>عنان دادند بر هنجار آن کوه</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به ذوق خویش هر یک نکته پیوند</p></div>
<div class="m2"><p>سخن را بر مذاق خود ز سد بند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>عمل پیوند عشق تازه آغاز</p></div>
<div class="m2"><p>نهان از یک به یک در پوزش راز</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از این پرسیدی آداب بساطش</p></div>
<div class="m2"><p>وزان ترتیب اسباب نشاطش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>که در بزمش بساط آرایی از کیست</p></div>
<div class="m2"><p>بساطش را نشاط افزایی از کیست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مذاقش را چه زهر است و چه تریاک</p></div>
<div class="m2"><p>هوس سوز است طبعش یا هوسناک</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دلش سخت است یا نرم است چونست</p></div>
<div class="m2"><p>عتابش بیش یا لطفش فزونست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>غروری خواهدش بودن به ناچار</p></div>
<div class="m2"><p>که اسباب غرورش هست بسیار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بگوییدم که رخش بی نیازی</p></div>
<div class="m2"><p>کجا تازد کجا آرد به بازی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بگفتندش که آری پر غرور است</p></div>
<div class="m2"><p>ولی جایی که استغنا ضرور است</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تغافلهای او با تاجداران</p></div>
<div class="m2"><p>تواضعهای او با خاکساران</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>کس ار مسکین بود مسکین نوازست</p></div>
<div class="m2"><p>و گر نه پای استغنا دراز است</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>سحاب رحمت است و سخت باران</p></div>
<div class="m2"><p>ولی بر کشتزار عجز کاران</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>از آن ابری که گردد قطره‌انگیز</p></div>
<div class="m2"><p>کند از رشحهٔ خود سبزه نوخیز</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو آید وقت آن کان سبزهٔ تر</p></div>
<div class="m2"><p>رسد جایی کز آن دهقان خورد بر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>فرو بارد چنان محکم تگرگی</p></div>
<div class="m2"><p>که نی شاخش بجا ماند نه برگی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چنان ابری که گر بر خشک خاری</p></div>
<div class="m2"><p>نم خود را دهد گاهی گذاری</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چنان نشوی دهد دربار آن خار</p></div>
<div class="m2"><p>که نخلی گردد و آرد رطب بار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>وفا تخمی‌ست رسته از گل او</p></div>
<div class="m2"><p>فراموشی نمی‌داند دل او</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>دلی دارد که گر موری شود ریش</p></div>
<div class="m2"><p>به سد عذرش فرستد مرهم خویش</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به یک ایما بیابد یک جهان راز</p></div>
<div class="m2"><p>به یک دیدن بگوید سد چنان باز</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ز شوخیها که مخصوص جوانیست</p></div>
<div class="m2"><p>تو گویی عاشق مرکب دوانیست</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>به خاصان بر نشسته صبح تا شام</p></div>
<div class="m2"><p>ندارد هیچ جا یک ذره آرام</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ازین جانب دواند تیر در شست</p></div>
<div class="m2"><p>شود ز آنسوی مرغ کشته در دست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>یکی چابک عنانش زیر زین است</p></div>
<div class="m2"><p>که نی بر آسمان، نی بر زمین است</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>هر آن جنبش که بر خاطر گذشته</p></div>
<div class="m2"><p>بدان میزان عنان انداز گشته</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>رود بر راه موی پر خم و پیچ</p></div>
<div class="m2"><p>که پیچ و خم نجنبد زان شدن هیچ</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>گرش افتد به چشم مور رفتار</p></div>
<div class="m2"><p>نگردد ور از آن رفتن خبردار</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بتازد آنقدر روزیش کان راه</p></div>
<div class="m2"><p>نپوید ابلق گردون به یک ماه</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>همان در رقص باشد زیر رانش</p></div>
<div class="m2"><p>اگر تازد جهان اندر جهانش</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>برقصد چون نرقصد آری آری</p></div>
<div class="m2"><p>که دارد آنچنان چابک سواری</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>سواری چون سوار لعب دانی</p></div>
<div class="m2"><p>سواری خود سر و چابک عنانی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>چو خسرو گر چو خسرو سد هزارند</p></div>
<div class="m2"><p>چو او ره سر کند دنباله دارند</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بتازد از کناره در میانه</p></div>
<div class="m2"><p>به بالا برده دست و تازیانه</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ز شوخی در پی این یک دواند</p></div>
<div class="m2"><p>به بازی بر سر آن یک جهاند</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>کنون هر جا که هست اندر سواری‌ست</p></div>
<div class="m2"><p>شکار انداز کبک کوهساری‌ست</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بگفتا وه چه خوش باشد که ناگاه</p></div>
<div class="m2"><p>سمندش را گذار افتد بر این راه</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بگفتندش که راهی نیست بسیار</p></div>
<div class="m2"><p>از اینجا تا به آن دامان کهسار</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>عجب نبود که آید از پی گشت</p></div>
<div class="m2"><p>که نزدیک است آن صحرا به این دشت</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>یکی سدگشت شوق و اضطرابش</p></div>
<div class="m2"><p>ز دل یکباره طاقت رفت و تابش</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>هجوم آورد رغبتهای جانی</p></div>
<div class="m2"><p>سراپا دیده شد در دیده‌بانی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>نه یک دیدن همه دستش نظر گاه</p></div>
<div class="m2"><p>نشانده سد نگه در هر گذرگاه</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>بلی چون آرزو در دل نهد گام</p></div>
<div class="m2"><p>نظر گردد مجاور در ره کام</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>به وسواس گمان آرزومند</p></div>
<div class="m2"><p>به راه آرزو سالی شود بند</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>اساسی دارد این امید دیدار</p></div>
<div class="m2"><p>که نتوان کندنش کاهی ز دیوار</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>اگر سد تیشهٔ حرمان شود تیز</p></div>
<div class="m2"><p>نگردد گرد این بی جنبش‌آمیز</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>نفرساید بنای استوارش</p></div>
<div class="m2"><p>نسازد کهنه طول انتظارش</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>خوش است امید و امید خوش انجام</p></div>
<div class="m2"><p>که در ریزد به یکبار از در و بام</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>خوشا امید اگر آید فرادست</p></div>
<div class="m2"><p>خوشا بخت کسی کاین دولتش هست</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>تک و پوی نظر از حد گذشته</p></div>
<div class="m2"><p>در آن صحرا نگاهش پهن گشته</p></div></div>