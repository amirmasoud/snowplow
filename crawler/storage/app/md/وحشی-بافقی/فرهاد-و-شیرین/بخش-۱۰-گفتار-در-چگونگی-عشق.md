---
title: >-
    بخش ۱۰ - گفتار در چگونگی عشق
---
# بخش ۱۰ - گفتار در چگونگی عشق

<div class="b" id="bn1"><div class="m1"><p>یکی میل است با هر ذره رقاص</p></div>
<div class="m2"><p>کشان هر ذره را تا مقصد خاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رساند گلشنی را تا به گلشن</p></div>
<div class="m2"><p>دواند گلخنی را تا به گلخن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر پویی ز اسفل تا به عالی</p></div>
<div class="m2"><p>نبینی ذره‌ای زین میل خالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آتش تا به باد از آب تا خاک</p></div>
<div class="m2"><p>ز زیر ماه تا بالای افلاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همین میل است اگر دانی ، همین میل</p></div>
<div class="m2"><p>جنیبت در جنیبت ، خیل در خیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر این رشته‌های پیچ در پیچ</p></div>
<div class="m2"><p>همین میل است و باقی هیچ بر هیچ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از این میل است هر جنبش که بینی</p></div>
<div class="m2"><p>به جسم آسمانی یا زمینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همین میل است کهن را درآموخت</p></div>
<div class="m2"><p>که خود را برد و بر آهن ربا دوخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همین میل آمد و با کاه پیوست</p></div>
<div class="m2"><p>که محکم کار را بر کهرباست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به هر طبعی نهاده آرزویی</p></div>
<div class="m2"><p>تک و پو داده هر یک را به سویی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برون آورده مجنون را مشوش</p></div>
<div class="m2"><p>به لیلی داده زنجیرش که می‌کش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز شیرین کوهکن را داده شیون</p></div>
<div class="m2"><p>فکنده بیستون پیشش که می‌کن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز تاب شمع گشته آتش افروز</p></div>
<div class="m2"><p>زده پروانه را آتش که می‌سوز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز گل بر بسته بلبل را پر و بال</p></div>
<div class="m2"><p>شکسته خار در جانش که می‌نال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غرض کاین میل چون گردد قوی پی</p></div>
<div class="m2"><p>شود عشق و درآید در رگ و پی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وجود عشق کش عالم طفیل است</p></div>
<div class="m2"><p>ز استیلای قبض و بسط میل است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نبینی هیچ جز میلی در آغاز</p></div>
<div class="m2"><p>ز اصل عشق اگر جویی نشان باز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر یک شعله در خود سد هزار است</p></div>
<div class="m2"><p>به اصلش بازگردی یک شرار است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شراری باشد اول آتش انگیز</p></div>
<div class="m2"><p>کز استیلاست آخر آتش تیز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تف این شعله ما را در جگر باد</p></div>
<div class="m2"><p>از این آتش دل ما پر شرر باد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ازین آتش دل آن را که داغیست</p></div>
<div class="m2"><p>اگر توفان شود او را فراغیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کسی کش نیست این آتش فسرده‌ست</p></div>
<div class="m2"><p>سراپا گر همه جانست مرده‌ست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر سد آب حیوان خورده باشی</p></div>
<div class="m2"><p>چو عشقی در تو نبود مرده باشی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مدار زندگی بر چیست برعشق</p></div>
<div class="m2"><p>رخ پایندگی در کیست در عشق</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز خود بگسل ولی زنهار زنهار</p></div>
<div class="m2"><p>به عشق آویز و عشق از دست مگذار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به عین عشق آنکو دیده‌ور شد</p></div>
<div class="m2"><p>همه عیب جهان پیشش هنر شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هنر سنجی کند سنجیدهٔ عشق</p></div>
<div class="m2"><p>نبیند عیب هرگز دیدهٔ عشق</p></div></div>