---
title: >-
    بخش ۴ - در ستایش حضرت پیغمبر«ص»
---
# بخش ۴ - در ستایش حضرت پیغمبر«ص»

<div class="b" id="bn1"><div class="m1"><p>حکیم عقل کز یونان زمین است</p></div>
<div class="m2"><p>اگر چه بر همه بالانشین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر جا شرع بر مسند نشیند</p></div>
<div class="m2"><p>کسش جز در برون در نبیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلی شرع است ایوان الاهی</p></div>
<div class="m2"><p>نبوت اندر او اورنگ شاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بساطی کش نبوت مجلس آراست</p></div>
<div class="m2"><p>کجا هر بوالفضولی را در او جاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرد هر چند پوید گاه و بیگاه</p></div>
<div class="m2"><p>نیابد جای جز بیرون درگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکوشد تا کند بیرون در جای</p></div>
<div class="m2"><p>چو نزدیک در آید گم کند پای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه شد گو باش گامی تا در کام</p></div>
<div class="m2"><p>چو پا نبود چه یک فرسخ چه یک گام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسا کوری که آید تا در بار</p></div>
<div class="m2"><p>چو چشمش نیست سر کوبد به دیوار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگر هم از درون بانگی برآید</p></div>
<div class="m2"><p>که چشمی لطف کردیمش، درآید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در این ایوان که با طغرای جاوید</p></div>
<div class="m2"><p>برون آرند حکم بیم و امید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نبوت مسند آرایان تقدیر</p></div>
<div class="m2"><p>وز او اقلیم جان کردند تسخیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به عالی خطبهٔ «الملک لله»</p></div>
<div class="m2"><p>ز ماهی صیتشان بررفت تا ماه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهان را در صلای کار جمهور</p></div>
<div class="m2"><p>به لطف و قهر تو کردند منشور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه شاهانی که تخت و تاج خواهند</p></div>
<div class="m2"><p>ازین ده‌های ویران باج خواهند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از آن شاهان که کشور گیر جانند</p></div>
<div class="m2"><p>ولایت بخش ملک جاودانند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عطاهاشان به هر بی‌برگ و بی ساز</p></div>
<div class="m2"><p>هزاران روضهٔ پرنعمت و ناز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بود ملک ابد کمتر عطاشان</p></div>
<div class="m2"><p>اگر باور نداری شو گداشان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شهانی فارغ از خیل وخزانه</p></div>
<div class="m2"><p>طفیل پادشاهیشان زمانه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه از آفرینش برگزیده</p></div>
<div class="m2"><p>همه از نور یک ذات آفریده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چه ذاتی عین نور ذوالجلالی</p></div>
<div class="m2"><p>چه نوری اله اله لایزالی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز نورش هر کجا آثار روحی‌ست</p></div>
<div class="m2"><p>به خدمت اندرش هر جا فتوحی‌ست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جهان را علت غائی وجودش</p></div>
<div class="m2"><p>وجود جمله موج بحر جودش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>محمد تاجدار تخت کونین</p></div>
<div class="m2"><p>دو کون از وی پر از زیب و پر از زین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چراغ چشم چرخ انجم افروز</p></div>
<div class="m2"><p>ز نامش حرز تو مار شب و روز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فلک میدان سوار لامکان پوی</p></div>
<div class="m2"><p>مجره صولجان آسمان کوی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شکست آموز کار لات و عزا</p></div>
<div class="m2"><p>نگونسازی از او در طاق کسری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شده ز آب وضوی آو به یک مشت</p></div>
<div class="m2"><p>به گردون دود از آتشگاه زردشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شکوه او صلیب از پا در افکند</p></div>
<div class="m2"><p>کزان هیزم بسوزد زند و پازند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عرب را زو برآمد آفتابی</p></div>
<div class="m2"><p>که از وی صبح هستی بود تابی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه خورشیدی که چون پنهان کند روی</p></div>
<div class="m2"><p>گذارد دهر را ظلمت ز هر سوی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فروزان نیری کاندر نقاب است</p></div>
<div class="m2"><p>ازو عالم سراسر آفتاب است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز شرع او که مهر انور آمد</p></div>
<div class="m2"><p>جهان را مهر بالای سر آمد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چنان شد ظلمت کفر از جهان دور</p></div>
<div class="m2"><p>که ناگه خال بت رویان شود نور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز عزت مولدش با مکه آن کرد</p></div>
<div class="m2"><p>که اندر هر شبان روزی زن ومرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سجود از چار حد مرکز گل</p></div>
<div class="m2"><p>برندش پنج نوبت در مقابل</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هزاران راه را یک راه کرده</p></div>
<div class="m2"><p>سخن بر رهروان کوتاه کرده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سپرده ره به ره داران مقصود</p></div>
<div class="m2"><p>همه غولان ره را کرده نابود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>میان آب و گل آدم نهان بود</p></div>
<div class="m2"><p>که او پیغمبر آخر زمان بود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نداده با نفس یک حرف پیوند</p></div>
<div class="m2"><p>که نقش زر نگشته سکه مانند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز جنبش گیر از وی تا به آرام</p></div>
<div class="m2"><p>نبود الا رموز وحی و الهام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو شد قلب آزمای آفرینش</p></div>
<div class="m2"><p>به معیاری که دانند اهل بینش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نخست آورد سوی آسمان دست</p></div>
<div class="m2"><p>فلک را سیم قلب ماه بشکست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز نقد خود چو دیدش شرمساری</p></div>
<div class="m2"><p>درستی دادش و کامل عیاری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که یعنی آمدم ای قلب کاران</p></div>
<div class="m2"><p>به کامل کردن ناقص عیاران</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کرا قلبیست تا بعد از شکستن</p></div>
<div class="m2"><p>درستش کرده بسپارم به دستش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نه در دستش همین شق قمر بود</p></div>
<div class="m2"><p>به هر انگشت از اینش سد هنر بود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به تخت هستی ار خاص است اگر عام</p></div>
<div class="m2"><p>همه در حیطهٔ فرمان او رام</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زمانه خانه زاد مدت اوست</p></div>
<div class="m2"><p>ز خردی باز اندر خدمت اوست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز رویش روز تابی وام کرده</p></div>
<div class="m2"><p>زمانه آفتابش نام کرده</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چه می‌گویم به جنب رحمت عام</p></div>
<div class="m2"><p>بود بیهوده وام و نسبت وام</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به شب از گیسوی خود داده تاری</p></div>
<div class="m2"><p>بر او هر شب کواکب را نثاری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هم از گنجینهٔ جودش ستانند</p></div>
<div class="m2"><p>گهرهایی که بر مویش فشانند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دویده آسمان عمری به راهش</p></div>
<div class="m2"><p>که کرده ذروهٔ خود تختگاهش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چه مایه ابر کرده اشکباری</p></div>
<div class="m2"><p>که گشته خاصه شغل چترداری</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>زر شک شغل او خورشید افلاک</p></div>
<div class="m2"><p>زند هر شام چتر خویش بر خاک</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سحابش بود بر سر تازیانه</p></div>
<div class="m2"><p>چو دید آن خلق و حسن جاودانه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سپندی سوخت در دفع گزندش</p></div>
<div class="m2"><p>به بالا جمع شد دود سپندش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کسی از چشم بد خود نیستش باک</p></div>
<div class="m2"><p>که خواند «ان یکاد»ش ایزد پاک</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>در آن عرصه که نور جاودانست</p></div>
<div class="m2"><p>براق جان در او چابک عنانست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>جنیبت تا به حدی پیش رانده</p></div>
<div class="m2"><p>که از پی سایه نیزش بازمانده</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به هر جا کآفتاب آنجا نهد پای</p></div>
<div class="m2"><p>پس دیوار باشد سایه را جای</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>فتادی سایه‌اش گر بر سر خاک</p></div>
<div class="m2"><p>زمین سر برزدی از جیب افلاک</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو راه خدمتش نسپرد سایه</p></div>
<div class="m2"><p>در آن پستی که بودش ماند مایه</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گرش سایه زمین بوسیدی از دور</p></div>
<div class="m2"><p>دویدی چون غلامان از پیش نور</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به ذوق بزم قرب وحدت انجام</p></div>
<div class="m2"><p>بدانسان قالبی بودش سبک گام</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>که گرنه بر شکم می‌بست سنگش</p></div>
<div class="m2"><p>ندیدی کس به دیگر جا درنگش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>تعالی الله چه قالب اصل جانها</p></div>
<div class="m2"><p>دوان درسایهٔ لطفش روانها</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>زهی قالب نه قالب جان عالم</p></div>
<div class="m2"><p>نه تنها جان و بس جانان عالم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ز جسمش گوخرد اندازه بردار</p></div>
<div class="m2"><p>حدیث جان همان در پرده بگذار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>که ترسم گر شود بی‌پرده آن راز</p></div>
<div class="m2"><p>نباشد کس حریف وهم غماز</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>در آن قالب کسی کاین جانش باشد</p></div>
<div class="m2"><p>به گردون برشدن آسانش باشد</p></div></div>