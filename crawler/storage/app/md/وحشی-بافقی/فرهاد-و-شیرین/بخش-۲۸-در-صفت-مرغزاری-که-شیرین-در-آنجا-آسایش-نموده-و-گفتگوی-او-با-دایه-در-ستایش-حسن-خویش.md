---
title: >-
    بخش ۲۸ - در صفت مرغزاری که شیرین در آنجا آسایش نموده و گفتگوی او با دایه در ستایش حسن خویش
---
# بخش ۲۸ - در صفت مرغزاری که شیرین در آنجا آسایش نموده و گفتگوی او با دایه در ستایش حسن خویش

<div class="b" id="bn1"><div class="m1"><p>همایون دشتی و خوش مرغزاری</p></div>
<div class="m2"><p>که شیرین را بود آنجا گذاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مبارک منزلی ، دلکش مکانی</p></div>
<div class="m2"><p>که شیرین در وی آساید زمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فضایی خوشتر از فردوس باید</p></div>
<div class="m2"><p>که آنجا خاطر شیرین گشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهی کش در دل و جان است منزل</p></div>
<div class="m2"><p>ز آب و گل کجا بگشایدش دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلی کش نالهٔ دلها خوش آید</p></div>
<div class="m2"><p>سرود کبک و دراجش نشاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بتی کش خو به دلهای فکار است</p></div>
<div class="m2"><p>کجا میلش به گشت لاله زار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی کش خسرو و فرهاد باید</p></div>
<div class="m2"><p>کجا از سرو و بیدش یاد آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگار نازنین شیرین مهوش</p></div>
<div class="m2"><p>چو زلف خود پریشان و مشوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تمنای درونی شاد می‌داشت</p></div>
<div class="m2"><p>امید خاطری آزاد می‌داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وزان غافل که تا گیتی به پا بود</p></div>
<div class="m2"><p>مکافات جفا کاری جفا بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل آزاد و فرهاد آتشین دل</p></div>
<div class="m2"><p>روان شاد و خسرو پای در گل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ولی چون لازم خوبی غرور است</p></div>
<div class="m2"><p>نکویی علت طبع غیور است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به دل آن درد را همواره می‌کرد</p></div>
<div class="m2"><p>به یاران خوشدلی اظهار می‌کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به ساغر چهره را می‌کرد گلگون</p></div>
<div class="m2"><p>لبش خندان چو ساغر دل پر از خون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسی ترتیب دادی محفل خوش</p></div>
<div class="m2"><p>ولی کو جان شاد و کو دل خوش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به هر جا جشن کردی آن دلارام</p></div>
<div class="m2"><p>ولی یکجا دلش نگرفتی آرام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو میل دل شدی سوی شرابش</p></div>
<div class="m2"><p>به اشک آمیختی صهبای تابش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مگر از ضعف دل پرهیز می‌کرد</p></div>
<div class="m2"><p>که صهبا را گلاب آمیز می‌کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به یاد روی خسرو جام خوردی</p></div>
<div class="m2"><p>ولی فرهاد را هم نام بردی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنین صحرا به صحرا دشت در دشت</p></div>
<div class="m2"><p>فریب خویشتن می‌داد و می‌گشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز هر جا می‌گذشت از بیقراری</p></div>
<div class="m2"><p>که با طبعم ندارد سازگاری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه از ناصبوری های دل بود</p></div>
<div class="m2"><p>بهانه تهمتش بر آب و گل بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به دشتی ناگهان افتاد راهش</p></div>
<div class="m2"><p>که از هر گونه گل بود و گیاهش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از او در رشک گلزار ارم بود</p></div>
<div class="m2"><p>دو گل در وی به یک مانند کم بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هوایش معتدل خاکش روان بخش</p></div>
<div class="m2"><p>زلالش همچو خاک خضر جان بخش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>غزالان وی از سنبل چریده</p></div>
<div class="m2"><p>گوزنانش به سنبل آرمیده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شقایق سوختی دایم سپندش</p></div>
<div class="m2"><p>که از چشم خسان ناید گزندش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنان آماده نشو و نما بو</p></div>
<div class="m2"><p>کز او هر برگ را چیدی بجا بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نبستی پرده گر دایم سحابش</p></div>
<div class="m2"><p>فسردی از نزاکت آفتابش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز بس روییده در وی سبزه با هم</p></div>
<div class="m2"><p>سحاب از برگ دادی ریشه را نم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز بس عطر اندر آن خاک و هوابود</p></div>
<div class="m2"><p>گرش صحرای چین گفتی خطا بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به روی سبزه کبکانش به بازی</p></div>
<div class="m2"><p>خرام آموز خوبان طرازی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>غزالانش به خوبان ختابی</p></div>
<div class="m2"><p>نموده راه و رسم دلربایی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز بس گل کاندرو هر سو شکفته</p></div>
<div class="m2"><p>زمینش سر به سر در گل نهفته</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کس ار باری از آن صحرا گذشتی</p></div>
<div class="m2"><p>خزان در خاطرش دیگر نگشتی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سرشتهٔ نشأه می با هوایش</p></div>
<div class="m2"><p>نهفته باغ جنت در فضایش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو بگذشت اندر آن دشت آن یگانه</p></div>
<div class="m2"><p>نماندش بهر بگذشتن بهانه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به پای چشمه‌ای آن چشمهٔ نوش</p></div>
<div class="m2"><p>فرود آمد که تا جامی کند نوش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به ساقی گفت آبی در قدح ریز</p></div>
<div class="m2"><p>که اندر سینه دارم آتشی تیز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز بیتابی ببین در پیچ و تابم</p></div>
<div class="m2"><p>فشان بر آتش دل از می‌آبم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به مطرب گفت قانون طرب ساز</p></div>
<div class="m2"><p>به قانونی که بهتر برکش آواز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رهی سرکن که غم از دل رهاند</p></div>
<div class="m2"><p>سر و کار دل از غم بگسلاند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به فرمان صنم ساقی صلا گفت</p></div>
<div class="m2"><p>خمار آلودگان را مرحبا گفت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>می گلرنگ در جام طرب کرد</p></div>
<div class="m2"><p>به مستی هوشیاری را ادب کرد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نی مطرب چنان آهنگ برداشت</p></div>
<div class="m2"><p>که گفتی دور از شیرین شکر داشت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دماغ از آب می چون شست وشو کرد</p></div>
<div class="m2"><p>به دایه از غم دل گفت و گو کرد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که کس چون من نیفتد در پی دل</p></div>
<div class="m2"><p>نبازد عمر در سودای باطل</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز کف دل داده و غمخوار گشته</p></div>
<div class="m2"><p>پی دل هر طرف آواره گشته</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز شهر و بوم خود محروم مانده</p></div>
<div class="m2"><p>به هر ویرانه همچون بوم مانده</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دلی دارم که با هرکس به جنگ است</p></div>
<div class="m2"><p>بر او پهنای هفت اقلیم تنگ است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ستیزم گر به جانان رای آن کو</p></div>
<div class="m2"><p>گریزم گر ز دوران پای آن کو</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نه جانان را سر ناکامی من</p></div>
<div class="m2"><p>نه دوران در پی بدنامی من</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مرا از خویش باشد مشکل خویش</p></div>
<div class="m2"><p>که دارم هر چه دارم از دل خویش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جوانی صرف کرده در غم دل</p></div>
<div class="m2"><p>شمرده زخم دل را مرهم دل</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به نیرنگ کسان از ره فتاده</p></div>
<div class="m2"><p>به بوی ره درون چه فتاده</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>فریبی را طلب کاری شمرده</p></div>
<div class="m2"><p>فسونی را وفاداری شمرده</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هوس را درپذیرفته به یاری</p></div>
<div class="m2"><p>طمع را نام کرده دوستداری</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>وفا پنداشته مکر و حیل را</p></div>
<div class="m2"><p>محبت خوانده افسون و دغل را</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>عجبتر اینکه با پیمان شکستن</p></div>
<div class="m2"><p>به یار تازه عهد تازه بستن</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز شیرین بر زبانش نام هم نیست</p></div>
<div class="m2"><p>سزای نامه و پیغام هم نیست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کند خسرو گمان کز زغم شکر</p></div>
<div class="m2"><p>دل شیرین بود از غم پر آذر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>مرا خود اولا پروای آن نیست</p></div>
<div class="m2"><p>وگر باشد تو دانی جای آن نیست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو خورشید جمالم پرتو آرد</p></div>
<div class="m2"><p>به حربایی هزاران خسرو آرد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو گردد لعل شیرینم شکربار</p></div>
<div class="m2"><p>به سر دست شکر بینی مگس وار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به دل رشکی نه از پرویز دارم</p></div>
<div class="m2"><p>نه از پیوند شکر نیز دارم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>اگر شکر به حکم من به کار است</p></div>
<div class="m2"><p>وگر خسرو ز عشق من فکار است</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ندیدم چونکه مرد این کمندش</p></div>
<div class="m2"><p>به گیسوی شکر کردم به بندش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بلی شایسته شیر است زنجیر</p></div>
<div class="m2"><p>کمند و بند شد در خورد نخجیر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چو خسرو عشق را آمد مسخر</p></div>
<div class="m2"><p>چه دامش طرهٔ شیرین چه شکر</p></div></div>