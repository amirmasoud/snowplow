---
title: >-
    بخش ۳۱ - در افزونی محبت فرهاد و شور عشق او در فراق شیرین
---
# بخش ۳۱ - در افزونی محبت فرهاد و شور عشق او در فراق شیرین

<div class="b" id="bn1"><div class="m1"><p>عجب دردیست خو با کام کردن</p></div>
<div class="m2"><p>به نا گه زهر غم در جام کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سر بردن به شادی روزگاران</p></div>
<div class="m2"><p>به ناگه دور افتادن ز یاران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب کاریست بعد از شهریاری</p></div>
<div class="m2"><p>در افتادن به مسکینی و خواری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز اوج کامکاری اوفتادن</p></div>
<div class="m2"><p>به ناکامی و خواری دل نهادن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوشی چندان که در قربت فزون تر</p></div>
<div class="m2"><p>به مهجوری دل از غم پر ز خون تر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شود هر چند افزون آشنایی</p></div>
<div class="m2"><p>فزون تر گردد اندوه جدایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر چه کوهکن از جام شیرین</p></div>
<div class="m2"><p>ندید از تلخکامی کام شیرین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصال او دمی یا بیشتر بود</p></div>
<div class="m2"><p>وز آن یک دم نصیبش یک نظر بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محبت تیر خود را کارگر کرد</p></div>
<div class="m2"><p>به فرهاد آنچه کرد آن یک نظر کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو دید از یک نظر یک عمر شادی</p></div>
<div class="m2"><p>رسیدش نیز عمری نامردای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در آن کوه جفا کش با دل تنگ</p></div>
<div class="m2"><p>به جای تیشه سر می‌کوفت بر سنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز سنگ از تیشه گاهی می‌تراشید</p></div>
<div class="m2"><p>به ناخن سینه گاهی می‌خراشید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ولی چون تیشه بر سنگ او فکندی</p></div>
<div class="m2"><p>به جای سنگ نیز از سینه کندی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که نزهتگاه جانان سینه باید</p></div>
<div class="m2"><p>چو دل جایش درون سینه شاید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر او در سینه جای دل نهد سنگ</p></div>
<div class="m2"><p>تنش چون دل نهم در سینهٔ تنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به هر نقشی که بربستی به خارا</p></div>
<div class="m2"><p>به دل سد نقش بستی زان دلارا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از آن دیر آمد آن مشکو به انجام</p></div>
<div class="m2"><p>که کار او فزودی عشق خود کام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر مه بودی آن کوه ار چو گردون</p></div>
<div class="m2"><p>به ضرب تیشه‌اش کردی چو هامون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به هر جاکردی از آن پشته هموار</p></div>
<div class="m2"><p>به دل گفتی چو اینجا پا نهد یار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ادب نبود به نوک تیشه سودن</p></div>
<div class="m2"><p>چنین در عاشقی نااهل بودن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نمودی آن بلند و پست یکسان</p></div>
<div class="m2"><p>گهی با ناخن و گاهی به مژگان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به هر صورت که بستی زان جفا کار</p></div>
<div class="m2"><p>به دل گفتی کجا این و کجا یار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ستردی در دم آن نقشی که بستی</p></div>
<div class="m2"><p>پس آنگه دست خویش از تیشه خستی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بگفتی کاین سزای آنچنان دست</p></div>
<div class="m2"><p>که نقش اینچنین گستاخ بشکست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به روز و شب نه خوردش بود و نه خفت</p></div>
<div class="m2"><p>به خویش از وصل یار افسانه می‌گفت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به دل گفتی که ای مینای پر خون</p></div>
<div class="m2"><p>مده یکچند خون از دیده بیرون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که آن خونخواره چون آید به پیشت</p></div>
<div class="m2"><p>نیاید شرمی از مهمان خویشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بگفتی سینه را زین پیش مگداز</p></div>
<div class="m2"><p>تو نیز از تاب دل می‌سوز و می‌ساز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که چون نوشد ز خون دل شرابی</p></div>
<div class="m2"><p>مهیا سازی از بهرش کبابی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بگفتی دیده را کای ابر خون بار</p></div>
<div class="m2"><p>ز سیل خون چه می‌بندی ره یار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بس است این جوی خون پیوسته راندن</p></div>
<div class="m2"><p>که نتوان بررهش آبی فشاندن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به غم گفتی که ای همخوابهٔ دل</p></div>
<div class="m2"><p>برون کش رخت از ویرانهٔ دل</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که چون آن گنج خوبی در برآید</p></div>
<div class="m2"><p>چو جان جایش به غیر دل نشاید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به افغان گفت عشرت ساز او باش</p></div>
<div class="m2"><p>به سر می‌گفت پا انداز او باش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز خود پرداختی زان پس به گردون</p></div>
<div class="m2"><p>که ای از دور تو در ساغرم خون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز تو ای بیستون دل گر چه خون است</p></div>
<div class="m2"><p>فزونتر سختیم از بیستون است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو مهمانی به نزهتگاه شیرین</p></div>
<div class="m2"><p>مرا پیوسته تلخ تست شیرین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چه باشد کز در یاری در آیی</p></div>
<div class="m2"><p>مرا در عاشقی یاری نمایی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نمایی روی گلگون را بدین سوی</p></div>
<div class="m2"><p>که تاگلگون نمایم از سمش روی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ولیکن دانمت کاین حد نداری</p></div>
<div class="m2"><p>که او را موکشان سوی من آری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که دانم خاطر شیرین غیور است</p></div>
<div class="m2"><p>سرش از چنبر حکم تو دور است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو شیرین حلقهٔ گیسو گشاید</p></div>
<div class="m2"><p>چو من سد چون تواش در چنبر آید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>وزان پس با خیال دوست گفتی</p></div>
<div class="m2"><p>به خود گفتی ز خود پاسخ شنفتی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که یارا هم تو از محنت رهانم</p></div>
<div class="m2"><p>که کاری برنیاید زین و آنم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو یاری کن که گردون بر خلاف است</p></div>
<div class="m2"><p>تو بامن راست شو کاو بر گزاف است</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وگر گردون موافق با من آید</p></div>
<div class="m2"><p>تو چون بندی دری او چون گشاید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نگارا از ره بیداد باز آی</p></div>
<div class="m2"><p>بده داد من و بر من ببخشای</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مکن آزاد از دامم خدا را</p></div>
<div class="m2"><p>ولیکن با من بیدل مدارا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز دوری باشدم زان ناصبوری</p></div>
<div class="m2"><p>که از یاد تو دور افتم ز دوری</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گر از دوری فراموشم نسازی</p></div>
<div class="m2"><p>من و با درد دوری جان گدازی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نخست از مرگ می‌جستم کرانه</p></div>
<div class="m2"><p>که تا دوری نیفتد در میانه</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو می‌بینم غمت را جاودانی</p></div>
<div class="m2"><p>کنون مرگم به است از زندگانی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گمان این بود کان زلف درازم</p></div>
<div class="m2"><p>همین جا دام گسترده‌ست بازم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کنون چون بینم آن زلف دلاویز</p></div>
<div class="m2"><p>کشیده در ره دل تا عدم نیز</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مران ای دوست از این پس ز پیشم</p></div>
<div class="m2"><p>زمانی راه ده در وصل خویشم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نخواهم عزتی زین قربت از تو</p></div>
<div class="m2"><p>که خواری از من است و عزت از تو</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ندانم فرق عزت را ز خواری</p></div>
<div class="m2"><p>که عشقم کرده این آموزگاری</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ولی عشقت به لب آورده جانم</p></div>
<div class="m2"><p>همیخواهم که بر پایت فشانم</p></div></div>