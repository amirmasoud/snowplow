---
title: >-
    بخش ۱۴ - حکایت
---
# بخش ۱۴ - حکایت

<div class="b" id="bn1"><div class="m1"><p>زلیخا را چو پیری ناتوان کرد</p></div>
<div class="m2"><p>گلش را دست فرسود خزان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز چشمش روشنایی برد ایام</p></div>
<div class="m2"><p>نهادش پلکها بر هم چو بادام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمان بشکستش ابروی کماندار</p></div>
<div class="m2"><p>خدنگ انداز غمزه رفتش از کار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لبش را خشک شد سرچشمهٔ نوش</p></div>
<div class="m2"><p>بکلی نوشخندش شد فراموش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن پیری که سد غم حاصلش بود</p></div>
<div class="m2"><p>همان اندوه یوسف در دلش بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلش با عشق یوسف داشت پیوند</p></div>
<div class="m2"><p>به یوسف بود از هر چیز خرسند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر مویی ز عشق او نمی‌کاست</p></div>
<div class="m2"><p>بجز یوسف نمی جست و نمی‌خواست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کمال عشق در وی کارکر شد</p></div>
<div class="m2"><p>نهال آرزویش بارور شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر او نو گشت ایام جوانی</p></div>
<div class="m2"><p>مهیا کرد دور زندگانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به مزد آن که داد بندگی داد</p></div>
<div class="m2"><p>دوباره عشق او را زندگی داد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگرمی‌بایدت عمر دوباره</p></div>
<div class="m2"><p>مکن پیوند عمر از عشق پاره</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز هر جا حسن بیرون می‌نهد پای</p></div>
<div class="m2"><p>رخی از عشق هست آنجا زمین سای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیازی هست هر جا هست نازی</p></div>
<div class="m2"><p>نباشد ناز اگر نبود نیازی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نگاهی باید از مجنون در آغاز</p></div>
<div class="m2"><p>که آید چشم لیلی بر سر ناز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ایاز ار جلوه‌ای ندهد به بازار</p></div>
<div class="m2"><p>نیابد همچو محمودی خریدار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>میان حسن و عشق افتاد این شور</p></div>
<div class="m2"><p>ز ما غیر نگاهی ناید از دور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه عذرا آگهی دارد نه وامق</p></div>
<div class="m2"><p>که می‌گردند چوم معشوق و عاشق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زلیخا خفته و یوسف نهفته</p></div>
<div class="m2"><p>نه نام و نی نشان هم شنفته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز بیرون آگهی نه وز درون سوی</p></div>
<div class="m2"><p>به هم ناز و نیاز اندر تک وپوی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نیاز وناز را رایت به عیوق</p></div>
<div class="m2"><p>نه عاشق زان هنوز آگه نه معشوق</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز راه نسبت هر روح با روح</p></div>
<div class="m2"><p>دری از آشنایی هست مفتوح</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از این در کان به روی هر دو باز است</p></div>
<div class="m2"><p>ره آمد شد ناز و نیاز است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>میان آن دو دل کاین در بود باز</p></div>
<div class="m2"><p>بود در راه دایم قاصد راز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر عالم همه گردند همدست</p></div>
<div class="m2"><p>گمان این مبرکاین در توان بست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بود هرجا دری از خشت و از گل</p></div>
<div class="m2"><p>برآوردن توان الا در دل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تنی سهل است کردن از تنی دور</p></div>
<div class="m2"><p>دل از دل دور کردن نیست مقدور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در آن قربی که باشد قرب جانی</p></div>
<div class="m2"><p>خلل چون افکند بعد مکانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تن از تن دور باشد هست مقدور</p></div>
<div class="m2"><p>بلا باشد که باشد جان ز جان دور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>غرض گر آشناییهای جانست</p></div>
<div class="m2"><p>چه غم گر سد بیابان در میانست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که مجنون خواه در حی ، خواه در دشت</p></div>
<div class="m2"><p>به جولانگاه لیلی می‌کند گشت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نهانی صحبت جانها به جانها</p></div>
<div class="m2"><p>عجب مهریست محکم بر دهانها</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خوش آن صحبت که آنجا بار تن نیست</p></div>
<div class="m2"><p>نگهبان را مجال دم زدن نیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو دایم در میان راز می‌باش</p></div>
<div class="m2"><p>پس دیوار گو غماز می‌باش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در آن صحبت که جان دردسر آرد</p></div>
<div class="m2"><p>که باشد دیگری تا دم برآرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به شهوت قرب تن با تن ضرور است</p></div>
<div class="m2"><p>میان عشق و شهوت راه دور است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به شهوت قرب جسمانی‌ست ناچار</p></div>
<div class="m2"><p>ندارد عشق با این کارها کار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز بعد ظاهری خسرو زند جوش</p></div>
<div class="m2"><p>که خواهد دست با شیرین در آغوش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو پاک است از غرضها طبع فرهاد</p></div>
<div class="m2"><p>ز قرب و بعد کی می‌آیدش یاد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز شیرین نیست حاصل کام پرویز</p></div>
<div class="m2"><p>از آن پوید به بازار شکر تیز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ندارد کوهکن کامی ، که ناکام</p></div>
<div class="m2"><p>به کوی دیگرش باید زدی گام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به شغل سد هوس خسرو گرفتار</p></div>
<div class="m2"><p>به حکم حسن شیرین کی کند کار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بباید جست بیکاری چو فرهاد</p></div>
<div class="m2"><p>که بتوانش پی کاری فرستاد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نهد حسن از پی کار دلی پای</p></div>
<div class="m2"><p>که بتواند شد او را کارفرمای</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>رود خوبی شیرین عشق گویان</p></div>
<div class="m2"><p>نشان خانهٔ فرهاد جویان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بدان کش کار فرمایی بود کار</p></div>
<div class="m2"><p>سراغ کارکن امریست ناچار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نیاید کارها بی کارکن راست</p></div>
<div class="m2"><p>اگر چه عمده سعی کارفرماست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>درین خرم اساس دیر بنیاد</p></div>
<div class="m2"><p>به چیزی خاطر هر کس بود شاد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بود هر دل به ذوق خاص در بند</p></div>
<div class="m2"><p>ز مشغولی به شغل خاص خرسند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>برون از نسبت هر اشتراکی</p></div>
<div class="m2"><p>سرشته هر گلی از آب و خاکی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از آن گل شاخ امیدی دمیده</p></div>
<div class="m2"><p>به نشو خاص ازان گل سر کشیده</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به نوعی گشته هر شاخی برومند</p></div>
<div class="m2"><p>یکی را زهر دربار و یکی قند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مذاق هرکس از شاخی برد بهر</p></div>
<div class="m2"><p>یکی را قند قسمت شد یکی زهر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ولی آنکس که با تلخی کند خوی</p></div>
<div class="m2"><p>نسازد یک جهان زهرش ترش روی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کسی کز قند باشد چاشنی یاب</p></div>
<div class="m2"><p>ز اندک تلخیی گردد عنان تاب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ترش رویش کند یک تلخ بادام</p></div>
<div class="m2"><p>شکر جوید کز آن شیرین کند کام</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو خسرو را به زهر آلوده شد قند</p></div>
<div class="m2"><p>ز زهر چشم شیرین شکر خند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نمودش تلخ آن زهر پر از نوش</p></div>
<div class="m2"><p>که دادش عشوهٔ ماه قصب پوش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اگر چه بود شهد زهر مانند</p></div>
<div class="m2"><p>به جانش یک جهان تلخی پراکند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چنان آزرده گشتش طبع نازک</p></div>
<div class="m2"><p>که عاجز گشت نازش در تدارک</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بشد با گریه‌های خنده آلود</p></div>
<div class="m2"><p>لبش پر زهر و زهرش شکر اندود</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>دلش پر شکوه، جانش پرشکایت</p></div>
<div class="m2"><p>ولی خود دیر پروا در حکایت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>درون پرجوش و دل با سینه در جنگ</p></div>
<div class="m2"><p>سوی بازار شکر کرد آهنگ</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مزاج شاه نازک بود بسیار</p></div>
<div class="m2"><p>ندارد طبع نازک تاب آزار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بود نازک دو طبع اندر زمانه</p></div>
<div class="m2"><p>که جویند از پی رنجش بهانه</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>یکی طبع شهان و شهریاران</p></div>
<div class="m2"><p>یکی از گلرخان و گلعذاران</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ز طبع زود رنج پادشاهان</p></div>
<div class="m2"><p>مپرس از من ، بپرس از دادخواهان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ز خوی دیر صلح فتنه سازان</p></div>
<div class="m2"><p>بپرس از من ، مپرس از بی نیازان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کسی زین هر دو گر خود بهره‌مند است</p></div>
<div class="m2"><p>که داند خشم و ناز او که چند است</p></div></div>