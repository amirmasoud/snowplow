---
title: >-
    بخش ۵ - در چگونگی شبی که پیغمبر بر آسمان بر شد
---
# بخش ۵ - در چگونگی شبی که پیغمبر بر آسمان بر شد

<div class="b" id="bn1"><div class="m1"><p>شبی روشنتر از سرچشمهٔ نور</p></div>
<div class="m2"><p>رخ شب در نقاب روز مستور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دمیده صبح دولت آسمان را</p></div>
<div class="m2"><p>ز خواب انگیخته بخت جوان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شک از روز مرغان شب آهنگ</p></div>
<div class="m2"><p>خزیده شیپره در فرجه تنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان روز و شب فرق آنقدر بود</p></div>
<div class="m2"><p>که هر سیاره خورشید دگر بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد از تحت‌الثرا تا اوج افلاک</p></div>
<div class="m2"><p>همه ره چون دلی از تیرگی پاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه روشندلان آسمانی</p></div>
<div class="m2"><p>دوان گرد سرای ام هانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن دولتسرا تا عرش اعظم</p></div>
<div class="m2"><p>ملایک بافته پر در پر هم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمانه چار دیوار عناصر</p></div>
<div class="m2"><p>حلی بربسته ز انواع نوادر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز گوهرها که بوده آسمان را</p></div>
<div class="m2"><p>پر از در کرده راه کهکشان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رهی آراسته از عرش تا فرش</p></div>
<div class="m2"><p>براقی جسته بر فرش از در عرش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>براقی گرمی برق از تکش وام</p></div>
<div class="m2"><p>ز فرشش تا فراز عرش یک گام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندیده نقش پا چشم گمانش</p></div>
<div class="m2"><p>نسوده دست وهم کس عنانش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به مغرب نعلش ار خوردی به خاره</p></div>
<div class="m2"><p>به مشرق بود تا جستی شراره</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازین روی زمین بی‌زخم مهمیز</p></div>
<div class="m2"><p>بر آن سوی زمین جستی به یک خیز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو اوصاف تک و پویش کنم ساز</p></div>
<div class="m2"><p>سخن در گوش تازد پیش از آواز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به هر جا آمده در عرصه پویی</p></div>
<div class="m2"><p>زمین وآسمان طی کرده گویی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به زیر پا درش هنگام رفتار</p></div>
<div class="m2"><p>نمی‌گردید مور خفته بیدار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نبودی چون دل عاشق قرارش</p></div>
<div class="m2"><p>که خواهد جان عالم شد سوارش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خدیو عالم جان شاه «لولاک»</p></div>
<div class="m2"><p>مقیمان درش سکان افلاک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بساط آرای خلوتگاه «لاریب»</p></div>
<div class="m2"><p>سواره ره شناس عرصهٔ غیب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>محمد شبرو «اسرابعبده »</p></div>
<div class="m2"><p>زمان را نظم عقد روز و شب ده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>محمد جمله را سرخیل و سردار</p></div>
<div class="m2"><p>جهان را سنگ کفر از راه بردار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زهی عز براق آن جهانگیر</p></div>
<div class="m2"><p>که پیک ایزدش بودی عنانگیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سرای ام هانی را زهی قدر</p></div>
<div class="m2"><p>که می‌تابید در وی آن مه بدر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بزد جبریل بر در حلقهٔ راز</p></div>
<div class="m2"><p>که بیرون آی و بر کون ومکان تاز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>برون آ یا نبی‌اله، برون آی</p></div>
<div class="m2"><p>برون آ با رخ چون مه برون آی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برون فرما که مه را دل شکسته</p></div>
<div class="m2"><p>ز شوقت بر سر آتش نشسته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عطارد تا ز وصلت مژده بشیند</p></div>
<div class="m2"><p>چو طفل مکتب است اندر شب عید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برون تاز و به حال زهره پرداز</p></div>
<div class="m2"><p>که چنگ طاقتش افتاده از ساز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فرو رفته‌ست خور در آرزویت</p></div>
<div class="m2"><p>تو باقی مانی و خورشید رویت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کشد گر مدت حرمان از این بیش</p></div>
<div class="m2"><p>زند بهرام برخود خنجر خویش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز برجیس و ز کیوان خود چه پرسی</p></div>
<div class="m2"><p>که می‌گرید بر ایشان عرش و کرسی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برون نه گام و لطفی یارشان کن</p></div>
<div class="m2"><p>نگاه رحمتی در کارشان کن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سریر افروز عرش از خوابگاهش</p></div>
<div class="m2"><p>برون آمد دو عالم خاک راهش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به یک عالم زمین داد و زمان داد</p></div>
<div class="m2"><p>به دیگر یک بقای جاودان داد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>براقش پیش باز آمد به تعجیل</p></div>
<div class="m2"><p>دویده در رکاب آویخت جبریل</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رکاب آراست پای احترامش</p></div>
<div class="m2"><p>عنان پیر است دست احتشامش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به سوی مسجد اقصا عنان داد</p></div>
<div class="m2"><p>تک و پو با درخش آسمان داد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز آدم تا مسیحا انبیا جمع</p></div>
<div class="m2"><p>همه پروانه آسا گرد آن شمع</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در آن مسجد امام انبیا شد</p></div>
<div class="m2"><p>خم ابروش محراب دعا شد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پس آنگه خیر باد انبیا کرد</p></div>
<div class="m2"><p>براقش رو به راه کبریا کرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به زیر پی نخستین عرصه پیمود</p></div>
<div class="m2"><p>قمر رخ بر رکاب روشنش سود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فروغی کآمدی کرد از رکابش</p></div>
<div class="m2"><p>ندادی در دو هفته آفتابش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وز آن منزل همان دم کرد شبگیر</p></div>
<div class="m2"><p>دبستان دوم جا ساخت چون تیر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>عطارد لوح خود آورد پیشش</p></div>
<div class="m2"><p>که اینم هست کن نعلین خویشش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو در بزم سوم آوازه انداخت</p></div>
<div class="m2"><p>به چادر زهره ساز خود نهان ساخت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نبودی گر نهان در چادر او</p></div>
<div class="m2"><p>شکستی ساز او را بر سر او</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به کاخ چارمین جا ساخت بر صدر</p></div>
<div class="m2"><p>نهان شد خور ز شرم آن مه بدر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مسیح انجیل زیر آورد از طاق</p></div>
<div class="m2"><p>که جلد مصحف این کهنه اوراق</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به یک حمله که آورد آن جهانگیر</p></div>
<div class="m2"><p>دژ مریخ را فرمود تسخیر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شدش بهرام با تیغ و کفن پیش</p></div>
<div class="m2"><p>که کردم توبه از خون کردن خویش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گذر بردار شرع مشتری کرد</p></div>
<div class="m2"><p>به احکام خود او را رهبری کرد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که بشکن آلت ناهید چنگی</p></div>
<div class="m2"><p>ز خون شو مانع مریخ جنگی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>وز آنجا بر در دیر زحل تاخت</p></div>
<div class="m2"><p>چو او را پیر راهب دید بشناخت</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بگفتنش داده بودندم نشانی</p></div>
<div class="m2"><p>تویی پیغمبر آخر زمانی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شهادت گفت و جان در پای او داد</p></div>
<div class="m2"><p>به شکر خندهٔ حلوای او داد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ثوابت از دو جانب در رسیدند</p></div>
<div class="m2"><p>دو شش درج گهر پیشش کشیدند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نظر بر تحفه‌شان نگشود و درتاخت</p></div>
<div class="m2"><p>ز پیش غیب شادروان برانداخت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گذر بر منتهای سد ره فرمود</p></div>
<div class="m2"><p>به سدره جبرئیلش کرد بدرود</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>عماری دار شد رفرف وز آنجای</p></div>
<div class="m2"><p>به صحن بارگاه قدس زد پای</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تویی برقع برافکند از میانه</p></div>
<div class="m2"><p>دویی شد محو وحدت جاودانه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زبان بیزبانی را ز سر کرد</p></div>
<div class="m2"><p>به گوش جان دلش بشنید و بر کرد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در آن خلوت که آنجا گم شود هوش</p></div>
<div class="m2"><p>نکرد از جمع گمنامان فراموش</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>در آن دیوان نبرد از یاد ما را</p></div>
<div class="m2"><p>خطی آورد و کرد آزاد ما را</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>زبان بستم که سر این حکایت</p></div>
<div class="m2"><p>خدا می‌داند و شاه ولایت</p></div></div>