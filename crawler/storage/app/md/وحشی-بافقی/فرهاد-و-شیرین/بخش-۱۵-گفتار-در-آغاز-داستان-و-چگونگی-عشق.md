---
title: >-
    بخش ۱۵ - گفتار در آغاز داستان و چگونگی عشق
---
# بخش ۱۵ - گفتار در آغاز داستان و چگونگی عشق

<div class="b" id="bn1"><div class="m1"><p>مرا زین گفتگوی عشق بنیاد</p></div>
<div class="m2"><p>که دارد نسبت از شیرین و فرهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غرض عشق است و شرح نسبت عشق</p></div>
<div class="m2"><p>بیان رنج عشق و محنت عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دروغی میسرایم راست مانند</p></div>
<div class="m2"><p>به نسبت می‌دهم با عشق پیوند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که هر نوگل که عشقم می‌نهد پیش</p></div>
<div class="m2"><p>نوایی می‌زنم بر عادت خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آهنگی که مطرب می‌کند ساز</p></div>
<div class="m2"><p>به آن آهنگ می‌آیم به آواز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منم فرهاد و شیرین آن شکرخند</p></div>
<div class="m2"><p>کز آن چون کوهکن جان بایدم کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه فرهاد و چه شیرین این بهانه‌ست</p></div>
<div class="m2"><p>سخن اینست و دیگرها فسانه‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیا ای کوهکن با تیشهٔ تیز</p></div>
<div class="m2"><p>که دارد کار شیرین شکر ریز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو شیرینی ترا شد کارفرمای</p></div>
<div class="m2"><p>بیا خوش پای کوبان پیش نه پای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برو پرویز گو از کوی شیرین</p></div>
<div class="m2"><p>اگر نبود حریف خوی شیرین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که آمد تیشه بر کف سخت جانی</p></div>
<div class="m2"><p>که بگذارد به عالم داستانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنون بشنو در این دیباچهٔ راز</p></div>
<div class="m2"><p>که شیرین می‌رود چون بر سر ناز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تقاضای جمال اینست و خوبی</p></div>
<div class="m2"><p>که شوقی باشد اندر پای کوبی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو خواهد غمزه بر جانی زند نیش</p></div>
<div class="m2"><p>کسی باید که جانی آورد پیش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>و گر گاهی برون تازد نگاهی</p></div>
<div class="m2"><p>تواند تاختن بر قلبگاهی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به عشقی گر نباشد حسن مشغول</p></div>
<div class="m2"><p>بماند کاروان ناز معزول</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو خسرو جست از شیرین جدایی</p></div>
<div class="m2"><p>معطل ماند شغل دلربایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به غایت خاطر شیرین غمین ماند</p></div>
<div class="m2"><p>از آن بی رونقی اندوهگین ماند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز بی یاری دلی بودش چنان تنگ</p></div>
<div class="m2"><p>که بودی با در ودیوار در جنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دلش در تنگنای سینه خسته</p></div>
<div class="m2"><p>به لب جان در خبر گیری نشسته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به جاسوسان سپرده راه پرویز</p></div>
<div class="m2"><p>خبردار از شمار گام شبدیز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر بر سنگ خوردی نعل شبرنگ</p></div>
<div class="m2"><p>وزان خوردن شراری جستی از سنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هنوز آثار گرمی با شرر بود</p></div>
<div class="m2"><p>کز آن در مجلس شیرین خبر بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خبر دادند شیرین را که خسرو</p></div>
<div class="m2"><p>به شکر کرده پیمان هوس نو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از آن پیمان شکن یار هوس کوش</p></div>
<div class="m2"><p>تف غیرت نهادش در جگر نوش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از آن بد عهد دمساز قدم سست</p></div>
<div class="m2"><p>تراوشهای اشکش رخ به خون شست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از آن زخمی که بر دل کارگر داشت</p></div>
<div class="m2"><p>گذار گریه بر خون جگر داشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از آن نیشش که در جان کار می‌کرد</p></div>
<div class="m2"><p>درون سنگ را افکار می‌کرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه غیرت با دلش می‌کرد کاری</p></div>
<div class="m2"><p>کز آسیبش توان کردن شماری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دو جا غیرت کند زور آزمایی</p></div>
<div class="m2"><p>چنان گیرد کز و نتوان رهایی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یکی آنجا که بیند عاشق از دور</p></div>
<div class="m2"><p>ز شمع خویش بزم غیر پر نور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دگر جایی که معشوق وفا کیش</p></div>
<div class="m2"><p>ببیند نوگلی با بلبل خویش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو شیرین را ز طبع غیرت اندوز</p></div>
<div class="m2"><p>شکست اندر دل آن تیر جگر دوز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر آن می‌بود کرد چاره‌ای پیش</p></div>
<div class="m2"><p>که بیرون آردش از سینه ریش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ولی هر چند کوشش بیش می‌کرد</p></div>
<div class="m2"><p>دل خود را فزونتر ریش می‌کرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نه خسرو در دلش جا آنچنان داشت</p></div>
<div class="m2"><p>که آسان مهرش از دل بر توان داشت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو در طبع کسی ذوقی کند جای</p></div>
<div class="m2"><p>عجب دارم کزان بیرون نهد پای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز بیخ و بن درختی کی توان کند</p></div>
<div class="m2"><p>کز آن بر جا نماند ریشه‌ای چند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نهالی بود خسرو رسته زان گل</p></div>
<div class="m2"><p>ز بیخ و ریشه کندن بود مشکل</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نمی‌رفت از دل شیرین خیالش</p></div>
<div class="m2"><p>که با جان داشت پیوند آن نهالش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نه با کس حرف گفتی نه شنفتی</p></div>
<div class="m2"><p>وگر گفتی عتاب آلوده گفتی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به رنجش رفتن پرویز از آن کاخ</p></div>
<div class="m2"><p>بر او اهل حرم را داشت گستاخ</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به آن گستاخ گویان سرایی</p></div>
<div class="m2"><p>نبودش هیچ میل آشنایی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جدایی را بهانه ساز می‌کرد</p></div>
<div class="m2"><p>به هر حرفی عتاب آغاز می‌کرد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زبانش زخم خنجر داشت در زیر</p></div>
<div class="m2"><p>چه خنجر ، زخم زهر آلوده شمشیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کسی کالودهٔ زخمی‌ست جانش</p></div>
<div class="m2"><p>همیشه زهر بارد از زبانش</p></div></div>