---
title: >-
    بخش ۲۳ - گفتار اندر دلربایی شیرین از فرهاد مسکین و گفت و شنید آن دو به طریق راز و نیاز در پردهٔ راز
---
# بخش ۲۳ - گفتار اندر دلربایی شیرین از فرهاد مسکین و گفت و شنید آن دو به طریق راز و نیاز در پردهٔ راز

<div class="b" id="bn1"><div class="m1"><p>خوشا عشق خوش آغاز خوش انجام</p></div>
<div class="m2"><p>همه ناکامی اما اصل هر کام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشا عشق و خوشا عهد خوش عشق</p></div>
<div class="m2"><p>خوشا آغاز سوز آتش عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه آتش است و آتش افروز</p></div>
<div class="m2"><p>مبادا کم که خوش سوزیست این سوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه خوش عهدیست عهد عشقبازی</p></div>
<div class="m2"><p>خصوصا اول این جان گدازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آن شادی که بود اندر زمانه</p></div>
<div class="m2"><p>نهادند از کرانه در میانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو یکجا جمع شد آن شادی عام</p></div>
<div class="m2"><p>شدش آغاز عشق و عاشقی نام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بتان کاردان خوبان پرکار</p></div>
<div class="m2"><p>در آغاز وفا یارند وخوش یار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولیکن از دمی فریاد فریاد</p></div>
<div class="m2"><p>که عشق تازه گردد دیر بنیاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو دید از دور شیرین عاشق نو</p></div>
<div class="m2"><p>سبک در تاخت گلگون سبکرو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به آنجانب که می‌شد در تک و تاز</p></div>
<div class="m2"><p>به جای گردش از ره خاستی ناز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به راه آن غبار توتیاسای</p></div>
<div class="m2"><p>همه تن چشم مرد حیرت افزای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عنان را سست کرده لعبت مست</p></div>
<div class="m2"><p>که آن مسکن بر آن آسان زند دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به خنده مصلحت دیدی فریبش</p></div>
<div class="m2"><p>که چون غارت کند صبر و شکیبش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اداها در بیان دلربایی</p></div>
<div class="m2"><p>نگه‌ها گرم حرف آشنایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به هر گامی که گلگون برگرفتی</p></div>
<div class="m2"><p>اسیر نو نیازی درگرفتی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به استقبال هر جولان نازی</p></div>
<div class="m2"><p>دوانیدی برون خیل نیازی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کشش بود از دو جانب سخت بازو</p></div>
<div class="m2"><p>به میزان محبت هم ترازو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز سویی حسن در زور آزمایی</p></div>
<div class="m2"><p>ز سویی عشق در زنجیر خایی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از آن جانب اشارتها که پیش آی</p></div>
<div class="m2"><p>وز این سو خاکساری ها که کو پای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از آنسو تیغ ناز اندر کف بیم</p></div>
<div class="m2"><p>وز اینجانب سر اندر دست تسلیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به هر گامی شدی نو آرزویی</p></div>
<div class="m2"><p>نهان از لب گذشتی گفتگویی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به سرعت شوق چابک گام می‌رفت</p></div>
<div class="m2"><p>صبوری لب پر از دشنام می‌رفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو آن چابک عنان آمد فرا پیش</p></div>
<div class="m2"><p>به خاک افتاد پیشش آن وفا کیش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سراپا گشت جان بهر سپردن</p></div>
<div class="m2"><p>همه تن سر برای سجده بردن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دعاها با نیاز عشق پرورد</p></div>
<div class="m2"><p>به زیر لب نثار یار می‌کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سری چون بندگان افکنده در پیش</p></div>
<div class="m2"><p>جبینی از سجود بندگی ریش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سراسیمه نگه در چشمخانه</p></div>
<div class="m2"><p>که چون نظاره را یابد بهانه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سراپای وجود از عشق در جوش</p></div>
<div class="m2"><p>همین لب از حدیث عشق خاموش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پری‌رخ را عنان مستانه در دست</p></div>
<div class="m2"><p>نگاهش مست و چشمش مست و خود مست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فریب از گوشه‌های چشم و ابرو</p></div>
<div class="m2"><p>دوانیده برون سد مرحبا گو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نگه در حال پرسی گرم گفتار</p></div>
<div class="m2"><p>نه گوش آگاه از آن نی لب خبردار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تواضعها به رسم عادت وناز</p></div>
<div class="m2"><p>به شرم آراسته انجام و آغاز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برون آورد مستی از حجابش</p></div>
<div class="m2"><p>ولی بسته همان بند نقابش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جمال ناز را پیرایه نو کرد</p></div>
<div class="m2"><p>عبارت را تبسم پیشرو کرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سخن را چاشنی داد از شکر خند</p></div>
<div class="m2"><p>بگفتش خیر مقدم ای هنرمند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بگو تا چیست نامت وز کجایی</p></div>
<div class="m2"><p>که گویا سال ها شد کشنایی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جوابش داد کای ماه قصب پوش</p></div>
<div class="m2"><p>مبادت از خشن پوشان فراموش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سدت مسکین چو من در جان گدازی</p></div>
<div class="m2"><p>همیشه کار تو مسکین نوازی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یکی مسکینم از چین نام فرهاد</p></div>
<div class="m2"><p>غلام تو ولیک از خویش آزاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فکن یا حلقه‌ام در گوش امید</p></div>
<div class="m2"><p>طریق بندگی بین تا به جاوید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بیا این بنده را در بیع خویش آر</p></div>
<div class="m2"><p>پشیمان گر شوی آزادش انگار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به شیرین بذله شیرین شکر ریز</p></div>
<div class="m2"><p>برون داد این فریب عشوه آمیز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که مارا بنده‌ای باید وفادار</p></div>
<div class="m2"><p>که نگریزد اگر بیند سد آواز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>قبول خدمت ما صعب کاریست</p></div>
<div class="m2"><p>در این خدمت دگرگونه شماریست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دلی باید ز آهن، جانی از سنگ</p></div>
<div class="m2"><p>که بتواند زدن در کار ما چنگ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>اگر این جان و دل داری بیا پیش</p></div>
<div class="m2"><p>وگرنه باش بر آزادی خویش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بگفتش کاین دل و جان جای عشق است</p></div>
<div class="m2"><p>وجودم عرصه غوغای عشق است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همیشه کار جورت امتحان باد</p></div>
<div class="m2"><p>دلم را تاب و جانم را توان باد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اگر بر سر زنی تیغ ستیزم</p></div>
<div class="m2"><p>مبادا قوت پای گریزم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مرا آزار کن تا می‌توانی</p></div>
<div class="m2"><p>وفاداری ببین و سخت جانی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دل و جان کردم از فولاد آن روز</p></div>
<div class="m2"><p>که برق این امیدم شد درون سوز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به تابان کوره‌ای در امتحانم</p></div>
<div class="m2"><p>که تا بینی چه فولادیست جانم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بگفتش ترسم این جان چو فولاد</p></div>
<div class="m2"><p>که از سختیش با من می‌کنی یاد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو خوی گرمم آتش برفروزد</p></div>
<div class="m2"><p>اگر یاقوت باشد هم بسوزد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>جوابی گرم گفتش آتش آلود</p></div>
<div class="m2"><p>که اینک جان برآر از خرمنش دود</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در آن وادی که میل دل زند گام</p></div>
<div class="m2"><p>چه باشد جان که او را کس برد نام</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>من و میل تو با میل تو جان چیست</p></div>
<div class="m2"><p>دگر جان را که خواهد دید جان کیست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>شکر لب گفت کاین میل از کجا خاست</p></div>
<div class="m2"><p>بگفت از یک دو حرف آشنا خاست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بگفتش کن چه حرف آشنا بود</p></div>
<div class="m2"><p>بگفتا مژده‌ای چند از وفا بود</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بگفت از گلرخان بیند وفا کس</p></div>
<div class="m2"><p>بگفت این آرزو عشاق را بس</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بگفت این عشقبازان خود کیانند</p></div>
<div class="m2"><p>بگفتا سخت قومی مهربانند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بگفتش تاکی است این مهربانی</p></div>
<div class="m2"><p>بگفتا هست تا گردند فانی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بگفتا چون فنا گردند عشاق</p></div>
<div class="m2"><p>بگفتا همچنان باشند مشتاق</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بگفتش نخل مشتاقی دهد بار</p></div>
<div class="m2"><p>بگفت آری ولی حرمان بسیار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بگفتا درد حرمان را چه درمان</p></div>
<div class="m2"><p>بگفتا وای وای از درد حرمان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بگفتش لاف عشق و ناله بی جاست</p></div>
<div class="m2"><p>بگفتا درد حرمان ناله فرماست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بگفت از صبر باید چاره سازی</p></div>
<div class="m2"><p>بگفتا صبر کو در عشقبازی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بگفت از عشقبازی چیست مقصود</p></div>
<div class="m2"><p>بگفتا رستگی از بود و نابود</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بگفتش می‌توان با دوست پیوست</p></div>
<div class="m2"><p>بگفت آری اگر از خود توان رست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بگفتش وصل به یا هجر از دوست</p></div>
<div class="m2"><p>بگفتا آنچه میل خاطر اوست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ز هر رشته که شیرین عقده بگشاد</p></div>
<div class="m2"><p>یکی گوهر بر آن آویخت فرهاد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>نشد خوبی عنان جنبان نازی</p></div>
<div class="m2"><p>کزان کوته شود دست نیازی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو حسن و عشق در جولانگه ناز</p></div>
<div class="m2"><p>عنان دادند لختی در تک و تاز</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نگهبانان ز هر سو در رسیدند</p></div>
<div class="m2"><p>دو مرغ هم نوا دم در کشیدند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>حکایت ماند بر لب نیم گفته</p></div>
<div class="m2"><p>شکسته مثقب و در نیم سفته</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>سخن را پرده‌ای نو باز کردند</p></div>
<div class="m2"><p>ز پرده نغمه‌ای نو ساز کردند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>اگر چه ظاهرا صورت دگر بود</p></div>
<div class="m2"><p>ولی پنهان نوایی بیشتر بود</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>نوای عشقبازان خوش نواییست</p></div>
<div class="m2"><p>که هر آهنگ او را ره به جاییست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>اگر چه سد نوا خیزد از این چنگ</p></div>
<div class="m2"><p>چو نیکو بنگری باشد یک آهنگ</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>حکایت ماند بر لب نیم گفته</p></div>
<div class="m2"><p>شکسته مثقب و در نیم سفته</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>غرض عشق است اوصاف کمالش</p></div>
<div class="m2"><p>اگر وحشی سراید یا وصالش</p></div></div>