---
title: >-
    بخش ۴۱ - غزل خواندن فرهاد
---
# بخش ۴۱ - غزل خواندن فرهاد

<div class="b" id="bn1"><div class="m1"><p>که بر رویم نگاهی کن خدا را</p></div>
<div class="m2"><p>به صحبت آشنا کن آشنا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بوسی زان لبم بنواز از مهر</p></div>
<div class="m2"><p>مکن پنهان ز رنجوران دوا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گدای کوی تو گشتم به شاهی</p></div>
<div class="m2"><p>به خوان وصل خود بنشان گدا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان عاشقانم کن سر افراز</p></div>
<div class="m2"><p>بنه تا سر نهم بر پات یارا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر خسرو نیم فرهاد عشقم</p></div>
<div class="m2"><p>که از یاری به سر بردم وفا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیم صابر که صبر آرم به هجران</p></div>
<div class="m2"><p>بده کام دلم یا دل خدا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غزل را چون به پایان برد فرهاد</p></div>
<div class="m2"><p>به شیرین گفت از هجر تو فریاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه تلخ است آنچنان کامم ز هجران</p></div>
<div class="m2"><p>که چون خسرو شکرخایم به دندان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بده بوسی از آن لعل چو قندم</p></div>
<div class="m2"><p>که تو عیسی دمی من درد مندم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خمار هجر دارم ده شرابم</p></div>
<div class="m2"><p>که از بهر شراب تو کبابم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل شیرین به حالش سوخت دردم</p></div>
<div class="m2"><p>به ساقی گفت کو آن ساغر جم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیا یک دم ز خود آزاد سازم</p></div>
<div class="m2"><p>خراب از عشق چون فرهاد سازم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شنید و جام پر کرد و به او داد</p></div>
<div class="m2"><p>کشید و داد جامی هم به فرهاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سوم ساغر چو نوشیدند با هم</p></div>
<div class="m2"><p>به صحبت سخت جوشیدند با هم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنین بودند تا شب گشت آن روز</p></div>
<div class="m2"><p>نهان شد چهر مهر عالم افروز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به مغرب شد نهان مهر دل آرا</p></div>
<div class="m2"><p>ز مشرق ماه بدر آمد به بالا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو رخ بنهفت خور بنمود کیوان</p></div>
<div class="m2"><p>چراغان شد ز کوکبهای رخشان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پرستاران شیرین راز گفتند</p></div>
<div class="m2"><p>سخنهایی که باید باز گفتند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که امشب را کجا ؟ چون برسر آری ؟</p></div>
<div class="m2"><p>که را با خود به بزم و بستر آری ؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رود زینجا که و ماند که اینجا ؟</p></div>
<div class="m2"><p>نظر کن تا چه می‌باید به فردا</p></div></div>