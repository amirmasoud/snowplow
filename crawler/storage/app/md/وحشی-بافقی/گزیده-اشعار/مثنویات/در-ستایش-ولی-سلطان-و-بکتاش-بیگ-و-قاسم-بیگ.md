---
title: >-
    در ستایش ولی سلطان و بکتاش بیگ و قاسم بیگ
---
# در ستایش ولی سلطان و بکتاش بیگ و قاسم بیگ

<div class="b" id="bn1"><div class="m1"><p>ای ظفر در رکاب دولت تو</p></div>
<div class="m2"><p>تهنیت خوان فتح و نصرت تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسند آرای ملک امن و امان</p></div>
<div class="m2"><p>قهرمان زمان ولی سلطان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بشارت زند به فتح تومهر</p></div>
<div class="m2"><p>گشته بر کوس چرم گاو سپهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رایتت کز هر آفت است مصون</p></div>
<div class="m2"><p>نفتد عکسش اندر آب نگون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عزم تو چون عنان بجنباند</p></div>
<div class="m2"><p>راه سیارگان بگرداند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قهرت آنجا که در مصاف آید</p></div>
<div class="m2"><p>کار شمشیر از غلاف آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کجا آورد سپاه تو زور</p></div>
<div class="m2"><p>پیل پنهان شود به خانه مور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر صفی کان به جنگت آمده پیش</p></div>
<div class="m2"><p>مرگ خالی نموده ترکش خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر سپاهی که با تو کرده جدل</p></div>
<div class="m2"><p>گشته دندانه دار تیغ اجل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لشکرت گر بر آسمان تازد</p></div>
<div class="m2"><p>آسمان با زمین یکی سازد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تیغ قهرت به باد پیمایی</p></div>
<div class="m2"><p>بر سر خصم کرده میرایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون کند حمله تو رو به عدو</p></div>
<div class="m2"><p>پشت کرده مخالف از همه رو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تیر باران تو کند ز شکوه</p></div>
<div class="m2"><p>زره تنگ حلقه در بر کوه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر کجا تیغ تو سر افرازد</p></div>
<div class="m2"><p>نیزه آنجا منار سر سازد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خنجرت در غلاف فتنه بلاست</p></div>
<div class="m2"><p>چون زبان در دهان اژدرهاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اژدر از دم به کوره تاب دهد</p></div>
<div class="m2"><p>تا حسامت به زهر آب دهد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپرت کآسمان نشان باشد</p></div>
<div class="m2"><p>لشکری را حصار جان باشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دست یازی چو بر کمان ستیز</p></div>
<div class="m2"><p>مرگ خواهد ز تیر پای گریز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تیرت آنجا که پی سپر باشد</p></div>
<div class="m2"><p>دیده مور را خطر باشد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بوم و ملک تو خاک رستم خیز</p></div>
<div class="m2"><p>روبهش ضیغم هژبر ستیز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کرم خاکی به خاک این بروبوم</p></div>
<div class="m2"><p>اژدها سیرت و نهنگ رسوم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بسته در بحر و بر نهنگان راه</p></div>
<div class="m2"><p>دشت بر اژدها نموده سیاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رأی و تدبیرت از خلل خالی</p></div>
<div class="m2"><p>همچو ذات تو رأی تو عالی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عدل تو چون شود صلاح اندیش</p></div>
<div class="m2"><p>گرگ دست آورد به گردن میش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شد ز کوس تو گوش چون سیماب</p></div>
<div class="m2"><p>بانگ تو مضطرش جهاند از خواب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نعل رخشت چو سنگ‌سا گردد</p></div>
<div class="m2"><p>کوه الماس توتیا گردد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شرر از نعلش ار فراز آید</p></div>
<div class="m2"><p>کوه یاقوت در گداز آید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ملک از انصاف تو چنان آباد</p></div>
<div class="m2"><p>که در او جغد کس ندارد یاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جغد در خانه هما چه کند</p></div>
<div class="m2"><p>ظلم در کشور شما چه مند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ظلم ترک دیار تو داده</p></div>
<div class="m2"><p>به دیار مخالف افتاده</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وای بر خصم بخت بر گشته</p></div>
<div class="m2"><p>که تو شمشیر و او سپر گشته</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کار زخم است تیغ بران را</p></div>
<div class="m2"><p>گو سپر چاک زن گریبان را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از بزرگان کسی به سان تو نیست</p></div>
<div class="m2"><p>خاندانی چو خاندان تو نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر یک از خاندان تو جانی</p></div>
<div class="m2"><p>یا جهانگیر یا جهانبانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اول آن نیر بلنداقبال</p></div>
<div class="m2"><p>آفتاب سپهر جاه و جلال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ملک آرای سلطنت پیرای</p></div>
<div class="m2"><p>بی‌عدیل زمان به عدل و به رای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مطلع آفتاب دین و دول</p></div>
<div class="m2"><p>مقطع‌حل و عقد ملک و ملل</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کار فرمای چرخ کار افزای</p></div>
<div class="m2"><p>نسق آرای ملک بار خدای</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از بن و بیخ ظلم برکنده</p></div>
<div class="m2"><p>تخم عدلش ز جا پراکنده</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>صعوه شاهین کش از حمایت تو</p></div>
<div class="m2"><p>باز گنجشک در ولایت تو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شیر گوید ثنای آن روباه</p></div>
<div class="m2"><p>که سگش را بر او فتاده نگاه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رخش او را سپهر غاشیه دار</p></div>
<div class="m2"><p>مدتش را زمانه عاشق زار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نظرش دلگشای دلتنگان</p></div>
<div class="m2"><p>گذرش بوسه گاه سرهنگان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سلطنت مفتخر به خدمت او</p></div>
<div class="m2"><p>تاکی افتد قبول حضرت او</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سایه پرورد ظل یزدانی</p></div>
<div class="m2"><p>نام او زیب خاتم جانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر امان از گزند خواهد کس</p></div>
<div class="m2"><p>نام عباس بیگ حرزش و بس</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>طرفه نامی که ورد مرد و زن است</p></div>
<div class="m2"><p>حر ز جان است و هیکل بدن است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>عین این نام عقل را تاج است</p></div>
<div class="m2"><p>به همین تاج عقل محتاج است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بای این اسم بای بسم الله</p></div>
<div class="m2"><p>الف او ستون خیمه چاه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سین او بر سر ستم اره</p></div>
<div class="m2"><p>به مسمای او جهان غره</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>غره گشته بدو جهان و بجاست</p></div>
<div class="m2"><p>زانکه کار جهان از او به نواست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>عالم از ذات او مکرم باد</p></div>
<div class="m2"><p>تا قیامت پناه عالم باد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بر سرش ظل خسروی بادا</p></div>
<div class="m2"><p>پشت نواب از او قوی بادا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بر سرم سایه‌اش مخلد باد</p></div>
<div class="m2"><p>لطف بسیار او یکی سد باد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>وصف بکتاش بیگ چون گویم</p></div>
<div class="m2"><p>به که همت ز همتش جویم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تا نباشد سخن چو همت او</p></div>
<div class="m2"><p>نتوان کرد وصف حضرت او</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تا نباشد بلندی سخنم</p></div>
<div class="m2"><p>دست بر دامنش چگونه زنم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>رفعتش کانچنان بلند رواست</p></div>
<div class="m2"><p>زانسوی چرخ آسمان نواست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>عقل و دولت موافقت کردند</p></div>
<div class="m2"><p>از گریبانش سر بر آوردند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>عقل او حل و عقد را قانون</p></div>
<div class="m2"><p>دولتش دین و داد را مضمون</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خاطرش صبح دولت جاوید</p></div>
<div class="m2"><p>رای او نور دیدهٔ خورشید</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آفتاب ار به خاطرش گذرد</p></div>
<div class="m2"><p>سایه کوه جاودان ببرد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>همه کارش به دانش و فرهنگ</p></div>
<div class="m2"><p>مور در صلح و اژدها در جنگ</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>قهر او آتش نهنگ گذار</p></div>
<div class="m2"><p>زو سمندر به بحر آتش بار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>لطف او مرگ را حیات دهد</p></div>
<div class="m2"><p>به حیات ابد برات دهد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به خدا راست آشکار و نهانش</p></div>
<div class="m2"><p>کرده رفع دویی دلش به زبانش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>فخر گو بر زمانه کن پدری</p></div>
<div class="m2"><p>کش خدا بخشد آنچنان پسری</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نه پسر بلکه کوه فر و شکوه</p></div>
<div class="m2"><p>زو پدر پشت باز داده به کوه</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تا ابد یارب آن پسر باشد</p></div>
<div class="m2"><p>بر مراد دل پدر باشد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>با منش آنقدر عنایت باد</p></div>
<div class="m2"><p>که زبان شرح آن نیارد داد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>خواهم از در هزار دریا پر</p></div>
<div class="m2"><p>تاکند آن هزار دریا در</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>همه ایثار نام قاسم بیگ</p></div>
<div class="m2"><p>پس شوم عذر خواه قاسم بیگ</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>گر هزاران جهان در و گهر است</p></div>
<div class="m2"><p>در نثارش متاع مختصر است</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بود و نابود پیش او همرنگ</p></div>
<div class="m2"><p>کوه با کاه نزد او همسنگ</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>در شمارش یک و هزار یکی</p></div>
<div class="m2"><p>خاک را با زر اعتبار یکی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>گنج عالم برش پشیزی نیست</p></div>
<div class="m2"><p>هیچ چیزش به چشم چیزی نیست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>یکتنه چون به کارزار آید</p></div>
<div class="m2"><p>گوییا یک جهان سوار آید</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چون زند نعره و کشد شمشیر</p></div>
<div class="m2"><p>باز گردد به سینه غرش شیر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بجهد تیغش از چنار چو مار</p></div>
<div class="m2"><p>زندش گر به سالخورده چنار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چون کشد بر کمان سخت خدنگ</p></div>
<div class="m2"><p>شست صافش کند مشبک سنگ</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>نیزه چون افکند به نیزه مهر</p></div>
<div class="m2"><p>مهر افتد نگون ز رخش سپهر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>گر ز باران ابر آزاری</p></div>
<div class="m2"><p>سپهی را کند سپر داری</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نگذارد که تیر آن باران</p></div>
<div class="m2"><p>بر سپه بارد و سپه داران</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>با نهیبش ز خصم رفته سکون</p></div>
<div class="m2"><p>جسته از حلقه زره بیرون</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>در صف رزم تیغ بهرام است</p></div>
<div class="m2"><p>در گه بزم زهره را جام است</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>جام زهر است یعنی اصل سرور</p></div>
<div class="m2"><p>خرم آنجا که او نمود عبور</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>تیغ بهرام یعنی آنسان تیز</p></div>
<div class="m2"><p>که ز سهمش اجل نمود گریز</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>خاطرش آتش ستاره شرار</p></div>
<div class="m2"><p>طبع وقادش آب آتشبار</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>فکرتش فرد گرد تنها سیر</p></div>
<div class="m2"><p>سد بیابان از او به مسلک غیر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>گر همه سحر بارد از رقمش</p></div>
<div class="m2"><p>سر فرو ناورد بدان قلمش</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>نه بدانسانش همت است بلند</p></div>
<div class="m2"><p>که به اعجاز هم شود خرسند</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>طبع عالیش چون نشست به قدر</p></div>
<div class="m2"><p>پیش او سحر را چه عزت و قدر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>تازگی خانه زاد فکرت او</p></div>
<div class="m2"><p>نازکی بنده طبیعت او</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>سخنش معجزی‌ست سحر نمای</p></div>
<div class="m2"><p>خاطرش آتشی‌ست آب گشای</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>هرکجا شد سلیقه‌اش معمار</p></div>
<div class="m2"><p>برد قلاب زحمت از بازار</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>شعر تا در پناه خاطر اوست</p></div>
<div class="m2"><p>هست مقبول طبع دشمن و دوست</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>علم را در پناه پوینده</p></div>
<div class="m2"><p>درجات کمال جوینده</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>شعر را کرده در به دولت باز</p></div>
<div class="m2"><p>بر درش یک جهان سخن پرداز</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>جمله را حامی و پناه همه</p></div>
<div class="m2"><p>خسرو جمله پادشاه همه</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>در ترقی همه به تربیتش</p></div>
<div class="m2"><p>ناز پروردگان مکرمتش</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>مجلس آرای عیش خوش نقشان</p></div>
<div class="m2"><p>بهترین شخص برگزیده لسان</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>باد از صدر تا به صف نعال</p></div>
<div class="m2"><p>مفتخر مجلسش ز اهل کمال</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>دو گرامی برادر نامی</p></div>
<div class="m2"><p>کآمدند اصل نیک فرجامی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>دو دلاور، دو شیر دل ، دو دلیر</p></div>
<div class="m2"><p>کب گردد ز حمله شان دل شیر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>دو بهادر، دو مرد مردانه</p></div>
<div class="m2"><p>دو دلیر و دو شیر فرزانه</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>پشت بر پشت او نهاده چو کوه</p></div>
<div class="m2"><p>هریکی ز آن دو سد جهان شکوه</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>هر سه بسته کمر به خدمت سخت</p></div>
<div class="m2"><p>پیش هر یک ستاده دولت و بخت</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>در رکاب خدایگان باشند</p></div>
<div class="m2"><p>نه که تا حشر جاودان باشند</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>ظل نواب باد بر سرشان</p></div>
<div class="m2"><p>سد چو وحشی بود ثناگرشان</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>پدران و برادران و همه</p></div>
<div class="m2"><p>راعی خلق و خلقشان چو رمه</p></div></div>