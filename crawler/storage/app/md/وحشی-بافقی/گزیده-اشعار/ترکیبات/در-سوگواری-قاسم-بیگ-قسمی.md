---
title: >-
    در سوگواری قاسم‌بیگ قسمی
---
# در سوگواری قاسم‌بیگ قسمی

<div class="b" id="bn1"><div class="m1"><p>پشت من بشکست کوه درد جان فرسای من</p></div>
<div class="m2"><p>باز افزاید همان این درد کار افزای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشت چشمم ژرف دریایی وآتش خون دل</p></div>
<div class="m2"><p>شاخ مرجان اندر او مژگان خون پالای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تخته‌ای زین نه سفینه کس نبیند بر کنار</p></div>
<div class="m2"><p>گر رود بر اوج از اینسان موجهٔ دریای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پاسبان گنج را ماند، شده گنجش به باد</p></div>
<div class="m2"><p>الحذر از دود آه اژدها آسای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه چو مرغابی و گاهم چون سمندر پرورند</p></div>
<div class="m2"><p>اشک دریاآفرین و آه دوزخ زای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان چو سیمابم در آتش زین در آبم چون نمک</p></div>
<div class="m2"><p>تا بخود بینم نه ترکیب است و نه اجزای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز عیشی خواستم زاید چه دانستم که چرخ</p></div>
<div class="m2"><p>حامله دارد به سد ماتم شب یلدای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون به خاک گلخنم شد جبهه فرسا روزگار</p></div>
<div class="m2"><p>دفع درد سر مکن گو بخت سندل سای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ماتمی گشتند اجزای وجودم دور نیست</p></div>
<div class="m2"><p>گر ز داغ تو سیه پوشید سر تا پای من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پای تا سر داغ گشتم دل سرا پا درد شد</p></div>
<div class="m2"><p>چند نالم وای دل تا چند سوزم وای من</p></div></div>
<div class="b2" id="bn11"><p>چرخ نیلی خم پلاسم برد و ازرق فام کرد</p>
<p>و ز تپانچه روی من رنگ پلاسم وام کرد</p></div>
<div class="b" id="bn12"><div class="m1"><p>جامه نیلی گشت و از سیلی رخم نیلوفری</p></div>
<div class="m2"><p>عاقبت این بود رنگم زین خم خاکستری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آب چشم از دامنم نیل آب و بر اطراف خاک</p></div>
<div class="m2"><p>رود نیلی دیده‌ام در فرش ماتم گستری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بسکه موج رود نیل چشم من بر اوج رفت</p></div>
<div class="m2"><p>شد گیاه نیل سبز از مرغزار اخضری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در مصیبت خانه‌ام پاگشت کاهی لاجرم</p></div>
<div class="m2"><p>کاه برگی شد تن کاهیده‌ام از لاغری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود در دستم سلیمانی نگینی ، گم شده‌ست</p></div>
<div class="m2"><p>بی جهت قدم نشد چون حلقهٔ انگشتری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دیده مکروه بین را نوک مژگان بهر چیست</p></div>
<div class="m2"><p>باری از خنجر نگردد کاش کردی نشتری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زور بازو می‌نماید چرخ چون پشتم شکست</p></div>
<div class="m2"><p>بیش از ین بایست با من کردش این زور آوری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در ربود از حقه‌ام تریاق چرخ مهره باز</p></div>
<div class="m2"><p>وین زمانم می‌کند در جیب افعی پروری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گور خود کندم به ناخن خاک آن بر سرکنان</p></div>
<div class="m2"><p>دستم آمد با کفن دوزی ز پیراهن دری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سوگواران مجلسی دارند و خون در گردش است</p></div>
<div class="m2"><p>من در آن مجلس فرو رفته ز جام آخری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>افسر افشار بردی تا نهی برفرق خویش</p></div>
<div class="m2"><p>فکر خود کن ای فلک کاری نکردی سرسری</p></div></div>
<div class="b2" id="bn23"><p>اینکه قاسم بیگ قسمی کشته شد تحریک تست</p>
<p>هر چه شد از شومی روی شب تاریک تست</p></div>
<div class="b" id="bn24"><div class="m1"><p>یارب آن شب کز جهان می‌بست بار درد عشق</p></div>
<div class="m2"><p>برد ازین عالم به آن عالم چه راه آورد عشق</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خون او گلگونهٔ رخسارهٔ جور است از آنک</p></div>
<div class="m2"><p>شد شهید و رو نگردانید از ناورد عشق</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عاشق مردانه رفت و حسرت سد مرده برد</p></div>
<div class="m2"><p>پر بگردد حسن چون او کم بیابد مرد عشق</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حسن باقی ای بسا لطفی که در کارش کند</p></div>
<div class="m2"><p>زانکه روحی برد از این عالم بلا پرورد عشق</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رفت تا بی دوست سوزد از تف جانش بهشت</p></div>
<div class="m2"><p>واتش دوزخ کند افسرده ز آه سرد عشق</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>روز استقبال روحش آمدند از راه خلد</p></div>
<div class="m2"><p>روح مجنون پیش و در پس سد بیابان گرد عشق</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بد قماریهای شطرنج مجازی خوش نکرد</p></div>
<div class="m2"><p>رفت تا جایی که می‌بازند خاصان نرد عشق</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>می‌شد و می‌گفت روحش با تن بسمل شده</p></div>
<div class="m2"><p>حلق خونین و رخ زرد است سرخ و زرد عشق</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عشق باخود برد و عالم با هوسناکان گذشت</p></div>
<div class="m2"><p>زانکه عشق اندر خور او بود و او در خورد عشق</p></div></div>
<div class="b2" id="bn33"><p>ماتم عشق وعزای او چه با عالم نکرد</p>
<p>کیست در عالم که برخود نوحه ماتم نکرد</p></div>
<div class="b" id="bn34"><div class="m1"><p>اهل نطق از گریه شست وشوی دفتر کرده‌اند</p></div>
<div class="m2"><p>رخت بخت خود بدان آب سیه تر کرده‌اند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سوخته اهل سخن اوراق و کلک و هر چه هست</p></div>
<div class="m2"><p>کرده پس خاکسترش در مشت و بر سر کرده‌اند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>برق کز دل جسته تا عالم بسوزد هم ز راه</p></div>
<div class="m2"><p>باز گردانیده وندر سینه خنجر کرده‌اند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>توتیان را نی شکر زار تمنا خورده خاک</p></div>
<div class="m2"><p>نوحه خوان چون زاغ مشکین جامه در بر کرده‌اند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در کسوف گل شده خورشید و حربا فطرتان</p></div>
<div class="m2"><p>خویش را زندانی سوراخ شپر کرده‌اند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در زده آتش به آب بحر غواصان فکر</p></div>
<div class="m2"><p>مسکن مرغابیان جای سمندر کرده‌اند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گرم طبعان در فلک آتش فکنده و اختران</p></div>
<div class="m2"><p>کسوت خاکستری در بر چو اخگر کرده‌اند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گشته در کوه و کمر وحشی نهادان و ز عقاب</p></div>
<div class="m2"><p>بهر پرواز عدم دریوزهٔ پر کرده‌اند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خانه‌ای ترتیب داده فرقه گم کرده گنج</p></div>
<div class="m2"><p>وندر آن دهلیزه کام و حلق اژدر کرده‌اند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بهر ثبت این مصیبت نامه ارباب قلم</p></div>
<div class="m2"><p>در دوات دیده کلک از نوک نشتر کرده‌اند</p></div></div>
<div class="b2" id="bn44"><p>ماتم صعب است کامد پیش ارباب سخن</p>
<p>گو سخن هم در سیاهی شو چو اصحاب سخن</p></div>
<div class="b" id="bn45"><div class="m1"><p>سخت نادانسته کاری کرد چرخ و اخترش</p></div>
<div class="m2"><p>درسر این کار خواهد رفت زرین افسرش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وای بر اختر که مردی را که خنجر بر شکافت</p></div>
<div class="m2"><p>زهرهٔ چرخ آب می‌گردد هنوز از خنجرش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بی گمان ناگاه تیرش می‌جهد بر پشت چرخ</p></div>
<div class="m2"><p>سوده خود بر دست او یک بار پیکان و برش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شهسوار ما که چوبین اسب زیر ران کشید</p></div>
<div class="m2"><p>مرکب زرینه زین گو خاک می‌خور بر درش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مرکبی کش دم بریدند ار بود رخش سپهر</p></div>
<div class="m2"><p>غاشیه شال سیه زیبد پی زین زرش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بر سر تربت چه حاصل تاج زر بر سندلی</p></div>
<div class="m2"><p>تاجداری را که بر خاک لحد باشد سرش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گر بود تاج زر خور چون ز سر خالی بماند</p></div>
<div class="m2"><p>تاج پوشی نیست از خاک سیه لایقترش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در جهان نایاب شد خاک سیه چون کیمیا</p></div>
<div class="m2"><p>بس کزین ماتم به سر کردند در هر کشورش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سوگواران رایگان دانند و از گردون خزند</p></div>
<div class="m2"><p>قیمت مشک ار نهد بر تودهٔ خاکسترش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>این که می‌خوانی شبش روز است رفته در عزا</p></div>
<div class="m2"><p>گشته شب عریان و کردهٔ جامهٔ خود در برش</p></div></div>
<div class="b2" id="bn55"><p>نی همین ما را سیه پوشید و ماتم دار کرد</p>
<p>این مصیبت در شب و روز زمانه کار کرد</p></div>
<div class="b" id="bn56"><div class="m1"><p>بومی آمد نامهٔ عنوان سیه بر بال او</p></div>
<div class="m2"><p>نامه‌ای بتر ز روی نامبارک فال او</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خانه شهری سیه گردد ز بال افشانیش</p></div>
<div class="m2"><p>بر که خواهد سایه افکندن بدا احوال او</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هر گه این بوم آمد و بر طرف بامش پر گشاد</p></div>
<div class="m2"><p>صحن گلخن گشت سقف خانه اقبال او</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>از همه دیوار ما کوتاه‌تر دید و نشست</p></div>
<div class="m2"><p>نامه‌ای چون پر زاغ او زبان حال او</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نامه‌ای پیچیده طومار مصیبت را تنور</p></div>
<div class="m2"><p>گریه‌ها پوشیده در تفصیل و در اجمال او</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نامه‌ای سر تا سر او ای دریغا ای دریغ</p></div>
<div class="m2"><p>در نوشتن کرده کاتب اشکی از دنبال او</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نام قاسم بیگی قسمی به خون‌آغشته حرف</p></div>
<div class="m2"><p>بسکه در وقت رقم می‌رفت اشک آل او</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>زخم موری کشته شیری را بلی لغزد چو پای</p></div>
<div class="m2"><p>پشه ای پیش آید و پیلی شود پامال او</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>آن بریده سر که بر دست این خطا رفتش که بود</p></div>
<div class="m2"><p>زهره‌اش بشکافت خوف خنجر قتال او</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پردلی بود او که روبر تیر رفتی سینه چاک</p></div>
<div class="m2"><p>عاشقی می‌کرد می‌گفتی به خط و خال او</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نقش هستی شست و شیر از بیشه اندیشد هنوز</p></div>
<div class="m2"><p>بر کنار بیشه بگذارند اگر تمثال او</p></div></div>
<div class="b2" id="bn67"><p>همچو او مردانه مردی در صف مردان نبود</p>
<p>مرد جنگش اژدها گر بود رو گردان نبود</p></div>
<div class="b" id="bn68"><div class="m1"><p>صولتش کار گوزن و گور آسان کرده بود</p></div>
<div class="m2"><p>کوه و بیشه بر پلنگ و شیر زندان کرده بود</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>اژدها را روزگاری هول مار نیزه‌اش</p></div>
<div class="m2"><p>برده در سوراخ تنگ مور پنهان کرده بود</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>برق تیغش ساختی چون بیشهٔ آتش زده</p></div>
<div class="m2"><p>نیزهٔ شیران اگر دشتی نیستان کرده بود</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ای دریغا آن سبکدستی که خنجر بر کفش</p></div>
<div class="m2"><p>بوسه ناداده ز خون خصم توفان کرده بود</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>کاسه گو خود را اگر دادی به سگبانش سپهر</p></div>
<div class="m2"><p>او کنون این نه قرابه سنگباران کرده بود</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سینه ماهی و پشت گاو در هم داشت راه</p></div>
<div class="m2"><p>تیغ را تا دست او ایما به یلمان کرده بود</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>آگهی زین زود رفتن داشت کز آغاز عمر</p></div>
<div class="m2"><p>خیر بادا هرچه بودش تا سر و جان کرده بود</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>دخل مستقبل به راه خرج ماضی ریخته</p></div>
<div class="m2"><p>نقد حال خویش را با نسیه یکسان کرده بود</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>هر چه در دامان دریا بود و اندر جیب کان</p></div>
<div class="m2"><p>اهل حاجت را همه در جیب و دامان کرده بود</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>اینکه جان و سر نمی‌بخشید بود از بهر آنک</p></div>
<div class="m2"><p>سرطفیل دوستان ، جان وقت جانان کرده بود</p></div></div>
<div class="b2" id="bn78"><p>همت او چشم بر دنیا و مافیها نداشت</p>
<p>نسبتی با مردم بی‌حالت دنیا نداشت</p></div>
<div class="b" id="bn79"><div class="m1"><p>تاجداران را سری بود و سران را افسری</p></div>
<div class="m2"><p>کش نیابی سد یک او گر بگردی کشوری</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>روز احسان جود سر تا پا ، سر تا پا کرم</p></div>
<div class="m2"><p>قلزمی نیسان ، غلامی ابر، عمان چاکری</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>روز میدان پای تا سر دل ، ز سر تا پا جگر</p></div>
<div class="m2"><p>شیر هیبت ، صف شکافی ، تیر صولت ، صفدری</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>تیغ او چون در نبردی با اجل گشتی قرین</p></div>
<div class="m2"><p>تا اجل کشتی یکی ، او کشته بودی لشکری</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>دود روزن بودی آتشگاه قهرش را سپهر</p></div>
<div class="m2"><p>دوزخ تابیده در خاکستر او اخگری</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>همچو اویی زین کهن ترکیب ناید در وجود</p></div>
<div class="m2"><p>عنصری ازنو مگر سازند و چرخ و اختری</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چرخ خوش دیر آشکارا کرد و پنهان ساخت زود</p></div>
<div class="m2"><p>گوهر ذاتش که مثلش کس ندیده جوهری</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>درج را سر بر گشاید دیر و زودش سر نهد</p></div>
<div class="m2"><p>جوهری را چون بود در درج نادر گوهری</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>لاف یکرنگی و او خونین کفن در خاک و من</p></div>
<div class="m2"><p>نی به سینه دشنه‌ای رانده نه بر دل خنجری</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>شرم بادا روی خویشم این عزا باشد که کس</p></div>
<div class="m2"><p>مشت کاهی پاشد و بر سر کند خاکستری</p></div></div>
<div class="b2" id="bn89"><p>بود این حق وفا الحق که ریزم خون خویش</p>
<p>هم درون خود کشم در خون و هم بیرون خویش</p></div>
<div class="b" id="bn90"><div class="m1"><p>بود این شرط عزا کاول وداع جان کنم</p></div>
<div class="m2"><p>جسم را آنگه سزای خوش در دامان کنم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>سنگ بردارم هنوزم جان برون ننهاده رخت</p></div>
<div class="m2"><p>تا رود غمخانهٔ تن بر سرش ویران کنم</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>لیکن این تدبیرها خواهد فراغ خاطری</p></div>
<div class="m2"><p>خود کرا پروا که گوید این کنم یا آن کنم</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>غیر از این ناید ز من که آتش برآرم از جگر</p></div>
<div class="m2"><p>اشک و آهی از پی تسکین دل سامان کنم</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>سردهم هر دم شط خونی به روی روزگار</p></div>
<div class="m2"><p>لخت ابری هر نفش در چرخ سر گردان کنم</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>یاد خواهد کرد عالم زاب توفان زای نوح</p></div>
<div class="m2"><p>گر تنور سینه خواهم کاتشین توفان کنم</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>از شکاف سینه این توفان برون خواهد نهاد</p></div>
<div class="m2"><p>در قفس این باد را تا چند در زندان کنم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>دود برمی‌آورد از آب برق آه من</p></div>
<div class="m2"><p>به که بر قلزم بگریم نوحه بر عمان کنم</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>آب ابر چشم من توفان آتش چون کشد</p></div>
<div class="m2"><p>دجله‌ای گیرم که در هر قطره‌اش پنهان کنم</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>اینهمه دشوار در راه است عالم را ز من</p></div>
<div class="m2"><p>خنجری کو تا من این دشوارها آسان کنم</p></div></div>
<div class="b2" id="bn100"><p>بر شکافم سینه وز تشویش عالم وارهم</p>
<p>عالم از من وارهد من هم ز ماتم وارهم</p></div>
<div class="b" id="bn101"><div class="m1"><p>خشک شد بحری که دهرش کان گوهر می‌نهاد</p></div>
<div class="m2"><p>گوهری از وی به خشک و تر برابر می‌نهاد</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>آفتابی شد فرو کز خاطرش در کان عهد</p></div>
<div class="m2"><p>آسمان گنجینه‌های پر ز گوهر می‌نهاد</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>مهر بر لب زد سخن سنجی که چون لب می‌گشود</p></div>
<div class="m2"><p>قفل حیرت بر زبان هر سخنور می‌نهاد</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>فاقدی پرداخت جای از خود که در میزان قدر</p></div>
<div class="m2"><p>نکته‌ای را در مقابل بدره زر می‌نهاد</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>طایری پر ریخت کاو را وقت پرواز بلند</p></div>
<div class="m2"><p>مرغ شاخ سدره ، سدره بوسه بر پر می‌نهاد</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>خسروی منشور معنی شست کز دیوان او</p></div>
<div class="m2"><p>چرخ هر جا یک رقم میدید بر سر می‌نهاد</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>آب می‌شد اختر از شرم و فرو می‌شد به خاک</p></div>
<div class="m2"><p>در نطقش کز فلک پهلوی اختر می‌نهاد</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>در مبارز خانهٔ معنی زبان تیر او</p></div>
<div class="m2"><p>بر گلوی حرف گیران نوک خنجر می‌نهاد</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>دفتر او را زمان شیرازه می‌بست و سپهر</p></div>
<div class="m2"><p>دفتر اقران برای جلد دفتر می‌نهاد</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>دست ننهادی اگر بر سینهٔ او روزگار</p></div>
<div class="m2"><p>پای بر معراج نطق از جمله برتر می‌نهاد</p></div></div>
<div class="b2" id="bn111"><p>از سخن گر طالعی می‌داشتند آیندگان</p>
<p>ای بسا دفتر کزو می‌ماند با پایندگان</p></div>
<div class="b" id="bn112"><div class="m1"><p>طایر روحش که مرغی بود علوی آشیان</p></div>
<div class="m2"><p>چند روزی گشت صید دام این سفلی مکان</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>در مضیق این قفس سد کسرش اندر بال و پر</p></div>
<div class="m2"><p>ز آفت این دامگه سد نقصش اندر جسم و جان</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>چنگل شاهین آزارش به جای دست شاه</p></div>
<div class="m2"><p>کلبهٔ صیاد خونخوارش به جای بوستان</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>کرده گم بستان اصلی پرفشان بی‌اختیار</p></div>
<div class="m2"><p>در خزان بی‌بهار و در بهار بی‌خزان</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>ز آشیان بی‌نشان در چار دیوار مقیم</p></div>
<div class="m2"><p>و آمده بال و پرش سنگ حوادث را نشان</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>سر به زیر بال دایم ز آفت گرد فتور</p></div>
<div class="m2"><p>وز غبار آن همیشه بال و پروازش گران</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>ناگهان آمد صفیری ز آشیان سدره‌اش</p></div>
<div class="m2"><p>گرد بال افشاند و مرغ سدره شد زین خاکدان</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>جای پروازش فراز سدره کن یارب که هست</p></div>
<div class="m2"><p>درخور پرواز بال همتش جای جنان</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>مرغ شاخ سدره گردد هر که این پرواز یافت</p></div>
<div class="m2"><p>آن پرش ده کاو تواند شد به سدره پرفشان</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>آشیانش بر کنار قصر لطف خویش ساز</p></div>
<div class="m2"><p>کای خوشا آن مرغ کش آنجای باشد آشیان</p></div></div>
<div class="b2" id="bn122"><p>وحشی او رفت و نیاید باز از درالسلام</p>
<p>ظل نواب ولی سلطان بماند مستدام</p></div>
<div class="b" id="bn123"><div class="m1"><p>باد تا جاوید عمر و دولت عباس بیگ</p></div>
<div class="m2"><p>ناگزیر دور بادا مدت عباس بیگ</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>باد چون اقبال و دولت در سجود دایمی</p></div>
<div class="m2"><p>سلطنت در قبله‌گاه شوکت عباس بیگ</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>باد تا هستی‌ست بر لشکر گه گیتی محیط</p></div>
<div class="m2"><p>ظل ممتد لوای همت عباس بیگ</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>در امور معظم ار ایام سوگندی خورد</p></div>
<div class="m2"><p>باد سوگند عظیمش عزت عباس بیگ</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>زلزله فرمای نخلستان جان یعنی اجل</p></div>
<div class="m2"><p>باد لزران همچو بید از هیبت عباس بیگ</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>آسمان بربود اگر یک در ز بهر تاج خویش</p></div>
<div class="m2"><p>از سه عالی گوهر پر قیمت عباس بیگ</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>این دو باقی مانده در را تا ابد بادا بقا</p></div>
<div class="m2"><p>بهر زیب و زین تاج رفعت عباس بیگ</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>گر ز پا افتاد نخلی زان دو سرو تازه باد</p></div>
<div class="m2"><p>جاودان سر سبز باغ حشمت عباس بیگ</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>باد روشن زان دو مصباحش شبستان مراد</p></div>
<div class="m2"><p>رفت اگر شمعی ز بزم عشرت عباس بیگ</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>این دو را تا رستخیز از وصل نومیدی مباد</p></div>
<div class="m2"><p>تا به حشر ار برد آن یک حسرت عباس بیگ</p></div></div>
<div class="b2" id="bn133"><p>تا ابد این خاندان را باغ دولت تازه باد</p>
<p>طایر اقبالشان دایم بلندآوازه باد</p></div>