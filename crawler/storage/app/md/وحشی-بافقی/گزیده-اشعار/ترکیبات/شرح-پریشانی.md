---
title: >-
    شرح پریشانی
---
# شرح پریشانی

<div class="b" id="bn1"><div class="m1"><p>دوستان شرح پریشانی من گوش کنید</p></div>
<div class="m2"><p>داستان غم پنهانی من گوش کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قصهٔ بی سر و سامانی من گوش کنید</p></div>
<div class="m2"><p>گفت و گوی من و حیرانی من گوش کنید</p></div></div>
<div class="b2" id="bn3"><p>شرح این آتش جان سوز نگفتن تا کی؟</p>
<p>سوختم سوختم این راز نهفتن تا کی</p></div>
<div class="b" id="bn4"><div class="m1"><p>روزگاری من و دل ساکن کویی بودیم</p></div>
<div class="m2"><p>ساکن کوی بت عربده‌جویی بودیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل و دین باخته، دیوانهٔ رویی بودیم</p></div>
<div class="m2"><p>بستهٔ سلسلهٔ سلسله مویی بودیم</p></div></div>
<div class="b2" id="bn6"><p>کس در آن سلسله غیر از من و دل، بند نبود</p>
<p>یک گرفتار از این جمله که هستند نبود</p></div>
<div class="b" id="bn7"><div class="m1"><p>نرگس غمزه زنش این همه بیمار نداشت</p></div>
<div class="m2"><p>سنبل پر شکنش هیچ گرفتار نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این همه مشتری و گرمی بازار نداشت</p></div>
<div class="m2"><p>یوسفی بود ولی هیچ خریدار نداشت</p></div></div>
<div class="b2" id="bn9"><p>اول آن کس که خریدار شدش من بودم</p>
<p>باعث گرمی بازار شدش من بودم</p></div>
<div class="b" id="bn10"><div class="m1"><p>عشق من شد سبب خوبی و رعنایی او</p></div>
<div class="m2"><p>داد رسوایی من شهرت زیبایی او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس که دادم همه جا شرح دلارایی او</p></div>
<div class="m2"><p>شهر پر گشت ز غوغای تماشایی او</p></div></div>
<div class="b2" id="bn12"><p>این زمان عاشق سرگشته فراوان دارد</p>
<p>کی سر برگ من بی سر و سامان دارد؟</p></div>
<div class="b" id="bn13"><div class="m1"><p>چاره این است و ندارم به از این رای دگر</p></div>
<div class="m2"><p>که دهم جای دگر دل به دل‌آرای دگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چشم خود فرش کنم زیر کف پای دگر</p></div>
<div class="m2"><p>بر کف پای دگر بوسه زنم جای دگر</p></div></div>
<div class="b2" id="bn15"><p>بعد از این رای من این است و همین خواهد بود</p>
<p>من بر این هستم و البته چنین خواهدبود</p></div>
<div class="b" id="bn16"><div class="m1"><p>پیش او یار نو و یار کهن هر دو یکیست</p></div>
<div class="m2"><p>حرمت مدعی و حرمت من هر دو یکیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قول زاغ و غزل مرغ چمن هر دو یکیست</p></div>
<div class="m2"><p>نغمهٔ بلبل و غوغای زغن هر دو یکیست</p></div></div>
<div class="b2" id="bn18"><p>این ندانسته که قدر همه یکسان نبود</p>
<p>زاغ را مرتبهٔ مرغ خوش الحان نبود</p></div>
<div class="b" id="bn19"><div class="m1"><p>چون چنین است پی کار دگر باشم به</p></div>
<div class="m2"><p>چند روزی پی دلدار دگر باشم به</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عندلیب گل رخسار دگر باشم به</p></div>
<div class="m2"><p>مرغ خوش نغمهٔ گلزار دگر باشم به</p></div></div>
<div class="b2" id="bn21"><p>نوگلی کو که شوم بلبل دستان سازش؟</p>
<p>سازم از تازه جوانان چمن ممتازش</p></div>
<div class="b" id="bn22"><div class="m1"><p>آن که بر جانم از او دم به دم آزاری هست</p></div>
<div class="m2"><p>می‌توان یافت که بر دل ز منش باری هست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از من و بندگی من اگرش عاری هست</p></div>
<div class="m2"><p>بفروشد که به هر گوشه خریداری هست</p></div></div>
<div class="b2" id="bn24"><p>به وفاداری من نیست در این شهر کسی</p>
<p>بنده‌ای همچو مرا هست خریدار بسی</p></div>
<div class="b" id="bn25"><div class="m1"><p>مدتی در ره عشق تو دویدیم بس است</p></div>
<div class="m2"><p>راه صد بادیهٔ درد بریدیم بس است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قدم از راه طلب باز کشیدیم بس است</p></div>
<div class="m2"><p>اول و آخر این مرحله دیدیم بس است</p></div></div>
<div class="b2" id="bn27"><p>بعد از این ما و سرکوی دل‌آرای دگر</p>
<p>با غزالی به غزلخوانی و غوغای دگر</p></div>
<div class="b" id="bn28"><div class="m1"><p>تو مپندار که مهر از دل محزون نرود</p></div>
<div class="m2"><p>آتش عشق به جان افتد و بیرون نرود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وین محبت به صد افسانه و افسون نرود</p></div>
<div class="m2"><p>چه گمان غلط است این، برود چون نرود</p></div></div>
<div class="b2" id="bn30"><p>چند کس از تو و یاران تو آزرده شود</p>
<p>دوزخ از سردی این طایفه افسرده شود</p></div>
<div class="b" id="bn31"><div class="m1"><p>ای پسر چند به کام دگرانت بینم</p></div>
<div class="m2"><p>سرخوش و مست ز جام دگرانت بینم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مایه عیش مدام دگرانت بینم</p></div>
<div class="m2"><p>ساقی مجلس عام دگرانت بینم</p></div></div>
<div class="b2" id="bn33"><p>تو چه دانی که شدی یار چه بی باکی چند</p>
<p>چه هوسها که ندارند هوسناکی چند</p></div>
<div class="b" id="bn34"><div class="m1"><p>یار این طایفهٔ خانه برانداز مباش</p></div>
<div class="m2"><p>از تو حیف است به این طایفه دمساز مباش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>می‌شوی شهره، به این فرقه هم‌آواز مباش</p></div>
<div class="m2"><p>غافل از لعب حریفان دغا باز مباش</p></div></div>
<div class="b2" id="bn36"><p>به که مشغول به این شغل نسازی خود را</p>
<p>این نه کاری‌ست، مبادا که ببازی خود را</p></div>
<div class="b" id="bn37"><div class="m1"><p>در کمین تو بسی عیب شماران هستند</p></div>
<div class="m2"><p>سینه پر درد ز تو کینه گذاران هستند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>داغ بر سینه ز تو سینه فکاران هستند</p></div>
<div class="m2"><p>غرض اینست که در قصد تو یاران هستند</p></div></div>
<div class="b2" id="bn39"><p>باش مردانه که ناگاه قفایی نخوری</p>
<p>واقف کشتی خود باش که پایی نخوری</p></div>
<div class="b" id="bn40"><div class="m1"><p>گر چه از خاطر وحشی هوس روی تو رفت</p></div>
<div class="m2"><p>وز دلش آرزوی قامت دلجوی تو رفت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شد دل‌آزرده و آزرده دل از کوی تو رفت</p></div>
<div class="m2"><p>با دل پر گله از ناخوشی خوی تو رفت</p></div></div>
<div class="b2" id="bn42"><p>حاش لله که وفای تو فراموش کند</p>
<p>سخن مصلحت‌آمیز کسان گوش کند</p></div>