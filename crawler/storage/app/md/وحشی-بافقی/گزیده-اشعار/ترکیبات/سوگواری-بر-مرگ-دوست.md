---
title: >-
    سوگواری بر مرگ دوست
---
# سوگواری بر مرگ دوست

<div class="b" id="bn1"><div class="m1"><p>دیده گو اشک ندامت شو و بیرون فرما</p></div>
<div class="m2"><p>دیدن دیده چه کار آیدم از دوست جدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عوض یوسف گم گشته چو اخوان بینید</p></div>
<div class="m2"><p>دیده خوب است به شرطی که بود نابینا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه دانم که نمی‌یابیش ای مردم چشم</p></div>
<div class="m2"><p>باش با اشک من و روی زمین می‌پیما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در قیامت مگرش باز ببینم که فتاد</p></div>
<div class="m2"><p>در میان فاصله ما را ز بقا تا به فنا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یار در قصرچنان مایحه‌ای ذیل جهان</p></div>
<div class="m2"><p>ماکجاییم و تماشاگه دیدار کجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یاد آن یار سفرکرده محمل تابوت</p></div>
<div class="m2"><p>کانچنان راند که نشنید کسش بانگ درا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رسم پیغام و خبر نیست ، مصیبت اینست</p></div>
<div class="m2"><p>به دیاری که سفر کرد سفر کردهٔ ما</p></div></div>
<div class="b2" id="bn8"><p>به چه پیغام کنم خوش دل آزردهٔ خویش</p>
<p>از که پرسم سخن یار سفر کرده خویش</p></div>
<div class="b" id="bn9"><div class="m1"><p>یاد و سد یاد از آن عهد که در صحبت یار</p></div>
<div class="m2"><p>خاطری داشتم از عیش جهان بر خوردار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه مرا چهره‌ای از اشک مصیبت خونین</p></div>
<div class="m2"><p>نه مرا سینه‌ای از ناخن حسرت افکار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاطری داشتم القصه چو خرم باغی</p></div>
<div class="m2"><p>لاله عیش شکفته گل شادی بر بار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آه کان باغ پر از لاله و گل یافت خزان</p></div>
<div class="m2"><p>لاله‌ها شد همه داغ دل و گلها همه خار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برسیده‌ست در این باغ خزانی هیهات</p></div>
<div class="m2"><p>کی دگر بلبل ما را بود امید بهار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بلبلی کش قفس تنگ و پروبال شکست</p></div>
<div class="m2"><p>به چه امید دگر یاد کند از گلزار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر همه روی زمین شد گل و گلزار چه حظ</p></div>
<div class="m2"><p>یار چون نیست مرا با گل و گلزار چه کار</p></div></div>
<div class="b2" id="bn16"><p>یار اگر هست به هر جا که روی گلزار است</p>
<p>گل گلزار که بی یار بود مسمار است</p></div>
<div class="b" id="bn17"><div class="m1"><p>کاشکی نوگل ما چون گل بستان بودی</p></div>
<div class="m2"><p>که چو رفتی گذرش سوی گلستان بودی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کاش چاهی که در او یوسف ما افکندند</p></div>
<div class="m2"><p>راه بازآمدنش جانب کنعان بودی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کاشکی آنکه نهان کشت ز ما یک تن را</p></div>
<div class="m2"><p>بر سرش راه سرچشمهٔ حیوان بودی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شب هجران چه دراز است خصوصا این شب</p></div>
<div class="m2"><p>کاش روزی ز پس این شب هجران بودی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه قدر گریه توان کرد در این غم به دو چشم</p></div>
<div class="m2"><p>کاش سر تا قدمم دیده گریان بودی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آنکه بر مرکب چوبین بنشست و بدواند</p></div>
<div class="m2"><p>کاش اینجا دگرش فرصت جولان بودی</p></div></div>
<div class="b2" id="bn23"><p>سیر از عمر خود و زندگی خویشتنم</p>
<p>نیست پروای خود از بی تو دگر زیستنم</p></div>
<div class="b" id="bn24"><div class="m1"><p>ای سرا پای وجودت همه زخم و غم و درد</p></div>
<div class="m2"><p>اینهمه خنجر و شمشیر به جان تو که کرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هیچ مردی سپهی بر سر یک خسته کشد</p></div>
<div class="m2"><p>روی این مرد سیه باد کش اینست نبرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حال تو آه چه پرسیم چه خواهد بودن</p></div>
<div class="m2"><p>حال مردی که کشندش به ستم سد نامرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>غیر از آن کافتد و از هم بکنندش چه کنند</p></div>
<div class="m2"><p>شیر رنجور چو بینند شغالانش فرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که خبر داشت که چندین دد آدم صورت</p></div>
<div class="m2"><p>بهر جان تو ز خوان تو فلکشان پرورد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سرد مهری فلک با چو تو خون گرمی آه</p></div>
<div class="m2"><p>کردکاری که مرا ساخت ز عالم دل سرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون ترا زیر گل و خاک ببینند افسوس</p></div>
<div class="m2"><p>آنکه دیدن نتوانست به دامان تو گرد</p></div></div>
<div class="b2" id="bn31"><p>مردم از غم ، چه کنم، پیش که گویم غم خویش</p>
<p>همه دارند ترا ماتم و من ماتم خویش</p></div>
<div class="b" id="bn32"><div class="m1"><p>یارب آنها که پی قتل تو فتوا دادند</p></div>
<div class="m2"><p>زندگانی ترا خانه به یغما دادند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یارب آنها که ز خمخانهٔ بیدار ترا</p></div>
<div class="m2"><p>رطل خون درعوض ساغر صهبا دادند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یارب آنها که رماندند ز تو طایر روح</p></div>
<div class="m2"><p>جای آن مرغ به سر منزل عقبا دادند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یارب آنها که نهادند به بالین تو پای</p></div>
<div class="m2"><p>تن بیمار تو بر بستر خون جا دادند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یارب آنها که ز محرومیت ای گوهر پاک</p></div>
<div class="m2"><p>ابر مژگان مرا مایهٔ دریا دادند</p></div></div>
<div class="b2" id="bn37"><p>زنده باشند و به زندان بلایی دربند</p>
<p>کز خدا مرگ شب و روز به زاری طلبند</p></div>