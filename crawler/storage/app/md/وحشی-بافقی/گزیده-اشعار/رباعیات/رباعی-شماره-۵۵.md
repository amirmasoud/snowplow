---
title: >-
    رباعی شمارهٔ ۵۵
---
# رباعی شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>رخسار تو ای تازه گل گلشن جان</p></div>
<div class="m2"><p>کز آبله شبنمی نشسته ست بر آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لاله ست ولی آمده با ژاله قرین</p></div>
<div class="m2"><p>ماهی‌ست ولی کرده به سیاره قران</p></div></div>