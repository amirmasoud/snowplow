---
title: >-
    رباعی شمارهٔ ۲۸
---
# رباعی شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>آنان که به کویی نگران می‌گردند</p></div>
<div class="m2"><p>پیوسته مرا به قصد جان می گردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رشک نبات می‌دهم جان که چرا</p></div>
<div class="m2"><p>گرد سر هم نام فلان می‌گردند</p></div></div>