---
title: >-
    رباعی شمارهٔ ۳۱
---
# رباعی شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>ای چرخ مرا دلی ست بیداد پسند</p></div>
<div class="m2"><p>بیمم دهی از سنگ حوادث تا چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من شیشه نیم که بشکند سنگ توام</p></div>
<div class="m2"><p>مرغ قفسم که گشتم آزاد ز بند</p></div></div>