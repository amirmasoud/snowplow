---
title: >-
    رباعی شمارهٔ ۳۵
---
# رباعی شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>در صید گهت که جان طرب ساز آید</p></div>
<div class="m2"><p>سیمرغ اسیر چنگل بازآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جا که صدای طبل باز تو رسد</p></div>
<div class="m2"><p>سد مرغ دل از شوق به پرواز آید</p></div></div>