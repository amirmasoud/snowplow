---
title: >-
    رباعی شمارهٔ ۱
---
# رباعی شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>یارب که بقای جاودانی بادا</p></div>
<div class="m2"><p>کامت بادا و کامرانی بادا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر اشربه‌ای کز پی درمان نوشی</p></div>
<div class="m2"><p>خاصیت آب زندگانی بادا</p></div></div>