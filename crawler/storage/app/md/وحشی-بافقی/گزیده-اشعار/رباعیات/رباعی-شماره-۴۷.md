---
title: >-
    رباعی شمارهٔ ۴۷
---
# رباعی شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>فن تو و سد هزار برهان کمال</p></div>
<div class="m2"><p>شغل من و یک جهان خیالات محال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو منزوی مدرسهٔ عالی فضل</p></div>
<div class="m2"><p>من بیهده گرد راست بازار خیال</p></div></div>