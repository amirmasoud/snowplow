---
title: >-
    رباعی شمارهٔ ۳۲
---
# رباعی شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>یا صاحب ننگ و نام می‌باید بود</p></div>
<div class="m2"><p>یا شهرهٔ خاص و عام می‌باید بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>القصه کمال جهد می‌باید کرد</p></div>
<div class="m2"><p>در وادی خود تمام می‌باید بود</p></div></div>