---
title: >-
    رباعی شمارهٔ ۴۸
---
# رباعی شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>در نامه رقم ز خانه‌ای یافته‌ام</p></div>
<div class="m2"><p>وز عنبر تر شمامه‌ای یافته‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شوق دمی هزار بارش خوانم</p></div>
<div class="m2"><p>گویی تو که گنج نامه‌ای یافته‌ام</p></div></div>