---
title: >-
    رباعی شمارهٔ ۲۰
---
# رباعی شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>وحشی که همیشه میل ساغر دارد</p></div>
<div class="m2"><p>جز باده کشی چه کار دیگر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیوسته کدویش ز می ناب پر است</p></div>
<div class="m2"><p>یعنی که مدام باده در سر دارد</p></div></div>