---
title: >-
    رباعی شمارهٔ ۴۳
---
# رباعی شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>ای صیت معالجات تو عالم گیر</p></div>
<div class="m2"><p>و آوازه تو کرده جهان را تسخیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب که جدا مباد تا عالم هست</p></div>
<div class="m2"><p>صحت ز تنت چو نور از بدر منیر</p></div></div>