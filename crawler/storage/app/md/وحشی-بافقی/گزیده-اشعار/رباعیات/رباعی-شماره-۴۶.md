---
title: >-
    رباعی شمارهٔ ۴۶
---
# رباعی شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>ای جان و تنم مطیع و شوق تو مطاع</p></div>
<div class="m2"><p>رفتی و جدا زان رخ خورشید شعاع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیهات که جان وداع تن کرد و نداد</p></div>
<div class="m2"><p>چندان مهلت که تن شتابد به وداع</p></div></div>