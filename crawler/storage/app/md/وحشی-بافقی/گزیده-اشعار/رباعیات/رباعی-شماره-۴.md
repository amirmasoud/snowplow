---
title: >-
    رباعی شمارهٔ ۴
---
# رباعی شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>جان سوخت ز داغ دوری یار مرا</p></div>
<div class="m2"><p>افزود سد آزار بر آزار مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من کشتنیم کز او جدایی جستم</p></div>
<div class="m2"><p>ای هجر به جرم این بکش زار مرا</p></div></div>