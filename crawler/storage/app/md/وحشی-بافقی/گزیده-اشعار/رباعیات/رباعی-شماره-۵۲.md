---
title: >-
    رباعی شمارهٔ ۵۲
---
# رباعی شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>از آبله‌ای تازه گل باغ ارم</p></div>
<div class="m2"><p>حاشا که شود طراوت روی تو کم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی جوهر حسن لاله است از ژاله</p></div>
<div class="m2"><p>نی زیور خوبی گل است از شبنم</p></div></div>