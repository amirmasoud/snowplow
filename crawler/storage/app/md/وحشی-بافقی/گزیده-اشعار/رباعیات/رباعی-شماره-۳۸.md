---
title: >-
    رباعی شمارهٔ ۳۸
---
# رباعی شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>تا شکل هلال گردد از چرخ پدید</p></div>
<div class="m2"><p>کز بهر در شادی عید است کلید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز وشب عمر بی زوالت بادش</p></div>
<div class="m2"><p>مستلزم اجر روزه و شادی عید</p></div></div>