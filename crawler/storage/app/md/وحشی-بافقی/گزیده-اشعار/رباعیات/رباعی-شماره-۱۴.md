---
title: >-
    رباعی شمارهٔ ۱۴
---
# رباعی شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>شاها سر روزگار پامال تو باد</p></div>
<div class="m2"><p>گردون ز کتل کشان اجلال تو باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر صید مرادی که بود در عالم</p></div>
<div class="m2"><p>فتراک پرست رخش اقبال تو باد</p></div></div>