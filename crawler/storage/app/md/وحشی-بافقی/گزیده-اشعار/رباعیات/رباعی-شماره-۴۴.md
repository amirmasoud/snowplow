---
title: >-
    رباعی شمارهٔ ۴۴
---
# رباعی شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>آن شمع که دوش بود تب تا سحرش</p></div>
<div class="m2"><p>صحت پی رفع تب در آمد ز درش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تب از بدنش راه‌گریزی می‌جست</p></div>
<div class="m2"><p>فصاد جهاند از ره نیشترش</p></div></div>