---
title: >-
    رباعی شمارهٔ ۳۴
---
# رباعی شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>تا پای کسی سلسله آرا نشود</p></div>
<div class="m2"><p>او را سر قدر آسمان سا نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز ار نشود صید و نیفتد در قید</p></div>
<div class="m2"><p>او را به سر دست شهان جا نشود</p></div></div>