---
title: >-
    رباعی شمارهٔ ۲۶
---
# رباعی شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>می‌خواست فلک که تلخ کامم بکشد</p></div>
<div class="m2"><p>ناکردهٔ می طرب به جامم، بکشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسپرد به شحنه فراق تو مرا</p></div>
<div class="m2"><p>تا او به عقوبت تمامم بکشد</p></div></div>