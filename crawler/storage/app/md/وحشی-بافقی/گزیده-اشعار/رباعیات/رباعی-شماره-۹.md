---
title: >-
    رباعی شمارهٔ ۹
---
# رباعی شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>شاها سربخت بر در دولت تست</p></div>
<div class="m2"><p>یک خیمه فلک ز اردوی شوکت تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خیمهٔ چرخ را ستونی باید</p></div>
<div class="m2"><p>اندازه ستون خیمهٔ رفعت تست</p></div></div>