---
title: >-
    رباعی شمارهٔ ۱۸
---
# رباعی شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>جرم است سراپای من خاک نهاد</p></div>
<div class="m2"><p>لیکن بودم به عفو او خاطر شاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای وای اگر عفو نباشد ، ای وای</p></div>
<div class="m2"><p>فریاد اگر جرم نبخشد ، فریاد</p></div></div>