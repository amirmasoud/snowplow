---
title: >-
    غزل ۱۴
---
# غزل ۱۴

<div class="b" id="bn1"><div class="m1"><p>چیست قصد خون من آن ترک کافر کیش را</p></div>
<div class="m2"><p>ای مسلمانان نمی‌دانم گناه خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که پرسی موجب این ناله‌های دلخراش</p></div>
<div class="m2"><p>سینه‌ام بشکاف تا بینی درون خویش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به بدنامی کشد کارم در آخر دور نیست</p></div>
<div class="m2"><p>من که نشنیدم در اول پند نیک اندیش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لطف خوبان گرچه دارد ذوق بیش از بیش، لیک</p></div>
<div class="m2"><p>حالتی دیگر بود بیداد بیش از بیش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حد وحشی نیست لاف عشق آن سلطان حسن</p></div>
<div class="m2"><p>حرف باید زد به حد خویشتن درویش را</p></div></div>