---
title: >-
    غزل ۴
---
# غزل ۴

<div class="b" id="bn1"><div class="m1"><p>چند به دل فرو خورم این تف سینه تاب را</p></div>
<div class="m2"><p>در ته دوزخ افکنم جان پر اضطراب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تافته عشق دوزخی ز اهل نصیحت اندرو</p></div>
<div class="m2"><p>بر من و دل گماشته سد ملک عذاب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوق ، به تازیانه گر دست بدین نمط زند</p></div>
<div class="m2"><p>زود سبک عنان کند صبر گران رکاب را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه خدنگ نیمکش می‌خورم از تغافلش</p></div>
<div class="m2"><p>کاش تمام کش کند نیمکش عتاب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیل خیال کیست این کز در چشمخانه‌ها</p></div>
<div class="m2"><p>می‌کشد اینچنین برون خلوتیان خواب را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌جهد آهم از درون پاس جمال دار، هان</p></div>
<div class="m2"><p>صرصر ما نگون کند مشعل آفتاب را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وحشی و اشک حسرت و تف هوای بادیه</p></div>
<div class="m2"><p>آب ز چشم تر بود ره سپر سراب را</p></div></div>