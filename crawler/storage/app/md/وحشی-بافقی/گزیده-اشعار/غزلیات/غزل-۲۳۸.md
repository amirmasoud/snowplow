---
title: >-
    غزل ۲۳۸
---
# غزل ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>عشق می‌فرمایدم مستغنی از دیدار باش</p></div>
<div class="m2"><p>چند گه با یار بودی، چند گه بی یار باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوق می‌گوید که آسان نیست بی او زیستن</p></div>
<div class="m2"><p>صبر می‌گوید که باکی نیست گو دشوار باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وصل خواری بر دهد ای طایر بستان پرست</p></div>
<div class="m2"><p>گلستان خواهی قفس، مستغنی از گلزار باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وصل اگر اینست و ذوقش این که من دریافتم</p></div>
<div class="m2"><p>گر ز حرمانت بسوزد هجر منت دار باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبر خواهم کرد وحشی از غم نادیدنش</p></div>
<div class="m2"><p>من چو خواهم مرد گو از حسرت دیدار باش</p></div></div>