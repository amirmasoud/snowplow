---
title: >-
    غزل ۳۸۶
---
# غزل ۳۸۶

<div class="b" id="bn1"><div class="m1"><p>آتشی در جان ما افروختی</p></div>
<div class="m2"><p>رفتی و ما را ز حسرت سوختی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌وداع دوستان کردی سفر</p></div>
<div class="m2"><p>از که این راه و روش آموختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرنه از یاران بدی دیدی چرا</p></div>
<div class="m2"><p>دیده از دیدار یاران دوختی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌رخ او طرح صبر انداختی</p></div>
<div class="m2"><p>ای دل این صبر از کجا آموختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی از جانت علم زد آتشی</p></div>
<div class="m2"><p>خانمان عالمی را سوختی</p></div></div>