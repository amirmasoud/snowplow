---
title: >-
    غزل ۲۴۰
---
# غزل ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>ترک ما کردی برو همصحبت اغیار باش</p></div>
<div class="m2"><p>یار ما چون نیستی با هر که خواهی یار باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مست حسنی با رقیبان میل می خوردن مکن</p></div>
<div class="m2"><p>بد حریفانند آنها گفتمت هشیار باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه ما را هیچ برخورداری از وصلش نبود</p></div>
<div class="m2"><p>از نهال وصل او گو غیر برخوردار باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه می‌دانم که دشوار است صبر از روی دوست</p></div>
<div class="m2"><p>چند روزی صبر خواهم کرد گو دشوار باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبر خواهم کرد وحشی در غم نادیدنش</p></div>
<div class="m2"><p>من که خواهم مرد گو از حسرت دیدار باش</p></div></div>