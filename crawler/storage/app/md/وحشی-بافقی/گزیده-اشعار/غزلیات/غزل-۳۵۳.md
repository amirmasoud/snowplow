---
title: >-
    غزل ۳۵۳
---
# غزل ۳۵۳

<div class="b" id="bn1"><div class="m1"><p>میان مردمانم خوار کردی عزت من کو</p></div>
<div class="m2"><p>سگ کوی تو بودم روزگاری حرمت من کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سد جان می‌خرم گردی که خیزد از سر راهت</p></div>
<div class="m2"><p>ندارم قدر خاک راه پیشت ، قیمت من کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به داغم هر زمان دردی فزاید محرم بزمت</p></div>
<div class="m2"><p>کسی کو با تو گوید درد و داغ حسرت من کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو خواهد بی‌گناهی را کشد احوال من پرسد</p></div>
<div class="m2"><p>که آن بی‌خانمان پیدا نشد در صحبت من کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگو در بزم او دایم به عیش و عشرتی وحشی</p></div>
<div class="m2"><p>کدامین عیش و عشرت ، مردم از غم ، عشرت من کو</p></div></div>