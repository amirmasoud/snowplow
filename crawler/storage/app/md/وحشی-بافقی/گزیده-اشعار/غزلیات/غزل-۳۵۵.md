---
title: >-
    غزل ۳۵۵
---
# غزل ۳۵۵

<div class="b" id="bn1"><div class="m1"><p>شد بی‌حساب کشور جانها خراب از او</p></div>
<div class="m2"><p>ترک است و تندخو چه عجب بی حساب از او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پروانه یک زمان دگر زنده بیش نیست</p></div>
<div class="m2"><p>ای شمع سرکشی مکن و رخ متاب از او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر در نقاب خواب کش ای بلهوس که تو</p></div>
<div class="m2"><p>بی‌یار زنده‌ای و نداری حجاب از او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا پرده برگرفت ز ماه تمام خویش</p></div>
<div class="m2"><p>رو زردی تمام کشید آفتاب از او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی که نیم کشته به خون می‌تپد ز تو</p></div>
<div class="m2"><p>با جان مگر برون رود این اضطراب از او</p></div></div>