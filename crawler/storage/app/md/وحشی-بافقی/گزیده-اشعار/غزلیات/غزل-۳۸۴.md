---
title: >-
    غزل ۳۸۴
---
# غزل ۳۸۴

<div class="b" id="bn1"><div class="m1"><p>چه خوش بودی دلا گر روی او هرگز نمی‌دیدی</p></div>
<div class="m2"><p>جفاهای چنین از خوی او هرگز نمی‌دیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن‌هایی که در حق تو سر زد از رقیب من</p></div>
<div class="m2"><p>گرت می‌بود دردی سوی او هرگز نمی‌دیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدین بد حالی افکندی مرا ای چشم تر آخر</p></div>
<div class="m2"><p>چه بودی گر رخ نیکوی او هرگز نمی‌دیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز اشک ناامیدی کاش ای دل کور می‌گشتی</p></div>
<div class="m2"><p>که زینسان غیر را پهلوی او هرگز نمی‌دیدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا صد کوه محنت کاشکی پیش آمدی وحشی</p></div>
<div class="m2"><p>که می‌مردی و راه کوی او هرگز نمی‌دیدی</p></div></div>