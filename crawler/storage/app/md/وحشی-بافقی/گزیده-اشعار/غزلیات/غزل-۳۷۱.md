---
title: >-
    غزل ۳۷۱
---
# غزل ۳۷۱

<div class="b" id="bn1"><div class="m1"><p>قلب سپه ماست به یک حمله شکسته</p></div>
<div class="m2"><p>با غمزه بگو تا نزند تیغ دو دسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیکان ز جگر جسته و زخمی شده جان هم</p></div>
<div class="m2"><p>وین طرفه که تیرت ز کمانخانه نجسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امید من از طایر وصل تو بریده‌ست</p></div>
<div class="m2"><p>نتوان پر او بست به این تار گسسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دور من و دست و دعایی اگرم تو</p></div>
<div class="m2"><p>بر خوان ثنائی در دریوزه نبسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگذاشت کسادی که غباری بنشانیم</p></div>
<div class="m2"><p>زین جنس محبت که بر او گرد نشسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز نرهد آنکه تواش بند نهادی</p></div>
<div class="m2"><p>میرد به قفس مرغ پر و بال شکسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وحشی نتوان خرمن امید نهادن</p></div>
<div class="m2"><p>زین تخم تمنا که تو کشتی و نرسته</p></div></div>