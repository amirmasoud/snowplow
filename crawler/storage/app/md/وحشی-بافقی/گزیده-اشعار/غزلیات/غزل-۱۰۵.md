---
title: >-
    غزل ۱۰۵
---
# غزل ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>ناز برگیرد کمان در وقت ترکش بستنت</p></div>
<div class="m2"><p>فتنه پاکوبان شود هنگام ابرش جستنت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لاله آتشناک رویاند ز آب و خاک دشت</p></div>
<div class="m2"><p>ز آب خوی رخساره از گرد سواری شستنت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش دست و قبضه‌ات میرم که خوش مردم کش است</p></div>
<div class="m2"><p>در کمان ناز تیر دلبری پیوستنت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چه آتشها کند بر هر سر کویی بلند</p></div>
<div class="m2"><p>شوخی طبع تو و یک جا دمی نشستنت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشیم من جای من میدانگه نخجیر تست</p></div>
<div class="m2"><p>نیستم صیدی که باید کشت و باید خستنت</p></div></div>