---
title: >-
    غزل ۱۹۹
---
# غزل ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>ز کار بستهٔ ما عقدهٔ حرمان که بگشاید</p></div>
<div class="m2"><p>که سازد این کلید و قفل این زندان که بگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گلخن گر روم از رشک گلخن تاب در بندند</p></div>
<div class="m2"><p>به روی ناکسی چون من در بستان که بگشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین کز دیدن هر ناپسندم خون بجوش آمد</p></div>
<div class="m2"><p>اگر نه سیل خون زور آورد مژگان که بگشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جگر تا لب گره از غصه و سد عقده در خاطر</p></div>
<div class="m2"><p>کجا ظاهر کنم وین عقدهٔ پنهان که بگشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طلسم دوستی پرخوف و گنج وصل پردشمن</p></div>
<div class="m2"><p>عجب گنجی‌ست اما تا طلسم آن که بگشاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگو وحشی که بگشاید در امید ما آخر</p></div>
<div class="m2"><p>خدا بگشاید این در، آخر ای نادان که بگشاید</p></div></div>