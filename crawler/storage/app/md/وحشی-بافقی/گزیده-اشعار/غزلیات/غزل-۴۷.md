---
title: >-
    غزل ۴۷
---
# غزل ۴۷

<div class="b" id="bn1"><div class="m1"><p>در دل همان محبت پیشینه باقی است</p></div>
<div class="m2"><p>آن دوستی که بود در این سینه باقی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز آ و حسن جلوه ده و عرض ناز کن</p></div>
<div class="m2"><p>کان دل که بود صاف چو آیینه باقی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ما فروتنی‌ست بکش تیغ انتقام</p></div>
<div class="m2"><p>بر خاطر شریفت اگر کینه باقی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقدینه وفاست همان بر عیار خویش</p></div>
<div class="m2"><p>قفلی که بود بر در گنجینه باقی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی اگر ز کسوت رندی دلت گرفت</p></div>
<div class="m2"><p>زهد و صلاح و خرقهٔ پشمینه باقی است</p></div></div>