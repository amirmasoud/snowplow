---
title: >-
    غزل ۱۰۴
---
# غزل ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>گرم آمد و بر آتش شوقم نشاند و رفت</p></div>
<div class="m2"><p>آتش به جای آب ز چشمم فشاند و رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد چو باد و مضطربم کرد همچو برق</p></div>
<div class="m2"><p>وز آتشم زبانه به گردون رساند و رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برخاستم که دست دعایی برآورم</p></div>
<div class="m2"><p>دشنام داد و راه دگر کرد و راند و رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پی دویدمش که عنان گیریی کنم</p></div>
<div class="m2"><p>افراشت تازیانه و مرکب جهاند و رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی نشد نصیبم ازو تازیانه‌ای</p></div>
<div class="m2"><p>چشمم به حسرت از پی او بازماند و رفت</p></div></div>