---
title: >-
    غزل ۴۸
---
# غزل ۴۸

<div class="b" id="bn1"><div class="m1"><p>ترک من تیغ به کف ، بر زده دامن برخاست</p></div>
<div class="m2"><p>جان فدایش که به خون ریختن من برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌کشیدند ملایک همه چون سرمه به چشم</p></div>
<div class="m2"><p>هر غباری که ترا از سم توسن برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرمن مشک چو بر دور مهت ظاهر شد</p></div>
<div class="m2"><p>دود از جان من سوخته خرمن برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وحشی سوخته را بستر سنجاب نمود</p></div>
<div class="m2"><p>هر سحرگه که ز خاکستر گلشن برخاست</p></div></div>