---
title: >-
    غزل ۱۴۸
---
# غزل ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>چندین عنایت از پی چندین جفا چه بود</p></div>
<div class="m2"><p>تغییر طور خویش چرا مدعا چه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما کشتهٔ جفا نه برای وفا شدیم</p></div>
<div class="m2"><p>سد جان فدای خنجر تو خونبها چه بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی شکوه و شکایت ما ترک جور چیست</p></div>
<div class="m2"><p>دیدی چه ناصواب ، بفرما خطا چه بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طبع تو هیچ خاطر ما در میان ندید</p></div>
<div class="m2"><p>منع جفا و جور ز بهر خدا چه بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چینندت این هوس ز کجا ای نهال لطف</p></div>
<div class="m2"><p>بر ما ثمر فشانی شاخ وفا چه بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با این غرور حسن که سد نخل سربلند</p></div>
<div class="m2"><p>از پا فکند ، نرمی او با گیا چه بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وحشی نیاز و عجز تواش داشت بر وفا</p></div>
<div class="m2"><p>خود کرده‌ای چنین به خودش جرم ما چه بود</p></div></div>