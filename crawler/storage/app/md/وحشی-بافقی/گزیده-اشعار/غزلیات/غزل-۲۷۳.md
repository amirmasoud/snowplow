---
title: >-
    غزل ۲۷۳
---
# غزل ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>به آنکه بر سرلطفی مکش ز منت خویشم</p></div>
<div class="m2"><p>سگ وفای خود و بندهٔ محبت خویشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سزای خدمت شایسته است لطف چه منت</p></div>
<div class="m2"><p>ز خدمتم خجل و حقگزار خدمت خویشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عنایت تو به پاداش صبردارم و طاقت</p></div>
<div class="m2"><p>به شکر صبر خود و ذکر خیرطاقت خویشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پلنگ خوی غزالی که می‌رمد ز فرشته</p></div>
<div class="m2"><p>چگونه ساختمش رام صید قدرت خویشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کام شیر درون رفتن و به کام رسیدن</p></div>
<div class="m2"><p>کراست زهره و یارا غلام جرأت خویشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه خوش گزیده‌امت از بساط حسن فروشان</p></div>
<div class="m2"><p>نه عاشق تو که من عاشق بصیرت خویشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا رسد که چو وحشی چنین دلیر درآیم</p></div>
<div class="m2"><p>که خوانده لطف تو در سایهٔ حمایت خویشم</p></div></div>