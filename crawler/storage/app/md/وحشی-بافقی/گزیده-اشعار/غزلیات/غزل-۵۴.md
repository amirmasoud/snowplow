---
title: >-
    غزل ۵۴
---
# غزل ۵۴

<div class="b" id="bn1"><div class="m1"><p>ابروی تو جنبید و خدنگی ز کمان جست</p></div>
<div class="m2"><p>بر سینه چنان خورد که از جوشن جان جست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چشم چه بود آه که ناگاه گشودی</p></div>
<div class="m2"><p>این فتنه دگر چیست که از خواب گران جست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بودم و دل بود و کناری و فراغی</p></div>
<div class="m2"><p>این عشق کجا بود که ناگه به میان جست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جرگهٔ او گردن جان بست به فتراک</p></div>
<div class="m2"><p>هر صید که از قید کمند دگران جست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردن بنه ای بستهٔ زنجیر محبت</p></div>
<div class="m2"><p>کز زحمت این بند به کوشش نتوان جست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم که مگر پاس تف سینه توان داشت</p></div>
<div class="m2"><p>حرفی به زبان آمد و آتش ز دهان جست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وحشی می منصور به جام است مخور هان</p></div>
<div class="m2"><p>ناگاه شدی بیخود و حرفی ز زبان جست</p></div></div>