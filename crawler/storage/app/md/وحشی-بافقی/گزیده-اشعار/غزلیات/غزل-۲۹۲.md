---
title: >-
    غزل ۲۹۲
---
# غزل ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>دور از چمن وصل یکی مرغ اسیرم</p></div>
<div class="m2"><p>ترسم که شوی غافل و در دام بمیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم که شوم ازنظر لطف تو غایب</p></div>
<div class="m2"><p>هر چند که پر دردم و بسیار حقیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر آب فراموشی ازین بیشتر آید</p></div>
<div class="m2"><p>ترسم که فرو شوید از آن لوح ضمیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان کرد وداع تن و برخاست که وحشی</p></div>
<div class="m2"><p>بنشین تو که من در قدم موکب میرم</p></div></div>