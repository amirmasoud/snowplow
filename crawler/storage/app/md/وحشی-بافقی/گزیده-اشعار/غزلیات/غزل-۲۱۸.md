---
title: >-
    غزل ۲۱۸
---
# غزل ۲۱۸

<div class="b" id="bn1"><div class="m1"><p>چرا ستمگر من با کسی جفا نکند</p></div>
<div class="m2"><p>جفای او همه کس می‌کشد چرا نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فغان ز سنگدل من که خون سد مظلوم</p></div>
<div class="m2"><p>به ظلم ریزد و اندیشه از خدا نکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه غصه‌ها که نخوردم ز آشنایی تو</p></div>
<div class="m2"><p>خدا ترا به کسی یارب آشنا نکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کدام سنگدل از درد من خبر دارد</p></div>
<div class="m2"><p>که با وجود دل سخت گریه‌ها نکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشیده جام و سر بی‌گنه کشی دارد</p></div>
<div class="m2"><p>عجب که بر نکشد تیغ و قصد ما نکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جای خویش نیامد مرا چو وحشی دل</p></div>
<div class="m2"><p>اگر ز تیر تو پیکان به سینه جا نکند</p></div></div>