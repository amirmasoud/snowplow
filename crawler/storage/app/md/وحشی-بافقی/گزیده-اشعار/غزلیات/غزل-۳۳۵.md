---
title: >-
    غزل ۳۳۵
---
# غزل ۳۳۵

<div class="b" id="bn1"><div class="m1"><p>پیش تو بسی از همه کس خوارترم من</p></div>
<div class="m2"><p>زان روی که از جمله گرفتارترم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی که نماند دگری بر سر کویت</p></div>
<div class="m2"><p>دانی که ز اغیار وفادارترم من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر بی کسی من نگر و چارهٔ من کن</p></div>
<div class="m2"><p>زان کز همه کس بی کس و بی‌یارترم من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیداد کنی پیشه و چون از تو کنم داد</p></div>
<div class="m2"><p>زارم بکشی کز که ستمکارترم من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی به طبیب من بیچاره که گوید</p></div>
<div class="m2"><p>کامروز ز دیروز بسی زارترم من</p></div></div>