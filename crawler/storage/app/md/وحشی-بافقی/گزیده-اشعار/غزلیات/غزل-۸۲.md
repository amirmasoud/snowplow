---
title: >-
    غزل ۸۲
---
# غزل ۸۲

<div class="b" id="bn1"><div class="m1"><p>کس به بزم دلبران از دور گردان پیش نیست</p></div>
<div class="m2"><p>قرب نزدیکان مجلس حرف و صوتی بیش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صلات عاشقان دوری و تنهاییست رکن</p></div>
<div class="m2"><p>گو قضا کن طاعت خود هر که اینش کیش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما نکو دانیم طور حسن دور افتاده دوست</p></div>
<div class="m2"><p>قرب ارزانی به مشتاقی که دور اندیش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر خوانند نزدیکان ولیکن لطف شاه</p></div>
<div class="m2"><p>منتظر جز بر ره دریوزهٔ درویش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>انگبین زهر هلاک تست با دوری بساز</p></div>
<div class="m2"><p>ای مگس مرگ تو در نوش است اندر نیش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلبران وحشی حکیمانند ضایع کی کنند</p></div>
<div class="m2"><p>مرهم خود را بر آن دل کز محبت ریش نیست</p></div></div>