---
title: >-
    غزل ۱۹۳
---
# غزل ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>هرگز به غرض عشق من آلوده نگردد</p></div>
<div class="m2"><p>چشمم به کف پای کسی سوده نگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آلوده نیم چون دگران این هنرم هست</p></div>
<div class="m2"><p>کز صحبت من هیچکس آلوده نگردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پروانه‌ام و عادت من سوختن خویش</p></div>
<div class="m2"><p>تا پاک نسوزم دلم آسوده نگردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با بلهوس از پاکی دامان تو گفتم</p></div>
<div class="m2"><p>تا باز به دنبال تو بیهوده نگردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی ز غمش جان تو فرسود عجب نیست</p></div>
<div class="m2"><p>جانست نه سنگست که فرسوده نگردد</p></div></div>