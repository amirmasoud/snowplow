---
title: >-
    غزل ۳۸۰
---
# غزل ۳۸۰

<div class="b" id="bn1"><div class="m1"><p>جایی روم که جنس وفا را خرد کسی</p></div>
<div class="m2"><p>نام متاع من به زبان آورد کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاری که دستگیری یاری کند کجاست</p></div>
<div class="m2"><p>گر سینه‌ای خراشد و جیبی درد کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاریست هر چه هست و ز یاری غرض وفاست</p></div>
<div class="m2"><p>یاری که بیوفاست کجا می‌برد کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهقان چه خوب گفت چو می‌کند خاربن</p></div>
<div class="m2"><p>شاخی کش این بر است چرا پرورد کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی برای صحبت یاران بی‌وفا</p></div>
<div class="m2"><p>خاطر چرا حزین کند و غم خورد کسی</p></div></div>