---
title: >-
    غزل ۲۹۱
---
# غزل ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>انجام حسن او شد پایان عشق من هم</p></div>
<div class="m2"><p>رفت آن نوای بلبل بی برگ شد چمن هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرد آنچنان جمالی در کنج خانه ضایع</p></div>
<div class="m2"><p>بر عشق من ستم کرد بر حسن خویشتن هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدمستی غرورش هنگامه گرم نگذاشت</p></div>
<div class="m2"><p>افسرده کرد صحبت بر هم زد انجمن هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو مست جام خوبی غافل مشو که دارد</p></div>
<div class="m2"><p>این دست شیشه پر کن سنگ قدح شکن هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان کندن عبث را بر خود کنیم شیرین</p></div>
<div class="m2"><p>یکچند کوه می‌کند بیهوده کوهکن هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وحشی حدیث تلخست بار درخت حرمان</p></div>
<div class="m2"><p>گویند تلخ کامان زین تلختر سخن هم</p></div></div>