---
title: >-
    غزل ۳۰۹
---
# غزل ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>دیریست که رندانه شرابی نکشیدیم</p></div>
<div class="m2"><p>در گوشهٔ باغی می نابی نکشیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سبزه قدم بر لب جویی ننهادیم</p></div>
<div class="m2"><p>چون لاله قدح بر لب آبی نکشیدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر چهره کشیدیم نقاب کفن افسوس</p></div>
<div class="m2"><p>کز چهرهٔ مقصود نقابی نکشیدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسیار عذابی که کشیدیم ولیکن</p></div>
<div class="m2"><p>دشوارتر از هجر عذابی نکشیدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی به رخ ما در فیضی نگشودند</p></div>
<div class="m2"><p>تا پای طلب از همه بابی نکشیدیم</p></div></div>