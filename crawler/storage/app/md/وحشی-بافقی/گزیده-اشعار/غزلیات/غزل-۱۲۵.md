---
title: >-
    غزل ۱۲۵
---
# غزل ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>کی دیدمش که قصد دل زار من نکرد</p></div>
<div class="m2"><p>ننشست با رقیبی و آزار من نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک شمه کار در فن ناز و کرشمه نیست</p></div>
<div class="m2"><p>کز یک نگاه چشم تو در کار من نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم مرنج و گوش کن از من حکایتی</p></div>
<div class="m2"><p>رنجش نمود و گوش به گفتار من نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خندان نشست و شمع شبستان غیر شد</p></div>
<div class="m2"><p>رحمی به گریه‌های شب تار من نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی نماند هیچ سیاست که هجر یار</p></div>
<div class="m2"><p>با جان خسته و دل افکار من نکرد</p></div></div>