---
title: >-
    غزل ۳۲۶
---
# غزل ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>من اگر این بار رفتم ، رفتم آزارم مکن</p></div>
<div class="m2"><p>این تغافلهای بیش از پیش در کارم مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای برگشتن نخواهم داشت خواهم رفت و ماند</p></div>
<div class="m2"><p>در تماشا گاه دیگر نقش دیوارم مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنده می‌خواهی ز خدمتکار خود غافل مباش</p></div>
<div class="m2"><p>می‌شود ناگه کسی دیگر خریدارم مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من که مستم مجلست گر هست و میر مجلسی</p></div>
<div class="m2"><p>بزم خود افسرده خواهی کرد هشیارم مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عزت سگ هست در کوی تو وحشی خود چه کرد</p></div>
<div class="m2"><p>گر چه عاشق خوار می‌باید، چنین خوارم مکن</p></div></div>