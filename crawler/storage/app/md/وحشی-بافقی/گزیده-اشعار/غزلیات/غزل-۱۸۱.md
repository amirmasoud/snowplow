---
title: >-
    غزل ۱۸۱
---
# غزل ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>کسی کز رشک من محروم از آن پیمان شکن گرید</p></div>
<div class="m2"><p>اگر در بزم او بیند مرا، بر حال من گرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بزم عیش بی دردان به جانم ، کو غم آبادی</p></div>
<div class="m2"><p>که سوزد یک طرف مجنون و یک سو کوهکن گرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه می‌پرسی حدیث درد پروردی که احوالش</p></div>
<div class="m2"><p>کسی هرگز نفهمد بسکه هنگام سخن گرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشینم من هم از اندوه و، دور از کوی او گریم</p></div>
<div class="m2"><p>غریب و دردمندی هر کجا دور از وطن گرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برو ای پند گو بگذار وحشی را که این مسکین</p></div>
<div class="m2"><p>دمی بنشیند و بر روزگار خویشتن گرید</p></div></div>