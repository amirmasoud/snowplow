---
title: >-
    غزل ۵۱
---
# غزل ۵۱

<div class="b" id="bn1"><div class="m1"><p>بهر دلم که درد کش و داغدار تست</p></div>
<div class="m2"><p>داروی صبر باید و آن در دیار تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک بار نام من به غلط بر زبان نراند</p></div>
<div class="m2"><p>ما را شکایت از قلم مشکبار تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر پاره کاغذی دو سه مدی توان کشید</p></div>
<div class="m2"><p>دشنام و هر چه هست غرض یادگار تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو بی‌وفا چه باز فراموش پیشه‌ای</p></div>
<div class="m2"><p>بیچاره آن اسیر که امیدوار تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هان این پیام وصل که اینک روانه است</p></div>
<div class="m2"><p>جانم به لب رسیده که در انتظار تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجنون هزار نامهٔ ز لیلی زیاده داشت</p></div>
<div class="m2"><p>وحشی که همچو یار فراموشکار تست</p></div></div>