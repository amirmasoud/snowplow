---
title: >-
    غزل ۱۴۳
---
# غزل ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>بود آن وقتی که دشنام تو خاطر خواه بود</p></div>
<div class="m2"><p>بنده بودیم و زبان ماجرا کوتاه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حق یاریهای سابق گر نبستی راه نطق</p></div>
<div class="m2"><p>درجواب این که گفتی نکته‌ای در راه بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش ازینم جان فزودی لذت دشنام او</p></div>
<div class="m2"><p>اله اله از چه امروز اینچنین جانکاه بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو مده فرمان که دیگر نیست دل فرمان پذیر</p></div>
<div class="m2"><p>حکم او می‌رفت چندانی که اینجا شاه بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سالها هم بگذرد وحشی که سویش نگذرم</p></div>
<div class="m2"><p>تا نپنداری که خشم ما همین یک ماه بود</p></div></div>