---
title: >-
    غزل ۲۶۵
---
# غزل ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>تا چند به غمخانهٔ حسرت بنشینم</p></div>
<div class="m2"><p>وقتست که با یار به عشرت بنشینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی طاقتیم در ره او می‌رود از حد</p></div>
<div class="m2"><p>کو صبر که در گوشهٔ طاقت بنشینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چند روم از پی او بند کنیدم</p></div>
<div class="m2"><p>باشد که زمانی به فراغت بنشینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغ تو مرا شمع صفت سوخت کجایی</p></div>
<div class="m2"><p>مگذار که با اشک ندامت بنشینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پامال شدم چند چو وحشی به ره غم</p></div>
<div class="m2"><p>از دست تو بر خاک مذلت بنشینم</p></div></div>