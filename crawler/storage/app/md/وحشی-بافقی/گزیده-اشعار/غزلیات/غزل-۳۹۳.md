---
title: >-
    غزل ۳۹۳
---
# غزل ۳۹۳

<div class="b" id="bn1"><div class="m1"><p>ای جوان ترک وش میر کدامین لشکری</p></div>
<div class="m2"><p>ای خوشا آن کشوری کانجا تو صاحب کشوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای سوار فرد از لشکر جدا افتاده‌ای</p></div>
<div class="m2"><p>یا از آن ترکان یغما پیشهٔ غارتگری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتشت در آب پنهانست و زهرت در شکر</p></div>
<div class="m2"><p>آشکارا گر چه با من همچو شیر و شکری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواه شکر ریز و خواهی زهر در جامم که تو</p></div>
<div class="m2"><p>گر چه زهرم می‌چشانی از شکر شیرین تری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی آن صید افکنت گر افکند در خون منال</p></div>
<div class="m2"><p>نیستی لایق به فتراکش که صید لاغری</p></div></div>