---
title: >-
    غزل ۱۵۵
---
# غزل ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>ترسم در این دلهای شب از سینه آهی سرزند</p></div>
<div class="m2"><p>برقی ز دل بیرون جهد آتش به جایی درزند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عهده چون آید برون گر بر زمین آمد سری</p></div>
<div class="m2"><p>آن نیمه‌های شب که او با مدعی ساغر زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوس نبرد ما مزن اندیشه کن کز خیل ما</p></div>
<div class="m2"><p>گر یک دعا تازد برون بر یک جهان لشکر زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتشفشانست این هوا ، پیرامن ما نگذری</p></div>
<div class="m2"><p>خصمی به بال خود کند مرغی که اینجا پرزند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می بی صفا، نی بی نوا ، وقتست اگر در بزم ما</p></div>
<div class="m2"><p>ساقی می دیگر دهد مطرب رهی دیگر زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را درین زندان غم من بعد نتوان داشتن</p></div>
<div class="m2"><p>بندی مگر بر پانهد، قفلی مگر بر در زند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وحشی ز بس آزردگی زهر از زبانم می‌چکد</p></div>
<div class="m2"><p>خواهم دلیری کاین زمان خود را بر این خنجر زند</p></div></div>