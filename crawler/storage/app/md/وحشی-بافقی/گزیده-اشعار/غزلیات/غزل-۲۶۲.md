---
title: >-
    غزل ۲۶۲
---
# غزل ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>تو زمن پرس قدر روز وصال</p></div>
<div class="m2"><p>تشنه داند که چیست آب زلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذوق آن جستن از قفس ناگاه</p></div>
<div class="m2"><p>من شناسم نه مرغ فارغ بال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌توان مرد بهر آن هجران</p></div>
<div class="m2"><p>کش وصال تو باشد از دنبال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این منم، این منم به خدمت تو</p></div>
<div class="m2"><p>ای خوشم حال و ای خوشم احوال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این تویی، این تویی برابر من</p></div>
<div class="m2"><p>ای خوشم بخت و ای خوشم اقبال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وحشی اسباب خوشدلی همه هست</p></div>
<div class="m2"><p>ای دریغا دو جام مالامال</p></div></div>