---
title: >-
    غزل ۱۳۶
---
# غزل ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>به راز عشق زبان در میان نمی‌باشد</p></div>
<div class="m2"><p>زبان ببند که آنجا بیان نمی‌باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میان عاشق و معشوق یک کرشمه بس است</p></div>
<div class="m2"><p>بیان حال به کام و زبان نمی‌باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل رمیدهٔ من زخم دار صید گهیست</p></div>
<div class="m2"><p>که زخم صید به تیر و کمان نمی‌باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن روایی بازار کم عیارانست</p></div>
<div class="m2"><p>که در میان محک امتحان نمی‌باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر به من نشوی مهربان درین غرضیست</p></div>
<div class="m2"><p>کسی به خلق تو نامهربان نمی‌باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به عالمی که منم منتهای غصه مپرس</p></div>
<div class="m2"><p>که قطع مدت و طی زمان نمی‌باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبان به کام مکش وحشی از فسانهٔ عشق</p></div>
<div class="m2"><p>بگو که خوشتر ازین داستان نمی‌باشد</p></div></div>