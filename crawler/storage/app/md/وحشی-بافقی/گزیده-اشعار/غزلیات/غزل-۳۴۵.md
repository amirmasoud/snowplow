---
title: >-
    غزل ۳۴۵
---
# غزل ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>ز کویت رخت بربستم نگاهی زاد راهم کن</p></div>
<div class="m2"><p>به تقصیر عنایت یک تبسم عذر خواهم کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ره آوارگی در پیش و از پی دیدهٔ حسرت</p></div>
<div class="m2"><p>وداعی نام نه این را و چشمی بر نگاهم کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز کوی او که کار پاسبان کعبه می‌کردم</p></div>
<div class="m2"><p>خدایا بی ضرورت گر روم سنگ سیاهم کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخوان ای عشق افسونی و آن افسون بدم بر من</p></div>
<div class="m2"><p>مرا بال و پری ده مرغ آن پرواز گاهم کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کنعانم مبر ای بخت من یوسف نمی‌خواهم</p></div>
<div class="m2"><p>ببرآنجا که کوی اوست در زندان و چاهم کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز سد فرسنگ از پشت حریفان جسته پیکانم</p></div>
<div class="m2"><p>مرو نزدیک او وحشی حذر از تیر آهم کن</p></div></div>