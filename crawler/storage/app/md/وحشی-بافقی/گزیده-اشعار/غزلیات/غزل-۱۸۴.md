---
title: >-
    غزل ۱۸۴
---
# غزل ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>غلام عشق حاشا کز جفای یار بگریزد</p></div>
<div class="m2"><p>نه عاشق بلهوس باشد که از آزار بگریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببر، گر بلبلی درد سر بیهوده از گلشن</p></div>
<div class="m2"><p>که گوید عاشق روی گلم و ز خار بگریزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نباشد بی وفا گل بلکه مرغی بی وفا باشد</p></div>
<div class="m2"><p>که چون گل را نماند خوبی رخسار بگریزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس است این طعنه از پروانه تا جاوید بلبل را</p></div>
<div class="m2"><p>که رنگ و بوی گل چون رفت از گلزار بگریزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا از نسبت خود عشق را تهمت نهد وحشی</p></div>
<div class="m2"><p>کسی کز جور یار و طعنهٔ اغیار بگریزد</p></div></div>