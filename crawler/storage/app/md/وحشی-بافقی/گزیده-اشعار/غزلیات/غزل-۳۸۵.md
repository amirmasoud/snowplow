---
title: >-
    غزل ۳۸۵
---
# غزل ۳۸۵

<div class="b" id="bn1"><div class="m1"><p>چه دیدی ای که هرگز بد نبینی</p></div>
<div class="m2"><p>که سوی مبتلای خود نبینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عفا ک اله مرا کشتی و رفتی</p></div>
<div class="m2"><p>نکو رفتی الاهی بد نبینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجو پایان دریای محبت</p></div>
<div class="m2"><p>که گردی غرق و آنرا حد نبینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز مقصودم بر آوردی رقیبا</p></div>
<div class="m2"><p>الاهی ره سوی مقصد نبینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه طور بد ز من دیدی که سویم</p></div>
<div class="m2"><p>به آن طوری که می‌باید نبینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منم وحشی همین مردود بزمش</p></div>
<div class="m2"><p>به پیشش دیگران را بد نبینی</p></div></div>