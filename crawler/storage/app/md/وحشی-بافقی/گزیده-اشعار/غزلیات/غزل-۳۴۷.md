---
title: >-
    غزل ۳۴۷
---
# غزل ۳۴۷

<div class="b" id="bn1"><div class="m1"><p>تو پاک دامن نوگلی من بلبل نالان تو</p></div>
<div class="m2"><p>پاک از همه آلایشی عشق من و دامان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زینسان متاز ای سنگدل ترسم بلغزد توسنت</p></div>
<div class="m2"><p>کز خون ناحق کشتگان گل شد سر میدان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جا بجنبد لشکری کز فتنه عالم پرشود</p></div>
<div class="m2"><p>گر غمزه را فرمان دهد جنبیدن مژگان تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو خوش بیا جولان کنان گو جان ما بر باد رو</p></div>
<div class="m2"><p>ای خاک جان عالمی در عرصه جولان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سهلست قتل عالمی بنشین تو و نظاره کن</p></div>
<div class="m2"><p>کز عهد می‌آید برون یک دیدن پنهان تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بردل اگر خنجر خورد بر دیده گر نشتر خلد</p></div>
<div class="m2"><p>آگه نگردم بسکه شد چشم و دلم حیران تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وحشی چه پرهیزی برو خود را بزن بر تیغ او</p></div>
<div class="m2"><p>آخر تو را چون می‌کشد این درد بی درمان تو</p></div></div>