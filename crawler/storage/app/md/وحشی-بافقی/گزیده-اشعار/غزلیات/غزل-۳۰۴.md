---
title: >-
    غزل ۳۰۴
---
# غزل ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>مدتی شد کز گلستانی جدا افتاده‌ام</p></div>
<div class="m2"><p>عندلیبم سخت بی برگ و نوا افتاده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوبهاری می‌دماند از خاک من گل وان گذشت</p></div>
<div class="m2"><p>گشته‌ام پژمرده و ز نشو و نما افتاده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هوای گلشنی سد ره چو مرغ بسته‌بال</p></div>
<div class="m2"><p>کرده‌ام آهنگ پرواز و بجا افتاده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نمی‌پویم ره دیدار عذرم ظاهر است</p></div>
<div class="m2"><p>بسکه در زنجیر غم ماندم ز پا افتاده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه گمان رستگی دارم نه امید خلاص</p></div>
<div class="m2"><p>سخت در تشویش و محکم در بلا افتاده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مایهٔ هستی تمامی سوختم بر یاد وصل</p></div>
<div class="m2"><p>مفلسم وحشی به فکر کیمیا افتاده‌ام</p></div></div>