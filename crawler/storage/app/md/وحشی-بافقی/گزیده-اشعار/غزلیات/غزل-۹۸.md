---
title: >-
    غزل ۹۸
---
# غزل ۹۸

<div class="b" id="bn1"><div class="m1"><p>اسیر جلوهٔ هر حسن عشقبازی هست</p></div>
<div class="m2"><p>میان هر دو حقیقت نیاز و نازی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هر دری که نهد حسن پای ناز برون</p></div>
<div class="m2"><p>بر آستانهٔ آن در سر نیازی هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر مکلف عشقی سر نیاز بنه</p></div>
<div class="m2"><p>که هر که هست به کیش خودش نمازی هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو نیک درنگری عشق ما مجازی نیست</p></div>
<div class="m2"><p>حقیقتی پس هر پردهٔ مجازی هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میان عاشق و معشوق کی دویی گنجد</p></div>
<div class="m2"><p>برو برو که تو پنداری امتیازی هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وداع خویش کن اول اگر رفیق منی</p></div>
<div class="m2"><p>که این رهیست خطرناک و ترکتازی هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه احتراز از آن جانب است همواره</p></div>
<div class="m2"><p>گهی ز جانب وحشی هم احترازی هست</p></div></div>