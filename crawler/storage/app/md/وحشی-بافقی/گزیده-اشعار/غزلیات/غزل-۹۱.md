---
title: >-
    غزل ۹۱
---
# غزل ۹۱

<div class="b" id="bn1"><div class="m1"><p>پر گشت دل از راز نهانی که مرا هست</p></div>
<div class="m2"><p>نامحرم راز است زبانی که مرا هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با کس نتوان گفتن و پنهان نتوان داشت</p></div>
<div class="m2"><p>از درد همین است فغانی که مرا هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل سپری ساز ز پولاد صبوری</p></div>
<div class="m2"><p>با عربده سخت کمانی که مرا هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشهور جهان ساخت بر آواز عزیزش</p></div>
<div class="m2"><p>در کوی تو رسوای جهانی که مرا هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بادیست که با بوی تو یک بار نیامیخت</p></div>
<div class="m2"><p>این محرم پیغام رسانی که مرا هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محروم کن گردنم از طوق دگرهاست</p></div>
<div class="m2"><p>از داغ وفای تو نشانی که مرا هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک خندهٔ رسمی ز تو ننهاده ذخیره</p></div>
<div class="m2"><p>این چشم به حسرت نگرانی که مرا هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زایل نکند چین جبین و نگه چشم</p></div>
<div class="m2"><p>بر لطف نهان تو گمانی که مرا هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وحشی تو بده جان که نیاید به عیادت</p></div>
<div class="m2"><p>این یار خوش قاعده دانی که مرا هست</p></div></div>