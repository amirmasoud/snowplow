---
title: >-
    غزل ۳۰۷
---
# غزل ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>آتش به جگر زان رخ افروخته دارم</p></div>
<div class="m2"><p>وین گریهٔ تلخ از جگرسوخته دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی تو چه اندوخته‌ای ز آتش دوری</p></div>
<div class="m2"><p>این داغ که بر جان غم اندوخته دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انداخته‌ام صید مراد از نظر خویش</p></div>
<div class="m2"><p>یعنی صفت باز نظر دوخته دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دام غمت تازه فتادم نگهم دار</p></div>
<div class="m2"><p>من عادت مرغان نو آموخته دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی به دل این آتش سوزنده‌چو فانوس</p></div>
<div class="m2"><p>از پرتو آن شمع بر افروخته دارم</p></div></div>