---
title: >-
    غزل ۱۷۱
---
# غزل ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>اغیار را آسان کشد عاشق چو ترک جان کند</p></div>
<div class="m2"><p>هر کس که از جان بگذرد بسیار خون آسان کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دل به راه سیل غم جان را چه غمخواری کنی</p></div>
<div class="m2"><p>این خانهٔ اندوه را بگذار تا ویران کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان صرف پرکاری که او چون رو به بازار آورد</p></div>
<div class="m2"><p>بازار خوبان بشکند نرخ بلا ارزان کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بی سر و سامانیم یاران نصیحت تا به کی</p></div>
<div class="m2"><p>او می‌گذارد تا کسی فکر سرو سامان کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد کعبهٔ دل از بتان بتخانه وحشی چون کنم</p></div>
<div class="m2"><p>داغ رقیبانش اگر آتشگه گبران کند</p></div></div>