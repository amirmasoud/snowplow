---
title: >-
    غزل ۴۱
---
# غزل ۴۱

<div class="b" id="bn1"><div class="m1"><p>عتاب اگر چه همان در مقام خونریز است</p></div>
<div class="m2"><p>ولیک تیغ تغافل نه آنچنان تیز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلیریی که دلم کرد و می‌زند در صلح</p></div>
<div class="m2"><p>به اعتماد نگه‌های رغبت آمیز است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مریض طفل مزاجند عاشقان ورنه</p></div>
<div class="m2"><p>علاج رنج تغافل دو روز پرهیز است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدیم مات به شطرنج غایبانهٔ تو</p></div>
<div class="m2"><p>به ما بخند که خوش بازیت به انگیز است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنند سلسله در گردنش به زلف تو حشر</p></div>
<div class="m2"><p>دلم که بستهٔ آن طرهٔ دلاویز است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جگر زد آبله وز دیده می‌چکد نمکاب</p></div>
<div class="m2"><p>که بخت شور به ریش جگر نمکریز است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رقیب عزت خود گو مبر که بردر عشق</p></div>
<div class="m2"><p>حریف کوهکنی نیست آنکه پرویز است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به ذوق جستن فرهاد می‌رود گلگون</p></div>
<div class="m2"><p>تو این مبین که عنان بر عنان شبدیز است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شدست دیدهٔ وحشی شکوفه دار و هنوز</p></div>
<div class="m2"><p>در انتظار ثمر زان نهال نوخیز است</p></div></div>