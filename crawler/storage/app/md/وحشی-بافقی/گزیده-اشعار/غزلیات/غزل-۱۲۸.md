---
title: >-
    غزل ۱۲۸
---
# غزل ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>تاب رخ او مهر جهانتاب ندارد</p></div>
<div class="m2"><p>جز زلف کسی پیش رخش تاب ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواب آورد افسانه و افسانهٔ عاشق</p></div>
<div class="m2"><p>هر کس که کند گوش دگر خواب ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پهلوی من و تکیهٔ خاکستر گلخن</p></div>
<div class="m2"><p>دیوانه سر بستر سنجاب ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیل مژه ترسم که تن از پای در آرد</p></div>
<div class="m2"><p>کاین سست بنا طاقت سیلاب ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر سجده کند پیش تو چندان عجبی نیست</p></div>
<div class="m2"><p>وحشی که جز ابروی تو محراب ندارد</p></div></div>