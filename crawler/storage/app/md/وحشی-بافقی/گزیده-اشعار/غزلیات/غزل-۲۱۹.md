---
title: >-
    غزل ۲۱۹
---
# غزل ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>پرسیدن حال دل ریشم بگذارید</p></div>
<div class="m2"><p>یک دم به غم و محنت خویشم بگذارید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاران به میان من و آن مست میایید</p></div>
<div class="m2"><p>گر می‌کشد آن عربده‌کیشم بگذارید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی که برید از ره این کشته عشقش</p></div>
<div class="m2"><p>آنچه از دو سه روز از همه پیشم بگذارید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وحشی‌صفتم جامهٔ سد پاره بدوزند</p></div>
<div class="m2"><p>چسبیده به زخم دل ریشم بگذارید</p></div></div>