---
title: >-
    غزل ۱۹۲
---
# غزل ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>می‌کشم زان تند خو گر صد تغافل می‌کند</p></div>
<div class="m2"><p>دیگری باشد کجا چندین تحمل می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌کند فریاد بلبل از کمال شوق باد</p></div>
<div class="m2"><p>غنچه گویا خنده‌ای در کار بلبل می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر رخ چون زر سرشک همچو سیمم دید و گفت</p></div>
<div class="m2"><p>این گدا را بین که اظهار تجمل می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف او دل برد و کاکل در پی جانست وای</p></div>
<div class="m2"><p>کانچه با جانم نکرد آن زلف، کاکل می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌کند بی نوگلی خونابهٔ دل در کنار</p></div>
<div class="m2"><p>در چمن وحشی چنین دامن پر از گل می‌کند</p></div></div>