---
title: >-
    غزل ۲۵۲
---
# غزل ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>بست زبان شکوه ام لب به سخن گشادنش</p></div>
<div class="m2"><p>عذر عتاب گفتن و وعدهٔ وصل دادنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود جهان جهان فریب از پی جان مضطرب</p></div>
<div class="m2"><p>آمدن و گذشتن و رفتن و ایستادنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناز دماند از زمین، فتنه فشاند از هوا</p></div>
<div class="m2"><p>طرز خرام کردن و پا به زمین نهادنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جذب محبتش کشد، هست بهانه‌ای و بس</p></div>
<div class="m2"><p>اینهمه تند گشتن و در پی من فتادنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی اگر چنین بود وضع زمانه بعد ازین</p></div>
<div class="m2"><p>وای بر آن که باید از مادر دهر زادنش</p></div></div>