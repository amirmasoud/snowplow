---
title: >-
    غزل ۹۷
---
# غزل ۹۷

<div class="b" id="bn1"><div class="m1"><p>تو جفاکن که از اینسوی وفاداری هست</p></div>
<div class="m2"><p>طاقت و صبر مرا حوصلهٔ خواری هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با دلم هر چه توان کرد بکن تا بکشد</p></div>
<div class="m2"><p>کز من و جان منش نیز مددکاری هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌خرم مایه هر شکوه به سد شکر ز تو</p></div>
<div class="m2"><p>من خریدار، گرت جنس دل آزاری هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرد زنجیر به مژگان ادب پاک کند</p></div>
<div class="m2"><p>آنکه در قید کسش ذوق گرفتاری هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما به دامان تو نازیم که پاکست چو گل</p></div>
<div class="m2"><p>ورنه در شهر بسی لعبت بازاری هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکر جورش کن و خشنودی او جو وحشی</p></div>
<div class="m2"><p>که درازست شب حسرت و بیداری هست</p></div></div>