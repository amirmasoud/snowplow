---
title: >-
    غزل ۲۰۹
---
# غزل ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>جنونی داشتم زین پیش بازم آن جنون آمد</p></div>
<div class="m2"><p>مرا تا چون برون آرد که پر غوغا درون آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که دارد باطل السحری که بر بازوی جان بندم</p></div>
<div class="m2"><p>که جادوی قدیمی بر سر سحر و فسون آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندانم چون شود انجام مجلس کان حریف افکن</p></div>
<div class="m2"><p>میی افکند در ساغر کزان می بوی خون آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپر انداختیم اینست اگر چین خم ابرو</p></div>
<div class="m2"><p>که زور این کمان از بازوی طاقت فزون آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا خوانی و من دوری کنم با یک جهان رغبت</p></div>
<div class="m2"><p>چنین باشد بلی‌آن کس که بختش واژگون آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگو وحشی چگونه آمدت این مهر در سینه</p></div>
<div class="m2"><p>همی‌دانم که خوب آمد نمی‌دانم که چون آمد</p></div></div>