---
title: >-
    غزل ۳۸۲
---
# غزل ۳۸۲

<div class="b" id="bn1"><div class="m1"><p>چون کوه غم تاب آورد جسمی بدین فرسودگی</p></div>
<div class="m2"><p>غم بر نتابد بیش ازین باید تن فرمودگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی ناله‌ای نزدیک لب نی گریه‌ای در دل گره</p></div>
<div class="m2"><p>یارب نصیب من مکن اینست اگر آسودگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی به عشق دیگری آلوده‌ای تهمت مکن</p></div>
<div class="m2"><p>حاشا معاذالله کجا عشق من و آلودگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفت آن سوار تندرو ماند این سگ دنباله‌دو</p></div>
<div class="m2"><p>بشتاب ای پای طلب یارب مبادت سودگی</p></div></div>