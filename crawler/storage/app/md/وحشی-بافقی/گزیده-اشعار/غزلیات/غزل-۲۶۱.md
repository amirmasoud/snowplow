---
title: >-
    غزل ۲۶۱
---
# غزل ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>مده از خنده فریب و مزن از غمزه خدنگ</p></div>
<div class="m2"><p>رو که ما را به تو من بعد نه صلح است و نه جنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمزه گو ناوک خود بیهده زن پس مفکن</p></div>
<div class="m2"><p>که دل و جان دگر ساختم از آهن و سنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عذرم این بس اگر از کوی تو رفتم که نماند</p></div>
<div class="m2"><p>نام نیکی که توانم بدلش ساخت به ننگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلبل آن به که فریب گل رعنا نخورد</p></div>
<div class="m2"><p>که دو روزیست وفاداری یاران دو رنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه حسرت نه به آیینه وحشی آن کرد</p></div>
<div class="m2"><p>که توان بردنش از صیقل ابروی تو زنگ</p></div></div>