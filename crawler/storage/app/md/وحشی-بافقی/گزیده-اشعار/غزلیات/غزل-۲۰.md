---
title: >-
    غزل ۲۰
---
# غزل ۲۰

<div class="b" id="bn1"><div class="m1"><p>ساکن گلخن شدم تا صاف کردم سینه را</p></div>
<div class="m2"><p>دادم از خاکستر گلخن صفا آیینه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش رندان حق شناسی در لباسی دیگر است</p></div>
<div class="m2"><p>پر به ما منمای زاهد خرقهٔ پشمینه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گنج صبری بیش ازین در دل به قدر خویش بود</p></div>
<div class="m2"><p>لشکر غم کرد غارت نقد این گنجینه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز مردن درد دل بر خاک می‌سازم رقم</p></div>
<div class="m2"><p>چون کنم کس نیست تا گویم غم دیرینه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر به کشتن کین وحشی می‌رود از سینه‌ات</p></div>
<div class="m2"><p>کرد خون خود بحل ، بردار تیغ کینه را</p></div></div>