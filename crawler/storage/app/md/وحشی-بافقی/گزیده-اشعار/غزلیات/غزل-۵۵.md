---
title: >-
    غزل ۵۵
---
# غزل ۵۵

<div class="b" id="bn1"><div class="m1"><p>بگذران دانسته از ما
گر ادایی سر زده‌ست</p></div>
<div class="m2"><p>بوده نادانسته گر از
ما خطایی سر زده‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر ای صاحب متاعِ حسن این دشنام چیست</p></div>
<div class="m2"><p>در سرِ دریوزه گر از
ما دعایی سر زده‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الله‌الله محرمِ رازِ
تو سازم حرفِ صوت</p></div>
<div class="m2"><p>این زبان و تیغ اگر
حرفی ز جایی سر زده‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>التفاتِ ابرِ رحمت نیست
ورنه بر درت</p></div>
<div class="m2"><p>تخمِ مهری کشتم و
شاخِ وفایی سر زده‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابرِ رحمت گر نبارد
گو سمومش خود مسوز</p></div>
<div class="m2"><p>بعدِ صد خونِ جگر کـاینجا
گیایی سر زده‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست وحشی بلبلِ این
باغ و مست از بوی گل</p></div>
<div class="m2"><p>از سرِ مستی ست گر
از وی نوایی سر زده‌ست</p></div></div>