---
title: >-
    غزل ۱۴۹
---
# غزل ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>دوش از عربده یک مرتبه باز آمده بود</p></div>
<div class="m2"><p>چشم پر عربده‌اش بر سر ناز آمده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمش از ظاهر حالم خبری می‌پرسید</p></div>
<div class="m2"><p>غمزه‌اش نیز به جاسوسی راز آمده بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود هنگامهٔ من گرم چنان ز آتش شوق</p></div>
<div class="m2"><p>که نگاهش به تماشای نیاز آمده بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیر داند که نگاهش چه بلا گرمی داشت</p></div>
<div class="m2"><p>زانکه در بوتهٔ غیرت به گداز آمده بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه اداها که ندیدم چه نظرها که نکرد</p></div>
<div class="m2"><p>بنده‌اش من که عجب بنده نواز آمده بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آرزو بود که هر لحظه به سویت می‌تاخت</p></div>
<div class="m2"><p>داشت می‌دانی و خوش در تک و تاز آمده بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وحشی از بزم که این مایهٔ خوشحالی یافت</p></div>
<div class="m2"><p>که سوی کلبهٔ ما با می و ساز آمده بود</p></div></div>