---
title: >-
    غزل ۲۷۰
---
# غزل ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>عشق ما پرتو ندارد ما چراغ مرده‌ایم</p></div>
<div class="m2"><p>گرم کن هنگامهٔ دیگر که ما افسرده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر همه مرهم شوی ما را نباشی سودمند</p></div>
<div class="m2"><p>کز تو پر آزردگی داریم و بس آزرده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لخت لخت است این جگر چون خود نباشد لخت لخت</p></div>
<div class="m2"><p>که مگر دندان حسرت بر جگر افشرده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در نمی‌گیرد باو نیرنگ سازیهای ما</p></div>
<div class="m2"><p>گر چه ز افسون آب از آتش برون آورده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی آن چشمت اگر خواند به خود نادیده کن</p></div>
<div class="m2"><p>کان فریب است اینکه ما سد بار دیگر خورده‌ایم</p></div></div>