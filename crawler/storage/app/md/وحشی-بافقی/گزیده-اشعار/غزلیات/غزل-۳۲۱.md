---
title: >-
    غزل ۳۲۱
---
# غزل ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>در بزم وصل اگر چه همین در میان منم</p></div>
<div class="m2"><p>چون نیک بنگری ز همه بر کران منم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگی ز گل ندارم و بویی ز یاسمن</p></div>
<div class="m2"><p>آری کلیددار در بوستان منم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خار وخس زیاده بر آتش نهاد نیست</p></div>
<div class="m2"><p>گر بوستان حسن ترا باغبان منم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معلوم مهربانی اهل هوس که چیست</p></div>
<div class="m2"><p>بشنو سخن که عاشقم و مهربان منم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای گل اگر به گفتهٔ وحشی عمل کنی</p></div>
<div class="m2"><p>سد ساله نو بهار خزان را ضمان منم</p></div></div>