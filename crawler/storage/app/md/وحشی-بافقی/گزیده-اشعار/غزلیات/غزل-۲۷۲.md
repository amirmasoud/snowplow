---
title: >-
    غزل ۲۷۲
---
# غزل ۲۷۲

<div class="b" id="bn1"><div class="m1"><p>نیستیم از دوریت با داغ حرمان نیستیم</p></div>
<div class="m2"><p>دل پشیمان است لیکن ما پشیمان نیستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه از دل می‌رود عشق به جان آمیخته</p></div>
<div class="m2"><p>با وجود این وداع صعب گریان نیستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گو جراحت کهنه شو ما از علاج آسوده‌ایم</p></div>
<div class="m2"><p>درد گو ما را بکش در فکر درمان نیستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچه مارا خوار می‌کرد آن محبت بود و رفت</p></div>
<div class="m2"><p>گو به چشم آن مبین مارا که ما آن نیستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما سپر انداختیم اینک حریف عشق نیست</p></div>
<div class="m2"><p>طبل برگشتن بزن ما مرد میدان نیستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یوسف دیگر به دست آریم وحشی قحط نیست</p></div>
<div class="m2"><p>ما مگر درمصر یعنی شهر کاشان نیستیم</p></div></div>