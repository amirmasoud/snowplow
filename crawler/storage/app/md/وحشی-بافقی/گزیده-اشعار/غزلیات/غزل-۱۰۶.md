---
title: >-
    غزل ۱۰۶
---
# غزل ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>گرد سر تو گردم و آن رخش راندنت</p></div>
<div class="m2"><p>وان‌دست و تازیانه و مرکب جهاندنت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهری به ترکتاز دهد بلکه عالمی</p></div>
<div class="m2"><p>ترکانه برنشستن و هر سو دواندنت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش خدنگ پرکش ناز تو جان دهم</p></div>
<div class="m2"><p>وان شست باز کردن و تا پر نشاندنت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میرم به آن عتاب که گویا سرشته‌اند</p></div>
<div class="m2"><p>سد لطف با ادای تعرض رساندنت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طرز نگاه نازم و جنبیدن مژه</p></div>
<div class="m2"><p>وان دامن کرشمه به مردم فشاندنت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وحشی اگر تو فارغی از درد عشق ، چیست</p></div>
<div class="m2"><p>این آه و ناله کردن و این شعر خواندنت</p></div></div>