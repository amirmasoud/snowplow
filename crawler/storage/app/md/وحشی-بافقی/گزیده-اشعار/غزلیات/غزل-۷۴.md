---
title: >-
    غزل ۷۴
---
# غزل ۷۴

<div class="b" id="bn1"><div class="m1"><p>کوچنان یاری که داند قدر اهل درد چیست</p></div>
<div class="m2"><p>چیست عشق و کیست مرد عشق و درد مرد چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلشن حسنی ولی بر آه سرد ما مخند</p></div>
<div class="m2"><p>آه اگر یابی که تأثیر هوای سرد چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای که می‌گویی نداری شاهدی بر درد عشق</p></div>
<div class="m2"><p>جان غم پرورد و آه سرد و روی زرد چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه می‌پرسد نشان راحت و لذت ز ما</p></div>
<div class="m2"><p>کاش پرسد اول این معنی که خواب و خورد چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرنه عاشق صبر می‌دارد به تنهایی ز دوست</p></div>
<div class="m2"><p>آنچه می‌گویند از مجنون تنها گرد چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وحشی از پی گر نبودی آن سوار تند را</p></div>
<div class="m2"><p>می‌رسی باز از کجا وین چهرهٔ پر گرد چیست</p></div></div>