---
title: >-
    غزل ۸۷
---
# غزل ۸۷

<div class="b" id="bn1"><div class="m1"><p>چه لطف‌ها که در این شیوهٔ نهانی نیست</p></div>
<div class="m2"><p>عنایتی که تو داری به من بیانی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرشمه گرم سؤال است، لب مکن رنجه</p></div>
<div class="m2"><p>که احتیاج به پرسیدن زبانی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رموز کشف و کرامات سالکان طریق</p></div>
<div class="m2"><p>ورای رمز شناسی و نکته‌دانی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هر که خواه نشین گرچه این نه شیوهٔ تست</p></div>
<div class="m2"><p>که از تو در دل ما راه بدگمانی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا ز کیش محبت همین پسند افتاد</p></div>
<div class="m2"><p>که گر چه هست سد آواز سرگرانی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو خون مردهٔ وحشی چرا نمی‌ریزی</p></div>
<div class="m2"><p>بریز تا برود، آب زندگانی نیست</p></div></div>