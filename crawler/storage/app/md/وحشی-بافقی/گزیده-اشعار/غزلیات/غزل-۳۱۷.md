---
title: >-
    غزل ۳۱۷
---
# غزل ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>نه من از تو مهر خواهم نه تو بگذری ز کین هم</p></div>
<div class="m2"><p>نه تر است این مروت نه مراست چشم این هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه بهانه ساخت دیگر به هلاک بیگناهان</p></div>
<div class="m2"><p>که تعرض است بر لب گرهیست بر جبین هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به میان جنگ و صلحت من و دست و آن دعاها</p></div>
<div class="m2"><p>که ز آستین بر آید نه رود به آستین هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه همین فلک خجل شد ز کف نیاز عشقم</p></div>
<div class="m2"><p>که ز سجده‌های شوقم شده منفعل زمین هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برسان ز خرمن خود مددی به بی نصیبان</p></div>
<div class="m2"><p>که نه خرمن تو ماند نه هجوم خوشه چین هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه متاع رستگاری بودم ز سجدهٔ بت</p></div>
<div class="m2"><p>که ذخیره‌ای نبردم ز نگاه واپسین هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تو خوش نماست وحشی ره و رسم زهد و رندی</p></div>
<div class="m2"><p>که دلیست حق شناس و نظری خدای بین هم</p></div></div>