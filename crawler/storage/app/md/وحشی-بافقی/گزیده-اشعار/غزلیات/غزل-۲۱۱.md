---
title: >-
    غزل ۲۱۱
---
# غزل ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>کی اهل دل به کام خود از دوستان برند</p></div>
<div class="m2"><p>تا کارشان به جان نرسد کی ز جان برند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ما برید یار به اندک حکایتی</p></div>
<div class="m2"><p>چندان نبود این که ز هم دوستان برند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد گرم تا شنید ز ما سوز دل چو شمع</p></div>
<div class="m2"><p>آه این چه حرف بود که ما را زبان برند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکس که گشت باعث سوز فراق ما</p></div>
<div class="m2"><p>یارب سرش به مجلس او شمعسان برند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی مبر به تیغ ز جانان که اهل دل</p></div>
<div class="m2"><p>از هم نمی‌برند اگر از جهان برند</p></div></div>