---
title: >-
    غزل ۳۴۶
---
# غزل ۳۴۶

<div class="b" id="bn1"><div class="m1"><p>ای که دل بردی ز دلدار من آزارش مکن</p></div>
<div class="m2"><p>آنچه او در کار من کردست در کارش مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هندوی چشم تو شد می‌بین خریدارانه‌اش</p></div>
<div class="m2"><p>اعتمادی لیک بر ترکان خونخوارش مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه تو سلطان حسنی دارد او هم کشوری</p></div>
<div class="m2"><p>شوکت حسنش مبر بی‌قدر و مقدارش مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انتقام از من کشد مپسند بر من این‌ستم</p></div>
<div class="m2"><p>رخصت نظاره‌اش ده منع دیدارش مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جای دیگر دارد او شهباز اوج جان ماست</p></div>
<div class="m2"><p>هم قفس با خیل مرغان گرفتارش مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این چه گستاخی‌ست وحشی تا چه باشد حکم ناز</p></div>
<div class="m2"><p>التماس لطف با او کردن از یارش مکن</p></div></div>