---
title: >-
    غزل ۷۱
---
# غزل ۷۱

<div class="b" id="bn1"><div class="m1"><p>مریض عشق اگر سد بود علاج یکیست</p></div>
<div class="m2"><p>مرض یکی و طبیعت یکی، مزاج یکیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمام در طلب وصل و وصل می‌طلبیم</p></div>
<div class="m2"><p>اگر یکیم و اگر سد که احتیاج یکیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه مانده اسیر است همچنان خوش باش</p></div>
<div class="m2"><p>که منتهای ره کاروان حاج یکیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریب تاج مرصع مده به سربازان</p></div>
<div class="m2"><p>که ترک سر بر این جمع و ننگ تاج یکیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همین منادی عشقست در درون خراب</p></div>
<div class="m2"><p>که آنکه می‌دهد این ملک را رواج یکیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه جای زحمت و راحت که پیش پای طلب</p></div>
<div class="m2"><p>حریر نسترن و نشتر زجاج یکیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجز فساد مجو وحشی از طبیعت دهر</p></div>
<div class="m2"><p>که وضع عنصر و تألیف امتزاج یکیست</p></div></div>