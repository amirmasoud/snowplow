---
title: >-
    غزل ۱۲۲
---
# غزل ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>جانان نظری کو ز وفا داشت ندارد</p></div>
<div class="m2"><p>لطفی که از این پیش به ما داشت ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رحمی که به این غمزده‌اش بود نماندست</p></div>
<div class="m2"><p>لطفی که به این بی سرو پا داشت ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن پادشه حسن ندانم چه خطا دید</p></div>
<div class="m2"><p>کان لطف که نسبت به گدا داشت ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر یار خبردار شود از غم عاشق</p></div>
<div class="m2"><p>جوری که به این قوم روا داشت ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی اگر از دیده رود خون عجبی نیست</p></div>
<div class="m2"><p>کان گوشهٔ چشمی که به ما داشت ندارد</p></div></div>