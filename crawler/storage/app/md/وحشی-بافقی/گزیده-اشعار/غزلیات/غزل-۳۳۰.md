---
title: >-
    غزل ۳۳۰
---
# غزل ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>مرا با خار غم بگذار و گشت باغ و گلشن کن</p></div>
<div class="m2"><p>پی آرایش بزم حریفان گل به دامن کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو شمع مجلس افروزی ، من سرگشته پروانه</p></div>
<div class="m2"><p>مرا آتش به جان زن دیگران را خانه روشن کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکن نادیده وز من تند چون بیگانگان مگذر</p></div>
<div class="m2"><p>مرا شاید که جایی دیده باشی چشم بر من کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو کار من نخواهد شد به کام دوستان از تو</p></div>
<div class="m2"><p>هلاکم ساز باری فارغم از طعن دشمن کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببین وحشی که چون سویت به زهر چشم می‌بیند</p></div>
<div class="m2"><p>ترا زان پیش کز مجلس براند عزم رفتن کن</p></div></div>