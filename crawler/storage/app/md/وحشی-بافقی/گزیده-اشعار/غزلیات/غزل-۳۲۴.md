---
title: >-
    غزل ۳۲۴
---
# غزل ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>آمد آمد حسن در رخش غرور انگیختن</p></div>
<div class="m2"><p>اینک اینک عشق می‌آید به شور انگیختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کرا کحل محبت چشم جان روشن نساخت</p></div>
<div class="m2"><p>روز حشرش همچنان خواهند کور انگیختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پا به حرمت نه در این وادی که موسی حد نداشت</p></div>
<div class="m2"><p>گرد نعلین از تجلیگاه طور انگیختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسم بزم ماست دود از دل بر آوردن نخست</p></div>
<div class="m2"><p>سوختن چون عود و از مجمر بخور انگیختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست کردن در کمر با عشق کاری سهل نیست</p></div>
<div class="m2"><p>فتنه‌ای نتوان ز بهر خود به زور انگیختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرصهٔ عشق و حریف ما چنین منصوبه باز</p></div>
<div class="m2"><p>سخت بازی چیست بازیهای دور انگیختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیز و دامن برفشان وحشی که کار دهر نیست</p></div>
<div class="m2"><p>جز غبار فتنه و گرد فتور انگیختن</p></div></div>