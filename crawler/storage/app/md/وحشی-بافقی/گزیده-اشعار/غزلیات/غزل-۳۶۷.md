---
title: >-
    غزل ۳۶۷
---
# غزل ۳۶۷

<div class="b" id="bn1"><div class="m1"><p>گذشتم از درت بر خاک سد جا چشم تر مانده</p></div>
<div class="m2"><p>ببین کز اشک سرخم سد نشان بر خاک در مانده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا بنگر که غمناکیست چشم آرزو بر در</p></div>
<div class="m2"><p>به امید نگاهی بر سراین رهگذر مانده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجز من هر کرا دیدی ز بیماران غم گشتی</p></div>
<div class="m2"><p>هنوز از کف منه خنجر که بیمار دگر مانده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآمد عمرها کز دور دیدم نخل بالایش</p></div>
<div class="m2"><p>هنوزم آن قد و رفتار در پیش نظر مانده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر کس گفته بی‌تقریب وحشی عرض حال خود</p></div>
<div class="m2"><p>که در بزمت به این تقریب یک دم بیشتر مانده</p></div></div>