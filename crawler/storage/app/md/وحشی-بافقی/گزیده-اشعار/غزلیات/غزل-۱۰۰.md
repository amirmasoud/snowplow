---
title: >-
    غزل ۱۰۰
---
# غزل ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>از پیِ بهبودِ دردِ ما دوا سودی نداشت</p></div>
<div class="m2"><p>هرکه شد بیمارِ دردِ عشق بهبودی نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود روزی آن عنایت‌ها که با ما می‌نمود</p></div>
<div class="m2"><p>خوش نمودی داشت اما آنچنان بودی نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش کـآمد با رقیبان مست و خنجر می‌کشید</p></div>
<div class="m2"><p>غیرِ قصدِ کشتنِ ما هیچ مقصودی نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق غالب گشت اگر در بزمِ او آهی زدم</p></div>
<div class="m2"><p>کی فروزان گشت جایی کـآتشی دودی نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جایِ خود در بزمِ خوبان شمع‌سان چون گرم
کرد</p></div>
<div class="m2"><p>آنکه اشکِ گرم و آهِ آتش‌آلودی نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داشت سودای رخش وحشی به سر در هر نفس</p></div>
<div class="m2"><p>لیک از آن سودا چه حاصل یک دمش سودی
نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وحشی از دردِ محبت لذتی چندان نیافت</p></div>
<div class="m2"><p>هرکه جسمی ریش و جان دردفرسودی نداشت</p></div></div>