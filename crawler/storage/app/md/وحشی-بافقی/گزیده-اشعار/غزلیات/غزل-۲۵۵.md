---
title: >-
    غزل ۲۵۵
---
# غزل ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>تکیه کردم بر وفای او غلط کردم ، غلط</p></div>
<div class="m2"><p>باختم جان در هوای او غلط کردم، غلط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمر کردم صرف او فعلی عبث کردم ، عبث</p></div>
<div class="m2"><p>ساختم جان را فدای او غلط کردم ، غلط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل به داغش مبتلا کردم خطا کردم ، خطا</p></div>
<div class="m2"><p>سوختم خود را برای او غلط کردم ، غلط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینکه دل بستم به مهر عارضش بد بود بد</p></div>
<div class="m2"><p>جان که دادم در هوای او غلط کردم ، غلط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو وحشی رفت جانم درهوایش حیف ، حیف</p></div>
<div class="m2"><p>خو گرفتم با جفای او غلط کردم ، غلط</p></div></div>