---
title: >-
    غزل ۱۵۷
---
# غزل ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>لب بجنبان که سر تنگ شکر بگشاید</p></div>
<div class="m2"><p>شکرستان ترا قفل ز در بگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمزه را بخش اجازت که به خنجر بکند</p></div>
<div class="m2"><p>دیده‌ای کو به تو گستاخ نظر بگشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ره نظارگیان بسته به مژگان فرما</p></div>
<div class="m2"><p>که به یک چشم زدن راه گذر بگشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گلویم ز تو این گریه که شد عقدهٔ درد</p></div>
<div class="m2"><p>گرهی نیست که از جای دگر بگشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب مارا به در صبح نه آن قفل زدند</p></div>
<div class="m2"><p>که به مفتاح دعاهای سحر بگشاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه را کشت، بگویید که با خاطر جمع</p></div>
<div class="m2"><p>این زمان باز کند تیغ و کمر بگشاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راه تقریب حکایت ندهی وحشی را</p></div>
<div class="m2"><p>که مبادا گله را پیش تو سر بگشاید</p></div></div>