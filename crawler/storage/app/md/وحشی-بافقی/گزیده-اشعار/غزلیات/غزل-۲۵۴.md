---
title: >-
    غزل ۲۵۴
---
# غزل ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>نیستم یک دم ز درد و محنت هجران خلاص</p></div>
<div class="m2"><p>کو اجل تا سازدم زین درد بی درمان خلاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار دشوار است برمن ، وقت کار است ای اجل</p></div>
<div class="m2"><p>سعی کن باشد که گردانی مرا آسان خلاص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشتی تابوت می‌خواهم که آب از سرگذشت</p></div>
<div class="m2"><p>تا به آن کشتی کنم خود را ازین توفان خلاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند نالم بردرش ای همنشین زارم بکش</p></div>
<div class="m2"><p>کو رهد از درد سر ، من گردم از افغان خلاص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بست وحشی با دل خرم ازین غمخانه رخت</p></div>
<div class="m2"><p>چون گرفتاری که خود را یابد از زندان خلاص</p></div></div>