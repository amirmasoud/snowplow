---
title: >-
    غزل ۵۹
---
# غزل ۵۹

<div class="b" id="bn1"><div class="m1"><p>خوش صیدِ غافلی به سر تیر آمده‌ست</p></div>
<div class="m2"><p>زه کن کمانِ ناز که نخجیر آمده‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی به کارِ تیغِ تو آید نگاه دار</p></div>
<div class="m2"><p>این گردنی که در خمِ زنجیر آمده‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کو عشق تا شوند همه معترف به عجز</p></div>
<div class="m2"><p>اول خرد که از پیِ تدبیر آمده‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشقی که ما دواسبه از او می‌گریختیم</p></div>
<div class="m2"><p>این است کـآمده‌ست و عنان‌گیر آمده‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملکِ دلِ مرا که سواری بس است عشق</p></div>
<div class="m2"><p>با یک جهان سپاه به تسخیر آمده‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خاره کنده‌اند حریفان به حکمِ عشق</p></div>
<div class="m2"><p>جویی که چند فرسخ از آن شیر آمده‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی لطفی‌ای به حالِ تو دیدم که سوختم</p></div>
<div class="m2"><p>وحشی بگو که از تو چه تقصیر آمده‌ست</p></div></div>