---
title: >-
    غزل ۳۵۱
---
# غزل ۳۵۱

<div class="b" id="bn1"><div class="m1"><p>ترسم جنون غالب شود طغیان کند سودای تو</p></div>
<div class="m2"><p>طوقم به گردن برنهد عشق جنون فرمای تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌آیی و می‌افکند چا کم به جیب عافیت</p></div>
<div class="m2"><p>شاخ گلی دامن کشان یعنی قد رعنای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وقتی نگاهی رسم بود از چشم سنگین دل بتان</p></div>
<div class="m2"><p>آن رسم هم منسوخ شد در عهد استغنای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرسوده سرها در رهت در هر سری سد آرزو</p></div>
<div class="m2"><p>وان آرزوها خاک شد یک یک به زیر پای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی ببین اندوه دل وز سخت جانی دم مزن</p></div>
<div class="m2"><p>کز هم بپاشد کوه را اندوه جان فرسای تو</p></div></div>