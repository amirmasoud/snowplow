---
title: >-
    غزل ۲۳۹
---
# غزل ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>تن اگر نبود ز نزدکان چو شد گو دور باش</p></div>
<div class="m2"><p>دیده در وصل است پا از بزم گو مهجور باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نگاهی کان به هر ماهی کنی آنهم ز دور</p></div>
<div class="m2"><p>سهل باشد گو عنایت گونه منظور باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک نگاه لطف از چشم تو ما را می‌رسد</p></div>
<div class="m2"><p>گو کسی کاین نیز نتواند که بیند کور باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزم بدمستان عشق است این به حکمت باده نوش</p></div>
<div class="m2"><p>ساقی مجلس شود هم مست و هم مخمور باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لطف با اغیار و کین با ما تفاوت از کجاست</p></div>
<div class="m2"><p>با همه هر نوع می‌باشی به یک دستور باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیل بی لطفی همین سر در بنای ما مده</p></div>
<div class="m2"><p>خانهٔ ما یا همه ویرانه یا معمور باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار ما و کار وحشی پیش تیغت چون یکیست</p></div>
<div class="m2"><p>گو دلت بی رحم و بازوی ستم پر زور باش</p></div></div>