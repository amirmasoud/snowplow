---
title: >-
    غزل ۳۴۱
---
# غزل ۳۴۱

<div class="b" id="bn1"><div class="m1"><p>زین سان که تند می‌گذرد خوش‌خرام من</p></div>
<div class="m2"><p>کی ملتفت شود به جواب سلام من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم بگو از آن لبِ شیرین حکایتی</p></div>
<div class="m2"><p>سد تلخ گفت دلبر شیرین‌کلام من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن شمع گر ز سوز دل من خبر نداشت</p></div>
<div class="m2"><p>بهر چه برفروخت چو بشنید نام من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کامی نیافتم ز لب او به بوسه‌ای</p></div>
<div class="m2"><p>هرگز نبود آن لب شیرین به کام من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی غزال من که به من آرمیده بود</p></div>
<div class="m2"><p>وحشی چنان نشد که شود باز رام من</p></div></div>