---
title: >-
    غزل ۱۱۰
---
# غزل ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>هجران رفیقِ بختِ زبونِ کسی مباد</p></div>
<div class="m2"><p>خصمی چنین دلیر به خونِ کسی مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب حریفِ گرم‌کنی همچو آرزو</p></div>
<div class="m2"><p>گرم اختلاطِ داغِ درونِ کسی مباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این شعله‌های ظاهر و باطن گدازِ هجر</p></div>
<div class="m2"><p>پیراهنِ درون و برونِ کسی مباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن گریه‌های شوق که غلتید کوه از او</p></div>
<div class="m2"><p>سیلِ بنای صبر و سکونِ کسی مباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد بندِ شوق پاره کند زورِ آرزو</p></div>
<div class="m2"><p>یارب که بختِ شور و جنونِ کسی مباد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نعلم به نامِ جملهٔ اجزا در آتش است</p></div>
<div class="m2"><p>جادویِ او به فکرِ فسونِ کسی مباد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وحشی هزار بادیه دورم ز کعبه کرد</p></div>
<div class="m2"><p>این بختِ بد که راهنمونِ کسی مباد</p></div></div>