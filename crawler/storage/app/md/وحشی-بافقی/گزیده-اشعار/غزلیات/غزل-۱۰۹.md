---
title: >-
    غزل ۱۰۹
---
# غزل ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>هرگزم یارب از آن دیدار مهجوری مباد</p></div>
<div class="m2"><p>این نگاه دور را از روی او دوری مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من کجا و رخصت آن بزم دانم جای خویش</p></div>
<div class="m2"><p>دیگران هم رخصت ار خواهند دستوری مباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر مرض کز عشق پیش آمد علاجش بر منست</p></div>
<div class="m2"><p>لیک جانم را ز درد رشک و رنجوری مباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم غارت کرده را صعب است از دیدار دوخت</p></div>
<div class="m2"><p>هیچ عاشق را الهی هرگز این کوری مباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوهر حسن تو کنج خانهٔ آباد نیست</p></div>
<div class="m2"><p>بر بنای جان وحشی نام معموری مباد</p></div></div>