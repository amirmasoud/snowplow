---
title: >-
    غزل ۱۲
---
# غزل ۱۲

<div class="b" id="bn1"><div class="m1"><p>عزت مبردر کار دل این لطف بیش از پیش را</p></div>
<div class="m2"><p>این بس که ضایع می‌کنی برمن جفای خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لطفی که بد خو سازدم ناید به کار جان من</p></div>
<div class="m2"><p>اسباب کین آماده کن خوی ملال اندیش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند سیل فتنه گر چون بخت باشد ور رسی</p></div>
<div class="m2"><p>کشتی به دیوار آوری ویرانهٔ درویش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر کافر عشق بتان جایز نباشد مرحمت</p></div>
<div class="m2"><p>بی جرم باید سوختن مفتی منم این کیش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشقم خراش سینه شد گو لطف تو مرهم منه</p></div>
<div class="m2"><p>گر التفاتی می‌کنی ناسور کن این ریش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نیش زنبورم به دل گو زهر می‌ریز از مژه</p></div>
<div class="m2"><p>افیون حیرت خورده‌ام زحمت ندانم نیش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با پادشاه من بگو وحشی که چون دور از تو شد</p></div>
<div class="m2"><p>تاریخ برخوان گه گهی خوبان عهد خویش را</p></div></div>