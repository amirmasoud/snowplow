---
title: >-
    غزل ۳۵
---
# غزل ۳۵

<div class="b" id="bn1"><div class="m1"><p>یاد او کردم ز جان سد آه درد آلود خاست</p></div>
<div class="m2"><p>خوی گرمش در دلم بگذشت و از دل دود خاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نفس امشب فرو بردم جدا از صبح وصل</p></div>
<div class="m2"><p>کز سر بالین من آن سست پیمان زود خاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش در مجلس به بوی زلف او آهی زدم</p></div>
<div class="m2"><p>آتشی افتاد در مجمر که دود از عود خاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سرود درد من در بزم او افتاد شور</p></div>
<div class="m2"><p>نی ز درد من بنالید و فغان از رود خاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه وحشی خاک شد بنشست همچون گردباد</p></div>
<div class="m2"><p>از زمین دیگر به عزم کعبهٔ مقصود خاست</p></div></div>