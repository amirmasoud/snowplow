---
title: >-
    غزل ۷۹
---
# غزل ۷۹

<div class="b" id="bn1"><div class="m1"><p>مست آمدی که موجب چندین ملال چیست</p></div>
<div class="m2"><p>هشیار چون شوی به تو گویم که حال چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من حرف می کشیدن اغیار می‌زنم</p></div>
<div class="m2"><p>آن مست ناز را عرق انفعال چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خنجر کشی که ما ز تو قطع نظر کنیم</p></div>
<div class="m2"><p>کی می‌بریم از تو ، ترا در خیال چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دشت هجر می‌رسم آگاهیم دهید</p></div>
<div class="m2"><p>وضع نشست و خاست به بزم وصال چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی مپرس مسأله عاشقی ز من</p></div>
<div class="m2"><p>مفتی منم به دین محبت سؤال چیست</p></div></div>