---
title: >-
    غزل ۶۴
---
# غزل ۶۴

<div class="b" id="bn1"><div class="m1"><p>آنکس که مرا از نظر انداخته اینست</p></div>
<div class="m2"><p>اینست که پامال غمم ساخته، اینست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوخی که برون آمده شب مست و سرانداز</p></div>
<div class="m2"><p>تیغم زده و کشته و نشناخته، اینست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترکی که ازو خانهٔ من رفته به تاراج</p></div>
<div class="m2"><p>اینست که از خانه برون تاخته اینست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماهی که بود پادشه خیل نکویان</p></div>
<div class="m2"><p>اینست که از ناز قد افراخته، اینست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی که به شطرنج غم و نرد محبت</p></div>
<div class="m2"><p>یکباره متاع دل و دین باخته اینست</p></div></div>