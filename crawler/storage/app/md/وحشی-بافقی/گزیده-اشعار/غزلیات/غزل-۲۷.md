---
title: >-
    غزل ۲۷
---
# غزل ۲۷

<div class="b" id="bn1"><div class="m1"><p>پاک ساز از غیر دل ، وز خود تهی شو چون حباب</p></div>
<div class="m2"><p>گر سبک روحی توانی خیمه زد بر روی آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خودنمایی کی کند آن کس که واصل شد به دوست</p></div>
<div class="m2"><p>چون نماید مه چو گردد متصل با آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی دهد در جلوه گاه دوست عاشق راه غیر</p></div>
<div class="m2"><p>دم مزن از عشق اگر ره می‌دهی بر دیده خواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست بر ذرات یکسان پرتو خورشید فیض</p></div>
<div class="m2"><p>لیک باید جوهر قابل که گردد لعل ناب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی از دریای رحمت گر دهندت رشحه‌ای</p></div>
<div class="m2"><p>گام بر روی هوا آسان زنی همچون سحاب</p></div></div>