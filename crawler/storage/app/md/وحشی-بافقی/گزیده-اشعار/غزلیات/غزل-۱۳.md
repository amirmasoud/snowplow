---
title: >-
    غزل ۱۳
---
# غزل ۱۳

<div class="b" id="bn1"><div class="m1"><p>منع مهر غیر نتوان کرد یار خویش را</p></div>
<div class="m2"><p>هر که باشد، دوست دارد دوستار خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نگاهی از پی کاریست بر حال کسی</p></div>
<div class="m2"><p>عشق می‌داند نکو آداب کار خویش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر گو از من قیاس کار کن این عشق چیست</p></div>
<div class="m2"><p>می‌کند بیچاره ضایع روزگار خویش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صید ناوک خورده خواهد جست، ما خود بسملیم</p></div>
<div class="m2"><p>ای شکار افکن بتاز از پی شکار خویش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تو اخلاصم دگر شد بسکه دیدم نقض عهد</p></div>
<div class="m2"><p>من که در آتش نگردانم عیار خویش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بادهٔ این شیشه بیش از ساغر اغیار نیست</p></div>
<div class="m2"><p>بشکنیم از جای دیگر ما خمار خویش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار رفت از دست ،وحشی پای بستی کن ز صبر</p></div>
<div class="m2"><p>این بنای طاقت نااستوار خویش را</p></div></div>