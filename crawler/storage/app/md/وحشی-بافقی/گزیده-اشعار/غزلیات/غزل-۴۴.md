---
title: >-
    غزل ۴۴
---
# غزل ۴۴

<div class="b" id="bn1"><div class="m1"><p>امروز ناز عذر جفاهای رفته خواست</p></div>
<div class="m2"><p>عذری که او نخواست، تبسم ، نهفته خواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بندهٔ نگه که به سد شرح و بسط گفت</p></div>
<div class="m2"><p>حرف عنایتی که تبسم، نگفته خواست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نوک غمزه سفته شد و خوب سفته شد</p></div>
<div class="m2"><p>درهای راز هم که نگاهش نسفته خواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لطف آمد و تلافی سد ساله می‌کند</p></div>
<div class="m2"><p>خشم ارچه کرد هر چه در این یک دو هفته خواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بارد به وقت خود همه باران التفات</p></div>
<div class="m2"><p>ابر عنایتی که ریاضی شکفته خواست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل را نوید کاتش خوی تو پاک سوخت</p></div>
<div class="m2"><p>خار و خسی کش از سر آن کوی رفته خواست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکر خدا را که مرد به بیداری فراق</p></div>
<div class="m2"><p>وحشی کسی که دیدهٔ بخت تو خفته خواست</p></div></div>