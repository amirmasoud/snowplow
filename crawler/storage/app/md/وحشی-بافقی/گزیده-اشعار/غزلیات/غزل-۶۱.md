---
title: >-
    غزل ۶۱
---
# غزل ۶۱

<div class="b" id="bn1"><div class="m1"><p>از تو همین تواضع عامی مرا بس است</p></div>
<div class="m2"><p>در هفته‌ای جواب سلامی مرا بس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی صدر وصل خواهم و نی پیشگاه قرب</p></div>
<div class="m2"><p>همراهی تو یک دو سه گامی مرا بس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیهوده گرد عرصهٔ جولانگه توام</p></div>
<div class="m2"><p>گاهی کرشمه‌ای و خرامی مرا بس است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خمخانه‌ای نمی‌طلبم از شراب وصل</p></div>
<div class="m2"><p>یک قطره بازمانده جامی مرا بس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی مگو، بگو سگ کو ، بلکه خاک راه</p></div>
<div class="m2"><p>یعنی ز تو نوازش مامی مرا بس است</p></div></div>