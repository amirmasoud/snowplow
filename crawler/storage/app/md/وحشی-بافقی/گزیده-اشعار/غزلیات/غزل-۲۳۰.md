---
title: >-
    غزل ۲۳۰
---
# غزل ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>دوش پر عربده‌ای بود و نه آنست امروز</p></div>
<div class="m2"><p>نگهش قاصد سد لطف نهانست امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسنش آنست ولی خود نه همانست بلی</p></div>
<div class="m2"><p>بودی آفت دل ، راحت جانست امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی در روی و نگه بر نگه و چشم به چشم</p></div>
<div class="m2"><p>حرف ما و تو چه محتاج زبانست امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرح رازی که میان من و او خواهد بود</p></div>
<div class="m2"><p>بیش از حوصلهٔ نطق و بیانست امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چه ها بر سر و دستار حریفان گذرد</p></div>
<div class="m2"><p>زان می‌تند که در رطل گرانست امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر کمان می‌کشد آن غمزهٔ خدنگی که مپرس</p></div>
<div class="m2"><p>ای خوشا سینهٔ وحشی که نشانست امروز</p></div></div>