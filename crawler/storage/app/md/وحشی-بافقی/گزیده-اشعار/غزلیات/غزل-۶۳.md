---
title: >-
    غزل ۶۳
---
# غزل ۶۳

<div class="b" id="bn1"><div class="m1"><p>خود رنجم و خود صلح کنم عادتم اینست</p></div>
<div class="m2"><p>یک روز تحمل نکنم طاقتم اینست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خنجر الماس نهادم ز تو پهلو</p></div>
<div class="m2"><p>آسوده دلا بین که ز تو راحتم اینست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جایی که بود خاک به سد عزت سرمه</p></div>
<div class="m2"><p>بیقدر تر از خاک رهم، عزتم اینست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با خاک من آمیخته خونابهٔ حسرت</p></div>
<div class="m2"><p>زین آب سرشتند مرا ، طینتم اینست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میلم همه جاییست که خواری همه آنجاست</p></div>
<div class="m2"><p>با خصلت ذاتی چه کنم فطرتم اینست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وحشی نرود از در جانان به سد آزار</p></div>
<div class="m2"><p>در اصل چنین آمده‌ام ، خصلتم اینست</p></div></div>