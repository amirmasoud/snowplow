---
title: >-
    غزل ۱۹۷
---
# غزل ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>که جان برد اگر آن مست سرگران بدرآید</p></div>
<div class="m2"><p>کلاه کج نهد از ناز و بر سرگذر آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسید بار دگر بار حسن حکم چه باشد</p></div>
<div class="m2"><p>دگر که از نظر افتد که باز در نظر آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سوی مصر به کنعان عجب رهیست که باشد</p></div>
<div class="m2"><p>هنوز قافله در مصر و قاصد و خبر آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمینه خاصیت عشق جذبه‌ایست که کس را</p></div>
<div class="m2"><p>ز هر دری که پرانند بیش ، بیشتر آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سبو به دوش و صراحی به دست و محتسب از پی</p></div>
<div class="m2"><p>نعوذبالله اگر پای من به سنگ بر آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگو که وحشیم آید ز پی اگر بروم من</p></div>
<div class="m2"><p>چه مانعست نیاید چرا به چشم و سر آید</p></div></div>