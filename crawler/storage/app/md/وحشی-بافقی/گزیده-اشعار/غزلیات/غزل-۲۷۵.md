---
title: >-
    غزل ۲۷۵
---
# غزل ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>این بس که تماشایی بستان تو باشم</p></div>
<div class="m2"><p>مرغ سر دیوار گلستان تو باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کافیست همین بهره‌ام از مائدهٔ وصل</p></div>
<div class="m2"><p>کز دور مگس ران سر خوان تو باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این منصب من بس که چو رخش تو شود زین</p></div>
<div class="m2"><p>جاروب کش عرصهٔ جولان تو باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهم که شود دست سراپای وجودم</p></div>
<div class="m2"><p>در شغل عنان گیری یکران تو باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بزمگه یوسف اگر ره دهدم بخت</p></div>
<div class="m2"><p>درآرزوی گوشهٔ زندان تو باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در تشنگیم طالع بد جان به لب آرد</p></div>
<div class="m2"><p>گر خود به سر چشمهٔ حیوان تو باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من وحشیم و نغمه سرای چمن حسن</p></div>
<div class="m2"><p>معذورم اگر مرغ غزلخوان تو باشم</p></div></div>