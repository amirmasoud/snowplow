---
title: >-
    غزل ۱۸
---
# غزل ۱۸

<div class="b" id="bn1"><div class="m1"><p>ننموده استخوان ز تن ناتوان مرا</p></div>
<div class="m2"><p>پیدا شده فتیلهٔ زخم نهان مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا زد به نام من غم او قرعهٔ جنون</p></div>
<div class="m2"><p>شد پاره پاره قرعه صفت استخوان مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمری به سر سبوی حریفان کشیده‌ام</p></div>
<div class="m2"><p>هرگز ندیده است کسی سرگران مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از یک نفس برآر ز من دود شمعسان</p></div>
<div class="m2"><p>نبود اگر به بزم تو ، بند زبان مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشی ببین که یار به عشرت سرا نشست</p></div>
<div class="m2"><p>بیرون در گذاشت به حال سگان مرا</p></div></div>