---
title: >-
    غزل ۱۳۸
---
# غزل ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>امروز ناز را به نیازم نظر نبود</p></div>
<div class="m2"><p>زان شیوه‌های خاص یکی جلوه‌گر نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم از غرور اگر چه نمی‌گشت ملتفت</p></div>
<div class="m2"><p>عجز نگاه حسرت من بی اثر نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس شیوه‌های ناز که در پرده داشت حسن</p></div>
<div class="m2"><p>اما تبسمی که شود پرده در نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن خنده‌ها که غنچهٔ سیراب می‌نهفت</p></div>
<div class="m2"><p>بیرون ز زیر پردهٔ گلبرگ تر نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من کشته کرشمه مژگان که بر جگر</p></div>
<div class="m2"><p>خنجر زد آنچنان که نگه را خبر نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل را که نومقید زندان حسرت است</p></div>
<div class="m2"><p>جز عرض عشق هیچ گناه دگر نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وحشی نگفتمت که غرور آورد نیاز</p></div>
<div class="m2"><p>این سرکشی و ناز چرا بیشتر نبود</p></div></div>