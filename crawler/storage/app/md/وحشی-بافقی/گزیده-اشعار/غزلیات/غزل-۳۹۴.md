---
title: >-
    غزل ۳۹۴
---
# غزل ۳۹۴

<div class="b" id="bn1"><div class="m1"><p>از برای خاطر اغیار خوارم می‌کنی</p></div>
<div class="m2"><p>من چه کردم کاینچنین بی‌اعتبارم می‌کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزگاری آنچه با من کرد استغنای تو</p></div>
<div class="m2"><p>گر بگویم گریه‌ها بر روزگارم می‌کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نمی‌آیم به سوی بزمت از شرمندگیست</p></div>
<div class="m2"><p>زانکه هر دم پیش جمعی شرمسارم می‌کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بدانی حال من گریان شوی بی‌اختیار</p></div>
<div class="m2"><p>ای که منع گریه بی‌اختیارم می‌کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفته‌ای تدبیر کارت می‌کنم وحشی منال</p></div>
<div class="m2"><p>رفت کار از دست کی تدبیر کارم می‌کنی</p></div></div>