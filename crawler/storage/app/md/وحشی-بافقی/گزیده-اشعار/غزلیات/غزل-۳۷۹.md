---
title: >-
    غزل ۳۷۹
---
# غزل ۳۷۹

<div class="b" id="bn1"><div class="m1"><p>چه فروشدی به کلفت چه شدت چه حال داری</p></div>
<div class="m2"><p>برو و بکش دو جامی که بسی ملال داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل تست فارغ از غم که شراب عیش خوردی</p></div>
<div class="m2"><p>تو به عیش کوش و مستی که فراغ بال داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو نشسته در مقابل من و صد خیال باطل</p></div>
<div class="m2"><p>که به عالم تخیل به که اتصال داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کدام علم یارب به دل تو اندر آیم</p></div>
<div class="m2"><p>که ببینم و بدانم که چه در خیال داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ترشح عنایت غم باز مانده‌ای خور</p></div>
<div class="m2"><p>تو که کاروان جانها به لب زلال داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه خوش است از تو وحشی ز شراب عشق مستی</p></div>
<div class="m2"><p>که نه خستهٔ فراقی نه غم وصال داری</p></div></div>