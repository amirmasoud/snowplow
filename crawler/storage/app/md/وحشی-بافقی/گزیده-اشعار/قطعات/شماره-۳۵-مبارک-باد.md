---
title: >-
    شمارهٔ ۳۵ - مبارک باد
---
# شمارهٔ ۳۵ - مبارک باد

<div class="b" id="bn1"><div class="m1"><p>مبارک باد می‌گویند شه را</p></div>
<div class="m2"><p>جهانی بسته صف در خدمت او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ولیکن من بعکس جمله هستم</p></div>
<div class="m2"><p>مبارکباد گوی خلعت او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرا زان رو که خلعت شد مشرف</p></div>
<div class="m2"><p>به تشریف قبول حضرت او</p></div></div>