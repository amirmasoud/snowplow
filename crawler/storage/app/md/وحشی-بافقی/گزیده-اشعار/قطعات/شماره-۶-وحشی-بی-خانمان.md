---
title: >-
    شمارهٔ ۶ - وحشی بی‌خانمان
---
# شمارهٔ ۶ - وحشی بی‌خانمان

<div class="b" id="bn1"><div class="m1"><p>ای پیش همت تو متاع سرای دهر</p></div>
<div class="m2"><p>بی قدرتر از آنکه توان رایگان فروخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جایی که کمترین نفرت بار خود گشود</p></div>
<div class="m2"><p>یک جنس خود به مایهٔ سد بحر و کان فروخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هندوی تو گهی که برون آمد از حجاز</p></div>
<div class="m2"><p>از بهر عشر حاصل هندوستان فروخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آگه نیی که از پی وجه معاش خویش</p></div>
<div class="m2"><p>هر چیز داشت وحشی بی خانمان فروخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چیزی که از بلاد عراق آمدش به دست</p></div>
<div class="m2"><p>آورد و در دیار جرون در زمان فروخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بهر وجه آب وضو اندر این دیار</p></div>
<div class="m2"><p>سجاده کرد در گرو و طیلسان فروخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دارد کنون فروختنی آبروی و بس</p></div>
<div class="m2"><p>وان جنس نیست اینکه به هر کس توان فروخت</p></div></div>