---
title: >-
    شمارهٔ ۳۰ - هجو شما می‌کنم
---
# شمارهٔ ۳۰ - هجو شما می‌کنم

<div class="b" id="bn1"><div class="m1"><p>به ما خواجه تا چند خواهید گفت</p></div>
<div class="m2"><p>که قرض شما را ادا می‌کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ادای دگر گر چنین می‌کنید</p></div>
<div class="m2"><p>به رخصت که هجو شما می‌کنم</p></div></div>