---
title: >-
    شمارهٔ ۲۴ - به مفت نیز نیرزد
---
# شمارهٔ ۲۴ - به مفت نیز نیرزد

<div class="b" id="bn1"><div class="m1"><p>زری که می‌طلبم دوش لطف فرمودی</p></div>
<div class="m2"><p>ز من کسی نستاند به سد هزار نیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مفت نیز نیرزد و گرنه هم خود گوی</p></div>
<div class="m2"><p>که من چرا زر مفتی چنین دهم به تو باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هزل دست به دستش برند و اندازند</p></div>
<div class="m2"><p>به جان رسیدم از این دست بر دو دست انداز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زریست لایق همیان و کیسهٔ تاجر</p></div>
<div class="m2"><p>چرا که خرج نگردد به سالهای دراز</p></div></div>