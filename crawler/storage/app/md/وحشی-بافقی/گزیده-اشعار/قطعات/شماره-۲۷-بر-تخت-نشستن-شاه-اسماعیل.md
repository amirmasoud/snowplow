---
title: >-
    شمارهٔ ۲۷ - بر تخت نشستن شاه اسماعیل
---
# شمارهٔ ۲۷ - بر تخت نشستن شاه اسماعیل

<div class="b" id="bn1"><div class="m1"><p>شاه تهماسب خسرو عادل</p></div>
<div class="m2"><p>که ز شاهان کسش ندیده عدیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داد انصاف و عدل داد الحق</p></div>
<div class="m2"><p>تا قیامت گذاشت ذکر جمیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پسر داد نوبت شاهی</p></div>
<div class="m2"><p>زد به آهنگ خلد طبل رحیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوبت او گذشت و شد تاریخ :</p></div>
<div class="m2"><p>نوبت داد شاه اسمعیل</p></div></div>