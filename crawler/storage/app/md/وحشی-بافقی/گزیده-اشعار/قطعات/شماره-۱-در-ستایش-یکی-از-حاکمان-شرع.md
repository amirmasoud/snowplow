---
title: >-
    شمارهٔ ۱ - در ستایش یکی از حاکمان شرع
---
# شمارهٔ ۱ - در ستایش یکی از حاکمان شرع

<div class="b" id="bn1"><div class="m1"><p>ای داده سپهر شرع را نور</p></div>
<div class="m2"><p>از پرتو رأی عالم آرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناهید ز مطربی کشد دست</p></div>
<div class="m2"><p>گر نهی تو بر فلک نهد پا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دست تو کلک معجز آثار</p></div>
<div class="m2"><p>هم خاصیت عصای موسا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دمساز کلام جان فزایت</p></div>
<div class="m2"><p>با معجزه دم مسیحا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تقویت شریعت تو</p></div>
<div class="m2"><p>متقن همه جا بنای تقوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از حکم توچرخ کی کشد سر</p></div>
<div class="m2"><p>او راست مگر دو سر چو جوزا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تهمت نقص و وصمت عیب</p></div>
<div class="m2"><p>حکم تو چو ذات تو مبرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از نسبت پستی و تنزل</p></div>
<div class="m2"><p>طبع تو چو قدر تو معرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ضابطه مسائل نحو</p></div>
<div class="m2"><p>آن نظم که کرده طبعت انشا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کس در عرب و عجم نظیرش</p></div>
<div class="m2"><p>نشنیده به هیچ نحو از انحا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا نظم ترا ز بر کند چرخ</p></div>
<div class="m2"><p>برداشته سبحه ثریا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>افتاده مرا قضیه‌ای چند</p></div>
<div class="m2"><p>اندوه نتیجهٔ قضایا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دردست فقیر کم بضاعت</p></div>
<div class="m2"><p>بود اندکی از متاع دنیا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنرا به مکاریی سپردم</p></div>
<div class="m2"><p>او رفته کنون به راه عقبا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صادق نفسان گواه حالند</p></div>
<div class="m2"><p>در صدق چو صبح بلکه افزا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مگذار که این متاع بی‌قدر</p></div>
<div class="m2"><p>تاراج شود چو خوان یغما</p></div></div>