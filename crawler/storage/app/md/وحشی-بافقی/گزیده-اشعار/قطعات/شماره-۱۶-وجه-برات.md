---
title: >-
    شمارهٔ ۱۶ - وجه برات
---
# شمارهٔ ۱۶ - وجه برات

<div class="b" id="bn1"><div class="m1"><p>خواجه وجه برات خود بدهد</p></div>
<div class="m2"><p>تا مرا گفتگو نباید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا زرم را به کس حواله کند</p></div>
<div class="m2"><p>تا مرا هجو او نباید کرد</p></div></div>