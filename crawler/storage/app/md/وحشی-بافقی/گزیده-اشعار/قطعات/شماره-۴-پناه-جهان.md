---
title: >-
    شمارهٔ ۴ - پناه جهان
---
# شمارهٔ ۴ - پناه جهان

<div class="b" id="bn1"><div class="m1"><p>زهی پایه چتر اقبال تو</p></div>
<div class="m2"><p>ز فرط بلندی برون از جهات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پناه جهان قطب گردون مکان</p></div>
<div class="m2"><p>وجود تو مستظهر کاینات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گرد تو گردند نیک اختران</p></div>
<div class="m2"><p>چو بر گرد قطب شمالی بنات</p></div></div>