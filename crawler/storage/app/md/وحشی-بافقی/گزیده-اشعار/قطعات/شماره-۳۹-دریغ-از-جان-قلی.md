---
title: >-
    شمارهٔ ۳۹ - دریغ از جان قلی
---
# شمارهٔ ۳۹ - دریغ از جان قلی

<div class="b" id="bn1"><div class="m1"><p>دریغ از جان قلی کز جور گردون</p></div>
<div class="m2"><p>کناری پر ز خون رفت از میانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمانه دشنهٔ جورش چنان زد</p></div>
<div class="m2"><p>که نوک دشنه در دل کرد خانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طلب کردم چو تاریخش خرد گفت :</p></div>
<div class="m2"><p>شهید دشنهٔ جور زمانه</p></div></div>