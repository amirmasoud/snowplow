---
title: >-
    شمارهٔ ۲۲ - در خیمهٔ سوداگردان
---
# شمارهٔ ۲۲ - در خیمهٔ سوداگردان

<div class="b" id="bn1"><div class="m1"><p>درون خیمه سوداگران نیست</p></div>
<div class="m2"><p>ز جنس خوردنی جز کرس در کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تیر خیمه دایم چشمشان باز</p></div>
<div class="m2"><p>که هست از نان کماج آن نمودار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود بر بار دایم دیگشان لیک</p></div>
<div class="m2"><p>بر آن باری که باشد بر شتر بار</p></div></div>