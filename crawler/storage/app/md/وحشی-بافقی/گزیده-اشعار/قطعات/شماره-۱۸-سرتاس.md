---
title: >-
    شمارهٔ ۱۸ - سرتاس
---
# شمارهٔ ۱۸ - سرتاس

<div class="b" id="bn1"><div class="m1"><p>ای که هر خلعتی که در بر توست</p></div>
<div class="m2"><p>زینت دوش آسمان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جسمش از جامه تو پوشیده‌ست</p></div>
<div class="m2"><p>هر که در حیز مکان باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلعت خاصه کز شرافت آن</p></div>
<div class="m2"><p>شرفم برهمه جهان باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشته شاعر، بلی شود شاعر</p></div>
<div class="m2"><p>هر که همدوش شاعران باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه او گفته بنده می‌خواند</p></div>
<div class="m2"><p>زانکه خود سخت بی‌زبان باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته : ای درفشان گوهر بخش</p></div>
<div class="m2"><p>که کفت رشک بحر و کان باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر درت اطلس فلک پوشد</p></div>
<div class="m2"><p>آنکه او خاک آستان باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلعت خاصه کز شرافت آن</p></div>
<div class="m2"><p>دعویم بر همه عیان باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌پسندی که جامه چون من</p></div>
<div class="m2"><p>در بر مردکی چنان باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کش نه کفش و نه چاقشور بود</p></div>
<div class="m2"><p>نه کمربند در میان باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باشد او را همین سرتاسی</p></div>
<div class="m2"><p>نه سری هم که مو بر آن باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فوطه‌ای چون فتیله مشعل</p></div>
<div class="m2"><p>آن سر کل در آن نهان باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مصلحت چیست من به او چه کنم</p></div>
<div class="m2"><p>هر چه امر خدایگان باشد</p></div></div>