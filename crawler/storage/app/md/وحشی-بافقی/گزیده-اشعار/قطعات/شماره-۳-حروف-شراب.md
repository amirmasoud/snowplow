---
title: >-
    شمارهٔ ۳ - حروف شراب
---
# شمارهٔ ۳ - حروف شراب

<div class="b" id="bn1"><div class="m1"><p>بر درخانه قدح نوشی</p></div>
<div class="m2"><p>رفتم و کردم التماس شراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیشه‌ای لطف کرد، اما بود</p></div>
<div class="m2"><p>چون حروف شراب ، نیمی آب</p></div></div>