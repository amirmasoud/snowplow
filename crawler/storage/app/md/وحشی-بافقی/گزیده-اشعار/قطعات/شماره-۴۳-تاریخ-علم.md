---
title: >-
    شمارهٔ ۴۳ - تاریخ علم
---
# شمارهٔ ۴۳ - تاریخ علم

<div class="b" id="bn1"><div class="m1"><p>زیب عالم علم شاه خلیل الله است</p></div>
<div class="m2"><p>که سر قدر رسانیده ز مه تا ماهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علمی ساخته الحق که چو گردید بلند</p></div>
<div class="m2"><p>دست اندیشه‌اش از ذیل کند کوتاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علم پایه بلندی که در او شقه چرخ</p></div>
<div class="m2"><p>چون شود راست به زیر فلک خرگاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهجهٔ نور فشانش چو کند جلوه گری</p></div>
<div class="m2"><p>رنگ خورشید کند رشک فروغش کاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در گواهند دو مصرع که رقم گشته به ذیل</p></div>
<div class="m2"><p>هر یکی داده ز تاریخ علم آگاهی :</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جای عزت طلبان داعیه جان داران</p></div>
<div class="m2"><p>باد پای علم عز خلیل اللاهی</p></div></div>