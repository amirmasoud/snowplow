---
title: >-
    قصیدهٔ شمارهٔ ۲۰ - در ستایش میرمیران
---
# قصیدهٔ شمارهٔ ۲۰ - در ستایش میرمیران

<div class="b" id="bn1"><div class="m1"><p>ای برسر سپهر برین برده ترکتاز</p></div>
<div class="m2"><p>خورشید بر سمند بلند تو طبل باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دادند بهر لعل زر نقره خنگ تو</p></div>
<div class="m2"><p>در کورهٔ سپهر زر مهر را گداز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دولت بود متابع بخت جوان تو</p></div>
<div class="m2"><p>محمود را گزیر کجا باشد از ایاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوته شود فسانه دور و دراز خصم</p></div>
<div class="m2"><p>در عرصه‌ای که تیغ تو گردد زبان دراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در پا فکند کبک به جنب حمایتت</p></div>
<div class="m2"><p>خلخال دار حلقهٔ زرین چشم باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ماه نو قضا پی محمل کشیدنت</p></div>
<div class="m2"><p>هر ماه بر جمازه گردون نهد جهاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خاطرت که پرده در نار موسویست</p></div>
<div class="m2"><p>می‌خواست شمع لاف زند لب گزید گاز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مانند نرگس آنکه بود با تو سرگران</p></div>
<div class="m2"><p>دست زمانه برکندش پوست چون پیاز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دندان زنی به کسر وقار تو زد عدو</p></div>
<div class="m2"><p>لیک ایمنست کوه ز مقراضه گراز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد سر فکنده دشمن جاهت که کس ندید</p></div>
<div class="m2"><p>پیش عقاب دعوی گردنکشی ز غاز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اول اگر ز تیغ تو شد سرفکنده خصم</p></div>
<div class="m2"><p>آخر ولی سنان تواش کرد سرفراز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جای مخالف تو دهد جان که هیچکس</p></div>
<div class="m2"><p>نبود به غیر زاغ که بر وی کند نماز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا واهب عطای تو ننهاد خوان جود</p></div>
<div class="m2"><p>از روی حرص سیر نگردید چشم آز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شادی کمینه خادم عشرت سرای تست</p></div>
<div class="m2"><p>ناشاد آنکه بر رخ او در کنی فراز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زیبد که چون صدف دهنش پر گهر کنی</p></div>
<div class="m2"><p>وحشی که لب به ذکر عطای تو کرد باز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دادم طراز کسوت معنی ز نام تو</p></div>
<div class="m2"><p>طرز کلام بنگر و طبع سخن طراز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا مقتضای عشق چنین است کورند</p></div>
<div class="m2"><p>عشاق در برابر ناز بتان نیاز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بادا نیازمند جنابت عروس بخت</p></div>
<div class="m2"><p>چندان که میل طبع جوانان بود به ناز</p></div></div>