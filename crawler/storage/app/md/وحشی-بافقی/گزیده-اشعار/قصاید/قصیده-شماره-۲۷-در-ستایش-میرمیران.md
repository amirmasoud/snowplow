---
title: >-
    قصیدهٔ شمارهٔ ۲۷ - در ستایش میرمیران
---
# قصیدهٔ شمارهٔ ۲۷ - در ستایش میرمیران

<div class="b" id="bn1"><div class="m1"><p>بر کسانی که ببینند به روی تو هلال</p></div>
<div class="m2"><p>عید باشد همه روز و همه ماه وهمه سال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میرمیران که بود طلعت فرخندهٔ او</p></div>
<div class="m2"><p>صبح عیدی که شدآفاق از او فرخ فال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به اندازهٔ قدر تو و صدر تو زیند</p></div>
<div class="m2"><p>کس در ایوان تو برنگذرد از صف نعال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسکه انصاف تو برتافته سرپنجهٔ ظلم</p></div>
<div class="m2"><p>عبث محض نمایند پلنگان چنگال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قهرت آنجا که کند زلزله تفرقه عام</p></div>
<div class="m2"><p>حفظ جمعیت اجزا نکند طبع جبال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عزمت آنجا که شده در مدد ناصیه صلب</p></div>
<div class="m2"><p>ریشه در آهن و فولاد فرو برده نهال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌شود کور حسود تو و درمانش نیست</p></div>
<div class="m2"><p>که مصون است کمال تو ز آسیب زوال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دایم از نیر تابنده به سمت الرأس است</p></div>
<div class="m2"><p>گو به سوراخ نشین شب پره ، کوته کن بال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرنه هم لطف تو باشد سپر جان عدو</p></div>
<div class="m2"><p>سایه با تیغ رود خصم ترا در دنبال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مور از تشت برون آید و این ممکن نیست</p></div>
<div class="m2"><p>کاختر تیرهٔ خصمت بدر آید زو بال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیده بخت بداندیش تو از گردش چرخ</p></div>
<div class="m2"><p>چون ببیند رخ مقصود که امریست محال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چارهٔ باصرهٔ اعمی فطری چه کند</p></div>
<div class="m2"><p>گر چه در صنعت خود موی شکافد کحال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر به خون ریختن خصم تو فتوا طلبند</p></div>
<div class="m2"><p>خونش آواز برآرد که حلال است حلال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فلک ثابت از آنسوی زمان تازد رخش</p></div>
<div class="m2"><p>از سمند تو اگر کسب کند استعجال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رایت ار سرمه کش دیدهٔ اندیشه شود</p></div>
<div class="m2"><p>در شب تار توان دید پی پای خیال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صیت آسایش عدل تو برانگیزدشان</p></div>
<div class="m2"><p>کز مضیق رحم آیند سوی مهد اطفال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دست انصاف تو آن کرد که در پای حمام</p></div>
<div class="m2"><p>حلقهٔ دیدهٔ باز است چو زرین خلخال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر کند خصم تو در آینه آن روی کریه</p></div>
<div class="m2"><p>از رخش در پس آیینه گریزد تمثال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جودت از بلعجبیها شده مغناطیسی</p></div>
<div class="m2"><p>که کشد جذبه‌اش از کام و زبان حرف سال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هیچ حرف طمع از دل به سوی لب نشتاف</p></div>
<div class="m2"><p>کش سد آری و بلی از تو نکرد استقبال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>داورا از مدد فیض و ثنای تو مرا</p></div>
<div class="m2"><p>خاطری هست چو بحری ز گهر مالامال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نرسد جز تو به کس گوهری از خاطر من</p></div>
<div class="m2"><p>کرده‌ام وقف تو این بحر لبالب ز زلال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>معدن طبع مرا کرد پر از جوهر خاص</p></div>
<div class="m2"><p>پرتو تربیت عام تو خورشید مثال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>این جواهر نه متاعیست که هر جا یابند</p></div>
<div class="m2"><p>همه دانند که نادر بود این طرز مقال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سخن من نه ز جنس سخن مدعی است</p></div>
<div class="m2"><p>که بود بر سر کو سد سد ازین سنگ و سفال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وحشی اینجا چو رسیدی به همین قطع نمای</p></div>
<div class="m2"><p>که چو ممدوح تو تمییز کند نقص و کمال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا مقرر بود این وضع به تاریخ عرب</p></div>
<div class="m2"><p>که بود عید صیام اول ماه شوال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر تو ای قبلهٔ احرار عرب تا به عجم</p></div>
<div class="m2"><p>عید باشد همه روز و همه ماه و همه سال</p></div></div>