---
title: >-
    قصیدهٔ شمارهٔ ۱۳ - در ستایش میرمیران
---
# قصیدهٔ شمارهٔ ۱۳ - در ستایش میرمیران

<div class="b" id="bn1"><div class="m1"><p>باد فرخنده عید و فصل بهار</p></div>
<div class="m2"><p>بر تو و شاهزاده‌های کبار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میر میران که روی خرم تست</p></div>
<div class="m2"><p>عید احرار و قبلهٔ ابرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر یمین و یسار تو چو روند</p></div>
<div class="m2"><p>آن دو شهزادهٔ فلک مقدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اله اله چه رشکها که برند</p></div>
<div class="m2"><p>بر هم وقدر هم یمین و یسار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ترا آسمان جنیبت کش</p></div>
<div class="m2"><p>وی ترا آفتاب غاشیه دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوه را همچو برق سرعت داد</p></div>
<div class="m2"><p>هر کجا عزم تو نمود گذار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برق را همچو کوه ساکن ساخت</p></div>
<div class="m2"><p>هر کجا حلم تو گرفت قرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مور با حفظ تو برون آید</p></div>
<div class="m2"><p>از ته پای پیل بی آزار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خصم بیهوده گردگو می‌کرد</p></div>
<div class="m2"><p>گرد بازار نکبت و ادبار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه متاعی‌ست دولت و اقبال</p></div>
<div class="m2"><p>که فروشند بر سر بازار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باز بر نسر طایر اندازند</p></div>
<div class="m2"><p>بازداران تو ، به روز شکار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر فلک نسر طایر ایمن نیست</p></div>
<div class="m2"><p>کبک خود چیست و بر سر کهسار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر به دیوار بر کشد به مثل</p></div>
<div class="m2"><p>نقش خصم تو کلک نقش نگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تن رود سرنگون که کوته چاه</p></div>
<div class="m2"><p>سر رود مضطرب که کو سردار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بد سگالت که مرد وخاکش خورد</p></div>
<div class="m2"><p>بلکه از خاک او نماند غبار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لحدش دیدمی به خواب که بود</p></div>
<div class="m2"><p>همچو سوراخ مار تیره و تار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیکری اندر او ز دود جحیم</p></div>
<div class="m2"><p>پای تا سر سیاه گشته چو قار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دل پر زنگ کینه گر سوده</p></div>
<div class="m2"><p>مانده یک کف سیاهی زنگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چشم در چشمخانه خاک شده</p></div>
<div class="m2"><p>مانده یک مشت نشتر و مسمار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قدرتت چون زبون نواز شده</p></div>
<div class="m2"><p>صولتت چون رود به دفع مضار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عجز بگریزد از جبلت مور</p></div>
<div class="m2"><p>زهر بگریزد از طبیعت مار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در کف استقامت رایت</p></div>
<div class="m2"><p>جز خط راست ناید از پر گار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آب حزمت گرش به روی زنند</p></div>
<div class="m2"><p>جهد از خواب صورت دیوار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>داورا دادگسترا شاها</p></div>
<div class="m2"><p>ای جهان را به ذاتت استظهار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>واجب العرض خود به خدمت تو</p></div>
<div class="m2"><p>گر اجازت بود کنم اظهار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به خدایی که لطف او بخشد</p></div>
<div class="m2"><p>سد گنه را به نیم استغفار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از خطایی چو کفر سجده بت</p></div>
<div class="m2"><p>بگذرد عفو او به یک اقرار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رقمی پیش طاق وحدت او</p></div>
<div class="m2"><p>لیس فی الدار غیره دیار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آنکه نسبت به بی نیازی او</p></div>
<div class="m2"><p>هست یکسان چه یار و چه اغیار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وانکه محتاج اوست هر کس هست</p></div>
<div class="m2"><p>خواه بدکار و خواه نیکوکار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن کس اول ز چشم تو فکند</p></div>
<div class="m2"><p>هر کرا پیش خلق خواهد خوار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وانکه آخر کند غلام تواش</p></div>
<div class="m2"><p>هر کرا آفرید دولتیار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که به دارالعبادهٔ تکلیف</p></div>
<div class="m2"><p>مدتی قبل از آن که یابم بار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دم ازین خاندان زدم چون کرد</p></div>
<div class="m2"><p>اقتضای طبیعتم مختار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>این کشش ذاتی است و هر ذاتی</p></div>
<div class="m2"><p>هست تا هست ذات را آثار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در میان عقیدهٔ من و غیر</p></div>
<div class="m2"><p>هست شاها تفاوت بسیار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>من نمی‌خواهم از تو غیر از تو</p></div>
<div class="m2"><p>او نمی‌خواهد از تو جز دینار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همت هر کس از تو چیزی خواست</p></div>
<div class="m2"><p>غیر دینار جست و ما دیدار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>من سگ این درم اگر دگران</p></div>
<div class="m2"><p>خادم این درند وخدمتکار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به خدا کز پی گدایی نیست</p></div>
<div class="m2"><p>اینکه مدح تو می‌کنم تکرار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از در مدح و زیور نامت</p></div>
<div class="m2"><p>می‌دهم زیب و زینت اشعار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چون بگویم گدا نیم ، هستم</p></div>
<div class="m2"><p>شاعران را گدایی است شعار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هنر من گدایی است و مرا</p></div>
<div class="m2"><p>از گدایی چگونه باشد عار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خاصه زینسان گداییی که گدا</p></div>
<div class="m2"><p>زان شود صاحب ضیاع و عقار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از چه کس از کسی که گوید چرخ</p></div>
<div class="m2"><p>که مرا هم گدای خویش شمار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آنقدر گویم ای که دست و دلت</p></div>
<div class="m2"><p>مایه بخش معادن است و بحار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که گدای توام نه از همه کس</p></div>
<div class="m2"><p>همه کس داند از صغار و کبار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>فرقهٔ خود پسند کس مپسند</p></div>
<div class="m2"><p>همگی عجب و جملگی پندار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از پی جر و اخذ سر تا پای</p></div>
<div class="m2"><p>همه دست و زبان چو بید و چنار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آنچنان فرقه زیاده طلب</p></div>
<div class="m2"><p>که طلب می‌کنند پنج از چار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چه عجب گر ز بیم طامعه شان</p></div>
<div class="m2"><p>کور بنهد عصا و کل دستار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گر ز ابرامشان سخن راند</p></div>
<div class="m2"><p>قابض روح بر سر بیمار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خوش بمیرند خستگان آسان</p></div>
<div class="m2"><p>ندهد هیچ خسته جان دشوار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شکرلله کزین گروه نیم</p></div>
<div class="m2"><p>من و شکر و زبان شکر گزار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شکر کز نقد کنز لایفنی</p></div>
<div class="m2"><p>همتم پر نمود جیب و کنار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>وحشی این شکر و این شکایت چیست</p></div>
<div class="m2"><p>تا کی و چند طی کن این تومار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>در دعای دوام دولت شاه</p></div>
<div class="m2"><p>دست عجز و کف نیاز برآر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تا جهان را بهار و عیدی هست</p></div>
<div class="m2"><p>در جهان باشی ای جهان وقار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>که جهان از رخ خجستهٔ تست</p></div>
<div class="m2"><p>خرم و خوش چو عید و فصل بهار</p></div></div>