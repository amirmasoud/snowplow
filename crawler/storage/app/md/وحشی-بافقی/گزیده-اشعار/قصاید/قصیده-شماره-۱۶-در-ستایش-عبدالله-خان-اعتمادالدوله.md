---
title: >-
    قصیدهٔ شمارهٔ ۱۶ - در ستایش عبدالله خان اعتمادالدوله
---
# قصیدهٔ شمارهٔ ۱۶ - در ستایش عبدالله خان اعتمادالدوله

<div class="b" id="bn1"><div class="m1"><p>سد زبان خواهم که سازم یک به یک گوهر نثار</p></div>
<div class="m2"><p>در ثنای میرزای کام بخش کامکار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجلس آرای وزارت انجمن پیرای عدل</p></div>
<div class="m2"><p>گوهر دریا کفایت اختر مهر اقتدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بازده گو پشت دولت از وجود او به کوه</p></div>
<div class="m2"><p>اعتمادالدوله آن پشت و پناه روزگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر پسر را کان پدر باشد به استصواب اوست</p></div>
<div class="m2"><p>هر چه گیتی پرورد در تحت امر اختیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پسر گلزار عز کشوری را آب و رنگ</p></div>
<div class="m2"><p>و ز پدر نخل وقار لشکری را برگ و بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیخ کش دولت نشاند بار آرد عزوشان</p></div>
<div class="m2"><p>تخم کش حشمت فشاند بر دهد عز و وقار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گو پسر بر دهر فرمان ده که باز انسان پدر</p></div>
<div class="m2"><p>از صلاحش نیست بیرون شیخ و شاب و شهریار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوهری کز صلب آن دریاست می‌زیبد اگر</p></div>
<div class="m2"><p>زینت افسر کنندش خسروان تاجدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آصف جمجاه عبدالله دریا دل که هست</p></div>
<div class="m2"><p>کان ز طبع او خجل بحر از کف او شرمسار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کشتی اندیشه گر در قلزم قهرش فتد</p></div>
<div class="m2"><p>بشکند جایی که ناید تخته‌ای زان بر کنار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر ضمیر او که مرآت تصاویر قضاست</p></div>
<div class="m2"><p>آنچه در اوهام بالقوه است بالفعل آشکار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حرف خوانان کتاب لطف او را در نظر</p></div>
<div class="m2"><p>نسخه تریاق فاروق است نقش پشت مار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لطف و قهرش سبزه پرور سازد و گوهر گداز</p></div>
<div class="m2"><p>قطره در قعر سقر ، وندر تک دریا شرار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حکم او گر سایه بر کهسار اندازد به فرض</p></div>
<div class="m2"><p>چاهساری آورد پیدا به جای کوهسار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ماند ار گردون به خارستان قهرش بگذرد</p></div>
<div class="m2"><p>پاره‌ای از اطلس او بر سر هر نوک خار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در گشاد و بست با دستش تشبه می‌کنند</p></div>
<div class="m2"><p>گرنه این می‌بود جزر و مد نبودی در بحار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با خطش کز خطهٔ شادیست دارد نسبتی</p></div>
<div class="m2"><p>صبح خرم زانجهت خیزد ز خاک زنگبار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باد اگر رخش سلیمان بود زیر ران اوست</p></div>
<div class="m2"><p>دیو طبعی کافرید از آذرش پروردگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در طلوع مهرش ار با پرتو خور سردهند</p></div>
<div class="m2"><p>پیش از او آید به غرب از شرق تا پای جدار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نقشش از عالم جهد بیرون اگر بر پشت او</p></div>
<div class="m2"><p>مقرعه در دست تمثالی کشد صورت نگار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باد گویی اسب شطرنج است مانده در عری</p></div>
<div class="m2"><p>در بساط بازی آن عرصه گردد راهوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر هوا پویان تواند گشت پیش از نفخ صور</p></div>
<div class="m2"><p>کوه بر فتراک او گر دست سازد استوار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از دو دستش درگه بازی دو ابروی سیاه</p></div>
<div class="m2"><p>بر فراز دیدهٔ خورشید گردد آشکار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قرص مهر و ماه چون آرد به زیر پا و دست</p></div>
<div class="m2"><p>زان دو هاون سرمه کوبد بهر چشم روزگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ور بیفشارد قدم سازد عروس زهره را</p></div>
<div class="m2"><p>زان یکی خلخال سیمین زین یکی زرین سوار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نشکند در زیر پایش از سبک خیزی حباب</p></div>
<div class="m2"><p>گر کند با پیکر چون کوه در دریا گذار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آید از حد مکان بر لامکان زان پیشتر</p></div>
<div class="m2"><p>کز سر زین سایه بر خاک ره افتد از سوار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باید الحق اینچنین عالم نوردی تا بود</p></div>
<div class="m2"><p>لایق ران و رکاب داور گیتی مدار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مایهٔ اکسیر از او گیرند اهل کیمیا</p></div>
<div class="m2"><p>گر به خاک رهگذر بینی به عین اعتبار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای که خاک پای یکران فلک میدان تست</p></div>
<div class="m2"><p>خسرو سیارگان را زیت تاج افتخار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بهر حمل محملت بستن حلال از زر جهاز</p></div>
<div class="m2"><p>این جهان پیما که هستش کهکشان سیمین مهار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وه چه گفتم چون شود محمل کش اجلال تو</p></div>
<div class="m2"><p>ناقه دیرینه سال باز مانده از قطار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دست مظلومان چنان کردی قوی کاهو بره</p></div>
<div class="m2"><p>با بروت شیر بازی می‌کند در مرغزار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مرغزاری را که از آب حمایت پروری</p></div>
<div class="m2"><p>هر غزالی کاندراو گردد شود ضیغم شکار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>با سر سد جا شکسته صرصر آید باز پس</p></div>
<div class="m2"><p>پیش راهش گر کشد حفظ تو سدی از غبار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خواهد از اجرای حکمت سبزی باغ سپهر</p></div>
<div class="m2"><p>از زمین بر آسمان جاری شود سد جویبار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کار فرمای طبیعت را اگر گویی ببند</p></div>
<div class="m2"><p>رخنه‌های فتنه این قلعهٔ نیلی حصار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از پی اجزای گل بر آسمان آرند گرم</p></div>
<div class="m2"><p>جزو خاکی را دخان و جزو آبی را بخار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در خور اوصاف آصف نیست وحشی این مقال</p></div>
<div class="m2"><p>شو به عجز خویش قائل بر دعا کن اختصار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا توان تعریف کردن رأی نیکان را به نور</p></div>
<div class="m2"><p>تا توان تشبیه کردن روی خوبان را به نار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>باد از روی تو نار شمع خاور عاریت</p></div>
<div class="m2"><p>باد از روی تو نور ماه انور مستعار</p></div></div>