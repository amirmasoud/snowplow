---
title: >-
    قصیدهٔ شمارهٔ ۱۵ - در ستایش غیاث الدین محمد میرمیران
---
# قصیدهٔ شمارهٔ ۱۵ - در ستایش غیاث الدین محمد میرمیران

<div class="b" id="bn1"><div class="m1"><p>ای بخت خفته خیز و نشین خوش به اعتبار</p></div>
<div class="m2"><p>زیرا که با تو بر سر لطف آمده‌ست یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان تو خوش بخند که حسرت سر آمده‌ست</p></div>
<div class="m2"><p>آن گریه و دعای سحر کرده است کار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل تورا نوید که پیدا شدش کلید</p></div>
<div class="m2"><p>آن در که بسته بود به روی تو استوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشتی ما که موج غمش داشت در میان</p></div>
<div class="m2"><p>برخاست باد شرطه و افتاد بر کنار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منت خدای را که بدل شد همه به شکر</p></div>
<div class="m2"><p>آن شکوه‌ها که داشتم از وضع روزگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گو مدعی خناق کن از قرب من که هست</p></div>
<div class="m2"><p>رشگ دراز دست و حریف گلو فشار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وقت شکفتگی و گل افشانی من است</p></div>
<div class="m2"><p>خارم همه گل است و خزانم همه بهار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من بلبل ترانه زن باغ دولتم</p></div>
<div class="m2"><p>یعنی که آمده‌ست گل دولتم ببار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست این همه ذخیرهٔ دولت که مینهم</p></div>
<div class="m2"><p>از فیض یک توجه سلطان نامدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ماه بلند کوکبه کوکب احتشام</p></div>
<div class="m2"><p>شاه سپهر مسند خورشید اقتدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یعنی غیاث دین محمد که یافته</p></div>
<div class="m2"><p>نظم دو کون بر لقب نام او قرار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اندر رکاب حشمت و میدان شوکتش</p></div>
<div class="m2"><p>جمشید یک پیاده و خورشید یک سوار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هفت آسمان و چرخ نهم مشتبه شوند</p></div>
<div class="m2"><p>یابند اگر به درگه او فرصت شمار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای رفعت از علاقه قدر تو مرتفع</p></div>
<div class="m2"><p>وی فخر را به نسبت ذات تو افتخار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از ساکنان صف نعالند نه فلک</p></div>
<div class="m2"><p>جایی که همت تو نشیند به صدر بار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ایزد چو کرد تعبیه در چرخ نظم کون</p></div>
<div class="m2"><p>دادش به مقتضای رضای تو اختیار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا رهنمای امر تو تعیین نکرد راه</p></div>
<div class="m2"><p>اجرام را به چرخ معین نشد مدار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از نعل دست و پا سمند تو زهره را</p></div>
<div class="m2"><p>در ساعداست یا ره و در گوش گوشوار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حفظ تو واجب است فلک را که داردت</p></div>
<div class="m2"><p>از سد جهان خلاصه دوران به یادگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنجا که باشد از تف خون تو یک اثر</p></div>
<div class="m2"><p>کوه قوی نهاد به یک تف شود نزار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دریای آتش ار بود از حفظ نام تو</p></div>
<div class="m2"><p>ماهی موم سالم از آنجا کند گذار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر نامیه به نرمی خویت عمل کند</p></div>
<div class="m2"><p>از راه طبع کسوت قاقم دهد به خار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نشو گیاه عمر حسودت ز چشمه ایست</p></div>
<div class="m2"><p>کز رشحه‌ای از آن شده پرورده زهر مار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آبش به نام سینهٔ خصم تو گر دهند</p></div>
<div class="m2"><p>با خنجر کشیده دمد پنجهٔ چنار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از جام بغض هر که فلک گشت سرگران</p></div>
<div class="m2"><p>الا به خون دشمن تو نشکند خمار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تیغیست خصمی تو که بسیار گردنان</p></div>
<div class="m2"><p>خود را بر آن زدند و فتادند خوار و زار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در حملهٔ نخست سپر بایدش فکند</p></div>
<div class="m2"><p>با تیغ گردنی که کند قصد کارزار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با قوت تسلط شاهین عدل تو</p></div>
<div class="m2"><p>سیمرغ را مگس به سهولت کند شکار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کان از زبان تیشه چه آواز برکشید</p></div>
<div class="m2"><p>گر از کف عطای تو نامد به زینهار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در معرض شمارهٔ او گو میا حساب</p></div>
<div class="m2"><p>دست امید بخش تو چون شد وظیفه بار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دریا گهی که موج زند زان قبیل نیست</p></div>
<div class="m2"><p>امواج او که رخنه در او افکند بخار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از بهر ثبت و ضبط ثواب و گناه تو</p></div>
<div class="m2"><p>تا آفریده آن دو ملک آفریدگار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بالا نکرده سر ز رقم کاتب یمین</p></div>
<div class="m2"><p>ناورده دست سوی قلم ضابط یسار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عدل تو حاکمیست که اندر حمایتش</p></div>
<div class="m2"><p>از بس قویست دست ضغیفان این دیار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جایی رسیده کار که در خاک پاک یزد</p></div>
<div class="m2"><p>حد نیست باد را که کند زور بر غبار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شاها توجه تو سخن می‌کند نه من</p></div>
<div class="m2"><p>ورنه من از کجا و زبان سخن گزار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بودم خزف فروش سر چار سوی فکر</p></div>
<div class="m2"><p>پر ساختی دکان من از در شاهوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نظمم اگر چه بود زری سکه‌ای نداشت</p></div>
<div class="m2"><p>از نام نامی تو زری گشت سکه دار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اطناب در سخنی نیست مختصر</p></div>
<div class="m2"><p>وحشی از آن سبب به دعا کرد اختصار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا رخش روزگار نیاید به زیر زین</p></div>
<div class="m2"><p>تا توسن فلک نتوان داشت در جدار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بادا زبون رایض اقبال و جاه تو</p></div>
<div class="m2"><p>همواره توسن فلک و رخش روزگار</p></div></div>