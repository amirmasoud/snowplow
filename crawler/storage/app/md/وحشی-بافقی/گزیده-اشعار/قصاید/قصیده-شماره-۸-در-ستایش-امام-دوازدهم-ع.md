---
title: >-
    قصیدهٔ شمارهٔ ۸ - در ستایش امام دوازدهم «ع»
---
# قصیدهٔ شمارهٔ ۸ - در ستایش امام دوازدهم «ع»

<div class="b" id="bn1"><div class="m1"><p>سپهر قصد من زار ناتوان دارد</p></div>
<div class="m2"><p>که بر میان کمر کین ز کهکشان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جفای چرخ نه امروز می‌رود بر من</p></div>
<div class="m2"><p>به ما عداوت دیرینه در میان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر نه تیر جفا بر کیمنه می‌فکند</p></div>
<div class="m2"><p>چرا سپهر ز قوس قزح کمان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کنج بی‌کسی و غربتم من آن مرغی</p></div>
<div class="m2"><p>که سنگ تفرقه دورش ز آشیان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منم خرابه نشینی که گلخن تابان</p></div>
<div class="m2"><p>به پیش کلبهٔ من حکم بوستان دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منم که سنگ حوادث مدام در دل سخت</p></div>
<div class="m2"><p>به قصد سوختنم آتشی نهان دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی که کرد نظر بر رخ خزانی من</p></div>
<div class="m2"><p>سرشک دمبدم از دیده‌ها روان دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه سازم آه که از بخت واژگونه من</p></div>
<div class="m2"><p>بعکس گشت خواصی که زعفران دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلا اگر طلبی سایهٔ همای شرف</p></div>
<div class="m2"><p>مشو ملول گرت چرخ ناتوان دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز ضعف خویش برآ خوش از آن جهت که همای</p></div>
<div class="m2"><p>ز هر چه هست توجه به استخوان دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرت دهد به مثل زال چرخ گردهٔ مهر</p></div>
<div class="m2"><p>چو سگ بر آن ندوی کان ترا زیان دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدوز دیده ز مکرش که ریزهٔ سوزن</p></div>
<div class="m2"><p>پی هلاک تو اندر میان نان دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کسی ز معرکه‌ها سرخ رو برون آید</p></div>
<div class="m2"><p>که سینه صاف چو تیغ است و یک زبان دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو کلک تیره نهادی که می‌شود دو زبان</p></div>
<div class="m2"><p>همیشه روسیهی پیش مردمان دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز دستبرد اراذل مدام دربند است</p></div>
<div class="m2"><p>چو زر کسی که دل خلق شادمان دارد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کسی که مار صفت در طریق آزار است</p></div>
<div class="m2"><p>مدام بر سر گنج طرب مکان دارد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خود آن که پشت بر اهل زمانه کرد چو ما</p></div>
<div class="m2"><p>رخ طلب به ره صاحب الزمان دارد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شه سریر ولایت محمد بن حسن</p></div>
<div class="m2"><p>که حکم بر سر ابنای انس و جان دارد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کفش که طعنه به لطف و سخای بحر زند</p></div>
<div class="m2"><p>دلش که خنده به جود و عطای کان دارد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به یک گدای فرومایه صرف می‌سازد</p></div>
<div class="m2"><p>به یک فقیر تهی کیسه در میان دارد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زری که صیرفی کان به درج کوه نهاد</p></div>
<div class="m2"><p>دری که گوهری بحر در دکان دارد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دهان کان زر اندود بازمانده چرا</p></div>
<div class="m2"><p>اگر نه حیرت از آن دست زرفشان دارد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر نه دامن چترش پناه مهر شود</p></div>
<div class="m2"><p>ز باد فتنه چراغش که در امان دارد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به راه او شکفد غنچهٔ تمنایش</p></div>
<div class="m2"><p>هوای باغ جنان آن که در جهان دارد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لباس عمر عدو را ز مهجهٔ علمش</p></div>
<div class="m2"><p>نتیجه‌ایست که از نور مه کتان دارد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تویی که رخش ترا از برای پای انداز</p></div>
<div class="m2"><p>زمانه اطلس نه توی آسمان دارد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برون خرام که بهر سواری تو مسیح</p></div>
<div class="m2"><p>سمند گرم رو مهر را عنان دارد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نهال جاه ترا آب تا دهد کیوان</p></div>
<div class="m2"><p>ز چرخ و کاهکشان دلو و ریسمان دارد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به دهر راست روی سرفراز گشته که او</p></div>
<div class="m2"><p>سری به خون عدوی تو چون سنان دارد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بود گشایش کار جهان به پهلویش</p></div>
<div class="m2"><p>ترا کسی که چو در سر بر آستان دارد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کلید حب تو بهر گشاد کارش بس</p></div>
<div class="m2"><p>کسی که آرزوی روضهٔ جنان دارد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز نور رأی تو و آفتاب مادر دهر</p></div>
<div class="m2"><p>به مهد دهر دو فرزند توأمان دارد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>رسید عدل تو جائی که زیر گنبد چرخ</p></div>
<div class="m2"><p>کبوتر از پر شهباز سایبان دارد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اگر اشاره نمایی به گرگ نیست غریب</p></div>
<div class="m2"><p>که پاس گله به سد خوبی شبان دارد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شها ز گردش دوران شکایتیست مرا</p></div>
<div class="m2"><p>که گر ز جا بردم اشک جای آن دارد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز واژگونی این بخت خویش حیرانم</p></div>
<div class="m2"><p>که هر کرا دل من دوستر ز جان دارد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همیشه در پی آزار جان زار من است</p></div>
<div class="m2"><p>به قصد من کمر کینه بر میان دارد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حدیث خود به همین مختصر کنم وحشی</p></div>
<div class="m2"><p>کسی کجا سر تفسیر این بیان دارد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همیشه تا که بود کشتی سپهر که او</p></div>
<div class="m2"><p>ز خاک لنگر و از سدره سایبان دارد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به دهر کشتی عمر مطیع جاهش را</p></div>
<div class="m2"><p>ز موج خیز فنا دور و در امان دارد</p></div></div>