---
title: >-
    قصیدهٔ شمارهٔ ۳۹ - قصیده
---
# قصیدهٔ شمارهٔ ۳۹ - قصیده

<div class="b" id="bn1"><div class="m1"><p>چه در گوش گل گفت باد خزانی</p></div>
<div class="m2"><p>که انداخت از سر کلاه کیانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بالای اشجار از باد دستی</p></div>
<div class="m2"><p>نسیم خزان می‌کند زر فشانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تاراج برگ درختان ز هر سو</p></div>
<div class="m2"><p>کند موذی باد موشک دوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شده برف ظاهر به فرق صنوبر</p></div>
<div class="m2"><p>چو دستار بر تارک مولتانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن چهره شد سرخ برگ رزانرا</p></div>
<div class="m2"><p>که خوردند سیی ز باد خزانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز یخ آب را لوح سیمین به دامن</p></div>
<div class="m2"><p>چو طفلی که دارد سر درس خوانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بلبل نظر کرد کز لشکری دی</p></div>
<div class="m2"><p>گل افتاد از مسند کامرانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کفن کرد از برف بر خود مهیا</p></div>
<div class="m2"><p>که بی او نمی‌خواهم این زندگانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببین گردش دور و طور زمان را</p></div>
<div class="m2"><p>به گردش درآور می ارغوانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می کهنه و نو خطی را طلب کن</p></div>
<div class="m2"><p>که حظ یابی از نوبهار جوانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سبک باش و بردار رطل گران را</p></div>
<div class="m2"><p>که از دل برد بار محنت گرانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به دست آر تا می‌توان جام باده</p></div>
<div class="m2"><p>مده عشرت از دست تا می‌توانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به یاران جانی دمی خو بر آور</p></div>
<div class="m2"><p>که عیشی‌ست خوش بزم یاران جانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خوش آن شیشه کز وی درخشان شود می</p></div>
<div class="m2"><p>چو مینای چرخ و سهیل یمانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که در بزم عشرت به گردش درآری</p></div>
<div class="m2"><p>به کامت شود گردش آسمانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه شادی ازین به که در بزم عشرت</p></div>
<div class="m2"><p>نشینی و ساقی برابر نشانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رسانی دماغ از شراب دمادم</p></div>
<div class="m2"><p>سرود پیاپی به گردون رسانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قدح چون حریفان می‌کش به مجلس</p></div>
<div class="m2"><p>نبندد لب از خنده کامرانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو مستان ز تأثیر آهنگ مطرب</p></div>
<div class="m2"><p>کند چشم مینای می خونچکانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به سازنده دف آورد روی در روی</p></div>
<div class="m2"><p>نوازنده با نی کند همزبانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مقارن به فریاد گردد کمانچه</p></div>
<div class="m2"><p>چو از تیر غم خصم صاحبقرانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چه صاحبقرانی که او را قرینه</p></div>
<div class="m2"><p>نگردیده موجود را دار فانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>علی ولی والی ملک هستی</p></div>
<div class="m2"><p>که دانش بنای جهان راست بانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زحل گر به درگاه قصر رفیعش</p></div>
<div class="m2"><p>نورزد نکو شیوه پاسبانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فلک از شهاب و هلالش کند غل</p></div>
<div class="m2"><p>به شکل غلامان هندوستانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به گلخن وزد گر نسیمی ز لطفش</p></div>
<div class="m2"><p>ز لطف نسیمش کند گلستانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>و گر باد قهرش وزد سوی گلشن</p></div>
<div class="m2"><p>درخت گل آید به آتش فشانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر از عرش اعلا شود زاغ کیوان</p></div>
<div class="m2"><p>ز سد پایه برتر ز عالی مکانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کجا با همای سر بارگاهش</p></div>
<div class="m2"><p>تواند زدن لاف هم آشیانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پر فرق گردنکشان سپاهش</p></div>
<div class="m2"><p>کند خسرو مهر را سایبانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگر زاغ بر بام قصرش نشیند</p></div>
<div class="m2"><p>کند با زحل دعوی توأمانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عجب نبود از بارگاه رفیعش</p></div>
<div class="m2"><p>اگر کهکشانش کند پاسبانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تویی آن گرانمایه در گرامی</p></div>
<div class="m2"><p>که چون جوهر اولت نیست ثانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سمند بلندت به قطع مراحل</p></div>
<div class="m2"><p>کند با کمیت فلک همعنانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در آن دم که گلگون چو برق جهنده</p></div>
<div class="m2"><p>به خون ریز دشمن به میدان جهانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همای ظفر بر سرت گسترد پر</p></div>
<div class="m2"><p>به روی زمین فرش خون گسترانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>غراب از سر شوق گوید به کرکس</p></div>
<div class="m2"><p>که ای بیخبر خیز و ده مژدگانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که روزی شد از دولت دست و تیغش</p></div>
<div class="m2"><p>ترا و مرا نعمت جاودانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در این دشت از جور گرگ حوادث</p></div>
<div class="m2"><p>مطیعش اگر شیوه سازد شبانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اسد را ز گردون مرس کرده چون سگ</p></div>
<div class="m2"><p>شهاب آورد از پی پاسبانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>وگر چرخ زنجیر عدل از مجرد</p></div>
<div class="m2"><p>نبندد به آیین نوشیروانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز میل شهابش برای سیاست</p></div>
<div class="m2"><p>ببینی کنی تیر و هر سو دوانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به کف تیغ رخشنده رخش سبک پی</p></div>
<div class="m2"><p>به میدان کین بر سر خصم رانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نهد از سرای جهان بار بر خر</p></div>
<div class="m2"><p>به آهنگ سر منزل آن جهانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به هر سو نشان ماند از خون ایشان</p></div>
<div class="m2"><p>چو آتش به منزل پس از کاروانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ثریاست یا از شفق مهر گردون</p></div>
<div class="m2"><p>چو آلوده لب از می ارغوانی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چنان سیلیی زد بر او دست پهنت</p></div>
<div class="m2"><p>که از ضرب آن ماند بر وی نشانی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زمین گر به پای سمندت نیفتد</p></div>
<div class="m2"><p>به دستت عدم چون غبارش نشانی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>وگر چرخ اطلس رود بر خلافت</p></div>
<div class="m2"><p>روانی چه کرباسش از هم درانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شها داد از ناکسان زمانه</p></div>
<div class="m2"><p>فغان از خسیسان آخر زمانی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به صوف و سقرلاتشان پشت گرمی</p></div>
<div class="m2"><p>به مردم ز دستارشان سر گرانی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خری چند مایل به جلهای رنگین</p></div>
<div class="m2"><p>ددی چند راغب به آفت رسانی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همه صاحب اسب و استر ولیکن</p></div>
<div class="m2"><p>ز نا قابلی قابل خر چرانی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سزاوار آن جمله کز اسب و استر</p></div>
<div class="m2"><p>کشی زیر و بمشان زنی تا توانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>پس آنگه شترها کنی پیش هر یک</p></div>
<div class="m2"><p>به صحرا فرستی پی ساربانی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بود خوبتر وصف صوف مرقع</p></div>
<div class="m2"><p>به گوش خردشان ز سبع المثانی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز بازار آیند چون شب به خانه</p></div>
<div class="m2"><p>به پرسند هر یک ز نوکر نهانی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>که دیروز چون از فلان جا گذشتم</p></div>
<div class="m2"><p>نمی‌کرد تعریف صوفم فلانی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز پی‌شان غلامان ز کرس شبانه</p></div>
<div class="m2"><p>زمین‌گیر چون سایه از ناتوانی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو وحشی وطن کن به دشت خموشی</p></div>
<div class="m2"><p>مکن ناله از درد بی‌خانمانی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>همان گیر کز تست این دیر ششدر</p></div>
<div class="m2"><p>پر از زر در او نه خم خسروانی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>مخور غم گرت نیست اسب رونده</p></div>
<div class="m2"><p>چو بر توسن طبع داری روانی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سخن گستری بر دعا ختم سازم</p></div>
<div class="m2"><p>که سر می‌کشد خامه از هم زبانی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>الا تا مه نو در این کهنه میدان</p></div>
<div class="m2"><p>کند گوی خورشید را صولجانی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به چوگانی عیش بادا سواره</p></div>
<div class="m2"><p>مطیعت به میدان گه کامرانی</p></div></div>