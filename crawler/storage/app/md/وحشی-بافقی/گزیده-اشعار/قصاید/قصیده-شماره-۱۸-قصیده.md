---
title: >-
    قصیدهٔ شمارهٔ ۱۸ - قصیده
---
# قصیدهٔ شمارهٔ ۱۸ - قصیده

<div class="b" id="bn1"><div class="m1"><p>ای فلک چند ز بیداد تو بینم آزار</p></div>
<div class="m2"><p>من خود آزرده دلم با دل خویشم بگذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند ما را ز جفای تو دود اشک به روی</p></div>
<div class="m2"><p>ما به روی تو نیاریم تو خود شرم بدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جفاگر غرضت ریختن خون من است</p></div>
<div class="m2"><p>پا کشیدم ز جهان تیغ بکش دست برآر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشت بر عکس هر آن نقش مرادی که زدم</p></div>
<div class="m2"><p>جرم بازنده چه باشد که بد افتاد قمار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک از رشتهٔ تدبیر نگردد به مراد</p></div>
<div class="m2"><p>نافه را تار عناکب نتوان کرد مهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داغ اندوه مرا باز مپرسید حساب</p></div>
<div class="m2"><p>نیست آن چیز کواکب که درآید به شمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر فلک مرهم زنگار کنم کافی نیست</p></div>
<div class="m2"><p>بسکه این سینه ز الماس نجوم است فکار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سنگباران شدم از دست غم دهر و هنوز</p></div>
<div class="m2"><p>بخت سر گشته‌ام از خواب نگردد بیدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند باشم به غم و غصهٔ ایام صبور</p></div>
<div class="m2"><p>چند گیرم به سر کوچهٔ اندوه قرار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می‌روم داد زنان بر در دارای زمان</p></div>
<div class="m2"><p>آنکه بر مقصد او دور فلک راست مدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آصف ملک جهان خواجهٔ با نام و نشان</p></div>
<div class="m2"><p>سایهٔ مرحمت شاه سلیمان آثار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چرخ پیش نظر همت او پاره مسی‌ست</p></div>
<div class="m2"><p>که درین مهره گل گشته نهان در زنگار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنکه چون‌گل به هواداری او خندان نیست</p></div>
<div class="m2"><p>که درین مهرهٔ گل گشته نهان در زنگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنکه چون گل به هواداری او خندان نیست</p></div>
<div class="m2"><p>هست با سبزه گلنار مدامش سر و کار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لیک زهری که بود در ته جامش سبزه</p></div>
<div class="m2"><p>لیک خونی که بود بر سر داغش گلنار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>توسن قدر تو زان سوی فلک تا بجهد</p></div>
<div class="m2"><p>سدره‌اش رایض اندیشه کند میخ جدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رشک احسان تو زد در دل دریا آتش</p></div>
<div class="m2"><p>هست دود دل دریا که شدش نام بخار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیست سر برزده هر گوشه حباب از سر آب</p></div>
<div class="m2"><p>چشم بر راه کف جود تو دارند بحار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر کمان یک جهت خصم بداندیش تو نیست</p></div>
<div class="m2"><p>از چه رو تیر دو شاخه کندش از سوفار</p></div></div>