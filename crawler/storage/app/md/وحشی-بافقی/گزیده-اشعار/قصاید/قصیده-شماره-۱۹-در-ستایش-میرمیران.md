---
title: >-
    قصیدهٔ شمارهٔ ۱۹ - در ستایش میرمیران
---
# قصیدهٔ شمارهٔ ۱۹ - در ستایش میرمیران

<div class="b" id="bn1"><div class="m1"><p>لله الحمد کز حضیض خطر</p></div>
<div class="m2"><p>شد به اوج آفتاب دین پرور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم خفاش کور گو می‌باش</p></div>
<div class="m2"><p>کز فلک مهر بگذراند افسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکرلله که حفظ یزدانی</p></div>
<div class="m2"><p>پیش تیر قضا گرفت سپر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جست بیرون ز پشت دشمن شاه</p></div>
<div class="m2"><p>ناوک پر کشی که داشت قدر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابر خیرات شاه بست تتق</p></div>
<div class="m2"><p>گشت باران او زر و گوهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دور شو گو بلا ز سر تا پا</p></div>
<div class="m2"><p>دهر گو باش فتنه پا تا سر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نحل عمر و بنای دانش را</p></div>
<div class="m2"><p>زان چه آسیب یا از آن چه ضرر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرخ ویران نگردد از توفان</p></div>
<div class="m2"><p>نشود کنده طوبی از صر صر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه که سد شکر سد هزاران شکر</p></div>
<div class="m2"><p>که سر آمد زمان فتنه و شر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صبح شادی رسید خنده زنان</p></div>
<div class="m2"><p>کار خود کرد گریه‌های سحر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کوس شادی زدند بر سر چرخ</p></div>
<div class="m2"><p>رقص کردند انجم و مه و خور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گریه‌ها رفت و خنده ها آمد</p></div>
<div class="m2"><p>ای خوشا گریه‌های خنده‌اثر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خوش بخند ای زمانه خواهی داشت</p></div>
<div class="m2"><p>خنده بهر کدام روز دگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عیش کن عیش کن که ممکن نیست</p></div>
<div class="m2"><p>که بود روزگار ازین خوشتر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عیش و عشرت درآمد از در وبام</p></div>
<div class="m2"><p>بنگر بر بساط خود بنگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صحت شاه و خلعت شاهی</p></div>
<div class="m2"><p>آن در آمد ز بام و این از در</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صحتی و چه صحت کامل</p></div>
<div class="m2"><p>خلعتی و چه خلعتی در خور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صحتی دامن از مرض چیده</p></div>
<div class="m2"><p>خلعت عمر جاودان در بر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خلعتی پای رفعتش بر چرخ</p></div>
<div class="m2"><p>افسر عز سرمدی بر سر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنچنان خلعت اینچنین صحت</p></div>
<div class="m2"><p>بر تن و جان شاه دین پرور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باد زیبنده تا به صبح نشور</p></div>
<div class="m2"><p>باد پاینده تا دم محشر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>میرمیران که تا جهان باشد</p></div>
<div class="m2"><p>باشد او در جهان جهان داور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صحت عمر و دولتش جاوید</p></div>
<div class="m2"><p>اخترش یار ودولتش یاور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ایکه خواهی عطای بیخواهش</p></div>
<div class="m2"><p>بر در کبریای او بگذر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا ببینی بلند درگاهی</p></div>
<div class="m2"><p>شمسه‌اش طاق چرخ را زیور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زو روان آرزوی خاطرها</p></div>
<div class="m2"><p>کاروان کاروان به هر کشور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گنج احسان در او و دربان نه</p></div>
<div class="m2"><p>خانهٔ گنج و گنج بی اژدر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بسکه از مهر بر برات سخاش</p></div>
<div class="m2"><p>سوده گردد نگین انگشتر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر بدخشان تمام لعل شود</p></div>
<div class="m2"><p>ناید از عهدهٔ دو هفته بدر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بحری از دانش است مالامال</p></div>
<div class="m2"><p>نه کنارش پدید و نه معبر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جمله حالات گیتی‌اش در ذکر</p></div>
<div class="m2"><p>همه تاریخ عالمش از بر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سرو را نطفهٔ عدوی ترا</p></div>
<div class="m2"><p>نقش می‌بست دست صورتگر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چشم تا می‌نگاشت نشتر بود</p></div>
<div class="m2"><p>به گلو چون رسید شد خنجر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>طرفه مرغی‌ست خصم یاوه درا</p></div>
<div class="m2"><p>بیضه آرد به دعوی گوهر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چه توان کرد می‌رسد او را</p></div>
<div class="m2"><p>آمده دعوی خودش باور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اینقدر خود چرا نمی‌داند</p></div>
<div class="m2"><p>که شما دیگرید و او دیگر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کیست او قطره‌ایست بی مقدار</p></div>
<div class="m2"><p>بلکه از قطره پاره‌ای کمتر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>قطره‌ای را چه کار با عمان</p></div>
<div class="m2"><p>عرضی را چه بحث با جوهر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گوهر این بلند پروازی</p></div>
<div class="m2"><p>زانکه او نیست مرغ این منظر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ماکیان تا به بام مزبله بیش</p></div>
<div class="m2"><p>نپرد گر چه بال دارد و پر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>امر و نهی ترا به کل امور</p></div>
<div class="m2"><p>هرکه نبود مطیع و فرمانبر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کافرش خوانم و کنم ثابت</p></div>
<div class="m2"><p>کافر است او به شرع پیغمبر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زانکه گر هست امر تو در نهی</p></div>
<div class="m2"><p>هست عین شریعت اطهر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هر که او تابع شریعت نیست</p></div>
<div class="m2"><p>هست درحکم شرع و دین کافر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در حواشی دولتت شاها</p></div>
<div class="m2"><p>کرده از بس طهارت تو اثر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>لب به سد احتیاط تر سازد</p></div>
<div class="m2"><p>مشک سقای کویت از کوثر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر سکندر که آب حیوان جست</p></div>
<div class="m2"><p>نور رأی تو بودیش رهبر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>روی شستی نه دست ز آب حیات</p></div>
<div class="m2"><p>لب تر داشتی نه دیدهٔ تر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>زنده بودی هنوز و پیش تو داشت</p></div>
<div class="m2"><p>دست بر سینه چون کمین چاکر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اخذ می‌کرد از تو عز و شکوه</p></div>
<div class="m2"><p>کسب می‌کرد از تو علم و هنر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>روغنی در چراغ بخت نداشت</p></div>
<div class="m2"><p>آب جست و نبودش آبشخور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زنده بودی و خدمتت کردی</p></div>
<div class="m2"><p>بودی ار بخت یار اسکندر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چون نشینی و مسند آرایی</p></div>
<div class="m2"><p>و ز دو سو آن دو نامدار پسر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چون سپهری ولی سپهر نهم</p></div>
<div class="m2"><p>که نشیند میان شمس و قمر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>عنبر اندر مجالس خلقت</p></div>
<div class="m2"><p>خدمتی پیش برده بود مگر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>وقت فرصت به طیب خلق تو زد</p></div>
<div class="m2"><p>به طریقی که کس نیافت خبر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بوی غماز بود و پرده درید</p></div>
<div class="m2"><p>لاجرم روسیاه شد عنبر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در زمان عدالت تو که هست</p></div>
<div class="m2"><p>شوهر شیر ماده آهوی نر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مادری کرد گرگ ماده و شد</p></div>
<div class="m2"><p>دایه بره‌های بی‌مادر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ظالمی بود نام او گردون</p></div>
<div class="m2"><p>خلق در دست ظلم او مضطر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>زو فقیران تمام در آزار</p></div>
<div class="m2"><p>زو اسیران تمام در آذر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>در قرانهاش سد خطر ور غم</p></div>
<div class="m2"><p>در نظرهاش سد ضرر مضمر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سوختش آتش سیاست شاه</p></div>
<div class="m2"><p>دور دادش به باد خاکستر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مجملا از وجود او نگذاشت</p></div>
<div class="m2"><p>غیرخاکستری و چند شرر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دهر زد جار کای ستمکاران</p></div>
<div class="m2"><p>ظلم آخر شود به این منجر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>پند گیرید کاین زمان اینست</p></div>
<div class="m2"><p>آنکه دی چرخ بود دوش اختر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>حبذا این دراز دستی عدل</p></div>
<div class="m2"><p>کش سر چرخ هست در چنبر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>سرظالم چو خاک کردی پست</p></div>
<div class="m2"><p>سر بلندیت باد ای سرور</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>سایه دولت تو بر سر خلق</p></div>
<div class="m2"><p>سایهٔ پادشه ترا بر سر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ای ز تو روشنم چراغ سخن</p></div>
<div class="m2"><p>چون چراغ دریچهٔ خاور</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هر چراغی که از تو افروزند</p></div>
<div class="m2"><p>شرق و غرب جهان کند انور</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>اندرین روزها که حضرت شاه</p></div>
<div class="m2"><p>تکیه فرموده بود بر بستر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>یک شبم هیچگونه خواب نبود</p></div>
<div class="m2"><p>آمدم بر در دعای سحر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به نماز و نیاز رفتم پیش</p></div>
<div class="m2"><p>که وضو داشتم ز خون جگر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>در میان نماز خوابم برد</p></div>
<div class="m2"><p>خواب دیدم که گنبد اخضر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>شق شد و دختری برون آمد</p></div>
<div class="m2"><p>گفتمش خیر مقدم ای دختر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>کیستی با چنین شمایل و شکل</p></div>
<div class="m2"><p>مرحبا ای نگار خوش منظر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>پیکرتو کجاست گر جانی</p></div>
<div class="m2"><p>ما ندیدیم جان بی پیکر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گفت خود را بگو مبارک باد</p></div>
<div class="m2"><p>که شدت نام در زمانه سمر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>همچو من دختری خدا دادت</p></div>
<div class="m2"><p>دختری مادر هزار پسر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>آنچنان دختری که تا سد قرن</p></div>
<div class="m2"><p>زو بماند بلند نام پدر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>قلمت کو که گردد آبستن</p></div>
<div class="m2"><p>کآمدم تا بزایم از مادر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ساعت سعد اختیار کنم</p></div>
<div class="m2"><p>به سر خویش در کشم چادر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بروم تا حریم خلوت شاه</p></div>
<div class="m2"><p>در رخ آورده گوشهٔ معجر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>رو نهفته ز چشم نا محرم</p></div>
<div class="m2"><p>در روم بزم شاه را از در</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چون غلامان بیفتمش در پای</p></div>
<div class="m2"><p>چون کنیزان بگردمش بر سر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به کنیزی گرم قبول کند</p></div>
<div class="m2"><p>بکنم ناز بر مه و اختر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ور نه آنجا به خدمتی باشم</p></div>
<div class="m2"><p>هست آنجا چو من هزار دگر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>می‌شنیدم ولی که می‌گفتند</p></div>
<div class="m2"><p>پیش از آن کیم اینطرف به سفر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>کای شفاء القلوب دل خوش دار</p></div>
<div class="m2"><p>که ترا نیست غیر از او شوهر</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>زین نکاح آنقدر برانی کام</p></div>
<div class="m2"><p>که تو خود هم نیایدت باور</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>کام بخشا زتو مسم زر شد</p></div>
<div class="m2"><p>کار خود کرد کیمیای نظر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چه شناسند این سخن آنها</p></div>
<div class="m2"><p>که ندانند بصره را ز بصر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>تو شناسی که جوهری داند</p></div>
<div class="m2"><p>هنر و عیب و قیمت جوهر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>چه برم آب این سخن بر آن</p></div>
<div class="m2"><p>کش مساویست اختر و اخگر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>حجره را گور اگر تماشاییست</p></div>
<div class="m2"><p>اندر او خواه لعل و خواه حجر</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>گردن خر به در نیارایم</p></div>
<div class="m2"><p>گوهرست این سخن نه مهره خر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>کاه باید نه زعفران خر را</p></div>
<div class="m2"><p>گاو را پنبه دانه به که درر</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>داورا رسم و عادت شعر است</p></div>
<div class="m2"><p>که اگر شان دهند سد کشور</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>همچنان کشوری دگر طلبند</p></div>
<div class="m2"><p>این چنینند شاعران اکثر</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>بنده هم شاعرم ولی ز شما</p></div>
<div class="m2"><p>صله چندان گرفته‌ام که اگر</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>در خور شکر آن سخن رانم</p></div>
<div class="m2"><p>بایدم طرح کرد سد دفتر</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>خود نمی‌خواهم ار نه آماده‌ست</p></div>
<div class="m2"><p>هم مرا اسب و هم مرا نوکر</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>زانکه شاعر که اسب و نوکر یافت</p></div>
<div class="m2"><p>خویش را برد و کرد بر قنطر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>طیب الله ختم کن وحشی</p></div>
<div class="m2"><p>که به اطناب شد سخن منجر</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>تا به دست طبیب قانونیست</p></div>
<div class="m2"><p>تن چون ساز و نبض همچو وتر</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>باد قانون صحت تو به ساز</p></div>
<div class="m2"><p>رگت ایمن ز زخمهٔ نشتر</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>مجلس دلکشت به ساز و نوا</p></div>
<div class="m2"><p>ماه رقاص و زهره رامشگر</p></div></div>