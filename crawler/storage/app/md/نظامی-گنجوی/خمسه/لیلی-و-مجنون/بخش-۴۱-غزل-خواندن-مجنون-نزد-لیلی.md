---
title: >-
    بخش ۴۱ - غزل خواندن مجنون نزد لیلی
---
# بخش ۴۱ - غزل خواندن مجنون نزد لیلی

<div class="b" id="bn1"><div class="m1"><p>آیا تو کجا و ما کجائیم</p></div>
<div class="m2"><p>تو زان که‌ای و ما ترائیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مائیم و نوای بی‌نوائی</p></div>
<div class="m2"><p>بسم‌الله اگر حریف مائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افلاس خران جان فروشیم</p></div>
<div class="m2"><p>خز پاره کن و پلاس پوشیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بندگی زمانه آزاد</p></div>
<div class="m2"><p>غم شاد به ما و ما به غم شاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تشنه جگر و غریق آبیم</p></div>
<div class="m2"><p>شب کور و ندیم آفتابیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گمراه و سخن زره نمائی</p></div>
<div class="m2"><p>در ده نه و لاف دهخدائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ده راند و دهخدای نامیم</p></div>
<div class="m2"><p>چون ماه به نیمه تمامیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌مهره و دیده حقه بازیم</p></div>
<div class="m2"><p>بی‌پا و رکیب رخش تازیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز در غم تو قدم نداریم</p></div>
<div class="m2"><p>غم‌دار توئیم و غم نداریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در عالم اگرچه سست خیزیم</p></div>
<div class="m2"><p>در کوچگه رحیل تیزیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گوئی که بمیر در غمم زار</p></div>
<div class="m2"><p>هستم ز غم تو اندرین کار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آخر به زنم به وقت حالی</p></div>
<div class="m2"><p>بر طبل رحیل خود دوالی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرگ از دمه گر هراس دارد</p></div>
<div class="m2"><p>با خود نمد و پلاس دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شب خوش مکنم که نیست دلکش</p></div>
<div class="m2"><p>بی‌تو شب ما و آنگهی خوش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ناآمده رفتن این چه سازست</p></div>
<div class="m2"><p>ناکشته درودن اینچه رازست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با جان منت قدم نسازد</p></div>
<div class="m2"><p>یعنی که دو جان بهم نسازد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا جان نرود ز خانه بیرون</p></div>
<div class="m2"><p>نایی تو از این بهانه بیرون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جانی به هزار بار نامه</p></div>
<div class="m2"><p>معزول کنش ز کار نامه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جانی به از این بیار در ده</p></div>
<div class="m2"><p>پائی به از این بکار درنه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر جان که نه از لب تو آید</p></div>
<div class="m2"><p>آید به لب و مرا نشاید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وان جان که لب تواش خزانه است</p></div>
<div class="m2"><p>گنجینه عمر جاودانه است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بسیار کسان ترا غلامند</p></div>
<div class="m2"><p>اما نه چو من مطیع نامند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا هست ز هستی تو یادم</p></div>
<div class="m2"><p>آسوده و تن درست و شادم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وانگه که ز دل نیارمت یاد</p></div>
<div class="m2"><p>باشم به دلی که دشمنت باد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زین پس تو و من و من تو زین پس</p></div>
<div class="m2"><p>یک دل به میان ما دو تن بس</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وان دل دل تو چنین صوابست</p></div>
<div class="m2"><p>یعنی دل من دلی خرابست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>صبحی تو و با تو زیست نتوان</p></div>
<div class="m2"><p>الا به یکی دل و دو صد جان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در خود کشمت که رشته یکتاست</p></div>
<div class="m2"><p>تا این دو عدد شود یکی راست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون سکه ما یگانه گردد</p></div>
<div class="m2"><p>نقش دوئی از میانه گردد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بادام که سکه نغز دارد</p></div>
<div class="m2"><p>یک تن بود و دو مغز دارد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>من با توام آنچه مانده بر جای</p></div>
<div class="m2"><p>کفشی است برون فتاده از پای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آنچه آن من است با تو نور است</p></div>
<div class="m2"><p>دورم من از آنچه از تو دور است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تن کیست که اندرین مقامش</p></div>
<div class="m2"><p>بر سکه تو زنند نامش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سر نزل غم ترا نشاید</p></div>
<div class="m2"><p>زیر علم ترا نشاید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جانیست جریده در میان چست</p></div>
<div class="m2"><p>وان نیز نه با منست با تست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو سگدل و پاسبانت سگ روی</p></div>
<div class="m2"><p>من خاک ره سگان آن کوی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سگبانی تو همی گزینم</p></div>
<div class="m2"><p>در جنب سگان از آن نشینم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یعنی ددگان مرا به دنبال</p></div>
<div class="m2"><p>هستند سگان تیز چنگال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو با زر و با درم همه سال</p></div>
<div class="m2"><p>خالت درم و زر است خلخال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا خال درم وش تو دیدم</p></div>
<div class="m2"><p>خلخال ترا درم خریدم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ابر از پی نوبهار بگریست</p></div>
<div class="m2"><p>مجنون ز پی تو زار بگریست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چرخ از رخ مه جمال گیرد</p></div>
<div class="m2"><p>مجنون به رخ تو فال گیرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هندوی سیاه پاسبانت</p></div>
<div class="m2"><p>مجنون ببر تو همچنانست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بلبل ز هوای گل به گرد است</p></div>
<div class="m2"><p>مجنون ز فراق تو به درد است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خلق از پی لعل می‌کند کان</p></div>
<div class="m2"><p>مجنون ز پی تو می‌کند جان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یارب چه خوش اتفاق باشد</p></div>
<div class="m2"><p>گر با منت اشتیاق باشد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مهتاب شبی چو روز روشن</p></div>
<div class="m2"><p>تنها من و تو میان گلشن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>من با تو نشسته گوش در گوش</p></div>
<div class="m2"><p>با من تو کشیده نوش در نوش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در بر کشمت چو رود در چنگ</p></div>
<div class="m2"><p>پنهان کنمت چو لعل در سنگ</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گردم ز خمار نرگست مست</p></div>
<div class="m2"><p>مستانه کشم به سنبلت دست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>برهم شکنم شکنج گیسوت</p></div>
<div class="m2"><p>تاگوش کشم کمان ابروت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>با نار برت نشست گیرم</p></div>
<div class="m2"><p>سیب زنخت به دست گیرم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گه نار ترا چو سیب سایم</p></div>
<div class="m2"><p>گه سیب ترا چو نار خایم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گه زلف برافکنم به دوشت</p></div>
<div class="m2"><p>گه حلقه برون کنم ز گوشت</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گاه از قصبت صحیفه شویم</p></div>
<div class="m2"><p>گه با رطبت بدیهه گویم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گه گرد گلت بنفشه کارم</p></div>
<div class="m2"><p>گاهی ز بنفشه گل برآرم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گه در بر خود کنم نشستت</p></div>
<div class="m2"><p>که نامه غم دهم به دستت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>یار اکنون شو که عمر یار است</p></div>
<div class="m2"><p>کار است به وقت و وقت کار است</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چشمه منما چو آفتابم</p></div>
<div class="m2"><p>مفریب ز دور چون سرابم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از تشنگی جمالت ای جان</p></div>
<div class="m2"><p>جوجو شده‌ام چو خالت ای جان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>یک جو ندهی دلم در این کار</p></div>
<div class="m2"><p>خوناب دلم دهی به خروار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>غم خوردن بی تو می‌توانم</p></div>
<div class="m2"><p>می خوردن با تو نیز دانم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در بزم تو می‌خجسته فالست</p></div>
<div class="m2"><p>یعنی به بهشت می حلالست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>این گفت و گرفت راه صحرا</p></div>
<div class="m2"><p>خون در دل و در دماغ صفرا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>وان سرو رونده زان چمنگاه</p></div>
<div class="m2"><p>شد روی گرفته سوی خرگاه</p></div></div>