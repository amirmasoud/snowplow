---
title: >-
    بخش ۸ - در شکایت حسودان و منکران
---
# بخش ۸ - در شکایت حسودان و منکران

<div class="b" id="bn1"><div class="m1"><p>بر جوش دلا که وقت جوش است</p></div>
<div class="m2"><p>گویای جهان چرا خموش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میدان سخن مراست امروز</p></div>
<div class="m2"><p>به زین سخنی کجاست امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اجری خور دسترنج خویشم</p></div>
<div class="m2"><p>گر محتشمم ز گنج خویشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین سحر سحرگهی که رانم</p></div>
<div class="m2"><p>مجموعه هفت سبع خوانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سحری که چنین حلال باشد</p></div>
<div class="m2"><p>منکر شدنش وبال باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سحر سخن چنان تمامم</p></div>
<div class="m2"><p>کایینه غیب گشت نامم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمشیر زبانم از فصیحی</p></div>
<div class="m2"><p>دارد سر معجز مسیحی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نطقم اثر آنچنان نماید</p></div>
<div class="m2"><p>کز جذر اصم زبان گشاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حرفم ز تبش چنان فروزد</p></div>
<div class="m2"><p>کانگشت بر او نهی بسوزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شعر آب ز جویبار من یافت</p></div>
<div class="m2"><p>آوازه به روزگار من یافت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این بی‌نمکان که نان خورانند</p></div>
<div class="m2"><p>در سایه من جهان خورانند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>افکندن صید کار شیر است</p></div>
<div class="m2"><p>روبه ز شکار شیر سیر است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از خوردن من به کام و حلقی</p></div>
<div class="m2"><p>آن به که ز من خورند خلقی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حاسد ز قبول این روائی</p></div>
<div class="m2"><p>دور از من و تو به ژاژ خائی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون سایه شده به پیش من پست</p></div>
<div class="m2"><p>تعریض مرا گرفته در دست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر پیشه کنم غزل‌سرائی</p></div>
<div class="m2"><p>او پیش نهد دغل درآئی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر ساز کنم قصایدی چست</p></div>
<div class="m2"><p>او باز کند قلایدی سست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بازم چو به نظم قصه راند</p></div>
<div class="m2"><p>قصه چه کنم که قصه خواند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من سکه زنم به قالبی خوب</p></div>
<div class="m2"><p>او نیز زند ولیک مقلوب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کپی همه آن کند که مردم</p></div>
<div class="m2"><p>پیداست در آب تیره انجم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر هر جسدی که تابد آن نور</p></div>
<div class="m2"><p>از سایه خویش هست رنجور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سایه که نقیصه ساز مردست</p></div>
<div class="m2"><p>در طنز گری گران نورداست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>طنزی کند و ندارد آزرم</p></div>
<div class="m2"><p>چون چشمش نیست کی بود شرم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیغمبر کو نداشت سایه</p></div>
<div class="m2"><p>آزاد نبود از این طلایه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دریای محیط را که پاکست</p></div>
<div class="m2"><p>از چرک دهان سگ چه باکست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هرچند ز چشم زرد گوشان</p></div>
<div class="m2"><p>سرخست رخم ز خون جوشان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون بحر کنم کناره‌شوئی</p></div>
<div class="m2"><p>اما نه ز روی تلخ‌روئی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زخمی چو چراغ می‌خورم چست</p></div>
<div class="m2"><p>وز خنده چو شمع می‌شوم سست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون آینه گر نه آهنینم</p></div>
<div class="m2"><p>با سنگ دلان چرا نشینم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کان کندن من مبین که مردم</p></div>
<div class="m2"><p>جان کندن خصم بین ز دردم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در منکر صنعتم بهی نیست</p></div>
<div class="m2"><p>کالا شب چارشنبهی نیست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دزد در من به جای مزدست</p></div>
<div class="m2"><p>بد گویدم ارچه بانگ دزدست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دزدان چو به کوی دزد جویند</p></div>
<div class="m2"><p>در کوی دوند و دزد گویند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در دزدی من حلال بادش</p></div>
<div class="m2"><p>بد گفتن من وبال باشد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بیند هنر و هنر نداند</p></div>
<div class="m2"><p>بد می‌کند اینقدر نداند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر با بصر است بی‌بصر باد</p></div>
<div class="m2"><p>وز کور شد است کورتر باد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>او دزدد و من گدازم از شرم</p></div>
<div class="m2"><p>دزد افشاریست این نه آزرم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نی‌نی چو به کدیه دل نهاد است</p></div>
<div class="m2"><p>گو خیزد و بیا که در گشاد است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آن کاوست نیازمند سودی</p></div>
<div class="m2"><p>گر من بدمی چه چاره بودی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گنج دو جهان در آستینم</p></div>
<div class="m2"><p>در دزدی مفلسی چه بینم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>واجب صدقه‌ام به زیر دستان</p></div>
<div class="m2"><p>گو خواه بدزد و خواه بستان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دریای در است و کان گنجم</p></div>
<div class="m2"><p>از نقب زنان چگونه رنجم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گنجینه به بند می‌توان داشت</p></div>
<div class="m2"><p>خوبی به سپند می‌توان داشت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مادر که سپندیار دادم</p></div>
<div class="m2"><p>با درع سپندیار زادم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در خط نظامی ار نهی گام</p></div>
<div class="m2"><p>بینی عدد هزار و یک نام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>والیاس کالف بری ز لامش</p></div>
<div class="m2"><p>هم با نود و نه است نامش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زینگونه هزار و یک حصارم</p></div>
<div class="m2"><p>با صد کم یک سلیح دارم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هم فارغم از کشیدن رنج</p></div>
<div class="m2"><p>هم ایمنم از بریدن گنج</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گنجی که چنین حصار دارد</p></div>
<div class="m2"><p>نقاب در او چکار دارد؟</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اینست که گنج نیست بی‌مار</p></div>
<div class="m2"><p>هرجا که رطب بود خار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هر ناموری که او جهانداشت</p></div>
<div class="m2"><p>بدنام کنی ز همرهان داشت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یوسف که ز ماه عقد می‌بست</p></div>
<div class="m2"><p>از حقد برادران نمی‌رست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>عیسی که دمش نداشت دودی</p></div>
<div class="m2"><p>می‌برد جفای هر جهودی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>احمد که سرآمد عرب بود</p></div>
<div class="m2"><p>هم خسته خار بولهب بود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دیر است که تا جهان چنین است</p></div>
<div class="m2"><p>پی نیش مگس کم انگبین است</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تا من منم از طریق زوری</p></div>
<div class="m2"><p>نازرد زمن جناح موری</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دری به خوشاب نشستم</p></div>
<div class="m2"><p>شوریدن کار کس نجستم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>زآنجا که نه من حریف خویم</p></div>
<div class="m2"><p>در حق سگی بدی نگویم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بر فسق سگی که شیریم داد</p></div>
<div class="m2"><p>(لاعیب له) دلیریم داد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دانم که غضب نهفته بهتر</p></div>
<div class="m2"><p>وین گفته که شد نگفته بهتر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>لیکن به حساب کاردانی</p></div>
<div class="m2"><p>بی‌غیرتی است بی‌زبانی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آن کس که ز شهر آشنائیست</p></div>
<div class="m2"><p>داند که متاع ما کجائیست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>وانکو به کژی من کشد دست</p></div>
<div class="m2"><p>خصمش نه منم که جز منی هست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>خاموش دلا ز هرزه گوئی</p></div>
<div class="m2"><p>می‌خور جگری به تازه‌روئی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چون گل به رحیل کوس می‌زن</p></div>
<div class="m2"><p>بر دست کشنده بوس می‌زن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نان خورد ز خون خویش می‌دار</p></div>
<div class="m2"><p>سر نیست کلاه پیش می‌دار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>آزار کشی کن و میازار</p></div>
<div class="m2"><p>کازرده تو به که خلق بازار</p></div></div>