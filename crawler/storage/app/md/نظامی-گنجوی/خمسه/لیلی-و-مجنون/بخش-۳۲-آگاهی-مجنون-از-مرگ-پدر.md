---
title: >-
    بخش ۳۲ - آگاهی مجنون از مرگ پدر
---
# بخش ۳۲ - آگاهی مجنون از مرگ پدر

<div class="b" id="bn1"><div class="m1"><p>روزی ز قضا به وقت شبگیر</p></div>
<div class="m2"><p>می‌رفت شکاریی به نخجیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر نجد نشسته بود مجنون</p></div>
<div class="m2"><p>چون بر سر تاج در مکنون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صیاد چو دید بر گذر شیر</p></div>
<div class="m2"><p>بگشاد در او زبان چو شمشیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرسید ورا چو سوکواران</p></div>
<div class="m2"><p>کای دور از اهل بیت و یاران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فارغ که ز پیش تو پسی هست</p></div>
<div class="m2"><p>یا جز لیلی ترا کسی هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نز مادر و نز پدر بیادت</p></div>
<div class="m2"><p>بی‌شرم کسی که شرم بادت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون تو خلفی به خاک بهتر</p></div>
<div class="m2"><p>کز ناخلفی براوری سر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گیرم ز پدر به زندگانی</p></div>
<div class="m2"><p>دوری طلبیدی از جوانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون مرد پدر ترا بقا باد</p></div>
<div class="m2"><p>آخر کم ازآنکه آریش یاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آیی به زیارتش زمانی</p></div>
<div class="m2"><p>واری ز ترحمش نشانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در پوزش تربتش پناهی</p></div>
<div class="m2"><p>عذری ز روان او بخواهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مجنون ز نوای آن کج آهنگ</p></div>
<div class="m2"><p>نالید و خمید راست چون چنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خود را ز دریغ بر زمین زد</p></div>
<div class="m2"><p>بسیار طپانچه بر جبین زد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز آرام و قرا گشت خالی</p></div>
<div class="m2"><p>تاگور پدر دوید حالی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون شوشه تربت پدر دید</p></div>
<div class="m2"><p>الماس شکسته در جگر دید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر تربتش اوفتاد بی‌هوش</p></div>
<div class="m2"><p>بگرفتش چون جگر در آغوش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از دوستی روان پاکش</p></div>
<div class="m2"><p>تر کرد به آب دیده خاکش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گه خاک ورا گرفت در بر</p></div>
<div class="m2"><p>گه کرد ز درد خاک بر سر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زندانی روز را شب آمد</p></div>
<div class="m2"><p>بیمار شبانه را تب آمد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>او خود همه ساله درستم بود</p></div>
<div class="m2"><p>کز گام نخست اسیر غم بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آنکس که اسیر بیم گردد</p></div>
<div class="m2"><p>چون باشد چون یتیم گردد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نومید شده ز دستگیری</p></div>
<div class="m2"><p>با ذل یتیمی و اسیری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>غلطید بران زمین زمانی</p></div>
<div class="m2"><p>می‌جست ز هم نشین نشانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون غم خور خویش را نمی‌یافت</p></div>
<div class="m2"><p>از غم خوردن عنان نمی‌تافت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چندان ز مژه سرشک خون ریخت</p></div>
<div class="m2"><p>کاندام زمین به خون برآمیخت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفت ای پدر ای پدر کجائی</p></div>
<div class="m2"><p>کافسر به پسر نمی‌نمائی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای غم خور من کجات جویم</p></div>
<div class="m2"><p>تیمار غم تو با که گویم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو بی پسری صلاح دیدی</p></div>
<div class="m2"><p>زان روی به خاک درکشیدی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من بی‌پدری ندیده بودم</p></div>
<div class="m2"><p>تلخست کنون که آزمودم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سر کوفت دوریم مکن بیش</p></div>
<div class="m2"><p>من خود خجلم ز کرده خویش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فریاد برآید از نهادم</p></div>
<div class="m2"><p>کاید ز نصیحت تو یادم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو رایض من بکش خرامی</p></div>
<div class="m2"><p>من توسن تو به بد لگامی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو گوش مرا چو حلقه زر</p></div>
<div class="m2"><p>من دور ز تو چو حلقه بر در</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>من کرده درشتی و تو نرمی</p></div>
<div class="m2"><p>از من همه سردی از تو گرمی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو در غم جان من به صد درد</p></div>
<div class="m2"><p>من گرد جهان گرفته ناورد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو بستر من ز گرد رفته</p></div>
<div class="m2"><p>من رفته به ترک خواب گفته</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو بزم نشاط من نهاده</p></div>
<div class="m2"><p>من بر سر سنگی اوفتاده</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو گفته دعا و اثر نکرده</p></div>
<div class="m2"><p>من کشته درخت و بر نخورده</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جان دوستی ترا به مردم</p></div>
<div class="m2"><p>یاد آرم و جان برآرم از غم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بر جامه ز دیده نیل پاشم</p></div>
<div class="m2"><p>تا کور و کبود هر دو باشم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آه ای پدر آه از آنچه کردم</p></div>
<div class="m2"><p>یک درد نه با هزار دردم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آزردمت ای پدر نه بر جای</p></div>
<div class="m2"><p>وای ار به حلم نمی‌کنی وای</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آزار تو راه ما مگیراد</p></div>
<div class="m2"><p>ما را به گناه ما مگیراد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای نور ده ستاره من</p></div>
<div class="m2"><p>خوشنودی تست چاره من</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ترسم کندم خدای مأخوذ</p></div>
<div class="m2"><p>گر تو نشوی ز بنده خوشنود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گفتی جگر منی به تقدیر</p></div>
<div class="m2"><p>وانگاه بدین جگر زنی تیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر من جگر توام منابم</p></div>
<div class="m2"><p>چون بی نمکان مکن کبابم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زینسان جگرت به خون گشائی</p></div>
<div class="m2"><p>تو در جگر زمین چرائی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خون جگرم خوری بدین روز</p></div>
<div class="m2"><p>خوانی جگرم زهی جگر سوز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>با من جگرت جگر خور افتاد</p></div>
<div class="m2"><p>کاتش به چنین جگر در افتاد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گر در حق تو شدم گنه کار</p></div>
<div class="m2"><p>گشتم به گناه خود گرفتار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گر پند به گوش در نکردم</p></div>
<div class="m2"><p>از زخم تو گوشمال خوردم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>زینگونه دریغ و آه می‌کرد</p></div>
<div class="m2"><p>روزی به شبی سیاه می‌کرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تا شب علم سیاه ننمود</p></div>
<div class="m2"><p>ناله‌اش ز دهل زدن نیاسود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چون هاتف صبح دم برآورد</p></div>
<div class="m2"><p>وز کوه شفق علم برآورد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>اکسیری صبح کیمیاگر</p></div>
<div class="m2"><p>کرد از دم خویش خاک را زر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>آن خاک روان ز روی آن خاک</p></div>
<div class="m2"><p>بر پشته نجد رفت غمناک</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>می‌کرد همان سرشک باری</p></div>
<div class="m2"><p>اما به طریق سوکواری</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>می‌زد نفسی به شور بختی</p></div>
<div class="m2"><p>می‌زیست به صد هزار سختی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>می‌برد ز بهر دلفروزی</p></div>
<div class="m2"><p>روزی به شبی شبی به روزی</p></div></div>