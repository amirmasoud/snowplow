---
title: >-
    بخش ۴۶ - ختم کتاب به نام شروانشاه
---
# بخش ۴۶ - ختم کتاب به نام شروانشاه

<div class="b" id="bn1"><div class="m1"><p>شاها ملکا جهان پناها</p></div>
<div class="m2"><p>یک شاه نه بل هزار شاها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمشید یکم به تخت‌گیری</p></div>
<div class="m2"><p>خورشید دوم به بی‌نظیری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شروانشه کیقباد پیکر</p></div>
<div class="m2"><p>خاقان کبیر ابوالمظفر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی شروانشاه بل جهانشاه</p></div>
<div class="m2"><p>کیخسرو ثانی اختسان شاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ختم قران پادشاهی</p></div>
<div class="m2"><p>بی‌خاتم تو مباد شاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی که به طالع مبارک</p></div>
<div class="m2"><p>بیرون بری از سپهر تارک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشغول شوی به شادمانی</p></div>
<div class="m2"><p>وین نامه نغز را بخوانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از پیکر این عروس فکری</p></div>
<div class="m2"><p>گه گنج بری و گاه بکری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن باد که در پسند کوشی</p></div>
<div class="m2"><p>ز احسنت خودش پرند پوشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در کردن این چنین تفضل</p></div>
<div class="m2"><p>از تو کرم وز من تو کل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرچه دل پاک و بخت فیروز</p></div>
<div class="m2"><p>هستند تو را نصیحت آموز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زین ناصح نصرت آلهی</p></div>
<div class="m2"><p>بشنو دو سه حرف صبحگاهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر کام جهان جهان بپرداز</p></div>
<div class="m2"><p>کان به که تومانی از جهان باز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ملکی که سزای رایت تست</p></div>
<div class="m2"><p>خود در حرم ولایت تست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>داد و دهشت کران ندارد</p></div>
<div class="m2"><p>گر بیش کنی زیان ندارد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کاریکه صلاح دولت تست</p></div>
<div class="m2"><p>در جستن آن مکن عنان سست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از هرچه شکوه تو به رنج است</p></div>
<div class="m2"><p>پردازش اگرچه کان و گنج است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>موئی مپسند ناروائی</p></div>
<div class="m2"><p>در رونق کار پادشائی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دشمن که به عذر شد زبانش</p></div>
<div class="m2"><p>ایمن مشو وز در برانش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قادر شو و بردبار می‌باش</p></div>
<div class="m2"><p>می می‌خور و هوشیار می‌باش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بازوی تو گرچه هست کاری</p></div>
<div class="m2"><p>از عون خدای خواه یاری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رای تو اگرچه هست هشیار</p></div>
<div class="m2"><p>رای دیگران ز دست مگذار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با هیچ دو دل مشو سوی حرب</p></div>
<div class="m2"><p>تا سکه درست خیزد از ضرب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از صحبت آن کسی بپرهیز</p></div>
<div class="m2"><p>کو باشد گاه نرم و گه تیز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرجا که قدم نهی فراپیش</p></div>
<div class="m2"><p>باز آمدن قدم بیندیش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا کار به نه قدم برآید</p></div>
<div class="m2"><p>گر ده نکنی به خرج شاید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مفرست پیام داد جویان</p></div>
<div class="m2"><p>الا به زبان راست گویان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در قول چنان کن استواری</p></div>
<div class="m2"><p>کایمن شود از تو زینهاری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کس را به خود از رخ گشوده</p></div>
<div class="m2"><p>گستاخ مکن نیازموده</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر عهد کس اعتماد منمای</p></div>
<div class="m2"><p>تا در دل خود نیابیش جای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مشمار عدوی خرد را خرد</p></div>
<div class="m2"><p>خار از ره خود چنین توان برد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در گوش کسی میفکن آن راز</p></div>
<div class="m2"><p>کازرده شوی ز گفتنش باز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آنرا که زنی ز بیخ بر کن</p></div>
<div class="m2"><p>وآنرا که تو برکشی میفکن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از هرچه طلب کنی شب و روز</p></div>
<div class="m2"><p>بیش از همه نیکنامی اندوز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر کشتن آنکه با زبونیست</p></div>
<div class="m2"><p>تعجیل مکن اگرچه خونیست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر دوری کام خویش منگر</p></div>
<div class="m2"><p>کاقبال تواش درآرد از در</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زاینجمله فسانها که گویم</p></div>
<div class="m2"><p>با تو به سخن بهانه جویم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گرنه دل تو جهان خداوند</p></div>
<div class="m2"><p>محتاج نشد به جنس این پند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زانجا که تراست رهنمائی</p></div>
<div class="m2"><p>ناید ز تو جز صواب رائی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>درع تو به زیر چرخ گردان</p></div>
<div class="m2"><p>بس باد دعای نیک مردان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>حرز تو به وقت شادکامی</p></div>
<div class="m2"><p>بس باشد همت نظامی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یارب ز جمال این جهاندار</p></div>
<div class="m2"><p>آشوب و گزند را نهاندار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر در که زند تو سازکارش</p></div>
<div class="m2"><p>هرجا که رود تو باش یارش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بادا همه اولیاش منصور</p></div>
<div class="m2"><p>و اعداش چنانکه هست مقهور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>این نامه که نامدار وی باد</p></div>
<div class="m2"><p>بر دولت وی خجسته پی باد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هم فاتحه‌ایش هست مسعود</p></div>
<div class="m2"><p>هم عاقبتیش باد محمود</p></div></div>