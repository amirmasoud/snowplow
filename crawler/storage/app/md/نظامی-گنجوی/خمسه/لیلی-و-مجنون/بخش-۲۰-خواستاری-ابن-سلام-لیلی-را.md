---
title: >-
    بخش ۲۰ - خواستاری ابن‌سلام لیلی را
---
# بخش ۲۰ - خواستاری ابن‌سلام لیلی را

<div class="b" id="bn1"><div class="m1"><p>فهرست کش نشاط این باغ</p></div>
<div class="m2"><p>بر ران سخن چنین کشد داغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کانروز که مه به باغ می‌رفت</p></div>
<div class="m2"><p>چون ماه دو هفته کرده هر هفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل بر سر سرو دسته بسته</p></div>
<div class="m2"><p>بازار گلاب و گل شکسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلفین مسلسلش گره‌گیر</p></div>
<div class="m2"><p>پیچیده چو حلقه‌های زنجیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ره ز بنی‌اسد جوانی</p></div>
<div class="m2"><p>دیدش چو شکفته گلستانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شخصی هنری به سنگ و سایه</p></div>
<div class="m2"><p>در چشم عرب بلند پایه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسیار قبیله و قرابات</p></div>
<div class="m2"><p>کارش همه خدمت و مراعات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوش همه خلق بر سلامش</p></div>
<div class="m2"><p>بخت ابن‌سلام کرده نامش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم سیم خدا و هم قوی پشت</p></div>
<div class="m2"><p>خلقی سوی او کشیده انگشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از دیدن آن چراغ تابان</p></div>
<div class="m2"><p>در چاره چو باد شد شتابان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آگه نه که گرچه گنج بازد</p></div>
<div class="m2"><p>با باد چراغ در نسازد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون سوی و طنگه آمد از راه</p></div>
<div class="m2"><p>بودش طمع وصال آن ماه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مه را نگرفت کس در آغوش</p></div>
<div class="m2"><p>این نکته مگر شدش فراموش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چاره طلبید و کس فرستاد</p></div>
<div class="m2"><p>در جستن عقد آن پریزاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا لیلی را به خواستاری</p></div>
<div class="m2"><p>در موکب خود کشد عماری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیرنگ نمود و خواهش انگیخت</p></div>
<div class="m2"><p>خاکی شد و زر چو خاک می‌ریخت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پذرفت هزار گنج شاهی</p></div>
<div class="m2"><p>وز رم گله بیش از آنکه خواهی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون رفت میانجی سخنگوی</p></div>
<div class="m2"><p>در جستن آن نگار دلجوی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خواهش کریی بدست بوسی</p></div>
<div class="m2"><p>می‌کرد ز بهر آن عروسی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هم مادر و هم پدر نشستند</p></div>
<div class="m2"><p>وامید در آن حدیث بستند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفتند سخن به جای خویش است</p></div>
<div class="m2"><p>لیکن قدری درنگ پیش است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کاین تازه بهار بوستانی</p></div>
<div class="m2"><p>دارد عرضی ز ناتوانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون ماه ز بهیش باز خندیم</p></div>
<div class="m2"><p>شکرانه دهیم و عقد بندیم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>این عقد نشان سود باشد</p></div>
<div class="m2"><p>انشاء الله که زود باشد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اما نه هنوز روزکی چند</p></div>
<div class="m2"><p>می‌باید شد به وعده خرسند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا غنچه گل شکفته گردد</p></div>
<div class="m2"><p>خار از در باغ رفته گردد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گردنش به طوق زر درآریم</p></div>
<div class="m2"><p>با طوق زرش به تو سپاریم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون ابن‌سلام ازان نیازی</p></div>
<div class="m2"><p>شد نامزد شکیب سازی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرکب به دیار خویشتن راند</p></div>
<div class="m2"><p>بنشست و غبار خویش بنشاند</p></div></div>