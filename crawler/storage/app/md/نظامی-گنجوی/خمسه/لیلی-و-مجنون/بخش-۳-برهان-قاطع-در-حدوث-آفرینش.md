---
title: >-
    بخش ۳ - برهان قاطع در حدوث آفرینش
---
# بخش ۳ - برهان قاطع در حدوث آفرینش

<div class="b" id="bn1"><div class="m1"><p>در نوبت بار عام دادن</p></div>
<div class="m2"><p>باید همه شهر جام دادن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فیاضه ابر جود گشتن</p></div>
<div class="m2"><p>ریحان همه وجود گشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باریدن بی‌دریغ چون مل</p></div>
<div class="m2"><p>خندیدن بی‌نقاب چون گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرجای چو آفتاب راندن</p></div>
<div class="m2"><p>در راه ببدره زر فشاندن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دادن همه را به بخشش عام</p></div>
<div class="m2"><p>وامی و حلال کردن آن وام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرسیدن هر که در جهان هست</p></div>
<div class="m2"><p>کز فاقه روزگار چون رست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتن سخنی که کار بندد</p></div>
<div class="m2"><p>زان قطره چو غنچه باز خندد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من کین شکرم در آستین است</p></div>
<div class="m2"><p>ریزم که حریف نازنین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر جمله جهان فشانم این نوش</p></div>
<div class="m2"><p>فرزند عزیز خود کند گوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من بر همه تن شوم غذاساز</p></div>
<div class="m2"><p>خود قسم جگر بدو رسد باز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای ناظر نقش آفرینش</p></div>
<div class="m2"><p>بر دار خلل ز راه بینش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در راه تو هر کرا وجودیست</p></div>
<div class="m2"><p>مشغول پرستش و سجودیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر طبل تهی مزن جرس را</p></div>
<div class="m2"><p>بیکار مدان نوای کس را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر ذره که هست اگر غباریست</p></div>
<div class="m2"><p>در پرده مملکت بکاریست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این هفت حصار برکشیده</p></div>
<div class="m2"><p>بر هزل نباشد آفریده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وین هفت رواق زیر پرده</p></div>
<div class="m2"><p>آخر به گزاف نیست کرده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کار من و تو بدین درازی</p></div>
<div class="m2"><p>کوتاه کنم که نیست بازی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دیباچه ما که در نورد است</p></div>
<div class="m2"><p>نز بهر هوی و خواب و خورد است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از خواب و خورش به اربتابی</p></div>
<div class="m2"><p>کین در همه گاو و خر بیابی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زان مایه که طبعها سرشتند</p></div>
<div class="m2"><p>ما را ورقی دگر نوشتند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا در نگریم و راز جوئیم</p></div>
<div class="m2"><p>سررشته کار باز جوئیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بینیم زمین و آسمان را</p></div>
<div class="m2"><p>جوئیم یکایک این و آن را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کاین کار و کیائی از پی چیست</p></div>
<div class="m2"><p>او کیست کیای کار او کیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر خط که برین ورق کشید است</p></div>
<div class="m2"><p>شک نیست در آنکه آفرید است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر هر چه نشانه طرازیست</p></div>
<div class="m2"><p>ترتیب گواه کار سازیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سوگند دهم بدان خدایت</p></div>
<div class="m2"><p>کین نکته به دوست رهنمایت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کان آینه در جهان که دید است</p></div>
<div class="m2"><p>کاول نه به صیقلی رسید است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بی‌صیقلی آینه محال است</p></div>
<div class="m2"><p>هردم که جز این زنی وبال است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در هر چه نظر کنی به تحقیق</p></div>
<div class="m2"><p>آراسته کن نظر به توفیق</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>منگر که چگونه آفریده است</p></div>
<div class="m2"><p>کان دیده‌وری ورای دیده است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بنگر که ز خود چگونه برخاست</p></div>
<div class="m2"><p>وآن وضع به خود چگونه شد راست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا بر تو به قطع لازم آید</p></div>
<div class="m2"><p>کان از دگری ملازم آید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون رسم حواله شد برسام</p></div>
<div class="m2"><p>رستی تو ز جهل و من ز دشنام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر نقش بدیع کایدت پیش</p></div>
<div class="m2"><p>جز مبدع او در او میندیش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زین هفت پرند پرنیان رنگ</p></div>
<div class="m2"><p>گر پای برون نهی خوری سنگ</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پنداشتی این پرند پوشی</p></div>
<div class="m2"><p>معلوم تو گردد ار بکوشی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سررشته راز آفرینش</p></div>
<div class="m2"><p>دیدن نتوان به چشم بینش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>این رشته قضا نه آنچنان تافت</p></div>
<div class="m2"><p>کورا سررشته وا توان یافت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سررشته قدرت خدائی</p></div>
<div class="m2"><p>بر کس نکند گره گشائی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>عاجز همه عاقلان و شیدا</p></div>
<div class="m2"><p>کین رقعه چگونه کرد پیدا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گرداند کس که چون جهان کرد</p></div>
<div class="m2"><p>ممکن که تواند آنچنان کرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چون وضع جهان ز ما محالست</p></div>
<div class="m2"><p>چونیش برون‌تر از خیالست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در پرده راز آسمانی</p></div>
<div class="m2"><p>سریست ز چشم ما نهانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چندانکه جنیبه رانم آنجا</p></div>
<div class="m2"><p>پی برد نمی‌توانم آنجا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در تخته هیکل رقومی</p></div>
<div class="m2"><p>خواندم همه نسخه نجومی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بر هر چه از آن برون کشیدم</p></div>
<div class="m2"><p>آرام گهی درون ندیدم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دانم که هر آنچه ساز کردند</p></div>
<div class="m2"><p>بر تعبیه‌ایش باز کردند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هرچ آن نظری در او توان بست</p></div>
<div class="m2"><p>پوشیده خزینه‌ای در آن هست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آن کن که کلید آن خزینه</p></div>
<div class="m2"><p>پولاد بود نه آبگینه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تا چون به خزینه در شتابی</p></div>
<div class="m2"><p>شربت طلبی نه زهر یابی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>پیرامن هر چه ناپدیدست</p></div>
<div class="m2"><p>جدول کش خود خطی کشیدست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>وآن خط که ز اوج بر گذشته</p></div>
<div class="m2"><p>عطفیست به میل بازگشته</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کاندیشه چو سر به خط رساند</p></div>
<div class="m2"><p>جز باز پس آمدن نداند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پرگار چو طوف ساز گردد</p></div>
<div class="m2"><p>در گام نخست باز گردد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>این حلقه که گرد خانه بستند</p></div>
<div class="m2"><p>از بهر چنین بهانه بستند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تا هر که ز حلقه بر کند سر</p></div>
<div class="m2"><p>سرگشته شود چو حلقه بر در</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>در سلسله فلک مزن دست</p></div>
<div class="m2"><p>کین سلسله را هم آخری هست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گر حکم طبایع است بگذار</p></div>
<div class="m2"><p>کو نیز رسد به آخر کار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بیرون‌تر ازین حواله گاهیست</p></div>
<div class="m2"><p>کانجا به طریق عجز راهیست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زان پرده نسیم ده نفس را</p></div>
<div class="m2"><p>کو پرده کژ نداد کس را</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>این هفت فلک به پرده سازی</p></div>
<div class="m2"><p>هست از جهت خیال بازی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زین پرده ترانه ساخت نتوان</p></div>
<div class="m2"><p>واین پرده به خود شناخت نتوان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گر پرده شناس ازین قیاسی</p></div>
<div class="m2"><p>هم پرده خود نمی‌شناسی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گر باربدی به لحن و آواز</p></div>
<div class="m2"><p>بی‌پرده مزن دمی بر این ساز</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>با پرده دریدگان خودبین</p></div>
<div class="m2"><p>در خلوت هیچ پرده منشین</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>آن پرده طلب که چون نظامی</p></div>
<div class="m2"><p>معروف شوی به نیکنامی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>تا چند زمین نهاد بودن</p></div>
<div class="m2"><p>سیلی خود خاک و باد بودن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چون باد دویدن از پی خاک</p></div>
<div class="m2"><p>مشغول شدن به خار و خاشاک</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بادی که وکیل خرج خاکست</p></div>
<div class="m2"><p>فراش گریوه مغاکست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بستاند ازین بدان سپارد</p></div>
<div class="m2"><p>گه مایه برد گهی بیارد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چندان که زمیست مرز بر مرز</p></div>
<div class="m2"><p>خاکیست نهاده درز بر درز</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>گه زلزله گاه سیل خیزد</p></div>
<div class="m2"><p>زین ساید خاک و زان بریزد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چون زلزله ریزد آب ساید</p></div>
<div class="m2"><p>درزی زخریطه واگشاید</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>وان درز به صدمه‌های ایام</p></div>
<div class="m2"><p>وادی کده‌ای شود سرانجام</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>جوئی که درین گل خرابست</p></div>
<div class="m2"><p>خاریده باد و چاک آبست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>از کوی زمین چو بگذری باز</p></div>
<div class="m2"><p>ابر و فلک است در تک و تاز</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>هر یک به میانه دگر شرط</p></div>
<div class="m2"><p>افتاده به شکل گوی در خرط</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>این شکل کری نه در زمین است</p></div>
<div class="m2"><p>هر خط که به گرد او چنین است</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>هر دود کزین مغاک خیزد</p></div>
<div class="m2"><p>تا یک دو سه نیزه بر ستیزد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>وآنگه به طریق میل ناکی</p></div>
<div class="m2"><p>گردد به طواف دیر خاکی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ابری که برآید از بیابان</p></div>
<div class="m2"><p>تا مصعد خود شود شتابان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بر اوج صعود خود بکوشد</p></div>
<div class="m2"><p>از حد صعود بر نجوشد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>او نیز طواف دیر گیرد</p></div>
<div class="m2"><p>از دایره میل می‌پذیرد</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بینیش چو خیمه ایستاده</p></div>
<div class="m2"><p>سر بر افق زمین نهاده</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>تا در نگری به کوچ و خیلش</p></div>
<div class="m2"><p>دانی که به دایره است میلش</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>هر جوهر فردکو بسیط است</p></div>
<div class="m2"><p>میلش به ولایت محیط است</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گردون که محیط هفت موج است</p></div>
<div class="m2"><p>چندان که همی‌رود در اوج است</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>گر در افق است و گر در اعلاست</p></div>
<div class="m2"><p>هرجا که رود به سوی بالاست</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>زآنجا که جهان خرامی اوست</p></div>
<div class="m2"><p>بالائی او تمامی اوست</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بالا طلبان که اوج جویند</p></div>
<div class="m2"><p>بالای فلک جز این نگویند</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>نز علم فلک گره گشائیست</p></div>
<div class="m2"><p>خود در همه علم روشنائیست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>گرمایه جویست ور پشیزی</p></div>
<div class="m2"><p>از چار گهر در اوست چیزی</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>اما نتوان نهفت آن جست</p></div>
<div class="m2"><p>کین دانه در آب و خاک چون رست</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>گرمایه زمین بدو رساند</p></div>
<div class="m2"><p>بخشیدن صورتش چه داند</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>وآنجا که زمین به زیر پی‌بود</p></div>
<div class="m2"><p>در دانه جمال خوشه کی بود</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>گیرم که ز دانه خوشه خیزد</p></div>
<div class="m2"><p>در قالب صورتش که ریزد</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>در پرده این خیال گردان</p></div>
<div class="m2"><p>آخر سببی است حال گردان</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>نزدیک تو آن سبب چه چیز است</p></div>
<div class="m2"><p>بنمای که این سخن عزیز است</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>داننده هر آن سبب که بیند</p></div>
<div class="m2"><p>داند که مسبب آفریند</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>زنهار نظامیا در این سیر</p></div>
<div class="m2"><p>پابست مشو به دام این دیر</p></div></div>