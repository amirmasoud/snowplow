---
title: >-
    بخش ۱۳ - در صفت عشق مجنون
---
# بخش ۱۳ - در صفت عشق مجنون

<div class="b" id="bn1"><div class="m1"><p>سلطان سریر صبح خیزان</p></div>
<div class="m2"><p>سر خیل سپاه اشک ریزان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>متواری راه دلنوازی</p></div>
<div class="m2"><p>زنجیری کوی عشقبازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قانون مغنینان بغداد</p></div>
<div class="m2"><p>بیاع معاملان فریاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طبال نفیر آهنین کوس</p></div>
<div class="m2"><p>رهبان کلیسیای افسوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جادوی نهفته دیو پیدا</p></div>
<div class="m2"><p>هاروت مشوشان شیدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کیخسرو بی کلاه و بی‌تخت</p></div>
<div class="m2"><p>دل خوش کن صدهزار بی رخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اقطاع ده سپاه موران</p></div>
<div class="m2"><p>اورنگ نشین پشت گوران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دراجه قلعه‌های وسواس</p></div>
<div class="m2"><p>دارنده پاس دیر بی‌پاس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجنون غریب دل شکسته</p></div>
<div class="m2"><p>دریای ز جوش نانشسته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یاری دو سه داشت دل رمیده</p></div>
<div class="m2"><p>چون او همه واقعه رسیده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با آن دو سه یار هر سحرگاه</p></div>
<div class="m2"><p>رفتی به طواف کوی آن ماه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیرون ز حساب نام لیلی</p></div>
<div class="m2"><p>با هیچ سخن نداشت میلی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرکس که جز این سخن گشادی</p></div>
<div class="m2"><p>نشنودی و پاسخش ندادی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن کوه که نجد بود نامش</p></div>
<div class="m2"><p>لیلی به قبیله هم مقامش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از آتش عشق و دود اندوه</p></div>
<div class="m2"><p>ساکن نشدی مگر بر آن کوه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر کوه شدی و میزدی دست</p></div>
<div class="m2"><p>افتان خیزان چو مردم مست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آواز نشید برکشیدی</p></div>
<div class="m2"><p>بی‌خود شده سو به سو دویدی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وانگه مژه را پر آب کردی</p></div>
<div class="m2"><p>با باد صبا خطاب کردی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کی باد صبا به صبح برخیز</p></div>
<div class="m2"><p>در دامن زلف لیلی آویز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گو آنکه به باد داده تست</p></div>
<div class="m2"><p>بر خاک ره اوفتاده تست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از باد صبا دم تو جوید</p></div>
<div class="m2"><p>با خاک زمین غم تو گوید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بادی بفرستش از دیارت</p></div>
<div class="m2"><p>خاکیش بده به یادگارت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر کو نه چو باد بر تو لرزد</p></div>
<div class="m2"><p>نه باد که خاک هم نیرزد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وانکس که نه جان به تو سپارد</p></div>
<div class="m2"><p>آن به که ز غصه جان برآرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر آتش عشق تو نبودی</p></div>
<div class="m2"><p>سیلاب غمت مرا ربودی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ور آب دو دیده نیستی یار</p></div>
<div class="m2"><p>دل سوختی آتش غمت زار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خورشید که او جهان فروزست</p></div>
<div class="m2"><p>از آه پرآتشم بسوزست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای شمع نهان خانه جان</p></div>
<div class="m2"><p>پروانه خویش را مرنجان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جادو چشم تو بست خوابم</p></div>
<div class="m2"><p>تا گشت چنین جگر کبابم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای درد و غم تو راحت دل</p></div>
<div class="m2"><p>هم مرهم و هم جراحت دل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>قند است لب تو گر توانی</p></div>
<div class="m2"><p>از وی قدری به من رسانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کاشفته گی مرا درین بند</p></div>
<div class="m2"><p>معجون مفرح آمد آن قند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هم چشم بدی رسید ناگاه</p></div>
<div class="m2"><p>کز چشم تو اوفتادم ای ماه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بس میوه آبدار چالاک</p></div>
<div class="m2"><p>کز چشم بد اوفتاد بر خاک</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>انگشت کش زمانه‌اش کشت</p></div>
<div class="m2"><p>زخمیست کشنده زخم انگشت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از چشم رسیدگی که هستم</p></div>
<div class="m2"><p>شد چون تو رسیده‌ای ز دستم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نیلی که کشند گرد رخسار</p></div>
<div class="m2"><p>هست از پی زخم چشم اغیار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خورشید که نیلگون حروفست</p></div>
<div class="m2"><p>هم چشم رسیده کسوفست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر گنج که برقعی نپوشد</p></div>
<div class="m2"><p>در بردن آن جهان بکوشد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>روزی که هوای پرنیان پوش</p></div>
<div class="m2"><p>خلخال فلک نهاد بر گوش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سیماب ستارها در آن صرف</p></div>
<div class="m2"><p>شد ز آتش آفتاب شنگرف</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مجنون رمیده دل چو سیماب</p></div>
<div class="m2"><p>با آن دو سه یار ناز برتاب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آمد به دیار یار پویان</p></div>
<div class="m2"><p>لبیک زنان و بیت گویان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>می‌شد سوی یار دل رمیده</p></div>
<div class="m2"><p>پیراهن صابری دریده</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>می‌گشت به گرد خرمن دل</p></div>
<div class="m2"><p>می‌دوخت دریده دامن دل</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>می‌رفت نوان چو مردم مست</p></div>
<div class="m2"><p>می‌زد به سر و به روی بر دست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون کار دلش ز دست بگذشت</p></div>
<div class="m2"><p>بر خرگه یار مست بگذشت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بر رسم عرب نشسته آنماه</p></div>
<div class="m2"><p>بر بسته ز در شکنج خرگاه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آن دید درین و حسرتی خورد</p></div>
<div class="m2"><p>وین دید در آن و نوحه‌ای کرد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>لیلی چو ستاره در عماری</p></div>
<div class="m2"><p>مجنون چو فلک به پرده‌داری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>لیلی کله بند باز کرده</p></div>
<div class="m2"><p>مجنون گله‌ها دراز کرده</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>لیلی ز خروش چنگ در بر</p></div>
<div class="m2"><p>مجنون چو رباب دست بر سر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>لیلی نه که صبح گیتی افروز</p></div>
<div class="m2"><p>مجنون نه که شمع خویشتن سوز</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>لیلی بگذار باغ در باغ</p></div>
<div class="m2"><p>مجنون غلطم که داغ بر داغ</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>لیلی چو قمر به روشنی چست</p></div>
<div class="m2"><p>مجنون چو قصب برابرش سست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>لیلی به درخت گل نشاندن</p></div>
<div class="m2"><p>مجنون به نثار در فشاندن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>لیلی چه سخن؟ پری فشی بود</p></div>
<div class="m2"><p>مجنون چه حکایت؟ آتشی بود</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>لیلی سمن خزان ندیده</p></div>
<div class="m2"><p>مجنون چمن خزان رسیده</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>لیلی دم صبح پیش می‌برد</p></div>
<div class="m2"><p>مجنون چو چراغ پیش می‌مرد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>لیلی به کرشمه زلف بر دوش</p></div>
<div class="m2"><p>مجنون به وفاش حلقه در گوش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>لیلی به صبوح جان نوازی</p></div>
<div class="m2"><p>مجنون به سماع خرقه بازی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>لیلی ز درون پرند می‌دوخت</p></div>
<div class="m2"><p>مجنون ز برون سپند می‌سوخت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>لیلی چو گل شکفته می‌رست</p></div>
<div class="m2"><p>مجنون به گلاب دیده می‌شست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>لیلی سر زلف شانه می‌کرد</p></div>
<div class="m2"><p>مجنون در اشک دانه می‌کرد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>لیلی می مشگبوی در دست</p></div>
<div class="m2"><p>مجنون نه ز می ز بوی می مست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>قانع شده این از آن به بوئی</p></div>
<div class="m2"><p>وآن راضی از این به جستجوئی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>از بیم تجسس رقیبان</p></div>
<div class="m2"><p>سازنده ز دور چون غریبان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تا چرخ بدین بهانه برخاست</p></div>
<div class="m2"><p>کان یک نظر از میانه برخاست</p></div></div>