---
title: >-
    بخش ۲۳ - عتاب کردن مجنون با نوفل
---
# بخش ۲۳ - عتاب کردن مجنون با نوفل

<div class="b" id="bn1"><div class="m1"><p>مجنون چو شنید بوی آزرم</p></div>
<div class="m2"><p>کرد از سر کین کمیت را گرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بانوفل تیغ‌زن برآشفت</p></div>
<div class="m2"><p>کی از تو رسیده جفت با جفت!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>احسنت زهی امیدواری</p></div>
<div class="m2"><p>به زین نبود تمام کاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این بود بلندی کلاهت؟</p></div>
<div class="m2"><p>شمشیر کشیدن سپاهت؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این بود حساب زورمندیت؟</p></div>
<div class="m2"><p>وین بود فسون دیو بندیت؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جولان زدن سمندت این بود؟</p></div>
<div class="m2"><p>انداختن کمندت این بود؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رایت که خلاف رای من کرد</p></div>
<div class="m2"><p>نیکو هنری به جای من کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن دوست که بد سلام دشمن</p></div>
<div class="m2"><p>کردیش کنون تمام دشمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وان در که بد از وفا پرستی</p></div>
<div class="m2"><p>بر من به هزار قفل بستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از یاری تو بریدم ای یار</p></div>
<div class="m2"><p>بردی زه کار من زهی کار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس رشته که بگسلد زیاری</p></div>
<div class="m2"><p>بس قایم کافتد از سواری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس تیر شبان که در تک افتاد</p></div>
<div class="m2"><p>بر گرگ فکند و بر سگ افتاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرچه کرمت بلند نامست</p></div>
<div class="m2"><p>در عهده عهد ناتمامست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نوفل سپر افکنان ز حربش</p></div>
<div class="m2"><p>بنواخت به رفقهای چربش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کز بی‌مددی و بی‌سپاهی</p></div>
<div class="m2"><p>کردم به فریب صلح خواهی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اکنون که به جای خود رسیدم</p></div>
<div class="m2"><p>نز تیغ برنده خو بریدم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>لشگر ز قبیله‌ها بخوانم</p></div>
<div class="m2"><p>پولاد به سنگ درنشانم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ننشینم تا به زخم شمشیر</p></div>
<div class="m2"><p>این یاوه ز بام ناورم زیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وآنگه ز مدینه تا به بغداد</p></div>
<div class="m2"><p>در جمع سپاه کس فرستاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در جستن کین ز هر دیاری</p></div>
<div class="m2"><p>لشگر طلبید روزگاری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آورد به هم سپاهی انبوه</p></div>
<div class="m2"><p>پس پره کشید کوه تا کوه</p></div></div>