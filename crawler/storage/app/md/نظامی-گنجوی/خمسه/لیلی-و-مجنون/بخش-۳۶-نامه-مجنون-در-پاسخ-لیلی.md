---
title: >-
    بخش ۳۶ - نامه مجنون در پاسخ لیلی
---
# بخش ۳۶ - نامه مجنون در پاسخ لیلی

<div class="b" id="bn1"><div class="m1"><p>بود اول آن خجسته پرگار</p></div>
<div class="m2"><p>نام ملکی که نیستش یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانای نهان و آشکارا</p></div>
<div class="m2"><p>کو داد گهر به سنگ خارا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارای سپهر و اخترانش</p></div>
<div class="m2"><p>دارنده نعش و دخترانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بینا کن دل به آشنائی</p></div>
<div class="m2"><p>روز آور شب به روشنائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیراب کن بهار خندان</p></div>
<div class="m2"><p>فریادرس نیازمندان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وانگه ز جگر کبابی خویش</p></div>
<div class="m2"><p>گفته سخن خرابی خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاین نامه ز من که بی‌قرارم</p></div>
<div class="m2"><p>نزدیک تو ای قرار کارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نی نی غلطم ز خون بجوشی</p></div>
<div class="m2"><p>وانگه به کجا به خون فروشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یعنی ز من کلید در سنگ</p></div>
<div class="m2"><p>نزدیک تو ای خزینه در چنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من خاک توام بدین خرابی</p></div>
<div class="m2"><p>تو آب کیی که روشن آیی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من در قدم تو می‌شوم پست</p></div>
<div class="m2"><p>تو در کمر که می‌زنی دست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من درد ستان تو نهانی</p></div>
<div class="m2"><p>تو درد دل که می‌ستانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من غاشیه تو بسته بر دوش</p></div>
<div class="m2"><p>تو حلقه کی نهاده در گوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای کعبه من جمال رویت</p></div>
<div class="m2"><p>محراب من آستان کویت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای مرهم صد هزار سینه</p></div>
<div class="m2"><p>درد من و می در آبگینه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای تاج ولی نه بر سر من</p></div>
<div class="m2"><p>تاراج تو لیک در بر من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای گنج ولی به دست اغیار</p></div>
<div class="m2"><p>زان گنج به دست دوستان مار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای باغ ارم به بی کلیدی</p></div>
<div class="m2"><p>فردوس فلک به ناپدیدی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای بند مرا مفتح از تو</p></div>
<div class="m2"><p>سودای مرا مفرح از تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>این چوب که عود بیشه تست</p></div>
<div class="m2"><p>مشکن که هلاک تیشه تست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بنواز مرا مزن که خاکم</p></div>
<div class="m2"><p>افروخته کن که گردناکم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر بنوازی بهارت آرم</p></div>
<div class="m2"><p>ور زخم زنی غبارت آرم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لطفست به جای خاک در خورد</p></div>
<div class="m2"><p>کز لطف گل آید از جفا گرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در پای توام به سرفشانی</p></div>
<div class="m2"><p>همسر مکنم به سرگرانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون برخیزد طریق آزرم</p></div>
<div class="m2"><p>گردد همه شرمناک بی‌شرم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هستم به غلامی تو مشهور</p></div>
<div class="m2"><p>خصمم کنی ار کنی ز خود دور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>من در ره بندگی کشم بار</p></div>
<div class="m2"><p>تو پایه خواجگی نگه‌دار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با تو سپرم میفکنم زیر</p></div>
<div class="m2"><p>چون بفکنیم شوم به شمشیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر آلت خویشتن مزن سنگ</p></div>
<div class="m2"><p>با لشگر خویشتن مکن جنگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون بر تن خویشتن زنی نیش</p></div>
<div class="m2"><p>اندام درست را کنی ریش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن کن که به رفق و دلنوازی</p></div>
<div class="m2"><p>آزادان را به بنده سازی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آن به که درم خریده تو</p></div>
<div class="m2"><p>سرمه نبرد ز دیده تو</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر خواجه که این کفایتش نیست</p></div>
<div class="m2"><p>بر بنده خود ولایتش نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وان کس که بدین هنر تمامست</p></div>
<div class="m2"><p>نخریده ورا بسی غلامست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هستم چو غلام حلقه در گوش</p></div>
<div class="m2"><p>می‌دار به بندگیم و مفروش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ای در کنف دگر خزیده</p></div>
<div class="m2"><p>جفتی به مراد خود گزیده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نگشاده فقاعی از سلامم</p></div>
<div class="m2"><p>بر تخته یخ نوشته نامم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یک نعل بر ابرشم ندادی</p></div>
<div class="m2"><p>صد نعل در آتشم نهادی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>روزم چو شب سیاه کردی</p></div>
<div class="m2"><p>هم زخم زدی هم آه کردی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در دل ستدن ندادیم داد</p></div>
<div class="m2"><p>گر جان ببری کی آریم یاد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زخمی به زبان همی فروشی</p></div>
<div class="m2"><p>من سوختم و تو بر نجوشی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نه هر که زبان دراز دارد</p></div>
<div class="m2"><p>زخم از تن خویش باز دارد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سوسن ز سر زبان درازی</p></div>
<div class="m2"><p>شد در سر تیغ و تیغ بازی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>یاری که بود مرا خریدار</p></div>
<div class="m2"><p>هم بر رخ او بود پدیدار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آنچ از تو مرا در این مقامست</p></div>
<div class="m2"><p>بنمای مرا که تا کدامست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>این است که عهد من شکستی؟</p></div>
<div class="m2"><p>در عهده دیگری نشستی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>با من به زبان فریب سازی</p></div>
<div class="m2"><p>با او به مراد عشق بازی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گر عاشقی آه صادقت کو</p></div>
<div class="m2"><p>با من نفس موافقت کو</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در عشق تو چون موافقی نیست</p></div>
<div class="m2"><p>این سلطنتست عاشقی نیست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تو فارغ از آنکه بی دلی هست</p></div>
<div class="m2"><p>و اندوه ترا معاملی هست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>من دیده به روی تو گشاده</p></div>
<div class="m2"><p>سر بر سر کوی تو نهاده</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بر قرعه چار حد کویت</p></div>
<div class="m2"><p>فالی زنم از برای رویت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>آسوده کسی که در تو بیند</p></div>
<div class="m2"><p>نه آنکه بروز من نشیند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خرم نه مرا توانگری را</p></div>
<div class="m2"><p>کو دارد چون تو گوهری را</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>باغ ارچه ز بلبلان پرآبست</p></div>
<div class="m2"><p>انجیر نواله غرابست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>آب از دل باغبان خورد نار</p></div>
<div class="m2"><p>باشد که خورد چو نقل بیمار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دیریست که تا جهان چنین است</p></div>
<div class="m2"><p>محتاج تو گنج در زمین است</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کی می‌بینم که لعل گلرنگ؟</p></div>
<div class="m2"><p>بیرون جهد از شکنجه سنگ</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>وآنماه کز اوست دیده را نور</p></div>
<div class="m2"><p>گردد ز دهان اژدها دور</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زنبور پریده شهد مانده</p></div>
<div class="m2"><p>خازن شده ماه و مهد مانده</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بگشاده خزینه وز حصارش</p></div>
<div class="m2"><p>افتاده به در خزینه دارش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز آیینه غبار زنگ برده</p></div>
<div class="m2"><p>گنجینه به جای و مار مرده</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>دزبانوی من ز دز گشاده</p></div>
<div class="m2"><p>دزبان وی از دز اوفتاده</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گر من شدم از چراغ تو دور</p></div>
<div class="m2"><p>پروانه تو مباد بی‌نور</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گر کشت مرا غم ملامت</p></div>
<div class="m2"><p>باد ابن‌سلام را سلامت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ای نیک و بد مزاجم از تو</p></div>
<div class="m2"><p>دردم ز تو و علاجم از تو</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هرچند حصارت آهنین است</p></div>
<div class="m2"><p>لؤلؤی ترت صدف نشین است</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>وز حلقه زلف پر شکنجت</p></div>
<div class="m2"><p>در دامن اژدهاست گنجت</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دانی که ز دوستاری خویش</p></div>
<div class="m2"><p>باشد دل دوستان بداندیش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بر من ز تو صد هوس نشیند</p></div>
<div class="m2"><p>گر بر تو یکی مگس نشیند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>زان عاشق کورتر کسی نیست</p></div>
<div class="m2"><p>کورا مگسی چو کرکسی نیست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چون مورچه بی‌قرار از آنم</p></div>
<div class="m2"><p>تا آن مگس از شکر برانم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>این آن مثل است کان جوانمرد</p></div>
<div class="m2"><p>بی‌مایه حساب سود می‌کرد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>اندوه گل نچیده می‌داشت</p></div>
<div class="m2"><p>پاس در ناخریده می‌داشت</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بگذشت ز عشقت ای سمنبر</p></div>
<div class="m2"><p>کار از لب خشک و دیده تر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>شوریده‌ترم از آنچه دیدی</p></div>
<div class="m2"><p>مجنون‌تر از آنکه می‌شنیدی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>با تو خودی من از میان رفت</p></div>
<div class="m2"><p>و این راه به بی‌خودی توان رفت</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>عشقی که دل اینچنین نورزد</p></div>
<div class="m2"><p>در مذهب عشق جو نیرزد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چون عشق تو روی می‌نماید</p></div>
<div class="m2"><p>گر روی تو غایب است شاید</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>عشق تو رقیب راز من باد</p></div>
<div class="m2"><p>زخم تو جگر نواز من باد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>با زخم من ارچه مرهمی نیست</p></div>
<div class="m2"><p>چون تو به سلامتی غمی نیست</p></div></div>