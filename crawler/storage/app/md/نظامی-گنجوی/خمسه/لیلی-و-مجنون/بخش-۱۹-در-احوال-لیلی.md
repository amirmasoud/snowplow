---
title: >-
    بخش ۱۹ - در احوال لیلی
---
# بخش ۱۹ - در احوال لیلی

<div class="b" id="bn1"><div class="m1"><p>سر دفتر آیت نکوئی</p></div>
<div class="m2"><p>شاهنشه ملک خوبروئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فهرست جمال هفت پرگار</p></div>
<div class="m2"><p>از هفت خلیفه جامگی خوار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رشک رخ ماه آسمانی</p></div>
<div class="m2"><p>رنج دل سرو بوستانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منصوبه گشای بیم و امید</p></div>
<div class="m2"><p>میراث ستان ماه و خورشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محراب نماز بت‌پرستان</p></div>
<div class="m2"><p>قندیل سرای و سرو بستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم خوابه عشق و هم سرناز</p></div>
<div class="m2"><p>هم خازن و هم خزینه پرداز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیرایه گر پرند پوشان</p></div>
<div class="m2"><p>سرمایه ده شکر فروشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل‌بند هزار در مکنون</p></div>
<div class="m2"><p>زنجیر بر هزار مجنون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لیلی که بخوبی آیتی بود</p></div>
<div class="m2"><p>وانگشت کش ولایتی بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیراب گلش پیاله در دست</p></div>
<div class="m2"><p>از غنچه نوبری برون جست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرو سهیش کشیده‌تر شد</p></div>
<div class="m2"><p>میگون رطبش رسیده‌تر شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می‌رست به باغ دل فروزی</p></div>
<div class="m2"><p>می‌کرد به غمزه خلق سوزی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از جادوئی که در نظر داشت</p></div>
<div class="m2"><p>صد ملک بنیم غمزه برداشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می‌کرد بوقت غمزه سازی</p></div>
<div class="m2"><p>برتازی و ترک ترکتازی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صیدی ز کمند او نمی‌رست</p></div>
<div class="m2"><p>غمزش بگرفت و زلف می‌بست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از آهوی چشم نافه‌وارش</p></div>
<div class="m2"><p>هم نافه هم آهوان شکارش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وز حلقه زلف وقت نخجیر</p></div>
<div class="m2"><p>بر گردن شیر بست زنجیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از چهره گل از لب انگبین کرد</p></div>
<div class="m2"><p>کان دید طبرزد آفرین کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دلداده هزار نازنینش</p></div>
<div class="m2"><p>در آرزوی گل انگبینش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زلفش ره بوسه خواه می‌رفت</p></div>
<div class="m2"><p>مژگانش خدادهاد می‌گفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زلفش به کمند پیش می‌خواند</p></div>
<div class="m2"><p>مژگانش به دور باش می‌راند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برده بدو رخ ز ماه بیشی</p></div>
<div class="m2"><p>گل را دو پیاده داده پیشی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قدش چو کشیده زاد سروی</p></div>
<div class="m2"><p>رویش چو به سرو بر تذروی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لبهاش که خنده بر شکرزد</p></div>
<div class="m2"><p>انگشت کشیده بر طبرزد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لعلش که حدیث بوس می‌کرد</p></div>
<div class="m2"><p>بر تنگ شکر فسوس می‌کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چاه زنخش که سر گشاده</p></div>
<div class="m2"><p>صد دل به غلط در او فتاده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زلفش رسنی فکنده در راه</p></div>
<div class="m2"><p>تا هر که فتد برآرد از چاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با اینهمه ناز و دلستانی</p></div>
<div class="m2"><p>خون شد جگرش ز مهربانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در پرده که راه بود بسته</p></div>
<div class="m2"><p>می‌بود چو پرده بر شکسته</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>می‌رفت نهفته بر سر بام</p></div>
<div class="m2"><p>نظاره‌کنان ز صبح تا شام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا مجنون را چگونه بیند</p></div>
<div class="m2"><p>با او نفسی کجا نشیند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>او را به کدام دیده جوید</p></div>
<div class="m2"><p>با او غم دل چگونه گوید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از بیم رقیب و ترس بدخواه</p></div>
<div class="m2"><p>پوشیده بنیم شب زدی آه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون شمع به زهر خنده می‌زیست</p></div>
<div class="m2"><p>شیرین خندید و تلخ بگریست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گل را به سرشک می‌خراشید</p></div>
<div class="m2"><p>وز چوب رفیق می‌تراشید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>می‌سوخت به آتش جدائی</p></div>
<div class="m2"><p>نه دود در او نه روشنائی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آیینه درد پیش می‌داشت</p></div>
<div class="m2"><p>مونس ز خیال خویش می‌داشت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پیدا شغبی چو باد می‌کرد</p></div>
<div class="m2"><p>پنهان جگری چو خاک می‌خورد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جز سایه نبود پرده‌دارش</p></div>
<div class="m2"><p>جز پرده کسی نه غمگسارش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از بس که به سایه راز می‌گفت</p></div>
<div class="m2"><p>همسایه او به شب نمی‌خفت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>می‌ساخت میان آب و آتش</p></div>
<div class="m2"><p>گفتی که پریست آن پریوش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خنیاگر زن صریر دوک است</p></div>
<div class="m2"><p>تیر آلت جعبه ملوکست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>او دوک دو سرفکنده از چنگ</p></div>
<div class="m2"><p>برداشته تیر یکسر آهنگ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از یک سر تیر کارگر شد</p></div>
<div class="m2"><p>سرگردان دوک از آن دو سر شد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دریا دریا گهر بر آهیخت</p></div>
<div class="m2"><p>کشتی کشتی زدیده می‌ریخت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>می‌خورد غمی به زیر پرده</p></div>
<div class="m2"><p>غم خورده ورا و غم نخورده</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در گوش نهاده به زیر پرده</p></div>
<div class="m2"><p>چون حلقه نهاده گوش بر در</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>با حلقه گوش خویش می‌ساخت</p></div>
<div class="m2"><p>وان حلقه به گوش کس نینداخت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در جستن نور چشمه ماه</p></div>
<div class="m2"><p>چون چشمه بمانده چشم بر راه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تا خود که بدو پیامی آرد</p></div>
<div class="m2"><p>زآرام دلش سلامی آرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بادی که ز نجد بردمیدی</p></div>
<div class="m2"><p>جز بوی وفا در او ندیدی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>وابری که از آن طرف گشادی</p></div>
<div class="m2"><p>جز آب لطف بدو ندادی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هرجا که ز کنج خانه می‌دید</p></div>
<div class="m2"><p>بر خود غزلی روانه می‌دید</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هر طفل که آمدی ز بازار</p></div>
<div class="m2"><p>بیتی گفتی نشانده‌بر کار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هرکس که گذشت زیر بامش</p></div>
<div class="m2"><p>می‌داد به بیتکی پیامش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>لیلی که چنان ملاحتی داشت</p></div>
<div class="m2"><p>در نظم سخن فصاحتی داشت</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ناسفته دری و در همی سفت</p></div>
<div class="m2"><p>چون خود همه بیت بکر می‌گفت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بیتی که ز حسب حال مجنون</p></div>
<div class="m2"><p>خواندی به مثل چو در مکنون</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>آنرا دگری جواب گفتی</p></div>
<div class="m2"><p>آتش بشنیدی آب گفتی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>پنهان ورقی به خون سرشتی</p></div>
<div class="m2"><p>وان بیتک را بر او نوشتی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بر راهگذر فکندی از بام</p></div>
<div class="m2"><p>دادی ز سمن به سرو پیغام</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آن رقعه کسی که بر گرفتی</p></div>
<div class="m2"><p>برخواندی و رقص در گرفتی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بردی و بدان غریب دادی</p></div>
<div class="m2"><p>کز وی سخن غریب زادی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>او نیز بدیهه‌ای روانه</p></div>
<div class="m2"><p>گفتی به نشان آن نشانه</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>زین گونه میان آن دو دلبند</p></div>
<div class="m2"><p>می‌رفت پیام گونه‌ای چند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>زاوازه آن دو بلبل مست</p></div>
<div class="m2"><p>هر بلبله‌ای که بود بشکست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>زان هردو بریشم خوش آواز</p></div>
<div class="m2"><p>بر ساز بسی بریشم ساز</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بر رورد رباب و ناله چنگ</p></div>
<div class="m2"><p>یک رنگ نوای آن دو آهنگ</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>زایشان سخنی به نکته راندن</p></div>
<div class="m2"><p>وز چنگ زدن ز نای خواندن</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>از نغمه آن دو هم ترانه</p></div>
<div class="m2"><p>مطرب شده کودکان خانه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>خصمان در طعنه باز کردند</p></div>
<div class="m2"><p>در هر دو زبان دراز کردند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>وایشان ز بد گزاف گویان</p></div>
<div class="m2"><p>خود را به سرشک دیده شویان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بودند بر این طریق سالی</p></div>
<div class="m2"><p>قانع به خیال و چون خیالی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چون پرده کشید گل به صحرا</p></div>
<div class="m2"><p>شد خاک به روی گل مطرا</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>خندید شکوفه بر درختان</p></div>
<div class="m2"><p>چون سکه روی نیکبختان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>از لاله سرخ و از گل زرد</p></div>
<div class="m2"><p>گیتی علم دو رنگ بر کرد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>از برگ و نوا به باغ و بستان</p></div>
<div class="m2"><p>با برگ و نوا هزار دستان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>سیرابی سبزه‌های نوخیز</p></div>
<div class="m2"><p>از لولو تر زمرد انگیز</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>لاله ز ورق فشانده شنگرف</p></div>
<div class="m2"><p>کافتاده سیاهیش بر آن حرف</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>زلفین بنفشه از درازی</p></div>
<div class="m2"><p>در پای فتاده وقت بازی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>غنچه کمر استوار می‌کرد</p></div>
<div class="m2"><p>پیکان کشیی ز خار می‌کرد</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>گل یافت ستبرق حریری</p></div>
<div class="m2"><p>شد باد به گوشواره‌گیری</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نیلوفر از آفتاب گلرنگ</p></div>
<div class="m2"><p>بر آب سپر فکند بی جنگ</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>سنبل سر نافه باز کرده</p></div>
<div class="m2"><p>گل دست بدو دراز کرده</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>شمشاد به جعد شانه کردن</p></div>
<div class="m2"><p>گلنار به نار دانه کردن</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>نرگس ز دماغ آتشین تاب</p></div>
<div class="m2"><p>چون تب زدگان بجسته از خواب</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>خورشید ز قطره‌های باده</p></div>
<div class="m2"><p>خون از رگ ارغوان گشاده</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>زان چشمه سیم کز سمن رست</p></div>
<div class="m2"><p>نسرین ورقی که داشت می‌شست</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>گل دیده ببوس باز می‌کرد</p></div>
<div class="m2"><p>چون مثل ندید ناز می‌کرد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>سوسن نه زبان که تیغ در بر</p></div>
<div class="m2"><p>نی نی غلطم که تیغ بر سر</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>مرغان زبان گرفته چون زاغ</p></div>
<div class="m2"><p>بگشاده زبان مرغ در باغ</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>دراج زدل کبابی انگیخت</p></div>
<div class="m2"><p>قمری نمکی ز سینه می‌ریخت</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>هر فاخته بر سر چناری</p></div>
<div class="m2"><p>در زمزمه حدیث یاری</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بلبل ز درخت سرکشیده</p></div>
<div class="m2"><p>مجنون صفت آه برکشیدی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>گل چون رخ لیلی از عماری</p></div>
<div class="m2"><p>بیرون زده سر به تاجداری</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>در فصل گلی چنین همایون</p></div>
<div class="m2"><p>لیلی ز وثاق رفت بیرون</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بند سر زلف تاب داده</p></div>
<div class="m2"><p>گلراز بنفشه آب داده</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>از نوش لبان آن قبیله</p></div>
<div class="m2"><p>گردش چو گهر یکی طویله</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ترکان عرب نشینشان نام</p></div>
<div class="m2"><p>خوش باشد ترک‌تازی اندام</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>در حلقه آن بتان چون حور</p></div>
<div class="m2"><p>می‌رفت چنانکه چشم بد دور</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>تا سبزه باغ را به بیند</p></div>
<div class="m2"><p>در سایه سرخ گل نشیند</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>با نرگس تازه جام گیرد</p></div>
<div class="m2"><p>با لاله نبید خام گیرد</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>از زلف دهد بنفشه را تاب</p></div>
<div class="m2"><p>وز چهره گل شکفته را آب</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>آموزد سرو را سواری</p></div>
<div class="m2"><p>شوید ز سمن سپید کاری</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>از نافه غنچه باج خواهد</p></div>
<div class="m2"><p>وز ملک چمن خراج خواهد</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بر سبزه ز سایه نخل بندد</p></div>
<div class="m2"><p>بر صورت سرو و گل بخندد</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>نه‌نه غرضش نه این سخن بود</p></div>
<div class="m2"><p>نه سرو و گل و نه نسترن بود</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>بودش غرض آنکه در پناهی</p></div>
<div class="m2"><p>چون سوختگان برآرد آهی</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>با بلبل مست راز گوید</p></div>
<div class="m2"><p>غمهای گذشته باز گوید</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>یابد ز نسیم گلستانی</p></div>
<div class="m2"><p>از یار غریب خود نشانی</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>باشد که دلش گشاده گردد</p></div>
<div class="m2"><p>باری ز دلش فتاده گردد</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>نخلستانی بدان زمین بود</p></div>
<div class="m2"><p>کارایش نقشبند چین بود</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>از حله به حله نخل گاهش</p></div>
<div class="m2"><p>در باغ ارم گشاده راهش</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>نزهت گاهی چنان گزیده</p></div>
<div class="m2"><p>در بادیه چشم کس ندیده</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>لیلی و دگر عروس نامان</p></div>
<div class="m2"><p>رفتند بدان چمن خرامان</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چون گل به میان سبزه بنشست</p></div>
<div class="m2"><p>بر سبزه ز سایه گل همی‌بست</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>هرجا که نسیم او درآمد</p></div>
<div class="m2"><p>سوسن بشکفت و گل برآمد</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>بر هر چمنی که دست می‌شست</p></div>
<div class="m2"><p>شمشاد دمید و سرو می‌رست</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>با سرو بنان لاله رخسار</p></div>
<div class="m2"><p>آمد به نشاط و خنده در کار</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>تا یک چندی نشاط می‌ساخت</p></div>
<div class="m2"><p>آخر ز نشاطگه برون تاخت</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>تنها بنشست زیر سروی</p></div>
<div class="m2"><p>چون بر پر طوطیی تذروی</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>بر سبزه نشسته خرمن گل</p></div>
<div class="m2"><p>نالید چو در بهار بلبل</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>نالید و بناله در نهانی</p></div>
<div class="m2"><p>می‌گفت ز روی مهربانی</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>کای یار موافق وفادار</p></div>
<div class="m2"><p>وی چون من وهم به من سزاوار</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>ای سرو جوانه جوانمرد</p></div>
<div class="m2"><p>وی با دل گرم و با دم سرد</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>آی از در آنکه در چنین باغ</p></div>
<div class="m2"><p>آیی و زدائی از دلم داغ</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>با من به مراد دل نشینی</p></div>
<div class="m2"><p>من نارون و تو سرو بینی</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>گیرم ز منت فراغ من نیست</p></div>
<div class="m2"><p>پروای سرای و باغ من نیست</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>آخر به زبان نیکنامی</p></div>
<div class="m2"><p>کم زآنکه فرستیم پیامی؟</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>ناکرده سخن هنوز پرواز</p></div>
<div class="m2"><p>کز رهگذری برآمد آواز</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>شخصی غزلی چو در مکنون</p></div>
<div class="m2"><p>می‌خواند ز گفتهای مجنون</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>کی پرده در صلاح کارم</p></div>
<div class="m2"><p>امید تو باد پرده دارم</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>مجنون به میان موج خونست</p></div>
<div class="m2"><p>لیلی به حساب کار چونست</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>مجنون جگری همی‌خراشد</p></div>
<div class="m2"><p>ثلیلی نمک از که می‌تراشد</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>مجنون به خدنگ خار سفته است</p></div>
<div class="m2"><p>لیلی به کدام ناز خفته است</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>مجنون به هزار نوحه نالد</p></div>
<div class="m2"><p>لیلی چه نشاط می‌سکالد</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>مجنون همه درد و داغ دارد</p></div>
<div class="m2"><p>لیلی چه بهار و باغ دارد</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>مجنون کمر نیاز بندد</p></div>
<div class="m2"><p>لیلی به رخ که باز خندد</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>مجنون ز فراق دل رمیداست</p></div>
<div class="m2"><p>لیلی به چه راحت آرمید است</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>لیلی چو سماع این غزل کرد</p></div>
<div class="m2"><p>بگریست وز گریه سنگ حل کرد</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>زانسرو بنان بوستانی</p></div>
<div class="m2"><p>می‌دید در او یکی نهانی</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>کز دوری دوست بر چه سانست</p></div>
<div class="m2"><p>بر دوست چگونه مهربانست</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>چون باز شدند سوی خانه</p></div>
<div class="m2"><p>شد در صدف آن در یگانه</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>داننده راز راز ننهفت</p></div>
<div class="m2"><p>با مادرش آنچه دید بر گفت</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>تا مادر مشفقش نوازد</p></div>
<div class="m2"><p>در چاره گریش چاره سازد</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>مادر ز پی عروس ناکام</p></div>
<div class="m2"><p>سرگشته شده چو مرغ در دام</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>می‌گفت گرش گذارم از دست</p></div>
<div class="m2"><p>آن شیفته گشت و این شود مست</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>ور صابریی بدو نمایم</p></div>
<div class="m2"><p>بر ناید ازو وزو برآیم</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>بر حسرت او دریغ می‌خورد</p></div>
<div class="m2"><p>می‌خورد دریغ و صبر می‌کرد</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>لیلی که چو گنج شد حصاری</p></div>
<div class="m2"><p>می‌بود چو ماه در عماری</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>می‌زد نفسی گرفته چون میغ</p></div>
<div class="m2"><p>می‌خورد غمی نهفته چون تیغ</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>دلتنگ چنانکه بود می‌زیست</p></div>
<div class="m2"><p>بی‌تنگ دلی به عشق در کیست</p></div></div>