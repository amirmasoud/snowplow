---
title: >-
    بخش ۴۴ - صفت رسیدن خزان و در گذشتن لیلی
---
# بخش ۴۴ - صفت رسیدن خزان و در گذشتن لیلی

<div class="b" id="bn1"><div class="m1"><p>شرطست که وقت برگ‌ریزان</p></div>
<div class="m2"><p>خونابه شود ز برگ‌ریزان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خونی که بود درون هر شاخ</p></div>
<div class="m2"><p>بیرون چکد از مسام سوراخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قاروره آب سرد گردد</p></div>
<div class="m2"><p>رخساره باغ زرد گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاخ آبله هلاک یابد</p></div>
<div class="m2"><p>زر جوید برگ و خاک یابد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرگس به جمازه بر نهد رخت</p></div>
<div class="m2"><p>شمشاد در افتد از سر تخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیمای سمن شکست گیرد</p></div>
<div class="m2"><p>گل نامه غم به دست گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر فرق چمن کلاله خاک</p></div>
<div class="m2"><p>پیچیده شود چو مار ضحاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون باد مخالف آید از دور</p></div>
<div class="m2"><p>افتادن برگ هست معذور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کانان که ز غرقگه گریزند</p></div>
<div class="m2"><p>ز اندیشه باد رخت ریزند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نازک جگران باغ رنجور</p></div>
<div class="m2"><p>شیرین نمکان تاک مخمور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>انداخته هندوی کدیور</p></div>
<div class="m2"><p>زنگی بچگان تاک را سر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرهای تهی ز طره کاخ</p></div>
<div class="m2"><p>آویخته هم به طره شاخ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیب از زنخی بدان نگونی</p></div>
<div class="m2"><p>بر نار زنخ زنان که چونی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نار از جگر کفیده خویش</p></div>
<div class="m2"><p>خونابه چکانده بر دل ریش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر پسته که شد دهن دریده</p></div>
<div class="m2"><p>عناب ز دور لب گزیده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در معرکه چنین خزانی</p></div>
<div class="m2"><p>شد زخم رسیده گلستانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>لیلی ز سریر سر بلندی</p></div>
<div class="m2"><p>افتاد به چاه دردمندی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شد چشم زده بهار باغش</p></div>
<div class="m2"><p>زد باد تپانچه بر چراغش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن سر که عصابهای زر بست</p></div>
<div class="m2"><p>خود را به عصا به دگر بست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گشت آن تن نازک قصب پوش</p></div>
<div class="m2"><p>چون تار قصب ضعیف و بی‌توش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شد بدر مهیش چون هلالی</p></div>
<div class="m2"><p>وان سرو سهیش چون خیالی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سودای دلش به سر درآمد</p></div>
<div class="m2"><p>سرسام سرش به دل برآمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرمای تموز ژاله را برد</p></div>
<div class="m2"><p>باد آمد و برگ لاله را برد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تب لرزه شکست پیکرش را</p></div>
<div class="m2"><p>تبخاله گزید شکرش را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بالین طلبید زاد سروش</p></div>
<div class="m2"><p>وز سرو فتاده شد تذروش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>افتاد چنانکه دانه از کشت</p></div>
<div class="m2"><p>سر بند قصب به رخ فرو هشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر مادر خویش راز بگشاد</p></div>
<div class="m2"><p>یکباره در نیاز بگشاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کای مادر مهربان چه تدبیر</p></div>
<div class="m2"><p>کاهو بره زهر خورد با شیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در کوچگه اوفتاد رختم</p></div>
<div class="m2"><p>چون سست شدم مگیر سختم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خون می‌خورم این چه مهربانیست</p></div>
<div class="m2"><p>جان می‌کنم این چه زندگانیست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چندان جگر نهفته خوردم</p></div>
<div class="m2"><p>کز دل به دهن رسید دردم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون جان ز لبم نفس گشاید</p></div>
<div class="m2"><p>گر راز گشاده گشت شاید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون پرده ز راز بر گرفتم</p></div>
<div class="m2"><p>بدرود که راه در گرفتم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در گردنم آر دست یکبار</p></div>
<div class="m2"><p>خون من و گردن تو زنهار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کان لحظه که جان سپرده باشم</p></div>
<div class="m2"><p>وز دوری دوست مرده باشم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سرمم ز غبار دوست درکش</p></div>
<div class="m2"><p>نیلم ز نیاز دوست برکش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فرقم ز گلاب اشک تر کن</p></div>
<div class="m2"><p>عطرم ز شمامه جگر کن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بر بند حنوطم از گل زرد</p></div>
<div class="m2"><p>کافور فشانم از دم سرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خون کن کفنم که من شهیدم</p></div>
<div class="m2"><p>تا باشد رنگ روز عیدم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آراسته کن عروس‌وارم</p></div>
<div class="m2"><p>بسپار به خاک پرده دارم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آواره من چو گردد آگاه</p></div>
<div class="m2"><p>کاواره شدم من از وطن گاه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دانم که ز راه سوگواری</p></div>
<div class="m2"><p>آید به سلام این عماری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون بر سر خاک من نشیند</p></div>
<div class="m2"><p>مه جوید لیک خاک بیند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بر خاک من آن غریب خاکی</p></div>
<div class="m2"><p>نالد به دریغ و دردناکی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>یاراست و عجب عزیز یاراست</p></div>
<div class="m2"><p>از من به بر تو یادگار است</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از بهر خدا نکوش داری</p></div>
<div class="m2"><p>در وی نکنی نظر به خواری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آن دل که نیابیش بجوئی</p></div>
<div class="m2"><p>وان قصه که دانیش بگوئی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>من داشته‌ام عزیزوارش</p></div>
<div class="m2"><p>تو نیز چو من عزیز دارش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گو لیلی ازین سرای دلگیر</p></div>
<div class="m2"><p>آن لحظه که می‌برید زنجیر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در مهر تو تن به خاک می‌داد</p></div>
<div class="m2"><p>بر یاد تو جان پاک می‌داد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>در عاشقی تو صادقی کرد</p></div>
<div class="m2"><p>جان در سر کار عاشقی کرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>احوال چه پرسیم که چون رفت</p></div>
<div class="m2"><p>با عشق تو از جهان برون رفت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تا داشت در این جهان شماری</p></div>
<div class="m2"><p>جز با غم تو نداشت کاری</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>وان لحظه که در غم تو می‌مرد</p></div>
<div class="m2"><p>غمهای تو راه توشه می‌برد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>وامروز که در نقاب خاکست</p></div>
<div class="m2"><p>هم در هوس تو دردناکست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چون منتظران درین گذرگاه</p></div>
<div class="m2"><p>هست از قبل تو چشم بر راه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>می‌پاید تا تو در پی آیی</p></div>
<div class="m2"><p>سرباز پس است تا کی آیی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>یک ره برهان از انتظارش</p></div>
<div class="m2"><p>در خز به خزینه کنارش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>این گفت و به گریه دیده‌تر کرد</p></div>
<div class="m2"><p>وآهنگ ولایت دگر کرد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چون راز نهفته بر زبان داد</p></div>
<div class="m2"><p>جانان طلبید و زود جان داد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مادر که عروس را چنان دید</p></div>
<div class="m2"><p>آیا که قیامت آن زمان دید</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>معجز ز سر سپید بگشاد</p></div>
<div class="m2"><p>موی چو سمن به باد برداد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در حسرت روی و موی فرزند</p></div>
<div class="m2"><p>برمیزد و موی و روی می‌کند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>هر مویه که بود خواندش از بر</p></div>
<div class="m2"><p>هر موی که داشت کندش از سر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پیرانه گریست بر جوانیش</p></div>
<div class="m2"><p>خون ریخت بر آب زندگانیش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گه ریخت سرشک بر سرینش</p></div>
<div class="m2"><p>گه روی نهاد بر جبینش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چندان ز سرشگهاش خون رست</p></div>
<div class="m2"><p>کان چشمه آب را به خون شست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چندان ز غمش به مهر نالید</p></div>
<div class="m2"><p>کز ناله او سپهر نالید</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>آن نوحه که خون شود بدو سنگ</p></div>
<div class="m2"><p>می‌کرد بران عقیق گلرنگ</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>مه را ز ستاره طوق بربست</p></div>
<div class="m2"><p>صندوق جگر هم از جگر بست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>آراستش آنچنان که فرمود</p></div>
<div class="m2"><p>گل را به گلاب و عنبرآلود</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بسپرد به خاک و نامدش باک</p></div>
<div class="m2"><p>کاسایش خاک هست در خاک</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>خاتون حصار شد حصاری</p></div>
<div class="m2"><p>آسود غم از خزینه‌داری</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>طغرا کش این مثال مشهور</p></div>
<div class="m2"><p>بر شقه چنان نبشت منشور</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>کز حادثه وفات آن ماه</p></div>
<div class="m2"><p>چون قیس شکسته دل شد آگاه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>گریان شد و تلخ تلخ بگریست</p></div>
<div class="m2"><p>بی گریه تلخ در جهان کیست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>آمد سوی آن حظیره جوشان</p></div>
<div class="m2"><p>چون ابر شد از درون خروشان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بر مشهد او که موج خون بود</p></div>
<div class="m2"><p>آن سوخته دل مپرس چون بود</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>از دیده چو خون سرشک ریزان</p></div>
<div class="m2"><p>مردم ز نفیر او گریزان</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>در شوشه تربتش به صد رنج</p></div>
<div class="m2"><p>پیچید چنانکه مار بر گنج</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>از بس که سرشک لاله‌گون ریخت</p></div>
<div class="m2"><p>لاله ز گیاه گورش انگیخت</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>خوناب جگر چو شمع پالود</p></div>
<div class="m2"><p>بگشاد زبان آتش آلود</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>وانگاه به دخمه سر فرو کرد</p></div>
<div class="m2"><p>می‌گفت و همی گریست از درد</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>کای تازه گل خزان رسیده</p></div>
<div class="m2"><p>رفته ز جهان جهان ندیده</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چونی ز گزند خاک چونی</p></div>
<div class="m2"><p>در ظلمت این مغاک چونی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>آن خال چو مشک دانه چونست</p></div>
<div class="m2"><p>وان چشمک آهوانه چونست</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چونست عقیق آبدارت</p></div>
<div class="m2"><p>وآن غالیه‌های تابدارت</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>نقشت به چه رنگ می‌طرازند</p></div>
<div class="m2"><p>شمعت به چه طشت می‌گدازند</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بر چشم که جلوه می‌نمائی</p></div>
<div class="m2"><p>در مغز که نافه می‌گشائی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>سروت به کدام جویبار است</p></div>
<div class="m2"><p>بزمت به کدام لاله زاراست</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>چونی ز گزندهای این خار</p></div>
<div class="m2"><p>چون می‌گذرانی اندر این غار</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>در غار همیشه جای ماراست</p></div>
<div class="m2"><p>ای ماه ترا چه جای غاراست</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بر غار تو غم خورم که یاری</p></div>
<div class="m2"><p>چون غم نخورم که یار غاری</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>هم گنج شدی که در زمینی</p></div>
<div class="m2"><p>گر گنج نه‌ای چرا چنینی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>هر گنج که درون غاریست</p></div>
<div class="m2"><p>بر دامن او نشسته ماریست</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>من مار کز آشیان برنجم</p></div>
<div class="m2"><p>بر خاک تو پاسبان گنجم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>شوریده بدی چو ریگ در راه</p></div>
<div class="m2"><p>آسوده شدی چو آب در چاه</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چون ماه غریبیت نصیب است</p></div>
<div class="m2"><p>از مه نه غریب اگر غریب است</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>در صورت اگر ز من نهانی</p></div>
<div class="m2"><p>از راه صفت درون جانی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>گر دور شدی ز چشم رنجور</p></div>
<div class="m2"><p>یک چشم زد از دلم نه‌ای دور</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>گر نقش تو از میانه برخاست</p></div>
<div class="m2"><p>اندوه تو جاودانه برجاست</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>این گفت و نهاد دست بر دست</p></div>
<div class="m2"><p>چرخی زد و دستبند بشکست</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>برداشت ره ولایت خویش</p></div>
<div class="m2"><p>مشتی ددگانش از پس و پیش</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>در رقص رحیل ناقه می‌راند</p></div>
<div class="m2"><p>بر حسب فراق بیت می‌خواند</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>در گفتن حالت فراقی</p></div>
<div class="m2"><p>حرفی ز وفا نماند باقی</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>می‌داد به گریه ریگ را رنگ</p></div>
<div class="m2"><p>می‌زد سری از دریغ بر سنگ</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بر رهگذری نماند خاری</p></div>
<div class="m2"><p>کز ناله نزد بر او شراری</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>در هیچ رهی نماند سنگی</p></div>
<div class="m2"><p>کز خون خودش نداد رنگی</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>چون سخت شدی ز گریه کارش</p></div>
<div class="m2"><p>برخاستی آرزوی یارش</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>از کوه درآمدی چو سیلی</p></div>
<div class="m2"><p>رفتی سوی روضه گاه لیلی</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>سر بر سر خاک او نهادی</p></div>
<div class="m2"><p>برخاک هزار بوسه دادی</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>با تربت آن بت وفا دار</p></div>
<div class="m2"><p>گفتی غم دل به زاری زار</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>او بر سر شغل و محنت خویش</p></div>
<div class="m2"><p>وان دام و دد ایستاده در پیش</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>او زمزم گشته ز آب دیده</p></div>
<div class="m2"><p>وایشان حرمی در او کشیده</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چشم از ره او جدا نکردند</p></div>
<div class="m2"><p>کس را بر او رها نکردند</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>از بیم ددان بدان گذرگاه</p></div>
<div class="m2"><p>بر جمله خلق بسته شد راه</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>تا او نشدی ز مرغ تا مور</p></div>
<div class="m2"><p>کس پی ننهاد گرد آن گور</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>زینسان ورقی سیاه می‌کرد</p></div>
<div class="m2"><p>عمری به هوس تباه می‌کرد</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>روزی دو سه با سگان آن ده</p></div>
<div class="m2"><p>می‌زیست چنانکه مرگ از او به</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>گه قبله ز گور یار می‌ساخت</p></div>
<div class="m2"><p>گاه از پس گور دشت می‌تاخت</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>در دیده مور بود جایش</p></div>
<div class="m2"><p>وز گور به گور بود پایش</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>وآخر چو به کار خویش درماند</p></div>
<div class="m2"><p>او نیز رحیل نامه برخواند</p></div></div>