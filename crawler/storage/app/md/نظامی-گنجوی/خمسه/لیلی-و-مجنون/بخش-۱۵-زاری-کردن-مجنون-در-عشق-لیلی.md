---
title: >-
    بخش ۱۵ - زاری کردن مجنون در عشق لیلی
---
# بخش ۱۵ - زاری کردن مجنون در عشق لیلی

<div class="b" id="bn1"><div class="m1"><p>مجنون چو شنید پند خویشان</p></div>
<div class="m2"><p>از تلخی پند شد پریشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زد دست و درید پیرهن را</p></div>
<div class="m2"><p>کاین مرده چه می‌کند کفن را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن کز دو جهان برون زند تخت</p></div>
<div class="m2"><p>در پیرهنی کجا کشد رخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون وامق از آرزوی عذرا</p></div>
<div class="m2"><p>گه کوه گرفت و گاه صحرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترکانه ز خانه رخت بربست</p></div>
<div class="m2"><p>در کوچگه رحیل بنشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دراعه درید و درع می‌دوخت</p></div>
<div class="m2"><p>زنجیر برید و بند می‌سوخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌گشت ز دور چون غریبان</p></div>
<div class="m2"><p>دامن بدریده تا گریبان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر کشتن خویش گشته والی</p></div>
<div class="m2"><p>لاحول ازو به هر حوالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیوانه صفت شده به هر کوی</p></div>
<div class="m2"><p>لیلی لیلی زنان به هر سوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>احرام دریده سر گشاده</p></div>
<div class="m2"><p>در کوی ملامت او فتاده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با نیک و بدی که بود در ساخت</p></div>
<div class="m2"><p>نیک از بد و بد ز نیک نشناخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می‌خواند نشید مهربانی</p></div>
<div class="m2"><p>بر شوق ستاره یمانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر بیت که آمد از زبانش</p></div>
<div class="m2"><p>بر یاد گرفت این و آنش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حیران شده هر کسی در آن پی</p></div>
<div class="m2"><p>می‌دید و همی گریست بر وی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>او فارغ از آنکه مردمی هست</p></div>
<div class="m2"><p>یا بر حرفش کسی نهد دست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حرف از ورق جهان سترده</p></div>
<div class="m2"><p>می‌بود نه زنده و نه مرده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر سنگ فتاده خوار چون گِل</p></div>
<div class="m2"><p>سنگ دگرش فتاده بر دل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صافی تن او چو درد گشته</p></div>
<div class="m2"><p>در زیر دو سنگ خرد گشته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون شمع جگرگداز مانده</p></div>
<div class="m2"><p>یا مرغِ ز جفت باز مانده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در دل همه داغ دردناکی</p></div>
<div class="m2"><p>بر چهره غبارهای خاکی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون مانده شد از عذاب و اندوه</p></div>
<div class="m2"><p>سجاده برون فکند از انبوه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بنشست و به های‌های بگریست</p></div>
<div class="m2"><p>کاوخ چکنم دوای من چیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آواره ز خان و مان چنانم</p></div>
<div class="m2"><p>کز کوی به خانه ره ندانم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نه بر در دیر خود پناهی</p></div>
<div class="m2"><p>نه بر سر کوی دوست راهی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قرابهٔ نام و شیشهٔ ننگ</p></div>
<div class="m2"><p>افتاد و شکست بر سر سنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شد طبل بشارتم دریده</p></div>
<div class="m2"><p>من طبل رحیل برکشیده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ترکی که شکار لنگ اویم</p></div>
<div class="m2"><p>آماجگه خدنگ اویم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یاری که ز جان مطیعم او را</p></div>
<div class="m2"><p>در دادن جان شفیعم او را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر مستم خواند یار، مستم</p></div>
<div class="m2"><p>ور شیفته گفت نیز هستم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون شیفتگی و مستیم هست</p></div>
<div class="m2"><p>در شیفته، دل مجوی و در مست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آشفته چنان نیم به تقدیر</p></div>
<div class="m2"><p>کاسوده شوم به هیچ زنجیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ویران نه چنان شد است کارم</p></div>
<div class="m2"><p>کابادی خویش چشم دارم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای کاش که بر من اوفتادی</p></div>
<div class="m2"><p>خاکی که مرا به باد دادی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یا صاعقه‌ای درآمدی سخت</p></div>
<div class="m2"><p>هم خانه بسوختی و هم رخت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کس نیست که آتشی در آرد</p></div>
<div class="m2"><p>دود از من و جان من برآرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اندازد در دَم نهنگم</p></div>
<div class="m2"><p>تا باز رهد جهان ز ننگم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از ناخلفی که در زمانم</p></div>
<div class="m2"><p>دیوانه خلق و دیو خان‌ام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خویشان مرا ز خوی من خار</p></div>
<div class="m2"><p>یاران مرا ز نام من عار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خونریز من خراب خسته</p></div>
<div class="m2"><p>هست از دیت و قصاص رسته</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای هم نفسان مجلس و رود</p></div>
<div class="m2"><p>بدرود شوید جمله بدرود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کان شیشهٔ می که بود در دست</p></div>
<div class="m2"><p>افتاده شد آبگینه بشکست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر در رهم آبگینه شد خورد</p></div>
<div class="m2"><p>سیل آمد و آبگینه را برد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا هر که به من رسید رایش</p></div>
<div class="m2"><p>نازارد از آبگینه پایش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای بی‌خبران ز درد و آهم</p></div>
<div class="m2"><p>خیزید و رها کنید راهم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>من گم شده‌ام مرا مجوئید</p></div>
<div class="m2"><p>با گم‌شدگان سخن مگوئید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تا کی ستم و جفا کنیدم</p></div>
<div class="m2"><p>با محنت خود رها کنیدم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بیرون مکنید از این دیارم</p></div>
<div class="m2"><p>من خود به گریختن سوارم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از پای فتاده‌ام چه تدبیر</p></div>
<div class="m2"><p>ای دوست بیا و دست من گیر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>این خسته که دل سپرده تست</p></div>
<div class="m2"><p>زنده به تو بِه که مرده تست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بنواز به لطف یک سلامم</p></div>
<div class="m2"><p>جان تازه نما به یک پیامم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دیوانه منم به رای و تدبیر</p></div>
<div class="m2"><p>در گردن تو چراست زنجیر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در گردن خود رسن میفکن</p></div>
<div class="m2"><p>من بِه باشم رسن به گردن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>زلف تو درید هر چه دل دوخت</p></div>
<div class="m2"><p>این پرده‌دری ورا که آموخت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دل بردن زلف تو نه زور است</p></div>
<div class="m2"><p>او هندو و روزگار کور است</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کاری بکن ای نشان کارم</p></div>
<div class="m2"><p>زین چَه که فرو شدم برآرم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>یا دست بگیر از این فسوسم</p></div>
<div class="m2"><p>یا پای بدار تا ببوسم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بی کار نمی‌توان نشستن</p></div>
<div class="m2"><p>در کنج خطاست دست بستن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بی‌رحمتم این چنین چه ماندی</p></div>
<div class="m2"><p>(ارحم ترحم) مگر نخواندی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>آسوده که رنج بر ندارد</p></div>
<div class="m2"><p>از رنجوران خبر ندارد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سیری که به گرسنه نهد خوان</p></div>
<div class="m2"><p>خردک شکند به کاسه در نان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>آن راست خبر از آتش گرم</p></div>
<div class="m2"><p>کو دست درو زند بی‌آزرم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ای هم من و هم تو آدمیزاد</p></div>
<div class="m2"><p>من خار خسک تو شاخ شمشاد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>زرنیخ چو زر کجا عزیز است</p></div>
<div class="m2"><p>زان یک من ازین به یک پشیز است</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ای راحت جان من کجائی</p></div>
<div class="m2"><p>در بردن جان من چرائی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>جرم دل عذر خواه من چیست</p></div>
<div class="m2"><p>جز دوستیت گناه من چیست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>یکشب ز هزار شب مرا باش</p></div>
<div class="m2"><p>یک رای صواب گو خطا باش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گردن مکش از رضای اینکار</p></div>
<div class="m2"><p>در گردن من خطای اینکار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>این کم زده را که نام کم نیست</p></div>
<div class="m2"><p>آزرم تو هست هیچ غم نیست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>صفرای تو گر مشام سوز است</p></div>
<div class="m2"><p>لطفت ز پی کدام روز است</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گر خشم تو آتشی زند تیز</p></div>
<div class="m2"><p>آبی ز سرشک من بر او ریز</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ای ماه نو ام ستاره تو</p></div>
<div class="m2"><p>من شیفتهٔ نظاره تو</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>به گر به توام نمی‌نوازند</p></div>
<div class="m2"><p>کاشفته و ماه نو نسازند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>از سایه نشان تو نپرسم</p></div>
<div class="m2"><p>کز سایه خویشتن می‌بترسم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>من کار ترا به سایه دیده</p></div>
<div class="m2"><p>تو سایه ز کار من بریده</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بردی دل و جانم این چه شور است</p></div>
<div class="m2"><p>این بازی نیست دست زور است</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>از حاصل تو که نام دارم</p></div>
<div class="m2"><p>بی‌حاصلی تمام دارم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بر وصل تو گرچه نیست دستم</p></div>
<div class="m2"><p>غم نیست چو بر امید هستم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>گر بیند طفل تشنه در خواب</p></div>
<div class="m2"><p>کورا به سبوی زر دهند آب</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>لیکن چو ز خواب خوش براید</p></div>
<div class="m2"><p>انگشت ز تشنگی بخاید</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>پایم چو دو لام خم‌پذیر است</p></div>
<div class="m2"><p>دستم چو دو یا شکنج گیر است</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>نام تو مرا چو نام دارد</p></div>
<div class="m2"><p>کو نیز دو یا دو لام دارد</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>عشق تو ز دل نهادنی نیست</p></div>
<div class="m2"><p>وین راز به کس گشادنی نیست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>با شیر به تن فرو شد این راز</p></div>
<div class="m2"><p>با جان به در آید از تنم باز</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>این گفت و فتاد بر سر خاک</p></div>
<div class="m2"><p>نظارگیان شدند غمناک</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>گشتند به لطف چاره سازش</p></div>
<div class="m2"><p>بردند به سوی خانه بازش</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>عشقی که نه عشق جاودانیست</p></div>
<div class="m2"><p>بازیچه شهوت جوانیست</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>عشق آن باشد که کم نگردد</p></div>
<div class="m2"><p>تا باشد از این قدم نگردد</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>آن عشق نه سرسری خیالست</p></div>
<div class="m2"><p>کو را ابد الابد زوالست</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>مجنون که بلند نام عشقست</p></div>
<div class="m2"><p>از معرفت تمام عشقست</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>تا زنده به عشق بارکش بود</p></div>
<div class="m2"><p>چون گل به نسیم عشق خوش بود</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>واکنون که گلش رحیل یابست</p></div>
<div class="m2"><p>این قطره که ماند ازو گلابست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>من نیز بدان گلاب خوشبوی</p></div>
<div class="m2"><p>خوش می‌کنم آب خود درین جوی</p></div></div>