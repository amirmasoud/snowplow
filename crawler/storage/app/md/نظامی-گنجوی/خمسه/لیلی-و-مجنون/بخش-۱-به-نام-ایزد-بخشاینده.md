---
title: >-
    بخش ۱ - به نام ایزد بخشاینده
---
# بخش ۱ - به نام ایزد بخشاینده

<div class="b" id="bn1"><div class="m1"><p>ای نام تو بهترین سرآغاز</p></div>
<div class="m2"><p>بی‌نام تو نامه کی کنم باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای یاد تو مونس روانم</p></div>
<div class="m2"><p>جز نام تو نیست بر زبانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای کارگشای هر چه هستند</p></div>
<div class="m2"><p>نام تو کلید هر چه بستند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای هیچ خطی نگشته ز اول</p></div>
<div class="m2"><p>بی‌حجت نام تو مسجل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای هست کن اساس هستی</p></div>
<div class="m2"><p>کوته ز درت درازدستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خطبه تو تبارک الله</p></div>
<div class="m2"><p>فیض تو همیشه بارک الله</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای هفت عروس نه عماری</p></div>
<div class="m2"><p>بر درگه تو به پرده‌داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای هست نه بر طریق چونی</p></div>
<div class="m2"><p>دانای برونی و درونی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای هرچه رمیده وارمیده</p></div>
<div class="m2"><p>در کن فیکون تو آفریده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای واهب عقل و باعث جان</p></div>
<div class="m2"><p>با حکم تو هست و نیست یکسان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای محرم عالم تحیر</p></div>
<div class="m2"><p>عالم ز تو هم تهی و هم پر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای تو به صفات خویش موصوف</p></div>
<div class="m2"><p>ای نهی تو منکر امر معروف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای امر تو را نفاذ مطلق</p></div>
<div class="m2"><p>وز امر تو کائنات مشتق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای مقصد همت بلندان</p></div>
<div class="m2"><p>مقصود دل نیازمندان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای سرمه کش بلند بینان</p></div>
<div class="m2"><p>در باز کن درون نشینان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای بر ورق تو درس ایام</p></div>
<div class="m2"><p>ز آغاز رسیده تا به انجام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صاحب توئی آن دگر غلامند</p></div>
<div class="m2"><p>سلطان توئی آن دگر کدامند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>راه تو به نور لایزالی</p></div>
<div class="m2"><p>از شرک و شریک هر دو خالی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در صنع تو کامد از عدد بیش</p></div>
<div class="m2"><p>عاجز شده عقل علت اندیش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ترتیب جهان چنانکه بایست</p></div>
<div class="m2"><p>کردی به مثابتی که شایست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر ابلق صبح و ادهم شام</p></div>
<div class="m2"><p>حکم تو زد این طویله را بام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر هفت گره به چرخ دادی</p></div>
<div class="m2"><p>هفتاد گره بدو گشادی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خاکستری ار ز خاک سودی</p></div>
<div class="m2"><p>صد آینه را بدان زدودی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر هر ورقی که حرف راندی</p></div>
<div class="m2"><p>نقش همه در دو حرف خواندی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بی کوه‌کنی ز کاف و نونی</p></div>
<div class="m2"><p>کردی تو سپهر بیستونی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر جا که خزینه شگرفست</p></div>
<div class="m2"><p>قفلش به کلید این دو حرفست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حرفی به غلط رها نکردی</p></div>
<div class="m2"><p>یک نکته درو خطا نکردی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در عالم عالم آفریدن</p></div>
<div class="m2"><p>به زین نتوان رقم کشیدن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر دم نه به حق دسترنجی</p></div>
<div class="m2"><p>بخشی به من خراب گنجی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گنج تو به بذل کم نیاید</p></div>
<div class="m2"><p>وز گنج کس این کرم نیاید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از قسمت بندگی و شاهی</p></div>
<div class="m2"><p>دولت تو دهی بهر که خواهی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از آتش ظلم و دود مظلوم</p></div>
<div class="m2"><p>احوال همه تراست معلوم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هم قصه نانموده دانی</p></div>
<div class="m2"><p>هم نامه نانوشته خوانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عقل آبله پای و کوی تاریک</p></div>
<div class="m2"><p>وآنگاه رهی چو موی باریک</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>توفیق تو گر نه ره نماید</p></div>
<div class="m2"><p>این عقده به عقل کی گشاید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عقل از در تو بصر فروزد</p></div>
<div class="m2"><p>گر پای درون نهد بسوزد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ای عقل مرا کفایت از تو</p></div>
<div class="m2"><p>جستن ز من و هدایت از تو</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>من بی‌دل و راه بیمناکست</p></div>
<div class="m2"><p>چون راهنما توئی چه باکست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>عاجز شدم از گرانی بار</p></div>
<div class="m2"><p>طاقت نه چگونه باشد این کار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>می‌کوشم و در تنم توان نیست</p></div>
<div class="m2"><p>کازرم تو هست باک از آن نیست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر لطف کنی و گر کنی قهر</p></div>
<div class="m2"><p>پیش تو یکی است نوش یا زهر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شک نیست در اینکه من اسیرم</p></div>
<div class="m2"><p>کز لطف زیم ز قهر میرم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یا شربت لطف دار پیشم</p></div>
<div class="m2"><p>یا قهر مکن به قهر خویشم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر قهر سزای ماست آخر</p></div>
<div class="m2"><p>هم لطف برای ماست آخر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا در نقسم عنایتی هست</p></div>
<div class="m2"><p>فتراک تو کی گذارم از دست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وآن دم که نفس به آخر آید</p></div>
<div class="m2"><p>هم خطبه نام تو سراید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>وآن لحظه که مرگ را بسیجم</p></div>
<div class="m2"><p>هم نام تو در حنوط پیچم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چون گرد شود وجود پستم</p></div>
<div class="m2"><p>هرجا که روم تو را پرستم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در عصمت اینچنین حصاری</p></div>
<div class="m2"><p>شیطان رجیم کیست باری</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چون حرز توام حمایل آمود</p></div>
<div class="m2"><p>سرهنگی دیو کی کند سود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>احرام گرفته‌ام به کویت</p></div>
<div class="m2"><p>لبیک زنان به جستجویت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>احرام شکن بسی است زنهار</p></div>
<div class="m2"><p>ز احرام شکستنم نگهدار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>من بیکس و رخنها نهانی</p></div>
<div class="m2"><p>هان ای کس بیکسان تو دانی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چون نیست به جز تو دستگیرم</p></div>
<div class="m2"><p>هست از کرم تو ناگزیرم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>یک ذره ز کیمیای اخلاص</p></div>
<div class="m2"><p>گر بر مس من زنی شوم خاص</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>آنجا که دهی ز لطف یک تاب</p></div>
<div class="m2"><p>زر گردد خاک و در شود آب</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>من گر گهرم و گر سفالم</p></div>
<div class="m2"><p>پیرایه توست روی مالم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>از عطر تو لافد آستینم</p></div>
<div class="m2"><p>گر عودم و گر درمنه اینم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>پیش تو نه دین نه طاعت آرم</p></div>
<div class="m2"><p>افلاس تهی شفاعت آرم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تا غرق نشد سفینه در آب</p></div>
<div class="m2"><p>رحمت کن و دستگیر و دریاب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بردار مرا که اوفتادم</p></div>
<div class="m2"><p>وز مرکب جهل خود پیادم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هم تو به عنایت الهی</p></div>
<div class="m2"><p>آنجا قدمم رسان که خواهی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>از ظلمت خود رهائیم ده</p></div>
<div class="m2"><p>با نور خود آشنائیم ده</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>تا چند مرا ز بیم و امید</p></div>
<div class="m2"><p>پروانه دهی به ماه و خورشید</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تا کی به نیاز هر نوالم</p></div>
<div class="m2"><p>بر شاه و شبان کنی حوالم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>از خوان تو با نعیم‌تر چیست</p></div>
<div class="m2"><p>وز حضرت تو کریمتر کیست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>از خرمن خویش ده زکاتم</p></div>
<div class="m2"><p>منویس به این و آن براتم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تا مزرعه چو من خرابی</p></div>
<div class="m2"><p>آباد شود به خاک و آبی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خاکی ده از آستان خویشم</p></div>
<div class="m2"><p>وابی که دغل برد ز پیشم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>روزی که مرا ز من ستانی</p></div>
<div class="m2"><p>ضایع مکن از من آنچه مانی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>وآندم که مرا به من دهی باز</p></div>
<div class="m2"><p>یک سایه ز لطف بر من انداز</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>آن سایه نه کز چراغ دور است</p></div>
<div class="m2"><p>آن سایه که آن چراغ نوراست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>تا با تو چو سایه نور گردم</p></div>
<div class="m2"><p>چون نور ز سایه دور گردم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>با هر که نفس برآرم اینجا</p></div>
<div class="m2"><p>روزیش فروگذارم اینجا</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>درهای همه ز عهد خالیست</p></div>
<div class="m2"><p>الا در تو که لایزالیست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>هر عهد که هست در حیاتست</p></div>
<div class="m2"><p>عهد از پس مرگ بی‌ثباتست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چون عهد تو هست جاودانی</p></div>
<div class="m2"><p>یعنی که به مرگ و زندگانی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چندانکه قرار عهد یابم</p></div>
<div class="m2"><p>از عهد تو روی برنتابم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بی‌یاد توام نفس نیاید</p></div>
<div class="m2"><p>با یاد تو یاد کس نیاید</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>اول که نیافریده بودم</p></div>
<div class="m2"><p>وین تعبیه‌ها ندیده بودم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>کیمُخت اگر از ذمیم کردی</p></div>
<div class="m2"><p>با زاز ذمیم ادیم کردی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بر صورت من ز روی هستی</p></div>
<div class="m2"><p>آرایش آفرین تو بستی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>واکنون که نشانه گاه جودم</p></div>
<div class="m2"><p>تا باز عدم شود وجودم</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>هرجا که نشاندیم نشستم</p></div>
<div class="m2"><p>وآنجا که بریم زیر دستم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>گردیده رهیت من در این راه</p></div>
<div class="m2"><p>گه بر سر تخت و گه بن چاه</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>گر پیر بوم و گر جوانم</p></div>
<div class="m2"><p>ره مختلف است و من همانم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>از حال به حال اگر بگردم</p></div>
<div class="m2"><p>هم بر رق اولین نوردم</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بی‌جاحتم آفریدی اول</p></div>
<div class="m2"><p>آخر نگذاریم معطل</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>گر مرگ رسد چرا هراسم</p></div>
<div class="m2"><p>کان راه بتست می‌شناسم</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>این مرگ نه، باغ و بوستانست</p></div>
<div class="m2"><p>کو راه سرای دوستانست</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>تا چند کنم ز مرگ فریاد</p></div>
<div class="m2"><p>چون مرگ ازوست مرگ من باد</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>گر بنگرم آن چنان که رایست</p></div>
<div class="m2"><p>این مرگ نه مرگ نقل جایست</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>از خورد گهی به خوابگاهی</p></div>
<div class="m2"><p>وز خوابگهی به بزم شاهی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>خوابی که به بزم تست راهش</p></div>
<div class="m2"><p>گردن نکشم ز خوابگاهش</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>چون شوق تو هست خانه خیزم</p></div>
<div class="m2"><p>خوش خسبم و شادمانه خیزم</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>گر بنده نظامی از سر درد</p></div>
<div class="m2"><p>در نظم دعا دلیریی کرد</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>از بحر تو بینم ابر خیزش</p></div>
<div class="m2"><p>گر قطره برون دهد مریزش</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>گر صد لغت از زبان گشاید</p></div>
<div class="m2"><p>در هر لغتی ترا ستاید</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>هم در تو به صد هزار تشویر</p></div>
<div class="m2"><p>دارد رقم هزار تقصیر</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ور دم نزند چو تنگ حالان</p></div>
<div class="m2"><p>دانی که لغت زبان لالان</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>گر تن حبشی سرشته تست</p></div>
<div class="m2"><p>ور خط ختنی نبشته تست</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>گر هر چه نبشته‌ای بشوئی</p></div>
<div class="m2"><p>شویم دهن از زیاده گوئی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ور باز به داورم نشانی</p></div>
<div class="m2"><p>ای داور داوران تو دانی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>زان پیش کاجل فرا رسد تنگ</p></div>
<div class="m2"><p>و ایام عنان ستاند از چنگ</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>ره باز ده از ره قبولم</p></div>
<div class="m2"><p>بر روضه تربت رسولم</p></div></div>