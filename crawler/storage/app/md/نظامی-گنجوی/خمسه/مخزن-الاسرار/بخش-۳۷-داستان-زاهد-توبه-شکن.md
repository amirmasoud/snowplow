---
title: >-
    بخش ۳۷ - داستان زاهد توبه شکن
---
# بخش ۳۷ - داستان زاهد توبه شکن

<div class="b" id="bn1"><div class="m1"><p>مسجدیئی بسته آفات شد</p></div>
<div class="m2"><p>معتکف کوی خرابات شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می به دهن برد و چو می می‌گریست</p></div>
<div class="m2"><p>کای من بیچاره مرا چاره چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغ هوا در دلم آرام گرد</p></div>
<div class="m2"><p>دانه تسبیح مرا دام کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کعبه مرا رهزن اوقات بود</p></div>
<div class="m2"><p>خانه اصلیم خرابات بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طالع بد بود و بد اختر شدم</p></div>
<div class="m2"><p>نامزد کوی قلندر شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم ادب زیر نقاب از من است</p></div>
<div class="m2"><p>کوی خرابات خراب از من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تنگ جهان بر من مهجور باد</p></div>
<div class="m2"><p>گرد من ازدامن من دور باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نه قضا بود من و لات کی</p></div>
<div class="m2"><p>مسجدی و کوی خرابات کی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همت از آنجا که نظر کرده بود</p></div>
<div class="m2"><p>گفت جوابی که در آن پرده بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کاین روش از راه قضا دور دار</p></div>
<div class="m2"><p>چون تو قضا را بجوی صد هزار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر در عذر آی و گنه را بشوی</p></div>
<div class="m2"><p>آنگه ازین شیوه حدیثی بگوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون تو روی عذر پذیرت برند</p></div>
<div class="m2"><p>ورنه خود آیند و اسیرت برند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سبزه چریدن ز سر خاک بس</p></div>
<div class="m2"><p>نیشکر سبز تو افلاک بس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا نبرد خوابت ازو گوشه کن</p></div>
<div class="m2"><p>اندکی از بهر عدم توشه کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خوش نبود دیده به خوناب در</p></div>
<div class="m2"><p>زنده و مرده به یکی خواب در</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دین که ترا دید چنین مست خواب</p></div>
<div class="m2"><p>چهره نهان کرد به زیر نقاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خیز نظامی که ملک بر نشست</p></div>
<div class="m2"><p>همسر اینجا چه شوی پای بست</p></div></div>