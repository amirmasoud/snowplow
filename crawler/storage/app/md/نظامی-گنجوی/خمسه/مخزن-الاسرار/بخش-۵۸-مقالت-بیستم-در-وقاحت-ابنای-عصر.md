---
title: >-
    بخش ۵۸ - مقالت بیستم در وقاحت ابنای عصر
---
# بخش ۵۸ - مقالت بیستم در وقاحت ابنای عصر

<div class="b" id="bn1"><div class="m1"><p>ما که به خود دست برافشانده‌ایم</p></div>
<div class="m2"><p>بر سر خاکی چه فرومانده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صحبت این خاک ترا خار کرد</p></div>
<div class="m2"><p>خاک چنین تعبیه بسیار کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر همه رفت و به پس گستریم</p></div>
<div class="m2"><p>قافله از قافله واپس تریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این دو فرشته شده در بند ما</p></div>
<div class="m2"><p>دیو ز بدنامی پیوند ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرم رو سرد چو گلخن گریم</p></div>
<div class="m2"><p>سرد پی گرم چو خاکستریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نور دل و روشنی سینه کو</p></div>
<div class="m2"><p>راحت و آسایش پارینه کو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبح شباهنگ قیامت دمید</p></div>
<div class="m2"><p>شد علم صبح روان ناپدید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خنده غفلت به دهان درشکست</p></div>
<div class="m2"><p>آرزوی عمر به جان درشکست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از کف این خاک به افسونگری</p></div>
<div class="m2"><p>چاره آن ساز که چون جان بری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر پر ازین دام که خونخواره‌ایست</p></div>
<div class="m2"><p>زیرکی از بهر چنین چاره‌ایست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرگ ز روباه به دندان تراست</p></div>
<div class="m2"><p>روبه از آن رست که به دان تراست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهد بر آن کن که وفا را شوی</p></div>
<div class="m2"><p>خود نپرستی و خدا را شوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاک دلی شو که وفائی دروست</p></div>
<div class="m2"><p>وز گل انصاف گیائی دروست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر هنری کان ز دل آموختند</p></div>
<div class="m2"><p>بر زه منسوج وفا دوختند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر هنری در تن مردم بود</p></div>
<div class="m2"><p>چون نپسندی گهری گم بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر بپسندیش دگر سان شود</p></div>
<div class="m2"><p>چشمه آن آب دو چندان شود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مردم پرورده به جان پرورند</p></div>
<div class="m2"><p>گر هنری در طرفی بنگرند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خاک زمین جز به هنر پاک نیست</p></div>
<div class="m2"><p>وین هنر امروز درین خاک نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر هنری سر ز میان برزند</p></div>
<div class="m2"><p>بی‌هنری دست بدان درزند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کار هنرمند به جان آورند</p></div>
<div class="m2"><p>تا هنرش را به زبان آورند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حمل ریاضت به تماشا کنند</p></div>
<div class="m2"><p>نسبت اندیشه به سودا کنند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نام کرم ساخته مشتی زیان</p></div>
<div class="m2"><p>اسم وفا بندگی رایگان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفته سخا را قدری ریشخند</p></div>
<div class="m2"><p>خوانده سخن را طرفی لورکند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نقش وفا بر سر یخ می‌زنند</p></div>
<div class="m2"><p>بر مه و خورشید زنخ میزنند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر نفسی مرهم راحت بود</p></div>
<div class="m2"><p>بر دل این قوم جراحت بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر ز لبی شربت شیرین چشند</p></div>
<div class="m2"><p>دست به شیرینه به رویش کشند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر جگر پخته انجیر فام</p></div>
<div class="m2"><p>سرکه فروشند چو انگور خام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چشم هنر بین نه کسی را درست</p></div>
<div class="m2"><p>جز خلل و عیب ندانند جست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>حاصل دریا نه همه در بود</p></div>
<div class="m2"><p>یک هنر از طبع کسی پر بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دجله بود قطره‌ای از چشم کور</p></div>
<div class="m2"><p>پای ملخ پر بود از دست مور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عیب خرند این دو سه ناموسگر</p></div>
<div class="m2"><p>بی هنر و بر هنر افسوسگر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تیره‌تر از گوهر گل در گلند</p></div>
<div class="m2"><p>تلخ‌تر از غصه دل بر دلند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دود شوند ار به دماغی رسند</p></div>
<div class="m2"><p>باد شوند ار به چراغی رسند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حال جهان بین که سرانش که‌اند</p></div>
<div class="m2"><p>نامزد و نامورانش که‌اند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>این دو سه بدنام کهن مهد خویش</p></div>
<div class="m2"><p>می‌شکنندم همه چون عهد خویش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>من به صفت چون مه گردون شوم</p></div>
<div class="m2"><p>نشکنم ار بشکنم افزون شوم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رنج گرفتم ز حد افزون برند</p></div>
<div class="m2"><p>با فلک این رقعه به سر چون برند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بر سخن تازه‌تر از باغ روح</p></div>
<div class="m2"><p>منکر دیرینه چو اصحاب نوح</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای علم خضر غزائی بکن</p></div>
<div class="m2"><p>وی نفس نوح دعائی بکن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دل که ندارد سر بیدادشان</p></div>
<div class="m2"><p>باد فرامش کند ار یادشان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>با بدشان کان نه باندازه‌ایست</p></div>
<div class="m2"><p>خامشی من قوی آوازه‌ایست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>حقه پر آواز به یک در بود</p></div>
<div class="m2"><p>گنگ شود چون شکمش پر بود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خنبره نیمه برآرد خروش</p></div>
<div class="m2"><p>لیک چو پر گردد گردد خموش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر پری از دانش خاموش باش</p></div>
<div class="m2"><p>ترک زبان گوی و همه گوی باش</p></div></div>