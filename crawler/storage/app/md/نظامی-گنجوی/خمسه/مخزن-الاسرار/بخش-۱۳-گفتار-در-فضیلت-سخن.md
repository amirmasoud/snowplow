---
title: >-
    بخش ۱۳ - گفتار در فضیلت سخن
---
# بخش ۱۳ - گفتار در فضیلت سخن

<div class="b" id="bn1"><div class="m1"><p>جنبش اول که قلم برگرفت</p></div>
<div class="m2"><p>حرف نخستین ز سخن درگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرده خلوت چو برانداختند</p></div>
<div class="m2"><p>جلوت اول به سخن ساختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا سخن آوازه دل در نداد</p></div>
<div class="m2"><p>جان تن آزاده به گل در نداد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون قلم آمد شدن آغاز کرد</p></div>
<div class="m2"><p>چشم جهان را به سخن باز کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی سخن آوازه عالم نبود</p></div>
<div class="m2"><p>این همه گفتند و سخن کم نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در لغت عشق سخن جان ماست</p></div>
<div class="m2"><p>ما سخنیم این طلل ایوان ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خط هر اندیشه که پیوسته‌اند</p></div>
<div class="m2"><p>بر پر مرغان سخن بسته‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست درین کهنهٔ نوخیزتر</p></div>
<div class="m2"><p>موی شکافی ز سخن تیزتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اول اندیشه، پسینِ شمار</p></div>
<div class="m2"><p>هم سخنست این سخن اینجا بدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تاجوران تاجورش خوانده‌اند</p></div>
<div class="m2"><p>وان دگران، آن دگرش خوانده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گه بنوای علمش برکشند</p></div>
<div class="m2"><p>گه بنگار قلمش درکشند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>او ز علم فتح نماینده‌تر</p></div>
<div class="m2"><p>وز قلم اقلیم گشاینده‌تر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرچه سخن خود ننماید جمال</p></div>
<div class="m2"><p>پیش پرستندهٔ مشتی خیال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ما که نظر بر سخن افکنده‌ایم</p></div>
<div class="m2"><p>مرده اوئیم و بدو زنده‌ایم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرد پیان آتش ازو تافتند</p></div>
<div class="m2"><p>گرم روان آب درو یافتند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اوست درین ده زده آبادتر</p></div>
<div class="m2"><p>تازه‌تر از چرخ و کهن زادتر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رنگ ندارد ز نشانی که هست</p></div>
<div class="m2"><p>راست نیاید بزبانی که هست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با سخن آنجا که برآرد علم</p></div>
<div class="m2"><p>حرف زیادست و زبان نیز هم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرنه سخن رشته جان تافتی</p></div>
<div class="m2"><p>جان سر این رشته کجا یافتی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ملک طبیعت به سخن خورده‌اند</p></div>
<div class="m2"><p>مهر شریعت به سخن کرده‌اند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کان سخن ما و زر خویش داشت</p></div>
<div class="m2"><p>هر دو به صراف سخن پیش داشت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کز سخن تازه و زر کهن</p></div>
<div class="m2"><p>گوی چه به گفت سخن به سخن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پیک سخن ره بسر خویش برد</p></div>
<div class="m2"><p>کس نبرد آنچه سخن پیش برد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سیم سخن زن که درم خاک اوست</p></div>
<div class="m2"><p>زر چه سگست آهوی فتراک اوست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>صدرنشین تر ز سخن نیست کس</p></div>
<div class="m2"><p>دولت این ملک سخن راست بس</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هرچه نه دل بی خبرست از سخن</p></div>
<div class="m2"><p>شرح سخن بیشترست از سخن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا سخن است از سخن آوازه باد</p></div>
<div class="m2"><p>نام نظامی به سخن تازه باد</p></div></div>