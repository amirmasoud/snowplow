---
title: >-
    بخش ۳۴ - مقالت هشتم در بیان آفرینش
---
# بخش ۳۴ - مقالت هشتم در بیان آفرینش

<div class="b" id="bn1"><div class="m1"><p>پیشتر از پیشتران وجود</p></div>
<div class="m2"><p>کآب نخوردند ز دریای جود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کف این ملک یساری نبود</p></div>
<div class="m2"><p>در ره این خاک غباری نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وعده تاریخ به سر نامده</p></div>
<div class="m2"><p>لعبتی از پرده به در نامده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز و شب آویزش پستی نداشت</p></div>
<div class="m2"><p>جان و تن آمیزش هستی نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشمکش جور در اعضا هنوز</p></div>
<div class="m2"><p>کن مکن عدل نه پیدا هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فیض کرم کرد مواسای خویش</p></div>
<div class="m2"><p>قطره‌ای افکند ز دریای خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حالی از آن قطره که آمد برون</p></div>
<div class="m2"><p>گشت روان این فلک آبگون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زآب روان گرد برانگیختند</p></div>
<div class="m2"><p>جوهر تو ز آن عرض آمیختند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چونکه تو برخیزی ازین کارگاه</p></div>
<div class="m2"><p>باشد برخاسته گردی ز راه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای خُنُک آن شب که جهان بی تو بود</p></div>
<div class="m2"><p>نقش تو بی صورت و جان بی تو بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم فلک فارغ ازین جستجوی</p></div>
<div class="m2"><p>گوش زمین رسته ازین گفتگوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا تو درین ره ننهادی قدم</p></div>
<div class="m2"><p>شکر بسی داشت وجود از عدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فارغ از آبستنی ات روز و شب</p></div>
<div class="m2"><p>نامیه عنین و طبیعت عزب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باغ جهان زحمت خاری نداشت</p></div>
<div class="m2"><p>خاک سراسیمه غباری نداشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طالع جوزا که کمر بسته بود</p></div>
<div class="m2"><p>از ورم رگ زدنت رسته بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مه که سیه‌روی شدی در زمین</p></div>
<div class="m2"><p>طشت تو رسواش نکردی چنین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زهره هنوز آب درین گل نریخت</p></div>
<div class="m2"><p>شهپر هاروت به بابل نریخت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از تو مجرد ز می و آسمان</p></div>
<div class="m2"><p>توبه کنار و غم تو در میان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا به تو طغرای جهان تازه گشت</p></div>
<div class="m2"><p>گنبد پیروزه پر آوازه گشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از بدی چشم تو کوکب نرست</p></div>
<div class="m2"><p>کوکبه مهد کواکب شکست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بود مه و سال ز گردش بری</p></div>
<div class="m2"><p>تا تو نکردیش تعرف گری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>روی جهان کاینهٔ پاک شد</p></div>
<div class="m2"><p>زین نفسی چند خلل ناک شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مشعلهٔ صبح تو بردی به شام</p></div>
<div class="m2"><p>صادق و کاذب تو نهادیش نام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خاک زمین در دهن آسمان</p></div>
<div class="m2"><p>تا که چرا پیش تو بندد میان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر فلکت میوه جان گفته‌اند</p></div>
<div class="m2"><p>میشنوش کان به زبان گفته‌اند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تاج تو افسوس که از سر، بِه است</p></div>
<div class="m2"><p>جل از سگ و توبره از خر، بِه است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لاف بسی شد که درین لافگاه</p></div>
<div class="m2"><p>بر تو جهانی بجوی خاک راه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خود تو کفی خاک به جانی دهی</p></div>
<div class="m2"><p>یک جو کَهگِل به جهانی دهی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای ز تو بالای زمین زیر رنج</p></div>
<div class="m2"><p>جای تو هم زیر زمین بِه چو گنج</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>روغن مغز تو که سیمابی است</p></div>
<div class="m2"><p>سرد بدین فندق سنجابی است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تات چو فندق نکند خانه تنگ</p></div>
<div class="m2"><p>بگذر ازین فندق سنجاب رنگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روز و شب از قاقم و قندز جداست</p></div>
<div class="m2"><p>این دلهٔ پیسه پلنگ اژدهاست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گُربه نِه‌ای دست درازی مکن</p></div>
<div class="m2"><p>با دلهٔ دِه، دله بازی مکن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شیر تَنیدست درین ره لعاب</p></div>
<div class="m2"><p>سر چو گَوَزنان چه نهی سوی آب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر فلکت عشوه آبی دهد</p></div>
<div class="m2"><p>تا نفریبی که سرابی دهد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تیز مران کآب فلک دیده‌ای</p></div>
<div class="m2"><p>آب دهن خور که نمک دیده‌ای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا نشوی تشنه به تدبیر باش</p></div>
<div class="m2"><p>سوخته خرمن، چو تباشیر باش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یوسف تو تا ز بر چاه بود</p></div>
<div class="m2"><p>مصر الهیش نظرگاه بود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زرد رخ از چرخ کبود آمدی</p></div>
<div class="m2"><p>چونکه درین چاه فرود آمدی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اینهمه صفرای تو بر روی زرد</p></div>
<div class="m2"><p>سرکهٔ ابروی تو کاری نکرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پیه تو چون روغن صد ساله بود</p></div>
<div class="m2"><p>سرکه ده ساله بر ابرو چه سود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خون پدر دیده درین هفتخوان</p></div>
<div class="m2"><p>آب مریز از پی این هفت نان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آتش در خرمن خود میزنی</p></div>
<div class="m2"><p>دولت خود را به لگد میزنی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>می‌تک و می‌تاز که میدان تُراست</p></div>
<div class="m2"><p>کار بفرمای که فرمان تُراست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>این دو سه روزی که شدی جام گیر</p></div>
<div class="m2"><p>خوش خور و خوش خُسب و خوش آرام گیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هم به تو بر سخت جفا کرده‌اند</p></div>
<div class="m2"><p>زان رسنت سست رها کرده‌اند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>لنگ شده پای و میان گشته کوز</p></div>
<div class="m2"><p>سوختهٔ روغن خویشی هنوز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>لاجرم اینجا دغل مطبخی</p></div>
<div class="m2"><p>روز قیامت علف دوزخی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>پر شده گیر این شکم از آب و نان</p></div>
<div class="m2"><p>ای سبک آنگاه نباشی گران؟</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گر به خُورِش، بیش کسی زیستی</p></div>
<div class="m2"><p>هر که بسی خورد بسی زیستی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>عمر کم است از پی آن پر بهاست</p></div>
<div class="m2"><p>قیمت عمر از کمی عمر خاست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کم خور و بسیاری راحت نگر</p></div>
<div class="m2"><p>بیش خور و بیش جراحت نگر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>عقل تو با خورد چه بازار داشت</p></div>
<div class="m2"><p>حرص تُرا بر سر اینکار داشت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>حرص تو از فتنه بود ناشکیب</p></div>
<div class="m2"><p>بگذر ازین ابله زیرک فریب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>حرص تو را عقل بدان داده‌اند</p></div>
<div class="m2"><p>کان نخوری کت نفرستاده‌اند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ترسم ازین پیشه که پیشت کند</p></div>
<div class="m2"><p>رنگ پذیرندهٔ خویشت کند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هر به دو نیکی که درین محضرند</p></div>
<div class="m2"><p>رنگ پذیرندهٔ یکدیگرند</p></div></div>