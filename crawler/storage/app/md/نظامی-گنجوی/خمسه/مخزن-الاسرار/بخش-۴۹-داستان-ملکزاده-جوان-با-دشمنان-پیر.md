---
title: >-
    بخش ۴۹ - داستان ملکزاده جوان با دشمنان پیر
---
# بخش ۴۹ - داستان ملکزاده جوان با دشمنان پیر

<div class="b" id="bn1"><div class="m1"><p>قصد شنیدم که در اقصای مرو</p></div>
<div class="m2"><p>بود ملکزاده جوانی چو سرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مضطرب از دولتیان دیار</p></div>
<div class="m2"><p>ملک بر او شیفته چون روزگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تازگیش را کهنان در ستیز</p></div>
<div class="m2"><p>پر خطر او زان خطر نیم خیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک شب از آن فتنه پر اندیشه خفت</p></div>
<div class="m2"><p>دید که پیریش در آن خواب گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کای مه نو برج کهن را بکن</p></div>
<div class="m2"><p>و ای گل نو شاخ کهن را بزن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به تو بر ملک مقرر شود</p></div>
<div class="m2"><p>عیش تو از خوی تو خوشتر شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شه چو سر از خواب گران بر گرفت</p></div>
<div class="m2"><p>آن دو سه تن را ز میان برگرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تازه بنا کرد و کهن در نوشت</p></div>
<div class="m2"><p>ملک بر آن تازه ملک تازه گشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخنه کن ملک سرافکنده به</p></div>
<div class="m2"><p>لشگر بد عهد پراکنده به</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر نکشد شاخ تو از سرو بن</p></div>
<div class="m2"><p>تا نزنی گردن شاخ کهن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا نشود بسته لب جویبار</p></div>
<div class="m2"><p>پنجه دعوی نگشاید چنار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا نکنی رهگذر چشمه پاک</p></div>
<div class="m2"><p>آب نزاید ز دل و چشم خاک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با تو برون از تو برون پروریست</p></div>
<div class="m2"><p>گوش ترا نیک نصیحت گریست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یک نفس آن تیغ برآر از غلاف</p></div>
<div class="m2"><p>چند غلافش کنی ای بر خلاف</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن نفس از حقه این خاک نیست</p></div>
<div class="m2"><p>این حق آن هم نفس پاک نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیش چنین کس همگی پیش کش</p></div>
<div class="m2"><p>نام کرم بر همه خویش کش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دولتیان کام  و درم یافتند</p></div>
<div class="m2"><p>دولت باقی ز کرم یافتند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تخم کرم کشت سلامت بود</p></div>
<div class="m2"><p>چون برسد برگ قیامت بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یارب  از آن گنج که احسان تست</p></div>
<div class="m2"><p>نقد نظامی سره کن کان تست</p></div></div>