---
title: >-
    بخش ۱ - آغاز سخن
---
# بخش ۱ - آغاز سخن

<div class="b" id="bn1"><div class="m1"><p>بسم‌الله الرحمن الرحیم</p></div>
<div class="m2"><p>هست کلید در گنج حکیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فاتحه فکرت و ختم سخن</p></div>
<div class="m2"><p>نام خدایست بر او ختم کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش وجود همه آیندگان</p></div>
<div class="m2"><p>بیش بقای همه پایندگان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سابقه سالار جهان قدم</p></div>
<div class="m2"><p>مرسله پیوند گلوی قلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرده گشای فلک پرده‌دار</p></div>
<div class="m2"><p>پردگی پرده شناسان کار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مبدع هر چشمه که جودیش هست</p></div>
<div class="m2"><p>مخترع هر چه وجودیش هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لعل طراز کمر آفتاب</p></div>
<div class="m2"><p>حله گر خاک و حلی بند آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرورش‌آموز درون پروران</p></div>
<div class="m2"><p>روز برآرنده روزی خوران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مهره کش رشته باریک عقل</p></div>
<div class="m2"><p>روشنی دیده تاریک عقل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داغ نه ناصیه داران پاک</p></div>
<div class="m2"><p>تاج ده تخت نشینان خاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خام کن پخته تدبیرها</p></div>
<div class="m2"><p>عذر پذیرنده تقصیرها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شحنه غوغای هراسندگان</p></div>
<div class="m2"><p>چشمه تدبیر شناسندگان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اول و آخر بوجود و صفات</p></div>
<div class="m2"><p>هست کن و نیست کن کاینات</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با جبروتش که دو عالم کمست</p></div>
<div class="m2"><p>اول ما آخر ما یکدمست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کیست درین دیر گه دیر پای</p></div>
<div class="m2"><p>کو لمن الملک زند جز خدای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود و نبود آنچه بلندست و پست</p></div>
<div class="m2"><p>باشد و این نیز نباشد که هست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پرورش آموختگان ازل</p></div>
<div class="m2"><p>مشکل این کار نکردند حل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کز ازلش علم چه دریاست این</p></div>
<div class="m2"><p>تا ابدش ملک چه صحراست این</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اول او اول بی ابتداست</p></div>
<div class="m2"><p>آخر او آخر بی‌انتهاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روضه ترکیب ترا حور ازوست</p></div>
<div class="m2"><p>نرگس بینای ترا نور ازوست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کشمکش هر چه در و زندگیست</p></div>
<div class="m2"><p>پیش خداوندی او بندگیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر چه جز او هست بقائیش نیست</p></div>
<div class="m2"><p>اوست مقدس که فنائیش نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>منت او راست هزار آستین</p></div>
<div class="m2"><p>بر کمر کوه و کلاه زمین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا کرمش در تتق نور بود</p></div>
<div class="m2"><p>خار زگل نی زشکر دور بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون که به جودش کرم آباد شد</p></div>
<div class="m2"><p>بند وجود از عدم آزاد شد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در هوس این دو سه ویرانه ده</p></div>
<div class="m2"><p>کار فلک بود گره در گره</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا نگشاد این گره وهم سوز</p></div>
<div class="m2"><p>زلف شب ایمن نشد از دست روز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون گهر عقد فلک دانه کرد</p></div>
<div class="m2"><p>جعد شب از گرد عدم شانه کرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زین دو سه چنبر که بر افلاک زد</p></div>
<div class="m2"><p>هفت گره بر کمر خاک زد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کرد قبا جبه خورشید و ماه</p></div>
<div class="m2"><p>زین دو کله‌وار سپید و سیاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زهره میغ از دل دریا گشاد</p></div>
<div class="m2"><p>چشمه خضر از لب خضرا گشاد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جام سحر در گل شبرنگ ریخت</p></div>
<div class="m2"><p>جرعه آن در دهن سنگ ریخت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زاتش و آبی که بهم در شکست</p></div>
<div class="m2"><p>پیه در و گرده یاقوت بست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خون دل خاک زبحران باد</p></div>
<div class="m2"><p>در جگر لعل جگرگون نهاد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>باغ سخا را چو فلک تازه کرد</p></div>
<div class="m2"><p>مرغ سخن را فلک آوازه کرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نخل زبانرا رطب نوش داد</p></div>
<div class="m2"><p>در سخن را صدف گوش داد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پرده‌نشین کرد سر خواب را</p></div>
<div class="m2"><p>کسوت جان داد تن آب را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زلف زمین در بر عالم فکند</p></div>
<div class="m2"><p>خال (عصی) بر رخ آدم فکند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>روی زر از صورت خواری بشست</p></div>
<div class="m2"><p>حیض گل از ابر بهاری بشست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زنگ هوا را به کواکب سترد</p></div>
<div class="m2"><p>جان صبا را به ریاحین سپرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خون جهان در جگر گل گرفت</p></div>
<div class="m2"><p>نبض خرد در مجس دل گرفت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خنده به غمخوارگی لب کشاند</p></div>
<div class="m2"><p>زهره به خنیاگری شب نشاند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ناف شب از مشک فروشان اوست</p></div>
<div class="m2"><p>ماه نو از حلقه به گوشان اوست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پای سخنرا که درازست دست</p></div>
<div class="m2"><p>سنگ سراپرده او سر شکست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>وهم تهی پای بسی ره نبشت</p></div>
<div class="m2"><p>هم زدرش دست تهی بازگشت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>راه بسی رفت و ضمیرش نیافت</p></div>
<div class="m2"><p>دیده بسی جست و نظیرش نیافت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>عقل درآمد که طلب کردمش</p></div>
<div class="m2"><p>ترک ادب بود ادب کردمش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هر که فتاد از سر پرگار او</p></div>
<div class="m2"><p>جمله چو ما هست طلبگار او</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سدره نشینان سوی او پر زدند</p></div>
<div class="m2"><p>عرش روان نیز همین در زدند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گر سر چرخست پر از طوق اوست</p></div>
<div class="m2"><p>ور دل خاکست پر از شوق اوست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زندهٔ نام جبروتش احد</p></div>
<div class="m2"><p>پایه تخت ملکوتش ابد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خاص نوالش نفس خستگان</p></div>
<div class="m2"><p>پیک روانش قدم بستگان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دل که زجان نسبت پاکی کند</p></div>
<div class="m2"><p>بر در او دعوی خاکی کند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>رسته خاک در او دانه‌ایست</p></div>
<div class="m2"><p>کز گل باغش ارم افسانه‌ایست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خاک نظامی که بتایید اوست</p></div>
<div class="m2"><p>مزرعه دانه توحید اوست</p></div></div>