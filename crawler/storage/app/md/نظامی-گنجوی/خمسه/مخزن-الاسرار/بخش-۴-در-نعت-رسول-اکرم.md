---
title: >-
    بخش ۴ - در نعت رسول اکرم
---
# بخش ۴ - در نعت رسول اکرم

<div class="b" id="bn1"><div class="m1"><p>تخته اول که الف نقش بست</p></div>
<div class="m2"><p>بر در محجوبه احمد نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حلقه حی را کالف اقلیم داد</p></div>
<div class="m2"><p>طوق ز دال و کمر از میم داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاجرم او یافت از آن میم و دال</p></div>
<div class="m2"><p>دایره دولت و خط کمال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود درین گنبد فیروزه خشت</p></div>
<div class="m2"><p>تازه ترنجی زسرای بهشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسم ترنجست که در روزگار</p></div>
<div class="m2"><p>پیش دهد میوه پس آرد بهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنت نبیا چو علم پیش برد</p></div>
<div class="m2"><p>ختم نبوت به محمد سپرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مه که نگین دان زبرجد شدست</p></div>
<div class="m2"><p>خاتم او مهر محمد شدست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوش جهان حلقه کش میم اوست</p></div>
<div class="m2"><p>خود دو جهان حلقه تسلیم اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواجه مساح و مسیحش غلام</p></div>
<div class="m2"><p>آنت بشیر اینت مبشر به نام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امی گویا به زبان فصیح</p></div>
<div class="m2"><p>از الف آدم و میم مسیح</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو الف راست به عهد و وفا</p></div>
<div class="m2"><p>اول و آخر شده بر انبیا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نقطه روشن‌تر پرگار کن</p></div>
<div class="m2"><p>نکته پرگارترین سخن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از سخن او ادب آوازه‌ای</p></div>
<div class="m2"><p>وز کمر او فلک اندازه‌ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کبر جهان گرچه بسر بر نکرد</p></div>
<div class="m2"><p>سر به جهان هم به جهان در نکرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عصمتیان در حرمش پردگی</p></div>
<div class="m2"><p>عصمت از او یافته پروردگی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تربتش از دیده جنایت ستان</p></div>
<div class="m2"><p>غربتش از مکه جبایت ستان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خامشی او سخن دلفروز</p></div>
<div class="m2"><p>دوستی او هنر عیب سوز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فتنه فرو کشتن ازو دلپذیر</p></div>
<div class="m2"><p>فتنه شدن نیز برو ناگزیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر همه سر خیل و سر خیر بود</p></div>
<div class="m2"><p>قطب گرانسنگ سبک سیر بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شمع الهی ز دل افروخته</p></div>
<div class="m2"><p>درس ازل تا ابد آموخته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چشمه خورشید که محتاج اوست</p></div>
<div class="m2"><p>نیم هلال از شب معراج اوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تخت نشین شب معراج بود</p></div>
<div class="m2"><p>تخت نشان کمر و تاج بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>داده فراخی نفس تنگ را</p></div>
<div class="m2"><p>نعل زده خنگ شب آهنگ را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از پی باز آمدنش پای بست</p></div>
<div class="m2"><p>موکبیان سخن ابلق بدست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون تک ابلق بتمامی رسید</p></div>
<div class="m2"><p>غاشیه داری به نظامی رسید</p></div></div>