---
title: >-
    بخش ۷ - نعت دوم
---
# بخش ۷ - نعت دوم

<div class="b" id="bn1"><div class="m1"><p>ای تن تو پاک‌تر از جان پاک</p></div>
<div class="m2"><p>روح تو پرورده روحی فداک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقطه گه خانه رحمت توئی</p></div>
<div class="m2"><p>خانه بر نقطه زحمت توئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راهروان عربی را تو ماه</p></div>
<div class="m2"><p>یاوگیان عجمی را تو راه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ره به تو یابند و تو ره ده نه‌ای</p></div>
<div class="m2"><p>مهتر ده خود تو و در ده نه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون تو کریمان که تماشا کنند</p></div>
<div class="m2"><p>رستی تنها نه به تنها کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سر خوانی که رطب خورده‌ای</p></div>
<div class="m2"><p>از پی ما زله چه آورده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لب بگشا تا همه شکر خورند</p></div>
<div class="m2"><p>ز آب دهانت رُطَب‌ِ تَر خورند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای شب گیسوی تو روز نجات</p></div>
<div class="m2"><p>آتش سودای تو آب حیات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل شده شیفته روی تو</p></div>
<div class="m2"><p>سلسله شیفتگان موی تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرخ ز طوق کمرت بنده‌ای</p></div>
<div class="m2"><p>صبح ز خورشید رخت خنده‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عالَمِ تَر، دامنِ خشک ،از تو یافت</p></div>
<div class="m2"><p>ناف زمین نافه مشک از تو یافت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از اثر خاک تو مشکین غبار</p></div>
<div class="m2"><p>پیکر آن بوم شده مشک بار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاک تو از باد سلیمان به است</p></div>
<div class="m2"><p>روضه چه گویم که ز رضوان به است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کعبه که سجاده تکبیر تست</p></div>
<div class="m2"><p>تشنه جلاب تباشیر تست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تاج تو و تخت تو دارد جهان</p></div>
<div class="m2"><p>تخت زمین آمد و تاج آسمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سایه نداری تو که نور مهی</p></div>
<div class="m2"><p>رو تو که خود سایه نور اللهی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چار علم رکن مسلمانیت</p></div>
<div class="m2"><p>پنج دعا نوبت سلطانیت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خاک ذلیلان شده گلشن به تو</p></div>
<div class="m2"><p>چشم غریبان شده روشن به تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا قدمت در شب گیسو فشان</p></div>
<div class="m2"><p>بر سر گردون شده دامن کشان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پر زر و در گشته ز تو دامنش</p></div>
<div class="m2"><p>خشتک زر سوزه پیراهنش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در صدف صبح به دست صفا</p></div>
<div class="m2"><p>غالیه بوی تو سایَد صبا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لاجرم آنجا که صبا تاخته</p></div>
<div class="m2"><p>لشگر عنبر علم انداخته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بوی کز آن عنبر لرزان دهی</p></div>
<div class="m2"><p>گر به دو عالم دهی ارزان دهی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سدره ز آرایش صدرت زهیست</p></div>
<div class="m2"><p>عرش در ایوان تو کرسی نهیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>روزن حاجت چو بود صبح تاب</p></div>
<div class="m2"><p>ذره بود عرش در آن آفتاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گرنه ز صبح آینه بیرون فتاد</p></div>
<div class="m2"><p>نور تو بر خاک زمین چون فتاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای دو جهان، زیر زمین از چه‌ای؟</p></div>
<div class="m2"><p>گنج نه‌ای خاک نشین از چه‌ای؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا تو به خاک اندری ای گنج پاک</p></div>
<div class="m2"><p>شرط بود گنج سپردن به خاک</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گنج ترا فقر تو ویرانه بس</p></div>
<div class="m2"><p>شمع ترا ظِلِّ تو پروانه بس</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چرخ مقوس هدف آه تست</p></div>
<div class="m2"><p>چنبر دلوش رسن چاه تست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ایندو طرف گرد سپید و سیاه</p></div>
<div class="m2"><p>راه تو را پیک ز پیکان راه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عقل شفا جوی و طبیبش توئی</p></div>
<div class="m2"><p>ماه سفرساز و غریبش توئی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خیز و شب منتظران روز کن</p></div>
<div class="m2"><p>طبع نظامی طرب افروز کن</p></div></div>