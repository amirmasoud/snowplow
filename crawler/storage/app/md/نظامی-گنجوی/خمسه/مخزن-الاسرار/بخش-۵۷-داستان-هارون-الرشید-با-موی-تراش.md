---
title: >-
    بخش ۵۷ - داستان هارون‌الرشید با موی تراش
---
# بخش ۵۷ - داستان هارون‌الرشید با موی تراش

<div class="b" id="bn1"><div class="m1"><p>دور خلافت چو به هارون رسید</p></div>
<div class="m2"><p>رایت عباس به گردون رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیم شبی پشت به همخوابه کرد</p></div>
<div class="m2"><p>روی در آسایش گرمابه کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موی تراشی که سرش میسترد</p></div>
<div class="m2"><p>موی به مویش به غمی میسپرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کای شده آگاه ز استادیم</p></div>
<div class="m2"><p>خاص کن امروز به دامادیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خطبه تزویج پراکنده کن</p></div>
<div class="m2"><p>دختر خود نامزد بنده کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبع خلیفه قدری گرم گشت</p></div>
<div class="m2"><p>باز پذیرنده آزرم گشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت حرارت جگرش تافتست</p></div>
<div class="m2"><p>وحشتی از دهشت من یافتست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیخودیش کرد چنین یافه‌گوی</p></div>
<div class="m2"><p>ورنه نکردی ز من این جستجوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روز دگر نیکترش آزمود</p></div>
<div class="m2"><p>بر درم قلب همان سکه بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تجربتش کرد چنین چند بار</p></div>
<div class="m2"><p>قاعدهٔ مرد نگشت از قرار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کار چو بی رونقی از نور برد</p></div>
<div class="m2"><p>قصه به دستوری دستور برد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کز قلم موی تراشی درست</p></div>
<div class="m2"><p>بر سرم این آمد و این سر به تست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منصب دامادی من بایدش</p></div>
<div class="m2"><p>ترک ادب بین که چه فرمایدش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هرگه کاید چو قضا بر سرم</p></div>
<div class="m2"><p>سنگ دراندازد در گوهرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در دهنش خنجر و در دست تیغ</p></div>
<div class="m2"><p>سر به دو شمشیر سپارم دریغ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت وزیر ایمنی از رای او</p></div>
<div class="m2"><p>بر سر گنجست مگر پای او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چونکه رسد بر سرت آن ساده مرد</p></div>
<div class="m2"><p>گو ز قدمگاه نخستین بگرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر بچخد گردن گرابزن</p></div>
<div class="m2"><p>ورنه قدمگاه نخستین بکن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>میر مطیع از سر طوعی که بود</p></div>
<div class="m2"><p>جای بدل کرد به نوعی که بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون قدم از منزل اول برید</p></div>
<div class="m2"><p>گونه حلاق دگرگونه دید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کم سخنی دید دهن دوخته</p></div>
<div class="m2"><p>چشم و زبانی ادب آموخته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا قدمش بر سر گنجینه بود</p></div>
<div class="m2"><p>صورت شاهیش در آیینه بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون قدم از گنج تهی ساز کرد</p></div>
<div class="m2"><p>کلبه حلاقی خود باز کرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زود قدمگاهش بشکافتند</p></div>
<div class="m2"><p>گنج به زیر قدمش یافتند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرکه قدم بر سر گنجی نهاد</p></div>
<div class="m2"><p>چون به سخن آمد گنجی گشاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گنج نظامی که طلسم افکنست</p></div>
<div class="m2"><p>سینه صافی و دل روشنست</p></div></div>