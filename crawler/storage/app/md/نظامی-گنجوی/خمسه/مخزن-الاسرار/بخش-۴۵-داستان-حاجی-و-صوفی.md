---
title: >-
    بخش ۴۵ - داستان حاجی و صوفی
---
# بخش ۴۵ - داستان حاجی و صوفی

<div class="b" id="bn1"><div class="m1"><p>کعبه روی عزم ره آغاز کرد</p></div>
<div class="m2"><p>قاعده کعبه روان ساز کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآنچه فزون از غرض کار داشت</p></div>
<div class="m2"><p>مبلغ یک بدره دینار داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت فلان صوفی آزاد مرد</p></div>
<div class="m2"><p>کاستن از عالم کوتاه گرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دلم آید که دیانت در اوست</p></div>
<div class="m2"><p>در کس اگر نیست امانت در اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفت و نهانیش فرا خانه برد</p></div>
<div class="m2"><p>بدرهٔ دینار به صوفی سپرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت نگه دار در این پرده راز</p></div>
<div class="m2"><p>تا چو من آیم به من آریش باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواجه ره بادیه را درگرفت</p></div>
<div class="m2"><p>شیخ زر عاریه را برگرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یارب و زنهار که خود چند بود</p></div>
<div class="m2"><p>تا دل درویش در آن بند بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت به زر کار خود آراستم</p></div>
<div class="m2"><p>یافتم آن گنج که می‌خواستم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زود خورم تا نکند بستگی</p></div>
<div class="m2"><p>آنچه خدا داد به آهستگی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باز گشاد از گره آن بند را</p></div>
<div class="m2"><p>داد طرب داد شبی چند را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جملهٔ آن زر که بر خویش داشت</p></div>
<div class="m2"><p>بذل شکم کرد و شکم پیش داشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دست بدان حقه دینار کرد</p></div>
<div class="m2"><p>زلف بتان حلقه زنار کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خرقهٔ شیخانه شده شاخ شاخ</p></div>
<div class="m2"><p>تنگدلی مانده و عذری فراخ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صید چنان خورد که داغش نماند</p></div>
<div class="m2"><p>روغنی از بهر چراغش نماند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حاجی ما چون ز سفر گشت باز</p></div>
<div class="m2"><p>کرد بران هندوی خود ترکتاز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت بیاور به من ای تیزهوش</p></div>
<div class="m2"><p>گفت چه؟ گفتا زر، گفتا خموش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در کرم آویز و رها کن لجاج</p></div>
<div class="m2"><p>از ده ویران که ستاند خراج</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صرف شد آن بدره هوا در هوا</p></div>
<div class="m2"><p>مفلس و بدره ز کجا تا کجا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غارتی از ترک نبرده‌ست کس</p></div>
<div class="m2"><p>رخت به هندو نسپرده‌ست کس</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رکنی تو رکن دلم را شکست</p></div>
<div class="m2"><p>خردم از آن خرده که بر من نشست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مال به صد خنده به تاراج داد</p></div>
<div class="m2"><p>رفت و به صد گریه به پا ایستاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفت کرم کن که پشیمان شدیم</p></div>
<div class="m2"><p>کافر بودیم و مسلمان شدیم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>طبع جهان از خلل آبستن است</p></div>
<div class="m2"><p>گر خللی رفت خطا بر من است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا کرمش گفت به صد رستخیز</p></div>
<div class="m2"><p>خیز که درویش بپای است خیز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سیم خدا چون به خدا بازگشت</p></div>
<div class="m2"><p>سیم کشی کرد و ازاو درگذشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ناصح خود شد که بدین در مپیچ</p></div>
<div class="m2"><p>هیچ ندارد چه ستانم ز هیچ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زو چه ستانم که جوی نیستش</p></div>
<div class="m2"><p>جز گرویدن گروی نیستش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آنچه از آن مال درین صوفی است</p></div>
<div class="m2"><p>میم مطوق الف کوفی است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفت نخواهی که وبالت کنم</p></div>
<div class="m2"><p>وانچه حرام است حلالت کنم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دست بدار ای چو فلک زرق ساز</p></div>
<div class="m2"><p>زآستن کوته و دست دراز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هیچ دل از حرص و حسد پاک نیست</p></div>
<div class="m2"><p>معتمدی بر سر این خاک نیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دین سره نقدیست به شیطان مده</p></div>
<div class="m2"><p>یارهٔ فغفور به سگبان مده</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر دهی ای خواجه غرامت تراست</p></div>
<div class="m2"><p>مایه ز مفلس نتوان باز خواست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>منزل عیب است هنر توشه رو</p></div>
<div class="m2"><p>دامن دین گیر و فرا گوشه رو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چرخ نه بر بی‌درمان می‌زند</p></div>
<div class="m2"><p>قافلهٔ محتشمان می‌زند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شحنه این راه چو غارتگر است</p></div>
<div class="m2"><p>مفلسی از محتشمی بهتر است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دیدم از آنجا که جهان بینی است</p></div>
<div class="m2"><p>کآفت زنبور ز شیرینی است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شیر مگر تلخ بدان گشت خود</p></div>
<div class="m2"><p>کز پس مرگش نخورد دام و دد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شمع ز برخاستنی وا نشست</p></div>
<div class="m2"><p>مه ز تمامی طلبیدن شکست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>باد که با خاک به گرگ آشتیست</p></div>
<div class="m2"><p>ایمن از این راه ز ناداشتیست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مرغ شمر را مگر آگاهی است</p></div>
<div class="m2"><p>کآفت ماهی درم ماهی است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زر که ترازوی نیاز تو شد</p></div>
<div class="m2"><p>فاتحهٔ پنج نماز تو شد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پاک نگردی ز ره این نیاز</p></div>
<div class="m2"><p>تا چو نظامی نشوی پاکباز</p></div></div>