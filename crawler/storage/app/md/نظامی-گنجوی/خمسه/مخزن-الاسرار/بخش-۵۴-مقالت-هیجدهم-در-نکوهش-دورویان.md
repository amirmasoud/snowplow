---
title: >-
    بخش ۵۴ - مقالت هیجدهم در نکوهش دورویان
---
# بخش ۵۴ - مقالت هیجدهم در نکوهش دورویان

<div class="b" id="bn1"><div class="m1"><p>قلب زنی چند که برخاستند</p></div>
<div class="m2"><p>قالبی از قلب نو آراستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شکم از روی بکن پشتشان</p></div>
<div class="m2"><p>حرف نگهدار ز انگشتشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش تو از نور موافق‌ترند</p></div>
<div class="m2"><p>وز پَسَت از سایه منافق‌ترند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساده‌تر از شمع و گره‌تر ز عود</p></div>
<div class="m2"><p>ساده به دیدار و کره در وجود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جور پذیران عنایت گذار</p></div>
<div class="m2"><p>عیب نویسان شکایت شمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهر، دهن در دهن آموخته</p></div>
<div class="m2"><p>کینه، گره بر گره اندوخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرم ولیک از جگر افسرده‌تر</p></div>
<div class="m2"><p>زنده ولی از دل خود مرده‌تر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صحبتشان بر محل در مزن</p></div>
<div class="m2"><p>مست نه‌ای پای درین گل مزن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خازن کوهند مگو رازشان</p></div>
<div class="m2"><p>غمز نخواهی مده آوازشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لاف زنان کز تو عزیزی شوند</p></div>
<div class="m2"><p>جهد کنان کز تو به چیزی شوند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون بود آن صلح ز ناداشتی</p></div>
<div class="m2"><p>خشم خدا باد بر آن آشتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر نفسی کان غرض‌آمیز شد</p></div>
<div class="m2"><p>دوستیی دشمنی‌انگیز شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دوستیی کان ز توئی و منیست</p></div>
<div class="m2"><p>نسبت آن دوستی از دشمنیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زهر ترا دوست چه خواند؟ شکر</p></div>
<div class="m2"><p>عیب ترا دوست چه داند؟ هنر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دوست بود مرهم راحت رسان</p></div>
<div class="m2"><p>گرنه رها کن سخن ناکسان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گربه بود کز سر هم پوستی</p></div>
<div class="m2"><p>بچه خود را خورد از دوستی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دوست کدام؟ آنکه بود پرده‌دار</p></div>
<div class="m2"><p>پرده‌درند اینهمه چون روزگار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جمله بر آن کز تو سبق چون برند</p></div>
<div class="m2"><p>سکه کارت بچه افسون برند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با تو عنان بسته صورت شوند</p></div>
<div class="m2"><p>وقت ضرورت به ضرورت شوند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دوستی هر که ترا روشن است</p></div>
<div class="m2"><p>چون دلت انکار کند دشمن است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تن چه شناسد که ترا یار کیست</p></div>
<div class="m2"><p>دل بود آگه که وفادار کیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکدل داری و غم دل هزار</p></div>
<div class="m2"><p>یک گل پژمرده و صد نیش خار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ملک هزارست و فریدون یکی</p></div>
<div class="m2"><p>غالیه بسیار و دماغ اندکی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پرده درد هر چه درین عالمست</p></div>
<div class="m2"><p>راز ترا هم دل تو محرمست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون دل تو بند ندارد بر آن</p></div>
<div class="m2"><p>قفل چه خواهی ز دل دیگران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گرنه تنک دل شده‌ای وین خطاست</p></div>
<div class="m2"><p>راز تو چون روز به صحرا چراست؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر دل تو نز تنکی راز گفت</p></div>
<div class="m2"><p>شیشه که می خورد چرا باز گفت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون بود از همنفسی ناگزیر</p></div>
<div class="m2"><p>همنفسی را ز نفس وا مگیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پای نهادی چو درین داوری</p></div>
<div class="m2"><p>کوش که همدست به دست آوری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا نشناسی گهر یار خویش</p></div>
<div class="m2"><p>یاوه مکن گوهر اسرار خویش</p></div></div>