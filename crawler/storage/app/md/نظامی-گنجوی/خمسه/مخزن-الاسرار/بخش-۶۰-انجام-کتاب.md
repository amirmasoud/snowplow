---
title: >-
    بخش ۶۰ - انجام کتاب
---
# بخش ۶۰ - انجام کتاب

<div class="b" id="bn1"><div class="m1"><p>صَبَّحَکَ الله صباح ای دبیر</p></div>
<div class="m2"><p>چون قلم از دست شدم دستگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاین نمط از چرخ فزونی کند</p></div>
<div class="m2"><p>با قلمم بوقلمونی کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین همه الماس که بگداختم</p></div>
<div class="m2"><p>گزلکی از بهر ملک ساختم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاهن شمشیرم در سنگ بود</p></div>
<div class="m2"><p>کوره آهنگریم تنگ بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دولت اگر همدمیئی ساختی</p></div>
<div class="m2"><p>بخت بدین نیز نپرداختی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دلم آید که گنه کرده‌ام</p></div>
<div class="m2"><p>کین ورقی چند سیه کرده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچه درین حجله خرگاهی است</p></div>
<div class="m2"><p>جلوه‌گری چند سحرگاهی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین بره میخور چه خوری دودها</p></div>
<div class="m2"><p>آتش در زن به نمک سودها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیش رو آهستگیی پیشه کن</p></div>
<div class="m2"><p>گر کنی اندیشه به اندیشه کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر سخنی کز ادبش دوری است</p></div>
<div class="m2"><p>دست بر او مال که دستوری است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>و آنچه نه از عِلم برآرد عَلَم</p></div>
<div class="m2"><p>گر منم آن حرف درو کش قلم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر نه درو داد سخن دادمی</p></div>
<div class="m2"><p>شهر به شهرش نفرستادمی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این طرفم کرد سخن پای بست</p></div>
<div class="m2"><p>جمله اطراف مرا زیردست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت زمانه نه زمینی بجنب</p></div>
<div class="m2"><p>چون ز منان چند نشینی بجنب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بکر معانیم که همتاش نیست</p></div>
<div class="m2"><p>جامه باندازه بالاش نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیم تنی تا سر زانوش هست</p></div>
<div class="m2"><p>از سر آن بر سر زانو نشست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بایدش از حله قد آراستن</p></div>
<div class="m2"><p>تا ادبش باشد برخاستن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از نظر هر کهن و تازه‌ای</p></div>
<div class="m2"><p>حاصل من چیست جز آوازه‌ای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرمی هنگامه و زر هیچ نه</p></div>
<div class="m2"><p>زحمت بازار و دگر هیچ نه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گنجه گره کرده گریبان من</p></div>
<div class="m2"><p>بی گرهی گنج عراق آن من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بانگ برآورد جهان کای غلام</p></div>
<div class="m2"><p>گنجه کدام است و نظامی کدام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شکر که این نامه به عنوان رسید</p></div>
<div class="m2"><p>پیشتر از عمر به پایان رسید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کرد نظامی ز پی زیورش</p></div>
<div class="m2"><p>غرقه گوهر ز قدم تا سرش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>باد مبارک گهر افشان او</p></div>
<div class="m2"><p>بر ملکی کاین گهر است آن او</p></div></div>