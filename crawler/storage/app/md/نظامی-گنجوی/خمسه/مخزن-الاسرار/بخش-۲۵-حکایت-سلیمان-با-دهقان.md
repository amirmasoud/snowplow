---
title: >-
    بخش ۲۵ - حکایت سلیمان با دهقان
---
# بخش ۲۵ - حکایت سلیمان با دهقان

<div class="b" id="bn1"><div class="m1"><p>روزی از آنجا که فراغی رسید</p></div>
<div class="m2"><p>باد سلیمان به چراغی رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مملکتش رخت به صحرا نهاد</p></div>
<div class="m2"><p>تخت بر این تخته مینا نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دید بنوعی که دلش پاره گشت</p></div>
<div class="m2"><p>برزگری پیر در آن ساده دشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خانه ز مشتی غله پرداخته</p></div>
<div class="m2"><p>در غله دان کرم انداخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانه فشان گشته بهر گوشه‌ای</p></div>
<div class="m2"><p>رسته ز هر دانه او خوشه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرده آن دانه که دهقان گشاد</p></div>
<div class="m2"><p>منطق مرغان ز سلیمان گشاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت جوانمرد شو ای پیرمرد</p></div>
<div class="m2"><p>کاین قَدَرَت بود، ببایست خورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دام نه‌ای دانه فشانی مکن</p></div>
<div class="m2"><p>با چو منی مرغ زبانی مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیل نداری گل صحرا مخار</p></div>
<div class="m2"><p>آب نیابی جو دهقان مکار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما که به سیراب زمین کاشتیم</p></div>
<div class="m2"><p>زانچه بِکِشتیم چه برداشتیم؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا تو درین مزرعهٔ دانه سوز</p></div>
<div class="m2"><p>تشنه و بی آب چه آری بروز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیر بدو گفت مرنج از جواب</p></div>
<div class="m2"><p>فارغم از پرورش خاک و آب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با تر و با خشک مرا نیست کار</p></div>
<div class="m2"><p>دانه ز من پرورش از کردگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آب من اینک عرق پشت من</p></div>
<div class="m2"><p>بیل من اینک سرانگشت من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیست غم ملک و ولایت مرا</p></div>
<div class="m2"><p>تا منم این دانه کفایت مرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه بشارت به خودم میدهد</p></div>
<div class="m2"><p>دانه یکی هفتصدم میدهد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دانه به انبازی شیطان مکار</p></div>
<div class="m2"><p>تا ز یکی هفتصد آید به بار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دانهٔ شایسته بباید نخست</p></div>
<div class="m2"><p>تا گره خوشه گشاید درست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر نظری را که برافروختند</p></div>
<div class="m2"><p>جامه باندازه تن دوختند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رخت مسیحا نکشد هر خری</p></div>
<div class="m2"><p>محرم دولت نبود هر سری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کرگدنی گردن پیلی خورد</p></div>
<div class="m2"><p>مور ز پای ملخی نگذرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بحر به صد رود شد آرام گیر</p></div>
<div class="m2"><p>جوی به یک سیل برآرد نفیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هست در این دایرهٔ لاجورد</p></div>
<div class="m2"><p>مرتبه مرد بمقدار مرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دولتیی باید صاحب درنگ</p></div>
<div class="m2"><p>کز قدری ناز نیاید بتنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر نفسی حوصله ناز نیست</p></div>
<div class="m2"><p>هر شکمی حاملهٔ راز نیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ناز نگویم که ز خامی بود</p></div>
<div class="m2"><p>ناز کشی کار نظامی بود</p></div></div>