---
title: >-
    بخش ۲۳ - حکایت نوشیروان با وزیر خود
---
# بخش ۲۳ - حکایت نوشیروان با وزیر خود

<div class="b" id="bn1"><div class="m1"><p>صیدکنان مرکب نوشیروان</p></div>
<div class="m2"><p>دور شد از کوکبه خسروان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مونس خسرو شده دستور و بس</p></div>
<div class="m2"><p>خسرو و دستور و دگر هیچکس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه در آن ناحیت صید یاب</p></div>
<div class="m2"><p>دید دهی چون دل دشمن خراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنگ دو مرغ آمده در یکدِگَر</p></div>
<div class="m2"><p>وز دل شه قافیه‌شان تنگتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت به دستور چه دم میزنند</p></div>
<div class="m2"><p>چیست صغیری که به هم میزنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت وزیر ای ملک روزگار</p></div>
<div class="m2"><p>گویم اگر شه بود آموزگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این دو نوا نز پی رامشگریست</p></div>
<div class="m2"><p>خطبه‌ای از بهر زناشوهریست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دختری این مرغ بدان مرغ داد</p></div>
<div class="m2"><p>شیربها خواهد از او بامداد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کاین ده ویران بگذاری به ما</p></div>
<div class="m2"><p>نیز چنین چند سپاری به ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن دگرش گفت کزین درگذر</p></div>
<div class="m2"><p>جور ملک بین و برو غم مخور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر ملک اینست نه بس روزگار</p></div>
<div class="m2"><p>زین ده ویران دهمت صد هزار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در ملک این لفظ چنان درگرفت</p></div>
<div class="m2"><p>کاه براورد و فغان برگرفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دست بسر بر زد و لختی گریست</p></div>
<div class="m2"><p>حاصل بیداد به جز گریه چیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زین ستم انگشت به دندان گزید</p></div>
<div class="m2"><p>گفت ستم بین که به مرغان رسید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جور نگر کز جهت خاکیان</p></div>
<div class="m2"><p>جغد نشانم به دل ماکیان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای من غافل شده دنیا پرست</p></div>
<div class="m2"><p>بس که زنم بر سر ازین کار دست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مال کسان چند ستانم بزور</p></div>
<div class="m2"><p>غافلم از مردن و فردای گور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا کی و کی دست‌درازی کنم</p></div>
<div class="m2"><p>با سر خود بین که چه بازی کنم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ملک بدان داد مرا کردگار</p></div>
<div class="m2"><p>تا نکنم آنچه نیاید به کار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من که مسم را به زر اندوده‌اند</p></div>
<div class="m2"><p>میکنم آنها که نفرموده‌اند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نام خود از ظلم چرا بد کنم</p></div>
<div class="m2"><p>ظلم کنم وای که بر خود کنم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بهتر از این در دلم آزرم داد</p></div>
<div class="m2"><p>یا ز خدا یا ز خودم شرم باد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ظلم شد امروز تماشای من</p></div>
<div class="m2"><p>وای به رسوائی فردای من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سوختنی شد تن بیحاصلم</p></div>
<div class="m2"><p>سوزد از این غصه دلم بر دلم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چند غبار ستم انگیختن</p></div>
<div class="m2"><p>آب خود و خون کسان ریختن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>روز قیامت ز من این ترکتاز</p></div>
<div class="m2"><p>باز بپرسند و بپرسند باز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شرم زدم چون ننشینم خجل</p></div>
<div class="m2"><p>سنگ دلم چون نشوم تنگدل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بنگر تا چند ملامت برم</p></div>
<div class="m2"><p>کاین خجلی را به قیامت برم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بار منست آنچه مرا بارگیست</p></div>
<div class="m2"><p>چاره من بر من بیچارگیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زین گهر و گنج که نتوان شمرد</p></div>
<div class="m2"><p>سام چه برداشت فریدون چه برد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا من ازین امر و ولایت که هست</p></div>
<div class="m2"><p>عاقبت‌الامر چه دارم به دست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شاه در آن باره چنان گرم گشت</p></div>
<div class="m2"><p>کز نفسش نعل فرس نرم گشت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چونکه به لشگر گه و رایت رسید</p></div>
<div class="m2"><p>بوی نوازش به ولایت رسید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حالی از آن خطه قلم برگرفت</p></div>
<div class="m2"><p>رسم بدو راه ستم برگرفت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>داد بگسترد و ستم درنبشت</p></div>
<div class="m2"><p>تا نفس آخر از آن برنگشت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بعد بسی گردش بخت آزمای</p></div>
<div class="m2"><p>او شد و آوازه عدلش بجای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یافته در خطه صاحبدلی</p></div>
<div class="m2"><p>سکه نامش رقم عادلی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>عاقبتی نیک سرانجام یافت</p></div>
<div class="m2"><p>هر که دَرِ عدل زد این نام یافت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>عمر به خشنودی دلها گذار</p></div>
<div class="m2"><p>تا ز تو خوشنود بود کردگار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سایه خورشید سواران طلب</p></div>
<div class="m2"><p>رنج خود و راحت یاران طلب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>درد ستانی کن و درماندهی</p></div>
<div class="m2"><p>تات رسانند به فرماندهی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گرم شو از مهر و ز کین سرد باش</p></div>
<div class="m2"><p>چون مه و خورشید جوانمرد باش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر که به نیکی عمل آغاز کرد</p></div>
<div class="m2"><p>نیکی او روی بدو باز کرد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گنبد گردنده ز روی قیاس</p></div>
<div class="m2"><p>هست به نیکی و بدی حق شناس</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>طاعت کن روی بتاب از گناه</p></div>
<div class="m2"><p>تا نشوی چون خجلان عذر خواه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>حاصل دنیا چو یکی ساعتست</p></div>
<div class="m2"><p>طاعت کن کز همه بِه طاعتست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>عذر میاور نه حیَل خواستند</p></div>
<div class="m2"><p>این سخنست از تو عمل خواستند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گر بسخن کار میسر شدی</p></div>
<div class="m2"><p>کار نظامی بفلک بر شدی</p></div></div>