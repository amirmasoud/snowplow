---
title: >-
    بخش ۱۹ - ثمره خلوت دوم
---
# بخش ۱۹ - ثمره خلوت دوم

<div class="b" id="bn1"><div class="m1"><p>عمر بر آن فرش ازل بافته</p></div>
<div class="m2"><p>آنچه شده باز بدل یافته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوش در آن نامه تحیت رسان</p></div>
<div class="m2"><p>دیده در آن سجده تحیات خوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنگ دل از خنده ترکان شکر</p></div>
<div class="m2"><p>سرمه بر از چشم غزالان نظر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تُرک قصب پوش من آنجا چو ماه</p></div>
<div class="m2"><p>کرده دلم را چو قصب رخنه گاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مه که به شب دست برافشانده‌بود</p></div>
<div class="m2"><p>آنشب تا روز فرو مانده‌بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناوک غمزه‌اش چو سبک پر شدی</p></div>
<div class="m2"><p>جان به زمین بوسه برابر شدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمع ز نورش مژه پر اشک داشت</p></div>
<div class="m2"><p>چشم چراغ آبله از رشک داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر ستمی که بجفا درگرفت</p></div>
<div class="m2"><p>دل به تبرک به وفا برگرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گه شده او سبزه و من جوی آب</p></div>
<div class="m2"><p>گه شده من گازر و او آفتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان رطب آنشب که بری داشتم</p></div>
<div class="m2"><p>بیخبرم گر خبری داشتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کان مه نو کو کمر از نور داشت</p></div>
<div class="m2"><p>ماه نو از شیفتگان دور داشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شیفتهٔ شیفتهٔ خویش بود</p></div>
<div class="m2"><p>رغبتی از من صد ازو بیش بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل به تمنا که چو بودی ز روز</p></div>
<div class="m2"><p>گر شب ما را نشدی پرده سوز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>امشب اگر جفت سلامت شدی</p></div>
<div class="m2"><p>هم نفس روز قیامت شدی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روشنی آن شب چون آفتاب</p></div>
<div class="m2"><p>جویم بسیار و نبینم به خواب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جز به چنان شب طربم خوش نبود</p></div>
<div class="m2"><p>تا شبخوش کرد شبم خوش نبود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زان همه شب یارب یارب کنم</p></div>
<div class="m2"><p>بو که شبی جلوه آن شب کنم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>روز سفید آن نه شب داج بود</p></div>
<div class="m2"><p>بود شب، اما شب معراج بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ماه که بر لعل فلک کان کند</p></div>
<div class="m2"><p>در غم آن شب همه شب جان کند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روز که شب دشمنیش مذهبست</p></div>
<div class="m2"><p>هم به تمنای چنان یکشبست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من شده فارغ که ز راه سحر</p></div>
<div class="m2"><p>تیغ زنان صبح درآمد ز در</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آتش خورشید ز مژگان من</p></div>
<div class="m2"><p>آب روان کرد بر ایوان من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ابر بباغ آمده بازی‌کنان</p></div>
<div class="m2"><p>جامهٔ خورشید نمازی‌کنان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حوضهٔ این چشمه که خورشید بست</p></div>
<div class="m2"><p>چون من و تو چند سبو را شکست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چرخ ستاره زده بر سیم ناب</p></div>
<div class="m2"><p>زر طلی از ورق آفتاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صبح گران خسب سبک خیز شد</p></div>
<div class="m2"><p>دشنه بدست از پی خون ریز شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>من ز مصافش سپر انداخته</p></div>
<div class="m2"><p>جان سپر دشنه او ساخته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در پی جانم سحر از جوی جست</p></div>
<div class="m2"><p>تشنه کشی کرد و بر او پل شکست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بانگ برآمد زخرابات من</p></div>
<div class="m2"><p>کی سحر اینست مکافات من</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پیشترک زین که کسی داشتم</p></div>
<div class="m2"><p>شمع شب افروز بسی داشتم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آنشب و آنشمع نماندم چه سود</p></div>
<div class="m2"><p>نیست چنان شد که تو گوئی نبود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نیش در آن زن که ز تو نوش خورد</p></div>
<div class="m2"><p>پشم در آن کش که ترا پنبه کرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خام‌کشی کن که صواب آن بود</p></div>
<div class="m2"><p>سوختن سوخته آسان بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>صبح چو در گریه من بنگریست</p></div>
<div class="m2"><p>بر شفق از شفقت من خون گریست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سوخته شد خرمن روز از غمم</p></div>
<div class="m2"><p>چشمه خورشید فسرد از دمم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>با همه زهرم فلک امید داد</p></div>
<div class="m2"><p>مار شبم مهره خورشید داد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون اثر نور سحر یافتم</p></div>
<div class="m2"><p>بی خبرم گر چه خبر یافتم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر که درین مهد روان راه یافت</p></div>
<div class="m2"><p>بیشتر ز نور سحرگاه یافت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای ز خجالت همه شبهای تو</p></div>
<div class="m2"><p>رو سیه از روز طرب‌های تو</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>من که ازین شب صفتی کرده‌ام</p></div>
<div class="m2"><p>آن صفت از معرفتی کرده‌ام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شب صفت پرده تنهائیست</p></div>
<div class="m2"><p>شمع در او گوهر بینائیست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>عود و گلابی که بر او بسته شد</p></div>
<div class="m2"><p>ناله و اشک دو سه دلخسته شد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>وانهمه خوبی که دران صدر بود</p></div>
<div class="m2"><p>نور خیالات شب قدر بود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>محرم این پرده زنگی نورد</p></div>
<div class="m2"><p>کیست در این پرده زنگار خورد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>صبح که پروانگی آموختست</p></div>
<div class="m2"><p>خوشتر ازان شمع نیفروختست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کوش کز آن شمع بداغی رسی</p></div>
<div class="m2"><p>تا چو نظامی به چراغی رسی</p></div></div>