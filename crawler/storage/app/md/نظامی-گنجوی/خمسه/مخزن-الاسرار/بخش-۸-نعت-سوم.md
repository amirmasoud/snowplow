---
title: >-
    بخش ۸ - نعت سوم
---
# بخش ۸ - نعت سوم

<div class="b" id="bn1"><div class="m1"><p>ای مدنی بُرقِع و مَکی نقاب</p></div>
<div class="m2"><p>سایه نشین چند بود آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر مهی از مهر تو موئی بیار</p></div>
<div class="m2"><p>ور گلی از باغ تو بوئی بیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منتظران را به لب آمد نفس</p></div>
<div class="m2"><p>ای ز تو فریاد به فریادرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوی عجم ران منشین در عرب</p></div>
<div class="m2"><p>زرده روز اینک و شبدیز شب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک برآرای و جهان تازه کن</p></div>
<div class="m2"><p>هردو جهان را پُر از آوازه کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سکه تو زن ،تا اُمرا کم زنند</p></div>
<div class="m2"><p>خطبه تو کن تا خطبا دم زنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک تو بوئی به ولایت سپرد</p></div>
<div class="m2"><p>باد نفاق آمد و آن بوی برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز کش این مسند از آسودگان</p></div>
<div class="m2"><p>غسل ده این منبر از آلودگان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خانه غولند بپردازشان</p></div>
<div class="m2"><p>در غله دان عدم اندازشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کم کن اجری که زیادت خورند</p></div>
<div class="m2"><p>خاص کن اقطاع که غارتگرند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما همه جسمیم بیا جان تو باش</p></div>
<div class="m2"><p>ما همه موریم سلیمان تو باش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از طرفی رخنه دین میکنند</p></div>
<div class="m2"><p>وز دگر اطراف کمین میکنند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شحنه توئی قافله تنها چراست</p></div>
<div class="m2"><p>قلب تو داری علم آنجا چراست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا علیی در صف میدان فرست</p></div>
<div class="m2"><p>یا عمری در ره شیطان فرست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شب به سر ماه یمانی درآر</p></div>
<div class="m2"><p>سر چو مه از برد یمانی برآر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با دو سه در بند کمربند باش</p></div>
<div class="m2"><p>کم زن این کم زده چند باش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پانصد و هفتاد بس ایام خواب</p></div>
<div class="m2"><p>روز بلندست به مجلس شتاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خیز و بفرمای سرافیل را</p></div>
<div class="m2"><p>باد دمیدن دو سه قندیل را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خلوتی پرده اسرار شو</p></div>
<div class="m2"><p>ما همه خفتیم تو بیدار شو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز آفت این خانهٔ آفت پذیر</p></div>
<div class="m2"><p>دست برآور همه را دست گیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر چه رضای تو به جز راست نیست</p></div>
<div class="m2"><p>با تو کسی را سر وا خواست نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر نظر از راه عنایت کنی</p></div>
<div class="m2"><p>جمله مهمات کفایت کنی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دایره بنمای به انگشت دست</p></div>
<div class="m2"><p>تا به تو بخشیده شود هر چه هست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>با تو تصرف که کند وقت کار</p></div>
<div class="m2"><p>از پی آمرزش مشتی غبار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از تو یکی پرده برانداختن</p></div>
<div class="m2"><p>وز دو جهان خرقه درانداختن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مغز نظامی که خبر جوی تست</p></div>
<div class="m2"><p>زنده دل از غالیه بوی تست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از نفسش بوی وفائی ببخش</p></div>
<div class="m2"><p>ملک فریدون به گدائی ببخش</p></div></div>