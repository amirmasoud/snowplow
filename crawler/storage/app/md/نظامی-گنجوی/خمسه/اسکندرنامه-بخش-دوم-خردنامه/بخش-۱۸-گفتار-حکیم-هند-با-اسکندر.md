---
title: >-
    بخش ۱۸ - گفتار حکیم هند با اسکندر
---
# بخش ۱۸ - گفتار حکیم هند با اسکندر

<div class="b" id="bn1"><div class="m1"><p>مغنی غنا را درآور به جوش</p></div>
<div class="m2"><p>که در باغ بلبل نباید خموش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر خاطرم را به جوش آوری</p></div>
<div class="m2"><p>من گنگ را در خروش آوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همان فیلسوف جهاندیده گفت</p></div>
<div class="m2"><p>که چون دانش آمد ره شاه رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهن مهر کرد ز می خوشگوار</p></div>
<div class="m2"><p>که بنیاد شادی ندید استوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی روز کز صبح زرین نقاب</p></div>
<div class="m2"><p>به نظارگان رخ نمود آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سکندر به آیین فرهنگ خویش</p></div>
<div class="m2"><p>ملوکانه برشد به اورنگ خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درآمد رقیبی که اینک ز راه</p></div>
<div class="m2"><p>فرستاده هندو آمد به شاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نماید که در حضرت شهریار</p></div>
<div class="m2"><p>پیام آورم باز خواهید بار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بفرمود شه تا شتاب آورند</p></div>
<div class="m2"><p>مغان را سوی آفتاب آورند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به فرمان شه سوی مغ تاختند</p></div>
<div class="m2"><p>رهش باز دادند و بنواختند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درآمد مغ خدمت آموخته</p></div>
<div class="m2"><p>مغانه چو آتش برافروخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو تابنده خورشید را دید زود</p></div>
<div class="m2"><p>به رسم مغانش پرستش نمود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به فرمان شاهش رقیبان دست</p></div>
<div class="m2"><p>نشاندند جایی‌که شاید نشست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سخن می‌شد از هر دری دلپسند</p></div>
<div class="m2"><p>ز خاک زمین تا به چرخ بلند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به اندازهٔ هر کس هنر می‌نمود</p></div>
<div class="m2"><p>به گفتار خود قدر خود می‌فزود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو در هندو آمد نشاط سخن</p></div>
<div class="m2"><p>گل تازه رست از درخت کهن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بسی نکته‌های گره بسته گفت</p></div>
<div class="m2"><p>که آن در ناسفته را کس نسفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فلک راز لب حقه پرنوش کرد</p></div>
<div class="m2"><p>جهان را ز در حلقه در گوش کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ثنای جهاندار گیتی پناه</p></div>
<div class="m2"><p>چنان گفت کافروخت آن بارگاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو گشت از ثنا پیر پرداخته</p></div>
<div class="m2"><p>نقاب سخن شد برانداخته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که تاریک پروانه‌ای سوی باغ</p></div>
<div class="m2"><p>روان شد به امید روشن چراغ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مگر کان چراغ آشنائی دهد</p></div>
<div class="m2"><p>من تیره را روشنائی دهد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>منم پیشوای همه هندوان</p></div>
<div class="m2"><p>به اندیشه پیر و به قوت جوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سخنهای سربسته دارم بسی</p></div>
<div class="m2"><p>که نگشاید آن بسته را هر کسی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شنیدم کز این دور آموزگار</p></div>
<div class="m2"><p>سرآمد توئی بر همه روزگار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خرد رشتهٔ در یکتای توست</p></div>
<div class="m2"><p>درفش گره باز کن رای توست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر چه خداوند تاجی و تخت</p></div>
<div class="m2"><p>بر دانشت نیز داد است بخت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر گفته را از تو یابم جواب</p></div>
<div class="m2"><p>پرستش بگردانم از آفتاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وگر ناید از شه جوابی به دست</p></div>
<div class="m2"><p>دگرباره بر خر توان رخت بست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ولیکن نخواهم که جز شهریار</p></div>
<div class="m2"><p>رود در سخن هیچکس را شمار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زمن پرسش و پاسخ آید ز تو</p></div>
<div class="m2"><p>جواب سخن فرخ آید ز تو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جهاندار گفتا بهانه مجوی</p></div>
<div class="m2"><p>سخن هر چه پوشیده داری بگوی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جهاندیدهٔ هندو زمین بوسه داد</p></div>
<div class="m2"><p>زبانی چو شمشیر هندی گشاد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو کرد آفرینی سزاوار شاه</p></div>
<div class="m2"><p>بپرسیدش از کار گیتی پناه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که چون من ز خود رخت بیرون برم؟</p></div>
<div class="m2"><p>سوی آفریننده ره چون برم؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یکی آفریننده دانم که هست</p></div>
<div class="m2"><p>کجا جویمش چون شوم ره به دست؟</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نشانش پدید است و او ناپدید</p></div>
<div class="m2"><p>در بسته را از که جویم کلید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>وجودش که صاحب معانی شدست</p></div>
<div class="m2"><p>زمینیست یا آسمانی شد است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در اندیشه یا در نظر جویمش</p></div>
<div class="m2"><p>چو پرسند جایش کجا گویمش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کجا جای دارد ز بالا و زیر</p></div>
<div class="m2"><p>به حجت شود مرد پرسنده سیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جهاندار پاسخ چنین داد باز</p></div>
<div class="m2"><p>که هم کوتهست این سخن هم دراز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو از خویشتن روی بر تافتی</p></div>
<div class="m2"><p>به ایزد چنان دان که ره یافتی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>طلب کردن جای او رای نیست</p></div>
<div class="m2"><p>که جای آفریننده را جای نیست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نه کس راز او را تواند شمرد</p></div>
<div class="m2"><p>نه اندیشه داند بدو راه برد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بدان چیزها دارد اندیشه راه</p></div>
<div class="m2"><p>که باشد بدو دیده را دستگاه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خدا را نشاید در اندیشه جست</p></div>
<div class="m2"><p>که دیو است هرچ آن ز اندیشه رست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هر اندیشه‌ای کان بود در ضمیر</p></div>
<div class="m2"><p>خیالی بود آفرینش پذیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هرانچ او ندارد در اندیشه جای</p></div>
<div class="m2"><p>سوی آفریننده شد رهنمای</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به غفلت نشاید شد این راه را</p></div>
<div class="m2"><p>که ابر از تو پنهان کند ماه را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نشان بس بود کرده بر کردگار</p></div>
<div class="m2"><p>چو اینجا رسیدی هم اینجا بدار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به ایزد شناسی همین شد قیاس</p></div>
<div class="m2"><p>از این نگذرد مرد ایزدشناس</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو هندو جواب سکندر شنید</p></div>
<div class="m2"><p>به شب بازی دیگر آمد پدید</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که هرچ از زمین باشد و آسمان</p></div>
<div class="m2"><p>نهایت گهی باشدش بیگمان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خبرده که بیرون از این بارگاه</p></div>
<div class="m2"><p>به چیزی دیگر هست یا نیست راه</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>اگر هست چون زان کس آگاه نیست</p></div>
<div class="m2"><p>وگر نیست بر نیستی راه نیست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>جهاندار گفت از حساب کهن</p></div>
<div class="m2"><p>به آزرم تر سکه زن بر سخن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>برون زاسمان و زمین برمتاز</p></div>
<div class="m2"><p>که نائی به سررشتهٔ خویش باز</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>فلک بر تو زان هفت مندل کشید</p></div>
<div class="m2"><p>که بیرون ز مندل نشاید دوید</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>از این مندل خون نشاید گذشت</p></div>
<div class="m2"><p>که چرخ ایستادست با تیغ و طشت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>حصاریست این بارگاه بلند</p></div>
<div class="m2"><p>در او گشته اندیشها شهر بند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو اندیشه زاین پرده درنگذرد</p></div>
<div class="m2"><p>پس پرده راز پی چون برد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نجوید دگر پردهٔ راز را</p></div>
<div class="m2"><p>خبرهای انجام و آغاز را</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بدین داستانها زند رهنمای</p></div>
<div class="m2"><p>که نادیده را نیست اندیشه جای</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گر اندیشی آنرا که نادیده‌ای</p></div>
<div class="m2"><p>چو نیکو ببینی خطا دیده‌ای</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بسا کس که من دیده انگاشتم</p></div>
<div class="m2"><p>خیالش در اندیشه بنگاشتم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سرانجام چون دیدمش وقت کار</p></div>
<div class="m2"><p>نه آن بود کز وی گرفتم شمار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>جهانی دگر هست پوشیده روی</p></div>
<div class="m2"><p>به آنجا توان کردن این جستجوی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دگر باره گفتش به من گوی راست</p></div>
<div class="m2"><p>که ملک جهان بر دو قسمت چراست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>جهانی بدین خوبی آراستن</p></div>
<div class="m2"><p>چه باید جهانی دگر خواستن</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو پیداست کاینجا توانیم زیست</p></div>
<div class="m2"><p>به آنجا سفر کردن از بهر چیست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو آنجا نشستنگه آمد درست</p></div>
<div class="m2"><p>به اینجا گذشتن چه باید نخست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خردمند شه گفت: ای ساده مرد</p></div>
<div class="m2"><p>چنین دان و از دل فروشوی گرد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>که ایزد دو گیتی بدان آفرید</p></div>
<div class="m2"><p>که آنجا بود گنج و اینجا کلید</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>در اینجا کنی کشت و کارنوی</p></div>
<div class="m2"><p>در آنجا بر کشته را بدروی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>در این گردد از حال خود هر چه هست</p></div>
<div class="m2"><p>در آن بر یکی حال باید نشست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>دو پرگار برزد جهان آفرین</p></div>
<div class="m2"><p>در این آفرینش دران آفرین</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>پلست این و بر پل بباید گذشت</p></div>
<div class="m2"><p>به دریا بود سیل را بازگشت</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو چشمه روان گردد از کوهسار</p></div>
<div class="m2"><p>به دریاش باید گرفتن قرار</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>دگر باره پرسید هندوی پیر</p></div>
<div class="m2"><p>که جان چیست در پیکر جان پذیر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>نماید مرا کاتشی تافتست</p></div>
<div class="m2"><p>شراری از او کالبد یافتست</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>فرو مردن جان و آتش یکیست</p></div>
<div class="m2"><p>در این بد بود گر کسی را شکیست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چو آتش در او گرم دل گشت شاه</p></div>
<div class="m2"><p>به تندی در او کرد لختی نگاه</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بدو گفت کاهریمنی سان توست</p></div>
<div class="m2"><p>اگر جانی آتش بود جان توست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>نخواندی که جان چون سفر ساز گشت</p></div>
<div class="m2"><p>از آن کس که آمد بدو بازگشت</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چو ز آتش بود جنبش جان نخست</p></div>
<div class="m2"><p>به دوزخ توان جای او باز جست</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>دگر آنکه گفتی به وقت فراغ</p></div>
<div class="m2"><p>فرو مردن جان بود چون چراغ</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>غلط گفته‌ای جان علوی گرای</p></div>
<div class="m2"><p>نمیرد ولیکن شود باز جای</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>حکایت ز شخصی که او جان سپرد</p></div>
<div class="m2"><p>چه گویند؟ جان داد یا جان بمرد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بگویند جان داد و این نیست زرق</p></div>
<div class="m2"><p>ز داده بود تا فرو مرده فرق</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ز جان درگذر کان فروغیست پاک</p></div>
<div class="m2"><p>ز نور الهی نه از آب و خاک</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>دگر گونه هندو سخن کرد ساز</p></div>
<div class="m2"><p>به پرسیدن خوابش آمد نیاز</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>که بینندهٔ خواب را در خیال</p></div>
<div class="m2"><p>چه نیرو برون آورد پروبال</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>که منزل به منزل رود کوه و دشت</p></div>
<div class="m2"><p>ببیند جهان در جهان سرگذشت</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>چو بیننده آنجاست این خفته کیست</p></div>
<div class="m2"><p>و گر نقشبند آن شد این نقش چیست</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>به پاسخ دگر باره شد شاه تیز</p></div>
<div class="m2"><p>که خواب از خیالی بود خانه خیز</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>خیال همه خوابها خانگیست</p></div>
<div class="m2"><p>در آن آشنائی نه بیگانگیست</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>اگر مرده گر زنده بینی به خواب</p></div>
<div class="m2"><p>ز شمع تو می‌خیزد آن نور و تاب</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>نمایندهٔ اندیشهٔ پاک توست</p></div>
<div class="m2"><p>نمودهٔ تمنای ادراک توست</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>گرت در دل آید که راز نفهت</p></div>
<div class="m2"><p>چرا گشت پیدا برآنکس که خفت</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>روان چون برهنه شود در خیال</p></div>
<div class="m2"><p>نپوشد براو صورت هیچ حال</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>نبینی کسی کو ریاضتگر است</p></div>
<div class="m2"><p>به بیداری آن گنج را رهبر است</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>همان بیند آن مرد بیدار هوش</p></div>
<div class="m2"><p>که دیگر کس از خواب و خواب از سروش</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>دگر باره هندو درآمد به گفت</p></div>
<div class="m2"><p>گهر کرد با نوک الماس جفت</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>که بی چشم بد شاهیی ده مرا</p></div>
<div class="m2"><p>ز چشم بد آگاهیی ده مرا</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>چه نیروست در جنبش چشم بد</p></div>
<div class="m2"><p>که نیکوی خود را کند چشم زد</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>از او کارگرتر جهان آزمای</p></div>
<div class="m2"><p>ندیده است بینندهٔ جان گزای</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>همه چیز را کازمایش رسد</p></div>
<div class="m2"><p>چو دیده پسندد فزایش رسد</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>جز او را که هرچ او پسند آورد</p></div>
<div class="m2"><p>سر و گردنش زیر بند آورد</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>به هر حرفتی در که دیدیم ژرف</p></div>
<div class="m2"><p>درستی ندیدیم در هیچ حرف</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>همین یک کماندار شد کز نخست</p></div>
<div class="m2"><p>بر آماج گه تیر او شد درست</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>بگو تا چه نیروست نیروی او</p></div>
<div class="m2"><p>سپند از چه برد آفت از خوی او</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>چه دانم که من چشم بد دیده‌ام</p></div>
<div class="m2"><p>پسندیده یا نا پسندیده‌ام</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>جهاندار گفتش که صاحب قیاس</p></div>
<div class="m2"><p>چنین آرد از رای معنی شناس</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>که بر هر چه گردد نظر جایگیر</p></div>
<div class="m2"><p>گذر بر هوائی کند ناگزیر</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>بر آن چیز کارد همی تاختن</p></div>
<div class="m2"><p>کند با هوا رای دم ساختن</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>بنه چون درآرد بدان رخنه گاه</p></div>
<div class="m2"><p>هوا نیز باید در آن رخنه راه</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>هوا گر هوائی بود سودمند</p></div>
<div class="m2"><p>در ارکان آن چیز ناید گزند</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>مزاج هوا چون بود زهرناک</p></div>
<div class="m2"><p>بیندازد آن چیز را در مغاک</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>هوائی بد است آنکه بر چشم زد</p></div>
<div class="m2"><p>بد آرد به همراهی چشم بد</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>ولیکن به نزدیک من در نهفت</p></div>
<div class="m2"><p>جز این علتی هست کان کس نگفت</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>نه چشم بد است آنچنان کارگر</p></div>
<div class="m2"><p>که نقش روند است پیش نظر</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>چو بیند عجب کاریی در خیال</p></div>
<div class="m2"><p>به تأدیب چشمش دهد گوشمال</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>تعجب روانیست در راه او</p></div>
<div class="m2"><p>نباید جز او در نظرگاه او</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>چو نقش حریفی شگفت آیدش</p></div>
<div class="m2"><p>دغا باختن در گرفت آیدش</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>گرفتار کن را دهد پیچ پیچ</p></div>
<div class="m2"><p>بدان تا نگردد گرفتار هیچ</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>کسی را که چشمی رسد ناگهان</p></div>
<div class="m2"><p>دهن دره‌اش اوفتد در دهان</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>رسانندهٔ چشم را جوش خون</p></div>
<div class="m2"><p>بخاری ز پیشانی آرد برون</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>به این هر دو معنی شناسند و بس</p></div>
<div class="m2"><p>که این چشم زن بود و آن چشم رس</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>سپند از پی آن شد افروخته</p></div>
<div class="m2"><p>که آفت به آتش شود سوخته</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>فسونگر دگرگونه گفتست راز</p></div>
<div class="m2"><p>که چون با سپند آتش آمد فراز</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>رسد بر فلک دود مشگین سپند</p></div>
<div class="m2"><p>فلک خود زره باز دارد گزند</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>دگر باره هندوی رومی پرست</p></div>
<div class="m2"><p>درآورد پولاد هندی به دست</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>که از نیک و بد مرد اخترسگال</p></div>
<div class="m2"><p>خبر چون دهد چون زند نقش فال</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>ز نقشی که از کار ناید برون</p></div>
<div class="m2"><p>به نیک و به بد چون شود رهنمون</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>چنین گفتش آن مایهٔ ایزدی</p></div>
<div class="m2"><p>که هرچ آن ز نیکی رسد یا بدی</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>هر آیینه در نقش این گنبد است</p></div>
<div class="m2"><p>اگر نیک نیکست اگر بد بداست</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>سگالندهٔ فال چون قرعه راند</p></div>
<div class="m2"><p>ز طالع تواند همی نقش خواند</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>نمودار طالع نماید درست</p></div>
<div class="m2"><p>ز تخمی که خواهد دران زرع رست</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>خدائی که هست آفرینش پناه</p></div>
<div class="m2"><p>چو بیند نیازی در این عرضه‌گاه</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>به اندازهٔ آنکه باشد نیاز</p></div>
<div class="m2"><p>نماید به ما بودنیهای راز</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>فرستد سروشی و با او کلید</p></div>
<div class="m2"><p>کند راز سربسته بر ما پدید</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>از آن باده هندو چنان مست شد</p></div>
<div class="m2"><p>که یکباره شمشیرش از دست شد</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>دگر باره پرسید کز چین و زنگ</p></div>
<div class="m2"><p>ورقهای صورت چرا شد دو رنگ</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>چو یکسان بود رنگ‌ها در لوید</p></div>
<div class="m2"><p>چرا این سیه گشت و آن شد سپید</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>جهاندار گفت این گرایندهٔ گوی</p></div>
<div class="m2"><p>دو رنگست یکی رنگی از وی مجوی</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>دو رویست خورشید آیینه وش</p></div>
<div class="m2"><p>یکی روی در چین یکی در حبش</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>به روئی کند رویها را چو ماه</p></div>
<div class="m2"><p>به روئی دگر رویها در سیاه</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>چو هندوی دانا به چندین سئوال</p></div>
<div class="m2"><p>زبون شد ز فرهنگ دانش سگال</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>به تسلیم شه بوسه بر خاک زد</p></div>
<div class="m2"><p>شه از خرمی سر بر افلاک زد</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>همه زیرکان بر چنان هوش و رای</p></div>
<div class="m2"><p>دمیدند و خواندند نام خدای</p></div></div>