---
title: >-
    بخش ۱۹ - خلوت ساختن اسکندر با هفت حکیم در آفرینش نخست
---
# بخش ۱۹ - خلوت ساختن اسکندر با هفت حکیم در آفرینش نخست

<div class="b" id="bn1"><div class="m1"><p>مغنی بیار آن ره باستان</p></div>
<div class="m2"><p>مرا یاریی ده در این داستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زدستان گیتی مگر جان برم</p></div>
<div class="m2"><p>بر این داستان ره به پایان برم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین آمد از فیلسوف این سخن</p></div>
<div class="m2"><p>که چون شد به شه تازه روز کهن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به فیروزی بخت فرخنده فال</p></div>
<div class="m2"><p>درآمد به بخشیدن ملک و مال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس بخشش او در آن مرز و بوم</p></div>
<div class="m2"><p>برافتاد درویشی از اهل روم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهادند سر خسروان بردرش</p></div>
<div class="m2"><p>به فرماندهی گشته فرمان برش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به فرخندگی شاه فیروز بخت</p></div>
<div class="m2"><p>یکی روز برشد به فیروزه تخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن راند از انصاف و از دین و داد</p></div>
<div class="m2"><p>گهی درج می‌بست و گه می‌گشاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو لختی سخن گفت از آن در که بود</p></div>
<div class="m2"><p>به خلوتگه خویش رغبت نمود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از آن فیلسوفان گزین کرد هفت</p></div>
<div class="m2"><p>که بر خاطر کس خطائی نرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ارسطو که بد مملکت را وزیر</p></div>
<div class="m2"><p>بلیناس برنا و سقراط پیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فلاطون و والیس و فرفوریوس</p></div>
<div class="m2"><p>که روح القدس کردشان دست‌بوس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همان هفتمین هرمس نیک رای</p></div>
<div class="m2"><p>که بر هفتمین آسمان کرد جای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنین هفت پرگار بر گرد شاه</p></div>
<div class="m2"><p>در آن دایره شه شده نقطه گاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طرازنده بزمی چو تابنده هور</p></div>
<div class="m2"><p>هم از باده خالی هم از باد دور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل شه در آن مجلس تنگبار</p></div>
<div class="m2"><p>به ابرو فراخی درآمد به کار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به دانندگان راز بگشاد و گفت</p></div>
<div class="m2"><p>که تا کی بود راز ما در نهفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسی شب به مستی شد و بیخودی</p></div>
<div class="m2"><p>گذاریم یک روز در بخردی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یک امروز بینیم در ماه و مهر</p></div>
<div class="m2"><p>گشائیم سر بسته‌های سپهر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدانیم کاین خرگه گاو پشت</p></div>
<div class="m2"><p>چگونه درآمد به خاک درشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین بود تا بود بالا و زیر</p></div>
<div class="m2"><p>بدانسان که بد گفت باید دلیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنان واجب آمد به رای درست</p></div>
<div class="m2"><p>که ترکیب اول چه بود از نخست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه افزایش و کاهش نو بنو</p></div>
<div class="m2"><p>بنا بود پیشینه شد پیشرو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نخستین سبب را در این تاروپود</p></div>
<div class="m2"><p>بجوئیم از اجرام چرخ کبود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدین زیرکی جمعی آموزگار</p></div>
<div class="m2"><p>نیارد به‌هم بعد از این روزگار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ندانیم کز مادر این راه رنج</p></div>
<div class="m2"><p>کرا پای خواهد فروشد به گنج</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بگوئید هر یک به فرهنگ خویش</p></div>
<div class="m2"><p>که این کار از آغاز چون بود پیش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به تقدیر و حکم جهان آفرین</p></div>
<div class="m2"><p>نخست آسمان کرده شد با زمین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بیا تا برون آوریم از نهفت</p></div>
<div class="m2"><p>که اول بهار جهان چون شکفت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چگونه نهادش بنا گر بنا؟</p></div>
<div class="m2"><p>چه بانگ آمد از ساز اول غنا؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو شاه این سخن را سرآغاز کرد</p></div>
<div class="m2"><p>چنان گنج سربسته را باز کرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز تاریخ آن کارگاه کهن</p></div>
<div class="m2"><p>فروبست بر فیلسوفان سخن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ولیکن نیوشنده را در جواب</p></div>
<div class="m2"><p>سخن واجب آمد به فکر صواب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چنان رفت رخصت به رای درست</p></div>
<div class="m2"><p>کارسطو کند پیشوائی نخست</p></div></div>