---
title: >-
    بخش ۳۰ - خردنامه ارسطو
---
# بخش ۳۰ - خردنامه ارسطو

<div class="b" id="bn1"><div class="m1"><p>چنین بود در نامهٔ رهنمای</p></div>
<div class="m2"><p>از آن پس که بود آفرین خدای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که شاها به دانش دل آباددار</p></div>
<div class="m2"><p>ز بی دانشان دور شو یاد دار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دری را که بندش بود ناپدید</p></div>
<div class="m2"><p>ز دانا توان بازجستن کلید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر دولتی کاوری در شمار</p></div>
<div class="m2"><p>سجودی بکن پیش پروردگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به پیروزی خود قوی دل مباش</p></div>
<div class="m2"><p>ز ترس خدا هیچ غافل مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدا ترس را کارساز است بخت</p></div>
<div class="m2"><p>بود ناخدا ترس را کار سخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر جا که باشی تنومند و شاد</p></div>
<div class="m2"><p>سپندی به آتش فکن بامداد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مباش ایمن از دیدن چشم بد</p></div>
<div class="m2"><p>نه از چشم بد بلکه از چشم خود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنین زد مثل مرد گوهر شناس</p></div>
<div class="m2"><p>که گر خوبی از خویشتن در هراس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بار آن درختی نیابد گزند</p></div>
<div class="m2"><p>که از خاک سربرنیارد بلند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دو شاخه گشایان نخجیرگاه</p></div>
<div class="m2"><p>به فحلان نخجیر یابند راه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سبق برد خود را تک آهسته‌دار</p></div>
<div class="m2"><p>حسد را به خود راه بربسته دار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حسد مرد را دل به درد آورد</p></div>
<div class="m2"><p>میان دو آزاده گرد آورد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به کینه مبر هیچکس را ز جای</p></div>
<div class="m2"><p>چو از جای بردی درآرش ز پای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرت با کسی هست کین کهن</p></div>
<div class="m2"><p>نژادش مکن یکسر از بیخ و بن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مخواه از کسی کین آبای او</p></div>
<div class="m2"><p>نظر بیش کن در محابای او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز خورشید تا سایه موئی بود</p></div>
<div class="m2"><p>که این روشن آن تیره روئی بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز خرما به دستی بود تا بخار</p></div>
<div class="m2"><p>که این گل‌شکر باشد آن ناگوار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صدف گرچه همسایه شد با نهنگ</p></div>
<div class="m2"><p>در تاج دارد نه شمشیر جنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برادر به جرم برادر مگیر</p></div>
<div class="m2"><p>که بس فرق باشد ز خون تا بشیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مزن در کس از بهر کس نیش را</p></div>
<div class="m2"><p>به پای خود آویز هر میش را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو آمرزش ایزدی بایدت</p></div>
<div class="m2"><p>نباید که رسم بدی آیدت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدان را بد آید ز چرخ کبود</p></div>
<div class="m2"><p>به نیکان همه نیکی آید فرود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مکن جز به نیکی گرایندگی</p></div>
<div class="m2"><p>که در نیکنامی است پایندگی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>منه بر دل نیکنامان غبار</p></div>
<div class="m2"><p>که بدنامی آرد سرانجام کار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مکن کار بد گوهران را بلند</p></div>
<div class="m2"><p>که پروردن گرگت آرد گزند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>میامیز در هیچ بد گوهری</p></div>
<div class="m2"><p>مده کیمیائی به خاکستری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو بد گوهری سربرآرد زمرد</p></div>
<div class="m2"><p>کند گوهر سرخ را روی زرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زدن با خداوند فرهنگ رای</p></div>
<div class="m2"><p>به فرهنگ باشد تو را رهنمای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو سود درم بیش خواهی نه کم</p></div>
<div class="m2"><p>مزن رای با مردم بی درم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کشش جستن از مردم سست کوش</p></div>
<div class="m2"><p>جواهر خری باشد از جو فروش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه جنسی از گور و گاو و پلنگ</p></div>
<div class="m2"><p>به جنسیت آرند شادی به چنگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو در پرده ناجنس باشد همال</p></div>
<div class="m2"><p>ز تهمت بسی نقش بندد خیال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دو آیینه را چون بهم برنهی</p></div>
<div class="m2"><p>شود هر دو از عاریتها تهی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مشو با زبون افکنان گاو دل</p></div>
<div class="m2"><p>که مانی در اندوه چون خر به گل</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جوانمردی شیر با آدمی</p></div>
<div class="m2"><p>ز مردم رمی دان نه از مردمی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بر آنکس که با سخت روئی بود</p></div>
<div class="m2"><p>درشتی به از نرم خوئی بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ستیزنده را چون بود سخت کار</p></div>
<div class="m2"><p>به نرمی طلب کن به سختی بدار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سر خصم چون گردد از فتنه پر</p></div>
<div class="m2"><p>به چربی بیاور به تیزی ببر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو افتی میان دو بدخواه خام</p></div>
<div class="m2"><p>پراکنده‌شان کن لگام از لگام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>درافکن به‌هم‌گرگ را با پلنگ</p></div>
<div class="m2"><p>تو بر آرد را از میان دو سنگ</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کسی را که باشد ز دهقان و شاه</p></div>
<div class="m2"><p>به اندازهٔ پایهٔ نه پایگاه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بسوی توانا توانا فرست</p></div>
<div class="m2"><p>به دانا هم از جنس دانا فرست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فرستاده را چون بود چاره ساز</p></div>
<div class="m2"><p>به اندرز کردن نباشد نیاز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به جائی که آهن درآید به زنگ</p></div>
<div class="m2"><p>به زر داد آهن برآور ز سنگ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خزینه ز بهر زر آکندنست</p></div>
<div class="m2"><p>زر از بهر دشمن پراکندنست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به چربی توان پای روباه بست</p></div>
<div class="m2"><p>به حلوا دهد طفل چیزی زدست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو مطرب به سور کسان شادباش</p></div>
<div class="b" id="bn49"><div class="m1"><p>ز بند خود ار سروی آزاد باش</p></div>
<div class="b" id="bn50"><div class="m1"><p>میارای خود را چو ریحان باغ</p></div>
<div class="m2"><p>به دست کسان خوبتر شد چراغ</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خزینه که با توست بر توست بار</p></div>
<div class="m2"><p>چو دادی به دادن شوی رستگار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زر آن آتشی نیست کاکندنیست</p></div>
<div class="m2"><p>شراریست کز خود پراکندنیست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مگو کز زر و صاحب زر که به</p></div>
<div class="m2"><p>گره بدتر از بند و بند از گره</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چنین گفت با آتش آتش پرست</p></div>
<div class="m2"><p>که از ما که بهتر به جائی که هست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بگفت آتش ار خواهی آموختن</p></div>
<div class="m2"><p>تو را کشت باید مرا سوختن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>فراخ آستین شو کزین سبز شاخ</p></div>
<div class="m2"><p>فتد میوه در آستین فراخ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز سیری مباش آنچنان شاد کام</p></div>
<div class="m2"><p>که از هیضهٔ زهری درافتد به جام</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به گنجینهٔ مفلسی راه برد</p></div>
<div class="m2"><p>بیفتاد و از شادمانی بمرد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همان تشنهٔ گرم را آب سرد</p></div>
<div class="m2"><p>پیاپی نشاید به یکباره خورد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به هر منزلی کاوری تاختن</p></div>
<div class="m2"><p>نشاید درو خوابگه ساختن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مخور آب نا آزموده نخست</p></div>
<div class="m2"><p>به دیگر دهانی کن آن بازجست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نه آن میوه‌ای کو غریب آیدت</p></div>
<div class="m2"><p>کزو ناتوانی نصیب آیدت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به وقت خورش هر که باشد طبیب</p></div>
<div class="m2"><p>بپرهیزد از خوردهای غریب</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بر آن ره که نارفته باشد کسی</p></div>
<div class="m2"><p>مرو گرچه همراه داری بسی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>رهی کو بود دور از اندیشه پاک</p></div>
<div class="m2"><p>به از راه نزدیک اندیشناک</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گرانباری مال چندان مجوی</p></div>
<div class="m2"><p>که افتد به لشگرگهت گفتگوی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>زهر غارت و مال کاری به دست</p></div>
<div class="m2"><p>به درویش ده هر یک از هر چه هست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نهانی بخواهندگان چیز ده</p></div>
<div class="m2"><p>که خشنودی ایزد از چیز به</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دهش کز نظرها نهانی بود</p></div>
<div class="m2"><p>حصار بد آسمانی بود</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سپه را به اندازه ده پایگاه</p></div>
<div class="m2"><p>مده بیشتر مالی از خرج راه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>شکم بنده را چون شکم گشت سیر</p></div>
<div class="m2"><p>کند بد دلی گر چه باشد دلیر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>نه سیری چنان ده که گردند مست</p></div>
<div class="m2"><p>نه بگذارشان از خورش تنگدست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چنان زی که هنگام سختی و ناز</p></div>
<div class="m2"><p>بود لشگر از جزتوئی بی نیاز</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به روزی دو نوبت برآرای خوان</p></div>
<div class="m2"><p>سران سپه را یکایک بخوان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>مخور باده در هیچ بیگانه بوم</p></div>
<div class="m2"><p>تن آسان مشو تا نباشی به روم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بروشنترین کس ودیعت سپار</p></div>
<div class="m2"><p>که از آب روشن نیاید غبار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو روشن‌ترست آفتاب از گروه</p></div>
<div class="m2"><p>امانت بدو داد دریا و کوه</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>اگر مقبلی مقبلانرا شناس</p></div>
<div class="m2"><p>که اقبال را دارد اقبال پاس</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>مده مدبران را بر خویش راه</p></div>
<div class="m2"><p>که انگور از انگور گردد سیاه</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>وفا خصلت مادر آورد توست</p></div>
<div class="m2"><p>مگر از سرشتی که بود از نخست</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چو مردم بگرداند آیین و حال</p></div>
<div class="m2"><p>بگردد بر او سکهٔ ملک و مال</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ز خوی قدیمی نشاید گذشت</p></div>
<div class="m2"><p>که نتوان به خوی دگر بازگشت</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>منه خوی اصلی چو فرزانگان</p></div>
<div class="m2"><p>مشو پیرو خوی بیگانگان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>پیاده که اوراست آیین شود</p></div>
<div class="m2"><p>نگونسار گردد چو فرزین شود</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>اگر صاحب اقبال بینی کسی</p></div>
<div class="m2"><p>نبینم که با او بکوشی بسی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>به هر گردشی با سپهر بلند</p></div>
<div class="m2"><p>ستیزه مبر تا نیابی گزند</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بنه دل به هرچ آورد روزگار</p></div>
<div class="m2"><p>مگردان سراز پند آموزگار</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>اگر نازی از دولت آید پدید</p></div>
<div class="m2"><p>سر از ناز دولت نباید کشید</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بنازی که دولت نماید مرنج</p></div>
<div class="m2"><p>که در ناز دولت بود کان گنج</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>چو هنگام ناز تو آید فراز</p></div>
<div class="m2"><p>کشد دولت آنروز نیز از تو ناز</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>صدف زان همه تن شدست استخوان</p></div>
<div class="m2"><p>که مغزی چو در دارد اندرمیان</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ازان سخت شد کان گوهر چو سنگ</p></div>
<div class="m2"><p>که ناید گهر جز به سختی به چنگ</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>به سختی در اختر مشو بدگمان</p></div>
<div class="m2"><p>که فرخ‌تر آید زمان تا زمان</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ز پیروزه گون گنبد انده مدار</p></div>
<div class="m2"><p>که پیروز باشد سرانجام کار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>مشو ناامید ارشود کار سخت</p></div>
<div class="m2"><p>دل خود قوی کن به نیروی بخت</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بر انداز سنگی به بالا دلیر</p></div>
<div class="m2"><p>دگرگون بود کار کاید به زیر</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>رها کن ستم را به یکبارگی</p></div>
<div class="m2"><p>که کم عمری آرد ستمکارگی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>شه از داد خود گر پشیمان شود</p></div>
<div class="m2"><p>ولایت ز بیداد ویران شود</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>تو را ایزد از بهر عدل آفرید</p></div>
<div class="m2"><p>ستم ناید از شاه عادل پدید</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>نکورای چون رای را بد کند</p></div>
<div class="m2"><p>چنان دان که بد در حق خود کند</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چو گردد جهان گاهگاه از نورد</p></div>
<div class="m2"><p>به گرمای گرم و به سرمای سرد</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>در آن گرم و سردی سلامت مجوی</p></div>
<div class="m2"><p>که گرداند از عادت خویش روی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چنان به که هر فصلی از فصل سال</p></div>
<div class="m2"><p>به خاصیت خود نماید خصال</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>ربیع از ربیعی نماید سرشت</p></div>
<div class="m2"><p>تموز از تموز آورد سرنبشت</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>چو هرچ او بگردد ز ترتیب کار</p></div>
<div class="m2"><p>بگردد بر او گردش روزگار</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بجای تو گر بد کند ناکسی</p></div>
<div class="m2"><p>تو نیز ارکنی نیکوی با کسی</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>همانرا همین را فراموش کن</p></div>
<div class="m2"><p>زبان از بدو نیک خاموش کن</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>مژه در نخفتن چو الماس دار</p></div>
<div class="m2"><p>به بیداری آفاق را پاس دار</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>چنین زد مثل کاردان بزرگ</p></div>
<div class="m2"><p>که پاس شبانست پابند گرگ</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>چو یابی توانائیی در سرشت</p></div>
<div class="m2"><p>مزن خنده کانجا بود خنده زشت</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>وگر ناتوانی درآید به کار</p></div>
<div class="m2"><p>مکن عاجزی برکسی آشکار</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>لب از خندهٔ خرمی درمبند</p></div>
<div class="m2"><p>غمین باش پنهان و پیدا بخند</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>به هر جا که حربی فراز آیدت</p></div>
<div class="m2"><p>به حرب آزمایان نیاز آیدت</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>هزیمت پذیر از دگر حربگاه</p></div>
<div class="m2"><p>نباید که یابد درآن حرب راه</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>گریزنده چون ره به دست آورد</p></div>
<div class="m2"><p>به کوشندگان درشکست آورد</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چو خواهی که باشد ظفر یار تو</p></div>
<div class="m2"><p>ظفر دیده باید سپهدارتو</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>به فرخ رکابان فیروزمند</p></div>
<div class="m2"><p>عنان عزیمت برآور بلند</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>به هرچ آری از نیک و از بد بجای</p></div>
<div class="m2"><p>بد از خویشتن بین و نیک از خدای</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چو این نامه نامور شد تمام</p></div>
<div class="m2"><p>به شه داد و شه گشت ازو شادکام</p></div></div>