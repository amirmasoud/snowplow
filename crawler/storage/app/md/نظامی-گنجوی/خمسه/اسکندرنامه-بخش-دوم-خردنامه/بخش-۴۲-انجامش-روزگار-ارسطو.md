---
title: >-
    بخش ۴۲ - انجامش روزگار ارسطو
---
# بخش ۴۲ - انجامش روزگار ارسطو

<div class="b" id="bn1"><div class="m1"><p>مغنی دلم سیر گشت از نفیر</p></div>
<div class="m2"><p>برآور یکی ناله بر بانگ زیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر نالهٔ زیرم آید به گوش</p></div>
<div class="m2"><p>ازین ناله زار گردم خموش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سکندر چو زین کنده بگشاد بند</p></div>
<div class="m2"><p>برافکند بر حصن گردون کمند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه فیلسوفان درگاه او</p></div>
<div class="m2"><p>در آن پویه گشتند همراه او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ارسطو چو واماند از آن آفتاب</p></div>
<div class="m2"><p>از ابر سیه بست بر خود نقاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیاهی بپوشید و در غم نشست</p></div>
<div class="m2"><p>چو وقت آمد او نیز هم رخت بست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سرو سهی رفت بالندگی</p></div>
<div class="m2"><p>طبیعت درآمد به نالندگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشستند یونانیان گرد او</p></div>
<div class="m2"><p>ز استاد او تا به شاگرد او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو دیدند کان پیک منزل شناس</p></div>
<div class="m2"><p>به منزل شود بی رقیبان پاس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خبر بازجستند از آن هوشمند</p></div>
<div class="m2"><p>که پیدا کن احوال چرخ بلند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگو تا چه جوهر شد این آسمان</p></div>
<div class="m2"><p>کزو دور شد هر کسی را گمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شتابنده راه دیگر سرای</p></div>
<div class="m2"><p>چنین گفت کایزد بود رهنمای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسی رهبری بر فلک ساختم</p></div>
<div class="m2"><p>بدین دل که من پرده بشناختم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو خواهم شد اکنون به بیچارگی</p></div>
<div class="m2"><p>درین ره نبینم جز آوارگی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جهان فیلسوف جهان خواندم</p></div>
<div class="m2"><p>رصد بند هفت آسمان داندم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جهان مدخل از دانش آراستم</p></div>
<div class="m2"><p>نبشتم درو هر چه می‌خواستم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه در شناسائی اختران</p></div>
<div class="m2"><p>فرو گفته احوال گردون درآن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کنون کز یقین گفت باید سخن</p></div>
<div class="m2"><p>رها کن رصد نامهای کهن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به یزدان پاک ار مرا آگهیست</p></div>
<div class="m2"><p>که این خوان پوشیده پر یا تهیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سخن چون بدینجا رسانید ساز</p></div>
<div class="m2"><p>سخنگوی مرد از سخن ماند باز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بپالود روغن ز روشن چراغ</p></div>
<div class="m2"><p>بفرمود کارند سیبی ز باغ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به کف برنهاد آن نوازنده سیب</p></div>
<div class="m2"><p>به بوئی همی داد جان را شکیب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نفس را چو زین طارم نیل رنگ</p></div>
<div class="m2"><p>گذرگه درآمد به دهلیز تنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بخندید و گفت الرحیل ای گروه</p></div>
<div class="m2"><p>که صبح مرا سر برآمد ز کوه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز یزدان پاک آمد این جان پاک</p></div>
<div class="m2"><p>سپردم دگر ره به یزدان پاک</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بگفت این و برزد یکی باد سرد</p></div>
<div class="m2"><p>برآورد گردون ازو نیز گرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چوبگذشت و بگذاشت آسیب را</p></div>
<div class="m2"><p>به باران بینداخت آن سیب را</p></div></div>