---
title: >-
    بخش ۴۸ - انجامش روزگار نظامی
---
# بخش ۴۸ - انجامش روزگار نظامی

<div class="b" id="bn1"><div class="m1"><p>مغنی ره رامش جان بساز</p></div>
<div class="m2"><p>نوازش کنم زان ره دل‌نواز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان زن نوا از یکی تا به صد</p></div>
<div class="m2"><p>که در بزم خسرو زدی باربد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظامی چو این داستان شد تمام</p></div>
<div class="m2"><p>به عزم شدن نیز برداشت گام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه بس روزگاری برین برگذشت</p></div>
<div class="m2"><p>که تاریخ عمرش ورق در نوشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فزون بود شش مه ز شصت و سه سال</p></div>
<div class="m2"><p>که بر عزم ره بر دهل زد دوال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو حال حکیمان پیشینه گفت</p></div>
<div class="m2"><p>حکیمان بخفتند و او نیز خفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفیقان خود را به گاه رحیل</p></div>
<div class="m2"><p>گه از ره خبرداد و گاه از دلیل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخندید و گفتا که آمرزگار</p></div>
<div class="m2"><p>به آمرزشم کرد امیدوار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زما زحمت خویش دارید دور</p></div>
<div class="m2"><p>شما وین‌سرا ما و دارالسرور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین گفتگو بد که خوابش ربود</p></div>
<div class="m2"><p>تو گفتی که بیداریش خود نبود</p></div></div>