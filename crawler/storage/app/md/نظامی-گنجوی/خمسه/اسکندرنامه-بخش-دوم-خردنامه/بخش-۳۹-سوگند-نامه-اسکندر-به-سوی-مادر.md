---
title: >-
    بخش ۳۹ - سوگند نامه اسکندر به سوی مادر
---
# بخش ۳۹ - سوگند نامه اسکندر به سوی مادر

<div class="b" id="bn1"><div class="m1"><p>مغنی دگر باره بنواز رود</p></div>
<div class="m2"><p>به یادآر از آن خفتگان در سرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببین سوز من ساز کن ساز تو</p></div>
<div class="m2"><p>مگر خوش بخفتم برآواز تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو برگل شبیخون کند زمهریر</p></div>
<div class="m2"><p>به طفلی شود شاخ گلبرگ پیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشاید شدن مرگ را چاره‌ساز</p></div>
<div class="m2"><p>در چاره برکس نکردند باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تب مرگ چون قصد مردم کند</p></div>
<div class="m2"><p>علاج از شناسنده پی گم کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شب را گزارش درآمد به زیست</p></div>
<div class="m2"><p>بخندید خورشید و شبنم گریست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهاندار نالنده‌تر شد ز دوش</p></div>
<div class="m2"><p>ز بانگ جرسها برآمد خروش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ارسطو جهاندیدهٔ چاره ساز</p></div>
<div class="m2"><p>به بیچارگی ماند از آن چاره باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کامید بهی در شهنشه ندید</p></div>
<div class="m2"><p>در اندازهٔ کار او ره ندید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به شه گفت کای شمع روشن روان</p></div>
<div class="m2"><p>به تو چشم روشن همه خسروان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو پروردگان را نظر شد زکار</p></div>
<div class="m2"><p>نظر دار بر فیض پروردگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن پیشتر کامد این سیل تیز</p></div>
<div class="m2"><p>چرا بر نیامد ز ما رستخیز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وزان پیش کاین می‌بریزد به جام</p></div>
<div class="m2"><p>چرا جان ما بر نیامد ز کام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نخواهم که موئیت لرزان شود</p></div>
<div class="m2"><p>ترا موی افتد مرا جان شود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ولیک از چنین شربتی ناگزیر</p></div>
<div class="m2"><p>نباشد کس ایمن زبرنا و پیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه دل می‌دهد گفتن این می بنوش</p></div>
<div class="m2"><p>که میخوارگان را برآرد ز هوش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه گفتن توان کاین صراحی بریز</p></div>
<div class="m2"><p>که در بزم شه کرد نتوان ستیز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دریغا چراغی بدین روشنی</p></div>
<div class="m2"><p>بخواهد نشستن ز بی روغنی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مدار از تهی روغنی دل به داغ</p></div>
<div class="m2"><p>که ناگه ز پی برفروزد چراغ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جهاندار گفتا ازین درگذر</p></div>
<div class="m2"><p>که آمد مرا زندگانی بسر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به فرمان من نیست گردان سپهر</p></div>
<div class="m2"><p>نه من داده‌ام گردش ماه و مهر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کفی خاکم و قطره‌ای آب سست</p></div>
<div class="m2"><p>ز نر ماده‌ای آفریده نخست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز پروردگیهای پروردگار</p></div>
<div class="m2"><p>به آنجا رسیدم سرانجام کار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که چندان که شاید شدن پیش و پس</p></div>
<div class="m2"><p>مرا بود بر جملگی دسترس</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در آن وقت کردم جهان خسروی</p></div>
<div class="m2"><p>که هم جان قوی بود و هم تن قوی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو آمد کنون ناتوانی پدید</p></div>
<div class="m2"><p>به دیگر کده رخت باید کشید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مده بیش ازینم شراب غرور</p></div>
<div class="m2"><p>که هست آب حیوان ازین چاه دور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زدوزخ مشو تشنه را چاره جوی</p></div>
<div class="m2"><p>سخن در بهشتست و آن چارجوی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دعا را به آمرزش آور به کار</p></div>
<div class="m2"><p>مگر رحمتی بخشد آمرزگار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو رخت از بر کوه برد آفتاب</p></div>
<div class="m2"><p>سر شاه شاهان در آمد به خواب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شب آمد چه شب کاژدهائی سیاه</p></div>
<div class="m2"><p>فرو بست ظلمت پس و پیش راه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شبی سخت بی مهر و تاریک چهر</p></div>
<div class="m2"><p>به تاریکی اندر که دیدست مهر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ستاره گره بسته بر کارها</p></div>
<div class="m2"><p>فرو دوخته لب به مسمارها</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فلک دزد و ماه فلک دزدگیر</p></div>
<div class="m2"><p>بهم هردو افتاده در خم قیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جهان چون سیه دودی انگیخته</p></div>
<div class="m2"><p>به موئی ز دوزخ درآویخته</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در آن شب بدانگونه بگداخت شاه</p></div>
<div class="m2"><p>که در بیست و هفتم شب خویش ماه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو از مهر مادر به یاد آمدش</p></div>
<div class="m2"><p>پریشانی اندر نهاد آمدش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بفرمود کز رومیان یک دبیر</p></div>
<div class="m2"><p>که باشد خردمند و بیدار و پیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به دود سیه در کشد خامه را</p></div>
<div class="m2"><p>نویسد سوی مادرش نامه را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در آن نامه سوگندهای گران</p></div>
<div class="m2"><p>فریبنده چون لابه مادران</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که از بهر من دل نداری نژند</p></div>
<div class="m2"><p>نکوشی به فریاد ناسودمند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دبیر زبان آور از گفت شاه</p></div>
<div class="m2"><p>جهان کرد برنامه خوانان سیاه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دو شاخه سرکلک یک شاخ کرد</p></div>
<div class="m2"><p>فلک را به فرهنگ سوراخ کرد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو بر شقهٔ کاغذ آمد عبیر</p></div>
<div class="m2"><p>شد اندام کاغذ چو مشگین حریر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز پرگار معنی که باریک شد</p></div>
<div class="m2"><p>نویسنده را چشم تاریک شد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>پس از آفرین آفریننده را</p></div>
<div class="m2"><p>که بینائی او داد بیننده را</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>یکی و بدو هر یکی را نیاز</p></div>
<div class="m2"><p>یکایک همه خلق را کارساز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چنین بسته بود آن فروزان نگار</p></div>
<div class="m2"><p>از آن پرورشها که آید به کار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>که این نامه از من که اسکندرم</p></div>
<div class="m2"><p>سوی چار مادر نه یک مادرم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>که گر قطره شد چشمه بدرود باد</p></div>
<div class="m2"><p>شکسته سبو برلب رود باد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگر سرخ سیبی درآمد به گرد</p></div>
<div class="m2"><p>ز رونق میفتاد نارنج زرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بر این زرد گل گرستم کرد باد</p></div>
<div class="m2"><p>درخت گل سرخ سرسبز باد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نه این گویم ای مادر مهربان</p></div>
<div class="m2"><p>که مهر از دل آید فزون از زبان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بسوزی یکی گر خبر بشنوی</p></div>
<div class="m2"><p>که چون شد به باد آن گل خسروی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مسوز از پی دست پرورد خویش</p></div>
<div class="m2"><p>بنه دست بر سوزش درد خویش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ازین سوزت ایام دوری دهاد</p></div>
<div class="m2"><p>خدایت درین غم صبوری دهاد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به شیری که خوردم ز پستان تو</p></div>
<div class="m2"><p>به خواب خوشم در شبستان تو</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به سوز دل مادر پیش میر</p></div>
<div class="m2"><p>که باشد جوان مرده و او مانده پیر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به فرمان پذیران دنیا و دین</p></div>
<div class="m2"><p>به فرماندهٔ آسمان و زمین</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به حجت نویسان دیوان خاک</p></div>
<div class="m2"><p>به جاوید مانان مینوی پاک</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به زندانیان زمین زیر خشت</p></div>
<div class="m2"><p>به نزهت نشینان خاک بهشت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به جانی کزو جانور شد نبات</p></div>
<div class="m2"><p>به جان داوری کارد از غم نجات</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به موجی که خیزد ز دریای جود</p></div>
<div class="m2"><p>به امری کزو سازور شد وجود</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به آن نام کز نامها برترست</p></div>
<div class="m2"><p>به آن نقش کارایش پیکرست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به پرگار هفت آسمان بلند</p></div>
<div class="m2"><p>به فهرست هفت اختر ارجمند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به آگاهی مرد یزدان شناس</p></div>
<div class="m2"><p>به ترسائی عقل صاحب قیاس</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به هر شمع کز دانش افروختند</p></div>
<div class="m2"><p>به هر کیسه کز فیض بر دوختند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به فرقی که دولت براو تافتست</p></div>
<div class="m2"><p>به پائی که راه رضا یافتست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به پرهیز گاران پاکیزه‌رای</p></div>
<div class="m2"><p>به باریک بینان مشکل گشای</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به خوشبوئی خاک افتادگان</p></div>
<div class="m2"><p>به خوش‌خوئی طبع آزادگان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به آزرم سلطان درویش دوست</p></div>
<div class="m2"><p>به درویش قانع که سلطان خود اوست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>به سرسبزی صبح آراسته</p></div>
<div class="m2"><p>به مقبولی نزل ناخواسته</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به شب زنده داران بیگاه خیز</p></div>
<div class="m2"><p>به خاکی غریبان خونابه ریز</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به شب ناله تلخ زندانیان</p></div>
<div class="m2"><p>به قندیل محراب روحانیان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به محتاجی طفل تشنه به شیر</p></div>
<div class="m2"><p>به نومیدی دردمندان پیر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به ذل غریبان بیمار توش</p></div>
<div class="m2"><p>به اشک یتیمان پیچیده گوش</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>به عزلت نشینان صحرای درد</p></div>
<div class="m2"><p>به ناخن کبودان سرمای سرد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>به ناخفتگیهای غمخوارگان</p></div>
<div class="m2"><p>به درماندگیهای بیچارگان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به رنجی که خسبد برآسودگی</p></div>
<div class="m2"><p>به عشقی که پاکست از آلودگی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>به پیروزی عقل کوتاه دست</p></div>
<div class="m2"><p>به خرسندی زهد خلوت پرست</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>به حرفی که در دفتر مردمیست</p></div>
<div class="m2"><p>به نقشی که محمل کش آدمیست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>به دردی که زخمش پدیدار نیست</p></div>
<div class="m2"><p>به زخمی که با مرهمش کار نیست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>به صبری که در ناشکیبا بود</p></div>
<div class="m2"><p>به شرمی که در روی زیبا بود</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به فریاد فریاد آن یک نفس</p></div>
<div class="m2"><p>که نومید باشد ز فریادرس</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>به صدقی که روید زدین پروران</p></div>
<div class="m2"><p>به وحیی که آید به پیغمبران</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بدان ره کزو نیست کس را گزیر</p></div>
<div class="m2"><p>بدان راهبر کو بود دستگیر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به آن در کزین درگذشتن به دوست</p></div>
<div class="m2"><p>مرا و ترا بازگشتن به دوست</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>به نادیدن روی دمساز تو</p></div>
<div class="m2"><p>به محرومی گوش از آواز تو</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>به آن آرزو کز منت بس مباد</p></div>
<div class="m2"><p>بدین عاجزی کاین چنین کس مباد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>به داد آفرینی که دارنده اوست</p></div>
<div class="m2"><p>همان جان ده و جان برآرنده اوست</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>که چون این وثیقت رسد سوی تو</p></div>
<div class="m2"><p>نگیرد گره طاق ابروی تو</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>مصیبت نداری نپوشی پلاس</p></div>
<div class="m2"><p>به هنجار منزل شوی ره شناس</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>نپیچی به ناله نگردی ز راه</p></div>
<div class="m2"><p>کنی در سرانجام گیتی نگاه</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>اگر ماندنی شد جهان بر کسی</p></div>
<div class="m2"><p>بمان در غم و سوگواری بسی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ور ایدونکه بر کس نماند جهان</p></div>
<div class="m2"><p>تو نیز آشنا باش با همرهان</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>گرت رغبت آید که انده خوری</p></div>
<div class="m2"><p>کنی سوگواری و ماتم گری</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>از آن پیش کانده خوری زینهار</p></div>
<div class="m2"><p>برآرای مهمانیی شاهوار</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>بخوان خلق را جمله مهمان خویش</p></div>
<div class="m2"><p>منادی برانگیز بر خوان خویش</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>که آن کس خورد این خورشهای پاک</p></div>
<div class="m2"><p>که غایب نباشد ورا زیر خاک</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>اگر زان خورشها خورد میهمان</p></div>
<div class="m2"><p>تو نیز انده من بخور در زمان</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>وگر کس نیارد نظر سوی خورد</p></div>
<div class="m2"><p>تو نیز انده غایبان درنورد</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>غم من مخور کان من در گذشت</p></div>
<div class="m2"><p>به کار غم خویش کن بازگشت</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چنان دان که پایم دوچندین درنگ</p></div>
<div class="m2"><p>نه هم پای عمرم درآید به سنگ؟</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چو بسیاری عمر ما اندکیست</p></div>
<div class="m2"><p>اگر ده بود سال و گر صد یکیست</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>چرا ترسم از رفتن هشت باغ</p></div>
<div class="m2"><p>که در با کلیدست و ره با چراغ</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>چرا سر نیارم سوی آن سریر</p></div>
<div class="m2"><p>که جاوید باشم بر او جایگیر</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چرا خوش ترانم بدان صیدگاه</p></div>
<div class="m2"><p>که بی دود ابرست و بی گرد راه</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>چو بر من نماند این سرای فریب</p></div>
<div class="m2"><p>زمن باد واماندگان را شکیب</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>چو شبدیز من جست از این تند رود</p></div>
<div class="m2"><p>زمن باد بر دوستداران درود</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>رهانید ما را فلک زین حصار</p></div>
<div class="m2"><p>که بادا همه کس چو ما رستگار</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>چو نامه بسر برد و عنوان نبشت</p></div>
<div class="m2"><p>فرستاد و خود رفت سوی بهشت</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>به صد محنت آورد شب را به روز</p></div>
<div class="m2"><p>همه روز نالید با درد و سوز</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>دیگر شب که شب تخت بر پیل زد</p></div>
<div class="m2"><p>زمین چون فلک جامه در نیل زد</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>چو خورشید گردنده بر گرد روی</p></div>
<div class="m2"><p>در آن شب ز ناخن برآورد موی</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>ستاره فروریخت ناخن ز چنگ</p></div>
<div class="m2"><p>هوا شد پر از ناخن سیم رنگ</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>ز دیده فرو بستن روی شاه</p></div>
<div class="m2"><p>به ناخن خراشیدهٔ روی ماه</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>پلاسی ز گیسوی شب ساختند</p></div>
<div class="m2"><p>زمین را به گردن درانداختند</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>ز کام ذنب زهری انگیختند</p></div>
<div class="m2"><p>مه چرخ را در گلو ریختند</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>دگرگونه شد شاه از آیین خویش</p></div>
<div class="m2"><p>کاجل دید بالای بالین خویش</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>بیفشرد خون رگش زیر پی</p></div>
<div class="m2"><p>ز جوشیدن خون بر آورد خوی</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>سیاهی ز دیده بدزدید خال</p></div>
<div class="m2"><p>سپیده دمش را درآمد زوال</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>به جان آمد و جانش از کار شد</p></div>
<div class="m2"><p>دم جان سپردن پدیدار شد</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>بخندید و در خنده چون شمع مرد</p></div>
<div class="m2"><p>بدان کس که جان داد جان را سپرد</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>ز شمع دمنده چنان رفت نور</p></div>
<div class="m2"><p>کز او ماند بیننده را چشم دور</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>شتابنده مرغ آن چنان بر پرید</p></div>
<div class="m2"><p>که تا آشیان هیچ مرغش ندید</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>ندیدم کسی را زکار آگهان</p></div>
<div class="m2"><p>که آگه شد از کارهای نهان</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>درین کار اگر چارهٔ کس شناخت</p></div>
<div class="m2"><p>چرا چارهٔ کار خود را نساخت</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>سکندر چو بربست ازین خانه رخت</p></div>
<div class="m2"><p>زدندش به بالای این خیمه تخت</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>چه نیکی که اندر جهان او نکرد</p></div>
<div class="m2"><p>جهانش بیازرد و نیکو نکرد</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>سرانجام چون در پس پرده رفت</p></div>
<div class="m2"><p>ز بیداد گیتی دل آزرده رفت</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>اگر چه ز ره تافتن تفته بود</p></div>
<div class="m2"><p>رهی رفت کان راه نارفته بود</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>ره انجام را هر کجا ساز داد</p></div>
<div class="m2"><p>از آن ره به گیتی خبر باز داد</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>چرا چون به کوچ عدم راه رفت</p></div>
<div class="m2"><p>خبرهای آن راه با کس نگفت</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>مگر هر که درگیرد این راه پیش</p></div>
<div class="m2"><p>فرامش کند راه گفتار خویش</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>اگر گفتنی بودی این قصه باز</p></div>
<div class="m2"><p>نهفته نماندی درین پرده راز</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>بهار سکندر چو از باد سخت</p></div>
<div class="m2"><p>به خاک اوفتاد از کیانی درخت</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>زدند از کمرهای زرکار او</p></div>
<div class="m2"><p>یکی مهد زرین سزاوار او</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>پرند درونش ز کافور پر</p></div>
<div class="m2"><p>به دیبای بیرون برآموده در</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>از اندودن مشک و ماورد و عود</p></div>
<div class="m2"><p>به جودی شده موج طوفان جود</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>رقیبی که عطرش کفن سای کرد</p></div>
<div class="m2"><p>به تابوت زرین درش جای کرد</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>چو تن مرد و اندام چون سیم سود</p></div>
<div class="m2"><p>کفن عطر و تابوت سیمین چه سود</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>ز تابوت فرموده بد شهریار</p></div>
<div class="m2"><p>که یک دست او را کنند آشکار</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>در آن دست خاکی تهی ریخته</p></div>
<div class="m2"><p>منادی ز هر سو برانگیخته</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>که فرمانده هفت کشور زمین</p></div>
<div class="m2"><p>همین یک تن آمد ز شاهان همین</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>ز هر گنج دنیا که دربار بست</p></div>
<div class="m2"><p>بجز خاک چیزی ندارد به دست</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>شما نیز چون از جهان بگذرید</p></div>
<div class="m2"><p>ازین خاکدان تیره خاکی برید</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>سوی مصر بردندش از شهر زور</p></div>
<div class="m2"><p>که بود آن دیار از بد اندیش دور</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>به اسکندریش وطن ساختند</p></div>
<div class="m2"><p>ز تختش به تخته در انداختند</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>ز داغ جهان هیچ‌کس جان نبرد</p></div>
<div class="m2"><p>کس این رقعه با او به پایان نبرد</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>برابر در ایوان آن تختگاه</p></div>
<div class="m2"><p>نهادند زیرزمین تخت شاه</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>ندارد جهان دوستی با کسی</p></div>
<div class="m2"><p>نیابی درو مهربانی بسی</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>به خاکش سپردند و گشتند باز</p></div>
<div class="m2"><p>در دخمه کردند بر وی فراز</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>جهان را بدینگونه شد رسم و راه</p></div>
<div class="m2"><p>به آرد بگاه و ندارد نگاه</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>به پایان رساندند چندین هزار</p></div>
<div class="m2"><p>نیامد به پایان هنوز این شمار</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>نه زین رشته سر می‌توان تافتن</p></div>
<div class="m2"><p>نه سر رشته را می‌توان یافتن</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>تجسس گری شرط این کوی نیست</p></div>
<div class="m2"><p>درین پرده جز خامشی روی نیست</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>ببین در جهان گر جهان دیده‌ای</p></div>
<div class="m2"><p>کز و چند کس را زیان دیده‌ای</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>جهانی که با این‌چنین خواریست</p></div>
<div class="m2"><p>نه در خورد چندین ستمگاریست</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>چه بینی درین طارم سرمه گون</p></div>
<div class="m2"><p>که می آید از میل او سیل خون</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>چو خورشید شد آتشین میل او</p></div>
<div class="m2"><p>در انداز سنگی به قندیل او</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>درین میل منگر که زرین وشست</p></div>
<div class="m2"><p>که آن زر نه از سرخی آتشست</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>سر سازگاری ندارد سپهر</p></div>
<div class="m2"><p>کمر بسته بر کین ما ماه و مهر</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>مشو جفت این جادوی زرق ساز</p></div>
<div class="m2"><p>که پنهان کشست آشکارا نواز</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>برون لاف مرهم پرستی زند</p></div>
<div class="m2"><p>درون زخمهای دو دستی زند</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>ز شغل جهان درکش ایدوست دست</p></div>
<div class="m2"><p>که ماهی بدین جوشن از تیغ رست</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>چو طوفان انصاف خواهی بود</p></div>
<div class="m2"><p>نترسد ز غرق آنکه ماهی بود</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>جهان چون دکان بریشم کشیست</p></div>
<div class="m2"><p>ازو نیمی آبی دگر آتشیست</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>دهد حلقه‌ای را ازینسو بهی</p></div>
<div class="m2"><p>وزان سو کند حلقه‌ای را تهی</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>به گیتی پژوهی چه پائیم دیر</p></div>
<div class="m2"><p>که دودیست بالا و گردیست زیر</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>بدان ماند احوال این دود و گرد</p></div>
<div class="m2"><p>که هست آسمان با زمین در نبرد</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>اگر آسمان با زمین ساختی</p></div>
<div class="m2"><p>ز ما هر زمانش نپرداختی</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>نظامی گره برزن این بند را</p></div>
<div class="m2"><p>مترس و مترسان تنی چند را</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>به مهمانی بزم سلطان شدن</p></div>
<div class="m2"><p>نشاید بره بر پشیمان شدن</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>چو سلطان صلا دردهد گوش کن</p></div>
<div class="m2"><p>می تلخ بر یاد او نوش کن</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>سکندر کزان جام چون گل شکفت</p></div>
<div class="m2"><p>ستد جام و بر یاد او خورد و خفت</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>کسی را که آن می‌خورد نوش باد</p></div>
<div class="m2"><p>بجز یاد سلطان فراموش باد</p></div></div>