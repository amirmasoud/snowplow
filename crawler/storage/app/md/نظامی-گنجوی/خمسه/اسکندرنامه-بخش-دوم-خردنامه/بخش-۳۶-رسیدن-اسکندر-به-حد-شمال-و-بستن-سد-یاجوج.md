---
title: >-
    بخش ۳۶ - رسیدن اسکندر به حد شمال و بستن سد یاجوج
---
# بخش ۳۶ - رسیدن اسکندر به حد شمال و بستن سد یاجوج

<div class="b" id="bn1"><div class="m1"><p>مغنی دل تنگ را چاره نیست</p></div>
<div class="m2"><p>بجز سازکان هست و بیغاره نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دماغ مرا کز غم آمد به جوش</p></div>
<div class="m2"><p>به ابریشم ساز کن حلقه گوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو در خانه خویش رفت آفتاب</p></div>
<div class="m2"><p>ز گرمی شد اندام شیران کباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تبشهای باحوری از دستبرد</p></div>
<div class="m2"><p>ز روی هوا چرک تری سترد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیا دانه بگشاد و نبوشت برگ</p></div>
<div class="m2"><p>بلاله ستان اندر افتاد مرگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بجوشید در کوه و صحرا بخار</p></div>
<div class="m2"><p>شکر خنده زد میوه بر میوده‌دار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز هامون سوی کوه شد عندلیب</p></div>
<div class="m2"><p>به غربت همی گفت چیزی غریب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به گوش اندرش از هوای تموز</p></div>
<div class="m2"><p>نوای چکاوک نیامد هنوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درفشنده خورشید گردون نورد</p></div>
<div class="m2"><p>ز باد خزان نیش عقرب نخورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شب و روز می‌گشت در چین و زنگ</p></div>
<div class="m2"><p>به دود افکنی طشت آتش به چنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شیران درید از سردست زور</p></div>
<div class="m2"><p>گهی ساق گاو و گهی سم گور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در ایام با حور و گرمای گرم</p></div>
<div class="m2"><p>که از تاب خورشید شد سنگ نرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سکندر ز چین رای خرخیز کرد</p></div>
<div class="m2"><p>در خواب را تنگ دهلیز کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رها کرد خاقان چین را به جای</p></div>
<div class="m2"><p>دگر باره سوی سفر کرد رای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسی گنج در پیش خاقان کشید</p></div>
<div class="m2"><p>وز آنجا سپه در بیابان کشید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فرو کوفت بر کوس دولت دوال</p></div>
<div class="m2"><p>ز مشرق درآمد به حد شمال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیابان و ریگ روان دید و بس</p></div>
<div class="m2"><p>نه پرنده دروی نه جنبنده کس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسی رفت و کس در بیابان ندید</p></div>
<div class="m2"><p>همان راه را نیز پایان ندید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زمین دید رخشان و از رخنه دور</p></div>
<div class="m2"><p>درو ریگ رخشنده مانند نور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به شه گفت رهبر که این ریگ پاک</p></div>
<div class="m2"><p>همه نقره شد نقرهٔ تابناک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به اندازه بردار ازین راه گنج</p></div>
<div class="m2"><p>نه چندان که محمل کش آید به رنج</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به لشگر مگوور نه از عشق سیم</p></div>
<div class="m2"><p>گران‌بار گردند و یابند بیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همه بارشه بود پر زر ناب</p></div>
<div class="m2"><p>بدان نقره نامد دلش را شتاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ولیک آرزو درمنش کار کرد</p></div>
<div class="m2"><p>ازو اشتری چند را بار کرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدان راه می‌رفت چون باد تیز</p></div>
<div class="m2"><p>هوا را ندید از زمین گرد خیز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به یک هفته ننشست بر جامه گرد</p></div>
<div class="m2"><p>که از نقره بود آن زمین را نورد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو گفتی که شد خاک و آبش دونیم</p></div>
<div class="m2"><p>یکی نیمه سیماب و یک نیمه سیم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نه در سیمش آرام شایست کرد</p></div>
<div class="m2"><p>نه سیماب را نیز شایست خورد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز سودای ره کان نه کم درد بود</p></div>
<div class="m2"><p>سوادی بدان سیم در خورد بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کجا چشمه‌ای بود مانند نوش</p></div>
<div class="m2"><p>در آن آب سیماب را بود جوش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو شورش نبودی در آب زلال</p></div>
<div class="m2"><p>ز سیماب کس را نبودی ملال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بخوردندی آن آبها را دلیر</p></div>
<div class="m2"><p>که آب از زبر بود و سیماب زیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو شورش در آب آمدی پیش و پس</p></div>
<div class="m2"><p>نخوردندی آن آب را هیچ‌کس</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وگر خوردی از راه غفلت کسی</p></div>
<div class="m2"><p>نماندی درو زندگانی بسی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بفرمود شه تا چو رای آورند</p></div>
<div class="m2"><p>در آن آب دانش به جای آورند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنان برکشند آب را زابگیر</p></div>
<div class="m2"><p>که ساکن بود آب جنبش پذیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدین‌گونه یک ماه رفتند راه</p></div>
<div class="m2"><p>بسی مردم از تشنگی شد تباه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رسیدند از آن مفرش سیم سود</p></div>
<div class="m2"><p>به خاکی کزاو بودشان زاد بود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نهادند برخاک رخسار پاک</p></div>
<div class="m2"><p>که خاکی نیاساید الا به خاک</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پدید آمد آرامگاهی زدور</p></div>
<div class="m2"><p>چنان کز شب تیزه تابنده هور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بر افراخته طاقی از تیغ کوه</p></div>
<div class="m2"><p>که از دیدنش در دل آمد شکوه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به بالای آن طاق پیروزه رنگ</p></div>
<div class="m2"><p>کشیده کمر کوهی از خاره سنگ</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گروهی بر آن کوه دین پروران</p></div>
<div class="m2"><p>مسلمان و فارغ ز پیغمبران</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به الهام یزدان ز روی قیاس</p></div>
<div class="m2"><p>در احوال خود گشته یزدان شناس</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو دیدند سیمای اسکندری</p></div>
<div class="m2"><p>پذیرا شدندش به پیغمبری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به تعلیم او خاطر آراستند</p></div>
<div class="m2"><p>وزو دانش و داد درخواستند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سکندر برایشان در دین گشاد</p></div>
<div class="m2"><p>بجز دین و دانش بسی چیز داد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو دیدند شاهی چنان چاره ساز</p></div>
<div class="m2"><p>به چاره گری در گشادند باز</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>که شفقت برای داور دستگیر</p></div>
<div class="m2"><p>براین زیر دستان فرمان پذیر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پس این گریوه در این سنگلاخ</p></div>
<div class="m2"><p>یکی دشت بینی چو دریا فراخ</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گروهی در آن دشت یاجوج نام</p></div>
<div class="m2"><p>چو ما آدمی زاده و دیو فام</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو دیوان آهن دل الماس چنگ</p></div>
<div class="m2"><p>چو گرگان بد گوهر آشفته رنگ</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>رسیده ز سر تا قدم مویشان</p></div>
<div class="m2"><p>نبینی نشانی تو از رویشان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به چنگال و دندان همه چون دده</p></div>
<div class="m2"><p>به خون ریختن چنگ و دندان زده</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بگیرند هنگام تک باد را</p></div>
<div class="m2"><p>به ناخن بسنبند پولاد را</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همه در خرام و خورش ناسپاس</p></div>
<div class="m2"><p>نه بینی در ایشان کس ایزد شناس</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>زهر طعمه‌ای کان بود جستنی</p></div>
<div class="m2"><p>طعامی ندارند جز رستنی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ندارند جز خواب و جز خورد کار</p></div>
<div class="m2"><p>نمیرد یکی تا نزاید هزار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گیائیست آنجا زمین خیزشان</p></div>
<div class="m2"><p>چو بلبل بود دانه تیزشان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از آن هر شبان روز بهری خورند</p></div>
<div class="m2"><p>همانجا بخسبند و درنگذرند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو بر آفتاب افکند ماه جرم</p></div>
<div class="m2"><p>بجوشنده برخود به کردار کرم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>خورند آنچه یابند بی ترس و بیم</p></div>
<div class="m2"><p>بدین گونه تا ماه گردد دو نیم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو گیرد گمی ماه ناکاسته</p></div>
<div class="m2"><p>شره گردد از جمله برخاسته</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>فتد سال تا سال از ابر سیاه</p></div>
<div class="m2"><p>ستمکاره تنینی آن جایگاه</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به اندازه آنک در دشت و کوه</p></div>
<div class="m2"><p>از او سیر کردند چندان گروه</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به امید آن کوه دریا ستیز</p></div>
<div class="m2"><p>که اندازدش ابر سیلاب ریز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو آواز تندر خروش آورند</p></div>
<div class="m2"><p>زمین را ز دوزخ به جوش آورند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز سرمستی خون آن اژدها</p></div>
<div class="m2"><p>کنند آب و دانه یکی مه رها</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دگر خوردشان نیست جز بیخ و برگ</p></div>
<div class="m2"><p>نباشند بیمار تا روز مرگ</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو میرد از ایشان یکی آن گروه</p></div>
<div class="m2"><p>خورندش همانسان در آن دشت و کوه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نه مردار ماند در آن خاک شور</p></div>
<div class="m2"><p>نه کس مرده‌ای نیز بیند نه گور</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>جز این یک هنر نیست کان آب و خاک</p></div>
<div class="m2"><p>ز مردار دورست و از مرده پاک</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بهر مدت آرند بر ما شتاب</p></div>
<div class="m2"><p>کنند آشیانهای ما را خراب</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ز ما گوسپندان به غارت برند</p></div>
<div class="m2"><p>خورشهای ما هر چه باشد خورند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز گرگ آن چنان کم گریزد گله</p></div>
<div class="m2"><p>کزان گرگساران سگ مشغله</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو درما به کشتن ستیز آورند</p></div>
<div class="m2"><p>بکوشند و بر ما گریز آورند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>گریزیم از ایشان بر این کوه سخت</p></div>
<div class="m2"><p>به کردار پرندگان بر درخت</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ندارند پائی چنان آن گروه</p></div>
<div class="m2"><p>که ما را درارند از آن تیغ کوه</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به دفع چنان سخت پتیاره‌ای</p></div>
<div class="m2"><p>ثوابت بود گر کنی چاره‌ای</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو بشنید شه حکم یا جوج را</p></div>
<div class="m2"><p>که پیل افکند هر یکی عوج را</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بدان گونه سدی ز پولاد بست</p></div>
<div class="m2"><p>که تا رستخیزش نباشد شکست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چو طالع نمود آن بلند اختری</p></div>
<div class="m2"><p>که شد ساخته سد اسکندری</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>از آن مرحله سوی شهری شتافت</p></div>
<div class="m2"><p>که بسیار کس جست و آن را نیافت</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>دگر باره در کار عالم روی</p></div>
<div class="m2"><p>روان شد سراپردهٔ خسروی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بر آن کار چون مدتی برگذشت</p></div>
<div class="m2"><p>بتازید یک ماه بر کوه و دشت</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>پدید آمد آراسته منزلی</p></div>
<div class="m2"><p>که از دیدنش تازه شد هر دلی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>جهاندار با ره بسیچان خویش</p></div>
<div class="m2"><p>ره آورد چشم از ره آورد پیش</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>دگرگونه دید آن زمین را سرشت</p></div>
<div class="m2"><p>هم آب روان دید هم کار و کشت</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>همه راه بر باغ و دیوار نی</p></div>
<div class="m2"><p>گله در گله کس نگهدارنی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ز لشگر یکی دست برزد فراخ</p></div>
<div class="m2"><p>کزان میوه‌ای برگشاید ز شاخ</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>نچیده یکی میوه‌تر هنوز</p></div>
<div class="m2"><p>ز خشکی تنش چون کمان گشت کوز</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>سواری دگر گوسپندی گرفت</p></div>
<div class="m2"><p>تبش کرد و زان کار بندی گرفت</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>سکندر چو زین عبرت آگاه گشت</p></div>
<div class="m2"><p>ز خشک و ترش دست کوتاه گشت</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بفرمود تا هر که بود از سپاه</p></div>
<div class="m2"><p>ز باغ کسان دست دارد نگاه</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>چو لختی گراینده شد در شتاب</p></div>
<div class="m2"><p>گذر کرد از آن سبزه و جوی آب</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>پدیدار شد شهری آراسته</p></div>
<div class="m2"><p>چو فردوسی از نعمت و خواسته</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>چو آمد به دروازه شهر تنگ</p></div>
<div class="m2"><p>ندیدش دری زآهن و چوب و سنگ</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>در آن شهر شد باتنی چند پیر</p></div>
<div class="m2"><p>همه غایت اندیش و عبرت پذیر</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>دکانها بسی یافت آراسته</p></div>
<div class="m2"><p>درو قفل از جمله برخاسته</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>مقیمان آن شهر مردم نواز</p></div>
<div class="m2"><p>به پیش آمدندش به صد عذر باز</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>فرود آوریدندش از ره به کاخ</p></div>
<div class="m2"><p>به کاخی چو مینوی مینا فراخ</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بسی خوان نعمت برآراستند</p></div>
<div class="m2"><p>نهادند و خود پیش برخاستند</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>پرستش نمودند با صد نیاز</p></div>
<div class="m2"><p>زهی میزبانان مهمان نواز</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چو پذرفت شه نزلشان را به مهر</p></div>
<div class="m2"><p>بدان خوب چهران برافروخت چهر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>بپرسیدشان کاین چنین بی هراس</p></div>
<div class="m2"><p>چرائید و خود را ندارید پاس</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بدین ایمنی چون زیبد از گزند</p></div>
<div class="m2"><p>که بر در ندارد کسی قفل و بند</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>همان باغبان نیست در باغ کس</p></div>
<div class="m2"><p>رمه نیز چوپان ندارد ز پس</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>شبانی نه و صد هزاران گله</p></div>
<div class="m2"><p>گله کرده بر کوه و صحرا یله</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>چگونست و این ناحفاظی ز چیست</p></div>
<div class="m2"><p>حفاظ شما را تولا به کیست</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>بزرگان آن داد پرور دیار</p></div>
<div class="m2"><p>دعا تازه کردند بر شهریار</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>که آن کس که بر فرقت افسر نهاد</p></div>
<div class="m2"><p>بقای تو بر قدر افسر دهاد</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>خدا باد در کارها یاورت</p></div>
<div class="m2"><p>هنر سکه نام نام آورت</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>چو پرسیدی از حال ما نیک و بد</p></div>
<div class="m2"><p>بگوئیم شه را همه حال خود</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>چنان دان حقیقت که ما این گروه</p></div>
<div class="m2"><p>که هستیم ساکن درین دشت و کوه</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>گروهی ضعیفان دین پروریم</p></div>
<div class="m2"><p>سرموئی از راستی نگذریم</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>نداریم بر پردهٔ کج بسیچ</p></div>
<div class="m2"><p>بجز راست بازی ندانیم هیچ</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>در کجروی برجهان بسته‌ایم</p></div>
<div class="m2"><p>ز دنیا بدین راستی رسته‌ایم</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>دروغی نگوئیم در هیچ باب</p></div>
<div class="m2"><p>به شب باژگونه نبینیم خواب</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>نپرسیم چیزی کزو سود نیست</p></div>
<div class="m2"><p>که یزدان از آن کار خشنود نیست</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>پذیریم هرچ آن خدائی بود</p></div>
<div class="m2"><p>خصومت خدای آزمائی بود</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>نکوشیم با کردهٔ کردگار</p></div>
<div class="m2"><p>پرستنده را با خصومت چکار</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>چو عاجز بود یار یاری کنیم</p></div>
<div class="m2"><p>چو سختی رسد بردباری کنیم</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>گر از ما کسی را زیانی رسد</p></div>
<div class="m2"><p>وزان رخنه ما را نشانی رسد</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>بر آریمش از کیسه خویش کام</p></div>
<div class="m2"><p>به سرمایه خود کنیمش تمام</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>ندارد ز ما کس زکس مال بیش</p></div>
<div class="m2"><p>همه راست قسمیم در مال خویش</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>شماریم خود را همه همسران</p></div>
<div class="m2"><p>نخندیم بر گریه دیگران</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>ز دزدان نداریم هرگز هراس</p></div>
<div class="m2"><p>نه در شهر شحنه نه در کوی پاس</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>ز دیگر کسان ما ندزدیم چیز</p></div>
<div class="m2"><p>ز ما دیگران هم ندزدند نیز</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>نداریم در خانها قفل و بند</p></div>
<div class="m2"><p>نگهبان نه با گاو و با گوسفند</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>خدا کرد خردان ما را بزرگ</p></div>
<div class="m2"><p>ستوران ما فارغ از شیر و گرگ</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>اگر گرگ بر میش ما دم زند</p></div>
<div class="m2"><p>هلاکش در آن حال بر هم زند</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>گر از کشت ماکس برد خوشه‌ای</p></div>
<div class="m2"><p>رسد بر دلش تیری از گوشه‌ای</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>بکاریم دانه گه کشت و کار</p></div>
<div class="m2"><p>سپاریم کشته به پروردگار</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>نگردیم بر گرد گاورس و جو</p></div>
<div class="m2"><p>مگر بعد شش مه که باشد درو</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>به ما از آنچه بر جای خود می‌رسد</p></div>
<div class="m2"><p>یکی دانه را هفتصد می‌رسد</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>چنین گریکی کارو گر صد کنیم</p></div>
<div class="m2"><p>توکل بر ایزد نه بر خود کنیم</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>نگهدار ما هست یزدان و بس</p></div>
<div class="m2"><p>به یزدان پناهیم و دیگر به کس</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>سخن چینی از کس نیاموختیم</p></div>
<div class="m2"><p>ز عیب کسان دیده بر دوختیم</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>گر از ما کسی را رسد داوری</p></div>
<div class="m2"><p>کنیمش سوی مصلحت یاوری</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>نباشیم کس را به بد رهنمون</p></div>
<div class="m2"><p>نجوئیم فتنه نریزیم خون</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>به غم‌خواری یکدگر غم خوریم</p></div>
<div class="m2"><p>به شادی همان یار یکدیگریم</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>فریب زر و سیم را در شمار</p></div>
<div class="m2"><p>نباریم و ناید کسی را به کار</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>نداریم خوردی یک از یک دریغ</p></div>
<div class="m2"><p>نخواهیم جو سنگی از کس به تیغ</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>دد و دام را نیست از ما گریز</p></div>
<div class="m2"><p>نه ما را برآزار ایشان ستیز</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>به وقت نیاز آهو و غرم و گور</p></div>
<div class="m2"><p>ز درها در آیند ما را به زور</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>از آن جمله چون در شکار آوریم</p></div>
<div class="m2"><p>به مقدار حاجت بکار آوریم</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>دگرها که باشیم از آن بی‌نیاز</p></div>
<div class="m2"><p>نداریمشان از در و دشت باز</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>نه بسیار خواریم چون گاو و خر</p></div>
<div class="m2"><p>نه لب نیز بر بسته ازخشک و تر</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>خوریم آن‌قدر مایه از گرم و سرد</p></div>
<div class="m2"><p>که چندان دیگر توانیم خورد</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>ز ما در جوانی نمیرد کسی</p></div>
<div class="m2"><p>مگر پیر کو عمر دارد بسی</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>چومیرد کسی دل نداریم تنگ</p></div>
<div class="m2"><p>که درمان آن درد ناید به چنگ</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>پس کس نگوئیم چیزی نهفت</p></div>
<div class="m2"><p>که در پیش رویش نیاریم گفت</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>تجسس نسازیم کاین کس چه کرد</p></div>
<div class="m2"><p>فغان بر نیاوریم کان را که خورد</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>بهرسان که ما را رسد خوب و زشت</p></div>
<div class="m2"><p>سر خود نتابیم از آن سرنوشت</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>بهرچ آفریننده کردست راست</p></div>
<div class="m2"><p>نگوئیم کین چون و آن از کجاست</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>کسی گیرد از خلق با ما قرار</p></div>
<div class="m2"><p>که باشد چو ما پاک و پرهیزگار</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>چو از سیرت ما دگرگون شود</p></div>
<div class="m2"><p>ز پرگار ما زود بیرون شود</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>سکندر چو دید آن چنان رسم و راه</p></div>
<div class="m2"><p>فرو ماند سرگشته بر جایگاه</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>کز آن خوبتر قصه نشنیده بود</p></div>
<div class="m2"><p>نه در نامه خسروان دیده بود</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>به دل گفت ازین رازهای شگفت</p></div>
<div class="m2"><p>اگر زیرکی پند باید گرفت</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>نخواهم دگر در جهان تاختن</p></div>
<div class="m2"><p>به هر صید گه دامی انداختن</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>مرا بس شد از هر چه اندوختم</p></div>
<div class="m2"><p>حسابی کزین مردم آموختم</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>همانا که پیش جهان آزمای</p></div>
<div class="m2"><p>جهان هست ازین نیک‌مردان بجای</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>بدیشان گرفتست عالم شکوه</p></div>
<div class="m2"><p>که اوتاد عالم شدند این گروه</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>اگر سیرت اینست ما برچه‌ایم</p></div>
<div class="m2"><p>وگر مردم اینند پس ما که‌ایم</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>فرستادن ما به دریا و دشت</p></div>
<div class="m2"><p>بدان بود تا باید اینجا گذشت</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>مگر سیرگردم ز خوی ددان</p></div>
<div class="m2"><p>در آموزم آیین این بخردان</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>گر این قوم را پیش ازین دیدمی</p></div>
<div class="m2"><p>به گرد جهان بر نگردیدمی</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>به کنجی در از کوه بنشستمی</p></div>
<div class="m2"><p>به ایزد پرستی میان بستمی</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>ازین رسم نگذشتی آیین من</p></div>
<div class="m2"><p>جز این دین نبودی دگر دین من</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>چو دید آن چنان دین و دین پروری</p></div>
<div class="m2"><p>نکرد از بنه یاد پیغمبری</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>چو در حق خود دیدشان حق شناس</p></div>
<div class="m2"><p>درود و درم دادشان بی‌قیاس</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>از آن مملکت شادمان بازگشت</p></div>
<div class="m2"><p>روان کرد لشگر چو دریا به دشت</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>زرنگین علمهای دیبای روم</p></div>
<div class="m2"><p>وشی پوش گشته همه مرز و بوم</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>بهر کوه و بیشه ز شاخ و ز شخ</p></div>
<div class="m2"><p>پراکنده لشگر چومور و ملخ</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>بهرجا که او تاختی بارگی</p></div>
<div class="m2"><p>رهاندی بسی کس ز بیچارگی</p></div></div>