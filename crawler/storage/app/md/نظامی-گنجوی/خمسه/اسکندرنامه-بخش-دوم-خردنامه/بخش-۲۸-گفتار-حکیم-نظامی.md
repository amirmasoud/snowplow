---
title: >-
    بخش ۲۸ - گفتار حکیم نظامی
---
# بخش ۲۸ - گفتار حکیم نظامی

<div class="b" id="bn1"><div class="m1"><p>نظامی بر این در مجنبان کلید</p></div>
<div class="m2"><p>که نقش ازل بسته را کس ندید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزرگ آفریننده هر چه هست</p></div>
<div class="m2"><p>ز هرچ آفرید است بالا و پست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخستین خرد را پدیدار کرد</p></div>
<div class="m2"><p>ز نور خودش دیده بیدار کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر آن نقش کز کلک قدرت نگاشت</p></div>
<div class="m2"><p>ز چشم خرد هیچ پنهان نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر نقش اول کز آغاز بست</p></div>
<div class="m2"><p>کز آن پرده چشم خرد باز بست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شد بسته نقش نخستین طراز</p></div>
<div class="m2"><p>عصابه ز چشم خرد کرد باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر آن گنج پوشیده کامد پدید</p></div>
<div class="m2"><p>بدست خرد باز دادش کلید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز اول حسابی که سربسته بود</p></div>
<div class="m2"><p>وز آنجا خرد چشم بربسته بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیگر جا که پنهان نبود از خرد</p></div>
<div class="m2"><p>خرد را چو پرسی به دوره برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وز آن جاده کو بر خرد بست راه</p></div>
<div class="m2"><p>حکایت مکن زو حکایت مخواه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به آنجا تواند خرد راه برد</p></div>
<div class="m2"><p>که فرسنگ و منزل تواند شمرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ره غیب ازان دورتر شد بسی</p></div>
<div class="m2"><p>که اندیشه آنجا رساند کسی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خردمندی آنراست کز هر چه هست</p></div>
<div class="m2"><p>چو نادیدنی بود ازو دیده بست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو صنعت به صانع تو را ره نمود</p></div>
<div class="m2"><p>نوائی بر این پرده نتوان فزود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سخن بین که با مرکب نیم لنگ</p></div>
<div class="m2"><p>چگونه برون آمد از راه تنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همانا که آن هاتف خضر نام</p></div>
<div class="m2"><p>که خارا شکافیست خضرا خرام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>درودم رسانید و بعد از درود</p></div>
<div class="m2"><p>به کاخ من آمد ز گنبد فرود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دماغ مرا بر سخن کرد گرم</p></div>
<div class="m2"><p>سخن گفت با من به آواز نرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که چندین سخنهای خلوت سگال</p></div>
<div class="m2"><p>حوالت مکن بر زبانهای لال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو میخاری این سرو را بیخ و بن</p></div>
<div class="m2"><p>بر آن فیلسوفان چه بندی سخن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چرا بست باید سخنهای نغز</p></div>
<div class="m2"><p>بر آن استخوانهای پوسیده مغز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به خوان کسان بر مخور نان خویش</p></div>
<div class="m2"><p>شکینه بنه بر سر خوان خویش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بلی مردم دور نا مردمند</p></div>
<div class="m2"><p>نه بر انجمن فتنه بر انجمند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نه خاکی ولی چون زمین خاک دوست</p></div>
<div class="m2"><p>نه خاک آدمی بلکه خاکی نکوست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مشعبد شد این خاک نیرنگ ساز</p></div>
<div class="m2"><p>که هم مهره دزداست و هم مهره باز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کند مهره‌ای را به کف در نهان</p></div>
<div class="m2"><p>دگر باره آرد برون از دهان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فرو بردنش هست زرنیخ زرد</p></div>
<div class="m2"><p>برآوردنش نیل با لاجورد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به وقت خزان می‌خورد عود خشک</p></div>
<div class="m2"><p>به فصل بهار آورد ناف مشک</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تن آدمی را که خواهد فشرد</p></div>
<div class="m2"><p>ندانم که چون باز خواهد سپرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تن ما که در خاکش آکندگی است</p></div>
<div class="m2"><p>نه در نیستی در پراکندگی است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پراکنده‌ای کو بود جایگیر</p></div>
<div class="m2"><p>گر آید فراهم بود دلپذیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو هرچ آن بود بر زمین ریز ریز</p></div>
<div class="m2"><p>به سیماب جمع آورد خاک بیز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو زر پراکنده را چاره ساز</p></div>
<div class="m2"><p>به سیماب دیگر ره آرد فراز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر اجزای ما را که بودش روان</p></div>
<div class="m2"><p>دگر باره جمعی بود می‌توان</p></div></div>