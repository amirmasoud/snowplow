---
title: >-
    بخش ۱ - به نام ایزد بخشاینده
---
# بخش ۱ - به نام ایزد بخشاینده

<div class="b" id="bn1"><div class="m1"><p>خرد هر کجا گنجی آرد پدید</p></div>
<div class="m2"><p>ز نام خدا سازد آنرا کلید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدای خرد بخش بخرد نواز</p></div>
<div class="m2"><p>همان ناخردمند را چاره ساز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رهائی ده بستگان سخن</p></div>
<div class="m2"><p>توانا کن ناتوانان کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نهان آشکارا درون و برون</p></div>
<div class="m2"><p>خرد را به درگاه او رهنمون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برارندهٔ سقف این بارگاه</p></div>
<div class="m2"><p>نگارنده نقش این کارگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دانستنش عقل را ناگزیر</p></div>
<div class="m2"><p>بزرگی و دانائیش دلپذیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به حکم آشکارا به حکمت نهفت</p></div>
<div class="m2"><p>ستاینده حیران ازو وقت گفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سزای پرستش پرستنده را</p></div>
<div class="m2"><p>تولا بدو مرده و زنده را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ورای همه بوده‌ای بود او</p></div>
<div class="m2"><p>همه رشته‌ای گوهر آمود او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی کز دوئی حضرتش هست پاک</p></div>
<div class="m2"><p>نه از آب و آتش نه از باد و خاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه آفریدست در هفت پوست</p></div>
<div class="m2"><p>بدو آفرین کافریننده اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه بود را هست ازو ناگزیر</p></div>
<div class="m2"><p>به بود کس او نیست نسبت پذیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدو هیچ پوینده را راه نیست</p></div>
<div class="m2"><p>خردمند ازین حکمت آگاه نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرت مذهب این شد که بالا بود</p></div>
<div class="m2"><p>ز تعظیم او زیر تنها بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وگر ذات او زیر گوئی که هست</p></div>
<div class="m2"><p>خدا را نخواند کسی زیردست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو از ذات معبود رانی سخن</p></div>
<div class="m2"><p>به زیر و به بالا دلیری مکن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو در قدرت آید سخن زان دلیر</p></div>
<div class="m2"><p>که بی قدرتش نیست بالا و زیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به هرچ آرد از زیر و بالا پدید</p></div>
<div class="m2"><p>سر از خط فرمان نباید کشید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی را ز گردون دهد بارگاه</p></div>
<div class="m2"><p>یکی را ز کیوان درآرد به چاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دلی را فروزان کند چون چراغ</p></div>
<div class="m2"><p>نهد بر دل دیگر از درد داغ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه بیشیی پیش او اندکیست</p></div>
<div class="m2"><p>بزرگی و خردی به پیشش یکیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چه کوهی بر او چه یک کاه برگ</p></div>
<div class="m2"><p>چه با امر او زندگانی چه مرگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه گوینده خاکی کس آرد بدست</p></div>
<div class="m2"><p>نه بر آب نقشی توان نیز بست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جز او کیست کز خاک آدم سرشت</p></div>
<div class="m2"><p>بر آب این چنین نقش داند نوشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو ره یاوه گردد نماینده اوست</p></div>
<div class="m2"><p>چو در بسته باشد گشاینده اوست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تواناست بر هر چه او ممکنست</p></div>
<div class="m2"><p>گر آن چیز جنبنده یا ساکنست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تنومند ازو جمله کاینات</p></div>
<div class="m2"><p>بدو زنده هر کس که دارد حیات</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همه بودی از بود او هست نام</p></div>
<div class="m2"><p>تمام اوست دیگر همه ناتمام</p></div></div>