---
title: >-
    بخش ۷۷ - پاسخ دادن شیرین خسرو را
---
# بخش ۷۷ - پاسخ دادن شیرین خسرو را

<div class="b" id="bn1"><div class="m1"><p>اجازت داد شیرین باز لب را</p></div>
<div class="m2"><p>که در گفت آورد شیرین رطب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقیق از تارک لؤلؤ برانگیخت</p></div>
<div class="m2"><p>گهر می‌بست و مروارید می‌ریخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخستین گفت کای شاه جوانبخت</p></div>
<div class="m2"><p>به تو آراسته هم تاج و هم تخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نیروی تو بر بدخواه پیوست</p></div>
<div class="m2"><p>علم را پای باد و تیغ را دست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بالای تو دولت را قبا چست</p></div>
<div class="m2"><p>به بازوی تو گردون را کمان سست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز یارت بخت باد از بخت یاری</p></div>
<div class="m2"><p>که پشتیوان پشت روزگاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس آنگه تند شد چون کوه آتش</p></div>
<div class="m2"><p>به خسرو گفت کی سالار سرکش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو شاهی رو که شه را عشقبازی</p></div>
<div class="m2"><p>تکلف کردنی باشد مجازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نباشد عاشقی جز کار آنکس</p></div>
<div class="m2"><p>که معشوقیش باشد در جهان بس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مزن طعنه مرا در عشق فرهاد</p></div>
<div class="m2"><p>به نیکی کن غریبی مرده را یاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا فرهاد با آن مهربانی</p></div>
<div class="m2"><p>برادر خوانده‌ای بود آن جهانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه یکساعت به من در تیز دیده</p></div>
<div class="m2"><p>نه از شیرین جز آوازی شنیده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدان تلخی که شیرین کرد روزش</p></div>
<div class="m2"><p>چو عود تلخ شیرین بود سوزش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از او دیدم هزار آزرم دلسوز</p></div>
<div class="m2"><p>که نشنیدم پیامی از تو یکروز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرا خاری که گل باشد بر آن خار</p></div>
<div class="m2"><p>به از سروی که هرگز ناورد بار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز آهن زیر سر کردن ستونم</p></div>
<div class="m2"><p>به از زرین کمر بستن به خونم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مسی کز وی مرا دستینه سازند</p></div>
<div class="m2"><p>به از سیمی که در دستم گدازند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چراغی کو شبم را برفروزد</p></div>
<div class="m2"><p>به از شمعی که رختم را بسوزد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بود عاشق چو دریا سنگ در بر</p></div>
<div class="m2"><p>منم چون کوه دایم سنگ بر سر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به زندان مانده چون آهن درین سنگ</p></div>
<div class="m2"><p>دل از شادی و دست از دوستان تنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مبادا تنگدل را تنگ دستی</p></div>
<div class="m2"><p>که با دیوانگی صعب است مستی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو مستی دارم و دیوانگی هست</p></div>
<div class="m2"><p>حریفی ناید از دیوانه مست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قلم در کش به حرف دست سایم</p></div>
<div class="m2"><p>که دست حرف گیران را نشایم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همان انگار کامد تند بادی</p></div>
<div class="m2"><p>ز باغت برد برگی بامدادی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرا سیلاب محنت در بدر کرد</p></div>
<div class="m2"><p>تو رخت خویشتن برگیر و برگرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من اینک مانده‌ام در آتش تیز</p></div>
<div class="m2"><p>تو در من بین و عبرت گیر و بگریز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هوا کافور بیزی می نماید</p></div>
<div class="m2"><p>هوای ما اگر سرد است شاید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو ابر از شور بختی شد نمک بار</p></div>
<div class="m2"><p>دل از شیرین شورانگیز بردار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هوا داری مکن شب را چو خفاش</p></div>
<div class="m2"><p>چو باز جره خور روز روباش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شد آن افسانه‌ها کز من شنیدی</p></div>
<div class="m2"><p>گذشت آن مهربانیها که دیدی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شعیری زان شعار نو نماند است</p></div>
<div class="m2"><p>و گر تازی ندانی جو نماند است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه آن ترکم که من تازی ندانم</p></div>
<div class="m2"><p>شکن کاری و طنازی ندانم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فلک را طنزگه کوی من آمد</p></div>
<div class="m2"><p>شکن خود کار گیسوی من آمد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دلت گر مرغ باشد پر نگیرد</p></div>
<div class="m2"><p>دمت گر صبح باشد در نگیرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر صد خواب یوسف داری از بر</p></div>
<div class="m2"><p>همانی و همان عیسی و بس خر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر آنگه می‌زدی یک حربه چون میغ</p></div>
<div class="m2"><p>چو صبح اکنون دو دستی میزنی تیغ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدی دیلم کیائی برگزیدی</p></div>
<div class="m2"><p>تبر بفروختی زوبین خریدی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برو کز هیچ روئی در نگنجی</p></div>
<div class="m2"><p>اگر موئی که موئی در نگنجی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به زور و زرق کسب اندوزی خویش</p></div>
<div class="m2"><p>نشاید خورد بیش از روزی خویش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گره بر سینه زن بی رنج مخروش</p></div>
<div class="m2"><p>ادب کن عشوه را یعنی که خاموش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>حلالی خور چو بازان شکاری</p></div>
<div class="m2"><p>مکن چون کرکسان مردار خواری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مرا شیرین بدان خوانند پیوست</p></div>
<div class="m2"><p>که بازیهای شیرین آرم از دست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یکی را تلخ‌تر گریانم از جام</p></div>
<div class="m2"><p>یکی را عیش خوشتر دارم از نام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گلابم گر کنم تلخی چه باکست</p></div>
<div class="m2"><p>گلاب آن به که او خود تلخ ناکست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نبیذی قاتلم بگذارم از دست</p></div>
<div class="m2"><p>که از بویم بمانی سالها مست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو نام من به شیرینی بر آید</p></div>
<div class="m2"><p>اگر گفتار من تلخ است شاید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دو شیرینی کجا باشد بهم نغز</p></div>
<div class="m2"><p>رطب با استخوان به جوز با مغز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>درشتی کردنم نزخار پشتی است</p></div>
<div class="m2"><p>بسا نرمی که در زیر درشتی است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گهر در سنگ و خرما هست در خار</p></div>
<div class="m2"><p>وز اینسان در خرابی گنج بسیار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تحمل را بخود کن رهنمونی</p></div>
<div class="m2"><p>نه چندانی که بار آرد زبونی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زبونی کان ز حد بیرون توان کرد</p></div>
<div class="m2"><p>جهودی شد جهودی چون توان کرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو خرگوش افکند در بردباری</p></div>
<div class="m2"><p>کند هر کودکی بروی سواری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو شاهین باز ماند از پریدن</p></div>
<div class="m2"><p>ز گنجشکش لگد باید چشیدن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شتر کز هم جدا گردد قطارش</p></div>
<div class="m2"><p>ز خاموشی کشد موشی مهارش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کسی کو جنگ شیران آزماید</p></div>
<div class="m2"><p>چو شیر آن به که دندانی نماید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سگان وقتی که وحشت ساز گردند</p></div>
<div class="m2"><p>ز یکدیگر به دندان باز گردند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>پس آنگه بر زبان آورد سوگند</p></div>
<div class="m2"><p>به هوش زیرک و جان خردمند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به قدر گنبد پیروزه گلشن</p></div>
<div class="m2"><p>به نور چشمه خورشید روشن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به هر نقشی که در فردوس پاکست</p></div>
<div class="m2"><p>به هر حرفی که در منشور خاکست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بدان زنده گه او هرگز نمیرد</p></div>
<div class="m2"><p>به بیداری که خواب او را نگیرد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به دارائی که تن‌ها را خورش داد</p></div>
<div class="m2"><p>به معبودی که جان را پرورش داد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>که بی کاوین اگر چه پادشاهی</p></div>
<div class="m2"><p>ز من برنایدت کامی که خواهی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بدین تندی ز خسرو روی برتافت</p></div>
<div class="m2"><p>ز دست افکند گنجی را که دریافت</p></div></div>