---
title: >-
    بخش ۱۱۳ - جان دادن شیرین در دخمه خسرو
---
# بخش ۱۱۳ - جان دادن شیرین در دخمه خسرو

<div class="b" id="bn1"><div class="m1"><p>چو صبح از خواب نوشین سر برآورد</p></div>
<div class="m2"><p>هلاک جان شیرین بر سر آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیاهی از حبش کافور می‌برد</p></div>
<div class="m2"><p>شد اندر نیمه ره کافوردان خرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز قلعه زنگیی در ماه می‌دید</p></div>
<div class="m2"><p>چو مه در قلعه شد زنگی بخندید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بفرمودش به رسم شهریاری</p></div>
<div class="m2"><p>کیانی مهدی از عود قماری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفته مهد را در تخته زر</p></div>
<div class="m2"><p>بر آموده به مروارید و گوهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آئین ملوک پارسی عهد</p></div>
<div class="m2"><p>بخوابانید خسرو را در آن مهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهاد آن مهد را بر دوش شاهان</p></div>
<div class="m2"><p>به مشهد برد وقت صبح گاهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهانداران شده یکسر پیاده</p></div>
<div class="m2"><p>بگرداگرد آن مهد ایستاده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قلم ز انگشت رفته باربد را</p></div>
<div class="m2"><p>بریده چون قلم انگشت خود را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بزرگ امید خرد امید گشته</p></div>
<div class="m2"><p>بلرزانی چو برگ بید گشته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به آواز ضغیف افغان برآورد</p></div>
<div class="m2"><p>که ما را مرگ شاه از جان برآورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پناه و پشت شاهان عجم کو</p></div>
<div class="m2"><p>سپهسالار و شمشیر و علم کو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کجا کان خسرو دنییش خوانند</p></div>
<div class="m2"><p>گهی پرویز و گه کسریش خوانند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو در راه رحیل آمد روارو</p></div>
<div class="m2"><p>چه جمشید و چه کسری و چه خسرو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گشاده سر کنیزان و غلامان</p></div>
<div class="m2"><p>چو سروی در میان شیرین خرامان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نهاده گوهرآگین حلقه در گوش</p></div>
<div class="m2"><p>فکنده حلقه‌های زلف بر دوش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کشیده سرمه‌ها در نرگس مست</p></div>
<div class="m2"><p>عروسانه نگار افکنده بر دست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پرندی زرد چون خورشید بر سر</p></div>
<div class="m2"><p>حریری سرخ چون ناهید در بر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پس مهد ملک سرمست میشد</p></div>
<div class="m2"><p>کسی کان فتنه دید از دست میشد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گشاده پای در میدان عهدش</p></div>
<div class="m2"><p>گرفته رقص در پایان مهدش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گمان افتاد هر کس را که شیرین</p></div>
<div class="m2"><p>ز بهر مرگ خسرو نیست غمگین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همان شیرویه را نیز این گمان بود</p></div>
<div class="m2"><p>که شیرین را بر او دل مهربان بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همه ره پای کوبان میشد آن ماه</p></div>
<div class="m2"><p>بدینسان تا به گنبد خانه شاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پس او در غلامان و کنیزان</p></div>
<div class="m2"><p>ز نرگس بر سمن سیماب ریزان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو مهد شاه در گنبد نهادند</p></div>
<div class="m2"><p>بزرگان روی در روی ایستادند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>میان دربست شیرین پیش موبد</p></div>
<div class="m2"><p>به فراشی درون آمد به گنبد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در گنبد به روی خلق در بست</p></div>
<div class="m2"><p>سوی مهد ملک شد دشنه در دست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جگرگاه ملک را مهر برداشت</p></div>
<div class="m2"><p>ببوسید آن دهن کاو بر جگر داشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدان آیین که دید آن زخم را ریش</p></div>
<div class="m2"><p>همانجا دشنه‌ای زد بر تن خویش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به خون گرم شست آن خوابگه را</p></div>
<div class="m2"><p>جراحت تازه کرد اندام شه را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پس آورد آنگهی شه را در آغوش</p></div>
<div class="m2"><p>لبش بر لب نهاد و دوش بر دوش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به نیروی بلند آواز برداشت</p></div>
<div class="m2"><p>چنان کان قوم از آوازش خبر داشت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که جان با جان و تن و با تن به پیوست</p></div>
<div class="m2"><p>تن از دوری و جان از داوری رست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به بزم خسرو آن شمع جهانتاب</p></div>
<div class="m2"><p>مبارک باد شیرین را شکر خواب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به آمرزش رساد آن آشنائی</p></div>
<div class="m2"><p>که چون اینجا رسد گوید دعائی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کالهی تازه دار این خاکدان را</p></div>
<div class="m2"><p>بیامرز این دو یار مهربان را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زهی شیرین و شیرین مردن او</p></div>
<div class="m2"><p>زهی جان دادن و جان بردن او</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چنین واجب کند در عشق مردن</p></div>
<div class="m2"><p>به جانان جان چنین باید سپردن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نه هر کو زن بود نامرد باشد</p></div>
<div class="m2"><p>زن آن مرد است کو بی‌درد باشد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بسا رعنا زنا کو شیر مرد است</p></div>
<div class="m2"><p>بسا دیبا که شیرش در نورد است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>غباری بر دمید از راه بیداد</p></div>
<div class="m2"><p>شبیخون کرد بر نسرین و شمشاد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بر آمد ابری از دریای اندوه</p></div>
<div class="m2"><p>فرو بارید سیلی کوه تا کوه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز روی دشت بادی تند برخاست</p></div>
<div class="m2"><p>هوا را کرد با خاک زمین راست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بزرگان چون شدند آگه ازین راز</p></div>
<div class="m2"><p>برآوردند حالی یکسر آواز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>که احسنت ای زمان وای زمین زه</p></div>
<div class="m2"><p>عروسان را به دامادان چنین ده</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو باشد مطرب زنگی و روسی</p></div>
<div class="m2"><p>نشاید کرد ازین بهتر عروسی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دو صاحب تاج را هم تخت کردند</p></div>
<div class="m2"><p>در گنبد بر ایشان سخت کردند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>وز آنجا باز پس گشتند غمناک</p></div>
<div class="m2"><p>نوشتند این مثل بر لوح آن خاک</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>که جز شیرین که در خاک درشتست</p></div>
<div class="m2"><p>کسی از بهر کس خود را نکشت است</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>منه دل بر جهان کین سرد ناکس</p></div>
<div class="m2"><p>وفا داری نخواهد کرد با کس</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چه بخشد مرد را این سفله ایام</p></div>
<div class="m2"><p>که یک یک باز نستاند سرانجام</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به صد نوبت دهد جانی به آغاز</p></div>
<div class="m2"><p>به یک نوبت ستاند عاقبت باز</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو بر پائی طلسمی پیچ پیچی</p></div>
<div class="m2"><p>چو افتادی شکستی هیچ هیچی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>درین چنبر که محکم شهر بندیست</p></div>
<div class="m2"><p>نشان ده گردنی کو بی کمندیست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نه با چنبر توان پرواز کردن</p></div>
<div class="m2"><p>نه بتوان بند چنبر باز کردن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>درین چنبر گشایش چون نمائیم</p></div>
<div class="m2"><p>چو نگشادست کس ما چون گشائیم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همان به کاندرین خاک خطرناک</p></div>
<div class="m2"><p>ز جور خاک بنشینیم بر خاک</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بگرییم از برای خویش یکبار</p></div>
<div class="m2"><p>که بر ما کم کسی گرید چو ما زار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شنیدستم که افلاطون شب و روز</p></div>
<div class="m2"><p>به گریه داشتی چشم جهانسوز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بپرسیدند ازو کاین گریه از چیست</p></div>
<div class="m2"><p>بگفتا چشم کس بیهوده نگریست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>از آن گریم که جسم و جان دمساز</p></div>
<div class="m2"><p>بهم خو کرده‌اند از دیرگه باز</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>جدا خواهند گشت از آشنائی</p></div>
<div class="m2"><p>همی گریم بدان روز جدائی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>رهی خواهی شدن کان ره درازست</p></div>
<div class="m2"><p>به بی‌برگی مشو بی‌برگ و سازست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بپای جان توانی شد بر افلاک</p></div>
<div class="m2"><p>رها کن شهر بند خاک بر خاک</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>مگو بر بام گردون چون توان رفت</p></div>
<div class="m2"><p>توان رفت ارز خود بیرون توان رفت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بپرس از عقل دوراندیش گستاخ</p></div>
<div class="m2"><p>که چون شاید شدن بر بام این کاخ</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چنان کز عقل فتوی میستانی</p></div>
<div class="m2"><p>علم برکش بر این کاخ کیانی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>خرد شیخ الشیوخ رای تو بس</p></div>
<div class="m2"><p>ازو پرس آنچه می‌پرسی نه از کس</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>سخن کز قول آن پیر کهن نیست</p></div>
<div class="m2"><p>بر پیران وبال است آن سخن نیست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>خرد پای و طبیعت بند پایست</p></div>
<div class="m2"><p>نفس یک یک چو سوهان بند سایست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بدین زرین حصار آن شد برومند</p></div>
<div class="m2"><p>که از خود برگرفت این آهنین بند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چو این خصمان که از یارت برارند</p></div>
<div class="m2"><p>بر آن کارند کز کارت برآرند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ازین خرمن مخور یک دانه گاورس</p></div>
<div class="m2"><p>برو میلرز و بر خود نیز میترس</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چو عیسی خر برون برزین تنی چند</p></div>
<div class="m2"><p>بمان در پای گاوان خرمنی چند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ازین نه گاوپشت آدمیخوار</p></div>
<div class="m2"><p>بنه بر پشت گاوافکن زمین‌وار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>اگر زهره شوی چون بازکاوی</p></div>
<div class="m2"><p>درین خر پشته هم بر پشت گاوی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بسا تشنه که بر پندار بهبود</p></div>
<div class="m2"><p>فریب شوره‌ای کردش نمک سود</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بسا حاجی که خود را از اشتر انداخت</p></div>
<div class="m2"><p>که تلخک را ز ترشک باز نشناخت</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>حصار چرخ چون زندان سرائیست</p></div>
<div class="m2"><p>کمر در بسته گردش اژدهائیست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چگونه تلخ نبود عیش آن مرد</p></div>
<div class="m2"><p>که دم با اژدهائی بایدش کرد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چو بهمن زین شبستان رخت بر بند</p></div>
<div class="m2"><p>حریفی کردنت با اژدها چند</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>گرت خود نیست سودی زین جدائی</p></div>
<div class="m2"><p>نه آخر ز اژدها یابی رهائی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چه داری دوست آنکش وقت مردن</p></div>
<div class="m2"><p>به دشمن تر کسی باید سپردن</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به حرمت شو کزین دیر مسیلی</p></div>
<div class="m2"><p>شود عیسی به حرمت خر به سیلی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>سلامت بایدت کس را میازار</p></div>
<div class="m2"><p>که بد را در عوض تیز است بازار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>از آن جنبش که در نشونبات است</p></div>
<div class="m2"><p>درختان را و مرغان را حیات است</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>درخت افکن بود کم زندگانی</p></div>
<div class="m2"><p>به درویشی کشد نخجیر بانی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>علم بفکن که عالم تنگ نایست</p></div>
<div class="m2"><p>عنان درکش که مرکب لنگ پایست</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>نفس بردار ازین نای گلوتنگ</p></div>
<div class="m2"><p>گره بگشای ازین پای کهن لنگ</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>به ملکی در چه باید ساختن جای</p></div>
<div class="m2"><p>که غل بر گردنست و بند بر پای</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>ازین هستی که یابد نیستی زود</p></div>
<div class="m2"><p>بباید شد بهست و نیست خشنود</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ز مال و ملک و فرزند و زن و زور</p></div>
<div class="m2"><p>همه هستند همراه تو تا گور</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>روند این همرهان غمناک با تو</p></div>
<div class="m2"><p>نیاید هیچ کس در خاک با تو</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>رفیقانت همه بدساز گردند</p></div>
<div class="m2"><p>ز تو هر یک به راهی باز گردند</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>به مرگ و زندگی در خواب و مستی</p></div>
<div class="m2"><p>توئی با خویشتن هر جا که هستی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>ازین مشتی خیال کاروان زن</p></div>
<div class="m2"><p>عنان بستان علم بر آسمان زن</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>خلاف آن شد که در هر کارگاهی</p></div>
<div class="m2"><p>مخالف دید خواهی بارگاهی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>نفس کو بر سپهر آهنگ دارد</p></div>
<div class="m2"><p>ز لب تا ناف میدان تنگ دارد</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>بده گر عاقلی پرواز خود را</p></div>
<div class="m2"><p>که کشتند از تو به صد بار صد را</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>زمین کز خون ما باکی ندارد</p></div>
<div class="m2"><p>به بادش ده که جز خاکی ندارد</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>دلا منشین که یاران برنشستند</p></div>
<div class="m2"><p>بنه بر بند کایشان رخت بستند</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>درین کشتی چو نتوان دیر ماندن</p></div>
<div class="m2"><p>بباید رخت بر دریا فشاندن</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>درین دریا سر از غم بر میاور</p></div>
<div class="m2"><p>فرو خور غوطه و دم بر میاور</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بدین خوبی جمالی کادمی راست</p></div>
<div class="m2"><p>اگر بر آسمان باشد ز می‌راست</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>بفرساید زمین و بشکند سنگ</p></div>
<div class="m2"><p>نماند کس درین پیغوله تنگ</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>پی غولان درین پیغوله بگذار</p></div>
<div class="m2"><p>فرشته شو قدم زین فرش بردار</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>جوانمردان که در دل جنگ بستند</p></div>
<div class="m2"><p>به جان و دل ز جان آهنگ رستند</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>ز جان کندن کسی جان برد خواهد</p></div>
<div class="m2"><p>که پیش از دادن جان مرد خواهد</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>نمانی گر بماند خو بگیری</p></div>
<div class="m2"><p>بمیران خویشتن را تا نمیری</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>بسا پیکر که گفتی آهنین است</p></div>
<div class="m2"><p>به صد زاری کنون زیرزمین است</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>گر اندام زمین را باز جوئی</p></div>
<div class="m2"><p>همه خاک زمین بودند گوئی</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>کجا جمشید و افریدون و ضحاک</p></div>
<div class="m2"><p>همه در خاک رفتند ای خوشا خاک</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>جگرها بین که در خوناب خاک است</p></div>
<div class="m2"><p>ندانم کاین چه دریای هلاک است</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>که دیدی کامد اینجا کوس پیلش</p></div>
<div class="m2"><p>که برنامد ز پی بانگ رحیلش</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>اگر در خاک شد خاکی ستم نیست</p></div>
<div class="m2"><p>سرانجام وجود الا عدم نیست</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>جهان بین تا چه آسان می‌کند مست</p></div>
<div class="m2"><p>فلک بین تا چه خرم می‌زند دست</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>نظامی بس کن این گفتار خاموش</p></div>
<div class="m2"><p>چه گوئی با جهانی پنبه در گوش</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>شکایتهای عالم چند گوئی</p></div>
<div class="m2"><p>بپوش این گریه را در خنده‌روئی</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چه پیش آرد زمان کان در نگردد</p></div>
<div class="m2"><p>چه افرازد زمین کان برنگردد</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>درختی را که بینی تازه بیخش</p></div>
<div class="m2"><p>کند روزی ز خشکی چار میخش</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>بهاری را کند گیتی فروزی</p></div>
<div class="m2"><p>به بادش بر دهد ناگاه روزی</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>دهد بستاند و عاری ندارد</p></div>
<div class="m2"><p>بجز داد و ستد کاری ندارد</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>جنایتهای این نه شیشه تنگ</p></div>
<div class="m2"><p>همه در شیشه کن بر شیشه زن سنگ</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>مگر در پای دور گرم کینه</p></div>
<div class="m2"><p>شکسته گردد این سبز آبگینه</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>بده دنیی مکن کز بهر هیچت</p></div>
<div class="m2"><p>دهد این چرخ پیچاپیچ پیچت</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>ز خود بگذر که با این چار پیوند</p></div>
<div class="m2"><p>نشاید رست ازین هفت آهنین بند</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>گل و سنگ است این ویرانه منزل</p></div>
<div class="m2"><p>درو ما را دو دست و پای در گل</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>درین سنگ و درین گل مرد فرهنگ</p></div>
<div class="m2"><p>نه گل بر گل نهد نه سنگ بر سنگ</p></div></div>