---
title: >-
    بخش ۵۶ - رای زدن خسرو در کار فرهاد
---
# بخش ۵۶ - رای زدن خسرو در کار فرهاد

<div class="b" id="bn1"><div class="m1"><p>ز نزدیکان خود با محرمی چند</p></div>
<div class="m2"><p>نشست و زد درین معنی دمی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که با این مرد سودائی چه سازیم</p></div>
<div class="m2"><p>بدین مهره چگونه حقه بازیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرش مانم بدو کارم تباه است</p></div>
<div class="m2"><p>و گر خونش بریزم بی‌گناه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسی کوشیدم اندر پادشائی</p></div>
<div class="m2"><p>مگر عیدی کنم بی‌روستائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کند بر من کنون عید آن مه نو</p></div>
<div class="m2"><p>که کرد آشفته‌ای را یار خسرو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خردمندان چنین دادند پاسخ</p></div>
<div class="m2"><p>که ای دولت به دیدار تو فرخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمین مولای تو صاحب‌کلاهان</p></div>
<div class="m2"><p>به خاک پای تو سوگند شاهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان اندازه عمر درازت</p></div>
<div class="m2"><p>سعادت یار و دولت کارسازت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر این آشفته را تدبیر سازیم</p></div>
<div class="m2"><p>نه ز آهن کز زرش زنجیر سازیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که سودا را مفرح زر بود زر</p></div>
<div class="m2"><p>مفرح خود به زر گردد میسر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نخستش خواند باید با صد امید</p></div>
<div class="m2"><p>زرافشانی بر او کردن چو خورشید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به زر نز دلستان کز دین برآید</p></div>
<div class="m2"><p>بدین شیرینی از شیرین بر آید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسا بینا که از زر کور گردد</p></div>
<div class="m2"><p>بس آهن کو به زر بی‌زور گردد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرش نتوان به زر معزول کردن</p></div>
<div class="m2"><p>به سنگی بایدش مشغول کردن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که تا آن روز کآید روز او تنگ</p></div>
<div class="m2"><p>گذارد عمر در پیکار آن سنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو شه بشنید قول انجمن را</p></div>
<div class="m2"><p>طلب فرمود کردن کوهکن را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در آوردندش از در چون یکی کوه</p></div>
<div class="m2"><p>فتاده از پسش خلقی به انبوه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نشان محنت اندر سر گرفته</p></div>
<div class="m2"><p>رهی بی‌خویش اندر بر گرفته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز رویش گشته پیدا بی‌قراری</p></div>
<div class="m2"><p>بر او بگریسته دوران به زاری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه در خسرو نگه کرد و نه در تخت</p></div>
<div class="m2"><p>چو شیران پنجه کرد اندر زمین سخت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>غم شیرین چنان از خود ربودش</p></div>
<div class="m2"><p>که پروای خود و خسرو نبودش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ملک فرمود تا بنواختندش</p></div>
<div class="m2"><p>بهر گامی نثاری ساختندش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز پای آن پیل‌بالا را نشاندند</p></div>
<div class="m2"><p>به پایش پیل‌بالا زر فشاندند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو گوهر در دل پاکش یکی بود</p></div>
<div class="m2"><p>ز گوهرها زر و خاکش یکی بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو مهمان را نیامد چشم بر زر</p></div>
<div class="m2"><p>ز لب بگشاد خسرو درج گوهر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به هر نکته که خسرو ساز می‌داد</p></div>
<div class="m2"><p>جوابش هم به نکته باز می‌داد</p></div></div>