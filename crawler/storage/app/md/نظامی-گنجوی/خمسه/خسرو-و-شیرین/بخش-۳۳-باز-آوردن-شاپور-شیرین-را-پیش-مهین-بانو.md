---
title: >-
    بخش ۳۳ - باز آوردن شاپور شیرین را پیش مهین بانو
---
# بخش ۳۳ - باز آوردن شاپور شیرین را پیش مهین بانو

<div class="b" id="bn1"><div class="m1"><p>چو شیرین را ز قصر آورد شاپور</p></div>
<div class="m2"><p>ملک را یافت از میعادگه دور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرود آوردش از گلگون رهوار</p></div>
<div class="m2"><p>به گلزار مهین بانو دگر بار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چمن را سرو داد و روضه را حور</p></div>
<div class="m2"><p>فلک را آفتاب و دیده را نور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرستاران و نزدیکان و خویشان</p></div>
<div class="m2"><p>که بودند از پی شیرین پریشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو دیدندش زمین را بوسه دادند</p></div>
<div class="m2"><p>زمین گشتند و در پایش فتادند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسی شکر و بسی شکرانه کردند</p></div>
<div class="m2"><p>جهانی وقف آتش‌خانه کردند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهین بانو نشاید گفت چون بود</p></div>
<div class="m2"><p>که از شادی ز شادروان برون بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو پیری کو جوانی باز یابد</p></div>
<div class="m2"><p>بمیرد زندگانی باز یابد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرش در بر گرفت از مهربانی</p></div>
<div class="m2"><p>جهان از سر گرفتش زندگانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه چندان دلخوشی و مهر دادش</p></div>
<div class="m2"><p>که در صد بیت بتوان کرد یادش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز گنج خسروی و ملک شاهی</p></div>
<div class="m2"><p>فدا کردش که می‌کن هرچه خواهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شکنج شرم در مویش نیاورد</p></div>
<div class="m2"><p>حدیث رفته بر رویش نیاورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو می‌دانست کان نیرنگ‌سازی</p></div>
<div class="m2"><p>دلیلی روشن است از عشق‌بازی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دگر کز شه نشانها بود دیده</p></div>
<div class="m2"><p>وزان سیمین‌بران لختی شنیده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سر خم بر می جوشیده می‌داشت</p></div>
<div class="m2"><p>به گل خورشید را پوشیده می‌داشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دلش می‌داد تا فرمان پذیرد</p></div>
<div class="m2"><p>قوی‌دل گردد و درمان پذیرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نوازش‌های بی‌اندازه کردش</p></div>
<div class="m2"><p>همان عهد نخستین تازه کردش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همان هفتاد لعبت را بدو داد</p></div>
<div class="m2"><p>که تا بازی کند با لعبتان شاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دگر ره چرخ لعبت‌باز دستی</p></div>
<div class="m2"><p>به بازی برد با لعبت‌پرستی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو شیرین باز دید آن دختران را</p></div>
<div class="m2"><p>ز مه پیرایه داد آن اختران را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همان لهو و نشاط اندیشه کردند</p></div>
<div class="m2"><p>همان بازار پیشین پیشه کردند</p></div></div>