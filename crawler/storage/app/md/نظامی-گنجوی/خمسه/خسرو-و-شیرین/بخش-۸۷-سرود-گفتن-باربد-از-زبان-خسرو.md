---
title: >-
    بخش ۸۷ - سرود گفتن باربد از زبان خسرو
---
# بخش ۸۷ - سرود گفتن باربد از زبان خسرو

<div class="b" id="bn1"><div class="m1"><p>نکیسا چون ز شاه آتش برانگیخت</p></div>
<div class="m2"><p>ستای باربد آبی بر او ریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به استادی نوائی کرد بر کار</p></div>
<div class="m2"><p>کز او چنگ نیکسا شد نگونسار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ترکیب ملک برد آن خلل را</p></div>
<div class="m2"><p>به زیرافکن فرو گفت این غزل را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببخاشی ای صنم بر عذرخواهی</p></div>
<div class="m2"><p>که صد عذر آورد در هر گناهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر از حکم تو روزی سر کشیدم</p></div>
<div class="m2"><p>بسی زهر پشیمانی چشیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرفتم هر چه من کردم گناهست</p></div>
<div class="m2"><p>نه آخر آب چشمم عذر خواهست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پشیمانم زهر بادی که خوردم</p></div>
<div class="m2"><p>گرفتارم بهر غدری که کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قلم در حرف کش بی آبیم را</p></div>
<div class="m2"><p>شفیع آرم بتو بی خوابیم را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازین پس سر ز پایت برندارم</p></div>
<div class="m2"><p>سر از خاک سرایت بر ندارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنم در خانه یک چشم جایت</p></div>
<div class="m2"><p>به دیگر چشم بوسم خاک پایت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سگم وز سگ بتر پنهان نگویم</p></div>
<div class="m2"><p>گرت جان از میان جان نگویم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نصیب من ز تو در جمله هستی</p></div>
<div class="m2"><p>سلامی بود و آن در نیز بستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر محروم شد گوش از سلامت</p></div>
<div class="m2"><p>زبان را تازه می‌دارم به نامت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در این تب گرچه بر نارم فغانی</p></div>
<div class="m2"><p>گرم پرسی ندارد هم زیانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز تو پرسش مرا امید خامست</p></div>
<div class="m2"><p>اگر بر خاطرت گردم تمامست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نداری دل که آیی برکنارم</p></div>
<div class="m2"><p>و گر داری من آن طالع ندارم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نمائی کز غمت غمناکم ای جان</p></div>
<div class="m2"><p>نگوئی من کدامین خاکم ای جان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر تو راضیی کاین دل خرابست</p></div>
<div class="m2"><p>رضای دوستان جستن صوابست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو بر من تا توانی ناز میساز</p></div>
<div class="m2"><p>که تا جانم بر آید می‌کشم ناز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>منم عاشق مرا غم سازگار است</p></div>
<div class="m2"><p>تو معشوقی ترا با غم چکار است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو گر سازی وگرنه من برانم</p></div>
<div class="m2"><p>که سوزم در غمت تا می‌توانم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا گر نیست دیدار تو روزی</p></div>
<div class="m2"><p>تو باقی باش در عالم فروزی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر من جان دهم در مهربانی</p></div>
<div class="m2"><p>ترا باید که باشد زندگانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر من برنخوردم از نکوئی</p></div>
<div class="m2"><p>تو برخوردار باش از خوبروئی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو دایم مان که صحبت جاودان نیست</p></div>
<div class="m2"><p>من ارمانم وگرنه باک از آن نیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز تو بی‌روزیم خوانند و گویم</p></div>
<div class="m2"><p>مرا آن به که من بهروز اویم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرا گر روز و روزی رفت بر باد</p></div>
<div class="m2"><p>ترا هر روز روز از روز به باد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو بر زد باربد بر خشک رودی</p></div>
<div class="m2"><p>بدین‌تری که بر گفتم سرودی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دل شیرین بدان گرمی برافروخت</p></div>
<div class="m2"><p>که چون روغن چراغ عقل را سوخت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنان فریاد کرد آن سرو آزاد</p></div>
<div class="m2"><p>کزان فریاد شاه آمد به فریاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شهنشه چون شنید آواز شیرین</p></div>
<div class="m2"><p>رسیلی کرد و شد دمساز شیرین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در آن پرده که شیرین ساختی ساز</p></div>
<div class="m2"><p>هم آهنگیش کردی شه به آواز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو شخصی کو بکوهی راز گوید</p></div>
<div class="m2"><p>بدو کوه آن سخن را باز گوید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ازین سو مه ترانه بر کشیده</p></div>
<div class="m2"><p>وزان سو شاه پیراهن دریده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو از سوز دو عاشق آه برخاست</p></div>
<div class="m2"><p>صداع مطربان از راه برخاست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ملک فرمود تا شاپور حالی</p></div>
<div class="m2"><p>ز جز خسرو سرا را کرد خالی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بر آن آواز خرگاهی پر از جوش</p></div>
<div class="m2"><p>سوی خرگاه شد بی‌صبر و بیهوش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در آمد در زمان شاپور هشیار</p></div>
<div class="m2"><p>گرفتش دست و گفتا جانگه‌دار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر چه کار خسرو می‌شد از دست</p></div>
<div class="m2"><p>چو خود را دستگیری دید بنشست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پس آنگه گفت کین آواز دلسوز</p></div>
<div class="m2"><p>چه آواز است رازش در من آموز</p></div></div>