---
title: >-
    بخش ۱۱۲ - کشتن شیرویه خسرو را
---
# بخش ۱۱۲ - کشتن شیرویه خسرو را

<div class="b" id="bn1"><div class="m1"><p>شبی تاریک نور از ماه برده</p></div>
<div class="m2"><p>فلک را غول وار از راه برده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمانه با هزاران دست بی‌زور</p></div>
<div class="m2"><p>فلک با صد هزاران دیده شبکور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهنشه پای را با بند زرین</p></div>
<div class="m2"><p>نهاده بر دو سیمین ساق شیرین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بت زنجر موی از سیمگون دست</p></div>
<div class="m2"><p>به زنجیر زرش بر مهره می‌بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شفقت ساقهای بند سایش</p></div>
<div class="m2"><p>همی مالید و می‌بوسید پایش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حکایت‌های مهرانگیز می‌گفت</p></div>
<div class="m2"><p>که بر بانگ حکایت خوش توان خفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هر لفظی دهن پر نوش می‌داشت</p></div>
<div class="m2"><p>بر آواز شهنشه گوش می‌داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو خسرو خفت و کمتر شد جوابش</p></div>
<div class="m2"><p>به شیریت در سرایت کرد خوابش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو یار نازنین در خواب رفته</p></div>
<div class="m2"><p>فلک بیدار و از چشم آب رفته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان می‌گفت کامد فتنه سرمست</p></div>
<div class="m2"><p>سیاهی بر لبش مسمار می‌بست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرود آمد ز روزن دیو چهری</p></div>
<div class="m2"><p>نبوده در سرشتش هیچ مهری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو قصاب از غضب خونی نشانی</p></div>
<div class="m2"><p>چو نفاط از بروت آتش‌فشانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو دزد خانه بر کالا همی جست</p></div>
<div class="m2"><p>سریر شاه را بالا همی جست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به بالین شه آمد تیغ در مشت</p></div>
<div class="m2"><p>جگرگاهش درید و شمع را کشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنان زد بر جگرگاهش سر تیغ</p></div>
<div class="m2"><p>که خون برجست ازو چون آتش از میغ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو از ماهی جدا کرد آفتابی</p></div>
<div class="m2"><p>برون زد سر ز روزن چون عقابی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ملک در خواب خوش پهلو دریده</p></div>
<div class="m2"><p>گشاده چشم و خود را کشته دیده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز خونش خوابگه طوفان گرفته</p></div>
<div class="m2"><p>دلش از تشنگی از جان گرفته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به دل گفتا که شیرین را ز خوشخواب</p></div>
<div class="m2"><p>کنم بیدار و خواهم شربتی آب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دگر ره گفت با خطر نهفته</p></div>
<div class="m2"><p>که هست این مهربان شبها نخفته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو بیند بر من این بیداد و خواری</p></div>
<div class="m2"><p>نخسبد دیگر از فریاد و زاری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همان به کین سخن ناگفته باشد</p></div>
<div class="m2"><p>شوم من مرده و او خفته باشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به تلخی جان چنان داد آن وفادار</p></div>
<div class="m2"><p>که شیرین را نکرد از خواب بیدار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شکفته گلبنی بینی چو خورشید</p></div>
<div class="m2"><p>به سرسبزی جهان را داده امید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برآید ناگه ابری تند و سرمست</p></div>
<div class="m2"><p>بخون ریز ریاحین تیغ در دست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدان سختی فرو بارد تگرگی</p></div>
<div class="m2"><p>کزان گلبن نماند شاخ و برگی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو گردد باغبان خفته بیدار</p></div>
<div class="m2"><p>به باغ اندر نه گل بیند نه گلزار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه گوئی کز غم گل خون نریزد</p></div>
<div class="m2"><p>چو گل ریزد گلابی چون نریزد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز بس خون کز تن شه رفت چون آب</p></div>
<div class="m2"><p>در آمد نرگس شیرین ز خوشخواب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دگر شبها که بختش یار گشتی</p></div>
<div class="m2"><p>به بانگ نای و نی بیدار گشتی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فلک بنگر چه سردی کرد این بار</p></div>
<div class="m2"><p>که خون گرم شاهش کرد بیدار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پریشان شد چو مرغ تاب دیده</p></div>
<div class="m2"><p>که بود آن سهم را در خواب دیده</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پرند از خوابگاه شاه برداشت</p></div>
<div class="m2"><p>یکی دریای خون دیده آه برداشت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز شب می‌جست نور آفتابی</p></div>
<div class="m2"><p>دریغا چشمش آمد در خرابی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سریری دید سر بی‌تاج کرده</p></div>
<div class="m2"><p>چراغی روغنش تاراج کرده</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خزینه در گشاده گنج برده</p></div>
<div class="m2"><p>سپه رفته سپهسالار مرده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به گریه ساعتی شب را سیه کرد</p></div>
<div class="m2"><p>بسی بگریست وانگه عزم ره کرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گلاب و مشک با عنبر برآمیخت</p></div>
<div class="m2"><p>بر آن اندام خون آلود می‌ریخت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فرو شستش به گلاب و به کافور</p></div>
<div class="m2"><p>چنان کز روشنی می‌تافت چون نور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چنان بزمی که شاهان را طرازند</p></div>
<div class="m2"><p>بسازیدش کز آن بهتر نسازند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو شه را کرده بود آرایشی چست</p></div>
<div class="m2"><p>به کافور و گلاب اندام او شست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همان آرایش خود نیز نو کرد</p></div>
<div class="m2"><p>بدین اندیشه صد دل را گرو کرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دل شیرویه شیرین را ببایست</p></div>
<div class="m2"><p>ولیکن با کسی گفتن نشایست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نهانی کس فرستادش که خوش باش</p></div>
<div class="m2"><p>یکی هفته درین غم بارکش باش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو هفته بگذرد ماه دو هفته</p></div>
<div class="m2"><p>شود در باغ من چون گل شکفته</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خداوندی دهم بر هر گروهش</p></div>
<div class="m2"><p>ز خسرو بیشتر دارم شکوهش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو گنجش زیر زر پوشیده دارم</p></div>
<div class="m2"><p>کلید گنج‌ها او را سپارم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو شیرین این سخنها را نیوشید</p></div>
<div class="m2"><p>چو سرکه تند شد چون می بجوشید</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>فریبش داد تا باشد شکیبش</p></div>
<div class="m2"><p>نهاد آن کشتنی دل بر فریبش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پس آنگه هر چه بود اسباب خسرو</p></div>
<div class="m2"><p>ز منسوخ کهن تا کسوت نو</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به محتاجان و محرومان ندا کرد</p></div>
<div class="m2"><p>ز بهر جان شاهنشه فدا کرد</p></div></div>