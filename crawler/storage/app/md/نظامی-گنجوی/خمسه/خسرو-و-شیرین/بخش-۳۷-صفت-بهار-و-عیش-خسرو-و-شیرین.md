---
title: >-
    بخش ۳۷ - صفت بهار و عیش خسرو و شیرین
---
# بخش ۳۷ - صفت بهار و عیش خسرو و شیرین

<div class="b" id="bn1"><div class="m1"><p>چو پیر سبزپوش آسمانی</p></div>
<div class="m2"><p>ز سبزه برکشد بیخ جوانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوانان را و پیران را دگر بار</p></div>
<div class="m2"><p>به سرسبزی در آرد سرخ گلزار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل از گل تخت کاوسی بر آرد</p></div>
<div class="m2"><p>بنفشه پر طاووسی بر آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسا مرغا که عشق‌آوازه گردد</p></div>
<div class="m2"><p>بسا عشق کهن کان تازه گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو خرم شد به شیرین جان خسرو</p></div>
<div class="m2"><p>جهان می‌کرد عهد خرمی نو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو از خرم‌بهار و خرمی‌دوست</p></div>
<div class="m2"><p>به گل‌ها بردرید از خرمی پوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل از شادی علم در باغ می‌زد</p></div>
<div class="m2"><p>سپاه فاخته بر زاغ می‌زد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سمن ساقی و نرگس جام در دست</p></div>
<div class="m2"><p>بنفشه در خمار و سرخ گل مست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبا برقع گشاده مادگان را</p></div>
<div class="m2"><p>صلا درداده کار افتادگان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شمال انگیخته هر سو خروشی</p></div>
<div class="m2"><p>زده بر گاو چشمی پیل گوشی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمین نطع شقایق‌پوش گشته</p></div>
<div class="m2"><p>شقایق مهد مرزنگوش گشته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سهی سرو از چمن قامت کشیده</p></div>
<div class="m2"><p>ز عشق لاله پیراهن دریده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بنفشه تاب زلف افکنده بر دوش</p></div>
<div class="m2"><p>گشاده باد نسرین را بناگوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عروسان ریاحین دست بر روی</p></div>
<div class="m2"><p>شگرفان شکوفه شانه در موی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هوا بر سبزه گوهرها گسسته</p></div>
<div class="m2"><p>زمرد را به مروارید بسته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نموده ناف خاک آبستنی‌ها</p></div>
<div class="m2"><p>ز ناف آورده بیرون رستنی‌ها</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غزال شیرمست از دلنوازی</p></div>
<div class="m2"><p>بگرد سبزه با مادر به بازی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تذروان بر ریاحین پر فشانده</p></div>
<div class="m2"><p>ریاحین در تذروان پر نشانده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زهر شاخی شکفته نوبهاری</p></div>
<div class="m2"><p>گرفته هر گلی بر کف نثاری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نوای بلبل و آوای دراج</p></div>
<div class="m2"><p>شکیب عاشقان را داده تاراج</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین فصلی بدین عاشق‌نوازی</p></div>
<div class="m2"><p>خطا باشد خطا بی‌عشق‌بازی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خرامان خسرو و شیرین شب و روز</p></div>
<div class="m2"><p>بهر نزهت‌گهی شاد و دل‌افروز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گهی خوردند می در مرغزاری</p></div>
<div class="m2"><p>گهی چیدند گل در کوهساری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ریاحین بر ریاحین باده در دست</p></div>
<div class="m2"><p>به شهرود آمدند آن روز سرمست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جنیبت بر لب شهرود بستند</p></div>
<div class="m2"><p>به بانگ رود و رامشگر نشستند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حلاوت‌های شیرین شکرخند</p></div>
<div class="m2"><p>نی شهرود را کرده نی قند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همان رونق ز خوبیش آن طرف را</p></div>
<div class="m2"><p>که از باران نیسانی صدف را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عبیر ارزان ز جعد مشکبیزش</p></div>
<div class="m2"><p>شکر قربان ز لعل شهدخیزش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز بس خنده که شهدش بر شکر زد</p></div>
<div class="m2"><p>به خوزستان شد افغان طبرزد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قد چون سروش از دیوان شاهی</p></div>
<div class="m2"><p>به گلبن داده تشریف سپاهی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو گل بر نرگسش کرده نظاره</p></div>
<div class="m2"><p>به دندان کرده خود را پاره پاره</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سمن کز خواجگی بر گل زدی دوش</p></div>
<div class="m2"><p>غلام آن بناگوش از بن گوش</p></div></div>