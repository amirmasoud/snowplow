---
title: >-
    بخش ۱۰۵ - تمثیل موبد دوم
---
# بخش ۱۰۵ - تمثیل موبد دوم

<div class="b" id="bn1"><div class="m1"><p>دوم موبد به قصری کرد مانند</p></div>
<div class="m2"><p>که بر گردون کشد گیتی خداوند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از او شخصی فرو افتد گران سنگ</p></div>
<div class="m2"><p>ز بیم جان زند در کنگره چنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ماندن دست و بازو ریش گردد</p></div>
<div class="m2"><p>وز افتادن مضرت بیش گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکنجه گرچه پنجه‌اش را کند سست</p></div>
<div class="m2"><p>کند سر پنجه را در کنگره چست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم آخر کار کو بی‌تاب گردد</p></div>
<div class="m2"><p>هم او هم کنگره پرتاب گردد</p></div></div>