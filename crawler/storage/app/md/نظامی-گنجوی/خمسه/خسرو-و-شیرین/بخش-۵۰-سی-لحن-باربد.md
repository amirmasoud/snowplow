---
title: >-
    بخش ۵۰ - (سی لحن باربد)
---
# بخش ۵۰ - (سی لحن باربد)

<div class="b" id="bn1"><div class="m1"><p>در آمد باربد چون بلبل مست</p></div>
<div class="m2"><p>گرفته بربطی چون آب در دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز صد دستان که او را بود در ساز</p></div>
<div class="m2"><p>گزیده کرد سی لحن خوش آواز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بی‌لحنی بدان سی لحن چون نوش</p></div>
<div class="m2"><p>گهی دل دادی و گه بستدی هوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بربط چون سر زخمه درآورد</p></div>
<div class="m2"><p>ز رود خشک بانگ تر درآورد</p></div></div>
<div class="b2" id="bn5"><p>اول گنج بادآورد</p></div>
<div class="b" id="bn6"><div class="m1"><p>چو باد از گنج بادآورد راندی</p></div>
<div class="m2"><p>ز هر بادی لبش گنجی فشاندی</p></div></div>
<div class="b2" id="bn7"><p>دوم گنج گاو</p></div>
<div class="b" id="bn8"><div class="m1"><p>چو گنج گاو را کردی نواسنج</p></div>
<div class="m2"><p>برافشاندی زمین هم گاو و هم گنج</p></div></div>
<div class="b2" id="bn9"><p>سوم گنج سوخته</p></div>
<div class="b" id="bn10"><div class="m1"><p>ز گنج سوخته چون ساختی راه</p></div>
<div class="m2"><p>ز گرمی سوختی صد گنج را آه</p></div></div>
<div class="b2" id="bn11"><p>چهارم شادروان مروارید</p></div>
<div class="b" id="bn12"><div class="m1"><p>چو شادروان مروارید گفتی</p></div>
<div class="m2"><p>لبش گفتی که مروارید سفتی</p></div></div>
<div class="b2" id="bn13"><p>پنجم تخت طاقدیسی</p></div>
<div class="b" id="bn14"><div class="m1"><p>چو تخت طاقدیسی ساز کردی</p></div>
<div class="m2"><p>بهشت از طاق‌ها در باز کردی</p></div></div>
<div class="b2" id="bn15"><p>ششم و هفتم ناقوسی و اورنگی</p></div>
<div class="b" id="bn16"><div class="m1"><p>چو ناقوسی و اورنگی زدی ساز</p></div>
<div class="m2"><p>شدی ارونگ چون ناقوس از آواز</p></div></div>
<div class="b2" id="bn17"><p>هشتم حقه کاوس</p></div>
<div class="b" id="bn18"><div class="m1"><p>چو قند از حقه کاوس دادی</p></div>
<div class="m2"><p>شکر کالای او را بوس دادی</p></div></div>
<div class="b2" id="bn19"><p>نهم ماه بر کوهان</p></div>
<div class="b" id="bn20"><div class="m1"><p>چون لحن ماه بر کوهان گشادی</p></div>
<div class="m2"><p>زبانش ماه بر کوهان نهادی</p></div></div>
<div class="b2" id="bn21"><p>دهم مشک دانه</p></div>
<div class="b" id="bn22"><div class="m1"><p>چو برگفتی نوای مشک دانه</p></div>
<div class="m2"><p>ختن گشتی ز بوی مشک خانه</p></div></div>
<div class="b2" id="bn23"><p>یازدهم آرایش خورشید</p></div>
<div class="b" id="bn24"><div class="m1"><p>چو زد زآرایش خورشید راهی</p></div>
<div class="m2"><p>در آرایش بدی خورشید ماهی</p></div></div>
<div class="b2" id="bn25"><p>دوازدهم نیمروز</p></div>
<div class="b" id="bn26"><div class="m1"><p>چو گفتی نیمروز مجلس‌افروز</p></div>
<div class="m2"><p>خرد بی‌خود بدی تا نیمه روز</p></div></div>
<div class="b2" id="bn27"><p>سیزدهم سبز در سبز</p></div>
<div class="b" id="bn28"><div class="m1"><p>چو بانگ سبز در سبزش شنیدی</p></div>
<div class="m2"><p>ز باغ زرد سبزه بر دمیدی</p></div></div>
<div class="b2" id="bn29"><p>چهاردهم قفل رومی</p></div>
<div class="b" id="bn30"><div class="m1"><p>چو قفل رومی آوردی در آهنگ</p></div>
<div class="m2"><p>گشادی قفل گنج از روم و از زنگ</p></div></div>
<div class="b2" id="bn31"><p>پانزدهم سروستان</p></div>
<div class="b" id="bn32"><div class="m1"><p>چو بر دستان سروستان گذشتی</p></div>
<div class="m2"><p>صبا سالی به سروستان نگشتی</p></div></div>
<div class="b2" id="bn33"><p>شانزدهم سرو سهی</p></div>
<div class="b" id="bn34"><div class="m1"><p>و گر سرو سهی را ساز دادی</p></div>
<div class="m2"><p>سهی سروش به خون خط باز دادی</p></div></div>
<div class="b2" id="bn35"><p>هفدهم نوشین باده</p></div>
<div class="b" id="bn36"><div class="m1"><p>چو نوشین باده را در پرده بستی</p></div>
<div class="m2"><p>خمار باده نوشین شکستی</p></div></div>
<div class="b2" id="bn37"><p>هیجدهم رامش جان</p></div>
<div class="b" id="bn38"><div class="m1"><p>چو کردی رامش جان را روانه</p></div>
<div class="m2"><p>ز رامش جان فدا کردی زمانه</p></div></div>
<div class="b2" id="bn39"><p>نوزدهم ناز نوروز یا ساز نوروز</p></div>
<div class="b" id="bn40"><div class="m1"><p>چو در پرده کشیدی ناز نوروز</p></div>
<div class="m2"><p>به نوروزی نشستی دولت آن روز</p></div></div>
<div class="b2" id="bn41"><p>بیستم مشگویه</p></div>
<div class="b" id="bn42"><div class="m1"><p>چو بر مشگویه کردی مشگ مالی</p></div>
<div class="m2"><p>همه مشگو شدی پرمشک حالی</p></div></div>
<div class="b2" id="bn43"><p>بیست و یکم مهرگانی</p></div>
<div class="b" id="bn44"><div class="m1"><p>چو نو کردی نوای مهرگانی</p></div>
<div class="m2"><p>ببردی هوش خلق از مهربانی</p></div></div>
<div class="b2" id="bn45"><p>بیست و دوم مروای نیک</p></div>
<div class="b" id="bn46"><div class="m1"><p>چو بر مروای نیک انداختی فال</p></div>
<div class="m2"><p>همه نیک آمدی مروای آن سال</p></div></div>
<div class="b2" id="bn47"><p>بیست و سوم شبدیز</p></div>
<div class="b" id="bn48"><div class="m1"><p>چو در شب بر گرفتی راه شبدیز</p></div>
<div class="m2"><p>شدندی جمله آفاق شب‌خیز</p></div></div>
<div class="b2" id="bn49"><p>بیست و چهارم شب فرخ</p></div>
<div class="b" id="bn50"><div class="m1"><p>چو بر دستان شب فرخ کشیدی</p></div>
<div class="m2"><p>از آن فرخنده‌تر شب کس ندیدی</p></div></div>
<div class="b2" id="bn51"><p>بیست و پنجم فرخ‌روز</p></div>
<div class="b" id="bn52"><div class="m1"><p>چو یارش رای فرخ‌روز گشتی</p></div>
<div class="m2"><p>زمانه فرخ و فیروز گشتی</p></div></div>
<div class="b2" id="bn53"><p>بیست و ششم غنچه کبک دری</p></div>
<div class="b" id="bn54"><div class="m1"><p>چو کردی غنچه کبک دری تیز</p></div>
<div class="m2"><p>ببردی غنچه کبک دلاویز</p></div></div>
<div class="b2" id="bn55"><p>بیست و هفتم نخجیرگان</p></div>
<div class="b" id="bn56"><div class="m1"><p>چو بر نخجیرگان تدبیر کردی</p></div>
<div class="m2"><p>بسی چون زهره را نخجیر کردی</p></div></div>
<div class="b2" id="bn57"><p>بیست و هشتم کین سیاوش</p></div>
<div class="b" id="bn58"><div class="m1"><p>چو زخمه راندی از کین سیاوش</p></div>
<div class="m2"><p>پر از خون سیاوشان شدی گوش</p></div></div>
<div class="b2" id="bn59"><p>بیست و نهم کین ایرج</p></div>
<div class="b" id="bn60"><div class="m1"><p>چو کردی کین ایرج را سرآغاز</p></div>
<div class="m2"><p>جهان را کین ایرج نو شدی باز</p></div></div>
<div class="b2" id="bn61"><p>سی‌ام باغ شیرین</p></div>
<div class="b" id="bn62"><div class="m1"><p>چو کردی باغ شیرین را شکربار</p></div>
<div class="m2"><p>درخت تلخ را شیرین شدی بار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نواهائی بدینسان رامش‌انگیز</p></div>
<div class="m2"><p>همی زد باربد در پرده تیز</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بگفت باربد کز بار به گفت</p></div>
<div class="m2"><p>زبان خسروش صدبار زه گفت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چنان بد رسم آن بدر منور</p></div>
<div class="m2"><p>که بر هر زه بدادی بدره زر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به هر پرده که او بنواخت آن روز</p></div>
<div class="m2"><p>ملک گنجی دگر پرداخت آن روز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به هر پرده که او برزد نوائی</p></div>
<div class="m2"><p>ملک دادش پر از گوهر قبائی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>زهی لفظی که گر بر تنگ‌دستی</p></div>
<div class="m2"><p>زهی گفتی زهی زرین به دستی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>درین دوران گرت زین به پسندند</p></div>
<div class="m2"><p>زهی پشمین به گردن وانبندند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ز عالی همتی گردن برافراز</p></div>
<div class="m2"><p>طناب هرزه از گردن بینداز</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به خرسندی طمع را دیده بردوز</p></div>
<div class="m2"><p>ز چون من قطره دریائی درآموز</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>که چندین گنج بخشیدم به شاهی</p></div>
<div class="m2"><p>وز آن خرمن نجستم برگ کاهی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به بی‌برگی سخن را راست کردم</p></div>
<div class="m2"><p>نه او داد و نه من درخواست کردم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>مرا این بس که پر کردم جهان را</p></div>
<div class="m2"><p>ولی نعمت شدم دریا و کان را</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>نظامی گر زه زرین بسی هست</p></div>
<div class="m2"><p>زه تو زهد شد مگذارش از دست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بدین زه گر گریبان را طرازی</p></div>
<div class="m2"><p>کنی بر گردنان گردن فرازی</p></div></div>