---
title: >-
    بخش ۸ - ستایش اتابک اعظم شمس‌الدین ابوجعفر محمدبن ایلدگز
---
# بخش ۸ - ستایش اتابک اعظم شمس‌الدین ابوجعفر محمدبن ایلدگز

<div class="b" id="bn1"><div class="m1"><p>به فرح فالی و فیروزمندی</p></div>
<div class="m2"><p>سخن را دادم از دولت بلندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طراز آفرین بستم قلم را</p></div>
<div class="m2"><p>زدم بر نام شاهنشه رقم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر و سر خیل شاهان شاه آفاق</p></div>
<div class="m2"><p>چو ابرو با سری هم جفت و هم طاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملک اعظم اتابک داور دور</p></div>
<div class="m2"><p>که افکند از جهان آوازه جور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابو جعفر محمد کز سر جود</p></div>
<div class="m2"><p>خراسان گیر خواهد شد چو محمود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهانگیر آفتاب عالم افروز</p></div>
<div class="m2"><p>بهر بقعه قِران ساز و قرین سوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلیل آنک آفتاب خاص و عام است</p></div>
<div class="m2"><p>که شمس‌الدین و الدنیاش نام است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان چون شمس کانجم را دهد نور</p></div>
<div class="m2"><p>دهد ما را سعادت چشم بد دور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آن بخشش که رحمت عام کردند</p></div>
<div class="m2"><p>دو صاحب را محمد نام کردند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی ختم نبوت گشته ذاتش</p></div>
<div class="m2"><p>یکی ختم ممالک بر حیاتش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی برج عرب را تا ابد ماه</p></div>
<div class="m2"><p>یکی ملک عجم را از ازل شاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی دین را ز ظلم آزاد کرده</p></div>
<div class="m2"><p>یکی دنیا به عدل آباد کرده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زهی نامی که کرد از چشمهٔ نوش</p></div>
<div class="m2"><p>دو عالم را دو میمش حلقه در گوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز رشک نام او عالم دو نیم است</p></div>
<div class="m2"><p>که عالم ، یکی او را دو میم است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به ترکان قلم بی‌نسخ تاراج</p></div>
<div class="m2"><p>یکی میمش کمر بخشد یکی تاج</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به نور تاجبخشی چون درخش است</p></div>
<div class="m2"><p>بدین تایید نامش تاج بخش است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو طوفی سوی جود آرد وجودش</p></div>
<div class="m2"><p>ز جودی بگذرد طوفان جودش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فلک با او کرا گوید که برخیز</p></div>
<div class="m2"><p>که هست این قایم افکن قایم آویز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>محیط از شرم جودش زیر افلاک</p></div>
<div class="m2"><p>جبین‌واری عرق شد بر سر خاک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو دریا در دهد بی‌تلخ روئی</p></div>
<div class="m2"><p>گهر بخشد چو کان بی‌تنگ خوئی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ببارش تیغ او چون آهنین میغ</p></div>
<div class="m2"><p>کلید هفت کشور نام آن تیغ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جهت شش طاق او بر دوش دارد</p></div>
<div class="m2"><p>فلک نه حلقه هم در گوش دارد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جهان چون مادران گشته مطیعش</p></div>
<div class="m2"><p>بنام عدل زاده چون ربیعش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خبرهائی که بیرون از اثیر است</p></div>
<div class="m2"><p>به کشف خاطر او در ضمیر است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کدامین علم کو در دل ندارد</p></div>
<div class="m2"><p>کدام اقبال کو حاصل ندارد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به سر پنجه چو شیران دلیر است</p></div>
<div class="m2"><p>بدین شیر افکنی یارب چه شیر است؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نه با شیری کسی را رنجه دارد</p></div>
<div class="m2"><p>نه از شیران کسی هم پنجه دارد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سنانش ، موی باریکی سترده</p></div>
<div class="m2"><p>ز چشم موی بینان موی برده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز هر مقراضه کو چون صبح رانده</p></div>
<div class="m2"><p>عدو چون میخ در مقراض مانده</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زهر شمشیر کو چون صبح جسته</p></div>
<div class="m2"><p>مخالف چون شفق در خون نشسته</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سمندش در شتاب آهنگ بیشی</p></div>
<div class="m2"><p>فلک را هفت میدان داده پیشی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زمین زیر عنانش گاو ریش است</p></div>
<div class="m2"><p>اگر چه هم عنان گاومیش است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کله بر چرخ دارد فرق بر ماه</p></div>
<div class="m2"><p>کله داری چنین باید زهی شاه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همه عالم گرفت از نیک رائی</p></div>
<div class="m2"><p>چنین باشد بلی ظِلِّ خدائی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سیاهی و سپیدی هر چه هستند</p></div>
<div class="m2"><p>گذشت از کردگار او را پرستند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زره‌پوشان دریای شکن گیر</p></div>
<div class="m2"><p>به فرق دشمنش پوینده چون تیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>طرفداران کوه آهنین چنگ</p></div>
<div class="m2"><p>به رجم حاسدش برداشته سنگ</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گلوی خصم وی سنگین درایست</p></div>
<div class="m2"><p>چو مغناطیس از آن آهنربایست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نشد غافل ز خصم آگاهی این است</p></div>
<div class="m2"><p>نخسبد شرط شاهنشاهی این است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اتابک ایلد گز شاه جهان گیر</p></div>
<div class="m2"><p>که زد بر هفت کشور چار تکبیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دو عالم را بدین یک جان سپرده است</p></div>
<div class="m2"><p>چو جانش هست نتوان گفت مرده است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جهان زنده بدین صاحبقران است</p></div>
<div class="m2"><p>درین شک نیست کو جان جهان است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جز این یکسر ندارد شخص عالم</p></div>
<div class="m2"><p>مبادا کز سرش موئی شود کم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کس از مادر بدین دولت نزاده است</p></div>
<div class="m2"><p>حبش تا چین بدین دولت گشاده است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>فکنده در عراق او باده در جام</p></div>
<div class="m2"><p>فتاده هیبتش در روم و در شام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>صلیب زنگ را بر تارک روم</p></div>
<div class="m2"><p>به دندان ظفر خائیده چون موم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سپاه روم را کز ترک شد پیش</p></div>
<div class="m2"><p>به هندی تیغ کرده هندوی خویش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شکارستان او ابخاز و دربند</p></div>
<div class="m2"><p>شبیخونش به خوارزم و سمرقند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز گنجه فتح خوزستان که کرده است؟</p></div>
<div class="m2"><p>ز عمان تا به اصفاهان که خورده است؟</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ممیراد این فروغ از روی این ماه</p></div>
<div class="m2"><p>میفتاد این کلاه از فرق این شاه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هر آن چیزی که او را نیست مقصود</p></div>
<div class="m2"><p>به آتش سوخته گر هست خود عود</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هر آنکس کز جهان با او زند سر</p></div>
<div class="m2"><p>در آب افتاد اگر خود هست شکر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هر آن شخصی که او را هست ازو رنج</p></div>
<div class="m2"><p>به زیر خاک باد ار خود بود گنج</p></div></div>