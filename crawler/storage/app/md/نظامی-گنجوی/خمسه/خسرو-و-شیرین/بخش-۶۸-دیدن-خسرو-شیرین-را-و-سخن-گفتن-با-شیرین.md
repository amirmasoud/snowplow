---
title: >-
    بخش ۶۸ - دیدن خسرو شیرین را و سخن گفتن با شیرین
---
# بخش ۶۸ - دیدن خسرو شیرین را و سخن گفتن با شیرین

<div class="b" id="bn1"><div class="m1"><p>چو خسرو دید ماه خرگهی را</p></div>
<div class="m2"><p>چمن کرد از دل آن سرو سهی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهشتی دید در قصری نشسته</p></div>
<div class="m2"><p>بهشتی‌وار در بر خلق بسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عشق او که یاری بود چالاک</p></div>
<div class="m2"><p>ز کرسی خواست افتادن سوی خاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به عیاری ز جای خویش برجست</p></div>
<div class="m2"><p>برابر دست خود بوسید و بنشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبان بگشاد با عذری دل‌آویز</p></div>
<div class="m2"><p>ز پرسش کرد بر شیرین شکرریز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که دایم تازه باش ای سرو آزاد</p></div>
<div class="m2"><p>سرت سبز و رخت سرخ و دلت شاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان روشن به روی صبح‌خندت</p></div>
<div class="m2"><p>فلک در سایه سرو بلندت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم را تازه کرد این خرمی‌ها</p></div>
<div class="m2"><p>خجل کردی مرا از مردمی‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز گنج و گوهر و منسوج و دیبا</p></div>
<div class="m2"><p>رهم کردی چو مهد خویش زیبا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز نعلک‌های گوش گوهرآویز</p></div>
<div class="m2"><p>فکندی لعل‌ها در نعل شبدیز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بس گوهر که در نعلم کشیدی</p></div>
<div class="m2"><p>به رخ بر رشته لعلم کشیدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همین باشد نثارافشان کویت</p></div>
<div class="m2"><p>به رویت شادم ای شادی به رویت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به من درساختی چون شهد با شیر</p></div>
<div class="m2"><p>ز خدمت‌ها نکردی هیچ تقصیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ولی در بستنت بر من چرا بود</p></div>
<div class="m2"><p>خطا دیدم نگارا یا خطا بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زمین‌وارم رها کردی به پستی</p></div>
<div class="m2"><p>تو رفتی چون فلک بالا نشستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نگویم بر توام بالایی‌ای هست</p></div>
<div class="m2"><p>که در جنس سخن رعنایی‌ای هست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه مهمان توام؟ بر روی مهمان</p></div>
<div class="m2"><p>چرا در بایدت بستن بدینسان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نشاید بست در بر میهمانی</p></div>
<div class="m2"><p>که جز تو نیستش جان و جهانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کریمانی که با مهمان نشینند</p></div>
<div class="m2"><p>به مهمان بهترک زین باز بینند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مگر ماهی تو یا حور ای پریوش</p></div>
<div class="m2"><p>که نزدیکت نباشد آمدن خوش</p></div></div>