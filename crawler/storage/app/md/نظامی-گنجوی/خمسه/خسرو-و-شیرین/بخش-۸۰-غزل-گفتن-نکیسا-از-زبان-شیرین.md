---
title: >-
    بخش ۸۰ - غزل گفتن نکیسا از زبان شیرین
---
# بخش ۸۰ - غزل گفتن نکیسا از زبان شیرین

<div class="b" id="bn1"><div class="m1"><p>نکیسا بر طریقی کان صنم خواست</p></div>
<div class="m2"><p>فرو گفت این غزل در پرده راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مخسب ای دیده دولت زمانی</p></div>
<div class="m2"><p>مگر کز خوشدلی یابی نشانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآی از کوه صبر ای صبح امید</p></div>
<div class="m2"><p>دلم را چشم روشن کن به خورشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بساز ای بخت با من روزکی چند</p></div>
<div class="m2"><p>کلیدی خواه و بگشای از من این بند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سر بیرون کن ای طالع گرانی</p></div>
<div class="m2"><p>رها کن تا توانی ناتوانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به عیاری برآر ای دوست دستی</p></div>
<div class="m2"><p>برافکن لشگر غم را شکستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جگر در تاب و دل در موج خونست</p></div>
<div class="m2"><p>گر آری رحمتی وقتش کنونست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه زین افتاده‌تر یابی ضعیفی</p></div>
<div class="m2"><p>نه زین بیچاره‌تر یابی حریفی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر بر کف ندانم ریخت آبی</p></div>
<div class="m2"><p>توانم کرد بر آتش کبابی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>و گر جلاب دادن را نشایم</p></div>
<div class="m2"><p>فقاعی را به دست آخر گشایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>و گر نقشی ندانم دوخت آخر</p></div>
<div class="m2"><p>سپند خانه دانم سوخت آخر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و گر چینی ندانم در نشاندن</p></div>
<div class="m2"><p>توانم گردی از دامن فشاندن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>میندازم چو سایه بر سر خاک</p></div>
<div class="m2"><p>که من خود اوفتادم زار و غمناک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو مه در خانه پروینیت باید</p></div>
<div class="m2"><p>چو زهره درد بر چینیت باید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرایت را بهر خدمت که خواهی</p></div>
<div class="m2"><p>کنیزی می‌کنم دعوی نه شاهی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا پرسی که چونی زارزویم</p></div>
<div class="m2"><p>چو میدانی و می‌پرسی چه گویم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غریبی چون بود غمخوار مانده</p></div>
<div class="m2"><p>ز کار افتاده و در کار مانده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو گل در عاشقی پرده دریده</p></div>
<div class="m2"><p>ز عالم رفته و عالم ندیده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو خاک آماجگاه تیر گشته</p></div>
<div class="m2"><p>چو لاله در جوانی پیر گشته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به امیدی جهان بر باد داده</p></div>
<div class="m2"><p>به پنداری بدین روز اوفتاده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه هم پشتی که پشتم گرم دارد</p></div>
<div class="m2"><p>نه بختی کز غریبان شرم دارد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مثل زد غرفه چون می‌مرد بی‌رخت</p></div>
<div class="m2"><p>که باید مرده را نیز از جهان بخت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز بی کامی دلم تنها نشین است</p></div>
<div class="m2"><p>بسازم گر ترا کام اینچنین است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو برناید مرا کامی که باید</p></div>
<div class="m2"><p>بسازم تا ترا کامی بر آید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مگر تلخ آمد آن لب را وجودم</p></div>
<div class="m2"><p>که وقت ساختن سوزد چو عودم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرا این سوختن سوری عظیمست</p></div>
<div class="m2"><p>که سوز عاشقان سوزی سلیمست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نخواهم کرد بر تو حکم رانی</p></div>
<div class="m2"><p>گرم زین بهترک داری تو دانی</p></div></div>