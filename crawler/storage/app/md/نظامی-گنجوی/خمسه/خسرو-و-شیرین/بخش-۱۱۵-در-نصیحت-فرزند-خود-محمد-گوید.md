---
title: >-
    بخش ۱۱۵ - در نصیحت فرزند خود محمد گوید
---
# بخش ۱۱۵ - در نصیحت فرزند خود محمد گوید

<div class="b" id="bn1"><div class="m1"><p>ببین ای هفت ساله قره‌العین</p></div>
<div class="m2"><p>مقام خویشتن در قاب قوسین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منت پروردم و روزی خدا داد</p></div>
<div class="m2"><p>نه بر تو نام من نام خدا باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین دور هلالی شاد می‌خند</p></div>
<div class="m2"><p>که خندیدیم ماهم روزکی چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو بدر انجمن گردد هلاکت</p></div>
<div class="m2"><p>بر افروزند انجم را جمالت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قلم درکش به حرفی کان هوائیست</p></div>
<div class="m2"><p>علم برکش به علمی کان خدائیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ناموسی که گوید عقل نامی</p></div>
<div class="m2"><p>زهی فرزانه فرزند نظامی</p></div></div>