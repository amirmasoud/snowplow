---
title: >-
    بخش ۱۷ - به خواب دیدن خسرو نیای خویش انوشیروان را
---
# بخش ۱۷ - به خواب دیدن خسرو نیای خویش انوشیروان را

<div class="b" id="bn1"><div class="m1"><p>چو آمد زلف شب در عطر رسائی</p></div>
<div class="m2"><p>به تاریکی فرو شد روشنائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برون آمد ز پرده سحر سازی</p></div>
<div class="m2"><p>شش اندازی بجای شیشه بازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به طاعت خانه شد خسرو کمر بست</p></div>
<div class="m2"><p>نیایش کرد یزدان را و بنشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به برخورداری آمد خواب نوشین</p></div>
<div class="m2"><p>که بر ناخورده بود از خواب دوشین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیای خویشتن را دید در خواب</p></div>
<div class="m2"><p>که گفت ای تازه خورشید جهان تاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر شد چار مولای عزیزت</p></div>
<div class="m2"><p>بشارت می‌دهم بر چار چیزت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی چون ترشی آن غوره خوردی</p></div>
<div class="m2"><p>چو غوره زان ترشروئی نکردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلارامی تو را در بر نشیند</p></div>
<div class="m2"><p>کزو شیرین‌تری دوران نبیند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوم چون مرکبت را پی بریدند</p></div>
<div class="m2"><p>وزان بر خاطرت گردی ندیدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به شبرنگی رسی شبدیز نامش</p></div>
<div class="m2"><p>که صرصر درنیابد گردگامش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیم چون شه به دهقان داد تختت</p></div>
<div class="m2"><p>وزان تندی نشد شوریده بختت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به دست آری چنان شاهانه تختی</p></div>
<div class="m2"><p>که باشد راست چون زرین درختی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چهارم چون صبوری کردی آغاز</p></div>
<div class="m2"><p>در آن پرده که مطرب گشت بی‌ساز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نوا سازی دهندت بار بدنام</p></div>
<div class="m2"><p>که بر یادش گوارد زهر در جام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به جای سنگ خواهی یافتن زر</p></div>
<div class="m2"><p>به جای چار مهره چار گوهر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ملک‌زاده چو گشت از خواب بیدار</p></div>
<div class="m2"><p>پرستش کرد یزدان را دگر بار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زبان را روز و شب خاموش می‌داشت</p></div>
<div class="m2"><p>نمودار نیارا گوش می‌داشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه شب با خردمندان نخفتی</p></div>
<div class="m2"><p>حکایت باز پرسیدی و گفتی</p></div></div>