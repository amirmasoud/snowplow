---
title: >-
    بخش ۱۲۰ - طلب کردن طغرل شاه حکیم نظامی را
---
# بخش ۱۲۰ - طلب کردن طغرل شاه حکیم نظامی را

<div class="b" id="bn1"><div class="m1"><p>چو داد اندیشه جادو دماغم</p></div>
<div class="m2"><p>ز چشم افسای این لعبت فراغم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هر عقلی مبارک بادم آمد</p></div>
<div class="m2"><p>طریق العقل واحد یادم آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکایت گونه‌ای می‌کردم از بخت</p></div>
<div class="m2"><p>که در بازو کمانی داشتم سخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسی تیر از کمان افکنده بودم</p></div>
<div class="m2"><p>نشد بر هیچ کاغذ کازمودم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکایت چون برانگیزد خروشی</p></div>
<div class="m2"><p>نماند بی‌بها گوهر فروشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین مهدی که ماهش در نقابست</p></div>
<div class="m2"><p>ز مه بگذر سخن در آفتابست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خریدندش به چندان دلپسندی</p></div>
<div class="m2"><p>رساندندش به چرخ از سربلندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پذیرفتند چندان ملک و مالم</p></div>
<div class="m2"><p>که باور کردنش آمد محالم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسی چینی نورد نابریده</p></div>
<div class="m2"><p>بجز مشک از هوا گردی ندیده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همان ختلی خرام خسروانی</p></div>
<div class="m2"><p>سر افسار زر و طوق کیانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به شریفم حدیث از گنج می‌رفت</p></div>
<div class="m2"><p>غلام از ده کنیز از پنج می‌رفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پذیرشها نگر در کار چون ماند</p></div>
<div class="m2"><p>ستورم چون سقط شد بار چون ماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پذیرنده چگونه رخت برداشت</p></div>
<div class="m2"><p>زمین کشته را ندروده بگذاشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدین افسوس می خوردم دریغی</p></div>
<div class="m2"><p>ز دم بر خویشتن چون شمع تیغی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که ناگه پیکی آمد نامه در دست</p></div>
<div class="m2"><p>به تعجیلم درودی داد و بنشست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که سی روزه سفر کن کاینک از راه</p></div>
<div class="m2"><p>به سی فرسنگی آمد موکب شاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ترا خواهد که بیند روزکی چند</p></div>
<div class="m2"><p>کلید خویش را مگذار در بند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مثالم داد کاین توقیع شاهست</p></div>
<div class="m2"><p>همه شحنه همه تعویذ را هست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مثال شاه را بر سر نهادم</p></div>
<div class="m2"><p>سه جا بوسیدم و سر بر گشادم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فرو خواندم مر آن فرمان به فرهنگ</p></div>
<div class="m2"><p>کلیدم ز آهن آمد آهن از سنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به عزم خدمت شه جستم از جای</p></div>
<div class="m2"><p>در آوردم به پشت بارگی پای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برون راندم سوی صحرا شتابان</p></div>
<div class="m2"><p>گرفته رقص در کوه و بیابان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز گوران تک ربودم در دویدن</p></div>
<div class="m2"><p>گرو بردم ز مرغان در پریدن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز رقص ره نمی‌شد طبع سیرم</p></div>
<div class="m2"><p>ز من رقاص‌تر مرکب بزیرم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه ره سجده می‌بردم قلم‌وار</p></div>
<div class="m2"><p>به تارک راه می‌رفتم چو پرگار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به هر منزل کزان ره می‌بردم</p></div>
<div class="m2"><p>دعای دولت شه می‌شنیدم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بهر چشمه که آبی تازه خوردم</p></div>
<div class="m2"><p>بشکر شه دعائی تازه کردم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نسیم دولت از هر کوه ورودی</p></div>
<div class="m2"><p>ز لطف شاه می‌دادم درودی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز مشگین بوی آن حضرت بهرگام</p></div>
<div class="m2"><p>زمین در زیر من چون عنبر خام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو بر خود رنج ره کوتاه کردم</p></div>
<div class="m2"><p>زمین بوس بساط شاه کردم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>درون شد قاصد و شه را خبر کرد</p></div>
<div class="m2"><p>که چشمه بر لب دریا گذر کرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برون آمد ز درگه حاجب خاص</p></div>
<div class="m2"><p>ز دریا داد گوهرها به غواص</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرا در بزمگاه شاه بردند</p></div>
<div class="m2"><p>عطارد را به برج ماه بردند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نشسته شاه چون تابنده خورشید</p></div>
<div class="m2"><p>به تاج کیقباد و تخت جمشید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زمین بوسش فلک را تشنه کرده</p></div>
<div class="m2"><p>مه از سرهنگ پاسش دشنه خورده</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شکوه تاجش از فر جهانگیر</p></div>
<div class="m2"><p>فکنده قیروان را جامه در قیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>طرف‌داران ز سقسین تا سمرقند</p></div>
<div class="m2"><p>به نوبتگاه درگاهش کمربند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>درش بر حمل کشورها گشاده</p></div>
<div class="m2"><p>همه در حمل بر حمل ایستاده</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به دریا ماند موج نیل رنگش</p></div>
<div class="m2"><p>که در دل بود هم در هم نهنگش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سر تاج قزلشاه از سر تخت</p></div>
<div class="m2"><p>نهاده تاج دولت بر سر بخت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بهشتی بزمش از بزم بهشتی</p></div>
<div class="m2"><p>ز حوضکهای می پر کرده کشتی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کف رادش به هر کس داده بهری</p></div>
<div class="m2"><p>گهی شهری و گاهی حمل شهری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز تیغ تنگ چشمان حصاری</p></div>
<div class="m2"><p>قدر خان را در آن در تنگباری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خروش ارغنون و ناله چنگ</p></div>
<div class="m2"><p>رسانیده به چرخ زهره آهنگ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به ریشم زن نواها بر کشیده</p></div>
<div class="m2"><p>بریشم پوش پیراهن دریده</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نواها مختلف در پرده‌سازی</p></div>
<div class="m2"><p>نوازش متفق در جان نوازی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>غزلهای نظامی را غزالان</p></div>
<div class="m2"><p>زده بر زخمهای چنگ نالان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گرفته ساقیان می بر کف دست</p></div>
<div class="m2"><p>شهنشه خورده می بدخواه شه مست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو دادندش خبر کامد نظامی</p></div>
<div class="m2"><p>فزودش شادیی بر شادکامی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شکوه زهد من بر من نگهداشت</p></div>
<div class="m2"><p>نه زان پشمی که زاهد در کله داشت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بفرمود از میان می بر گرفتن</p></div>
<div class="m2"><p>مدارای مرا پی بر گرفتن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به خدمت ساقیان را داشت در بند</p></div>
<div class="m2"><p>به سجده مطربان را کرد خرسند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اشارت کرد کاین یک روز تا شام</p></div>
<div class="m2"><p>نظامی را شویم از رود و از جام</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نوای نظم او خوشتر ز رود است</p></div>
<div class="m2"><p>سراسر قولهای او سرود است</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو خضر آمد ز باده سر بتابیم</p></div>
<div class="m2"><p>که آب زندگی با خضر یابیم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>پس آنکه حاجب خاص آمد و گفت</p></div>
<div class="m2"><p>درای ای طاق با هر دانشی جفت</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>درون رفتم تنی لرزنده چون بید</p></div>
<div class="m2"><p>چو ذره کو گراید سوی خورشید</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سر خود همچنان بر گردن خویش</p></div>
<div class="m2"><p>سرافکنده فکنده هر دو در پیش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بدان تا بوسم او را چون زمین پای</p></div>
<div class="m2"><p>چو دیدم آسمان برخاست از جای</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گرفتم در کنار از دل نوازی</p></div>
<div class="m2"><p>به موری چون سلیمان کرد بازی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>من از تمکین او جوشی گرفتم</p></div>
<div class="m2"><p>دو عالم را در آغوشی گرفتم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو بر پای ایستادم گفت بنشین</p></div>
<div class="m2"><p>به سوگندم نشاند این منزلت بین</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>قیام خدمتش را نقش بستم</p></div>
<div class="m2"><p>چو گفت اقبال او بنشین نشستم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سخن گفتم چو دولت وقت می‌دید</p></div>
<div class="m2"><p>سخنهائی که دولت می‌پسندید</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>از آن بذله که رضوانش پسندد</p></div>
<div class="m2"><p>زبانی گر به گوش آرد بخندد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نصیحتها که شاهان را بشاید</p></div>
<div class="m2"><p>وصیتها کز او درها گشاید</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بسی پالودهای زعفرانی</p></div>
<div class="m2"><p>به شکر خندشان دادم نهانی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>گهی چون ابرشان گریه گشادم</p></div>
<div class="m2"><p>گهی چو گل نشاط خنده دادم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چنان گفتم که شاه احسنت می‌گفت</p></div>
<div class="m2"><p>خرد بیدار می‌شد جهل می‌خفت</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سماعم ساقیان را کرده مدهوش</p></div>
<div class="m2"><p>مغنی را شه دستان فراموش</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>در آمد راوی و بر خواند چون در</p></div>
<div class="m2"><p>ثنائی کان بساز از گنج شد پر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>حدیثم را چو خسرو گوش می‌کرد</p></div>
<div class="m2"><p>ز شیرینی دهن پر نوش می‌کرد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>حکایت چون به شیرینی در آمد</p></div>
<div class="m2"><p>حدیث خسرو و شیرین بر آمد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>شهنشه دست بر دوشم نهاده</p></div>
<div class="m2"><p>ز تحسین حلقه در گوشم نهاده</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>شکر ریزان همی کرد از عنایت</p></div>
<div class="m2"><p>حدیث خسرو و شیرین حکایت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>که گوهربند بنیادی نهادی</p></div>
<div class="m2"><p>در آن صنعت سخن را داد دادی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>گزارشهای بی‌اندازه کردی</p></div>
<div class="m2"><p>بدان تاریخ ما را تازه کردی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>نه گل دارد بدین تری هوائی</p></div>
<div class="m2"><p>نه بلبل زین نوآئین تر نوائی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گشاده خواندن او بیت بر بیت</p></div>
<div class="m2"><p>رگ مفاوج را چون روغن زیت</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ز طلق اندودگی کامد حریرش</p></div>
<div class="m2"><p>هم آتش دایه شد هم ز مهریرش</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چه حلوا کرده‌ای در جوش این جیش</p></div>
<div class="m2"><p>که هر کو می‌خورد می‌گوید العیش</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>در آن پالوده پالوده چون شیر</p></div>
<div class="m2"><p>ز شیرینی نکردی هیچ تقصیر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>عروسی را بدان شیرین سواری</p></div>
<div class="m2"><p>که بودش برقع شیرین عماری</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چو بر دندان ما کردی حلالش</p></div>
<div class="m2"><p>چه دندان مزد شد با زلف و خالش</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ترا هم بر من و هم بر برادر</p></div>
<div class="m2"><p>معاشی فرض شد چون شیر مادر</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>برادر کو شهنشاه جهان بود</p></div>
<div class="m2"><p>جهان را هم ملک هم پهلوان بود</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بدان نامه که بردی سالها رنج</p></div>
<div class="m2"><p>چه دادت دست مزد از گوهر و گنج</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>شنیدم قرعه‌ای زد بر خلاصت</p></div>
<div class="m2"><p>دو پاره ده نوشت از ملک خاصت</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چه گوئی آن دهت دادند یا نه</p></div>
<div class="m2"><p>مثال ده فرستادند یانه</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>چو دانستم که خواهد فیض دریا</p></div>
<div class="m2"><p>که گردد کار بازرگان مهیا</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>همان خاک خراب آباد گردد</p></div>
<div class="m2"><p>به بند افتاده‌ای آزاد گردد</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>دعای تازه‌ای خواندم چو بختش</p></div>
<div class="m2"><p>به گوهر بر گرفتم پای تختش</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چو بر خواندم دعای دولت شاه</p></div>
<div class="m2"><p>ز بازیهای چرخش کردم آگاه</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>که من یاقوت این تاج مکلل</p></div>
<div class="m2"><p>نه از بهر بها بر بستم اول</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>دری دیدم به کیوان بر کشیده</p></div>
<div class="m2"><p>به بی‌مثلی جهان مثلش ندیده</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>برو نقشی نوشتم تا بماند</p></div>
<div class="m2"><p>دهد بر من در ودی آنکه خواند</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>مرا مقصود ازین شیرین فسانه</p></div>
<div class="m2"><p>دعای خسروان آمد بهانه</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چو شکر خسرو آمد بر زبانم</p></div>
<div class="m2"><p>فسون شکر و شیرین چه خوانم</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>بلی شاه سعید از خاص خویشم</p></div>
<div class="m2"><p>پذیرفت آنچه فرمودی ز پیشم</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>چو بحر عمر او کشتی روان کرد</p></div>
<div class="m2"><p>مرا نه جمله عالم را زیان کرد</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>ولی چون هست شاهی چون تو بر جای</p></div>
<div class="m2"><p>همان شهزادگان کشور آرای</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>از آن پذرفتهای رغبت‌انگیز</p></div>
<div class="m2"><p>دگرباره شود بازار من تیز</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>پذیرفت آن دعا و حمد را شاه</p></div>
<div class="m2"><p>به اخلاصی که بود از دل بدو راه</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چو خو با حمد و با اخلاص من کرد</p></div>
<div class="m2"><p>ده حدونیان را خص من کرد</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>به مملوکی خطی دادم مسلسل</p></div>
<div class="m2"><p>به توقیع قزلشاهی مسجل</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>که شد بخشیده این ده بر تمامی</p></div>
<div class="m2"><p>ز ما برزاد برزاد نظامی</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>به ملک طلق دادم بی‌غرامت</p></div>
<div class="m2"><p>به طلقی ملک او شد تا قیامت</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>کسی کاین راستی را نیست باور</p></div>
<div class="m2"><p>منش خصم و خدایش باد داور</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>اگر طعنی زند بر وی خسیسی</p></div>
<div class="m2"><p>بجز وحشت مباد او را انیسی</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>به لعنت باد تا باشد زمانه</p></div>
<div class="m2"><p>تبارش تیر لعنت را نشانه</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>چو کار افتاده‌ای را کار شد راست</p></div>
<div class="m2"><p>در گنجینه بگشاد و براراست</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>درونم را به تأیید الهی</p></div>
<div class="m2"><p>برونم را به خلعت‌های شاهی</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>چو از تشریف خود منشوریم داد</p></div>
<div class="m2"><p>به طاعت گاه خود دستوریم داد</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>شدم نزدیک شه با بخت مسعود</p></div>
<div class="m2"><p>وزو باز آمدم با تخت محمود</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چنان رفتم که سوی کعبه حجاج</p></div>
<div class="m2"><p>چنان باز آمدم کاحمد ز معراج</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>شنیدم حاسدی زانها که دانی</p></div>
<div class="m2"><p>که دزد کیسه بر باشد نهانی</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>به یوسف صورتی گرگی همی زاد</p></div>
<div class="m2"><p>به لوزینه درون الماس می‌داد</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>که‌ای گیتی نگشته حق شناست</p></div>
<div class="m2"><p>ز بهر چیست چندینی سپاست</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>عروسی کاسمان بوسید پایش</p></div>
<div class="m2"><p>دهی ویرانه باشد رو نمایش؟</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>دهی و آنگه چه ده چون کوره تنگ</p></div>
<div class="m2"><p>که باشد طول و عرضش نیم فرسنگ</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>ندارد دخل و خرجش کیسه‌پرداز</p></div>
<div class="m2"><p>سوادش نیم کار ملک ابخاز</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>چنین دادم جواب حاسد خویش</p></div>
<div class="m2"><p>که نعمت خواره را کفران میندیش</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>چرا می‌باید ای سالوک نقاب</p></div>
<div class="m2"><p>در آن ویرانه افتادن چو مهتاب</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>بحمد من نگر حمدونیان چیست</p></div>
<div class="m2"><p>که یک حمد اینچنین به کانچنان بیست</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>اگر بینی در آن ده کار و کشتی</p></div>
<div class="m2"><p>مرا در هر سخن بینی بهشتی</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>گر او دارد ز دانه خوشه پر</p></div>
<div class="m2"><p>من آرم خوشه خوشه دانه در</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>گر او را ز ابر فیض آب فراتست</p></div>
<div class="m2"><p>مرا در فیض لب آب حیاتست</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>گر او را بیشه‌ای با استواریست</p></div>
<div class="m2"><p>مرا صد بیشه از عود قماریست</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>سپاس من نه از وجه منالست</p></div>
<div class="m2"><p>بدان وجهست کاین وجهی حلالست</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>و گر دارد خرابی سوی او راه</p></div>
<div class="m2"><p>خراب آباد کن بس دولت شاه</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>ز خرواری صدف یک دانه در به</p></div>
<div class="m2"><p>زلال اندک از طوفان پر به</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>نه این ده شاه عالم رای آن داشت</p></div>
<div class="m2"><p>که ده بخشد چو خدمت جای آن داشت</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>ولی چون ملک خرسندیم را دید</p></div>
<div class="m2"><p>ولایت در خور خواهنده بخشید</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>چو من خرسندم و بخشنده خشنود</p></div>
<div class="m2"><p>تو نقد بوالفضولی خرج کن زود</p></div></div>