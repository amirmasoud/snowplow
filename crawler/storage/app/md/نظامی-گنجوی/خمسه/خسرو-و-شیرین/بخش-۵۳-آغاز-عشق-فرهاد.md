---
title: >-
    بخش ۵۳ - آغاز عشق فرهاد
---
# بخش ۵۳ - آغاز عشق فرهاد

<div class="b" id="bn1"><div class="m1"><p>پری‌پیکر نگار پرنیان‌پوش</p></div>
<div class="m2"><p>بت سنگین‌دل سیمین بناگوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن وادی که جایی بود دلگیر</p></div>
<div class="m2"><p>نخوردی هیچ خوردی خوش‌تر از شیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرش صدگونه حلوا پیش بودی</p></div>
<div class="m2"><p>غذاش از مادیان و میش بودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از او تا چارپایان دورتر بود</p></div>
<div class="m2"><p>ز شیر آوردن او را درد سر بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که پیرامون آن وادی به خروار</p></div>
<div class="m2"><p>همه خرزهره بد چون زهره مار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز چوب زهر چون چوپان خبر داشت</p></div>
<div class="m2"><p>چراگاه گله جای دگر داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل شیرین حساب شیر می‌کرد</p></div>
<div class="m2"><p>چه فن سازد در آن تدبیر می‌کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که شیر آوردن از جایی چنان دور</p></div>
<div class="m2"><p>پرستاران او را داشت رنجور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو شب زلف سیاه افکند بر دوش</p></div>
<div class="m2"><p>نهاد از ماه زرین حلقه در گوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن حلقه که بود آن ماه دلسوز</p></div>
<div class="m2"><p>چو مار حلقه می‌پیچید تا روز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشسته پیش او شاپور تنها</p></div>
<div class="m2"><p>فرو کرده ز هر نوعی سخن‌ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از این اندیشه کان سرو سهی داشت</p></div>
<div class="m2"><p>دل فرزانه شاپور آگهی داشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو گلرخ پیش او آن قصه برگفت</p></div>
<div class="m2"><p>نیوشنده چو برگ لاله بشکفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نمازش برد چون هندو پری را</p></div>
<div class="m2"><p>ستودش چون عطارد مشتری را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که هست اینجا مهندس مردی استاد</p></div>
<div class="m2"><p>جوانی نام او فرزانه فرهاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به وقت هندسه عبرت‌نمایی</p></div>
<div class="m2"><p>مجسطی‌دان و اقلیدس‌گشایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به تیشه چون سر صنعت بخارد</p></div>
<div class="m2"><p>زمین را مرغ بر ماهی نگارد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به صنعت سرخ گل را رنگ بندد</p></div>
<div class="m2"><p>به آهن نقش چین بر سنگ بندد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به پیشه دست بوسندش همه روم</p></div>
<div class="m2"><p>به تیشه سنگ خارا را کند موم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به استادی چنین کارت برآید</p></div>
<div class="m2"><p>بدین چشمه گل از خارت برآید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بود هر کار بی‌استاد دشوار</p></div>
<div class="m2"><p>نخست استاد باید آنگهی کار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شود مرد از حساب انگشتری‌گر</p></div>
<div class="m2"><p>ولیک از موم و گل نز آهن و زر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرم فرمان دهی فرمان پذیرم</p></div>
<div class="m2"><p>به دست آوردنش بر دست گیرم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که ما هر دو به چین همزاد بودیم</p></div>
<div class="m2"><p>دو شاگرد از یکی استاد بودیم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو هر مایه که بود از پیشه برداشت</p></div>
<div class="m2"><p>قلم بر من فکند او تیشه برداشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو شاپور این حکایت را به سر برد</p></div>
<div class="m2"><p>غم شیر از دل شیرین بدر برد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو روز آیینه خورشید دربست</p></div>
<div class="m2"><p>شب صد چشم هر صد چشم بربست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تجسس کرد شاپور آن زمین را</p></div>
<div class="m2"><p>به دست آورد فرهاد گزین را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به شادروان شیرین برد شادش</p></div>
<div class="m2"><p>به رسم خواجگان کرسی نهادش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>درآمد کوهکن مانند کوهی</p></div>
<div class="m2"><p>کز او آمد خلایق را شکوهی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو یک پیل از ستبری و بلندی</p></div>
<div class="m2"><p>به مقدار دو پیلش زورمندی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رقیبان حرم بنواختندش</p></div>
<div class="m2"><p>به واجب جایگاهی ساختندش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برون پرده فرهاد ایستاده</p></div>
<div class="m2"><p>میان دربسته و بازو گشاده</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در اندیشه که لعبت‌باز گردون</p></div>
<div class="m2"><p>چه بازی آردش زان پرده بیرون</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جهان ناگه شبیخون سازی‌ای کرد</p></div>
<div class="m2"><p>پس آن پرده لعبت‌بازی‌ای کرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به شیرین‌خنده‌های شکرین‌ساز</p></div>
<div class="m2"><p>درآمد شکر شیرین به آواز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دو قفل شکر از یاقوت برداشت</p></div>
<div class="m2"><p>وز او یاقوت و شکر قوت برداشت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رطب‌هایی که نخلش بار می‌داد</p></div>
<div class="m2"><p>رطب را گوشمال خار می‌داد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به نوش‌آباد آن خرمای در شیر</p></div>
<div class="m2"><p>شکر خواند انگبین را چاشنی‌گیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز بس کز دامن لب شکر افشاند</p></div>
<div class="m2"><p>شکر دامن به خوزستان برافشاند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شنیدم نام او شیرین از آن بود</p></div>
<div class="m2"><p>که در گفتن عجب شیرین‌زبان بود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز شیرینی چه گویم هر چه خواهی</p></div>
<div class="m2"><p>بر آوازش بخفتی مرغ و ماهی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>طبرزد را چو لب پرنوش کردی</p></div>
<div class="m2"><p>ز شکر حلقه‌ها در گوش کردی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در آن مجلس که او لب برگشادی</p></div>
<div class="m2"><p>نبودی تن که حالی جان ندادی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کسی را کان سخن در گوش رفتی</p></div>
<div class="m2"><p>گر افلاطون بدی از هوش رفتی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو بگرفت آن سخن فرهاد در گوش</p></div>
<div class="m2"><p>ز گرمی خون گرفتش در جگر جوش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>برآورد از جگر آهی شغب‌ناک</p></div>
<div class="m2"><p>چو مصروعی ز پای افتاد بر خاک</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به روی خاک می‌غلتید بسیار</p></div>
<div class="m2"><p>وز آن سر کوفتن پیچید چون مار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو شیرین دید کان آرام‌رفته</p></div>
<div class="m2"><p>دلی دارد چو مرغ از دام رفته</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هم از راه سخن شد چاره‌سازش</p></div>
<div class="m2"><p>بدان دانه به دام آورد بازش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>پس آنگه گفت کای داننده استاد</p></div>
<div class="m2"><p>چنان خواهم که گردانی مرا شاد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مراد من چنان است ای هنرمند</p></div>
<div class="m2"><p>که بگشائی دل غمگینم از بند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به چابک‌دستی و استادکاری</p></div>
<div class="m2"><p>کنی در کار این قصر استواری</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گله دور است و ما محتاج شیریم</p></div>
<div class="m2"><p>طلسمی کن که شیر آسان بگیریم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز ما تا گوسفندان یک دو فرسنگ</p></div>
<div class="m2"><p>بباید کند جوئی محکم از سنگ</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>که چوپانانم آنجا شیر دوشند</p></div>
<div class="m2"><p>پرستارانم این جا شیر نوشند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز شیرین گفتن و گفتار شیرین</p></div>
<div class="m2"><p>شده هوش از سر فرهاد مسکین</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سخن‌ها را شنیدن می‌توانست</p></div>
<div class="m2"><p>ولیکن فهم‌کردن می‌ندانست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>زبانش کرد پاسخ را فرامشت</p></div>
<div class="m2"><p>نهاد از عاجزی بر دیده انگشت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>حکایت بازجست از زیردستان</p></div>
<div class="m2"><p>که مستم کوردل باشند مستان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ندانم کوچه می‌گوید بگویید</p></div>
<div class="m2"><p>ز من کامی که می‌جوید بجویید</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>رقیبان آن حکایت برگرفتند</p></div>
<div class="m2"><p>سخن‌هایی که رفت از سر گرفتند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو آگه گشت از آن اندیشه فرهاد</p></div>
<div class="m2"><p>فکند آن حکم را بر دیده بنیاد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>در آن خدمت به غایت چابکی داشت</p></div>
<div class="m2"><p>که کار نازنینان نازکی داشت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>از آنجا رفت بیرون تیشه در دست</p></div>
<div class="m2"><p>گرفت از مهربانی پیشه در دست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چنان از هم درید اندام آن بوم</p></div>
<div class="m2"><p>که می‌شد زیر زخمش سنگ چون موم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به تیشه روی خارا می‌خراشید</p></div>
<div class="m2"><p>چو بید از سنگ مجرا می‌تراشید</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به هر تیشه که بر سنگ آزمودی</p></div>
<div class="m2"><p>دو هم‌سنگش جواهر مزد بودی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به یک ماه از میان سنگ خارا</p></div>
<div class="m2"><p>چو دریا کرد جوئی آشکارا</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ز جای گوسفندان تا در کاخ</p></div>
<div class="m2"><p>دورویه سنگ‌ها زد شاخ در شاخ</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو کار آمد به آخر حوضه‌ای بست</p></div>
<div class="m2"><p>که حوض کوثرش زد بوسه بر دست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چنان ترتیب کرد از سنگ جوئی</p></div>
<div class="m2"><p>که در درزش نمی‌گنجید موئی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>در آن حوضه که کرد او سنگ‌بستش</p></div>
<div class="m2"><p>روان شد آب گفتی زآب‌دستش</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بنا چندان تواند بود دشوار</p></div>
<div class="m2"><p>که بنا را نیاید تیشه در کار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>اگر صد کوه باید کند پولاد</p></div>
<div class="m2"><p>زبون باشد به دست آدمیزاد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چه چاره کان بنی‌آدم نداند</p></div>
<div class="m2"><p>به جز مردن که زان بیچاره ماند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>خبر بردند شیرین را که فرهاد</p></div>
<div class="m2"><p>به ماهی حوضه بست و جوی بگشاد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چنان کز گوسفندان شام و شبگیر</p></div>
<div class="m2"><p>به حوض آید به پای خویشتن شیر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بهشتی‌پیکر آمد سوی آن دشت</p></div>
<div class="m2"><p>بگرد جوی شیر و حوض برگشت</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چنان پنداشت کان حوض گزیده</p></div>
<div class="m2"><p>نکرده‌است آدمی هست آفریده</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بلی باشد ز کار آدمی دور</p></div>
<div class="m2"><p>بهشت و جوی شیر و حوضه و حور</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بسی بر دست فرهاد آفرین کرد</p></div>
<div class="m2"><p>که رحمت بر چنان کس کاین چنین کرد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چو زحمت دور شد نزدیک خواندش</p></div>
<div class="m2"><p>ز نزدیکان خود برتر نشاندش</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>که استادیت را حق چون گذاریم</p></div>
<div class="m2"><p>که ما خود مزد شاگردان نداریم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ز گوهر شب‌چراغی چند بودش</p></div>
<div class="m2"><p>که عقد گوش گوهربند بودش</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ز نغزی هر دری مانند تاجی</p></div>
<div class="m2"><p>وز او هر دانه شهری را خراجی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گشاد از گوش با صد عذر چون نوش</p></div>
<div class="m2"><p>شفاعت کرد کاین بستان و بفروش</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چو وقت آید کزین به دست یابیم</p></div>
<div class="m2"><p>ز حق خدمتت سر برنتابیم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بر آن گنجینه فرهاد آفرین خواند</p></div>
<div class="m2"><p>ز دستش بستد و در پایش افشاند</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>وز آنجا راه صحرا تیز برداشت</p></div>
<div class="m2"><p>چو دریا اشک صحراریز برداشت</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>ز بیم آن‌که کار از نور می‌شد</p></div>
<div class="m2"><p>به صد مردی ز مردم دور می‌شد</p></div></div>