---
title: >-
    بخش ۹۳ - اولین جنبش
---
# بخش ۹۳ - اولین جنبش

<div class="b" id="bn1"><div class="m1"><p>خبر ده کاولین جنبش چه چیز است</p></div>
<div class="m2"><p>که این دانش بر دانا عزیز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوابش داد ما ده راندگانیم</p></div>
<div class="m2"><p>وز اول پرده بیرون ماندگانیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز واپس ماندگان ناید درست این</p></div>
<div class="m2"><p>نخستین را نداند جز نخستین</p></div></div>