---
title: >-
    بخش ۴۸ - آگهی خسرو از مرگ بهرام چوبین
---
# بخش ۴۸ - آگهی خسرو از مرگ بهرام چوبین

<div class="b" id="bn1"><div class="m1"><p>چو شاهنشاه صبح آمد بر اورنگ</p></div>
<div class="m2"><p>سپاه روم زد بر لشگر زنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآمد یوسفی نارنج در دست</p></div>
<div class="m2"><p>ترنج مه زلیخاوار بشکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد از چشم فلک نیرنگ‌سازی</p></div>
<div class="m2"><p>گشاد ابروی‌ها در دلنوازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در پیروزه‌گون گنبد گشادند</p></div>
<div class="m2"><p>به پیروزی جهان را مژده دادند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمانه ایمن از غوغا و فریاد</p></div>
<div class="m2"><p>زمین آسوده از تشنیع و بیداد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به فال فرخ و پیرایه نو</p></div>
<div class="m2"><p>نهاده خسروانی تخت خسرو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سراپرده به سدره سر کشیده</p></div>
<div class="m2"><p>سماطینی به گردون بر کشیده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ستاده قیصر و خاقان و فغفور</p></div>
<div class="m2"><p>یک آماج از بساط پیشگه دور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هر گوشه مهیا کرده جائی</p></div>
<div class="m2"><p>بر او زانو زده کشورخدائی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طرفداران که صف در صف کشیدند</p></div>
<div class="m2"><p>ز هیبت پشت پای خویش دیدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کسی کش در دل آمد سر بریدن</p></div>
<div class="m2"><p>نیارست از سیاست باز دیدن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بس گوهر کمرهای شب‌افروز</p></div>
<div class="m2"><p>در گستاخ بینی بسته بر روز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قبا بسته کمرداران چون پیل</p></div>
<div class="m2"><p>کمربندی زده مقدار ده میل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در آن صف کاتش از بیم آب گشتی</p></div>
<div class="m2"><p>سخن گر زر بدی سیماب گشتی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نشسته خسرو پرویز بر تخت</p></div>
<div class="m2"><p>جوان فر و جوان طبع و جوان بخت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دورویه گرد تخت پادشائیش</p></div>
<div class="m2"><p>کشیده صف غلامان سرائیش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز خاموشی در آن زرینه‌پرگار</p></div>
<div class="m2"><p>شده نقش غلامان نقش دیوار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زمین را زیر تخت آرام داده</p></div>
<div class="m2"><p>به رسم خاص بار عام داده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به فتح‌الباب دولت بامدادان</p></div>
<div class="m2"><p>ز در پیکی در آمد سخت شادان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زمین بوسید و گفتا شادمان باش</p></div>
<div class="m2"><p>همیشه در جهان شاه جهان باش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو زرین بهره باش از تخت زرین</p></div>
<div class="m2"><p>که چوبین بهره شد بهرام چوبین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نشاط از خانه چوبین برون تاخت</p></div>
<div class="m2"><p>که چوبین خانه از دشمن به پرداخت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شهنشاه از دل سنگین ایام</p></div>
<div class="m2"><p>مثل زد بر تن چوبین بهرام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که تا بر ما زمانه چوب‌زن بود</p></div>
<div class="m2"><p>فلک چوبک‌زن چوبینه‌تن بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو چوب دولت ما شد برآور</p></div>
<div class="m2"><p>مه چوبینه چوبین شد به خاور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه این بهرام اگر بهرام گور است</p></div>
<div class="m2"><p>سرانجام از جهانش بهره گور است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر بهرام گوری رفت ازین دام</p></div>
<div class="m2"><p>بیا تا بنگری صد گور بهرام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر بهرام گوری رفت ازین دام</p></div>
<div class="m2"><p>بیا تا بنگری صد گور بهرام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جهان تا در جهان یاریش می‌کرد</p></div>
<div class="m2"><p>تمنای جهانداریش می‌کرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کجا آن شیر کز شمشیرگیری</p></div>
<div class="m2"><p>چو مستان کرد با ما شیرگیری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کجا آن تیغ کاتش در جهان زد</p></div>
<div class="m2"><p>تپانچه بر درفش کاویان زد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بسا فرزانه را کو شیرزاد است</p></div>
<div class="m2"><p>فریب خاکیان بر باد داده‌است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بسا گرگ جوان کز روبه پیر</p></div>
<div class="m2"><p>به افسون بسته شد در دام نخجیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از آن بر گرگ روبه راست شاهی</p></div>
<div class="m2"><p>که روبه دام بیند گرگ ماهی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بسا شه کز فریب یافه‌گویان</p></div>
<div class="m2"><p>خصومت را شود بی‌وقت جویان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سرانجام از شتاب خام تدبیر</p></div>
<div class="m2"><p>به جای پرنیان بر دل نهد تیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز مغروری کلاه از سر شود دور</p></div>
<div class="m2"><p>مبادا کس به زور خویش مغرور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چراغ ارچه ز روغن نور گیرد</p></div>
<div class="m2"><p>بسا باشد که از روغن بمیرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خورش‌ها را نمک رو تازه دارد</p></div>
<div class="m2"><p>نمک باید که نیز اندازه دارد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مخور چندان که خرما خار گردد</p></div>
<div class="m2"><p>گوارش در دهن مردار گردد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چنان خور کز ضرورت‌های حالت</p></div>
<div class="m2"><p>حرام دیگران باشد حلالت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مقیمی را که این دروازه باید</p></div>
<div class="m2"><p>غم و شادیش را اندازه باید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مجو بالاتر از دوران خود جای</p></div>
<div class="m2"><p>مکش بیش از گلیم خویشتن پای</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو دریا بر مزن موجی که داری</p></div>
<div class="m2"><p>مپر بالاتر از اوجی که داری</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به قدر شغل خود باید زدن لاف</p></div>
<div class="m2"><p>که زردوزی نداند بوریاباف</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چه نیکو داستانی زد هنرمند</p></div>
<div class="m2"><p>هلیله با هلیله قند با قند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نه فرخ شد نهاد نو نهادن</p></div>
<div class="m2"><p>ره و رسم کهن بر باد دادن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به قندیل قدیمان در زدن سنگ</p></div>
<div class="m2"><p>به کالای یتیمان بر زدن چنگ</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هر آنکو کشت تخمی کشته بر داد</p></div>
<div class="m2"><p>نه من گفتم که دانه زو خبر داد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نه هر تخمی درختی راست روید</p></div>
<div class="m2"><p>نه هر رودی سرودی راست گوید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به سرهنگی حمایل کردن تیغ</p></div>
<div class="m2"><p>بسا مه را که پوشد چهره در میغ</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تو خونریزی مبین کو شیر گیرد</p></div>
<div class="m2"><p>که خونش گیرد ارچه دیر گیرد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از این ابلق سوار نیم زنگی</p></div>
<div class="m2"><p>که در زیر ابلقی دارد دو رنگی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مباش ایمن که باخوی پلنگ است</p></div>
<div class="m2"><p>کجا یکدل شود آخر دو رنگ است</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ستم در مذهب دولت روا نیست</p></div>
<div class="m2"><p>که دولت با ستمگار آشنا نیست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>خری در کاهدان افتاد ناگاه</p></div>
<div class="m2"><p>نگویم وای بر خر وای بر کاه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مگس بر خوان حلوا کی کند پشت</p></div>
<div class="m2"><p>به انجیری غرابی چون توان کشت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به سیم دیگران زرین مکن کاخ</p></div>
<div class="m2"><p>کزین دین رخنه گردد کیسه سوراخ</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نگه دار اندرین آشفته بازار</p></div>
<div class="m2"><p>کدین گازر از نارج عطار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>مشو خامش چو کار افتد به زاری</p></div>
<div class="m2"><p>که باشد خامشی نوعی ز خواری</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>شنیدستم که در زنجیر عامان</p></div>
<div class="m2"><p>یکی بود است ازین آشفته‌نامان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو با او ساختی نابالغی جنگ</p></div>
<div class="m2"><p>به بالغ‌تر کسی برداشتی سنگ</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بپرسیدند کز طفلان خوری خار</p></div>
<div class="m2"><p>ز پیران کین کشی چون باشد این کار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بخنده گفت اگر پیران نخندند</p></div>
<div class="m2"><p>کجا طفلان ستمکاری پسندند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو دست از پای ناخشنود باشد</p></div>
<div class="m2"><p>به جرم پای سر مأخوذ باشد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به جباری مبین در هیچ درویش</p></div>
<div class="m2"><p>که او هم محتشم باشد بر خویش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ز عیب نیک مردم دیده بردوز</p></div>
<div class="m2"><p>هنر دیدن ز چشم بد میاموز</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>هنر بیند چو عیب این چشم جاسوس</p></div>
<div class="m2"><p>تو چشم زاغ بین نه پای طاووس</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ترا حرفی به صد تزویر در مشت</p></div>
<div class="m2"><p>منه بر حرف کس بیهوده انگشت</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به عیب خویش یک دیده نمائی؟</p></div>
<div class="m2"><p>به عیب دیگران صد صد گشائی ؟</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نه کم ز آیینه‌ای در عیب‌جوئی</p></div>
<div class="m2"><p>به آیینه رها کن سخت‌روئی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>حفاظ آینه این یک هنر بس</p></div>
<div class="m2"><p>که پیش کس نگوید غیبت کس</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو سایه روسیاه آنکس نشیند</p></div>
<div class="m2"><p>که واپس گوید آنچ از پیش بیند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نشاید دید خصم خویش را خرد</p></div>
<div class="m2"><p>که نرد از خام‌دستان کم توان برد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>مشو غره بر آن خرگوش زرفام</p></div>
<div class="m2"><p>که بر خنجر نگارد مرد رسام</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>که چون شیران بدان خنجر ستیزند</p></div>
<div class="m2"><p>بدو خون بسی خرگوش ریزند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>در آب نرم‌رو منگر به خواری</p></div>
<div class="m2"><p>که تند آید گه زنهارخواری</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بر آتش دل منه کو رخ فروزد</p></div>
<div class="m2"><p>که وقت آید که صد خرمن بسوزد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به گستاخی مبین در خنده شیر</p></div>
<div class="m2"><p>که نه دندان نماید بلکه شمشیر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>هر آنکس کو زند لاف دلیری</p></div>
<div class="m2"><p>ز جنگ شیر یابد نام شیری</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چو کین‌خواهی ز خسرو کرد بهرام</p></div>
<div class="m2"><p>ز کین خسروان خسرو شدش نام</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>به ار با کم ز خود خود را نسنجی</p></div>
<div class="m2"><p>کز افکندن وز افتادن برنجی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ستیزه با بزرگان به توان برد</p></div>
<div class="m2"><p>که از همدستی خردان شوی خرد</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>نهنگ آن به که در دریا ستیزد</p></div>
<div class="m2"><p>کز آب خرد ماهی خرد خیزد</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چو خسرو گفت بسیاری درین باب</p></div>
<div class="m2"><p>بزرگان ریختند از دیدگان آب</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>فرود آمد ز تخت آن روز دلتنگ</p></div>
<div class="m2"><p>روان کرده ز نرگس آب گلرنگ</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>سه روز اندوه خورد از بهر بهرام</p></div>
<div class="m2"><p>نه با تخت آشنا می‌شد و نه با جام</p></div></div>