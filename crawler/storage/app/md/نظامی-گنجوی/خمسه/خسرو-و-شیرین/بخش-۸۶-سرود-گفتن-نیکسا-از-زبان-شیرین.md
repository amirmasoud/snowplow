---
title: >-
    بخش ۸۶ - سرود گفتن نیکسا از زبان شیرین
---
# بخش ۸۶ - سرود گفتن نیکسا از زبان شیرین

<div class="b" id="bn1"><div class="m1"><p>نکیسا در ترنم جادوی ساخت</p></div>
<div class="m2"><p>پس آنگه این غزل در راهوی ساخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بساز ای یار با یاران دلسوز</p></div>
<div class="m2"><p>که دی رفت و نخواهد ماند امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گره بگشای با ما بستگی چند</p></div>
<div class="m2"><p>شتاب عمر بین آهستگی چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز یاری حکم کن تا شهریاری</p></div>
<div class="m2"><p>ندارد هیچ بنیاد استواری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به روزی چند با این سست رختی</p></div>
<div class="m2"><p>بدین سختی چه باید کرد سختی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به عمری کو بود پنجاه یا شصت</p></div>
<div class="m2"><p>چه باید صد گره بر جان خود بست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسا تا به که ماند از طیرگی سرد</p></div>
<div class="m2"><p>بسا سکبا که سگبان پخت و سگ خورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوش آن باشد که امشب باده نوشیم</p></div>
<div class="m2"><p>امان باشد؟ که فردا باز کوشیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بر فردا نماند امیدواری</p></div>
<div class="m2"><p>بباید کردن امشب سازگاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان بسیار شب بازی نمودست</p></div>
<div class="m2"><p>جهان نادیده‌ای جانا چه سودست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهاری داری ازوی بر خور امروز</p></div>
<div class="m2"><p>که هر فصلی نخواهد بود نوروز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گلی کو را نبوید آدمی زاد</p></div>
<div class="m2"><p>چو هنگام خزان آید برد باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گل آن بهتر کزو گلاب خیزد</p></div>
<div class="m2"><p>گلابی گر گذارد گل بریزد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در آن حضرت که نام زر سفالست</p></div>
<div class="m2"><p>چو من مس در حساب آید محالست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لب دریا و آنگه قطره آب</p></div>
<div class="m2"><p>رخ خورشید و آنگه کرم شبتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو بازار تو هست از نیکوی تیز</p></div>
<div class="m2"><p>کسادی را چو من رونق برانگیز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بخر کالای کاسد تا توانی</p></div>
<div class="m2"><p>به کار آید یکی روزت چه دانی؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درستی گرچه دارد کار و باری</p></div>
<div class="m2"><p>شکسته بسته نیز آید به کاری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر چه زر به مهر افزون عیارست</p></div>
<div class="m2"><p>قراضه ریزها هم در شمارست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نهادستی ز عشقم حلقه در گوش</p></div>
<div class="m2"><p>بدین عیبم خریدی باز مفروش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تمنای من از عمر و جوانی</p></div>
<div class="m2"><p>وصال تست وانگه زندگانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به پیغامی ز تو راضی است گوشم</p></div>
<div class="m2"><p>بر آیم زنی اگر زین بیش کوشم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>منم در پای عشقت رفته از دست</p></div>
<div class="m2"><p>به خلوت خورده می تنها شده مست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>منم آن سایه کز بالا و از زیر</p></div>
<div class="m2"><p>ز پایت سر نگردانم به شمشیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نگردم از تو تابی سر نگردم</p></div>
<div class="m2"><p>ز تو تا در نگردم برنگردم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سخن تا چند گویم با خیالت</p></div>
<div class="m2"><p>برون رانم جنیبت با جمالت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بهر سختی که تا اکنون نمودم</p></div>
<div class="m2"><p>چو لحن مطربان در پرده بودم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کنون در پرده خون خواهم افتاد</p></div>
<div class="m2"><p>چو برق از پرده بیرون خواهم افتاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چراغ از دیده چندان روی پوشد</p></div>
<div class="m2"><p>که دیگ روغنش ز آتش نجوشد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بخسبانم ترا من می خورم ناب</p></div>
<div class="m2"><p>که من سرمست خوش باشم تو در خواب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بجای توتیا گردت ستانم</p></div>
<div class="m2"><p>گهی بوسه گهی دردت ستانم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سر زلفت به گیسو باز بندم</p></div>
<div class="m2"><p>گهی گریم ز عشقت گاه خندم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چنان بندم به دل نقش نگینت</p></div>
<div class="m2"><p>که بر دستت نداند آستینت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در آغوش آنچنان گیرم تنت را</p></div>
<div class="m2"><p>که نبود آگهی پیراهنت را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو لعبت باز شب پنهان کند راز</p></div>
<div class="m2"><p>من اندر پرده چون لعبت شوم باز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر از دستم چنین کاری بر آید</p></div>
<div class="m2"><p>ز هر خاریم گلزاری بر آید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خدایا ره به پیروزیم گردان</p></div>
<div class="m2"><p>چنین پیروزیی روزیم گردان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو خسرو گوش کرد این بیت چالاک</p></div>
<div class="m2"><p>ز حالت کرد حالی جامه را چاک</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به صد فریاد گفت ای باربد هان</p></div>
<div class="m2"><p>قوی کن جان من در کالبدهان</p></div></div>