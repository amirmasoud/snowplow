---
title: >-
    بخش ۲۵ - دیدن خسرو شیرین را در چشمه سار
---
# بخش ۲۵ - دیدن خسرو شیرین را در چشمه سار

<div class="b" id="bn1"><div class="m1"><p>سخن گوینده پیر پارسی خوان</p></div>
<div class="m2"><p>چنین گفت از ملوک پارسی دان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چون خسرو به ارمن کس فرستاد</p></div>
<div class="m2"><p>به پرسش کردن آن سرو آزاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب و روز انتظار یار می‌داشت</p></div>
<div class="m2"><p>امید وعده دیدار می‌داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شام و صبح اندر خدمت شاه</p></div>
<div class="m2"><p>کمر می‌بست چون خورشید و چون ماه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو تخت آرای شد طرف کلاهش</p></div>
<div class="m2"><p>ز شادی تاج سر می‌خواند شاهش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرامی بود بر چشم جهاندار</p></div>
<div class="m2"><p>چنین تا چشم زخم افتاد در کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که از پولاد کاری خصم خونریز</p></div>
<div class="m2"><p>درم را سکه زد بر نام پرویز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به هر شهری فرستاد آن درم را</p></div>
<div class="m2"><p>بشورانید از آن شاه عجم را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بیم سکه و نیروی شمشیر</p></div>
<div class="m2"><p>هراسان شد کهن گرگ از جوان شیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان پنداشت آن منصوبه را شاه</p></div>
<div class="m2"><p>که خسرو باخت آن شطرنج ناگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر آن دلشد که لعبی چند سازد</p></div>
<div class="m2"><p>بگیرد شاه نو را بند سازد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حسابی بر گرفت از روی تدبیر</p></div>
<div class="m2"><p>نبود آگه ز بازیهای تقدیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که نتوان راه خسرو را گرفتن</p></div>
<div class="m2"><p>نه در عقده مه نو را گرفتن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو هر کو راستی در دل پذیرد</p></div>
<div class="m2"><p>جهان گیرد جهان او را نگیرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بزرگ امید ازین معنی خبر یافت</p></div>
<div class="m2"><p>شه نو را به خلوت جست و دریافت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حکایت کرد کاختر در وبالست</p></div>
<div class="m2"><p>ملک را با تو قصد گوشمالست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بباید زفت روزی چند ازین پیش</p></div>
<div class="m2"><p>شتاب آوردن و بردن سر خویش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مگر کاین آتشت بی‌دود گردد</p></div>
<div class="m2"><p>وبال اخترت مسعود گردد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو خسرو دید کاشوب زمانه</p></div>
<div class="m2"><p>هلاکش را همی سازد بهانه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به مشگو رفت پیش مشگ مویان</p></div>
<div class="m2"><p>وصیت کرد با آن ماهرویان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که می‌خواهم خرامیدن به نخجیر</p></div>
<div class="m2"><p>دو هفته بیش و کم زین کاخ دلگیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شما خندان و خرم دل نشینید</p></div>
<div class="m2"><p>طرب سازید و روی غم نبینید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر آید نار پستانی در این باغ</p></div>
<div class="m2"><p>چو طاووسی نشسته بر پر زاغ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فرود آرید کان مهمان عزیز است</p></div>
<div class="m2"><p>شما ماهید و خورشید آن کنیز است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بمانیدش که تا بیغم نشیند</p></div>
<div class="m2"><p>طرب می‌سازد و شادی گزیند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>و گر تنگ آید از مشکوی خضرا</p></div>
<div class="m2"><p>چو خضر آهنگ سازد سوی صحرا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در آن صحرا که او خواهد بتازید</p></div>
<div class="m2"><p>بهشتی روی را قصری بسازید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدان صورت که دل دادش گوائی</p></div>
<div class="m2"><p>خبر می‌داد از الهام خدائی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو گفت این قصه بیرون رفت چون باد</p></div>
<div class="m2"><p>سلیمان وار با جمعی پریزاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زمین کن کوه خود را گرم کرده</p></div>
<div class="m2"><p>سوی ارمن زمین را نرم کرده</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز بیم شاه می‌شد دل پر از درد</p></div>
<div class="m2"><p>دو منزل را به یک منزل همی کرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قضا را اسبشان در راه شد سست</p></div>
<div class="m2"><p>در آن منزل که آن مه موی می‌شست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>غلامان را بفرمود ایستادن</p></div>
<div class="m2"><p>ستوران را علوفه برنهادن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تن تنها ز نزدیک غلامان</p></div>
<div class="m2"><p>سوی آن مرغزار آمد خرامان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>طوافی زد در آن فیروزه گلشن</p></div>
<div class="m2"><p>میان گلشن آبی دید روشن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو طاووسی عقابی باز بسته</p></div>
<div class="m2"><p>تذروی بر لب کوثر نشسته</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گیا را زیر نعل آهسته می‌سفت</p></div>
<div class="m2"><p>در آن آهستگی آهسته می‌گفت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر این بت جان بودی چه بودی</p></div>
<div class="m2"><p>ور این اسب آن من بودی چه بودی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نبود آگه که آن شبرنگ و آن ماه</p></div>
<div class="m2"><p>به برج او فرود آیند ناگاه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بسا معشوق کاید مست بر در</p></div>
<div class="m2"><p>سبل در دیده باشد خواب در سر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بسا دولت که آید بر گذرگاه</p></div>
<div class="m2"><p>چو مرد آگه نباشد گم کند راه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز هر سو کرد بر عادت نگاهی</p></div>
<div class="m2"><p>نظر ناگه در افتادش به ماهی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو لختی دید از آن دیدن خطر دید</p></div>
<div class="m2"><p>که بیش آشفته شد تا بیشتر دید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>عروسی دید چون ماهی مهیا</p></div>
<div class="m2"><p>که باشد جای آن مه بر ثریا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نه ماه آیینهٔ سیماب داده</p></div>
<div class="m2"><p>چو ماه نخشب از سیماب زاده</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در آب نیلگون چون گل نشسته</p></div>
<div class="m2"><p>پرندی نیلگون تا ناف بسته</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همه چشمه ز جسم آن گل اندام</p></div>
<div class="m2"><p>گل بادام و در گل مغز بادام</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>حواصل چون بود در آب چون رنگ؟</p></div>
<div class="m2"><p>همان رونق در او از آب و از رنگ</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز هر سو شاخ گیسو شانه می‌کرد</p></div>
<div class="m2"><p>بنفشه بر سر گل دانه می‌کرد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اگر زلفش غلط می‌کرد کاری</p></div>
<div class="m2"><p>که دارم در بن هر موی ماری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نهان با شاه می‌گفت از بنا گوش</p></div>
<div class="m2"><p>که مولای توام هان حلقه در گوش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو گنجی بود گنجش کیمیاسنج</p></div>
<div class="m2"><p>به بازی زلف او چون مار بر گنج</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>فسونگر مار را نگرفته در مشت</p></div>
<div class="m2"><p>گمان بردی که مار افسای را کشت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کلید از دست بستانبان فتاده</p></div>
<div class="m2"><p>ز بستان نار پستان در گشاده</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دلی کان نار شیرین کار دیده</p></div>
<div class="m2"><p>ز حسرت گشته چون نار کفیده</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بدان چشمه که جای ماه گشته</p></div>
<div class="m2"><p>عجب بین کافتاب از راه گشته</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو بر فرق آب می‌انداخت از دست</p></div>
<div class="m2"><p>فلک بر ماه مروارید می بست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تنش چون کوه برفین تاب می‌داد</p></div>
<div class="m2"><p>ز حسرت شاه را برفاب می‌داد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شه از دیدار آن بلور دلکش</p></div>
<div class="m2"><p>شده خورشید یعنی دل پر آتش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>فشاند از دیده باران سحابی</p></div>
<div class="m2"><p>که طالع شد قمر در برج آبی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سمنبر غافل از نظاره شاه</p></div>
<div class="m2"><p>که سنبل بسته بد بر نرگسش راه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو ماه آمد برون از ابر مشگین</p></div>
<div class="m2"><p>به شاهنشه در آمد چشم شیرین</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>همائی دید بر پشت تذروی</p></div>
<div class="m2"><p>به بالای خدنگی رسته سروی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز شرم چشم او در چشمه آب</p></div>
<div class="m2"><p>همی لرزی چون در چشمه مهتاب</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>جز این چاره ندید آن چشمه قند</p></div>
<div class="m2"><p>که گیسو را چو شب بر مه پراکند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>عبیر افشاند بر ماه شب افروز</p></div>
<div class="m2"><p>به شب خورشید می‌پوشید در روز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سوادی بر تن سیمین زد از بیم</p></div>
<div class="m2"><p>که خوش باشد سواد نقش بر سیم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دل خسرو بر آن تابنده مهتاب</p></div>
<div class="m2"><p>چنان چون زر در آمیزد به سیماب</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ولی چون دید کز شیر شکاری</p></div>
<div class="m2"><p>بهم در شد گوزن مرغزاری</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>زبون‌گیری نکرد آن شیر نخجیر</p></div>
<div class="m2"><p>که نبود شیر صیدافکن زبون گیر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به صبری کاورد فرهنگ در هوش</p></div>
<div class="m2"><p>نشاند آن آتش جوشنده را جوش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>جوانمردی خوش آمد را ادب کرد</p></div>
<div class="m2"><p>نظرگاهش دگر جائی طلب کرد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به گرد چشمه دل را دانه می‌کاشت</p></div>
<div class="m2"><p>نظر جای دگر بیگانه می‌داشت</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دو گل بین کز دو چشمه خار دیدند</p></div>
<div class="m2"><p>دو تشنه کز دو آب آزار دیدند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>همان را روز اول چشمه زد راه</p></div>
<div class="m2"><p>همین از چشمه‌ای افتاد در چاه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به سرچشمه گشاید هر کسی رخت</p></div>
<div class="m2"><p>به چشمه نرم گردد توشه سخت</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>جز ایشان را که رخت از چشمه بردند</p></div>
<div class="m2"><p>ز نرمیها به سختیها سپردند</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>نه بینی چشمه‌ای کز آتش دل</p></div>
<div class="m2"><p>ندارد تشنه‌ای را پای در گل</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نه خورشید جهان کاین چشمه خون</p></div>
<div class="m2"><p>بدین کار است گردان گرد گردون</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو شه می‌کرد مه را پرده‌داری</p></div>
<div class="m2"><p>که خاتون برد نتوان بی‌عماری</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>برون آمد پریرخ چون پری تیز</p></div>
<div class="m2"><p>قبا پوشید و شد بر پشت شبدیز</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>حسابی کرد با خود کاین جوانمرد</p></div>
<div class="m2"><p>که زد بر گرد من چون چرخ ناورد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>شگفت آید مرا گر یار من نیست</p></div>
<div class="m2"><p>دلم چون برد اگر دلدار من نیست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>شنیدم لعل در لعل است کانش</p></div>
<div class="m2"><p>اگر دلدار من شد کو نشانش</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>نبود آگه که شاهان جامه راه</p></div>
<div class="m2"><p>دگرگونه کنند از بیم بدخواه</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>هوای دل رهش می‌زد که برخیز</p></div>
<div class="m2"><p>گل خود را بدین شکر برآمیز</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گر آن صورت بد این رخشنده جانست</p></div>
<div class="m2"><p>خبر بود آن واین باری عیانست</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>دگر ره گفت از این ره روی برتاب</p></div>
<div class="m2"><p>روا نبود نمازی در دو محراب</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ز یک دوران دو شربت خورد نتوان</p></div>
<div class="m2"><p>دو صاحب را پرستش کرد نتوان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>و گر هست این جوان آن نازنین شاه</p></div>
<div class="m2"><p>نه جای پرسش است او را در این راه</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>مرا به کز درون پرده بیند</p></div>
<div class="m2"><p>که بر بی‌پردگان گردی نشیند</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>هنوز از پرده بیرون نیست این کار</p></div>
<div class="m2"><p>ز پرده چون برون آیم بیکبار</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>عقاب خویش را در پویه پر داد</p></div>
<div class="m2"><p>ز نعلش گاو و ماهی را خبر داد</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>تک از باد صبا پیشی گرفته</p></div>
<div class="m2"><p>به جنبش با فلک خویشی گرفته</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>پری را می‌گرفت از گرم خیزی</p></div>
<div class="m2"><p>به چشم دیو در می‌شد ز تیزی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>پس از یک لحضه خسرو باز پس دید</p></div>
<div class="m2"><p>به جز خود ناکسم گر هیچکس دید</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>ز هر سو کرد مرکب را روانه</p></div>
<div class="m2"><p>نه دل دید و نه دلبر در میانه</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>فرود آمد بدان چشمه زمانی</p></div>
<div class="m2"><p>ز هر سو جست از آن گوهرنشانی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>شگفت آمد دلش را کاین چنین تیز</p></div>
<div class="m2"><p>بدین زودی کجا رفت آن دلاویز</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>گهی سوی درختان دید گستاخ</p></div>
<div class="m2"><p>که گوئی مرغ شد پرید بر شاخ</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>گهی دیده به آب چشمه می‌شست</p></div>
<div class="m2"><p>چو ماهی ماه را در آب می‌جست</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>زمانی پل بر آب چشم بستی</p></div>
<div class="m2"><p>گهی بر آب چشمه پل شکستی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ز چشمش برده آن چشمه سیاهی</p></div>
<div class="m2"><p>در او غلطید چون در چشمه ماهی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چنان نالید کز بس نالش او</p></div>
<div class="m2"><p>پشیمان شد سپهر از مالش او</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>مه و شبدیز را در باغ می‌جست</p></div>
<div class="m2"><p>به چشمی باز و چشمی زاغ می‌جست</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>ز هر سو حمله بر چون باز نخجیر</p></div>
<div class="m2"><p>که زاغی کرد بازش را گرو گیر</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>از آن زاغ سبک پر مانده پر داغ</p></div>
<div class="m2"><p>جهان تاریک بروی چون پر زاغ</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>شده زاغ سیه باز سپیدش</p></div>
<div class="m2"><p>درخت خار گشته مشک بیدش</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>ز بیدش گربه بید انجیر کرده</p></div>
<div class="m2"><p>سرشگش تخم بید انجیر خورده</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>خمیده بیدش از سودای خورشید</p></div>
<div class="m2"><p>بلی رسم است چوگان کردن از بید</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>بر آورد از جگر سوزنده آهی</p></div>
<div class="m2"><p>که آتش در چو من مردم گیاهی</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بهاری یافتم زو بر نخوردم</p></div>
<div class="m2"><p>فراتی دیدم و لب تر نکردم</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>به نادانی ز گوهر داشتم چنگ</p></div>
<div class="m2"><p>کنون می‌بایدم بر دل زدن سنگ</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>گلی دیدم نچیدم بامدادش</p></div>
<div class="m2"><p>دریغا چون شب آمد برد بادش</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>در آبی نرگسی دیدم شکفته</p></div>
<div class="m2"><p>چو آبی خفته وز او آب خفته</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>شنیدم کاب خفتد زر شود خاک</p></div>
<div class="m2"><p>چرا سیماب گشت آن سرو چالاک</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>همائی بر سرم می‌داد سایه</p></div>
<div class="m2"><p>سریرم را ز گردون کرد پایه</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>بر آن سایه چو مه دامن فشاندم</p></div>
<div class="m2"><p>چو سایه لاجرم بی سنگ ماندم</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>نمد زینم نگردد خشک از این خون</p></div>
<div class="m2"><p>بترزینم تبر زین چون بود چون</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>برون آمد گلی از چشمه آب</p></div>
<div class="m2"><p>نمی‌گویم به بیداری که در خواب</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>کنون کان چشمه را با گل نه بینم</p></div>
<div class="m2"><p>چو خار آن به که بر آتش نشینم</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>که فرمودم که روی از مه بگردان</p></div>
<div class="m2"><p>چو بخت آمد به راهت ره بگردان</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>کدامین دیو طبعم را بر این داشت</p></div>
<div class="m2"><p>که از باغ ارم بگذشت و بگذاشت</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>همه جائی شکیبائی ستودست</p></div>
<div class="m2"><p>جز این یکجا که صید از من ربودست</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>چو برق از جان چراغی برفروزم</p></div>
<div class="m2"><p>شکیب خام را بر وی بسوزم</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>اگر من خوردمی زان چشمه آبی</p></div>
<div class="m2"><p>نبایستی ز دل کردن کبابی</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>نصیحت بین که آن هندو چه فرمود</p></div>
<div class="m2"><p>که چون مالی بیابی زود خور زود</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>در این باغ از گل سرخ و گل زرد</p></div>
<div class="m2"><p>پشیمانی نخورد آنکس که برخورد</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>من وزین پس جگر در خون کشیدن</p></div>
<div class="m2"><p>ز دل پیکان غم بیرون کشیدن</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>زنم چندان طپانچه بر سر و روی</p></div>
<div class="m2"><p>که یارب یاربی خیزد ز هر موی</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>مگر کاسوده‌تر گردم در این درد</p></div>
<div class="m2"><p>تنور آتشم لختی سود سرد</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>ز بحر دیده چندان در ببارم</p></div>
<div class="m2"><p>که جز گوهر نباشد در کنارم</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>کسی کاو را ز خون آماس خیزد</p></div>
<div class="m2"><p>کی آسوده شود تا خون نریزد</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>زمانی گشت گرد چشمه نالان</p></div>
<div class="m2"><p>به گریه دستها بر چشم مالان</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>زمانی بر زمین افتاد مدهوش</p></div>
<div class="m2"><p>گرفت آن چشمه را چون گل در آغوش</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>از آن سرو روان کز چنگ رفته</p></div>
<div class="m2"><p>ز سروش آب و از گل رنگ رفته</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>سهی سروش فتاده بر سر خاک</p></div>
<div class="m2"><p>شده لرزان چنان کز باد خاشاک</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>به دل گفتا گر این ماه آدمی بود</p></div>
<div class="m2"><p>کجا آخر قدمگاهش زمی بود</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>و گر بود او پری دشوار باشد</p></div>
<div class="m2"><p>پری بر چشمه‌ها بسیار باشد</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>به کس نتوان نمود این داوری را</p></div>
<div class="m2"><p>که خسرو دوست می‌دارد پریرا</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>مرا زین کار کامی برنخیزد</p></div>
<div class="m2"><p>پری پیوسته از مردم گریزد</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>به جفت مرغ آبی باز کی شد</p></div>
<div class="m2"><p>پری با آدمی دمساز کی شد</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>سلیمانم بباید نام کردن</p></div>
<div class="m2"><p>پس آنگاهی پری را رام کردن</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>ازین اندیشه لختی باز می‌گفت</p></div>
<div class="m2"><p>حکایت‌های دلپرداز می‌گفت</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>به نومیدی دل از دلخواه برداشت</p></div>
<div class="m2"><p>به دارالملک ارمن راه برداشت</p></div></div>