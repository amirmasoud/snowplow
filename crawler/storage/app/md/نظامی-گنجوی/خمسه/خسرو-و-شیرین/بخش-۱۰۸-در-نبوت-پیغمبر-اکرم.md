---
title: >-
    بخش ۱۰۸ - در نبوت پیغمبر اکرم
---
# بخش ۱۰۸ - در نبوت پیغمبر اکرم

<div class="b" id="bn1"><div class="m1"><p>سخن چون شد به معصومان حوالت</p></div>
<div class="m2"><p>ملک پرسیدش از تاج رسالت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که شخصی در عرب دعوی کند کیست؟</p></div>
<div class="m2"><p>به نسبت دین او با دین ما چیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوابش داد کان حرف الهی</p></div>
<div class="m2"><p>برونست از سپیدی و سیاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گنبد در کنند این قوم ناورد</p></div>
<div class="m2"><p>برون از گنبد است آواز آن مرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه ز انجم گوید ونز چرخ اعلاش</p></div>
<div class="m2"><p>که نقشند این دو او شاگرد نقاش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کند بالای این نه پرده پرواز</p></div>
<div class="m2"><p>نیم زان پرده چون گویم از این راز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن بازی شها با دین تازی</p></div>
<div class="m2"><p>که دین حق است و با حق نیست بازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بجوشید از نهیب اندام پرویز</p></div>
<div class="m2"><p>چو اندام کباب از آتش تیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ولی چون بخت پیروزی نبودش</p></div>
<div class="m2"><p>صلای احمدی روزی نبودش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو شیرین دیدکان دیرینه استاد</p></div>
<div class="m2"><p>در گنج سخن بر شاه بگشاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ثنا گفتش که‌ای پیر یگانه</p></div>
<div class="m2"><p>ندیده چون توئی چشم زمانه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو بر خسرو گشادی گنج کانی</p></div>
<div class="m2"><p>نصیبی ده مرا نیز ار توانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کلیدی کن نه زنجیری در این بند</p></div>
<div class="m2"><p>فرو خوان از کلیله نکته‌ای چند</p></div></div>