---
title: >-
    بخش ۴۰ - افسانه‌سرائی ده دختر
---
# بخش ۴۰ - افسانه‌سرائی ده دختر

<div class="b" id="bn1"><div class="m1"><p>فرنگیس اولین مرکب روان کرد</p></div>
<div class="m2"><p>که دولت در زمین گنجی نهان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن دولت فریدونی خبر داشت</p></div>
<div class="m2"><p>زمین را باز کرد آن گنج برداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سهیل سیم‌تن گفتا تذروی</p></div>
<div class="m2"><p>به بازی بود در پائین سروی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرود آمد یکی شاهین به شبگیر</p></div>
<div class="m2"><p>تذرو نازنین را کرد نخجیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجب‌نوش شکر پاسخ چنین گفت</p></div>
<div class="m2"><p>که عنبربو گلی در باغ بشکفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهشتی مرغی آمد سوی گلزار</p></div>
<div class="m2"><p>ربود آن عنبرین گل را به منقار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن به داستانی زد فلکناز</p></div>
<div class="m2"><p>که ما را بود یک چشم از جهان باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به ما چشمی دگر کرد آشنائی</p></div>
<div class="m2"><p>دو به بیند ز چشمی روشنائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همیلا گفت آبی بود روشن</p></div>
<div class="m2"><p>روان گشته میان سبز گلشن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جوان شیری بر آمد تشنه از راه</p></div>
<div class="m2"><p>بدان چشمه دهان‌ تر کرد ناگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همایون گفت لعلی بود کانی</p></div>
<div class="m2"><p>ز غارتگاه بیاعان نهانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در آمد دولت شاهی به تاراج</p></div>
<div class="m2"><p>نهاد آن لعل را بر گوشه تاج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سمن‌ترک سمن‌بر گفت یک روز</p></div>
<div class="m2"><p>جدا گشت از صدف دری شب‌افروز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فلک در عقد شاهی بند کردش</p></div>
<div class="m2"><p>به یاقوتی دگر پیوند کردش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پریزاد پریرخ گفت ماهی</p></div>
<div class="m2"><p>به بازی بود در نخجیر گاهی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر آمد آفتابی ز آسمان بیش</p></div>
<div class="m2"><p>کشید آن ماه را در چنبر خویش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ختن خاتون چنین گفت از سر هوش</p></div>
<div class="m2"><p>که تنها بود شمشادی قصب پوش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدو پیوست ناگه سروی آزاد</p></div>
<div class="m2"><p>که خوش باشد به یکجا سرو و شمشاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زبان بگشاد گوهرملک دلبند</p></div>
<div class="m2"><p>که زهره نیز تنها بود یک چند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سعادت بر گشاد اقبال را دست</p></div>
<div class="m2"><p>قران مشتری در زهره پیوست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو آمد در سخن نوبت به شاپور</p></div>
<div class="m2"><p>سخن را تازه کرد از عشق منشور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که شیرین انگبینی بود در جام</p></div>
<div class="m2"><p>شهنشه روغن او شد سرانجام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به رنگ‌آمیزی صنعت من آنم</p></div>
<div class="m2"><p>که در حلوای ایشان زعفرانم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پس آنگه کردشان در پهلوی یاد</p></div>
<div class="m2"><p>که احسنت ای جهان پهلو دو همزاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جهان را هر دو چون روشن درخشید</p></div>
<div class="m2"><p>ز یکدیگر مبرید و ملخشید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سخن چون بر لب شیرین گذر کرد</p></div>
<div class="m2"><p>هوا پر مشک و صحرا پر شکر کرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز شرم اندر زمین می‌دید و می‌گفت</p></div>
<div class="m2"><p>که دل بی‌عشق بود و یار بی‌جفت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو شاپور آمد اندر چاره کار</p></div>
<div class="m2"><p>دلم را پاره کرد آن پاره کار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قضای عشق اگرچه سرنبشته است</p></div>
<div class="m2"><p>مرا این سرنبشت او درنبشت است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو سر رشته سوی این نقش زیباست</p></div>
<div class="m2"><p>ز سرخی نقش رویم نقش دیباست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرا کز دست خسرو نقل و جام است</p></div>
<div class="m2"><p>نه کیخسرو پنا خسرو غلام است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سرم از سایه او تاجور باد</p></div>
<div class="m2"><p>ندیمش بخت و دولت راهبر باد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو دور آمد به خسرو گفت باری</p></div>
<div class="m2"><p>سیه شیری بد اندر مرغزاری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گوزنی بر ره شیر آشیان کرد</p></div>
<div class="m2"><p>رسن در گردن شیر ژیان کرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>من آن شیرم که شیرینم به نخجیر</p></div>
<div class="m2"><p>به گردن بر نهاد از زلف زنجیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر شیرین نباشد دستگیرم</p></div>
<div class="m2"><p>چو شمع از سوزش بادی بمیرم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>و گر شیر ژیان آید به حربم</p></div>
<div class="m2"><p>چو شیرین سوی من باشد بچربم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حریفان جنس و یاران اهل بودند</p></div>
<div class="m2"><p>به هر حرفی که می‌شد دست سودند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دل محرم بود چون تخته خاک</p></div>
<div class="m2"><p>بر او دستی زنی حالی شود پاک</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دگر ره طبع شیرین گرم‌تر گشت</p></div>
<div class="m2"><p>دلش در کار خسرو نرم‌تر گشت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>قدح پر باده کرد و لعل پر نوش</p></div>
<div class="m2"><p>به خسرو داد کاین را نوش کن نوش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بخور کین جام شیرین نوش بادت</p></div>
<div class="m2"><p>به جز شیرین همه فرموش بادت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ملک چون گل شدی هر دم شکفته</p></div>
<div class="m2"><p>از آن لعل نسفته لعل سفته</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گهی گفت ای قدح شب رخت بندد</p></div>
<div class="m2"><p>تو بگری تلخ تا شیرین بخندند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گهی گفت ای سحر منمای دندان</p></div>
<div class="m2"><p>مخند آفاق را بر من مخندان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدست آن بتان مجلس‌افروز</p></div>
<div class="m2"><p>سپهر انگشتری می‌باخت تا روز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ببرد انگشتری چون صبح برخاست</p></div>
<div class="m2"><p>که بر بانگ خروس انگشتری خواست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بتان چون یافتند از خرمی بهر</p></div>
<div class="m2"><p>شدند از ساحت صحرا سوی شهر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جهان خوردند و یک جو غم نخوردند</p></div>
<div class="m2"><p>ز شادی کاه برگی کم نکردند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو آمد شیشه خورشید بر سنگ</p></div>
<div class="m2"><p>جهان بر خلق شد چون شیشه تنگ</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دگر ره شیشه می برگرفتند</p></div>
<div class="m2"><p>چو شیشه باده‌ها بر سر گرفتند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بر آن شیشه‌دلان از ترکتازی</p></div>
<div class="m2"><p>فلک را پیشه گشته شیشه‌بازی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به می خوردن طرب را تازه کردند</p></div>
<div class="m2"><p>به عشرت جان شب را تازه کردند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>همان افسانه دوشینه گفتند</p></div>
<div class="m2"><p>همان لعل پرندوشینه سفتند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دل خسرو ز عشق یار پرجوش</p></div>
<div class="m2"><p>به یاد نوش لب می‌کرد می نوش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>می رنگین زهی طاووس بی‌مار</p></div>
<div class="m2"><p>لب شیرین زهی خرمای بیخار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نهاده بر یکی کف ساغر مل</p></div>
<div class="m2"><p>گرفته بر دگر کف دسته گل</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>از آن می‌ خورد و زان گل بوی برداشت</p></div>
<div class="m2"><p>پی دل جستن دلجوی برداشت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شراب تلخ در جانش اثر کرد</p></div>
<div class="m2"><p>به شیرینی سوی شیرین نظر کرد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به غمزه گفت با او نکته‌ای چند</p></div>
<div class="m2"><p>که بود از بوسه لبها را زبانبند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هم از راه اشارت‌های فرخ</p></div>
<div class="m2"><p>حدیث خویشتن را یافت پاسخ</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>سخن‌ها در کرشمه می‌نهفتند</p></div>
<div class="m2"><p>به نوک غمزه گفتند آنچه گفتند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>همه شب پاسبانی پیشه کردند</p></div>
<div class="m2"><p>بسی شب را درین اندیشه کردند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز گرمی روی خسرو خوی گرفته</p></div>
<div class="m2"><p>صبوح خرمی را پی گرفته</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>که شیرین را چگونه مست یابد</p></div>
<div class="m2"><p>بر آن تنگ شکر چون دست یابد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نمی‌افتاد فرصت در میانه</p></div>
<div class="m2"><p>که تیر خسرو افتد بر نشانه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دل شادش به دیدار دل‌افروز</p></div>
<div class="m2"><p>طرب می‌کرد و خوش می‌بود تا روز</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو بر شبدیز شب گلگون خورشید</p></div>
<div class="m2"><p>ستام افکند چون گلبرگ بر بید</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>مه و خورشید دل در صید بستند</p></div>
<div class="m2"><p>به شبدیز و به گلگون برنشستند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>شدند از مرز موقان سوی شهرود</p></div>
<div class="m2"><p>بنا کردند شهری از می و رود</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گهی بر گرد شط بستند زنجیر</p></div>
<div class="m2"><p>ز مرغ و ماهی افکندند نخجیر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>گهی بر فرضه نوشاب شهرود</p></div>
<div class="m2"><p>جهان پر نوش کردند از می و رود</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>گهی راندند سوی دشت مندور</p></div>
<div class="m2"><p>تهی کردنددشت از آهو و گور</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بدینسان روزها تدبیر کردند</p></div>
<div class="m2"><p>گهی عشرت گهی نخجیر کردند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>عروس شب چو نقش افکند بر دست</p></div>
<div class="m2"><p>به شهرآرائی انجم کله بربست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>عروس شاه نیز از حجله برخاست</p></div>
<div class="m2"><p>به روی خویشتن مجلس بیاراست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>عروسان دگر با او شده یار</p></div>
<div class="m2"><p>همه مجلس عروس و شاه بیکار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>شکر بسیار و بادام اندکی بود</p></div>
<div class="m2"><p>کبوتر بی حد و شاهین یکی بود</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>همه بر یاد خسرو می‌ گرفتند</p></div>
<div class="m2"><p>پیاپی خوشدلی را پی گرفتند</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>شبی بی‌رود و رامشگر نبودند</p></div>
<div class="m2"><p>زمانی بی می و ساغر نبودند</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>می و معشوق و گلزار و جوانی</p></div>
<div class="m2"><p>ازین خوشتر نباشد زندگانی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>تماشای گل و گلزار کردن</p></div>
<div class="m2"><p>می لعل از کف دلدار خوردن</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>حمایل دستها در گردن یار</p></div>
<div class="m2"><p>درخت نارون پیچیده بر نار</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به دستی دامن جانان گرفتن</p></div>
<div class="m2"><p>به دیگر دست نبض جان گرفتن</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>گهی جستن به غمزه چاره‌سازی</p></div>
<div class="m2"><p>گهی کردن به بوسه نرد بازی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>گه آوردن بهار تر در آغوش</p></div>
<div class="m2"><p>گهی بستن بنفشه بر بناگوش</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گهی در گوش دلبر راز گفتن</p></div>
<div class="m2"><p>گهی غم‌های دل‌پرداز گفتن</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>جهان این است و این خود در جهان نیست</p></div>
<div class="m2"><p>و گر هست ای عجب جز یک زمان نیست</p></div></div>