---
title: >-
    بخش ۴۱ - مراد طلبیدن خسرو از شیرین و مانع شدن او
---
# بخش ۴۱ - مراد طلبیدن خسرو از شیرین و مانع شدن او

<div class="b" id="bn1"><div class="m1"><p>شبی از جمله شبهای بهاری</p></div>
<div class="m2"><p>سعادت رخ نمود و بخت یاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شده شب روشن از مهتاب چون روز</p></div>
<div class="m2"><p>قدح برداشته ماه شب‌افروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن مهتاب روشنتر ز خورشید</p></div>
<div class="m2"><p>شده باده روان در سایه بید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صفیر مرغ و نوشانوش ساقی</p></div>
<div class="m2"><p>ز دلها برده اندوه فراقی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمامه با شمایل راز می‌گفت</p></div>
<div class="m2"><p>صبا تفسیر آیت باز می‌گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سهی سروی روان بر هر کناری</p></div>
<div class="m2"><p>زهر سروی شکفته نوبهاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی بر جای ساغر دف گرفته</p></div>
<div class="m2"><p>یکی گلاب‌دان بر کف گرفته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو دوری چند رفت از جام نوشین</p></div>
<div class="m2"><p>گران شد هر سری از خواب دوشین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حریفان از نشستن مست گشتند</p></div>
<div class="m2"><p>به رفتن با ملک همدست گشتند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خمار ساقیان افتاده در تاب</p></div>
<div class="m2"><p>دماغ مطربان پیچیده در خواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مهیا مجلسی بی‌گرد اغیار</p></div>
<div class="m2"><p>بنا می‌زد گلی بی‌زحمت خار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شه از راه شکیبائی گذر کرد</p></div>
<div class="m2"><p>شکار آرزو را تنگ‌تر کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سر زلف گره‌گیر دلارام</p></div>
<div class="m2"><p>به دست آورد و رست از دست ایام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لبش بوسید و گفت ای من غلامت</p></div>
<div class="m2"><p>بده دانه که مرغ آمد به دامت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر آنچ از عمر پیشین رفت گو رو</p></div>
<div class="m2"><p>کنون روز از نوست و روزی از نو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من و تو جز من و تو کیست اینجا</p></div>
<div class="m2"><p>حذر کردن نگوئی چیست اینجا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی ساعت من دلسوز را باش</p></div>
<div class="m2"><p>اگر روزی بدی امروز را باش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسان میوه دار نابرومند</p></div>
<div class="m2"><p>امید ما و تقصیر تو تا چند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر خود پولی از سنگ کبود است</p></div>
<div class="m2"><p>چو بی‌آبست پل زان سوی رود است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سگ قصاب را در پهلوی میش</p></div>
<div class="m2"><p>جگر باشد و لیک از پهلوی خویش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بسا ابرا که بندد کله مشک</p></div>
<div class="m2"><p>به عشوه باغ دهقان را کند خشک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بسا شوره زمین کز آبناکی</p></div>
<div class="m2"><p>دهان تشنگان را کرد خاکی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه باید زهر در جامی نهادن</p></div>
<div class="m2"><p>ز شیرینی بر او نامی نهادن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به ترک لولوی تر چون توان گفت</p></div>
<div class="m2"><p>که لولو را به‌ تری به توان سفت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بره در شیرمستی خورد باید</p></div>
<div class="m2"><p>که چون پخته شود گرگش رباید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کبوتر بچه چون آید به پرواز</p></div>
<div class="m2"><p>ز چنگ شه فتد در چنگل باز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به سرپنجه مشو چون شیر سرمست</p></div>
<div class="m2"><p>که ما را پنجه شیرافکنی هست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گوزن کوه اگر گردن‌فراز است</p></div>
<div class="m2"><p>کمند چاره را بازو دراز است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر آهوی بیابان گرم‌خیز است</p></div>
<div class="m2"><p>سکان شاه را تک تیز نیز است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مزن چندین گره بر زلف و خالت</p></div>
<div class="m2"><p>زکاتی ده قضا گردان مالت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو بازرگان صد خروار قندی</p></div>
<div class="m2"><p>چه باشد گر به تنگی در نبندی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو نیل خویش را یابی خریدار</p></div>
<div class="m2"><p>اگر در نیل باشی باز کن بار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شکر پاسخ به لطف آواز دادش</p></div>
<div class="m2"><p>جوابی چون طبرزد باز دادش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که فرخ ناید از چون من غباری</p></div>
<div class="m2"><p>که هم‌تختی کند با تاجداری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خر خود را چنان چابک نبینم</p></div>
<div class="m2"><p>که با تازی سواری برنشینم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نیم چندان شگرف اندر سواری</p></div>
<div class="m2"><p>که آرم پای با شیر شکاری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر نازی کنم مقصودم آنست</p></div>
<div class="m2"><p>که در گرمی شکر خوردن زیانست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو زین گرمی برآسائیم یک چند</p></div>
<div class="m2"><p>مرا شکر مبارک شاه را قند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>وزین پس بر عقیق الماس می‌داشت</p></div>
<div class="m2"><p>زمرد را به افعی پاس می‌داشت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سرش گر سرکشی را رهنمون بود</p></div>
<div class="m2"><p>تقاضای دلش یارب که چون بود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شده از سرخ‌روئی تیز چون خار</p></div>
<div class="m2"><p>خوشا خاری که آرد سرخ گل بار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به هر موئی که تندی داشت چون شیر</p></div>
<div class="m2"><p>هزاران موی قاقم داشت در زیر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کمان ابرویش گر شد گره گیر</p></div>
<div class="m2"><p>کرشمه بر هدف می‌راند چون تیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سنان در غمزه کامد نوبت جنگ</p></div>
<div class="m2"><p>به هر جنگی درش صد آشتی رنگ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نمک در خنده کین لب را مکن ریش</p></div>
<div class="m2"><p>به هر لفظِ مکن در صد بکن ،بیش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>قصب بر رخ که گر نوشم نهانست</p></div>
<div class="m2"><p>بناگوشم به خرده در میانست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ازین سو حلقه لب کرده خاموش</p></div>
<div class="m2"><p>ز دیگر سو نهاده حلقه در گوش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به چشمی ناز بی‌اندازه می‌کرد</p></div>
<div class="m2"><p>به دیگر چشم عذری تازه می‌کرد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو سر پیچید گیسو مجلس آراست</p></div>
<div class="m2"><p>چو رخ گرداند گردن عذر آن خواست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو خسرو را به خواهش گرم دل یافت</p></div>
<div class="m2"><p>مروت را در آن بازی خجل یافت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نمود اندر هزیمت شاه را پشت</p></div>
<div class="m2"><p>به گوگرد سفید آتش همی کشت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بدان پشتی چو پشتش ماند واپس</p></div>
<div class="m2"><p>که روی شاه پشتیوان من بس</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>غلط گفتم نمودش تخته عاج</p></div>
<div class="m2"><p>که شه را نیز باید تخت با تاج</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>حساب دیگر آن بودش در این کوی</p></div>
<div class="m2"><p>که پشتم نیز محرابست چون روی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دگر وجه آنکه گر وجهی شد از دست</p></div>
<div class="m2"><p>از آن روشنترم وجهی دگر هست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چه خوش نازیست ناز خوبرویان</p></div>
<div class="m2"><p>ز دیده رانده را در دیده جویان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به چشمی طیرگی کردن که برخیز</p></div>
<div class="m2"><p>به دیگر چشم دل دادن که مگریز</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به صد جان ارزد آن رغبت که جانان</p></div>
<div class="m2"><p>نخواهم گوید و خواهد به صد جان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو خسرو دید کان ماه نیازی</p></div>
<div class="m2"><p>نخواهد کردن او را چاره سازی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به گستاخی در آمد کی دلارام</p></div>
<div class="m2"><p>گواژه چند خواهی زد بیارام</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو می خوردی و می‌ دادی به من بار</p></div>
<div class="m2"><p>چرا باید که من مستم تو هشیار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به هشیاری مشو با من که مستی</p></div>
<div class="m2"><p>چو من بی‌دل نه‌ای؟ حقا که هستی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ترا این کبک بشکستن چه سود است</p></div>
<div class="m2"><p>که باز عشق کبکت را ربوده‌است</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>و گر خواهی که در دل راز پوشی</p></div>
<div class="m2"><p>شکیبت باد تا با دل بکوشی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تو نیز اندر هزیمت بوق می‌زن</p></div>
<div class="m2"><p>ز چاهی خمیه بر عیوق می‌زن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>درین سودا که با شمشیر تیز است</p></div>
<div class="m2"><p>صلاح گردن‌افرازان گریز است</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>تو خود دانی که در شمشیربازی</p></div>
<div class="m2"><p>هلاک سر بود گردن‌فرازی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دلت گرچه به دلداری نکوشد</p></div>
<div class="m2"><p>بگو تا عشوه رنگی می‌فروشد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بگوید دوستم ور خود نباشد</p></div>
<div class="m2"><p>مرا نیک افتد او را بد نباشد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بسی فال از سر بازیچه برخاست</p></div>
<div class="m2"><p>چو اختر می‌گذشت آن فال شد راست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چه نیکو فال زد صاحب معانی</p></div>
<div class="m2"><p>که خود را فال نیکو زن چو دانی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بد آید فال چون باشی بداندیش</p></div>
<div class="m2"><p>چو گفتی نیک نیک آید فراپیش</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>مرا از لعل تو بوسی تمامست</p></div>
<div class="m2"><p>حلالم کن که آن نیزم حرامست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>و گر خواهی که لب زین نیز دوزم</p></div>
<div class="m2"><p>بدین گرمی نه کان گاهی بسوزم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>از آن ترسم که فردا رخ خراشی</p></div>
<div class="m2"><p>که چون من عاشقی را کشته باشی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ترا هم خون من دامن بگیرد</p></div>
<div class="m2"><p>که خون عاشقان هرگز نمیرد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>گرفتم رای دمسازی نداری</p></div>
<div class="m2"><p>ببوسی هم سر بازی نداری</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ندارم زهره بوس لبانت</p></div>
<div class="m2"><p>چه بوسم؟ آستین یا آستانت</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نگویم بوسه را میری به من ده</p></div>
<div class="m2"><p>لبت را چاشنی‌گیری به من ده</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بده یک بوسه تا ده واستانی</p></div>
<div class="m2"><p>ازین به چون بود بازارگانی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چو بازرگان صد خروار قندی</p></div>
<div class="m2"><p>به ار با من به قندی در نبندی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چو بگشائی گشاید بند بر تو</p></div>
<div class="m2"><p>فرو بندی فرو بندند بر تو</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چو سقا آب چشمه بیش ریزد</p></div>
<div class="m2"><p>ز چشمه که‌آب خیزد بیش خیزد</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>در آغوشت کشم چون آب در میغ</p></div>
<div class="m2"><p>مرا جانی تو با جان چون زنم تیغ</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>سر زلف تو چون هندوی ناپاک</p></div>
<div class="m2"><p>بروز پاک رختم را برد پاک</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>به دزدی هندویت را گر نگیرم</p></div>
<div class="m2"><p>چو هندو دزد نافرمان پذیرم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>اگر چه دزد با صد دهره باشد</p></div>
<div class="m2"><p>چو بانگش بر زنی بی‌زهره باشد</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>نبرد دزد هندو را کسی دست</p></div>
<div class="m2"><p>که با دزدی جوانمردیش هم هست</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>کمند زلف خود در گردنم بند</p></div>
<div class="m2"><p>به صید لاغر امشب باش خرسند</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>تو دل‌خر باش تا من جان فروشم</p></div>
<div class="m2"><p>تو ساقی باش تا من باده نوشم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>شب وصلت لبی پرخنده دارم</p></div>
<div class="m2"><p>چراغ آشنائی زنده دارم</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>حساب حلقه خواهد کرد گوشم</p></div>
<div class="m2"><p>تو می‌خر بنده تا من می‌فروشم</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>شمار بوسه خواهد بود کارم</p></div>
<div class="m2"><p>تو می‌ده بوسه تا من می‌شمارم</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بیا تا از در دولت درآئیم</p></div>
<div class="m2"><p>چو دولت خوش بر آمد خوش برآئیم</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>یک امشب تازه داریم این نفس را</p></div>
<div class="m2"><p>که بر فردا ولایت نیست کس را</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>به نقد امشب چو با هم سازگاریم</p></div>
<div class="m2"><p>نظر بر نسیه فردا چه داریم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>مکن بازی بدان زلف شکن‌گیر</p></div>
<div class="m2"><p>به من بازی کن امشب دست من گیر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>به جان آمد دلم درمان من ساز</p></div>
<div class="m2"><p>کنار خود حصار جان من ساز</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ز جان شیرین‌تری ای چشمه نوش</p></div>
<div class="m2"><p>سزد گر گیرمت چون جان در آغوش</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>چو شکر گر لبت بوسم و گر پای</p></div>
<div class="m2"><p>همه شیرین‌تر آید جایت از جای</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>همه تن در تو شیرینی نهفتند</p></div>
<div class="m2"><p>به کم‌کاری تو را شیرین نگفتند</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>درین شادی به ار غمگین نباشی</p></div>
<div class="m2"><p>نه شیرین باشی ار شیرین نباشی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>شکر لب گفت از این زنهارخواری</p></div>
<div class="m2"><p>پشیمان شو مکن بی‌زینهاری</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>که شه را بد بود زنهار خوردن</p></div>
<div class="m2"><p>بد آمد در جهان بد کار کردن</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>مجوی آبی که آبم را بریزد</p></div>
<div class="m2"><p>مخواه آن کام کز من برنخیزد</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>کزین مقصود بی‌مقصود گردم</p></div>
<div class="m2"><p>تو آتش گشته‌ای من عود گردم</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>مرا بی‌عشق دل خود مهربان بود</p></div>
<div class="m2"><p>چو عشق آمد فسرده چون توان بود</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>گر از بازار عشق اندازه گیرم</p></div>
<div class="m2"><p>به تو هر دم نشاطی تازه گیرم</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>ولیکن نرد با خود باخت نتوان</p></div>
<div class="m2"><p>همیشه با خوشی درساخت نتوان</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>جهان نیمی ز بهر شادکامی است</p></div>
<div class="m2"><p>دگر نیمه ز بهر نیک‌نامی است</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>چه باید طبع را بدرام کردن</p></div>
<div class="m2"><p>دو نیکو نام را بدنام کردن</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>همان بهتر که از خود شرم داریم</p></div>
<div class="m2"><p>بدین شرم از خدا آزرم داریم</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>زن افکندن نباشد مردرائی</p></div>
<div class="m2"><p>خودافکن باش اگر مردی نمائی</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>کسی کافکند خود را بر سر آمد</p></div>
<div class="m2"><p>خود افکن با همه عالم برآمد</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>من آن شیرین درخت آبدارم</p></div>
<div class="m2"><p>که هم حلوا و هم جلاب دارم</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>نخست از من قناعت کن به جلاب</p></div>
<div class="m2"><p>که حلوا هم تو خواهی خورد مشتاب</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>به اول شربت از حلوا میندیش</p></div>
<div class="m2"><p>که حلوا پس بود جلاب در پیش</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چو ما را قند و شکر در دهان هست</p></div>
<div class="m2"><p>به خوزستان چه باید در زدن دست</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>زلال آب چندانی بود خوش</p></div>
<div class="m2"><p>کز او بتوان نشاند آشوب آتش</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>چو آب از سرگذشت آید زیانی</p></div>
<div class="m2"><p>و گر خود باشد آب زندگانی</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>گر این دل چون تو جانان را نخواهد</p></div>
<div class="m2"><p>دلی باشد که او جان را نخواهد</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>ولی تب کرده را حلوا چشیدن</p></div>
<div class="m2"><p>نیرزد سالها صفرا کشیدن</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>بسا بیمار کز بسیار خواری</p></div>
<div class="m2"><p>بماند سال و مه در رنج و زاری</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>اگر چه طبع جوید میوه‌ تر</p></div>
<div class="m2"><p>اگر چه میل دارد دل به شکر</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>ملک چون دید کو در کار خام است</p></div>
<div class="m2"><p>زبانش توسن است و طبع رام است</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>به لابه گفت کای ماه جهان تاب</p></div>
<div class="m2"><p>عتاب دوستان نازست بر تاب</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>صواب آید روا داری پسندی</p></div>
<div class="m2"><p>که وقت دستگیری دست‌بندی</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>دویدم تا به تو دستی در آرم</p></div>
<div class="m2"><p>به دست آرم تو را دستی برآرم</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>چو می‌بینم کنون زلفت مرا بست</p></div>
<div class="m2"><p>تو در دست آمدی من رفتم از دست</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>نگویم در وفا سوگند بشکن</p></div>
<div class="m2"><p>خمارم را به بوسی چند بشکن</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>اسیری را به وعده شاد می‌کن</p></div>
<div class="m2"><p>مبارک مرده‌ای آزاد می‌کن</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>ز باغ وصل پر گل کن کنارم</p></div>
<div class="m2"><p>چو دانی کز فراقت بر چه خارم</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>مگر زان گل گلاب‌آلود گردم</p></div>
<div class="m2"><p>به بوی از گلستان خشنود گردم</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>تو سرمست و سر زلف تو در دست</p></div>
<div class="m2"><p>اگر خوشدل نشینم جای آن هست</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>چو با تو می‌ خورم چون کش نباشم</p></div>
<div class="m2"><p>تو را بینم چرا دلخوش نباشم</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>کمر زرین بود چون با تو بندم</p></div>
<div class="m2"><p>دهن شیرین شود چون با تو خندم</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>گر از من می‌بری چون مهره از مار</p></div>
<div class="m2"><p>من از گل باز می‌مانم تو از خار</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>گر از درد سر من می‌شوی فرد</p></div>
<div class="m2"><p>من از سر دور می‌مانم تو از درد</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>جگر خور کز تو به یاری ندارم</p></div>
<div class="m2"><p>ز تو خوشتر جگرخواری ندارم</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>مرا گر روی تو دلکش نباشد</p></div>
<div class="m2"><p>دلم باشد ولیکن خوش باشد</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>اگر دیده شود بر تو بدل گیر</p></div>
<div class="m2"><p>بود در دیده خس لیکن به تصغیر</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>و گر جان گردد از رویت عنان‌تاب</p></div>
<div class="m2"><p>بود جان را عروسی لیک در خواب</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>عتابی گر بود ما را ازین پس</p></div>
<div class="m2"><p>میانجی در میانه موی تو بس</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>فلک چون جام یاقوتین روان کرد</p></div>
<div class="m2"><p>ز جرعه خاک را یاقوت‌سان کرد</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>ملک برخاست جام باده در دست</p></div>
<div class="m2"><p>هنوز از باده دوشینه سرمست</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>همان سودا گرفته دامنش را</p></div>
<div class="m2"><p>همان آتش رسیده خرمنش را</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>هوای گرم بود و آتش تیز</p></div>
<div class="m2"><p>نمی‌کرد از گیاه خشک پرهیز</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>گرفت آن نار پستان را چنان سخت</p></div>
<div class="m2"><p>که دیبا را فرو بندند بر تخت</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>بسی کوشید شیرین تا به صد زور</p></div>
<div class="m2"><p>قضای شیر گشت از پهلوی گور</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>ملک را گرم دید از بیقراری</p></div>
<div class="m2"><p>مکن گفتا بدینسان گرم‌کاری</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>چه باید خویشتن را گرم کردن</p></div>
<div class="m2"><p>مرا در روی خود بی‌شرم کردن</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>چو تو گرمی کنی نیکو نباشد</p></div>
<div class="m2"><p>گلی کو گرم شد خوشبو نباشد</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>چو باشد گفتگوی خواجه بسیار</p></div>
<div class="m2"><p>به گستاخی پدید آید پرستار</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>به گفتن با پرستاران چه کوشی</p></div>
<div class="m2"><p>سیاست باید اینجا یا خموشی</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>ستور پادشاهی تا بود لنگ</p></div>
<div class="m2"><p>به دشواری مراد آید فراچنگ</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>چو روز بینوائی بر سر آید</p></div>
<div class="m2"><p>مرادت خود به زور از در درآید</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>نباشد هیچ هشیاری در آن مست</p></div>
<div class="m2"><p>که غل بر پای دارد جام در دست</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>تو دولت جو که من خود هستم اینک</p></div>
<div class="m2"><p>به دست آر آن که من در دستم اینک</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>نخواهم نقش بی‌دولت نمودن</p></div>
<div class="m2"><p>من و دولت به هم خواهیم بودن</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>ز دولت‌دوستی جان بر تو ریزم</p></div>
<div class="m2"><p>نیم دشمن که از دولت گریزم</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>طرب کن چون در دولت گشادی</p></div>
<div class="m2"><p>مخور غم چون به روز نیک زادی</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>نخست اقبال وانگه کام جستن</p></div>
<div class="m2"><p>نشاید گنج بی‌آرام جستن</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>به صبری می‌توان کامی خریدن</p></div>
<div class="m2"><p>به آرامی دلارامی خریدن</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>زبان آنگه سخن چشم آنگهی نور</p></div>
<div class="m2"><p>نخست انگور و آنگه آب انگور</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>به گرمی کار عاقل به نگردد</p></div>
<div class="m2"><p>به تک دانی که بز فربه نگردد</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>درین آوارگی ناید برومند</p></div>
<div class="m2"><p>که سازم با مراد شاه پیوند</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>اگر با تو بیاری سر درآرم</p></div>
<div class="m2"><p>من آن یارم که از کارت برآرم</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>تو ملک پادشاهی را بدست آر</p></div>
<div class="m2"><p>که من باشم اگر دولت بود یار</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>گرت با من خوش آید آشنائی</p></div>
<div class="m2"><p>همی ترسم که از شاهی برآئی</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>و گر خواهی به شاهی باز پیوست</p></div>
<div class="m2"><p>دریغا من که باشم رفته از دست</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>جهان در نسل تو ملکی قدیم است</p></div>
<div class="m2"><p>بدست دیگران عیبی عظیم است</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>جهان آنکس برد کو برشتابد</p></div>
<div class="m2"><p>جهانگیری توقف برنتابد</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>همه چیزی ز روی کدخدائی</p></div>
<div class="m2"><p>سکون برتابد الا پادشائی</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>اگر در پادشاهی بنگری تیز</p></div>
<div class="m2"><p>سبق برده‌است از عزم سبک‌خیز</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>جوانی داری و شیری و شاهی</p></div>
<div class="m2"><p>سری و با سری صاحب کلاهی</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>ولایت را ز فتنه پای بگشای</p></div>
<div class="m2"><p>یکی ره دستبرد خویش بنمای</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>بدین هندو که رختت راگرفته است</p></div>
<div class="m2"><p>به ترکی تاج و تختت را گرفته است</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>به تیغ آزرده کن ترکیب جسمش</p></div>
<div class="m2"><p>مگر باطل کنی ساز طلسمش</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>که دست خسروان در جستن کام</p></div>
<div class="m2"><p>گهی با تیغ باید گاه با جام</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>ز تو یک تیغ تنها بر گرفتن</p></div>
<div class="m2"><p>ز شش حد جهان لشگر گرفتن</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>کمر بندد فلک در جنگ با تو</p></div>
<div class="m2"><p>دراندازد به دشمن سنگ با تو</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>مرا نیز ار بود دستی نمایم</p></div>
<div class="m2"><p>وگرنه در دعا دستی گشایم</p></div></div>