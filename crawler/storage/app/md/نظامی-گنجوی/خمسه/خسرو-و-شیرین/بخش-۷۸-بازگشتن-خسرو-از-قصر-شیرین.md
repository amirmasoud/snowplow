---
title: >-
    بخش ۷۸ - بازگشتن خسرو از قصر شیرین
---
# بخش ۷۸ - بازگشتن خسرو از قصر شیرین

<div class="b" id="bn1"><div class="m1"><p>شباهنگام کاهوی ختن گرد</p></div>
<div class="m2"><p>ز ناف مشک خود خود را رسن کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار آهو بره لبها پر از شیر</p></div>
<div class="m2"><p>بر این سبزه شدند آرامگه گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک چون آهوی نافه دریده</p></div>
<div class="m2"><p>عتاب یار آهو چشم دیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هر سو قطره‌های برف و باران</p></div>
<div class="m2"><p>شده بارنده چون ابر بهاران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز هیبت کوه چون گل می‌گدازید</p></div>
<div class="m2"><p>ز برف ارزیز بر دل می‌گدازید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به زیر خسرو از برف درم ریز</p></div>
<div class="m2"><p>نقاب نقره بسته خنگ شبدیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبانش موی شد وز هیچ روئی</p></div>
<div class="m2"><p>به مشگین موی در نگرفت موئی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسی نالید تا رحمت کند یار</p></div>
<div class="m2"><p>به صد فرصت نشد یک نکته بر کار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفیرش گرچه هر دم تیزتر بود</p></div>
<div class="m2"><p>جوابش هر زمان خونریزتر بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو پاسی از شب دیجور بگذشت</p></div>
<div class="m2"><p>از آن در شاه دل رنجور بگذشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرس می‌راند چون بیمار خیزان</p></div>
<div class="m2"><p>ز دیده بر فرس خوناب ریزان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر از پس مانده میشد با دل ریش</p></div>
<div class="m2"><p>رهی بی‌خویشتن بگرفته در پیش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه پای آنکه راند اسب را تیز</p></div>
<div class="m2"><p>نه دست آن که برد پای شبدیز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سرشک و آه راه ره توشه بسته</p></div>
<div class="m2"><p>ز مروارید بر گل خوشه بسته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>درین حسرت که آوخ گر درین راه</p></div>
<div class="m2"><p>پدیدار آمدی یا کوه یا چاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مگر بودی درنگم را بهانه</p></div>
<div class="m2"><p>بماندی رختم این جا جاوادانه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گهی می‌زد ز تندی دست بر دست</p></div>
<div class="m2"><p>گهی دستارچه بر دیده می‌بست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو آمد سوی لشکرگاه نومید</p></div>
<div class="m2"><p>دلش می‌سوخت از گرمی چو خورشید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>درید ابر سیاه از سبز گلشن</p></div>
<div class="m2"><p>بر آمد ماهتابی سخت روشن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شهنشه نوبتی بر چرخ پیوست</p></div>
<div class="m2"><p>کنار نوبتی را شقه بر بست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه از دل در جهان نظاره می‌کرد</p></div>
<div class="m2"><p>بجای جامه دل را پاره می‌کرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به آسایش نمودن سر نمی‌داشت</p></div>
<div class="m2"><p>سر از زانوی حسرت برنمی‌داشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ندیم و حاجب و جاندار و دستور</p></div>
<div class="m2"><p>همه رفتند و خسرو ماند و شاپور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به صنعت هر دم آن استاد نقاش</p></div>
<div class="m2"><p>بر او نقش طرب بستی که خوش باش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زدی بر آتش سوزان او آب</p></div>
<div class="m2"><p>به رویش در بخندیدی چو مهتاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دلش دادی که شیرین مهربانست</p></div>
<div class="m2"><p>بدین تلخی مبین کش در زبانست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر شیرین سر پیکار دارد</p></div>
<div class="m2"><p>رطب دانی که سر با خار دارد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مکن سودا که شیرین خشم ریزد</p></div>
<div class="m2"><p>ز شیرینی به جز صفرا چه خیزد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرنج از گرمی شیرین رنجور</p></div>
<div class="m2"><p>که شیرینی به گرمی هست مشهور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ملک چون جای خالی دید از اغیار</p></div>
<div class="m2"><p>شکایت کرد با شاپور بسیار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که دیدی تا چه رفت امروز با من</p></div>
<div class="m2"><p>چه کرد آن شوخ عالم سوز با من</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چه بی‌شرمی نمود آن ناخدا ترس</p></div>
<div class="m2"><p>چو زن گفتی کجا شرم و کجا ترس</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کله چون نارون پیشش نهادم</p></div>
<div class="m2"><p>به استغفار چون سرو ایستادم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تبر بر نارون گستاخ میزد</p></div>
<div class="m2"><p>به دهره سرو بن را شاخ میزد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نه زان سرما نوازش گرم گشتش</p></div>
<div class="m2"><p>نه دل زان سخت روئی نرم گشتش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زبانش سر بسر تیر و تبر بود</p></div>
<div class="m2"><p>یکایک عذرش از جرمش بتر بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بلی تیزی نماید یار با یار</p></div>
<div class="m2"><p>نه تا این حد که باشد خار با خار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز تیزی نیز من دارم نشانی</p></div>
<div class="m2"><p>مرا در کالبد هم هست جانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر هاروت بابل شد جمالش</p></div>
<div class="m2"><p>و گر سر بابل هندوست خالش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز بس سردی که چون یخ شد سرشتم</p></div>
<div class="m2"><p>فسون هر دو را بر یخ نوشتم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>غمش را کز شکیبائی فزونست</p></div>
<div class="m2"><p>من غمخواره می‌دانم که چونست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سرشت طفل بد را دایه داند</p></div>
<div class="m2"><p>بد همسایه را همسایه داند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مرا او دشمنی آمد نهانی</p></div>
<div class="m2"><p>نهفته کین و ظاهر مهربانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چه خواهش کان نکردم دوش با او</p></div>
<div class="m2"><p>نپذرفت و جدا شد هوش با او</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سخنهای خوش از هر رسم و راهی</p></div>
<div class="m2"><p>بگفتم سالی و نشنید ماهی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شب آمد روشنائی هم نبخشید</p></div>
<div class="m2"><p>شکست و مومیائی هم نبخشید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اگر چه وصل شیرین بی‌نمک نیست</p></div>
<div class="m2"><p>وزو شیرین‌تری زیر فلک نیست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مرا پیوند او خواری نیرزد</p></div>
<div class="m2"><p>نمک خوردن جگرخواری نیرزد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به زیر پای پیلان در شدن پست</p></div>
<div class="m2"><p>به از پیش خسیسان داشتن دست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به آب اندر شدن غرفه چو ماهی</p></div>
<div class="m2"><p>از آن به کز وزغ زنهار خواهی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به ناخن سنگ بر کندن ز کهسار</p></div>
<div class="m2"><p>به از حاجت به نزد ناسزاوار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همه کس در در آب پاک یابد</p></div>
<div class="m2"><p>کسی کو خاک جوید خاک یابد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چرا در سنگ ریزه کان کنم کان</p></div>
<div class="m2"><p>چه بی‌روغن چراغی جان کنم جان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چه باید ملک جان دادن به شوخی</p></div>
<div class="m2"><p>که بنشیند کلاغش بر کلوخی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مرا چون من کسی باید به ناموس</p></div>
<div class="m2"><p>که باشد همسر طاوس طاوس</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نخستین خاک را بوسید شاپور</p></div>
<div class="m2"><p>پس آنگه زد بر آتش آب کافور</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کز این تندی نباید تیز بودن</p></div>
<div class="m2"><p>جوانمردیست عذرانگیز بودن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ستیز عاشقان چون برق باشد</p></div>
<div class="m2"><p>میان ناز و وحشت فرق باشد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اگر گرمست شیرین هست معذور</p></div>
<div class="m2"><p>که شیرینی به گرمی هست مشهور</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نه شیرین خود همه خرما دهانی</p></div>
<div class="m2"><p>ندارد لقمه بی‌استخوانی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گرت سر گردد از صفرای شیرین</p></div>
<div class="m2"><p>ز سر بیرون مکن سودای شیرین</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>مگر شیرین از آن صفرا خبر داشت</p></div>
<div class="m2"><p>که چندان سر که در زیر شکر داشت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو شیرینی و ترشی هست در کار</p></div>
<div class="m2"><p>از این صفرا و سودا دست مگذار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>عجب ناید ز خوبان زود سیری</p></div>
<div class="m2"><p>چنانک از سگ سگی وز شیر شیری</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شبه با در بود عادت چنین است</p></div>
<div class="m2"><p>کلید گنج زرین آهنین است</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به جور از نیکوان نتوان بریدن</p></div>
<div class="m2"><p>بباید ناز معشوقان کشیدن</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>همه خوبان چنین باشند بدخوی</p></div>
<div class="m2"><p>عروسی کی بود بیرنگ و بی‌بوی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کدامین گل بود بی‌زحمت خار</p></div>
<div class="m2"><p>کدامین خط بود بی‌زخم پرگار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ز خوبان توسنی رسم قدیمست</p></div>
<div class="m2"><p>چو مار آبی بود زخمش سلیمست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>رهائی خواهی از سیلاب اندوه</p></div>
<div class="m2"><p>قدم بر جای باید بود چون کوه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گر از هر باد چون کاهی بلرزی</p></div>
<div class="m2"><p>اگر کوهی شوی کاهی نیرزی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>به ار کامت به ناکامی برآید</p></div>
<div class="m2"><p>که بوی عنبر از خامی برآید</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بر آن مه ترکتازی کرد نتوان</p></div>
<div class="m2"><p>که بر مه دست یازی کرد نتوان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>زنست آخر در اندر بند و مشتاب</p></div>
<div class="m2"><p>که از روزن فرود آید چو مهتاب</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>مگر ماه و زن از یک فن در آیند</p></div>
<div class="m2"><p>که چون دربندی از روزن در آیند</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چه پنداری که او زین غصه دورست</p></div>
<div class="m2"><p>نه دورست او ولی دانم صبورست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>گر از کوه جفا سنگی در افتد</p></div>
<div class="m2"><p>ترا بر سایه او را بر سر افتد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>و گر خاری ز وحشت حاصل آید</p></div>
<div class="m2"><p>ترا بر دامن او را بر دل آید</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>یک امشب ار صبوری کرد باید</p></div>
<div class="m2"><p>شب آبستن بود تا خود چه زاید</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ندارد جاودان طالع یکی خوی</p></div>
<div class="m2"><p>نماند آب دایم در یکی جوی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>همه ساله نباشد کامکاری</p></div>
<div class="m2"><p>گهی باشد عزیزی گاه خواری</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بهر نازی که بر دولت کند بخت</p></div>
<div class="m2"><p>نباید دولتی را داشتن سخت</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>کجا پرگار گردش ساز گردد</p></div>
<div class="m2"><p>به گردش گاه اول باز گردد</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>هر آن رایض که او توسن کند رام</p></div>
<div class="m2"><p>کند آهستگی با کره خام</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>به صبرش عاقبت جائی رساند</p></div>
<div class="m2"><p>که بروی هر که را خواهد نشاند</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>به صبر از بند گردد مرد رسته</p></div>
<div class="m2"><p>که صبر آمد کلید کار بسته</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گشاید بند چون دشوار گردد</p></div>
<div class="m2"><p>بخندد صبح چون شب تار گردد</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>امیدم هست کاین سختی سرآید</p></div>
<div class="m2"><p>مراد شه بدین زودی برآید</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بدین وعده ملک را شاد می‌کرد</p></div>
<div class="m2"><p>خرابی را به رفق آباد می‌کرد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ز دولت بر رخ شه خال میزد</p></div>
<div class="m2"><p>چو اختر می‌گذشت او فال میزد</p></div></div>