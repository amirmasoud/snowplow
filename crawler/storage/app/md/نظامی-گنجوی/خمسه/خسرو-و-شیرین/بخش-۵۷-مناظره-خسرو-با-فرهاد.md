---
title: >-
    بخش ۵۷ - مناظره خسرو با فرهاد
---
# بخش ۵۷ - مناظره خسرو با فرهاد

<div class="b" id="bn1"><div class="m1"><p>نخستین بار گفتش کز کجایی</p></div>
<div class="m2"><p>بگفت از دار ملک آشنایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگفت آنجا به صنعت در چه کوشند</p></div>
<div class="m2"><p>بگفت انده خرند و جان فروشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگفتا جان‌فروشی در ادب نیست</p></div>
<div class="m2"><p>بگفت از عشقبازان این عجب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگفت از دل شدی عاشق بدینسان؟</p></div>
<div class="m2"><p>بگفت از دل تو می‌گویی من از جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفتا عشق شیرین بر تو چون است</p></div>
<div class="m2"><p>بگفت از جان شیرینم فزون است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفتا هر شبش بینی چو مهتاب</p></div>
<div class="m2"><p>بگفت آری چو خواب آید کجا خواب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگفتا دل ز مهرش کی کنی پاک</p></div>
<div class="m2"><p>بگفت آنگه که باشم خفته در خاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگفتا گر خرامی در سرایش</p></div>
<div class="m2"><p>بگفت اندازم این سر زیر پایش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگفتا گر کند چشم تو را ریش</p></div>
<div class="m2"><p>بگفت این چشم دیگر دارمش پیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفتا گر کسیش آرد فرا چنگ</p></div>
<div class="m2"><p>بگفت آهن خورد ور خود بود سنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگفتا گر نیابی سوی او راه</p></div>
<div class="m2"><p>بگفت از دور شاید دید در ماه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفتا دوری از مه نیست درخور</p></div>
<div class="m2"><p>بگفت آشفته از مه دور بهتر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگفتا گر بخواهد هر چه داری</p></div>
<div class="m2"><p>بگفت این از خدا خواهم به زاری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگفتا گر به سر یابیش خوشنود</p></div>
<div class="m2"><p>بگفت از گردن این وام افکنم زود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگفتا دوستیش از طبع بگذار</p></div>
<div class="m2"><p>بگفت از دوستان ناید چنین کار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بگفت آسوده شو که این کار خام است</p></div>
<div class="m2"><p>بگفت آسودگی بر من حرام است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگفتا رو صبوری کن در این درد</p></div>
<div class="m2"><p>بگفت از جان صبوری چون توان کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگفت از صبر کردن کس خجل نیست</p></div>
<div class="m2"><p>بگفت این دل تواند کرد دل نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفت از عشق کارت سخت زار است</p></div>
<div class="m2"><p>بگفت از عاشقی خوش‌تر چه کار است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگفتا جان مده بس دل که با اوست</p></div>
<div class="m2"><p>بگفتا دشمنند این هر دو بی‌دوست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگفتا در غمش می‌ترسی از کس</p></div>
<div class="m2"><p>بگفت از محنت هجران او بس</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگفتا هیچ همخوابیت باید</p></div>
<div class="m2"><p>بگفت ار من نباشم نیز شاید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگفتا چونی از عشق جمالش</p></div>
<div class="m2"><p>بگفت آن کس نداند جز خیالش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بگفت از دل جدا کن عشق شیرین</p></div>
<div class="m2"><p>بگفتا چون زیم بی‌جان شیرین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگفت او آن من شد زو مکن یاد</p></div>
<div class="m2"><p>بگفت این کی کند بیچاره فرهاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بگفت ار من کنم در وی نگاهی</p></div>
<div class="m2"><p>بگفت آفاق را سوزم به آهی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو عاجز گشت خسرو در جوابش</p></div>
<div class="m2"><p>نیامد بیش پرسیدن صوابش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به یاران گفت کز خاکی و آبی</p></div>
<div class="m2"><p>ندیدم کس بدین حاضر جوابی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به زر دیدم که با او بر نیایم</p></div>
<div class="m2"><p>چو زرش نیز بر سنگ آزمایم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گشاد آنگه زبان چون تیغ پولاد</p></div>
<div class="m2"><p>فکند الماس را بر سنگ بنیاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که ما را هست کوهی بر گذرگاه</p></div>
<div class="m2"><p>که مشکل می‌توان کردن بدو راه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>میان کوه راهی کند باید</p></div>
<div class="m2"><p>چنانک آمد شد ما را بشاید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدین تدبیر کس را دسترس نیست</p></div>
<div class="m2"><p>که کار تست و کار هیچ کس نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به حق حرمت شیرین دلبند</p></div>
<div class="m2"><p>کز این بهتر ندانم خورد سوگند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که با من سر بدین حاجت درآری</p></div>
<div class="m2"><p>چو حاجتمندم این حاجت برآری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جوابش داد مرد آهنین‌چنگ</p></div>
<div class="m2"><p>که بردارم ز راه خسرو این سنگ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به شرط آنکه خدمت کرده باشم</p></div>
<div class="m2"><p>چنین شرطی به جای آورده باشم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دل خسرو رضای من بجوید</p></div>
<div class="m2"><p>به ترک شکر شیرین بگوید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنان در خشم شد خسرو ز فرهاد</p></div>
<div class="m2"><p>که حلقش خواست آزردن به پولاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دگر ره گفت از این شرطم چه باک است</p></div>
<div class="m2"><p>که سنگ است آنچه فرمودم نه خاک است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر خاک است چون شاید بریدن</p></div>
<div class="m2"><p>و گر برد کجا شاید کشیدن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به گرمی گفت کاری شرط کردم</p></div>
<div class="m2"><p>و گر زین شرط برگردم نه مردم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>میان دربند و زور دست بگشای</p></div>
<div class="m2"><p>برون شو دست‌برد خویش بنمای</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو بشنید این سخن فرهاد بی‌دل</p></div>
<div class="m2"><p>نشان کوه جست از شاه عادل</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به کوهی کرد خسرو رهنمونش</p></div>
<div class="m2"><p>که خواند هر کس اکنون بی‌ستونش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به حکم آنکه سنگی بود خارا</p></div>
<div class="m2"><p>به سختی روی آن سنگ آشکارا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز دعوی گاه خسرو با دلی خوش</p></div>
<div class="m2"><p>روان شد کوهکن چون کوه آتش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بر آن کوه کمرکش رفت چون باد</p></div>
<div class="m2"><p>کمر دربست و زخم تیشه بگشاد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نخست آزرم آن کرسی نگه‌داشت</p></div>
<div class="m2"><p>بر او تمثال‌های نغز بنگاشت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به تیشه صورت شیرین بر آن سنگ</p></div>
<div class="m2"><p>چنان بر زد که مانی نقش ارژنگ</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>پس آنگه از سنان تیشه تیز</p></div>
<div class="m2"><p>گزارش کرد شکل شاه و شبدیز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بر آن صورت شنیدی کز جوانی</p></div>
<div class="m2"><p>جوانمردی چه کرد از مهربانی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>وز آن دنبه که آمد پیه‌پرورد</p></div>
<div class="m2"><p>چه کرد آن پیرزن با آن جوانمرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اگرچه دنبه بر گرگان تله بست</p></div>
<div class="m2"><p>به دنبه شیرمردی زان تله رست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو پیه از دنبه زانسان دید بازی</p></div>
<div class="m2"><p>تو بر دنبه چرا پیه می‌گدازی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مکن که‌این میش دندان پیر دارد</p></div>
<div class="m2"><p>به خوردن دنبه‌ای دلگیر دارد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو برج طالعت نامد ذنب‌دار</p></div>
<div class="m2"><p>ز پس رفتن چرا باید ذنب‌وار</p></div></div>