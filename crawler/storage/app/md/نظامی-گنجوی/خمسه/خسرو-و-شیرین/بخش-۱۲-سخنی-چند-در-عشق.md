---
title: >-
    بخش ۱۲ - سخنی چند در عشق
---
# بخش ۱۲ - سخنی چند در عشق

<div class="b" id="bn1"><div class="m1"><p>مراکز عشق به ناید شعاری</p></div>
<div class="m2"><p>مبادا تا زیم جز عشق کاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلک جز عشق محرابی ندارد</p></div>
<div class="m2"><p>جهان بی‌خاک عشق آبی ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غلام عشق شو کاندیشه این است</p></div>
<div class="m2"><p>همه صاحب دلان را پیشه این است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان عشقست و دیگر زرق سازی</p></div>
<div class="m2"><p>همه بازیست الا عشقبازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر بی‌عشق بودی جان عالم</p></div>
<div class="m2"><p>که بودی زنده در دوران عالم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی کز عشق خالی شد فسردست</p></div>
<div class="m2"><p>کرش صد جان بود بی‌عشق مردست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر خود عشق هیچ افسون نداند</p></div>
<div class="m2"><p>نه از سودای خویشت وارهاند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشو چون خر بخورد و خواب خرسند</p></div>
<div class="m2"><p>اگر خود گربه باشد دل در و بند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به عشق گربه گر خود چیرباشی</p></div>
<div class="m2"><p>از آن بهتر که با خود شیرباشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نروید تخم کس بی‌دانه عشق</p></div>
<div class="m2"><p>کس ایمن نیست جز در خانه عشق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز سوز عشق بهتر در جهان چیست</p></div>
<div class="m2"><p>که بی او گل نخندید ابر نگریست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شنیدم عاشقی را بود مستی</p></div>
<div class="m2"><p>و از آنجا خاست اول بت‌پرستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همان گبران که بر آتش نشستند</p></div>
<div class="m2"><p>ز عشق آفتاب آتش پرستند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مبین در دل که او سلطان جانست</p></div>
<div class="m2"><p>قدم در عشق نه کو جان جانست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم از قبله سخن گوید هم از لات</p></div>
<div class="m2"><p>همش کعبه خزینه هم خرابات</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر عشق اوفتد در سینه سنگ</p></div>
<div class="m2"><p>به معشوقی زند در گوهری چنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که مغناطیس اگر عاشق نبودی</p></div>
<div class="m2"><p>بدان شوق آهنی را چون ربودی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>و گر عشقی نبودی بر گذرگاه</p></div>
<div class="m2"><p>نبودی کهربا جوینده کاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بسی سنگ و بسی گوهر بجایند</p></div>
<div class="m2"><p>نه آهن را نه که را می‌ربایند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هران جوهر که هستند از عدد بیش</p></div>
<div class="m2"><p>همه دارند میل مرکز خویش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر آتش در زمین منفذ نیابد</p></div>
<div class="m2"><p>زمین بشکافد و بالا شتابد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>و گر آبی بماند در هوا دیر</p></div>
<div class="m2"><p>به میل طبع هم راجع شود زیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>طبایع جز کشش کاری ندانند</p></div>
<div class="m2"><p>حکیمان این کشش را عشق خوانند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر اندیشه کنی از راه بینش</p></div>
<div class="m2"><p>به عشق است ایستاده آفرینش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر از عشق آسمان آزاد بودی</p></div>
<div class="m2"><p>کجا هرگز زمین آباد بودی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو من بی‌عشق خود را جان ندیدم</p></div>
<div class="m2"><p>دلی بفروختم جانی خریدم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز عشق آفاق را پردود کردم</p></div>
<div class="m2"><p>خرد را دیده خواب‌آلود کردم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کمر بستم به عشق این داستان را</p></div>
<div class="m2"><p>صلای عشق در دادم جهان را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مبادا بهره‌مند از وی خسیسی</p></div>
<div class="m2"><p>به جز خوشخوانی و زیبانویسی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز من نیک آمد این اربد نویسند</p></div>
<div class="m2"><p>به مزد من گناه خود نویسند</p></div></div>