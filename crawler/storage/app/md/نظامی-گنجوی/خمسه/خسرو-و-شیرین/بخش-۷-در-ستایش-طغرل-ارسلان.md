---
title: >-
    بخش ۷ - در ستایش طغرل ارسلان
---
# بخش ۷ - در ستایش طغرل ارسلان

<div class="b" id="bn1"><div class="m1"><p>چون سلطان جوان شاه جوانبخت</p></div>
<div class="m2"><p>که برخوردار باد از تاج و از تخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سریر افروز اقلیم معانی</p></div>
<div class="m2"><p>ولایت گیر ملک زندگانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پناه ملک شاهنشاه طغرل</p></div>
<div class="m2"><p>خداوند جهان سلطان عادل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملک طغرل که دارای وجود است</p></div>
<div class="m2"><p>سپهر دولت و دریای جود است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سلطانی به تاج و تخت پیوست</p></div>
<div class="m2"><p>به جای ارسلان بر تخت بنشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من این گنجینه را در می‌گشادم</p></div>
<div class="m2"><p>بنای این عمارت می‌نهادم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مبارک بود طالع نقش بستم</p></div>
<div class="m2"><p>فلک گفتا مبارک باد و هستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدین طالع که هست این نقش را فال</p></div>
<div class="m2"><p>مرا چون نقش خود نیکو کند حال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو نقش از طالع سلطان نماید</p></div>
<div class="m2"><p>چو سلطان گر جهان گیرست شاید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ازین پیکر که معشوق دل آمد</p></div>
<div class="m2"><p>به کم مدت فراغت حاصل آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درنگ از بهر آن افتاد در راه</p></div>
<div class="m2"><p>که تا از شغلها فارغ شود شاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حبش را زلف بر طمغاج بندد</p></div>
<div class="m2"><p>طراز شوشتر در چاج بندد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به باز چتر عنقا را بگیرد</p></div>
<div class="m2"><p>به تاج زر ثریا را بگیرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شکوهش چتر بر گردون رساند</p></div>
<div class="m2"><p>سمندش کوه از جیحون جهاند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به فتح هفت کشور سر برآرد</p></div>
<div class="m2"><p>سر نه چرخ را در چنبر آرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گهش خاقان خراج چین فرستد</p></div>
<div class="m2"><p>گهش قیصر گزیت دین فرستد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بحمدالله که با قدر بلندش</p></div>
<div class="m2"><p>کمالی در نیابد جز سپندش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من از شفقت سپند مادرانه</p></div>
<div class="m2"><p>بدود صبحدم کردم روانه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به شرط آنکه گر بوئی دهد خوش</p></div>
<div class="m2"><p>نهد بر نام من نعلی بر آتش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدان لفظ بلند گوهر افشان</p></div>
<div class="m2"><p>که جان عالمست و عالم جان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اتابک را بگوید کای جهانگیر</p></div>
<div class="m2"><p>نظامی وانگهی صدگونه تقصیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نیامد وقت آن کاو را نوازیم؟</p></div>
<div class="m2"><p>ز کار افتاده‌ای را کار سازیم؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به چشمی چشم این غمگین گشائیم؟</p></div>
<div class="m2"><p>به ابروئیش از ابروچین گشائیم؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز ملک ما که دولت راست بنیاد</p></div>
<div class="m2"><p>چه باشد گر خرابی گردد آباد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنین گوینده‌ای در گوشه تا کی</p></div>
<div class="m2"><p>سخندانی چنین بی‌توشه تا کی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از آن شد خانهٔ خورشید معمور</p></div>
<div class="m2"><p>که تاریکان عالم را دهد نور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سخای ابر از آن آمد جهانگیر</p></div>
<div class="m2"><p>که در طفلی گیاهی را دهد شیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کنون عمری است کین مرغ سخن سنج</p></div>
<div class="m2"><p>به شکر نعمت ما می‌برد رنج</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نخورده جامی از میخانه ما</p></div>
<div class="m2"><p>کند از شکرها شکرانهٔ ما</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شفیعی چون من و چون او غلامی</p></div>
<div class="m2"><p>چو تو کیخسروی کمتر ز جامی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نظامی چیست این گستاخ روئی</p></div>
<div class="m2"><p>که با دولت کنی گستاخ گوئی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خداوندی که چون خاقان و فغفور</p></div>
<div class="m2"><p>به صد حاجت دری بوسندش از دور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چه عذر آری تو ای خاکی‌تر از خاک</p></div>
<div class="m2"><p>که گویائی درین خط خطرناک</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یکی عذر است کو در پادشاهی</p></div>
<div class="m2"><p>صفت دارد ز درگاه الهی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدان در هر که بالاتر فروتر</p></div>
<div class="m2"><p>کسی کافکنده‌تر گستاخ روتر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نبینی برق، کاهن را بسوزد</p></div>
<div class="m2"><p>چراغ پیره زن چون برفروزد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همان دریا که موجش سهمناک است</p></div>
<div class="m2"><p>گلی را باغ و باغی را هلاک است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سلیمان است شه با او درین راه</p></div>
<div class="m2"><p>گهی ماهی سخن گوید گهی ماه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دبیران را به آتش گاه سباک</p></div>
<div class="m2"><p>گهی زر در حساب آید گهی خاک</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خدایا تا جهان را آب و رنگ است</p></div>
<div class="m2"><p>فلک را دور و گیتی را درنگ است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جهان را خاص این صاحبقران کن</p></div>
<div class="m2"><p>فلک را یار این گیتی ستان کن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مُمَتِّع دارش از بخت و جوانی</p></div>
<div class="m2"><p>ز هر چیزش فزون ده زندگانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مبادا دولت از نزدیک او دور</p></div>
<div class="m2"><p>مبادا تاج را بی‌فرق او نور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فراخی باد از اقبالش جهان را</p></div>
<div class="m2"><p>ز چترش سربلندی آسمان را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مقیم جاودانی باد جانش</p></div>
<div class="m2"><p>حریم زندگانی آستانش</p></div></div>