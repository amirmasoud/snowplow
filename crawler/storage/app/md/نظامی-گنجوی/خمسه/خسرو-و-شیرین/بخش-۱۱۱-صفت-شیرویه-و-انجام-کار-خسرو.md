---
title: >-
    بخش ۱۱۱ - صفت شیرویه و انجام کار خسرو
---
# بخش ۱۱۱ - صفت شیرویه و انجام کار خسرو

<div class="b" id="bn1"><div class="m1"><p>چو خسرو تخته حکمت در آموخت</p></div>
<div class="m2"><p>به آزادی جهان را تخته بر دوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز مریم بود یک فرزند خامش</p></div>
<div class="m2"><p>چو شیران ابخر و شیرویه نامش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شنیدم من که آن فرزند قتال</p></div>
<div class="m2"><p>در آن طفلی که بودش قرب نه سال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو شیرین را عروسی بود می‌گفت</p></div>
<div class="m2"><p>که شیرین کاشگی بودی مرا جفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز مهرش باز گویم یا ز کینش</p></div>
<div class="m2"><p>ز دانش یا ز دولت یا ز دینش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرای شاه ازو پر دود می‌بود</p></div>
<div class="m2"><p>بدو پیوسته ناخشنود می‌بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزرگ امید را گفت ای خردمند</p></div>
<div class="m2"><p>دلم بگرفت از این وارونه فرزند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از این نافرخ اختر می‌هراسم</p></div>
<div class="m2"><p>فساد طالعش را می‌شناسم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بد فعلی که دارد در سر خویش</p></div>
<div class="m2"><p>چو گرگ ایمن نشد بر مادر خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ازین ناخوش نیاید خصلتی خوش</p></div>
<div class="m2"><p>که خاکستر بود فرزند آتش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نگوید آنچه کس را دلکش آید</p></div>
<div class="m2"><p>همه آن گوید او کو را خوش آید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه با فرش همی بینم نه با سنگ</p></div>
<div class="m2"><p>ز فر و سنگ بگریزد به فرسنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو دود از آتش من گشت خیزان</p></div>
<div class="m2"><p>ز من زاده ولی از من گریزان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سرم تاج از سرافرازان ربودست</p></div>
<div class="m2"><p>خلف بس ناخلف دارم چه سوداست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه بر شیرین نه بر من مهربانست</p></div>
<div class="m2"><p>نه با همشیرگان شیرین زبانست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به چشمی بیند این دیو آن پری را</p></div>
<div class="m2"><p>که خر در پیشه‌ها پالانگری را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز من بگذر که من خود گرزه مارم</p></div>
<div class="m2"><p>بلی مارم که چون او مهره دارم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه هر زن زن بود هر زاده فرزند</p></div>
<div class="m2"><p>نه هر گل میوه آرد هر نیی قند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بسا زاده که کشت آن را کزو زاد</p></div>
<div class="m2"><p>بس آهن کو کند بر سنگ بیداد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بسا بیگانه کز صاحب وفائی</p></div>
<div class="m2"><p>ز خویشان بیش دارد آشنائی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بزرگ امید گفت ای پیش بین شاه</p></div>
<div class="m2"><p>دل پاکت ز هر نیک و بد آگاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گرفتم کاین پسر درد سر تست</p></div>
<div class="m2"><p>نه آخر پاره‌ای از گوهر تست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نشاید خصمی فرزند کردن</p></div>
<div class="m2"><p>دل از پیوند بی‌پیوند کردن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کسی بر ناربن نارد لگد را</p></div>
<div class="m2"><p>کا تاج سر کند فرزند خود را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درخت تود از آن آمد لگدخوار</p></div>
<div class="m2"><p>که دارد بچه خود را نگونسار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو نیکی بد نباشد نیز فرزند</p></div>
<div class="m2"><p>بود تره به تخم خویش مانند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قبای زر چو در پیرایش افتد</p></div>
<div class="m2"><p>ازو هم زر بود کارایش افتد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر توسن شد این فرزند جماش</p></div>
<div class="m2"><p>زمانه خود کند رامش تو خوش باش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جوانی دارد زینسان پر از جوش</p></div>
<div class="m2"><p>به پیری توسنی گردد فراموش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنان افتد از آن پس رای خسرو</p></div>
<div class="m2"><p>که آتش خانه باشد جای خسرو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نسازد با همالان هم نشستی</p></div>
<div class="m2"><p>کند چون موبدان آتش‌پرستی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو خسرو را به آتش خانه شد رخت</p></div>
<div class="m2"><p>چو شیر مست شد شیرویه بر تخت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به نوشانوش می در کاس می‌داشت</p></div>
<div class="m2"><p>ز دورا دور شه را پاس می‌داشت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدان نگذاشت آخر بند کردش</p></div>
<div class="m2"><p>به کنجی از جهان خرسند کردش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در آن تلخی چنان برداشت با او</p></div>
<div class="m2"><p>که جز شیرین کسی نگذاشت با و</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دل خسرو به شیرین آن چنان شاد</p></div>
<div class="m2"><p>که با صد بند گفتا هستم آزاد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نشاندی ماه را گفتی میندیش</p></div>
<div class="m2"><p>که روزی هست هر کس را چنین پیش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز بادی کو کلاه از سر کند دور</p></div>
<div class="m2"><p>گیاه آسوده باشد سرو رنجور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر آنچ او فحل‌تر باشد ز نخجیر</p></div>
<div class="m2"><p>شکارافکن بدو خوشتر زند تیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو کوه از زلزله گردد به دونیم</p></div>
<div class="m2"><p>ز افتادن بلندان را بود بیم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر آن پخته که دندانش بزرگست</p></div>
<div class="m2"><p>به دنبالش بسی دندان گرگست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به هر جا کاتشی گردد زر اندود</p></div>
<div class="m2"><p>بسوی نیکوان خوشتر رود دود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تو در دستی اگر دولت شد از دست</p></div>
<div class="m2"><p>چو تو هستی همه دولت مرا هست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شکر لب نیز از او فارغ نبودی</p></div>
<div class="m2"><p>دلش دادی و خدمت می‌نمودی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>که در دولت چنین بسیار باشد</p></div>
<div class="m2"><p>گهی شادی گهی تیمار باشد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شکنج کار چون در هم نشیند</p></div>
<div class="m2"><p>بمیرد هر که در ماتم نشیند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گشاده روی باید بود یک چند</p></div>
<div class="m2"><p>که پای و سر نباید هر دو دربند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نشاید کرد بر آزار خود زور</p></div>
<div class="m2"><p>که بس بیمار وا گشت از لب گور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نه هر کش صحت او را تب نگیرد</p></div>
<div class="m2"><p>نه هر کس را که تب گیرد بمیرد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بسا قفلا که بندش ناپدید است</p></div>
<div class="m2"><p>چو وابینی نه قفل است آن کلید است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به دانائی ز دل پرداز غم را</p></div>
<div class="m2"><p>که غم‌غم را کشد چون ریگ نم را</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اگر جای تو را بگرفت بدخواه</p></div>
<div class="m2"><p>مقنع نیز داند ساختن ماه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ولی چون چاه نخشب آب گیرد</p></div>
<div class="m2"><p>جهان از آهنی کی تاب گیرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>در این کشور که هست از تیره‌رائی</p></div>
<div class="m2"><p>شبه کافور و اعمی روشنائی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بباید ساخت با هر ناپسندی</p></div>
<div class="m2"><p>که ارزد ریش گاوی ریشخندی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ستیز روزگار از شرم دور است</p></div>
<div class="m2"><p>ازو دوری طلب کازرم دور است</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دو کس را روزگار آزرم داد است</p></div>
<div class="m2"><p>یکی کو مرد و دیگر کو نزاد است</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نماند کس درین دیر سپنجی</p></div>
<div class="m2"><p>تو نیز ار هم نمانی تا نرنجی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اگر بودی جهان را پایداری</p></div>
<div class="m2"><p>بهر کس چون رسیدی شهریاری</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>فلک گر مملکت پاینده دادی</p></div>
<div class="m2"><p>ز کیخسرو به خسرو کی فتادی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کسی کو دل بر این گلزار بندد</p></div>
<div class="m2"><p>چو گل زان بیشتر گرید که خندد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>اگر دنیا نماند با تو مخروش</p></div>
<div class="m2"><p>چنان پندار کافتد بارت از دوش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ز تو یا مال ماند یا تو مانی</p></div>
<div class="m2"><p>پس آن به کو نماند تا تو مانی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو بربط هر که او شادی‌پذیر است</p></div>
<div class="m2"><p>ز درد گوشمالش ناگزیر است</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بزن چون آفتاب آتش درین دیر</p></div>
<div class="m2"><p>که بی‌عیسی نیابی در خران خیر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چه مارست اینکه چون ضحاک خونخوار</p></div>
<div class="m2"><p>هم از پشت تو انگیزد ترا مار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به شهوت ریزه‌ای کز پشت راندی</p></div>
<div class="m2"><p>عقوبت بین که چون بی‌پشت ماندی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>درین پسته منه بر پشت باری</p></div>
<div class="m2"><p>شکم‌واری طلب نه پشت‌واری</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بعنین و سترون بین که رستند</p></div>
<div class="m2"><p>که بر پشت و شکم چیزی نبستند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گرت عقلی است بی‌پیوند میباش</p></div>
<div class="m2"><p>بدانچت هست از او خرسند میباش</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نه ایمن‌تر ز خرسندی جهانیست</p></div>
<div class="m2"><p>نه به ز آسودگی نزهت سنا نیست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چو نانی هست و آبی پای درکش</p></div>
<div class="m2"><p>که هست آزاد طبعی کشوری خوش</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به خرسندی برآور سر که رستی</p></div>
<div class="m2"><p>بلائی محکم آمد سرپرستی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>همان زاهد که شد در دامن غار</p></div>
<div class="m2"><p>به خرسندی مسلم گشت از اغیار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>همان کهبد که ناپیداست در کوه</p></div>
<div class="m2"><p>به پرواز قناعت رست از انبوه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>جهان چون مار افعی پیچ پیچ است</p></div>
<div class="m2"><p>ترا آن به کزو در دست هیچ است</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو از دست تو ناید هیچ کاری</p></div>
<div class="m2"><p>به دست دیگران میگیر ماری</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو دربندی بدان میباش خرسند</p></div>
<div class="m2"><p>که تو گنجی بود گنجینه دربند</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>و گر در چاه یابی پایه خویش</p></div>
<div class="m2"><p>سعادت نامه یوسف بنه پیش</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو زیر از قدر تو جای تو باشد</p></div>
<div class="m2"><p>علم دان هر که بالای تو باشد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>تو پنداری که تو کم قدر داری</p></div>
<div class="m2"><p>توئی تو کز دو عالم صدر داری</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>دل عالم توئی در خود مبین خرد</p></div>
<div class="m2"><p>بدین همت توان گوی از جهان برد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چنان دان کایزد از خلقت گزید است</p></div>
<div class="m2"><p>جهان خاص از پی تو آفرید است</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بدین اندیشه چون دلشاد گردی</p></div>
<div class="m2"><p>ز بند تاج و تخت آزاد گردی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>و گر باشی به تخت و تاج محتاج</p></div>
<div class="m2"><p>زمین را تخت کن خورشید را تاج</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بدین تسکین ز خسرو سوز می‌برد</p></div>
<div class="m2"><p>بدین افسانه خوش خوش روز می‌برد</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>شب آمد همچنان آن سرو آزاد</p></div>
<div class="m2"><p>سخن می‌گفت و شه را دل همی داد</p></div></div>