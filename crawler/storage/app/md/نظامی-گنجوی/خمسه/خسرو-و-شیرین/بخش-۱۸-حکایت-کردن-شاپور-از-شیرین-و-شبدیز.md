---
title: >-
    بخش ۱۸ - حکایت کردن شاپور از شیرین و شبدیز
---
# بخش ۱۸ - حکایت کردن شاپور از شیرین و شبدیز

<div class="b" id="bn1"><div class="m1"><p>ندیمی خاص بودش نام شاپور</p></div>
<div class="m2"><p>جهان گشته ز مغرب تالهاور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نقاشی به مانی مژده داده</p></div>
<div class="m2"><p>به رسامی در اقلیدس گشاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قلم زن چابکی صورتگری چست</p></div>
<div class="m2"><p>که بی کلک از خیالش نقش می‌رست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان در لطف بودش آبدستی</p></div>
<div class="m2"><p>که بر آب از لطافت نقش بستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمین بوسید پیش تخت پرویز</p></div>
<div class="m2"><p>فرو گفت این سخنهای دلاویز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که گر فرمان دهد شاه جهانم</p></div>
<div class="m2"><p>بگویم صد یک از چیزی که دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشارت کرد خسرو کی جوانمرد</p></div>
<div class="m2"><p>بگو گرم و مکن هنگامه را سرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زبان بگشاد شاپور سخنگوی</p></div>
<div class="m2"><p>سخن را بهره داد از رنگ و از بوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که تا گیتیست گیتی بنده بادت</p></div>
<div class="m2"><p>زمانه سال و مه فرخنده بادت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جمالت را جوانی هم نفس باد</p></div>
<div class="m2"><p>همیشه بر مرادت دسترس باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غمین باد آنکه او شادت نخواهد</p></div>
<div class="m2"><p>خراب آنکس که آبادت نخواهد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسی گشتم درین خرگاه شش طاق</p></div>
<div class="m2"><p>شگفتی‌ها بسی دیدم در آفاق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از آن سوی کهستان منزلی چند</p></div>
<div class="m2"><p>که باشد فرضه دریای دریند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زنی فرماندهست از نسل شاهان</p></div>
<div class="m2"><p>شده جوش سپاهش تا سپاهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه اقلیم اران تا به ارمن</p></div>
<div class="m2"><p>مقرر گشته بر فرمان آن زن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ندارد هیچ مرزی بی‌خرابی</p></div>
<div class="m2"><p>همه دارد و مگر تختی و تاجی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هزارش قلعه بر کوه بلند است</p></div>
<div class="m2"><p>خزینه‌اش را خدا داند که چند است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز جنس چارپا چندان که خواهی</p></div>
<div class="m2"><p>به افزونی فزون از مرغ و ماهی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ندارد شوی و دارد کامرانی</p></div>
<div class="m2"><p>به شادی می‌گذارد زندگانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز مردان بیشتر دارد سترکی</p></div>
<div class="m2"><p>مهین بانوش خوانند از بزرگی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شمیرا نام دارد آن جهانگیر</p></div>
<div class="m2"><p>شمیرا را مهین بانوست تفسیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نشست خویش را در هر هوائی</p></div>
<div class="m2"><p>به هر فصلی مهیا کرده جائی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به فصل گل به موقان است جایش</p></div>
<div class="m2"><p>که تا سرسبز باشد خاک پایش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به تابستان شود بر کوه ارمن</p></div>
<div class="m2"><p>خرامد گل به گل خرمن به خرمن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به هنگام خزان آید به ابخاز</p></div>
<div class="m2"><p>کند در جستن نخجیر پرواز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زمستانش به بردع میل چیر است</p></div>
<div class="m2"><p>که بردع را هوای گرمسیر است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چهارش فصل ازینسان در شمار است</p></div>
<div class="m2"><p>به هر فصلی هوائیش اختیار است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نفس یک یک به شادی می‌شمارد</p></div>
<div class="m2"><p>جهان خوش خوش به بازی می‌گذارد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>درین زندانسرای پیچ بر پیچ</p></div>
<div class="m2"><p>برادرزاده‌ای دارد دگر هیچ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پری دختی پری بگذار ماهی</p></div>
<div class="m2"><p>به زیر مقنعه صاحب کلاهی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شب افروزی چو مهتاب جوانی</p></div>
<div class="m2"><p>سیه چشمی چو آب زندگانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کشیده قامتی چون نخل سیمین</p></div>
<div class="m2"><p>دو زنگی بر سر نخلش رطب چین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز بس کاورد یاد آن نوش لب را</p></div>
<div class="m2"><p>دهان پر آب شکر شد رطب را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به مروارید دندانهای چون نور</p></div>
<div class="m2"><p>صدف را آب دندان داده از دور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دو شکر چون عقیق آب داده</p></div>
<div class="m2"><p>دو گیسو چون کمند تاب داده</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خم گیسوش تاب از دل کشیده</p></div>
<div class="m2"><p>به گیسو سبزه را بر گل کشیده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شده گرم از نسیم مشک بیزش</p></div>
<div class="m2"><p>دماغ نرگس بیمار خیزش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فسونگر کرده بر خود چشم خود را</p></div>
<div class="m2"><p>زبان بسته به افسون چشم بد را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به سحری کاتش دلها کند تیز</p></div>
<div class="m2"><p>لبش را صد زبان هر صد شکر ریز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نمک دارد لبش در خنده پیوست</p></div>
<div class="m2"><p>نمک شیرین نباشد وان او هست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو گوئی بینیش تیغیست از سیم</p></div>
<div class="m2"><p>که کرد آن تیغ سیبی را به دو نیم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز ماهش صد قصب را رخنه یابی</p></div>
<div class="m2"><p>چو ماهش رخنه‌ای بر رخ نه یابی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به شمعش بر بسی پروانه بینی</p></div>
<div class="m2"><p>زنازش سوی کس پروانه بینی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>صبا از زلف و رویش حله‌پوش است</p></div>
<div class="m2"><p>گهی قاقم گهی قندز فروش است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>موکل کرده بر هر غمزه غنجی</p></div>
<div class="m2"><p>زنخ چون سیب و غبغب چون ترنجی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>رخش تقویم انجم را زده راه</p></div>
<div class="m2"><p>فشانده دست بر خورشید و بر ماه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دو پستان چون دو سیمین نار نوخیز</p></div>
<div class="m2"><p>بر آن پستان گل بستان درم ریز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز لعلش بوسه را پاسخ نخیزد</p></div>
<div class="m2"><p>که لعل اروا گشاید در بریزد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نهاده گردن آهو گردنش را</p></div>
<div class="m2"><p>به آب چشم شسته دامنش را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به چشم آهوان آن چشمه نوش</p></div>
<div class="m2"><p>دهد شیرافکنان را خواب خرگوش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هزار آغوش را پر کرده از خار</p></div>
<div class="m2"><p>یک آغوش از گلشن ناچیده دیار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شبی صد کس فزون بیند به خوابش</p></div>
<div class="m2"><p>نه بیند کس شبی چون آفتابش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر اندازه ز چشم خویش گیرد</p></div>
<div class="m2"><p>برآهوئی صد آهو بیش گیرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ز رشک نرگس مستش خروشان</p></div>
<div class="m2"><p>به بازار ارم ریحان فروشان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به عید آرای ابروی هلالی</p></div>
<div class="m2"><p>ندیدش کس که جان نسپرد حالی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به حیرت مانده مجنون در خیالش</p></div>
<div class="m2"><p>به قایم رانده لیلی با جمالش</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به فرمانی که خواهد خلق را کشت</p></div>
<div class="m2"><p>به دستش ده قلم یعنی ده انگشت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>مه از خوبیش خود را خال خوانده</p></div>
<div class="m2"><p>شب از خالش کتاب فال خوانده</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز گوش و گردنش لولو خروشان</p></div>
<div class="m2"><p>که رحمت بر چنان لولو فروشان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>حدیثی و هزار آشوب دلبند</p></div>
<div class="m2"><p>لبی و صد هزاران بوسه چون قند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سر زلفی ز ناز و دلبری پر</p></div>
<div class="m2"><p>لب و دندانی از یاقوت و از در</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از آن یاقوت و آن در شکر خند</p></div>
<div class="m2"><p>مفرح ساخته سودائیی چند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>خرد سرگشته بر روی چو ماهش</p></div>
<div class="m2"><p>دل و جان فتنه بر زلف سیاهش</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>هنر فتنه شده بر جان پاکش</p></div>
<div class="m2"><p>نبشته عهده عنبر به خاکش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>رخش نسرین و بویش نیز نسرین</p></div>
<div class="m2"><p>لبش شیرین و نامش نیز شیرین</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>شکر لفظان لبش را نوش خوانند</p></div>
<div class="m2"><p>ولیعهد مهین بانوش دانند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>پریرویان کزان کشور امیرند</p></div>
<div class="m2"><p>همه در خدمتش فرمان پذیرند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز مهتر زادگان ماه پیکر</p></div>
<div class="m2"><p>بود در خدمتش هفتاد دختر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بخوبی هر یکی آرام جانی</p></div>
<div class="m2"><p>به زیبائی دلاویز جهانی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>همه آراسته با رود و جامند</p></div>
<div class="m2"><p>چو مه منزل به منزل می‌خرامند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گهی بر خرمن مه مشک پوشند</p></div>
<div class="m2"><p>گهی در خرمن گل باده نوشند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز برقع نیستشان بر روی بندی</p></div>
<div class="m2"><p>که نارد چشم زخم آنجا گزندی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بخوبی در جهان یاری ندارند</p></div>
<div class="m2"><p>به گیتی جز طرب کاری ندارند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چو باشد وقت زور آن زورمندان</p></div>
<div class="m2"><p>کنند از شیر چنگ از پیل دندان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به حمله جان عالم را بسوزند</p></div>
<div class="m2"><p>به ناوک چشم کوکب را بدوزند</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>اگر حور بهشتی هست مشهور</p></div>
<div class="m2"><p>بهشت است آن طرف وان لعتبان حور</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>مهین بانو که آن اقلیم دارد</p></div>
<div class="m2"><p>بسی زینگونه زر و سیم دارد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بر آخر بسته دارد ره نوردی</p></div>
<div class="m2"><p>کز او در تک نیابد باد گردی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>سبق برده ز وهم فیلسوفان</p></div>
<div class="m2"><p>چو مرغابی نترسد زاب طوفان</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>به یک صفرا که بر خورشید رانده</p></div>
<div class="m2"><p>فلک را هفت میدان باز مانده</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>به گاه کوه کندن آهنین سم</p></div>
<div class="m2"><p>گه دریا بریدن خیز ران دم</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>زمانه گردش و اندیشه رفتار</p></div>
<div class="m2"><p>چو شب کارآگه و چون صبح بیدار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نهاده نام آن شبرنگ شبدیز</p></div>
<div class="m2"><p>بر او عاشق‌تر از مرغ شب آویز</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>یکی زنجیر زر پیوسته دارد</p></div>
<div class="m2"><p>بدان زنجیر پایش بسته دارد</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>نه شیرین‌تر ز شیرین خلق دیدم</p></div>
<div class="m2"><p>نه چون شبدیز شبرنگی شنیدم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چو بر گفت این سخن شاپور هشیار</p></div>
<div class="m2"><p>فراغت خفته گشت و عشق بیدار</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>یکایک مهر بر شیرین نهادند</p></div>
<div class="m2"><p>بدان شیرین زبان اقرار دادند</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>که استادی که در چین نقش بندد</p></div>
<div class="m2"><p>پسندیده بود هرچ او پسندد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چنان آشفته شد خسرو بدان گفت</p></div>
<div class="m2"><p>کزان سودا نیاسود و نمی‌خفت</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>همه روز این حکایت باز می‌جست</p></div>
<div class="m2"><p>جز این تخم از دماغش برنمی‌رست</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>در این اندیشه روزی چند می‌بود</p></div>
<div class="m2"><p>به خشک افسانه‌ای خرسند می‌بود</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چو کار از دست شد دستی بر آورد</p></div>
<div class="m2"><p>صبوری را به سرپائی در آورد</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>به خلوت داستان خواننده را خواند</p></div>
<div class="m2"><p>بسی زین داستان با وی سخن راند</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بدو گفت ای به کار آمد وفادار</p></div>
<div class="m2"><p>به کار آیم کنون کز دست شد کار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>چو بنیادی بدین خوبی نهادی</p></div>
<div class="m2"><p>تمامش کن که مردی اوستادی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>مگو شکر حکایت مختصر کن</p></div>
<div class="m2"><p>چو گفتی سوی خوزستان گذر کن</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>ترا باید شد چون بت‌پرستان</p></div>
<div class="m2"><p>به دست آوردن آن بت را به دستان</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>نظر کردن که در دل دارد؟</p></div>
<div class="m2"><p>سر پیوند مردم زاد دارد؟</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>اگر چون موم نقش می‌پذیرد</p></div>
<div class="m2"><p>بر او زن مهر ما تا نقش گیرد</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ور آهن دل بود منشین و بر گرد</p></div>
<div class="m2"><p>خبر ده تا نکوبم آهن سرد</p></div></div>