---
title: >-
    بخش ۲۲ - نمودن شاپور صورت خسرو را بار سوم
---
# بخش ۲۲ - نمودن شاپور صورت خسرو را بار سوم

<div class="b" id="bn1"><div class="m1"><p>شباهنگام کاین عنقای فرتوت</p></div>
<div class="m2"><p>شکم پر کرد ازین یک دانه یاقوت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دشت انجرک آرام کردند</p></div>
<div class="m2"><p>بنوشانوش می‌در جام کردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن صحرا فرو خفتند سرمست</p></div>
<div class="m2"><p>ریاحین زیر پای و باده بر دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو روز از دامن شب سر برآورد</p></div>
<div class="m2"><p>زمانه تاج زرین بر سر آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آن پیروزه تخت آن تاجداران</p></div>
<div class="m2"><p>رها کردند می بر جرعه خواران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز آنجا تا در دیر پری سوز</p></div>
<div class="m2"><p>پریدند آن پریرویان به یک روز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن مینوی میناگون چمیدند</p></div>
<div class="m2"><p>فلک را رشته در مینا کشیدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بساطی سبز چون جان خردمند</p></div>
<div class="m2"><p>هوائی معتدل چون مهر فرزند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نسیمی خوشتر از باد بهشتی</p></div>
<div class="m2"><p>زمین را در به دریا گل به کشتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شقایق سنگ را بتخانه کرده</p></div>
<div class="m2"><p>صبا جعد چمن را شانه کرده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مسلسل گشته بر گلهای حمری</p></div>
<div class="m2"><p>نوای بلبل و آواز قمری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پرنده مرغکان گستاخ گستاخ</p></div>
<div class="m2"><p>شمایل بر شمایل شاخ بر شاخ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهر گوشه دو مرغک گوش بر گوش</p></div>
<div class="m2"><p>زده بر گل صلای نوش بر نوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدان گلشن رسید آن نقش پرداز</p></div>
<div class="m2"><p>همان نقش نخستین کرد آغاز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پری پیکر چو دید آن سبزه خوش</p></div>
<div class="m2"><p>به می بنشست با جمعی پریوش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دگر ره دید چشم مهربانش</p></div>
<div class="m2"><p>در آن صورت که بود آرام جانش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شگفتی ماند از آن نیرنگ سازی</p></div>
<div class="m2"><p>گذشت اندیشه کارش ز بازی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دل سرگشته را دنبال برداشت</p></div>
<div class="m2"><p>به پای خود شد آن تمثال برداشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در آن آیینه دید از خود نشانی</p></div>
<div class="m2"><p>چو خود را یافت بی‌خود شد زمانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنان شد در سخن ناساز گفتن</p></div>
<div class="m2"><p>کزان گفتن نشاید باز گفتن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لعاب عنکبوتان مگس گیر</p></div>
<div class="m2"><p>همائی را نگر چون کرد نخجیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در آن چشمه که دیوان خانه کردند</p></div>
<div class="m2"><p>پری را بین که چون دیوانه کردند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به چاره هر کجا تدبیر سازند</p></div>
<div class="m2"><p>نه مردم دیو را نخجیر سازند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو آن گل برگ رویان بر سر خاک</p></div>
<div class="m2"><p>گل صد برگ را دیدند غمناک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدانستند کان کار پری نیست</p></div>
<div class="m2"><p>عجب کاریست کاری سرسری نیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از آن پیشه پشیمانی گرفتند</p></div>
<div class="m2"><p>بر آن صورت ثناخوانی گرفتند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که سر بازی کنیم و جان فشانیم</p></div>
<div class="m2"><p>مگر کاحوال صورت باز دانیم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو شیرین دید که ایشان راستگویند</p></div>
<div class="m2"><p>به چاره راست کردن چاره جویند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به یاری خواستن بنمود زاری</p></div>
<div class="m2"><p>که یاران را ز یارانست یاری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ترا از یار نگریزد بهر کار</p></div>
<div class="m2"><p>خدای است آنکه بی مثل است و بی یار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بسا کارا که از یاری برآید</p></div>
<div class="m2"><p>به باید یار تا کاری برآید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدان بت پیکران گفت آن دلارام</p></div>
<div class="m2"><p>کز این پیکر شدم بی‌صبر و آرام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بیا تا این حدیث از کس نپوشیم</p></div>
<div class="m2"><p>بدین تمثال نوشین باده نوشیم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دگر باره نشاط آغاز کردند</p></div>
<div class="m2"><p>می‌آوردند و عشرت ساز کردند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پیاپی شد غزلهای فراقی</p></div>
<div class="m2"><p>بر آمد بانک نوشا نوش ساقی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بت شیرین نبید تلخ در دست</p></div>
<div class="m2"><p>از آن تلخی و شیرینی جهان مست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بهر نوبت که می‌بر لب نهادی</p></div>
<div class="m2"><p>زمین را پیش صورت بوسه دادی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو مستی عاشقی را تنگ‌تر کرد</p></div>
<div class="m2"><p>صبوری در زمان آهنگ در کرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یکی را زان بتان بنشاند در راه</p></div>
<div class="m2"><p>که هر کس را که بینی بر گذرگاه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نظر کن تا درین سامان چو پوید</p></div>
<div class="m2"><p>وزین صورت به پرسش تا چه گوید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بسی پرسیده شد پنهان و پیدا</p></div>
<div class="m2"><p>نمی‌شد سر آن صورت هویدا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تن شیرین گرفت از رنج سستی</p></div>
<div class="m2"><p>کز آن صورت ندادش کس درستی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در آن اندوه می‌پیچید چون مار</p></div>
<div class="m2"><p>فشاند از جزعها لولوی شهوار</p></div></div>