---
title: >-
    بخش ۶۷ - رفتن خسرو سوی قصر شیرین به بهانه شکار
---
# بخش ۶۷ - رفتن خسرو سوی قصر شیرین به بهانه شکار

<div class="b" id="bn1"><div class="m1"><p>چو عالم برزد آن زرین‌علم را</p></div>
<div class="m2"><p>کز او تاراج باشد خیل غم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک را رغبت نخجیر برخاست</p></div>
<div class="m2"><p>ز طالع تهمت تقصیر برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به فالی چون رخ شیرین همایون</p></div>
<div class="m2"><p>شهنشه سوی صحرا رفت بیرون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خروش کوس و بانگ نای برخاست</p></div>
<div class="m2"><p>زمین چون آسمان از جای برخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علمداران علم بالا کشیدند</p></div>
<div class="m2"><p>دلیران رخت در صحرا کشیدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برون آمد مهین شهسواران</p></div>
<div class="m2"><p>پیاده در رکابش تاجداران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز یک‌سو دست در زین بسته فغفور</p></div>
<div class="m2"><p>ز دیگر سو سپه‌سالار قیصور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کمر دربسته و ابروگشاده</p></div>
<div class="m2"><p>کلاه کیقبادی کژ نهاده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نهاده غاشیه‌اش خورشید بر دوش</p></div>
<div class="m2"><p>رکابش کرده مه را حلقه در گوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درفش کاویانی بر سر شاه</p></div>
<div class="m2"><p>چو لختی ابر که‌افتد بر سر ماه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کمر شمشیرهای زرنگارش</p></div>
<div class="m2"><p>به گرد اندر شده زرین حصارش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نبود از تیغ‌ها پیرامن شاه</p></div>
<div class="m2"><p>به یک میدان کسی را پیش و پس راه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در آن بیشه که بود از تیر و شمشیر</p></div>
<div class="m2"><p>زبان گاو برده زهره شیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دهان دور باش از خنده می‌سفت</p></div>
<div class="m2"><p>فلک را دور باش از دور می‌گفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سواد چتر زرین باز بر سر</p></div>
<div class="m2"><p>چو بر مشکین‌حصاری برجی از زر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر افتادی سر یک سوزن از میغ</p></div>
<div class="m2"><p>نبودی جای سوزن جز سر تیغ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نفیر چاوشان از دور شو دور</p></div>
<div class="m2"><p>ز گیتی چشم بد را کرده مهجور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>طراق مقرعه بر خاک و بر سنگ</p></div>
<div class="m2"><p>ادب کرده زمین را چند فرسنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زمین از بار آهن خم‌گرفته</p></div>
<div class="m2"><p>هوا را از روا رو دم گرفته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جنیبت‌کش وشاقان سرایی</p></div>
<div class="m2"><p>روانه صدصد از هر سو جدایی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>غریو کوس‌ها بر کوهه پیل</p></div>
<div class="m2"><p>گرفته کوه و صحرا میل در میل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز حلقوم دراهای درفشان</p></div>
<div class="m2"><p>مشبک‌های زرین عنبرافشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صد و پنجاه سقا در سپاهش</p></div>
<div class="m2"><p>به آب گل همی شستند راهش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صد و پنجاه مجمردار دلکش</p></div>
<div class="m2"><p>فکنده بوی‌های خوش در آتش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هزاران طرف زرین طوق‌بسته</p></div>
<div class="m2"><p>همه میخ درستک‌ها شکسته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدان تا هر کجا کو اسب راند</p></div>
<div class="m2"><p>به هر کامی درستی بازماند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>غریبی گر گذر کردی بر آن راه</p></div>
<div class="m2"><p>بدانستی که کرد آنجا گذر شاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدین آیین چو بیرون آمد از شهر</p></div>
<div class="m2"><p>به استقبالش آمد گردش دهر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شده بر عارض لشکر جهان تنگ</p></div>
<div class="m2"><p>که شاهنشه کجا می‌دارد آهنگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنین فرمود خورشید جهانگیر</p></div>
<div class="m2"><p>که خواهم کرد روزی چند نخجیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو در نالیدن آمد طبلک باز</p></div>
<div class="m2"><p>درآمد مرغ صیدافکن به پرواز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روان شد در هوا باز سبک‌پر</p></div>
<div class="m2"><p>جهان خالی شد از کبک و کبوتر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی هفته در آن کوه و بیابان</p></div>
<div class="m2"><p>نرستند از عقابینش عقابان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پیاپی هر زمان نخجیر می‌کرد</p></div>
<div class="m2"><p>به نخجیری دگر تدبیر می‌کرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بنه در یک شکارستان نمی‌ماند</p></div>
<div class="m2"><p>شکارافکن شکارافکن همی راند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وز آنجا همچنان بر دست زیرین</p></div>
<div class="m2"><p>رکاب افشاند سوی قصر شیرین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وز آنجا همچنان بر دست زیرین</p></div>
<div class="m2"><p>رکاب افشاند سوی قصر شیرین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به یک فرسنگی قصر دل‌آرام</p></div>
<div class="m2"><p>فرود آمد چو باده در دل جام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شب از عنبر جهان را کله می‌بست</p></div>
<div class="m2"><p>زمستان بود و باد سرد می‌جست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زمین کز سردی آتش داشت در زیر</p></div>
<div class="m2"><p>پرند آب را می‌کرد شمشیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر چه جای باشد گرمسیری</p></div>
<div class="m2"><p>نشاید کرد با سرما دلیری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ملک فرمود که‌آتش برفروزند</p></div>
<div class="m2"><p>به من عنبر به خرمن عود سوزند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بخورانگیز شد عود قماری</p></div>
<div class="m2"><p>هوا می‌کرد خود کافورباری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به آسایش توانا شد تن شاه</p></div>
<div class="m2"><p>غنود از اول شب تا سحرگاه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو لعل آفتاب از کان بر آمد</p></div>
<div class="m2"><p>ز عشق روز شب را جان بر آمد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>فلک سرمست بود از پویه چون پیل</p></div>
<div class="m2"><p>خناق شب کبودش کرد چون نیل</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>طبیبان شفق مدخل گشادند</p></div>
<div class="m2"><p>فلک را سرخی از اکحل گشادند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ملک ز آرامگه برخاست شادان</p></div>
<div class="m2"><p>نشاط آغاز کرد از بامدادان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نبیذی چند خورد از دست ساقی</p></div>
<div class="m2"><p>نماند از شادمانی هیچ باقی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو آشوب نبیذش در سر افتاد</p></div>
<div class="m2"><p>تقاضای مرادش در بر افتاد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>برون شد مست و بر شبدیز بنشست</p></div>
<div class="m2"><p>سوی قصر نگارین راند سرمست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دل از مستی شده رقاص با او</p></div>
<div class="m2"><p>غلامی چند خاص الخاص با او</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خبر کردند شیرین را رقیبان</p></div>
<div class="m2"><p>که اینک خسرو آمد بی‌نقیبان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دل پاکش ز ننگ و نام ترسید</p></div>
<div class="m2"><p>وزان پرواز بی‌هنگام ترسید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>حصار خویش را درداد بستن</p></div>
<div class="m2"><p>رقیبی چند را بر در نشستن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به دست هر یک از بهر نثارش</p></div>
<div class="m2"><p>یکی خوان زر که بی‌حد بد شمارش</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز مقراضی و چینی بر گذرگاه</p></div>
<div class="m2"><p>یکی میدان بساط افکند بر راه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>همه ره را طراز گنج بردوخت</p></div>
<div class="m2"><p>گلاب افشاند و خود چون عود می‌سوخت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به بام قصر برشد چون یکی ماه</p></div>
<div class="m2"><p>نهاده گوش بر در دیده بر راه</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز هر نوک مژه کرده سنانی</p></div>
<div class="m2"><p>بر او از خون نشانده دیده‌بانی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>برآمد گردی از ره توتیارنگ</p></div>
<div class="m2"><p>که روشن‌چشم از او شد چشمه در سنگ</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>برون آمد ز گرد آن صبح روشن</p></div>
<div class="m2"><p>پدید آمد از آن گلخانه گلشن</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در آن مشعل که برد از شمع‌ها نور</p></div>
<div class="m2"><p>چراغ انگشت بر لب مانده از دور</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>خدنگی رسته از زین خدنگش</p></div>
<div class="m2"><p>که شمشاد آب گشت از آب و رنگش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>مرصع‌پیکری در نیمه دوش</p></div>
<div class="m2"><p>کلاه خسروی بر گوشه گوش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>رخی چون سرخ‌گل نوبر دمیده</p></div>
<div class="m2"><p>خطی چون غالیه گردش کشیده</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گرفته دسته نرگس به دستش</p></div>
<div class="m2"><p>به خوش‌خوابی چو نرگس‌های مستش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>گلش زیر عرق غواص گشته</p></div>
<div class="m2"><p>تذروش زیر گل رقاص گشته</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>کمربندان به گردش دسته بسته</p></div>
<div class="m2"><p>به دست هر یک از گل دسته دسته</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو شیرین دید خسرو را چنان مست</p></div>
<div class="m2"><p>ز پای افتاد و شد یک‌باره از دست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ز بیهوشی زمانی بی‌خبر ماند</p></div>
<div class="m2"><p>به هوش آمد به کار خویش درماند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>که گر نگذارم اکنون در وثاقش</p></div>
<div class="m2"><p>ندارم طاقت زخم فراقش</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>و گر لختی ز تندی رام گردم</p></div>
<div class="m2"><p>چو ویسه در جهان بدنام گردم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بکوشم تا خطا پوشیده باشم</p></div>
<div class="m2"><p>چو نتوانم نه من کوشیده باشم؟</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چو شاه آمد نگهبانان دویدند</p></div>
<div class="m2"><p>زر افشاندند و دیباها کشیدند</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بسا ناگشته را کز در درآرند</p></div>
<div class="m2"><p>سپهر و دور بین تا در چه کارند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ملک بر فرش دیباهای گل‌رنگ</p></div>
<div class="m2"><p>جنیبت راند و سوی قصر شد تنگ</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>دری دید آهنین در سنگ بسته</p></div>
<div class="m2"><p>ز حیرت ماند بر در دل‌شکسته</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نه روی آنکه از در بازگردد</p></div>
<div class="m2"><p>نه رای آنکه قفل‌انداز گردد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>رقیبی را به نزد خویشتن خواند</p></div>
<div class="m2"><p>که ما را نازنین بر در چرا ماند</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چه تلخی دید شیرین در من آخر</p></div>
<div class="m2"><p>چرا در بست از این‌سان بر من آخر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>درون شو گو نه شاهنشه غلامی</p></div>
<div class="m2"><p>فرستاده‌است نزدیکت پیامی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>که مهمانی به خدمت می‌گراید</p></div>
<div class="m2"><p>چه فرمایی درآید یا نیاید</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>تو که‌اندر لب نمک پیوسته داری</p></div>
<div class="m2"><p>به مهمان‌بر چرا در بسته داری</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>درم بگشای کاخر پادشاهم</p></div>
<div class="m2"><p>به پای خویشتن عذر تو خواهم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>تو خود دانی که من از هیچ رایی</p></div>
<div class="m2"><p>ندارم با تو در خاطر خطایی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بباید با منت دمساز گشتن</p></div>
<div class="m2"><p>تو را نادیده نتوان بازگشتن</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>و گر خواهی که اینجا کم نشینم</p></div>
<div class="m2"><p>رها کن کز سر پایت ببینم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بدین زاری پیامی شاه می‌گفت</p></div>
<div class="m2"><p>شکرلب می‌شنید و آه می‌گفت</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>کنیزی کاردان را گفت آن ماه</p></div>
<div class="m2"><p>به خدمت خیز و بیرون رو سوی شاه</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>فلان شش طاق دیبا را برون بر</p></div>
<div class="m2"><p>بزن با طاق این ایوان برابر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ز خار و خاره خالی کن میانش</p></div>
<div class="m2"><p>معطر کن به مشک و زعفرانش</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بساط گوهرین در وی بگستر</p></div>
<div class="m2"><p>بیار آن کرسی شش‌پایه زر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بنه در پیشگاه و شقه دربند</p></div>
<div class="m2"><p>پس آنگه شاه را گو که‌ای خداوند</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>نه ترک این سرا هندوی این بام</p></div>
<div class="m2"><p>شهنشه را چنین داده‌است پیغام</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>پرستار تو شیرین هوس‌جفت</p></div>
<div class="m2"><p>به لفظ من شهنشه را چنین گفت</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>که گر مهمان مایی ناز منمای</p></div>
<div class="m2"><p>به هر جا کت فرود آرم فرود آی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>صواب آن شد ز روی پیش‌بینی</p></div>
<div class="m2"><p>که امروزی در این منظر نشینی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>من آیم خود به خدمت بر سر کاخ</p></div>
<div class="m2"><p>زمین بوسم به نیروی تو گستاخ</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>بگوییم آنچه ما را گفت باید</p></div>
<div class="m2"><p>چو گفتیم آن کنیم آنگه که شاید</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>کنیز کاردان بیرون شد از در</p></div>
<div class="m2"><p>برون برد آنچه فرمود آن سمنبر</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>همه ترتیب کرد آیین زربفت</p></div>
<div class="m2"><p>فرود آورد خسرو را و خود رفت</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>رخ شیرین ز خجلت گشته پرخوی</p></div>
<div class="m2"><p>که نزل شاه چون سازد پیاپی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چو از نزل زرافشانی بپرداخت</p></div>
<div class="m2"><p>ز جلاب و شکر نزلی دگر ساخت</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>به دست چاشنی‌گیری چو مهتاب</p></div>
<div class="m2"><p>فرستادش ز شربت‌های جلاب</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>پس آنگه ماه را پیرایه بربست</p></div>
<div class="m2"><p>نقاب آفتاب از سایه بربست</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>فرو پوشید گلناری پرندی</p></div>
<div class="m2"><p>بر او هر شاخ گیسو چون کمندی</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>کمندی حلقه‌وار افکنده بر دوش</p></div>
<div class="m2"><p>ز هر حلقه جهانی حلقه در گوش</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>حمایل‌پیکری از زر کانی</p></div>
<div class="m2"><p>کشیده بر پرندی ارغوانی</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>سرآغوشی برآموده به گوهر</p></div>
<div class="m2"><p>به رسم چینیان افکنده بر سر</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>سیه‌شعری چو زلف عنبرافشان</p></div>
<div class="m2"><p>فرود آویخت بر ماه درفشان</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بدین طاوس‌کرداری همایی</p></div>
<div class="m2"><p>روان شد چون تذروی در هوایی</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>نشاط دلبری در سر گرفته</p></div>
<div class="m2"><p>نیازی دیده نازی درگرفته</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>سوی دیوار قصر آمد خرامان</p></div>
<div class="m2"><p>زمین بوسید شه را چون غلامان</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>گشاد از گوش گوهرکش بسی لعل</p></div>
<div class="m2"><p>سم شبدیز را کرد آتشین‌نعل</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>همان صد دانه مروارید خوشاب</p></div>
<div class="m2"><p>به فرق‌افشان خسرو کرد پرتاب</p></div></div>