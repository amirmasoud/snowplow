---
title: >-
    بخش ۴۶ - وصیت کردن مهین بانو شیرین را
---
# بخش ۴۶ - وصیت کردن مهین بانو شیرین را

<div class="b" id="bn1"><div class="m1"><p>مهین بانو دلش دادی شب و روز</p></div>
<div class="m2"><p>بدان تا نشکند ماه دل افروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی روزش به خلوت پیش خود خواند</p></div>
<div class="m2"><p>که عمرش آستین بر دولت افشاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کلید گنج‌ها دادش که برگیر</p></div>
<div class="m2"><p>که پیشت مرد خواهد مادر پیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آمد کار اندامش به سستی</p></div>
<div class="m2"><p>به بیماری کشید از تن‌درستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو روزی چند بر وی رنج شد چیر</p></div>
<div class="m2"><p>تن از جان سیر شد جان از جهان سیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان از جان شیرینش جدا کرد</p></div>
<div class="m2"><p>به شیرین هم جهان هم جان رها کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرو شد آفتابش در سیاهی</p></div>
<div class="m2"><p>بنه در خاک برد از تخت شاهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین است آفرینش را ولایت</p></div>
<div class="m2"><p>که باشد هر بهاری را نهایت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیامد شیشه‌ای از سنگ در دست</p></div>
<div class="m2"><p>که باز آن شیشه را هم سنگ نشکست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فغان زین چرخ کز نیرنگ‌سازی</p></div>
<div class="m2"><p>گهی شیشه کند گه شیشه‌بازی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به اول عهد زنبور انگبین کرد</p></div>
<div class="m2"><p>به آخر عهد باز آن انگبین خورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدین قالب که بادش در کلاهست</p></div>
<div class="m2"><p>مشو غره که مشتی خاک راهست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بادی کو کلاه از سر کند دور</p></div>
<div class="m2"><p>گیاه آسوده باشد سرو رنجور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدین خان کو بنا بر باد دارد</p></div>
<div class="m2"><p>مشو غره که بد بنیاد دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چه می‌پیچی درین دام گلوپیچ</p></div>
<div class="m2"><p>که جوزی پوده بینی در میان هیچ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو روباهان و خرگوشان منه گوش</p></div>
<div class="m2"><p>به روبه‌بازی این خواب خرگوش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بسا شیر شکار و گرگ جنگی</p></div>
<div class="m2"><p>که شد در زیر این روبه پلنگی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نظر کردم ز روی تجربت هست</p></div>
<div class="m2"><p>خوشی‌های جهان چون خارش دست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به اول دست را خارش خوش افتد</p></div>
<div class="m2"><p>به آخر دست بر دست آتش افتد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همیدون جام گیتی خوشگوار است</p></div>
<div class="m2"><p>به اول مستی و آخر خمار است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رها کن غم که دنیا غم نیرزد</p></div>
<div class="m2"><p>مکن شادی که شادی هم نیرزد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر خواهی جهان در پیش کردن</p></div>
<div class="m2"><p>شکم‌واری نخواهی بیش خوردن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرت صد گنج هست ار یک درم نیست</p></div>
<div class="m2"><p>نصیبت زین جهان جز یک شکم نیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همی تا پای دارد تن‌درستی</p></div>
<div class="m2"><p>ز سختی‌ها نگیرد طبع سستی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو برگردد مزاج از استقامت</p></div>
<div class="m2"><p>به دشواری به دست آید سلامت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دهان چندان نماید نوش‌خندی</p></div>
<div class="m2"><p>که یابد در طبیعت نوشمندی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو گیرد ناامیدی مرد را گوش</p></div>
<div class="m2"><p>کند راه رهائی را فراموش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جهان تلخ است خوی تلخناکش</p></div>
<div class="m2"><p>به کم خوردن توان رست از هلاکش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مشو پرخواره چون کرمان در این گور</p></div>
<div class="m2"><p>به کم خوردن کمر دربند چون مور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز کم خوردن کسی را تب نگیرد</p></div>
<div class="m2"><p>ز پر خوردن به روزی صد بمیرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>حرام آمد علف تاراج کردن</p></div>
<div class="m2"><p>به دارو طبع را محتاج کردن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو باشد خوردن نان گلشکروار</p></div>
<div class="m2"><p>نباشد طبع را با گلشکر کار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو گلبن هر چه بگذاری بخندد</p></div>
<div class="m2"><p>چو خوردی گر شکر باشد بگندد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو دنیا را نخواهی چند جوئی</p></div>
<div class="m2"><p>بدو پوئی بد او چند گوئی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>غم دنیا کسی در دل ندارد</p></div>
<div class="m2"><p>که در دنیا چو ما منزل ندارد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>درین صحرا کسی کو جای‌گیر است</p></div>
<div class="m2"><p>ز مشتی آب و نانش ناگزیر است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مکن دلتنگی ای شخصت گلی تنگ</p></div>
<div class="m2"><p>که بد باشد دلی تنگ و گلی تنگ</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جهان از نام آن کس ننگ دارد</p></div>
<div class="m2"><p>که از بهر جهان دلتنگ دارد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>غم روزی مخور تا روز ماند</p></div>
<div class="m2"><p>که خود روزی‌رسان روزی رساند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فلک با این همه ناموس و نیرنگ</p></div>
<div class="m2"><p>شب و روز ابلقی دارد کهن لنگ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بر این ابلق که آمد شد گزیند</p></div>
<div class="m2"><p>چو این آمد فرود آن برنشیند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در این سیلاب غم کز ما پدر برد</p></div>
<div class="m2"><p>پسر چون زنده ماند چون پدر مرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کسی کو خون هندوئی بریزد</p></div>
<div class="m2"><p>چو وارث باشد آن خون برنخیزد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چه فرزندی تو با این ترکتازی</p></div>
<div class="m2"><p>که هندوی پدرکش را نوازی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بزن تیری بدین کوژ کمان‌پشت</p></div>
<div class="m2"><p>که چندین پشت بر پشت تو را کشت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>فلک را تا کمان بی‌زه نگردد</p></div>
<div class="m2"><p>شکار کس در او فربه نگردد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گوزنی را که ره بر شیر باشد</p></div>
<div class="m2"><p>گیا در زیر پی شمشیر باشد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تو ایمن چون شدی بر ماندن خویش</p></div>
<div class="m2"><p>که داری باد در پس چاه در پیش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مباش ایمن که این دریای خاموش</p></div>
<div class="m2"><p>نکرده‌است آدمی خوردن فراموش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کدامین ربع را بینی ربیعی</p></div>
<div class="m2"><p>کزان بقعه برون ناید بقیعی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>جهان آن به که دانا تلخ گیرد</p></div>
<div class="m2"><p>که شیرین زندگانی تلخ میرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کسی کز زندگی با درد و داغ است</p></div>
<div class="m2"><p>به وقت مرگ خندان چون چراغ است</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سرانی کز چنین سر پرفسوسند</p></div>
<div class="m2"><p>چون گل گردن زنان را دست‌بوسند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اگر واعظ بود گوید که چون کاه</p></div>
<div class="m2"><p>تو بفکن تا منش بردارم از راه</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>و گر زاهد بود صد مرده کوشد</p></div>
<div class="m2"><p>که تو بیرون کنی تا او بپوشد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو نامد در جهان پاینده چیزی</p></div>
<div class="m2"><p>همه ملک جهان نرزد پشیزی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ره‌آورد عدم ره‌توشه خاک</p></div>
<div class="m2"><p>سرشت صافی آمد گوهر پاک</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چنین گفتند دانایان هشیار</p></div>
<div class="m2"><p>که نیک و بد به مرگ آید پدیدار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بسا زن نام کانجا مرد یابی</p></div>
<div class="m2"><p>بسا مردا که رویش زرد یابی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>خداوندا چو آید پای بر سنگ</p></div>
<div class="m2"><p>فتد کشتی در آن گردابه تنگ</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نظامی را به آسایش رسانی</p></div>
<div class="m2"><p>ببخشی و به بخشایش رسانی</p></div></div>