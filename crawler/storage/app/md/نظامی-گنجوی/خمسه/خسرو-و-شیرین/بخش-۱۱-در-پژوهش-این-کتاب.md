---
title: >-
    بخش ۱۱ - در پژوهش این کتاب
---
# بخش ۱۱ - در پژوهش این کتاب

<div class="b" id="bn1"><div class="m1"><p>مرا چون هاتف دل دید دمساز</p></div>
<div class="m2"><p>بر آورد از رواق همت آواز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بشتاب ای نظامی زود دیرست</p></div>
<div class="m2"><p>فلک بد عهد و عالم زود سیرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهاری نو برآر از چشمه نوش</p></div>
<div class="m2"><p>سخن را دست بافی تازه در پوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در این منزل بهمت ساز بردار</p></div>
<div class="m2"><p>درین پرده به وقت آواز بردار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمین سازند اگر بی‌وقت رانی</p></div>
<div class="m2"><p>سراندازند اگر بی‌وقت خوانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبان بگشای چون گل روزکی چند</p></div>
<div class="m2"><p>کز این کردند سوسن را زبان‌بند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن پولاد کن چون سکه زر</p></div>
<div class="m2"><p>بدین سکه درم را سکه می‌بر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نخست آهنگری باتیغ بنمای</p></div>
<div class="m2"><p>پس آنگه صیقلی را کارفرمای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخن کان از سر اندیشه ناید</p></div>
<div class="m2"><p>نوشتن را و گفتن را نشاید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخن را سهل باشد نظم دادن</p></div>
<div class="m2"><p>بباید لیک بر نظم ایستادن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سخن بسیار داری اندکی کن</p></div>
<div class="m2"><p>یکی را صد مکن صد را یکی کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو آب از اعتدال افزون نهد گام</p></div>
<div class="m2"><p>ز سیرابی به غرق آرد سرانجام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو خون در تن عادت بیش گردد</p></div>
<div class="m2"><p>سزای گوشمال نیش گردد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سخن کم گوی تا بر کار گیرند</p></div>
<div class="m2"><p>که در بسیار بد بسیار گیرند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترا بسیار گفتن گر سلیم است</p></div>
<div class="m2"><p>مگو بسیار دشنامی عظیم است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سخن جانست و جان داروی جانست</p></div>
<div class="m2"><p>مگر چون جان عزیز از بهر آنست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو مردم بین که چون بیرای و هوشند</p></div>
<div class="m2"><p>که جانی را به نانی می‌فروشند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سخن گوهر شد و گوینده غواص</p></div>
<div class="m2"><p>به سختی در کف آید گوهر خاص</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز گوهر سفتن استادان هراسند</p></div>
<div class="m2"><p>که قیمت مندی گوهر شناسند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه بینی وقت سفتن مرد حکاک</p></div>
<div class="m2"><p>به شاگردان دهد در خطرناک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر هشیار اگر مخمور باشی</p></div>
<div class="m2"><p>چنان زی کز تعرض دور باشی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هزارت مشرف بی‌جامگی هست</p></div>
<div class="m2"><p>به صد افغان کشیده سوی تو دست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به غفلت بر میاور یک نفس را</p></div>
<div class="m2"><p>مدان غافل ز کار خویش کس را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نصیحت‌های هاتف چون شنیدم</p></div>
<div class="m2"><p>چون هاتف روی در خلوت کشیدم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در آن خلوت که دل دریاست آنجا</p></div>
<div class="m2"><p>همه سرچشمه‌ها آنجاست آنجا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نهادم تکیه گاه افسانه‌ای را</p></div>
<div class="m2"><p>بهشتی کردم آتش خانه‌ای را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو شد نقاش این بتخانه دستم</p></div>
<div class="m2"><p>جز آرایش بر او نقشی نبستم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر چه در سخن کاب حیاتست</p></div>
<div class="m2"><p>بود جایز هر آنچه از ممکنات است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو بتوان راستی را درج کردن</p></div>
<div class="m2"><p>دروغی را چه باید خرج کردن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز کژ گوئی سخن را قدر کم گشت</p></div>
<div class="m2"><p>کسی کو راستگو شد محتشم گشت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو صبح صادق آمد راست گفتار</p></div>
<div class="m2"><p>جهان در زر گرفتش محتشم‌وار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو سرو از راستی بر زد علم را</p></div>
<div class="m2"><p>ندید اندر خزان تاراج غم را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرا چون مخزن‌الاسرار گنجی</p></div>
<div class="m2"><p>چه باید در هوس پیمود رنجی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ولیکن در جهان امروز کس نیست</p></div>
<div class="m2"><p>که او را درهوس نامه هوس نیست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هوس پختم به شیرین دستکاری</p></div>
<div class="m2"><p>هوس ناکان غم را غمگساری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنان نقش هوس بستم بر او پاک</p></div>
<div class="m2"><p>که عقل از خواندنش گردد هوسناک</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه در شاخی زدم چون دیگران دست</p></div>
<div class="m2"><p>که بروی جز رطب چیزی توان بست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حدیث خسرو و شیرین نهان نیست</p></div>
<div class="m2"><p>وزان شیرین‌تر الحق داستان نیست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر چه داستانی دلپسند است</p></div>
<div class="m2"><p>عروسی در وقایه شهربند است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بیاضش در گزارش نیست معروف</p></div>
<div class="m2"><p>که در بردع سوادش بود موقوف</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز تاریخ کهن سالان آن بوم</p></div>
<div class="m2"><p>مرا این گنج نامه گشت معلوم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کهن سالان این کشور که هستند</p></div>
<div class="m2"><p>مرا بر شقه این شغل بستند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نیارد در قبولش عقل سستی</p></div>
<div class="m2"><p>که پیش عاقلان دارد درستی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نه پنهان بر درستیش آشکار است</p></div>
<div class="m2"><p>اثرهائی کز ایشان یادگار است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اساس بیستون و شکل شبدیز</p></div>
<div class="m2"><p>همیدون در مداین کاخ پرویز</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هوسکاری آن فرهاد مسکین</p></div>
<div class="m2"><p>نشان جوی شیر و قصر شیرین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همان شهر و دو آب خوشگوارش</p></div>
<div class="m2"><p>بنای خسرو و جای شکارش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>حدیث باربد با ساز دهرود</p></div>
<div class="m2"><p>همان آرام گاه شه به شهرود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>حکیمی کاین حکایت شرح کردست</p></div>
<div class="m2"><p>حدیث عشق از ایشان طرح کردست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو در شصت اوفتادش زندگانی</p></div>
<div class="m2"><p>خدنگ افتادش از شست جوانی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به عشقی در که شست آمد پسندش</p></div>
<div class="m2"><p>سخن گفتن نیامد سودمندش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نگفتم هر چه دانا گفت از آغاز</p></div>
<div class="m2"><p>که فرخ نیست گفتن گفته را باز</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در آن جزوی که ماند از عشقبازی</p></div>
<div class="m2"><p>سخن راندم نیت بر مرد غازی</p></div></div>