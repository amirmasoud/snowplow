---
title: >-
    بخش ۹ - خطاب زمین بوس
---
# بخش ۹ - خطاب زمین بوس

<div class="b" id="bn1"><div class="m1"><p>زهی دارنده اورنگ شاهی</p></div>
<div class="m2"><p>حوالتگاه تایید الهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پناه سلطنت، پشت خلافت</p></div>
<div class="m2"><p>ز تیغت تا عدم، موئی مسافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فریدون دوم، جمشید ثانی</p></div>
<div class="m2"><p>غلط گفتم که حشو است این معانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریدون بود طفلی گاو پرورد</p></div>
<div class="m2"><p>تو بالغ دولتی هم شیر و هم مرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ستد جمشید را، جان، مارِ ضحاک</p></div>
<div class="m2"><p>ترا جان بخشد اژدرهای افلاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ایشان داشتندی تخت با تاج</p></div>
<div class="m2"><p>تو تاج و تخت می‌بخشی به محتاج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کند هر پهلوی خسرو نشانی</p></div>
<div class="m2"><p>تو خود هم خسروی هم پهلوانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سلیمان را نگین بود و ترا دین</p></div>
<div class="m2"><p>سکندر داشت آیینه، تو آیین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندیدند آنچه تو دیدی ز ایام</p></div>
<div class="m2"><p>سکندر ز آینه، جمشید از جام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهی ملک جوانی خرم از تو</p></div>
<div class="m2"><p>اساس زندگانی محکم از تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر صد تخت خود بر پشت پیل است</p></div>
<div class="m2"><p>چو بی نقش تو باشد تخت نیل است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به تیغِ آهنین عالم گرفتی</p></div>
<div class="m2"><p>به زرین جام، جای جم گرفتی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به آهن چون فراهم شد خزینه</p></div>
<div class="m2"><p>از آهن وقف کن بر آبگینه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به دستوری حدیثی چند کوتاه</p></div>
<div class="m2"><p>بخواهم گفت اگر فرمان دهد شاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من از سِحرِ سَحَر پیکان راهم</p></div>
<div class="m2"><p>جرس جنبانِ هارورتانِ شاهم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نخستین مرغ بودم من درین باغ</p></div>
<div class="m2"><p>گرم بلبل کنی کینت و گر زاغ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به عرض بندگی دیر آمدم دیر</p></div>
<div class="m2"><p>و گر دیر آمدم شیر آمدم شیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه خوش گفت این سخن پیر جهانگرد</p></div>
<div class="m2"><p>که دیر آی و درست آی ای جوانمرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در این اندیشه بودم مدتی چند</p></div>
<div class="m2"><p>که نزلی سازم از بهر خداوند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نبودم تحفهٔ چیپال و فغفور</p></div>
<div class="m2"><p>که پیش آرم زمین را بوسم از دور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدین مشتی خیال فکرت انگیز</p></div>
<div class="m2"><p>بساط بوسه را کردم شکر ریز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر چه مور قربان را نشاید</p></div>
<div class="m2"><p>ملخ نزل سلیمان را نشاید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نبود آبی جز این در مغز میغم</p></div>
<div class="m2"><p>و گر بودی نبودی جان دریغم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به ذره آفتابی را که گیرد</p></div>
<div class="m2"><p>به گنجشکی عقابی را که گیرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه سود افسوس من کز کدخدائی</p></div>
<div class="m2"><p>جز این موئی ندارم در کیائی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حدیث آنکه چون دل گاه و بیگاه</p></div>
<div class="m2"><p>ملازم نیستم در حضرتِ شاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نباشد بر ملک پوشیده رازم</p></div>
<div class="m2"><p>که من جز با دعا با کس نسازم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نظامی اکدشی خلوت نشین است</p></div>
<div class="m2"><p>که نیمی سرکه نیمی انگبین است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز طبع‌ِ تر گشاده چشمهٔ نوش</p></div>
<div class="m2"><p>به زهد خشک، بسته بار بر دوش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دهان زهدم ار چه خشک خانی است</p></div>
<div class="m2"><p>لسان رَطبَم، آب زندگانی است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو مشک از ناف عزلت بو گرفتم</p></div>
<div class="m2"><p>به تنهائی چو عنقا خو گرفتم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گل بزم از چو من خاری نیاید</p></div>
<div class="m2"><p>ز من غیر از دعا کاری نیاید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ندانم کرد خدمتهای شاهی</p></div>
<div class="m2"><p>مگر لختی سجود صبحگاهی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رعونت در دماغ از دام ترسم</p></div>
<div class="m2"><p>طمع در دل ز کار خام ترسم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>طمع را خرقه بر خواهم کشیدن</p></div>
<div class="m2"><p>رعونت را قبا خواهم دریدن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>من و عشقی مجرد باشم آنگاه</p></div>
<div class="m2"><p>بیاسایم چو مفرد باشم آنگاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سر خود را به فتراکت سپارم</p></div>
<div class="m2"><p>ز فتراکت چو دولت سر بر آرم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گرم دور افکنی در بوسم از دور</p></div>
<div class="m2"><p>و گر بنوازیَم نُورٌ علی نور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به یک خنده گرت باید چو مهتاب</p></div>
<div class="m2"><p>شب افروزی کنم چون کِرمِ شبتاب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو دولت هر که را دادی به خود راه</p></div>
<div class="m2"><p>نِبِشتی بر سرش یا میر یا شاه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو چشم صبح در هر کس که دیدی</p></div>
<div class="m2"><p>پلاسِ ظلمت ازوی در کشیدی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به هر کشور که چون خورشید راندی</p></div>
<div class="m2"><p>زمین را بدره بدره زر فشاندی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زر افشانت همه ساله چنین باد</p></div>
<div class="m2"><p>چو تیغت حصن جانت آهنین باد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جهان بیرون مباد از حکم و رایت</p></div>
<div class="m2"><p>زمین خالی مباد از خاکِ پایت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سرت زیر کلاه خسروی باد</p></div>
<div class="m2"><p>به خسرو زادگان پشتت قوی باد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به هر منزل که مشک افشان کنی راه</p></div>
<div class="m2"><p>منور باش چون خورشید و چون ماه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به هر جانب که روی آری به تقدیر</p></div>
<div class="m2"><p>رکابت باد چون دولت، جهانگیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جنابت بر همه آفاق منصور</p></div>
<div class="m2"><p>سپاهت قاهر و اعدات مقهور</p></div></div>