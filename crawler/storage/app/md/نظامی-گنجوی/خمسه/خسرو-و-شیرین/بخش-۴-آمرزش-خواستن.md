---
title: >-
    بخش ۴ - آمرزش خواستن
---
# بخش ۴ - آمرزش خواستن

<div class="b" id="bn1"><div class="m1"><p>خدایا چون گِلِ ما را سرشتی</p></div>
<div class="m2"><p>وثیقت نامه‌ای بر ما نوشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ما بر خدمت خود عرض کردی</p></div>
<div class="m2"><p>جزای آن به خود بر فرض کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو ما با ضعف خود دربند آنیم</p></div>
<div class="m2"><p>که بگزاریم خدمت تا توانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو با چندان عنایت‌ها که داری</p></div>
<div class="m2"><p>ضعیفان را کجا ضایع گذاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدین امیدهای شاخ در شاخ</p></div>
<div class="m2"><p>کرم‌های تو ما را کرد گستاخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>و گرنه ما کدامین خاک باشیم</p></div>
<div class="m2"><p>که از دیوار تو رنگی تراشیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلاصی ده که روی از خود بتابیم</p></div>
<div class="m2"><p>به خدمت کردنت توفیق یابیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز ما خود خدمتی شایسته ناید</p></div>
<div class="m2"><p>که شادروان عزت را بشاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ولی چون بندگیمان گوشه گیر است</p></div>
<div class="m2"><p>ز خدمت بندگان را ناگزیر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر خواهی به ما خط در کشیدن</p></div>
<div class="m2"><p>ز فرمانت که یارد سر کشیدن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>و گر گردی ز مشتی خاک خشنود</p></div>
<div class="m2"><p>تورا نبود زیان ما را بود سود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در آن ساعت که ما مانیم و هوئی</p></div>
<div class="m2"><p>ز بخشایش فرو مگذار موئی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیامرز از عطای خویش ما را</p></div>
<div class="m2"><p>کرامت کن لقای خویش ما را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من آن خاکم که مغزم دانهٔ تست</p></div>
<div class="m2"><p>بدین شمعی دلم پروانهٔ تست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>توئی کاوَّل ز خاکم آفریدی</p></div>
<div class="m2"><p>به فضلم زآفرینش بر گزیدی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو روی افروختی چشمم برافروز</p></div>
<div class="m2"><p>چو نعمت دادیَم شکرم در آموز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به سختی، صبر ده، تا پای دارم</p></div>
<div class="m2"><p>در آسانی مکن فرموش کارم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شناسا کن به حکمتهای خویشم</p></div>
<div class="m2"><p>برافکن برقع غفلت ز پیشم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هدایت را ز من پرواز مستان</p></div>
<div class="m2"><p>چو اول دادی آخر باز مستان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به تقصیری که از حد بیش کردم</p></div>
<div class="m2"><p>خجالت را شفیع خویش کردم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بهر سهوی که در گفتارم افتد</p></div>
<div class="m2"><p>قلم در کش کزین بسیارم افتد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رهی دارم به هفتاد و دو هنجار</p></div>
<div class="m2"><p>از آن یک ره گل و هفتاد و دو خار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عقیدم را در آن ره کش عماری</p></div>
<div class="m2"><p>که هست آن راه، راه رستگاری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو را جویم ز هر نقشی که دانم</p></div>
<div class="m2"><p>تو مقصودی ز هر حرفی که خوانم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز سرگردانی تست اینکه پیوست</p></div>
<div class="m2"><p>بهر نااهل و اهلی می‌زنم دست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به عزم خدمتت برداشتم پای</p></div>
<div class="m2"><p>گر از ره یاوه گشتم راه بنمای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نیت بر کعبه آورد است جانم</p></div>
<div class="m2"><p>اگر در بادیه میرم ندانم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بهر نیک و بدی کاندر میانه است</p></div>
<div class="m2"><p>کرم بر تست و آن دیگر بهانه است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یکی را پای بشکستی و خواندی</p></div>
<div class="m2"><p>یکی را بال و پردادی و راندی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ندانم تا من مسکین کدامم</p></div>
<div class="m2"><p>ز محرومان و مقبولان چه نامم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگر دین دارم و گر بت پرستم</p></div>
<div class="m2"><p>بیامرزم بهر نوعی که هستم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به فضل خویش کن فضلی مرا یار</p></div>
<div class="m2"><p>به عدل خود مکن با فعل من کار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ندارد فعل من آن زور بازو</p></div>
<div class="m2"><p>که با عدل تو باشد هم ترازو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بلی از فعل من فضل تو بیش است</p></div>
<div class="m2"><p>اگر بنوازیَم بر جای خویش است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به خدمت خاص کن خرسندیَم را</p></div>
<div class="m2"><p>به کس مگذار حاجتمندیَم را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنان دارم که در نابود و در بود</p></div>
<div class="m2"><p>چنان باشم کزو باشی تو خشنود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فراغم ده ز کار این جهانی</p></div>
<div class="m2"><p>چو افتد کار با تو خود تو دانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>منه بیش از کشش تیمار بر من</p></div>
<div class="m2"><p>به قدر زور من نه بار بر من</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چراغم را ز فیض خویش ده نور</p></div>
<div class="m2"><p>سرم را زآستان خود مکن دور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دل مست مرا هشیار گردان</p></div>
<div class="m2"><p>ز خواب غفلتم بیدار گردان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چنان خسبان چو آید وقت خوابم</p></div>
<div class="m2"><p>که گر ریزد گلم ماند گلابم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زبانم را چنان ران بر شهادت</p></div>
<div class="m2"><p>که باشد ختم کارم بر سعادت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تنم را در قناعت زنده دل دار</p></div>
<div class="m2"><p>مزاجم را بطاعت معتدل دار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو حکمی راند خواهی یا قضائی</p></div>
<div class="m2"><p>به تسلیم آفرین در من رضائی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دماغ دردمندم را دوا کن</p></div>
<div class="m2"><p>دواش از خاک پای مصطفی کن</p></div></div>