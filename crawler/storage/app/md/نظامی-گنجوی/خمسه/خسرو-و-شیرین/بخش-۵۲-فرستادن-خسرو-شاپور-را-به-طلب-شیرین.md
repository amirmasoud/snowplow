---
title: >-
    بخش ۵۲ - فرستادن خسرو شاپور را به طلب شیرین
---
# بخش ۵۲ - فرستادن خسرو شاپور را به طلب شیرین

<div class="b" id="bn1"><div class="m1"><p>شفاعت کرد روزی شه به شاپور</p></div>
<div class="m2"><p>که تا کی باشم از دلدار خود دور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیار آن ماه را یک شب درین برج</p></div>
<div class="m2"><p>که پنهان دارمش چون لعل در درج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از بهر صلاح دولت خویش</p></div>
<div class="m2"><p>نیارم رغبتی کردن بدو بیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که ترسم مریم از بس ناشکیبی</p></div>
<div class="m2"><p>چو عیسی برکشد خود را صلیبی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همان بهتر که با آن ماه دلدار</p></div>
<div class="m2"><p>نهفته دوستی ورزم پری‌وار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر چه سوخته پایم ز راهش</p></div>
<div class="m2"><p>چو دست سوخته دارم نگاهش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر این شوخ آن پری‌رخ را ببیند</p></div>
<div class="m2"><p>شود دیوی و بر دیوی نشیند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پذیرفتار فرمان گشت نقاش</p></div>
<div class="m2"><p>که بندم نقش چین را در تو خوش باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به قصر آمد چو دریایی پر از جوش</p></div>
<div class="m2"><p>که باشد موج آن دریا همه نوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حکایت کرد با شیرین سرآغاز</p></div>
<div class="m2"><p>که وقت آمد که بر دولت کنی ناز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ملک را در شکارت رخش تند است</p></div>
<div class="m2"><p>ولیک از مریمش شمشیر کند است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن او را چنین آزرم دارد</p></div>
<div class="m2"><p>که از پیمان قیصر شرم دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیا تا یک سواره برنشینیم</p></div>
<div class="m2"><p>ره مشگوی خسرو برگزینیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طرب می‌ساز با خسرو نهانی</p></div>
<div class="m2"><p>سر آید خصم را دولت چو دانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بت تنها نشین ماه تهی‌رو</p></div>
<div class="m2"><p>تهی از خویشتن تنها ز خسرو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به تندی برزد آوازی به شاپور</p></div>
<div class="m2"><p>که از خود شرم دار ای از خدا دور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مگو چندین که مغزم را برفتی</p></div>
<div class="m2"><p>کفایت کن تمام است آنچه گفتی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه هر گوهر که پیش آید توان سفت</p></div>
<div class="m2"><p>نه هرچ آن بر زبان آید توان گفت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه هر آبی که پیش آید توان خورد</p></div>
<div class="m2"><p>نه هرچ از دست برخیزد توان کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نیاید هیچ از انصاف تو یادم</p></div>
<div class="m2"><p>به بی‌انصافیت انصاف دادم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از این صنعت خدا دوری دهادت</p></div>
<div class="m2"><p>خرد ز این کار دستوری دهادت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر آوردی مرا از شهریاری</p></div>
<div class="m2"><p>کنون خواهی که از جانم برآری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>من از بی‌دانشی در غم فتادم</p></div>
<div class="m2"><p>شدم خشک از غم اندر نم فتادم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در آن جان گر ز من بودی یکی سوز</p></div>
<div class="m2"><p>به گیسو رفتمی راهش شب و روز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خر از دکان پالان‌گر گریزد</p></div>
<div class="m2"><p>چو بیند جوفروش از جای خیزد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کسادی چون کشم گوهرنژادم</p></div>
<div class="m2"><p>نخوانده چون روم آخر نه بادم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو ز آب حوض تر گشته‌است زینم</p></div>
<div class="m2"><p>خطا باشد که در دریا نشینم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه فرمائی دلی با این خرابی</p></div>
<div class="m2"><p>کنم با اژدهایی هم‌نقابی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو آن درگاه را درخور نیفتم</p></div>
<div class="m2"><p>به زور آن به که از در درنیفتم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ببین تا چند بار اینجا فتادم</p></div>
<div class="m2"><p>به غمخواری و خواری دل نهادم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نیفتاد آن رفیق بی‌وفا را</p></div>
<div class="m2"><p>که بفرستد سلامی خشک ما را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به یک گز مقنعه تا چند کوشم</p></div>
<div class="m2"><p>سلیح مردمی تا چند پوشم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>روا نبود که چون من زن‌شماری</p></div>
<div class="m2"><p>کله‌داری کند با تاجداری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>قضای بد نگر که‌آمد مرا پیش</p></div>
<div class="m2"><p>خسک بر خستگی و خار بر ریش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به گل چیدن بدم در خار ماندم</p></div>
<div class="m2"><p>به کاری می‌شدم در بار ماندم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو خود بد کردم از کس چون خروشم</p></div>
<div class="m2"><p>خطای خود ز چشم بد چه پوشم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یکی را گفتم این جان و جهان است</p></div>
<div class="m2"><p>جهان بستد کنون دربند جان است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه هرکس که آتشی گوید زبانش</p></div>
<div class="m2"><p>بسوزاند تف آتش دهانش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ترازو را دو سر باشد نه یک سر</p></div>
<div class="m2"><p>یکی جو در حساب آرد یکی زر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ترازویی که ما را داد خسرو</p></div>
<div class="m2"><p>یکی سر دارد آن هم نیز پر جو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دلم زان جو که خرباری ندارد</p></div>
<div class="m2"><p>به غیر از خوردنش کاری ندارد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نمانم جز عروسی را در این سنگ</p></div>
<div class="m2"><p>که از گچ کرده باشندش به نیرنگ</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>عروس گچ شبستان را نشاید</p></div>
<div class="m2"><p>ترنج موم ریحان را نشاید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بسی کردم شگرفی‌ها که شاید</p></div>
<div class="m2"><p>که گویم وز توام شرمی نیاید</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چه کرد آن رهزن خونخواره من</p></div>
<div class="m2"><p>جز آتش پاره‌ای در باره من</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>من اینک زنده او با یار دیگر</p></div>
<div class="m2"><p>ز مهر انگیخته بازار دیگر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اگر خود روی من رویی است از سنگ</p></div>
<div class="m2"><p>در او بیند فروریزد از این ننگ</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گرفتم سگ صفت کردندم آخر</p></div>
<div class="m2"><p>به شیر سگ نپروردندم آخر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سگ از من به بود گر تا توانم</p></div>
<div class="m2"><p>فریبش را چو سگ از در نرانم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شوم پیش سگ اندازم دلی را</p></div>
<div class="m2"><p>که خواهد سگ دل بی‌حاصلی را</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دل آن به کو بدان کس وانبیند</p></div>
<div class="m2"><p>که در سگ بیند و در ما نبیند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مرا خود کاشکی مادر نزادی</p></div>
<div class="m2"><p>و گر زادی بخورد سگ بدادی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بیا تا کژ نشینم راست گویم</p></div>
<div class="m2"><p>چه خواری‌ها کز او نامد برویم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هزاران پرده بستم راست در کار</p></div>
<div class="m2"><p>هنوزم پرده کژ می‌دهد یار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شد آبم و او به مویی تر نیامد</p></div>
<div class="m2"><p>چنان کابی به آبی بر نیامد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چگونه راست آید رهزنی را</p></div>
<div class="m2"><p>که ریزد آبروی چون منی را</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>فرس با من چنان در جنگ رانده‌است</p></div>
<div class="m2"><p>که جای آشتی‌رنگی نمانده‌است</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو ما را نیست پشمی در کلاهش</p></div>
<div class="m2"><p>کشیدم پشم در خیل و سپاهش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز بس سر زیر او بردن خمیدم</p></div>
<div class="m2"><p>ز بس تار غمش خود را ندیدم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دلم کور است و بینائی گزیند</p></div>
<div class="m2"><p>چه کوری دل چه آن کس کو نبیند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سرم می‌خارد و پروا ندارم</p></div>
<div class="m2"><p>که در عشقش سر خود را بخارم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زبانم خود چنین پرزخم از آن است</p></div>
<div class="m2"><p>که هرچ او می‌دهد زخم زبان است</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سزد گر با من او همدم نباشد</p></div>
<div class="m2"><p>ز کس بختم نبد زو هم نباشد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بدین بختم چون او همخوابه باید</p></div>
<div class="m2"><p>کز او سرسام را گرمابه پاید</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دلم می‌جست و دانستم کز ایام</p></div>
<div class="m2"><p>زیانی دید خواهم کام و ناکام</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بلی هست آزموده در نشان‌ها</p></div>
<div class="m2"><p>که هر کش دل جهد بیند زیان‌ها</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>کنونم می‌جهد چشم گهربار</p></div>
<div class="m2"><p>چه خواهم دید بسم‌الله دگربار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>مرا زین قصر بیرون گر بهشت است</p></div>
<div class="m2"><p>نباید رفت اگرچه سرنبشت است</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گر آید دختر قیصر نه شاپور</p></div>
<div class="m2"><p>ازین قصرش به رسوائی کنم دور</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به دستان می‌فریبندم نه مستم</p></div>
<div class="m2"><p>نیارند از ره دستان به دستم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>اگر هوش مرا در دل ندانند</p></div>
<div class="m2"><p>من آن دانم که در بابل ندانند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>سر اینجا به بود سرکش نه آنجا</p></div>
<div class="m2"><p>که نعل اینجاست در آتش نه آنجا</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>اگر خسرو نه کیخسرو بود شاه</p></div>
<div class="m2"><p>نباید کردنش سر پنجه با ماه</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به ار پهلو کند زین نرگس مست</p></div>
<div class="m2"><p>نهد پیشم چو سوسن دست بر دست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>و گر با جوش گرمم برستیزد</p></div>
<div class="m2"><p>چنان جوشم کز او جوشن بریزد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>فرستم زلف را تا یک فن آرد</p></div>
<div class="m2"><p>شکیبش را رسن در گردن آرد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بگویم غمزه را تا وقت شبگیر</p></div>
<div class="m2"><p>سمندش را به رقص آرد به یک تیر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ز گیسو مشک بر آتش فشانم</p></div>
<div class="m2"><p>چو عودش بر سر آتش نشانم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ز تاب زلف خویش آرم به تابش</p></div>
<div class="m2"><p>فرو بندم به سحر غمزه خوابش</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>خیالم را بفرمایم که در خواب</p></div>
<div class="m2"><p>بدین خاکش دواند تیز چون آب</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>مرا بگذار تا گریم بدین روز</p></div>
<div class="m2"><p>تو مادر مرده را شیون میاموز</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>منم کز یاد او پیوسته شادم</p></div>
<div class="m2"><p>که او در عمرها نارد به یادم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ز مهرم گرد او بویی نگردد</p></div>
<div class="m2"><p>غم من بر دلش مویی نگردد</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>گر آن نامهربان از مهر سیر است</p></div>
<div class="m2"><p>زمانه بر چنین بازی دلیر است</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>شکیبائی کنم چندان که یک روز</p></div>
<div class="m2"><p>درآید از در مهر آن دل‌افروز</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>کمند دل در آن سرکش چه پیچم</p></div>
<div class="m2"><p>رسن در گردن آتش چه پیچم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>زمینم من به قدر او آسمان‌وار</p></div>
<div class="m2"><p>زمین را کی بود با آسمان کار</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>کند با جنس خود هر جنس پرواز</p></div>
<div class="m2"><p>کبوتر با کبوتر باز با باز</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>نشاید باد را در خاک بستن</p></div>
<div class="m2"><p>نه باهم آب و آتش را نشستن</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>چو وصلش نیست از هجران چه ترسم</p></div>
<div class="m2"><p>تنی نازنده از زندان چه ترسم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بود سرمایه‌داران را غم بار</p></div>
<div class="m2"><p>تهیدست ایمن است از دزد و طرار</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>نه آن مرغم که بر من کس نهد قید</p></div>
<div class="m2"><p>نه هر بازی تواند کردنم صید</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>گر آید خسرو از بتخانه چین</p></div>
<div class="m2"><p>ز شورستان نیابد شهد شیرین</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>اگر شبدیز توسن را تکی هست</p></div>
<div class="m2"><p>ز تیزی نیز گلگون را رگی هست</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>و گر مریم درخت قند کشته‌است</p></div>
<div class="m2"><p>رطب‌های مرا مریم سرشته‌است</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>گر او را دعوی صاحب‌کلاهی است</p></div>
<div class="m2"><p>مرا نیز از قصب سربند شاهی است</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>نخواهم کردن این تلخی فراموش</p></div>
<div class="m2"><p>که جان شیرین کند مریم کند نوش</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>یکی در جست و دریا در کمین یافت</p></div>
<div class="m2"><p>یکی سرکه طلب کرد انگبین یافت</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>همه ساله نباشد سینه بر دست</p></div>
<div class="m2"><p>به هرجا گرد رانی گردنی هست</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>نبودم عاشق ار بودم به تقدیر</p></div>
<div class="m2"><p>پشیمانم خطا کردم چه تدبیر</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>مزاحی کردم او درخواست پنداشت</p></div>
<div class="m2"><p>دروغی گفتم او خود راست پنداشت</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>دل من هست از این بازار بی‌زار</p></div>
<div class="m2"><p>قسم خواهی به دادار و به دیدار</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>سخن را رشته بس باریک رشتم</p></div>
<div class="m2"><p>و گرچه در شب تاریک رشتم</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چنین تا کی چو موم افسرده باشم</p></div>
<div class="m2"><p>برافروزم و گر نه مرده باشم</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>به نفرینش نگویم خیر و شر هیچ</p></div>
<div class="m2"><p>خداوندا تو می‌دانی دگر هیچ</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>لب آنکس را دهم کو را نیاز است</p></div>
<div class="m2"><p>نه دستی راست حلوا کان دراز است؟</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بهاری را که بر خاکش فشانی</p></div>
<div class="m2"><p>از آن به کش برد باد خزانی</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>گرفتار سگان گشتن به نخجیر</p></div>
<div class="m2"><p>به از افسوس شیران زبون‌گیر</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>بیا گو گر منت باید چو مردان</p></div>
<div class="m2"><p>به پای خود کسی رنجه مگردان</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>هژبرانی که شیران شکارند</p></div>
<div class="m2"><p>به پای خود پیام خود گذارند</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>چو دولت پای‌بست اوست پایم</p></div>
<div class="m2"><p>به پای دیگران خواندن نیایم</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>به دوش دیگران زنبیل سایند؟</p></div>
<div class="m2"><p>به دندان کسان زنجیر خایند؟</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>چه تدبیر از پی تدبیر کردن</p></div>
<div class="m2"><p>نخواهم خویشتن را پیر کردن</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>به پیری می خورم؟ بادم قدح خرد</p></div>
<div class="m2"><p>که هنگام رحیل آخور زند کرد</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>به نادانی درافتادم بدین دام</p></div>
<div class="m2"><p>به دانائی برون آیم سرانجام</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>مگر نشنیدی از جادوی جوزن</p></div>
<div class="m2"><p>که داند دود هر کس راه روزن</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>مرا این رنج و این تیمار دیدن</p></div>
<div class="m2"><p>ز دل باید نه از دلدار دیدن</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>همه جا دزد از بیگانه خیزد</p></div>
<div class="m2"><p>مرا بنگر که دزد از خانه خیزد</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>به افسون از دل خود رست نتوان</p></div>
<div class="m2"><p>که دزد خانه را در بست نتوان</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>چو کوران گر نه لعل از سنگ پرسم</p></div>
<div class="m2"><p>چرا ده بینم و فرسنگ پرسم</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>دل من در حق من رای بد زد</p></div>
<div class="m2"><p>به دست خود تبر بر پای خود زد</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>دلی دارم کز او حاصل ندارم</p></div>
<div class="m2"><p>مرا آن به که دل با دل ندارم</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>دلم ظالم شد و یارم ستمکار</p></div>
<div class="m2"><p>ازین دل بی‌دلم زین یار بی‌یار</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>شدم دلشاد روزی با دل‌افروز</p></div>
<div class="m2"><p>از آن روز اوفتادستم بدین روز</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>غم روزی خورد هرکس به تقدیر</p></div>
<div class="m2"><p>چو من غم‌روزی اوفتادم چه تدبیر</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>نهان تا کی کنم سوزی به سوزی</p></div>
<div class="m2"><p>به سر تا کی برم روزی به روزی</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>مرا کز صبر کردن تلخ شد کام</p></div>
<div class="m2"><p>سزد گر لعبت صبرم نهی نام</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>اگر دورم ز گنج و کشور خویش</p></div>
<div class="m2"><p>نه آخر هستم آزاد سر خویش</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>نشاید حکم کردن بر دو بنیاد</p></div>
<div class="m2"><p>یکی بر بی‌طمع دیگر بر آزاد</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>وزان پس مهر لولو بر شکر زد</p></div>
<div class="m2"><p>به عناب و طبرزد بانگ بر زد</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>که گر شه گوید او را دوست دارم</p></div>
<div class="m2"><p>بگو کاین عشوه ناید در شمارم</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>و گر گوید بدان صبحم نیاز است</p></div>
<div class="m2"><p>بگو بیدار منشین شب دراز است</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>و گر گوید به شیرین کی رسم باز</p></div>
<div class="m2"><p>بگو با روزه مریم همی ساز</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>و گر گوید بدان حلوا کشم دست</p></div>
<div class="m2"><p>بگو رغبت به حلوا کم کند مست</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>و گر گوید کشم تنگش در آغوش</p></div>
<div class="m2"><p>بگو کاین آرزو بادت فراموش</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>و گر گوید کنم زان لب شکرریز</p></div>
<div class="m2"><p>بگو دور از لبت دندان مکن تیز</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>و گر گوید بگیرم زلف و خالش</p></div>
<div class="m2"><p>بگو تا ها! نگیری ها! ممالش</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>و گر گوید نهم رخ بر رخ ماه</p></div>
<div class="m2"><p>بگو با رخ برابر چون شود شاه</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>و گر گوید ربایم زان زنخ گوی</p></div>
<div class="m2"><p>بگو چوگان خوری زان زلف بر روی</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>و گر گوید بخایم لعل خندان</p></div>
<div class="m2"><p>بگو از دور می‌خور آب دندان</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>گر از فرمان من سر برگراید</p></div>
<div class="m2"><p>بگو فرمان فراقت راست شاید</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>فراقش گر کند گستاخ‌بینی</p></div>
<div class="m2"><p>بگو برخیزمت یا می‌نشینی</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>وصالش گر بگوید زان اویم</p></div>
<div class="m2"><p>بگو خاموش باشی تا نگویم</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>فرومی‌خواند ازین مشتی فسانه</p></div>
<div class="m2"><p>در او تهدیدهای مادگانه</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>عتابش گرچه می‌زد شیشه بر سنگ</p></div>
<div class="m2"><p>عقیقش نرخ می‌برید در جنگ</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>چو بر شاپور تندی زد خمارش</p></div>
<div class="m2"><p>ز رنج دل سبک‌تر گشت بارش</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>به نرمی گفت کای مرد سخنگوی</p></div>
<div class="m2"><p>سخن در مغز تو چون آب در جوی</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>اگر وقتی کنی بر شه سلامی</p></div>
<div class="m2"><p>بدان حضرت رسان از من پیامی</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>که شیرین گوید ای بدمهر بدعهد</p></div>
<div class="m2"><p>کجا آن صحبت شیرین‌تر از شهد</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>مرا ظن بود کز من برنگردی</p></div>
<div class="m2"><p>خریدار بتی دیگر نگردی</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>کنون در خود خطا کردی ظنم را</p></div>
<div class="m2"><p>که در دل جای کردی دشمنم را</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>ازین بیداد دل در داد بادت</p></div>
<div class="m2"><p>ز آه تلخ شیرین یاد بادت</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>چو بخت خفته یاری را نشائی</p></div>
<div class="m2"><p>چو دوران سازگاری را نشائی</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>بدین خواری مجویم گر عزیزم</p></div>
<div class="m2"><p>خط آزادیم ده گر کنیزم</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>ترا من همسرم در همنشینی</p></div>
<div class="m2"><p>به چشم زیردستانم چه بینی</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>چنین در پایه زیرم مکن جای</p></div>
<div class="m2"><p>وگرنه بر درت بالا نهم پای</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>به پلپل دانه‌های اشک جوشان</p></div>
<div class="m2"><p>دوانم بر در خویشت خروشان</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>نداری جز مراد خویشتن کار</p></div>
<div class="m2"><p>نباید بود ازینسان خویشتن‌دار</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>چو تو دل بر مراد خویش داری</p></div>
<div class="m2"><p>مراد دیگران کی پیش داری</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>مرا تا خار در ره می‌شکستی</p></div>
<div class="m2"><p>کمان در کار ده ده می‌شکستی</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>بخار تلخ شیرین بود گستاخ</p></div>
<div class="m2"><p>چو شیرین شد رطب خار است بر شاخ</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>به باغ افکندت پالود خونم</p></div>
<div class="m2"><p>چو بر بگرفت باغ از در برونم</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>نگشتم ز آتشت گرم ای دل‌افروز</p></div>
<div class="m2"><p>به دودت کور می‌کردم شب و روز</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>جفا زین بیش؟ که اندامم شکستی</p></div>
<div class="m2"><p>چو نام‌آور شدی نامم شکستی</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>عمل‌داران چو خود را ساز بینند</p></div>
<div class="m2"><p>به معزولان ازین به بازبینند</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>به معزولی به چشمم درنشستی</p></div>
<div class="m2"><p>چو عامل گشتی از من چشم بستی</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>به آب دیده کشتی چند رانم</p></div>
<div class="m2"><p>وصالت را به یاری چند خوانم</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>چو بی‌یار آمدی من بودمت یار</p></div>
<div class="m2"><p>چو در کاری نباشد با منت کار</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>چو کارم را به رسوائی فکندی</p></div>
<div class="m2"><p>سپر بر آب رعنائی فکندی</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>برات کشتنم را ساز دادی</p></div>
<div class="m2"><p>به آسیب فراقم باز دادی</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>نماند از جان من جز رشته تایی</p></div>
<div class="m2"><p>مکش کین رشته سر دارد به جایی</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>مزن شمشیر بر شیرین مظلوم</p></div>
<div class="m2"><p>ترا آن بس که راندی نیزه بر روم</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>چو نقش کارگاه رومیت هست</p></div>
<div class="m2"><p>ز رومی کار ارمن دور کن دست</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>ز باغ روم گل داری به خرمن</p></div>
<div class="m2"><p>مکن تاراج تخت و تاج ارمن</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>مکن کز گرمی آتش زود خیزد</p></div>
<div class="m2"><p>وز آتش ترسم آنگه دود خیزد</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>هزار از بهر می خوردن بود یار</p></div>
<div class="m2"><p>یکی از بهر غم خوردن نگه‌دار</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>مرا در کار خود رنجور داری</p></div>
<div class="m2"><p>کشی در دام و دامن دور داری</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>خسک بر دامن دوران میفشان</p></div>
<div class="m2"><p>نمک بر جان مهجوران میفشان</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>ترا در بزم شاهان خوش برد خواب</p></div>
<div class="m2"><p>ز بنگاه غریبان روی برتاب</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>رها کن تا در این محنت که هستم</p></div>
<div class="m2"><p>خدای خویشتن را می‌پرستم</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>به دام آورده گیر این مرغ را باز</p></div>
<div class="m2"><p>دگرباره به صحرا کرده پرواز</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>مشو راهی که خر در گل بماند</p></div>
<div class="m2"><p>ز کارت بی‌دلان را دل بماند</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>مزن آتش در این جان ستمکش</p></div>
<div class="m2"><p>رها کن خانه‌ای از بهر آتش</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>در این آتش که عشق افروخت بر من</p></div>
<div class="m2"><p>دریغا عشق خواهد سوخت خرمن</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>غمت بر هر رگم پیچید ماری</p></div>
<div class="m2"><p>شکستم در بن هر موی خاری</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>نه شب خسبم نه روز آسایشم هست</p></div>
<div class="m2"><p>نه از تو ذره‌ای بخشایشم هست</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>صبوری چون کنم عمری چنین تنگ</p></div>
<div class="m2"><p>به منزل چون رسم پایی چنین لنگ</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>ز اشک و آه من در هر شماری</p></div>
<div class="m2"><p>بود دریا نمی دوزخ شراری</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>در این دریا کِم آتش گشت کشتی</p></div>
<div class="m2"><p>مرا هم دوزخی خوان هم بهشتی</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>وگرنه بر در دوزخ نهانی</p></div>
<div class="m2"><p>چرا می‌جویم آب زندگانی</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>مرا چون بد نباشد حال بی تو؟</p></div>
<div class="m2"><p>که بودم با تو پار امسال بی تو</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>ترا خاکی است خاک از در گذشته</p></div>
<div class="m2"><p>مرا آبی است آب از سر گذشته</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>بر آب دیده کشتی چند رانم</p></div>
<div class="m2"><p>وصالت را به یاری چند خوانم</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>همه کارم که بی تو ناتمام است</p></div>
<div class="m2"><p>چنین خام از تمناهای خام است</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>نبینی هر که میرد تا نمیرد</p></div>
<div class="m2"><p>امید از زندگانی برنگیرد</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>خرد ما را به دانش رهنمون است</p></div>
<div class="m2"><p>حساب عشق ازین دفتر برون است</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>بر این ابلق کسی چابک‌سوار است</p></div>
<div class="m2"><p>که در میدان عشق آشفته‌کار است</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>مفرح ساختن فرزانگان راست</p></div>
<div class="m2"><p>چو شد پرداخته دیوانگان راست</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>به عشق‌اندر صبوری خام‌کاری است</p></div>
<div class="m2"><p>بنای عاشقی بر بی‌قراری است</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>صبوری از طریق عشق دور است</p></div>
<div class="m2"><p>نباشد عاشق آنکس کو صبور است</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>بدینسان گرچه شیرین است رنجور</p></div>
<div class="m2"><p>ز خسرو باد دائم رنج و غم دور</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>چو بر شاپور خواند این داستان را</p></div>
<div class="m2"><p>سبک بوسید شاپور آستان را</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>که از تدبیر ما رای تو بیش است</p></div>
<div class="m2"><p>همه گفتار تو بر جای خویش است</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>وزان پس گر دلش اندیشه سفتی</p></div>
<div class="m2"><p>سخن با او نسنجیده نگفتی</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>سخن باید بدانش درج کردن</p></div>
<div class="m2"><p>چو زر سنجیدن آنگه خرج کردن</p></div></div>