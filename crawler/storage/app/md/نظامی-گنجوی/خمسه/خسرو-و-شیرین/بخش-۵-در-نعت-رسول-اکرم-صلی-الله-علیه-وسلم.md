---
title: >-
    بخش ۵ - در نعت رسول اکرم صلی الله علیه وسلم
---
# بخش ۵ - در نعت رسول اکرم صلی الله علیه وسلم

<div class="b" id="bn1"><div class="m1"><p>محمد کآفرینش هست خاکش</p></div>
<div class="m2"><p>هزاران آفرین بر جان پاکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چراغ افروز چشم اهل بینش</p></div>
<div class="m2"><p>طراز کارگاه آفرینش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر و سرهنگ، میدان وفا را</p></div>
<div class="m2"><p>سپه سالار و سر خیل، انبیا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرقع بر کش نر ماده‌ای چند</p></div>
<div class="m2"><p>شفاعت خواه کار افتاده‌ای چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ریاحین بخش باغ صبحگاهی</p></div>
<div class="m2"><p>کلید مخزن گنج الهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یتیمان را نوازش در نسیمش</p></div>
<div class="m2"><p>از آنجا نام شد در یتیمش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به معنی، کیمیای خاک آدم</p></div>
<div class="m2"><p>به صورت توتیای چشم عالم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرای شرع را چون چار حد بست</p></div>
<div class="m2"><p>بنا بر چار دیوار ابد بست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز شرع خود نبوت را نوی داد</p></div>
<div class="m2"><p>خرد را در پناهش پیروی داد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اساس شرع او ختم جهان است</p></div>
<div class="m2"><p>شریعت‌ها بدو منسوخ از آن است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جوانمردی رحیم و تند چون شیر</p></div>
<div class="m2"><p>زبانش گه کلید و گاه شمشیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ایازی خاص و از خاصان گزیده</p></div>
<div class="m2"><p>ز مسعودی به محمودی رسیده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خدایش تیغ نصرت داده در چنگ</p></div>
<div class="m2"><p>کز آهن نقش داند بست بر سنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به معجز بدگمانان را خجل کرد</p></div>
<div class="m2"><p>جهانی سنگدل را تنگ دل کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو گل بر آبروی دوستان شاد</p></div>
<div class="m2"><p>چو سرو از آبخورد عالم آزاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فلک را داده سَروَش سبز پوشی</p></div>
<div class="m2"><p>عمامش باد را عنبر فروشی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زده در موکب سلطان سوارش</p></div>
<div class="m2"><p>به نوبت پنج نوبت چار یارش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سریر عرش را نعلین او تاج</p></div>
<div class="m2"><p>امین وحی و صاحب سِرِّ معراج</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز چاهی برده مهدی را به انجم</p></div>
<div class="m2"><p>ز خاکی کرده دیوی را به مردم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خلیل از خیل تاشان سپاهش</p></div>
<div class="m2"><p>کلیم از چاوشان بارگاهش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به رنج و راحتش در کوه و غاری</p></div>
<div class="m2"><p>حرم ماری و محرم سوسماری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گهی دندان بدست سنگ داده</p></div>
<div class="m2"><p>گهی لب بر سر سنگی نهاده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لب و دندانش از آن در سنگ زد چنگ</p></div>
<div class="m2"><p>که دارد لعل و گوهر جای در سنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سر دندان کنش را زیر چنبر</p></div>
<div class="m2"><p>فلک دندان کنان آورده بر در</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بصر در خواب و دل در استقامت</p></div>
<div class="m2"><p>زبانش امتی گو تا قیامت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من آن تشنه لب غمناک اویم</p></div>
<div class="m2"><p>که او آب من و من خاک اویم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به خدمت کرده‌ام بسیار تقصیر</p></div>
<div class="m2"><p>چه تدبیر ای نبی‌الله چه تدبیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کنم درخواستی زان روضه پاک</p></div>
<div class="m2"><p>که یک خواهش کنی در کار این خاک</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برآری دست از آن بُردِ یمانی</p></div>
<div class="m2"><p>نمائی دست برد آنگه که دانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کالهی بر نظامی کار بگشای</p></div>
<div class="m2"><p>ز نفس کافرش زنار بگشای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دلش در مخزن آسایش آور</p></div>
<div class="m2"><p>بر آن بخشودنی بخشایش آور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگر چه جرم او کوه گران است</p></div>
<div class="m2"><p>ترا دریای رحمت بیکران است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بیامرزش روان آمرزی آخر</p></div>
<div class="m2"><p>خدای رایگان آمرزی آخر</p></div></div>