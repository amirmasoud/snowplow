---
title: >-
    بخش ۹۱ - اندرز شیرین خسرو را در داد و دانش
---
# بخش ۹۱ - اندرز شیرین خسرو را در داد و دانش

<div class="b" id="bn1"><div class="m1"><p>به نزهت بود روزی با دل‌افروز</p></div>
<div class="m2"><p>سخن در داد و دانش می‌شد آن روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمین بوسید شیرین کای خداوند</p></div>
<div class="m2"><p>ز رامش سوی دانش کوش یک چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی کوشیده‌ای در کامرانی</p></div>
<div class="m2"><p>بسی دیگر به کام دل برانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان را کرده‌ای از نعمت آباد</p></div>
<div class="m2"><p>خرابش چون توان کردن به بیداد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آن گاوی که ازوی شیر خیزد</p></div>
<div class="m2"><p>لگد در شیر گیرد تا بریزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حذر کن زانکه ناگه در کمینی</p></div>
<div class="m2"><p>دعای بد کند خلوت‌نشینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنی پیر از نفسهای جوانه</p></div>
<div class="m2"><p>زند تیری سحرگه بر نشانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندارد سودت آنگه بانگ و فریاد</p></div>
<div class="m2"><p>که نفرین داده باشد ملک بر باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسا آیینه کاندر دست شاهان</p></div>
<div class="m2"><p>سیه گشت از نفیر داد خواهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو دولت روی برگرداند از راه</p></div>
<div class="m2"><p>همه کاری نه بر موقع کند شاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو برگ باغ گیرد ناتوانی</p></div>
<div class="m2"><p>خبر پیشین برد باد خزانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو دور از حاضران میرد چراغی</p></div>
<div class="m2"><p>کشندش پیش از آن در دیده داغی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو سیلی ریختن خواهد به انبوه</p></div>
<div class="m2"><p>بغرد کوهه ابر از سر کوه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تگرگی کو زند گشنیز بر خاک</p></div>
<div class="m2"><p>رسد خود بوی گشنیزش بر افلاک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>درختی کاول از پیوند کژ خاست</p></div>
<div class="m2"><p>نشاید جز به آتش کردنش راست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جهانسوزی بد است و جور سازی</p></div>
<div class="m2"><p>ترا به گر رعیت را نوازی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از آن ترسم که گرد این مثل راست</p></div>
<div class="m2"><p>که آن شه گفت کو را کس نمی‌خواست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کهن دولت چو باشد دیر پیوند</p></div>
<div class="m2"><p>رعیت را نباشد هیچ در بند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز مثل خود جهان را طاق بیند</p></div>
<div class="m2"><p>جهان خود را به استحقاق بیند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز مغروری که در سر ناز گیرد</p></div>
<div class="m2"><p>مراعات از رعیت باز گیرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نو اقبالی بر آرد دست ناگاه</p></div>
<div class="m2"><p>کند دست دراز از خلق کوتاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خلایق را چو نیکو خواه گردد</p></div>
<div class="m2"><p>باجماع خلایق شاه گردد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خردمندی و شاهی هر دو داری</p></div>
<div class="m2"><p>سپیدی و سیاهی هر دو داری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نجات آخرت را چاره‌گر باش</p></div>
<div class="m2"><p>در این منزل ز رفتن با خبر باش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کسی کو سیم و زر ترکیب سازد</p></div>
<div class="m2"><p>قیامت را کجا ترتیب سازد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ببین دور از تو شاهانی که مردند</p></div>
<div class="m2"><p>ز مال و ملک و شاهی هیچ بردند؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بمانی، مال بد خواه تو باشد</p></div>
<div class="m2"><p>ببخشی، شحنه راه تو باشد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فرو خوان قصه دارا و جمشید</p></div>
<div class="m2"><p>که با هر یک چه بازی کرد خورشید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در این نه پرده آهنگ آنچنان ساز</p></div>
<div class="m2"><p>که دانی پردهٔ پوشیده را راز</p></div></div>