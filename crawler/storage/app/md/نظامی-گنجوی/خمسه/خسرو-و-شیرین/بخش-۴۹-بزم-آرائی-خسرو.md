---
title: >-
    بخش ۴۹ - بزم‌آرائی خسرو
---
# بخش ۴۹ - بزم‌آرائی خسرو

<div class="b" id="bn1"><div class="m1"><p>چهارم روز مجلس تازه کردند</p></div>
<div class="m2"><p>غناها را بلند آوازه کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بخشیدن در آمد دست دریا</p></div>
<div class="m2"><p>زمین گشت از جواهر چون ثریا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک چون شد ز نوش ساقیان مست</p></div>
<div class="m2"><p>غم دیدار شیرین بردش از دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طلب فرمود کردن باربد را</p></div>
<div class="m2"><p>وزو درمان طلب شد درد خود را</p></div></div>