---
title: >-
    بخش ۹۵ - اجرام کواکب
---
# بخش ۹۵ - اجرام کواکب

<div class="b" id="bn1"><div class="m1"><p>دگر ره گفت کاجرام کواکب</p></div>
<div class="m2"><p>ندانم بر چه مرکوبند راکب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شنیدستم که هر کوکب جهانیست</p></div>
<div class="m2"><p>جداگانه زمین و آسمانیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوابش داد کاین ما هم شنیدیم</p></div>
<div class="m2"><p>درستی را بدان قایم ندیدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو وا جستیم از آن صورت که حالست</p></div>
<div class="m2"><p>رصد بنمود کاین معنی محالست</p></div></div>