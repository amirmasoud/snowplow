---
title: >-
    بخش ۱۳ - عذر انگیزی در نظم کتاب
---
# بخش ۱۳ - عذر انگیزی در نظم کتاب

<div class="b" id="bn1"><div class="m1"><p>در آن مدت که من در بسته بودم</p></div>
<div class="m2"><p>سخن با آسمان پیوسته بودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی برج کواکب می‌بریدم</p></div>
<div class="m2"><p>گهی ستر ملایک می‌دریدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یگانه دوستی بودم خدائی</p></div>
<div class="m2"><p>به صد دل کرده با جان آشنائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تعصب را کمر در بسته چون شیر</p></div>
<div class="m2"><p>شده بر من سپر بر خصم شمشیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دنیا بدانش بند کرده</p></div>
<div class="m2"><p>ز دنیا دل بدین خرسند کرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبی در هم شده چون حلقه زر</p></div>
<div class="m2"><p>به نقره نقره زد بر حلقه در</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درآمد سر گرفته سر گرفته</p></div>
<div class="m2"><p>عتابی سخت با من در گرفته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که احسنت ای جهاندار معانی</p></div>
<div class="m2"><p>که در ملک سخن صاحبقرانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس از پنجاه چله در چهل سال</p></div>
<div class="m2"><p>مزن پنجه در این حرف ورق مال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین روزه چو هستی پای بر جای</p></div>
<div class="m2"><p>به مردار استخوانی روزه مگشای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نکرده آرزو هرگز ترا بند</p></div>
<div class="m2"><p>که دنیا را نبودی آرزومند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو داری در سنان نوک خامه</p></div>
<div class="m2"><p>کلید قفل چندین گنج‌نامه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مسی را زر بر اندودن غرض چیست</p></div>
<div class="m2"><p>زر اندر سیم‌تر زین می‌توان زیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چرا چون گنج قارون خاک بهری</p></div>
<div class="m2"><p>نه استاد سخن گویان دهری؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در توحید زن کاوازه داری</p></div>
<div class="m2"><p>چرا رسم مغان را تازه داری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سخندانان دلت را مرده دانند</p></div>
<div class="m2"><p>اگر چه زند خوانان زنده خوانند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز شورش کردن آن تلخ گفتار</p></div>
<div class="m2"><p>ترشروئی نکردم هیچ در کار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز شیرین کاری شیرین دلبند</p></div>
<div class="m2"><p>فرو خواندم به گوشش نکته‌ای چند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وزان دیبا که می‌بستم طرازش</p></div>
<div class="m2"><p>نمودم نقش‌های دل نوازش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو صاحب سنگ دید آن نقش ارژنگ</p></div>
<div class="m2"><p>فرو ماند از سخن چون نقش بر سنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدو گفتم ز خاموشی چه جوئی</p></div>
<div class="m2"><p>زبانت کو که احسنتی بگوئی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به صد تسلیم گفت ای من غلامت</p></div>
<div class="m2"><p>زبانم وقف بر تسبیح نامت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو بشنیدم ز شیرین داستان را</p></div>
<div class="m2"><p>ز شیرینی فرو بردم زبان را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چنین سحری تو دانی یاد کردن</p></div>
<div class="m2"><p>بتی را کعبه‌ای بنیاد کردن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مگر شیرین بدان کردی دهانم</p></div>
<div class="m2"><p>که در حلقم شکر گردد زبانم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر خوردم زبان را من شکروار</p></div>
<div class="m2"><p>زبان چون توئی بادا شکربار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به پایان بر چو این ره بر گشادی</p></div>
<div class="m2"><p>تمامش کن چو بنیادش نهادی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در این گفتن ز دولت یاریت باد</p></div>
<div class="m2"><p>برومندی و برخورداریت باد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چرا گشتی درین بی‌غوله پا بست</p></div>
<div class="m2"><p>چنین نقد عراقی بر کف دست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رکاب از شهربند گنجه بگشای</p></div>
<div class="m2"><p>عنان شیر داری پنجه بگشای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فرس بیرون فکن میدان فراخست</p></div>
<div class="m2"><p>تو سرسبزی و دولت سبز شاخست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زمانه نغز گفتاری ندارد</p></div>
<div class="m2"><p>و گر دارد چو تو باری ندارد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همائی کن برافکن سایه برکار</p></div>
<div class="m2"><p>ولایت را به جغدی چند مسپار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چراغند این دو سه پروانه خویش</p></div>
<div class="m2"><p>پدیدار آمده در خانه خویش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دو منزل گر شوند از شهر خود دور</p></div>
<div class="m2"><p>نبینی هیچ کس را رونق و نور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو آن خورشید نورانی قیاسی</p></div>
<div class="m2"><p>که مشرق تا به مغرب روشناسی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو تو حالی نهادی پای در پیش</p></div>
<div class="m2"><p>به کنجی هر کسی گیرد سر خویش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هم آفاق هنر یابد حصاری</p></div>
<div class="m2"><p>هم اقلیم سخن بیند سواری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به تندی گفتم ای بخت بلندم</p></div>
<div class="m2"><p>نه تو قصابی و من گوپسندم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مدم دم تا چراغ من نمیرد</p></div>
<div class="m2"><p>که در موسی دم عیسی نگیرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به حشوی چندم آتش برمیفروز</p></div>
<div class="m2"><p>که من خود چون چراغم خویشتن سوز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>من آن شیشه‌ام که گر بر من زنی سنگ</p></div>
<div class="m2"><p>ز نام و کنیتم گیرد جهان ننگ</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مسی بینی زری به روی کشیده</p></div>
<div class="m2"><p>به مرداری کلابی بر دمیده</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نبینی جز هوای خویش قوتم</p></div>
<div class="m2"><p>بجز بادی نیابی در بروتم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>فلک در طالعم شیری نموده‌است</p></div>
<div class="m2"><p>ولیکن شیر پشمینم چه سوداست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نه آن شیرم که با دشمن برآیم</p></div>
<div class="m2"><p>مرا آن بس که من با من برآیم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نشاطی پیش ازین بود آن قدم رفت</p></div>
<div class="m2"><p>غروری کز جوانی بود هم رفت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>حدیث کودکی و خودپرستی</p></div>
<div class="m2"><p>رها کن کان خیالی بود و مستی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو عمر از سی گذشت یا خود از بیست</p></div>
<div class="m2"><p>نمی‌شاید دگر چون غافلان زیست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نشاط عمر باشد تا چهل سال</p></div>
<div class="m2"><p>چهل ساله فرو ریزد پر و بال</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>پس از پنجه نباشد تندرستی</p></div>
<div class="m2"><p>بصر کندی پذیرد پای سستی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو شصت آمد نشست آمد پدیدار</p></div>
<div class="m2"><p>چو هفتاد آمد افتاد آلت از کار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به هشتاد و نود چون در رسیدی</p></div>
<div class="m2"><p>بسا سخنی که از گیتی کشیدی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>وز آنجا گر به صد منزل رسانی</p></div>
<div class="m2"><p>بود مرگی به صورت زندگانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>اگر صد سال مانی ور یکی روز</p></div>
<div class="m2"><p>بباید رفت ازین کاخ دل افروز</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>پس آن بهتر که خود را شاد داری</p></div>
<div class="m2"><p>در آن شادی خدا را یاد داری</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به وقت خوشدلی چون شمع پرتاب</p></div>
<div class="m2"><p>دهن پر خنده داری دیده پر آب</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو صبح آن روشنان از گریه رستند</p></div>
<div class="m2"><p>که برق خنده را بر لب ببستند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چوبی گریه نشاید بود خندان</p></div>
<div class="m2"><p>وزین خنده نشاید بست دندان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بیاموزم تو را گر کاربندی</p></div>
<div class="m2"><p>که بی گریه زمانی خوش بخندی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو خندان گردی از فرخنده فالی</p></div>
<div class="m2"><p>بخندان تنگدستی را به مالی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نه بینی آفتاب آسمان را</p></div>
<div class="m2"><p>کز آن خندد که خنداند جهان را</p></div></div>