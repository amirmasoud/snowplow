---
title: >-
    بخش ۸۸ - بیرون آمدن شیرین از خرگاه
---
# بخش ۸۸ - بیرون آمدن شیرین از خرگاه

<div class="b" id="bn1"><div class="m1"><p>حکایت بر گرفته شاه و شاپور</p></div>
<div class="m2"><p>جهان دیدند یکسر نور در نور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پری پیکر برون آمد ز خرگاه</p></div>
<div class="m2"><p>چنان کز زیر ابر آید برون ماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو عیاران سرمست از سر مهر</p></div>
<div class="m2"><p>به پای شه در افتاد آن پری چهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو شه معشوق را مولای خود دید</p></div>
<div class="m2"><p>سر مه را به زیر پای خود دید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شادی ساختنش بر فرق خود جای</p></div>
<div class="m2"><p>که شه را تاج بر سر به که در پای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آن خدمت که یارش ساز می‌کرد</p></div>
<div class="m2"><p>مکافاتش یکی ده باز می‌کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو کار از پای بوسی برتر آمد</p></div>
<div class="m2"><p>تقاضای دهن بوسی بر آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن آتش که بر خاطر گذر کرد</p></div>
<div class="m2"><p>ترش روئی به شیرین در اثر کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ملک حیران شده کان روی گلرنگ</p></div>
<div class="m2"><p>چرا شد شاد و چون شد باز دلتنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نهان در گوش خسرو گفت شاپور</p></div>
<div class="m2"><p>که گر مه شد گرفته هست معذور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برای آنکه خود را تا به امروز</p></div>
<div class="m2"><p>بنام نیک پرورد آن دل‌افروز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنون ترسد که مطلق دستی شاه</p></div>
<div class="m2"><p>نهد خال خجالت بر رخ ماه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو شه دانست کان تخم برومند</p></div>
<div class="m2"><p>بدو سر در نیارد جز به پیوند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بسی سوگند خورد و عهدها بست</p></div>
<div class="m2"><p>که بی کاوین نیارد سوی او دست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بزرگان جهان را جمع سازد</p></div>
<div class="m2"><p>به کاوین کردنش گردن فرازد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ولی باید که می در جام ریزد</p></div>
<div class="m2"><p>که از دست این زمان آن برنخیزد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یک امشب شادمان با هم نشینیم</p></div>
<div class="m2"><p>به روی یکدیگر عالم به بینیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو عهد شاه را بشنید شیرین</p></div>
<div class="m2"><p>به خنده برگشاد از ماه پروین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لبش با در به غواصی در آمد</p></div>
<div class="m2"><p>سر زلفش به رقاصی بر آمد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خروش زیور زر تاب داده</p></div>
<div class="m2"><p>دماغ مطربان را خواب داده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لبش از می قدح بر دست کرده</p></div>
<div class="m2"><p>به جرعه ساقیان را مست کرده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز شادی چون تواند ماند باقی</p></div>
<div class="m2"><p>که مه مطرب بود خورشید ساقی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دل از مستی چنان مخمور مانده</p></div>
<div class="m2"><p>کز اسباب غرضها دور مانده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دماغ از چاشنیهای دگر نوش</p></div>
<div class="m2"><p>ز لذت کرده شهوت را فراموش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بخور عطر و آنگه روی زیبا</p></div>
<div class="m2"><p>دل از شادی کجا باشد شکیبا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فرو مانده ز بازیهای دلکش</p></div>
<div class="m2"><p>در آب و آتش اندر آب و آتش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کششهائی بدان رغبت که باید</p></div>
<div class="m2"><p>چو مغناطیس کاهن را رباید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ولیکن بود صحبت زینهاری</p></div>
<div class="m2"><p>نکردند از وفا زنهار خواری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو آمد در کف خسرو دل دوست</p></div>
<div class="m2"><p>برون آمد ز شادی چون گل از پوست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دل خود را چو شمع از دیده پالود</p></div>
<div class="m2"><p>پرند ماه را پروین بر آمود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به مژگان دیده را در ماه می‌دوخت</p></div>
<div class="m2"><p>مگر بر مجمر مه عود می‌سوخت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گهی میسود نرگس بر پرندش</p></div>
<div class="m2"><p>گهی می‌بست سنبل بر کمندش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گهی بر نار سیمینش زدی دست</p></div>
<div class="m2"><p>گهی لرزید چون سیماب پیوست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گهی مرغول جعدش باز کردی</p></div>
<div class="m2"><p>ز شب بر ماه مشک‌انداز کردی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که از فرق سرش معجر گشادی</p></div>
<div class="m2"><p>غلامانه کلاهش بر نهادی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که از گیسوش بستی بر میان بند</p></div>
<div class="m2"><p>که از لعلش نهادی در دهان قند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گهی سودی عقیقش را به انگشت</p></div>
<div class="m2"><p>گه آوردی زنخ چون سیب در مشت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گهی دستینه از دستش ربودی</p></div>
<div class="m2"><p>به بازو بندیش بازو نمودی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گهی خلخالهاش از پای کندی</p></div>
<div class="m2"><p>بجای طوق در گردن فکندی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گه آوردی فروزان شمع در پیش</p></div>
<div class="m2"><p>درو دیدی و در حال دل خویش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گهی گفتی تنم را جان توئی تو</p></div>
<div class="m2"><p>گهی گفت این منم من آن توئی تو؟</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دلش در بند آن پاکیزه دلبند</p></div>
<div class="m2"><p>به شاهد بازی آن شب گشت خرسند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نشاط هر دو در شهوت پرستی</p></div>
<div class="m2"><p>به شیر مست ماند از شیر مستی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>صدف می‌داشت درج خویش را پاس</p></div>
<div class="m2"><p>که تا بر در نیفتد نوک الماس</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز بانک بوسهای خوشتر از نوش</p></div>
<div class="m2"><p>زمانه ارغنون کرده فراموش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دهل‌زن چون دهل را ساز می‌کرد</p></div>
<div class="m2"><p>هنوز این لابه و آن ناز می‌کرد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بدینسان هفته‌ای دمساز بودند</p></div>
<div class="m2"><p>گهی با عذر و گه با ناز بودند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به روز آهنگ عشرت داشتندی</p></div>
<div class="m2"><p>دمی بیخوشدلی نگذاشتندی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به شب نرد قناعت باختندی</p></div>
<div class="m2"><p>به بوسه کعبتین انداختندی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شب هفتم که کار از دست می‌شد</p></div>
<div class="m2"><p>غرض دیوانه شهوت مست می‌شد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ملک فرمود تا هم در شب آن ماه</p></div>
<div class="m2"><p>به برج خویشتن روشن کند راه</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سپاهی چون کواکب در رکابش</p></div>
<div class="m2"><p>که از پری خدا داند حسابش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نشیند تا به صد تمکینش آرند</p></div>
<div class="m2"><p>چو مه در محمل زرینش آرند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چنان کاید به برج خویشتن ماه</p></div>
<div class="m2"><p>به قصر خویشتن آمد ز خرگاه</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو رفت آن نقد سیمین باز در سنگ</p></div>
<div class="m2"><p>ز نقد سیم شد دست جهان تنگ</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>فلک بر کرد زرین بادبانی</p></div>
<div class="m2"><p>نماند از سیم کشتیها نشانی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>شهنشه کوچ کرد از منزل خویش</p></div>
<div class="m2"><p>گرفته راه دارالملک در پیش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به شهر آمد طرب را کار فرمود</p></div>
<div class="m2"><p>برآسود و ز می خوردن نیاسود</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به فیض ابروی سیما درخشی</p></div>
<div class="m2"><p>جهان را تازه کرد از تاج بخشی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>درآمد مرد را بخشنده دارد</p></div>
<div class="m2"><p>زمین تا در نیارد بر نیارد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نه ریزد ابر بی توفیر دریا</p></div>
<div class="m2"><p>نه بی‌باران شود دریا مهیا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نه بر مرد تهی رو هست باجی</p></div>
<div class="m2"><p>نه از ویرانه کس خواهد خراجی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>شبی فرمود تا اختر شناسان</p></div>
<div class="m2"><p>کنند اندیشه دشوار و آسان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بجویند از شب تاریک تارک</p></div>
<div class="m2"><p>به روشن خاطری روزی مبارک</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>که شاید مهد آن ماه دلفروز</p></div>
<div class="m2"><p>به برج آفتاب آوردن آن روز</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>رصدبندان بر او مشکل گشادند</p></div>
<div class="m2"><p>طرب را طالعی میمون نهادند</p></div></div>