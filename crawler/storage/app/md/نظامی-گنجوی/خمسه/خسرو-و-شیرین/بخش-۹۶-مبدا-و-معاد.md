---
title: >-
    بخش ۹۶ - مبداء و معاد
---
# بخش ۹۶ - مبداء و معاد

<div class="b" id="bn1"><div class="m1"><p>دگر ره گفت ما اینجا چرائیم</p></div>
<div class="m2"><p>کجا خواهیم رفتن وز کجائیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوابش داد و گفت از پرده این راز</p></div>
<div class="m2"><p>نگردد کشف هم با پرده میساز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که ره دورست ازین منزل که مائیم</p></div>
<div class="m2"><p>ندیده راه منزل چون نمائیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو زین ره بستگان یابی رهائی</p></div>
<div class="m2"><p>بدانی خود که چونی وز کجائی</p></div></div>