---
title: >-
    بخش ۵ - دعای پادشاه سعید علاء الدین کرپ ارسلان
---
# بخش ۵ - دعای پادشاه سعید علاء الدین کرپ ارسلان

<div class="b" id="bn1"><div class="m1"><p>ای دل از این خیال سازی چند</p></div>
<div class="m2"><p>به خیالی خیال بازی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سر این خیال درگذرم</p></div>
<div class="m2"><p>دور به ز این خیالها نظرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچه مقصود شد در این پرگار</p></div>
<div class="m2"><p>چار فصل است به ز فصل بهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اولین فصل آفرین خدای</p></div>
<div class="m2"><p>کافرینش به فضل اوست به پای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>واندگر فصل خطبه نبوی</p></div>
<div class="m2"><p>کین کهن سکه زو گرفت نوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فصل دیگر دعای شاه جهان</p></div>
<div class="m2"><p>کان دعا در برآورد ز دهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فصل آخر نصیحت آموزی</p></div>
<div class="m2"><p>پادشه را به فتح و فیروزی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پادشاهی که ملک هفت اقلیم</p></div>
<div class="m2"><p>دخل دولت بدو کند تسلیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حجت مملکت به قول و به قهر</p></div>
<div class="m2"><p>آیتی از خدا یگانه دهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خسرو تاج بخش تخت نشان</p></div>
<div class="m2"><p>بر سر تاج و تخت گنج فشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عمده مملکت علاء الدین</p></div>
<div class="m2"><p>حافظ و ناصر زمان و زمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نام او رتبت علا دارد</p></div>
<div class="m2"><p>گر گذشت از فلک روا دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فلک بی علا چه باشد پست</p></div>
<div class="m2"><p>در علا بی فلک بلندی هست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاه کرپ ارسلان کشور گیر</p></div>
<div class="m2"><p>به ز آلپ ارسلان به تاج و سریر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مهدیی کافتاب این مهد است</p></div>
<div class="m2"><p>دولتش ختم آخرین عهد است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رستمی کز فلک سواری رخش</p></div>
<div class="m2"><p>هم بزرگ است و هم بزرگی بخش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همسر آسمان و هم کف ابر</p></div>
<div class="m2"><p>هم به تن شیر و هم به نام هژبر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قفل هستی چو در کلید آمد</p></div>
<div class="m2"><p>عالم از جوهری پدید آمد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اوست آن عالمی که از کف خویش</p></div>
<div class="m2"><p>هردم آرد هزار جوهر بیش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صحف گردون ز شرح او ورقی</p></div>
<div class="m2"><p>عرق دریا ز فیض او عرقی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بحر و بر هردو زیر فرمانش</p></div>
<div class="m2"><p>بری و بحری آفرین خوانش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سربلندی چنان بلند سریر</p></div>
<div class="m2"><p>کز بلندیش خرد گشت ضمیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در بزرگی برابر ملک است</p></div>
<div class="m2"><p>وز بلندی برادر فلک است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر تن دشمنان برقع دوز</p></div>
<div class="m2"><p>برق شمشیر اوست برقع سوز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نسل اقسنقری مؤید ازو</p></div>
<div class="m2"><p>اب وجد با کمال ابجد ازو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فتح بر خاک پای او زده فرق</p></div>
<div class="m2"><p>فتنه در آب تیغ او شده غرق</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آب او آتش از اثیر انگیز</p></div>
<div class="m2"><p>خاک او باد را عبیر آمیز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در نبردش که شیر خارد دم</p></div>
<div class="m2"><p>اسب دشمن به سر شود نه به سم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در صبوحش که خون رز ریزد</p></div>
<div class="m2"><p>زاب یخ بسته آتش انگیزد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حربه را چون به حرب تیز کند</p></div>
<div class="m2"><p>روز را روز رستخیز کند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون در کان جود بگشاید</p></div>
<div class="m2"><p>گنج بخشد گناه بخشاید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شه چو دریاست بی‌دروغ و دریغ</p></div>
<div class="m2"><p>جزر و مدش به تازیانه و تیغ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هرچه آرد به زخم تیغ فراز</p></div>
<div class="m2"><p>به سر تازیانه بخشد باز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مشتری‌وار بر سپهر بلند</p></div>
<div class="m2"><p>گور کیوان کند به سم سمند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر ندیدی بر اژدها شیری</p></div>
<div class="m2"><p>وافتابی کشیده شمشیری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شاه را بین که در مصاف و شکار</p></div>
<div class="m2"><p>اژدها صورتست و شیر سوار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ناچخش زیر اژدهای علم</p></div>
<div class="m2"><p>اژدها را چو مار کرده قلم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تنگی مطرحش به تیر دو شاخ</p></div>
<div class="m2"><p>کرده بر شیر شرزه گور فراخ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نوک تیرش به هر کجا که بتافت</p></div>
<div class="m2"><p>گه جگر دوخت گاه موی شکافت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بازی خرس برده از شمشیر</p></div>
<div class="m2"><p>خرس بازی در آوریده به شیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شیرگیری ولیک نز مستی</p></div>
<div class="m2"><p>شیرگیری به اژدها دستی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گرگ درنده را به کوه سهند</p></div>
<div class="m2"><p>دست و پائی به یک دو شاخ افکند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شه چو از گرگ دست و پا برده</p></div>
<div class="m2"><p>شیر با او به دست و پا مرده</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تیرش از دست گرگ و پای پلنگ</p></div>
<div class="m2"><p>برسم گور کرده صحرا تنگ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>صیدگاهش ز خون دریا جوش</p></div>
<div class="m2"><p>گاه گرگینه گه پلنگی پوش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بر گرازی که تیغ راند تیز</p></div>
<div class="m2"><p>گیرد از زخم او گراز گریز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون به چرم کمان درآرد زور</p></div>
<div class="m2"><p>چرم را بر گوزن سازد گور</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کند ارپای در نهد به مصاف</p></div>
<div class="m2"><p>سنگ را چون عقیق زهره شکاف</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آن نماید به تیغ زهراندود</p></div>
<div class="m2"><p>کاسمان از زمین برآرد دود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اوست در بزم ورزم یافته نام</p></div>
<div class="m2"><p>جان ده و جان ستان به تیغ و به جام</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خاک تیره ز روشنائی او</p></div>
<div class="m2"><p>چشم روشن به آشنائی او</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ناف خلقش چو کلک رسامان</p></div>
<div class="m2"><p>مشک در جیب و لعل در دامان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گشته از مشک و لعل او همه جای</p></div>
<div class="m2"><p>مملکت عقد بند و غالیه‌سای</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از قبای چنو کله‌داری</p></div>
<div class="m2"><p>ز آسمان تا زمین کله‌واری</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>وز کمان چنو جهان‌گیری</p></div>
<div class="m2"><p>چرخ نه قبضه کمترین تیری</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زان بزرگی که در سگالش اوست</p></div>
<div class="m2"><p>چار گوهر چهار بالش اوست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دشمنش چون درخت بیخ زده</p></div>
<div class="m2"><p>بر در او به چار میخ زده</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز آفتاب جلال اوست چو ماه</p></div>
<div class="m2"><p>روی ما سرخ و روی خصم سیاه</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چه عجب کافتاب زرین نعل</p></div>
<div class="m2"><p>کوه را سنگ داد و کانرا لعل</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گوهری کان حرم دریده اوست</p></div>
<div class="m2"><p>کان گوهر درم خریده اوست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>داد جرعش به کوه و دریا قوت</p></div>
<div class="m2"><p>نام این در نشان آن یاقوت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>پاس دار دو حکم در دو سرای</p></div>
<div class="m2"><p>ضابط حکم خلق و حکم خدای</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>می‌پذیرد ز فیض یزدان ساز</p></div>
<div class="m2"><p>می‌رساند به بندگانش باز</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چون جهان زو گرفت پیروزی</p></div>
<div class="m2"><p>فرخی بادش از جهان روزی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همه روزش خجسته باد به فال</p></div>
<div class="m2"><p>پادشاهیش را مباد زوال</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نظم اولاد او به سعد نجوم</p></div>
<div class="m2"><p>باد در بدر تا ابد منظوم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>از فروغ دو صبح زیبا چهر</p></div>
<div class="m2"><p>باد روشن چو آفتاب سپهر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دو ملک زاده بلند سریر</p></div>
<div class="m2"><p>این جهان‌جوی و آن ولایت‌گیر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>این فریدون صفت به دانش ورای</p></div>
<div class="m2"><p>وان به کیخسروی رکیب گشای</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>نقش این بر طراز افسر و گاه</p></div>
<div class="m2"><p>نصرت‌الدین ملک محمد شاه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نام آن بر فلک ز راه رصد</p></div>
<div class="m2"><p>گشته من بعدی اسمه احمد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>دایم این را ز نصرتست کلید</p></div>
<div class="m2"><p>وان ز فتح فلک شدست پدید</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نصرت این را به تربیت کاری</p></div>
<div class="m2"><p>فلک آنرا به تقویت داری</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>این ز نصرت زده سه پایه بخت</p></div>
<div class="m2"><p>فلک آنرا چهار پایه تخت</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چشم شه زیر چرخ مینائی</p></div>
<div class="m2"><p>باد روشن بدین دو بینائی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>دور ملکش بدین دو قطب جلال</p></div>
<div class="m2"><p>منتظم باد بر جنوب و شمال</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>دولتش صید و صید فربه باد</p></div>
<div class="m2"><p>روزش از روز و شب به باد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>باد محجوبه نقاب شبش</p></div>
<div class="m2"><p>نور صبح محمدی نسبش</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>این چو آبادی چرخ باد بجود</p></div>
<div class="m2"><p>وان شده ختم امهات وجود</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>نام این خضر جاودانی باد</p></div>
<div class="m2"><p>حکم آن آب زندگانی باد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>در حفاظ خط سلیمانی</p></div>
<div class="m2"><p>عرش بلقیس باد نورانی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>سایه شه که هست چشمه نور</p></div>
<div class="m2"><p>زان گل و گلستان مبادا دور</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ازلی شد جهان پناهی او</p></div>
<div class="m2"><p>ابدی باد پادشاهی او</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ای کمر بسته کلاه تو بخت</p></div>
<div class="m2"><p>زنده‌دار جهان به تاج و به تخت</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>شب به پاس تو هندویست سیاه</p></div>
<div class="m2"><p>بسته بر گرد خود جلاجل ماه</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>صبح مفرد رو حمایل کش</p></div>
<div class="m2"><p>در رکابت نفس برآرد خوش</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>شام دیلم گله که چاکر تست</p></div>
<div class="m2"><p>مشکبو از کیائی در تست</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>روز رومی چو شب شود زنگی</p></div>
<div class="m2"><p>گر برونش کنی ز سرهنگی</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>در همه سفره کاسمان دارد</p></div>
<div class="m2"><p>اجری مملکت دو نان دارد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>کمتر اجری خور ترا به قیاس</p></div>
<div class="m2"><p>قوت هفت اختر است جرعه کاس</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>خاتم نصرت الهی را</p></div>
<div class="m2"><p>ختم بر تست پادشاهی را</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>آسمان کافتاب ازو اثریست</p></div>
<div class="m2"><p>بر میان تو کمترین کمریست</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>مه که از چرخ تخت زر کرده است</p></div>
<div class="m2"><p>با سریر تو سر به سر کرده است</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>آب باران که اصل پاکی شد</p></div>
<div class="m2"><p>با تو چون چشم شور خاکی شد</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>لعل با تیغ تو خزف رنگی</p></div>
<div class="m2"><p>کوه با حلم تو سبک سنگی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>پادشاهان که در جهان هستند</p></div>
<div class="m2"><p>هر یک ابری به دست بر بستند</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>جز یک ابر تو کابر نیسانیست</p></div>
<div class="m2"><p>آن دیگر ابرها زمستانیست</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>خوان نهند آنگهی که خون بخورند</p></div>
<div class="m2"><p>نان دهند آنگهی که جان ببرند</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>تو بر آن کس که سایه‌اندازی</p></div>
<div class="m2"><p>دیر خوانی و زود بنوازی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>قدر اهل هنر کسی داند</p></div>
<div class="m2"><p>که هنر نامه‌ها بسی خواند</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>آنکه عیب از هنر نداند باز</p></div>
<div class="m2"><p>زو هنرمند کی پذیرد ساز</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>ملک را ز آفرینشت شرفست</p></div>
<div class="m2"><p>وآفرین‌نامه‌ای به هر طرفست</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>در یزک داری ولایت جود</p></div>
<div class="m2"><p>دولت تست پاسدار وجود</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>رونقی کز تو دید دولت و دین</p></div>
<div class="m2"><p>باغ نادیده ز ابر فروردین</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>گر کیان را به طالع فرخ</p></div>
<div class="m2"><p>هفت خوان بود با دوازده رخ</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>آسمان با بروج او به درست</p></div>
<div class="m2"><p>هفت خوان و دوازده رخ تست</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>همه عالم تنست و ایران دل</p></div>
<div class="m2"><p>نیست گوینده زین قیاس خجل</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>چونکه ایران دل زمین باشد</p></div>
<div class="m2"><p>دل ز تن به بود یقین باشد</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>زان ولایت که مهتران دارند</p></div>
<div class="m2"><p>بهترین جای بهتران دارند</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>دل توئی وین مثل حکایت تست</p></div>
<div class="m2"><p>که دل مملکت ولایت تست</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>ای به خضر و سکندری مشهور</p></div>
<div class="m2"><p>مملکت را ز علم و عدل تو نور</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>ز آهنی گر سکندر آینه ساخت</p></div>
<div class="m2"><p>خضر اگر سوی آب حیوان تاخت</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>گوهر آینه است سینه تو</p></div>
<div class="m2"><p>آب حیوان در آبگینه تو</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>هر ولایت که چون تو شه دارد</p></div>
<div class="m2"><p>ایزد از هر بدش نگه دارد</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>زان سعادت که در سرت دانند</p></div>
<div class="m2"><p>مقبل هفت کشورت خوانند</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>پنجمین کشور از تو آبادان</p></div>
<div class="m2"><p>وز تو شش کشور دیگر شادان</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>همه مرزی ز مهربانی تو</p></div>
<div class="m2"><p>به تمنای مرزبانی تو</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چار شه داشتند چار طراز</p></div>
<div class="m2"><p>پنجمین شان توئی به عمر دراز</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>داشت اسکندر ارسطاطالیس</p></div>
<div class="m2"><p>کز وی آموخت علمهای نفیس</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>بزم نوشیروان سپهری بود</p></div>
<div class="m2"><p>کز جهانش بزرگمهری بود</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>بود پرویز را چه باربدی</p></div>
<div class="m2"><p>که نوا صد نه صدهزار زدی</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>وان ملک را که بد ملکشه نام</p></div>
<div class="m2"><p>بود دین‌پروری چو خواجه نظام</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>تو کز ایشان به افسری داری</p></div>
<div class="m2"><p>چون نظامی سخنوری داری</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>ای نظامی بلند نام از تو</p></div>
<div class="m2"><p>یافته کار او نظام از تو</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>خسروان دیگر زکان گزاف</p></div>
<div class="m2"><p>می‌زنند از خزینه بخشی لاف</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>دانه در خاک شور می‌ریزند</p></div>
<div class="m2"><p>سرمه در چشم کور می‌بیزند</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>در گل شوره دانه افشانی</p></div>
<div class="m2"><p>بر نیارد مگر پشیمانی</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>در زمینی درخت باید کشت</p></div>
<div class="m2"><p>کاورد میوه‌ای چو باغ بهشت</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>باده چون خاک را دهد ساقی</p></div>
<div class="m2"><p>نام دهقان کجا بود باقی</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>جز تو کز داد و دانشت حرمیست</p></div>
<div class="m2"><p>کیست کو را به جای خود کرمیست</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>من که الحق شناختم به قیاس</p></div>
<div class="m2"><p>کاهل فرهنگ را تو داری پاس</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>نخری زرق کیمیاسازان</p></div>
<div class="m2"><p>نپذیری فریب طنازان</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>نقش این کارنامه ابدی</p></div>
<div class="m2"><p>در تو بستم به طالع رصدی</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>مقبل آن کس که دخل دانه او</p></div>
<div class="m2"><p>بر چنین آورد به خانه او</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>کابد الدهر تا بود بر جای</p></div>
<div class="m2"><p>باشد از نام او صحیفه گشای</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>نه چنان کز پس قرانی چند</p></div>
<div class="m2"><p>قلمش درکشد سپهر بلند</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>چونکه پختم به دور هفت هزار</p></div>
<div class="m2"><p>دیگ پختی چنین به هفت افزار</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>نوشش از بهر جان فروزی تست</p></div>
<div class="m2"><p>نوش بادت بخور که روزی تست</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>چاشنی گیریش به جان کردم</p></div>
<div class="m2"><p>وانگهی بر تو جانفشان کردم</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>ای فلکها به خویش تو بلند</p></div>
<div class="m2"><p>هم فلک زاد و هم فلک پیوند</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>بر فلک چون پرم که من زمیم</p></div>
<div class="m2"><p>کی رسم در فرشته کادمیم</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>خواستم تا به نیشکر قلمی</p></div>
<div class="m2"><p>سبزه رویانم از سواد زمی</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>از شکر توشه‌های راه کنم</p></div>
<div class="m2"><p>تا شکر ریز بزم شاه کنم</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>گز نیم محرم شکر ریزی</p></div>
<div class="m2"><p>پاس دار شهم به شب خیزی</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>آفتابست شاه عالمتاب</p></div>
<div class="m2"><p>دیده من شده برابرش آب</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>آفتاب ار توان بر آب زدن</p></div>
<div class="m2"><p>آب نتوان بر آفتاب زدن</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>چشم با چشمه‌گر نمی‌سازد</p></div>
<div class="m2"><p>با خیالش خیال می‌بازد</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>چیست کان نیست در خزینه شاه</p></div>
<div class="m2"><p>به جز این نقد نو رسیده ز راه</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>دستگاهیش ده به سم سمند</p></div>
<div class="m2"><p>تا شود پایگاهش از تو بلند</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>کشته کوه کابر ساقی اوست</p></div>
<div class="m2"><p>خوردن آب چه ندارد دوست</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>من که محتاج آب آن دستم</p></div>
<div class="m2"><p>از دگر آبها دهان بستم</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>نقص در باشد اربها کنمش</p></div>
<div class="m2"><p>هم به تسلیم شه رها کنمش</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>گر نیوشی چو زهره راه نوم</p></div>
<div class="m2"><p>کنی انگشت کش چو ماه نوم</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>ورنه بینی که نقش بس خردست</p></div>
<div class="m2"><p>باد ازین گونه گل بسی بردست</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>عمر بادت که داد و دین داری</p></div>
<div class="m2"><p>آن دهادت خدا که این داری</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>هرچه نیک اوفتد ز دولت تست</p></div>
<div class="m2"><p>عهد آن چیز باد بر تو درست</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>وآنچه دور افتد از عنایت تو</p></div>
<div class="m2"><p>دور باد از تو و ولایت تو</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>باد تا بر سپهر تابد هور</p></div>
<div class="m2"><p>دوستت دوستکام و دشمن کور</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>دشمنانت چنان که با دل تنگ</p></div>
<div class="m2"><p>سنگ بر سر زنند و سر بر سنگ</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>بیشیت هست بیش دانی باد</p></div>
<div class="m2"><p>وز همه بیش زندگانی باد</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>از حد دولت تو دست زوال</p></div>
<div class="m2"><p>دور و مهجور باد در همه حال</p></div></div>