---
title: >-
    بخش ۲۳ - عتاب کردن بهرام با سران لشگر
---
# بخش ۲۳ - عتاب کردن بهرام با سران لشگر

<div class="b" id="bn1"><div class="m1"><p>روزی از طالع مبارک بخت</p></div>
<div class="m2"><p>رفت بهرم‌گور بر سر تخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکجا شاه و شهریاری بود</p></div>
<div class="m2"><p>تاج بخشی و تاجداری بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه در زیر تخت پایه شاه</p></div>
<div class="m2"><p>صف کشیدند چون ستاره و ماه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شه زبان برگشاد چون شمشیر</p></div>
<div class="m2"><p>گفت کای میر و مهتران دلیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لشگر از بهر صلح باید و جنگ</p></div>
<div class="m2"><p>کاین نباشد چه آدمی و چه سنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شما کیست کو به هیچ نبرد</p></div>
<div class="m2"><p>مردیی کان ز مردم آید کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من که از دهر بر گزیدمتان</p></div>
<div class="m2"><p>در کدامین مصاف دیدمتان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کامد از هیچکس چنان کاری</p></div>
<div class="m2"><p>کاید از پر دلی و عیاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از سر تیغتان به وقت گزند</p></div>
<div class="m2"><p>بر کدامین مخالف آمد بند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا که دیدم که پای پیش نهاد</p></div>
<div class="m2"><p>دشمنی بست و کشوری بگشاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این زند لاف کایرجی گهرم</p></div>
<div class="m2"><p>وان به دعوی که آرشی هنرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این ز گیو آن ز رستم آرد نام</p></div>
<div class="m2"><p>این نه کنیت هژبر و آن ضرغام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کس ندیدم که کارزاری کرد</p></div>
<div class="m2"><p>چون گه کار بود کاری کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خوشتر آن شد که هرکسی به نهفت</p></div>
<div class="m2"><p>گوید افسوس شاه ما که بخفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>می‌خورد وز کسی نیارد یاد</p></div>
<div class="m2"><p>از چنین شه کسی نباشد شاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرچه من می‌خورم چنان نخورم</p></div>
<div class="m2"><p>که ز مستی غم جهان نخورم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر خورم حوضه می از کف حور</p></div>
<div class="m2"><p>تیغم از جوی خون نباشد دور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برق‌وارم به وقت بارش میغ</p></div>
<div class="m2"><p>به یکی دست می به دیگر تیغ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می‌خورم کار مجلس آرایم</p></div>
<div class="m2"><p>تیغ را نیز کار فرمایم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خواب خرگوش من نهفته بود</p></div>
<div class="m2"><p>خصم را بیند ارچه خفته بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خنده و مستیم به تأویلست</p></div>
<div class="m2"><p>خنده شیر و مستی پیلست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شیر در وقت خنده خون ریزد</p></div>
<div class="m2"><p>کیست کز پیل مست نگریزد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ابلهان مست و بی‌خبر باشند</p></div>
<div class="m2"><p>هوشیاران می دگر باشند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آنکه در عقل پستیش نبود</p></div>
<div class="m2"><p>می‌خورد لیک مستیش نبود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر سر باده چونکه رای آرم</p></div>
<div class="m2"><p>تاج قیصر به زیر پای آرم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون منش را به باده تیز کنم</p></div>
<div class="m2"><p>بر سر خصم جرعه‌ریز کنم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دوستان را چو در می‌آویزم</p></div>
<div class="m2"><p>گنج قارون ز آستین ریزم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دشمنان را گهی که بیخ زنم</p></div>
<div class="m2"><p>به کبابی جگر به سیخ زنم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نیک‌خواهان من چه پندارند</p></div>
<div class="m2"><p>کاختران سپهر بیکارند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>من اگر چند خفته باشم و مست</p></div>
<div class="m2"><p>بخت بیدار من به کاری هست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به چنین خوابها که من مستم</p></div>
<div class="m2"><p>خواب خاقان نگر که چون بستم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به یکی پی غلط که افشردم</p></div>
<div class="m2"><p>رخت هندو نگر که چون بردم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سگ بود کو ز ناتوانی خویش</p></div>
<div class="m2"><p>خوش نخسبد به پاسبانی خویش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اژدها گرچه خسبد اندر غار</p></div>
<div class="m2"><p>شیر نر بر درش نیابد بار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شه چو این داستان خوش بر گفت</p></div>
<div class="m2"><p>روی آزادگان چو گل بشکفت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همه سر بر زمین نهادندش</p></div>
<div class="m2"><p>پاسخی عاجزانه دادندش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کانچه شه گفت با کمربندان</p></div>
<div class="m2"><p>هست پیرایه خردمندان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همه راحرز جان و تن کردیم</p></div>
<div class="m2"><p>حلقه گوش خویشتن کردیم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تاج بر فرق شه خدای نهاد</p></div>
<div class="m2"><p>کوشش خلق باد باشد باد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سرورانی که سروری کردند</p></div>
<div class="m2"><p>با تو بسیار همسری کردند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هیچکس با تو تاجور نشدند</p></div>
<div class="m2"><p>همه در سر شدند و سر نشدند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آنچه ما بنده دیده‌ایم ز شاه</p></div>
<div class="m2"><p>کس ندیدست از سپید و سیاه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دیو را بست و اژدها را سوخت</p></div>
<div class="m2"><p>پیل را کشت و کرگدن را دوخت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شیر بگذار و گور نخچیرست</p></div>
<div class="m2"><p>دام و دد خود نشانه تیرست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به جز او کیست کو به وقت شکار</p></div>
<div class="m2"><p>گردن گور درکشد به کنار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گاه سازد هدف ز خال پلنگ</p></div>
<div class="m2"><p>گاه دندان کند ز کام نهنگ</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گه در ابروی هند چین فکند</p></div>
<div class="m2"><p>گه به هندی سپاه چین شکند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گه ز فغفور باج بستاند</p></div>
<div class="m2"><p>گه ز قیصر خراج بستاند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گرچه شیر افکنان بسی بودند</p></div>
<div class="m2"><p>کز دهن مغز شیر پالودند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شیر مرد اوست کو به سیصد مرد</p></div>
<div class="m2"><p>قهر سیصد هزار دشمن کرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>قصه خسروان پیشینه</p></div>
<div class="m2"><p>هست پیدا ز مهر و از کینه</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گر برآورد هر کسی نامی</p></div>
<div class="m2"><p>بود با لشگری به ایامی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در مصافی چنین به چندان مرد</p></div>
<div class="m2"><p>آنچه او کرد کس نیارد کرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چون ز شاهان شمار برگیرند</p></div>
<div class="m2"><p>زو یکی با هزار برگیرند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هریکی را یکی نشان باشد</p></div>
<div class="m2"><p>او به تنها همه جهان باشد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>لخت بر هر سری که سخت کند</p></div>
<div class="m2"><p>چون در طارمش دو لخت کند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تیرش ار سوی سنگ خاره شود</p></div>
<div class="m2"><p>سنگ چون ریگ پاره‌پاره شود</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نوش بخشد به مهره مار سنان</p></div>
<div class="m2"><p>مار گیرد به اژدهای عنان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>هر تنی کو خلاف او سازد</p></div>
<div class="m2"><p>شمع‌وارش زمانه بگدازد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سر که بر تیغ او برون آید</p></div>
<div class="m2"><p>زان سر البته بوی خون آید</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مستی او نشان هشیاریست</p></div>
<div class="m2"><p>خواب او خواب نیست بیداریست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>وان زمانی که می‌پرست شود</p></div>
<div class="m2"><p>او خورد می عدوش مست شود</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>اوست از جمله خلق داناتر</p></div>
<div class="m2"><p>بر همه نیک و بد تواناتر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کاردان اوست در زمانه و بس</p></div>
<div class="m2"><p>نیست محتاج کاردانی کس</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تا زمین زیر چرخ دارد پای</p></div>
<div class="m2"><p>بر فلک باد حکم او را جای</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هم زمین در پناه سایه او</p></div>
<div class="m2"><p>هم فلک زیر تخت پایه او</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>کاردانان چو این سخن گفتند</p></div>
<div class="m2"><p>پیش یاقوت کهربا سفتند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شاه نعمان از آن میان برخاست</p></div>
<div class="m2"><p>بزم شه را به آفرین آراست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گفت هرجا که تخت شاه رسد</p></div>
<div class="m2"><p>گرچه ماهی بود به ماه رسد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>آدمی کیست تا به تارک شاه</p></div>
<div class="m2"><p>راست یا کج کند حساب کلاه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>افسر ایزد نهاد بر سر تو</p></div>
<div class="m2"><p>سبز باد از سر تو افسر تو</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ما که مولای بارگاه توایم</p></div>
<div class="m2"><p>سرور از سایه کلاه توایم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>از تو داریم هرچه ما را هست</p></div>
<div class="m2"><p>بر تر و خشک ما تو داری دست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>از عرب تا عجم به مولائی</p></div>
<div class="m2"><p>سر فشانیم اگر بفرمائی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>مدتی هست کز هنرمندی</p></div>
<div class="m2"><p>بر در شه کنم کمربندی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چون شدم سر بزرگ درگاهش</p></div>
<div class="m2"><p>یافتم راه توشه از راهش</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>کر مثالم دهد به معذوری</p></div>
<div class="m2"><p>تا به خانه شوم به دستوری</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>لختی از رنج ره برآسایم</p></div>
<div class="m2"><p>چون رسد حکم شاه باز آیم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گر نه تا زنده‌ام به خدمت شاه</p></div>
<div class="m2"><p>سر نگردانم از پرستش گاه</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>شاه فرمود تا ز گوهر و گنج</p></div>
<div class="m2"><p>دست خازن شود جواهرسنج</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>آورد تحفهای سلطانی</p></div>
<div class="m2"><p>مصری و مغربی و عمانی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>حمل‌داران در آمدند به کار</p></div>
<div class="m2"><p>حمل بر حمل ساختند نثار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>زر به خروار و مشک نافه به گیل</p></div>
<div class="m2"><p>وز غلام و کنیز چندین خیل</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>مرتفع جامه‌های قیمت مند</p></div>
<div class="m2"><p>بیشتر زانکه گفت شاید چند</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>تازی اسبان پارسی پرورد</p></div>
<div class="m2"><p>همه دریا گذار و کوه نورد</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>تیغ هندی و ذرع داودی</p></div>
<div class="m2"><p>کشتی جود راند بر جودی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>لعل و در بیش از آنکه قدر و قیاس</p></div>
<div class="m2"><p>داندش در فروش و لعل شناس</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>گوهر آموده تاجی از سر خویش</p></div>
<div class="m2"><p>با قبائی ز دخل ششتر بیش</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>داد تا زان دهش رخش رخشید</p></div>
<div class="m2"><p>وز یمن تا عدن به او بخشید</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>با چنین نعمتی ز درگه شاه</p></div>
<div class="m2"><p>رفت نعمان چو زهره از بر ماه</p></div></div>