---
title: >-
    بخش ۴ - سبب نظم کتاب
---
# بخش ۴ - سبب نظم کتاب

<div class="b" id="bn1"><div class="m1"><p>چون اشارت رسید پنهانی</p></div>
<div class="m2"><p>از سرا پرده سلیمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر گرفتم چو مرغ بال گشای</p></div>
<div class="m2"><p>تا کنم بر در سلیمان جای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در اشارت چنان نمود برید</p></div>
<div class="m2"><p>که هلالی برآور از شب عید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچنان کز حجاب تاریکی</p></div>
<div class="m2"><p>کس نبیند در او ز باریکی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کند صید سحرسازی تو</p></div>
<div class="m2"><p>جاودان را خیال بازی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پلپلی چند را بر آتش ریز</p></div>
<div class="m2"><p>غلغلی در فکن به آتش تیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مومی افسرده را در این گرمی</p></div>
<div class="m2"><p>نرم گردان ز بهر دل نرمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهد بیرون جهان ازین ره تنگ</p></div>
<div class="m2"><p>پای کوبی بس است بر خر لنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عطسه‌ای ده ز کلک نافه گشای</p></div>
<div class="m2"><p>تا شود باد صبح غالیه سای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باد گو رقص بر عبیر کند</p></div>
<div class="m2"><p>سبزه را مشک در حریر کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رنج بر وقت رنج بردن تست</p></div>
<div class="m2"><p>گنج شه در ورق شمردن تست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رنج برد تو ره به گنج برد</p></div>
<div class="m2"><p>ببرد گنج هر که رنج برد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تاک انگور تا نگرید زار</p></div>
<div class="m2"><p>خنده خوش نیارد آخر کار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مغز بی‌استخوان ندید کسی</p></div>
<div class="m2"><p>انگبینی کجاست بی‌مگسی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ابر بی آب چند باشی چند</p></div>
<div class="m2"><p>گرم داری تنور نان در بند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پرده بر بند و چابکی بنمای</p></div>
<div class="m2"><p>روی بکران پردگی بگشای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون برید از من این غرض درخواست</p></div>
<div class="m2"><p>شادمانی نشست و غم برخاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جستم از نامه‌های نغز نورد</p></div>
<div class="m2"><p>آنچه دل را گشاده داند کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرچه تاریخ شهر یاران بود</p></div>
<div class="m2"><p>در یکی نامه اختیار آن بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چابک اندیشه رسیده نخست</p></div>
<div class="m2"><p>همه را نظم داده بود درست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مانده زان لعل ریزه لختی گرد</p></div>
<div class="m2"><p>هر یکی زان قراضه چیزی کرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من از آن خرده چو گهر سنجی</p></div>
<div class="m2"><p>بر تراشیدم این چنین گنجی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا بزرگان چو نقد کار کنند</p></div>
<div class="m2"><p>از همه نقدش اختیار کنند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آنچ ازو نیم گفته بد گفتم</p></div>
<div class="m2"><p>گوهر نیم سفته را سفتم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وانچ دیدم که راست بود و درست</p></div>
<div class="m2"><p>ماندمش هم برآن قرار نخست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جهد کردم که در چنین ترکیب</p></div>
<div class="m2"><p>باشد آرایشی ز نقش غریب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بازجستم ز نامه‌های نهان</p></div>
<div class="m2"><p>که پراکنده بود گرد جهان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زان سخنها که تازیست و دری</p></div>
<div class="m2"><p>در سواد بخاری و طبری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وز دگر نسخها پراکنده</p></div>
<div class="m2"><p>هر دری در دفینی آکنده</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر ورق کاوفتاد در دستم</p></div>
<div class="m2"><p>همه را در خریطه‌ای بستم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون از آن جمله در سواد قلم</p></div>
<div class="m2"><p>گشت سر جمله‌ام گزیده بهم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گفتمش گفتنی که بپسندند</p></div>
<div class="m2"><p>نه که خود زیرکان بر او خندند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دیر این نامه را چو زند مجوس</p></div>
<div class="m2"><p>جلوه زان داده‌ام به هفت عروس</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا عروسان چرخ اگر یک راه</p></div>
<div class="m2"><p>در عروسان من کنند نگاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از هم آرایشی و هم کاری</p></div>
<div class="m2"><p>هر یکی را یکی کند یاری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آخر از هفت خط که یار شود</p></div>
<div class="m2"><p>نقطه‌ای بر نشان کار شود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نقشبند ارچه نقش ده دارد</p></div>
<div class="m2"><p>سر یک رشته را نگهدارد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یک سر رشته گر ز خط گردد</p></div>
<div class="m2"><p>همه سررشته‌ها غلط گردد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کس برین رشته گرچه راست نرفت</p></div>
<div class="m2"><p>راستی در میان ماست نرفت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>من چو رسام رشته پیمایم</p></div>
<div class="m2"><p>از سر رشته نگذرد پایم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>رشته یکتاست ترسم از خطرش</p></div>
<div class="m2"><p>خاصه ز اندازه برده‌ام گهرش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در هزار آب غسل باید کرد</p></div>
<div class="m2"><p>تا به آبی رسی که شاید خورد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آبی انداختند و مردم شد</p></div>
<div class="m2"><p>آب انداخته بسی گم شد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>من کزان آب در کنم چو صدف</p></div>
<div class="m2"><p>ارزم آخر به مشتی آب و علف</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سخنی خوشتر از نواله نوش</p></div>
<div class="m2"><p>کی سخاسوی من ندارد گوش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در سخاو سخن چه می‌پیچم</p></div>
<div class="m2"><p>کار بر طالع است و من هیچم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نسبت عقربی است با قوسی</p></div>
<div class="m2"><p>بخل محمود بذل فردوسی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اسدی را که بودلف بنواخت</p></div>
<div class="m2"><p>طالع و طالعی بهم در ساخت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>من چه می‌گویم این چه گفت منست</p></div>
<div class="m2"><p>کابم از ابر و درم از عدنست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>صدف از ابر گر سخا بیند</p></div>
<div class="m2"><p>ابر نیز از صدف وفا بیند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کابر آنچ از هوا نثار کند</p></div>
<div class="m2"><p>صدفش در شاهوار کند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>این سخن را که جاه می‌خواهم</p></div>
<div class="m2"><p>مدد از فیض شاه می‌خواهم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هرچه او را عیار یا عددیست</p></div>
<div class="m2"><p>سبب استقامتش مددیست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ور مدد پیش بارگه باشد</p></div>
<div class="m2"><p>چار در چار شانزده باشد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>جبرئیلم به چینی قلمم</p></div>
<div class="m2"><p>بر صحیفه چنین کشد رقمم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کین فسون را که چینی آموز است</p></div>
<div class="m2"><p>جامه نو کن که فصل نوروز است</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>آنچنان کن ز دیو پنهانش</p></div>
<div class="m2"><p>که نبیند مگر سلیمانش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>زو طلب کن مرا که فخر من اوست</p></div>
<div class="m2"><p>من کیم بازمانده لختی پوست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>موم سادم ز مهر خاتم دور</p></div>
<div class="m2"><p>خالی از انگبین و از زنبور</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تا سلیمان ز نقش خاتم خویش</p></div>
<div class="m2"><p>مهر من بر چه صورت آرد بیش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>روی اگر سرخ و گر سیاه بود</p></div>
<div class="m2"><p>نقشبندش دبیر شاه بود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بر من آن شد که در سخن سنجی</p></div>
<div class="m2"><p>ده دهی زر دهم نه ده پنجی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نخرد گر کسی عبیر مرا</p></div>
<div class="m2"><p>مشک من مایه بس حریر مرا</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>زان نمطها که رفت پیش از ما</p></div>
<div class="m2"><p>نوبری کس نداد بیش از ما</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نغز گویان که گفتنی گفتند</p></div>
<div class="m2"><p>مانده گشتند و عاقبت خفتند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ما که اجری تراش آن گرهیم</p></div>
<div class="m2"><p>پند واگیر داهیان دهیم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گرچه ز الفاظ خود به تقصیریم</p></div>
<div class="m2"><p>در معانی تمام تدبیریم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>پوست بی‌مغز دیده‌ایم چو خواب</p></div>
<div class="m2"><p>مغز بی‌پوست داده‌ایم چو آب</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>با همه نادری و نو سخنی</p></div>
<div class="m2"><p>برنتابیم روی از آن کهنی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>حاصلی نیست زین در آمودن</p></div>
<div class="m2"><p>جز به پیمانه باد پیمودن</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چیست کانرا من جواهرسنج</p></div>
<div class="m2"><p>بر نسنجیدم از جواهر و گنج</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>برگشادم بسی خزانه خاص</p></div>
<div class="m2"><p>هم کلیدی نیافتم به خلاص</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>با همه نزلهای صبح نزول</p></div>
<div class="m2"><p>هم به استغفر اللهم مشغول</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ای نظامی مسیح تو دم تست</p></div>
<div class="m2"><p>دانش تو درخت مریم تست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چون رطب ریز این درخت شدی</p></div>
<div class="m2"><p>نیک بادت که نیک‌بخت شدی</p></div></div>