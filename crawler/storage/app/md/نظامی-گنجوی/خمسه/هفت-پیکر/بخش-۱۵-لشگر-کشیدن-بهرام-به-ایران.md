---
title: >-
    بخش ۱۵ - لشگر کشیدن بهرام به ایران
---
# بخش ۱۵ - لشگر کشیدن بهرام به ایران

<div class="b" id="bn1"><div class="m1"><p>بس کن ای جادوی سخن پیوند</p></div>
<div class="m2"><p>سخن رفته چند گوئی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون گل از کام خود برار نفس</p></div>
<div class="m2"><p>کام تو عطرسازی کام تو بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچنان رفت عهد من ز نخست</p></div>
<div class="m2"><p>باکه؟ با آنکه عهد اوست درست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کانچه گوینده دگر گفتست</p></div>
<div class="m2"><p>ما به می خوردنیم و او خفتست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بازش اندیشه مال خود نکنم</p></div>
<div class="m2"><p>بد بود بد خصال خود نکنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا توانم چو باد نوروزی</p></div>
<div class="m2"><p>نکنم دعوی کهن دوزی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه در شیوه گهر سفتن</p></div>
<div class="m2"><p>شرط من نیست گفته واگفتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لیک چون ره به گنج خانه یکیست</p></div>
<div class="m2"><p>تیرها گر دو شد نشانه یکیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون نباشد ز باز گفت گزیر</p></div>
<div class="m2"><p>دانم انگیخت از پلاس حریر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دو مطرز به کیمیای سخن</p></div>
<div class="m2"><p>تازه کردند نقدهای کهن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن ز مس کرد نقره نقره خاص</p></div>
<div class="m2"><p>وین کند نقره را به زر خلاص</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مس چو دیدی که نقره شد به عیار</p></div>
<div class="m2"><p>نقره گر زر شود شگفت مدار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عقد پیوند این سریر بلند</p></div>
<div class="m2"><p>این چنین داد عقد را پیوند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که چو بهرام‌گور گشت آگاه</p></div>
<div class="m2"><p>زانچ بیگانه‌ای ربود کلاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر طلب کردن کلاه کیان</p></div>
<div class="m2"><p>کینه را در گشاد و بست میان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>داد نعمان منذرش یاری</p></div>
<div class="m2"><p>در طلب کردن جهانداری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گنج از آن بیشتر که شاید گفت</p></div>
<div class="m2"><p>گوهر افزون از آنکه شاید سفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لشگر انگیخت بیش از اندازه</p></div>
<div class="m2"><p>کینه‌ور تیز گشت و کین تازه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از یمن تا عدن ز روی شمار</p></div>
<div class="m2"><p>در هم افتاد صدهزار سوار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه پولاد پوش و آهن خای</p></div>
<div class="m2"><p>کین کش و دیو بند و قلعه گشای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر یکی در نورد خود شیری</p></div>
<div class="m2"><p>قایم کشوری به شمشیری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در روارو فتاد موکب شاه</p></div>
<div class="m2"><p>نم به ماهی رسید و گرد به ماه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ناله کرنای و روئین خم</p></div>
<div class="m2"><p>در جگر کرده زهره‌ها را گم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کوس روئین بلند کرد آواز</p></div>
<div class="m2"><p>زخمه بر کاسه ریخت کاسه‌نواز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کوه و صحرا ز بس نفیر و خروش</p></div>
<div class="m2"><p>بر طبقهای آسمان زد جوش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لشگری بیشتر ز مور و ملخ</p></div>
<div class="m2"><p>گرم کینه چو آتش دوزخ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پایگه جوی تخت شاه شدند</p></div>
<div class="m2"><p>وز یمن سوی تختگاه شدند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آگهی یافت تخت گیر جهان</p></div>
<div class="m2"><p>کاژدهائی دگر گشاد دهان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر زمین آمد آسمان را میل</p></div>
<div class="m2"><p>وز یمن سر برآورید سهیل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شیر نر پنجه برگشاد به زور</p></div>
<div class="m2"><p>تا کند خصم را چو گور به گور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تخت گیرد کلاه بستاند</p></div>
<div class="m2"><p>بنشیند غبار بنشاند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نامداران و موبدان سپاه</p></div>
<div class="m2"><p>همه گرد آمدند بر در شاه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>انجمن ساختند و رای زدند</p></div>
<div class="m2"><p>سرکشی را به پشت پای زدند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رای ایشان بدان کشید انجام</p></div>
<div class="m2"><p>که نویسند نامه بر بهرام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هرچه فرمود عقل بنوشتند</p></div>
<div class="m2"><p>پوست ناکنده دانه را کشتند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کاتب نامه سخن پرداز</p></div>
<div class="m2"><p>در سخن داد شرح حال دراز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نامه چون شد نبشته پیچیدند</p></div>
<div class="m2"><p>رفتن راه را بسیچیدند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون رسیدند و آمدند فرود</p></div>
<div class="m2"><p>شاه نو را زمانه داد درود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>حاجیان دل به کارشان دادند</p></div>
<div class="m2"><p>بار جستند و بارشان دادند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>داد بهرام شاه دستوری</p></div>
<div class="m2"><p>تا فراتر شوند ازان دوری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پیش رفتند با هزار هراس</p></div>
<div class="m2"><p>سجده بردند و داشتند سپاس</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آن کزان جمله گوی دانش برد</p></div>
<div class="m2"><p>بر سر نامه بوسه داد و سپرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نامه را مهر برگشاد دبیر</p></div>
<div class="m2"><p>خواند بر شهریار کشور گیر</p></div></div>