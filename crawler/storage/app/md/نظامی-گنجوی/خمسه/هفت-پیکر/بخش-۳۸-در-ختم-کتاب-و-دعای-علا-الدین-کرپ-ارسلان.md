---
title: >-
    بخش ۳۸ - در ختم کتاب و دعای علاء الدین کرپ ارسلان
---
# بخش ۳۸ - در ختم کتاب و دعای علاء الدین کرپ ارسلان

<div class="b" id="bn1"><div class="m1"><p>چون فروزنده شد به عکس و عیار</p></div>
<div class="m2"><p>نقد این گنجه خیز رومی کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام شاهنشهی برو بستم</p></div>
<div class="m2"><p>کاب گیرد ز نقش او دستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه رومی قبای چینی تاج</p></div>
<div class="m2"><p>جزیتش داده چین و روم خراج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یافته از ره اصول و فروع</p></div>
<div class="m2"><p>بخت ایشوع و رای بختیشوع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر زمین بوسش آسمان بر پای</p></div>
<div class="m2"><p>و آفرینش ز جاه او بر جای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در نظامی که آسمان دارد</p></div>
<div class="m2"><p>اجری مملکت دو نان دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان مروت که بوی مشک دهد</p></div>
<div class="m2"><p>لؤلؤتر چو خاک خشک دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از زمین تا اثیر درد و کفست</p></div>
<div class="m2"><p>صافی او شد که مایه شرفست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ذهب دادنش به سائل خویش</p></div>
<div class="m2"><p>زر مصری ز ریگ مکی بیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تیغش آن کرده در صلابت سنگ</p></div>
<div class="m2"><p>کاتش تیز با تراش خدنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بید برگش به نوک موی شکاف</p></div>
<div class="m2"><p>نافه کوه را فکنده ز ناف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درعش از دست صبح نیزه گشای</p></div>
<div class="m2"><p>نیزش از درع ماه حلقه ربای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شش جهت بر قبای او زرهی</p></div>
<div class="m2"><p>هفت چرخ از کمند او گرهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای نظامی امیدوار به تو</p></div>
<div class="m2"><p>نظم دوران روزگار به تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زمی از قدرت آسمان داند</p></div>
<div class="m2"><p>و آسمانت هم آسمان خواند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دور و نزدیگ چون در آب سپهر</p></div>
<div class="m2"><p>تیز و آهسته چون در آینه مهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قائم عهد عالمی به درست</p></div>
<div class="m2"><p>قائم نامده فکندهٔ تست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با همه چون ملک بر آمده‌ای</p></div>
<div class="m2"><p>وز همه چون فلک سر آمده‌ای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این چنین نامه بر تو شاید بست</p></div>
<div class="m2"><p>کز تو جای بلند نامی هست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چونکه شد لعل بسته بر تاجش</p></div>
<div class="m2"><p>بر تو بستم ز بیم تاراجش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر به سمع تو دلپسند شود</p></div>
<div class="m2"><p>چون سریر تو سربلند شود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خار کان انگبین بر او رانند</p></div>
<div class="m2"><p>زیرکانش ترانگبین خوانند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>میوه‌ای دادمت ز باغ ضمیر</p></div>
<div class="m2"><p>چرب و شیرین چو انگبین در شیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ذوق انجیر داده دانهٔ او</p></div>
<div class="m2"><p>مغز بادام در میانهٔ او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پیش بیرونیان برونش نغز</p></div>
<div class="m2"><p>وز درونش درونیان را مغز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حقه‌ای بسته پر ز در دارد</p></div>
<div class="m2"><p>وز عبارت کلید پر دارد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در دران رشته سر گرای بود</p></div>
<div class="m2"><p>که کلیدش گره گشای بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر چه در نظم او ز نیک و بدست</p></div>
<div class="m2"><p>همه رمز و اشارت خردست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر یک افسانه‌ای جداگانه</p></div>
<div class="m2"><p>خانهٔ گنج شد نه افسانه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آنچه کوتاه جامه شد جسدش</p></div>
<div class="m2"><p>کردم از نظم خود دراز قدش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وآنچه بودش درازی از حد بیش</p></div>
<div class="m2"><p>کوتهی دادمش به صنعت خویش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کردم این تحفه را گزارش نغز</p></div>
<div class="m2"><p>اینت چرب استخوان شیرین مغز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا دراری به حسن او نظری</p></div>
<div class="m2"><p>جلوه‌ای دادمش به هر هنری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>لطف بسیار دخل اندک خرج</p></div>
<div class="m2"><p>کرده در هر دقیقه درجی درج</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دست ناکرده دلستانی چند</p></div>
<div class="m2"><p>بکر چون روی غنچه زیر پرند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مصرعی زر و مصرعی از در</p></div>
<div class="m2"><p>تهی از دعوی و ز معنی پر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا بدانند کز ضمیر شگرف</p></div>
<div class="m2"><p>هر چه خواهم دراورم به دو حرف</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>وانچه بر هفت کنج خانهٔ راز</p></div>
<div class="m2"><p>بستم آرایشی فراخ و دراز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>غرض آن شد که چشم از آرایش</p></div>
<div class="m2"><p>در فراخی پذیرد آسایش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آنچه بینی که بر بساط فراخ</p></div>
<div class="m2"><p>کرده‌ام چشم و گوش را گستاخ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تنگ چشمان معنیم هستند</p></div>
<div class="m2"><p>که رخ از چشم تنگ بربستند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هر عروسی چو گنج سر بسته</p></div>
<div class="m2"><p>زیر زلفش کلید زر بسته</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر که این کان گشاد زر باید</p></div>
<div class="m2"><p>بلکه در یابد آن که دریابد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>من که نقاش نیشکر قلمم</p></div>
<div class="m2"><p>رطب افشان نخل این حرمم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نی کلکم ز کشتزار هنر</p></div>
<div class="m2"><p>به عطارد رساند سنبل تر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سنبله کرد سنبلم را خاص</p></div>
<div class="m2"><p>گرچه القاص لایحب القاص</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون من از قلعه قناعت خویش</p></div>
<div class="m2"><p>شاه را گنج زر کشیدم پیش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>در ادا کردن زر جایز</p></div>
<div class="m2"><p>وامدار منست روئین دز</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>وامداری نه کز تهی شکمی</p></div>
<div class="m2"><p>دز روئین بود ز بی در می</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کاهن تیز آن گریوهٔ سنگ</p></div>
<div class="m2"><p>لعل و الماس ریخت صد فرسنگ</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>لعل بر دست دوستان به قیاس</p></div>
<div class="m2"><p>وز پی پای دشمنان الماس</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آن نه دز کعبه مسلمانیست</p></div>
<div class="m2"><p>مقدس رهروان روحانیست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>میخ زرین و مرکز زمی است</p></div>
<div class="m2"><p>نام رویین دزش ز محکمی است</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>یافت دریافت نارسیده او</p></div>
<div class="m2"><p>زهره را هم زره دریده او</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>جبل الرحمه زان حریم دریست</p></div>
<div class="m2"><p>بو قبیس از کلاه او کمریست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ابدی باد خط این پرگار</p></div>
<div class="m2"><p>زان بلند آفتاب نقطه قرار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>در دزی چون حصار پیوندند</p></div>
<div class="m2"><p>نامه‌ای بر کبوتری بندند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تا برد نامه را کبوتر شاد</p></div>
<div class="m2"><p>بر آنکس که او رسد فریاد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>من که در شهر بند کشور خویش</p></div>
<div class="m2"><p>بسته دارم گریز گه پس و پیش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نامه در مرغ نامه بربستم</p></div>
<div class="m2"><p>کو رساند به شاه من رستم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ای فلک بر در تو حلقه به گوش</p></div>
<div class="m2"><p>هم خطا پوش و هم خطائی پوش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چون مرا دولت تو یاری کرد</p></div>
<div class="m2"><p>طبع بین تا چه سحرکاری کرد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>از پس پانصد و نود سه بران</p></div>
<div class="m2"><p>گفتم این نامه را چو ناموران</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>روز بر چارده ز ماه صیام</p></div>
<div class="m2"><p>چار ساعت ز روز رفته تمام</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>باد بر تو مبارک این پیوند</p></div>
<div class="m2"><p>تا نشینی بر این سریر بلند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نوشی آب حیات ازین ابیات</p></div>
<div class="m2"><p>زنده مانی چو خضر از آب حیات</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ای که در ملک جاودان بادی</p></div>
<div class="m2"><p>ملک با عمر و عمر با شادی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>گر نرنجی ز راه معذوری</p></div>
<div class="m2"><p>گویمت نکته‌ای به دستوری</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بزمهای تو گرچه رنگینست</p></div>
<div class="m2"><p>آنچه بزم مخلد است اینست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>هر چه هست از حساب گوهر و گنج</p></div>
<div class="m2"><p>راحت اینست و آن دگر همه رنج</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>آن اگر صد کشد به پانصد سال</p></div>
<div class="m2"><p>دیر زی تو که هم رسد به زوال</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>وین خزینه که خاص درگاهست</p></div>
<div class="m2"><p>ابدالدهر با تو همراهست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>این سخن را که شد خرد پرورد</p></div>
<div class="m2"><p>بر دعای تو ختم خواهی کرد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دولتی باش هر کجا باشی</p></div>
<div class="m2"><p>در رکابت فلک به فراشی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>دولتت را که بر زیارت باد</p></div>
<div class="m2"><p>خاتم کار بر سعادت باد</p></div></div>