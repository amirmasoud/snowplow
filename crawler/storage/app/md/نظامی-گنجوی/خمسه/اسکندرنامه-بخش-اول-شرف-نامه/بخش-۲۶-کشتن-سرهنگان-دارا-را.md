---
title: >-
    بخش ۲۶ - کشتن سرهنگان دارا را
---
# بخش ۲۶ - کشتن سرهنگان دارا را

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی از من مرا دور کن</p></div>
<div class="m2"><p>جهان از می‌لعل پر نور کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میی کو مرا ره به منزل برد</p></div>
<div class="m2"><p>همه دل برند او غم دل برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان گر چه آرامگاهی خوشست</p></div>
<div class="m2"><p>شتابنده را نعل در آتشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو در دارد این باغ آراسته</p></div>
<div class="m2"><p>در و بند ازین هر دو برخاسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درا از درباغ و بنگر تمام</p></div>
<div class="m2"><p>ز دیگر در باغ بیرون خرام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر زیرکی با گلی خو مگیر</p></div>
<div class="m2"><p>که باشد بجا ماندنش ناگزیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ایندم که داری به شادی بسیچ</p></div>
<div class="m2"><p>که آینده و رفته هیچست هیچ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه‌ایم آمده از پی دلخوشی</p></div>
<div class="m2"><p>مگر کز پی رنج و سختی کشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خزان را کسی در عروسی نخواند</p></div>
<div class="m2"><p>مگر وقت آن کاب و هیزم نماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گزارندهٔ نظم این داستان</p></div>
<div class="m2"><p>سخن راند بر سنت راستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که چون آتش روز روشن گذشت</p></div>
<div class="m2"><p>پر از دود شد گنبد تیز گشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شب از ماه بربست پیرایه‌ای</p></div>
<div class="m2"><p>شگفتی بود نور بر سایه‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طلایه ز لشگرگه هر دو شاه</p></div>
<div class="m2"><p>شده پاس دارنده تا صبحگاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یتاقی به آمد شدن چون خراس</p></div>
<div class="m2"><p>نیاسود دراجه از بانگ پاس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسا خفته کز هیبت پیل مست</p></div>
<div class="m2"><p>سراسیمه هر ساعت از خواب جست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غنوده تن مرد از رنج و تاب</p></div>
<div class="m2"><p>نظر هر زمانی درآمد ز خواب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیایش کنان هر دو لشگر به راز</p></div>
<div class="m2"><p>که‌ای کاشکی بودی امشب دراز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مگر کان درازی نمودی درنگ</p></div>
<div class="m2"><p>به دیری پدید آمدی روز جنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سگالش چنان شد دو کوشنده را</p></div>
<div class="m2"><p>که ریزند صفرای جوشنده را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو خورشید روشن برآرد کلاه</p></div>
<div class="m2"><p>پدیدار گردد سپید از سیاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دو خسرو عنان در عنان آورند</p></div>
<div class="m2"><p>ره دوستی در میان آورند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به آزرم خشنودی از یکدیگر</p></div>
<div class="m2"><p>بتابند و زان برنتابند سر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو دارا دران داوری رای جست</p></div>
<div class="m2"><p>دل رای زن بود در رای سست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سوی آشتی کس نشد رهنمون</p></div>
<div class="m2"><p>نمودند رایش به شمشیر و خون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که ایرانی از رومی بیش خورد</p></div>
<div class="m2"><p>به قایم کجا ریزد اندر نبرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو فردا فشاریم در جنگ پای</p></div>
<div class="m2"><p>ز رومی نمانیم یک تن بجای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدین عشوه دادند شه را شکیب</p></div>
<div class="m2"><p>یکی بر دلیری یکی بر فریب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همان قاصدان نیز کردند جهد</p></div>
<div class="m2"><p>که بر خون او بسته بودند عهد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سکندر ز دیگر طرف چاره ساز</p></div>
<div class="m2"><p>که چون پای دارد دران ترکتاز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خیال دو سرهنگ را پیش داشت</p></div>
<div class="m2"><p>جز آن خود که سرهنگی خویش داشت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چنین گفت با پهلوانان روم</p></div>
<div class="m2"><p>که فردا درین مرکز سخت بوم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بکوشیم کوشیدنی مردوار</p></div>
<div class="m2"><p>رگ جان به کوشش کنیم استوار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر دست بردیم ماراست ملک</p></div>
<div class="m2"><p>وگر ما شدیم آن داراست ملک</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>قیامت که پوشیدهٔ رای ماست</p></div>
<div class="m2"><p>بود روزی آن روز فردای ماست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به اندیشه‌هائی چنین هولناک</p></div>
<div class="m2"><p>دو لشگر غنودند با ترس و باک</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو گیتی در روشنی باز کرد</p></div>
<div class="m2"><p>جهان بازی دیگر آغاز کرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به آتش به دل گشت مشتی شرار</p></div>
<div class="m2"><p>کلیچه شد آن سیم کاووس وار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>درآمد به جنبش دو لشگر چو کوه</p></div>
<div class="m2"><p>کز آن جنبش آمد جهان را ستوه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فریدون نسب شاه بهمن نژاد</p></div>
<div class="m2"><p>چو برخاست از اول بامداد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همه ساز لشگر به ترتیب جنگ</p></div>
<div class="m2"><p>برآراست از جعبه نیم لنگ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز پولاد صد کوه بر پای کرد</p></div>
<div class="m2"><p>به پائین او گنج را جای کرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو بر میمنه سازور گشت کار</p></div>
<div class="m2"><p>همان میسره شد چو روئین حصار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جناح از هوا در زمین برد بیخ</p></div>
<div class="m2"><p>پس آهنگ شد چون زمین چار میخ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جهاندار در قلبگه کرد جای</p></div>
<div class="m2"><p>درفش کیانیش بر سر به پای</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سکندر که تیغ جهان‌سوز داشت</p></div>
<div class="m2"><p>چنان تیغی از بهر آن روز داشت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>برانگیخت رزمی چو بارنده میغ</p></div>
<div class="m2"><p>تگرگش ز پیکان و باران ز تیغ</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>جناح سپه را به گردون کشید</p></div>
<div class="m2"><p>سم بارکی بر سر خون کشید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گرانمایگان را بدانسان که خواست</p></div>
<div class="m2"><p>بفرمود رفتن سوی دست راست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گروهی که پرتابیان ساختشان</p></div>
<div class="m2"><p>چپ انداز شد بر چپ انداختشان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همان استواران درگاه را</p></div>
<div class="m2"><p>کز ایشان بدی ایمنی شاه را</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به قلب اندرون داشت با خویشتن</p></div>
<div class="m2"><p>چو پولاد کوهی شد آن پیلتن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>برآمد ز قلب دو لشگر خروش</p></div>
<div class="m2"><p>رسید آسمان را قیامت به گوش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تبیره بغرید چون تند شیر</p></div>
<div class="m2"><p>درآمد به رقص اژدهای دلیر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ز شوریدن ناله کر نای</p></div>
<div class="m2"><p>برافتاد تب لرزه بر دست و پای</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز فریاد روئین خم از پشت پیل</p></div>
<div class="m2"><p>نفیر نهنگان برآمد ز نیل</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز بس بانگ شیپور زهره شکاف</p></div>
<div class="m2"><p>بدرید زهره بپیچید ناف</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز غریدن کوس خالی دماغ</p></div>
<div class="m2"><p>زمین لرزه افتاد در کوه و راغ</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>درآمد ز بحران سر بید برگ</p></div>
<div class="m2"><p>گشاده بر او روزن درع و ترگ</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز بس تیر باران که آمد به جوش</p></div>
<div class="m2"><p>فکند ابر بارانی خود ز دوش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گران تیر باران کنون آمدی</p></div>
<div class="m2"><p>بجای نم از ابر خون آمدی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خروشیدن کوس روئینه کاس</p></div>
<div class="m2"><p>نیوشنده را داد بر جان هراس</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>جلاجل زنان از نواهای زنگ</p></div>
<div class="m2"><p>برآورده خون از دل خاره سنگ</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به جنبش درآمد دو دریای خون</p></div>
<div class="m2"><p>شد از موج آتش زمین لاله گون</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>زمین کو بساطی شد آراسته</p></div>
<div class="m2"><p>غباری شد از جای برخاسته</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به ابرو درآمد کمان را شکنج</p></div>
<div class="m2"><p>شتابان شده تیر چون مار گنج</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ستیزنده از تیغ سیماب ریز</p></div>
<div class="m2"><p>چو سیماب کرده گریزا گریز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ز پولاد پیکان پیکر شکن</p></div>
<div class="m2"><p>تن کوه لرزنده بر خویشتن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز نوک سنان چرخ دولاب رنگ</p></div>
<div class="m2"><p>ز پرگار گردش فرو مانده لنگ</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ز بس زخم کوپال خارا ستیز</p></div>
<div class="m2"><p>زمین را شده استخوان ریز ریز</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ز بس در دهن ناچخ انداختن</p></div>
<div class="m2"><p>نفس را نه راه برون تاختن</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>سنان در سنان رسته چون نوک خار</p></div>
<div class="m2"><p>سپر بر سپر بسته چون لاله‌زار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>گریزندگان را در آن رستخیز</p></div>
<div class="m2"><p>نه روی رهائی نه راه گریز</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سواران همه تیر پرداخته</p></div>
<div class="m2"><p>گهی تیر و گه ترکش انداخته</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>در آن مسلخ آدمیزادگان</p></div>
<div class="m2"><p>زمین گشته کوه از بس افتادگان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به جان برد خود هر کسی گشته شاد</p></div>
<div class="m2"><p>کس از کشته خود نیاورده یاد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ندارد کسی سوک در حربگاه</p></div>
<div class="m2"><p>نه کس جز قراکند پوشد سپاه</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سخن گو سخن سخت پاکیزه راند</p></div>
<div class="m2"><p>که مرگ به انبوه را جشن خواند</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو مرگ از یکی تن برارد هلاک</p></div>
<div class="m2"><p>شود شهری از گریه اندوهناک</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به مرگ همه شهر ازین شهر دور</p></div>
<div class="m2"><p>نگرید کس ارچه بود ناصبور</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ز بس کشته بر کشته مردان مرد</p></div>
<div class="m2"><p>شده راه بر بسته بر ره نورد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بران دجله خون بلند آفتاب</p></div>
<div class="m2"><p>چو نیلوفر افکنده زورق دراب</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>سنان سکندر دران داوری</p></div>
<div class="m2"><p>سبق برده از چشمه خاوری</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>شراری که شمشیر دارا فکند</p></div>
<div class="m2"><p>تبش در دل سنگ خارا فکند</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چو لشگر به لشگر درآمیختند</p></div>
<div class="m2"><p>قیامت ز گیتی برانگیختند</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>پراکندگی در سپاه اوفتاد</p></div>
<div class="m2"><p>برینش در آزرم شاه اوفتاد</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>سپه چون پراکنده شد سوی جنگ</p></div>
<div class="m2"><p>فراخی درآمد به میدان تنگ</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>کس از خاصگان پیش دارا نبود</p></div>
<div class="m2"><p>کزو در دل کس مدارا نبود</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>دو سرهنگ غدار چون پیل مست</p></div>
<div class="m2"><p>بر آن پیلتن بر گشادند دست</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>زدندش یکی تیغ پهلو گذار</p></div>
<div class="m2"><p>که از خون زمین گشت چون لاله‌زار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>درافتاد دارا بدان زخم تیز</p></div>
<div class="m2"><p>ز گیتی برآمد یکی رستخیز</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>درخت کیانی درآمد به خاک</p></div>
<div class="m2"><p>بغلطید در خون تن زخمناک</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>برنجد تن نازک از درد و داغ</p></div>
<div class="m2"><p>چه خویشی بود باد را با چراغ</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>کشنده دو سرهنگ شوریده رای</p></div>
<div class="m2"><p>به نزد سکندر گرفتند جای</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>که آتش ز دشمن برانگیختیم</p></div>
<div class="m2"><p>به اقبال شه خون او ریختیم</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ز دارا سر تخت پرداختیم</p></div>
<div class="m2"><p>سرتاج اسکندر افراختیم</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>به یک زخم کردیم کارش تباه</p></div>
<div class="m2"><p>سپردیم جانش به فتراک شاه</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بیا تا ببینی و باور کنی</p></div>
<div class="m2"><p>به خونش سم بارگی ترکنی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چو آمد ز ما آنچه کردیم رای</p></div>
<div class="m2"><p>تو نیز آنچه گفتی بیاور بجای</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>به ما بخش گنجی که پذرفته‌ای</p></div>
<div class="m2"><p>وفا کن به چیزی که خود گفته‌ای</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>سکندر چو دانست کان ابلهان</p></div>
<div class="m2"><p>دلیرند بر خون شاهنشهان</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>پشیمان شد از کرده پیمان خویش</p></div>
<div class="m2"><p>که برخاستش عصمت از جان خویش</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>فرو میرد امیدواری ز مرد</p></div>
<div class="m2"><p>چو همسال را سر درآید بگرد</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>نشان جست کان کشور آرای کی</p></div>
<div class="m2"><p>کجا خوابگه دارد از خون و خوی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>دو بیداد پیشه به پیش اندرون</p></div>
<div class="m2"><p>به بیداد خود شاه را رهنمون</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>چو در موکب قلب دارا رسید</p></div>
<div class="m2"><p>ز موکب روان هیچ‌کس را ندید</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>تن مرزبان دید در خاک و خون</p></div>
<div class="m2"><p>کلاه کیانی شده سرنگون</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>سلیمانی افتاده در پای مور</p></div>
<div class="m2"><p>همان پشهٔ کرده بر پیل زور</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>به بازوی بهمن برآموده مار</p></div>
<div class="m2"><p>ز روئین در افتاده اسفندیار</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>بهار فریدون و گلزار جم</p></div>
<div class="m2"><p>به باد خزان گشته تاراج غم</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>نسب نامه دولت کیقباد</p></div>
<div class="m2"><p>ورق بر ورق هر سوئی برده باد</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>سکندر فرود آمد از پشت بور</p></div>
<div class="m2"><p>درآمد به بالین آن پیل زور</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بفرمود تا آن دو سرهنگ را</p></div>
<div class="m2"><p>دو کنج زخمه خارج آهنگ را</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>بدارند بر جای خویش استوار</p></div>
<div class="m2"><p>خود از جای جنبید شوریده‌وار</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>به بالینگه خسته آمد فراز</p></div>
<div class="m2"><p>ز درع کیانی گره کرد باز</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>سر خسته را بر سر ران نهاد</p></div>
<div class="m2"><p>شب تیره بر روز رخشان نهاد</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>فرو بسته چشم آن تن خوابناک</p></div>
<div class="m2"><p>بدو گفت برخیز ازین خون و خاک</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>رها کن که در من رهائی نماند</p></div>
<div class="m2"><p>چراغ مرا روشنائی نماند</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>سپهرم بدانگونه پهلو درید</p></div>
<div class="m2"><p>که شد در جگر پهلویم ناپدید</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>تو ای پهلوان کامدی سوی من</p></div>
<div class="m2"><p>نگهدار پهلو ز پهلوی من</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>که با آنکه پهلو دریدم چو میغ</p></div>
<div class="m2"><p>همی آید از پهلویم بوی تیغ</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>سر سروران را رها کن ز دست</p></div>
<div class="m2"><p>تو مشکن که ما را جهان خود شکست</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>چو دستی که بر ما درازی کنی</p></div>
<div class="m2"><p>به تاج کیان دست‌یازی کنی</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>نگهدار دستت که داراست این</p></div>
<div class="m2"><p>نه پنهان چو روز آشکاراست این</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>چو گشت آفتاب مرا روی زرد</p></div>
<div class="m2"><p>نقابی به من درکش از لاجورد</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>مبین سرو را در سرافکندگی</p></div>
<div class="m2"><p>چنان شاه را در چنین بندگی</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>درین بندم از رحمت آزاد کن</p></div>
<div class="m2"><p>به آمرزش ایزدم یاد کن</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>زمین را منم تاج تارک نشین</p></div>
<div class="m2"><p>ملرزان مرا تا نلرزد زمین</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>رها کن که خواب خوشم میبرد</p></div>
<div class="m2"><p>زمین آب و چرخ آتشم میبرد</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>مگردان سر خفته را از سریر</p></div>
<div class="m2"><p>که گردون گردان برآرد نفیر</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>زمان من اینک رسد بی‌گمان</p></div>
<div class="m2"><p>رها کن به خواب خوشم یک زمان</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>اگر تاج خواهی ربود از سرم</p></div>
<div class="m2"><p>یکی لحظه بگذار تا بگذرم</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>چو من زین ولایت گشادم کمر</p></div>
<div class="m2"><p>تو خواه افسر از من ستان خواه سر</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>سکندر بنالید کای تاجدار</p></div>
<div class="m2"><p>سکندر منم چاکر شهریار</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>نخواهم که بر خاک بودی سرت</p></div>
<div class="m2"><p>نه آلودهٔ خون شدی پیکرت</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>ولیکن چه سودست کاین کار بود</p></div>
<div class="m2"><p>تأسف ندارد درین کار سود</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>اگر تاجور سر برافراختی</p></div>
<div class="m2"><p>کمر بند او چاکری ساختی</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>دریغا به دریا کنون آمدم</p></div>
<div class="m2"><p>که تا سینه در موج خون آمدم</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>چرا مرکبم را نیفتاد سم</p></div>
<div class="m2"><p>چرا پی نکردم درین راه گم</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>مگر ناله شاه نشنیدمی</p></div>
<div class="m2"><p>نه روزی بدین روز را دیدمی</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>به دارای گیتی و دانای راز</p></div>
<div class="m2"><p>که دارم به بهبود دارا نیاز</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>ولیکن چو بر شیشه افتاد سنگ</p></div>
<div class="m2"><p>کلید در چاره ناید به چنگ</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>دریغا که از نسل اسفندیار</p></div>
<div class="m2"><p>همین بود و بس ملک را یادگار</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>چه بودی که مرگ آشکارا شدی</p></div>
<div class="m2"><p>سکندر هم آغوش دارا شدی</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>چه سودست مردن نشاید به زور</p></div>
<div class="m2"><p>که پیش از اجل رفت نتوان به گور</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>به نزدیک من یکسر موی شاه</p></div>
<div class="m2"><p>گرامیتر از صد هزاران کلاه</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>گر این زخم را چاره دانستمی</p></div>
<div class="m2"><p>طلب کردمی تا توانستمی</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>نه تاج و نه اورنگ شاهنشهی</p></div>
<div class="m2"><p>که ماند ز دارای دولت تهی</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>چرا خون نگریم بران تاج و تخت</p></div>
<div class="m2"><p>که دارنده را بر درافکند رخت</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>مباد آن گلستان که سالار او</p></div>
<div class="m2"><p>بدین خستگی باشد از خار او</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>نفیر از جهانی که دارا کشست</p></div>
<div class="m2"><p>نهان پرور و آشکارا کشست</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>به چاره‌گری چون ندارم توان</p></div>
<div class="m2"><p>کنم نوحه بر زاد سرو جوان</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>چه تدبیر داری مراد تو چیست</p></div>
<div class="m2"><p>امید از که داری و بیمت ز کیست</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>بگو هر چه داری که فرمان کنم</p></div>
<div class="m2"><p>به چاره‌گری با تو پیمان کنم</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>چو دارا شنید این دم دل‌نواز</p></div>
<div class="m2"><p>به خواهشگری دیده را کرد باز</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>بدو گفت کای بهترین بخت من</p></div>
<div class="m2"><p>سزاوار پیرایه و تخت من</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>چه پرسی ز جانی به جان آمده</p></div>
<div class="m2"><p>گلی در سموم خزان آمده</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>جهان شربت هرکس از یخ سرشت</p></div>
<div class="m2"><p>بجز شربت ما که بر یخ نوشت</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>ز بی آبیم سینه سوزد درون</p></div>
<div class="m2"><p>قدم تا سرم غرق دریای خون</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>چوبرقی که در ابر دارد شتاب</p></div>
<div class="m2"><p>لب از آب خالی و تن غرق آب</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>سبوئی که سوراخ باشد نخست</p></div>
<div class="m2"><p>به موم و سریشم نگردد درست</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>جهان غارت از هر دری میبرد</p></div>
<div class="m2"><p>یکی آورد دیگری میبرد</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>نه زو ایمن اینان که هستند نیز</p></div>
<div class="m2"><p>نه آنان که رفتند رستند نیز</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>ببین روز من راستی پیشه کن</p></div>
<div class="m2"><p>تو تیز از چنین روزی اندیشه کن</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>چو هستی به پند من آموزگار</p></div>
<div class="m2"><p>بدین روز ننشاندت روزگار</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>نه من به ز بهمن شدم کاژدها</p></div>
<div class="m2"><p>بخاریدن سر نکردش رها</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>نه ز اسفندیار آن جهانگیر گرد</p></div>
<div class="m2"><p>که از چشم زخم جهان جان نبرد</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>چو در نسل ما کشتن آمد نخست</p></div>
<div class="m2"><p>کشنده نسب کرد بر ما درست</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>تو سرسبز بادی به شاهنشهی</p></div>
<div class="m2"><p>که من کردم از سبزه بالین تهی</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>چو درخواستی کارزوی تو چیست</p></div>
<div class="m2"><p>به وقتی که بر من بباید گریست</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>سه چیز آرزو دارم اندر نهان</p></div>
<div class="m2"><p>براید به اقبال شاه جهان</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>یکی آنکه بر کشتن بی‌گناه</p></div>
<div class="m2"><p>تو باشی درین داوری دادخواه</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>دویم آنکه بر تاج و تخت کیان</p></div>
<div class="m2"><p>چو حاکم تو باشی نیاری زیان</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>دل خود بپردازی از تخم کین</p></div>
<div class="m2"><p>نپردازی از تخمه ما زمین</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>سوم آنکه بر زیردستان من</p></div>
<div class="m2"><p>حرم نشکنی در شبستان من</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>همان روشنک را که دخت منست</p></div>
<div class="m2"><p>بدان نازکی دست پخت منست</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>بهم خوابی خود کنی سربلند</p></div>
<div class="m2"><p>که خوان گردد از نازکان ارجمند</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>دل روشن از روشنک برمتاب</p></div>
<div class="m2"><p>که با روشنی به بود آفتاب</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>سکندر پذیرفت ازو هر چه گفت</p></div>
<div class="m2"><p>پذیرنده برخاست گوینده خفت</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>کبودی و کوژی درآمد به چرخ</p></div>
<div class="m2"><p>که بغداد را کرد به کاخ و کرخ</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>درخت کیان را فرو ریخت بار</p></div>
<div class="m2"><p>کفن دوخت بر درع اسفندیار</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>چو مهر از جهان مهربانی برید</p></div>
<div class="m2"><p>شبه ماند و یاقوت شد ناپدید</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>سکندر بدان شاه فرخ نژاد</p></div>
<div class="m2"><p>شبانگاه بگریست تا بامداد</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>درو دید و بر خویشتن نوحه کرد</p></div>
<div class="m2"><p>که او را همان زهر بایست خورد</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>چو روز آخور صبح ابلق سوار</p></div>
<div class="m2"><p>طویله برون زد بر این مرغزار</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>سکندر بفرمود کارند ساز</p></div>
<div class="m2"><p>برندش بجای نخستینه باز</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>ز مهد زر و گنبد سنگ بست</p></div>
<div class="m2"><p>مهیاش کردند جای نشست</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>چو خلوتگهش آن چنان ساختند</p></div>
<div class="m2"><p>ازو زحمت خویش پرداختند</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>تنومند را قدر چندان بود</p></div>
<div class="m2"><p>که در خانه کالبد جان بود</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>چو بیرون رود جوهر جان ز تن</p></div>
<div class="m2"><p>گریزی ز هم‌خوابه خویشتن</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>چراغی که بادی درو دردمی</p></div>
<div class="m2"><p>چه بر طاق ایوان چه زیر زمی</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>اگر بر سپهری وگر بر مغاک</p></div>
<div class="m2"><p>چو خاکی شوی عاقبت باز خاک</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>بسا ماهیا کو شود خورد مور</p></div>
<div class="m2"><p>چو در خاک شور افتد از آب شور</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>چنینست رسم این گذرگاه را</p></div>
<div class="m2"><p>که دارد به آمد شد این راه را</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>یکی را درارد به هنگامه تیز</p></div>
<div class="m2"><p>یکی را ز هنگامه گوید که خیز</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>مکن زیر این لاجوردی بساط</p></div>
<div class="m2"><p>بدین قلعهٔ کهر باگون نشاط</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>که رویت کند کهرباوار زرد</p></div>
<div class="m2"><p>کبودت کند جامه چون لاجورد</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>گوزنی که در شهر شیران بود</p></div>
<div class="m2"><p>به مرگ خودش خانه ویران بود</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>چو مرغ از پی کوچ برکش جناح</p></div>
<div class="m2"><p>مشو مست راح اندرین مستراح</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>بزن برق‌وار آتشی در جهان</p></div>
<div class="m2"><p>جهان را ز خود واره و وارهان</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>سمندر چو پروانه آتش روست</p></div>
<div class="m2"><p>ولیک این کهن لنگ و آن خوشروست</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>اگر شاه ملکست و گر ملک شاه</p></div>
<div class="m2"><p>همه راه رنجست و با رنج راه</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>که داند که این خاک دیرینه‌وار</p></div>
<div class="m2"><p>بهر غاری اندر چه دارد ز غور</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>کهن کیسه شد خاک پنهان شکنج</p></div>
<div class="m2"><p>که هرگز برون نارد آواز گنج</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>زر از کیسهٔ نو برارد خروش</p></div>
<div class="m2"><p>سبوی نو از تری آید به جوش</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>که داند که این زخمهٔ دام و دد</p></div>
<div class="m2"><p>چه تاریخها دارد از نیک و بد</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>چه نیرنگ با بخردان ساختست</p></div>
<div class="m2"><p>چه گردنکشان را سر انداختست</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>فلک نیست یکسان هم آغوش تو</p></div>
<div class="m2"><p>طرازش دورنگست بر دوش تو</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>گهت چون فرشته بلندی دهد</p></div>
<div class="m2"><p>گهت با ددان دستبندی دهد</p></div></div>
<div class="b" id="bn209"><div class="m1"><p>شبانگه بنانیت نارد به یاد</p></div>
<div class="m2"><p>کلیچه به گردون دهد بامداد</p></div></div>
<div class="b" id="bn210"><div class="m1"><p>چه باید درین هفت چشمه خراس</p></div>
<div class="m2"><p>ز بهر جوی چند بردن سپاس</p></div></div>
<div class="b" id="bn211"><div class="m1"><p>چو خضر از چنین روزیی روزه گیر</p></div>
<div class="m2"><p>چو هست آب حیوان نه خرما نه شیر</p></div></div>
<div class="b" id="bn212"><div class="m1"><p>ازین دیو مردم که دام و ددند</p></div>
<div class="m2"><p>نهان شو که هم‌صحبتان بدند</p></div></div>
<div class="b" id="bn213"><div class="m1"><p>پی گور کز دشتبانان گمست</p></div>
<div class="m2"><p>ز نامردمیهای این مردمست</p></div></div>
<div class="b" id="bn214"><div class="m1"><p>گوزن گرازنده در مرغزار</p></div>
<div class="m2"><p>ز مردم گریزد سوی کوه و غار</p></div></div>
<div class="b" id="bn215"><div class="m1"><p>همان شیر کو جای در بیشه کرد</p></div>
<div class="m2"><p>ز بد عهدی مردم اندیشه کرد</p></div></div>
<div class="b" id="bn216"><div class="m1"><p>مگر گوهر مردمی گشت خرد</p></div>
<div class="m2"><p>که در مردمان مردمیها بمرد</p></div></div>
<div class="b" id="bn217"><div class="m1"><p>اگر نقش مردن بخوانی شگرف</p></div>
<div class="m2"><p>بگوید که مردم چنینست حرف</p></div></div>
<div class="b" id="bn218"><div class="m1"><p>به چشم اندرون مردمک را کلاه</p></div>
<div class="m2"><p>هم از مردم مردمی شد سیاه</p></div></div>
<div class="b" id="bn219"><div class="m1"><p>نظامی به خاموشکاری بسیچ</p></div>
<div class="m2"><p>به گفتار ناگفتنی در مپیچ</p></div></div>
<div class="b" id="bn220"><div class="m1"><p>چو هم رستهٔ خفتگانی خموش</p></div>
<div class="m2"><p>فرو خسب یا پنبه درنه به گوش</p></div></div>
<div class="b" id="bn221"><div class="m1"><p>بیاموز ازین مهره لاجورد</p></div>
<div class="m2"><p>که با سرخ سرخست و با زرد زرد</p></div></div>
<div class="b" id="bn222"><div class="m1"><p>شبانگه که صد رنگ بیند بکار</p></div>
<div class="m2"><p>براید به صد دست چون نوبهار</p></div></div>
<div class="b" id="bn223"><div class="m1"><p>سحرگه که یک چشمه یابد کلید</p></div>
<div class="m2"><p>به آیین یک چشمه آید پدید</p></div></div>