---
title: >-
    بخش ۵۸ - نشاط کردن اسکندر با کنیزک چینی
---
# بخش ۵۸ - نشاط کردن اسکندر با کنیزک چینی

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی آن آب آتش خیال</p></div>
<div class="m2"><p>درافکن بدان کهرباگون سفال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوارنده آبی کزین تیره خاک</p></div>
<div class="m2"><p>بدو شاید اندوه را شست پاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبی روشن از روز و رخشنده‌تر</p></div>
<div class="m2"><p>مهی ز آفتابی درفشنده‌تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سرسبزی گنبد تابناک</p></div>
<div class="m2"><p>زمرد شده لوح طفلان خاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ستاره بران لوح زیبا ز سیم</p></div>
<div class="m2"><p>نوشته بسی حرف از امید و بیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دبیری که آن حرفها را شناخت</p></div>
<div class="m2"><p>درین غار بی غور منزل نساخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به شغل جهان رنج بردن چه سود</p></div>
<div class="m2"><p>که روزی به کوشش نشاید فزود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان غم نیرزد به شادی گرای</p></div>
<div class="m2"><p>نه کز بهر غم کرده‌اند این سرای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان از پی شادی و دلخوشیست</p></div>
<div class="m2"><p>نه از بهر بیداد و محنت کشیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در این جای سختی نگیریم سخت</p></div>
<div class="m2"><p>از این چاه بی بن برآریم رخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می شادی آور به شادی نهیم</p></div>
<div class="m2"><p>ز شادی نهاده به شادی دهیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو دی رفت و فردا نیامد پدید</p></div>
<div class="m2"><p>به شادی یک امشب بباید برید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنان به که امشب تماشا کنیم</p></div>
<div class="m2"><p>چو فردا رسد کار فردا کنیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غم نامده خورد نتوان به زور</p></div>
<div class="m2"><p>به بزم اندرون رفت نتوان به گور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مکن جز طرب در می اندیشه‌ای</p></div>
<div class="m2"><p>پدید است بازار هر چه پیشه‌ای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه باید به خود بر ستم داشتن</p></div>
<div class="m2"><p>همه ساله خود را به غم داشتن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه پیچیم در عالم پیچ پیچ</p></div>
<div class="m2"><p>که هیچست ازو سود و سرمایه هیچ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گریزیم از این کوچگاه رحیل</p></div>
<div class="m2"><p>از آن پیش کافتیم درپای پیل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خوریم آنچه از ما به گوری خورند</p></div>
<div class="m2"><p>بریم آنچه از ما به غارت برند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر برد خواهی چنان مایه بر</p></div>
<div class="m2"><p>که بردند پیشینگان دگر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر ترسی از رهزن و باج خواه</p></div>
<div class="m2"><p>که غارت کند آنچه بیند به راه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به درویش ده آنچه داری نخست</p></div>
<div class="m2"><p>که بنگاه درویش را کس نجست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نبینی که ده یک دهان خراج</p></div>
<div class="m2"><p>به دهلیز درویش دزدند باج</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چه زیرک شد آن مرد بنیاد سنج</p></div>
<div class="m2"><p>که ویرانه را ساخت باروی گنج</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو تاریخ یک‌روزه دارد جهان</p></div>
<div class="m2"><p>چرا گنج صد ساله داری نهان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بیا تا نشینیم و شادی کنیم</p></div>
<div class="m2"><p>شبی در جهان کیقبادی کنیم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یک امشب ز دولت ستانیم داد</p></div>
<div class="m2"><p>زدی و ز فردا نیاریم یاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بترسیم از آنها کزو سود نیست</p></div>
<div class="m2"><p>کزین پیشه اندیشه خوشنود نیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدانچ آدمی را بود دسترس</p></div>
<div class="m2"><p>بکوشیم تا خوش برآید نفس</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به چاره دل خویشتن خوش کنیم</p></div>
<div class="m2"><p>نه چندان که تن نعل آتش کنیم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دمی را که سرمایه از زندگیست</p></div>
<div class="m2"><p>به تلخی سپردن نه فرخندگیست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنان بر زن این دم که دادش دهی</p></div>
<div class="m2"><p>که بادش دهی گر به بادش دهی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فدا کن درم خوش‌دلی را بسیچ</p></div>
<div class="m2"><p>که ارزان بود دل خریدن به هیچ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز بهر درم تند و بدخو مباش</p></div>
<div class="m2"><p>تو باید که باشی درم گو مباش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مشو در حساب جهان سخت گیر</p></div>
<div class="m2"><p>همه سخت‌گیری بود سخت میر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به آسان گذاری دمی می شمار</p></div>
<div class="m2"><p>که آسان زید مرد آسان گذار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شبی فرخ و ساعتی ارجمند</p></div>
<div class="m2"><p>بود شادمانی درو دلپسند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گزارش چنین می‌کند جوهری</p></div>
<div class="m2"><p>سخن را به یاقوت اسکندری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که اسکندر آن شب به مهر تمام</p></div>
<div class="m2"><p>به یاد لب دوست پر کرد جام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به نوشین لب آن جام را نوش کرد</p></div>
<div class="m2"><p>ز لب جام را حلقه در گوش کرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نشسته به کردار سرو جوان</p></div>
<div class="m2"><p>که گه لاله ریزد گهی ارغوان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز عنبر خطی بر گل انگیخته</p></div>
<div class="m2"><p>بر گل جهان آب گل ریخته</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هم از فتح دشمن دلش شاد بود</p></div>
<div class="m2"><p>هم از دوستیش خانه آباد بود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>طلب کرد یار دلارام را</p></div>
<div class="m2"><p>پری پیکر نازی اندام را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز نامحرمان کرد خرگه تهی</p></div>
<div class="m2"><p>سماع و سماع آور خرگهی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بتی فرق و گیسو برآراسته</p></div>
<div class="m2"><p>مرادی به صد آرزو خواسته</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>لب از ناردانه دلاویزتر</p></div>
<div class="m2"><p>زبان از طبرزد شکر ریزتر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دهانی و چشمی به اندازه تنگ</p></div>
<div class="m2"><p>یکی راه دل زد یکی راه چنگ</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سر آغوش و گیسوی عنبر فشان</p></div>
<div class="m2"><p>رسن وار در عطف دامن کشان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>طرازندهٔ مجلس و بزمگاه</p></div>
<div class="m2"><p>نوازندهٔ چنگ در چنگ شاه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به فرمان شه چنگ را ساز کرد</p></div>
<div class="m2"><p>در درج گوهر ز لب باز کرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>که از شادی امشب جهان را نویست</p></div>
<div class="m2"><p>همه شادی از دولت خسرویست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به هنگام گل خوش بود روزگار</p></div>
<div class="m2"><p>بخندد جهان چون بخندد بهار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو خورشید روشن برآید به اوج</p></div>
<div class="m2"><p>ز روشن جهان برزند نور موج</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>صبا چون درآید به دیبا گری</p></div>
<div class="m2"><p>زمین رومی آرد هوا ششتری</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گل سرخ چون کله بندد به باغ</p></div>
<div class="m2"><p>فروزد ز هر غنچه‌ای صد چراغ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سکندر چو پیروزی آرد به چنگ</p></div>
<div class="m2"><p>نه زیبا بود آینه زیر زنگ</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو کیخسرو ار می‌شود جام گیر</p></div>
<div class="m2"><p>چرا جام خالی بود بر سریر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ملک گر ز جمشید بالاترست</p></div>
<div class="m2"><p>رخ من ز خورشید والاتر است</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>شه ار شد فریدون زرینه کفش</p></div>
<div class="m2"><p>به فتحش منم کاویانی درفش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>شه ار کیقباد بلند افسرست</p></div>
<div class="m2"><p>مرا افسر از مشک و از عنبرست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>شه ار هست کاوس فیروزه تاج</p></div>
<div class="m2"><p>ز من بایدش خواستن تخت عاج</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>شه ار چون سلیمان شود دیو بند</p></div>
<div class="m2"><p>مرا در جهان هست دیوانه چند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>شه ار زانکه عالم گرفت ای شگفت</p></div>
<div class="m2"><p>من آنرا گرفتم که عالم گرفت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>اگر چه کمند جهانگیر شاه</p></div>
<div class="m2"><p>فتاد است بر گردن مهر و ماه</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کمندی من از زلف برسازمش</p></div>
<div class="m2"><p>نترسم به گردن دراندازمش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گر او را کمندی بود ماه گیر</p></div>
<div class="m2"><p>مرا هم کمندی بود شاه گیر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>گر او ناوک اندازد از زوردست</p></div>
<div class="m2"><p>مرا غمزهٔ ناوک انداز هست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گر او حربه دارد به خون ریختن</p></div>
<div class="m2"><p>من از چهره خون دانم انگیختن</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گر او قصد شمشیر بازی کند</p></div>
<div class="m2"><p>زبانم به شمشیر بازی کند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گر او لختی از زر برآرد به دوش</p></div>
<div class="m2"><p>دو لختی است زلفین من گرد گوش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>گر او را یکی طوق بر مرکبست</p></div>
<div class="m2"><p>مرا بین که ده طوق بر غبغبست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>گر او حقه‌ها دارد از لعل و در</p></div>
<div class="m2"><p>مرا حقه‌ای هست از لعل پر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گر ایدون که یاقوت او کانیست</p></div>
<div class="m2"><p>مرا لب چو یاقوت رمانیست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>گر او چرخ را هست انجم شناس</p></div>
<div class="m2"><p>مرا انجم چرخ دارند پاس</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>گر او را علم هست بالای سر</p></div>
<div class="m2"><p>مرا صد علم هست بیرون در</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>گر او شاه عالم شد از سروری</p></div>
<div class="m2"><p>منم شاه خوبان به جان پروری</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو برقع براندازم از روی خویش</p></div>
<div class="m2"><p>ندارم جهان را به یک موی خویش</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چو بر مه کشم گیسوی عنبرین</p></div>
<div class="m2"><p>به گیسو کشم ماه را بر زمین</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو تنگ شکر در عقیق آورم</p></div>
<div class="m2"><p>ز پسته شراب رحیق آورم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>رحیقم به رقص آورد آب را</p></div>
<div class="m2"><p>عقیقم مفرح دهد خواب را</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ز مه طوق خواهی ببین غبغبم</p></div>
<div class="m2"><p>ز قند ار نمک باید اینک لبم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بدین قند کو با شکر خندیست</p></div>
<div class="m2"><p>در بوسه بین چون سمرقندیست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>اگر کیمیا سنگ را زر کند</p></div>
<div class="m2"><p>نسیم من از خاک عنبر کند</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>سهیل یمن تاب را با ادیم</p></div>
<div class="m2"><p>همان شد که بوی مرا با نسیم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>به چشمی دل خسته بریان کنم</p></div>
<div class="m2"><p>به چشمی دگر غارت جان کنم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>از این سو کنم صید و بنوازمش</p></div>
<div class="m2"><p>وز آنسو به دریا دراندازمش</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>فریبم به درمان و سوزم به درد</p></div>
<div class="m2"><p>منم کاین کنم جز من این کس نکرد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>اگر راهبم بیند از راه دور</p></div>
<div class="m2"><p>برد سجده چون هیربد پیش نور</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>وگر زاهدی باشد از خاره سنگ</p></div>
<div class="m2"><p>درآرم به رقصش به یک بانگ چنگ</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>کنم سیم‌کاری که سیمین تنم</p></div>
<div class="m2"><p>ولی قفل گنجینه را نشکنم</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>در باغ ما را که شد ناپدید</p></div>
<div class="m2"><p>بجز باغبان کس نداند کلید</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>رطبهای‌تر گرچه دارم بسی</p></div>
<div class="m2"><p>بجز خار خشگم نبیند کسی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>گلابم ولی دردسر می‌دهم</p></div>
<div class="m2"><p>نمک خواه خود را جگر می‌دهم</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>مگر دید شب ترکی روی من</p></div>
<div class="m2"><p>که چون خال من گشت هند ویمن</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>مگر ماه نو کان هلالی کند</p></div>
<div class="m2"><p>به امید من خانه خالی کند</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>چو زلفم درآید به بازیگری</p></div>
<div class="m2"><p>به دام آورد پای کبک دری</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>بنا گوشم ار برگشاید نقاب</p></div>
<div class="m2"><p>دهان گل سرخ گردد پر آب</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>زنخ را چو برسازم از زلف بند</p></div>
<div class="m2"><p>به آب معلق درارم کمند</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>چو پیدا کنم لطف اندام را</p></div>
<div class="m2"><p>سرین بشکنم مغز بادام را</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چو ساعد گشایم ز بازوی نرم</p></div>
<div class="m2"><p>سمن را ورق درنوردم ز شرم</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>شکر چاشنی گیر نوش منست</p></div>
<div class="m2"><p>گهر حلقه در گوش گوش منست</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>دهانم گرو بست با مشتری</p></div>
<div class="m2"><p>گرو برد کو دارد انگشتری</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>جنابی که با گل خورم نوش باد</p></div>
<div class="m2"><p>مرا یاد و گل را فراموش باد</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>یک افسون چشمم به بابل رسید</p></div>
<div class="m2"><p>کزو آمد آن جادوئیها پدید</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>ز جعدم یکی موی بر چین گذشت</p></div>
<div class="m2"><p>کزو مشک شد ناف آهو به دشت</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چو حلقه کنم زلف بر طرف گوش</p></div>
<div class="m2"><p>بیا تا دل رفته بینی ز هوش</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>کرشمه چو در چشم مست آورم</p></div>
<div class="m2"><p>صد از دست رفته به دست آورم</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>دلی را که سر سوی راه افکنم</p></div>
<div class="m2"><p>نمایم زنخ تا به چاه افکنم</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ز موئی به عاشق دهم طوق و تاج</p></div>
<div class="m2"><p>به بوئی ز خلج ستانم خراج</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>به سلطانی چین نهم مهر موم</p></div>
<div class="m2"><p>زنم پنج نوبت به تاراج روم</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>جگر گوشه چینیانم به خال</p></div>
<div class="m2"><p>چراغ دل رومیانم به فال</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>طبرزد دهم چون شوم خواب خیز</p></div>
<div class="m2"><p>طبر خون زنم چون کنم غمزه تیز</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>لبم لعل را کارسازی کند</p></div>
<div class="m2"><p>خیالم به خورشید بازی کند</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>مغ دیر سیمین صنم خواندم</p></div>
<div class="m2"><p>صنم خانهٔ باغ ارم داندم</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چو شد نار پستانم انگیخته</p></div>
<div class="m2"><p>ز بستان دل نار شد ریخته</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>ز نارم که نارنج نوروزیست</p></div>
<div class="m2"><p>که را بخت گوئی که را روزیست</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>مبارک درختم که بر دوستم</p></div>
<div class="m2"><p>برآور گلم گر چه در پوستم</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>من و آب سرخ و سر سبز شاه</p></div>
<div class="m2"><p>جهان گو فرو شو به آب سیاه</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>برآنم که دستان به کار آورم</p></div>
<div class="m2"><p>چو چنگ خودش در کنار آورم</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>گهی بوسه بر چشم مستش دهم</p></div>
<div class="m2"><p>گهی زلف خود را به دستش دهم</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>به شرطی کنم جان خود جای او</p></div>
<div class="m2"><p>که هرگز نتابم سر از پای او</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>چنان خسبم از مهر آن آفتاب</p></div>
<div class="m2"><p>که سر در قیامت برآرم ز خواب</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>گر آبیست گو زندگانی دهد</p></div>
<div class="m2"><p>وگر سایه‌ای گو جوانی دهد</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>کند وصل من زندگانی دراز</p></div>
<div class="m2"><p>جوانی دهم چون درآیم به ناز</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>سکندر به حیوان خطا می‌رود</p></div>
<div class="m2"><p>من این‌جا سکندر کجا می‌رود</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>اگر راه ظلمات می‌بایدش</p></div>
<div class="m2"><p>سرزلف من راه بنمایدش</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>وگر زانکه جوید ز یاقوت رنگ</p></div>
<div class="m2"><p>همان آورد آب حیوان به چنگ</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>لب من که یاقوت رخشان در اوست</p></div>
<div class="m2"><p>بسی چشمه چون آب حیوان در اوست</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>جهان خسروا چند گردن کشی</p></div>
<div class="m2"><p>بر این آب حیوان مشو آتشی</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>پریرویم و چون پری در پرند</p></div>
<div class="m2"><p>چو دل بسته‌ای در پری در مبند</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>مرا با تو در باد و بستن مباد</p></div>
<div class="m2"><p>شکن باد لیکن شکستن مباد</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>بس این سنگ سخت از دل انگیختن</p></div>
<div class="m2"><p>به نازک دلان در نیامیختن</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>مکن ترکی ای میل من سوی تو</p></div>
<div class="m2"><p>که ترک توام بلکه هندوی تو</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>بدین آسمانی زمین توام</p></div>
<div class="m2"><p>ز چینم ولی درد چین توام</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>گل من گلی سایه پروردنیست</p></div>
<div class="m2"><p>که سایه به خورشید درخورد نیست</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>چو من میوه در سایهٔ خانه بس</p></div>
<div class="m2"><p>که ناخوش بود میوهٔ خانه رس</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>مرا خود تو ریحان خوشبوی گیر</p></div>
<div class="m2"><p>ز ریحان بود خانه را ناگزیر</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>رها کن به نخجیر این کبک باز</p></div>
<div class="m2"><p>بترس از عقابان نخجیر ساز</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>رطب کو رسیده بود بر درخت</p></div>
<div class="m2"><p>به سستی رسد گر نگیریش سخت</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>نیابی ز من به جگر خواره‌ای</p></div>
<div class="m2"><p>جگر خواره‌ای نه شکر باره‌ای</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>چه دلها که خون شد ز خون خوردنم</p></div>
<div class="m2"><p>چه خونها که ماندست در گردنم</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>به داور شدم با شکر بارها</p></div>
<div class="m2"><p>مرا بیش از او بود بازارها</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>به آواز و چهره کش و دلکشم</p></div>
<div class="m2"><p>همان خوش همین خوش خوش اندر خوشم</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>چو ساقی شوم می‌نباشد حرام</p></div>
<div class="m2"><p>چو مطرب شوم نوش ریزد ز جام</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>چو بر رود دستان کنم دست خوش</p></div>
<div class="m2"><p>کنم مست وانگه شوم مست کش</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>ز دور این چنین دلبریها کنم</p></div>
<div class="m2"><p>در آغوش جان پروریها کنم</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>برابر دهم دیده را دل‌خوشی</p></div>
<div class="m2"><p>چو در برکشندم کنم دل کشی</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>من و نالهٔ چنگ و نوشینه می</p></div>
<div class="m2"><p>ز من عاشقان کی شکیبندکی</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>چو تو شهریاری بود یار من</p></div>
<div class="m2"><p>چه باشد به جز خرمی کار من</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>چو من نیست اندر جهان کس به کام</p></div>
<div class="m2"><p>ازان نیست اندر جهانم به نام</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>چو بر زد دلاویز چنگی به چنگ</p></div>
<div class="m2"><p>چنین قولی از قند عناب رنگ</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>درآمد شه از مهر آن نوشناز</p></div>
<div class="m2"><p>بدان جره کبک چون جره باز</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>تذرو بهاری درآمد به غنج</p></div>
<div class="m2"><p>برون آمد از مهد زرین ترنج</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>سرا بود خالی و معشوقه مست</p></div>
<div class="m2"><p>عنان رفت یک باره دل را ز دست</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>شبی خلوت و ماهروئی چنان</p></div>
<div class="m2"><p>ازو چون توان درکشیدن عنان</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>گوزن جوان را بیفکند شیر</p></div>
<div class="m2"><p>به تاراجگاهش درآمد دلیر</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>به صید حواصل درآمد عقاب</p></div>
<div class="m2"><p>به مهمانی ماه رفت آفتاب</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>زمانی چو شکر لبش می‌گزید</p></div>
<div class="m2"><p>زمانی چو نیشکرش می‌مزید</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>به بر در گرفت آن سمن سینه را</p></div>
<div class="m2"><p>ز در مهر برداشت گنجینه را</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>نخورده میی دید روشن گوار</p></div>
<div class="m2"><p>یکی باغ در بسته پر سیب و نار</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>عقیقی نیازرده بر مهر خویش</p></div>
<div class="m2"><p>نگینی به الماس ناگشته ریش</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>نچیده گلی خار برچیده‌ای</p></div>
<div class="m2"><p>بجز باغبان مرد نادیده‌ای</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>از آن گرمی و آتش افزون شدن</p></div>
<div class="m2"><p>ز جوشنده خون خواست بیرون شدن</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>ز شیرین زبان شکر انگیختند</p></div>
<div class="m2"><p>چو شیر و شکر درهم آویختند</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>به هم درخزیده دو سرو بلند</p></div>
<div class="m2"><p>به بادام و روغن درافتاده قند</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>دو پی هر دو چون لاف الف خم زده</p></div>
<div class="m2"><p>دو حرف از یکی جنس درهم زده</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>چو لولوی ناسفته را لعل سفت</p></div>
<div class="m2"><p>هم آسود لولو و هم لعل خفت</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>سکندر بدان چشمه زندگی</p></div>
<div class="m2"><p>بسی کرد شادی و فرخندگی</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>چنین چند شب دل به شادی سپرد</p></div>
<div class="m2"><p>وزان مرحله رخت بیرون نبرد</p></div></div>