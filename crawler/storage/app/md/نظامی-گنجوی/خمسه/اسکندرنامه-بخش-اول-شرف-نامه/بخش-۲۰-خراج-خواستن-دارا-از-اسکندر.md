---
title: >-
    بخش ۲۰ - خراج خواستن دارا از اسکندر
---
# بخش ۲۰ - خراج خواستن دارا از اسکندر

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی آن جام آیینه فام</p></div>
<div class="m2"><p>به من ده که بر دست به جای جام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو زان جام کیخسرو آیین شوم</p></div>
<div class="m2"><p>بدان جام روشن جهان بین شوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا تا ز بیداد شوئیم دست</p></div>
<div class="m2"><p>که بی داد نتوان ز بیداد رست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه بندیم دل در جهان سال و ماه</p></div>
<div class="m2"><p>که هم دیو خانست و هم غول راه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان وام خویش از تو یکسر برد</p></div>
<div class="m2"><p>به جرعه فرستد به ساغر برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو باران که یک یک مهیا شود</p></div>
<div class="m2"><p>شود سیل و آنگه به دریا شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا تا خوریم آنچه داریم شاد</p></div>
<div class="m2"><p>درم بر درم چند باید نهاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نهنگی به ما برگذر کرده گیر</p></div>
<div class="m2"><p>همه گنج ناخورده را خورده گیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از آن گنج کاورد قارون به دست</p></div>
<div class="m2"><p>سرانجام در خاک بین چون نشست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وزان خشت زرین شداد عاد</p></div>
<div class="m2"><p>چه آمد به جز مردن نامراد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درین باغ رنگین درختی نرست</p></div>
<div class="m2"><p>که ماند از قفای تبرزن درست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گزارش کن زیور تاج و تخت</p></div>
<div class="m2"><p>چنین گفت کان شاه فیروز بخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی روز فارغ دل و شاد بهر</p></div>
<div class="m2"><p>بر آسوده بود از هوسهای دهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می‌ناب در جام شاهنشهی</p></div>
<div class="m2"><p>گهی پر همی کرد و گاهی تهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حکیمان هشیار دل پیش او</p></div>
<div class="m2"><p>خردمند مونس خرد خویش او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به هر نسبتی کامد از بانگ چنگ</p></div>
<div class="m2"><p>سخن شد بسی در نمطهای تنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به هر جرعه می‌که شه می‌فشاند</p></div>
<div class="m2"><p>مهندس درختی در او می‌نشاند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درخشان شده می‌چو روشن درخش</p></div>
<div class="m2"><p>قدح شکر افشان و می‌نوش بخش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دماغ نیوشنده را سرگران</p></div>
<div class="m2"><p>ز نوش می‌و رود رامشگران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سرشک قدح نالهٔ ارغنون</p></div>
<div class="m2"><p>روان کرده از رودها رود خون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زهی زخم کز زخمهٔ چون شکر</p></div>
<div class="m2"><p>شود رود خشکی بدو رود تر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در آن بزم آراسته چون بهشت</p></div>
<div class="m2"><p>گل افشان‌تر از ماه اردیبهشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سکندر جهانجوی فرخ سریر</p></div>
<div class="m2"><p>نشسته چو بر چرخ بدر منیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز دارا درآمد فرستاده‌ای</p></div>
<div class="m2"><p>سخنگوی و روشن‌دل آزاده‌ای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو خسرو پرستان پرستش نمود</p></div>
<div class="m2"><p>هم او را و هم شاه خود را ستود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو کرد آفرین بر جهان پهلوان</p></div>
<div class="m2"><p>شنیده سخن کرد با او روان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز دارا درود آوریدش نخست</p></div>
<div class="m2"><p>نداده خراج کهن باز جست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که چون بود کز گوهر و طوق و تاج</p></div>
<div class="m2"><p>ز درگاه ما واگرفتی خراج</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زبونی چه دیدی تو در کار ما</p></div>
<div class="m2"><p>که بردی سر از خط پرگار ما</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همان رسم دیرینه را کاربند</p></div>
<div class="m2"><p>مکن سرکشی تا نیابی گزند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سکندر ز گرمی چنان برفروخت</p></div>
<div class="m2"><p>که از آتش دل زبانش بسوخت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کمان گوشهٔ ابرویش خم گرفت</p></div>
<div class="m2"><p>ز تندیش گوینده را دم گرفت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چنان دید در قاصد راه سنج</p></div>
<div class="m2"><p>که از جوش دل مغزش آمد به رنج</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زبان چون ز گرمی بر آشفته شد</p></div>
<div class="m2"><p>سخن‌های ناگفتنی گفته شد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>فرو گفت لختی سخنهای سخت</p></div>
<div class="m2"><p>چو گوید خداوند شمشیر و تخت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که را در خرد رای باشد بلند</p></div>
<div class="m2"><p>نگوید سخن‌های ناسودمند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زبان گر به گرمی صبوری کند</p></div>
<div class="m2"><p>ز دوری کن خویش دوری کند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سخن گر چه با او زهازه بود</p></div>
<div class="m2"><p>نگفتن هم از گفتنش به بود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو خوش گفت فرزانهٔ پیش بین</p></div>
<div class="m2"><p>زبان گوشتین است و تیغ آهنین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نباشد به خود بر کسی مرزبان</p></div>
<div class="m2"><p>که گوید هر آنچ آیدش بر زبان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گزارنده پیر کیانی سرشت</p></div>
<div class="m2"><p>گزارش چنین کرد از آن سرنبشت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که وقتی که از گوهر و تیغ و تاج</p></div>
<div class="m2"><p>ز یونان شدی پیش دارا خراج</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در آن گوهرین گنج بن ناپدید</p></div>
<div class="m2"><p>بدی خایهٔ زر خدای آفرید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>منقش یکی خسروانی بساط</p></div>
<div class="m2"><p>که بیننده را تازه کردی نشاط</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چوقاصد زبان تیغ پولاد کرد</p></div>
<div class="m2"><p>خراج کهن گشته را یاد کرد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>برو بانگ زد شهریار دلیر</p></div>
<div class="m2"><p>که نتوان ستد غارت از تندشیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زمانه دگرگونه آیین نهاد</p></div>
<div class="m2"><p>شد آن مرغ کو خایه زرین نهاد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سپهر آن بساط کهن در نوشت</p></div>
<div class="m2"><p>بساطی دگر ملک را تازه گشت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>همه ساله گوهر نخیزد ز سنگ</p></div>
<div class="m2"><p>گهی صلح سازد جهان گاه جنگ</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به گردن کشی بر می‌آور نفس</p></div>
<div class="m2"><p>به شمشیر با من سخن گوی بس</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو را آن کفایت که شمشیر من</p></div>
<div class="m2"><p>نیارد سر تخت تو زیر من</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو من با رکابی که برداشتم</p></div>
<div class="m2"><p>عنان جهان بر تو بگذاشتم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تو با آنکه داری چنان توشه‌ای</p></div>
<div class="m2"><p>رها کن مرا در چنین گوشه‌ای</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بر آنم میاور که عزم آورم</p></div>
<div class="m2"><p>به هم پنجه‌ای با تو رزم آورم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به یک سو نهم مهر و آزرم را</p></div>
<div class="m2"><p>به جوش آورم کینهٔ گرم را</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مگر شه نداند که در روز جنگ</p></div>
<div class="m2"><p>چه سرها بریدم در اقصای زنگ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به یک تاختن تا کجا تاختم</p></div>
<div class="m2"><p>چه گردنکشان را سرانداختم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کسی کارمغانی دهد طوق و تاج</p></div>
<div class="m2"><p>چو زنهاریان چون فرستد خراج</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز من مصر باید نه زر خواستن</p></div>
<div class="m2"><p>سخن چون زر مصری آراستن</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ببین پایگاه مرا تا کجاست</p></div>
<div class="m2"><p>بدان پایه باید ز من مایه خواست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مینگیز فتنه میفروز کین</p></div>
<div class="m2"><p>خرابی میاور در ایران زمین</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تو را ملکی آسوده بی داغ و رنج</p></div>
<div class="m2"><p>مکن ناسپاسی در آن مال وگنج</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مشوران به خودکامی ایام را</p></div>
<div class="m2"><p>قلم درکش اندیشهٔ خام را</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز من آنچه بر نایدت در مخواه</p></div>
<div class="m2"><p>چنان باش با من که با شاه شاه</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>فرستاده کاین داستان گوش کرد</p></div>
<div class="m2"><p>سخنهای خود را فراموش کرد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سوی شاه شد داغ بر دل کشان</p></div>
<div class="m2"><p>شتابنده چون برق آتش فشان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>فرو گفت پیغامهای درشت</p></div>
<div class="m2"><p>کزو سروبن را دو تا گشت پشت</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو دارا جواب سکندر شنید</p></div>
<div class="m2"><p>یکی دور باش از جگر بر کشید</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>که بی سکه‌ای را چه یارا بود</p></div>
<div class="m2"><p>که هم سکهٔ نام دارا بود</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به تندی بسی داستان یاد کرد</p></div>
<div class="m2"><p>گزان شد نیوشنده را روی زرد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بخندید و گفت اندر آن زهر خند</p></div>
<div class="m2"><p>که افسوس بر کار چرخ بلند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>فلک بین چه ظلم آشکارا کند</p></div>
<div class="m2"><p>که اسکندر آهنگ دارا کند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سکندر نه گر خود بود کوه قاف</p></div>
<div class="m2"><p>که باشد که من باشمش هم مصاف</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چنان پشه‌ای را به جنگ عقاب</p></div>
<div class="m2"><p>که از قطره‌دان پیش دریای آب</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>سبک قاصدی را به درگاه او</p></div>
<div class="m2"><p>فرستاد و شد چشم بر راه او</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>یکی گوی و چوگان به قاصد سپرد</p></div>
<div class="m2"><p>قفیزی پر از کنجد ناشمرد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>در آموختنش راز آن پیشکش</p></div>
<div class="m2"><p>بدان تعبیه شد دل شاه خوش</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>سوی روم شد قاصد تیزگام</p></div>
<div class="m2"><p>ز دارا پذیرفته با خود پیام</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>زره چون در آمد بر شاه روم</p></div>
<div class="m2"><p>فروزنده شد همچو آتش ز موم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>سرافکنده در پایه بندگی</p></div>
<div class="m2"><p>نمودش نشان پرستندگی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>نخستین گره کز سخن باز کرد</p></div>
<div class="m2"><p>سخن را به چربی سرآغاز کرد</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>که فرمان دهان حاکم جان شدند</p></div>
<div class="m2"><p>فرستادگان بنده فرمان شدند</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چه فرمایدم شاه فیروز رای</p></div>
<div class="m2"><p>که فرمان فرمانده آرم به جای؟</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>سکندر بدانست کان عذر خواه</p></div>
<div class="m2"><p>پیامی درشت آرد از نزد شاه</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>به بی غاره گفتا بیاور پیام</p></div>
<div class="m2"><p>پیام‌آور از بند بگشاد کام</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>متاعی که در سله خویش داشت</p></div>
<div class="m2"><p>بیاورد و یک یک فرا پیش داشت</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چو آورده پیش سکندر نهاد</p></div>
<div class="m2"><p>به پیغام دارا زبان برگشاد</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ز چوگان و گوی اندر آمد نخست</p></div>
<div class="m2"><p>که طفلی تو بازی به این کن درست</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>وگر آرزوی نبرد آیدت</p></div>
<div class="m2"><p>ز بیهودگی دل به درد آیدت</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>همان کنجد ناشمرده فشاند</p></div>
<div class="m2"><p>کزین بیش خواهم سپه بر تو راند</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>سکندر جهان داور هوشمند</p></div>
<div class="m2"><p>درین فالها دید فتحی بلند</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>مثل زد که هر چه آن گریزد ز پیش</p></div>
<div class="m2"><p>به چوگان کشیدش توان پیش خویش</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>مگر شاه از آن داد چوگان به من</p></div>
<div class="m2"><p>که تا زو کشم ملک بر خویشتن</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>همان گوی را مرد هیئت شناس</p></div>
<div class="m2"><p>به شکل زمین می نهد در قیاس</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>چو گوی زمین شاه ما را سپرد</p></div>
<div class="m2"><p>بدین گوی خواهم ازو گوی برد</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>چو زین گونه کرد آن گزارشگری</p></div>
<div class="m2"><p>به کنجد در آمد در داوری</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>فرو ریخت کنجد به صحن سرای</p></div>
<div class="m2"><p>طلب کرد مرغان کنجد ربای</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>به یک لحظه مرغان در او تاختند</p></div>
<div class="m2"><p>زمین را ز کنجد بپرداختند</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>جوابیست گفتا درین رهنمون</p></div>
<div class="m2"><p>چو روغن که از کنجد آید برون</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>اگر لشکر از کنجد انگیخت شاه</p></div>
<div class="m2"><p>مرا مرغ کنجد خور آمد سپاه</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>پس آنگه قفیزی سپندان خرد</p></div>
<div class="m2"><p>به پاداش کنجد به قاصد سپرد</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>که شه گر کشد لشگری زان قیاس</p></div>
<div class="m2"><p>سپاه مرا هم بدینسان شناس</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چو قاصد جوابی چنین دید سخت</p></div>
<div class="m2"><p>به پشت خر خویش بربست رخت</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>به دارا رساند از سکندر جواب</p></div>
<div class="m2"><p>جوابی گلوگیر چون زهر ناب</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>برآشفت از آن طیرگی شاه را</p></div>
<div class="m2"><p>که حجت قوی بود بدخواه را</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>جهاندار دارا دران داوری</p></div>
<div class="m2"><p>طلب کرد از ایرانیان یاوری</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>ز چین و ز خوارزم و غزنین و غور</p></div>
<div class="m2"><p>زمین آهنین شد ز نعل ستور</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>سپاهی بهم کرد چون کوه قاف</p></div>
<div class="m2"><p>همه سنگ فرسای و آهن شکاف</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>چو عارض شمار سپه برگرفت</p></div>
<div class="m2"><p>فرو ماند عقل از شمردن شگفت</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ز جنگی سواران چابک رکاب</p></div>
<div class="m2"><p>به نهصد هزار اندر آمد حساب</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>جهانجوی چون دید کز لشگرش</p></div>
<div class="m2"><p>همی موج دریا زند کشورش</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>سپاهی چو آتش سوی روم راند</p></div>
<div class="m2"><p>کجا او شد آن بوم را بوم خواند</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>به ارمن درآمد چو دریای تند</p></div>
<div class="m2"><p>صبا را شد از گرد او پای کند</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>زمین در زمین تا به اقصای روم</p></div>
<div class="m2"><p>بجوشید دریا بلرزید بوم</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>علف در زمین گشت چون گنج گم</p></div>
<div class="m2"><p>ز نعل ستوران پیگانه سم</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>پی شاه اگر آفتابی کند</p></div>
<div class="m2"><p>به هر جا که تابد خرابی کند</p></div></div>