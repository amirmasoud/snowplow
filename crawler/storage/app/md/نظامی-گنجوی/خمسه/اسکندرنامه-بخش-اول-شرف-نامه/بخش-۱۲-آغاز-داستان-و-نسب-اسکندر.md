---
title: >-
    بخش ۱۲ - آغاز داستان و نسب اسکندر
---
# بخش ۱۲ - آغاز داستان و نسب اسکندر

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی آن آب حیوان گوار</p></div>
<div class="m2"><p>به دولت سرای سکندر سپار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که تا دولتش بوسه بر سر دهد</p></div>
<div class="m2"><p>به میراث خوار سکندر دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گزارنده نامه خسروی</p></div>
<div class="m2"><p>چنین داد نظم سخن را نوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که از جمله تاجداران روم</p></div>
<div class="m2"><p>جوان دولتی بود از آن مرز و بوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهی نامور نام او فیلقوس</p></div>
<div class="m2"><p>پذیرای فرمان او روم و روس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یونان زمین بود مأوای او</p></div>
<div class="m2"><p>به مقدونیه خاص‌تر جای او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نو آیین‌ترین شاه آفاق بود</p></div>
<div class="m2"><p>نوا زادهٔ عیص اسحق بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان دادگر بود کز داد خویش</p></div>
<div class="m2"><p>دم گرگ را بست بر پای میش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گلوی ستم را بدان سان فشرد</p></div>
<div class="m2"><p>که دارا بدان داوری رشک برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سبق جست بر وی به شمشیر و تاج</p></div>
<div class="m2"><p>فرستاد کس تا فرستد خراج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شه روم را بود رایی درست</p></div>
<div class="m2"><p>رضا جست و با او خصومت نجست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کسی را که دولت کند یاوری</p></div>
<div class="m2"><p>که یارد که با او کند داوری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرستاد چندان بدو گنج و مال</p></div>
<div class="m2"><p>کزو دور شد مالش بد سگال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدان خرج خشنود شد شاه روم</p></div>
<div class="m2"><p>ز سوزنده آتش نگهداشت موم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو فتح سکندر در آمد به کار</p></div>
<div class="m2"><p>دگرگونه شد گردش روزگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه دولت نه دنیا به دارا گذاشت</p></div>
<div class="m2"><p>سنان را سر از سنگ خارا گذاشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در این داستان داوریها بسیست</p></div>
<div class="m2"><p>مرا گوش بر گفتهٔ هر کسیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنین آمد از هوشیاران روم</p></div>
<div class="m2"><p>که زاهد زنی بود از آن مرز و بوم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به آبستنی روز بیچاره گشت</p></div>
<div class="m2"><p>ز شهر وز شوی خود آواره گشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو تنگ آمدش وقت بار افکنی</p></div>
<div class="m2"><p>برو سخت شد درد آبستنی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به ویرانهٔ بار بنهاد و مرد</p></div>
<div class="m2"><p>غم طفل می‌خورد و جان می‌سپرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که گوئی که پرورد خواهد تو را</p></div>
<div class="m2"><p>کدامین دده خورد خواهد تو را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وز این بی خبر بد که پروردگار</p></div>
<div class="m2"><p>چگونه ورا پرورد وقت کار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چه گنجینه‌ها زیر بارش کشند</p></div>
<div class="m2"><p>چه اقبالها در کنارش کشند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو زن مرد و آن طفل بی کس بماند</p></div>
<div class="m2"><p>کس بی کسانش به جائی رساند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که ملک جهان را ز فرهنگ ورای</p></div>
<div class="m2"><p>شد از قاف تا قاف کشور گشای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ملک فیلقوس از تماشای دشت</p></div>
<div class="m2"><p>شکار افکنان سوی آن زن گذشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زنی دیده مرده بدان رهگذر</p></div>
<div class="m2"><p>به بالین او طفلی آورده سر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز بی شیری انگشت خود می‌مزید</p></div>
<div class="m2"><p>به مادر بر انگشت خود می‌گزید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بفرمود تا چاکران تاختند</p></div>
<div class="m2"><p>به کار زن مرده پرداختند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز خاک ره آن طفل را برگرفت</p></div>
<div class="m2"><p>فرو ماند از آن روز بازی شگفت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ببرد و بپرورد و بنواختش</p></div>
<div class="m2"><p>پس از خود ولیعهد خود ساختش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دگرگونه دهقان آزر پرست</p></div>
<div class="m2"><p>به دارا کند نسل او باز بست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز تاریخها چون گرفتم قیاس</p></div>
<div class="m2"><p>هم از نامه مرد ایزد شناس</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در آن هر دو گفتار چستی نبود</p></div>
<div class="m2"><p>گزافه سخن را درستی نبود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>درست آن شد از گفتهٔ هر دیار</p></div>
<div class="m2"><p>که از فیلقوس آمد آن شهریار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دگر گفتها چون عیاری نداشت</p></div>
<div class="m2"><p>سخنگو بر آن اختیاری نداشت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چنین گوید آن پیر دیرینه سال</p></div>
<div class="m2"><p>ز تاریخ شاهان پیشینه حال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که در بزم خاص ملک فیلقوس</p></div>
<div class="m2"><p>بتی بود پاکیزه و نوعروس</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به دیدن همایون به بالا بلند</p></div>
<div class="m2"><p>به ابرو کمانکش به گیسو کمند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو سروی که پیدا کند در چمن</p></div>
<div class="m2"><p>ز گیسو بنفشه ز عارض سمن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جمالی چو در نیم‌روز آفتاب</p></div>
<div class="m2"><p>کرشمه کنان نرگسی نیم خواب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سر زلف بیچان چو مشک سیاه</p></div>
<div class="m2"><p>وزو مشگبو گشته مشکوی شاه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بر آن ماه‌رو شه چنان مهربان</p></div>
<div class="m2"><p>که جز یاد او نامدش بر زبان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به مهرش شبی شاه در برگرفت</p></div>
<div class="m2"><p>ز خرمای شه نخلین برگرفت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شد از ابر نیسان صدف باردار</p></div>
<div class="m2"><p>پدیدار شد لؤلؤ شاهسوار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو نه مه برآمد بر آبستنی</p></div>
<div class="m2"><p>به جنبش درآمد رگ رستنی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به وقت ولادت بفرمود شاه</p></div>
<div class="m2"><p>که دانا کند سوی اختر نگاه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز راز نهفته نشانش دهد</p></div>
<div class="m2"><p>وز آن جنبش آرام جانش دهد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شناسندگان برگرفتند ساز</p></div>
<div class="m2"><p>ز دور فلک باز جستند راز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به سیر سپهر انجمن ساختند</p></div>
<div class="m2"><p>ترازوی انجم برافراختند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اسد بود طالع خداوند زور</p></div>
<div class="m2"><p>کزو دیدهٔ دشمنان گشت کور</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شرف یافته آفتاب از حمل</p></div>
<div class="m2"><p>گراینده از علم سوی عمل</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>عطارد به جوزا برون تاخته</p></div>
<div class="m2"><p>مه و زهره در ثور جا ساخته</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بر آراسته قوس را مشتری</p></div>
<div class="m2"><p>زحل در ترازو به بازیگری</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ششم خانه را کرده بهرام جای</p></div>
<div class="m2"><p>چو خدمتگران گشته خدمت نمای</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چنین طالعی کامد آن نور ازو</p></div>
<div class="m2"><p>چه گویم زهی چشم بد دور ازو</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو زاد آن گرامی به فالی چنین</p></div>
<div class="m2"><p>برافروخت باغ از نهالی چنین</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>در احکام هفت اختر آمد پدید</p></div>
<div class="m2"><p>که دنیا بدو داد خواهد کلید</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از آن فرخی مرد اخترشناس</p></div>
<div class="m2"><p>خبر داد تا کرد خسرو سپاس</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>شه از مهر فرزند پیروز بخت</p></div>
<div class="m2"><p>در گنج بگشاد و برشد به تخت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به شادی گرائید از اندوه رنج</p></div>
<div class="m2"><p>به خواهندگان داد بسیار گنج</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به پیروزی آن می مشگبوی</p></div>
<div class="m2"><p>می و مشگ می‌ریخت بر طرف جوی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو شد ناز پرورده آن شاخ سرو</p></div>
<div class="m2"><p>خرامنده شد چون خرامان تذرو</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شد از چنبر مهد میدان گرای</p></div>
<div class="m2"><p>ز گهواره در مرکب آورد پای</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کمان خواست از دایه و چوبه تیر</p></div>
<div class="m2"><p>گهی کاغذش برهدف گه حریر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو شد رسته‌تر کار شمشیر کرد</p></div>
<div class="m2"><p>ز شیر افکنی جنگ با شیر کرد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>وز آن پس نشاط سواری گرفت</p></div>
<div class="m2"><p>پی شاهی و شهریاری گرفت</p></div></div>