---
title: >-
    بخش ۶ - در حسب حال و انجام روزگار
---
# بخش ۶ - در حسب حال و انجام روزگار

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی آن می نشان ده مرا</p></div>
<div class="m2"><p>از آن داروی بیهشان ده مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان داروی تلخ بیهش کنم</p></div>
<div class="m2"><p>مگر خویشتن را فراموش کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظامی بس این صاحب آوازگی</p></div>
<div class="m2"><p>کهن گشتن و هم‌چنان تازگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو شیران سرپنجه بگشای چنگ</p></div>
<div class="m2"><p>چو روبه میارای خود را به رنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شنیدم که روباه رنگین بروس</p></div>
<div class="m2"><p>خود آرای باشد به رنگ عروس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو باران بود روز یا باد و گرد</p></div>
<div class="m2"><p>برون ناورد موی خویش از نورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کنجی کند بی علف جای خویش</p></div>
<div class="m2"><p>نلیسد مگر دست با پای خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پی پوستی خون خود را خورد</p></div>
<div class="m2"><p>همه کس تن او پوست را پرورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرانجام کاید اجل سوی او</p></div>
<div class="m2"><p>وبال تن او شود موی او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدان موینه قصد خونش کنند</p></div>
<div class="m2"><p>به رسوائی از سر برونش کنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بساطی چه باید بر آراستن</p></div>
<div class="m2"><p>کزو ناگزیر است برخاستن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر آن جانور کو خودآرای نیست</p></div>
<div class="m2"><p>طمع را بر آزار او رای نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برون آی از این پردهٔ هفت رنگ</p></div>
<div class="m2"><p>که زنگی بود آینه زیر زنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بس این جادوئیها برانگیختن</p></div>
<div class="m2"><p>چو جادو به کس درنیامیختن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه گوگرد سرخی نه لعل سپید</p></div>
<div class="m2"><p>که جوینده باشد ز تو ناامید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به مردم درآمیز اگر مردمی</p></div>
<div class="m2"><p>که با آدمی خو گرست آدمی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر کان گنجی چو نائی بدست</p></div>
<div class="m2"><p>بسی گنج از اینگونه در خاک هست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو دور افتد از میوه خور میوه‌دار</p></div>
<div class="m2"><p>چه خرما بود نخل بن را چه خار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جوانی شد و زندگانی نماند</p></div>
<div class="m2"><p>جهان گو ممان چون جوانی نماند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جوانی بود خوبی آدمی</p></div>
<div class="m2"><p>چو خوبی رود کی بود خرمی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو پی سست و پوسیده گشت استخوان</p></div>
<div class="m2"><p>دگر قصه سخت روئی مخوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>غرور جوانی چو از سر نشست</p></div>
<div class="m2"><p>ز گستاخ کاری فرو شوی دست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بهی چهرهٔ باغ چندان بود</p></div>
<div class="m2"><p>که شمشاد با لاله خندان بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو باد خزانی درآید به باغ</p></div>
<div class="m2"><p>زمانه دهد جای بلبل به زاغ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شود برگ ریزان ز شاخ بلند</p></div>
<div class="m2"><p>دل باغبانان شود دردمند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ریاحین ز بستان شود ناپدید</p></div>
<div class="m2"><p>در باغ را کس نجوید کلید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بنال ای کهن بلبل سالخورد</p></div>
<div class="m2"><p>که رخساره سرخ گل گشت زرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دو تا شد سهی سرو آراسته</p></div>
<div class="m2"><p>کدیور شد از سایه برخاسته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو تاریخ پنجه درآمد به سال</p></div>
<div class="m2"><p>دگرگونه شد بر شتابنده حال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سر از بار سنگین درآمد به سنگ</p></div>
<div class="m2"><p>جمازه به تنگ آمد از راه تنگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فرو ماند دستم ز می خواستن</p></div>
<div class="m2"><p>گران گشت پایم ز بر خاستن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تنم گونهٔ لاجوردی گرفت</p></div>
<div class="m2"><p>گلم سرخی انداخت زردی گرفت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هیون رونده ز ره مانده باز</p></div>
<div class="m2"><p>به بالینگه آمد سرم را نیاز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همان بور چوگانی باد پای</p></div>
<div class="m2"><p>به صد زخم چوگان نجنبد ز جای</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>طرب را به میخانه گم شد کلید</p></div>
<div class="m2"><p>نشان پشیمانی آمد پدید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>برآمد ز کوه ابر کافور بار</p></div>
<div class="m2"><p>مزاج زمین گشت کافور خوار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گهی دل به رفتن نیاید به گوش</p></div>
<div class="m2"><p>صراحی تهی گشت و ساقی خموش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سر از لهو پیچید و گوش از سماع</p></div>
<div class="m2"><p>که نزدیک شد کوچگه را وداع</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به وقتی چنین کنج بهتر ز کاخ</p></div>
<div class="m2"><p>که دوران کند دست یازی فراخ</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تماشای پروانه چندان بود</p></div>
<div class="m2"><p>که شمع شب افروز خندان بود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو از شمع خالی کنی خانه را</p></div>
<div class="m2"><p>نبینی دگر نقش پروانه را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به روز جوانی و نوزادگی</p></div>
<div class="m2"><p>زدم لاف پیری و افتادگی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کنون گر به غم شادمانی کنم</p></div>
<div class="m2"><p>به پیرانه سر چون جوانی کنم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو پوسیده چوبی که در کنج باغ</p></div>
<div class="m2"><p>فروزنده باشد به شب چون چراغ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شب افروز کرمی که تابد ز دور</p></div>
<div class="m2"><p>ز بی‌نوری شب زند لاف نور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>اگر دیدمی در خود افزایشی</p></div>
<div class="m2"><p>طلب کردمی جای آسایشی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به آسودگی عمر نو کردمی</p></div>
<div class="m2"><p>جهان را به شادی گرو کردمی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو روز جوانی به پایان رسید</p></div>
<div class="m2"><p>سپیده دم از مشرق آمد پدید</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به تدبیر آنم که سر چون نهم</p></div>
<div class="m2"><p>چگونه پی از کار بیرون نهم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سری کو سزاوار باشد به تاج</p></div>
<div class="m2"><p>سرین گاه او مشک باید نه عاج</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از آن پیش کاین هفت پرگار نیز</p></div>
<div class="m2"><p>کند خط عمر مرا ریز ریز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>درآرم به هر زخمه‌ای دست خویش</p></div>
<div class="m2"><p>نگهدارم آوازهٔ هست خویش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به هر مهره‌ای حقه‌بازی کنم</p></div>
<div class="m2"><p>به واماند خود چاره‌سازی کنم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو رهوار گیلیم ازین پل گذشت</p></div>
<div class="m2"><p>به گیلان ندارم سر بازگشت</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>در این ره چو من خوابنیده بسیست</p></div>
<div class="m2"><p>نیارد کسی یاد که آنجا کسیست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به یادآور ای تازه کبک دری</p></div>
<div class="m2"><p>که چون بر سر خاک من بگذری</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گیا بینی از خاکم انگیخته</p></div>
<div class="m2"><p>سرین سوده پائین فرو ریخته</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>همه خاک فرش مرا برده باد</p></div>
<div class="m2"><p>نکرده ز من هیچ هم عهد یاد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نهی دست بر شوشه خاک من</p></div>
<div class="m2"><p>به یاد آری از گوهر پاک من</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>فشانی تو بر من سرشکی ز دور</p></div>
<div class="m2"><p>فشانم من از آسمان بر تو نور</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>دعای تو بر هر چه دارد شتاب</p></div>
<div class="m2"><p>من آمین کنم تا شود مستجاب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>درودم رسانی رسانم درود</p></div>
<div class="m2"><p>بیائی بیایم ز گنبد فرود</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مرا زنده پندار چون خویشتن</p></div>
<div class="m2"><p>من آیم به جان گر تو آیی به تن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مدان خالی از هم نشینی مرا</p></div>
<div class="m2"><p>که بینم تو را گر نبینی مرا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>لب از خفته‌ای چند خامش مکن</p></div>
<div class="m2"><p>فرو خفتگان را فرامش مکن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو آن‌جا رسی می درافکن به جام</p></div>
<div class="m2"><p>سوی خوابگاه نظامی خرام</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نپنداری ای خضر پیروز پی</p></div>
<div class="m2"><p>که از می مرا هست مقصود می</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>از آن می همه بی‌خودی خواستم</p></div>
<div class="m2"><p>بدان بی‌خودی مجلس آراستم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>مرا ساقی از وعده ایزدیست</p></div>
<div class="m2"><p>صبوح از خرابی می‌از بیخودیست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>وگرنه به یزدان که تا بوده‌ام</p></div>
<div class="m2"><p>به می دامن لب نیالوده‌ام</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گر از می شدم هرگز آلوده کام</p></div>
<div class="m2"><p>حلال خدایست بر من حرام</p></div></div>