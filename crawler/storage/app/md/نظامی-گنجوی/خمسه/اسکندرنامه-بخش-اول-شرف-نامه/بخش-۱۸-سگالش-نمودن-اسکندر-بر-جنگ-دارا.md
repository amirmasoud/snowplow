---
title: >-
    بخش ۱۸ - سگالش نمودن اسکندر بر جنگ دارا
---
# بخش ۱۸ - سگالش نمودن اسکندر بر جنگ دارا

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی آن می‌که فرخ پیست</p></div>
<div class="m2"><p>به من ده که داروی مردم میست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میی کوست حلوای هر غم کشی</p></div>
<div class="m2"><p>ندیده به جز آفتاب آتشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان بینم از میل جوینده پر</p></div>
<div class="m2"><p>یکی سوی دریا یکی سوی در</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه بینم کسی را در این روزگار</p></div>
<div class="m2"><p>که میلش بود سوی آموزگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو من بلبلی را بود ناگزیر</p></div>
<div class="m2"><p>کز این گوش گیران شوم گوشه‌گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مشغولی نغمهٔ این سرود</p></div>
<div class="m2"><p>شوم فارغ از شغل دریا و رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بیرون جهم گه گه از کنج باغ</p></div>
<div class="m2"><p>ترنجی به دستم چو روشن چراغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نبینم کس از هوشیاران مست</p></div>
<div class="m2"><p>که دادن توان آن ترنجش به دست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دگر باره از دست این دوستان</p></div>
<div class="m2"><p>گریز آورم سوی آن بوستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تماشای این باغ دلکش کنم</p></div>
<div class="m2"><p>بدو خاطر خویش را خوش کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گزارشگر کارگاه سخن</p></div>
<div class="m2"><p>چنین گوید از موبدان کهن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که چون شاه روم از شبیخون زنگ</p></div>
<div class="m2"><p>برآسود و آمد مرادش به چنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پذیره شد آسایش و خواب را</p></div>
<div class="m2"><p>روان کرد بر کف می ناب را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به نوروز بنشست و می نوش کرد</p></div>
<div class="m2"><p>سرود سرایندگان گوش کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نبودی ز شه دور تا وقت خواب</p></div>
<div class="m2"><p>مغنی و ساقی و رود و شراب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حسابی به جز کامرانی نداشت</p></div>
<div class="m2"><p>از آن به کسی زندگانی نداشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نشسته جهاندار گیتی فروز</p></div>
<div class="m2"><p>به فیروزی آورده شب را به روز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به پیرامنش فیلسوفان دهر</p></div>
<div class="m2"><p>جهان را به داد و دهش داد بهر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ارسطو به ساغر فلاطون به جام</p></div>
<div class="m2"><p>می خام ریزنده بر خون خام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مغنی سراینده بر بانگ رود</p></div>
<div class="m2"><p>به نوروزی شه نو آیین سرود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که دولت پناها جوان بخت باش</p></div>
<div class="m2"><p>همه ساله با افسر و تخت باش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گرو کن به عمر ابد جام را</p></div>
<div class="m2"><p>گرو گیر کن باده خام را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بساط می ارغوانی بنه</p></div>
<div class="m2"><p>طرب ساز و داد جوانی بده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو داری جوانی و اقبال هست</p></div>
<div class="m2"><p>به رود و به می شاد باید نشست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو ترتیب شمشیر کردی تمام</p></div>
<div class="m2"><p>بر آرای مجلس به ترتیب جام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جهان گیر در سایه تاج و تخت</p></div>
<div class="m2"><p>نگیرد جهان با تو این کار سخت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سیاهی گرفتی سپیدی بگیر</p></div>
<div class="m2"><p>چنین ابلقی با شدت ناگزیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>علم بر فلک زن که عالم تراست</p></div>
<div class="m2"><p>به دولت در آویز کان هم تراست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شه از نصرت مصر و تاراج زنگ</p></div>
<div class="m2"><p>به چهره در آورده بود آب و رنگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زبون کردن دشمن آسان گرفت</p></div>
<div class="m2"><p>حساب خراج از خراسان گرفت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به هم سنگی خویش در روم و شام</p></div>
<div class="m2"><p>نیامد کسش در ترازو تمام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به دارا نداد آنچه داد از نخست</p></div>
<div class="m2"><p>همان داده را نیز ازو باز جست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از آنجا که روز جوانیش بود</p></div>
<div class="m2"><p>تمنای کشور ستانیش بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کمربند ایرانیان سست کرد</p></div>
<div class="m2"><p>به ایران گرفتن کمر چست کرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>درختی که او سر برآرد بلند</p></div>
<div class="m2"><p>به دیگر درختان رساند گزند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به نخجیر شد شاه یک روز کش</p></div>
<div class="m2"><p>هم او خوش‌منش بود و هم‌روز خوش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شکار افکنان دشتها در نوشت</p></div>
<div class="m2"><p>همی کرد نخجیر در کوه و دشت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فلک وار می‌شد سری پر شکوه</p></div>
<div class="m2"><p>گهی سوی صحرا گهی سوی کوه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گذشت از قضا بر یکی کوهسار</p></div>
<div class="m2"><p>که بود از بسی گونه در وی شکار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دو کبک دری دید بر خاره سنگ</p></div>
<div class="m2"><p>به آیین کبکان جنگی به جنگ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گه آن مغز این را به منقار خست</p></div>
<div class="m2"><p>گه این بال آنرا به ناخن شکست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در آن معرکه راند شه بارگی</p></div>
<div class="m2"><p>همی بود بر هر دو نظارگی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز سختی که کبکان در آویختند</p></div>
<div class="m2"><p>ز نظارهٔ شاه نگریختند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شگفتی فرومانده شه زان شمار</p></div>
<div class="m2"><p>که در مغز مرغان چه بود آن خمار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>یکی را نشان کرد بر نام خویش</p></div>
<div class="m2"><p>برو بست فال سرانجام خویش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دگر مرغ را نام دارا نهاد</p></div>
<div class="m2"><p>بر آن فال چشم آشکارا نهاد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دو مرغ دلاور در آن داوری</p></div>
<div class="m2"><p>زمانی نمودند جنگ آوری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همان مرغ شد عاقبت کامگار</p></div>
<div class="m2"><p>که بر نام خود فال زد شهریار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو پیروز دید آنچنان حال را</p></div>
<div class="m2"><p>دلیل ظفر یافت آن فال را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خرامنده کبک ظفر یافته</p></div>
<div class="m2"><p>پرید از برکبک بر تافته</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سوی پشتهٔ کوه پرواز کرد</p></div>
<div class="m2"><p>عقابی درآمد سرش باز کرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو بشکست کبک دری را عقاب</p></div>
<div class="m2"><p>ملک کبک بشکست و آمد به تاب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز پرواز پیروزی خویشتن</p></div>
<div class="m2"><p>نبودش همانا غم جان و تن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بدانست کاقبال یاری دهد</p></div>
<div class="m2"><p>به دارا در کامگاری دهد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ولیکن در آن دولت کامگار</p></div>
<div class="m2"><p>نباشد بسی عمر او پایدار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شنیدم که بود اندر آن خاره کوه</p></div>
<div class="m2"><p>مقرنس یکی طاق گردون شکوه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>که پرسندگان زو به آواز خویش</p></div>
<div class="m2"><p>خبر باز جستندی از راز خویش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>صدائی شنیدندی از کوه سخت</p></div>
<div class="m2"><p>بر انسان که بودی نمودار بخت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بفرمود شه تا یکی هوشمند</p></div>
<div class="m2"><p>خبر باز پرسد ز کوه بلند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>که چون در جهان ریزش خون بود</p></div>
<div class="m2"><p>سرانجام اقبال او چون بود</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بپرسید پرسندهٔ نغز فال</p></div>
<div class="m2"><p>که چون می‌نماید سرانجام حال؟</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>سکندر شود بر جهان چیره دست؟</p></div>
<div class="m2"><p>به دارای دارا درآرد شکست؟</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>صدائی برآورد کوه از نهفت</p></div>
<div class="m2"><p>همان را که او گفته بدباز گفت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>از آن فال فرخ دل خسروی</p></div>
<div class="m2"><p>چو کوه قوی یافت پشت قوی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به خرم دلی زان طرف بازگشت</p></div>
<div class="m2"><p>سوی بزمگاه آمد از کوه و دشت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به تدبیر بنشست با انجمن</p></div>
<div class="m2"><p>چو سرو سهی در میان چمن</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سخن راند ز اندازه کار خویش</p></div>
<div class="m2"><p>ز پیروزی صلح و پیکار خویش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>که چون من به نیروی گیتی پناه</p></div>
<div class="m2"><p>به گردون گردان رساندم کلاه</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گزیت رباخوارگان چون دهم</p></div>
<div class="m2"><p>به خود بر چنین خواریی چون نهم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به دارا چرا داد باید خراج</p></div>
<div class="m2"><p>کزو کم ندارم نه گوهر نه تاج</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گر او تاج دارد مرا تیغ هست</p></div>
<div class="m2"><p>چو تیغم بود تاجم آید به دست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>گر او لشگر آرد به پیکار من</p></div>
<div class="m2"><p>نگهدار من بس نگهدار من</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>مرا نصرت ایزدی حاصلست</p></div>
<div class="m2"><p>که رایم قوی لشگرم یکدلست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>سپه را که فیروزمندی رسد</p></div>
<div class="m2"><p>ز یاران یک دل بلندی رسد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>دو درزی ز دل بشکند کوه را</p></div>
<div class="m2"><p>پراکندگی آرد انبوه را</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>امیدم چنان شد به نیروی بخت</p></div>
<div class="m2"><p>که بستانم از دشمنان تاج و تخت</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چه باید رصدگاه دارا شدن</p></div>
<div class="m2"><p>به جزیت دهی آشکارا شدن</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>شما زیرکان از سریاوری</p></div>
<div class="m2"><p>چه گوئید چون باشد این داوری</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چه حجت بود پیش دارا مرا</p></div>
<div class="m2"><p>نهانی کند آشکارا مرا</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>شناسندگان سرانجام کار</p></div>
<div class="m2"><p>دعا تازه کردند بر شهریار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>که تا چرخ گردنده و اخترست</p></div>
<div class="m2"><p>وزین هر دو آمیزش گوهرست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چراغ جهان گوهر شاه باد</p></div>
<div class="m2"><p>رخ شاه روشن‌تر از ماه باد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>توئی آنکه نیروی بینش به توست</p></div>
<div class="m2"><p>برومندی آفرینش به توست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به هر جا که باشی خداوند باش</p></div>
<div class="m2"><p>ز تخمی که کاری برومند باش</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چو پرسیدی از ما به فرخنده رای</p></div>
<div class="m2"><p>بگوئیم چون بخت شد رهنمای</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چنانست رخصت برای صواب</p></div>
<div class="m2"><p>که شه بر مخالف نیارد شتاب</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>تو بنشین گر او با تو جنگ آورد</p></div>
<div class="m2"><p>بر او تیغ تو کار تنگ آورد</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ز دست تو یک تیغ برداشتن</p></div>
<div class="m2"><p>ز دشمن سر و تیغ بگذاشتن</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>گوزنی که با شیر بازی کند</p></div>
<div class="m2"><p>زمین جای قربان نمازی کند</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ز دارا نیاید به جز نای و نوش</p></div>
<div class="m2"><p>گر آید به تو خونش آید به جوش</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>تو زو بیش در لشگر آراستن</p></div>
<div class="m2"><p>خراج از زبونان توان خواستن</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>شبیخون تو تا بیابان زنگ</p></div>
<div class="m2"><p>تماشای او تا شبستان تنگ</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>تو دین پروری خصم کین پرورست</p></div>
<div class="m2"><p>فرشته دگر اهرمن دیگرست</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>تو شمشیرگیری و او جام گیر</p></div>
<div class="m2"><p>تو بر سر نشینی و او بر سریر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>تو با دادی او هست بیدادگر</p></div>
<div class="m2"><p>تو میزان زور او ترازوی زر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>تو بیداری او بی خودی می‌کند</p></div>
<div class="m2"><p>تو نیکی کنی او بدی می‌کند</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بدآن بد که از جمله شهر و سپاه</p></div>
<div class="m2"><p>ز نیکان ندارد کسی نیکخواه</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ببینی که روزی هم آزار او</p></div>
<div class="m2"><p>کسادی در آرد به بازار او</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>نوازشگری های بد رام تو</p></div>
<div class="m2"><p>برآرد به هفتم فلک نام تو</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ز حق دشمنی چند باطل ستیز</p></div>
<div class="m2"><p>مکن چون کند باطل از حق گریز</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>کمربند بیداری بخت گیر</p></div>
<div class="m2"><p>کله داریی کن سر تخت گیر</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>نباید که بندد تو را این خیال</p></div>
<div class="m2"><p>که دولت به ملک است و نصرت به مال</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>سری کردن مردم از مردمیست</p></div>
<div class="m2"><p>وگرنه همه آدمی آدمیست</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>همه مردمی سرفرازی کند</p></div>
<div class="m2"><p>سر آن شد که مردم نوازی کند</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>دد و دام را شیر از آنست شاه</p></div>
<div class="m2"><p>که مهمان نوازست در صیدگاه</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>جهان خوش بدان نیست کری به دست</p></div>
<div class="m2"><p>به زنجیر و قفلش کنی پای بست</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>ز عیش خوش آنگه نشانش دهی</p></div>
<div class="m2"><p>کز اینش ستانی به آنش دهی</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>جوانمرد پیوسته با کس بود</p></div>
<div class="m2"><p>کس آن را نباشد که ناکس بود</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>بدان کس که او را خمیریست خام</p></div>
<div class="m2"><p>همه کس دهد نان پخته به وام</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>مروت تو داری و مردی تو راست</p></div>
<div class="m2"><p>بداندیش را گنج با اژدهاست</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>گر او تندر آمد تو هستی درخش</p></div>
<div class="m2"><p>گر او گنجدان شد توئی گنج بخش</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>پدر گرچه با قوت شیر بود</p></div>
<div class="m2"><p>به کین خواستن نرم شمشیر بود</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>تو آن شیرگیری که در وقت جنگ</p></div>
<div class="m2"><p>ز شمشیر تو خون شود خاره سنگ</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>چگوئی سیاهان زنگی سرشت</p></div>
<div class="m2"><p>که بودند چون دیو دژخیم زشت</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چو با تیغ تو سرکشی ساختند</p></div>
<div class="m2"><p>به جز سر چه در پایت انداختند</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چو زان سیلها بر نگشتی چو کوه</p></div>
<div class="m2"><p>از این قطره‌ها هم نداری شکوه</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>نهنگی که او پیل را پی کند</p></div>
<div class="m2"><p>از آهو بره عاجزی کی کند</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>هژبر ژیان کی شود صید گور</p></div>
<div class="m2"><p>سیه مارکی روی تابد ز مور</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>عقابی که نخجیر سازی کند</p></div>
<div class="m2"><p>به فروجکان دست بازی کند</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>دگر کاختران نیک خواه تواند</p></div>
<div class="m2"><p>همان خاکیان خاک راه تواند</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>نمودار گیتی گشائی تراست</p></div>
<div class="m2"><p>خلل خصم را مومیائی تراست</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>به چندین نشانهای فیروزمند</p></div>
<div class="m2"><p>بداندیش را چون نباید گزند</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>به فالی کز اختر توان برشمرد</p></div>
<div class="m2"><p>توداری درین داوری دستبرد</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>همان در حروف خط هندسی</p></div>
<div class="m2"><p>تو غالب‌تری گر سخن بررسی</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>پلنگر که لشکرکش زنگ بود</p></div>
<div class="m2"><p>به وقتی که با قوت چنگ بود</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>به مغلوبم و غالب چو بشتافتیم</p></div>
<div class="m2"><p>در آن فتح غالب تو را یافتیم</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>چو پیروز بود آن نمونش به فال</p></div>
<div class="m2"><p>در این هم توان بود پیروز حال</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>شه از نصرت رهنمایان خویش</p></div>
<div class="m2"><p>حساب جهانگیری آورد پیش</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>به هر جا که شمشیر و ساغر گرفت</p></div>
<div class="m2"><p>به نیک اختری فال اختر گرفت</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>به فرخندگی فال زن ماه و سال</p></div>
<div class="m2"><p>که فرخ بود فال فرخ به فال</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>مزن فال بد کاورد حال بد</p></div>
<div class="m2"><p>مبادا کسی کو زند فال بد</p></div></div>