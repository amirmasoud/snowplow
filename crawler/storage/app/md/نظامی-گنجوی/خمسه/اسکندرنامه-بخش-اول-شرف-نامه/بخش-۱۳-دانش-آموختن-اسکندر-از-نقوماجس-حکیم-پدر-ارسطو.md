---
title: >-
    بخش ۱۳ - دانش آموختن اسکندر از نقوماجس حکیم پدر ارسطو
---
# بخش ۱۳ - دانش آموختن اسکندر از نقوماجس حکیم پدر ارسطو

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی آن راح ریحان سرشت</p></div>
<div class="m2"><p>به من ده که بر یادم آمد بهشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر ز آن می آباد کشتی شوم</p></div>
<div class="m2"><p>وگر غرقه گردم بهشتی شوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشا روزگارا که دارد کسی</p></div>
<div class="m2"><p>که بازار حرصش نباشد بسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به قدر بسندش یساری بود</p></div>
<div class="m2"><p>کند کاری ار مرد کاری بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان می‌گذارد به خوشخوارگی</p></div>
<div class="m2"><p>به اندازه دارد تک بارگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه بذلی که طوفان برآرد ز مال</p></div>
<div class="m2"><p>نه صرفی که سختی درآرد به حال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه سختی از بستگی لازمست</p></div>
<div class="m2"><p>چو در بشکنی خانه پر هیزم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان زی کزان زیستن سالیان</p></div>
<div class="m2"><p>تو را سود و کس را نباشد زیان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گزارنده درج دهقان نورد</p></div>
<div class="m2"><p>گزارندگان را چنین یاد کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که چون شاه یونان ملک فیلقوس</p></div>
<div class="m2"><p>برآراست ملک جهان چون عروس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به فرزانه فرزند شد سر بلند</p></div>
<div class="m2"><p>که فرخ بود گوهر ارجمند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو فرزند خود را خردمند یافت</p></div>
<div class="m2"><p>شد ایمن که شایسته فرزند یافت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ندارد پدر هیچ بایسته‌تر</p></div>
<div class="m2"><p>ز فرزند شایسته شایسته‌تر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نشاندش به دانش در آموختن</p></div>
<div class="m2"><p>که گوهر شود سنگ از افروختن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نقوماجس آنکو خردمند بود</p></div>
<div class="m2"><p>ارسطوی داناش فرزند بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به آموزگاری برو رنج برد</p></div>
<div class="m2"><p>بیاموختش آنچه نتوان شمرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ادبهای شاهی هنرهای نغز</p></div>
<div class="m2"><p>که نیروی دل باشد و نور مغز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز هر دانشی کو بود در قیاس</p></div>
<div class="m2"><p>وزو گردد اندیشه معنی شناس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برآراست آن گوهر پاک را</p></div>
<div class="m2"><p>چو انجم که آراید افلاک را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خبر دادش از هر چه در پرده بود</p></div>
<div class="m2"><p>کسی کم چنان طفل پرورده بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه ساله شهزاده تیزهوش</p></div>
<div class="m2"><p>به جز علم را ره ندادی به گوش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به باریک بینی چو بشتافتی</p></div>
<div class="m2"><p>سخن‌های باریک دریافتی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ارسطو که هم‌درس شهزاده بود</p></div>
<div class="m2"><p>به خدمتگری دل به دو داده بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر آنچ از پدر مایه اندوختی</p></div>
<div class="m2"><p>گزارش کنان دروی آموختی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو استاد دانا به فرهنگ ورای</p></div>
<div class="m2"><p>ملک زاده را دید بر گنج پای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به تعلیم او بیشتر برد رنج</p></div>
<div class="m2"><p>که خوش‌دل کند مرد را پاس گنج</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو منشور اقبال او خواند پیش</p></div>
<div class="m2"><p>درو بست عنوان فرزند خویش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به روزی که طالع پذیرنده بود</p></div>
<div class="m2"><p>نگین سخن مهر گیرنده بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به شهزاده بسپرد فرزند را</p></div>
<div class="m2"><p>به پیمان در افزود سوگند را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که چون سر براری به چرخ بلند</p></div>
<div class="m2"><p>ز مکتب به میدان جهانی سمند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سر دشمنان بر زمین آوری</p></div>
<div class="m2"><p>جهان زیر مهر نگین آوری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همایون کنی تخت را زیر تاج</p></div>
<div class="m2"><p>فرستندت از هفت کشور خراج</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر آفاق کشور خدائی کنی</p></div>
<div class="m2"><p>جهان در جهان پادشائی کنی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به یاد آری این درس و تعلیم را</p></div>
<div class="m2"><p>پرستش نسازی زر و سیم را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نظر بر نداری ز فرزند من</p></div>
<div class="m2"><p>به جای آوری حق پیوند من</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به دستوری او شوی شغل سنج</p></div>
<div class="m2"><p>که دستور دانا به از تیغ و گنج</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو را دولت او را هنر یاور است</p></div>
<div class="m2"><p>هنرمند با دولتی در خور است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هنر هر کجا یافت قدری تمام</p></div>
<div class="m2"><p>به دولت خدائی برآورد نام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همان دولتی کارجمندی گرفت</p></div>
<div class="m2"><p>ز رای بلندان بلندی گرفت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو خواهی که بر مه رسانی سریر</p></div>
<div class="m2"><p>ازین نردبان باشدت ناگزیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ملک زاده با او بهم داد دست</p></div>
<div class="m2"><p>به پذرفتگاری بر آن عهد بست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که شاهی چو بر من کند شغل راست</p></div>
<div class="m2"><p>وزیر او بود بر من ایزد گواست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نتابم سر از رأی و پیمان او</p></div>
<div class="m2"><p>نبندم کمر جز به فرمان او</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سرانجام کاقبال یاری نمود</p></div>
<div class="m2"><p>برآن عهد شاه استواری نمود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو استاد دانست کان طفل خرد</p></div>
<div class="m2"><p>بخواهد ز گردنکشان گوی برد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از آن هندسی حرف شکلی کشید</p></div>
<div class="m2"><p>که مغلوب و غالب درو شد پدید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بدو داد کین حرف را وقت کار</p></div>
<div class="m2"><p>به نام خود و خصم خود برشمار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اگر غالب از دایره نام توست</p></div>
<div class="m2"><p>شمار ظفر در سرانجام توست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>وگر ز آنکه ناغالبی در قیاس</p></div>
<div class="m2"><p>ز غالب‌تر از خویشتن در هراس</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شه آن حرف بستد ز دانای پیر</p></div>
<div class="m2"><p>شد آن داوری پیش او دلپذیر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو هر وقت کان حرف بنگاشتی</p></div>
<div class="m2"><p>ز پیروزی خود خبر داشتی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بر اینگونه می‌زیست بارای و هوش</p></div>
<div class="m2"><p>ز هر دانش آورده دیگی به جوش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هم او همتی زیرک اندیش داشت</p></div>
<div class="m2"><p>هم اندیشه زیرکان بیش داشت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به فرمان کار آگهان کار کرد</p></div>
<div class="m2"><p>بدین آگهی بخت را یار کرد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هنر پیشه فرزند استاد او</p></div>
<div class="m2"><p>که هم‌درس او بود و هم‌زاد او</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>عجب مهربان بود بر مرزبان</p></div>
<div class="m2"><p>دل مرزبان هم بدو مهربان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نکردی یکی مرغ بر بابزن</p></div>
<div class="m2"><p>کارسطو نبودی بر آن رای زن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نجستی ز تدبیر او دوریی</p></div>
<div class="m2"><p>بهر کار ازو خواست دستوریی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو پرگار چرخ از بر کوه و دشت</p></div>
<div class="m2"><p>برین دایره مدتی چند گشت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ملک فیلقوس از جهان رخت برد</p></div>
<div class="m2"><p>جهان را به شاهنشه نو سپرد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>جهان چیست بگذر ز نیرنگ او</p></div>
<div class="m2"><p>رهائی به چنگ آور از چنگ او</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>درختی است شش پهلو و چاربیخ</p></div>
<div class="m2"><p>تنی چند را بسته بر چار میخ</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>یکایک ورقهای ما زین درخت</p></div>
<div class="m2"><p>به زیر اوفتد چون وزد باد سخت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مقیمی نبینی درین باغ کس</p></div>
<div class="m2"><p>تماشا کند هر یکی یک نفس</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>در او هر دمی نوبری می‌رسد</p></div>
<div class="m2"><p>یکی می‌رود دیگری می‌رسد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>جهان کام و ناکام خواهی سپرد</p></div>
<div class="m2"><p>به خود کامگی پی چه خواهی فشرد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>درین چارسو هیچ هنگامه نیست</p></div>
<div class="m2"><p>که کیسه بر مرد خودکامه نیست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به دام جهان هستی از وام او</p></div>
<div class="m2"><p>بده وام او رستی از دام او</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>شبی نعلبندی و پالانگری</p></div>
<div class="m2"><p>حق خویشتن خواستند از خری</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>خر از پای رنجیده و پشت ریش</p></div>
<div class="m2"><p>بیفکندشان نعل و پالان به پیش</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو از وام‌داری خر آزاد گشت</p></div>
<div class="m2"><p>بر آسود و از خویشتن شاد گشت</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>تو نیز ای به خاکی شده گردناگ</p></div>
<div class="m2"><p>بده وام و بیرون چه از گرد و خاک</p></div></div>