---
title: >-
    بخش ۷ - در شرف این نامه بر دیگر نامه‌ها
---
# بخش ۷ - در شرف این نامه بر دیگر نامه‌ها

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی از سر بنه خواب را</p></div>
<div class="m2"><p>می ناب ده عاشق ناب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میی گو چو آب زلال آمده است</p></div>
<div class="m2"><p>بهر چار مذهب حلال آمده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلا تا بزرگی نیاری به دست</p></div>
<div class="m2"><p>به جای بزرگان نشاید نشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزرگیت باید در این دسترس</p></div>
<div class="m2"><p>به یاد بزرگان برآور نفس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن تا نپرسند لب بسته دار</p></div>
<div class="m2"><p>گهر نشکنی تیشه آهسته‌دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نپرسیده هر کو سخن یاد کرد</p></div>
<div class="m2"><p>همه گفته خویش را باد کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بی دیده نتوان نمودن چراغ</p></div>
<div class="m2"><p>که جز دیده را دل نخواهد به باغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن گفتن آنگه بود سودمند</p></div>
<div class="m2"><p>کز آن گفتن آوازه گردد بلند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو در خورد گوینده ناید جواب</p></div>
<div class="m2"><p>سخن یاوه کردن نباشد صواب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دهن را به مسمار بر دوختن</p></div>
<div class="m2"><p>به از گفتن و گفته را سوختن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه می‌گویم ای نانیوشنده مرد</p></div>
<div class="m2"><p>ترا گوش بر قصهٔ خواب و خورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه دانی که من خود چه فن میزنم</p></div>
<div class="m2"><p>دهل بر در خویشتن میزنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>متاع گران مایه دارم بسی</p></div>
<div class="m2"><p>نیارم برون تا نخواهد کسی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خریدار در چون صدف دیده دوخت</p></div>
<div class="m2"><p>بدین کاسدی در نشاید فروخت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرا با چنین گوهری ارجمند</p></div>
<div class="m2"><p>همی حاجت آید به گوهر پسند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیوشنده‌ای خواهم از روزگار</p></div>
<div class="m2"><p>که گویم بدو راز آموزگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بکاوم به الماس او کان خویش</p></div>
<div class="m2"><p>کنم بسته در جان او جان خویش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زمانه چنین پیشه‌ها پر دهد</p></div>
<div class="m2"><p>یکی درستاند یکی در دهد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دلی کو که بی جان خراشی بود</p></div>
<div class="m2"><p>کمندی که بی دور باشی بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مگر مار برد گنج از آن رو نشست</p></div>
<div class="m2"><p>که تا رایگان مهره ناید به دست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر نخل خرما نباشد بلند</p></div>
<div class="m2"><p>ز تاراج هر طفل یابد گزند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به شحنه توان پاس ره داشتن</p></div>
<div class="m2"><p>به خاکستر آتش نگه داشتن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ازین خوی خوش کو سرشت منست</p></div>
<div class="m2"><p>بسی رخنه در کار و کشت منست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دگر رهروان کاین کمر بسته‌اند</p></div>
<div class="m2"><p>به خوی بد از رهزنان رسته‌اند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدان تا گریزند طفلان راه</p></div>
<div class="m2"><p>چو زنگی چرا گشت باید سیاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به راهی که خواهم شدن رخت کش</p></div>
<div class="m2"><p>ره آورد من بس بود خوی خوش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به خوی خوش آموده به گوهرم</p></div>
<div class="m2"><p>بدین زیستم هم بدین بگذرم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو از بهر هر کس دری سفتنی است</p></div>
<div class="m2"><p>سرودی هم از بهر خود گفتنی است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز چندین سخن گو سخن یاد دار</p></div>
<div class="m2"><p>سخن را منم در جهان یادگار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سخن چون گرفت استقامت به من</p></div>
<div class="m2"><p>قیامت کند تا قیامت به من</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>منم سرو پیرای باغ سخن</p></div>
<div class="m2"><p>به خدمت میان بسته چون سرو بن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فلک‌وار دور از فسوس همه</p></div>
<div class="m2"><p>سرآمد ولی پای بوس همه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو برجیس در جنگ هر بدگمان</p></div>
<div class="m2"><p>کمان دارم و برندارم کمان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو زهره درم در ترازو نهم</p></div>
<div class="m2"><p>ولی چون دهم بی ترازو دهم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نخندم بر اندوه کس برق‌وار</p></div>
<div class="m2"><p>که از برق من در من افتد شرار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به هر خار چون گل صلائی زنم</p></div>
<div class="m2"><p>به هر زخم چون نی نوائی زنم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مگر کاتش است این دل سوخته</p></div>
<div class="m2"><p>که از خار خوردن شد افروخته</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو دریا شوم دشمنی عیب شوی</p></div>
<div class="m2"><p>نه چون آینه دوستی عیب گوی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به خواهنده آن بخشم از مال و گنج</p></div>
<div class="m2"><p>که از باز دادن نیایم به رنج</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نمایم جو و گندم آرم به جای</p></div>
<div class="m2"><p>نه چون جو فروشان گندم نمای</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پس و پیش چون آفتابم یکیست</p></div>
<div class="m2"><p>فروغم فراوان فریب‌اند کیست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پس هیچ پشتی چنان نگذرم</p></div>
<div class="m2"><p>که در پیش رویش خجالت برم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز بدگوی بد گفته پنهان کنم</p></div>
<div class="m2"><p>به پاداش نیکش پشیمان کنم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نگویم بداندیش را نیز بد</p></div>
<div class="m2"><p>کزان گفته باشم بداندیش خود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بدین نیکی آرندم از دشت و رود</p></div>
<div class="m2"><p>ز نیکان و از نیکنامان درود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وزین حال اگر نیز گردان شوم</p></div>
<div class="m2"><p>زیارتگه نیک مردان شوم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شوم بر درم ریز خود در فشان</p></div>
<div class="m2"><p>کنم سرکشی لیک با سرکشان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز بی آلتی وانماندم به کنج</p></div>
<div class="m2"><p>جهان باد و از باد ترسد ترنج</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز شاهان گیتی در این غار ژرف</p></div>
<div class="m2"><p>که را بود چون من حریفی شگرف</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>که دید است بر هیچ رنگین گلی</p></div>
<div class="m2"><p>ز من عالی آوازه‌تر بلبلی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به هر دانشی دفتر آراسته</p></div>
<div class="m2"><p>به هر نکته‌ای خامه‌ای خواسته</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>پذیرفته از هر فنی روشنی</p></div>
<div class="m2"><p>جداگانه در هر فنی یک فنی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شکر دانم از هر لب انگیختن</p></div>
<div class="m2"><p>گلابی ز هر دیده‌ای ریختن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کسی را که در گریه آرم چو آب</p></div>
<div class="m2"><p>بخندانمش باز چون آفتاب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به دستم دراز دولت خوش عنان</p></div>
<div class="m2"><p>طبر زد چنین شد طبر خون چنان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>توانم در زهد بر دوختن</p></div>
<div class="m2"><p>به بزم آمدن مجلس افروختن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ولیکن درخت من از گوشه رست</p></div>
<div class="m2"><p>ز جا گر بجنبد شود بیخ سست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چهله چهل گشت و خلوت هزار</p></div>
<div class="m2"><p>به بزم آمدن دور باشد ز کار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به هنگام سیل آشکارا شدن</p></div>
<div class="m2"><p>نشاید ز ری تا بخارا شدن</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همان به که با این چنین باد سخت</p></div>
<div class="m2"><p>برون ناورم چون گل از گوشه رخت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به خود کم شوم خلق را رهنمای</p></div>
<div class="m2"><p>همایون ز کم دیدن آمد همای</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>سرم پیچد از خفتن و تاختن</p></div>
<div class="m2"><p>ندانم جز این چاره‌ای ساختن</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گه از هر سخن بر تراشم گلی</p></div>
<div class="m2"><p>بر آن گل زنم ناله چون بلبلی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>اگر به ز خود گلبنی دیدمی</p></div>
<div class="m2"><p>گل سرخ یا زرد ازو چیدمی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو از ران خود خورد باید کباب</p></div>
<div class="m2"><p>چه گردم به در یوزه چون آفتاب</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نشینم چو سیمرغ در گوشه‌ای</p></div>
<div class="m2"><p>دهم گوش را از دهن توشه‌ای</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ملالت گرفت از من ایام را</p></div>
<div class="m2"><p>به کنج ارم بردم آرام را</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>در خانه را چون سپهر بلند</p></div>
<div class="m2"><p>زدم بر جهان قفل و بر خلق بند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ندانم که دور از چه سان میرود</p></div>
<div class="m2"><p>چه نیک و چه بد در جهان میرود</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>یکی مرده شخصم به مردی روان</p></div>
<div class="m2"><p>نه از کاروانی و در کاروان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به صد رنج دل یک نفس می‌زنم</p></div>
<div class="m2"><p>بدان تا نخسبم جرس می‌زنم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ندانم کسی کو به جان و به تن</p></div>
<div class="m2"><p>مراد و ستر دارد از خویشتن</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ز مهر کسان روی برتافتم</p></div>
<div class="m2"><p>کس خویش هم خویش را یافتم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بر عاشقان نیک اگر بد شوم</p></div>
<div class="m2"><p>همان به که معشوق خود خود شوم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>گرم نیست روزی ز مهر کسان</p></div>
<div class="m2"><p>خدایست رزاق و روزی رسان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>در حاجت از خلق بربسته به</p></div>
<div class="m2"><p>ز دربانی آدمی رسته به</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>مرا کاشکی بودی آن دسترس</p></div>
<div class="m2"><p>که نگذارمی حاجت کس به کس</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>در این مندل خاکی از بیم خون</p></div>
<div class="m2"><p>نیارم سر آوردن از خط برون</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بدین حال و مندل کسی چون بود</p></div>
<div class="m2"><p>که زندانی مبدل خون بود</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>در خلق را گل براندوده‌ام</p></div>
<div class="m2"><p>درین در بدین دولت آسوده‌ام</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چهل روز خود را گرفتم زمام</p></div>
<div class="m2"><p>کادیم از چهل روز گردد تمام</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چو در چار بالش ندیدم درنگ</p></div>
<div class="m2"><p>نشستم در این چار دیوار تنگ</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ز هر جو که انداختم در خراس</p></div>
<div class="m2"><p>دری باز دادم به جوهر شناس</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>هزار آفرین بر سخن پروری</p></div>
<div class="m2"><p>که بر سازد از هر جوی جوهری</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>تر و خشکی اشک و رخسار من</p></div>
<div class="m2"><p>به کهگل براندود دیوار من</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>تن اینجا به پست جوین ساختن</p></div>
<div class="m2"><p>دل آنجا به گنجینه پرداختن</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به بازی نبردم جهان را به سر</p></div>
<div class="m2"><p>که شغلی دگر بود جز خواب و خور</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>نخفتم شبی شاد بر بستری</p></div>
<div class="m2"><p>که نگشادم آن شب ز دانش دری</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ضمیرم نه زن بلکه آتش‌زنست</p></div>
<div class="m2"><p>که مریم صفت بکر آبستنست</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>تقاضای آن شوی چون آیدش</p></div>
<div class="m2"><p>که از سنگ و آهن برون آیدش</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بدین دل‌فریبی سخن‌های بکر</p></div>
<div class="m2"><p>به سختی توان زادن از راه فکر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>سخن گفتن بکر جان سفتن است</p></div>
<div class="m2"><p>نه هر کس سزای سخن گفتن است</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>به دری سفالینه‌ای سفته گیر</p></div>
<div class="m2"><p>سرودی به گرمابه در گفته گیر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بیندیش از آن دشتهای فراخ</p></div>
<div class="m2"><p>کز آواز گردد گلو شاخ شاخ</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>چو بر سکه شاه زر میزنی</p></div>
<div class="m2"><p>چنان زن که گر بشکند نشکنی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>جهودی مسی را زراندود کرد</p></div>
<div class="m2"><p>دکان غارتیدن بدان سود کرد</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>نه انجیر شد نام هر میوه‌ای</p></div>
<div class="m2"><p>نه مثل زبیده است هر بیوه‌ای</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>دو هندو برآید ز هندوستان</p></div>
<div class="m2"><p>یکی دزد باشد دیگر پاسبان</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>من از آب این نقره تابناک</p></div>
<div class="m2"><p>فرو شستم آلودگیهای خاک</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ازین پیکر آنگه گشایم پرند</p></div>
<div class="m2"><p>که باشد رسیده چو نخل بلند</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چو در میوهٔ نارسیده رسی</p></div>
<div class="m2"><p>بجنبانیش نارسیده کسی</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>کند سوقیی سیب را خانه رس</p></div>
<div class="m2"><p>ولی خوش نیاید به دندان کس</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>شود نرم از افشردن انجیر خام</p></div>
<div class="m2"><p>ولی چون خوری خون برآید ز کام</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>شکوفه که بیگه نخندد به شاخ</p></div>
<div class="m2"><p>کند میوه را بر درختان فراخ</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>زمینی که دارد بر و بوم سست</p></div>
<div class="m2"><p>اساسی برو بست نتوان درست</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>به رونق توانم من این کار کرد</p></div>
<div class="m2"><p>به بی‌رونقی کار ناید ز مرد</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چو در دانه باشد تمنای سود</p></div>
<div class="m2"><p>کدیور در آید به کشت و درود</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>غله چون شود کاسد و کم بها</p></div>
<div class="m2"><p>کند برزگر کار کردن رها</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>ترنم شناسان دستان نیوش</p></div>
<div class="m2"><p>ز بانگ مغنی گرفتند گوش</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ضرورت شد این شغل را ساختن</p></div>
<div class="m2"><p>چنین نامه نغز پرداختن</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>که چون در کتابت شود جای گیر</p></div>
<div class="m2"><p>نیوشنده را زان بود ناگزیر</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>به نقشی که نزد کلان نیست خرد</p></div>
<div class="m2"><p>نمودم بدین داستان دستبرد</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>از این آشنا روی‌تر داستان</p></div>
<div class="m2"><p>خنیده نیامد بر راستان</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>دگر نامه‌ها را که جوئی نخست</p></div>
<div class="m2"><p>به جمهور ملت نباشد درست</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>نباشد چنین نامه تزویر خیز</p></div>
<div class="m2"><p>نبشته به چندین قلمهای تیز</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>به نیروی نوک چنین خامه‌ها</p></div>
<div class="m2"><p>شرف دارد این بر دگر نامه‌ها</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>از آن خسروی می که در جام اوست</p></div>
<div class="m2"><p>شرف نامهٔ خسروان نام اوست</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>سخنگوی پیشینه دانای طوس</p></div>
<div class="m2"><p>که آراست روی سخن چون عروس</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>در آن نامه کان گوهر سفته راند</p></div>
<div class="m2"><p>بسی گفتنیهای ناگفته ماند</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>اگر هر چه بشنیدی از باستان</p></div>
<div class="m2"><p>به گفتی دراز آمدی داستان</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>نگفت آنچه رغبت پذیرش نبود</p></div>
<div class="m2"><p>همان گفت کز وی گزیرش نبود</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>دگر از پی دوستان زله کرد</p></div>
<div class="m2"><p>که حلوا به تنها نشایست خورد</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>نظامی که در رشته گوهر کشید</p></div>
<div class="m2"><p>قلم دیده‌ها را قلم درکشید</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>بناسفته دری که در گنج یافت</p></div>
<div class="m2"><p>ترازوی خود را گهر سنج یافت</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>شرف‌نامه را فرخ آوازه کرد</p></div>
<div class="m2"><p>حدیث کهن را بدو تازه کرد</p></div></div>