---
title: >-
    بخش ۵۳ - جنگ پنجم اسکندر باروسیان
---
# بخش ۵۳ - جنگ پنجم اسکندر باروسیان

<div class="b" id="bn1"><div class="m1"><p>دگر روز کین طاق پیروزه رنگ</p></div>
<div class="m2"><p>برآورد یاقوت رخشان ز سنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الانی سواری چو غرنده شیر</p></div>
<div class="m2"><p>برآمد سیاه اژدهائی به زیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی گرز هفتاد مردی بدست</p></div>
<div class="m2"><p>که البرز را مغز درهم شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مبارز طلب کرد و می‌کشت مرد</p></div>
<div class="m2"><p>ز گردان گیتی برآورد گرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز رومی و ایرانی و خاوری</p></div>
<div class="m2"><p>بسی را فکند اندران داوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همان روسی افکن سوار دلیر</p></div>
<div class="m2"><p>برون آمد از پره چون نره شیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمان را زهی برزد از چرم خام</p></div>
<div class="m2"><p>بشست اندر آورد یک تیر تام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نیروی دست کمان گیر او</p></div>
<div class="m2"><p>بیفتاد الانی به یک تیر او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو ماسورهٔ هندباری به رنگ</p></div>
<div class="m2"><p>میان آکنیده به تیر خدنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دگر ره یکی روسی گربه چشم</p></div>
<div class="m2"><p>چو شیران به ابرو درآورده خشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سلاح آزمائی درآموخته</p></div>
<div class="m2"><p>بسی درع را پاره بردوخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درآمد به شمشیر بازی چو برق</p></div>
<div class="m2"><p>ز سر تا قدم زیر پولاد غرق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پذیره شده شورش جنگ را</p></div>
<div class="m2"><p>لحیفی برافکنده شبرنگ را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر چه دلی داشت چون خاره سنگ</p></div>
<div class="m2"><p>نبود آزموده خطرهای جنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به تنهائی آن پیشه ورزیده بود</p></div>
<div class="m2"><p>ز شمشیر دشمن نلرزیده بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو آن اژدها دم برانداختش</p></div>
<div class="m2"><p>شکاری زبون دید بشناختش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سلاحی بر او دید بیش از نبرد</p></div>
<div class="m2"><p>جل و جامه‌ای بهتر از اسب و مرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به یک ضربتش جان ز تن درکشید</p></div>
<div class="m2"><p>به جل برقعش برقع اندر کشید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دگر روسیی بست بر کین کمر</p></div>
<div class="m2"><p>همان رفت با او که با آن دگر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دلیر دگر جنگ را ساز کرد</p></div>
<div class="m2"><p>به تیری دگر جان ازو باز کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بهر تیر کز شست او شد روان</p></div>
<div class="m2"><p>به پهلو درآمد یکی پهلوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به ده چوبهٔ تیر آن سوار بهی</p></div>
<div class="m2"><p>زده پهلوان کرد میدان تهی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دگر باره پنهان ز بینندگان</p></div>
<div class="m2"><p>بیامد بجای نشینندگان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چنین چند روز آن نبرده سوار</p></div>
<div class="m2"><p>به پوشیدگی حرب کرد آشکار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نبد هیچکس را دگر یارگی</p></div>
<div class="m2"><p>که با او برون افکند بارگی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به جایی رسیدند کر بیم تیغ</p></div>
<div class="m2"><p>پراکندگیشان درآمد چو میغ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شکیبی به ناموس می‌ساختند</p></div>
<div class="m2"><p>خیالی به نیرنگ می‌باختند</p></div></div>