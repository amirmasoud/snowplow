---
title: >-
    بخش ۵۲ - جنگ چهارم اسکندر با روسیان
---
# بخش ۵۲ - جنگ چهارم اسکندر با روسیان

<div class="b" id="bn1"><div class="m1"><p>چو خورشید برزد سر از سبز میل</p></div>
<div class="m2"><p>فرو شست گردون قبا را ز نیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دگر باره شیران نمودند شور</p></div>
<div class="m2"><p>ز گوران همه دشت کردند گور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به غلغل درآمد جرس با درای</p></div>
<div class="m2"><p>بجوشید خون از دم کرنای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز فریاد شیپور و آواز کوس</p></div>
<div class="m2"><p>پدید آمد از سرخ گل سندروس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همان جودره سوی میدان شتافت</p></div>
<div class="m2"><p>که در خود یکی ذره سستی نیافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر باره هندی چو شیر سیاه</p></div>
<div class="m2"><p>درافکند ختلی به ناوردگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی چابکی کرد با جودره</p></div>
<div class="m2"><p>نمی‌رفت بر کار زخمی سره</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم آخر در ابرو یکی چین فکند</p></div>
<div class="m2"><p>سر جودره بر سر زین فکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برآورد از افکندنش کام خویش</p></div>
<div class="m2"><p>سپردش به نعل ره انجام خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلیرانه می‌گشت و می‌خواست مرد</p></div>
<div class="m2"><p>تهی کرد جای از بسی هم نبرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی نامور بود طرطوس نام</p></div>
<div class="m2"><p>به مردی درآورده در روس نام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو سرخ اژدهائی به پیچندگی</p></div>
<div class="m2"><p>همه بر هلاکش بسیچندگی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سوی هندی آمد چو سیلی به جوش</p></div>
<div class="m2"><p>که از کوه در پستی آرد خروش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در آن داوریهای بیگانگی</p></div>
<div class="m2"><p>نمودند بسیار مردانگی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرانجام روسی یکی حمله کرد</p></div>
<div class="m2"><p>کزان عود هندی برآورد گرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بپرداخت از خونش اندام را</p></div>
<div class="m2"><p>چو می‌ریخت بر سنگ زد جام را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز سر ترگ برداشت گفتا منم</p></div>
<div class="m2"><p>هژبری کزین گونه شیر افکنم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا مادر من که طرطوس خواند</p></div>
<div class="m2"><p>به روسی زبان رستم روس خواند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کسی کو زند بر من ابرو گره</p></div>
<div class="m2"><p>کفن به که پوشد به جای زره</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز میدان نخواهم شدن باز جای</p></div>
<div class="m2"><p>مگر لشگری را درارم ز پای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شه از کشتن هندی و زخم روس</p></div>
<div class="m2"><p>بپیچید بر خود چو زلف عروس</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بران بود کارد عنان سوی جنگ</p></div>
<div class="m2"><p>دگر باره در عزمش آمد درنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چپ و راست می‌دید تا از سپاه</p></div>
<div class="m2"><p>که خواهد شد از کینه ور کینه خواه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روان کرد مرکب شتابنده‌ای</p></div>
<div class="m2"><p>ز پولاد چین برق تابنده‌ای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همایون سواری چو غرنده شیر</p></div>
<div class="m2"><p>توانا و چابک عنان و دلیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنان غرق در آهن اندام او</p></div>
<div class="m2"><p>که بی‌دانه جز بر نفس کام او</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به جولان زدن سرفرازی کنان</p></div>
<div class="m2"><p>به شمشیر چون برق بازی کنان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از آن چابکیها که می‌کرد چست</p></div>
<div class="m2"><p>برابر شده دست بدخواه سست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بران روسی افکند مرکب چو باد</p></div>
<div class="m2"><p>به تیغ آزمائی بغل برگشاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنان زد که از تیغ گردن زنش</p></div>
<div class="m2"><p>سر دشمن افتاد در دامنش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از آن شیر دل‌تر سواری دگر</p></div>
<div class="m2"><p>درآمد به پرخاش چون شیر نر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به زخمی دگر هم سرافکنده شد</p></div>
<div class="m2"><p>چنین تا سری چند برکنده شد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فزون از چهل روسی کوه پشت</p></div>
<div class="m2"><p>به آسانی آن شیر جنگی بکشت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بهر سو که می‌راند شبرنگ را</p></div>
<div class="m2"><p>ز خون لعل کرد آهنش سنگ را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به هر حمله کانگیخت از هر دری</p></div>
<div class="m2"><p>فرو ریخت از روسیان لشگری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو بر خون شتابنده شد نیش او</p></div>
<div class="m2"><p>نیامد کس از بیم در پیش او</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یکی حمله نیک را ساز داد</p></div>
<div class="m2"><p>عنان را به چابک عنان باز داد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در آن حمله کان کوه آهسته کرد</p></div>
<div class="m2"><p>صد افکند و صد کشت و صد خسته کرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شه از شیر مردیش حیران شده</p></div>
<div class="m2"><p>بران دست و تیغ آفرین خوان شده</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدین گونه می‌کرد پیگارها</p></div>
<div class="m2"><p>همی ریخت آتش در آن خارها</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فلک تا نشد بر سرش مشگسای</p></div>
<div class="m2"><p>نیامد ز آوردگه باز جای</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو در برقع کوه رفت آفتاب</p></div>
<div class="m2"><p>سر روز روشن درآمد به خواب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شب تیره چون اژدهای سیاه</p></div>
<div class="m2"><p>ز ماهی برآورد سر سوی ماه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سیه کرد بر شیروان راه را</p></div>
<div class="m2"><p>فرو برد چون اژدها ماه را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سوار شبیخون بر از تاختن</p></div>
<div class="m2"><p>برآسود و آمد به شب ساختن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به تاریکی شب چنان شد نهان</p></div>
<div class="m2"><p>که نشناختن هیچکس در جهان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شه از مردی آن سوار دلیر</p></div>
<div class="m2"><p>گمان برد کان شیر دل بود شیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>در اندیشه می‌گفت کان شهریار</p></div>
<div class="m2"><p>که امروز کرد آنچنان کارزار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دریغا اگر روی او دیدمی</p></div>
<div class="m2"><p>صدش گنج سربسته بخشیدمی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>قوی بازوئی کرد و خلقی بکشت</p></div>
<div class="m2"><p>چو بازوی خویشم قوی کرد پشت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نبود آدمی بود شیر عرین</p></div>
<div class="m2"><p>که بادا بران شیر مرد آفرین</p></div></div>