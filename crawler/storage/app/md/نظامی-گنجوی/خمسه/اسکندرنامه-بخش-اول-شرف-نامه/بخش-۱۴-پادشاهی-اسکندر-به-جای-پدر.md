---
title: >-
    بخش ۱۴ - پادشاهی اسکندر به جای پدر
---
# بخش ۱۴ - پادشاهی اسکندر به جای پدر

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی از خود رهائیم ده</p></div>
<div class="m2"><p>ز رخشنده می روشنائیم ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میی کو ز محنت رهائی دهد</p></div>
<div class="m2"><p>به آزردگان مومیائی دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخن سنجی آمد ترازو به دست</p></div>
<div class="m2"><p>درست زر اندود را می‌شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تصرف در آن سکه بگذاشتم</p></div>
<div class="m2"><p>کزان سیم در زر خبر داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر انگشت من حرف‌گیری کند</p></div>
<div class="m2"><p>ندانم کسی کو دبیری کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ولی تا قوی دست شد پشت من</p></div>
<div class="m2"><p>نشد حرف گیر کس انگشت من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبینم به بدخواهی اندر کسی</p></div>
<div class="m2"><p>که من نیز بدخواه دارم بسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ره من همه زهر نوشیدنست</p></div>
<div class="m2"><p>هنر جستم و عیب پوشیدنست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدان ره که خود را نمودم نخست</p></div>
<div class="m2"><p>قدم داشتم تابه آخر درست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دباغت چنان دادم این چرم را</p></div>
<div class="m2"><p>که برتابد آسیب و آزرم را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان خواهم از پاک پروردگار</p></div>
<div class="m2"><p>کزین ره نگردم سرانجام کار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گزارای نقش گزارش پذیر</p></div>
<div class="m2"><p>که نقش از گزارش ندارد گزیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنین نقش بندد که چون شاه روم</p></div>
<div class="m2"><p>به ملک جهان نقش برزد به موم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ولایت ز عدلش پر آوازه گشت</p></div>
<div class="m2"><p>بدو تاج و تخت پدر تازه گشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همان رسمها کز پدر دیده بود</p></div>
<div class="m2"><p>نمود آنچه رایش پسندیده بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همان عهد دیرینه برجای داشت</p></div>
<div class="m2"><p>علمهای پیشینه بر پای داشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به دارا همان گنج زر می‌سپرد</p></div>
<div class="m2"><p>بران عهد پیشینه پی می‌فشرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز فرمانبران ملک فیلقوس</p></div>
<div class="m2"><p>نشد کس در آن شغل با وی شموش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که بود از پدر دوست انگیزتر</p></div>
<div class="m2"><p>به دشمن کشی تیغ او تیزتر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنان شد که با زور بازوی او</p></div>
<div class="m2"><p>نچربید کس در ترازوی او</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو در زور پیچیدی اندام را</p></div>
<div class="m2"><p>گره برزدی گوش ضرغام را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کباده ز چرخه کمان ساختی</p></div>
<div class="m2"><p>بهر گشتنی تیری انداختی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به نخجیر گه شیری کردی شکار</p></div>
<div class="m2"><p>ز گور و گوزنش نرفتی شمار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ربود از دلیران تواناتری</p></div>
<div class="m2"><p>سر زیرکان شد به داناتری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو خطش قلم راند بر آفتاب</p></div>
<div class="m2"><p>یکی جدول انگیخت از مشک ناب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فلک زان خط جدول انگیخته</p></div>
<div class="m2"><p>سواد حبش را ورق ریخته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حساب جهانگیری آورد پیش</p></div>
<div class="m2"><p>جهان را زبون دید در دست خویش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همش هوش دل بود و هم زوردست</p></div>
<div class="m2"><p>بدین هر دو بر تخت شاید نشست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به هر کاری کو جست نام آوری</p></div>
<div class="m2"><p>در آن کار دادش فلک یاوری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همه روم از آن سرو نوخاسته</p></div>
<div class="m2"><p>به ریحان سرسبزی آراسته</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ازو بسته نقشی به هر خانه‌ای</p></div>
<div class="m2"><p>رسیده به هر کشور افسانه‌ای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گهی راز با انجمن می‌نهاد</p></div>
<div class="m2"><p>گه از راز انجم گره می‌گشاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به انبوه می با جوانان گرفت</p></div>
<div class="m2"><p>به خلوت پی کار دانان گرفت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نه آن کرد با مردم از مردمی</p></div>
<div class="m2"><p>که آید در اندیشهٔ آدمی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به آزردن کس نیاورد رای</p></div>
<div class="m2"><p>برون از خط عدل ننهاد پای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به بازارگانان رها کرد باج</p></div>
<div class="m2"><p>نجست از مقیمان شهری خراج</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز دیوان دهقان قلم برگرفت</p></div>
<div class="m2"><p>به بی‌مایگان هم درم درگرفت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>عمارت همی کرد و زر می‌فشاند</p></div>
<div class="m2"><p>همه خار می‌کند و گل می‌نشاند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به هر ناحیت نام داغش کشید</p></div>
<div class="m2"><p>به مصر و حبس بوی باغش کشید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گشاده دو دستش چو روشن درخش</p></div>
<div class="m2"><p>یکی تیغ زن شد یکی تاج بخش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ترازو خود آن به که دارد دو سر</p></div>
<div class="m2"><p>یکی جای آهن یکی جای زر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هر آن کار اقبال را درخورست</p></div>
<div class="m2"><p>به آهن چو آهن به زر چون زرست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنان دادگر شد که هر مرز و بوم</p></div>
<div class="m2"><p>زدی داستان کای خوشا مرز روم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ارسطو که دستور درگاه بود</p></div>
<div class="m2"><p>به هر نیک و بد محرم شاه بود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سکندر به تدبیر دانا وزیر</p></div>
<div class="m2"><p>به کم روزگاری شد آفاق گیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وزیری چنین شهریاری چنان</p></div>
<div class="m2"><p>جهان چون نگیرد قراری چنان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همه کار شاهان گیتی نکوه</p></div>
<div class="m2"><p>ز رای وزیران پذیرد شکوه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ملک شاه و محمود و نوشیروان</p></div>
<div class="m2"><p>که بردند گوی از همه خسروان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>پذیرای پند وزیران شدند</p></div>
<div class="m2"><p>که از جملهٔ دور گیران شدند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شه ما که بدخواه را کرد خرد</p></div>
<div class="m2"><p>برای وزیر از جهان گوی برد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مرا و تو را گه شود پای سست</p></div>
<div class="m2"><p>تن شاه باید که ماند درست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مبادا که شه را رسد پای لغز</p></div>
<div class="m2"><p>که گردد سر ملک شوریده مغز</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو باشد کند چشم بد بازیی</p></div>
<div class="m2"><p>کند دیو بافتنه دم سازیی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جهان دادخواهست و شه دادگیر</p></div>
<div class="m2"><p>ز داور نباشد جهان را گزیر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>جهان را به صاحب جهان نور باد</p></div>
<div class="m2"><p>وزین داوری چشم بد دور باد</p></div></div>