---
title: >-
    بخش ۲۱ - شتافتن اسکندر به جنگ دارا
---
# بخش ۲۱ - شتافتن اسکندر به جنگ دارا

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی آن راوق روح بخش</p></div>
<div class="m2"><p>به کام دلم درفشان چون درخش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من او را خورم دل‌فروزی بود</p></div>
<div class="m2"><p>مرا او خورد خاک روزی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه نیکو متاعیست کار آگهی</p></div>
<div class="m2"><p>کزین قد عالم مبادا تهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عالم کسی سر برآرد بلند</p></div>
<div class="m2"><p>که در کار عالم بود هوشمند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بازی نپیماید این راه را</p></div>
<div class="m2"><p>نگهدارد از دزد بنگاه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیندازد آن آلت از بار خویش</p></div>
<div class="m2"><p>کزو روزی آسان کند کار خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میفکن کول گر چه خوار آیدت</p></div>
<div class="m2"><p>که هنگام سرما به کار آیدت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی بر گریوه ز سرما بمرد</p></div>
<div class="m2"><p>که از کاهلی جامه با خود نبرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گزارندهٔ شرح شاهنشهی</p></div>
<div class="m2"><p>چنین داد پرسنده را آگهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که دارا چو لشگر به ارمن کشید</p></div>
<div class="m2"><p>تو گفتی که آمد قیامت پدید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نبود آگه اسکندر از کار او</p></div>
<div class="m2"><p>که آرد قیامت به پیکار او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رسیدند زنهاریان خیل خیل</p></div>
<div class="m2"><p>که طوفان به دریا درآورد سیل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شبیخون دارا درآمد ز راه</p></div>
<div class="m2"><p>ز پولاد پوشان زمین شد سیاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پژوهنده‌ای گفت بدخواه مست</p></div>
<div class="m2"><p>شب و روز غافل شد آنجاکه هست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر او شاه اگر یک شبیخون کند</p></div>
<div class="m2"><p>ز ملکش همانا که بیرون کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سکندر بخندید و دادش جواب</p></div>
<div class="m2"><p>که پنهان نگیرد جهان آفتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ملک را به وقت عنان تافتن</p></div>
<div class="m2"><p>به دزدی نشاید ظفر یافتن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پژوهنده دیگر آغاز کرد</p></div>
<div class="m2"><p>که دارانه چندان سپه ساز کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که آن را شمردن توان درقیاس</p></div>
<div class="m2"><p>کسانیکه هستند لشگر شناس</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سکندر بدو گفت یک تیغ تیز</p></div>
<div class="m2"><p>کند پیه صد گاو را ریزریز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سپه را جوابی چنان ارجمند</p></div>
<div class="m2"><p>بلند آمد از شهریار بلند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خبر گرم‌تر شد همی هر زمان</p></div>
<div class="m2"><p>که آمد به روم اژدهائی دمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سکندر چو دانست کان تیغ میغ</p></div>
<div class="m2"><p>به تندر برآرد همی برق تیغ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فرستاد تا لشگر از هر دیار</p></div>
<div class="m2"><p>روانه شود بر در شهریار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز مصر و ز افرنجه و روم و روس</p></div>
<div class="m2"><p>شد آراسته لشگری چون عروس</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو انبوه شد لشگر بیکران</p></div>
<div class="m2"><p>عدد خواست از نام نام‌آوران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خبر داد عارض که سیصد هزار</p></div>
<div class="m2"><p>برآمد دلیران مفرد سوار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو شد ساخته کار لشگر تمام</p></div>
<div class="m2"><p>یکی انجمن ساخت بیرود و جام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نشستند بیدار مغزان روم</p></div>
<div class="m2"><p>به مهر ملک نرم کردند موم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شه از کار دارا و پیگار او</p></div>
<div class="m2"><p>سخن راند و پیچید در کار او</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چنین گفت کاین نامور شهریار</p></div>
<div class="m2"><p>کمر بست بر جستن کارزار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چه سازیم تدبیرش از صلح و جنگ</p></div>
<div class="m2"><p>که آمد به آویختن کار تنگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر برنیاریم تیغ از نیام</p></div>
<div class="m2"><p>به مردی ز ما برنیارند نام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وگر تاج بستانم از تاجور</p></div>
<div class="m2"><p>به بیداد خود بسته باشم کمر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کیان را کی از ملک بیرون کنم</p></div>
<div class="m2"><p>من این رهزنی با کیان چون کنم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بترسم که اختر بدین طیرگی</p></div>
<div class="m2"><p>بداندیش ما را دهد چیرگی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چه تدبیر باشد در این رسم و راه</p></div>
<div class="m2"><p>کزو کار بر ما نگردد تباه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به اندیشه خوب و رای صواب</p></div>
<div class="m2"><p>پدید آورید این سخن را جواب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جهان‌دیده پیران بیدار هوش</p></div>
<div class="m2"><p>چو گفتار گوینده کردند گوش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به پاسخ گشادند یکسر زبان</p></div>
<div class="m2"><p>دعا تازه کردند بر مرزبان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که سرسبز باد این همایون درخت</p></div>
<div class="m2"><p>که شاخش بلند است و نیروش سخت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به تاج و به تختش جهان تازه باد</p></div>
<div class="m2"><p>سر خصم او تاج دروازه باد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همه رای او هست چون او درست</p></div>
<div class="m2"><p>درستی چه باید ز ما باز جست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ولیکن ز فرمان او نگذریم</p></div>
<div class="m2"><p>به جز راه فرمان او نسپریم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چنان در دل آید جهان دیده را</p></div>
<div class="m2"><p>همان زیرکان پسندیده را</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>که چون کینه ور شد دل کینه خواه</p></div>
<div class="m2"><p>همه خار وحشت برآمد ز راه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تو نیز آتش کینه را برفروز</p></div>
<div class="m2"><p>که فرخ بود آتش کینه سوز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>توسرو نوی خصم بید کهن</p></div>
<div class="m2"><p>کجا سر کشد بید با سرو بن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کهن باغ را وقت نو کردنست</p></div>
<div class="m2"><p>نوان در حساب درو کردنست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به دیبای این دولت تازه عهد</p></div>
<div class="m2"><p>عروس جهان را برآرای مهد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بداندیش تو هست بیدادگر</p></div>
<div class="m2"><p>بپیچد رعیت ز بیداد سر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چه باید هراسیدنت زان کسی</p></div>
<div class="m2"><p>که دارد هم از خانه دشمن بسی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>قلم درکش آیین بیداد را</p></div>
<div class="m2"><p>کفایت کن از خلق فریاد را</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ز خصم تو چون مملکت گشت سیر</p></div>
<div class="m2"><p>به خصم افکنی پای در نه دلیر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تنوری چنین گرم در بند نان</p></div>
<div class="m2"><p>ره انجام را گرم‌تر کن عنان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کجا شاه را پای ما را سر است</p></div>
<div class="m2"><p>دلی کو کز این داوری بر در است</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تمنای شه را که بر هم زند</p></div>
<div class="m2"><p>که را زهره باشد که این دم زند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بر این ختم شد رخصت رهنمون</p></div>
<div class="m2"><p>که شه پیش دستی نیارد به خون</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نگهدارد آزرم تخت کیان</p></div>
<div class="m2"><p>به خونریزی اول نبندد میان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سکندر چو در حکم آن داوری</p></div>
<div class="m2"><p>ز لشگر کشان یافت آن یاوری</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به دستوری رخصت راستان</p></div>
<div class="m2"><p>به لشگر کشی گشت هم‌داستان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>یکی روز کز گردش روزگار</p></div>
<div class="m2"><p>بدست آمدش طالعی اختیار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بفالی همایون بترتیب راه</p></div>
<div class="m2"><p>بفرمود کز جای جنبد سپاه</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>عنان تاب شد شاه پیروز جنگ</p></div>
<div class="m2"><p>میان بسته بر کین بدخواه تنگ</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز شمشیر پولاد چون شیر مست</p></div>
<div class="m2"><p>به کشور گشائی کلیدی بدست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سپاهی چو زنبور با نیشتر</p></div>
<div class="m2"><p>ز غوعای زنبور هم بیشتر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نشان جسته بود از درفش بلند</p></div>
<div class="m2"><p>که ماند از فریدون فیرزومند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به وقتی که آن وقت سازنده بود</p></div>
<div class="m2"><p>فلک دوستان را نوازنده بود</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بسی برتر از کاویانی درفش</p></div>
<div class="m2"><p>به منجوق برزد پرندی به نقش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>صنوبر ستونی به پنجه ارش</p></div>
<div class="m2"><p>به پیراستن یافته پرورش</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>برو اژدها پیکری از حریر</p></div>
<div class="m2"><p>که بیننده را زو برآمد نفیر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>زده بر سر از جعد پرچم کلاه</p></div>
<div class="m2"><p>چو بر کله کوه ابر سیاه</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به فرسنگها بود پیدا ز دور</p></div>
<div class="m2"><p>عقابی سیه پر و بالش ز نور</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>شد آن اژدها با چنان لشگری</p></div>
<div class="m2"><p>به سر بر چنان اژدها پیکری</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>جهان کرد از آشوب خود دردناک</p></div>
<div class="m2"><p>ز بهر چه؟ از بهر یک مشت خاک</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>از این گربه گون خاک تا چندچند</p></div>
<div class="m2"><p>به شیری توان کردنش گرگ بند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>جهان یک نواله ست پیچیده سر</p></div>
<div class="m2"><p>در او گاه حلوا بود گه جگر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>فلک در بلندی زمین در مغاک</p></div>
<div class="m2"><p>یکی طشت خون شد یکی طشت خاک</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نبشته برین هر دو آلوده طشت</p></div>
<div class="m2"><p>چو خون سیاوش بسی سرگذشت</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>زمین گر بضاعت برون آورد</p></div>
<div class="m2"><p>همه خاک در زیر خون آورد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>نیفتد درین طشت فریاد کس</p></div>
<div class="m2"><p>که بر بسته شد راه فریادرس</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چو فریاد را در گلو بست راه</p></div>
<div class="m2"><p>گلو بسته به مرد فریاد خواه</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>به ار پرده خود حصاری کنی</p></div>
<div class="m2"><p>به خاموشی خویش یاری کنی</p></div></div>