---
title: >-
    بخش ۱۷ - باز گشتن اسکندر از جنگ زنگ با فیروزی
---
# بخش ۱۷ - باز گشتن اسکندر از جنگ زنگ با فیروزی

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی از می مرا مست کن</p></div>
<div class="m2"><p>چو می در دهی نقل بر دست کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن می که دل را برو خوش کنم</p></div>
<div class="m2"><p>به دوزخ درش طلق آتش کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برومند باد آن همایون درخت</p></div>
<div class="m2"><p>که در سایه او توان برد رخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه از میوه آرایش خوان دهد</p></div>
<div class="m2"><p>گه از سایه آسایش جان دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به میوه رسیده بهاری چنین</p></div>
<div class="m2"><p>ز رونق میفتاد کاری چنین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شد بارور میوه‌دار جوان</p></div>
<div class="m2"><p>به دست تبر دادنش چون توان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمستان برون رفت و آمد بهار</p></div>
<div class="m2"><p>برآورده سبزه سر از جویبار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دگر باره سرسبز شد خاک خشک</p></div>
<div class="m2"><p>بنفشه برآمیخت عنبر به مشک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به عنبر خری نرگس خوابناک</p></div>
<div class="m2"><p>چو کافورتر سر برون زد ز خاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گشادم من از قفل گنجینه بند</p></div>
<div class="m2"><p>به صحرا علم برکشیدم بلند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نهان پیکر آن هاتف سبز پوش</p></div>
<div class="m2"><p>که خواند سراینده آنرا سروش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به آواز پوشیدگان گفت خیز</p></div>
<div class="m2"><p>گزارش کن از خاطر گنج ریز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که چون رومی از زنگی آنکین کشید</p></div>
<div class="m2"><p>سکندر کجا رخش در زین کشید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گزارنده داستان دری</p></div>
<div class="m2"><p>چنین داد نظم گزارش گری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که چون فرخی شاه را گشت جفت</p></div>
<div class="m2"><p>چو گلنار خندید و چون گل شکفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درگنج بگشاد بر گنج خواه</p></div>
<div class="m2"><p>توانگر شد از گنج و گوهر سپاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برآسود یک هفته بر جای جنگ</p></div>
<div class="m2"><p>به یاقوت می رنگ داد آذرنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو سقای باران و فراش باد</p></div>
<div class="m2"><p>زدند آب و رفتند ره بامداد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شد از راه او گرد برخاسته</p></div>
<div class="m2"><p>که بی‌گرد به راه آراسته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو بی گرد شد راه را کرد راه</p></div>
<div class="m2"><p>درآمد به زین شاه گیتی پناه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روار و زنان نای زرین زدند</p></div>
<div class="m2"><p>سراپرده بر پشت پروین زدند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز دریای افرنجه تا رود نیل</p></div>
<div class="m2"><p>بجوش آمد از بانگ طبل رحیل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دراینده هر سو درای شتر</p></div>
<div class="m2"><p>ز بانگ تهی مغز را کرد پر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دهان جلاجل به هرای زر</p></div>
<div class="m2"><p>ز شور جرس گوشها کرده کر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به موکب روان لشگر از هر کنار</p></div>
<div class="m2"><p>نه چندان که داند کس آنرا شمار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جهاندار در موکب خاص خویش</p></div>
<div class="m2"><p>خرامنده بر کبک رقاص خویش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو لختی زمین ز آن طرف در نوشت</p></div>
<div class="m2"><p>ز پهلوی وادی درآمد به دشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز بس رایت انگیزی سرخ و زرد</p></div>
<div class="m2"><p>مقرنس شده گنبد لاجورد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز صحرا غنیمت برآورده کوه</p></div>
<div class="m2"><p>ز گوهر کشیدن هیونان ستوه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز بس گنج آگنده بر پشت پیل</p></div>
<div class="m2"><p>به صد جای پل بسته بر رود نیل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدین فرخی شاه فیروزمند</p></div>
<div class="m2"><p>برافراخته سر به چرخ بلند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به مصرآمد و مصریان را نواخت</p></div>
<div class="m2"><p>به آئین خود کار آن شهر ساخت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وز آنجا روان شد به دریا کنار</p></div>
<div class="m2"><p>پذیرفت یک چندی آنجا قرار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به هر منزلی کو علم برکشید</p></div>
<div class="m2"><p>در آن منزل آمد عمارت پدید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به گنج و به فرمان در آن ریگ بوم</p></div>
<div class="m2"><p>عمارت بسی کرد بر رسم روم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر آبادی راه می‌برد رنج</p></div>
<div class="m2"><p>بر آن ریگ می‌ریخت چون ریگ گنج</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نخستین عمارت به دریا کنار</p></div>
<div class="m2"><p>بنا کرد شهری چو خرم بهار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به آبادی و روشنی چون بهشت</p></div>
<div class="m2"><p>همش جای بازار و هم جای کشت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به اسکندر آن شهر چون شد تمام</p></div>
<div class="m2"><p>هم اسکندریه‌ش نهادند نام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو پرداخت آن نغز بنیاد را</p></div>
<div class="m2"><p>که مانند شد مصر و بغداد را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به یونان شدن گشت عزمش درست</p></div>
<div class="m2"><p>که آن‌جا رود مرد کاید نخست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز دریا گذر کرد و آمد به روم</p></div>
<div class="m2"><p>جهان نرم در زیر مهرش چو موم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بدان موم چون رغبتش خاستی</p></div>
<div class="m2"><p>بکردی ازو هر چه می‌خواستی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بزرگان روم آفرین خوان شدند</p></div>
<div class="m2"><p>بر آن گوهری گوهرافشان شدند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همه شهر یونان بیاراستند</p></div>
<div class="m2"><p>که دیدند ازو آنچه می‌خواستند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نشاندند مطرب فشاندند مال</p></div>
<div class="m2"><p>که آمد چنان بازیی در خیال</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مخالف شکن شاه پیروز بخت</p></div>
<div class="m2"><p>به فیروز فالی برآمد به تخت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز فیروزی دولت کامگار</p></div>
<div class="m2"><p>نشاط نو انگیخت در روزگار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بسی ارمغانی ز تاراج زنگ</p></div>
<div class="m2"><p>به هر سو فرستاد بی وزن و سنگ</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز گنجی که او را فرستاد دهر</p></div>
<div class="m2"><p>به هر گنجدانی فرستاد بهر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو نوبت به سربخش دارا رسید</p></div>
<div class="m2"><p>شتر بار زر تا بخارا رسید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گزین کرد مردی به فرهنگ ورای</p></div>
<div class="m2"><p>که آیین آن خدمت آرد بجای</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گزید از غنیمت طرایف بسی</p></div>
<div class="m2"><p>کز آن سان نبیند طرایف کسی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گرانمایه‌هایی که باشد غریب</p></div>
<div class="m2"><p>ز مرکوب و گوهر ز دیبا و طیب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>برون از طبقهای پرزر خشک</p></div>
<div class="m2"><p>به صندوق عنبر به خروار مشک</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>یکی خرمن از سیم بگداخته</p></div>
<div class="m2"><p>یکی خانه کافور ناساخته</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>زعود گره بارها بسته تنگ</p></div>
<div class="m2"><p>که هر بار از او بود صد من به سنگ</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>مرصع بسی تیغ گوهر نگار</p></div>
<div class="m2"><p>نمطهای زرافهٔ آبدار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کنیزان چابک غلامان چست</p></div>
<div class="m2"><p>به هنگام خدمتگری تندرست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همان تختهای مکلل ز عاج</p></div>
<div class="m2"><p>به گوهر بر آموده با طوق و تاج</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>اسیران زنجیر بر پا و دست</p></div>
<div class="m2"><p>به بالا و پهنا چو پیلان مست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز گوش بریده شتر بارها</p></div>
<div class="m2"><p>ز سرهای پر کاه خروارها</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ز پیلان پیکار ده زنده پیل</p></div>
<div class="m2"><p>گه رزم جوشنده چون رود نیل</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بدین سان گرانمایهای سره</p></div>
<div class="m2"><p>فرستاد با قاصدی یکسره</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو آمد فرستادهٔ راه سنج</p></div>
<div class="m2"><p>به دارا سپرد آن گرانمایه گنج</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>شکوهید دارا ز نزلی چنان</p></div>
<div class="m2"><p>حسد را برو تیزتر شد عنان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>پذیرفت گنجینه بی قیاس</p></div>
<div class="m2"><p>پذیرفته را نامد از وی سپاس</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نه بر جای خود پاسخی ساز کرد</p></div>
<div class="m2"><p>در کین پوشیده را باز کرد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>فرستاده آن پاسخ سرسری</p></div>
<div class="m2"><p>نپوشید بر رای اسکندری</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سکندر شد آزرده از کار او</p></div>
<div class="m2"><p>نهانی همی داشت آزار او</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ز پیروزی دولت و جاه خویش</p></div>
<div class="m2"><p>نبودش سرکین بدخواه خویش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز هر سو خبر ترکتازی نمود</p></div>
<div class="m2"><p>که رومی به زنگی چه بازی نمود</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ز هر کشوری قاصدان تاختند</p></div>
<div class="m2"><p>بدین چیرگی تهنیت ساختند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>در طعنه بر رومیان بسته شد</p></div>
<div class="m2"><p>همان رومی از بددلی رسته شد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>زمانه چو عاجز نوازی کند</p></div>
<div class="m2"><p>به تند اژدها مور بازی کند</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>در این آسیا دانه بینی بسی</p></div>
<div class="m2"><p>به نوبت درآس افکند هرکسی</p></div></div>