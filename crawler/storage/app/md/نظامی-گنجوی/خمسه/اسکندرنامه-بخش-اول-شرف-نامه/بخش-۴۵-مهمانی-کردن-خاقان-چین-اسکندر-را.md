---
title: >-
    بخش ۴۵ - مهمانی کردن خاقان چین اسکندر را
---
# بخش ۴۵ - مهمانی کردن خاقان چین اسکندر را

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی آزاد کن گردنم</p></div>
<div class="m2"><p>سرشک قدح ریز در دامنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرشگی که از صرف پالودگی</p></div>
<div class="m2"><p>فرو شوید از دامن آلودگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکن ترکی ای ترک چینی نگار</p></div>
<div class="m2"><p>بیا ساعتی چین در ابرو میار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم را به دلداریی شاد کن</p></div>
<div class="m2"><p>ز بند غم امروزم آزاد کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر دخل خاقان چین آن توست</p></div>
<div class="m2"><p>مکن خرج را رود، باران توست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخور چیزی از مال و چیزی بده</p></div>
<div class="m2"><p>ز بهر کسان نیز چیزی بنه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مخور جمله ترسم که دیر ایستی</p></div>
<div class="m2"><p>به پیرایه سر بد بود نیستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خرج بر خود چنان در مبند</p></div>
<div class="m2"><p>که گردی ز ناخوردگی دردمند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان نیز یکسر مپرداز گنج</p></div>
<div class="m2"><p>گه آیی ز بیهوده خواری به رنج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به اندازه‌ای کن بر انداز خویش</p></div>
<div class="m2"><p>که باشد میانه نه اندک نه بیش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو رشته ز سوزن قوی‌تر کنی</p></div>
<div class="m2"><p>بسا چشم سوزن که در سر کنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سخن را گزارشگر نقشبند</p></div>
<div class="m2"><p>چنین نقش بر زد به چینی پرند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کز آوازهٔ شه جهان گشت پر</p></div>
<div class="m2"><p>که چین را در آمود دامن به در</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شب و روز خاقان در آن کرد صرف</p></div>
<div class="m2"><p>که شه را دهد پایمردی شگرف</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ملوکانه مهمانیی سازدش</p></div>
<div class="m2"><p>جهان در سم مرکب اندازدش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کند پیشکشهای شاهانه پیش</p></div>
<div class="m2"><p>به اندازهٔ پایهٔ کار خویش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی روز کرد از جهان اختیار</p></div>
<div class="m2"><p>فروزنده چون طالع شهریار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برآراست بزمی چو روشن بهشت</p></div>
<div class="m2"><p>که دندان شیران بر آن شیره هشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنان از می و میوهٔ خوشگوار</p></div>
<div class="m2"><p>برآراست مهمانیی شاهوار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که هیچ آرزوئی به عالم نبود</p></div>
<div class="m2"><p>که یک یک بران خوان فراهم نبود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گذشت از خورشهای چینی سرشت</p></div>
<div class="m2"><p>که رضوان ندید آنچنان در بهشت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز شکر بسی پخته حلوای نغز</p></div>
<div class="m2"><p>به بادام شیرینش آکنده مغز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>طرائف به زانسان که دنیا پرست</p></div>
<div class="m2"><p>یکی آورد زان به عمری به دست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جواهر نه چندان که جوهر شناس</p></div>
<div class="m2"><p>کند نیم آن را به سالی قیاس</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو شد خانهٔ گنج پرداخته</p></div>
<div class="m2"><p>بدانگونه مهمانیی ساخته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شه ترک با شهرگان دیار</p></div>
<div class="m2"><p>به خواهشگری شد بر شهریار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زمین داد بوسه به آیین پیش</p></div>
<div class="m2"><p>فزود از زمین بوس او قدر خویش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نیایش کنان گفت اگر بخت شاه</p></div>
<div class="m2"><p>کند بر سر تخت این بنده راه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سرش را به افسر گرامی کند</p></div>
<div class="m2"><p>بدین سر بزرگیش نامی کند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پذیرفت شه خواهش گرم او</p></div>
<div class="m2"><p>به رفتن نگه داشت آزرم او</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شه و لشگر شه به یکبارگی</p></div>
<div class="m2"><p>بران خوان شدند از سر بارگی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زمین از سر گنج بگشاد بند</p></div>
<div class="m2"><p>روا رو برآمد به چرخ بلند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سکندر چو بر خوان خاقان رسید</p></div>
<div class="m2"><p>پی خضر بر آب حیوان رسید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یکی تخت زر دید چون آفتاب</p></div>
<div class="m2"><p>درو چشمهٔ در چو دریای آب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به شادی بران تخت زرین نشست</p></div>
<div class="m2"><p>ز کافور و عنبر ترنجی بدست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جهانجوی فغفور بر دست راست</p></div>
<div class="m2"><p>به خدمت کمر بست و بر پای خاست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نوازش کنانش ملک پیش خواند</p></div>
<div class="m2"><p>ملک وار بر کرسی زر نشاند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دگر تاجداران به فرمان شاه</p></div>
<div class="m2"><p>به زانو نشستند در پیشگاه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بفرمود خاقان که آرند خورد</p></div>
<div class="m2"><p>ز خوانهای زرین شود خاک زرد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فرو ریخت شاهانه برگی فراخ</p></div>
<div class="m2"><p>چو برگ رز از برگ ریزان شاخ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دران آرزوگاه فرخار دیس</p></div>
<div class="m2"><p>نکرد آرزو با معامل مکیس</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بهشتی صفت هر چه درخواستند</p></div>
<div class="m2"><p>بران مائده خوان برآراستند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو خوردند هرگونه‌ای خوردها</p></div>
<div class="m2"><p>نمودند بر باده ناوردها</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نشاط می‌قرمزی ساختند</p></div>
<div class="m2"><p>بساطی هم از قرمز انداختند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نشسته به رامش ز هر کشوری</p></div>
<div class="m2"><p>غریب اوستادی و رامشگری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نوا ساز خنیاگران شگرف</p></div>
<div class="m2"><p>به قانون او زان برآورده حرف</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بریشم نوازان سغدی سرود</p></div>
<div class="m2"><p>به گردون برآورده آواز رود</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سرایندگان ره پهلوی</p></div>
<div class="m2"><p>ز بس نغمه داده نوا را نوی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>همان پای کوبان کشمیر زاد</p></div>
<div class="m2"><p>معلق زن از رقص چون دیو باد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز یونانیان ارغنون زن بسی</p></div>
<div class="m2"><p>که بردند هوش از دل هر کسی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کمر بسته رومی و چینی به هم</p></div>
<div class="m2"><p>برآورده از روم و از چین علم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در گنج بگشاد چیپال چین</p></div>
<div class="m2"><p>بپرداخت از گنج قارون زمین</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نخست از جواهر درآمد به کار</p></div>
<div class="m2"><p>ز دراعه و درع گوهر نگار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ز بلور تابنده چون آفتاب</p></div>
<div class="m2"><p>یکی دست مجلس بتری چو آب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز دیبای چینی به خروارها</p></div>
<div class="m2"><p>هم از مشک چین با وی انبارها</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>طبقهای کافور با بوی مشک</p></div>
<div class="m2"><p>ز کافورتر بیشتر عود خشک</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کمانهای چاچی و چینی پرند</p></div>
<div class="m2"><p>گرانمایه شمشیرها نیز چند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تکاور سمندان ختلی خرام</p></div>
<div class="m2"><p>همه تازه پیکر همه تیزگام</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>یکی کاروان جمله شاهین و باز</p></div>
<div class="m2"><p>به چرز و کلنگ افگنی تیز تاز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چهل پیل با تخت و بر گستوان</p></div>
<div class="m2"><p>بلند و قوی مغز و سخت استخوان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>غلامان لشگر شکن خیل خیل</p></div>
<div class="m2"><p>کنیزان که در مرده آرند میل</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو نزلی چنین پیش مهمان کشید</p></div>
<div class="m2"><p>جز این پیشکشها فراوان کشید</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>پس از ساعتی گنج نو باز کرد</p></div>
<div class="m2"><p>از آن خوبتر تحفه‌ای ساز کرد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>خرامنده ختلی کش و دم سیاه</p></div>
<div class="m2"><p>تکاورتر از باد در صبحگاه</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>رونده یکی تخت شاهنشهی</p></div>
<div class="m2"><p>نشینندش از پویه بی‌آگهی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سبق برده از آهوان در شتاب</p></div>
<div class="m2"><p>به گرمی چو آتش به نرمی چو آب</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به صحرا ز مرغان سبک خیز تر</p></div>
<div class="m2"><p>به دریا دراز ماهیان تیزتر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به چابک روی پیکرش دیو زاد</p></div>
<div class="m2"><p>به گردندگی کنیتش دیو باد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به انگیزش از آسمان کم نبود</p></div>
<div class="m2"><p>صبا مرد میدان او هم نبود</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چنان رفت و آمد به آوردگاه</p></div>
<div class="m2"><p>که واماند ازو وهم در نیمراه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>فرس را رخ افکنده در وقت شور</p></div>
<div class="m2"><p>فکنده فرس پیل را وقت زور</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چو وهم از همه سوی مطلق خرام</p></div>
<div class="m2"><p>چو اندیشه در تیز رفتن تمام</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سمندی نگویم سمندر فشی</p></div>
<div class="m2"><p>سمندر فشی نه سکندر کشی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>شکاری یکی مرغ شوریده سر</p></div>
<div class="m2"><p>ز خواب شب فتنه شوریده‌تر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چو دوران درآمد شدن تیز بال</p></div>
<div class="m2"><p>شدن چون جنوب آمدن چون شمال</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>عقابین پولاد در جنگ او</p></div>
<div class="m2"><p>عقابان سیه جامه ز آهنگ او</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بسی خون گرو کرده در گردنش</p></div>
<div class="m2"><p>عقابین چنگ عقاب افکنش</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>جگر سای سیمرغ در تاختن</p></div>
<div class="m2"><p>شکارش همه کرگدن ساختن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>غضنباک و خونریز و گستاخ چشم</p></div>
<div class="m2"><p>خدای آفریدش ز بیداد و خشم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>طغان شاه مرغان و طغرل به نام</p></div>
<div class="m2"><p>به سلطانی اندر چو طغرل تمام</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>کنیزی سیه چشم و پاکیزه روی</p></div>
<div class="m2"><p>گل اندام و شکر لب و مشگبوی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بتی چون بهشتی برآراسته</p></div>
<div class="m2"><p>فریبی به صد آرزو خواسته</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>خرامنده ماهی چو سرو بلند</p></div>
<div class="m2"><p>مسلسل دو گیسو چو مشکین کمند</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>برو غبغبی کاب ازو می‌چکید</p></div>
<div class="m2"><p>بر آتش بر آب معلق که دید</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>رخش بر بنفشه گل انداخته</p></div>
<div class="m2"><p>بنفشه نگهبان گل ساخته</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>سهی سرو محتاج بالای او</p></div>
<div class="m2"><p>شکر بنده و شهد مولای او</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>کمر بستهٔ زلف او مشک ناب</p></div>
<div class="m2"><p>که زلفش کمر بست بر آفتاب</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>سخنگوی شهدی شکر باره‌ای</p></div>
<div class="m2"><p>به شهد و شکر بر ستمگاره‌ای</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بلورین تن و قاقمی پشت او</p></div>
<div class="m2"><p>به شکل دم قاقم انگشت او</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ز سیمین زنخ گوئی انگیخته</p></div>
<div class="m2"><p>بر او طوقی از غبغب آویخته</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بدان طوق و گوی آن مه مهر جوی</p></div>
<div class="m2"><p>ز مه طوق برده ز خورشید گوی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ز ابرو کمان کرده و ز غمزه تیر</p></div>
<div class="m2"><p>به تیر و کمان کرده صد دل اسیر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چو می‌خوردی از لطف اندام وی</p></div>
<div class="m2"><p>ز حلقش پدید آمدی رنگ می</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>هزار آفرین بر چنان دایه‌ای</p></div>
<div class="m2"><p>که پرورد از انسان گرانمایه‌ای</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>نزد بر کس از تنگ چشمی نظر</p></div>
<div class="m2"><p>ز چشمش دهانش بسی تنگ تر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>تو گفتی که خود نیست او را دهان</p></div>
<div class="m2"><p>همان نام او نیست اندر جهان</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>رسانندهٔ تحفهٔ ارجمند</p></div>
<div class="m2"><p>به تعریف آن تحفه شد سربلند</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>که این مرغ و این بارگی وین کنیز</p></div>
<div class="m2"><p>عزیزند و بر شاه بادا عزیز</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>نه کس بر چنین خنگ ختلی نشست</p></div>
<div class="m2"><p>نه مرغی چنین آید آسان به دست</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>به گفتن چه حاجت که هنگام کار</p></div>
<div class="m2"><p>هنرهای خود را کنند آشکار</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>کنیزی بدین چهره هم خوار نیست</p></div>
<div class="m2"><p>که در خوب‌روئی کسش یار نیست</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>سه خصلت در او مادر آورد هست</p></div>
<div class="m2"><p>که آنرا چهارم نیاید به دست</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>یکی خوبروئی و زیبندگی</p></div>
<div class="m2"><p>که هست آیتی در فریبندگی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>دویم زورمندی که وقت نبرد</p></div>
<div class="m2"><p>نپیچد عنان را ز مردان مرد</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>سه دیگر خوش آوازی و بانگ رود</p></div>
<div class="m2"><p>که از زهره خوشتر سراید سرود</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>چو آواز خود بر کشد زیر و زار</p></div>
<div class="m2"><p>بخسبد بر آواز او مرغ و مار</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>جهانجوی را زان دل آرام چست</p></div>
<div class="m2"><p>خوش آوازی و خوبی آمد درست</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>حدیث دلیری و مردانگی</p></div>
<div class="m2"><p>نپذیرفت و بود آن ز فرزانگی</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>سمن نازک و خار محکم بود</p></div>
<div class="m2"><p>که مردانگی در زنان کم بود</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>زن ار سیمتن نی که روئین تنست</p></div>
<div class="m2"><p>ز مردی چه لافد که زن هم زنست</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>اگر ماهی از سنگ خارا بود</p></div>
<div class="m2"><p>شکار نهنگان دریا بود</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>ز کاغذ نشاید سپر ساختن</p></div>
<div class="m2"><p>پس آنکه به آب اندر انداختن</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>گران داشت آن نکته را شهریار</p></div>
<div class="m2"><p>زنان را به مردی ندید استوار</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بپذرفتنش حلقه در گوش کرد</p></div>
<div class="m2"><p>چو پذرفت نامش فراموش کرد</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چو آن پیشکشها پذیرفت شاه</p></div>
<div class="m2"><p>شد از خوان خاقان سوی خوابگاه</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>سحرگه که طاوس مشرق خرام</p></div>
<div class="m2"><p>برون زد سر از طاق فیروزه فام</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>دگر باره شه باده بر کف نهاد</p></div>
<div class="m2"><p>برامش در بارگه برگشاد</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>بسر برد روزی دو در رود و می</p></div>
<div class="m2"><p>دگر پاره شد مرکبش تیز پی</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>سوی بازگشتن بسی چید کار</p></div>
<div class="m2"><p>بگردنگی گشت چون روزگار</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>پری چهره ترکی که خاقان چین</p></div>
<div class="m2"><p>به شه داد تا داردش نازنین</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>از آنجا که شه را نیامد پسند</p></div>
<div class="m2"><p>چو سایه پس پرده شد شهر بند</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>برافروخت آن ماه چون آفتاب</p></div>
<div class="m2"><p>فرو ریخت بر گل ز نرگس گلاب</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>به زندان سرای کنیزان شاه</p></div>
<div class="m2"><p>همی بود چون سایه در زیر چاه</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>یکی روز کاین چرخ چوگان پرست</p></div>
<div class="m2"><p>ز شب بازی آورد گوئی به دست</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>سکندر که از خسروان گوی برد</p></div>
<div class="m2"><p>عنان را به چوگانی خود سپرد</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>در آمد به طیارهٔ کوهکن</p></div>
<div class="m2"><p>فرس پیل بالا و شه پیلتن</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>علم بر کشیدند گردنکشان</p></div>
<div class="m2"><p>پدید آمد از روز محشر نشان</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>ز لشگر که عرضش به فرسنگ بود</p></div>
<div class="m2"><p>بیابان به نخجیر بر تنگ بود</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>ز صحرای چین تا به دریای چند</p></div>
<div class="m2"><p>زمین در زمین بود زیر پرند</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>سیه چون در آمد به عرض شمار</p></div>
<div class="m2"><p>گزیده در او بود پانصد هزار</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>پس و پیش ترکان طاوس رنگ</p></div>
<div class="m2"><p>چپ و راست شیران پولاد چنگ</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>به قلب اندرون شاه دریا شکوه</p></div>
<div class="m2"><p>سپه گرد بر گرد دریا چو کوه</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>بجز پیل زوران آهن کلاه</p></div>
<div class="m2"><p>چهل پیل جنگی پس و پشت شاه</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>هزار و چهل سنجق پهلوی</p></div>
<div class="m2"><p>روان در پی رایت خسروی</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>کمرهای زرین غلامان خاص</p></div>
<div class="m2"><p>چو بر شوشهٔ نقرهٔ زر خلاص</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>و شاقان جوشنده چون آب سیل</p></div>
<div class="m2"><p>ز هر سو جنیبت کشان خیل خیل</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>ندیمان شایسته بر گرد شاه</p></div>
<div class="m2"><p>که آسان از ایشان شود رنج راه</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>خرامان شده خسرو خسروان</p></div>
<div class="m2"><p>طرفدار چین در رکابش روان</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>شهنشه چو بنوشت لختی زمین</p></div>
<div class="m2"><p>اشارت چنین شد به خاقان چین</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>که گردد سوی خانهٔ خویش باز</p></div>
<div class="m2"><p>به اقلیم ترکان کند ترکتاز</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>جهانجوی را ترک بدرود کرد</p></div>
<div class="m2"><p>به آب مژه روی را رود کرد</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>عنان تافته شاه گیتی نورد</p></div>
<div class="m2"><p>ز صحرا به جیجون رسانید گرد</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>چو آمد به نزدیک آن ژرف رود</p></div>
<div class="m2"><p>بفرمود تا لشگر آید فرود</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>بر آن فرضه جایی دل‌افروز دید</p></div>
<div class="m2"><p>نشستن بر آن جای فیروز دید</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>طناب سراپردهٔ خسروی</p></div>
<div class="m2"><p>کشیدند و شد میخ مرکز قوی</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>ز بس نوبتیهای گوهر نگار</p></div>
<div class="m2"><p>چو باغ ارم گشت جیحون کنار</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>چو شه کشور ماورالنهر دید</p></div>
<div class="m2"><p>جهانی نگویم که یک شهر دید</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>از آن مال کز چین به چنگ آمدش</p></div>
<div class="m2"><p>بسی داد کانجا درنگ آمدش</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>بناهای ویرانه آباد کرد</p></div>
<div class="m2"><p>بسی شهر نو نیز بنیاد کرد</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>سمرقند را کادمی شاد ازوست</p></div>
<div class="m2"><p>شنیده چنین شد که بنیاد ازوست</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>خبر گرم شد در خراسان و روم</p></div>
<div class="m2"><p>که شاهنشه آمد ز بیگانه بوم</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>بهر شهری از شادی فتح شاه</p></div>
<div class="m2"><p>بشارت زنان بر گرفتند راه</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>به شکرانه رایت برافراختند</p></div>
<div class="m2"><p>به هر خانه‌ای خرمی ساختند</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>فرستاد هر کس بسی مال و گنج</p></div>
<div class="m2"><p>به درگاه شاه از پی پای رنج</p></div></div>