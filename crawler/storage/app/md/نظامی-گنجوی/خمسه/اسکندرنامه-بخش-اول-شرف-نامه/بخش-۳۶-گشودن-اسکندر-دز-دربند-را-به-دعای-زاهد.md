---
title: >-
    بخش ۳۶ - گشودن اسکندر دز دربند را به دعای زاهد
---
# بخش ۳۶ - گشودن اسکندر دز دربند را به دعای زاهد

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی آن می‌که ناز آورد</p></div>
<div class="m2"><p>جوانی دهد عمر باز آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به من ده که این هر دو گم کرده‌ام</p></div>
<div class="m2"><p>قناعت به خوناب خم کرده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی کو در نیک‌نامی زند</p></div>
<div class="m2"><p>در این حلقه لاف غلامی زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نیکی چنان پرورد نام خویش</p></div>
<div class="m2"><p>کزو نیک یابد سرانجام خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دراعهٔ در گریزد تنش</p></div>
<div class="m2"><p>که آن درع باشد نه پیراهنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به از نام نیکو دگر نام نیست</p></div>
<div class="m2"><p>بد آنکس که نیکو سرانجام نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو می‌خواهی ای مرد نیکی پسند</p></div>
<div class="m2"><p>که نامی برآری به نیکی بلند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی جامه در نیک‌نامی بپوش</p></div>
<div class="m2"><p>به نیکی دگر جامه‌ها میفروش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبینی که باشد ز مشگین حریر</p></div>
<div class="m2"><p>فروشندهٔ مشک را ناگزیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گزارنده این نو آیین خیال</p></div>
<div class="m2"><p>دم از نیک‌نامان زدی ماه و سال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سکندر که آن نیکنامی نمود</p></div>
<div class="m2"><p>بران نام نیکو بسی کرد سود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه سوی نیکان نظر داشتی</p></div>
<div class="m2"><p>بدان را بر خویش نگذاشتی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز کشور خدایان و شهزادگان</p></div>
<div class="m2"><p>نظر پیش کردی به افتادگان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کجا زاهدی خلوتی یافتی</p></div>
<div class="m2"><p>به خلوت گهش زود بشتافتی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهر جا که رزمی برآراستی</p></div>
<div class="m2"><p>از ایشان به همت مدد خواستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همانا کزان بود پیروز جنگ</p></div>
<div class="m2"><p>که پیروزه را فرق کردی ز سنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپاهی که با او به جنگ آمدند</p></div>
<div class="m2"><p>از آن پیشه کو داشت تنگ آمدند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نمودند کای داور روزگار</p></div>
<div class="m2"><p>به تعلیم تو دولت آموزگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ترا فتح و فیروزی از لشگرست</p></div>
<div class="m2"><p>تو زاهد نوازی سخن دیگرست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به شمشیر باید جهان را گشاد</p></div>
<div class="m2"><p>تو از نیک‌مردان چه آری به یاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو همت سلاحست در دستبرد</p></div>
<div class="m2"><p>بگو تا کنیم آنچه داریم خرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ازین پس که بر هم نبردان زنیم</p></div>
<div class="m2"><p>در همت نیک‌مردان زنیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جهاندار ازین داوریهای سخت</p></div>
<div class="m2"><p>نگهداشت پاسخ به نیروی بخت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سخن بر بدیهه نیاید صواب</p></div>
<div class="m2"><p>به وقت خودش داد باید جواب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو لشگر سوی کوه البرز راند</p></div>
<div class="m2"><p>بهر ناحیت نایبی را نشاند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به دهلیزهٔ رهگذرهای سخت</p></div>
<div class="m2"><p>ز شروان چو شیران همی برد رخت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در آن تاختن کارزومند بود</p></div>
<div class="m2"><p>رهش بر گذرگاه دربند بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نبود آنگه آن شهر آراسته</p></div>
<div class="m2"><p>دزی بود در وی بسی خواسته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در آن دز تنی چند ره داشتند</p></div>
<div class="m2"><p>که کس را در آن راه نگذاشتند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو شه را سراپرده آنجا زدند</p></div>
<div class="m2"><p>رقیبان دز خیمه بالا زدند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در دز ببستند بر روی شاه</p></div>
<div class="m2"><p>نکردند در تیغ و لشکر نگاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به نوبتگه شاه نشتافتند</p></div>
<div class="m2"><p>سر از خدمت بارگه تافتند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر خواندشان داور دور گیر</p></div>
<div class="m2"><p>به رفتن نگشتند فرمان پذیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وگر دفتر داوری در نوشت</p></div>
<div class="m2"><p>ندادند راهش بر کوه و دشت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همان چاره دید آن خردمند شاه</p></div>
<div class="m2"><p>که بردارد آن بند از بندگاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به لشکر بفرمود تا صد هزار</p></div>
<div class="m2"><p>درآیند پیرامن آن حصار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به خرسنگ غضبان خرابش کنند</p></div>
<div class="m2"><p>به سیلاب خون غرق آبش کنند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چهل روز لشگر شغب ساختند</p></div>
<div class="m2"><p>کزان دز کلوخی نینداختند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز پرتاب او ناوک افکند بال</p></div>
<div class="m2"><p>کمندی نه کانجا رساند دوال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>عروسک زنانی چو دیوان شموس</p></div>
<div class="m2"><p>خجل گشته زان قلعه چون عروس</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نه عراده بر گرد او ره شناس</p></div>
<div class="m2"><p>نه از گردش منجنیقش هراس</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو عاجز شدند اندر آن تاختن</p></div>
<div class="m2"><p>وزان جوز بر گنبد انداختن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شه کاردان مجلسی نو نهاد</p></div>
<div class="m2"><p>سران را طلب کرد و ابرو گشاد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چه گوئید گفتا درین بند کوه</p></div>
<div class="m2"><p>که آورد از اندیشه ما را ستوه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ولایت گشایان گردن فراز</p></div>
<div class="m2"><p>نشستند و بردند شه را نماز</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>که ما بندگان تا کمر بسته‌ایم</p></div>
<div class="m2"><p>بدین روز یک روز ننشسته‌ایم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چهل روز باشد که بیخورد و خواب</p></div>
<div class="m2"><p>ستیزیم با ابر و با آفتاب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تو دانی که بر تارک مهر و میغ</p></div>
<div class="m2"><p>نشاید زدن نیزه و تیر و تیغ</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو دیوان بسی چاره‌ها ساختیم</p></div>
<div class="m2"><p>از این دیوخانه نپرداختیم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همان به که گردیم ازین راه تنگ</p></div>
<div class="m2"><p>گریوه نوردیم و سائیم سنگ</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شهنشه چو دانست کان سروران</p></div>
<div class="m2"><p>فرو مانده بودند و عاجز در آن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو در سرمه زد چشم خورشید میل</p></div>
<div class="m2"><p>فرو رفت گوهر به دریای نیل</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شه از گنج گوهر به دریا کنار</p></div>
<div class="m2"><p>یکی مجلس آراست چون نوبهار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بپرسید چون حلقه گشت انجمن</p></div>
<div class="m2"><p>از آن سرفرازان لشگر شکن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>که از گوشه‌داران در این گوشه کیست</p></div>
<div class="m2"><p>که بر ماتم آرزوها گریست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>یکی گفت کای شاه دانش پرست</p></div>
<div class="m2"><p>پرستشگری در فلان غار هست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به کس روی ننماید از هیچ راه</p></div>
<div class="m2"><p>کند بی نیازی به مشتی گیاه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>شهنشاه برخاست هم در زمان</p></div>
<div class="m2"><p>عنان ناب گشت از بر همدمان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز خاصان تنی چند همراه کرد</p></div>
<div class="m2"><p>نشان جست و آمد بر نیک‌مرد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ره از شب چو روز بداندیش بود</p></div>
<div class="m2"><p>وشاقی و شمعی روان پیش بود</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو نزدیک غار آمد از راه دور</p></div>
<div class="m2"><p>به غار اندر افتاد از آن شمع نور</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>پرستنده چون پرتو نور دید</p></div>
<div class="m2"><p>ز تاریکی غار بیرون دوید</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>فرشته وشی دید چون آفتاب</p></div>
<div class="m2"><p>برآورده اقبال را سر ز خواب</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>جهاندیده نزد جهاندار تاخت</p></div>
<div class="m2"><p>به نور جهانداری او را شناخت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بدو گفت شخصی بهی پیکری</p></div>
<div class="m2"><p>گمانم چنانست کاسکندری</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>شه از مهربانی بدو داد دست</p></div>
<div class="m2"><p>درون رفت و پیشش به زانو نشست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بپرسید از او کاشنای تو کیست</p></div>
<div class="m2"><p>ز دنیا چه پوشی و خورد تو چیست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چه دانستی ای زاهد هوشیار</p></div>
<div class="m2"><p>که اسکندرم من درین تنگ غار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دعا کرد زاهد که دلشاد باش</p></div>
<div class="m2"><p>ز بند ستمگاری آزاد باش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به اقبال باد اخترت خاسته</p></div>
<div class="m2"><p>به نیروی اقبالت آراسته</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>اگر زانکه بشناختم شاه را</p></div>
<div class="m2"><p>شناسد به شب هر کسی ماه را</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>نه آیینه تنها تو داری بدست</p></div>
<div class="m2"><p>مرا در دل آیینه‌ای نیز هست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به صد سال کو را ریاضت زدود</p></div>
<div class="m2"><p>یکی صورت آخر تواند نمود</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دگر آنچه پرسد خداوند رای</p></div>
<div class="m2"><p>که چونست زاهد در این تنگ جای</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به نیروی تو شادم و تندرست</p></div>
<div class="m2"><p>تنومندتر ز آنچه بودم نخست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ز مهر و زکین با کسم یاد نیست</p></div>
<div class="m2"><p>کس از بندگان چون من آزاد نیست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>جهان را ندیدم وفا داریی</p></div>
<div class="m2"><p>نخواهد کس از بی وفا یاریی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو برسختم اندیشهٔ کار خویش</p></div>
<div class="m2"><p>همین گوشه دیدم سزاوار خویش</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بریدم ز هر آشنائی شمار</p></div>
<div class="m2"><p>بس است آشنای من آموزگار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>به بسیار خواری نیارم بسیچ</p></div>
<div class="m2"><p>که پری دهد ناف را پیچ پیچ</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>گیا پوشم و قوت من هم گیا</p></div>
<div class="m2"><p>کنم سنگ را زر بدین کیمیا</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بود سالها کز سر آیندگان</p></div>
<div class="m2"><p>ندیدم کسی جز تو ز آیندگان</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>سبب چیست کامشب درین کنج غار</p></div>
<div class="m2"><p>به نیک اختری رنجه شد شهریار</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>در غار من وانگهی چون توئی</p></div>
<div class="m2"><p>یکی پاس شه را کم از هندوئی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>جهاندار گفت ای جهاندیده پیر</p></div>
<div class="m2"><p>از این آمدن داشتم ناگزیز</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>خدای آهنی را بدو نیم کرد</p></div>
<div class="m2"><p>به ما هر دوان آن دو تسلیم کرد</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>کلیدی و تیغی بدینسان نگاشت</p></div>
<div class="m2"><p>کلید آن تو تیغ بر من گذاشت</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چو من زاهن تیغ گیتی فروز</p></div>
<div class="m2"><p>کنم یاری عدل در نیم روز</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>تو در نیمه شب نیز اگر یاوری</p></div>
<div class="m2"><p>کلیدی بجنبان در این داوری</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>مگر کز کلید تو و تیغ من</p></div>
<div class="m2"><p>گشاده شود کار این انجمن</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>حصاری است بر سفت این تیغ کوه</p></div>
<div class="m2"><p>درو رهزنانند چندین گروه</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>همه روز و شب کاروانها زنند</p></div>
<div class="m2"><p>ز بد گوهری راه جانها زنند</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>در آن جستجویم که بگشایمش</p></div>
<div class="m2"><p>به داد و به دانش بیارایمش</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>تو نیز ار به همت کنی یاریی</p></div>
<div class="m2"><p>در این ره کند بخت بیداریی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ز رهزن شود راه پرداخته</p></div>
<div class="m2"><p>شود توشهٔ رهروان ساخته</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>چو آگاه شد مرد ایزد شناس</p></div>
<div class="m2"><p>که دزدان بر آن قلعه دارند پاس</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>یکی منجنیق از نفس برگشاد</p></div>
<div class="m2"><p>که بر قلعهٔ آسمان در گشاد</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چنان زد در آن کوههٔ منجنیق</p></div>
<div class="m2"><p>که شد کوه در وی چو دریا غریق</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>به شه گفت برخیز و شو باز جای</p></div>
<div class="m2"><p>که آن کوهپایه درآمد ز پای</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>چو شاهنشه آمد سوی بزم خویش</p></div>
<div class="m2"><p>مقیمان مجلس دویدند پیش</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>دگر باره مجلس بیاراستند</p></div>
<div class="m2"><p>به رامش نشستند و می خواستند</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>کس آمد که دژبان این کوهسار</p></div>
<div class="m2"><p>ستاد است بر در به امید بار</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>بفرمود شه تا درآرند زود</p></div>
<div class="m2"><p>درآمد بر شاه و خدمت نمود</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چو بر شه دعا کرد از اندازه بیش</p></div>
<div class="m2"><p>کلید در دز بینداخت پیش</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>خبر کرد کامشب ز نیروی شاه</p></div>
<div class="m2"><p>خرابی درآمد بدین قلعه گاه</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>دو برج رزین زین دز سنگ بست</p></div>
<div class="m2"><p>ز برج ملک دور درهم شکست</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>ز خشم خدا منجنیقی رسید</p></div>
<div class="m2"><p>دز افتاد و ناگاه درهم درید</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>گرش منجنیق تو کردی خراب</p></div>
<div class="m2"><p>به ذره کجا ریختی آفتاب</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>خرابیش دانم نه زین لشگرست</p></div>
<div class="m2"><p>که این منجنیق از دزی دیگرست</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>چو حکم دز آسمانی تراست</p></div>
<div class="m2"><p>تو دانی و دز حکمرانی تراست</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>نگه کرد شه سوی لشکر کشان</p></div>
<div class="m2"><p>کزین به دعا را چه باشد نشان</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>چهل روز باشد که مردان کار</p></div>
<div class="m2"><p>به شمشیر کوشند با این حصار</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>به چندین سر تیغ الماس رنگ</p></div>
<div class="m2"><p>نسفتند جو سنگی از خاره سنگ</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>به آهی که برداشت بی توشه‌ای</p></div>
<div class="m2"><p>فرو ریخت از منظرش گوشه‌ای</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>شما را چه رو مینماید درین</p></div>
<div class="m2"><p>که بی نیک‌مردان مبادا زمین</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>بزرگان لشکر به عذرآوری</p></div>
<div class="m2"><p>پشیمان شدند از چنان داوری</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>زمین بوسه دادند در بزم شاه</p></div>
<div class="m2"><p>که خالی مباد از تو تخت و کلاه</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>قوی باد در ملک بازوی تو</p></div>
<div class="m2"><p>بقا باد نقد ترازوی تو</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چنین حرفها را تو دانی شناخت</p></div>
<div class="m2"><p>که یزدان ترا سایه خویش ساخت</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>چو ما نیز از این پرده آگه شدیم</p></div>
<div class="m2"><p>براه آمدیم ارچه از ره شدیم</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>فرستاد شه تا به دز تاختند</p></div>
<div class="m2"><p>از آن رهزنان دز بپرداختند</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>بجای دز اقطاعها داد شان</p></div>
<div class="m2"><p>سوی دادهٔ خود فرستادشان</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>در آن سنگ بسته دز اوج سای</p></div>
<div class="m2"><p>عمارتگری کرد بسیار جای</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>خرابیش را یکسر آباد کرد</p></div>
<div class="m2"><p>دز ظلم را خانهٔ داد کرد</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>نواحی نشینان آن کوهسار</p></div>
<div class="m2"><p>تظلم نمودند هنگام بار</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>که ازبیم قفچاق وحشی سرشت</p></div>
<div class="m2"><p>درین مرز تخمی نیاریم کشت</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>چو هر گه کزین سو شتاب آورند</p></div>
<div class="m2"><p>برینش درین کشت و آب آورند</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>ازین روی ما را زیانها رسد</p></div>
<div class="m2"><p>ز نان تنگی آفت به جانها رسد</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>گر آرد ملک هیچ بخشایشی</p></div>
<div class="m2"><p>رساند بدین کشور آسایشی</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>درین پاسگه رخنهائی که هست</p></div>
<div class="m2"><p>عمارت کند تا شود سنگ بست</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>مگر زافت آن بیابانیان</p></div>
<div class="m2"><p>به راحت رسد کار خزرانیان</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>بفرمود شه تاگذرگاه کوه</p></div>
<div class="m2"><p>ببندند خزرانیان هم‌گروه</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>ز پولاد و ارزیز و از خاره سنگ</p></div>
<div class="m2"><p>برآرند سدی در آن راه تنگ</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>ز خارا تراشان احکام کار</p></div>
<div class="m2"><p>که بر کوه دانند بستن حصار</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>فرستاد خلقی به انبوه را</p></div>
<div class="m2"><p>گذر داد بر بستن آن کوه را</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>چو زابادی رخنه پرداختند</p></div>
<div class="m2"><p>به عزم شدن رایت افراختند</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>شد از زخمهٔ کاسه و زخم کوس</p></div>
<div class="m2"><p>خدنگ اندران بیشه‌ها آبنوس</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>ملک بارگه سوی صحرا کشید</p></div>
<div class="m2"><p>عنان راه را داد و منزل برید</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>چو سیاره چرخ شبدیز راند</p></div>
<div class="m2"><p>بهر برج کامد سعادت رساند</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>چو زلف شب از حلقه عنبری</p></div>
<div class="m2"><p>سمن ریخت بر طاق نیلوفری</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>شه و لشگر از رنج ره سودگی</p></div>
<div class="m2"><p>رسیدند لختی به آسودگی</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>تنی چند را از رقیبان راه</p></div>
<div class="m2"><p>ز بهر شب افسانه بنشاند شاه</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>از ایشان خبرهای آن کوه و دشت</p></div>
<div class="m2"><p>بپرسید و آگه شد از سرگذشت</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>پس آنگاه از هر نشیب و فراز</p></div>
<div class="m2"><p>به گوش ملک برگشادند راز</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>نمودند کاینجا حصاریست خوب</p></div>
<div class="m2"><p>که دور است ازو تند باد جنوب</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>یکی سنگ مینای مینو سرشت</p></div>
<div class="m2"><p>به زیبائی و خرمی چون بهشت</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>سریر سرافراز شد نام او</p></div>
<div class="m2"><p>درو تخت کیخسرو و جام او</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>چو کیخسرو از ملک پرداخت رخت</p></div>
<div class="m2"><p>نهاد اندران تاجگه جام و تخت</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>همان گور خانه ز غاری گزید</p></div>
<div class="m2"><p>کز آتش در آن غار نتوان خزید</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>هم از تخمهٔ او در آن پیشگاه</p></div>
<div class="m2"><p>ملک زاده‌ای هست بر جمله شاه</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>پرستش کند جای آن شاه را</p></div>
<div class="m2"><p>نگهدارد آن جام وآن گاه را</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>جهان مرزبان شاه گیتی نورد</p></div>
<div class="m2"><p>برافروخت کاین داستان گوش کرد</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>کجا بستدی فرخ آیین دزی</p></div>
<div class="m2"><p>چه از زورمندی چه از عاجزی</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>اگر آشکارا بدی گر نهان</p></div>
<div class="m2"><p>بر آن دز شدی تاجدار جهان</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>بدیدی دز از دز فرود آمدی</p></div>
<div class="m2"><p>به دزبان بر از وی درود آمدی</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>بنا دیده دیدن هوسناک بود</p></div>
<div class="m2"><p>بهر جا که شد چست و چالاک بود</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>چو آن شب صفتهای آن دز شنید</p></div>
<div class="m2"><p>به دز دیدنش رغبت آمد پدید</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>مگر کز کهن جام کیخسروی</p></div>
<div class="m2"><p>دهد مجلس مملکت را نوی</p></div></div>