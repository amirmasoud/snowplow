---
title: >-
    بخش ۹ - ستایش اتابیک اعظم نصرةالدین ابوبکربن محمد
---
# بخش ۹ - ستایش اتابیک اعظم نصرةالدین ابوبکربن محمد

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی آن آب یاقوت‌وار</p></div>
<div class="m2"><p>در افکن بدان جام یاقوت بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سفالینه جامی که می جان اوست</p></div>
<div class="m2"><p>سفالین زمین خاک ریحان اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علم برکش ای آفتاب بلند</p></div>
<div class="m2"><p>خرامان شو ای ابر مشگین پرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنال ای دل رعد چون کوس شاه</p></div>
<div class="m2"><p>بخند ای لب برق چون صبحگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بار ای هوا قطره ناب را</p></div>
<div class="m2"><p>بگیر ای صدف در کن این آب را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برا ای در از قعر دریای خویش</p></div>
<div class="m2"><p>ز تاج سر شاه کن جای خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهی که آرزومند معراج توست</p></div>
<div class="m2"><p>زمین بوس او درةالتاج توست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سکندر شکوهی که در جمله ساز</p></div>
<div class="m2"><p>شکوه سکندر بدو گشت باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمین زنده‌دار آسمان زنده کن</p></div>
<div class="m2"><p>جهان گیر دشمن پراکنده کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طرفدار مغرب به مردانگی</p></div>
<div class="m2"><p>قدر خان مشرق به فرزانگی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان پهلوان نصرةالدین که هست</p></div>
<div class="m2"><p>بر اعدای خود چون فلک چیره‌دست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مخالف پس اندیش و او پیش بین</p></div>
<div class="m2"><p>بداندیش کم مهر و او بیش‌کین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خداوند شمشیر و تخت و کلاه</p></div>
<div class="m2"><p>سه نوبت زن پنج نوبت پناه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به رستم رکابی روان کرده رخش</p></div>
<div class="m2"><p>هم اورنگ پیرای و هم تاج بخش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شهان را ز رسمی که آیین بود</p></div>
<div class="m2"><p>کلید آهنین گنج زرین بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جز او کاهن تیغ روشن کند</p></div>
<div class="m2"><p>کلید از زر و گنج از آهن کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو آب فرات آشکارانواز</p></div>
<div class="m2"><p>چو سرچشمه نیل پنهان گداز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر سایه بر آفتاب افکند</p></div>
<div class="m2"><p>در آن چشمهٔ آتش آب افکند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وگر ماه نو را براتی دهد</p></div>
<div class="m2"><p>ز نقص کمالش نجاتی دهد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر انعام او بر شمارد کسی</p></div>
<div class="m2"><p>بدان تا کند شکر نعمت بسی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز شکر وی آن نعمت افزون بود</p></div>
<div class="m2"><p>ولی نعمتی بیش از این چون بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فلک وار با هر که بندد کمر</p></div>
<div class="m2"><p>بر آب افکند چون زمینش سیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بریزد در آشوب چون میغ او</p></div>
<div class="m2"><p>سر تیغ کوه از سر تیغ او</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر آنچ او نموده گه کارزار</p></div>
<div class="m2"><p>نه رستم نموده نه اسفندیار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>صلاح جهان آن شب آمد پدید</p></div>
<div class="m2"><p>که از مولد این صبح صادق دمید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کجا گام زد خنگ پدرام او</p></div>
<div class="m2"><p>زمین یافت سرسبزی از گام او</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به هر دایره کو زده ترکتاز</p></div>
<div class="m2"><p>ز پرگار خطش گره کرده باز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بران بقعه کاو بارگی تاخته</p></div>
<div class="m2"><p>زمین گنج قارون برانداخته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر آن دژ که او رایت انگیخته</p></div>
<div class="m2"><p>سر کوتوال از دژ آویخته</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر دیگران کاصلشان آدمیست</p></div>
<div class="m2"><p>همه مردمند او همه مردمیست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ندانم کس از مردم روشناس</p></div>
<div class="m2"><p>کزان مردمی نیست بر وی سپاس</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز بس ناز و نعمت کزو رانده‌اند</p></div>
<div class="m2"><p>ولی‌نعمت عالمش خوانده‌اند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر مرده‌ای سر آرد ز گور</p></div>
<div class="m2"><p>بگیرد همه شهر و بازار شور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هزاران دل مرده از عدل شاه</p></div>
<div class="m2"><p>شود زنده و خصم ناید به راه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو عیسی بسی مرده را زنده کرد</p></div>
<div class="m2"><p>به خلقی چنین خلق را بنده کرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جهان بود چون کان گوهر خراب</p></div>
<div class="m2"><p>به آبادی افتاد ازین آفتاب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زمین دوزخی بود بی کار و کشت</p></div>
<div class="m2"><p>به ابری چنین تازه شد چون بهشت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز هر نعمتی کایدش نو به نو</p></div>
<div class="m2"><p>دهد بخش خواهندگان جو به جو</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به هر نیکوی چون خرد پی‌برد</p></div>
<div class="m2"><p>جهان یاد نیک از جهان کی بود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گر از نخل طوبی رسد در بهشت</p></div>
<div class="m2"><p>به هر کوشکی شاخ عنبر سرشت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>رسد شرق تا غرب احسان او</p></div>
<div class="m2"><p>به هر خانه‌ای نعمت خوان او</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زهی بارگاهی که چون آفتاب</p></div>
<div class="m2"><p>ز مشرق به مغرب رساند طناب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به کیخسروی نامش افتاده چست</p></div>
<div class="m2"><p>نسب کرده بر کیقبادی درست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به هر وادیی کو عنان تافته</p></div>
<div class="m2"><p>در منه به دامن درم یافته</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز کنجش زمین کیسه بر دوخته</p></div>
<div class="m2"><p>سمن سیم و خیری زر اندوخته</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کجا گنج دانی پشیزی در او</p></div>
<div class="m2"><p>که از گنج او نیست چیزی در او</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو از تاج او شد فلک سر بلند</p></div>
<div class="m2"><p>سرش باد از آن تاج فیروزمند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زهی خضر و اسکندر کاینات</p></div>
<div class="m2"><p>که هم ملک داری هم آب حیات</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو اسکندری شاه کشورگشای</p></div>
<div class="m2"><p>چو خضر از ره افتاده را رهنمای</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همه چیز داری که آن درخورست</p></div>
<div class="m2"><p>نداری یکی چیز و آن همسرست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو دریا نگویم گران سایه‌ای</p></div>
<div class="m2"><p>همانا که چون کان گرانمایه‌ای</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو در صید شیران شعار افکنی</p></div>
<div class="m2"><p>به تیری دو پیکر شکار افکنی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو در جنگ پیلان گشائی کمند</p></div>
<div class="m2"><p>دهی شاه قنوج را پیل بند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اگر شیر گور افکند وقت زور</p></div>
<div class="m2"><p>تو شیر افکنی بلکه بهرام گور</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چه دولت که در بند کار تو نیست</p></div>
<div class="m2"><p>چه مقصود کان در کنار تو نیست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بسا گردن سخت کیمخت چرم</p></div>
<div class="m2"><p>که شد چون دوال از رکاب تو نرم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دو شخص ایمنند از تو کایی به جوش</p></div>
<div class="m2"><p>یکی نرم گردن یکی سفته گوش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به عذر از تو بدخواه جان می‌برد</p></div>
<div class="m2"><p>بدین عهد رایت جهان می‌برد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو برگشت گرد جهان روزگار</p></div>
<div class="m2"><p>ز شش پادشه ماند شش یادگار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کلاه از کیومرث تختگیر</p></div>
<div class="m2"><p>ز جمشید تیغ از فریدون سریر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز کیخسرو آن جام گیتی نمای</p></div>
<div class="m2"><p>که احکام انجم درو یافت جای</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>فروزنده آیینهٔ گوهری</p></div>
<div class="m2"><p>نمودار تاریخ اسکندری</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>همان خاتم لعل بر دوخته</p></div>
<div class="m2"><p>به مهر سلیمانی افروخته</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بدین گونه شش چیز در حرف تست</p></div>
<div class="m2"><p>گواه سخن نام شش حرف تست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>جز این نیز بینم تو را شش خصال</p></div>
<div class="m2"><p>که بادی برومند ازو ماه و سال</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>یکی آنکه از گنج آراسته</p></div>
<div class="m2"><p>دهی آرزوهای ناخواسته</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دویم مردمی کردن بی قیاس</p></div>
<div class="m2"><p>عوض باز ناجستن از حق‌شناس</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>سوم دل به شفقت برآراستن</p></div>
<div class="m2"><p>ستمدیده را داد دل خواستن</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چهارم علم بر ثریا زدن</p></div>
<div class="m2"><p>چو خورشید لشگر به تنها زدن</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>همان پنجم از مجرم عذر خواه</p></div>
<div class="m2"><p>ز روی کرم عفو کردن گناه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ششم عهد و پیمان نگهداشتن</p></div>
<div class="m2"><p>وفا داری از یاد نگذاشتن</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز تو شش جهت بی روائی مباد</p></div>
<div class="m2"><p>وز این شش خصالت جدائی مباد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به پرواز ملکت دو شاهین به کار</p></div>
<div class="m2"><p>یکی در خزینه یکی در شکار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دو مار از برای تو توفیر سنج</p></div>
<div class="m2"><p>یکی مار مهره یکی مار گنج</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>جهان خسروا زیر هفت آسمان</p></div>
<div class="m2"><p>طرفدار پنجم توئی بی گمان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>جهان را به فرمان چندین بلاد</p></div>
<div class="m2"><p>ستون در تست ذات العماد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>همه شب که مه طوف گردون کند</p></div>
<div class="m2"><p>چراغ ترا روغن افزون کند</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>همه روز خورشید با تاج زر</p></div>
<div class="m2"><p>به پائین تخت تو بندد کمر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>سپارنده پادشاهی به تو</p></div>
<div class="m2"><p>سپرد از جهان هر چه خواهی به تو</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بدان داد ملکت که شاهی کنبی</p></div>
<div class="m2"><p>چو داور شوی داد خواهی کنی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>که بازی کند بر پریشه زور</p></div>
<div class="m2"><p>نه پیلی نهد پای بر پشت مور</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>سپاس از خداوند گیتی پناه</p></div>
<div class="m2"><p>که بیشست از این قصه انصاف شاه</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>به انصاف شه چشم دارم یکی</p></div>
<div class="m2"><p>که بیند در این داستان اندکی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>گر افسانه‌ای بیند از کار دور</p></div>
<div class="m2"><p>نه سایه بر او گستراند نه نور</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>وگر بیند از در در او موج موج</p></div>
<div class="m2"><p>سراینده را سر برآرد به اوج</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>در این گنجنامه زر از جهان</p></div>
<div class="m2"><p>کلید بسی گنج کردم نهان</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>کسی کان کلید زر آرد به دست</p></div>
<div class="m2"><p>طلسم بسی گنج داند شکست</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>وگر گنج پنهان نیارد پدید</p></div>
<div class="m2"><p>شود خرم آخر به زرین کلید</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>تو دانی که این گوهر نیم سفت</p></div>
<div class="m2"><p>چه گنجینه‌ها دارد اندر نهفت</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>نشاط از تو دارد گهر سفتنم</p></div>
<div class="m2"><p>سزاوار توست آفرین گفتنم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>خرد کاسمان را زمین می‌کند</p></div>
<div class="m2"><p>برین آفرین آفرین می‌کند</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چو فرمان چنین آمد از شهریار</p></div>
<div class="m2"><p>که بر نام ما نقش بند این نگار</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>به گفتار شه مغز را تر کنم</p></div>
<div class="m2"><p>بگفت کان مغز در سر کنم</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>فرستم عروسی بدان بزمگاه</p></div>
<div class="m2"><p>کزو چشم روشن شود بزم شاه</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>عروسی چنین شاه را بنده باد</p></div>
<div class="m2"><p>بران فحل آفاق فرخنده باد</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>به اندازه آنکه نزدیک و دور</p></div>
<div class="m2"><p>چراغ جهان تاب را هست نور</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>گل باغ شه عالم افروز باد</p></div>
<div class="m2"><p>چراغ شبش مشعل روز باد</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>دریده دهن بد سگالش چو داغ</p></div>
<div class="m2"><p>زبان سوخته دشمنش چون چراغ</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>نظامی چو دولت در ایوان او</p></div>
<div class="m2"><p>شب و روز باد آفرین خوان او</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ز چشم بد آن کس نیابد گزند</p></div>
<div class="m2"><p>که پیوسته سوزد بر آتش سپند</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>ز سحر آن سرا را نیابی خراب</p></div>
<div class="m2"><p>که دارد سفالینه‌ای پر سداب</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>سداب و سپند رقیبان شاه</p></div>
<div class="m2"><p>دعای نظامی است در صبحگاه</p></div></div>