---
title: >-
    بخش ۵۴ - جنگ ششم اسکندر با روسیان
---
# بخش ۵۴ - جنگ ششم اسکندر با روسیان

<div class="b" id="bn1"><div class="m1"><p>چنین تا یکی روز کاین چرخ پیر</p></div>
<div class="m2"><p>برآورد گوهر ز دریای قیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دگر باره میدان شد آراسته</p></div>
<div class="m2"><p>ز بیغولها نعره برخاسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز لشگرگه روس بانگ جرس</p></div>
<div class="m2"><p>به عیوق بر می‌شد از پیش و پس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشیدند صف قلب داران روس</p></div>
<div class="m2"><p>وزان قلب آراسته چون عروس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کهن پوستینی درآمد به چنگ</p></div>
<div class="m2"><p>چو از ژرف دریا برآید نهنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیاده به کردار یکپاره کوه</p></div>
<div class="m2"><p>ز پانصد سوارش فزونتر شکوه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درشتی که چون پنجه را گرم کرد</p></div>
<div class="m2"><p>به افشردن الماس را نرم کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو عفریتی از بهر خون آمده</p></div>
<div class="m2"><p>ز دهلیز دوزخ برون آمده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی سلسله بسته بر پای او</p></div>
<div class="m2"><p>دراز و قوی هم به بالای او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو شیران وحشی در آن سلسله</p></div>
<div class="m2"><p>جهان کرده پر شور و پر مشغله</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز هر سو که جستی یک آماجگاه</p></div>
<div class="m2"><p>زمین گشتی از زورمندیش چاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سلاحش نه جز آهنی سر به خم</p></div>
<div class="m2"><p>کز او کوه را در کشیدی به هم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز هر سو بدان آهن مرد کش</p></div>
<div class="m2"><p>به مردم کشی دست می‌کرد خوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز سختی که بد خلقت خام او</p></div>
<div class="m2"><p>سفن بسته کیمخت اندام او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو آوردی آهنگ بر کارزار</p></div>
<div class="m2"><p>نکردی براو تیغ پولاد کار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درآمد چنان اژدها باره‌ای</p></div>
<div class="m2"><p>فرشته کشی آدمی خواره‌ای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کسی را که دیدی گرفتی چو مور</p></div>
<div class="m2"><p>به کندی سرش را به یک دست زور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرایش نکردی به کار دگر</p></div>
<div class="m2"><p>گهی پای کندی ز تن گاه سر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز لشگرگه شه به نیروی دست</p></div>
<div class="m2"><p>بسی خلق را پای و پهلو شکست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جریده سواری توانا و چست</p></div>
<div class="m2"><p>به کار مصاف اندر آمد درست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درآمد که گردن فرازی کند</p></div>
<div class="m2"><p>بدان آتش تیز بازی کند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو دیدش ز دور آن نهنگ دمان</p></div>
<div class="m2"><p>گرفتن همان بود و کشتن همان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دگر نامداری درآمد دلیر</p></div>
<div class="m2"><p>هم آوردش آن شیر جنگی به زیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدینگونه از زخمهای درشت</p></div>
<div class="m2"><p>تنی پنجه از نامداران بکشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز بس دل که آن شیر درنده خست</p></div>
<div class="m2"><p>دل شیر مردان لشگر شکست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شگفتی فرو ماند صاحب خرد</p></div>
<div class="m2"><p>که نه آدمی بود و نه دام و دد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شب تیره چون بانگ برزد به روز</p></div>
<div class="m2"><p>سرافکنده شد مهر گیتی فروز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شه از حیرت کار آن اهرمن</p></div>
<div class="m2"><p>سخن راند پوشیده با انجمن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که این آدمی کش چه پتیاره بود</p></div>
<div class="m2"><p>که از جنگ او خلق بیچاره بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سلاحی نه در قبضهٔ دست او</p></div>
<div class="m2"><p>همه با سلاحان شده پست او</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر آنم که او آدمی زاد نیست</p></div>
<div class="m2"><p>وگر هست ازین بوم آباد نیست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز ویرانه جائیست وحشی نهاد</p></div>
<div class="m2"><p>به صورت چو مردم نه مردم نژاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شناسنده‌ای کان زمین را شناخت</p></div>
<div class="m2"><p>به تمکین پاسخ علم بر فراخت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که چون داد فرمان شه دادگر</p></div>
<div class="m2"><p>نمایم بدو حال آن جانور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یکی کوه نزدیک تاریکیست</p></div>
<div class="m2"><p>که راهش چو موئی ز باریکیست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>درو آدمی پیکرانی چنین</p></div>
<div class="m2"><p>به ترکیب خاکی به زور آهنین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نداند کسی اصل ایشان درست</p></div>
<div class="m2"><p>که چون بودشان زاد و بوم از نخست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همه سرخ رویند و پیروزه چشم</p></div>
<div class="m2"><p>ز شیران نترسند هنگام خشم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنان زورمندند و افشرده گام</p></div>
<div class="m2"><p>که یک تن بود لشگری را تمام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگر ماده گر نر بود در ستیز</p></div>
<div class="m2"><p>برانگیزد از عالمی رستخیز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بهر داوری کاوفتد راستند</p></div>
<div class="m2"><p>جز این مذهبی را نیاراستند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ندید است کس مرده ز ایشان یکی</p></div>
<div class="m2"><p>مگر زنده و آن زنده نیز اندکی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بود هر یکی را قدر مایهٔ میش</p></div>
<div class="m2"><p>کزان میش برسازد اسباب خویش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به نیروی پشم است بازارشان</p></div>
<div class="m2"><p>متاعی جز این نیست در بارشان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ندارند گنجینه‌ای هیچکس</p></div>
<div class="m2"><p>سمور سیه را شناسند و بس</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سموری که باشد به خلقت سیاه</p></div>
<div class="m2"><p>نخیزد ز جایی جز آن جایگاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز پیشانی هریک از مردو زن</p></div>
<div class="m2"><p>سرونیست بر رسته چون کرگدن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اگر با سرونشان نباشد سرشت</p></div>
<div class="m2"><p>چه ایشان به صورت چه روسان زشت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کسی را که آید تمنای خواب</p></div>
<div class="m2"><p>شود بر درختی چو پران عقاب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سرون در فشارد به شاخ بلند</p></div>
<div class="m2"><p>چو دیوی بخسبد دران دیو بند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو بینی به شاخی برانگیخته</p></div>
<div class="m2"><p>یکی اژدها بینی آویخته</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بخسبد شبانروزی از بیخودی</p></div>
<div class="m2"><p>که خواب است بنیاد نابخردی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو روسی شبانان بر او بگذرند</p></div>
<div class="m2"><p>دران دیو آویخته بنگرند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به آهستگی سوی آن اهرمن</p></div>
<div class="m2"><p>بیایند و پنهان کنند انجمن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>رسنها ببارند وبندش کنند</p></div>
<div class="m2"><p>ز زنجیر آهن کمندش کنند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>برو چون مسلسل شود بند سخت</p></div>
<div class="m2"><p>کشندش به پنجاه مرد از درخت</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو آن بندی آگاه گردد ز کار</p></div>
<div class="m2"><p>خروشد خروشیدنی رعدوار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گر آن بند را بر تواند شکست</p></div>
<div class="m2"><p>کشد هر یکی را به یک مشت دست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>وگر سخت باشد در آن بستگی</p></div>
<div class="m2"><p>به روی آورندش به آهستگی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>برو بند و زنجیر محکم کنند</p></div>
<div class="m2"><p>وز او آب و نانی فراهم کنند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>برندش به هر کوی و هر خانه‌ای</p></div>
<div class="m2"><p>گشاید از آن دامشان دانه‌ای</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>وگر جنگی افتد به ناچارشان</p></div>
<div class="m2"><p>بدان زنده پیلست پیگارشان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>کشندش به زنجیر چون اژدها</p></div>
<div class="m2"><p>نیارند کردن ز بندش رها</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو گردد چنان آتشی جنگجوی</p></div>
<div class="m2"><p>نماند ز جای در کسی رنگ و بوی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>جهاندار در کار آن پای لغز</p></div>
<div class="m2"><p>ازان داستان ماند شوریده مغز</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به صاحب خبر گفت کاندیشه نیست</p></div>
<div class="m2"><p>همه چوبهٔ تیری ز یک بیشه نیست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گر اقبال من کارسازی کند</p></div>
<div class="m2"><p>سرش بر سر نیزه بازی کند</p></div></div>