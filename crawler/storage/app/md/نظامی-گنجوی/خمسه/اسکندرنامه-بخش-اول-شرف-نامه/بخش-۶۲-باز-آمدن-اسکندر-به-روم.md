---
title: >-
    بخش ۶۲ - باز آمدن اسکندر به روم
---
# بخش ۶۲ - باز آمدن اسکندر به روم

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی آن باده بردار زود</p></div>
<div class="m2"><p>که بی باده شادی نشاید نمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یک جرعه زان باده یاریم ده</p></div>
<div class="m2"><p>ز چنگ اجل رستگاریم ده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مژه تا به‌هم بر زنی روزگار</p></div>
<div class="m2"><p>به صد نیک و بد باشد آموزگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سری را کند بر زمین پای بند</p></div>
<div class="m2"><p>سری را برآرد به چرخ بلند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درآرد ز منظر یکی را به چاه</p></div>
<div class="m2"><p>برآرد ز ماهی یکی را به ماه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کند هر زمان چند بازی بسیچ</p></div>
<div class="m2"><p>سرانجام بازیش هیچست هیچ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از این توسنی به که باشیم رام</p></div>
<div class="m2"><p>که سیلی خورد مرکب بد لگام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو تازی فرس بدلگامی کند</p></div>
<div class="m2"><p>خر مصریان را گرامی کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان در جهان خلق بسیار دید</p></div>
<div class="m2"><p>رمید از همه با کسی نارمید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان آن کسی راست کاندر جهان</p></div>
<div class="m2"><p>شود آگه از کار کارآگهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گزارش چنین شد درین کارآگاه</p></div>
<div class="m2"><p>که چون زد در آن غار شه بارگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسی گنج در کار آن غار کرد</p></div>
<div class="m2"><p>وزان غار شهری چو بلغار کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بلغار فرخ درآمد به روس</p></div>
<div class="m2"><p>براراست آن مرز را چون عروس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وز آنجا درآمد به دریای روم</p></div>
<div class="m2"><p>برون برد کشتی به آباد بوم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بزرگان روم آگهی یافتند</p></div>
<div class="m2"><p>سوی رایت شاه بشتافتند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به شکرانه جان را کشیدند پیش</p></div>
<div class="m2"><p>چو دیدند روی خداوند خویش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه خاک روم از ره آورد شاه</p></div>
<div class="m2"><p>برافروخت چون شب به رخشنده ماه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو یاقوت شد روی هر جوهری</p></div>
<div class="m2"><p>ز یاقوت ظلمات اسکندری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در آرایش آمد همه روی شهر</p></div>
<div class="m2"><p>زمین یافت از گنج پوشیده بهر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بهشتی ز هر قصری انگیختند</p></div>
<div class="m2"><p>زر و سیم را بر زمین ریختند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شکستند قفل در گنج را</p></div>
<div class="m2"><p>جهان قفل بر زد در رنج را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به برج خود آمد فروزنده ماه</p></div>
<div class="m2"><p>بسر بر چو خورشید چینی کلاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شه از روم شد با زمین خویش بود</p></div>
<div class="m2"><p>به روم آمد از آسمان بیش بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو آبی که ابرش به بالا برد</p></div>
<div class="m2"><p>به باز آمدن در به دریا برد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نشست از بر تخت یونان به ناز</p></div>
<div class="m2"><p>برآسود ازان رنج و راه دراز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز دل دامن هفت کشور گذاشت</p></div>
<div class="m2"><p>به هر کشوری نایبی برگماشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ملوک طوایف به فرمان او</p></div>
<div class="m2"><p>کمر بسته بر عهد و پیمان او</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به تشریف او سرفراز آمدند</p></div>
<div class="m2"><p>سوی کشور خویش باز آمدند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جداگانه هرکس به کبر و کشی</p></div>
<div class="m2"><p>برآورده گردن به گردن کشی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کسی گردن خود کسی را نداد</p></div>
<div class="m2"><p>به خود هر کسی گردنی برگشاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به یاد سکندر گرفتند جام</p></div>
<div class="m2"><p>جز او هیچکس را نبردند نام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو شه باز بر تخت یونان رسید</p></div>
<div class="m2"><p>بدو داد گنج سعادت کلید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز دانش بسی مایه ها ساز کرد</p></div>
<div class="m2"><p>در حکمت ایزدی باز کرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو فرمان رسیدش به پیغمبری</p></div>
<div class="m2"><p>نپیچید گردن ز فرمانبری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دگر باره زاد سفر برگرفت</p></div>
<div class="m2"><p>حساب جهان گشتن از سر گرفت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دو نوبت جهان را جهاندار گشت</p></div>
<div class="m2"><p>یکی شهر و کشور یکی کوه و دشت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدین نوبت آن بود کاباد بوم</p></div>
<div class="m2"><p>همه یک به یک دید و آمد به روم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دگر نوبت آن شد که بی‌راه و راه</p></div>
<div class="m2"><p>روان کرد رایت چو خورشید و ماه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو زین بزمگه باز پرداختم</p></div>
<div class="m2"><p>شکر ریز بزمی دگر ساختم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سخنهای بزمی درین نیم درج</p></div>
<div class="m2"><p>بسی کردم از بکر اندیشه خرج</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر آن در که یک یک در او بسته‌ام</p></div>
<div class="m2"><p>به هر مطلعی باز پیوسته‌ام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به یک جای در رشته آرند باز</p></div>
<div class="m2"><p>پر از در شود رشتهٔ عقد ساز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جداگانه فهرست هر پیکری</p></div>
<div class="m2"><p>ز قانون حکمت بود دفتری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همان ساقیان و گزارشگران</p></div>
<div class="m2"><p>که بر هم نشاندم کران تا کران</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نشیننده هر یک ز روی قیاس</p></div>
<div class="m2"><p>چو بر گنج گوهر نگهبان پاس</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>که داند چنین نقشی انگیختن</p></div>
<div class="m2"><p>بدین دلبری رنگی آمیختن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چنان بستم ابریشم ساز او</p></div>
<div class="m2"><p>که از زهره خوشتر شد آواز او</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به جائی که ناراستی یافتم</p></div>
<div class="m2"><p>بر او زیور راستی بافتم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سخن کان نه بر راستی ره برد</p></div>
<div class="m2"><p>بود خوار اگر پایه بر مه برد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کجا پیش پیرای پیر کهن</p></div>
<div class="m2"><p>غلط رانده بود از درستی سخن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>غلط گفته را تازه کردم طراز</p></div>
<div class="m2"><p>بدین عذر وا گفتم آن گفته باز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو شد نیمه‌ای ز این بنا مهره بست</p></div>
<div class="m2"><p>مرا نیمهٔ عالم آمد به دست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دگر نیمه را گر بود روزگار</p></div>
<div class="m2"><p>چنان گویم از طبع آموزگار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>که خواننده را سر برآرد ز خواب</p></div>
<div class="m2"><p>به رقص آورد ماهیان را در آب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>زمانه گرم داد خواهد امان</p></div>
<div class="m2"><p>چنین آمد اندیشه را در گمان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>که در باغ این نقش رومی نورد</p></div>
<div class="m2"><p>گل سرخ رویانم از خاک زرد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کنم گنجی از سفته طبع پر</p></div>
<div class="m2"><p>چو فیروزه فیروز و دری چو در</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز هر باغی آرم گلی نغز بوی</p></div>
<div class="m2"><p>ز هر گل گلابی درآرم به جوی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گر اقبال شه باشدم دستگیر</p></div>
<div class="m2"><p>سخن زود گردد گزارش پذیر</p></div></div>