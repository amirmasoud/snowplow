---
title: >-
    بخش ۴۲ - رفتن اسکندر از هندوستان به چین
---
# بخش ۴۲ - رفتن اسکندر از هندوستان به چین

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی آن آب چون ارغوان</p></div>
<div class="m2"><p>کزو پیر فرتوت گردد جوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به من ده که تا زو جوانی کنم</p></div>
<div class="m2"><p>گل زرد را ارغوانی کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سعادت به ما روی بنمود باز</p></div>
<div class="m2"><p>نوازندهٔ ساز بنواخت ساز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن را گزارش به یاری رسید</p></div>
<div class="m2"><p>سخن گو به امیدواری رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گزارش کنان تیز کن مغز را</p></div>
<div class="m2"><p>گزارش ده این نامهٔ نغز را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبرده جهاندار فرخ نبرد</p></div>
<div class="m2"><p>خبر ده که با فور فوران چه کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گزارندهٔ حرف این حسب حال</p></div>
<div class="m2"><p>ز پرده چنین می‌نماید خیال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که چون شاه فارغ شد از کار کید</p></div>
<div class="m2"><p>گهی رای می‌کرد و گه رای صید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روان کرد لشگر به تاراج فور</p></div>
<div class="m2"><p>ز پیروزیش کرد یکباره دور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو شه تیغ را برکشید از نیام</p></div>
<div class="m2"><p>بداندیش را سر درآمد به دام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه ملک و مالش به تاراج داد</p></div>
<div class="m2"><p>سرش را ز شمشیر خود تاج داد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو افتاده شد خصم در پای او</p></div>
<div class="m2"><p>به دیگر کسی داده شد جای او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وز آنجا به رفتن علم برفراخت</p></div>
<div class="m2"><p>که آن خاک با باد پایان نساخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سه چیز است کان در سه آرامگاه</p></div>
<div class="m2"><p>بود هر سه کم عمر و گردد تباه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به هندوستان اسب و در پارس پیل</p></div>
<div class="m2"><p>به چین گربه زینسان نماید دلیل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جهاندار چون دید کان آب و خاک</p></div>
<div class="m2"><p>ز پوینده اسبان برآرد هلاک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز هندوستان شد به تبت زمین</p></div>
<div class="m2"><p>ز تبت درآمد به اقصای چین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو بر اوج تبت رسید افسرش</p></div>
<div class="m2"><p>به خنده درآمد همه لشگرش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بپرسید کاین خنده از بهر چیست</p></div>
<div class="m2"><p>بجایی‌که بر خود بباید گریست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نمودند کین زعفران گونهٔ خاک</p></div>
<div class="m2"><p>کند مرد را بی سبب خنده ناک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عجب ماند شه زان بهشتی سواد</p></div>
<div class="m2"><p>که چون آورد خندهٔ بی‌مراد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به دشواری راه بر خشک وتر</p></div>
<div class="m2"><p>همی برد منزل به منزل به سر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ره از خون جنبندگان خشک دید</p></div>
<div class="m2"><p>همه دشت بر نافهٔ مشک دید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو دید آهوی دشت را نافه‌دار</p></div>
<div class="m2"><p>نفرمود کاهو کند کس شکار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به هر جا که لشگر گذر داشتی</p></div>
<div class="m2"><p>به خروارها نافه برداشتی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو لختی بیابان چین درنوشت</p></div>
<div class="m2"><p>به آبادی آمد ز ویرانه دشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو مینا چراگاهی آمد پدید</p></div>
<div class="m2"><p>که از خرمی سر به مینو کشید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به هر پنج گامی در آن مرغزار</p></div>
<div class="m2"><p>روانه شده چشمه‌ای خوشگوار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هوای خوش و بیشه‌های فراخ</p></div>
<div class="m2"><p>درختان بارآور سبز شاخ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>روان آب در سبزهٔ آبخورد</p></div>
<div class="m2"><p>چو سیماب در پیکر لاجورد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گیاهان نو رسته از قطره پر</p></div>
<div class="m2"><p>چو بر شاخ مینا برآموده در</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پی آهو از چشمه انگیخته</p></div>
<div class="m2"><p>چو بر نیفه‌ها نافه‌ها ریخته</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سم گور بر سبزه خاریده جای</p></div>
<div class="m2"><p>چو بر سبز دیبا خط مشک سای</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سوادی که در وی سیاهی نبود</p></div>
<div class="m2"><p>وگر بود جز پشت ماهی نبود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سکندر چو دید آن سواد بهی</p></div>
<div class="m2"><p>ز سودای هندوستان شد تهی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در آب و چراگاه آن مرحله</p></div>
<div class="m2"><p>بفرمود کردن ستوران یله</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یکی هفته از خرمی یافت بهر</p></div>
<div class="m2"><p>بر آسود با پهلوانان دهر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دگر هفته روزی پسندیده جست</p></div>
<div class="m2"><p>کزو فال فیروزی آید درست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بفرمود تا کوس بنواختند</p></div>
<div class="m2"><p>از آن مرحله سوی چین تاختند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دهلزن چو شد بر دهل خشمناک</p></div>
<div class="m2"><p>برآورد فریاد از باد پاک</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو آیینهٔ چینی آمد پدید</p></div>
<div class="m2"><p>سکندر سپه را سوی چین کشید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نشستند بر تازی تیز جوش</p></div>
<div class="m2"><p>همه خاره خفتان و پولاد پوش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هوای خوش و راه بیخار بود</p></div>
<div class="m2"><p>وگر بود خار انگبین دار بود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز شیرین گیاهان کوه و دره</p></div>
<div class="m2"><p>شکر یافته شیر آهو بره</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بر آن صیدگه چون گذر کرد شاه</p></div>
<div class="m2"><p>معنبر شد از گرد او صیدگاه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هر آهو که با داغ او زاده بود</p></div>
<div class="m2"><p>زنافه کشی نافش افتاده بود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گوزنی کزو روی بر خاک داشت</p></div>
<div class="m2"><p>به چشمش جهان چشم تریاک داشت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جهانجوی می‌شد چو غرنده شیر</p></div>
<div class="m2"><p>جهنده هژبری شکاری به زیر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شکار افکنان در بیابان چین</p></div>
<div class="m2"><p>بپرداخت از گور و آهو زمین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>حریر زمین زیر سم ستور</p></div>
<div class="m2"><p>شده گور چشم از بسی چشم گور</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به مقراضهٔ تیر پهلو شکاف</p></div>
<div class="m2"><p>بسی آهو افکنده با نافهٔ ناف</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ادیم گوزنان سرین تا بسر</p></div>
<div class="m2"><p>ز پیکان زر گشته چون کان زر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کمان شهنشه کمین ساخته</p></div>
<div class="m2"><p>گوزنی به هر تیری انداخته</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به نقاشی نوک تیر خدنگ</p></div>
<div class="m2"><p>تهی کرده صحرای چین را ز رنگ</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به نخجیر کرد در آن صیدگاه</p></div>
<div class="m2"><p>یکی روز تا شب بسر برد راه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو ترک حصاری ز کار اوفتاد</p></div>
<div class="m2"><p>عروس جهان در حصار اوفتاد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>زسودای او شب چو هندو زنی</p></div>
<div class="m2"><p>شده جو زنان گرد هر برزنی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>شهنشه فرود آمد از بارگی</p></div>
<div class="m2"><p>همان لشگرش نیز یکبارگی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به تدبیر آسایش آورد رای</p></div>
<div class="m2"><p>نجنبید تا روز مرغی ز جای</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو خاتون یغما به خلخال زر</p></div>
<div class="m2"><p>زخرگاه خلخ برآورد سر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>جهانی چو هندو به دود افکنی</p></div>
<div class="m2"><p>چو یغما و خلخ شد از روشنی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زکوس شهنشه برآمد خروش</p></div>
<div class="m2"><p>به یغما و خلخ در افتاد جوش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>شه عالم آهنج گیتی نورد</p></div>
<div class="m2"><p>در آن خاک یکماه کرد آبخورد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>طویله زدند آخر انگیختند</p></div>
<div class="m2"><p>به سبز آخران برعلف ریختند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>خبر شد به خاقان که صحرا و کوه</p></div>
<div class="m2"><p>شد از نعل پولاد پوشان ستوه</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>درآمد یکی سیل از ایران زمین</p></div>
<div class="m2"><p>که نه چین گذارد نه خاقان چین</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شتابنده سیلی که برکوه و دشت</p></div>
<div class="m2"><p>زطوفان پیشینه خواهد گذشت</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تگرگش زمین را ثریا کند</p></div>
<div class="m2"><p>هلاک نهنگان دریا کند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>سیاه اژدهائی که در هیچ بوم</p></div>
<div class="m2"><p>نیامد چو او تند شیری ز روم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>حبش داغ بر روی فرمان اوست</p></div>
<div class="m2"><p>سیه پوشی زنگ از افغان اوست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به دارا رسانید تاراج را</p></div>
<div class="m2"><p>ز شاهان هندو ستد تاج را</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چو فارغ شد از غارت فوریان</p></div>
<div class="m2"><p>کمر بست بر کین فغفوریان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>گر آن ژرف دریا درآید ز جای</p></div>
<div class="m2"><p>ندارد دران داوری کوه پای</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بترسید خاقان و زد رای ترس</p></div>
<div class="m2"><p>که بود از چنان دشمنی جای ترس</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به هر مرزبان خطی از خان نبشت</p></div>
<div class="m2"><p>که در مرز ما خاک با خون سرشت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ز شاه خطا تا به خان ختن</p></div>
<div class="m2"><p>فرستاد و ترتیب کرد انجمن</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سپاه سپنجاب و فرغانه را</p></div>
<div class="m2"><p>دگر مرزداران فرزانه را</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ز خرخیز و از چاچ و از کاشغر</p></div>
<div class="m2"><p>بسی پهلوان خواند زرین کمر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چو عقد سپه برهم آموده شد</p></div>
<div class="m2"><p>دل خان خانان برآسوده شد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>به کوه رونده درآورد پای</p></div>
<div class="m2"><p>چو پولاد کوهی روان شد ز جای</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>دو منزل کم و بیش نزدیک شاه</p></div>
<div class="m2"><p>طویله فرو بست و زد بارگاه</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>شب و روز پرسیدی از شهریار</p></div>
<div class="m2"><p>که با او چه شب بازی آرد به کار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نهان رفته جاسوس را باز جست</p></div>
<div class="m2"><p>که تا حال او بازگوید درست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>خبر دادش آن مرد پنهان پژوه</p></div>
<div class="m2"><p>که شاهیست با شوکت و با شکوه</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>دها و دهش دارد و مردمی</p></div>
<div class="m2"><p>فرشته است در صورت آدمی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>خردمند و آهسته و تیزهوش</p></div>
<div class="m2"><p>به خلوت سخنگو به زحمت خموش</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به سنگ و سکونت برآرد نفس</p></div>
<div class="m2"><p>نکوشد به تعجیل در خون کس</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ستم را زبان عدل را سود ازو</p></div>
<div class="m2"><p>خدا راضی و خلق خشنود ازو</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>نیارد زکس جز به نیکی به یاد</p></div>
<div class="m2"><p>نگردد به اندوه کس نیز شاد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ندیدم کسی کو بر او دست برد</p></div>
<div class="m2"><p>نه مردانه‌ای کو ز بیمش نمرد</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>مگر تیرش از جعبه آرشست</p></div>
<div class="m2"><p>که از نوک او خاره با خارشست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چو شمشیر گیرد بود چون درخش</p></div>
<div class="m2"><p>چو می بر کف آرد شود گنج بخش</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چو نقد سخن در عیار آورد</p></div>
<div class="m2"><p>همه مغز حکمت به کار آورد</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>سخن نشنود کان نباشد درست</p></div>
<div class="m2"><p>نگیرد پذیرفتهٔ خویش سست</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>به هر جایگه رونق‌انگیز کار</p></div>
<div class="m2"><p>بجز در شبستان و جز در شکار</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>به نخجیر کردن ندارد درنگ</p></div>
<div class="m2"><p>شکیبا بود چون رسد وقت جنگ</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>جهان ایمن از دانش و داد او</p></div>
<div class="m2"><p>ملک بر ملک زاد بر زاد او</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>به میدان سر شهسواران بود</p></div>
<div class="m2"><p>به مستی به از هوشیاران بود</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چو خندد خیالی غریب آیدش</p></div>
<div class="m2"><p>چو طیبت کند بوی طیب آیدش</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>فراوان شکیبست و اندک سخن</p></div>
<div class="m2"><p>گه راستی راست چون سرو بن</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>سیاست کند چون شود کینه‌ور</p></div>
<div class="m2"><p>ببخشاید آنگه که یابد ظفر</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>لبش در سخن موج طوفان زند</p></div>
<div class="m2"><p>همه رای با فیلسوفان زند</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>به تدبیر پیران کند کارها</p></div>
<div class="m2"><p>جوانان برد سوی پیگارها</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>پناهد به ایزد به بیگاه و گاه</p></div>
<div class="m2"><p>نیفتد به بد مرد ایزد پناه</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>چو در زین کشد سرو آزاد را</p></div>
<div class="m2"><p>بر اسبی که پیل افکند باد را</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>هم آورد او گر بود زنده پیل</p></div>
<div class="m2"><p>کم از قطره باشد بر رود نیل</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>مبادا که اسبش حروفی کند</p></div>
<div class="m2"><p>که از چرم شیر اسب خونی کند</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>پس و پیش چنبر جهاند چو مار</p></div>
<div class="m2"><p>چب و راست آتش زند چون شرار</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>ملوکی کز افسر نشان داشتند</p></div>
<div class="m2"><p>جهان را به لشگر کشان داشتند</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>جز او نیست در لشگرش تیغزن</p></div>
<div class="m2"><p>زهی لشگر آرای لشگر شکن</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>نیندیشد از هیچ خونخواره‌ای</p></div>
<div class="m2"><p>مگر کز ضعیفی و بیچاره‌ای</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>فراخ افکند بارگه را بساط</p></div>
<div class="m2"><p>به اندازه خندد چو یابد نشاط</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>نبیند ز تعظیم خود در کسی</p></div>
<div class="m2"><p>چو بیند نوازش نماید بسی</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>خزینه است بخشیدن گوهرش</p></div>
<div class="m2"><p>طویله بود دادن استرش</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>به خواهندگان گر کسی زر دهد</p></div>
<div class="m2"><p>به جای زر او شهر و کشور دهد</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>مرادی که آرد دلش در شمار</p></div>
<div class="m2"><p>دهد روزگارش به کم روزگار</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>چو خاقان خبر یافت زان بخردی</p></div>
<div class="m2"><p>شکوهید از آن فره ایزدی</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>به آزرم خسرو دلش نرم شد</p></div>
<div class="m2"><p>بسیچش به دیدار او گرم شد</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>بر اندیشهٔ جنگ بر بست راه</p></div>
<div class="m2"><p>بهانه طلب کرد بر صلح شاه</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>به شاه جهان قصه برداشتند</p></div>
<div class="m2"><p>که ترکان چین رایت افراشتند</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>شهنشه مثل زد که نخجیر خام</p></div>
<div class="m2"><p>به پای خود آن به که آید به دام</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>اگر با من او هم‌نبردی کند</p></div>
<div class="m2"><p>نه مردی که آزاد مردی کند</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>مراد شما را سبک راه کرد</p></div>
<div class="m2"><p>به ما بر ره دور کوتاه کرد</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>چنان آرمش چین در ابروی تنگ</p></div>
<div class="m2"><p>که در چین بگرید بر او خاره سنگ</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>سپیده دمان کز سپهر کبود</p></div>
<div class="m2"><p>رسانید خورشید شه را درود</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>دبیر عطارد منش را نشاند</p></div>
<div class="m2"><p>که بر مشتری زهره داند فشاند</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>یکی نامه درخواست آراسته</p></div>
<div class="m2"><p>فروزان‌تر از ماه ناکاسته</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>سخن ساخته در گزارش دو نیم</p></div>
<div class="m2"><p>یکی نیمه ز امید ودیگر ز بیم</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>دبیر قلمزن قلم برگرفت</p></div>
<div class="m2"><p>نخستین سخن ز افزین درگرفت</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>جهان آفریننده را کرد یاد</p></div>
<div class="m2"><p>که بی یاد او آفرینش مباد</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>خدائی که امید و آرام ازوست</p></div>
<div class="m2"><p>دل مرد جوینده را کام ازوست</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>به بیچارگی چارهٔ کار ما</p></div>
<div class="m2"><p>درآب و در آتش نگهدار ما</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>چو بخشش کند ره نماید به گنج</p></div>
<div class="m2"><p>چو بخشایش آرد رهاند ز رنج</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>جهان را نبود از بنه هیچ ساز</p></div>
<div class="m2"><p>بفرمان او نقش بست این طراز</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>گزیده کسی کو به فرمان اوست</p></div>
<div class="m2"><p>بر او آفرین کافرین خوان اوست</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>چو کلک از سر نامه پرداختند</p></div>
<div class="m2"><p>سخن بر زبان شه انداختند</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>که این نامه ز اسکندر چیره دست</p></div>
<div class="m2"><p>به خاقان که بادا سکندر پرست</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>به فرمان دارای چرخ کبود</p></div>
<div class="m2"><p>ز ما باد بر جان خاقان درود</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>چنان داند آن خسرو داد بخش</p></div>
<div class="m2"><p>که چون ما درین بوم راندیم رخش</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>نه بر جنگ از ایران زمین آمدیم</p></div>
<div class="m2"><p>به مهمان خاقان چین آمدیم</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>بدان دل که از راه فرمانبری</p></div>
<div class="m2"><p>کند میهمان را پرستشگری</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>به شهر شما گر بلند آفتاب</p></div>
<div class="m2"><p>ز مشرق کند سوی مغرب شتاب</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>من آن آفتابم که اینک ز راه</p></div>
<div class="m2"><p>زمغرب به مشرق کشیدم سپاه</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>سیه تا سپیدی گرفتم به تیغ</p></div>
<div class="m2"><p>بدادم به خواهندگان بی‌دریغ</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>ز حد حبش عزم چین ساختم</p></div>
<div class="m2"><p>زمغرب به مشرق زمین تاختم</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>ز پایینگه آفتاب بلند</p></div>
<div class="m2"><p>سوی جلوه گاهش رساندم سمند</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>به هندوستان کاشتم مشک بید</p></div>
<div class="m2"><p>بکارم به چین یاسمین سپید</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>اگر ترسی از پیچ دوران من</p></div>
<div class="m2"><p>مپیچان سر از خط فرمان من</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>وگر پیچی از امر من رای و هوش</p></div>
<div class="m2"><p>بپیچاندت چرخ گردنده گوش</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>به جائی میاور که این تند شیر</p></div>
<div class="m2"><p>به نخجیر گوران دراید دلیر</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>بگردان پی شیر ازین بوستان</p></div>
<div class="m2"><p>مده پیل را یاد هندوستان</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>بلا بر سر خود فرود آورند</p></div>
<div class="m2"><p>که بر یاد مستان سرود آورند</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>ببین تا ز شمشیر من روز جنگ</p></div>
<div class="m2"><p>چه دریای خون شد به صحرای زنگ</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>چگونه ز دارا نشاندم غرور</p></div>
<div class="m2"><p>چه کردم بجای فرومایه فور</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>دگر خسروان را به نیروی بخت</p></div>
<div class="m2"><p>به سر چون درآوردم از تاج و تخت</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>گر ایدون که آید فریدون به من</p></div>
<div class="m2"><p>گرفتار گردد همیدون به من</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>به هر مرز و بومی که من تاختم</p></div>
<div class="m2"><p>ز بیگانه آن خانه پرداختم</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>کسی گو مرا نیکخواهی نمود</p></div>
<div class="m2"><p>ز من هیچ بدخواهی او را نبود</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>چو دادم کسی را به خود زینهار</p></div>
<div class="m2"><p>نگشتم بر آن گفته زنهار خوار</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>زبانم چو بر عهد شد رهنمون</p></div>
<div class="m2"><p>نبردم سر از عهد و پیمان برون</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>به یغما و چین زان نیارم نشست</p></div>
<div class="m2"><p>که یغمائی و چینی آرم به دست</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>مرا خود بسی در دریائیست</p></div>
<div class="m2"><p>غلامان چینی و یغمائیست</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>به زیر آمدن ز آسمان بر زمین</p></div>
<div class="m2"><p>بسی بهتر از ملک ایران به چین</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>چه داری تو ای ترک چین در دماغ</p></div>
<div class="m2"><p>که بر باد صرصر کشانی چراغ</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>به جای فرستادن نزل و گنج</p></div>
<div class="m2"><p>چرا با هزبران شدی کینه سنج</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>فرود آمدن چیست بر طرف راه</p></div>
<div class="m2"><p>چو سد سکندر کشیدن سپاه</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>اگر قصد پیکار ما ساختی</p></div>
<div class="m2"><p>بخوری بر آتش برانداختی</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>وگر پیش اقبال باز آمدی</p></div>
<div class="m2"><p>کجا عذر اگر عذر ساز آمدی</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>خبر ده مرا تا بدانم شمار</p></div>
<div class="m2"><p>که در سلهٔ مارست یا مهرهٔ مار</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>سپاه از صبوری به جوش آمدند</p></div>
<div class="m2"><p>ز تقصیر من در خروش آمدند</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>هزبرانم آهوی چین دیده‌اند</p></div>
<div class="m2"><p>کم آهوی فربه چنین دیده‌اند</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>بریدند زنجیر شیران من</p></div>
<div class="m2"><p>دلیرند بر خون دلیران من</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>پرتیر و منقار پیکان تیز</p></div>
<div class="m2"><p>کنند از شغب جعبه را ریز ریز</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>سنان چشم در راه این دشمسنت</p></div>
<div class="m2"><p>گر آنجا منی گر ز من صد منست</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>غلامان ترکم چو گیرند شست</p></div>
<div class="m2"><p>ز تیری رسد لشگری را شکست</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>اگر خسرو شست میران بود</p></div>
<div class="m2"><p>هم آماج این شست گیران بود</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>چو بر دودهٔ دود من برگذشت</p></div>
<div class="m2"><p>اگر نقش چین بود شد دود دشت</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>ز پیوند آزرم چون بگذرم</p></div>
<div class="m2"><p>مباد آبم ار با کس آبی خورم</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>سنانم چنان اژدها را خورد</p></div>
<div class="m2"><p>که طوفان آتش گیا را خورد</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>چو تیرم گذر بر دلیران کند</p></div>
<div class="m2"><p>نشانه ز پهلوی شیران کند</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>گرم ژرف دریا بود هم نبرد</p></div>
<div class="m2"><p>ز دریا برآرم بر شمشیر گرد</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>وگر کوه باشد بجوشانمش</p></div>
<div class="m2"><p>به زنگار آهن بپوشانمش</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>بهم پنجهٔ پیل را بشکنم</p></div>
<div class="m2"><p>شه پیلتن بلکه پیل افکنم</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>سرین خوردن گور و پشت گوزن</p></div>
<div class="m2"><p>ندارد بر شیر درنده وزن</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>چو شاهین بحری درآید به کار</p></div>
<div class="m2"><p>دهد ماهیان را ز مرغان شکار</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>شما ماهیانید بی پا و چنگ</p></div>
<div class="m2"><p>مرا اژدها در دهن چو نهنگ</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>سگان نیز کان استخوان می‌خورند</p></div>
<div class="m2"><p>به دندان چون تیغ نان می‌خورند</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>به هر جا که نیروی من پی فشرد</p></div>
<div class="m2"><p>مرا بود پیروزی و دستبرد</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>چو کین آوری کین ستانی کنم</p></div>
<div class="m2"><p>شوی مهربان مهربانی کنم</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>اگر گوهرت باید و گر نهنگ</p></div>
<div class="m2"><p>ز دریای من هر دو آید به چنگ</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>ندیدی مگر تیغم انگیخته</p></div>
<div class="m2"><p>نهنگی و گوهر بر او ریخته</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>من آن گنج و آن اژدها پیکرم</p></div>
<div class="m2"><p>که زهر است و پازهر در ساغرم</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>به نزد تو از گنج و از اژدها</p></div>
<div class="m2"><p>خبر ده به من تا چه آرد بها</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>گر آیی تنت در پرند آورم</p></div>
<div class="m2"><p>وگر نی سرت زیر بند آورم</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>درشتی و نرمی نمودم تو را</p></div>
<div class="m2"><p>بدین هر دو قول آزمودم تو را</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>اگر پای خاکی کنی بر درم</p></div>
<div class="m2"><p>چو خورشید بر خاک چین بگذرم</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>و گر نی دراندازم از راه کین</p></div>
<div class="m2"><p>همه خاک چین را به دریای چین</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>چو نامه بخوانی نسازی درنگ</p></div>
<div class="m2"><p>نمائی به من صورت صلح و جنگ</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>تغافل نسازی که سیلاب نیز</p></div>
<div class="m2"><p>به جوشست در ابر سیلاب ریز</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>زبان دان یکی مرد مردم شناس</p></div>
<div class="m2"><p>طلب کرد کز کس ندارد هراس</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>فرستاد تا نامهٔ نغز برد</p></div>
<div class="m2"><p>به مهر سکندر به خاقان سپرد</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>چو خاقان فرو خواند عنوان شاه</p></div>
<div class="m2"><p>فرو خواست افتادن از اوج گاه</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>از آن هیبتش در دل آمد هراس</p></div>
<div class="m2"><p>که زیرک منش بود و زیرک شناس</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>دو پیکر خیالی بر او بست راه</p></div>
<div class="m2"><p>که بر شه زنم یا شوم نزد شاه</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>دو رنگی در اندیشه تاب آورد</p></div>
<div class="m2"><p>سر چاره گر زیر خواب آورد</p></div></div>