---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>دارم گه و بیگه ز که و مه کم و بیش</p></div>
<div class="m2"><p>نفع و ضرر و خیر ز بیگانه و خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینطرفه که آندوست چو دشمن مه و سال</p></div>
<div class="m2"><p>گوید بد و نیکم شب و روز از پس و پیش</p></div></div>