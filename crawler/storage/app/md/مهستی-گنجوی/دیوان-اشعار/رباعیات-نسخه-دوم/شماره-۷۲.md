---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>اشکم ز دو دیده متصل می آید</p></div>
<div class="m2"><p>از بهر تو ای مهر گسل می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار بدار حرمت اشک مرا</p></div>
<div class="m2"><p>کاین قافله از کعبه دل می آید</p></div></div>