---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>زلف تو چو آه من درازی دارد</p></div>
<div class="m2"><p>خلق خوش تو بنده نوازی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم خوش تو دوست از آن میدارم</p></div>
<div class="m2"><p>کز منت سرمه بی نیازی دارد</p></div></div>