---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>چون در دلت آن بود که گیری یاری</p></div>
<div class="m2"><p>برگردی ازین دل شده بی آزاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون روز وداع بود بایستی گفت</p></div>
<div class="m2"><p>تا سیر ترت دیده بدیدی باری</p></div></div>