---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>گه خصم شوی مرا و گه یار آئی</p></div>
<div class="m2"><p>روزی بهزار گونه در کار آئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایدوست نترسی که بخون دل من</p></div>
<div class="m2"><p>روزی به چنین شبی گرفتار آئی</p></div></div>