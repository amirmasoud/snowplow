---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>ای روی تو از تازه گل بربر به</p></div>
<div class="m2"><p>وز چین و خط از خلخ و از بربر به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد بنده بربری تو را بنده شده</p></div>
<div class="m2"><p>بر بربر بنده نه که بر بربر به</p></div></div>