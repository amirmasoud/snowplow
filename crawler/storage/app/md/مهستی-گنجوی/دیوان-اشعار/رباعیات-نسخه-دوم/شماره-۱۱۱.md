---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>لعل تو مکیدن آرزو می کردم</p></div>
<div class="m2"><p>می با تو کشیدن آرزو می کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مستی و در جنون و در هشیاری</p></div>
<div class="m2"><p>چنگ تو شنیدن آرزو می کردم</p></div></div>