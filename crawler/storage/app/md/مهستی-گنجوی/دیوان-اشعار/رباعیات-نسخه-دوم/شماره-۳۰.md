---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>آتش بوزید و جامه شوم بسوخت</p></div>
<div class="m2"><p>وز جامه شوم نیمه روم بسوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر پای بدم که شمع را بنشانم</p></div>
<div class="m2"><p>آتش ز سر شمع همه موم بسوخت</p></div></div>