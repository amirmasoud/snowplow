---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>در طاس فلک نقش قضا و قدر است</p></div>
<div class="m2"><p>مشکل گرهیست، خلق ازین بیخبر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پندار مدار کین گره بگشائی</p></div>
<div class="m2"><p>دانستن این گره بقدر بشر است</p></div></div>