---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>با خلق بداوری بود قاضی چرخ</p></div>
<div class="m2"><p>وز علم و عمل بری بود قاضی چرخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر مشته اگر می برید نیست عجب</p></div>
<div class="m2"><p>ز آنروی که مشتری بود قاضی چرخ</p></div></div>