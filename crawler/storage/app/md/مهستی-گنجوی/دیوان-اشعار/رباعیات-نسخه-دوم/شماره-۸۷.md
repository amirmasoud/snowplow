---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>نسرین تو زد پریر بر من آذر</p></div>
<div class="m2"><p>دی باد ز سنبلت، مرا داد خبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز در آبم از تو چون نیلوفر</p></div>
<div class="m2"><p>فردا ز گل تو خاک ریزم بر سر</p></div></div>