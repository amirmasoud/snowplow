---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>شهری زن و مرد در رخت مینگرند</p></div>
<div class="m2"><p>وز سوز غم عشق تو جان در خطرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جامه که سالی پدرت بفروشد</p></div>
<div class="m2"><p>از دست تو عاشقان بروزی بدرند</p></div></div>