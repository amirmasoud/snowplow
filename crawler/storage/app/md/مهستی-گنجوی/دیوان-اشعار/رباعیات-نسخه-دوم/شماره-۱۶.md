---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>هنگام صبوح، گر بت حور سرشت</p></div>
<div class="m2"><p>پر می قدحی بمن دهد بر لب کشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که از من این سخن باشد زشت</p></div>
<div class="m2"><p>سگ به ز من ار هیچ کنم یاد بهشت</p></div></div>