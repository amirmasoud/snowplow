---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>ایام بر آنست که تا بتواند</p></div>
<div class="m2"><p>یک روز مرا بکام دل ننشاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عهدی دارد فلک که تا گرد جهان</p></div>
<div class="m2"><p>خود می گردد، مرا همی گرداند</p></div></div>