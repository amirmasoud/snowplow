---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>تا سنبل تو غالیه سائی نکند</p></div>
<div class="m2"><p>باد سحری نافه گشائی نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زاهد صد ساله ببیند دستت</p></div>
<div class="m2"><p>در گردن من که پارسائی نکند</p></div></div>