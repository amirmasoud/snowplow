---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>ای عقرب زلفت زده بر جانم نیش</p></div>
<div class="m2"><p>تیر قد تو مرا بر آورده ز کیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد خط تو توقیع سلاطین ز آنروی</p></div>
<div class="m2"><p>شرحست توکلت علی الله معنیش</p></div></div>