---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>آنی که به لطف تو سراسر نمکی</p></div>
<div class="m2"><p>چون بر گل تازه برچکیده نمکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شیر ز پستان لطافت نمکی</p></div>
<div class="m2"><p>پیغمبری ای دوست ولیکن نه مکی</p></div></div>