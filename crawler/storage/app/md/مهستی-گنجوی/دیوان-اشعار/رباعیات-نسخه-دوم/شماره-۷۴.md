---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>چون پوست کشد کارد بدندان گیرد</p></div>
<div class="m2"><p>آهن ز لبش قیمت مرجان گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او کارد بدست خویش میزان گیرد</p></div>
<div class="m2"><p>تا جان گیرد هر آنچه با جان گیرد</p></div></div>