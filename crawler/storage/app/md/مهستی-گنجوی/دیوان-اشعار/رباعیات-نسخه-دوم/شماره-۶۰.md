---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>هر کارد که از کشته خود برگیرد</p></div>
<div class="m2"><p>و اندر لب و دندان چو شکر گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بار دگر بر گلوی کشته نهد</p></div>
<div class="m2"><p>از ذوق لبش زندگی از سرگیرد</p></div></div>