---
title: >-
    شمارهٔ ۱۵۸
---
# شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>هان تا به خرابات مجازی نائی</p></div>
<div class="m2"><p>تا کار قلندری نسازی نائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این جا، ره رندان سراندازانست</p></div>
<div class="m2"><p>جانبازانند تا نبازی نائی</p></div></div>