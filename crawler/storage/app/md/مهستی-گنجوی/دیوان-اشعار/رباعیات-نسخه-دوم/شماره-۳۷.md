---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>آن کودک نعلبند و داس اندر دست</p></div>
<div class="m2"><p>چون نعل بر اسب بست از پای نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین نادره تر که دید، در عالم پست</p></div>
<div class="m2"><p>بدری بسم اسب هلالی بر بست</p></div></div>