---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>هم مستم و هم غلام سرمستانم</p></div>
<div class="m2"><p>بیزار ز زهد و بنده ایمانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بنده آن دمم که ساقی گوید</p></div>
<div class="m2"><p>یک جام دگر بگیر و من نستانم</p></div></div>