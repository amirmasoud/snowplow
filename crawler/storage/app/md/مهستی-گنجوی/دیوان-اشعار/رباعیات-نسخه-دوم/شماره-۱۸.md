---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>در عالم جان خطبه بنام خط اوست</p></div>
<div class="m2"><p>صبح دل عشاق ز شام خط اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تشبیه خطش بمشک می کردم، عقل</p></div>
<div class="m2"><p>گفتا، غلطی، مشک غلام خط اوست</p></div></div>