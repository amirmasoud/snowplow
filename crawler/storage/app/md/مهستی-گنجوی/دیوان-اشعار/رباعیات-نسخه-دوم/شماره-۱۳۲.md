---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>رفتی پسرا دوش بمی نوشیدن</p></div>
<div class="m2"><p>بودت هوس یار اگر ورزیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی تو بکنده اند معلومم شد</p></div>
<div class="m2"><p>من روی نو کنده چون توانم دیدن</p></div></div>