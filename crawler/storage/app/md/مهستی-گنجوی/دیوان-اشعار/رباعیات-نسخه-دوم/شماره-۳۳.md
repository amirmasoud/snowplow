---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>در آتش دل پریر بودم بنهفت</p></div>
<div class="m2"><p>دی باد صبا خوش سخنی با من گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کامروز هر آنکه آبروئی دارد</p></div>
<div class="m2"><p>فرداش بخاک تیره می باید خفت</p></div></div>