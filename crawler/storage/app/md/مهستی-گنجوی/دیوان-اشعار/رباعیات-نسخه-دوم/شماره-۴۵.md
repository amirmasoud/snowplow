---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>. . . چاه عقیقیست پناهی دهدت</p></div>
<div class="m2"><p>رز بالش نقره تکیه گاهی دهدت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه نقطه سیماب چو ریزی در وی</p></div>
<div class="m2"><p>نه ماه شود چهارده ماهی دهدت</p></div></div>