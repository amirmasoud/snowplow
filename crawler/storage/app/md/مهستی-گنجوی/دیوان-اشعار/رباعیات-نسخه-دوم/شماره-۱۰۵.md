---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>هر جوی که از چهره بناخن کندم</p></div>
<div class="m2"><p>از دیده کنون آب در او می بندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی آبی روی بود ار یکچندم</p></div>
<div class="m2"><p>آب از مژه بر روی از آن می بندم</p></div></div>