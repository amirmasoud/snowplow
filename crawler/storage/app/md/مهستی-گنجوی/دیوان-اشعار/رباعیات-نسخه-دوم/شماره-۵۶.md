---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>شاهان چو بروز بزم ساغر گیرند</p></div>
<div class="m2"><p>بر ساز و نوای چنگ چاکر گیرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست چو منی که پای بند طربست</p></div>
<div class="m2"><p>در خام ندوزند که در زر گیرند</p></div></div>