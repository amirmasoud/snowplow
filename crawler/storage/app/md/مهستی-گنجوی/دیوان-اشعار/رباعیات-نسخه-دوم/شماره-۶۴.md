---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>در دل همه شرک و روی بر خاک چه سود</p></div>
<div class="m2"><p>زهری که بجان رسید تریاک چه سود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود را بمیان خلق زاهد کردن</p></div>
<div class="m2"><p>با نفس پلید و جامه پاک چه سود</p></div></div>