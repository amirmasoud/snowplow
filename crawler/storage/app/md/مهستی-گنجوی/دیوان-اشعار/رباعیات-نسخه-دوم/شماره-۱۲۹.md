---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>زلفین تو سی زنگی و هر سی مستان</p></div>
<div class="m2"><p>سی مستانند خفته در سیمستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاج است بناگوش تو با سیم است آن</p></div>
<div class="m2"><p>زین سیمستان بوسه کم از سر مستان</p></div></div>