---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>نوشین لب او دوش بلا لا می گفت</p></div>
<div class="m2"><p>صد نکته به از لؤلؤ لالا می گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا ندهم کام فلان بیچاره</p></div>
<div class="m2"><p>لالاش که لال باد لالا می گفت</p></div></div>