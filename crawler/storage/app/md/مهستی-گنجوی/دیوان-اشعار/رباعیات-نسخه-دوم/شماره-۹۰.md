---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>دلدار کله دوز من از روی هوس</p></div>
<div class="m2"><p>میدوخت کلاهی ز نسیج اطلس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر هر ترکی هزار زه می گفتم</p></div>
<div class="m2"><p>با آنکه چهار ترک را یک زه بس</p></div></div>