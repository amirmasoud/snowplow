---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>من «مهستیم» از همه خوبان شده طاق</p></div>
<div class="m2"><p>مشهور بحسن در خراسان و عراق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای «پور خطیب گنجه» از بهر خدا</p></div>
<div class="m2"><p>مگذار چنین بسوزم از درد فراق</p></div></div>