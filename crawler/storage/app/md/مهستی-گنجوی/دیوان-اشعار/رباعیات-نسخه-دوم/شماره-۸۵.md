---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>ای «پور خطیب گنجه» پندی بپذیر</p></div>
<div class="m2"><p>بر تخت طرب نشین بکف ساغر گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از طاعت و معصیت خدا مستغنی است</p></div>
<div class="m2"><p>داد دل خود تو از می و دلبر گیر</p></div></div>