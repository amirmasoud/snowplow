---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>روز ازل آمدم نشان غم تو</p></div>
<div class="m2"><p>جان تا بابد بود مکان غم تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من جان و دل خویش از آن دارم دوست</p></div>
<div class="m2"><p>این داغ تو دارد آن نشان غم تو</p></div></div>