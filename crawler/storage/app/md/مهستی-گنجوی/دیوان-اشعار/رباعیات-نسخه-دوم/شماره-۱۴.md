---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>دریای سرشک دیده پر نم ماست</p></div>
<div class="m2"><p>و آن بار که کوه بر نتابد غم ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حسرت همدمی بشد عمر عزیز</p></div>
<div class="m2"><p>ما در غم همدمیم و غم همدم ماست</p></div></div>