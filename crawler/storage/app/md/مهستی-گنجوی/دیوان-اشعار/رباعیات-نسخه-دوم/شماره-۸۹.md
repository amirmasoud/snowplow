---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>چندان بکنم ترا من ای طرفه پسر</p></div>
<div class="m2"><p>خدمت که مگر رحم کنی بر چاکر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز نکنم برون من ایجان جهان</p></div>
<div class="m2"><p>پای از خط بندگی وز عهد تو سر</p></div></div>