---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>دیدم چو مه و مهر میان کویش</p></div>
<div class="m2"><p>گرمابه زده آبچکان از مویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که یکی بوسه دهم بر رویش</p></div>
<div class="m2"><p>زینسو زن من رسید وز آنسو شویش</p></div></div>