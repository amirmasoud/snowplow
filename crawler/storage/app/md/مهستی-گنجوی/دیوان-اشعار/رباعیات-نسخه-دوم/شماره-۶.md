---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>گفتی که مرا بیتو بسی غمخواره است</p></div>
<div class="m2"><p>بی رشوت و پاره از توأم صد چاره است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر رشوه طلب کنی مرا . . . رشوه است</p></div>
<div class="m2"><p>ور پاره طلب کنی مرا . . . پاره است</p></div></div>