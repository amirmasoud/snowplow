---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>گل ساخت ز شکل غنچه پیکانی چند</p></div>
<div class="m2"><p>تا حمله برد بحسن بر تو دلبند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید رخت چو تیغ بنمود از دور</p></div>
<div class="m2"><p>پیکان سپری کرد، سپر هم افکند</p></div></div>