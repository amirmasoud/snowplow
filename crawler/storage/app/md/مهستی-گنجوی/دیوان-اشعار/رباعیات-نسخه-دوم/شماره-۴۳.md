---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>تا بر زنخ تو آشنا شد ریشت</p></div>
<div class="m2"><p>بر روی تو انگشت نما شد ریشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریش آوردی و کنده ای میدانم</p></div>
<div class="m2"><p>ور زانکه نکنده ای کجا شد ریشت</p></div></div>