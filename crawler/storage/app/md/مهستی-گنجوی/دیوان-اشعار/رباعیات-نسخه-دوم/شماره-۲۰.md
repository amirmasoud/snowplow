---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>این خوش پسران که اصلشان از چکل است</p></div>
<div class="m2"><p>سبحان الله سرشتشان از چه گل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیرین سخن و شکر لب و سیم برند</p></div>
<div class="m2"><p>یارب که چنین آبحیات از چکل است</p></div></div>