---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>در بتکده پیش بت تحیات خوش است</p></div>
<div class="m2"><p>با ساغر یکمنی مناجات خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تسبیح مصلای ریائی خوش نیست</p></div>
<div class="m2"><p>زنار نیاز در خرابات خوش است</p></div></div>