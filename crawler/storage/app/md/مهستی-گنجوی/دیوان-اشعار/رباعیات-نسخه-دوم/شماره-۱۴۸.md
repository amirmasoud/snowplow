---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>هر چند چو خاک راه خوارم گیری</p></div>
<div class="m2"><p>خاک توام، ارچه خاکسارم گیری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بحر غمم ز اشک شاید که بلطف</p></div>
<div class="m2"><p>نزدیک لب آئی، بکنارم گیری</p></div></div>