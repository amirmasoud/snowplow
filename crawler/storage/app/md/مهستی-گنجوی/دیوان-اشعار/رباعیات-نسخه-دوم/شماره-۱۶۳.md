---
title: >-
    شمارهٔ ۱۶۳
---
# شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>در وقت بهار جز لب جوی مجوی</p></div>
<div class="m2"><p>جز وصف رخ یار سمن روی مگوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز باده گلرنگ به شبگیر مگیر</p></div>
<div class="m2"><p>جز زلف بنان عنبرین بوی مبوی</p></div></div>