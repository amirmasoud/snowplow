---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>گفتم که مرا از نظر انداخته</p></div>
<div class="m2"><p>گفتا که به مهر دگران ساخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که ترا شناختم بی مهری</p></div>
<div class="m2"><p>گفتا که مرا هنوز نشناخته</p></div></div>