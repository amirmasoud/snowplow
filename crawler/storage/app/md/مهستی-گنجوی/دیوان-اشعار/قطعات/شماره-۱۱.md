---
title: >-
    شمارهٔ  ۱۱
---
# شمارهٔ  ۱۱

<div class="b" id="bn1"><div class="m1"><p>بس سپوزید خواجه خاتون را</p></div>
<div class="m2"><p>اول روز تا به آخر شام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیو شهوت به لب گزید انگشت</p></div>
<div class="m2"><p>ته کشید آب غسل در حمام</p></div></div>