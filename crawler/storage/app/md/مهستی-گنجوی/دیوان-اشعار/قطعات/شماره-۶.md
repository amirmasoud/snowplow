---
title: >-
    شمارهٔ  ۶
---
# شمارهٔ  ۶

<div class="b" id="bn1"><div class="m1"><p>چون خواهم رفت بی تو چندین منزل</p></div>
<div class="m2"><p>کز دست شدم هم به نخستین منزل</p></div></div>