---
title: >-
    شمارهٔ  ۱۷
---
# شمارهٔ  ۱۷

<div class="b" id="bn1"><div class="m1"><p>پوستینی بخواستم از تو</p></div>
<div class="m2"><p>تا زمستان بسر بریم در آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرمت ما بر تو بود چنانک</p></div>
<div class="m2"><p>حرمت پوستین بر تابستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بده ای خواجه پوستینم هین</p></div>
<div class="m2"><p>بیشتر زانک پوستینت هان</p></div></div>