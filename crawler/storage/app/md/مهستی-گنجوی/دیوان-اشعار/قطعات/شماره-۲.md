---
title: >-
    شمارهٔ  ۲
---
# شمارهٔ  ۲

<div class="b" id="bn1"><div class="m1"><p>صابرا نامه گرامی تو</p></div>
<div class="m2"><p>روح افسرده را روان بخشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسرع کلک تو به فن ادب</p></div>
<div class="m2"><p>روشنی بر ظلام جان بخشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>«مهستی» را کلام شیوایت</p></div>
<div class="m2"><p>راحت جسم ناتوان بخشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با شمیم چکامه و غزلت</p></div>
<div class="m2"><p>دین گل را بباغبان بخشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهوای مصاحبات تو دل</p></div>
<div class="m2"><p>صابری را به این و آن بخشد</p></div></div>