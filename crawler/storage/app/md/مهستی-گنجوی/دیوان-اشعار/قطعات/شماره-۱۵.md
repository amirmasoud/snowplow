---
title: >-
    شمارهٔ  ۱۵
---
# شمارهٔ  ۱۵

<div class="b" id="bn1"><div class="m1"><p>در آن شه ره دیو کز پس نهیب</p></div>
<div class="m2"><p>فرشته چو طاوس پر می‌نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غلامان مخدوم فردوس را</p></div>
<div class="m2"><p>از اسبی که پی بر قمر می‌نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر یک دو فرسنگ صد بار بیش</p></div>
<div class="m2"><p>فرو می‌گرفتند و بر می‌نهاد</p></div></div>