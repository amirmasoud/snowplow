---
title: >-
    شمارهٔ  ۱۸
---
# شمارهٔ  ۱۸

<div class="b" id="bn1"><div class="m1"><p>بخدائی که . . . ن</p></div>
<div class="m2"><p>فرض کردست بندگی کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که مرا مرگ خوشتر از آنک</p></div>
<div class="m2"><p>این چنین با تو زندگی کردن</p></div></div>