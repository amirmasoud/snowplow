---
title: >-
    شمارهٔ  ۵
---
# شمارهٔ  ۵

<div class="b" id="bn1"><div class="m1"><p>باید سه هزار سال کز چشمه حور</p></div>
<div class="m2"><p>تا کان گهر گردد و یا معدن زر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاها تو به یک سخن کنار و دهنم</p></div>
<div class="m2"><p>هم معدن زر کردی و هم کان گهر</p></div></div>