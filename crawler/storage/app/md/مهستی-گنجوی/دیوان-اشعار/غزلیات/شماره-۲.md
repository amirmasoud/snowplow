---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>جام را در کف دست تو نشست دگر است</p></div>
<div class="m2"><p>ید بیضا دگر و دست تو دست دگر است</p></div></div>