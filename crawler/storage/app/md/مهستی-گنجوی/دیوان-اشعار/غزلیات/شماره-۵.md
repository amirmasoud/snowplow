---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>آن خال عنبرین که نگارم برو زده</p></div>
<div class="m2"><p>دل میبرد از آنکه بوجه نکو زده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قصاب وار مردم چشمم بچابکی</p></div>
<div class="m2"><p>مژگان قناره کرده و دلها بر او زده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کوزه آب پیش لبش در چکی چکیست</p></div>
<div class="m2"><p>ورنه ز دسته دست چرا در گلو زده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشاق سربسر همه دیوانه گشته اند</p></div>
<div class="m2"><p>تا او گره به سلسله مشکبو زده</p></div></div>