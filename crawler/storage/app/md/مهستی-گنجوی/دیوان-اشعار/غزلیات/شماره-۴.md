---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>در فغانم از دل دیرآشنای خویشتن</p></div>
<div class="m2"><p>خو گرفتم همچو نی با ناله‌های خویشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز غم و دردی که دارد دوستی‌ها با دلم</p></div>
<div class="m2"><p>یار دلسوزی ندیدم در سرای خویشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من کیم؟ دیوانه‌ای کز جان خریدار غم است</p></div>
<div class="m2"><p>راحتی را مرگ می‌داند برای خویشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع بزم دوستانم زنده‌ام از سوختن</p></div>
<div class="m2"><p>در ورای روشنی بینم فنای خویشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن حبابم کز حیات خویش دل برکنده‌ام</p></div>
<div class="m2"><p>زان که خود بر آب می‌بینم بنای خویشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غنچه پژمرده‌ای هستم که از کف داده‌ام</p></div>
<div class="m2"><p>در بهار زندگی عطر و صفای خویشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آرزوهای جوانی همچو گل بر باد رفت</p></div>
<div class="m2"><p>آرزوی مرگ دارم از خدای خویشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همدمی دلسوز نبود «مهستی» را همچو شمع</p></div>
<div class="m2"><p>خود بباید اشک ریزد در عزای خویشتن</p></div></div>