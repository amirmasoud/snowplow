---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>شبی در برت گر بیاسودمی</p></div>
<div class="m2"><p>سر فخر بر آسمان سودمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمال تو گر زانکه من دارمی</p></div>
<div class="m2"><p>بجای تو گر زانکه من بودمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بیچارگان رحمت آوردمی</p></div>
<div class="m2"><p>برافتادگان بر ببخشودمی</p></div></div>