---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>از من طمع وصال داری</p></div>
<div class="m2"><p>الحق هوس محال داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصلم نتوان به خواب دیدن</p></div>
<div class="m2"><p>این چیست که در خیال داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جائی که صبا گذر ندارد</p></div>
<div class="m2"><p>آیا تو کجا مجال داری</p></div></div>