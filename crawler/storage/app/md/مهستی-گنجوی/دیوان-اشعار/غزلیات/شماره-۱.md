---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>چون اشتیاق من به تو افزون ز شرح بود</p></div>
<div class="m2"><p>ممکن نشد که شرح دهم اشتیاق را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تلخی فراق تو تلخ است عیش من</p></div>
<div class="m2"><p>اندازه نیست تلخی روز فراق را</p></div></div>