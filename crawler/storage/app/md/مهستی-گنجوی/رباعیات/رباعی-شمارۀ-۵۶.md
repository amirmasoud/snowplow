---
title: >-
    رباعی شمارۀ ۵۶
---
# رباعی شمارۀ ۵۶

<div class="b" id="bn1"><div class="m1"><p>قصه چه کنم که اشتیاق تو چه کرد</p></div>
<div class="m2"><p>با من دل پر زرق و نفاق تو چه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون زلف دراز تو شبی می‌باید</p></div>
<div class="m2"><p>تا با تو بگویم که فراق تو چه کرد</p></div></div>