---
title: >-
    رباعی شمارۀ ۱۳۹
---
# رباعی شمارۀ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>از ضعف تن آنچنان توانم رفتن</p></div>
<div class="m2"><p>کز دیدهٔ خود نهان توانم رفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگداخته‌ام چنان که گر آه زنم</p></div>
<div class="m2"><p>با ناله بر آسمان توانم رفتن</p></div></div>