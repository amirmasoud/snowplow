---
title: >-
    رباعی شمارۀ ۶۹
---
# رباعی شمارۀ ۶۹

<div class="b" id="bn1"><div class="m1"><p>آن‌ها که هوای عشق موزون زده‌اند</p></div>
<div class="m2"><p>هر نیم شبی سجاده در خون زده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشنیدستی که عاشقان خیمهٔ عشق</p></div>
<div class="m2"><p>از گردش هفت چرخ بیرون زده‌اند</p></div></div>