---
title: >-
    رباعی شمارۀ ۱۵۵
---
# رباعی شمارۀ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>ابریست که خون دیده بارد غم تو</p></div>
<div class="m2"><p>زهریست که تریاک ندارد غم تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر نفسی هزار محنت زده را</p></div>
<div class="m2"><p>بی دل کند و ز جان بر آرد غم تو</p></div></div>