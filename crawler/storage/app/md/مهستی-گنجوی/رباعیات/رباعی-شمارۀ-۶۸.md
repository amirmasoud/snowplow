---
title: >-
    رباعی شمارۀ ۶۸
---
# رباعی شمارۀ ۶۸

<div class="b" id="bn1"><div class="m1"><p>تا از تف آب چرخ افراشته‌اند</p></div>
<div class="m2"><p>غم در دل من چو آتش انباشته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرگشته چو باد می‌دوم در عالم</p></div>
<div class="m2"><p>تا خاک من از چه جای برداشته‌اند</p></div></div>