---
title: >-
    رباعی شمارۀ ۱۶۴
---
# رباعی شمارۀ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>ای روی تو ماه را شکست آورده</p></div>
<div class="m2"><p>و ای قد تو سرو را به پست آورده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانم به سر کار تو در خواهد شد</p></div>
<div class="m2"><p>این جان به خون دل به دست آورده</p></div></div>