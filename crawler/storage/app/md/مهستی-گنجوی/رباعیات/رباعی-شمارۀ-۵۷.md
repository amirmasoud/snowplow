---
title: >-
    رباعی شمارۀ ۵۷
---
# رباعی شمارۀ ۵۷

<div class="b" id="bn1"><div class="m1"><p>بگذشت پریر باز به سر لاله و درد</p></div>
<div class="m2"><p>دی خاک چمن سنبل تر بار آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز خور آب شادمانی زیراک</p></div>
<div class="m2"><p>فردا همی آتش غم باید خورد</p></div></div>