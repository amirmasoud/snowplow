---
title: >-
    رباعی شمارۀ ۱۷۲
---
# رباعی شمارۀ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>هر گه که تو نعل اسب دیگران بندی</p></div>
<div class="m2"><p>داغی دگرم بر دل حیران بندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قربان شومت پیش چو بر …</p></div>
<div class="m2"><p>وز کیش بر آیم چو تو قربان بندی</p></div></div>