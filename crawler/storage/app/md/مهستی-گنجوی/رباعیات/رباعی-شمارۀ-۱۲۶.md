---
title: >-
    رباعی شمارۀ ۱۲۶
---
# رباعی شمارۀ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>ای فاختهٔ مهر چون به تو درنگرم</p></div>
<div class="m2"><p>زیبائی طاوس به بازی شمرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خندهٔ کبک چون در آئی ز درم</p></div>
<div class="m2"><p>دل همچو کبوتری بپرّد ز برم</p></div></div>