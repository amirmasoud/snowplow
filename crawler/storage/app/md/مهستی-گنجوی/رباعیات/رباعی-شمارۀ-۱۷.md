---
title: >-
    رباعی شمارۀ ۱۷
---
# رباعی شمارۀ ۱۷

<div class="b" id="bn1"><div class="m1"><p>ایام چو آتشکده از سینهٔ ماست</p></div>
<div class="m2"><p>عالم کهن از وجود دیرینهٔ ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینک به مثل چو کوزه‌ای آب خوریم</p></div>
<div class="m2"><p>از خاک برادران پیشینهٔ ماست</p></div></div>