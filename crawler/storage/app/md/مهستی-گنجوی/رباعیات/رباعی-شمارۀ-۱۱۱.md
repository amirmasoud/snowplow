---
title: >-
    رباعی شمارۀ ۱۱۱
---
# رباعی شمارۀ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>ترکم چو کمان کشید کردم نگهش</p></div>
<div class="m2"><p>دیدم مه و عقربی به زیر کلهش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه بود رخش عقرب زلف سیهش</p></div>
<div class="m2"><p>وز عقرب در قوس همی رفت مهش</p></div></div>