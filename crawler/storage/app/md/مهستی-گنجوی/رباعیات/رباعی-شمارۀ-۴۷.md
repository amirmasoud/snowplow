---
title: >-
    رباعی شمارۀ ۴۷
---
# رباعی شمارۀ ۴۷

<div class="b" id="bn1"><div class="m1"><p>ای باد که جان فدای پیغام تو باد</p></div>
<div class="m2"><p>گر برگذری به کوی آن حورنژاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو در سر راه مهستی را دیدم</p></div>
<div class="m2"><p>کز آرزوی تو جان شیرین می‌داد</p></div></div>