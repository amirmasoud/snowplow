---
title: >-
    رباعی شمارۀ ۸۰
---
# رباعی شمارۀ ۸۰

<div class="b" id="bn1"><div class="m1"><p>منگر به زمین که خاک و آبت بیند</p></div>
<div class="m2"><p>منگر به فلک که آفتابت بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانم بشود ز غیرت ای جان و جهان</p></div>
<div class="m2"><p>گر زانکه شبی کسی به خوابت بیند</p></div></div>