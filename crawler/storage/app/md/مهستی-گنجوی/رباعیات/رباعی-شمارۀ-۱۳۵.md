---
title: >-
    رباعی شمارۀ ۱۳۵
---
# رباعی شمارۀ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>ما بندگی آن رخ زیبات کنیم</p></div>
<div class="m2"><p>و آزادگی طرهٔ رعنایت کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شطرنج غمت مدام چون ما بازیم</p></div>
<div class="m2"><p>باید که دلت نرنجد ار مات کنیم</p></div></div>