---
title: >-
    رباعی شمارۀ ۶۶
---
# رباعی شمارۀ ۶۶

<div class="b" id="bn1"><div class="m1"><p>سودازدهٔ جمال تو باز آمد</p></div>
<div class="m2"><p>تشنه شدهٔ وصال تو باز آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نو کن قفس و دانهٔ لطفی تو بپاش</p></div>
<div class="m2"><p>کان مرغ شکسته بال تو باز آمد</p></div></div>