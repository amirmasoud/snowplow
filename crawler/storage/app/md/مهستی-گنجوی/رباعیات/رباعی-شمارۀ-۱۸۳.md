---
title: >-
    رباعی شمارۀ ۱۸۳
---
# رباعی شمارۀ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>جسمی دارم دلی خراب اندر وی</p></div>
<div class="m2"><p>جانی دارم هزار تاب اندر وی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز آرزوی تو دارم شب و روز</p></div>
<div class="m2"><p>چشمی و هزار چشمه آب اندر وی</p></div></div>