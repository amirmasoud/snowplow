---
title: >-
    رباعی شمارۀ ۹۴
---
# رباعی شمارۀ ۹۴

<div class="b" id="bn1"><div class="m1"><p>ای لعل تو تا لانهٔ بستان بهار</p></div>
<div class="m2"><p>با دام توام ز آب رزان داده خمار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حسرت شفتالویت ای سیب زنخ</p></div>
<div class="m2"><p>رنگم چو به است و اشک چون دانه انار</p></div></div>