---
title: >-
    رباعی شمارۀ ۸۴
---
# رباعی شمارۀ ۸۴

<div class="b" id="bn1"><div class="m1"><p>سهمی که مرا دلبر خباز دهد</p></div>
<div class="m2"><p>نه از سر کیمخ کز سر ناز دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چنگ غمش بمانده‌ام همچو خمیر</p></div>
<div class="m2"><p>ترسم که بدست آتشم باز ذهذ</p></div></div>