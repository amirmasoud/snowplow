---
title: >-
    رباعی شمارۀ ۱۴۳
---
# رباعی شمارۀ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>دوش از غم هجرت ای بت عهدشکن</p></div>
<div class="m2"><p>چون دوست همی گریست بر من دشمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که من از عشق تو می‌نالیدم</p></div>
<div class="m2"><p>تا روز همی سوخت دل شمع به من</p></div></div>