---
title: >-
    رباعی شمارۀ ۱
---
# رباعی شمارۀ ۱

<div class="b" id="bn1"><div class="m1"><p>دوشینه شبم بود شبیه یلدا</p></div>
<div class="m2"><p>آن مونس غمگسار نامد عمدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب تا به سحر ز دیده دُر می‌سفتم</p></div>
<div class="m2"><p>می‌گفتم رب لاتذرنی فردا</p></div></div>