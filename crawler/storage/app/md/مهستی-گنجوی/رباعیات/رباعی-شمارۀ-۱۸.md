---
title: >-
    رباعی شمارۀ ۱۸
---
# رباعی شمارۀ ۱۸

<div class="b" id="bn1"><div class="m1"><p>افسوس که اطراف گلت خار گرفت</p></div>
<div class="m2"><p>زاغ آمد و لاله را به منقار گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیماب زنخدان تو آورد مداد</p></div>
<div class="m2"><p>شنگرف لب لعل تو زنگار گرفت</p></div></div>