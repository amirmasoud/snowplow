---
title: >-
    رباعی شمارۀ ۹۳
---
# رباعی شمارۀ ۹۳

<div class="b" id="bn1"><div class="m1"><p>گر بت رخ توست بت‌پرستی خوش‌تر</p></div>
<div class="m2"><p>ور باده ز جام توست مستی خوش‌تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هستی عشق تو از آن نیست شوم</p></div>
<div class="m2"><p>کین نیستی از هزار هستی خوشتر</p></div></div>