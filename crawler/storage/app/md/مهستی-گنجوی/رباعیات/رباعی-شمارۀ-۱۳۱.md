---
title: >-
    رباعی شمارۀ ۱۳۱
---
# رباعی شمارۀ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>با ابر همیشه در عتابش بینم</p></div>
<div class="m2"><p>جویندهٔ نور آفتابش بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر مردمک دیدهٔ من نیست چرا</p></div>
<div class="m2"><p>هرگه که طلب کنم در آبش بینم</p></div></div>