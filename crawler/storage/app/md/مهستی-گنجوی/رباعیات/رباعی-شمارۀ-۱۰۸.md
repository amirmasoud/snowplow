---
title: >-
    رباعی شمارۀ ۱۰۸
---
# رباعی شمارۀ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>من تازه‌گلی که نباشد خارش</p></div>
<div class="m2"><p>یا بلبل خوش‌گو که بود غمخوارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بازی که سر دست شهان جاش بود</p></div>
<div class="m2"><p>در دام تو افتاد نکو می‌دارش</p></div></div>