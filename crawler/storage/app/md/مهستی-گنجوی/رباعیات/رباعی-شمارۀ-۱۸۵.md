---
title: >-
    رباعی شمارۀ ۱۸۵
---
# رباعی شمارۀ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>در دل نگذارمت که افگار شوی</p></div>
<div class="m2"><p>در دیده ندارمت که بس خوار شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جان کنمت جای نه در دیده و دل</p></div>
<div class="m2"><p>تا با نفس باز پسین یار شوی</p></div></div>