---
title: >-
    رباعی شمارۀ ۵۱
---
# رباعی شمارۀ ۵۱

<div class="b" id="bn1"><div class="m1"><p>غم با لطف تو شادمانی گردد</p></div>
<div class="m2"><p>عمر از نظر تو جاودانی گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر باد به دوزخ برد از کوی تو خاک</p></div>
<div class="m2"><p>آتش همه آب زندگانی گردد</p></div></div>