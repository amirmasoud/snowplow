---
title: >-
    رباعی شمارۀ ۱۷۱
---
# رباعی شمارۀ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>خندان بدو رخ گل بدیع آوردی</p></div>
<div class="m2"><p>واندر که دی فصل ربیع آوردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دانستی که دل به گل می‌‌ندهم</p></div>
<div class="m2"><p>رفتی و بنفشه را شفیع آوردی</p></div></div>