---
title: >-
    رباعی شمارۀ ۱۶۱
---
# رباعی شمارۀ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>معشوقه لطیف و چست و بازاری به</p></div>
<div class="m2"><p>عاشق همه با ناله و با زاری به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا که دلت ببرده‌ام باز ببر</p></div>
<div class="m2"><p>گفتم که تو برده‌ای تو باز آری به</p></div></div>