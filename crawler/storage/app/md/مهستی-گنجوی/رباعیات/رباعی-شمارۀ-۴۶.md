---
title: >-
    رباعی شمارۀ ۴۶
---
# رباعی شمارۀ ۴۶

<div class="b" id="bn1"><div class="m1"><p>گر ملک تو مصر و روم و چین خواهد بود</p></div>
<div class="m2"><p>آفاق تو را زین نگین خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش باش که عاقبت نصیب من و تو</p></div>
<div class="m2"><p>ده گز کفن و سه گز زمین خواهد بود</p></div></div>