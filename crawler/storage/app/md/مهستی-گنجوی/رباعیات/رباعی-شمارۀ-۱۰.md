---
title: >-
    رباعی شمارۀ ۱۰
---
# رباعی شمارۀ ۱۰

<div class="b" id="bn1"><div class="m1"><p>خط بین که فلک بر رخ دلخواه نبشت</p></div>
<div class="m2"><p>بر برگ گل و بنفشه ناگاه نبشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید خطی به بندگیش می‌داد</p></div>
<div class="m2"><p>کاغذ مگرش نبود بر ماه نبشت</p></div></div>