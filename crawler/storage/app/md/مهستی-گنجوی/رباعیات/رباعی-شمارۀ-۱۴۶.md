---
title: >-
    رباعی شمارۀ ۱۴۶
---
# رباعی شمارۀ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>از مهر خود و کین تو در تابم من</p></div>
<div class="m2"><p>در چشم تو گوئی به میان آبم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا من گنهی کردم و در خشمی تو</p></div>
<div class="m2"><p>یا تو دگری داری و در خوابم من</p></div></div>