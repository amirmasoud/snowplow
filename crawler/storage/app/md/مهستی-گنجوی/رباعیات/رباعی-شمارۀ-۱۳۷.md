---
title: >-
    رباعی شمارۀ ۱۳۷
---
# رباعی شمارۀ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>زلفین تو سی زنگی و هر سی مستان</p></div>
<div class="m2"><p>سی مستان‌اند خفته در سیمستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاج است بناگوش تو یا سیم است آن</p></div>
<div class="m2"><p>ز آن سیمستان بوسه کنم از سی مَسِتان</p></div></div>