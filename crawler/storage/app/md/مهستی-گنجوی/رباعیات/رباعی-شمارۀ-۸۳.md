---
title: >-
    رباعی شمارۀ ۸۳
---
# رباعی شمارۀ ۸۳

<div class="b" id="bn1"><div class="m1"><p>در دل همه شرک روی بر خاک چه سود؟</p></div>
<div class="m2"><p>زهری که به جان رسید تریاک چه سود؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود را به میان خلق زاهد کردن</p></div>
<div class="m2"><p>با نفس پلید جامهٔ پاک چه سود؟</p></div></div>