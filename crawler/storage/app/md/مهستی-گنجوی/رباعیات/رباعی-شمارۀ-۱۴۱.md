---
title: >-
    رباعی شمارۀ ۱۴۱
---
# رباعی شمارۀ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>دی خوش پسری دیدم اندر زوزن</p></div>
<div class="m2"><p>گر لاف زنی ز خوبرویان زو زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او بر دل من رحم نکرد و زن کرد</p></div>
<div class="m2"><p>خود داد منش ستاند زو زن</p></div></div>