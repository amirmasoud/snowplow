---
title: >-
    رباعی شمارۀ ۴۱
---
# رباعی شمارۀ ۴۱

<div class="b" id="bn1"><div class="m1"><p>سوگند به آفتاب یعنی رویت</p></div>
<div class="m2"><p>و آنگاه به مشک ناب یعنی مویت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم که ز دیده هر شبی آب زنم</p></div>
<div class="m2"><p>مأوای دل خراب یعنی کویت</p></div></div>