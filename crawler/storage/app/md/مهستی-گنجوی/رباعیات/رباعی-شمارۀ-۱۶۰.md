---
title: >-
    رباعی شمارۀ ۱۶۰
---
# رباعی شمارۀ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>ای روی تو از تازه گل بربر به</p></div>
<div class="m2"><p>وز چین و خطا و خلج و بربر به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد بندهٔ بربری تو را بنده شد</p></div>
<div class="m2"><p>بربر بر بنده نه که بربر بر به</p></div></div>