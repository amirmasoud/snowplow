---
title: >-
    رباعی شمارۀ ۹۲
---
# رباعی شمارۀ ۹۲

<div class="b" id="bn1"><div class="m1"><p>جانا تو ز دیده اشک بیهوده مبار</p></div>
<div class="m2"><p>دلتنگی من بس است دل تنگ مدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو معشوقی گریستن کار تو نیست</p></div>
<div class="m2"><p>کار من بیچاره به من باز گذار</p></div></div>