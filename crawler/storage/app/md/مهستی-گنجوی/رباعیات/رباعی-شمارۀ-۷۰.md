---
title: >-
    رباعی شمارۀ ۷۰
---
# رباعی شمارۀ ۷۰

<div class="b" id="bn1"><div class="m1"><p>پیوسته خرابات ز رندان خوش باد</p></div>
<div class="m2"><p>در دامن زهد و زاهدی آتش باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دلق به صد پاره و آن صوف کبود</p></div>
<div class="m2"><p>افتاده به زیر پای دُردی‌کش باد</p></div></div>