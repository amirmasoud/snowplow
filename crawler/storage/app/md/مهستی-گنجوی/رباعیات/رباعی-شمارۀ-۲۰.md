---
title: >-
    رباعی شمارۀ ۲۰
---
# رباعی شمارۀ ۲۰

<div class="b" id="bn1"><div class="m1"><p>دریای سرشک دیدهٔ پر نم ماست</p></div>
<div class="m2"><p>وان بار که کوه برنتابد غم ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حسرت همدمی بشد عمر عزیز</p></div>
<div class="m2"><p>ما در غم همدمیم و غم همدم ماست</p></div></div>