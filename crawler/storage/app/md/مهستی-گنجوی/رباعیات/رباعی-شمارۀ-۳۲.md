---
title: >-
    رباعی شمارۀ ۳۲
---
# رباعی شمارۀ ۳۲

<div class="b" id="bn1"><div class="m1"><p>در آتش دل پریر بودم بنهفت</p></div>
<div class="m2"><p>دی باد صبا خوش سخنی با من گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کامروز هر آن که آبرویی دارد</p></div>
<div class="m2"><p>فرداش به خاک تیره می‌باید خفت</p></div></div>