---
title: >-
    رباعی شمارهٔ ۱۲۲
---
# رباعی شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>من عهد تو سخت سست می‌دانستم</p></div>
<div class="m2"><p>بشکستن آن درست می‌دانستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این دشمنی ای دوست که با من ز جفا</p></div>
<div class="m2"><p>آخر کردی نخست می‌دانستم</p></div></div>