---
title: >-
    رباعی شمارۀ ۸۹
---
# رباعی شمارۀ ۸۹

<div class="b" id="bn1"><div class="m1"><p>خطت چو بنفشه از گل آورد پدید</p></div>
<div class="m2"><p>آورد خطی که بر سر ماه کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیوسته ز شب صبح دمیدی اکنون</p></div>
<div class="m2"><p>آشوب دل مرا شب از صبح دمید</p></div></div>