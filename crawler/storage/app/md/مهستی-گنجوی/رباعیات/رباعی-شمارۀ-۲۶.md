---
title: >-
    رباعی شمارۀ ۲۶
---
# رباعی شمارۀ ۲۶

<div class="b" id="bn1"><div class="m1"><p>جوله پسری که جان و دل خستهٔ اوست</p></div>
<div class="m2"><p>از تار زلفش تن من بستهٔ اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی پود چو تار زلف در شانه کند</p></div>
<div class="m2"><p>ز آن این تن زار گشته پیوستهٔ اوست</p></div></div>