---
title: >-
    رباعی شمارۀ ۱۵۴
---
# رباعی شمارۀ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>آب ارچه نمی‌رود به جویم با تو</p></div>
<div class="m2"><p>جز در ره مردمی نپریم با تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که چه کرده‌ام نگوئی با من</p></div>
<div class="m2"><p>آن چیست نکرده‌ای چه گویم با تو</p></div></div>