---
title: >-
    رباعی شمارۀ ۹۰
---
# رباعی شمارۀ ۹۰

<div class="b" id="bn1"><div class="m1"><p>بس غصه که از چشمهٔ نوش تو رسید</p></div>
<div class="m2"><p>تا دست من امروز به دوش تو رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گوش تو دانه‌های دٌر می‌بینم</p></div>
<div class="m2"><p>آب چشمم مگر به گوش تو رسید</p></div></div>