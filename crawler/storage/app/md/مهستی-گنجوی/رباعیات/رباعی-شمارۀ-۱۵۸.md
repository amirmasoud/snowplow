---
title: >-
    رباعی شمارۀ ۱۵۸
---
# رباعی شمارۀ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>مؤذن پسری تازه‌تر از لالهٔ مرو</p></div>
<div class="m2"><p>رنگ رخش آب برده از خون تذرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آوازهٔ قامت خوشش چون برخاست</p></div>
<div class="m2"><p>در خال بباغ در نماز آمد سرو</p></div></div>