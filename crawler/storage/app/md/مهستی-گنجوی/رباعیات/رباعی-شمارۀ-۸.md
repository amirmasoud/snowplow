---
title: >-
    رباعی شمارۀ ۸
---
# رباعی شمارۀ ۸

<div class="b" id="bn1"><div class="m1"><p>در مرو پریر لاله انگیخت</p></div>
<div class="m2"><p>دی نیلوفر به بلخ در آب گریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خاک نشابور گل امروز آمد</p></div>
<div class="m2"><p>فردا به هری باد سمن خواهد ریخت</p></div></div>