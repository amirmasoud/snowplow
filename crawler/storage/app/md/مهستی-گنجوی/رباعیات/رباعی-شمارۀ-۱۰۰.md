---
title: >-
    رباعی شمارۀ ۱۰۰
---
# رباعی شمارۀ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>اشتربانا چو عزم کردی به سفر</p></div>
<div class="m2"><p>مگذار مرا خسته و ز اینجا مگذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر اشتر با تو از پی بارگشی‌ست</p></div>
<div class="m2"><p>من بارکش عم مرا نیز ببر</p></div></div>