---
title: >-
    رباعی شمارۀ ۴۵
---
# رباعی شمارۀ ۴۵

<div class="b" id="bn1"><div class="m1"><p>بی یاد تو در تنم نفس پیکان باد</p></div>
<div class="m2"><p>دل زنده باندهت چو تن بیجان باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر در تن من بهیج نوعی شادیست</p></div>
<div class="m2"><p>الا به غمت پوست برو زندان باد</p></div></div>