---
title: >-
    رباعی شمارۀ ۱۷۹
---
# رباعی شمارۀ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>از دیده اگر نه خون روان داشتمی</p></div>
<div class="m2"><p>رازت ز دل خسته نهان داشتمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور زانکه نبودی دم سرد و رخ زرد</p></div>
<div class="m2"><p>رازت نه ز دل نهان ز جان داشتمی</p></div></div>