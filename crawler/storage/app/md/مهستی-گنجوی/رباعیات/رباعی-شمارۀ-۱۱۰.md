---
title: >-
    رباعی شمارۀ ۱۱۰
---
# رباعی شمارۀ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>در ره چو بداشتم به سوگندانش</p></div>
<div class="m2"><p>از شرم عرق کرد زخ خندانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس بر رخ زرد من بخندید به لطف</p></div>
<div class="m2"><p>عکس رخ من فتاد بر دندانش</p></div></div>