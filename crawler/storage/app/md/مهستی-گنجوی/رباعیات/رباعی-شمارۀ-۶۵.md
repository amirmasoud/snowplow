---
title: >-
    رباعی شمارۀ ۶۵
---
# رباعی شمارۀ ۶۵

<div class="b" id="bn1"><div class="m1"><p>این اشک عقیق رنگ من چون بچکد</p></div>
<div class="m2"><p>آب از دل سنگ و چشم گردون بچکد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمم چو ز تو برید ازو خون بچکید</p></div>
<div class="m2"><p>شک نیست که از بریدگی خون بچکد</p></div></div>