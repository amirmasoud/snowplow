---
title: >-
    رباعی شمارۀ ۵۲
---
# رباعی شمارۀ ۵۲

<div class="b" id="bn1"><div class="m1"><p>مشکی که ز چین ختن آهو دارد</p></div>
<div class="m2"><p>از چینِ سرِ زلف تو آهو دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کس که شبی با غم تو یار بشد</p></div>
<div class="m2"><p>تا وقت سحر ناله و آهو دارد</p></div></div>