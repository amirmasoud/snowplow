---
title: >-
    رباعی شمارۀ ۱۱۵
---
# رباعی شمارۀ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>تا کی ز غم تو رخ به خون شوید دل</p></div>
<div class="m2"><p>و آزرم وصال تو به جان جوید دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رحم آر کز آسمان نمی‌بارد جان</p></div>
<div class="m2"><p>بخشای که از زمین نمی‌روید دل</p></div></div>