---
title: >-
    رباعی شمارهٔ ۳۵
---
# رباعی شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>با هر که نشینی و قدح نوش کنی</p></div>
<div class="m2"><p>از رشک مرا خراب و مدهوش کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که چو می خورم تو را یاد کنم</p></div>
<div class="m2"><p>ترسم که شوی مست و فراموش کنی</p></div></div>