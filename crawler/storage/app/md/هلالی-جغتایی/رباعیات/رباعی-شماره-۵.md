---
title: >-
    رباعی شمارهٔ ۵
---
# رباعی شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>گر دل برود من نروم از نظرت</p></div>
<div class="m2"><p>ور جان بدهم، خاک شوم در گذرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون گرد شوم بر آستانت آیم</p></div>
<div class="m2"><p>بنشینم و برنخیزم از خاک درت</p></div></div>