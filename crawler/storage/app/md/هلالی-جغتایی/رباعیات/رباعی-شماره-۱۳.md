---
title: >-
    رباعی شمارهٔ ۱۳
---
# رباعی شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>چون صورت زیبای تو انگیخته‌اند</p></div>
<div class="m2"><p>صد حسن و ملاحت به هم آمیخته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>القصه که شکل عالم‌آرای تو را</p></div>
<div class="m2"><p>در قالب آرزوی ما ریخته‌اند</p></div></div>