---
title: >-
    رباعی شمارهٔ ۱۲
---
# رباعی شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>آنی که تمام از نمکت ریخته‌اند</p></div>
<div class="m2"><p>ذرات وجودت ز نمک بیخته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با شیرهٔ جان‌ها نمک آمیخته‌اند</p></div>
<div class="m2"><p>تا همچو تو صورتی برانگیخته‌اند</p></div></div>