---
title: >-
    رباعی شمارهٔ ۷
---
# رباعی شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>از بس که مرا دولت بیدار کم‌ست</p></div>
<div class="m2"><p>گفتن نتوان که تا چه مقدار کم‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنجی‌ست فراقت که کمش بسیارست</p></div>
<div class="m2"><p>عیشی‌ست وصال تو، که بسیار کم‌ست</p></div></div>