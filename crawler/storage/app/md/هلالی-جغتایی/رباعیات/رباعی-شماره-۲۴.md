---
title: >-
    رباعی شمارهٔ ۲۴
---
# رباعی شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>نی از تو حیات جاودان می‌خواهم</p></div>
<div class="m2"><p>نی عیش و تنعم جهان می‌خواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی کام دل و راحت جان می‌خواهم</p></div>
<div class="m2"><p>آنی که رضای توست آن می‌خواهم</p></div></div>