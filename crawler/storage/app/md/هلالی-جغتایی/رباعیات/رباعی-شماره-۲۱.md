---
title: >-
    رباعی شمارهٔ ۲۱
---
# رباعی شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>در عشق نکویان چه فراق و چه وصال؟</p></div>
<div class="m2"><p>بدحالی عاشقان بود در همه حال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر وصل بود مدام سوزست و گداز</p></div>
<div class="m2"><p>ور هجر بود تمام رنجست و ملال</p></div></div>