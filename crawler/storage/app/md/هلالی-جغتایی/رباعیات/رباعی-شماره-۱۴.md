---
title: >-
    رباعی شمارهٔ ۱۴
---
# رباعی شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>هر کس که می عشق به جامش کردند</p></div>
<div class="m2"><p>از دردی درد تلخ‌کامش کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویا همه غم‌های جهان در یک جا</p></div>
<div class="m2"><p>جمع آمده بود، عشق نامش کردند</p></div></div>