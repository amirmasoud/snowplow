---
title: >-
    بخش ۲۶ - در تعریف کمان شاه گوید
---
# بخش ۲۶ - در تعریف کمان شاه گوید

<div class="b" id="bn1"><div class="m1"><p>بر سر دست شه کمانی بود</p></div>
<div class="m2"><p>که مه نو ازو نشانی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خم شده همچو ابروی خوبان</p></div>
<div class="m2"><p>کرده هر گوشهٔ عالمی قربان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو ابروی یار در خوی زه</p></div>
<div class="m2"><p>لیک در گوشه‌ها فکنده گره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون جوانان به جنگ خو کرده</p></div>
<div class="m2"><p>همچو شیران به حمله رو کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گره افکنده بر سر ابرو</p></div>
<div class="m2"><p>مه عیدش کمند بر بازو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر کمان داشت ناوک خون‌ریز</p></div>
<div class="m2"><p>راست همچون خدنگ مژگان تیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که او را کشیده تا سر دوش</p></div>
<div class="m2"><p>سروقدی کشیده در آغوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در تماشای قد دل‌جویش</p></div>
<div class="m2"><p>گوشهٔ چشم مردمان سویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ره دوستان فتاده به خاک</p></div>
<div class="m2"><p>دشمنان را ز دور کرده هلاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاه در علم قبضه کامل بود</p></div>
<div class="m2"><p>چون کمان سوی تیر مایل بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>استخوان را اگر نشان کردی</p></div>
<div class="m2"><p>تیر را مغز استخوان کردی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مور اگر آمدی برابر تیر</p></div>
<div class="m2"><p>چشم او دوختی ز یک پر تیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چشمش از دوختن شدی چو فراز</p></div>
<div class="m2"><p>بازش از خم تیر کردی باز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاه چو تیر بر نشانه کشید</p></div>
<div class="m2"><p>آن گدا آه عاشقانه کشید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت شاها، دلم نشان تو باد</p></div>
<div class="m2"><p>رگ جانم زه کمان تو باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حلقهٔ دیده باد زهگیرت</p></div>
<div class="m2"><p>تا رسد گاه کاه بر تیرت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کاش تیرت مرا نشانه کند</p></div>
<div class="m2"><p>تا که آید به سینه خانه کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تیر نی از تو بر جگر خوردن</p></div>
<div class="m2"><p>خوش‌تر آید ز نی شکر خوردن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نی تیری که در کمان داری</p></div>
<div class="m2"><p>کاش آن را به سینه‌ام کاری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر خدنگی نیاید از شستت</p></div>
<div class="m2"><p>خود بگو، چون ننالم از دستت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا هدف غیر این گدا کردی</p></div>
<div class="m2"><p>قدر انداز من، خطا کردی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا تو را استخوان نشان شده است</p></div>
<div class="m2"><p>تنم از ضعف استخوان شده است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مو شکافی به چشم ناوک‌زن</p></div>
<div class="m2"><p>مو اگر می‌شکافی اینک من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هیچ رنجی به دست تو مرساد!</p></div>
<div class="m2"><p>چشم‌زخمی به شست تو مرساد!</p></div></div>