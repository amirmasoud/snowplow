---
title: >-
    بخش ۳۵ - بزم‌آرایی شاه و نظر کردن گدا
---
# بخش ۳۵ - بزم‌آرایی شاه و نظر کردن گدا

<div class="b" id="bn1"><div class="m1"><p>شب که در بزم‌گاه مینا رنگ</p></div>
<div class="m2"><p>زهره با چنگ راست کرد آهنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده از سرخی شفق کردند</p></div>
<div class="m2"><p>اختران لعل در طبق کردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه را دل به سوی باده کشید</p></div>
<div class="m2"><p>باده با مهوشان ساده کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر عشرت نشست در جایی</p></div>
<div class="m2"><p>کان گدا را بود تماشایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاه در بزم با هزار شکوه</p></div>
<div class="m2"><p>آن گدا در نظاره از سر کوه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجلس آراستند و می خوردند</p></div>
<div class="m2"><p>می به آواز چنگ و نی خوردند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی ساقی ز باده گل گل شد</p></div>
<div class="m2"><p>غلغل شیشه صوت بلبل شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد لب گل‌رخان شراب‌آلود</p></div>
<div class="m2"><p>همچو برگ گل گلاب‌آلود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عکس رخ بر شراب افگندند</p></div>
<div class="m2"><p>بر شفق آفتاب افگندند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لب شیرین به بادهٔ زرین</p></div>
<div class="m2"><p>چو رساندند گشت لب شیرین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خندهٔ شاهدان شورانگیز</p></div>
<div class="m2"><p>گشت در جام باده شکرریز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چشم ساقی ز باده مست شده</p></div>
<div class="m2"><p>ترک مخمور می‌پرست شده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اهل مجلس شکفته و خرم</p></div>
<div class="m2"><p>فارغ از هرچه هست در عالم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شیشهٔ زهد را زدند به سنگ</p></div>
<div class="m2"><p>تار تسبیح شد بریشم چنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پر می لعل شد پیالهٔ زر</p></div>
<div class="m2"><p>گل رعنا نمود پیش نظر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شیشهٔ صاف و آن می دلکش</p></div>
<div class="m2"><p>چون دل صاف عاشقان بی غش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دختر رز به شیشه منزل کرد</p></div>
<div class="m2"><p>گرم خون بود جای در دل کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شیشهٔ می که پر ز خون افتاد</p></div>
<div class="m2"><p>در درون هر چه داشت بیرون داد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مطرب صاف عندلیب آهنگ</p></div>
<div class="m2"><p>ساخت آهنگ و چنگ زد در چنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دیگری دف گرفت بیخود و مست</p></div>
<div class="m2"><p>همچو طفلان نواخت بر سر دست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نی تهی ماند از هوی و هوس</p></div>
<div class="m2"><p>زان کمر بست در قبول نفس</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر ندا کز صدای عود آمد</p></div>
<div class="m2"><p>چنگ بشنید و در سجود آمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ناله آمد رباب را بم و زیر</p></div>
<div class="m2"><p>زان که بر وی کمانچه می‌زد تیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شکل قانون چو مضطر آمد راست</p></div>
<div class="m2"><p>صفحهٔ سینه‌اش به نقش آراست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از برای فروغ مجلس شاه</p></div>
<div class="m2"><p>شمع و مشعل شدند زهره و ماه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بزم شه را چو شمع گلشن کرد</p></div>
<div class="m2"><p>دید درویش و دیده روشن کرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شاه در بزم با هزار شکوه</p></div>
<div class="m2"><p>و آن گدا را نظاره از سر کوه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا به نزدیک بزم‌گاه آمد</p></div>
<div class="m2"><p>بهر نظاره سوی شاه آمد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفت شاید که در فروغ چراغ</p></div>
<div class="m2"><p>بینم آن شمع بزم را به فراغ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون میسر نبود بزم حضور</p></div>
<div class="m2"><p>شاد بود از نگاه دورادور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر کسی جام عشرتی می‌خورد</p></div>
<div class="m2"><p>او به صد رشک حسرتی می‌خورد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>می‌کشیدند می به نغمهٔ نی</p></div>
<div class="m2"><p>آن گدا آه می‌کشید از پی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شاه بر لب نهاد جام شراب</p></div>
<div class="m2"><p>آن گدا بی شراب مست و خراب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شه ز دست حریف می می‌خورد</p></div>
<div class="m2"><p>آن گدا خون ز دست وی می‌خورد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شاه در لاله‌زار خرم و خوش</p></div>
<div class="m2"><p>و آن گدا در میانهٔ آتش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شاه ساغر گرفته از سر عیش</p></div>
<div class="m2"><p>و آن گدا را شکسته ساغر عیش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شاه می‌کرد نوش باده به کام</p></div>
<div class="m2"><p>آن گدا تلخ‌کام و زهرآشام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شاه چون رخ ز باده می‌افروخت</p></div>
<div class="m2"><p>آن گدا ز آتش رخش می‌سوخت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شاه را ذوق و حالتی که مپرس</p></div>
<div class="m2"><p>آن گدا را ملامتی که مپرس</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آن شب القصه تا به آخر شب</p></div>
<div class="m2"><p>مجلس عیش بود و بزم و طرب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عاقبت، کار خویش کرد شراب</p></div>
<div class="m2"><p>اهل مجلس شدند مست و خراب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>باده نوشان ز باده مست شدند</p></div>
<div class="m2"><p>سر به پای قدخ ز دست شدند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خواب چون رو به آن گروه نهاد</p></div>
<div class="m2"><p>باز درویش سر به کوه نهاد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کوه با عاشقان هم‌آوازست</p></div>
<div class="m2"><p>پایدارست زان سرافرازست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همچو نازک‌دلان ز جا نرود</p></div>
<div class="m2"><p>متصل با تو گوید و شنود</p></div></div>