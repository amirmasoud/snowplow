---
title: >-
    بخش ۴۷ - به وصل رسیدن درویش و دوری او بار دیگر به جور رقیب
---
# بخش ۴۷ - به وصل رسیدن درویش و دوری او بار دیگر به جور رقیب

<div class="b" id="bn1"><div class="m1"><p>گفت راوی که شاه هر نفسی</p></div>
<div class="m2"><p>آن گدا را همی نواخت بسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خبر آمد که از فلان کشور</p></div>
<div class="m2"><p>بر سر شاه می‌رسد لشکر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌شمارست لشکر دشمن</p></div>
<div class="m2"><p>پای تا سر نهفته در آهن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاه باید که فکر کار کند</p></div>
<div class="m2"><p>دفع آن خیل بی‌شمار کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاه باید که لشکر انگیزد</p></div>
<div class="m2"><p>در سواری چو گرد برخیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو از این قصه شد رقیب آگاه</p></div>
<div class="m2"><p>رفت و گفت از سر حسد با شاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نزد ارباب عقل معلوم‌ست</p></div>
<div class="m2"><p>که نظر سوی ناکسان شوم‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که را بخت بد ز پا انداخت</p></div>
<div class="m2"><p>دیگرش سر بلند نتوان ساخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حذر از قوم بخت‌برگشته</p></div>
<div class="m2"><p>که چو خویشت کنند سرگشته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یارب این سفله از کجا آمد؟</p></div>
<div class="m2"><p>که به سر وقت ما بلا آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این سخن گفت و کرد محرومش</p></div>
<div class="m2"><p>بهره این داد طالع شومش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عاشق از وصل چون جدا افتاد</p></div>
<div class="m2"><p>دست بر سر زد و ز پا افتاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت باز این چه حالت‌ست مرا</p></div>
<div class="m2"><p>این چه رنج و ملالت‌ست مرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر از ابر فتنه بارد سنگ</p></div>
<div class="m2"><p>آرد آن سنگ بر سرم آهنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر از دشت فتنه روید خار</p></div>
<div class="m2"><p>خلد آن خار بر دلم صد بار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چشم من گر به گل نظر فگند</p></div>
<div class="m2"><p>گل شود خار و در دلم شکند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دست من گر به کف سبو گیرد</p></div>
<div class="m2"><p>می‌شود خون و در گلو گیرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر روم سوی چشمه در ظلمات</p></div>
<div class="m2"><p>شربت مرگ گردد آب حیات</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر زنم گاک تا به راه افتم</p></div>
<div class="m2"><p>گام اول درون چاه افتم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بختم از چاه گر برون فگند</p></div>
<div class="m2"><p>باز فی‌الحال سرنگون فگند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آه! ازین بخت واژگون که مراست</p></div>
<div class="m2"><p>وای از این طالع نگون که مراست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عدم من به از وجود من‌ست</p></div>
<div class="m2"><p>گر بمیرم هنوز سود من‌ست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آمد از شوق مرگ جان به لبم</p></div>
<div class="m2"><p>می‌دهم جان و مرگ می‌طلبم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا کی افغان ز من برون آید</p></div>
<div class="m2"><p>کاشکی جان ز تن برون آید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از نفس‌های گرم سوخت تنم</p></div>
<div class="m2"><p>کو اجل تا دگر نفس نزنم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نیست هرگز از نشاط در دل من</p></div>
<div class="m2"><p>گویی از غم سرشته شد گل من</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دور گردون ز من چه می‌خواهد</p></div>
<div class="m2"><p>که تنم را چو کاه می‌کاهد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>داد مانند کاه بر بادم</p></div>
<div class="m2"><p>زان به گردون رسید فریادم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چرخ پیرست روز و شب گردان</p></div>
<div class="m2"><p>تا کند حمله با جوانمردان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خویش را صبح و شام زیب دهد</p></div>
<div class="m2"><p>همه آفاق را فریب دهد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>راست گویم؟ کج‌ست فطرت او</p></div>
<div class="m2"><p>راستی نیست در جبلت او</p></div></div>