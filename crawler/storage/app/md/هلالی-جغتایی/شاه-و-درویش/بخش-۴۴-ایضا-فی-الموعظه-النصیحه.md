---
title: >-
    بخش ۴۴ - ایضا فی الموعظه  النصیحه
---
# بخش ۴۴ - ایضا فی الموعظه  النصیحه

<div class="b" id="bn1"><div class="m1"><p>لاله‌زار جهان عجب باغی‌ست</p></div>
<div class="m2"><p>که از آن باغ هر نفس داغی‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست بوی نشاط در گل او</p></div>
<div class="m2"><p>محنت‌افزاست صوت بلبل او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهن غنچه‌اش که خندان‌ست</p></div>
<div class="m2"><p>دل پرخون دردمندان‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست هر برگ و شاخ در چمنش</p></div>
<div class="m2"><p>تن گل‌چهره‌ای و پیرهنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر بنفشه که بر لب جویی‌ست</p></div>
<div class="m2"><p>گره زلف عنبرین‌مویی‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لاله کز خاک می‌دمد هر سال</p></div>
<div class="m2"><p>صفحهٔ عارض‌ست و نقطهٔ خال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کجا تازه سرو رعنایی‌ست</p></div>
<div class="m2"><p>قد موزون سروبالایی‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا توانی دل از جهان بگسل</p></div>
<div class="m2"><p>رشتهٔ مهر از این و آن بگسل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جاودان نیست عالم فانی</p></div>
<div class="m2"><p>تو درو جاودان کجا مانی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روی در ملک جاودانی کن</p></div>
<div class="m2"><p>ترک این کهنه دیر فانی کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پا در این دام پیچ‌پیچ منه</p></div>
<div class="m2"><p>همه هیچ‌ند، دل به هیچ منه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیش گوهرشناس گوهرسنج</p></div>
<div class="m2"><p>هست عالم چو عرصهٔ شطرنج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که به بازیچه هر زمان شاهی</p></div>
<div class="m2"><p>سوی این عرصه می کند راهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر خوری همچو خضر آب حیات</p></div>
<div class="m2"><p>تشنه‌لب جان دهی درین ظلمات</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فی المثل عمر نوح گر یابی</p></div>
<div class="m2"><p>چون به توفان رسی خطر یابی</p></div></div>