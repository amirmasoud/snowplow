---
title: >-
    بخش ۱۳ - حالات شاه و گدا در مکتب
---
# بخش ۱۳ - حالات شاه و گدا در مکتب

<div class="b" id="bn1"><div class="m1"><p>صبح دم کز نسیم مهرافروز</p></div>
<div class="m2"><p>دور شد طرهٔ شب از رخ روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شست دوران ز آب چشمهٔ مهر</p></div>
<div class="m2"><p>ظلمت شب ز کارگاه سپهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوخت بر مجمر سپهر بلند</p></div>
<div class="m2"><p>ز آتش مهر دانه‌های سپند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب از فلک هویدا شد</p></div>
<div class="m2"><p>قطره‌ها ریخت چشمه پیدا شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهر از چرخ نیلگون سر زد</p></div>
<div class="m2"><p>یوسف از آب نیل سر بر زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش موسوی به طور آمد</p></div>
<div class="m2"><p>ظلمت شب برفت نور آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بعد ظلمت بر این بلند ایوان</p></div>
<div class="m2"><p>روی بنمود چشمه حیوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شه که صد ناز و عشوه در سر داشت</p></div>
<div class="m2"><p>ناگه از خواب ناز سر برداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از گریبان ناز سر بر کرد</p></div>
<div class="m2"><p>سر برآورد و فتنه را سر کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم کله کج نهاد بر سر خویش</p></div>
<div class="m2"><p>هم قبا چست کرد در بر خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حلقه زلف ساخت زیور گوش</p></div>
<div class="m2"><p>چین کاکل فگند بر سر دوش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر میان همچو موی بست کمر</p></div>
<div class="m2"><p>صد کمر بسته را شکست کمر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قد برافراخت همچو عمر دراز</p></div>
<div class="m2"><p>سوی مکتب قدم نهاد به ناز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چشم درویش مستمند به راه</p></div>
<div class="m2"><p>گهر افشان برای مقدم شاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ناگه آن سرو ناز پیدا شد</p></div>
<div class="m2"><p>فتنهٔ رفته باز پیدا شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون بدید آن جمال زیبایی</p></div>
<div class="m2"><p>کرد بنیاد ناشکیبایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دل و جانش در اضطراب افتاد</p></div>
<div class="m2"><p>مست بیخود شد و خراب افتاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دم به دم حال او دگرگون شد</p></div>
<div class="m2"><p>من چه گویم حال او چون شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاه چو دید بی‌قراری او</p></div>
<div class="m2"><p>در دلش کار کرد زاری او</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پیش او رفت و گفت حال تو چیست؟</p></div>
<div class="m2"><p>در چه اندیشه‌ای؟ خیال تو چیست؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ساعتی با گدای خود بنشست</p></div>
<div class="m2"><p>رفت آن‌گه به جای خود بنشست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جای در پیش‌گاه خانه گرفت</p></div>
<div class="m2"><p>و آن گدا جا بر آستانه گرفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بس که بودند هر دو مایل هم</p></div>
<div class="m2"><p>جا گرفتند در مقابل هم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چشم بر چشم و دیده بر دیده</p></div>
<div class="m2"><p>هر زمان سوی یکدگر دیده</p></div></div>