---
title: >-
    بخش ۳۳ - بزم‌آرایی لشکر به شکار
---
# بخش ۳۳ - بزم‌آرایی لشکر به شکار

<div class="b" id="bn1"><div class="m1"><p>چون ز بهر نشاط نوروزی</p></div>
<div class="m2"><p>شد چمن پر بساط فیروزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غنچه و گل به عیش کوشیدند</p></div>
<div class="m2"><p>جامهٔ سرخ و سبز پوشیدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهن تنگ غنچه خندان شد</p></div>
<div class="m2"><p>ژاله در وی فتاد و دندان شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرگس تر به روی لاله فتاد</p></div>
<div class="m2"><p>چشم مخمور بر پیاله فتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه از روی گل نقاب انداخت</p></div>
<div class="m2"><p>بلبلان را در اضطراب انداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لاله از کوه آشکارا شد</p></div>
<div class="m2"><p>لعل از سنگ خاره پیدا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برگ سوسن که سبز رنگ نمود</p></div>
<div class="m2"><p>خنجری در میان زنگ نمود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لالهٔ آتش چو در تنور افروخت</p></div>
<div class="m2"><p>قرص‌ها در ته تنور بسوخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فاخته بال و پر ز هم بگشاد</p></div>
<div class="m2"><p>شانه شد بهر طرهٔ شمشاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از می شوق مست شد بلبل</p></div>
<div class="m2"><p>چشم خود سرخ کرد بر رخ گل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سبزه از بس که رشته با هم بافت</p></div>
<div class="m2"><p>چون سطرلاب سبز بر هم تافت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در چنین وقت و ساعتی فرخ</p></div>
<div class="m2"><p>آن سهی سرو قامت گل‌ رخ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون به عزم شکار بیرون رفت</p></div>
<div class="m2"><p>لشکر بی‌شمار بیرون رفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود نزدیک شهر صحرایی</p></div>
<div class="m2"><p>دور دوری، گشاده پهنایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خاک او سر به سر عبیرآمیز</p></div>
<div class="m2"><p>باد او دم به دم نشاط‌انگیز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سنبل و سوسنش هم خوش‌رنگ</p></div>
<div class="m2"><p>لاله‌اش آب‌دار و آتش رنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صورت وحش و طیر او زیبا</p></div>
<div class="m2"><p>همه دلکش چو نقش بر دیبا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سبز مرغان او ز سبزی پَر</p></div>
<div class="m2"><p>مرغ‌زاری تمام سبزهٔ تر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سبزه‌اش خط عنبرین مویان</p></div>
<div class="m2"><p>لاله‌اش عارض نکورویان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاه چون خیمه زد در آن صحرا</p></div>
<div class="m2"><p>گفت کز هر طرف کنند ندا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وحشیان را تمام گرد کنند</p></div>
<div class="m2"><p>کار اهل شکار ورد کنند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خلق بر گرد صید صف بستند</p></div>
<div class="m2"><p>رخنه‌ها را ز هر طرف بستند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چابکان تیغ را علم کردند</p></div>
<div class="m2"><p>صید را دست و پا قلم کردند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سر و شاخ گوزن بشکستند</p></div>
<div class="m2"><p>گردن کرگدن فرو بستند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شد نشان خدنگ داغ پلنگ</p></div>
<div class="m2"><p>داغ‌ها را فتیله گشت خدنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از برای گریختن نخجیر</p></div>
<div class="m2"><p>پر برآورد، لیک از پر تیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شیر هر دم ز خشم و کینهٔ خویش</p></div>
<div class="m2"><p>پنجه می‌زد ولی به سینهٔ ریش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گور از بس که دید فتنه و شور</p></div>
<div class="m2"><p>دهنش باز ماند چون لب گور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آهو از گریه چشم پر نم داشت</p></div>
<div class="m2"><p>بر سر گور مرده ماتم داشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خواب خرگوش از سر او جست</p></div>
<div class="m2"><p>چشم خود را دیگر به خواب نبست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>روبه از هول جان در آن آشوب</p></div>
<div class="m2"><p>ساخت دم در ره سگان جاروب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در هوای هر پرنده‌ای که پرید</p></div>
<div class="m2"><p>ترکی از ناوکش به سیخ کشید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر غزالی که از زمین برجست</p></div>
<div class="m2"><p>چابکی در کمند پایش بست</p></div></div>