---
title: >-
    بخش ۹ - خطاب هلالی با مدعی
---
# بخش ۹ - خطاب هلالی با مدعی

<div class="b" id="bn1"><div class="m1"><p>ای که با من سر سخن داری</p></div>
<div class="m2"><p>گفتگوی نو و کهن داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساعتی گوش هوش با من دار</p></div>
<div class="m2"><p>مستمع باش گوش با من دار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوش کن این فسانهٔ دیرین</p></div>
<div class="m2"><p>چه بری نام خسرو و شیرین؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بشنو از من حکایت غرا</p></div>
<div class="m2"><p>چه دهی شرح وامق و عذرا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاد گیر این حکایت موزون</p></div>
<div class="m2"><p>چه بری نام لیلی و مجنون؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکر خلوت‌سرای فکرست این</p></div>
<div class="m2"><p>فکر تهمت مکن که بکرست این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمده در مقام جلوه‌گری</p></div>
<div class="m2"><p>تا به عین رضا درو نگری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز قبول نظر نمی‌خواهد</p></div>
<div class="m2"><p>التفات دگر نمی‌خواهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچه هست از سعادت نظرست</p></div>
<div class="m2"><p>نظر اکسیر کیمیا اثرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا رب این تحفه را گرامی کن</p></div>
<div class="m2"><p>یکی از نام‌های نامی کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا ز صاحب‌دلی نظر یابد</p></div>
<div class="m2"><p>شرف التفات در یابد</p></div></div>