---
title: >-
    بخش ۱
---
# بخش ۱

<div class="b" id="bn1"><div class="m1"><p>ای وجود تو اصل هر موجود</p></div>
<div class="m2"><p>هستی و بوده‌ای و خواهی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صانع هر بلند و پست تویی</p></div>
<div class="m2"><p>همه هیچند، هرچه هست تویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقشبند صحیفهٔ ازل تویی</p></div>
<div class="m2"><p>یا وجود قدیم لم‌یزل تویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی ازل آگه از بدایت تو</p></div>
<div class="m2"><p>نی ابد واقف از نهایت تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ازل تا ابد سفید و سیاه</p></div>
<div class="m2"><p>همه بر سر وحدت تو گواه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ورق نانوشته می‌خوانی</p></div>
<div class="m2"><p>سخن ناشنیده می‌دانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش تو طایران قدوسی</p></div>
<div class="m2"><p>بهر یک دانه در زمین‌بوسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روی ما سوی توست از همه سو</p></div>
<div class="m2"><p>سوی ما روی تست از همه رو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سجودیم رو به درگه تو</p></div>
<div class="m2"><p>پا ز سر کرده‌ایم در ره تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چیست این طرفه گنبد والا؟</p></div>
<div class="m2"><p>رفته گردی ز درگهت بالا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کعبه سنگی بر آستانهٔ تو</p></div>
<div class="m2"><p>قبله راهی به سوی خانهٔ تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صبح را با شفق برآمیزی</p></div>
<div class="m2"><p>آب و آتش به هم درآمیزی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زلف شب را نقاب روز کنی</p></div>
<div class="m2"><p>مهر و مه را جهان فروز کنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فلک از ماه و مهر چهره‌فروز</p></div>
<div class="m2"><p>داغ‌ها دارد از غمت شب و روز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بحر از هیبت تو آب شده</p></div>
<div class="m2"><p>غرق دریای اضطراب شده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرد کویت زمین به خاک نشست</p></div>
<div class="m2"><p>گشت در پای بندگان تو پست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کوه از جانب تو آهنگست</p></div>
<div class="m2"><p>از تو بار دلش گران‌سنگست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باد را از تو آه دردآلود</p></div>
<div class="m2"><p>خاک را از تو روی گردآلود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آتش از شوق داغ بر دل ماند</p></div>
<div class="m2"><p>آب از گریه پای در گل ماند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه سر بر خط قضای تو اند</p></div>
<div class="m2"><p>سر به سر طالب رضای تو اند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرچه آن در نشیب و در اوج است</p></div>
<div class="m2"><p>تو محیطی و آن موجست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>موج اگر نیست بحر را چه غمست</p></div>
<div class="m2"><p>بحر اگر نیست موج خود عدمست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>موج دریاست این جهان خراب</p></div>
<div class="m2"><p>بی‌ثباتست همچو نقش بر آب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گه ز موج دگر خورد بر هم</p></div>
<div class="m2"><p>گه ز باد هوا شود در هم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من به امید گوهر نایاب</p></div>
<div class="m2"><p>کشتی افکنده‌ام درین گرداب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کشتی من ز موج بیرون بر</p></div>
<div class="m2"><p>همچو نوحش بر اوج گردون بر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر ز من جز گنه نمی‌آید</p></div>
<div class="m2"><p>از تو غیر از کرم نمی‌شاید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گرچه لب‌تشنه‌ام فتاده به خاک</p></div>
<div class="m2"><p>چون تو را بحر لطف هست چه باک؟</p></div></div>