---
title: >-
    بخش ۴۲ - در صفت خزان و وفات کردن خسرو
---
# بخش ۴۲ - در صفت خزان و وفات کردن خسرو

<div class="b" id="bn1"><div class="m1"><p>این بود اقتضای لیل و نهار</p></div>
<div class="m2"><p>که رسد افت خزان و بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاخ سبزی که رفته بر افلاک</p></div>
<div class="m2"><p>چهرهٔ زرد خود نهد بر خاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز چون وقت برگ‌ریز آمد</p></div>
<div class="m2"><p>لشکر سبزه در گریز آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ، بی گل ز نغمه شد خاموش</p></div>
<div class="m2"><p>با که گوید سخن چو نبود گوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبل از بوستان شد آواره</p></div>
<div class="m2"><p>گل صد برگ شد به صد پاره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پشت طاقت بنفشه را خم شد</p></div>
<div class="m2"><p>بهر خود در لباس ماتم شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قمری از ناله و خروش بماند</p></div>
<div class="m2"><p>سوسن ده زبان خموش بماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل شد و خارها به گلشن بماند</p></div>
<div class="m2"><p>اطلس از دست رفت و سوزن ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رنگ نارنج زعفرانی شد</p></div>
<div class="m2"><p>اشک عناب ارغوانی شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روی مه را گرفت پردهٔ گرد</p></div>
<div class="m2"><p>بلکه در پرده رفت با رخ زرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نار را پرده‌های دل خون شد</p></div>
<div class="m2"><p>پاره پاره ز دیده بیرون شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سیب از بهر گرمی و سردی</p></div>
<div class="m2"><p>کرد پیدا کبودی و زردی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پسته از شاخ سرنگون افتاد</p></div>
<div class="m2"><p>مغزش از استخوان برون افتاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زخم‌ناک و شکسته شد بادام</p></div>
<div class="m2"><p>چشم‌زخمی رسیدش از ایام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خوشهٔ پاک تاک از سر تاک</p></div>
<div class="m2"><p>دانهٔ لعل درفکند به خاک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر سر شاخ برگ و بار نماند</p></div>
<div class="m2"><p>در گلستان به غیر خار نماند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در چنین موسمی که خسرو گل</p></div>
<div class="m2"><p>رفت و مرد از فراق او بلبل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خسر از عرصهٔ ممالک خویش</p></div>
<div class="m2"><p>سفر آخرت گرفت به پیش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گاه در تاب بود و گه در تب</p></div>
<div class="m2"><p>دلش آمد به جان و جان بر لب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در عرق روی زردش از تب و تاب</p></div>
<div class="m2"><p>همچو برگ خزان میانهٔ آب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شد تنش چو کمان بر آن رگ و پی</p></div>
<div class="m2"><p>استخوانی و پوستی بر وی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بس که از درد دل به جان آمد</p></div>
<div class="m2"><p>دلش از درد در فغان آمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>درد او لحظه لحظه افزون شد</p></div>
<div class="m2"><p>عاقبت حال او دگرگون شد</p></div></div>