---
title: >-
    بخش ۱۸ - راندن کوئوال گدا را از مکتب به رقابت خود
---
# بخش ۱۸ - راندن کوئوال گدا را از مکتب به رقابت خود

<div class="b" id="bn1"><div class="m1"><p>هیچ جا در جهان حبیبی نیست</p></div>
<div class="m2"><p>که به دنبال او رقیبی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردمان تا حبیب می‌گویند</p></div>
<div class="m2"><p>در برابر رقیب می‌گویند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کسی جان به آن جهان نبرد</p></div>
<div class="m2"><p>از بلای رقیب جان نبرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاه را سنگ‌دل رقیبی بود</p></div>
<div class="m2"><p>یک ز انصاف بی‌نصیبی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار او زهر چشم بود از قهر</p></div>
<div class="m2"><p>کاسهٔ چشم او چو کاسهٔ زهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به غضب تیز کرده خویَش را</p></div>
<div class="m2"><p>خنده هرگز ندیده رویش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهر آزار خلق در مشتش</p></div>
<div class="m2"><p>شکل کژدم گرفته انگشتش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه سرپنجه‌ای چنین دارد</p></div>
<div class="m2"><p>مشت کژدم در آستین دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با وجود چنین ستیزه و قهر</p></div>
<div class="m2"><p>میر بازار بود و شحنهٔ شهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حکم بر خاص و عام بود او را</p></div>
<div class="m2"><p>اختیار تمام بود او را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سفله را هرگز اعتبار مباد</p></div>
<div class="m2"><p>مدعی صاحب اختیار مباد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حاصل قصه آن که آن بدکیش</p></div>
<div class="m2"><p>گشت واقف ز قصه درویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همچو سگ تند شد به قصد گدا</p></div>
<div class="m2"><p>تا ازان آستانش ساخت جدا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن گدا را چون راند از در شاه</p></div>
<div class="m2"><p>مدتی می‌نشست بر سر راه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از سر راه نیز مانع شد</p></div>
<div class="m2"><p>سعی درویش جمله ضایع شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غیر از اینش نماند هیچ رهی</p></div>
<div class="m2"><p>که رود شب به کوی دوست گهی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کرد بیجاره این‌چنین تدبیر</p></div>
<div class="m2"><p>که رود به کوی او شبگیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>راز او چون به روی روز افتاد</p></div>
<div class="m2"><p>شب تاریک دل‌فروز افتاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پردهٔ صدهزار عیب شب‌ست</p></div>
<div class="m2"><p>یکی از پرده‌های غیب شب‌ست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شب که سر برزند ز سر ظلمات</p></div>
<div class="m2"><p>در سیاهی نماید آب حیات</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نور معراج در دل شب تافت</p></div>
<div class="m2"><p>مصطفی آن‌‌چه یافت در شب یافت</p></div></div>