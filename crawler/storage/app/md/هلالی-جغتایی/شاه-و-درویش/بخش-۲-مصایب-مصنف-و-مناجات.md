---
title: >-
    بخش ۲ - مصایب مصنف و مناجات
---
# بخش ۲ - مصایب مصنف و مناجات

<div class="b" id="bn1"><div class="m1"><p>ای دوای درون خسته‌دلان</p></div>
<div class="m2"><p>مرهم سینهٔ شکسته دلان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرهمی لطف کن، که خسته‌دلم</p></div>
<div class="m2"><p>مرحمت کن که [بس] شکسته‌دلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه من سر به سر گنه کردم</p></div>
<div class="m2"><p>نامهٔ خویش را سیه کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو درین نامهٔ سیاه مبین</p></div>
<div class="m2"><p>کرم خویش بین گناه مبین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من خود از کرده‌های خود خجلم</p></div>
<div class="m2"><p>تو مکن روز حشر منفعلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با وجود گناه‌کاری‌ها</p></div>
<div class="m2"><p>از تو دارم امیدواری‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زانکه بر توست اعتماد همه</p></div>
<div class="m2"><p>ای مراد من و مراد همه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو کریمی و بی‌نوای توام</p></div>
<div class="m2"><p>پادشاهی و من گدای توام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نی گدایی که این و آن خواهم</p></div>
<div class="m2"><p>کام دل، آرزوی جان خواهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلکه باشد گدایی‌ام دردی</p></div>
<div class="m2"><p>اشک سرخی و چهرهٔ زردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا به راهت ز اهل درد شوم</p></div>
<div class="m2"><p>برنخیزم اگرچه گرد شوم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون به خاک اوفتم به صد خواری</p></div>
<div class="m2"><p>تو ز خاکم به لطف برداری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرچه در خورد آتشم چون شرر</p></div>
<div class="m2"><p>نظری گر به من رسد چه ضرر؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من نگویم که لطف و احسان کن</p></div>
<div class="m2"><p>بنده‌ام هرچه شایدت آن کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عاقبت بگسلد چو بند از بند</p></div>
<div class="m2"><p>بند بند مرا به خود پیوند</p></div></div>