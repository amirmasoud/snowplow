---
title: >-
    بخش ۱۲ - حال گدا به وقت شب در جدایی شاه‌زاده
---
# بخش ۱۲ - حال گدا به وقت شب در جدایی شاه‌زاده

<div class="b" id="bn1"><div class="m1"><p>چون شب تیره در میان آمد</p></div>
<div class="m2"><p>دل درویش در فغان آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که دل شب چرا ز مهر تهی‌ست؟</p></div>
<div class="m2"><p>تیره شد روزم این چه روسیهی‌ست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه شد آیا گرفت ماه امشب؟</p></div>
<div class="m2"><p>باشد از دود دل سیاه امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ شب این چنین سیاه نبود</p></div>
<div class="m2"><p>گویی امشب چراغ ماه نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد پر از دود گنبد گردون</p></div>
<div class="m2"><p>روزنی نیست تا رود بیرون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه روی زمین سیاه شد آه!</p></div>
<div class="m2"><p>که نشستم دگر به خاک سیاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان شیرین رسید بر لب من</p></div>
<div class="m2"><p>صد شب دیگران و یک شب من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلکه این صد شبست نیست شکی</p></div>
<div class="m2"><p>که به خونم همه شدند یکی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وه! که خورشید رو به ره کرده</p></div>
<div class="m2"><p>رفته و روز من سیه کرده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آسمان واقف است از غم من</p></div>
<div class="m2"><p>که سیه‌پوش شد به ماتم من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صبح از من نمی‌کند یادی</p></div>
<div class="m2"><p>آخر ای مرغ صبح، فریادی!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کوس امشب غریو کم دارد</p></div>
<div class="m2"><p>ز آب چشمم مگر که نم دارد؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قمری از بانگ صبح لب بربست</p></div>
<div class="m2"><p>تا شد از ناله‌ام فغانش پست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دیده‌ها بر ستاره دادم تا دم صبح</p></div>
<div class="m2"><p>چون شفق می‌گریست از غم صبح</p></div></div>