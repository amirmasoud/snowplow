---
title: >-
    بخش ۱۵ - حال عشق شاه‌زاده با گدا
---
# بخش ۱۵ - حال عشق شاه‌زاده با گدا

<div class="b" id="bn1"><div class="m1"><p>باز چون ظلمت شب آمد پیش</p></div>
<div class="m2"><p>مبتلای فراق شد درویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بامدادان که طفل این مکتب</p></div>
<div class="m2"><p>صفحه را شست از سیاهی شب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسمان زد به رسم هر روزه</p></div>
<div class="m2"><p>قلم زر به لوح فیروزه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اهل مکتب ز خواب برجستند</p></div>
<div class="m2"><p>به خیال سبق میانن بستند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با قد همچو سرو و روی همچو ماه</p></div>
<div class="m2"><p>همه جمع آمدند غیر از شاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل درویش هیچ از آن نشکفت</p></div>
<div class="m2"><p>هر دم آهسته زیر لب می‌گفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه هستند یار نیست، چه سود؟</p></div>
<div class="m2"><p>سرو من در کنار نیست چه سود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یار می‌باید و نمی‌آید</p></div>
<div class="m2"><p>غیر می‌آید و نمی‌باید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود شه‌زاده را یکی همزاد</p></div>
<div class="m2"><p>که ز مادر به شکل او کم زاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>واقف از حال شاه در همه حال</p></div>
<div class="m2"><p>همدم و همنشین او مه و سال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون بسی بی‌قرار شد درویش</p></div>
<div class="m2"><p>گفت به ز بی‌قراری خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که چرا دیر کرد شاه امروز؟</p></div>
<div class="m2"><p>ساخت روز مرا سیاه امروز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آفتاب مرا چه آمد پیش؟</p></div>
<div class="m2"><p>که نیامد برون ز خانه خویش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برده خواب صبوح از دستش</p></div>
<div class="m2"><p>یا می ناز کرده سرمستش؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا سحرگه نشسته بود مگر؟</p></div>
<div class="m2"><p>ور نه تا چاشت چیست خواب سحر؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود در گفتگو که آمد شاه</p></div>
<div class="m2"><p>شد ز گفت و شنودشان آگاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رشکش آمد که عاشق نگران</p></div>
<div class="m2"><p>نگران‌ست جانب دگران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چشم عاشق به یار باید و بس</p></div>
<div class="m2"><p>عاشقی کی رواست پیش دو کس؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کفت هی! هی! عجب خطا کردم</p></div>
<div class="m2"><p>که به این بوالهوس وفا کردم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر وفایی درین گدا بودی</p></div>
<div class="m2"><p>این چنین در به در چرا بودی؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در سگ در به در وفا نبود</p></div>
<div class="m2"><p>در به در خود به جز گدا نبود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بنده چون کرد بندگی کسی</p></div>
<div class="m2"><p>نخرندش که گشته است بسی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گه به همزاد خود برآشفتی</p></div>
<div class="m2"><p>به صد آشفتگی به او گفتی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>میل علت چو نیست بیش از من</p></div>
<div class="m2"><p>پس چرا آمدی تو پیش از من؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گاه از مکتبش برون کردی</p></div>
<div class="m2"><p>جگرش را به طعنه خون کردی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که به مکتب دگر میا با من</p></div>
<div class="m2"><p>یا تو آیی درین طرف یا من</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گه قلم را به خاک افگندی</p></div>
<div class="m2"><p>گه ورق را ز یکدگر کندی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کردی اظهار رشک و غیرت خویش</p></div>
<div class="m2"><p>رشک خوبان بود ز عاشق بیش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>صفحه را پیش روی آوردی</p></div>
<div class="m2"><p>چهرهٔ خویش را نهان کردی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فتنهٔ اهل حسن در عالم</p></div>
<div class="m2"><p>بر سر عاشقان بود ماتم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شاه در فکر کار درویش‌ست</p></div>
<div class="m2"><p>خواجه را میل بندهٔ خویش‌ست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر سپاهی به شاه خود نازد</p></div>
<div class="m2"><p>شاه هم بر سپاه خود تازد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از خجالت هلاک شد درویش</p></div>
<div class="m2"><p>گفت راضی شدم به مردن خویش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جان‌گدازست ناتوانی من</p></div>
<div class="m2"><p>مرگ بهتر ز زندگانی من</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آه! ازین طالعی که من دارم</p></div>
<div class="m2"><p>گریه از بخت خویشتن دارم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شوخ من گرچه نکته‌دان افتاد</p></div>
<div class="m2"><p>لیک بسیار بدگمان افتاد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خواستم سوی گوهر آرم دست</p></div>
<div class="m2"><p>دستم از سنگ حادثات شکست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>عمر می‌خواستم ز آب حیات</p></div>
<div class="m2"><p>تشنه مردم ز شوق در ظلمات</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شاه شیرین‌زبان شکرلب</p></div>
<div class="m2"><p>بار دیگر چو رفت از مکتب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خواند همزاد را به خدمت خویش</p></div>
<div class="m2"><p>که چه می‌گفت با تو آن درویش؟</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>قصه را پیش شاه کرد بیان</p></div>
<div class="m2"><p>به طریقی که حال گشت عیان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یافت شه از ادای آن تسکین</p></div>
<div class="m2"><p>بست دل در وفای آن مسکین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کو رسولی که از برای خدا</p></div>
<div class="m2"><p>حال من هم کند به شاه ادا؟</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا دگر قصد این گدا نکند</p></div>
<div class="m2"><p>بند بندم ز هم جدا نکند</p></div></div>