---
title: >-
    بخش ۲۴ - جستن کبوتر شاه بر درویش و نامه نوشتن به بال او
---
# بخش ۲۴ - جستن کبوتر شاه بر درویش و نامه نوشتن به بال او

<div class="b" id="bn1"><div class="m1"><p>بود شه را کبوتری که فلک</p></div>
<div class="m2"><p>نه پری دید مثل او نه ملک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پریدن بلند پایهٔ او</p></div>
<div class="m2"><p>چون همای ارجمند سایهٔ او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قمری از بهر بندگی کردن</p></div>
<div class="m2"><p>پیش او رفته طوق در گردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حلقهٔ چشم باز را کنده</p></div>
<div class="m2"><p>زره زر به پایش افگنده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرده پرواز تا مه و انجم</p></div>
<div class="m2"><p>دم همه سوده و شده همه دم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی آن هدهد همایون فر</p></div>
<div class="m2"><p>بس که می‌زد به گرد گردون پر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سر قصر شاه دور افتاد</p></div>
<div class="m2"><p>اندک اندک ز راه دور افتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بعد از آن کز هوا فرود آمد</p></div>
<div class="m2"><p>بر سر آن گدا فرود آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر او سود بر سپهر بلند</p></div>
<div class="m2"><p>که به فرقش همای سایه فگند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت فرق من آشیانهٔ توست</p></div>
<div class="m2"><p>قطرهٔ اشکم آب و دانهٔ توست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن کبوتر به فرق آن محزون</p></div>
<div class="m2"><p>بود چون مرغ بر سر مجنون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آتشین آه را همی افروخت</p></div>
<div class="m2"><p>که چو پروانه بال او می‌سوخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بعد از آن دست برد سوی قلم</p></div>
<div class="m2"><p>تا کند حسب حال خویش رقم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شرح بی‌مهری زمانه کند</p></div>
<div class="m2"><p>نامه بنویسد و روانه کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قصهٔ محنت فراق نوشت</p></div>
<div class="m2"><p>شرح غم‌های اشتیاق نوشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرگه از سوز دل رقم می‌زد</p></div>
<div class="m2"><p>آتش اندر نی قلم می‌زد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون نوشت از رقیب و از ستمش</p></div>
<div class="m2"><p>نامه در پیچ و تاب شد ز غمش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نامه را بر پر کبوتر بست</p></div>
<div class="m2"><p>پر دیگر به بال او بربست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ره نمودش به سوی منظر شاه</p></div>
<div class="m2"><p>کرد پرواز و رفت تا بر شاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرغ روحش پرید از سر او</p></div>
<div class="m2"><p>تا پرد همره کبوتر او</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شاه چون خواند عرض حال گدا را</p></div>
<div class="m2"><p>گفت کز هر طرف کنند ندا را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کین همه خلق بی‌شمارهٔ شهر</p></div>
<div class="m2"><p>جمع گردند بر ککنارهٔ شهر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سوی میدان برند تیر و کمان</p></div>
<div class="m2"><p>به تماشا روند پیر و جوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر گروهی نشانه‌ای سازند</p></div>
<div class="m2"><p>تیر خود بر نشانه اندازند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر که در حکم ما کند تقصیر</p></div>
<div class="m2"><p>خویش را کند نشانهٔ تیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون رسید این ندا به گوش گدا</p></div>
<div class="m2"><p>خواست تا جان کند ز شوق فدا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رفت و جا بر کنار میدان کرد</p></div>
<div class="m2"><p>شه دگر روز عزم جولان کرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر که بیماری فراق کشید</p></div>
<div class="m2"><p>عاقبت شربت وصال چشید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر که غمگین در انتظار نشست</p></div>
<div class="m2"><p>شادمان در حریم یار نشست</p></div></div>