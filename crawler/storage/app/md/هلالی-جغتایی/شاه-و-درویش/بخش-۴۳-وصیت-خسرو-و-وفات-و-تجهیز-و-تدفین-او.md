---
title: >-
    بخش ۴۳ - وصیت خسرو و وفات و تجهیز و تدفین او
---
# بخش ۴۳ - وصیت خسرو و وفات و تجهیز و تدفین او

<div class="b" id="bn1"><div class="m1"><p>شاه را خواندی سوی خود خسرو</p></div>
<div class="m2"><p>گفت از من وصیتی بشنو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عدل پیش آور پادشاهی کن</p></div>
<div class="m2"><p>ظلم بگذار و هرچه خواهی کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نبینی ز هیچ رهگذری</p></div>
<div class="m2"><p>گردی از خود به دامن دگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر مپیچ از رضای درویشان</p></div>
<div class="m2"><p>که سرافراز عالمند ایشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که یابد ز فقر آگاهی</p></div>
<div class="m2"><p>نکند میل شوکت شاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بسا شاه عاقبت‌اندیش</p></div>
<div class="m2"><p>که ز شاهی گذشت و شد درویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که بر درگه تو داد کند</p></div>
<div class="m2"><p>طلب حاجت و مراد کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگرش هیبت تو لال کند</p></div>
<div class="m2"><p>نتواند که عرض حال کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو گل بر رخسش تبسم کن</p></div>
<div class="m2"><p>به سخن‌های خوش تکلم کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از قلم‌زن به لطف یاد بکن</p></div>
<div class="m2"><p>بر سیه نامه اعتماد بکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر جراحت که بر دل از ستمست</p></div>
<div class="m2"><p>همه از نوک نیزه و قلمست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قیمت عدل را شکست مده</p></div>
<div class="m2"><p>جانب شرع را ز دست مده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زان که میزان راستی شرع‌ست</p></div>
<div class="m2"><p>اصل شرع‌ست و غیر از آن فرع‌ست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این وصیت چون بکرد جان بسپرد</p></div>
<div class="m2"><p>جان به جان‌آفرین روان بسپرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر کسی بهر ماتم افغان کرد</p></div>
<div class="m2"><p>ماتمی شد که شرح نتوان کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شعلهٔ آه تا به گردون رفت</p></div>
<div class="m2"><p>دجلهٔ اشک تا به جیحون رفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه آفاق در خروش شدند</p></div>
<div class="m2"><p>همه ترکان سیاه‌پوش شدند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لشکر از ماتمش سیه در بر</p></div>
<div class="m2"><p>مضطرب چو سیاهی لشکر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زان سیاهی که داشت لشکر او</p></div>
<div class="m2"><p>خطهٔ هند گشت کشور او</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کمر زر که بر میان می بست</p></div>
<div class="m2"><p>حلقهٔ پشتش از کمر بشکست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شد سیه‌روز ز ماتمش خاتم</p></div>
<div class="m2"><p>کند رخسار خود در آن ماتم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تاج یک سو فتاد و ابتر شد</p></div>
<div class="m2"><p>همه خیل و سپاه بی‌سر شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تخت بر خاک ره ز پا افتاد</p></div>
<div class="m2"><p>که سلیمان عصر شد بر باد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>این یکی آه دردناکی زدی</p></div>
<div class="m2"><p>وان دگری جیب جامه چاگ زدی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدنش را ز گریه می‌شستند</p></div>
<div class="m2"><p>کفنش را ز حله می‌جستند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آخرش جانب لحد بردند</p></div>
<div class="m2"><p>همچو گنجش به خاک بسپردند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آن که اوج فلک نشیمن ساخت</p></div>
<div class="m2"><p>عاقبت زیر خاک مسکن ساخت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آن که از حله پیرهن پوشید</p></div>
<div class="m2"><p>کند پیراهن و کفن پوشید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن که بر فرق تاج از زر کرد</p></div>
<div class="m2"><p>در لحد رفت و خاک بر سر کرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هیچ کس در جهان قدم نزند</p></div>
<div class="m2"><p>که قدم جانب عدم نزند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر که گهواره ساخت منزل خویش</p></div>
<div class="m2"><p>رفت و تابوت کرد محفل خویش</p></div></div>