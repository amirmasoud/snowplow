---
title: >-
    بخش ۴۰ - نامه نوشتن خسرو و خواستن شه‌زاده را از سیاحت کنار دریا
---
# بخش ۴۰ - نامه نوشتن خسرو و خواستن شه‌زاده را از سیاحت کنار دریا

<div class="b" id="bn1"><div class="m1"><p>خوشنویسی که این رقم زده بود</p></div>
<div class="m2"><p>بر ورق این‌چنین قلم زده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که فرستاد خسرو عادل</p></div>
<div class="m2"><p>نامه‌ای سوی شاه دریادل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نامه‌ای در نهایت خوبی</p></div>
<div class="m2"><p>خط آن نامه آیت خوبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نو خطی در کمال حسن و جمال</p></div>
<div class="m2"><p>زیب رخساره کرده از خط و خال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقش عنوان و خط مضمونش</p></div>
<div class="m2"><p>فیض‌بخش از درون و بیرونش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا مزین به مشک هر ورقی</p></div>
<div class="m2"><p>یا پر از رشتهٔ گهر طبقی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خط آن نامه بود خط نجات</p></div>
<div class="m2"><p>چون شب قدر در میان برات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حاصل نامه آن که حضرت شاه</p></div>
<div class="m2"><p>غیرت آفتاب و خجلت ماه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شهریار دیار ماه‌وشان</p></div>
<div class="m2"><p>ماه مسندنشین شاه‌نشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میوهٔ باغ زندگانی من</p></div>
<div class="m2"><p>نقد گنجینهٔ جوانی من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن که میل دلم به جانب اوست</p></div>
<div class="m2"><p>وان که جانم همیشه طالب اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باید این نامه را چو برخواند</p></div>
<div class="m2"><p>رخش دولت به این طرف راند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که دگر قوت فراق نماند</p></div>
<div class="m2"><p>طاقت درد اشتیاق نماند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عمر ده روزه غیر بادی نیست</p></div>
<div class="m2"><p>هیچ بر عمر اعتمادی نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خاصه بر عمر همچو من پیری</p></div>
<div class="m2"><p>که شد از دست و نیست تدبیری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زود باشد کزین چمن بروم</p></div>
<div class="m2"><p>تو بیا پیش از آن که من بروم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا تو رفتی ز دیده نور برفت</p></div>
<div class="m2"><p>تا تو غایب شدی حضور برفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رحم کن بر دل رمیدهٔ من</p></div>
<div class="m2"><p>مردمی کن بیا به دیدهٔ من</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روز عمرم به شب رسید بیا</p></div>
<div class="m2"><p>جانم از غم به سر رسید بیا</p></div></div>