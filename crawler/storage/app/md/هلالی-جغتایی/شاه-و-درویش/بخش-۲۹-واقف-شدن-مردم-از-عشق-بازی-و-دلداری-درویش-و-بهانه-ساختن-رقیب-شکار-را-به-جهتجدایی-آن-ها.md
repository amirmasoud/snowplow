---
title: >-
    بخش ۲۹ - واقف شدن مردم از عشق‌بازی و دلداری درویش و بهانه ساختن رقیب شکار را به جهتجدایی آن‌ها
---
# بخش ۲۹ - واقف شدن مردم از عشق‌بازی و دلداری درویش و بهانه ساختن رقیب شکار را به جهتجدایی آن‌ها

<div class="b" id="bn1"><div class="m1"><p>چند روزی که شاه بنده‌نواز</p></div>
<div class="m2"><p>سوی درویش جلوه کرد به ناز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردمان پی به حال او بردند</p></div>
<div class="m2"><p>ره به فکر و خیال او بردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیب‌جویان به عیب رو کردند</p></div>
<div class="m2"><p>وز سر طعنه گفتگو کردند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که چرا شاه با گدا یارست؟</p></div>
<div class="m2"><p>پادشه را خود از گدا عارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مسند شاه و بوریای گدا؟</p></div>
<div class="m2"><p>الله! الله! کجاست تا به کجا؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از گدا عشق شاه لایق نیست</p></div>
<div class="m2"><p>بلکه او مدعی‌ست، عاشق نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پاک‌بازان دعای شه گفتند</p></div>
<div class="m2"><p>در معنی در این سخن سفتند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که بدین‌سان شه پسندیده</p></div>
<div class="m2"><p>کس ندیدست بلکه نشنیده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاه گر با گدا چنین بازد</p></div>
<div class="m2"><p>همه کس را گدای خود سازد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زین سخن‌ها رقیب واقف شد</p></div>
<div class="m2"><p>طبع ناساز او مخالف شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از غضب خون او به جوش آمد</p></div>
<div class="m2"><p>چون خم باده در خروش آمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت اگر خون این گدا ریزم</p></div>
<div class="m2"><p>بهر خود فتنه‌ای برانگیزم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاه ازین قصه گر خبر یابد</p></div>
<div class="m2"><p>رخ ز من تا به حشر می‌تابد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر بگویم به او، گران آید</p></div>
<div class="m2"><p>ور نگویم دلمبه جان آید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس همان به که حیله‌ای بکنم</p></div>
<div class="m2"><p>شاه را از گدا جدا فگنم</p></div></div>