---
title: >-
    بخش ۳۲ - وصف غزال کوهی
---
# بخش ۳۲ - وصف غزال کوهی

<div class="b" id="bn1"><div class="m1"><p>در صف آهوان غزالی بود</p></div>
<div class="m2"><p>کش عجب نازنین جمالی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم از بوی نافه‌اش مشکین</p></div>
<div class="m2"><p>پیش او آهوی ختن مسکین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوخ چشمی به غمزه شعبده‌باز</p></div>
<div class="m2"><p>چشم شوخش تمام عشوه و ناز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویی آن چشم شوخ در بازی</p></div>
<div class="m2"><p>شوخ‌چشمی‌ست در نظربازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه بودند آهوان خیلی</p></div>
<div class="m2"><p>بد گدا را به سوی او میلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دم از مژه جای او می‌رُفت</p></div>
<div class="m2"><p>هر نفس در هوای او می‌گفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم او چشم شاه را مانند</p></div>
<div class="m2"><p>آن بلای سیاه را مانند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نافه ی او که مشک چین دارد</p></div>
<div class="m2"><p>بوی آن زلف عنبرین دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفسش مشک‌بار می‌آید</p></div>
<div class="m2"><p>زان نفس بوی یار می‌آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من سگ آهویی که هر نفسی</p></div>
<div class="m2"><p>خوش دلم می‌کند به یاد کسی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون مرا نیست رنگی از رویش</p></div>
<div class="m2"><p>لاجرم شادمانم از بویش</p></div></div>