---
title: >-
    قصیدهٔ شمارهٔ ۲
---
# قصیدهٔ شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>گر جان کنم به حسرت زان لب نمی‌کند دل</p></div>
<div class="m2"><p>دل کندن از لب او جان کندنی‌ست مشکل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قبله‌ست روی جانان، لعلش چو آب حیوان</p></div>
<div class="m2"><p>این یک مقابل جان و آن یک به جان مقابل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درست دعا بر آرم، هرگز فرو نیارم</p></div>
<div class="m2"><p>الا دمی که سازم در گردنت حمایل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای من سگ خیالت، آن‌جا که اوست هرگز</p></div>
<div class="m2"><p>نه حاجب‌ست مانع، نه پرده‌دار حایل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بازی مکن که پیشت، در خون و خاک غلتم</p></div>
<div class="m2"><p>نه مرده و نه زنده، چون مرغ نیم‌بسمل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بر زلال حیوان ریزد حمیم قهرت</p></div>
<div class="m2"><p>آن آب زندگی را سازد چو زهر قاتل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر در سموم باشد اندک نسیم لطفت</p></div>
<div class="m2"><p>در یک نفس جهان را بخشد حیات کامل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بهر مطربانت سازد فلک همیشه</p></div>
<div class="m2"><p>این چرخ چنبری را خورشید و مه جلاجل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست کرم گشودی، بذل درم نمودی</p></div>
<div class="m2"><p>پیش از دعای داعی، پیش از نماز سایل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در سلک آن لئالی، خود را مکش هلالی</p></div>
<div class="m2"><p>سررشته را نگه دار، زین رشته دست مگسل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بادا تمام مردم در خدمت تو حاضر</p></div>
<div class="m2"><p>بادا نظام انجم از طلعت تو حاصل</p></div></div>