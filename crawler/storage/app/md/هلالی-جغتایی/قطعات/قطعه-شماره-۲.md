---
title: >-
    قطعهٔ شمارهٔ ۲
---
# قطعهٔ شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>تا کی اندوه روزگار خوریم؟</p></div>
<div class="m2"><p>فکر نابود و بود چندین چیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نباشد ز غصه نتوان مرد</p></div>
<div class="m2"><p>ور بود شاد نیز نتوان زیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا که در دست کیست روزی ما؟</p></div>
<div class="m2"><p>و آنچه در دست ماست روزی کیست؟</p></div></div>