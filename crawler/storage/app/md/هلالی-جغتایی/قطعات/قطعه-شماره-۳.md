---
title: >-
    قطعهٔ شمارهٔ ۳
---
# قطعهٔ شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>آه! ازین روزگار برگشته</p></div>
<div class="m2"><p>که ز من لحظه لحظه برگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر فلک را به کام خود خواهم</p></div>
<div class="m2"><p>او به کام کس دگر گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور ز جام نشاط باده خورم</p></div>
<div class="m2"><p>باده خونابهٔ جگر گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور قدم بر بساط سبزه نهم</p></div>
<div class="m2"><p>سبزه در حال نیشتر گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیک با این خوشم، که طالع من</p></div>
<div class="m2"><p>نتواند ازین بتر گردد</p></div></div>