---
title: >-
    قطعهٔ شمارهٔ ۹
---
# قطعهٔ شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>ای سیه‌نامه، کز برای نجات</p></div>
<div class="m2"><p>حرفی از باب رحمتی طلبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبقتم چیست؟ گفته‌ای زین باب</p></div>
<div class="m2"><p>«سبقت رحمتی علی غضبی»</p></div></div>