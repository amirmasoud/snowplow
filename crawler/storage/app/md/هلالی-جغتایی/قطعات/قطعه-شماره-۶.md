---
title: >-
    قطعهٔ شمارهٔ ۶
---
# قطعهٔ شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>دلا، تا توان مهر گیتی مورز</p></div>
<div class="m2"><p>که تیغ سیاست به کینت کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشو غره، گر ابلق چرخ را</p></div>
<div class="m2"><p>قضا و قدر زیر زینت کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرفتم که بر آسمان رفته‌ای</p></div>
<div class="m2"><p>عجل عاقبت بر زمینت کشد</p></div></div>