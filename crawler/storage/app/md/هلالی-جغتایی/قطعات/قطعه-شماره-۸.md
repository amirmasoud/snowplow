---
title: >-
    قطعهٔ شمارهٔ ۸
---
# قطعهٔ شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>محمد عربی آبروی هر دو سراست</p></div>
<div class="m2"><p>کسی که خاک درش نیست خاک بر سر او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شنیده‌ام که تکلم نمود همچو مسیح</p></div>
<div class="m2"><p>بدین حدیث لب لعل روح‌پرور او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که من مدینهٔ علمم، علی درست مرا</p></div>
<div class="m2"><p>عجب خجسته حدیثی‌ست! من سگ در او</p></div></div>