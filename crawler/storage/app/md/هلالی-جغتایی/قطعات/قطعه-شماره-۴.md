---
title: >-
    قطعهٔ شمارهٔ ۴
---
# قطعهٔ شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>چیست آن خسرو سیمین‌بدن زرین‌تاج؟</p></div>
<div class="m2"><p>که به شب خانهٔ فولاد نشیمن دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو ستون‌ست ولی از مدد خیمه بپاست</p></div>
<div class="m2"><p>سیم‌گون‌ست ولی جامه ز آهن دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بته پیرهن آل عجب شاخ گلی‌ست!</p></div>
<div class="m2"><p>که ازو خانهٔ ما زینت گلشن دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاهد پرده‌نشینی‌ست که با روی چو ماه</p></div>
<div class="m2"><p>در درون‌ست و برون را همه روشن دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاهی از آتش دل شعله فتد در جیبش</p></div>
<div class="m2"><p>گاهی از باد صبا چاک به دامن دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست در خانه که از آن همه شب تا دم صبح</p></div>
<div class="m2"><p>که غم سوختن و کشتن و مردن دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با تن سیمی کافور چو رخ افروزد</p></div>
<div class="m2"><p>تاب آتشکده و تابش گلشن دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمع طاوس مگر حل کند این مسئله را</p></div>
<div class="m2"><p>که دل روشن او حکم دل من دارد</p></div></div>