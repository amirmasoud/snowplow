---
title: >-
    غزل شمارهٔ ۱۲۸
---
# غزل شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>دلم، پیش لبت، با جان شیرین در فغان آمد</p></div>
<div class="m2"><p>خدا را، چاره دل کن، که این مسکین بجان آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا، ای سرو، گلزار جوانی را غنیمت دان</p></div>
<div class="m2"><p>که خواهد نوبهار حسن را روزی خزان آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببزم دیگران، دامن کشان، تا کی توان رفتن؟</p></div>
<div class="m2"><p>بسوی عاشقان هم گاه گاهی میتوان آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیاتی یافتم از وعده قتلش، بحمد الله!</p></div>
<div class="m2"><p>که ما را هر چه در دل بود او را بر زبان آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر زلفت ز بالا بر زمین افتاد و خوشحالم</p></div>
<div class="m2"><p>که بهر خاکساران آیتی از آسمان آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ملولم از غم دوران، سبک دوشی کن، ای ساقی</p></div>
<div class="m2"><p>ببر این کوه محنت را، که بر دلها گران آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمند زلف لیلی میکشد از ذوق مجنون را</p></div>
<div class="m2"><p>که از شهر عدم بیخود بصحرای جهان آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بامیدی که در پای سگانت جان برافشاند</p></div>
<div class="m2"><p>هلالی، نقد جان در آستین، بر آستان آمد</p></div></div>