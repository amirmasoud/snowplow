---
title: >-
    غزل شمارهٔ ۳۶۲
---
# غزل شمارهٔ ۳۶۲

<div class="b" id="bn1"><div class="m1"><p>دردا! که باز ما را دردی عجب رسیده</p></div>
<div class="m2"><p>هم دل ز دست رفته، هم جان بلب رسیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن ماهرو که با من شبها بروز کردی</p></div>
<div class="m2"><p>رفتست و در فراقش روزم بشب رسیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی باشد آنکه: بینم از دولت وصالش</p></div>
<div class="m2"><p>اندوه و درد رفته، عیش و طرب رسیده؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشکل که در قیامت بینند اهل دوزخ</p></div>
<div class="m2"><p>آنها که بر تو از من از تاب و تب رسیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیر از طلب، هلالی، کاری مکن درین ره</p></div>
<div class="m2"><p>هرکس رسیده جایی، بعد از طلب رسیده</p></div></div>