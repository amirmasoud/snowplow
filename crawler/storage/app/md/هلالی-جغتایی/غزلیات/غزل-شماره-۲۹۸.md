---
title: >-
    غزل شمارهٔ ۲۹۸
---
# غزل شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>جان بحسرت نتوان بی رخ جانان دادن</p></div>
<div class="m2"><p>خواهمش دیدن و حیران شدن و جان دادن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو جهان در عوض یک سر موی تو کمست</p></div>
<div class="m2"><p>دل و جان خود چه متاعیست که نتوان دادن؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جرعه ای بخش از آن لب، که ثوابیست عظیم</p></div>
<div class="m2"><p>تشنه را آب ز سر چشمه حیوان دادن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خال اگر نیست رخ خوب ترا ز آن سببست</p></div>
<div class="m2"><p>که بموری نتوان ملک سلیمان دادن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی افسانه خود پیش خیالت گویم؟</p></div>
<div class="m2"><p>درد سر این همه خوش نیست بمهمان دادن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی تو هجران بسرم گر اجل آرد روزی</p></div>
<div class="m2"><p>می توان جام خود از شوق بهجران دادن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چنین موج زند اشک هلالی هر دم</p></div>
<div class="m2"><p>خانمان را همه خواهیم بتوفان دادن</p></div></div>