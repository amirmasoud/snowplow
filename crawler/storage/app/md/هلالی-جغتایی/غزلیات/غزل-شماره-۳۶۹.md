---
title: >-
    غزل شمارهٔ ۳۶۹
---
# غزل شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>با تو هر ساعت مرا عرض نیازست این همه</p></div>
<div class="m2"><p>من نمی دانم ترا با من چه نازست این همه؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خنده ات جانست و لب جان بخش و خطت جانفزا</p></div>
<div class="m2"><p>مایه جمعیت و عمر درازست این همه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواب از چشم و دلم از دست دوست از کار رفت</p></div>
<div class="m2"><p>از فسون آن دو چشم سحر سازست این همه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلشن کوی ترا از جانب جنت دریست</p></div>
<div class="m2"><p>لیک بر ما بسته و بر غیر بازست این همه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سجود آستانت چهره ام پر گرد شد</p></div>
<div class="m2"><p>گرد چون گویم؟ که نور آن نمازست این همه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذوق ناوکهای دلدوزش مرا در دل نشست</p></div>
<div class="m2"><p>کز نوازشهای یار دلنوازست این همه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرح غمهای هلالی گوش کردن مشکلست</p></div>
<div class="m2"><p>مستمع را نکته های جان گدازست این همه</p></div></div>