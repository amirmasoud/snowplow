---
title: >-
    غزل شمارهٔ ۷۹
---
# غزل شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>دل چه باشد کز برای یار ازان نتوان گذشت</p></div>
<div class="m2"><p>یار اگر اینست، بالله می توان از جان گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خیال آن قد رعنا گذشتن مشکلست</p></div>
<div class="m2"><p>راست میگویم، بلی، از راستی نتوان گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز بروز وصل عمر و زندگی حیفست حیف</p></div>
<div class="m2"><p>حیف از آن عمری که بر من در شب هجران گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار بگذشت، از همه خندان و از من خشمناک</p></div>
<div class="m2"><p>عمر بر من مشکل و بر دیگران آسان گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون گذشت از دل خدنگش، گو: بیا، تیر اجل</p></div>
<div class="m2"><p>هر چه آید بگذرد، چون هر چه آمد آن گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش ازین گر جام عشرت داشتم، حالا چه سود؟</p></div>
<div class="m2"><p>از فلک دور دگر خواهم، که آن دوران گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلق گویندم: هلالی، درد خود را چاره کن</p></div>
<div class="m2"><p>کی توانم چاره دردی که از درمان گذشت؟</p></div></div>