---
title: >-
    غزل شمارهٔ ۸۹
---
# غزل شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>مرا بباده، نه باغ و بهار شد باعث</p></div>
<div class="m2"><p>بهار و باغ چه باشد؟ که یار شد باعث</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسیده بود گل، آن سرو هم بباغ آمد</p></div>
<div class="m2"><p>بیار می، که یکی صد هزار شد باعث</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبود ناله مرغ چمن ز جلوه گل</p></div>
<div class="m2"><p>لطافت رخ آن گلعذار شد باعث</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بمیکده رفتیم عذر ما بپذیر</p></div>
<div class="m2"><p>که باده خوردن ما را خمار شد باعث</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر ز کوی تو رفتیم اختیار نبود</p></div>
<div class="m2"><p>فغان و ناله بی اختیار شد باعث</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر از تو یک دو سه روزی جدا شدیم مرنج</p></div>
<div class="m2"><p>که گردش فلک و روزگار شد باعث</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قرار در شکن زلف یار خواهم کرد</p></div>
<div class="m2"><p>بدین قرار دل بیقرار شد باعث</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بمجلس تو هلالی کشید طعن رقیب</p></div>
<div class="m2"><p>گل وصال تو بر زخم خار شد باعث</p></div></div>