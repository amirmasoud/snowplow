---
title: >-
    غزل شمارهٔ ۱۷۹
---
# غزل شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>جان خواهم از خدا، نه یکی، بلکه صد هزار</p></div>
<div class="m2"><p>تا صد هزار بار بمیرم برای یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من زارم و تو زار، دلا، یک نفس بیا</p></div>
<div class="m2"><p>تا هر دو در فراق بنالیم زار زار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بسکه ریخت گریه خون در کنار من</p></div>
<div class="m2"><p>پر شد ازین کنار، جهان، تا بآن کنار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در روزگار هجر تو روزم سیاه شد</p></div>
<div class="m2"><p>بر روز من ببین که: چها کرد روزگار؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون دل اسیر تست، ز کوی خودش مران</p></div>
<div class="m2"><p>دلداریی کن و دل ما را نگاه دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کام من از دهان تو یک حرف بیش نیست</p></div>
<div class="m2"><p>بهر خدا که: لب بگشا، کام من بر آر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون خاک شد هلالی مسکین براه تو</p></div>
<div class="m2"><p>خاکش بگرد رفت و شد آن گرد هم غبار</p></div></div>