---
title: >-
    غزل شمارهٔ ۹۹
---
# غزل شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>ترا گهی که نظر بر من خراب افتد</p></div>
<div class="m2"><p>دلم ز بسکه تپد در من اضطراب افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم بیاد لبت هر زمان شود بیخود</p></div>
<div class="m2"><p>علی الخصوص زمانی که در شراب افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو چون شراب خوری با رقیب خنده زنان</p></div>
<div class="m2"><p>ز خنده تو نمک در دل کباب افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بهر جلوه چو خورشید من رود بر بام</p></div>
<div class="m2"><p>بخانها همه از روزن آفتاب افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگو: بدوزخ هجر افگنم هلالی را</p></div>
<div class="m2"><p>روا مدار که بیچاره در عذاب افتد</p></div></div>