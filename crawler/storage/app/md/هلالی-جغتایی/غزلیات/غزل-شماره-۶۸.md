---
title: >-
    غزل شمارهٔ ۶۸
---
# غزل شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>نخل بالای تو سر تا بقدم شیرینست</p></div>
<div class="m2"><p>این چه نخلست که هم نازک و هم شیرینست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه چون نیشکری نازک و شیرین و لطیف</p></div>
<div class="m2"><p>بند بند تو، ز سر تا بقدم شیرینست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه در عهد تو شیرین سخنان بسیارند</p></div>
<div class="m2"><p>کس بشیرین سخنی مثل تو کم شیرینست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دم صبحست، بیا، تا قدح از کف ننهیم</p></div>
<div class="m2"><p>که می تلخ درین یک دو سه دم شیرینست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نوشتست هلالی سخن لعل لبت</p></div>
<div class="m2"><p>چون نی قند سراپای قلم شیرینست</p></div></div>