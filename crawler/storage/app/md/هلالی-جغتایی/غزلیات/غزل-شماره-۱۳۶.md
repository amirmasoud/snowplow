---
title: >-
    غزل شمارهٔ ۱۳۶
---
# غزل شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>پیش از روزی، که خاک قالبم گل ساختند</p></div>
<div class="m2"><p>بهر سلطان خیالت کشور دل ساختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد هزاران آفرین بر کلک نقاشان صنع</p></div>
<div class="m2"><p>کز گل و آب این چنین شکل و شمایل ساختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوبرویان را جفا دادند و استغنا و ناز</p></div>
<div class="m2"><p>بر گرفتاران، بغایت، کار مشکل ساختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار ما این بود کز خوبان نگه داریم دل</p></div>
<div class="m2"><p>عاقبت ما را ز کار خویش غافل ساختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه! ازین حسرت که: هر جا خواستم بینم رخش</p></div>
<div class="m2"><p>پیش چشم من هزاران پرده حایل ساختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کجا رفتند خوبان، به شد از باغ بهشت</p></div>
<div class="m2"><p>خاصه آن جایی که روزی چند منزل ساختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می تپم، نی مرده و نی زنده، بر خاک درش</p></div>
<div class="m2"><p>همچو آن مرغی، که او را نیم بسمل ساختند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منظر عیش هلالی از فلک بگذشته بود</p></div>
<div class="m2"><p>خیل اندوه تو با خاکش مقابل ساختند</p></div></div>