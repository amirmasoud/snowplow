---
title: >-
    غزل شمارهٔ ۱۷۶
---
# غزل شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>می نویسم سخن از آتش دل بر کاغذ</p></div>
<div class="m2"><p>جای آنست اگر شعله زند در کاغذ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون قلم سوختی از آتش دل نامه من</p></div>
<div class="m2"><p>اگر از آب دو چشمم نشدی تر کاغذ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخن لعل تو خواهیم که در زر گیریم</p></div>
<div class="m2"><p>کاش سازند دگر از ورق زر کاغذ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خط مشکین ورق روی ترا زیبد و بس</p></div>
<div class="m2"><p>قابل آیت رحمت نبود هر کاغذ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرح بی مهری آن ماه بپایان نرسد</p></div>
<div class="m2"><p>فی المثل گر شود افلاک سراسر کاغذ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مردم از غم که: چرا نامه نوشتی برقیب؟</p></div>
<div class="m2"><p>نشدی، کاش! درین شهر میسر کاغذ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا هلالی صفت ماه جمال تو نوشت</p></div>
<div class="m2"><p>گشت، چون صفحه خورشید، منور کاغذ</p></div></div>