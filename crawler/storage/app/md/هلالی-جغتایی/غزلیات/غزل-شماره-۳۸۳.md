---
title: >-
    غزل شمارهٔ ۳۸۳
---
# غزل شمارهٔ ۳۸۳

<div class="b" id="bn1"><div class="m1"><p>گفتی: بگو که: بنده فرمان کیستی؟</p></div>
<div class="m2"><p>ما بنده توایم، تو سلطان کیستی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان میدهد ز بهر تو خلقی بهر طرف</p></div>
<div class="m2"><p>آیا ازین میانه تو جانان کیستی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای گنج حسن، با تو چه حاجت بیان شوق؟</p></div>
<div class="m2"><p>هم خود بگو که: در دل ویران کیستی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می بینمت که: بر سر ناز و کرشمه ای</p></div>
<div class="m2"><p>تا باز در کمین دل و جان کیستی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما از غمت هلاک و تو با غیر هم نفس</p></div>
<div class="m2"><p>بنگر کجاست درد و تو درمان کیستی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دور از رخ تو روز هلالی سیاه شد</p></div>
<div class="m2"><p>تا خود تو آفتاب درخشان کیستی؟</p></div></div>