---
title: >-
    غزل شمارهٔ ۴۰
---
# غزل شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>گر دعای دردمندان مستجابست، ای حبیب</p></div>
<div class="m2"><p>از خدا هرگز نخواهم خواست جز مرگ رقیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد بیماری و اندوه غریبی مشکلست</p></div>
<div class="m2"><p>وای مسکینی که هم بیمار باشد هم غریب!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر ببالینم ز درد هجر، نزدیک آمدست</p></div>
<div class="m2"><p>کز سر بالین من شرمنده برخیزد طبیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیگران دارند هر یک صد امید از خوان وصل</p></div>
<div class="m2"><p>من ز درد بی نصیبی چند باشم بی نصیب؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای صبا، جهدی کن و بگشا نقاب غنچه را</p></div>
<div class="m2"><p>تا کی از دیدار گل محروم باشد عندلیب؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان دهان کام منست و هست پنهان زیر لب</p></div>
<div class="m2"><p>چشم می دارم که کام من برآید عنقریب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون هلالی بی مه رویت ز جان سیر آمدم</p></div>
<div class="m2"><p>کس مباد از خوان وصل ماهرویان بی نصیب!</p></div></div>