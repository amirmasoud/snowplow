---
title: >-
    غزل شمارهٔ ۱۵
---
# غزل شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>یک دو روزی می گذارد یار من تنها مرا</p></div>
<div class="m2"><p>وه! که هجران می کشد امروز، یا فردا مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهر دلگیرست، تا آهنک صحرا کرد یار</p></div>
<div class="m2"><p>میروم، شاید که بگشاید دل از صحرا مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار آنجا و من این جا، وه! چه باشد گر فلک</p></div>
<div class="m2"><p>یار را این جا رساند، یا برد آنجا مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناله کمتر کن، دلا، پیش سگانش بعد ازین</p></div>
<div class="m2"><p>چند سازی در میان مردمان رسوا مرا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیر بدنامی ندارم سودی از سودای عشق</p></div>
<div class="m2"><p>مایه بازار رسواییست این سودا مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می کشم، گفتی: هلالی را باستغنا و ناز</p></div>
<div class="m2"><p>آری، آری، می کشد آن ناز و استغنا مرا</p></div></div>