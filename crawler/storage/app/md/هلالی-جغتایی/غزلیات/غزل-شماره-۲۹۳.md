---
title: >-
    غزل شمارهٔ ۲۹۳
---
# غزل شمارهٔ ۲۹۳

<div class="b" id="bn1"><div class="m1"><p>ای پریچهره من، چند نشینی بکسان؟</p></div>
<div class="m2"><p>دامن چون تو گلی حیف که گیرند خسان!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه من، چند باغیار کنی هم نفسی؟</p></div>
<div class="m2"><p>تیره شد آینه لطف تو زین هم نفسان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش هر سفله بشیرین سخنی لب مگشا</p></div>
<div class="m2"><p>شکرستان تو حیفست بکام مگسان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تکیه بر عشق جوانان هوسناک مکن</p></div>
<div class="m2"><p>که بغیر از هوسی نیست درین بوالهوسان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوخت بیچاره هلالی ز جفاهای رقیب</p></div>
<div class="m2"><p>چاره اش وصل حبیبست، خدایا، برسان!</p></div></div>