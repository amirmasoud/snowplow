---
title: >-
    غزل شمارهٔ ۲۶۷
---
# غزل شمارهٔ ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>آنکه از درد دل خود بفغانست منم</p></div>
<div class="m2"><p>وانکه از زندگی خویش بجانست منم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه هر روز دل از مهر بتان بردارد</p></div>
<div class="m2"><p>چون شود روز دگر باز همانست منم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه در حسن کنون شهره شهرست تویی</p></div>
<div class="m2"><p>وانکه در عشق تو رسوای جهانست منم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه در صومعه چل سال شب آورد بروز</p></div>
<div class="m2"><p>وین زمان معتکف دیر مغانست منم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در غمت گر چه بیک بار پریشان شده دل</p></div>
<div class="m2"><p>آنکه صد بار پریشان تر از آنست منم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقان تو همه نامی و نشانی دارند</p></div>
<div class="m2"><p>آنکه در عشق تو بی نام و نشانست منم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاقبت همچو هلالی شدم افسانه دهر</p></div>
<div class="m2"><p>آنکه هر جا سخنش ورد زبانست منم</p></div></div>