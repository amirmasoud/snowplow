---
title: >-
    غزل شمارهٔ ۲۵۵
---
# غزل شمارهٔ ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>خود را نشان ناوک بد خوی خود کنم</p></div>
<div class="m2"><p>رویش، بدین بهانه، مگر سوی خود کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر موی من هزار زبان باد در غمش</p></div>
<div class="m2"><p>تا من حکایت از غم یک موی خود کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا در حریم کوی تو پهلو نهاده ام</p></div>
<div class="m2"><p>هر دم هزار عیش ز پهلوی خود کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبها، که سر گران شوم از ساغر فراق</p></div>
<div class="m2"><p>بالین خود هم از سر زانوی خود کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آیینه وار خاک شدم از غبار غیر</p></div>
<div class="m2"><p>باشد که روی او طرف روی خود کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امشب ز وصف غیر، هلالی، خموش باش</p></div>
<div class="m2"><p>تا من سخن ز ماه سخن گوی خود کنم</p></div></div>