---
title: >-
    غزل شمارهٔ ۴۱۲
---
# غزل شمارهٔ ۴۱۲

<div class="b" id="bn1"><div class="m1"><p>تیر و کمان گرفته‌ای، سوی شکار می‌روی</p></div>
<div class="m2"><p>صید تواند عالمی، بهر چه کار می‌روی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانب صیدگه شدی، همره خویش بر مرا</p></div>
<div class="m2"><p>بی‌سگ خویشتن مرو، چون به شکار می‌روی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وه! چه سوار طرفه‌ای! کز سر مهر پیش تو</p></div>
<div class="m2"><p>چرخ پیاده می‌رود چون تو سوار می‌روی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون گذری به چشم من بر مژه‌ها قدم منه</p></div>
<div class="m2"><p>چند به پای همچو گل بر سر خار می‌روی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد تن زار من چو خس، بهر خدا، تو ای صبا</p></div>
<div class="m2"><p>همره خود ببر مرا، گر بر یار می‌روی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دل خاکسار من، کی تو به گرد او رسی؟</p></div>
<div class="m2"><p>کز پی بادپای او همچو غبار می‌روی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار چو بر قفای خود هیچ نگه نمی‌کند</p></div>
<div class="m2"><p>چند، هلالی، از پیش بی‌خود و زار می‌روی؟</p></div></div>