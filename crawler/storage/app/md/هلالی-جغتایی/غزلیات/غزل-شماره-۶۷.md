---
title: >-
    غزل شمارهٔ ۶۷
---
# غزل شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>بهر که قصه دل گفته ام دلش خونست</p></div>
<div class="m2"><p>توهم مپرس ز من، تا نگویمت چونست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم، که درد من از هیچ بیدلی کم نیست</p></div>
<div class="m2"><p>تویی، که ناز تو از هر چه گویم افزونست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگو که: خواب اجل بست چشم مردم را</p></div>
<div class="m2"><p>که چشم بندی آن نرگس پر افسونست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همای وصل تو پاینده باد بر سر من</p></div>
<div class="m2"><p>که زیر سایه او طالعم همایونست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنون که با توام، ای کاش دشمنان مرا</p></div>
<div class="m2"><p>خبر دهند که: لیلی بکام مجنونست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبیب، گو: بعلاج مریض عشق مکوش</p></div>
<div class="m2"><p>که کار او دگر و حال او دگرگونست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هلالی، از دهن و قامتش حکایت کن</p></div>
<div class="m2"><p>که این علامت ادراک طبع موزونست</p></div></div>