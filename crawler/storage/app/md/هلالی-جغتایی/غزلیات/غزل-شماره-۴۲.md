---
title: >-
    غزل شمارهٔ ۴۲
---
# غزل شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>ای شده خوی تو با من بتر از خوی رقیب</p></div>
<div class="m2"><p>روزم از هجر سیه ساخته چون روی رقیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفته بودی که: سگ ما ز رقیب تو بهست</p></div>
<div class="m2"><p>لیک پیش تو به از ماست سگ کوی رقیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه از کعبه کوی تو مرا مانع شد</p></div>
<div class="m2"><p>گر همه قبله شود، رو نکنم سوی رقیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن همه چین که در ابروی رقیبت دیدم</p></div>
<div class="m2"><p>کاش در زلف تو بودی، نه در ابروی رقیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا رقیب از تو مرا وعده دشنام آورد</p></div>
<div class="m2"><p>ذوق این مژده مرا ساخت دعاگوی رقیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بهر موی رقیب از فلک آید ستمی</p></div>
<div class="m2"><p>آن همه نیست سزای سر یک موی رقیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار پهلوی رقیبست و من از رشک هلاک</p></div>
<div class="m2"><p>غیر ازین فایده ای نیست ز پهلوی رقیب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون هلالی اگر از پای فتادم چه عجب؟</p></div>
<div class="m2"><p>چه کنم؟ نیست مرا قوت بازوی رقیب</p></div></div>