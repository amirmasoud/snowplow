---
title: >-
    غزل شمارهٔ ۵۰
---
# غزل شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>روز نوروزست، سرو گل عذار من کجاست؟</p></div>
<div class="m2"><p>در چمن یاران همه جمعند یار من کجاست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مونسم جز آه و یارب نیست شبها تا بروز</p></div>
<div class="m2"><p>آه و یارب! مونس شبهای تار من کجاست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشته مردم، هر یکی، امروز، صید چابکی</p></div>
<div class="m2"><p>چابک صید افگن مردم شکار من کجاست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست یک ساعت قرار این جان بی آرام را</p></div>
<div class="m2"><p>یارب! آن آرام جان بی قرار من کجاست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوخت از درد جدایی دل بامید وصال</p></div>
<div class="m2"><p>مرهم داغ دل امیدوار من کجاست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزگاری شد که دور افتاده ام، آخر بپرس</p></div>
<div class="m2"><p>کان سیه روز پریشان روزگار من کجاست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود عمری بر سر کویت هلالی خاک ره</p></div>
<div class="m2"><p>رفت بر باد و نگفتی: خاکسار من کجاست؟</p></div></div>