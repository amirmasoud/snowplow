---
title: >-
    غزل شمارهٔ ۳۵۶
---
# غزل شمارهٔ ۳۵۶

<div class="b" id="bn1"><div class="m1"><p>آن سایه نیست، دایم دنبال او فتاده</p></div>
<div class="m2"><p>چون من سیاه بختی سر در پیش نهاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دم ز جور خوبان در حیرتم که: ایزد</p></div>
<div class="m2"><p>آنرا که داده حسنی، مهری چرا نداده؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با جمع عشقبازان تنها مرا چه نسبت؟</p></div>
<div class="m2"><p>آن جمله کمتر از من، من از همه زیاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نام من برآید در حلقه سگانت</p></div>
<div class="m2"><p>طوق سگ تو بادا، در گردنم قلاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر میل باده داری، ای ترک مست، با من</p></div>
<div class="m2"><p>در دست هر چه دارم، بادا فدای باده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشمان خود برویم از مرحمت گشادی</p></div>
<div class="m2"><p>درهای رحمت تست بر روی من گشاده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا سر نهد بپایت، یا جان دهد هلالی</p></div>
<div class="m2"><p>اینک ز سر گذشته، منت بجان نهاده</p></div></div>