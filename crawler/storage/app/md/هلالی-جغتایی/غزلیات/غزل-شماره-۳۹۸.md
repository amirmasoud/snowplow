---
title: >-
    غزل شمارهٔ ۳۹۸
---
# غزل شمارهٔ ۳۹۸

<div class="b" id="bn1"><div class="m1"><p>بس که جانها همه شد صرف تو جانان کسی</p></div>
<div class="m2"><p>جان اگر نیست و گر هست تویی جان کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر بنده ستمهای تو از حد بگذشت</p></div>
<div class="m2"><p>شرمسارم ز کرمهای تو سلطان کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چاک شد جیب من، ای هجر، ز دست ستمت</p></div>
<div class="m2"><p>نرسد دست تو، یارب، بگریبان کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال شبهای مرا بی خبری کی داند؟</p></div>
<div class="m2"><p>که شبی روز نکردست بهجران کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر جدا ماندم از آن ماه ملامت مکنید</p></div>
<div class="m2"><p>چه کنم؟ چرخ فلک نیست بفرمان کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوسم هست که: دامان تو گیرم، لیکن</p></div>
<div class="m2"><p>بی کسان را نرسد دست بدامان کسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از فغانهای هلالی خبری نیست ترا</p></div>
<div class="m2"><p>وه! که هرگز نکنی گوش بافغان کسی</p></div></div>