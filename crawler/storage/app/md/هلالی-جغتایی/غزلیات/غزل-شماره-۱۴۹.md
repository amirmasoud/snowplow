---
title: >-
    غزل شمارهٔ ۱۴۹
---
# غزل شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>با من اول آن همه رسم وفاداری چه بود؟</p></div>
<div class="m2"><p>بعد ازان بی موجبی چندین جفاگاری چه بود؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرحمت بگذاشتی، تیغ جفا برداشتی</p></div>
<div class="m2"><p>آن محبت ها کجا شد؟ این ستمگاری چه بود؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم چشمم ز آزارت بخون آغشته شد</p></div>
<div class="m2"><p>نور چشم من، بگو: کین مردم آزاری چه بود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من نمی گویم که: چندین دشمنی آخر چراست؟</p></div>
<div class="m2"><p>لیک می پرسم که: اول آن همه یاری چه بود؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان دو گیسو، گر خدا قید گرفتاران نخواست</p></div>
<div class="m2"><p>این همه ترتیب اسباب گرفتاری چه بود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نبود، ای شوخ، آهنگ دلازاری ترا</p></div>
<div class="m2"><p>بی جهت با عاشقان آهنگ بیزاری چه بود؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوی خود خواندی هلالی را و راندی عاقبت</p></div>
<div class="m2"><p>عزت او را بدل کردن باین خواری چه بود؟</p></div></div>