---
title: >-
    غزل شمارهٔ ۱۸۱
---
# غزل شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>ای قامتت ز سرو سهی سرفرازتر</p></div>
<div class="m2"><p>لعلت، ز هر چه شرح دهم، دلنوازتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر آنکه با تو شبی آورم بروز</p></div>
<div class="m2"><p>خواهم شبی ز روز قیامت درازتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان از تب فراق تو در یک نفس گداخت</p></div>
<div class="m2"><p>هرگز تبی نبود ازین جانگدازتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من در رهت نهاده بیاری سر نیاز</p></div>
<div class="m2"><p>تو هر زمان زیاری من بی نیازتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درباختیم دنیی و عقبی بعشق پاک</p></div>
<div class="m2"><p>در کوی عشق نیست ز ما پاکبازتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دردا! که باز کار هلالی ز دست رفت</p></div>
<div class="m2"><p>کارش بساز، ای ز همه کارسازتر</p></div></div>