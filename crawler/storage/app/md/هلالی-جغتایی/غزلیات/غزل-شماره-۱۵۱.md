---
title: >-
    غزل شمارهٔ ۱۵۱
---
# غزل شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>یار اگر مرهم داغ دل محزون نشود</p></div>
<div class="m2"><p>با چنین داغ دلم خون نشود چون نشود؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز دل سخت تو خون شد همه دلها ز غمم</p></div>
<div class="m2"><p>دل مگر سنگ بود کز غم من خون نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این که با ما ستمت کم نشود باکی نیست</p></div>
<div class="m2"><p>کوشش ما همه اینست که: افزون نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بسر منزل لیلی گذری، جلوه کنان</p></div>
<div class="m2"><p>نیست ممکن که: ترا بیند و مجنون نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسکه در ناله ام از گردش گردون همه شب</p></div>
<div class="m2"><p>هیچ شب نیست دو صد ناله بگردون نشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته ای: خون تو ریزم، چه سعادت به ازین؟</p></div>
<div class="m2"><p>نیت خیر تو، یارب، که دگرگون نشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واعظا، ترک هلالی کن و افسانه مخوان</p></div>
<div class="m2"><p>کشته عشق بتان زنده بافسون نشود</p></div></div>