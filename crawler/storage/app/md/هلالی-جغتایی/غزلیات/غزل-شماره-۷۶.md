---
title: >-
    غزل شمارهٔ ۷۶
---
# غزل شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>کدام جلوه، که در سر و سرفراز تو نیست؟</p></div>
<div class="m2"><p>کدام فتنه، که در جلوه های ناز تو نیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن بخاک درش، ای رقیب، عرض نیاز</p></div>
<div class="m2"><p>که نازنین مرا حاجت نیاز تو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلا، بشام فراق از بلای حشر مپرس</p></div>
<div class="m2"><p>که روز کوته او چون شب دراز تو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سجده پیش رخش منع ما مکن، زاهد</p></div>
<div class="m2"><p>نیاز اهل محبت کم از نماز تو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکوی عشق، هلالی، نساختی کاری</p></div>
<div class="m2"><p>چه شد؟ مگر کرم دوست کارساز تو نیست؟</p></div></div>