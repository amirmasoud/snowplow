---
title: >-
    غزل شمارهٔ ۲۹۷
---
# غزل شمارهٔ ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>منم، چون غنچه، در خوناب زان گل برگ تر پنهان</p></div>
<div class="m2"><p>دلم صد پاره و هر پاره در خون جگر پنهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تماشای رخش، در دیده خوابی بود، پنداری</p></div>
<div class="m2"><p>که من تا چشم وا کردم شد از پیش نظر پنهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طبیبا، داغهای سینه را صد بار مرهم نه</p></div>
<div class="m2"><p>که دارم در ته هر داغ صد داغ دگر پنهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خط سبزی که خواهد رست از آن لب چیست میدانی؟</p></div>
<div class="m2"><p>برای کشتن من زهر دارد در شکر پنهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگو: تا زنده باشی عشق را از خلق پنهان کن</p></div>
<div class="m2"><p>که راز عاشقی هرگز نماند این قدر پنهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه تنها آشکارا داغ عشقت سوخت جان من</p></div>
<div class="m2"><p>بلای عشق جانسوزست، اگر پیدا و گر پنهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هلالی را چه سود از عشق پنهان داشتن در دل؟</p></div>
<div class="m2"><p>چو در عالم نخواهد ماند آخر این خبر پنهان</p></div></div>