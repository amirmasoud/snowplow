---
title: >-
    غزل شمارهٔ ۳۷۳
---
# غزل شمارهٔ ۳۷۳

<div class="b" id="bn1"><div class="m1"><p>ای که به خون مردمان چشم سیاه کرده‌ای</p></div>
<div class="m2"><p>کشته شدست عالمی، تا تو نگاه کرده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست به رخ نهاده‌ای، بهر حجاب از حیا</p></div>
<div class="m2"><p>پنجه آفتاب را برقع ماه کرده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پادشهی و ملک دل هست خراب ظلم تو</p></div>
<div class="m2"><p>زان که بلا و فتنه را خیل و سپاه کرده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخر عمر بر رخم داغ جفا کشیده‌ای</p></div>
<div class="m2"><p>پیر سفیدموی را نامه‌سیاه کرده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش، هلالی، این همه برق نبود بر فلک</p></div>
<div class="m2"><p>باز مگر ز سوز دل ناله و آه کرده‌ای؟</p></div></div>