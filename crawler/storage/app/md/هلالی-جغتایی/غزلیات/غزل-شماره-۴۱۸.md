---
title: >-
    غزل شمارهٔ ۴۱۸
---
# غزل شمارهٔ ۴۱۸

<div class="b" id="bn1"><div class="m1"><p>چند رسوا شوم از عشق من شیدایی؟</p></div>
<div class="m2"><p>عشق خوبست، ولیکن نه بدین رسوایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواستم پیش تو گویم غم تنهایی خویش</p></div>
<div class="m2"><p>آمدی سوی من و رفت غم تنهایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مست عشقیم، اگر هیچ ندانیم چه غم؟</p></div>
<div class="m2"><p>ذوق نادانی ما به ز غم دانایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر زمین جلوه نمودی، فلک از رشک بسوخت</p></div>
<div class="m2"><p>که فلک را ملکی نیست باین زیبایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو و گل نازک و رعناست، ولی نتوان یافت</p></div>
<div class="m2"><p>گل باین نازکی و سرو باین رعنایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چمن پیش تو رشکست ز نرگس ما را</p></div>
<div class="m2"><p>گر چه مشهور جهانست بنابینایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفتی و دیر شد ایام فراقت، چه کنم؟</p></div>
<div class="m2"><p>زور باز آی، که مردم ز غم تنهایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون سگ تست هلالی، دگرش منع مکن</p></div>
<div class="m2"><p>که درین راه چرا میروی و می آیی؟</p></div></div>