---
title: >-
    غزل شمارهٔ ۳۸۸
---
# غزل شمارهٔ ۳۸۸

<div class="b" id="bn1"><div class="m1"><p>ای مسلمانان، گرفتارم بدست کافری</p></div>
<div class="m2"><p>شوخ چشمی، تیز خشمی، ظالمی، غارتگری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با اسیران و غریبان سرکشی هر دم کنی</p></div>
<div class="m2"><p>از رخ گل رنگ او هر سو بهار خرمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با حریفان دگر معشوق عاشق پروری</p></div>
<div class="m2"><p>وز دهان تنگ او هر گوشه تنگ شکری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چیست دانی، صف بصف، مژگان تیزش هر طرف؟</p></div>
<div class="m2"><p>ناوک اندازان سپاهی، نیزه داران لشکری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بر سیمین، دلی داری، بسختی همچو سنگ</p></div>
<div class="m2"><p>وه! که دارد این چنین سنگین دلی، سیمین بری؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بندگانش تاجدارانند و گرد کوی او</p></div>
<div class="m2"><p>هر قدم تاج سری، افتاده بر خاک دری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاب ظلم او ندارم، الله الله! چون کنم؟</p></div>
<div class="m2"><p>من گدای بی کسی، او پادشاه کشوری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای که می گویی: هلالی، سر نخواهی باختن</p></div>
<div class="m2"><p>باش تا فردا میان خاک و خون بینی سری</p></div></div>