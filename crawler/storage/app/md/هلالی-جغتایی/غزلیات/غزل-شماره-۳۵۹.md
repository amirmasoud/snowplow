---
title: >-
    غزل شمارهٔ ۳۵۹
---
# غزل شمارهٔ ۳۵۹

<div class="b" id="bn1"><div class="m1"><p>بر بستر هلاکم، بیمار و زار مانده</p></div>
<div class="m2"><p>کارم ز دست رفته، دستم ز کار مانده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتست وصل جانان، ماندست جان بزاری</p></div>
<div class="m2"><p>ای کاشکی! نماندی این جان زار مانده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من کیستم؟ غریبی، از وصل بی نصیبی</p></div>
<div class="m2"><p>هجران یار دیده، دور از دیار مانده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دل ز گلعذاری، بودست خار خاری</p></div>
<div class="m2"><p>آن دل نمانده، اما آن خار خار مانده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با آنکه در هوایش، خاکم بگرد رفته</p></div>
<div class="m2"><p>او را هنوز از من بر دل غبار مانده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر جا که من براهی خود را باو رساندم</p></div>
<div class="m2"><p>او تیز در گذشته، من شرمسار مانده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وه! چون کنم؟ هلالی، کان ماه با رقیبان</p></div>
<div class="m2"><p>فارغ نشسته و من در انتظار مانده</p></div></div>