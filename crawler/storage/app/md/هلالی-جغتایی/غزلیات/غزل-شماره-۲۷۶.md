---
title: >-
    غزل شمارهٔ ۲۷۶
---
# غزل شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>ای که از خوبان مراد ما تویی مقصود هم</p></div>
<div class="m2"><p>چون تویی هرگز نبودست و نخواهد بود هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بسودای تو افتادیم در بازار عشق</p></div>
<div class="m2"><p>از زیان هر دو عالم فارغیم، از سود هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که بخت بد مرا سرگشته دارد چون فلک</p></div>
<div class="m2"><p>از فلک ناشادم و از بخت ناخشنود هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرد راهش گر برویم گل نخواهد کرد عشق</p></div>
<div class="m2"><p>چشم من گریان چرا شد، چهره گردآلوده هم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر، ای آرام جانها، رحمتی فرما که من</p></div>
<div class="m2"><p>سینه مجروح دارم، جان غم فرسود هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوز خود را چون نهان دارم؟ کزان رخسار و زلف</p></div>
<div class="m2"><p>در دل افتاد آتش و از جان برآمد دود هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون دل زار هلالی بی تو افغان برکشید</p></div>
<div class="m2"><p>چنگ بر درد دلش در ناله آمد، عود هم</p></div></div>