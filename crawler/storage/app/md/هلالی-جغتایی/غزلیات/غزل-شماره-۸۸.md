---
title: >-
    غزل شمارهٔ ۸۸
---
# غزل شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>خدا را، تند سوی من مبین، چون بنگرم سویت</p></div>
<div class="m2"><p>تغافل کن زمانی، تا ببینم یک زمان رویت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خاک کوی من، گفتی: برو، یا خاک شو اینجا</p></div>
<div class="m2"><p>چو آخر خاک خواهم شد من و خاک سر کویت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنم زارست و جان محزون، جگر پر درد و دل پر خون</p></div>
<div class="m2"><p>ترحم کن، که دیگر نیست تاب تندی از خویت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بصد تیغ ستم کشتی مرا، عذر تو چون خواهم؟</p></div>
<div class="m2"><p>کرمها میکنی، صد آفرین بر دست و بازویت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس از عمری اگر یک لحظه پهلوی تو بنشینم</p></div>
<div class="m2"><p>رقیب اندر میان آید، که دور افتم ز پهلویت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میانت یکسر مویست و جان در اشتیاق او</p></div>
<div class="m2"><p>بیا، ای جان مشتاقان فدای هر سر مویت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هلالی را نگشتی، گر سجود از دیدنت مانع</p></div>
<div class="m2"><p>سرش در سجده بودی، تا قیامت، پیش ابرویت</p></div></div>