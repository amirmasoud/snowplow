---
title: >-
    غزل شمارهٔ ۳
---
# غزل شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ای نور خدا در نظر از روی تو ما را</p></div>
<div class="m2"><p>بگذار که در روی تو بینیم خدا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نکهت جان بخش تو همراه صبا شد</p></div>
<div class="m2"><p>خاصیت عیسیست دم باد صبا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند که در راه تو خوبان همه خاکند</p></div>
<div class="m2"><p>حیفست که بر خاک نهی آن کف پا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش تو دعا گفتم و دشنام شنیدم</p></div>
<div class="m2"><p>هرگز اثری بهتر ازین نیست دعا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می خواستم آسوده بکنجی بنشینم</p></div>
<div class="m2"><p>بالای تو ناگاه بر انگیخت بلا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن روز که تعلیم تو می کرد معلم</p></div>
<div class="m2"><p>بر لوح تو ننوشت مگر حرف وفا را؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر یار کند میل، هلالی، عجبی نیست</p></div>
<div class="m2"><p>شاهان چه عجب گر بنوازند گدا را؟</p></div></div>