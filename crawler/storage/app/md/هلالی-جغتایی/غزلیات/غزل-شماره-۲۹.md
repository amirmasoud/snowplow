---
title: >-
    غزل شمارهٔ ۲۹
---
# غزل شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>بحمدالله که صحت داد ایزد پادشاهی را</p></div>
<div class="m2"><p>برآورد از سر نو بر سپهر حسن ماهی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معاذالله! اگر می کاست یک جو خرمن حسنش</p></div>
<div class="m2"><p>بباد نیستی می داد هر برگ گیاهی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو پا برداشتی، ای نرگس رعنا، بغمازی</p></div>
<div class="m2"><p>قدم آهسته نه، دیگر مرنجان خاک راهی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بشکر آنکه شاه مسند حسنی، بصد عزت</p></div>
<div class="m2"><p>مران از خاک راه خود بخواری دادخواهی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بیمارند چشمان تو خون کم می توان کردن</p></div>
<div class="m2"><p>چرا هر لحظه می ریزند خون بی گناهی را؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سهی سرو ریاض حسن چون سر سبز و خرم شد</p></div>
<div class="m2"><p>چه نقصان گر خزان پژمرده می سازد گیاهی را؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هلالی را فدای آن شه خوبان کن، ای گردون</p></div>
<div class="m2"><p>چرا بی تاب میداری مه انجم سپاهی را؟</p></div></div>