---
title: >-
    غزل شمارهٔ ۲۱۳
---
# غزل شمارهٔ ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>ترک یاری کردی، از وصل تو یاران را چه حظ؟</p></div>
<div class="m2"><p>دشمن احباب گشتی، دوستداران را چه حظ؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ندارد وعده وصل تو امکان وفا</p></div>
<div class="m2"><p>غیر داغ انتظار امیدواران را چه حظ؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم من، کز گریه نابیناست، چون بیند رخت؟</p></div>
<div class="m2"><p>از تماشای چمن ابر بهاران را چه حظ؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد بی درمان خوبان چون نمی گیرد قرار</p></div>
<div class="m2"><p>دردمندان را چه حاصل، بیقراران را چه حظ؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن سوار از خاک ما تا کی برانگیزد غبار؟</p></div>
<div class="m2"><p>از غبار انگیختن، یارب، سواران را چه حظ؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میدهد خاک رهش خاصیت آب حیات</p></div>
<div class="m2"><p>ور نه زین گرد مذلت خاکساران را چه حظ؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یارب از قتل هلالی چیست مقصود بتان؟</p></div>
<div class="m2"><p>از هلاک عندلیبان گلعذاران را چه حظ؟</p></div></div>