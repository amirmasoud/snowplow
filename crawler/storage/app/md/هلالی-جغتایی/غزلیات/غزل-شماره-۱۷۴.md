---
title: >-
    غزل شمارهٔ ۱۷۴
---
# غزل شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>من نمیخواهم که : در کویش مرا بسمل کنید</p></div>
<div class="m2"><p>حیف باشد کان چنان خاکی بخونم گل کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نخواهم زیست دور از روی او، بهر خدا</p></div>
<div class="m2"><p>تیغ بردارید و پیش او مرا بسمل کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر قتلم رنجه میدارید دست ناز کش</p></div>
<div class="m2"><p>هم بدست خود مرا قربان آن قاتل کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بعزم خاک بردارید تابوت مرا</p></div>
<div class="m2"><p>هر قدم، صد جا، بگرد کوی او منزل کنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا رخش من بینم و جز من نبیند دیگری</p></div>
<div class="m2"><p>پیش رویش پرده چشم مرا حایل کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل در آن کویست و من بیدل، خدا را، بعد ازین</p></div>
<div class="m2"><p>بگذرید از فکر دل، فکر من بیدل کنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای حریفانی، که جا در بزم آن مه کرده اید</p></div>
<div class="m2"><p>تا هلالی هم درآید، رخصتی حاصل کنید</p></div></div>