---
title: >-
    غزل شمارهٔ ۳۳۷
---
# غزل شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>چند پنهان کنم افسانه هجران از تو؟</p></div>
<div class="m2"><p>حال من بر همه پیداست، چه پنهان از تو؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع جمعی و همه سوخته وصل تواند</p></div>
<div class="m2"><p>گنج حسنی و جهانی همه ویران از تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باری، ای کافر بی رحم، چه در دل داری؟</p></div>
<div class="m2"><p>که نیاسود دل هیچ مسلمان از تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جیب گل پیرهنان چاک شد از دست غمت</p></div>
<div class="m2"><p>ورنه بودی همه را سر بگریبان از تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست این غنچه خندان که شکفتست بباغ</p></div>
<div class="m2"><p>دل خونین جگرانست پریشان از تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غنچه در باغ ز باد سحر آشفته نبود</p></div>
<div class="m2"><p>بلکه صد پاره دلی داشت پریشان از تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طالب وصل ترا محنت هجران شرطست</p></div>
<div class="m2"><p>تا میسر نشود کام دل آسان از تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن پری بزم بیاراست، هلالی، برخیز</p></div>
<div class="m2"><p>جام جم گیر، که شد ملک سلیمان از تو</p></div></div>