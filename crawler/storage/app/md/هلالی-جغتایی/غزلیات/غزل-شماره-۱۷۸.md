---
title: >-
    غزل شمارهٔ ۱۷۸
---
# غزل شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>وه! چه شورانگیزی، این شیرین پسر؟</p></div>
<div class="m2"><p>هم نمک می ریزد از تو، هم شکر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک پایت، چون مرا فرق سرست</p></div>
<div class="m2"><p>من چرا بر دارم از پای تو سر؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک گشتم، لاله از خاکم دمید</p></div>
<div class="m2"><p>هم چنان داغ تو دارم بر جگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی خبر بودن ز عالم، آگهیست</p></div>
<div class="m2"><p>زاهد افسرده کی دارد خبر؟</p></div></div>