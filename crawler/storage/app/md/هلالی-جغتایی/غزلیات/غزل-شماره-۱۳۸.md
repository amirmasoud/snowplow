---
title: >-
    غزل شمارهٔ ۱۳۸
---
# غزل شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>عاشقان، هر چند مشتاق جمال دلبرند</p></div>
<div class="m2"><p>دلبران بر عاشقان از عاشقان عاشق ترند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق می نازد بحسن و حسن می نازد بعشق</p></div>
<div class="m2"><p>آری، آری، این دو معنی عاشق یکدیگرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گلستان گر بپای بلبلان خاری خلد</p></div>
<div class="m2"><p>نو عروسان چمن صد جامه بر تن میدرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان شیرین با لبت آمیخت، گویا، در ازل</p></div>
<div class="m2"><p>گوهر جان من و لعل تو از یک گوهرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای رقیب، از منع ما بگذر، که جانبازان عشق</p></div>
<div class="m2"><p>از سر جان بگذرند، اما ز جانان نگذرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مردم و رحمی ندیدم زین بتان سنگدل</p></div>
<div class="m2"><p>من نمی دانم مسلمانند، یا خود کافرند؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با تن لاغر، هلالی، از غم خوبان منال</p></div>
<div class="m2"><p>تن اگر بگداخت، با کی نیست، جان می پرورند</p></div></div>