---
title: >-
    غزل شمارهٔ ۳۷۶
---
# غزل شمارهٔ ۳۷۶

<div class="b" id="bn1"><div class="m1"><p>امشب تو باز چشم و چراغ که بوده‌ای؟</p></div>
<div class="m2"><p>جانم بسوخت، مرهم داغ که بوده‌ای؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای باغ نوشکفته کجا رفته‌ای چو ابر؟</p></div>
<div class="m2"><p>ای سرو نورسیده به باغ که بوده‌ای؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من چون چراغ چشم به راه تو داشتم</p></div>
<div class="m2"><p>ای نور هردو دیده چراغ که بوده‌ای؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم هزار تفرقه در گوشه فراق</p></div>
<div class="m2"><p>کز فارغان بزم فراغ که بوده‌ای؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای گل که جان ز بوی خوشت تازه می‌شود،</p></div>
<div class="m2"><p>مردم ز رشک، عطر دماغ که بوده‌ای؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز این غبار چیست، هلالی، به روی تو؟</p></div>
<div class="m2"><p>در کوی مهوشان به سراغ که بوده‌ای؟</p></div></div>