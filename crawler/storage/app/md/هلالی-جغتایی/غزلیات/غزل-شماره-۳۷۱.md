---
title: >-
    غزل شمارهٔ ۳۷۱
---
# غزل شمارهٔ ۳۷۱

<div class="b" id="bn1"><div class="m1"><p>دوش پیمانه تهی آمدم از می خانه</p></div>
<div class="m2"><p>کاشکی! پر شود امروز مرا پیمانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد مردن اگر از قالب من خشت زنند</p></div>
<div class="m2"><p>آیم و باز شوم خشت در می خانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواستم کین دل سودا زده عاقل گردد</p></div>
<div class="m2"><p>وه! که عاقل نشد و ساخت مرا دیوانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتابی و رخت شمع جهان افروزست</p></div>
<div class="m2"><p>همه ذرات جهان گرد سرت پروانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می تپد مرغ دلم بر سر آن دانه دل</p></div>
<div class="m2"><p>چه کند؟ خرمن عمرست همین یک دانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آشنایی ز جفاهای تو محرومم ساخت</p></div>
<div class="m2"><p>ای خوش آن روز که بودیم ز هم بیگانه!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قصه خویش به احباب چه گویم هر شب؟</p></div>
<div class="m2"><p>این شب آن نیست که کوته شود از افسانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوش در کلبه ویران هلالی بودیم</p></div>
<div class="m2"><p>حال دیوانه خرابست درین ویرانه</p></div></div>