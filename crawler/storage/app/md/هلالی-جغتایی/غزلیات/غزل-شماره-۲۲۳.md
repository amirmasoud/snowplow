---
title: >-
    غزل شمارهٔ ۲۲۳
---
# غزل شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>ای در دلم ز آتش عشق تو صد الم</p></div>
<div class="m2"><p>هر یک الم نشانه چندین هزار غم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصل تو زود رفت و فراق تو دیر ماند</p></div>
<div class="m2"><p>فریاد ازین عقوبت بسیار و عمر کم!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانی کدام روز عدم شد وجود ما؟</p></div>
<div class="m2"><p>روزی که عاشقی بوجود آمد از عدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویند: درد عشق بدرمان نمیرسد</p></div>
<div class="m2"><p>من چون زیم؟ که عاشقم و دردمندهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماییم و نیم جانی و هر دم هزار آه</p></div>
<div class="m2"><p>اینک بباد میرود آن نیز دم بدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون آب زندگیست قدم تا بفرق سر</p></div>
<div class="m2"><p>خواهم درون جان کنمت فرق تا قدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای پادشاه حسن، هلالی گدای تست</p></div>
<div class="m2"><p>خواهم که سوی او گذری از ره کرم</p></div></div>