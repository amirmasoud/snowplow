---
title: >-
    غزل شمارهٔ ۳۹۶
---
# غزل شمارهٔ ۳۹۶

<div class="b" id="bn1"><div class="m1"><p>تا کی بکنج صبر جگر خون کند کسی؟</p></div>
<div class="m2"><p>امکان صبر نیست، دگر چون کند کسی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان را اگر بمهر تو از دل برون کند</p></div>
<div class="m2"><p>از جان چگونه مهر تو بیرون کند کسی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یارب، چه حالتست؟ که روزی هزار بار</p></div>
<div class="m2"><p>هر لحظه آرزوی تو افزون کند کسی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون می کنی، یکی بترحم نگاه کن</p></div>
<div class="m2"><p>تا بهر یک نگاه تو صد خون کند کسی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیرانم از جنون هلالی و طعن خلق</p></div>
<div class="m2"><p>یعنی: چرا ملامت مجنون کند کسی؟</p></div></div>