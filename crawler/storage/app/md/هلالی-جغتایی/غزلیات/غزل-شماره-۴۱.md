---
title: >-
    غزل شمارهٔ ۴۱
---
# غزل شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>من بکویت عاشق زار و دل غمگین غریب</p></div>
<div class="m2"><p>چون زید بیچاره عاشق؟ چون کند مسکین غریب؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرسش حال غریبان رسم و آیینست، لیک</p></div>
<div class="m2"><p>هست در شهر شما این رسم و این آیین غریب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خم زلف کجت دلها غریب افتاده اند</p></div>
<div class="m2"><p>زلف تو شام غریبانست و ما چندین غریب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت دشنامم بشکر خنده لب بگشا، که هست</p></div>
<div class="m2"><p>در میان تلخ گفتن خنده شیرین غریب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر ز بالین غریبی بر ندارد تا بحشر</p></div>
<div class="m2"><p>گر طبیبی چون تو یابد بر سر بالین غریب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسکه باشد شاد هر کس با رفیقان در وطن</p></div>
<div class="m2"><p>رو بدیوار غم آرد خسته غمگین غریب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر کویت هلالی بس غریب و بی کسست</p></div>
<div class="m2"><p>آخر، ای شاه غریبان، لطف کن بر این غریب</p></div></div>