---
title: >-
    غزل شمارهٔ ۵۸
---
# غزل شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>ماه من، عیدست و شهری را نظر بر روی تست</p></div>
<div class="m2"><p>روی تو چون ماه عید و ماه نو ابروی تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشن آن چشمی که ماه عید بر روی تو دید</p></div>
<div class="m2"><p>شادی آن کس که روز عید در پهلوی تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می رود هر کس بطوف عید گاه از کوی تو</p></div>
<div class="m2"><p>من ز کویت چون روم؟ چون عید گاهم کوی تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در صباح عید، اگر مشغول تکبیرند خلق</p></div>
<div class="m2"><p>بر زبانم از سحر تا شام گفت و گوی تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بیندازی خدنگی از کمان ابرویت</p></div>
<div class="m2"><p>بر دل و بر سینه من منت ابروی تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز عید و مایل خوبان ز هر سو عالمی</p></div>
<div class="m2"><p>میل من از جمله خوبان عالم سوی تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کسی هندوی خود را شاد سازد روز عید</p></div>
<div class="m2"><p>شاد کن مسکین هلالی را، که او هندوی تست</p></div></div>