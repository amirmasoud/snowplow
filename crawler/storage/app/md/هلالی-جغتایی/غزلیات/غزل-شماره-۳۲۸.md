---
title: >-
    غزل شمارهٔ ۳۲۸
---
# غزل شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>خوش باشد اگر باشم در طرف چمن با او</p></div>
<div class="m2"><p>من باشم و او باشد، او باشد و من با او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر هم زدن چشمش جان می برد از مردم</p></div>
<div class="m2"><p>کی زنده توان بودن یک چشم زدن با او؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با او چو پس از عمری خواهم سخنی گویم</p></div>
<div class="m2"><p>هرگز نشود پیدا تقریب سخن با او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانم بر جانانست، من خود تن بی جانم</p></div>
<div class="m2"><p>آری ز کجا باشد جان در تن و تن با او؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا عهد شکست آن مه بگداخت هلالی را</p></div>
<div class="m2"><p>دیدی که چه کرد آخر، آن عهدشکن با او؟</p></div></div>