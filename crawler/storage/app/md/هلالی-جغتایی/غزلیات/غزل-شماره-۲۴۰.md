---
title: >-
    غزل شمارهٔ ۲۴۰
---
# غزل شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>هر زمان بر صف خوبان بتماشا گذرم</p></div>
<div class="m2"><p>چون رسم پیش تو نتوانم از آنجا گذرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم آن سر که: بسودای تو بازم سر خویش</p></div>
<div class="m2"><p>سر چه کار آید؟ اگر زین سر و سودا گذرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان خط سبز و لب لعل گذشتن نتوان</p></div>
<div class="m2"><p>گر بصد مرتبه از خضر و مسیحا گذرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم نشینا، قدمی چند بمن همره شو</p></div>
<div class="m2"><p>که برش طاقت آن نیست که تنها گذرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قصر مقصود بلندست، خدایا، سببی</p></div>
<div class="m2"><p>که ازین مرحله بر عالم بالا گذرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رشته مهر تو گر دست دهد، همچو مسیح</p></div>
<div class="m2"><p>پا بگردن نهم و از سر دنیا گذرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من که امروز، هلالی، خوشم از دولت عشق</p></div>
<div class="m2"><p>بهتر آنست کز اندیشه فردا گذرم</p></div></div>