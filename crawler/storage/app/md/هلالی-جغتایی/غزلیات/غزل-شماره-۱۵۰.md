---
title: >-
    غزل شمارهٔ ۱۵۰
---
# غزل شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>کاکل ز چه بگذاشته ای تا کمر خود؟</p></div>
<div class="m2"><p>مگذار بلاهای چنین را بسر خود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتار ترا، گر ملک از عرش ببیند</p></div>
<div class="m2"><p>آید بزمین فرش کند بال و پر خود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم تو نهان یک نظر از لطف بینداخت</p></div>
<div class="m2"><p>ما را ز چه انداخته ای از نظر خود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیروز ز حال همه عالم خبرم بود</p></div>
<div class="m2"><p>امروز چنانم که: ندارم خبر خود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عشق تو از من اثری بیش نماندست</p></div>
<div class="m2"><p>نزدیک شد آن دم که نیابم اثر خود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من کشته شوم به که جدا افتم از آن در</p></div>
<div class="m2"><p>زارم بکش و دور میفگن ز در خود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دور از تو چه گویم: بچه حالست هلالی؟</p></div>
<div class="m2"><p>درمانده بدرد دل خونین جگر خود</p></div></div>