---
title: >-
    غزل شمارهٔ ۲۲۸
---
# غزل شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>بیار بی وفا عمری وفا کردم ندانستم</p></div>
<div class="m2"><p>بامید وفا بر خود جفا کردم ندانستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل آزاری، که هرگز دیده بر مردم نیندازد</p></div>
<div class="m2"><p>بسان مردمش در دیده جا کردم ندانستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر گفتم که: دارد یار من آیین دلجویی</p></div>
<div class="m2"><p>معاذالله! غلط کردم، خطا کردم، ندانستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلای جان من آن شوخ و من افتاده در کویش</p></div>
<div class="m2"><p>دریغا! خانه در کوی بلا کردم ندانستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر بیگانه باشد خوی او از آشنا بهتر</p></div>
<div class="m2"><p>بآن بیگانه خود را آشنا کردم ندانستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرفتم آن سر زلف و کشیدم صد گرفتاری</p></div>
<div class="m2"><p>بدست خویش خود را مبتلا کردم ندانستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هلالی، پیش آن مه شرمسارم زین شکایتها</p></div>
<div class="m2"><p>درین معنی بغایت ماجرا کردم ندانستم</p></div></div>