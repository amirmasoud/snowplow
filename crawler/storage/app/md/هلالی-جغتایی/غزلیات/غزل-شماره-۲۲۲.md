---
title: >-
    غزل شمارهٔ ۲۲۲
---
# غزل شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>آمد بهار و خوشدلم از رنگ و بوی گل</p></div>
<div class="m2"><p>آن به که می کشم دو سه روزی بروی گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل دیدم، آرزوی کسی در دلم فتاد</p></div>
<div class="m2"><p>کز دیدنش کسی نکند آرزوی گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این دم که بوی دلکش گل میدهد نسیم</p></div>
<div class="m2"><p>بس دلکشست گشت گلستان ببوی گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش آن که یار باشد و من در حریم باغ</p></div>
<div class="m2"><p>من سوی او نظر فگنم، او بسوی گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دید آن دو رخ هلالی و آسوده دل نشست</p></div>
<div class="m2"><p>از جست و جوی لاله و از گفت و گوی گل</p></div></div>