---
title: >-
    غزل شمارهٔ ۵۳
---
# غزل شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>هر آتشین گلی، که بر اطراف خاک ماست</p></div>
<div class="m2"><p>از آتش دل و جگر چاک چاک ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامن کشان ز خاک شهیدان گذشته ای</p></div>
<div class="m2"><p>گردی، که دامن تو گرفتست، خاک ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی، برو، که باده گل رنگ بی لبش</p></div>
<div class="m2"><p>گر آب زندگیست که زهر هلاک ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پاکست همچو دامن گل چشم ما ولی</p></div>
<div class="m2"><p>دامان یار پاک تر از چشم پاک ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهقان سالخورده، که پاینده باد، گفت:</p></div>
<div class="m2"><p>آنست آب خضر، که در جوی تاک ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درمان دل مجوی، هلالی، که درد عشق</p></div>
<div class="m2"><p>خاص از برای جان و دل دردناک ماست</p></div></div>