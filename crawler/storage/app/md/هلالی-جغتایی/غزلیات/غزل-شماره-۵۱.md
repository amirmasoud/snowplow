---
title: >-
    غزل شمارهٔ ۵۱
---
# غزل شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>ای باد صبح، منزل جانان من کجاست؟</p></div>
<div class="m2"><p>من مردم، از برای خدا، جان من کجاست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبهای هجر همچو منی کس غریب نیست</p></div>
<div class="m2"><p>کس را تحمل شب هجران من کجاست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر خاک شد بر آن سر میدان و او نگفت:</p></div>
<div class="m2"><p>گویی که بود در خم چوگان من کجاست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوبان سمند ناز بمیدان فگنده اند</p></div>
<div class="m2"><p>چابک سوار عرصه میدان من کجاست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی رقیب دست و گریبان شود بمن؟</p></div>
<div class="m2"><p>شوخی که می گرفت گریبان من کجاست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش آنکه: چون بسینه ز پیکان نشان نیافت</p></div>
<div class="m2"><p>تیر دگر کشید که: پیکان من کجاست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از نه فلک گذشت، هلالی، فغان من</p></div>
<div class="m2"><p>بنگر که: من کجایم و افغان من کجاست؟</p></div></div>