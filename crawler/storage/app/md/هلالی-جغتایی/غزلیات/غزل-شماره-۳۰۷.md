---
title: >-
    غزل شمارهٔ ۳۰۷
---
# غزل شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>از فراق آن پری هر دم فزون شد درد من</p></div>
<div class="m2"><p>ساخت ظاهر درد دل را اشک و رنگ زرد من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بکی از عشق او جور و جفا خواهم کشید؟</p></div>
<div class="m2"><p>ای رفیقان، سوخت دیگر جان غم پرورد من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه دور از آستان دوست گشتم خاک راه</p></div>
<div class="m2"><p>کاش! روزی باد در کویش رساند گرد من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش عشق تو در جان من شیدا فتاد</p></div>
<div class="m2"><p>شد مدد با آتش عشق تو آه سرد من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون هلالی در غم عشق بتان سنگدل</p></div>
<div class="m2"><p>محنت و اندوه خوبان برد خواب و خورد من</p></div></div>