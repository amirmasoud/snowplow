---
title: >-
    غزل شمارهٔ ۳۰۶
---
# غزل شمارهٔ ۳۰۶

<div class="b" id="bn1"><div class="m1"><p>نظاره کن در آینه خود را، حبیب من</p></div>
<div class="m2"><p>اما بشرط آنکه نگردی رقیب من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از وطن جدا و دل من ز من جدا</p></div>
<div class="m2"><p>آگاه نیست یار ز حال غریب من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین سان که درد عشق توام ساخت ناتوان</p></div>
<div class="m2"><p>مشکل زیم، اگر تو نباشی طبیب من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی خورم غم و پی تسکین درد خویش</p></div>
<div class="m2"><p>گویم بخود که: در ازل این شد نصیب من؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آزرده شد هلالی و آن گل نگفت هیچ:</p></div>
<div class="m2"><p>تا کی جفای خار کشد عندلیب من؟</p></div></div>