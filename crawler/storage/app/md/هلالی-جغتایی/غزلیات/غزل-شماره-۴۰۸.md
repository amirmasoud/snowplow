---
title: >-
    غزل شمارهٔ ۴۰۸
---
# غزل شمارهٔ ۴۰۸

<div class="b" id="bn1"><div class="m1"><p>ز روی ناز و حیا منعم از نیاز کنی</p></div>
<div class="m2"><p>نیازمند توام، گر هزار ناز کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی که جانب احباب چشم باز کنی</p></div>
<div class="m2"><p>بهر نیاز که بینی هزار ناز کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همیشه باز کنی چشم لطف سوی کسان</p></div>
<div class="m2"><p>چو گویمت که: مکن، از ستیزه باز کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز پیش دیده ما گر نهان شدی چه عجب؟</p></div>
<div class="m2"><p>فرشته خویی و از مردم احتراز کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمان وصل تو عمر منست، وه! چه شود</p></div>
<div class="m2"><p>اگر نشینی و عمر مرا دراز کنی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار سجده کنی، جان من، بآن نرسد</p></div>
<div class="m2"><p>که بر جنازه مقتول خود نماز کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلا، زدی نفس گرم و آب شد جگرم</p></div>
<div class="m2"><p>نعوذ بالله، اگر آه جان گداز کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیاز خویش، هلالی، بخلق عرضه مده</p></div>
<div class="m2"><p>خوش آنکه روی بدرگاه بی نیاز کنی!</p></div></div>