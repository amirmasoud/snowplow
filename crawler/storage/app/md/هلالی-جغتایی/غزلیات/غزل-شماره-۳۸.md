---
title: >-
    غزل شمارهٔ ۳۸
---
# غزل شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>شب هجرست و مرگ خویش خواهم از خدا امشب</p></div>
<div class="m2"><p>اجل روزی چو سویم خواهد آمد، گو: بیا امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین دردی که من دارم نخواهم زیست تا فردا</p></div>
<div class="m2"><p>بیا، بنشین، که جان خواهم سپرد امروز، یا امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل و جانی که بود، آواره شد دوش از غم هجران</p></div>
<div class="m2"><p>دگر، یارب! غم هجران چه میخواهد ز ما امشب؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه سر شد خاک درگاهت، نه پا فرسود در راهت</p></div>
<div class="m2"><p>مرا چون شمع باید سوخت از سر تا بپا امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب آمد، باز دور افگند از وصلت هلالی را</p></div>
<div class="m2"><p>دریغا! شد هلال و آفتاب از هم جدا امشب</p></div></div>