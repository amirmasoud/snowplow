---
title: >-
    غزل شمارهٔ ۱۰۵
---
# غزل شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>مسکین طبیب، چاره دردم خیال کرد</p></div>
<div class="m2"><p>بیچاره را ببین: چه خیال محال کرد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی می رسد خیال طبیبان بدرد من؟</p></div>
<div class="m2"><p>دردم بدان رسید که نتوان خیال کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارد هزار تفرقه دل در شب فراق</p></div>
<div class="m2"><p>کو آن فراغتی که بروز وصال کرد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل پیش عارض تو شد از انفعال سرخ</p></div>
<div class="m2"><p>آن خنده ای که کردهم از انفعال کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنگین دلی، که اسب جفا تاخت بر سرم</p></div>
<div class="m2"><p>موری ضعیف را بستم پایمال کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلطان وقت شد ز گدایان کوی عشق</p></div>
<div class="m2"><p>درویش میل سلطنت بی زوال کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی: که حلقه ساخت، هلالی، قد ترا</p></div>
<div class="m2"><p>آن کس که ابروان ترا چون هلال کرد</p></div></div>