---
title: >-
    غزل شمارهٔ ۵۲
---
# غزل شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>ز باغ عمر عجب سرو قامتی برخاست</p></div>
<div class="m2"><p>بگو که: در همه عالم قیامتی برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سمند عشق بهر منزلی، که جولان کرد</p></div>
<div class="m2"><p>غبار فتنه ز گرد ملامتی برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مقیم کوی تو چون در حریم کعبه نشست</p></div>
<div class="m2"><p>بآه حسرت و اشک ندامتی برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم براه ملامت فتاد و این عجبست</p></div>
<div class="m2"><p>عجب تر آن که: ز کوی سلامتی برخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>براه عشق هلالی فتاده بود ز پا</p></div>
<div class="m2"><p>سمند مقدم صاحب کرامتی برخاست</p></div></div>