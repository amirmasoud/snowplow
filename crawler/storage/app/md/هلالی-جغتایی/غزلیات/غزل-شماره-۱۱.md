---
title: >-
    غزل شمارهٔ ۱۱
---
# غزل شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>یار ما هرگز نیازارد دل اغیار را</p></div>
<div class="m2"><p>گل سراسر آتشست، اما نسوزد خار را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیگر از بی طاقتی خواهم گریبان چاک زد</p></div>
<div class="m2"><p>چند پوشم سینه ریش و دل افگار را؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر من آزرده رحمی کن، خدا را، ای طبیب</p></div>
<div class="m2"><p>مرهمی نه، کز دلم بیرون برد آزار را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باغ حسنت تازه شد از دیده گریان من</p></div>
<div class="m2"><p>چشم من آب دگر داد آن گل رخسار را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز هجر از خاطرم اندیشه وصلت نرفت</p></div>
<div class="m2"><p>آرزوی صحت از دل کی رود بیمار را؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حال خود گفتی: بگو، بسیار و اندک هرچه هست</p></div>
<div class="m2"><p>صبر اندک را بگویم، یا غم بسیار را؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیدن دیدار جانان دولتی باشد عظیم</p></div>
<div class="m2"><p>از خدا خواهد هلالی دولت دیدار را</p></div></div>