---
title: >-
    غزل شمارهٔ ۲۵
---
# غزل شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>بچه نسبت کنم آن سرو قد دلجو را؟</p></div>
<div class="m2"><p>هر چه گویم، به از آنست، چه گویم او را؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشنو، از بهر خدا، در حق من قول رقیب</p></div>
<div class="m2"><p>که نکو نیست شنیدن خبر بد گو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه بد خوی مرا داد چنان روی نکو</p></div>
<div class="m2"><p>کاشکی خوی نکوهم دهد آن بد خو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیغ بر من چه زنی؟ حیف که همچون تو کسی</p></div>
<div class="m2"><p>بهر آزار سگی رنجه کند بازو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمت آهوست، نظر سوی رقیبان مفگن</p></div>
<div class="m2"><p>پند بشنو، بسگان رام مکن آهو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسکه دارم المی بر دل از آزردن او</p></div>
<div class="m2"><p>شب همه شب به خس و خار نهم پهلو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون هلالی صفت روی نکو گویم و بس</p></div>
<div class="m2"><p>که بسی معتقدم این صفت نیکو را</p></div></div>