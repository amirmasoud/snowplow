---
title: >-
    غزل شمارهٔ ۳۵۵
---
# غزل شمارهٔ ۳۵۵

<div class="b" id="bn1"><div class="m1"><p>ماییم جا بگوشه می خانه ساخته</p></div>
<div class="m2"><p>خود را حریف ساغر و پیمانه ساخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کس که تاب داده بهم طره ترا</p></div>
<div class="m2"><p>زنجیر بهر عاشق دیوانه ساخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل نیست این که در تن افسرده منست</p></div>
<div class="m2"><p>دیوانه ایست جای بویرانه ساخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل خانه خداست، چه سازم که کافری</p></div>
<div class="m2"><p>آن خانه را گرفته و بت خانه ساخته؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شمع، پرتوی بهلالی فگن، که او</p></div>
<div class="m2"><p>خود را بسوز عشق تو پروانه ساخته</p></div></div>