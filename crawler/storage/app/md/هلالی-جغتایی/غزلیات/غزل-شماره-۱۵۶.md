---
title: >-
    غزل شمارهٔ ۱۵۶
---
# غزل شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>زان پیشتر که جانان ناگه ز در درآید</p></div>
<div class="m2"><p>از شادی وصالش، ترسم که: جان برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناصح بصبر ما را بسیار خواند، لیکن</p></div>
<div class="m2"><p>ما عاشقیم و از ما این کار کمتر آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای ترک شوخ، باری، در سر چه فتنه داری؟</p></div>
<div class="m2"><p>کز شوخی تو هردم صد فتنه بر سر آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز عکس خود، که بینی، ز آیینه گاه گاهی</p></div>
<div class="m2"><p>مثل تو دیگری کو، تا در برابر آید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی که: با تو یارم، آه! این دروغ گفتی</p></div>
<div class="m2"><p>ور زانکه راست باشد کی از تو باور آید؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برگرد شمع رویت پروانه شد هلالی</p></div>
<div class="m2"><p>یک بار، گر برانی، صد بار دیگر آید</p></div></div>