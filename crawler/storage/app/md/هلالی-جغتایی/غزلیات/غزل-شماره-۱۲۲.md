---
title: >-
    غزل شمارهٔ ۱۲۲
---
# غزل شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>تا سلسله زلف تو زنجیر جنون شد</p></div>
<div class="m2"><p>وابستگی این دل دیوانه فزون شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرمنده شد از عکس جمالت مه و خورشید</p></div>
<div class="m2"><p>وز عارض گل رنگ تو دل غنچه خون شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون شد دل من، دم بدم، از فرقت دلبر</p></div>
<div class="m2"><p>زان رو ز ره دیده خونبار برون شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنجا، که صبا را گذری نیست، که گوید:</p></div>
<div class="m2"><p>حال دل این خسته، بدلدار، که چون شد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند قدت، راست، هلالی، چو الف بود</p></div>
<div class="m2"><p>از بار غم دوست، بیک بار، چو نون شد</p></div></div>