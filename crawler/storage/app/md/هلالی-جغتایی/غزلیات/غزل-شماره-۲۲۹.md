---
title: >-
    غزل شمارهٔ ۲۲۹
---
# غزل شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>هر شب بسر کوی تو از پای در افتم</p></div>
<div class="m2"><p>وز شوق تو آهی زنم و بی خبر افتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بار غم اینست، که من میکشم از تو</p></div>
<div class="m2"><p>بالله! که اگر کوه شوم از کمر افتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهم بزنی تیر و بتیغم بنوازی</p></div>
<div class="m2"><p>تا در دم کشتن بتو نزدیکتر افتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من بعد بر آنم که ببوی سر زلفت</p></div>
<div class="m2"><p>برخیزم و دنبال نسیم سحر افتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شیخ، بمحراب مرا سجده مفرما</p></div>
<div class="m2"><p>بگذار، خدا را، که بر آن خاک در افتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گمراهی من بین که: درین مرحله هر روز</p></div>
<div class="m2"><p>از وادی مقصود بجای دگر افتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیلاب سرشک از مژه بگشای، هلالی</p></div>
<div class="m2"><p>مپسند که: آغشته بخون جگر افتم</p></div></div>