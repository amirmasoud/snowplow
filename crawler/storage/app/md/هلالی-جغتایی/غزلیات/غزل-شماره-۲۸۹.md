---
title: >-
    غزل شمارهٔ ۲۸۹
---
# غزل شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>یارب، غم بی‌رحمی جانان به که گویم؟</p></div>
<div class="m2"><p>جانم غم او سوخت، غم جان به که گویم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی یار و نه غمخوار و نه کس محرم اسرار</p></div>
<div class="m2"><p>رنجوری و مهجوری و حرمان به که گویم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آشفته شد از قصه من خاطر جمعی</p></div>
<div class="m2"><p>دیگر چه کنم؟ حال پریشان به که گویم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویند طبیبان که: بگو درد خود، اما</p></div>
<div class="m2"><p>دردی که گذشتست ز درمان به که گویم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دردی، که مرا ساخته رسوا، همه دانند</p></div>
<div class="m2"><p>داغی، که مرا سوخته پنهان، به که گویم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندوه تو ناگفته و درد تو نهان به</p></div>
<div class="m2"><p>این پیش که ظاهر کنم و آن به که گویم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلقی همه با هم سخن وصل تو گویند</p></div>
<div class="m2"><p>من بی‌کسم، افسانه هجران به که گویم؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دور طرب، افسوس! که بگذشت، هلالی</p></div>
<div class="m2"><p>دور دگر آمد، غم دوران به که گویم؟</p></div></div>