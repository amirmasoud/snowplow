---
title: >-
    غزل شمارهٔ ۳۶۸
---
# غزل شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>زین پیش لطف بود و کنون جور و کین همه</p></div>
<div class="m2"><p>اول چه بود آن همه؟ آخر چه این همه؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوبان، ز اهل درد شما را چه آگهی؟</p></div>
<div class="m2"><p>ایشان نیازمند و شما نازنین همه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمهای دوست، اندک و بسیار هر چه هست</p></div>
<div class="m2"><p>بادا نصیب این دل اندوهگین همه!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دیده، از غبار رهش توتیا مجوی</p></div>
<div class="m2"><p>کز گریه تو گل شده روی زمین همه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ناگهان به سوی هلالی قدم نهی</p></div>
<div class="m2"><p>سازد نثار مقدم تو عقل و دین همه</p></div></div>