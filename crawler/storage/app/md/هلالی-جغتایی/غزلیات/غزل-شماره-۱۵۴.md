---
title: >-
    غزل شمارهٔ ۱۵۴
---
# غزل شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>ماه من، زلف شب قدرست و رویت روز عید</p></div>
<div class="m2"><p>در سر ماهی شب و روزی باین خوبی که دید؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو من برخاست، از قدش قیامت شد پدید</p></div>
<div class="m2"><p>غیر آن قامت، که من دیدم، قیامت را که دید؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن زنخدان را، که پر کردند ز آب زندگی</p></div>
<div class="m2"><p>بر کفم نه، کز کمال نازکی خواهد چکید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون در آغوشت گرفتم قالب من جان گرفت</p></div>
<div class="m2"><p>غالبا جان آفرین جسم تو از جان آفرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون کف پایت نهادی بر دلم آرام یافت</p></div>
<div class="m2"><p>دست ازو گر باز داری، همچنان خواهد تپید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چونکه بگذشتی تو اشک من روان شد از پیت</p></div>
<div class="m2"><p>عزم پابوس تو دارد، هر کجا خواهد رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میکشم بار غم از هجران و این کوه بلاست</p></div>
<div class="m2"><p>من ندانم کین بلا را تا یکی خواهم کشید؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وه! چه پیش آمد، هلالی، کان غزال مشکبوی</p></div>
<div class="m2"><p>ناگهان از من رمید و با رقیبان آرمید؟</p></div></div>