---
title: >-
    غزل شمارهٔ ۳۰۹
---
# غزل شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>بخاک پای تو، ای سرو ناز پرور من</p></div>
<div class="m2"><p>که جز هوای وصال تو نیست در سر من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>براه عشق تو خاکم، طریق من اینست</p></div>
<div class="m2"><p>درین طریق نباشد کسی برابر من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم تو در دل تنگم نشست و منفعلم</p></div>
<div class="m2"><p>که نیست لایق تو کلبه محقر من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جلوه سمن و سرو دل نیاساید</p></div>
<div class="m2"><p>کجاست سرو سهی قامت سمن بر من؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز ترک مست من، ای زاهدان، کناره کنید</p></div>
<div class="m2"><p>که نیست هیچ مسلمان حریف کافر من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حذر کنید، رقیبان، ز سیل مژگانم</p></div>
<div class="m2"><p>که دردمندم و خون می چکد ز خنجر من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عتاب کرد و جفا نیز می کند، هیهات!</p></div>
<div class="m2"><p>هنوز تا چه کند طالع ستمگر من؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هلالی، از می عشرت مرا نصیبی نیست</p></div>
<div class="m2"><p>مگر به خون جگر پر کنند ساغر من</p></div></div>