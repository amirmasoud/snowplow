---
title: >-
    غزل شمارهٔ ۲۳۴
---
# غزل شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>ز پیر میکده عمری در التماس شدم</p></div>
<div class="m2"><p>که خاک درگه دیر فلک اساس شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم مرا بغم دیگران قیاس مکن</p></div>
<div class="m2"><p>که من نشانه غمهای بی قیاس شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا ز حسن تو صنع خدای ظاهر شد</p></div>
<div class="m2"><p>ترا شناختم، آنگه خداشناس شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپاس عید بود پاس نقل و باده و جام</p></div>
<div class="m2"><p>هزار شکر که مشغول این سپاس شدم!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پلاس فقر، هلالی، لباس فخر منست</p></div>
<div class="m2"><p>من از برای تفاخر درین لباس شدم</p></div></div>