---
title: >-
    غزل شمارهٔ ۳۱۵
---
# غزل شمارهٔ ۳۱۵

<div class="b" id="bn1"><div class="m1"><p>فدای آن سگ کو باد جان ناتوان من</p></div>
<div class="m2"><p>که بعد از مرگ در کوی تو آرد استخوان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو داری عزم رفتن، با تو نتوان درد دل گفتن</p></div>
<div class="m2"><p>که وقت رفتن جانست و میگیرد زبان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از بی مهری آن ماه مردم، کی بود، یارب؟</p></div>
<div class="m2"><p>که با من مهربان گردد مه نامهربان من؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبان یار شیرینست و کام من بصد تلخی</p></div>
<div class="m2"><p>زهی لذت! اگر باشد زبانش در دهان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گمان دارم که: با من اتفاقی هست آن مه را</p></div>
<div class="m2"><p>چه باشد، آه! اگر روزی یقین گردد گمان من؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تب هجران بنوبت میستاند جان مشتاقان</p></div>
<div class="m2"><p>گرین نوبت بجان من رسد، ای وای جان من!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هلالی، شعلهای برق آهم رفت بر گردون</p></div>
<div class="m2"><p>ملک را بر فلک دل سوخت از آه و فغان من</p></div></div>