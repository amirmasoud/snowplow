---
title: >-
    غزل شمارهٔ ۲۳۰
---
# غزل شمارهٔ ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>براهت بینم و از بیخودی بر رهگذر غلتم</p></div>
<div class="m2"><p>بهر جا پا نهی، از شوق پا بوست بسر غلتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر پهلو، که می افتم، بپهلوی سگت شبها</p></div>
<div class="m2"><p>نمیخواهم کز آن پهلو بپهلوی دگر غلتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان در وقت بسمل از تو میخواهم چنان زخمی</p></div>
<div class="m2"><p>که عمری نیم بسمل باشم و بر خاک در غلتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بامیدی که روزی بر سرم آید سگ کویت</p></div>
<div class="m2"><p>در آن کو هر شبی تا روز در خون جگر غلتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان زار و ضعیفم در هوای سرو بالایی</p></div>
<div class="m2"><p>که همچون خار و خاشاک از دم باد سحر غلتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمیخواهم که از بزم وصال او روم بیرون</p></div>
<div class="m2"><p>کرم کن، ساقیا، جامی که آنجا بی خبر غلتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هلالی، چون مرا در کوی آن مه ناتوان بینی</p></div>
<div class="m2"><p>بگیر از دستم و بگذار تا بار دگر غلتم</p></div></div>