---
title: >-
    غزل شمارهٔ ۳۲۷
---
# غزل شمارهٔ ۳۲۷

<div class="b" id="bn1"><div class="m1"><p>عاشقم کردی و گفتی با رقیب تندخو</p></div>
<div class="m2"><p>عاشق روی توام، با هر که می خواهی بگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان من، دلجویی اغیار کردن تا بکی؟</p></div>
<div class="m2"><p>گاه گاهی هم دل سرگشته ما را بجو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای طبیب، از بهر درد ما غم درمان مخور</p></div>
<div class="m2"><p>زانکه ما با درد بی درمان او کردیم خو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو مویی شد تنم، گو: از میان بردار عشق</p></div>
<div class="m2"><p>بعد ازین مویی نگنجد در میان ما و او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفت آن آب حیات از جویبار چشم من</p></div>
<div class="m2"><p>کی بود، یارب، که آب رفته باز آید بجو؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صورت دعوی گل، معنی ندارد با رخت</p></div>
<div class="m2"><p>چون ندارد صورت و معنی چه سود از رنگ و بو؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر کویش، هلالی، رخ بخون شستن چه سود؟</p></div>
<div class="m2"><p>سوی تیغ آبدارش بین و دست از جان بشو</p></div></div>