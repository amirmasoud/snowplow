---
title: >-
    غزل شمارهٔ ۲۵۲
---
# غزل شمارهٔ ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>مشکل که رود داغت هرگز ز دل چاکم</p></div>
<div class="m2"><p>تا لاله مگر روزی سر بر زند از خاکم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روز بخون ریزم آیی و رقیب از پی</p></div>
<div class="m2"><p>زان واقعه خوشحالم، زین واسطه غمناکم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای ترک شکار افگن، شمشیر مکش بر من</p></div>
<div class="m2"><p>یا آنکه پس از کشتن بر بند بفتراکم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این دیده که من دارم، آلوده بخون اولی</p></div>
<div class="m2"><p>زان رو که نمی دانی قدر نظر پاکم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چند هلالی را در آتش غم سوزی؟</p></div>
<div class="m2"><p>من آدمیم، یا رب، یا خود خس و خاشاکم؟</p></div></div>