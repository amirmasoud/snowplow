---
title: >-
    غزل شمارهٔ ۲۶۰
---
# غزل شمارهٔ ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>گر جفایی رفت، از جانان جدایی چون کنم؟</p></div>
<div class="m2"><p>من سگ آن آستانم، بی وفایی چون کنم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد عمری آشنا گشتی بصد خون جگر</p></div>
<div class="m2"><p>باز اگر بیگانه گردی، آشنایی چون کنم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفتی و در محنت جان کندنم انداختی</p></div>
<div class="m2"><p>گر بیایی زنده مانم، ور نیایی چون کنم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهدا، از نقل و می بیهوده منعم میکنی</p></div>
<div class="m2"><p>من که رندی کرده باشم، پارسایی چون کنم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفته ای: تا کی هلالی زار نالد همچو عود؟</p></div>
<div class="m2"><p>چون گرفتارم بچنگ بی نوایی چون کنم؟</p></div></div>