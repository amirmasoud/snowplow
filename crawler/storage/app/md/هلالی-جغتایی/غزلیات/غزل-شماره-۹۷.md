---
title: >-
    غزل شمارهٔ ۹۷
---
# غزل شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>سایه ای کز قد و بالای تو بر ما افتد</p></div>
<div class="m2"><p>به ز نوریست که از عالم بالا افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا دیده بر آن قامت رعنا افتد</p></div>
<div class="m2"><p>رود از دست دل زار و همه جا افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که در کوی تو روزی بهوس پای نهاد</p></div>
<div class="m2"><p>عاقبت هم بسر کوی تو از پا افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه افتد سر ما در گذر او همه روز</p></div>
<div class="m2"><p>کاش! روی گذر او بسر ما افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افتد از گریه تن زار هلالی هر سو</p></div>
<div class="m2"><p>همچو خاشاک ضعیفی که بدریا افتد</p></div></div>