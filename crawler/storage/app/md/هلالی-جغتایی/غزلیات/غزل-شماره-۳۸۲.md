---
title: >-
    غزل شمارهٔ ۳۸۲
---
# غزل شمارهٔ ۳۸۲

<div class="b" id="bn1"><div class="m1"><p>ای ز بهار تازه تر، تازه بهار کیستی؟</p></div>
<div class="m2"><p>وه! چه نگار طرفه ای! طرفه نگار کیستی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست رخ تو ماه نو، کوکبه تو شاه حسن</p></div>
<div class="m2"><p>ماه کدام کشوری؟ شاه دیار کیستی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاله و سرو این چمن منفعلند پیش تو</p></div>
<div class="m2"><p>سرو کدام گلشنی؟ لاله عذار کیستی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خسته رنج فرقتم، کشته درد حیرتم</p></div>
<div class="m2"><p>من بمیان محنتم، تو بکنار کیستی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چیست، هلالی، این همه محنت و درد عاشقی؟</p></div>
<div class="m2"><p>حال تو زار شد، بگو: عاشق زار کیستی؟</p></div></div>