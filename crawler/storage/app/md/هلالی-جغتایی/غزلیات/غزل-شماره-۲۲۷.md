---
title: >-
    غزل شمارهٔ ۲۲۷
---
# غزل شمارهٔ ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>ز سوز سینه کبابم، ز سیل دیده خرابم</p></div>
<div class="m2"><p>تو شمع بزم کسانی و من در آتش و آبم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا عقوبت هجر تو بهتر از همه شادیست</p></div>
<div class="m2"><p>تو راحت دگران شو، که من برای عذابم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدیگران منشین و بجان من مزن آتش</p></div>
<div class="m2"><p>مرا مسوز، که من خود بر آتش تو کبابم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر برای هلاک منست ناز و عتابت</p></div>
<div class="m2"><p>بیا و قتل کن ایدون، که مستحق عتابم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سؤال بوسه نمودم، ولی تو لب نگشودی</p></div>
<div class="m2"><p>سخن بعرض رسید و در انتظار جوابم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگرد روی تو پروانه ام، که شمع مرادی</p></div>
<div class="m2"><p>اگر تو روی بتابی، من از تو روی نتابم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بقدر خاک ره از من کسی حساب نگیرد</p></div>
<div class="m2"><p>بکوی دوست، هلالی، ببین که: در چه حسابم؟</p></div></div>