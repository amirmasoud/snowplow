---
title: >-
    غزل شمارهٔ ۱۰۱
---
# غزل شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>آب حیات حسنت گل برگ تر ندارد</p></div>
<div class="m2"><p>طعم دهان تنگت تنگ شکر ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دیده، تیز منگر در روی نازک او</p></div>
<div class="m2"><p>کز غایت لطافت تاب نظر ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هر گذر که باشی، نتوان گذشتن از تو</p></div>
<div class="m2"><p>آری، چو جانی و کس از جان گذر ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سگ را بخون آهو رخصت مده، که مسکین</p></div>
<div class="m2"><p>از رشک چشم مستت خون در جگر ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عشق تو هلالی از ترک سر بسر شد</p></div>
<div class="m2"><p>دیوانه است و عاشق، پروای سر ندارد</p></div></div>