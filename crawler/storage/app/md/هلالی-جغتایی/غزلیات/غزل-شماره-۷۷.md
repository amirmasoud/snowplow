---
title: >-
    غزل شمارهٔ ۷۷
---
# غزل شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>دگرم بسته آن زلف سیه نتوان داشت</p></div>
<div class="m2"><p>آن چنانم که بزنجیر نگه نتوان داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاب خیل و سپه زلف و رخی نیست مرا</p></div>
<div class="m2"><p>روز و شب معرکه با خیل و سپه نتوان داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کی آن چاه ذقن را نگرم با لب خشک؟</p></div>
<div class="m2"><p>این همه تشنه مرا بر لب چه نتوان داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده بر بستم و نومید نشستم، چه کنم؟</p></div>
<div class="m2"><p>بیش ازین دیده بامید بره نتوان داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با وجود رخ او دیدن گل کی زیباست؟</p></div>
<div class="m2"><p>پیش خورشید نظر جانب مه نتوان داشت</p></div></div>