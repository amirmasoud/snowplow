---
title: >-
    غزل شمارهٔ ۲۰۹
---
# غزل شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>مردم و خود را ز غمهای جهان کردم خلاص</p></div>
<div class="m2"><p>عالمی را هم ز فریاد و فغان کردم خلاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در غم عشق جوانی می شنیدم پند پیر</p></div>
<div class="m2"><p>خویشتن را از غم پیر و جوان کردم خلاص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش زمانی دست داد از عالم مستی مرا</p></div>
<div class="m2"><p>کز دو عالم خویش را در یک زمان کردم خلاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر بازار رمزی گفتم از سودای عشق</p></div>
<div class="m2"><p>مردمان را از غم سود و زیان کردم خلاص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش: آخر هلالی را ز هجران سوختی</p></div>
<div class="m2"><p>گفت: او را از بلای جاودان کردم خلاص</p></div></div>