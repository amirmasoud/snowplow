---
title: >-
    غزل شمارهٔ ۳۳۲
---
# غزل شمارهٔ ۳۳۲

<div class="b" id="bn1"><div class="m1"><p>خاکم بره پیک حریم حرم او</p></div>
<div class="m2"><p>باشد که بجایی برسم در قدم او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر داغ دلم مرهم راحت مگذارید</p></div>
<div class="m2"><p>تا کم نشود راحت درد و الم او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین گونه که بر من ستم دوست خوش آید</p></div>
<div class="m2"><p>خوش نیست که بر غیر من آید ستم او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می سوزم و این آه جگر سوز دلیلست</p></div>
<div class="m2"><p>کز جان و دلم دود برآورد غم او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داریم امید کرم از یار، ولیکن</p></div>
<div class="m2"><p>دیدیم ستمها و امید کرم او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تیغ تو صد کشته شود زنده بیک دم</p></div>
<div class="m2"><p>گویا دم جان پرور عیسیست دم او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم که: هلالی ز غمت سوی عدم رفت</p></div>
<div class="m2"><p>گفتا: چه تفاوت ز وجود و عدم او؟</p></div></div>