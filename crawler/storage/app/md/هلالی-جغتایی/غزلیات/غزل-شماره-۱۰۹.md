---
title: >-
    غزل شمارهٔ ۱۰۹
---
# غزل شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>باغ عیش من بجای گل همه خار آورد</p></div>
<div class="m2"><p>آری، این نخلی که من دارم، همین بار آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوه از سیل سرشکم در صدا آید، بلی</p></div>
<div class="m2"><p>گریه من سنگ را در ناله زار آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالمی در گریه است از ناله جانسوز من</p></div>
<div class="m2"><p>نوحه ای کز درد خیزد گریه بسیار آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دل آزرده را جز داغ او مرهم نهم</p></div>
<div class="m2"><p>بر دل آن مرهم شود داغی که آزار آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که ابروی تو دید و مایل محراب شد</p></div>
<div class="m2"><p>زود باشد کز خجالت رو بدیوار آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ز خورشید جمالت گرم شد بازار حسن</p></div>
<div class="m2"><p>هر دم این دیوانه را سودا ببازار آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پای بر فرق هلالی نه، که بهر مقدمت</p></div>
<div class="m2"><p>هر زمان صد گوهر از چشم گهر بار آورد</p></div></div>