---
title: >-
    غزل شمارهٔ ۳۸۹
---
# غزل شمارهٔ ۳۸۹

<div class="b" id="bn1"><div class="m1"><p>چند پرسم خبر وصل و نیابم اثری؟</p></div>
<div class="m2"><p>مگر این بخت بخوابست و ندارد خبری؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند از دیده برویت نگرم پیش رقیب؟</p></div>
<div class="m2"><p>گوشه ای خواهم و از روی فراغت نظری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگران مانع انسند، خوش آن خلوت وصل</p></div>
<div class="m2"><p>که همین ما و تو باشیم و نباشد دگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میوه عیش نخوردیم ز نخل قد تو</p></div>
<div class="m2"><p>این چه عمریست که از عمر نخوردیم بری؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سحر از زلف تو بویی بمن آورد نسیم</p></div>
<div class="m2"><p>چه فرح بخش نسیمی، چه مبارک سحری!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوه پرسیم شد از ابر، بیا، تا بکشیم</p></div>
<div class="m2"><p>ساغر لعل ز سر پنجه زرین کمری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تلخ شد کام هلالی، بتمنای لبت</p></div>
<div class="m2"><p>تا بکی زهر توان خورد بیاد شکری؟</p></div></div>