---
title: >-
    غزل شمارهٔ ۳۶۶
---
# غزل شمارهٔ ۳۶۶

<div class="b" id="bn1"><div class="m1"><p>گر نیست جام گلگون، خوش نیست دور لاله</p></div>
<div class="m2"><p>بی می چه نشأئه خیزد؟ از دیدن پیاله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من نوح روزگارم، از گریه غرق توفان</p></div>
<div class="m2"><p>کو همدمی که گویم درد هزار ساله؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کی بناز و شوخی لب را گزی بدندان؟</p></div>
<div class="m2"><p>گل برگ نازکت را آزرده ساخت ژاله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قتل رقیب خود را با من حواله کردی</p></div>
<div class="m2"><p>از دست من چه آید؟ هم با خدا حواله!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر صفحه دل من ذکر می است و شاهد</p></div>
<div class="m2"><p>عقد محبت آمد مضمون این پیاله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمدیده ای، که خواند شرح غم هلالی</p></div>
<div class="m2"><p>از خون دیده خود رنگین کند رساله</p></div></div>