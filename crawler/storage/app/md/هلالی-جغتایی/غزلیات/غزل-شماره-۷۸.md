---
title: >-
    غزل شمارهٔ ۷۸
---
# غزل شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>در مجلس اگر او نظری با دگری داشت</p></div>
<div class="m2"><p>دانند حریفان که در آن هم نظری داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر لاله، که با داغ دل از خاک برآمد</p></div>
<div class="m2"><p>دیدم که: ز سودای تو پر خون جگری داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امروز سر زلف تو آشفته چرا بود؟</p></div>
<div class="m2"><p>گویا ز پریشانی دلها خبری داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریاد! که رفت از سرم آن سرو، که عمری</p></div>
<div class="m2"><p>من خاک رهش بودم و بر من گذری داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با جام و قدح عزم چمن کرد، چو نرگس</p></div>
<div class="m2"><p>هر کس که درین روز بکف سیم و زری داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین مرحله آهنگ عدم کرد هلالی</p></div>
<div class="m2"><p>مانند غریبی، که هوای سفری داشت</p></div></div>