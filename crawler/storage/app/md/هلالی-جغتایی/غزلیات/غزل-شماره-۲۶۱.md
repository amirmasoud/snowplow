---
title: >-
    غزل شمارهٔ ۲۶۱
---
# غزل شمارهٔ ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>جان من، جان و دل خویش نثار تو کنم</p></div>
<div class="m2"><p>بود و نابود همه در سر کار تو کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دگر دور نیفتد ز رخت مردم چشم</p></div>
<div class="m2"><p>خواهمش بر کنم و خال عذار تو کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو سگ با تو سراسیمه ام، ای طرفه غزال</p></div>
<div class="m2"><p>می روم در هوس آنکه: شکار تو کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای گل تازه، که دیر آمده ای پیش نظر،</p></div>
<div class="m2"><p>زود مگذر، که تماشای بهار تو کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماه من، سوی هلالی بگذر از سر مهر</p></div>
<div class="m2"><p>سرمه دیده گریان ز غبار تو کنم</p></div></div>