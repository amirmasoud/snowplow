---
title: >-
    غزل شمارهٔ ۱۴
---
# غزل شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>یار، چون در جام می بیند، رخ گل فام را</p></div>
<div class="m2"><p>عکس رویش چشمه خورشید سازد جام را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام می بر دست من نه، نام نیک از من مجوی</p></div>
<div class="m2"><p>نیک نامی خود چه کار آید من بد نام را؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقیا، جام و قدح را صبح و شام از کف منه</p></div>
<div class="m2"><p>کین چنین خورشید و ماهی نیست صبح و شام را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فتنه انگیزست دوران، جام می در گردش آر</p></div>
<div class="m2"><p>تا نبینم فتنهای گردش ایام را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خدا خواهد هلالی دم بدم جام نشاط</p></div>
<div class="m2"><p>کو حریفی، تا بساقی گوید این پیغام را؟</p></div></div>