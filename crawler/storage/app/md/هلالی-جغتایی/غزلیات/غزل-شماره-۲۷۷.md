---
title: >-
    غزل شمارهٔ ۲۷۷
---
# غزل شمارهٔ ۲۷۷

<div class="b" id="bn1"><div class="m1"><p>نقد جان را در بهای زلف جانان می دهم</p></div>
<div class="m2"><p>عاشقم و ز بهر سودای چنین جان می دهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که از حال من آشفته می پرسی، مپرس</p></div>
<div class="m2"><p>کز پریشانی خبرهای پریشان می دهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش آن لب زار می میرم، زهی حسرت! که من</p></div>
<div class="m2"><p>تشنه لب جان بر کنار آب حیران می دهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این چنین کز چشم من هر گوشه می بارد سرشک</p></div>
<div class="m2"><p>عاقبت از گریه مردم را بتوفان می دهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دور ازو، هجران، اگر قصد هلاک من کند</p></div>
<div class="m2"><p>عمر خود می بخشم و جان را بهجران می دهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که روزی دل بخوبان داد، آخر جان دهد</p></div>
<div class="m2"><p>وای جان من! که آخر دل بایشان می دهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در غم هجران، هلالی، از فغان منعم مکن</p></div>
<div class="m2"><p>زانکه من تسکین درد خود بافغان می دهم</p></div></div>