---
title: >-
    غزل شمارهٔ ۴۴
---
# غزل شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>چیست پیراهن آن دلبر شیرین حرکات؟</p></div>
<div class="m2"><p>همچو سرچشمه خضرست و بدن آب حیات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چه قدست و چه رفتار و چه شیرین حرکات؟</p></div>
<div class="m2"><p>گوییا موج زنان می گذرد آب حیات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بیاد لب او زهر دهندم که: بنوش</p></div>
<div class="m2"><p>تلخی زهر ز هر در دهدم ذوق نبات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این چه ماهیست که در کلبه تاریک منست؟</p></div>
<div class="m2"><p>آب حیوان نتوان یافت چنین در ظلمات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسکه از ناله دلم دوش قیامت می کرد</p></div>
<div class="m2"><p>عرصه کوی ترا ساخت زمین عرصات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند گویی ز سر ناز که: جان ده بوفا؟</p></div>
<div class="m2"><p>جان من، کار دگر نیست مرا غیر وفات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رحم بر عاشق درویش ندارند بتان</p></div>
<div class="m2"><p>وه! که در مذهب این سنگدلان نیست زکات!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماند بیچاره هلالی بکمند تو اسیر</p></div>
<div class="m2"><p>این محالست که او را بود امکان نجات</p></div></div>