---
title: >-
    غزل شمارهٔ ۱۵۲
---
# غزل شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>لعل جان‌بخشت، که یاد از آب حیوان می‌دهد</p></div>
<div class="m2"><p>زنده را جان می‌ستاند، مرده را جان می‌دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دور بادا چشم بد، کامروز در میدان حسن</p></div>
<div class="m2"><p>شهسوار من سمند ناز جولان می‌دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یارب! اندر ساغر دوران شراب وصل نیست</p></div>
<div class="m2"><p>یا به دور ما همه خوناب هجران می‌دهد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل مگر پا بسته زلف تو شد کز حال او</p></div>
<div class="m2"><p>باد می‌آید، خبرهای پریشان می‌دهد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست درد عشق خوبان را به درمان احتیاج</p></div>
<div class="m2"><p>گر طبیب این درد بیند ترک درمان می‌دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موجب این گریه‌های تلخ می‌دانی که چیست؟</p></div>
<div class="m2"><p>عشوه شیرین که آن لب‌های خندان می‌دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای اجل، سوی هلالی بهر جان بردن میا</p></div>
<div class="m2"><p>زان که عاشق گاه مردن جان به جانان می‌دهد</p></div></div>