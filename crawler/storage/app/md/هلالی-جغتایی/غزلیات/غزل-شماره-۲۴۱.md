---
title: >-
    غزل شمارهٔ ۲۴۱
---
# غزل شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>خواهم که: بزیر قدمت زار بمیرم</p></div>
<div class="m2"><p>هر چند کنی زنده، دگر بار بمیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانم که: چرا خون مرا زود نریزی</p></div>
<div class="m2"><p>خواهی که بجان کندن بسیار بمیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من طاقت نادیدن روی تو ندارم</p></div>
<div class="m2"><p>مپسند که در حسرت دیدار بمیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید حیاتم بلب بام رسیدست</p></div>
<div class="m2"><p>آن به که در آن سایه دیوار بمیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی که: ز رشک تو هلا کند رقیبان</p></div>
<div class="m2"><p>من نیز برآنم که ازین عار بمیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون یار بسر وقت من افتاد، هلالی</p></div>
<div class="m2"><p>وقتست اگر در قدم یار بمیرم</p></div></div>