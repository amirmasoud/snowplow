---
title: >-
    غزل شمارهٔ ۱۱۱
---
# غزل شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>یار، هر چند که رعنا و سهی قد باشد</p></div>
<div class="m2"><p>گر بعشاق نکویی بکند بد باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقصد اهل نظر خاک در تست، بلی</p></div>
<div class="m2"><p>چون تو مقصود شوی کوی تو مقصد باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه در حسن بود یکصد خوبان جهان</p></div>
<div class="m2"><p>حسن خلقی اگرش هست یکی صد باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الف قد تو پیش همه مقبول افتاد</p></div>
<div class="m2"><p>این نه حرفیست که بروی قلم رد باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موی ژولیده من بین و وفا کن، ور نه</p></div>
<div class="m2"><p>سبزه بینی که مرا بر سر مرقد باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش: دل بخم زلف تو در قید بماند</p></div>
<div class="m2"><p>گفت: دیوانه همان به که مقید باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حد کس نیست، هلالی، که شود همره ما</p></div>
<div class="m2"><p>زانکه این مرحله را محنت بی حد باشد</p></div></div>