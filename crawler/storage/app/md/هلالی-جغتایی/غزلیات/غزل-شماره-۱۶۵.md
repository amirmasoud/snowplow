---
title: >-
    غزل شمارهٔ ۱۶۵
---
# غزل شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>هردم از چشم تو دل را نظری می‌باید</p></div>
<div class="m2"><p>صد نظر دید و هنوزش دگری می‌باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن قدر سرکشی و ناز، که باید، داری</p></div>
<div class="m2"><p>شیوه مهر و وفا هم قدری می‌باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه در عالم خوبی‌ست از آن خوب‌تری</p></div>
<div class="m2"><p>نتوان گفت کزان خوب‌تری می‌باید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به امید نظری در گذرت خاک شدیم</p></div>
<div class="m2"><p>از تو بر ما نظری و گذری می‌باید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی: از وصل خبر یافته‌ای، خوش دل باش</p></div>
<div class="m2"><p>خبری هست ولیکن اثری می‌باید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به قدم طی نشود راه بیابان فراق</p></div>
<div class="m2"><p>قطع این مرحله را بال و پری می‌باید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ره عشق، هلالی، خبر از خویش مپرس</p></div>
<div class="m2"><p>که درین راه ز خود بی‌خبری می‌باید</p></div></div>