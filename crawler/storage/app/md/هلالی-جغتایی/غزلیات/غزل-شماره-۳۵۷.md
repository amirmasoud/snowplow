---
title: >-
    غزل شمارهٔ ۳۵۷
---
# غزل شمارهٔ ۳۵۷

<div class="b" id="bn1"><div class="m1"><p>جان من، گاهی سخن کن ز آن لب و کامی بده</p></div>
<div class="m2"><p>ور سخن با عاشقان حیفست، دشنامی بده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دل از دست تو بی آرام شد، بهر خدا</p></div>
<div class="m2"><p>بر دلم دستی نه و یک لحظه آرامی بده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میکنم پیش تو عرض حال بی سامان دل</p></div>
<div class="m2"><p>گر توانی قصه او را سرانجامی بده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقیا، از آتش دل شعله در جانم فتاد</p></div>
<div class="m2"><p>تا زنم آبی بر آتش، لطف کن، جامی بده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ترا فارغ شود خاطر ز سختی های دهر</p></div>
<div class="m2"><p>چند روزی دل بدست نازک اندامی بده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان من در حسرت آن ساعد سیمین بسوخت</p></div>
<div class="m2"><p>چند سوزی بیدلان را؟ وعده کامی بده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناصحا، پند تو از طعن هلالی تا بکی؟</p></div>
<div class="m2"><p>ای نکو نام دو عالم، ترک بدنامی بده</p></div></div>