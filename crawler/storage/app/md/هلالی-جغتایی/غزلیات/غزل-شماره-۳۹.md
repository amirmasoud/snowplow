---
title: >-
    غزل شمارهٔ ۳۹
---
# غزل شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>سر نمی تابم ز شمشیر حبیب</p></div>
<div class="m2"><p>هر چه آید بر سر من، یا نصیب!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بدرد آمد من بیچاره را</p></div>
<div class="m2"><p>چاره درد دلم کن، ای طبیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای که گویی: چونی و حال تو چیست؟</p></div>
<div class="m2"><p>من غریب و حال من باشد غریب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا رقیبت هست ما را قدر نیست</p></div>
<div class="m2"><p>نیست گردد، یارب! از پیشت رقیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زار می نالد هلالی، بی رخت</p></div>
<div class="m2"><p>آن چنان کز حسرت گل عندلیب</p></div></div>