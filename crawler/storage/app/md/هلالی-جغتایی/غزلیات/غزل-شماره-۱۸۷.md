---
title: >-
    غزل شمارهٔ ۱۸۷
---
# غزل شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>با رخ زرد آمدم سوی درت، ای سروناز</p></div>
<div class="m2"><p>یعنی آوردم بخاک درگهت روی نیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دولت حسن و جوانی یک دو روزی بیش نیست</p></div>
<div class="m2"><p>در نیاز ما نگر، چندین بحسن خود مناز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر بگذشت و شب تاریک هجر آخر نشد</p></div>
<div class="m2"><p>یا شبم کوتاه می بایست، یا عمرم دراز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تاب بیماری ندارم بیش ازینها، ای فلک</p></div>
<div class="m2"><p>یا نسیم روح پرور، یا سموم جان گداز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردم چشم هلالی پاک می بازد نظر</p></div>
<div class="m2"><p>رو متاب، ای نازنین، از مردمان پاکباز</p></div></div>