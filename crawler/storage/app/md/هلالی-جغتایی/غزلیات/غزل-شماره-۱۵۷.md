---
title: >-
    غزل شمارهٔ ۱۵۷
---
# غزل شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>اگر نه از گل نو رسته بوی یار آید</p></div>
<div class="m2"><p>هوای باغ و تماشای گل چه کار آید؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار میرسد، آهنگ باغ کن، زان پیش</p></div>
<div class="m2"><p>که رفته باشی و بار دگر بهار آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز باده سرخوشی خود، زمان زمان، نو کن</p></div>
<div class="m2"><p>چنان مکن که: رود مستی و خمار آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فتاد کشتی عمرم بموج خیر فراق</p></div>
<div class="m2"><p>امید نیست کزین ورطه بر کنار آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار عاشق دلخسته خاک راه تو باد</p></div>
<div class="m2"><p>ولی مباد که بر دامنت غبار آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جدا ز لعل تو هر قطره ای ز آب حیات</p></div>
<div class="m2"><p>مرا بدیده چو پیکان آبدار آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بار نیست برین آستان هلالی را</p></div>
<div class="m2"><p>ازین چه سود که روزی هزار بار آید؟</p></div></div>