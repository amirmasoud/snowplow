---
title: >-
    غزل شمارهٔ ۳۱۳
---
# غزل شمارهٔ ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>تا بکی تند شوی بهر جفای دل من؟</p></div>
<div class="m2"><p>چند روزی بوفا کوش برای دل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تو میداشتی این آتش پنهان، که مراست</p></div>
<div class="m2"><p>دل بی رحم تو میسوخت، چه جای دل من؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاش لله! که دلم ترک تو گوید بجفا</p></div>
<div class="m2"><p>کز جفاهای تو بیشست وفای دل من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان دو گیسوی دلاویز چه امکان گریز؟</p></div>
<div class="m2"><p>که دو زنجیر نهادند بپای دل من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر طبیبی که خبر داشت ز بیماری عشق</p></div>
<div class="m2"><p>غیر وصل تو نفرمود دوای دل من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل گرفتار بلاییست، هلالی، که مپرس</p></div>
<div class="m2"><p>کس گرفتار مبادا ببلای دل من!</p></div></div>