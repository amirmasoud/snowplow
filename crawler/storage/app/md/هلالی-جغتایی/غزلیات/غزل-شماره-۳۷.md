---
title: >-
    غزل شمارهٔ ۳۷
---
# غزل شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>گل رویت عرق کرد از می ناب</p></div>
<div class="m2"><p>ز شبنم تازه شد گل برگ سیراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بناز آن چشم را از خواب مگشای</p></div>
<div class="m2"><p>همان بهتر که باشد فتنه در خواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تعالی الله! چه حسنست اینکه هر روز</p></div>
<div class="m2"><p>دهد سر پنجه خورشید را تاب؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز پا افتادم، آخر دست من گیر</p></div>
<div class="m2"><p>همین گویم: مرا دریاب، دریاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو در سر میل ابروی تو دارم</p></div>
<div class="m2"><p>سر ما کی فرودآید بمحراب؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهاران از در می خانه مگذر</p></div>
<div class="m2"><p>عجب فصلیست، جهد کرده دریاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هلالی، می بروی ماهرویان</p></div>
<div class="m2"><p>خوش آید، خاصه در شبهای مهتاب</p></div></div>