---
title: >-
    غزل شمارهٔ ۳۲
---
# غزل شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>نمی توان بجفا قطع دوستداری ما</p></div>
<div class="m2"><p>که از جفای تو بیشست با تو یاری ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسی چو ابر بهاران گریستیم و هنوز</p></div>
<div class="m2"><p>گلی نرست ز باغ امیدواری ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بچشم چون تو عزیزی شدیم خوار ولی</p></div>
<div class="m2"><p>ز عزت دگران بهترست خواری ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غبار کوی تو ما را ز چهره دور مباد</p></div>
<div class="m2"><p>که با تو می کند اظهار خاکساری ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز حال زار هلالی شبی که یاد کنم</p></div>
<div class="m2"><p>فلک بناله درآید ز آه و زاری ما</p></div></div>