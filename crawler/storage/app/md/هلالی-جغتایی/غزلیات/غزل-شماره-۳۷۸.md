---
title: >-
    غزل شمارهٔ ۳۷۸
---
# غزل شمارهٔ ۳۷۸

<div class="b" id="bn1"><div class="m1"><p>مست با رخسار آتشناک بیرون تاختی</p></div>
<div class="m2"><p>جلوه ای کردی و آتش در جهان انداختی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نمی پرداختی آخر بفکر کار ما</p></div>
<div class="m2"><p>کاشکی! اول بحال ما نمی پرداختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی نوا گشتم بکویت چون گدایان سالها</p></div>
<div class="m2"><p>وه! که یک بارم بسنگی چون سگان ننواختی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دل درویش، با خوبان نظر بازی مکن</p></div>
<div class="m2"><p>کندرین بازیچه نقد دین و دل پرداختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که کردی ناله، ای دل، بر سر بازار و کوی</p></div>
<div class="m2"><p>هم مرا، هم خویش را، رسوای عالم ساختی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر خونریز هلالی تیغ خود کردی علم</p></div>
<div class="m2"><p>در فن عاشق کشی آخر علم افراختی</p></div></div>