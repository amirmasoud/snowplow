---
title: >-
    غزل شمارهٔ ۲۶۶
---
# غزل شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>دلم ز دست شد، از دست دل چه چاره کنم؟</p></div>
<div class="m2"><p>اگر بدست من افتد، هزار پاره کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشست بزم تو، لیکن کجاست طاقت آن</p></div>
<div class="m2"><p>که در میان رقیبان ترا نظاره کنم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگو: کناره کن از من، که جان ز کف ندهی</p></div>
<div class="m2"><p>تو در میانه جانی، چه سان کناره کنم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه سنگدلی، از من این مناسب نیست</p></div>
<div class="m2"><p>که نسبت دل سختت بسنگ خاره کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هلالی، از رخ جانان بماه نتوان دید</p></div>
<div class="m2"><p>ز آفتاب چرا روی در ستاره کنم؟</p></div></div>