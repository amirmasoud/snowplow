---
title: >-
    غزل شمارهٔ ۳۴۴
---
# غزل شمارهٔ ۳۴۴

<div class="b" id="bn1"><div class="m1"><p>سازم قدم ز دیده و آیم بسوی تو</p></div>
<div class="m2"><p>تا هر قدم بدیده کشم خاک کوی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی تو خوب و خوی تو بد، آه! چون کنم؟</p></div>
<div class="m2"><p>ای کاش! همچو روی تو می بود خوی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منما جمال خویش بهر کج نظر، که نیست</p></div>
<div class="m2"><p>چشم بدان مناسب روی نکوی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان و دل آرزوی وصال تو کرده اند</p></div>
<div class="m2"><p>من نیز کرده با دل و جان آرزوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون من هلاک روی توام، رخ ز من متاب</p></div>
<div class="m2"><p>بگذار تا: هلاک شوم پیش روی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دل، ز دیده گریه شادی طمع مدار</p></div>
<div class="m2"><p>کین آب رفته باز نیاید بجوی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی، مران ز مجلس خویشم، که خو گرفت</p></div>
<div class="m2"><p>دستم بجام باده و چشمم بروی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتی: کنم هلالی دیوانه را علاج</p></div>
<div class="m2"><p>ای من غلام سلسله مشک بوی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از لطف گفته ای که: هلالی غلام ماست</p></div>
<div class="m2"><p>ای من غلام لطف چنین گفتگوی تو</p></div></div>