---
title: >-
    غزل شمارهٔ ۳۴۳
---
# غزل شمارهٔ ۳۴۳

<div class="b" id="bn1"><div class="m1"><p>مردم ازین الم: که نمردم برای تو</p></div>
<div class="m2"><p>ای خاک بر سرم، که نشد خاک پای تو!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر اختیار مرگ بدستم دهد قضا</p></div>
<div class="m2"><p>روزی هزار بار بمیرم برای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم نیست گر ز مهر تو دل پاره پاره شد</p></div>
<div class="m2"><p>ای کاش! ذره ذره شود در هوای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویم دعا و عمر ابد خواهم از خدا</p></div>
<div class="m2"><p>تا عمر خویش صرف کنم در دعای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آرزوی آنکه: بمن آشنا شوی</p></div>
<div class="m2"><p>آمیختم بهر که بود آشنای تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جای تو در حریم وصالست، ای رقیب</p></div>
<div class="m2"><p>ای کاش! بودمی، من بیدل، بجای تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پادشاهی همه آفاق خوشترست</p></div>
<div class="m2"><p>این سلطنت که: گشت هلالی گدای تو</p></div></div>