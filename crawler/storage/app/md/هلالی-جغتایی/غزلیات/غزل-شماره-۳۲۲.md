---
title: >-
    غزل شمارهٔ ۳۲۲
---
# غزل شمارهٔ ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>مردم از درد و نگفتی: دردمند ماست این</p></div>
<div class="m2"><p>دردمندان را نمی پرسی، چه استغناست این؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سایه بالای آن سرو از سر من کم مباد!</p></div>
<div class="m2"><p>زانکه بر من رحمتی از عالم بالاست این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواستم کان سرو روزی در کنار آید، ولی</p></div>
<div class="m2"><p>با کجی های فلک هرگز نیاید راست این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جای دل در سینه بود و جای تیرت در دلم</p></div>
<div class="m2"><p>آن ز جا رفتست؟ اما هم چنان برجاست این</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشک گلگون مرا بر چهره هر کس دید گفت:</p></div>
<div class="m2"><p>کز غم گل چهره ای آشفته و شیداست این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش: فرداست با من وعده وصل تو، گفت:</p></div>
<div class="m2"><p>دل بفردای قیامت نه، که آن فرداست این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر کویش، هلالی، درد عشق خویش را</p></div>
<div class="m2"><p>بیش ازین پنهان مکن، کز چهره ات پیداست این</p></div></div>