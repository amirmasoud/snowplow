---
title: >-
    غزل شمارهٔ ۲۸۶
---
# غزل شمارهٔ ۲۸۶

<div class="b" id="bn1"><div class="m1"><p>نوبهارست، بیا، تا قدحی نوش کنیم</p></div>
<div class="m2"><p>باشد این محنت ایام فراموش کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقیا، هوش و خرد تفرقه خاطر ماست</p></div>
<div class="m2"><p>باده پیش آر، که ترک خرد و هوش کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حد ما نیست که پیش تو بگوییم سخن</p></div>
<div class="m2"><p>هم تو با ما سخنی گوی، که ما گوش کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بارها غم بتو گفتیم، ز ما نشنیدی</p></div>
<div class="m2"><p>بعد ازین مصلحت آنست که خاموش کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ ناگفته بجانیم ز نیش ستمت</p></div>
<div class="m2"><p>وای! اگر زان لب شیرین طمع نوش کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما که باشیم، که ما را دهد آغوش تو دست؟</p></div>
<div class="m2"><p>با خیال تو مگر دست در آغوش کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار چون ساقی بزمست، هلالی، برخیز</p></div>
<div class="m2"><p>تا بیک جرعه ترا واله و مدهوش کنیم</p></div></div>