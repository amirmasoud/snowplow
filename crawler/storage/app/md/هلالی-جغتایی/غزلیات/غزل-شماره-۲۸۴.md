---
title: >-
    غزل شمارهٔ ۲۸۴
---
# غزل شمارهٔ ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>خیز، تا امروز با هم ساغر صهبا کشیم</p></div>
<div class="m2"><p>خویش را دامن کشان تا دامن صحرا کشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باغ و بستان دلکشست و کوه و صحرا هم خوشست</p></div>
<div class="m2"><p>هر کجا، گویی، بساط عیش را آنجا کشیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس چرا از دست دنیا ساغر محنت کشد؟</p></div>
<div class="m2"><p>ساغری گیریم و دست از محنت دنیا کشیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقیا، میخانه دریاییست پر ز آب حیات</p></div>
<div class="m2"><p>جهد کن، تا کشتی خود را در آن دریا کشیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نازنینان سرکش و ما در مقام احتیاج</p></div>
<div class="m2"><p>جای آن دارد کزیشان ناز استغنا کشیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ز حال زار خود پیش تو نتوان دم زدن</p></div>
<div class="m2"><p>گوشه ای گیریم و آهی از دل شیدا کشیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای رقیب سنگدل، زین خشم و کین بگذر، که ما</p></div>
<div class="m2"><p>ناز رعنایی ز یار نازک رعنا کشیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فکر خوبان کن، هلالی، فکر دیگر تا بکی؟</p></div>
<div class="m2"><p>خود چرا بر لوح خاطر نقش نازیبا کشیم؟</p></div></div>