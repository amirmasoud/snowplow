---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>با دل گفتم کای دل پرفن! چکنم؟</p></div>
<div class="m2"><p>وین درد گرفته را به دامن چکنم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل سر زده و خجل به من کرد نگاه</p></div>
<div class="m2"><p>گفت این همه دیده می کنم من چکنم؟</p></div></div>