---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>ای کم زده خورشید فلک از رایت</p></div>
<div class="m2"><p>عاجز شده کان ز طبع گوهرزایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن زهره نداشت رنج کاید بر تو</p></div>
<div class="m2"><p>آمد به بهانه بوسه زد بر پایت</p></div></div>