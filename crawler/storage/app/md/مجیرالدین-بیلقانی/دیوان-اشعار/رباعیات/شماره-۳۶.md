---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>هر زخم جفایی که فلک پنهان داشت</p></div>
<div class="m2"><p>زد بر دل ما از قضا فرمان داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دور فلک دست فرو هل که به صبر</p></div>
<div class="m2"><p>با زخم تو پای بیش ازین نتوان داشت</p></div></div>