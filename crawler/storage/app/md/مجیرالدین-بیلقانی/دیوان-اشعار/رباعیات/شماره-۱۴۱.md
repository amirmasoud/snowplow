---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>ای موی سپید! اگر شبی با یاری</p></div>
<div class="m2"><p>بنشینم و از عیش بر آید کاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد عذر نهم گر بودش آزاری</p></div>
<div class="m2"><p>این جور ترا چه عذر سازم باری</p></div></div>