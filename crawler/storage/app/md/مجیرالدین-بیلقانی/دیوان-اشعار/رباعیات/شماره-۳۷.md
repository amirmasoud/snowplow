---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>با من غمت ای فقاعی حور سرشت</p></div>
<div class="m2"><p>هنگام وفا تخم تبهکاری کشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دل که فقاع از تو گشودی همه سال</p></div>
<div class="m2"><p>اکنون سخن وصل تو بر یخ بنوشت</p></div></div>