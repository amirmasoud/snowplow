---
title: >-
    شمارهٔ ۱۳۵
---
# شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>چون سایه نه نیستم نه هستم بی تو</p></div>
<div class="m2"><p>وز سایه خویشتن گسستم بی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا سایه وصل برگرفتی ز سرم</p></div>
<div class="m2"><p>چون سایه به خاک در نشستم بی تو</p></div></div>