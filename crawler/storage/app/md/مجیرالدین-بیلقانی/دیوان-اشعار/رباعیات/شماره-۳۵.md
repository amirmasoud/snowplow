---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>چون دلم که زلف یارم خم داشت</p></div>
<div class="m2"><p>در حلقه او رفت و قدم محکم داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیچاره ندانست چوبه در نگریست</p></div>
<div class="m2"><p>هر حلقه از آن زلف دری در غم داشت</p></div></div>