---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>هر کو به جهان غمخور زلف تو شود</p></div>
<div class="m2"><p>دلتنگ تر از چنبر زلف تو شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وآن دل به طمع غمخور زلف تو شود</p></div>
<div class="m2"><p>دانی چه کند در سر زلف تو شود؟</p></div></div>