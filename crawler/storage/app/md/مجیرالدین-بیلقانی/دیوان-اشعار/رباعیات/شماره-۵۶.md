---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>گفتم ز سپاهان مدد جان خیزد</p></div>
<div class="m2"><p>لعلی است مروت که از آن کان خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی دانستم کاهل سپاهان کورند؟</p></div>
<div class="m2"><p>با آنهمه سرمه کز سپاهان خیزد</p></div></div>