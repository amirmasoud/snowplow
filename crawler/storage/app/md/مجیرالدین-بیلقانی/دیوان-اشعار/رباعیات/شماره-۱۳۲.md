---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>کندم همه شب ریش ز عشقت تا تو!</p></div>
<div class="m2"><p>کافر شدی و نماند خونی با تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون که تو ریش می کنم من نکنم</p></div>
<div class="m2"><p>یا من کنم این بیشه ناخوش یا تو</p></div></div>