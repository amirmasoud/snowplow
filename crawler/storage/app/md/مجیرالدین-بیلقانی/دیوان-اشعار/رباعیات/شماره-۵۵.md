---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>هر دم به تو خصمی دگرم برخیزد</p></div>
<div class="m2"><p>ترسم که ز عشقت اثرم برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وآن روز که ساعتی برم بنشینی</p></div>
<div class="m2"><p>بیمست که عالم از سرم برخیزد</p></div></div>