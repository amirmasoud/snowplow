---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>من دوش به یار گفتم ای تنگ شکر</p></div>
<div class="m2"><p>شاید که ترا گزم بار دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگرفت به دندان لب چون بسد تر</p></div>
<div class="m2"><p>یعنی لب از آن تست و دندان بر سر</p></div></div>