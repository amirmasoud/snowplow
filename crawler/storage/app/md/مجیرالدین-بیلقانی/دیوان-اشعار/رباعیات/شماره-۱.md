---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>دلبند سمنبرست گلرفتارا</p></div>
<div class="m2"><p>شیرین سخنا ماه شکر گفتارا!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که هیچ یاد ناری ما را</p></div>
<div class="m2"><p>روز تو خجسته باد یارب یارا!</p></div></div>