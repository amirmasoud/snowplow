---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>در معرکه شیر آتش انگیز تویی</p></div>
<div class="m2"><p>در بزم به از هزار پرویز تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون ریز عدو به خنجر تیز تویی</p></div>
<div class="m2"><p>اسلام نواز و قایم آویز تویی</p></div></div>