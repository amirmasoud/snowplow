---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>زان دل ز من ای سرو سهی! نستانی</p></div>
<div class="m2"><p>خواهی که ز من دل تهی نستانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا لب ندهی دل ز رهی نستانی</p></div>
<div class="m2"><p>اینست سخن تا ندهی نستانی</p></div></div>