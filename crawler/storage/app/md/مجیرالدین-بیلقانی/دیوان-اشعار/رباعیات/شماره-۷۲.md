---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>مگذار که سودم به زیان در برود</p></div>
<div class="m2"><p>و آوازه جورت به جهان در برود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانا ز پی بوس و کناری که نبود</p></div>
<div class="m2"><p>شاید که دل من به میان در برود</p></div></div>