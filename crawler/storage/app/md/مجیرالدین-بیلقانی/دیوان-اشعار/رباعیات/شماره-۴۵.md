---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>با من چو شبی به وصل در پیوندد</p></div>
<div class="m2"><p>نشسته هنوز رخت بر می بندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنشینم و در فراق او می گریم</p></div>
<div class="m2"><p>برخیزد و بر گریه من می خندد</p></div></div>