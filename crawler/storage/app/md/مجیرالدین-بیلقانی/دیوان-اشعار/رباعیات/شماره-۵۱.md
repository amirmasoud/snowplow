---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>دل گر سوی لهو و ناز کم می یازد</p></div>
<div class="m2"><p>شاید که ز غم به خود نمی پردازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندین غم دل که سوی ما می تازد</p></div>
<div class="m2"><p>گر دل همه از سنگ بود بگدازد</p></div></div>