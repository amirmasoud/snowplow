---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>یک دست به مصحف و دگر دست به جام</p></div>
<div class="m2"><p>گه نزد حلال مانده گه نزد حرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماییم درین عالم ناپخته خام</p></div>
<div class="m2"><p>نه کافر مطلق نه مسلمان تمام</p></div></div>