---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>چون کار من از شهاب بر چرخ رسید</p></div>
<div class="m2"><p>وز دست غمم به لطف خود باز خرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هجر آمد و روز من سیه کرد چو شب</p></div>
<div class="m2"><p>یعنی که شهاب جز به شب نتوان دید</p></div></div>