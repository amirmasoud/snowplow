---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>ای دل طمع از جفاپرستی بردار</p></div>
<div class="m2"><p>هشیار شو و دلت ز مستی بردار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کار خلاص تو دعا خواهم کرد</p></div>
<div class="m2"><p>ای پای ز جای رفته دستی بردار</p></div></div>