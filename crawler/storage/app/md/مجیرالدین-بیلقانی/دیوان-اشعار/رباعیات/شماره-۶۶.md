---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>یارم سر سرو سایه ور می شکند</p></div>
<div class="m2"><p>بر برگ سمن سنبل تر می شکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوز دل و آب چشم گریان مرا</p></div>
<div class="m2"><p>می داند و می بیند و بر می شکند</p></div></div>