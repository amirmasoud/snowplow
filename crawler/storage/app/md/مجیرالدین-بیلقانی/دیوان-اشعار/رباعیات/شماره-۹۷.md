---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>گر دیده بدی روی ترا یوسف مصر</p></div>
<div class="m2"><p>آشفته شدی چو من گدا یوسف مصر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این همه هم به نیکویی غره مشو</p></div>
<div class="m2"><p>با آنهمه نیکویی کجا یوسف مصر؟</p></div></div>