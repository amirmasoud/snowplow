---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>آن دل که همیشه در طرب داشت شتاب</p></div>
<div class="m2"><p>وان دیده که بد رخ تو وی را محراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هجر تو ای نوش لب تلخ جواب</p></div>
<div class="m2"><p>پروانه آتشست و پیمانه آب</p></div></div>