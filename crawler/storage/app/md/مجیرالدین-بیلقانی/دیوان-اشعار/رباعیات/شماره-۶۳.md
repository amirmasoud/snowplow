---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>با چشم چگونه اشگ دارد پیوند؟</p></div>
<div class="m2"><p>بودیم چنان هر دو بهم روزی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد قطره سرشگ خون ز چشم افگندم</p></div>
<div class="m2"><p>امروز که او چو اشگم از چشم فگند</p></div></div>