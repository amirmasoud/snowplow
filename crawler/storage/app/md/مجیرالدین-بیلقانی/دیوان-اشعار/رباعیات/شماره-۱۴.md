---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>تن در غم تو همچو زه از کاستی است</p></div>
<div class="m2"><p>واندر تو چو دامن همه ناراستی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر بسته شد از تو چو گریبان غم آنک</p></div>
<div class="m2"><p>در عشق تو سر گشاده چون آستی است</p></div></div>