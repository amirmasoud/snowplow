---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>آساید اگر شوی تو همخوابه من</p></div>
<div class="m2"><p>گوش فلک ستمگر از لابه من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دوست جهانیان چون شنگرف برند</p></div>
<div class="m2"><p>خاک سر کوی تو ز خونابه من</p></div></div>