---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>هر کو شرف خدمت تو بگزیند</p></div>
<div class="m2"><p>فارغ شود از جهان و خوش بنشیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>والله که مبارک شود آن کس را روز</p></div>
<div class="m2"><p>کز اول بامداد رویت بیند</p></div></div>