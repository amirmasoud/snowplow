---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>ای دیده نگویی که ترا با دل چیست</p></div>
<div class="m2"><p>وی دل ز تو چند خواهد این دیده گریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دیده تو عاقبت نمی اندیشی</p></div>
<div class="m2"><p>وی دل تو به عاقبت نمی دانی زیست</p></div></div>