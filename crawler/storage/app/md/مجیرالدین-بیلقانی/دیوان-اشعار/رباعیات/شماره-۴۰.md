---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>آنرا که بد امید وصال از چشمت</p></div>
<div class="m2"><p>دور از لبت افتاد به حال از چشمت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان دل که ز عشق حله در گوش تو شد</p></div>
<div class="m2"><p>خورده ست هزار گوشمال از چشمت</p></div></div>