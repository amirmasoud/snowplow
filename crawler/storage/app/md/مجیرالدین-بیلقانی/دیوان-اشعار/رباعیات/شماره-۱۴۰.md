---
title: >-
    شمارهٔ ۱۴۰
---
# شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>با دل گفتم چو دلبری بگزیدی</p></div>
<div class="m2"><p>بنشین و بگو که با منش چون دیدی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل گفت که از تو جان طمع می دارد</p></div>
<div class="m2"><p>می دان تو و این سخن ز من نشنیدی</p></div></div>