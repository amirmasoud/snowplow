---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>ای صبر نگفتی چو غمی پیش آید</p></div>
<div class="m2"><p>خوش باش که کار تو ز من بگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتی چو کلاه گوشه غم دیدی</p></div>
<div class="m2"><p>ای صبر کنون کفش کرا می باید؟</p></div></div>