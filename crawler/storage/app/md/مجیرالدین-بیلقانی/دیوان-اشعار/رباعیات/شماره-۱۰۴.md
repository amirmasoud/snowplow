---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>ای چرخ به زیر پای تو پست شدم</p></div>
<div class="m2"><p>وز جام شفق جرعه تو مست شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محنت خور و مستمند پیوست شدم</p></div>
<div class="m2"><p>دستی بر نه کنون که از دست شدم</p></div></div>