---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>بس دل که ز عشق تو سر خویش گرفت</p></div>
<div class="m2"><p>بس جان که به بازار تو از دست برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم همه سوختی وزین کم نکنی</p></div>
<div class="m2"><p>تا بر رخت آتشست و در پیش تو نفت</p></div></div>