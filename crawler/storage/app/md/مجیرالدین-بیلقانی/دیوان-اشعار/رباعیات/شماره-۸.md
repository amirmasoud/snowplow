---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>گل صبحدم از باد برآشفت و بریخت</p></div>
<div class="m2"><p>با باد صبا حکایتی گفت و بریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بد عهدی عمر بین که گل در ده روز</p></div>
<div class="m2"><p>سر بر زد و غنچه کرد و بشکفت و بریخت</p></div></div>