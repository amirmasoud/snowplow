---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>صد عشوه نغز و دلستان آوردی</p></div>
<div class="m2"><p>تا عمر عزیزم به زیان آوردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی به برم جان تو دل خوش می دار</p></div>
<div class="m2"><p>تا جان بردن مرا به جان آوردی</p></div></div>