---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>آن عاقل دوست همچو ابله دشمن</p></div>
<div class="m2"><p>آن کرد به من کان نکند صد دشمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر مهر چنین بود غلام کینم</p></div>
<div class="m2"><p>ور دوست چنین کند عفاالله دشمن</p></div></div>