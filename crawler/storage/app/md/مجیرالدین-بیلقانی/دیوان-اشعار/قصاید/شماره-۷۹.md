---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>لله درک ای ز جهان بر سر آمده</p></div>
<div class="m2"><p>ذات تو از مکان خرد برتر آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شخص مطهر تو نهالی است در کجا؟</p></div>
<div class="m2"><p>در بوستان ملک و معالی بر آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلطان یک سواره که خورشید نام اوست</p></div>
<div class="m2"><p>صد ره به زیر سایه عدلت در آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با آب لطف و آتش خشمت که دور باد</p></div>
<div class="m2"><p>هم آب خشک مانده هم آتش تر آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نامت به وقت بستن مشروح مملکت</p></div>
<div class="m2"><p>از جمله خسروان همه سر دفتر آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر شب ز بهر پاس تو گردون سرمه رنگ</p></div>
<div class="m2"><p>با صد هزار دیده چون عبهر آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکر به خدمت سخن دلگشای تو</p></div>
<div class="m2"><p>بسته میان به صورت نیکشر آمده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بقعه ای که سکه به نام تو نیست هست</p></div>
<div class="m2"><p>آواز الغیاث ز نقش زر آمده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خطه ای که خطبه به نام تو می کنند</p></div>
<div class="m2"><p>روح الامین به تهنیت منبر آمده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خصم بداختر تو که چون شب سیه دلست</p></div>
<div class="m2"><p>دور از تو روز کورتر از اختر آمده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نظاره وجود تو چندین هزار گل</p></div>
<div class="m2"><p>هر شب بدین حدیقه نیلوفر آمده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حکم تو چنبری است که سرهای گردنان</p></div>
<div class="m2"><p>هست از طریق عجز در آن چنبر آمده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو پهلوان ملکی و پهلوی مشرکی</p></div>
<div class="m2"><p>از تیغ تازه روی تو بر بستر آمده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تیرت گشاده چرخ یکم همچو تیغ صبح</p></div>
<div class="m2"><p>ای رای تو چو صبح دوم انور آمده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیزارم از سعادت اگر در زمانه هست</p></div>
<div class="m2"><p>صاحب سعادتی چو تو از مادر آمده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خیل تو هست انجم و صدر تو آسمان</p></div>
<div class="m2"><p>تو آفتاب و ذره تو لکشر آمده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در معرکه ز خنجر آتش فشان تو</p></div>
<div class="m2"><p>اجزای خاک تیره چو خاکستر آمده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر سر که دست رحمت ازو بر گرفته ای</p></div>
<div class="m2"><p>از پای کوب حادثه اندر سر آمده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فر تو از عدوی تو ناید که کس ندید</p></div>
<div class="m2"><p>کار دم مسیح ز دم خر آمده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون تیغ یک زبانی و چون چرخ ساده دل</p></div>
<div class="m2"><p>وز تیغ تست چرخ چنین مضطر آمده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حمل جهان به صدر تو چون ابر و آفتاب</p></div>
<div class="m2"><p>از باختر رسیده و از خاور آمده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صد ره فقع گشاد سپهر سداب رنگ</p></div>
<div class="m2"><p>زان تیغ همچو برگ سداب اخضر آمده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جاوید زی که صید همه کاینات هست</p></div>
<div class="m2"><p>با جره باز همت تو لاغر آمده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مه در خسوف، هندوی این آستان شده</p></div>
<div class="m2"><p>گل در صبوح، خارکش این در آمده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از بهر استماع مقامات عدل تو</p></div>
<div class="m2"><p>افلاک جمله گوش چو سیسنبر آمده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در عهد تو که یوسف مصر جهان تویی</p></div>
<div class="m2"><p>هستند میش و گرگ به آبشخور آمده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر اوج ملک شاه که گردون دیگرست</p></div>
<div class="m2"><p>رأی تو هست ثابته دیگر آمده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در چشمم است تا به یکی هفته دگر</p></div>
<div class="m2"><p>نصرت به دست بوس، بدین محضر آمده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خصمت به نیم شب سوی گرگان گریخته</p></div>
<div class="m2"><p>بر وی ز فتنه، حادثه منکر آمده</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دردست، خنجرش به مثل پرنیان شده</p></div>
<div class="m2"><p>واندیشه در دلش بتر از خنجر آمده</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای فضل حق ز طبع لطیفت عیان شده</p></div>
<div class="m2"><p>وی رزق خلق در کف تو مضمر آمده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بر خور ز ملک و هیچ میندیش از آنکه هست</p></div>
<div class="m2"><p>شاخی به دست خصم تو بس بی بر آمده</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>باشد هلاک مورچه، چون پر بر آورد</p></div>
<div class="m2"><p>بدخواه تست مورچه پر بر آمده</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از شوق حضرت تو سپهر پیاله رنگ</p></div>
<div class="m2"><p>لب بر گشاده تر ز لب ساغر آمده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بزمت بهشت و جام تو شد چشمه حیات</p></div>
<div class="m2"><p>تو خضر ثانی و پدر اسکندر آمده</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از فر او که تا به ابد منقطع مباد</p></div>
<div class="m2"><p>در حکم تو سه نوبت و شش کشور آمده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وز دولتش که تا گه محشر به جای باد</p></div>
<div class="m2"><p>بر دشمن تو محنت صد محشر آمده</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا روی روز و طره شب هست در نظر</p></div>
<div class="m2"><p>چون یاسمین نموده و چون عنبر آمده</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا در میان باغ بود نفحه شمال</p></div>
<div class="m2"><p>گه نقش بند گشته و گه زرگر آمده</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>صدر تو بوسه جای ملوک زمانه باد</p></div>
<div class="m2"><p>ای بر سر ملوک جهان افسر آمده</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کار تو بر زمین به از آن باد کآسمان</p></div>
<div class="m2"><p>باشد به بارگاه تو خدمتگر آمده</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سال کهن به یمن سعادت برون شده</p></div>
<div class="m2"><p>روز نوت به دولت و شادی در آمده</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مداح حضرت تو مجیر از پی ثنا</p></div>
<div class="m2"><p>با طبع همچو کان زر و گوهر آمده</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خصم تو سر بریده و ذات تو از کمال</p></div>
<div class="m2"><p>بر جمله سروران جهان سرور آمده</p></div></div>