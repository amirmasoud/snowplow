---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>نداست سوی من از دل به هر نفس صد بار</p></div>
<div class="m2"><p>که پای مرغ قناعت به دام صبر در آر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو سایه خاک در کس مبوس از آنکه ترا</p></div>
<div class="m2"><p>فتاده پرتو خورشید فقر بر دیوار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمانه حادثه زایی است پیش او منشین</p></div>
<div class="m2"><p>که بار بر تو نهد گر نهد به پیش تو بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذر کن از فلک ایرا که بر سرای نجات</p></div>
<div class="m2"><p>دری است جرم فلک لیک آتشین مسمار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا ز صحبت گردون کرانه به زیرا</p></div>
<div class="m2"><p>تو زشت رویی و او صوفیی است آینه دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طلاق نامه نیابی ز خود چنین که تویی</p></div>
<div class="m2"><p>دراز امید و سیه دل نشسته چون طومار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز باغ عالمت ار میوه تلخ و ترش دهند</p></div>
<div class="m2"><p>بخور که شاخ خسک زعفران نیارد بار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا ندای دل ار چند گوشمال ده است</p></div>
<div class="m2"><p>به گوش جان شوم این پند را پذیرفتار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز روی صورت و معنی جهان خوش است ولی</p></div>
<div class="m2"><p>چنانکه پای ورم کرده فربهی است نزار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نواله چون به دلت در دهد؟ که طوطی را</p></div>
<div class="m2"><p>فتد ز طعم شکر خون تازه در منقار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا چو معده شد از هفت ابای عزلت سیر</p></div>
<div class="m2"><p>دلم به گرسنگان کرد هشت خلد ایثار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهان و نان همه چون دایره ست و من نشوم</p></div>
<div class="m2"><p>ز عشق هر دو دهن باز کرده چون پرگار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نشان حرص ز دل هم به دل شود زیرا</p></div>
<div class="m2"><p>که زهر مار شود دفع هم به مهره مار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو مرد کار نیی زان سبب که در ره فقر</p></div>
<div class="m2"><p>چو مویی از سر تو رفت رفتی از سر کار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ببند لب ز سخن تا به مرگ میری از آنک</p></div>
<div class="m2"><p>زه کمان خورد از لب گشادگی، سوفار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مباش بسته صورت که موم رنگین است</p></div>
<div class="m2"><p>شکوفه ای که برد نخل بند در بازار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بهار عالمی و کوتهست عمر تو زانک</p></div>
<div class="m2"><p>دراز روز بهاری بود نه عمر بهار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شب امید تو آبستن آنگهی گردد</p></div>
<div class="m2"><p>که عقل تو ز عروس جهان شود بیزار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ترا چو مرد یقین چون کنی شکار نجات؟</p></div>
<div class="m2"><p>که درجهان نکند کس به باز مرده شکار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مباش زنده مردار اگر کسی از حرص</p></div>
<div class="m2"><p>که آن سگست که هم زنده است و هم مردار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جهان تیره به چشم تو روشن آمد از آنک</p></div>
<div class="m2"><p>سر تو هست برون از دریچه پندار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو تا تویی نروی راه و دست در نکشی</p></div>
<div class="m2"><p>که پا یکی است ترا چون درخت و دست هزار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برو که جای تو هم خاک به که معقد صدق</p></div>
<div class="m2"><p>از آنکه جای جعل پارگین به از گلزار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مدار چشم و ببین و مگوی چون بینم؟</p></div>
<div class="m2"><p>که پر نداشت و بپرید جعفر طیار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز مرغزار قناعت قدم مبر کانجا</p></div>
<div class="m2"><p>نبات روح نوازست وآب نوشگوار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر آنچه نیک تو شد بد شمر که بیضه قز</p></div>
<div class="m2"><p>تراست جامه ولی کرم پیله راست حصار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سپر ز عافیت اولیتر اندرین منزل</p></div>
<div class="m2"><p>که موش قلعه گشای است و پشه نیز مگذار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خلیفه را پسری! گر چه بر خلاف پدر</p></div>
<div class="m2"><p>به جای تن دل و دیوان شده خلیفه شعار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خلاف شرع مگو همچو چنگ تا نشوی</p></div>
<div class="m2"><p>گلو بریده به ده جای راست چون مزمار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز خاک، عقل نجوید موافقت که درو</p></div>
<div class="m2"><p>جهت شش آمد و حس پنج و امهات چهار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مگیر انس که راحت نماند در صحبت</p></div>
<div class="m2"><p>مجوی مشگ که آهو نماند در تاتار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عنان دل به کف صدق ده که انجم چرخ</p></div>
<div class="m2"><p>سپیده دم همه بر صبح صادقست نثار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زمانه دیده راحت بسوخت نیست عجب</p></div>
<div class="m2"><p>که داشت پیل و پلاس از شهاب در شب تار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جهان به پرده کژ گوهر دل تو شکست</p></div>
<div class="m2"><p>تو با شکسته دلی پرده بند موسیقار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که خورد جرعه راحت به زیر جام فلک</p></div>
<div class="m2"><p>که همچو جرعه ندید آب روی ریخته خوار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>قرین ثابته کی گشت فکرت از شهوت</p></div>
<div class="m2"><p>معید مدرسه کی شد چکاوک از تکرار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حیات حاصل هر صورتی مدان زیرا</p></div>
<div class="m2"><p>که نفس نقش بود زین حساب در فرخار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سماک رامح گردون کشید نیزه چو دید</p></div>
<div class="m2"><p>که حلقه ایست جهان زیر گنبد دوار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو سر ز حلقه بکش پیش از آن که رمح سماک</p></div>
<div class="m2"><p>درون حلقه کند حلق هستی تو فگار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>برید خاطر من صبحدم ندا در داد</p></div>
<div class="m2"><p>که زین نشیمن خاکی نظر ببر زنهار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سبل گرفته ببین چشم آسمان ز شفق</p></div>
<div class="m2"><p>از آنکه دیده برین مرکز افگند هر بار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو عیسی ار هوست چشمه سار گردونست</p></div>
<div class="m2"><p>گیای دهر بدین خر طببعتان بگذار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بهای یکشبه وصل عدم، سه روح بده</p></div>
<div class="m2"><p>که رایگان به خسان رخ نمی نماید یار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مباش همدم کس چون دم تو یافت صفا</p></div>
<div class="m2"><p>که آینه، سیه از همنفس شود ناچار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به دیده خار، همه گل نگر که اندر چشم</p></div>
<div class="m2"><p>شناسی این قدر آخر که گل بهست از خار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مبین به کبک که او فاسقی است در خرقه</p></div>
<div class="m2"><p>نگر به مور که او مؤمنی است با زنار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ترا عزیمت عزلت درست کی گردد؟</p></div>
<div class="m2"><p>که همچو زر شده ای ز آرزوی زر بیمار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شگفت نیست اگر بر دل تو زر گذرد</p></div>
<div class="m2"><p>که زر نخست محک بیند آنگهی معیار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کف ترازو اگر پر زرست شاید از آنک</p></div>
<div class="m2"><p>ستاند و دهد او بی دروغ و بی آزار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو زر به دست تو افتاد دست و روی بشوی</p></div>
<div class="m2"><p>که هست صورت شش مرده بر یکی دینار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مجیر تا ز عدم بر بساط خاک نشست</p></div>
<div class="m2"><p>چو حقه خسته دل است و چو مهره کج رفتار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز چار شهر طبیعت نجات یافت چنانک</p></div>
<div class="m2"><p>به خلوه خانه روحانیان گرفت قرار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>رساند گوی سخن تا به ساق عرش مجید</p></div>
<div class="m2"><p>ز پای بوسی خورشید آسمان آثار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>طلسم بند مجسطی گشای شمس الدین</p></div>
<div class="m2"><p>که دین به پشتی او تازه روست چون گلنار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>وحید عصر براهیم احمد آنکه ازوست</p></div>
<div class="m2"><p>ثبات شرع براهیم و احمد مختار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به بارگاه دل طاهرش ز رحمت فیض</p></div>
<div class="m2"><p>چنان شده ست که روح القدس نیابد بار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز کلک مصری هندو نمایش این عجب است</p></div>
<div class="m2"><p>که شب به فتوی او هندویست مصری خوار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نماست مصر از آنجا به بهشت ادرش</p></div>
<div class="m2"><p>عطاردش قلم آورد و مشتری دستار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سوار کردش ازل بر سیه سپید علوم</p></div>
<div class="m2"><p>بلی بر ابلق صبح، آفتاب گشت سوار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>مجره، نامه حکمیست با بنات النعش</p></div>
<div class="m2"><p>به نیکی سخنش هر دو کرده اند اقرار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سیاه رویم ازو کرباند مده</p></div>
<div class="m2"><p>شود سیاه ز شمس آنکه بیندش بسیار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بدان خدای که بالای خاک در شش روز</p></div>
<div class="m2"><p>ببست قدرت او هفت پرده زنگار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به طبع پاک تو ای درس گوی مکتب شرع</p></div>
<div class="m2"><p>که همچو لوح ازل واقفست بر اسرار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>که این شکسته دل از آرزوی خدمت تو</p></div>
<div class="m2"><p>چو چشم حور و چو مژگان سیه دلست و نزار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نو افرید ز خاطر شعار مدحت تو</p></div>
<div class="m2"><p>از آنکه شیوه نو کار اوست در اشعار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>دعا به مدح بپیوست وین هم آزادیست</p></div>
<div class="m2"><p>که مهر خاتم قرآن نشاید استغفار</p></div></div>