---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ای عارض چو ماه ترا چاکر آفتاب</p></div>
<div class="m2"><p>یک بنده تو ماه سزد دیگر آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش رخ چو ماه تو بنهاده از حیا</p></div>
<div class="m2"><p>هر نخوتی که داشته اندر سر آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بوی مشک زلف تو ناید همی زند</p></div>
<div class="m2"><p>دم در هزار روزن چون مجمر آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسوت کبود دارد و رخ زرد سال و ماه</p></div>
<div class="m2"><p>در عشق رویت ای بت سیمین بر آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آرزوی روی تو هر صبحدم چو من</p></div>
<div class="m2"><p>رخساره زرد خیزد از بستر آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی روی و موی تو نبرد هیچ کس گمان</p></div>
<div class="m2"><p>بر آفتاب عنبر و در عنبر آفتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آفتاب عبهر تو هست تازه تر</p></div>
<div class="m2"><p>گر فر و تازگی برد از عبهر آفتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روی چو آفتاب به چشم چو نرگست</p></div>
<div class="m2"><p>از تازگی دهد که به نیلوفر آفتاب؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسیار کرده دفتر خوبی مطالعه</p></div>
<div class="m2"><p>جز روی تو نیافته سر دفتر آفتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمری است تا ز مشرق و مغرب همی رود</p></div>
<div class="m2"><p>با کام خشک و چشم تر ای دلبر آفتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از روی خود ندید در اطراف شرق و غرب</p></div>
<div class="m2"><p>جز رای شاه عادل روشنتر آفتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاهنشه ملوک قزل ارسلان که بخت</p></div>
<div class="m2"><p>از روی و رای اوست شده انور آفتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خطبه به نام رفعت قدرش همی کنند</p></div>
<div class="m2"><p>در اوج برج جوزا از منبر آفتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از بیم خوار داشت که بر وی رسید ازو</p></div>
<div class="m2"><p>ترکان همی کند رخ زر اصفر آفتاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سکه به نام بخشش جودش همی زند</p></div>
<div class="m2"><p>در قعر جوف و خارا دره بر آفتاب؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای کان لطف و عنصر مردی نپرورد</p></div>
<div class="m2"><p>در صد هزار کان چو یکی گوهر آفتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خاک در تو قبله آمال شد از آن</p></div>
<div class="m2"><p>خلقی نهاده روی چو خرما در آفتاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خلق تو بهره داد به مرد و زن آنچنان</p></div>
<div class="m2"><p>کز روشنی به مشگ . . . آفتاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر چرخ چنبری بکشد سر ز حکم تو</p></div>
<div class="m2"><p>خردش چوذره ذره کند . . . آفتاب</p></div></div>