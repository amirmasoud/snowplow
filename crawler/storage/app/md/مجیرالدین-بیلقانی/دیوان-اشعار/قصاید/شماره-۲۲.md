---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>این نادره بین باز کز ایام بر آمد</p></div>
<div class="m2"><p>در باغ جهان شاخ حوادث ببر آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وین بلعجبی ها که برین مهره خاکی است</p></div>
<div class="m2"><p>از حقه گردون مشعبد بدر آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگرفت سرا نگشت به دندان فلک، از عجز</p></div>
<div class="m2"><p>چندانکه فلک را همه ناخن بسر آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در باغ زمانه که نباتش همه زهرست</p></div>
<div class="m2"><p>نیشکر اگر چند خوش و سبز و تر آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مفریب نظر کن که هم از نکبت ایام</p></div>
<div class="m2"><p>صد گونه گره بر دل یک نیشکر آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فی الجمله جهان همچو رباطی است مسدس</p></div>
<div class="m2"><p>کز بهر وجود و عدم او را دو در آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز نخورد غم که ازین در که برون شد؟</p></div>
<div class="m2"><p>هرگز نکند یاد کز آن در که در آمد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صاحب نظری کو که ز من بشنود آخر؟</p></div>
<div class="m2"><p>این بلعجبی ها که مرا در نظر آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس شعبه بازی که به تقدیر قضا شد</p></div>
<div class="m2"><p>بس نادره کاری که ز دست قدر آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وین طرفه که از هر چه قضا کرد و قدر خواست</p></div>
<div class="m2"><p>احوال وی و قلعه او طرفه تر آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>القصه عمر کرد عمارت طبرک را</p></div>
<div class="m2"><p>پنداشت کزو جمله غرضهاش بر آمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون عاقبة الامر نظر کرد در آن کار</p></div>
<div class="m2"><p>سعیش همه باطل شد و نفعش ضرر آمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>معمور شد اول ز عمر گر چه به آخر</p></div>
<div class="m2"><p>چنبر شد و در گردن عمر عمر آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>او بیشتری جست ولی بر رگ جانش</p></div>
<div class="m2"><p>ناگاه ز فصاد اجل نیشتر آمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون مار ز سوراخ برون آمد و بی شک</p></div>
<div class="m2"><p>سر کوفته شد مار چو بر رهگذر آمد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پنداشت که عصیان اتابک ز خرد کرد</p></div>
<div class="m2"><p>خاری خردش را ز قضا بر بصر آمد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زان قلعه مشؤم برون آمد و در حال</p></div>
<div class="m2"><p>چاه سر چاهانش مقام و مقر آمد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از بی ادبی قصد جگر گوشه شه داشت</p></div>
<div class="m2"><p>تا لاجرمش تیر فنا بر جگر آمد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وآنکس که پس از وی ز پی مملکت ری</p></div>
<div class="m2"><p>بر عشوه اقبال و امید ظفر آمد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر چند که حال عمر آخر به بد افتاد</p></div>
<div class="m2"><p>حال ری بیچاره از آن بد بتر آمد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون تکیه همه بر طبرک بود دلش را</p></div>
<div class="m2"><p>بر پای دلش زان طبرک هم تبر آمد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفتا سحر آید شب اندیشه ما را</p></div>
<div class="m2"><p>خود واقعه بر وی همه وقت سحر آمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یارب چه شگفتست که در مدت یک ماه</p></div>
<div class="m2"><p>از کارگه حکمت حکمت بدر آمد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>این بهر امان کرد و ازو در خطر افتاد</p></div>
<div class="m2"><p>وآن آتش ازو جست نصیبش شرر آمد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در باغ جوانی شجری گشت که هر روز</p></div>
<div class="m2"><p>بر وی غم و اندیشه به جای ثمر آمد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فرسوده بزیر پی پیلان بلا باد</p></div>
<div class="m2"><p>چون خاک و گر خود همه خاکش درر آمد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گم باد اثر او ز جهان گر بحقیقت</p></div>
<div class="m2"><p>از فتنه درین خطه خاکی اثر آمد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>المنة لله که خدنگ غم از اینجا</p></div>
<div class="m2"><p>بر چشم و دل خصم شه دادگر آمد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اسکندر ثانی که اقالیم جهان را</p></div>
<div class="m2"><p>در نوبت او مدت سختی بسر آمد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جمشید زمان اعظم اتابک که ز تیغش</p></div>
<div class="m2"><p>صد واقعه بر خطه روم و خزر آمد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شغلش همه ترتیب مهمات جهان شد</p></div>
<div class="m2"><p>کارش همه تحصیل فنون هنر آمد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر چند که در چشم خرد چرخ بزرگ است</p></div>
<div class="m2"><p>نه چرخ بر همت او مختصر آمد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سری است ورا با ملک العرش کزان سر</p></div>
<div class="m2"><p>کارش همه شایسته تر از یکدگر آمد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وز عاطفت لطف ملک جل جلاله</p></div>
<div class="m2"><p>اقبال ورا یار و خرد راهبر آمد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سلطانش پدر خواند که در مملکت او</p></div>
<div class="m2"><p>مشفق تر و شایسته تر از صد پدر آمد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وز حضرت جلت به سوی بارگه او</p></div>
<div class="m2"><p>بر لفظ بریدان سعادت خبر آمد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کای شاه تو آنی که درین عالم فانی</p></div>
<div class="m2"><p>چون عمر خضر عمر تو بی حد و مر آمد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آباد بر او باد که از نطفه پاکش</p></div>
<div class="m2"><p>چون نصرت دین خسرو والا سیر آمد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آن شاه جوانبخت که این نه فلک پیر</p></div>
<div class="m2"><p>در جنب جلالش چو جهان بی خطر آمد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بحریست که لب تا لب آن بحر، درر گشت</p></div>
<div class="m2"><p>کانیست که سرتاسر آن کان، گهر آمد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ای شاه تو بی آنکه ز عدل تو درین عهد</p></div>
<div class="m2"><p>آهو بره فریادرس شیر نر آمد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از غایت انصاف تو در خطه عالم</p></div>
<div class="m2"><p>با گرگ، قج و میش به یک آبخور آمد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>با همت تو چشمه خورشید سها شد</p></div>
<div class="m2"><p>پیش کف تو عرصه قلزم شمر آمد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا بر در و درگاه تو اقبال حشر کرد</p></div>
<div class="m2"><p>بر جان حسودت ز حوادث حشر آمد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا خشک و تر جمله ممالک به کف تست</p></div>
<div class="m2"><p>خصم تو ز غم خشک لب و دیده تر آمد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تا هیچ خردمند نگوید به جهان در</p></div>
<div class="m2"><p>کز چشمه خورشید گل زرد بر آمد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا مطبخی عالم سفلی شده خورشید</p></div>
<div class="m2"><p>تا رنگرز مرکز خاکی قمر آمد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>عمر تو و رای عدد جذر اصم باد</p></div>
<div class="m2"><p>کز عمر تو در ملک جهان زیب و فر آمد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بادا شجر ذات تو پاینده ازیراک</p></div>
<div class="m2"><p>در باغ هنر سخت به آیین شجر آمد</p></div></div>