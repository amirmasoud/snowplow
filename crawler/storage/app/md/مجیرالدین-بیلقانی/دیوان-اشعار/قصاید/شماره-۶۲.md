---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>دست مخالف ببست تیغ جهان پهلوان</p></div>
<div class="m2"><p>کار موافق گشاد دست قزل ارسلان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز بدین زنده گشت ملکت کاوس کی</p></div>
<div class="m2"><p>باز بدان تازه گشت سنت نوشیروان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پست شد از جاه این، مرتبت اردشیر</p></div>
<div class="m2"><p>قطع شد از جاه آن، منزلت اردوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزم جز این را نگفت حاتم دریا نوال</p></div>
<div class="m2"><p>رزم جز آن را نخواند رستم گردون کمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جود در ایام این، دید کف کام بخش</p></div>
<div class="m2"><p>ملک به دوران آن، یافت کف کامران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیبت این جلوه داد، هیبت آن عرضه کرد</p></div>
<div class="m2"><p>حادثه بسته لب، دهر گشاده زبان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرخ نیارد چنین، خسرو نادر قرین</p></div>
<div class="m2"><p>دهر نیارد چنان، صفدر صاحبقران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تیغ گهر دار این، خصم سپارد به آب</p></div>
<div class="m2"><p>دست گهربار آن دوست رساند به نان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باره این را فلک، باز ببندد رکاب</p></div>
<div class="m2"><p>حمله آنرا قضا، باز نپیچد عنان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لشکر این بگذرد، کوه بجنبد ز جای</p></div>
<div class="m2"><p>مرکب آن در رسد، چرخ ببندد میان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جز ملک تاج بخش، اعظم اتابک کر است؟</p></div>
<div class="m2"><p>مه چو قزل ارسلان شه چو جهان پهلوان</p></div></div>