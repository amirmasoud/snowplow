---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>چه جرم است این بر آورده سر از دریای موج افگن؟</p></div>
<div class="m2"><p>به کوه اندر دمان آتش به چرخ اندر کشان دامن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخ گردون ز لون او به عنبر گشته آلوده</p></div>
<div class="m2"><p>دل هامون ز اشگ او به گوهر گشته آبستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی از میغ او گردد نهفته شاخ در لؤلؤ</p></div>
<div class="m2"><p>گهی از سعی او گردد سرشته خاک از لادن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنالد سخت بی علت بجوشد تند بی کینه</p></div>
<div class="m2"><p>بخندد گرم بی شادی بگرید زار بی شیون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گهی باشد چو برطرف زمرد بیخته عنبر</p></div>
<div class="m2"><p>گهی باشد چو در لوح خماهن ریخته چندن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمین آرای دود اندام گردون سای آتش دل</p></div>
<div class="m2"><p>سیه دیدار گوهر پاش میناپوش دیباتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز لاله باغ را دارد پر از بیجاده گون رایت</p></div>
<div class="m2"><p>ز سبزه راع را دارد پر از فیروزه گون خرمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گهی با مهر همخانه گهی با باد هم پیشه</p></div>
<div class="m2"><p>گهی با چرخ هم زانو گهی با بحر هم برزن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بشوید چهره نسرین بتابد طره سنبل</p></div>
<div class="m2"><p>بسنبد دیده نرگس بدرد جامه بر گلشن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو راه مردم ظالم هوا از جسم او انبه</p></div>
<div class="m2"><p>چو رای خسرو عادل زمین از چشم او روشن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مصاف افروز دشمن سوز شاه نیمروز آنکو</p></div>
<div class="m2"><p>درین ملکست کافی رأی وافی عهد صافی ظن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ملک بوالفضل نصربن خلف فرزانه تاج الدین</p></div>
<div class="m2"><p>که برباید همی تاج از سر شاهان شیر اوژن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زمانه بد سگالش را همی گوید که لاتأمن</p></div>
<div class="m2"><p>فرشته نیکخواهش را همی گوید که لاتحزن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حسامش را دهد زهره به هدیه شیر گردن کش</p></div>
<div class="m2"><p>سنانش را دهد مهره به رشوت مار دندان زن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بنان گردد ز تحریر قیاس جود او عاجز</p></div>
<div class="m2"><p>زبان گردد ز تقریر ثنای ذات او الکن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو تا ز درخش نگزیند بجز صحن فلک میدان</p></div>
<div class="m2"><p>چو بازد گوی نپسندد بجز قوس قزح چوگان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نماند از تیر و گرز او مگر بر روی رایت ها</p></div>
<div class="m2"><p>عقاب نادریده دل هزبر ناشکسته تن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جلال قدر او بحد صفات عدل او بی عد</p></div>
<div class="m2"><p>عطای دست او بی مر سخای طبع او بی من</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هدف گشت آسمان گویی خدنگش را که اندر شب</p></div>
<div class="m2"><p>نماید روی مه یکسر هدف کردار پر روزن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ایا در پایه تختت زمانه ساخته مأوی</p></div>
<div class="m2"><p>و یا در سایه تختت ستاره یافته مأمن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدانگه کز سجستان سوی غزنی برد آن لشگر</p></div>
<div class="m2"><p>همه با دولت خسرو همه با صولت بهمن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ملک تأیید بدر آیین، فلک تأثیر کوه آلت</p></div>
<div class="m2"><p>نهنگ آسیب ببر آفت پلنگ آشوب شیرافگن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دلیرانی که از گردون به نوک رمح سیاره</p></div>
<div class="m2"><p>ربودندی چو گنجشکان به منقار از زمین ارزن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مخالف چون برون آمد به میدان با چنان لشکر</p></div>
<div class="m2"><p>چو شیران عرین پر دل چو دیوان لعین پر فن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در آورده به پیش صف چو گردون زنده پیلانی</p></div>
<div class="m2"><p>که گردون شان بوقت کین نیارد گشت پیرامن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو کوه زفت شخص آورد چو غول گست حیلت گر</p></div>
<div class="m2"><p>چو باد تیز دریا بر چو تیر تند هامون کن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو ضرغام قوی جوشان چو عفریت حرون کوشان</p></div>
<div class="m2"><p>چو تمساح روان هایل چو ثعبان سیه ریمن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سپاهی از نهاد دیو و تو در جنگشان رستم</p></div>
<div class="m2"><p>گروهی بر نهاد خوک و تو در حربشان بیژن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قضا در تیغ سیمابی نهاده ریزه مرجان</p></div>
<div class="m2"><p>اجل بر درع زنگاری فشانده خرده روین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو خواب اندر سر مردان گزیده تیغ تو مرقد</p></div>
<div class="m2"><p>چو وهم اندر دل گردان گرفته تیغ تو مسکن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شده ز ارواح گمراهان هوا چون حلقه خاتم</p></div>
<div class="m2"><p>شده ز اجسام بدخواهان زمین چون چشمه سوزن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اجل با حربه قاطع بلا با باره سابق</p></div>
<div class="m2"><p>زمین در حله احمر زمان در کله ادکن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو در قلب سپه گویی بزیر ران در آورده</p></div>
<div class="m2"><p>تک او تیز چون صرصر رگ او سخت چون آهن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز نصرت بر تنت درقه ز قدرت بر کفت خنجر</p></div>
<div class="m2"><p>ز دولت بر سرت مغفر ز حشمت بر تنت جوشن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنان رفت از کمان تو سوی خصمان همی ناوک</p></div>
<div class="m2"><p>که گاه رجم سیاره ز گردون سوی آهرمن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو شد رای همایونت قوی با رایت عالی</p></div>
<div class="m2"><p>شد آثار ظفر ظاهر، شد انوار قدر روشن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نگشت از فر تو خسته از بن خونخوارگان یک دل</p></div>
<div class="m2"><p>نشد از زخم تو رسته از آن بیچارگان یک تن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زهی رسم بدیع تو عروس ملک را زیور</p></div>
<div class="m2"><p>خهی رای رفیع تو چراغ فتح را روغن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>درین بقعت پدید آمد که ناورد از بنی آدم</p></div>
<div class="m2"><p>ز اهل سیستان هرگز به مردی ایزد ذوالمن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هراسانند پیوسته ز پیکان تو مهر و مه</p></div>
<div class="m2"><p>تن آسانند همواره زاحسان تو مرد و زن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از آن خصمت چو پرویزن ز دیده خون همی ریزد</p></div>
<div class="m2"><p>که از تیرت دماغ او مشبک شد چو پرویزن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سزد ناهید، دست بخت مسعود ترا یاره</p></div>
<div class="m2"><p>شود خورشید فرق پای میمون ترا گر زن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خداوندا! اگر هستم بذات از خدمتت غایب</p></div>
<div class="m2"><p>ز جور عالم جافی ز دور گنبد توسن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مرا حرزست پیوسته ثنای تو به هر موضع</p></div>
<div class="m2"><p>برآوردست همواره دعای تو به هر مسکن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کنون نزدت فرستادم عروسی چون شکر کورا</p></div>
<div class="m2"><p>معالی هست پیرایه، معانی هست پیراهن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زمانه از شرف او را عصا به بسته بر جبهت</p></div>
<div class="m2"><p>ستاره از لطف او را قلاده کرده در گردن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر او را بهره ای باشد ز اقبال قبول تو</p></div>
<div class="m2"><p>شود خار مرادش گل، شود زهر مرامش من</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اگر چه مادحان داری ز من بهتر فراوانی</p></div>
<div class="m2"><p>یقین دانم که بد گویند پیشت شرح حال من</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نوای خوش نوا آید ز سوی حضرت عالی</p></div>
<div class="m2"><p>مرا ناگه چو موسی را ز سوی وادی ایمن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>الا تا از زمین لاله بروید در مه نیسان</p></div>
<div class="m2"><p>الا تا از هوا ژاله ببارد در مه بهمن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز مهرت باد چون لاله ز خنده چهره ناصح</p></div>
<div class="m2"><p>ز بیمت باد چون ژاله ز گریه دیده دشمن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>قدر را عزم تو قدوه، قضا را جزم تو عمده</p></div>
<div class="m2"><p>اجل را رزم تو قانون جهان را بزم تو گلشن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به نعل کره ختلی حصار دشمنان بسپر</p></div>
<div class="m2"><p>به نوک نیزه خطی سپاه دشمنان بشکن</p></div></div>