---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>باد صبح است که مشاطه جعد چمن است</p></div>
<div class="m2"><p>یا دم عیسی پیوند نسیم سمن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نکهت نافه مشگ است نه نافه است نه مشگ</p></div>
<div class="m2"><p>اثر آه جگر سوخته ای همچو من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس سرد گرم رو از بهر چراست؟</p></div>
<div class="m2"><p>یادم آمد ز پی آنکه رسول چمن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یارب این شیوه نو چیست؟ که از جنبش باد</p></div>
<div class="m2"><p>طره لاله پر از نافه مشگ ختن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد با دست تهی بر سر خس تاج نه است</p></div>
<div class="m2"><p>ابر با دامن تر بر دل گل نوبه زن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرقه مخروق کند از سر حالت گل و صبح</p></div>
<div class="m2"><p>کاین بر آن عاشق و آن بر دم این مفتن است؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده مرده نرگس همه گریان نگرد</p></div>
<div class="m2"><p>به سوی لاله که او زنده اندر کفن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بید یا سج زن باغست و صبا حلقه ربای</p></div>
<div class="m2"><p>ابر ناورد کن و صاعقه زوبین فگن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لاله و گل را زاندیشه آن عمر که نیست</p></div>
<div class="m2"><p>گر دلی هست همه ساله به غم ممتحن است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گنبد گل چو ز هم رفت به بادی گروست</p></div>
<div class="m2"><p>قحف لاله چو تهی شد به دمی مرتهن است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گل اگر یوسف عهدست عجب نبود از آنک</p></div>
<div class="m2"><p>رود نیلش قدح و ملکت مصرش چمن است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گل چو یوسف نبود من غلطم نیک برفت</p></div>
<div class="m2"><p>آنچنان غرقه به خون کوست مگر پیرهن است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قفس خاک پر از زمزمه فاخته است</p></div>
<div class="m2"><p>مجمر باغ پر از لخلخه نسترن است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بوی شیر از دهن سوسن از آن می آید</p></div>
<div class="m2"><p>که هنوزش سر پستان صبا در دهن است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ده زبانست و نگوید سخن و حق با اوست</p></div>
<div class="m2"><p>با چنان عمر که اوراست چه جای سخن است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سبزه گر نیمچه بر آب کشد باکی نیست</p></div>
<div class="m2"><p>کاب را روز و شب از باده ز ره در بدن است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنکه در باغ همی غنچه کله کژ ننهد</p></div>
<div class="m2"><p>راست بشنو ز من از هیبت شاه ز من است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>طاس زر بر سر نرگس همه شب در صحر است</p></div>
<div class="m2"><p>این هم از غایت عدل شه عالی سنن است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاه گردون حشر خسرو خورشید رکاب</p></div>
<div class="m2"><p>که چو خورشید فلک صفدر و لشکر شکن است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مالک شش جهت و عاقله هفت اقلیم</p></div>
<div class="m2"><p>که چو عقل ایمن و فارغ ز فساد و فتن است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پهلوان شاه جهانبخش که خاک قدمش</p></div>
<div class="m2"><p>حر ز جان ملک و سرمه چشم پرن است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اینت نوباوه اقبال که با خوی خوشش</p></div>
<div class="m2"><p>دامن و دست جهان پر گل و پر یاسمن است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>غصه خصمش از آن همچو فلک تو بر توست</p></div>
<div class="m2"><p>که سعادات فلک را به در او سکن است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خصمش ار خون خورد و سوزد می شاید از آنک</p></div>
<div class="m2"><p>شمع را قوت همه خون دل خویشتن است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ور به گردن زدن آسوده شود جایش هست</p></div>
<div class="m2"><p>چه کند راحت شمع از ره گردن زدن است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تیغ سر مستش در عربده گردد چو عقیق</p></div>
<div class="m2"><p>وین عجب نبود چون مولد اصلش یمن است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آن یمانی گهر روم ستان کز فزعش</p></div>
<div class="m2"><p>پشت افلاک چوی موی حبشی پرشکن است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چشم بد دور ز شاهی که بداندیش ازو</p></div>
<div class="m2"><p>اینما کان هر آن کس که بود در محن است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا بدو آب سعادت دهد از چشمه خضر</p></div>
<div class="m2"><p>دلو خورشید گهر چنبر زرین رسن است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بوی اقبال بهر بقعه که هست از در اوست</p></div>
<div class="m2"><p>که به یثرب اثر آه اویس قرن است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن محمدصفت و نام که عدلش عمری است</p></div>
<div class="m2"><p>وان علی مرتبت و علم که خلقش حسن است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جرعه جام جلالش مثلا موج زنی است</p></div>
<div class="m2"><p>که فلک رخنه کن از قوت و قلزم شکن است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بحر تر دامن و کان خشک لب است از چه؟ از آن</p></div>
<div class="m2"><p>که حدیثش حسد گوهر و در عدن است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دشمن از گوهر تیغش که چو پر مگسی است</p></div>
<div class="m2"><p>عنکبوت آسا پیرامن خود پرده تن است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ور نشنید پس آن پرده نه پی خردگیست</p></div>
<div class="m2"><p>که زنست او و زنان را پس پرده وطن است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>صدر او را به ضرورت کره خاکی جاست</p></div>
<div class="m2"><p>یوسف را ز حسد هفده نبهره ثمن است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مشتری هر سحر از منبر شش پایه خویش</p></div>
<div class="m2"><p>در ثنای تو زحل فهم و عطارد فطن است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شاد باش ای شه لشکر کش غازی که تراست</p></div>
<div class="m2"><p>قاعده لطف و کرم از کرم ذوالمنن است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>رشته جان تو چون سلسله چرخ قوی است</p></div>
<div class="m2"><p>دیده بخت تو چون چشم فلک بی وسن است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو اگر جهد کنی ور نکنی تاج دهی</p></div>
<div class="m2"><p>رستم ار تیغ زند ور نزند تهمتن است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سایه در دزدد از سهم تو خورشید فلک</p></div>
<div class="m2"><p>که به معنی همه تن تیغ و به صورت مجن است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آخر از پوست برون آمد و بی زرق بزیست</p></div>
<div class="m2"><p>با تو این چرخ تهی مغز که پر زرق و فن است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مرد و زن را ز زمانه کرمت داد خلاص</p></div>
<div class="m2"><p>هم علی رغم زمانه که نه مرد و نه زن است</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خسروا باده خور امروز که در سایه سرو</p></div>
<div class="m2"><p>باده بر کار طرب راست تر از نارون است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>رطل دلویست پر از آب طرب لیک از که؟</p></div>
<div class="m2"><p>از کف یوسف رویی که چهش در ذقن است</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مست بر خاسته ترکی که سپهرش هندوست</p></div>
<div class="m2"><p>خراب ناکرده بتی کش دل و جانها شمن است</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>روز نو باده کهن خواه که در مذهب عیش</p></div>
<div class="m2"><p>رونق روز نو از جام شراب کهن است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تا برای مدد نور درین صفه خاک</p></div>
<div class="m2"><p>شمع انجم را از طارم نیلی لگن است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>قوت فیض الهی مدد جان تو باد</p></div>
<div class="m2"><p>که وجود تو به رحمت مدد جان و تن است</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>باد از دور فلک قسم کله گوشه تو</p></div>
<div class="m2"><p>هر سعادت که به دوران فلک مقترن است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>این دعا از سر صدق است به رغبت بشنو</p></div>
<div class="m2"><p>زانکه حرز در تو ورد دعاهای من است</p></div></div>