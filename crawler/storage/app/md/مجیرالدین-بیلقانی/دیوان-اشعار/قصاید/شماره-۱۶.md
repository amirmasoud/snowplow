---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>تن به مهر تو دل ز جان برداشت</p></div>
<div class="m2"><p>جان امید از همه جهان برداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاسبان صبر بود بر در دل</p></div>
<div class="m2"><p>دزد غم ساز پاسبان برداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عافیت وقتی ار چه قاعده بود</p></div>
<div class="m2"><p>ترکتاز غم تو آن برداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشقت اول قدم که دست کشید</p></div>
<div class="m2"><p>مهر بشکست و نقد جان برداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف بستی فلک سپر بفگند</p></div>
<div class="m2"><p>لب گشادی شکر فغان برداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک راه توام از آنکه مرا</p></div>
<div class="m2"><p>عشقت از خاک رایگان برداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفته ای سایه از تو بردارم</p></div>
<div class="m2"><p>سایه از خاک چون توان برداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو فگندی مرا ز چشم ولیک</p></div>
<div class="m2"><p>کرم شاه کامران برداشت</p></div></div>