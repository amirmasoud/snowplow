---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>این خسیسان کز طمع طفل سخن می پرورند</p></div>
<div class="m2"><p>سربسر ابلیس طبع اند ار چه آدم پیکرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو بیژن در بن چاه ضلالت مانده اند</p></div>
<div class="m2"><p>گر چه در پرویزن ایام چون خس بر سرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در فریب عامه همچون صبح کاذب چابک اند</p></div>
<div class="m2"><p>لیک همچون صبح صادق ستر خاصان می درند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موش مقلوبند و هرجا کز پلنگ روز و شب</p></div>
<div class="m2"><p>بر دلی زخمی رسد خاکی بر آنجا گسترند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو اشترشان سزای گه کشی دان در هنر</p></div>
<div class="m2"><p>وآن مبین کایشان چو راه کهکشان در زیورند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیشه سان بر سنگ از آن زد گنبد نارنج رنگ</p></div>
<div class="m2"><p>کز نسب چون شیشه روشن روی و تاری گوهرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آهنین دارند رخ چون آینه زانک از تری</p></div>
<div class="m2"><p>زیر این هفت آینه جز خویشتن را ننگرند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آستین در رشک من تر کرده اند از اشگ چشم</p></div>
<div class="m2"><p>تا ز لوح خاطرم نقش معانی بسترند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طبع من کانست و دل دریا و این بی دولتان</p></div>
<div class="m2"><p>چون کف دریا همه تر دامن و خس پرورند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شیر سگ خوردند و با من روبهی زان می کنند</p></div>
<div class="m2"><p>زآتش طبعم گریزند ار همه شیر نرند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در جدل شیرند وین خوشتر که چون گاو ابلهند</p></div>
<div class="m2"><p>در هنر گاوند وین بتر که چون شیرابخرند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همجو مورند از پی خونم میان بر بسته باز</p></div>
<div class="m2"><p>ریزه های خوان طبع من چو موران می برند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عودشان بیدست ازیشان دود برخیزد نه بوی</p></div>
<div class="m2"><p>تا بر آتش مانده زین سیماب گون نه مجمرند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>راوق صرف صفا من خوردم اندر جام جم</p></div>
<div class="m2"><p>وین حریفان بین که از جان در دراوق می خورند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باد ریسه ست آسمان در همت من، وین خسان</p></div>
<div class="m2"><p>همچو دوک از حرص یعنی ریسمان در حنجرند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روزم از دیدارشان چون چشم آهو گشت از آنک</p></div>
<div class="m2"><p>من چو مسجد پاک و ایشان همچو سگ بد محضرند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>لعبت آسا از برون خوب از درون سو مرده اند</p></div>
<div class="m2"><p>لاجرم تصحیف لعبت را چو شیطان در خورند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر قصب پیچند می شاید که مصری مذهبند</p></div>
<div class="m2"><p>ور قبا بندند می زیبد که ترکی مخبرند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آب در سنگم از آن روشن دلم و ایشان همه</p></div>
<div class="m2"><p>سنگ در آیند از آن هم خشک خاطر هم ترند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غم غرابند ار چه سرتاسر بیان چون بلبل اند</p></div>
<div class="m2"><p>هم غلامند ار چه لب تا لب زبان چون خنجرند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همچو کرم پیله زاطلس چشم و ابرو کرده اند</p></div>
<div class="m2"><p>لیک در چشم خرد چون چین ابرو منکرند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مغزشان در کاسه سر آتش شهوت بسوخت</p></div>
<div class="m2"><p>کز پی آب سکره تر صفت چون ساغرند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>راست خواهی جمله کژگویان اعور خاطرند</p></div>
<div class="m2"><p>نیک پرسی جمله بدفعلان خنثی منظرند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دخلشان همچون چراغ از سوی پس بد پیش ازین</p></div>
<div class="m2"><p>وین عجبتر کاین زمان چون شمع صاحب افسرند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>طشت زرینشان وبال جان ایشان دان از آنک</p></div>
<div class="m2"><p>شمع را هر لحظه سر در طشت زرین می برند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نعلشان در آتشست از نظم سحرآسای من</p></div>
<div class="m2"><p>هر زمان چون نعل از آن در چارمیخ دیگرند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دست دل را دسته های نستر آمد شعر من</p></div>
<div class="m2"><p>وین دغل بازان دین بازوی جان را نشترند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زآدمیشان نشمرد عقل ار چه در جمعند از آنک</p></div>
<div class="m2"><p>صفر بنگارند در اعداد لیکن نشمرند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نقش ایشان نقش گرمابه است و می دان کز جهان</p></div>
<div class="m2"><p>با زنی چشم و جسم الا جنب در نگذرند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دین ابراهیمشان در دیده مسمارست از آنک</p></div>
<div class="m2"><p>هم دروگر هم دروع آور بسان آزرند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تیر من آه سحرگاهست و تیغ من زبان</p></div>
<div class="m2"><p>بشکنم صفشان به تیر و تیغ اگر صد لشکرند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ملک جم در ظلمت دلشان بکف شد لاجرم</p></div>
<div class="m2"><p>چشمه جان را نه خضر ندای عجب اسکندرند!</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ماده اند از از روی معنی لیک هنگام سخن</p></div>
<div class="m2"><p>طبعشان معنی نزاید زانکه از صورت نرند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بهر دق مصریند اندر تب دق لاجرم</p></div>
<div class="m2"><p>لب کبود و دیده تر چون نیل و چون نیلوفرند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کیمیای خاطرم خاک سخن را کرد زر</p></div>
<div class="m2"><p>وین خران چون صورت گچ مرده یک جو زرند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سکه شان برد آسمان تا همچو زر شش سری</p></div>
<div class="m2"><p>همدم جوقی همه مردار مردم پیکرند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دست دست تست با مشتی دغاهان ای مجیر!</p></div>
<div class="m2"><p>داوشان در کن که با زحمت همه در ششدرند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در جهان امروز صاحب ذوق و معنی طبع تست</p></div>
<div class="m2"><p>وین که خصمانند دعوی دار یا دعوت گرند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بر تو صد دشمن به یک جو تا تو در صف سخن</p></div>
<div class="m2"><p>عیسی وقتی و ایشان سر به سر کون خرند</p></div></div>