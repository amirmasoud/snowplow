---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>پرده مستان نواخت زخمه باد سحر</p></div>
<div class="m2"><p>باده ده ای عشق تو همچو سحر پرده در</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دایره بزم را نقطه چو از خال تست</p></div>
<div class="m2"><p>ساغر تا خط بیار سر ز خط ما مبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردمیی کن به شرط از پی ما پیش از آنک</p></div>
<div class="m2"><p>شهری در پای تو کشته شود سر بسر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رطل غمت می کشم پس تو به چون من کسی</p></div>
<div class="m2"><p>خط به چه در می کشی چون لب رطل ای پسر!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک شدم پیش تو جرعه خاصت مراست</p></div>
<div class="m2"><p>زانکه به بزم کرام خاک بود جرعه خور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم من خشک لب پیش تو پر اشگ به</p></div>
<div class="m2"><p>تا تو شکر لب کنی نقل ز بادام تر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نه ربابم مرا زخم مزن بر میان</p></div>
<div class="m2"><p>با دگری همچو چنگ دست مکن در کمر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چاشت خورد عشق تو بر دل ما تا ز حسن</p></div>
<div class="m2"><p>زلف تو بر نیم شب خنده زند چون سحر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از غم تو همچو نی صد گرهم بر دلست</p></div>
<div class="m2"><p>ای دو دل آخر بر آر کام من از یک شکر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زر به ترازو بخواه از من و با من مشو</p></div>
<div class="m2"><p>گاهی چون زر دو روی گه چو ترازو دو سر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خار به زر نه مرا زانکه من اکنون چو گل</p></div>
<div class="m2"><p>زر به کف آورده ام از پی تو سیمبر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاک زرم چون خسان بهر چو تو نقره ای</p></div>
<div class="m2"><p>کو کند این کار من با تو به از آب زر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همچو شکوفه بریز خون من ار من ترا</p></div>
<div class="m2"><p>گویم چون برگ گل از سر زر در گذر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر در تو آفتاب آینه صورت شده است</p></div>
<div class="m2"><p>تا دود آیینه وار در طلبت در بدر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سینه مکن کز غمت گشت جگرها کباب</p></div>
<div class="m2"><p>کم کن ارت زهره ایست رنج دلی از جگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مردم چشم منی پس تو ز نامردمی</p></div>
<div class="m2"><p>کار مرا کژمکن از خم ابرو بتر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>می نیم از بوی من سر که چه ریزی ز روی</p></div>
<div class="m2"><p>می خور و از من مکن همچو من از می حذر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر تو مرا همچو اشگ بفگنی از چشم خوار</p></div>
<div class="m2"><p>باز کنم همچو اشگ جای تو اندر بصر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گوشه دل گر خوری شاید کاقطاع تست</p></div>
<div class="m2"><p>جان به نظر کن که کرد شاه سوی وی نظر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قلزم دجله عطا مهدی دجال بند</p></div>
<div class="m2"><p>کسری جمشید جام خسرو خورشید فر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بلبل داود لحن تا ز چمن شد بدر</p></div>
<div class="m2"><p>شکل زره کرد از آب باد مسیحا اثر</p></div></div>