---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>تا رنج عیش یاد همایون منیر باد</p></div>
<div class="m2"><p>چشم جهان به جاه و جلالش قریر باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دولت به زور چرخ که چرخش به روزگار</p></div>
<div class="m2"><p>نصرت سپار باد و سعادت پذیر باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انوار مهر ز آب رخش مستعار شد</p></div>
<div class="m2"><p>امواج بحر از کف تو مستعیر باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایوانش قبله گاه وضیع و شریف شد</p></div>
<div class="m2"><p>میدانش جای بوس صغیر و کبیر باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردی که باد صبح بر آرد ز درگهش</p></div>
<div class="m2"><p>از نفع بر زمانه چو ابر مطیر باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاکی که بگسلد فلک از لعل مرکبش</p></div>
<div class="m2"><p>از قدر بر ستاره چو در سر سریر باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای نرد گوهر به تو اهل البصیر شد؟</p></div>
<div class="m2"><p>پیوسته چشم شرع بر این بصیر باد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیداری نجوم فلک گر نه رای تست</p></div>
<div class="m2"><p>تیر ابکم و زحل اصم و ضریر باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هفت اختر و سپهر به حکم تو راضیند</p></div>
<div class="m2"><p>ور نیستند هر دو علمشان قصیر باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چار امهات نشو به نور تو یافتند</p></div>
<div class="m2"><p>ور نیستند خط نشینان و صیر باد؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر اوج چرخ بر لب صحن سرات نیست</p></div>
<div class="m2"><p>از نکبت زوال چو بحر قعیر باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر سطح خاک بردم لطف نزد جمال</p></div>
<div class="m2"><p>خرم ترین فضاش چو قعر سعیر باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کیوان اگر نه خصم ترا می کند اسیر</p></div>
<div class="m2"><p>در دلو چاه تا به قیامت اسیر باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مریخ گر تنوره خصم تو بشکند</p></div>
<div class="m2"><p>جرم حمل نصیب تنور اثیر باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرجیس گر وفای ترا حق گزار نیست</p></div>
<div class="m2"><p>در چشم دهر حکم زوالش حقیر باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای ظل تو مثال ره نور ز اختران</p></div>
<div class="m2"><p>خورشید ز آفتاب دلت مستنیر باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن در رکاب مجلس این پرده پرورست</p></div>
<div class="m2"><p>در مجلس تو پردگی پرده گیر باد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>. . . رضای دل تو نیست</p></div>
<div class="m2"><p>روشن شبش ز نامه یوم عسیر باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر آب بحر نز پی در پروری تست</p></div>
<div class="m2"><p>طبع و دمش چو آتش و چون زمهریر باد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ور زانکه نز برای تو باشد عیار کان</p></div>
<div class="m2"><p>گنج دلش ز مایه گوهر فقیر باد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ور نظم و نثر بنده نه مدح و ثنای تست</p></div>
<div class="m2"><p>. . . غدیر باد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ور صوت این غزل نه روان بخش عز تست</p></div>
<div class="m2"><p>صوت روان زهره چو عشر عشیر باد</p></div></div>