---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>مهره عمرم ربود شعبده آسمان</p></div>
<div class="m2"><p>کشت چراغ دلم شمع سپهرالامان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر پایم گداخت سفره خاکی چو شمع</p></div>
<div class="m2"><p>با سر دستم فگند تیر فلک چون کمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرد بود همچو صبح بزم حریفان غم</p></div>
<div class="m2"><p>گر ننهندم چو شمع شب همه شب در میان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خصم خودم زانکه چرخ گر کندم بر درخت</p></div>
<div class="m2"><p>سر بدر آرد چو شمع از دهنم ریسمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع دل کس نیم پس چه سبب همچو شمع</p></div>
<div class="m2"><p>مرده نفس می زنم بر لب این خاکدان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوختم از خود چنانک می شوم از باد صبح</p></div>
<div class="m2"><p>همچو سر شمع از آب عاجز و فریاد خوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روشنی کار من جز پی محنت مبین</p></div>
<div class="m2"><p>راستی قد شمع جز پی سوزش مدان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دهر مرا کمچو شمع بی گنه آویخته است</p></div>
<div class="m2"><p>گر بفروشد بلاست ور بگدازد هوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه به تن فربهم کم نخورم خون که شمع</p></div>
<div class="m2"><p>از بهی و فربهی بیش بود بر زیان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از در این شش جهات چون روم اکنون که کرد</p></div>
<div class="m2"><p>پای به بندم چو شمع گردش این هفتخوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زنده شوم همچو شمع از پس مردن چو هست</p></div>
<div class="m2"><p>مستع سحر من صاحب هفتم قران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کهف امم فخر دین زنگی خضر آستین</p></div>
<div class="m2"><p>قطب و دل شمع شرع موسی طور آستان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خسرو سلطان جناب کز حسد او چو شمع</p></div>
<div class="m2"><p>صد رهه بر خود گریست عالم نامهربان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فتنه به حاجب چه خواست غیبتش از صدر ملک؟</p></div>
<div class="m2"><p>زانکه بود شمع دزد خواب خوش پاسبان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ظلم که بنشسته بود تو بر تو همچو شمع</p></div>
<div class="m2"><p>با تف شمشیر او سوخت ز سر تا میان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از لب خویش آسمان دندان سازد چو شمع</p></div>
<div class="m2"><p>تا همه ساید بر آنک تافت ز حکمش عنان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غم ز چه گیرد به کار خصم ورا شمع وار</p></div>
<div class="m2"><p>زانکه چو زر ریاست بر محکم امتحان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برد چو شمع از میان ظلمت ظلم ای عجب</p></div>
<div class="m2"><p>قدرت قدرش که هست در ره دین قهرمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای به تو ناحق چو شمع دیده به طفلی عذاب</p></div>
<div class="m2"><p>وی ز تو دولت چو سرو گشته به پیری جوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هست چو شمع به روز جرم عطارد زرشگ</p></div>
<div class="m2"><p>تا گه توقیع دید کلک تو اندر بنان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ساخت به کردار شمع در ره عشقت مجیر</p></div>
<div class="m2"><p>هم ز دل، آتشکده هم ز دو رخ زعفران</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از سر اعجاز طبع، شمع صفت در ثنات</p></div>
<div class="m2"><p>ز آتش خاطر نمود چشمه آب روان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خاطر او آتشست گر چه درو طعنه زد</p></div>
<div class="m2"><p>آنکه هنوزش چو شمع می دود آب از دهان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز آتش غم جست باز همچو براتی شمع</p></div>
<div class="m2"><p>یا به خودش در پذیر یا ز خودش وارهان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا که به شب هست شمع محرم اسرار خلق</p></div>
<div class="m2"><p>بر دل پاک تو باد سر الهی عیان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شمع جلال ترا باد به نیک اختری</p></div>
<div class="m2"><p>پرتوش از باختر تافته تا قیروان</p></div></div>