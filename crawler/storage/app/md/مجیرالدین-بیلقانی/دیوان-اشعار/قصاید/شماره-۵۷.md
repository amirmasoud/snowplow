---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>تا دار ملک زان سوی عالم بساختم</p></div>
<div class="m2"><p>خود را ز کاینات مسلم بساختم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر چنبر جهان گذرم بود ازین سبب</p></div>
<div class="m2"><p>تن چون رسن نزار و پر از خم بساختم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدم که زخم حادثه مرهم پذیر نیست</p></div>
<div class="m2"><p>با زخم بی حمایت مرهم بساختم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دل شکستم آرزوی جام می چنانک</p></div>
<div class="m2"><p>صد جام جم به طبع بیکدم بساختم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر من جهان چو حلقه خاتم شدست از آنک</p></div>
<div class="m2"><p>ملکی برون ز مملکت جم بساختم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدم که ملک فقر من از ملک جم بهست</p></div>
<div class="m2"><p>زر وام کردم از رخ و خاتم بساختم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر مصاف حادثه از صبح و ماه نو</p></div>
<div class="m2"><p>با نعل زر جنیبت ادهم بساختم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همدم نبود مردمک چشم را از آن</p></div>
<div class="m2"><p>در دیده جای کردم و همدم بساختم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در چشم من ببین که ز بهر سریر اوست</p></div>
<div class="m2"><p>این عاج و آبنوس که در هم بساختم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من همچو چنگ با دل سرگشته چون رباب</p></div>
<div class="m2"><p>هم زیر راست کردم و هم بم بساختم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از بس نوای غم که من و دل بهم زدیم</p></div>
<div class="m2"><p>صد زخمه بیش سوخت ولی هم بساختم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>داروی دلخوشی به عدم باز رفته به</p></div>
<div class="m2"><p>اکنون که من مفرحی از غم بساختم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زیرا که همچو گنبد گل بی بقا نمود</p></div>
<div class="m2"><p>کاری که زیر گنبد اعظم بساختم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چندان سرشگ دیده فشردم به دم کزو</p></div>
<div class="m2"><p>عقدی برای گردن عالم بساختم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون با سه شش مجیر ز ایام بیش ماند</p></div>
<div class="m2"><p>در بر دو یک نهادم و با کم بساختم</p></div></div>