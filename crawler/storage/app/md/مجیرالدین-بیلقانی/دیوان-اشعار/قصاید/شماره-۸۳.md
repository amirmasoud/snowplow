---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>زهی با حسن تو همدم صفا و لطف روحانی</p></div>
<div class="m2"><p>به زلف ار سایه خضری به لب چون آب حیوانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه و خورشید پیشانی کنند از روی بی شرمی</p></div>
<div class="m2"><p>اگر پیش رخت بر خاک ره ننهند پیشانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم سوزی و این معنی رگی با جان همی دارد</p></div>
<div class="m2"><p>مکن این ناخوشی با جان که صد ره خوشتر از جانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه پرسم از تو حال دل چو می دانی که می دانم</p></div>
<div class="m2"><p>غم دل با تو چون گویم چو می دانم که می دانی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نداری زلف را بر گوش اگر به گوش واداری</p></div>
<div class="m2"><p>کز آن شکل پریشان هست جمعی را پریشانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان مصرست و تو یوسف چهی پر آب زیر لب</p></div>
<div class="m2"><p>عزیزان جهان را دل در آن چاه است زندانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو در کار دالم سستی و من در سختی افتاده</p></div>
<div class="m2"><p>اگر سخت آیدت ور نه به غایت سست پیمانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو خواهی یار و خواهی نه سعادت پادشاهی را</p></div>
<div class="m2"><p>که صبح دین و دولت هر دو زو گشتند نورانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شه اقلیم بخش اعظم اتابک کز جلال او</p></div>
<div class="m2"><p>نهادش نام در اول سپهر اسکندر ثانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پناه دوده سلجوق کز تأثیر انصافش</p></div>
<div class="m2"><p>شود همخوابه میش ازامن با گرگ بیابانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فلک سیر ملک سیرت که بر درگاه او زیبد</p></div>
<div class="m2"><p>ملک را آستان بوسی فلک را بنده فرمانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رکاب آسمان سایش به هر جانب که روی آرد</p></div>
<div class="m2"><p>بگیرد هر دو فتراکش به رغبت لطف ربانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رکابش را دو در می دان که او خود شکل در دارد</p></div>
<div class="m2"><p>یکی در در رضای حق دوم در فضل یزدانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ایا گردون عنان شاهی که ریزد آسمان جوجو</p></div>
<div class="m2"><p>اگر یک لحظه از کارش عنان دل بگردانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو موسی دست تا هستی شبان اطراف عالم را</p></div>
<div class="m2"><p>نیارد گرگ پروردن سپهر سبز بارانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه گویم خصم سگ روی تو چون بیند که هر ساعت</p></div>
<div class="m2"><p>ز شمیر تو شیرافگن سگانرا هست مهمانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عنان تو چو زنجیرست و خصم تو چو دیوانه</p></div>
<div class="m2"><p>بجنبد جانش اندر تن چو زنجیرش بجنبانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سخن را پشت و رو نبود اگر گویم درین دوران</p></div>
<div class="m2"><p>تویی روی جهانداری تویی پشت مسلمانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جهان چون تیر از آن شد راست کز خون جهان شوران</p></div>
<div class="m2"><p>سر پیکان تو لعل است همچون لعل پیکانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه نور الهی بر تو شد پیدا ازین معنی</p></div>
<div class="m2"><p>که با آن حضرت قدسی ترا سری است پنهانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نشستی ظلم در عالم چو اضطراب تو بر تو</p></div>
<div class="m2"><p>اگر تیغت نبودی زیر این گرون پنگانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر آن کاری که نتواند فلک ترتیب آن کردن</p></div>
<div class="m2"><p>تو کن ترتیب آن زیرا تو بتوانی که بتوانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جهانگیر و خدا ترسی و مقبل پس روا باشد</p></div>
<div class="m2"><p>اگر گویم که هم جم هم سکندر هم سلیمانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو خورشیدی و بی حکمت نماند جای در گیتی</p></div>
<div class="m2"><p>که بی خورشید جایی نیست در آباد و ویرانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به منبر کی رود هرگز سری کان نیست منقادت</p></div>
<div class="m2"><p>شکاری کی تواند شد سگی کان هست کهدانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سلامت روی در دزدد اگر تو سعی واگیری</p></div>
<div class="m2"><p>جهان از پیش برخیزد اگر تو فتنه ننشانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مسلم شد کز آن ایران و تورانت مسلم شد</p></div>
<div class="m2"><p>که تو با فر افریدون و با قدر قدر خانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جهانبخش و جهانگیری زهی قدر و زهی قدرت</p></div>
<div class="m2"><p>که در یک روز اگر خواهی جهان بدهی و بستانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برآرد حمله گرم تو دود از آب و از آتش</p></div>
<div class="m2"><p>چو تیغ آب رنگت کرد عزم آتش افشانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>روان طغرل و مسعود شادند از تو زین معنی</p></div>
<div class="m2"><p>که سلطان ارسلان را شد مسلم از تو سلطانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چه گویم من ز فر دولت و اقبال تو شاها</p></div>
<div class="m2"><p>که هرچ آن آید اندرو هم من صد بار چندانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>درخت دولتت دارد دو شاخ تازه کز هر دو</p></div>
<div class="m2"><p>به اقبال تو پیدا گشت آثار جهانبانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی در عقد پیروزی فزون از در دریایی</p></div>
<div class="m2"><p>یکی در تاج بهروزی به از یاقوت رمانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دو ماهند اندرین برج و دو سروند اندرین بستان</p></div>
<div class="m2"><p>که رشگ ماه چرخند و ستیز سرو بستانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همی تا طره پیرایی نمایند از ره صنعت</p></div>
<div class="m2"><p>عروسان چمن را هم صبا هم ابر نیسانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همی تا گل ندارد تاب پیش باد خوارزمی</p></div>
<div class="m2"><p>همی تا نور گیرد مه ز خورشید خراسانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جهانت باد محکوم و سپهرت باد در فرمان</p></div>
<div class="m2"><p>سلیمان وار حکمت را متابع انسی و جانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رفیقت طالع میمون به هر جانب که روی آری</p></div>
<div class="m2"><p>معینت ایزد بیچون به هر جایی که درمانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر چه عمر کس باقی نخواهد بود در عالم</p></div>
<div class="m2"><p>کرامت عمر باقی بادت اندر عالم فانی</p></div></div>