---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>نه دل ز یار شکیبد نه می بسازد یار</p></div>
<div class="m2"><p>به غم فرو نشوم گر به سر برآید کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سر گذشت مرا آب و صبر می گوید</p></div>
<div class="m2"><p>که جان بپای بری یا شوی ز سر بیزار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چگونه از در دل در شوم که دستم گیر</p></div>
<div class="m2"><p>که زد ز عجز دلم پشت دست بر دیوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا چو جرعه اگر خون دل بریزد دوست</p></div>
<div class="m2"><p>چو جرعه خاک ببوسم به پیش او ناچار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصال او نکنم طمع از آنکه می دانم</p></div>
<div class="m2"><p>که عاشقان نشوند از زمانه برخوردار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خار عشق ویم گر چه بهر دشمن و دوست</p></div>
<div class="m2"><p>چو گل به خنده خونین درم شبی صد بار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن هوا که همای وصال او نپرید</p></div>
<div class="m2"><p>عقاب فتنه در آن ناحیت گرفت قرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه شوخ دیده کس است که او شاخ عشوه او</p></div>
<div class="m2"><p>بجز شکوفه بی دولتی نیارد بار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کدام لب که ازو بوی جان نمی آید</p></div>
<div class="m2"><p>ز بس که جان به لب آورد دست فرقت یار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رسید کوکبه سعد بر جنیبت گل</p></div>
<div class="m2"><p>مثال داد جهان را به فر و عدل بهار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنفشه را گل سوری مگر به خرده گرفت</p></div>
<div class="m2"><p>که مانده بر سر یک پای بهر استغفار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو دید سبزه که گل پای در رکاب آورد</p></div>
<div class="m2"><p>کشید نیمچه یعنی که خسروست سوار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسوخت خون دل خویش لاله تا دانست</p></div>
<div class="m2"><p>که در جهانش بقا اندکست و غم بسیار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جهان به نرگس تر گفت شوخ چشم کسی</p></div>
<div class="m2"><p>به خنده گفت ولیکن نه چون تو بی زنهار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زبان سوسن آزاد گنگ ماند چو دید</p></div>
<div class="m2"><p>که با حریف جهان خامشی به از گفتار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به سمع لاله رسید آنکه غنچه پیکان ساخت</p></div>
<div class="m2"><p>دلش چو سینه من گشت از نهیب فگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عروس سبزه چو در جلوه شد به مجلس گل</p></div>
<div class="m2"><p>ز سیم خام طبقها شکوفه کرد نثار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به باغ بلبل ازین پس ز بهر فتوی عیش</p></div>
<div class="m2"><p>ثنای خسرو عالی نسب کند تکرار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپهر قطب معالی روان قالب عقل</p></div>
<div class="m2"><p>مسیح ملت ملک اختر سپهر تبار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>محمد بن روادی که باز مرتبتش</p></div>
<div class="m2"><p>بر آشیانه روحانیان گرفت قرار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کلید گنج هنر کاتش بلارک او</p></div>
<div class="m2"><p>درون معرکه هست اژدهای جان او بار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو تیر چار پرش سر برد به حلق عدو</p></div>
<div class="m2"><p>سه روح خصم برون آید از ره سوفار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز رشگ حمله گرمش سلاح دار سپهر</p></div>
<div class="m2"><p>به جای تیغ ببستست بر میان زنار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در آن زمان که شود روی طارم ازرق</p></div>
<div class="m2"><p>ز گرد اسب یلان تیره در صف پیکار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز بیم ناوک گردان زمانه را بینی</p></div>
<div class="m2"><p>کشیده سر به تن تیره در کشف کردار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جهان به حیله دم اندر کشیده چون نقطه</p></div>
<div class="m2"><p>اجل به کینه دهن باز کرده چون پرگار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شود ز خون یلان همچو پای کبک دری</p></div>
<div class="m2"><p>میان معرکه سیمرغ مرگ را منقار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تبارک الله کان روز خسرو عادل</p></div>
<div class="m2"><p>چگونه زود بر آرد ز جان خصم دمار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ظفر گرفته عنانش ندا کند در صف</p></div>
<div class="m2"><p>زهی مظفر پیروز بخت خصم شکار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در آن مهم که میان دو صف پدید آید</p></div>
<div class="m2"><p>یقین بدانکه سر تیغ اوست کارگزار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>حدیث اوست کنون در کتابخانه چرخ</p></div>
<div class="m2"><p>حدیث رستم دستان به کلبه عطار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز گرز او کند ایام شربتی شافی</p></div>
<div class="m2"><p>هر آنگهی که شود شخص مملکت بیمار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پریر بود که در هم شکست چون دفتر</p></div>
<div class="m2"><p>صفی به حیله و فن راست کرده چون طومار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به لوری از هنر او و لور گندی خصم</p></div>
<div class="m2"><p>نبات خون آلودست و ابر طوفان بار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سپهر حقه صفت شد زمانه حلقه بگوش</p></div>
<div class="m2"><p>که تا کند چو زمانه به بندگیش اقرار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز بیم بخشش او عالم ستم پیشه</p></div>
<div class="m2"><p>نهفت زر و گهر در دل جبال و بحار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جهان پناها! من عاجزم ز مدحت تو</p></div>
<div class="m2"><p>که هست بر دلم از مکر حاسدان آزار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به بارگاه تو از بنده نقلها کردند</p></div>
<div class="m2"><p>کزان نشست بر اطراف خاطر تو غبار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدان خدای که بالای خاک در شش روز</p></div>
<div class="m2"><p>ببافت قدرت او هفت پرده از زنگار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به صنع او که ببست از پی صلاح ابد</p></div>
<div class="m2"><p>دریچه های فلک را به آتشین مسمار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به امر او که ز کاف و ز نون پدید آورد</p></div>
<div class="m2"><p>بسیط خاکی و اشکال گنبد دوار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به لطف قبه اعظم به قدر عرش مجید</p></div>
<div class="m2"><p>به حسن و زینت جنت به قهر و سطوت نار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به جاه و جای ملایک به قرب روح قدس</p></div>
<div class="m2"><p>به حرمت شب قدر و به حق روز شمار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به امر و نهی و به وعد و وعید مصحف مجد</p></div>
<div class="m2"><p>که هست فاتحه اش گنج نامه اسرار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به حق صفوت آدم که از نتیجه اوست</p></div>
<div class="m2"><p>درین نشیمن خاک از وجود خلق آثار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به رتبت نفس پاک عیسی مریم</p></div>
<div class="m2"><p>به معجز سخن خوب احمد مختار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به صدق یوسف مصری و بی گناهی گرگ</p></div>
<div class="m2"><p>به نغمه خوش داود و لحن موسیقار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به عابدان که جهان را نکرده اند قبول</p></div>
<div class="m2"><p>به عارفان که صفا را نکرده اند انکار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به عدل و عفت بوبکر و عمر خطاب</p></div>
<div class="m2"><p>به شرم و صولت عثمان و حیدر کرار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به جود حاتم طائی و حلم احنف قیس</p></div>
<div class="m2"><p>به زهد بوذر و تقوی جعفر طیار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به خاک تیره شمایل به نار نور نمای</p></div>
<div class="m2"><p>به باد نادره صنعت به آب نوشگوار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به تیغ فتنه نشان تو کز مهابت اوست</p></div>
<div class="m2"><p>که از نهال حوادث نه بیخ ماند و نه بار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به دولت تو که سیارگان هفت سپهر</p></div>
<div class="m2"><p>برو سعادت و تأیید کرده اند ایثار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به نعمت تو که هستند اسیر منت او</p></div>
<div class="m2"><p>مجاوران جناب سپهر آینه دار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به هیبت تو که آتش کند ز چشمه آب</p></div>
<div class="m2"><p>به رحمت تو که سوسن دهد ز سینه خار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به ساغرت که ازو آب کوثرست خجل</p></div>
<div class="m2"><p>به مرکبت که برو سعد اکبرست سوار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به حزم و عزم رکاب و عنان فرخ تو</p></div>
<div class="m2"><p>که روزگار مسیرند و آفتاب مدار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به مجلس تو که ناهید را ز هیبت اوست</p></div>
<div class="m2"><p>قدی چو چنگ دو تا و تنی چو زیر نزار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به جان پاک تو ای معدن سخا و سخن</p></div>
<div class="m2"><p>به خاک پای تو ای مرکز سکون و وقار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>که بنده تو مجیر از هر آنچه گفت حسود</p></div>
<div class="m2"><p>خبر ندارد و بر خاطرش نکرد گذر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گر آگهی است ورا زین سخن بدان که شدست</p></div>
<div class="m2"><p>ز لطف و رحمت پروردگار حق بیزار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>و گر بخفت شبی بر خلاف دولت تو</p></div>
<div class="m2"><p>مباد دیده اقبال و بخت او بیدار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سپهر قد را! امروز شعر مدح ترا</p></div>
<div class="m2"><p>به فر طبع من از جرم مشتری است شعار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>میان عقد کند زان گهر عروس بهشت</p></div>
<div class="m2"><p>که بحر خاطر پاک من افگند به کنار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>سخنوری چو من الحق به حضرت تو سزد</p></div>
<div class="m2"><p>از آنک نغمه بلبل خوش آید از گلزار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نو آفریده ام از دل شعار مدحت تو</p></div>
<div class="m2"><p>که هست کار من این طرز تازه در اشعار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دعا به آخر مدح تو زان نمی گویم</p></div>
<div class="m2"><p>که مهر خاتم قرآن نشاید استغفار</p></div></div>