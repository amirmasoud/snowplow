---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>چون ترا غالیه بر گرد رقم ریخته اند</p></div>
<div class="m2"><p>بر زر روی من از اشگ درم ریخته اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا رقم زد خط رخسار ترا کاتب صنع</p></div>
<div class="m2"><p>عاشقان روح بر آن شکل رقم ریخته اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیل خط و بقم روی تو در دور سرشگ</p></div>
<div class="m2"><p>از دل و دیده من نیل و بقم ریخته اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گه گریه رود جزع عقیقی گهرم</p></div>
<div class="m2"><p>گهر ناب ز سر تا به قدم ریخته اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایمن اندر الم و داغ امان تو که جست؟</p></div>
<div class="m2"><p>ایمنی بر سر آن داغ و الم ریخته اند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه سگم من که کنم آهوی وصل تو شکار</p></div>
<div class="m2"><p>که بسی شیر در آن دام تو دم ریخته اند</p></div></div>