---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>ای ز لبت لاله را آب خوشی در دهان</p></div>
<div class="m2"><p>آتش روی تو بس آینه عقل و جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشت ز عکس رخت سینه خاک آینه</p></div>
<div class="m2"><p>شد ز خیال لبت چشم فلک گلستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر تو چون آینه، دل شده ام جمله تن</p></div>
<div class="m2"><p>زانکه نشاید نهاد با تو دلی در میان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آینه خواه و ببین زلف و لب ار بایدت</p></div>
<div class="m2"><p>هندوی آتش نشین طوطی شکرفشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جعد تو رسمی نوست بر رخ چون آفتاب</p></div>
<div class="m2"><p>زانکه کس از شب نکرد آینه را سایبان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساده چو آیینه ای، سوده چو خاکسترم</p></div>
<div class="m2"><p>چون ز منی تازه روی رو مکنم هان و هان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با تو چو یک رو شدم بر صفت آینه</p></div>
<div class="m2"><p>پس تو چو شانه مباش با چو منی صد زبان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در غمت ار خون خورم آه نکنم در رخت</p></div>
<div class="m2"><p>زانکه تو دانی کز آه آینه بیند زیان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست بدستت فگند، حسن تو چون آینه</p></div>
<div class="m2"><p>پای بپایم سپرد، عشق تو چون آستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به که خیال تو هست ساخته با چشم من</p></div>
<div class="m2"><p>کاینه با آبنوس ساخته به بی گمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان به کفم تا کنم بر تو به عیدی نثار</p></div>
<div class="m2"><p>کاینه دیدن به عید خوش نشود رایگان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غالیه دل تویی زان رخ چون آینه</p></div>
<div class="m2"><p>قافله سالار شرع مفتی صاحبقران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حاکم حیدر قضا عالم جم مرتبه</p></div>
<div class="m2"><p>آینه جان و عقل عاقله انس و جان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>افضل عیسی نفس کاینه آسا به نطق</p></div>
<div class="m2"><p>کشف همه مشکلات کرده ز گیتی ضمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دهر که بد عیب جوی بر صف آینه</p></div>
<div class="m2"><p>راست چو مقراض بست در ره حکمش میان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>محرم دست سیاه ذات وی آمد نه خصم</p></div>
<div class="m2"><p>زانکه به دست سیاه آینه شد داستان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فتنه که چون شانه برد موی به شوخی ز سر</p></div>
<div class="m2"><p>همچو از آیینه کور کرد ز صدرش کران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خصم ورا طمطراق کس نکند بی سبب</p></div>
<div class="m2"><p>آینه را دست زر بر ندهد بی بیان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آینه بوسی نهاد بر کف او لاجرم</p></div>
<div class="m2"><p>ساخت ز بیجاده طوق یافت ز زر طیلسان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نیز نبیند سه روح مثل وی از چار طبع</p></div>
<div class="m2"><p>خود نبود در دو کون آینه از استخوان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صدمه حکمش شکست شیشه آن طایفه</p></div>
<div class="m2"><p>کاینه آسا بدند دم خور و رشوت ستان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خاک در اوست صبح ورنه ربودی فلک</p></div>
<div class="m2"><p>آینه ش از روی دست ابلقش از زیر ران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا کف او آینه است یعنی بدهد ز دست</p></div>
<div class="m2"><p>هر تر و خشکی که هست در شکم بحر و کان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تنگ بود با ثناش نه ورق چرخ از آنک</p></div>
<div class="m2"><p>کس به یکی آینه بر نکشد هفتخوان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هست دلش جام جم آینه نامش مکن</p></div>
<div class="m2"><p>زانکه به زخمه به است باربد از پاسبان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از در او کن طلب منهج حق زان سبب</p></div>
<div class="m2"><p>کاینه از چین نکوست عود ز هندوستان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای دلت از نه فلک ساخته نیم آینه</p></div>
<div class="m2"><p>وی ز دلت هشت خلد یافته صد میزبان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آینه چون مرد را باز نماید به مرد</p></div>
<div class="m2"><p>دهر دو رو را بدو باز نمودی چنان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عالم شش گوشه راست قهر تو کاری عظیم</p></div>
<div class="m2"><p>معجب یک چشم راست آینه باری گران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خصم تو چون آینه دارد روی آهنین</p></div>
<div class="m2"><p>گر به بزرگی کند ذات ترا امتحان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خاک تو یعنی مجیر سوخت دل از زنگ غم</p></div>
<div class="m2"><p>کز صفت آینه خاطرش آمد به جان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کرد لب دل کبود آینه آسا ز تب</p></div>
<div class="m2"><p>پس به همین نیشکر یافت از آن تب امان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آینه گون قرص خور دید که ننهاد کس</p></div>
<div class="m2"><p>تازه چنین تره ای بر سر این تیره خوان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر چه سخنور بسی است فرق تو کن زان سبب</p></div>
<div class="m2"><p>کاینه و کفچه اند پیش خرد این و آن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سخره هر عامه را هم سخن من منه</p></div>
<div class="m2"><p>چنبر دف را به طنز آینه چین مخوان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا که بود خشک مغز پیش خرد آینه</p></div>
<div class="m2"><p>خصم تو تر دیده باد در صف ذل و هوان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خاطر تو کز صفاش آینه خاکسترست</p></div>
<div class="m2"><p>باد ز زنگ فنا تا به ابد بی نشان</p></div></div>