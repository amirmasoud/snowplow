---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>سیاهی می کند با من سر زلف نگونسارش</p></div>
<div class="m2"><p>به لب می آورد جانم لب لعل شکربارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا خاری نهاد از هجر خویش آن یار همچون گل</p></div>
<div class="m2"><p>که در پای دل سرگشته دایم می خلد خارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شوخی پاره ای کارست در راه غمش ما را</p></div>
<div class="m2"><p>اگر دم سرد شد شاید که دل گرم است در کارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بار عشق او عاجز شدن تر دامنی باشد</p></div>
<div class="m2"><p>چو بنهادم دل از اول سزای خنگ دربارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز تیمار سر زلفش بجان آمد دل تنگم</p></div>
<div class="m2"><p>که از روی وفاداری نمی دارند تیمارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برو جان عرضه کردن نیست الا عین درویشی</p></div>
<div class="m2"><p>چو می بینم که جان با خاک یکسان شد به بازارش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر زنار در بندد دلم بی او عجب نبود</p></div>
<div class="m2"><p>کنون کز مشگ پیدا گشت بر گلبرگ زنارش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز من خون می خورد چشمش ولی چون بینمش گویم</p></div>
<div class="m2"><p>نکو چشمی است این، یارب! ز چشم بد نگهدارش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی کردم فغان زین بیش چون از سر گذشت آبم</p></div>
<div class="m2"><p>من و المستغاث اکنون ز جزع و لعل خونخوارش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دهان او به شکل نیم دینار و هزاران دل</p></div>
<div class="m2"><p>خرید و کم نشد یک جو ز شکل نیم دینارش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیم من مرد ناز او که با این چاره سازیها</p></div>
<div class="m2"><p>دلم چون خر به گل درماند از ناز بخروارش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غلط گفتم که باشد دل که من دارم دریغ از وی؟</p></div>
<div class="m2"><p>که گر جان جوید از من هم نخواهم جستن آغازش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کبوتر وار بر پرد دلم از سینه پر غم</p></div>
<div class="m2"><p>چو بینم همچو کبک نر خرامان وقت رفتارش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غلام زلف چون هندوی آن ترکم که هر ساعت</p></div>
<div class="m2"><p>برآید ناله صد بی گناه از زیر هر تارش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو من دیدار او بینم زبانم بی من این گوید</p></div>
<div class="m2"><p>که شادیها به روی آنک من شادم به دیدارش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به چشم و غمزه جادو جهان بر خلق بفروشد</p></div>
<div class="m2"><p>بخوبی گر بود روزی شه عالم خریدارش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جهانبخش ملک پرور جهاندار ملک سیرت</p></div>
<div class="m2"><p>که دارد لطف ربانی جهانبخش و جهاندارش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گل بستان دولت نصرة الدین پهلوان کایزد</p></div>
<div class="m2"><p>فزون کرد از همه شاهان عالم جاه و مقدارش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نصیرالحق والمله خداوندی فلک قدری</p></div>
<div class="m2"><p>که حاصل شد به اندک سال دولتهای بسیارش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>درختی گشت ذات او بنامیزد بنامیزد</p></div>
<div class="m2"><p>که برگش نصرت و فتحست وتأیید و ظفر، بارش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زمام دولت باقی به دست او از آن آمد</p></div>
<div class="m2"><p>که لطف حق به صد چندین همی بیند سزاوارش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رود در آتش دوزخ کسی کو رفت در کینش</p></div>
<div class="m2"><p>بود در زینها حق کسی کآمد به زنهارش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سرای عالم خاکی که سقفش آسمان آمد</p></div>
<div class="m2"><p>از آن معمور می ماند که رأی اوست معمارش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر پرسد کسی از تو که در شش گوشه گیتی</p></div>
<div class="m2"><p>حوادث از که شد خفته؟ بگو از بخت بیدارش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کسی کز دفتر عصیان او یک سطر برخواند</p></div>
<div class="m2"><p>سیه رویی شود حاصل به آخر همچو طومارش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چه سود ار خصم او از شربت تیغش بپرهیزد</p></div>
<div class="m2"><p>که گر خواهد و گرنی هم بباید خورد ناچارش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر خواهی که صد کیخسرو اندر یک قبا بینی</p></div>
<div class="m2"><p>به پیروزی ببین بنشسته اندر صفه بارش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>و گر خواهی که در زینی هزاران روستم یابی</p></div>
<div class="m2"><p>به میدان درگه جولان ببین بر خنگ رهوراش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو از وی کار دین نیکست و چشم مملکت روشن</p></div>
<div class="m2"><p>خداوندا! نگهداری ز زخم چشم اغیارش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تعالی الله چه دولتیار شخصست او که در عالم</p></div>
<div class="m2"><p>به هر کاری که روی آرد در آن دولت بود یارش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گه توقیع چون در دست گیرد کلک میمون را</p></div>
<div class="m2"><p>مزاج مملکت گردد درست از کلک به بارش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز گفتارش جهان را هر زمان باشد شکر ریزی</p></div>
<div class="m2"><p>هزارن جان خوش چون جان من برخی گفتارش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بهر جایی که باشد گنج اگر ماری وطن گیرد</p></div>
<div class="m2"><p>ظفر گنج است و تیغ خسرو عالم ستان مارش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ببارد صاعقه بر خصم بد گوهر گه هیجا</p></div>
<div class="m2"><p>چو خندد تیغ پرگوهر به دست ابر کردارش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سپهسالاری اسلام از آن بر وی مقرر شد</p></div>
<div class="m2"><p>که نصرت یار باشد هر سپه را کوست سالارش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یقین می دان که از سلطان نشانست او که هر ساعت</p></div>
<div class="m2"><p>رسد فریاد ملک و دین سر تیغ گهردارش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هر آنکس کو ببیند روی نورانی او داند</p></div>
<div class="m2"><p>که نور شفقت و رحمت همی تابد ز رخسارش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نگون شد رایت بدعت و لیک از زخم شمشیرش</p></div>
<div class="m2"><p>فزون شد رونق سنت ولیک از رأی هشیارش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ارگ خصمش ز رشگ دولت او خون نمی گرید</p></div>
<div class="m2"><p>بدین معنی گرانی می کند خصم سبکسارش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>و گر شد دشمنش فربه ز نعمت، هم روا باشد</p></div>
<div class="m2"><p>که گردون از پی کشتن همی دارد به پروارش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر دم بر خلافش بر لب آرد مشتری روزی</p></div>
<div class="m2"><p>طناب گردنش سازد سپهر از پیچ دستارش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جهان کردست اقراری که او شاهیست دین پرور</p></div>
<div class="m2"><p>گواهی می دهد امروز ملک و دین به اقرارش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>پناه آل سلجوقش همی خواندم خرد گفتا</p></div>
<div class="m2"><p>چه می خونی بدان لفظی که خواندی پار و پیرارش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بگیرد ملک اسکندر چو اسکندر پدر دارد</p></div>
<div class="m2"><p>که بادا عمر جاویدان به ملک اندر خضر وارش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جوانی پیر عقل است این و آن پیر جوان دولت</p></div>
<div class="m2"><p>که بادا سایه آن پیر بر سر مانده هموارش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>رسید از راه دور امروز عید و گل به بزم شه</p></div>
<div class="m2"><p>بدان تا خوش کند از عیش بزم همچو گلزارش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به عید اکنون به کار آید شراب صرف گلرنگش</p></div>
<div class="m2"><p>به باغ اکنون به بار آید گل صد برگ گلنارش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جهان از عید خرم گشت و باغ از گل به سامان شد</p></div>
<div class="m2"><p>کنون ما و می و گلبرگ و زیر و ناله زارش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>میی چون چه؟ چنانک از لطف دلجویی کند روحش</p></div>
<div class="m2"><p>گلی چون چه؟ چنانک آید به حسرت مشک تاتارش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز عید و گل ممتع گشت برخوردار و خرم دل</p></div>
<div class="m2"><p>شهی کاندر همه عالم پسندیدست آثارش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مبارک باد عید فطر و ختم روزه بر جانش</p></div>
<div class="m2"><p>ازو پذرفته طاعتهای با اخلاص دادارش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>پدر از روی او خرم سپهر از قدر او عاجر</p></div>
<div class="m2"><p>موافق گشته در هر کار دور چرخ دوراش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خدایا چون تو دانی کوستم بر خلق نپسندد</p></div>
<div class="m2"><p>مسلم داری از دوران گردون ستمکارش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چنان کو بندگانت را به شفقت نیک می دارد</p></div>
<div class="m2"><p>بیفزا دولت باقی و در دولت نگهدارش</p></div></div>