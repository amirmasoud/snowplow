---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>شش جهت ملک را کار یکی در ده است</p></div>
<div class="m2"><p>کز پس هفتم قران ملک به دست شه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مادر هفت آسمان گر چه همه فتنه زاد</p></div>
<div class="m2"><p>تا به مراد دلش حامله شد نه مه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شه ره کون و فساد پاک شد از حادثات</p></div>
<div class="m2"><p>یعنی از انصاف شاه بدرقه ای بر ره است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرز گرانسنگ او مغز عدو سرمه کرد</p></div>
<div class="m2"><p>دان که ازین ماجرا دیده ملک آگه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همت یوسف لقاش هست بر تبت چنانک</p></div>
<div class="m2"><p>وقت نظر پیش او دلو فلک در چه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شخص عدو علتی است داروی او تیغ شاه</p></div>
<div class="m2"><p>هم بخورد بی خلاف گر چه در آن مکره است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای شه کسری عطا خسرو گردون رکاب</p></div>
<div class="m2"><p>کز کف تو آفتاب همچو هلال از مه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خصم ترا روزگار گر چه فریبی دهد</p></div>
<div class="m2"><p>می سزد ای شیر دل کان سگ و این روبه است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفت سائل شمار زر که نه در دست تست</p></div>
<div class="m2"><p>حسرت خربان شمار خر که نه در بنگه است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دشمنت ار با هنر نیست مگر یار غار</p></div>
<div class="m2"><p>پیش تو چون عنکبوت وقت سخن جوله است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چرخ گر از خسروان به ز تو عاقل شناخت</p></div>
<div class="m2"><p>با همه کار آگهی چرخ هنوز ابله است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نطق به یادت نزد سوسن از آن الکن است</p></div>
<div class="m2"><p>سرمه ز خاکت نساخت نرگس از آن اکمه است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عیسی عهدی به دم موسی هرون نسب</p></div>
<div class="m2"><p>هم ید بیضا ترا هم دم روح الله است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مصلحت است آب و نار در سر تیغ تو زانک</p></div>
<div class="m2"><p>قافه صبح را تیغ تو منزلگه است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خسرو اقلیم بخش شاه علی دل عمر</p></div>
<div class="m2"><p>آنکه جهان از دلش راست یکی از ده است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خیز پگاه و بگیر خطه خاکی چو روز</p></div>
<div class="m2"><p>کز سم شبدیز تو روز عدو بیگه است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کار ظفر راست کن چون قد مسطر به تیغ</p></div>
<div class="m2"><p>زانکه ز بس فتنه بار کژ چو سر بر مه است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گشت به شکل رباب حادثه گردن دراز</p></div>
<div class="m2"><p>هین بدهش گوشمال کز در باد افره است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ملک پناها! مرا قافیه ناگه رسید</p></div>
<div class="m2"><p>لاجرم اندر مدیح ختم سخن ناگه است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وارث عمر ابد عمر دراز تو باد</p></div>
<div class="m2"><p>زانک بر عمر تو عمر ابد کوته است</p></div></div>