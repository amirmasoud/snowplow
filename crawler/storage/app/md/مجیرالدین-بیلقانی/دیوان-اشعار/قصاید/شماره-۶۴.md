---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>قاعده ای نهاد خوش حسن تو باز در جهان</p></div>
<div class="m2"><p>عشق تو زد سه نوبه ای بر در دار ملک جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعبده لب ترا از پی دلبری فلک</p></div>
<div class="m2"><p>ماند به شکل حقه ای مهره مار در دهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تو من شکسته دل همچو پیاله در خطم</p></div>
<div class="m2"><p>زانکه چو جرعه خون من ریخت غم تو رایگان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سکه حسن تازه کرد از تو سپهر بوالعجب</p></div>
<div class="m2"><p>خیز و به ما ز قند لب چاشنیی بیار هان!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی نمکی مکن چو خاک از سر خوی آتشی</p></div>
<div class="m2"><p>کز دل ماست خوش نمک دیگ غم تو در جهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سینه مکن که روی من از تو گرفت زنگ غم</p></div>
<div class="m2"><p>یک نفسم به روی خود از تف سینه وارهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشت سیه سپید و خوش چشم من از خیال تو</p></div>
<div class="m2"><p>تا دهدم در آینه از رخ و زلف تو نشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده من ز خار غم از چه نمی شود تهی؟</p></div>
<div class="m2"><p>زانکه ز عکس روی تو چشم من است گلستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رو که ز مه نکوتری تا ز هوس همی رود</p></div>
<div class="m2"><p>سر زده و کبود لب گرد در تو آسمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم نکند به بوسه ای لعل تو تا نبیندم</p></div>
<div class="m2"><p>همچو سفال نو شده خشک لب از تو هر زمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در طلبت همی دوم چون سگ پای سوخته</p></div>
<div class="m2"><p>گر چه ز ناز هر زمان خام تری چو استخوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حلقه به گوش تو شدم چند ز چشمم افگنی</p></div>
<div class="m2"><p>بار غمم سبک بکن سر به چه می کنی گران؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>راست چو شمع وقت روز از بر من کران مکن</p></div>
<div class="m2"><p>چون دل و جان نهاده ام با تو چه شمع در میان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل به دو نیمه می کنم با تو به شکل پسته ای</p></div>
<div class="m2"><p>با من اگر شبی شود فندق تو شکر فشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رو که به عون آسمان هستی از آن رخ چو مه</p></div>
<div class="m2"><p>نایب آفتاب تو سایه حق خدایگان</p></div></div>