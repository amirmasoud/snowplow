---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>این خر جبلتان که قدم بر قدم نهند</p></div>
<div class="m2"><p>بی معنی اند و در ره معنی قدم نهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناشسته هیچ یک حدث جهل وین عجب</p></div>
<div class="m2"><p>کاغاز هر سخن ز حدوث و قدم نهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جام شکسته زیر کف پای خاطرند</p></div>
<div class="m2"><p>خود را ز خاطر ار چه همه جام جم نهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی سایه اند گر چه جهان در جهان زنند</p></div>
<div class="m2"><p>بی مایه اند گر چه درم بر درم نهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لاشی ء و شی ء در عدمند ار چه در وجود</p></div>
<div class="m2"><p>خود را نظام عقد وجود و عدم نهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیش اند و کم چو خاک ز بی آبی آنچنانک</p></div>
<div class="m2"><p>من بیشتر ز بیش و مرا کم ز کم نهند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهل قلم نه بلکه قلمدان شدند از آنک</p></div>
<div class="m2"><p>سر بر خط قلم نه بر اهل قلم نهند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نم ز آفتابه شان نشود یک نفس جدا</p></div>
<div class="m2"><p>گر طشت آتشین همه را بر شکم نهند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون مرگ سرخ و مار سیاهند کز حسد</p></div>
<div class="m2"><p>شاخ امل برند و بنای الم نهند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از پشت گاو، بدعت هر یک گذشته دان</p></div>
<div class="m2"><p>تا ساق عرش گر چه قسم بر قسم نهند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سردند و پرده در چو دم صبح و در صفا</p></div>
<div class="m2"><p>ترجیح خویش بر نفس صبحدم نهند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با من برابرند به معنی ولی چنانک</p></div>
<div class="m2"><p>شاخ خسک برابر باغ ارم نهند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ایشان و من مگوی که در شرع نیست راست</p></div>
<div class="m2"><p>مصحف در آبخانه و بت در حرم نهند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من غم برای جان خورم ایشان ز بهر نان</p></div>
<div class="m2"><p>آری هموم خلق به قدر همم نهند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دعوت گرند جمله و صاحبقران کفر</p></div>
<div class="m2"><p>تا دعوة الکواکب و قرآن بهم نهند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لا زان شدست نیم گره تا گه عطا</p></div>
<div class="m2"><p>بندی ز دست بخل ز لا بر نعم نهند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صفرند جای یافته در صدر و هم رواست</p></div>
<div class="m2"><p>کاندر حساب، صفر به جای رقم نهند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>انعام عام پرورا عمی دلند از آنک</p></div>
<div class="m2"><p>هر عامه را به مرتبه خال و عم نهند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آبستن اند چون شب و روزی نهند بار</p></div>
<div class="m2"><p>کز نفخ صور بر همه شان یار غم نهند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حسان لقب شدند و کسی در عرب نماند</p></div>
<div class="m2"><p>کاین نام بر کسی ز خسان عجم نهند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت آن غراب خو که چه مرغی است این مجیر</p></div>
<div class="m2"><p>کورا درون دایره مدح و ذم نهند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سیمرغ عزلتی است که ناسفتگان چرخ</p></div>
<div class="m2"><p>در گنج خاطرش همه در حکم نهند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ایشان کیند؟ یافه درایان که بهر صیت</p></div>
<div class="m2"><p>خود را به زور در دهن زیر و بم نهند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بوجهل سیرتان و همه بوالحکیم نام</p></div>
<div class="m2"><p>کایین چو رفت نام همه بوالحکم نهند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رستم ز رخش باز ندانند روز باک</p></div>
<div class="m2"><p>گر داغ رخش بر کتف روستم نهند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تر دامنند همچو سحاب ار چه چون سپهر</p></div>
<div class="m2"><p>بر آستین مجره به جای علم نهند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دعوی کرم کنند و کریمند اگر کرام</p></div>
<div class="m2"><p>تر دامنی نه تر سخنی از کرم نهند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بیرون شوند چون من ازین حلقه گربه سر</p></div>
<div class="m2"><p>سر بر خط خطاب امام امم نهند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شاه صفا امیر معانی که شرع و عقل</p></div>
<div class="m2"><p>اندر خلاف حکمت و شرعش حکم نهند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قطب زمانه ناصر دین کز صفای او</p></div>
<div class="m2"><p>هر صبح بار قهر ضیا بر ظلم نهند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گردون به شکل اوست نه چون او که شاخ بید</p></div>
<div class="m2"><p>نبود بقم اگر چه در آب بقم نهند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر آسمان نه اوست چرا شکل آسمان؟</p></div>
<div class="m2"><p>دلق کبود در پس پشتی بخم نهند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چندان بقاش باد که در صفه صفا</p></div>
<div class="m2"><p>گردون و خواجه خرقه نیلی بهم نهند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عمرش ز حد جذر اصم در گذشته باد</p></div>
<div class="m2"><p>تا مشکل عدو همه جذر اصم نهند</p></div></div>