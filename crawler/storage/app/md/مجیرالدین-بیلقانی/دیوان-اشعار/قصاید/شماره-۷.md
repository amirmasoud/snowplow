---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>مساز حجره وحدت درین مضیق خراب</p></div>
<div class="m2"><p>که روی صبح سلامت بماند زیر نقاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز کاینات ببر پی که بر دریچه دل</p></div>
<div class="m2"><p>تویی نخست پس آنگاه کاینات حجاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز زهر فقر طلب نوشدارو از پی آن</p></div>
<div class="m2"><p>که آب ناخوش دریاست جای در خوشاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نقد عمر قناعت بخر که نیست خطا</p></div>
<div class="m2"><p>یکی نفس به دو عالم مده که هست صواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز نوش و زهر جهان چون رهی که تعبیه است؟</p></div>
<div class="m2"><p>دوا و درد ز بهر تو در دو پر ذباب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببین به بزر قطونا که وقت خاییدن</p></div>
<div class="m2"><p>خمیر مایه زهرست و هست در جلاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر آشیان جهان خوشدلی مجوی که کس</p></div>
<div class="m2"><p>نیافت شهپر عنقا بر آشیان غراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو دل بر آتش وحدت بسوز تا نکند</p></div>
<div class="m2"><p>جهان بی نمک از گوشه دل تو کباب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکم مشو همه چون کوزه فقاع ز حرص</p></div>
<div class="m2"><p>مگر که در خور تریاقها شوی چو سداب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو تشنه ای و درین مهد خاک چون طفلان</p></div>
<div class="m2"><p>به پیش چشم تو یکسان شدست آب و سراب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو زد پلنگ شب و روزت آب وحدت جوی</p></div>
<div class="m2"><p>که زخم خورده او را گریز نیست ز آب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به طمع همنفسی جان مکن که از پی این</p></div>
<div class="m2"><p>بسی دوید و ندید آفتاب گردون تاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فلک به شکل حبابست و نیک عهدی او</p></div>
<div class="m2"><p>خوش است سخت ولی کم بقاست همچو حباب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازان چو گوی و چو دولاب خشک مغزی و تر</p></div>
<div class="m2"><p>که پای بسته ای از هفت گوی و نه دولاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنان خیال شب و روز در دل تو برست</p></div>
<div class="m2"><p>که چشم شوخ تو از روز و شب گرفت خضاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ترا به دست تو سر می برد زمانه از آن</p></div>
<div class="m2"><p>که هست پر عقاب آفت وجود عقاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز رنگ و بوی جهان صدمه فنا خوشتر</p></div>
<div class="m2"><p>که آب خوش مزه به مرد تشنه را ز گلاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فلک که کیسه بر عمر تست شب همه شب</p></div>
<div class="m2"><p>گشاده چشم به قصد تو و تو اندر خواب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به حرب تو شب دیجور، دیلمی کله است</p></div>
<div class="m2"><p>به جای حربه به دست اندرون گرفته شهاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بده عنان قناعت اگر همی خواهی</p></div>
<div class="m2"><p>که پای بوس خسیسان شوی بسان رکاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چهار طاق فلک بی طناب از آن ماندست</p></div>
<div class="m2"><p>که در گلوی تو کرد ای سلیم قلب طناب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو گردنا بشود گوشمال خورده دهر</p></div>
<div class="m2"><p>کسی که بیهده گردن کشی کند چو رباب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مخور لعاب دهن تا به نان کس چه رسد</p></div>
<div class="m2"><p>که کرم پیله بمیرد به عاقبت ز لعاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز ژنده جوی صفا کافتاب کم تابد</p></div>
<div class="m2"><p>چو ابر بر خشن آسمان زند سنجاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دل تو پر ز حسابست چو دل پنگان</p></div>
<div class="m2"><p>جهان نمای از آن نیست همچو اصطرلاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دلی که قابل اسرار لوح محفوظ است</p></div>
<div class="m2"><p>بسان تخته خاکش مساز جای حساب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به زرد و سرخ جهان تا فریفته نشوی</p></div>
<div class="m2"><p>که خون دهد عنب ار دفع خون کند عناب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عدوی تست جهان گر چه بر مصیبت تو</p></div>
<div class="m2"><p>سیاه جامه شود هر شبی به شکل مصاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز پنج رکن شریعت سپر بساز از آن</p></div>
<div class="m2"><p>که تیر چار پری آفتست در پرتاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بگیر صف قناعت به صدق اگر خواهی</p></div>
<div class="m2"><p>که شش جهات جهان پیش تو شود محراب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز موج خون جگر خستگان خاکی دان</p></div>
<div class="m2"><p>که بادبان فلک می رود چنین به شتاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مقدرا ملکا در کف ارادت تست</p></div>
<div class="m2"><p>کلید رحمت و سر رشته ثواب و عقاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مجیر حلقه بگوش جناب تست از آن</p></div>
<div class="m2"><p>که واثق است به افضال از آن رفیع جناب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز لطف تست که چون خصم نیست از پی زر</p></div>
<div class="m2"><p>کبود لب شده و تب گرفته چون سیماب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو روشن است دلش زیبد ار بدش گویند</p></div>
<div class="m2"><p>که سگ به بانگ در آید ز پرتو مهتاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سزای حضرت تو با تو چون خطاب کند؟</p></div>
<div class="m2"><p>که بی هدایت تو سر بسر خطاست خطاب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دلش سپید کن ار چند زیر چرخ کبود</p></div>
<div class="m2"><p>دل سپید چو گوگرد سرخ شد نایاب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>توقعش همه اینست کز عنایت تو</p></div>
<div class="m2"><p>رسد به بشر طوبی لهم و حسن مآب</p></div></div>