---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>زیوری از نو بدین چرخ کهن بر بسته اند</p></div>
<div class="m2"><p>گوهری شاهد برین دریای اخضر بسته اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب روان گلشن نیلوفری را همچو صبح</p></div>
<div class="m2"><p>رویها بگشاده اند این بار و زیور بسته اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شفق صد جرعه بین در طاس گردون ریخته</p></div>
<div class="m2"><p>تا ز ظلمت طره ای بر روی اختر بسته اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در افق خونریز خورشیدست و خون آلود ازوست</p></div>
<div class="m2"><p>این تتق کز باختر تا حد خاور بسته اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاغ شب بر ره هزاران بیضه زرین نهاد</p></div>
<div class="m2"><p>تا به مغرب طغرل خورشید را پر بسته اند!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیم شب خیزان مشرق را ز زر مغربی</p></div>
<div class="m2"><p>آفتاب آسا بعینه بر سر افسر بسته اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر عروس آسمان مشاطگان صنع حق</p></div>
<div class="m2"><p>این هزاران عقد در یارب چه در خور بسته اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب محک رنگست و انجم زر علی الاطلاق از آنک</p></div>
<div class="m2"><p>این محک را بر هوا از بهر آن زر بسته اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان خاصان خاک این شب باد چون یکدم در او</p></div>
<div class="m2"><p>راه رحمت بر دل خاصان، سراسر بسته اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کار آن رندان خاک انداز به کز آب خشک</p></div>
<div class="m2"><p>پرده آن لحظه گرد آتش تر بسته اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عاقبت از پرده بیرون اوفتد آن پرده ها</p></div>
<div class="m2"><p>کان حریفان صبحدم بر روی مزهر بسته اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>توشه جان از لب لعل شکروش برده اند</p></div>
<div class="m2"><p>گوشه دل در خم زلف معنبر بسته اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاهدی دارند چون در جلوه گه خندد فراخ</p></div>
<div class="m2"><p>عقل پندارد به مجلس تنگ شکر بسته اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روی آن دارد که گویند آن زمان بر روی او</p></div>
<div class="m2"><p>پرتوی از فر شاه هفت کشور بسته اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قهرمان ملک آن صاحبقرانی کز جلال</p></div>
<div class="m2"><p>خاک پایش قدسیان بر تارک سر بسته اند</p></div></div>