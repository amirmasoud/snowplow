---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>سروی که بر مهش ز شب تیره چنبرست</p></div>
<div class="m2"><p>لؤلؤش زیر لعل و گلشن زیر عنبرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرورده سپهر ستم پیشه شد به حسن</p></div>
<div class="m2"><p>زین روی عشوه ده چو سپهر ستمگرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیر شکنج زلفش و در شکرین لبش</p></div>
<div class="m2"><p>صد فتنه مدغم است و دو صد نکته مضرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ریختم سرشگ چو سیمو سمن ز چشم</p></div>
<div class="m2"><p>زیبد که دوست سیم سرین و سمنبرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور عقل من شدش به دل و دیده مشتری</p></div>
<div class="m2"><p>عیبش مکن که همچو دل و دیده در خورست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر زلف همچو عود گره زد به رغم من</p></div>
<div class="m2"><p>یعنی که پر گره و خم نکوترست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون گویمش که سرو و مهی چون ز روی حسن</p></div>
<div class="m2"><p>رشگ مه نو و حد سرو کشمرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در عرض روی حوروش و قد دلکشش</p></div>
<div class="m2"><p>نی سرو بر کشیده و نی مه منورست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتی شگفت بین که رخش در غمم مقیم</p></div>
<div class="m2"><p>همچون گل شکفته به سرخی مشهرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در عشق دوست سرخ بود روی بیدلی</p></div>
<div class="m2"><p>کش خون دل ز دیده همه شب مقطرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر خشک و تر که در کف من بد ز عقل و هوش</p></div>
<div class="m2"><p>بر روی خوب و غمزه شوخش مقررست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وین طرفه تر که در هوسش دیده و لبم</p></div>
<div class="m2"><p>هر دم ز سینه خشک وز خون جگر ترست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در دست فتنه طره عقلم مشوش است</p></div>
<div class="m2"><p>وز خون دیده چشمه عیشم مکدرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زین غم که چون ز چنبر عشقش برون جهم</p></div>
<div class="m2"><p>شخصم نحیف چون رسن و قد چو چنبرست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در گوش هر که حلقه غم کرد شک مکن</p></div>
<div class="m2"><p>کز عیش خوش چو حلقه همه عمر بر درست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زلفش چو ظلمت است و لبش چشمه خضر</p></div>
<div class="m2"><p>واندر ره غمش دل من چون سکندرست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زیبد که من ز ظلمت ظلمش برون جهم</p></div>
<div class="m2"><p>چون همرهم مدیح شه عدل گسترست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کیخسرو دوم شه خورشید مرتبت</p></div>
<div class="m2"><p>کو ملک بخش و خصم کش و بنده پرورست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سرچشمه ملوک عمر عز نصره</p></div>
<div class="m2"><p>کز عقل کل به مرتبه و قدر برترست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در صدر وصف چو چتر فریدون مؤیدست</p></div>
<div class="m2"><p>بر نیک و بد چو گردش گردون مظفرست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>طبعش ز بد چو روح پیمبر مقدس است</p></div>
<div class="m2"><p>شخصش ز عیب چون دم عیسی مطهرست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در بزم وقت عشرت و در رزم روز کین</p></div>
<div class="m2"><p>زربخش و عدل گستر و دلدوز و صفدرست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در بحر لطف و در چمن نصرتش مقیم</p></div>
<div class="m2"><p>یک برگ خشک طوبی و یک قطره کوثرست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وهمش برون ز نه فلک و هشت جنت است</p></div>
<div class="m2"><p>جودش فزون ز شش جهت و هفت کشور است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فتح مبین و نصر عزیزش به شرق و غرب</p></div>
<div class="m2"><p>روشن چو فتح رستم سگزی و نوذرست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صدر شهی به شخص لطیفش مزین است</p></div>
<div class="m2"><p>شغل عدو ز عزم متینش مبترست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خشمش چو دوزخیست که در وی به روز و شب</p></div>
<div class="m2"><p>سوزد حسود همچو سمن گر سمندرست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صیتش فزوده ولوله در خیل خلخ است</p></div>
<div class="m2"><p>تیغش فکنده زلزله در قصر فیصر است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زنده بدوست جود طبیعی و جود خلق</p></div>
<div class="m2"><p>چون نیک بنگری بر جودش مزورست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فرخنده خسرویست که در جنب همتش</p></div>
<div class="m2"><p>نه چرخ و هشت خلد و دو گیتی محقرست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سرور به تیغ شد نه به حیلت که نزد عقل</p></div>
<div class="m2"><p>در ملک سروری به سر تیغ و خنجرست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در بزم، زر چو قطره و دستش چو میغ شد</p></div>
<div class="m2"><p>در صف، عدوش روبه و تیغش غضنفرست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر هر کسی مسخر دور سپهر شد</p></div>
<div class="m2"><p>بنگر بدو که دور سپهرش مسخرست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چرخ بنفشه رنگ سیه دل ز هیبتش</p></div>
<div class="m2"><p>بی خورد و خفت شب همه شب همچو عبهرست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عرش مجید پیش دلش کم ز خردلیست</p></div>
<div class="m2"><p>بحر محیط پیش کفش کم ز فر غرست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر گویمش که عمر و حیدر صفت سزد</p></div>
<div class="m2"><p>کو در صف چو عمر و در صف چو حیدرست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خورشید نزد عرصه قدرش چو پشه یست</p></div>
<div class="m2"><p>سیمرغ پیش مخلب قهرش کبوترست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر صبحدم ز رشک دل و طبع روشنش</p></div>
<div class="m2"><p>قرص فلک ز دیده به خون جگر درست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>رویش چو دید فتح و ظفر گفت دیر زی</p></div>
<div class="m2"><p>کز رشگ روی تو رخ گردون مجد رست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نقش نگین تو خلل ملک دشمن است</p></div>
<div class="m2"><p>درج مدیح تو حسد درج گوهرست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در دفترست مدح تو مسطور وزین قبل</p></div>
<div class="m2"><p>خصمت سپید دست و سیه دل چو دفترست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بر بیرقت ز طره بلقیس پرچم است</p></div>
<div class="m2"><p>بریغلقت ز پنجه سیمرغ شهپرست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر چه ملوک جز تو درین عصر دیگرند</p></div>
<div class="m2"><p>بشنو ز من که دولت و فر تو دیگرست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>روی کرم به طبع لطیفت مزین است</p></div>
<div class="m2"><p>جعد سخن به مدح شریفت معنبرست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پشت زمین ز عدل تو چون صحن جنت است</p></div>
<div class="m2"><p>روی فلک ز جود تو پر زر و زیورست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در حضرتت مجیر به عز قبول تو</p></div>
<div class="m2"><p>شیرین حدیث و خوش سخن و سحرپرورست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>طبعش چو تیغ جان شکرت تیز و روشن است</p></div>
<div class="m2"><p>شعرش چو لفظ پر شکرت نغز و دلبرست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نثرش شگفت منطقی تیز فکرت است</p></div>
<div class="m2"><p>نظمش شگفت عنصری مدح گسترست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خرم نشین که موکب نوروز در رسید</p></div>
<div class="m2"><p>می خور که بخت بر در و معشوق در برست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زر بخش و رطل گیر و طرب جوی و عیش کن</p></div>
<div class="m2"><p>کز دست خنجر تو عدو دست بر سرست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بختت قوی و ملک قویم و فلک رهی است</p></div>
<div class="m2"><p>شغل طرب میسر و گردون مسخرست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چون روز نو رسید درین بزم چون بهشت</p></div>
<div class="m2"><p>می خور که روز خصم تو چون روز محشرست</p></div></div>