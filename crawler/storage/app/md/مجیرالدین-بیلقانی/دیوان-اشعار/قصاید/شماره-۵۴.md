---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>سرو سهی که سجده برد سرو کشمرش</p></div>
<div class="m2"><p>سنبل دمید بر طرف سوسن ترش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلبرگ شکل، در خوی خونین نشست دل</p></div>
<div class="m2"><p>زان چشم چون بنفشه و زان چشم عبهرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون دلم ز دیده برون می کند به هجر</p></div>
<div class="m2"><p>گویی چو دید خون دلم هست در خورش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شمع زرد رویم و چون غنچه تنگدل</p></div>
<div class="m2"><p>کز شمع و غنچه هست رخ و لب نکوترش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون در خورد به عقل؟ که گویم گه صفت</p></div>
<div class="m2"><p>ترک سهیل جبهت و سرو سمنبرش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترکی که دید؟ سلسله مشگ بر رخش</p></div>
<div class="m2"><p>سروی که؟ دید چشمه خورشید بر سرش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پشتم بسان حلقه زرین خمیده کرد</p></div>
<div class="m2"><p>زلف شکسته بر زبر حلقه زرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم شوم به حلقه زلفش مگر شبی</p></div>
<div class="m2"><p>گشتم به زور حلقه ولی حلقه بر درش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زلفش چو چنبرست و تنم چون رسن به شکل</p></div>
<div class="m2"><p>روزی برون برد رسنم سر به چنبرش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سنگین دلم ز غم چو دل لعل خون گرفت</p></div>
<div class="m2"><p>وز عشوه کم نکرد عقیق سخنورش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یعنی به صبح خنده زند غنچه بر چمن</p></div>
<div class="m2"><p>چون میغ تر کند به سرشگ مقطرش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در بر نگیرمش به گه وصل طرفه نیست</p></div>
<div class="m2"><p>هر گه که بنگرم سوی زلف معنبرش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زلفش چو عقربست و گر نیست در برم</p></div>
<div class="m2"><p>عیبم مکن که عقربت مست است در برش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون بر مهش دو خط مزور بدید دل</p></div>
<div class="m2"><p>در خود کشید خط ز دو خط مزورش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر دل ز من به دست نخستین ببرد دوست</p></div>
<div class="m2"><p>زیبد که بود مهره من در مششدرش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زین پس به بند عشوه نبندد دلم چو هست</p></div>
<div class="m2"><p>صدر سخن مدیح شه عدل گسترش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قطب ملوک نصرت دین کز هنر شدند</p></div>
<div class="m2"><p>دور سپهر و خطه گیتی مسخرش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پشت هدی محمد مهدی صفت که هست</p></div>
<div class="m2"><p>سقف شرف ز گنبد پیروزه برترش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر منبر سپهر خطیب خرد چه گفت؟</p></div>
<div class="m2"><p>بحر گهر ده و گهر بحر پیکرش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در خوی نشسته روح ز شخص مقدسش</p></div>
<div class="m2"><p>در شرم رفته عقل ز روح مطهرش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در همت رفیع وی از نقطه کم بود</p></div>
<div class="m2"><p>جرم زمین و عرصه چرخ مدورش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر دل نبشته عقل و بصر شکل موکبش</p></div>
<div class="m2"><p>در دیده کرده فتح و ظفر گرد لشکرش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نصرت دو دیده بر عقب تیر و ترکشش</p></div>
<div class="m2"><p>دولت نشسته در کنف درع و مغفرش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>محکوم گشته نه فلک و هشت جنتش</p></div>
<div class="m2"><p>گردن نهاده شش جهت و هفت اخترش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ملک کرم چو ملک زمین شد مسلمش</p></div>
<div class="m2"><p>شیر فلک چو شیر علم شد مسخرش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر کس که چون قلم نکند خدمتش به سر</p></div>
<div class="m2"><p>بینی ز دور چرخ سیه دل چو دفترش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یک روز نیست کز سر عجزش نمی رسد</p></div>
<div class="m2"><p>خدمت ز چین و خلخ و جزیت ز قیصرش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گردون دلی که در صف کین کرد لطف حق</p></div>
<div class="m2"><p>بر نیک و بد چو گردش گردون مظفرش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هم طوف کرده عدل و هنر گرد ملکتش</p></div>
<div class="m2"><p>هم خوش نشسته فتح و ظفر در معسکرش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مشرک شکسته دل ز تف تیر و بیلکش</p></div>
<div class="m2"><p>ملحد بریده سر ز سر تیغ و خنجرش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رستم نگویمش که گه رزم و روز بزم</p></div>
<div class="m2"><p>صد بنده به ز رستم سگزیست بر درش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در دولت پدر که سکندر چنو نبود</p></div>
<div class="m2"><p>شد ملکت سکندر رومی میسرش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وز بخت فرخش نبود طرفه گر به طوع</p></div>
<div class="m2"><p>در خدمت شریف رود صد سکندرش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در روز حمله جرعه بود بحر قلزمش</p></div>
<div class="m2"><p>در وقت ضربه ذره شود کوه منکرش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر شیر بیرقش ز پی کسب مرتبت</p></div>
<div class="m2"><p>یغلیق بسته طره جبریل شهپرش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون گویمش که سنجر عهدی که نزد عقل</p></div>
<div class="m2"><p>یک روزه بخشش است همه ملک سنجرش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گویی که هست دست و دل حیدر و عمر</p></div>
<div class="m2"><p>در ملک عدل عمر و شمشیر حیدرش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تیغش که کرد جوهر بی حد و مر پدید</p></div>
<div class="m2"><p>در حرب هست کشته بی حد و بی مرش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جسم عدو ز گوهر تیغش گسسته شد</p></div>
<div class="m2"><p>گویی که خصم جسم عدو گشت جوهرش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کلکش گرفت گونه بیمار زرد روی</p></div>
<div class="m2"><p>وی طرفه تر که مشگ سیه شد مزورش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در بیشه شیر دیده کنون بحر موج زن</p></div>
<div class="m2"><p>زین روی گشت روی به رنگ معصفرش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بشکست خرد پیکر گردون چو خنگ شه</p></div>
<div class="m2"><p>یک شیهه در فگند به گوش دو پیکرش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>صرصر چو روز رزم به گردش نمی رسد</p></div>
<div class="m2"><p>تشبیه چون کنم؟ گه رفتن به صرصرش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر کوه کوه بر نشیندی نگه کنش</p></div>
<div class="m2"><p>ور چرخ برق سیر ندیدی تو بنگرش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در چشم خون خصم بود صحب بسدش</p></div>
<div class="m2"><p>در گوش لحن کوس بود زخم مزهرش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خورشید خنگ خسرو خسرو نسب شدست</p></div>
<div class="m2"><p>خنگی که دید؟ خنجر زرین به کف درش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دین پروری که حرص تهی معده سیر شد</p></div>
<div class="m2"><p>چون خورد نیم لقمه ز جود موقرش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>در طبع هست خسرو گردون مطوعش</p></div>
<div class="m2"><p>در چشم هست گوهر گیتی محقرش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گر دست سوی رطل کند هست همرخش</p></div>
<div class="m2"><p>ور میل سوی عیش کند هست در خورش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بر لب سزد کنون لب معشوق گلرخش</p></div>
<div class="m2"><p>بر فک سزد کنون می صرف مقطرش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زهره گرفت زخمه عشرت به مجلسش</p></div>
<div class="m2"><p>خورشید برده رحمت دولت به محضرش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بزمش بهشت گشت ز خوبی و خرمی</p></div>
<div class="m2"><p>رطلش نبید صرف شده حوض کوثرش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شعر مجیر و قول خوش مقدسی شده</p></div>
<div class="m2"><p>در طبع خوش چو برگ گل و بوی عنبرش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هم درو چرخ کرده چو دولت مؤیدش</p></div>
<div class="m2"><p>هم بخت نیک کرده چو گردون معمرش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دست ستم بریده به شمشیر لطف حق</p></div>
<div class="m2"><p>کرده بری ز نکبت چرخ ستمگرش</p></div></div>