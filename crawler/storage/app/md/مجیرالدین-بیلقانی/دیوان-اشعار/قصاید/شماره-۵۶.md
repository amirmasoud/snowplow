---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>امن الرقیب نوائب الایام</p></div>
<div class="m2"><p>فسرت و عم عجائب الایام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما شبت من کبر و شیب هامتی</p></div>
<div class="m2"><p>صرف الهوی و شوائب الایام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لدغ المقارب لا یوترغب ما</p></div>
<div class="m2"><p>حمل الشتاء مناکب الایام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشتیت من نعی الزمان و بعده</p></div>
<div class="m2"><p>فی الضیر راد عقارب الایام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسقی شواربه و مادری العلی</p></div>
<div class="m2"><p>لما تعاطی شارب الایام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا لیت شعری هل تری منتوفة</p></div>
<div class="m2"><p>طرز الظلام و شارب الایام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لا تصف ان کنت النحاح فمن صفا</p></div>
<div class="m2"><p>کدرت علیه مشارب الایام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وارکب مطاالافلاک تنج فانه</p></div>
<div class="m2"><p>و سنان یعثر راکب الایام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اطربت من نعسی الزمان لاننی</p></div>
<div class="m2"><p>مذعض قلبی ناشب الایام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صیرت مثل العود جسمی شاحبا</p></div>
<div class="m2"><p>فجرت علیه مضارب الایام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لا تعذلنی بالمشیب فاننی</p></div>
<div class="m2"><p>طفل عذبه غرائب الایام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ان لاح فی شعری البیاض فانه</p></div>
<div class="m2"><p>نقع اثار مواکب الایام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هب ان راسی محلس و لطا لما</p></div>
<div class="m2"><p>هزم الشاب کتائب الایام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کتب السواد علی البیاض و هیهنا</p></div>
<div class="m2"><p>بالضد یکتب کاتب الایام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حتام یعقص ثم یعتقل بعده</p></div>
<div class="m2"><p>فرع الدجی و ترائب الایام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اغربت کآلعنقاء من مسرة</p></div>
<div class="m2"><p>اذحف عنی غارب الایام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فالان عندی واحد ان بدلت</p></div>
<div class="m2"><p>بالمشرقین مغارب الایام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>و ان اختفیت فعرق عدوی نابص</p></div>
<div class="m2"><p>اذ عرانی غارب الایام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>احجمت عن طلب المواهب موقتا</p></div>
<div class="m2"><p>ان تسترد مواهب الایام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عدم النجاح فلم یلن لسمیدع</p></div>
<div class="m2"><p>جنب المثالب جانب الایام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مالی وغیل الدهر اذ غلبت به</p></div>
<div class="m2"><p>غلب الاسود ثعالب الایام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>افحمت و الافحام لیس بشیمتی</p></div>
<div class="m2"><p>حتی یسیر رکائب الایام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لکن اذاالغربان ینعق فی اللوی</p></div>
<div class="m2"><p>خرست هناک عنادب الایام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حل الهوی ان حاک فیک ملامه</p></div>
<div class="m2"><p>و اعجز فدونک عائب الایام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فالیوم لایسمحن الا نظرة</p></div>
<div class="m2"><p>مثل الحباب جنائب الایام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تبغی المنی سفها و فی طلب المنی</p></div>
<div class="m2"><p>فقد السواد ذوائب الایام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ان تبت عن جرح الانام فتوبتی</p></div>
<div class="m2"><p>اهدت الی توائب الایام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>قالوا و ما اثمر الصدق مقالهم</p></div>
<div class="m2"><p>ان المجیر لتائب الایام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>انی لیبغاء تعطش بعد ان</p></div>
<div class="m2"><p>سدت علیه مذاهب الایام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مهلا سیروی الحلب عن بحر الندی</p></div>
<div class="m2"><p>ان لم یصده مخالب الایام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بحر خضم لم یزل بفتائه</p></div>
<div class="m2"><p>یحظی و یرجو خائب الایام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قطب المحامد افضل بن محمد</p></div>
<div class="m2"><p>صفو الانام و صاحب الایام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نهر السحائب جوده و قد انجلت</p></div>
<div class="m2"><p>عن اصغریه سحائب الایام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ان کفت الایام ارزاق الوری</p></div>
<div class="m2"><p>فالکف منه نائب الایام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>و لقد غلطت فعن فواضل سیبه</p></div>
<div class="m2"><p>یعطی و یطلق راتب الایام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هو سیف حق لاتزال کلیلة</p></div>
<div class="m2"><p>عن صفحتیه قواضب الایام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فبد سته زاد الزمان مفاخرا</p></div>
<div class="m2"><p>و قد اضمحل معائب الایام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حاکی الغیاهب لونه و نعشت</p></div>
<div class="m2"><p>بمن احتواه غیاهب الایام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>و لرایه انکشف الغیوب کانما</p></div>
<div class="m2"><p>نقضت لدیه حقائب الایام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شمس الشریعه یستضیی بنوره</p></div>
<div class="m2"><p>بدر العلی و کواکب الایام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اعو لایام هبته مراتبا</p></div>
<div class="m2"><p>واقول زاد مراتب الایام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>من کان معتمدا لمصاب رایه</p></div>
<div class="m2"><p>لم یدن منه مصائب الایام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>الدهر طاطا راسه عن حکمه</p></div>
<div class="m2"><p>وسعت الیه رغائب الایام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ثقلت و خف بجوده و لمنه</p></div>
<div class="m2"><p>یجب الغریم و غارب الایام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نعماه تنعش نعش یحیی برمک</p></div>
<div class="m2"><p>من جدلته سوالب الایام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نصبت له بوسادة قرشیه</p></div>
<div class="m2"><p>منها استعیر مناصب الایام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>یا زین دین الله زدت مکارما</p></div>
<div class="m2"><p>نفضت بهن مثالب الایام</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بمکانک المیمون یمدح دائما</p></div>
<div class="m2"><p>بعد الهجاء عواقب الایام</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اذ لم تلد دنیاک مثلک فاعذرن</p></div>
<div class="m2"><p>ان لم یلدن نجائب الایام</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اتعیب ایاما وقتک مکارها</p></div>
<div class="m2"><p>لانال طولک عائب الایام</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ایامنا شنئت عداک کانما</p></div>
<div class="m2"><p>نفر العداة ربائب الایام</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>انفذت فیک خریدة عربیه</p></div>
<div class="m2"><p>یصبو الیها خاطب الایام</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>و نظمت مدحک کالعقود مردفا</p></div>
<div class="m2"><p>لم یغن عنه کواعب الایام</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>و ترکت اسلوبی القدیم فانه</p></div>
<div class="m2"><p>نسجت علیه عناکب الایام</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>لا زلت مغشی الفناء و من ابی</p></div>
<div class="m2"><p>قامت علیه نوادب الایام</p></div></div>