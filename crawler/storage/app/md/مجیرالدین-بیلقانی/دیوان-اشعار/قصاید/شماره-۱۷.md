---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>عافیت رخت از جهان برداشت</p></div>
<div class="m2"><p>مکرمت دیده زین مکان برداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتابی که خاک را زر کرد</p></div>
<div class="m2"><p>سایه زین تیره خاکدان برداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون روان شد ز چشم من که فلک</p></div>
<div class="m2"><p>خونم از اکحل روان برداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اسب صبرم ز رنج پوست فگند</p></div>
<div class="m2"><p>محنتم مغز استخوان برداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دم دم این عمر من نهفته ربود</p></div>
<div class="m2"><p>کم کم این گنج من نهان برداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرسم من به همرهان وفا</p></div>
<div class="m2"><p>زانکه شب رفت و کاروان برداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لب به دندانم از جهان که مرا</p></div>
<div class="m2"><p>نقد عمر از ره دهان برداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا کی از قرص مهر و کاسه چرخ</p></div>
<div class="m2"><p>کین چنین رفت و آن چنان برداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مه سپهر و مه مهر چون خردم</p></div>
<div class="m2"><p>طمع از کاسه دل زنان برداشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روز کارم به کار گیرد از آنک</p></div>
<div class="m2"><p>رخم از رنگ زر نشان برداشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا کی اندر میان سرای جهان</p></div>
<div class="m2"><p>باید این رنج بیکران برداشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شکر کین غم کنون ز ششدر خاک</p></div>
<div class="m2"><p>صاحب هفتمین قران برداشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کیقباد دوم مظفر دین</p></div>
<div class="m2"><p>کز عدو تیغ او امان برداشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شه قزل ارسلان که دست و دلش</p></div>
<div class="m2"><p>از جهان نام بحر و کان برداشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه اول قدم ز روی زمین</p></div>
<div class="m2"><p>فتنه آخرالزمان برداشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه با او فرو نهاد فلک</p></div>
<div class="m2"><p>چون کمند فلک ستان برداشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شیشه آسمان چو باده بریخت</p></div>
<div class="m2"><p>راست کو تیغ شیشه سان برداشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سر گردنکشان چو تاج خروس</p></div>
<div class="m2"><p>به سر تیغ سرفشان برداشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همتش چون هوای گردون کرد</p></div>
<div class="m2"><p>پای ازین خطه هوان برداشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ثور را پرچم از کتف بستد</p></div>
<div class="m2"><p>قوس را قبضه از کمان برداشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فتنه را تیغ او میان بدوزد</p></div>
<div class="m2"><p>تا به یکبارش از میان برداشت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صبح یک روز خیل تا شش بود</p></div>
<div class="m2"><p>علم آفتاب از آن برداشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کار کردش ز شش جهات جهان</p></div>
<div class="m2"><p>نام و ناموس هفتخوان برداشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روز روشن ثناش می خواندم</p></div>
<div class="m2"><p>باغ از آن کلک ضمیران برداشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شب تیره دعاش می گفتم</p></div>
<div class="m2"><p>سرو از آن سر به آسمان برداشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به سر دست کوه و دریا را</p></div>
<div class="m2"><p>روزی از روی امتحان برداشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دهن بحر تا به سینه ببرد</p></div>
<div class="m2"><p>کمر کوه تا میان برداشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سپر ماه را به نوک سنان</p></div>
<div class="m2"><p>جوجو از راه کهکشان برداشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تیغ او کش یکیست آهن و پشم</p></div>
<div class="m2"><p>پنبه از گوش گرد نان برداشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پشت چرخ سبکرو از چه خم است؟</p></div>
<div class="m2"><p>زانکه زو منتی گران برداشت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مشتری وار پیش او بهرام</p></div>
<div class="m2"><p>تیغ بنهاد و طیلسان برداشت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ای فلک صولتی که خاک درت</p></div>
<div class="m2"><p>پرده آب، بی گمان برداشت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر بزرگیت دل کسی ننهاد</p></div>
<div class="m2"><p>که دل از عقل خرده دان برداشت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سر ز خطت کسی کشد که قلم</p></div>
<div class="m2"><p>از ورق های خان و مان برداشت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>حلق خصمت که حوله آسای است</p></div>
<div class="m2"><p>دست گردون به ریسمان برداشت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وانکه او کرد گردنی با تو</p></div>
<div class="m2"><p>شیر خشم تو گرده ران برداشت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از پی خدمت درت که ز لطف</p></div>
<div class="m2"><p>صف روضه جنان برداشت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مور با ضعف خود کمر در بست</p></div>
<div class="m2"><p>پشه با عجز خود سنان برداشت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به خدایی که امر او به دو حرف</p></div>
<div class="m2"><p>هفت گردون به یک دخان برداشت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به خزان زان لکا که صنعش ریخت</p></div>
<div class="m2"><p>طرفی طرف بوستان برداشت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مه دی ز آن سمن که حکمش ببخت</p></div>
<div class="m2"><p>حصه صحن گلستان برداشت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تهمت خون به حق ز گرگ فگند</p></div>
<div class="m2"><p>دین باطل به یک شبان برداشت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که نه جز طوق دار تست هر آنک</p></div>
<div class="m2"><p>سر ازین تیره آستان برداشت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دست فیض تو تخته بند نیاز</p></div>
<div class="m2"><p>خوش خوش از پای انس و جان برداشت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هر چه در چشم ملک ناخنه بود</p></div>
<div class="m2"><p>ناخن قهر تو عیان برداشت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خاتمت مهرهای گردون را</p></div>
<div class="m2"><p>از دو عالم یگان یگان برداشت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون برانی سوار گوید عقل</p></div>
<div class="m2"><p>روستم راه سیستان برداشت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چون بگیری پیاله گوید بزم</p></div>
<div class="m2"><p>آب بین کانش روان برداشت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>زین سخن طبع خوش حدیث مجیر</p></div>
<div class="m2"><p>حدث از شعر باستان برداشت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>وقت مدحت ز گنج خاطر من</p></div>
<div class="m2"><p>مایه صد گنج شایگان برداشت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شه ستایی چو من نداند کرد</p></div>
<div class="m2"><p>هر که او شه ره بیان برداشت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چه دعا گویمت که عالم پیر</p></div>
<div class="m2"><p>نقص ازین دولت جوان برداشت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>جاودان زی که هرکس از عدلت</p></div>
<div class="m2"><p>لذت عمر جاودان برداشت</p></div></div>