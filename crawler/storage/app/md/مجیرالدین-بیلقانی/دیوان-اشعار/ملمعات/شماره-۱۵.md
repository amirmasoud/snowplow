---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>یا من علا یوم الندی کعبه</p></div>
<div class="m2"><p>و من له الافق محل وضیع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسطت کف الجود حتی اذا</p></div>
<div class="m2"><p>اردت ان تمسک لا تستطیع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جاوز منک الحق حدالصبی</p></div>
<div class="m2"><p>و کان فیما قبل طفلا رضیع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انت ربیع الفضل مستقبلا</p></div>
<div class="m2"><p>و نحن فی ذاالحر ملقی صریع</p></div></div>