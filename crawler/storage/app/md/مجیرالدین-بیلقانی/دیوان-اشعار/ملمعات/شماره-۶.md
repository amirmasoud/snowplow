---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>لقیت فخرا یا امام ذوی الادب</p></div>
<div class="m2"><p>و بذالک العلیا قد افتخر اللقب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و وعیت لفظک اذ وعظت فلم یزل</p></div>
<div class="m2"><p>ادنی مقرطة بما زان الذهب؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بحر المعانی فی کلامک مضمر</p></div>
<div class="m2"><p>و کلامک الدر النثیر المنتخب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الدر وسط البحر لیس بمعجب</p></div>
<div class="m2"><p>البحر فی الدر النثیر من العجب</p></div></div>