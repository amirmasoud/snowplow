---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>غیرت وجه حالی من وجهک المنقش</p></div>
<div class="m2"><p>شوشت حال قلبی من صد غک المشوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای قد و قامتت خوب وی زلف و عارضت خوش</p></div>
<div class="m2"><p>آبی بر آتشم زن آبی بده چو اتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهلا فداک روح فالجسم فات عشقا</p></div>
<div class="m2"><p>رفقا فکدت انی من دمعی الموشش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش خوش نشاط گستر بر دامن گل اکنون</p></div>
<div class="m2"><p>کافگند روی گیتی از سبزه رنگ مفرش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا منیتی و روحی فات الفؤاد فارحم</p></div>
<div class="m2"><p>یا سلوتی و عیشی طال العناء فانعش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون باخت دیده و دل خوش خوش مجیر با تو</p></div>
<div class="m2"><p>روزش مخواه تیره عیشش مخواه ناخوش</p></div></div>