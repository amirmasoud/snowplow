---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>قد قدم العید عائدا بجلال</p></div>
<div class="m2"><p>فاسق لنا قهوة بایمن فال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عید رسید ای کنار من ز تو خالی!</p></div>
<div class="m2"><p>بزم بیارای و می بیاور حالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وجهک بدرالدجی وصدغک لیل</p></div>
<div class="m2"><p>هات سلافا علی طلوع هلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رطل گران کن که روی در طرب آورد</p></div>
<div class="m2"><p>شاه قزل ارسلان سپهر معالی</p></div></div>