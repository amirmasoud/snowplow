---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>یا غزالی یا غزالی لاتَحَدنی بِالوِصالٖ</p></div>
<div class="m2"><p>مع ان تعرف حالی فی فُنونِ الإخِتلالٖ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو طراز هر جمالی در نهایات کمالی</p></div>
<div class="m2"><p>رشک مشک و غیرت گل هم به خَدّ و هم به خالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این اشکو حالتی فی قید هجر واشتعالی</p></div>
<div class="m2"><p>لست اهلا بالوصال ام رهنتم بالملال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه جانم را وبالی ورچه عمرم را زوالی</p></div>
<div class="m2"><p>باد احسنتت در تزاید باد عمرت بر توالی</p></div></div>