---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>قد باهت الثغور باعلام بهلوان</p></div>
<div class="m2"><p>و ارتاحت السعود بایام بهلوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می در فگن به جام غم انجام پهلوان</p></div>
<div class="m2"><p>کاب حیات گشت می از جام پهلوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لا زالت الثواقت فی لجه الدجی</p></div>
<div class="m2"><p>مغبرة بتربة اقدام بهلوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز مباد خطبه و سکه به شرق و غرب</p></div>
<div class="m2"><p>بی کنیت مبارک و بی نام پهلوان</p></div></div>