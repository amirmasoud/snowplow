---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>هات یا عقلی و دینی من صبابات الزمان</p></div>
<div class="m2"><p>لا تعدنی ان قلبی لیس یرضی بالامان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سر حالت نگارا! عیش کن تا در جهانی</p></div>
<div class="m2"><p>زانکه تو طفلی درین ره حیلت گردون ندانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لا تلومونی فانی هائم اهوی المثانی</p></div>
<div class="m2"><p>غننی قولا علیها واترک السبع المثانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه خرمن سوز عقلی ورچه شورانگیز جانی</p></div>
<div class="m2"><p>با مجیر از روی رحمت گر بسازی می‌توانی</p></div></div>