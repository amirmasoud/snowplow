---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>به خدایی که در موجودات</p></div>
<div class="m2"><p>جز به امرش نمی شود منظوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بماندم چو قالبی بی روح</p></div>
<div class="m2"><p>تا ز دیدار تو شدم محروم</p></div></div>