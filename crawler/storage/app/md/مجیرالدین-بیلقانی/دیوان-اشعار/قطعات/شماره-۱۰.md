---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>نیکویی کن شها که در عالم</p></div>
<div class="m2"><p>نام شاهان به نیکویی سمر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک صحیفه ز نام نیک ترا</p></div>
<div class="m2"><p>بهتر از صد خزانه گهر است</p></div></div>