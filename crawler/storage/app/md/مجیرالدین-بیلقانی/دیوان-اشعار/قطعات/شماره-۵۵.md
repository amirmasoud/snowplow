---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>قسم به واهب عقلی که پیش علم قدیم</p></div>
<div class="m2"><p>یکی است چشمه خورشید و سایه عنقاش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمین شود ز یکی امر او چو سایه چاه</p></div>
<div class="m2"><p>در آبگون قفس این آفتاب آتش پاش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که هست طمع جمال آفتاب تأثیری</p></div>
<div class="m2"><p>که پس روی است کم از سایه گنبد خضراش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا چو سایه سیه روی کرد و خانه نشین</p></div>
<div class="m2"><p>به نظم نثر صفت طبع آفتاب آساش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فزون ز ذره ز لفظ آفتاب می پاشد</p></div>
<div class="m2"><p>مگر چو سایه در افتاد فیض حق به قفاش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان به دست زبان آفتاب وار گشاد</p></div>
<div class="m2"><p>از آن چو سایه فلک بوسه می دهد بر پاش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به زیر بار غم همچو سایه زیر قدم</p></div>
<div class="m2"><p>زرشگ آنکه گذشت آفتاب بر بالاش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان دوا ز دمش برد اگر نه بگرفتمی</p></div>
<div class="m2"><p>بسان سایه و خورشید دق و استسقاش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکست گوهر دریا و باد آب نشاند</p></div>
<div class="m2"><p>به شعر چون گهر و آب طبع چون دریاش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به لطف گر ید بیضا بدو نماید صبح</p></div>
<div class="m2"><p>به شکل شام گرفتست بی گمان سوداش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنانش را گهر شبچراغ چون خوانم؟</p></div>
<div class="m2"><p>که هست مشعله روز لاف زن ز ضیاش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپید مهره صیتش چنان دمید جهان</p></div>
<div class="m2"><p>که رخنه خواست شد این سبز حقه از آواش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به چشم مردم از آن گشت همچو مردم چشم</p></div>
<div class="m2"><p>که در سواد توان یافتن ید بیضاش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دلم ز عقده غم چون میان بیت گشاد</p></div>
<div class="m2"><p>چو بستم از زبر دل قصیده غراش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سبک بضاعتم و کم بها به شکل نثار</p></div>
<div class="m2"><p>اگر نثار نسازم ز عمر بیش بهاش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خطش چو زلف صواب نگین شدست به حسن</p></div>
<div class="m2"><p>بخواند نقش خطا هر که خواند نقش خطاش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بسا که طیره حورا دهد رقیب بهشت</p></div>
<div class="m2"><p>به کلک سر زده مانند طره حوراش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر نه بوس حلالست و رشگ عین حلال</p></div>
<div class="m2"><p>چرا چو دید دلم گشت در زمان شیداش؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه لایق است به من مدح او که در خور نیست</p></div>
<div class="m2"><p>کلاه گوشه نرگس به چشم نابیناش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نثار او چو مرا شد علاج جان پیوند</p></div>
<div class="m2"><p>دعاش گویم و دانم که واجب است دعاش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سیه سپیدی دوران قصیده اش بادا</p></div>
<div class="m2"><p>که او بود به همه حال مقطع و مبداش</p></div></div>