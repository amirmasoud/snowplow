---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>بزرگ جهان خواجه شاه قدر</p></div>
<div class="m2"><p>که بر عالم مکرمت پادشاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وزیری که خورشید بر اوج خویش</p></div>
<div class="m2"><p>بر او کم از دانه آسیاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قوام کرم فخر دین کز کفش</p></div>
<div class="m2"><p>طمع را همه کار کژ گشت راست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تماشاگه همت عالیش</p></div>
<div class="m2"><p>از آن سوی دروازه کبرییاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمین تا بدین گونه بر هم نشست</p></div>
<div class="m2"><p>چنویی ز پشت زمین بر نخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه فتوی کند در حق بنده ای</p></div>
<div class="m2"><p>که در شیوه بندگی بی ریاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که او را ز ناگه به هنگام کوچ</p></div>
<div class="m2"><p>غلامی در افزود و اسبی بکاست</p></div></div>