---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>اوحد دین چون ز حادثات زمانه</p></div>
<div class="m2"><p>کار وزارت به مرگ خویش تبه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با من مسکین خرد به زاری زاری</p></div>
<div class="m2"><p>گفت هزاران هزار آوخ واه کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر دل تو باز حادثه حشر آورد</p></div>
<div class="m2"><p>بر در تو باز رنج نایبه ره کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمر ربود از تو روزگار نه مخدوم</p></div>
<div class="m2"><p>سر ستد از تو فلک نه قصد کله کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش این هست شرط ماتم او چیست؟</p></div>
<div class="m2"><p>گفت مرا چون به صبح و شام نگه کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موی ببر اینکه صبح موی ببرید</p></div>
<div class="m2"><p>جامه سیه کن که شام جامه سیه کرد</p></div></div>