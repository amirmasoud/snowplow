---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>جنت دنیاست عرض این همایون بارگاه</p></div>
<div class="m2"><p>راحت جاوید را در ساحت او خانمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل گواهی می دهد کاین کعبه اقبال را</p></div>
<div class="m2"><p>کرد معمار فلک دایم به معموری ضمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش آن ایوان اگر تقدیر، دستوری دهد</p></div>
<div class="m2"><p>سجده آرد طاق کسری نه که طاق آسمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ظل او غمخوارگان را چون ارم بزم طرب</p></div>
<div class="m2"><p>صحن او پرسندگان را چون حرم صحن امان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پاسبانی بر سرش در پاس هر بامی قضا</p></div>
<div class="m2"><p>پیشکاری در برش در پیش هر کاری زمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز رقیبان هنر در وی نبوده هیچ کس</p></div>
<div class="m2"><p>جز امینان خرد در وی نبوده دیده بان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوته از بالای اوج منظرش دست یقین</p></div>
<div class="m2"><p>قاصر از پهنای بسط مطرحش پای گمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از پی جانداری سلطان عالی هیکلش</p></div>
<div class="m2"><p>شحنه جوشن وران چرخ بردارد کمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دور باش عکس او لاحول دیو آمد که هست</p></div>
<div class="m2"><p>هفت نقش منفعل از چار قطر او رمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مطرب طبع است خاک پای او در خاصیت</p></div>
<div class="m2"><p>راست چون خاکی که باشد مدفن زر جمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خسته محنت حریم او پسندد ملتجا</p></div>
<div class="m2"><p>هاتف دولت صدای او گزیند ترجمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دولت فر به ز فر اوست راعی عجاف</p></div>
<div class="m2"><p>سایه لاغر ز یاد او مراعی سمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مالک او گر نبودی مسند قاضی القضات</p></div>
<div class="m2"><p>در جناب او سعادت کی نشستی این زمان؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یارب اقبالی ده او را بر خصوم اولیا</p></div>
<div class="m2"><p>چون اجل امید بندد چون سخن جاویدمان</p></div></div>