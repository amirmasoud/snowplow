---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>شاها جمال طبلکی این زن وفاست کو</p></div>
<div class="m2"><p>نحس زحل دهد به وجود اورمزد را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او نرد خدمتت به دغا هفت سال برد</p></div>
<div class="m2"><p>دریاب کار این دغل مهره دزد را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عمر خویش یک بزه دانم که کرده ای</p></div>
<div class="m2"><p>مردی بکن زبان ببر این زن بمزد را</p></div></div>