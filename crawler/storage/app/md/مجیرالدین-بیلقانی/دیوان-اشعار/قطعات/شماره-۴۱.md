---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>به خدایی که قدرت او را</p></div>
<div class="m2"><p>ماه را عاجز محاق کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاین دل ریش آرزومندم</p></div>
<div class="m2"><p>تا که با وصلت اتفاق کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر زند خنده بر دروغ زند</p></div>
<div class="m2"><p>ور کند شادیی نفاق کند</p></div></div>