---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>به خدایی که نفس ناطقه را</p></div>
<div class="m2"><p>با تن تیره آشنایی داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که مرا کم شبی چراغ حیات</p></div>
<div class="m2"><p>بی جمال تو روشنایی داد</p></div></div>