---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>ز روح القدس دوش کردم سؤالی</p></div>
<div class="m2"><p>که از مکرمت چیست کاو حد ندارد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا گفت کای مادح حضرت او</p></div>
<div class="m2"><p>چه گویم ز اوحد که او حد ندارد؟</p></div></div>