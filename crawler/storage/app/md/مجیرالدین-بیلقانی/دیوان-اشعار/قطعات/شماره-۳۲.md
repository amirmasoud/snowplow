---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>از بستر غم که جای بدخواه تو باد</p></div>
<div class="m2"><p>برخیز سبک ورنه جهان برخیزد</p></div></div>