---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>صاحبا صدرا! به ذات آنکه کلک قدرتش</p></div>
<div class="m2"><p>سنبله گردون و راه کهکشان داند نگاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه به داس ماه نو بدرود خوید سنبله</p></div>
<div class="m2"><p>گه به راه کهکشان در دانه انجم بکاشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاشتر و اسب من از دیروز باز از کاه و جو</p></div>
<div class="m2"><p>جز که راه کهکشان و سنبله گردون نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو از کهدان جمشیدی فرستی اندکی</p></div>
<div class="m2"><p>آن بود سهل و شود مر هر دو را ترتیب چاشت</p></div></div>