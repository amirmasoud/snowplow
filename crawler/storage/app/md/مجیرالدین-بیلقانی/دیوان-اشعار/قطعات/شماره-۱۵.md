---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>شاها بدان خدای که در دست قدرتش</p></div>
<div class="m2"><p>هفت آسمان چو مهره به دست مشعبدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرمان دهی که در خم چوگان حکم اوست</p></div>
<div class="m2"><p>آن گویهای زر که برین سبز گنبد است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کین بنده تا ز خدمت بزم تو دور ماند</p></div>
<div class="m2"><p>روزی دمی خوش از دل او بر نیامدست</p></div></div>