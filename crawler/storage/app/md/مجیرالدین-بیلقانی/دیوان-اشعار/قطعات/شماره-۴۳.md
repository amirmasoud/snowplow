---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>گفت آن کس که در جهان او را</p></div>
<div class="m2"><p>بجز از زحمت جهان ننهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام او پختگان آتش لطف</p></div>
<div class="m2"><p>بجز از خام قلتبان ننهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش او زمره سبک روحان</p></div>
<div class="m2"><p>قله قاف را گران ننهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که مجیر آنکس است کز طمعش</p></div>
<div class="m2"><p>در کف کس خط امان ننهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اژدها طبع گردد از سخنش</p></div>
<div class="m2"><p>هم سر گنج شایگان ننهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون زبانی شود زبانش اگر</p></div>
<div class="m2"><p>مهر احسانش بر زبان ننهند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این چنین تهمتی به بی خردی</p></div>
<div class="m2"><p>بر بزرگان خرده دان ننهند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این دو کلپتره را جواب بسی است</p></div>
<div class="m2"><p>لیکن او را محل آن ننهند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کاشکی داندی که پر هنران</p></div>
<div class="m2"><p>هنر خویش در هوان ننهند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که او بر کران نشست از بد</p></div>
<div class="m2"><p>با وی انصاف در میان ننهند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا تنور آتشین زبان نشود</p></div>
<div class="m2"><p>نانش البته در دهان ننهند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که در قدح کم ز مدح آید</p></div>
<div class="m2"><p>لقبش کامل البیان ننهند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وانکه چون آستان فتد در پای</p></div>
<div class="m2"><p>پیش او سر بر آستان ننهند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در دل سوسن ار نه حربه کشد</p></div>
<div class="m2"><p>زر حل کرده رایگان ننهند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تخت خورشید اگر نه تیغ زند</p></div>
<div class="m2"><p>بر سر چارم آسمان ننهند</p></div></div>