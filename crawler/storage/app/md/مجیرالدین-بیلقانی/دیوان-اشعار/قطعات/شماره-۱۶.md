---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>خسروا! یک روزه عزم خدمتت</p></div>
<div class="m2"><p>از بهشت جاودانی خوشترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدن درگاه تو هر بامداد</p></div>
<div class="m2"><p>از همه عهد جوانی خوشترست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در رکابت هر غمی کاید به روی</p></div>
<div class="m2"><p>آن غم از صد شادمانی خوشترست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنده را از هر چه در عالم خوشست</p></div>
<div class="m2"><p>هم به بزمت مدح خوانی خوشترست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حق همی داند که این بیچاره را</p></div>
<div class="m2"><p>بی تو مرگ از زندگانی خوشترست</p></div></div>