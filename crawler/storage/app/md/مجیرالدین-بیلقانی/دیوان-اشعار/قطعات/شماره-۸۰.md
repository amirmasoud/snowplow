---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>زهی ملک بخشی که خصمت ندارد</p></div>
<div class="m2"><p>بجز تیره روزی و جز تنگ عمری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم بلبل باغ معنی و لیکن</p></div>
<div class="m2"><p>شدم آرزومند قمری ز غمری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همانا بکن صید اگر می توانی</p></div>
<div class="m2"><p>چو من بلبلی را به یک جفت قمری</p></div></div>