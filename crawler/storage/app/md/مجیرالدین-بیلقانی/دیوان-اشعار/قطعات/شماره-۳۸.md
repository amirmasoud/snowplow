---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>عیسی وقت ما رئیس الدین</p></div>
<div class="m2"><p>مایه مرکز هنر باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پسر بوالیمین خری است عجب</p></div>
<div class="m2"><p>کز همه علم بی خبر باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن خرست این مسیح و پیش خرد</p></div>
<div class="m2"><p>عیسی از خر طبیب تر باشد</p></div></div>