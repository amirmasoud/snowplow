---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>مرا دل نه و دردهای دلی</p></div>
<div class="m2"><p>که عاجز شدستم ز درمان خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندانم من خسته آن کیم؟</p></div>
<div class="m2"><p>که باری نیم بی گمان آن خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تن و جان خود می خورم در عنا</p></div>
<div class="m2"><p>چو شمعم همه ساله مهمان خویش</p></div></div>