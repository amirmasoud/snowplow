---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>صفاهان خرم و خوش می نماید</p></div>
<div class="m2"><p>بسان پَرّ شهرآرای طاووس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ولی زین زاغ طبعان کاهل شهرند</p></div>
<div class="m2"><p>نجس شد بال خوش سیمای طاووس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یقین می دان که مجموع سپاهان</p></div>
<div class="m2"><p>چو طاووس است و اینها پای طاووس</p></div></div>