---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>با شبروان شدم به در یار نیم شب</p></div>
<div class="m2"><p>جستم بسوی حضرت او بار نیم شب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گر چه آهنین بدو مسمار آتشین</p></div>
<div class="m2"><p>آهم نه در گذاشت نه مسمار نیم شب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید بود قافله سالار آسمان</p></div>
<div class="m2"><p>بربست رخت قافله سالار نیم شب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب را هزار طره فزون بود و کس ندید</p></div>
<div class="m2"><p>بی آه سرد از آن همه یک تار نیم شب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر جام می که عقل به من داد نیمروز</p></div>
<div class="m2"><p>آمد به رغم من همه در کار نیم شب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن جا که دیده ها ز غمش خون همی گریست</p></div>
<div class="m2"><p>رفتم نهان ز دیده اغیار نیم شب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آواز داد هاتف غیبی حذر کنید!</p></div>
<div class="m2"><p>کامد حریف مست دگر بار نیم شب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون مهره خسته شد دلم از بس که بر گرفت</p></div>
<div class="m2"><p>مهر راز در خزانه اسرار نیم شب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امروز شکرین سخنم زانکه کرده ام</p></div>
<div class="m2"><p>قوت روان ز لعل شکر بار نیم شب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صبرم چو ناخن از پی آن سر بریده شد</p></div>
<div class="m2"><p>کاورد خون به ناخن من یار نیم شب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زنهار در زبان مجیر از چه ماند از آنک</p></div>
<div class="m2"><p>از یار باز گشت به زنهار نیم شب</p></div></div>