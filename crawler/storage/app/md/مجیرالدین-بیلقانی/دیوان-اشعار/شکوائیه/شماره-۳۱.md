---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>هیچ وفایی ز روزگار ندیدم</p></div>
<div class="m2"><p>هیچ میی خالی از خمار ندیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیده ام از باغ روزگار بسی گل</p></div>
<div class="m2"><p>لیک بجز در میانه خار ندیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جُرعه راحت که خورد تا پس از آن من؟</p></div>
<div class="m2"><p>بر سر خاکش چو جَرعه، خوار ندیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خُم ایام کاولش همه دُردست</p></div>
<div class="m2"><p>شربتی از عیش خوشگوار ندیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راحت دل گفته اند هست درین عهد</p></div>
<div class="m2"><p>این همه گفتند و هیچ بار ندیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همنفسان رفته اند و در غم ایشان</p></div>
<div class="m2"><p>راحتی از هیچ غمگسار ندیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس که شمردم شمار عمر به صد دست</p></div>
<div class="m2"><p>یک نفس خوش در آن شمار ندیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دم نزدم با کسی به وصل که در پی</p></div>
<div class="m2"><p>بر دل خویش از فراق بار ندیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرقت اگر فی المثل چو بحر محیط است</p></div>
<div class="m2"><p>تا منم، آن بحر را کنار ندیدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لذت ایام من به صد نرسد لیک</p></div>
<div class="m2"><p>غصه ایام کم از هزار ندیدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه من تنها ز روزگار دل آشوب</p></div>
<div class="m2"><p>کار طرب نیک برقرار ندیدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا منم اندر زمانه هیچ کسی را</p></div>
<div class="m2"><p>شادی و راحت ز روزگار ندیدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عیش بهارست و بی خزان به جهان در</p></div>
<div class="m2"><p>هیچ نسیمی ز نو بهار ندیدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جمله بدیدم سرای عمر و لیکن</p></div>
<div class="m2"><p>هیچ بنا در وی استوار ندیدم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کار کسی، راست بر مراد دل او</p></div>
<div class="m2"><p>در خم نه سقف زرنگار ندیدم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عاقبة الامر تکیه گاه سلامت</p></div>
<div class="m2"><p>بهتر از الطاف کردگار ندیدم</p></div></div>