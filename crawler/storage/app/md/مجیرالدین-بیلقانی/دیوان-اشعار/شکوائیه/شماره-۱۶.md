---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>یک ذره مهر در ایام مانده نیست</p></div>
<div class="m2"><p>یک قطره آب در رخ اجرام مانده نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا جام روزگار پر زا خون خلق شد</p></div>
<div class="m2"><p>کس را شراب خوش مزه در جام مانده نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلگونه موافقت و تاب عافیت</p></div>
<div class="m2"><p>در روی دهر و طره ایام مانده نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جستم ز خاک صورت راحت زمانه گفت</p></div>
<div class="m2"><p>مرغی طلب مکن که درین دام مانده نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دود و شرر مجوی که با سنگ حادثات</p></div>
<div class="m2"><p>قندیل صبح و مجمره شام مانده نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان دیگ مکرمت که جهان پخت پیش ازین</p></div>
<div class="m2"><p>اندر جهان بحز طمع خام مانده نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دهر تا نجات دو صد ساله راه هست</p></div>
<div class="m2"><p>وز خاک تا امید یکی گام مانده نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیمرغ و مردمی بهم اند از برای آنک</p></div>
<div class="m2"><p>زان جز حدیث وزین بحز از نام مانده نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلها ز غم بسوخت مگر خرمی بمرد؟</p></div>
<div class="m2"><p>جم دل شکسته گشت مگر جام مانده نیست؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آخر مجیر از همه کامی دهن بشست</p></div>
<div class="m2"><p>وین است چاره چون به جهان کام مانده نیست</p></div></div>