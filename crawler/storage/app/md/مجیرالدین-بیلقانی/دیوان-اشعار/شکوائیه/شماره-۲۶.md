---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>هر دوست که اختیار ما بود</p></div>
<div class="m2"><p>واندر بد و نیک یار ما بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غایت لطف و مهربانی</p></div>
<div class="m2"><p>غمخواره و حق گزار ما بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از طبع لطیف و طلعت خوب</p></div>
<div class="m2"><p>در فصل خزان بهار ما بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما خرم و خوش به صحبت او</p></div>
<div class="m2"><p>او شاد به روزگار ما بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هر کاری که باز جستیم</p></div>
<div class="m2"><p>دیدیم که دوستدار ما بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز صحبت هر یکی ازیشان</p></div>
<div class="m2"><p>شادی همه شب شکار ما بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وز همدمی و صفای هر یک</p></div>
<div class="m2"><p>در عشرت کار کار ما بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفتند چنانکه هر یکی را</p></div>
<div class="m2"><p>غم در دل بی قرار ما بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کردیم شمار چون بدیدیم</p></div>
<div class="m2"><p>این حال نه در شمار ما بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون غم نخوریم چون شد از دست؟</p></div>
<div class="m2"><p>هر دوست که غمگسار ما بود</p></div></div>