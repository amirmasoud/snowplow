---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>هیچ کس را رنج خاطر در جهان حاصل مباد</p></div>
<div class="m2"><p>وز فراق دوستان کار کسی مشکل مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ رنجی نیست بر دل بتر از رنج فراق</p></div>
<div class="m2"><p>هیچ کس را یارب این رنج از جهان بر دل مباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل مردم کم شود در کار فرقت هر زمان</p></div>
<div class="m2"><p>این چنین شربت نصیب مردم عاقل مباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بلا منزل گرفت آنکس که ماند اندر فراق</p></div>
<div class="m2"><p>هیچ نازک طبع را اندر بلا منزل مباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمر را با رنج دل بی حاصلی حاصل شناس</p></div>
<div class="m2"><p>عمر کس ار دشمنست ار دوست بی حاصل مباد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که او گوید فراق دوستان آسان بود</p></div>
<div class="m2"><p>رنج فرقت از دل او یک نفس زایل مباد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بد و نیک جهان غافل نشستن شرط نیست</p></div>
<div class="m2"><p>هیچ عاقل از بد و نیک جهان غاقل مباد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تصاریف زمان پای عزیزان در گل است</p></div>
<div class="m2"><p>یارب از دست جهان پای کسی در گل مباد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست عهدی تا به ما اندوه فرقت مایل است</p></div>
<div class="m2"><p>انده فرقت ازین پس سوی ما مایل مباد</p></div></div>