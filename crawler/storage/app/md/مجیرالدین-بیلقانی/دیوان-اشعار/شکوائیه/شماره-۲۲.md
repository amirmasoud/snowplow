---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>مدت انده ما زود به سر خواهد شد</p></div>
<div class="m2"><p>وز دل ما غم و اندیشه بدر خواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار شادی و فراغت به نوا خواهد شد</p></div>
<div class="m2"><p>خانه وحشت و غم زیر و زبر خواهد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیش ما گر چه بسی تلخ تر از حنظل بود</p></div>
<div class="m2"><p>شکر کان حنظل ما همچو شکر خواهد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساغر و باده ما هم به صفت هم به صفا</p></div>
<div class="m2"><p>آب خشک ای عجب و آتش تر خواهد شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کسی را که خبر شد ز غم و انده ما</p></div>
<div class="m2"><p>از دل خرم ما زود خبر خواهد شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همدم مجلس ما لهو و طرب خواهد بود</p></div>
<div class="m2"><p>همره موکب ما فتح و ظفر خواهد شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیز غم را که چو شمشیر قضا کارگرست</p></div>
<div class="m2"><p>سینه دشمن ما پیش سپر خواهد شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه ما را جگری داد جهان باکی نیست</p></div>
<div class="m2"><p>قوت اعدا پس ازین خون جگر خواهد شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کار ما ساخته تر از زر تو خواهد گشت</p></div>
<div class="m2"><p>وز حسد شکل عدو شمشه زر خواهد شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غم و شادی جهان را نبود هیچ ثبات</p></div>
<div class="m2"><p>هر زمان حال وی از شکل، دگر خواهد شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوش برانیم و بدانیم بهر گونه که هست</p></div>
<div class="m2"><p>راحت و محنت ایام به سر خواهد شد</p></div></div>