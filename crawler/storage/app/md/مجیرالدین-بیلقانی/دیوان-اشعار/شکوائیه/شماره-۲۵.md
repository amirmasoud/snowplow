---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>آنچه موی از جور با ما می کند</p></div>
<div class="m2"><p>راست خواهی سخت رسوا می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیست مقصودش که پیش از وقت خویش</p></div>
<div class="m2"><p>از شب من صبح پیدا می کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زرگر ایام در بازار عمر</p></div>
<div class="m2"><p>مشگ ما را سیم سیما می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردمان قصد کسان پنهان کنند</p></div>
<div class="m2"><p>موی من قصد آشکارا می کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزگار از زحمت موی سپید</p></div>
<div class="m2"><p>این سیاهی بین که با ما می کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمد و بنشست با روی سپید</p></div>
<div class="m2"><p>در همه عالم چنینها می کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من چه بد کردم که او هر ساعتی؟</p></div>
<div class="m2"><p>با من آن بد را مکافا می کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من جوان تازه و پیری مرا</p></div>
<div class="m2"><p>قصدهای بی محابا می کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چه کرد ایام اگر بر جای بود</p></div>
<div class="m2"><p>این یکی باری نه بر جا می کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دور گردون را گناهی نیز نیست</p></div>
<div class="m2"><p>کین قضای حق تعالی می کند</p></div></div>