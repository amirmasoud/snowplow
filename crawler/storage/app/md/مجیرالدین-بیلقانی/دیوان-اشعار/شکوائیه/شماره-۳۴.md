---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>منم آنکس که شادی را سر و کاری نمی بینم</p></div>
<div class="m2"><p>به عالم خوشدلی را روز بازاری نمی بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درختی طرفه شد عالم که من چندانکه می جویم</p></div>
<div class="m2"><p>بجز اندوه و اندیشه برو باری نمی بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوانی گلستان خرم و تازه است و من هرگز</p></div>
<div class="m2"><p>گلی زین گلستان تازه بی خاری نمی بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین دوران دل خرم بدان آوازه می ماند</p></div>
<div class="m2"><p>کزو جز نام و آوازه من آثاری نمی بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلی آزاد وقتی خوش نه روزی بلکه یک ساعت</p></div>
<div class="m2"><p>به عالم گر کسی دیدست من باری نمی بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کدامین روز کزد گردون سیه میغی نمی خیزد؟</p></div>
<div class="m2"><p>کدامین شب که ایام آزاری نمی بینم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاسباب طرب در طبع جز وحشت نمی یابم</p></div>
<div class="m2"><p>ز انواع فرح بر دل بجز باری نمی بینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فلک از پار تا امسال با من تند شد زآنسان</p></div>
<div class="m2"><p>کزو هر لحظه جز رنجی و آزاری نمی بینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه سود ار کار من هر روز بالا بیشتر گیرد؟</p></div>
<div class="m2"><p>چو من خود را به وقت عیش بر کاری نمی بینم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عزیزان رفته اند از چشم و غمشان مانده اندر دل</p></div>
<div class="m2"><p>به غم خوردن ز خود بهتر سزاواری نمی بینم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نگهدارم درین حالت مگر هم لطف حق گردد؟</p></div>
<div class="m2"><p>که من بهتر ز لطف حق نگهداری نمی بینم</p></div></div>