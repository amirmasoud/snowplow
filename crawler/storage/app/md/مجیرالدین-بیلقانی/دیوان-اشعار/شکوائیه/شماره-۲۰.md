---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>هر کو ز نژاد آدم افتاد</p></div>
<div class="m2"><p>شادی به نصیب او کم افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنمای دلی که او درین دور</p></div>
<div class="m2"><p>از صدمه غم مسلم افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زخمی که زمانه بر دلم زد</p></div>
<div class="m2"><p>افزون ز هزار مرهم افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون خوش بود ای عجب دل آنک؟</p></div>
<div class="m2"><p>در یک سالش دو ماتم افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قصه چکنم که ساغر غم؟</p></div>
<div class="m2"><p>بر دست دلم دمادم افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان در عجب است و عقل عاجز</p></div>
<div class="m2"><p>کین واقعه ها چه محکم افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان روز که رنج با دل ما</p></div>
<div class="m2"><p>از نکبت دهر همدم افتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دیده خوشدلی نمک ریخت</p></div>
<div class="m2"><p>در قامت خرمی خم افتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه لهو و نشاط با هم آمد</p></div>
<div class="m2"><p>نه یکشبه عیش درهم افتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آری به مراد کم زند دم</p></div>
<div class="m2"><p>آن کس که ز نسل آدم افتاد</p></div></div>