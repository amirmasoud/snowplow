---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>جهان فتح من آخر به نوبهار رسد</p></div>
<div class="m2"><p>نهال عیش من آخر به برگ و بار رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلی که کوفته رنج و زخم خورد غم است</p></div>
<div class="m2"><p>به عیش و عشرت بی حد و بی شمار رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امید هست کزین پس نوا و واله زیر</p></div>
<div class="m2"><p>به بزم ما بدل ناله های زار رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر به خاطر هرکس ز گلستان جهان</p></div>
<div class="m2"><p>گهی نسیم گل و گاه زخم خار رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشیدایم بسی زخم خار، وقت آمد</p></div>
<div class="m2"><p>که کار ما به گل لعل آبدار رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به عذر یک غم دل گیر کز زمانه رسید</p></div>
<div class="m2"><p>نشاط و خرمی و کام دل، هزار رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خوش دلی و طرب دست باز نتوان داشت</p></div>
<div class="m2"><p>اگر به خاطر ما گه گهی غبار رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز چشم زخم فنا . . .</p></div>
<div class="m2"><p>ز روزگار چنین غم به روزگار رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که گفت یا که؟ گمان برد کز زمانه به ما</p></div>
<div class="m2"><p>غم بزرگ به یک سال در دو بار رسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رسید موسم آن کز شرابخانه لطف</p></div>
<div class="m2"><p>به دست ما مدد جام خوشگوار رسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز تاب دل به سر زلف تابدار رسیم</p></div>
<div class="m2"><p>چو عاشقی که ز هجران به وصل یار رسد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به گوش ما بدل ناله های غمزدگان</p></div>
<div class="m2"><p>حدیث و نکته چون در شاهوار رسد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طرب رسد ز پس غم چنانکه وقت خزان</p></div>
<div class="m2"><p>نسیم کوکبه لطف نوبهار رسد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خروش بربط و بانگ سرود و ناله چنگ</p></div>
<div class="m2"><p>ز بزم بر فلک سبز زر نگار رسد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طراز حاصل این جمله آن تواند بود</p></div>
<div class="m2"><p>که سوی ما مدد لطف کردگار رسد</p></div></div>