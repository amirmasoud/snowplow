---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>نیست روزی که نیستم دلتنگ</p></div>
<div class="m2"><p>از چه از آسمان آینه رنگ؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخت محنت نهم به منزل عیش</p></div>
<div class="m2"><p>زانکه شد بارگیر شادی لنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل من کز صفا چو آینه بود</p></div>
<div class="m2"><p>یافت از فرقت عزیزان زنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تو پرسم چگونه دارد دل</p></div>
<div class="m2"><p>ور بود آفریده ز آهن و سنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طاقت غصه های پشتا پشت</p></div>
<div class="m2"><p>قوت زخمهای رنگارنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنچه در دل غم فراق کند</p></div>
<div class="m2"><p>نکند صد هزار تیر خدنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکنم عیش از آنکه خوش نبود</p></div>
<div class="m2"><p>عیش های فراخ با دل تنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون فراغت ز چنگ شد بیرون</p></div>
<div class="m2"><p>زشت باشد فغان و ناله چنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نکبت پار و غصه امسال</p></div>
<div class="m2"><p>که همی داشت سوی من آهنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ریخت از روی عیش رونق و آب</p></div>
<div class="m2"><p>برد از کار دل طراوت و رنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کم کند تکیه بر جهان دو روی</p></div>
<div class="m2"><p>هر که عاقل بود به صلح و به جنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زانکه با اهل روزگار او</p></div>
<div class="m2"><p>حیله روبه است و خوی پلنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل من کی بود به رنج سزا؟</p></div>
<div class="m2"><p>در سزا کی بود به کام نهنگ؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از جهان غم به من چگونه فتاد؟</p></div>
<div class="m2"><p>نه جهان تنگ شد چو حلقه تنگ؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر خدای اعتماد آن دارم</p></div>
<div class="m2"><p>که دهد شهد اگر چه داد شرنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون رسیدست رنج دل به شتاب</p></div>
<div class="m2"><p>مرکب خرمی رسد به درنگ</p></div></div>