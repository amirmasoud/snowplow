---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>عهدیست تا نصیبه ما از جهان غم است</p></div>
<div class="m2"><p>حال دل از فلک چو فلک نیک بر هم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عالم از فراغت خاطر اثر نماند</p></div>
<div class="m2"><p>آری مگر فراغت از آن سوی عالم است؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مدت جوانی و در عهد کودکی</p></div>
<div class="m2"><p>ما را چه روز رنج و چه هنگام ماتم است؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بر مراد خویش یکی دم نمی زند</p></div>
<div class="m2"><p>از بس که با حوادث ایام درهم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جستم ز روزگار دمی خوش، زمانه گفت</p></div>
<div class="m2"><p>چیزی مجوی بیش که اندر جهان کم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دست ماست خاتم اقبال و کار عیش</p></div>
<div class="m2"><p>هر روز با شگونه تر از نقش خاتم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کردم مسلم درین عهد کم کسی است</p></div>
<div class="m2"><p>کورا ز روزگار دلی خوش مسلم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دارم هر آن چه باید از اسباب خرمی</p></div>
<div class="m2"><p>اما خلاف در دل آزاد و خرم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر عشرتی که بی دل فارغ کنی هباست</p></div>
<div class="m2"><p>هر شادیی که از سر وحشت کنی غم است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خود غم مگیر با که زنم دم که در جهان</p></div>
<div class="m2"><p>نه یار دلنواز و نه دلدار محرم است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای غم به آخر آی! که خامی بسی نمود</p></div>
<div class="m2"><p>این رنج ها که در خم این سبز طارم است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیدم روی غصه کنون وقت خوشدلی است</p></div>
<div class="m2"><p>خوردیم ز خم فتنه، کنون جای مرهم است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>انصاف ده! که قسمت ما غم بسی رسید</p></div>
<div class="m2"><p>گر زانکه در جهان غم و شادی مقسم است</p></div></div>