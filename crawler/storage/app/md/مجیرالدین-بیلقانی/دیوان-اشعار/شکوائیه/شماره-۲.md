---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>آنها که بوده اند ز دل دوستدار ما</p></div>
<div class="m2"><p>در نیک و بد موافق و انده گسار ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان جمع دوستان و عزیزان که بود خوش</p></div>
<div class="m2"><p>زایشان همیشه عیش دل روزگار ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفتند از این زمانه بد عهد زیر خاک</p></div>
<div class="m2"><p>هم عهد ما گذاشته هم زینهار ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشتند پایمال حوادث بدان صفت</p></div>
<div class="m2"><p>گویی به هیچ وقت نبودند یار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما در میان عشرت خوش کم نشسته ایم</p></div>
<div class="m2"><p>تا دور مانده اند به عجز از کنار ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بد در شمار صحبت هر یک دمی کنون</p></div>
<div class="m2"><p>بی حاصلی است حاصل و بس زین شمار ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یارب که از فراق عزیزان چه بارهاست؟</p></div>
<div class="m2"><p>بر طبع نازک و دل نابردبار ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از ما به اضطرار جدا شد دلی که او</p></div>
<div class="m2"><p>بود از طریق مهر و وفا اختیار ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از دوستان همدم و یاران هم نفس</p></div>
<div class="m2"><p>با آوخ و دریغ فتادست کار ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ماییم در خمار فراق و کسی نماند</p></div>
<div class="m2"><p>کو بشکند به باده انس این خمار ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خود محرمی کجاست درین عهد تا دهد؟</p></div>
<div class="m2"><p>یک دم قرار عیش دل بی قرار ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون با کشار حادثه ماندیم پس چه سود؟</p></div>
<div class="m2"><p>زین آشکار بودن شیران شکار ما</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با آنکه هست از سر فرمان دهی ز بخت</p></div>
<div class="m2"><p>امن و پناه خلق جهان در جوار ما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بی دوستان هم نفس از لذت اوفتاد</p></div>
<div class="m2"><p>عیش لطیف و جام می خوشگوار ما</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیکی کنیم و نیکویی ایرا که در جهان</p></div>
<div class="m2"><p>این هر دو به بود که بود یادگار ما</p></div></div>