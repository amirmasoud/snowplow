---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>چه وقت موی سیپدست روز برنایی</p></div>
<div class="m2"><p>چه روز زحمت پیری است وقت زیبایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا که اول عهد جوانی امروزست</p></div>
<div class="m2"><p>شبم چو روز همی گردد اینت رسوایی!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رواست کز پی موی سیاه دل تنگم</p></div>
<div class="m2"><p>که در میان سیاهی است نور بینایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو ای زمانه چه دیدی در آنکه پیش از وقت؟</p></div>
<div class="m2"><p>عبیر بر سر کافور تازه اندایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو ای سپید موی! از خدای شرمی دار</p></div>
<div class="m2"><p>نه وقت تست که تو از کمین برون آیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا بنفشه چو نورسته است هم شاید</p></div>
<div class="m2"><p>گرم بنفشه به برگ سمن نیالایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر چه موی سیاه و سپید هر دو یکی است</p></div>
<div class="m2"><p>مرا که فارغم از تازگی و برنایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولیک خوش نبود کز سپید کاری خویش</p></div>
<div class="m2"><p>ز ظلم موی سپیدم به خلق بنمایی</p></div></div>