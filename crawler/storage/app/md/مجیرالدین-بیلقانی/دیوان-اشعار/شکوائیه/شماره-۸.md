---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>فلک باز از نهان خارم نهادست</p></div>
<div class="m2"><p>که پیری پای بر کارم نهادست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا خاری چنین ننهاد دیگر</p></div>
<div class="m2"><p>اگر چه خار بسیارم نهادست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدین موی سپید ار راست خواهی</p></div>
<div class="m2"><p>بنای رنج و آزارم نهادست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا تا دهر سنبل یاسمین کرد</p></div>
<div class="m2"><p>به دل بر بار تیمارم نهادست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تحکم بین که گردون برگ کافور</p></div>
<div class="m2"><p>به جای مشک بر بارم نهادست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>معاذالله که این موی سپیدست!</p></div>
<div class="m2"><p>که دل بر جنگ و پیکارم نهادست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به راه عیش بر، دامی است ز انده</p></div>
<div class="m2"><p>که این چرخ ستمکارم نهادست</p></div></div>