---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>تا لاله ز شوخی به جهان شو در افگند</p></div>
<div class="m2"><p>از پرده بسی خسته دلان را بدر افگند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف و رخ معشوقه ما دید بعینه</p></div>
<div class="m2"><p>هر دیده که بر لاله سحرگه نظر افگند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر خون که جهان خورد از آزرده دلان پار</p></div>
<div class="m2"><p>امسال زمین از جگر خویش افگند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش دل شده بود از مدد عمر که ناگه</p></div>
<div class="m2"><p>بیم اجلش خون سیه در جگر افگند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن روز که بشکفت ز یاقوت سپر ساخت</p></div>
<div class="m2"><p>و آن شب که فرو ریخت ز عالم سپر افگند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغی است عجب لاله که بر شاخ زمانه</p></div>
<div class="m2"><p>شب بال بر آورد و سحرگاه پر افگند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با لاله همی خواست مجیر اسب طرب تاخت</p></div>
<div class="m2"><p>یک حادثه پیش آمد و صد دفع در افگند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوسن به سحرگه به زبانی که ورا هست</p></div>
<div class="m2"><p>از فتح شه اندر همه عالم خبر افگند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شه زاده محمد پسر اعظم اتابک</p></div>
<div class="m2"><p>کاو سایه همه بر سر فتح و ظفر افگند</p></div></div>