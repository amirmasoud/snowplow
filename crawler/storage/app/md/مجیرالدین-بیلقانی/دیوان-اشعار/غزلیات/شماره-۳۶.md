---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>با که گشایم نفس کاهل صفایی نماند؟</p></div>
<div class="m2"><p>در همه روی زمین بسته گشایی نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر چمن روزگار پی چه نهم کاندرو؟</p></div>
<div class="m2"><p>نوش نهالی نرست مهر گیایی نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قافله خرمی زان سوی عالم گذشت</p></div>
<div class="m2"><p>وز پی واماندگان بانگ درایی نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل طلب یار کرد باز بترکش گرفت</p></div>
<div class="m2"><p>دید که در هیچ کس هیچ وفایی نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر کوی جهان انس مگیر ای مجیر!</p></div>
<div class="m2"><p>کز همه دل خستگان جنس تو جایی نماند</p></div></div>