---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>گفتم ای ترک نظر سوی من غمگین کن</p></div>
<div class="m2"><p>رحمتی بر من محنت زده مسکین کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق من بی لب شیرین تو تلخ است چو زهر</p></div>
<div class="m2"><p>به یکی بوسه همه زهر مرا شیرین کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل من شده از آتش هجران تو گرم</p></div>
<div class="m2"><p>زان لب لعل دل گرم مرا تسکین کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خنده خوش می‌زن و مجلس همه در شکر گیر</p></div>
<div class="m2"><p>زلف بفشان و جهان پر گل و پر نسرین کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت من ماهم و همچون تو بود خام طمع</p></div>
<div class="m2"><p>هر که گوید که طمع بر مه و بر پروین کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم ای ترک مزن بر دل من داغ جفا</p></div>
<div class="m2"><p>ور زنی، مرهم از آن دو لب چون نوشین کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمش چند کنم در غم تو ناله زار؟</p></div>
<div class="m2"><p>گفت گر وصل منت باید صد چندین کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم از سیم میانت اثری یابم گفت</p></div>
<div class="m2"><p>یا میانم مطلب یا کمرم زرین کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور ندانی که وجوه کمر زر به کجاست؟</p></div>
<div class="m2"><p>مدحت بارگه خسرو خوب آیین کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ارسلان شاه جهانبخش که گوید کرمش</p></div>
<div class="m2"><p>جای خاک دَرَم از تارک علیین کن</p></div></div>