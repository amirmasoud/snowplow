---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>گر چشم شوخ تو ز جفا خفته نیستی</p></div>
<div class="m2"><p>کار جهان چو زلف تو آشفته نیستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور نیستی به همت لعل تو هیچ دل</p></div>
<div class="m2"><p>با نوک غمزه های تو ناسفته نیستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چوگان زدن نه کار تو بودی بر آفتاب</p></div>
<div class="m2"><p>گره راه گوی ز آه دلم رفته نیستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون لاله زیر زلف تو جایی گرفتمی</p></div>
<div class="m2"><p>گر بخت من چو نرگس تو خفته نیستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی مجیر با تو که در خون کس مشو</p></div>
<div class="m2"><p>گر با تو این سخن دگری گفته نیستی</p></div></div>