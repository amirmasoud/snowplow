---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>گر ز چشم من خیالت یک زمان برخاستی</p></div>
<div class="m2"><p>طمع دل زان طره عنبر فشان برخاستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور به شب گردون شنیدی ناله و افغان من</p></div>
<div class="m2"><p>از فغان من ز گردون صد فغان برخاستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نبودی بر زمین بار غمم شک نیستی</p></div>
<div class="m2"><p>کز سبکساری زمین چون آسمان برخاستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی وفا یاری و گر من بودمی خصم وفا</p></div>
<div class="m2"><p>زود رسم بی وفایی از جهان برخاستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ز بیم جان اسیر عشوه های گرم تست</p></div>
<div class="m2"><p>گر دل من دل بدی از بند جان برخاستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لاشه دل بس گرانبارست از آن در ره فتاد</p></div>
<div class="m2"><p>آه اگر از زیر این بار گران برخاستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در میان غم مجیر از جان خجل شد کاشکی</p></div>
<div class="m2"><p>یا مجیر خسته یا غم زین میان برخاستی</p></div></div>