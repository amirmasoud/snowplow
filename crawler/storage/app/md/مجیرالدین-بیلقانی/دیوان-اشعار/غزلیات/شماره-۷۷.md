---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>انصاف داده‌ام که به خوبی یگانه‌ای</p></div>
<div class="m2"><p>واندر جهان به حیله و افسون فسانه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاکیزه‌تر ز غنچه گل بر بنفشه‌ای</p></div>
<div class="m2"><p>پوشیده‌تر ز دانه در در خزانه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاخی است دلبری که تو آن را شکوفه‌ای</p></div>
<div class="m2"><p>عقدی است دلبری که تو آن را میانه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شادی به روی تو که ز تو شاد نیست کس</p></div>
<div class="m2"><p>صد جان فدای غم که تو غم را بهانه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خون من مشو که نه همدست عالمی</p></div>
<div class="m2"><p>بر جان من مزن نه تو یار زمانه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی که آیم از پی دیدار بر درت</p></div>
<div class="m2"><p>با من برون پرده و بی‌من به خانه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی بدان مجیر که از دل ترا نیم</p></div>
<div class="m2"><p>عقلی و هوش نیست عجب گر مرا نه‌ای!</p></div></div>