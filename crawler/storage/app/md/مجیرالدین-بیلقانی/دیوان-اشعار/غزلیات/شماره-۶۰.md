---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>ای عارض چون روزت روزم به شب آورده</p></div>
<div class="m2"><p>وی غمزه جانسوزت جانم به لب آورده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جزعت به جگر خواری دل برده به صد زاری</p></div>
<div class="m2"><p>لعلت به شکر باری شکلی عجب آورده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهر تب صد غمگین داده ز لب شیرین</p></div>
<div class="m2"><p>وز بهر من مسکین زان مهر تب آورده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دریاب که هم روزی بودم ز سر سوزی</p></div>
<div class="m2"><p>با چون تو جگر دوزی روزی به شب آورده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم سروی و هم بلبل هم سوسنی و هم گل</p></div>
<div class="m2"><p>ای بر سمن از سنبل شور و شغب آورده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد حال مجیر از غم بر دف زده در عالم</p></div>
<div class="m2"><p>تو از غم او هر دم چون می طرب آورده</p></div></div>