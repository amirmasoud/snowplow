---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>گره مشگ بر سمن چه زنی؟</p></div>
<div class="m2"><p>لشکر زنگ بر ختن چه زنی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون تو گویی که جان نفس نزنم</p></div>
<div class="m2"><p>من چو گویم که بوسه تن چه زنی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ز لعل تو بوسه از طلبم</p></div>
<div class="m2"><p>بر شکر لؤلؤ عدن چه زنی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>او نرنجد بدین قدر بدهد</p></div>
<div class="m2"><p>تو لگد در فتوح من چه زنی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد گریبان دریده شد ز غمت</p></div>
<div class="m2"><p>چاک بر طرف پیرهن چه زنی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم از غم بسوخت دم چه دهی؟</p></div>
<div class="m2"><p>غم تو دل ببرد تن چه زنی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر زمانی مرا که مرغ توام</p></div>
<div class="m2"><p>سینه بر نوک بابزن چه زنی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ز تو دل شکسته گشت مجیر</p></div>
<div class="m2"><p>بر دلش زخم دل شکن چه زنی؟</p></div></div>