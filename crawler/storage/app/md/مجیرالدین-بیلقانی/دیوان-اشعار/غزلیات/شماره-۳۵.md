---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>جانا خبر وصل تو زی ما که رساند؟</p></div>
<div class="m2"><p>یا قصه ما سوی تو تنها که رساند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توتوست چو دفتر غم ما از هوس آنک</p></div>
<div class="m2"><p>زینجا به تو طومار غم ما که رساند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینجا که منم طمع وصال تو محال است</p></div>
<div class="m2"><p>وآنجا که تویی حال من آنجا که رساند؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دورست بخارا نرسد پیش تو اما</p></div>
<div class="m2"><p>سوزنده بخاری به بخارا که رساند؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان رفت و زبان را سخن اینست که یارب</p></div>
<div class="m2"><p>زی مادم جان بخش مسیحا که رساند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر مرغ که بد در ره عشق تو پرافگند</p></div>
<div class="m2"><p>اکنون به تو نامه که دهد یا که رساند؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امروز مجیر است ز غم با نفس سرد</p></div>
<div class="m2"><p>تا خود نفس سرد به فردا که رساند؟</p></div></div>