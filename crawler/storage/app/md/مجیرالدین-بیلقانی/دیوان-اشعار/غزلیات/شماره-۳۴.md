---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>خوش است حسن تو تا دل ز یار بستاند</p></div>
<div class="m2"><p>چو دل ستد ز دل و جان قرار بستاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تویی و عشوه آن روی چون بهار که او</p></div>
<div class="m2"><p>خراج نیکوی از نوبهار بستاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمت به هر دو جهان چون دهم که سرد بود</p></div>
<div class="m2"><p>کسی که گل بدهد نوک خار بستاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر ز من گله کردی که نستدم ز تو جان</p></div>
<div class="m2"><p>اگر ز تو نستدهای انتظار بستاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهای حسن تو بنهاده ایم گوهر عمر</p></div>
<div class="m2"><p>بده که گر ندهی روزگار بستاند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمت خوش است ولیک از زمانه می ترسم</p></div>
<div class="m2"><p>که او چو غم بدهد غمگسار بستاند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مجیر خسته تیر فراق باد آن روز</p></div>
<div class="m2"><p>که جان نداده دل از دست یار بستاند</p></div></div>