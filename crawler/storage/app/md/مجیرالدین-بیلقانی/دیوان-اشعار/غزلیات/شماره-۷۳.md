---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>ای به حسن آفت جهان که تویی</p></div>
<div class="m2"><p>که شناسد ترا چنان که تویی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه عالم بتان عشوه دهند</p></div>
<div class="m2"><p>نه ازین دست دلستان که تویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تو دور اوفتاده ام عجب است</p></div>
<div class="m2"><p>این چنین در میان جان که تویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی آیی درین میان که منم</p></div>
<div class="m2"><p>من که باشم در آن میان که تویی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی وفا خواندیم چه می خوانی؟</p></div>
<div class="m2"><p>خسته ای را بدان زبان که تویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیک دانی که بی وفا نه منم</p></div>
<div class="m2"><p>ای نکو روی بد گمان که تویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر به درد تو شاد نیست مجیر</p></div>
<div class="m2"><p>بی وفا باد همچنان که تویی</p></div></div>