---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>بلبل آمد به در باغ و ز گل راه بخواست</p></div>
<div class="m2"><p>وز گل این بار به فریاد و علی الله بخواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل بدو گفت اگرت آرزویی هست بخواه</p></div>
<div class="m2"><p>خدمت خاص گلستان به سحرگاه بخواست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاله با جام می سرخ چو آمد بر گل</p></div>
<div class="m2"><p>گل ز بلبل غزلی تازه و دلخواه بخواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس فرستاد به من بلبل و از طبع مجیر</p></div>
<div class="m2"><p>غزلی مخلص آن مدح شهنشاه بخواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ارسلان شاه جهانگیر که خورشید فلک</p></div>
<div class="m2"><p>از دل و همت او مرتبه و جاه بخواست</p></div></div>