---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>گلی کو کزو زخم خاری نیاید؟</p></div>
<div class="m2"><p>میی کو کزان می خماری نیاید؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز پشت جهان هر نفس عاشقان را</p></div>
<div class="m2"><p>غم آید ولی غمگساری نیاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازین رهروان گر بسی بر شماری</p></div>
<div class="m2"><p>از آن حاصل الا شماری نیاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر کار اهل عدم دارد ار نه</p></div>
<div class="m2"><p>ازین جمع نا اهل کاری نیاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر سقف گردون به صد پاره گردد</p></div>
<div class="m2"><p>بجز بر سر سوگواری نیاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی کز میان جهان بر سر آمد</p></div>
<div class="m2"><p>ز دریای غم بر کناری نیاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مجیر از تو کار جهان به نگردد</p></div>
<div class="m2"><p>که از سبزه تر بهاری نیاید</p></div></div>