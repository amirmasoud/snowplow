---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>جز رگ جان عشق تو دگر چه گشاید؟</p></div>
<div class="m2"><p>یا ز تو جز رنج و درد سر چه گشاید؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه نظر بسته ای و گر چه نبندی؟</p></div>
<div class="m2"><p>خسته دلان را ز یک نظر چه گشاید؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پر بهم آورد مرغ عافیت از تو</p></div>
<div class="m2"><p>مرغ بر آتش نهاده، پر چه گشاید؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ره تو نقد تازه بستن دلهاست</p></div>
<div class="m2"><p>از تو جز این یک دری دگر چه گشاید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمر چو رفت از عنایت تو خیرد؟</p></div>
<div class="m2"><p>دل چو تبه شد ز گلشکر چه گشاید؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تو نظر خواستی مجیر ولیکن</p></div>
<div class="m2"><p>بسته غم را ازین قدر چه گشاید؟</p></div></div>