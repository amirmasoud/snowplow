---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>چون تو از غم ندیده ای خواری</p></div>
<div class="m2"><p>از غم ما کجا خبر داری؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خفته ای خوش چو بخت من همه شب</p></div>
<div class="m2"><p>تو چه دانی ز رنج بیداری؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فارغی نیک دانم از کارم</p></div>
<div class="m2"><p>فارغم چون تو بر سر کاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بار غم را چو نیک می نگرم</p></div>
<div class="m2"><p>در خورست این جفا به سر باری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شو جفا کن که از تو تا به وفا</p></div>
<div class="m2"><p>پاره ای راه هست پنداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سالها شد که صید تست مجیر</p></div>
<div class="m2"><p>برهان یا بکش چه می داری؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رایگان از کفم مده که مرا</p></div>
<div class="m2"><p>می کند پهلوان خریداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نصرة الدین که روز نصرت و فتح</p></div>
<div class="m2"><p>ملک را تیغ او دهد یاری</p></div></div>