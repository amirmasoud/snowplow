---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>زلف کافرکیش تو آیین ایمان بر گرفت</p></div>
<div class="m2"><p>عقل را صف بر شکست و عالم جان بر گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعل در کان، خاک بر سر کرد تا یاقوت تو</p></div>
<div class="m2"><p>پرده ظلمت ز پیش آب حیوان بر گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خشک سالی بود عالم را چو عشقت رخ نمود</p></div>
<div class="m2"><p>از سرشگ چشم من یک نیمه طوفان بر گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا سر زلف تو چوگان گشت در میدان جان</p></div>
<div class="m2"><p>صد هزاران گوی زر گردون به دندان بر گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سایه را بر من فگن کآخر شناسی این قدر</p></div>
<div class="m2"><p>کز سر خاک ای شکر لب سایه نتوان بر گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دی خیالت دید کز عشقت چنان کردم فغان</p></div>
<div class="m2"><p>کز فغان من فلک عالم به افغان بر گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساز آن کردی که درمان سازی از بهر مجیر</p></div>
<div class="m2"><p>بر مگیر این رنج کو دردت به درمان بر گرفت</p></div></div>