---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>تا بسته بند زلف پریشان گشاده ای</p></div>
<div class="m2"><p>صد نافه مشگ بر گل خندان گشاده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را که دست بر رگ صد دل نهاده ایم</p></div>
<div class="m2"><p>دل بسته ای به زلف و رگ جان گشاده ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سینه مکن به بستن دل زان قبل که تو</p></div>
<div class="m2"><p>دل بسته ای نه ملک خراسان گشاده ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خندی ز گریه من و آنگه گمان بری</p></div>
<div class="m2"><p>کاب بقا ز چشمه حیوان گشاده ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بستیم عهد که دل نشکنی دگر</p></div>
<div class="m2"><p>چونست پس که گوی گریبان گشاده ای؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتیم که بر مجیر گشایی در وصال</p></div>
<div class="m2"><p>تو هر چه در فراق درست آن گشاده ای</p></div></div>