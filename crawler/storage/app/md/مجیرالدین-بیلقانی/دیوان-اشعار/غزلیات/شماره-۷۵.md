---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>می دان که باز راه ستم بر گرفته ای</p></div>
<div class="m2"><p>کز پیش گوش زلف به خم بر گرفته ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریاب کار خود که ز ما دل ربوده ای</p></div>
<div class="m2"><p>دزدیده مهر خاتم جم بر گرفته ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر جا که بند غم به دلی بر نهاده ای</p></div>
<div class="m2"><p>صد سلسله ز پای ستم بر گرفته ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی جفا و جور بهم بر توان گرفت</p></div>
<div class="m2"><p>رفتی وفا و مهر بهم بر گرفته ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک توایم خون دل ما مریز از آنک</p></div>
<div class="m2"><p>ما را ز خاک نز پی غم بر گرفته ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راضی است دل به عشوه خشک و حدیث تر</p></div>
<div class="m2"><p>وین قاعده به دور تو هم بر گرفته ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بشکسته ای زیاده ز دلها دل مجیر</p></div>
<div class="m2"><p>پس در بهای وصل به کم بر گرفته ای</p></div></div>