---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>خسروی کاینه روی ظفر خنجر اوست</p></div>
<div class="m2"><p>رونق سلطنت از تیغ ظفرپرور اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بام بی در که فلک کنیت و گردون لقب است</p></div>
<div class="m2"><p>عاشق و شیفته خدمت بام و در اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس ازین گر ننهد فتنه کله کژ چه عجب؟</p></div>
<div class="m2"><p>کان کله کش سر انصاف بود بر سر اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قرص خورشید که چون چنبر زرین رسن است</p></div>
<div class="m2"><p>جسته هر صبحدمی چون رسن از چنبر اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرخ از آن شادی او خورد که در بزم جلال</p></div>
<div class="m2"><p>آب حیوان به صفت قطره ای از ساغر اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سایه پر همای از چه سعادت اثرست؟</p></div>
<div class="m2"><p>زانکه از فر ملک خاصیتی در پر اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتح اگر شد خلف تیغ دو رویش چه عجب؟</p></div>
<div class="m2"><p>چون شب و روز عروس ظفر اندر بر اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نرد دولت که برد زو؟ که فلک را گه لعب</p></div>
<div class="m2"><p>مهره گر دوست و گرده همه در ششدر اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اوست در خورد جهان گر نه بدین همت و قدر</p></div>
<div class="m2"><p>این کلوخی که جهانست چه اندر خور اوست؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو نی بر دل شکر گرهی هست از آنک</p></div>
<div class="m2"><p>رشگ خورد سخن خوبتر از شکر اوست</p></div></div>
<div class="b2" id="bn11"><p>هر زمانش ز فلک تحفه جلالی دگرست</p>
<p>همچو چوگانش برین گوی هلالی دگرست</p></div>