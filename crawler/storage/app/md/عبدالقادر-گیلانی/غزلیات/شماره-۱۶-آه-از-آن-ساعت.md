---
title: >-
    شمارهٔ ۱۶ - آه از آن ساعت
---
# شمارهٔ ۱۶ - آه از آن ساعت

<div class="b" id="bn1"><div class="m1"><p>یا رب آن ساعت که خلق از ما نیارد هیچ یاد</p></div>
<div class="m2"><p>رحمت خود کن قرین ما الی یوم التّناد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نامه نیکان شده برطاعت آیا چون کنم</p></div>
<div class="m2"><p>نامه های ما بدان چیزی ندارد جزسواد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینچنین کالای پرعیبی که گردد روز ماست</p></div>
<div class="m2"><p>گرنبودش روز بازارش بنامت جز کساد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عید شد عیدی به رحمت ده خداوندا به ما</p></div>
<div class="m2"><p>ورتو ندهی ازکه جویند بندگان نامراد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ردمکن یا رب تو ما را چون به بازار الست</p></div>
<div class="m2"><p>عیبهای ما همه دیدی وکردی نامراد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب رسن در گردن اندازم بگریم زار زار</p></div>
<div class="m2"><p>از غم عمر عزیز خود که بر دادم به باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این زمان از بس که بی او زندگانی می کنم</p></div>
<div class="m2"><p>وقت مردن جان نمی دانیم چون خواهیم داد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آه از آن ساعت که عزرائیل قصد جان کند</p></div>
<div class="m2"><p>جان شیرین را بباید داد ولب نتوان گشاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا دم آخر چه خواهد کرد با ما آه ،آه</p></div>
<div class="m2"><p>ای خوشا وقت کسی کز مادرش هرگز نزاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نامه می خوانند و می گویند کرام الکاتبین</p></div>
<div class="m2"><p>درجمیع عمر این بنده نیاورد خوف یاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیش تابوتم منادی کن بگو این بنده ای است</p></div>
<div class="m2"><p>کو گنه بسیار کرده برخدا کرد اعتماد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یا رب آن کس را بیمرزی که بعد از مرگ ما</p></div>
<div class="m2"><p>روح ما را او به تکبیری کند گهگاه یاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گربخاکم بگذری یا بگذرم بر خاطرت</p></div>
<div class="m2"><p>این دعا می کن که یا رب گور او پر نور باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رحم خواهد کرد بر من خواهد آمرزیدنم</p></div>
<div class="m2"><p>روی زرد خود چو بر خاک لحد خواهم نهاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>محیی گر چه بس بدی کرده ندارد نیکوئی</p></div>
<div class="m2"><p>لیک میدارد به جان درحق نیکان اعتماد</p></div></div>