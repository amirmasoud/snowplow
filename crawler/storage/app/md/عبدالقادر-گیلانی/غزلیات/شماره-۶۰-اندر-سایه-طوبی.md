---
title: >-
    شمارهٔ ۶۰ - اندر سایه طوبی
---
# شمارهٔ ۶۰ - اندر سایه طوبی

<div class="b" id="bn1"><div class="m1"><p>اشک سرخ و روی زرد من گواه است ای کریم</p></div>
<div class="m2"><p>برکمال عشق دیدار تو بالله العظیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی هوای تو هوادار تو کی خرّم شود</p></div>
<div class="m2"><p>درهوای غرفه های قصر جنّات النّعیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش عشق تو را ای دوست نتواند نشاند</p></div>
<div class="m2"><p>تا ابد در دل اگر شعله زند نار جحیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بیندازی تو بر دوزخ تجلّی جمال</p></div>
<div class="m2"><p>نیک و بد دارند منّت تا ابد باشد مقیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرنبودی وصل تو باشد قرین وصل تو</p></div>
<div class="m2"><p>بعد چندین قرن ،چون زنده شود عظم رمیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با تو عهدی بسته ام ای دوست در روز ازل</p></div>
<div class="m2"><p>تا ابد خواهیم بودن برهمان عهد قدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چار جویِ آب و شهد و شیر و خُمر اندر بهشت</p></div>
<div class="m2"><p>شربت بیمارِ دیدارِ تو نبوَد ای حکیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب حوض کوثر اندر سایه طوبی عطش</p></div>
<div class="m2"><p>کِی نشاندی گر نبودی از سر کویت نسیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برصراط پل اگر دوزخ بود چون نگذرد</p></div>
<div class="m2"><p>بی سروپائی که رفته برصراط مستقیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوست اندر گوش عاشق راز گوید روز وصل</p></div>
<div class="m2"><p>نیست اندر خورد گوش هرکس این درّ یتیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دربرون پرده باشد این همه خوف و رجا</p></div>
<div class="m2"><p>در درون پرده رو کانجا امید است و نه بیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پای گدایان بر در او شی لله بر زنید</p></div>
<div class="m2"><p>تا شما را بخشد آنچه دارد آن شاه کریم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دولت دیدار حق محیی چو یابی در بهشت</p></div>
<div class="m2"><p>نور آن در طالعِ تو ، باشد از لطف عمیم</p></div></div>