---
title: >-
    شمارهٔ ۳۳ - خیمه به محشر
---
# شمارهٔ ۳۳ - خیمه به محشر

<div class="b" id="bn1"><div class="m1"><p>طبل قیامت بکوفت آن ملک نفخ صور</p></div>
<div class="m2"><p>کاتب منشور ماست مالک یوم النّشور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر زلحد برزدیم خیمه به محشر زدیم</p></div>
<div class="m2"><p>بی خدا اندر لحد چند نباشم صبور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازسرشوق ونشاط پای نهم بر صراط</p></div>
<div class="m2"><p>تا زدم گرمِ ما گرم شود آن نشور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که ندادی تو مال درطلبِ آن جمال</p></div>
<div class="m2"><p>ما به تو بگذاشتیم دیدن دیدارِ حور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مست خدائیم ما ، کِی به خود آئیم ما</p></div>
<div class="m2"><p>ساقی ما چون خداست باده شراب طهور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نورخدا در نظرگاه تجلّی حق</p></div>
<div class="m2"><p>با تو کند آنچه کرد با حجر کوه طور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وقت تجلّی از اودیده بینا مجوی</p></div>
<div class="m2"><p>اوچو نماید جمال، چشمِ تورا زوست نور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه به نزدیکِ اوست دولتِ جاوید یافت</p></div>
<div class="m2"><p>رویِ سعادت ندید آنکه از او ماند دور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مژده وصل خدا گر به لحد بشنویم</p></div>
<div class="m2"><p>زنده شود جان و تن پیشتر از نفخِ صور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حور چو آرا کنند رو به سوی ما کنند</p></div>
<div class="m2"><p>چشم نگه دار از آن ،دوست بود بس غیور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مست تو قصر بهشت کرده به زیرو زبر</p></div>
<div class="m2"><p>چون نکند زانکه نیست هستیِ او بی قصور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرچه تو قصر بهشت کرده ای عنبر سرشت</p></div>
<div class="m2"><p>از جگر سوخته ، می برم آنجا بخور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>میکند او بهرِدوست هرنفسی ماتمی</p></div>
<div class="m2"><p>محیی ماتم زده کی کند ای دوست شور</p></div></div>