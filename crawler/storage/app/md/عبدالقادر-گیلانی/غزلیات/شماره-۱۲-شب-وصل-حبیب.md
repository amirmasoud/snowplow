---
title: >-
    شمارهٔ ۱۲ - شب وصل حبیب
---
# شمارهٔ ۱۲ - شب وصل حبیب

<div class="b" id="bn1"><div class="m1"><p>آن که آتش افکند درخلق جانان من است</p></div>
<div class="m2"><p>وانکه می سوزد از آن رویش همین جان من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا شدم دیوانه پیشم قصر شه ویرانه است</p></div>
<div class="m2"><p>کآسمان فیروزه ای ازطاق ایوان من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق ورزیدم نهان ای وای بر من کین زمان</p></div>
<div class="m2"><p>نقل هرمجلس حدیث عشق پنهان من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرفلک خواهد که سازد خانه مردم خراب</p></div>
<div class="m2"><p>گو مکش زحمت که کار چشم گریان من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه در دم بگذرد باشد شبی وصل حبیب</p></div>
<div class="m2"><p>وآنچه را پایان نباشد روز هجران من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرد محیی و سیه پوشید بهر ماتمش</p></div>
<div class="m2"><p>هرکجا ورقی بود ز اوراق دیوان من است</p></div></div>