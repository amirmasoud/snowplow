---
title: >-
    شمارهٔ ۱۷ - آرزو دارم
---
# شمارهٔ ۱۷ - آرزو دارم

<div class="b" id="bn1"><div class="m1"><p>روزنی جز زخم تیرش در سرای تن مباد</p></div>
<div class="m2"><p>غیر داغ حسرتش تا بام آن روزن مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق روی بتان یا رب مبادا هیچکس</p></div>
<div class="m2"><p>ورکسی عاشق شود یارا به سان من مباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرده از تیرجفا هر لحظه چاکی در دلم</p></div>
<div class="m2"><p>آنکه از خاریش هرگز چاک در دامن مباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهرومه را روشنی از پرتو رخسار توست</p></div>
<div class="m2"><p>بی رخت هرگز چراغ مهر ومه روشن مباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جنّت عاشق چو باشد بعد مردن کوی یار</p></div>
<div class="m2"><p>مرغ جانم را جز آن دیوار و در مسکن مباد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آرزو دارم که در عشقت تن بیمار من</p></div>
<div class="m2"><p>خالی از افغان وزاری فارغ از شیون مباد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاج شاهی چون شود با خاک یکسان عاقبت</p></div>
<div class="m2"><p>افسر محیی به جز خاکستر گلخن مباد</p></div></div>