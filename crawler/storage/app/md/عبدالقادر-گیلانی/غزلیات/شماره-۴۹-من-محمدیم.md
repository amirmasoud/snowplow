---
title: >-
    شمارهٔ ۴۹ - من محمّدیم
---
# شمارهٔ ۴۹ - من محمّدیم

<div class="b" id="bn1"><div class="m1"><p>غلام حلقه بگوش رسول ساداتم</p></div>
<div class="m2"><p>ره نجات نموده حبیب آیاتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کفایت است ز روح رسول و اولادش</p></div>
<div class="m2"><p>همیشه در دو جهان جمله مهمّاتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زغیر آل نبی اگر حاجتی طلبم</p></div>
<div class="m2"><p>روا مباد یکی از هزار حاجاتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم ز حبّ محمّد پر است و آل مجید او</p></div>
<div class="m2"><p>گواه حال من است این همه حکایاتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو ذرّه ذرّه شود این تنم به خاک لحد</p></div>
<div class="m2"><p>تو بشنوی صلوات از جمیع ذرّاتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمینه خادم خدّام خاندان توام</p></div>
<div class="m2"><p>ز خادمی تو دائم بود مباهاتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلام گویم و صلوات با تو هر نفسی</p></div>
<div class="m2"><p>قبول کن به کرم این سلام و صلواتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گناه بی حدّ من بین تو یا رسول الله</p></div>
<div class="m2"><p>شفاعتی بکن و محو کن خیاناتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرآنکه بدتر از اونیست من از او بترم</p></div>
<div class="m2"><p>ندانم این که بتو چون شود ملاقاتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زنیک و بد همه دانند من محمّدیم</p></div>
<div class="m2"><p>خلائقی که کند گوش بر مقالاتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگوی محیی که بهر نجات میگویم</p></div>
<div class="m2"><p>درود سرور کونین در مناجاتم</p></div></div>