---
title: >-
    شمارهٔ ۴ - همت مردانه
---
# شمارهٔ ۴ - همت مردانه

<div class="b" id="bn1"><div class="m1"><p>بی حجابانه درآ از در کاشانه ما</p></div>
<div class="m2"><p>که کسی نیست به جز وردِ تو درخانه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بیائی به سر تربت ویرانه ما</p></div>
<div class="m2"><p>بینی از خون جگر آب زده خانه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فتنه انگیز مشو کاکل مشکین مگشای</p></div>
<div class="m2"><p>تاب زنجیر ندارد دل دیوانه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ باغ ملکوتیم و دراین دیر خراب</p></div>
<div class="m2"><p>می شود نور تجلّای خدا دانه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با احد در لحد تنگ بگوئیم که دوست</p></div>
<div class="m2"><p>آشنایم توئی و غیر تو بیگانه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نکیر آید و پرسد که بگو ربّ تو کیست</p></div>
<div class="m2"><p>گویم آنکس که ربود این دل دیوانه ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منکر نعره ما کو که به ما عربده کرد</p></div>
<div class="m2"><p>تا به محشر شنود نعره مستانه ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکر الله که نمردیم و رسیدیم به دوست</p></div>
<div class="m2"><p>آفرین باد بر این همت مردانه ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محیی بر شمع تجلای جمالش می سوخت</p></div>
<div class="m2"><p>دوست می گفت زهی همت پروانه ما</p></div></div>