---
title: >-
    شمارهٔ ۴۶ - خانه عشق
---
# شمارهٔ ۴۶ - خانه عشق

<div class="b" id="bn1"><div class="m1"><p>کی بود آیا که بنمائی جمال با کمال</p></div>
<div class="m2"><p>زنده گردند ماهیان مرده از آب زلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درقیاما حشر را حاجت به نفخ صور نیست</p></div>
<div class="m2"><p>بگذرد بر کوی خلقی مژده کوی وصال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جهنم خوش توان بودن اگر یکبار تو</p></div>
<div class="m2"><p>در همه عمر آئی و پرسی و گوئی چیست حال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر در این زندان تو با مائی ،نگشتم من ملول</p></div>
<div class="m2"><p>گر در آن زندان به ما باشی کجا باشد ملال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خانه عشق ،دلست و آنچنان پر شد ز دوست</p></div>
<div class="m2"><p>کانچه غیر دوست است ، در وی نمی یابد مجال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر سر موئی شود فردوس اعلی اشک او</p></div>
<div class="m2"><p>گنجد اندر خانه عشق ، بود امری محال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون خلقی ریخت بیکین هیچ دانی کیست آن</p></div>
<div class="m2"><p>ور تو نام او نگوئی بگذرانش در خیال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشتگان نعره زنانند هیچ دانی کیست آن</p></div>
<div class="m2"><p>برکشنده هیچ نه ور کشته را باشد وبال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از سر دنیا برای دوست بگذشتن چه سود</p></div>
<div class="m2"><p>سهل باشد در گذشتن از شریک پیر زال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سایه طوبی و حوض کوثر و باغ بهشت</p></div>
<div class="m2"><p>خوش مقامی باشد اما با جمال ذوالجلال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کی شود بی جذب مغناطیس وصلش متصل</p></div>
<div class="m2"><p>ذره ذره خاک آدم بعد چندین ماه و سال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عشق و مستی و جنون در طالع ما دیده اند</p></div>
<div class="m2"><p>چون ز مادر زاده گشتیم و پدر بگشاد فال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اوّل و آخر توئی و ظاهر و باطن توئی</p></div>
<div class="m2"><p>کیست دیگر غیر تو و چیست چندین قیل و قال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو زما و ما ز بوی تو چنین گشتیم مست</p></div>
<div class="m2"><p>ورنه مستی چنین ، بی می ندارد احتمال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بوی یار آمد به ما آری بیاید بوی دوست</p></div>
<div class="m2"><p>در مشام آن که دارد او به آن یار اتصال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بعد چندین قرن گویند رحمه الله علیه</p></div>
<div class="m2"><p>چون بخوانند خلق شعر محیی صاحب کمال</p></div></div>