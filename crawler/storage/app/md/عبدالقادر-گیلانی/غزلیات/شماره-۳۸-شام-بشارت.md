---
title: >-
    شمارهٔ ۳۸ - شام بشارت
---
# شمارهٔ ۳۸ - شام بشارت

<div class="b" id="bn1"><div class="m1"><p>تو لذت عمل را از کارزار ما پرس</p></div>
<div class="m2"><p>آئین سلطنت را از حال زار ما پرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن لذّتی که باشد از اشتهاد صادق</p></div>
<div class="m2"><p>شام بشارت وصل از روزگار ما پرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجنون عشق ما را از باغ و راغ کم گوی</p></div>
<div class="m2"><p>از وی تو سور جوی و بوی بهار ما پرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خان و مان وهرکس ، کردم خراب او را</p></div>
<div class="m2"><p>منبعد اگر بخواهی اندر دیار ما پرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرشب زلطف پرسم احوال تو چگونه است</p></div>
<div class="m2"><p>ذوق خطاب ما را از دل نگار ما پرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برتربت خراب عشاق ما نظر کن</p></div>
<div class="m2"><p>واز ذرّه ذرّه خاکش تو انتظار ما پرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق نِئی چه دانی درد فراق ما را</p></div>
<div class="m2"><p>رو رو تو این مصیبت از سوگوار ما پرس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشق که از غم من کاهیده گشت و جان داد</p></div>
<div class="m2"><p>این مرغزار او را از مرغزار ما پرس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو صاف دل چه دانی نالیدن سحرگه</p></div>
<div class="m2"><p>آئین دردمندی از درد خارما پرس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل از غم دو عالم فارغ کن و پس آنگه</p></div>
<div class="m2"><p>آنی به پیش محیی از لطف یار ما پرس</p></div></div>