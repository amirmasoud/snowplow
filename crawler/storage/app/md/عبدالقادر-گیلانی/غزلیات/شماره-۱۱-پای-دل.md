---
title: >-
    شمارهٔ ۱۱ - پای دل
---
# شمارهٔ ۱۱ - پای دل

<div class="b" id="bn1"><div class="m1"><p>پای دل در کوی عشقت تا به زانو در گِل است</p></div>
<div class="m2"><p>همّتی دارید با من زانکه کاری مشکل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ندانم کین دل دیوانه را مقصود چیست</p></div>
<div class="m2"><p>کو همیشه سوی سرگردانی من مایل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فیل محمودی فرو ماند اگر بیند به خواب</p></div>
<div class="m2"><p>بارسنگینی که از درد تو ما را بر دل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دل آواره آخرچند میگوئی مگو</p></div>
<div class="m2"><p>اندران کوئی که پای صدهزاران در گل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همدمم آه است، محرم غم در ایام شباب</p></div>
<div class="m2"><p>وقت عیش و نوجوانی وچه ناخوش حاصل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خودبخود گویم سخنها بگریم زار زار</p></div>
<div class="m2"><p>محرم راز غریبان لابد اشک سائل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محیی با این زندگانی گر گمان داری که تو</p></div>
<div class="m2"><p>راه حق رفتی یقین میدان نه ، فکر باطل است</p></div></div>