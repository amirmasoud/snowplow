---
title: >-
    شمارهٔ ۶۲ - قلندرخانه عشق
---
# شمارهٔ ۶۲ - قلندرخانه عشق

<div class="b" id="bn1"><div class="m1"><p>ما به جنّت از برای کار دیگر می‌رویم</p></div>
<div class="m2"><p>نه تفرّج کردن طوبی و کوثر می‌رویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقصد ما حسن یوسف باشد اندر شهر مصر</p></div>
<div class="m2"><p>ما نه در مصر از برای قند و شکّر می‌رویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندر آن خلوت که در وی ره نیابد جبرئیل</p></div>
<div class="m2"><p>بی‌سر و پا ما به پیش دوست اکثر می‌رویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌گریزند زاهدان خشک از تردامنی</p></div>
<div class="m2"><p>ما بر خورشید خود با دامن تر می‌رویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پارسا گوید به کوی ما بیا شو نام‌نیک</p></div>
<div class="m2"><p>ما در آن کوچه خدا داناست کمتر می‌رویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما ز دنیا کو قلندرخانهٔ عشق خداست</p></div>
<div class="m2"><p>سوی عقبی عاشق و مست و قلندر می‌رویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیخ ما عشق است ما پی در پی او تا ابد</p></div>
<div class="m2"><p>بی‌عصا و خرقه و کجکول و لنگر می‌رویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زَهرهٔ ما را مبر از قهرها با نیکویی</p></div>
<div class="m2"><p>ما اگر نیکیم و گر بد هم بدان در می‌رویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر کفن ما را تو ای غسّال بوی خوش مسا</p></div>
<div class="m2"><p>ما به گور از بهر آن دلبر، معطّر می‌رویم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دولت دیدار می‌خواهیم در جنّات عدن</p></div>
<div class="m2"><p>ما نه آنجا از برای زیور و زر می‌رویم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>محیی ما را همچو کوه افسرده می‌بینی ولی</p></div>
<div class="m2"><p>ما به سر چون ابر خوش بی‌پا و بی‌سر می‌رویم</p></div></div>