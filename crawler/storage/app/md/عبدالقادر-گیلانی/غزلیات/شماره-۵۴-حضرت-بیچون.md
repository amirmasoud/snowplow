---
title: >-
    شمارهٔ ۵۴ - حضرت بیچون
---
# شمارهٔ ۵۴ - حضرت بیچون

<div class="b" id="bn1"><div class="m1"><p>بی تماشای جمالت روضه را هامون کنم</p></div>
<div class="m2"><p>حور عین را از درون قصرها بیرون کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حور زیبا روی را خواهیم دادن سه طلاق</p></div>
<div class="m2"><p>گرنه رو در نور روی حضرت بیچون کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روضه را جلوه مده رضوان که بالله العظیم</p></div>
<div class="m2"><p>من به یک آهش بسوزانم تو را مجنون کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب دارد ای بهشتی کوثر و طوبای تو</p></div>
<div class="m2"><p>من به یکدم کار و بار هر دو را یکسو کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرنه در فردوس باشد دیدن دیدار دوست</p></div>
<div class="m2"><p>زاویه در هاویه بگزیده دیده خون کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایهاالعشّاق اگر معشوق بردارد نقاب</p></div>
<div class="m2"><p>دیده ما در خور او نیست ،آیا چون کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محیی با ما دار خود را بی ریاضت تا تو را</p></div>
<div class="m2"><p>چون جنید و شبلی و بایزید و ذوالنّون کنم</p></div></div>