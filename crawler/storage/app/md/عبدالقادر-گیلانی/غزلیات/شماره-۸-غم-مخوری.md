---
title: >-
    شمارهٔ ۸ - غم مخوری
---
# شمارهٔ ۸ - غم مخوری

<div class="b" id="bn1"><div class="m1"><p>غم مخوری که عاقبت جای تو صدر جنت است</p></div>
<div class="m2"><p>روی دل تو تا ابد سوی رضای حضرت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم مخوری که مرغ جان چون زتنت همی پرد</p></div>
<div class="m2"><p>منزل آشیان او مقعدصدق نیت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم مخوری که این تنت چون به لحد فرو رود</p></div>
<div class="m2"><p>خاک تن تو تا به حشرغرقه آب رحمت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم مخوری که حق تو را از همه خلق برگزید</p></div>
<div class="m2"><p>این زجمال لطف او نه زکمال خدمت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم مخوری که روزوشب سیصدوشصت لطف حق</p></div>
<div class="m2"><p>در توهمی نظرکند اینهمه از محبت است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم مخوری که هر کجا که تویی خدای توست</p></div>
<div class="m2"><p>درطلب خدا ترا بنده بگو چه زحمت است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم مخوری که عشق خود با گل تو به هم سرشت</p></div>
<div class="m2"><p>عشق خدای تو به تو همدم اصل خلقت است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم مخوری که با تو هست آن دگری به غیر تو</p></div>
<div class="m2"><p>او نه تو هست ؛نه تو او گفتن او به رحمت است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم مخوری که بی شراب مست وخراب گشته ای</p></div>
<div class="m2"><p>محتسبان شهررا گو که شراب جنت است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غم مخوری که حق ترا بنده خویش خوانده است</p></div>
<div class="m2"><p>بندگی خدا ترا؛ محیی نشان دولت است</p></div></div>