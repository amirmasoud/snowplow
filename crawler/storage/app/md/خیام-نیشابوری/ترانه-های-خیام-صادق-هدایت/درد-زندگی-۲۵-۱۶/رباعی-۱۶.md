---
title: >-
    رباعی ۱۶
---
# رباعی ۱۶

<div class="b" id="bn1"><div class="m1"><p>امروز که نوبت جوانی من است،</p></div>
<div class="m2"><p>می نوشم از آن‌که کامرانی من است؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیبم مکنید. گرچه تلخ است خوش است،</p></div>
<div class="m2"><p>تلخ است، از آن‌که زندگانی من است.</p></div></div>