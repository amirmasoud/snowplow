---
title: >-
    رباعی ۱۷
---
# رباعی ۱۷

<div class="b" id="bn1"><div class="m1"><p>گر آمدنم به من بُدی، نامَدَمی.</p></div>
<div class="m2"><p>ور نیز شدن به من بدی، کی شدمی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بِهْ زان نَبُدی که اندرین دیْرِ خراب،</p></div>
<div class="m2"><p>نه آمدمی، نه شدمی، نه بُدَمی.</p></div></div>