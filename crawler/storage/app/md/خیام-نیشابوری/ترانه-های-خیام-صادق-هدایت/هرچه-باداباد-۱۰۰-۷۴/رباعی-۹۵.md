---
title: >-
    رباعی ۹۵
---
# رباعی ۹۵

<div class="b" id="bn1"><div class="m1"><p>چون عمر به سر رسد، چه بغداد چه بلخ،</p></div>
<div class="m2"><p>پیمانه چو پر شود، چه شیرین و چه تلخ؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش باش که بعد از من و تو ماه بسی،</p></div>
<div class="m2"><p>از سَلْخ به غُرّه آید، از غُرّه به سَلْخ!</p></div></div>