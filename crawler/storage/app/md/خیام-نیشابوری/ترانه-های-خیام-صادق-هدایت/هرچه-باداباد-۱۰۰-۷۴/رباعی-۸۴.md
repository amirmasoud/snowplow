---
title: >-
    رباعی ۸۴
---
# رباعی ۸۴

<div class="b" id="bn1"><div class="m1"><p>* آنان‌که اسیر عقل و تمییز شدند،</p></div>
<div class="m2"><p>در حسرتِ هست‌ونیست ناچیز شدند؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو با خبرا، تو آب انگور گُزین،</p></div>
<div class="m2"><p>کان بی‌خبران به غوره مِیْویز شدند!</p></div></div>