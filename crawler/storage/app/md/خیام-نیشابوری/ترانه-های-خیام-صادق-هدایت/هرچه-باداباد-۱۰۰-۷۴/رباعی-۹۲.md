---
title: >-
    رباعی ۹۲
---
# رباعی ۹۲

<div class="b" id="bn1"><div class="m1"><p>* من هیچ ندانم که مرا آن‌که سرشت،</p></div>
<div class="m2"><p>از اهل بهشت کرد، یا دوزخ زشت؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامی و بتی و بَربَطی بر لب کِشت.</p></div>
<div class="m2"><p>این هر سه مرا نقد و تو را نسیه بهشت.</p></div></div>