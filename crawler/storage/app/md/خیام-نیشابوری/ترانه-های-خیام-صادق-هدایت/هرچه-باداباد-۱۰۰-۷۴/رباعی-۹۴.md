---
title: >-
    رباعی ۹۴
---
# رباعی ۹۴

<div class="b" id="bn1"><div class="m1"><p>چون آمدنم به من نَبُد روز نخست،</p></div>
<div class="m2"><p>وین رفتنِ بی‌مراد عَزمی است درست،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برخیز و میان ببند ای ساقی چُسْت،</p></div>
<div class="m2"><p>کاندوهِ جهان به می فروخواهم‌شست.</p></div></div>