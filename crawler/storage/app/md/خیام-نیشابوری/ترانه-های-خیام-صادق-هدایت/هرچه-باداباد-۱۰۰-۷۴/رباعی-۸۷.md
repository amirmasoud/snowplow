---
title: >-
    رباعی ۸۷
---
# رباعی ۸۷

<div class="b" id="bn1"><div class="m1"><p>* گویند که دوزخی بُوَد عاشق و مست،</p></div>
<div class="m2"><p>قولی است خلاف، دل در آن نتوان بست،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عاشق و مست دوزخی خواهد بود،</p></div>
<div class="m2"><p>فردا باشد بهشت همچون کفِ دست!</p></div></div>