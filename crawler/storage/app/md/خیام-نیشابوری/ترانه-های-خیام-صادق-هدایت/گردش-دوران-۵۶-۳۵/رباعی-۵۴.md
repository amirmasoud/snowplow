---
title: >-
    رباعی ۵۴
---
# رباعی ۵۴

<div class="b" id="bn1"><div class="m1"><p>آن قصر که بهرام درو جام گرفت،</p></div>
<div class="m2"><p>آهو بچه کرد و روبَهْ آرام گرفت؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهرام که گور می‌گرفتی همه عمر،</p></div>
<div class="m2"><p>دیدی که چگونه گور بهرام گرفت؟</p></div></div>