---
title: >-
    رباعی ۳۸
---
# رباعی ۳۸

<div class="b" id="bn1"><div class="m1"><p>یارانِ موافق همه از دست شدند،</p></div>
<div class="m2"><p>در پای اجل یکان‌یکان پَست شدند،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بودیم به یک شراب در مجلسِ عمر،</p></div>
<div class="m2"><p>یک دَوْر ز ما پیشترک مست شدند!</p></div></div>