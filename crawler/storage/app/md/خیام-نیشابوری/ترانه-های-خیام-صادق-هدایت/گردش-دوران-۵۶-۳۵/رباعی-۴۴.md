---
title: >-
    رباعی ۴۴
---
# رباعی ۴۴

<div class="b" id="bn1"><div class="m1"><p>اجزای پیاله‌ای که درهم پیوست،</p></div>
<div class="m2"><p>بشکستنِ آن روا نمی‌دارد مست،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندین سر و ساقِ نازنین و کفِ دست،</p></div>
<div class="m2"><p>از مِهرِ که پیوست و به کینِ که شکست؟</p></div></div>