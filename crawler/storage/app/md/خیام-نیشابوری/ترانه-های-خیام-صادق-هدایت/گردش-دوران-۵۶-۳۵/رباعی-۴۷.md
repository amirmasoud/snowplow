---
title: >-
    رباعی ۴۷
---
# رباعی ۴۷

<div class="b" id="bn1"><div class="m1"><p>می خور که به زیرِ گِل بسی خواهی خفت،</p></div>
<div class="m2"><p>بی مونس و بی رفیق و بی همدم و جفت؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار به کس مگو تو این رازِ نهفت:</p></div>
<div class="m2"><p>هر لاله که پَژْمُرد، نخواهد بِشْکفت.</p></div></div>