---
title: >-
    رباعی ۴۳
---
# رباعی ۴۳

<div class="b" id="bn1"><div class="m1"><p>جامی است که عقل آفرین می‌زندش،</p></div>
<div class="m2"><p>صد بوسه زِ مِهْر بر جَبین می‌زندش؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این کوزه‌گر دَهْر چنین جامِ لطیف</p></div>
<div class="m2"><p>می‌سازد و باز بر زمین می‌زندش!</p></div></div>