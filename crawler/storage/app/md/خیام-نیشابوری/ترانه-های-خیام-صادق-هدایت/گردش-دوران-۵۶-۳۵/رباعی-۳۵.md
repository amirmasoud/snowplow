---
title: >-
    رباعی ۳۵
---
# رباعی ۳۵

<div class="b" id="bn1"><div class="m1"><p>افسوس که نامهٔ جوانی طی شد،</p></div>
<div class="m2"><p>وان تازه‌بهار زندگانی دی شد؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حالی که ورا نام جوانی گفتند،</p></div>
<div class="m2"><p>معلوم نشد که او کیْ آمد، کیْ شد!</p></div></div>