---
title: >-
    رباعی ۴۸
---
# رباعی ۴۸

<div class="b" id="bn1"><div class="m1"><p>* پیری دیدم به خانهٔ خَمّاری،</p></div>
<div class="m2"><p>گفتم: نکنی ز رفتگان اِخباری؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا، می خور که همچو ما بسیاری،</p></div>
<div class="m2"><p>رفتند و کسی بازنیامد باری!</p></div></div>