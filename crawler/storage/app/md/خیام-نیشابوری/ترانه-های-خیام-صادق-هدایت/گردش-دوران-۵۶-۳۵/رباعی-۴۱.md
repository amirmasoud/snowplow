---
title: >-
    رباعی ۴۱
---
# رباعی ۴۱

<div class="b" id="bn1"><div class="m1"><p>یک قطرهٔ آب بود و با دریا شد،</p></div>
<div class="m2"><p>یک ذرّهٔ خاک و با زمین یکتا شد،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد شدن تو اندرین عالم چیست؟</p></div>
<div class="m2"><p>آمد مگسی پدید و ناپیدا شد.</p></div></div>