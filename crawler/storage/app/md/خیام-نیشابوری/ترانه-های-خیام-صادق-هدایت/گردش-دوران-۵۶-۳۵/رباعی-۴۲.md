---
title: >-
    رباعی ۴۲
---
# رباعی ۴۲

<div class="b" id="bn1"><div class="m1"><p>* می‌پرسیدی که چیست این نقشِ مجاز،</p></div>
<div class="m2"><p>گر برگویم حقیقتش هست دراز،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقشی است پدید آمده از دریایی،</p></div>
<div class="m2"><p>و آنگاه شده به قَعْرِ آن دریا باز.</p></div></div>