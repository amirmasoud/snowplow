---
title: >-
    رباعی ۳۴
---
# رباعی ۳۴

<div class="b" id="bn1"><div class="m1"><p>نیکی و بدی که در نهادِ بشر است،</p></div>
<div class="m2"><p>شادی و غمی که در قضا و قدر است،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با چرخ مکن حواله کاندر رَهِ عقل،</p></div>
<div class="m2"><p>چرخ از تو هزار بار بیچاره‌تر است.</p></div></div>