---
title: >-
    رباعی ۳۳
---
# رباعی ۳۳

<div class="b" id="bn1"><div class="m1"><p>در گوشِ دلم گفت فلک پنهانی:</p></div>
<div class="m2"><p>حُکمی که قضا بُوَد ز من می‌دانی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گردشِ خود اگر مرا دست بُدی،</p></div>
<div class="m2"><p>خود را برهاندمی ز سر گردانی.</p></div></div>