---
title: >-
    رباعی ۳۱
---
# رباعی ۳۱

<div class="b" id="bn1"><div class="m1"><p>* تا کی ز چراغِ مسجد و دودِ کُنِشْت؟</p></div>
<div class="m2"><p>تا کی ز زیانِ دوزخ و سودِ بهشت؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو بر سر لوح بین که استادِ قضا</p></div>
<div class="m2"><p>اندر ازل آن‌چه بودنی بود، نوشت.</p></div></div>