---
title: >-
    رباعی ۲۸
---
# رباعی ۲۸

<div class="b" id="bn1"><div class="m1"><p>افلاک که جز غم نفزایند دگر؛</p></div>
<div class="m2"><p>نَنْهَند به جا تا نربایند دگر؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نا آمدگان اگر بدانند که ما</p></div>
<div class="m2"><p>از دَهْر چه می‌کشیم، نایند دگر.</p></div></div>