---
title: >-
    رباعی ۲۷
---
# رباعی ۲۷

<div class="b" id="bn1"><div class="m1"><p>چون روزی و عمر بیش‌وکم نتوان‌کرد،</p></div>
<div class="m2"><p>خود را به کم و بیش دُژَم نتوان‌کرد؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار من و تو چنان‌که رأی من و تو ست</p></div>
<div class="m2"><p>از موم به دست خویش هم نتوان‌کرد.</p></div></div>