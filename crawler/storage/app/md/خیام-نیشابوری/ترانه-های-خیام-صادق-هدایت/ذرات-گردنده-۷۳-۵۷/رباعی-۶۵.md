---
title: >-
    رباعی ۶۵
---
# رباعی ۶۵

<div class="b" id="bn1"><div class="m1"><p>دیدم به سرِ عمارتی مردی فرد،</p></div>
<div class="m2"><p>کاو گِل به لگد می‌زد و خوارش می‌کرد،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان گِل به زبانِ حال با او می‌گفت:</p></div>
<div class="m2"><p>ساکن، که چو من بسی لگد خواهی‌خورد!</p></div></div>