---
title: >-
    رباعی ۶۲
---
# رباعی ۶۲

<div class="b" id="bn1"><div class="m1"><p>چون ابر به نوروز رخِ لاله بشست،</p></div>
<div class="m2"><p>بر خیز و به جامِ باده کن عزمِ درست،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاین سبزه که امروز تماشاگه توست،</p></div>
<div class="m2"><p>فردا همه از خاک تو برخواهد رُسْت!</p></div></div>