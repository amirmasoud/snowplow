---
title: >-
    رباعی ۷۳
---
# رباعی ۷۳

<div class="b" id="bn1"><div class="m1"><p>در کارگهِ کوزه‌گری بودم دوش،</p></div>
<div class="m2"><p>دیدم دو هزار کوزه گویا و خموش؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر یک به زبانِ حال با من گفتند:</p></div>
<div class="m2"><p>«کو کوزه‌گر و کوزه‌خر و کوزه‌فروش؟»</p></div></div>