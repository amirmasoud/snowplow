---
title: >-
    رباعی ۷۰
---
# رباعی ۷۰

<div class="b" id="bn1"><div class="m1"><p>* هان کوزه‌گرا بپای اگر هشیاری،</p></div>
<div class="m2"><p>تا چند کنی بر گِل مردم خواری؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انگشتِ فریدون و کَفِ کیخسرو،</p></div>
<div class="m2"><p>برچرخ نهاده‌ای، چه می‌پنداری؟</p></div></div>