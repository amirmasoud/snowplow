---
title: >-
    رباعی ۶۸
---
# رباعی ۶۸

<div class="b" id="bn1"><div class="m1"><p>زان کوزهٔ میْ که نیست در وی ضرری،</p></div>
<div class="m2"><p>پُر کن قَدَحی بخور، به من دِهْ دگری،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان پیشتر ای پسر که دررَهْگُذری،</p></div>
<div class="m2"><p>خاک من و تو کوزه کند کوزه‌گری.</p></div></div>