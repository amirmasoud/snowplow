---
title: >-
    رباعی ۶۱
---
# رباعی ۶۱

<div class="b" id="bn1"><div class="m1"><p>ابر آمد و زار بر سرِ سبزه گریست،</p></div>
<div class="m2"><p>بی بادهٔ گُلرنگ نمی‌شاید زیست؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این سبزه که امروز تماشاگه ماست،</p></div>
<div class="m2"><p>تا سبزهٔ خاک ما تماشاگه کیست!</p></div></div>