---
title: >-
    رباعی ۶۴
---
# رباعی ۶۴

<div class="b" id="bn1"><div class="m1"><p>می خور که فلک بهر هلاک من و تو،</p></div>
<div class="m2"><p>قصدی دارد به جانِ پاک من و تو؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سبزه نشین و میِ روشن می‌خور؛</p></div>
<div class="m2"><p>کاین سبزه بسی دمد ز خاک من و تو!</p></div></div>