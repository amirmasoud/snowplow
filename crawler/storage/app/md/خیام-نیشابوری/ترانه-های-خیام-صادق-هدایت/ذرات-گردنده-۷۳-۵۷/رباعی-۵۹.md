---
title: >-
    رباعی ۵۹
---
# رباعی ۵۹

<div class="b" id="bn1"><div class="m1"><p>ای پیرِ خردمند پِگَهْ‌تر برخیز،</p></div>
<div class="m2"><p>وان کودکِ خاک‌بیز را بنگر تیز،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پندش ده و گو که، نرم‌نرمک می‌بیز،</p></div>
<div class="m2"><p>مغزِ سرِ کیقباد و چشمِ پرویز!</p></div></div>