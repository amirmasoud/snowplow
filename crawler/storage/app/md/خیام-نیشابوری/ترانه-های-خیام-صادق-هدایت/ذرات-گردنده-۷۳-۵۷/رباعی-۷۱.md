---
title: >-
    رباعی ۷۱
---
# رباعی ۷۱

<div class="b" id="bn1"><div class="m1"><p>در کارگه کوزه‌گری کردم رای،</p></div>
<div class="m2"><p>بر پلهٔ چرخ دیدم استاد به‌پای،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌کرد دلیر کوزه را دسته و سر،</p></div>
<div class="m2"><p>از کَلّهٔ پادشاه و از دست گدای!</p></div></div>