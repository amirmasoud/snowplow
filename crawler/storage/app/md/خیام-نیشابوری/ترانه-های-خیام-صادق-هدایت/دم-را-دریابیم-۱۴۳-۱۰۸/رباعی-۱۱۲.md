---
title: >-
    رباعی ۱۱۲
---
# رباعی ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>چون عهده نمی‌شود کسی فردا را،</p></div>
<div class="m2"><p>حالی خوش کن تو این دلِ سودا را،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می نوش به ماهتاب، ای ماه که ماه</p></div>
<div class="m2"><p>بسیار بگردد و نیابد ما را.</p></div></div>