---
title: >-
    رباعی ۱۳۱
---
# رباعی ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>* تَن زن چو به زیرِ فَلَکِ بی‌باکی،</p></div>
<div class="m2"><p>می نوش چو در جهانِ آفت‌ناکی؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون اوّل و آخِرت به جز خاکی نیست،</p></div>
<div class="m2"><p>انگار که بر خاک نه‌ای در خاکی.</p></div></div>