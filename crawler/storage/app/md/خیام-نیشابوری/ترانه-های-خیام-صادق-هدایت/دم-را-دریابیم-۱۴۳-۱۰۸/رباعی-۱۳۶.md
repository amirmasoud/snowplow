---
title: >-
    رباعی ۱۳۶
---
# رباعی ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>* دوْرانِ جهان بی می و ساقی هیچ است،</p></div>
<div class="m2"><p>بی زمزمهٔ نایِ عراقی هیچ است؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند در احوال جهان می‌نگرم،</p></div>
<div class="m2"><p>حاصل همه عشرت است و باقی هیچ است.</p></div></div>