---
title: >-
    رباعی ۱۳۰
---
# رباعی ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>* ای دوست بیا تا غمِ فردا نخوریم،</p></div>
<div class="m2"><p>وین یک‌دمِ عمر را غنیمت شمریم؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فردا که ازین دیْر کُهَن درگذریم؛</p></div>
<div class="m2"><p>با هفت‌هزارسالگان سربه‌سریم.</p></div></div>