---
title: >-
    رباعی ۱۱۵
---
# رباعی ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>وقت سحر است، خیز ای مایهٔ ناز،</p></div>
<div class="m2"><p>نرمک‌نرمک باده خور و چنگ نواز،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کان‌ها که بجایند نپایند کسی،</p></div>
<div class="m2"><p>و آن‌ها که شدند کس نمی‌آید باز!</p></div></div>