---
title: >-
    رباعی ۱۲۸
---
# رباعی ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>* از آمدنِ بهار و از رفتنِ دی،</p></div>
<div class="m2"><p>اوراقِ وجودِ ما همی‌گردد طی؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می خور، مخور اندوه، که گفته‌است حکیم:</p></div>
<div class="m2"><p>غم‌های جهان چو زَهر و تِریاقش می.</p></div></div>