---
title: >-
    رباعی ۱۳۹
---
# رباعی ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>لب بر لب کوزه بردم از غایت آز،</p></div>
<div class="m2"><p>تا زو طلبم واسطهٔ عمرِ دراز،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب بر لب من نهاد و می‌گفت به راز:</p></div>
<div class="m2"><p>می خور، که بدین جهان نمی‌آیی باز!</p></div></div>