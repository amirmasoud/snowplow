---
title: >-
    رباعی ۱۰۲
---
# رباعی ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>دنیا دیدی و هرچه دیدی هیچ است،</p></div>
<div class="m2"><p>و آن نیز که گفتی و شنیدی هیچ است،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرتاسرِ آفاق دویدی هیچ است،</p></div>
<div class="m2"><p>و آن نیز که در خانه خزیدی هیچ است.</p></div></div>