---
title: >-
    رباعی ۱۰۳
---
# رباعی ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>دنیا به مراد رانده گیر، آخِر چه؟</p></div>
<div class="m2"><p>وین نامهٔ عمر خوانده گیر، آخِر چه؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرم که به کامِ دل بماندی صد سال،</p></div>
<div class="m2"><p>صد سال دگر بمانده گیر، آخر چه؟</p></div></div>