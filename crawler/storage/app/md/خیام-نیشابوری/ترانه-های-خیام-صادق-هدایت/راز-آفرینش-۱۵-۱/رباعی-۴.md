---
title: >-
    رباعی ۴
---
# رباعی ۴

<div class="b" id="bn1"><div class="m1"><p>ای دل تو به ادراکِ معمّا نرسی،</p></div>
<div class="m2"><p>در نکتهٔ زیرکانِ دانا نرسی؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینجا ز می و جام بهشتی می‌ساز،</p></div>
<div class="m2"><p>کانجا که بهشت است رسی یا نرسی </p></div></div>