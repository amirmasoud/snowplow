---
title: >-
    رباعی ۹
---
# رباعی ۹

<div class="b" id="bn1"><div class="m1"><p>اَجرام که ساکنان این ایوان‌اند،</p></div>
<div class="m2"><p>اسبابِ تَرَدُّدِ خردمندان‌اند،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هان تا سرِ رشتهٔ خِرَد گُم نکنی،</p></div>
<div class="m2"><p>کانان که مُدَبّرند سرگردان‌اند!</p></div></div>