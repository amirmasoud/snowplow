---
title: >-
    رباعی ۱
---
# رباعی ۱

<div class="b" id="bn1"><div class="m1"><p>هرچند که رنگ و روی زیباست مرا،</p></div>
<div class="m2"><p>چون لاله رخ و چو سَرْو بالاست مرا،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معلوم نشد که در طَرَبخانهٔ خاک</p></div>
<div class="m2"><p>نقّاشِ ازل بهرِ چه آراست مرا؟</p></div></div>