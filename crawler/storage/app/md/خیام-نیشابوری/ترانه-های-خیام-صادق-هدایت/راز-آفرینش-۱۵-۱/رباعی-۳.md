---
title: >-
    رباعی ۳
---
# رباعی ۳

<div class="b" id="bn1"><div class="m1"><p>از آمدنم نبود گردون را سود،</p></div>
<div class="m2"><p>وز رفتن من جاه و جلالش نفزود؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز هیچ‌کسی نیز دو گوشم نشنود،</p></div>
<div class="m2"><p>کاین آمدن و رفتنم از بهر چه بود!</p></div></div>