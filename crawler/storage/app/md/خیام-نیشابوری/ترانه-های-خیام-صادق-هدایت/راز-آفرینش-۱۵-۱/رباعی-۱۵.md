---
title: >-
    رباعی ۱۵
---
# رباعی ۱۵

<div class="b" id="bn1"><div class="m1"><p>گاوی است بر آسمان قَرینِ پروین،</p></div>
<div class="m2"><p>گاوی است دگر نهفته در زیر زمین؛</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بینایی، چشمِ حقیقت بگشا:</p></div>
<div class="m2"><p>زیر و زَبَرِ دو گاو مشتی خر بین.</p></div></div>