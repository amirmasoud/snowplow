---
title: >-
    رباعی شمارهٔ ۶۴
---
# رباعی شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>ای بس که نباشیم و جهان خواهد بود</p></div>
<div class="m2"><p>نی نام ز ما و نی‌ نشان خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین پیش نبودیم و نبد هیچ خلل</p></div>
<div class="m2"><p>زین پس چو نباشیم همان خواهد بود</p></div></div>