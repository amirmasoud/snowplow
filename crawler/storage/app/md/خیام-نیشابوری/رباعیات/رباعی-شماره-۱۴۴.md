---
title: >-
    رباعی شمارهٔ ۱۴۴
---
# رباعی شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>گاویست در آسمان و نامش پروین</p></div>
<div class="m2"><p>یک گاو دگر نهفته در زیر زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم خردت باز کن از روی یقین</p></div>
<div class="m2"><p>زیر و زبر دو گاو مشتی خر بین</p></div></div>