---
title: >-
    رباعی شمارهٔ ۹
---
# رباعی شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>اکنون که گل سعادتت پربار است</p></div>
<div class="m2"><p>دست تو ز جام می چرا بیکار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می خور که زمانه دشمنی غدار است</p></div>
<div class="m2"><p>دریافتن روز چنین دشوار است</p></div></div>