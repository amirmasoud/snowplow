---
title: >-
    رباعی شمارهٔ ۱۴۳
---
# رباعی شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>قومی متفکرند اندر ره دین</p></div>
<div class="m2"><p>قومی به گمان فتاده در راه یقین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میترسم از آن که بانگ آید روزی</p></div>
<div class="m2"><p>کای بیخبران راه نه آنست و نه این</p></div></div>