---
title: >-
    رباعی شمارهٔ ۱۲۹
---
# رباعی شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>دشمن به غلط گفت که من فلسفیم</p></div>
<div class="m2"><p>ایزد داند که آنچه او گفت نیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیکن چو در این غم آشیان آمده‌ام</p></div>
<div class="m2"><p>آخر کم از آنکه من بدانم که کیم</p></div></div>