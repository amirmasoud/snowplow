---
title: >-
    رباعی شمارهٔ ۱۲۴
---
# رباعی شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>برخیزم و عزم باده ناب کنم</p></div>
<div class="m2"><p>رنگ رخ خود به رنگ عناب کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این عقل فضول پیشه را مشتی می</p></div>
<div class="m2"><p>بر روی زنم چنانکه در خواب کنم</p></div></div>