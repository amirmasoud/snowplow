---
title: >-
    رباعی شمارهٔ ۱۴۷
---
# رباعی شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>می خوردن و گرد نیکوان گردیدن</p></div>
<div class="m2"><p>به زانکه بزرق زاهدی ورزیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عاشق و مست دوزخی خواهد بود</p></div>
<div class="m2"><p>پس روی بهشت کس نخواهد دیدن</p></div></div>