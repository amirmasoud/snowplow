---
title: >-
    رباعی شمارهٔ ۱۱۳
---
# رباعی شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>وقت سحر است خیز ای مایه ناز</p></div>
<div class="m2"><p>نرمک نرمک باده خور و چنگ نواز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کانها که بجایند نپایند بسی</p></div>
<div class="m2"><p>و آنها که شدند کس نمیاید باز</p></div></div>