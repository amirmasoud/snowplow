---
title: >-
    رباعی شمارهٔ ۱۸
---
# رباعی شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>این یک دو سه روز نوبت عمر گذشت</p></div>
<div class="m2"><p>چون آب به جویبار و چون باد به دشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز غم دو روز مرا یاد نگشت</p></div>
<div class="m2"><p>روزی که نیامده‌ست و روزی که گذشت</p></div></div>