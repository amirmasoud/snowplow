---
title: >-
    رباعی شمارهٔ ۱
---
# رباعی شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>برخیز بتا بیا ز بهر دل ما</p></div>
<div class="m2"><p>حل کن به جمال خویشتن مشکل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک کوزه شراب تا به هم نوش کنیم</p></div>
<div class="m2"><p>زآن پیش که کوزه‌ها کنند از گل ما</p></div></div>