---
title: >-
    رباعی شمارهٔ ۹۱
---
# رباعی شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>هر راز که اندر دل دانا باشد</p></div>
<div class="m2"><p>باید که نهفته‌تر ز عنقا باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاندر صدف از نهفتگی گردد در</p></div>
<div class="m2"><p>آن قطره که راز دل دریا باشد</p></div></div>