---
title: >-
    رباعی شمارهٔ ۷۶
---
# رباعی شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>در دهر چو آواز گل تازه دهند</p></div>
<div class="m2"><p>فرمای بتا که می به‌اندازه دهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حور و قصور و ز بهشت و دوزخ</p></div>
<div class="m2"><p>فارغ بنشین که آن هر آوازه دهند</p></div></div>