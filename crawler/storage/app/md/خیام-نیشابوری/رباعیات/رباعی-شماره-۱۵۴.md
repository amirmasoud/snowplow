---
title: >-
    رباعی شمارهٔ ۱۵۴
---
# رباعی شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>بنگر ز صبا دامن گل چاک شده</p></div>
<div class="m2"><p>بلبل ز جمال گل طربناک شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سایه گل نشین که بسیار این گل</p></div>
<div class="m2"><p>در خاک فرو ریزد و ما خاک شده</p></div></div>