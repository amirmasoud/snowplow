---
title: >-
    رباعی شمارهٔ ۵۶
---
# رباعی شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>آن‌ها که کهن شدند و این‌ها که نوند</p></div>
<div class="m2"><p>هر کس به مراد خویش یک تک به دوند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این کهنه‌جهان به کس نماند باقی</p></div>
<div class="m2"><p>رفتند و رویم دیگر آیند و روند</p></div></div>