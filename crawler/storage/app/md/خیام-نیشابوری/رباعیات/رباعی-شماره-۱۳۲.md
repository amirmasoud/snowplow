---
title: >-
    رباعی شمارهٔ ۱۳۲
---
# رباعی شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>من بی می ناب زیستن نتوانم</p></div>
<div class="m2"><p>بی باده کشید بارتن نتوانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بنده آن دمم که ساقی گوید</p></div>
<div class="m2"><p>یک جام دگر بگیر و من نتوانم</p></div></div>