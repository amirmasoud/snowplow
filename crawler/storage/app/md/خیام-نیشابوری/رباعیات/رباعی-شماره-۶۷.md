---
title: >-
    رباعی شمارهٔ ۶۷
---
# رباعی شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>بر پشت من از زمانه تو می‌آید</p></div>
<div class="m2"><p>وز من همه کار نانکو می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان عزم رحیل کرد و گفتم بمرو</p></div>
<div class="m2"><p>گفتا چه کنم خانه فرومی‌آید</p></div></div>