---
title: >-
    رباعی شمارهٔ ۳
---
# رباعی شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>قرآن که مهین کلام خوانند آن را</p></div>
<div class="m2"><p>گهگاه نه بر دوام خوانند آن را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر گرد پیاله آیتی هست مقیم</p></div>
<div class="m2"><p>کاندر همه جا مدام خوانند آن را</p></div></div>