---
title: >-
    رباعی شمارهٔ ۴۶
---
# رباعی شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>می لعل مذاب است و صراحی کان است</p></div>
<div class="m2"><p>جسم است پیاله و شرابش جان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن جام بلورین که ز می خندان است</p></div>
<div class="m2"><p>اشکی است که خون دل در او پنهان است</p></div></div>