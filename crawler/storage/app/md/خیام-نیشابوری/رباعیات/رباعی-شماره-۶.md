---
title: >-
    رباعی شمارهٔ ۶
---
# رباعی شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ماییم و می و مطرب و این کنج خراب</p></div>
<div class="m2"><p>جان و دل و جام و جامه پر درد شراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فارغ ز امید رحمت و بیم عذاب</p></div>
<div class="m2"><p>آزاد ز خاک و باد و از آتش و آب</p></div></div>