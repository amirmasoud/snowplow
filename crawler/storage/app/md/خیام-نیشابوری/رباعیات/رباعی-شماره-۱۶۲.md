---
title: >-
    رباعی شمارهٔ ۱۶۲
---
# رباعی شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>ای دوست حقیقت شنواز من سخنی</p></div>
<div class="m2"><p>با باده لعل باش و با سیم تنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کانکس که جهان کرد فراغت دارد</p></div>
<div class="m2"><p>از سبلت چون تویی و ریش چو منی</p></div></div>