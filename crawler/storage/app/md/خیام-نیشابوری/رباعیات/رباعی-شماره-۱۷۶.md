---
title: >-
    رباعی شمارهٔ ۱۷۶
---
# رباعی شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>گر کار فلک به عدل سنجیده بدی</p></div>
<div class="m2"><p>احوال فلک جمله پسندیده بدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور عدل بدی بکارها در گردون</p></div>
<div class="m2"><p>کی خاطر اهل فضل رنجیده بدی</p></div></div>