---
title: >-
    رباعی شمارهٔ ۹۲
---
# رباعی شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>هر صبح که روی لاله شبنم گیرد</p></div>
<div class="m2"><p>بالای بنفشه در چمن خم گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انصاف مرا ز غنچه خوش می‌آید</p></div>
<div class="m2"><p>کاو دامن خویشتن فراهم گیرد</p></div></div>