---
title: >-
    رباعی شمارهٔ ۲۰
---
# رباعی شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>پیش از من و تو لیل و نهاری بوده‌ست</p></div>
<div class="m2"><p>گردنده فلک نیز بکاری بوده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجا که قدم نهی تو بر روی زمین</p></div>
<div class="m2"><p>آن مردمک چشم نگاری بوده‌ست</p></div></div>