---
title: >-
    رباعی شمارهٔ ۱۵
---
# رباعی شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>این کوزه چو من عاشق زاری بوده‌ست</p></div>
<div class="m2"><p>در بند سر زلف نگاری بوده‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این دسته که بر گردن او می‌بینی</p></div>
<div class="m2"><p>دستی‌ست که بر گردن یاری بوده‌ست</p></div></div>