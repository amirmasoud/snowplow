---
title: >-
    رباعی شمارهٔ ۲۸
---
# رباعی شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>چون نیست حقیقت و یقین اندر دست</p></div>
<div class="m2"><p>نتوان به امید شک همه عمر نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هان تا ننهیم جام می از کف دست</p></div>
<div class="m2"><p>در بی‌خبری مرد چه هشیار و چه مست</p></div></div>