---
title: >-
    رباعی شمارهٔ ۴۱
---
# رباعی شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>گویند کسان بهشت با حور خوش است</p></div>
<div class="m2"><p>من می‌گویم که آب انگور خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این نقد بگیر و دست از آن نسیه بدار</p></div>
<div class="m2"><p>کآواز دهل شنیدن از دور خوش است</p></div></div>