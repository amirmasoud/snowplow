---
title: >-
    رباعی شمارهٔ ۱۹
---
# رباعی شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>بر چهرۀ گل نسیم نوروز خوش است</p></div>
<div class="m2"><p>در صحن چمن روی دل‌افروز خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دی که گذشت هر چه گویی خوش نیست</p></div>
<div class="m2"><p>خوش باش و ز دی مگو که امروز خوش است</p></div></div>