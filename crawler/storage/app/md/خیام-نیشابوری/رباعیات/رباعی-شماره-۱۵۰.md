---
title: >-
    رباعی شمارهٔ ۱۵۰
---
# رباعی شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>از آمدن و رفتن ما سودی کو</p></div>
<div class="m2"><p>وز تار امید عمر ما پودی کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندین سروپای نازنینان جهان</p></div>
<div class="m2"><p>می‌سوزد و خاک می‌شود دودی کو</p></div></div>