---
title: >-
    رباعی شمارهٔ ۵۹
---
# رباعی شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>اجرام که ساکنان این ایوان‌اند</p></div>
<div class="m2"><p>اسباب تردد خردمندان‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هان تا سر رشتهٔ خرد گم نکنی</p></div>
<div class="m2"><p>کآنان که مدبرند سرگردان‌اند</p></div></div>