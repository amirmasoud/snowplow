---
title: >-
    رباعی شمارهٔ ۸
---
# رباعی شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ابر آمد و باز بر سر سبزه گریست</p></div>
<div class="m2"><p>بی بادهٔ گلرنگ نمی‌باید زیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این سبزه که امروز تماشاگه ماست</p></div>
<div class="m2"><p>تا سبزهٔ خاک ما تماشاگه کیست</p></div></div>