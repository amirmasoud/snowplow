---
title: >-
    رباعی شمارهٔ ۱۵۱
---
# رباعی شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>از تن چو برفت جان پاک من و تو</p></div>
<div class="m2"><p>خشتی دو نهند بر مغاک من و تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آنگاه برای خشت گور دگران</p></div>
<div class="m2"><p>در کالبدی کشند خاک من و تو</p></div></div>