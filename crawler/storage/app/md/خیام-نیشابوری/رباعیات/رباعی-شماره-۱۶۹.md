---
title: >-
    رباعی شمارهٔ ۱۶۹
---
# رباعی شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>چندان که نگاه می‌کنم هر سویی</p></div>
<div class="m2"><p>در باغ روان است ز کوثر جویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صحرا چو بهشت است ز کوثر کم گوی</p></div>
<div class="m2"><p>بنشین به بهشت با بهشتی رویی</p></div></div>