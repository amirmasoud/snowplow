---
title: >-
    رباعی شمارهٔ ۱۴۲
---
# رباعی شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>قانع به یک استخوان چو کرکس بودن</p></div>
<div class="m2"><p>به ز آن که طفیل خوان ناکس بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با نان جوین خویش حقا که به است</p></div>
<div class="m2"><p>کالوده و پالوده هر خس بودن</p></div></div>