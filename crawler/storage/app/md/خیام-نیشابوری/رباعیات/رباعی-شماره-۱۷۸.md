---
title: >-
    رباعی شمارهٔ ۱۷۸
---
# رباعی شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>هنگام صبوح ای صنم فرخ پی</p></div>
<div class="m2"><p>برساز ترانه‌ای و پیش‌آور می</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کافکند بخاک صد هزاران جم و کی</p></div>
<div class="m2"><p>این آمدن تیرمه و رفتن دی</p></div></div>