---
title: >-
    رباعی شمارهٔ ۱۲۳
---
# رباعی شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>برخیز ز خواب تا شرابی بخوریم</p></div>
<div class="m2"><p>زان پیش که از زمانه تابی بخوریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاین چرخ ستیزه روی ناگه روزی</p></div>
<div class="m2"><p>چندان ندهد زمان که آبی بخوریم</p></div></div>