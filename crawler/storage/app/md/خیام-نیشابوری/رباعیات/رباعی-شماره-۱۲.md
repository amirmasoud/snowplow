---
title: >-
    رباعی شمارهٔ ۱۲
---
# رباعی شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>ای چرخ فلک خرابی از کینهٔ توست</p></div>
<div class="m2"><p>بیدادگری شیوهٔ دیرینهٔ توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خاک اگر سینه تو بشکافند</p></div>
<div class="m2"><p>بس گوهر قیمتی که در سینهٔ توست</p></div></div>