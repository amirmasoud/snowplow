---
title: >-
    رباعی شمارهٔ ۳۵
---
# رباعی شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>در فصل بهار اگر بتی حور سرشت</p></div>
<div class="m2"><p>یک ساغر می دهد مرا بر لب کشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند به نزد عامه این باشد زشت</p></div>
<div class="m2"><p>سگ به ز من است اگر برم نام بهشت</p></div></div>