---
title: >-
    رباعی شمارهٔ ۵۷
---
# رباعی شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>آن‌کس که زمین و چرخ و افلاک نهاد</p></div>
<div class="m2"><p>بس داغ که او بر دل غمناک نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسیار لب چو لعل و زلفین چو مشک</p></div>
<div class="m2"><p>در طبل زمین و حقهٔ خاک نهاد</p></div></div>