---
title: >-
    رباعی شمارهٔ ۱۶۱
---
# رباعی شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>ایدل تو به اسرار معما نرسی</p></div>
<div class="m2"><p>در نکته زیرکان دانا نرسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینجا به می لعل بهشتی می ساز</p></div>
<div class="m2"><p>کانجا که بهشت است رسی یا نرسی</p></div></div>