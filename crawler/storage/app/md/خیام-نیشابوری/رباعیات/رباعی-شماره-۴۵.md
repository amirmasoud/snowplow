---
title: >-
    رباعی شمارهٔ ۴۵
---
# رباعی شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>می خوردن و شاد بودن آیین من است</p></div>
<div class="m2"><p>فارغ بودن ز کفر و دین دین من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم به عروس دهر کابین تو چیست</p></div>
<div class="m2"><p>گفتا دل خرم تو کابین من است</p></div></div>