---
title: >-
    رباعی شمارهٔ ۱۰
---
# رباعی شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>امروز تو را دسترس فردا نیست</p></div>
<div class="m2"><p>واندیشهٔ فردات به جز سودا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ضایع مکن این دم ار دلت شیدا نیست</p></div>
<div class="m2"><p>کاین باقی عمر را بها پیدا نیست</p></div></div>