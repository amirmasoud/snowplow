---
title: >-
    رباعی شمارهٔ ۱۷۵
---
# رباعی شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>گر دست دهد ز مغز گندم نانی</p></div>
<div class="m2"><p>وز می دو منی ز گوسفندی رانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با لاله رخی و گوشه بستانی</p></div>
<div class="m2"><p>عیشی بود آن نه حد هر سلطانی</p></div></div>