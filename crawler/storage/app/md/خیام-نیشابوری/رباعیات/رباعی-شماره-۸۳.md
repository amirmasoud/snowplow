---
title: >-
    رباعی شمارهٔ ۸۳
---
# رباعی شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>کم کن طمع از جهان و می‌زی خرسند</p></div>
<div class="m2"><p>از نیک و بد زمانه بگسل پیوند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می در کف و زلف دلبری گیر که زود</p></div>
<div class="m2"><p>هم بگذرد و نماند این روزی چند</p></div></div>