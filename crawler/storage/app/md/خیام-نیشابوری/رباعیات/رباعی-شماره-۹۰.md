---
title: >-
    رباعی شمارهٔ ۹۰
---
# رباعی شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>می خور که ز دل کثرت و قلت ببرد</p></div>
<div class="m2"><p>و اندیشه هفتاد و دو ملت ببرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرهیز مکن ز کیمیایی که از او</p></div>
<div class="m2"><p>یک جرعه خوری هزار علت ببرد</p></div></div>