---
title: >-
    شمارهٔ ۳ - من نتایج افکاره فی مرثیه‌اخیه‌الصاحب الاجل الاکرام خواجه عبدالغنی
---
# شمارهٔ ۳ - من نتایج افکاره فی مرثیه‌اخیه‌الصاحب الاجل الاکرام خواجه عبدالغنی

<div class="b" id="bn1"><div class="m1"><p>ستیزه گر فلکا از جفا و جور تو داد</p></div>
<div class="m2"><p>نفاق پیشه سپهرا ز کینه‌ات فریاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا ز ساغر بیداد شربتی دادی</p></div>
<div class="m2"><p>که تا قیامتم از مرگ یاد خواهد کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرابگوش رسانیدی از جفا حرفی</p></div>
<div class="m2"><p>که رفت تا ابدم حرف عافیت از یاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آب و آتشم از تاب کو سموم اجل</p></div>
<div class="m2"><p>که ذره ذره دهد خاک هستیم بر باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه مشفقی که شود بر هلاک من باعث</p></div>
<div class="m2"><p>نه مونسی که کند در فنای من امداد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه قاصدی که ز مرغ شکسته بال وی‌ام</p></div>
<div class="m2"><p>برد سلام به آن نخل بوستان مراد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرم فدای تو این باد صبح دم برخیز</p></div>
<div class="m2"><p>برو به عالم ارواح ازین خراب آباد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشان گمشدهٔ من بجو ز خرد و بزرگ</p></div>
<div class="m2"><p>سراغ یوسف من کن ز بنده و آزاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جلوه‌گاه جوانان پارسا چه رسی</p></div>
<div class="m2"><p>ز رخش عزم فرود آ و نوحه کن بنیاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو دیده بر رخ عبدالغنی من فکنی</p></div>
<div class="m2"><p>ز روی درد بر آر از زبان من فریاد</p></div></div>
<div class="b2" id="bn11"><p>بگو برادرت ای نور دیده داده پیام</p>
<p>که ای ممات تو بر من حیات کرده حرام</p></div>
<div class="b" id="bn12"><div class="m1"><p>دلم که می‌شد از ادراک دوری تو هلاک</p></div>
<div class="m2"><p>تو خود بگو که هلاک تو چون کند ادراک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو خورده ضربت مرگ و مرا برآمده جان</p></div>
<div class="m2"><p>تو کرده زهر اجل نوش و من ز درد هلاک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به خاک خفته تو از تندباد فتنه چو سرو</p></div>
<div class="m2"><p>به باد رفته من از آه خویش چون خاشاک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر از تو بگسلم ای نونهال رشتهٔ مهر</p></div>
<div class="m2"><p>به تیغ کین رگ جانم بریده باد چو تاک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ور از پی تو نتازم سمند جان به عدم</p></div>
<div class="m2"><p>سرم به دست اجل بسته باد بر فتراک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شبی نمی‌گذرد کز غمت نمی‌گذرد</p></div>
<div class="m2"><p>شرار آهم از انجم فغانم از افلاک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر آتش دل خود سوختن چو ممکن نیست</p></div>
<div class="m2"><p>به هرزه می‌کشم از سینه آه آتشناک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اجل چو جامهٔ جانم نمی‌درد بی‌تو</p></div>
<div class="m2"><p>درین هوس به عبث می‌کنم گریبان چاک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز ابر دیده به خوناب اشگم آلوده</p></div>
<div class="m2"><p>کجاست برق اجل تا مرا بسوزد پاک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روا بود که تو در زیر خاک باشی و من</p></div>
<div class="m2"><p>سیاه پوشم و بر سر کنم ز ماتم خاک</p></div></div>
<div class="b2" id="bn22"><p>چرا تو جامه نکردی سیاه در غم من</p>
<p>چرا تو خاک نکردی بسر ز ماتم من</p></div>
<div class="b" id="bn23"><div class="m1"><p>چرا ز باغ من ای سرو بوستان رفتی</p></div>
<div class="m2"><p>مرا ز پای فکندی و خود روان رفتی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در یگانه من از چه ساختی دریا</p></div>
<div class="m2"><p>کنار من ز سرشک و خود از میان رفتی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز دیدهٔ پدر ای یوسف دیار بقا</p></div>
<div class="m2"><p>چرا به مصر فنا بی‌برادران رفتی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به شمع روی تو چشم قبیله روشن بود</p></div>
<div class="m2"><p>به چشم زخم غریبی ز دودمان رفتی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گمان نبود که مرگ تو بینم اندر خواب</p></div>
<div class="m2"><p>مرا به خواب گران کرده بیگمان رفتی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو را چه جای نمودند در نشیمن قدس</p></div>
<div class="m2"><p>که بی‌توقف ازین تیرهٔ خاکدان رفتی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>درین قضیه تو را نیست حسرتی که مراست</p></div>
<div class="m2"><p>اگرچه با دل پر حسرت از جهان رفتی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مراست غم که شدم ساکن جحیم فراق</p></div>
<div class="m2"><p>تو را چه غم که سوی روضهٔ جنان رفتی</p></div></div>
<div class="b2" id="bn31"><p>ز رفتن تو من از عمر بی‌نصیب شدم</p>
<p>سفر تو کردی و من در جهان غریب شدم</p></div>
<div class="b" id="bn32"><div class="m1"><p>کجائی ای گل گلزار زندگانی من</p></div>
<div class="m2"><p>کجائی ای ثمر نخل شادمانی من</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز دیده تا شدی ای شاخ ارغوان پنهان</p></div>
<div class="m2"><p>به خون نشانده مرا اشک ارغوانی من</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بیا ببین که فلک از غم جوانی تو</p></div>
<div class="m2"><p>چو آتشی زده در خرمن جوانی من</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بیا ببین که چه سان بی‌بهار عارض تو</p></div>
<div class="m2"><p>به خون دل شده تر چهرهٔ خزانی من</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خیال مرثیه‌ات چون کنم که رفته به باد</p></div>
<div class="m2"><p>متاع خرده شناسی و نکته دانی من</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اجل که خواست تو را جان ستاند از ره کین</p></div>
<div class="m2"><p>چرا نخست نیامد به جان ستانی من</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو در وفات نمردم چه لاف مهر زنم</p></div>
<div class="m2"><p>که خاک بر سر من باد و مهربانی من</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز شربتی که چشیدی مرا بده قدری</p></div>
<div class="m2"><p>که بی‌وجود تو تلخ است زندگانی من</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز پرسشم همه کس پا کشید جز غم تو</p></div>
<div class="m2"><p>که هست تا به دم مرگ یار جانی من</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو مرگ همچو توئی دیدم و ندادم جان</p></div>
<div class="m2"><p>زمانه شد متحیر ز سخت جانی من</p></div></div>
<div class="b2" id="bn42"><p>که هر که جان رودش زنده چون تواند بود</p>
<p>چراغ مرده فروزنده چون تواند بود</p></div>
<div class="b" id="bn43"><div class="m1"><p>کجاست کام دل و آرزوی دیدهٔ من</p></div>
<div class="m2"><p>کجاست نور دو چشم رمد رسیدهٔ من</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گزیده‌اند ز من جملهٔ همدمان دوری</p></div>
<div class="m2"><p>کجاست همدم یکتای برگزیدهٔ من</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>فغان که از قفس سینه زود رفت برون</p></div>
<div class="m2"><p>چو مرغ روح تو مرغ دل رمیدهٔ من</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>امید بود که روز اجل رود در خاک</p></div>
<div class="m2"><p>به اهتمام تو جسم ستم کشیدهٔ من</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>فغان که چرخ به صد اهتمام می‌شوید</p></div>
<div class="m2"><p>غبار قبر تو اکنون به آب دیدهٔ من</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زمانه بی تو مرا گو کباب کن که شده‌ست</p></div>
<div class="m2"><p>پر از نمک دل مجروح خون چکیدهٔ من</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سیاه باد زبانش که بی‌محابا راند</p></div>
<div class="m2"><p>زبان به مرثیه این کلک سر بریدهٔ من</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز شوره گل طلبد هر که بعد ازین جوید</p></div>
<div class="m2"><p>طراوت از غزل و صنعت از قصیدهٔ من</p></div></div>
<div class="b2" id="bn51"><p>چرا که بلبل طبعم شکسته بال شده</p>
<p>زبان طوطی نطقم ز غصه لال شده</p></div>
<div class="b" id="bn52"><div class="m1"><p>گل عذار تو در خاک گشت خوار دریغ</p></div>
<div class="m2"><p>خط غبار تو در قبر شد غبار دریغ</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بهار آمد و گل در چمن شکفت و تو را</p></div>
<div class="m2"><p>شکفته شد گل حسرت درین بهار دریغ</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بماند داغ تو در سینه یادگار و نماند</p></div>
<div class="m2"><p>فروغ روی تو در چشم اشگبار دریغ</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نکرده شخص تو بر رخش عمر یک جولان</p></div>
<div class="m2"><p>روان به مرکب تابوت شد سوار دریغ</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بهار عمر تو را بود وقت نشو و نما</p></div>
<div class="m2"><p>تگرگ مرگ برآورد از آن دمار دریغ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز قد و روی تو صد آه و صدهزار فغان</p></div>
<div class="m2"><p>ز خلق و خوی تو صد حیف و صد هزار دریغ</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز مهربانیت ای ماه اوج مهر افسوس</p></div>
<div class="m2"><p>ز همزبانیت ای سرو گل عذار دریغ</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تو را سپهر ملاعب گران‌بها چون یافت</p></div>
<div class="m2"><p>ربود از منت ای در شاه‌وار دریغ</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>شکفته‌تر ز تو در باغ ما نبود گلی</p></div>
<div class="m2"><p>به چشم زخم خسان ریختی ز بار دریغ</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تو کز قبیله چو یوسف عزیزتر بودی</p></div>
<div class="m2"><p>به حیله گرگ اجل ساختت شکار دریغ</p></div></div>
<div class="b2" id="bn62"><p>دریغ و درد که شد نرگس تو زود به خواب</p>
<p>گل عذار تو بی‌وقت شد به زیر نقاب</p></div>
<div class="b" id="bn63"><div class="m1"><p>فغان که بی‌گل رویت دلم فکار بماند</p></div>
<div class="m2"><p>به سینه‌ام ز تو صد گونه خار بماند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>غبار خط تو تا شد نهان ز دیدهٔ من</p></div>
<div class="m2"><p>ز آهم آینهٔ دیده در غبار بماند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز لاله‌زار جهان تا شدی به باغ جنان</p></div>
<div class="m2"><p>دلم ز داغ فراقت چو لاله‌زار بماند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ز بودن تو مرا شادی که بود به دل</p></div>
<div class="m2"><p>به دل به غم شد و در جان بی‌قرار بماند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>تو از میان شدی و همدمی نماند به من</p></div>
<div class="m2"><p>به غیر طفل سرشگم که در کنار بماند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تو زخم تیر اجل خوردی از قضا و مرا</p></div>
<div class="m2"><p>به دل جراحت آن تیر جان شکار بماند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به هیچ زخم نماند جراحتی که مرا</p></div>
<div class="m2"><p>ز نیش هجر تو بر سینه فکار بماند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>تو رستی از غم این روزگار تیره ولی</p></div>
<div class="m2"><p>مصیبتی به من تیره روزگار بماند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>اجل تو را به دیار فنا فکند و مرا</p></div>
<div class="m2"><p>به راه پیک اجل چشم انتظار بماند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>فغان که خشک شد از گریه چشم و تابد</p></div>
<div class="m2"><p>بنای فرقت ما و تو استوار بماند</p></div></div>
<div class="b2" id="bn73"><p>طناب عمر تو را زد اجل به تیغ دریغ</p>
<p>گسست رابطهٔ ما ز هم دریغ</p></div>
<div class="b" id="bn74"><div class="m1"><p>چه داغها که مرا از غم تو بر تن نیست</p></div>
<div class="m2"><p>چه چاکها که ز هجر تو در دل من نیست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>کدام دجله که از اشک من نه چون دریاست</p></div>
<div class="m2"><p>کدام خانه که از آه من چو گلخن نیست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>مرا چو لاله ز داغ تو در لباس حیات</p></div>
<div class="m2"><p>کدام چاک که از جیب تا ابد امن نیست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>دگر ز پرتو خورشید و نور ماه چه فیض</p></div>
<div class="m2"><p>مرا که بی‌مه روی تو دیده روشن نیست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>شکسته بال نشاطم چنان که تا یابد</p></div>
<div class="m2"><p>جز آشیان غمم هیچ جا نشیمن نیست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چو بحر بر سر از ان کف زنم که از کف من</p></div>
<div class="m2"><p>دری فتاده که در هیچ کان و معدن نیست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>از آن به بانک هزارم که رفته از چمنم</p></div>
<div class="m2"><p>گلی به باد که در صحن هیچ گلشن نیست</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چو او برادر با جان برابر من بود</p></div>
<div class="m2"><p>مرا ز درویش زنده بودن نیست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ببین برابری او با جان که تاریخش</p></div>
<div class="m2"><p>به جز برادر با جان برابر من نیست</p></div></div>
<div class="b2" id="bn83"><p>خبر ز حالت ما آن برادران دارند</p>
<p>که جان به یکدیگر از مهر در میان دارند</p></div>
<div class="b" id="bn84"><div class="m1"><p>برادرا ز فراق تو در جهان چه کنم</p></div>
<div class="m2"><p>به دل چه سازم و با جان ناتوان چه کنم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>قدم ز بار فراق تو شد کمان او</p></div>
<div class="m2"><p>جدل به چرخ مقوس نمی‌توان چه کنم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>توان تحمل بار فراق کرد به صبر</p></div>
<div class="m2"><p>ولی فراق تو باریست بس گران چه کنم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>تب فراق توام سوخت استخوان و هنوز</p></div>
<div class="m2"><p>برون نمی‌رود از مغز استخوان چه کنم</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>به جانم و اجل از من نمی‌ستاند جان</p></div>
<div class="m2"><p>درین معامله درمانده‌ام به جان چه کنم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ز جستجوی تو جانم به لب رسید و مرا</p></div>
<div class="m2"><p>نمی‌دهند به راه عدم نشان چه کنم</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>به همزبانیم آیند دوستان لیکن</p></div>
<div class="m2"><p>مرا که با تو زبان نیست همزبان چه کنم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>فلک ز ناله زارم گرفت گوش و هنوز</p></div>
<div class="m2"><p>اجل نمی‌نهدم مهر بر دهان چه کنم</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>هلاک محتشم از زیستن به هست اما</p></div>
<div class="m2"><p>اجل مضایقه‌ای می‌کند در آن چکنم</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>محیط اشک مرا در غم تو نیست کران</p></div>
<div class="m2"><p>من فتاده در آن بحر بی‌کران چه کنم</p></div></div>
<div class="b2" id="bn94"><p>چنین که غرقه طوفان اشک شد تن من</p>
<p>اگر چو شمع نمیرم رواست کشتن من</p></div>
<div class="b" id="bn95"><div class="m1"><p>مهی که بی تو برآمد در ابر پنهان باد</p></div>
<div class="m2"><p>گلی که بی تو بروید به خاک یکسان باد</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>شکوفه‌ای که سر از خاک برکند بی تو</p></div>
<div class="m2"><p>چو برگ عیش من از باد فتنه ریزان باد</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>گلی که بی تو بپوشد لباس رعنائی</p></div>
<div class="m2"><p>ز دست حادثه‌اش چاک در گریبان باد</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>درین بهار اگر سبزه از زمین بدمد</p></div>
<div class="m2"><p>چو خط سبز تو در زیر خاک پنهان باد</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>اگر بسر نهد امسال تاج زر نرگس</p></div>
<div class="m2"><p>سرش ز بازی گردون به نیزه گردان باد</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>اگر نه لاله بداغ تو سر زند از کوه</p></div>
<div class="m2"><p>لباس زندگیش چاک تا به دامان باد</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>اگر نه سنبل ازین تعزیت سیه پوشد</p></div>
<div class="m2"><p>چو روزگار من آشفته و پریشان باد</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>اگر بنفشه نسازد رخ از طپانچه کبود</p></div>
<div class="m2"><p>مدام خون زد و چشمش بروی مژگان باد</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>من شکسته دل سخت جان سوخته بخت</p></div>
<div class="m2"><p>که پیکرم چو تن نازک تو بی جان باد</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>اگر جدا ز تو دیگر بنای عیش نهم</p></div>
<div class="m2"><p>بنای هستیم از سیل فتنه ویران باد</p></div></div>
<div class="b2" id="bn105"><p>تو را مباد به جز عیش در ریاض جنان</p>
<p>من این چنین گذرانم همیشه و تو چنان</p></div>
<div class="b" id="bn106"><div class="m1"><p>تو را به سایهٔ طوبی و سدرهٔ جا بادا</p></div>
<div class="m2"><p>نوید آیهٔ طوبی لهم تو را بادا</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>زلال رحمت حق تا بود بخلد روان</p></div>
<div class="m2"><p>روان پاک تو در جنت‌العلا بادا</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>اگرچه آتش بیگانگی زدی بر من</p></div>
<div class="m2"><p>به بحر رحمت حق جانت آشنا بادا</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>در آفتاب غمم گرچه سوختی جانت</p></div>
<div class="m2"><p>به سایهٔ علم سبز مصطفی بادا</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>چو تلخکام ز دنیا شدی شراب طهور</p></div>
<div class="m2"><p>نصیب از کف پر فیض مرتضی بادا</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>نبی چو گفت شهید است هرکه مرد غریب</p></div>
<div class="m2"><p>تو را ثواب شهیدان کربلا بادا</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>دمی که حشر غریبان کنند روزی تو</p></div>
<div class="m2"><p>شفاعت علی موسی رضا بادا</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>چو رو به جانب جنت کنی ز هر جانب</p></div>
<div class="m2"><p>به گوشت از ملک جنت این ندا بادا</p></div></div>
<div class="b2" id="bn114"><p>که ای شراب اجل کرده در جوانی نوش</p>
<p>بیا و از کف حورا می طهور بنوش</p></div>