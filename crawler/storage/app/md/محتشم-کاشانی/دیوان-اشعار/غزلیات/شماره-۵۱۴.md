---
title: >-
    شمارهٔ  ۵۱۴
---
# شمارهٔ  ۵۱۴

<div class="b" id="bn1"><div class="m1"><p>حرف در مجلس نگویم جز به هم زانوی او</p></div>
<div class="m2"><p>تا به چشمی سوی او بینم به چشمی سوی او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میشود صد نکته‌ام خاطر نشان تا میشود</p></div>
<div class="m2"><p>نیم جنبشها تمام از گوشهٔ ابروی او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان شکارافکن همینم بس که مخصوص منست</p></div>
<div class="m2"><p>لذت زخم نهانی خوردن از آهوی او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چاک دلها محض حرفی بود تا روزی که کرد</p></div>
<div class="m2"><p>سر ز جیب ناز بیرون نرگس جادوی او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زخم تیر عشق بر ما بود تهمت تا فکند</p></div>
<div class="m2"><p>گردش دوران کمان حسن بر بازوی او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌محابا غوطه در دریای آتش خوردن است</p></div>
<div class="m2"><p>بی‌حذر برقع کشیدن ز آفتاب روی او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل ز پهلویش برون خواهد فتاد از اضطراب</p></div>
<div class="m2"><p>تن که از ترتیب بزم افتاده در پهلوی او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نکهتش در جنبش آرد خفتگان خاک را</p></div>
<div class="m2"><p>چون فشاند با دگرد از موی عنبر بوی او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرد آن منظر بگردان یک رهم ای سیل اشک</p></div>
<div class="m2"><p>کشته چون بیرون بری یکباره‌ام از کوی او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در جنونم آن چه می‌بایست واقع شد کنون</p></div>
<div class="m2"><p>بخت می‌باید که زنجیر آرد از گیسوی او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>محتشم کز دشت و وادی رو به شهر آورد کیست</p></div>
<div class="m2"><p>شیر دل دیوانه‌ای زنجیر خواه از موی او</p></div></div>