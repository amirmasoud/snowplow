---
title: >-
    شمارهٔ  ۱۷۵
---
# شمارهٔ  ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>مرا خیال تو شبها به خواب نگذارد</p></div>
<div class="m2"><p>چو تن به خواب دهم اضطراب نگذارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال آرزوئی می‌پزم که می‌ترسم</p></div>
<div class="m2"><p>اگر تو هم بگذاری حجاب نگذارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به طرف جوی اگر بگذری به این حرکات</p></div>
<div class="m2"><p>خرامش تو تحرک در آب نگذارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو گرم قتل اجل نارسیده‌ای که شوی</p></div>
<div class="m2"><p>فلک به سایه‌اش از آفتاب نگذارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به من کسی شده خصم ای اجل که در کارم</p></div>
<div class="m2"><p>عنان به دست تو سنگین رکاب نگذارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز ناز بسته لب اما به غمزه فرموده</p></div>
<div class="m2"><p>که یک سوال مرا بی‌جواب نگذارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار جرعه دهد عشوه‌اش به بوالهوسان</p></div>
<div class="m2"><p>چو دور محتشم آید عتاب نگذارد</p></div></div>