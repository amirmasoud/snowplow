---
title: >-
    شمارهٔ  ۴۵۷
---
# شمارهٔ  ۴۵۷

<div class="b" id="bn1"><div class="m1"><p>پا چون کشم ز کوی تو کانجا زمان زمان</p></div>
<div class="m2"><p>می‌آورد کشاکش عشقم کشان کشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان زار و تن نزار شد از بس که می‌رسد</p></div>
<div class="m2"><p>جور فلک برین ستم دلبران بر آن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نیستیم در خور وصل ای اجل بیا</p></div>
<div class="m2"><p>ما را ز چنگ فرقت آن دلستان ستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل داشت این گمان که رهائی بود ز تو</p></div>
<div class="m2"><p>خط لبت چو گشت عیان شد کم آن گمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفتی و گشت دیده لبالب ز در اشگ</p></div>
<div class="m2"><p>باز آی تا به پای تو ریزم روان روان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دل کناره کن ز بت من که روز و شب</p></div>
<div class="m2"><p>بسته است بهر کشتن اسلامیان میان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داغی که میهنی به دل از دست آن نگار</p></div>
<div class="m2"><p>ای محتشم ز دیدهٔ مردم نهان نه آن</p></div></div>