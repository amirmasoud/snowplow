---
title: >-
    شمارهٔ  ۲۷۱
---
# شمارهٔ  ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>ازین لیلی و شانم خاطر ناشاد نگشاید</p></div>
<div class="m2"><p>به جز شیرین کسی بند از دل فرهاد نگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چمن از دل گشایانست اما بر دل بلبل</p></div>
<div class="m2"><p>که دارد قید گل از سنبل و شمشاد نگشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رگ باریک جانم خود به مژگان سیه بگشا</p></div>
<div class="m2"><p>که بیمار تو را این مشکل از فصاد نگشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخواهی داد اگر داد کسی رخ بر کسی منما</p></div>
<div class="m2"><p>که دیگر دادخواهان را رگ فریاد نگشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو ای دل چون به بسمل لایقی بگذر ز آزادی</p></div>
<div class="m2"><p>که بنداز گردن صیدی چنین صیاد نگشاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بزور دست و پائی بندهٔ خود را دگر بگشا</p></div>
<div class="m2"><p>که روزی راه طعن بندهٔ آزاد نگشاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آه من گشادی بر در آن دل نشد پیدا</p></div>
<div class="m2"><p>دلی کز سنگ بادش لاجرم از باد نگشاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گشاد درد زین کاخ از درون جستم ندا آمد</p></div>
<div class="m2"><p>که از بیرون در این خانه گر بگشاد نگشاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگو ای محتشم با ناصح خود بین که بی حاصل</p></div>
<div class="m2"><p>زبان طعنه برمجنون ما در زاد نگشاید</p></div></div>