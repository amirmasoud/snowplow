---
title: >-
    شمارهٔ  ۳۱۱
---
# شمارهٔ  ۳۱۱

<div class="b" id="bn1"><div class="m1"><p>حسن را تکیه‌گه آن طرف کلاهست امروز</p></div>
<div class="m2"><p>ناز را خواب گه سیاهست امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ز بالا و قدش درزند آتش به جهان</p></div>
<div class="m2"><p>فتنه در رهگذرش چشم براهست امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود بی‌زلفت اگر یوسف حسنی در چاه</p></div>
<div class="m2"><p>به مدد کاری او بر لب چاهست امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کو دل و تاب کزان زلف و خط و خال سیاه</p></div>
<div class="m2"><p>حسن را دغدغهٔ عرض سپاهست امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش عشق من ازو بود نهان وای به من</p></div>
<div class="m2"><p>که بر آگاهیش آن چهره گواهست امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهربان چرب زبان گرم نگه بود امشب</p></div>
<div class="m2"><p>تندخو تلخ سخن تیز نگاهست امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم پیک نظر دوش دوانید مرا</p></div>
<div class="m2"><p>روز امید مرا شعلهٔ آهست امروز</p></div></div>