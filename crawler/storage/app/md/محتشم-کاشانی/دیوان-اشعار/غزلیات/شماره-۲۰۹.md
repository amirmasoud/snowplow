---
title: >-
    شمارهٔ  ۲۰۹
---
# شمارهٔ  ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>گه رفتن آن پری رو بوداع ما نیامد</p></div>
<div class="m2"><p>شه حسن بود آری بدر گدا نیامد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو شنیدم از رقیبان خبر عزیمت او</p></div>
<div class="m2"><p>دلم آن چنان ز جا شد که دگر به جا نیامد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو ز مهر دستانم به سر آمدند کس را</p></div>
<div class="m2"><p>ز خراب حالی من به زبان دعا نیامد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خبر من پریشان ببر ای صبا به آن مه</p></div>
<div class="m2"><p>پس از آن بگو که مسکین ز پیت چرا نیامد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز قدم شکستگی بود و فتادگی که قاصد</p></div>
<div class="m2"><p>به تو بی‌وفا فرستاد و خود از قفا نیامد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من خسته چون ز حیرت ندرم چو گل گریبان</p></div>
<div class="m2"><p>که رسولی از تو سویم به جز از صبا نیامد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز کجاشد آن صنم را سفر آرزو که هرگز</p></div>
<div class="m2"><p>ز زمانه محتشم را به سر این بلا نیامد</p></div></div>