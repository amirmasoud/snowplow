---
title: >-
    شمارهٔ  ۴۷۰
---
# شمارهٔ  ۴۷۰

<div class="b" id="bn1"><div class="m1"><p>گرچه در دیدهٔ‌تر جای تو نتوان کردن</p></div>
<div class="m2"><p>به همین قطع تمنای تو نتوان کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصل را گرچه به کوشش نتوان یافت ولی</p></div>
<div class="m2"><p>هجر را مانع سودای تو نتوان کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنم از بهر تو دانسته خلاف دل خویش</p></div>
<div class="m2"><p>چون خلاف دل دانای تو نتوان کردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه کفر است ز بس سرکشیت می‌ترسم</p></div>
<div class="m2"><p>کز خدا نیز تمنای تو نتوان کردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دل تنگی و این طرفه که نه گردون را</p></div>
<div class="m2"><p>صدف گوهر یکتای تو نتوان کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهم از خلق نهانت کنم اما چه کنم</p></div>
<div class="m2"><p>که تو خورشیدی و اخفای تو نتوان کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر سراپا چو فلک دیده توان گشت هنوز</p></div>
<div class="m2"><p>سیر خود را ز تماشای تو نتوان کردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر کنی وعده هم ای یار غلط وعده چه سود</p></div>
<div class="m2"><p>که نیائی و تقاضای تو نتوان کردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محتشم گر تو کنی ترک سخن صد کان را</p></div>
<div class="m2"><p>به دل طبع گهر زای تو نتوان کردن</p></div></div>