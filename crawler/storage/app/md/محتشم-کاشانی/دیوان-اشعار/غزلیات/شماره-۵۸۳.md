---
title: >-
    شمارهٔ  ۵۸۳
---
# شمارهٔ  ۵۸۳

<div class="b" id="bn1"><div class="m1"><p>اقبال ظفر پیوند در کار جهانبانی</p></div>
<div class="m2"><p>اقبال ولیخا نیست اقبال ولیخانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز وی به که داد ایزد در سلک سرافرازان</p></div>
<div class="m2"><p>اقبال شهنشاهی در مرتبهٔ خانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مخلوق به این نصرت ممکن نبود گویا</p></div>
<div class="m2"><p>موجود به شکل او شد نصرت ربانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن ضبط و پی افشردن در ضبط اساس ملک</p></div>
<div class="m2"><p>بعد دو جهانی داشت از طاقت انسانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلطانی و خانی را شرمست ز شان وی</p></div>
<div class="m2"><p>آن منصب دیگر را حق داردش ارزانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در ملک سخا جاهیست کانجا به رضای او</p></div>
<div class="m2"><p>یک مورچه می‌بخشد صد ملک سلیمانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دور فلک دورش دور است که بی‌جنبش</p></div>
<div class="m2"><p>دست دگرست اینجا در دایره گردانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در مدح ولیخان باد برپا علم کلکش</p></div>
<div class="m2"><p>تا محتشم افرازد رایات سخن رانی</p></div></div>