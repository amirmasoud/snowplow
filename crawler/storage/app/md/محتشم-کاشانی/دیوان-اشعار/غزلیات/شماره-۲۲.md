---
title: >-
    شمارهٔ  ۲۲
---
# شمارهٔ  ۲۲

<div class="b" id="bn1"><div class="m1"><p>چو افکنده ببیند در خون تنم را</p></div>
<div class="m2"><p>کنید آفرین ترک صید افکنم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیاید گر از دیده سیلی دمادم</p></div>
<div class="m2"><p>که شوید ز آلودگی دامنم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور از خاک آتش علم برنیاید</p></div>
<div class="m2"><p>که هر شام روشن کند مدفنم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به فانوس تن گر رسد گرمی دل</p></div>
<div class="m2"><p>بسوزد بر اندام پیراهنم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زغم چون گریزم که پیوسته دارد</p></div>
<div class="m2"><p>چو پیراهن این فتنه پیرامنم را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشرف کن ای ماه اوج سعادت</p></div>
<div class="m2"><p>ز مسکین نوازی شبی مسکنم را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دمهای بدگو مشو گرم قتلم</p></div>
<div class="m2"><p>بهر بادی آتش مزن خرمنم را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیم محتشم خالی از ناله چون نی</p></div>
<div class="m2"><p>که خوش دارد او شیوهٔ شیونم را</p></div></div>