---
title: >-
    شمارهٔ  ۱۴۸
---
# شمارهٔ  ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>درختان تا شوند از باد گاهی راست گاهی کج</p></div>
<div class="m2"><p>قد خلق از سجودت باد گاهی راست گاهی کج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بس حسرت که دارد بر تواضع کردن شیرین</p></div>
<div class="m2"><p>کشد نقش مرا فرهاد گاهی راست گاهی کج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زند پر مرغ روحم چون شود از باد جولانش</p></div>
<div class="m2"><p>اطاقه بر سر شمشاد گاهی راست گاهی کج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نزاکت بین که سروش می‌شود مانند شاخ گل</p></div>
<div class="m2"><p>به نازک جنبشی از باد گاهی راست گاهی کج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلا زه بر کمان بندد چو در رقص آن سهی بالا</p></div>
<div class="m2"><p>کند رعنا روی بنیاد گاهی راست گاهی کج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمان بر من کشید و دل‌نواز مدعی هم شد</p></div>
<div class="m2"><p>که تیرش بر نشان افتاد گاهی راست گاهی کج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به فکر قد و زلفش محتشم دیوانه شد امشب</p></div>
<div class="m2"><p>خیالش بس که رو می‌داد گاهی راست گاهی کج</p></div></div>