---
title: >-
    شمارهٔ  ۱۸
---
# شمارهٔ  ۱۸

<div class="b" id="bn1"><div class="m1"><p>گر بهم می‌زدم امشب مژهٔ پر نم را</p></div>
<div class="m2"><p>آب می‌برد به یک چشم زدن عالم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوز دیرینه‌ام از وصل نشد کم چه کنم</p></div>
<div class="m2"><p>که اثر نیست درین داغ کهن مرهم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن پری چهره مگر دست بدارد از جور</p></div>
<div class="m2"><p>ورنه بر باد دهد خاک بنی‌آدم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای تو را شیردلی در خم هر موی به بند</p></div>
<div class="m2"><p>قید هر صید مکن زلف خم اندر خم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنشین در حرم خاص دل ای دوست که من</p></div>
<div class="m2"><p>دور دارم ز رخت دیدهٔ نامحرم را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باددر بزم غمم نشه‌ای از درد نصیب</p></div>
<div class="m2"><p>که در آن نشئه ز شادی نشناسم غم را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواهی اکسیر بقا محتشم از دست مده</p></div>
<div class="m2"><p>ساغر دم به دم و ساقی عیسی دم را</p></div></div>