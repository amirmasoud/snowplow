---
title: >-
    شمارهٔ  ۳۹۳
---
# شمارهٔ  ۳۹۳

<div class="b" id="bn1"><div class="m1"><p>بس که چشم امشب به چشم عشوه‌سازش داشتم</p></div>
<div class="m2"><p>از نگه کردن بسوی غیر بازش داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیر جز تیر تغافل از کمان او نخورد</p></div>
<div class="m2"><p>بس که پاس غمزهٔ مردم نوازش داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به قصد نیم نازی ننگرد سوی رقیب</p></div>
<div class="m2"><p>گوشه چشمی به چشم نیم نازش داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشت راز من عیان بس کز اشارات نهان</p></div>
<div class="m2"><p>با رقیبان در مقام احترازش داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داشت او مستغنیم از ناز دیگر مهوشان</p></div>
<div class="m2"><p>از نیاز غیر من هم بی‌نیازش داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زور عشقم بین که تازان می‌گذشت آن شهسوار</p></div>
<div class="m2"><p>از کششهای کمند شوق بازش داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خیالش محتشم در دست بازی بود و من</p></div>
<div class="m2"><p>دست در زنجیر از زلف درازش داشتم</p></div></div>