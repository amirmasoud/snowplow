---
title: >-
    شمارهٔ  ۶۸
---
# شمارهٔ  ۶۸

<div class="b" id="bn1"><div class="m1"><p>با من بدی امروز زاطوار تو پیداست</p></div>
<div class="m2"><p>بدگو سخنی گفته ز گفتار تو پیداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همت آئینهٔ نیر دلان صورت خوبت</p></div>
<div class="m2"><p>این صورت از آئینهٔ رخسار تو پیداست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن نکته سربسته که مستی است بیانش</p></div>
<div class="m2"><p>ز آشفتگی بستن دستار تو پیداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خون یکی کرده‌ای امروز صبوحی</p></div>
<div class="m2"><p>از سرخوشی نرگس خون‌خوار تو پیداست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساغر زده می‌آئی و کیفیت مستی</p></div>
<div class="m2"><p>از بی سر و سامانی رفتار تو پیداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داری سر آزار که تهدید نهانی</p></div>
<div class="m2"><p>از جنبش لبهای شکر بار تو پیداست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دزدیده بهم بر زده‌ای خاطر جمعی</p></div>
<div class="m2"><p>از درهمی طره طرار تو پیداست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در حرف زدن محتشم از حیرت آن رو</p></div>
<div class="m2"><p>رفته است شعور تو ز اشعار تو پیداست</p></div></div>