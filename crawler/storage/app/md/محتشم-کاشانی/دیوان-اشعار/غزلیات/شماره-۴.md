---
title: >-
    شمارهٔ  ۴
---
# شمارهٔ  ۴

<div class="b" id="bn1"><div class="m1"><p>درخشان شیشه‌ای خواهم می رخشان در و پیدا</p></div>
<div class="m2"><p>چو زیبا پیکری از پای تا سر جان درو پیدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبازان در چو ناید دیده‌ام گوید چه بحرست این</p></div>
<div class="m2"><p>که هر گه باد ننشیند شود طوفان درو پیدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیه ابریست چشمم در هوای هالهٔ خطش</p></div>
<div class="m2"><p>علامتهای پیدا گشتن باران درو پیدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو گیرم پیش رویش باشدم هر دیده دریائی</p></div>
<div class="m2"><p>ز عکس چین زلفش موج بی‌پایان درو پیدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنی از استخوان و پوست دارم دل درو ظاهر</p></div>
<div class="m2"><p>چو فانوسی که باشد آتش پنهان درو پیدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پر از جدول نماید صفحهٔ آیینهٔ رویش</p></div>
<div class="m2"><p>که دایم هست عکس آن صف مژگان درو پیدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کف پایش که بوسد محتشم و ز خود رود هردم</p></div>
<div class="m2"><p>ز جان آئینه‌ای دان صورت بیجان درو پیدا</p></div></div>