---
title: >-
    شمارهٔ  ۱۹
---
# شمارهٔ  ۱۹

<div class="b" id="bn1"><div class="m1"><p>چو دی ز عشق من آگه شد و شناخت مرا</p></div>
<div class="m2"><p>به اولین نگه از شرم آب ساخت مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یک نگاه مرا گرم شوق ساخت ولی</p></div>
<div class="m2"><p>در انتظار نگاه دگر گداخت مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چنگ بیم رگ جانم آشکار سپرد</p></div>
<div class="m2"><p>ولی چنان که نفهمید کس نواخت مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عافیت شده بودم تمام نقد حضور</p></div>
<div class="m2"><p>به حیله برد دل عشق‌باز و باخت مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سواد اعظم اقلیم عافیت بودم</p></div>
<div class="m2"><p>خراب ساخت سواری به نیم تاخت مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از بهشت فراغت شدم به دوزخ عشق</p></div>
<div class="m2"><p>که هرگز از خنکی آن هوا نساخت مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دردمندی من کیست محتشم که الم</p></div>
<div class="m2"><p>به اهل درد نه پرداخت تا شناخت مرا</p></div></div>