---
title: >-
    شمارهٔ  ۴۹۰
---
# شمارهٔ  ۴۹۰

<div class="b" id="bn1"><div class="m1"><p>ز بس کز توست زیر بارجان مبتلای من</p></div>
<div class="m2"><p>چو ریگ از هم بپاشد کوه اگر باشد به جای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به قدر عشق اگر در حشر یابد مرتبت عاشق</p></div>
<div class="m2"><p>بود بر دوش مجنون در صف محشر لوای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شود مجنون ز لیلی منفعل فرهاد از شیرین</p></div>
<div class="m2"><p>چو با مهر تو سنجد داور محشر وفای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شود دوزخ سراسر حرف من گر عشق خوبان را</p></div>
<div class="m2"><p>گنه داند خدا وانگه به فعل آرد جزای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر در وادی وصلش بنودی یک جهان درمان</p></div>
<div class="m2"><p>مرا تنها جهانی درد کی دادی خدای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بس کز عاشقی پا در کلم ممکن نمی‌دانم</p></div>
<div class="m2"><p>که بیرون آید از گل روز محشر نیز پای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهر چشمی شود صد چشمه خون محتشم جاری</p></div>
<div class="m2"><p>چو افتد در میان روز قیامت ماجرای من</p></div></div>