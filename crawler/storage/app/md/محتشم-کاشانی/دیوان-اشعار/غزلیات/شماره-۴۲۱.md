---
title: >-
    شمارهٔ  ۴۲۱
---
# شمارهٔ  ۴۲۱

<div class="b" id="bn1"><div class="m1"><p>من منفعل که پیشت دو جهان گناه دارم</p></div>
<div class="m2"><p>بچه روی عذر گویم که رخ سیاه دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من اگر گناه‌کارم تو به عفو کار خود کن</p></div>
<div class="m2"><p>که زبان توبه گوی و لب عذر خواه دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم آن که یک جهان را ز غمت به باد دادم</p></div>
<div class="m2"><p>تو قبول اگر نداری دو جهان گواه دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه چنان برخش آهم زده تازه حسنت</p></div>
<div class="m2"><p>که عنان آن توانم نفسی نگاه دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چنین کشنده هجری سگ بخت چاره سازم</p></div>
<div class="m2"><p>که اگرچه دورم از در به دل تو راه دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز درون شعله خیزم مشو از غرور ایمن</p></div>
<div class="m2"><p>که درین نهفته‌ ترکش همه تیر آه دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به یکی نگاه جانم بستان که تا قیامت</p></div>
<div class="m2"><p>دل خویش را تسلی به همان نگاه دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملک‌الملکوک عشقم که به من نمانده الا</p></div>
<div class="m2"><p>تن بی‌قبا که به روی سر بی‌کلاه دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بتان تو را گزیدم که شه بتان حسنی</p></div>
<div class="m2"><p>من اگرچه خود گدایم دل پادشاه دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شه وادی جنونم به در آی ز شهر و بنگر</p></div>
<div class="m2"><p>که ز وحشیان صحرا چه قدر سپاه دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو به محتشم نداری نظری و من به این خوش</p></div>
<div class="m2"><p>گه نگاه دور دوری به تو گاه گاه دارم</p></div></div>