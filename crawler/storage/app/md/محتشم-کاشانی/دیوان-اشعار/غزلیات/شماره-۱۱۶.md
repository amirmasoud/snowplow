---
title: >-
    شمارهٔ  ۱۱۶
---
# شمارهٔ  ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>ای گل امروز اداهای تو بی‌چیزی نیست</p></div>
<div class="m2"><p>خندهٔ وسوسه فرمای تو بی‌چیزی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌زند غیر در صلح به من چیزی هست</p></div>
<div class="m2"><p>و اندرین باب تقاضای تو بی چیزی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میدهی پهلوی خاصان به اشارت جایم</p></div>
<div class="m2"><p>این خصوصیت بیجای تو بی‌چیزی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من خود ای شوخ گنه کارم و مستوجب قهر</p></div>
<div class="m2"><p>با من امروز مدارای تو بی‌چیزی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فاش در کشتن من گرچه نمی‌گوئی هیچ</p></div>
<div class="m2"><p>جنبش لعل شکرخای تو بی‌چیزی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنگ آشفتگی از روی تو گر نیست عیان</p></div>
<div class="m2"><p>پیچش زلف سمن سای تو بی‌چیزی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم زان ستم اندیش حذر کن که امروز</p></div>
<div class="m2"><p>اضطراب دل شیدای تو بی‌چیزی نیست</p></div></div>