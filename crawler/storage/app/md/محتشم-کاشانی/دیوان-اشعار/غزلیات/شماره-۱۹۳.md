---
title: >-
    شمارهٔ  ۱۹۳
---
# شمارهٔ  ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>چو عشق کوس سکون از گران عیاری زد</p></div>
<div class="m2"><p>قرار خیمه با صحرای بی‌قراری زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو روز ماند عیار حضور قلب درست</p></div>
<div class="m2"><p>ز اصل سکه چو برنقد کامکاری زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش آن نگار که چون کار و بار حسن آراست</p></div>
<div class="m2"><p>حجاب در نظرش دم ز پرده داری زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخست بر سر من تاخت هر شکار انداز</p></div>
<div class="m2"><p>که بر سمند جفا طبل جان شکاری زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دست مرحمتش کار مرهم آسان است</p></div>
<div class="m2"><p>کسی که بر دل من این خدنگ کاری زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرفت ناقه لیلی به خود سوی مجنون</p></div>
<div class="m2"><p>کز آن طرف کشش دست در عماری زد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبرد بار به منزل چو محتشم ز جفا</p></div>
<div class="m2"><p>کسی که پیش رخت لاف پرده‌داری زد</p></div></div>