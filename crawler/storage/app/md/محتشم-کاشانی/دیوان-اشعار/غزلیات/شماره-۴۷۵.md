---
title: >-
    شمارهٔ  ۴۷۵
---
# شمارهٔ  ۴۷۵

<div class="b" id="bn1"><div class="m1"><p>چون شدم صیدت به گیسوی خودت دربند کن</p></div>
<div class="m2"><p>تا ابد با خود به این قیدم قوی پیوند کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای گل رعنا برای عندلیب بی‌نصیب</p></div>
<div class="m2"><p>نیست گر بوئی به رنگی از خودت خورسند کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تلخی شیرین لبان ناموس را خوش مایه‌ایست</p></div>
<div class="m2"><p>تا توانی زهر باش ای شوخ و کار قند کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای مسیحا دم که صد بیمار در پی میروی</p></div>
<div class="m2"><p>یک نفس بنشین دوای دردمندی چند کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کعبهٔ مقصودی الحق سر زگمراهان مپیچ</p></div>
<div class="m2"><p>قبلهٔ حاجاتی آخر رو به حاجت‌مند کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌رود ای مادر ایام کار ما ز دست</p></div>
<div class="m2"><p>یک سفارش از برای ما به این فرزند کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اعتمادت نیست گر بر عهدهای محتشم</p></div>
<div class="m2"><p>خیز و هر یک عهد او محکم به صد پیوند کن</p></div></div>