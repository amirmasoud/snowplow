---
title: >-
    شمارهٔ  ۵۹۰
---
# شمارهٔ  ۵۹۰

<div class="b" id="bn1"><div class="m1"><p>دیده‌ام مست و سرانداز و غزل خوان برهی</p></div>
<div class="m2"><p>شاه مشرب پسری ترک و شی کج کلهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخل آتش ثمری سرو مرصع کمری</p></div>
<div class="m2"><p>عالم‌افروز سهیلی علم افراز مهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدر به این‌ده جان چشم فریبندهٔ دل</p></div>
<div class="m2"><p>طرفهٔ طاوس خرامی عجب آهو نگهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملک دل می‌رود از دست که کردست ظهور</p></div>
<div class="m2"><p>شاه عاشق حشمی خسرو یک دل سپهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقد جان بر طبق عرض نه ای دل که رسید</p></div>
<div class="m2"><p>باج خواهنده مهی کیسه تهی پادشهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیر ازو گر همه جان برد و بحل گشت که دید</p></div>
<div class="m2"><p>جان ستان آدمی رستمی بی‌گنهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم بهر فرود آمدن آن شه حسن</p></div>
<div class="m2"><p>ساز از دیده و ثاقی و ز دل بارگهی</p></div></div>