---
title: >-
    شمارهٔ  ۴۴۳
---
# شمارهٔ  ۴۴۳

<div class="b" id="bn1"><div class="m1"><p>وصل کو تا بی‌نیاز از وصل آن دلبر شوم</p></div>
<div class="m2"><p>ترک او گویم پرستار بت دیگر شوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل کو تا سرکشم یک چند از طوق جنون</p></div>
<div class="m2"><p>یعنی آزاد از کمند آن پری پیکر شوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کو دلی چون سنگ تا از لعل او یک‌بارگی</p></div>
<div class="m2"><p>برکنم دندان و خون آشام از آن ساغر شوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند غیرت بیند و گویند با من کاشکی</p></div>
<div class="m2"><p>کم شود حسن تو یا او کور یا من کر شوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من دم بیزاری از عشق تو می‌خواهم دگر</p></div>
<div class="m2"><p>با وجود آن که هردم بر تو عاشق‌تر شوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذره‌ای از من نخواهی یافت دیگر سوز خویش</p></div>
<div class="m2"><p>گر ز عشقت آن قدر سوزم که خاکستر شوم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صحبت ما و تو شدموقوف تا روزی که من</p></div>
<div class="m2"><p>با دل پرخون دو چارت در صفت محشر شوم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر طفیل توست اما با تو هستم سر گران</p></div>
<div class="m2"><p>تا به شمشیر اجل فارغ ز بار سر شوم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محتشم شد مانعم قرب رقیب از بزم او</p></div>
<div class="m2"><p>ورنه من می‌خواستم کز جان سگ آن در شوم</p></div></div>