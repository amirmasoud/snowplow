---
title: >-
    شمارهٔ  ۱۵
---
# شمارهٔ  ۱۵

<div class="b" id="bn1"><div class="m1"><p>ای نگهت تیغ تیز غمزهٔ غماز را</p></div>
<div class="m2"><p>پشت به چشم تو گرم قافلهٔ ناز را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز جزا تا رود شور قیامت به عرش</p></div>
<div class="m2"><p>رخصت یک عشوه ده چشم فسون ساز را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرگس مردم کشت ننگرد از گوشه‌ای</p></div>
<div class="m2"><p>تا نستاند به ناز جان نظر باز را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شعلهٔ بازار قتل پست شود گر کنی</p></div>
<div class="m2"><p>نایب ترکان چشم صد قدر انداز را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن تو در گل نهاد پای ملک بر فلک</p></div>
<div class="m2"><p>بس که نهادی بلند پایهٔ اعجاز را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم سخنگوی کرد کار زبان چون رقیب</p></div>
<div class="m2"><p>منع نمود از سخن آن بت طناز را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دید که خاصان تمام آفت جان منند</p></div>
<div class="m2"><p>داد به پیک نظر قاصدی راز را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یافت پس از صد نگه مطلب مخصوص خویش</p></div>
<div class="m2"><p>دیده که جوینده بود عشوهٔ ممتاز را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیز نگاهی به بزم پرده برافکند و کرد</p></div>
<div class="m2"><p>پرده در محتشم نرگس غماز را</p></div></div>