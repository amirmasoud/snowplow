---
title: >-
    شمارهٔ  ۹۸
---
# شمارهٔ  ۹۸

<div class="b" id="bn1"><div class="m1"><p>گرچه بیش از حد امکان التفات یار هست</p></div>
<div class="m2"><p>رشک هم چندان که ممکن نیست با اغیار هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زخم نوک خار رابا خود ده‌ای بلبل قرار</p></div>
<div class="m2"><p>کاندرین بستان گل بی‌خار را هم خار هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اضطرابم دار معذور ای پری کانجا که تو</p></div>
<div class="m2"><p>در ظهوری جنبش اندر صورت دیوار هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبرم آن مقدار میفرما که می‌خواهد دلت</p></div>
<div class="m2"><p>گر زمان حسن میدانی که آن مقدار هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند بر ما عرض عشق عاشقان خود کنی</p></div>
<div class="m2"><p>عشق اگر کم نیست ای گل حسن هم بسیار هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوش اهل عشق از نظم غزل بی‌بهره نیست</p></div>
<div class="m2"><p>تا زبان محتشم را قوت گفتار هست</p></div></div>