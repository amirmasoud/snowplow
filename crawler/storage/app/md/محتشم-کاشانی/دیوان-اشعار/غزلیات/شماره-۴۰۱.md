---
title: >-
    شمارهٔ  ۴۰۱
---
# شمارهٔ  ۴۰۱

<div class="b" id="bn1"><div class="m1"><p>ز کج بینی به زلفت نسبت چین ختن کردم</p></div>
<div class="m2"><p>غلط بود آن چه من دیدم خطا بود آن چه من کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر از محنت غربت بمیرم جای آن دارد</p></div>
<div class="m2"><p>که بهر چون تو بدخوئی چرا ترک وطن کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر از تربتم بوی وفا ناید عجب نبود</p></div>
<div class="m2"><p>که خاک پای آن بدمهر را عطر کفن کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو گوی از غم به سر می‌غلطم و بر خاک می‌گردم</p></div>
<div class="m2"><p>که خود را از چه سرگردان آن سیمین بد نکردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زور غصه‌ام کشت آن که عمری از برای او</p></div>
<div class="m2"><p>گرفتم کوه غم از پیش و کار کوهکن کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تواکنون گر دلی داری به سر کن محتشم با او</p></div>
<div class="m2"><p>که من خود ترک آن سنگین دل پیمان‌شکن کردم</p></div></div>