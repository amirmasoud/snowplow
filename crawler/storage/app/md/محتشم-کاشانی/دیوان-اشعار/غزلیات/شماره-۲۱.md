---
title: >-
    شمارهٔ  ۲۱
---
# شمارهٔ  ۲۱

<div class="b" id="bn1"><div class="m1"><p>روزگاری که رخت قبلهٔ جان بود مرا</p></div>
<div class="m2"><p>روی دل تافته از هر دو جهان بود مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند روزی که به سودای تو جان می‌دادم</p></div>
<div class="m2"><p>حاصل از زندگی خویش همان بود مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یادباد آن که به خلوتگه وصلت شب و روز</p></div>
<div class="m2"><p>دل سرا پردهٔ صد راز نهان بود مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یادباد آن که چو آغاز سخن می‌کردی</p></div>
<div class="m2"><p>با تو صد زمزمه در زیر زبان بود مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاد باد آن که چو می‌شد سرت از باده گران</p></div>
<div class="m2"><p>دوش منت کش آن بار گران بود مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یاد باد آن که به بالین تو شبهای دراز</p></div>
<div class="m2"><p>پاسبان مردم چشم نگران بود مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یاد باد آن که دمی گر ز درت می‌رفتم</p></div>
<div class="m2"><p>محتشم پیش سگان تو ضمان بود مرا</p></div></div>