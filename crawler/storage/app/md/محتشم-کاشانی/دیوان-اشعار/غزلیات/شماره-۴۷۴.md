---
title: >-
    شمارهٔ  ۴۷۴
---
# شمارهٔ  ۴۷۴

<div class="b" id="bn1"><div class="m1"><p>ای تو نکرده جز جفا آن چه نکرده‌ای بکن</p></div>
<div class="m2"><p>تیغ بکش به خون ما آن چه نکرده‌ای بکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای زده عقل و راه دین خواهی اگر متاع جان</p></div>
<div class="m2"><p>بی خبر از درم درا آن چه نکرده‌ای بکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند به منتم کشی کز ستمت نکشته‌ام</p></div>
<div class="m2"><p>ای ستمت به از وفا آن چه نکرده‌ای بکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که ربوده‌ای به رخ صد دل و مایلی بدین</p></div>
<div class="m2"><p>عقدهٔ زلف برگشا آن چه نکرده‌ای بکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که نبوده بر درت مثل من از جفا کشان</p></div>
<div class="m2"><p>میروم این زمان بیا آن چه نکرده‌ای بکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای نه نموده روی مه برده هزار دل ز ره</p></div>
<div class="m2"><p>روی به محتشم نما آن چه نکرده‌ای بکن</p></div></div>