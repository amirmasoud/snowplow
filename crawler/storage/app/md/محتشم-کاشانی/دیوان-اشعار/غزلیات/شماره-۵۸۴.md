---
title: >-
    شمارهٔ  ۵۸۴
---
# شمارهٔ  ۵۸۴

<div class="b" id="bn1"><div class="m1"><p>رو ای صبا بر آن سرو دلستان که تو دانی</p></div>
<div class="m2"><p>زمین به بوس که منت در آن زمان که تو دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو شرح حال تو پرسد ز محرمان به اشارت</p></div>
<div class="m2"><p>بگو که قاصدم از جانب فلان که تو دانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس از نیاز به او عرض کن چنانکه نرنجد</p></div>
<div class="m2"><p>حکایتی ز زبانم به آن زبان که تو دانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر به خنده لب کامبخش خود نگشاید</p></div>
<div class="m2"><p>ازو به گریه و زاری طلب کن آن که تو دانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر به ابروی پرچین گره زند به کرشمه</p></div>
<div class="m2"><p>گره‌گشائی ازین کار کن چنان که تو دانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشان خنده چو پیدا بود از آن لب نوشین</p></div>
<div class="m2"><p>همان به خواه که گفتیم به آن لسان که تو دانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جز صبا که برد محتشم چنین غزلی را</p></div>
<div class="m2"><p>دلیر جانب آن سرو نکته‌دان که تو دانی</p></div></div>