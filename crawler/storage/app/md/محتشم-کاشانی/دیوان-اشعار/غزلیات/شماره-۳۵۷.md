---
title: >-
    شمارهٔ  ۳۵۷
---
# شمارهٔ  ۳۵۷

<div class="b" id="bn1"><div class="m1"><p>من بی‌تو ندارم از چمن حظ</p></div>
<div class="m2"><p>دور از سمنت ز یاسمن حظ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی روی تو در چمن ندارند</p></div>
<div class="m2"><p>از صحبت هم گل و سمن حظ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌قد تو نارواست کردن</p></div>
<div class="m2"><p>از دیدن سرو و نارون حظ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک ذره نمی‌فروشم ای گل</p></div>
<div class="m2"><p>تشویق تو من به صد تو من حظ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش می‌کند از دراز دستی</p></div>
<div class="m2"><p>آغوش تو از تو سیمتن حظ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با حسن طبیعت است کز وی</p></div>
<div class="m2"><p>با طبع کنند مرد و زن حظ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جعد تو ذقن طراز دل را</p></div>
<div class="m2"><p>چون تشنه از آن چه ذقن حظ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز جام که دید از آن دهن کام</p></div>
<div class="m2"><p>جز جامه که کرد ازان بدن حظ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای می که به جوشم از تو چون خم</p></div>
<div class="m2"><p>خوش داری از آن لب و دهن حظ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این پیرهن این توای که داری</p></div>
<div class="m2"><p>زان جوهر زیر پیرهن حظ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی‌تابم از این که می‌کند زلف</p></div>
<div class="m2"><p>بازی بازی از آن ذقن حظ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لب می‌گزم از حسد که دارد</p></div>
<div class="m2"><p>خط زان دو لب شکرشکن حظ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در مهد که دایه ساقیش بود</p></div>
<div class="m2"><p>می‌کرد از آن لبان لبن حظ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گو شیخ مگو مراخطا کار</p></div>
<div class="m2"><p>من دارم از آن بت ختن حظ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>او ره زن کاروان جانهاست</p></div>
<div class="m2"><p>وین قافله را ز راه زن حظ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پر زلزله شد جهان و دارد</p></div>
<div class="m2"><p>زان زلزله در جهان فکن حظ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با لذت عشق خسروی داشت</p></div>
<div class="m2"><p>شیرین ز مذاق کوه‌کن حظ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پروانه قرب شمع یابد</p></div>
<div class="m2"><p>مرغی که کند ز سوختن حظ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شد گرم که آردم به اعراض</p></div>
<div class="m2"><p>اعراض رقیب داشتن حظ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بد خوئی محتشم به این خوی</p></div>
<div class="m2"><p>خطیست که دارد از سخن حظ</p></div></div>