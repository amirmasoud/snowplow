---
title: >-
    شمارهٔ  ۱۱۲
---
# شمارهٔ  ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>گر با توام ز دیدن غیرم گزیر نیست</p></div>
<div class="m2"><p>ور دورم از تو خاطرم آرام‌گیر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هجر اینچنینم و در وصل آن چنان</p></div>
<div class="m2"><p>خوش آن که هجر و وصل تواش در ضمیر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیمار دل به ترک تو صحبت‌پذیر نیست</p></div>
<div class="m2"><p>اما بلاست اینکه نصیحت‌پذیر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرهاد رخم پرور چشم حقارتست</p></div>
<div class="m2"><p>اما به دیدهٔ دل شیرین حقیر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسرو حریص تاختن رخش شور هست</p></div>
<div class="m2"><p>اما حریف ساختن جوی شیر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در زیر خنجر اجلش شکر واجب است</p></div>
<div class="m2"><p>صیدی که او بقید محبت اسیر نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سینهٔ خار اشارات او به غیر</p></div>
<div class="m2"><p>زخمیست محتشم که کم از زخم‌تیر نیست</p></div></div>