---
title: >-
    شمارهٔ  ۴۹
---
# شمارهٔ  ۴۹

<div class="b" id="bn1"><div class="m1"><p>خیالش را به نوعی انس در جان من است امشب</p></div>
<div class="m2"><p>که با این نیم جانیها دو جانم در تنست امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به صحبت هر که را خواند نهان آرد به قتل آخر</p></div>
<div class="m2"><p>مرا هم خوانده گویا نبوت قتل منست امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کف شمشیر و در سر باده چند اغیار را جوئی</p></div>
<div class="m2"><p>مرا هم هست جانی کز غرض خونخوردنست امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بدمستی به مجلس دستم اندر گردن افکندی</p></div>
<div class="m2"><p>اگر من جان برم صدخونت اندر گردنست امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سری کز باده بودی بر سر دوش سرافرازان</p></div>
<div class="m2"><p>به هشیاری من افتاده را در دامنست امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرم کوبند اگر چون زر بهم باشد به مهر او</p></div>
<div class="m2"><p>که دل اسرار آن طرف عیار مخزنست امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بزم دوست محروم از زبان خود شدم اما</p></div>
<div class="m2"><p>چه‌ها دربارهٔ من بر زبان دشمن است امشب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن خلعت که بر قد رقیب از لطف میدوزی</p></div>
<div class="m2"><p>هزارم سوزن الماس در پیراهن است امشب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دمی بر محتشم پیما می دیدار ای ساقی</p></div>
<div class="m2"><p>که ذوقش جرعه خواه از باده مردافکن است امشب</p></div></div>