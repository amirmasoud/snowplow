---
title: >-
    شمارهٔ  ۲۷۲
---
# شمارهٔ  ۲۷۲

<div class="b" id="bn1"><div class="m1"><p>گر از درج دهانش دم زنم از من به تنگ آید</p></div>
<div class="m2"><p>ور از خوی بدش گویم سخن به جنگ آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پردازم به تیر از دل کشیدن کو برآرد پر</p></div>
<div class="m2"><p>ز بس کز شست او بر دل خدنگ بی‌درنگ آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخ از می ارغوانی کرد و بیرون رفت از مجلس</p></div>
<div class="m2"><p>به این رنگ از بر ما رفت تا دیگر چه رنگ آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آه گریه آلودم خط ز نگاریش سر زد</p></div>
<div class="m2"><p>چو نم گیرد هوا ناچار بر آئینهٔ زنگ آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان بدنام عالم گشتم از عشق نکونامی</p></div>
<div class="m2"><p>که اهل عشق را ننگ از من بی‌نام و ننگ آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حذر کن گزندم زین نخستین ای رقیب از دل</p></div>
<div class="m2"><p>که در ره نیش کار دهر که راز سینه سنگ آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگویم قصهٔ دلتنگی خود محتشم با او</p></div>
<div class="m2"><p>که ترسم من نیابم حاصلی و آن مه به تنگ آید</p></div></div>