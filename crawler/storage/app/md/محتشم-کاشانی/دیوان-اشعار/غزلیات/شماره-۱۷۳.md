---
title: >-
    شمارهٔ  ۱۷۳
---
# شمارهٔ  ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>دیگر که هوای گل خود روی تو دارد</p></div>
<div class="m2"><p>سیلاب سرشک که سر کوی تو دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر هم زده دارد گل نازک ورقت را</p></div>
<div class="m2"><p>آن باد مخالف که گذر سوی تو دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق تو چه عام است که هرکس به تصور</p></div>
<div class="m2"><p>آئینهٔ خاصی ز مه روی تو دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر شیفته کز جیب جنون سر بدر آرد</p></div>
<div class="m2"><p>بر گردن دل سلسله از موی تو دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر مرغ محبت که به آهنگ دمی خاست</p></div>
<div class="m2"><p>شهبال توجه ز دو ابروی تو دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دام که افکنده فلک در ره صیدی</p></div>
<div class="m2"><p>پیوند بسر رشتهٔ گیسوی تو دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر بی سر و پا را که خرد راند چه دیدم</p></div>
<div class="m2"><p>مجنون شده سر در پی آهوی تو دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر تیر که عشق از سر بازیچه رها کرد</p></div>
<div class="m2"><p>زور اثر قوت بازوی تو دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر خیمه که از وسوسه زد خانهٔ سیاهی</p></div>
<div class="m2"><p>آن خیمه ستون از قد دل جوی تو دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر باد که جائی گل عشقی شکفانید</p></div>
<div class="m2"><p>چون نیک رسیدیم به او بوی تو دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر بوالهوسی یک غزل محتشم آموخت</p></div>
<div class="m2"><p>صد زمزمه با لعل سخنگوی تو دارد</p></div></div>