---
title: >-
    شمارهٔ  ۴۵۰
---
# شمارهٔ  ۴۵۰

<div class="b" id="bn1"><div class="m1"><p>بس که ما از روی رسوایی نقاب افکنده‌ایم</p></div>
<div class="m2"><p>عشق رسوا را هم از خود در حجاب افکنده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا فکنده طرح صلح آن جنگجو با ما هنوز</p></div>
<div class="m2"><p>یاز دهشت خویش را در اضطراب افکنده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آتش دل دوزخی داریم کز اندیشه‌اش</p></div>
<div class="m2"><p>خلق را پیش از قیامت در عذاب افکنده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مژده ده صبح شهادت را که چون هندوی شب</p></div>
<div class="m2"><p>ما سر خود پیش تیغ آفتاب افکنده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخش خواهش را عنان گردیده بیش از حد سبک</p></div>
<div class="m2"><p>گرچه ما از صبر لنگر بر رکاب افکنده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پاس بیداران این مجلش تو را ای دل که ما</p></div>
<div class="m2"><p>از برای مصلحت خود را به خواب افکنده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما به راه عشق با این شعف از تاثیر شوق</p></div>
<div class="m2"><p>پا ز کار افتادگان را رد شتاب افکنده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لنگری ای توبه فرمایان که ما این دم هنوز</p></div>
<div class="m2"><p>کشتی ساغر به دریای شراب افکنده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محتشم اکنون که یاران طرح شعر افکنده‌اند</p></div>
<div class="m2"><p>ما قلم بشکسته آتش در کتاب افکنده‌ایم</p></div></div>