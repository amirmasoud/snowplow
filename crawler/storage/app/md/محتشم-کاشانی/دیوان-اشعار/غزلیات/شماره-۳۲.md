---
title: >-
    شمارهٔ  ۳۲
---
# شمارهٔ  ۳۲

<div class="b" id="bn1"><div class="m1"><p>برین در می‌کشند امشب جهان‌پیما سمندی را</p></div>
<div class="m2"><p>به سرعت می‌برند از باغ ما سرو بلندی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم صحرائیان دارم که غافل گیری گردون</p></div>
<div class="m2"><p>به صحرا می‌برد از شهر بند صید بندی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپهرم مایهٔ بازیچهٔ خود کرده پنداری</p></div>
<div class="m2"><p>که باز از گریه‌ام درخنده دارد نوشخندی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سزاوار فراقم من که از خوبان پسندیدم</p></div>
<div class="m2"><p>دل بیزار الفت دشمنی آفت پسندی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی‌گفتم که آن بی درد با صد غصه نگذارد</p></div>
<div class="m2"><p>به درد بی‌کسی در کنج محنت دردمندی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم ازسینه خواهد جست بیرون محتشم تا کی</p></div>
<div class="m2"><p>بود تاب نشستن در دل آتش سپندی را</p></div></div>