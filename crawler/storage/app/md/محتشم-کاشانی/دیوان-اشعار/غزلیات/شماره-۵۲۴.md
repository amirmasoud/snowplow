---
title: >-
    شمارهٔ  ۵۲۴
---
# شمارهٔ  ۵۲۴

<div class="b" id="bn1"><div class="m1"><p>کاکل که سر نهاده به طرف جبین تو</p></div>
<div class="m2"><p>صد فتنه می‌کند به سر نازنین تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کین منت نشسته به خاطر مگر رقیب</p></div>
<div class="m2"><p>حرفی ز کینه ساخته خاطر نشین تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمری دمید بر تو دل گرم بافسون</p></div>
<div class="m2"><p>وز کین نگشت گرم دل آهنین تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هشدار ای غزال که صد جا نشسته‌اند</p></div>
<div class="m2"><p>صید افکنان دست هوس در کمین تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین دستبردها چو نگین در حصار باش</p></div>
<div class="m2"><p>تا هست ملک حسن به زیر نگین تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر پی بری به کج نظری‌های مدعی</p></div>
<div class="m2"><p>حاصل شود به راستی ما یقین تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیرت نگر که میرم اگر وقت کشتنم</p></div>
<div class="m2"><p>گیرد ز رحم دست تو را آستین تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای محتشم اگر به مه من رسی بگو</p></div>
<div class="m2"><p>کز هجر مرد عاشق زار حزین تو</p></div></div>