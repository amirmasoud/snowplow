---
title: >-
    شمارهٔ  ۳۵۵
---
# شمارهٔ  ۳۵۵

<div class="b" id="bn1"><div class="m1"><p>صبر در جور و جفای تو غلط بود غلط</p></div>
<div class="m2"><p>تکیه برعهد و وفای تو غلط بود غلط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش ابروی کجت سجده خطا بود خطا</p></div>
<div class="m2"><p>سر نهادن به رضای تو غلط بود غلط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با تو شطرنج هوس چیدن و بودن ز غرور</p></div>
<div class="m2"><p>ایمن از مغلطهای تو غلط بود غلط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردبر درد خود افزودن و صابر بودن</p></div>
<div class="m2"><p>به تمنای دوای تو غلط بود غلط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بناشادیم ای شوخ بلا بودی شاد</p></div>
<div class="m2"><p>شادبودن به بلای تو غلط بود غلط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود چون رای تو آزار من از بهر رقیب</p></div>
<div class="m2"><p>دیدن آزار برای تو غلط بود غلط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم حسرت پابوس تو چون برد به خاک</p></div>
<div class="m2"><p>جانفشانیش به پای تو غلط بود غلط</p></div></div>