---
title: >-
    شمارهٔ  ۳۵۸
---
# شمارهٔ  ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>ز لاله‌زار مرا بی‌جمال دل نواز چه فیض</p></div>
<div class="m2"><p>ز جام می لب ساقی گل عذار چه حظ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در انجمن که نباشد مغنی گل رخ</p></div>
<div class="m2"><p>ز صوت فاخته و نغمهٔ هزار چه حظ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکار تا شده دلهای بی محبت را</p></div>
<div class="m2"><p>ز تیر غمزهٔ خوبان جان شکار چه حظ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو نیست در نظر آن گل که نوبهار من است</p></div>
<div class="m2"><p>مرا ز باغ چه حاصل ز نوبهار چه حظ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غرض مشاهده حسن توست از خوبان</p></div>
<div class="m2"><p>وگر بی‌تو ز خوبان روزگار چه حظ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین دیار دل محتشم خوش است به یار</p></div>
<div class="m2"><p>گهی که یار نباشد درین دیار چه حظ</p></div></div>