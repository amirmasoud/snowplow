---
title: >-
    شمارهٔ  ۲۴۶
---
# شمارهٔ  ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>یک دم ای سرو ز غمهای تو آزاد که بود</p></div>
<div class="m2"><p>یک شب ای ماه ز بیداد تو بیداد که بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم از ذوق چودی تیغ کشیدی بر من</p></div>
<div class="m2"><p>کامشب از درد درین کوی به فریاد که بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دور از بزم تو ماندم که ز می‌شستم دست</p></div>
<div class="m2"><p>ورنه آن کس که مرا توبه ز می داد که بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به خاک رهم از کینه برابر کردی</p></div>
<div class="m2"><p>آن که پا بر سرم از دست تو ننهاد که بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخت دور از تو چه می‌کرد به خواب اجلم</p></div>
<div class="m2"><p>آن که ننمود درین واقعه ارشاد که بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون به ناشادی مردم ز تو شادان بودم</p></div>
<div class="m2"><p>آن که ناشادی من دید و نشد شاد که بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون تو ماهی که نترسید ز آه من و داد</p></div>
<div class="m2"><p>خرمن محتشم دلشده برباد که بود</p></div></div>