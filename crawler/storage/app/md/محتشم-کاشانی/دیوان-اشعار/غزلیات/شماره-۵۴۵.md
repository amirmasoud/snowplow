---
title: >-
    شمارهٔ  ۵۴۵
---
# شمارهٔ  ۵۴۵

<div class="b" id="bn1"><div class="m1"><p>نمی‌دانم ز خود افتادگان داری خبر یا نه</p></div>
<div class="m2"><p>ز دور این نالهٔ ما در دلت دارد اثر یا نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یقین داری که دارم از خیالت پیکری با خود</p></div>
<div class="m2"><p>که شب تا صبح دم می‌گردمش بر گرد سر یانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گوشت هیچ می‌گوید که اینک می‌رسد از پی</p></div>
<div class="m2"><p>چو باد صرصر آن دیوانهٔ صحرا سپر یا نه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خاطر میرسانی هیچ گه کان دشت پیما را</p></div>
<div class="m2"><p>به زور انداختم از پا من بیدادگر یا نه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برای آزمایش بار من بر کوه نه یک دم</p></div>
<div class="m2"><p>ببین خواهد شکستن کوه را صد جا کمر یا نه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو جان را نیست در رفتن توقف هیچ میگوئی</p></div>
<div class="m2"><p>که باید بازگشتن بی‌توقف زین سفر یا نه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوشتم نامه وز گمراهی طالع نمی‌دانم</p></div>
<div class="m2"><p>که خواهد ره به آن مه برد مرغ نامه‌بر یا نه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیا و محتشم از بهر من دیوان خود بگشا</p></div>
<div class="m2"><p>به بین بر لشگر غم می‌کنم آخر ظفر یا نه</p></div></div>