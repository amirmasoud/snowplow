---
title: >-
    شمارهٔ  ۲۷۶
---
# شمارهٔ  ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>چو غافل از اجل صیدی سوی صیاد می‌آید</p></div>
<div class="m2"><p>نخستین رفتن خویشم در آن کو یاد می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من پا بسته روز وعده‌ات آن مضطرب صیدم</p></div>
<div class="m2"><p>که خود را می‌کشم در قید تا صیاد می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر دیگر مخاطب نیستم پیشش چرا قاصد</p></div>
<div class="m2"><p>جواب نامه‌ام می‌آرد و ناشاد می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خون ریز من مسکین چو فرمان داده‌ای باری</p></div>
<div class="m2"><p>وصیت میکن از من گوش تا جلاد می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بتان را هست جانب دارای پنهان که خسرو را</p></div>
<div class="m2"><p>به آن غالب حریفی رشک بر فرهاد می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلیل اتحاد این بس که خون میرانداز مجنون</p></div>
<div class="m2"><p>به دست لیلی آن نیشی که از فساد می‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل خامش زبانم کرده فرقت نامه‌ای انشا</p></div>
<div class="m2"><p>که هرگه می‌نویسم خامه در فریاد می‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببین ای پند گوآه من و بر مجمع دیگر</p></div>
<div class="m2"><p>چراغ خویش روشن کن که اینجا باد می‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان می‌آید از دل آه سرد محتشم سوزان</p></div>
<div class="m2"><p>که پنداری ز راه کوره حداد می‌آید</p></div></div>