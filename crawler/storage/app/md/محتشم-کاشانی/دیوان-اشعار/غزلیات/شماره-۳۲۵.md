---
title: >-
    شمارهٔ  ۳۲۵
---
# شمارهٔ  ۳۲۵

<div class="b" id="bn1"><div class="m1"><p>ای سنگ دل ز پرسش روز جزا بترس</p></div>
<div class="m2"><p>خون من غریب مریز از خدا بترس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دم به سینه راه مده کینهٔ مرا</p></div>
<div class="m2"><p>وز آه سینه سوز من مبتلا بترس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر بی‌دلان ز سخت دلیها مکش عنان</p></div>
<div class="m2"><p>از سنگ خود نه‌ای تو ز تیر دعا بترس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌ترس و باک من به خطا ترک کس مکن</p></div>
<div class="m2"><p>زان ناوک خطا که ندارد خطا بترس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دی با رقیب یافت مرا آشنا و گفت</p></div>
<div class="m2"><p>ای محتشم ازین سگ نا آشنا بترس</p></div></div>