---
title: >-
    شمارهٔ  ۳۱۶
---
# شمارهٔ  ۳۱۶

<div class="b" id="bn1"><div class="m1"><p>ز دور یاسمنت سبزه سر نکرده هنوز</p></div>
<div class="m2"><p>بنفشه از سمنت سربدر نکرده هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گرد ماه عذارت نگشته هالهٔ زلف</p></div>
<div class="m2"><p>خطت احاطه دور قمر نکرده هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه جای خط که نسیمی از آن خجسته بهار</p></div>
<div class="m2"><p>به گلستان جمالت گذر نکرده هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرفته‌ای همهٔ عالم به حسن عالم گیر</p></div>
<div class="m2"><p>اگرچه لشگر خط تو سر نکرده هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم نمی‌خوری و میبری گمان که فلک</p></div>
<div class="m2"><p>مرا ز مهر تو بی‌خواب و خور نکرده هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شمع گرم ملاقات مردمی و صبا</p></div>
<div class="m2"><p>ز آه سرد منت باخبر نکرده هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نصیحتت که به صد گونه کرده‌ام پیداست</p></div>
<div class="m2"><p>که در دلت یکی از صد اثر نکرده هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولی با این همه مجنون دل رمیدهٔ تو</p></div>
<div class="m2"><p>خیال طرفه غزال دگر نکرده هنوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز چشم اگرچه فکندی فتاده خود را</p></div>
<div class="m2"><p>ز الفتات تو قطع نظر نکرده هنوز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عجب که این غزل امشب به سمع یار رسد</p></div>
<div class="m2"><p>که هست تازه و مطرب ز بر نکرده هنوز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز محتشم مکن ای گل تو نیز قطع نظر</p></div>
<div class="m2"><p>که جای غیر تو در چشم تر نکرده هنوز</p></div></div>