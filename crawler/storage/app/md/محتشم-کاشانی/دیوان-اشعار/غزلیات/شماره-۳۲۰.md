---
title: >-
    شمارهٔ  ۳۲۰
---
# شمارهٔ  ۳۲۰

<div class="b" id="bn1"><div class="m1"><p>با من از ابنای عالم دلبری مانده است و بس</p></div>
<div class="m2"><p>دلبری را تا که در عالم نمی‌ماند به کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار چشم نیم باز اوست در میدان ناز</p></div>
<div class="m2"><p>از خدنگ نیم کس فارس فکندن از فرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار بر در کی ستادی غیر در بر کی بدی</p></div>
<div class="m2"><p>آن غلط تمییز اگر بشناختی عشق از هوس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست امشب محمل لیلی روان یا کرده‌اند</p></div>
<div class="m2"><p>بهر سرگردانی مجنون زبان بند جرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون دل کز سینه تال میزد از دست تو جوش</p></div>
<div class="m2"><p>عاقبت راه تردد بست بر پیک نفس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد جهان جان خواهم از بهر بلا گردانیت</p></div>
<div class="m2"><p>چون به حشر آئی دو عالم دادخواهداز پیش و پس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرغ طبعم را مکن آزار کو را داده‌اند</p></div>
<div class="m2"><p>آشیان آنجا که ایمن نیست سیمرغ از مگس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من گل آن آتشین با غم که در پیرامنش</p></div>
<div class="m2"><p>برق عالم سوز دارد صد خطر از خار و خس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محتشم را یک نظر باقیست در چشم و لبت</p></div>
<div class="m2"><p>یک نگه دارد تمنا یک سخن دارد هوس</p></div></div>