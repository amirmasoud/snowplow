---
title: >-
    شمارهٔ  ۸۸
---
# شمارهٔ  ۸۸

<div class="b" id="bn1"><div class="m1"><p>منتظری عمرها گر بگذاری نشست</p></div>
<div class="m2"><p>آخر از آن ره بر او گردسواری نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه ز دشت وجود خاست درین صید گاه</p></div>
<div class="m2"><p>بهر وی اندر کمین شیر شکاری نشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد تو را چون رساند فتنه به میدان دهر</p></div>
<div class="m2"><p>هرکه سر فتنه داشت رفت و به کاری نشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غمزه زنان آمدی شاهسوار اجل</p></div>
<div class="m2"><p>تیغ به دست تو داد خود به کناری نشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون مرا گرچه داد عاشقی تو به باد</p></div>
<div class="m2"><p>هیچ ازین رهگذر بر تو غباری نشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در قدح عشق‌ریز باده مرد آزمای</p></div>
<div class="m2"><p>کز سر دعوی به بزم باده گساری نشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم خسته را پر بره انتظار</p></div>
<div class="m2"><p>چهره به خون شد نگار تا به نگاری نشست</p></div></div>