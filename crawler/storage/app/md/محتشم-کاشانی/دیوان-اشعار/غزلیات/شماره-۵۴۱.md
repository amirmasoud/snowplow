---
title: >-
    شمارهٔ  ۵۴۱
---
# شمارهٔ  ۵۴۱

<div class="b" id="bn1"><div class="m1"><p>به دست دیده عنان دل فکار مده</p></div>
<div class="m2"><p>مرا ببین و به چشم خود اختیار مده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز غیرت ای گل نازک ورق چو دامن پاک</p></div>
<div class="m2"><p>کشیدی از کف بلبل به چنگ خار مده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رشک دادن من در دو روزه رنجش خود</p></div>
<div class="m2"><p>هزار مست هوس را به بزم بار مده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به غیر کامده زان زلف تابدار به رنج</p></div>
<div class="m2"><p>به غیر شربت شمشیر آب‌دار مده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غرور سد نگه شد خدای را زین بیش</p></div>
<div class="m2"><p>شراب ناز به آن چشم پر خمار مده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بز جر منصب فرهادیم بده اما</p></div>
<div class="m2"><p>ز حکم خسرویم سر به کوهسار مده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار وعدهٔ پر انتظار دادی و رفت</p></div>
<div class="m2"><p>کنون که وعده قتل است انتظار مده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرفته تیغ تو چون در نیام ناز قرار</p></div>
<div class="m2"><p>نوید قتل به جان‌های بی‌قرار مده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر به هیچ نمی‌ارزم از زبون کشیم</p></div>
<div class="m2"><p>به دست چشم سیه مست جان شکار مده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وگر به کار تو می‌آیم از برای خودم</p></div>
<div class="m2"><p>نگاه دار و به چنگال روزگار مده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غرض اطاعت حکم است محتشم زین نظم</p></div>
<div class="m2"><p>به طول دردسر آن بزرگوار مده</p></div></div>