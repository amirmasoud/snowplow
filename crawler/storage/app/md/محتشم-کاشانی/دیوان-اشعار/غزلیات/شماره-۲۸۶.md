---
title: >-
    شمارهٔ  ۲۸۶
---
# شمارهٔ  ۲۸۶

<div class="b" id="bn1"><div class="m1"><p>ای زهر خندهٔ تو چو شهد و شکر لذیذ</p></div>
<div class="m2"><p>زهر تو از نبات کسان بیشتر لذیذ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از قد و لب ریاض تو را ای بهار ناز</p></div>
<div class="m2"><p>هم نخل نازک آمده و هم ثمر لذیذ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدت که هست نیشکر بوستان حسن</p></div>
<div class="m2"><p>سر تا به پاست لذت و پا تا به سر لذیذ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دشنام تلخ زود مکن بس که در مذاق</p></div>
<div class="m2"><p>زهریست این که بیشتر است از شکر لذیذ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی هزار گنج نهادی شکر فروش</p></div>
<div class="m2"><p>بودی اگر شکر جو لبت ای پسر لذیذ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن لب که من گزیده‌ام امروز کافرم</p></div>
<div class="m2"><p>گر میوهٔ بهشت بود این قدر لذیذ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطرب ز محتشم غزلی کن ادا که هست</p></div>
<div class="m2"><p>نظم وی و ادای تو با یکدیگر لذیذ</p></div></div>