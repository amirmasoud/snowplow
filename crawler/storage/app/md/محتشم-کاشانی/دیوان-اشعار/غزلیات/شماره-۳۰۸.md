---
title: >-
    شمارهٔ  ۳۰۸
---
# شمارهٔ  ۳۰۸

<div class="b" id="bn1"><div class="m1"><p>ناصحا از سر بالین من این پند ببر</p></div>
<div class="m2"><p>خفته بیدار به افسانه نگردد هرگز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ غم ترک دل ما نکند تا به ابد</p></div>
<div class="m2"><p>جغد دلگیر ز ویرانه نگردد هرگز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای مقیمانه درین دیر دو در کرده مقام</p></div>
<div class="m2"><p>خیز کاین راهگذر خانه نگردد هرگز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک دم ای شیخ خبر باش که جنت به جحیم</p></div>
<div class="m2"><p>به دل از جرم دو پیمانه نگردد هرگز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه جان گردد اگر آب و هوا در تن سرو</p></div>
<div class="m2"><p>جانشین قد جانانه نگردد هرگز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محتشم چشم امید تو به این رشحهٔ رشگ</p></div>
<div class="m2"><p>صدف آن در یک دانه نگردد هرگز</p></div></div>