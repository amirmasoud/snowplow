---
title: >-
    شمارهٔ  ۵۵۱
---
# شمارهٔ  ۵۵۱

<div class="b" id="bn1"><div class="m1"><p>بر دل فکنده پرتو نادیده آفتابی</p></div>
<div class="m2"><p>در پرده بازی کرد رخساره در نقابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بحر دل هوائی گردیده شورش انگیز</p></div>
<div class="m2"><p>وز جای خویش جنبید دریای اضطرابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌باک خسروی داد فرمان به غارت جان</p></div>
<div class="m2"><p>دیوانه لشگری تاخت بر کشور خرابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گنجشک را چه طاقت در عرصه‌ای که آنجا</p></div>
<div class="m2"><p>گرم شکار گردد سیمرغ کش عقابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاشاک کی بماند بر ساحل سلامت</p></div>
<div class="m2"><p>از قلزمی که خیزد آتش‌فشان سحابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر رخش عبرت ای دل زین نه که می‌دهد باز</p></div>
<div class="m2"><p>دادسبک عنانی صبر گران رکابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ما اثر چه ماند در کشوری که راند</p></div>
<div class="m2"><p>کام از هلاک درویش سلطان کامیابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از نیم رشحه امروز پا در گلم چه سازم</p></div>
<div class="m2"><p>فردا که گردد این نم از سرگذشته آبی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان لب که می‌فشاند بر سایل آب حیوان</p></div>
<div class="m2"><p>جان تشنه سئوالیست من کشته جوابی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیروز با تو دل را صدپرده در میان بود</p></div>
<div class="m2"><p>امروز در میان نیست جز پردهٔ حجابی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای محتشم درین بزم مردانه کوش کایام</p></div>
<div class="m2"><p>بهر تو کرده در جام مردآزما شرابی</p></div></div>