---
title: >-
    شمارهٔ  ۱۶۷
---
# شمارهٔ  ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>دلم از غمش چه گویم که ره نفس ندارد</p></div>
<div class="m2"><p>غم او نمی‌گذارد که نفس نگه ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه ز مزرع امیدم دمد از جفای ترکی</p></div>
<div class="m2"><p>که ز ابر التفاتش همه تیغ و تیر بارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تن خویش تا سپردم به سگش ز غیرت آن</p></div>
<div class="m2"><p>که خدنگ نیمه‌کش را نفسی نگاه دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نشستنش به مسجد به ره نیاز زاهد</p></div>
<div class="m2"><p>شده یک جهت نمازی به دو قبل می‌گذارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو که داغ تیره روزی نشمرده‌ای چه دانی</p></div>
<div class="m2"><p>شب تار محتشم را که ستاره می‌شمارد</p></div></div>