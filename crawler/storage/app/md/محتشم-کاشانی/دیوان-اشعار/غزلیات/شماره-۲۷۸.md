---
title: >-
    شمارهٔ  ۲۷۸
---
# شمارهٔ  ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>با وجود آن که پیوند آن پری از من برید</p></div>
<div class="m2"><p>گر ز مهرش سر کشم باید سرم از تن برید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من نخواهم داشت دست از حلقهٔ فتراک او</p></div>
<div class="m2"><p>گر سرم خواهد به جور آن ترک صید افکن برید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من به مهرش جان ندادم خاصه در ایام هجر</p></div>
<div class="m2"><p>گر برم نام وفا باید زبان من برید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلعت عشاق را می‌داد خیاط ازل</p></div>
<div class="m2"><p>بر تن من خلعت از خاکستر گلخن برید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در رهش افروخت اقبال از گیاه تر چراغ</p></div>
<div class="m2"><p>در شب تار آن که راه وادی ایمن برید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی بریدی متصل از دوستدار خویش دست</p></div>
<div class="m2"><p>گر توانستی زبان طعنهٔ دشمن برید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم را از غم خود دید گریان پیش او</p></div>
<div class="m2"><p>گفت می‌باید ازین رسوای تر دامن برید</p></div></div>