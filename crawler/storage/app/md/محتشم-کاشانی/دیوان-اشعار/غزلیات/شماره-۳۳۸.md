---
title: >-
    شمارهٔ  ۳۳۸
---
# شمارهٔ  ۳۳۸

<div class="b" id="bn1"><div class="m1"><p>آن شاه حسن بین و به تمکین نشستنش</p></div>
<div class="m2"><p>و آن خیرگی و طرف کله برشکستنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن تیر غمزه پرکش و از منتظر کشی است</p></div>
<div class="m2"><p>موقوف صد کمان ز کمانخانه جستنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سروی است در برم که براندام نازنین</p></div>
<div class="m2"><p>ماند نشان ز بند قبا چست بستنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر رشتهٔ رضا به دل غیر بسته یار</p></div>
<div class="m2"><p>اما چنان نبسته که به توان گسستنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باشد کمینه بازی آن طفل بر دلم</p></div>
<div class="m2"><p>بر همزدن دو چشم و به صد نیش خستنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صیدیست محتشم که به قیدی فتاده لیک</p></div>
<div class="m2"><p>مرگیست بی‌تکلف از آن قید رستنش</p></div></div>