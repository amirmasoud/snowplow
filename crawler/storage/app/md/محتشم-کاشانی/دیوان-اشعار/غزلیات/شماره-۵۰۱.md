---
title: >-
    شمارهٔ  ۵۰۱
---
# شمارهٔ  ۵۰۱

<div class="b" id="bn1"><div class="m1"><p>بر رخ به قصد دل منه زلف دو تا را بیش از این</p></div>
<div class="m2"><p>در کشور خود سر مده خیل بلا را بیش از این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد ره شکست ای رشک مه حسنت دل و دین را سپه</p></div>
<div class="m2"><p>برطرف مه طرف کله مشکن خدا را بیش از این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل کرده ساز ای نوش لب در وعده قانونی عجب</p></div>
<div class="m2"><p>گر داری آهنگ طرب بنواز ما را بیش از این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخل ترت در پیرهن چون نیکشر شد پرشکن</p></div>
<div class="m2"><p>محکم مبند ای سیمتن بند قبا را بیش از این</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میدان ظلم از اشک ما شد جای لغزشهای پا</p></div>
<div class="m2"><p>جولان مده بهر خدا رخش جفا را بیش از این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دل که می‌آمد روان تیرش ز قدرت بر نشان</p></div>
<div class="m2"><p>ترسم نداری در کمان تیر دعا را بیش از این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرسان ز حال محتشم هستی ولی بسیار کم</p></div>
<div class="m2"><p>پرسند ارباب کرم حال گدا را بیش از این</p></div></div>