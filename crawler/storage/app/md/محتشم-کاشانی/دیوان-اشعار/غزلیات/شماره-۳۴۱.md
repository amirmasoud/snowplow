---
title: >-
    شمارهٔ  ۳۴۱
---
# شمارهٔ  ۳۴۱

<div class="b" id="bn1"><div class="m1"><p>سحر به کوچه بیگانه‌ای فتادم دوش</p></div>
<div class="m2"><p>فتاد ناگهم آواز آشنا در گوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که خوش به بانگ بلند از خواص می می‌خواست</p></div>
<div class="m2"><p>ازو دهاده و از اهل بزم نوشانوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من حزین تن و سر گوش گشته و رفته</p></div>
<div class="m2"><p>ز پا تحرک و از تن توان و از دل هوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ستادم آن قدر آن جا که داد مرغ سحر</p></div>
<div class="m2"><p>هزار مرتبه داد خروش و گشت خموش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صباح سر زده آن کو صبوح کرده بتی</p></div>
<div class="m2"><p>گران خرام و سرانداز و بیخود و مدهوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرفته بهر وی از پاس واقفان سر راه</p></div>
<div class="m2"><p>نموده تکیه‌گهش نیز محرمان سر و دوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو پیش رفتم خود را زدم در آن آتش</p></div>
<div class="m2"><p>که بود آن که ازو دیگ سینه میزد جوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بی شعوریم اول اگر ز جا نشناخت</p></div>
<div class="m2"><p>شناخت عاقبت اما ز طرز راه و خروش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان به تنگ من از سرخوشی درآمد تنگ</p></div>
<div class="m2"><p>که گوئی آمده تنگم گرفته در آغوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگرچه جای هزار اعتراض بود آن جا</p></div>
<div class="m2"><p>بر آن قدح کش بی‌قید کیش عشرت کوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نگفت محتشم از اقتضای وقت جز این</p></div>
<div class="m2"><p>که می ز بزم رود خود به کوی باده فروش</p></div></div>