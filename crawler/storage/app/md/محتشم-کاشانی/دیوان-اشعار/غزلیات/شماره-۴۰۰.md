---
title: >-
    شمارهٔ  ۴۰۰
---
# شمارهٔ  ۴۰۰

<div class="b" id="bn1"><div class="m1"><p>زخم نگهت نهفته خوردم</p></div>
<div class="m2"><p>پنهان نگهی دگر که مردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد عقل و زمان مستی آمد</p></div>
<div class="m2"><p>خود را به تو این زمان سپردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیر نگهم زدی چو پنهان</p></div>
<div class="m2"><p>راهی به نوازش تو بردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌گشت لبم خضاب اگر دوش</p></div>
<div class="m2"><p>دامن گه گریه می‌فشردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از زخم اجل کشنده‌تر بود</p></div>
<div class="m2"><p>از دست تو ضربتی که خوردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل بی‌تو شبی که داغ می‌سوخت</p></div>
<div class="m2"><p>تا صبح ستاره می‌شمردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای هم دم محتشم در این بزم</p></div>
<div class="m2"><p>صاف از تو که من حریف دردم</p></div></div>