---
title: >-
    شمارهٔ  ۵۸
---
# شمارهٔ  ۵۸

<div class="b" id="bn1"><div class="m1"><p>این چه چوگان سر زلف و چه گوی ذقن است</p></div>
<div class="m2"><p>این چه ترکانه قباپوشی و لطف بدن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چه ابروست که پیوسته اشارت فرماست</p></div>
<div class="m2"><p>وین چه چشمست که با اهل نظر در سخنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این چه خالست که قیمت شکن مشک ختاست</p></div>
<div class="m2"><p>وین چه جعد است که صد تعبیه‌اش در شکنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این چه رخشنده عذار است که از پرتو آن</p></div>
<div class="m2"><p>آه انجم شررم شمع هزار انجمن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این چه غمزه است که چشم تو ز بی‌باکی او</p></div>
<div class="m2"><p>مست و خنجر کش و عاشق کش مردم فکنست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وای برجان اسیران تو گر دریابند</p></div>
<div class="m2"><p>از نگه کردنت آن شیوه که مخصوص منست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم تا بودت جان مشو از دوست جدا</p></div>
<div class="m2"><p>کاین جدائی سبب تفرقهٔ جان و تن است</p></div></div>