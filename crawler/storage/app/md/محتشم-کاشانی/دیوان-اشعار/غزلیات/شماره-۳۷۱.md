---
title: >-
    شمارهٔ  ۳۷۱
---
# شمارهٔ  ۳۷۱

<div class="b" id="bn1"><div class="m1"><p>بیچاره باشد همواره عاشق</p></div>
<div class="m2"><p>عشق این چنین است بیچاره عاشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردون نگردد روزی که گردد</p></div>
<div class="m2"><p>از کوی معشوق آواره عاشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد پاره شد دل اما همان هست</p></div>
<div class="m2"><p>بر روی خوبان هر پاره عاشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر سر کشیدی یکباره معشوق</p></div>
<div class="m2"><p>از پا فتادی صد باره عاشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر شرم بودی هرگز نکردی</p></div>
<div class="m2"><p>در روی معشوق نظاره عاشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبود گر آدم ای ترک خونخوار</p></div>
<div class="m2"><p>خواهی تراشید از خارهٔ عاشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسنت فزون باد تا محتشم را</p></div>
<div class="m2"><p>بینند یاران همواره عاشق</p></div></div>