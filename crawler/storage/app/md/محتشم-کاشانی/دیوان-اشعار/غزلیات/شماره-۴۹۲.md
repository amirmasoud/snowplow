---
title: >-
    شمارهٔ  ۴۹۲
---
# شمارهٔ  ۴۹۲

<div class="b" id="bn1"><div class="m1"><p>با وجود وصل شد زندان حرمان جای من</p></div>
<div class="m2"><p>برکنار آب حیوان تشنهٔ مردم وای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باغبان کاندر درون بر دست گلچین گل نزد</p></div>
<div class="m2"><p>دست منعش در برون صد تیشه زد بر پای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سایه بر هر کس فکند الا من دوزخ نصیب</p></div>
<div class="m2"><p>سر و طوبی قد گل روی بهشت آرای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست باقی رشحه‌ای از وصل و جان من کباب</p></div>
<div class="m2"><p>من که امروز این چنینم وای بر فردای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پر گیاه حسرتی خواهد دمانیدن ز خاک</p></div>
<div class="m2"><p>در پی این کاروان اشگ جهان پیمای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تفقدهای عامم نیز کردی ناامید</p></div>
<div class="m2"><p>بیش ازین بود از تو امید دل شیدای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم افغان که مستغنی است از یاد گدا</p></div>
<div class="m2"><p>پادشاه بی‌غم و سلطان بی‌پروای من</p></div></div>