---
title: >-
    شمارهٔ  ۳۲۶
---
# شمارهٔ  ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>خموشیت گره افکند در دل همه کس</p></div>
<div class="m2"><p>بگو حدیثی و بگشای مشکل همه کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان که هر نظری قابل جمال تو نیست</p></div>
<div class="m2"><p>مکن چو آینه خود را مقابل همه کس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخی که بال ملک را خطر ز شعلهٔ اوست</p></div>
<div class="m2"><p>روا بود که شود شمع محفل همه کس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عداوتم به دل کاینات داده قرار</p></div>
<div class="m2"><p>محبتی که سرشتست در دل همه کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمانه گشت پرآشوب و من به این خوش دل</p></div>
<div class="m2"><p>که از خیال تو خالی شود دل همه کس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زرشک مایل مرگم که از غلط کاریست</p></div>
<div class="m2"><p>به غیر محتشم آن سرو مایل همه کس</p></div></div>