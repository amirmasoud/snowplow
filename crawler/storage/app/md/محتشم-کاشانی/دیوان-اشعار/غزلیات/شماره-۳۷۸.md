---
title: >-
    شمارهٔ  ۳۷۸
---
# شمارهٔ  ۳۷۸

<div class="b" id="bn1"><div class="m1"><p>ما که می‌سازیم خود را در فراق او هلاک</p></div>
<div class="m2"><p>از وفای او به جان‌یم از برای او هلاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لطف او در رنگ استغنا و بر من عکس غیر</p></div>
<div class="m2"><p>از برای لطف استغنا نمای او هلاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من که تنگ آوردنش در بر تصور کرده‌ام</p></div>
<div class="m2"><p>می‌شوم از رشگ تنگی قبای او هلاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بجنبد باد می‌میرم که از بی‌تابیم</p></div>
<div class="m2"><p>بهر جنبشهای زلف مشگسای او هلاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای فلک یک روز کامم از وفای او بده</p></div>
<div class="m2"><p>پیش از آن روزی که گردم از جفای او هلاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌نهد تا غمزه ناوک در کمان می‌سازدم</p></div>
<div class="m2"><p>اضطراب نرگس ناوک گشای او هلاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زخم دلخواهی که خورد از دست جانان محتشم</p></div>
<div class="m2"><p>مدعی از رشک خواهد شد به جای او هلاک</p></div></div>