---
title: >-
    شمارهٔ  ۲۵۹
---
# شمارهٔ  ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>پیش او نیک و بد عاشق اگر ظاهر شود</p></div>
<div class="m2"><p>مدت هجر من و وصل رقیب آخر شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوده ذاتی هم که چون یابد مجال گفتگوی</p></div>
<div class="m2"><p>یک حدیثی موجب آزار صد خاطر شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذره‌ای قدرت ندارد خصم و می آزاردم</p></div>
<div class="m2"><p>وای گر مثل تو برآزار من قادر شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچه از ما گفت در غیبت رقیب روسیه</p></div>
<div class="m2"><p>خود بر او خواهد شدن اکنون اگر حاضر شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی حدیثی میکنی باور نه سوگندی قبول</p></div>
<div class="m2"><p>جای آن دارد که از دستت کسی کافر شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد زبان گر با شدم چون بید گویم شکر تو</p></div>
<div class="m2"><p>بند بندم کن خلاف آن اگر ظاهر شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم پیشش بافسون غیر جای خود گرفت</p></div>
<div class="m2"><p>لیک کار من نخواهد کرد اگر ساحر شود</p></div></div>