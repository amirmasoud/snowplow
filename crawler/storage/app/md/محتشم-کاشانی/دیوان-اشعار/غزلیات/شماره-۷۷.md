---
title: >-
    شمارهٔ  ۷۷
---
# شمارهٔ  ۷۷

<div class="b" id="bn1"><div class="m1"><p>بر درت کانجا سیاست مانع از داد من است</p></div>
<div class="m2"><p>آن که بی‌زنجیر در بند است فریاد من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن که می‌گردد مدام از دور باش خشم و کین</p></div>
<div class="m2"><p>دور دور از بارگاه خاطرت یاد من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای خوش آن مشکل که چون خسرو نداند حل آن</p></div>
<div class="m2"><p>طبع شیرین بشکفد کاین کار فرهاد من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دادن از روی زمین خاک بنی‌آدم به باد</p></div>
<div class="m2"><p>کمترین بازیچهٔ طفل پریزاد من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جهان خاکی که هرگز ترنگردد جز با اشک</p></div>
<div class="m2"><p>گر نشان جویند ازان خاک غم آباد من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن که پای مرغ دل می‌بندد از روی هوا</p></div>
<div class="m2"><p>طبع سحرانگیز وحشی بند صیاد من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انس آن بد الفت پیمان گسل با محتشم</p></div>
<div class="m2"><p>همچو پیوند طرب با جان ناشاد من است</p></div></div>