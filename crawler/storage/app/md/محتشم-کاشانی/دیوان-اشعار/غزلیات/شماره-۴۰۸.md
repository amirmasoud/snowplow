---
title: >-
    شمارهٔ  ۴۰۸
---
# شمارهٔ  ۴۰۸

<div class="b" id="bn1"><div class="m1"><p>ز لطف و قهر او و در خنده های گریه آلودم</p></div>
<div class="m2"><p>نمی‌یابم که مقبولم نمی‌دانم که مردودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جرمم در گذر یا بسملم کن به کی داری</p></div>
<div class="m2"><p>در آب و آتش از امید بود و بیم نابودم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یک تقصیر در مجلس به گرد خجلت آلودی</p></div>
<div class="m2"><p>رخی را کز وفا عمری به خاک درگهت سودم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گفتار غرض گو ناامیدم ساختی از خود</p></div>
<div class="m2"><p>بلی مقصود من این بود دیگر نیست مقصودم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه اندیشم دگر از گرمی بازار بدگویان</p></div>
<div class="m2"><p>که نه فکر زیان ماندست نه اندیشه سودم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شمعم گر تو برداری سر از تن در حقیقت به</p></div>
<div class="m2"><p>که چون مجمر نهد غیری به سر تاج زراندودم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به قول ناکسانم بیش ازین مانع مشو زین در</p></div>
<div class="m2"><p>که در خیل سگانت پیش ازین من هم کسی بودم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چون محتشم صدبارم اندازی در آتش هم</p></div>
<div class="m2"><p>چنان سوزم که جز بوی وفایت ناید از دودم</p></div></div>