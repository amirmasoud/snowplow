---
title: >-
    شمارهٔ  ۶۶
---
# شمارهٔ  ۶۶

<div class="b" id="bn1"><div class="m1"><p>چون دم جان دادنم آهی ز جانان برنخاست</p></div>
<div class="m2"><p>آهی از من سر نزد کز مردم افغان برنخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریه طوفان خیز گشت و از سرم برخاست دود</p></div>
<div class="m2"><p>باری از من گریه کم سرزد که طوفان برنخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه شور شهسواران بود در میدان حسن</p></div>
<div class="m2"><p>عرصه تاز آن مه نشد گردی ز میدان برنخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست و تیغ آن قبا گلگون نشد هرگز بلند</p></div>
<div class="m2"><p>بر سر غیری که ما را شعله از جان برنخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌رسد او را اگر جولان کند بر آفتاب</p></div>
<div class="m2"><p>کز زمین چون او سواری گرم جولان برنخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناوکی ننشست ازو بر سینهٔ پر آتشم</p></div>
<div class="m2"><p>کاتشم یک نیزه از چاک گریبان برنخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشت در کوی رقیبم یار و کس مانع نشد</p></div>
<div class="m2"><p>یک مسلمان محتشم زان کافرستان برنخاست</p></div></div>