---
title: >-
    شمارهٔ  ۳۴۲
---
# شمارهٔ  ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>آهوی او که بود بیشه دل صیدگهش</p></div>
<div class="m2"><p>می‌گدازد جگر شیر ز طرز نگهش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بدآموزی آن غمزه نمی‌گردد سیر</p></div>
<div class="m2"><p>ناز کافتاده به دنبالهٔ چشم سیهش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو جهان گشته به حسنی که اکر در عرصات</p></div>
<div class="m2"><p>به همان حسن درآید گذرند از گنهش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مه جبینی ز زمین خاسته کز قوت حسن</p></div>
<div class="m2"><p>پنجه در پنجهٔ خورشید فکند است مهش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وای بر ملک دل و دین که شد آخر ز بتان</p></div>
<div class="m2"><p>نامسلمان پسری فتنه‌گری پادشهش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چکند گر نکند خانهٔ مردم ویران</p></div>
<div class="m2"><p>پادشاهی که به جز فتنه نباشد سپهش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم در گذر آن چشم که من دیدم دوش</p></div>
<div class="m2"><p>جبرئیل ار گذرد می‌زند از غمزه رهش</p></div></div>