---
title: >-
    شمارهٔ  ۷
---
# شمارهٔ  ۷

<div class="b" id="bn1"><div class="m1"><p>که زد بر یاری ما چشم زخمی اینچنین یارا</p></div>
<div class="m2"><p>که روزی شد پس از وصل چنان هجر چنین ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو خود رفتی ولی باد جنون خواهد دواند از پی</p></div>
<div class="m2"><p>بسان شعلهٔ آتش من مجنون رسوا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو خود رو در سفر کردی ولی صحرا سپر کردی</p></div>
<div class="m2"><p>به صد شیدائی مجنون من مجنون شیدا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرس آهسته ران کاندر پیت از پویه فرسوده</p></div>
<div class="m2"><p>قدمها تا به زانو گمرهان دشت پیما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب تاریک و گمراهان ز دنبال تو سر گردان</p></div>
<div class="m2"><p>برون ار از سحاب برقع آن روی مه آسا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خطر گاهیست گرد خرگهت از شیشهای دل</p></div>
<div class="m2"><p>خدا را بر زمین ای مست ناز آهسته نه پا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو میرد محتشم دور از قدت باری چو باز آئی</p></div>
<div class="m2"><p>به خاکش گه گهی کن سایه گستر نخل بالا را</p></div></div>