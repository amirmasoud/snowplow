---
title: >-
    شمارهٔ  ۳۸۰
---
# شمارهٔ  ۳۸۰

<div class="b" id="bn1"><div class="m1"><p>مژده ای صبر که شد هجرت هجران نزدیک</p></div>
<div class="m2"><p>یوسف مصر وفا گشت به کنعان نزدیک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم غمین از خبر فرقت دوری شد و گشت</p></div>
<div class="m2"><p>دوری فرقت و محرومی حرمان نزدیک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشت سررشته بعد من از آن در کوتاه</p></div>
<div class="m2"><p>شد ره مور به درگاه سلیمان نزدیک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرد عیسی ز فلک مرحله چند نزول</p></div>
<div class="m2"><p>درد این خاک نشین گشت به درمان نزدیک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوی خیر آید ازین وضع که یک مرتبه شد</p></div>
<div class="m2"><p>کوی درویش به نزهت گه سلطان نزدیک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قرب آن سرو سمن پیرهن از شوق مرا</p></div>
<div class="m2"><p>چاک پیراهن جان ساخت به جانان نزدیک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم گرچه نشد قطع ره هجر تمام</p></div>
<div class="m2"><p>حالیا راه طلب گشت به جانان نزدیک</p></div></div>