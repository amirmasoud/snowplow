---
title: >-
    شمارهٔ  ۴۱۱
---
# شمارهٔ  ۴۱۱

<div class="b" id="bn1"><div class="m1"><p>به هجران کرده بودم خو که ناگه روی او دیدم</p></div>
<div class="m2"><p>کمند عقل بگسستم ز نو دیوانه گردیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفتم پنبهٔ آسایش از داغ جنون یعنی</p></div>
<div class="m2"><p>به باغ عاشقی از سر گل دیوانگی چیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم زان آفت جان بود فارغ‌وز بلا ایمن</p></div>
<div class="m2"><p>ز آفت دوستی باز آن بلا برخود پسندیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز راه عشق بر می‌گشتم آن رعنا دچارم شد</p></div>
<div class="m2"><p>ازان راهی که می‌رفتم پشیمان بازگردیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هنوزم با نهال قامتش باقیست پیوندی</p></div>
<div class="m2"><p>که هرجا دیدم او را جلوه‌گر چون بید لرزیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان ترسیده‌ام از غمزهٔ مردم شکار او</p></div>
<div class="m2"><p>که هرگاه آن پری در چشمم آمد چشم پوشیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن ره محتشم کان سروقد میرفت و من در پی</p></div>
<div class="m2"><p>زمین فرسوده شد از بس که بر وی چهره مالیدم</p></div></div>