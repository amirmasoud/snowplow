---
title: >-
    شمارهٔ  ۲۵۶
---
# شمارهٔ  ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>آن مه که صورتش ز مقابل نمی‌رود</p></div>
<div class="m2"><p>از دیده گرچه می‌رود از دل نمی‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زور کمند جذبه من بین که ناقه‌اش</p></div>
<div class="m2"><p>بسیار دست و پا زد و محمل نمی‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاضر کنید توسن او کز سرشک من</p></div>
<div class="m2"><p>ره پر گلست و ناقه درین گل نمی‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طور من آن یگانه نمی‌آورد به یاد</p></div>
<div class="m2"><p>تا با رفیق تو دو سه منزل نمی‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مجنون صفت رمیده ز شهرم دل آنچنان</p></div>
<div class="m2"><p>کش می‌کشند اگر به سلاسل نمی‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیغ اجل سزاست تن کاهل مرا</p></div>
<div class="m2"><p>کاندر قفای آن بت قاتل نمی‌رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بحر عشق محتشم از جان طمع ببر</p></div>
<div class="m2"><p>کاین زورق شکسته به ساحل نمی‌رود</p></div></div>