---
title: >-
    شمارهٔ  ۲۵۴
---
# شمارهٔ  ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>آن که اشگم از پیش منزل به منزل می‌رود</p></div>
<div class="m2"><p>وه که با من وعده می‌فرمود و با دل می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشگم از بی دست و پائی در پی این دل شکار</p></div>
<div class="m2"><p>بر زمین غلطان چو مرغ نیم بسمل می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال مستعجل وصالی چون بود کاندر وداع</p></div>
<div class="m2"><p>تا گشاید چشم تر بیند که محمل می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با وجود آن که ضبط گریه خود می‌کنم</p></div>
<div class="m2"><p>ناقه‌اش از اشک من تا سینه در گل می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوگلی کازارش از جنبیدن باد صباست</p></div>
<div class="m2"><p>آه کز آه من آزرده غافل می‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محتشم بهر نگاه آخرین در زیر تیغ</p></div>
<div class="m2"><p>می‌کند عجزی که خون از چشم قاتل می‌رود</p></div></div>