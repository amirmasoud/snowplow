---
title: >-
    شمارهٔ  ۵۱۵
---
# شمارهٔ  ۵۱۵

<div class="b" id="bn1"><div class="m1"><p>یارب آن مه را که خواهم زد قضا در کوی او</p></div>
<div class="m2"><p>آن قدر ذوق تماشا ده که بینم روی او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در قیامت کز زمین خیزند سربازان عشق</p></div>
<div class="m2"><p>صد قیامت بیش خیزد از زمین کوی او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فتنه‌ها برپا کند کز پا نشنید روز حشر</p></div>
<div class="m2"><p>در میان خلق محشر چشم عاشق جوی او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چین ابرویش ز درگه بیشتر نگذاردم</p></div>
<div class="m2"><p>شاه حسنش را همانا حاجبست ابروی او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌شود نسرینش از خشم نهانی ارغوان</p></div>
<div class="m2"><p>تا دگر بهر که آتش می‌فروزد خوی او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زخم ما ممتاز کی گردد اگر تیرش کند</p></div>
<div class="m2"><p>رخنه در هر دل به قدر قوت بازوی او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساکنان خلد بر اهل زمین حسرت برند</p></div>
<div class="m2"><p>گر برد باد زمین پیما به جنت بوی او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نرگس حاضرجوابش می‌دهد در ره جواب</p></div>
<div class="m2"><p>قاصدی را کز اشارت می‌فرستم سوی او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوش سازد محتشم چشم اشارت فهم را</p></div>
<div class="m2"><p>لب به جنبش چون درآرد چشم مضمون گوی او</p></div></div>