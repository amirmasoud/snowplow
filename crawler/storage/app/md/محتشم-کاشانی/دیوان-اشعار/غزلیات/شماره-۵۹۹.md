---
title: >-
    شمارهٔ  ۵۹۹
---
# شمارهٔ  ۵۹۹

<div class="b" id="bn1"><div class="m1"><p>دل خود رای مرا برده گل خودروئی</p></div>
<div class="m2"><p>ترک خنجر کش مردم کش آتش خوئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طفل نو سلسله‌ای شوخ تنگ حوصله‌ای</p></div>
<div class="m2"><p>شاه دیوانه و شی ماه مشوش موئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر و کارم به غزالیست کزاغیار مدام</p></div>
<div class="m2"><p>می‌کند روکش مردم به یک آدم روئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده پرنور شود نرگس نابینا را</p></div>
<div class="m2"><p>گر به گلشن رسد از پیرهن او بوئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوش بر بد سخنم کی منهی امروز ای گل</p></div>
<div class="m2"><p>خورده بر گوش تو گویا سخن بدگوئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند سویت نگرم عشوهٔ چشمی بنما</p></div>
<div class="m2"><p>عشوهٔ چشم نباشد گره ابروئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشوهٔ غلب شده بر محتشم آری چکند</p></div>
<div class="m2"><p>ناتوانی چنین خصم قوی بازوئی</p></div></div>