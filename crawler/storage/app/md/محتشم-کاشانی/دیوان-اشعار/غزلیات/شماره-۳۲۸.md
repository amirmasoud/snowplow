---
title: >-
    شمارهٔ  ۳۲۸
---
# شمارهٔ  ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>شبی که می‌فکند بی تو در دلم الم آتش</p></div>
<div class="m2"><p>ز آه من به فلک می‌رود علم علم آتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کباب کرده دل صد هزار لیلی و شیرین</p></div>
<div class="m2"><p>لبت که در عرب افکنده شور و در عجم آتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز جرم عشق اگر عاشقان روند به دوزخ</p></div>
<div class="m2"><p>شود به جانب من شعله‌کش ز صد قدم آتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سوز دل چو به او شرح حال خویش نویسم</p></div>
<div class="m2"><p>هزار بار فتد در زبانه قلم آتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چونی بهر که سرآورده‌ام دمی شب هجران</p></div>
<div class="m2"><p>درو فکنده‌ام از ناله‌های زیر و بم آتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یک پیاله که افروختنی چراغ رخت را</p></div>
<div class="m2"><p>فکندی ای گل رعنا به حال محتشم آتش</p></div></div>