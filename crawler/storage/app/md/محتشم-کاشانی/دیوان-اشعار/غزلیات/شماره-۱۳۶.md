---
title: >-
    شمارهٔ  ۱۳۶
---
# شمارهٔ  ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>بی‌پرده برآئی چو به صحرای قیامت</p></div>
<div class="m2"><p>خلد از هوس آید به تماشای قیامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هنگامه بگردد چو خورد غلغلهٔ تو</p></div>
<div class="m2"><p>بر معرکه معرکه آرای قیامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حشر گر آید نم رحمت ز کف تو</p></div>
<div class="m2"><p>روید همه شمشیر ز صحرای قیامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در قتل من امروز مبر خوف مکافات</p></div>
<div class="m2"><p>کاین داوری افتاد به فردای قیامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنشین و مجنبان لب عشاق که کم نیست</p></div>
<div class="m2"><p>غوغای قیام تو ز غوغای قیامت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پروردهٔ تفتندهٔ بیابان تمنا</p></div>
<div class="m2"><p>جنت شمرد دوزخ فردای قیامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرداست دوان محتشم از دست تو در حشر</p></div>
<div class="m2"><p>با صد تن عریان همه رسوای قیامت</p></div></div>