---
title: >-
    شمارهٔ  ۲۷۷
---
# شمارهٔ  ۲۷۷

<div class="b" id="bn1"><div class="m1"><p>به مرگ کوه کن کزوی المها یاد می‌آید</p></div>
<div class="m2"><p>هنوز از کوه تا دم میزنی فریاد می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همانا در کمال عشق نقصی بود مجنون را</p></div>
<div class="m2"><p>که نامش بر زبانها کمتر از فرهاد می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بد من گر به گوشت خوش نمی‌آید چه سراست این</p></div>
<div class="m2"><p>که بد گوی من از کوی تو دایم شاد می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه بیداد است این بنشین و رسوائی مکن کز تو</p></div>
<div class="m2"><p>اگر بیداد می‌آید ز من هم داد می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازین به فکر کارم کن که در دامت من آن صیدم</p></div>
<div class="m2"><p>که خود را می‌کنم آزاد تا صیاد می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سزای هرچه دی در بزم کردم امشبم دادی</p></div>
<div class="m2"><p>تو را چون یک یک از حالات مستی یاد می‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به منع مدعی زین بزم بی حاصل زبان مگشا</p></div>
<div class="m2"><p>که این کار از زبان خنجر جلاد می‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سگش صد دست و پا زد تا به آنکو برد با خویشم</p></div>
<div class="m2"><p>خوش آن یاری که از وی این قدر امداد می‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بیداد آید از وی محتشم دل را بشارت ده</p></div>
<div class="m2"><p>که خوبان را به دل رحمی پس از بیداد می‌آید</p></div></div>