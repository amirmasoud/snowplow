---
title: >-
    شمارهٔ  ۴۱۲
---
# شمارهٔ  ۴۱۲

<div class="b" id="bn1"><div class="m1"><p>با تو آن روز که شطرنج محبت چیدم</p></div>
<div class="m2"><p>ماتی خود ز تو در بازی اول دیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوسم رخ به رخ شاه خیال تو نشاند</p></div>
<div class="m2"><p>آن قدر کز رخ شرم تو خجل گردیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اسب جرات چو هوس تاخت به جولانگه عشق</p></div>
<div class="m2"><p>من رخ از عرصهٔ راحت طلبی تابیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>استخوان‌بندی شطرنج جهان کی شده بود</p></div>
<div class="m2"><p>صبح ابداع که من مهر تو می‌ورزیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هجر چون اسب حریفان مسافر زین کرد</p></div>
<div class="m2"><p>عرصه خالی شد از آشوب و من آرامیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن دلارام که منصوبه طرازی فن اوست</p></div>
<div class="m2"><p>بیدقی راند که صد بازی از آن فهمیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فکر خود کن تو هم ای دل که به تاراج بساط</p></div>
<div class="m2"><p>شاه عشق آمد و من خانهٔ خود برچیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محتشم از تو و از قدر تو افسوس که من</p></div>
<div class="m2"><p>پشه و پیل درین عرصه برابر دیدم</p></div></div>