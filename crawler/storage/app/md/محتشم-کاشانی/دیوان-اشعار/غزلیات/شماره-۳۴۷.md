---
title: >-
    شمارهٔ  ۳۴۷
---
# شمارهٔ  ۳۴۷

<div class="b" id="bn1"><div class="m1"><p>کاش مرگم سازد امشب از فغان کردن خلاص</p></div>
<div class="m2"><p>تا سگش از درد سر آسوده گردد من خلاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد گرفتاری ز حد بیرون اجل کو تا شود</p></div>
<div class="m2"><p>من ز دل فارغ دل از جان رسته جان از تن خلاص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داشتم در صید گاه صد زخم از بتان</p></div>
<div class="m2"><p>در نخستین ضربتم کرد آن شکارافکن خلاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوختم ز آهی که هست اندر دلم از تیر خویش</p></div>
<div class="m2"><p>روزنی کن تا شوم از دود این گلخن خلاص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی تو از هستی به جام مرغ روحم را بخوان</p></div>
<div class="m2"><p>از قفس تا گردد آن فرقت کش گلشن خلاص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محتشم در عاشقی بدنام شد پاکش بسوز</p></div>
<div class="m2"><p>تا شوی از ننگ آن رسوای تر دامن خلاص</p></div></div>