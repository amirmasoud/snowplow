---
title: >-
    شمارهٔ  ۲۲۸
---
# شمارهٔ  ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>عجب که دولت من بی‌بقائیی نکند</p></div>
<div class="m2"><p>بهانه جوی من از من جداییی نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دادخواه پرست آن گذر عجب کامروز</p></div>
<div class="m2"><p>برون نیاید و تیغ آزماییی نکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه دلخوشی بودم زان مسیح دم که مرا</p></div>
<div class="m2"><p>هلاک بیند و معجز نماییی نکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برش ادا نکنم مدعای خود هرگز</p></div>
<div class="m2"><p>که مدعی ز حسد بد اداییی نکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمان وصل حبیب از پی هلاک رقیب</p></div>
<div class="m2"><p>خوش است عمر اگر بی وفائیی نکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشان دهم به سگش غایبانه مردم را</p></div>
<div class="m2"><p>که با رقیب به سهو آشنائیی نکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین که گشته ز می ذوق بخش ساقی دور</p></div>
<div class="m2"><p>عجب که محتشم از وی گداییی نکند</p></div></div>