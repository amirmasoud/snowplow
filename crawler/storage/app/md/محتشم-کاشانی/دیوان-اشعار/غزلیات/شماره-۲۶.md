---
title: >-
    شمارهٔ  ۲۶
---
# شمارهٔ  ۲۶

<div class="b" id="bn1"><div class="m1"><p>مالک المک شوم چون ز جنون هامون را</p></div>
<div class="m2"><p>در روش غاشیه بردوش نهم مجنون را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نه آیینهٔ روی تو برابر باشد</p></div>
<div class="m2"><p>آه من تیره کند آینهٔ گردون را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تصرف نکند عشوهٔ خوبان در دل</p></div>
<div class="m2"><p>چه اثر عارض گلگون و قد موزون را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محمل لیلی از آن واسطه بستند بلند</p></div>
<div class="m2"><p>که به آن دست تصرف نرسد مجنون را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست چون حسن تو بر تختهٔ هستی رقمی</p></div>
<div class="m2"><p>این چه حسن است بنازم قلم بی‌چون را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن چنان تشنهٔ وصلم که کسی باشد اگر</p></div>
<div class="m2"><p>تشنهٔ آب به یکدم بکشد جیحون را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم پای به سختی مکش از وادی عشق</p></div>
<div class="m2"><p>گل این مرحله گیر آبلهٔ پر خون را</p></div></div>