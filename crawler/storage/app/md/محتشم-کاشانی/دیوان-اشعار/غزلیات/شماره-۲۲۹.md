---
title: >-
    شمارهٔ  ۲۲۹
---
# شمارهٔ  ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>لعل تو رد شکست من زمزمه بس نمی‌کند</p></div>
<div class="m2"><p>آن چه تو دوست میکنی دشمن کس نمی‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سخن حریف سوز آن چه تو آتشین زبان</p></div>
<div class="m2"><p>با من خسته میکنی شعله به خس نمی‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راحله از درت روان کردم و این دل طپان</p></div>
<div class="m2"><p>می‌کند امشب از فغان آن چه جرس نمی‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خم زلف بعد ازین جا منما به مرغ دل</p></div>
<div class="m2"><p>مرغ قفس شکن دگر میل قفس نمی‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ دلی که می‌جهد خاصه ز دام حیله‌ای</p></div>
<div class="m2"><p>دانه اگر ز در بود باز هوس نمی‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محتشم از کمند شد خسته چنان که چون توئی</p></div>
<div class="m2"><p>می‌رود از قفا و او روی به پس نمی‌کند</p></div></div>