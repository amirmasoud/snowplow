---
title: >-
    شمارهٔ  ۳۳۱
---
# شمارهٔ  ۳۳۱

<div class="b" id="bn1"><div class="m1"><p>رخش شمعی است دود آن کمند عنبر آلودش</p></div>
<div class="m2"><p>عجب شمعی که از بالا به پایان می‌رود دودش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دمی در بزم و صد ره می‌کشد از بیم و امیدم</p></div>
<div class="m2"><p>عتاب عشوه آمیز و خطاب خنده آلودش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان آب و آتش داردم دیوانه وش طفلی</p></div>
<div class="m2"><p>که در یک لحظه صد ره می‌شوم مقبول و مردودش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو گنجشگیست مرغ دل به دست طفل بی‌باکی</p></div>
<div class="m2"><p>که پیش من عزیزش دارد اما می‌کشد زودش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من زا لعبت پرستیها دل بازی‌خوری دارم</p></div>
<div class="m2"><p>که دارد کودکی با صد هزار آزار خشنودش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسی در تابم از مردم نوازیهای او با آن</p></div>
<div class="m2"><p>که می‌دانم به جز بی‌تابی من نیست مقصودش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طبیب محتشم در عشق پرکاریست کز قدرت</p></div>
<div class="m2"><p>به الماس جفا خوش می‌کند داغ نمک سودش</p></div></div>