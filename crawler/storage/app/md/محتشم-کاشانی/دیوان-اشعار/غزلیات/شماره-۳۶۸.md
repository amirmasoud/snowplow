---
title: >-
    شمارهٔ  ۳۶۸
---
# شمارهٔ  ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>بعد مرگ من نکرد آن مه تاسف برطرف</p></div>
<div class="m2"><p>می‌توان مرد از برای او تکلف برطرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نگردد سیر عاشق بر سر خوان وصال</p></div>
<div class="m2"><p>بود در منع زلیخا حق یوسف برطرف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاصه من کرده باغ وصل را اما در آن</p></div>
<div class="m2"><p>بر تماشا نیستم قادر تکلیف بطرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فیض من بنگر که چون رفتم به بزمش صد حجاب</p></div>
<div class="m2"><p>در میان آمد ولی شد بی توقف برطرف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند آری در میان تعریف بزم صوفیان</p></div>
<div class="m2"><p>باده صافی به دست آور تصرف بر طرف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخت ساعت ساعتم از وصل سازد کامیاب</p></div>
<div class="m2"><p>گر شود از وعدهای او تخلف برطرف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم مرد و ز تیغش مشکل خود حل نساخت</p></div>
<div class="m2"><p>تا ابد مشکل که گیرد زین تاسف برطرف</p></div></div>