---
title: >-
    شمارهٔ  ۲۷
---
# شمارهٔ  ۲۷

<div class="b" id="bn1"><div class="m1"><p>شوم هلاک چو غیری خورد خدنگ تو را</p></div>
<div class="m2"><p>که دانم آشتئی در قفاست جنگ تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که کرده پیش تو اظهار سوز ما امروز</p></div>
<div class="m2"><p>که آتش غضب افروخته است رنگ تو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مصوران قلم از مو کنند تا نکشند</p></div>
<div class="m2"><p>زیاده از سرموئی دهان تنگ تو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمان زمان کنم افزون جراحت تن خویش</p></div>
<div class="m2"><p>ز بس که بوسه زنم زخمهای سنگ تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جریده گرد من امشب گرت رفیقی نیست</p></div>
<div class="m2"><p>چه باعث است به ره دمبدم درنگ تو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مدعی پر و بالی مده که پروازش</p></div>
<div class="m2"><p>بباد بر دهد ای سرو نام و ننگ تو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز حرف پر دلی محتشم پرست جهان</p></div>
<div class="m2"><p>ز بس که جای به دل می‌دهد خدنگ تو را</p></div></div>