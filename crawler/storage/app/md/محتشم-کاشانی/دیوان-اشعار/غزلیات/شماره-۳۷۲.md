---
title: >-
    شمارهٔ  ۳۷۲
---
# شمارهٔ  ۳۷۲

<div class="b" id="bn1"><div class="m1"><p>ز تب نالان شدی جانان عاشق</p></div>
<div class="m2"><p>بلا گردان جانت جان عاشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سوز نالهٔ عاشق گدازت</p></div>
<div class="m2"><p>به گردون می‌رسد افغان عاشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تب گرم تو عالم را سیه کرد</p></div>
<div class="m2"><p>ز خود بر سینهٔ سوزان عاشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دمی صد بار از درد تو می‌مرد</p></div>
<div class="m2"><p>اجل می‌برد اگر فرمان عاشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بالینت دمی نبود که گرید</p></div>
<div class="m2"><p>نیالاید به خون دامان عاشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشی گر آهی از دل خیزد آتش</p></div>
<div class="m2"><p>ز جان عاشقان جانان عاشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جان محتشم نه درد خود را</p></div>
<div class="m2"><p>که باشد درد و محنت زان عاشق</p></div></div>