---
title: >-
    شمارهٔ  ۱۹۰
---
# شمارهٔ  ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>چو ممکن نیست کانمه پاسبان محفلم سازد</p></div>
<div class="m2"><p>بکوشم تا سگ دنباله گیر محملم سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از وی چون پرده افتد برملا از من کند رنجش</p></div>
<div class="m2"><p>که از همراهی خود با رقیبان غافلم سازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کندبر من بتیغ آن بت گنه ثابت که هر ساعت</p></div>
<div class="m2"><p>ز بیم جان بنا واقع گناهی قایلم سازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دل بس رازهای پرده گر سر بر زند روزی</p></div>
<div class="m2"><p>که دل فرسائی بار جفا نازک دلم سازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز فتانی به ایمائی کند واقف رقیبان را</p></div>
<div class="m2"><p>اجازت ده نگاهش چون به ابرو مایلم سازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خارج پیچشی‌ها در دمم باید شدن بیرون</p></div>
<div class="m2"><p>دمی از مصلحت در بزم خود گر داخلم سازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درونم محتشم زان مست کین خواهد شدن شادان</p></div>
<div class="m2"><p>ولی روزی که دور چرخ ساغر از گلم سازد</p></div></div>