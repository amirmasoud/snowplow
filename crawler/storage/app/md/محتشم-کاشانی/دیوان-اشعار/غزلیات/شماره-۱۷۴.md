---
title: >-
    شمارهٔ  ۱۷۴
---
# شمارهٔ  ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>طبیب من ز هجر خود مرارنجور می‌دارد</p></div>
<div class="m2"><p>مرا رنجور کرد از هجر و از خود دور می‌دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو عذری هست در تقصیر طاعت می پرستان را</p></div>
<div class="m2"><p>امام شهر گر دارد مرا معذور می‌دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به باطن گر ندارد زاهد خلوت نشین عیبی</p></div>
<div class="m2"><p>چرا در خرقهٔ خود را این چنین مستور می‌دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بینی صفائی در رخ زاهد مرو از ره</p></div>
<div class="m2"><p>که صادق نیست صبح کاذب اما نور می‌دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیه روزم ولی هستم پرستار آفتابی را</p></div>
<div class="m2"><p>که عالم را منور در شب دی جور میدارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طلب کن نشئه زان ساقی که بیمی چشم خوبان را</p></div>
<div class="m2"><p>به قدر هوش ما گه مست و گه مخمور میدارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس از یک مردمی گر میکنی صد جور پی‌درپی</p></div>
<div class="m2"><p>همان یک مردمی را محتشم منظور می‌دارد</p></div></div>