---
title: >-
    شمارهٔ  ۵۴۹
---
# شمارهٔ  ۵۴۹

<div class="b" id="bn1"><div class="m1"><p>دی باز جرعه نوش ز جام که بوده‌ای</p></div>
<div class="m2"><p>صد کام تلخ کرده به کام که بوده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنجا که بود بهر تو در خاک دامها</p></div>
<div class="m2"><p>دام که پاره کرده و رام که بوده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنجا که جسته‌اند تو را چون هلال عید</p></div>
<div class="m2"><p>برقع گشودهٔ ماه تمام که بوده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرگرمیت چو برده به کسب هوا برون</p></div>
<div class="m2"><p>خورشیدوار بر در و بام که بوده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای صد هزار صید دل آزاد کرده‌ات</p></div>
<div class="m2"><p>خود صیدوار بسته دام که بوده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب عارفانه ساقی بزم که گشته‌ای</p></div>
<div class="m2"><p>تا روز جرعه نوش ز جام که بوده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حالت شکفتگی از رغم محتشم</p></div>
<div class="m2"><p>حالت طلب ز طرز کلام که بوده‌ای</p></div></div>