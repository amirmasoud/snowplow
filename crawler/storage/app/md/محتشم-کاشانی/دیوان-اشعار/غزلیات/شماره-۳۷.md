---
title: >-
    شمارهٔ  ۳۷
---
# شمارهٔ  ۳۷

<div class="b" id="bn1"><div class="m1"><p>فرمود مرا سجدهٔ خویش آن بت رعنا</p></div>
<div class="m2"><p>در سجده فتادم که سمعنا واطعنا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما دخل به خود در می‌دیدار نگردیم</p></div>
<div class="m2"><p>ما حل له شارعنا فیه شرعنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بودیم ز ذرات به خورشید رخش نی</p></div>
<div class="m2"><p>الفرع رئینا والی الاصل رجعنا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزی که دل از عین تعلق به تو بستیم</p></div>
<div class="m2"><p>من غیرک یاقرة عینی و قطعنا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در زاریم از ضعف عمل پیش تو صد ره</p></div>
<div class="m2"><p>ضعف الفرغ الاکبر و یارب فزعنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دار شفایت مرضی دفع نکردیم</p></div>
<div class="m2"><p>لکن کسل الروح من الروح و قعنا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر محتشم از غم علم عین نگون کرد</p></div>
<div class="m2"><p>انا علم البهجة بالهم رفعنا</p></div></div>