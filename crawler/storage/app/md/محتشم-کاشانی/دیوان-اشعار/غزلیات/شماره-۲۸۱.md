---
title: >-
    شمارهٔ  ۲۸۱
---
# شمارهٔ  ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>سرو خرامان من طره پریشان رسید</p></div>
<div class="m2"><p>سلسلهٔ عشق را سلسله جنبان رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چاک به دامان رساند جیب شکیبم که باز</p></div>
<div class="m2"><p>سرو قباپوش من برزده دامان رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم زلیخای عشق باز شد از خواب خویش</p></div>
<div class="m2"><p>هودج یوسف نمود فتنه ز کنعان رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محمل لیلی حسن ناقه ز وادی رساند</p></div>
<div class="m2"><p>بر سر مجنون عشق شوق شتابان رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باره شیرین نهاد سر به ره بیستون</p></div>
<div class="m2"><p>کوه کن غصه را قصه به پایان رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد شهنشاه عشق بر در دل شد بلند</p></div>
<div class="m2"><p>کشور بی‌ضبط را مژدهٔ سلطان رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خانهٔ مردم نهاد رو به خرابی که باز</p></div>
<div class="m2"><p>دجلهٔ چشم مرا نوبت طوفان رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در نظر اولم اشک به دل شد به خون</p></div>
<div class="m2"><p>بس که به دل زخمها زان بت فتان رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن که ز خاصان او طاقت نازی نداشت</p></div>
<div class="m2"><p>از پی آزردنش کار به درمان رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر لب زخم دلم در نفس آخرین</p></div>
<div class="m2"><p>شکر که از دست دوست شربت پیکان رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان شکیبنده را صبر به جانان رساند</p></div>
<div class="m2"><p>محتشم خسته را درد به درمان رسید</p></div></div>