---
title: >-
    شمارهٔ  ۳۴
---
# شمارهٔ  ۳۴

<div class="b" id="bn1"><div class="m1"><p>تا همتم به دست طلب زد در بلا</p></div>
<div class="m2"><p>دربست شد مسخر من کشور بلا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست قضا به مژده کلاه از سرم ربود</p></div>
<div class="m2"><p>چون می‌نهاد بر سر من افسر بلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن دم هنوز قلعه مهدم حصار بود</p></div>
<div class="m2"><p>کاورد عشق بر سر من لشکر بلا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر کوهکن ز رتبه مقدم نوشته‌اند</p></div>
<div class="m2"><p>نام بلا کشان تو در دفتر بلا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بنده بود بی‌تو به داغ جنون اسیر</p></div>
<div class="m2"><p>تابنده بود بر سر او افسر بلا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا هست کاکل تو بلاجو عجب اگر</p></div>
<div class="m2"><p>کاهد زمانه یک سر مو از سر بلا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردیست مرد عشق که دایم چو محتشم</p></div>
<div class="m2"><p>در یوزه مراد کند از در بلا</p></div></div>