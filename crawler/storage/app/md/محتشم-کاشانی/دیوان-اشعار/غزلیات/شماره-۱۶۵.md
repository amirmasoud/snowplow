---
title: >-
    شمارهٔ  ۱۶۵
---
# شمارهٔ  ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>نخواهم از جمال عالم آشوبت نقاب افتد</p></div>
<div class="m2"><p>که من دیوانه گردم بازو خلقی در عذاب افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بس لطف من و اندام زیبایت عجب دارم</p></div>
<div class="m2"><p>که دیبا گر بپوشی سایه‌ات بر آفتاب افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر در خواب بینم پیرهن را بر تنت پیچان</p></div>
<div class="m2"><p>تنم از رشگ آن بر بستر اندر پیچ و تاب افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غنود آن نرگس و شد بر طرف غوغا ز هر گوشه</p></div>
<div class="m2"><p>ز بد مستی که بزم آراید و ناگه به خواب افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چسان پنهان کنم از همنشینان مهر مه‌روئی</p></div>
<div class="m2"><p>که چون نامش برآید جان من در اضطراب افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز هجر افتادم از دریوزه وصلش چو گمراهی</p></div>
<div class="m2"><p>که جوید آب و با چندین مشقت در سراب افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندارد محتشم تاب نظر هنگام لطف او</p></div>
<div class="m2"><p>معاذالله اگر بر من نگاهش از عتاب افتد</p></div></div>