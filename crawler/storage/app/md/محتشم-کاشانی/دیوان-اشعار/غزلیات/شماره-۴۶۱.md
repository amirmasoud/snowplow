---
title: >-
    شمارهٔ  ۴۶۱
---
# شمارهٔ  ۴۶۱

<div class="b" id="bn1"><div class="m1"><p>تا به کی جان کسی دل بری از هیچ کسان</p></div>
<div class="m2"><p>آفت حسن بتان است هجوم مگسان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو ز خود غافلی ای شمع ملک پروانه</p></div>
<div class="m2"><p>که چو گل هر نفسی میزنی آتش به کسان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زده آتش به جهان حسن تو وز بیم نفس</p></div>
<div class="m2"><p>تا شود روی تو آئینهٔ آتش نفسان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشور حسن بیک تاخت بگیری چو شوند</p></div>
<div class="m2"><p>هم رهان ره سودای تو باری فرسان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حریم حرمت پای سگانست دراز</p></div>
<div class="m2"><p>وز سر کوی تو شیران همه کوته مرسان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رزق شاهنشهی حسن چه داند صنمی</p></div>
<div class="m2"><p>که سجود در او سرزند از بوالهوسان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندگیها کندت محتشم بی کس اگر</p></div>
<div class="m2"><p>مکنی نسبتش از بنده شناسی به کسان</p></div></div>