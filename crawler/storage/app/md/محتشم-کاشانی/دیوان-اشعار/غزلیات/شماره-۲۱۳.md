---
title: >-
    شمارهٔ  ۲۱۳
---
# شمارهٔ  ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>بهترین طاقی که زیر طاق گردون بسته‌اند</p></div>
<div class="m2"><p>بر فراز منظر آن چشم میگون بسته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیرتی دارم که بنایان شیرین کار صنع</p></div>
<div class="m2"><p>بیستون طاق دو ابروی تو را چون بسته‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ازل تا حال گوئی نخل بندان قدت</p></div>
<div class="m2"><p>کرده‌اند انگیز تا این نخل موزون بسته‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جذبهٔ دل برده شیرین را به کوه بیستون</p></div>
<div class="m2"><p>مردم ظاهر نگر تهمت به گلگون بسته‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سگان لیلیم حیران که در اطراف حی</p></div>
<div class="m2"><p>با وجود آشنائی راه مجنون بسته‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مژده مجنون را که امشب محرمان بر راحله</p></div>
<div class="m2"><p>محمل لیلی به قصد سیر هامون بسته‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرده‌اند از وعدهٔ وصل آن دو لعل دلگشا</p></div>
<div class="m2"><p>پرنمک در کار تا از زخم ما خون بسته‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیر این خون بسته مژگان مردم چشم ترم</p></div>
<div class="m2"><p>از خس و خاشاک پل بر روی جیحون بسته‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حاجیان خلوت دل با خیال او مرا</p></div>
<div class="m2"><p>دردرون جا داده‌اند و در ز بیرون بسته‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترک خدمت چو نتوان کین بنده پرور خسروان</p></div>
<div class="m2"><p>پای ما درپایهٔ چتر همایون بسته‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا ز محرومی به خوابش هم نبینم محتشم</p></div>
<div class="m2"><p>خواب بر چشمم دو چشم او به افسون بسته‌اند</p></div></div>