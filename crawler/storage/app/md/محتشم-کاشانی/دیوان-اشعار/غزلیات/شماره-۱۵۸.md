---
title: >-
    شمارهٔ  ۱۵۸
---
# شمارهٔ  ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>زندگانی بی غم عشق بتان یکدم مباد</p></div>
<div class="m2"><p>هر که این عالم ندارد زنده در عالم مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باد عمرم آن قدر کز شاخ وصلت برخورم</p></div>
<div class="m2"><p>ور نمی‌خواهی تو بر خورداریم آن هم مباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌خدنگت یاددارم صد جراحت بر جگر</p></div>
<div class="m2"><p>هیچ کس را این جراحتهای بی‌مرهم مباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ز حرمانش ندارم زندگی بر خود حرام</p></div>
<div class="m2"><p>مرغ روحم در حریم حرمتش محرم مباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز وصل دلبران گر شد نصیب دیگران</p></div>
<div class="m2"><p>سایهٔ شبهای هجرت از سرما کم مباد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش کز درد عشقت غم ندارم در جهان</p></div>
<div class="m2"><p>گفت هر عاشق که دردی دارد او را غم مباد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نباشد محتشم خوش‌دل به دور خط دوست</p></div>
<div class="m2"><p>از بهار حسن او مرغ دلم خرم مباد</p></div></div>