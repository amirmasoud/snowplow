---
title: >-
    شمارهٔ  ۵۷۷
---
# شمارهٔ  ۵۷۷

<div class="b" id="bn1"><div class="m1"><p>از باده عیشم بود مستانه به کف جامی</p></div>
<div class="m2"><p>زد ساغر من بر سنگ دیوانه می‌آشامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای هم دم از افسانه یک لحظه به خوابش کن</p></div>
<div class="m2"><p>شاید که جهان گیرد یک مرتبه آرامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با این همه زهدای بت در عشق تو نزدیکست</p></div>
<div class="m2"><p>کز مستی و بدنامی بر خویش نهم نامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر کار تو در پرهیز پر پیش نمی‌آید</p></div>
<div class="m2"><p>در وادی رسوائی من پیش نهم گامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بسته زبان از خشم خود گو که نمی‌باید</p></div>
<div class="m2"><p>با این همه تلخی‌ها شیریی دشنامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن کرد گرفتارم کز زلف بتان افکند</p></div>
<div class="m2"><p>در راه بنی آدم گیرنده ترین دامی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با این همه چالاکی ای پیک صبا تا چند</p></div>
<div class="m2"><p>جانی به لب آوردن ز آوردن پیغامی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هنگامه به آن کو بر ای دیو جنون شاید</p></div>
<div class="m2"><p>کان شوخ تماشا دوست سر برکند از بامی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فردا چه شود یارب کان شوخ به بزم آمد</p></div>
<div class="m2"><p>دیروز به ایمائی امروز به ابرامی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای سرو چمن مفروش پر ناز که می‌باید</p></div>
<div class="m2"><p>رعنائی بالا را زیبائی اندامی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در بزم تو این بد نام جان داد و نداد ایام</p></div>
<div class="m2"><p>از دست تواش جامی وز لعل تواش کامی</p></div></div>