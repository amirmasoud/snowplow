---
title: >-
    شمارهٔ  ۳۹۹
---
# شمارهٔ  ۳۹۹

<div class="b" id="bn1"><div class="m1"><p>دی به دنبال یکی کبک خرام افتادم</p></div>
<div class="m2"><p>رفتم از شهر به صحرا و به دام افتادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر این باده همه داروی بیهوشی بود</p></div>
<div class="m2"><p>که من لجه‌کش از یک دو سه جام افتادم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن چه قد بود و چه قامت که ز نظارهٔ آن</p></div>
<div class="m2"><p>تا دم صبح قیامت ز قیام افتادم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به اشارت مگر احوال بگویم که چه شد</p></div>
<div class="m2"><p>که ز گویائی از آن طرز کلام افتادم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ زخمی نزد آن غمزه که کاری نفتاد</p></div>
<div class="m2"><p>من افتاده چگویم ز کدام افتادم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من که بودم ز مقیمان سر کوی حضور</p></div>
<div class="m2"><p>از کجا آه به این طرفه مقام افتادم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم بوی جنونم همه کس فاش شنید</p></div>
<div class="m2"><p>چون درین سلسله غالیهٔ فام افتادم</p></div></div>