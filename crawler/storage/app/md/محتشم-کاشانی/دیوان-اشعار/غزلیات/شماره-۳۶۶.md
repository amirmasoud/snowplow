---
title: >-
    شمارهٔ  ۳۶۶
---
# شمارهٔ  ۳۶۶

<div class="b" id="bn1"><div class="m1"><p>چو بر من زد آن ترک خون خوار تیغ</p></div>
<div class="m2"><p>شد از خون گرمم شرر بار تیغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شدم آن چنان کشته او به میل</p></div>
<div class="m2"><p>که از میل من شد خبردار تیغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه چابک‌تری از تو هست ای اجل</p></div>
<div class="m2"><p>باو سر فرو آر و بسپار تیغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه جائیست کوی تو کانجا مدام</p></div>
<div class="m2"><p>ز در سنگ بارد ز دیوار تیغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازین بزم اگر دفع من واجبست</p></div>
<div class="m2"><p>بنه ساغر از دست و بردار تیغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شود بر زبان تا وصیت تمام</p></div>
<div class="m2"><p>خدا را زمانی نگهدار تیغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شده چشم مست تو خنجر گذار</p></div>
<div class="m2"><p>تو در دست این مست مگذار تیغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بقا سر بجیب فنا در کشد</p></div>
<div class="m2"><p>اگر برکشد آن ستمکار تیغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سگ آن دلیرم که وقت غضب</p></div>
<div class="m2"><p>شود پیش او محتشم وار تیغ</p></div></div>