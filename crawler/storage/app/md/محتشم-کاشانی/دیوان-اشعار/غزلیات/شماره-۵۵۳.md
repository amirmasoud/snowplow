---
title: >-
    شمارهٔ  ۵۵۳
---
# شمارهٔ  ۵۵۳

<div class="b" id="bn1"><div class="m1"><p>اگر مقدار عشق پاک را دلدار دانستی</p></div>
<div class="m2"><p>مرا بسیار جستی قدر من بسیار دانستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبودی کوه کن در عشق اگر بی‌غیرتی چون من</p></div>
<div class="m2"><p>رقابت با هوسناکی چو خسرو عار دانستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به قدر درک و دانش مرد را مقدرا می‌دانند</p></div>
<div class="m2"><p>چه خوش بودی اگر یار من این مقدار دانستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تفاوت‌ها شدی در غیرت و بی‌غیرتی پیدا</p></div>
<div class="m2"><p>اگر آن بی‌تفاوت یار از اغیار دانستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیه‌چشمی که درخوابست از کید بداندیشان</p></div>
<div class="m2"><p>چه بودی قدر پاس دیدهٔ بیدار دانستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بت پر کار من کائین دل‌داری نمی‌داند</p></div>
<div class="m2"><p>نجستی یک دل از دستش اگر این کار دانستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگشتی شعلهٔ بازار رنجش یک نفس ساکن</p></div>
<div class="m2"><p>اگر ازار او را محتشم آزار دانستی</p></div></div>