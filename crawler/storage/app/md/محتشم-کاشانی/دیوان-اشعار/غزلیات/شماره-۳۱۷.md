---
title: >-
    شمارهٔ  ۳۱۷
---
# شمارهٔ  ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>ای در زمان خط تو بازار فتنه تیز</p></div>
<div class="m2"><p>انجام دور حسن تو آغاز رستخیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جولانی تو راست که جولان ز لعب تو</p></div>
<div class="m2"><p>صد رستخیز خاسته از هر نشست و خیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر روز می‌کند ز ره دعوی آفتاب</p></div>
<div class="m2"><p>کشتی حسن با تو قدر لیک در گریز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داده خواص نافه به ناف زمین هوا</p></div>
<div class="m2"><p>هرگه به جنبش آمده آن زلف مشگبیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانی که چیست دوستی و کوشش وصال</p></div>
<div class="m2"><p>با جان خود خصومت و با بخت خود ستیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تلخی صبر گفت ولی کرد آشکار</p></div>
<div class="m2"><p>عذری ز پی بجنبش لبهای شهد ریز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچند آتشش بود افسرده محتشم</p></div>
<div class="m2"><p>او تیز می‌کند به نگه‌های تیز تیز</p></div></div>