---
title: >-
    شمارهٔ  ۵۳۹
---
# شمارهٔ  ۵۳۹

<div class="b" id="bn1"><div class="m1"><p>خط اگرت سبزه طرف لاله نهفته</p></div>
<div class="m2"><p>دایرهٔ ماه را به هاله نهفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیخ که دامن‌کش از بتان شده ای گل</p></div>
<div class="m2"><p>داغ تو در آستین چو لاله نهفته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابر برای شکست شیشه غنچه</p></div>
<div class="m2"><p>در بغل لاله سنگ ژاله نهفته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌کنم از خوی نازکت شب هجران</p></div>
<div class="m2"><p>پیش خیال تو نیز ناله نهفته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تن که نه قربانی بتان شود اولی</p></div>
<div class="m2"><p>در دهن گور آن نواله نهفته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن چه خضر سالها شتافتش از پی</p></div>
<div class="m2"><p>در دو پیاله می دو ساله نهفته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش بناگوش او ز طره سیه پوش</p></div>
<div class="m2"><p>برگ گل و لاله در گلاله نهفته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نامهٔ قتلم نوشته فاش و به قاصد</p></div>
<div class="m2"><p>داده ز تاکید صد رساله نهفته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دید که می‌میرم از تغافل چشمش</p></div>
<div class="m2"><p>کرد نگاهی به من حواله نهفته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منع من ای شیخ کن ز مشرب خودرو</p></div>
<div class="m2"><p>سبحه مگردان عنان پیاله نهفته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شیردلی محتشم کجاست که خواند</p></div>
<div class="m2"><p>این غزل از من بر آن غزاله نهفته</p></div></div>