---
title: >-
    شمارهٔ  ۱۴۷
---
# شمارهٔ  ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>اغیار را به صحبت جانان چه احتیاج</p></div>
<div class="m2"><p>بی درد را به نعمت درمان چه احتیاج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در قتل من که ریخته جسمم ز هم مکوش</p></div>
<div class="m2"><p>کشتی چو شد شکسته به طوفان چه احتیاج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخل توام به سعی مربی ثمر مبخش</p></div>
<div class="m2"><p>خودرسته را به خدمت دهقان چه احتیاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی زنده دم تو کشد منت مسیح</p></div>
<div class="m2"><p>پاینده را به چشمهٔ حیوان چه احتیاج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لعبتان چین به خیال تو فارغیم</p></div>
<div class="m2"><p>تا جان بود به صورت بی‌جان چه احتیاج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعد طریق کعبهٔ مقصد ز قرب دل</p></div>
<div class="m2"><p>چون بسته شد به بستن پیمان چه احتیاج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر ثبوت عشق چو در بزم منکران</p></div>
<div class="m2"><p>دل چاک شد به چاک گریبان چه احتیاج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش ضمیر دلبر ما فی‌الضمیر دان</p></div>
<div class="m2"><p>اظهار کردن غم پنهان چه احتیاج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در فقر چون عزیزی و خواری مساویند</p></div>
<div class="m2"><p>درویش را به عزت سلطان چه احتیاج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون دیگریست قاضی حاجات محتشم</p></div>
<div class="m2"><p>مور ضعیف را به سلیمان چه احتیاج</p></div></div>