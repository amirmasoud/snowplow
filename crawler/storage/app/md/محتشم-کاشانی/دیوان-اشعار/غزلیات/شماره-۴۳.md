---
title: >-
    شمارهٔ  ۴۳
---
# شمارهٔ  ۴۳

<div class="b" id="bn1"><div class="m1"><p>برشکن طرف کله چون بفکنی از رخ نقاب</p></div>
<div class="m2"><p>صبح صادق کن عیان بعد از طلوع آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت امشب صبر کن چندان که در خواب آیمت</p></div>
<div class="m2"><p>صبر خواهم کرد من اما که خواهد کرد خواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سهل باشد ملک دل زیر و زبر زاشوب عشق</p></div>
<div class="m2"><p>ملک ایمان را نگهدارد خدا زین انقلاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دی که در من دیدن آن آفتاب آتش فکند</p></div>
<div class="m2"><p>دیده آبی زد بر آتش ورنه می‌گشتم کباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون عنان گیرم سواری را کز استیلای حسن</p></div>
<div class="m2"><p>می‌رود پیوسته صدا به رو کمانش در رکاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق اگر پاکست در انجام صحبت میشود</p></div>
<div class="m2"><p>رسم معشوقان نیاز آئین عشاقان عتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز من مظلوم کز عمر خودم بیزار کیست</p></div>
<div class="m2"><p>آن که آزارش گناه و کشتنش باشد ثواب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در میان بیم و امیدم که هر دم می‌کند</p></div>
<div class="m2"><p>مرگ در کارم تعلل زیاد در قتلم شتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دی سوال بوسه‌ای زان شوخ کردم گفت نیست</p></div>
<div class="m2"><p>محتشم حرف چنین راغیر خاموشی جواب</p></div></div>