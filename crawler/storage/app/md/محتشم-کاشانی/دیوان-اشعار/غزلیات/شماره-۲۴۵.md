---
title: >-
    شمارهٔ  ۲۴۵
---
# شمارهٔ  ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>دی ز شوخی بر من آن توسن دوانیدن چه بود</p></div>
<div class="m2"><p>نارسیده بر سر من باز گردیدن چه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تشنه‌ای را کز تمنا عاقبت میسوختی</p></div>
<div class="m2"><p>آب از بازیچه‌اش بر لب رسانیدن چه بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خسته‌ای را کز جفا می‌کردی آخر قصد جان</p></div>
<div class="m2"><p>در علاجش اول آن مقدار کوشیدن چه بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دلت نشکفته بود از گریهٔ پردرد من</p></div>
<div class="m2"><p>سر فرو بردن چو گل در جیب و خندیدن چه بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرنه مرگ من به کام دشمنان می‌خواستی</p></div>
<div class="m2"><p>بهر قتلم با رقیب آن مصلحت دیدن چه بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور نبودت ننگ و عار از کشتن من بعد قتل</p></div>
<div class="m2"><p>آن تاسف خوردن و انگشت خائیدن چه بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم ای گشته در عالم بدین داری علم</p></div>
<div class="m2"><p>بعد چندین ساله زهد این بت پرستیدن چه بود</p></div></div>