---
title: >-
    شمارهٔ  ۵۹۴
---
# شمارهٔ  ۵۹۴

<div class="b" id="bn1"><div class="m1"><p>مرا حرص نگه هردم به رغبت می‌برد جائی</p></div>
<div class="m2"><p>که هست آفت گمار از غمزه بر من چشم شهلائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیاد حور و فکر خلد اگر غافل زیم شاید</p></div>
<div class="m2"><p>که می‌بینم عجب روئی و می‌باشم عجب جائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی از عاشقان چشم مردم پرورش می‌شد</p></div>
<div class="m2"><p>اگر می‌بود نرگس را چو مردم چشم بینائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو ممکن نیست بودن بی‌بلا بسیار ممنونم</p></div>
<div class="m2"><p>که افکندست عشقم در بلای سرو بالائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندانم چون کنم در صحبت او حفظ دین خود</p></div>
<div class="m2"><p>که چشمش می‌کند تاراج ایمانم به ایمائی</p></div></div>