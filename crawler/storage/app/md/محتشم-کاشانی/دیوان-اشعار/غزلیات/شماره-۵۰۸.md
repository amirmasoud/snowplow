---
title: >-
    شمارهٔ  ۵۰۸
---
# شمارهٔ  ۵۰۸

<div class="b" id="bn1"><div class="m1"><p>آن کوست قبلهٔ همه کس قبله‌جو در او</p></div>
<div class="m2"><p>و آن روست قبله‌ای که کند کعبهٔ رو در او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آیینه ساز چشم من این شیشه ساخته</p></div>
<div class="m2"><p>نوعی که جز تو کس ننماید نکو در او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آب و هوای لطف تو گلزار کام ماست</p></div>
<div class="m2"><p>باغ شکفته صد گل بی‌رنگ و بو در او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داری دلی که هست محل ملایمت</p></div>
<div class="m2"><p>بد خوئی هزار بت تندخو در او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کویت چه گلشن است که از دجله‌های چشم</p></div>
<div class="m2"><p>جاری تراست خون دل از آب جو در او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باید به آب داد کتابی که هیچ جا</p></div>
<div class="m2"><p>نبود حدیث حرمت جام و سبو در او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین کلبه نگذرید تماشائیان که هست</p></div>
<div class="m2"><p>دیوانه‌ای از آن بت زنجیر مو در او</p></div></div>