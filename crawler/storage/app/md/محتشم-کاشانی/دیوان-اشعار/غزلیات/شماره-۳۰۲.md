---
title: >-
    شمارهٔ  ۳۰۲
---
# شمارهٔ  ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>زهی ربوده لعل تو صد فسون پرداز</p></div>
<div class="m2"><p>فریب خورده چشمت هزار شعبده باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رقیب محرم راز تو گشت نزدیک است</p></div>
<div class="m2"><p>که اشگ من به درد صدهزار پردهٔ راز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به صد شعف جهم از جا چو خوانیم سگ خویش</p></div>
<div class="m2"><p>چه جای آن که به سوی خودم کنی آواز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به طول و عرض شبی در وصال می‌خواهم</p></div>
<div class="m2"><p>که بر تو عرض کنم قصه‌های دور و دراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نام نامی محمود در قلمرو عشق</p></div>
<div class="m2"><p>زدند سکهٔ شاهی ولی طفیل ایاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به عهد لیلی و شیرین هزار عاشق بود</p></div>
<div class="m2"><p>شدند زان همه مجنون و کوه کن ممتاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجب اگر تو هم از سوز من الم نکشی</p></div>
<div class="m2"><p>که هست آتش پروانه سوز شمع گداز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بپرس از نفست سر آن دهن که جز او</p></div>
<div class="m2"><p>کسی نرفته به راه عدم که آید باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به غیر دیدنش از طاقتم ازو نگذاشت</p></div>
<div class="m2"><p>که غیرت ار همه کاهیست سست و کوه گداز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو نیست محتشم آن مه ز مهر دمسازت</p></div>
<div class="m2"><p>به داغ هجر بسوز و بسوز هجر بساز</p></div></div>