---
title: >-
    شمارهٔ  ۳۷۳
---
# شمارهٔ  ۳۷۳

<div class="b" id="bn1"><div class="m1"><p>بر در دل می‌زنند نوبت سلطان عشق</p></div>
<div class="m2"><p>ما و جنون می‌دهیم وعده به میدان عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رایت شاه جنون جلوه نما شد ز دور</p></div>
<div class="m2"><p>چاک به دامن رساند گرد بیابان عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن که ز لعلت فکند شور به دریای حسن</p></div>
<div class="m2"><p>کشتی ما را نخست داد به طوفان عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر جرم منند عفو و جزا در تلاش</p></div>
<div class="m2"><p>تا بچه فرمان دهد حاکم دیوان عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق ز فرمان حسن داد به دست توام</p></div>
<div class="m2"><p>وه چه شدی گر بدی حسن به فرمان عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلف تو را آن که کرد سلسلهٔ پیوند حسن</p></div>
<div class="m2"><p>ساخت جنون مرا سلسلهٔ جنبان عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرد چو حسنت برون سر به گریبان دهر</p></div>
<div class="m2"><p>عابد و زاهد زدند دست به دامان عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرد وی از بس حذر مور ندارد گذر</p></div>
<div class="m2"><p>این دل ویران که هست ملک سلیمان عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ماه رخ آن صنم مه چه رایان حسن</p></div>
<div class="m2"><p>داغ دل محتشم شمسه ایوان عشق</p></div></div>