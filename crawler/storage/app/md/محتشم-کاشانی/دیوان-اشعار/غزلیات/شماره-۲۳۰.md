---
title: >-
    شمارهٔ  ۲۳۰
---
# شمارهٔ  ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>آخر ای پیمان گسل یاران به یاران این کنند</p></div>
<div class="m2"><p>دوستان بی موجبی با دوستداران این کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ره رخشت فتادم خاک من دادی به باد</p></div>
<div class="m2"><p>شهسواران در روش با خاکساران این کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرهم از تیر تو جستم زخم بیدادم زدی</p></div>
<div class="m2"><p>دلنوازان جان من با دل فکاران این کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواستم تسکین سپند آتشت کردی مرا</p></div>
<div class="m2"><p>ای قرار جان و دل با بی قراران این کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رو به شهر وصل کردم تا عدم راندی مرا</p></div>
<div class="m2"><p>آخر ایمه با غریبان شهریاران این کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من غمت خوردم تو بر رغمم شدی غمخوار غیر</p></div>
<div class="m2"><p>با حریفان غم خود غمگساران این کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم در جان سپاری بود و خونش ریختی</p></div>
<div class="m2"><p>ای هزارت جان فدا با جان سپاران این کنند</p></div></div>