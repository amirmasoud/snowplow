---
title: >-
    شمارهٔ  ۵۰۷
---
# شمارهٔ  ۵۰۷

<div class="b" id="bn1"><div class="m1"><p>مراست رشتهٔ جان کاکل معنبر او</p></div>
<div class="m2"><p>فغان اگر سر موئی شود کم از سر او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه کاکل است که بر سر فتاده سر و مرا</p></div>
<div class="m2"><p>همای حس فکنده است سایه بر سر او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برابری به مه او روی نکرد مهی</p></div>
<div class="m2"><p>که رو نساخت چو آیینه در برابر او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر نقاب گشاید گل سمنبر من</p></div>
<div class="m2"><p>به گلستان چه نماید گل و سمن بر او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا ز دولت صد سالهٔ وصال آن به</p></div>
<div class="m2"><p>که غیر یک نفس آواره باشد از در او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو قتل بی‌گنهان خواهی ای فلک ز نهار</p></div>
<div class="m2"><p>بریز خلق من اول ولی به خنجر او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو محتشم شرف این بس که خلق دانندم</p></div>
<div class="m2"><p>کمین بنده‌ای از بندگان کمتر او</p></div></div>