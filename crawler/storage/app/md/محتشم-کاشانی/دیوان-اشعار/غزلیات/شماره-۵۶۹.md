---
title: >-
    شمارهٔ  ۵۶۹
---
# شمارهٔ  ۵۶۹

<div class="b" id="bn1"><div class="m1"><p>از بهر حسرت دادنم هر لحظه منشین با کسی</p></div>
<div class="m2"><p>اوقات خود ضایع مکن بر رغم چون من ناکسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شوخیت بر قتل خود دارم گمان اما کجا</p></div>
<div class="m2"><p>پروای این ناکس کند مثل تو بی‌پروا کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اقبال و ادبارم نگر کامشب به راهی این پسر</p></div>
<div class="m2"><p>تنها دچارم گشت و من همراه بودم با کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با غیر اگر عمری بود پیدا نگردد هیچ کس</p></div>
<div class="m2"><p>یک دم به من چون برخورد در دم شود پیدا کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با آن که خار غیرتم در پا بود از پی دوم</p></div>
<div class="m2"><p>در راه چون همره شود با آن گل رعنا کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر در خطر تن در عنا دل در گرو جان در بلا</p></div>
<div class="m2"><p>فکر سلامت چون کند با این ملامت‌ها کسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داری ز شیدا گشتگان رسوا بسی در دشت غم</p></div>
<div class="m2"><p>در سلگ ایشان محتشم رسواتر از رسوا کسی</p></div></div>