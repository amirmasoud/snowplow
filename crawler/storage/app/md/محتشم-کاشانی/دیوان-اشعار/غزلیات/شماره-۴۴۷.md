---
title: >-
    شمارهٔ  ۴۴۷
---
# شمارهٔ  ۴۴۷

<div class="b" id="bn1"><div class="m1"><p>تو به زور حسن ایمن مشو از سپاه آهم</p></div>
<div class="m2"><p>که من ضعیف پیکر ملک قوی سپاهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شه چار رکن عشقم که به چار سوی غیرت</p></div>
<div class="m2"><p>ز سیه گلیم محنت زده‌اند بارگاهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه هوای سربلندی نه خیال ارجمندی</p></div>
<div class="m2"><p>نه سراسری و خرگه نه غم سرو کلاهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هجوم وحشیانم شده متفق سپاهی</p></div>
<div class="m2"><p>که ز خسروی چو مجنون به ستیزه باج خواهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جنون فزود هردم چو بلای ناگهانی</p></div>
<div class="m2"><p>در و دشت در حصارم دد و دام در پناهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زده سر ز باغ رویت چه گیاه خوش نسیمی</p></div>
<div class="m2"><p>که گل جنون شکفته ز نسیم آن گیاهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تو محتشم چه پنهان که دگر به قصد ایمان</p></div>
<div class="m2"><p>ز بتان نامسلمان صنمی زده است راهم</p></div></div>