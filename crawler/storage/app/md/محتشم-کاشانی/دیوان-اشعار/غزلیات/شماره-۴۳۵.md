---
title: >-
    شمارهٔ  ۴۳۵
---
# شمارهٔ  ۴۳۵

<div class="b" id="bn1"><div class="m1"><p>زین گونه چو در مشق جنون حلقه چو نونم</p></div>
<div class="m2"><p>فرداست که سر حلقه ارباب جنونم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بار دلم از کوه فزونست عجب نیست</p></div>
<div class="m2"><p>گر خم شود از بار چنین قد چو نونم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بندهٔ مه خود شدم ایام</p></div>
<div class="m2"><p>از قید دگر سیمبران کرد برونم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمت به خدنگ مژه‌کار دل من ساخت</p></div>
<div class="m2"><p>نگذاشت که تیغت شود آلوده به خونم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد شکر که چون لاله به داغ کهن دل</p></div>
<div class="m2"><p>آراسته در عشق تو بیرون و درونم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من محتشم شاعر و شیرین سخن اما</p></div>
<div class="m2"><p>لال است زبانم که به چنگ تو زبونم</p></div></div>