---
title: >-
    شمارهٔ  ۲۱۷
---
# شمارهٔ  ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>عاشقان نرد محبت چو به دلبر بازند</p></div>
<div class="m2"><p>شرط عشق است که اول دل و دین دربازند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن چه جان دو جهان افکند آسان بگرو</p></div>
<div class="m2"><p>نرد شوخی است که خوبان سمنبر بازند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دیاری که ز یاد از همه می‌باید باخت</p></div>
<div class="m2"><p>حکم ناز است که طایفه کمتر بازند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر داد محبت که حسابی دگرست</p></div>
<div class="m2"><p>بی‌حسابست که تا سر بود افسر بازند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرد دعویست که چون عرصه شود تنگ آنجا</p></div>
<div class="m2"><p>سروران افسر و بی‌پا و سران سر بازند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بندی شش جهتم فرد چو آن مهرهٔ نرد</p></div>
<div class="m2"><p>کش جدا در عقب عقده ششدر بازند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست در عشق قماری که حرج نیست در آن</p></div>
<div class="m2"><p>گرچه بر روی مصلای پیامبر بازند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محتشم نرد ملاقات بتان باعشاق</p></div>
<div class="m2"><p>هست خوش خاصه کز افراط مکرر بازند</p></div></div>