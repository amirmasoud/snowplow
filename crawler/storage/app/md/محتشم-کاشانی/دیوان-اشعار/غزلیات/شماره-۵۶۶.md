---
title: >-
    شمارهٔ  ۵۶۶
---
# شمارهٔ  ۵۶۶

<div class="b" id="bn1"><div class="m1"><p>باز بر من نظر افکنده شکار اندازی</p></div>
<div class="m2"><p>به شکار آمده در دشت دلم شهبازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده از گوشه کنارم هدف ناوک ناز</p></div>
<div class="m2"><p>گوشه چشم خدنگ افکن صید اندازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون بهای دو جهانست در اثنای عتاب</p></div>
<div class="m2"><p>از لبش خنده‌ای از گوشهٔ چشمش نازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن مجلسیش می‌کشد از ذوق مرا</p></div>
<div class="m2"><p>چون زیم گر شنوم روزی از آن لب رازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زکات قدمت بر لب بام آی امشب</p></div>
<div class="m2"><p>چون به گوشت رسد آلوده به درد آوازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشمت از غمزه مرا کشت و لب زنده نساخت</p></div>
<div class="m2"><p>آخر ای یوسف عیسی نفسان اعجازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم دل چو به آن غمزه سپردی زنهار</p></div>
<div class="m2"><p>برحذر باش که واقف نشود غمازی</p></div></div>