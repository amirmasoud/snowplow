---
title: >-
    شمارهٔ  ۲۹۸
---
# شمارهٔ  ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>دانم اگر از دلبری قانع به جانی ای پسر</p></div>
<div class="m2"><p>داد سبک دستی دهم در سر فشانی ای پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسم وفا بنیاد کن آواره‌ای را یاد کن</p></div>
<div class="m2"><p>درمانده‌ای را شاد کن تا در نمانی ای پسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر خاکساران بی‌خبر مستانه بر رخش جفا</p></div>
<div class="m2"><p>در شاه راه دلبری خوش میدوانی ای پسر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسنت همی گوید که هان خوش جهانی را به کس</p></div>
<div class="m2"><p>هیچت نمی‌گوید که هی نی جوانی ای پسر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با صد شکایت پیش تو چون آیم اندر یک سخن</p></div>
<div class="m2"><p>بندی زبانم گویا جادو زبانی ای پسر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیشب سبکدستی تو را می‌داد گستاخانه می</p></div>
<div class="m2"><p>کامروز از آن لایعقلی بر سر گرانی ای پسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیوان شعر محتشم پر آتش است از حرف جور</p></div>
<div class="m2"><p>غافل مشو از سوز او روزی بخوانی ای پسر</p></div></div>