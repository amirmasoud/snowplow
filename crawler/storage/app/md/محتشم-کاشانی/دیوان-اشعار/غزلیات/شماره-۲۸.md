---
title: >-
    شمارهٔ  ۲۸
---
# شمارهٔ  ۲۸

<div class="b" id="bn1"><div class="m1"><p>با چنین جرمی نراندم از دل ویران تو را</p></div>
<div class="m2"><p>این قدرها جای در دل بوده است ای جان تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساحری گویا با چندین خطا چون دیگران</p></div>
<div class="m2"><p>راندن از چشم و برون کردن ز دل نتوان تو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خدا بهر تو خواهم صد بلا اما اگر</p></div>
<div class="m2"><p>در بلائی بینمت گردم بلاگردان تو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیستم راضی به مرگت لیک می‌خواهم چو خود</p></div>
<div class="m2"><p>از غم ناکس پرستی در تب هجران تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن چنان شوخی که خواهی داشت مرد مرا به تنگ</p></div>
<div class="m2"><p>گر کنم در پرده‌های چشم خود پنهان تو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از لباس غیرتم عریان نمی‌دیدی اگر</p></div>
<div class="m2"><p>می‌توانستم که دارم دست از دامان تو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم در غیرت این سستی که من دیدم ز تو</p></div>
<div class="m2"><p>بی‌تکلف می‌توان کشتن به جرم آن تو را</p></div></div>