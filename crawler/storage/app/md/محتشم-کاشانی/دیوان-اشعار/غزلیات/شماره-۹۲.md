---
title: >-
    شمارهٔ  ۹۲
---
# شمارهٔ  ۹۲

<div class="b" id="bn1"><div class="m1"><p>حکمی که همچو آب روان در دیار اوست</p></div>
<div class="m2"><p>خونریز عاشقان تبه روزگار اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غیرتم هلاک که بر صید تازه‌ای</p></div>
<div class="m2"><p>هم زخم زخم کاری و هم کار کار اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون می‌چکاند از دل صد صید بی‌نصیب</p></div>
<div class="m2"><p>تیر شکاری که نصیب شکار اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدعاقبت کسی که چو من اعتماد وی</p></div>
<div class="m2"><p>بر عهدهای بسته نا استوار اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حرفی که می‌گذارد و می‌داردم خموش</p></div>
<div class="m2"><p>لطف نهان و مرحمت آشکار اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باغیست تازه باغ عذارش که بی گزاف</p></div>
<div class="m2"><p>صد فصل در میان خزان و بهار اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیکوترین نوازش جانان محتشم</p></div>
<div class="m2"><p>آزار جان خسته و جسم فکار اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فریاد اگر نه جابر آزار او شود</p></div>
<div class="m2"><p>سلمان جابری که خداوندگار اوست</p></div></div>