---
title: >-
    شمارهٔ  ۵۸۷
---
# شمارهٔ  ۵۸۷

<div class="b" id="bn1"><div class="m1"><p>محتشم چون عمر صرف خدمت وی می‌کنی</p></div>
<div class="m2"><p>پادشاهی گر نکردی این زمان کی می‌کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توسن عمر آن جهان‌پیما ستور باد پا</p></div>
<div class="m2"><p>یک جهان طی می‌کند چون بادپا هی می‌کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سختی راه محبت را دلیل این بس که تو</p></div>
<div class="m2"><p>در نخستین منزلی هرچند ره طی می‌کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقیا بر ساحل غم مانده‌ام وقتست اگر</p></div>
<div class="m2"><p>کشیت ساغر روان در قلزم می می‌کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنبل از تاب جمالت می‌نشیند در عرق</p></div>
<div class="m2"><p>زلف را هرگه نقاب روی پر خوی می‌کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آهوان در پایت ای مجنون از آن سر می‌نهند</p></div>
<div class="m2"><p>کآشنایی با سگ لیلی پیاپی می‌کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفته بودی می‌کنم با محتشم روزی وفا</p></div>
<div class="m2"><p>شاه خوبان وعده کردی و وفا کی می‌کنی</p></div></div>