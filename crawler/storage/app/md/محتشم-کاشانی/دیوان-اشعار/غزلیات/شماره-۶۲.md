---
title: >-
    شمارهٔ  ۶۲
---
# شمارهٔ  ۶۲

<div class="b" id="bn1"><div class="m1"><p>زخم جفای یار که بر سینه مرهم است</p></div>
<div class="m2"><p>از بخت من زیاده و از لطف او کم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کودک دل است و دو و لعب دوست لیک</p></div>
<div class="m2"><p>در قید اختلاط ز قید معلم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پنهان گلی شکفته درین بزم کان نگار</p></div>
<div class="m2"><p>خود را شکفته دارد و بسیار درهم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد مست و از تواضع بی‌اختیار او</p></div>
<div class="m2"><p>در بزم شد عیان که نهان با که همدمست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترسم برات لطف گدائی رسد به مهر</p></div>
<div class="m2"><p>کان لعل خاتمیست که در دست خاتمست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از گریه‌های هجر شکست بنای جان</p></div>
<div class="m2"><p>موقوف یک نم دیگر از چشم پر نمست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر صبح دم من و سر کوی بتان بلی</p></div>
<div class="m2"><p>شغلی است این که بر همهٔ کاری مقدم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با این خصایل ملکی بر خلاف رسم</p></div>
<div class="m2"><p>باید که سجدهٔ تو کند هر که آدم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با غم که جان در آرزوی خیر باد اوست</p></div>
<div class="m2"><p>گفتار محتشم همه دم خیر مقدم است</p></div></div>