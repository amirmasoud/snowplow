---
title: >-
    شمارهٔ  ۲۶۱
---
# شمارهٔ  ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>چشمم چو روز واقعه در خواب می‌شود</p></div>
<div class="m2"><p>کین من از دل تو عنان تاب می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که آتشت بنشانم به آب تیغ</p></div>
<div class="m2"><p>تا تیغ میکشی دل من آب می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مجلسی که باده باغیار می‌دهی</p></div>
<div class="m2"><p>خون جگر حوالهٔ احباب می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از روی سیمگون چو سحر پرده می‌کشی</p></div>
<div class="m2"><p>مه بر فلک ز شرم تو سیماب می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در طاعت از تواضعت اندیشهٔ جواب</p></div>
<div class="m2"><p>جنبش فکن در ابروی محراب می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن وعدهٔ دروغ تو هم گه گهی نکوست</p></div>
<div class="m2"><p>کارام بخش عاشق بی‌تاب می‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بخت تیره هرچه طلب کرد محتشم</p></div>
<div class="m2"><p>چون کیمیای وصل تو نایاب می‌شود</p></div></div>