---
title: >-
    شمارهٔ  ۳۰۶
---
# شمارهٔ  ۳۰۶

<div class="b" id="bn1"><div class="m1"><p>ای هنوزت مژه از صف شکنی بر سر ناز</p></div>
<div class="m2"><p>گوشهٔ چشم تو دنباله کش لشگر ناز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما به جان ناز کشیم از تو اگر هم روزی</p></div>
<div class="m2"><p>خط اجازت ده حسنت شود ازکشور ناز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نام جلاد بران غمزه منه کاندر قتل</p></div>
<div class="m2"><p>کار جلاد نباشد زدن خنجر ناز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده هرچند که گستاخ بود چون بیند</p></div>
<div class="m2"><p>تکیهٔ نخل گران بار تو بر بستر ناز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بردرت منتظرند اهل هوس وای اگر</p></div>
<div class="m2"><p>در رغبت بگشائی و ببندی در ناز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر آن نرگس پرحوصله گردم که ز من</p></div>
<div class="m2"><p>صد نگه بیند و یک ره نگرد از سر ناز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم را شود آن روز سیه دفتر عمر</p></div>
<div class="m2"><p>که بشوئی تو ز بسیاری خط دفتر ناز</p></div></div>