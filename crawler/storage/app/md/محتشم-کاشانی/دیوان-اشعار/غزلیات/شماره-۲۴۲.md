---
title: >-
    شمارهٔ  ۲۴۲
---
# شمارهٔ  ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>دردا که وصل یار به جز یک نفس نبود</p></div>
<div class="m2"><p>یک جرعه از وصال چشیدیم و بس نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد درد دل فزون که به عیسی دمی چنان</p></div>
<div class="m2"><p>دل خسته‌ای چنین دو نفس هم نفس نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بختم ز وصل یک دمه آن مرهمی که ساخت</p></div>
<div class="m2"><p>تسکین ده جراحت چندین هوس نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ظل همای وصل که گسترده شد مرا</p></div>
<div class="m2"><p>بر سر به قدر سایهٔ بال مگس نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بردی مرا به نقش وفا نقد جان ز دست</p></div>
<div class="m2"><p>این دستبرد جان کسی حد کس نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گرمی وصال تمامم بسوختی</p></div>
<div class="m2"><p>این نیم لطف از تو مرا ملتمس نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر پشت دست خویش گزد محتشم سزد</p></div>
<div class="m2"><p>جز یک دمش به وصل تو چون دسترس نبود</p></div></div>