---
title: >-
    شمارهٔ  ۲۵۵
---
# شمارهٔ  ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>باز ما را جان به استقبال جانان می‌رود</p></div>
<div class="m2"><p>تن به جا می‌ماند و دل همره جان می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز جیبی چاک خواهم زد که دستم هر زمان</p></div>
<div class="m2"><p>بی‌خود از وسواس دل سوی گریبان می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز خواهم در خروش آمد که وقت حرف صوت</p></div>
<div class="m2"><p>بر زبان نطقم اول آه و افغان می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز خواهم غوطه زد در خون که از بحر درون</p></div>
<div class="m2"><p>سوی چشمم ابر خون باری شتابان می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز دست از دیده خواهم شست گز عیب کسان</p></div>
<div class="m2"><p>می‌کند ایما که آن یوسف ز کنعان می‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز محکم می‌شود با درد پیمان دلم</p></div>
<div class="m2"><p>کاینچنین بردم گمان کان سست پیمان می‌رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز لازم شد وداع جان که هردم هاتقی</p></div>
<div class="m2"><p>با دلم آهسته می‌گوید که جانان می‌رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز درخواب پریشان دیدنم شب تا به روز</p></div>
<div class="m2"><p>چون نباشم کز کف آن زلف پریشان می‌رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محتشم در عشق رفت آن صبر و سامانی که بود</p></div>
<div class="m2"><p>بخت اکنون از من بی‌صبر و سامان می‌رود</p></div></div>