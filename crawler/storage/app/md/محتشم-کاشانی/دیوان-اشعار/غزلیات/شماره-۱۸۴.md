---
title: >-
    شمارهٔ  ۱۸۴
---
# شمارهٔ  ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>جدائی تو هلاکم ز اشتیاق تو کرد</p></div>
<div class="m2"><p>تو با من آن چه نکردی غم فراق تو کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مرگ تلخ شود کام ناصحی که چنین</p></div>
<div class="m2"><p>شراب صحبت ما تلخ در مذاق تو کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عمر بر نخورد آن که قصد خرمن ما</p></div>
<div class="m2"><p>به تیز ساختن آتش نفاق تو کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اجل که بی مددی قتل این و آن کردی</p></div>
<div class="m2"><p>چو وقت کار من آمد به اتفاق تو کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فغان که هر که به نامحرمی مثل گردید</p></div>
<div class="m2"><p>فلک به رغم منش محرم وثاق تو کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبانه هر که به بزمی فتاد و رفت فرو</p></div>
<div class="m2"><p>صباح سر به در از غرفة رواق تو کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خود هلاکتری دید و سینه چاکتری</p></div>
<div class="m2"><p>به هر که محتشم اظهار اشتیاق تو کرد</p></div></div>