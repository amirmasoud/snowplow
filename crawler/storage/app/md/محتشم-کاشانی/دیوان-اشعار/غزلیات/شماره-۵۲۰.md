---
title: >-
    شمارهٔ  ۵۲۰
---
# شمارهٔ  ۵۲۰

<div class="b" id="bn1"><div class="m1"><p>ای گردن بلند قدان در کمند تو</p></div>
<div class="m2"><p>رعنائی آفریده قد بلند تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر صرصری سوار وز دل می‌برد قرار</p></div>
<div class="m2"><p>طرز گران خرامی رعنا سمند تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش نرخ خندهٔ تو به بازار آرزو</p></div>
<div class="m2"><p>افکنده در مزاد لب نوشخند تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من چون کنم که طور بد ناپسند من</p></div>
<div class="m2"><p>گردد پسند خاطر مشکل پسند تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چندم فتاده بینی و گوئی که کیست این</p></div>
<div class="m2"><p>بیمار تو شکسته تو دردمند تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دردت مباد و باد بر آتش سپندوار</p></div>
<div class="m2"><p>چشم حسود از پی دفع گزند تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قتلش رواست گر همه صید حرم بود</p></div>
<div class="m2"><p>آن صید کاضطراب کند در کمند تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باید که به نواخت ز صید گریزپای</p></div>
<div class="m2"><p>آن صید به که دست دهد خود به بند تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پای گریز محتشم از دور بسته است</p></div>
<div class="m2"><p>عشق دراز سلسلهٔ صید پند تو</p></div></div>