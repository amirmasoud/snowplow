---
title: >-
    شمارهٔ  ۱۰۳
---
# شمارهٔ  ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>ای در درون جان ز دل من کرانه چیست</p></div>
<div class="m2"><p>جائی چنین کراست درون آبهانه چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر زمان زمانه به شغلی قیام داشت</p></div>
<div class="m2"><p>جز عشق در زمان تو شغل زمانه چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خون گرفته‌ای نگرفته عنان تو</p></div>
<div class="m2"><p>این خون که می‌چکد ز سر تازیانه چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرگار خود چو عشق به گردش در آورد</p></div>
<div class="m2"><p>ظاهر شود که کار درین کارخانه چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر عشق نیست واسطه بر گرد یک نهال</p></div>
<div class="m2"><p>پرواز صد همای بلند آشیانه چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غالب حریف صحبت اگر دی نبوده غیر</p></div>
<div class="m2"><p>امروزش این مصاحبت غالبانه چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گیرد ز من امانت جان قاصدی که او</p></div>
<div class="m2"><p>گوید که در میان من و او نشانه چیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون چشم اوست نازی و از من بهانه‌ای</p></div>
<div class="m2"><p>خلقی برای آشتی اندر میانه چیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوابم گرفت محتشم از گفته‌های تو</p></div>
<div class="m2"><p>بیتی بخوان ز گفتهٔ سلمان بهانه چیست</p></div></div>