---
title: >-
    شمارهٔ  ۱۰۲
---
# شمارهٔ  ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>درهم است آن بت طناز نمی‌دانم چیست</p></div>
<div class="m2"><p>ملتفت نیست به من باز نمی‌دانم چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بودی بنده‌نواز آن مه و امروز از ناز</p></div>
<div class="m2"><p>کرده قانون دگر ساز نمی‌دانم چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوشهٔ چشم به من دارد و مخصوصان را</p></div>
<div class="m2"><p>می‌کند سوی خود آواز نمی‌دانم چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد ره افتاده نگاهش به غلط جانب من</p></div>
<div class="m2"><p>این نگاه غلط انداز نمی‌دانم چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من گمان زد به گنه و آن بت بدخو کرده</p></div>
<div class="m2"><p>با حریفان جدل آغاز نمی‌دانم چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راز در پرده و اهل غرض استاده خموش</p></div>
<div class="m2"><p>غرض از پوشش این راز نمی‌دانم چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم سر به گریبان حیل برده رقیب</p></div>
<div class="m2"><p>فکر آن شعبده پرداز نمی‌دانم چیست</p></div></div>