---
title: >-
    شمارهٔ  ۵۳۲
---
# شمارهٔ  ۵۳۲

<div class="b" id="bn1"><div class="m1"><p>باز برخاسته از دشت بلا گرد سپاه</p></div>
<div class="m2"><p>آرزو سایه سپه فتنه جنبت کش شاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زده بر قلب سپاهی و دلیل است برین</p></div>
<div class="m2"><p>وضع دستارو سراسیمگی پر کلاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کم نگاه است ز بس حوصله اما دارد</p></div>
<div class="m2"><p>پادشاهانه نگاهی به دل چند نگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان رخ توبه شکن منع نگه ممکن نیست</p></div>
<div class="m2"><p>که شود هر نگه آلوده به صدگونه گناه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارد ای اختر تابنده به دور تو جهان</p></div>
<div class="m2"><p>روز پر نور دو خورشید و شب تیره دو ماه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر لب و خط بنمائی به خدا میل کنند</p></div>
<div class="m2"><p>آهوان چمن قدس به این آب و گیاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زخم ناخورده گذشتم زهم ای سنگین دل</p></div>
<div class="m2"><p>در کمان تیر نگاه این همه دارند نگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صحبت ما و تو پوشیده به از خلق جهان</p></div>
<div class="m2"><p>گرچه بر عصمت ما هر دو جهانند گواه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز انتظار تو غلط وعده‌ام از بیم و امید</p></div>
<div class="m2"><p>همه شب دست به سر گوش به در چشم به راه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منظر دیدهٔ یعقوب ز حرمان تاریک</p></div>
<div class="m2"><p>چهرهٔ یوسف گل چهرهٔ چراغ ته چاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>محتشم رشحه‌ای از لجه رحمت کافی است</p></div>
<div class="m2"><p>گر در آیند به محشر دو جهان نامه سیاه</p></div></div>