---
title: >-
    شمارهٔ  ۵۴۲
---
# شمارهٔ  ۵۴۲

<div class="b" id="bn1"><div class="m1"><p>شبهای هجران همنشین از مهر او یادم مده</p></div>
<div class="m2"><p>همسایه را دردسر از افغان و فریادم مده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زاری و افغان من گردد دل او سخت تر</p></div>
<div class="m2"><p>ای گریه بر آبم مران ای آه بر بادم مده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون میرم و کین منش باقی بود ای بخت بد</p></div>
<div class="m2"><p>جز جانب دوزخ صلازین محنت آبادم مده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین سان که آن نامهربان شاد است از ناشادیم</p></div>
<div class="m2"><p>گر مهربانی ای فلک هرگز دل شادم مده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هردم به داد آیم برت از ذوق بیداد دگر</p></div>
<div class="m2"><p>خواهی به داد من رسی بیداد کن دادم مده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هردم کنم صد کوه غم در بیستون عشق تو</p></div>
<div class="m2"><p>من سخن جان دیگرم نسبت به فرهادم مده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم به بیدادم مکش درخنده شد کای محتشم</p></div>
<div class="m2"><p>حکمت بر افلاطون مخوان تعلیم بیدادم مده</p></div></div>