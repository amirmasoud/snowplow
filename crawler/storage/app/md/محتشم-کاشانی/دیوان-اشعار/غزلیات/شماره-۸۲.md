---
title: >-
    شمارهٔ  ۸۲
---
# شمارهٔ  ۸۲

<div class="b" id="bn1"><div class="m1"><p>حرف عشقت مگر امشب ز یکی سرزده است</p></div>
<div class="m2"><p>که حیا این همه آتش به گلت در زده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زده جام غضب آن غمزه مگر غمزده‌ای</p></div>
<div class="m2"><p>طاق ابروی تو را گفته و ساغر زده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شعلهٔ شمع جمالت شده برهم زده آه</p></div>
<div class="m2"><p>مرغ روح که به پیرامن آن پرزده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خونت از غیرت اشک که به جوش است که باز</p></div>
<div class="m2"><p>گل تبخاله ز شیرین رطبت سرزده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌گذشتی وز میغ مژه خون می‌بارید</p></div>
<div class="m2"><p>که به حیران شده‌ای چشم تو خنجر زده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جیب جانش ز من اندر خطر است آن که چنین</p></div>
<div class="m2"><p>دامن سعی به راه طلبت بر زده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حاجبت کرده کمان زه مگر از کم حذری</p></div>
<div class="m2"><p>داد جرات زده‌ای قصر تو را در زده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوش حریفیست که در وادی عشقت همه جا</p></div>
<div class="m2"><p>خیمه با محتشم از لاف برابر زده است</p></div></div>