---
title: >-
    شمارهٔ  ۵۴۴
---
# شمارهٔ  ۵۴۴

<div class="b" id="bn1"><div class="m1"><p>قلم نسخ بران بر ورق حسن همه</p></div>
<div class="m2"><p>کاین قلمرو به تو داده است خدا یک قلمه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان دو هندوی سیه مست که مردم فکنند</p></div>
<div class="m2"><p>تیغ هندیست نگاه تو ولیکن دو دمه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش‌تر از عشرت صد سالهٔ هشیارانست</p></div>
<div class="m2"><p>با می صاف دو ساله طرب یک دو مه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دم ناصح واعظ دلم اندر چاهیست</p></div>
<div class="m2"><p>که ز یک سوی سموم است وز یک سوی دمه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رهزنان در صدد غارت و خوبان غافل</p></div>
<div class="m2"><p>گرگ بیداز ز هر گوشه و در خواب رمه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم نزع است وز شوق کلمات تو مرا</p></div>
<div class="m2"><p>یک نفس بیش نمانده است بگو یک کلمه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم فتنه قوی دست شد آن دم که نهاد</p></div>
<div class="m2"><p>زلف نو سلسله‌اش سلسله بر پای همه</p></div></div>