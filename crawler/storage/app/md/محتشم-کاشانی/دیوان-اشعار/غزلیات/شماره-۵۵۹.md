---
title: >-
    شمارهٔ  ۵۵۹
---
# شمارهٔ  ۵۵۹

<div class="b" id="bn1"><div class="m1"><p>بر در درج قفل زدم یک چندی</p></div>
<div class="m2"><p>عاقبت داد گشادش بت شکر خندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخت از ذوق گرفتاری من می‌کوشد</p></div>
<div class="m2"><p>دست و بازوی کمندافکن وحشی بندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لطف ممتاز کن آماده که آمد بر در</p></div>
<div class="m2"><p>بی‌نیاز از تو جهانی به تو حاجتمندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به نزدیک‌ترین وعدهٔ وصلت برسم</p></div>
<div class="m2"><p>از خدا می‌طلبم عمر ابد پیوندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر از مادر دوران همه یوسف زاید</p></div>
<div class="m2"><p>ننشیند چو تو بر دامن او فرزندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مژده‌ای درد که در دام تو افتاد آخر</p></div>
<div class="m2"><p>نامفید به دوائی بالم خورسندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درام از مرغ شب‌آویز دلی نالان‌تر</p></div>
<div class="m2"><p>من که دارم ز دل آویز کمندی بندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دگر امشب چه نظر دیده ندانم که به من</p></div>
<div class="m2"><p>می‌کند لطف ولی لطف غضب مانندی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهر نادیدن آن رو گه و بی‌گه ناصح</p></div>
<div class="m2"><p>می‌دهد بندم و آن گه چه مؤثر پندی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هست دشنام پیاپی ز لب شیرینش</p></div>
<div class="m2"><p>شربتی غیر مکرر ز مکرر قندی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>محتشم عشوهٔ طاقت شکن ساقی بزم</p></div>
<div class="m2"><p>اگر اینست دگر می‌شکنم سوگندی</p></div></div>