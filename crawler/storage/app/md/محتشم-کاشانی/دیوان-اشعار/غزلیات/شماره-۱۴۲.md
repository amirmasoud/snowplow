---
title: >-
    شمارهٔ  ۱۴۲
---
# شمارهٔ  ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>دادم از دست برون دامن دلبر به عبث</p></div>
<div class="m2"><p>به گمانهای غلط رفتم از آن در به عبث</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چهرهٔ عصمت او یافت تغییر به دروغ</p></div>
<div class="m2"><p>مشرب عشرت من گشت مکدر به عبث</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیره گشت آینهٔ پاکی آن مه به خلاف</p></div>
<div class="m2"><p>شد سیه روز من سوخته اختر به عبث</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود در قبضهٔ تسخیر من اقلیم وصال</p></div>
<div class="m2"><p>ناکهان باختم آن ملک مسخر به عبث</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصل هر نقد که در دامن امیدم ریخت</p></div>
<div class="m2"><p>من بی صرفه تلف ساختم اکثر به عبث</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جامهٔ هجر که بر قامت صبر است دراز</p></div>
<div class="m2"><p>بر قد خویش بریدم من ابتر به عبث</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم گر نشد آشفته دماغت ز جنون</p></div>
<div class="m2"><p>به چه دادی ز کف آن زلف معنبر به عبث</p></div></div>