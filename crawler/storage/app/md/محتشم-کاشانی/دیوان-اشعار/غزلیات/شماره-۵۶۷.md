---
title: >-
    شمارهٔ  ۵۶۷
---
# شمارهٔ  ۵۶۷

<div class="b" id="bn1"><div class="m1"><p>چه باشد گر سنان غمزه را زین تیزتر سازی</p></div>
<div class="m2"><p>دل ریش مرا در عشق ازین خونریزتر سازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گذر بروادی ناز افکنی دامن‌کشان واندم</p></div>
<div class="m2"><p>به یک دامن فشانی آتشم را تیزتر سازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلا بر گرد من میگرد اما دست می‌یابد</p></div>
<div class="m2"><p>گهی بر من کزین خود را بلاانگیزتر سازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هلاک از نرگس بیمار خواهی ساخت آن روزم</p></div>
<div class="m2"><p>که در خون‌خواریش امروز ناپرهیزتر سازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز نایابی در وصل تو قیمت یاب‌تر گردد</p></div>
<div class="m2"><p>محیط حسن را هرچند طوفان خیزتر سازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به راه قدمت عشقت شتاب آموزتر گردم</p></div>
<div class="m2"><p>خطابت را اگر با من عتاب‌آمیزتر سازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهد سر برسم رخش تو چون صد محتشم هردم</p></div>
<div class="m2"><p>اگر فتراک خود را زین شکار آویزتر سازی</p></div></div>