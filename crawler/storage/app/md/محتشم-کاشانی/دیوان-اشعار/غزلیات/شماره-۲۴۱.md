---
title: >-
    شمارهٔ  ۲۴۱
---
# شمارهٔ  ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>مهی که شمع رخش نور دیدهٔ من بود</p></div>
<div class="m2"><p>ز دیده رفت و مرا سوخت این چه رفتن بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا کشنده‌ترین ورطهٔ محل وداع</p></div>
<div class="m2"><p>سرشگ رانی آن سر پاکدامن بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فکند چشم حسودم جدا ز دوست چه دوست</p></div>
<div class="m2"><p>یکی که مایهٔ رشگ هزار دشمن بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشید روز به شامم چه شام آن که درو</p></div>
<div class="m2"><p>ستارهٔ سحر روز مرگ روشن بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وزید باد فراقی چه باد آنکه ز دهر</p></div>
<div class="m2"><p>برندهٔ من بر باد رفته خرمن بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسید سیل فنائی چه سیل آن که رهش</p></div>
<div class="m2"><p>به مامن من مجنون دشت مسکن بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برآمد ابر بلائی چه ابر آن که نخست</p></div>
<div class="m2"><p>ترشحش ز برای خرابی من بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو یار گرم سفر شد اگرچه شمع صفت</p></div>
<div class="m2"><p>به باد می‌شد ازو هر سری که بر تن بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسوخت محتشم اول که از سپاه فراق</p></div>
<div class="m2"><p>ستیزه یزک اندروی آتش افکن بود</p></div></div>