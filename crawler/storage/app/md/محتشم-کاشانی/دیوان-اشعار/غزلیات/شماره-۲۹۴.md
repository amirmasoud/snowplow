---
title: >-
    شمارهٔ  ۲۹۴
---
# شمارهٔ  ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>پیشت از سهوی که کردم ای خدیو کامکار</p></div>
<div class="m2"><p>شرمسارم شرمسارم شرمسارم شرمسار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود خاک غفلتم در دیدهٔ جوهر شناس</p></div>
<div class="m2"><p>کز خزف نشناختم در خاصه در شاهوار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با تو گستاخانه آمد در سخن این بی‌شعور</p></div>
<div class="m2"><p>این چه درکست و شعور استغفرالله زین شعار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمت دستم بگیر و مردم از شرمندگی</p></div>
<div class="m2"><p>گرچه می‌گویند این را بندگان با کردگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده‌ام بر پشت پا شد تا قیامت دوخته</p></div>
<div class="m2"><p>بس که برمن گشت گردون زین ممر خجلت گمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طرفه‌تر این کان غلط زین بندهٔ گمنام شد</p></div>
<div class="m2"><p>واقع اندر مجلس دستور خورشید اشتهار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پادشاه محتشم مه رایت انجم حشم</p></div>
<div class="m2"><p>کز سپاه فتنه بادا حشمت او در حصار</p></div></div>