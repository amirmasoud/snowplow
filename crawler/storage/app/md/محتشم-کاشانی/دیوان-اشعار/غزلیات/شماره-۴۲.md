---
title: >-
    شمارهٔ  ۴۲
---
# شمارهٔ  ۴۲

<div class="b" id="bn1"><div class="m1"><p>ای زیر مشق سر خط حسن تو افتاب</p></div>
<div class="m2"><p>در مشق با کشیدن زلف تو مشگ ناب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس نقش خامه زیر و زبر گشت تا از آن</p></div>
<div class="m2"><p>نقشی چنین ز دقت صانع شد انتخاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عکست که ای کرده در آب ای محیط حسن</p></div>
<div class="m2"><p>می‌بیندت مگر که دل و دارد اضطراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عالمی که رتبهٔ حسن از یگانگیست</p></div>
<div class="m2"><p>نه آینه است عکس پذیر از رخت نه آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیهات ما و عزم وصال محال تو</p></div>
<div class="m2"><p>کان کار وهم و فعل خیالست و شغل خواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا شهسوار صبر سبکتر کند عنان</p></div>
<div class="m2"><p>با ناز خویش گو که گران تر کند رکاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از من نهفته مانده به بزم از حجاب عشق</p></div>
<div class="m2"><p>روئی که آن نهفته نمی‌گردد از نقاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امروز ساقیا شده زاهد حجاب بزم</p></div>
<div class="m2"><p>برخیز و می بیار که برخیزد این حجاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیتی شنو ز محتشم ای بت که بهتراست</p></div>
<div class="m2"><p>یک بیت عاشقانه ز بیتی پر از کتاب</p></div></div>