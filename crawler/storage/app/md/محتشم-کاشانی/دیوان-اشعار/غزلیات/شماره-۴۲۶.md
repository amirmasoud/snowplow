---
title: >-
    شمارهٔ  ۴۲۶
---
# شمارهٔ  ۴۲۶

<div class="b" id="bn1"><div class="m1"><p>افکن گذر به کلبه ما تا بهم رسد</p></div>
<div class="m2"><p>از گرد رهگذار تو کحلی برای چشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر در وثاق خاک نشینان قدم نهی</p></div>
<div class="m2"><p>سازند خاک پای تو را توتیای چشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیرون مرو ز منزل مردم نشین خویش</p></div>
<div class="m2"><p>ای منزل تو منظر نزهت سرای چشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از مردمی اگر به حجاب ای مراد دل</p></div>
<div class="m2"><p>پیدا کنم برای تو جائی ورای چشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چشم آفتاب برآید گر افکنی</p></div>
<div class="m2"><p>پرتو به خانه دلم از غرفه‌های چشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناید فرو سرم به فلک گر تو سرفراز</p></div>
<div class="m2"><p>آئی فرو به بارگه دل گشای چشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر محتشم گذار فکن کز برای توست</p></div>
<div class="m2"><p>گوهر فشانی مژه‌اش در سرای چشم</p></div></div>