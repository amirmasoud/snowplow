---
title: >-
    شمارهٔ  ۴۲۵
---
# شمارهٔ  ۴۲۵

<div class="b" id="bn1"><div class="m1"><p>ای هزارت چشم در هر گوشه سرگردان چشم</p></div>
<div class="m2"><p>آهوی چشم سیه مستان تو را قربان چشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردمند از درد چشمت چشم بیماران ولی</p></div>
<div class="m2"><p>درد برچیدن ز چشمت جمله را درمان چشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورد تا چشم تو چشم ای نرگس باران اشگ</p></div>
<div class="m2"><p>شوخ چشمان را براند نرگس از بستان چشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا دهد چشمم برای صحت چشمت زکوة</p></div>
<div class="m2"><p>نور چشم من پر از در کرده‌ام دامان چشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم بر چشم من سرگشته افکن تا تو را</p></div>
<div class="m2"><p>بهر دفع چشم بد گردم بلاگردان چشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم بر چشم از رقیب محتشم‌پوشان که هست</p></div>
<div class="m2"><p>چشم بر چشم رقیب انداختن نقصان چشم</p></div></div>