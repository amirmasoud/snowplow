---
title: >-
    شمارهٔ  ۴۴
---
# شمارهٔ  ۴۴

<div class="b" id="bn1"><div class="m1"><p>همچو شمعم هست شبها بی‌رخ آن آفتاب</p></div>
<div class="m2"><p>دیده گریان سینهٔ بریان تن گدازان دل کباب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسته شد از چار حد بر من در وصلش که هست</p></div>
<div class="m2"><p>دل غمین خاطر حزین تن در بلاجان در عذاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در زمین و آسمان دارند ز آب و تاب او</p></div>
<div class="m2"><p>آب شرم آئینه رو مهتاب خورشید اضطراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو کی گیرد به گلشن جای سروی کش بود</p></div>
<div class="m2"><p>پیرهن گل سرسمن رخ نسترن خط مشگناب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیره بختم آنقدر کز طالع من می‌شود</p></div>
<div class="m2"><p>نور ظلمت روز شب گوهر حجر دریا سراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون گرفتم دامنش مردم ز ناکامی که بود</p></div>
<div class="m2"><p>دست لرزان دل طپان من منفعل او در حجاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدعی از رشک بر در چون نمرد امشب که بود</p></div>
<div class="m2"><p>بزم دلکش باده بی غش یار سرخوش من خراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرمبادم کز گمانهای کجم آن سرور است</p></div>
<div class="m2"><p>سر گران لب پر گله گل رد عرق نرگس به خواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محتشم دارد بتی بی‌رحم کاندر کیش اوست</p></div>
<div class="m2"><p>رحم ظلم احسان سیاست مهر کین گرمی عتاب</p></div></div>