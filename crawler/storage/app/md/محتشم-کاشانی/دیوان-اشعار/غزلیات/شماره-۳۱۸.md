---
title: >-
    شمارهٔ  ۳۱۸
---
# شمارهٔ  ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>بزم کین آرا و در ساغر می بیداد ریز</p></div>
<div class="m2"><p>کامران بنشین و در کام من ناشاد ریز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ز من دارد دلت گردی پس از قتلم بسوز</p></div>
<div class="m2"><p>بعد از آن خاکسترم در ره گذار باد ریز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جرعه‌ای زان می که شیرین بهر خسرو کرده صاف</p></div>
<div class="m2"><p>ای فلک کاری کن و در کاسهٔ فرهاد ریز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز قسمت به اسحاب تربیت یارب که گفت</p></div>
<div class="m2"><p>کاین همه باران رد بر اهل استعداد ریز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دل آن بی رحم چون فرمان به خونریزت دهد</p></div>
<div class="m2"><p>زخم او بنما و خون از دیدهٔ جلاد ریز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای سپهر از بهر تاب آوردن این سلسله</p></div>
<div class="m2"><p>روبنای نو نه و طرح نوی بنیاد ریز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حرم گر پا نهی آید ندا کای آسمان</p></div>
<div class="m2"><p>خون صید این زمین در پای این صیاد ریز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خفته در پای گل آن سرو ای صبا در جنبش آ</p></div>
<div class="m2"><p>گل ز شاخ آهسته بیرون آر و بر شمشاد ریز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مس بود اکسیر را قابل نه آهن محتشم</p></div>
<div class="m2"><p>رو تو نقد خویش را در کوره حداد ریز</p></div></div>