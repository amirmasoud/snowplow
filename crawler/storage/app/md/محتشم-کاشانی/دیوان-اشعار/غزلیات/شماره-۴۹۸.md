---
title: >-
    شمارهٔ  ۴۹۸
---
# شمارهٔ  ۴۹۸

<div class="b" id="bn1"><div class="m1"><p>به دوستی خودم میکشی که رای من است این</p></div>
<div class="m2"><p>به خویش دشمنی کرده‌ام سزای من است این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گداختم ز جفا تا وفا به عهد تو کردم</p></div>
<div class="m2"><p>بلی نتیجهٔ عهد تو و فای من است این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به قول مدعیم میکشی و نیستی آگه</p></div>
<div class="m2"><p>که در غمی که منم عین مدعای من است این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وفا نگر که دم قتل من ز خیل سگانش</p></div>
<div class="m2"><p>یکی نکرد شفاعت که آشنای من است این</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجب نباشد اگر پا کشم ز مسند قربت</p></div>
<div class="m2"><p>تو آفتابی و من ذره‌ام چه جای من است این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم که گشته ز بی‌غیرتی مقیم در آن کو</p></div>
<div class="m2"><p>از آن مقام برانش که بی رضای من است این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر ز غم برهی محتشم دچار تو گردد</p></div>
<div class="m2"><p>بگو کمینه غلام گریز پای من است این</p></div></div>