---
title: >-
    شمارهٔ  ۵۳۶
---
# شمارهٔ  ۵۳۶

<div class="b" id="bn1"><div class="m1"><p>جلوهٔ آن حور پیکر خونم از دل ریخته</p></div>
<div class="m2"><p>بندهٔ آن صانعم کان پیکر از گل ریخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهر لیلی بین که اشگش بر سر راه وداع</p></div>
<div class="m2"><p>همچو باران بر سر مجنون ز محمل ریخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترک خونریزی مسافر گشته کز دنبال او</p></div>
<div class="m2"><p>خون دل‌ها بر زمین منزل به منزل ریخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون رنگینم که ریزان گشته از چشم پرآب</p></div>
<div class="m2"><p>گوئی از جوی گلوی مرغ بسمل ریخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غرفه‌ام در گوهر و در بس که چشم خون فشان</p></div>
<div class="m2"><p>از تک بحر دلم گوهر به ساحل ریخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش چشم ساحرت هاروت از شرمندگی</p></div>
<div class="m2"><p>نسخهٔ‌های سحر را در چاه بابل ریخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صحن میدان کرده رنگ آن خون که در هنگام قتل</p></div>
<div class="m2"><p>گریه‌های محتشم از چشم قاتل ریخته</p></div></div>