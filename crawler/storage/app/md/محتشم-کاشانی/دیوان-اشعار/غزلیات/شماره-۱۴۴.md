---
title: >-
    شمارهٔ  ۱۴۴
---
# شمارهٔ  ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>زهی طغیان حسنت بر شکیب کار من باعث</p></div>
<div class="m2"><p>ظهورت بر زوال عقل دعوی دار من باعث</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندانم از تو هر چند از ستم فرمائی آزارم</p></div>
<div class="m2"><p>که آن حسن ستم فرماست بر آزار من باعث</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو تا غایت نبودی خانمان ویران کن مردم</p></div>
<div class="m2"><p>تو را شد بر تطاول پستی دیوار من باعث</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز کس حرمت دوشم چه خود را دور میداری</p></div>
<div class="m2"><p>که ایمای تو شد بر جرات اغیار من باعث</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خدا خون جهانی از تو خواهد خواست چون کرده</p></div>
<div class="m2"><p>جهان را بر خرابی دیدهٔ خونبار من باعث</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر در عشوه خواهم کرد گم ضبط زبان تا کی</p></div>
<div class="m2"><p>شود لطف کمت بر رنجش بسیار من باعث</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سبک کردم عیار خویش از این غافل که خواهد شد</p></div>
<div class="m2"><p>بر استیلای نازش خفت مقدار من باعث</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گره بر رشتهٔ ذکر ملایک می‌تواند زد</p></div>
<div class="m2"><p>سر زلفت که شد بر بستن زنار من باعث</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گزیدم صد ره انگشت تحیر چون ز محرومی</p></div>
<div class="m2"><p>به زیر تیغ شد بر زخم او زنهار من باعث</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز ذوق امروز مردم حال غیر از وی چو پرسیدم</p></div>
<div class="m2"><p>که بر اعراض پنهانی شد استغفار من باعث</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غم او محتشم بستی در نطقم اگر گه</p></div>
<div class="m2"><p>نگشتی اقتضای طبع بر گفتار من باعث</p></div></div>