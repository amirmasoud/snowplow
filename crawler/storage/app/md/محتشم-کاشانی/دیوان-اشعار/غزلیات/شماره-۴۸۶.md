---
title: >-
    شمارهٔ  ۴۸۶
---
# شمارهٔ  ۴۸۶

<div class="b" id="bn1"><div class="m1"><p>به زیر لب سخنگویان گذشت آن دلربا از من</p></div>
<div class="m2"><p>گره گردیده حرفی در دل او گوئیا از من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبانش خامش از شرم ولبش در جنبش از خوبی</p></div>
<div class="m2"><p>نمی‌دانم چه در دل دارد آن کان حیا از من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جبین پرچین و دل پرکین سبک کام و گران تمکین</p></div>
<div class="m2"><p>ز پیشم رفت تا در خاطرش باشد چها از من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا هم راز چون با غیر دید و لب گزید آن بت</p></div>
<div class="m2"><p>ندانستم که پاس راز او می‌داشت یا از من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان بی‌اعتبارم پیش او کز بهر خونریزم</p></div>
<div class="m2"><p>کشد تیغ جفا گر بشنود نام وفا از من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو هم رازم به کس بیندشود دهشت بر او غالب</p></div>
<div class="m2"><p>دلش از راز داران نیست ایمن غالبا از من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دریا قوت را چون کرد پنهان این کمان ببردم</p></div>
<div class="m2"><p>که می‌ترسد ز رازش حرفی افتد برملا از من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نهانی می‌نمایندم بهم خاصان او گویا</p></div>
<div class="m2"><p>به آن بیگانه خو هم گفته حرف آشنا از من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دهد غماز را دشنام پیش محتشم یعنی</p></div>
<div class="m2"><p>تو هم باید دگر حرفی نگوئی هیچ جا از من</p></div></div>