---
title: >-
    شمارهٔ  ۳۱۳
---
# شمارهٔ  ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>ز عنبر آتش حسنت نکرده دود هنوز</p></div>
<div class="m2"><p>محل رخ ز می افروختن نبود هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گرد مشگ نیالوده دامن رخسار</p></div>
<div class="m2"><p>به باده بود لب آلودن تو زود هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که شد به می سبب آلایش وجود تو را</p></div>
<div class="m2"><p>نیامده گنهی از تو در وجود هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نموده رشحه‌کشیها نهالت از می ناب</p></div>
<div class="m2"><p>نکرده در چمن سرکشی نمود هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لبت که دوش برو کاسه بوسه زده است</p></div>
<div class="m2"><p>بود بدیدهٔ باریک بین کبود هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز پند محتشم افسوس کز طبیعت تو</p></div>
<div class="m2"><p>که کاست نشاء ذوق می و فزود هنوز</p></div></div>