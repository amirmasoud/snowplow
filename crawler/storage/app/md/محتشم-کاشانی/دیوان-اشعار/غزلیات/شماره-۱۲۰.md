---
title: >-
    شمارهٔ  ۱۲۰
---
# شمارهٔ  ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>گرچه بر رویم در لطف از توجه بازداشت</p></div>
<div class="m2"><p>تا توانست از درم بیرون به حکم نازداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جراتم با آن که بی‌دهشت به صحبت می‌دواند</p></div>
<div class="m2"><p>دور باش مجلس خاصم بر آن در بازداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بزم شد فانوس و جانان شمع و دل پروانه‌ای</p></div>
<div class="m2"><p>کز برون خد را بگرد شمع در پرواز داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل که در بزمش به حیلت دخل نتوانست کرد</p></div>
<div class="m2"><p>گریه بر خواننده عقل حیل پرداز داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد نصیب من که صید لاغرم اما ز دور</p></div>
<div class="m2"><p>در کمان هر تیر کان ترک شکارانداز داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر رخم محرومی صحبت در امید بست</p></div>
<div class="m2"><p>خاصه آن صحبت که وی با محرمان راز داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم کز قرب روز افزون تمام امید بود</p></div>
<div class="m2"><p>کی خبر زین عشق هجر انجام وصل آغاز داشت</p></div></div>