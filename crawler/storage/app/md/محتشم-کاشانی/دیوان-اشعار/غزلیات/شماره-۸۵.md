---
title: >-
    شمارهٔ  ۸۵
---
# شمارهٔ  ۸۵

<div class="b" id="bn1"><div class="m1"><p>کمر به کین تو ای دل چو یار جانی بست</p></div>
<div class="m2"><p>طمع مدار که دیگر کمر توانی بست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بزم وصل قدم چون نهم که عصمت او</p></div>
<div class="m2"><p>گشود دست و مرا پای کامرانی بست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دری که دیده بروی دلم گشود این بود</p></div>
<div class="m2"><p>که عشق آمد و درهای شادمانی بست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گز از خماردهم جان عجب مدار ای دل</p></div>
<div class="m2"><p>که ساقی از لب من آب زندگانی بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخ از دریچهٔ معنی نمود آن که به ناز</p></div>
<div class="m2"><p>میان حسن و نظر سدلن ترانی بست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکست ساغر دل را به صد ملامت و باز</p></div>
<div class="m2"><p>به دستیاری یک عشوهٔ نهائی بست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نیم معذرتی آن هم از زبان فریب</p></div>
<div class="m2"><p>در هزار شکایت ز نکته دانی بست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو گرد قصد نگه کار غیر ساخت نخست</p></div>
<div class="m2"><p>که چشم او به فریب از نگاهبانی بست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به عرض عشق نهان محتشم زبان چو گشود</p></div>
<div class="m2"><p>میانهٔ من و او راه همزبانی بست</p></div></div>