---
title: >-
    شمارهٔ  ۵۸۹
---
# شمارهٔ  ۵۸۹

<div class="b" id="bn1"><div class="m1"><p>این است که خوار و زارم از وی</p></div>
<div class="m2"><p>درهم شده کار و بارم از وی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این است که در جهان به صدرنگ</p></div>
<div class="m2"><p>گردیده خزان بهارم از وی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینست آن که امروز</p></div>
<div class="m2"><p>افسانهٔ روزگارم از وی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا پای حیات من نلغزد</p></div>
<div class="m2"><p>من دست هوس ندارم از وی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی که به دلبری میان بست</p></div>
<div class="m2"><p>شد دجلهٔ خون کنارم از وی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای ناصح عاقل آن کمر بین</p></div>
<div class="m2"><p>اینست که من نزارم از وی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در زیر قباش آن بدن بین</p></div>
<div class="m2"><p>اینست که زیر بارم از وی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن بند قبا که بسته پیکر</p></div>
<div class="m2"><p>اینست که بسته کارم از وی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن خال ببین بر آن زنخدان</p></div>
<div class="m2"><p>اینست که داغدارم از وی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن زلف ببین بر آن بناگوش</p></div>
<div class="m2"><p>اینست که بیقرارم از وی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن درج عقیق بین می‌آلود</p></div>
<div class="m2"><p>اینست که در خمارم از وی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن نرگس مست بین بلابار</p></div>
<div class="m2"><p>اینست که اشگبارم از وی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن ابرو بین به قابلی طاق</p></div>
<div class="m2"><p>اینست که سوگوارم از وی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن کاکل شانه کرده را باش</p></div>
<div class="m2"><p>اینست که دل فکارم از وی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حاصل چه عزیز محتشم اوست</p></div>
<div class="m2"><p>من ممنونم که خوارم از وی</p></div></div>