---
title: >-
    شمارهٔ  ۲۴
---
# شمارهٔ  ۲۴

<div class="b" id="bn1"><div class="m1"><p>جان بر لب و ز یار هزار آرزو مرا</p></div>
<div class="m2"><p>بگذار ای طبیب زمانی باو مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین تب چنان ره نفسم تنگ شد که هیچ</p></div>
<div class="m2"><p>جز آب تیغ او نرود در گلو مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن بلبلم که جلوهٔ آتش گل من است</p></div>
<div class="m2"><p>در دام آرزو نکشد رنگ و بو مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از طره دو تا به دو زنجیر بسته است</p></div>
<div class="m2"><p>چون شیر وحشی آن بت زنجیر مو مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوی بد است مائدهٔ حسن را نمک</p></div>
<div class="m2"><p>زین جاست حرص دیدن آن تندخو مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذرات من ز مهر تو خالی نمی‌شوند</p></div>
<div class="m2"><p>گر ذره ذره میکنی ای فتنه‌جو مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در عاشقی مرا چه گنه کافریدگار</p></div>
<div class="m2"><p>خود آفریده عاشق روی نکو مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اقبال محتشم که چو طبعش بلند بود</p></div>
<div class="m2"><p>افراخت سر به سجدهٔ آن خاک کو مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا آمدم به سجدهٔ سلمان جابری</p></div>
<div class="m2"><p>ناید به کس دگر سر همت فرو مرا</p></div></div>