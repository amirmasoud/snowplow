---
title: >-
    شمارهٔ  ۵۹۵
---
# شمارهٔ  ۵۹۵

<div class="b" id="bn1"><div class="m1"><p>به جائی امن آرامیده مرغی داشت ماوایی</p></div>
<div class="m2"><p>صدای شهپر شاهین برآمد ناگه از جائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقابی در رسید از اوج استیلا و پیش وی</p></div>
<div class="m2"><p>به جز تسلیم نتوانست صید ناتوانائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکارانداز صیادی برآمد تیغ کین بر کف</p></div>
<div class="m2"><p>فکند آشوب در وحشی شکاری بند برپائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به برج خویش ساکن بود ثابت کوکبی ناگه</p></div>
<div class="m2"><p>چو سیمایش به بحر اضطراب افکند سیمائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنی کز جا نجنبیدی ز آشوب قیامت هم</p></div>
<div class="m2"><p>قیام‌انگیز وی گردید فرقد و بالائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز گرد ره به تاراج دل افتادند چشمانش</p></div>
<div class="m2"><p>چنان کافتند غارت پیشگان درخوان یغمائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبانی داده‌اند از عشوه آن چشم سخنگو را</p></div>
<div class="m2"><p>که در گوش خرد صد حرف می‌گوید به ایمائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمین فرسایی از سجده‌های شکر واجب شد</p></div>
<div class="m2"><p>که سر در کلبهٔ من زد کله بر آسمان سائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پی عذر قدومت محتشم تا دم آخر</p></div>
<div class="m2"><p>بر آن در جبهه‌سائی آستان از سجده فرسائی</p></div></div>