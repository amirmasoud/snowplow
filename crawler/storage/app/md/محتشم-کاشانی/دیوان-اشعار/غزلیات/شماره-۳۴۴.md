---
title: >-
    شمارهٔ  ۳۴۴
---
# شمارهٔ  ۳۴۴

<div class="b" id="bn1"><div class="m1"><p>مهی که زینت حسنست گرمی خویش</p></div>
<div class="m2"><p>طپانچه بر رخ خورشید می‌زند رویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرنده را ز چرا باز می‌تواند داشت</p></div>
<div class="m2"><p>نگاه دلکش ناوک گشای آهویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار خنجر زهر آب داده نرگس او</p></div>
<div class="m2"><p>کشیده بهر دلیری که بنگرد سویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان ربود دل مرا که هیچ دیده ندید</p></div>
<div class="m2"><p>همین کدیایت محل غمزهٔ محل جویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز راه دیده به دل می‌رسد هزار پیام</p></div>
<div class="m2"><p>به نیم جنبشی از گوشه‌های ابرویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدنگ نیمکش غمزه‌اش نخورده هنوز</p></div>
<div class="m2"><p>به من چشانده فلک زور و دست و بازویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهفته کرده کمانی به زه که بی‌خبرند</p></div>
<div class="m2"><p>ز ناوک افکنی آن دو چشم جادویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خموشیش نه ز اعراض بود دی که نداد</p></div>
<div class="m2"><p>به لب مجال سخن غمزه سخنگویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هنوز محتشم آن ماه نارسیده ز راه</p></div>
<div class="m2"><p>بیا ببین که چه غوغاست بر سر کویش</p></div></div>