---
title: >-
    شمارهٔ  ۱۸۷
---
# شمارهٔ  ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>بهتر است از هرچه دهقان در چمن می‌پرورد</p></div>
<div class="m2"><p>آن چه آن نازک بدن در پیرهن می‌پرورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان دو زلف و عارضم پیوسته در حیرت کنون</p></div>
<div class="m2"><p>بیضهٔ خورشید را زاغ و زغن می‌پرورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نافه دارد بوئی از زلفت که بهر احترام</p></div>
<div class="m2"><p>ایزدش در ناف آهوی ختن می‌پرورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست شیرین را درین خمخانه از حسرت دریغ</p></div>
<div class="m2"><p>بادهٔ تلخی که بهر کوه کن می‌پرورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهره‌ای از دامنم خار است از آن گل پیرهن</p></div>
<div class="m2"><p>گرد خرمن بین که اندر گل سمن میپرورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌دهد از اشگ سرخم آب تیغ خویش را</p></div>
<div class="m2"><p>تشنهٔ خون مرا از خون من می‌پرورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق در هر آب و گل حالی دگر دارد از آن</p></div>
<div class="m2"><p>محتشم جان می‌گدازد غیر تن می‌پرورد</p></div></div>