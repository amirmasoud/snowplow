---
title: >-
    شمارهٔ  ۴۰۲
---
# شمارهٔ  ۴۰۲

<div class="b" id="bn1"><div class="m1"><p>به مجلس بحث از آن خصمانه اغیار می‌کردم</p></div>
<div class="m2"><p>که جانب داری فهم از ادای یار می‌کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بختم با حریفان کار مشکل شد که پی در پی</p></div>
<div class="m2"><p>به تعلیم اشارات نهانش کار می‌کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان در بحث با اغیار و دل در مشورت با او</p></div>
<div class="m2"><p>من از دل بی‌خبر نظارهٔ دیدار می‌کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن می‌گفتم اندر بزم با پهلونشینانش</p></div>
<div class="m2"><p>نظر را در میان مشغول آن رخسار می‌کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوید بزم خاصم دوش باعث بود در مجلس</p></div>
<div class="m2"><p>که بهر زود رفتن کوشش بسیار می‌کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رقیبی بود در بیداری شبگردیم با او</p></div>
<div class="m2"><p>که پی گم کرده امشب سیر با اغیار می‌کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهان می‌خواستم چون از حریفان لطف او با خود</p></div>
<div class="m2"><p>بهر یک حرفی از بی‌لطفیش اظهار می‌کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در افشای جدل با مدعی از مصلحت بینی</p></div>
<div class="m2"><p>به ظاهر گفتگوئی نیز با دل‌دار می‌کردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی‌شد محتشم گر دوست امشب هم زبان من</p></div>
<div class="m2"><p>میان دشمنان کی جرات این مقدار می‌کردم</p></div></div>