---
title: >-
    شمارهٔ  ۴۸۰
---
# شمارهٔ  ۴۸۰

<div class="b" id="bn1"><div class="m1"><p>چون نمودی رخ به من یک لحظه بدخوئی مکن</p></div>
<div class="m2"><p>شربت دیدار شیرین به ترش روئی مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌کنم گر بیخ عیش خویش میگوئی بکن</p></div>
<div class="m2"><p>می‌کنم گر قصد جان خویش میگوئی مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با بدان نیکی ندارد حاصلی غیر از بدی</p></div>
<div class="m2"><p>گر بخود بد نیستی با غیر نیکوئی مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غمزه‌ات محتاج افسون نیست در تسخیر خلق</p></div>
<div class="m2"><p>صاحب اعجاز را تعلیم جادوئی مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من که خود کم کرده‌ام دل در رهت دادم مده</p></div>
<div class="m2"><p>عاشق بیداد را خوش دل به دلجوئی مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر درین دیوان گناه ما خطای عاشقی است</p></div>
<div class="m2"><p>گو کسی در نامهٔ ما این خطا شوئی مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترک بد خوئی کن اما با گدای پرهوس</p></div>
<div class="m2"><p>گرچه باشد محتشم زنهار خوش خوئی مکن</p></div></div>