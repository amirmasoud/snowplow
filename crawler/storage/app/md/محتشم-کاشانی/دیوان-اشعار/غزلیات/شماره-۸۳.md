---
title: >-
    شمارهٔ  ۸۳
---
# شمارهٔ  ۸۳

<div class="b" id="bn1"><div class="m1"><p>از عاشقان حوالی آن خانه پر شده است</p></div>
<div class="m2"><p>دارالشفای عشق ز دیوانه پر شده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خود نگشته است به کس آشنا دلی</p></div>
<div class="m2"><p>راه وثاقش از پی بیگانه پر شده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاره به جام خانه چشمم فکند عکس</p></div>
<div class="m2"><p>این خانه از پری چو پری خانه پر شده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جرعه‌ای که ریخته ساقی به جام ما</p></div>
<div class="m2"><p>گش فلک ز نعرهٔ مستانه پر شده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رگهای جانم از گرهٔ غم به ذکر هجر</p></div>
<div class="m2"><p>چون رشتهای سجه صد دانه پر شده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشاق را به دور تو از بادهٔ حیات</p></div>
<div class="m2"><p>قالب تهی فتاده و پیمانه پر شده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردد مگر به وصف تو مقبول اهل طبع</p></div>
<div class="m2"><p>دیوان محتشم که ز افسانه پر شده است</p></div></div>