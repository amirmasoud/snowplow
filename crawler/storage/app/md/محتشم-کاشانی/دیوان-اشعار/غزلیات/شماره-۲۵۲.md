---
title: >-
    شمارهٔ  ۲۵۲
---
# شمارهٔ  ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>اول منزل عشقست بیابان فنا</p></div>
<div class="m2"><p>عاشقی کو که درین ره دو سه منزل برود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتن ناقه گهی جانب مجنون نیکوست</p></div>
<div class="m2"><p>که به تحریک نشینندهٔ محمل برود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل را بر لب آن چاه ذقن پا لغزد</p></div>
<div class="m2"><p>دل به آن ناحیه جهلست که عاقل برود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارد آن غمزه کمانی که به چشم نگران</p></div>
<div class="m2"><p>ناوکی سردهد آهسته که تا دل برود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارم از خوف و رجا کشتی سر گردانی</p></div>
<div class="m2"><p>که نه در ورطه بماند نه به ساحل برود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق چون کهنه شود محو نگردد به فراق</p></div>
<div class="m2"><p>نخل از جا نرود ریشه چو در گل برود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابر رحمت چو ترشح کند امید کزان</p></div>
<div class="m2"><p>رقم قتل من از نامهٔ قاتل برود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیر پروای کسی بشنو و تاخیر مکن</p></div>
<div class="m2"><p>تا به آن مرتبه تاخیر به ساحل برود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر کنی قصد قتالی و نیالائی تیغ</p></div>
<div class="m2"><p>خون ز بسمل گه صد ناشده بسمل برود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>محتشم لال شود طوطی طبعم می‌گفت</p></div>
<div class="m2"><p>اگر آن آینه رویم ز مقابل برود</p></div></div>