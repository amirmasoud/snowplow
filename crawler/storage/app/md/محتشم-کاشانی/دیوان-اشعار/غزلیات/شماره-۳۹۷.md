---
title: >-
    شمارهٔ  ۳۹۷
---
# شمارهٔ  ۳۹۷

<div class="b" id="bn1"><div class="m1"><p>ز خاک کوی تو گریان سفر گزیدم و رفتم</p></div>
<div class="m2"><p>ز گریه رخت به غرقاب خون کشیدم ورفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدم به زمین ریخت از دو شیشهٔ دیده</p></div>
<div class="m2"><p>گلاب آن گل حسرت که از تو چیدم و رفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز نخل تفرقه خیزت که داد بر به رقیبان</p></div>
<div class="m2"><p>علاقه دل و پیوند جان بردم و رفتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو غیر چید گل وصلت از مساهله من</p></div>
<div class="m2"><p>چو خار در جگر خویشتن خلیدم و رفتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درون پرده صبرم ز حد چو رفت تحمل</p></div>
<div class="m2"><p>ز پاس دامن آن پرده بر دریدم و رفتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخ امید به عهدت ز عاقبت نگریها</p></div>
<div class="m2"><p>سیه در آینهٔ بخت خویش دیدم و رفتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به پند دیدهٔ صحبت پسند کار نکردم</p></div>
<div class="m2"><p>نصیحت دل عزلت گزین شنیدم و رفتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا لقب کن ازین پس سگ رمیده ز آهو</p></div>
<div class="m2"><p>کز آهوئی چو تو با صد هوس رمیدم و رفتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکیب را چو نیامد ز پس نوید امیدی</p></div>
<div class="m2"><p>به شرح محتشم پیش بین رسیدم و رفتم</p></div></div>