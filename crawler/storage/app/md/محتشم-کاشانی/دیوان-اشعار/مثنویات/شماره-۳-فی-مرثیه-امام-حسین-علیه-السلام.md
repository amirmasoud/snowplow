---
title: >-
    شمارهٔ ۳ - فی مرثیه امام حسین علیه‌السلام
---
# شمارهٔ ۳ - فی مرثیه امام حسین علیه‌السلام

<div class="b" id="bn1"><div class="m1"><p>به نال ای دل که دیگر ماتم آمد</p></div>
<div class="m2"><p>بگری ای دیده ایام غم آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل غم سرزد از باغ مصیبت</p></div>
<div class="m2"><p>جهان را تازه شد داغ مصیبت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان گردید از ماتم دگرگون</p></div>
<div class="m2"><p>لباس تعزیت پوشیده گردون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز باغ غصه کوه از پا فتاده</p></div>
<div class="m2"><p>زمین را لرزه بر اعضا فتاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک تیغ ملامت بر کشیده</p></div>
<div class="m2"><p>ز ماه نو الف بر سر شیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین غم آفتاب از قصر افلاک</p></div>
<div class="m2"><p>فکنده خویش را چون سایه بر خاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عروس مه گسسته موی خود را</p></div>
<div class="m2"><p>خراشیده به ناخن روی خود را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خروش بحر از گردون گذشته</p></div>
<div class="m2"><p>سرشک ابر از جیحون گذشته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو نیز ای دل چو ابر نوبهاری</p></div>
<div class="m2"><p>به بار از دیده هر اشگی که داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که روز ماتم آل رسول است</p></div>
<div class="m2"><p>عزای گلبن باغ بتول است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عزای سید دنیا و دین است</p></div>
<div class="m2"><p>عزای سبط خیرالمرسلین است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عزای شاه مظلومان حسین است</p></div>
<div class="m2"><p>که ذاتش عین نور و نور عین است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دمی کز دست چرخ فتنه پرداز</p></div>
<div class="m2"><p>ز پا افتاد آن سرو سرافراز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غبار از عرصهٔ غبرا برآمد</p></div>
<div class="m2"><p>غریو از گنبد خضرا برآمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ملایک بی‌خود از گردون فتادند</p></div>
<div class="m2"><p>میان کشتگان در خون فتادند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مسلمانان خروش از جان برآرید</p></div>
<div class="m2"><p>محبان از جگر افغان برآرید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>درین ماتم بسوز و درد باشید</p></div>
<div class="m2"><p>به اشگ سرخ و رنگ زرد باشید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسان غنچه دلها چاک سازید</p></div>
<div class="m2"><p>چو نرگس دیده‌ها نمناک سازید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز خون دیده در جیحون نشینید</p></div>
<div class="m2"><p>چو شاخ ارغوان در خون نشینید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به ماتم بیخ عیش از جان برآرید</p></div>
<div class="m2"><p>به زاری تخم غم در دل بکارید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که در دل این زمان تخم ملامت</p></div>
<div class="m2"><p>برشادی دهد روز قیامت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خداوندا به حق آل حیدر</p></div>
<div class="m2"><p>به حق عترت پاک پیامبر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که سوی محتشم چشم عطا کن</p></div>
<div class="m2"><p>شفیعش را شهید کربلا کن</p></div></div>