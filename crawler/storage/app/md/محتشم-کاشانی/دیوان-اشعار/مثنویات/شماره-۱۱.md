---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>سخن طی می‌کنم ناگاه در خواب</p></div>
<div class="m2"><p>در آن بی‌گه که در جو خفته بود آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گوش آمد صدایی در چنانم</p></div>
<div class="m2"><p>که کرد از هزیمت مرغ جانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان برخاستم از جا مشوش</p></div>
<div class="m2"><p>که برخیزد سپند از روی آتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان بیرون دویدم بیخودانه</p></div>
<div class="m2"><p>که خود را ساختم گم در میانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من درمانده کز بیرون این در</p></div>
<div class="m2"><p>به آن صیاد جان بودم گمان بر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شست شوق تیری خورده بودم</p></div>
<div class="m2"><p>که تا در می‌گشودم مرده بودم</p></div></div>