---
title: >-
    شمارهٔ ۲ - وله فی‌المثنوی
---
# شمارهٔ ۲ - وله فی‌المثنوی

<div class="b" id="bn1"><div class="m1"><p>بحمدالله کز الطاف الهی</p></div>
<div class="m2"><p>مزین شد دگر اورنگ شاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنو کوس بشارت کوفت گردون</p></div>
<div class="m2"><p>در استقلال نواب همایون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منادی زن برای سجدهٔ عام</p></div>
<div class="m2"><p>گران کرد از منادی گوش ایام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که طالع گشت خورشید جهان‌تاب</p></div>
<div class="m2"><p>جهان بگشود چشم خفته از خواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشست از نو درین کاخ مخیم</p></div>
<div class="m2"><p>به سالاری جهان سالار اعظم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمین از آسمان شد تهنیت جو</p></div>
<div class="m2"><p>زبان آسمان شد تهنیت‌گو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دم و پشت کمان فتنه شد نرم</p></div>
<div class="m2"><p>مبارکباد را بازار شد گرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زبان هرکه می‌جنبید در کام</p></div>
<div class="m2"><p>به سامع نکته‌ای می‌کرد اعلام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیان هرکه حرف آغاز می‌کرد</p></div>
<div class="m2"><p>دری ز ابواب دعوی باز می‌کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قضا می‌گفت من امداد کردم</p></div>
<div class="m2"><p>که عالم را ز نو آباد کردم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فلک می‌گفت بود از پرتو من</p></div>
<div class="m2"><p>که دیگر شد چراغ دهر روشن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ملک می‌گفت از تسبیح من بود</p></div>
<div class="m2"><p>که از کار جهان این عقده بگشود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درین مدت شبی بگذشت بر کس</p></div>
<div class="m2"><p>کزین گفت و شنو یک دم کند بس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا هم خورد حرفی چند بر گوش</p></div>
<div class="m2"><p>که می‌برد استماع آن ز دل هوش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز لفظ منهیان عالم غیب</p></div>
<div class="m2"><p>ز گفت آگهان سر لاریب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی زان حرفهای راست تعبیر</p></div>
<div class="m2"><p>قلم می‌آورد در سلک تحریر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شبی روشن به نور مشعل بدر</p></div>
<div class="m2"><p>ز فیاض قدر با لیلة القدر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درو وحشت به دامن پا کشیده</p></div>
<div class="m2"><p>ز راحت آب در جو آرمیده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من بی دل که از خوابم ملال است</p></div>
<div class="m2"><p>دلم ماوای سلطان خیال است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز ذوق صحت شاه جهاندار</p></div>
<div class="m2"><p>نه چشمم خفته بود آن شب نه بیدار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درین اندیشه بودم کایزد پاک</p></div>
<div class="m2"><p>چه نیکو داشت پاس خطهٔ خاک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چه ملکی را ز نو دارالامان کرد</p></div>
<div class="m2"><p>چه جانی در تن خلق جهان کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه شمعی را به محض قدرت افروخت</p></div>
<div class="m2"><p>که خصم از پرتوش پروانه‌وش سوخت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چه شاهی را دگر کرسی نشین ساخت</p></div>
<div class="m2"><p>که عزمش باره بر چرخ برین تاخت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز بس کاین ذوق می‌برد از دلم هوش</p></div>
<div class="m2"><p>زبان نکته سنجم بود خاموش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دل اما داستانی گوش می‌کرد</p></div>
<div class="m2"><p>که از کیفیتم مدهوش می‌کرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زبان حال گوئی از سر سوز</p></div>
<div class="m2"><p>ز آغاز شب این افسانه تا روز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز بلقیس جهان می‌کرد تقریر</p></div>
<div class="m2"><p>به جمشید جوانبخت جهانگیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که ای شاه سریر کامرانی</p></div>
<div class="m2"><p>سزاوار بقای جاودانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو آن شمع جهانتابی که یک یا چند</p></div>
<div class="m2"><p>جمالت بوده بر مردم تتق بند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>من آن پروانهٔ شب زنده‌دارم</p></div>
<div class="m2"><p>که پاس شمع دولت بوده کارم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که افسون خوانده‌ام بر پیکر شاه</p></div>
<div class="m2"><p>گهی گردیده‌ام گرد سر شاه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گذشته پرمهی از غره تا سلخ</p></div>
<div class="m2"><p>که بر خود خواب شیرین کرده‌ام تلخ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کشک دارندگان شب نخفته</p></div>
<div class="m2"><p>پرستاران ترک خواب گفته</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یکی را زین الم میسوخت دامن</p></div>
<div class="m2"><p>یکی را دل یکی را خرمن تن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ولی من بودم ای شاه جهانبان</p></div>
<div class="m2"><p>که هم تن هم دلم میسوخت هم جان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز دل بازان جانباز وفادار</p></div>
<div class="m2"><p>به گرد پیکرت پروانهٔ کردار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بسی پر میزدند ای شمع سرکش</p></div>
<div class="m2"><p>ولی من میزدم خود را بر آتش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>غم وردت سراسر زان من بود</p></div>
<div class="m2"><p>بلاگردان جانت جان من بود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مرا دل بود از بهر تو در بند</p></div>
<div class="m2"><p>مرا جان بود با جان تو پیوند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر عضوی ز اعضای شریفت</p></div>
<div class="m2"><p>وگر جزوی ز اجزای لطیفت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سر موئی ز درد آزرده میشد</p></div>
<div class="m2"><p>گل امید من پژمرده میشد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>وگر تخفیفی از آزار می‌یافت</p></div>
<div class="m2"><p>دلم یک دم ز غم زنهار می‌یافت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که آن حالت که شاه به جرو برداشت</p></div>
<div class="m2"><p>مرا در آب و آتش بیشتر داشت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>رضا بودم که هستی بخش عالم</p></div>
<div class="m2"><p>به عمر شاه عمر من کند ضم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زبانم بس که مشغول دعا بود</p></div>
<div class="m2"><p>نمی‌گفت‌م گرم صد مدعا بود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همینم بود روز و شب مناجات</p></div>
<div class="m2"><p>نهان از خلق با قاضی حاجات</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>که ای دانای حکمت‌های مکنوز</p></div>
<div class="m2"><p>هزاران بوعلی را حکمت‌آموز</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خداوند رحیم و بنده پرور</p></div>
<div class="m2"><p>توان بخش توانای توانگر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>حفیظ یونس اندر بطن ماهی</p></div>
<div class="m2"><p>به لطف بی‌دریغ پادشاهی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نگهدار خلیل از نار نمرود</p></div>
<div class="m2"><p>به مخفی رشحه‌های لجهٔ جود</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>برون آرنده ایوب از رنج</p></div>
<div class="m2"><p>چنان کز چنگ چندین اژدها گنج</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به نوعی کاین شهان را داشتی پاس</p></div>
<div class="m2"><p>به حکمت‌های کس ناکرده احساس</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>برین مهر سپهر سروری نیز</p></div>
<div class="m2"><p>برین شاه سریر داوری نیز</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز روی مرحمت شو سایه گستر</p></div>
<div class="m2"><p>چو نخل‌تر برانگیزش ز بستر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به صحت کن به دل بیماریش را</p></div>
<div class="m2"><p>مؤید دار گیتی داریش را</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>فلک را آن چنان کن پاسبانش</p></div>
<div class="m2"><p>که دارد پاس تا آخر زمانش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نصیب او حیات همین اوست</p></div>
<div class="m2"><p>چراغ دودهٔ انسان همین اوست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کسی در فکر درویشان جز او نیست</p></div>
<div class="m2"><p>خبر دار از دل ایشان جز او نیست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نه‌تنها هاتف این افسانه می‌گفت</p></div>
<div class="m2"><p>که این در هرکه درکی داشت میسفت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مرا هم هرچه امشب بر زبان بود</p></div>
<div class="m2"><p>به گوشم آن چه می‌آمد همان بود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>الهی تا بقا باشد جهان را</p></div>
<div class="m2"><p>بقا ده این شه صاحبقران را</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>که دیگر دهر ار ارحام واصلاب</p></div>
<div class="m2"><p>چنین ذاتی نخواهد دید در خواب</p></div></div>