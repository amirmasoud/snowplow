---
title: >-
    قصیدهٔ شمارهٔ ۶۸ - وله ایضا
---
# قصیدهٔ شمارهٔ ۶۸ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>به که درین گفته معجز بیان</p></div>
<div class="m2"><p>درج بود نام خدای جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکر که قیوم کریم احد</p></div>
<div class="m2"><p>جانده پوزش طلب و جانستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پایهٔ ده عقده ز گیتی گشای</p></div>
<div class="m2"><p>پادشه ملک به حارس رسان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرد اگر حکم که شاه سلیم</p></div>
<div class="m2"><p>ماه فلک فطرت جم پاسبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بار جهان بست و باقدام این</p></div>
<div class="m2"><p>دل ز بقا کند و ز آثار آن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خورد بهم حد جهانی ولی</p></div>
<div class="m2"><p>شد به دمی تازه زمین و زمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از که ز شاهی که به اقبال اوست</p></div>
<div class="m2"><p>فتنهٔ ایام ز مردم نهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاهسواری که ز شاهان بود</p></div>
<div class="m2"><p>امجد و اشجع به کمال و توان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیر مصافی که به هیجا در آب</p></div>
<div class="m2"><p>جسته مبارز ز بنان سنان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کوه شکوهی که ز تمکین نهاد</p></div>
<div class="m2"><p>بزم تعین به اساس کران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صاحب عالم که ازو برقرار</p></div>
<div class="m2"><p>مانده رفاهیت کون و مکان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باد بر این طرفه بنا از نشاط</p></div>
<div class="m2"><p>تا ابد این بانی صاحبقران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عزلت ده روزه او را بلی</p></div>
<div class="m2"><p>باد به دل خسروی جاودان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هست محال آن که ببندد به فکر</p></div>
<div class="m2"><p>آدمی این عقد درر عقده‌سان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای ملک ستان کبیر</p></div>
<div class="m2"><p>وی شه کامل نسق کامران</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرچه به لوح دل دانای خود</p></div>
<div class="m2"><p>زد رقم مدت امن و امان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیش ز هر پادشهی کوس هم</p></div>
<div class="m2"><p>کوفت در اصلاح مهم جان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باد ازو دور به دوران که هست</p></div>
<div class="m2"><p>پادشه و شیردل و نوجوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می نگرد دل چو به هر مصرعی</p></div>
<div class="m2"><p>کامده یک فکر از آن داستان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هست بدانسان که به رمز و حساب</p></div>
<div class="m2"><p>فهم شود سال جلوسش از آن</p></div></div>