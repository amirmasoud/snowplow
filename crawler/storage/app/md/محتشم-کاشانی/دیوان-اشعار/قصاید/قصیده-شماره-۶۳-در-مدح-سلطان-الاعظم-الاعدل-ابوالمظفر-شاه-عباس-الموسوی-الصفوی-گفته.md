---
title: >-
    قصیدهٔ شمارهٔ ۶۳ - در مدح سلطان‌الاعظم الاعدل ابوالمظفر شاه عباس الموسوی الصفوی گفته
---
# قصیدهٔ شمارهٔ ۶۳ - در مدح سلطان‌الاعظم الاعدل ابوالمظفر شاه عباس الموسوی الصفوی گفته

<div class="b" id="bn1"><div class="m1"><p>شد عراق آباد روزی کز خراسان شد روان</p></div>
<div class="m2"><p>دوش بر دوش ظفر رایات شاه نوجوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاسبان ملت و دین قهرمان ماء و طین</p></div>
<div class="m2"><p>آسمان عز و تمکین پادشاه انس و جان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صورت لطف خدا کهف‌الوری نورالهدی</p></div>
<div class="m2"><p>اختر بیضا ضیا چشم جهان بین جهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ضابط قانون دولت حافظ ملک و ملل</p></div>
<div class="m2"><p>حارث ایران و توران باعث امن و امان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاه عباس جهانگیر آفتاب بی‌زوال</p></div>
<div class="m2"><p>فارس رخش خلافت وارث طهماسب خان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن که گرد فتنه شد بر باد چون ایزد سپرد</p></div>
<div class="m2"><p>بادپای کامرانی را به دست او عنان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وانکه پای شخص آفت شد سبک‌رو در فرار</p></div>
<div class="m2"><p>چون رکاب پادشاهی شد ز پای او گران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از ازل گردید در تسخیر اقطاع زمین</p></div>
<div class="m2"><p>نصرت او را علی موسی جعفر ضمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مهر هر صبح از شعاع خود شود جاروب بند</p></div>
<div class="m2"><p>بهر آن فرزانه فراش ره صاحب زمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیضهٔ مرغ جلالش قدر بیضا بشکند</p></div>
<div class="m2"><p>گر تواند یافت گنجایش درین هفت آسمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پشهٔ او لنگر اندازد اگر بر پشت پیل</p></div>
<div class="m2"><p>دست و پای پیل یابد کوتهی از استخوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر سر این هفت چرخ آرد فرو گردست و تیغ</p></div>
<div class="m2"><p>در عدد گردد زمین هم چارده چون آسمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صد دو پیکر در زمین در هر قدم پیدا شود</p></div>
<div class="m2"><p>روز هیجا گر کند شمشیر خود را امتحان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زو روی گوی زمین را یک جهان دور افکند</p></div>
<div class="m2"><p>گر زمین ز آهن ز مغناطیسی باشد صولجان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی‌رضای او که آسیبی نمی‌دارد روا</p></div>
<div class="m2"><p>نیست چون ممکن که تیر آفت آید بر نشان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون خدنگ ناز خوبان تغافل پیشه است</p></div>
<div class="m2"><p>در زمانش فتنه هر ناوک که دارد در کمان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خاک ریزد بر سر عدل خود از شرمندگی</p></div>
<div class="m2"><p>گر ز خاک امروز سر بیرون کند نوشیروان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در زمان امر و نهی جاریش نبود محال</p></div>
<div class="m2"><p>رجعت آب معلق گشته سوی ناودان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کاشکی در فرش بودی عرش علوی تا بود</p></div>
<div class="m2"><p>پادشاه این چنین را بارگاهی آن چنان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سهو کردم جای او بالاتر از عرشست و نیست</p></div>
<div class="m2"><p>زان طرف سفلی مکان بندگانش لامکان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از عروج پاسبان بر بام قصر و منظرش</p></div>
<div class="m2"><p>تارک عرش است منت کش ز پای نردبان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خوش جهانی خوش زبانی خوش جهانداریست این</p></div>
<div class="m2"><p>کز پی امنیت عالم بماند جاودان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای دل پرشوق کز تعجیل حال کرده‌ای</p></div>
<div class="m2"><p>کلک چوبین پای را در وادی مدحش روان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>باش تا خود صور اسرافیل عدلش بردمد</p></div>
<div class="m2"><p>کشتگان ظلم بردارند سر زین خاکدان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باش تا این شوکت سرکوب یک سر بشکند</p></div>
<div class="m2"><p>بیضه‌های سرکشی را در کلاه سرکشان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>باش تا زین دولت بیدار برخیزد دگر</p></div>
<div class="m2"><p>دولت طهماسب شاهی را سر از خواب گران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>باش تا ایام گلها بشکفاند زین بهار</p></div>
<div class="m2"><p>واندرین بستان پدید آید بهار بی‌خزان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باش تا دوران شجرها بردماند زین چمن</p></div>
<div class="m2"><p>کز بلندی سایه اندازند بر باغ جنان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>باش تا شاهان برای خونبهای خویشتن</p></div>
<div class="m2"><p>مال از روم آورند و باج از هندوستان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>باش تا بر ظالم اجرای سیاست چون شود</p></div>
<div class="m2"><p>عدل گوید القتال و ظلم گوید الامان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>باش تا بهر وفور جیش و جمعیت رسد</p></div>
<div class="m2"><p>از دیار استمالت کاروان درکاروان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>باش تا باران ابر در فشان رحمتش</p></div>
<div class="m2"><p>در گوهر گیرد جهان را قیروان تا قیروان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>باش تا ازرفعت قدر و علوشان شوند</p></div>
<div class="m2"><p>نقطه‌های قاف اقبال بلندش فرقدان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>باش تا دانا و نادان را کند از هم جدا</p></div>
<div class="m2"><p>موشکافی‌های این مردم شناس نکته‌دان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از شهان معنی و صورت جلوس هفت شاه</p></div>
<div class="m2"><p>بر سریر کامکاری شد در این دولت عنان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پادشاه اولین سلطان صفی که آوازه‌اش</p></div>
<div class="m2"><p>با و جود ترک دنیا بر گذشت از آسمان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شاه ثانی شاه حیدر کاو هم از همت نکرد</p></div>
<div class="m2"><p>میل دنیا با وجود قدر ذات و عظم شان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شاه ثالث شاه اسمعیل دین پرور که داد</p></div>
<div class="m2"><p>مذهب اثنا عشر را او رواج اندر جهان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شاه رابع پادشاه بحر و بر طهماسب شاه</p></div>
<div class="m2"><p>آن که آمد با زمانش توامان امن و مان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شاه خامس شاه اسمعیل ثانی کان چه کرد</p></div>
<div class="m2"><p>قاصر است از شرح آن تاریخ گویان را زبان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شاه سادس بعد از آن سلطان محمد پادشاه</p></div>
<div class="m2"><p>کز وراثت بر سریر خسروی شد کامران</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شاه سابع شاه عباس آفتاب شرق و غرب</p></div>
<div class="m2"><p>انتخاب دوده آدم چراغ دودمان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>می‌شد ار سابع به یک گردش چو عباس آشکار</p></div>
<div class="m2"><p>گشت او سابع نه حمزه خسرو جنت مکان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>قصه کوته چون ز صنع صانع لفظ آفرین</p></div>
<div class="m2"><p>سابع و عباس را بود این تناسب در میان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در حروف حمزه حرفی نیز در سابع نبود</p></div>
<div class="m2"><p>ز اقتضای حکمت و آثار اسرار نهان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>این شه روی زمین شد و آن شه زیر زمین</p></div>
<div class="m2"><p>قاسم ابن قادر جان ده قدیر جان ستان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از دو شاخ یک درخت ار باغبان برد یکی</p></div>
<div class="m2"><p>شاخ دیگر از فزونی سر کشد بر آسمان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>عمرد خود افزود از آن بر عمر این نصرت قرین</p></div>
<div class="m2"><p>آن که می‌خواندند خلقش حمزهٔ صاحبقران</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تا به این پیوند از عمر طبیعی بگذرد</p></div>
<div class="m2"><p>وین طبیعت خاص او سازند و این طول زمان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کاش انسان طیروش بال و پری هم داشتی</p></div>
<div class="m2"><p>تا گه و بیگه بدی گرد سر او پر زنان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>من که پای ناروانم زین سعادت مانع است</p></div>
<div class="m2"><p>کز تردد ذره‌وش یابم به خورشید اقتران</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از پی اقبال سر مد قبلهٔ خود کرده‌ام</p></div>
<div class="m2"><p>از سجود دور آن آستان را کعبه‌سان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بهر انشای ثنایش از خدا دارم امید</p></div>
<div class="m2"><p>عمر نوح و طبع خسرو نظم در طی لسان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تا بود کز صدهزار اندر بیان آرم یکی</p></div>
<div class="m2"><p>وان گه از رویش برانگیزم هزاران داستان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>پادشاها گرچه در پای سریر سلطنت</p></div>
<div class="m2"><p>هست در مدحت هزاران شاعر روشن‌روان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>فکر جمعی چون ستوران سواری گرم رو</p></div>
<div class="m2"><p>هم سمین اندر جوارح هم سمین اندر نشان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>طبع جمعی چون جمل‌های قطاری راست رو</p></div>
<div class="m2"><p>وز روانی سبعهٔ سیاره را در پی روان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>داری اما بنده افتاده از پائی که هست</p></div>
<div class="m2"><p>در رکاب شخص طبعش خسرو سیارگان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دوش شاهان سخن کز طیلسان پر زیب گشت</p></div>
<div class="m2"><p>از عنانش می‌کشد صد منت از برگستوان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گر درخت نظمش از مشرق برون آید شود</p></div>
<div class="m2"><p>خلق مغرب را پر آب از میوه‌های او دهان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>لیک از بی‌امتیازی‌های گردون تاکنون</p></div>
<div class="m2"><p>بوده است از خلق منت کش برای آب و آن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دارد امید این زمان کز امتیاز پادشاه</p></div>
<div class="m2"><p>در جهان آثار طبعش بیش ازین بود نهان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گر بود نظمش متین سازند ثبت اندر متون</p></div>
<div class="m2"><p>و ربود حشو از حواشی هم کشندش بر کران</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>محتشم هرچند میدان سخن را نیست پهن</p></div>
<div class="m2"><p>رخش قدرت بیش ازین در عرصه جرات مران</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تا به شاهان جهانگیر ایزد از احسان دهد</p></div>
<div class="m2"><p>ملک موروثی و دیگر ملک‌ها در تحت آن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>شغل شه فتح ممالک باد لیک اول کند</p></div>
<div class="m2"><p>فتح ملک روم بعد از فتح آذربایجان</p></div></div>