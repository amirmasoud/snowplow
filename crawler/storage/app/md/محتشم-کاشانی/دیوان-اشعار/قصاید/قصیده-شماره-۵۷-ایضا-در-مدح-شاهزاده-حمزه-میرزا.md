---
title: >-
    قصیدهٔ شمارهٔ ۵۷ - ایضا در مدح شاهزاده حمزه میرزا
---
# قصیدهٔ شمارهٔ ۵۷ - ایضا در مدح شاهزاده حمزه میرزا

<div class="b" id="bn1"><div class="m1"><p>بود به چنگ درنگ جیب مهم جهان</p></div>
<div class="m2"><p>تا به میان زد قضا دامن آخر زمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در طبقات ملوک پادشهی برگزید</p></div>
<div class="m2"><p>تیغ زن و صف‌شکن شیردل و نوجوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوانده ز آیندگی خطبهٔ پایندگی</p></div>
<div class="m2"><p>بسته ز پایندگی راه بر آیندگان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خسرو مهدی ظهور کز نصفت گستری</p></div>
<div class="m2"><p>ریشه دجال ظلم کند ازین خاکدان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پادشه نامدار کز ازل از بخت داشت</p></div>
<div class="m2"><p>منت هم نامیش حمزهٔ صاحبقران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن که در آغاز عمر گشت به تایید حق</p></div>
<div class="m2"><p>ملک و ملل را حفیظ امن و امان را ضمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرش نگارنده‌اش چهرهٔ حور پری</p></div>
<div class="m2"><p>سده فشارنده‌اش جبههٔ خاقان و خان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساقی بزمش به بذل تاج به فغفور بخش</p></div>
<div class="m2"><p>صاحب قصرش به حکم باج ز قیصرستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وانکه چو شد دهر را واسطهٔ دفع شر</p></div>
<div class="m2"><p>گشت قوی خلق را رابطهٔ جسم و جان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میوه چش باغ او ذائقهٔ حسن و ناز</p></div>
<div class="m2"><p>نازکش داغ او ناصیهٔ انس و جان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رشحهٔ فیضش کشد زر ز مسامات ارض</p></div>
<div class="m2"><p>تا با بد مشنواد بوی بهار این خزان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حکمت او چون کند آتش تدبیر تیز</p></div>
<div class="m2"><p>باز تواند گرفت مال صعود از دخان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نال قلم گر شود از کف حفظش علم</p></div>
<div class="m2"><p>چرخ تواند زدن بر سر آن آسمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>موی اگر پل شود در کنف حفظ وی</p></div>
<div class="m2"><p>تا ابدش نگسلد پویه پیل دمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بس که به سر گشته است چرخ بگرد درش</p></div>
<div class="m2"><p>آبله بر فرق سر یافته از فرقدان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا رودش در رکاب چرخ طویل انتظار</p></div>
<div class="m2"><p>بر کنفش شد کهن غاشیهٔ کهکشان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر به جهان افکند مصلحتش پرتوی</p></div>
<div class="m2"><p>پرتو مهتاب را صلح فتد با کتان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بهر تو طاعت تمام جبهه و لب می‌شود</p></div>
<div class="m2"><p>می‌رسد از رهروان هرچه بر آن آستان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حکمتش اندر خزان بیشتر از سرخ بید</p></div>
<div class="m2"><p>سازد و بیرون کشد خون ز رگ زعفران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگذرد از خاره‌تیر گرچه در اثنای کار</p></div>
<div class="m2"><p>نرم کند مشت او مهرهٔ پشت کمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مادر جود از سخا حامله چون شد فتاد</p></div>
<div class="m2"><p>با کرم حیدری همت او توامان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای به صلابت سمر وی به سیاست مثل</p></div>
<div class="m2"><p>وی به شجاعت علم وی به مهابت نشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از تو که سر تا قدم شعلهٔ سوزنده‌ای</p></div>
<div class="m2"><p>نایرهٔ مرکز فتاد دایرهٔ عظم و شان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شیهه شبدیز تو سینهٔ رستم خراش</p></div>
<div class="m2"><p>نیزهٔ خون ریز تو آتش جرات نشان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نور ضمیرت که تافت بر صفت ماه تاب</p></div>
<div class="m2"><p>شد به کتان هم مزاج پردهٔ راز نهان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از اثر نار بغض یافته مانند مار</p></div>
<div class="m2"><p>خصم تو بر زیر پوست آبله بر استخوان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کاه تو با کوه خصم سنجد اگر روزگار</p></div>
<div class="m2"><p>سایه به چرخ افکند پایهٔ کوه گران</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عهد تو تا زودتر روی به دهر آورد</p></div>
<div class="m2"><p>سیلی سرعت کند رنجه نشای زبان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چرخ گری را اگر پاس تو گردد حفیظ</p></div>
<div class="m2"><p>با دل جمع ایستد بر سر نوک سنان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر به شتابندگان نهی تو گردد دچار</p></div>
<div class="m2"><p>پای صبا را نخست رعشه کند تاروان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تنگ قبا شاهدیست عزم تو گوئی که ساخت</p></div>
<div class="m2"><p>قدرت پروردگار کاستش اندر مکان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زور تخلخل اگر عرصه نکردی وسیع</p></div>
<div class="m2"><p>تنگ فضائی بدی بر تو فضای جهان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دشمن از ادبار اگر در ره رمحت فتد</p></div>
<div class="m2"><p>آید از اقبال تو کار سنان از بنان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پیش کفت دوده ایست صرصری اندر قفا</p></div>
<div class="m2"><p>هرچه ازل تا ابد کرده بهم بحر و کان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آن که تو را مدعاست تیر جگر دوز تو</p></div>
<div class="m2"><p>منکر شان تو را ساخته خاطر نشان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز آفت بخت نگون خصم تو را در مزاج</p></div>
<div class="m2"><p>غیر گل گرد میخ نشکفد از زعفران</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کعبه کوکب که هست راه دو عالم درو</p></div>
<div class="m2"><p>صد ره و یک مشتریست هر ره و صد کاروان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر به زمین بسپری نعل سمند جلال</p></div>
<div class="m2"><p>آینه دانی شود سربه سر این خاکدان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بارهٔ خورشید را هر سحری می‌کنند</p></div>
<div class="m2"><p>بر زبر چرخ زین تا کشی‌اش زیر ران</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>لیک به روی زمین از حرکات سریع</p></div>
<div class="m2"><p>داردش اندر سبل رخش تو سیلاب ران</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شایدش از پویه خواند کشتی دریای خشک</p></div>
<div class="m2"><p>عزمش اگر کوه را بگذرد اندر کمان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چنبر چرخش برون بفشرد ار وقت لعب</p></div>
<div class="m2"><p>بر کفل اندازدش سایهٔ دوال عنان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>صبح گرش سر دهی بگذرد از ظهر چاشت</p></div>
<div class="m2"><p>بس که ز همراهیش باز پس افتد زمان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در کفلش چون کشند از حرکاتش زند</p></div>
<div class="m2"><p>طعنه به بال ملک دامن بر گستوان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر بکند کام خویش تنگ به حیلت‌گری</p></div>
<div class="m2"><p>باشد از امکان برون تاختنش بر مکان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کاسهٔ سمش هزار کاسهٔ سر بشکند</p></div>
<div class="m2"><p>بانگ هیاهوی رزم بشنود ار ناگهان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نیک توان یافتن صنعت او در یورش</p></div>
<div class="m2"><p>لیک از ابعاد اگر رفت تناهی توان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جامه قطع مکان دوخته هرکه که کس</p></div>
<div class="m2"><p>بر قد صد ساله راه بوده رسانیم آن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بس که سبک خیزیش جذب کند ثقل وی</p></div>
<div class="m2"><p>بر شمرد بحر را در ره هندوستان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خلقه حاتم کند مس سراپای وی</p></div>
<div class="m2"><p>مرد برو گر زند هی ز پی امتحان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>با کفل همچو کوه دانهٔ تسبیح را</p></div>
<div class="m2"><p>رشته شود وقت کار آن فرس کاروان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>باد ز پس‌ماندگی پیش فتد هم گهی</p></div>
<div class="m2"><p>گرد جهان گر بود در عقب او دوان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در ره باریک کرد پویهٔ او بی‌رواج</p></div>
<div class="m2"><p>کار رسن با زر ابر زیر ریسمان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بر زبر چار سم کرده سبک خشکیش</p></div>
<div class="m2"><p>از ره او گاه گاه نیم بلالی عیان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چون شده آن تیز گام هم تک باد صبا</p></div>
<div class="m2"><p>یافته حسن زمین کام صبا را گران</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>خنک فلک را اسمش داغ نهد بر سرین</p></div>
<div class="m2"><p>گرچه ز سطح زمین پا ننهد بر کران</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>باشد این شهسوار بهتر ازین صد هزار</p></div>
<div class="m2"><p>توسن فربه سرین تازی لاغر میان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>من که زبان جهان در ازلم شد لقب</p></div>
<div class="m2"><p>در صفتش خویش را یافتم الکن زبان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دادگرا سرورا شیردلا صفدرا</p></div>
<div class="m2"><p>گرچه درین دولتست محتشم از مادحان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>لیک به شغل دعا است آن قدرش اشتغال</p></div>
<div class="m2"><p>کز صفتش عاجز است صاحب طی لسان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>پاس حیاتش بدار ز آن که بحر ز دعا</p></div>
<div class="m2"><p>حفظ و نگهبانیست ختم بر این پاسبان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>طول ز حد شد برون به که سخن را کنون</p></div>
<div class="m2"><p>ختم کند بر دعا کلک مطول بیان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ملک جهان تا رود بر نهج رسم دهر</p></div>
<div class="m2"><p>دست به دست از ملوک ای شه کشورستان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>از اثر طول عهد مهد زمین را ز تو</p></div>
<div class="m2"><p>کس نستاند مگر مهدی صاحب زمان</p></div></div>