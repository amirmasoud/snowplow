---
title: >-
    قصیدهٔ شمارهٔ ۴۱ - در مدح فرهاد بیک غلام حاکم دارالسلطنه اصفهان
---
# قصیدهٔ شمارهٔ ۴۱ - در مدح فرهاد بیک غلام حاکم دارالسلطنه اصفهان

<div class="b" id="bn1"><div class="m1"><p>در نسبت است خسرو شاهان نامدار</p></div>
<div class="m2"><p>فرهاد بیک معتمد شاه کامکار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید رای ماه لوای فلک شکوه</p></div>
<div class="m2"><p>نصرت شعار فتح دثار ظفر مدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زور آور بلند سنان قوی کمند</p></div>
<div class="m2"><p>شیرافکن نهنگ کش اژدها شکار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رستم شجاعتی که چو دست آورد به حرب</p></div>
<div class="m2"><p>صد دست از نظارهٔ حربش رود به کار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دریا سخاوتی که چو گرم سخا شود</p></div>
<div class="m2"><p>بحر از کفش برآورد انگشت زینهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوه وجود خصم ز باد عمود او</p></div>
<div class="m2"><p>چون بیستون ز تیشهٔ فرهاد شد غبار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در گوی باختن نبود دور اگر کند</p></div>
<div class="m2"><p>گوی زمین ز هیبت چوگان او فرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر در مقام تربیت ذره‌ای شود</p></div>
<div class="m2"><p>در دم رساندش به فلک آفتاب‌وار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور التفات تقویت پشه‌ای کند</p></div>
<div class="m2"><p>خوش خوش برآرد از دم پیل دمان دمار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر مرد عرصه تنگ کند وقت دارو گیر</p></div>
<div class="m2"><p>بر خصم کارزار کند روزگار زار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای شهسوار عرصهٔ قدرت که ایزدت</p></div>
<div class="m2"><p>بر هرچه اختیار کنی داده اختیار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دارم حکایتی به تو از دور آسمان</p></div>
<div class="m2"><p>دارم شکایتی به تو از جور روزگار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سی سال شد که از پی هم می‌کنم روان</p></div>
<div class="m2"><p>از نظم تحفه‌ها بدر شاه شهریار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وز بهر من ز خلعت و زر آن چه می‌رسد</p></div>
<div class="m2"><p>بیش از دو ماه یا سه نمی‌آیدم به کار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وز بیع سست مشتریانم همیشه هست</p></div>
<div class="m2"><p>ز افکار خویش نفرت وز اشعار خویش عار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حالا که بی‌هدایت تدبیر همرهان</p></div>
<div class="m2"><p>یعنی به همعنانی تقدیر کردگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرهاد شد دلیل و به خسرو رهم نمود</p></div>
<div class="m2"><p>وز بیستون زحمتم آورد بر کنار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دارم امید آن که بود ز التفات او</p></div>
<div class="m2"><p>در یک رهم تردد و بر یک درم قرار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وز بهر یک کریم مطاع سخن نهم</p></div>
<div class="m2"><p>بر تازه بختیان ز یکی تا ز صد هزار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وانعام اولین که بامداد او بود</p></div>
<div class="m2"><p>ممتاز باشد از همه در چشم اعتبار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وان لاف‌ها که من زده‌ام از حمایتش</p></div>
<div class="m2"><p>بر مرد و زن نتیجه آن گردد آشکار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وین پا که من برای امیدش نهاده‌ام</p></div>
<div class="m2"><p>دست مرا به سر ننهد ناامیدوار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وان نرد غائبانه که با من فکند طرح</p></div>
<div class="m2"><p>کم نقش اگر شود ننهد بر عقب مدار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حاصل که همعنانی همت نموده چست</p></div>
<div class="m2"><p>بر توسن مراد به لطفم کند سوار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای هادی طریق مراد از قضا شبی</p></div>
<div class="m2"><p>بودم ز نامرادی خود سخت سوگوار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کانروز گرد راه پیام آوری برون</p></div>
<div class="m2"><p>وز غائبانه لطف توام ساخت شرمسار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کای خوش کلام طوطی بستان معرفت</p></div>
<div class="m2"><p>وی شوخ لهجه بلبل گلزار روزگار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شعر تو کسوتیست شهانش در آرزو</p></div>
<div class="m2"><p>نظم تو گوهریست سرانش در انتظار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر دوش نیست قابل این نازنین وشق</p></div>
<div class="m2"><p>هر گوش نیست لایق این طرفه گوشوار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر صاحب بصارت هوشی متاع خویش</p></div>
<div class="m2"><p>در بیع آن فکن که دهد در خورش نثار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یعنی ولیعهد شهنشاه تاج بخش</p></div>
<div class="m2"><p>شهزادهٔ قدر خطر صاحب اقتدار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>امید محتشم که بماند مدار دهر</p></div>
<div class="m2"><p>بر ذات این یگانه جهانگیر کامکار</p></div></div>