---
title: >-
    قصیدهٔ شمارهٔ ۳۸ - این قصیدهٔ را به جهت محمد نامی گفته
---
# قصیدهٔ شمارهٔ ۳۸ - این قصیدهٔ را به جهت محمد نامی گفته

<div class="b" id="bn1"><div class="m1"><p>به ساحل خواهد افتادن دگر بار</p></div>
<div class="m2"><p>دری از جنبش دریای اسرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنان در کشف رازی خواهد آورد</p></div>
<div class="m2"><p>زبان کلک را دیگر به گفتار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حدیث لطف و بی‌لطفی مولی</p></div>
<div class="m2"><p>لب تقریر خواهد کرد اظهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه مولی آن که در بازار معنی است</p></div>
<div class="m2"><p>سخن را بهترین میزان و معیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلیغی کاندر اوصاف کمالش</p></div>
<div class="m2"><p>به عجز خود بلاغت راست اقرار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهین دستور اعظم رای اکبر</p></div>
<div class="m2"><p>کز اخلاصند شاهانش پرستار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سمی نیر اوج رسالت</p></div>
<div class="m2"><p>محمد مهرانور نور انوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که بر روی زمینش خالق‌الارض</p></div>
<div class="m2"><p>ز آفات زمان بادا نگهدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بازارش سه در برد از من ایام</p></div>
<div class="m2"><p>یکی فرد و دو از نسبت بهم یار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه درها گنج‌های خسروانه</p></div>
<div class="m2"><p>ز حمل هر یکی گیتی گران بار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ولی از همت آن فرزانه گنجور</p></div>
<div class="m2"><p>چو از من آن در را شد خریدار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دو در را ثلث یک در داد قیمت</p></div>
<div class="m2"><p>وزین خاطر نشینم شد که این بار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در این بازار از بخت بد من</p></div>
<div class="m2"><p>از آن سودا به غایت بود بیزار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خدا را ای صبا در گوش آصف</p></div>
<div class="m2"><p>بگو آهسته کای دانای اسرار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شناسای دم و نطق گهر ریز</p></div>
<div class="m2"><p>خداوند دل و دست درم بار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شنیدم از بسی مردم که داری</p></div>
<div class="m2"><p>به مروارید و گوهر میل بسیار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>و گر گاهی به دست در فروشی</p></div>
<div class="m2"><p>به کف می‌آیدت یک در شهوار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو باد گل‌فشان می‌ریزی از دست</p></div>
<div class="m2"><p>زر سرخش بپا خروار خروار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بفرما کز گهرها چیست حالی</p></div>
<div class="m2"><p>تو را در مخزن ای دریای ذخار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که می‌نازد به آنها گوش شاهان</p></div>
<div class="m2"><p>جز آنها کت من آوردم به بازار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به تخصیص آن چنان کز بهر شهرت</p></div>
<div class="m2"><p>بر آن نام خوشت کندم نگین‌وار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خموش ای محتشم کز بالغان است</p></div>
<div class="m2"><p>به غایت خود ستائی ناسزاوار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>درین سان سرزمینی تخم دعوی</p></div>
<div class="m2"><p>نمی‌آرد به جز شرمندگی بار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در نظم تو را با این زبونی</p></div>
<div class="m2"><p>بهائی داد آن رای جهاندار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که در چشم دل از صد گنج بیش است</p></div>
<div class="m2"><p>به قیمت نه به عظم و قدر و مقدار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سراسر تحفه‌های برگزیده</p></div>
<div class="m2"><p>علم از بی‌نظیری‌ها در انظار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر دیگر دری داری بیاور</p></div>
<div class="m2"><p>کزین به نیست در عالم خریدار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شروع اندر ثنایش کن که چون او</p></div>
<div class="m2"><p>کریمی نیست در بازار اشعار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زهی برگرد قصرت پاسبان‌وار</p></div>
<div class="m2"><p>بسر تا روز گردان چرخ دوار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زهی اعظم وزیری کز شکوهت</p></div>
<div class="m2"><p>وزارت راست از شاهنشهی عار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زهی گردون سریری کز سرورت</p></div>
<div class="m2"><p>ابد سیر است چنگ زهره بر تار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو آن مسند نشینی کایستاده</p></div>
<div class="m2"><p>ز تعظیمت به خدمت چرخ سیار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو آن آصف نشانی کاوفتاده</p></div>
<div class="m2"><p>ز توصیف سلیمانی در اقطار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اگر بالفرض باشد رای امرت</p></div>
<div class="m2"><p>برون آید چو تیغ از جلد خودمار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>و گر در جنبش آید باد نهیت</p></div>
<div class="m2"><p>بره سیل نگون ماند ز رفتار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کنی گر منع وحشت از طبایع</p></div>
<div class="m2"><p>به شهر آیند یک سر وحش کوهسار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چراغ دین چو گردد از تو ذوالنور</p></div>
<div class="m2"><p>بسوزد کافر صد ساله زنار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر جازم شود دهقان سعیت</p></div>
<div class="m2"><p>دماند در جبل ز احجار اشجار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نیابد در پناه حفظت آسیب</p></div>
<div class="m2"><p>حریر برگ گل از سوزن خار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>و گر ماه از تو پوشد کسوت نور</p></div>
<div class="m2"><p>شود از روز روشن‌تر شب تار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر یکبار خواهی رفع ظلمت</p></div>
<div class="m2"><p>برآرد خور سر از ظلمات ناچار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر از حکمت زنی دم در زمانت</p></div>
<div class="m2"><p>چه عنقا و چه اکسیر و چه بیمار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر حیز طلب گردد جلالت</p></div>
<div class="m2"><p>برون تازد فرس زین چار دیوار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دو عالم بر در و گوهر شود تنگ</p></div>
<div class="m2"><p>شوی غواص چون در بحر افکار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز گل گر پیکری سازی و در وی</p></div>
<div class="m2"><p>دمی یک نفخه گردد مرغ طیار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جهان را سر به سر این قابلیت</p></div>
<div class="m2"><p>نبود ای قیصر اسکندر آثار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که گرد خوب و زشتش باشد از حفظ</p></div>
<div class="m2"><p>حفیظی چون تو گردانندهٔ پرگار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اگر کس از سر ملکت گزینی</p></div>
<div class="m2"><p>جرون را حالیا تالار سالار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>و گرنه گر بدی در بسته از تو</p></div>
<div class="m2"><p>همهٔ انصار بی‌اعوان و انصار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چنان حفظش نمودی کز دل مور</p></div>
<div class="m2"><p>ضمیر انورت بودی خبردار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سرای جغد هم گشت از تو معمور</p></div>
<div class="m2"><p>چو گردیدی درین ویرانه معمار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گر از مرغان این گلشن مرا نیز</p></div>
<div class="m2"><p>که جز شکر نمی‌ریزم ز منقار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دهی زین بیش ره در گلشن خویش</p></div>
<div class="m2"><p>شود شکرستان این طرفه گلزار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>وز اوصافت چنان عالم شود پر</p></div>
<div class="m2"><p>که بر امسال صد حسرت برد پار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>غرض کز بهر ترتیب ثنایت</p></div>
<div class="m2"><p>من از بحر ضمیر معجز آثار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کشم در رشتهٔ فکرت لالی</p></div>
<div class="m2"><p>ز آغاز لیالی تا به اسحار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خموش ای دل که از بسیارگوئی</p></div>
<div class="m2"><p>دل نازک دلان می‌یابد آزار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>عنان تاب از ره افکار شو هان</p></div>
<div class="m2"><p>که شد ز اطناب پای خامهٔ افکار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به تنگ آمد ثنا از دست نطقت</p></div>
<div class="m2"><p>دعا نوبت طلب شد دست بردار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>درین سطح از پی رسم دوایر</p></div>
<div class="m2"><p>بود تا گردش پرگار در کار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز امرت هر که در دوران کشد سر</p></div>
<div class="m2"><p>چو پرگارش فلک سازد نگون‌سار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بود تا ملک جسم از خسرو روح</p></div>
<div class="m2"><p>بود تاسر بر آن اقلیم سردار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تو سردار جهان باشی و دایم</p></div>
<div class="m2"><p>بود جای سر خصمت سر دار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به کینت هر که بر بالین نهد سر</p></div>
<div class="m2"><p>نگردد تابه صبح حشر بیدار</p></div></div>