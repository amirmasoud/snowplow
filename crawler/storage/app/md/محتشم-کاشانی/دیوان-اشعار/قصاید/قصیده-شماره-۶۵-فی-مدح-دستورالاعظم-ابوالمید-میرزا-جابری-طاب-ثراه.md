---
title: >-
    قصیدهٔ شمارهٔ ۶۵ - فی مدح دستورالاعظم ابوالمید میرزا جابری طاب ثراه
---
# قصیدهٔ شمارهٔ ۶۵ - فی مدح دستورالاعظم ابوالمید میرزا جابری طاب ثراه

<div class="b" id="bn1"><div class="m1"><p>باد مسعود و همایون خلعت شاه جهان</p></div>
<div class="m2"><p>بر وزیر جم سریر کامکار کامران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آصف اعظم مهین دستور خاقان عجم</p></div>
<div class="m2"><p>مرکز عالم گزین معیار پرگار جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میرزا سلمان سلیمان زمان فخر زمین</p></div>
<div class="m2"><p>پایهٔ دین و دول سرمایهٔ امن و امان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن که از جوهرشناسی روز بازار ازل</p></div>
<div class="m2"><p>فخر کرد از جوهر ذاتش زمین بر آسمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وانکه گنجور کزو آفرینش برنیافت</p></div>
<div class="m2"><p>گوهری مانند او در مخزن آخر زمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست رایش پادشاهی کز ازل دارد لقب</p></div>
<div class="m2"><p>مه‌لوا فرمانروا کشورگشا گیتی‌ستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برخی از اوصاف او در آصف بن برخیاست</p></div>
<div class="m2"><p>زان که از کرسی نشین فرقت تا کرسی نشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر سر طور ظفر او راند موسی وار رخش</p></div>
<div class="m2"><p>در تن دهر سقیم او کرد عیسی‌وار جان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود دهر پیر را طبع زلیخا کاین چنین</p></div>
<div class="m2"><p>شاهد یوسف جمال عهد او کردش جوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن چه گردان توانا در جهانگیری کنند</p></div>
<div class="m2"><p>در بنانش می‌تواند کرد کلک ناتوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خلق بهر داوری بر آستانش صف زنند</p></div>
<div class="m2"><p>آفتاب خاوری چون سر زند از خاوران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آستینش جبههٔ فرسایندهٔ میر و وزیر</p></div>
<div class="m2"><p>آستانش سجدهٔ فرمایندهٔ سلطان و خان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دهر معلول از علاجش خستهٔ عیسی طبیب</p></div>
<div class="m2"><p>خلق عالم در پناهش گله موسی شبان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می‌تواند کرد تدبیرش به یکدیگر به دل</p></div>
<div class="m2"><p>ثقل و خفت در مزاج آهن و طبع دخان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مانده پرگاری ز حفظش کز برای پاس مال</p></div>
<div class="m2"><p>دزد چون پرگار می‌گردد به گرد کاروان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از نهیب نهی او در نیمه ره باز ایستد</p></div>
<div class="m2"><p>تیر پرانی که بیرون رفته باشد از کمان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گوی را از جا بجنباند به نیروی قضا</p></div>
<div class="m2"><p>گر کند احساس منع از صولت او صولجان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>انتقامش چون کند دست ضعیفان راقوی</p></div>
<div class="m2"><p>پشه در دم برکند گوش از پیل دمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مژدهٔ عونش چو سازد زیر دستان را دلیر</p></div>
<div class="m2"><p>از تلاش روبه افتد در زیان شیر ژیان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عون او خلق جهان از از بد عالم پناه</p></div>
<div class="m2"><p>عهد او عهد و امان را تا دم محشر ضمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر بدندی در زمان او به جای جود و عدل</p></div>
<div class="m2"><p>شهره گشتی بخل و ظلم از حاتم و نوشیروان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بحر بازی بازی از در و گوهر گردد تهی</p></div>
<div class="m2"><p>چون کند وقت گوهر بخشی قلم را امتحان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>های و هوی و لشگر و خیل و سپه در کار نیست</p></div>
<div class="m2"><p>عالمی را کان جهان سالار باشد پاسبان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از پی گنجایش برخاست دیوار حجاب</p></div>
<div class="m2"><p>از میان چار دیوار مکان و لامکان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بی‌طلب حاضر شود چون خوردنیهای بهشت</p></div>
<div class="m2"><p>بر سر خوان نوالش هرچه آید در گمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عرشیان آیند اگر بهر تواضع بر زمین</p></div>
<div class="m2"><p>خسروان را آستین بوسند و او را آستان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در زمین ذات و خیر دولتش روزی که کرد</p></div>
<div class="m2"><p>نصرت استیلا پی رد جلای ناگهان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دهر هم دولت یمینش گفت و هم نصرت یسار</p></div>
<div class="m2"><p>چرخ هم شوکت قرینش خواند و هم صاحبقران</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خلعتی کایزد به قد کبریای او برید</p></div>
<div class="m2"><p>در زمان شاه عالی همت حاتم زمان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر بریزند از در جوئی به هامون آب بحر</p></div>
<div class="m2"><p>ور به بیزند از گوهر خواهی به دقت خاک کان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ور ملک از کارگاه قدرت آرد تار و پود</p></div>
<div class="m2"><p>ور فلک از نقش بند غیب گیرد نقشدان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نقش تشریفی چنان صورت نمی‌بندد مگر</p></div>
<div class="m2"><p>در میان دستی برآرد نقش پرداز جهان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وه چه تشریف آسمانی در زمین انجم نما</p></div>
<div class="m2"><p>سهو کردم آفتابی بر زمین اختر فشان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر سر تشریح تاجی فرق گوهرهای فرد</p></div>
<div class="m2"><p>با کمر در جوهراندوزیش دعوی در میان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در خور آن تاج تابان جقه‌ای کز همسری</p></div>
<div class="m2"><p>می‌زند پر بر پر خورشید در یک آشیان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از شعاع چارقب روز و شب اندر شش جهت</p></div>
<div class="m2"><p>مشعل خورشید مخفی و سواد شب نهان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از علامت‌های تشریف شریف آصفی</p></div>
<div class="m2"><p>همرهش زرین دواتی سربه سر گوهرنشان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از پی تشریف اسبی در سبک خیزی چو باد</p></div>
<div class="m2"><p>زیر زین آسمان سنگ از گوهرهای گران</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مرکبی کاندم که آرمیده راند راکبش</p></div>
<div class="m2"><p>شام باشد دهری خفتن در آذربایجان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>توسنی کز روز باد پویه‌اش گوی زمین</p></div>
<div class="m2"><p>در شتاب افتد چو کشتی کش دواند بادبان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از در مغرب برانگیزد سم سختش غبار</p></div>
<div class="m2"><p>گر به مشرق نرم یابد در کف فارس عنان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بردن نامش گر ابکم بگذراند در ضمیر</p></div>
<div class="m2"><p>تا ابد در خویش یابد نشاه طی لسان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>رنگ خنگ آسمان دارد ز سر تا پا که هست</p></div>
<div class="m2"><p>آفتابش ماه پیشانی هلالش داغ ران</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بهر این تشریف از پر کله تا نعل رخش</p></div>
<div class="m2"><p>تهنیت فرض است بر خلق زمین و آسمان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>حاصل از وی چون گران شد مسند از هر باب کرد</p></div>
<div class="m2"><p>عقل تاریخی تجسس هم گران و هم روان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>اعتمادالدولتش بد چون درین دولت لقب</p></div>
<div class="m2"><p>آن لقب را دوخسان آورد طبع نکته‌دان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر چو یک سال آمد افزون بود عین مصلحت</p></div>
<div class="m2"><p>تا به این علت مصون ماند ز چشم حاسدان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>قصه کوته چون قدم دروای فکرت نهاد</p></div>
<div class="m2"><p>عقل دور اندیشه در اندیشهٔ اصلاح آن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>طبع دقت پیشه بر اندیشه سبقت کرد و گفت</p></div>
<div class="m2"><p>اعتمادالدوله افسر بخش بادا در جهان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آصفا عالم مدارا بختیارا داورا</p></div>
<div class="m2"><p>ای به زور بخت کامل قدرت و بالغ توان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>عرضه‌ای دارم چه قول مردم بالغ سخن</p></div>
<div class="m2"><p>هم طویل اندر مضامین هم قصیر اندر بیان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>طوطی شیرین زبان شکرستان عراق</p></div>
<div class="m2"><p>کز جفای قرض خواهان بود زهرش در دهان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>با وجود این همه بی‌دست و پائیها که داشت</p></div>
<div class="m2"><p>گشته بود از تنگدستی عازم هندوستان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>وان چه بیش از جمله‌اش آواره می‌کرد از وطن</p></div>
<div class="m2"><p>قرض پر شلتاق دیوان بود آن بار گران</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تا که از امداد صاحب مژده بخشش رسید</p></div>
<div class="m2"><p>بخشش مقرون به تشریف شه صاحبقران</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>من به این پاداش بر چیزی که حالا قادرم</p></div>
<div class="m2"><p>هست ارسال ثناها کاروان در کاروان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بی‌تکلف صاحبا کردی ز وامی فارغم</p></div>
<div class="m2"><p>کز هراسش بود بی‌آرام در تن مرغ جان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>وز طلب گشتند بر امید دیگر لطف‌ها</p></div>
<div class="m2"><p>قرض خواهان دیگر هم اندکی کوته‌زبان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ای تمام احسان اگر در عهد شاهی این چنین</p></div>
<div class="m2"><p>کز زر و گوهر خزاین را تهی کرد آن چنان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بنده را یکبارگی از قرض خواهان واخری</p></div>
<div class="m2"><p>سود پندارم درین سودا بود بیش از زیان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>محتشم ای در فن خود از توقع برکنار</p></div>
<div class="m2"><p>آمدی آخر درین فن نیک بیرون از میان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بحر خواهش را کرانی نیست پیدا لب ببند</p></div>
<div class="m2"><p>پس زبان بگشای در عرض دعای بیکران</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تا درین کاخ عظیم‌الرکن خوش بنیان دهند</p></div>
<div class="m2"><p>از بنای بی‌زوال دولت و ملت نشان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>پایهٔ بنیان این ملت تو باشی پایدار</p></div>
<div class="m2"><p>اعظم ارکان این دولت تو باشی جاودان</p></div></div>