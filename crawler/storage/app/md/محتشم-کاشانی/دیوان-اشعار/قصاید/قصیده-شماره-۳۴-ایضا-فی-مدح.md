---
title: >-
    قصیدهٔ شمارهٔ ۳۴ - ایضا فی مدح
---
# قصیدهٔ شمارهٔ ۳۴ - ایضا فی مدح

<div class="b" id="bn1"><div class="m1"><p>وقت کم بختی که مرغ دولتم می‌ریخت پر</p></div>
<div class="m2"><p>بهر دفع غم شبی در گلشنی بردم بسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از قضا در حسب حال من به آواز حزین</p></div>
<div class="m2"><p>بلبلی با بلبلی می‌گفت در وقت سحر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاندرین خاکی رباط پرملال کم نشاط</p></div>
<div class="m2"><p>وندرین سفلی بساط کم ثبات پرخطر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذره‌ای را آفتابی بر گرفت از خاک راه</p></div>
<div class="m2"><p>ساختندش حاسدان یکسان به خاک رهگذر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صعوه‌ای را شاهبازی ساخت هم پرواز بخت</p></div>
<div class="m2"><p>واژگون بختان شکستندش ز غیرت بال و پر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تشنه‌ای را کام بخشی شربتی در کام ریخت</p></div>
<div class="m2"><p>مفسدان کردند کامش راز حنظل تلخ تر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بینوائی راسخی طبعی به یک بخشش نواخت</p></div>
<div class="m2"><p>از حسدهای گدا طبعان رسیدش صد ضرر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر غریبی شهریاری از تفقد در گشود</p></div>
<div class="m2"><p>در به روی خیربندان بر رخش بستند در</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صیدی ازنخچیر بندی بود در قید قبول</p></div>
<div class="m2"><p>رشگ مردودان به صحرای هلاکش دادسر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود ویران کلبه‌ای از لطف گردون رتبه‌ای</p></div>
<div class="m2"><p>در بلندی طاق دوران ساختش زیر و زبر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قصه کوته ماه ایران میر میران کایزدش</p></div>
<div class="m2"><p>کرد ازبس سربلندی سرور جن و بشر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وز طلوع آفتاب دولتش از فرش خاک</p></div>
<div class="m2"><p>سر به سر ذرات عالم را به عرش افراخت سر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از ترشح کردن ابر کف کافیش داشت</p></div>
<div class="m2"><p>محتشم از پیشتر چشم تفقد بیشتر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن ترشح بی‌خطائی ناگهان باز ایستاد</p></div>
<div class="m2"><p>و آن تفقد بی‌گناهی گشت مسدودالممر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من نمی‌دانم چه واقع شد که کرد از جرم آن</p></div>
<div class="m2"><p>لطف آن سرور ز جیب سر گرانی سر بدر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>و اندر اوقات مریدی جز خلوص از وی چه دید</p></div>
<div class="m2"><p>آن سرو سرخیل افراد بشر از خیر و شر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن خدنگ اندازی از قوس دعا صبح و مسا</p></div>
<div class="m2"><p>یا نه آن بیداری از عین بکا شام و سحر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یا نه آن بی‌عیب مدحت‌ها که از انشای آن</p></div>
<div class="m2"><p>ذیل گردون پر در است و جیب دوران پر گوهر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یا نه آن بی‌ریب یاربها که از دل بر زبان</p></div>
<div class="m2"><p>نارسیده می‌کند از سقف این منظر گذر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یا نه آن اخلاص ورزیها که اخلاص فقیر</p></div>
<div class="m2"><p>با نصیر ملت اندر جنبش آمده مختصر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بلبل افسانه گو چون پرده از مضمون کشید</p></div>
<div class="m2"><p>بلبل مضمون شنو گفت ای رفیق چاره‌بر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خیز و در گوش دل آن بی‌گنه خوان این سرود</p></div>
<div class="m2"><p>کای ز طبعت جلوه گر اشخاص معنی در صور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن که در دانستن قدر سخن همتاش نیست</p></div>
<div class="m2"><p>کی معطل می‌کند او چون توئی را این قدر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در تو پوشانند اگر از عیب مردم صد لباس</p></div>
<div class="m2"><p>کی شود پوشیده پیش خاطر او این هنر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کز نی خوش جنبش کلک تو در اوصاف او</p></div>
<div class="m2"><p>می‌رود زین شکرستان تا به خوزستان شکر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وز ثنایش طبع مضمون آفرینش می‌کند</p></div>
<div class="m2"><p>در تن شخص فصاحت هر زمان جان دگر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وز مدیحش کاروان سالار فکرت می‌دهد</p></div>
<div class="m2"><p>کاروانهای جواهر را سر اندر بحر و بر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر نصیحت می‌پذیری خیز و در باغ خیال</p></div>
<div class="m2"><p>از زلال نظم کن نخل قلم را بارور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وز سحاب تربیت هرچند بر کشت دلت</p></div>
<div class="m2"><p>ز اقتضای خشگ سال لطف کم ریزد مطر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آن چنان رو بر سر مدحش کز اعجاز سخن</p></div>
<div class="m2"><p>از حجر دهقانی طبعت برانگیزد شجر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وز شجر بی‌انتظار مدت نشو و نما</p></div>
<div class="m2"><p>دامن آفاق هم پر گل شود هم پر ثمر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من که بر لب داشتم ز افسردگی مهر سکوت</p></div>
<div class="m2"><p>بر گرفتم مهر و بگرفتم ثنا خوانی ز سر</p></div></div>