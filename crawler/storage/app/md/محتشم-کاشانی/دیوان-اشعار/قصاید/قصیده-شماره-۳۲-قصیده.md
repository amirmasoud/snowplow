---
title: >-
    قصیدهٔ شمارهٔ ۳۲ - قصیده
---
# قصیدهٔ شمارهٔ ۳۲ - قصیده

<div class="b" id="bn1"><div class="m1"><p>سهی بالای بزم آرای مه سیمای مهرآسا</p></div>
<div class="m2"><p>قدح پیمای غم‌فرسای روح‌افزای جان‌پرور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرت گردم چه واقع شد که در مجموعهٔ یاری</p></div>
<div class="m2"><p>رقم‌های محبت را قلم بر سر زدی اکثر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازینت دوستر دانسته بودم کز فراق خود</p></div>
<div class="m2"><p>گماری دشمنی از مرگ بدتر بر من ابتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه من آن کوچه پیمایم که شبها تا سحر بودی</p></div>
<div class="m2"><p>برای شمع راه من چراغ روزن و منظر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شد آن مهربانیها که دایم بود در مجلس</p></div>
<div class="m2"><p>ز تر دامانی چشم نمینم آستینت تر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجا رفت آن خصوصیت که از همدم نوازیها</p></div>
<div class="m2"><p>نبود آرام از آن دست نگارین حلقه را بردر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گمان دارد دلم زین سرکشی ای شمع بی‌پروا</p></div>
<div class="m2"><p>که داری از هوای دل سر پروانه‌ای دیگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز پایت برندارم سر اگر دارم کنی برپا</p></div>
<div class="m2"><p>ز کویت وانگیرم پا اگر تیغم زنی برسر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو را بازار گرم و من زرشک نو خریداران</p></div>
<div class="m2"><p>از آن بازار در آزار از آن آزار در آذر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من از تشویش جان با این گران باری سبک نمکین</p></div>
<div class="m2"><p>تو از تمکین دل با آن سبک روحی گران لنگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ازین پس محتشم مشکل که آن صیاد مستغنی</p></div>
<div class="m2"><p>کند ضایع خدنگ خویش بر صیدی چنین لاغر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بساط عاشقی طی ساز کز بهر دعای شه</p></div>
<div class="m2"><p>در نه آسمان باز است و آمین گوت هفت اختر</p></div></div>