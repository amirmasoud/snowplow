---
title: >-
    قصیدهٔ شمارهٔ ۳۰ - تجدید مطلع
---
# قصیدهٔ شمارهٔ ۳۰ - تجدید مطلع

<div class="b" id="bn1"><div class="m1"><p>شبی به دایتش از روزگار هجر به تر</p></div>
<div class="m2"><p>نهایتش چو زمان وصال فیض اثر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبی در اول دی شام تیره‌تر ز عشا</p></div>
<div class="m2"><p>ولی در آخر او صبح پیشتر ز سحر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبی عیان شده از جیب او ره ظلمات</p></div>
<div class="m2"><p>ولی زلال بقا زیر دامنش مضمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبی چو غره ماه محرم اول او</p></div>
<div class="m2"><p>ولی ز سلخ مه روزهٔ آخرش خوش‌تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبی مشوش و ژولیده موی چون عاشق</p></div>
<div class="m2"><p>ولی به چشم خرد سیم ساق چون دلبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبی جواهر فیضش ز افسر افتاده</p></div>
<div class="m2"><p>ولی رسیده به زانویش از زمین گوهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبی ز آهن زنگار بسته مغفروار</p></div>
<div class="m2"><p>ولی به پای تحمل کشیده موزهٔ زر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز شام تا به دو پاس تمام آن شب بود</p></div>
<div class="m2"><p>مرا صحیفهٔ حالات خویش مد نظر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمان زمان به سرم از وساوس بشری</p></div>
<div class="m2"><p>سپاه غم به صد آشوب می‌کشید حشر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گهی ز وسوسه بی کسی و تنهائی</p></div>
<div class="m2"><p>چو غنچه دست من تنگ دل گریبان در</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گهی ز کید اعادی دلم در اندیشه</p></div>
<div class="m2"><p>که منزوی شده بر روی خلق بندد در</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گهی ز فوت برادر غمی برابر کوه</p></div>
<div class="m2"><p>دل مرا ز تسلط نموده زیر و زبر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گهی ستاده مجسم به پیش دیده و دل</p></div>
<div class="m2"><p>پسر برادرم آن کودک ندیده پدر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که در ولایت هند از عداوت گردون</p></div>
<div class="m2"><p>فتاده طفل و یتیم و غریب و بی‌مادر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گذشت برخی از آن شب برین نمط حاصل</p></div>
<div class="m2"><p>که دل فکار و جگر ریش بود جان مضطر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو بعد از آن سپه خواب براساس حواس</p></div>
<div class="m2"><p>گشود دست و تنم را فکند در بستر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گذشت اول آن خواب اگرچه در غفلت</p></div>
<div class="m2"><p>ولی در آخر آن فیض بود بی‌حد و مر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه دید دیدهٔ دل‌افروز عالمی که در آن</p></div>
<div class="m2"><p>گوهر به جای حجر بود و در به جای مدر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز مشرقش که نجوم بروج دولت را</p></div>
<div class="m2"><p>ز عین نور صفا بود مطلع و مظهر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ستاره‌ای بدرخشید کز اشعهٔ آن</p></div>
<div class="m2"><p>فروغ بخش شد این کهنه تودهٔ اغبر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سهیلی از افق فیض شد بلند کزان</p></div>
<div class="m2"><p>عقیق رنگ شد این کهنه گنبد اخضر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>غرض که پادشهی بر سریر عزت و جاه</p></div>
<div class="m2"><p>به من نمود جمالی ز آفتاب انور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>من گدا متفکر که این کدام شه است</p></div>
<div class="m2"><p>که آفتاب صفت سوده بر سپهر افسر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز غیب هاتفی آواز داد که ای غافل</p></div>
<div class="m2"><p>برآوردندهٔ حاجات توست این سرور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پناه ملک و ملل شاه و شاهزادهٔ هند</p></div>
<div class="m2"><p>که خاک روب در اوست خسرو خاور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فلک سریر و عطارد دبیر و مهر ضمیر</p></div>
<div class="m2"><p>ستارهٔ لشگر و کیوان غلام و مه چاکر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نظام بخش خواقین دین نظام‌الملک</p></div>
<div class="m2"><p>کمین بارگه کبریا شه اکبر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نطاق بند خواقین گره گشای ملوک</p></div>
<div class="m2"><p>خدایگان سلاطین جسم جهان داور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بلند رتبه سورای که رخش سرکش او</p></div>
<div class="m2"><p>نهد ز کاسهٔ سم بر سر فلک مغفر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هژبر حملهٔ دلیری که شیر چرخ پلنگ</p></div>
<div class="m2"><p>چنان هراسد ازو کز درندهٔ شیر نفر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مصاف بیشه نهنگی که زورق گردون</p></div>
<div class="m2"><p>ز پیش او گذرانند حاملان به حذر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز جا بجنبد اگر تند باد صولت او</p></div>
<div class="m2"><p>ز هیبتش گسلد کشتی زمین لنگر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گهی ز دغدغهٔ ناقه کش بر افتد نام</p></div>
<div class="m2"><p>چو فاق تیر مرا کام پر ز خون جگر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر استعانه کند ماه ازو به وقت خسوف</p></div>
<div class="m2"><p>زمین ز دغدغه از جا رود به این همه فر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>و گر مدد طلبد مهر ازو محل کسوف</p></div>
<div class="m2"><p>ز جوز هر جهد از سهم وی چو سر قمر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو خلق او ره آزار را کنند مسدود</p></div>
<div class="m2"><p>گشاید از بن دندان مار جوی شکر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز گرمی غضبش سنگ ریزه در ته آب</p></div>
<div class="m2"><p>ز تاب واهمه یابد حرارت اخگر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مهی بتافت که از پرتو تجلی آن</p></div>
<div class="m2"><p>فرود دیدهٔ ایام را جلای دیگر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سپهر مرتبهٔ شاها به رب ارض و سما</p></div>
<div class="m2"><p>به شاه غایب و حاضر خدای جن و بشر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به شاه تخت رسالت محمد عربی</p></div>
<div class="m2"><p>حریف غالب چندین هزار پیغمبر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به جوشن تن خیرالبشر علی ولی</p></div>
<div class="m2"><p>حصار قلعهٔ دین فاتح در خیبر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که نور چشم من آن کودک یتیم غریب</p></div>
<div class="m2"><p>که دامن دکن از آب چشم او شده تر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به لطف سوی منش کن روانکه باقی عمر</p></div>
<div class="m2"><p>مرا به بوی برادر چه جان بود در بر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>امید دیگرم اینست و ناامید نیم</p></div>
<div class="m2"><p>که تا جهان بودی خسرو جهان پرور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به اهل بیت محمد که ذیل طاهرشان</p></div>
<div class="m2"><p>بود ز پردهٔ چشم فرشتگان اطهر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به آب چشم یتیمان کربلا که بود</p></div>
<div class="m2"><p>بر او درخت شفاعت از آن خجسته ثمر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به دفتر کرامت نام این گدا بنگار</p></div>
<div class="m2"><p>به حال محتشم ای شاه محتشم بنگر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چنان به کام تو باشد که گر اراده کنی</p></div>
<div class="m2"><p>سفال زر شود و خاک مشک و خار گوهر</p></div></div>