---
title: >-
    قصیدهٔ شمارهٔ ۱ - در توحید حضرت باریتعالی و موعظه
---
# قصیدهٔ شمارهٔ ۱ - در توحید حضرت باریتعالی و موعظه

<div class="b" id="bn1"><div class="m1"><p>نفیر مرغ سحر خوان چو شد بلندنوا</p></div>
<div class="m2"><p>پرید زاغ شب از روی بیضهٔ بیضا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طلایه‌دار سپاه حبش که بود قمر</p></div>
<div class="m2"><p>ربود رنگ ز رویش خروج شاه ختا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوار یک تنه چین دواسبه تاخت چنان</p></div>
<div class="m2"><p>که خیل زنگ شد از باد او به باد فنا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریخت گاو شب از شیر بیشهٔ مشرق</p></div>
<div class="m2"><p>وز آن گریز برآمد ز خامشان غزا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غراب شب که سحر شد کلاغ ابیض بال</p></div>
<div class="m2"><p>عقاب خور ز سرش پوست کند از استیلا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار چشم ز انجم گشوده بود هنوز</p></div>
<div class="m2"><p>که برد دزد سحر خال شب ز روی هوا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو صبح بر محک شب کشیده شد زرمهر</p></div>
<div class="m2"><p>به یکدم آن سیه آیینه گشت غرق جلا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ریاض چرخ ز انجم شکوفهٔ نارنج</p></div>
<div class="m2"><p>چو ریخت در دو نفس شد برش ریاض آرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترنج دافع صفراست وین عجب که نبرد</p></div>
<div class="m2"><p>ترنج مهر ز طبع جهان به جز سودا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به روی تختهٔ افلاک چون ز مهرهٔ مهر</p></div>
<div class="m2"><p>بیاض صبح به آن طول و عرض یافت صفا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشان میر ختن شد چنان نوشته که هیچ</p></div>
<div class="m2"><p>نماند دوده درین کاسهٔ نگون برجا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سحر ز یوسف گم‌گشته پیرهن چو نمود</p></div>
<div class="m2"><p>ز مهر دیدهٔ یعقوب دهر شد بینا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز صبح سینه صافی نمود ماهی شب</p></div>
<div class="m2"><p>که روی یونس خورشید بود ازو پیدا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گلیم تیره فرعون شب در آب انداخت</p></div>
<div class="m2"><p>ید کلیم کزو یافت بر و بحر ضیا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گشود شب در صندوق آبنوس از صبح</p></div>
<div class="m2"><p>وز آن نمود زری سکه‌اش به نام خدا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر نه سکه به نام خدا بر او بودی</p></div>
<div class="m2"><p>چنین روان نشدی در بسیط ارض و سما</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه سکه است بر این زر که نیستش کاری</p></div>
<div class="m2"><p>بکار خانه تغییر تا به روز جزا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه داور است جهان را که سکهٔ خانهٔ اوست</p></div>
<div class="m2"><p>رواق چرخ پرانجم به آن شکوه و بها</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه کردگار ستائیست این خموش ای نطق</p></div>
<div class="m2"><p>بوادی به ازین کن روان سمند ثنا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زری که در خور آئین پادشاهی اوست</p></div>
<div class="m2"><p>به جنب او زر مهر است کم ز سیم بها</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زهی به ذات جلیلی که برقد صفتش</p></div>
<div class="m2"><p>قصیر مانده لباس فصاحت فصحا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زهی به وجه جمیلی که شخص معرفتش</p></div>
<div class="m2"><p>به صد حجاب کند جلوه پیش ذهن و ذکا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کشنده طبقات نه آسمان برهم</p></div>
<div class="m2"><p>بهر یک از جهتی سیر مختلف فرما</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برآورنده ز شرق و فرو برنده به غرب</p></div>
<div class="m2"><p>لوای زرکش خورشید هر صباح و مسا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فزون کننده و کاهنده قمر به مرور</p></div>
<div class="m2"><p>ره حساب شهور و سنین به خلق نما</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به امتزاج عناصر ز عالی و سافل</p></div>
<div class="m2"><p>وجود بخش خلایق ز اسفل و اعلا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به دست قابلی محرمان خلوت قرب</p></div>
<div class="m2"><p>جمیله شاهد اعجاز را جمال آرا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برون کشنده حوا ز پهلوی آدم</p></div>
<div class="m2"><p>خمیر مایهٔ ده نسل آدم از حوا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برنده بر فلک ادریس را و بر تن او</p></div>
<div class="m2"><p>برنده رخت اقامت به قامت دنیا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نقاب بند ز طوفان به چهرهٔ عالم</p></div>
<div class="m2"><p>به استغاثهٔ نوح از تنور چشمه گشا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز قوم هود که یک نیمه در زمین رفتند</p></div>
<div class="m2"><p>درو کننده نیمی دگر به داس صبا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز سنگ خاره برون آورنده ناقه</p></div>
<div class="m2"><p>دعای بندهٔ صالح شنو به سمع رضا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حرارت از دل آتش ستان برای خلیل</p></div>
<div class="m2"><p>اثر ز دست مؤثر به دست صنع ربا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>روان کننده به هنگام ذبح اسماعیل</p></div>
<div class="m2"><p>بشیر حکم که گردد برندهٔ نابرا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برآورنده به عیوق شهر مردم لوط</p></div>
<div class="m2"><p>نگون کننده ز وارونه رائی فسقی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>لباس باصره پوشان بدیدهٔ یعقوب</p></div>
<div class="m2"><p>ز بوی پیرهن یوسف فرشته لقا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بطی خشک و تر الیاس و خضر را چو ملک</p></div>
<div class="m2"><p>ز خلق خاکی و آبی کننده مستثنی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>عطا کننده به او وعدهٔ بعید به موت</p></div>
<div class="m2"><p>بقا دهنده به این تا قریب صبح جزا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به بانگ صیحه روح‌الامین ز قوم شعیب</p></div>
<div class="m2"><p>دهنده خرمن جانها به تند باد فنا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>قوی کنندهٔ دست کلیم لجه شکاف</p></div>
<div class="m2"><p>روان کنندهٔ احکام وی به چوب و عصا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در آب کوچه پدید آورنده از هر سو</p></div>
<div class="m2"><p>به محض صنع مشبک کننده دریا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>درآورنده موسی ز گرد راه به بحر</p></div>
<div class="m2"><p>روان کنندهٔ فرعون مدبرش ز قفا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز انتقام به زاری کشنده فرعون</p></div>
<div class="m2"><p>وز التفات به ساحل کشنده موسی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به بطن حوت مقید کنندهٔ یونس</p></div>
<div class="m2"><p>به جرم سرکشی از قوم مبتلا به بلا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دگر به لطف ز قید جسد گداز چنان</p></div>
<div class="m2"><p>گرفته دست امید افکننده‌اش به عرا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به مال و ملک و باولاد و عترت ایوب</p></div>
<div class="m2"><p>زننده برق فنا وز قفا دهندهٔ بقا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مزاج موم به آهن ده از ید داود</p></div>
<div class="m2"><p>به زیر ران سلیمان ستور کش ز صبا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به عهد شیب ز همخوابه عقیم‌الطبع</p></div>
<div class="m2"><p>به حضرت زکریا دهنده یحیا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز ابر صلب بشر قطره ناچکانیده</p></div>
<div class="m2"><p>صدف گران کن مریم ز گوهر عیسا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به یک اشاره ز انگشت آفتاب رسل</p></div>
<div class="m2"><p>محمد عربی شاه یثرب و بطحا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شکاف در قمر افکن به آسمان بلند</p></div>
<div class="m2"><p>به دهر غلغله افکن ز بانگ و اعجبا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مزاج آتش سوزنده را رماننده</p></div>
<div class="m2"><p>ز قصد موی دلاویز بوی آن مولا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>برای گفتن تسبیح خویش در کف وی</p></div>
<div class="m2"><p>زبان دهنده و ناطق کننده حصبا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بذئب و ضب سخن آموز کز نبوت او</p></div>
<div class="m2"><p>خبر دهنده به ناقاتلان آن دعوا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز دشت سوی وی اشجار را دواننده</p></div>
<div class="m2"><p>که ستر خویش کند آن یگانه دو سرا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مکان دهندهٔ آن مهر منجلی در غار</p></div>
<div class="m2"><p>کشان ز تار عناکب بر او نقاب خفا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سر نیاز غضنفر نهنده بر ره عجز</p></div>
<div class="m2"><p>بر کمینه محبش به کوری اعدا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به دست خادم وی چوبی از ارادهٔ او</p></div>
<div class="m2"><p>بدل کننده به شمع منیر شعشعه زا</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گه از میان دو انگشت معجز آثارش</p></div>
<div class="m2"><p>به آب مرحمت آتش فشان مسرب‌ها</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گه از کفش به طعام قلیل بخشنده</p></div>
<div class="m2"><p>کفایتی که به خلق کثیر کرده وفا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هم از سحاب برد سایبان فرازنده</p></div>
<div class="m2"><p>هم از تنش نرساننده سایه بر غبرا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>برآورنده ز حنانه دور ازو ناله</p></div>
<div class="m2"><p>چو تکیه‌گاه دگر شد ز منبرش پیدا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>زبان به بره بریان دهنده تا نشود</p></div>
<div class="m2"><p>ز شکر انا املح دهان به زهر آلا</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>لبن کش از بز پستان اثر ندیده ز شیر</p></div>
<div class="m2"><p>به یمن مس سر انگشت آن طلم گشا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کننده شجر از جا برای معجز او</p></div>
<div class="m2"><p>کننده ره سپرش سوی وی به یک ایما</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>دگر باره حکمش دو نیم سازنده</p></div>
<div class="m2"><p>کشنده نیمی از آنجا و در کشنده به جا</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>مراجعت ده نیمی دگر به موضع خویش</p></div>
<div class="m2"><p>که جلوه‌گر شود از هر دو وحدت اولا</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به سرعتی گذراننده‌اش ز هفت سپهر</p></div>
<div class="m2"><p>برای گفتن اسرار خود شب اسرا</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>که از حرارت بستر هنوز بود اثر</p></div>
<div class="m2"><p>به خوابگه چو ز معراج شد رجوع نما</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به یکدو چشم زدن ز آب چشمه دهنش</p></div>
<div class="m2"><p>دهنده چشم رمد دیده را کمال شفا</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ید مؤید حیدر علی عالی قدر</p></div>
<div class="m2"><p>کننده در خیبر کننده در هیجا</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>عنان مهر ز مغرب کشنده تا نزند</p></div>
<div class="m2"><p>نماز کامل او خیمه در فضای قضا</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سخن به گوش رسان وی از زبان زمین</p></div>
<div class="m2"><p>شب وقوع زفافش به بهترین نسا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>پی جواب حسن در سؤال ابن اخی</p></div>
<div class="m2"><p>به نطق ضبی زبان بسته را لسان آرا</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>غزاله را بندائی روان کننده ز دشت</p></div>
<div class="m2"><p>به مسجد از پی تسکین سیدالشهدا</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تکلم از حجرالاسود آورنده به فعل</p></div>
<div class="m2"><p>به استغاثه سجاد آن محیط بکا</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>به باقر از لغت گرگ آگهاننده</p></div>
<div class="m2"><p>حقیقت مرض جفت وی برای دوا</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>دهنده از دم صادق به چار طیر قتیل</p></div>
<div class="m2"><p>حیات نو که خلیل این چنین نمود احیا</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به آب چاه نداده که دلو افتاده</p></div>
<div class="m2"><p>پی طهارت کاظم ز ته برد بالا</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>به شیر پرده حوالت کن هلاک عدو</p></div>
<div class="m2"><p>پی رضای امام امم علی رضا</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>به محه‌ای ثمرتر ز نخل خشگ رسان</p></div>
<div class="m2"><p>ز فیض آب وضوی تقی شد اتقا</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>صفای جان صعالیک ده ز حور و قصور</p></div>
<div class="m2"><p>برغم باز رهان نقی در آن ماوا</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>به صیقل سر انگشت نور بخش ز کی</p></div>
<div class="m2"><p>برون ز دیده اعمی برنده رنگ عما</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>هزار ساله شرافت به مهد مستی بخش</p></div>
<div class="m2"><p>ز مهدی آن مه غایب به غیبت کبرا</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ز نور مخفی او تا به انقراض جهان</p></div>
<div class="m2"><p>فروغ ده به چراغ بقیه دنیا</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>در التفات نهانی به این اجله دین</p></div>
<div class="m2"><p>که حصر معجزشان نیست کم ز حصر و حصا</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>اگر نه طی مباحث شود چگونه بود</p></div>
<div class="m2"><p>به قدر شاهد معنی لباس لفظ رسا</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>درین قصیده که سر رشتهٔ کلام کشید</p></div>
<div class="m2"><p>به یک خزانه گهر جمله ناگزیر احصا</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ملول اگر نشدی باش مستمع که کنم</p></div>
<div class="m2"><p>قصیده‌ای دگر از بحر معرفت انشاء</p></div></div>