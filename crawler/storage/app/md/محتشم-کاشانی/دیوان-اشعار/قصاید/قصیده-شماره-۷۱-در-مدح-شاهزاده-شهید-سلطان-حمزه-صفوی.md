---
title: >-
    قصیدهٔ شمارهٔ ۷۱ - در مدح شاهزاده شهید سلطان حمزه صفوی
---
# قصیدهٔ شمارهٔ ۷۱ - در مدح شاهزاده شهید سلطان حمزه صفوی

<div class="b" id="bn1"><div class="m1"><p>مژده عالم را که دهر از امر رب‌العالمین</p></div>
<div class="m2"><p>بهر شاه نوجوان رخش خلافت کرد زین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاتم شاهنشهی را بهر آن گیتی پناه</p></div>
<div class="m2"><p>کنده حکاک قضا الملک منی بر نگین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امر عالی را به امر عالی او عنقریب</p></div>
<div class="m2"><p>در فرامین گشته فرمان همایون جانشین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوس شادی داده صد نوبت به نام او صدا</p></div>
<div class="m2"><p>بر کجا بر پیشگاه غرفهٔ چرخ برین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر زمین بهرجلوس آن جلیس تخت و بخت</p></div>
<div class="m2"><p>سوده هر جانب سریر خسروی صد ره جبین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خطبها بهر لباس تازه افکنده ببر</p></div>
<div class="m2"><p>همچو بسم‌الله بیرون کرده دست از آستین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سکه‌ها بهر ملاقات زر نو سینه چاک</p></div>
<div class="m2"><p>تا زند از عشق خود را بر درمهای ثمین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر زر خورشید هم نامش توان دیدن اگر</p></div>
<div class="m2"><p>دیدن اندر وی تواند چشم عقل دوربین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وه چه نامست این که می‌بارد ازو فتح و ظفر</p></div>
<div class="m2"><p>صاحب نام آن که می‌نازد به او دنیا و دین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باعث تعمیر عالم پاسبان بحر و بر</p></div>
<div class="m2"><p>مایهٔ تخمیر آدم قهرمان ماء و طین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاه سلطان حمزه خاقان قضا فرمان که هست</p></div>
<div class="m2"><p>کمترین طغراکش احکام او طغرل تکین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن که در آغاز عمر از غیرت دین هیچ‌جا</p></div>
<div class="m2"><p>نیستش آرامگاهی در جهان جز صدر زین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وانکه بار منتش خم کرده پشت آسمان</p></div>
<div class="m2"><p>بس که می‌پردازد از اعدای دین روی زمین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غیر او فردی که دید از پادشاهان کو بود</p></div>
<div class="m2"><p>روز و شب بهر جهاد از صدر زین مسند گزین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اوست در خفتان دیگر یا برون آورده سر</p></div>
<div class="m2"><p>حمزهٔ صاحبقران از جیب آن نصرت قرین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ابر اگر بردارد از دریای استیلاش آب</p></div>
<div class="m2"><p>شیر برفین برکند گوش از سر شیر عرین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیست چندان خاک کز ماتم کند خصمش به سر</p></div>
<div class="m2"><p>خاک میدان را به خون از بس که می‌سازد عجین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جان فدای او که در هر ضربت تارک شکافت</p></div>
<div class="m2"><p>آفرین بر دست و تیغش می‌کند جان‌آفرین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آفتاب از بیم سر بر نارد از جیب افق</p></div>
<div class="m2"><p>صبح اگر گیرد به دست آن شاه صفدر تیغ کین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آسیاهائی به خون آورده در گردش که حق</p></div>
<div class="m2"><p>در جهادش داده میراث از امیرالمؤمنین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روم از شور ظهورش چون بود جائی که هست</p></div>
<div class="m2"><p>او در آذربایجان غوغاش در اقلیم چین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیکر آرای عدو گردد مشبک کار دهر</p></div>
<div class="m2"><p>در سپاه او کمان‌داران چه خیزند از کمین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر قد دارئیش دوران لباس کوتهست</p></div>
<div class="m2"><p>تار و پودش گرچه از خیط شهور است و سنین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کرد پیش از عهد شاهی آن چه صد خسرو نکرد</p></div>
<div class="m2"><p>ملک را می‌باید الحق مالک‌الملکی چنین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شاهد حقیتش هم بس به قانون جمل</p></div>
<div class="m2"><p>این که سلطان حمزه یکسانست با حق مبین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حق مبین گشته از نقش حروف اسم او</p></div>
<div class="m2"><p>تا زوال دشمنان باطلش گردد یقین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قلعهٔ تبریز تا بستاند از رومی به جنگ</p></div>
<div class="m2"><p>گفتم از بهر تفال یکه مصراعی متین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کز قفای فتح از آن گردد دو تاریخ آشکار</p></div>
<div class="m2"><p>دال بر اقبال آن جنگ‌آور قسور کمین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون ستاند قلعه و تاریخها پر شد به کو</p></div>
<div class="m2"><p>قلعه از رومی ستاندی شاه جم قدرآفرین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>با دعای اهل کاشان این دعاگو محتشم</p></div>
<div class="m2"><p>آسمانها را کند پر ز اولین تا هفتمین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بهر آن دارای هفت اقلیم باردار حافظی</p></div>
<div class="m2"><p>کاسمان نامش کند جوشن زمین حصن حصین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>داعیان را نیز فیض از مبداء فیاض باد</p></div>
<div class="m2"><p>شهریاری هم که هست ارباب دعوت را معین</p></div></div>