---
title: >-
    قصیدهٔ شمارهٔ ۷ - وله ایضا من بدایع افکاره
---
# قصیدهٔ شمارهٔ ۷ - وله ایضا من بدایع افکاره

<div class="b" id="bn1"><div class="m1"><p>سرورا ادعیه‌ات تا برسانم به نصاب</p></div>
<div class="m2"><p>از دعا هر نفسم نقش جدیدیست بر آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپه ادعیه‌ام روی فلک می‌گیرد</p></div>
<div class="m2"><p>تا تو را می‌رسد از روی زمین پا به رکاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچنانست دلم بهر تو از ادعیه گرم</p></div>
<div class="m2"><p>که فلک از نفسم می‌شنود بوی کباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌کنم هر سو مویت به دعائی پیوند</p></div>
<div class="m2"><p>من که پیوند بر دیدهٔ خویشم از خواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرده‌ای داعیهٔ حرب و حصارت شده است</p></div>
<div class="m2"><p>آن قدر ادعیه کافزون ز شمار است و حساب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از که از گوشه‌نشینی که به بیداری کرد</p></div>
<div class="m2"><p>چشم خود را تبه از بهر تو در عین شباب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر خود خصمت اگر قلعهٔ آهن سازد</p></div>
<div class="m2"><p>عنکبوتیست که بر خود تند از لعب لعاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای گزین طیر همایون که درین طرفه چمن</p></div>
<div class="m2"><p>شاهبازی تو و بدخواه سیه بخت غراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بادی از جنبش شهبال تو می‌باید و بس</p></div>
<div class="m2"><p>که شود در صف هیجا سپه آشوب ذباب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بال بگشای که از گلشن روم آمده‌اند</p></div>
<div class="m2"><p>فوجی از صعوه به صباغی چنگال عقاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این مثل ورد زبانهاست که دیر آوردست</p></div>
<div class="m2"><p>هست یعنی رهی از صوب تامل به صواب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کار چون هست به هنگامی و وقتی موقوف</p></div>
<div class="m2"><p>چه تقدم چه تاخر چه تانی چه شتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تیر و شمشیر شوند از عمل خود معزول</p></div>
<div class="m2"><p>در سپاهی که نگاهی کنی از عین عتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ذره ذره مگر از آتش غم افروزی</p></div>
<div class="m2"><p>ورنه اجرام بر افلاک بسوزند ز تاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>موج بحر غضبت خیمه و خر گاه عدو</p></div>
<div class="m2"><p>عنقریب است که آورده فرو همچو حباب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>محتشم دعوت خود کن یزک لشگر و ساز</p></div>
<div class="m2"><p>خانهٔ دشمن خان پیشتر از حرب خراب</p></div></div>