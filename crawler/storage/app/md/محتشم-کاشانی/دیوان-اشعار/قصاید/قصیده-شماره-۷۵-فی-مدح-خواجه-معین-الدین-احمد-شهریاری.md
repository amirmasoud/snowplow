---
title: >-
    قصیدهٔ شمارهٔ ۷۵ - فی مدح خواجه معین‌الدین احمد شهریاری
---
# قصیدهٔ شمارهٔ ۷۵ - فی مدح خواجه معین‌الدین احمد شهریاری

<div class="b" id="bn1"><div class="m1"><p>بر اشراف این عید و آن کامکاری</p></div>
<div class="m2"><p>مبارک بود خاصه بر شهریاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کزین گوهر افسر سر بلندی</p></div>
<div class="m2"><p>مهین داور کشور نامداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معین ملل کز ازل قسمتش زد</p></div>
<div class="m2"><p>به بخت همایون در بختیاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قضا صولتی کاسمان سده‌اش را</p></div>
<div class="m2"><p>کند بوسه کاری به صد خاکساری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدر قدرتی کز صفات کمینش</p></div>
<div class="m2"><p>یکی نام دارد سپهر اقتداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جنب نعالش که پایان ندراد</p></div>
<div class="m2"><p>کجا در حسابست عالم مداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در اطراف صیتش چو باد است پویان</p></div>
<div class="m2"><p>بر اشراف حکمش چو آبست جاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو او کس نکرد از خدا بندگان هم</p></div>
<div class="m2"><p>الا ای به خلق آیت رستگاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به آن کبریا و شکوه و جلالت</p></div>
<div class="m2"><p>حلیمی و بی‌کبری و بردباری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ازل تا ابد از خرابیست ایمن</p></div>
<div class="m2"><p>بنای جلالت ز محکم حصاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ازین هم فزون پایهٔ دولتت را</p></div>
<div class="m2"><p>ز دارای تو عهد باد استواری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گل گلشن شهریاری علیخان</p></div>
<div class="m2"><p>که در فیض باریست ابر بهاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جلیل اختر برج عالی مکانی</p></div>
<div class="m2"><p>جلی سکهٔ نقد کامل عیاری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شمارند صاحب شعوران دوران</p></div>
<div class="m2"><p>زادنی صفاتش حکومت شعاری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ضمیریست در صبح نو عهدی او را</p></div>
<div class="m2"><p>فرزوان تر از آفتاب نهاری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سپهر از برایش عروس جهان شد</p></div>
<div class="m2"><p>به عقد دوام است در خواستگاری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زند ابرش اندر عنان قره هرگه</p></div>
<div class="m2"><p>که طبعش کند میل ابرش سواری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جز این از وقارش نگویم که او را</p></div>
<div class="m2"><p>هجائی و ذمیست گردون وقاری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طویل البقا باد عزمش که عالم</p></div>
<div class="m2"><p>به او تا ابد دارد امیدواری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جهان داورا محتشم بندهٔ تو</p></div>
<div class="m2"><p>که لال است در شکر نعمت گذاری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ازین نظم مقصودش اینست کورا</p></div>
<div class="m2"><p>نه از سلک مدحت فروشان شماری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز دنبال هم داد صد غوطه او را</p></div>
<div class="m2"><p>نوال تو در لجهٔ شرمساری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مسازش طمع پیشه ترسم برآید</p></div>
<div class="m2"><p>سر عزتش از گریبان خواری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به جان آفرینی که در آفرینش</p></div>
<div class="m2"><p>تو را داد این امتیازی که داری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به بطحایئیی کایزدش خواند احمد</p></div>
<div class="m2"><p>تو را نیز نگذاشت زان رتبه عاری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به خیبر گشائی که از خیل خاصان</p></div>
<div class="m2"><p>تو را داد در شهر خود شهریاری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که گر بگذرانی سرم را ز گردون</p></div>
<div class="m2"><p>و گر مغزم از کاسهٔ سر برآری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سر موئی از من نیابی تفاوت</p></div>
<div class="m2"><p>در اخلاص و دلسوزی و جان سپاری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دعائیست بر لب یقین الاجابه</p></div>
<div class="m2"><p>که حاجت ندارد بالحاح و زاری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بود تا تو را شیوهٔ دیوان نشینی</p></div>
<div class="m2"><p>بود تا مرا پیشهٔ دیوان نگاری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در اوصافت ای صدر دیوان نشینان</p></div>
<div class="m2"><p>نی کلک من باد در شهد باری</p></div></div>