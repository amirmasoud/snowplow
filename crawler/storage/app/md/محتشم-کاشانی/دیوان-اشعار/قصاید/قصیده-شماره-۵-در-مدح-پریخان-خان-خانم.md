---
title: >-
    قصیدهٔ شمارهٔ ۵ - در مدح پریخان خان خانم
---
# قصیدهٔ شمارهٔ ۵ - در مدح پریخان خان خانم

<div class="b" id="bn1"><div class="m1"><p>تا نقش ناتوانی من چرخ زد بر آب</p></div>
<div class="m2"><p>شد چون حباب خانهٔ جمعیتم خراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کاو کاو تیشه پیکر خراش درد</p></div>
<div class="m2"><p>بنیاد من رساند سپهر نگون به آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جسمم ز تاب درد سراسیمه کشتی است</p></div>
<div class="m2"><p>لنگر گسل ز جنبش دریای اضطراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز انسان که گرگ در غنم افتد غنیم‌وار</p></div>
<div class="m2"><p>در لشکر حواس من افکنده انقلاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهرم به حال مرگ نشانداست در حیات</p></div>
<div class="m2"><p>دورم شراب شیب چشانده است در شباب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیوند تن نمی‌گسلد جان که تا رهم</p></div>
<div class="m2"><p>با آن که چرخ می‌دهدش صد هزار تاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرغیست بخت سوخته من که آمده</p></div>
<div class="m2"><p>هم پیشهٔ سمندر وهم کسوت عراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>افسرده‌ام چنان که اگر آه سرد من</p></div>
<div class="m2"><p>بر دوزخ افکند گذراند ازدش زتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اما خوشم که اخگر خس پوش دل ز غیب</p></div>
<div class="m2"><p>می‌آید از خجسته نسیمی به التهاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بوی بهشت می‌شنوم از ریاض لطف</p></div>
<div class="m2"><p>گوئی خلاص می‌شوم از دوزخ عذاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از درگهی که هست سگش آهوی حرم</p></div>
<div class="m2"><p>در گردنم به یک کشش افکنده صد طناب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لیکن چو نیست پای تردد چه سان شوم</p></div>
<div class="m2"><p>بهر شرف ز سجده آن سده بهره یاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یک ذره‌ام توان چو نمانداست چون کنم</p></div>
<div class="m2"><p>خورشیدوار ناصیه سائی بر آن جناب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برخیز ای صبا که ازین پس نمی‌شود</p></div>
<div class="m2"><p>شوق سبک عنان متحمل گران زکاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از من دعا و از تو شدن حاملش چنان</p></div>
<div class="m2"><p>کارام را وداع کند عزمت از شتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از من ثنا و از تو رساندن دوان دوان</p></div>
<div class="m2"><p>جائی که قطرهٔ بحر شود ذرهٔ آفتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یعنی جناب عالی بلقیس روزگار</p></div>
<div class="m2"><p>یعنی حریم حرمت نواب مستطاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شهزادهٔ زمان و زمین شمسهٔ جهان</p></div>
<div class="m2"><p>زهرای زهره حاجبهٔ مریم احتجاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاه‌پری و انس پری خان که گر بدی</p></div>
<div class="m2"><p>بلقیس پادشاهی ازو کردی اکتساب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خیرالنساء عهد که دوران جز او نداد</p></div>
<div class="m2"><p>عز مشارکت احدی را به این خطاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>معصومهٔ زمان که نبات زمانه‌اند</p></div>
<div class="m2"><p>از احتسا عصمت او عصمت احتساب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هودج کشان شخص عفافش نمی‌کشند</p></div>
<div class="m2"><p>بر دیدهٔ ملک ز ورع دامن ثیاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گردیده دایم‌الحرکت از عبادتش</p></div>
<div class="m2"><p>دست فرشتگان ز رقم کردن ثواب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>می‌سنجدش به زهد و طهارت خرد مدام</p></div>
<div class="m2"><p>با طاهرات حجره زهرا و بوتراب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از بهر پادشاهی نسوان قضا نکرد</p></div>
<div class="m2"><p>فردی ز کاینات به این خوبی انتخاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مهر فلک کنیزک خورشید نام اوست</p></div>
<div class="m2"><p>کاندر پس سه پرده نشست است از حجاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وز شرم کس نکرده نگه در رخش درست</p></div>
<div class="m2"><p>از بس که دارد از نظر مردم اجتناب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در خواب نیز تا نتواند نظر فکند</p></div>
<div class="m2"><p>نامحرمی بر آن مه خورشید احتجاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نبود عجب اگر کند از دیده ذکور</p></div>
<div class="m2"><p>معمار کارخانه احساس منع خواب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خود هم به عکس صورت خود گر نظر کند</p></div>
<div class="m2"><p>ترسم که عصمتش کند اعراض در عتاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فرمان دهد که عکس پذیری به عهد او</p></div>
<div class="m2"><p>بیرون برد قضا هم از آئینه هم ز آب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آن مریم زمان که به عفت سرای او</p></div>
<div class="m2"><p>بوی کسی نبرده نسیمی به هیچ باب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از عصمتش بدیع مدان کز کمال شرم</p></div>
<div class="m2"><p>دارد جمال خود ز ملک نیز در نقاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر خاکروبه حرم او که می‌برند</p></div>
<div class="m2"><p>از بهر کحل دیدهٔ ملایک به صد شتاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در دامن سحاب فتد ذره‌ای از آن</p></div>
<div class="m2"><p>تا دامن ابد دمد از خاک مشگناب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر بام قصر اگر شب مهتاب پا نهد</p></div>
<div class="m2"><p>گردون به چشم ماه کشد میل از شهاب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>می‌بود مهر اگر چو کنیزان دیگرش</p></div>
<div class="m2"><p>هرگز نمی‌فکند ز رخ برقع سحاب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در جنب فر معجر ادنی کنیز او</p></div>
<div class="m2"><p>آرد شکوه افسر قیصر که در حساب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هست از غرور صنعه تانیث صعوه را</p></div>
<div class="m2"><p>در عهد او نظر به حقارت سوی عقاب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گر بگذرد بر آب نسیم حمایتش</p></div>
<div class="m2"><p>گدست صباد دگر ندرد پردهٔ حباب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ناهید همچو عود بر آتش فکنده چنگ</p></div>
<div class="m2"><p>تقویش ساز کرده چو قانون احتساب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چون گشته شخص شوکت او مایل رکوب</p></div>
<div class="m2"><p>گردون رکاب داری او کرده ارتکاب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سرلشگران عسکر او صاحب الرؤس</p></div>
<div class="m2"><p>گردن کشان لشگر او مالک الرقاب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هردم کند ظفر ز پی زیب دولتش</p></div>
<div class="m2"><p>دست عروس ملک به خون عدو خضاب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از باد حملهٔ سپه او سپاه خصم</p></div>
<div class="m2"><p>بر هم خورد چنان که ز صرصر صف ذباب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چون خلق در مقام سبک روحی آردش</p></div>
<div class="m2"><p>در زیر پای او نبود مور در عذاب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اما نهد به هیبت اگر پای بر زمین</p></div>
<div class="m2"><p>بیرون برد مهابت او جنبش از دواب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بر درگهش گدای کمین مملکت مدار</p></div>
<div class="m2"><p>در خدمتش غلام کمین سلطنت مب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ای سجدهٔ درت همه را مقصد و مرام</p></div>
<div class="m2"><p>وی خاک درگهت همه را مرجع و مب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ای قدرت تو چشمهٔ گشاینده از رخام</p></div>
<div class="m2"><p>وی حکمت تو تشنهٔ نوازنده از سراب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>رای تو در امور کلید در صلاح</p></div>
<div class="m2"><p>فکر تو در مهام دلیل ره صواب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>محتاج یک حدیث توام در مهم خویش</p></div>
<div class="m2"><p>ای هر حدیث از تو برابر به صد کتاب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سی‌سال شد که طبع من از گوهر سخن</p></div>
<div class="m2"><p>گردیده گوشواره کش گوش شیخ و شباب</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از معنی لباب کلامست نظم من</p></div>
<div class="m2"><p>تحید و نعت و منقبتم لب آن لباب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چون سینهٔ صدف صدف سینه‌ها تمام</p></div>
<div class="m2"><p>در عهد من گران شده از گوهر مذاب</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سرتاسر جهان ز در نظم من پر است</p></div>
<div class="m2"><p>الا خزانه دل نواب کامیاب</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>من در زمان این ملک مشتری غلام</p></div>
<div class="m2"><p>با این همه در رچو محیطم در اضطراب</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>یک در به بیع طبع همایون او رسان</p></div>
<div class="m2"><p>تا وارهم ز فاقه من خانمان خراب</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بر جان من ترحمی ای ابر مرحمت</p></div>
<div class="m2"><p>کز تاب آفتاب حوادث شدم کباب</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از کاینات رو به تو آورده محتشم</p></div>
<div class="m2"><p>ای قبلهٔ مراد ازو روی بر متاب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کاندر ستایش تو ز درهای مخزنی</p></div>
<div class="m2"><p>داده است دقت نظرش داد انتخاب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>وقت دعا رسید دعائی که از مجیب</p></div>
<div class="m2"><p>بر اوج لامکان به سمعنا شود مجاب</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تا در دعا تضرع والحاح سائلان</p></div>
<div class="m2"><p>در جنبش آورد به اجابت لب جواب</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بهر تو دعا که کند در دلی گذر</p></div>
<div class="m2"><p>از دل گذر نکرده به لب باد مستجاب</p></div></div>