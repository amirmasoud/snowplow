---
title: >-
    قصیدهٔ شمارهٔ ۴۲ - وله ایضا من لطف انفاسه فی مدح اعتمادالدوله میرزا سلمان جابری
---
# قصیدهٔ شمارهٔ ۴۲ - وله ایضا من لطف انفاسه فی مدح اعتمادالدوله میرزا سلمان جابری

<div class="b" id="bn1"><div class="m1"><p>در وثاق خاص خود گرد یساق افشاند باز</p></div>
<div class="m2"><p>آصف کرسی نشین مسند فراز سرفراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشکوه دور باش صولت هیبت لزوم</p></div>
<div class="m2"><p>با فروغ آفتاب دولت حاسد گداز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وه چه آصف آن که در حصر صفاتش لازم است</p></div>
<div class="m2"><p>با علو فطرت و طی لسان عمر دراز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اصل قانون بزرگی میرزا سلمان که هست</p></div>
<div class="m2"><p>بینوایان را ز کوچک پروری‌ها دلنواز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دعای او به آهنگ اجابت در عراق</p></div>
<div class="m2"><p>راست جوش کاروانست از صفاهان تا حجاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک و تازی از مخالف تا مؤالف نسپرند</p></div>
<div class="m2"><p>راه ایوان همایون گر ازو نبود جواز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رای ملک آرا که کرد از دانش عالم فروز</p></div>
<div class="m2"><p>بی‌مشقت بر رخ دشمن در عالم فروز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نبودی سد او بودی چو سیلاب نگون</p></div>
<div class="m2"><p>ظلم را بر ملک عیش ترک و تازی ترکنار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست نازش بر نیاز پادشاهان دور نیست</p></div>
<div class="m2"><p>گر به ایجاد چنین ذاتی بنازد بی‌نیاز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کارسازیهای او در سازگار سلطنت</p></div>
<div class="m2"><p>هست نقش منتخب از نقش دان کارساز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>محض اعجاز است در اثنای حکم دار و گیر</p></div>
<div class="m2"><p>از تعدی اجتناب و از تطاول احتراز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر خلاف رای او گر آسمان را از کمان</p></div>
<div class="m2"><p>تیر تدبیری جهد گرداندش تقدیر باز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خوانده خوان نوال از همت او جن و انس</p></div>
<div class="m2"><p>رانده ملک وجود از بخشش او حرص و آز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای صبا در گوش شه گو کای سلیمان زمان</p></div>
<div class="m2"><p>بر سلیمان ناز کن اما به این آصف بناز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>می‌شود ز آهنگ دور اما محل نفخ صور</p></div>
<div class="m2"><p>بهر دفع ظلم قانونی که عدلش کرده ساز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در حقیقت آن قدرها از مزاج اوست فرق</p></div>
<div class="m2"><p>بر مزاج پادشاهان کز حقیقت بر مجاز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای مهین آصف که بر گرد سرت در گردشست</p></div>
<div class="m2"><p>مرغ روح آصف‌بن برخیا از اهتزاز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برخی از اوصاف ذاتت طبع ازین طرز جدید</p></div>
<div class="m2"><p>تا نکرد تا انشا به کام دل نشد دیوان طراز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نیست روزی کز برای ضبط گیتی نشنود</p></div>
<div class="m2"><p>گوش تقدیر از زبان شخص تدبیر تو راز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آستانت را خرد با آسمان سنجید و یافت</p></div>
<div class="m2"><p>عرش آن را در نشیب وفرش این را برفراز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر کنی در ایلغاری حکم بی‌مهلت روند</p></div>
<div class="m2"><p>بختیان آسمان در زیر بارت بی‌جهاز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هست در چنگال عصفور تو عنقای فلک</p></div>
<div class="m2"><p>راست چون پر کنده گنجشکی به چنگ شاهباز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مصر دولت را عزیزی و به منت می‌کشند</p></div>
<div class="m2"><p>یوسفان با آن همه نازک دلیها از تو ناز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بس که با یک یک ز مملوکان خویشی مهربان</p></div>
<div class="m2"><p>کار عشق افتاده یک محمود را با صد ایاز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خصم کج بنیاد اگر زد با تو لاف همسری</p></div>
<div class="m2"><p>راستان را در میان باز است چشم امتیاز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در مشام جان خیال عطر نرگس پختهٔ عشق</p></div>
<div class="m2"><p>گو علم برمیفراز از خامی سودا پیاز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ناتوان بازار رشک از بهر خصم ناتوان</p></div>
<div class="m2"><p>گرم می ساز و بهر وجهش که خواهی می‌گداز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دشمن آهن دلت از سختی اندر بغض و کین</p></div>
<div class="m2"><p>کام خواهد یافتن آخر ولی در کام گاز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>داری اندر جمله معنی هزاران پردگی</p></div>
<div class="m2"><p>همچو من شیدای هر یک صد هزاران عشقباز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نظم لعب آیین ما نسبت به آن لفظ متین</p></div>
<div class="m2"><p>چون معلق‌های طفلانست در جنب نماز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا ره خواهش به دست آز پوید پای فقر</p></div>
<div class="m2"><p>تا در دلها ز تاب فقر کوبد دست آز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون در رزق خدا بر روی درویش و غنی</p></div>
<div class="m2"><p>بر گدا و محتشم بادا در لطف تو باز</p></div></div>