---
title: >-
    قصیدهٔ شمارهٔ ۶۴ - فی مدح ولده ولیجان سلطان ترکمان گفته
---
# قصیدهٔ شمارهٔ ۶۴ - فی مدح ولده ولیجان سلطان ترکمان گفته

<div class="b" id="bn1"><div class="m1"><p>چو دی نسیم سحر خورد بر مشام جهان</p></div>
<div class="m2"><p>صبا رسید و رسانید بوی روضهٔ جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتاد زمزمه ذوقناک در افواه</p></div>
<div class="m2"><p>که یافت لذت از آن صدهزار کام و زبان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دشت خاست غباری که فیض نور از وی</p></div>
<div class="m2"><p>زیاده از دگران یافت دیدهٔ نگران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صدای نوبت دولت بلند گشت و درید</p></div>
<div class="m2"><p>فلک ز صولت آن پرده‌های گوش کران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منادی طرب آهنگ بانگ زد که رسید</p></div>
<div class="m2"><p>مواکب ظفر آثار شهریار جهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امیرزاده عالی نسب ولیجان بیک</p></div>
<div class="m2"><p>ولیعهد ابد انتساب خان زمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزرگ فر بلند اختر قوی فطرت</p></div>
<div class="m2"><p>جلیل قدر فلک رتبهٔ رفیع مکان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز رتبهٔ طاق میان هزار یکه سوار</p></div>
<div class="m2"><p>ز جذبهٔ فرد میان هزار یکه جوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بزم ازو متنزل سران افسر بخش</p></div>
<div class="m2"><p>به رزم ازو متوهم ملوک ملک ستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شود چو گرم عطا آه از ذخایر بحر</p></div>
<div class="m2"><p>دهد چو داد سخا وای بر دفاین کان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به آن محیط عطا بس خطاست نسبت ابر</p></div>
<div class="m2"><p>که هست او گوهر افشان و ابر قطره چکان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به مجمعی که نباشد ورای خسرو و شاه</p></div>
<div class="m2"><p>بود ز رتبه نشان این چه رتبه است و چه شان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هزار عذر بگوید اگر قضا ناگه</p></div>
<div class="m2"><p>جهد خدنگ قضا بی‌رضای او ز کمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به مهرهٔ کمر کوه اگر اشاره کند</p></div>
<div class="m2"><p>هزار مرحله ره در میان به نوک سنان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به زور خط شعاعی چنان شود سفته</p></div>
<div class="m2"><p>که سر کن فیکون آشکار گردد از آن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>محل نیزه رساندن ز زورمندی وی</p></div>
<div class="m2"><p>تفاوتی نکند در اثر سنان وبنان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر قضا مدد از وی طلب کند شکند</p></div>
<div class="m2"><p>به زور باد پر پشه پشت پیل دمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بلامکان جهد از هیبتش کرنگ فلک</p></div>
<div class="m2"><p>حواله گر بسرونش کند دوال عنان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مه فلک که به نعل سمند اوست قرین</p></div>
<div class="m2"><p>ستاره‌ایست که با آفتاب کرده قران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به شرح حسن وفایش که شیوهٔ ابدیست</p></div>
<div class="m2"><p>نه عمر نوح وفا می‌کند نه طی لسان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز قدسیمبران بزم او عجب چمنی است</p></div>
<div class="m2"><p>که نخل‌هاش چمانند و سروهاش روان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز روی لاله‌رخان مجلسش عجب باغی است</p></div>
<div class="m2"><p>که از شراب و خمار آمدش بهار و خزان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز پرتو نظرش حسن رایت پرورشی</p></div>
<div class="m2"><p>که طعنه بر پریان می‌زنند آدمیان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بلند رتبه امیرا کسی که از توفیق</p></div>
<div class="m2"><p>گرفته بود زمین و زمان بتیغ زبان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فکنده بود به جائی کمند نظم بلند</p></div>
<div class="m2"><p>که می‌نمود از آن کوتهی کمند گمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنان زبون شده امروز کز مشاهده‌اش</p></div>
<div class="m2"><p>زمین پر است ز سیلاب چشم اهل زمان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بلیه‌ای که بر او آسمان گماشته است</p></div>
<div class="m2"><p>گران‌تر است ز حمل زمین تحمل آن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مگر امانی و آمالش از حمایت تو</p></div>
<div class="m2"><p>روا شوند که یابد از آن بلیه امان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به کیمیای نظر گر مس وجودش را</p></div>
<div class="m2"><p>توجه تو کند زر بر این عمل چه زیان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سخن تمام چو شد محتشم برای دعا</p></div>
<div class="m2"><p>به جنبش آر زمانی زبان ادعیه خوان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همیشه تا بود از روز و روزگار اثر</p></div>
<div class="m2"><p>مدام تا بود از شاه و شهریار نشان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به روزگار دراز آن خدیو ملک طراز</p></div>
<div class="m2"><p>بود سریر نشین بلکه پادشاه نشان</p></div></div>