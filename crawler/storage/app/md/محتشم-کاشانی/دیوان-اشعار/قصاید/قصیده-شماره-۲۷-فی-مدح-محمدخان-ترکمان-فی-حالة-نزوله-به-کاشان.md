---
title: >-
    قصیدهٔ شمارهٔ ۲۷ - فی مدح محمدخان ترکمان فی حالة نزوله به کاشان
---
# قصیدهٔ شمارهٔ ۲۷ - فی مدح محمدخان ترکمان فی حالة نزوله به کاشان

<div class="b" id="bn1"><div class="m1"><p>دوش ز ره قاصدی خرم و خندان رسید</p></div>
<div class="m2"><p>کز نفس او به دل رایحهٔ جان رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سرو بر چون فشاند گرد معنبر نسیم</p></div>
<div class="m2"><p>فیض به پست و بلند از اثر آن رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی بشارت نمود زآینهٔ صدق و گفت</p></div>
<div class="m2"><p>از پی آئین و عدل داور دوران رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیک صبا هم رساند مژده کز اقبال و بخت</p></div>
<div class="m2"><p>بر در شهر سبا تخت سلیمان رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از عقبش فوج فوج لشگری آمد گران</p></div>
<div class="m2"><p>شور ز گردون گذشت گرد به کیوان رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا شود اطفای ظلم بر سر ذرات ملک</p></div>
<div class="m2"><p>گرمتر از آفتاب سایهٔ سبحان رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عزم دل شهریار سوی ره این دیار</p></div>
<div class="m2"><p>بود چنان کز بهار مژده به بستان رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرد بدین سو عبور لشگر عیش و سرور</p></div>
<div class="m2"><p>غصه به تاراج رفت قصه به پایان رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>موکب پر کوکبه با دو جهان دبدبه</p></div>
<div class="m2"><p>از حرکات نسیم غالیه افشان رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرد سپه کوه بر رخ گردون نشست</p></div>
<div class="m2"><p>کوکبهٔ خور شکست دبدبه‌خان رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خان معلی لقب که اسم محمد بر او</p></div>
<div class="m2"><p>خلعت توفیق بود کز بر یزدان رسید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>والی والا سریر آن که بر ایوان قدر</p></div>
<div class="m2"><p>پایهٔ بالائیش تا نهم ایوان رسید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>میر سکندر سپاه آن که به پابوس او</p></div>
<div class="m2"><p>صد جم و دارا چو رفت نوبت خاقان رسید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عازم کاشان هنوز ناشده اندیشه‌اش</p></div>
<div class="m2"><p>طنطنهٔ شوکتش تا به خراسان رسید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غوث بلندست و پس ابر وجودش کزو</p></div>
<div class="m2"><p>سایه بگردون فتاد مایه به عمان رسید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا نپذیرد خلل سلسلهٔ مملکت</p></div>
<div class="m2"><p>سلسله‌ها را تمام سلسله جنبان رسید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باد مرادی برخاست برق رواجی بجست</p></div>
<div class="m2"><p>فلک ز طوفان گذشت ملک به سامان رسید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا شکند در جهان رونق دیوان ظلم</p></div>
<div class="m2"><p>با دو جهان عدل و داد حاکم دیوان رسید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چاره بر ملک را مالک دوران رساند</p></div>
<div class="m2"><p>بس که به چرخ بلند زین بلد افغان رسید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در عظمت هرچه داشت صورت فرض محال</p></div>
<div class="m2"><p>از پی تعظیم او جمله به امکان رسید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روز دغا در مصاف تیغ مبارز شکاف</p></div>
<div class="m2"><p>بر سر فارس چو راند بر فرس آسان رسید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سینهٔ اعدای او خانهٔ زنبور شد</p></div>
<div class="m2"><p>بس که ز شستش بر او ناوک پران رسید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خصم دغا هرکجا کرد ز دستش فراز</p></div>
<div class="m2"><p>مرگ همانجا به او دست و گریبان رسید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بس که شد از هیبتش جان ز بدنها برون</p></div>
<div class="m2"><p>تیغ بهر سو که راند بر تن بی‌جان رسید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جانب او بس که داشت بیش ز امکان فضا</p></div>
<div class="m2"><p>بر سر خصمش اجل پیش ز فرمان رسید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای مه انجم حشم وی ملک محتشم</p></div>
<div class="m2"><p>کز نسقت ملک را کار به سامان رسید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>من برهٔ طاعتت گرچه ز دوران نیم</p></div>
<div class="m2"><p>جان به لب طاقتم از غم دوران رسید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شربت لطفی فرست کاین تن رنجور را</p></div>
<div class="m2"><p>درد کشیدن خطاست حال که درمان رسید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا ز صعود بخار خواهد از ابر بهار</p></div>
<div class="m2"><p>قطره ز بالا فتاد رشحه به بستان رسید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ابر نوال تو را مایه کم از یم مباد</p></div>
<div class="m2"><p>کز تو به هر کس که بود رشحهٔ احسان رسید</p></div></div>