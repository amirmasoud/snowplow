---
title: >-
    قصیدهٔ شمارهٔ ۳۱ - در ستایش جلال‌الدین محمد اکبر پادشاه فرماید
---
# قصیدهٔ شمارهٔ ۳۱ - در ستایش جلال‌الدین محمد اکبر پادشاه فرماید

<div class="b" id="bn1"><div class="m1"><p>چو از جوزا برون تازد تکاور خسرو خاور</p></div>
<div class="m2"><p>تف نعلش برآرد دود ازین دریای پهناور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتد در معدنیان آتشی کز گرمی آهن</p></div>
<div class="m2"><p>زره سازی کند آسان‌تر از داود آهنگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر افتد مرغی از تاب هوا در آتش سوزان</p></div>
<div class="m2"><p>پی دفع حرارت تنگ گیرد شعله را در بر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سمندر گر برون آید ز آتش دوزخی بیند</p></div>
<div class="m2"><p>که تا برگردد از تف هوا در گیردش پیکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنه‌کاران سمندر سان به آتش در روند آسان</p></div>
<div class="m2"><p>نسیمی گر ازین گرما وزد بر عرصهٔ محشر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یخ اندر زیر و آتش بر زبر یابند بالینه</p></div>
<div class="m2"><p>به تخت اخگر و تخت هوا از عجز خاکستر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جز سطح معقر آن هم از نزدیکی آتش</p></div>
<div class="m2"><p>نماند هیچ جز وی مضحل ناگشته از مجمر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نوعی مایعات بیضه گردد صلب از گرمی</p></div>
<div class="m2"><p>که هرچندش به جوشانی شود صلبیتش کمتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نظیر این هوا ظاهر شود اما به شرط آن</p></div>
<div class="m2"><p>که در هر ذره از اجزاش باشد دوزخی مضمر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود در شدت حدت مساوی هر دو را مدت</p></div>
<div class="m2"><p>ازین گرما اگر یخ در گدازید و اگر مرمر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شود نقش حجر زایل ولی از حفظ یزدانی</p></div>
<div class="m2"><p>نگردد زایل از زر سکهٔ شاه جهان پرور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>محیط مرکز دوران طراز سکهٔ شاهی</p></div>
<div class="m2"><p>که می‌گردند گوئی گرد نامش سکه‌ها بر زر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهان سالار اعظم حارس محروسهٔ عالم</p></div>
<div class="m2"><p>قوام طینت آدم دلیل قدرت داور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جلال‌الدین محمد اکبر آن خاقان جم فرمان</p></div>
<div class="m2"><p>حفیظ عالم امکان عزیز خالق اکبر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جهانبانی که گر طالب شود دربسته ملکی را</p></div>
<div class="m2"><p>فلک صد عالم در بسته را به روی گشاید در</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سلیمانی که گر خواهد صبا را ز یرران خود</p></div>
<div class="m2"><p>تکاسف کرده سازد جای یک زین پشت پهناور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قدر امری که گر در قطرهٔ عظم او دمد بادی</p></div>
<div class="m2"><p>کند در شش جهت هفت آسمان را از تخلخل تر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نظیر شام اجلاسش بساط صبح نورانی</p></div>
<div class="m2"><p>عدیل روز اقبالش شب معراج پیغمبر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به یک احسان کند از روی همت کار صد حاتم</p></div>
<div class="m2"><p>به یک سائل دهد در روز بخشش باج صد کشور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برد باد از شکوه صعوهٔ او شوکت عنقا</p></div>
<div class="m2"><p>شود آب از هراس روبه او زهره قصور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زند گر بر زمین رمح دو سر از زورمندیها</p></div>
<div class="m2"><p>رود از ناف گاو و سینهٔ ماهی برون یکسر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صف‌آرای یزک داران خیلش خسرو خاقان</p></div>
<div class="m2"><p>پرستار کشک داران قصرش کسری و قیصر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هنوز اندر دغانا گشته گرد آلود می‌آرد</p></div>
<div class="m2"><p>به جنبش بهر گرد افشاندنش روح‌الامین شهپر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به یک هی بر درد از هم اگر هفتاد صف بیند</p></div>
<div class="m2"><p>در آن مرد آزما میدان و چون حیدر شود صفدر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نچربد یک سر مو راست بر چپ ز اقتدار او</p></div>
<div class="m2"><p>کند چون در کشش تقسیم ترک تارک و مغفر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر جنبد ز جا باد قیامت جنبش قهرش</p></div>
<div class="m2"><p>تزلزل بشکند نه کشتی افلاک را لنگر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سم گاو زمین یابد خبر از زور بازویش</p></div>
<div class="m2"><p>زند چون بر سر شیر فلک گر ز جبل پیکر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر راند به خاور خیل زور آور شود صدجا</p></div>
<div class="m2"><p>خلل از غلظت گرد سپه در سد اسکندر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به عزم کبریا با خسروان گر سنجدش دروان</p></div>
<div class="m2"><p>ز دیوار آید آواز هوالاعظم هوالاکبر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زهی شاه بزرگ القاب کادنی بندگانت را</p></div>
<div class="m2"><p>به خدمت نیز اعظم نویسد ذرهٔ احقر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگر خواهی ز دوران رفع ظلمت در رسد فرمان</p></div>
<div class="m2"><p>که در ظلمات از هر ذره خورشیدی برآرد سر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>و گر تاریک خواهی دهر را چون روز خصم خود</p></div>
<div class="m2"><p>به جای مشعل بیضا برآید دود از خاور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بروز باد اگر خواهی روان جسم جمادی را</p></div>
<div class="m2"><p>جبل را چون حمل در جنبش آرد جنبش صرصر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به جیب جوشن جیشت سراغ مثل اسب خود</p></div>
<div class="m2"><p>در و دروازه کنکان زند هنگامهٔ محشر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وجودنازکت رونق ده بازار حلاجی</p></div>
<div class="m2"><p>هراس نیزه‌ات غارتگر دکان جوشن گر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز تاب شعلهٔ رمحت درخت فتنه بار افکن</p></div>
<div class="m2"><p>ز آب چشمه تیغت نهال فتح بارآور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در آن عالم که می‌گنجد شکوهٔ کبریای تو</p></div>
<div class="m2"><p>زمین و آسمان دیگر است و وسعت دیگر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سرایت گر کند در عالم استغنای ذات تو</p></div>
<div class="m2"><p>رضیع از خشک لب سیر و نگیرد شیر از مادر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر تبدیل طبع آب و خاک اندر خیال آری</p></div>
<div class="m2"><p>بجنبد کشتی اندر بحر چون صرصر دود دربر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>وگر حفظت به حال خویشتن خواهد طبایع را</p></div>
<div class="m2"><p>کبود از سیلی سرما نگردد چهرهٔ اخگر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خورد گر بر زمین و آسمان زور تلاش تو</p></div>
<div class="m2"><p>زمین را بگسلد لنگر فلک را بشکند محور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز مصباحی که خواهی کلبهٔ احباب از آن روشن</p></div>
<div class="m2"><p>نخیزد دود تا محشر چه قندیل مه انور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>وزان آتش که خواهی تیره از وی خانهٔ اعدا</p></div>
<div class="m2"><p>تولد یابد از هر یک شرر صد تودهٔ خاکستر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شها مشتاق خاک هند ایرانی غلام تو</p></div>
<div class="m2"><p>که از توران بر او بار است محنتهای زور آور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اگر می‌داشت تا غایت شفیعی کز رحیق او</p></div>
<div class="m2"><p>کند پر ساقیان بزم شاهنشاه را ساغر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>درین ملک از خرابیها نمی‌دیدند چون دریا</p></div>
<div class="m2"><p>لبش خشک و کفش خالی و آهش سرد و چشمش تر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به این بعد مسافت چشم آن دارد که خسرو را</p></div>
<div class="m2"><p>ز مدحت گستری گردد به قرب معنوی چاکر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>که چون مرغان بی‌بال و پر از بار دل ویران</p></div>
<div class="m2"><p>ز ایران نیستش جنبش میسر گرد برآرد پر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در اقطار جهان تا ز اقتضای گردش دوران</p></div>
<div class="m2"><p>به نوبت بر سر شاهان نهد ظل هما افسر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نهد بر سر یکایک مستعدان خلافت را</p></div>
<div class="m2"><p>کلاه پادشاهی سایهٔ شاه همایون فر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو بر روی زمینی آن بلند اقبال کز گردون</p></div>
<div class="m2"><p>رسد در روز هیجا به هر عون عسکرت لشگر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نهد یک دم به نظم این غزل سمع همایون را</p></div>
<div class="m2"><p>که هست از مخزن پرگوهرش کوچکترین گوهر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بگو ای نامه‌بر به یار کای منظور خوش منظر</p></div>
<div class="m2"><p>ملایم خوی زیبا روی مشگین موی سیمین بر</p></div></div>