---
title: >-
    قصیدهٔ شمارهٔ ۲۱ - وله من جواهر المنظوماته فی مدح محمدخان ترکمان گفته
---
# قصیدهٔ شمارهٔ ۲۱ - وله من جواهر المنظوماته فی مدح محمدخان ترکمان گفته

<div class="b" id="bn1"><div class="m1"><p>زمانه را دگر آبی به روی کار آمد</p></div>
<div class="m2"><p>که آب روی سلاطین روزگار آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبا به عزم بشارت بگرد شهر سبا</p></div>
<div class="m2"><p>ز پای تخت سلیمان کامکار آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب اگر دو جهان تن دهد به گنجایش</p></div>
<div class="m2"><p>به این شکوه که آن یکه شهسوار آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو آفتاب که آید ز ابر تیره برون</p></div>
<div class="m2"><p>سمند عزم برون رانده از غبار آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو عیش ساز کن ای جان مضطرب که ز راه</p></div>
<div class="m2"><p>قرار بخش اسیران بی‌قرار آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو دیده باز کن ای بخت منتظر که صبا</p></div>
<div class="m2"><p>به توتیا کشی چشم انتظار آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو ای صبا که زره می‌رسی نوید آلود</p></div>
<div class="m2"><p>ببر به شهر بشارت که شهریار آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهین خدیو سلاطین کامکار رسید</p></div>
<div class="m2"><p>خدایگان خواقین نامدار آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قوام ضابطه شش جهت محمدخان</p></div>
<div class="m2"><p>که هفت دایرهٔ چرخ را مدار آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه خان جهان جلالت که از جلالت و شان</p></div>
<div class="m2"><p>ز خسروان جهاندار در شمار آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بلند رتبه‌سواری که نعل شبرنگش</p></div>
<div class="m2"><p>سر اکاسره را تاج افتخار آمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپهر سده امیری که شرفه قصرش</p></div>
<div class="m2"><p>فراز غرفه این بیستون حصار آمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز تنگ ظرفی خود دارد انفعال جهان</p></div>
<div class="m2"><p>ز ذات او که به غایت بزرگوار آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز زیرکی به غلامیش هر که کرد اقرار</p></div>
<div class="m2"><p>ز نیک بختی و اقبال بختیار آمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به پیش رای جهانگیر او مخالف را</p></div>
<div class="m2"><p>جهان سپار نگویم که جان سپار آمد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طریق شیر شکاری به کائنات نمود</p></div>
<div class="m2"><p>اگرچه پنجه نیالوده از شکار آمد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ایا به عقل گران لنگری که در جنبت</p></div>
<div class="m2"><p>خرد به آن همه دانش سبک عیار آمد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو آن دقیقه شناسی که حسن تدبیرت</p></div>
<div class="m2"><p>همه موافقت تقدیر کردگار آمد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صلاح رای تو در فتنه بس که صبر نمود</p></div>
<div class="m2"><p>دل مفتون دشمن به زینهار آمد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سحاب تیغ مطر ریزی نکرده هنوز</p></div>
<div class="m2"><p>نهال فتح ز دهقانیت به بار آمد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>توقف ارچه گره گشت کار نصرت را</p></div>
<div class="m2"><p>محل کار ولی بیشتر به کار آمد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز ناز خوی بتان دارد آرزو چه عجب</p></div>
<div class="m2"><p>اگر امید تو را دیر در کنار آمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عدو چو پنجهٔ قدرت به پنجهٔ تو فکند</p></div>
<div class="m2"><p>چه تا بهاش که در دست اقتدار آمد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به جای ماند دو روزی ولی نرفت از جا</p></div>
<div class="m2"><p>اساس دولت و نصرت که استوار آمد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خوشا سحاب صلاح تو کز ترشح آن</p></div>
<div class="m2"><p>تمام ناشده فصل خزان بهار آمد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>برای جان عدو قهرت آتشی افروخت</p></div>
<div class="m2"><p>که کار شعلهٔ دوزخ زهر شرار آمد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ولی چو حلم تواش بر در انابت دید</p></div>
<div class="m2"><p>بر او ز ابر ترحم عطیه بار آمد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جهان فدای شعورت که تا به قوت عقل</p></div>
<div class="m2"><p>جهان ستان ز عدوی ستم شعار آمد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه در ضمیر کسی فکر کارزار گذشت</p></div>
<div class="m2"><p>نه بر زبان کسی حرف گیر و دار آمد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>درین محیط پرآشوب زورق که و مه</p></div>
<div class="m2"><p>ز لنگری که تو را بود بر کنار آمد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگرچه بود به گردت حصارهای دعا</p></div>
<div class="m2"><p>دعای محتشمت بهترین حصار آمد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پناه جان تو آن حصن سخت بنیان باد</p></div>
<div class="m2"><p>که نام آن کنف آفریدگار آمد</p></div></div>