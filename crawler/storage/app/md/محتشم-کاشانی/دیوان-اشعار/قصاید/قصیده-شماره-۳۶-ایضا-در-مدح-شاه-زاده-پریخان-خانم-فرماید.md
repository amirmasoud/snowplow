---
title: >-
    قصیدهٔ شمارهٔ ۳۶ - ایضا در مدح شاه‌زاده پریخان خانم فرماید
---
# قصیدهٔ شمارهٔ ۳۶ - ایضا در مدح شاه‌زاده پریخان خانم فرماید

<div class="b" id="bn1"><div class="m1"><p>گشت در مهد گران جنبش دهر آخر کار</p></div>
<div class="m2"><p>خوش خوش از خواب گراندیدهٔ بختم بیدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ادهم واشهب پدرام شب و روز شدند</p></div>
<div class="m2"><p>زیر ران امل از رایض صبرم رهوار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داروی صبر که بس دیر اثر بود آخر</p></div>
<div class="m2"><p>اثری داد که نگذشت ز دردم آثار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشتی را که به یک جذبهٔ گرداب تعب</p></div>
<div class="m2"><p>دور می‌برد به ته بخت کشیدش به کنار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیر شد خسرو بهجت سپه‌انگیز ولی</p></div>
<div class="m2"><p>زود از خیل غم و درد برآورد دمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر آن کلبه که زیبش ز حجر بود اکنون</p></div>
<div class="m2"><p>بدر و گوهرش آراسته شد سقف و جدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خشک بومی که برو چشم جهان زار گریست</p></div>
<div class="m2"><p>شد به یک چشم زدن رشک هزاران گلزار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این نسیم چه چمن بود که از بوالعجبی</p></div>
<div class="m2"><p>در خزان زد به مشام دل من بوی بهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این رحیق چه قدح بود که بر لب چو رسید</p></div>
<div class="m2"><p>دگر از ذوق نیابد به زبان نام خمار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منم آن نخل خزان دیده که دارم امروز</p></div>
<div class="m2"><p>به بشارات بهار ابدی استبشار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گلشن بخت من است آن که ز اقبال درو</p></div>
<div class="m2"><p>زده صد خرمن گل جوش زهر بوتهٔ خار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به زمین دشمن سرکوفته‌ام رفته فرو</p></div>
<div class="m2"><p>ز جهان حاسد کم‌حوصله‌ام کرده فرار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این ازان رشک که الحال از آن حالت پیش</p></div>
<div class="m2"><p>آن ازین غصه که امسال به صد عزت بار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کرده از قوت امداد خودم رتبه بلند</p></div>
<div class="m2"><p>داده در ساحت اعزاز خودم رخصت یار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پایهٔ تقویت زهرهٔ برجیس مقام</p></div>
<div class="m2"><p>سایهٔ تربیت شمسهٔ بلقیس وقار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پادشاه ملک و انس پریخان خانم</p></div>
<div class="m2"><p>که ز شاهنشهی حور و پری دارد عار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مریم فاطمه ناموس که ناموس جهان</p></div>
<div class="m2"><p>دارد از حسن عفافش چو ملک هفت حصار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قسمت آموخته در گه رزاق کبیر</p></div>
<div class="m2"><p>که کفش واسطهٔ رزق صغار است و کبار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن که با عصمت او رابعهٔ حجلهٔ چرخ</p></div>
<div class="m2"><p>در پس پرده به رسوائی خود کرد اقرار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وانکه با عفت وی کوه گران سنگ نمود</p></div>
<div class="m2"><p>دعوی وزن ولی پیش خرد کرد انکار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا درین قصر مقرنس نتواند دادن</p></div>
<div class="m2"><p>کش نشان از رخ آن شمسهٔ خورشید عذار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به کسی بخت به خوابش هم اگر بنماید</p></div>
<div class="m2"><p>نگذارد که شود تا به قیامت بیدار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عهد علیای کمین جاریه‌اش بندد اگر</p></div>
<div class="m2"><p>چرخ بر ناقهٔ خود گیردش از بهر مهار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درکشد ناقهٔ مهار از کف او گر نکند</p></div>
<div class="m2"><p>سر تانیث خود اول به ضرورت اظهار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عطر پروردهٔ هوای حرم عالی او</p></div>
<div class="m2"><p>بر زمین مشک فشان چون شود و عالیه بار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جنبش از باد برد حکمت بی چون بیرون</p></div>
<div class="m2"><p>که مبادا به مشامی کند آن نفخه گذار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ماه کز خیل ذکور است ز غم می‌کاهد</p></div>
<div class="m2"><p>که ز نامحرمیش نیست در آن حضرت بار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مهر کز سلک اناث است امیدی دارد</p></div>
<div class="m2"><p>که به آئین کنیزان شودش آینه دار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ماه اگر برقع از آن رخ به غلط بردارد</p></div>
<div class="m2"><p>غضبش حسن بصیرت ببرد از ابصار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نیست بر دامن پاک آنقدرش گرد هوس</p></div>
<div class="m2"><p>که بر آئینهٔ مهر از اثر هیچ غبار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>لرزد از نازکی خوی لطیفش چون بید</p></div>
<div class="m2"><p>باد چون بر قدمش گل کند از شاخ بهار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شمع بزمش اگر از باد نشیند مه و مهر</p></div>
<div class="m2"><p>سر بر آرند سراسیمه ز جیب شب تار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سایه را خواهد اگر از حرم اخراج کند</p></div>
<div class="m2"><p>مانع پرتو خورشید نگردد دیوار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ای کهان سپه صف شکنت پیل شکوه</p></div>
<div class="m2"><p>ای سگان حرم محترمت شیر شکار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>حکم جزمت همه جا همچون قضا بی‌مهلت</p></div>
<div class="m2"><p>تیغ قهرت همه دم همچون اجل بی زنهار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تقویت جسته ز عونت قدر ذی قدرت</p></div>
<div class="m2"><p>تربیت دیده به دورت فلک بی‌پرگار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>صیت انصاف تو چون آبروان در اطراف</p></div>
<div class="m2"><p>ذکر الطاف تو چون باد وزان در اقطار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بر نشان کف پایت رخ صد ماه جبین</p></div>
<div class="m2"><p>بر هلال سم رخشت سر صد شاه سوار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در رکابت همه اصناف ملک غاشیه کش</p></div>
<div class="m2"><p>از صفات همه اوراق فلک غاشیه‌دار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از برای مدد لشکر منصور تو بس</p></div>
<div class="m2"><p>نصرت و فتح که تازان ز یمین‌اند و یسار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر فتد بر ضعفا پرتوی از تربیتت</p></div>
<div class="m2"><p>ای قدر قضا قدرت گردون مقدار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پشه و مور و ملخ فی‌المثل ار عظم شوند</p></div>
<div class="m2"><p>همه پیل افکن و اژدر در و سیمرغ شکار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>من کزین بیشتر از رهگذر پستی بخت</p></div>
<div class="m2"><p>داشتم تکیه که از خار و خس راهگذار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>این دم از لطف تو ای شمسهٔ ایوان شرف</p></div>
<div class="m2"><p>این دم از عون تو ای زهرهٔ گردون وقار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پای بر مسند مه می‌نهم از استیلا</p></div>
<div class="m2"><p>تکیه بر بالش خود می‌کنم از استکبار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وین هنوز اول آثار ترقیست که من</p></div>
<div class="m2"><p>تازه باغ شجرانگیزم و تو ابر بهار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بنده پرور ملکا گر چه ز دارائی ملک</p></div>
<div class="m2"><p>داری از هند و حبش تا بدر چین و تتار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جان فشانند غلامان فدائی بی‌حد</p></div>
<div class="m2"><p>مدح خوانند مطیعان ثنائی بسیار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>یک غلام است ولیکن ز سیاه و ز سفید</p></div>
<div class="m2"><p>یک مطیع است ولیکن ز کبار و ز صغار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>که اگر دست اجل جیب حیاتش بدرد</p></div>
<div class="m2"><p>وندرین بقعه کند نقد بقا بر تو نثار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>وز گلستان ثنای تو به حسرت به برد</p></div>
<div class="m2"><p>بلبل نطق وی آن طایر نادر گفتار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>جای او هیچ ستاینده نگیرد در دور</p></div>
<div class="m2"><p>گر کند تا باید سعی سپهر دوار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>محتشم لاف گزاف این همه سبحان‌الله</p></div>
<div class="m2"><p>خود ستائیست کند به که کنی استغفار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پیش بلقیس و شی کز پیش از حور و پری</p></div>
<div class="m2"><p>فوج فوج‌اند دوان بنده‌وش و چاکروار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تو که باشی که کنی چاکری خود ظاهر</p></div>
<div class="m2"><p>تو که باشی که کنی بندگی خود اظهار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>از تو این بس که دهی آینهٔ او ترتیب</p></div>
<div class="m2"><p>از تو این بس که کنی ادعیهٔ او تکرار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>آفتابا به خدائی که خداوندی اوست</p></div>
<div class="m2"><p>سبب ظابطه رابطهٔ لیل و نهار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به رسولی که شب طاعت از افراط قیام</p></div>
<div class="m2"><p>خواند مزملش از غایت رافت جبار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به امیری که در احرام نمازش هر شب</p></div>
<div class="m2"><p>بانگ تکبیر ز تکبیر رسیدی به هزار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کاندرین ظلمت شب کز اثر خواب گران</p></div>
<div class="m2"><p>نیست جز چشم من و چشم کواکب بیدار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>آن قدر می‌کنم از بهر بقای تو دعا</p></div>
<div class="m2"><p>که مرا می‌رود از کار زبان زان اذکار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آنقدر ذکر تو می‌آورم از دل به زبان</p></div>
<div class="m2"><p>که مرا میفکند کثرت نطق از گفتار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تا شود ظل همای عظمت گسترده</p></div>
<div class="m2"><p>ز خدیوان جهان حارث گیتی سالار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ظل نواب همایون نشود کم ز سرت</p></div>
<div class="m2"><p>وز سر خلق جهان ظل تو تا روز شمار</p></div></div>