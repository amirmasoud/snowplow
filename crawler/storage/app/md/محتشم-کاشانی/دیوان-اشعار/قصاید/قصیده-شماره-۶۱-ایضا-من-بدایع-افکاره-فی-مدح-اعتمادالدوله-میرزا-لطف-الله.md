---
title: >-
    قصیدهٔ شمارهٔ ۶۱ - ایضا من بدایع افکاره فی مدح اعتمادالدوله میرزا لطف الله
---
# قصیدهٔ شمارهٔ ۶۱ - ایضا من بدایع افکاره فی مدح اعتمادالدوله میرزا لطف الله

<div class="b" id="bn1"><div class="m1"><p>دمید صبحی و از پرتو دمیدن آن</p></div>
<div class="m2"><p>به ذره‌ای نظر افکند آفتاب جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه صبح چهره نمایندهٔ هزار امید</p></div>
<div class="m2"><p>که مشکل است بیانش به صدهزار زبان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه آفتاب بلند اختر سپهر جلال</p></div>
<div class="m2"><p>که برد طلعت او ظلمت از زمین و زمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدار اهل زمین اعتماد دولت و دین</p></div>
<div class="m2"><p>حفیظ ملک و ملل پاسبان کون و مکان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گزیده نسخهٔ لطف‌اله لطف الله</p></div>
<div class="m2"><p>که هست آینهٔ صنع صانع دیان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محیط مکرمتی کز درش برد مه و سال</p></div>
<div class="m2"><p>گدا به کشتی چوبین ذخایر عمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جلیل موهبتی کاسمان به دو کشد</p></div>
<div class="m2"><p>زری که سایل او را بریزد از دمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یگانه صانع خیاط خانهٔ تقدیر</p></div>
<div class="m2"><p>بریده بر قد او خلعت بزرگی و شان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نهد به سجدهٔ او هفت عضو خود به زمین</p></div>
<div class="m2"><p>به آسمان اگر ازشان او دهند نشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان به عهد وی امساک شد قبیح که هست</p></div>
<div class="m2"><p>حرام در نظر عقل روزهٔ رمضان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به زیر بال و پر خویش مرغ تربیتش</p></div>
<div class="m2"><p>ز بیضه‌های عصافیر شد عقاب پران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رود چو سوی نشان تیر دقتش ز سپهر</p></div>
<div class="m2"><p>هزار زه شنود گوش گوشه‌های کمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنان که خاک شناسد خراش تیشهٔ تیز</p></div>
<div class="m2"><p>سخای دست ودلش بحر می‌شناسد و کان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زهی به ذات تو نازنده مسند تکمین</p></div>
<div class="m2"><p>زهی ز عظم تو شرمندهٔ وسعت امکان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز لطف خویش خدا لطف خویش خواند تو را</p></div>
<div class="m2"><p>تبارک‌الله از الطاف خالق منان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جوان کننده دوران پیر ساخت تو را</p></div>
<div class="m2"><p>هم اتفاقی تدبیر پیر و بخت جوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به خال چهرهٔ زنگی اگر نظر فکنی</p></div>
<div class="m2"><p>شود ز مردمی انسان دیدهٔ انسان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زینت ارچه مقام است لیک بالنسبة</p></div>
<div class="m2"><p>تو آتشی و کواکب شرار و چرخ و خان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جهان مدار از بس که شرمسار تو را</p></div>
<div class="m2"><p>به دوش زانوم از جبهه مانده بار گران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بزرگوارا از بس به زیر بار توام</p></div>
<div class="m2"><p>ز انحنا شده جیبم مصاحب دامان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زمانه راست چنین اقتضا که گوهر مدح</p></div>
<div class="m2"><p>ز قدر اگر چه بود گوشوار گوش جهان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به صد شعف چو ستاند ز مادحش ممدوح</p></div>
<div class="m2"><p>وز آفرین لب مدح آفرین شود جنبان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وز انتعاش کند زیب مجلسش یک چند</p></div>
<div class="m2"><p>چو لاله و سمن ونرگس و گل و ریحان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز عمد صد رهش افتد نظر بر او اما</p></div>
<div class="m2"><p>به سهو نیز نیفتد به فکر قیمت آن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو آن بزرگ عطائی که در نظم مرا</p></div>
<div class="m2"><p>ندیده قیمتش ارسال کردی از احسان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>و گر وظیفه هر ساله ساختی آن را</p></div>
<div class="m2"><p>هزار سال بود ملک عمرت آبادان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>منم کهن بلدی در کمال ویرانی</p></div>
<div class="m2"><p>تو گنج عالم ویران یگانه ایران</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حصار این بلد کهنه کن به آب و گلی</p></div>
<div class="m2"><p>که سیل حادثه هرگز نسازدش ویران</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>غلام بی بدلت محتشم که از افلاس</p></div>
<div class="m2"><p>کنون تخلص او مفلسی است در دیوان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو درد فاقه‌اش اکثر دواپذیر شده</p></div>
<div class="m2"><p>علاج مابقی از حکمت تو هست آسان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همیشه تاز پی اعتماد اهل وداد</p></div>
<div class="m2"><p>کنند بیعت و پیمان مشدد از ایمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>امیدوار چنانم که دولت ابدی</p></div>
<div class="m2"><p>ز بیعتت نکشد دست و نگسلد پیمان</p></div></div>