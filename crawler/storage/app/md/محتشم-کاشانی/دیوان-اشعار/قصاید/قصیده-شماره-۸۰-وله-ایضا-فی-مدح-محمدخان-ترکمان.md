---
title: >-
    قصیدهٔ شمارهٔ ۸۰ - وله ایضا فی مدح محمدخان ترکمان
---
# قصیدهٔ شمارهٔ ۸۰ - وله ایضا فی مدح محمدخان ترکمان

<div class="b" id="bn1"><div class="m1"><p>بیا ای رسول از در مهربانی</p></div>
<div class="m2"><p>به من یاری کن چون یاران جانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان زین کن از سعی رخش عزیمت</p></div>
<div class="m2"><p>که با باد صرصر کند همعنانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان ره سر کن به سرعت که از تو</p></div>
<div class="m2"><p>ز صرصر سبک‌تر گریزد گرانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو بر خنک سیلاب سرعت نهی زین</p></div>
<div class="m2"><p>ز چشم من آموز سیلاب رانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جنبش در آر آنچنان باره‌ات را</p></div>
<div class="m2"><p>که گردد روان بخش عزم از روانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرت نیست مشکل به شوکت پناهان</p></div>
<div class="m2"><p>امانت سپاری ودیعت رسانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غرض کاین گوهرهای بحر بلاغت</p></div>
<div class="m2"><p>که دارند در وزن و قیمت گرانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ازین کمترین بندهٔ کم بضاعت</p></div>
<div class="m2"><p>ببر ارمغانی به نواب خانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سمی محمد که یکتاست اسمش</p></div>
<div class="m2"><p>در القاب تنزیلی آسمانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به یک کارسازی که کاریست لازم</p></div>
<div class="m2"><p>غمی رابه دل کن به صد شادمانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان داوران را خداوند و صاحب</p></div>
<div class="m2"><p>مصاحب به نواب صاحبقرانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سکندر سپاهی که فرداست و یکتا</p></div>
<div class="m2"><p>در اقلیم گیری و کشورستانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ایالت پناهی که بختش رسانده</p></div>
<div class="m2"><p>ز کرسی نشینی به کسری نشانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پناه قزلباش کاندر شکوهش</p></div>
<div class="m2"><p>قدر باشکوه قزل ارسلانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سر چرخ را دیده با افسر خود</p></div>
<div class="m2"><p>به درگاه خویش از بلند آستانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ملقب به ظلم است از بس تفاوت</p></div>
<div class="m2"><p>در ایام او عدل نوشیروانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز تهدید عدل شدید انتقامش</p></div>
<div class="m2"><p>کند گله را گرگ سارق شبانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درین دولت از روی نیروی صولت</p></div>
<div class="m2"><p>قوی پشت ازو شوکت ترکمانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به قدر دو عمر از جهان بهره دارد</p></div>
<div class="m2"><p>شب و روز در عالم کامرانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که بر دیدهٔ دولتش خواب گشته</p></div>
<div class="m2"><p>حرام از برای جهان پاسبانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر در سپه بعضی از سروران را</p></div>
<div class="m2"><p>شد آهنگ دارائی آن جهانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سر او سلامت که دارد ز رفعت</p></div>
<div class="m2"><p>سزاواری فر تاج کیانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زهی نیک رائی که معمار سعیت</p></div>
<div class="m2"><p>بنای صلاح جهان راست بانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر سد حفظ تو حایل نگردد</p></div>
<div class="m2"><p>زمین پر شود ز آفت آسمانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به دم دایم آتش فروزند مردم</p></div>
<div class="m2"><p>ولیکن تو دانا دل از کامرانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پی پستی شعلهٔ فتنه هرجا</p></div>
<div class="m2"><p>دمیدی دمی کردی آتش نشانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو سهم جهادت به حکم اشارت</p></div>
<div class="m2"><p>چو تیر قضا می‌رسد بر نشانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سپاه تو را روز هیجا چه حاجت</p></div>
<div class="m2"><p>بشست آزمائی و زورین کمانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز خاصیت خصمیت دشمنان را</p></div>
<div class="m2"><p>کند موی سنجاب بر تن سنانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جلالت کزین تنگ میدان برونست</p></div>
<div class="m2"><p>از آن سو کند دهر را دیده‌بانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به عهد تو حکم سلاطین دیگر</p></div>
<div class="m2"><p>همه ناروان چون زر ایروانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زبان صلاح تو شمشیر قاطع</p></div>
<div class="m2"><p>در اصلاح آفات آخر زمانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به این طینت ای زینت چار عنصر</p></div>
<div class="m2"><p>بر آب و گلت می‌رسد قهرمانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سرا سرورا داد از دست دوران</p></div>
<div class="m2"><p>که داد از ستم داد نامهربانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر افروخته آتشی در عذابم</p></div>
<div class="m2"><p>که دودش رسیده به چرخ دخانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دورنگی و یک رنگ سوزیش دارد</p></div>
<div class="m2"><p>رخم را به حیثیت زعفرانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که چون رنگ کارم دگرگون نگردد</p></div>
<div class="m2"><p>به این اشگ کولاکی ارغوانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز دولاب گردانی آن مشعبد</p></div>
<div class="m2"><p>کز آن غرق فتنه است این مصرفانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز من یوسفی گشته امسال غایب</p></div>
<div class="m2"><p>که هجرش مرا کرده یعقوب ثانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چه یوسف عزیزی به صد گنج ارزان</p></div>
<div class="m2"><p>به بازار سودائیان معانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به بال و پر معرفت شاهبازی</p></div>
<div class="m2"><p>به چرخ آشنا از بلند آشیانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جلی اختری شبه اجرام گردون</p></div>
<div class="m2"><p>نمایان دری رشگ درهای کانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مرا وارث و یادگار از برادر</p></div>
<div class="m2"><p>ولیعهد و فرزند و دلبند جانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به چنگال اعراب افتاده حالا</p></div>
<div class="m2"><p>چو گلبرگ در دست باد خزانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چه اعراب قومی نه از قسم انسان</p></div>
<div class="m2"><p>همه غول سان از عجاب لسانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو صید آدمی زان گر ازان گریزان</p></div>
<div class="m2"><p>که دارند خوی سگان از عوانی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ملاقات یک روزهٔ آن لئیمان</p></div>
<div class="m2"><p>مقابل به جان کندن جاودانی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>که دارند اسیران خود را معذب</p></div>
<div class="m2"><p>به صحرا نوردی و اشتر چرانی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>پس از سالی آنگاهشان بر سر ره</p></div>
<div class="m2"><p>به امید آمد شد کاروانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به این نیت آرند کز عنف و غلظت</p></div>
<div class="m2"><p>ستانند از یک به یک ارمغانی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فروشندشان بعد از آن همچو یوسف</p></div>
<div class="m2"><p>به افسانه خوانی و جادو زبانی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>جهان کارسازا من اکنون چه سازم</p></div>
<div class="m2"><p>درین بینوائی به این ناتوانی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مگر حل این مشگل سخت عقده</p></div>
<div class="m2"><p>تو سرور به عنوان دیگر توانی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>وگرنه محال است آوردن او</p></div>
<div class="m2"><p>به حجت نویسی و قاصد دوانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>قصیر است وقت و طویل است قصه</p></div>
<div class="m2"><p>تو را نیز نفرت ازین قصه خوانی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>محل تنگ‌تر زانکه من رفته‌رفته</p></div>
<div class="m2"><p>کشم پرده از رازهای نهانی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سخن می‌کنم کوته آن گوهر آنجا</p></div>
<div class="m2"><p>بزر در گرو مانده دیگر تو دانی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ولی زین سخن این توقع ندارم</p></div>
<div class="m2"><p>من مفلس ای توامان امانی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>که دست تو گرد سفر نافشانده</p></div>
<div class="m2"><p>کند بر من و نظم من زرفشانی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بلی آن دو دعوی که تفصیل یک یک</p></div>
<div class="m2"><p>شنیدست دارنده از من زبانی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو نطقش به سمع معلی رساند</p></div>
<div class="m2"><p>تو فرمان دهش گر به جائی رسانی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ازین کامیابی شود محتشم را</p></div>
<div class="m2"><p>سرانجام عمر اول کامرانی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بود تا در آغاز عمر مطول</p></div>
<div class="m2"><p>جوانی طراوت ده زندگانی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>تو را ای جوانبخت از اقبال بادا</p></div>
<div class="m2"><p>در انجام عمر طبیعی جوانی</p></div></div>