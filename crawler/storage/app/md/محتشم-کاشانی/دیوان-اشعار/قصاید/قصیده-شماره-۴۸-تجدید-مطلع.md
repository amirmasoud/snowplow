---
title: >-
    قصیدهٔ شمارهٔ ۴۸ - تجدید مطلع
---
# قصیدهٔ شمارهٔ ۴۸ - تجدید مطلع

<div class="b" id="bn1"><div class="m1"><p>من و دو اسبه دوانیدن کمیت قلم</p></div>
<div class="m2"><p>به مدح یکه سوار قلم رو آدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من و مجاهده در راه دین به کلک و زبان</p></div>
<div class="m2"><p>ز وصف شاه مجاهد به ذوالفقار دو دم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من و رساندن صیت ثنا ز غرفهٔ ماه</p></div>
<div class="m2"><p>به آفتاب فلک چاکر فرشتهٔ حشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ولی خالق اکبر علی عالی قدر</p></div>
<div class="m2"><p>که هست ناطقه پیش ثنای او ابکم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علیم علم لدنی کزو ورای نبی</p></div>
<div class="m2"><p>همین یگانه خداوند اعلم است علم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امین گنج الهی که راز خلوت غیب</p></div>
<div class="m2"><p>تمام گفته به او مصطفی بوجه اتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محیط مرکز دل کانچه در خیال هنوز</p></div>
<div class="m2"><p>نداده دست بهم هست پیش او ملهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهی که خواهد اگر اتحاد نوع به جنس</p></div>
<div class="m2"><p>دهند دست معیشت به هم رمض و اصم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و گر اراده کند فصل را مبه این نوع</p></div>
<div class="m2"><p>کمند ربط و مساوات بگسلند ز هم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل حقیر نوازش که جلوه‌گاه خداست</p></div>
<div class="m2"><p>چو کعبه‌ایست که از عرش اعظم است اعظم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز فرش چون ننهد پا به عرش بت‌شکنی</p></div>
<div class="m2"><p>که بختش از بردوش نبی دهد سلم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به معجزش زد و صد ساله ره رساند باد</p></div>
<div class="m2"><p>زبان ابکم فطری سخن به گوش اصم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به جنب چشمهٔ فیضش سر تفاخر خویش</p></div>
<div class="m2"><p>به جیب جاه فرو برده از حیا زمزم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه او که دیده امینی که در حریم وصال</p></div>
<div class="m2"><p>میان سر خدا و نبی بود محرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس از رسول به از وی گلی نداد برون</p></div>
<div class="m2"><p>قدیم گلبن گل بار بوستان قدم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در آمدن به جهان پای عرش سای نهاد</p></div>
<div class="m2"><p>ز بطن شمسه برج شرف به فرش حرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قدم نهاد برون هم به مسجد از دنیا</p></div>
<div class="m2"><p>ز فتنه زائی افعال زاده ملجم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دو در یک صدفش را نمونه بودندی</p></div>
<div class="m2"><p>به عیسی ار ز قضا موسی شدی توام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به بحر اگر فتد اوراق مدح و منقبتش</p></div>
<div class="m2"><p>ز حفظ خالق یم تا ابد نگیرد نم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ببین چنین که رسیده است از نعیم عطا</p></div>
<div class="m2"><p>به بلبلان گلستان منقبت چه نعم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>علی‌الخصوص به سر خیل منقبت گویان</p></div>
<div class="m2"><p>که ریختی در جنت بها ز نوک قلم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فصیح بلبل خوش لهجه کاشی مداح</p></div>
<div class="m2"><p>که بود روضهٔ آمل ازو ریاض ارم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به مدح شاه عدو بندش از مهارت طبع</p></div>
<div class="m2"><p>چو داد سلسلهٔ هفت بند دست بهم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر به سر خفی بود اگر بوجه جلی</p></div>
<div class="m2"><p>برای او صله‌ها شد ز کلک غیب رقم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به پیروی من گستاخ هم برسم قدیم</p></div>
<div class="m2"><p>به حکم شوق نهادم بر آن بساط قدم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به قدر وسع دری سفتم از تتبع آن</p></div>
<div class="m2"><p>که گر ز من نبدی قیمتش نبودی کم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ورش خرد به ترازوی طبع سنجیدی</p></div>
<div class="m2"><p>شدی هر آینه شاهین آن ترازو خم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در انتظار نشستم به ساحل امید</p></div>
<div class="m2"><p>که موج کی زند از بحر من محیط کرم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کی از ریاض امل سر برآورد نخلی</p></div>
<div class="m2"><p>کی از دلم برد آرد زمانه بیخ الم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رساند مژده به یک بار هاتفی که نوشت</p></div>
<div class="m2"><p>برات جایزه شاه عرب به شاه عجم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سپهر کوکبه طهماسب پادشاه که برد</p></div>
<div class="m2"><p>به یمن نصرت دین برنهم سپهر علم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مجاهدی که ز تهدید او بدیدهٔ کشند</p></div>
<div class="m2"><p>غبار راه عباد صمد عبید صنم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شهی که خادم شرعند در عساکر او</p></div>
<div class="m2"><p>ز مهتران امم تا به کهتران خدم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز صیت تقویش از خوف نام خود لرزد</p></div>
<div class="m2"><p>چو لاله در گذر باد جام در کف جم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز بیم شحنهٔ ناموس او عیان نشود</p></div>
<div class="m2"><p>ز سادگی نرسد تا بس که روی درم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز دست از شفق آتش بساز خود زهره</p></div>
<div class="m2"><p>که داده زان عملش اجتناب شاه قسم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سحاب با کف او داشت بحث بر سر فیض</p></div>
<div class="m2"><p>ز شرم گشت عرق ریز بس که شد ملزم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دل و کفش گه ایثار در موافقت‌اند</p></div>
<div class="m2"><p>دو قلزم متلاطم به یکدیگر منضم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سهیل لطفش اگر پرتو افکند بر زیر</p></div>
<div class="m2"><p>ز آتش حسد آید به جوش خون به قم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مه سر علم او کند چو پنجه دراز</p></div>
<div class="m2"><p>به اشتلم ز سر مهر برکند پرچم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عمود خاره شکن گر کند بلند شود</p></div>
<div class="m2"><p>ز باد ضربت او کوره در کمر مدغم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خمد ز گرز گران سنگ او اگر به مثل</p></div>
<div class="m2"><p>شود ستون سپر و دست و بازوی رستم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مبار زانش اگر تاخت بر زمانه کنند</p></div>
<div class="m2"><p>دهند گاو زمین را ز فرط زلزله رم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به خیمه‌گاه سپاهش زمین کند پیدا</p></div>
<div class="m2"><p>لکاشف از کشش بی‌حد طناب خیم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سگ درش نبود گر به مردمی مامور</p></div>
<div class="m2"><p>به زهر چشم کند آب زهره ضیغم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>فسون حفظش اگر بر زمین شود مرقوم</p></div>
<div class="m2"><p>رود گزندگی از طبع افعی ارقم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز شهسوار عرب کنده شد در از خیبر</p></div>
<div class="m2"><p>ز شهریار عجم از زمانه بیخ ستم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>فلک به باطن و ظاهر نمی‌تواند یافت</p></div>
<div class="m2"><p>دو شهسوار چنین در قصیده عالم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جهان به معنی و صورت نمی‌تواند جست</p></div>
<div class="m2"><p>دو شاه بیت چنین در قصیدهٔ عالم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>عجب‌تر آن که یکی کرده با یکی ز خلوص</p></div>
<div class="m2"><p>بهم علاقه فرزندی و غلامی ضم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فلک سئوال کنانست ازین تواضع و نیست</p></div>
<div class="m2"><p>جز این مقاله جواب شه ستاره حشم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بدر که شاه ولایت بود چرا نزند</p></div>
<div class="m2"><p>پسر که شاه جهان باشد از غلامی دم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مهم دنیی و عقبی فتاده است مرا</p></div>
<div class="m2"><p>به این شهنشه اعظم به آن شه اکرم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کزو به روضهٔ رضوان رسم چه مرده به جان</p></div>
<div class="m2"><p>وزین بلجهٔ احسان رسم چه تشنه بیم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>یگانه پادشها یک گداست در عهدت</p></div>
<div class="m2"><p>که رفع پستی خود کرده از علو همم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز بار فقر به جانست و خم نکرده هنوز</p></div>
<div class="m2"><p>به سجدهٔ ملکان پشت خود برای شکم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>برون نرفته برای طمع ز کشور شاه</p></div>
<div class="m2"><p>اگر به ملک خودش خوانده فی‌المثل حاتم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کنون که عادت فقرش نشانده بر سر راه</p></div>
<div class="m2"><p>که روبراه نیاز آر یا به راه عدم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همان به حالت خویش است و بی‌نیازی را</p></div>
<div class="m2"><p>شعار و شیوهٔ خود کرده از جمیع شیم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هان به وقت همت مدد نمی‌طلبد</p></div>
<div class="m2"><p>ز اقویای جهان در میان لشگر غم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>اگر کریم به بارد ز آسمان حاشا</p></div>
<div class="m2"><p>که جز ز پادشه خود شود رهین کرم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو داغ با دل خونین نشسته تا روزی</p></div>
<div class="m2"><p>ز لطف شاه پذیرد جراحتش مرهم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>قسم به شاه و به نعماش کانچه گفتم ازو</p></div>
<div class="m2"><p>فلک مطابق واقع شنید و گفت نعم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو محتشم شده نامش اگر مسمی را</p></div>
<div class="m2"><p>به اسم ربط دهد شاه ازو چه گردد کم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همیشه تا ز پی بردن متاع بقا</p></div>
<div class="m2"><p>کند فنا بره دست برد پا محکم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>برای پاس بقای تو از کمند دعا</p></div>
<div class="m2"><p>دو دست او به قفا بسته باد مستحکم</p></div></div>