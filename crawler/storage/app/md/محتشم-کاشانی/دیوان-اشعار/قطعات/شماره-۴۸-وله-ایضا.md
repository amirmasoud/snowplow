---
title: >-
    شمارهٔ ۴۸ - وله ایضا
---
# شمارهٔ ۴۸ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>محیط دولت اقبال خواجه میر حسن</p></div>
<div class="m2"><p>که بود تاجر فرزانه‌ای چو او نادر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بی‌ثباتی ویرانهٔ جهان دانست</p></div>
<div class="m2"><p>زدود نقش فریبش ز صفحهٔ خاطر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وزین سراچه فانی قدم کشید و رسید</p></div>
<div class="m2"><p>ز سیر عالم باقی به نعمت وافر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو خواست دل که برد ره به گنج تاریخش</p></div>
<div class="m2"><p>وزین مقوله شود نکته‌ای بر او ظاهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رمز نکته‌رسی گفت خواجه میر حسن</p></div>
<div class="m2"><p>گذشت از سر ویرانه جهان آخر</p></div></div>