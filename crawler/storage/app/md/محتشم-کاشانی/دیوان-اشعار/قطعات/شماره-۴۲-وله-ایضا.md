---
title: >-
    شمارهٔ ۴۲ - وله ایضا
---
# شمارهٔ ۴۲ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>هر هنر من که زانگیز طبع</p></div>
<div class="m2"><p>در نظر عقل شود جلوه‌گر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خصم بداندیش حسد پیشه را</p></div>
<div class="m2"><p>ناوکی از رشک رسد بر جگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طوطی شیرین عمل نطق من</p></div>
<div class="m2"><p>کام جهان را چو کند پرشکر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چاشنی آن به مذاق حسود</p></div>
<div class="m2"><p>چون رسد از زهر بود تلخ‌تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آب و هوای چمن طبع من</p></div>
<div class="m2"><p>چون شود اشجار سخن پرثمر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی جهتی ناخن دخل غنیم</p></div>
<div class="m2"><p>میوه خراشی کند از هر شجر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طایر عنقا لقب درک من</p></div>
<div class="m2"><p>بیضهٔ معنی چو کشد زیر پر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خصم سیه‌رو کندش زاغ نام</p></div>
<div class="m2"><p>روح قدس گر زند از بیضه سر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جنبش دریای خیالات من</p></div>
<div class="m2"><p>افکند از تک چو به ساحل گوهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مدعی آن لل شهوار را</p></div>
<div class="m2"><p>گاه خزف خواند و گاهی حجر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ابر مطیر شکرین کلک من</p></div>
<div class="m2"><p>بر چمن دهر چو ریزد مطر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دوست خورد نیشکر از فیض آن</p></div>
<div class="m2"><p>زهر گیا دشمن حیوان سیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>محتشم اندر نظر عیب جو</p></div>
<div class="m2"><p>عیب تو این است که داری هنر</p></div></div>