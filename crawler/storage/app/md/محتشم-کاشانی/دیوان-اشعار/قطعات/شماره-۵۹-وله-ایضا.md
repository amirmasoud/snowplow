---
title: >-
    شمارهٔ ۵۹ - وله ایضا
---
# شمارهٔ ۵۹ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>افتخار اهل دولت خواجه احمد آن که بود</p></div>
<div class="m2"><p>نشه اقبالش از فیض ازل در آب و گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طایر روحش به شهبال توجه ناگهان</p></div>
<div class="m2"><p>در هوای آن جهان زین آشیان برداشت ظل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دل و جان بود مولای علی و آل او</p></div>
<div class="m2"><p>لاجرم چون گشت در جنت به ایشان متصل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر تاریخ وفاتش هاتفی از غیب گفت</p></div>
<div class="m2"><p>خواجه مولای علی و آل بود از جان ودل</p></div></div>