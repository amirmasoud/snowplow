---
title: >-
    شمارهٔ ۴۴ - وله ایضا
---
# شمارهٔ ۴۴ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>ای چراغ منتظر سوزان که می‌باید مرا</p></div>
<div class="m2"><p>بهر برخورداری از هر وعده‌ات عمری دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وی خدیو صبر فرمایان که می‌باید تو را</p></div>
<div class="m2"><p>بینوائی بر در از ایوب صبر اندوزتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با وجود آن که دست درفشانت مسرفی است</p></div>
<div class="m2"><p>کز عطای اوست کان در خوف و دریا در خطر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بنای مستقیم الجود میریزد مدام</p></div>
<div class="m2"><p>از نی کلکت شکر همچون نبات از نیشکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محتشم که امسال افلاسش فزونست از قیاس</p></div>
<div class="m2"><p>از شما انعام خواهد بیشتر از پیشتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیشت آمد به هر حال کردن اندک زری</p></div>
<div class="m2"><p>با تمنای مطول با متاع مختصر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از برای او به جای زر فرستادی نبات</p></div>
<div class="m2"><p>تا زبانش دیرتر در جنبش آمد بهر زر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرکهٔ مفت از عسل با آن که شیرین‌تر بود</p></div>
<div class="m2"><p>این نبات مفت بود از زهر قاتل تلخ‌تر</p></div></div>