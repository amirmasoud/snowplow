---
title: >-
    شمارهٔ ۵۱ - در مدح میرامین‌الدین محمد گوید
---
# شمارهٔ ۵۱ - در مدح میرامین‌الدین محمد گوید

<div class="b" id="bn1"><div class="m1"><p>آن سپهر ایوانکه از بخت بلند</p></div>
<div class="m2"><p>داردش کیوان به صد اخلاص پاس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان فلک مسند که می‌گوید ملک</p></div>
<div class="m2"><p>پاسبان آستانش را سپاس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میرامین‌الدین محمد که آسمان</p></div>
<div class="m2"><p>ارتفاع از شان او کرد اقتباس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وز بلندی زد سر ایوان وی</p></div>
<div class="m2"><p>طعنهٔ کوته کمندی بر حواس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن که دارد اطلس زر دوز چرخ</p></div>
<div class="m2"><p>پیش فرش مجلسش قدر پلاس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وانکه دارد قبهٔ زرین مهر</p></div>
<div class="m2"><p>پیش گل میخ درش رنگ نحاس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم مه و ناهید را هر شام گه</p></div>
<div class="m2"><p>روبخشت آستان او مماس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم رخ خورشید را هر صبح دم</p></div>
<div class="m2"><p>با در گردون اساس او مساس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سجود آستانش چرخ را</p></div>
<div class="m2"><p>از نهیب پاسبان در دل هراس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون خیال منزل دقت پسند</p></div>
<div class="m2"><p>گشت او را در دل دقت‌شناس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کرد برپا این چنین قصری که هست</p></div>
<div class="m2"><p>آسمان یک طاقش از روی قیاس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>داد ترتیب این چنین کاخی که هست</p></div>
<div class="m2"><p>پایه‌اش را جز به اوج خور تماس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حاصل این عالی بناصورت چو بست</p></div>
<div class="m2"><p>از خرد تاریخ او شد التماس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طبع سحرانگیز پوشانید تیز</p></div>
<div class="m2"><p>از دو تاریخ این دو مصرع را لباس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قصر گردون طاق کیوان پاسبان</p></div>
<div class="m2"><p>کاخ عالی پایهٔ اعلی اساس</p></div></div>