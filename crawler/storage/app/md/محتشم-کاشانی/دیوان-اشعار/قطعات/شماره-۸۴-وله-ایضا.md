---
title: >-
    شمارهٔ ۸۴ - وله ایضا
---
# شمارهٔ ۸۴ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>ای همایون فارس میدان دولت کاورند</p></div>
<div class="m2"><p>کهکشان بهر ستوران تو کاه از کهکشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه ناچارست بهر هر ستوری کاه و جو</p></div>
<div class="m2"><p>تا به دستور ستور من نیفتد از توان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرکب من نام جو نشنید هرگز زان سبب</p></div>
<div class="m2"><p>می‌کنم کاه فقط خواهش ز دستور زمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که به این حیوان رساندن گرچه شغل لازمست</p></div>
<div class="m2"><p>بام اندای منازل هست لازم‌تر از آن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آصفا وقت است تنگ و کاه و در دهها فراخ</p></div>
<div class="m2"><p>خامه در دست تو فرمانبر به تحریک بنان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک نفس شو ملتفت وز رشحه ریزیهای کلک</p></div>
<div class="m2"><p>زحمت یک ساله کن رفع از من بی‌خانمان</p></div></div>