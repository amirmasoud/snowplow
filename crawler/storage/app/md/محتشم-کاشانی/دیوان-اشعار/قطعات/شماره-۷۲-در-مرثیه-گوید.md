---
title: >-
    شمارهٔ ۷۲ - در مرثیه گوید
---
# شمارهٔ ۷۲ - در مرثیه گوید

<div class="b" id="bn1"><div class="m1"><p>باز طوفان اجل نابود ساخت</p></div>
<div class="m2"><p>گوهری از قلزم ز خار علم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز دست مرگ بی‌هنگام کند</p></div>
<div class="m2"><p>میوه‌ای بایسته از اشجار علم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن که در طفلی ز استعداد ذات</p></div>
<div class="m2"><p>بود پیدا در رخش آثار علم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وانکه در مهد از جبینش می‌نمود</p></div>
<div class="m2"><p>جوهر خالص گران مقدار علم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سعد اصغر آن که سعد اکبرش</p></div>
<div class="m2"><p>می‌ستود از پرتو انوار علم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود آن گلدسته چون از نازکی</p></div>
<div class="m2"><p>زیب گلزار طراوت بار علم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفت و گفت از بهر تاریخش خرد</p></div>
<div class="m2"><p>آه از آن گل‌دسته بازار علم</p></div></div>