---
title: >-
    شمارهٔ ۱۰۴ - وله ایضا
---
# شمارهٔ ۱۰۴ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>ای دل انصاف ده که چون نبود</p></div>
<div class="m2"><p>دور از جور خویش شرمنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کز پی هم ز گلشن سادات</p></div>
<div class="m2"><p>سه همایون درخت افکنده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اول آن نونهال گلشن جان</p></div>
<div class="m2"><p>که شدی مرده از دمش زنده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل باغ صفا صفی‌الدین</p></div>
<div class="m2"><p>که رخش بر سمن زدی خنده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس ضیای زمان و شمس زمین</p></div>
<div class="m2"><p>آن دو نخل بلند و زیبنده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که شد اسباب عیش خرد و بزرگ</p></div>
<div class="m2"><p>از غم فوتشان پراکنده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون به آئین جد و باب شدند</p></div>
<div class="m2"><p>جنت آرا به ذات فرخنده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا دو تاریخ آشکار شود</p></div>
<div class="m2"><p>این دو مصراع سزد از بنده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دور از بوستان مصطفوی</p></div>
<div class="m2"><p>یک نهال و دو نخل افکنده</p></div></div>