---
title: >-
    شمارهٔ ۶۷ - وله ایضا
---
# شمارهٔ ۶۷ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>گلبن گلزار سیادت که بود</p></div>
<div class="m2"><p>زبدهٔ سادات ذوی الاحترام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبل بستان قرائت که داشت</p></div>
<div class="m2"><p>بهره ازو سامعه خاص و عام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میر صفی گوهر اختر شعاع</p></div>
<div class="m2"><p>شمع قبایل مه گردون مقام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن که شدش در صغر سن ز فیض</p></div>
<div class="m2"><p>کشور تجوید مسخر تمام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا که ازین دیر پر آشوب کرد</p></div>
<div class="m2"><p>روی توجه سوی دارالسلام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پی تاریخ وفاتش نوشت</p></div>
<div class="m2"><p>کلک قضا قاری شیرین کلام</p></div></div>