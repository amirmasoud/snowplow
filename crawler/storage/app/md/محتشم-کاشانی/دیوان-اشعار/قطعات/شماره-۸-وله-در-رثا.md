---
title: >-
    شمارهٔ ۸ - وله در رثاء
---
# شمارهٔ ۸ - وله در رثاء

<div class="b" id="bn1"><div class="m1"><p>زین زمان خلاصه ذریت نبی</p></div>
<div class="m2"><p>مهر سپهر مرتبهٔ ماه فلک جناب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یعنی قوام ملت و دین آن که در جهان</p></div>
<div class="m2"><p>ننهاد پای سعی جز اندر ره صواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم خورده بذر مزرع جودش بزرگ و خرد</p></div>
<div class="m2"><p>هم خوشه‌چین خرمن او بود شیخ و شاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون آن یگانه مطلع انوار فیض بود</p></div>
<div class="m2"><p>سر بر زد از سپهر وجودش دو آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آراسته یکی به کمالات حیدری</p></div>
<div class="m2"><p>وز علم جعفری دگری گشته کامیاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون درگذشت از پی تاریخ او خرد</p></div>
<div class="m2"><p>غیر از دو آفتاب نیاورد در حساب</p></div></div>