---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>اشعث طماع عهد خود جمال قصه خوان</p></div>
<div class="m2"><p>آن که چون او طامعی در بحر و بر صورت نبست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمریانش ناگهان کشتند و هر فردی که بود</p></div>
<div class="m2"><p>رست از اخذ و جهید آن خر گدای زرپرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل چون تاریخ قتلش خواست از پیر خرد</p></div>
<div class="m2"><p>گفت هر فردی که بود از اشعث طماع رست</p></div></div>