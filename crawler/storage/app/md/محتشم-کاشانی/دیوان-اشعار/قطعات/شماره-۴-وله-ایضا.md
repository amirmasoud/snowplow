---
title: >-
    شمارهٔ ۴ - وله ایضا
---
# شمارهٔ ۴ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>بر همچو زنی لب لعاب افشان را</p></div>
<div class="m2"><p>در حالت اعراض و خوشی احسان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم به تماشاگه خلق آورمت</p></div>
<div class="m2"><p>چون مسخره کاورد برون طفلان را</p></div></div>