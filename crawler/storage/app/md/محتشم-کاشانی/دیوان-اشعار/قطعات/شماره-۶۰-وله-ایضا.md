---
title: >-
    شمارهٔ ۶۰ - وله ایضا
---
# شمارهٔ ۶۰ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>دلا بنگر این بی‌محابا فلک را</p></div>
<div class="m2"><p>که شد تا چه غایت به بیداد مایل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز روی زمین گردی انگیخت آسان</p></div>
<div class="m2"><p>که کار زمین و زمان ساخت مشکل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان بست آن سنگ دل دست ما را</p></div>
<div class="m2"><p>که خورشید را رو بینداید از گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اجل شد دلیر این چنین هم که ریزد</p></div>
<div class="m2"><p>به کام مسیح زمان زهر قاتل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>انیس سلاطین جلیس خواقین</p></div>
<div class="m2"><p>سپهر معارف جهان فضائل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سمی نبی نور دین ماه ملت</p></div>
<div class="m2"><p>محمد ملک ذات قدسی خصائل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حکیمی که سد متین علاجش</p></div>
<div class="m2"><p>میان حیات او اجل بود حایل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مسیحا دمی کز دمش روح رفته</p></div>
<div class="m2"><p>شدی باز در پیکر مرغ بسمل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>افاضل پناهی که در سایهٔ او</p></div>
<div class="m2"><p>شدی کمترین ذرهٔ خورشید کامل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو شهباز مرغ بلند آشیانش</p></div>
<div class="m2"><p>ز همت فکند از جهان بر جنان ظل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمودند از بهر تاریخ فوتش</p></div>
<div class="m2"><p>به دیباچهٔ خاطر و صفحهٔ دل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حکیمان رقم سرور اهل حکمت</p></div>
<div class="m2"><p>افاضل پناهان پناه افاضل</p></div></div>