---
title: >-
    شمارهٔ ۷۵ - وله ایضا
---
# شمارهٔ ۷۵ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>سرا سروران جد اعلای تو</p></div>
<div class="m2"><p>محمد رسول امین کریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که از بس به خلق خداوند بود</p></div>
<div class="m2"><p>به نام خود او را رئوف و رحیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گران سنگ شد لنگر حلم او</p></div>
<div class="m2"><p>به خفت کشیدن ز خضم لعیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به میراثش اکنون تو را می‌رسد</p></div>
<div class="m2"><p>تحمل باعدا ز خلق عظیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که از زمرهٔ عترت وی توئی</p></div>
<div class="m2"><p>که ذاتت حلیم است و طبعت سلیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غرض کز جهالت به خدام تو</p></div>
<div class="m2"><p>که می‌گفت اگر خصم بی‌ترس و بیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به حملش ز در دور کردی چنان</p></div>
<div class="m2"><p>که شرمنده برتافت روزان حریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدان سان که از کعبهٔ دل شود</p></div>
<div class="m2"><p>بلا حول آواره دیو رجیم</p></div></div>