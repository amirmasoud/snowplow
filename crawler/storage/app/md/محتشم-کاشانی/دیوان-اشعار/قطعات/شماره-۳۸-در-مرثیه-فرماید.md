---
title: >-
    شمارهٔ ۳۸ - در مرثیه فرماید
---
# شمارهٔ ۳۸ - در مرثیه فرماید

<div class="b" id="bn1"><div class="m1"><p>آه کامسال اندرین بستان سرای</p></div>
<div class="m2"><p>دهر هر گل را که بهتر دید چید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واندرختی را که خوش‌تر بود پار</p></div>
<div class="m2"><p>چرخ ناخوش خوی از بی‌خش برید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وانکه در برداشت تشریف قبول</p></div>
<div class="m2"><p>دست مرگ اول لباس او برید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاجرم زان پیشتر کاید ز شیب</p></div>
<div class="m2"><p>شاه راه عمر را پایان پدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیک مرگ از دشت آفت بی‌محل</p></div>
<div class="m2"><p>بر سر حافظ محمد جان رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وه چه حافظ آن فرید روزگار</p></div>
<div class="m2"><p>کایزدش در عهد خود فرد آفرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن که بود از پرتو انفاس او</p></div>
<div class="m2"><p>گرمی هنگامهٔ شاه شهید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وانکه دوران انتظار شغل او</p></div>
<div class="m2"><p>از محرم تا محرم می‌کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>واندرین ماتم سرا گل‌بانگ او</p></div>
<div class="m2"><p>گوش حوران جنان هم می‌شنید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عندلیب روحش از بستان دهر</p></div>
<div class="m2"><p>از صدای کوس رحلت چون رمید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهر تاریخش یکی از غیب گفت</p></div>
<div class="m2"><p>عندلیبی باز ازین بستان پرید</p></div></div>