---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>شخصی که به ریشیش چو نظر می‌دوزم</p></div>
<div class="m2"><p>صد فصل ز ریشخند می‌آموزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اصلاح چو کرد خواست تاریخش را</p></div>
<div class="m2"><p>خندید یکی و گفت ریشت گوزم</p></div></div>