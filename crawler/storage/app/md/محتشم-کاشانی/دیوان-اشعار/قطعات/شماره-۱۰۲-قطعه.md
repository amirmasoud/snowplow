---
title: >-
    شمارهٔ ۱۰۲ - قطعه
---
# شمارهٔ ۱۰۲ - قطعه

<div class="b" id="bn1"><div class="m1"><p>ای مهین آصفی که عالم را</p></div>
<div class="m2"><p>آستان تو ملجاء است و پناه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وی گزین سروری که بر کرمت</p></div>
<div class="m2"><p>راستان دو عالمند گواه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وزرای دگر که داشته‌اند</p></div>
<div class="m2"><p>عزت و شان خود به جود نگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون ازیشان چو شاعران دگر</p></div>
<div class="m2"><p>همت من نبوده احسان خواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جو و کاهی برای استر من</p></div>
<div class="m2"><p>می‌فرستاده‌اند بی‌اکراه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو که از لطف خالق رازق</p></div>
<div class="m2"><p>بر همه فایقی به حشمت و جاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا چو حکام سابق از احسان</p></div>
<div class="m2"><p>بفرست از برای او جو و کاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا برای ملازمان دگر</p></div>
<div class="m2"><p>بستان از من این بلای سیاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ورنه مانند برق خرمن‌سوز</p></div>
<div class="m2"><p>سر به صحراش میدهم ناگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کز تف شعله‌های آتش جوع</p></div>
<div class="m2"><p>نگذراد درین حدود گیاه</p></div></div>