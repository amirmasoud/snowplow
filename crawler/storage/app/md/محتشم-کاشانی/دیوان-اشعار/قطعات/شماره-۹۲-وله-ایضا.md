---
title: >-
    شمارهٔ ۹۲ - وله ایضا
---
# شمارهٔ ۹۲ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>ای شهریار ذیشان کز غایت بزرگی</p></div>
<div class="m2"><p>شان تو بی‌نیاز است از مدح خوانی من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد بنای حسنت هست آهنین حصاری</p></div>
<div class="m2"><p>از پاس دعوت خلق چون پاسبانی من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این پاسبانی اما چون دولت تو باقیست</p></div>
<div class="m2"><p>جان نیز اگر برآید از جسم فانی من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش از عطیهٔ تو ای نوبهار دولت</p></div>
<div class="m2"><p>از شرم زردتر شد رنگ خزانی من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با آن که بر وجودت از دعوت و تحیت</p></div>
<div class="m2"><p>دایم گوهر فشانیست شغل نهانی من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر عادت زمانه‌ای داور یگانه</p></div>
<div class="m2"><p>موقوف سیم و زر نیست گوهرفشانی من</p></div></div>