---
title: >-
    شمارهٔ ۳۱ - وله ایضا
---
# شمارهٔ ۳۱ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>سپهر حوصله آن ابر دست دریا دل</p></div>
<div class="m2"><p>که جیب و دامن پر زر به سایل افشاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حساب بخشش او در جهان به خلق خدا</p></div>
<div class="m2"><p>به غیر قادر دانا کسی نمی‌داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در اولم یکی از قابلان لطف چو دید</p></div>
<div class="m2"><p>به تحفه خواست مرا شرمسار گرداند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ولی در آخر کارم چو یافت ناقابل</p></div>
<div class="m2"><p>به آن رسید که آنها که داده بستاند</p></div></div>