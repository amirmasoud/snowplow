---
title: >-
    شمارهٔ ۷۶ - وله ایضا
---
# شمارهٔ ۷۶ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>اگر خرمنی را تبه کرد برقی</p></div>
<div class="m2"><p>که دودش گذر کرد از چرخ گردان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر خانه‌ای را ز جا کند سیلی</p></div>
<div class="m2"><p>که صد دیده گردیده چون ابر نیسان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگر بحر جمعیتی خورده برهم</p></div>
<div class="m2"><p>که یک شهر را پرتوش کرده ویران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اجل گرد ماتم رسانیده دیگر</p></div>
<div class="m2"><p>ز صحرای غبرا به ایوان کیهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو موجی زد این بحر یارب که یک سر</p></div>
<div class="m2"><p>تبه گشت و برخاست صد گونه طوفان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو باد مخالف برآمد که یک گل</p></div>
<div class="m2"><p>تلف گشت و صد خار ازو ماند برجان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که داد ای فلک آخرین تیغ کینت</p></div>
<div class="m2"><p>که پیوند یاران بریدی بدین سان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که کرد ای سپهر این قدرها دلیرت</p></div>
<div class="m2"><p>که کار به این مشکلی کردی آسان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه مقصود بودت که یک دودمان را</p></div>
<div class="m2"><p>چراغ فرح کشتی از باد حرمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زدی بی‌محل چنگ در حبیب عمرش</p></div>
<div class="m2"><p>دریدی ز سنگین دلی تا به دامان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو را از دل آمد که آن تازه گل را</p></div>
<div class="m2"><p>کنی همچو خاشاک با خاک یکسان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو چون کندی از باغ جان گلبنی را</p></div>
<div class="m2"><p>که گل بوی گل داشت از نکهت آن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو چون جیب جان پاره کردی گلی را</p></div>
<div class="m2"><p>که می‌آمدش بوی جان از گریبان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درین ماتم ای دوستان دور نبود</p></div>
<div class="m2"><p>اگر از دل دشمنان خیزد افغان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سزد گر ازین غصهٔ بدخواه صد ره</p></div>
<div class="m2"><p>گزد پشت دست تاسف به دندان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو او بود مقصود و گلزار هستی</p></div>
<div class="m2"><p>پدر را درین برک ریزنده بستان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو گلدسته‌ای بود آن نخل نورس</p></div>
<div class="m2"><p>که از گلشن جانش آورد دوران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همان به که از بهر تاریخ فوتش</p></div>
<div class="m2"><p>به کلک بدایع رقم خوش نویسان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نویسند مقصود گلزار هستی</p></div>
<div class="m2"><p>نگارند گلدستهٔ گلشن جان</p></div></div>