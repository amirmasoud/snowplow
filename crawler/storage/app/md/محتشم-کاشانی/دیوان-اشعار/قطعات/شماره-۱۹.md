---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>زبدة الاخوان فصیح خوش کلام</p></div>
<div class="m2"><p>صاحب نظم و مقالات فصیح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن که در شعر و معما روز و شب</p></div>
<div class="m2"><p>می‌ستودش دهر مخفی و صریح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از صبوح و باده او را گشته بود</p></div>
<div class="m2"><p>چهرهٔ شخص کمالاتش صبیح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناگه از بیداد صیاد اجل</p></div>
<div class="m2"><p>داد جان بر باد چون صید ذبیح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر تاریخ وفاتش چون نیافت</p></div>
<div class="m2"><p>عقل دوراندیش تاریخ صحیح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرده بر مدت فزون یک سال و گفت</p></div>
<div class="m2"><p>حیف و صد حیف از کمالات فصیح</p></div></div>