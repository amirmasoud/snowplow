---
title: >-
    شمارهٔ ۲۲ - وله ایضا
---
# شمارهٔ ۲۲ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>خان حاتم دل جم جاه که جبار جلیل</p></div>
<div class="m2"><p>هرچه از بدو ازل داد باو نیکو داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زرو گنج ملوک آن که به صد بنده دهند</p></div>
<div class="m2"><p>آن سخن سنج به یک بنده مدحت گو داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود از دولت آن مالک مملوک نواز</p></div>
<div class="m2"><p>هرچه ما بی درمان را ز فواید رو داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر نقدی که درین وقت به از گنجی بود</p></div>
<div class="m2"><p>منت از شاه کشیدیم ولی زر او داد</p></div></div>