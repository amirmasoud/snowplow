---
title: >-
    شمارهٔ ۱۰۹ - وله ایضا
---
# شمارهٔ ۱۰۹ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>شکر کز فیض کرد بار دگر</p></div>
<div class="m2"><p>جنبشی بحر لطف ربانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوهری از محیط نسل نهاد</p></div>
<div class="m2"><p>رو به ساحل چو نجم نورانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهی از برج سلطنت گردید</p></div>
<div class="m2"><p>نور بخش جهان ظلمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نازنین صورتی که تصویرش</p></div>
<div class="m2"><p>نیست یارای خامه مانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معتدل پیکری که تعدیلش</p></div>
<div class="m2"><p>عقل را داده سر به حیرانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میر سلطان مراد خان که ازوست</p></div>
<div class="m2"><p>در بقا روی عالم فانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نایب آب سمی جد که قضا است</p></div>
<div class="m2"><p>ابجد آموزش از ادب دانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لایق داوری و دارائی</p></div>
<div class="m2"><p>قابل خسروی و خاقانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خلف میرزا محمد خان</p></div>
<div class="m2"><p>صورت لطف و قهر سبحانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خان نوعهد نوجوانکه باو</p></div>
<div class="m2"><p>می‌کند فخر مسند خانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در سرور است تا قیام قیام</p></div>
<div class="m2"><p>از جلوسش سریر سلطانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن جهان بان که داده از رایش</p></div>
<div class="m2"><p>بانی این جهان جهان به این‌ی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وان جوان دل که هست تا ابدش</p></div>
<div class="m2"><p>زیر ران توسن طرب رانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن که ایزد نگین ملک باو</p></div>
<div class="m2"><p>داشت با آن گرانی ارزانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وانکه از رشک خاتمش لب خویش</p></div>
<div class="m2"><p>می گزد خاتم سلیمانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مدتی کان یگانه بود ز تو</p></div>
<div class="m2"><p>خانهٔ ازدواج را بانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بود او در محیط نسلش طاق</p></div>
<div class="m2"><p>چون در شاه‌وار عمانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گوهر فرد میر شاهی خان</p></div>
<div class="m2"><p>کش معین بادعون یزدانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چند روزی چو رفت و باز آمد</p></div>
<div class="m2"><p>ابر صلبش به گوهر افشانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گشت شهزاده دوم پیدا</p></div>
<div class="m2"><p>کاولش کردم آن ثنا خوانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>محتشم این زمان قلم بردار</p></div>
<div class="m2"><p>وز خیالات طبع سبحانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بهر سال ولادتش بنگار</p></div>
<div class="m2"><p>مه نو شاه‌زادهٔ ثانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لیک بر مدت اندرین مصراع</p></div>
<div class="m2"><p>هست چیزی زیاده نادانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر شود شاه زاده شهزاده</p></div>
<div class="m2"><p>می‌شود رفع آن به آسانی</p></div></div>