---
title: >-
    شمارهٔ ۲۸ - در مدح سلطان مرادخان گوید
---
# شمارهٔ ۲۸ - در مدح سلطان مرادخان گوید

<div class="b" id="bn1"><div class="m1"><p>حبذا مرز و بوم دارالمرز</p></div>
<div class="m2"><p>که به خلد از شرف مقابل شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه شرف این که چون ز اقبالش</p></div>
<div class="m2"><p>لطف پروردگار شامل شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میر سلطان مرادخان آن جا</p></div>
<div class="m2"><p>از سپهر وجود نازل شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاتم ملک کرد چون در دست</p></div>
<div class="m2"><p>حاتم او را کمینه سایل شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عموم رسوم معدلتش</p></div>
<div class="m2"><p>رسم ظلم از زمانه زایل شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصه کوته عروس دولت را</p></div>
<div class="m2"><p>عقد بند آن خدیو عادل شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بعد از آن داد ایزدش خلفی</p></div>
<div class="m2"><p>که به عهد شباب کامل شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو خلف آن نتیجهٔ اقبال</p></div>
<div class="m2"><p>کز شرف قبلهٔ قبایل شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حضرت میرزا محمد خان</p></div>
<div class="m2"><p>که سرو سرور اماثل شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم طرازندهٔ مجالس گشت</p></div>
<div class="m2"><p>هم فروزندهٔ محافل شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون برای بقای نسل شریف</p></div>
<div class="m2"><p>طبع آن مه به زهره مایل شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زان محیط جلال هم گوهری</p></div>
<div class="m2"><p>متوجه به سوی ساحل شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه گوهر آن که در بهای دو کون</p></div>
<div class="m2"><p>قیمتش صد خزانه فاضل شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وارث ملک میرشاهی خان</p></div>
<div class="m2"><p>که به شاهیش دهر قایل شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حاصل آن ماه افتاب نژاد</p></div>
<div class="m2"><p>چون به ملک وجود واصل شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بهر سال ولادتش گفتم</p></div>
<div class="m2"><p>ماهی از آفتاب حاصل شد</p></div></div>