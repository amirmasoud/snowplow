---
title: >-
    شمارهٔ ۸۱ - مرثیه در شهادت میر معصوم گوید رحمه‌الله
---
# شمارهٔ ۸۱ - مرثیه در شهادت میر معصوم گوید رحمه‌الله

<div class="b" id="bn1"><div class="m1"><p>امیر اعدل اعظم پناه ملک و ملل</p></div>
<div class="m2"><p>ملاذ اهل جهان کارساز اهل زمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک مواکب انجم سپاه مه رایت</p></div>
<div class="m2"><p>فلک سرادق کرسی بساط عرش ایوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپهر مرتبهٔ معصوم بیک آن که رساند</p></div>
<div class="m2"><p>صدای کوس تسلط به گوش عالمیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز ملک خود سفر حج گزید با خلقی</p></div>
<div class="m2"><p>که مثل او گوهری در صدف نداشت جهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلالهٔ نبوی شمع دوده صفوی</p></div>
<div class="m2"><p>صفای دودهٔ آدم خلاصه انسان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرآمد علما تاج تارک فضلا</p></div>
<div class="m2"><p>دلیل وادی دین هادی ره عرفان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لطیف طبع و زکی فطرت و صحیح ذکا</p></div>
<div class="m2"><p>دقایق آگه و روشن دل و حقایق دان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرشتهٔ هیات و خوش منطق و صحیح کلام</p></div>
<div class="m2"><p>بلیغ لفظ و معانی رس و بدیع بیان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رفیع مرتبه خان میرزا که پس خرد</p></div>
<div class="m2"><p>به حسن فطرت او در جهان نداد نشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن سفر که به جز اهل خدمت ایشان را</p></div>
<div class="m2"><p>نبود یک تن از انصار و یک کس از اعوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لباس حج چه در احرام گاه پوشیدند</p></div>
<div class="m2"><p>به جای خود و زره بی‌خبر ز تیغ و سنان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سنان و تیغ از آن جسمهای جان‌پرور</p></div>
<div class="m2"><p>برآن خجسته زمین خون فشان و خون‌باران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم از شهادت ایشان فلک دگر باره</p></div>
<div class="m2"><p>نمود واقعهٔ کربلا به پیر و جوان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هم از مصیبت آن سروران به نوحه نشست</p></div>
<div class="m2"><p>زمانه با دل بریان و دیدهٔ گریان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>درین قضیه چو تاریخ خواستند ز من</p></div>
<div class="m2"><p>ز غیب داد یکی این دو مصرعم به زبان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نموده واقعهٔ کربلا دگر باره</p></div>
<div class="m2"><p>عجب که تا با بدنوحه بس کند دوران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو ای رفیق زهر مصرعی به جو تاریخ</p></div>
<div class="m2"><p>که من به گریهٔ رفیقم مراچه فرصت آن</p></div></div>