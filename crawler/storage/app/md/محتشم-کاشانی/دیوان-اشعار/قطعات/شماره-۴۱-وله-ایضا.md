---
title: >-
    شمارهٔ ۴۱ - وله ایضا
---
# شمارهٔ ۴۱ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>ابوالفتح بیک آن گرامی جوان</p></div>
<div class="m2"><p>که رخت بقا سوی عقبی کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غریو از جهان خاست کان شاخ گل</p></div>
<div class="m2"><p>به آن تازگی پا ز دنیا کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو تاریخ او خواستم عقل گفت</p></div>
<div class="m2"><p>ابوالفتح بیک از جهان پا کشید</p></div></div>