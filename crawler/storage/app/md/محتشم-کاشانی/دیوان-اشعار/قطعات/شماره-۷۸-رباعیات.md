---
title: >-
    شمارهٔ ۷۸ - رباعیات
---
# شمارهٔ ۷۸ - رباعیات

<div class="b" id="bn1"><div class="m1"><p>ازین شش رباعی که کلکم نگاشت</p></div>
<div class="m2"><p>برای جلوس خدیو جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار و صد بیست تاریخ از او</p></div>
<div class="m2"><p>قدم زد برون هشت افزون بران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدین سان که از هر دو مصرع زنند</p></div>
<div class="m2"><p>بهم خالداران دم از اقتران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر سادگان پس گروه نخست</p></div>
<div class="m2"><p>ثباتی و بر عکس آن همچنان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شد زین چهار اقتران در عدد</p></div>
<div class="m2"><p>هزار و صد و چار مطلب عیان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز هر مصرعی نیز به روی فزود</p></div>
<div class="m2"><p>یکی از تواریخ معجز بیان</p></div></div>