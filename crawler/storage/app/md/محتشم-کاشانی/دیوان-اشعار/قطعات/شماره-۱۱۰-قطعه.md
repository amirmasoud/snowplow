---
title: >-
    شمارهٔ ۱۱۰ - قطعه
---
# شمارهٔ ۱۱۰ - قطعه

<div class="b" id="bn1"><div class="m1"><p>ای شمع سرکشان که به سر پنجهٔ جفا</p></div>
<div class="m2"><p>سر رشته وفای مرا تاب داده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سارمت فکار به زخم سخن مرنج</p></div>
<div class="m2"><p>چون خنجر زبان مرا آب داده‌ای</p></div></div>