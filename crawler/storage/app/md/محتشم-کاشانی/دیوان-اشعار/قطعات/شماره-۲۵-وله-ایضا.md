---
title: >-
    شمارهٔ ۲۵ - وله ایضا
---
# شمارهٔ ۲۵ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>ای عطا پیشه که دریای سخا و کرمت</p></div>
<div class="m2"><p>در تلاطم همه گوهر به کنار اندازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محتشم کیست که مثل تو گران مقداری</p></div>
<div class="m2"><p>بروی از خلق سبک روح گذار اندازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون به این لطف سرافراز شد اکنون آن به</p></div>
<div class="m2"><p>کانچه دارد به رهت بهر نثار اندازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیک از نظم گران سنگ مناسب‌تر نیست</p></div>
<div class="m2"><p>آن چه در پای تو ای کوه وقار اندازد</p></div></div>