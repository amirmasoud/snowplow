---
title: >-
    شمارهٔ ۶۶ - وله ایضا
---
# شمارهٔ ۶۶ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>بر روی فرش اغبری مستدیر سقف</p></div>
<div class="m2"><p>در زیر چرخ چنبری لاجورد فام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از محتشم ز سر کشی چرخ یک مهم</p></div>
<div class="m2"><p>افتاد با سر آمد ارباب احتشام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با آن که لطف بی‌بدل او به این محب</p></div>
<div class="m2"><p>ز الطاف خاص بود نه از لطفهای عام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با آن که در کفایت آن سعی‌ها نمود</p></div>
<div class="m2"><p>نواب آفتاب لقای فلک مقام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با آن که دوستان مدبر در آن مهم</p></div>
<div class="m2"><p>دادند داد کوشش و امداد و اهتمام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوهرشناسی آخر از ایشان که در سخن</p></div>
<div class="m2"><p>اعجاز می‌نمود بگیرائی کلام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انکار را به همت دستور نامدار</p></div>
<div class="m2"><p>کرد آنچنان که شرط حمایت بود تمام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن آصفی که می‌کندش دهر انقیاد</p></div>
<div class="m2"><p>وان آصفی که می‌کندش چرخ احترام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر خلق واجبست که در مدح او کنند</p></div>
<div class="m2"><p>منعم به سیدالوزرا اشرف‌الانام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ظلش که ظل سایهٔ خلق خداست باد</p></div>
<div class="m2"><p>بر مملکت مخلد و مبسوط و مستدام</p></div></div>