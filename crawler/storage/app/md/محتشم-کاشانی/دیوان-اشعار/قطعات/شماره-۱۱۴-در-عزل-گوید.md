---
title: >-
    شمارهٔ ۱۱۴ - در عزل گوید
---
# شمارهٔ ۱۱۴ - در عزل گوید

<div class="b" id="bn1"><div class="m1"><p>سرور عادیان سر غولان</p></div>
<div class="m2"><p>آن که نبود به هیاتش دگری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان بزرگ شترلبان که بود</p></div>
<div class="m2"><p>پیش او صد نواله ماحضری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بودی او را برادر کوچک</p></div>
<div class="m2"><p>دادی ار عوج را خدا پسری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلب بسیار بوده رد عالم</p></div>
<div class="m2"><p>لیک از وی نبوده قلب تری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خر دزدیده رنگ کرده فروخت</p></div>
<div class="m2"><p>کس به این رنگ دیده دزد خری</p></div></div>