---
title: >-
    رباعی شمارهٔ ۲
---
# رباعی شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای کرده قدوم تو سرافراز مرا</p></div>
<div class="m2"><p>وز یک جهتان ساختهٔ ممتاز مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خاک مذلتم چو برداشته‌ای</p></div>
<div class="m2"><p>یک باره نگهدار و مینداز مرا</p></div></div>