---
title: >-
    رباعی شمارهٔ ۵۲
---
# رباعی شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>آن راه که از حال سهیلی است جمیل</p></div>
<div class="m2"><p>از میل درو به که نمایم تعجیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاشوب و نوای فرح نو در دل</p></div>
<div class="m2"><p>افکنده طرب نامهٔ شاه اسمعیل</p></div></div>