---
title: >-
    رباعی شمارهٔ ۲۵
---
# رباعی شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>خورشید سپهر سر بلندی بهزاد</p></div>
<div class="m2"><p>کز مادر دهر از همه عالم زیر سرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتند که بر بستر ضعف است ملول</p></div>
<div class="m2"><p>بهر شعفش به دلف بشین باد آن ضاد</p></div></div>