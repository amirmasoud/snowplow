---
title: >-
    رباعی شمارهٔ ۱۵
---
# رباعی شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>سلاخ که آدمی کشی شیوهٔ اوست</p></div>
<div class="m2"><p>چون ریزش خون دوست می‌دارد دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سر ببرد مرا نه پیچم گردن</p></div>
<div class="m2"><p>ور پوست کند مرا نگنجم در پوست</p></div></div>