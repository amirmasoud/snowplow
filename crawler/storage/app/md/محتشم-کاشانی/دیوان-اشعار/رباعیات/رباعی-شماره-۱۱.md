---
title: >-
    رباعی شمارهٔ ۱۱
---
# رباعی شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>این آب که خضر ازو بقا خواسته است</p></div>
<div class="m2"><p>وز غیرتش آب زندگی کاسته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از قوت فواره نگشتست بلند</p></div>
<div class="m2"><p>کز جای ز تعظیم تو برخاسته است</p></div></div>