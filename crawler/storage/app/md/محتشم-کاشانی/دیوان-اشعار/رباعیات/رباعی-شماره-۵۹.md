---
title: >-
    رباعی شمارهٔ ۵۹
---
# رباعی شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>این بستر خستگی که انداخته‌ام</p></div>
<div class="m2"><p>بروی ز تب هجری تو بگداخته‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابروی تو لیک در نظر محرابیست</p></div>
<div class="m2"><p>کز سجده آن به فرقتت ساخته‌ام</p></div></div>