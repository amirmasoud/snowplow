---
title: >-
    رباعی شمارهٔ ۵۶
---
# رباعی شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>ای نام تو در هر لغتی ذکر انام</p></div>
<div class="m2"><p>وز تذکرهٔ نام تو شیرین لب و کام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌نام تو شعله‌ها تباهند تباه</p></div>
<div class="m2"><p>با نام تو کارها تمامند تمام</p></div></div>