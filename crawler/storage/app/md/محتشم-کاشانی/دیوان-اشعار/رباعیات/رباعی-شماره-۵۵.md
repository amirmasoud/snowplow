---
title: >-
    رباعی شمارهٔ ۵۵
---
# رباعی شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>بر پیکر آن سرور خورشید علم</p></div>
<div class="m2"><p>کز عارضه‌ای گشته مزاجش درهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان به دمم دعا که برباد رود</p></div>
<div class="m2"><p>از آینهٔ وجود او گرد الم</p></div></div>