---
title: >-
    رباعی شمارهٔ ۲۱
---
# رباعی شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>چیزی که به گل داده خدا زیبائیست</p></div>
<div class="m2"><p>وان نیز که داده سرور ار عنائیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اما به تو آن چه داده از پا تا سر</p></div>
<div class="m2"><p>اسباب یگانگی و بی‌همتائیست</p></div></div>