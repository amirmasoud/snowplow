---
title: >-
    رباعی شمارهٔ ۷۰
---
# رباعی شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>اسلام مگو آفت ایام است این</p></div>
<div class="m2"><p>افت چه بلای صبر و آرام است این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کفر آمد و داد خاک ایمان بر باد</p></div>
<div class="m2"><p>از قوت اسلام چه اسلام است این</p></div></div>