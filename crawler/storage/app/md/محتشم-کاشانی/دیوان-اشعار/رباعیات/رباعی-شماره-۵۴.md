---
title: >-
    رباعی شمارهٔ ۵۴
---
# رباعی شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>اسبی که بود پویه گهش چرخ نهم</p></div>
<div class="m2"><p>در تک شکند تارک خورشید بسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برگرد جهان چو شعلهٔ جواله</p></div>
<div class="m2"><p>گر چرخ زند نگسلدش دم از دم</p></div></div>