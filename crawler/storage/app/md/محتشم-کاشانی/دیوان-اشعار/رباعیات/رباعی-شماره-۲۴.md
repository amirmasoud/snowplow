---
title: >-
    رباعی شمارهٔ ۲۴
---
# رباعی شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>فرهاد ز کوه کندن بی‌بنیاد</p></div>
<div class="m2"><p>آوازهٔ شهرتش در افاق افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این نادرهٔ فرهاد اگر کوه نکند</p></div>
<div class="m2"><p>صد کوه طلا به منعم و مفلس داد</p></div></div>