---
title: >-
    رباعی شمارهٔ ۳
---
# رباعی شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ای نورده آیینهٔ احساس مرا</p></div>
<div class="m2"><p>لطف تو کلید قفل وسواس مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام تو خدا کرده چو فرهاد تو نیز</p></div>
<div class="m2"><p>بردار ز پیش کوه افلاس مرا</p></div></div>