---
title: >-
    رباعی شمارهٔ ۳۴
---
# رباعی شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>ای بی تو چو هم دم به من خسته نموده</p></div>
<div class="m2"><p>آیینه که بینم این تن غم فرسود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد به نظر خیالی اما آن نیز</p></div>
<div class="m2"><p>چون نیک نمود جز خیال تو نبود</p></div></div>