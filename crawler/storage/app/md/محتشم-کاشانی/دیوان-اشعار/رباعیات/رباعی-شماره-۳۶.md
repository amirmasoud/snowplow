---
title: >-
    رباعی شمارهٔ ۳۶
---
# رباعی شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>عفوی که ز اندازه بدر خواهد بود</p></div>
<div class="m2"><p>ظرفش ز جهان وسیع‌تر خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ساحت صحرای گناهی که مراست</p></div>
<div class="m2"><p>جا یافته بیش جاوه گر خواهد بود</p></div></div>