---
title: >-
    رباعی شمارهٔ ۸
---
# رباعی شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ای قصر بلند آسمان پیش تو پست</p></div>
<div class="m2"><p>خلقت همهٔ زیردست از روز الست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر تافته روزگار دستم به جفا</p></div>
<div class="m2"><p>دریاب و گرنه میرود کار ز دست</p></div></div>