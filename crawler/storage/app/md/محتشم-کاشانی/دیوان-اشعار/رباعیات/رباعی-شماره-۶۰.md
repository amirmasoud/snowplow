---
title: >-
    رباعی شمارهٔ ۶۰
---
# رباعی شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>دی از کرم داور دوران کردم</p></div>
<div class="m2"><p>سودی و زیان نیز دو چندان کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طالع بنگر که بر در حاتم دهر</p></div>
<div class="m2"><p>رفتم که کنم فایدهٔ نقصان کردم</p></div></div>