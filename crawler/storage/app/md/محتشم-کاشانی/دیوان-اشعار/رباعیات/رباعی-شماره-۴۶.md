---
title: >-
    رباعی شمارهٔ ۴۶
---
# رباعی شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>ای صید سگ شیر شکار تو پلنگ</p></div>
<div class="m2"><p>وی چرخ شکاری تو با چرخ به چنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با آن که کند کلنگ بیخ همه چیز</p></div>
<div class="m2"><p>شاهین تو کند از جهان بیخ کلنگ</p></div></div>