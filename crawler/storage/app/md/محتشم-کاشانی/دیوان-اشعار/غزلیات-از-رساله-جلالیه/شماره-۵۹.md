---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>برای خاطر غیرم به صد جفا کشتی</p></div>
<div class="m2"><p>ببین برای که ای بی‌وفا کرا کشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بران دمی که دمیدی نهان بر آتش غیر</p></div>
<div class="m2"><p>چراغ انجمن افروز عشق ما کشتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رقیب دامن پاکت گرفت و پاک نسوخت</p></div>
<div class="m2"><p>دریغ و درد که زود آتش حیا کشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو من هلاک شوم از طبیب شهر بپرس</p></div>
<div class="m2"><p>که مرگ کشت مرا یا تو بی‌وفا کشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی ندیده که یک تن دو جا شود کشته</p></div>
<div class="m2"><p>مرا تو آفت جان صد هزار جا کشتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرم ز کنگر غیرت بر اهل درد نما</p></div>
<div class="m2"><p>مرا چو بر در دروازه بلا کشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حریف درد تو شد محتشم به صد امید</p></div>
<div class="m2"><p>تو بی‌مروتش از حسرت دوا کشتی</p></div></div>