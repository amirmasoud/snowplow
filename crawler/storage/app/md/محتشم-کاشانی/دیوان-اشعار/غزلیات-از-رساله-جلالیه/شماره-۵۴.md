---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>گرچه دیدم بر عذار عصمتت خال گناه</p></div>
<div class="m2"><p>چشم از رویت نبستم روی چشم من سیاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کم نگه کردم که رویت را ندیدم سوی غیر</p></div>
<div class="m2"><p>غیرتم بنگر که دیگر می‌کنم سویت نگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدعی سررشتهٔ وصلت به چنگ آورده است</p></div>
<div class="m2"><p>هست زلف در همت اینک به این مغنی گواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیر پر کید و تو بی‌قید و من از مجلس برون</p></div>
<div class="m2"><p>جز خدا دیگر که پاس عصمتت دارد نگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حکم غیرت نیست در ملک دلم جاری بلی</p></div>
<div class="m2"><p>از سیاستهای پیشین تایب است این پادشاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گردد ای بت تا کی ازین جنگهای زرگری</p></div>
<div class="m2"><p>از تو ضایع ناوک بیداد و از من تیر آه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ته دل با کسان میدار صحبت بعد از آن</p></div>
<div class="m2"><p>میشو از لطف زبانی محتشم را عذر خواه</p></div></div>