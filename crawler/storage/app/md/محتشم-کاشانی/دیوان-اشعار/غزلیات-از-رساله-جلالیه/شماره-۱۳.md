---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>الهی تا ز حسن و عشق در عالم نشان باشد</p></div>
<div class="m2"><p>به کام عشق بازان شاه حسنت کامران باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الهی خلعت حسنت که جیبش ظاهر است اکنون</p></div>
<div class="m2"><p>ظهور دامنش تا دامن آخر زمان باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الهی تا ز باغ حسن خیزد نخل استغنا</p></div>
<div class="m2"><p>تذر و عصمتت را برترین شاخ آشیان باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الهی تا هوس باشد کنار و بوس طالب را</p></div>
<div class="m2"><p>شه حسن تو را تیغ تغافل در میان باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الهی عاشق از معشوق تا باشد تواضع جو</p></div>
<div class="m2"><p>دو ابروی تو را تیر تکبر در کمان باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الهی تا طلب خواهنده باشد ابروی پرچین</p></div>
<div class="m2"><p>چو ماری گنج یاقوت لبت را پاسبان باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الهی محتشم چشم خیانت گر کند سویت</p></div>
<div class="m2"><p>به پیش ناوک خشم تو چشم او نشان باشد</p></div></div>