---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>نمی‌گفتم که خواهد دوخت غیرت چشمم از رویت</p></div>
<div class="m2"><p>نمی‌گفتم که خواهد بست همت رختم از کویت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی‌گفتم کمند سرکشی بگسل که می‌ترسم</p></div>
<div class="m2"><p>دل من زین کشاکش بگسلد پیوند از مویت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی‌گفتم نگردان قبلهٔ بد نیتان خود را</p></div>
<div class="m2"><p>وگرنه روی می‌گردانم از محراب ابرویت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی‌گفتم سخن دربارهٔ بدگوهران کم گو</p></div>
<div class="m2"><p>که دندان می‌کنم یکباره از لعل سخنگویت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی‌گفتم بهر کس روی منما و مکن نوعی</p></div>
<div class="m2"><p>که گر از حسرت رویت بمیرم ننگرم سویت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی‌گفتم ازین مردم فریبی میکنی کاری</p></div>
<div class="m2"><p>که من باطل کنم بر خویش سحر چشم جادویت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی‌گفتم ازین به محتشم را بند بر دل نه</p></div>
<div class="m2"><p>که خواهد جست و خواهد جست او از زلف هندویت</p></div></div>