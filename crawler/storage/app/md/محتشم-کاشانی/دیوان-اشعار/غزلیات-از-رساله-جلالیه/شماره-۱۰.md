---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>آن که چشمت را ز خواب ناز بیداری نداد</p></div>
<div class="m2"><p>دلبری دادت بقدر ناز ودلداری نداد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن که کرد از قوت حسنت قوی بازوی جور</p></div>
<div class="m2"><p>قدرتت یک ذره بر ترک جفا کاری نداد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن که کرد آزار دل را جوهر شمشیر حسن</p></div>
<div class="m2"><p>اختیارت هیچ در قطع دل آزاری نداد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه دردی بی‌دوا نگذاشت یارب از چه رو</p></div>
<div class="m2"><p>غم به من داد و تو را پروای غمخواری نداد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن که کردت در دبستان نکوئی ذو فنون</p></div>
<div class="m2"><p>در فن یاری تو را تعلیم پنداری نداد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن که داد از قد و کاکل شاه حسنت را علم</p></div>
<div class="m2"><p>رایت ظلم تو را بیم از نگونساری نداد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن که بار بی‌دلان کرد از غم عشقت فزون</p></div>
<div class="m2"><p>محتشم را تا نکشت از غم سبکباری نداد</p></div></div>