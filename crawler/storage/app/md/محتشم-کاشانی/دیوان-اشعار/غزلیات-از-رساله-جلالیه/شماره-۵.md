---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>چون پیش یار قید و رهائی برابر است</p></div>
<div class="m2"><p>آن جا اگر روی و گر آئی برابر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک لحظه با تو بودن و با غیر دیدنت</p></div>
<div class="m2"><p>با صد هزار سال جدائی برابر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لطفی نمی‌کنی که طفیل رقیب نیست</p></div>
<div class="m2"><p>لطفی چنین به قهر خدائی برابر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر بوالهوس که گفت فدای تو جان من</p></div>
<div class="m2"><p>پیشت به عاشقان فدائی برابر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوخی که نرخ بوسه به جائی دهد قرار</p></div>
<div class="m2"><p>در کیش ما به حاتم طائی برابر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از غیر رو نهفتن و در پرده دم زدن</p></div>
<div class="m2"><p>با صد هزار چهرهٔ گشائی برابر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل خوس مکن به خسرو بی‌عشق محتشم</p></div>
<div class="m2"><p>کاین خسروی کنون به گدائی برابر است</p></div></div>