---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>بترس از آن که درآرد سر از دهان من آتش</p></div>
<div class="m2"><p>به جانب تو کشد شعله از زبان من آتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بترس از آن که ز آمیزشت به چرب زبانان</p></div>
<div class="m2"><p>شود زبانه‌کش از مغز استخوان من آتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بترس از آن که چه باران لطف بر همه باری</p></div>
<div class="m2"><p>به برق آه زند در دل تو جان من آتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بترس از آن که ز حرف حریف سوز نوشتن</p></div>
<div class="m2"><p>به جانب تو زند در قلم بنان من آتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بترس از آن که چه سک دامن تو گیرم و گیرد</p></div>
<div class="m2"><p>بدامنت ز زبان شرر فشان من آتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بترس از آن که چو من تیر آه افکنم از دل</p></div>
<div class="m2"><p>به جای تیر جهد از دم کمان من آتش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بترس از آن که ز سوزنده شعرها گه و بیگه</p></div>
<div class="m2"><p>به مجلست فکند محتشم لسان من آتش</p></div></div>