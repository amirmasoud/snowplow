---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>به بازی آفتاب را چه گفتم ماه رنجیدی</p></div>
<div class="m2"><p>دلیرم کردی اول در سخن آنگاه رنجیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز من در باب آن زلف و زنخدان خواستی حرفی</p></div>
<div class="m2"><p>چو من از ریسمانت رفتم اندر چاه رنجیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تیغت نیم به سمل گشته بود ای ماه مرغ دل</p></div>
<div class="m2"><p>چو از تقصیر خویشت ساختم آگاه رنجیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کشتن سر بلندم دیر می‌کردی چه گفتم من</p></div>
<div class="m2"><p>که بر قدم لباس شوق شد کوتاه رنجیدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهانت را چه گفتم هیچ بر من خرده نگرفتی</p></div>
<div class="m2"><p>ولی این حرف چون افتاد در افواه رنجیدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز ره صد ره برون شد غیر و طبعت زو نشد رنجه</p></div>
<div class="m2"><p>چرا زین بی دل گمره به یک بی‌راه رنجیدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدیث محتشم بر خاطرت ماند گران اول</p></div>
<div class="m2"><p>چو بد تاویل کرد آن حرف را بدخواه رنجیدی</p></div></div>