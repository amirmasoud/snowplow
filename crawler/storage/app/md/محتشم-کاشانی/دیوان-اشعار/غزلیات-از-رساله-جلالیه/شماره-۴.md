---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>عشقت زهم برآورد یاران مهربان را</p></div>
<div class="m2"><p>از همچو مرگ به گسست پیوند جسم و جان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا طرح هم زبانی با این و آن فکندی</p></div>
<div class="m2"><p>کردند تیز برهم صد همزبان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از لطف عام کردی در بزم خاص باهم</p></div>
<div class="m2"><p>در نیم لحظه دشمن صد ساله دوستان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمعی که باهم اول بودند راست چون تیر</p></div>
<div class="m2"><p>در کینهٔ هم آخر کردند زه کمان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد ستیزه برخاست وز یکدیگر جدا کرد</p></div>
<div class="m2"><p>مانند دود آتش اهل دو دودمان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهری ز آشنایان پر بود ای یگانه</p></div>
<div class="m2"><p>بیگانه کرد عشقت از هم یگان یگان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد دست عهد باهم دست تو از کناره</p></div>
<div class="m2"><p>شمشیر بر میان زد پیوند این و آن را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما با کسی که بودیم پیوسته بر در مهر</p></div>
<div class="m2"><p>باب النزاع کردیم آن طرفه آستان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با محتشم رفیقی طرح رقابت افکند</p></div>
<div class="m2"><p>کی ره به خاطر خود می‌دادم این گمان را</p></div></div>