---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>بهر تسخیر دلم پادشهی تازه رسید</p></div>
<div class="m2"><p>فکر خود کن که سپه بر در دروازه رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق زد بر در دل نوبت سلطان دگر</p></div>
<div class="m2"><p>کوچ کن کوچ که از صد طرف آوازه رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهر دل زود بپرداز که از چار طرف</p></div>
<div class="m2"><p>لشگری تازه برون از حد و اندازه رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مژده محمل مه کوکبه‌ای می‌آرند</p></div>
<div class="m2"><p>از درون رخش برون تاز که جمازه رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میوهٔ وصل تو آن به که گذارم به رقیب</p></div>
<div class="m2"><p>از ریاض دگرم چون ثمر تازه رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقیا باده ز خمخانهٔ دیگر برسان</p></div>
<div class="m2"><p>که درین بزم مرا کار به خمیازه رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتشم طرح کتاب دیگر افکند مگر</p></div>
<div class="m2"><p>کار اوراق جلالیه به شیرازه رسید</p></div></div>