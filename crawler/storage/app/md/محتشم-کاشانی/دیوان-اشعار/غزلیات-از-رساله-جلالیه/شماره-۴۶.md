---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>اگر خواهی دعای من کنی بر مدعای من</p></div>
<div class="m2"><p>بگو بیمار عشق من شود یارب فدای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر عمرم نمانده است ای پسر بادا بقای تو</p></div>
<div class="m2"><p>دگر مانده است بر عمر تو افزاید خدای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یاران این وصیت می‌کنم کز تیغ جور تو</p></div>
<div class="m2"><p>چو گردم کشته دامانت نگیرند از برای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تیغ بی دریغم چون کشد جلاد عشق تو</p></div>
<div class="m2"><p>چو گوئی حیف از آن مسکین همین بس خونبهای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جای کور اگر در دوزخ افتم نبودم باکی</p></div>
<div class="m2"><p>که میدانم به خصم من نخواهی داد جای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز من پیوند مگسل ای نهال بوستان دل</p></div>
<div class="m2"><p>ز تن تا نگسلد پیوند جان مبتلای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه آئی بر سر خاکم بگو کز خاک سربر کن</p></div>
<div class="m2"><p>وفای من ببین ای کشته تیغ جفای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس آنگه گر دعائی گوئیم این گو که در محشر</p></div>
<div class="m2"><p>چو سر از خاک برداری نبینی جز لقای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازین خوش‌تر چه باشد کز تو چون پرسند کی بی‌غم</p></div>
<div class="m2"><p>کجا شد محتشم گوئی که مرد اندر وفای من</p></div></div>
<div class="b2" id="bn10"><p>نمی‌دانم چسان در ره فتادم</p>
<p>که رفت از تاب رفتن هم زیادم</p></div>