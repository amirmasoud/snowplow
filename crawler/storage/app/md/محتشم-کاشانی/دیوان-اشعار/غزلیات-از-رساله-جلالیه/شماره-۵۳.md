---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>شدم از گریه نابینا چراغ دیدهٔ من کو</p></div>
<div class="m2"><p>سیه گزدید بزمم شمع مجلس دیدهٔ من کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عنان بخت هر بی دل که بینی دلبری دارد</p></div>
<div class="m2"><p>نگهدار عنان بخت بر گردیدهٔ من کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به میزان نظر طور بتان را جمله سنجیدم</p></div>
<div class="m2"><p>ندیدم یک کران تمکین بت سنجیدهٔ من کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود دامن به دست صد خس این گلهای رعنا را</p></div>
<div class="m2"><p>گل یکرنگ دامن از خسان برچیدهٔ من کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو مجنونی ببینی در بیابانها بپرس ای مه</p></div>
<div class="m2"><p>که مجنون بیابان گرد محنت دیدهٔ من کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو ناوک خورده صیدی را تنی بسمل بگو با خود</p></div>
<div class="m2"><p>که صید زخمی در خاک و خون غلطیدهٔ من کو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز اشک محتشم افتاد شور اندر جهان بی تو</p></div>
<div class="m2"><p>تو خود هرگز نگفتی عاشق شوریدهٔ من کو</p></div></div>