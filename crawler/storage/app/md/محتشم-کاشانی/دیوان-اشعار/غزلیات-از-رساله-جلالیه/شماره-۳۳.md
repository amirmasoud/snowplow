---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>منم شکسته نهال ریاض عشق و گلی</p></div>
<div class="m2"><p>ز دهر می‌کند امسال غالبا بی‌خم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به زخم ناوک او چون شوم شهید کنید</p></div>
<div class="m2"><p>شهید ناوک شاطر جلال تاریخم</p></div></div>