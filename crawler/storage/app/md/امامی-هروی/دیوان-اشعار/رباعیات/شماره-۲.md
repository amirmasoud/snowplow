---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>در عالم عشق طور، طور غم تست</p></div>
<div class="m2"><p>در دور زمانه جور، جور غم تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ساغر دیده زان مدامست مرا</p></div>
<div class="m2"><p>خونابه ی دل که دور، دور غم تست</p></div></div>