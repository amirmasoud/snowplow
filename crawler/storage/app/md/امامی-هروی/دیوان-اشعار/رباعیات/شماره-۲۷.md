---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>گر داشتمی بقدر همت دستی</p></div>
<div class="m2"><p>دستم چو بدامن عرض پیوستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوهر کش گردون جهان بگسستی</p></div>
<div class="m2"><p>قرّابه ی گوهر فلک، بشکستی</p></div></div>