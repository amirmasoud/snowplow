---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>کی فتنه ی نرگس تو در خواب شود</p></div>
<div class="m2"><p>چند از رخ تو زلف تو در تاب شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لؤلؤ بنما، ز لعل، تا درّ خوشاب</p></div>
<div class="m2"><p>در کام صدف ز شرم او آب شود</p></div></div>