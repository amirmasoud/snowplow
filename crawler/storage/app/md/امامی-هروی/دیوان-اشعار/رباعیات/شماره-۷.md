---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>شیرین دهنت چون صفت جان دارد</p></div>
<div class="m2"><p>خود را سزد ار، ز دیده پنهان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن چشمه که خضر یافت زو آب حیات</p></div>
<div class="m2"><p>لعل تو بهر لطیفه ی آن دارد</p></div></div>