---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>ای دوست من از هیچ مشوش گردم</p></div>
<div class="m2"><p>وز نیمه ی نیم ذره در خوش گردم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آب لطیفتر مزاجی دارم</p></div>
<div class="m2"><p>دریاب مرا و گرنه ناخوش گردم</p></div></div>