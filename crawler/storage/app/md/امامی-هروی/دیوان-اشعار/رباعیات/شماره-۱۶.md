---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>ای برده خیال تو مرا خواب ز چشم</p></div>
<div class="m2"><p>هجران توام گشوده سیلاب ز چشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در فرقت تو یکنفسم خالی نیست</p></div>
<div class="m2"><p>خاک از سر و آتش از دل و آب ز چشم</p></div></div>