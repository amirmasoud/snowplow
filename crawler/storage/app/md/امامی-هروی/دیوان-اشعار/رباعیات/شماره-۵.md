---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>تا حاصل دردم سبب درمان گشت</p></div>
<div class="m2"><p>پستیم بلندی شد و کفر ایمان گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان و دل و تن حجاب ره بود و کنون</p></div>
<div class="m2"><p>تن دل شد دل جان شد و جان جانان گشت</p></div></div>