---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>ای رفته و برگزیده بر ما دگران</p></div>
<div class="m2"><p>من مستحق وصل توام یاد گران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انصاف بده رواست در مذهب عشق</p></div>
<div class="m2"><p>دل با تو و من بی تو و تو با دگران</p></div></div>