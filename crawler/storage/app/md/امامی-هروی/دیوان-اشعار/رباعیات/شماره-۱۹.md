---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>بی روی تو گرزآنکه نمی داشت دلم</p></div>
<div class="m2"><p>هم چشم وفا از تو همی داشت دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو چشم دل منی و در معرض جان</p></div>
<div class="m2"><p>از چشم خود این چشم نمی داشت دلم</p></div></div>