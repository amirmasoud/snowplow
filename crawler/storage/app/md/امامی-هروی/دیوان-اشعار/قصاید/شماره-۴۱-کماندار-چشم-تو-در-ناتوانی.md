---
title: >-
    شمارهٔ ۴۱ - کماندار چشم تو در ناتوانی
---
# شمارهٔ ۴۱ - کماندار چشم تو در ناتوانی

<div class="b" id="bn1"><div class="m1"><p>کماندار چشم تو در ناتوانی</p></div>
<div class="m2"><p>روان بخش لعل تو در بی نشانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برد راه گردنگش دور گردون</p></div>
<div class="m2"><p>دهد آب سرچشمهٔ زندگانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشکر بیان کردی از جان فروزی</p></div>
<div class="m2"><p>بنرگس خبر دادی از دلستانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که احیای روح است لعل بدخشی</p></div>
<div class="m2"><p>که قانون سحر است جزع یمانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازینسان که شمشیر در روی گیتی</p></div>
<div class="m2"><p>کشیده است چشمت بنا مهربانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر نیستی آب حیوان لعلت</p></div>
<div class="m2"><p>زمانه نبستی در زندگانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرد خیره ماند است در چشم شوخت</p></div>
<div class="m2"><p>که در عین مخموری و ناتوانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه با فلک می کند هم نبردی</p></div>
<div class="m2"><p>همه با قضا می کند همعنانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روان خرمست از لب جان فزایت</p></div>
<div class="m2"><p>که لعلت بعکس می ارغوانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گهی روح را می کند دستگیری</p></div>
<div class="m2"><p>گهی طبع را می دهد شادمانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیاقوت تاب دل مهر و ماهی</p></div>
<div class="m2"><p>بخورشید آب رخ جسم و جانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگر جرعه ی بزم جام وزیری</p></div>
<div class="m2"><p>مگر خاک درگاه صدر جهانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهر سخا فخر ملک آفتابی</p></div>
<div class="m2"><p>که دور سهپرش ندیده است ثانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پناه هدی، شمس دین، کاب حکمش</p></div>
<div class="m2"><p>ببرد آب روی فلک از روانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وزیری که درگاهش اهل جهان را</p></div>
<div class="m2"><p>جهان امانست و جان امانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وزیری که چون بر پیمبر (ص) نبوت</p></div>
<div class="m2"><p>بدوختم گشته است صاحبقرانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زهی کار کلک تو گیتی فروزی</p></div>
<div class="m2"><p>زهی شغل رأی تو خسرو نشانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زهی سقف و بنیاد ایوان دین را</p></div>
<div class="m2"><p>بیان و بنان تو معمار و بانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز احکام و انعام کلک جاری تو</p></div>
<div class="m2"><p>شود در رگ کان زر زندگانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نظر های انعام تست آفتابی</p></div>
<div class="m2"><p>اثرهای احکام تست آسمانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سپهر برین تا نکرد از تواضع</p></div>
<div class="m2"><p>جناب رفیع ترا آستانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر ایوان قدر تو بر جرم کیوان</p></div>
<div class="m2"><p>مکرر نشد منصب پاسبانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز خورشید رأیت رسیدست گیتی</p></div>
<div class="m2"><p>در ایام پیری بروز جوانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز تأثیر عدل تو از گرگ امین تر</p></div>
<div class="m2"><p>ندیدست سر خیل ملک شبانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وزیری که با خنجر شاه، کلکش</p></div>
<div class="m2"><p>ببرهان قاطع کند تو أمانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنان در امانست گیتی ز حفظت</p></div>
<div class="m2"><p>که در حفظ یزدان تو اندر امانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنان قاصر است از مدیح تو طبعم</p></div>
<div class="m2"><p>که اندیشه از ذوره ی لا مکانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رسیدست در آرزوی ثنایت</p></div>
<div class="m2"><p>مرا بر لب حفظ جان معانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کشیدست در پا باقبال مدحت</p></div>
<div class="m2"><p>بقای ابد دیبه خسروانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زمان تا ز سیر کواکب زمین را</p></div>
<div class="m2"><p>بهاری کند گاه و گاهی خزانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ریاحین بزم ترا باد هر دم</p></div>
<div class="m2"><p>بهاری نو از دولت و کامرانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بد اندیش جاه ترا برگ چهره</p></div>
<div class="m2"><p>ز باد اجل باد چون زعفرانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سرشک حسودت پراکنده بر رخ</p></div>
<div class="m2"><p>چو بر برگ بی جاده بهرمانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شب و روز عمر ترا تا بمدت</p></div>
<div class="m2"><p>شود چون قضا و قدر جاودانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>قدر کرده هر شام صبح منادی</p></div>
<div class="m2"><p>قضا خوانده هر صبح سبع المثانی</p></div></div>