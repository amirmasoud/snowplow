---
title: >-
    شمارهٔ ۴۰ - در مدح علاء الدوله رکن الدین ابوالفتح قتلغ شاه
---
# شمارهٔ ۴۰ - در مدح علاء الدوله رکن الدین ابوالفتح قتلغ شاه

<div class="b" id="bn1"><div class="m1"><p>منت ایزد را که بعد از طول و عرض و سال و ماه</p></div>
<div class="m2"><p>اختر داد استقامت یافت بر گردون و جاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتاب نصرت از چرخ ظفر بنمود روی</p></div>
<div class="m2"><p>در پناه سایه ی پشت هدی ظل اله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتابی کافتاب افتاد در پایش ز چرخ</p></div>
<div class="m2"><p>آسمانی کاسمان بنهاد در دورش کلاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتابی مملکت در سیر و شاهی در جوار</p></div>
<div class="m2"><p>آسمانی سلطنت در دور و گیتی در پناه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن ز عکس رأیت منصور و دارای زمین</p></div>
<div class="m2"><p>وین ز فرّ سایه ی چترش پناه و تاج و گاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تاجبخش داد پرور، داور عادل، که هست:</p></div>
<div class="m2"><p>چهار طبعش خرج جود و نه سپهرش خاک راه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو اعظم، علاء الدوله، جمشید زمان</p></div>
<div class="m2"><p>رکن دین، بوالفتح، قتلغ شاه بن محمود شاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صفدر لشکر شکن شاهی که باشد در مصاف</p></div>
<div class="m2"><p>گوهر شمشیر او بر گوهر پاکش گواه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سایه بزدان خداوندی که دست لطف او</p></div>
<div class="m2"><p>دارد اندر تاب آتش آب نیلوفر نگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زبده دوران عدو بندی که چشم آفتاب</p></div>
<div class="m2"><p>می نیارد کردن اندر سایه ی چترش نگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن فلک رفعت شهنشاهی که بگدازد ز شرم</p></div>
<div class="m2"><p>بر سپهر از عکس ماه چتر او هر ماه، ماه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و آن ملک پرور جهانداری که بر درگاه اوست</p></div>
<div class="m2"><p>پادشاهان را جبین و داد خواهان را جباه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای باستحقاق با قصر مشید قدر تو</p></div>
<div class="m2"><p>قصر هفت اختر قیصر پشت نه گردون دو تاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وی جانگیری که چون در نیزه پیچی روز رزم</p></div>
<div class="m2"><p>بگسلند از هم ز روز و شب قطار سال و ماه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ابر احسان تو گر بر کوه خاره بگذرد</p></div>
<div class="m2"><p>از کمر شاخ زمرد سر بر آرد چون گیاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ور تفی ز اندیشه ی قهر تو بر دریا رسد</p></div>
<div class="m2"><p>هر کجا چاهیست آتش رخ برافروزد ز چاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تاجبخشی را پناهی پادشاهی را شکوه</p></div>
<div class="m2"><p>اهل دل را پایمردی اهل دین را دستگاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر کجا عون تو آمد مهر برچیند قضا</p></div>
<div class="m2"><p>هر کجا عفو تو امد رخت بر بندد گناه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صورت جاه تو چون در بارگاه آید؛ شود</p></div>
<div class="m2"><p>بارگاه از فرّ او فردوس بی هیچ اشتباه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دین پناها مر امامی را ز راه اعتقاد</p></div>
<div class="m2"><p>ورد اوقاتست حرز مدح تو بیگاه و گاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر درین حضرت جبونی یابد اندر ملک نظم</p></div>
<div class="m2"><p>هم ز حکمت صور سازد هم ز رفعت بارگاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا ز ترکیب طبایع بسته بر ایوان چشم</p></div>
<div class="m2"><p>پرده ی باشد سپید و گله ی دروی سیاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چشم روشن بادت از دیدار نور چشم ملک</p></div>
<div class="m2"><p>دوست گو بفزا ازین معنی و دشمن گو بکاه</p></div></div>