---
title: >-
    شمارهٔ  ۱۳ - در مدح فخرالملک
---
# شمارهٔ  ۱۳ - در مدح فخرالملک

<div class="b" id="bn1"><div class="m1"><p>شاه انجم چون ز برج جدی سر برمی‌زند</p></div>
<div class="m2"><p>شدت سرما دم از تأثیر اختر می‌زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سر گیتی سحاب از برف چادر می‌کشد</p></div>
<div class="m2"><p>در رخ گردون غمام از برق خنجر می‌زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرش گیتی را بخار از یشم تزئین می‌دهد</p></div>
<div class="m2"><p>طرح بستان را نسیم از سیم زیور می‌زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مادر پیر جهان هر روز پشت دست ابر</p></div>
<div class="m2"><p>غیبت خورشید را بر روی خاور می‌زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر مخالف نیست ماه دی طبایع، پس چرا</p></div>
<div class="m2"><p>رعد چون کوس رحیل ماه آذر می‌زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک پنهان می‌شود آتش علم بر می‌کشد</p></div>
<div class="m2"><p>آب جنجر می‌نماید، باد نشتر می‌زند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وز بر شاخ از پرند و پرنیان بر بود باد</p></div>
<div class="m2"><p>مسند و تخت چمن ز الماس و مرمر می‌زند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاغ را در باغ با شبنم حواصل می‌کند</p></div>
<div class="m2"><p>چون نمایم راه آتش، بر سمندر می‌زند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش خاک آتش از بس سردی آب و هوا</p></div>
<div class="m2"><p>مرغ آبی در هوای با بزن پر می‌زند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاخ آهو پر می مهر از چه می درد سپهر</p></div>
<div class="m2"><p>گر نه رای خدمت دستور کشور می‌زند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمس دین و ملک، دستوری که بزم و ساغرش</p></div>
<div class="m2"><p>بی‌تکلف طعنه بر فردوس و کوثر می‌زند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ملجاء دولت که خاک دامغان را پای فخر</p></div>
<div class="m2"><p>می‌رسد زو بر سر ملک جهان گر می‌زند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وآنکه دست همتش گاه سخا مسمار بخل</p></div>
<div class="m2"><p>بر در آوازهٔ یحیی و جعفر می‌زند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وانکه از رفعت زمین حضرتش پهلوی قدر</p></div>
<div class="m2"><p>گر تواضع می‌کند با چرخ اخضر می‌زند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون قلم در دست می‌گیرد کواکب را به رمز</p></div>
<div class="m2"><p>چرخ می‌گوید که دریا موج گوهر می‌زند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روح می‌بخشد مکارم را چو منقار از بنانش</p></div>
<div class="m2"><p>طوطی شکرشکن در مشک اذفر می‌زند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نزهت بزمت حوادث را به نیروی طرب</p></div>
<div class="m2"><p>با شکوه آسمان چون حلقه بر در می‌زند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حبذا بزمی که در قلب شتا قلب شتاش</p></div>
<div class="m2"><p>آتش اندر خرمن یاقوت احمر می‌زند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باده رنگینش بر خورشید تاوان می‌کند</p></div>
<div class="m2"><p>عارض ساقیش با ماه سما برمی‌زند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لاله را از ساغرش در سنگ دل‌خون می‌شود</p></div>
<div class="m2"><p>عکس جامش خنده بر اجرام ازهر می‌زند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در هوای خرمش در پرده‌های دل‌نواز</p></div>
<div class="m2"><p>زهره چون بر سازهای روح‌پرور می‌زند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پرتو جام مدام از دست ترکان چگل</p></div>
<div class="m2"><p>سنگ بر قندیل سالوس مزوّر می‌زند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رنگ و بوی آبی و رمان و سیب از ساحتش</p></div>
<div class="m2"><p>خاک در چشم ریاحین معطر می‌زند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رنگ آبی زو نشان روی عاشق می‌دهد</p></div>
<div class="m2"><p>گرچه دم هرجا که هست از بوی دلبر می‌زند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرکه پا در عرضه دولت‌پناهش می‌نهد</p></div>
<div class="m2"><p>دست دل در دامن پیغام ساغر می‌زند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لطف و قهر داور دنیا و دین، در صدر او</p></div>
<div class="m2"><p>کشوری می‌بخشد و ملکی به هم بر می‌زند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در چنین بزمی روان می‌پرورد هر کو چو من</p></div>
<div class="m2"><p>دم ز مدح خواجهٔ خورشید منظر می‌زند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای خداوندی که دور بارگاهت را جهان</p></div>
<div class="m2"><p>گاه رفعت بر سر چرخ مدوّر می‌زند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آتش خورشید هر ماهی عطارد را ز چرخ</p></div>
<div class="m2"><p>غیرت توقیعت اندر کلک و دفتر می‌زند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پیشگاه مسند و رای ترا تا آفتاب</p></div>
<div class="m2"><p>بوسه‌ها بر آستان نورگستر می‌زند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر در ارحبابت ار گردی است، گردون می‌کشد</p></div>
<div class="m2"><p>در کف بدخواهت ار سنگی است، بر سر می‌زند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دین‌پناها ز آنکه از زرها امامی بهتر است</p></div>
<div class="m2"><p>خاطر و قاد او در مدح تو در می‌زند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دامن گیتی پر از نقد امامی گشت و چرخ</p></div>
<div class="m2"><p>رأیت افلاس او هرروز برتر می‌زند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نی غلط می‌گویم این معنی که دست همتش</p></div>
<div class="m2"><p>کافرم گر کعبه آمال را در می‌زند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جام دولت تا جهان در دور گردون می‌خورد</p></div>
<div class="m2"><p>لاف دوران تا سپهر از خط محور می‌زند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>انجم و افلاک را سر بر در جاه تو باد</p></div>
<div class="m2"><p>تا حسودت بر در بی‌دولتی سر می‌زند</p></div></div>