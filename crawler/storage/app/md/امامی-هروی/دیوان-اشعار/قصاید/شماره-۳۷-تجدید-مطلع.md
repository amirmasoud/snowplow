---
title: >-
    شمارهٔ ۳۷ - تجدید مطلع
---
# شمارهٔ ۳۷ - تجدید مطلع

<div class="b" id="bn1"><div class="m1"><p>زهی جلال تو از عرش متکا کرده</p></div>
<div class="m2"><p>فروغ رای تو خورشید را سها کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قضا ز قدرت کلک تو مهره برچیده</p></div>
<div class="m2"><p>فلک بخاک جناب تو التجا کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان بعهد تو نا ایمن از سپهر شده</p></div>
<div class="m2"><p>فلک بدور تو تا بر جهان وفا کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پناه عدل ترا کعبه ی امان خوانده</p></div>
<div class="m2"><p>جناب جاه ترا قبله دعا کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آفتاب ضمیر تو در سواد امور</p></div>
<div class="m2"><p>مسیر کلک ترا منبع ضیا کرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو علت یرقان، در عروق کان، گه کین</p></div>
<div class="m2"><p>ز خون لعل نهیب تو کهربا کرده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غبار لعل کمیت تو ملکرا در چشم</p></div>
<div class="m2"><p>ز سنگ سرمه و از خاک تو تیا کرده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو کوه باد فرو مانده زو بسیر جهان</p></div>
<div class="m2"><p>بدو خطاب همه کوه باد پا کرده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز جرم خاک مه عکس نعل او بضیاء</p></div>
<div class="m2"><p>فروغ چهره ی خورشید را ضیا کرده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز کائنات بیک تک گذشته و بدو گام</p></div>
<div class="m2"><p>ز ابتدای جهان تا بانتها کرده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو چرخ چارم، خورشید دین و دنیا را</p></div>
<div class="m2"><p>ز سطح خانه ی زین خط استوار کرده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلم بفر مدیح تو در ممالک نظم</p></div>
<div class="m2"><p>نموده سحر و از آن سحر کیمیا کرده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صفات ذات تو طبع مرا مسیح شده</p></div>
<div class="m2"><p>ادای مدح تو خوف مرا رجا کرده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو، صوت راوی مدحت شنیده خاطر من</p></div>
<div class="m2"><p>درین میان غزلی دلفریب ادا کرده</p></div></div>