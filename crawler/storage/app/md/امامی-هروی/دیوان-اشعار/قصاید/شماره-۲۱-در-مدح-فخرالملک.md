---
title: >-
    شمارهٔ ۲۱ - در مدح فخرالملک
---
# شمارهٔ ۲۱ - در مدح فخرالملک

<div class="b" id="bn1"><div class="m1"><p>دوش چون برزد سر از جیب افق بدر منیر</p></div>
<div class="m2"><p>زورق زرین شتابان گشت در دریای قیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زورقی از نور و دریائی ز ظلمت، همچنانک</p></div>
<div class="m2"><p>رای دستور آورد بدخواه جاهش در ضمیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دامن دریا، از آن دریا، چو دریا، پرگهر</p></div>
<div class="m2"><p>مرزع ایام از آن زورق، چو زورق، در مسیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عزم عالیحضرتی کردم که از شوق ثناش</p></div>
<div class="m2"><p>در ریاض خلد آتش می شود جان ظهیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طالع سعد و هوای مدح صاحب چون نمود</p></div>
<div class="m2"><p>راه کرمانم خوش و آسان بپای باد گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماه مهر افروز من در کاروان آورد روی</p></div>
<div class="m2"><p>زلف و ابرو چون کمان و غمزه و بالا چو تیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلف چون بر لاله، سنبل، خط چو بر آتش دخان</p></div>
<div class="m2"><p>لب چو در یاقوت جان، رخساره چون در باده شیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخ صبوح اندر بهار و لب شراب اندر صبوح</p></div>
<div class="m2"><p>خط عبیر اندر گلستان، زلف تاب اندر عبیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون شبم زو، روز روشن گشت وز روز رخش</p></div>
<div class="m2"><p>کاروان در جنبش آمد، کاروانی در تعیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت کای در عشق من قولت سقیم و عهدست</p></div>
<div class="m2"><p>گفت کای در کار خود رایت جوان و بخت پیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در چنین فصلی که گوئی ز التهاب امر داد</p></div>
<div class="m2"><p>جنبش گردون مزاج باد را طبع اثیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جوشن ماهی ز گرمی هوا در عین آب</p></div>
<div class="m2"><p>همچنان سوزد که اندر شعله ی آتش حریر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر سمندر را در آتش پرورش بودی کنون</p></div>
<div class="m2"><p>می بسوزد حدّت گرماش بردم زمهریر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کوه آهن، دجله ی سیماب شد بر وی چو تافت</p></div>
<div class="m2"><p>برق تیغ آفتاب اندر میان ماه تیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتم ای جنت ز باغ فضل تو خاک زمین</p></div>
<div class="m2"><p>گفتم ای دوزخ ز تاب هجر تو عشر عشیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر شبی بی مهر رخسارت بروز آرد دلم</p></div>
<div class="m2"><p>روز عیشش چون شب زلفت پریشان باد و تیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کرد، چون کردم اساس عذرهای خوشگوار</p></div>
<div class="m2"><p>گشت، چون گشتم بگرد نکته های دلپذیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نرگسش را از تحسّر تاب و دل پروین فشان</p></div>
<div class="m2"><p>فندقش در بی بدیلی نظم پروین دستگیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز اشتیاقم ریخت بر زر طلی از سیم مذاب</p></div>
<div class="m2"><p>در وداعش گشت گلبرگ طری بدر منیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر گل نسرین روان کرد او گلابی گرم و من</p></div>
<div class="m2"><p>راندم از خون جگر سیلاب بر برگ زریر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برگرفتم زو دل و چون دولت آوردم بطبع</p></div>
<div class="m2"><p>روی دل در قبله ی اقبال در گاه وزیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آسمان داد فخرالملک شمس الدین، که داد</p></div>
<div class="m2"><p>دین و دنیا را حیات از حکم او حی قدیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صاحب کلک و قلم صدری که دست و کلک اوست</p></div>
<div class="m2"><p>موجب دریا و گوهر، مایه ی خورشید و تیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بسته اش را در تفکر تنگ شکر پایمال</p></div>
<div class="m2"><p>قند را در تعجب عقد گوهر دستگیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آنکه تا پروانه ی روزیست دستش خلق را</p></div>
<div class="m2"><p>کان و دریائی نماید از نظر خوار و حقیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>و آنکه تا نصرت ز کلک اوست تیغ شاه را</p></div>
<div class="m2"><p>ورد اوقاتست دین و ملک را نعم النصیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با خرد، در مدح او گفتم تو میدانی که هست</p></div>
<div class="m2"><p>ای جهان را از تو چون جان را ز جانان ناگزیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ابر احسان را بمعنی دست او بحر محیط</p></div>
<div class="m2"><p>کشت دولت را ببرهان کلک او ابر مطیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عقل گفت آگه نئی گوئی که دست و کلک اوست</p></div>
<div class="m2"><p>دور گردون را مشار و سیر انجم را مشیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دست دریا پرورش را بعد ازین توفیق خوان</p></div>
<div class="m2"><p>کلک فرمان گسترش را زین سپس تقدیر گیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای بنسبت باثبات حلم تو خارا بخار</p></div>
<div class="m2"><p>وی بهمت با وجود جود تو دریا غدیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نافذ امر تو حاکم چون قضا و چون قدر</p></div>
<div class="m2"><p>نهی کلک تو آگاه از قلیل و از کثیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همچو تو قبعت جهانداری کند امر ترا</p></div>
<div class="m2"><p>هر که سر بر خط نهد، چون خامه از دست دبیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گرنه از نیل سر کلک تو عمان قطره ایست</p></div>
<div class="m2"><p>در مکنون از چه معنی نیست در هر آبگیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گرنه از ایوان درگاه تو گردون غرفه ایست</p></div>
<div class="m2"><p>پس چرا در هر اثر درگاه تست او را اثیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کی توانستی طبایع که گردی زیر پای</p></div>
<div class="m2"><p>گر نگشتی خاک درگاهت سپهر مستدیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کی فکندی بر عصا آدم نظر، ثم اجتباه</p></div>
<div class="m2"><p>گر نبودی گوهرت را طینت آدم خبیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون نیاز از همت خصم تو اندر پرده رفت</p></div>
<div class="m2"><p>پیش از این، پوست خصمت برون آمد چو سیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در ازل گردون تنور خاطرت را گرم دید</p></div>
<div class="m2"><p>با کواکب گفت وقت آمد که در بندم فطیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در چنان روزی که گوئی آسمان در شأن او</p></div>
<div class="m2"><p>بر جهان می خواند «یوماً کان شره مستطیر»</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بیلک از صدر تن گردنکشان، می جست جای</p></div>
<div class="m2"><p>خنجر از قلب دماغ پر دلان، می جست تیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بی سپر گشتی ممالک سر کشیدندی سپاه</p></div>
<div class="m2"><p>در خطر بودی رعیت بی خطر ماندی خطیر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شد بلند اختر چو مهر رایت آمد در فروغ</p></div>
<div class="m2"><p>گشت بی رونق چو نوک کلک آمد در صریر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از صریر کلک دشمن سوز تو، تیغ و سپاه</p></div>
<div class="m2"><p>در پناه رای مهر افروز، تو تاج و سریر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دین پناها، صاحبا، من بنده را ز آنرو که نیست</p></div>
<div class="m2"><p>در سخن چون در سخا دست تو در عالم نظیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>عقل نپسندد که از راه حسد صاحب غرض</p></div>
<div class="m2"><p>هر زمان طعنی کند در لفظ و معنی، خیر، خیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر سخندانست و صاحب طبع و فاضل پس چرا</p></div>
<div class="m2"><p>خرمن ترکیب اشعارش نیر زد یک شعیر؟</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ور نمی داند سخن، گو دست از دعوی بدار</p></div>
<div class="m2"><p>خرده از بی خردگی بر مدح این حضرت مگیر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>پست و بی معنی است با جود تو و الفاظ من</p></div>
<div class="m2"><p>همت یحیی و جعفر - شعر اعشی و جریر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>حضرت دستور شرقست ای امامی دم مزن</p></div>
<div class="m2"><p>می نیندیشی ز نقد قلب و نقاد نصیر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا بنسبت با چهار ارکان نباشد نزد عقل</p></div>
<div class="m2"><p>جرم هفت اختر قیصر و قصر نه گردو قیصر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اختر و افلاک و ارکان باد در کل کمال</p></div>
<div class="m2"><p>سقف و صحن و ساحت ایوان جاهترا بشیر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سیر کلک را متابع هم ملوک و هم صدور</p></div>
<div class="m2"><p>دور حکمت را مساعد هم صغیر و هم کبیر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چهره ی ملت ز نور، روی و رایت با فروغ</p></div>
<div class="m2"><p>دیده دولت ز کحل خاک پایت مستنیر</p></div></div>