---
title: >-
    شمارهٔ  ۷ - در مدح شمس الدین صاحب دیوان
---
# شمارهٔ  ۷ - در مدح شمس الدین صاحب دیوان

<div class="b" id="bn1"><div class="m1"><p>آنکه بر تخت مکرمت شاهست</p></div>
<div class="m2"><p>شمس دین و دول شهنشاهست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رضی الملک کز دقایق غیب</p></div>
<div class="m2"><p>منهی نوک کلکش آگاهست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه تا روز، روز دولت اوست</p></div>
<div class="m2"><p>شب بیدولتی سحر گاهست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه تا عدل، عدل شامل اوست</p></div>
<div class="m2"><p>کهربا در حمایت کاهست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سر کلک او سخا گوئی</p></div>
<div class="m2"><p>در سقنقور قوه با هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غلطم، کافتاب همت او</p></div>
<div class="m2"><p>بر تر از اوج ماسوی اللهست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بزرگی که شاه جاه ترا</p></div>
<div class="m2"><p>افسر ماه و پایه ی جا هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وی سخا گستری که اطلس چرخ</p></div>
<div class="m2"><p>بر قد همت تو کوتاهست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جام گیتی نماست از چه تراست</p></div>
<div class="m2"><p>آسمان در پناه درگاهست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آفتابیست، کافتاب سپهر</p></div>
<div class="m2"><p>ز اقتباس فروغ او، ماهست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گوئی از یاد بزم و ساغر تو</p></div>
<div class="m2"><p>باده شادی فزا و غم کاهست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گوئی از حرض بذل و گاه سخا</p></div>
<div class="m2"><p>معطی دست تو عطا خواهست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لفظ عذب تو در نظام جهان</p></div>
<div class="m2"><p>رونق ملک و زیور جاهست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نوک کلک تو در معانی جود</p></div>
<div class="m2"><p>سلب اقران و نفی اشیا هست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دین پناها بهر نفس که زند</p></div>
<div class="m2"><p>گرچه دشمنت ناله و آهست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاه انجم که مرکب و بنه اش</p></div>
<div class="m2"><p>شش سوارند و هفت خرگاهست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با سپه دار آسمان، که ازو</p></div>
<div class="m2"><p>مرگ آشفته در کمین گاهست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گوید احکام ما و جنبش او</p></div>
<div class="m2"><p>محض اجبار و عین اکراهست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طبع من گرچه مدحت تو در او</p></div>
<div class="m2"><p>یونس و حوت و، یوسف و چاهست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن درختیست بارور که ترا</p></div>
<div class="m2"><p>ذکر باقی ازو در افواهست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>میوه اش نارسیده می برسد</p></div>
<div class="m2"><p>زانکه بی برگ و بر سر راهست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در دوم مرتبت ز روی حساب</p></div>
<div class="m2"><p>تا توان گفت پنج پنجاهست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شادمان زی که سال عمر ترا</p></div>
<div class="m2"><p>مدت چرخ روزی از ما هست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بود مأمور تو جهان تا بود</p></div>
<div class="m2"><p>هست محکوم تو جهان تا هست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که سخای تو و تلکم خصم</p></div>
<div class="m2"><p>صدمت شیر و کید روباهست</p></div></div>