---
title: >-
    شمارهٔ ۳۳ - در مدح فخرالملک
---
# شمارهٔ ۳۳ - در مدح فخرالملک

<div class="b" id="bn1"><div class="m1"><p>ای ز ایوان رفیعت آسمان گشته زمین</p></div>
<div class="m2"><p>ملک و دین را حلقهٔ درگاه تو حبل المتین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حشمت گیتی پناه تست دولت را مکان</p></div>
<div class="m2"><p>بارگاه آسمان سقف تو رفعت را مکین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاتم ملک جهان را باشد از بهر شرف</p></div>
<div class="m2"><p>عز و اقبال در و درگاه تو نقش نگین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کند با آسمان در مرتبت سقف قران</p></div>
<div class="m2"><p>می شود با آفتاب از روشنی صحنت قرین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هوای قبه ی سقف تو می گردد سپهر</p></div>
<div class="m2"><p>روز و شب گرد جهان با اندرونی آتشین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با رخی چون کهربا می بوسد از چارم فلک</p></div>
<div class="m2"><p>آفتاب از تاب مهر ماه دیوارت زمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نزهت بستان و حوض آب و خاک سطح تست</p></div>
<div class="m2"><p>روضه ی فردوس و عین کوثر و ماء معین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صحن باغ خرمت خلد برینست و در او</p></div>
<div class="m2"><p>ساحت جان پرورت پیرایه ی خلد برین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می نهد بر خاک پیش سایه دیوار تو</p></div>
<div class="m2"><p>مهر بهر بندگیء صاحب اعظم جبین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آسمان داد فخر الملک، شمس الدین که، هست</p></div>
<div class="m2"><p>منبع آب حیات از کلک او در ثمین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صاحب سیف و قلم، دین پرور عادل که هست</p></div>
<div class="m2"><p>در پناه و خامه و شمشیر او دنیا و دین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن جهانداری که در صدر وزارت می کند</p></div>
<div class="m2"><p>آفرین بر حضرت او حضرت جان آفرین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای مسیر کلک ملک آرای ملت پرورت</p></div>
<div class="m2"><p>ملک را صاحب شریعت شرع را روح الامین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آیت عدل تو تا مُنزل شد اندر شأن ملک</p></div>
<div class="m2"><p>پیش آهو با تواضع می رود شیر عرین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>معجز کلک تو تا ظاهر شد اندر باب نطق</p></div>
<div class="m2"><p>بعد از آن نگذشت کسرا بر زبان سحر مبین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زاتش کین تو چون شمع آنکه ناگه برفروخت</p></div>
<div class="m2"><p>گرچه با خورشید پهلو می زند در روز کین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هم بدان آتش جدا کرد آخر الامرش بقهر</p></div>
<div class="m2"><p>دور چرخ از جان شیرین همچو موم از انگبین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا بسیط خاک را، گیتی نفرساید از آب</p></div>
<div class="m2"><p>تا بنات نفس را گردون نگرداند بنین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خاک درگاه رفیعت را که آب زندگیست</p></div>
<div class="m2"><p>همچنین اقبال همدم باد و دولت همنشین</p></div></div>