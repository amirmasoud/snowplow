---
title: >-
    شمارهٔ ۳۵ - مژده دولت را که باز آمد سوی اقلیم جاه
---
# شمارهٔ ۳۵ - مژده دولت را که باز آمد سوی اقلیم جاه

<div class="b" id="bn1"><div class="m1"><p>مژده دولت را که باز آمد سوی اقلیم جاه</p></div>
<div class="m2"><p>صاحب صاحبقران، در سایه ظل اله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سپهر افکنده سایه، در جهان گسترده عدل</p></div>
<div class="m2"><p>هم جهانش زیر دست و هم سپهرش زیر گاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آصف جمشید دین، جمشید شادروان فضل</p></div>
<div class="m2"><p>پادشاه ملک داد و داد ملک پادشاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صاحب سیف و قلم صدری که تیغ و خامه اش</p></div>
<div class="m2"><p>پادشاهی را شکوهند و وزارت را پناه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمس دین و ملک دستوری که دست حفظ او</p></div>
<div class="m2"><p>دارد اندر عین آب، اجزاء خاکی را نگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه گر یاد آرد از ماضی باستقبال او</p></div>
<div class="m2"><p>باز گردانند روز و شب عنان سال و ماه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نبودی عنصرش را طینت آدم خمیر</p></div>
<div class="m2"><p>کی فکندی بر عصا آدم نظر ثم اجتباه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ورنه صدرش تربیت کردی بدولت جاه را</p></div>
<div class="m2"><p>گر نکردی از جهانداران سر افرازی بجاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای خداوندی که کوته کرد دور عدل تو</p></div>
<div class="m2"><p>جور مغناطیس از آهن دست بیچاره ز کاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عقل اولی را غرض در طول و عرض کائنات</p></div>
<div class="m2"><p>عرض تست از عرض جوهر تا عرض بیگاه و گاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پرتو رای تو هیچ ار سایه بر آب افکند</p></div>
<div class="m2"><p>هر کجا چاهیست خورشیدی بر آرد سر ز چاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رایت قدر ترا گر سر فرود آرد بچرخ</p></div>
<div class="m2"><p>خیمه ها گردند گردونها و کوکبها سپاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز ابر لطفت گر نشیند شبنمی بر روی خاک</p></div>
<div class="m2"><p>از زمین تا حشر مروارید روید چون گیاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور سمومی ز آتش قهر تو بر گردون رسد</p></div>
<div class="m2"><p>تیره گردد ز آتش قهر تو آب مهر و ماه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بارگاه قدرت از نه طاق گردون برتر است</p></div>
<div class="m2"><p>ای باستحقاق صاحب مسند این بارگاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قرب یکسال است تا در خاک کرمان</p></div>
<div class="m2"><p>جور گردون انتقام از خاطر من بیگناه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر ز دیوان قبولت یک نظر یابم کنند</p></div>
<div class="m2"><p>انجم و افلاک در توقیع دیوانم نگاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آتش اندیشه ز آب شعرم افتد در اثر</p></div>
<div class="m2"><p>لاف دعوی نیست اینک بنده و اینک گواه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زین قصیده دوستی منحول کرده چند بیت</p></div>
<div class="m2"><p>دوش بر من خواند دور از مدحت دستور شاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرچه دون پایه ی من باشد ار فخر آورد</p></div>
<div class="m2"><p>بر جهان از رونق الفاظ من خاک هراه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گنج گوهر را کجا نسبت کنم با خشت خام</p></div>
<div class="m2"><p>آب حیوان را چرا یکسان نهم با خاک راه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>منت ایزد را که منحول وی و الفاظ من</p></div>
<div class="m2"><p>عنت ذاتی و کافور و سقنقور است و باه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا سپیدی و سیاهی لازم روز و شبند</p></div>
<div class="m2"><p>روز و شب را بر در عمر تو باد آرامگاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روی نیکو خواه و رای دشمنت چون صبح و شام</p></div>
<div class="m2"><p>آن ز پیروزی سپید و این ز بدبختی سیاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>طالعت سعد و سپهرت تابع و بختت مطیع</p></div>
<div class="m2"><p>کار احبابت قوی و حال بد خواهت سیاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در بر خصمت شکسته ز آفت و ز آسیب دل</p></div>
<div class="m2"><p>بر خط امرت نهاده انجم و ارکان جباه</p></div></div>