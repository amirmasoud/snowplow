---
title: >-
    شمارهٔ ۴۲ - در مدح فخرالملک
---
# شمارهٔ ۴۲ - در مدح فخرالملک

<div class="b" id="bn1"><div class="m1"><p>صبح است در ده ای پسر ماه چهره می</p></div>
<div class="m2"><p>برخیز و آفتاب ببین در صفای وی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنمای رخ که طیره ی مهر است ای غلام</p></div>
<div class="m2"><p>پیش آر، می که وقت صبوحست ای صبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل در پیاله بند که از حضرت صبوح</p></div>
<div class="m2"><p>آورد خط بخون صراحی ز شوق می</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برخیز با نسیم صبا، ناز تا بچند</p></div>
<div class="m2"><p>می نوش؛ در حریم قضا زار تا بکی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای در نهاد مهر ز شوق لب تو شور</p></div>
<div class="m2"><p>وی برجبین حسن ز شرم رخ تو خوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کعبه ی جمال ترا بر سپهر صیت</p></div>
<div class="m2"><p>خاک رخ تو شد ز فروغ رخت جدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر لاف نیکوئی نزدی با رخ تو بدر</p></div>
<div class="m2"><p>طومار حسن او نشدی ز آفتاب طی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور مهر در کمان ز رخت تیر یافتی</p></div>
<div class="m2"><p>فصل بهار روی نمودی ز ماه دی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وصف تو شاهنامه ی خوبیست چون فکند</p></div>
<div class="m2"><p>بر وی همای مدح پناه زمانه حی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اکفی الکفاة کافی دین کز بنان اوست</p></div>
<div class="m2"><p>دیندار و ملک پرور و گوهر نگار، نی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صدری که جز سعادت باران کلک او</p></div>
<div class="m2"><p>ننشاند از هوای هدایت غبار غی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مقصود کون اگر نه کف و کلک اوستی</p></div>
<div class="m2"><p>برداشتی وجود ز کونین اسم شی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای مسرع ضمیر ترا ملک هر دو کون</p></div>
<div class="m2"><p>در حل و عقد ملک گذشته بزیر پی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر داغگاه توسن دولت بنام تو</p></div>
<div class="m2"><p>دست کفایت تو نهاد از دوام کی!</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اقبال را بجاه تو چندان تفاخر است</p></div>
<div class="m2"><p>که اسلام را به کعبه و اعراب را بحی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفتم روان حاتم طی در بنان تست</p></div>
<div class="m2"><p>پیر سپهر گفت چه حاتم؟ کدام طی؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کاندر جهان همت صاحب خزانه ایست</p></div>
<div class="m2"><p>در ملک صیت روی زمین صیت ملک ری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر قبله ی قبول نه خاک جناب تست</p></div>
<div class="m2"><p>در بارگاه شرع چه یغما برد چه فی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ور نیستی ز حکم تو کوتاه دست جسم</p></div>
<div class="m2"><p>آتش بجای آب برون آمدی ز فی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دور از تحمل تو شب از روز بگسلد</p></div>
<div class="m2"><p>گر بانگ بر زمانه زنی کای زمانه هی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا متفق شدند اطبا که در مذاق</p></div>
<div class="m2"><p>زهر هلاهل است لعاب دهان حی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>می در مذاق دشمن جاه تو زهر باد</p></div>
<div class="m2"><p>زهری چنانکه باز نگردد بنام وی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر روز در جوار تو دولت هزار بار</p></div>
<div class="m2"><p>هر روز در پناه تو ملت هزار پی</p></div></div>