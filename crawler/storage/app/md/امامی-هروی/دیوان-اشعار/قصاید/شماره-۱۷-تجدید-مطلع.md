---
title: >-
    شمارهٔ ۱۷ - تجدید مطلع
---
# شمارهٔ ۱۷ - تجدید مطلع

<div class="b" id="bn1"><div class="m1"><p>کای پریچهره مست خواب و خمار</p></div>
<div class="m2"><p>ای تو بهتر ز لعبتان تتار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقد لؤلؤ نموده از یاقوت</p></div>
<div class="m2"><p>لعل شکر گشاده از گفتار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب حیوانش بر دو گوشه ی لعل</p></div>
<div class="m2"><p>نظم پروپنش در دو دانه فار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سمنش رخ کشیده در سنبل</p></div>
<div class="m2"><p>سنبلش رنگ داده بر گلنار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرده بر طرف آفتاب پدید</p></div>
<div class="m2"><p>سیر ماهش هلالی از زنگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخش از زلف ماه در عقرب</p></div>
<div class="m2"><p>زلفش از چهره مار در گلزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عار او در پناه بدر منیر</p></div>
<div class="m2"><p>ماه او در ثعاب مشک تتار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آفت از شور سنبلش در شهر</p></div>
<div class="m2"><p>فتنه از دست نرگسش در کار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ساغری نیم مست عربده جو</p></div>
<div class="m2"><p>هندوئی پیچ پیچ و آینه دار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با رخی کافتاب پیش رخش</p></div>
<div class="m2"><p>کرد از شرم روی در دیوار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از در حجره ام در آمد و گفت</p></div>
<div class="m2"><p>کای بر اسب سخن چو روح سوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرچه امروز در بساط زمین</p></div>
<div class="m2"><p>جز تو کس را نباشد این احضار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که کند کشف در معانی بکر</p></div>
<div class="m2"><p>تا حجاب تراکم اطوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خرده بین ضمیر وقادش</p></div>
<div class="m2"><p>در پس پرده های غیب اسرار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیتکی چند کرده ام ترکیب</p></div>
<div class="m2"><p>به ز ترکیب عذب تو بسیار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در مدیح پناه پشت صدور</p></div>
<div class="m2"><p>آن زمین حلم آسمان مقدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صاحب تیغ و خامه کز قلمش</p></div>
<div class="m2"><p>خضر ز آب حیات کرده کنار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صدر اعظم شهاب دولت و دین</p></div>
<div class="m2"><p>قبله قدوه و صدور کیار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گهر نظم آبدار مرا</p></div>
<div class="m2"><p>بزبانی چو آب گوهربار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر خداوند خویش خواند و بداد</p></div>
<div class="m2"><p>داد این قطعه آن بت عیار</p></div></div>