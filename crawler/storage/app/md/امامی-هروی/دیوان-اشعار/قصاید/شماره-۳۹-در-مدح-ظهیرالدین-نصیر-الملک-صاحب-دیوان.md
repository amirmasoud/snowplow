---
title: >-
    شمارهٔ ۳۹ - در مدح ظهیرالدین نصیر الملک صاحب دیوان
---
# شمارهٔ ۳۹ - در مدح ظهیرالدین نصیر الملک صاحب دیوان

<div class="b" id="bn1"><div class="m1"><p>ای به صنعت شام را بر صبح پرچین ساخته</p></div>
<div class="m2"><p>صبح را میخانه کرده، شام را چین ساخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صحبت از برگ سمن بر گل فروغ انداخته</p></div>
<div class="m2"><p>شامت از مشک ختن بر ماه پرچین ساخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف مشکین گرد ماهت گرد عنبر بیخته</p></div>
<div class="m2"><p>گرد عنبر گرد ماهت خط مشکین ساخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاله خود رو از آن در عهد سنبل آمده</p></div>
<div class="m2"><p>سنبل سیراب ازین بر برگ نسرین ساخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو سیمینت روان گشته ولی در فکر دین</p></div>
<div class="m2"><p>با همت دیو و پری بر سرو و سیمین ساخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جزع و لعل ترا قیاس عقل در نفس و خیال</p></div>
<div class="m2"><p>صورت جان و دل فرهاد شیرین ساخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لعلت از چشمم گسسته نظم پروین، تا رخت</p></div>
<div class="m2"><p>جان و مرجان را پناه نظم پروین ساخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خون چشمم ریخته جزع تو تا لعل تو ام</p></div>
<div class="m2"><p>مشرب روح از عقیق گوهر آگین ساخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باد یاقوت شکر بار تو در بحران عجز</p></div>
<div class="m2"><p>چشم رنجور مرا چون جان شیرین ساخته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم رخ خوبت ز مشگناب بستر یافته</p></div>
<div class="m2"><p>هم سر زلفت ز سیم خام بالین ساخته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طبع من بیچاره از وصف رخ و زلفت بطبع</p></div>
<div class="m2"><p>در مدیح صاحب عادل دو اوین ساخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صاحب اعظم ظهیر الدین سپهر مکرمت</p></div>
<div class="m2"><p>آن شکار دولت از طه و یس ساخته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حاتم ثانی نصیر الملک کز درگاه اوست</p></div>
<div class="m2"><p>کار اهل فضل و زوّار و مساکین ساخته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنکه در پرواز باز فتنه دست عدل او</p></div>
<div class="m2"><p>آشیان صعوه اندر چشم شاهین ساخته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه جز در دور احسانش هنر را کس ندید</p></div>
<div class="m2"><p>این چنین کاری بحمدالله بآئین ساخته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حشمت دوران توقعیش قواعد دوخته</p></div>
<div class="m2"><p>دولت از درگاه تعظیمش قوانین ساخته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زرگر احسان او در حلقه های اشتیاق</p></div>
<div class="m2"><p>ز انتظار سائلان چرخ جهان بین ساخته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای ز روی مرتبت در بدو فطرت صنع حق</p></div>
<div class="m2"><p>گوهرت را موجب ایجاد و تکوین ساخته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا وجود شبه تو ممکن نباشد خویش را</p></div>
<div class="m2"><p>بکر خوانده عنصر و افلاک عنّین ساخته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیدق خورشید را هر صبح چرخ نیلگون</p></div>
<div class="m2"><p>در مجازات رخ تو رای فرزین ساخته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز آتش خشمت چو خون جوشیده در تن خصم را</p></div>
<div class="m2"><p>شربتی ز آب حسامت بهر تسکین ساخته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای باستحقاق و برهان حکمت پروردگار</p></div>
<div class="m2"><p>آستانت را مقر عزّ و تمکین ساخته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صرصر قهر تو از خوناب و مغز و استخوان</p></div>
<div class="m2"><p>در تن خصم تو ناوک کرده، زوبین ساخته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قرب ده سالست تا در کنج عزلت مادحت</p></div>
<div class="m2"><p>منزوی بودست و استغنا به تحسین ساخته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ممکن کینش شده در دیده ملک آرزو</p></div>
<div class="m2"><p>با خلاف خاطر اندر مکمن کین ساخته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خاطرش در خوض بحر انزوا ادراک را</p></div>
<div class="m2"><p>دست و دامن گوهر آگین و گهر چین ساخته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کرده خورسندیش قانع گر نه با پندار نفس</p></div>
<div class="m2"><p>این چنین کاری کرا گردد، بتخمین ساخته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا چو مهر از چرخ زنگاری پدید آید شود</p></div>
<div class="m2"><p>کسوت زربفت هفت اقلیم در چین ساخته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>باد توقیعت طراز کسوت دنیا کزوست</p></div>
<div class="m2"><p>تا قیامت کار ملک و دولت و دین ساخته</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خاطری کو سر فرو ناورده در دوران چرخ</p></div>
<div class="m2"><p>با دعای دولت مدحت چو آمین ساخته</p></div></div>