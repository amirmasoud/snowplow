---
title: >-
    شمارهٔ  ۱۴ - در مدح فخرالملک
---
# شمارهٔ  ۱۴ - در مدح فخرالملک

<div class="b" id="bn1"><div class="m1"><p>سلامی نجوم سما زو منور</p></div>
<div class="m2"><p>سلامی نسیم صبا زو معنبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلامی چو ارواح قدسی یکایک</p></div>
<div class="m2"><p>سلامی چو اجسام علوی سراسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلامی بسبع الثمانی مؤکد</p></div>
<div class="m2"><p>سلامی ز سبع السماوات برتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سلاکی در او رونق حسن مدغم</p></div>
<div class="m2"><p>سلامی در او قوه عشق مضمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلامی حروفش ز عقل مجرد</p></div>
<div class="m2"><p>سلامی وجودش ز نفس سخنور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلامی همه نزهت خلد اعلی</p></div>
<div class="m2"><p>سلامی همه صفوة آب کوثر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلامی همه شوق چون یاد جانان</p></div>
<div class="m2"><p>سلامی همه لطف چون لعل و گوهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سلامی شتابنده چون چرخ اعظم</p></div>
<div class="m2"><p>سلامی فروزنده چون نجم ازهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز من بنده بر حضرت دین پناهی</p></div>
<div class="m2"><p>که هستندش افلاک انجم مسخر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سپهر سخا، صاحب دین و دنیا</p></div>
<div class="m2"><p>جهان سخن، صاحب کلک و دفتر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پناه هدی مجد الدین آنکه کلکش</p></div>
<div class="m2"><p>نهالیست در باغ دین سایه گستر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زهی آفتاب سپهر سخن را</p></div>
<div class="m2"><p>سر کلک دولت پناه تو خاور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زهی بار و برگ درخت معانی</p></div>
<div class="m2"><p>ز باران الفاظ تو درّ و گوهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زهی سر حزم تو پیرامن دین</p></div>
<div class="m2"><p>شده ناقض صیت سد سکندر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زهی آب قندیل طبع لطیفم</p></div>
<div class="m2"><p>ز جان روغن مدحت آورده برسر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر افروخته شعله خاطرم زو</p></div>
<div class="m2"><p>چو ماه در افشان و خورشید انور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فلک رفعتا زآنکه حال رهی را</p></div>
<div class="m2"><p>درین وقت مدح و غزل نیست درخور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من بنده گفتم سلامی رساند</p></div>
<div class="m2"><p>رکاب همایونت را صدر کشور</p></div></div>