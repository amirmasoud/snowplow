---
title: >-
    شمارهٔ ۴۴ - قصیده در ملک فخرالملک
---
# شمارهٔ ۴۴ - قصیده در ملک فخرالملک

<div class="b" id="bn1"><div class="m1"><p>چون کبک شسته لب بشراب مروقی</p></div>
<div class="m2"><p>کبکی از آن بطوق معنبر مطوقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بزم خوبتر ز تذر و ملوّنی</p></div>
<div class="m2"><p>و اندر مصاف چیره تر از بازار ازرقی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آفتاب، طنز کنی و مسلّمی</p></div>
<div class="m2"><p>بر مشتری و ماه بخندی و بر حقی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ماه در لباس کبود منقط است</p></div>
<div class="m2"><p>تو شاه در لباس بسیج مفرقی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماند همی بروشنی ماهتاب از آن</p></div>
<div class="m2"><p>سیمین برت بزیر بغلطاق فستقی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر آب دیده پیش تو زورق روان کنم</p></div>
<div class="m2"><p>گر ز آنکه بینمت که تو مایل بزروقی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر حور عین بیند، عنّاب شکرت</p></div>
<div class="m2"><p>آیا که چون گزند سر انگشت فندقی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر شاه ملک حسنی اندر بساط دهر</p></div>
<div class="m2"><p>در صدر خواجه به بودت جای بیدقی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تاج امم، خدیو جهان، فخر ملک و دین</p></div>
<div class="m2"><p>کز آدم اوست گوهر و سنگند، ما بقی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون نزد سروران بکرم نام او برند</p></div>
<div class="m2"><p>تن در دمد زمانه باسم مطابقی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرزین ملک شاه که بر عرصه خرد</p></div>
<div class="m2"><p>با او رخ کمال در آید به بیدقی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دعوی همی کنم بزمان کرم که من</p></div>
<div class="m2"><p>بی مثلم از کرام و جهان مصدقی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای آنکه عز و جاه بزرگان کشوری</p></div>
<div class="m2"><p>وی آنکه صدر و بدر وزیران مطلقی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>محضول کارگاه نجوم مزینی</p></div>
<div class="m2"><p>مقصود گرد گشتن چرخ مطبقی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اندر بهار فضل نسیم معطری</p></div>
<div class="m2"><p>وندر نسیم خلق بهار خور نقی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیش حصار خرم تو حصن دولتست</p></div>
<div class="m2"><p>بحر محیط پای ندارد بخندقی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بی مجلس تو طبع ندارد معاشرت</p></div>
<div class="m2"><p>بی ساغر تو می بگذارد مروقی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>موضوع کردی از کف بخشنده اسم جود</p></div>
<div class="m2"><p>تو صدر کز مصادر اقبال مشتقی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فضل تو بخردان حقیقت بدیده اند</p></div>
<div class="m2"><p>زان در هنر بنزد بزرگان محققی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن دل که شد معلق مهر و هوای تو</p></div>
<div class="m2"><p>چون زلف دوست رنج ندید از معلقی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این شعر داشت قافیتی مغلق آنچنانک</p></div>
<div class="m2"><p>بر بستمش که کس نتواند ز مغلقی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من پارسی زبانم از آن کردم احتراز</p></div>
<div class="m2"><p>ز آن تازئی که خنده زند از مربقی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گردم همی بگرد سخنهای دلفریب</p></div>
<div class="m2"><p>در آرزوی شعر شعر معزی و ازرقی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ناید بدین قوافی زین خوبتر سخن</p></div>
<div class="m2"><p>گرچه سخن تر از نماید فرزدقی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>احمق بود که عرضه کند فضل پیش تو</p></div>
<div class="m2"><p>خرما ببصره بردن باشد ز احمقی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا زین چرخ اشهب و کره ی زمین بود</p></div>
<div class="m2"><p>از مرکب زمانه نیاید جز ابلقی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر هر مراد و کام که داری مظفری</p></div>
<div class="m2"><p>وز هر سپهر هر چه بخواهی موفقی</p></div></div>