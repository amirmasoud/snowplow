---
title: >-
    شمارهٔ ۲۶ - در مدح زین الدین هندی
---
# شمارهٔ ۲۶ - در مدح زین الدین هندی

<div class="b" id="bn1"><div class="m1"><p>دوش بیخود ز خود جدا گشتم</p></div>
<div class="m2"><p>با خدای خود آشنا گشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظری بر دلم فکند کز او</p></div>
<div class="m2"><p>کاشف رمز انبیا گشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببقای ابد رسیدم از آن</p></div>
<div class="m2"><p>که بکلی ز خود فنا گشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش ازین بنده خودم بودم</p></div>
<div class="m2"><p>که بمعقول مبتلا گشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نفس ز آستان عشق زدم</p></div>
<div class="m2"><p>بهبر عقل رهنما گشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قلب معشوق بودم اول کار</p></div>
<div class="m2"><p>ز غش و غیر تا جدا گشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خلوص عنایتش ز اخلاص</p></div>
<div class="m2"><p>زر شدم بلکه، کیمیا گشتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا حواس جهات و ارکان را</p></div>
<div class="m2"><p>در ره فقر پیشوا گشتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آسیا بر سرم بگشت ولی</p></div>
<div class="m2"><p>گرد خود همچو آسیا گشتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ره بده بردم آخر از پی غول</p></div>
<div class="m2"><p>گرچه بیهوده سالها گشتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ملک ده ملک خصم بود و در او</p></div>
<div class="m2"><p>به بسی جهد کدخدا گشتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تخم خود در زمین خود کشتم</p></div>
<div class="m2"><p>کشت خود را بخود نما گشتم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مصر جامع شد ایندم آن ده و من</p></div>
<div class="m2"><p>یوسف مصر کبریا گشتم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خاک ارکانش را سپهر شدم</p></div>
<div class="m2"><p>چشم اعیانش را سها گشتم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بمثالی دگر کنم تقدیر</p></div>
<div class="m2"><p>این خبر را که مبتدا گشتم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همچو باران چو از سحاب غرور</p></div>
<div class="m2"><p>سالک خطه ی هوی گشتم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صدف صدقم از هوی پر بود</p></div>
<div class="m2"><p>من از آن صدق با صفا گشتم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مدتی در میان صدق و صفا</p></div>
<div class="m2"><p>غرقه در بحر انزوا گشتم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سلوت خلوتم چو روی نمود</p></div>
<div class="m2"><p>در صدف در بی بها گشتم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در مراقب چو گوهرم دیدند</p></div>
<div class="m2"><p>افسر شاهرا سرا گشتم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>راه خود را بخود دلیل شدم</p></div>
<div class="m2"><p>درد حق را بحق دوا گشتم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آب حیوان بعلم و عقل شبی</p></div>
<div class="m2"><p>جستم و علم و عقرا گشتم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ناوک علمرا نشانه شدم</p></div>
<div class="m2"><p>سخره انجم و سها گشتم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کس نشانم نداد ز آب حیات</p></div>
<div class="m2"><p>گر درین هر دو خطه تا گشتم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آب حیوان شدم چو در ره فقر</p></div>
<div class="m2"><p>خاک سلطان اولیاء گشتم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زین دین، پیرهند، کز نظرش</p></div>
<div class="m2"><p>نظر رحمت خدا گشتم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آنکه از برق نور معرفتش</p></div>
<div class="m2"><p>تیره ی عقل را ضیاء گشتم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>و آنکه لطف ارادتش تا گفت</p></div>
<div class="m2"><p>تو مرا شو که من ترا گشتم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چمن شوقرا سحاب شدم</p></div>
<div class="m2"><p>گلبن عشق را صبا گشتم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مالک ملک فقر کز نفسش</p></div>
<div class="m2"><p>ملک بخش جهان گشا گشتم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سالک راه حق که در قدمش</p></div>
<div class="m2"><p>تا شدم خاک، توتیا گشتم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ای باخلاق حق شده موصوف</p></div>
<div class="m2"><p>تا ترا ناظم ثنا گشتم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آفتاب سپهر عرفانرا</p></div>
<div class="m2"><p>بی گمان خط استوا گشتم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مرده ی جسم را مسیح شدم</p></div>
<div class="m2"><p>هدهد روح را سبا گشتم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون غنیمت نمودیم در فقر</p></div>
<div class="m2"><p>یافتم فقر و ز اغنیاء گشتم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون شدم نیست در ولایت عشق</p></div>
<div class="m2"><p>والی خطه ی بقا گشتم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا سخندان بیان تواند کرد</p></div>
<div class="m2"><p>که سخن پرور از کجا گشتم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اقتدای سخن بنام تو باد</p></div>
<div class="m2"><p>که ز مدح تو مقتدا گشتم</p></div></div>