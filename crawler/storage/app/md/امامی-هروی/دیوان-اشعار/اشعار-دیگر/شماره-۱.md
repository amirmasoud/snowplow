---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ای خانه و ضمیر تو خورشید و تیر ملک</p></div>
<div class="m2"><p>ای در نظام دور جهان دستگیر ملک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم قدر تو شکوه ملوک و صدور دهر</p></div>
<div class="m2"><p>هم صدر تو پناه صغیر و کبیر ملک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در فیض مکرمت قلمت تا شکیب داد</p></div>
<div class="m2"><p>در صدر مملکت کرمت ناگزیر ملک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حصن حصین حزم تو قصر مشید جاه</p></div>
<div class="m2"><p>قصر مشید جاه تو چرخ اثیر ملک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با روزگار دوش چه کرد اقتصار طبع</p></div>
<div class="m2"><p>بخت جوان اهل هنر رای تیر ملک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم بعز جاه که سر بر فلک کشید</p></div>
<div class="m2"><p>دست سخا و مسند فضل از سریر ملک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت از شکوه دولت صاحبقران عهد</p></div>
<div class="m2"><p>دستور شرق حاتم ثانی مشیر ملک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان کرم، جهان معانی، ظهیر دین</p></div>
<div class="m2"><p>مانند روغنست در اجزاء شیر ملک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پی امر تو مباد وضیع و شریف دهر</p></div>
<div class="m2"><p>بی حکم تو مباد قلیل و کثیر ملک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا ز آفتاب رأی تو کرد اقتباس نور</p></div>
<div class="m2"><p>اجرام روشنند ز بدر منیر ملک</p></div></div>