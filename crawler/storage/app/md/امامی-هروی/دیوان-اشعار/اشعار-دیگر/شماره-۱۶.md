---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>دلم ز روح سحرگه سوال کرد به لفظی</p></div>
<div class="m2"><p>چنانکه آب خجل گشت از او به گاه روانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چیست در همه عالم به طبع موجب صحت</p></div>
<div class="m2"><p>که کیست در همه گیتی سجود حاتم ثانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جواب داد دلم را که ای بر اسب تفکر</p></div>
<div class="m2"><p>ز هر دو کون گذشته به گاه تیر عنانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی می است که مرا را به نقد باز رهاند</p></div>
<div class="m2"><p>به اتفاق طبایع ز اندوه دو جهانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی پناه صدور است و افتخار اکابر</p></div>
<div class="m2"><p>شهاب دولت و ملت، سپهر مهر معانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایا رسیده به جایی که هرچه در نظر آید</p></div>
<div class="m2"><p>به فکر پیر خرد را به قدر برتر از آنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا به یک دو صراحی، شراب لعل بر آنم</p></div>
<div class="m2"><p>که از سوال و جواب خرد توام برهانی</p></div></div>