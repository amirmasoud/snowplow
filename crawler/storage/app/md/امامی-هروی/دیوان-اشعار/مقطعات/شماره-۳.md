---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>بخدائی که بی ارادت او</p></div>
<div class="m2"><p>سرورا برگ خوش خرامی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که ترا گرچه بنده بسیارند</p></div>
<div class="m2"><p>بنده ی خاص جز امامی نیست</p></div></div>