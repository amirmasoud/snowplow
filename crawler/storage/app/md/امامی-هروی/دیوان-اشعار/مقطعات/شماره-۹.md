---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>پناه تیغ و قلم آفتاب مشرق ملک</p></div>
<div class="m2"><p>توئی که نور تو خورشید را بپوشاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توئی که حرف مدیر تو هر سحر، گیتی</p></div>
<div class="m2"><p>برای رفع حوادث بر آسمان خواند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنار بحر زند موج ز آب دیده ی ابر</p></div>
<div class="m2"><p>بگاه آنکه بیان تو شکر افشاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار بار بهر دم زمانه دریا را</p></div>
<div class="m2"><p>ز شرم بخشش دست تو آب گرداند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان پناها مدح تو و معانی من</p></div>
<div class="m2"><p>بدولت ابد و عمر جاودان ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو آسمان جلالی روا بود که مرا</p></div>
<div class="m2"><p>عنایت تو ز دام زمانه برهاند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو در عریء شکوهت سپهر سیر شود</p></div>
<div class="m2"><p>زمین حضرت تو بیدقی فرو راند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که بی مراد مرا دائم از چه رو داد</p></div>
<div class="m2"><p>که بیگناه مرا هر دم از چه رنجاند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز حضرتیم چرا چند گاه نگذارد</p></div>
<div class="m2"><p>بگوشه ایم چرا چند روز ننشاند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرا ز چرخ شکایت کنم که گرد جهان</p></div>
<div class="m2"><p>بسان دایه ام گرد نقطه گرداند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شود ملازم حضرت دلم به بنیادی</p></div>
<div class="m2"><p>نظام ملک مهان گر سری بجنباند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همیشه تا بقیاس خرد درست شود</p></div>
<div class="m2"><p>که چرخ داده ی خویش از زمانه بستاند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زمین قدر ترا بر سپهر دستی باد</p></div>
<div class="m2"><p>که در مراتب تو آسمان فرو ماند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مباد خاطر مداحت از مدیح تو خرد</p></div>
<div class="m2"><p>که گنده پیر جهان قدر او نمی داند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بنعمت تو که باد آنکه ز آستان تواش</p></div>
<div class="m2"><p>شکوه جاه تو گه گه چه کنم می راند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که پای جانش تا در رکاب جسم بود</p></div>
<div class="m2"><p>عنان همت ازین آستان نگرداند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو کامران و نکو نام در زمانه بمان</p></div>
<div class="m2"><p>که در جهان ز تو نام نکو همی ماند</p></div></div>