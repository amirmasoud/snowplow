---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>آن شنیدی که خار در گلزار</p></div>
<div class="m2"><p>گل خوشبوی را برادر خواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راست می گفت لیکن از پی خار</p></div>
<div class="m2"><p>هیچ دانا درخت گل ننشاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلکه دانا چو گلبنی پرورد</p></div>
<div class="m2"><p>چون بر اندام اولیا افشاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای امامی بگل نگردد خار</p></div>
<div class="m2"><p>چشم ادراکت از چه خیره بماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل بی خار کس نکشت و نرست</p></div>
<div class="m2"><p>هیچکس هیچ جا ندید و نخواند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوی گل، گل به لطف یار دهد</p></div>
<div class="m2"><p>بلبلی را که دل ز خار بماند</p></div></div>