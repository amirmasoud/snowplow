---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ای خداوندی که دائم پیر چرخ</p></div>
<div class="m2"><p>همچو دولت در هوای جاه تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک درگاهت، امامی، بنده وار</p></div>
<div class="m2"><p>چون فلک در سایه درگاه تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زنده گردانش که جانش در جهان</p></div>
<div class="m2"><p>در هوای حضرت دلخواه تست</p></div></div>