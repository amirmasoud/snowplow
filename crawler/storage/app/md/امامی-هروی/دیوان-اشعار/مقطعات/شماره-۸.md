---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>زهی دین پروری کاندر بنانت</p></div>
<div class="m2"><p>سپهر دولتست و اختر داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسیر خامه ی گیتی پناهت</p></div>
<div class="m2"><p>در انصاف تو بر خلق بگشاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بهر بندگی در پیش کلکت</p></div>
<div class="m2"><p>زبان بسته است چون نی سرو آزاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سحرگه بخل را در خواب دیدم</p></div>
<div class="m2"><p>که بر در گاه تو هر لحظه چون باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخاک اندر همی غلطید و می گفت:</p></div>
<div class="m2"><p>ز دست جود تو، فریاد، فریاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبیعت را خرد دی گفت باوی</p></div>
<div class="m2"><p>که خورشیدش بجز بر دیده ننهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روا باشد که بر دست وزارت</p></div>
<div class="m2"><p>ز تقصیر تو از درد آورد یاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی ترسی که دست انتقامش</p></div>
<div class="m2"><p>طبایع را بر آرد بیخ و بنیاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی دانی که گر خواهد جهان را</p></div>
<div class="m2"><p>بگیرد گوهر تکوین و ایجاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جوابش داد کای بر نه سپهرت</p></div>
<div class="m2"><p>تقدم همچو واحد را بر اعداد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگر با درد دل بردند ازین پیش</p></div>
<div class="m2"><p>ز جور دور گردون آدمی زاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو رای صاحب عادل جهان را</p></div>
<div class="m2"><p>بنور عدل روشن کرد و آباد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برون کردند درد از دل و از آن پس</p></div>
<div class="m2"><p>ستم بر درد می کردند و بیداد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پناه و ملجاء عالم چو او بود</p></div>
<div class="m2"><p>بیامد درد و اندر پایش افتاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سپهر آن درد برپیچید اکنون</p></div>
<div class="m2"><p>که صد چندان نصیب دشمنت باد</p></div></div>