---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>آسمان داد فخرالملک خورشید زمین</p></div>
<div class="m2"><p>ای جهان را عهد انصاف تو ایام شباب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خداوندی که نوک کلک ملک آرای تو</p></div>
<div class="m2"><p>هادی تیغ ممالک پرور مالک رقاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قهر و لفظش دستگیر آتش و آبند از آن</p></div>
<div class="m2"><p>کاندرین نار و نعیمست، اندر آن رنج و عذاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ز آب لطف و تاب قهر تو یاد آورد</p></div>
<div class="m2"><p>آب داند دهشت لطف تو از درّ خوشاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لفظ عذب تست در عرض جهان نظم و نثر</p></div>
<div class="m2"><p>نوک کلک تست بر اوج هوای جاه و آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاه معنی بحر اگر گوهر شود موج بحار</p></div>
<div class="m2"><p>روز احسان ابر اگر اختر بود فیض سحاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آسمان ملک و ملت را کند، پروین فروغ</p></div>
<div class="m2"><p>آفتاب دین و دولترا کند، مشکین نقاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرد خاک پای قدر و عکس نور رأی تست</p></div>
<div class="m2"><p>در جهان افتخار و بر سپهر فتح باب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آسمانی آسمان، در دور او تا مستقیم</p></div>
<div class="m2"><p>آفتابی، آفتاب از تاب او در اضطراب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دین پناها مر «امامی» را بهنگام سخن</p></div>
<div class="m2"><p>گرچه طبعی همچو آتش بود و نظمی همچو آب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شعله ی مدح تو تا در دامن طبعش گرفت</p></div>
<div class="m2"><p>سر بر آورد از گریبان ضمیرش، آفتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با چنین شعری روا باشد که روز خاطرش</p></div>
<div class="m2"><p>افتد از پیش شب ادبار هر دم در شتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مطلع خورشید، حربا خسته ی منقار بوم</p></div>
<div class="m2"><p>حضرت جمشید و طوطی بسته ی بند غراب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا طبیعت را قوامست و کواکب را مسیر</p></div>
<div class="m2"><p>تا جهان را اعتدالست و فلک را انقلاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طبع را حکم تو گردون باد گردون را مدار</p></div>
<div class="m2"><p>چرخ ملک تو کواکب باد و کوکب را مئاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دور آن پیوسته ملک دین و دنیا را قوام</p></div>
<div class="m2"><p>سیر این همواره آب و ملک و ملت را ذهاب</p></div></div>