---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>ایاز بهر تفاخر مخدرات سپهر</p></div>
<div class="m2"><p>همیشه در حرم حرمت تو کرده سجود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبب وجود بو دار نه چرخ وارون کار</p></div>
<div class="m2"><p>نیافریدی خود واجب الوجود، وجود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در تو مقصد گیتی است ز آنکه باز نگشت</p></div>
<div class="m2"><p>ز آستانه او هیچ خلق بی مقصود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمانه دست بیکبارگی ز بخل بشست</p></div>
<div class="m2"><p>چو دید پای سخای تو در خزائن جود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چنانچه بپرسی ز چرخ آیینه گون</p></div>
<div class="m2"><p>که زنگ حادثه ز آیینه رخت که زود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخاکپای تو کز نه فلک جواب آید</p></div>
<div class="m2"><p>که صدر مسند اقبال، فخر دین داود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهی ز جاه تو قاصر ضمیر هر مخلوق</p></div>
<div class="m2"><p>خهی ز جود تو خرم روان هر موجود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مقدمست سخای تو بر صدای سئوال</p></div>
<div class="m2"><p>منزهست کمال تو ز انتقام حسود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خاک درگه و تعظیم آستانه تو</p></div>
<div class="m2"><p>بنزد عالم و جاهل، بر خواص ور نود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که ملک و ملت و دیوان و صدر مسند را</p></div>
<div class="m2"><p>محل و رونق و تعظیم و قدر و جاه فزود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مدام تا نشود دور آسمان باطل</p></div>
<div class="m2"><p>مقیم تا نبود گردش زمین معهود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ترا سعود قرین باد و کردگار معین</p></div>
<div class="m2"><p>ترا زمانه رهی باد و عاقبت محمود</p></div></div>