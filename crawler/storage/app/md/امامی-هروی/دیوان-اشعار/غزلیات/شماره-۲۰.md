---
title: >-
    شمارهٔ  ۲۰
---
# شمارهٔ  ۲۰

<div class="b" id="bn1"><div class="m1"><p>باز بی‌دین و دلم کرد شبیّ و سحری</p></div>
<div class="m2"><p>از خم زلف فروغ رخ زرّین‌کمری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌محابا به سر زلف بلائی، ستمی</p></div>
<div class="m2"><p>بی‌تکلف به لب لعل، قضائی، قدری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شعله‌ای، برقی، تابی، شرری، تیغ‌زنی</p></div>
<div class="m2"><p>آتشی، آبی، روحی، خردی، تاجوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرتوی، نوری، حوری، قمری، خورشیدی</p></div>
<div class="m2"><p>شاهدی، شوخی، شنگی، شغبی، خوش سیری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساحری، تندی، جادوفکنی، خیره‌کشی</p></div>
<div class="m2"><p>فتنه‌ای، مستی، لشکرشکنی، عشوه‌گری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاصل صیت امامی ز حصول غم توست</p></div>
<div class="m2"><p>بی‌دلی، شیفته‌ای، سوخته‌ای، بی‌سپری</p></div></div>