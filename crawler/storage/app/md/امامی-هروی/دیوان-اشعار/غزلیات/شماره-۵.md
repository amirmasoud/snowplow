---
title: >-
    شمارهٔ  ۵
---
# شمارهٔ  ۵

<div class="b" id="bn1"><div class="m1"><p>ترک من دل بردن آئین می کند</p></div>
<div class="m2"><p>بر من بیدل ستم زین می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد ماه از مشک خرمن می زند</p></div>
<div class="m2"><p>مشک را بر ماه پرچین می کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم مستش پرده ی جان می درد</p></div>
<div class="m2"><p>کفر زلفش غارت دین می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عکس یاقوتش مذاق روح را</p></div>
<div class="m2"><p>چون بشکر خنده شیرین می کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاری خط معنبر می دهد</p></div>
<div class="m2"><p>پشتی مرغ جهان بین می کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا لب جان پاش مرجان پوش او</p></div>
<div class="m2"><p>همنشین خط مشکین می کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کفرش از اسلام پیدا می شود</p></div>
<div class="m2"><p>معجز اندر سحر تضمین می کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از نسیم لطف او هر دم دلش</p></div>
<div class="m2"><p>مغز جان را عنبر آگین می کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مهر خسارش امامی را ز چرخ</p></div>
<div class="m2"><p>گرچه دامن پر ز پروین می کند</p></div></div>