---
title: >-
    شمارهٔ  ۱۲
---
# شمارهٔ  ۱۲

<div class="b" id="bn1"><div class="m1"><p>در محیطی فکنده ام زورق</p></div>
<div class="m2"><p>که دو عالم از اوست مستغرق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نتوان زورق از محیط شناخت</p></div>
<div class="m2"><p>نه وجود محیط از زورق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب شد زورق وز سیر آسود</p></div>
<div class="m2"><p>اینت معنی مشکل و مغلق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هستیت را جز این نشانه نبود</p></div>
<div class="m2"><p>که شود غرق نیستی مطلق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کفر و اسلام و سنت و بدعت</p></div>
<div class="m2"><p>اصطلاحیست در میان فرق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نور خورشید بر سپهر یکیست</p></div>
<div class="m2"><p>شد تفاوت میان صبح و شفق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بعنایت ببین که اصل وجود</p></div>
<div class="m2"><p>نشود مختلف بهیچ نسق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حق پرستی و ما و من گوئی؟</p></div>
<div class="m2"><p>راه گم کرده ای، زهی احمق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما و حق لفظ احمق است به هم</p></div>
<div class="m2"><p>چو ز ما بگذری چه ماند، حق</p></div></div>