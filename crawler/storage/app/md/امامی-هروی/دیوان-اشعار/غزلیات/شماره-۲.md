---
title: >-
    شمارهٔ  ۲
---
# شمارهٔ  ۲

<div class="b" id="bn1"><div class="m1"><p>شبت ز بهر چه بر روز سایبان انداخت</p></div>
<div class="m2"><p>که روز من به شب تیره در گمان انداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که داد جز رخ و زلفت نشان روز و شبی</p></div>
<div class="m2"><p>که آن براین شکن و این گره بر آن انداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو زلف پر گرهت گر نگه کنی دو شبند</p></div>
<div class="m2"><p>که روز روی تو خود را در آن میان انداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شگفت مانده ام از چشم جادوی تو که چون</p></div>
<div class="m2"><p>به نیم روز گره در شبی چنان انداخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسا که روز بشب کردم از غمت که رخت</p></div>
<div class="m2"><p>نگفت سایه بر آن ناتوان، توان انداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بروز من منشیناد چشمت ارچه دلم</p></div>
<div class="m2"><p>ببرد و در شکن زلف دلستان انداخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر از جان امامی فدای آن شب و روز</p></div>
<div class="m2"><p>که وصف هر دواش آتش در استخوان انداخت</p></div></div>