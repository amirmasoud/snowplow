---
title: >-
    رباعی شمارهٔ ۹
---
# رباعی شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>زلفش بکشی شب دراز اندازد</p></div>
<div class="m2"><p>ور بگشایی چنگل باز اندازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور پیچ و خمش ز یک دگر بگشایند</p></div>
<div class="m2"><p>دامن دامن مشک طراز اندازد</p></div></div>