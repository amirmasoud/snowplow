---
title: >-
    رباعی شمارهٔ ۱۹
---
# رباعی شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>واجب نبود به کس بر، افضال و کرم</p></div>
<div class="m2"><p>واجب باشد هر آینه شکر نعم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تقصیر نکرد خواجه در ناواجب</p></div>
<div class="m2"><p>من در واجب چگونه تقصیر کنم؟</p></div></div>