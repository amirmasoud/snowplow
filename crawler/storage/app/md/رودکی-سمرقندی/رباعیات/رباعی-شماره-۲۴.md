---
title: >-
    رباعی شمارهٔ ۲۴
---
# رباعی شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>دیدار به دل فروخت، نفروخت گران</p></div>
<div class="m2"><p>بوسه به روان فروشد و هست ارزان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آری، که چو آن ماه بود بازرگان</p></div>
<div class="m2"><p>دیدار به دل فروشد و بوسه به جان</p></div></div>