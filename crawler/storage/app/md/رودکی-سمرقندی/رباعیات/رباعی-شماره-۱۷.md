---
title: >-
    رباعی شمارهٔ ۱۷
---
# رباعی شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>در جستن آن نگار پر کینه و جنگ</p></div>
<div class="m2"><p>گشتیم سراپای جهان با دل تنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد دست ز کار و رفت پا از رفتار</p></div>
<div class="m2"><p>آن، بس که به سر زدیم و این، بس که به سنگ</p></div></div>