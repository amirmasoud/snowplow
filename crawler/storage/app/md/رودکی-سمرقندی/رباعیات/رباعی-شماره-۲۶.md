---
title: >-
    رباعی شمارهٔ ۲۶
---
# رباعی شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>ای از گل سرخ رنگ بربوده و بو</p></div>
<div class="m2"><p>رنگ از پی رخ ربوده، بو از پی مو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل رنگ شود، چو روی شویی، همه جو</p></div>
<div class="m2"><p>مشکین گردد، چو مو فشانی، همه کو</p></div></div>