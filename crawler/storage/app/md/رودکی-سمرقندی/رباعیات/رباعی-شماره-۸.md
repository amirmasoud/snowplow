---
title: >-
    رباعی شمارهٔ ۸
---
# رباعی شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>بی روی تو خورشید جهان‌سوز مباد</p></div>
<div class="m2"><p>هم بی تو چراغ عالم افروز مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با وصل تو کس چو من بد آموز مباد</p></div>
<div class="m2"><p>روزی که تو را نبینم آن روز مباد</p></div></div>