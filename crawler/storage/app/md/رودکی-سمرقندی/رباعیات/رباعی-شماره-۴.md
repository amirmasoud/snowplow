---
title: >-
    رباعی شمارهٔ ۴
---
# رباعی شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>دل خسته و بستهٔ مسلسل موییست</p></div>
<div class="m2"><p>خون گشته و کشتهٔ بت هندوییست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودی ندهد نصیحتت، ای واعظ</p></div>
<div class="m2"><p>ای خانه خراب طرفه یک پهلوییست</p></div></div>