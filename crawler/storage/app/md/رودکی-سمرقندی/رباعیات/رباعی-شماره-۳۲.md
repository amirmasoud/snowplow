---
title: >-
    رباعی شمارهٔ ۳۲
---
# رباعی شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>ای طرفهٔ خوبان من، ای شهرهٔ ری</p></div>
<div class="m2"><p>لب را به سپید رگ بکن پاک از می</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>...</p></div>
<div class="m2"><p>...</p></div></div>