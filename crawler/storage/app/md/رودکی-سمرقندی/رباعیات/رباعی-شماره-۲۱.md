---
title: >-
    رباعی شمارهٔ ۲۱
---
# رباعی شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>در پیش خود آن نامه چو بلکامه نهم</p></div>
<div class="m2"><p>پروین ز سرشک دیده بر جامه نهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر پاسخ تو چو دست بر خامه نهم</p></div>
<div class="m2"><p>خواهم که: دل اندر شکن نامه نهم</p></div></div>