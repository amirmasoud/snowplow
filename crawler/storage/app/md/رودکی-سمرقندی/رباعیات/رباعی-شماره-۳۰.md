---
title: >-
    رباعی شمارهٔ ۳۰
---
# رباعی شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>زلفت دیدم، سر از چمان پیچیده</p></div>
<div class="m2"><p>وندر گل سرخ ارغوان پیچیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر بندی هزار دل در بندش</p></div>
<div class="m2"><p>در هر پیچی هزار جان پیچیده</p></div></div>