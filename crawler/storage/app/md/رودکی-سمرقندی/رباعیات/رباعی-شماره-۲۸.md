---
title: >-
    رباعی شمارهٔ ۲۸
---
# رباعی شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>چرخ کجه باز، تا نهان ساخت کجه</p></div>
<div class="m2"><p>با نیک و بد دایره درباخت کجه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هنگامهٔ شب گذشت و شد قصه تمام</p></div>
<div class="m2"><p>طالع به کفم یکی نینداخت کجه</p></div></div>