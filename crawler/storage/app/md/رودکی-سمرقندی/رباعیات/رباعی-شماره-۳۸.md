---
title: >-
    رباعی شمارهٔ ۳۸
---
# رباعی شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>نارفته به شاهراه وصلت گامی</p></div>
<div class="m2"><p>نایافته از حسن جمالت کامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناگاه شنیدم ز فلک پیغامی:</p></div>
<div class="m2"><p>کز خم فراق نوش بادت جامی!</p></div></div>