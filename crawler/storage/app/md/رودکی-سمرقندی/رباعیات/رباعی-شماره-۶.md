---
title: >-
    رباعی شمارهٔ ۶
---
# رباعی شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>چشمم ز غمت، به هر عقیقی که بسفت</p></div>
<div class="m2"><p>بر چهره هزار گل ز رازم بشکفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رازی، که دلم ز جان همی داشت نهفت</p></div>
<div class="m2"><p>اشکم به زبان حال با خلق بگفت</p></div></div>