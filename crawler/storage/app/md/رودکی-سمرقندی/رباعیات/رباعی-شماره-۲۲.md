---
title: >-
    رباعی شمارهٔ ۲۲
---
# رباعی شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>در منزل غم فگنده مفرش ماییم</p></div>
<div class="m2"><p>وز آب دو چشم دل پر آتش ماییم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم چو ستم کند ستمکش ماییم</p></div>
<div class="m2"><p>دست خوش روزگار ناخوش ماییم</p></div></div>