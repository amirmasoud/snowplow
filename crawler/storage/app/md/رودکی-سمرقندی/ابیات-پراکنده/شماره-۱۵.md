---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>همی بایدت رفت و راه دورست</p></div>
<div class="m2"><p>به سغده دار یکسر شغل راها</p></div></div>