---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>ماه تمامست روی دلبرک من</p></div>
<div class="m2"><p>وز دو گل سرخ اندر و پر گاله</p></div></div>