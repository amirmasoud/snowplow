---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>فاخته گون شد هوا ز گردش خورشید</p></div>
<div class="m2"><p>جامهٔ خانه بتیک فاخته گون شد</p></div></div>