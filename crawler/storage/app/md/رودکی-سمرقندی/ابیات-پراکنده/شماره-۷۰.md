---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>چون سپرم نه میان بزم به نوروز</p></div>
<div class="m2"><p>در مه بهمن بتاز و جان عدو سوز</p></div></div>