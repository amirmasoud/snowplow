---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>خاک کف پای رودکی نسزی تو</p></div>
<div class="m2"><p>هم بشوی گاو و هم بخایی برغست</p></div></div>