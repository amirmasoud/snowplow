---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>چو هامون دشمنانت پست بادند</p></div>
<div class="m2"><p>چو گردون دوستان والا همه سال</p></div></div>