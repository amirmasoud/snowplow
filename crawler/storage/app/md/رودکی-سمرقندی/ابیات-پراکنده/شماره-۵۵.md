---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>ماغ در آبگیر گشته روان</p></div>
<div class="m2"><p>راست چون کشتییست قیراندود</p></div></div>