---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>چه گر من همیشه ستا گوی باشم</p></div>
<div class="m2"><p>ستایم نباشد نکو جز به نامت</p></div></div>