---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>سرو بودیم چندگاه بلند</p></div>
<div class="m2"><p>کوژ گشتیم و چون درونه شدیم</p></div></div>