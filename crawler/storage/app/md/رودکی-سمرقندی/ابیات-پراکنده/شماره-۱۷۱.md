---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>شدم پیر بدین سان و تو هم خود نه جوانی</p></div>
<div class="m2"><p>مرا سینه پر انجوخ و تو چون چفته کمانی</p></div></div>