---
title: >-
    شمارهٔ ۱۳۳
---
# شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>نیست از من عجب که: گستاخم</p></div>
<div class="m2"><p>که تو کردی باولم دسته</p></div></div>