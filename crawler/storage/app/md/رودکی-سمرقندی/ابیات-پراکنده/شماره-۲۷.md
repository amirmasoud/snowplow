---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>شب قدر وصلت ز فرخندگی</p></div>
<div class="m2"><p>فرح بخش‌تر از فرسنافه است</p></div></div>