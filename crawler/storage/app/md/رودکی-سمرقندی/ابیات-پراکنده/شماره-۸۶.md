---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>بس عزیزم، بس گرامی، شاد باش</p></div>
<div class="m2"><p>اندرین خانه بسان نو بیوک</p></div></div>