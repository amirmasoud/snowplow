---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>بر هبک نهاده جام باده</p></div>
<div class="m2"><p>وان گاه ز هبک نوش کردش</p></div></div>