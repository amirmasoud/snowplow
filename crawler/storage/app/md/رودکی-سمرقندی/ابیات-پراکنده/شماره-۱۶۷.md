---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>جهانا، همانا کزین بی‌گناهی</p></div>
<div class="m2"><p>گنه کار ماییم و تو بی کنازی</p></div></div>