---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>چنان بار برآورد به خویشتن</p></div>
<div class="m2"><p>که من گویم: خوردست سوسمار</p></div></div>