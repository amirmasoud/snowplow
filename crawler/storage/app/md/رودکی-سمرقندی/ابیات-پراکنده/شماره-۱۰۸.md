---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>گر کس بودی که زی توام بفگندی</p></div>
<div class="m2"><p>خویشتن اندر نهادمی به فلاخن</p></div></div>