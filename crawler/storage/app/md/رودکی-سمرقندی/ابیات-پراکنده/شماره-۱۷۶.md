---
title: >-
    شمارهٔ ۱۷۶
---
# شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>جز برتری ندانی، گویی که آتشی</p></div>
<div class="m2"><p>جز راستی نجویی، ماناتر از وی</p></div></div>