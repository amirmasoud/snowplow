---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>همه نیوشهٔ خواجه به نیکویی و به صلحست</p></div>
<div class="m2"><p>همه نیوشهٔ نادان به جنگ و فتنه و غوغاست</p></div></div>