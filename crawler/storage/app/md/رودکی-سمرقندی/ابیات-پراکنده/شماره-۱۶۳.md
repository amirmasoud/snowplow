---
title: >-
    شمارهٔ ۱۶۳
---
# شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>نیل دمنده تویی به گاه عطیت</p></div>
<div class="m2"><p>پیل دمنده به گاه کینه گزاری</p></div></div>