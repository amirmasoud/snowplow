---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>ازان کوز ابری باز کردار</p></div>
<div class="m2"><p>کلفتش بسدین و تنش زرین</p></div></div>