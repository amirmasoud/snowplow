---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>بت پرستی گرفته ایم همه</p></div>
<div class="m2"><p>این جهان چون بتست و ما شمنیم</p></div></div>