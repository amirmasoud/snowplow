---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>شیر آلغده که بیرون جهد از خانه به صید</p></div>
<div class="m2"><p>تا به چنگ آرد آهو وآهو بره را</p></div></div>