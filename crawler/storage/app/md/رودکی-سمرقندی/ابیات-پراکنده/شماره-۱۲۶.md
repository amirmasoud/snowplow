---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>چنان که خاک سر شتی به زیر خاک شوی</p></div>
<div class="m2"><p>نیات خاک و تو اندر میان خاک آگین</p></div></div>