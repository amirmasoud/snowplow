---
title: >-
    شمارهٔ ۱۵۳
---
# شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>ایا خورشید سالاران گیتی</p></div>
<div class="m2"><p>سوار رزم ساز و گرد نستوه</p></div></div>