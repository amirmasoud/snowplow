---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>تازیان دوان همی آید</p></div>
<div class="m2"><p>همچو اندر فسیله اسب نهاز</p></div></div>