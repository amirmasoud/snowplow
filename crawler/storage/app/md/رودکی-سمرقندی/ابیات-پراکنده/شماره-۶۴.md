---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>فاخته بر سرو شاهرود بر آورد</p></div>
<div class="m2"><p>زخمه فرو هشت زندواف به طنبور</p></div></div>