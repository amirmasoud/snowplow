---
title: >-
    شمارهٔ ۱۵۵
---
# شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>مهر جویی ز من و بی مهری</p></div>
<div class="m2"><p>هده خواهی ز من و بیهده‌ای</p></div></div>