---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>تلخی و شیرینیش آمیخته است</p></div>
<div class="m2"><p>کس نخورد نوش و شکر با پیون</p></div></div>