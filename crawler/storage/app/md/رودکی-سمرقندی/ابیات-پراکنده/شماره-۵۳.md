---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>بدان مرغک مانم که همی دوش</p></div>
<div class="m2"><p>بزار از بر شاخک همی فنود</p></div></div>