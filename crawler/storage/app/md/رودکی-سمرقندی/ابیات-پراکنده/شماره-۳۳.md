---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>معذورم دارند، که اندوه و غیشت</p></div>
<div class="m2"><p>و اندوه و غیش من ازان جعد و غیشت</p></div></div>