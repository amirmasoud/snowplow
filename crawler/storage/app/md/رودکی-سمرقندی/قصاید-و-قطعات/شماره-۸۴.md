---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>چون گسی کردمت بدستک خویش</p></div>
<div class="m2"><p>گنه خویش بر تو افگندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خانه از روی تو تهی کردم</p></div>
<div class="m2"><p>دیده از خون دل بیاگندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب آید مرا ز کردهٔ خویش</p></div>
<div class="m2"><p>کز در گریه‌ام، همی خندم</p></div></div>