---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>جهانا! چه بینی تو از بچگان</p></div>
<div class="m2"><p>که گه مادری، گاه مادندرا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه پاذیر باید تو را نه ستون</p></div>
<div class="m2"><p>نه دیوار خشت و نه زآهن درا</p></div></div>