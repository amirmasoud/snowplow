---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>آن چیست بر آن طبق همی تابد؟</p></div>
<div class="m2"><p>چون ملحم زیر شعر عنابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقش به مثل چو ساعد حورا</p></div>
<div class="m2"><p>پایش به مثل چو پای مرغابی</p></div></div>