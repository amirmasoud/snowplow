---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>چون سپرم نه میان بزم به نوروز</p></div>
<div class="m2"><p>درمه بهمن بتاز و جان عدو سوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز تو بی رنج باش وجان تو خرم</p></div>
<div class="m2"><p>بانی و با رود و با نبیذ فنا روز</p></div></div>