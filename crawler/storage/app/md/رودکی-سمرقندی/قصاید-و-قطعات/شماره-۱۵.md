---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>به سرای سپنج مهمان را</p></div>
<div class="m2"><p>دل نهادن همیشگی نه رواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیر خاک اندرونت باید خفت</p></div>
<div class="m2"><p>گر چه اکنونت خواب بر دیباست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با کسان بودنت چه سود کند؟</p></div>
<div class="m2"><p>که به گور اندرون شدن تنهاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار تو زیر خاک مور و مگس</p></div>
<div class="m2"><p>چشم بگشا، ببین: کنون پیداست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن که زلفین و گیسویت پیراست</p></div>
<div class="m2"><p>گر چه دینار یا درمش بهاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ترا دید زردگونه شده</p></div>
<div class="m2"><p>سرد گردد دلش، نه نابیناست</p></div></div>