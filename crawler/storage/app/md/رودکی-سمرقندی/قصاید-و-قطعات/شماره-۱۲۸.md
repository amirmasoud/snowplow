---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>دل تنگ مدار، ای ملک، از کار خدایی</p></div>
<div class="m2"><p>آرام و طرب رامده از طبع جدایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد بار فتادست چنین هر ملکی را</p></div>
<div class="m2"><p>آخر برسیدند به هر کام روایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن کس که ترا دید و ترا بیند در جنگ</p></div>
<div class="m2"><p>داند که: تو با شیر به شمشیر درآیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این کار سمایی بد، نه قوت انسان</p></div>
<div class="m2"><p>کس را نبود قوت به کار سمایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنان که گرفتار شدند از سپه تو</p></div>
<div class="m2"><p>از بند به شمشیر تو یابند رهایی</p></div></div>