---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>مرا تو راحت جانی، معاینه، نه خبر</p></div>
<div class="m2"><p>کرا معاینه آید خبر چه سود کند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپر به پیش کشیدم خدنگ قهر ترا</p></div>
<div class="m2"><p>چو تیر بر جگر آید سپر چه سود کند؟</p></div></div>