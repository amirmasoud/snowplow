---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>هست بر خواجه پیچیده رفتن</p></div>
<div class="m2"><p>راست چون بر درخت پیچد سن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این عجبتر که: می نداند او</p></div>
<div class="m2"><p>شعر از شعر و خنب را از خن</p></div></div>