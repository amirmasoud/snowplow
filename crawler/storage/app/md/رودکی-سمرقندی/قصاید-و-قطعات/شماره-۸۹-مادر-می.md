---
title: >-
    شمارهٔ ۸۹ - مادر می
---
# شمارهٔ ۸۹ - مادر می

<div class="b" id="bn1"><div class="m1"><p>مادر می را بکرد باید قربان</p></div>
<div class="m2"><p>بچهٔ او را گرفت و کرد به زندان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بچهٔ او را ازو گرفت ندانی</p></div>
<div class="m2"><p>تاش نکوبی نخست و زو نکشی جان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز که نباشد حلال دور بکردن</p></div>
<div class="m2"><p>بچهٔ کوچک ز شیر مادر و پستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نخورد شیر هفت مه به تمامی</p></div>
<div class="m2"><p>از سر اردیبهشت تا بن آبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن گه شاید ز روی دین و ره داد</p></div>
<div class="m2"><p>بچه به زندان تنگ و مادر قربان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بسپاری به حبس بچهٔ او را</p></div>
<div class="m2"><p>هفت شباروز خیره ماند و حیران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز چو آید به هوش و حال ببیند</p></div>
<div class="m2"><p>جوش بر آرد، بنالد از دل سوزان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه زبر زیر گردد از غم و گه باز</p></div>
<div class="m2"><p>زیر زبر، همچنان ز انده جوشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زر بر آتش کجا بخواهی پالود</p></div>
<div class="m2"><p>جوشد، لیکن ز غم نجوشد چندان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باز به کردار اشتری که بود مست</p></div>
<div class="m2"><p>کفک بر آرد ز خشم و راند سلطان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرد حرس کفک‌هاش پاک بگیرد</p></div>
<div class="m2"><p>تا بشود تیرگیش و گردد رخشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آخر کارام گیرد و نچخد تیز</p></div>
<div class="m2"><p>درش کند استوار مرد نگهبان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون بنشیند تمام و صافی گردد</p></div>
<div class="m2"><p>گونهٔ یاقوت سرخ گیرد و مرجان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چند ازو سرخ چون عقیق یمانی</p></div>
<div class="m2"><p>چند ازو لعل چون نگین بدخشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ورش ببویی، گمان بری که گل سرخ</p></div>
<div class="m2"><p>بوی بدو داد و مشک و عنبر با بان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هم به خم اندر همی گدازد چونین</p></div>
<div class="m2"><p>تا به گه نوبهار و نیمهٔ نیسان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن گه اگر نیم شب درش بگشایی</p></div>
<div class="m2"><p>چشمهٔ خورشید را ببینی تابان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ور به بلور اندرون ببینی گویی:</p></div>
<div class="m2"><p>گوهر سرخست به کف موسی عمران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زفت شود رادمرد و سست دلاور</p></div>
<div class="m2"><p>گر بچشد زوی و روی زرد گلستان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وانک به شادی یکی قدح بخورد زوی</p></div>
<div class="m2"><p>رنج نبیند از آن فراز و نه احزان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>انده ده ساله را به طنجه براند</p></div>
<div class="m2"><p>شادی نو را ز ری بیارد و عمان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>با می چونین که سالخورده بود چند</p></div>
<div class="m2"><p>جامه بکرده فراز پنجه خلقان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مجلس باید بساخته، ملکانه</p></div>
<div class="m2"><p>از گل و از یاسمین و خیری الوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نعمت فردوس گستریده ز هر سو</p></div>
<div class="m2"><p>ساخته کاری که کس نسازد چونان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جامهٔ زرین و فرش‌های نو آیین</p></div>
<div class="m2"><p>شهره ریاحین و تخت‌های فراوان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بربط عیسی و لون‌های فؤادی</p></div>
<div class="m2"><p>چنگ مدک نیر و نای چابک جانان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یک صف میران و بلعمی بنشسته</p></div>
<div class="m2"><p>یک صف حران و پیر صالح دهقان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خسرو بر تخت پیشگاه نشسته</p></div>
<div class="m2"><p>شاه ملوک جهان، امیر خراسان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ترک هزاران به پای پیش صف اندر</p></div>
<div class="m2"><p>هر یک چون ماه بر دو هفته درفشان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر یک بر سر بساک مورد نهاده</p></div>
<div class="m2"><p>روش می سرخ و زلف و جعدش ریحان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>باده دهنده بتی بدیع ز خوبان</p></div>
<div class="m2"><p>بچهٔ خاتون ترک و بچهٔ خاقان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چونش بگردد نبیذ چند به شادی</p></div>
<div class="m2"><p>شاه جهان شادمان و خرم و خندان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از کف ترکی سیاه چشم پریروی</p></div>
<div class="m2"><p>قامت چون سرو و زلفکانش چوگان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زان می خوشبوی ساغری بستاند</p></div>
<div class="m2"><p>یاد کند روی شهریار سجستان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خود بخورد نوش و اولیاش همیدون</p></div>
<div class="m2"><p>گوید هر یک چو می بگیرد شادان:</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شادی بو جعفر احمد بن محمد</p></div>
<div class="m2"><p>آن مه آزادگان و مفخر ایران</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آن ملک عدل و آفتاب زمانه</p></div>
<div class="m2"><p>زنده بدو داد و روشنایی گیهان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آنکه نبود از نژاد آدم چون او</p></div>
<div class="m2"><p>نیز نباشد، اگر نگویی بهتان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>حجت یکتا خدای و سایهٔ اوی است</p></div>
<div class="m2"><p>طاعت او کرده واجب آیت فرقان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خلق ز خاک و ز آب و آتش و بادند</p></div>
<div class="m2"><p>وین ملک از آفتاب گوهر ساسان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فر بدو یافت ملک تیره و تاری</p></div>
<div class="m2"><p>عدن بدو گشت نیز گیتی ویران</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر تو فصیحی همه مناقب او گوی</p></div>
<div class="m2"><p>ور تو دبیری همه مدایح او خوان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ور تو حکیمی و راه حکمت جویی</p></div>
<div class="m2"><p>سیرت او گیر و خوب مذهب او دان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آن که بدو بنگری به حکمت گویی:</p></div>
<div class="m2"><p>اینک سقراط و هم فلاطن یونان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ور تو فقیهی و سوی شرع گرایی</p></div>
<div class="m2"><p>شافعی اینکت و بوحنیفه و سفیان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر بگشاید زبان به علم و به حکمت</p></div>
<div class="m2"><p>گوش کن اینک به علم و حکمت لقمان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مرد ادب را خرد فزاید و حکمت</p></div>
<div class="m2"><p>مرد خرد را ادب فزاید و ایمان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ور تو بخواهی فرشته‌ای که ببینی</p></div>
<div class="m2"><p>اینک اوی است آشکارا رضوان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خوب نگه کن بدان لطافت و آن روی</p></div>
<div class="m2"><p>تا تو ببینی بر این که گفتم برهان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پاکی اخلاق او و پاک نژادی</p></div>
<div class="m2"><p>با نیت نیک و با مکارم احسان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ور سخن او رسد به گوش تو یک راه</p></div>
<div class="m2"><p>سعد شود مر تو را نحوست کیوان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ورش به صدر اندرون نشسته ببینی</p></div>
<div class="m2"><p>جزم بگویی که: زنده گشت سلیمان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سام سواری، که تا ستاره بتابد</p></div>
<div class="m2"><p>اسب نبیند چنو سوار به میدان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>باز به روز نبرد و کین و حمیت</p></div>
<div class="m2"><p>گرش ببینی میان مغفر و خفتان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خوار نمایدت ژنده پیل بدانگاه</p></div>
<div class="m2"><p>ورچه بود مست و تیز گشته و غران</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ورش بدیدی سفندیار گه رزم</p></div>
<div class="m2"><p>پیش سنانش جهان دویدی و لرزان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گرچه به هنگام حلم کوه تن اوی</p></div>
<div class="m2"><p>کوه سیام است که کس نبیند جنبان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>دشمن اگر اژدهاست، پیش سنانش</p></div>
<div class="m2"><p>گردد چون موم پیش آتش سوزان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ور به نبرد آیدش ستارهٔ بهرام</p></div>
<div class="m2"><p>توشهٔ شمشیر او شود به گروگان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>باز بدان گه که می به دست بگیرد</p></div>
<div class="m2"><p>ابر بهاری چنو نبارد باران</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ابر بهاری جز آب تیره نبارد</p></div>
<div class="m2"><p>او همه دیبا به تخت و زر به انبان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>با دو کف او، ز بس عطا که ببخشد</p></div>
<div class="m2"><p>خوار نماید حدیث و قصهٔ توفان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>لاجرم از جود و از سخاوت اوی است</p></div>
<div class="m2"><p>نرخ گرفته حدیث و صامت ارزان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>شاعر زی او رود فقیر و تهیدست</p></div>
<div class="m2"><p>با زر بسیار بازگردد و حملان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>مرد سخن را ازو نواختن و بر</p></div>
<div class="m2"><p>مرد ادب را ازو وظیفهٔ دیوان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>باز به هنگام داد و عدل بر خلق</p></div>
<div class="m2"><p>نیست به گیتی چنو نبیل و مسلمان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>داد بیابد ضعیف همچو قوی زوی</p></div>
<div class="m2"><p>جور نبینی به نزد او و نه عدوان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نعمت او گستریده بر همه گیتی</p></div>
<div class="m2"><p>آنچه کس از نعمتش نبینی عریان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بستهٔ گیتی ازو بیابد راحت</p></div>
<div class="m2"><p>خستهٔ گیتی ازو بیابد درمان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>با رسن عفو آن مبارک خسرو</p></div>
<div class="m2"><p>حلقهٔ تنگ است هر چه دشت و بیابان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>پوزش بپذیرد و گناه ببخشد</p></div>
<div class="m2"><p>خشم نراند، به عفو کوشد و غفران</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>آن ملک نیمروز و خسرو پیروز</p></div>
<div class="m2"><p>دولت او یوز و دشمن آهوی نالان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>عمرو بن اللیث زنده گشت بدو باز</p></div>
<div class="m2"><p>با حشم خویش و آن زمانهٔ ایشان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>رستم را نام گر چه سخت بزرگ است</p></div>
<div class="m2"><p>زنده بدوی‌ست نام رستم دستان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>رودکیا، برنورد مدح همه خلق</p></div>
<div class="m2"><p>مدحت او گوی و مهر دولت بستان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ورچه بکوشی، به جهد خویش بگویی</p></div>
<div class="m2"><p>ورچه کنی تیز فهم خویش به سوهان،</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ورچه دو صد تابعه فریشته داری</p></div>
<div class="m2"><p>نیز پری باز و هر چه جنی و شیطان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>گفت ندانی سزاش و خیز و فراز آر</p></div>
<div class="m2"><p>آن که بگفتی چنان که باید نتوان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>اینک مدحی، چنانکه طاقت من بود</p></div>
<div class="m2"><p>لفظ همه خوب و هم به معنی آسان</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>جز به سزاوار میر گفت ندانم</p></div>
<div class="m2"><p>ور چه جریرم به شعر و طایی و حسان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>مدح امیری که مدح زوست جهان را</p></div>
<div class="m2"><p>زینت هم زوی و فر و نزهت و سامان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>سخت شکوهم که عجز من بنماید</p></div>
<div class="m2"><p>ورچه صریعم ابا فصاحت سحبان</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>برد چنین مدح و عرضه کرد زمانی</p></div>
<div class="m2"><p>ورچه بود چیره بر مدایح شاهان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>مدح همه خلق را کرانه پدید است</p></div>
<div class="m2"><p>مدحت او را کرانه نی و نه پایان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>نیست شگفتی که رودکی به چنین جای</p></div>
<div class="m2"><p>خیره شود بیروان و ماند حیران</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ورنه مرا بو عمر دلاور کردی</p></div>
<div class="m2"><p>وان گه دستوری گزیدهٔ عدنان</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>زهره کجا بودمی به مدح امیری؟</p></div>
<div class="m2"><p>کز پی او آفرید گیتی یزدان</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ورم ضعیفی و بی‌بدیم نبودی</p></div>
<div class="m2"><p>وآن که نبود از امیر مشرق فرمان</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>خود بدویدی بسان پیک مرتب</p></div>
<div class="m2"><p>خدمت او را گرفته چامه به دندان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>مدح رسول است، عذر من برساند</p></div>
<div class="m2"><p>تا بشناسد درست میر سخندان</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>عذر رهی خویش ناتوانی و پیری</p></div>
<div class="m2"><p>کو به تن خویش از این نیامد مهمان</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>دولت میرم همیشه باد بر افزون</p></div>
<div class="m2"><p>دولت اعدای او همیشه به نقصان</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>سرش رسیده به ماه بر، به بلندی</p></div>
<div class="m2"><p>و آن معادی به زیر ماهی پنهان</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>طلعت تابنده‌تر ز طلعت خورشید</p></div>
<div class="m2"><p>نعمت پاینده‌تر ز جودی و ثهلان</p></div></div>