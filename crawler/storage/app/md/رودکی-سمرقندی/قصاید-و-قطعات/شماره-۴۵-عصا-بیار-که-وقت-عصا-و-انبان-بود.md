---
title: >-
    شمارهٔ ۴۵ - عصا بیار که وقت عصا و انبان بود
---
# شمارهٔ ۴۵ - عصا بیار که وقت عصا و انبان بود

<div class="b" id="bn1"><div class="m1"><p>مرا بسود و فرو ریخت هر چه دندان بود</p></div>
<div class="m2"><p>نبود دندان، لا بل چراغ تابان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپید سیم زده بود و در و مرجان بود</p></div>
<div class="m2"><p>ستارهٔ سحری بود و قطره باران بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی نماند کنون زآن همه، بسود و بریخت</p></div>
<div class="m2"><p>چه نحس بود! همانا که نحس کیوان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه نحس کیوان بود و نه روزگار دراز</p></div>
<div class="m2"><p>چه بود؟ منت بگویم: قضای یزدان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان همیشه چنین است، گِرد گَردان است</p></div>
<div class="m2"><p>همیشه تا بوَد، آیین گِرد، گَردان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همان که درمان باشد، به جای درد شود</p></div>
<div class="m2"><p>و باز درد، همان کز نخست درمان بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کهن کند به زمانی همان کجا نو بود</p></div>
<div class="m2"><p>و نو کند به زمانی همان که خُلقان بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسا شکسته بیابان، که باغ خرم بود</p></div>
<div class="m2"><p>و باغ خرم گشت آن کجا بیابان بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی چه دانی؟ ای ماهروی مشکین موی</p></div>
<div class="m2"><p>که حال بنده از این پیش بر چه سامان بود؟!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به زلف چوگان نازِش همی کنی تو بدو</p></div>
<div class="m2"><p>ندیدی آن گه او را که زلف، چوگان بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد آن زمانه که رویش به سان دیبا بود</p></div>
<div class="m2"><p>شد آن زمانه که مویش به سان قطران بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان که خوبی، مهمان و دوست بود عزیز</p></div>
<div class="m2"><p>بشد که باز نیامد، عزیز مهمان بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسا نگار، که حیران بدی بدو در، چشم</p></div>
<div class="m2"><p>به روی او در، چشمم همیشه حیران بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شد آن زمانه، که او شاد بود و خرم بود</p></div>
<div class="m2"><p>نشاط او به فزون بود و غم به نقصان بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همی خرید و همی سخت، بیشمار درم</p></div>
<div class="m2"><p>به شهر، هر گه یک ترک نار پستان بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بسا کنیزک نیکو، که میل داشت بدو</p></div>
<div class="m2"><p>به شب ز یاری او نزد جمله پنهان بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به روز چون که نیارست شد به دیدن او</p></div>
<div class="m2"><p>نهیب خواجهٔ او بود و بیم زندان بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نبیذ روشن و دیدار خوب و روی لطیف</p></div>
<div class="m2"><p>اگر گران بد، زی من همیشه ارزان بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دلم خزانهٔ پر گنج بود و گنج سخن</p></div>
<div class="m2"><p>نشان نامهٔ ما مهر و شعر، عنوان بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همیشه شاد و ندانستمی که، غم چه بود؟</p></div>
<div class="m2"><p>دلم نشاط و طرب را فراخ میدان بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بسا دلا، که به سان حریر کرده به شعر</p></div>
<div class="m2"><p>از آن سپس که به کردار سنگ ‌و سندان بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همیشه چشمم زی زلفکان چابک بود</p></div>
<div class="m2"><p>همیشه گوشم زی مردم سخندان بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عیال نه، زن و فرزند نه، مئونت نه</p></div>
<div class="m2"><p>از این همه تنم آسوده بود و آسان بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو رودکی را -ای ماهرو!- کنون بینی</p></div>
<div class="m2"><p>بدان زمانه ندیدی که این چنینان بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدان زمانه ندیدی که در جهان رفتی</p></div>
<div class="m2"><p>سرود گویان، گویی هزاردستان بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شد آن زمانه که او انس رادمردان بود</p></div>
<div class="m2"><p>شد آن زمانه که او پیشکار میران بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همیشه شعر ورا زی ملوک دیوان است</p></div>
<div class="m2"><p>همیشه شعر ورا زی ملوک دیوان بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شد آن زمانه که شعرش همه جهان بنَوَشت</p></div>
<div class="m2"><p>شد آن زمانه که او شاعر خراسان بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کجا به گیتی بوده‌ست نامور دهقان</p></div>
<div class="m2"><p>مرا به خانهٔ او سیم بود و حُملان بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که را بزرگی و نعمت ز این و آن بودی</p></div>
<div class="m2"><p>مرا بزرگی و نعمت ز آل سامان بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بداد میر خراسانش چل هزار درم</p></div>
<div class="m2"><p>وزو فزونی یک پنجِ میرِ ماکان بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز اولیاش پراکنده نیز هشت هزار</p></div>
<div class="m2"><p>به من رسید بدان وقت، حالِ خوب آن بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو میر دید سخن، داد دادِ مردیِ خویش</p></div>
<div class="m2"><p>ز اولیاش چنان کز امیر فرمان بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کنون زمانه دگر گشت و من دگر گشتم</p></div>
<div class="m2"><p>عصا بیار، که وقت عصا و انبان بود</p></div></div>