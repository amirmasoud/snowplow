---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>دریغ! مدحت چون درو آبدار غزل</p></div>
<div class="m2"><p>که چابکیش نیاید همی به لفظ پدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اساس طبع ثنایست، بل قوی‌تر ازان</p></div>
<div class="m2"><p>ز آلت سخن آمد همی همه مانیذ</p></div></div>