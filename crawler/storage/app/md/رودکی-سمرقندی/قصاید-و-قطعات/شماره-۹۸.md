---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>ضیغمی نسل پذیرفته ز دیو</p></div>
<div class="m2"><p>آهویی نام نهاده یکران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتابی، که ز چابک قدمی</p></div>
<div class="m2"><p>بر سر ذره نماید جولان</p></div></div>