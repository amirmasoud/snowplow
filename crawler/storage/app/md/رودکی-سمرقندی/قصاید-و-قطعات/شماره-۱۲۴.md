---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>کسی را چو من دوستگان می چه باید؟</p></div>
<div class="m2"><p>که دل شاد دارد بهر دوستگانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه جز عیب چیزیست کان تو نداری</p></div>
<div class="m2"><p>نه جز غیب چیزیست کان تو ندانی</p></div></div>