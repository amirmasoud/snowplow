---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>دریغم آید خواندن گزاف وار دو نام</p></div>
<div class="m2"><p>بزرگوار دو نام از گزاف خواندن عام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی که خوبان را یکسره نکو خوانند</p></div>
<div class="m2"><p>دگر که: عاشق گویند عاشقان را نام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دریغم آید چون مر تو را نکو خوانند</p></div>
<div class="m2"><p>دریغم آید چون بر رهیت عاشق نام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا دلیست که از غمگنی چو دور شود</p></div>
<div class="m2"><p>به غمگنان شود و غم فراز گیرد وام</p></div></div>