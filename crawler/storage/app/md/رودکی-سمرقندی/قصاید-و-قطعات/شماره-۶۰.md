---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>به دور عدل تو در زیر چرخ مینایی</p></div>
<div class="m2"><p>چنان گریخت ز دهر دو رنگ، رنگ فتور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که باز شانه کند همچو باد سنبل را</p></div>
<div class="m2"><p>به نیش چنگل خون ریز تارک عصفور</p></div></div>