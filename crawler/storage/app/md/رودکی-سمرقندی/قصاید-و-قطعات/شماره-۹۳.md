---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>یخچه می‌بارید از ابر سیاه</p></div>
<div class="m2"><p>چون ستاره بر زمین از آسمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بگردد پای او از پای دار</p></div>
<div class="m2"><p>آشکوخیده بماند همچنان</p></div></div>