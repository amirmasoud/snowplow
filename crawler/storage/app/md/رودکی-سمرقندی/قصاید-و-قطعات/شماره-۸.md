---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>کس فرستاد به سر اندر عیار مرا</p></div>
<div class="m2"><p>که: مکن یاد به شعر اندر بسیار مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وین فژه پیر ز بهر تو مرا خوار گرفت</p></div>
<div class="m2"><p>برهاناد ازو ایزد جبار مرا</p></div></div>