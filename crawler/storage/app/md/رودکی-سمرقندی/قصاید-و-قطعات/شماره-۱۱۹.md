---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>ای غافل از شمار! چه پنداری؟</p></div>
<div class="m2"><p>کت خالق آفرید پی کاری؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری که مر توراست سرمایه</p></div>
<div class="m2"><p>وید است و کارهات به این زاری!</p></div></div>