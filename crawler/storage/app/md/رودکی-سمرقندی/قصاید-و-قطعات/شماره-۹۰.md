---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>هان! صائم نوالهٔ این سفله میزبان</p></div>
<div class="m2"><p>زین بی نمک ابا منه انگشت در دهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب تر مکن به آب، که طلقست در قدح</p></div>
<div class="m2"><p>دست از کباب دار، که زهرست توامان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با کام خشک و با جگر تفته درگذر</p></div>
<div class="m2"><p>ایدون که در سراسر این سبز گلستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کافور همچو گل چکد از دوش شاخسار</p></div>
<div class="m2"><p>زیبق چو آب بر جهد از ناف آبدان</p></div></div>