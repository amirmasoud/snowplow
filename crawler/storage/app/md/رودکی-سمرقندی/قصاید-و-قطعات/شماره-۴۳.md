---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>تا کی گویی که: اهل گیتی</p></div>
<div class="m2"><p>در هستی و نیستی لئیمند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون تو طمع از جهان بریدی</p></div>
<div class="m2"><p>دانی که همه جهان کریمند</p></div></div>