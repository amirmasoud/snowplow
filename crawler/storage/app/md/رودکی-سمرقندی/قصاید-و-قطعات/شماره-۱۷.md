---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>این جهان پاک خواب کردار است</p></div>
<div class="m2"><p>آن شناسد که دلش بیدار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیکی او به جایگاه بد است</p></div>
<div class="m2"><p>شادی او به جای تیمار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه نشینی بدین جهان هموار؟</p></div>
<div class="m2"><p>که همه کار او نه هموار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنش او نه خوب و چهرش خوب</p></div>
<div class="m2"><p>زشت کردار و خوب دیدار است</p></div></div>