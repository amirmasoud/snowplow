---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>امروز به هر حالی بغداد بخاراست</p></div>
<div class="m2"><p>کجا میر خراسانست، پیروزی آنجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی، تو بده باده ومطرب تو بزن رود</p></div>
<div class="m2"><p>تا می خورم امروز، که وقت طرب ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می هست ودرم هست و بت لاله رخان هست</p></div>
<div class="m2"><p>غم نیست وگر هست نصیب دل اعداست</p></div></div>