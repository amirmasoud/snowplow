---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>زه! دانا را گویند، که داند گفت</p></div>
<div class="m2"><p>هیچ نادان را داننده نگوید: زه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن شیرین از زفت نیارد بر</p></div>
<div class="m2"><p>بز به بج بج بر، هرگز نشود فربه</p></div></div>