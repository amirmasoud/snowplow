---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>همی بکشتی تا در عدو نماند شجاع</p></div>
<div class="m2"><p>همی بدادی تا در ولی نماند فقیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسا کسا که بره‌ست و فرخشه بر خوانش</p></div>
<div class="m2"><p>بسا کسا که جوین نان همی نیابد سیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مبادرت کن و خامش مباش چندینا</p></div>
<div class="m2"><p>اگرت بدره رساند همی به بدر منیر</p></div></div>