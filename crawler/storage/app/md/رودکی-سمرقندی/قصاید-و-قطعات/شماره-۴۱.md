---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>مهتران جهان همه مردند</p></div>
<div class="m2"><p>مرگ را سر همه فرو کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیر خاک اندرون شدند آنان</p></div>
<div class="m2"><p>که همه کوشک‌ها برآوردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هزاران هزار نعمت و ناز</p></div>
<div class="m2"><p>نه به آخر به جز کفن بردند؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود از نعمت آن چه پوشیدند</p></div>
<div class="m2"><p>و آن چه دادند و آن چه را خوردند</p></div></div>