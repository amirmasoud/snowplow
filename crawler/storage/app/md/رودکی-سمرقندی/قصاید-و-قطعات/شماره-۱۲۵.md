---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>آی دریغا! که خردمند را</p></div>
<div class="m2"><p>باشد فرزند و خردمند نی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور چه ادب دارد و دانش پدر</p></div>
<div class="m2"><p>حاصل میراث به فرزند نی</p></div></div>