---
title: >-
    بخش ۱۰۱
---
# بخش ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>من سخن گویم، تو کانایی کنی</p></div>
<div class="m2"><p>هر زمانی دست بر دستی زنی</p></div></div>