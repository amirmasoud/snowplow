---
title: >-
    بخش ۱۰۳
---
# بخش ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>شو، بدان گنج اندرون خمی بجوی</p></div>
<div class="m2"><p>زیر او سُمجی است، بیرون شد بدوی</p></div></div>