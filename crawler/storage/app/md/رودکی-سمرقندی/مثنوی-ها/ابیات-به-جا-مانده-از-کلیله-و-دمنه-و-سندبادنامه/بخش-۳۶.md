---
title: >-
    بخش ۳۶
---
# بخش ۳۶

<div class="b" id="bn1"><div class="m1"><p>چون که نالنده بدو گستاخ شد</p></div>
<div class="m2"><p>تن درستی آمد و در واخ شد</p></div></div>