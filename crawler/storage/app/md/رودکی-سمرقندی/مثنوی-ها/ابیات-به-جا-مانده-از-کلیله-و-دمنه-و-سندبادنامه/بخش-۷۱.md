---
title: >-
    بخش ۷۱
---
# بخش ۷۱

<div class="b" id="bn1"><div class="m1"><p>بس که برگفته پشیمان بوده‌ام</p></div>
<div class="m2"><p>بس که بر ناگفته شادان بوده‌ام</p></div></div>