---
title: >-
    بخش ۴۶
---
# بخش ۴۶

<div class="b" id="bn1"><div class="m1"><p>سرخی خفچه نگر از سرخ بید</p></div>
<div class="m2"><p>معصفر گون، پوشش او خود سفید</p></div></div>