---
title: >-
    بخش ۲۸
---
# بخش ۲۸

<div class="b" id="bn1"><div class="m1"><p>گر خوری از خوردن افزایدت رنج</p></div>
<div class="m2"><p>ور دمی مینو فراز آوردت و گنج</p></div></div>