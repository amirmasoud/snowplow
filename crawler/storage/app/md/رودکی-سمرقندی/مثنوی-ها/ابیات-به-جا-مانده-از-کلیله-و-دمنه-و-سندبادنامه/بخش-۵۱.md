---
title: >-
    بخش ۵۱
---
# بخش ۵۱

<div class="b" id="bn1"><div class="m1"><p>مرد مزدور اندر آغازید کار</p></div>
<div class="m2"><p>پیش او دوستان همی زد بی کیار</p></div></div>