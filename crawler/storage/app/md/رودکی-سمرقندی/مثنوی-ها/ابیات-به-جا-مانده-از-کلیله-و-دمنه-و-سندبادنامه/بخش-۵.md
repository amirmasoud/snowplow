---
title: >-
    بخش ۵
---
# بخش ۵

<div class="b" id="bn1"><div class="m1"><p>آن گرنج و آن شکر برداشت پاک</p></div>
<div class="m2"><p>وندر آن دستار آن زن بست خاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز کرد از خواب زن را نرم و خوش</p></div>
<div class="m2"><p>گفت: دزدانند و آمد پای پش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن زن از دکان فرود آمد چو باد</p></div>
<div class="m2"><p>پس فلرزنگش به دست اندر نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوی بگشاد آن فلرزش، خاک دید</p></div>
<div class="m2"><p>کرد زن را بانگ و گفتش: ای پلید</p></div></div>