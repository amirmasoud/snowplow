---
title: >-
    بخش ۶۵
---
# بخش ۶۵

<div class="b" id="bn1"><div class="m1"><p>دم سگ بینی ابا بتفوز سگ</p></div>
<div class="m2"><p>خشک گشت، کش نجنبد هیچ رگ</p></div></div>