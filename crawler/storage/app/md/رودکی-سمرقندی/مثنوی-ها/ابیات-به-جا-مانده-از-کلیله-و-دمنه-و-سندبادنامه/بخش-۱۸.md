---
title: >-
    بخش ۱۸
---
# بخش ۱۸

<div class="b" id="bn1"><div class="m1"><p>آفریده مردمان مر رنج را</p></div>
<div class="m2"><p>بیش کرده جان رنج آهنج را</p></div></div>