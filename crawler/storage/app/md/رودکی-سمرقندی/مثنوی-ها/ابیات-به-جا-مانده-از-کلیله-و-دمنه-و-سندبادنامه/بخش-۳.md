---
title: >-
    بخش ۳
---
# بخش ۳

<div class="b" id="bn1"><div class="m1"><p>هم چنان سرمه که دخت خوب روی</p></div>
<div class="m2"><p>هم به سان گرد بردارد ز روی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه هر روز اندکی برداردش</p></div>
<div class="m2"><p>بافدم روزی به پایان آردش</p></div></div>