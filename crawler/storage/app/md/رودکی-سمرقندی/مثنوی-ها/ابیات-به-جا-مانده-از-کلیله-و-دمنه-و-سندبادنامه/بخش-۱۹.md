---
title: >-
    بخش ۱۹
---
# بخش ۱۹

<div class="b" id="bn1"><div class="m1"><p>اندر آمد مرد با زن چرب چرب</p></div>
<div class="m2"><p>گنده پیر از خانه بیرون شد بترب</p></div></div>