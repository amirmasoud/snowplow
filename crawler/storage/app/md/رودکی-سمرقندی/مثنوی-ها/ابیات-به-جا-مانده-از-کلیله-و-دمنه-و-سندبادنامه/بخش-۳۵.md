---
title: >-
    بخش ۳۵
---
# بخش ۳۵

<div class="b" id="bn1"><div class="m1"><p>چونکه مالیده بدو گستاخ شد</p></div>
<div class="m2"><p>کار مالیده بدو در واخ شد</p></div></div>