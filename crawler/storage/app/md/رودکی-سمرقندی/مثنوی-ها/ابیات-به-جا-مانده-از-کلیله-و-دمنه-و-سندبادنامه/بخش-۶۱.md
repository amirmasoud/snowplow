---
title: >-
    بخش ۶۱
---
# بخش ۶۱

<div class="b" id="bn1"><div class="m1"><p>زد کلوخی بر هباک آن فزاک</p></div>
<div class="m2"><p>شد هباک او به کردار مغاک</p></div></div>