---
title: >-
    بخش ۴۵
---
# بخش ۴۵

<div class="b" id="bn1"><div class="m1"><p>زن چو این بشنیده شد خاموش بود</p></div>
<div class="m2"><p>کفشگر کانا و مردی لوش بود</p></div></div>