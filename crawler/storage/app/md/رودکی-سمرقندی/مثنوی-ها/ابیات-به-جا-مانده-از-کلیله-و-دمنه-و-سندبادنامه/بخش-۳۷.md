---
title: >-
    بخش ۳۷
---
# بخش ۳۷

<div class="b" id="bn1"><div class="m1"><p>کرد روبه یوزواری یک زَغَند</p></div>
<div class="m2"><p>خویشتن را زان میان بیرون فگند</p></div></div>