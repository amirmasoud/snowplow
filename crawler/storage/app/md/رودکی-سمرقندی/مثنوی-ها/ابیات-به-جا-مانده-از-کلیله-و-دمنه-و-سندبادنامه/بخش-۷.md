---
title: >-
    بخش ۷
---
# بخش ۷

<div class="b" id="bn1"><div class="m1"><p>گفت: هنگامی یکی شهزاده بود</p></div>
<div class="m2"><p>گوهری و پر هنر آزاده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد به گرمابه درون، اِستاد غوشت</p></div>
<div class="m2"><p>بود فربی و کلان، بسیارگوشت</p></div></div>