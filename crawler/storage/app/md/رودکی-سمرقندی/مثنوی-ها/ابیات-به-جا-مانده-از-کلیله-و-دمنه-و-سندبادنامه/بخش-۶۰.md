---
title: >-
    بخش ۶۰
---
# بخش ۶۰

<div class="b" id="bn1"><div class="m1"><p>موی سر جغبوت و جامه ریمناک</p></div>
<div class="m2"><p>از برون سو باد سرد و بیمناک</p></div></div>