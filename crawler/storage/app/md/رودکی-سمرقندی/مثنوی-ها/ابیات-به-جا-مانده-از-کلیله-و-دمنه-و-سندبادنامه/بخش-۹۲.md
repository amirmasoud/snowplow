---
title: >-
    بخش ۹۲
---
# بخش ۹۲

<div class="b" id="bn1"><div class="m1"><p>هست از مغز سرت، ای منگله</p></div>
<div class="m2"><p>همچو رش مانده تهی از کشکله</p></div></div>