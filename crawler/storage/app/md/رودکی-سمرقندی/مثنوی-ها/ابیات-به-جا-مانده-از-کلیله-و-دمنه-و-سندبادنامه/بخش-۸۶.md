---
title: >-
    بخش ۸۶
---
# بخش ۸۶

<div class="b" id="bn1"><div class="m1"><p>روی هر یک چون دو هفته گرد ماه</p></div>
<div class="m2"><p>جامه‌شان غفه، سموریشان کلاه</p></div></div>