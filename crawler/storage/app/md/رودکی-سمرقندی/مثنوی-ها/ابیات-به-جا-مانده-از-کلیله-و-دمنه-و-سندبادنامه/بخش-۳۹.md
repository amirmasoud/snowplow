---
title: >-
    بخش ۳۹
---
# بخش ۳۹

<div class="b" id="bn1"><div class="m1"><p>گنبدی نهمار بر برده، بلند</p></div>
<div class="m2"><p>نه ستونش از برون، نه زیر بند</p></div></div>