---
title: >-
    بخش ۷۰
---
# بخش ۷۰

<div class="b" id="bn1"><div class="m1"><p>نزد آن شاه زمین کردش پیام</p></div>
<div class="m2"><p>دارویی فرمود زامهران به نام</p></div></div>