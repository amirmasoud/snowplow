---
title: >-
    بخش ۹۹
---
# بخش ۹۹

<div class="b" id="bn1"><div class="m1"><p>آبکندی دور و بس تاریک جای</p></div>
<div class="m2"><p>لغز لغزان چون درو بنهند پای</p></div></div>