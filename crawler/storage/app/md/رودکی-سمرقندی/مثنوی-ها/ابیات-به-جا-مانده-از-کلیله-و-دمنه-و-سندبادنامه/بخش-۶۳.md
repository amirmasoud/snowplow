---
title: >-
    بخش ۶۳
---
# بخش ۶۳

<div class="b" id="bn1"><div class="m1"><p>خشم آمدش و همان گه گفت: ویک</p></div>
<div class="m2"><p>خواست کورا برکند از دیده کیک</p></div></div>