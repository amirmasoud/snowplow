---
title: >-
    بخش ۴۱
---
# بخش ۴۱

<div class="b" id="bn1"><div class="m1"><p>روز جستن تازیانی چون نوند</p></div>
<div class="m2"><p>بیش باشد تا تو باشی سودمند</p></div></div>