---
title: >-
    بخش ۸۹
---
# بخش ۸۹

<div class="b" id="bn1"><div class="m1"><p>پر بکنده، چنگ و چنگل ریخته</p></div>
<div class="m2"><p>خاک گشته، باد خاکش بیخته</p></div></div>