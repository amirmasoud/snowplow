---
title: >-
    بخش ۶
---
# بخش ۶

<div class="b" id="bn1"><div class="m1"><p>دمنه را گفتا که تا: این بانگ چیست؟</p></div>
<div class="m2"><p>با نهیب و سهم این آوای کیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دمنه گفت او را: جزین آوا دگر</p></div>
<div class="m2"><p>کار تو نه هست و سهمی بیشتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب هر چه بیشتر نیرو کند</p></div>
<div class="m2"><p>بند ورغ سست بوده بفگند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل گسسته داری از بانگ بلند</p></div>
<div class="m2"><p>رنجکی باشدت و آواز گزند</p></div></div>