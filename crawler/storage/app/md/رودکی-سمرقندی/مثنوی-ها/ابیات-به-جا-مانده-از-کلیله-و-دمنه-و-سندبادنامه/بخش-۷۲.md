---
title: >-
    بخش ۷۲
---
# بخش ۷۲

<div class="b" id="bn1"><div class="m1"><p>کرد باید مر مرا و او را رون</p></div>
<div class="m2"><p>شیر تا تیمار دارد خویشتن</p></div></div>