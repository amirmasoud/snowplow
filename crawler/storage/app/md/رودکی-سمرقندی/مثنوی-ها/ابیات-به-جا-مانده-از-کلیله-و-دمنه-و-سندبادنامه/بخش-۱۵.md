---
title: >-
    بخش ۱۵
---
# بخش ۱۵

<div class="b" id="bn1"><div class="m1"><p>آن که را دانم که: اویم دشمنست</p></div>
<div class="m2"><p>وز روان پاک بدخواه منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم به هر گه دوستی جویمش من</p></div>
<div class="m2"><p>هم سخن به آهستگی گویمش من</p></div></div>