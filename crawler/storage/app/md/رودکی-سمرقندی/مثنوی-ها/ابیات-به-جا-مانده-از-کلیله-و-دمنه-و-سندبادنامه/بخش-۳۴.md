---
title: >-
    بخش ۳۴
---
# بخش ۳۴

<div class="b" id="bn1"><div class="m1"><p>آنگهی گنجور مشک آمار کرد</p></div>
<div class="m2"><p>تا مرو را زان بدان بیدار کرد</p></div></div>