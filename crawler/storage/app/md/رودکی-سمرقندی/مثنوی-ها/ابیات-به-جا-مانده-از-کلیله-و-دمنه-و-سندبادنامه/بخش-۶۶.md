---
title: >-
    بخش ۶۶
---
# بخش ۶۶

<div class="b" id="bn1"><div class="m1"><p>چون فراز آید بدو آغاز مرگ</p></div>
<div class="m2"><p>دیدنش بیگار گرداند مجرگ</p></div></div>