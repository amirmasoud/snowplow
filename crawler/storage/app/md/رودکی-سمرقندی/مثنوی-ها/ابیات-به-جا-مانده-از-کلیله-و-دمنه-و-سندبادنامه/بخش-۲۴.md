---
title: >-
    بخش ۲۴
---
# بخش ۲۴

<div class="b" id="bn1"><div class="m1"><p>خایگان تو چو کابیله شدست</p></div>
<div class="m2"><p>رنگ او چون رنگ پاتیله شدست</p></div></div>