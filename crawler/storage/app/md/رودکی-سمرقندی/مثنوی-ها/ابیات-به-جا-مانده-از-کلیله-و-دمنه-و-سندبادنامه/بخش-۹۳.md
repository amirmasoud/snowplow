---
title: >-
    بخش ۹۳
---
# بخش ۹۳

<div class="b" id="bn1"><div class="m1"><p>بهترین یاران و نزدیکان همه</p></div>
<div class="m2"><p>نزد او دارم همیشه اندمه</p></div></div>