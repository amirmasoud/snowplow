---
title: >-
    بخش ۵۵
---
# بخش ۵۵

<div class="b" id="bn1"><div class="m1"><p>آتشی بنشاند از تن تفت و تیز</p></div>
<div class="m2"><p>چون زمانی بگذرد، گردد گمیز</p></div></div>