---
title: >-
    بخش ۳۸
---
# بخش ۳۸

<div class="b" id="bn1"><div class="m1"><p>مرد دینی رفت و آوردش کَنَند</p></div>
<div class="m2"><p>چون همی مهمان درِ من خواست کند</p></div></div>