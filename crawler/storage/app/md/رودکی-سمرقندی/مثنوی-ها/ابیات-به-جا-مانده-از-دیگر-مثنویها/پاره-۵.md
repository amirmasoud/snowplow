---
title: >-
    پاره ۵
---
# پاره ۵

<div class="b" id="bn1"><div class="m1"><p>لقمه‌ای از زهر زده در دهن</p></div>
<div class="m2"><p>مرگ فشردش همه در زیر غن</p></div></div>