---
title: >-
    پاره ۲
---
# پاره ۲

<div class="b" id="bn1"><div class="m1"><p>جوانی گسست و چیره زبانی</p></div>
<div class="m2"><p>طبعم گرفت نیز گرانی</p></div></div>