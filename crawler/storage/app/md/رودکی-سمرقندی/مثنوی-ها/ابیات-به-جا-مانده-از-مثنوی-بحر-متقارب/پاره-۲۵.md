---
title: >-
    پاره ۲۵
---
# پاره ۲۵

<div class="b" id="bn1"><div class="m1"><p>دو جوی روان از دهانش ز خلم</p></div>
<div class="m2"><p>دو خرمن زده بر دو چشمش ز خیم</p></div></div>