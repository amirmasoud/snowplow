---
title: >-
    پاره ۴۱
---
# پاره ۴۱

<div class="b" id="bn1"><div class="m1"><p>به خنیاگری نغز آورد روی</p></div>
<div class="m2"><p>که: چیزی که دل خوش کند، آن بگوی</p></div></div>