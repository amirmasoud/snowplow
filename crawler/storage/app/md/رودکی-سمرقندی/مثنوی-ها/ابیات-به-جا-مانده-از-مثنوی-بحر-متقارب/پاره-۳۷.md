---
title: >-
    پاره ۳۷
---
# پاره ۳۷

<div class="b" id="bn1"><div class="m1"><p>میلفنج دشمن، که دشمن یکی</p></div>
<div class="m2"><p>فزونست و دوست ار هزار اندکی</p></div></div>