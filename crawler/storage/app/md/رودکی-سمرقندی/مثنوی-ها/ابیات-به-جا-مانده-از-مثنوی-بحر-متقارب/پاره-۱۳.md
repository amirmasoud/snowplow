---
title: >-
    پاره ۱۳
---
# پاره ۱۳

<div class="b" id="bn1"><div class="m1"><p>نشست وسخن را همی خاش زد</p></div>
<div class="m2"><p>ز آب دهن کوه را شاش زد</p></div></div>