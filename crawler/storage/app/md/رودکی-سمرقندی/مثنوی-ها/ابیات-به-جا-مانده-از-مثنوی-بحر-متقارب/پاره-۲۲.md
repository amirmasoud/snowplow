---
title: >-
    پاره ۲۲
---
# پاره ۲۲

<div class="b" id="bn1"><div class="m1"><p>تن از خوی پر آب و دهان پر ز خاک</p></div>
<div class="m2"><p>زبان گشته از تشنگی چاک چاک</p></div></div>