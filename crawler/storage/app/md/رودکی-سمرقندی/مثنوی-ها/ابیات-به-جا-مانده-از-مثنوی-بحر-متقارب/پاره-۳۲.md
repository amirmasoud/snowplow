---
title: >-
    پاره ۳۲
---
# پاره ۳۲

<div class="b" id="bn1"><div class="m1"><p>که هرگه که تیره بگردد جهان</p></div>
<div class="m2"><p>بسوزد چو دوزخ شود با دران</p></div></div>