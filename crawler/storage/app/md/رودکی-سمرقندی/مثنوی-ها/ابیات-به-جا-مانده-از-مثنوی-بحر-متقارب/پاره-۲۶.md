---
title: >-
    پاره ۲۶
---
# پاره ۲۶

<div class="b" id="bn1"><div class="m1"><p>بهارست همواره هر روزیم</p></div>
<div class="m2"><p>به منکر فراوان، به معروف کم</p></div></div>