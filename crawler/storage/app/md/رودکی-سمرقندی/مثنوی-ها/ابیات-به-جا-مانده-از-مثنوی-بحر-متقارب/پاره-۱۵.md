---
title: >-
    پاره ۱۵
---
# پاره ۱۵

<div class="b" id="bn1"><div class="m1"><p>یکی بزم خرم بیاراستند</p></div>
<div class="m2"><p>می و رود و رامشگران خواستند</p></div></div>