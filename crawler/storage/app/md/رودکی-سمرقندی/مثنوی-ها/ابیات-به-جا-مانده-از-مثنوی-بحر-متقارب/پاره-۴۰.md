---
title: >-
    پاره ۴۰
---
# پاره ۴۰

<div class="b" id="bn1"><div class="m1"><p>جوان چون بدید آن نگاریده روی</p></div>
<div class="m2"><p>به سان دو زنجیر مرغول موی</p></div></div>