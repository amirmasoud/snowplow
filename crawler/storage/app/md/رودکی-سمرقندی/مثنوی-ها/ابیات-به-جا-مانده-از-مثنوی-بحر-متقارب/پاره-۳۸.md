---
title: >-
    پاره ۳۸
---
# پاره ۳۸

<div class="b" id="bn1"><div class="m1"><p>ایا خلعت فاخر از خرمی</p></div>
<div class="m2"><p>همی رفتی و می نوشتی ز می</p></div></div>