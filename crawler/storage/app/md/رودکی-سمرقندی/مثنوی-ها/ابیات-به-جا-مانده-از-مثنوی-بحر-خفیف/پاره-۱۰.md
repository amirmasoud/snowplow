---
title: >-
    پاره ۱۰
---
# پاره ۱۰

<div class="b" id="bn1"><div class="m1"><p>دخت کسری ز نسل کیکاوس</p></div>
<div class="m2"><p>درستی نام، نغز چون طاوس</p></div></div>