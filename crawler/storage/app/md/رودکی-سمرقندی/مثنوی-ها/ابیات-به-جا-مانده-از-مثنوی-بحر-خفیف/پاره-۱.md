---
title: >-
    پاره ۱
---
# پاره ۱

<div class="b" id="bn1"><div class="m1"><p>تا سمو سر برآورید از دشت</p></div>
<div class="m2"><p>گشت زنگار گون همه لب کشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر یکی کاردی ز خوان برداشت</p></div>
<div class="m2"><p>تا پزند از سمو طعامک چاشت</p></div></div>