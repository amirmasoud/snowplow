---
title: >-
    پاره ۲۳
---
# پاره ۲۳

<div class="b" id="bn1"><div class="m1"><p>چون نهاد او پهند را نیکو</p></div>
<div class="m2"><p>قید شد در پهند او آهو</p></div></div>