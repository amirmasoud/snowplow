---
title: >-
    پاره ۸
---
# پاره ۸

<div class="b" id="bn1"><div class="m1"><p>دور ماند از سرای خویش و تبار</p></div>
<div class="m2"><p>نسری ساخت بر سر کهسار</p></div></div>