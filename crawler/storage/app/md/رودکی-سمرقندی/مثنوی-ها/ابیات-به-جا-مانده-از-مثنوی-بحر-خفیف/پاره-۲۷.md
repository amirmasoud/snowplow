---
title: >-
    پاره ۲۷
---
# پاره ۲۷

<div class="b" id="bn1"><div class="m1"><p>آن که نشک آفرید و سرو سهی</p></div>
<div class="m2"><p>وان که بید آفرید و نار و بهی</p></div></div>