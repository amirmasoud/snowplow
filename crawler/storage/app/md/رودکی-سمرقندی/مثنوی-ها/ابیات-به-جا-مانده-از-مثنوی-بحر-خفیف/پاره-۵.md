---
title: >-
    پاره ۵
---
# پاره ۵

<div class="b" id="bn1"><div class="m1"><p>هر کرا راهبر زغن باشد</p></div>
<div class="m2"><p>گذر او به مرغزن باشد</p></div></div>