---
title: >-
    پاره ۲۴
---
# پاره ۲۴

<div class="b" id="bn1"><div class="m1"><p>چون به بانگ آمد از هوا بخنو</p></div>
<div class="m2"><p>می خور و بانگ رود و چنگ شنو</p></div></div>