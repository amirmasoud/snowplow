---
title: >-
    پاره ۲۶
---
# پاره ۲۶

<div class="b" id="bn1"><div class="m1"><p>ریش و سبلت همی خضاب کنی</p></div>
<div class="m2"><p>خویشتن را همی عذاب کنی</p></div></div>