---
title: >-
    پاره ۱۳
---
# پاره ۱۳

<div class="b" id="bn1"><div class="m1"><p>خویشتن پاک دار و بی‌پرخاش</p></div>
<div class="m2"><p>هیچ کس را مباش عاشق غاش</p></div></div>