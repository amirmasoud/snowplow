---
title: >-
    پاره ۱۷
---
# پاره ۱۷

<div class="b" id="bn1"><div class="m1"><p>از تو خالی نگارخانهٔ جم</p></div>
<div class="m2"><p>فرش دیبا فگنده بر بجکم</p></div></div>