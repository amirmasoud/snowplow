---
title: >-
    پاره ۵
---
# پاره ۵

<div class="b" id="bn1"><div class="m1"><p>نیارم بر کسی این راز بگشود</p></div>
<div class="m2"><p>مرا از خال هندوی تو بفنود</p></div></div>