---
title: >-
    پاره ۷
---
# پاره ۷

<div class="b" id="bn1"><div class="m1"><p>بود زودا، که آیی نیک خاموش</p></div>
<div class="m2"><p>چو مرغابی زنی در آب پاغوش</p></div></div>