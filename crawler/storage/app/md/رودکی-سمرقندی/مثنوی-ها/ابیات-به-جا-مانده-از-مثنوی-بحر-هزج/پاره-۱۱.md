---
title: >-
    پاره ۱۱
---
# پاره ۱۱

<div class="b" id="bn1"><div class="m1"><p>به راه اندر همی شد شاهراهی</p></div>
<div class="m2"><p>رسید او تا به نزد پادشاهی</p></div></div>