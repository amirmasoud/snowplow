---
title: >-
    پاره ۱
---
# پاره ۱

<div class="b" id="bn1"><div class="m1"><p>شبی دیرند و ظلمت را مهیا</p></div>
<div class="m2"><p>چو نابینا درو دو چشم بینا</p></div></div>