---
title: >-
    پاره ۱۲
---
# پاره ۱۲

<div class="b" id="bn1"><div class="m1"><p>بهشت آیین سرایی را بپرداخت</p></div>
<div class="m2"><p>زهر گونه درو تمثال‌ها ساخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز عود و چندن او را آستانه</p></div>
<div class="m2"><p>درش سیمین و زرین پالکانه</p></div></div>