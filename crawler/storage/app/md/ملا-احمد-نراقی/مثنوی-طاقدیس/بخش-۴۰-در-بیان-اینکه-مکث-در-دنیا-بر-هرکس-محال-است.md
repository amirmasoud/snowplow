---
title: >-
    بخش ۴۰ - در بیان اینکه مکث در دنیا بر هرکس محال است
---
# بخش ۴۰ - در بیان اینکه مکث در دنیا بر هرکس محال است

<div class="b" id="bn1"><div class="m1"><p>هان نپنداری که چون آید بسر</p></div>
<div class="m2"><p>عمر تو انجام یابد این سفر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا در این وادی تو هستی گام زن</p></div>
<div class="m2"><p>آخرین گامت در آن باشد وطن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی فزاید بعد از آنت مایه ای</p></div>
<div class="m2"><p>نی فزون گردد تورا پیرایه ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این سخن نزدیک دانایان خطاست</p></div>
<div class="m2"><p>نزد دانا این سفر بی انتهاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می روی منزل به منزل تا ابد</p></div>
<div class="m2"><p>در دو عالم لیس للسیر الاحد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم به دنیا هم به عقبا ای قوی</p></div>
<div class="m2"><p>می روی و می روی و می روی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچه گردد منتهی در این جهان</p></div>
<div class="m2"><p>آن عمل باشد عمل باشد بدان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این عمل لیک ای عمو آبستن است</p></div>
<div class="m2"><p>پس نژاد پاکش اندر مخزن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می نشاند ای برادر هر عمل</p></div>
<div class="m2"><p>کودکان ماه رویت در بغل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچنین هر کودکی که در شکم</p></div>
<div class="m2"><p>طفل دیگر هست زاید بی الم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس در این عالم دواسبه می روی</p></div>
<div class="m2"><p>سوی مقصد زان دو مرکب می دودی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن یکی باشد عملهای درست</p></div>
<div class="m2"><p>وان دگر نسل عملهای نخست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لیک در عقبی عمل شد چون تمام</p></div>
<div class="m2"><p>نامه ی اعمال را آمد ختام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زاده ی اعمال دنیا اندران</p></div>
<div class="m2"><p>نسلها دارد پیاپی بیکران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرق این دان ای رفیق نکته دان</p></div>
<div class="m2"><p>در میان این جهان و آن جهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فرق دیگر هست بین العالمین</p></div>
<div class="m2"><p>کان بود از مقتضای نشأتین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وان همینستی که چون نبود روا</p></div>
<div class="m2"><p>در سرای آخرت مرگ و فنا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زاده ی اعمال دنیا گر بماند</p></div>
<div class="m2"><p>تا که رخت خود به آن عالم کشاند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از برای آن دگر نبود زوال</p></div>
<div class="m2"><p>هست با هم نسل آن را اتصال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از پی همزادگانش می رسد</p></div>
<div class="m2"><p>هست نسلا بعد نسل تا ابد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لیک دنیا جای مرگست و نفاد</p></div>
<div class="m2"><p>هم در آنجا کون باشد هم فساد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زاده ی اعمال چون دیگر نژاد</p></div>
<div class="m2"><p>هست اندر معرض کون و فساد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تربیت کردی گر آن را روز و شب</p></div>
<div class="m2"><p>هم نگهبانیش از کسر و عطب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا بماند سالم از چشم بداک</p></div>
<div class="m2"><p>تا نیابد ره بدان حبط و هلاک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گردد آبستن به نسلی خوبتر</p></div>
<div class="m2"><p>یک سلاله آورد محبوبتر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لیک گر غافل شدی زان نیکزاد</p></div>
<div class="m2"><p>تا دمیدی هر زمانش مینماد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گرد او گیرند واغولان و دیو</p></div>
<div class="m2"><p>تاکشندش سوی خود از مکر و ریو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پس نمایندش هلاک اینست حبط</p></div>
<div class="m2"><p>پس بسر زن دستها کاینست حبط</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هین نگویی چون سفر بی انتهاست</p></div>
<div class="m2"><p>هم مسافر را به رنج و هم عناست</p></div></div>