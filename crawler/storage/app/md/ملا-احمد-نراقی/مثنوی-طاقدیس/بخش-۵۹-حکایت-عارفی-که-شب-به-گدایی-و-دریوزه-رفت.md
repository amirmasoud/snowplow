---
title: >-
    بخش ۵۹ - حکایت عارفی که شب به گدایی و دریوزه رفت
---
# بخش ۵۹ - حکایت عارفی که شب به گدایی و دریوزه رفت

<div class="b" id="bn1"><div class="m1"><p>بود در شهری یکی مرد خدای</p></div>
<div class="m2"><p>از در هر نیک و بد ببریده پای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جهان و اهل آن وارسته ای</p></div>
<div class="m2"><p>در به روی زشت و زیبا بسته ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزها در بندگی کردی به شام</p></div>
<div class="m2"><p>هم به شب تا صبحگاهان در قیام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزیش هر روز می آمد ز غیب</p></div>
<div class="m2"><p>می رسیدش راتبه بیشک و ریب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در رواتب او بر اندر شام و چاشت</p></div>
<div class="m2"><p>راتبه زان مطبخ پرنوش داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطبخی خالی ز دود و دردسر</p></div>
<div class="m2"><p>نی در آن آتش نه هیزم را گذر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار فرمایش همه قدوسیان</p></div>
<div class="m2"><p>از ثریا تا ثری شان میهمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بجز آن مطبخ پرنوش و قند</p></div>
<div class="m2"><p>جای دیگر ره نبرد آن ارجمند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می رسیدش مائده هر روز و شب</p></div>
<div class="m2"><p>بی صداع و کسب بی رنج و طلب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون ندارد پای رفتن خاربن</p></div>
<div class="m2"><p>امر آید سوی او از امر کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه او بشناخت باز و کلند</p></div>
<div class="m2"><p>بایدش با صد تعب کاریز کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می کشاند آب را دهقان به زور</p></div>
<div class="m2"><p>تا به پای خوشه ها از راه دور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دید اندر گاو و خر چون پای راه</p></div>
<div class="m2"><p>بردشان با چوب لت تا آبگاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دست کودک تا دهانش را ندید</p></div>
<div class="m2"><p>شیر از پستان مادر می مکید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا دکان نانوا را ره نبرد</p></div>
<div class="m2"><p>نان بجز از سفره بابا نخورد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود در طاعت چه او بیغش و ریب</p></div>
<div class="m2"><p>راتبه بودش ز شهرستان غیب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از قضا یکشب برای امتحان</p></div>
<div class="m2"><p>نامدش آن راتبه زان شارسان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای خدا زان امتحانها داد داد</p></div>
<div class="m2"><p>امتحان بس خانه ها برباد داد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن ابلیس از امتحان مردود شد</p></div>
<div class="m2"><p>طاعت صدقرن او نابود شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>امتحان بلعام را از پا فکند</p></div>
<div class="m2"><p>ساخت در تیه و به آتش آسمند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز امتحان برصیص عابد شد هلاک</p></div>
<div class="m2"><p>سجده آورد از برای آن نعاک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای پناه بی پناهان جهان</p></div>
<div class="m2"><p>می گریزم در پناهت ز امتحان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>من کجا و امتحانت ای خلیل</p></div>
<div class="m2"><p>پشه ی لاغر کجا و بار پیل</p></div></div>