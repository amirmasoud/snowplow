---
title: >-
    بخش ۱۸۹ - داستان آن بازرگان که خرما از بغداد به بصره برد بفروشد
---
# بخش ۱۸۹ - داستان آن بازرگان که خرما از بغداد به بصره برد بفروشد

<div class="b" id="bn1"><div class="m1"><p>بود در عهدی یکی بازارگان</p></div>
<div class="m2"><p>می ندید اصلاً ز سودایی زیان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خریدی سنگ یا خاک زمین</p></div>
<div class="m2"><p>می شدی نایاب چون در ثمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفت در بغداد بهر امتحان</p></div>
<div class="m2"><p>صد شتر خرما خرید و شد روان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانب بصره پی بیع و شرا</p></div>
<div class="m2"><p>تا ستاند زیره خرما را بها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیره را هم جانب کرمان برد</p></div>
<div class="m2"><p>سیب سازد سوی اصفاهان برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ببیند بخت خود را در عمل</p></div>
<div class="m2"><p>گفت با بخت و روان شد با عجل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از قضا سلطان بصره شد برون</p></div>
<div class="m2"><p>بهر صید از بصره با جمعی فزون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر طرف می تاخت از بهر شکار</p></div>
<div class="m2"><p>گه فکند اسب از یمین گه از یسار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاقبت زین گیر و دار و داوری</p></div>
<div class="m2"><p>یاوه شد ز انگشت شه انگشتری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قیمت آن باج کشمیر و ختن</p></div>
<div class="m2"><p>با خراج مصر و شامات و یمن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آمد اندر جستجو هم پادشاه</p></div>
<div class="m2"><p>هم امیر و هم وزیر و هم سپاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیشتر جستند و کمتر یافتند</p></div>
<div class="m2"><p>عاقبت از جستنش رو تافتند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خسته گشتند آن گروه از جستجو</p></div>
<div class="m2"><p>شاه خشم آلوده زاسب آمد فرو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر تل خاکی نشست او خشمناک</p></div>
<div class="m2"><p>پیشکارانش همه در روی خاک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از غضب می کرد در هرسو نگاه</p></div>
<div class="m2"><p>کاروانی دید می آید ز راه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت سوی من کشید این کاروان</p></div>
<div class="m2"><p>تا بپرسمشان ز جای و از مکان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا بپرسم چیست ایشان را متاع</p></div>
<div class="m2"><p>کلکم مسؤول کلکم قوم راع</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا بدانندم که من اینجاستم</p></div>
<div class="m2"><p>حاجتی گر هستشان گویاستم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاه راعی و رعیت چون گله</p></div>
<div class="m2"><p>کی گذارد گله را راعی یله</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر رعیت سوی راعی نایدی</p></div>
<div class="m2"><p>رفتن راعی سوی او بایدی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شاه آن باشد که خواند سوی خویش</p></div>
<div class="m2"><p>بینوایان سوزمندان پریش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ور بود بی دست و پایی لنگ و کور</p></div>
<div class="m2"><p>خود به سوی او رود از راه دور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نرم نرمک حالشان پرسان شود</p></div>
<div class="m2"><p>هم ز سوز دردشان جویان شود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای بسا بی دست و پایان علیل</p></div>
<div class="m2"><p>او فتاده زیر پای نره پیل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای بسا گنجشک بی برگ و نوا</p></div>
<div class="m2"><p>دور او بگرفته هرسو باشه ها</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر که را چنگالی و منقار بود</p></div>
<div class="m2"><p>در پی صید دل افکار بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دل بدزدند از درون سینه ها</p></div>
<div class="m2"><p>در میان چینه دانها چینه ها</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شاه در خرگاه و بر درگه حجاب</p></div>
<div class="m2"><p>چون نگردد شهر ویران ده خراب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هان و هان ای پادشه هشیار باش</p></div>
<div class="m2"><p>وقت خوابت می رسد هشیار باش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شاه نبود آنکه خسبد نیمروز</p></div>
<div class="m2"><p>صد برهنه بر درش با درد و سوز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شاهی و خفتن به سنجاب و سمور</p></div>
<div class="m2"><p>در برون افتاده صد مسکین عور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من بسی دارم در این مقصد مقال</p></div>
<div class="m2"><p>لیک بگذارم که تنگ امد مجال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هم مجال تنگ و هم دل تنگتر</p></div>
<div class="m2"><p>آنکه باید بشنود هم هست کر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چونکه شاه بصره شد در کاروان</p></div>
<div class="m2"><p>گفت که بود کاروان سالارتان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مرد بازرگان به پیش استاد و کرد</p></div>
<div class="m2"><p>صد ثنا از بهر آن سلطان فرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گفت آیی از کجا بار تو چیست</p></div>
<div class="m2"><p>در کجا این بار تو واگرد نیست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گفت از بغداد مقصد بصره است</p></div>
<div class="m2"><p>بارهایم جمله تمر و تمره است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گفت خرما سوی بصره می بری</p></div>
<div class="m2"><p>ابلهی هستی تو یا سوداگری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خامه چون اینجا رسید ای مرد هوش</p></div>
<div class="m2"><p>خون درون سینه ام آمد به جوش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زانکه گفتم هاتفی در گوش جان</p></div>
<div class="m2"><p>تو از آن ابله تری ای مستهان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مایه عمر گرامی داشتی</p></div>
<div class="m2"><p>هین بیاور تا چه زان برداشتی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>علم آموزی بری یا ارمغان</p></div>
<div class="m2"><p>از برای مدرس کروبیان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مایه ای کز ملک جان اندوختی</p></div>
<div class="m2"><p>خوش به مکتب داده ای بفروختی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دادی آن را و گرفتی از عوض</p></div>
<div class="m2"><p>به زبر به پیش به زیر به عوض</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا بری این را به آن محفل که هست</p></div>
<div class="m2"><p>جبرئیل آنجا یکی شاگرد پست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>می نیاری شرم ای صاحب شرف</p></div>
<div class="m2"><p>شصت ساله عمر خود کردی تلف</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>حاصل این شصت سال ای مرد مفت</p></div>
<div class="m2"><p>من چه گفتم آنچه گفت و این چه گفت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ب زبر ب پیش ب زیر ب بری</p></div>
<div class="m2"><p>تا کنی تعلیم جبریل از خری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>عمر خود دادی گرفتی ای حزون</p></div>
<div class="m2"><p>جزو دانی پر ز تخیلیل و ظنون</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آنچه کار تو نیاید هل یجوز</p></div>
<div class="m2"><p>هل یجوز نظمه ام لا یجوز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خرق آیا در فلک جایز بود</p></div>
<div class="m2"><p>این فلک قادر و یا عاجز بود</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هست آیا این هیولی یا صور</p></div>
<div class="m2"><p>یا صور هست از هیولی بیخبر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چند باشد یا رب اقسام عرض</p></div>
<div class="m2"><p>گر ندانم چون کنم با این غرض</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چند گز باشد زمین تا آسمان</p></div>
<div class="m2"><p>جانت از کونت برآید ای فلان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چند تخییلی به هم بر می نهی</p></div>
<div class="m2"><p>خویش را عالم نهی نام آنگهی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>علم اگر این است بگذار و برو</p></div>
<div class="m2"><p>صد شتر زین علم نزد من دو جو</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>رو سبد بافی بیاموز ای عمو</p></div>
<div class="m2"><p>گرده ی نانی بدست آور ازو</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هست علم فقه احکام ای پسر</p></div>
<div class="m2"><p>گر چه نزد اهل ایمان معتبر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>لیک امروز آنهمه تخییل شد</p></div>
<div class="m2"><p>سد راه و مانع تکمیل شد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>فقه خوب آمد ولی بهر عمل</p></div>
<div class="m2"><p>نی برای بحث و تعریف و جدل</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>پشکلی گر جست از کون بزی</p></div>
<div class="m2"><p>کور شد زان چشم مرد هرمزی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آن دیت آیا به صاحب بزد بود</p></div>
<div class="m2"><p>یادیت با قاضی هرمز بود</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گر ز قاف افتاد عنقا برچهی</p></div>
<div class="m2"><p>چند دلو از آن کشی گر آگهی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>خون حیض آید اگر از گوش زن</p></div>
<div class="m2"><p>حکم آن چبود بگو ای بوالحسن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گر زنی گردد ز جنی حامله</p></div>
<div class="m2"><p>ارث او چه بود ز جن ای صد دله</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>این غلط باشد غلط اندر غلط</p></div>
<div class="m2"><p>صرف کردن عمر خود را این نمط</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>کار داری اینقدر در پیش و پس</p></div>
<div class="m2"><p>ای برادر که خدا گوید که بس</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>گر بدانی در عقبها چیستت</p></div>
<div class="m2"><p>فرصت خاریدن سر نیستت</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خود بده انصاف ای مرد گزین</p></div>
<div class="m2"><p>هیچ عاقل می کند کاری چنین</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>وقت تنگ خویش را بفروختن</p></div>
<div class="m2"><p>این شلنگ تختها آموختن</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نام آن را علم کردن زابلهی</p></div>
<div class="m2"><p>بردنش نزد ملایک وانگهی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>این به نزد مرد دانا زشتتر</p></div>
<div class="m2"><p>یا به بصره بردنت خرمای تر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چون شنید این مرد بازرگان ز شاه</p></div>
<div class="m2"><p>بر زمین بنشست گفت ای جان تباه</p></div></div>