---
title: >-
    بخش ۴۳ - حکایت خلیفه با طاوس یمانی
---
# بخش ۴۳ - حکایت خلیفه با طاوس یمانی

<div class="b" id="bn1"><div class="m1"><p>آن خلیفه می شدی اندر حرم</p></div>
<div class="m2"><p>گرد بر گردش وشاقان و خدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمله میران و سران پیرامنش</p></div>
<div class="m2"><p>هرطرف بگرفته طرف دامنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوی خانه پادشاه ذوالجلال</p></div>
<div class="m2"><p>می خرامیدی چنین با صد دلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با هزاران ناز و تمکین تمام</p></div>
<div class="m2"><p>گام می زد جانب بیت الحرام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دید طاوس یمانی در رهش</p></div>
<div class="m2"><p>در خروش آمد نهاد آگهش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بانگ برزد کی تو دانای عوی</p></div>
<div class="m2"><p>جانب گرمابه گویا می روی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی سزاوار تو باشد کبر و ناز</p></div>
<div class="m2"><p>وانگهی در آستان بی نیاز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون شنید از وی خلیفه این مقال</p></div>
<div class="m2"><p>گفت بنگر من کیم چشمی بمال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می ندانی گوییا من کیستم</p></div>
<div class="m2"><p>زید و عمرو و بکر و خالد نیستم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چونکه این بشنید طاوس از غضب</p></div>
<div class="m2"><p>گفت چون نشناسمت من بوالعجب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آگهم ز انجام و از آغاز تو</p></div>
<div class="m2"><p>وانمایم بشنو اکنون راز تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اصل تو یک قطره ی آب منی</p></div>
<div class="m2"><p>کت پدر در شاشدان می پروری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چونکه از خود دور افکندت پدر</p></div>
<div class="m2"><p>مادر اندر شاشدان کردت مقر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاشدانت خانه گه دانت غذا</p></div>
<div class="m2"><p>پوششت اشکنبه و خونت غذا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اولت این آخرت مردار خوار</p></div>
<div class="m2"><p>کز تو نفرت می کند مردارخوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طعمه ی کرمان تن نازان شوی</p></div>
<div class="m2"><p>هم وقود نار دوزخیان شوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنت آغاز اینت انجام است حال</p></div>
<div class="m2"><p>از کثافات پلیدی یک جوال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یک شکنبه پر ز سرگین اشکمت</p></div>
<div class="m2"><p>وان به آکندن به هر دم از دمت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پس تو حمال نجاساتی کنون</p></div>
<div class="m2"><p>از چنین کس کبر سخت آید زبون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون خلیفه این شنید از وی خروش</p></div>
<div class="m2"><p>از نهادش شد بلند و شد ز هوش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حال ما اینست یکسر ای پسر</p></div>
<div class="m2"><p>دیده بگشا اول و آخر نگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>با چنین حالی زدن دم از منی</p></div>
<div class="m2"><p>نیست جز از احمقی و کودنی</p></div></div>