---
title: >-
    بخش ۵۶ - شرح حدیث قدسی کنت کنزاً مخفیاً فاحببت ان اعرف فخلقت الخلق لکی اعرف
---
# بخش ۵۶ - شرح حدیث قدسی کنت کنزاً مخفیاً فاحببت ان اعرف فخلقت الخلق لکی اعرف

<div class="b" id="bn1"><div class="m1"><p>خواست تا گردد عیان و آشکار</p></div>
<div class="m2"><p>سر زند خورشید در دیجورتار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیست خورشید ای که خاکم بر دهن</p></div>
<div class="m2"><p>ای زبانم کوته و لال از سخن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این زبان بی ادب ببریده باد</p></div>
<div class="m2"><p>وین دهان بیحیا دریده باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفرید این خلق نامعدود را</p></div>
<div class="m2"><p>جدولی ببرید بحر جود را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزنی بر بارگاه نور زد</p></div>
<div class="m2"><p>ذره ای زان نور بر این طور زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشت از آن خلق بی پایان پدید</p></div>
<div class="m2"><p>کل یوم و هوفی خلق جدید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سینه هاشان را دل آگاه داد</p></div>
<div class="m2"><p>در دبستان خردشان راه داد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچنین آن نقشهای بیحساب</p></div>
<div class="m2"><p>سربسر بودند پنهان در حجاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جملگی عاشق ظهور خویش را</p></div>
<div class="m2"><p>در طلب مرآت نور خویش را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواست تا رزاقیش گردد عیان</p></div>
<div class="m2"><p>آفرید آن طایفه محتاج نان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا رسد از قهر جانسوزش خبر</p></div>
<div class="m2"><p>کفر و اهریمن برآوردند سر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خواست تا غفاریش گردد پدید</p></div>
<div class="m2"><p>اهل جرم و معصیت را آفرید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای گنه کاران کنون با صد امید</p></div>
<div class="m2"><p>خانه ی غفاریش را در زنید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هم عطوفست و رؤوفست و کریم</p></div>
<div class="m2"><p>هم عفو و هم غفور و هم رحیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>واهب و حنان و منان است او</p></div>
<div class="m2"><p>صاحب لطف است و رحمن است او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جمله اینها عاصیان را طالبند</p></div>
<div class="m2"><p>طالبند و سوی ایشان مایلند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هریکی گویند هر شام و سحر</p></div>
<div class="m2"><p>یا عصاة انی لکم نعم المفر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اهربوا یا ایها العاصون الی</p></div>
<div class="m2"><p>لاتخافوا جرمکم طراعلی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای گروه مجرمان رو سیاه</p></div>
<div class="m2"><p>ای گنه کاران با صد دود آه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چونکه ایشان مر شما را یاورند</p></div>
<div class="m2"><p>غمگساران شفاعت گسترند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>راه نومیدی گرفتن بس خطاست</p></div>
<div class="m2"><p>بلکه انکار صفتهای خداست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>می کنی با نعمتهای بیکران</p></div>
<div class="m2"><p>با قیاس آن نعوت بیکران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بحر بی پایان کجا و قطره ای</p></div>
<div class="m2"><p>نیر اعظم کجا و ذره ای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بحر و خورشید از پی فهم شماست</p></div>
<div class="m2"><p>ورنه این تمثیلها عین خطاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پیش آن بحر کران پیچ پیچ</p></div>
<div class="m2"><p>جمله ی کون و مکان هیچ است هیچ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>با چنین دریای عفو بیکران</p></div>
<div class="m2"><p>کش نه آغازی نه انجامی توان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کفر باشد ناامیدی ای مهان</p></div>
<div class="m2"><p>گرچه باشد چون صفایی جرمتان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>من نیم نومید از آن بحر کرم</p></div>
<div class="m2"><p>ای هزاران خاک عالم بر سرم</p></div></div>