---
title: >-
    بخش ۴۲ - بیان اینکه به جهت امتحان دیدنیها پوشیده می شود
---
# بخش ۴۲ - بیان اینکه به جهت امتحان دیدنیها پوشیده می شود

<div class="b" id="bn1"><div class="m1"><p>کاندران رادان همه گمره شدند</p></div>
<div class="m2"><p>موشکافان جهان ابله شدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دور بینان کور و نابینا همه</p></div>
<div class="m2"><p>تیزهوشان بیهش و رسوا همه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داد بهر آزمایش جلوه ها</p></div>
<div class="m2"><p>هیچ اندر هیچ اندر هیچ را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وان چه بود آن چیز و بالاتر ز چیز</p></div>
<div class="m2"><p>کرد پنهان در پس صد پرده نیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این منی را در نظرها جلوه داد</p></div>
<div class="m2"><p>نام من هر هیچ بن هیچی نهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیخودی را از نظرها دور کرد</p></div>
<div class="m2"><p>دیده ها از دیدن آن کور کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به کی گویی من و من ای عمو</p></div>
<div class="m2"><p>گوییا نشناختی خود را نکو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قطره ی آب پلیدی مایه ات</p></div>
<div class="m2"><p>روز و شب ها سرکشی شد پایه ات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای منی تا چند مانی و منی</p></div>
<div class="m2"><p>ای دنی تا کی غرور و برتنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بایدت شستن بدست خویشتن</p></div>
<div class="m2"><p>کون خود هر روز و شب ای مؤتمن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای تو کون شور و تو کون خود بشو</p></div>
<div class="m2"><p>اینقدر منشین و ما و من مگو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فخر تو این بس که گویی نغز و چست</p></div>
<div class="m2"><p>کون خود را می توانم پاک شست</p></div></div>