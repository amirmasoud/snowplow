---
title: >-
    بخش ۸۷ - در بیان کل مامیز تموه با رهامکم فهو مخلوق مثلکم و مردود الیکم
---
# بخش ۸۷ - در بیان کل مامیز تموه با رهامکم فهو مخلوق مثلکم و مردود الیکم

<div class="b" id="bn1"><div class="m1"><p>علم تو جانا بجز تصویر نیست</p></div>
<div class="m2"><p>خود ببین تصویر جز تأثیر چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود خیالت می توراشد صورتی</p></div>
<div class="m2"><p>سازد آن را بهر فهمت آلتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود توراشیده کجا یزدان بود</p></div>
<div class="m2"><p>خاک عالم بر سر نادان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیر صورت نیست فهمت ای ظریف</p></div>
<div class="m2"><p>چیست صورت غیر مخلوقی ضعیف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ در خارج تو دیدی جان من</p></div>
<div class="m2"><p>صاحب آن صورت جانان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ندیدی آن اثر را وهم دان</p></div>
<div class="m2"><p>کار دست وهم را خالق مخوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچه در ذهن تو آید ای فتی</p></div>
<div class="m2"><p>هست مخلوقی چو تو بیدست و پا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلکه از تو پست تر صد مرحله</p></div>
<div class="m2"><p>هیچکس را او نگیرد چلمله</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون نه او را کردی از کس انتزاع</p></div>
<div class="m2"><p>بلکه خود کردیش از وهم اختوراع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست او را بازگشتی در برون</p></div>
<div class="m2"><p>بازگشتش هست وهم واژگون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاک را نسبت کجا با روح پاک</p></div>
<div class="m2"><p>بر سر ادراک اهل خاک خاک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنکه در دانایی خود عاجز است</p></div>
<div class="m2"><p>فکر او در درک حق ناجایز است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنکه خود را می نداند چیست آن</p></div>
<div class="m2"><p>کی شناسد پاک یزدان کیست آن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جمله فرسادان در اینجا ابلهند</p></div>
<div class="m2"><p>عارفان سرگشته اند و والهند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دوربینان جمله کورند و ضریر</p></div>
<div class="m2"><p>تیزهوشان ابلهند و در زحیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زمره ی کروبیان ماندند مات</p></div>
<div class="m2"><p>فرقه ی روحانیان اندر ثبات</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قلعه ی این قاف را عنقا ندید</p></div>
<div class="m2"><p>نی نهنگی قعر این یم را رسید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر زمین افکند این بار گران</p></div>
<div class="m2"><p>ناقه ها از جمله ی گنج تتان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اندرین ره یکقدم چون تاختند</p></div>
<div class="m2"><p>توسنان تنها به خاک انداختند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بحر ناپیدا کنار است ای رفیق</p></div>
<div class="m2"><p>هین مران کشتی که می گردی غریق</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دشت ناپیدا کرانست ای پسر</p></div>
<div class="m2"><p>هان مران مرکب که می آید به سر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ما عرفناک از دلیل ره شنو</p></div>
<div class="m2"><p>پس عصا دورافکن و دیگر مرو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون نشان از هستی خود یافتی</p></div>
<div class="m2"><p>آنچه شاید یافت ای جان یافتی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آسمان علم را این محور است</p></div>
<div class="m2"><p>آفتابش این و اینش اختر است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>میوه ی بستان علم این است این</p></div>
<div class="m2"><p>لؤلؤ عمان علم اینست این</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای خوش آن تاجر که آن لؤلؤ خرید</p></div>
<div class="m2"><p>ای خنک کامی که از این میوه چشید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هستی ما هست مطلق شد برآن</p></div>
<div class="m2"><p>منکشف از راه تحقیق و عیان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نی ز تقلید نیا و مام خویش</p></div>
<div class="m2"><p>داخل اسلام کرده نام خویش</p></div></div>