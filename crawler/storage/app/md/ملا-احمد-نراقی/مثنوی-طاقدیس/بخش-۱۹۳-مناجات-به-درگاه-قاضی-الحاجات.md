---
title: >-
    بخش ۱۹۳ - مناجات به درگاه قاضی الحاجات
---
# بخش ۱۹۳ - مناجات به درگاه قاضی الحاجات

<div class="b" id="bn1"><div class="m1"><p>ای خدا ای بی پناهان را پناه</p></div>
<div class="m2"><p>ای تو بر شاهان عالم پادشاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای نهان از جسم و جان دیدار تو</p></div>
<div class="m2"><p>گم شده عقل و خرد در کار تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم خداوند خداوندان تویی</p></div>
<div class="m2"><p>هم دوای درد بیدرمان تویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب توفان زای را کردی تو پل</p></div>
<div class="m2"><p>آتش سوزنده را کردی تو گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرگ شد گویا به پیراهن زتو</p></div>
<div class="m2"><p>پیرهن شد شاخ نسترون ز تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من چه گویم ظاهر و باطن تویی</p></div>
<div class="m2"><p>ای خدا هم اول و آخر تویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردن عالم به بند طوق توست</p></div>
<div class="m2"><p>آسمان در چرخ رقص از شوق توست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای خدا من هرچه گویم آن نئی</p></div>
<div class="m2"><p>در ثنایت آنچه جویم آن نئی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای تو عذرآموز هر شرمنده ای</p></div>
<div class="m2"><p>ای تو روزی بخش هر جنبنده ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای خدا دانم که من بد کرده ام</p></div>
<div class="m2"><p>لیک از من آنچه آمد کرده ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم تو آن کن آنچه می شاید ز تو</p></div>
<div class="m2"><p>ای جهان را آنچه می باید ز تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو من داری خداوندا بسی</p></div>
<div class="m2"><p>من ندارم لیک غیر از تو کسی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر من بیکس ندارم هیچکس</p></div>
<div class="m2"><p>گو نباشد رحمت لطف تو بس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هرکه از این می گریزد گو گریز</p></div>
<div class="m2"><p>چون تو باشی گو نباشد هیچ چیز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر فتادم دستگیر من تویی</p></div>
<div class="m2"><p>هم پناه و هم گریز من تویی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دست من گیر ای جهان را دستگیر</p></div>
<div class="m2"><p>ده پناهم ای تو عالم را گزیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر امیدی آمدستم بر درت</p></div>
<div class="m2"><p>چون روم نومید یا رب از برت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در ره امید و بیم افتاده ام</p></div>
<div class="m2"><p>در جنابت منتظر ایستاده ام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرچه کردم من بسی جرم و خطا</p></div>
<div class="m2"><p>از من آید ذلت و از تو عطا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر سیاه است ای خدا ما را گلیم</p></div>
<div class="m2"><p>تو سفیدش ساز از لطف عمیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر دریدم خود به دستم پرده را</p></div>
<div class="m2"><p>تو فرو مگذار بی پرده مرا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون تورا ستار دیدم ای خدا</p></div>
<div class="m2"><p>پرده ی خود را دریدم برملا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پرده دار عیب ناپاکی ما</p></div>
<div class="m2"><p>عذر خواه جرم بیباکی ما</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر دمم جرم و گناه تازه ای ست</p></div>
<div class="m2"><p>از تو لیک انعام بی اندازه ای ست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر به جرمم مکر شیطان افکند</p></div>
<div class="m2"><p>لطف تو سوزی به جانم می زند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از من آمد گر خطایی یا فساد</p></div>
<div class="m2"><p>زاری آموزی دهی توبه به یاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر زنی زخم و دوصد مرهم نهی</p></div>
<div class="m2"><p>ور کنی قهرم دوصد رحمت دهی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر ز من جرم است انعام از تو است</p></div>
<div class="m2"><p>گر ز من گامی است صد گام از تو است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر ره تاریک پیشم آوری</p></div>
<div class="m2"><p>صد چراغ از لطف پیشم آوری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای خدا ای من عطایت را رهین</p></div>
<div class="m2"><p>بنده ای از بندگانت را کمین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شکر گویم من کدامین نعمتت</p></div>
<div class="m2"><p>یاد آرم من کدامین رحمتت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یکنفس کو کز تو ناید صد عطا</p></div>
<div class="m2"><p>ساعتی کو نآمد از من صد خطا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کو دهان و کو زبان و کو کلام</p></div>
<div class="m2"><p>تا بگویم شرح آن انعام عام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن کدامین لطف و احسان گران</p></div>
<div class="m2"><p>که نکردی با من ای سلطان جان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وان چه زشتی وز هر زشتی بتر</p></div>
<div class="m2"><p>که نکردم من که صد خاکم بسر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در رحم دادی مرا جا از کرم</p></div>
<div class="m2"><p>پروریدی پیکرم در آن ظلم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کاسه ای کردی ز بالا واژگون</p></div>
<div class="m2"><p>کاین تنت را سر بود ای ذو فنون</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پنبه دانی را بهم آمیختی</p></div>
<div class="m2"><p>طرح چشم تیزبینم ریختی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کاین دو باشد دیده ی بینای تو</p></div>
<div class="m2"><p>هم طراز چهره ی زیبای تو</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آسمانها را در آن دادی محل</p></div>
<div class="m2"><p>جل شأن ربنا عزوجل</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گوشت پاره بر دهانم دوختی</p></div>
<div class="m2"><p>جمله اسما را بهم آموختی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کاین زبان و آلت گویای تو</p></div>
<div class="m2"><p>ترجمان خاطر دانای تو</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>پرورش دادی هوایی در صماخ</p></div>
<div class="m2"><p>هین برو بشنو ازین بانگ کراخ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چار دیوار مشید ساختی</p></div>
<div class="m2"><p>پیکرم را طرح نو انداختی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هم کشیدی ساقها کاین پای تو</p></div>
<div class="m2"><p>هم دو ساعد کین کف گیرای تو</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آنچه باید در درون و در برون</p></div>
<div class="m2"><p>جمله را دادی ز امر کاف و نون</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>روح را با جسم دادی ارتباط</p></div>
<div class="m2"><p>غیب را دادی بشاهد اختلاط</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>پس ره بیرون شدن زان تنگنای</p></div>
<div class="m2"><p>ره نمونی کردیم ای رهنمای</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>پا نهادم چون به صحرای شهود</p></div>
<div class="m2"><p>پهن کردی سفره انعام و جود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خون برایم شیر کردی در جگر</p></div>
<div class="m2"><p>هم ز پستان شیر را دادی ثمر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هم مکیدن مرمرا آموختی</p></div>
<div class="m2"><p>هم دل مادر برایم سوختی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گریه یادم دادی از بهر خبر</p></div>
<div class="m2"><p>در دل او گریه را دادی اثر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تلخ کردی خواب شیرینش به کام</p></div>
<div class="m2"><p>نی زکرم اندیشه کردی نی به حام</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تا تنم پرورده شد در این جهان</p></div>
<div class="m2"><p>سخت شد هم پی مرا هم استخوان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سی و سه دندان برایم ساختی</p></div>
<div class="m2"><p>طرح آنها در دهان انداختی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>پس ز شیرم سیر کردی در غذا</p></div>
<div class="m2"><p>طبع من را میل دادی اقتضا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>پاسبانی کردیم از هر گزند</p></div>
<div class="m2"><p>هم نگهبانی ز هر پست و بلند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در زمستان پوستین بخشیدیم</p></div>
<div class="m2"><p>هم به تابستان کتان پوشیدیم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>هم قبا دادی مرا هم پیرهن</p></div>
<div class="m2"><p>هم کلاه و موزه ای صاحب منن</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>در پناه آوردیم از گرم و سرد</p></div>
<div class="m2"><p>تا شدم زفت و کلان و شیرمرد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هم مرا دادی توانایی و گوش</p></div>
<div class="m2"><p>هم خرد هم عقل و دانایی و هوش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بر زبانم نام خود آموختی</p></div>
<div class="m2"><p>شمع توحیدم به دل افروختی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نور ایمان در دلم انداختی</p></div>
<div class="m2"><p>بندگی اندر سرشتم ساختی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دادیم در آستان حیدری</p></div>
<div class="m2"><p>هم سگی هم بندگی هم چاکری</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دادیم آگاهی از حل و حرام</p></div>
<div class="m2"><p>نی بتقلید شنیدن چون عوام</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هر دمی دادی عطای تازه ام</p></div>
<div class="m2"><p>در جهان کردی بلند آوازه ام</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هم عطا کردی ز نیکانم نژاد</p></div>
<div class="m2"><p>هم ز اخوان نکوبخت استناد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>هم سرم از هم سران افراشتی</p></div>
<div class="m2"><p>از همالان هم مرا برداشتی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دادیم پوران و دختان پاکزاد</p></div>
<div class="m2"><p>پاک زاد و پاک دین پاک اعتقاد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>پرده پوشیدی به روی کار من</p></div>
<div class="m2"><p>ستر کردی بر من ای ستار من</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>زشتهایم جمله خوب انگاشتی</p></div>
<div class="m2"><p>کرده ام ناکرده می پنداشتی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خواندمت کردی اجابت هر زمان</p></div>
<div class="m2"><p>مهربانتر از نیای مهربان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>من به قربانی خداییهای تو</p></div>
<div class="m2"><p>دل اسیر مهربانیهای تو</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>آنچه گفتم زانچه دانستم هزار</p></div>
<div class="m2"><p>بد یکی بل کمتر از یک بی شمار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>وانچه آن را من نمی دانم حساب</p></div>
<div class="m2"><p>گر نویسم می شود هفتصد کتاب</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>از تو اینها وانچه آن آمد ز من</p></div>
<div class="m2"><p>شرح آن نه از خامه آید نه از سخن</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>از سرم تا پای ذنبست و خطا</p></div>
<div class="m2"><p>از قدم تا فرق جرم است و خطا</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>در خور لطفت نکردم طاعتی</p></div>
<div class="m2"><p>از گنه فارغ نبودم ساعتی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>لیک دانی ای خدای ذوالمنن</p></div>
<div class="m2"><p>راه من زد نفس شوم اهرمن</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>وز گنه چون بید لرزیدم همی</p></div>
<div class="m2"><p>هیبت نهی تورا دیدم همی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ای خدا با این گناه بیشمار</p></div>
<div class="m2"><p>آدم بر درگهت امیدوار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ای خدا آن کن که فضلت را سزد</p></div>
<div class="m2"><p>عطر فضلت بر دماغم می وزد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ای خدا باشد دیت بر عاقله</p></div>
<div class="m2"><p>می کند هرکس عمل بر شا کله</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>شاکله ی تو جمله فضل است و عطا</p></div>
<div class="m2"><p>شاکله ی من جمله عصیان و خطا</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ای خدا دانم که من بد کرده ام</p></div>
<div class="m2"><p>آنچه کردم لیک با خود کرده ام</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>آمدم اکنون برت ای ذوالکرم</p></div>
<div class="m2"><p>فاش می گویم خدایا الندم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>این ندم را گر بگفتم بارها</p></div>
<div class="m2"><p>توبه ها بشکسته ام بسیارها</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>لیک مغرور کرمهای توام</p></div>
<div class="m2"><p>سرخوش از صهبای آلای توام</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>خود تو دادی یاد این عذرای رحیم</p></div>
<div class="m2"><p>گفته ی ما غر بالرب الکریم</p></div></div>