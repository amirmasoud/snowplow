---
title: >-
    بخش ۱
---
# بخش ۱

<div class="b" id="bn1"><div class="m1"><p>ای رفیقان بشنوید این داستان</p></div>
<div class="m2"><p>بشنوید این داستان از راستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پادشاهی بود در ملک جهان</p></div>
<div class="m2"><p>مالک الملک جهان و ملک جان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمله شاهان غاشیه گردان او</p></div>
<div class="m2"><p>جمله را سر بر خط فرمان او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آستانش پادشاهان را پناه</p></div>
<div class="m2"><p>بود او سالار و دیگرها سپاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در گلستان بود او را طوطیی</p></div>
<div class="m2"><p>دلگشا ونغز و زیبا طوطئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طوطئی خوش لهجه ی فرخ لقا</p></div>
<div class="m2"><p>طوطئی شیربن زبان و جان فزا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آشیانش کنگره ی قصر رفیع</p></div>
<div class="m2"><p>طوفگاهش عرصه ملک وسیع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جای او گاهی گلستان ارم</p></div>
<div class="m2"><p>گاه در دامان شاه محترم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می نخوردی لقمه جز از دست شاه</p></div>
<div class="m2"><p>جز ز دست پادشاه نیکخواه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قند و شکر می نهادش بر دهان</p></div>
<div class="m2"><p>روز و شب از دست خود آن ارسلان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چونکه گشتی تشنه شاه مستطاب</p></div>
<div class="m2"><p>دادیش از جام خاص خویش آب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درگه و بیگه انیس شاه بود</p></div>
<div class="m2"><p>جان او با جان شه همراه بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سرگذشتی تا لب طوطی نگفت</p></div>
<div class="m2"><p>گاه خفتن دیده ی سلطان نخفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صبحدم تا نطق آن گویا نشد</p></div>
<div class="m2"><p>چشم شه از خواب نوشین وانشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با کسی جز شاه طوطی رام نی</p></div>
<div class="m2"><p>شاه را بی او دمی آرام نی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر دو تن در عاشقی گشته سمر</p></div>
<div class="m2"><p>هر یکی معشوق و عاشق آندگر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جمله معشوقان عشاق ای پسر</p></div>
<div class="m2"><p>حالشان را اینچنین دان سر بسر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرکه شد معشوق عاشق نیز هست</p></div>
<div class="m2"><p>در دل او عشق شورانگیز هست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عشق عاشق هم زجذب عشق اوست</p></div>
<div class="m2"><p>گشته پیداوین کشاکش هم ازوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کهربا عاشق بود لیک ای عمو</p></div>
<div class="m2"><p>کاه را بنگر که آید سوی او</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این سخن را گر همی خواهی بیان</p></div>
<div class="m2"><p>رو یحبهم و یحبونه بخوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر نبودی حشمت سلطان حسن</p></div>
<div class="m2"><p>وان مناعتهای بی پایان حسن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کبر و ناز و بی نیازیهای آن</p></div>
<div class="m2"><p>دورباش خود نماییهای آن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خوبرویان پرده برمی داشتند</p></div>
<div class="m2"><p>ناله ها از سینه می افراشتند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پرده بر خود می دریدندی همه</p></div>
<div class="m2"><p>سوی عاشق می دویدندی همه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رویشان بودی ز عاشق زردتر</p></div>
<div class="m2"><p>آهشان از آه او پر دردتر</p></div></div>