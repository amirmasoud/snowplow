---
title: >-
    بخش ۲۰۲ - رجوع به حکایت ابراهیم خلیل و اسماعیل ع
---
# بخش ۲۰۲ - رجوع به حکایت ابراهیم خلیل و اسماعیل ع

<div class="b" id="bn1"><div class="m1"><p>تا عیان گردد سر پنهان تو</p></div>
<div class="m2"><p>پرتو اندازد به عالم جان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا شود آن مغز خوش پیدا ز پوست</p></div>
<div class="m2"><p>تا شناسد خلق بیگانه ز دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا شناسد هریکی ذات از عرض</p></div>
<div class="m2"><p>فربهی را واشناسد از مرض</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود همی دانست آن سلطان غیب</p></div>
<div class="m2"><p>پاکی آن پاکزاد از شک و ریب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پاک می دانستش از هر غل و غش</p></div>
<div class="m2"><p>صاف می دانستش و زیبا و خوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیر مهرش را به جانش راه نیست</p></div>
<div class="m2"><p>ره کلف را در رخ این ماه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن یکی ماه است و ماه بی کلف</p></div>
<div class="m2"><p>آن بود خورشید در برج شرف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن یکی دریا بی پهناستی</p></div>
<div class="m2"><p>قطره هایش لؤلؤ لالاستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هفت دریا پیش آن دریا نمی</p></div>
<div class="m2"><p>روح قدسی در لباس آدمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود خورشیدی ولیکن از غمام</p></div>
<div class="m2"><p>بود شمشیری ولیکن در نیام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواست تا بر عالمی پیدا شود</p></div>
<div class="m2"><p>نور او بر عالمی رخشا شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دوستان را دوستی آرد به یاد</p></div>
<div class="m2"><p>زامتحان بر وی دری از تو گشاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرده بودش امتحانها پیش ازین</p></div>
<div class="m2"><p>خواست لیکن ابتلایی بس متین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نور او در کوره های تابناک</p></div>
<div class="m2"><p>رفته و بیرون شده زیبا و پاک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نقش او را اندر آتش برده بود</p></div>
<div class="m2"><p>صافش از آتش برون آورده بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باز او را امتحان تازه خواست</p></div>
<div class="m2"><p>در جهان او را بلند آوازه خواست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زر او را برد در آتش نخست</p></div>
<div class="m2"><p>چونکه از آتش برون آمد درست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برد او را زیر دستان بلا</p></div>
<div class="m2"><p>سکه دولت بر او زد برملا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سکه زد او را به نام خویشتن</p></div>
<div class="m2"><p>کرد سرشارش ز جام خویشتن</p></div></div>