---
title: >-
    بخش ۲۲۷ - دستور یافتن حضرت سلیمان برای مهمانی مخلوقات
---
# بخش ۲۲۷ - دستور یافتن حضرت سلیمان برای مهمانی مخلوقات

<div class="b" id="bn1"><div class="m1"><p>کرد روزی را معین بعد سال</p></div>
<div class="m2"><p>داد فرمان از پی جمع نوال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جن و انس و وحش و طیر و دیو و دد</p></div>
<div class="m2"><p>جمله افتادند اندر سعی و کد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمله ساعی در براری و بحار</p></div>
<div class="m2"><p>باد حمال و زمین تحویل دار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پهن دشتی انتها بیگانه اش</p></div>
<div class="m2"><p>شد معین بهر شربتخانه اش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر آن انبارها انباشتند</p></div>
<div class="m2"><p>کوهها ز انبارها افراشتند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوسفند و گاو و اشتر بی شمار</p></div>
<div class="m2"><p>صد هزار اندر هزار اندر هزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی توان گفتن ولی دانم همی</p></div>
<div class="m2"><p>جمع شد سالی به سعی عالمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز میعاد آمد و بنشست شاه</p></div>
<div class="m2"><p>بر سریر عزت اندر وعده گاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیل دیو و جنیان و انسیان</p></div>
<div class="m2"><p>جمله را دامان خدمت بر میان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون برآمد آفتاب از کوهسار</p></div>
<div class="m2"><p>شد خطاب مستطاب از کردگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کای همه روزی خوران بحر و بر</p></div>
<div class="m2"><p>ساکنان خاوران تا باختر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ذی سلیمان نبی پویا شوید</p></div>
<div class="m2"><p>روزی امروز ازو جویا شوید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سوی مهمان خانه ی او رو کنید</p></div>
<div class="m2"><p>روزی خود را طلب از او کنید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جملگی امروز مهمان وی اند</p></div>
<div class="m2"><p>جیره خوار سفره ی خوان وی اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون رسید از رازق کل این خطاب</p></div>
<div class="m2"><p>جمله ی روزی خوران با صد شتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرغ و ماهی جن و انس و دیو و دد</p></div>
<div class="m2"><p>وحش و طیر و مار و مور و خوب و بد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جملگی پویای مهمانی شدند</p></div>
<div class="m2"><p>رو به شیلان سلیمانی شدند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با شکمهای تهی ز آرامگاه</p></div>
<div class="m2"><p>هریکی از دیگری می جست راه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وندران صحرا سلیمان بر سریر</p></div>
<div class="m2"><p>هر طرف پیچیده آواز تبیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گه نمایان شد ز هرسو میهمان</p></div>
<div class="m2"><p>کاروان در کاروان در کاروان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بالها بربسته بگشوده دهن</p></div>
<div class="m2"><p>رو به آن سو جملگی در تاختن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن یکی می رفت از پا آن زپر</p></div>
<div class="m2"><p>آن یکی از سینه آن دیگر زبر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن یکی غلطان تن خود می کشید</p></div>
<div class="m2"><p>آن یکی می جست و آن یک می دوید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ماهیی آمد نخست از طرف دشت</p></div>
<div class="m2"><p>دشت اندر فلس از آن غرق گشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نی کران پیدا ز طولش نی ز عرض</p></div>
<div class="m2"><p>ارض در پیشش چو ماهی پیش ارض</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفت بعد از شرح و تسلیم و ثنا</p></div>
<div class="m2"><p>یا بن داود النبی این الغذا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفت رو رو مطبخ و انبار پر</p></div>
<div class="m2"><p>هرچه داری اشتها آنجا بخور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سوی شربتخانه آمد ماهیک</p></div>
<div class="m2"><p>آنچه بود آنجا نهاد اندر حنک</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کرد یکسر آن زمین را رفت و رو</p></div>
<div class="m2"><p>لقمه ای کرد و فکند اندر گلو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جمله را بلعید با صد اشتها</p></div>
<div class="m2"><p>بازگشت و باز گفت این الغذا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای سلیمان لقمه ای شد آنچه بود</p></div>
<div class="m2"><p>هین دو لقمه دیگرم ده زود زود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>طعمه ام باشد سه لقمه هر غذا</p></div>
<div class="m2"><p>هم سه لقمه بایدم بهر عشا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>لقمه ها جمله زینگون لقمه ها</p></div>
<div class="m2"><p>می رساند خلق ارض و سما</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>لقمه هایم می رسد از خوان غیب</p></div>
<div class="m2"><p>بی طلب هر روزه ام بی نقص و عیب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جیره ای هر روزه از انبار او</p></div>
<div class="m2"><p>می رسد از جود و از قفیای او</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شد محول طعمه ی من ای نبی</p></div>
<div class="m2"><p>بر تو امروز اعطنی ثم اعطنی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شد سلیمان محو و حیران بر سریر</p></div>
<div class="m2"><p>گونه هایش شد ز خجلت چون ضریر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از سریر افکند خود را بر زمین</p></div>
<div class="m2"><p>بر زمین مالید از ذات جبین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کای خدا رحمی به این دلگشته خون</p></div>
<div class="m2"><p>کز گلیم خود نهاده پا برون</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>من یکی از جیره خواران توام</p></div>
<div class="m2"><p>یک طفیلی بر سر خوان توام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون طفیل دیگری آرد طفیل</p></div>
<div class="m2"><p>خود هم افتد در هزاران ویرویل</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یک تتمه دارد اما این خبر</p></div>
<div class="m2"><p>این زمان بگذار تا وقت دگر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از روایت بر حکایت باز گرد</p></div>
<div class="m2"><p>گو تمام حال آن مزدور مرد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گفت با زن کان کریم کارساز</p></div>
<div class="m2"><p>کیست می دانی کریم چاره ساز</p></div></div>