---
title: >-
    بخش ۴۱ - در بیان آنکه چون سفر را نهایت نیست لازم می آید  سبکبار و چالاک بود
---
# بخش ۴۱ - در بیان آنکه چون سفر را نهایت نیست لازم می آید  سبکبار و چالاک بود

<div class="b" id="bn1"><div class="m1"><p>نیست راحت تا سفر آید به سر</p></div>
<div class="m2"><p>انه کان السفر بعض السقر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این مسافر تا ابد در جنبش است</p></div>
<div class="m2"><p>بر کنار از راحت و آسایش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاین نباشد ای برادر جز قیاس</p></div>
<div class="m2"><p>از قیاس ابلیس شد در اقتباس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت جعفر آن امام راستین</p></div>
<div class="m2"><p>اول من قاس ابلیس اللعین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار بار آن نگارستان پاک</p></div>
<div class="m2"><p>کی توان کردن قیاس از شوره خاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندرین عالم مسافر روز و شب</p></div>
<div class="m2"><p>سوی منزل می رود با صد تعب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جهان پاک لیکن ای عمو</p></div>
<div class="m2"><p>منزل آید جلد و چابک سوی او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوی مقصد بایدت رفت این جهان</p></div>
<div class="m2"><p>مقصد آنجا سوی تو گردد روان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور تو هم ره را نوردی روز و شب</p></div>
<div class="m2"><p>نی از آنت رنج باشد نی تعب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این سفر راه عراق و شام نیست</p></div>
<div class="m2"><p>طی راهش را پی و اقدام نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راه شهر هستی ملک بقاست</p></div>
<div class="m2"><p>کی در این ره راه رو محتاج پاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پا نمی خواهم که گردد آبله</p></div>
<div class="m2"><p>بار سنگین نی که گردد راحله</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پای این ره شوق و مرکب عزم توست</p></div>
<div class="m2"><p>کاروان آن سبکباران چیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چونکه شوق آمد نه رنجست و نه غم</p></div>
<div class="m2"><p>با سبکباری نه محنت نه الم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای سبکباران خدا را همتی</p></div>
<div class="m2"><p>فرصتی تا هست از خود رحلتی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هان و هان ای رهروان بیگاه شد</p></div>
<div class="m2"><p>راهزن از حالتان آگاه شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همرهان رفتند و در خوابی هنوز</p></div>
<div class="m2"><p>پل شکست و آن سر آبی هنوز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای خنک آنکس که هنگام رحیل</p></div>
<div class="m2"><p>نی گرفتار کثیر و نی قلیل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خیزد از خواب سبک گردد روان</p></div>
<div class="m2"><p>نی غم یار و نه فکر کاروان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نی ز دزدش بیم و نی خوف عسس</p></div>
<div class="m2"><p>نی کسش در یاد و نی یادش ز کس</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هان و هان ای دوستان باوفا</p></div>
<div class="m2"><p>وقت کوچ آمد سوی ملک بقا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا کی آرامیده ره بی انتهاست</p></div>
<div class="m2"><p>وقت شد بیگاه و رهزن در قفاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بار سنگین بفکنید از راحله</p></div>
<div class="m2"><p>تا نیایند از قفای قافله</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بفکنید از دوش خود این بارها</p></div>
<div class="m2"><p>بارهایی بلکه این بیزارها</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آنچه داری ای برادر بار نیست</p></div>
<div class="m2"><p>جز خیال و صورت و پندار نیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز امتحان نادیدنیها دیده شد</p></div>
<div class="m2"><p>دیدنیها زامتحان پوشیده شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای توانایی که بهر آزمون</p></div>
<div class="m2"><p>بست این نقش بدیع واژگون</p></div></div>