---
title: >-
    بخش ۱۰۳ - حکایت زبون شدن پلنگ در دست آدمیزاد
---
# بخش ۱۰۳ - حکایت زبون شدن پلنگ در دست آدمیزاد

<div class="b" id="bn1"><div class="m1"><p>آن شنیدستی پلنگی شیرگیر</p></div>
<div class="m2"><p>آمد از کهسار سیل آسا به زیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناگهانش گربه ای آمد به پیش</p></div>
<div class="m2"><p>گربه ای زار و نحیف و سینه ریش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گربه ای بس لاغر و بیتاب و توش</p></div>
<div class="m2"><p>گربه ای عاجز چو پیش گربه موش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت با گربه پلنگ زورمند</p></div>
<div class="m2"><p>کاینچنین زار از چئی ای مستمند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اینچنین زار و نحیف از چیستی</p></div>
<div class="m2"><p>تو مگر از معشر ما نیستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هستی ای مسکین تو از جنس سباع</p></div>
<div class="m2"><p>هم سباع و هم بهر مرزی مطاع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هین بگو کو پنجه ی شیر اوژنت</p></div>
<div class="m2"><p>وان برو بازوی نخجیر افکنت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هین بگو ای گربه کو کوپال تو</p></div>
<div class="m2"><p>کو تنومندی تو کو یال تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناتوانا تن تو مسکین از چه ای</p></div>
<div class="m2"><p>عاجز و بیچاره چندین از چه ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زور و بازوی کدامین مهترت</p></div>
<div class="m2"><p>کرده تا این حد زبون و کهترت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گربه فریاد و فغان آغاز کرد</p></div>
<div class="m2"><p>زد بسر دست و شکایت باز کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کای امیر از من مپرس این داستان</p></div>
<div class="m2"><p>زانکه می ناید بگفتن راستان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زمره ی شیطان فریب آدم نژاد</p></div>
<div class="m2"><p>زاده ی خاک از ملک صد ره زیاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر به صورت آدم اما دیوسار</p></div>
<div class="m2"><p>در شمایل یوسف اما گرگ خوار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گربه شان را گر پلنگ افتد بچنگ</p></div>
<div class="m2"><p>گردد از موشان بسی کمتر پلنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ریش گاوش در لسان ساحری</p></div>
<div class="m2"><p>در شمار گاو نارد مشتری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیره زالش پنجه با رستم زند</p></div>
<div class="m2"><p>طفل خوردش بازوی نیرم زند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آدمی شان نام شیطانشان به بند</p></div>
<div class="m2"><p>خاکشان مسکن فلکشان در کمند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من همی اندر بچنگ این گروه</p></div>
<div class="m2"><p>چون پلنگانم کجا باشد شکوه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گربه کبود کز هژبر شرزه شیر</p></div>
<div class="m2"><p>آید اندر چنگ این گرگان اسیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زودش یاد مهتری از سر شود</p></div>
<div class="m2"><p>گربه چه از موش کوچکتر شود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون پلنگ از گربه این دستان شنید</p></div>
<div class="m2"><p>چون هژبر تیرخورده بردمید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفت هی هی این بنی آدم کجاست</p></div>
<div class="m2"><p>در کجاشان بنگه و حزنم کجاست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا بدرم پوست بر اندامشان</p></div>
<div class="m2"><p>تا ز سر بیرون کنم سرسامشان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هم بیارم مهتری را یاد تو</p></div>
<div class="m2"><p>هم بگیرم من از ایشان داد تو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رخنه در بنیان جانشان افکنم</p></div>
<div class="m2"><p>آتش اندر خانمانشان افکنم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رهنمونی کن مرا و پیش باش</p></div>
<div class="m2"><p>من ز دنبالم تو بی تشویش باش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گربه از پیش و پلنگ از پس بدشت</p></div>
<div class="m2"><p>ره نوردیدند تا یک سبز کشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرد کی دهقان بگرد کشت زار</p></div>
<div class="m2"><p>چون سحاب نوبهاران آبیار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گربه را چون دیده بر مرد اوفتاد</p></div>
<div class="m2"><p>گفت اینک آدم و خشک ایستاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شد پلنگ کینه پرور سوی مرد</p></div>
<div class="m2"><p>تا برانگیزد به کین گربه گرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پس به غریدن برآمد با غصب</p></div>
<div class="m2"><p>گفت ای اهریمن آدم لقب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هین تویی با گربه نیرو آزما</p></div>
<div class="m2"><p>شد زبون از تو چنین همجنس ما</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جنس شیران از تو شد اینگونه پست</p></div>
<div class="m2"><p>روی ناموس بزرگان از تو خست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مرد دهقان گفت آری این ستم</p></div>
<div class="m2"><p>بر وی آمد از من آید بر تو هم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بلکه شیر نر که سلطان شماست</p></div>
<div class="m2"><p>دست و پاها بسته در زندان ماست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جمله شیران را ببین از ما زبون</p></div>
<div class="m2"><p>هم پلنگانند از ما غرق خون</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کودکی از ما ببندد شیر را</p></div>
<div class="m2"><p>کز لبان خود نشسته شیر را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هست نیروی شما از ما وبال</p></div>
<div class="m2"><p>نیروی ما هست از بهر جلال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زاید از تن قوت شیر و پلنگ</p></div>
<div class="m2"><p>قوت ما جوشد از دریای هنگ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هست از جان شوکت و گرکام ما</p></div>
<div class="m2"><p>هست شیران را گریز از نام ما</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>این شنید از مرد دهقان چون پلنگ</p></div>
<div class="m2"><p>شد چو نطع خود ز غیرت رنگ رنگ</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تیز شد کای سخت روی سست رای</p></div>
<div class="m2"><p>تند شد کای دیو خوی ژاژ خای</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>لاف کم زن پاس جان خویش دار</p></div>
<div class="m2"><p>گر تورا مردیست دستی زن بکار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مرد آن باشد که بی گفتار خویش</p></div>
<div class="m2"><p>وانماید بر جهانی کار خویش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بندد و بگشاید اما بی کلام</p></div>
<div class="m2"><p>سوزد و بنوازد اما بی نیام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آنکه او گفتار بیکردار داشت</p></div>
<div class="m2"><p>راست گویم مرد از آن کس عار داشت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مرد مردان آنکه کاری گفت کرد</p></div>
<div class="m2"><p>مرد نبود بلکه باشد نیم مرد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آنکه می گوید ولی نارد بکار</p></div>
<div class="m2"><p>زن بود زن چادر و معجر بیار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چاره آن باشد که نه گفت و نه کرد</p></div>
<div class="m2"><p>باشد آن خنثی نه زن باشد نه مرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هی اگر مردی تو این گفتار چیست</p></div>
<div class="m2"><p>جای کردار است برخیز و بایست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تا ببینم زور و بازوی تورا</p></div>
<div class="m2"><p>وانمایم با تو نیروی تورا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خیز و بنگر حمله ی شیر اوژنم</p></div>
<div class="m2"><p>زخمهای پنجه ی پیل افکنم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شیر خشمینی و گر زخمی گراز</p></div>
<div class="m2"><p>از تو خواهم خواست کین گربه باز</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>از پلنگ آن مرد دهقان چون شنید</p></div>
<div class="m2"><p>زین سخن جای سگالیدن ندید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>راه و رسم مکر و حیله ساز کرد</p></div>
<div class="m2"><p>رفت شیری روبهی آغاز کرد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کای قوی افکن جوانمرد شجاع</p></div>
<div class="m2"><p>در جوانمردی پر از جنس سباع</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گیرودار جنگ را باید بسیج</p></div>
<div class="m2"><p>بی سلح یکدشت کندآور بهیچ</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مر تورا چنگال و یال آمد سلاح</p></div>
<div class="m2"><p>بی سلح من کی بود جنگم صلاح</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>باید اول اسلحه آراستن</p></div>
<div class="m2"><p>پس به جنگ دشمنان برخاستن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تو مسلح من برهنه ای امیر</p></div>
<div class="m2"><p>کی روا باشد نبرد و داروگیر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گفت ای آدم سلاح تو کجاست</p></div>
<div class="m2"><p>گفت آلات نبردم در سر است</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گفت سوی خانه رو آلات جنگ</p></div>
<div class="m2"><p>راست کن بر خویش و بازآ بید رنگ</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>خنده ای زد مرد دهقان قاه قاه</p></div>
<div class="m2"><p>کافرین ای جنگ جوی رزمخواه</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چون ندیدی مایه خود جنگ من</p></div>
<div class="m2"><p>راه جویی تا رهی از چنگ من</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هرکه را از هم نبرد آمد نهیب</p></div>
<div class="m2"><p>سست آرد در جدل پای شکیب</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ترسمت چون نیست نیروی نبرد</p></div>
<div class="m2"><p>چون زنان زن نه چون مردان مرد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>من نرفته همچنان گامی سه چار</p></div>
<div class="m2"><p>فرض انگاری حدیث الفرار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چونکه در سر داری آهنگ فرار</p></div>
<div class="m2"><p>هان برو ما را به کار خود گذار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چون ز دهقان این شنید آن بند مار</p></div>
<div class="m2"><p>گفت انصافت چه شد ما و فرار؟!</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هرچه می خواهی ز من پیمان بگیر</p></div>
<div class="m2"><p>رو به خانه از پی شمشیر و تیر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>گفت عهد تورا ندارم استوار</p></div>
<div class="m2"><p>عهد و سوگند تو نبود برقرار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>در خروش آمد پلنگ خانگزای</p></div>
<div class="m2"><p>گفت کای بیهوده شیخ ژاژخای</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>هرچه آن سرمایه ی آرام توست</p></div>
<div class="m2"><p>هرچه باشد نزد تو محکم نه سست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>گو چه باشد تا بدان گردن نهم</p></div>
<div class="m2"><p>هرچه می خواهی بگو تن دردهم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>گفت اگر تن می دهی تا سخت سخت</p></div>
<div class="m2"><p>دست و گردن بندمت بر این درخت</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بسته گردد تا تورا پای گریز</p></div>
<div class="m2"><p>دانمت بگشاده بازوی ستیز</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چون پلنگ این نکته اندر گوش کرد</p></div>
<div class="m2"><p>با درختی دست در آغوش کرد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>کاین درخت و من ببندم دست و پای</p></div>
<div class="m2"><p>رو سلاح آور بزودی از سرای</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>مرد سست آزرم از جا جست سخت</p></div>
<div class="m2"><p>بست آن آزاده محکم بر درخت</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بر درختش بست با نیروی پیل</p></div>
<div class="m2"><p>مهر بنهاد و روان بر دسته بیل</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بر سر و پهلو و دوش و پشت و روی</p></div>
<div class="m2"><p>کوفت چندان کز دو تن خون رفت جوی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>مرد را هرگه به بالا بیل رفت</p></div>
<div class="m2"><p>نعره آن جانور صد میل رفت</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>آنکه در سرپنجه مورش بود پیل</p></div>
<div class="m2"><p>پیل پیشش مور شد از بار بیل</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>لابه ها کرد و نیامد سودمند</p></div>
<div class="m2"><p>امن می جست و نبودش جز گزند</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>خرد شد از ضرب بیلش پا و دست</p></div>
<div class="m2"><p>پنجه اش افتاد و بازویش شکست</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>استخوانش شد سراسر ریزریز</p></div>
<div class="m2"><p>مرد با بیل آنچنانش در ستیز</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>هر طرف می کرد از حسرت نظر</p></div>
<div class="m2"><p>دید غیر از گربه کس نبود دگر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>نرم نرمک گربه را آواز کرد</p></div>
<div class="m2"><p>راه و رسم حیله جویی ساز کرد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>کای برادر گرچه من بدآب خورد</p></div>
<div class="m2"><p>چون تو کردم کوچک و مسکین و خورد</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>دست بردارد ز من این سخت پی</p></div>
<div class="m2"><p>یا گشاید بندم از تن گفت نی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>گر شوی کوچکتر از موش ای رفیق</p></div>
<div class="m2"><p>نیست امید خلاصی زین غریق</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ای برادر اهل دنیا سربسر</p></div>
<div class="m2"><p>آدمیزادند و از آدم بتر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>تا توانی میگریز از دامشان</p></div>
<div class="m2"><p>بلکه از جایی که باشد نامشان</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>آدمیزادند اما دیوسار</p></div>
<div class="m2"><p>الفرار از آدمیزاد الفرار</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>تا نیفتادستی اندر بندشان</p></div>
<div class="m2"><p>گردنت تا نیست در آوندشان</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>فارغی از رنجشان و دردشان</p></div>
<div class="m2"><p>عشوه ی جانسوز ناز سروشان</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>نی به پایت بند و نی در لب لویش</p></div>
<div class="m2"><p>پادشاهی هم برایشان هم به خویش</p></div></div>