---
title: >-
    بخش ۲۲۴ - تذکار جوان فقیر خارکن به جهت تفکر در احوال خود
---
# بخش ۲۲۴ - تذکار جوان فقیر خارکن به جهت تفکر در احوال خود

<div class="b" id="bn1"><div class="m1"><p>زان تفکر در خود و ایام خود</p></div>
<div class="m2"><p>اندر آغاز خود و انجام خود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخنه ای اندر دل خود باز کرد</p></div>
<div class="m2"><p>دل از آن رخنه گشودن ساز کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان نشاطش رخنه ای دیگر گشود</p></div>
<div class="m2"><p>در گشادش رخنه هردم می فزود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به دل آن رخنه ها دروازه شد</p></div>
<div class="m2"><p>طبل روحانی بلندآوازه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد ز بندرگاه غیب آنجا روان</p></div>
<div class="m2"><p>کاروان در کاروان در کاروان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس دل بیهوش او آمد بهوش</p></div>
<div class="m2"><p>گفت در گوش دلش آنگه سروش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کانچه می بینی ز عز و مال و جاه</p></div>
<div class="m2"><p>وصل معشوق و نیاز پادشاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دستبوس شاه و پابوس امیر</p></div>
<div class="m2"><p>روی بوس آن نگار دلپذیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سربه سر تأثیر رسم طاعت است</p></div>
<div class="m2"><p>رسم طاعت را چنین خاصیت است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دستمال صورت طاعات توست</p></div>
<div class="m2"><p>مزد طاعتهای بی نیات توست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قیمت کالای روی اندود توست</p></div>
<div class="m2"><p>اجرت سعی غرض آلود توست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میوه ی بید و صنوبرهای توست</p></div>
<div class="m2"><p>سود سوداهای پر صفرای توست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آمدی این دست مزد پای توست</p></div>
<div class="m2"><p>این جواب لفظ بیمعنای توست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این بهای آن مبارک مژده است</p></div>
<div class="m2"><p>این گلاب آن گل پرمژده است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هیچ کاری نزد ما بی اجر نیست</p></div>
<div class="m2"><p>هیچ صبحی نه که او را فجر نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرچه کالای تو بس نابود بود</p></div>
<div class="m2"><p>لیک نزد ما کجا مردود بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خویشتن را وانمودی آن ما</p></div>
<div class="m2"><p>آن ماکی رفته بی احسان ما</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر نه از ما بودی اما ای فتی</p></div>
<div class="m2"><p>پیش مردم خویش را خواندی ز ما</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هین بگیر این مزد صورت کاریت</p></div>
<div class="m2"><p>این ثواب اجر ظاهر داریت</p></div></div>