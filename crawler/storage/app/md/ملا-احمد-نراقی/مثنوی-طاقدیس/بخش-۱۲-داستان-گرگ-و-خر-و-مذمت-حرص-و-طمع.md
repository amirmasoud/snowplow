---
title: >-
    بخش ۱۲ - داستان گرگ و خر و مذمت حرص و طمع
---
# بخش ۱۲ - داستان گرگ و خر و مذمت حرص و طمع

<div class="b" id="bn1"><div class="m1"><p>یک خری افتاد و پای آن شکست</p></div>
<div class="m2"><p>جان آن از زحمت خر بنده رست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چونکه افتاد آن خر مسکین زکار</p></div>
<div class="m2"><p>برگرفت از پشت آن خر بنده بار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بیابانش به دام و دد سپرد</p></div>
<div class="m2"><p>برد باریهای خر از یاد برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اهل دنیا را سراسر ای پسر</p></div>
<div class="m2"><p>همچو آن خر بنده بیشک میشمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بار ایشان تاکشی چون خر بدوش</p></div>
<div class="m2"><p>جمله در دورت بجوشند و خروش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمله بر دور سرایت صبح و شام</p></div>
<div class="m2"><p>منتظر ایستاده از بهر سلام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هریک از ایشان جوالی پر زبار</p></div>
<div class="m2"><p>با خود آورده است بهرت ای حمار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا رباید از سلامی هوش تو</p></div>
<div class="m2"><p>پس گذارد بار خود بر دوش تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بکاوی آنچه می گوید درست</p></div>
<div class="m2"><p>معنیش خر کردن و تسخیر توست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گویدت گر بنده ام تا زنده ام</p></div>
<div class="m2"><p>یعنی ای خر من تو را خر بنده ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با خبر باش آنچه او می گویدت</p></div>
<div class="m2"><p>جو بدامن کرده تا بفریبدت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می نماید جو که گردی رام او</p></div>
<div class="m2"><p>رم نیاری افتی اندر دام او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کم کم آید پیش و گیرد گوش تو</p></div>
<div class="m2"><p>بار خود را پس نهد بر دوش تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روزگاری گر تورا آرد شکست</p></div>
<div class="m2"><p>از قضا برنایدت کاری ز دست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>می نبینی بر در خود دیگرش</p></div>
<div class="m2"><p>گوبیا هرگز نبودی تو خرش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ترک خر گیران کن اکنون ای پسر</p></div>
<div class="m2"><p>تا نکردستند ایشان ترک خر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای تو آسان این خران را خر مشو</p></div>
<div class="m2"><p>رخش سلطانی خر ابتر مشو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بار تکلیف تو بر دوش تو بس</p></div>
<div class="m2"><p>هین مکن سربار آن باری زکس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بار دیگر بر مدار ای بیخبر</p></div>
<div class="m2"><p>داری اندر پیش راهی پر خطر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>راه بس دور و دراز و هولناک</p></div>
<div class="m2"><p>کوه در کوه و مغاک اندر مغاک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سربسر راه تو کوه است و کتل</p></div>
<div class="m2"><p>کوهها سنگینتر از کوه امل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سنگلاخست و کریوه جمله راه</p></div>
<div class="m2"><p>در بیابانش نه آب و نه گیاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر طرف دزدان چابک در کمین</p></div>
<div class="m2"><p>هریکی ره بسته بر چرخ برین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پشته ها از کشته ها در هر کنار</p></div>
<div class="m2"><p>بوی خون می آید از هر نوک خار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اندر آن پیدا نه پیدا جای پای</p></div>
<div class="m2"><p>نه صهیل اسب و نه بانگ درای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هرکه پیش آید که من هستم دلیل</p></div>
<div class="m2"><p>گمرهان را می نمایم من سبیل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عاقبت بینی که غول رهزن است</p></div>
<div class="m2"><p>دیو آدم کش و یا اهرمن است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چونکه این راه ای پسر در پیش ماست</p></div>
<div class="m2"><p>بار سنگین در چنین راهی خطاست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در شتابست اندرین ره کاروان</p></div>
<div class="m2"><p>تو چه خواهی کرد با بار گران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رفته همراهان و تنها مانده ای</p></div>
<div class="m2"><p>غیر درگاهت زهر در رانده ای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در بیابان تن به مردن داده ای</p></div>
<div class="m2"><p>دل به مرگ خویشتن در داده ای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بینوایی مبتلایی خسته ای</p></div>
<div class="m2"><p>صید در دامی شکار بسته ای</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پای من لنگ و ره دشوار پیش</p></div>
<div class="m2"><p>بار من سنگین و پشت ناقه ریش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مرک از دستم عنان بگرفت رفت</p></div>
<div class="m2"><p>دزد آمد آب و نان بگرفت و رفت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کاروان چون رفت و واماندی ز راه</p></div>
<div class="m2"><p>سود کی بخشد تورا افغان و آه</p></div></div>