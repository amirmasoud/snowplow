---
title: >-
    بخش ۱۷۶ - سوزانیدن زنان هندوان خود را با شوهران
---
# بخش ۱۷۶ - سوزانیدن زنان هندوان خود را با شوهران

<div class="b" id="bn1"><div class="m1"><p>یاسه باشد در میان هندوان</p></div>
<div class="m2"><p>هم ز عهد باستان تا این زمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردگان خود بباید سوختن</p></div>
<div class="m2"><p>شعله ها از بهر او افروختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آبنوس و عود و صندل آورند</p></div>
<div class="m2"><p>خرمن اندر خرمن آتش می زنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس بسوزانند جسم مردگان</p></div>
<div class="m2"><p>همچنانکه روحشان در آن جهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواجه ای را چون سر آید زندگی</p></div>
<div class="m2"><p>گرد آیند از پی سوزندگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمع گردد عالمی از خاص و عام</p></div>
<div class="m2"><p>تا بسوزانند آن خواجه همام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک دو تن از خواصگان خلوتش</p></div>
<div class="m2"><p>نوعروسان حریم عزتش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جسم و جان در حضرتش قربان کنند</p></div>
<div class="m2"><p>خویش را بر شعله ی سوزان کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس بیارایند از پا تا به سر</p></div>
<div class="m2"><p>از زر و مرجان و یاقوت و گهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از حریر هند و دیبای فرنگ</p></div>
<div class="m2"><p>پرنیان سرخ و سبز و لاله رنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کرد هر هفت همچو طاوس بهار</p></div>
<div class="m2"><p>لب پر از صد خنده رخ پر از نگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیده ها مکحول و رخها غازه ناک</p></div>
<div class="m2"><p>زلف آشفته گریبان کرده چاک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دست افشان پای کوبان با دلال</p></div>
<div class="m2"><p>سوی آتش ره نوردند با عجال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مؤبدان اندر یمین و در یسار</p></div>
<div class="m2"><p>مطربان در نغمه اندر هر کنار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اینچنین آیند با ناز و خرام</p></div>
<div class="m2"><p>تا بر آن خرمن آتش تمام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تختهای صندل و عود و چنار</p></div>
<div class="m2"><p>برفراز هم چو تخت زرنگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قبه ای از تختها افراشته</p></div>
<div class="m2"><p>خواجه را بر کنگرش بگذاشته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نعش خواجه بر فراز تخت نار</p></div>
<div class="m2"><p>دور آن جای عروسان تتار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چونکه آیند آن عروسان پای تخت</p></div>
<div class="m2"><p>شاد و بر گاو فنا بندند رخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دست همت بر تن و بر جان زنند</p></div>
<div class="m2"><p>گوی هستی را به صد چوگان زنند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگذرند از جسم و از جان رایگان</p></div>
<div class="m2"><p>از برای خواجه ی بیحس و جان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آستین بر چهره عالم زنند</p></div>
<div class="m2"><p>اختلاط جسم و جان بر هم زنند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پس بر آن تخت فنا بالا روند</p></div>
<div class="m2"><p>هریکی در جای خود مأوا کنند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چونکه بنشینند بر آن تخته بست</p></div>
<div class="m2"><p>دل ببرند از خود و از هرچه هست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرگ پیش از مرگشان پیدا شود</p></div>
<div class="m2"><p>چشمشان بینا زبان گویا شود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خنده ی شادی زنند از انبساط</p></div>
<div class="m2"><p>آتش اندر شعله ایشان در نشاط</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پس ببینند آنچه ناید در نظر</p></div>
<div class="m2"><p>بشنوند آنها کز آن نبود خبر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زینت و زیور ز خود دور افکنند</p></div>
<div class="m2"><p>هم ز دست و گوش و گردن بگسلند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پس بسوی هرکسی زان انجمن</p></div>
<div class="m2"><p>رو کنند آیند با وی در سخن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آگهش سازند ز استقبال او</p></div>
<div class="m2"><p>آنچه آید پیش او زاحوال او</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بی تفاوت آنچه گویند آن شود</p></div>
<div class="m2"><p>عقلها از گفتشان حیران شود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اینچنین گویند تا آتش فتاد</p></div>
<div class="m2"><p>هم دهانشان هم زبان برباد داد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چونکه مردند از خود وفانی شدند</p></div>
<div class="m2"><p>آگه از اسرار پنهانی شدند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هندویی از بهر هندو مرده ای</p></div>
<div class="m2"><p>بگذرد از جان نیم افسرده ای</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پرده برچینند از پیشش عیان</p></div>
<div class="m2"><p>تا بر او گردد عیان سر نهان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در ره آن زنده ای سرمد اگر</p></div>
<div class="m2"><p>زنده جانی بگذرد از جان و سر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>من نمی دانم بگویم چون شود</p></div>
<div class="m2"><p>آنچه باید عقل از آن افزون شود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ای عجب بنگر که خیل زندگان</p></div>
<div class="m2"><p>خویش را سوزند بهر مردگان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>می کند آن زنده ی سرمد طلب</p></div>
<div class="m2"><p>از تو جان ندهی عجب ای صد عجب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نیم جانی ده از آن صد جان ستان</p></div>
<div class="m2"><p>پارگین ده چشمه ی حیوان ستان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یکقدم از خود گذر کن ای ودود</p></div>
<div class="m2"><p>پس قدم بگذار در ملک خلود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از تو تا سر منزل جانان تو</p></div>
<div class="m2"><p>یکدوگامی پیش نبود جان تو</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر دو عالم در تو باشد مستتر</p></div>
<div class="m2"><p>تو نداری از خود ای بابا خبر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پادشاهی و گدایی می کنی</p></div>
<div class="m2"><p>گنج داری بینوایی می کنی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تشنه می میری و عمان پیش تو</p></div>
<div class="m2"><p>چیست عمان آب حیوان پیش تو</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گنج اعظم در میان مشت توست</p></div>
<div class="m2"><p>خاتم جم در کهین انگشت توست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تو گدایی می کنی با آه و دود</p></div>
<div class="m2"><p>بر در هر خانه ی گبر و یهود</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سال و مه جان می کنی در حرص و آز</p></div>
<div class="m2"><p>نام آن را مینهی عمر دراز</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>روزها صد جان کنی اندر طلب</p></div>
<div class="m2"><p>تا بکف آری دو نان از بهر شب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پس خوری و فضله سازی نان خویش</p></div>
<div class="m2"><p>روز دیگر جان کنی چون روز پیش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کار تو اینست اندر ماه و سال</p></div>
<div class="m2"><p>گاه در جان کندنی گاهی مبال</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>روزگاران شغلت این است و فنت</p></div>
<div class="m2"><p>زندگانی نبود این جان کندنت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>می کنی جان تا برآید جان تو</p></div>
<div class="m2"><p>ای دریغا درد بیدرمان تو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>در میان چار خصم جنگجوی</p></div>
<div class="m2"><p>کی برد جان ای رفیق نیکخوی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گم غم مغلوبی این گاه آن</p></div>
<div class="m2"><p>چون تو رفتی خواه این و خواه آن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چونکه می رانند آخر ای عزیز</p></div>
<div class="m2"><p>خود بمیر و از غم مردن گریز</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>می توان تا کی غم ایام خورد</p></div>
<div class="m2"><p>زنگ از این غم باید از خاطر سترد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چاره ی این چیست دانی جان من</p></div>
<div class="m2"><p>از وجود خویش بیرون آمدن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کار آتش بایدت آموختن</p></div>
<div class="m2"><p>روز و شب از پای تا سر سوختن</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>عمر رفت و روز رفت و شام رفت</p></div>
<div class="m2"><p>سال رفت و ماه رفت ایام رفت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>دیگران رفتند پیش گنج و ناز</p></div>
<div class="m2"><p>در مقام خانه ی تو پیش باز</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ای تو سرمست هوا هشیار شو</p></div>
<div class="m2"><p>ای تو در خواب هوس بیدار شو</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>پنج روزی پنج و شش خواهد گذشت</p></div>
<div class="m2"><p>خواه ناخوش خواه خوش خواهد گذشت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>صد بهشت و دوزخ اندر راه توست</p></div>
<div class="m2"><p>دیده ی کار آگهان آگاه توست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>عقبها در پیش داری ای پسر</p></div>
<div class="m2"><p>چند چند آخر نشینی بیخبر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گر از آن سویت خبر نی کافری</p></div>
<div class="m2"><p>ور خبر داری و در خوابی خری</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>پس نباشد آنچه گفتند آگهان</p></div>
<div class="m2"><p>از خبرهای جهانی بی نشان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ای برادر مرده ای تو بارها</p></div>
<div class="m2"><p>زندگانی دیده ای بسیارها</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چونکه مردی از جمادی ای پسر</p></div>
<div class="m2"><p>زنده گشتی از نبات سبز و تر</p></div></div>