---
title: >-
    بخش ۵ - به جزیره فرستادن شاه طوطی را
---
# بخش ۵ - به جزیره فرستادن شاه طوطی را

<div class="b" id="bn1"><div class="m1"><p>یک جزیره بود در اقلیم شاه</p></div>
<div class="m2"><p>بود تا پاتخت شه شش ماه راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک جزیره پرگیاه و پر علف</p></div>
<div class="m2"><p>پردرخت میوه دار از هر طرف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجتمع از اهل هر شهر و دیار</p></div>
<div class="m2"><p>اندر آنجا آنچه ناید در شمار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشته جمع آنجا ز اهل هر زبان</p></div>
<div class="m2"><p>خلق انبوه و گروه بیکران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وندران پیری و پیر زنده ای</p></div>
<div class="m2"><p>پیر از علم و ادب آکنده ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیر دانشمند و دانش پروری</p></div>
<div class="m2"><p>دانش آموزی و دانش گستری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکته دانی آگهی از هر زبان</p></div>
<div class="m2"><p>نکته آموزی رفیقی مهربان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در زبان آموزی آن پیر جلیل</p></div>
<div class="m2"><p>بیزبانان را همه گشته دلیل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد قرار پادشاه بی نظیر</p></div>
<div class="m2"><p>کاو فرستد طوی خود نزد پیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سوی آن پیله فرستد مرغ خود</p></div>
<div class="m2"><p>تا بیاموزد زبان نیک و بد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس همه اسباب راهش ساز کرد</p></div>
<div class="m2"><p>بروی از رحمت دو صد درباز کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کرد با او لطفها زاندازه بیش</p></div>
<div class="m2"><p>پس ز رحمت خواند او را نزد خویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دستها مالید بر بال و پرش</p></div>
<div class="m2"><p>بوسه زد از مهربانی بر سرش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>داد او را از عنایت مایه ها</p></div>
<div class="m2"><p>خواند بر او هم ز رحمت آیه ها</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس بگفت ای مرغ خوش آواز من</p></div>
<div class="m2"><p>ای انیس و همدم و همزار من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای نوایت بینوایان را نوا</p></div>
<div class="m2"><p>ای همایون بال برتر از هما</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دوریت را من نمی کردم خیال</p></div>
<div class="m2"><p>ور همی کردم گمال کردم محال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>روز هجرت را به خواب ار دیدمی</p></div>
<div class="m2"><p>یا حدیث دوریت بشنیدمی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دیده را از نشتر غم خستمی</p></div>
<div class="m2"><p>گوش خود را از شنیدن بستمی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دیده چون از روی یاران دور شد</p></div>
<div class="m2"><p>گرچه صد نورش بود بی نور شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گوش محرم از حدیث دلبران</p></div>
<div class="m2"><p>صد صدا گر بشنود کر باشد آن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شرح ایام فراق دوستان</p></div>
<div class="m2"><p>آنچه من گویم تو صد چندان بدان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نامه ام را عالم ار پهنا بود</p></div>
<div class="m2"><p>شرح هجران را کجا گنجا بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سوز هجران را اگر سازم رقم</p></div>
<div class="m2"><p>هم به کاغذ آتش افتد هم قلم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ور بگویم شمه ای از سوز آن</p></div>
<div class="m2"><p>هم زبان سوزد مرا و هم دهان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای صفایی بگذر از این گفتگو</p></div>
<div class="m2"><p>حال شاهنشاه و طوطی را بگو</p></div></div>