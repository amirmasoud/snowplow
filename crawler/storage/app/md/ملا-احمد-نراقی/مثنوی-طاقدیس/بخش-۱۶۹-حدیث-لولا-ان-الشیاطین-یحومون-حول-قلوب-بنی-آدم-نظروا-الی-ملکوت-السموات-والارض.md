---
title: >-
    بخش ۱۶۹ - حدیث لولا ان الشیاطین یحومون حول قلوب بنی آدم نظروا الی ملکوت السموات والارض
---
# بخش ۱۶۹ - حدیث لولا ان الشیاطین یحومون حول قلوب بنی آدم نظروا الی ملکوت السموات والارض

<div class="b" id="bn1"><div class="m1"><p>گفت آن سالار اقلیم شهود</p></div>
<div class="m2"><p>بر روانش باد صد عالم درود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد دلهای بنی آدم اگر</p></div>
<div class="m2"><p>حزب شیطان را نبودی کر و فر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده شان دیدی ملایک را عیان</p></div>
<div class="m2"><p>سیر کردندی زمین و آسمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسمان و خاک دیدندی اسیر</p></div>
<div class="m2"><p>پیش تقدیر خداوند قدیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذره ذره خاک و قطره قطره آب</p></div>
<div class="m2"><p>جمله در فرمان آن والا جناب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم نوای بلبل و هم بانگ زاغ</p></div>
<div class="m2"><p>نغمه ی طوطی و غوغای کلاغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جمله را تسبیح و تقدیس خدا</p></div>
<div class="m2"><p>فهمد او گرچه نمی فهمیم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راویه بر دوش ابر از امر او</p></div>
<div class="m2"><p>رعد غران یک نهیب قهر او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بحر را از او بود موج و قرار</p></div>
<div class="m2"><p>بختی گردون ز حکمش در مهار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آسمان را غاشیه بر دوش از او</p></div>
<div class="m2"><p>اختوران را حلقه اندر گوش ازو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهر مشعل دار شیلان گاه او</p></div>
<div class="m2"><p>انجم هندو بچه درگاه او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آسمان با قد خم از کهکشان</p></div>
<div class="m2"><p>بهر خدمتکاریش بسته میان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باد را جاروب فراشی بکف</p></div>
<div class="m2"><p>می دود از بهر خدمت هر طرف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آب محو است و زند سر بر زمین</p></div>
<div class="m2"><p>می رود گاه از یسار و گه یمین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کوهها ایستاده با هم روبرو</p></div>
<div class="m2"><p>واله و حیران شیدایی او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سرو و شمشاد و صنوبر نارون</p></div>
<div class="m2"><p>صف زده در حضرتش در انجمن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای خوشا چشمی که آن بینای اوست</p></div>
<div class="m2"><p>وی مبارک دل که آن دانای اوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دیده ی دور از جمالش کور باد</p></div>
<div class="m2"><p>سینه ی بی یاد او در گور باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سینه ای کز یاد آن شه خالی است</p></div>
<div class="m2"><p>مرده باشد مرده را گودالی است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زنده آن باشد که از خود رسته شد</p></div>
<div class="m2"><p>در وجود زنده ای پیوسته شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چیست پیوستن به او دل باختن</p></div>
<div class="m2"><p>خویش را در پای او انداختن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دست از تدبیر خود برداشتن</p></div>
<div class="m2"><p>اختیار خود به او بگذاشتن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر همی خواهی حیات خوشگوار</p></div>
<div class="m2"><p>اختیار خود برو با او گذار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آسمان از این امانت تن کشید</p></div>
<div class="m2"><p>کوهها از هستیش بر خود تپید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بود انسان چون ظلوم و چون جهول</p></div>
<div class="m2"><p>لاجرم کرد این امانت را قبول</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این امانت چیست ای یار رفیق</p></div>
<div class="m2"><p>بار تکلیف است در پای عمیق</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>و این نتیجه اختیار قدرت است</p></div>
<div class="m2"><p>اندرین قدرت هزاران آفت است</p></div></div>