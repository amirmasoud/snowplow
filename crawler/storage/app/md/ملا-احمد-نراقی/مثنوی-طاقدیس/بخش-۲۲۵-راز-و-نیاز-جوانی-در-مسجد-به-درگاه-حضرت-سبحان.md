---
title: >-
    بخش ۲۲۵ - راز و نیاز جوانی در مسجد به درگاه حضرت سبحان
---
# بخش ۲۲۵ - راز و نیاز جوانی در مسجد به درگاه حضرت سبحان

<div class="b" id="bn1"><div class="m1"><p>یک جوانی بود در ایام پیش</p></div>
<div class="m2"><p>روزگار آورد بر روزش پریش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد تهی دست و پریشان روزگار</p></div>
<div class="m2"><p>نی به دستش بود کسبی و نه کار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مایه ای نی تا از آن سودی کند</p></div>
<div class="m2"><p>آتشی نه تا از آن دودی کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی کمانی تا شکاری افکند</p></div>
<div class="m2"><p>تخته ای نی تا قماری افکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز و شب در خانه سر در زیر بال</p></div>
<div class="m2"><p>با عیال و جفت خود اندر جدال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه فروش کاسه کردی گه فراش</p></div>
<div class="m2"><p>خانه گاهی رهن کردی گه خماش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا نماندش در سرا غیر از زمین</p></div>
<div class="m2"><p>با یکی همخوابه ی دل آهنین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا شبی گفتش که ای بیکاره مرد</p></div>
<div class="m2"><p>ای تو اندر کاهلی یکتا و فرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بکی اندر سرایی چون زنان</p></div>
<div class="m2"><p>رو برون فردا پی تحصیل نان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر نداری کسب مزدوری خوش است</p></div>
<div class="m2"><p>شاید آید نانی از مزدت به دست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون دگر نانی نمانده در بنه</p></div>
<div class="m2"><p>امشبی خوابیم با هم گرسنه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چونکه فردا شد زن آمد نزد شوی</p></div>
<div class="m2"><p>گفت بیرون رو زبهر جستجوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شد برون آن مرد مسکین از سرا</p></div>
<div class="m2"><p>تا مگر آید گشایش از کجا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در میان کوچه لختی ایستاد</p></div>
<div class="m2"><p>دید او را نامد از جایی گشاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ماند حیران با دل غم از جواب</p></div>
<div class="m2"><p>نی ذهاب او را میسر نی ایاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مسجدی دید آمد آنجا با نیاز</p></div>
<div class="m2"><p>با طهارت کرد رو بهر نماز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در نماز ایستاد آنجا تا به شام</p></div>
<div class="m2"><p>گه رکوع و گه سجود و گه قیام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چونکه شب شد سوی خانه بازگشت</p></div>
<div class="m2"><p>زن از آن جویای کشف راز گشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هین چه آوردی بیاور ای فتی</p></div>
<div class="m2"><p>تا بسازم هم عشا و هم غذا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفت گشتم من در امروز ای سلیم</p></div>
<div class="m2"><p>مزد کار کارفرمای کریم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت با من آن کریم دلفروز</p></div>
<div class="m2"><p>می دهم فردا تورا مزد دو روز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شرم کردم تا طلبکاری کنم</p></div>
<div class="m2"><p>یا سخن در حال خود جاری کنم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زن بگفتا سهل باشد ای جوان</p></div>
<div class="m2"><p>نان بگیرم وام از همسایگان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روز دویم آن جوان باز از وثاق</p></div>
<div class="m2"><p>شد برون چون ماه گردون از محاق</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در کناری ساعتی باز ایستاد</p></div>
<div class="m2"><p>عاقبت رو باز در مسجد نهاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بود آنجا تا به شام اندر نماز</p></div>
<div class="m2"><p>از برای آن کریم کارساز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شد درآمد باز سوی خانه شد</p></div>
<div class="m2"><p>با زن خود بر سر افسانه شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت فردا آن کریم باوفا</p></div>
<div class="m2"><p>می کند مزد سه روزم را عطا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زن در آن شب هم دو قرصی کرد وام</p></div>
<div class="m2"><p>گفت با شوهر به امید تمام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>روز سیم باز آمد آن جوان</p></div>
<div class="m2"><p>از وثاق خویش تا مسجد روان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رو به محراب نماز آورد باز</p></div>
<div class="m2"><p>دیده اش بر راه لطف دوست باز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زن نشسته منتظر اندر سرا</p></div>
<div class="m2"><p>مرد در مسجد به اوراد و دعا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آخر روز از بر یزدان فرد</p></div>
<div class="m2"><p>یک فرشته با سه روزه مزد مرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گوسفندی با یکی خروار آرد</p></div>
<div class="m2"><p>هم برای ذبح حیوان سنگ و کارد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هم یکی انبان آکنده به زر</p></div>
<div class="m2"><p>آمد و زد حلقه دارش را به در</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گفت آن زن با سروش بی نظیر</p></div>
<div class="m2"><p>هین سه روزه مزد شویت را بگیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آن کریم کارفرما داد و گفت</p></div>
<div class="m2"><p>این سه روزه مزد شوت ای نیک جفت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گو به شویت تا بیفزاید به کار</p></div>
<div class="m2"><p>تا فزایم مزد او من بیشمار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آن عطا را زن گرفت و بازگشت</p></div>
<div class="m2"><p>با نشاط و انبساط انباز گشت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آن غنم را کشت و نان را پخته کرد</p></div>
<div class="m2"><p>آنچه باید کرد حاضر بهر مرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>منتظر تا کی درآید در وثاق</p></div>
<div class="m2"><p>آن جوان نیکبخت با وفاق</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مرد چون فارغ شد از فرض عشا</p></div>
<div class="m2"><p>مدتی بنشست در ورد و دعا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بیخبر از آن عطاهای شگرف</p></div>
<div class="m2"><p>کامد است او را از آن دریای ژرف</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پس به سوی خانه ی خود شد روان</p></div>
<div class="m2"><p>زرد روی و خسته تن آزرده جان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گه به سوی آسمان کردی نگاه</p></div>
<div class="m2"><p>باز پیمودی به سوی خانه راه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گاه در فکر جواب جفت خویش</p></div>
<div class="m2"><p>تا چه طرحی ریزد اندر گفت خویش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آمد و بیرون در بنشست زار</p></div>
<div class="m2"><p>هم خجل از اشکم از زن شرمسار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چون ز شب بگذشت پاسی بس دراز</p></div>
<div class="m2"><p>نامد آن بیچاره سوی خانه باز</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>زن پی تفتیش حالش در گشود</p></div>
<div class="m2"><p>دیدش اندر پشت در زار و کبود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گردن کج کرده سر افکنده پیش</p></div>
<div class="m2"><p>واله و حیران به کار و بار خویش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گفتش اینجا از چه هستی منتظر</p></div>
<div class="m2"><p>از چه نایی در درون خانه در</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گفت هستم منتظر اینجا مقیم</p></div>
<div class="m2"><p>تا فرستد مزدم آن شخص کریم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گفت جانا اندرآ در خانه زود</p></div>
<div class="m2"><p>تا ببینی بخشش آن بحر جود</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اندرآ بین موج دریای کرم</p></div>
<div class="m2"><p>می فزاید زان نشاطم دمبدم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>اندرآ و هرکه را خواهی بخوان</p></div>
<div class="m2"><p>کان کریم امشب فرستاده است خوان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کیست برگو با من آن کان کرم</p></div>
<div class="m2"><p>می فزاید زان نشاطم دمبدم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>می دهد مزد سه روز کار تو</p></div>
<div class="m2"><p>می نگنجد آنچه در انبار تو</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گفت ای زن آن کریم مطلق است</p></div>
<div class="m2"><p>از کرمهایش جهان را رونق است</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گیر و ترسا و یهود و بت پرست</p></div>
<div class="m2"><p>جمله را بر خوان او باز است دست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هر دو عالم خوان یغمای وی است</p></div>
<div class="m2"><p>نیک و بد هم غرق نعمای وی است</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>روزها خورشید خوانسالار اوست</p></div>
<div class="m2"><p>هم به شبهای ماه مشعلدار اوست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ابر آزاری یکی سقای او</p></div>
<div class="m2"><p>باد فرش بساط آرای او</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>آسمان بسته میان از کهکشان</p></div>
<div class="m2"><p>بهر خدمتکاریش چون بندگان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>رعد طبالی بود در کوی او</p></div>
<div class="m2"><p>مهر و مه هندویی از مشکوی او</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>روز و شب نوبت چیان بام او</p></div>
<div class="m2"><p>خلق عالم در صلای عام او</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بحر و بر یک سفره ی شیلان اوست</p></div>
<div class="m2"><p>هر که آمد تا ابد مهمان اوست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>طایران اندر فراز شاخها</p></div>
<div class="m2"><p>مار و مور اندر بن سوراخها</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ماهیان در قعر دریاهای ژرف</p></div>
<div class="m2"><p>وحشیان در دشتهای بس شگرف</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دانه چین خرمن احسان او</p></div>
<div class="m2"><p>لقمه خوار مطبخ شیلان او</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>اوست روزی بخش هر جنبنده ای</p></div>
<div class="m2"><p>زندگی بخشای هرجا زنده ای</p></div></div>