---
title: >-
    بخش ۸۳ - بقیه حکایت آن جاهل در مجلس درس شیخ کامل
---
# بخش ۸۳ - بقیه حکایت آن جاهل در مجلس درس شیخ کامل

<div class="b" id="bn1"><div class="m1"><p>گفت چون شرع شریف استی کنون</p></div>
<div class="m2"><p>پای خود را من کشم ای ذوفنون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه را شرع شریف آمد لقب</p></div>
<div class="m2"><p>پاکشیدن پیش او باشد ادب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاشکی زین پیشتر گفتی سخن</p></div>
<div class="m2"><p>وارهانیدمان ز قید هر محن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این زبان آمد دلت را ترجمان</p></div>
<div class="m2"><p>چونکه دل نادان بود مگشا زبان</p></div></div>