---
title: >-
    بخش ۸۱ - تتمه احوال سید انبیا با کودکان عرب
---
# بخش ۸۱ - تتمه احوال سید انبیا با کودکان عرب

<div class="b" id="bn1"><div class="m1"><p>تا بگویم باقی احوال شاه</p></div>
<div class="m2"><p>با گروه کودکان در عرض راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانکه اصحابند در مسجد غمین</p></div>
<div class="m2"><p>چشم اندر راه خیرالمرسلین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مخلصی جوزانکه اصحاب رسول</p></div>
<div class="m2"><p>منتظر بنشسته در مسجد ملول</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون خرید آن شاه عالم خویش را</p></div>
<div class="m2"><p>دامنش از چنگ طفلان شد رها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بحر فارغ شد ز چنگ قطره ها</p></div>
<div class="m2"><p>مهر خود را واخرید از ذره ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت گریان قدر خود نشناختم</p></div>
<div class="m2"><p>هم بهای خویش روشن ساختم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد معین قیمت من بی گزاف</p></div>
<div class="m2"><p>قدر من اینست نبود جای لاف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درهمی ده بیست داد آن کاروان</p></div>
<div class="m2"><p>در بهای یوسف بی مثل و مان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رحمت حق بر روان پاک او</p></div>
<div class="m2"><p>ابر رضوان آب پاش خاک او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قیمت من گرد کانی شد سه چار</p></div>
<div class="m2"><p>بنده ام آری و اینم اعتبار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این چه حلم است ای دو عالم بنده ات</p></div>
<div class="m2"><p>از ثریا تا ثری شرمنده ات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این چه خلق است ای ملایک چاکرت</p></div>
<div class="m2"><p>زلف حورالعین جاروب درت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زیر بار منتت کون و مکان</p></div>
<div class="m2"><p>داده ای تن زیر بار کودکان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قیمت یک تار مویت صد جهان</p></div>
<div class="m2"><p>می کنی خود را بها ده گرد کان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خاک پایت را شرف بر مهر و ماه</p></div>
<div class="m2"><p>اختورانت بندگان در بارگاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آسمان یک خیمه ی کوتاه تو</p></div>
<div class="m2"><p>عرش و کرسی پایه ای از کاه تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یک خریداری نمودی از کرم</p></div>
<div class="m2"><p>بنده کردی هم عرب را هم عجم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یک دهن خواهم به پهنای جهان</p></div>
<div class="m2"><p>یک زبان جاری بود چون عسقلان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا سخن از وصف آن شه سرکنم</p></div>
<div class="m2"><p>حقه گردون پر از عنبر کنم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لیک تا گفت و شنو عالم پر است</p></div>
<div class="m2"><p>زین صدف گوشی است هرجا پر دراست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر نخواهی شرح نور آفتاب</p></div>
<div class="m2"><p>خلق او خشبوتر است از مشک ناب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بلکه نور آفتاب از روی اوست</p></div>
<div class="m2"><p>نافه گر خوشبو شده است از بوی اوست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خور ز روی او کند کسب ضیا</p></div>
<div class="m2"><p>گل رسد از او به صد برگ و نوا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آسمان رفعت ز شأن او گرفت</p></div>
<div class="m2"><p>از وقارش کوه اطمینان گرفت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چشمها عذب از لب شیرین او است</p></div>
<div class="m2"><p>خاک را آرام از تمکین او است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از کف او ابر باران ریز شد</p></div>
<div class="m2"><p>از دم او بحر لؤلؤ خیز شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سرو بهر خدمت او قد کشید</p></div>
<div class="m2"><p>لاله باداغش ز کوهستان دمید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نی اثر از گفت گوی او شنود</p></div>
<div class="m2"><p>کاین جهان را عطر او خوشبوی بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هرکه گوید مدح آن سلطان دین</p></div>
<div class="m2"><p>او ستایش می کند خود را یقین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یعنی ای یاران دلم داناستی</p></div>
<div class="m2"><p>روی او را دیده ام بیناستی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هم من از پیرایه داران شهم</p></div>
<div class="m2"><p>شه شناسم سرّ شه را آگهم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون تو گویی مدح خورشید جهان</p></div>
<div class="m2"><p>می نیفزاید کمال خور از آن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>می شود روشن از آن بینایی ات</p></div>
<div class="m2"><p>امتیاز ظلمت نورانی ات</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>می ستایی دیدگان خود یقین</p></div>
<div class="m2"><p>دیده ی من روشن است از نور بین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همچنین هرکس که زیبایی ستود</p></div>
<div class="m2"><p>شرح زیبایی خود را وانمود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>داد از صدق حواس خود خبر</p></div>
<div class="m2"><p>یعنی ای یاران نه من کورم نه کر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هم دلم دانا و جانم روشن است</p></div>
<div class="m2"><p>گفتهای من گواهان منست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همچنان که آن دگر یک کز گزاف</p></div>
<div class="m2"><p>از قبول و رد شد اندر اعتساف</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر نکوهش کرد گر خواند آفرین</p></div>
<div class="m2"><p>نی مطابق بود او با حق نه این</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خویشتن را ضایع و نابود کرد</p></div>
<div class="m2"><p>نزد دانا خویش را مردود کرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گفتهای او گواه عیب او ست</p></div>
<div class="m2"><p>از توراوش فهمی آب اندر سبو ست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بوی پنبه سوزت آمد در دماغ</p></div>
<div class="m2"><p>دانی اندر جامه ات افتاد کاغ</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>عالمی در محفل تدریس بود</p></div>
<div class="m2"><p>در مسائل ماهر و بی ویس بود</p></div></div>