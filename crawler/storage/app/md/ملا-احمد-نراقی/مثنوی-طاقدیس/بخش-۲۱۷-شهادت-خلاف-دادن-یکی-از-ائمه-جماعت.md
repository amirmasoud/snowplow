---
title: >-
    بخش ۲۱۷ - شهادت خلاف دادن یکی از ائمه جماعت
---
# بخش ۲۱۷ - شهادت خلاف دادن یکی از ائمه جماعت

<div class="b" id="bn1"><div class="m1"><p>آن یکی را بود وامی بر کسی</p></div>
<div class="m2"><p>از اجل بگذشته بود آن را بسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی آمد در تقاضا و طلب</p></div>
<div class="m2"><p>هم سجل بر کف گواهش بر عقب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت واپس دادم و دارم گواه</p></div>
<div class="m2"><p>خانه ی قاضی است فردا وعده گاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این بگفت و راه مسجد کرد ساز</p></div>
<div class="m2"><p>دید امامی با جماعت در نماز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سرش عمامه ای چون گرز سام</p></div>
<div class="m2"><p>در برش هم جبه ای چون سیم خام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبحه صد دانه اش در پیش رو</p></div>
<div class="m2"><p>ریشه عمامه اش زیر گلو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمد و اندر صف اول نشست</p></div>
<div class="m2"><p>با امام اندر نماز احرام بست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون شدند آن قوم فارغ از نماز</p></div>
<div class="m2"><p>پیش محراب آمد و با صد نیاز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوسه زد بر دست مولانا نخست</p></div>
<div class="m2"><p>وانگهی گفت ای امام دین درست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امشبی خواهم مرا منت نهی</p></div>
<div class="m2"><p>از قدومت کلبه ام زینت دهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بره ای در خانه دارم شیرمست</p></div>
<div class="m2"><p>هم برنج و قد و نان و میوه هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکشبی با مخلصان آری به سر</p></div>
<div class="m2"><p>هم مؤذن در رکابت هم پسر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت بالعین ای عزیز خوش لقا</p></div>
<div class="m2"><p>وعده را حتم است بر مؤمن وفا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این بگفت و رفت از نزد امام</p></div>
<div class="m2"><p>پس به سویش بازگشت از چند گام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت رازی دارم ای دانای راز</p></div>
<div class="m2"><p>رخصت ار باشد بگویم با نیاز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>می روم فردا سوی دارالقضا</p></div>
<div class="m2"><p>با فلانکس بهر آن سیصد طلا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت تا اکنون نداده است آن لکع</p></div>
<div class="m2"><p>واپست آن زر فبئس ماصنع</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت نی نی او طلبکار من است</p></div>
<div class="m2"><p>در کف او قید ماهار من است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>او ز من دارد سجل معتبر</p></div>
<div class="m2"><p>از من او جوید نصاب سیم و زر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفت خواهد از تو زر گیرد دوبار</p></div>
<div class="m2"><p>ای زهی بی شرمی و جور و قمار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت آری ای امام راستگو</p></div>
<div class="m2"><p>گر توانی چاره ی کارم بجو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفت با کی نی روم باکش و فش</p></div>
<div class="m2"><p>سوی دارالشرع فردا غم مکش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هم مؤذن شاهد است و هم پسر</p></div>
<div class="m2"><p>رو تو ایمن کن مهیا ماحضر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روز دیگر نزد قاضی آن غریم</p></div>
<div class="m2"><p>رفت و دعوا کرد بی تشویق و بیم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن دگر گفتا که دادم وام او</p></div>
<div class="m2"><p>گفت قاضی گر گواهت هست کو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ناگهان از در درآمد مولوی</p></div>
<div class="m2"><p>سبحه بر کف لب پر از ذکر جلی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از قدومش قاضی آمد در طرب</p></div>
<div class="m2"><p>هم مؤذن هم پسر اندر عقب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رفت اندر صدر ایوان قضا</p></div>
<div class="m2"><p>جا گرفت و دم زد از صبر و رضا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پس احادیث معنعن یاد کرد</p></div>
<div class="m2"><p>اهل مجلس را بسی ارشاد کرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفت مدیون ایها القاضی الامین</p></div>
<div class="m2"><p>از گواهانم یکی اینست این</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کرد قاضی چون سؤال از آن حمیم</p></div>
<div class="m2"><p>گفت مولانا کجا شد آن غریم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرد مسکین پیش او برپای خاست</p></div>
<div class="m2"><p>با تنحنح مولوی بنشست راست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گفت نی بتوان شهادت را نهفت</p></div>
<div class="m2"><p>لیک نتوانم سخن بیفکر گفت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>من ندانم قدر لکن ای مهان</p></div>
<div class="m2"><p>جزو جزو واقعه سازم بیان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>روز جمعه نیز در مسجد نخست</p></div>
<div class="m2"><p>داد او را در حضورم صد درست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>صبح شنبه داد هفتاد و یکی</p></div>
<div class="m2"><p>عصر هشتاد و چهارش بیشکی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>صبح یکشنبه چهل دادش تمام</p></div>
<div class="m2"><p>وقت پیشین یا دو یا سه والسلام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بایدم مردن نمی دانم دگر</p></div>
<div class="m2"><p>داده گر چیزی ندارم من خبر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آن غریم بینوا گفت ای امام</p></div>
<div class="m2"><p>داد باقی را حسابم شد تمام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون که مردن هست آیین ای عمود</p></div>
<div class="m2"><p>پس چه می بودی اگر مردن نبود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مرده بودی کاش دهری پیش از این</p></div>
<div class="m2"><p>تا ز مرگت زنده گشتی شرع دین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زنده گردد دین ز مرگ چون تویی</p></div>
<div class="m2"><p>کاش نبود در جهان یک مولوی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>داد از این دستاربندان داد داد</p></div>
<div class="m2"><p>رفت شرع و ملت از ایشان به باد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آنچه با دین خامه ایشان کند</p></div>
<div class="m2"><p>کی دم شمشیر بدکیشان کند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کاش نوک خامه شان بشکسته باد</p></div>
<div class="m2"><p>وین زبانهاشان ز گفتن بسته باد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کاش درسی می نخواندی از نخست</p></div>
<div class="m2"><p>چونکه خواندی کاش می خواندی درست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>یا کتاب حکم خود را گم کنید</p></div>
<div class="m2"><p>یا ترحم بر خود و مردم کنید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>حکم یزدان را رجال دیگر است</p></div>
<div class="m2"><p>شهر دین را کوتوال دیگر است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نزد حکم حق همه تسلیم شو</p></div>
<div class="m2"><p>در منی چون آل ابراهیم شو</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چونکه حکم حق رسد مردانه باش</p></div>
<div class="m2"><p>از زن و فرزند خود بیگانه باش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>راستی غیر از خدا بیگانه است</p></div>
<div class="m2"><p>گر زن و فرزند و گر جانانه است</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آنکه باشد اقرب از حبل الورید</p></div>
<div class="m2"><p>کس از این نزدیکتر چیزی شنید</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همرهت زآغاز تا انجام توست</p></div>
<div class="m2"><p>همدم صبح و انیس شام توست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>با تو در اصلاب و در ارحام بود</p></div>
<div class="m2"><p>با تو در آغاز و در انجام بود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>آشنا و خویش و مولای تو اوست</p></div>
<div class="m2"><p>همدم و همراز و همرای تو اوست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>آنچه داری یکسره از او بود</p></div>
<div class="m2"><p>آب بحر است آنچه اندر جو بود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>آنچه می خواهد تو هم می خواه آن</p></div>
<div class="m2"><p>آب دریا را به دریا کن روان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>مال و جسم و جان و فرزند و تبار</p></div>
<div class="m2"><p>جمله را در راه آن شه کن نثار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همچو ابراهیم کاول مال داد</p></div>
<div class="m2"><p>پس تن اندر شعله ی جوال داد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>اینک آمد نوبت فرزند او</p></div>
<div class="m2"><p>کند از دل ریشه ی پیوند او</p></div></div>