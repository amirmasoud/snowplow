---
title: >-
    بخش ۱۳ - مناجات با قاضی الحاجات
---
# بخش ۱۳ - مناجات با قاضی الحاجات

<div class="b" id="bn1"><div class="m1"><p>ای خدا من رهروی ام ناتوان</p></div>
<div class="m2"><p>بیکسی وامانده ای از کاروان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی مرا زاد و نه مرکب نی رفیق</p></div>
<div class="m2"><p>چون بپیمایم خدایا این طریق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی ره برگشتن و نی راه زیست</p></div>
<div class="m2"><p>روی رفتن هم نه یا رب چاره چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اول منزل به گل افتاده بار</p></div>
<div class="m2"><p>دزدهای رهزنم گشته دوچار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مانده ام من ای سواران همتی</p></div>
<div class="m2"><p>بر من پیر ای جوانان رحمتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کرم ای ره به منزل بردگان</p></div>
<div class="m2"><p>رحمتی بر این بیابان مردگان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای امیر کاروان آخر ببین</p></div>
<div class="m2"><p>مرکبم لنگیده بارم بر زمین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیئی لله ای امیر کاروان</p></div>
<div class="m2"><p>پای من لنگست و بار من گران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیئی لله ای سبکباران مدد</p></div>
<div class="m2"><p>بار من سنگین و ره پر دزد و دد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اند کی ای همرهان آهسته تر</p></div>
<div class="m2"><p>گاهگاهی بر فغانم یک نظر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گاهگاهی یک نگاهی بر فقا</p></div>
<div class="m2"><p>از ترحم سوی این پیر دوتا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کز عقب آمد پیاده پای لنگ</p></div>
<div class="m2"><p>بار سنگینش به دوش و پای لنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>الغیاث ای اهل همت الغیاث</p></div>
<div class="m2"><p>ای نگهبانان امت الغیاث</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>الغیاث ای خضر ره گم کردگان</p></div>
<div class="m2"><p>الغیاث ای تو نگهبان جهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای خلیفه حق و ای سلطان دین</p></div>
<div class="m2"><p>دیده بگشا سوی گمراهان ببین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چند باشد آفتابت در حجاب</p></div>
<div class="m2"><p>پرده بردار از رخ چون آفتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صحن این ظلمت سرا را نور بخش</p></div>
<div class="m2"><p>سوگواران جهان را سور بخش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تیره خاکش رشک آذرپوش کن</p></div>
<div class="m2"><p>پر ز آذریون و آذرپوش کن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر خر خود بر نشان دجال را</p></div>
<div class="m2"><p>عزل کن از عالم این عمال را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ظلم و ظلمت را ز عالم پاک کن</p></div>
<div class="m2"><p>سینه ی سفیانیان را چاک کن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>الغرض آن خر بصحرا اوفتاد</p></div>
<div class="m2"><p>دیده ی حسرت بهر سو می گشاد</p></div></div>