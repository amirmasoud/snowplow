---
title: >-
    بخش ۱۲۰ - در مناجات با قاضی الحاجات و استغاثه به آستان امام عصر ع
---
# بخش ۱۲۰ - در مناجات با قاضی الحاجات و استغاثه به آستان امام عصر ع

<div class="b" id="bn1"><div class="m1"><p>ای خدا ای رهنمای گمرهان</p></div>
<div class="m2"><p>ای تو دانا هم به پیدا و نهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای تو پیدا جز تو ناپیدا همه</p></div>
<div class="m2"><p>وی تو بینا جز تو نابینا همه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای منزه از چه و از چند و چون</p></div>
<div class="m2"><p>وی فزون از وهم و زندیشه برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای صفات ذات و ای ذات صفات</p></div>
<div class="m2"><p>ای به تو قائم بقای کاینات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای تو هست مطلق و صرف وجود</p></div>
<div class="m2"><p>ای بر ما غیب و در واقع شهود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دلیل راه هر گم کرده راه</p></div>
<div class="m2"><p>ای ضیابخش چراغ مهر و ماه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندرین پیدای ناپیدا کران</p></div>
<div class="m2"><p>من یکی گم کرده را هم ناتوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راه روشن من ز ره افتاده ام</p></div>
<div class="m2"><p>هم عنان خود به رهزن داده ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راه بس هموار و روشن یکسره</p></div>
<div class="m2"><p>من فتادستم به صد کوه و دره</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دره های پر ز غول راهزن</p></div>
<div class="m2"><p>گرد گشته جملگی بر دور من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می کشندم رهزنان در هر کنار</p></div>
<div class="m2"><p>در تلال و در دهار و کوهسار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>راه بینم روشن و فاش و عیان</p></div>
<div class="m2"><p>وندران بس کاروان در کاروان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چشم من بر راه و گوشم بر درای</p></div>
<div class="m2"><p>من ز راه افتاده ام ایوای وای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>راه می بینم به صد حسرت ز دور</p></div>
<div class="m2"><p>وندر آنجا کاروانها در عبور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کاروانها لطف حق سنگارشان</p></div>
<div class="m2"><p>گوش من بر هی هی نژ غارشان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کاروانها جمله ایمن از فریش</p></div>
<div class="m2"><p>کاروانسالارها در پیش پیش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کاروان در راه هموار و فراخ</p></div>
<div class="m2"><p>من اسیر راهزن در سنگلاخ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>می برندم هر دم از ره دورتر</p></div>
<div class="m2"><p>می زنندم گه به کوه و گه کمر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می کشندم ای خدا بر خار و خس</p></div>
<div class="m2"><p>می زنندم بر قفا از پیش و پس</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یا غیاث المستغیثین الغیاث</p></div>
<div class="m2"><p>ای نشاط جان غمگین الغیاث</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>الغیاث ای بیکران دریای لطف</p></div>
<div class="m2"><p>الغیاث ای موج توفان خیز لطف</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>می برد رهزن ز راهم دور دور</p></div>
<div class="m2"><p>دست بسته پا شکسته لوت و عور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بنگرید ای کاروان سالارها</p></div>
<div class="m2"><p>ای شما در روز و شب بیدارها</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای شبان تیره دور از رویتان</p></div>
<div class="m2"><p>ای معطر دشت و کوه از بویتان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مانده ام تنها رفیقان همتی</p></div>
<div class="m2"><p>رهزنم برد ای دلیران همتی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دور گشتم گم شد آواز جرس</p></div>
<div class="m2"><p>ای امیر کاروان فریاد رس</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای امیر کاروان واپسین</p></div>
<div class="m2"><p>ای وجودت لنگر چرخ و زمین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای دلیل راه هر گم کرده راه</p></div>
<div class="m2"><p>بی پناهان جهان را تو پناه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای تو سالار زمین و آسمان</p></div>
<div class="m2"><p>پادشاه دوره ی آخر زمان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>من یکی وامانده ی از کاروان</p></div>
<div class="m2"><p>در عقب افتاده لنگ و ناتوان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در میان رهزنان ماندم اسیر</p></div>
<div class="m2"><p>گه به بالا می کشندم گه به زیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرکبم بردند و آبم ریختند</p></div>
<div class="m2"><p>خون من با خاک ره آمیختند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر کنار ره نگاهی باز کن</p></div>
<div class="m2"><p>این کنار افتاده را آواز کن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از تو یک آواز و از من صد جواب</p></div>
<div class="m2"><p>از تو خواندن سوی خود از من شتاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای تو کشتیبان دریای شگرفت</p></div>
<div class="m2"><p>ناخدای کشتی این یمّ ژرف</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کشتیم بشکست و من گشتم غریق</p></div>
<div class="m2"><p>اندرین دریای ذخار و عمیق</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>می برد موجم همی بالا و زیر</p></div>
<div class="m2"><p>دست من گیر ای خدایت دستگیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ده نجات عاجز بیدست و پا</p></div>
<div class="m2"><p>از هلاک ای ناخدا بهر خدا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای شبان مهربان بنگر به دشت</p></div>
<div class="m2"><p>یک بز لاغر جدا از گله گشت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دور او بگرفته گرگان بیشمار</p></div>
<div class="m2"><p>حمله بر وی آورند از هر کنار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>رحمتی کن ای شبان مهربان</p></div>
<div class="m2"><p>این بز لاغر ز گرگان وا رهان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ای امین حق شه دنیا و دین</p></div>
<div class="m2"><p>ای تو خیرالمرسلین را جانشین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نک صفایی بنده ی درگاه توست</p></div>
<div class="m2"><p>افسر فخرش ز خاک راه توست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خود غلام توست ای سلطان راد</p></div>
<div class="m2"><p>زادگانش بندگان خانه زاد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>التفاتی کن بسوی این غلام</p></div>
<div class="m2"><p>نام او بس باشد او نبود تمام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از کرم کن خانه زادان را قبول</p></div>
<div class="m2"><p>چاکرانند از فروع و از اصول</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>التفاتی از تو خواهم والسلام</p></div>
<div class="m2"><p>تا کنم نقل زلیخا را تمام</p></div></div>