---
title: >-
    بخش ۲۷ - رنج و محنت اهل زرع و تجارت و ثروت
---
# بخش ۲۷ - رنج و محنت اهل زرع و تجارت و ثروت

<div class="b" id="bn1"><div class="m1"><p>دیگری در کار زرع است و شیار</p></div>
<div class="m2"><p>نی شبش آرام و نی روزش قرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذرد گر در هوا فوجی جراد</p></div>
<div class="m2"><p>او همی خواند اعوذ وان یکاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز در کناسی و سرگین کشی</p></div>
<div class="m2"><p>شب ز بیم عاقبت دل آتشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشت من امسال آیا چون شود</p></div>
<div class="m2"><p>دخل آن از پارکاش افزون شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی کجا گردد فزون باران کجاست</p></div>
<div class="m2"><p>کشت من امسال می دانم هباست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه گرفتار تقاضای خراج</p></div>
<div class="m2"><p>گه از او دیوانیان خواهند باج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد بهار گشت و وقت بوستان</p></div>
<div class="m2"><p>وقت گل گشت و چمن با دوستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواجه ی ما را کجا باشد فراغ</p></div>
<div class="m2"><p>کی تواند کرد سیر دشت و باغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رنج باغ از خواجه سیر از این و آن</p></div>
<div class="m2"><p>کشت دشت از او و گشت از دیگران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وان دگر تاجر نه شب دارد نه روز</p></div>
<div class="m2"><p>نی بیاد آمد به بهمن نی تموز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گه سوی آن شهر و گاهی سوی این</p></div>
<div class="m2"><p>گه به روم و گه به هند و گه به چین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بهر دانگی در تلاطم روز و شب</p></div>
<div class="m2"><p>می نیاساید زمانی از طلب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در تکادو تا به شبها روزها</p></div>
<div class="m2"><p>شب ز فکر بیهده در سوزها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گه خیال سود و سوداهای خام</p></div>
<div class="m2"><p>گاه در دل با شریکان در خصام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گه حساب مایه گه سود و زیان</p></div>
<div class="m2"><p>گه خیال و حجره و بیم دکان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حجره را دزدی مگر خواهد برید</p></div>
<div class="m2"><p>یا بخواهد ساخت قفلش را کلید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من چه خواهم کرد با مشتی عیال</p></div>
<div class="m2"><p>چون کنم با بچه های خردسال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>طفلهای نان خور حق ناشناس</p></div>
<div class="m2"><p>وین زنان بی حقوق نا سپاس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من چه خواهم کرد با فرزند و زن</p></div>
<div class="m2"><p>بایدم ناچار رفتن از وطن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>و آن یکی محبوس تزویر و ریا</p></div>
<div class="m2"><p>و آن دگر در قید تلبیس و دغا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اهل دنیا را بدینگون حالها</p></div>
<div class="m2"><p>روزهاشان بگذرد در سالها</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هیچشان از روز مردن یاد نی</p></div>
<div class="m2"><p>جسمشان جز قلعه فولاد نی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرگ باشد حق ولی همسایه را</p></div>
<div class="m2"><p>خانه آید بر سر اما دایه را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>من کجا و مردن ای مرد خبیر</p></div>
<div class="m2"><p>مردن چون من کسی آسان مگیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>این تنم را قلعه ی فولاد بین</p></div>
<div class="m2"><p>چار ارکانم قوی بنیاد بین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خواجه در خوابست ناگه موشکی</p></div>
<div class="m2"><p>می جهد بیرون ز سوراخ اندکی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خواجه پندارد که دزدی می رود</p></div>
<div class="m2"><p>می جهد از خواب و بیرون می دود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سر برهنه می دود بالا و زیر</p></div>
<div class="m2"><p>هی بگیر و هی بگیر و هی بگیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دزد آمد خانه را تاراج کرد</p></div>
<div class="m2"><p>مایه ام برد و مرا محتاج کرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جمع آیید ای همه همسایگان</p></div>
<div class="m2"><p>الامان و الامان و الامان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صبح آمد خواجه ی ما بستری ست</p></div>
<div class="m2"><p>هی چرا این خواجه از صحت بری ست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هی چه شد او را چرا تب کرده است</p></div>
<div class="m2"><p>خواجه گویا دوش سرما خورده است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هی بخوانید این طبیب و آن طبیب</p></div>
<div class="m2"><p>هی بیارید این دوا را بی شکیب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>روز دیگر شد فغان و شیونست</p></div>
<div class="m2"><p>چیست هی خواجه بکار مردنست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هی چه شد تابوت و کافور و کفن</p></div>
<div class="m2"><p>هی کجا غسال و مقری قبر کن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خواجه پنهان در لحد گردید زود</p></div>
<div class="m2"><p>خود تو گویی خواجه ای هرگز نبود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خواجه آخر با همه باد و بروت</p></div>
<div class="m2"><p>با کمال اینکه حی لایموت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از صدای پای موشی جانسپرد</p></div>
<div class="m2"><p>موشکی بیرون دوید و خواجه مرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای تفو بر اینچنین ماتمکده</p></div>
<div class="m2"><p>اف بر این منزلگه دیو و دده</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>الفرار این غافلان زین دیو جای</p></div>
<div class="m2"><p>الحذر ای غافلان زین غم سرای</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جای منزل نیست اینجا ای پسر</p></div>
<div class="m2"><p>سیل و صرصر را در این باشد گذر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رهگذار سیل را خانه مکن</p></div>
<div class="m2"><p>پیش صرصر خرمنت دانه مکن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در ره این سیل نتوان ساخت باغ</p></div>
<div class="m2"><p>پیش این صرصر مکن روشن چراغ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سقف اشکسته است در زیرش مخواب</p></div>
<div class="m2"><p>خانه گردد بر سرت ناگه خراب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کشتی و توفان و بس دریا عمیق</p></div>
<div class="m2"><p>خویش را بیرون فکن زود ای رفیق</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سرکش است اسب و به ره کوه و کمر</p></div>
<div class="m2"><p>هین از این مرکب فرود آی زودتر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>می نپندارم ولیکن ای عمو</p></div>
<div class="m2"><p>کایی از مرکب ز پند من فرو</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>می نپندارم که این اندرز و پند</p></div>
<div class="m2"><p>رخنه سازد در دلت ای ارجمند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>می نپندارم پذیرایی سخن</p></div>
<div class="m2"><p>بل ملول و تیره می گردی ز من</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کام طبعت زین سخنهای چودر</p></div>
<div class="m2"><p>تلخ می گردد بلی الحق مر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ای بسی را دان شیرین کارها</p></div>
<div class="m2"><p>بیش از این گفتند از این گفتارها</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بارها بشنیده ای گفتارشان</p></div>
<div class="m2"><p>دیده ای هم کارشان و بارشان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نی عیان بخشید سودت نی خبر</p></div>
<div class="m2"><p>نی اثر از عین دیدی نز اثر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پس کجا سودت دهد گفتار من</p></div>
<div class="m2"><p>باز دارد کی تورا انکار من</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بهتر آن باشد که بر بندم زبان</p></div>
<div class="m2"><p>این سخن بگذارم اکنون در میان</p></div></div>