---
title: >-
    بخش ۹۰ - در بیان اینکه وهم از حزب شیطان است
---
# بخش ۹۰ - در بیان اینکه وهم از حزب شیطان است

<div class="b" id="bn1"><div class="m1"><p>مخلصی از این مراحل باز جوی</p></div>
<div class="m2"><p>کار وهم و قلعه ی دل را بگوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روح را بهر خلافت پادشاه</p></div>
<div class="m2"><p>جانب جسمانیان بنموده راه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهر بند تن نشیمن گاه او</p></div>
<div class="m2"><p>صفه ی دل مستقرگاه او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل را گفتا مهین دستور باش</p></div>
<div class="m2"><p>شرع را گفتا چراغ نور باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای غضب تو شحنه ی درگاه باش</p></div>
<div class="m2"><p>ای حیا تو پرده ی خرگاه باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هان و هان ای وهم حیلت گر تو هم</p></div>
<div class="m2"><p>حیله جو در جلب خیر و دفع غم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیزبینی تو دقایق باز جوی</p></div>
<div class="m2"><p>چونکه جستی با خلیفه بازگوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای خیال احوال را تحویل گیر</p></div>
<div class="m2"><p>حافظ تو شغل گنجوری پذیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای نیاز و عجز و اقرار و فغان</p></div>
<div class="m2"><p>ناله و زاری آب دیدگان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جملگی باشید نزد پادشاه</p></div>
<div class="m2"><p>از گناه این خلیفه عذرخواه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای جوارح راه خدمت پیش گیر</p></div>
<div class="m2"><p>ای قوی دامان خدمت را بگیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جملتان اندر بر آن روح پاک</p></div>
<div class="m2"><p>سر گذارید از پی خدمت به خاک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هین مکن پی شور عقل ای روح کار</p></div>
<div class="m2"><p>کار را با عقل دانا دل گذار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هین تو هم ای عقل راه شرع گیر</p></div>
<div class="m2"><p>آنچه شرع آگهت گوید پذیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جمله تان شهزاده را یاری کنید</p></div>
<div class="m2"><p>قلعه ی دل را نگهداری کنید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مستقرگاه روح است این حصار</p></div>
<div class="m2"><p>باز داریدش ز دشمن زینهار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دشمنان هستند افزون از شمار</p></div>
<div class="m2"><p>در کمین بنشسته در دور حصار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا بگیرند این حصار بی نظیر</p></div>
<div class="m2"><p>تا نمایند آن خلیفه حق اسیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عقل را در چاه ظلمانی کنند</p></div>
<div class="m2"><p>پس در آن اقلیم ویرانی کنند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عقل را دور افکنند از بزم روح</p></div>
<div class="m2"><p>پس بر او بندند ابواب فتوح</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هم ببندندش ره افلاک را</p></div>
<div class="m2"><p>هم ره آمد شد املاک را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دور او گیرند پس اهریمنان</p></div>
<div class="m2"><p>سوی خود خوانند او را هر زمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا بسوی خود کشند آن شاه را</p></div>
<div class="m2"><p>منخسف سازند چهر ماه را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پس وزیر و پیشکار و پاسبان</p></div>
<div class="m2"><p>دیو اهریمن کنندش ای فغان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرکه زان دیوان کلان و زفت تر</p></div>
<div class="m2"><p>بهر دستوری همی بندد کمر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خوی او در روح ظاهرتر شود</p></div>
<div class="m2"><p>روح قدسی دیو را مظهر شود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آن یکش دستور دیو و حرص و آز</p></div>
<div class="m2"><p>وان دگر یک دیو امید دراز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وان یکی اهریمن کبر و غرور</p></div>
<div class="m2"><p>شد وزیرش کرد آثارش ظهور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن دگر را دیو شهوت رهنما</p></div>
<div class="m2"><p>وان دگر را دیو مکر و حیله ها</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آن یکی اهریمن ظلمش وزیر</p></div>
<div class="m2"><p>وان ز دیو غل و غش منت پذیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن یکی دیو خیانت رهزنش</p></div>
<div class="m2"><p>وان نفاق اموزد از اهریمنش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هریکی را زین نمط دیوی لعین</p></div>
<div class="m2"><p>می شود در ملک دستور مهین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عاقبت آن همنشینی و وداد</p></div>
<div class="m2"><p>می شود منجر به وصل و اتحاد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خود شود آن روح دیوی و چه دیو</p></div>
<div class="m2"><p>هم ز مکرش جمله دیوان در غریو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>روح گردد عاقبت اهریمنی</p></div>
<div class="m2"><p>بلکه صد اهریمن از مکر افکنی</p></div></div>