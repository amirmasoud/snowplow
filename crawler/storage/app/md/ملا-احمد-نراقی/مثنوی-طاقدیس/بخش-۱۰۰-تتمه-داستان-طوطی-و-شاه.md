---
title: >-
    بخش ۱۰۰ - تتمه داستان طوطی و شاه
---
# بخش ۱۰۰ - تتمه داستان طوطی و شاه

<div class="b" id="bn1"><div class="m1"><p>خورد چون طوطی فریب زاغ شوم</p></div>
<div class="m2"><p>روز و شب شد هم نشین با فوج بوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت از یادش نوای طوطیان</p></div>
<div class="m2"><p>شد زبانش بسته زان نطق و بیان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از گل و از گلستان دلگیر شد</p></div>
<div class="m2"><p>از نبات و قند و شکر سیر شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرد ایوان شهان را خیر یاد</p></div>
<div class="m2"><p>شد درخت و باغ و بستانش زیاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بال رنگارنگ او بر باد رفت</p></div>
<div class="m2"><p>بال افشاندن ورا از یاد رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جا گرفت اندر خرابی تنگ و تار</p></div>
<div class="m2"><p>بسترش گه خاک و گه خاشاک و خار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خرابی بی پر افتاده به خاک</p></div>
<div class="m2"><p>داده تن تنها و بی کس در هلاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنج تنهایی فتاده لوت و عور</p></div>
<div class="m2"><p>دور او بگرفته هر سومار و مور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بال و پر بشکسته پیکر ناتوان</p></div>
<div class="m2"><p>دیده ها بر کنده ببریده دهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قصه ی این طوطی مسکین درست</p></div>
<div class="m2"><p>سربسر احوال مرغ جان توست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن جزیره این جهان بیوفاست</p></div>
<div class="m2"><p>وندران آن پیر دانا عقل ماست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن هواها فوج بومند اندر آن</p></div>
<div class="m2"><p>در کمین بنشسته بهر مرغ جان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اهل دنیا دیو و اهریمن همه</p></div>
<div class="m2"><p>همچو بومانند اندر دمدمه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز اهل دنیا ای برادر الحذار</p></div>
<div class="m2"><p>الحذار از این گروه دیوسار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تابکی با اهل دنیا در خصام</p></div>
<div class="m2"><p>روز و شب از بهر دانگی از حرام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نی خبرداری ز مبدء نی معاد</p></div>
<div class="m2"><p>نی کنی از روز مرگ خویش یاد</p></div></div>