---
title: >-
    بخش ۱۸ - قرار گرفتن دل در جانب چپ
---
# بخش ۱۸ - قرار گرفتن دل در جانب چپ

<div class="b" id="bn1"><div class="m1"><p>سینه ات را روشنی بینی ز راست</p></div>
<div class="m2"><p>هم گشادش را از آنجا ابتداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وسعتی در سینه بینی از یمین</p></div>
<div class="m2"><p>کاندران هفت آسمان باشد دفین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در یمین سینه ات این انبساط</p></div>
<div class="m2"><p>بنگری چپ را بود سم الخیاط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ظلمت چپ آن زمان نی بیش شد</p></div>
<div class="m2"><p>تنگیش نی بیشتر از پیش شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیک پیش صحت و نور یمین</p></div>
<div class="m2"><p>تنگی ظلمت شود چپ را قرین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون مقابل با یمین آمد یسار</p></div>
<div class="m2"><p>زین سبب دل را یسار آمد قرار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بود آیینه ی دل رو برو</p></div>
<div class="m2"><p>یا نگارستان آن ملک نکو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هین نکویی چونکه مظهر دل بود</p></div>
<div class="m2"><p>باید اول سوی دل روشن بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچنانکه صورت خوب نکو</p></div>
<div class="m2"><p>می نماید از سوی آیینه رو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زانکه آیینه خود از دیده جداست</p></div>
<div class="m2"><p>آینه مردیدها را رهنماست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل ولی هم آینه هم دیده است</p></div>
<div class="m2"><p>نقشها را سربسر خود دیده است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل بود چون آینه چشم ای همام</p></div>
<div class="m2"><p>نقشها را روبرو بیند تمام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرچه بینی مظهرش باشد بصر</p></div>
<div class="m2"><p>بینیش لیکن مقابل ای پسر</p></div></div>