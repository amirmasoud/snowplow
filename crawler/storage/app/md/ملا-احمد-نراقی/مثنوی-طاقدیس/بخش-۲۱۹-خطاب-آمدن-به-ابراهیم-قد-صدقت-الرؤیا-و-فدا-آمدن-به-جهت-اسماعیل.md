---
title: >-
    بخش ۲۱۹ - خطاب آمدن به ابراهیم قَدْ صَدَّقْتَ الرُّؤْیا و فدا آمدن به جهت اسماعیل
---
# بخش ۲۱۹ - خطاب آمدن به ابراهیم قَدْ صَدَّقْتَ الرُّؤْیا و فدا آمدن به جهت اسماعیل

<div class="b" id="bn1"><div class="m1"><p>بود با خنجر خلیل اندر عتاب</p></div>
<div class="m2"><p>کآمد از اقلیم لاهوتش خطاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کای خلیل ای جمله خوبان را امام</p></div>
<div class="m2"><p>صدهزار احسنت صدقت المنام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امتثال امر ما کردی درست</p></div>
<div class="m2"><p>ما همین فرموده بودیم از نخست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از برای امتحان بود و یقین</p></div>
<div class="m2"><p>ابتلا بود ابتلایی بس متین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امتحان بود از برای غیر من</p></div>
<div class="m2"><p>من نخواهم امتحان ای مؤتمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نام نیکت خواستم اندر جهان</p></div>
<div class="m2"><p>در جهان جسم و در اقلیم جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواستم در عالم غیب و شهود</p></div>
<div class="m2"><p>از عبادت فاش گردد آنچه بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مقتضای فیض و لطف بیکران</p></div>
<div class="m2"><p>کشف استعداد باشد از نهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قوه را خواهد عتاب انکشاف</p></div>
<div class="m2"><p>تا به فعلیت درآید در مصاف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در سویدای نهادت این سؤال</p></div>
<div class="m2"><p>بود پنهان همچو بدری در همال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از عنایت خواستم گردد عیان</p></div>
<div class="m2"><p>تا شود از نور او روشن جهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا ابد تابنده باشد نور تو</p></div>
<div class="m2"><p>شور افتد در جهان از شور تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شور جانبازان بود شور عجب</p></div>
<div class="m2"><p>بحر و بر را آورد اندر طرب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بلبل شوریده ای در گلستان</p></div>
<div class="m2"><p>شور اندازد به جان بلبلان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جمله مرغان را به فریاد آورد</p></div>
<div class="m2"><p>عشق پنهان جمله را یاد آورد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سوته جانی در میان انجمن</p></div>
<div class="m2"><p>آتش اندازد به جان مرد و زن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یک سر شوریده گر سر بر کند</p></div>
<div class="m2"><p>شور در صد شهر و صد ده افکند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون شود آشفته در شهری سری</p></div>
<div class="m2"><p>شور اندازد میان کشوری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای خوش آن شوریده ی بی بال و پر</p></div>
<div class="m2"><p>نه شناسد سر ز پا نه پا ز سر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای خوشا وقت و قبای ژنده اش</p></div>
<div class="m2"><p>وان هوای مرده ی جان زنده اش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای خوشا حال پریشانان زار</p></div>
<div class="m2"><p>سینه های ریش و دلهای فکار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای خوشا دلهای گرم سوزناک</p></div>
<div class="m2"><p>ای خوشا آن سینه های چاک چاک</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای خدا خواهم سر شوریده ای</p></div>
<div class="m2"><p>چشم نمناکی دل تفتیده ای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بوریای کهنه کنج خلوتی</p></div>
<div class="m2"><p>تا یکی شوریده حالی صحبتی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای خدا خواهم دل دیوانه ای</p></div>
<div class="m2"><p>خلوتی در گوشه ی ویرانه ای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا به یاد دلکش جانانه ای</p></div>
<div class="m2"><p>سر کنم من ناله ی مستانه ای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گوشه ای خواهم به یاد دلبری</p></div>
<div class="m2"><p>زیر بال خود کشم یکدم سری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای خدا خواهم دل آشفته ای</p></div>
<div class="m2"><p>اوف بر روی دو عالم گفته ای</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دامن کوهی پریشان همدمی</p></div>
<div class="m2"><p>تا بگویم از پریشانی دمی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا توانی با پریشانان نشین</p></div>
<div class="m2"><p>صحبت شوریدگان را برگزین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همنشینی شان از ایشانت کند</p></div>
<div class="m2"><p>صحبت نیکان ز نیکانت کند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دور از این افسردگان خام شو</p></div>
<div class="m2"><p>دور از این هنجار نافرجام شو</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>صحبت تنبل تورا تنبل کند</p></div>
<div class="m2"><p>همسری باکل سرت را کل کند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر روی گرمابه با گر گر شوی</p></div>
<div class="m2"><p>مدتی با خر نشینی خر شوی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ور نیابی سوته جانی در جهان</p></div>
<div class="m2"><p>رو ازین افسرده جانان شو نهان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دور شو از این گروه دیوسار</p></div>
<div class="m2"><p>زین نفس سردان خدایا الفرار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از خدا میخواه توفیق طلب</p></div>
<div class="m2"><p>تا تورا اندر طلب بخشد طرب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آتشی بر خار و خاشاکت زند</p></div>
<div class="m2"><p>خنجری بر سینه ی چاکت زند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نار او پا تا بسر نورت کند</p></div>
<div class="m2"><p>زخمهایش حی دیهورت کند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خنجری بی زخم و بیرنج و شکنج</p></div>
<div class="m2"><p>بهر اسماعیل شد دریای گنج</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا ابد نامش ذبیح الله شد</p></div>
<div class="m2"><p>شاه آدم بلکه شاهنشاه شد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در تبارش شاهی و پیغمبری</p></div>
<div class="m2"><p>شد مخلد تا بروز داوری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زخمی ار بودی نمی دانم چه بود</p></div>
<div class="m2"><p>وه چه در بر روزگارش می گشود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هیچ رنجی در رهش بی گنج نیست</p></div>
<div class="m2"><p>بی نصیب آنکس که او را رنج نیست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نیش بیصد نوش در این راه نیست</p></div>
<div class="m2"><p>نیست خاری کش گلی همراه نیست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بی عطا هرگز گدا ز اینجا نرفت</p></div>
<div class="m2"><p>دزد از این خانه بی کالا نرفت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>صورت خدمت کنی مزد آورند</p></div>
<div class="m2"><p>ناروا جنس آوری نیکو خرند</p></div></div>