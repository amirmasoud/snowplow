---
title: >-
    بخش ۱۵۹ - مناجات با قاضی الحاجات
---
# بخش ۱۵۹ - مناجات با قاضی الحاجات

<div class="b" id="bn1"><div class="m1"><p>ای خدا ای از تو دلها را نشاط</p></div>
<div class="m2"><p>ای به یادت جسم و جان را ارتباط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای فلک سرگشته ی سودای تو</p></div>
<div class="m2"><p>هستی عالم به یک ایمای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرتو خورشید نورافشان ز توست</p></div>
<div class="m2"><p>آب و رنگ چهره ی خوبان ز توست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای همه هستی ز نور هست تو</p></div>
<div class="m2"><p>چشم امید همه در دست تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تو خواهم از عنایت یکنظر</p></div>
<div class="m2"><p>تا نه جان دانم نه تن دانم نه سر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکجا دردی خریداری کنم</p></div>
<div class="m2"><p>آتشی هرجا پرستاری کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای انیس جان غم فرسوده ام</p></div>
<div class="m2"><p>ای به یادت آه درد آلوده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک نظر از تو ز من جان باختن</p></div>
<div class="m2"><p>از تو سوزانیدن از من ساختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای خدا شوری که جان بازی کنم</p></div>
<div class="m2"><p>همتی ده تا سراندازی کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای بهشت و کوثر و طوبای من</p></div>
<div class="m2"><p>ای تو هم دنیا و هم عقبای من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راحت من روح من ریحان من</p></div>
<div class="m2"><p>روضه ی من باغ من رضوان من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاه من سلطان من مولای من</p></div>
<div class="m2"><p>بهجت این جان غم فرسای من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مذهب من ملت من دین من</p></div>
<div class="m2"><p>شادی این خاطر غمگین من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یامنی قلبی نعیمی جنتی</p></div>
<div class="m2"><p>یا هوی نفسی حیوتی بهجتی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یا ضیاءالقلب یا نورالقلوب</p></div>
<div class="m2"><p>یا مزیل الهم کشاف الکروب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یا طبیبی منک دائی والدواء</p></div>
<div class="m2"><p>یا حبیبی منک سقمی والشفاء</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای رفیق خلوت تنهاییم</p></div>
<div class="m2"><p>ای انیس ای دل سوداییم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ان ترید قتلی فی قتلی رضاک</p></div>
<div class="m2"><p>ذاک جسمی ذاک روحی فی فناک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نقد ذاتم کم عیار و پر غش است</p></div>
<div class="m2"><p>در خور صد کوه پر از آتش است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون جز آتش مصرف دیگر نداشت</p></div>
<div class="m2"><p>هرکجا بردم کس آن را بر نداشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اندرین بازار گرداندم بسی</p></div>
<div class="m2"><p>نزد هرکس بردم و هرناکسی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در بهایش یک پشیزی کس نداد</p></div>
<div class="m2"><p>پیش تو آوردم اینک ای جواد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رد مکن آن را که در بازار توست</p></div>
<div class="m2"><p>خار اما خاری از گلزار توست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر نمی خواهی تو هم ای دادگر</p></div>
<div class="m2"><p>من که او را پس نمی خواهم دگر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درد و اول من آن را یافتم</p></div>
<div class="m2"><p>بر سر بازار تو انداختم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هرکه خواهد سازدش گو پایمال</p></div>
<div class="m2"><p>خواهد او را سگ خورد خواهد شغال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هرچه آید بر سر او آن توست</p></div>
<div class="m2"><p>این متاع توست این دکان توست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این گمانم نیست لیکن این کریم</p></div>
<div class="m2"><p>ای عطایت عام وی عفوت عظیم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گه متاع فاسدی بس ناروا</p></div>
<div class="m2"><p>در کف مسکین فقیری بینوا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در همه بازارها گردانده ای</p></div>
<div class="m2"><p>از در هر ناکس و کس رانده ای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در دکانی یک خریداریش نه</p></div>
<div class="m2"><p>هیچ شهری روی بازاریش نه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پیش تو آورده با امیدها</p></div>
<div class="m2"><p>کای ز خصلت نعمتت جاویدها</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از من این کالای بی رونق بخر</p></div>
<div class="m2"><p>منگر آن را در امید من نگر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رد کنی آن را به او واپس دهی</p></div>
<div class="m2"><p>دست او بر دست هرناکس دهی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خاصه چون من عاجز و درمانده ای</p></div>
<div class="m2"><p>بیکسی خواری ز هر در رانده ای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مبتلایی دردمندی خسته ای</p></div>
<div class="m2"><p>مستمندی دست و پا بشکسته ای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خاصه با صد کوه امید و رجا</p></div>
<div class="m2"><p>آستانت را گرفت ملتجا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سالها خو کرده ی یغمای توست</p></div>
<div class="m2"><p>پای تا سر غرق در آلای توست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خاصه توحید تو پیش انداخته</p></div>
<div class="m2"><p>نزد تو آن را وسیله ساخته</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>روزگاران دم ز توحیدت زده</p></div>
<div class="m2"><p>بلکه با توحیدت از مام آمده</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دل ز توحید تو آمد پرفروغ</p></div>
<div class="m2"><p>غرق توحید تو از پا تا کزوغ</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هم تورا بحر کرم بشناخته</p></div>
<div class="m2"><p>اندر آن دریا سفینه ساخته</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خاصه دارد خاصگانی را پناه</p></div>
<div class="m2"><p>که پناه هر سفیدند و سیاه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چارده خورشید گردون شرف</p></div>
<div class="m2"><p>چارده بدر منیر بی کلف</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اولین شان آن مهین وخشور بود</p></div>
<div class="m2"><p>کز جبین او فروزان نور بود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>والضحی یک لمعه ای از نور او</p></div>
<div class="m2"><p>نکهتی واللیل از گیسوی او</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آیتی از خوی او خلق عظیم</p></div>
<div class="m2"><p>نعت او بالمؤمنین و هو رحیم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>آنکه صدر و بدر هر دو عالم اوست</p></div>
<div class="m2"><p>افتخار عز و نسل آدم اوست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خوشه چین خرمن علمش ملک</p></div>
<div class="m2"><p>خاشه روب محفل حکمش فلک</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آخرین شان مرکز دنیا و دین</p></div>
<div class="m2"><p>هم امان خلق و خالق را امین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ساقی این دوره ی آخر زمان</p></div>
<div class="m2"><p>باقی از بهر بقای کن فکان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سر مستور و در مخزون تو</p></div>
<div class="m2"><p>گنج پنهان لؤلؤ مکنون تو</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>آسمان اندر حریمش پرده ای</p></div>
<div class="m2"><p>آفتاب از خوان او یک گرده ای</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>حاکم و سلطان دارالملک دین</p></div>
<div class="m2"><p>مصطفی را جانشین آخرین</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>اوست شمع ماه و خور پروانه اش</p></div>
<div class="m2"><p>عرش و کرسی آستان خانه اش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>پرتو خورشید عکس روی اوست</p></div>
<div class="m2"><p>آب حیوان رشحه ای از جوی اوست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>عالم جانست و جان عالم است</p></div>
<div class="m2"><p>خاتم است و جانشین خاتم است</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>مایه ام عجز و امیدم بس دراز</p></div>
<div class="m2"><p>تکیه گاهم رحمتوست ای بی نیاز</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تحفه ام توحید و خاصانم پناه</p></div>
<div class="m2"><p>از چه می ترسم دگر ای پادشاه</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>آه و واویلاه ترسانم ز خود</p></div>
<div class="m2"><p>همچو شاخ بید لرزانم ز خود</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ای فغان از این عدوی خانگی</p></div>
<div class="m2"><p>کاش بودی بامنش بیگانگی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دفع کن اهل عدوی خانه را</p></div>
<div class="m2"><p>حمله آور آنگهی بیگانه را</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نفس خود ناکرده تسخیر ای فلان</p></div>
<div class="m2"><p>چون کنی تسخیر نفس دیگران</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>تا تویی در دست روباهان اسیر</p></div>
<div class="m2"><p>کی توانی پنجه زد با گرگ و شیر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>خانه ی خود را بگیر از دشمنان</p></div>
<div class="m2"><p>وانگهی رو کن به راه از اصفهان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>تا نگردی خود ز خود فرمان پذیر</p></div>
<div class="m2"><p>کی شود فرمان پذیرت شاه و میر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>رو تو اول نفس خود زنجیر کن</p></div>
<div class="m2"><p>هرکه خواهی آنگهی تسخیر کن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ای خنک آن کو که افکند این حریف</p></div>
<div class="m2"><p>جان خود را وارهانید از کثیف</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نفس او شد زیر فرمان پیش او</p></div>
<div class="m2"><p>شد مسلمان نفس کافر کیش او</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>عقل اینست ای رفیق معنوی</p></div>
<div class="m2"><p>هین بگو این با جناب مولوی</p></div></div>