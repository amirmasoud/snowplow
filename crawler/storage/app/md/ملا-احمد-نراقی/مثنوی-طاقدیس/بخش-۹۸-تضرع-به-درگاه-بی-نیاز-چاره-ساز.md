---
title: >-
    بخش ۹۸ - تضرع به درگاه بی نیاز چاره ساز
---
# بخش ۹۸ - تضرع به درگاه بی نیاز چاره ساز

<div class="b" id="bn1"><div class="m1"><p>ای خدا بنگر به این بار گران</p></div>
<div class="m2"><p>ناقه ای دارم ضعیف و ناتوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خدا این ناتوان را رحمتی</p></div>
<div class="m2"><p>رحمتی تا هست یا رب فرصتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای خدا از لطف و رحمت یکنظر</p></div>
<div class="m2"><p>سوی این افتاده ی بی پا و سر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای خدا ای مرهم جان فکار</p></div>
<div class="m2"><p>ای امید این دل امیدوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست من از کار خود برداشتم</p></div>
<div class="m2"><p>کار خود با رحمتت بگذاشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیشه بر بیخ سببها آختم</p></div>
<div class="m2"><p>هر سبب از بیخ و بن انداختم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی سبب می دانم و نی واسطه</p></div>
<div class="m2"><p>نی شناسم باعث و نی رابطه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون برآید دست قدرت ز آستین</p></div>
<div class="m2"><p>نی سبب ماند نه باعث نی معین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این سبب نبود خدایا جز طلسم</p></div>
<div class="m2"><p>قسمتی نبود ستم را غیر اسم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این سرا را آزمونگه ساختی</p></div>
<div class="m2"><p>پرده ای بر روی کار انداختی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نام این یک را سبب بگذاشتی</p></div>
<div class="m2"><p>در قفای آن مسبب داشتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا چنین دانند اهل آزمون</p></div>
<div class="m2"><p>کاین مسبب از سبب آید برون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ورنه دانا داند ای دانای کار</p></div>
<div class="m2"><p>غنچه و گل نیست اندر نوک خار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بارها من خارها بشکافتم</p></div>
<div class="m2"><p>نی در آنجا غنچه نی گل یافتم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چوب نار ار بشکنی هفتاد بار</p></div>
<div class="m2"><p>نی در آن گلنار می بینی نه نار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بشکنی گر بیضه ای را ای فلان</p></div>
<div class="m2"><p>نه خروس آنجا بود نا ماکیان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نطفه را هرچند کاوی بیشتر</p></div>
<div class="m2"><p>نی نشان از گاو بینی نی ز خر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رو زمین تا پشت ماهی کن شیار</p></div>
<div class="m2"><p>بین نه شمشاد است آنجا نه خیار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دانه بشکاف ای رفیق موشکاف</p></div>
<div class="m2"><p>بین نه آنجا پنبه بینی نی کلاف</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کرده ای رو پوش چوب و خاک را</p></div>
<div class="m2"><p>دانه را و بیضه را و کاک را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ورنه گر خواهی برآری گل زسنگ</p></div>
<div class="m2"><p>هم پلنگ از کوه و از قلزم نهنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر تو خواهی آوری خرما ز بید</p></div>
<div class="m2"><p>می گشایی جمله درها بی کلید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پیل و اشتر را ز خاک آری برون</p></div>
<div class="m2"><p>خوشه بیدانه دهی ای ذوفنون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ور تو خواهی پنبه دانه آشکار</p></div>
<div class="m2"><p>جامه های دوخته آرد ببار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دانه آرد نان پخته با ادام</p></div>
<div class="m2"><p>نطفه آرد اسب زرین با لگام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای خدا ای پادشاه راستین</p></div>
<div class="m2"><p>دست قدرت را برآر از آستین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آتش اندر خرمن اسباب زن</p></div>
<div class="m2"><p>چاره ای کن بی سبب در کار من</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ور سبب باید سبب سازی ز توست</p></div>
<div class="m2"><p>سرنگونی و سرافرازی ز توست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در خلاص این فرو رفته بلای</p></div>
<div class="m2"><p>یک سبب ساز ای سبب را رهنمای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کاروان رفت و من اندر لای و گل</p></div>
<div class="m2"><p>لای و گل از پای تا سر متصل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همرهان رفتند و من بی راحله</p></div>
<div class="m2"><p>مانده ام دور از رفیق و قافله</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همتی ای کاروانسالارها</p></div>
<div class="m2"><p>یک نگاهی سوی سنگین بارها</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کز قفا در لای و گل درمانده اند</p></div>
<div class="m2"><p>آیه ی نومیدی خود خوانده اند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ای امام عهد و سلطان زمان</p></div>
<div class="m2"><p>ای جهان را پادشاه و پاسبان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای امیر کاروان واپسین</p></div>
<div class="m2"><p>ای وجودت لنگر چرخ و زمین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یکنظر کن سوی واپس ماندگان</p></div>
<div class="m2"><p>یکنظر دارند از تو آرمان</p></div></div>