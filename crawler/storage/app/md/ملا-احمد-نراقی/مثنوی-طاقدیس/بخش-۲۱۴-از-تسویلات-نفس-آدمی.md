---
title: >-
    بخش ۲۱۴ - از تسویلات نفس آدمی
---
# بخش ۲۱۴ - از تسویلات نفس آدمی

<div class="b" id="bn1"><div class="m1"><p>دیده ای خواهم ازین هم تیزتر</p></div>
<div class="m2"><p>تا ببینم زیر این مکری دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاید این میل جهاد اندر نهاد</p></div>
<div class="m2"><p>منبعث باشد ز عقل ذوسداد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس خواهد از ره مکر و فریب</p></div>
<div class="m2"><p>سازدش از این سعادت بی نصیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زد برآب این نقش و جان را خیره کرد</p></div>
<div class="m2"><p>چشمه ی صاف خرد را تیره کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرد مسکین در سحرگاهان ز خواب</p></div>
<div class="m2"><p>می شود بیدار با صد اضطراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل پر از شوق مناجات حبیب</p></div>
<div class="m2"><p>خاطر اندر سجده ی حق بی شکیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز مناجات حبیبش یاد نیست</p></div>
<div class="m2"><p>غیر حق در پیش او جز یاد نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواهد از جا خیزد از بهر نماز</p></div>
<div class="m2"><p>بهر عرض راز با دانای راز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفس گوید آن فلان بیدار هست</p></div>
<div class="m2"><p>یا مگر گوشی پس دیوار هست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این نمازت خودنمایی می شود</p></div>
<div class="m2"><p>طاعتت زین رو ریایی می شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوابت اکنون محض خیر و قربت است</p></div>
<div class="m2"><p>ور گرایی سوی طاعت رتبت است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اینچنین خواب از نمار اولیتر است</p></div>
<div class="m2"><p>این قناعت از نیاز اولیتر است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زین سبب فرمود شاه انس و جان</p></div>
<div class="m2"><p>خواب دانا به ز ذکر جاهلان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زین خیالاتت فریبد هر زمان</p></div>
<div class="m2"><p>تا برآید آفتاب از خاوران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خواب شیرین خوابگاه زفت و گرم</p></div>
<div class="m2"><p>خاصه با همخوابه ی شیرین و نرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نفس بی انصاف از رحمت بری</p></div>
<div class="m2"><p>کی گذارد تا از آنها بگذری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>راهها بنماید افزون از حساب</p></div>
<div class="m2"><p>تا کند از شرع واجب بر تو خواب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ور به مسجد خواندت روزی خرد</p></div>
<div class="m2"><p>نفس دون صد حیله پیشت آورد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کاین خضوع و این خشوع و این ادب</p></div>
<div class="m2"><p>خودنمایی و ریا را شد سبب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا نمازت را کند نقرالغراب</p></div>
<div class="m2"><p>یا تورا سازد ز مسجد سد باب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ور گریزی از ریا در انجمن</p></div>
<div class="m2"><p>گویدت ای مقتدای مؤتمن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این ریا نبود ادب آموزی است</p></div>
<div class="m2"><p>خلق عالم را ز تو فیروزی است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از تو آموزند خلقان نیک و بد</p></div>
<div class="m2"><p>سعی کن در غنه و اطباق و مد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سوره طولانی کن و ذکر سجود</p></div>
<div class="m2"><p>راست کن خود در قیام و کج قعود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اندر اینجا طاعت مردانه کن</p></div>
<div class="m2"><p>مختصر گر می کنی در خانه کن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای مسلمانان فغان از دست نفس</p></div>
<div class="m2"><p>کم کسی برده است جان از دست نفس</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نفس تو در حیله و افسون گری ست</p></div>
<div class="m2"><p>لحظه ای خالی ز مکر و خدعه نیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>می نبینی لیک چون کور و کری</p></div>
<div class="m2"><p>می نفهمی لیک از بس که خری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هان و هان ایجان من هشیار باش</p></div>
<div class="m2"><p>با خبر از مکر این مکار باش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر بگویم مکر نفس ذوفنون</p></div>
<div class="m2"><p>طاقدیسم می شود از حد فزون</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مینهم آن را در اینجا در میان</p></div>
<div class="m2"><p>باز می گردم به سوی داستان</p></div></div>