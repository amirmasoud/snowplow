---
title: >-
    بخش ۳۳ - اهل و عیال، فرزند طماع یا دشمنان دوست نما
---
# بخش ۳۳ - اهل و عیال، فرزند طماع یا دشمنان دوست نما

<div class="b" id="bn1"><div class="m1"><p>آنچه خواهد بر تو فرزند و زنت</p></div>
<div class="m2"><p>کافرم گر می پسندد دشمنت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با رفیق خود یکی گفت ای پسر</p></div>
<div class="m2"><p>کاشکی بکشد مرا شخصی پدر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا برم هم ارث و هم گیرم دیت</p></div>
<div class="m2"><p>از پدر بهتر بود این منفعت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پاسخش داد آن دگر یک کی قرین</p></div>
<div class="m2"><p>من پدر را می نخواهم اینچنین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلکه می خواهم که چشمانش نخست</p></div>
<div class="m2"><p>برکنند و یک دیت گیرم درست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس ببرندش دو گوش حق نیوش</p></div>
<div class="m2"><p>یک دیت دیگر بگیرم بهر گوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بینیش را پس ببرند و از آن</p></div>
<div class="m2"><p>پس ببرندش دو لب آنگه زبان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس جدا سازند دستانش ز تن</p></div>
<div class="m2"><p>پایهایش پس ببرند از بدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس ذکر پس خصیتینش را برند</p></div>
<div class="m2"><p>پس کشندش جان من فارغ کنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تادیت گیرم برای هرکدام</p></div>
<div class="m2"><p>پس ستانم ارث او را بالتمام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر تو ای فرزند بادا آفرین</p></div>
<div class="m2"><p>می توان پرورد فرزندی چنین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می توان کردن تن و ایمان و جان</p></div>
<div class="m2"><p>رهن ایشان ای رفیق مهربان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای خدا پایی عطا کن تندرو</p></div>
<div class="m2"><p>تا برآرم خویشتن را زین گرو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هم برآرم گردن خود از کمند</p></div>
<div class="m2"><p>هم بیندازم ز پای خویش بند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دوستی با این گروه است ای فلان</p></div>
<div class="m2"><p>دوستی پادشه با زنگیان</p></div></div>