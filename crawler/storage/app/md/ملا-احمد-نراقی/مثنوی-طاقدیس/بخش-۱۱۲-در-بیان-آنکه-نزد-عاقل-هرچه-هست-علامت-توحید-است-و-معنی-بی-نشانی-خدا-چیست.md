---
title: >-
    بخش ۱۱۲ - در بیان آنکه نزد عاقل هرچه هست علامت توحید است و معنی بی نشانی خدا چیست
---
# بخش ۱۱۲ - در بیان آنکه نزد عاقل هرچه هست علامت توحید است و معنی بی نشانی خدا چیست

<div class="b" id="bn1"><div class="m1"><p>آنکه او باشد نشان خویشتن</p></div>
<div class="m2"><p>بی نشان کی باشد او ای مؤتمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اولم یکف بربک، ای سعید</p></div>
<div class="m2"><p>انه کان علی الکل شهید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه باشد گر نشانی ذو خبر</p></div>
<div class="m2"><p>می دهد کی بی نشان باشد دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی نشان است آن شهنشاه الست</p></div>
<div class="m2"><p>معنی آن لیک چیز دیگر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یعنی او را از اشاره پاکدان</p></div>
<div class="m2"><p>پاک هم از صورت از ادراک دان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وهم را اندر حریمش بار نیست</p></div>
<div class="m2"><p>عقل محرم اندر آن دربار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده ها کورند از دیدار او</p></div>
<div class="m2"><p>لیک دلها روشن از انوار او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای بزرگ آن پادشاه ذوالجلال</p></div>
<div class="m2"><p>خود نشان بی نشان و بی مثال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عالم از او هم پر و هم خالی است</p></div>
<div class="m2"><p>جان از او پرگوهر و اجلالی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده ها بینا از او و کور از او</p></div>
<div class="m2"><p>سینه ها سینا صفت پرنور از او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آسمان از بیم جودش یک حباب</p></div>
<div class="m2"><p>ذره ای از عکس نورش آفتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این سخن بگذار کاین بحر عمیق</p></div>
<div class="m2"><p>کرده سیاحان عالم را غریق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اندرین بیدای ناپیدا کران</p></div>
<div class="m2"><p>یاوه گشته شهسواران جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون به اینجا می رسی خاموش باش</p></div>
<div class="m2"><p>اندرین محفل سراپا گوش باش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از کلام حق ثنای او بجوی</p></div>
<div class="m2"><p>اندرین جا غیر لااحصی مگوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گو که آن شوریده ی یکتای فرد</p></div>
<div class="m2"><p>در کلیسا شد چه گفت و او چه کرد</p></div></div>