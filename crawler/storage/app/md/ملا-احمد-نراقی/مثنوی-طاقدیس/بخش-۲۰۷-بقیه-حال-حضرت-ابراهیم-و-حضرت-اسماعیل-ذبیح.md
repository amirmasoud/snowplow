---
title: >-
    بخش ۲۰۷ - بقیه حال حضرت ابراهیم و حضرت اسماعیل ذبیح
---
# بخش ۲۰۷ - بقیه حال حضرت ابراهیم و حضرت اسماعیل ذبیح

<div class="b" id="bn1"><div class="m1"><p>شست هاجر پیکر فرزند را</p></div>
<div class="m2"><p>شانه زد آن کاکل دلبند را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشک زد آن زلف عنبربار را</p></div>
<div class="m2"><p>زد گلاب آن چهره ی گلنار را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرمه اش در دیده ی مخمور کرد</p></div>
<div class="m2"><p>ظلمتی را داخل اندر نور کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جامه پوشانید او را رنگ رنگ</p></div>
<div class="m2"><p>اندر آغوش در کشیدش تنگ تنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف او بویید و بوسیدش جبین</p></div>
<div class="m2"><p>بر سر و بر رو کشیدش آستین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد در آتش سپند وان یکاد</p></div>
<div class="m2"><p>خواند و دستش را به ابراهیم داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست او بگرفت ابراهیم فرد</p></div>
<div class="m2"><p>شد روان و روی خود واپس نکرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پای کوبان شد روان سرشار و مست</p></div>
<div class="m2"><p>کارد بر کت دست فرزندش به دست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد روان اندر بیابان بیقرار</p></div>
<div class="m2"><p>پا زدی از شوق بر خارا و خار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زیر پایش خار گل خارا حریر</p></div>
<div class="m2"><p>گرد و خاک پهنه اش مشک و عبیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می دویدی در بیابان حرم</p></div>
<div class="m2"><p>در قفایش آن ذبیح محترم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت ای جان میهمانت می برم</p></div>
<div class="m2"><p>بلبلی تا گلستانت می برم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طوطئی ای جان من پرواز کن</p></div>
<div class="m2"><p>رو به هندستان عز و ناز کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رو بسوی کعبه ی مقصود کن</p></div>
<div class="m2"><p>این جهان را پا زن و بدرود کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>می رویم اینک به میدان منی</p></div>
<div class="m2"><p>هان و هان ای جان نثار ان الصلا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای حریفان سوی جانان می رویم</p></div>
<div class="m2"><p>از قفس سوی گلستان می رویم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سر بکف داریم از بهر خدا</p></div>
<div class="m2"><p>گر سری دارید یاران الصلا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون سخن اینجا رسید ای دوستان</p></div>
<div class="m2"><p>آتش افتادم به مغز استخوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باز هندستان به یادم اوفتاد</p></div>
<div class="m2"><p>شوری از نو در نهادم اوفتاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شعله ور شد آتش پنهان من</p></div>
<div class="m2"><p>موج زن شد بحر بی پایان من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در هوا دیدم پرافشان طایران</p></div>
<div class="m2"><p>در قفس شد مرغ جانم پرفشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دید مرغی در هوا پرواز کرد</p></div>
<div class="m2"><p>مرغ جانم پر زدن آغاز کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دید مرغان در هوا و از هوس</p></div>
<div class="m2"><p>بال و پر در همزد اما در قفس</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای دریغا در قفس را باز نیست</p></div>
<div class="m2"><p>هم پرم را قوت پرواز نیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای دریغا در قفس را بسته است</p></div>
<div class="m2"><p>ای دریغا بال من بشکسته است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>می نویسم داستان طوطیان</p></div>
<div class="m2"><p>طوطی جانم به فریاد و فغان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گاه گاهی آیدم بر سر جنون</p></div>
<div class="m2"><p>نوبت دیوانگی آمد کنون</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای رفیقان فکر تدبیرم کنید</p></div>
<div class="m2"><p>من شدم دیوانه زنجیرم کنید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لیک سودی نی کنون تدبیر را</p></div>
<div class="m2"><p>بگسلم از هم دوصد زنجیر را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>غیر آن زنجیر زلف مشکبار</p></div>
<div class="m2"><p>رو رو آن زنجیر از بهرم بیار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>غیر آن زنجیر مویی برشکست</p></div>
<div class="m2"><p>پاره سازم گر دوصد زنجیر هست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دوستان زنجیر زلف یار کو</p></div>
<div class="m2"><p>سلسله آن گیسوی طرار کو</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سلسله آن مو فکن در گردنم</p></div>
<div class="m2"><p>ورنه خود را من به دریا افکنم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چیست دریا من محیط آتشم</p></div>
<div class="m2"><p>هفت دریا را بیکدم درکشم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گویی ای همدم ز بهر دوستان</p></div>
<div class="m2"><p>این سخن بگذار و رو بر داستان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>طبع شعر من کنون بر باد رفت</p></div>
<div class="m2"><p>هم ردیف قافیه از یاد رفت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آهوی طبعم مگر صیاد دید</p></div>
<div class="m2"><p>بر کف او خنجر فولاد دید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رم گرفت از من به بحر و برگریخت</p></div>
<div class="m2"><p>رشته ی نظمم ز یکدیگر گسیخت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نظم چون آید که طبع از کار رفت</p></div>
<div class="m2"><p>دل به شوق دیده ی دیدار رفت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>باز شوقم شوری اندر سر فکند</p></div>
<div class="m2"><p>آتشم در خامه و دفتر فکند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نظم چون آید دلم هشیار نیست</p></div>
<div class="m2"><p>در سرم جز شوق وصل یار نیست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>داستان افسانه باشد ای فلان</p></div>
<div class="m2"><p>من همی بینم عیان این داستان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هست در گوش من آواز خلیل</p></div>
<div class="m2"><p>گشته جانبازان کویش را دلیل</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کالصلا ای دوستان الصلا</p></div>
<div class="m2"><p>عید قربانست یاران الصلا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>روز سربازیست در بازار عید</p></div>
<div class="m2"><p>گر سری دارید اینجا پا نهید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دل مرا در بر تپید از این صلا</p></div>
<div class="m2"><p>جان سبق کرده به میدان بلا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دل به یاد تیغ او در اضطراب</p></div>
<div class="m2"><p>سر به راه خنجرش دارد شتاب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جان ز شوق و وجد بال و پر زند</p></div>
<div class="m2"><p>تا مگر خود بردم خنجر زند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>لیک می گوید همی صیاد من</p></div>
<div class="m2"><p>نیستی اندر خور بیداد من</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تو هوسناکی و خونت پاک نیست</p></div>
<div class="m2"><p>این سرت شایسته فتوراک نیست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>حیف باشد خنجر من بر سرت</p></div>
<div class="m2"><p>خاک ما را نیست در خور پیکرت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>امتحان گاه است این میدان ما</p></div>
<div class="m2"><p>امتحان کردم تورا من بارها</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>امتحان کردم تورا ای بوالهوس</p></div>
<div class="m2"><p>در سرت نبود بجز سودا و بس</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هر دمت در سر هوای دیگر است</p></div>
<div class="m2"><p>دل تورا هر دم بجای دیگر است</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همچو طفلان پرهوسناکی هنوز</p></div>
<div class="m2"><p>در پی بازی با خاکی هنوز</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>یا برون کن این هوسها را ز دل</p></div>
<div class="m2"><p>یا سر خود را ز بهر خود بهل</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>یا سر سودای گوناگون ببر</p></div>
<div class="m2"><p>یا بکن سودای ما بیرون ز سر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>من همی گویم که ای سلطان داد</p></div>
<div class="m2"><p>سعی بی توفیق تو باد است باد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>باد پیمایی خدایا تا بچند</p></div>
<div class="m2"><p>یک عنایت کن خلاصم کن ز بند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>این دل ناشاد من را شاد کن</p></div>
<div class="m2"><p>هم ز بند غیر خود آزاد کن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مرغیم در گردنم افتاده دام</p></div>
<div class="m2"><p>از تو می جویم رهایی والسلام</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چون روان شد از پی قربان خلیل</p></div>
<div class="m2"><p>شد بلند از جان اهریمن عویل</p></div></div>