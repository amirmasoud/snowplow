---
title: >-
    بخش ۱۰۱ - حکایت شخصی که به چاه رفت و در ته چاه اژدها دید
---
# بخش ۱۰۱ - حکایت شخصی که به چاه رفت و در ته چاه اژدها دید

<div class="b" id="bn1"><div class="m1"><p>سخت ماند داستانت ای عمو</p></div>
<div class="m2"><p>قصه ی آن را که در چه شد فرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بیابان آن یکی می رفت فرد</p></div>
<div class="m2"><p>شیر مستی دید او را حمله کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه او تنها سفر کرد ای رفیق</p></div>
<div class="m2"><p>صد خطر پیش آمد او را در طریق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی تواند دید او جز یک جهت</p></div>
<div class="m2"><p>از جهان دیگرش آید سمت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن جوان بگریخت از شیر غرین</p></div>
<div class="m2"><p>با دل صد بیم وحشت را قرین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رشته امید او بگسیخته</p></div>
<div class="m2"><p>دید در چاهی رسن آویخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن رسن بگرفت و اندر چاه شد</p></div>
<div class="m2"><p>تا لب چه شیرش از همراه شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر سر چه شیر نر بنشست و او</p></div>
<div class="m2"><p>قطره آسا اندر آن چه شد فرو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیم راه چه چو طی کرد آن پسر</p></div>
<div class="m2"><p>بر نشیب چاه افتادش نظر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اژدهایی دید بگشوده دهان</p></div>
<div class="m2"><p>منتظر تا طعمه سازد آن جوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هوشش از سر رفت و رنگ از رخ پرید</p></div>
<div class="m2"><p>خون دل از دیده اش بر رخ دوید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناگهانش آمد آوازی به گوش</p></div>
<div class="m2"><p>سوی بالا دید و دید آنجا دو موش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از پی قطع رسن اندر جدل</p></div>
<div class="m2"><p>تقطعان الحبل جذه بالعجل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ریسمان را می بریدند ای ودود</p></div>
<div class="m2"><p>رشته عمرش همی برند زود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آه تا بینی رسن بگسیخته</p></div>
<div class="m2"><p>خون این بیچاره در چه ریخته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آه تا بینی فتد در قعر چاه</p></div>
<div class="m2"><p>طعمه گردد اژدها را آه آه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چونکه دید آن ماجرا را آن جوان</p></div>
<div class="m2"><p>ماند مسکین زار و حیران در میان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>داد از حیرت خدایا داد داد</p></div>
<div class="m2"><p>حیرتم زنجیرها بر دل نهاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حیرتوست آن کو جگرها خون کند</p></div>
<div class="m2"><p>عقل را حیرت ز سر بیرون کند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنکه شد تا سوی مقصد شاد شد</p></div>
<div class="m2"><p>آنکه شد مأیوس هم آزاد شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن یکی بیمار و او رست از مرض</p></div>
<div class="m2"><p>جان او شد شاد و حاصل شد غرض</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وان دگر مردش مریض آزاد شد</p></div>
<div class="m2"><p>شد دو روزی هم غمش از یاد شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ماتم آن دارد که اندر حیرت است</p></div>
<div class="m2"><p>نی مریضش را اجل نی صحت است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زین سبب بیمار داری ای پسر</p></div>
<div class="m2"><p>نزد دانا شد ز بیماری بتر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وان یکی فهمید معنی را درست</p></div>
<div class="m2"><p>لاله های شادیش از سینه رست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وان دگر مأیوس از فهمیدن است</p></div>
<div class="m2"><p>فارغ است و در پی خوابیدن است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آنکه نی فهمید و نی مأیوس شد</p></div>
<div class="m2"><p>سینه اش زندان صد مفروس شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نی مزه از خواب بیند نی ز خور</p></div>
<div class="m2"><p>خون خورد از اول شب تا سحر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن یکی بگرفت تخت و شاه شد</p></div>
<div class="m2"><p>بی تحیر بر فراز کاه شد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وان دگر یک ترک تاج و تخت کرد</p></div>
<div class="m2"><p>فکر کار آب و نان و رخت کرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هریکی را بهره ای از راحت است</p></div>
<div class="m2"><p>وان آن مسکین که اندر حیرت است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زین سبب آن افتخار عالمین</p></div>
<div class="m2"><p>قال ان الیأس احدی الراحتین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هرکه باشد در میان خوف و بیم</p></div>
<div class="m2"><p>باشدش پیوسته دل از غم دونیم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همچو آن مسکین که اندر چاه شد</p></div>
<div class="m2"><p>ز اژدها و موشها آگاه شد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مانده مسکین واله اندر کار خویش</p></div>
<div class="m2"><p>با دلی صد چاک و جانی ریش ریش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در میان چه نگون آویخته</p></div>
<div class="m2"><p>جای اشک از دیده ها خون ریخته</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دید ناگه خیل زنبور عسل</p></div>
<div class="m2"><p>در کمرگاه چه ایشان را محل</p></div></div>