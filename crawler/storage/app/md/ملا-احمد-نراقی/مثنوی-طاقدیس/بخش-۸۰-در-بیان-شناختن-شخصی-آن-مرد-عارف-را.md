---
title: >-
    بخش ۸۰ - در بیان شناختن شخصی آن مرد عارف را
---
# بخش ۸۰ - در بیان شناختن شخصی آن مرد عارف را

<div class="b" id="bn1"><div class="m1"><p>بانگ زد کی رو سیاهان عنود</p></div>
<div class="m2"><p>زد گریبان چاک و رخساره شخود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این گرفتاری که در چنگ شماست</p></div>
<div class="m2"><p>دزد نبود این فلان مرد خداست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این چراغ خلوت اهل حق است</p></div>
<div class="m2"><p>در ظلام دهر نور مطلق است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای عجب کاین نور را نشناختند</p></div>
<div class="m2"><p>تیغ ظلم و جور بر او آختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندک اندک آن خبر کردند فاش</p></div>
<div class="m2"><p>بر در و دیوار افتاد ارتعاش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پادشه از حادثه آگاه شد</p></div>
<div class="m2"><p>جانب بازار از خرگاه شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پابرهنه آمد آن شاه سترگ</p></div>
<div class="m2"><p>تا سیاستگاه آن مرد بزرگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دید او را غرق خون خویشتن</p></div>
<div class="m2"><p>در برش جلاد با تیغ دو من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شه چو دید این ماجرا حیران بماند</p></div>
<div class="m2"><p>هم در آندم جمله جلادان بخواند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داد فرمان هلاک شیروان</p></div>
<div class="m2"><p>هم امیر و هم عسس هم مهتوران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرد زاهد با کف پرخون ز جا</p></div>
<div class="m2"><p>جست و گفت ای پادشه بهر خدا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیگنه خون گرفتاران مریز</p></div>
<div class="m2"><p>نیست ایشان را گناهی هیچ چیز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر گناهی هست بر دست منست</p></div>
<div class="m2"><p>بالله این اندر خور ببریدنست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کیفر این دست بی انصاف داد</p></div>
<div class="m2"><p>آفرین بر دست آن جلاد باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر نشان بنشست تیر شست او</p></div>
<div class="m2"><p>من بنازم تیر او و شست او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر کف من شد جدا نبود شگفت</p></div>
<div class="m2"><p>کیفر کفر این کف کافر گرفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرتد فطری بد و خونش هدر</p></div>
<div class="m2"><p>ریخت هرکس خون او لله در</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شرک آورد آن خدای پاک را</p></div>
<div class="m2"><p>خون بباید ریخت این بی باک را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دست من می داند و من جرم او</p></div>
<div class="m2"><p>میرشب را با عسس مجرم مگو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کار ایشان عین خیر است و صواب</p></div>
<div class="m2"><p>باید از ایشان نمودن احتساب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بود آن خوش باد کین نابود کرد</p></div>
<div class="m2"><p>این بگفت و شاه را بدرود کرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>با کف ببریده دست خونچکان</p></div>
<div class="m2"><p>شکرگویان سوی خانه شد روان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر زمین نامد ز شادی پای او</p></div>
<div class="m2"><p>شکر حق می ریخت از لبهای او</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سر همی کردی بسوی آسمان</p></div>
<div class="m2"><p>گفت ای پروردگار مهربان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من چگونه شکر الطافت کنم</p></div>
<div class="m2"><p>شکر لطف قاف تاقافت کنم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر بن موی من ارگردد دهان</p></div>
<div class="m2"><p>هر دهانش را بدی هفتصد زبان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر زبان هفتصد لغت گویا شود</p></div>
<div class="m2"><p>در طریق شکر تو پویا شود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کی توانم ای برون از چند و چون</p></div>
<div class="m2"><p>آیم از یک عهده ی شکرت برون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گه فتاد از بهر سجده بر زمین</p></div>
<div class="m2"><p>بر زمین مالید رخسار و جبین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گاه کردی رو بسوی آسمان</p></div>
<div class="m2"><p>در سپاس و حمد بگشودی زبان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کای خدا ای لطفت از اندازه بیش</p></div>
<div class="m2"><p>دست لطفت مرهم دلهای ریش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من فدای لطفت بی پایان تو</p></div>
<div class="m2"><p>بنده ی شرمنده ی احسان تو</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه فلک سرگشته و شیدای توست</p></div>
<div class="m2"><p>مست و بیخود خاک از صهبای توست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جرعه ای دادی ز شوق افلاک را</p></div>
<div class="m2"><p>جرعه ی دیگر بسیط خاک را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آن بر قاضی کمر از شوق بست</p></div>
<div class="m2"><p>این ز شکر عشق تو افتاده مست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در هوایت آب و باد اندر طلب</p></div>
<div class="m2"><p>این روان و آن دوان در روز و شب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کوه برجا خشک اگر بینی درست</p></div>
<div class="m2"><p>ایستاده در حضور و محو توست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرغها کاندر هوا طیران کنند</p></div>
<div class="m2"><p>در هوای عشق تو جولان کنند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ماهیان در آب و موران در حجور</p></div>
<div class="m2"><p>طایران در شاخ و کرمان در صخور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جمله بر خوان تو مهمان تو اند</p></div>
<div class="m2"><p>جیره خوار خوان احسان تو اند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>من کجا و وصف ذات پاک تو</p></div>
<div class="m2"><p>ای ملایک قاصر از ادراک تو</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>من کجا و شرح نعمتهای تو</p></div>
<div class="m2"><p>ای دو عالم سفره ی یغمای تو</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کیست آن کو غرق نعمای تو نیست</p></div>
<div class="m2"><p>چیست آن چیزی که آلای تو نیست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نک یکی این لطف خشم آمیز بین</p></div>
<div class="m2"><p>خشم نازآلود عشق انگیز بین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>لطف کردی سوی خود خواندی مرا</p></div>
<div class="m2"><p>از در بیگانگان راندی مرا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>عشوه ای کردی و کارم ساختی</p></div>
<div class="m2"><p>آتش شوقم به جان انداختی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مدتی این آتشم خاموش بود</p></div>
<div class="m2"><p>سوز عشق از خاطرم فرموش بود</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دامنی امشب زدی بر نار من</p></div>
<div class="m2"><p>من فدای یار شیرین کار من</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شعله ی عشق من امشب تیز شد</p></div>
<div class="m2"><p>آتش پنهانم اخگر ریز شد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز آب حیوان خوشتر است این آتشم</p></div>
<div class="m2"><p>من در این آتش سمندروش خوشم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>در من امشب آتشی افروختی</p></div>
<div class="m2"><p>هرچه با من غیر عشقت سوختی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ناری آمد شد عیان صد نور ازو</p></div>
<div class="m2"><p>شد سراپایم درخت طور ازو</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نور بخشد آتش سوزان تو</p></div>
<div class="m2"><p>تا چه بخشد روی نورافشان تو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از کفت زخمی بتن آید مرا</p></div>
<div class="m2"><p>ساخت زخمت زنده ی سرمد مرا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>وه چه زخمت راحت صد درد من</p></div>
<div class="m2"><p>هم دوای جان غم پرورد من</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>وه چه زخمی مرهم صد داغ من</p></div>
<div class="m2"><p>روضه ی من گلشن من باغ من</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>زخم تو هر زخم را مرهم نهد</p></div>
<div class="m2"><p>مرده ی صد ساله را صد جان دهد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>روح بخشد قالب افسرده را</p></div>
<div class="m2"><p>زنده ی جاوید سازد مرده را</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>زخم تو بر جان من همیون بود</p></div>
<div class="m2"><p>زخم تو این مرهمت پس چون بود</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>آتشت اینست اگر ای شاه من</p></div>
<div class="m2"><p>هفت دوزخ باد جولانگاه من</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گر بود این زخمت ای سلطان داد</p></div>
<div class="m2"><p>تیغ تو بر تارکم جاوید باد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آتشت شمع است من پروانه ام</p></div>
<div class="m2"><p>در هوای زخم تو دیوانه ام</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هرچه خواهی آتش ای آتش فروز</p></div>
<div class="m2"><p>در من افکن خانمان من بسوز</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>آتشی بر جسم و جان من فکن</p></div>
<div class="m2"><p>ریشه ی هستی من از بن بکن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گل بود زخم تو و بلبل منم</p></div>
<div class="m2"><p>عندلیب آسا گرفتار گلم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>عاشقم من بر تو و کردار تو</p></div>
<div class="m2"><p>جان فدای نور تو و نار تو</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چون جراحت از تو باشد مرهم است</p></div>
<div class="m2"><p>مرهم از غیر تو زهر ارقم است</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>درد تو بر جان من درمان بود</p></div>
<div class="m2"><p>سیف و خنجر لاله و ریحان بود</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>یک جهان جانی خوش ارزان یافتم</p></div>
<div class="m2"><p>زخم دیگر زن که چندان یافتم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>این اثرهایی ز زخم و آتش است</p></div>
<div class="m2"><p>بلکه از دست نگار دلکش است</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>زخم و آتش از تو چون باشد نکوست</p></div>
<div class="m2"><p>خوی فاعل ساری اندر فعل اوست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>هرچه از زیبا رسد زیبا بود</p></div>
<div class="m2"><p>زهر از شیرین لبان حلوا بود</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>گرچه در دشنام صد زهر اندر است</p></div>
<div class="m2"><p>از لب شیرین ز شکر بهتر است</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بوسه ای از آن نگار پارکین</p></div>
<div class="m2"><p>بدتر از دشنام و با دشنام بین</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>زخم و مرهم در بر دانا یکی ست</p></div>
<div class="m2"><p>گر تفاوت هست آن هم اند کی ست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>امتیاز این دو از فاعل بود</p></div>
<div class="m2"><p>در میانشان فرق از این حاصل بود</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چون تویی فاعل جراحت راحت است</p></div>
<div class="m2"><p>با تو گلخن باغ و دوزخ جنت است</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>در میان عابد و شاه وجود</p></div>
<div class="m2"><p>زین نمط بگذشت بس گفت و شنود</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>تا زمین و آسمان پرنور شد</p></div>
<div class="m2"><p>حلقه ی کروبیان پرشور شد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>در میان جان و جانان رازهاست</p></div>
<div class="m2"><p>خامه و دفتر ورا محرم کجاست</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>دوست را با دوست باشد صد سخن</p></div>
<div class="m2"><p>کی توانش گفت در هر انجمن</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>راستی در خلوت جانان و جان</p></div>
<div class="m2"><p>جسم هم محرم نباشد در میان</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>در ستایش بود عارف تا سحر</p></div>
<div class="m2"><p>کردم اندر طاقدیسش مختصر</p></div></div>