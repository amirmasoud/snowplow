---
title: >-
    بخش ۱۱۰ - در بیان گردیدن مجنون گرد سگ کوی لیلی
---
# بخش ۱۱۰ - در بیان گردیدن مجنون گرد سگ کوی لیلی

<div class="b" id="bn1"><div class="m1"><p>هر سگی در کوی او دارد گذار</p></div>
<div class="m2"><p>درد او یارب به جان من گذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دید مجنون را یکی روزی که گشت</p></div>
<div class="m2"><p>او همی گرد سگی در طرف دشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک پای او همی برداشتی</p></div>
<div class="m2"><p>گه به دیده گه به سر بگذاشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت ای مجنون چه باشد این جنون</p></div>
<div class="m2"><p>این چه فن است از جنون ای ذیفنون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت مجنون از جنونم نیست این</p></div>
<div class="m2"><p>این سگ سر منزل لیلی است این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چونکه این سگ اندر آن کو آشناست</p></div>
<div class="m2"><p>خاک پایش طوطیای چشم ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه در ظاهر کنم سگ را طواف</p></div>
<div class="m2"><p>نیک می بیند حقیقت موشکاف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بر دانا جهت دان معتبر</p></div>
<div class="m2"><p>بر خصوصیت نیندازد نظر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسن باشد هر کجا نغز است خوش</p></div>
<div class="m2"><p>خواه از تبت بود یا از حبش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روی زیبا هر که دارد دلکش است</p></div>
<div class="m2"><p>هر که دارد قامت رعنا خوش است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواه باشد از حبش یا از خطا</p></div>
<div class="m2"><p>خواه باشد شاهزاده یا گدا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نغمه ی خوش دلکش است و دلفریب</p></div>
<div class="m2"><p>خواه از بربط بود یا عندلیب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دانش آید مقصد ای عالیجناب</p></div>
<div class="m2"><p>خواه از یونان بود یا فاریاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>علم از عالم فراگیر ای ولی</p></div>
<div class="m2"><p>خواه بن قسطا بود یا بوعلی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرد بازرگان ز سودا جست سود</p></div>
<div class="m2"><p>خواه باشد از مسلمان یا یهود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون جهت در نزد دانا شد قبول</p></div>
<div class="m2"><p>رو بکن ترک ملامت ای فضول</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>متحد باشد چه مقصد ای فتی</p></div>
<div class="m2"><p>گر بود راه طلب از هم جدا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کاروان مصر و عمان نجد و شام</p></div>
<div class="m2"><p>جمله را بطحا بود آخر مقام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر که ره گم کرد هم در این سفر</p></div>
<div class="m2"><p>هم بیفتادش سوی مقصد گذر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اندرین ره نیست بی اجر و ثواب</p></div>
<div class="m2"><p>زین خطا والله اعلم بالصواب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرکه را بینی پس ای یار نکو</p></div>
<div class="m2"><p>در رهی رو مقصد راهش بجو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زان طلب حیثیت قطع طریق</p></div>
<div class="m2"><p>با تو گر باشد یکی نعم الرفیق</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر رفیق ره نباشد گو مباش</p></div>
<div class="m2"><p>چون رفیق منزلست و خانه تاش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ور ندانی مقصدش هم لب ببند</p></div>
<div class="m2"><p>چون نئی آگه از او براو مخند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شاید او هم مقصدی دارد نکو</p></div>
<div class="m2"><p>سوی مقصد زین ره آورده است رو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای بسا رند خراباتی مست</p></div>
<div class="m2"><p>کو نداند سر ز پا و پا ز دست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر برآرد یک سحر از دل فغان</p></div>
<div class="m2"><p>نالد از افغان او هفت آسمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر زبان خود گشاید در گله</p></div>
<div class="m2"><p>در ملایک افکند صد ولوله</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آهی از دل گر برآرد صبحگاه</p></div>
<div class="m2"><p>عرش را در لرزه اندازد ز راه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر بگوید یا ربی او در خطاب</p></div>
<div class="m2"><p>آیدش لبیک عبدی در جواب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مست باشد او ولی نی مست می</p></div>
<div class="m2"><p>مست باشد از شراب خاص وی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر نه او را پای باشد نه سری</p></div>
<div class="m2"><p>پا و سر داده به راه دلبری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر بود بیخود ز عطر بوی اوست</p></div>
<div class="m2"><p>ور پریشانیست از گیسوی اوست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر به شیدایی برآورده است سر</p></div>
<div class="m2"><p>هم ندارد خود ز شیدایی خبر</p></div></div>