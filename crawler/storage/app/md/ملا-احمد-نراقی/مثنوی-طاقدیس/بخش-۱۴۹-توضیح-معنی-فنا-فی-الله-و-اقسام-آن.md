---
title: >-
    بخش ۱۴۹ - توضیح معنی فناء فی الله و اقسام آن
---
# بخش ۱۴۹ - توضیح معنی فناء فی الله و اقسام آن

<div class="b" id="bn1"><div class="m1"><p>این نه معنی فنا فی الله بود</p></div>
<div class="m2"><p>هرکسی با این فنا همره بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلکه آن باشد که این گردد عیان</p></div>
<div class="m2"><p>بر تو چون خورشید اندر آسمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خویش را بینی چراغی کور کور</p></div>
<div class="m2"><p>بل فزونتر پیش آن تابنده هور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی چه آن دیدن که می بیند حکیم</p></div>
<div class="m2"><p>از قیاس سست و برهان سقیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی چه آن دیدن که شب بیند خیال</p></div>
<div class="m2"><p>اختوران را پیش خود اندر سگال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانکه باشد این دو آن را در نظر</p></div>
<div class="m2"><p>می کند برهان ز یک نفی اثر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلکه چون دیدم در ایام تموز</p></div>
<div class="m2"><p>آفتاب اندر فلک در نیمروز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می نبیند در فلک جز آفتاب</p></div>
<div class="m2"><p>نی ز اختر ذات می بیند نه زاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دل عارف کند چون حق ظهور</p></div>
<div class="m2"><p>چون ظهور نور او در کوه طور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کوه انیت نماید ریز ریز</p></div>
<div class="m2"><p>می نماند از خود آنجا هیچ چیز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ذات او پنهان شود در صد غما</p></div>
<div class="m2"><p>چون ستاره پیش خورشید سما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صبح صادق از دلش سر بر زند</p></div>
<div class="m2"><p>آفتاب از مطلع جان سر زند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آفتاب کبریاء ذوالجلال</p></div>
<div class="m2"><p>خود شود پنهان چو پیش خور نهال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خور برآید اختوران پنهان شوند</p></div>
<div class="m2"><p>شیر آید روبهان بیرون روند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هستی مطلق چه نور افکن شود</p></div>
<div class="m2"><p>نیستی خود برو روشن شود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همچنین چون نور افعال صفات</p></div>
<div class="m2"><p>پرتو اندازد به دل از شش جهات</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیست بیند فعل خویش و ذات خویش</p></div>
<div class="m2"><p>نی اثر چون در بر صرصر حشیش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مختصر آن کز تجلیهای ذات</p></div>
<div class="m2"><p>در تجلیهای افعال و صفات</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ذات و فعل و وصف او گردد نهان</p></div>
<div class="m2"><p>چون ستاره پیش خورشید جهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نی ز فعل خود اثر بیند نه ذات</p></div>
<div class="m2"><p>نی ببیند از برای خود صفات</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جملگی گردند معدوم و هبا</p></div>
<div class="m2"><p>پیش آن ذات و صفات فعلها</p></div></div>