---
title: >-
    بخش ۱۵۸ - عشق و جان بازی پروانه در پای شمع
---
# بخش ۱۵۸ - عشق و جان بازی پروانه در پای شمع

<div class="b" id="bn1"><div class="m1"><p>همچو آن پروانه شمع افروختن</p></div>
<div class="m2"><p>هر دمی خود را بنوعی سوختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنگرید ای دوستان پروانه را</p></div>
<div class="m2"><p>دادن جان در ره جانانه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونکه بیند شمع را اندر وثاق</p></div>
<div class="m2"><p>گردد اول دور آن از اشتیاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر نهد آنگه به پایش از نیاز</p></div>
<div class="m2"><p>هین بزن پا بر سرم ای دلنواز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس گشاید بال و پر از انبساط</p></div>
<div class="m2"><p>رقص آرد دور آن با صد نشاط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وانگهی خود را زند بر نار آن</p></div>
<div class="m2"><p>جان دهد در آتش رخسار آن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوزد و افتد به پایش سوخته</p></div>
<div class="m2"><p>هم پر و هم بال آن افروخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیم جان خیزد ز جا با صد شعف</p></div>
<div class="m2"><p>خود بر آتش افکند از هر طرف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سوزد و گوید میان انجمن</p></div>
<div class="m2"><p>هین بسوزانم خوشا این سوختن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این نه آتش آب حیوان است این</p></div>
<div class="m2"><p>سوختن نی زاتش جان است این</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوزد و گوید به صد وجد و طرب</p></div>
<div class="m2"><p>شد به کام من جهان ای صد عجب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آتش اندر وی گرفت و نار شد</p></div>
<div class="m2"><p>نار رفت و لجه ی انوار شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سوخت پا تا سر خنک این سوختن</p></div>
<div class="m2"><p>عاشقی باید از آن آموختن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عاشقی کو ترسد از شمشیر و تیغ</p></div>
<div class="m2"><p>عشق نبود ای دریغا ای دریغ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عشق از پروانه باید یاد داشت</p></div>
<div class="m2"><p>ورنه خود از عاشقی آزاد داشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عاشقان را جسم و جان در کار نیست</p></div>
<div class="m2"><p>جسم و جانشان جز برای یار نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در ره او جسم و جان را خار کن</p></div>
<div class="m2"><p>خویش را از جسم و جان بیزار کن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جان فروشانند در بازار عشق</p></div>
<div class="m2"><p>از ورای عقل باشد کار عشق</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هان و هان ای دوستان باوفا</p></div>
<div class="m2"><p>پا نهید اندر بیابان وفا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خویش را در راه او فانی کنید</p></div>
<div class="m2"><p>عید قربان است قربانی کنید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تیر هرجا سینه را اسپر کنید</p></div>
<div class="m2"><p>هر کجا تیغی بسر افسر کنید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جسم و جان وقف ره جانان کنید</p></div>
<div class="m2"><p>بهر جانان ترک جسم و جان کنید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در سر کوی بقا منزل کنید</p></div>
<div class="m2"><p>جان خلاص از بند آب و گل کنید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کودکان در آب و گل بازی کنند</p></div>
<div class="m2"><p>شیرمردان فکر سربازی کنند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جان من گر عاشقی مردانه باش</p></div>
<div class="m2"><p>پیش شمع روی او پروانه باش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز آب و آتش بهر او پروا مکن</p></div>
<div class="m2"><p>ورنه رو در عاشقی پروا بکن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این ذغال تن در آن آتش فکن</p></div>
<div class="m2"><p>تا شود روشن از آن صد انجمن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نیم جانی ده عوض صدجان بگیر</p></div>
<div class="m2"><p>خاک و خل ده لؤلؤ و مرجان بگیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر همی خواهی وصال آن نگار</p></div>
<div class="m2"><p>پا بزن بر هر دو عالم مردوار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جنت و دوزخ به جانان کن نثار</p></div>
<div class="m2"><p>دوست را با جنت و دوزخ چه کار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جنت و دوزخ مرا یکسان بود</p></div>
<div class="m2"><p>جنتش سر منزل جانان بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دوست نبود آنکه می جوید بهشت</p></div>
<div class="m2"><p>بلکه گلشن جست و گلخن را بهشت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گلشن و گلخن بر عاشق یکیست</p></div>
<div class="m2"><p>گلخن و گلشن نمی داند که چیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر به گلخن جلوه ی دلدار اوست</p></div>
<div class="m2"><p>جنت او باغ او گلزار اوست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای که در فکر بهشت و کوثری</p></div>
<div class="m2"><p>دور شو تو مردک حلواخوری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>باغ جنت را اگر جویاستی</p></div>
<div class="m2"><p>جان بابا طالب خرماستی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دوست چیزی می نجوید غیر دوست</p></div>
<div class="m2"><p>جسم و جان و دنیی و عقباش اوست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جسم و جان بخشد به یک ایمای او</p></div>
<div class="m2"><p>هر دو کون افشاند اندر پای او</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>قدرتش فانی کند در قدرتش</p></div>
<div class="m2"><p>محو سازد وصف خود در حضرتش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دور اندازد ز خود این اقتدار</p></div>
<div class="m2"><p>اختیارش هرچه کرد او اختیار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نی کراهت باشدش نی اقتضا</p></div>
<div class="m2"><p>غیر ماشاء حبیبه مایشاء</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>فعل و ذات و وصف خود فانی کند</p></div>
<div class="m2"><p>در رهش این جمله قربانی کند</p></div></div>