---
title: >-
    بخش ۲۲۳ - حدیث تفکر ساعة خیر من عبادة سنه و در حدیث دیگر سبعین سنه
---
# بخش ۲۲۳ - حدیث تفکر ساعة خیر من عبادة سنه و در حدیث دیگر سبعین سنه

<div class="b" id="bn1"><div class="m1"><p>طاعت بی فکر و بی یاد حضور</p></div>
<div class="m2"><p>مرده باشد جان آن گور است گور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرف بیمعنی درخت بی ثمر</p></div>
<div class="m2"><p>ابر بی باران و بحر بی گهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طاعت بی فکر میدان ای جناب</p></div>
<div class="m2"><p>آن سخنهایی که گوید مرد خواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی از آنها مهر می خیزد نه کین</p></div>
<div class="m2"><p>نه به دنیا نفع می بخشد نه دین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای برادر دل بود خواب هوس</p></div>
<div class="m2"><p>هست فکر و یاد تو بانگ جرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دلت خوابیده شد وقت سحر</p></div>
<div class="m2"><p>مرغ فکرت را برافشان بال و پر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این دل خوابیده را بیدار کن</p></div>
<div class="m2"><p>وقت کارت رفت فکر کار کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه دل نبود بجز یک جرعه خون</p></div>
<div class="m2"><p>در درون سینه ها دارد سکون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لیک او را از ورای این جهان</p></div>
<div class="m2"><p>هست قسطی گر نمی دانی بدان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو بسدّ از نبات و از جماد</p></div>
<div class="m2"><p>قسط دارد زین دواش باشد نژاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نخله هم اندر نباتات ای حبیب</p></div>
<div class="m2"><p>باشد از حیوانی او را هم نصیب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عمه خواندش زین سبب شاه جهان</p></div>
<div class="m2"><p>بخل هم باشد عمویت ای فلان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم بود بوزینه یا اسب ای پسر</p></div>
<div class="m2"><p>مشترک در حد حیوان و بشر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جسم و دل هم هست حد مشترک</p></div>
<div class="m2"><p>در میان خیل انسان و ملک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم نصیب از غیب دارد هم شهود</p></div>
<div class="m2"><p>تارش از این بکشد و زآیینش بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روح حیوانی از آتش شد ثمر</p></div>
<div class="m2"><p>می دهد آب هرچه از خاکش خبر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وان شکنبه منبع روث و فساد</p></div>
<div class="m2"><p>از جگر خون از مثانه بول زاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرچه جسمانی ز درد و زخم و سوز</p></div>
<div class="m2"><p>هر یکی را باشد از عضوی بروز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وآنچه روحانی ز دل دارد رباط</p></div>
<div class="m2"><p>بیم و امید و غم و حزن و نشاط</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آری آری دل ز جای دیگر است</p></div>
<div class="m2"><p>در سر دلها هوای دیگر است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دل در این قالب غریبست ای حبیب</p></div>
<div class="m2"><p>رحمی آخر ای اخی بر این غریب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرغ دل از آشیان دیگر است</p></div>
<div class="m2"><p>لانه اش در بوستان دیگر است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فکر تو چون فکر رحمانی بود</p></div>
<div class="m2"><p>پیکی از اقلیم روحانی بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هست پیغامی ز یاران قدیم</p></div>
<div class="m2"><p>از برای این دل زار سقیم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یا شمیمی باشد از آن پیرهن</p></div>
<div class="m2"><p>از برای ساکن بیت الحزن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یا نسیمی باشد از دشت خطا</p></div>
<div class="m2"><p>نزد آهویی به صیاد آشنا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یا صفیر عندلیب همنفس</p></div>
<div class="m2"><p>پیش آن بلبل که باشد در قفس</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نامه ای باشد ز یاران وطن</p></div>
<div class="m2"><p>از برای این غریب ممتحن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زین سبب دلها ز فکرتهای پاک</p></div>
<div class="m2"><p>روشن و نورانیند و تابناک</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دل ز فکر پاک روشن می شود</p></div>
<div class="m2"><p>رشک صد گلزار و گلشن می شود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همچو مرغی از صفیر همنفس</p></div>
<div class="m2"><p>بال و پر برهم زند اندر قفس</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا بجایی گر گشاید بال و پر</p></div>
<div class="m2"><p>صد قفس را بشکند بر یکدگر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ور بود فکر تو ظلمانی و بد</p></div>
<div class="m2"><p>خانه ی دل تار و تیره می کند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دل شود پژمرده و بی بال و پر</p></div>
<div class="m2"><p>صحبت ناجنس آرد دردسر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دردسر گردد سر از بیگانه ای</p></div>
<div class="m2"><p>وای از نامحرمی در خانه ای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون به باغی گشت زاغی پرگشا</p></div>
<div class="m2"><p>بلبلان افتند آنجا از نوا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ور ابردشتی گوزنی برگذشت</p></div>
<div class="m2"><p>نوغزالان رم کنند از طرف دشت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آهویی پیدا شود گر از ختن</p></div>
<div class="m2"><p>آهوان سازند گردش انجمن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر به باغی بلبلی شد نغمه ساز</p></div>
<div class="m2"><p>جمله مرغان پر کنند از شوق باز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آشنایی گر به بزمی پا نهاد</p></div>
<div class="m2"><p>صد در از عشرت در آن محفل گشاد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون هوایی در دل از ملک صفاست</p></div>
<div class="m2"><p>دل به اخوان صفا زود آشناست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چون یکی زایشان به دل آرد گذار</p></div>
<div class="m2"><p>دل بوجد آید ز شوق آن دیار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شوق دل را میل آید از عقب</p></div>
<div class="m2"><p>میلها هم جذب را آمد سبب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جذب یاران را کشاند سوی دل</p></div>
<div class="m2"><p>محفل خوبان شود مشکوی دل</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دل ز نور رویشان روشن شود</p></div>
<div class="m2"><p>ساحت دل غیرت گلشن شود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دل شود مصباح پیش رویشان</p></div>
<div class="m2"><p>مشک و عنبر می شود از بویشان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از پس مصباحی اختر می شود</p></div>
<div class="m2"><p>وانگهی خورشید انور می شود</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بگذرد آنگه ز خورشیدی دور</p></div>
<div class="m2"><p>افکند جرم و شود دریای نور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نور ابیض می شود پا تا به سر</p></div>
<div class="m2"><p>پرتو افکندش به اعضای دگر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از پس این نور اخضر می شود</p></div>
<div class="m2"><p>از بیان و شرح برتر می شود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تن ز نورش جمله نورانی شود</p></div>
<div class="m2"><p>در لباس جسم روحانی بود</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سایه اش نبود ولیکن پایه اش</p></div>
<div class="m2"><p>عالمی را جا دهد در سایه اش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در محیطش سیر و در مرکز مقام</p></div>
<div class="m2"><p>از محیط و مرکزش هر دو پیام</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>صورتش جسمی و معنی روح پاک</p></div>
<div class="m2"><p>گویدش روح القدس روحی فداک</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>در لباس جسم و شخص شخص روح</p></div>
<div class="m2"><p>هردم از روحانش آمد صد فتوح</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چیستی آیا تو ای سرگشته دل</p></div>
<div class="m2"><p>هرچه هستی نیستی زین آب و گل</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>باشد از جسمی دگر پیرایه ات</p></div>
<div class="m2"><p>ور بود از آب و از گل مایه ات</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نور را با آب و گل آمیختند</p></div>
<div class="m2"><p>طرح خلوتخانه ی دل ریختند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نور غالب شد اگر برآب و گل</p></div>
<div class="m2"><p>نور گردد سر به سر اجزای دل</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>آب و گل گر چیره شد بر نور پاک</p></div>
<div class="m2"><p>پای تا سر می شود دل تیره خاک</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ای دریغا قدر دل نشناختیم</p></div>
<div class="m2"><p>در ره اول به مفتش باختیم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>باری ای دل خود تو میدان قدر خود</p></div>
<div class="m2"><p>هین مکن در ابر پنهان قدر خود</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تا بکی این ماه باشد در سحاب</p></div>
<div class="m2"><p>چند باشد در کسوف این آفتاب</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کن رها از عقده این خورشید را</p></div>
<div class="m2"><p>پرگشا این باز پر اسفید را</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چند باشی هم تک مشتی ذباب</p></div>
<div class="m2"><p>بال و پر بگشا از این زرین عقاب</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چند شهبازی اسیر خرمگس</p></div>
<div class="m2"><p>تا بکی عنقای قاف اندر قفس</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>پر گشا در عرش و پروازش ببین</p></div>
<div class="m2"><p>در بر سلطان جان نازش ببین</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>پس ببین در لامکانش آشیان</p></div>
<div class="m2"><p>طوفگاهش را نگر اقلیم جان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تنگ پیش جلوه اش این نه خیام</p></div>
<div class="m2"><p>باشدش در سدره و طوبی مقام</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>طایران قدس هم پرواز او</p></div>
<div class="m2"><p>بلبلان عرش هم آواز او</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>لیس الاذالعمری یا حبیب</p></div>
<div class="m2"><p>قلته والله فی ذاک الرقیب</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>رخنه ای بگشا در دل ای قرین</p></div>
<div class="m2"><p>پس گواه آنچه من گفتم ببین</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>همچنانکه آن جوان خارکن</p></div>
<div class="m2"><p>روشنش شد صدق این گفتار من</p></div></div>