---
title: >-
    بخش ۶۲ - جواب زن به شوهر خود
---
# بخش ۶۲ - جواب زن به شوهر خود

<div class="b" id="bn1"><div class="m1"><p>زن جوابش داد کای مرد خدا</p></div>
<div class="m2"><p>ای که هستی در عبادت پارسا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه فرمودی درستوست ای رفیق</p></div>
<div class="m2"><p>لیک گم کردی در این وادی طریق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه او از تو توکل خواسته</p></div>
<div class="m2"><p>هم وی این دکان کسب آراسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فهو حسبی را شنیدستی از آن</p></div>
<div class="m2"><p>و ابتغوا من فضله را هم بخوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن توکل از دل دانای توست</p></div>
<div class="m2"><p>کسب و حرفت کار دست و پای توست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواستند از دل توکل ای همام</p></div>
<div class="m2"><p>وز جوارح کسب با سعی تمام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رو زمین از خار و از خس پاک کن</p></div>
<div class="m2"><p>آب ده پس دانه اندر خاک کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تکیه کن آنگه به لطف کردگار</p></div>
<div class="m2"><p>گو خدایا دانه از خاکم برآر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قفل بر زن بر درد کان به شب</p></div>
<div class="m2"><p>وانگهی بسپار دکان را به رب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از توکل هیچکس بابا نشد</p></div>
<div class="m2"><p>تاره تزویج را پویا نشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا نگردی گرد تزویج و سفاد</p></div>
<div class="m2"><p>کی توکل در برت کودک نهاد</p></div></div>