---
title: >-
    بخش ۳۴ - دنباله داستان شاه و گرفتاری او به دست زنگیان
---
# بخش ۳۴ - دنباله داستان شاه و گرفتاری او به دست زنگیان

<div class="b" id="bn1"><div class="m1"><p>زنگیان را چون تصور کرده شاه</p></div>
<div class="m2"><p>دوستان مهربان نیکخواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت با ایشان سخنهای نهفت</p></div>
<div class="m2"><p>آنچه را نتوان بغیر از دوست گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرّ پنهان شهنشاه جهان</p></div>
<div class="m2"><p>زنگیان را زان سخنها شد عیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رشته ی آزرم خود بگسیختند</p></div>
<div class="m2"><p>فاش و بی پروا به شه آویختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد اسیر زنگیان آن شاه راد</p></div>
<div class="m2"><p>ای سپهر از جور بیداد تو داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گردنش را پالهنگ انداختند</p></div>
<div class="m2"><p>دست او بستند و محکم ساختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردنش را پالهنگ پالهنگ</p></div>
<div class="m2"><p>گرده اش وقف درفش عار و ننگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می کشیدندش به روی خار و خاک</p></div>
<div class="m2"><p>می زدندش حربه های سوزناک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>او خیو بر روی او انداختی</p></div>
<div class="m2"><p>وین به قتلش دم بدم پرداختی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گه به چوبش می زدند و گه به سنگ</p></div>
<div class="m2"><p>اینچنین راندند او را تا به زنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس در آنجا از پس اشکنجها</p></div>
<div class="m2"><p>در چهی کردند سلطان را رها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چاهی آن را قعر و پایان ناپدید</p></div>
<div class="m2"><p>نی فرج زان کام و نه رستن امید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس روان گشتند سوی باختر</p></div>
<div class="m2"><p>سوی ملک آن شه والاگهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آتش ظلم و ستم افروختند</p></div>
<div class="m2"><p>هرچه دیدند اندر آنجا سوختند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرنگون شد تخت شاه مستطاب</p></div>
<div class="m2"><p>قصرها ویران شد ایوانها خراب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هم رعیت هم سپه برباد رفت</p></div>
<div class="m2"><p>نام شاه و کشورش از یاد رفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آسمان بارید بر باغش تگرگ</p></div>
<div class="m2"><p>ریخت از هم نخل او را شاخ و برگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رسته گزها جای سرو و نسترن</p></div>
<div class="m2"><p>خار و خس رویید جای یاسمن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رفت فرخ فر هما زان مرز و بوم</p></div>
<div class="m2"><p>آشیانش شد مقام بوم شوم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عندلیب از بوستان پرواز کرد</p></div>
<div class="m2"><p>زاغ زشت آنجا سخن آغاز کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شد چراگاه غزالان تتار</p></div>
<div class="m2"><p>جوغ گوران و گوزنان را قرار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای رفیقان حال ما بی اشتباه</p></div>
<div class="m2"><p>سخت می ماند به حال پادشاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>می رویم از باده ی غفلت خراب</p></div>
<div class="m2"><p>در ره دنیا به صد شور و شتاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حزب شیطان از یسار و از یمین</p></div>
<div class="m2"><p>بهر صید ما نشسته در کمین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هان و هان ای راهرو هشیار شو</p></div>
<div class="m2"><p>دورتر زین راه ناهنجار شو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای تو در اقلیم امکان پادشاه</p></div>
<div class="m2"><p>بلکه صد شه در درونت با سپاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای تو زینت بخش اقلیم وجود</p></div>
<div class="m2"><p>ای ملایک کرده در پیشت سجود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای کمینه چاکرت چرخ بلند</p></div>
<div class="m2"><p>گردن گردن کشانت در کمند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای طفیل هستیت هم ماه و مهر</p></div>
<div class="m2"><p>ای برای خدمتت گردان سپهر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای زمینت جای چرخت زیر پای</p></div>
<div class="m2"><p>ای سرت کوتاه و فرقت عرش سای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای گرامی گوهر بحر وجود</p></div>
<div class="m2"><p>ای تو محرم در غرقگاه شهود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>الله الله قیمت خود را بدان</p></div>
<div class="m2"><p>خویش را مفروش ارزان ای جوان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>الله الله زود از این ره بازگرد</p></div>
<div class="m2"><p>ساعتی با عقل و هوش انباز گرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زنگیان اند اندرین ره در کمین</p></div>
<div class="m2"><p>می روی تا کی بگو غافل چنین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>حیف باشد چون تویی در دست زنگ</p></div>
<div class="m2"><p>حیف باشد چون تویی در چاه تنگ</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>حیف باشد چون تو در قید اسار</p></div>
<div class="m2"><p>حیف باشد چون تو گردد خاکسار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گو چه خوردستی که مستی اینچنین</p></div>
<div class="m2"><p>آسمان را می ندانی از زمین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>داروی بیهوشیت آیا که داد</p></div>
<div class="m2"><p>قفلها بر چشم و بر گوشت نهاد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کاین چنین غافل روی ره روز و شب</p></div>
<div class="m2"><p>می نیندازی نگاهی در عقب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نی پس ره بینی و نی پیش را</p></div>
<div class="m2"><p>نی کسی بشناسی و نی خویش را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>باز گرد ای جان از این ره باز گرد</p></div>
<div class="m2"><p>لحظه ای با عقل خود دمساز گرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>عقل می گوید مرو این راه را</p></div>
<div class="m2"><p>چنگ زنگی در میفکن شاه را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از کف خود ملک جاویدان مده</p></div>
<div class="m2"><p>ملک جاویدان زکف ارزان مده</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دولت سرمد تورا آماده است</p></div>
<div class="m2"><p>خوان نعمت تا ابد بنهاده است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>رو از این دولت متاب ای خوبروی</p></div>
<div class="m2"><p>دست از این نعمت بکش ای نیکخوی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خود تو می دانی که این ره راه نیست</p></div>
<div class="m2"><p>ور بود پایان آن جز چاه نیست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>لیک شیطان دانشت از یاد برد</p></div>
<div class="m2"><p>دفتر داناییت را باد برد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چند گویی باز گردم بعد از آن</p></div>
<div class="m2"><p>سالها بگذشت و می گویی همان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هرچه رفتی راه برگشتن دراز</p></div>
<div class="m2"><p>می شود کی می توانی گشت باز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>راه گردد دور تن سست و ضعیف</p></div>
<div class="m2"><p>می شود مرکب تورا لنگ و نحیف</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا بکی فردا و پس فردا کنی</p></div>
<div class="m2"><p>خویش را رسوا از این سودا کنی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>روزگاری شد که فردا گفته ای</p></div>
<div class="m2"><p>باز در جای نخستین خفته ای</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>من ندانم کی بود فردای تو</p></div>
<div class="m2"><p>وای تو ای وای تو ای وادی تو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هین مگو فردا و پس فردا دگر</p></div>
<div class="m2"><p>بلکه آید عمر تو فردا بسر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>قدر عمر خود چرا نشناختی</p></div>
<div class="m2"><p>ضایعش کردی و مفتش باختی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مایه ی عمری که ندهی صد جهان</p></div>
<div class="m2"><p>گر دهندت در بهای نرخ آن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>اندک اندک نی بها و نی ثمن</p></div>
<div class="m2"><p>می فروشی جمله را ای بوالحسن</p></div></div>