---
title: >-
    بخش ۲۳۰ - حدیث:المجاز قنطرة الحقیقه
---
# بخش ۲۳۰ - حدیث:المجاز قنطرة الحقیقه

<div class="b" id="bn1"><div class="m1"><p>عشق باشد لیک اگر نی صد مرض</p></div>
<div class="m2"><p>در مرض هم نفس دون را صد غرض</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه باشد صد غرض در آن نهان</p></div>
<div class="m2"><p>عشق باشد آن مرض نبود بدان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق خوانی آن مرضها را عجب</p></div>
<div class="m2"><p>شرم کو و کو حیا و کو ادب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از چه نام حق گذاری بر وثن</p></div>
<div class="m2"><p>این ورمها را چرا گویی سمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق باشد گر حقیقت گر مجاز</p></div>
<div class="m2"><p>از غرضها هست پاک و بی نیاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبود اینها عشق ناپاکی بود</p></div>
<div class="m2"><p>خودپرستی و هوسناکی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق پاکست وز ناپاکی جداست</p></div>
<div class="m2"><p>این جهان کشتی و عشقش ناخداست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق خود آلوده ی اقذار نیست</p></div>
<div class="m2"><p>با حقیقت یا مجازش کار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر هوایی هم بود اندر سری</p></div>
<div class="m2"><p>چونکه عشق آید کند سر را بری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق باشد آتش و هرجا فتاد</p></div>
<div class="m2"><p>پاک سازد هر پلیدی را فساد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر بود در جان تو سیصد مرض</p></div>
<div class="m2"><p>یا بدل باشد تورا هفتصد غرض</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چونکه عشق آید بسوزد سربسر</p></div>
<div class="m2"><p>از غرض نی از مرض ماند اثر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عشق پیش آهنگ راه جنت است</p></div>
<div class="m2"><p>کاروانسالار ملک و دولت است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر مسافر را که عشق آمد دلیل</p></div>
<div class="m2"><p>رخت او را می کشد تا سلسبیل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همچنانکه آن جوان خارکن</p></div>
<div class="m2"><p>عشق بردش تا به صدر انجمن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عشق کردش رهنمایی از نخست</p></div>
<div class="m2"><p>تا رسانیدش به مقصد جلد و چست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برد او را بر سر ره باز داشت</p></div>
<div class="m2"><p>بر سر راه بت طناز داشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر سر ره چونکه دیدش آن نگار</p></div>
<div class="m2"><p>از کمان ابروان کردش شکار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پس دو زلف تابدار چون کمند</p></div>
<div class="m2"><p>با دو صد نازش بگردن درفکند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پس بسوی خود کشیدش بی درنگ</p></div>
<div class="m2"><p>پس گرفتش اندر آغش تنگ تنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نفخه ای اندر مشام او رسید</p></div>
<div class="m2"><p>جذبه ای آمد در آغوشش کشید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جرعه ای از خم وحدت نوش کرد</p></div>
<div class="m2"><p>شاه و شاهی جمله را فرموش کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صحبت شهزاده اش از یاد رفت</p></div>
<div class="m2"><p>ترک او کرد و پی صیاد رفت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>باز آمد درربودش از ذباب</p></div>
<div class="m2"><p>آب حیوان دید بگذشت از سراب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آفتابش سر زد از کهسار دل</p></div>
<div class="m2"><p>شد از آن روشن در و دیوار دل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خور برآمد گشت پنهان اختوران</p></div>
<div class="m2"><p>روز روشن آمد و شب شد نهان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خویشتن را از جنیبت درفکند</p></div>
<div class="m2"><p>افسر از سر جامه ها از بر فکند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پابرهنه جانب کهسار رفت</p></div>
<div class="m2"><p>قطره ای شد سوی دریا بار رفت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ماهئی شد جانب عمان دوید</p></div>
<div class="m2"><p>ذره ای تا آفتاب جان رسید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رفت و در بر جمله ی اغیار بست</p></div>
<div class="m2"><p>دیده بر دیدار آن دلدار بست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بست در هم خویش و هم بیگانه را</p></div>
<div class="m2"><p>حلقه بر در زد در آن خانه را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بر در آن خانه روز و شب نشست</p></div>
<div class="m2"><p>سر به سنگ آستانش می شکست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا گشودندش در و دادند بار</p></div>
<div class="m2"><p>برگرفتندش به دامان و کنار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شد از آن برتر که بتوانیم گفت</p></div>
<div class="m2"><p>پس همان بهتر که باقی را نهفت</p></div></div>