---
title: >-
    بخش ۱۱۱ - حکایت شوریده ای که به کلیسای نصاری رفت
---
# بخش ۱۱۱ - حکایت شوریده ای که به کلیسای نصاری رفت

<div class="b" id="bn1"><div class="m1"><p>بود یک شوریده در شهر هری</p></div>
<div class="m2"><p>از بد و از نیک این عالم بری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسته ای از دام ننگ و قید و نام</p></div>
<div class="m2"><p>خودپرستی را بخود کرده حرام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه گاهی در خراباتش گذر</p></div>
<div class="m2"><p>گاه دیگر میکده او را مقر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلوت او سردم رندان پاک</p></div>
<div class="m2"><p>منزل او محفل هر سینه چاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از قدوم او فقیهی نیکنام</p></div>
<div class="m2"><p>داد تشریف هری با احتشام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهل شهر اندر لقایش سربسر</p></div>
<div class="m2"><p>می ربودندی سبق از یکدگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم وضیع و هم شریف از هر طرف</p></div>
<div class="m2"><p>ره سپر سویش پی درک شرف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد روان آن مردک شوریده نیز</p></div>
<div class="m2"><p>تا زیارتگاه آن شیخ عزیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوهری آرد به کف از آن صدف</p></div>
<div class="m2"><p>تا ز فیض خدمتش یابد شرف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چونکه شیخ از مقدمش آگاه شد</p></div>
<div class="m2"><p>جان او با صد غضب همراه شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت ای ناپاک ضایع روزگار</p></div>
<div class="m2"><p>ای که دارد دین اسلام از تو عار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با مسلمانانت آمیزش ز چیست</p></div>
<div class="m2"><p>رو که این محفل مقام چون تو نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با نصاری بایدت آمیختن</p></div>
<div class="m2"><p>خون تو اندر کلیسا ریختن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هین برو ای گبر از مسجد برون</p></div>
<div class="m2"><p>هین برو سوی کلیسا ای زبون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رو برون راه کلیسا پیش گیر</p></div>
<div class="m2"><p>زمره ی نصرانیان هم کیش گیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هست ز امثال تو دین خوار و ذلیل</p></div>
<div class="m2"><p>هست مسجد در حنین و در عویل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چونکه آن شوریده بشنید این سخن</p></div>
<div class="m2"><p>بی سخن آمد برون زان انجمن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زمره ی یاران خود آواز کرد</p></div>
<div class="m2"><p>ساز رفتن تا کلیسا ساز کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دور خود خاصان خود را گرد کرد</p></div>
<div class="m2"><p>شد به آهنگ کلیسا ره نورد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفت ای یاران بزرگی را سخن</p></div>
<div class="m2"><p>بر زبان بگذشته اندر انجمن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>واجب آمد امتثال امر او</p></div>
<div class="m2"><p>کی بود مرد خدا بیهوده گو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفته ی ارباب دین افسانه نیست</p></div>
<div class="m2"><p>تا ببینم سر این فرموده چیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پس به عزم آن کلیسا شد روان</p></div>
<div class="m2"><p>بود آن شوریده در شهر ازمهان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چونکه ترسایان از این آگه شدند</p></div>
<div class="m2"><p>در کلیسا جملگی جمع آمدند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>راهب تثلیث و موسائی تمام</p></div>
<div class="m2"><p>پادری و مادری و خاص و عام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زیور و پیرایه بیحد خواستند</p></div>
<div class="m2"><p>زان در و دیوار دیر آراستند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وندران هم زنگ و هم ناقوسها</p></div>
<div class="m2"><p>در خروش آورده بر ناموسها</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکطرف بر طاقها با فر و زیب</p></div>
<div class="m2"><p>خاجها بنهاده از یکسو صلیب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>صورت عیسی و مریم در فراز</p></div>
<div class="m2"><p>آن دو صورت دیر ایشان را طراز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آن دو تمثال اندرین محفل به صدر</p></div>
<div class="m2"><p>چون میان اختوران تابنده بدر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جمله توراسایان رده اندر رده</p></div>
<div class="m2"><p>در برابرشان ستاده صف زده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صورتی از نسخ اصنام و ظلام</p></div>
<div class="m2"><p>سوی آنها روی آن قوم لئام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>صورتی پا تا به سر در انفعال</p></div>
<div class="m2"><p>انفعال فعل قوم بدفعال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>صورتی از شرم و خجلت در عرق</p></div>
<div class="m2"><p>تا چرا خواندندشان مالیس حق</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>صورتی از جنس ما هم ینحتون</p></div>
<div class="m2"><p>زمره ی نصرانیانش یعبدون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یعبدونها وهی بالقول الصریح</p></div>
<div class="m2"><p>حیث یسمع من له السمع الصحیح</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>با زبانی کو شناسد اهل دل</p></div>
<div class="m2"><p>نشنود آن را بغیر از گوش دل</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نشهد ان الله ربی ما ولد</p></div>
<div class="m2"><p>لا و لا کان له کفوا احد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بی زبانان جهان را صد زبان</p></div>
<div class="m2"><p>نی سخن در پرده نی فاش و عیان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>این گواهی را همه گویاستند</p></div>
<div class="m2"><p>زین گواهی زنده و برجاستند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هیچ ذره در جهان پیدا نشد</p></div>
<div class="m2"><p>گر به توحید خدا گویا نشد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از عدم چیزی نیامد در وجود</p></div>
<div class="m2"><p>کو گواهی صدق بر وحدت نبود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>برگها و سبزه های بوستان</p></div>
<div class="m2"><p>هر ورق را سوره ی توحید دان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هر گیاهی کز زمین سر بر زند</p></div>
<div class="m2"><p>فاش گوید قل هوالله احد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در فلک بین اختوران بیشمار</p></div>
<div class="m2"><p>نور وحدت از جبین شان آشکار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هر سو مویی که بینی در جهان</p></div>
<div class="m2"><p>جملگی باشد نشان زان بی نشان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کثرت ما شاهد یکتاییش</p></div>
<div class="m2"><p>ربط با هم آیت داناییش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ای بزرگ آن پادشاه بی نشان</p></div>
<div class="m2"><p>کش بود هرچیز می بینی نشان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>من چه گویم این دهانم چاک باد</p></div>
<div class="m2"><p>بر سر و مدح و ثنایم خاکباد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بی نشان ماییم و هستی های ما</p></div>
<div class="m2"><p>وین عدمهایی که شد هستی نما</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نی کسی یابد نشان از بودمان</p></div>
<div class="m2"><p>آتشی نی گرمی و نی دودمان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نی نشانی در خود از هستی پدید</p></div>
<div class="m2"><p>اینچنین هستی بگو یا رب که دید</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ما نه خود هستیم و نی خود نیستیم</p></div>
<div class="m2"><p>می ندانم کیستیم و چیستیم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هست اگر بودیم کی فانی شدیم</p></div>
<div class="m2"><p>کی اسیر جهل و نادانی شدیم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کی به زندان مکان بودیم بند</p></div>
<div class="m2"><p>کی زمان کردی بگردنمان کمند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هست را با این گرفتاری چه کار</p></div>
<div class="m2"><p>هست را با ذلت و خواری چه کار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هست را کی نیستی تاری شود</p></div>
<div class="m2"><p>چیزی از خود چون شود عاری شود</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در عدم هستیم برگو از کجاست</p></div>
<div class="m2"><p>این بیا و این برو این داد خواست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اینهمه فریادهای و هو زکیست</p></div>
<div class="m2"><p>این بگیر و این بده از بهر چیست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ور بگویی شد مخمر از حکم</p></div>
<div class="m2"><p>این نمایش از وجود و از عدم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>عقل باور کی کند زیرا که نیست</p></div>
<div class="m2"><p>ماحصل در هر مزاج هست و نیست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گر بود چیزی ورای این سه است</p></div>
<div class="m2"><p>گرچه گویندش پی تفهیم هست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>من نمی دانم ولی خود چیست آن</p></div>
<div class="m2"><p>هرچه هست از صنع یزدانی ست آن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>من نمی دانم نه تو ای یار من</p></div>
<div class="m2"><p>این نه کار تو بود نی کار من</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>این مقام حیرت اندر حیرتوست</p></div>
<div class="m2"><p>حیرت اینجا عین علم و حکمت است</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>این خنک آنکو بحیرت باز شد</p></div>
<div class="m2"><p>فارغ از اوهام و از پندار شد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>پای استدلال و برهان را شکست</p></div>
<div class="m2"><p>از کمند سست و استدلال جست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>لنگ باشد پای برهان و دلیل</p></div>
<div class="m2"><p>مستدل خود زار و رنجور و علیل</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>مستدل رنجور و زار و مبتلا</p></div>
<div class="m2"><p>چوب استدلالش اندر کف عصا</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>می رود با این عصا گامی سه چار</p></div>
<div class="m2"><p>می فتد گاه از یمین و گه یسار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>می رود افتان و خیزان چند گام</p></div>
<div class="m2"><p>گاه باشد در قعود و گه قیام</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>عاقبت آید به بستر باز پس</p></div>
<div class="m2"><p>خسته و پیچیده در حلقش نفس</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نی ز سیرش منزلی گردیده طی</p></div>
<div class="m2"><p>نی بسویش مقصد از آن برده پی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نی ز میدان برده گویی زین عصا</p></div>
<div class="m2"><p>نی سبق بگرفته بر پیری دوتا</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>با خری گر سبقت اندیشی گرفت</p></div>
<div class="m2"><p>آن خر لاغر بر آن پیشی گرفت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بایدت گر سوی مقصد تاختن</p></div>
<div class="m2"><p>باید از کف این عصا انداختن</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ترک کردن تکیه بر این چوب سست</p></div>
<div class="m2"><p>دفع رنجوری ز خود کردن نخست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چیست رنجوری تو اوصاف تو</p></div>
<div class="m2"><p>تیره زانها گشته آب صاف تو</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بحث ما ز امراض پنهانی بود</p></div>
<div class="m2"><p>وان مرض حمای جسمانی بود</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>از هوای نفس و آن طبع عفن</p></div>
<div class="m2"><p>کشته است اخلاط نفسانی بتن</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>گشته وهم و شهوت و خلط و غضب</p></div>
<div class="m2"><p>محترق از آن هوای مکتسب</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>نفس را عارض شده سوءالمزاج</p></div>
<div class="m2"><p>الله الله کوششی کن در علاج</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نفس بیمار است پرهیزش بده</p></div>
<div class="m2"><p>رحم کن حلوا به نزد او منه</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>نفس تو مهموم و محرور است و زار</p></div>
<div class="m2"><p>بهر حق خرما ز بهر او میار</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بر سر طبع و هوایش خاک کن</p></div>
<div class="m2"><p>نفس را ز اخلاط فاسد پاک کن</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>زاید از این خلطها بیحد و مر</p></div>
<div class="m2"><p>کرمها در اندرونت ای پسر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>کرم نی بل اژدهای هفت سر</p></div>
<div class="m2"><p>هر سری را صد سر دیگر ببر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>هان و هان از خلط خود را پاک کن</p></div>
<div class="m2"><p>اژدها و کرم را در خاک کن</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>نفس نبود اینکه داری ای فتی</p></div>
<div class="m2"><p>غار در غاری بود پر اژدها</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>نفس نبود بلکه غار اژدهاست</p></div>
<div class="m2"><p>اندر آن از اژدها انبارهاست</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>هر که زین اخلاط خود را پاک کرد</p></div>
<div class="m2"><p>سینه و طبع و هوا را چاک کرد</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ای خوش آن جانی کزین اهوا برست</p></div>
<div class="m2"><p>رستن از اینها جهاد اکبر است</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>نفس را کردی چه فانی زین مواد</p></div>
<div class="m2"><p>فارغش کردی ز امراض و فساد</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ده غذا او را ز اخلاق نکو</p></div>
<div class="m2"><p>تا که گردی شیر مرد و زفت او</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>تا توانی شو به وی یکتا و فرد</p></div>
<div class="m2"><p>زورمند و پهلوان و شیرمرد</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>وانگهی بر رخش شرعش کن سوار</p></div>
<div class="m2"><p>هم عنانش را به بینایی سپار</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>پس سوی مقصد برانگیزان تو رخش</p></div>
<div class="m2"><p>رخش سرعت می رساند تا به عرش</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>بر براق شرع اگر بندی رکاب</p></div>
<div class="m2"><p>این براقت بگذراند از حجاب</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>از شریعت کن طریقت ای رفیق</p></div>
<div class="m2"><p>تا حقیقت می رسی از این طریق</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>از شریعت رو حقیقت را ببین</p></div>
<div class="m2"><p>ربک فاعبد فیأتیک الیقین</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چون از این ره رفتی ای زیبا جوان</p></div>
<div class="m2"><p>می شناسی آنچه دانستن تو آن</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>می شناسی خویش را بیتاب و پیچ</p></div>
<div class="m2"><p>هیچ بن هیچ ابن بن هیچ بن هیچ</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ای که دانستی که هستی بی نشان</p></div>
<div class="m2"><p>نی کسی کو هم نهان شد هم عیان</p></div></div>