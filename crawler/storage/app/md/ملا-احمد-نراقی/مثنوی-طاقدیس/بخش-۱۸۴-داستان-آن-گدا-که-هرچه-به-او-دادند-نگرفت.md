---
title: >-
    بخش ۱۸۴ - داستان آن گدا که هرچه به او دادند نگرفت
---
# بخش ۱۸۴ - داستان آن گدا که هرچه به او دادند نگرفت

<div class="b" id="bn1"><div class="m1"><p>یک بزرگی می گذشت اندر رهی</p></div>
<div class="m2"><p>دید ناگه بر لب بامی مهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه نه بل خورشید چارم آسمان</p></div>
<div class="m2"><p>روی او بر آسمان پرتوفشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از کمان ابروان مشکبار</p></div>
<div class="m2"><p>صید افکن از یمین و از یسار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وز کمند گیسوان تابدار</p></div>
<div class="m2"><p>بسته پای رهروان از هر کنار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن نگار صید جو از طرف بام</p></div>
<div class="m2"><p>با نگاهی ساخت کار آن همام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ز عارف برد بالا با کمند</p></div>
<div class="m2"><p>با کمند آن دو گیسوی بلند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل ربود از سینه و هوشش ز سر</p></div>
<div class="m2"><p>بیدل و بیهوش ماند آن ره سپر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوی منزل رفت و دندان بر جگر</p></div>
<div class="m2"><p>نی خبر از پای او را نی ز سر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک دو روزی خون دل خورد و خزید</p></div>
<div class="m2"><p>عاقبت عشقش عنان از کف کشید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آتش عشقش شرر انگیز شد</p></div>
<div class="m2"><p>جام صهبای غمش لبریز شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خانه بر او تنگ شد چون چشم میم</p></div>
<div class="m2"><p>دشت شد چون دوزخ و گلشن جحیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس گذر افکند اندر پای بام</p></div>
<div class="m2"><p>نی اثر زان دید و نی بشنید نام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس یکی زنبیل چون عباس دوس</p></div>
<div class="m2"><p>برگرفت و جست چون تیری ز قوس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رفت سوی خانه ی آن دلربا</p></div>
<div class="m2"><p>گفت یاران چیزی از بهر خدا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شیئی لله شیئی لله ای مهان</p></div>
<div class="m2"><p>من گدای عاجزم بس مستهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر در آن خانه بس فریاد کرد</p></div>
<div class="m2"><p>تا که صاحبخانه او را یاد کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سعی و همت هست مفتاح فرج</p></div>
<div class="m2"><p>من قرع باباً وقد لج ولج</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرکه در زد خانه ای را عاقبت</p></div>
<div class="m2"><p>درگشایندش به مهر و مرحمت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نیست از دون همتی چیزی بتر</p></div>
<div class="m2"><p>کو ز بی همت کسی محرومتر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سعی و همت در کسی چون جمع شد</p></div>
<div class="m2"><p>بزم عالم را چراغ و شمع شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من چنین دانم که آن مسکین گدا</p></div>
<div class="m2"><p>می شود از سعی و همت پادشا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>می توان از سعی بر افلاک شد</p></div>
<div class="m2"><p>ای خوش آنکو ساعی و چالاک شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>راه آن باید ولی جست از نخست</p></div>
<div class="m2"><p>وانگهی پیمود ره چالاک و چست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از کسالت مرد ابتر می شود</p></div>
<div class="m2"><p>لایق روبند و معجر می شود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زاید از دون همتی ای یار فرد</p></div>
<div class="m2"><p>ذلت و عجز و زبونی بهر مرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وز کسالت نکبت و ادبار زاد</p></div>
<div class="m2"><p>تا چه زاید خود از این دو ای عماد</p></div></div>