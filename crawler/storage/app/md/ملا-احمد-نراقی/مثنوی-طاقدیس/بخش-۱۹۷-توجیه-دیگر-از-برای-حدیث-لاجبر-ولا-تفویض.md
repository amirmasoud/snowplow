---
title: >-
    بخش ۱۹۷ - توجیه دیگر از برای حدیث لاجبر ولا تفویض
---
# بخش ۱۹۷ - توجیه دیگر از برای حدیث لاجبر ولا تفویض

<div class="b" id="bn1"><div class="m1"><p>فعل را خواهی کنی و خواه نه</p></div>
<div class="m2"><p>جز تورا در فعل و ترکش راه نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس نه بتواند بپیچد پنجه ات</p></div>
<div class="m2"><p>یا ز منع فعل سازد رنجه ات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با تو باشد اختیار و اختیار</p></div>
<div class="m2"><p>اختیارت را بود بیخ استوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ورنه آن قدرت نباشد جان من</p></div>
<div class="m2"><p>با چنین قدرت دم از قدرت مزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اختیارت چون به دست دیگری ست</p></div>
<div class="m2"><p>اختیارت اختیار ای دوست نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باشد او از تو بگیرد اختیار</p></div>
<div class="m2"><p>هم نماید از تو سلب اقتدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاجز و زار و زبون گرداندت</p></div>
<div class="m2"><p>از فرازی سرنگون گرداندت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا فرستد مانعی در کار تو</p></div>
<div class="m2"><p>سست سازد همت ستوار تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا بگرداند از آن میل دلت</p></div>
<div class="m2"><p>یا بیارد پیش شغل شاغلت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این نباشد اختیار و جبر نیز</p></div>
<div class="m2"><p>هست امر بین امرین ای عزیز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر کنی فعل از تو باشد ای عمو</p></div>
<div class="m2"><p>با شعور و با اراده مو به مو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست تو بگرفته شاه دستگیر</p></div>
<div class="m2"><p>آن نباشد جبر ای مرد خبیر</p></div></div>