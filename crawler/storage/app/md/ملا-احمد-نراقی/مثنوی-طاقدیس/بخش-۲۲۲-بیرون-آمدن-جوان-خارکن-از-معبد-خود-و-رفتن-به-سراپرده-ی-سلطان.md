---
title: >-
    بخش ۲۲۲ - بیرون آمدن جوان خارکن از معبد خود و رفتن به سراپرده ی سلطان
---
# بخش ۲۲۲ - بیرون آمدن جوان خارکن از معبد خود و رفتن به سراپرده ی سلطان

<div class="b" id="bn1"><div class="m1"><p>در بر آن خار کن با صد نیاز</p></div>
<div class="m2"><p>گاه کردندش سلام و گه نیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قامتش کردند از پا تا بسر</p></div>
<div class="m2"><p>غرق مروارید و یاقوت و گهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرقه ی پشمینه اش انداختند</p></div>
<div class="m2"><p>سندس و دیبا طرازش ساختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس کشیدندش جنیبت زیر ران</p></div>
<div class="m2"><p>کوه پیکر زین مرصع زرعنان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خار کن چون بر جنیبت شد سوار</p></div>
<div class="m2"><p>در رکابش شد دوان صد شهریار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با شکوه کوه فر کیقباد</p></div>
<div class="m2"><p>شهر را از مقدم خود رتبه داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهریارانش قطار اندر قطار</p></div>
<div class="m2"><p>دستیار و پیشکار و تنقطار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهریاران در برزن و بازار و بام</p></div>
<div class="m2"><p>آن یکی آورد لام و این نیام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون قدم در بارگاه شه نهاد</p></div>
<div class="m2"><p>آمدش از روزگار خویش یاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روزگار ذلت و پستی خویش</p></div>
<div class="m2"><p>بینوایی و تهی دستی خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روزهای گرم و آن هیزم کشی</p></div>
<div class="m2"><p>شامهای سرد و آن بی آتشی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نکبت و ادبار بیش از بیش خود</p></div>
<div class="m2"><p>خاطر زار و دل پر ریش خود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن پریشانی و آن بیچارگی</p></div>
<div class="m2"><p>از در هر خانه و آوارگی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از دل پر درد و دست کوتهش</p></div>
<div class="m2"><p>رنج بی اندازه ی سال و مهش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ناله ی شبها و درد روزها</p></div>
<div class="m2"><p>ساختن در روز و شب با سوزها</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وانچه می خواهد ز بهر خود کنون</p></div>
<div class="m2"><p>آنچه از وصف و بیان باشد فزون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پادشاهی از پس هیزم کشی</p></div>
<div class="m2"><p>از پس آن ناخوشیها این خوشی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شمع و کافور و چراغ زرنگار</p></div>
<div class="m2"><p>از پس تاریکی و شبهای تار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>محفلی و گلستان در گلستان</p></div>
<div class="m2"><p>وصل جانانی چه جانان جان جان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قصر شاهنشاهی و وصل حبیب</p></div>
<div class="m2"><p>خلوت خالی ز اغیار و رقیب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زین تفکر روزنی بر دل گشاد</p></div>
<div class="m2"><p>نوری از آن روزنش بر دل فتاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فکرت آمد قفل دلها را کلید</p></div>
<div class="m2"><p>در گشاید چون کلید آمد پدید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فکرت آمد همچو باران بهار</p></div>
<div class="m2"><p>ساحت دلها بود چون کشت زار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فکر باش نطفه و دل زاقدان</p></div>
<div class="m2"><p>دل چراغی هست فکرت زیب آن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فکر باشد شعله و دل چون زکال</p></div>
<div class="m2"><p>آن زکال از شعله یابد اشتعال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فکر باشد چون نسیم صبحگاه</p></div>
<div class="m2"><p>غنچه نشگفته دل بی اشتباه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زین سبب گفت آن رسول سرفراز</p></div>
<div class="m2"><p>فکر یک ساعت به از سالی نماز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بلکه باشد بهتر از هفتاد سال</p></div>
<div class="m2"><p>این سخن مهمل ندانی این همال</p></div></div>