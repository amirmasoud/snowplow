---
title: >-
    بخش ۲۳۸ - روز عاشورا و حالات اصحاب آن حضرت
---
# بخش ۲۳۸ - روز عاشورا و حالات اصحاب آن حضرت

<div class="b" id="bn1"><div class="m1"><p>سر سراسیمه برآورد آفتاب</p></div>
<div class="m2"><p>چهره ی افروخته با اضطراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جست بیرون از نهان دیوانه وار</p></div>
<div class="m2"><p>خویش را می زد همی بر کوهسار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون فشان از هر کناره زرد روی</p></div>
<div class="m2"><p>سر برهنه بی حجاب آشفته موی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشتری عمامه از تارک فکند</p></div>
<div class="m2"><p>زهره بر سر گیسوان خویش کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از غضب بهرام تیغ خود شکست</p></div>
<div class="m2"><p>خامه تیر افکند از حیرت ز دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخ نهفت اندر حجاب خاک ماه</p></div>
<div class="m2"><p>هم ز ایوان رفت کیوان سوی چاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشت پیدا تیغ خور از خاوران</p></div>
<div class="m2"><p>از لب آن خون به بحر و بر روان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکه تاز چرخ این نیلی حصار</p></div>
<div class="m2"><p>تاخت بیخود و کله بر کوهسار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از شعاعش نیزه ی خطی بکف</p></div>
<div class="m2"><p>یا مبارزگو دوید از هر طرف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آمد از گلدسته ی عرش این ندا</p></div>
<div class="m2"><p>این ندایی بل صدای آشنا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ارکبوا یا قوم یا جندالاله</p></div>
<div class="m2"><p>ای سپاه پادشاه کم سپاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ارکبوا یا قوم یا حزب الوفا</p></div>
<div class="m2"><p>انهضوا جولوا بمیدان الولا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رخش همت را به زیر ران کشید</p></div>
<div class="m2"><p>رخت عزت سوی علیین کشید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مؤمن و کافر بهم آمیختند</p></div>
<div class="m2"><p>کفر و دین با یکدیگر آویختند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نور و ظلمت گشت با هم ممتزج</p></div>
<div class="m2"><p>با ملک اهریمن آمد مزدوج</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای زمین کربلا غماز شو</p></div>
<div class="m2"><p>کفر و دین از یکدگر ممتاز شو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هان و هان ای دین برون آ از غلاف</p></div>
<div class="m2"><p>هین بده با کفر در میدان مصاف</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حمله آرید ای گروه نوریان</p></div>
<div class="m2"><p>ناریان را فاش سازید و عیان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای فرشته صورتان و سیرتان</p></div>
<div class="m2"><p>پرده بردارید از این اهریمنان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>العجل یا جندنا نحو القتال</p></div>
<div class="m2"><p>هین درآویزید شیران با شغال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پنجه ای ای شیر مردان وا کنید</p></div>
<div class="m2"><p>روبها را روبهی پیدا کنید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لاف شیری می زنند این روبهان</p></div>
<div class="m2"><p>روبهی شان را کنید اکنون عیان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون برآمد خور ز بحر قیر فام</p></div>
<div class="m2"><p>سر برآوردند شیران از کنام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تیغها بر کف بروها پر ز چین</p></div>
<div class="m2"><p>در ره جانانه جان در آستین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>طایران گلستان لامکان</p></div>
<div class="m2"><p>بالها بگشوده سوی آشیان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گوشها بر حکم و چشمان بر زمین</p></div>
<div class="m2"><p>تا چه فرماید شه دنیا و دین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ناگهان آمد برون از خیمه شاه</p></div>
<div class="m2"><p>طعنه زن نورش به نور مهر و ماه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت یاران وقت جانبازی رسید</p></div>
<div class="m2"><p>وقت اقبال و سرافرازی رسید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تیرها می بارد از شست قضا</p></div>
<div class="m2"><p>سینه ها باید هدفشان سینه ها</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تیغها می آید از دست قدر</p></div>
<div class="m2"><p>ای خوشا آنکس که پیش آورد سر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عید قربان است میدان رضا</p></div>
<div class="m2"><p>هرکه را باشد سر ما الصلا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون شنیدند این وفاداران ز شاه</p></div>
<div class="m2"><p>بر مه افکندند از شادی کلاه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جمله افتادند چون دریا به جوش</p></div>
<div class="m2"><p>جمله چون رعد آمدند اندر خروش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جمله اسپرها به دوش انداختند</p></div>
<div class="m2"><p>هم سر و هم سینه اسپر ساختند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جمله بوسیدند پایی را که ماه</p></div>
<div class="m2"><p>بوسه دادی بر کفش هر شامگاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کان فدایت هم سر و هم جان ما</p></div>
<div class="m2"><p>هم زن و فرزند و خانمان ما</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هرچه فرمایی همه گوشیم گوش</p></div>
<div class="m2"><p>هم نهاده در ره هر گوش هوش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از تو یک فرمان ز ما جان باختن</p></div>
<div class="m2"><p>یک اشاره از تو از ما تاختن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آن امیران در سخن با شاه کل</p></div>
<div class="m2"><p>کامد از میدان کین بانگ دهل</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>صیحه ی هل من مبارز شد بلند</p></div>
<div class="m2"><p>شور در شیران ملک جان فکند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شور هل من رخصة نحو القتال</p></div>
<div class="m2"><p>فی قتال اهل کفر والضلال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رخصتی ای شاه شهرستان دین</p></div>
<div class="m2"><p>تا برون آریم دست از آستین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>روبهان را پوست از سر برکشیم</p></div>
<div class="m2"><p>جمله را در خاک و خاکستر کشیم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جسمشان در چاه بر زین افکنیم</p></div>
<div class="m2"><p>روحشان در قعر سجین افکنیم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پاک سازیم این جهان از لوثشان</p></div>
<div class="m2"><p>نی از ایشان نام ماند نی نشان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خاک را از خونشان رنگین کنیم</p></div>
<div class="m2"><p>این جهان را رشک فروردین کنیم</p></div></div>