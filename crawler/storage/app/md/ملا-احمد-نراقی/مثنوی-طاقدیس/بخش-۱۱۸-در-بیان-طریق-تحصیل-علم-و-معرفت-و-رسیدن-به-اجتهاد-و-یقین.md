---
title: >-
    بخش ۱۱۸ - در بیان طریق تحصیل علم و معرفت و رسیدن به اجتهاد و یقین
---
# بخش ۱۱۸ - در بیان طریق تحصیل علم و معرفت و رسیدن به اجتهاد و یقین

<div class="b" id="bn1"><div class="m1"><p>هر رهی رفتن ندارد پای مزد</p></div>
<div class="m2"><p>ای بسی ره کاندران غول است و دزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رهبری باید طلب کردن نخست</p></div>
<div class="m2"><p>رهنمایی چابک و چالاک و چست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاروانی خواستن دور از نفاق</p></div>
<div class="m2"><p>کاروان سالارش از اهل وفاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ره سپردن بر درای کاروان</p></div>
<div class="m2"><p>پا نهادن جای یای کاروان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه افتد گر خطایی در سفر</p></div>
<div class="m2"><p>نزد اهل فقر باشد مغتفر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور کشی بی سعی و جد و اجتهاد</p></div>
<div class="m2"><p>نی سراغ از کاروان و اوستاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رو نهد در راه و افتد در خطا</p></div>
<div class="m2"><p>آن خطایش مغتفر باشد کجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم اگر بینی کسی را در رهی</p></div>
<div class="m2"><p>نبودت از مقصد او آگهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لیک آن ره را نباشد هیچ سو</p></div>
<div class="m2"><p>مقصد امید و منزل آرزو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هیچ منزل دانی اندر راه نیست</p></div>
<div class="m2"><p>ور بود غیر از هلاک جاه نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر تو باشد جان من ارشاد او</p></div>
<div class="m2"><p>هین فراموشت مبادا یاد او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس غرض از آنچه گفتم ای رفیق</p></div>
<div class="m2"><p>ز اختلاف مقصد و فرق طریق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در ثواب آنکه افتد در خطا</p></div>
<div class="m2"><p>وز نکردن طعن کس در ابتدا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این بود این داده بر گفتار گوش</p></div>
<div class="m2"><p>کز بقا آغاز بر هرکس مجوش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر رهی کاید به منزل عاقبت</p></div>
<div class="m2"><p>سوی مقصد باشد آن را خاتمت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کژی و دوری و دشواری آن</p></div>
<div class="m2"><p>نزد آخر بین بود سهل ای فلان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هم خطایی هرکه در ره اوفتاد</p></div>
<div class="m2"><p>بود سعی و بذل و جهد و اجتهاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نزد ارباب خرد نبود ملوم</p></div>
<div class="m2"><p>می نخوانند اهل عدل آن را ظلوم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هم رهی راکش ندانی تا کجاست</p></div>
<div class="m2"><p>رهروان را کردن ظلمی خطاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مقصد قوم خدا جوی ای نگار</p></div>
<div class="m2"><p>گرچه باشد متحد ره بی شمار</p></div></div>