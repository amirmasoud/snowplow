---
title: >-
    بخش ۱۵۶ - داستان خیک که بر روی آب دریا افتاده بود
---
# بخش ۱۵۶ - داستان خیک که بر روی آب دریا افتاده بود

<div class="b" id="bn1"><div class="m1"><p>دید خیکی پر فتاده روی آب</p></div>
<div class="m2"><p>می برد آبش بهرسو با شتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می رود گاهی به بالا گه به زیر</p></div>
<div class="m2"><p>گفت با همره که رختم را بگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا برآرم من از این آب و جل</p></div>
<div class="m2"><p>این یکی خیک پر از شهد و عسل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد برهنه پس به دریا باز شد</p></div>
<div class="m2"><p>غوطه ور در لجه ی ذخار شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد شناور سوی آن خیک عسل</p></div>
<div class="m2"><p>خویش را افکند بروی از عجل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از قضا بود آن یکی خرس دغا</p></div>
<div class="m2"><p>اندر آن غرغاب گشته مبتلا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست و پا گم کرده اندر آن حلیش</p></div>
<div class="m2"><p>هر طرف جویای یک برگ حشیش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا مگر خود را بچفساند در آن</p></div>
<div class="m2"><p>الغریق یتشبث را بخوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چونکه خود افکند آن طامع بر او</p></div>
<div class="m2"><p>او بر آن چفسیده چون محکم زلو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دستها در گردن و پا در کمر</p></div>
<div class="m2"><p>هین بیا و رقص خرس و خر نگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گه فرو رفتند تا قعر زمین</p></div>
<div class="m2"><p>گه شدند اندر یسار و گه یمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گه به زیر و گه به بالا آمدی</p></div>
<div class="m2"><p>گه برآوردی سر و پف پف زدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرد فریاد آن رفیقش کی ودود</p></div>
<div class="m2"><p>دست از این خیک عسل بردار زود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هین بیفکن خیک و از دریا برا</p></div>
<div class="m2"><p>بگذر از این سود پررنج و بلا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت بگذشتم من از خیک ای رفیق</p></div>
<div class="m2"><p>خیک از من نگذرد در این مضیق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خیک را نادیده من انگاشتم</p></div>
<div class="m2"><p>دست از خیک عسل برداشتم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برنمی دارد ز من دست این عنود</p></div>
<div class="m2"><p>التماس من بکن با خیک زود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خیک دانی چیست ای یار گزین</p></div>
<div class="m2"><p>شهرت بیهوده پیش آن و این</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چیست دانی خیک جاه و منصبت</p></div>
<div class="m2"><p>که از آن گشته سیه روز و شبت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چیست خیک آن محفل تدریس تو</p></div>
<div class="m2"><p>منبر و محراب پر تلبیس تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خیک دانی چیست فرزند و زنت</p></div>
<div class="m2"><p>چون کمند افتاده اندر گردنت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خیک چبود این زنان کهنه سال</p></div>
<div class="m2"><p>مانده اندر گردن ما چون وبال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>راستی خیکند و خیک زهرمار</p></div>
<div class="m2"><p>الفرار از این گروه دیوسار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روی هاشان خیک و اشکمها چو خیک</p></div>
<div class="m2"><p>وان زبانها خنجر و دلها چو دیگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خیک چبود این حریفان دغا</p></div>
<div class="m2"><p>دوستان فاش و اعدای خفا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای خدا زین خیکهامان کن خلاص</p></div>
<div class="m2"><p>نیست ما را جز تو از اینها مناص</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وهم ما را در مضیق انداخته</p></div>
<div class="m2"><p>خرس از خیک عسل نشناخته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای خوش آنکو وهم از جانش گریخت</p></div>
<div class="m2"><p>رشته ی پندار را از هم گسیخت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وصف ذات فعل خود نابود دید</p></div>
<div class="m2"><p>هرچه دید از آن جهان جود دید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خویش را فانی و هیچ و نیست دید</p></div>
<div class="m2"><p>آنچه دید از آنکه آن باقیست دید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>این فنای مخلصین است ای پسر</p></div>
<div class="m2"><p>خرم آنکو شد خلوصش راهبر</p></div></div>