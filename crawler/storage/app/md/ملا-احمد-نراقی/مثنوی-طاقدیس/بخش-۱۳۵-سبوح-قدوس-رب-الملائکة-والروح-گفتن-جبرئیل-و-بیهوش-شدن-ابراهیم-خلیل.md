---
title: >-
    بخش ۱۳۵ - «سبوح قدوس رب الملائکة والروح» گفتن جبرئیل و بیهوش شدن ابراهیم خلیل
---
# بخش ۱۳۵ - «سبوح قدوس رب الملائکة والروح» گفتن جبرئیل و بیهوش شدن ابراهیم خلیل

<div class="b" id="bn1"><div class="m1"><p>جمله را از بهر حق قربان نمود</p></div>
<div class="m2"><p>جمله را قربان آن سلطان نمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داده بود آن را خدای ذوالمنن</p></div>
<div class="m2"><p>مال و فرزندی چو اسماعیل تن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آستین بر جمله افشاند از وفا</p></div>
<div class="m2"><p>در ره آن پادشاه ذوالبها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یافت حق او را چو از خاصان خاص</p></div>
<div class="m2"><p>نام خلت یافت از وی اختصاص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برگزید او را خداوند جلیل</p></div>
<div class="m2"><p>خواند او را از برای خود خلیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلعت خلت رسید او را ز رب</p></div>
<div class="m2"><p>آمد او را هم خلیل الله لقب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تارکش را افسر خلت رسید</p></div>
<div class="m2"><p>مهر خلت شد به منشورش پدید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواست از جا غیرت کروبیان</p></div>
<div class="m2"><p>بحر غیرت گشت بزم قدسیان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غلغل سبحان ذی المجدالعلی</p></div>
<div class="m2"><p>والجمال الفرد والعزو الثناء</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شور و غوغا در میانشان درفکند</p></div>
<div class="m2"><p>جملگی گفتند با بانگ بلند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کی خدا ای برتر از وهم و خیال</p></div>
<div class="m2"><p>ای منزه از چه و چون و مثال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کی روا باشد که خاکی بس ذلیل</p></div>
<div class="m2"><p>در حریم کبریا گردد خلیل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از عناصر زاده ی خاکی نسب</p></div>
<div class="m2"><p>کی روا باشد که یابد این لقب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>او کجا و رتبه ی خلت کجا</p></div>
<div class="m2"><p>نطفه ای را اینچنین زینت کجا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرد بقال از چه بس استاد بود</p></div>
<div class="m2"><p>کی به پهلوی شهانش جای بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>موزه گر از جلد آهوی تتار</p></div>
<div class="m2"><p>سازی آن را از برای سرمیار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این نکوهش نی بد اول یارشان</p></div>
<div class="m2"><p>بود ز آغاز وجود این کارشان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یفسدو یسفک بخواندندش نخست</p></div>
<div class="m2"><p>پس گنه کار و جهول عهد سست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این نبودی از حسد یا بغض و کین</p></div>
<div class="m2"><p>حاش لله کی ملک هست اینچنین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بلکه از نادانی آن خلق پاک</p></div>
<div class="m2"><p>بود از اطوار این فرزند خاک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چونکه اکثر ذات او نشناختند</p></div>
<div class="m2"><p>جانب طعن و ملامت تاختند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از خلافت گاه در بحث و جدال</p></div>
<div class="m2"><p>گه ز خلت در میانشان قیل و قال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آدمیزادا ببین تو چیستی</p></div>
<div class="m2"><p>کان ملایک می نداند کیستی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قاصر از ادراک تو روحانیان</p></div>
<div class="m2"><p>خدمتت را قدسیان بسته میان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو خلیفه ی حقی و نایب مناب</p></div>
<div class="m2"><p>جانشین پادشاه مستطاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نسخه ی آیات ربانی استی</p></div>
<div class="m2"><p>مظهر اوصاف رحمانی استی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ضرب دارالملک و اقلیم جلال</p></div>
<div class="m2"><p>نقش دست نقشبند بی مثال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>قدر خود بشناس اوج خود بدان</p></div>
<div class="m2"><p>خویش را مفروش ارزان و مهان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اندرین بازار پرسودا و شر</p></div>
<div class="m2"><p>مشتری بسیار داری ای پسر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مشتری افزون ز تعداد و شمار</p></div>
<div class="m2"><p>جمله واکرده دکان از هر کنار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چونکه خود را می فروشی ای عمو</p></div>
<div class="m2"><p>مشتری قدر دانی را بجو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قدر دان و صاحب گنج و گهر</p></div>
<div class="m2"><p>تا تورا بر سر نهد اکلیل زر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بامدادان اسبهای خوش نژاد</p></div>
<div class="m2"><p>برد میدان بهر بیع آن اوستاد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بانگ برزد کاسبها را ارخری</p></div>
<div class="m2"><p>می فروشم فاش این المشتری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سوی استا شد روان از هر طرف</p></div>
<div class="m2"><p>مشتریها کیسه ی زرشان بکف</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مهتر سلطان یکی را می خرد</p></div>
<div class="m2"><p>جانب اصطبل سلطان می برد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>می برد آن را خرامان و چمان</p></div>
<div class="m2"><p>تا به نزد پادشاه قدر دان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>می دهندش جای جو قند و شکر</p></div>
<div class="m2"><p>هم مویز خشک و هم حلوای تر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زین ز زر سازند و از سیمش لگام</p></div>
<div class="m2"><p>جان او روبند در هر صبح و شام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>صبحها کان شه برآن گردد سوار</p></div>
<div class="m2"><p>در رکابش میرو سلطان صد هزار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سروران بوسند سمش از شعف</p></div>
<div class="m2"><p>مهتوران گردند دورش هر طرف</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آن یکی تیمار آن را انتظار</p></div>
<div class="m2"><p>وان همی جوید ز سمش سنگ و خار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>می خرد آن اسب دیگر یک فقیر</p></div>
<div class="m2"><p>کهکشانش گاه و شعرایش سفیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>می نبیند کاه و جو الا به خواب</p></div>
<div class="m2"><p>دایم از جوع البقر در اضطراب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نی صطبل او را نه آخور نی حصار</p></div>
<div class="m2"><p>خوابگاه او نه جز خارا و خار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از یسار و از یمین جویای کاه</p></div>
<div class="m2"><p>خاک بوید بهر جو تا صبحگاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گه به هیزم کش دهد او را کرا</p></div>
<div class="m2"><p>گه سپارد سوی حمالی ورا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زان بتر اسبی دگر کانرا خرد</p></div>
<div class="m2"><p>مرد عصار و سوی دکانش برد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نی اثر بیند ز آب و نی زکاه</p></div>
<div class="m2"><p>می خورد سرگین و آنهم گاه گاه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گردنش خم گشته زیر بار غنگ</p></div>
<div class="m2"><p>شرحه شرحه گردنش از عاد سنگ</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نیست تیمارش بجز از چوب تر</p></div>
<div class="m2"><p>مهترش نی غیر گرز گاو سر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بسته چشم و دست و پا اندر حصار</p></div>
<div class="m2"><p>گرز بر فرقش که هین روغن بیار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>راه پیماید بسختی روز و شب</p></div>
<div class="m2"><p>نی امید رحم و نی پای هرب</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>یکدمش آسایش و آرام نی</p></div>
<div class="m2"><p>راه او را آخر و انجام نی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ره رود اما نبرد منزلی</p></div>
<div class="m2"><p>زآمد و رفتن نه او را حاصلی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>می رود اما نه صحرایی نه دشت</p></div>
<div class="m2"><p>نی هوای تازه و نه سیرگشت</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نی رهش را مبدئی و نی ختام</p></div>
<div class="m2"><p>نی در آن ره منزلی و نی کنام</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نی مجال خفتن او را نی شنو</p></div>
<div class="m2"><p>تا تواند رفت گویندش برو</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گر بگویم رفتم این ره سالها</p></div>
<div class="m2"><p>ریختم هم یال و هم کوپالها</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گویدش استا که جان اندر تنت</p></div>
<div class="m2"><p>باشد این ره را بباید رفتنت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گر بگوید تابکی نبود جواب</p></div>
<div class="m2"><p>جز که رو روای هیون با صد شتاب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ای تو در بازار این دنیای دون</p></div>
<div class="m2"><p>مشتریها باشدت از حد فزون</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چون هوا و نفس کافر کیش تو</p></div>
<div class="m2"><p>دوستان سود خود اندیش تو</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>آن زن و فرزند و عم و خال تو</p></div>
<div class="m2"><p>وان عیال خفته اندر فال تو</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دشمن دیرینت آن دیو پلید</p></div>
<div class="m2"><p>کو به تو چفسیده محکم چون کبید</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>جملگی آنها خریدار تو اند</p></div>
<div class="m2"><p>در پی صید تو اشکار تو اند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>می خرندت از برای بندگی</p></div>
<div class="m2"><p>نی پی مولایی و فرخندگی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>می خرندت زیر صد بارت کشند</p></div>
<div class="m2"><p>یا کنندت پوست بر دارت کشند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>آن یکی خواهد پی شبکاریت</p></div>
<div class="m2"><p>وان دگر حمالی این عصاریت</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>وان کشد تا از تو کین جد و مام</p></div>
<div class="m2"><p>کینه های آن نیاکان گرام</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>این خریداران گدایان طریق</p></div>
<div class="m2"><p>جملگی عباس دوسند ای رفیق</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>آن خریداران عور کفش دزد</p></div>
<div class="m2"><p>از تو خواهند آش و نان و کار مزد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>یک خریدار دگر باشد تورا</p></div>
<div class="m2"><p>پادشاه جمله شاهانش گدا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>پادشاهی کشورش ملک وجود</p></div>
<div class="m2"><p>کشورش را نی جهات و نی حدود</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بلکه بر ملک عدم هم حکمران</p></div>
<div class="m2"><p>بر وجود و بر عدم حکمش روان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>سکه در لاهوت و در ناسوت زد</p></div>
<div class="m2"><p>نوبت از لاهوت در ناسوت زد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>قاف تا قافش همه گنج گران</p></div>
<div class="m2"><p>سفره ی او از مکان تا لامکان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>مشتری او انبیا و اولیا</p></div>
<div class="m2"><p>ملک سرمد مایه دست قالها</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>عقد ایجابش اگر جویی همین</p></div>
<div class="m2"><p>انّ الله اشتری من مؤمنین</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>مانده از تو یک قبول ای نیکبخت</p></div>
<div class="m2"><p>سر بجنبان تکیه زن بالای تخت</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بنده ی او گرد پس آزاد زی</p></div>
<div class="m2"><p>رو غم او خور ز هر غم شادزی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بنده ی او همچو ابراهیم باش</p></div>
<div class="m2"><p>پیش تیغ نار او تسلیم باش</p></div></div>