---
title: >-
    بخش ۷۷ - خوردن لقمان میوه ی تلخ را از دست خواجه ی خود
---
# بخش ۷۷ - خوردن لقمان میوه ی تلخ را از دست خواجه ی خود

<div class="b" id="bn1"><div class="m1"><p>بود لقمان نیکبختی را رفیق</p></div>
<div class="m2"><p>خدمتش را ملتزم در هر طریق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن یکی در خدمت این پی سپر</p></div>
<div class="m2"><p>این مرآن را مهربانتر از پدر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواجه را ناطور روزی بهر خوان</p></div>
<div class="m2"><p>یک گوارا میوه آورد ارمغان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میوه ی چند از گواران ذوالمنن</p></div>
<div class="m2"><p>داد لقمان را بدست خویشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه انعام تو جان پرور بود</p></div>
<div class="m2"><p>گر ز دست خود دهی خوشتر بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلکه تیغ از دست تو بر سر مرا</p></div>
<div class="m2"><p>خوبتر از تاج و از افسر مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زخم از دستت ز مرهم خوبتر</p></div>
<div class="m2"><p>زهر از تریاق جان محبوبتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خورد لقمان میوه ها را با نشاط</p></div>
<div class="m2"><p>هر یکی خوردی فزودی انبساط</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از نشاطی کز جبین او نمود</p></div>
<div class="m2"><p>خواجه را در میوه صد رغبت فزود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رغبت هم کاسه رغبت زاستی</p></div>
<div class="m2"><p>اندرین پنهان در آن پیداستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون یکی زان میوه برد اندر دهن</p></div>
<div class="m2"><p>شد مزاق خواجه زان تلخ و وژن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیگری برداشت دیدش تلختر</p></div>
<div class="m2"><p>طعم حنظل پیش طعمش نیشکر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یک گوارا میوه ای در آن گوار</p></div>
<div class="m2"><p>می ندید آن خواجه بهر خواجه یار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت لقمان را که ای شمع وثاق</p></div>
<div class="m2"><p>چون فرو بردی تو این زهر از مذاق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای عجب ای زهر خوردی چون شکر</p></div>
<div class="m2"><p>نی بر ابرو چین و نی بر رخ اثر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون چشیدی با نشاط این صبر را</p></div>
<div class="m2"><p>آفرین این طاقت و این صبر را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت لقمان سالیان بس دراز</p></div>
<div class="m2"><p>من شکرها خوردم از دست نیاز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر یکی تلخی از آن دستان چشم</p></div>
<div class="m2"><p>کی روا باشد که رو درهم کشم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کام من شیرین از آن کف سالهاست</p></div>
<div class="m2"><p>لحظه ای هم تلخ اگر باشد رواست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خورده ام شیرینی از دستت مدام</p></div>
<div class="m2"><p>گو یکی تلخی خورم زان ای همام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بس حلاوت از تو در کامم بود</p></div>
<div class="m2"><p>زهر اگر آنجا رسد شیرین شود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این دهانم از تو شد کان شکر</p></div>
<div class="m2"><p>حنظلی بخشد کجا آنجا اثر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>الغرض آن مرد عارف در طلب</p></div>
<div class="m2"><p>بر دری برداشت دست آن نیم شب</p></div></div>