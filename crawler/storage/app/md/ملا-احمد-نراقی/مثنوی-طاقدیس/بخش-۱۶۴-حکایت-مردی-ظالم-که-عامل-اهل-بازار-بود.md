---
title: >-
    بخش ۱۶۴ - حکایت مردی ظالم که عامل اهل بازار بود
---
# بخش ۱۶۴ - حکایت مردی ظالم که عامل اهل بازار بود

<div class="b" id="bn1"><div class="m1"><p>اندرین ایام اندر شهر کاش</p></div>
<div class="m2"><p>که نگهدارد خداوند از بلاش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود مردی شغل دیوان کار او</p></div>
<div class="m2"><p>مردم بازار در شیکار او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منصب سالاری بازار داشت</p></div>
<div class="m2"><p>محفل بازاریان را بار داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجمع او راز اهل سوق بود</p></div>
<div class="m2"><p>نیک و بدشان را هم او فاروق بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود گلجان اهل آن بازار را</p></div>
<div class="m2"><p>فربهی پنداشت آن آمار را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواجه چون خواهد سرایی ساختن</p></div>
<div class="m2"><p>خانه ای زیبا و نغز افراختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موضعی تالار و ایوان می کند</p></div>
<div class="m2"><p>موضع دیگر گلستان می کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بود آن جای خواب و راحتش</p></div>
<div class="m2"><p>تا بود این جای عیش و عشرتش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکطرف طرح ستاوند افکنند</p></div>
<div class="m2"><p>یکطرف دهلیز و دربند افکنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عرصه ای را گلشن از رز می کنند</p></div>
<div class="m2"><p>گوشه ای گلجان مبرز می کنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>موضع گلجان برای میختن</p></div>
<div class="m2"><p>خاشه و آخان در آنجا ریختن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا پلیدیهای مردار نجس</p></div>
<div class="m2"><p>زان سراگردند آنجا منطمس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صفه و مشکوی آن مشکین کنند</p></div>
<div class="m2"><p>کاخ و ایوان را عبیرآگین کنند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طارمش پاکیزه و زیبا کنند</p></div>
<div class="m2"><p>همچو مینو ساختن مینا کنند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از برای خلوت خاصان خویش</p></div>
<div class="m2"><p>بهر مهمانان سلطانان خویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در میان اهل عالم ای عمو</p></div>
<div class="m2"><p>مبرزند این ظالمان دیوخو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیکبختانند در این بوم و بر</p></div>
<div class="m2"><p>لطف حق دارد به ایشان صد نظر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جوهر جانشان ز علیین پاک</p></div>
<div class="m2"><p>روح صاف آمیخته با درد خاک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خشم و شهوت دامنش را کف زده</p></div>
<div class="m2"><p>وهم و عقل اندر بر هم صف زده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چند شیطان رفته در پیراهنش</p></div>
<div class="m2"><p>اهل سجین گرد در پیرامنش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آب صافش زین سبب پر لای شد</p></div>
<div class="m2"><p>راست تا چپ آمد و از جای شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای بسی نابودنیها بوده شد</p></div>
<div class="m2"><p>در پلیدیها بسی آلوده شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از کثافات و نجاسات برون</p></div>
<div class="m2"><p>شد کثیف و شد پلیدش اندرون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حکمت حق زاهل سجین ظالمان</p></div>
<div class="m2"><p>بهر این مصرف برآورد از میان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کردشان مبرز برای بندگان</p></div>
<div class="m2"><p>گرد آید تا پلیدیها در آن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لاجرم از ضرب و زور و چوب بند</p></div>
<div class="m2"><p>آن پلیدیها سوی خود می کشند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا شوند این نیکبختان صاف و خش</p></div>
<div class="m2"><p>پاک گردند از فضول غل و غش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا به علیین اعلا بر پرند</p></div>
<div class="m2"><p>رخت خود تا چشمه کوثر کشند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>الغرض آن مبرز بازاریان</p></div>
<div class="m2"><p>داشت با بازاریان صد داستان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>روزی از سادات مسکین فقیر</p></div>
<div class="m2"><p>داشت جنسی کم بها و بس حقیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جنس خود بی اذن آن ظالم فروخت</p></div>
<div class="m2"><p>شعله ی خشم ستمگر برفروخت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اخگر آن شعله بر درویش زد</p></div>
<div class="m2"><p>داد هم دشنام و هم سیلیش زد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>رفت و گفتا می کنم با جان ریش</p></div>
<div class="m2"><p>شکوه ات را با نیای کار خویش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفت او را سوی من آرید باز</p></div>
<div class="m2"><p>شکوه اش را تا کنم دور و دراز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>باز آمد زد بر او مشت و لگد</p></div>
<div class="m2"><p>گفت رو رو شکوه کن با جد خود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نزد جدت رو به این حال و بگوش</p></div>
<div class="m2"><p>تا درآرد کتفهایم را ز دوش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>این بگفت و تا سرای خویش رفت</p></div>
<div class="m2"><p>از قفایش آه آن درویش رفت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هم در آن شب جسم او را تب گرفت</p></div>
<div class="m2"><p>او بنای لعن بر منصب گرفت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تب فزون می گشت او را دمبدم</p></div>
<div class="m2"><p>او به پای توبه چسبید و ندم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در عمل عمال مار ارقم اند</p></div>
<div class="m2"><p>در گرفتاری چو پور ادهم اند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سایه ی بیماری درد و بلا</p></div>
<div class="m2"><p>از سر عمال یا رب کم مبا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شانه هایش صبحدم بگرفت درد</p></div>
<div class="m2"><p>پس سیه شد زان سپس آماس کرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آن ستمگر در فغان و در کراخ</p></div>
<div class="m2"><p>برزمین از درد می نالید ناخ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>او همین بارید بر دامن سرشک</p></div>
<div class="m2"><p>ویژگانش در پی دید و پزشک</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>عاقبت تیغی در آتش تافتند</p></div>
<div class="m2"><p>کتفهایش را از آن بشکافتند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کتفهایش سر کشیدند از درون</p></div>
<div class="m2"><p>ویله ی آن رفت تا چرخ نگون</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از پس صد ویله و صد ویل و وای</p></div>
<div class="m2"><p>جان سپرد و رفت تا دیگر سرای</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کفشهایش را درآورد آن نیا</p></div>
<div class="m2"><p>جان فدای آن نیای خوش لقا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هان و هان ای بی ادب هشیار باش</p></div>
<div class="m2"><p>هوشیار از گفت ناهنجار باش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گردن شیر است بی پروا مخار</p></div>
<div class="m2"><p>کام طنین است دست آنجا میار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>پا منه اینجا که سر می افکنند</p></div>
<div class="m2"><p>دم مزن بیجا که گردن می زنند</p></div></div>