---
title: >-
    بخش ۸۶ - در بیان حدیث من عرف نفسه فقد عرف ربه
---
# بخش ۸۶ - در بیان حدیث من عرف نفسه فقد عرف ربه

<div class="b" id="bn1"><div class="m1"><p>گفت زین ره لؤلؤ این نه صدف</p></div>
<div class="m2"><p>من عرف نفسه فقد ربه عرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکسی کو عارف نفس خود است</p></div>
<div class="m2"><p>عارف پروردگار سرمد است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شناسد هست بیجا و مکان</p></div>
<div class="m2"><p>هم نشان یابد ز بود بی نشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم شناسد هستی دور از هواس</p></div>
<div class="m2"><p>آفتابی را ز شمع آرد قیاس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هان چه گویی این زمان آزرم کن</p></div>
<div class="m2"><p>آفتاب و شمع چبود شرم کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک بر فرق قیاسات تو باد</p></div>
<div class="m2"><p>زین قیاساتت خدا کیفر دهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش خورشید ازل صد آفتاب</p></div>
<div class="m2"><p>ذره ی خوردی نیاید در حساب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روح را کی شمع گفتی می توان</p></div>
<div class="m2"><p>پیش آن خورشید بیمانند دان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست کمتر از پر پروانه ای</p></div>
<div class="m2"><p>بلکه آنهم نیست جز افسانه ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش فهم دانش ما کودکان</p></div>
<div class="m2"><p>می دهد نفس از خداوندش نشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نزد فهم ما و تو شد این نشان</p></div>
<div class="m2"><p>معرفت حق را وگرنه ای فلان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما کجا و درک ذات پاک او</p></div>
<div class="m2"><p>نیست کس را رتبه ی ادراک او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنکه کور از مام زایید ای همام</p></div>
<div class="m2"><p>کی تواند یافتن نور از ظلام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این وجودی کو نگنجد در جهان</p></div>
<div class="m2"><p>کی درآید در نهاد این و آن</p></div></div>