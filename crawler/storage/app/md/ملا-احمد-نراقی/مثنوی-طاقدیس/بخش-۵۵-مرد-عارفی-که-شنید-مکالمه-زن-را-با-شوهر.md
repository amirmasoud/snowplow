---
title: >-
    بخش ۵۵ - مرد عارفی که شنید مکالمه زن را با شوهر
---
# بخش ۵۵ - مرد عارفی که شنید مکالمه زن را با شوهر

<div class="b" id="bn1"><div class="m1"><p>عارفی می رفت روزی در رهی</p></div>
<div class="m2"><p>با دل دانا و جان آگهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناگهش آمد به گوش از روزنی</p></div>
<div class="m2"><p>کاین سخن می گفت با شوئی زنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذرانم از تو گر نان ناریم</p></div>
<div class="m2"><p>تشنه و بی آب و نان بگذاریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم اگر شبها نیاری روشنی</p></div>
<div class="m2"><p>هم اگر ندهی مرا پوشیدنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمله اینها بگذرانم ای عزیز</p></div>
<div class="m2"><p>از تو اینها من نخواهم هیچ چیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیک اگر بر من گزینی دیگری</p></div>
<div class="m2"><p>یا به رخسار دگر زن بنگری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگذرانم از تو هرگز این گناه</p></div>
<div class="m2"><p>نیکی از من دیگر ای شوهر مخواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرد عارف این سخن را چون شنید</p></div>
<div class="m2"><p>نعره ای بی اختیار از جان کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زد گریبان چاک و بیهوش اوفتاد</p></div>
<div class="m2"><p>جوی اشک از دیدگان بر رو گشاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جمع شد بر گرد او برنا و پیر</p></div>
<div class="m2"><p>هین چه دیدی ای تو بینا و خبیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این بنای عقل و هوشت از کجاست</p></div>
<div class="m2"><p>آتشی پیدا نه جوشت از کجاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت آتش هم نهان هم ظاهر است</p></div>
<div class="m2"><p>چشمتان اما ز دیدن قاصر است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نغمه ی داود از هرسو بلند</p></div>
<div class="m2"><p>گوشتان لیکن کرند و انجمند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حس جسمانی همه قشر است و پوست</p></div>
<div class="m2"><p>جسمهای روح مغز و لب اوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا زبان روح تو گویا نشد</p></div>
<div class="m2"><p>چشم او بینا و شم بویا نشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ذوق و لمس و سمع او دانا نشد</p></div>
<div class="m2"><p>دست او گیرا و پا پویا نشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حس تو بیمغز باشد بی سخن</p></div>
<div class="m2"><p>لفظ بیمعنی و ضرع بی لبن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هم گلابی بوی شمع بیفروغ</p></div>
<div class="m2"><p>هم سبوی خالی از دوشاب دوغ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قشر باشد جز به قشرش راه نیست</p></div>
<div class="m2"><p>هیچ اوصافی ز مغز آگاه نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنچه هست اینجا چو جوهر چو عرض</p></div>
<div class="m2"><p>زشت با زیبا و صحت با مرض</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جملگی قشرند مغز صافشان</p></div>
<div class="m2"><p>در ورای این جهان باشد عیان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون حواست را نباشد مغز نغز</p></div>
<div class="m2"><p>کی رسد اینها بسوی لب و مغز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جز به قشر آن را نباشد رابطه</p></div>
<div class="m2"><p>می نفهمند غیر آن زین واسطه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زین سبب فرمود آن بیچند و چون</p></div>
<div class="m2"><p>می نداند غیره والراسخون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چشم جای بگشای ای جان عمو</p></div>
<div class="m2"><p>تا ز هر چیزی ببینی مغز او</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پس ز مغز او ز اصلش پی بری</p></div>
<div class="m2"><p>پس ز مغز مغز اصلش بنگری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مارایت شیئا الا ومعه</p></div>
<div class="m2"><p>قدرایت ربه و مبدعه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چشم جان بگشای و بنگر بی حجاب</p></div>
<div class="m2"><p>هر طرف در جلوه سیصد آفتاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دیده ی جان پرده ها را بر درد</p></div>
<div class="m2"><p>در نهاد قشرها لب بنگرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در درون هریکی بیند عیان</p></div>
<div class="m2"><p>گشته صد خورشید نورافکن نهان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در درون آن دگر دارد وطن</p></div>
<div class="m2"><p>اهرمن در اهرمن در اهرمن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پرده بینی این یکی را در بهشت</p></div>
<div class="m2"><p>دوزخ آن یک را نهفته در سرشت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر گشایی گوش جان تیزهوش</p></div>
<div class="m2"><p>می نیابی در جهان چیزی خموش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>راز من شیئی والا بشنوی</p></div>
<div class="m2"><p>کشته ی امید خود را بدروی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مغز هر حرفی بگوش آید تورا</p></div>
<div class="m2"><p>مرغ جان زان در خروش آید تورا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هم بر این منوال باقی حواس</p></div>
<div class="m2"><p>همچو نطق و شامه و ذوق و مساس</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پاک گردی ره به پاکانت دهند</p></div>
<div class="m2"><p>جان شوی آغوش جانانت دهند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>صافیان از تو نفور و دردمند</p></div>
<div class="m2"><p>هر گروهی جنس خود را طالبند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ناریان مر ناریان را جاذبند</p></div>
<div class="m2"><p>نوریان مر نوریان را طالبند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>حبس در حبس و مضیق اندر مضیق</p></div>
<div class="m2"><p>صحبت دباغ و عطار ای رفیق</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مرد عارف گفت گوش جان من</p></div>
<div class="m2"><p>مغز و لب نشنید از گفتار زن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آمد از غیبم به گوش جان خطاب</p></div>
<div class="m2"><p>کای تو مانده وز پس نهصد حجاب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر نکو نبود نماز و روزه ات</p></div>
<div class="m2"><p>ور نباشد طاعت هر روزه ات</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر گنه آری سوی من کوه کوه</p></div>
<div class="m2"><p>زانچه آید هر دو عالم زان ستوه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>من بیامرزم ز فیض عام خود</p></div>
<div class="m2"><p>خوانده ام غفار مطلق نام خود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مغفرت مر معصیت را طالبست</p></div>
<div class="m2"><p>اهل عصیان را سوی خود جاذبست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مغفرت آری بود عصیان طلب</p></div>
<div class="m2"><p>چونکه عصیان شد ظهورش را سبب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همچنانکه ذات خلاق جهان</p></div>
<div class="m2"><p>بود غیب مطلق و راز نهان</p></div></div>