---
title: >-
    بخش ۹۴ - در بیان نوحه گری و ماتم داری بدن از مفارقت روح و حسرت بعدالموت
---
# بخش ۹۴ - در بیان نوحه گری و ماتم داری بدن از مفارقت روح و حسرت بعدالموت

<div class="b" id="bn1"><div class="m1"><p>ای رفیقان چون سخن اینجا رسید</p></div>
<div class="m2"><p>داستان تا شرح حال ما کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زد مغنی بر نوای دیگرم</p></div>
<div class="m2"><p>ریخت ساقی خون دل بر ساغرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناید از آن پرده آهنگ نشاط</p></div>
<div class="m2"><p>ناید از این باده بوی انبساط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اصل آن قانون نوای ماتم است</p></div>
<div class="m2"><p>صاف این صهبا همه درد و غم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان نوا نوحه به گوش آید همی</p></div>
<div class="m2"><p>زان شرابم دل بجوش آید همی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن نوا از حال جانم یاد داد</p></div>
<div class="m2"><p>گلبن عیش مرا برباد داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شعله ی آهم ره آذر گرفت</p></div>
<div class="m2"><p>نه فلک را آتش من در گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سینه ام چون کوره حداد شد</p></div>
<div class="m2"><p>دل درون سینه در فریاد شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبر از دست دلم دامن کشید</p></div>
<div class="m2"><p>بر تن خود جامه ی طاقت درید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور به بنیاد شکیب آمد شکست</p></div>
<div class="m2"><p>رفت ارکان توان از پای بست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آه بازم سخت کاری اوفتاد</p></div>
<div class="m2"><p>با غمم مشکل شماری اوفتاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یا احبائی هلموا بالعجل</p></div>
<div class="m2"><p>نیک ذاک الروح کالمزن العطل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>معشرالاحباب یا اهل الوداد</p></div>
<div class="m2"><p>احضروا حولی خذواعنی الحداد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اسمعوا یا اهل وادی عضتی</p></div>
<div class="m2"><p>ثم لومونی و شقوا کلبتی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>محفل غم گشت عشرت خانه ام</p></div>
<div class="m2"><p>باده خون گردید در پیمانه ام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای رفیقان حلقه ها گرد آورید</p></div>
<div class="m2"><p>جامه ها از بهر ماتم بر درید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سوی من آیید تا زاری کنیم</p></div>
<div class="m2"><p>عقل را در سوگ جان یاری کنیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>منزل اندر خاک و خاکستر کنیم</p></div>
<div class="m2"><p>خاک و خاکستر همه بر سر کنیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرچه سنگ آن را به فرق خود زنیم</p></div>
<div class="m2"><p>هرچه خار آن را به سینه بشکنیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هرچه اندر دشت خاکی سر کنیم</p></div>
<div class="m2"><p>دشت را از آب دیده تر کنیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گه بحسرت دستها بر سر زنیم</p></div>
<div class="m2"><p>گه به ناخن سینه و رخ برکنیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز آه گرم و ناله های پرشرر</p></div>
<div class="m2"><p>آتش اندازیم اندر خشک و تر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از سرشک چشم و خوناب جگر</p></div>
<div class="m2"><p>موجها سازیم اندر بحر و بر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای رفیقان با من انبازی کنید</p></div>
<div class="m2"><p>با من بیچاره دمسازی کنید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>انجمن سازید در پیرامنم</p></div>
<div class="m2"><p>تکمه بگشایید از پیراهنم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا بدامان جیب جان چاکم کنید</p></div>
<div class="m2"><p>خاکتان بر سر بسر خاکم کنید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نوحه آغازید بگشایید موی</p></div>
<div class="m2"><p>موکنان با مویه بخراشید روی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رشته سجاده در دامن کنید</p></div>
<div class="m2"><p>دامن از لخت جگر گلشن کنید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جامه ام نیلی کن ای همدم که من</p></div>
<div class="m2"><p>پیش دارم سوگ جان ممتحن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کحل گون کن جامه ای مجرم که باز</p></div>
<div class="m2"><p>خیرخیرم ماتمی آمد فراز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آستین ای هم دم از چشم ترم</p></div>
<div class="m2"><p>دور کن تا بگذرد آب از سرم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زین سپس ای دیده ی من خونگری</p></div>
<div class="m2"><p>خونگری ز ابر بهار افزونگری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای دهان از خنده اکنون لب ببند</p></div>
<div class="m2"><p>لب ببند از خنده و دیگر مخند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ریزم اکنون بر سر از یکدست خاک</p></div>
<div class="m2"><p>سازم از دست دگر دل چاک چاک</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یاری من کن دمی ای غمگسار</p></div>
<div class="m2"><p>تا بحال جان بگریم زار و زار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جامه سازم پاره پاره تاروپود</p></div>
<div class="m2"><p>مویه گویم چون زنان مرده رود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کاوخ آوخ آفتابم شد نهان</p></div>
<div class="m2"><p>مهر جان آمد به برج مهر جان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آوخ آوخ ماه من شد در محاق</p></div>
<div class="m2"><p>آوخ آمد کوکبم را احتوراق</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آتشی نمرودیان افروختند</p></div>
<div class="m2"><p>وندر آن آتش خلیلم سوختند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آذر اندر تخمه ی آذر گرفت</p></div>
<div class="m2"><p>دیو از دست جم انگشتر گرفت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کشته شد هابیل من ای داد داد</p></div>
<div class="m2"><p>کشتی نوحم به گرداب اوفتاد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ای دریغا یوسفم در چاه شد</p></div>
<div class="m2"><p>او بماند و کاروان در راه شد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دیو اورنگ سلیمانی گرفت</p></div>
<div class="m2"><p>کشور جم راه ویرانی گرفت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دیو آمد تاخت بر ملک دلم</p></div>
<div class="m2"><p>کرد غارت آنچه دید از حاصلم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هر متاعی بود در اقلیم جان</p></div>
<div class="m2"><p>شد به یغما کاروان در کاروان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کشور دل زان سپاه بیحساب</p></div>
<div class="m2"><p>هرچه بد غارت شد آن ملک خراب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کعبه ام شد پایمال پای پیل</p></div>
<div class="m2"><p>موسیم شد غرقه ی دریای نیل</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>یوسفم افتاد در چنگال گرگ</p></div>
<div class="m2"><p>پیشم آورد آسمان سوکی سترگ</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>باز باغ افروزدم سردی گرفت</p></div>
<div class="m2"><p>سبززار خرمی زردی گرفت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>باغ عشرت برگ ریزان ساز کرد</p></div>
<div class="m2"><p>گلبن دولت خزان آغاز کرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بامداد عیش من آمد به شام</p></div>
<div class="m2"><p>آفتاب دولتم شد در غمام</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ای دریغا ابر گوهر بار من</p></div>
<div class="m2"><p>ای دریغا گلشن و گلزار من</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ای دریغا مرهم هر داغ من</p></div>
<div class="m2"><p>راحت من روح من ریحان من</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ای دریغا چشمه ی حیوان من</p></div>
<div class="m2"><p>گلشن من جنت من باغ من</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ای دریغ از طوطی گویای من</p></div>
<div class="m2"><p>عندلیب بوستان آرای من</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>حیف از آن آهوی مشکین حیف حیف</p></div>
<div class="m2"><p>حیف از آن طاوس رنگین حیف حیف</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>حیف از آن شهباز اوج کبریا</p></div>
<div class="m2"><p>حیف از آن سرمایه ملک بقا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>آب حیوان تیره گون شد حیف حیف</p></div>
<div class="m2"><p>عقل مغلوب جنون شد حیف حیف</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مؤمنی در پنجه کافر دریغ</p></div>
<div class="m2"><p>عاجزی در چنگ زورآور دریغ</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ای دریغ اسلام را لشکر شکست</p></div>
<div class="m2"><p>کافری دندان پیغمبر شکست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کو فروغ کوکب فیروزیم</p></div>
<div class="m2"><p>کو ضیای اختر بهروزیم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>کو گل صدبرگ باغ افروز من</p></div>
<div class="m2"><p>کو مه شب آفتاب روز من</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>آفتاب جان به مغرب شد نهان</p></div>
<div class="m2"><p>وای جان ایوای جان ایوای جان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>روزگارم رفت روزم گشت پیر</p></div>
<div class="m2"><p>جان بدست دیو بی پروا اسیر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>جان علوی در چه سجین غریب</p></div>
<div class="m2"><p>مانده او را نی انیسی نی حبیب</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>طایر قدس آشیان شد در قفس</p></div>
<div class="m2"><p>دور هم از آشیان و هم نفس</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>آخر ای هم آشیانها همتی</p></div>
<div class="m2"><p>ای شما در گلستانها همتی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>یاد آرید ای محبان وطن</p></div>
<div class="m2"><p>روزی آخر زین غریب ممتحن</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>همتی ای نیکبختان همتی</p></div>
<div class="m2"><p>ای شما فارغ ز زندان همتی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>یاد آرید ای شما آزادگان</p></div>
<div class="m2"><p>زین اسیر مستمند مستهان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چون پسندید ای گروه قدسیان</p></div>
<div class="m2"><p>ای به ملک قدسیان جا و مکان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ای شما یکتن گرفتار و اسیر</p></div>
<div class="m2"><p>در میان دشمنان زار و حقیر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ای شما در عیش و شادی روز و شب</p></div>
<div class="m2"><p>خالی از اندوه و فارغ از تعب</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چون پسندید از شما یکتن غریب</p></div>
<div class="m2"><p>مانده از شادی و عشرت بی نصیب</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چون پسندید ای شما را عرشگاه</p></div>
<div class="m2"><p>بینوایی از شما محبوس چاه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ای شما را بنده اندر هر خمی</p></div>
<div class="m2"><p>از کمند اسفندیار و رستمی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بنگرید آخر سیاوش را اسیر</p></div>
<div class="m2"><p>در کف ترکان خونخوار دلیر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>آخر ای ترکان حمیت تان کجاست</p></div>
<div class="m2"><p>پهلوانی کو و غیرت تان کجاست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>آخر آن شهزاده را خون ریختند</p></div>
<div class="m2"><p>خون او با خاک ره آمیختند</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>یاد آرید آخر ای گردان نیو</p></div>
<div class="m2"><p>زان گرفتار کمند ریو دیو</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>یاد آرید ای امیران زان اسیر</p></div>
<div class="m2"><p>وقت او شد تنگ و روزش گشت دیر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>یاد آرید ای شهان از آن گدا</p></div>
<div class="m2"><p>این گدا هم بود از جنس شما</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>یاد آرید آخر ای یاران ما</p></div>
<div class="m2"><p>یکزمان از ما و از دوران ما</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>یاد آرید ای گروه دوستان</p></div>
<div class="m2"><p>وقت گشت و دشت سیر بوستان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>از غریبی مانده دور از شهر خویش</p></div>
<div class="m2"><p>با دلی از زخم هجران ریش ریش</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ای شما با هم بطرف جویبار</p></div>
<div class="m2"><p>ای شما دامن کشان بر سبزه زار</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>از من و ایام من یاد آورید</p></div>
<div class="m2"><p>از دل ناکام من یاد آورید</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>صبحگاهان چون به گلشن پا نهید</p></div>
<div class="m2"><p>یکقدم هم کو به یاد ما نهید</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>یاد آر ای محرم اسرار من</p></div>
<div class="m2"><p>از من و این سینه ی افکار من</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>در سحرگاهان بطرف بوستان</p></div>
<div class="m2"><p>چون بچینی گل به یاد دوستان</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>یک گل حسرت بچین بر یاد من</p></div>
<div class="m2"><p>یاد کن از این دل ناشاد من</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>با حریفان چون نشینی در چمن</p></div>
<div class="m2"><p>باده پیمایی ببویی نسترن</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>روزگار من فراموشت مباد</p></div>
<div class="m2"><p>جرعه ی بی یاد من نوشت مباد</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>چون شوی سرخوش ز شور باده نیز</p></div>
<div class="m2"><p>یک قدح بر یاد من برخاک ریز</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>یا به یاد من یکی ساغر بنوش</p></div>
<div class="m2"><p>ای فدایت جسم و جان و عقل و هوش</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>آری آری یاد یاران خوش بود</p></div>
<div class="m2"><p>خاصه از یاری که در آتش بود</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>آری آری یاد یاران کهن</p></div>
<div class="m2"><p>خوش بود خوش خاصه از یاری چو من</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>همچو من یاری به هجران سوخته</p></div>
<div class="m2"><p>دیده اندر راه جانان دوخته</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>دور از یار و دیار افتاده ای</p></div>
<div class="m2"><p>آه آه از چشم یار افتاده ای</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>نی به کام او شده روزی بسر</p></div>
<div class="m2"><p>بر مرادش نی شبی گشته سحر</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>کرده راحت را به دنیا خیر باد</p></div>
<div class="m2"><p>برده نام عیش و عشرت را زیاد</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>از بد و نیک جهان وارسته ای</p></div>
<div class="m2"><p>در به روی زشت و زیبا بسته ای</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>بر دو عالم آستین افشانده ای</p></div>
<div class="m2"><p>مصلحت را از در خود رانده ای</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>سیر از جان و جهان گردیده ای</p></div>
<div class="m2"><p>هرچه مشکل برخود آسان دیده ای</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>گوشه ای بگرفته ز اهل روزگار</p></div>
<div class="m2"><p>رم گرفته زین گروه دیوسار</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>کشتی خود را به توفان داده ای</p></div>
<div class="m2"><p>دل به غرقاب بلا بنهاده ای</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>هر کریوه در جهان طی کرده ای</p></div>
<div class="m2"><p>مرکب امید خود پی کرده ای</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>آتش اندر خانمان افکنده ای</p></div>
<div class="m2"><p>از جهان سیری ز جان دل کنده ای</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>سینه خود شرحه شرحه خواسته</p></div>
<div class="m2"><p>تن بتاب و تب دل از غم کاسته</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>زاتش دل هم به روز و هم به شب</p></div>
<div class="m2"><p>گاهی اندر تاب بوده گاه تب</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>سال و مه با جان خود اندر ستیز</p></div>
<div class="m2"><p>روز و شب از آشنایان در گریز</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>طایری افتاده در بند قفس</p></div>
<div class="m2"><p>نی رهایی و نه پروازش هوس</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>نه سرودی خوانده در فصل بهار</p></div>
<div class="m2"><p>با هم آوازان دمی بر شاخسار</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>نی پری افشانده اندر آشیان</p></div>
<div class="m2"><p>نی گشوده بالی اندر بوستان</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>تا سر از بیضه برآورده دمی</p></div>
<div class="m2"><p>غیر صیادش نبوده همدمی</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>نی کشیده در گلستانی نفس</p></div>
<div class="m2"><p>یا بدامی بوده جا یا در قفس</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>تا برآورده پری ناکام و کام</p></div>
<div class="m2"><p>اول پرواز افتاده به دام</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>کس ندیده همچو او پر سوخته</p></div>
<div class="m2"><p>آتش اندر آشیان افروخته</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>صد طنابش آب از سر رفته ای</p></div>
<div class="m2"><p>کاردش بر استخوان بگذشته ای</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>هر رگش صد نیش و نشتر خورده ای</p></div>
<div class="m2"><p>تیر بر دل تیغ بر سر خورده ای</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>خانه چون خواهد خراب دل کباب</p></div>
<div class="m2"><p>نی ز آتش باک دارد نی ز آب</p></div></div>