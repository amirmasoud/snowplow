---
title: >-
    بخش ۱۸۱ - آمدن ملائکه سموات به یاری خلیل الرحمن
---
# بخش ۱۸۱ - آمدن ملائکه سموات به یاری خلیل الرحمن

<div class="b" id="bn1"><div class="m1"><p>چون شنیدند این خطاب آن قدسیان</p></div>
<div class="m2"><p>از سما کردند رو در خاکدان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمله بگشودند از هم بال و پر</p></div>
<div class="m2"><p>جانب این دشت غبرا پی سپر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر یکی جستی از آن دیگر سبق</p></div>
<div class="m2"><p>تا بپایین آمدند از نه طبق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منجنیق اندر هوا برخاسته</p></div>
<div class="m2"><p>کامد اول یک ملک آراسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کای خلیل الله منم دارای باد</p></div>
<div class="m2"><p>حق زمام باد در دستم نهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم بده فرمان که گویم باد را</p></div>
<div class="m2"><p>برکند این قوم بی بنیاد را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جله را بر کوه و بر صحرا زند</p></div>
<div class="m2"><p>خیمه شان بر لجه ی دریا زند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم بریزد آتش سوزانشان</p></div>
<div class="m2"><p>هم به خانمان و هم بر جانشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جسم و جانشان دود خاکستر کند</p></div>
<div class="m2"><p>سروریها را ز سرشان درکند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکه بهر دیگری آتش فروخت</p></div>
<div class="m2"><p>باید اول خود در آن آتش بسوخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکه خواهد آتشی افروختن</p></div>
<div class="m2"><p>بایدش در آتش خود سوختن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت ابراهیم او را کی ملک</p></div>
<div class="m2"><p>مکرمت کردی جزاء خیرلک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ما و امت را برو با هم گذار</p></div>
<div class="m2"><p>این من و این آتش این پروردگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آتش اندر راه آن سلطان راد</p></div>
<div class="m2"><p>گرچه صد دریا بود باد است باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از پی آمد آن ملک دیگر روان</p></div>
<div class="m2"><p>کای خلیل صدق و ای جان جهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جمله دریاها به فرمان منند</p></div>
<div class="m2"><p>کوهها لرزان ز توفان منند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رخصتی ده بحر را بر پر زنم</p></div>
<div class="m2"><p>موج دریاها بر آن اخگر زنم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ناخدایم لیک توفانی کنم</p></div>
<div class="m2"><p>در بسیط خاک ویرانی کنم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت رو رو حق ز تو خشنود باد</p></div>
<div class="m2"><p>جان فدای آنکه هست و بود باد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چونکه در راه شه خوبان بود</p></div>
<div class="m2"><p>آب و آتش پیش من یکسان بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آبو آتش را بر من فرق نیست</p></div>
<div class="m2"><p>دوستان را فکر حرق و غرق نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر بسوزد این تنم قربان او</p></div>
<div class="m2"><p>ور ببخشاید زهی احسان او</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کان ملک دیگر ز پی آمد روان</p></div>
<div class="m2"><p>صد ثنا و صد ستایش در دهان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کاین منم سرهنگ ابر و رعد و برق</p></div>
<div class="m2"><p>گه به مغربشان کشم گاهی به شرق</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رخصتی ده تا بگویم مر سحاب</p></div>
<div class="m2"><p>اندر این آتش فرو ریزند آب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آتش نمرودیان ابتر کنم</p></div>
<div class="m2"><p>هم از آن پس خاکشان برسر کنم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفت آن شه هرچه باشد خوش بود</p></div>
<div class="m2"><p>خواه باشد آب خواه آتش بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>من ز تو آتش ز تو ای محتشم</p></div>
<div class="m2"><p>خواه در آبم فکن خواه آتشم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همچنین آمد از آن روحانیان</p></div>
<div class="m2"><p>آنچه ناید در شمار و در بیان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هریکی را عذرخواه آمد خلیل</p></div>
<div class="m2"><p>نی اعانت جست نی آمد دخیل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر که آن گستاخ بزم شاه شد</p></div>
<div class="m2"><p>از عوانان کی اعانت خواه شد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هرکه اندر بزم شه محرم بود</p></div>
<div class="m2"><p>کی دخیل حاجب و دربان شود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هرکه داند شاه را دانای راز</p></div>
<div class="m2"><p>کی به نزد ترجمان آرد نیاز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زان ملایک هیچ یک حاجت نخواست</p></div>
<div class="m2"><p>سر در آتش منجنیق آورد راست</p></div></div>