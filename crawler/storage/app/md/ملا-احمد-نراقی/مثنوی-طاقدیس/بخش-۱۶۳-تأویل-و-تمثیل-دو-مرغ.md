---
title: >-
    بخش ۱۶۳ - تأویل و تمثیل دو مرغ
---
# بخش ۱۶۳ - تأویل و تمثیل دو مرغ

<div class="b" id="bn1"><div class="m1"><p>عقل و نفسند این دو مرغ ای دوستان</p></div>
<div class="m2"><p>کاشیان دارند در این بوستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاشیانشان این تن خاکی بود</p></div>
<div class="m2"><p>سیرشان در جو افلاکی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>متحد با یکدگر آغازشان</p></div>
<div class="m2"><p>متفق اندر ازل پروازشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمدند از بوستان در توشکان</p></div>
<div class="m2"><p>سر زدند از بیضه ی روحانیان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پر زدند از گلستان جاودان</p></div>
<div class="m2"><p>آشیان بستند در این توشکان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با هم اینجا یار و دمساز آمدند</p></div>
<div class="m2"><p>هردم از سویی به پرواز آمدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بسا صیاد در این دشت و بام</p></div>
<div class="m2"><p>دانها پاشیده و افکنده دام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صوت مرغان چمن آموخته</p></div>
<div class="m2"><p>آتشی از بهرشان افروخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از صفیر خود ره مرغان زنند</p></div>
<div class="m2"><p>ای عجب خسبند و راه جان زنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هان و هان ای مرغ نوآموز من</p></div>
<div class="m2"><p>بلبل خوش نغمه ی فیروز من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این نوای عندلیب باغ نیست</p></div>
<div class="m2"><p>غیر بانگ جان خروش زاغ نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نفس مسکین از فریب حرص و آز</p></div>
<div class="m2"><p>سوی دانه می کند گردن دراز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر هوای آن صفیر دلفریب</p></div>
<div class="m2"><p>می رود بی صبر و آرام و شکیب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عقل می گوید که ای روح روان</p></div>
<div class="m2"><p>ای تورا در لامکانها آشیان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از فریب این چمن ایمن مشو</p></div>
<div class="m2"><p>زین صفیر مصطنع از ره مرو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نفس می گوید که ظن بد مبر</p></div>
<div class="m2"><p>ان بعض الظن اثم کن زبر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عقل می گوید یقین است این نه ظن</p></div>
<div class="m2"><p>حبک للشیئی یعمی دم مزن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نفس می گوید یقین گیرم ولی</p></div>
<div class="m2"><p>کی فریبد چون منی را ای ولی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عقل گوید ناگهان بفریفتند</p></div>
<div class="m2"><p>شهسواران اندرین ره شیفتند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نفس گوید گر فریبد سهل دان</p></div>
<div class="m2"><p>ناامیدی کار هر نااهل دان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عمر هست و زندگانی بس دراز</p></div>
<div class="m2"><p>توبه مقبول و در توبه است باز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>توبه خواهم گرد با سوز و گداز</p></div>
<div class="m2"><p>هم تلافیها به صد عجز و نیاز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بس نماز و روزه خواهم کرد من</p></div>
<div class="m2"><p>از درش دریوزه خواهم کرد من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ور نکردم توبه رفتم زین سرای</p></div>
<div class="m2"><p>هم نیم نومید از لطف خدای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ناامیدی کفر باشد ای عمو</p></div>
<div class="m2"><p>رو بخوان لاتیأسوا لاتقنطوا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هین ببین این نفس کافر کیش را</p></div>
<div class="m2"><p>چون فریبد عقل را و خویش را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر تو می دانی خدا را ذوالکرم</p></div>
<div class="m2"><p>این کرم در کار دنیا هست هم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پس چرا آن را به آن نگذاشتی</p></div>
<div class="m2"><p>مقتدر خود را در آن پنداشتی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دانه امسال ای رئیس ده مریز</p></div>
<div class="m2"><p>کاو رحیمست و دهد بی دانه نیز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یک سفر سرمایه مطلب ای ودود</p></div>
<div class="m2"><p>کان کریم است و دهد بی مایه سود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آیه ی رحمت همین ای بوالهوس</p></div>
<div class="m2"><p>آمده است از بهر کار دین و بس</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در نماز و روزه و حج و جهاد</p></div>
<div class="m2"><p>آیه ی لاتقنطوا داری بیاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کار دنیا چونکه پیش آمد تورا</p></div>
<div class="m2"><p>لیس للانسان الا ما سعی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آنکه در عقبی کریم است و رحیم</p></div>
<div class="m2"><p>بهر دنیاکی بخیل است و لئیم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آنکه را در کار دین صد چاره است</p></div>
<div class="m2"><p>بهر دنیایت چرا بیگانه است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ای تو دست آموز ابلیس پلید</p></div>
<div class="m2"><p>پرده ی خود تابکی خواهی درید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دست تو در دست شیطان تا بچند</p></div>
<div class="m2"><p>با خدا و خلق تا کی آسمند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دست خود از دست این ابتر بکش</p></div>
<div class="m2"><p>رخت خود تا چشمه ی کوثر بکش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای خدا فریاد از این نفس پلید</p></div>
<div class="m2"><p>تا کجا آخر مرا خواهد کشید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>می کشد نفسم که یا رب کشته باد</p></div>
<div class="m2"><p>در میان خاک و خون آغشته باد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر زمان امروز و فردا می کند</p></div>
<div class="m2"><p>خاکم اندر چشم بینا می کند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گوش من بگرفته می گرداندم</p></div>
<div class="m2"><p>هر کجا خواهد چو خر می راندم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هرچه می گویم که این چاهست چاه</p></div>
<div class="m2"><p>عاقبت گوید برآری سر ز راه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هرچه می گویم بیابان است این</p></div>
<div class="m2"><p>گویدم میرو که آسان است این</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سوختم تا چند خواهم سوختن</p></div>
<div class="m2"><p>شعله در جان تا بکی افروختن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سوختم آخر من ای فریادرس</p></div>
<div class="m2"><p>رحمتی فرما مرا فریاد رس</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کاشکی بودی سترون مام من</p></div>
<div class="m2"><p>در جهان هرگز نبودی نام من</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>من نبردستم حسد بر هیچکس</p></div>
<div class="m2"><p>جز به آنکس کو نزاد از مام و بس</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>رحمت حق بر تو باد ای نیکنام</p></div>
<div class="m2"><p>این پسر را می نزادی کاش مام</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کاش چون زادی نپروردی مرا</p></div>
<div class="m2"><p>یا به غرقابی رها کردی مرا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یا مرا کردی رها در کوهسار</p></div>
<div class="m2"><p>تا ز من گرگی برآوردی دمار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یا رسول الله یا مولی النعم</p></div>
<div class="m2"><p>یا غیاث الخلق یا کهف الامم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>یا محمد یا شفیع المذنبین</p></div>
<div class="m2"><p>یا امین الله رب العالمین</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سوره ی نور آیه ی رحمت تویی</p></div>
<div class="m2"><p>خلق عالم را ولی نعمت تویی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ما همه بنده تو مولای همه</p></div>
<div class="m2"><p>جمله چون قطره تو دریای همه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ای زمین و آسمان خاک درت</p></div>
<div class="m2"><p>حضرت روح القدس یک چاکرت</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نیست چون برجان خود رحمت مرا</p></div>
<div class="m2"><p>رحم کن تو ای ولی نعمت مرا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>خوانده در مصحف تورا احمد بنام</p></div>
<div class="m2"><p>احمدم من هم غلامت را غلام</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چونکه همنام توام خوارم مکن</p></div>
<div class="m2"><p>حق همنامی که انکارم مکن</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>فخر دارم من به این ای شهریار</p></div>
<div class="m2"><p>تو مرا مگذار خوار و شرمسار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ای امام عصر و سلطان زمان</p></div>
<div class="m2"><p>ای جهان جان و ای جان جهان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ای ز تو روشن چراغ مهر و ماه</p></div>
<div class="m2"><p>ای جهانت کشور و خلقت سپاه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ای وجودت لنگر چرخ و زمین</p></div>
<div class="m2"><p>ای تورا ملک ابد زیر نگین</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ای نبی را آخرین قایم مقام</p></div>
<div class="m2"><p>ای ولی مؤتمن رکن انام</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ای شه دوران و حق مشتهر</p></div>
<div class="m2"><p>صاحب دوران امام منتظر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گرچه عالم سربه سر پر نور توست</p></div>
<div class="m2"><p>چشم بدبین دور تا یا کور توست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>حرمت جدت که در من یک نظر</p></div>
<div class="m2"><p>مانده ام تنها به بند نفس در</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>همتی کن با من ای مولی رفیق</p></div>
<div class="m2"><p>تا مگر یابم نجات از این مضیق</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ای صفایی ای به صد غم مبتلا</p></div>
<div class="m2"><p>ای غریق بحر عصیان و خطا</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سخت می بینم تورا بس ناامید</p></div>
<div class="m2"><p>خون دل از دیده ات خواهد چکید</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گرچه نومیدی و یأس از خود بجاست</p></div>
<div class="m2"><p>لیکن نومیدی ز لطف حق خطاست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ای بسا از تو بتر بخشیده است</p></div>
<div class="m2"><p>گرچه دهر از تو بتر کم دیده است</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>تا توانی عجز و زاری پیش گیر</p></div>
<div class="m2"><p>در حیوة خویش سوگ خویش گیر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دست در دامان پیغمبر فکن</p></div>
<div class="m2"><p>کار خود با ساقی کوثر فکن</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بلکه او ناخوانده غمخوار همه است</p></div>
<div class="m2"><p>در دو عالم لطف او یار همه است</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>آری او باشد نگهدار همه</p></div>
<div class="m2"><p>ما همه جسمیم و او جان همه</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>گرچه باشد جسم از جان بیخبر</p></div>
<div class="m2"><p>جان بهر جزوی از آن دارد نظر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>متصل باشد بهر عضوی از آن</p></div>
<div class="m2"><p>ما همه جسمیم و آن مولاست جان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>زابتدا تا انتها همراه ماست</p></div>
<div class="m2"><p>از قدم تا فرق ما آگاه ماست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>جسم او پیدا و پنهان کار او</p></div>
<div class="m2"><p>سر به سر عالم پر از آثار او</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>نور او بر خوب و بر بد تافته</p></div>
<div class="m2"><p>هرکسی زان قسمت خود یافته</p></div></div>