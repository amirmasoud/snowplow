---
title: >-
    بخش ۱۳۷ - شاه اولیا که فرمود الهی ما عبدتک خوفاً من نارک
---
# بخش ۱۳۷ - شاه اولیا که فرمود الهی ما عبدتک خوفاً من نارک

<div class="b" id="bn1"><div class="m1"><p>طاعت من نی ز شوق جنت است</p></div>
<div class="m2"><p>نی ز بیم آتش پر وحشت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عبادت نی طمع دارم نه بیم</p></div>
<div class="m2"><p>هرچه می خواهی بکن ما از توایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ببخشی ور بسوزانی رواست</p></div>
<div class="m2"><p>نی کسی را جرأت چون و چراست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنده را با این کن و آن کن چه کار</p></div>
<div class="m2"><p>ملک ملک توست ای پروردگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنده ایم و پیشه ی ما بندگی است</p></div>
<div class="m2"><p>بندگیهامان همه شرمندگیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون تورا اهل پرستش یافتم</p></div>
<div class="m2"><p>در سپاس و طاعتت بشتافتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون تورا دیدم سزاوار سپاس</p></div>
<div class="m2"><p>بندگی را هم بر آن کردم قیاس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برالوهیت دو دیده دوختم</p></div>
<div class="m2"><p>وز خدایی بندگی آموختم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بندگی آموز از آن آزاده مرد</p></div>
<div class="m2"><p>کس خدا را بندگی چون او نکرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هان و هان همراه آن آزاد رو</p></div>
<div class="m2"><p>بگذر از مزدوری و آزاد شو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چیست آزادی در این ره بندگی</p></div>
<div class="m2"><p>بندگی شد شاهی و فرخندگی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طاعت مزدور بهر اجرت است</p></div>
<div class="m2"><p>بنده را طاعت ز خوف و خشیت است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زین نکوتر اینکه چون من بنده ام</p></div>
<div class="m2"><p>بندگیها می کنم تا زنده ام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خویشتن بینی در اینها اندر است</p></div>
<div class="m2"><p>طاعت احرار از اینها برتر است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طاعت آزادگان دانی که چیست</p></div>
<div class="m2"><p>این که در نیت بجز معبود نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چونکه او را اهل طاعت دیده اند</p></div>
<div class="m2"><p>طاعت او زین سبب بگزیده اند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون سزاوار نماز و طاعت است</p></div>
<div class="m2"><p>طاعت او خواجگی و حشمت است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بنده باید بودن اما ای پسر</p></div>
<div class="m2"><p>طاعت آزادگان باشد هنر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طاعت احرار خواهند از شما</p></div>
<div class="m2"><p>نی ز قید بندگی گشتن رها</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جوید از تو طاعت احرار را</p></div>
<div class="m2"><p>نی ز سر کردن برون افسار را</p></div></div>