---
title: >-
    بخش ۱۲۲ - اشاره به حدیث قدسی که فرمود دل دوستان خود را از طینت ابراهیم و موسی و عیسی و محمد ص آفریده ام
---
# بخش ۱۲۲ - اشاره به حدیث قدسی که فرمود دل دوستان خود را از طینت ابراهیم و موسی و عیسی و محمد ص آفریده ام

<div class="b" id="bn1"><div class="m1"><p>جان ابراهیم و موسای کلیم</p></div>
<div class="m2"><p>جان عیسی و شه بطحا حریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر گلی زان غیرت صد بوستان</p></div>
<div class="m2"><p>عطر گلهایش ز عطرستان جان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی دی آنجا راه یابد نی خزان</p></div>
<div class="m2"><p>نی ز با دست و نه از برقش زیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلشنی بادش نسیم زلف یار</p></div>
<div class="m2"><p>دست قدرت اندر آنجا بازیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلشنی آبش ز ینبوع حیات</p></div>
<div class="m2"><p>چار حدش هست بیرون از جهات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل به عشق او سپار و شاد زی</p></div>
<div class="m2"><p>از همه غمها برون آزاد زی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیمه زد چون در دلت سلطان عشق</p></div>
<div class="m2"><p>ملک دل گردیده شهرستان عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم هوا زانجا گریزد هم هوس</p></div>
<div class="m2"><p>جز یکی آنجا نیابی هیچکس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نی تورا باشد رضا و نی غضب</p></div>
<div class="m2"><p>نی طرب باشد تورا و نی هرب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنچه او خواهد همی خواهی و بس</p></div>
<div class="m2"><p>نی هوا باشد تورا و نی هوس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بلکه خواهش از تو بگریزد چنان</p></div>
<div class="m2"><p>کانچه تو خواهی نخواهی خواهد آن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نخل خواهش برفتد از بیخ و بن</p></div>
<div class="m2"><p>گوش خواهش نشنود جز امر کن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گیرد اندر بزم اطمینان مقام</p></div>
<div class="m2"><p>فادخلی فی جنتی آمد پیام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جنت او نی جنان سیب و نار</p></div>
<div class="m2"><p>آن جنان بر خود پرستان واگذار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر شدی از جنت او مختبر</p></div>
<div class="m2"><p>رو بخوان عند ملیک مقتدر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بارگاه صدق و ایوان جلال</p></div>
<div class="m2"><p>در جوار پادشاه ذوالجلال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نور این محفل بتابد چون به دل</p></div>
<div class="m2"><p>پاک گردد از ظلام آب و گل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پاک گردد همسر پاکان شود</p></div>
<div class="m2"><p>للخبیثین الخبیثات افکند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پا زند از جز یکی از کاینات</p></div>
<div class="m2"><p>هم بجوید طیبین و طیبات</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نی ز فردوسش نشاط و نی نعیم</p></div>
<div class="m2"><p>نی ز دوزخ باک او را نی جحیم</p></div></div>