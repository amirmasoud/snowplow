---
title: >-
    بخش ۲۱۳ - در بیان فریب دادن نفس اماره آدمی را
---
# بخش ۲۱۳ - در بیان فریب دادن نفس اماره آدمی را

<div class="b" id="bn1"><div class="m1"><p>در ریاضت روزی آوردی به شب</p></div>
<div class="m2"><p>شب رسانیدی به روز اندر طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده بر خود راحت دوران تمام</p></div>
<div class="m2"><p>روز و شب اندر صیام و در قیام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر دهان نفس افکنده لگام</p></div>
<div class="m2"><p>زان لگامش میخها رفته به کام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیغ فرمان بر سرش واداشته</p></div>
<div class="m2"><p>صد رقیبش پیش و پس بگماشته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل بهر دو دست خود بگرفته سخت</p></div>
<div class="m2"><p>می فشردی همچو آن فصاد رخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر زبان مسمار و بر لبها لویش</p></div>
<div class="m2"><p>بر دو دیده نشتر و بر سینه ریش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزی اندر خلوتی بنشسته زار</p></div>
<div class="m2"><p>در گلوی نفس افکنده فسار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناگهان آمد به گوشش از برون</p></div>
<div class="m2"><p>الغزا و الغزا یا مسلمون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای گروه مسلمین روز غزاست</p></div>
<div class="m2"><p>روز حرب کافران ناسزاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در نهادش شوق غزو آمد پدید</p></div>
<div class="m2"><p>دل درون سینه از شوقش تپید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طبع می کردش همی تحریص جنگ</p></div>
<div class="m2"><p>نفس می گفتش روان شو بیدرنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می رود وقت جهاد از دست خیز</p></div>
<div class="m2"><p>فرصتی تا هست خون خود بریز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هین برو گوی سعادت بازجوی</p></div>
<div class="m2"><p>ای خنک آنکو برد امروز گوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرد جست از جا ولی با صد شگفت</p></div>
<div class="m2"><p>بر به دندان ناخن از حیرت گرفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نفس را کی در غزا تلواسه است</p></div>
<div class="m2"><p>زیر این کاسه یقین نیم کاسه است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت ای نفس دغای حیله ور</p></div>
<div class="m2"><p>تا چه مکر تازه ای داری دگر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رهنمایی نیست کارت ای لعین</p></div>
<div class="m2"><p>اندرین ره کنده ای چاهی یقین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نفس اگر خواند تورا سوی حرم</p></div>
<div class="m2"><p>منتظر بنشانده آنجا صد صنم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مصحفی گر بخشدت کاین را بخوان</p></div>
<div class="m2"><p>مصحفش از مسجدی دزدیده دان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رهنمایی گر کند میدان یقین</p></div>
<div class="m2"><p>دزدها دارد در آن ره در کمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر به مسجد خواندت بهر نماز</p></div>
<div class="m2"><p>حیله ای دارد دو چشمان دار باز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رهزنست او کی نماید رهبری</p></div>
<div class="m2"><p>کی ز ابلیسی سزد پیغمبری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دزد راه توست نفست ای فتی</p></div>
<div class="m2"><p>از کفش در کش زمام ناقه را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ره خطرناکست و پر دزد و دغل</p></div>
<div class="m2"><p>هان عنان مرکبت از کف مهل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اندرین ره جان من باهوش باش</p></div>
<div class="m2"><p>پای تا سرچشم باش و گوش باش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چشم باش اما چه چشمی تیزبین</p></div>
<div class="m2"><p>گوش اما گوش خالی از طنین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفت با نفس ای بزرگ حیله جوی</p></div>
<div class="m2"><p>تا چه مکری در نظر داری بگوی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت می جویم خلاص از دست تو</p></div>
<div class="m2"><p>چند باشم من نشان شست تو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سالها شد تا درین گنج نهان</p></div>
<div class="m2"><p>می کشی هر دم به تیغم رایگان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بهر من هر لحظه قتل تازه ای ست</p></div>
<div class="m2"><p>هم ز نام نیک و نی آوازه ای ست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هفت هفتم می کشید اینجا چنین</p></div>
<div class="m2"><p>نی کست گوید ثنای آفرین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>غیر یک کشتن نباشد در جهاد</p></div>
<div class="m2"><p>می شوی فارغ ز هر رنج و فساد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پهن گردد نامت اندر روزگار</p></div>
<div class="m2"><p>نام نیکی ماند از تو یادگار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفت دانستم برو ای بدقمار</p></div>
<div class="m2"><p>هم خلاصی جویی و هم اشتهار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خود پرستی باشدت مقصد از آن</p></div>
<div class="m2"><p>می زنی یک تیر را بر دو نشان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>می دهی زهرم ولی در انگبین</p></div>
<div class="m2"><p>می زنی تیرم ولیکن در کمین</p></div></div>