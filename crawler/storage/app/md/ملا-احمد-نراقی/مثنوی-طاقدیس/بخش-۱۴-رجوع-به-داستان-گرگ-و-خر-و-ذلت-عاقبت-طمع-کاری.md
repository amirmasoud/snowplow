---
title: >-
    بخش ۱۴ - رجوع به داستان گرگ و خر و ذلت عاقبت طمع کاری
---
# بخش ۱۴ - رجوع به داستان گرگ و خر و ذلت عاقبت طمع کاری

<div class="b" id="bn1"><div class="m1"><p>ناگهان گرگی ز گرد ره رسید</p></div>
<div class="m2"><p>اندر آن صحرا خری افتاده دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نعره ی شادی برآورد از جگر</p></div>
<div class="m2"><p>کی دو دیده طالع میمون نگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوکبت اکنون برآمد از وبال</p></div>
<div class="m2"><p>آمد اینک روزی پاک حلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاد زی ای بخت میمون شاد زی</p></div>
<div class="m2"><p>از تهی دستی کنون آزادزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم از این خر میخور و هم مایه کن</p></div>
<div class="m2"><p>دورش از چشم بد همسایه کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمد از حرص و شره نزدیک خر</p></div>
<div class="m2"><p>کرده دندان تیز و خواهش تیزتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده بگشاد آن خر و دیدش ز دور</p></div>
<div class="m2"><p>بر سر او گشت بر پا نفخ صور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دید بر بالین خود گرگی کهن</p></div>
<div class="m2"><p>دید مرگ خود به چشم خویشتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت با خود تا بود جان در بدن</p></div>
<div class="m2"><p>بایدم خود را رهانیدن به فن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چونکه مردم هرکه درد گو بدر</p></div>
<div class="m2"><p>ارث ما را هرکه خواهد گو ببر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تن که باشد خاک راهی عاقبت</p></div>
<div class="m2"><p>زنده کو باشد بود زان منفعت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گو نباشد تن چو تن را جان نماند</p></div>
<div class="m2"><p>گو نماند تخت چون سلطان نماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان چو از تن رفت گو تن خاک شو</p></div>
<div class="m2"><p>خاک چون شد خاک آن بر باد رو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قیمت کالای تن از جان بود</p></div>
<div class="m2"><p>آسیای تن ز جان گردان بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس سلامی کرد و گفت ای پیر وحش</p></div>
<div class="m2"><p>از کرم بر این تن لاغر ببخش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا مرا در تن بود این نیم جان</p></div>
<div class="m2"><p>رحمتی فرما و مشکن استخوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرچه من درکار و بار مردنم</p></div>
<div class="m2"><p>ماهی افتاده اندر برزنم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لیک دارم جان و جان شیرین بود</p></div>
<div class="m2"><p>گرچه جان این خر مسکین بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگذر از این مشت پشم و استخوان</p></div>
<div class="m2"><p>زر خالص در عوض از من ستان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صاحب من بود صاحب مکنتی</p></div>
<div class="m2"><p>مکنت بسیار و وافر نعمتی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر من از رحمت نظرها داشتی</p></div>
<div class="m2"><p>چون خر عیسی مرا پنداشتی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آخورم از سنگ مرمر ساختی</p></div>
<div class="m2"><p>جای من از خار و خس پرداختی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کردیم پالان پرند رنگ رنگ</p></div>
<div class="m2"><p>تو بره کردی ز دیبای فرنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نعل کردی از زر خالص مرا</p></div>
<div class="m2"><p>حاضر اینک نعل زر بر دست و پا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نعلها برکن ز دست و پای من</p></div>
<div class="m2"><p>بگذر از جسم هلال آسای من</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نعلها از سم این بیچاره خر</p></div>
<div class="m2"><p>برکن و از قیمتش صد خر بخر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون شنید آن گرگ این راز آن ستور</p></div>
<div class="m2"><p>دیده ی داناییش گردید کور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آری آری از طمعها ای پسر</p></div>
<div class="m2"><p>چشمها و گوشها کور است و کر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پس زبانهای سخن سنج و دراز</p></div>
<div class="m2"><p>لال والکن گشته اند از حرص و آز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از طمع شد پاره دامان ورع</p></div>
<div class="m2"><p>ای دو صد لعنت بر این حرص و طمع</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای بسا تاج از طمع معجر شده</p></div>
<div class="m2"><p>ای چه مردانی ز زن کمتر شده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ای بسا درنده گرگ کهنه کار</p></div>
<div class="m2"><p>از طمع لاغر خری را شد شکار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آنکه مردان را درآورده به بند</p></div>
<div class="m2"><p>آنکه گردان را کشیده درکمند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دیدمش آبستن فوجی لوند</p></div>
<div class="m2"><p>از طمع گشته زبون خیره چند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شیر نر گردد چو روبه از طمع</p></div>
<div class="m2"><p>من طمع ذل و عز من قنع</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ماده گردند از طمع شیران نر</p></div>
<div class="m2"><p>از طمع چیزی نمی بینم بتر</p></div></div>