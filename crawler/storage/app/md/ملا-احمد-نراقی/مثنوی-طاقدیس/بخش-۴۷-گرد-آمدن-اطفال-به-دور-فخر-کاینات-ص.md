---
title: >-
    بخش ۴۷ - گرد آمدن اطفال به دور فخر کاینات ص
---
# بخش ۴۷ - گرد آمدن اطفال به دور فخر کاینات ص

<div class="b" id="bn1"><div class="m1"><p>آن شنیدستی که شاهنشاه دین</p></div>
<div class="m2"><p>پیشوای اولین و آخرین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوی مسجد روزی از دولتسرای</p></div>
<div class="m2"><p>می شدی تا فرض حق آرد بجای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اتفاقاً کودکی چند از عرب</p></div>
<div class="m2"><p>در ره شه در نشاط و در طرب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک بازی می نمودند آن گروه</p></div>
<div class="m2"><p>در ره آن پادشاه باشکوه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چونکه دیدند آن شه لولاک را</p></div>
<div class="m2"><p>آن جهان جان و جان پاک را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمله سوی او دویدند از طرب</p></div>
<div class="m2"><p>جزوها را سوی کل باشد هرب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر شراره بین گریزان تا اثیر</p></div>
<div class="m2"><p>قطره ها بنگر به دریاها قطیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نماند جسم و جان را ارتباط</p></div>
<div class="m2"><p>وارهند از امتزاج و اختلاط</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان علوی پر زند تا لامکان</p></div>
<div class="m2"><p>جسم خاکی جا کند تا خاکدان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان گریزد سوی علیین پاک</p></div>
<div class="m2"><p>تن گراید جانب آن تیره خاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه از نور است آمیزد به نور</p></div>
<div class="m2"><p>در نگارستان اقلیم سرور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وین که از سجین و خاک تیره بود</p></div>
<div class="m2"><p>رفت و اندر خاک ظلمانی غنود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاصه آن کلی که هر کل جزو اوست</p></div>
<div class="m2"><p>او بود مغز و دو عالم جمله پوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مصطفی جانست و عالم جمله جسم</p></div>
<div class="m2"><p>مصطفی معنی بود کونین اسم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کودکان چون زاده ی ایمان بدند</p></div>
<div class="m2"><p>جانب آن قلزم ایمان شدند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون به ایمان نطفه شان شد مستقر</p></div>
<div class="m2"><p>حرف ایمان بود ایشان را پدر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس در آن ره چون پدر را یافتند</p></div>
<div class="m2"><p>بی محابا سوی او بشتافتند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دست در جیب و گریبانش زدند</p></div>
<div class="m2"><p>پنجه اندر طرف دامانش زدند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن یکی بگرفته دامانش بکف</p></div>
<div class="m2"><p>وان دگر بر دامن او با شعف</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن دگر بر کتف او می جست چست</p></div>
<div class="m2"><p>وان دگر بر دامن او راه جست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کای تورا بختی گردون زیر بار</p></div>
<div class="m2"><p>نه سپهرت ناقه ها اندر قطار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یا رجاء الخلق فی نیل الامل</p></div>
<div class="m2"><p>کن لنافی یومنا هذا جمل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یا غیاث الخلق یا کهف الامم</p></div>
<div class="m2"><p>اشتر ما کودکان شو از کرم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اشتر ما شو ولی رهوار باش</p></div>
<div class="m2"><p>قد دوته فرما و اشتروا باش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چونکه می دیدند اطفال عرب</p></div>
<div class="m2"><p>کان شه عالم نبی منتخب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جای می دادی به کتفش گاه گاه</p></div>
<div class="m2"><p>آن دو نور دیده را بی اشتباه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>می نشانیدی به آن کتف گزین</p></div>
<div class="m2"><p>آن دو شمع محفل اهل یقین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آن دو یکتا گوهر بحر وجود</p></div>
<div class="m2"><p>آن دو تابان اختر برج شهود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دو سبق خوان دبستان الست</p></div>
<div class="m2"><p>آن دو از پستان ایمان شیرمست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آن جوانان جنان را افتخار</p></div>
<div class="m2"><p>عرض خلاق جهان را گوشوار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>قرتا عین رسول المؤتمن</p></div>
<div class="m2"><p>بضعة الزهرا حسین والحسن</p></div></div>