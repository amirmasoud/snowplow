---
title: >-
    بخش ۶۷ - حکایت آن شکم‌پرست پرخور که از قرآن کُلوا وَ اشْرَبوا را آموخته بود
---
# بخش ۶۷ - حکایت آن شکم‌پرست پرخور که از قرآن کُلوا وَ اشْرَبوا را آموخته بود

<div class="b" id="bn1"><div class="m1"><p>آن یکی پرسید از پرخواره ای</p></div>
<div class="m2"><p>حرفتی داری و یا بیکاره ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت من دارم همایون پیشه ای</p></div>
<div class="m2"><p>نیست جز آن پیشه ام اندیشه ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مقریم قرآن خوانی شغل من</p></div>
<div class="m2"><p>هم به خلوتگاه هم در انجمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست غیر از مصحف حق یار من</p></div>
<div class="m2"><p>حفظ کن در روز و شب هنجار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت از قرآن چه می دانی بگو</p></div>
<div class="m2"><p>گفت هان بشنو کلوا واشربوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت دیگر گفت دیگر نیست یاد</p></div>
<div class="m2"><p>نیست در قرآن بجز آن سیمناد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من ز قرآن غیر این نادیده ام</p></div>
<div class="m2"><p>بلکه از استاد هم نشنیده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آری آری ذوق و لمس و چشم و گوش</p></div>
<div class="m2"><p>نطق و تخییل و تصور حفظ و هوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جمله اینها پیشکاران دلند</p></div>
<div class="m2"><p>بزم دل را هم وثاق محفلند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنچه دل فرماید آن بیند بصر</p></div>
<div class="m2"><p>وانچه گفت آن بشنود گوش ای پسر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنچه بسپارد به دل آن را نگاه</p></div>
<div class="m2"><p>دارد و دور افکند از خود سواه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جمله اینها چاکرانند و خدم</p></div>
<div class="m2"><p>خدمت سلطان دل را ملتزم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنچه آن غیر از هوای دل بود</p></div>
<div class="m2"><p>گوش و هوش تو از آن زایل بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هان ببین آن فلسفی را ای سلیم</p></div>
<div class="m2"><p>کز تحکم نام خود کرده حکیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غیریؤتی الحکمة آن مرد غبی</p></div>
<div class="m2"><p>می نداند آیه ای را از نبی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وز نبی هم غیر حکمت ضاله ای</p></div>
<div class="m2"><p>نیست چیز دیگران را واله ای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن مباحی مذهب و صوفی لقب</p></div>
<div class="m2"><p>آن مرقع پوش دزد والحرب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرچه تا طاسین مصحف را خوید</p></div>
<div class="m2"><p>غیر مافی الارض من حرم ندید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روز و شب در طاعت امر کلوا</p></div>
<div class="m2"><p>چون گلو شد تا گلویش واشربوا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن فقیه شهر بین مست غرور</p></div>
<div class="m2"><p>کو زند کوس و دهار اندر دهور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از کتاب حق که گنج اعظم است</p></div>
<div class="m2"><p>رطب و یابس جمله در آن مدغم است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>علم ما کان و علم مایکون</p></div>
<div class="m2"><p>اندر آن اندر ظهور و در کمون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مخزن اسرار ربانیست آن</p></div>
<div class="m2"><p>مطلع انوار سبحانیست آن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نسخه بر لیغ ملک سرمد است</p></div>
<div class="m2"><p>نامه ی ناموس شرع احمد است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گوییا نشنیده غیر از فانکحوا</p></div>
<div class="m2"><p>طلقوا و فاغسلوا و امسحوا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ما ان امروز عنا قد هلک</p></div>
<div class="m2"><p>ثلث ثلثان کرد باید ماترک</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ارکعوا را خوانده تا آتوالزکوة</p></div>
<div class="m2"><p>غافل از آیات توحید و صفات</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فقه اعضا و جوارح را درست</p></div>
<div class="m2"><p>کرده ای در فقه نفس روح سست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر گروهی مسلکی بگزیده اند</p></div>
<div class="m2"><p>آیه ی چندش موافق دیده اند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بهر اصلاح هوای نفس خویش</p></div>
<div class="m2"><p>پیش عامه آیه ای آورده پیش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر نه اینها مکر نفس است و هوا</p></div>
<div class="m2"><p>جمله قرآن هست فرمان خدا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آخر آیات اگر هم از خداست</p></div>
<div class="m2"><p>حکمهای محکم ما و شماست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر نداری صد مرض گو پس چرا</p></div>
<div class="m2"><p>مؤمن بعضی و کافر بعض را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عقده ای دارم چه شد دانشوری</p></div>
<div class="m2"><p>تا گشاید بر من حیران دری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا بگوید فاش با طبل و علم</p></div>
<div class="m2"><p>ما اری یا لیت اهلافی الامم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عالم و عامی بهر ملت که هست</p></div>
<div class="m2"><p>در میانشان نیست صد ایزد پرست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نیست در عالم یکی مرد خدا</p></div>
<div class="m2"><p>جمله شان مفتون طبعند و هوا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یا بگوید نکته ای این ماجرا</p></div>
<div class="m2"><p>کو گزند این را دوا آن را چرا</p></div></div>