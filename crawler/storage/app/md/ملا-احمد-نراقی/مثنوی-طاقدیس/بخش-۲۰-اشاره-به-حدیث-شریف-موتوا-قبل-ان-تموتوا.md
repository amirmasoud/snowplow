---
title: >-
    بخش ۲۰ - اشاره به حدیث شریف موتوا قبل ان تموتوا
---
# بخش ۲۰ - اشاره به حدیث شریف موتوا قبل ان تموتوا

<div class="b" id="bn1"><div class="m1"><p>دل ازین دیوار خاکی کن جدا</p></div>
<div class="m2"><p>نصب کن بر طاق جان مجتبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیست کندن دل از این دیوار خاک</p></div>
<div class="m2"><p>کردنش زآلایش تن صاف و پاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی دل از آلایش تن پاک شد</p></div>
<div class="m2"><p>آدمی چون مرد و جسمش خاک شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونکه مردی دل ز تن مهجور گشت</p></div>
<div class="m2"><p>آینه دل از ره تن دور گشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تن شود کی خاک ای مرد سلیم</p></div>
<div class="m2"><p>چون نماند بهر تن امید و بیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مردن آن نبود که تن بیجان شود</p></div>
<div class="m2"><p>چارسوی این بدن ویران شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک گشتن آن نباشد ای پسر</p></div>
<div class="m2"><p>کافتد اعضایت جدا از یکدیگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن بود کاین جسم محنت کیش تو</p></div>
<div class="m2"><p>بی بها باشد چو خاکی پیش تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیده پوشی از زیان و سود آن</p></div>
<div class="m2"><p>نبودت باک از نبود و بود آن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده ی دل را نیندازی بر آن</p></div>
<div class="m2"><p>آینه ی دل را بپردازی از آن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پشت این آیینه سوی تن کنی</p></div>
<div class="m2"><p>روی آن بر وادی ایمن کنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل ذبیح آسا ازین تن برکنی</p></div>
<div class="m2"><p>دیده بر رخسار جانان افکنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرچه آن از جنس کالای تن است</p></div>
<div class="m2"><p>می نبینی گرچه فرزند و زنست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرده باشی نی زجان بلکه از بدن</p></div>
<div class="m2"><p>زنده باشی لیک از جان نی ز تن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل بود ایستاده اندر راه حسن</p></div>
<div class="m2"><p>هم از آن ره بگذرد اسپاه حسن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خانه ی جسمانیانش جا بود</p></div>
<div class="m2"><p>همنشین با زشت و با زیبا بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>می نیفتد در دلت لیک ای رفیق</p></div>
<div class="m2"><p>هیچ عکسی رهروان این طریق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگذرد صد کاروان زین رهگذر</p></div>
<div class="m2"><p>می نگردد از یکی دل باخبر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگذرد صد پادشه با کوکبه</p></div>
<div class="m2"><p>درنیابد گوش دل یک همهمه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غرق گردد گر به توفان بحر و بر</p></div>
<div class="m2"><p>پای دل از آن نگردد هیچ تر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آتش افتد گر به عالم سربسر</p></div>
<div class="m2"><p>درنیابد دل ز گرمی هیچ اثر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تن نشسته در میان خانمان</p></div>
<div class="m2"><p>آستین افشانده دل از این و آن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تن روان در کوچه و بازارها</p></div>
<div class="m2"><p>دل ولی فارغ از این پندارها</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دل بود خالی ولیکن از فتن</p></div>
<div class="m2"><p>خلوتی دارد ولی در انجمن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرده باشد لیک از میل و هوا</p></div>
<div class="m2"><p>زنده باشد لیک در ملک صفا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فارغ از این قوم و کارو بارشان</p></div>
<div class="m2"><p>ایمن از وسواس و از پندارشان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>می نبیند آنچه بینند این گروه</p></div>
<div class="m2"><p>گوش او می نشنود مایسمعوه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کور باشد لیک از نادیدنی</p></div>
<div class="m2"><p>کر بود اما زنابشنیدنی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لیک این نادیدنش نه از کوری است</p></div>
<div class="m2"><p>ناشنیدن نه از کری یا دوری است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر نیابد سرد و گرم روزگار</p></div>
<div class="m2"><p>آن نه از نادانیست و اضطرار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از زن و فرزند اگر عاری بود</p></div>
<div class="m2"><p>نی زبی دردی و بیعاری بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بلکه جانش را هوای دیگر است</p></div>
<div class="m2"><p>روی دل او را بجای دیگر است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تن در این ویرانه ده با عمرو و زید</p></div>
<div class="m2"><p>دل به ملک جان به جولان و طرید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شعله ای از سینه اش سر بر زده</p></div>
<div class="m2"><p>آتش اندر جمله خشک و تر زده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آتشی اندر دلش افروخته</p></div>
<div class="m2"><p>کانچه آنجا اندر آید سوخته</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گشته پیدا در دلش بحر عمیق</p></div>
<div class="m2"><p>کاندر آن گردیده بحر و بر غریق</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جرعه ای نوشیده و مستوست از آن</p></div>
<div class="m2"><p>بیخبر از پا و از دستوست از آن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>باده ای نوشیده از جام الست</p></div>
<div class="m2"><p>تا ابد از شور آن افتاده مست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آفتابی بر دلش افکنده نور</p></div>
<div class="m2"><p>نیست با آن نور دیگر را ظهور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آری از مشرق چو خورسر بر زند</p></div>
<div class="m2"><p>بارگه بر عرصه خاور زند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اختوران از دیده ها پنهان شوند</p></div>
<div class="m2"><p>همچو یوسف در چه کنعان شوند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خاصه خورشیدی که صد چون آفتاب</p></div>
<div class="m2"><p>پیش آن یک ذره ناید در حساب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خاصه خورشیدی که خورشید جهان</p></div>
<div class="m2"><p>سایه ی سیصد هزارم هست از آن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خاصه خورشیدی که گر گردد عیان</p></div>
<div class="m2"><p>عرش و کرسی باشد و هفت آسمان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خاصه خورشیدی که چون سازد ظهور</p></div>
<div class="m2"><p>نی گذارد موسی و نی کوه طور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از تجلی نحو طور بارقا</p></div>
<div class="m2"><p>صار دکا خر موسی صاعقا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>این چه خواهش بود ای موسی دگر</p></div>
<div class="m2"><p>طور را کردی چرا زیر و زبر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>این چه آتش بد که باز افروختی</p></div>
<div class="m2"><p>جمله اسرائیلیان را سوختی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>موسیا این نور مطلق از کجاست</p></div>
<div class="m2"><p>خیره از آن دیده ی عالم چراست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>انس و جن و عقل و روح و جسم و جان</p></div>
<div class="m2"><p>هم ملایک هم زمین و آسمان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>جملگی در پیش این رخشنده نور</p></div>
<div class="m2"><p>مشت خفاشند و عاجز از ظهور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کور کردی جمله را این نور چیست</p></div>
<div class="m2"><p>آفتابی در شب دیجور چیست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بنگری گر یک نظر در آفتاب</p></div>
<div class="m2"><p>بینی اندر دیده ی خود اضطراب</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>باشد اندر پیش چشمت روزگار</p></div>
<div class="m2"><p>تا زمانی تیره و تاریک و تار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>آفتابی را که چندین آفت است</p></div>
<div class="m2"><p>سال و ماه و روز و شب در نکبت است</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گه حضیض و گه هبوط و گه وبال</p></div>
<div class="m2"><p>گه کسوف و گه افول و گه زوال</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>این بود تأثیرش این مرد گزین</p></div>
<div class="m2"><p>تار سازد دیده ات از آن و این</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چون بود پس نور خورشید ازل</p></div>
<div class="m2"><p>نور صاف و خالی از هر غش و غل</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نور مطلق را چه باشد پس اثر</p></div>
<div class="m2"><p>کی دهد ره دیگری را در نظر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دیده ای کان بیند آن خورشید را</p></div>
<div class="m2"><p>کی ببیند تیر یا ناهید را</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هر دلی کان مشرق این نور شد</p></div>
<div class="m2"><p>فارغ از خود همچو کوه طور شد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>در دل هرکس تجلی می کند</p></div>
<div class="m2"><p>دل ز هر سوداش خالی می کند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هیچکس را با دل او کار نیست</p></div>
<div class="m2"><p>غیر یک کس را در آنجا بار نیست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>وه چه کس آن کس که جزاونیست کس</p></div>
<div class="m2"><p>هر وجودی از وجودش مقتبس</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ای خنک آن دل که در وی جای اوست</p></div>
<div class="m2"><p>حبذا آن سر که پر سودای اوست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>آن دلی کانماه را منزل بود</p></div>
<div class="m2"><p>من فدایش کان همایون دل بود</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دل فدای نام او و یاد او</p></div>
<div class="m2"><p>کشته ی بیداد او و داد او</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دل که یاد دوست باشد در وطن</p></div>
<div class="m2"><p>آب حیوانیست در ظلمات تن</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چیست دانی آب حیوان ای کیا</p></div>
<div class="m2"><p>آنکه سازد زنده ی سرمد تورا</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ای برادر یاد او را پیشه کن</p></div>
<div class="m2"><p>دل رها از چنگ هر اندیشه کن</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بند بردار ای پسر از پای دل</p></div>
<div class="m2"><p>پس ببین جولان روح افزای دل</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>دل به پرداز از هوای این و آن</p></div>
<div class="m2"><p>بر در دل روز و شب شو پاسبان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>تا در آن جز یاد جانان ای پسر</p></div>
<div class="m2"><p>ره نیاید هیچ سودایی دگر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>جان ز جانان می پذیرد ای شکوه</p></div>
<div class="m2"><p>یا احبائی الیک حر کوه</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>حرکوها الروح الی صوب الحبیب</p></div>
<div class="m2"><p>انه فی هذه الدنیا غریب</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>جان در این ویرانه ده بیگانه است</p></div>
<div class="m2"><p>در دیار قدس او را خانه است</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>جان در اقلیم صفا دارد وطن</p></div>
<div class="m2"><p>نی به شام و مصر و بغداد و یمن</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>این جهان زندان جانست ای پسر</p></div>
<div class="m2"><p>میل آن را سوی آن کشور نگر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>میل او بر آب صاف و سبزه زار</p></div>
<div class="m2"><p>وان شگفتیهای ایام بهار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>آن سرود و بهجت و نور و ضیاء</p></div>
<div class="m2"><p>وان نشاط و وجد از عود و نوا</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>جمله تأثیر دیار جان بود</p></div>
<div class="m2"><p>جان زهجرش روز و شب نالان بود</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>جان محبوس اندرین زندان تار</p></div>
<div class="m2"><p>روز و شب نالد ز هجران بهار</p></div></div>