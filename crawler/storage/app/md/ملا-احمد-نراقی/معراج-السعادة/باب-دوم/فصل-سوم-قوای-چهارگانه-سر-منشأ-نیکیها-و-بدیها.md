---
title: >-
    فصل سوم - قوای چهارگانه سر منشأ نیکیها و بدیها
---
# فصل سوم - قوای چهارگانه سر منشأ نیکیها و بدیها

<div class="n" id="bn1"><p>از آنچه گذشت روشن شد که از برای انسان اگر چه قوی و جوارح بسیار است، و لیکن همه آنها به غیر از چهار قهرمان، مطیع و فرمانبردارند، و ایشان را مدخلیتی در تغییر و تبدیل احوال مملکت نفس نیست، و آنچه منشأ اثر و باعث نیک و بد صفات، و خیر و شر ملکات هستند از این چهار قوه اند پس همه اخلاق نیک و بد از این چهار پدید آید، و منشأ صفات خیر و شر اینها هستند.</p></div>
<div class="n" id="bn2"><p>و لیکن، خیرات و نیکیهای قوه عاقله در حال تسلط و غلبه آن است، و بدیها و شرور آن در حالت زبونی و عجز آن در تحت سایر قوا، و آن سه قوه دیگر بر عکس این‌اند، زیرا که آثار خیر و نیکوئیهای آنها در حالت ذلت و انکسار و عجز ایشان در نزد عقل است، و شرور و آفات ایشان در وقت غلبه و استیلای آنها است بر عقل.</p></div>