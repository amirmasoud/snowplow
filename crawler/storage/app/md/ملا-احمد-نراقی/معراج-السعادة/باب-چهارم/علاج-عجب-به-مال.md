---
title: >-
    علاج عجب به مال
---
# علاج عجب به مال

<div class="n" id="bn1"><p>و اما عجب به مال: پس علاج آن این است که آفات مال را به نظر درآورد و تفکر کند که آن در معرض زوال و فنا، و عاری از دوام و بقاست گاهی نهب و غارت می شود، و زمانی به ظلم و ستم می رود، به آتش می سوزد، و به آب غرق می شود، دزد آن را می برد و طرار آن را می رباید، و غیر اینها از آفات سماویه و ارضیه و متذکر شود که در بین یهود و هندو کسانی هستند که مال و ثروتشان أفزونتر از ایشان است و اف بر شرفی که هندو و یهود در آن پیش باشد و تف بر بزرگی ای که دزد آن را در یک لحظه برباید و صاحبش را مفلس و ذلیل بنشاند.</p></div>
<div class="b" id="bn2"><div class="m1"><p>بسی کم است ز گاو و خر، آنکه در عالم</p></div>
<div class="m2"><p>زیادتی بود از دیگران به گاو و خرش</p></div></div>
<div class="n" id="bn3"><p>علاوه بر این، ملاحظه کند آیات و اخباری که در مذمت مال و حقارت مالداران رسیده و آنچه در فضیلت فقرا و شرافت فقر وعزت ایشان در روز قیامت و سبقتشان به بهشت وارد شده چگونه عاقل دیندار به مال شاد و فرحناک و خوشحال می گردد، و به آن عجب می کند با وجود اینکه حقوق بسیار از جانب پروردگار به آن تعلق می گیرد و از عهده همه آنها برآمدن در نهایت صعوبت و اشکال، و در حلال آن پستی مرتبه در روز قیامت و طول حساب، و در حرام آن مواخذه و عقاب است بلکه سزاوار مالداران آن است که ساعتی از خوف و اندوه، و از تقصیر مالیه و راه دخل و خرج آن خالی نباشد و حال اینکه قیمت مرد به کمال و هنر است نه به سیم و زر، بزرگی و شرف در بندگی خداوند اکبر است نه به گاو و خر.</p></div>
<div class="b" id="bn4"><div class="m1"><p>قلندران حقیقت به نیم جو نخرند</p></div>
<div class="m2"><p>قبای اطلس آن کس که از هنر عاری است</p></div></div>