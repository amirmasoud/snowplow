---
title: >-
    مقام چهارم
---
# مقام چهارم

<div class="n" id="bn1"><p>و دانستی که حد اعتدال این قوه، صفت عفت است، که منشأ جمیع صفات کمالیه متعلقه به این قوه است و دو طرف افراط و تفریط آن یکی شره است و دیگری خمود .</p></div>
<div class="n" id="bn2"><p>و اینها جنس جمیع رذایل متعلقه به این قوه، و منشأ و مصدر همه آنها هستند و ما اول بیان این دو جنس و ضد اینها را که عفت است می کنیم و بعد از آن به شرح صفاتی که در ضمن آنها مندرج اند می پردازیم.</p></div>
<div class="n" id="bn3"><p>پس در این مقام دو مطلب است: مطلب اول دو جنس صفات خبیثه متعلقه به قوه شهویه، و ضد آنها و در آن سه فصل است.</p></div>