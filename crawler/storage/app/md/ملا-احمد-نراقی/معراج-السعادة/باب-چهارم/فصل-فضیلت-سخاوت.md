---
title: >-
    فصل - فضیلت سخاوت
---
# فصل - فضیلت سخاوت

<div class="n" id="bn1"><p>ضد صفت بخل، «سخاوت» است و آن از ثمره زهد و بی مبالاتی به دنیا است و مشهورترین صفات پیغمبران خدا و معروف ترین اخلاق اصفیا و اولیاء است از معالی اخلاق، و صاحب آن پسندیده اهل آفاق است.</p></div>
<div class="n" id="bn2"><p>چنان که حضرت امیرالمومنین علیه السلام فرمود که «من جاد ساد» یعنی «هر که جود ورزید بزرگ گردید».</p></div>
<div class="b" id="bn3"><div class="m1"><p>فریدون فرخ فرشته نبود</p></div>
<div class="m2"><p>ز مشک و ز عنبر سرشته نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به داد و دهش یافت آن نیکوئی</p></div>
<div class="m2"><p>تو داد و دهش کن فریدون توئی</p></div></div>
<div class="n" id="bn5"><p>از حضرت پیغمبر صلی الله علیه و آله و سلم مروی است که «سخاوت درختی است از درختهای بهشت، که شاخهای خود را بر زمین آویخته است پس هر که یکی از آن شاخها را بگیرد، او را به بهشت می کشد» و فرمود که «سخی به خدا نزدیک، و به دلهای مردم نزدیک، و به بهشت نزدیک، و از آتش جهنم دور است» و فرمود که «خداوند عالم، مباهات می کند ملائکه را به کسی که اطعام مردمان کند» و فرمود: «خدا را بندگانی چند است که نعمت خود را مخصوص ایشان می گرداند، تا نفع به بندگان خدا رسانند پس هر کدام از ایشان که بخل نمایند در این منافع، خدا نعمت را از او به دیگری نقل می کند».</p></div>
<div class="b" id="bn6"><div class="m1"><p>تو با خلق نیکی کن ای نیکبخت</p></div>
<div class="m2"><p>که فردا نگیرد خدا بر تو سخت</p></div></div>
<div class="n" id="bn7"><p>و فرمود که «بهشت خانه اهل سخاوت است» و فرمود: «جوان سخی گناهکار، در نزد خدا محبوبتر است از پیر عابد بخیل» و از آن حضرت مروی است که «سخی را اهل آسمان ها دوست می دارند و اهل زمینها دوست دارند و طینت او از خاک پاک سرشته شده و آب چشم او از آب کوثر خلق شده و بخیل را اهل آسمانها و زمین ها دشمن دارند و خلقت او از خاک کثیف چرک آلود خلق شده و آب چشم او از آب عوسج مخلوق شده» «جمعی از اهل یمن بر حضرت فخر ذوالمنن وارد شدند و در میان ایشان مردی بود که بسیار سخن آور و حراف، و در گفتگو از همه عظیم تر، و مبالغه او در مباحثه با جناب پیغمبر صلی الله علیه و آله و سلم و حجت گرفتن بر آن سرور از همه بیشتر بود و به حدی مبالغه نمود که آن حضرت خشمناک گردید و رنگ مبارکش متغیر گشت و رگ پیشانی منورش پیچیده شد و چشم بر زمین انداخت، که جبرئیل آمد و گفت: خدایت سلام می رساند و می گوید که این مرد از اهل سخاوت، و نان ده است پس خشم آن حضرت فرونشست و سر بالا کرد و فرمود که: اگر نه این بود که مرا جبرئیل خبر داد که تو سخی نان دهی، ترا از خود می راندم و عبرت دیگران می کردم آن مرد گفت که خدای تو سخاوت را دوست دارد؟ فرمود: بلی آن مرد گفت: «اشهد ان لا اله الا الله و اشهد انک لرسول الله» به خدائی که تو را به حق برانگیخته است که هرگز احدی را از مال خود محروم برنگردانیدم مروی است که «چون حضرت موسی علیه السلام بر سامری دست یافت، خطاب عزت رسید که او را مکش، زیرا که او سخی است» و بالجمله فضیلت سخا، خود ظاهر و روشن، و صاحب آن در نزد خالق و خلق محبوب و مستحسن، و در دنیا در اکرام و اعزاز، و در عقبی سرافراز است و کدام عاقل، سرافرازی دو جهان را از دست می دهد و جمادی چند بر روی هم می نهد؟</p></div>
<div class="b" id="bn8"><div class="m1"><p>بیا تا خوریم آنچه داریم شاد</p></div>
<div class="m2"><p>درم بر درم چند باید نهاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درین باغ رنگین درختی نرست</p></div>
<div class="m2"><p>که ماند از قفای تبر زین درست</p></div></div>