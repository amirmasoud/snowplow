---
title: >-
    معالجه عملی حب جاه
---
# معالجه عملی حب جاه

<div class="n" id="bn1"><p>و اما معالجه عملی حب جاه، آن است که گمنامی و گوشه نشینی را اختیار کنی و از مواضعی که در آنجا مشهور هستی، و أهالی آن در صدد احترام تو هستند مسافرت و هجرت کنی و به مواضعی که در آنجا گمنام باشی مسکن نمایی و مجرد گوشه نشینی در خانه خود در آن شهری که مشهوری فایده نمی بخشد، بلکه غالب آن است که قبول عامه و حصول جاه از آن بیشتر حاصل شود پس بسا کسان که در شهر خود در خانه نشسته و در بر روی خود بسته و از مردم کناره کرده و به این سبب میل دلها به ایشان بیشتر و آن بیچاره این عمل را وسیله تحصیل جاه قرار داده و چنین می داند که ترک دنیا کرده هیهات، هیهات، فریب شیطان را خورده نظر به قلب خود افکند که اگر اعتقاد مردم از او زایل شود و در مقام مذمت و بدگویی برآیند چگونه دل او متألم می گردد و نفس او مضطرب می شود و در صدد چاره جویی او برمی آید، بداند که: حب جاه او را بر گوشه نشینی واداشته.</p></div>
<div class="n" id="bn2"><p>و عمده در علاج این صفت، قطع طمع کردن است از مردم و این حاصل نمی شود مگر به قناعت، زیرا هر که قناعت را پیشه خود کرد از مردم مستغنی می شود و چون از ایشان مستغنی شد دل او از ایشان فارغ می گردد و رد و قبول مردم در نظر او یکسان می نماید بلکه هر که از اهل معرفت باشد و او را طمعی به کسی نباشد مردم در نظر او چون چهارپایان می نمایند.</p></div>
<div class="n" id="bn3"><p>و از جمله معالجات عملیه حب جاه آنکه از چیزی که باعث زیادتی حب جاه و حرمت تو باشد احتراز کنی و أموری را که موجب سقوط وقع تو باشد مرتکب گردی، مادامی که منجر به خلاف شرعی نشود و بسیار در أخبار و آثاری که در مذمت جاه رسیده تتبع نمایی و فواید ضد آن را که گمنامی و خمول است به نظر درآوری.</p></div>