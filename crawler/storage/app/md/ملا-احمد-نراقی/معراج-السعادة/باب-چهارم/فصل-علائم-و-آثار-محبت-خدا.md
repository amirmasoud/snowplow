---
title: >-
    فصل - علائم و آثار محبت خدا
---
# فصل - علائم و آثار محبت خدا

<div class="n" id="bn1"><p>بدان که هر کسی چنان پندارد که او خدا را دوست دارد، بلکه گاه است گمان کند که او را از همه چیز دوست تر دارد و این مجرد پندار و محض غرور است و فی الجمله دوستی هم اگر باشد سزاوار نام محبت نیست و محبت بنده از برای خداوند متعال علاماتی چند دارد:</p></div>
<div class="n" id="bn2"><p>اول: آنکه طالب لقاء و شایق مشاهده و عیان بوده باشد و چون وصول به آن بر مردن موقوف است مشتاق مرگ باشد، و مرگ بر او گوارا باشد، و اصلا مرگ بر او گران نباشد و چگونه بر دوست حقیقی ناگوار است که به کوی دوست خود مسافرت نماید که به وصال او برسد و از این جهت بعضی از اکابر دین گفته است:</p></div>
<div class="b" id="bn3"><div class="m1"><p>«لا یکره الموت الا مریب</p></div>
<div class="m2"><p>لان الحبیب لا یکره لقاء الحبیب»</p></div></div>
<div class="n" id="bn4"><p>یعنی مرگ را کراهت نمی دارد مگر کسی که دوستی او مجرد دعوی باشد، زیرا دوست ملاقات دوست را مکروه نمی دارد.</p></div>
<div class="n" id="bn5"><p>و مخفی نماند که اگر چه شوق لقای خداوند و رغبت به انتقال از دار دنیا غالبا ثمره محبت و علامت آن است و لیکن این کلی نیست، چون می تواند شد که کسی از اهل محبت نباشد و شایق به مرگ باشد مثل کسی که در دنیا به شدت و بلای عظیم گرفتار باشد و به گمان خلاصی از آن تعبها، موت را آرزو کند و بسا باشد که کسی از جمله دوستان خدا باشد و مرگ را کراهت داشته باشد و لیکن کراهت او از مرگ به جهت زیاد کردن استعداد ملاقات خدا و تهیه اسباب لقاء است، مانند کسی که خبر آمدن محبوب او به خانه او به او برسد و او خواسته باشد که ساعتی آمدن تأخیر افتد تا خانه خود را پاکیزه کند و فرش نماید و از اغیار خالی سازد و اسباب لازمه را آماده کند تا فارغ البال و مطمئن خاطر از گلشن لقای او تمتع برد و این شخص اگر نه به جهت تهیه لقای الهی باشد ساعتی نمی خواهد در دنیا بوده باشد و علامت این سعی در عمل و صرف جمیع اوقات در کار آخرت است.</p></div>
<div class="n" id="bn6"><p>و اما کسی که کراهت او از مرگ به جهت ناگوار بودن ترک اهل و اولاد و اموال و بازماندن از شهوات دنیویه باشد مطلقا در دل او از یاد مرگ فرحی هم نرسد و چنین شخصی از مرتبه دوستی خدا دور است.</p></div>
<div class="n" id="bn7"><p>بلی: گاه است که کسی فی الجمله شوق به لقای الهی داشته باشد و با وجود آن دنیا را نیز دوست داشته باشد چنین کسی از کمال محبت پروردگار خالی است و کراهت او از موت هم کمتر از سابق است.</p></div>
<div class="n" id="bn8"><p>دوم آنکه طالب رضای خدا باشد و خواهش او را بر خواهش خود اختیار کند، زیرا دوست صادق هوای خود را فدای هوای محبوب می گرداند چنان که گفته اند:</p></div>
<div class="b" id="bn9"><div class="m1"><p>ارید وصاله و یرید هجری</p></div>
<div class="m2"><p>فأترک ما ارید لما یرید</p></div></div>
<div class="n" id="bn10"><p>یعنی: من خواهش وصال محبوب را دارم و او طالب دوری از من است، پس من خواهش او را برگزیدم و از سر مراد خود گذشتم تا او به مراد خود رسد.</p></div>
<div class="n" id="bn11"><p>پس هر که دوست خدا باشد باید ترک معاصی نموده ملازم طاعت و عبادت گردد و کسالت و بطالت را بگذار و هیچ عبادتی بر او گران نیاید.</p></div>
<div class="n" id="bn12"><p>منقول است که «چون زلیخا ایمان آورد و یوسف او را به نکاح درآورد از یوسف کناره گرفت و به عبادت الهی مشغول شد و چون روزی یوسف او را به خلوت دعوت کردی او وعده شب دادی و چون شب درآمد به روز تأخیر افکندی یوسف با او به عتاب آمد و گفت: چه شد آن دوستیها و شوق و محبت تو؟ زلیخا گفت: ای پیغمبر خدا من تو را وقتی دوست داشتم که خدای تو را نشناخته بودم، اما چون او را شناختم همه محبتها را از دل خود بیرون کردم و دیگری را بر او اختیار نمی کنم» و حق آن است که مرتکب بعضی از معاصی شدن با اصل محبت خدا منافاتی ندارد، گویا با کمال محبت منافی باشد، چنانچه مریض غذایی که از برای او ضرر دارد می خورد و از آن متضرر می شود، با وجود آنکه خود را دوست دارد و صحت خود را می خواهد و سبب این، غلبه خواهش نفس است بر محبت و ضعف محبت در جنب آن.</p></div>
<div class="n" id="bn13"><p>سوم: آنکه پیوسته در یاد خدا و دایم دل او به یاد خدا مشغول باشد، زیرا دل که مایل به چیزی باشد هرگز از یاد او نمی رود و چون محبت، به سر حد کمال رسید بجز محبوب، هیچ چیز به خاطر او نمی گذرد، بلکه از فکر خود ذاهل، و از اهل و عیال غافل می شود.</p></div>
<div class="n" id="bn14"><p>چهارم: آنکه همیشه زبان او به ذکر خدا مشغول باشد و بجز حدیث او چیزی نشنود و نگوید و به غیر از نام او نخواند و نجوید و در هر مجلسی به ذکر خدا و رسول او و تلاوت قرآن و بیان احادیث زبان گشاید.</p></div>
<div class="n" id="bn15"><p>پنجم: آنکه شوق به خلوات داشته باشد که مناجات با خدای خود کند گاهی او را یاد کند و زمانی با او راز گوید، ساعتی عذر تقصیرات خود خواهد، و لحظه ای عرض آرزوهای خود کند و گوید:</p></div>
<div class="b" id="bn16"><div class="m1"><p>گریزانم از آن از انجمن ها</p></div>
<div class="m2"><p>که در دل خلوتی با یار دارم</p></div></div>
<div class="n" id="bn17"><p>در اخبار آمده که «دروغ می گوید هر که دعوی محبت مرا کند و چون ظلمت شب او را فرو گیرد بخوابد و از یاد من غافل شود آیا هر دوستی لقای محبوب خود را دوست نمی دارد؟ و من اینک حاضرم از برای هر که طالب من باشد» بلی:</p></div>
<div class="b" id="bn18"><div class="m1"><p>عجبا للمحب کیف ینام</p></div>
<div class="m2"><p>کل نوم علی المحب حرام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خواب بر عاشقان حرام بود</p></div>
<div class="m2"><p>خواب آن کس کند که خام بود</p></div></div>
<div class="n" id="bn20"><p>ششم آنکه بر هیچ چیز از امور دنیویه که از دست او در رود تأسف نخورد و متألم و محزون نگردد و اگر دنیا و ما فیها از او باشد و به یک بار بر باد رود اندوهناک نشود.</p></div>
<div class="n" id="bn21"><p>و در مصایب و بلاها جزع ننماید و اگر همه دنیا به او رو آورد شاد نگردد، بلکه شادمانی و فرح او به محبوب خود باشد و بس.</p></div>
<div class="b" id="bn22"><div class="m1"><p>عاشقان را شادمانی و غم اوست</p></div>
<div class="m2"><p>دست مزد و اجرت خدمت هم اوست</p></div></div>
<div class="n" id="bn23"><p>و اگر کسی را محبت به سر حد کمال رسد دیگر از هیچ امری محزون و متألم نمی گردد، زیرا شادی او به چیزی است که زوال ندارد پس دل او همیشه فرحناک است.</p></div>
<div class="b" id="bn24"><div class="m1"><p>نمیرد دل زنده ازعشق دوست</p></div>
<div class="m2"><p>که آب حیات آتش عشق اوست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عجب ملکی است ملک عشق کآنجا</p></div>
<div class="m2"><p>سراسر کوه و صحرا گلستان است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یکی را جان در آن در آستین است</p></div>
<div class="m2"><p>یکی را سر در آن بر آستان است</p></div></div>
<div class="n" id="bn27"><p>هفتم آنکه بر همه بندگان خدا مشفق و مهربان باشد، زیرا مقتضای محبت، دوست داشتن دوستان و منسوبان محبوب است.</p></div>
<div class="n" id="bn28"><p>و مشهور است که مجنون، سگی را در کوی لیلی دید بر دور او می گردید و به او عشق می ورزید بلی: دوستی چنین است و باید کسی که دشمن خدا یا دشمن دوستان خدا باشد بغض و عداوت او را داشته باشد.</p></div>
<div class="n" id="bn29"><p>هشتم آنکه از هیبت و عظمت الهی خایف باشد و در زیر لوای کبریا و جلال، متذلل و مضطرب بوده باشد همچنان که معرفت جمال الهی موجب محبت می گردد، درک عظمت و جلال او نیز باعث خوف و دهشت می شود علاوه بر این، دوستان از ترسهای مخصوص دیگر نیز هست، چون ترس اعراض محب و حجاب او و راندن را درگاه و ایستادن در یک مقام.</p></div>
<div class="b" id="bn30"><div class="m1"><p>محنت قرب ز بعد افزون است</p></div>
<div class="m2"><p>جگر از محنت قربم خون است</p></div></div>
<div class="n" id="bn31"><p>نهم آنکه محبت خود را پوشیده دارد و زبان به اظهار آن نگشاید و در نزد مردمان به دعوای آن برنیاید و به کلمات داله بر محبت خدا و امثال آن اظهار وجد و نشاط نکند، زیرا اینها همه منافاتی تعظیم محبوب و مخالف هیبت و جلال اوست بلکه دوستی، سری است که میان محب و محبوب است و فاش نمودن سزاوار نیست علاوه بر این، بسا باشد در دعوی از حد واقع تجاوز کند و به دروغ و افترا افتد.</p></div>
<div class="n" id="bn32"><p>بلی کسی را که محبت به حد افراط رسیده باشد گاه باشد از شراب محبت چنان بی خود و مدهوش و مضطرب و حیران گردد که بی اختیار آثار محبت از او به ظهور رسد، چنین شخصی معذور، زیرا او در تحت سلطان محبت مقهور است و کسی که دانست معرفت و محبت خدا به نحوی که سزاوار او است از قوه بندگان خارج است و مطلع شد به اعتراف عظماء بنی نوع انسان از انبیا و اولیا به عجز و قصور، و از احوال ملائکه ملکوت و سکان قدس و جبروت آگاه گردیده و شنید که یک صنف از اصناف غیر متناهیه ملائکه صنفی هستند که عدد ایشان بقدر جمیع مخلوقاتی است که خدا آفریده و ایشان اهل محبت خدایند و سیصد هزار سال پیش از خلق عالم ایشان را خلق کرده و از آن روزی که خلق شده اند تا به حال به غیر از خداوند متعال هیچ چیز به خاطر ایشان خطور نکرده و غیر او را یاد ننموده اند، شرم می کنند و خجالت می کشند که محبت خود را معرفت و محبت نام نهند و زبان آنها از اظهار این دعوی لال می گردد.</p></div>
<div class="n" id="bn33"><p>مروی است که «یکی از اهل الله از بعضی از صدیقان استدعا نمود که از خدا مسئلت نماید که ذره ای از معرفت خود را به او عطا فرماید، چون او این مسئلت را نمود دفعه عقل او حیران، و دل او واله و سرگردان گشته سر به کوهها و بیابانها نهاده و دیوانه وار در صحراها و کوهها می گشت و هفت شبانه روز در مقامی ایستاد که نه او از چیزی منتفع شد و نه چیزی از او پس آن صدیق از خدا سوال نمود که قدری از آن ذره معرفت را که به او عطا نموده کم کند به او وحی شد که در این وقت صدهزار بنده چیزی از محبت ما را مسئلت نمودند ما یک ذره معرفت خود را میان ایشان قسمت فرمودیم و هر یک یک جزو از صدهزار جزو یک ذره معرفت دادیم و نیز به این بنده عطا فرمودیم به این حال شد پس آن صدیق عرض کرد: «سبحانک سبحانک» آنچه را به او عطا کرده ای کم کن پس خدا آن جزو معرفت را به هزار قسم کرد و یک قسم آن را باقی گذارد و تتمه را سلب نمود در آن وقت مثل یکی از کاملین ارباب معرفت گردید» بلی این ظاهر است که حقایق صفات الهیه از آن برتر که عقول بشر درک آنها را تواند کرد و احدی از کمل عارفین را طاقت آن نیست که یک جزو از اجزای غیرمتناهیه معرفت او را تحمل نماید پس وصول به کنه عظمت و جلال حضرت ربوبیت و سایر صفات کمال او محال، و آنچه در حق او می گویند یا وهم است یا خیال اگر تواند شد که چندین مقابل جمیع مخلوقات از آسمان ها و زمین ها و آنچه ما فوق آنهاست به قدر غیرمتناهی در میان یک حبه خردل جا کرد ممکن است که یک ذره از معرفت خدا در اعظم عقول بشر بگنجد: «فسبحان من لا سبیل إلی معرفته الا بالعجز عن معرفته».</p></div>
<div class="n" id="bn34"><p>و از جمله علامات محبت، انس و رضایت است و بعضی از عارفین، علامات محبت را در ابیاتی چند ذکر کرده و گفته است:</p></div>
<div class="b" id="bn35"><div class="m1"><p>لا تخد عن فلمحب دلائل</p></div>
<div class="m2"><p>و لدیه من تحف الحبیب وسائل</p></div></div>
<div class="n" id="bn36"><p>یعنی خدعه مکن در دعوای محبت که دوست را نشانهای چند است و تحفه هایی است که از دست دوست به او رسیده است.</p></div>
<div class="b" id="bn37"><div class="m1"><p>منها تنعمه بمر بلائه</p></div>
<div class="m2"><p>و سروره فی کل ما هو فاعل</p></div></div>
<div class="n" id="bn38"><p>از جمله آنکه تلخی بلاهای دوست در کام جان او شیرین گردد و به آنچه به او کند شاد و فرحناک شود.</p></div>
<div class="b" id="bn39"><div class="m1"><p>از محبت تلخها شیرین شود</p></div>
<div class="m2"><p>وز محبت مسها زرین شود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فالمنع منه عطیه مبذوله</p></div>
<div class="m2"><p>و الفقر اکرام و لطف عاجل</p></div></div>
<div class="n" id="bn41"><p>اگر منع کند محبت خود را از نعمتهای دنیویه، آن را عطا و بخشش داند و اگر او را به فقر مبتلا سازد آن را مکرمتی شمارد.</p></div>
<div class="b" id="bn42"><div class="m1"><p>و من الدلائل ان یری من عزمه</p></div>
<div class="m2"><p>طوع الحبیب و ان الح العاذل</p></div></div>
<div class="n" id="bn43"><p>و از علامات آن است که سر بر خط فرمان دوست باشد و در هوای او کوشد و از پند و ملامت نیندیشد و گوید:</p></div>
<div class="b" id="bn44"><div class="m1"><p>از تو ای دوست نگسلم پیوند</p></div>
<div class="m2"><p>گر به تیغم برند بند از بند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پند آنان دهند خلق ای کاش</p></div>
<div class="m2"><p>که ز عشق تو می دهندم پند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>و من الدلائل ان یری متبسما</p></div>
<div class="m2"><p>و القلب فیه من الحبیب بلابل</p></div></div>
<div class="n" id="bn47"><p>و از جمله علامات آن است که چهره او خندان باشد و دل او از فراق محبوب غمناک و اندوهناک بوده باشد.</p></div>
<div class="b" id="bn48"><div class="m1"><p>و من الدلائل ان یراء مشمرا</p></div>
<div class="m2"><p>فی خرقتین علی شطوط الساحل</p></div></div>
<div class="n" id="bn49"><p>و از علامات دیگر آنکه دو جامه کهنه به خود پیچیده و در صحرا و کوهها و کنار دریاها گردد و از مردم فرار اختیار نماید.</p></div>
<div class="b" id="bn50"><div class="m1"><p>و من الدلائل حزنه و نحیبه</p></div>
<div class="m2"><p>جوف الظلام فما له من عاذل</p></div></div>
<div class="n" id="bn51"><p>و از جمله علامات آنکه در ظلمتهای شب پنهان از هر ملامت کننده، آه سوزناک از دل دردناک برکشد بلی:</p></div>
<div class="b" id="bn52"><div class="m1"><p>مرغ شبخوان را بشارت باد کاندر راه عشق</p></div>
<div class="m2"><p>دوست را با ناله شبهای بیداران خوش است</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>و من الدلائل ان تراه باکیا</p></div>
<div class="m2"><p>ان قد رآه علی قبیح فاعل</p></div></div>
<div class="n" id="bn54"><p>و از جمله نشانه های محبت آنکه اگر عملی که محبوب را ناخوش آید از او سرزند اشک خونین از دیده روان سازد و دست ندامت بر سر زند.</p></div>
<div class="b" id="bn55"><div class="m1"><p>و من الدلائل ان تراه راضیا</p></div>
<div class="m2"><p>بملیکه فی کل حکم نازل</p></div></div>
<div class="n" id="bn56"><p>و از علامات آنکه هر چه به او رسد از محبوب او راضی شود و هر حکمی فرماید تن در دهد.</p></div>
<div class="b" id="bn57"><div class="m1"><p>و من الدلائل زهده فیما تری</p></div>
<div class="m2"><p>من دار ذل و النعیم الزائل</p></div></div>
<div class="n" id="bn58"><p>و از علامات آنکه پشت پا بر همه نعمتهای این خانه فانی زند و با یاد او چشم از همه امور دنیویه بپوشاند.</p></div>
<div class="b" id="bn59"><div class="m1"><p>ننگرد دیگر به سرو اندر چمن</p></div>
<div class="m2"><p>هرکه دید آن سرو سیم اندام را</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>و من الدلائل ان تراه مسلما</p></div>
<div class="m2"><p>کل الامور إلی الملیک العادل</p></div></div>
<div class="n" id="bn61"><p>و از نشانه ها آنکه از برای خود در هیچ امری اختیاری نگذارد و همه امور خود را به پادشاه عادل واگذارد و گوید:</p></div>
<div class="b" id="bn62"><div class="m1"><p>مرا با وجود تو هستی نماند</p></div>
<div class="m2"><p>به یاد توام خودپرستی نماند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>و من الدلائل ان تراه مسافرا</p></div>
<div class="m2"><p>نحو الجهاد و کل فعل فاضل</p></div></div>
<div class="n" id="bn64"><p>و از جمله علامات آنکه دامن همت برمیان زند و به اعمال فاضله و افعال حسنه از جهاد و حج و سایر عبادات پردازد و محب صادق چون إبراهیم خلیل است که تن و مال و فرزندان را در راه خدا دریغ نداشته، تن را به آتش سوزان داد و در آن وقت از روح القدس مدد نخواست.</p></div>
<div class="n" id="bn65"><p>إبراهیم خلیل الرحمن و محبت و معرفت خدا مروی است که «خدای تعالی إبراهیم را مال بسیار داده چنانکه چهارصد سگ با قلاده زرین در عقب گوسفندان او بودند و فرشتگان گفتند که دوستی إبراهیم از برای خدا به جهت مال و نعمتی است که به او عطا فرموده پادشاه عالم خواست که به ایشان بنماید که نه چنین است، به جبرئیل فرمود که برو و مرا در جایی که إبراهیم بشنود یاد کن جبرئیل برفت در وقتی که إبراهیم نزد گوسفندان بود بر بالای تلی ایستاد و به آواز خوشی گفت: «سبوح قدوس رب الملائکه و الروح» چون إبراهیم نام خدای را شنید جمیع اعضای او به حرکت آمد و فریاد برآورد و به مضمون این مقال گویا شد:</p></div>
<div class="b" id="bn66"><div class="m1"><p>کاین مطرب از کجاست که برگفت نام دوست</p></div>
<div class="m2"><p>تا جان و جامه بذل کنم بر پیام دوست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دل زنده می شود به امید وفای یار</p></div>
<div class="m2"><p>جان رقص می کند ز سماع کلام دوست</p></div></div>
<div class="n" id="bn68"><p>پس إبراهیم از چپ و راست نگاه کرد شخصی را بر تلی ایستاده دید به نزد وی دوید و گفت: تو بودی که نام دوست من را بردی؟ گفت: بلی، إبراهیم گفت: ای بنده حق نام حق را یکبار دیگر بگو و ثلث گوسفندانم از تو جبرئیل باز نام حق را بگفت.</p></div>
<div class="n" id="bn69"><p>إبراهیم گفت: یکبار دیگر بگو و نصف گوسفندانم از تو جبرئیل باز نام حق را بگفت حضرت إبراهیم در آن وقت از کثرت شوق و ذوق واله و بی قرار شد گفت: همه گوسفندانم از تو یکبار دیگر نام دوست مرا بگو جبرئیل باز بگفت إبراهیم گفت: مرا دیگر چیزی نیست خود را به تو دادم یکبار دیگر بگوی جبرئیل باز گفت پس إبراهیم گفت: بیا مرا با گوسفندان من ضبط کن که از آن توست جبرئیل گفت: إبراهیم مرا حاجت به گوسفندان تو نیست، من جبرئیلم و حقا که جای آن داری که خدا تو را دوست خود گردانید، که در وفاداری، کاملی و در مرتبه دوستی، صادق و در شیوه طاعت، مخلص ثابت قدم».</p></div>
<div class="n" id="bn70"><p>و مروی است که «روزی اسمعیل از شکار باز آمده بود إبراهیم در او نظر کرد او را دید با قدی چون سرو خرامان و رخساری چون ماه تابان إبراهیم را چون مهر پدری بجنبید و در دل او اثر محبت فرزند ظاهر گردید در همان شب خواب دید که امر حق چنان است که اسمعیل را قربانی کنی إبراهیم در اندیشه شد که آیا این امری است از رحمن یا وسوسه ای است از شیطان، چون شب دیگر در خواب شد همان خواب را دید دانست که امر حق تعالی است چون روز شد به هاجر مادر اسمعیل گفت: این فرزند را جامه نیکو در پوش و گیسوان او را شانه کن که وی را به نزدیک دوست برم هاجر سرش را شانه کرد و جامه پاکیزه اش پوشانید و بوسه بر رخسار او زد، حضرت خلیل الرحمن گفت: ای هاجر کارد و «رسنی به من ده هاجر گفت: به زیارت دوست می روی، کارد و رسن را چه کنی؟ گفت: شاید که گوسفندی بیاورند که قربان کنند.</p></div>
<div class="n" id="bn71"><p>ابلیس گفت: وقت آن است که مکری سازم و خاندان نبوت را براندازم، به صورت پیری نزد هاجر رفت و گفت: آیا می دانی إبراهیم، اسمعیل را به کجا می برد؟ گفت: به زیارت دوست گفت: می برد او را بکشد گفت: کدام پدر پسر را کشته است خاصه پدری چون إبراهیم و پسری چون اسمعیل؟ ابلیس گفت: می گوید خدا فرموده است هاجر گفت: هزار جان من و اسمعیل فدای راه خدا باد کاش مرا هزار هزار فرزند بودی و همه را در راه خدا قربان کردندی ابلیس چون از هاجر مأیوس شد به نزد إبراهیم آمد و گفت: ای إبراهیم فرزند خود را مکش که این خواب شیطان است إبراهیم فرمود: ای ملعون شیطان تویی گفت: آخر دلت می دهد که فرزند خود را به دست خود بکشی؟ إبراهیم فرمود: بدان خدای که جان من در قبضه قدرت اوست که اگر مرا از شرق عالم تا غرب عالم فرزندان بودی و دوست من فرمودی که قربان کن همه را به دست خود قربان کنم چون از حضرت خلیل نیز نومید شد روی سوی اسمعیل نهاد و گفت: پدرت تو را می برد بکشد گفت: از چه سبب؟ گفت: می گوید حق فرموده است گفت: حکم حق را باید گردن نهاد اسمعیل دانست که شیطان است سنگی برگرفت و بدو افکند.</p></div>
<div class="n" id="bn72"><p>و به این جهت حاجیان را واجب شد که در آن موضع سنگریزه بیندازند.</p></div>
<div class="n" id="bn73"><p>پس چون پدر و پسر به منی رسیدند إبراهیم گفت: ای پسر «انی اری فی المنام انی اذبحک» یعنی «ای پسر در خواب دیدم که تو را قربان باید کرد» اسمعیل گفت «یا ابت افعل ما تومر» یعنی «بکن ای پدر آنچه را مأموری» اما ای پدر وصیت من به تو آن است که دست و پای من را محکم ببندی که مبادا تیزی کارد به من رسد حرکتی کنم و جامه تو خون آلود شود و چون به خانه رسی مادر مرا تسلی دهی پس إبراهیم به دل قوی دست و پای اسمعیل را محکم بست، خروش از ملائکه ملکوت برخاست که زهی بزرگوار بنده ای که وی را در آتش انداختند از جبرئیل یاری نخواست و از برای رضای خدا فرزند خود را به دست خود قربان می کند پس إبراهیم کارد بر حلق اسملعیل نهاد، هر چند قوت کرد نبرید اسمعیل گفت: ای پدر زود فرمان حق را به جای آور فرمود: چه کنم هر چند قوت می کنم نمی برد گفت: ای پدر در روی من نظر می کنی شفقت پدری نمی گذارد، روی من را بر خاک نه و کارد بر قفا گذار.</p></div>
<div class="n" id="bn74"><p>إبراهیم چنان کرد و کارد نبرید اسماعیل گفت: ای پدر سر کارد را به حلق من فرو بر که در آن وقت آواز برآمد که «یا ابراهیم قد صدقت الرویا» یعنی «ای إبراهیم خواب خود را درست کردی دست از اسماعیل بدار و این گوسفند را به جای او قربانی کن» بلی:</p></div>
<div class="b" id="bn75"><div class="m1"><p>شوریده نباشد آنکه از سر ترسد</p></div>
<div class="m2"><p>عاشق نبود آنکه ز خنجر ترسد</p></div></div>