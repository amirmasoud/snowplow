---
title: >-
    تدبر در آفرینش و صنع پروردگار
---
# تدبر در آفرینش و صنع پروردگار

<div class="n" id="bn1"><p>و چون فضیلت تفکر در آیات عالم آفاق و انفس را دانستی، بدان که از تفکر در هر موجودی از موجودات مشاهده عجائب صنع پروردگار می توان نمود و از تدبر در هر مخلوقی از مخلوقات ملاحظه غرایب قدرت آفریدگار می توان کرد، زیرا که آنچه در اقلیم وجود بجز ذات پاک آفریدگار یافت می شود رشحه ای از رشحات وجود او و قطره ای از دریای بی منتهای فیض وجود او است و از اوج عالم مجردات تا حضیض منزل مادیات را اگر سیر کنی بجز صنع او نبینی، و از کشور افلاک تا خطه خاک را اگر تفحص کنی به غیر از آثار قدرت او نیابی مجردات و مادیات از صنایع عجیبه او و «جواهر» و اعراض فلکیات و عنصریات و بسائط و مرکبات از بدایع غریبه او.</p></div>
<div class="b" id="bn2"><div class="m1"><p>ای همه هستی ز تو پیدا شده</p></div>
<div class="m2"><p>خاک ضعیف از تو توانا شده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیر نشین علمت کاینات</p></div>
<div class="m2"><p>ما به تو قائم چو تو قائم به ذات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مبدأ هر چشمه که جودیش هست</p></div>
<div class="m2"><p>مخترع هر چه وجودیش هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر سر چرخ است پر از طوق اوست</p></div>
<div class="m2"><p>ور دل خاک است پر از شوق اوست</p></div></div>
<div class="n" id="bn6"><p>لوالدی قدس سره:</p></div>
<div class="b" id="bn7"><div class="m1"><p>ای ز وجود تو وجود همه</p></div>
<div class="m2"><p>پرتوی از بود تو بود همه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست کن و هست کن هست و نیست</p></div>
<div class="m2"><p>غیر تو و صنع تو موجود نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صفحه خضرا ز تو آراسته</p></div>
<div class="m2"><p>عرصه غبرا ز تو پیراسته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غیب ازل نور شهود از تو یافت</p></div>
<div class="m2"><p>لوح عدم نقش وجود از تو یافت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روشنی لعل بدخشان ز تو</p></div>
<div class="m2"><p>پرتو خورشید درخشان ز تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نورده ظلمتیان عدم</p></div>
<div class="m2"><p>ربط ده خیل حدوث و قدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جنبشی از بحر وجودت، سپهر</p></div>
<div class="m2"><p>پرتوی از عکس رخت، ماه و مهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نقطه‌ای از دفتر صنعت فلک</p></div>
<div class="m2"><p>شحنه‌ای از عسکر ملکت ملک</p></div></div>
<div class="n" id="bn15"><p>و هیچ ذره از ذرات عالم نیست مگر اینکه از انواع عجایب حکمت و غرایب صنعت پروردگار این قدر در آن یافت می شود که اگر جمیع عقلای عالم و حکمای بنی آدم از بدو آفرینش تا قیام قیامت دامن همت بر میان بندند که ادراک آن را کنند به عشری از اعشار و اندکی از بسیار آن نتوانند رسید، چه جای این که آثار قدرت کامله را در جمیع موجودات توانند فهمید</p></div>
<div class="n" id="bn16"><p>و مخفی نماند که موجوداتی که از کتم عدم به فضای وجود آمده اند بسیاری از آنها را ما نمی شناسیم، نه مجمل آنها را می دانیم نه مفصل، نه نامی از آنها شنیده ایم و نه نشانی و دست تصرف اوهام ما از آنها کوتاه، و قدم اندیشه ما را در نزد آنها راه نیست.</p></div>
<div class="n" id="bn17"><p>پس از برای ما تفکر در آنها و ادراک عجایب و غرایب آنها ممکن نیست، بلکه تفکر و تدبر ما منحصر است به آنچه مجملا وجود آنها را دانسته ایم و اصل آنها را شناخته و آنها بر دو قسم اند:</p></div>
<div class="n" id="bn18"><p>یکی آنچه دیده نمی شود و به حس در نمی آید و آن را «عالم ملکوت» گویند، مانند عالم عقول و نفوس مجرده و ملائکه و جن و شیاطین و از برای آنها انواع و طبقات بسیاری که بجز خالق آنها احاطه به آنها نتواند کرد.</p></div>
<div class="n" id="bn19"><p>و دیگری آنچه محسوس میشود و مشاهده میگردد و از برای آنها سه طبقه است:</p></div>
<div class="n" id="bn20"><p>یکی آنکه از عالم افلاک مشاهده میشود از ثوابت و سیارات و گردش آنها در لیل و نهار.</p></div>
<div class="n" id="bn21"><p>دوم: خطه خاک محسوس با آنچه در آن هست از بلندی و پستی و کوه و دریا و بیابان و صحرا و شطوط و انهار و معادن و اشجار و نباتات و حیوانات و جمادات.</p></div>
<div class="n" id="bn22"><p>سیم: عالم هوا با آنچه در آن مشاهده میشود از رعد و برق و برف و باران و ابر و صاعقه و امثال اینها و هر یک از این طبقات را انواع متکثره و هر نوعی را اقسام و اصناف غیر متناهیه است که هر یکی را صنعتی و هیئتی و اثر و خاصیتی و ظاهری و باطنی و حرکت و سکونی و حکمت و مصلحتی است که بجز خداوند دانا نتواند ادراک نمود.</p></div>
<div class="n" id="bn23"><p>و هر یک از اینها را که دست زنی محل تفکر و عبرت، و باعث بصیرت و معرفت می گردد، زیرا که همگی آنها گواهان عدل و شهود صدق اند بر وحدانیت خالق آنها و حکمت او کمال او قدرت و عظمت او.</p></div>
<div class="b" id="bn24"><div class="m1"><p>برگ درختان سبز در نظر هوشیار</p></div>
<div class="m2"><p>هر ورقش دفتری است معرفت کردگار</p></div></div>
<div class="n" id="bn25"><p>پس هر که دیده بصیرت بگشاید و به قدم حقیقت گرد سراپای عالم وجود برآید و مملکت خداوند و دود را سیر کند در هر ذره از ذرات مخلوقات، عجایب حکمت و آثار قدرت، این قدر مشاهده می کند که فهم او حیران و عقل او واله و سرگردان می گردد.</p></div>
<div class="n" id="bn26"><p>و شبهه ای نیست در اینکه طبقات عوالم پروردگار در شرافت و وسعت متفاوت اند و هر عالم پستی را نسبت به ما فوق آن قدر محسوسی نیست پس عالم خاک را که پست ترین عالم خداوند پاک است قدری نیست در نزد عالم هوا، همچنان که عالم هوا را مقداری نیست به قیاس عالم سموات، و عالم سموات را نسبت به عالم مثال، و عالم مثال را نظر به عالم ملکوت، و عالم ملکوت را نظر به عالم جبروت، و تمام اینها را نسبت به آنچه ما را راهی به ادراک آن نیست از عوالم الهیه و آنچه از مخلوقات را که بر روی پست ترین همه این عوالم است که زمین باشد از حیوانات و نباتات و جمادات قدر محسوسی نسبت به اهل زمین نیست و از برای هر یکی از این اجناس ثلاثه انواع و اقسام و اصناف و افراد بی نهایت است و هر یک از این عوالم مشتمل است بر عجایب بسیار و غرایب بی شمار که تعداد آنها از حد و حصر متجاوز و بنان و بیان از ترقیم آن ابکم و عاجز است.</p></div>