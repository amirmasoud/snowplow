---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>گفتم ز دعای من شبخیز حذر کن</p></div>
<div class="m2"><p>گفتا برو اظهار ورع جای دگر کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که قدم در ره عشق تو نهم گفت</p></div>
<div class="m2"><p>بگذار ولیکن قدم خویش ز سر کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم نظری بر رخ زیبای تو خواهم</p></div>
<div class="m2"><p>گفتا برو از هر دو جهان قطع نظر کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم که دلم، گفت سراغ ره ما گیر</p></div>
<div class="m2"><p>گفتم که سرم، گفت به فتراک نظر کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم چکنم ره به سر کوی تو یابم</p></div>
<div class="m2"><p>گفتا که برو خانه خود زیر و زبر کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم که ز غم ناله کنم گفت بپرهیز</p></div>
<div class="m2"><p>گفتم ز ستم شکوه کنم گفت حذر کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم که «صفایی» هوس وصل تو دارد</p></div>
<div class="m2"><p>گفتا ز سر خود هوس خام به در کن</p></div></div>