---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>یا رب ز بخت ماست که شد ناله بی اثر</p></div>
<div class="m2"><p>یا هرگز آه و ناله و زاری اثر نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان بی نشان ز هر که نشان جستم ای عجب</p></div>
<div class="m2"><p>دیدم چو من ز هیچ نشانی خبر نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم علاج غم به دعای سحر کنم</p></div>
<div class="m2"><p>غافل از اینکه تیره شب ما سحر نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردا که دوش طاعت سی سال خویش را</p></div>
<div class="m2"><p>دادم به می فروش به یک جرعه برنداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دنیا و آخرت همه دادم به عشق و بس</p></div>
<div class="m2"><p>شادم که این معامله یک جو ضرر نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ترک عشق کرد صفایی عجب مدار</p></div>
<div class="m2"><p>بیچاره تاب محنت از این بیشتر نداشت</p></div></div>