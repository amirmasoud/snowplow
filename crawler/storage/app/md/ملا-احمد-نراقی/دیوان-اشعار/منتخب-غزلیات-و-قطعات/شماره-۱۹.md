---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>در سر افتاده ست شوق باده ام</p></div>
<div class="m2"><p>چون کنم در دام زهد افتاده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساده لوحم همگنان دانند و من</p></div>
<div class="m2"><p>در پی مه طلعتان ساده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوش خواهد از من و من عقل و هوش</p></div>
<div class="m2"><p>در خراباتی گرو بنهاده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از محبت می کنندم منع و من</p></div>
<div class="m2"><p>خود ز مادر با محبت زاده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فاش کردی ای صفایی سرّ من</p></div>
<div class="m2"><p>شرم کن از مصحف و سجّاده ام</p></div></div>