---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>از راه وفا گاه ز ما یاد توان کرد</p></div>
<div class="m2"><p>گاهی به نگاهی دل ما شاد توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صید دل من لایق تیغ تو اگر نیست</p></div>
<div class="m2"><p>از بهر خدا آخرش آزاد توان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم مگر از ناله به رحم آورم ای دل</p></div>
<div class="m2"><p>ای وای (!) چه با خوی خداداد توان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین بعد کسی ناله من نشنود آری</p></div>
<div class="m2"><p>تا چند مگر ناله و فریاد توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستم زمی عشق چنان کز پی مرگم</p></div>
<div class="m2"><p>صید میکده از خاک من آزاد توان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>انصاف کجا رفت ببین مدرسه کردند</p></div>
<div class="m2"><p>جایی که در آن میکده بنیاد توان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منمای به زاهد تو ره کوی خرابات</p></div>
<div class="m2"><p>این ره نه به هر بوالهوس ارشاد توان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با غیر صفایی مه من عهد وفا بست</p></div>
<div class="m2"><p>دل را به چه امید دگر شاد توان کرد</p></div></div>