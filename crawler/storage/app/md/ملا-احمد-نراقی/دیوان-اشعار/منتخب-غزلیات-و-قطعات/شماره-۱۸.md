---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>این خانه ی دل خراب بهتر</p></div>
<div class="m2"><p>وین سینه ز غم کباب بهتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستار و ردا و جبّه ی من</p></div>
<div class="m2"><p>اندر گرو شراب بهتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اوراق کتاب دانش من</p></div>
<div class="m2"><p>شستن همه را به آب بهتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی که ز غم به خواب بینی</p></div>
<div class="m2"><p>پس کار همیشه خواب بهتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چند حدیث عقل ای دل؟</p></div>
<div class="m2"><p>بر هم نهی این کتاب بهتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رو رو دو سه درس عشق بشنو</p></div>
<div class="m2"><p>آواز نی و رباب بهتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهد در دین زند «صفایی»</p></div>
<div class="m2"><p>کردن ز وی اجتناب بهتر</p></div></div>