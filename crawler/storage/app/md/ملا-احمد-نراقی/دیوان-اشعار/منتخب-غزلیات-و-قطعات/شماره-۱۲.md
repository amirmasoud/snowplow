---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>عشاق تو جز دیدهٔ خونبار نخواهند</p></div>
<div class="m2"><p>غیر از دل آزرده افکار نخواهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد که این درد مرا کشت که آن دوست</p></div>
<div class="m2"><p>با من نکند مهر که اغیار نخواهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بوالهوسان دور شوید از من مسکین</p></div>
<div class="m2"><p>مردان دهش رونق بازار نخواهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو قیمت ما بشکند آنجا که کسی را</p></div>
<div class="m2"><p>باشند خریدار و خریدار نخواهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را هوس انجمنی نیست که عشاق</p></div>
<div class="m2"><p>جز خلوت و در دل گله با یار نخواهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویی بر زاهد چه حدیث از می و معشوق؟!</p></div>
<div class="m2"><p>این طایفه جز جبه و دستار نخواهند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منصور از آن بر سر دار است که خوبان</p></div>
<div class="m2"><p>ارباب وفا جز به سر دار نخواهند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا باشدشان عذر جفا خیل نکویان</p></div>
<div class="m2"><p>جز عاشق بدنام گنهکار نخواهند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنها که ز خوبان دلشان است به دامان</p></div>
<div class="m2"><p>صد خرمن گل گلشن و گلزار نخواهند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان بر کف خود گیر صفایی به ره عشق</p></div>
<div class="m2"><p>در کوی بتان درهم و دینار نخواهند</p></div></div>