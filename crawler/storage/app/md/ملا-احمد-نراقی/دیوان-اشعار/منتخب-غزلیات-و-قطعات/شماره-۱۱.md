---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>دانی که یار حاجت ما کی روا کند</p></div>
<div class="m2"><p>چون تیغ را به گردن ما آشنا کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی داد پیر میکده فتوی که لازم است</p></div>
<div class="m2"><p>بی عشق هرکه کرده نمازی قضا کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای پیک کوی یار به صیاد ما بگوی</p></div>
<div class="m2"><p>بهر خدا مرا بکشد یا رها کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را چو قبله ابروی یارست در نماز</p></div>
<div class="m2"><p>باید امام شهر به ما اقتدا کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما لطف و قهر را همه آماده ایم، لیک</p></div>
<div class="m2"><p>تا طبع یار زین دو کدام اقتضا کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رند خراب طی کند این راه پرخطر</p></div>
<div class="m2"><p>تا شیخ فکر کفش و عصا و ردا کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مفتی بخورد خون یتیمان شهر و باز</p></div>
<div class="m2"><p>بیچاره ناله از کمی اشتها کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مطلب بر است چونکه صفایی رضای دوست</p></div>
<div class="m2"><p>خواهد جفا نماید و خواهد وفا کند</p></div></div>