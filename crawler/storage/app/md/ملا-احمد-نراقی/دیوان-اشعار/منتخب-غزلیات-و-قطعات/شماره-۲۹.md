---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>ساقیا امشب مرا زان آب رمّانی بده</p></div>
<div class="m2"><p>جامها پی در پی از آبی که می دانی بده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شدم من مست و بی خود زان دو لعلم بوسه ها</p></div>
<div class="m2"><p>آشکارا گر نمی خواهی به پنهانی بده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد تهی دلها ز عشق و بسته شد میخانه ها</p></div>
<div class="m2"><p>رونقی یا رب به آیین مسلمانی بده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوره ی روحانیان است امشب اندر بزم ما</p></div>
<div class="m2"><p>هان و هان ساقی بیا صهبای روحانی بده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا رهایی زین خمار کهنه بخشایی مرا</p></div>
<div class="m2"><p>زآن شراب کهنه آنقدری که بتوانی بده</p></div></div>