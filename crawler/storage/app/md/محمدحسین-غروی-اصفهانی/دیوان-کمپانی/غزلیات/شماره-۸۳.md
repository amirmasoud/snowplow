---
title: >-
    شمارهٔ  ۸۳
---
# شمارهٔ  ۸۳

<div class="b" id="bn1"><div class="m1"><p>گفتم چه دیدم آن رخ و آن زلف تابدار:</p></div>
<div class="m2"><p>«آمنت بالذی خلق اللیل و النهار»</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از طور کوی دوست سنا برق روی دوست</p></div>
<div class="m2"><p>آمد چنان به جلوه که «آنست منه نار»</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دیده ای چه شمع دل افروز اشک ریز</p></div>
<div class="m2"><p>هر سینه ای ز داغ جهانسوز لاله زار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عاشقان نوای «انا الحق» بر آسمان</p></div>
<div class="m2"><p>می زد علم اگر که نمی بود بیم دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با قبۀ جلال وی این نه رواق را</p></div>
<div class="m2"><p>در دیدۀ کمال چه قدر است و اعتبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا خم شد آسمان که شود حلقۀ درش</p></div>
<div class="m2"><p>از مهر و مه نهاد به سر تاج افتخار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک تار از دو طرۀ او را بباد داد</p></div>
<div class="m2"><p>باد صبا و روز مرا تیره کرد و تار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با چین او کسی نبرد نام ملک چین</p></div>
<div class="m2"><p>تاری از او قلم زده بر خطۀ تتار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بالا بلند او گذری کرد از چمن</p></div>
<div class="m2"><p>آب از دو دیده کرد روان سرو جویبار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نخل شکر برش که ز شیرین گرفته تاج</p></div>
<div class="m2"><p>فرهادوار کرده مرا کوه غم ببار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زد مفتقر به یاد تو ای دوست این رقم</p></div>
<div class="m2"><p>شاید به روزگار بماند به یادگار</p></div></div>