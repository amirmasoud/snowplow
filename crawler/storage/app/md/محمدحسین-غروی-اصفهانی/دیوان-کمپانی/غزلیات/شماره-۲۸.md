---
title: >-
    شمارهٔ  ۲۸
---
# شمارهٔ  ۲۸

<div class="b" id="bn1"><div class="m1"><p>هرجا که بسوی تو می بینم</p></div>
<div class="m2"><p>یک جا همه روی تو می بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریای محیط دو گیتی را</p></div>
<div class="m2"><p>یک قطره ز جوی تو می بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طغرای صحیفۀ هستی را</p></div>
<div class="m2"><p>در طرۀ موی تو می بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ارکان اریکۀ حشمت را</p></div>
<div class="m2"><p>در کعبۀ کوی تو می بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از صبح ازل تا شام ابد</p></div>
<div class="m2"><p>یک نعمه ز هوی تو می بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبود عجب ار خم گردون را</p></div>
<div class="m2"><p>سرشار سبوی تو می بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من مفتقر سودا زده را</p></div>
<div class="m2"><p>شوریدۀ بوی تو می بینم</p></div></div>