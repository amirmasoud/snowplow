---
title: >-
    شمارهٔ  ۱۴
---
# شمارهٔ  ۱۴

<div class="b" id="bn1"><div class="m1"><p>با نیک و بد دنیا خوش باش</p></div>
<div class="m2"><p>چون بادۀ صافی بیغش باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خاک چو آب برم آرام</p></div>
<div class="m2"><p>وز باد هوی نه چو آتش باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خلق زمانه کناره بگیر</p></div>
<div class="m2"><p>یا با همگی بکشاکش باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با مردم نادان کله مزن</p></div>
<div class="m2"><p>تسلیم کن و بزا خفش باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دیو طبیعت دوری کن</p></div>
<div class="m2"><p>دیوانۀ یار پریوش باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمعیت خاطر اگر طلبی</p></div>
<div class="m2"><p>چون زلف نگار مشوش باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نقش نگار همی جوئی</p></div>
<div class="m2"><p>از اشک و سرشک منقش باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون مفتقر اندر کوی وفا</p></div>
<div class="m2"><p>از روی نیاز بلاکش باش</p></div></div>