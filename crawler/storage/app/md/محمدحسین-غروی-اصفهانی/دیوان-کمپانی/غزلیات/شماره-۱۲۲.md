---
title: >-
    شمارهٔ  ۱۲۲
---
# شمارهٔ  ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>که دهد مرا نشانی ز تو ای نگار جانی</p></div>
<div class="m2"><p>که نشان هر نشانی است نشان بی نشانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه ز صورت تو رسمی نه ز معنی تو اسمی</p></div>
<div class="m2"><p>که برون ز هر خیالی و فزون ز هر گمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بتو ای یگانه دلبر که شود دلیل و رهبر</p></div>
<div class="m2"><p>نه ترا به حسن مانند و نه در کمال ثانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر آنکه شعلۀ روی تو سوز دل نشاند</p></div>
<div class="m2"><p>بتجلی تو بینند جمال «لن ترانی»</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکدام سعی و کوشش به تو می توان رسیدن</p></div>
<div class="m2"><p>مگر آنکه چهره بگشائی و سوی خود کشانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه به جد و جهد مردی به مراد خود رسیدم</p></div>
<div class="m2"><p>نه ز احتمال هجران به وصال خود رسانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه مرا مجال درگاه تو تا بسر بیایم</p></div>
<div class="m2"><p>نه تو آمدی که تا سر فکنم بمژدگانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من و حسرت تو خوردن من و از غم تو مردن</p></div>
<div class="m2"><p>چه در از غمت نیاسود چه سود زندگانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من و آتش فراقت من و سوز اشتیاقت</p></div>
<div class="m2"><p>که توان ز جان گذشتن نتوان ز یار جانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه خوش است صبر بلبل به امید صحبت گل</p></div>
<div class="m2"><p>من و بعد از این تحمل، تو و هرچه می توانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل مفتقر ز خونابۀ غصۀ تو سر خوش</p></div>
<div class="m2"><p>ز تو درد، عین درمان، و غم تو شادمانی</p></div></div>