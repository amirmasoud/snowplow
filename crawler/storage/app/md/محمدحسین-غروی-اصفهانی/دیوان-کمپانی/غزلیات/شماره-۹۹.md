---
title: >-
    شمارهٔ  ۹۹
---
# شمارهٔ  ۹۹

<div class="b" id="bn1"><div class="m1"><p>به امید روی دلدار ز آبرو گذشتم</p></div>
<div class="m2"><p>به هوای صحبت یار ز های و هو گذشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سرشک چشم و خونابۀ دل نگار بستم</p></div>
<div class="m2"><p>ز خط عذار و خال لب و رنگ و بو گذشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکمند مشگمویان شده ام اسیر لیکن</p></div>
<div class="m2"><p>بدلاوری و همت ز میان مو گذشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه چو خضر سر بصحر از ده ام بجستجویت</p></div>
<div class="m2"><p>که ز جوی زندگی نیز بجستجو گذشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر چون کدوی بی مغز فکنده ام بپایت</p></div>
<div class="m2"><p>نه عجب که بهر سروری ز سر کدو گذشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه روزه گفتگوی من و عاشقان تو بودی</p></div>
<div class="m2"><p>چه نماید محرمی از سر گفتگو گذشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خیال شست و شوئی بدر تو رخت بستم</p></div>
<div class="m2"><p>ز غبار ره چنانم که ز شست و شو گذشتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل و دین من ز کف رفت بباد آرزوها</p></div>
<div class="m2"><p>چه غم تو روزیم شد ز هر آرزو گذشتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل مفتقر ز شوق تو لبالب است آری</p></div>
<div class="m2"><p>که هماره در بدر رفتم و کو به کو گذشتم</p></div></div>