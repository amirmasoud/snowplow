---
title: >-
    شمارهٔ  ۵۴
---
# شمارهٔ  ۵۴

<div class="b" id="bn1"><div class="m1"><p>دلی که شیفتۀ روی آن پریزاد است</p></div>
<div class="m2"><p>ز بند دیو طبیعت هماره آزاد است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خراب بادۀ عشق تو ای بت سرمست</p></div>
<div class="m2"><p>خرابی دل او زین خراب آباد است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز جام باده طلب عکس شاهد وحدت</p></div>
<div class="m2"><p>که نقش باطل کثرت چو کاه بر باد است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بکوی حقیقت گذر کنی بینی</p></div>
<div class="m2"><p>اساس ملک طبیعت چه سست بنیاد است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر تک و بوی مشو غره و بجو یاری</p></div>
<div class="m2"><p>که جسن صورت و معنی او خداداد است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگار من! بگشا چهره تا که بگشائی</p></div>
<div class="m2"><p>ز کار من گرهی را که مشکل افتاد است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قمار عشق بسی با تو باختیم ولی</p></div>
<div class="m2"><p>تو را عجب که فراموش شد، مرا یاد است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درون سوختگان را نمانده جز آهی</p></div>
<div class="m2"><p>چه حال داد نباشد چه جای فریاد است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قرین یار سهی سرو را چه آگاهی است</p></div>
<div class="m2"><p>از آن کسی که زمین گیر شاخ شمشاد است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز تلخ کامی ایام خوشترین خبری</p></div>
<div class="m2"><p>حکایت لب شیرین و شور فرهاد است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو مفتقر بغمم دوست مبتلائی نیست</p></div>
<div class="m2"><p>ولیک چون غم عشق است دل بسی شاد است</p></div></div>