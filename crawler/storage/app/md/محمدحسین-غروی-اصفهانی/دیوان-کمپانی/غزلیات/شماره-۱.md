---
title: >-
    شمارهٔ  ۱
---
# شمارهٔ  ۱

<div class="b" id="bn1"><div class="m1"><p>ای شمع جهان افروز بیا</p></div>
<div class="m2"><p>وی شاهد عالم سوز بیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای مهر سپهر قلمرو غیب</p></div>
<div class="m2"><p>شد روز ظهور و بروز بیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای طائر سعد فرخ رخ</p></div>
<div class="m2"><p>امروز توئی فیروز بیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزم از شب تیره تر است</p></div>
<div class="m2"><p>ای خود شب ما را روز بیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما دیده براه تو دوخته ایم</p></div>
<div class="m2"><p>از ما همه چشم مدوز بیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمریست گذشته بنادانی</p></div>
<div class="m2"><p>ای علم و ادب آموز بیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد گلشن عمر خزان از غم</p></div>
<div class="m2"><p>ای باد خوش نوروز بیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من مفتقر رنجور توام</p></div>
<div class="m2"><p>تا جان بلب است هنوز بیا</p></div></div>