---
title: >-
    شمارهٔ  ۱۱۰
---
# شمارهٔ  ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>سرم را پر کن از سودای عشق و سربلندم کن</p></div>
<div class="m2"><p>سویدای مرا سرشار شوق و مستمندم کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل آشفته ام چون آهوی وحشی رمید از من</p></div>
<div class="m2"><p>گره بگشا ز زاف مشگسای و در کمندم کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سکندروارم از سرچشمۀ حیوان مکن محروم</p></div>
<div class="m2"><p>چه خضرم کامیاب از لعل نوشخندم کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سلیمانا مرا دیو طبیعت کرده اندر بند</p></div>
<div class="m2"><p>باسم اعظم آزادم کن و فارغ ز بندم کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلا گردان خویشم کن به قربان سرت گردم</p></div>
<div class="m2"><p>بفرما جلوه ای بر آتش غیرت سپندم کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرابم کن ز جامی تا به آزادی زنم گامی</p></div>
<div class="m2"><p>مرا از خویشتن بیخود کن، از خود بهره مندم کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سمند طبع لنگست و مجال جانفشانی نیست</p></div>
<div class="m2"><p>مرا خاک ره میدان آن رفرف سمندم کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پسند طبع والای تو نبود مفتقر هرگز</p></div>
<div class="m2"><p>ولی قطع نظر جانا ز وضع ناپسندم کن</p></div></div>