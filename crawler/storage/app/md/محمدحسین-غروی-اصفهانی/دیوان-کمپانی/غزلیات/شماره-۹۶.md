---
title: >-
    شمارهٔ  ۹۶
---
# شمارهٔ  ۹۶

<div class="b" id="bn1"><div class="m1"><p>از رقیبان تو تا چند من اندیشه کنم</p></div>
<div class="m2"><p>بیخ اندیشۀ بد را چه خوش از ریشه کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش بینی کند از مرحلۀ عشقم دور</p></div>
<div class="m2"><p>شیوۀ رندی و مستی پس از این پیشه کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیشۀ شیر هوا را بزنم آتش عشق</p></div>
<div class="m2"><p>تا کی از کم خردی بیمی از این بیشه کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ریشۀ تفرقه را بر کنم از این دل ریش</p></div>
<div class="m2"><p>تا که در گلشن توحید مگر ریشه کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ره صدق و صفا راه وفا گیرم پیش</p></div>
<div class="m2"><p>تا به کی پیروی خلق جفا پیشه کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مفتقر سینه زند جوش ز سودای نگار</p></div>
<div class="m2"><p>چارۀ درد دل خویش از این شیشه کنم</p></div></div>