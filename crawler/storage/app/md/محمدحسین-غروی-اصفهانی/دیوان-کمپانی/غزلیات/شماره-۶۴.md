---
title: >-
    شمارهٔ  ۶۴
---
# شمارهٔ  ۶۴

<div class="b" id="bn1"><div class="m1"><p>فدائیان ره عشق دوست مرد رهند</p></div>
<div class="m2"><p>چه جان دهند به جانان ز بند تن برهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شاهراه طریقت حقیقت ار طلبی</p></div>
<div class="m2"><p>در آخرین نفست، اولین قدم بدهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بروز چاه طبیعت بدر که چون صدیق</p></div>
<div class="m2"><p>زمام مملکت مصر در کفت بنهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شوق گلشن جان بلبلان چنان مستند</p></div>
<div class="m2"><p>که بی تکلفی از دام این جهان برهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صفای دل بطلب رو سفید خواهی بود</p></div>
<div class="m2"><p>وگرنه ای چه بسا رو سفید و دل سیهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شور آن لب شیرین بنال چون فرهاد</p></div>
<div class="m2"><p>که خسیروان جهان فکر افسر و گلهند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظر به ملک دو گیتی کجا گدایان راست</p></div>
<div class="m2"><p>که در قلمرو وحدت یگانه پادشهند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه شمع، گاه تجلی بیزم شاهد جمع</p></div>
<div class="m2"><p>بروز حمله وری یکه تاز بزمگهند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه گیسوان مسلسل بدور روی نگار</p></div>
<div class="m2"><p>عجب مدار که سلطان عشق را سپهند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگرچه مفتقر از ذره کمتر است ولی</p></div>
<div class="m2"><p>امیدوار به آنان بود که مهر و مهند</p></div></div>