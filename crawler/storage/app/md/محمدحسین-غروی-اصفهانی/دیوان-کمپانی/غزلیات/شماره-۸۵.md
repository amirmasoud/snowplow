---
title: >-
    شمارهٔ  ۸۵
---
# شمارهٔ  ۸۵

<div class="b" id="bn1"><div class="m1"><p>خوشست از دوست گر لطفست و گر قهر</p></div>
<div class="m2"><p>به از شهد است از دست بتان زهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عروس عشق را در سنت عشق</p></div>
<div class="m2"><p>بود جان گرامی کمترین مهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آشوب رخت ای یار سرمست</p></div>
<div class="m2"><p>نمی بینم دلی فارغ در این شهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دنیا رخت می بایست بستن</p></div>
<div class="m2"><p>نشاید زندگی با فتنۀ دهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غمت در باطن است اما خموشم</p></div>
<div class="m2"><p>دریغا کاین خموشی بشکند ظهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دریای دلم چون خون زند موج</p></div>
<div class="m2"><p>به پیوندد سرشک دیده چون نهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ثنا جوی توأم هر صبح و هر شام</p></div>
<div class="m2"><p>دعاگوی توام فی السرّ و الجهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگو شد مفتقر بی بهره از دوست</p></div>
<div class="m2"><p>غمش دارم که باشد بهترین بهر</p></div></div>