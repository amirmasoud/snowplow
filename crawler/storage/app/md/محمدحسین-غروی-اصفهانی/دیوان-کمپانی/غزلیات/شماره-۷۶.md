---
title: >-
    شمارهٔ  ۷۶
---
# شمارهٔ  ۷۶

<div class="b" id="bn1"><div class="m1"><p>گهی به کعبۀ جانان سفر توانی کرد</p></div>
<div class="m2"><p>که در منای وفا ترک سر توانی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>براه عشق توانی که رهسپر گردی</p></div>
<div class="m2"><p>اگر که سینۀ خود را سپر توانی کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بچار بالش خواب آنگهی تو تکیه زنی</p></div>
<div class="m2"><p>که تن نشانۀ تیر سه پر توانی کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسیم صبح مراد آنگهی کند شادت</p></div>
<div class="m2"><p>که خدمت از سر شب تا سحر توانی کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز فیض گفت و شنودش چه بهره ها ببری</p></div>
<div class="m2"><p>اگر بطور شهودش گذر توانی کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا ببوی حقیقت دماغ تر گردد</p></div>
<div class="m2"><p>اگر که دیو طبیعت بدر توانی کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر بلند شوی از حضیض وهم و خیال</p></div>
<div class="m2"><p>ز اوج عقل چه خورشید سر توانی کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ره ارچه تیره و تار است و طی او مشکل</p></div>
<div class="m2"><p>ولی به همت اهل نظر توانی کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جدا مشو ز در دوست مفتقر هرگز</p></div>
<div class="m2"><p>که چارۀ دل ازین رهگذر توانی کرد</p></div></div>