---
title: >-
    شمارهٔ  ۸۱
---
# شمارهٔ  ۸۱

<div class="b" id="bn1"><div class="m1"><p>اگر به شرط مروت وفا توانی کرد</p></div>
<div class="m2"><p>گذر به صفۀ اهل صفا توانی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به همت ار بروی برتر از نشیمن خاک</p></div>
<div class="m2"><p>به زیر سایه هزاران هما توانی کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جد و جهد چه عنقا برو به قلۀ قاف</p></div>
<div class="m2"><p>اگر که حوصلۀ آن قضا توانی کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهود جلوۀ جانان ترا نصیب شود</p></div>
<div class="m2"><p>اگر زجاجۀ جان را جلا توانی کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گنج معرفت و دولت بقا برسی</p></div>
<div class="m2"><p>اگر تحمل فقر و فنا توانی کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به شاهباز حقیقت گهی تو ره ببری</p></div>
<div class="m2"><p>که باز آز و هوا را رها توانی کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر به گلشن دیدار او رهی یابی</p></div>
<div class="m2"><p>ز شور عشق چه دستان نوا توانی کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمیم طرۀ او گر ترا رسد به مشام</p></div>
<div class="m2"><p>فتوحها چه نسیم صبا توانی کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو دیده بر هم و چشم دلی فراهم کن</p></div>
<div class="m2"><p>به یک نظارۀ او کیما توانی کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به شاهراه طریقت چه رهسپار شوی</p></div>
<div class="m2"><p>ز گرد راه بسی توتیا توانی کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدر اشک و عقیق سرشک روی بشوی</p></div>
<div class="m2"><p>که تا به همت پاکان دعا توانی کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه حلقه بر در او باش مفتقر همه عمر</p></div>
<div class="m2"><p>که درد خویش ازین در دوا توانی کرد</p></div></div>