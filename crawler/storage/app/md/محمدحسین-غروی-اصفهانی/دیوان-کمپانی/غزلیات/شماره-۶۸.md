---
title: >-
    شمارهٔ  ۶۸
---
# شمارهٔ  ۶۸

<div class="b" id="bn1"><div class="m1"><p>رموز عشق را جز عاشقِ صادق نمی‌داند</p></div>
<div class="m2"><p>حدیث حسن عذرا را به جز وامق نمی‌داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا شوقی بود در دل که اظهارش بُوَد مشکل</p></div>
<div class="m2"><p>چه راز است آنکه جز صاحبدل شائق نمی‌داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علاج دردمند عشق صبر است و شکیبایی</p></div>
<div class="m2"><p>ز من بشنو، طبیب ماهر حاذق نمی‌داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلی سِرّ حقیقت راز مردان طریقت جوی</p></div>
<div class="m2"><p>لِسان عشق را البته هر ناطق نمی‌داند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیندیشد ز آخر هرکه ز اول عشق آموزد</p></div>
<div class="m2"><p>ز خود بگذشته هرگز سابق و لاحق نمی‌داند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشاید مشت خاکی را چه آتش سرکشی کردن</p></div>
<div class="m2"><p>که هر سنجیده خود را بر کسی فائق نمی‌داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مده فرماندهی در ملک دل دیو طبیعت را</p></div>
<div class="m2"><p>که عقل این حکمرانی را بر او لایق نمی‌داند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به عیب ظاهر مخلوق بر باطن مکن حکمی</p></div>
<div class="m2"><p>که مکنون خلایق را به جز خالق نمی‌داند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بپرس از مفتقر هر نکته‌ای کز عشق می‌خواهی</p></div>
<div class="m2"><p>فنون عشق عذرا را به جز وامق نمی‌داند</p></div></div>