---
title: >-
    شمارهٔ  ۷۱
---
# شمارهٔ  ۷۱

<div class="b" id="bn1"><div class="m1"><p>خسته ای را که دگر طاقت و قوت نبود</p></div>
<div class="m2"><p>گر تفقد نکنی شرط مروت نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پدر پیر فلک را نبود مهر و وفا</p></div>
<div class="m2"><p>شفقت نیز مگر رسم ابوت نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با فراق تو قرینم ز چه اندر همه عمر</p></div>
<div class="m2"><p>گر مرا با غم عشق تو اخوت نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلبرا هر که در این در به غلامی شده پیر</p></div>
<div class="m2"><p>گر شود رانده از این در ز فتوت نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کس به مقصد نرسد گر نکنی همراهی</p></div>
<div class="m2"><p>قطع این مرحله با قدرت و قوت نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مفتقر در همۀ کون و مکان کوی امید</p></div>
<div class="m2"><p>جز در صاحب دیوان نبوت نبود</p></div></div>