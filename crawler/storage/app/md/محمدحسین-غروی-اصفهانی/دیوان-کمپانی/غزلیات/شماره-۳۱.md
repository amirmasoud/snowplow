---
title: >-
    شمارهٔ  ۳۱
---
# شمارهٔ  ۳۱

<div class="b" id="bn1"><div class="m1"><p>تنها نه منم به کمند هوا</p></div>
<div class="m2"><p>من رام رکوب العشق هویٰ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از من نه عجب که گنه کارم</p></div>
<div class="m2"><p>وصفی الله عصی فغویٰ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز اشک و سرشک من از دل من</p></div>
<div class="m2"><p>من حدّث عن قلبی و رویٰ؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنجور ترا بهبودی نیست</p></div>
<div class="m2"><p>اذ لیس لداء الحب دوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد ملک عراق پر از آشوب</p></div>
<div class="m2"><p>ز سرود حجازی و شور و نوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو روح روان منی جانا</p></div>
<div class="m2"><p>نکنی ز چه حاجت بنده روا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه ز گریه مرا چشمی بینا</p></div>
<div class="m2"><p>نه ز سوز غمت گوشی شنوا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای شاخ گل تر من رحمی</p></div>
<div class="m2"><p>بر مفتقر بی برگ و نوا</p></div></div>