---
title: >-
    شمارهٔ  ۳۹
---
# شمارهٔ  ۳۹

<div class="b" id="bn1"><div class="m1"><p>ای روی تو قبلۀ حاجاتم</p></div>
<div class="m2"><p>وی کوی تو طور مناجاتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مصباح جهان افروز ترا</p></div>
<div class="m2"><p>از پرتو لطف تو مشکوتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر سینه شود سینا چه عجب</p></div>
<div class="m2"><p>گر جلوه کنی به میقاتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا خاک نشین ره تو شوم</p></div>
<div class="m2"><p>شد جام جهان بین مرآتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر گوهر معرفت اندوزم</p></div>
<div class="m2"><p>گنجینۀ کل کمالاتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>معمورۀ حسن تو کرده مرا</p></div>
<div class="m2"><p>سرگشتۀ کوی خراباتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بندۀ خویشم گردانی</p></div>
<div class="m2"><p>بیزار ز کشف و کراماتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای یوسف حسن ازل نظری</p></div>
<div class="m2"><p>کز عشق تو تا بابد ماتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این است کلافۀ مفتقرت</p></div>
<div class="m2"><p>این است بضاعت مزجاتم</p></div></div>