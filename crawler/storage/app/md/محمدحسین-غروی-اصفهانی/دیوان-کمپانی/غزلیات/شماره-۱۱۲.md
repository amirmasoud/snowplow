---
title: >-
    شمارهٔ  ۱۱۲
---
# شمارهٔ  ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>تا چند باشی از ما گریزان</p></div>
<div class="m2"><p>ما در قفایت افتان و خیزان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند باشیم چون شمع سوزان</p></div>
<div class="m2"><p>با شعلۀ آه، با اشک ریزان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چند بینند اهل بصیرت</p></div>
<div class="m2"><p>جور دمادم از بی تمیزان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنهاده تا چند بر خاک ذلت</p></div>
<div class="m2"><p>روی مذلت خیل عزیزان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیداد خوبان خوبست لیکن</p></div>
<div class="m2"><p>ای سنبل تر قدری به میزان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر خون ما را جانا بریزی</p></div>
<div class="m2"><p>لیک آبروی ما را مریزان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا یاد موی و بوی تو کردم</p></div>
<div class="m2"><p>آهوی طبعم شد مشک بیزان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر مفتقر را پیرایه‌ای نیست</p></div>
<div class="m2"><p>نبود به ار حسن در بی‌جهیزان</p></div></div>