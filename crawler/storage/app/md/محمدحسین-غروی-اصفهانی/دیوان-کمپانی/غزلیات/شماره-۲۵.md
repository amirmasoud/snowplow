---
title: >-
    شمارهٔ  ۲۵
---
# شمارهٔ  ۲۵

<div class="b" id="bn1"><div class="m1"><p>آن کیست که بستۀ بند تو نیست</p></div>
<div class="m2"><p>یا آنکه اسیر کمند تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دل نه دلست که از خامی</p></div>
<div class="m2"><p>در آتش عشق، سپند تو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از راه سعادت گمراهست</p></div>
<div class="m2"><p>آنکس که ارادتمند تو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لقمان که هزارانش پند است</p></div>
<div class="m2"><p>جز بندۀ حکمت و پند تو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرهاد تو را ای شیرین، لب</p></div>
<div class="m2"><p>خوشتر ز شکر و قند تو نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوته نظریم و مدیحۀ ما</p></div>
<div class="m2"><p>زیبندۀ سرو بلند تو نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چند دُر افشاند طبعم</p></div>
<div class="m2"><p>لیکن چکنم که پسند تو نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای مفتقر اینجا رفرف عقل</p></div>
<div class="m2"><p>لنگست و مجال سمند تو نیست</p></div></div>