---
title: >-
    شمارهٔ  ۱۰۳
---
# شمارهٔ  ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>من بناخن غم سینه می خراشم</p></div>
<div class="m2"><p>فی المثل چه فرهاد کوه می تراشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکساری تو کرده فرش راهم</p></div>
<div class="m2"><p>غمگساری تو صاحب فراشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ز دست جورت ناله ای بر آرم</p></div>
<div class="m2"><p>بر سر جهانی خاک غم بپاشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور باش غیرت رانده آن چنانم</p></div>
<div class="m2"><p>کز ره خیالت نیز دور باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای لب و دهانت رشک چشمۀ نوش</p></div>
<div class="m2"><p>چاره ای و رحمی زانکه ذو العطاشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی نازنینت بوی عنبرینت</p></div>
<div class="m2"><p>راحت معادم عشرت معاشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بند بندم از غم همچو نی بنالد</p></div>
<div class="m2"><p>در هوای کویت بسکه در تلاشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مفتقر ندارد جز کلافۀ لاف</p></div>
<div class="m2"><p>این بود متاعم وین بود قماشم</p></div></div>