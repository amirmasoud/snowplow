---
title: >-
    شمارهٔ  ۵۲
---
# شمارهٔ  ۵۲

<div class="b" id="bn1"><div class="m1"><p>از تو بیداد وز من ناله و فریاد خوش است</p></div>
<div class="m2"><p>چهچه از بلبل و از گل همه بیداد خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن لیلی بی سرگشتگی مجنون است</p></div>
<div class="m2"><p>شور شیرین و فداکاری فرهاد خوش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بندۀ شیفتۀ روی تو را آزادی است</p></div>
<div class="m2"><p>عشق در تربیت بنده و آزاد خوش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شعلۀ روی تو در خرمن دل آتش زد</p></div>
<div class="m2"><p>حاصل عمر اگر شد همه بر باد خوش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بکی در پی کوته نظرانی نگران</p></div>
<div class="m2"><p>چشم دل باز بکن صنعت استاد خوش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوی دیو از دل دیوانۀ خود بیرون کن</p></div>
<div class="m2"><p>گر ترا آمدن روی پریزاد خوش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیمی از سیل فنا نیست ز بد عاقبتی است</p></div>
<div class="m2"><p>ورنه این طرح دلاویز ز بنیاد خوش است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مفتقر! لشکر غم گر که خرابت نکند</p></div>
<div class="m2"><p>شادمان باش که در کشور آباد خوش است</p></div></div>