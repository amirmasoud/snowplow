---
title: >-
    شمارهٔ  ۱۲۴
---
# شمارهٔ  ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>لوح دل را اگر از نقش خطا ساده کنی</p></div>
<div class="m2"><p>خویش را آینۀ حسن خدا داده کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نگردد دلت از نائرۀ عشق کباب</p></div>
<div class="m2"><p>طمع خام بود گر هوس باده کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خانه را پاک کن از غیر که یار است غیور</p></div>
<div class="m2"><p>پاک زیبندۀ پاکست که آماده کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ز خلوت نرود دیو، کجا شایان است</p></div>
<div class="m2"><p>هوس آمدن روی پریزاده کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رستمی گر تو به این زال جهان رو نکنی</p></div>
<div class="m2"><p>یا نریمانی اگر پشت به این ماده کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا توانی در خلوتگه دل را بر بند</p></div>
<div class="m2"><p>ور نه کی منع توان از دل بگشاده کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر بلندی ز تو ای سرور من، نیست هنر</p></div>
<div class="m2"><p>هنر آنست که غمخواری افتاده کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل بکوی تو فرستادم و امید بود</p></div>
<div class="m2"><p>بپذیریّ و نگاهی بفرستاده کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مفتقر خواجۀ من بندۀ آزادۀ تست</p></div>
<div class="m2"><p>چه شود گر نظری جانب آزاده کنی</p></div></div>