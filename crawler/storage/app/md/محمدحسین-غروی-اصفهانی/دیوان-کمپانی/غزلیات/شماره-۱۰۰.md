---
title: >-
    شمارهٔ  ۱۰۰
---
# شمارهٔ  ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>تا شد آواره ز اقلیم حقیقت پدرم</p></div>
<div class="m2"><p>من از آن روز در این وادی غم در بدرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه چنان واله و سرگشته در این بادیه ام</p></div>
<div class="m2"><p>که ببانگ جرسی راه به جائی ببرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رحمی ای خضر ره گمشدگان بهر خدای</p></div>
<div class="m2"><p>بر لب خشک و دل سوخته و چشم ترم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه عشقست و هزاران خطرم از پس و پیش</p></div>
<div class="m2"><p>بی تو ای روح روان جان بسلامت نبرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشتی عمر گرفتار دو صد موج بلاست</p></div>
<div class="m2"><p>بار الها مددی کن، برهان از خطرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبود باک ز سرپنجۀ شاهین قضا</p></div>
<div class="m2"><p>گر بود سایۀ سلطان هما تاج سرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدم ای صبح مراد از افق بخت بلند</p></div>
<div class="m2"><p>تا بگردون نرسد شعلۀ آه سحرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مفتقر کیست؟ کمین بندۀ این درگاه است</p></div>
<div class="m2"><p>آری آری به غلامی درت مفتخرم</p></div></div>