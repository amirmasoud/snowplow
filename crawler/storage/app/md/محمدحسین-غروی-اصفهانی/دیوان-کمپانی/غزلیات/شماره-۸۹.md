---
title: >-
    شمارهٔ  ۸۹
---
# شمارهٔ  ۸۹

<div class="b" id="bn1"><div class="m1"><p>گر هوای سر کوی تو دهد بر بادم</p></div>
<div class="m2"><p>به حقیقت کند از بند هوا آزادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلوۀ روی قو از طور دلم برده قرار</p></div>
<div class="m2"><p>کز زمین می رسد اینک به فلک فریادم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سینه ام نالۀ جانسوز چه بنیاد کند</p></div>
<div class="m2"><p>سیل اشکی بزند سر بکند بنیادم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیلی حسن ترا بنده چنان مجنونم</p></div>
<div class="m2"><p>که خردمندی یک عمر برفت از یادم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خضر را چشمۀ حیوان همه ارزانی باد</p></div>
<div class="m2"><p>من دهان و لب شیرین ترا فرهادم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود امیدم که به جاه تو به جائی برسم</p></div>
<div class="m2"><p>در پی این طمع خام به چاه افتادم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آرزو داشتم از جام تو یابم کامی</p></div>
<div class="m2"><p>دو سه گامی شدم و دین و دل از کف دادم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواستم برگ و بری از گل روی تو برم</p></div>
<div class="m2"><p>بر زمینم بزد آن شاخۀ چون شمشادم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخت بستم به خرابات ز ویرانۀ دل</p></div>
<div class="m2"><p>تا که معمورۀ حسن تو کند آبادم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز غم عشق تو در لوح دلم نقش نبست</p></div>
<div class="m2"><p>مفتقر زین سبب از کلک مشیت شادم</p></div></div>