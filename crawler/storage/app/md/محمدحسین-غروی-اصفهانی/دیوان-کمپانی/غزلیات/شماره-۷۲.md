---
title: >-
    شمارهٔ  ۷۲
---
# شمارهٔ  ۷۲

<div class="b" id="bn1"><div class="m1"><p>عاشق از فتنۀ معشوق هراسان نشود</p></div>
<div class="m2"><p>تا که مشکل نشود واقعه آسان نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که ز آسیب ره عشق هراسان باشد</p></div>
<div class="m2"><p>به که اندر هوس سیب زنخدان نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا که از کار فرو بسته نباشد گریان</p></div>
<div class="m2"><p>یا که دل بستۀ آن پستۀ خندان نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منتهای غم آه، اول شادیست بلی</p></div>
<div class="m2"><p>تا به آخر نرسد درد تو درمان نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل آشفته ز جمعیت خاطر دور است</p></div>
<div class="m2"><p>تا که در حلقۀ آن زلف پریشان نشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا مسخر نشود دیو طبیعت روزی</p></div>
<div class="m2"><p>خاتم ملک در انگشت سلیمان نشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا نگردد چه عصا به هر کلیم افعی طبع</p></div>
<div class="m2"><p>از کفش چشمۀ خورشید درخشان نشود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شجر بی ثمر و شاخۀ بی برگ و بر است</p></div>
<div class="m2"><p>سر و دستی که نثاره ره جانان نشود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مفتقر روح وصالست فراق تن و جان</p></div>
<div class="m2"><p>به سر درست که تا این نشود آن نشود</p></div></div>