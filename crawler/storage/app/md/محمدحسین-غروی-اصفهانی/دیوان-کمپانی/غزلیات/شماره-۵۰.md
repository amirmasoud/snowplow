---
title: >-
    شمارهٔ  ۵۰
---
# شمارهٔ  ۵۰

<div class="b" id="bn1"><div class="m1"><p>کعبۀ کوی تو رشک خلد برین است</p></div>
<div class="m2"><p>قبلۀ روی تو آفت دل و دین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلسلۀ گیسوی تو حلقۀ دلها است</p></div>
<div class="m2"><p>پیچ و خم موی تو دام آهوی چین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمۀ نور است یا بود و ید بیضا</p></div>
<div class="m2"><p>پرتو نور است یا که نور جبین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خندۀ لعل تو یا که معجز بیّن</p></div>
<div class="m2"><p>غمزۀ چشم تو یا که سحر مبین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاخۀ طوبی مثال آن قد رعنا است</p></div>
<div class="m2"><p>سرو، هر آنکس شنیده است همین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلقت حشمت سزد به آن قد و بالا</p></div>
<div class="m2"><p>عالم هستی ترا بزیر نگین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچه که شوریده ام نموده چو فرهاد</p></div>
<div class="m2"><p>صحبت شیرین آن لب نمکین است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لیلی حسن ترا نه من، همه مجنون</p></div>
<div class="m2"><p>عشق رخت با جنون هماره قرین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بانگ انا الحق بزن که پرتو حسنت</p></div>
<div class="m2"><p>جلوه گر از طور آسمان و زمین است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تشنۀ دیدار تست مفتقر زار</p></div>
<div class="m2"><p>آب حیاتم توئی نه ماء معین است</p></div></div>