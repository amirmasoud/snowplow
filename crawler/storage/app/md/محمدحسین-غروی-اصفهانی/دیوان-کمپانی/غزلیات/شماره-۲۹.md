---
title: >-
    شمارهٔ  ۲۹
---
# شمارهٔ  ۲۹

<div class="b" id="bn1"><div class="m1"><p>تا گوهر عشق اندوخته‌ام</p></div>
<div class="m2"><p>چشم از همه عالم دوخته‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا با غم عشق تو ساخته‌ام</p></div>
<div class="m2"><p>همواره سراپا سوخته‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا سینۀ من سینای غم است</p></div>
<div class="m2"><p>چون نخلهٔ طور افروخته‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دفتر عشق تو روز نخست</p></div>
<div class="m2"><p>دیباچهٔ غم آموخته‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با شور غمت سودا زده‌ام</p></div>
<div class="m2"><p>نقد دل و دین بفروخته‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکس به دل‌آرامی دلشاد</p></div>
<div class="m2"><p>من مفتقر دل‌سوخته‌ام</p></div></div>