---
title: >-
    شمارهٔ  ۶۱
---
# شمارهٔ  ۶۱

<div class="b" id="bn1"><div class="m1"><p>تا دل آشفته ام شیفتۀ روی تست</p></div>
<div class="m2"><p>هر طرفی رو کنم روی دلم سوی تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگرد بیت الحرام طواف بر من حرام</p></div>
<div class="m2"><p>ای صنم خوش خرام کعبۀ من کوی تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ندهم از قصور به صحبت حسن حور</p></div>
<div class="m2"><p>بهشت اهل حضور صحبت دلجوی تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نافۀ مشک ختا گر طلبم من خطاست</p></div>
<div class="m2"><p>مشک من و عود من موی تو و بوی تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زندۀ لعل لبت خضر و نباشد عجب</p></div>
<div class="m2"><p>چشمۀ آب حیات قطره ای از جوی تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راهزن رهروان غمزۀ فتان تو</p></div>
<div class="m2"><p>دام دل عارفان سلسلۀ موی تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوز و گداز جهان از غم غمّازیت</p></div>
<div class="m2"><p>راز و نیاز همه در خم ابروی تست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طایفه‌ای مست می، مست هوا فرقه‌ای</p></div>
<div class="m2"><p>مفتقر بی‌نوا مست هیاهوی تست</p></div></div>