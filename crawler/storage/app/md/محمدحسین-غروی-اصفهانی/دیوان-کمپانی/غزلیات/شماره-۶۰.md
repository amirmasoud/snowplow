---
title: >-
    شمارهٔ  ۶۰
---
# شمارهٔ  ۶۰

<div class="b" id="bn1"><div class="m1"><p>مست صهبای تو در هر گذری نیست که نیست</p></div>
<div class="m2"><p>زانکه سودای تو در هیچ سری نیست که نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سینه گنجینۀ عشق تو و از لخت جگر</p></div>
<div class="m2"><p>لعل رمانی و والا گهری نیست که نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همتی بدرقۀ راه من گمشده کن</p></div>
<div class="m2"><p>راه عشقست ز هر سو خطری نیست که نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخل شکر بر تو زهر غم آورده ببار</p></div>
<div class="m2"><p>سرو آزاد ترا برگ و بری نیست که نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست بیداد بیند ای فلک سفله پرست</p></div>
<div class="m2"><p>ورنه این مظلمه را دادگری نیست که نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبح امید مرا تیره تر از شام مکن</p></div>
<div class="m2"><p>که مرا شعلۀ آه سحری نیست که نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق در پرده اگر باخته ام می دانم</p></div>
<div class="m2"><p>با چنین شور و نوا پرده دری نیست که نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه از بزم تو مهجور و بصورت دورم</p></div>
<div class="m2"><p>لیکم از عالم معنی خبری نیست که نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مفتقر خود بنظر بازی اگر می نازد</p></div>
<div class="m2"><p>تا بدانند که صاحب نظری نیست که نیست</p></div></div>