---
title: >-
    شمارهٔ  ۸۷
---
# شمارهٔ  ۸۷

<div class="b" id="bn1"><div class="m1"><p>گوهر عمر گرانمایه بود گرچه نفیس</p></div>
<div class="m2"><p>لیک از بهر نثار تو متاعیست خسیس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه ارباب قلم راست عطارد استاد</p></div>
<div class="m2"><p>لیک در مکتب عشق تو یکی تازه نویس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه در محفل دل عقل ندیمیست حکیم</p></div>
<div class="m2"><p>لیک با شیر قوی پنجۀ عشقشت جلیس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لشگر عشق بغارتگری کشور عقل</p></div>
<div class="m2"><p>کرد کاری که نه مرئوس بماند و نه رئیس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق دردیست که درمان نپذیرد هرگز</p></div>
<div class="m2"><p>ره بجائی نبرد فکر دو صد رسطالیس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نکتۀ عشق نگنجد به هزاران دفتر</p></div>
<div class="m2"><p>عشق درسی نبود ور چه مدرس ادریس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوهر عشق بجوی از صدف صدق و صفا</p></div>
<div class="m2"><p>ورنه افسانه و تلبیس بود از ابلیس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمر اگر می گذرد با تو نعیمی است مقیم</p></div>
<div class="m2"><p>زندگی بی غم عشق تو عذابی است بئیس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنچه را مفتقر از عشق سخن می گوید</p></div>
<div class="m2"><p>لوح سیمین بطلب با قلم زر نویس</p></div></div>