---
title: >-
    شمارهٔ  ۶۶
---
# شمارهٔ  ۶۶

<div class="b" id="bn1"><div class="m1"><p>مرا در سر بود شوری که در هر سر نمی‌گنجد</p></div>
<div class="m2"><p>نوای عاشقی در نای تن‌پرور نمی‌گنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کتاب لیلی و مجنون به مکتب خانه افسانه است</p></div>
<div class="m2"><p>حدیث عاشق و معشوق در دفتر نمی‌گنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وای عندلیب و شور قمری داستان‌ها ساخت</p></div>
<div class="m2"><p>چه لطف است اینکه اندر طائر دیگر نمی‌گنجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه هر مرغ شکرخایی بود طوطی شکرخا</p></div>
<div class="m2"><p>که طوطی را بود شهدی که در شکر نمی‌گنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز حسن دختر فکرم بود زال جهان واله</p></div>
<div class="m2"><p>کنار مادر گیتی جز این دختر نمی‌گنجد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا جز ساغر ابروی جانان آرزویی نیست</p></div>
<div class="m2"><p>نشاط عشق در هر باده و ساغر نمی‌گنجد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهال معرفت از جویبار چشم جوید آب</p></div>
<div class="m2"><p>چنان آبی که اندر چشمهٔ کوثر نمی‌گنجد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سلوک اهل دل از حیطۀ تعبیر بیرون است</p></div>
<div class="m2"><p>به جز در همت این معنای فرخ‌فر نمی‌گنجد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر مشتاق یاری مفتقر از خویش خالی شو</p></div>
<div class="m2"><p>خلیل عشق در بتخانهٔ آزر نمی‌گنجد</p></div></div>