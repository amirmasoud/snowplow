---
title: >-
    شمارهٔ  ۳۰
---
# شمارهٔ  ۳۰

<div class="b" id="bn1"><div class="m1"><p>از یار نیاز ندیده کسی</p></div>
<div class="m2"><p>جز عشوه و ناز ندیده کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند بسوز و بساز ولی</p></div>
<div class="m2"><p>این سوز و گداز ندیده کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک ساعت جور ترا بر من</p></div>
<div class="m2"><p>در عمر دراز ندیده کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز آگه اینهمه دوری را</p></div>
<div class="m2"><p>از محرم راز ندیده کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز لطف و نوازش دلجوئی</p></div>
<div class="m2"><p>از بنده نواز ندیده کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این شور و نوای عراقی را</p></div>
<div class="m2"><p>در ملک حجاز ندیده کسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز از لب مفتقرت هرگز</p></div>
<div class="m2"><p>این نغمه و ساز ندیده کسی</p></div></div>