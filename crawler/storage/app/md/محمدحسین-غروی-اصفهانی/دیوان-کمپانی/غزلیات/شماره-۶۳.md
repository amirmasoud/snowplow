---
title: >-
    شمارهٔ  ۶۳
---
# شمارهٔ  ۶۳

<div class="b" id="bn1"><div class="m1"><p>ما را به جهان جز به تو کاری نبود</p></div>
<div class="m2"><p>جز بر کرمت امیدواری نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیتابی عاشق بود از قلب سلیم</p></div>
<div class="m2"><p>زیرا که سلیم را قراری نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مست تو بجز دم ز انا الحق نزند</p></div>
<div class="m2"><p>در خاطرش اندیشۀ داری نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل نخلۀ طور است چه غوغا کو را</p></div>
<div class="m2"><p>جز سر انا الحق برو باری نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آئینۀ دل را که ز وحدت صافی است</p></div>
<div class="m2"><p>جز کثرت موهوم غباری نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کون و مکان دائرۀ وحدت را</p></div>
<div class="m2"><p>جز نقطۀ دل هیچ مداری نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن سینه که سینای تجلای تو شد</p></div>
<div class="m2"><p>در پرده در پس اختیاری نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بادۀ عشق هر که گردید خراب</p></div>
<div class="m2"><p>بر لاف و گزافش اعتباری نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در بزم شراب و ساقی گلرخ یار</p></div>
<div class="m2"><p>البته امید هوشیاری نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جانا نظری مفتقر کوی ترا</p></div>
<div class="m2"><p>جز فقر و فنادار و نداری نبود</p></div></div>