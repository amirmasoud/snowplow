---
title: >-
    شمارهٔ  ۸۲
---
# شمارهٔ  ۸۲

<div class="b" id="bn1"><div class="m1"><p>عاکفان حرمت قبلۀ اهل کرمند</p></div>
<div class="m2"><p>واقف از نکتۀ سر بستۀ لوح و قلمند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکساران تو ماه فلک ملک حدوث</p></div>
<div class="m2"><p>جان نثاران تو شاه ملکوت قدمند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرقه پوشان تو تشریف ده شاهانند</p></div>
<div class="m2"><p>دُرد نوشان تو ائینه گر جام جمند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بندگان تو ز زندان هوا آزادند</p></div>
<div class="m2"><p>در کمند تو و آسوده ز هر بیش و کمند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانب اهل طریقت به حقارت منگر</p></div>
<div class="m2"><p>زانکه در بادیۀ معرفت اول قدمند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه شورید و ژولیده و بی پا و سرند</p></div>
<div class="m2"><p>لیک در عالم جان صاحب طبل و علمند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناوک غمزۀ من بر دل این غمزدگان</p></div>
<div class="m2"><p>زانکه این سلسله صید حرم و محترمند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نام نام آور این طائفه را میمون دان</p></div>
<div class="m2"><p>زانکه در دفتر ایجاد مبارک قدمند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مفتقر دست تو و دامن آنان که همه</p></div>
<div class="m2"><p>خضرجانند و مسیحانفس و روح‌دمند</p></div></div>