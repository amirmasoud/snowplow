---
title: >-
    شمارهٔ  ۵۳
---
# شمارهٔ  ۵۳

<div class="b" id="bn1"><div class="m1"><p>مذمت عاشقان ز پستی همت است</p></div>
<div class="m2"><p>عشق رخ مهوشان فریضۀ ذمت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز عشق منعم مکن ای ز خدا بی خبر</p></div>
<div class="m2"><p>که نقطۀ مرکز دائرۀ رحمت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مترس از طعنۀ جاهل افعی صفت</p></div>
<div class="m2"><p>که عشق گنجینۀ معرفت و حکمت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نعمت عشق هان مباد غافل شوی</p></div>
<div class="m2"><p>که نقمت بی علاج غفلت از این نعمت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز پرتو نور عشق طور تجلی است دل</p></div>
<div class="m2"><p>چه داند آن کس که در بادیۀ ظلمت است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز عشق بر بند لب مگر ز روی ادب</p></div>
<div class="m2"><p>که حضرت عشق را نهایت حرمت است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صفای عشقت بود کدورت طبع را</p></div>
<div class="m2"><p>که عشق سرچشمۀ طهارت و عصمت است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز زحمت و صدمۀ عشق هراسان مباش</p></div>
<div class="m2"><p>که راحت جاودان در خور این زحمت است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق تو جانا مرا شد ز ازل سرنوشت</p></div>
<div class="m2"><p>که تا ابد مفتقر شاکر از این قسمت است</p></div></div>