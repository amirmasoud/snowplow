---
title: >-
    شمارهٔ  ۱۱۴
---
# شمارهٔ  ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>مائیم مست باده روز الست تو</p></div>
<div class="m2"><p>نی بلکه مست غمزۀ چشمان مست تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما کرده ایم سینه سپر تیغ عشق را</p></div>
<div class="m2"><p>نی بلکه دیده را هدف تیر شست تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما داده ایم سلسلۀ اختیار را</p></div>
<div class="m2"><p>نی بلکه رشتۀ دل و دین را بدست تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما بنده ایم و بستۀ غم توایم</p></div>
<div class="m2"><p>یا رب مرا مباد جدائی ز بست تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما دل شکسته ایم شکستن چه حاجت است</p></div>
<div class="m2"><p>هر چند هست عین درستی شکست تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برخیز ای دلیل دل رهروان که ما</p></div>
<div class="m2"><p>سرگشته ایم در ره عشق از نشست تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما را ز خوی دیو طبیعت نجات ده</p></div>
<div class="m2"><p>ای جان فدای روح حقیقت پرست تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرض نیاز در بر سرو بلند ناز</p></div>
<div class="m2"><p>کی مفتقر رسا است به بالای پست تو</p></div></div>