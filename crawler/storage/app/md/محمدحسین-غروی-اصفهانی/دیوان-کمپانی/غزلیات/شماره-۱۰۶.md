---
title: >-
    شمارهٔ  ۱۰۶
---
# شمارهٔ  ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>سروش غیب دوشم نکته‌ای را گفت در گوشم</p></div>
<div class="m2"><p>که تا صبح قیامت برد تاب از دل ز سر هوشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مناز ای ساقی مجلس به نوشانوش پی در پی</p></div>
<div class="m2"><p>که من از ساغر ابروی شاهد باده می‌نوشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلال چشمۀ حیوان ترا ای خضر ارزانی</p></div>
<div class="m2"><p>که من زان لعل میگون زندۀ سرچشمۀ نوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شراب عشق شوری در سرم افکنده‌ای شیرین</p></div>
<div class="m2"><p>که چون فرهاد تلخی‌های دوران شد فراموشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجب نبود گر از معمورۀ حسن تو ویرانم</p></div>
<div class="m2"><p>ز مینای تو سرمستم ز سینای تو مدهوشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود بار گرانی سر ز سودای تو ای سرور</p></div>
<div class="m2"><p>به قربان سرت گردم بگیر این بار از دوشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منم دستانسرای گلشن وحدت به صد الحان</p></div>
<div class="m2"><p>ولیکن عشوه‌های شاهد گل کرده خاموشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هزاران نکتهٔ باریک‌تر از موی می‌بینم</p></div>
<div class="m2"><p>در آن موی میان، لیکن ز بیم فتنه می‌پوشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشایش گرچه در کوشش بود ای عارف سالک</p></div>
<div class="m2"><p>من این ره را نمی‌پویم در این معنی نمی‌کوشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قمار عشق می‌بازم به یاد دوست می‌نازم</p></div>
<div class="m2"><p>خوشا روزی که بینم آن دلارا را در آغوشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به گردن می‌رسد جوش و خروش مفتقر آری</p></div>
<div class="m2"><p>که چون نی می‌خروشم گاه چون می گاه می‌جوشم</p></div></div>