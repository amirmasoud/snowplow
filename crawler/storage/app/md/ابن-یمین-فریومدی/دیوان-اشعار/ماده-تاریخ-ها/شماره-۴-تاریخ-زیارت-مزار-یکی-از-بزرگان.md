---
title: >-
    شمارهٔ  ۴ - تاریخ زیارت مزار یکی از بزرگان
---
# شمارهٔ  ۴ - تاریخ زیارت مزار یکی از بزرگان

<div class="b" id="bn1"><div class="m1"><p>ز هجرت نبوی رفته بود هفتصد و هشت</p></div>
<div class="m2"><p>دوشنبه از مه شعبان گذشته پانزده روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بنده ابن یمین چشم جان مکحل کرد</p></div>
<div class="m2"><p>بگرد خاک در این مزار جان افروز</p></div></div>