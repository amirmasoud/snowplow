---
title: >-
    شمارهٔ ٨ - وله مستزاد در مدح یمین الدوله عمده الملک حسن
---
# شمارهٔ ٨ - وله مستزاد در مدح یمین الدوله عمده الملک حسن

<div class="b" id="bn1"><div class="m1"><p>یا رب از من که برد سوی خراسان خبری</p></div>
<div class="m2"><p>ایصبا گر بودت هیچ مجال گذری</p></div></div>
<div class="b2" id="bn2"><p>بسوی حضرت دستور ز من</p></div>
<div class="b" id="bn3"><div class="m1"><p>عمده الملک یمین دول و دین که بود</p></div>
<div class="m2"><p>اتفاق همه کور است خصال و سیری</p></div></div>
<div class="b2" id="bn4"><p>همچو نامش بهمه حال حسن</p></div>
<div class="b" id="bn5"><div class="m1"><p>گر ز بار غمت از پای در آمد دل من</p></div>
<div class="m2"><p>دستگیری کن و از عین عنایت نظری</p></div></div>
<div class="b2" id="bn6"><p>بکرم سوی من زار فکن</p></div>
<div class="b" id="bn7"><div class="m1"><p>بگریبان دلم چنگ غم اندر زده ئی</p></div>
<div class="m2"><p>ترسم ای شادی جان زانکه بگیرد سحری</p></div></div>
<div class="b2" id="bn8"><p>بی منت آه دل من دامن</p></div>
<div class="b" id="bn9"><div class="m1"><p>از تو محرومم و این شیوه فضل و هنر است</p></div>
<div class="m2"><p>دور بادا ز من این فضل و نباشد هنری</p></div></div>
<div class="b2" id="bn10"><p>گر مرا دور کند ز اهل وطن</p></div>
<div class="b" id="bn11"><div class="m1"><p>طوطی جان بهوای شکر الفاظت</p></div>
<div class="m2"><p>عزم دارد که گرش باز شود بال و پری</p></div></div>
<div class="b2" id="bn12"><p>بر پرد زین قفس تیره تن</p></div>
<div class="b" id="bn13"><div class="m1"><p>ای ز لفظ تو سخن یافته آن نظم و نسق</p></div>
<div class="m2"><p>که بود نزد خرد در بر او مختصری</p></div></div>
<div class="b2" id="bn14"><p>سلک در عدن و عقد پرن</p></div>
<div class="b" id="bn15"><div class="m1"><p>ز آتش طبع گهر بار تو باد سحری</p></div>
<div class="m2"><p>بعدن گر برد از خاک خراسان خبری</p></div></div>
<div class="b2" id="bn16"><p>در صدف آب شود در عدن</p></div>
<div class="b" id="bn17"><div class="m1"><p>پرتو مشعله رای جهان آرایت</p></div>
<div class="m2"><p>گر کشد شعله بر افلاک شود زو شرری</p></div></div>
<div class="b2" id="bn18"><p>شمع زر پیکر فیروزه لگن</p></div>
<div class="b" id="bn19"><div class="m1"><p>هر کبوتر که بود برج جلالت وطنش</p></div>
<div class="m2"><p>گر کند سوی وطن از سوی گردون سفری</p></div></div>
<div class="b2" id="bn20"><p>ز انجم ثابته چیند ارزن</p></div>
<div class="b" id="bn21"><div class="m1"><p>لطف جان و دل تو هست بحدی که صبا</p></div>
<div class="m2"><p>بختن گر بود از نفحه لطفت اثری</p></div></div>
<div class="b2" id="bn22"><p>خون شود از حسدش مشک ختن</p></div>
<div class="b" id="bn23"><div class="m1"><p>خشمت ار تیغ کشد بر مه و ماهی گه کین</p></div>
<div class="m2"><p>جوشنی گر چه گرفت این یک و آن یک سپری</p></div></div>
<div class="b2" id="bn24"><p>بفکند این سپر و آن جوشن</p></div>
<div class="b" id="bn25"><div class="m1"><p>صاحبا تا سخن ابن یمین مدحت تست</p></div>
<div class="m2"><p>نیست همچون سخن او بحلاوت شکری</p></div></div>
<div class="b2" id="bn26"><p>در مذاق خرد اهل فطن</p></div>
<div class="b" id="bn27"><div class="m1"><p>بر دعا ختم کند مدحت جاهت پس ازین</p></div>
<div class="m2"><p>زانکه بر اوج مدیح تو خورشید فری</p></div></div>
<div class="b2" id="bn28"><p>می نیارد که رسد تیر سخن</p></div>
<div class="b" id="bn29"><div class="m1"><p>دشمن تو بمثل اطلس گردون پوشد</p></div>
<div class="m2"><p>همچو کرم قز اگر چند بگیرد خطری</p></div></div>
<div class="b2" id="bn30"><p>کسوت اول او باد کفن</p></div>