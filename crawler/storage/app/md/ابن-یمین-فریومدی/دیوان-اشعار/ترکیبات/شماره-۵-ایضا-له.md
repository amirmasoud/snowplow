---
title: >-
    شمارهٔ ۵ - ایضاً له
---
# شمارهٔ ۵ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>تا ابروی تو بدلربائی</p></div>
<div class="m2"><p>انگشت نمای چون هلال است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارد ز جمال تو خجالت</p></div>
<div class="m2"><p>خورشید که مظهر جمالست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای از تو جهان حسن آباد</p></div>
<div class="m2"><p>ایزد همه حسنها ترا داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عین کمال در امان باد</p></div>
<div class="m2"><p>حسنت که بغایت کمال است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک صبحدم ای نسیم خوشبوی</p></div>
<div class="m2"><p>بگذر بسوی نگار و بر گوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کز مویه تنم شدست چون موی</p></div>
<div class="m2"><p>وز ناله زار همچو نال است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مائیم ز عشقت ای پریوش</p></div>
<div class="m2"><p>با چشم و دلی پر آب و آتش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از هجر تو نیست زندگی خوش</p></div>
<div class="m2"><p>باز آی که نوبت وصال است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل کز تو صبور گشت یارا</p></div>
<div class="m2"><p>دل نیست که هست سنگ خارا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر هست ترا شکیب ما را</p></div>
<div class="m2"><p>باری ز تو صابری محال است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتیم خیال چون تو ماهی</p></div>
<div class="m2"><p>بینیم بخواب گاهگاهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لیک از غم چون تو دلپناهی</p></div>
<div class="m2"><p>خواب آیدم این هوس خیال است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر ابن یمین ز عشق رویت</p></div>
<div class="m2"><p>بر باد شود چو خاک کویت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیرون نپرد ز دام مویت</p></div>
<div class="m2"><p>مرغ دل او که بسته بال است</p></div></div>