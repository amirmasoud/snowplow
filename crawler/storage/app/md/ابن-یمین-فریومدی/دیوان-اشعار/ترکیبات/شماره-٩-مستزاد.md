---
title: >-
    شمارهٔ ٩ - مستزاد
---
# شمارهٔ ٩ - مستزاد

<div class="b" id="bn1"><div class="m1"><p>بر آینه سرمه سیه ریخته بین</p></div>
<div class="m2"><p>طوطی روان بحلق آویخته بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان سنبل تر</p></div>
<div class="m2"><p>ز انشاخ شکر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نرگس مست را برانگیخت ز خواب</p></div>
<div class="m2"><p>سبحان الله فتنه انگیخته بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میگفت دلم</p></div>
<div class="m2"><p>در دور قمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افسوس که عمر رفت بر بوک و مکر</p></div>
<div class="m2"><p>از رفته دگر چه سود امبار دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توفیق خدا باشد</p></div>
<div class="m2"><p>یابیم مراد دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرهست دلم شاد غمم نیست ازین</p></div>
<div class="m2"><p>کاصحاب خرد را بهمه حال نظر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر حکم قضا باشد</p></div>
<div class="m2"><p>در غی و رشاد دل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای آنک دل از تو بر نگیرم هرگز</p></div>
<div class="m2"><p>زان یاد همی دار که با من بستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میدان</p></div>
<div class="m2"><p>پیمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دریاب که جان رسیدست بلب</p></div>
<div class="m2"><p>لب بر لب من نه ایصنم تا بینند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بشتاب</p></div>
<div class="m2"><p>اصحاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وه وه که دلم ز آتش عشق تو بسوخت</p></div>
<div class="m2"><p>ای آب روان بلطف مانند میی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آری و الله</p></div>
<div class="m2"><p>بی اکراه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کفتم ز سر لطف که ای طرف پسر</p></div>
<div class="m2"><p>زان پیش که مور صف کشد گردشکر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خواهم نفسی ز صحبتت آسودن</p></div>
<div class="m2"><p>دانم که رضا داری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از شام شراب دادنت تا بسحر</p></div>
<div class="m2"><p>وانگه که فرو شد از سر مستی سر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گویم که میان ما چه خواهد بودن</p></div>
<div class="m2"><p>آئین وفا داری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای روی تو بر سپهر خوبی ماهی</p></div>
<div class="m2"><p>از بند غمت بر غم هر بد خواهی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آزادم کن</p></div>
<div class="m2"><p>دانم غم عشقت دل من شاد کند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا کی ز غم عشق تو هم گه گاهی</p></div>
<div class="m2"><p>دلشادم کن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای چشم سیاه تو بلای دل من</p></div>
<div class="m2"><p>مشتاق تو شد دلم برای دل من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برخیز و بیا</p></div>
<div class="m2"><p>گر خواجه ترا روزها رها می نکند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آخر شبکی بهر رضای دل من</p></div>
<div class="m2"><p>بگریز و بیا</p></div></div>