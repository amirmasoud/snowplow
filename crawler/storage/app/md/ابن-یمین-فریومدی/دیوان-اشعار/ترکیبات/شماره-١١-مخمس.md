---
title: >-
    شمارهٔ ١١ - مخمس
---
# شمارهٔ ١١ - مخمس

<div class="b" id="bn1"><div class="m1"><p>تا چند عمر خود بجوانان بسر کنیم</p></div>
<div class="m2"><p>من بعد ما ز عشق مجازی حذر کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سینه هر چه هست ز غیرش بدر کنیم</p></div>
<div class="m2"><p>ایدل بیا ز دنیی و عقبی گذر کنیم</p></div></div>
<div class="b2" id="bn3"><p>با یاد دوست از همه قطع نظر کنیم</p></div>
<div class="b" id="bn4"><div class="m1"><p>آن قادریکه خیل ملک از جلال اوست</p></div>
<div class="m2"><p>چندین هزار گوی و مگوی از سئوال اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ارض و سما و عرش ز وصف کمال اوست</p></div>
<div class="m2"><p>ذرات کاینات حجاب جمال اوست</p></div></div>
<div class="b2" id="bn6"><p>آهی کشیم و آنهمه زیر و زبر کنیم</p></div>
<div class="b" id="bn7"><div class="m1"><p>آن پیر پاک در صدف اینچنین بسفت</p></div>
<div class="m2"><p>ذکر حدیث لعل ترا در دلش نهفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هجر ترا کشید بهر کس سخن نگفت</p></div>
<div class="m2"><p>گفتم که چیست حال منت تا ابد بگفت</p></div></div>
<div class="b2" id="bn9"><p>با وعده وصال ترا در بدر کنیم</p></div>
<div class="b" id="bn10"><div class="m1"><p>با ما ز لطف خویش سفر وعده کرد یار</p></div>
<div class="m2"><p>نقد صفا ز نخل ثمر وعده کرد یار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکحرف ازان دو لعل شکر وعده کرد یار</p></div>
<div class="m2"><p>دیدار خود بملک دگر وعده کرد یار</p></div></div>
<div class="b2" id="bn12"><p>خیزید عاشقان همه عزم سفر کنیم</p></div>
<div class="b" id="bn13"><div class="m1"><p>در راه دوست اینهمه سوز و گداز چیست</p></div>
<div class="m2"><p>مستغرق جمال ترا از نماز چیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اندر نماز عشق مجازی نیاز چیست</p></div>
<div class="m2"><p>ابن یمین حکایت دور و دراز چیست</p></div></div>
<div class="b2" id="bn15"><p>عشقست هر چه هست سخن مختصر کنیم</p></div>