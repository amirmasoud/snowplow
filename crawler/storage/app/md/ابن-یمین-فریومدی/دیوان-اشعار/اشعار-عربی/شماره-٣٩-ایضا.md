---
title: >-
    شمارهٔ ٣٩ - ایضاً
---
# شمارهٔ ٣٩ - ایضاً

<div class="b" id="bn1"><div class="m1"><p>رایت العلم یرفع فی السمو</p></div>
<div class="m2"><p>ولم ار کالتواضع فی العلو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و من بسط اللسان علی سفیه</p></div>
<div class="m2"><p>فقد دفع السلاح الی العدو</p></div></div>