---
title: >-
    شمارهٔ ١٩ - ایضاً
---
# شمارهٔ ١٩ - ایضاً

<div class="b" id="bn1"><div class="m1"><p>ولم أدخل الحمام من أجل زینه</p></div>
<div class="m2"><p>فکیف و نار الشوق بین جوارحی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واکلنی لم یکفی فیض عبرتی</p></div>
<div class="m2"><p>دخلت لا بکی من جمیع جوارحی</p></div></div>