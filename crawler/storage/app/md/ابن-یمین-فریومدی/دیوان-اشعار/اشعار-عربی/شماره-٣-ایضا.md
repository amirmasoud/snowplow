---
title: >-
    شمارهٔ ٣ - ایضاً
---
# شمارهٔ ٣ - ایضاً

<div class="b" id="bn1"><div class="m1"><p>ثلاث هن فی البطیخ فضل</p></div>
<div class="m2"><p>و فی الانسان منقصه و ذله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خشونه جلده و الثقل فیه</p></div>
<div class="m2"><p>و صفره لونه من غیر عله</p></div></div>