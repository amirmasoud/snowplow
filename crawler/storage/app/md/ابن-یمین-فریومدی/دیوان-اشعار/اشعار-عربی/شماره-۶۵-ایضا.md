---
title: >-
    شمارهٔ ۶۵ - ایضاً
---
# شمارهٔ ۶۵ - ایضاً

<div class="b" id="bn1"><div class="m1"><p>الهی انت خلاق البرایا</p></div>
<div class="m2"><p>و وهاب النهاب بلا امتنان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انلنی فی الدنیی عرضا مصونا</p></div>
<div class="m2"><p>برغم الحاسدین بلا هوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و لا تشمت عداتی و کن بی</p></div>
<div class="m2"><p>حفیظا من تصاریف الزمان</p></div></div>