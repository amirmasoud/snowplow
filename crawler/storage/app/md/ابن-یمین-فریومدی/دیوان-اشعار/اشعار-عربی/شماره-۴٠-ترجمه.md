---
title: >-
    شمارهٔ ۴٠ - ترجمه
---
# شمارهٔ ۴٠ - ترجمه

<div class="b" id="bn1"><div class="m1"><p>علم را دیدم و تواضع را</p></div>
<div class="m2"><p>کان بزرگی و این بلندی داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانکه بر بیخرد زبان بگشاد</p></div>
<div class="m2"><p>خصم را ساز جنگ پیش نهاد</p></div></div>