---
title: >-
    شمارهٔ ٧۴ - ایضاً
---
# شمارهٔ ٧۴ - ایضاً

<div class="b" id="bn1"><div class="m1"><p>آتیک یا مولای بالامس زایرا</p></div>
<div class="m2"><p>فما انت مطلوبی و قد انا خائبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رجعت و قلبی یعلم الله ضیق</p></div>
<div class="m2"><p>ترکت هناک الروح عنی نائبا</p></div></div>