---
title: >-
    شمارهٔ ٢٠ - ترجمه
---
# شمارهٔ ٢٠ - ترجمه

<div class="b" id="bn1"><div class="m1"><p>مرا گریه چشم کافی چو نیست</p></div>
<div class="m2"><p>در اندوه لعل مرصع به یشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگرمابه از بهر آن میروم</p></div>
<div class="m2"><p>که تا گریدم جمله اعضا چو چشم</p></div></div>