---
title: >-
    شمارهٔ ٢۵ - ایضاً
---
# شمارهٔ ٢۵ - ایضاً

<div class="b" id="bn1"><div class="m1"><p>لا تودع السر الا عندذی کرم</p></div>
<div class="m2"><p>و السر عند کرام الناس مکتوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و السر عندی فی بیت له غلق</p></div>
<div class="m2"><p>قد ضاع مفتاحه و الباب مختوم</p></div></div>