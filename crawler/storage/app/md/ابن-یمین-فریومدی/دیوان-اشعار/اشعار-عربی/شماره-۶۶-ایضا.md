---
title: >-
    شمارهٔ ۶۶ - ایضاً
---
# شمارهٔ ۶۶ - ایضاً

<div class="b" id="bn1"><div class="m1"><p>تزهدت فی الدنیا الدنیه کلها</p></div>
<div class="m2"><p>فمالی سوی نیل المعالی مطالب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشقت المعالی و التکرم مذهبی</p></div>
<div class="m2"><p>و للناس فیما یعشقون مذاهب</p></div></div>