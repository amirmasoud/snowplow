---
title: >-
    شمارهٔ ۵٢ - ترجمه
---
# شمارهٔ ۵٢ - ترجمه

<div class="b" id="bn1"><div class="m1"><p>ترا ایزد چو بر دشمن ظفر داد</p></div>
<div class="m2"><p>بکام دوستانش سر جدا کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و گر خواهی مرام نیک مردان</p></div>
<div class="m2"><p>طمع از جان ببر او را رها کن</p></div></div>