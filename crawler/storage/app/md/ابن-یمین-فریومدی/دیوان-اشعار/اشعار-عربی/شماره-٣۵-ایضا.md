---
title: >-
    شمارهٔ ٣۵ - ایضاً
---
# شمارهٔ ٣۵ - ایضاً

<div class="b" id="bn1"><div class="m1"><p>بنت کرم ابکارها امها</p></div>
<div class="m2"><p>واهانو ها رؤس بالقدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ثم عادوا حکموها بینهم</p></div>
<div class="m2"><p>ویلهم من جور مظلوم حکم</p></div></div>