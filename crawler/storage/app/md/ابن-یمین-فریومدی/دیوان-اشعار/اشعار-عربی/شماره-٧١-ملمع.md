---
title: >-
    شمارهٔ ٧١ - ملمع
---
# شمارهٔ ٧١ - ملمع

<div class="b" id="bn1"><div class="m1"><p>قد اشرقت القلوب بالانوار</p></div>
<div class="m2"><p>اذ نورت الریاض بالانوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبل بصبوح از سر مستی میگفت</p></div>
<div class="m2"><p>در موسم گل حرام شد هشیاری</p></div></div>