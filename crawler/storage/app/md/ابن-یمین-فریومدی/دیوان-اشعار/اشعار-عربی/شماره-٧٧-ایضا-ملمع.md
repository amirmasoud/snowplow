---
title: >-
    شمارهٔ ٧٧ - ایضاً ملمع
---
# شمارهٔ ٧٧ - ایضاً ملمع

<div class="b" id="bn1"><div class="m1"><p>خداوندا بحق ان کرامت</p></div>
<div class="m2"><p>که ما را در ازل کردی گرامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنزدیک ملایک نفس ما شد</p></div>
<div class="m2"><p>بتعلیم اسامی از تو سامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ما نا دیده اسحتقاق احسان</p></div>
<div class="m2"><p>لقد اعطیتنا فوق المرام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا کافتاد عقد صحت ذات</p></div>
<div class="m2"><p>ز دستان فلک در بی نظامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز لطف خود بدینمعنی نگه کن</p></div>
<div class="m2"><p>و بدل حال سقمی بالسلام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اذا أبدئت بالاحسان تمم</p></div>
<div class="m2"><p>فما الاحسان الا بالتمام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نام نیک نیزم هم بمیران</p></div>
<div class="m2"><p>بود عمر مخلد نیکنامی</p></div></div>