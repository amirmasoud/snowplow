---
title: >-
    شمارهٔ ١٠ - مثنوی دیگر
---
# شمارهٔ ١٠ - مثنوی دیگر

<div class="b" id="bn1"><div class="m1"><p>طلب تا محرم اسرار کردی</p></div>
<div class="m2"><p>بآن مطلوب یار غار کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طلب کن تا خبر از دوست یابی</p></div>
<div class="m2"><p>وجود دوست را بی پوست یابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طلب کن تا خبر از گنج یابی</p></div>
<div class="m2"><p>تو کی این گنج را بی رنج یابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طلب چون رخت هستی میرهاند</p></div>
<div class="m2"><p>طلب کس را بمنزل میرساند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طلب باشد براق عشق جانان</p></div>
<div class="m2"><p>طلب باشد سواد اعظم جان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طلب سرمایه گنج وجودست</p></div>
<div class="m2"><p>طلب پیرایه اصل شهود است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو آن مطلوب را در خود طلب کن</p></div>
<div class="m2"><p>چو در خود یافتی دیگر طرب کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجب گنجیست در ویرانه تو</p></div>
<div class="m2"><p>همیشه همدم و همخانه تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر در جستجوی او شتابی</p></div>
<div class="m2"><p>تو او را عاقبت در خود بیابی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که هر کس طالب آن رو بگردد</p></div>
<div class="m2"><p>چو پروانه بگرد او بگردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمانی از تو او هرگز جدا نیست</p></div>
<div class="m2"><p>بجز تو با کسی هم آشنا نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برون از خود تو آنمطلب نجوئی</p></div>
<div class="m2"><p>ره بیهوده در عالم نپوئی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو کردی عاشقی با دوست آغاز</p></div>
<div class="m2"><p>دگر با عشق او میسوز و میساز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدنیا از تف هجران نسوزد</p></div>
<div class="m2"><p>بعقبی ز آتش نیران نسوزد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگفتا عاشقانرا میکشم من</p></div>
<div class="m2"><p>پس از کشتن دیت هم میکشم من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برای یک دیت صد بار کشتن</p></div>
<div class="m2"><p>مرا خوشتر بود از زنده گشتن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر صد بار عاشق کشته گردد</p></div>
<div class="m2"><p>بخون خویشتن آغشته گردد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز خون من اگر گلها بروید</p></div>
<div class="m2"><p>بخونش خونبها هرگز نجوید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه در کعبه نه در دیر مغانست</p></div>
<div class="m2"><p>ولیکن در میان جان نهانست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه در تست این مطلوب حاصل</p></div>
<div class="m2"><p>چه حاصل چون علایق گشت فاصل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرا بس آن نگاه گاهگاهت</p></div>
<div class="m2"><p>هزاران جان فدای یک نگاهت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا جز کشتنت راحت نباشد</p></div>
<div class="m2"><p>بدیت ماه من حاجت نباشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو او را یافتی عاشق شو اکنون</p></div>
<div class="m2"><p>بوصف دلبری لایق شو اکنون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نترسی زانکه خواهی کشته گشتن</p></div>
<div class="m2"><p>بخون خویشتن آغشته گشتن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه کشتن باشد این خود زندگانیست</p></div>
<div class="m2"><p>بقاهای حیات جاودانیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرا جز کشتنت افنا ضروریست</p></div>
<div class="m2"><p>که عاشق را فنا کشتن ضروریست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>منم پیدا و هم سر نهانت</p></div>
<div class="m2"><p>منم هم مغز تو هم استخوانت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دلارامی نکو خوئی چه رادی</p></div>
<div class="m2"><p>چه میورزی بعاشق اتحادی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همی گویم منم گوش و زبانت</p></div>
<div class="m2"><p>منم جان و تن و روح و روانت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز سر تا پای تو من گشتم ایدوست</p></div>
<div class="m2"><p>همه اعضای تو من گشتم ایدوست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرا از تو گزیر و چاره ئی نیست</p></div>
<div class="m2"><p>مرا همچون تو یک غمخواره ئی نیست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بیا بار دگر همراه باشیم</p></div>
<div class="m2"><p>همیشه همدم دلخواه باشیم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرا با تو چکار آید تن و جان</p></div>
<div class="m2"><p>مرا بی تو نباید باغ و بستان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>توئی باغ بهار و لاله زارم</p></div>
<div class="m2"><p>به آئینهای تو کاری ندارم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>توئی آئینه حسن جمالم</p></div>
<div class="m2"><p>توئی نوشنده بزم وصالم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تجلی رخت با من عیانست</p></div>
<div class="m2"><p>ولی از دیده هرکس نهانست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر چه اهل عالم خیل خیلست</p></div>
<div class="m2"><p>توئی مقصود و دیگرها طفیلست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>درین عالم اگر آدم نبودی</p></div>
<div class="m2"><p>تجلی در همه عالم نبودی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چه قربست اینکه در عالم چه قربست</p></div>
<div class="m2"><p>که عارف مست از میدان قربست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کشیده باده و صهبا ندیده</p></div>
<div class="m2"><p>خدا را دیده و خود را ندیده</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جز از وی در نهاد او دگر نیست</p></div>
<div class="m2"><p>سر موئی ز خود او را خبر نیست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چه خوش بزمی که جز جانان نباشد</p></div>
<div class="m2"><p>درین محرم بغیر از جان نباشد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جمال یار اگر برقع گشاید</p></div>
<div class="m2"><p>بتو بیتو جمال خود نماید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همه عالم جمال یار بینی</p></div>
<div class="m2"><p>جهانرا خالی از اغیار بینی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بساط قرب سلطان آتشین است</p></div>
<div class="m2"><p>بسوزد هر که با او همنشین است</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>الهی آتش قربی بر افروز</p></div>
<div class="m2"><p>تمام هستی ما را در او سوز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مرا بیتو چو این هستی نپاید</p></div>
<div class="m2"><p>چو هستی تو مرا هستی نباید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کسی داند که او معشوقبازست</p></div>
<div class="m2"><p>و بی یسمع و بی یبصر چه رازست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو شد آن گنج پنهان آشکارا</p></div>
<div class="m2"><p>ازو پر شد همه دریا و صحرا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چگویم شرح این دور و درازست</p></div>
<div class="m2"><p>مگر اینها از آن معشوقبازست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>وجود خویش را زانگنج پر ساز</p></div>
<div class="m2"><p>همه انبان خود را لعل و در ساز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>جهان پر گنج و این افلاس از چیست</p></div>
<div class="m2"><p>نمیدانم چرا این گنج مخفیست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که هر کس قیمت گوهر نداند</p></div>
<div class="m2"><p>سفه چون قدر مال و زر نداند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نهان در تست این گنج گرانسنگ</p></div>
<div class="m2"><p>تو خود را نیک کاو ایمرد دلتنگ</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>عجب گنجیست در ویرانه تو</p></div>
<div class="m2"><p>عجب جانی بود جانانه تو</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>فرو رفته همه در عین آن گنج</p></div>
<div class="m2"><p>ولی کس را وقوفی نیست بی رنج</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>اگر چه کرد با خود بس مدارا</p></div>
<div class="m2"><p>ولیکن گشت آخر بس مدا را</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>حدیث کنت کنزا را شنیدی</p></div>
<div class="m2"><p>ازین گنج نهان بر گو چه دیدی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>عجب گنجیست گنج جاودانه</p></div>
<div class="m2"><p>که او را نه میان و نی کرانه</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>درین کان هر که افتد کان شود او</p></div>
<div class="m2"><p>اگر جسمیست آخر جان شود او</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>فتاده تا ابد سر مست و قانع</p></div>
<div class="m2"><p>چشیده زین می پر شور و نافع</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ازین می گر تو هم خواهی چشیدن</p></div>
<div class="m2"><p>تو هم خواهی بیکجائی رسیدن</p></div></div>