---
title: >-
    شمارهٔ ۴ - چهار پند نوشیروان
---
# شمارهٔ ۴ - چهار پند نوشیروان

<div class="b" id="bn1"><div class="m1"><p>شنیدم که کسری یکی فرش داشت</p></div>
<div class="m2"><p>بر آن فرش هر گونه پندی نگاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخست آنکه دنیا نجوید کسی</p></div>
<div class="m2"><p>که داند بدو در نماند بسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوم آنکه سودی ندارد حذر</p></div>
<div class="m2"><p>زکاریکه رفتست اندر قدر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوم آنک دانای خالق شناس</p></div>
<div class="m2"><p>ز مخلوق بر سر نگیرد سپاس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چهارم چو روزی مقدر شدست</p></div>
<div class="m2"><p>چرا مرد آزاده چاکر شدست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر بگذری زینسخن راه نیست</p></div>
<div class="m2"><p>روان تو از دانش آگاه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شنیدم که روزی منوچهر شاه</p></div>
<div class="m2"><p>بپرسید از مهتری نیکخواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که در عالم از هر چه هست ارجمند</p></div>
<div class="m2"><p>چه چیزست شایسته و دلپسند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز گفتار گفت حکیمان بهست</p></div>
<div class="m2"><p>ز کردار کرد کریمان بهست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ازین گفت وزین کرد اگر بگذری</p></div>
<div class="m2"><p>نیابی خرد را جز این داوری</p></div></div>