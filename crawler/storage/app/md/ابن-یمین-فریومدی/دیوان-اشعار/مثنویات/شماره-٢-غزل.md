---
title: >-
    شمارهٔ ٢ - غزل
---
# شمارهٔ ٢ - غزل

<div class="b" id="bn1"><div class="m1"><p>فلک با ما سر یاری ندارد</p></div>
<div class="m2"><p>بجز میل دلا زاری ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز پهلوی من این گردون بیمهر</p></div>
<div class="m2"><p>جز آهنگ جگر خواری ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه بیدادست یا رب چرخ گردان</p></div>
<div class="m2"><p>که کاری جز ستمکاری ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم از وی چو زلف و چشم خوبان</p></div>
<div class="m2"><p>بجز تشویش و بیماری ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود در نظم کارم بس گرانجان</p></div>
<div class="m2"><p>اگر چه جز سبکساری ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازان چون ابر گریانم که چون رعد</p></div>
<div class="m2"><p>دلم جز ناله و زاری ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرانجانی بختم بین که از وی</p></div>
<div class="m2"><p>خرد امید بیداری ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلا آخر ز گردون چند پرسی</p></div>
<div class="m2"><p>که بر دردم دوا داری ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فلک را هیچ اگر آزرم باشد</p></div>
<div class="m2"><p>عزیزانرا بدین خواری ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز هجر دوستان ابن یمین را</p></div>
<div class="m2"><p>بدشمنکامی و زاری ندارد</p></div></div>
<div class="n" id="bn11"><p>بدشمنکامی و زاری ندارد</p></div>