---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>ای قبله صاحبنظران روی چو ماهت</p></div>
<div class="m2"><p>وی فتنه دوران قمر چشم سیاهت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد خار نهد حسن تو خورشید فلک را</p></div>
<div class="m2"><p>چون از گل سیراب دمد مهر گیاهت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترسم که نشان بر رخ زیبات بماند</p></div>
<div class="m2"><p>از غایت لطف ار کنم از دور نگاهت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عذر گنه شیفتگان روز قیامت</p></div>
<div class="m2"><p>روشن شود ایحوروش از روی چو ماهت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر درد دل ابن یمین ناله گواهست</p></div>
<div class="m2"><p>خود بر دل من بنده چه حاجت بگواهت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درد دل ما را بلب لعل دوا کن</p></div>
<div class="m2"><p>در گردنم ار باشد ازین هیچ گناهت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایدل مکن آه ستم از وی که ندارد</p></div>
<div class="m2"><p>آئینه حسن بت من طاقت آهت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر میطلبی از ستمش روی خلاصی</p></div>
<div class="m2"><p>جز حضرت نوئین جهان نیست پناهت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نوئین فلک مرتبه تالش که بدارد</p></div>
<div class="m2"><p>اندر کنف معدلت خویش نگاهت</p></div></div>