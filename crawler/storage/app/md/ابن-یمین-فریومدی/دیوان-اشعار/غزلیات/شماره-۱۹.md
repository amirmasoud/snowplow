---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>روی شهر آر ای یارم گر نبودی آفتاب</p></div>
<div class="m2"><p>کی شدی از دیدن او دیده مشتاق آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون گشاید از کمین ترک کمان ابرو خدنگ</p></div>
<div class="m2"><p>بر غرض دارد گذر همچون دعای مستجاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاب رخسار چو ماه او همی سوزد مرا</p></div>
<div class="m2"><p>سوزد آری تار کتانرا فروغ ماهتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاد میگردم چو دلبر میکند با من عتاب</p></div>
<div class="m2"><p>ز آنکه باشد دوستی بر جای تا باشد عتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلگشای و غم زدا آمد هوای یار من</p></div>
<div class="m2"><p>چون صبوح اندر بهار و همچو عمر اندر شباب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خط شیرین تو چون سبزه بر آب روان</p></div>
<div class="m2"><p>زلف مشکینت میانش سایبان بر آفتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر بپایت درفکند ابن یمین از شوق و گفت</p></div>
<div class="m2"><p>این که میبینم به بیداریست یا رب یا بخواب</p></div></div>