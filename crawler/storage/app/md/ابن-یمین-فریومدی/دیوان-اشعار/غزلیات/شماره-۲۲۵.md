---
title: >-
    شمارهٔ ۲۲۵
---
# شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>ای قاعده زلفت آشوب جهان بودن</p></div>
<div class="m2"><p>وی رسم لب لعلت آسایش جان بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در باغ چو بخرامی جانم بهوس خواهد</p></div>
<div class="m2"><p>در پای سهی سروت چون آب روان بودن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بهر شکار لد گشتست ترا آئین</p></div>
<div class="m2"><p>از غمزه و از ابرو با تیرو کمان بودن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو کان چو بود زلفت کار دل من باشد</p></div>
<div class="m2"><p>در عرصه میدانت چون گوی روان بودن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایجان و جهان من جز لطف تو نفزاید</p></div>
<div class="m2"><p>در غایت پیدائی از خلق نهان بودن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون دل مشتاقان خوردست لب لعلت</p></div>
<div class="m2"><p>سرخست لبت اینک منکر نتوان بودن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با ما نه چنین بودی زین پیش مکن جانا</p></div>
<div class="m2"><p>کز تو نبود لایق با ما نه چنان بودن</p></div></div>