---
title: >-
    شمارهٔ ۲۵۸
---
# شمارهٔ ۲۵۸

<div class="b" id="bn1"><div class="m1"><p>هستم بجستجوی تو پوینده کو بکو</p></div>
<div class="m2"><p>باشد که با توأم فتد از دست روبرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشقت درید پیرهن صبر من چنانک</p></div>
<div class="m2"><p>نتوان بدست عقل توان کردنش رفو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بگذری بشهر ز غوغای عاشقان</p></div>
<div class="m2"><p>سیلاب خون روان شود اندر چهار سو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من از تو دور و با تو رقیبست همنشین</p></div>
<div class="m2"><p>هست این ز روزگار که بادا برو تفو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آبحیات خضر خطت بهره میبرد</p></div>
<div class="m2"><p>من جان همی دهم چو سکندر در آرزو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم تنم فدای میان تو گشت گفت</p></div>
<div class="m2"><p>دیریست تا بدیده ام اینکار مو بمو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایزد گناه ابن یمین را جو عذر او</p></div>
<div class="m2"><p>روشن ز روی تست کند بی‌گمان عفو</p></div></div>