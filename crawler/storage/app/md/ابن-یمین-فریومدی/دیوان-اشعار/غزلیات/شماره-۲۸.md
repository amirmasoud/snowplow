---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>بفروغ مهر رویش که مهست از آن عبارت</p></div>
<div class="m2"><p>بفریب چشم مستش که دهد جهان بغارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنسیم جانفزایش که مسیح وار آرد</p></div>
<div class="m2"><p>سوی کشتگان هجران بوصال جان بشارت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که دمد بجای خار از سرخاک ما گل تر</p></div>
<div class="m2"><p>چون کند مزار ما را مه مهربان زیارت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر از بهار حسنش بچمن رسد نسیمی</p></div>
<div class="m2"><p>عجب ار نگیرد از سر بگه خزان نضارت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخط و عبارت او مر ساد چشم زخمی</p></div>
<div class="m2"><p>که ندید کس چنان خط نشنید از آن عبارت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز سر شک تر خرابست بنای صبر و شاید</p></div>
<div class="m2"><p>که کسی میان طوفان ندهد نشان عمارت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پسر یمین ز پایش بچه روی سر بتابد</p></div>
<div class="m2"><p>چو بآشکار دورش کند و نهان اشارت</p></div></div>