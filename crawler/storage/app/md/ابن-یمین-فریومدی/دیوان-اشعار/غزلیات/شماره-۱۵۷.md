---
title: >-
    شمارهٔ ۱۵۷
---
# شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>باد بهاری وزید از طرف جویبار</p></div>
<div class="m2"><p>بر سر میخوارگان کرد بدامن نثار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بن شاخ سمن بیضه کافور ناب</p></div>
<div class="m2"><p>وز سر سرو سهی نافه مشک تتار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر صبوحی زنان قمری مقری صفت</p></div>
<div class="m2"><p>کرد ندا صبحدم بر سر بید و چنار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کایدل اگر آگهی چون گل خیری شکفت</p></div>
<div class="m2"><p>پای ز گلشن مکش دست ز ساغر مدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد در آمد بدشت لاله بر آمد بکوه</p></div>
<div class="m2"><p>هر که شد آگه ازین هر دو بفصل بهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت که عیسی بلطف دم بجهان در دمید</p></div>
<div class="m2"><p>و آتش موسی بتافت از طرف کوهسار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فصل بهار ار بود طبع هوا معتدل</p></div>
<div class="m2"><p>پس بچمن از چه روی نوع دگر گشت کار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غمزه نرگس چرا یافت ز صفرا اثر</p></div>
<div class="m2"><p>طره سنبل چرا ساخت ز سودا شعار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ابن یمین صبحدم باده گلگون بکف</p></div>
<div class="m2"><p>شد بتماشای گل با صنم گلعذار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از سر گلبن شنید غلغل بلبل که گفت</p></div>
<div class="m2"><p>عیش و طرب کن که نیست خوشتر از این روزگار</p></div></div>