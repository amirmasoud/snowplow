---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>بگوش جان من آید دمادم آوازی</p></div>
<div class="m2"><p>که هست طایر جانرا هوای پروازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلی نشیمن او شاخسار سدره بود</p></div>
<div class="m2"><p>چه میکند قفسی و اندرو نه دمسازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعقل و علم اگر پرورش دهی جانرا</p></div>
<div class="m2"><p>ز سر غیب نماید بر او نهان رازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجردی چو مسیحا کجا که از سر وقت</p></div>
<div class="m2"><p>بهر نفس که بر آرد نماید اعجازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غذای طوطی جان تو شکر خرد است</p></div>
<div class="m2"><p>عزیز دار مر او را که ارزد اعزازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود ز جهل گرش آرزوی نفس دهی</p></div>
<div class="m2"><p>کسی بطعمه نداد ارزنی بشهبازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنزد ابن یمین کر چو مار خاک خوری</p></div>
<div class="m2"><p>بهست از آنکه چو موری مسخر آزی</p></div></div>