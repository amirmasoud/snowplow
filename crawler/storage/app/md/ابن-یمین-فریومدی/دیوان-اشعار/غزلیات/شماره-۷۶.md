---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>لعل شیرین تو پیرایه در عدن است</p></div>
<div class="m2"><p>زلف پر چین تو سرمایه مشک ختن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو تا بنده بالای تو شد از دل پاک</p></div>
<div class="m2"><p>بر زبان همه آزادی سرو چمن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر زنخدان تو چاهیست که دل یوسف اوست</p></div>
<div class="m2"><p>جانم اندر غم او ساکن بیت الحزن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نبندد کمر آن ماه سرافراز بناز</p></div>
<div class="m2"><p>ور نگوید سخن آن پسته که شور زمن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بچه دانند که دلدار مرا هست میان</p></div>
<div class="m2"><p>بچه معلوم توان کرد که او را دهن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لطف آنخال سیاه و رخ چون ماهش بین</p></div>
<div class="m2"><p>نقطه عنبر تر بر ورق نسترن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست ممکن که ازو صبر کنم ز آنک مرا</p></div>
<div class="m2"><p>نور در چشم و فرح در دل و جان در بدن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حاصل از زلف وی این دارد و بس ابن یمین</p></div>
<div class="m2"><p>که دل آشفته و سرگشته و با صد شکن است</p></div></div>