---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>ندانم صبغه الله است یا گلگون شرابست این</p></div>
<div class="m2"><p>چنین رنگین نباشد می مگر لعل مذابست این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ابریق ار سوی ساغر روان گردد می روشن</p></div>
<div class="m2"><p>ز بهر دفع دیو غم تو پنداری شهابست این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حباب از روی جام می چو بدرخشد خرد گوید</p></div>
<div class="m2"><p>که بر خورشید رخشنده سهیل تیز تابست این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشا در نصفی سیمین می روشن چو آب زر</p></div>
<div class="m2"><p>تو پنداری هلالست آن و در وی آفتابست این</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو روی ساقی مهوش ز تاب می عرق گیرد</p></div>
<div class="m2"><p>زرنگ و بوی پنداری مگر بر گل گلابست این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز ساقی خواستم آبی شرابی داد گلگونم</p></div>
<div class="m2"><p>بلطفش گفتم ایدلبر شرابست این نه آبست این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگفت ابن یمین بستان که آبست این ولی در وی</p></div>
<div class="m2"><p>فتاد از روی من عکسی تو پنداری شرابست این</p></div></div>