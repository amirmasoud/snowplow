---
title: >-
    شمارهٔ ۱۸۶
---
# شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>گرم ز عشق تو جان در بلاست گو میباش</p></div>
<div class="m2"><p>و گر چه با منت آئین جفاست گو میباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا که گر گذرت بر دو چشم من باشد</p></div>
<div class="m2"><p>چو خاک پای توأم توتیاست گو میباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه در بر کافور عارضت افعی است</p></div>
<div class="m2"><p>ترا چو لعل زمرد نماست گو میباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بچین زلف چو شام تو مایلست دلم</p></div>
<div class="m2"><p>شدن بجانب چین گر خطاست گو میباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه از من مسکین رقیب در خشم است</p></div>
<div class="m2"><p>ترا نظر چو بعین رضاست گو میباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان لیلی و مجنون مودتست و صفا</p></div>
<div class="m2"><p>اگر میان عرب ماجراست گو میباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بهر دانه خالت همیشه مرغ دلم</p></div>
<div class="m2"><p>بدام زلف تو گر مبتلاست گو میباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه ابن یمین را دل از تو پر درد است</p></div>
<div class="m2"><p>چو لطف صاحبدیوان دواست گو میباش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>علاء دولت و دین صاحبی که از عدلش</p></div>
<div class="m2"><p>ستم ز سیمبران گر رواست گو میباش</p></div></div>