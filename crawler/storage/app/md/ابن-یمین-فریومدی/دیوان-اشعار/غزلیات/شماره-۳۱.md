---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>با من آن مهروی من نامهربان از بهر چیست</p></div>
<div class="m2"><p>با دلم دایم بکین آن دلستان از بهر چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن نگار بی وفا را بی سبب چندین جفا</p></div>
<div class="m2"><p>بر مراد دشمنان با دوستان از بهر چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچم اندر و هم ناید کان سبکروح جهان</p></div>
<div class="m2"><p>بی سبب با دوستداران سرگران از بهر چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ندارد قصد صید جان مشتاقان خویش</p></div>
<div class="m2"><p>غمزه و ابروی او تیر و کمان از بهر چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میکنم جان و جهان ایثار آن آرام دل</p></div>
<div class="m2"><p>ور نباشد بهر اینجان و جهان از بهر چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دهان دارد مدام ابن یمین وصف لبش</p></div>
<div class="m2"><p>ور ندارد پس چنین شیرین زبان از بهر چیست</p></div></div>