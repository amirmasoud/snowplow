---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>بر من گذشت آن پسر شنگ نوش لب</p></div>
<div class="m2"><p>رخ در میان زلف چو مه در سواد شب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش خط هلال وش و روی چون مهش</p></div>
<div class="m2"><p>خورشید را ندید کسی عنبرین سلب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با وی رقیب همره و آری چنین بود</p></div>
<div class="m2"><p>دائم خمار با می و خار است با رطب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میرفت و پای تا سرم از تاب مهر او</p></div>
<div class="m2"><p>میسوخت آنچنانکه بسوزد زمه قصب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با چشم آهوانه او تاب در دلم</p></div>
<div class="m2"><p>افکند و همچو شیر همی سوزدم ز تب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست از دو کون شسته ام ای دوست بهر تو</p></div>
<div class="m2"><p>ور ز آنکه دشمنان شمرند اینسخن عجب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش بیار و خرمن عشاق را بسوز</p></div>
<div class="m2"><p>کآتش کند پدید که عود است یا حطب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر فتنه را بود سببی شکر حق در آن</p></div>
<div class="m2"><p>بهر هلاک ابن یمین هم توئی سبب</p></div></div>