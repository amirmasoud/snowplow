---
title: >-
    شمارهٔ ۱۴۹
---
# شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>هر که را در سر هوای چون تو دلداری بود</p></div>
<div class="m2"><p>جان فدا کردن درین ره کمترین کاری بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر رود سر در سر سودای وصلت باک نیست</p></div>
<div class="m2"><p>زین زیانها اندرین بازار بسیاری بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدن روی تو میخواهد دلم ای کاشکی</p></div>
<div class="m2"><p>طاقت نور تجلی توأش باری بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با تو چون پیوستم از دنیا بریدم بهر آنک</p></div>
<div class="m2"><p>زشت باشد گر بزیر خرقه زناری بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون دل دیوانه را زنجیر زلفت بند کرد</p></div>
<div class="m2"><p>عاقل ار پندش دهد بیهوده گفتاری بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ببینم شمع رویش جان دهم پروانه وار</p></div>
<div class="m2"><p>کمتر از پروانه بودن کمترین کاری بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان فشانی باید از ابن یمین آموختن</p></div>
<div class="m2"><p>هر کرا در سر هوای چون تو دلداری بود</p></div></div>