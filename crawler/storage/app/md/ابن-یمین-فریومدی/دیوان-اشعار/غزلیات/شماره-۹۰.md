---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>زهی من وفای تو نا دیده هیچ</p></div>
<div class="m2"><p>بغیر از جفای تو نشنیده هیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا دست هجرانت خاری نهاد</p></div>
<div class="m2"><p>گل دلگشای تو ناچیده هیچ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز مهرت تنم گشت همچون هلال</p></div>
<div class="m2"><p>مه دلربای تو نادیده هیچ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو خورشید حسنی و من ذره وار</p></div>
<div class="m2"><p>برون از هوای تو نگزیده هیچ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکردست ابن یمین سرمه ئی</p></div>
<div class="m2"><p>به از خاک پای تو در دیده هیچ</p></div></div>