---
title: >-
    شمارهٔ ۲۰۸
---
# شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>دل به بند سر زلفین تو دادیم و شدیم</p></div>
<div class="m2"><p>دجله بر عارض چون نیل گشادیم و شدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشود حجره دل منزل کس جز تو از آنک</p></div>
<div class="m2"><p>بر در از مهر تواش مهر نهادیم و شدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نبد زهره بوسیدن دستت ما را</p></div>
<div class="m2"><p>بزمین بوس تو در خاک فتادیم و شدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما بر آنیم که در پای تو چون ابن یمین</p></div>
<div class="m2"><p>سر فشانیم که آزاده و رادیم و شدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگشادی ز در خویش روانم کن از آنک</p></div>
<div class="m2"><p>دل به بند سر زلفین تو دادیم و شدیم</p></div></div>