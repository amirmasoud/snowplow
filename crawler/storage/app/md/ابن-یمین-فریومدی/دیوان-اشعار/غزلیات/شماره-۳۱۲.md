---
title: >-
    شمارهٔ ۳۱۲
---
# شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>زهی بوقت سخن لعلت از در افشانی</p></div>
<div class="m2"><p>شکسته رونق بازار گوهر کانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حدیث طره مشکینت قصه ایست دراز</p></div>
<div class="m2"><p>بکنه آن نرسد خاطر از پریشانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توئی که مینهد آبحیات از دل پاک</p></div>
<div class="m2"><p>به پیش لعل تو بر خاک تیره پیشانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم وصال تو میجست عقل میگفتش</p></div>
<div class="m2"><p>بخیره کشتی بر خشگ تا بکی رانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکایتی ز تو گر هست با تو گویم و بس</p></div>
<div class="m2"><p>که گر ز تست مرا درد هم تو درمانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بجز موافق طبع تو یکنفس نزنم</p></div>
<div class="m2"><p>گرم بر آتش سوزان چو عود بنشانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا که ابن یمینت بدیده جا سازد</p></div>
<div class="m2"><p>که جویبار سزد جای سرو بستانی</p></div></div>