---
title: >-
    شمارهٔ ۲۶۱
---
# شمارهٔ ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>ایفروغ رخت آتش زده بر خرمن ماه</p></div>
<div class="m2"><p>خوشه چین لب جانپرور تو روح الله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو سهی سروی اگر سرو سهی بست کمر</p></div>
<div class="m2"><p>تو دو هفته مهی ار ماه بر افراخت کلاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترسم آئینه رخسار ترا زنگ رسد</p></div>
<div class="m2"><p>ورنه هر دم بفلک بر کشم از جور تو آه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهدان عشق توام گر ز گنه میشمرند</p></div>
<div class="m2"><p>من نه آنم که کنم توبه از اینگونه گناه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هندوی زلف تو چون دست تطاول بگشاد</p></div>
<div class="m2"><p>جز تحمل نتوان کرد بلائیست سیاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گیرم از آتش دل پیش کسی دم نزنم</p></div>
<div class="m2"><p>چه کنم اشک روانرا بلغ السیل زباه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفته ئی پیش رقیبان منگر در رخ من</p></div>
<div class="m2"><p>که از اینکار شود حال تو ناگاه تباه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون کنم ابن یمین کشته حسن رخ تست</p></div>
<div class="m2"><p>دیده کشته سوی جان کند ایدوست نگاه</p></div></div>