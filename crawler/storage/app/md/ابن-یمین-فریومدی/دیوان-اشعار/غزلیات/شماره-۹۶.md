---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>آنها که درین دوران صاحبنظران باشند</p></div>
<div class="m2"><p>چون بر سر کوی او با هم گذران باشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مستی عشق او گر باخبرند از خود</p></div>
<div class="m2"><p>نزدیک خبرداران از بیخبران باشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بحر غمش غرقم آنها چه خبر دارند</p></div>
<div class="m2"><p>کز لجه این دریا مانده بکران باشند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر خسرو پرویز است فرهاد زمان گردد</p></div>
<div class="m2"><p>جائیکه درو یکسر شیرین پسران باشند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای باد بگو با او کز سوختگان خود</p></div>
<div class="m2"><p>بشنو سخنی کایشان صاحبنظران باشند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر ابن یمین عشقت گر عیب همی دانند</p></div>
<div class="m2"><p>آنها که کنند اینعیب از بیهنران باشند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از طعنه بدگویان ناچار گذر نبود</p></div>
<div class="m2"><p>عیسی چه محل دارد جائی که خران باشند</p></div></div>