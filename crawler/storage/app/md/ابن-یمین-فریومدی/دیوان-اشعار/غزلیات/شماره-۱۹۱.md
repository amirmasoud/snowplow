---
title: >-
    شمارهٔ ۱۹۱
---
# شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>ای ز چشمم شده پنهان و بدل در زده چنگ</p></div>
<div class="m2"><p>چون بسر میبری ایام درین کلبه تنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشت ز آئینه جان عکس رخت ظاهر از آنک</p></div>
<div class="m2"><p>صیقل نور تو بزدود ازو ظلمت زنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم بد دور چه زیباست بر آن روی چو ماه</p></div>
<div class="m2"><p>پیکر سنبل عنبر نفس غالیه رنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ز زنجیر سر زلف تو دیوانه شدم</p></div>
<div class="m2"><p>شادم ایجان که مرا نه غم نامست نه ننگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو اگر صلح و گر جنگ کنی محبوبی</p></div>
<div class="m2"><p>خوشتر از صلح کس دیگرم آید ز تو جنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سر کوی تو گفتم بسلامت بروم</p></div>
<div class="m2"><p>خود در آمد ز قضا پای دل زار بسنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوهر وصل تو در کام نهنگست ولیک</p></div>
<div class="m2"><p>در نهم گام و برون آورم از کام نهنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس که خوارم چو نی از قول مخالف دم تو</p></div>
<div class="m2"><p>هر رگی ناله دیگر کندم راست چو چنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دم عصمت مزن و تازه برون آی ز پوست</p></div>
<div class="m2"><p>روشنست ابن یمین را که تو بس شوخی و شنگ</p></div></div>