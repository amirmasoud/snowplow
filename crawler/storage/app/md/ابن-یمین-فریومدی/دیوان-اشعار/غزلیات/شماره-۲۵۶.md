---
title: >-
    شمارهٔ ۲۵۶
---
# شمارهٔ ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>مرحبا ایچشم جان روشن بنور رای تو</p></div>
<div class="m2"><p>دستگیر دل گه آشفتگی گیسوی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که دید آن موی و رو در کفر و در اسلام گفت</p></div>
<div class="m2"><p>صبح اسلام است و شام کفر روی و موی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش شمع روی تو مهر فلک پروانه ایست</p></div>
<div class="m2"><p>جفت شد ماه نو از طاق خم ابروی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب نور بخشی سایه از من وامدار</p></div>
<div class="m2"><p>تا شوم ذره صفت اندر هوای کوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با دل شوریده گفتم بر سر خوان امید</p></div>
<div class="m2"><p>جز جگر ما را نصیبی نیست از پهلوی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش تیر غمزه خوبان سپر گشتن ز عشق</p></div>
<div class="m2"><p>و آن کمان بالاترست از قوت بازوی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت کای ابن یمین از من مبین اندوه خویش</p></div>
<div class="m2"><p>دیده میآرد بلاهم سوی من هم سوی تو</p></div></div>