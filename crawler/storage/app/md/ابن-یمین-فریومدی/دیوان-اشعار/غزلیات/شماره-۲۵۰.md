---
title: >-
    شمارهٔ ۲۵۰
---
# شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>ای بخوبی رخ تو برده ز خورشید گرو</p></div>
<div class="m2"><p>گشته طاق خم ابروی تو جفت مه نو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که شب از روز شناسد بیقین گر نبود</p></div>
<div class="m2"><p>طره و چهره تو مایه ده ظلمت وضو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چو پروانه ز غم سوخت رقیبت چه غم است</p></div>
<div class="m2"><p>چون زمن شمع رخت باز نگیرد پرتو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو مکن شور و مکن کوه بتلخی فرهاد</p></div>
<div class="m2"><p>که رسیدست بکام از لب شیرین خسرو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه سرشکی تو که در چشم من آئی و روی</p></div>
<div class="m2"><p>مردم چشم منی از نظرم دور مرو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش سینه چو گندم ز غمت بشکافم</p></div>
<div class="m2"><p>گفت کز ماش بگوئید که بر ما بدو جو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دانه مهر تو کشت ابن یمین در دل تنگ</p></div>
<div class="m2"><p>و آبش از دیده همیداد غم آمد بد رو</p></div></div>