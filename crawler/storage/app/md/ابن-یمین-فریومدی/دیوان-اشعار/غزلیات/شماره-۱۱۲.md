---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>چو روی آن پری پیکر گلی بیخارکی باشد</p></div>
<div class="m2"><p>چو قدش سر و سیم اندام خوش رفتار کی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا یاقوت رمانی بمیگون لعل او ماند</p></div>
<div class="m2"><p>و گر ماند بلعل او چنان دربار کی باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهانش از لب کوثر بسی خوشتر که از کوثر</p></div>
<div class="m2"><p>نخیزد در و گر خیزد همه شهوار کی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم عشقش بدلداری برد هرگز کسی چون من</p></div>
<div class="m2"><p>مرا همچون غم عشقش کسی دلدار کی باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرم جان در سر مهرش رود زو بر نگیرم دل</p></div>
<div class="m2"><p>ز جان باشد شکیبائی ولی از یار کی باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا مستی عشق او ز سر بیرون نخواهد شد</p></div>
<div class="m2"><p>چو عشقش در دهد ساغر کسی هشیار کی باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گروهی را شگفت آمد که جان در کار او کردم</p></div>
<div class="m2"><p>ولی ز ابن یمین آخر شگفت اینکار کی باشد</p></div></div>