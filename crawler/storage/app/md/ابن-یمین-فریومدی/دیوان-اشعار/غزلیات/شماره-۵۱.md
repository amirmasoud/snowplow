---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>روی زیبای تو آرایش هر انجمن است</p></div>
<div class="m2"><p>لعل شیرین تو شور دل هر مرد و زن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خال مشکین تو بر عارض خورشیدوشت</p></div>
<div class="m2"><p>نقطه عنبر نو بر ورق نسترن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر بیاض رخ تو خط سیه باقی باد</p></div>
<div class="m2"><p>کان سوادیست کزو روشنی چشم منست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا رب آن در خوشابست و بنا گوش چو سیم</p></div>
<div class="m2"><p>یا سهیل یمن اندر بر ماه ختن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست در وصف دهانت سخنم تنگ مجال</p></div>
<div class="m2"><p>آن دهن خود که تو داری چه مجال سخن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیش زنبور عسل بر گل سیراب رسید</p></div>
<div class="m2"><p>نوش بنهاد در او و عسل اکنون دهن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو سایه پی خورشید رخت چون نرود</p></div>
<div class="m2"><p>دل که در گردنش از زلف تو مشکین رسن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بشکست دل بیچاره کجا در نگرد</p></div>
<div class="m2"><p>زلف مشکینت که سر تا بقدم پرشکن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یوسف حسنی و یعقوب صفت ابن یمین</p></div>
<div class="m2"><p>در فراق رخ تو ساکن بیت الحزن است</p></div></div>