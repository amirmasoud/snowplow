---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>بحسن روی تو خورشید عالم آرانیست</p></div>
<div class="m2"><p>بلطف رسته دندان تو ثریا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توئی چو سرو ولی سرو ماهرخ نبود</p></div>
<div class="m2"><p>توئی چو ماه ولی ماه سرو بالا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز جان غلام قد همچو سرو آزادت</p></div>
<div class="m2"><p>شدم ولی سخن راست با تو یارا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دست غم بر هم گر ترا بجانب من</p></div>
<div class="m2"><p>نظر بعین عنایت بود فاما نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از اینطرف که منم نیست در میان میلی</p></div>
<div class="m2"><p>میان تست اگر هست لیک پیدا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بزلف کافرت ایمان ندارد ابن یمین</p></div>
<div class="m2"><p>اگر چو زلف تو شوریده وش ز سودا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز چین زلف تو هر حلقه ئی بدست دلیست</p></div>
<div class="m2"><p>عجب که مملکت زنگبار یغما نیست</p></div></div>