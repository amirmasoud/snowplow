---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>گر چه هر دم ز غمت بر دلم آزاری هست</p></div>
<div class="m2"><p>باشم اغیار اگر جز تو مرا یاری هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشود دور ز تیغ تو که هرگز نکند</p></div>
<div class="m2"><p>بلبل اندیشه که اندر پی گل خاری هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خطت هست مزور سزد ایدوست از آنک</p></div>
<div class="m2"><p>خفته در سایه ابروی تو بیماری هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گنج حسنت بطلب آورم ایدوست بدست</p></div>
<div class="m2"><p>گر چه از هر طرفش غاشیه گون ماری هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شادی وصل تو گر بهره دل نیست مرا</p></div>
<div class="m2"><p>چه توان کرد غم هجر توأم باری هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر خود میندهی بارم و این خوش که مرا</p></div>
<div class="m2"><p>بر دل از سیم سرین تو گرانباری هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین پس از ابن یمین گشت دلم فارغ از آنک</p></div>
<div class="m2"><p>شب و روزش ز غم عشق تو دلداری هست</p></div></div>