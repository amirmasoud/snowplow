---
title: >-
    شمارهٔ ۱۶۸
---
# شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>از توأم آرزوی بوس و کنارست هنوز</p></div>
<div class="m2"><p>که گلستان تو بی زحمت خارست هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسرشک آب زنم خاک سر کوی ترا</p></div>
<div class="m2"><p>بر دل نازکت از بنده غبارست هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خزان غمت افتاد دل اما چشمم</p></div>
<div class="m2"><p>در هوای گل تو ابر بهارست هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم از خون جگر چهره پر از نقش و نگار</p></div>
<div class="m2"><p>وین عجب میل دلم سوی نگارست هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست شستیم ز جان و سر کویت بسرشک</p></div>
<div class="m2"><p>تا بآخر چه کشم اول کارست هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون جهانی ز می حسن تو مستند چرا</p></div>
<div class="m2"><p>در سر نرگس جادوت خمارست هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با بناگوش مکش ابروی مشکین چو کمان</p></div>
<div class="m2"><p>که دل از ناوک چشم تو فکارست هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از نسیم سحری دوش خبر پرسیدم</p></div>
<div class="m2"><p>از دل گم شده گفتا بر یارست هنوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت خو باز کن از صحبت دل ابن یمین</p></div>
<div class="m2"><p>کو بر آن جان و جهان عاشق زارست هنوز</p></div></div>