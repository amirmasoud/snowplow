---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>باز منزل بسر کوی نگار آوردیم</p></div>
<div class="m2"><p>بهر خاک درش از دیده نثار آوردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکر کردیم چو دیدیم گل عارض او</p></div>
<div class="m2"><p>که زمستان غمش را ببهار آوردیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برکنارست ز ما آن بت و ما خود دل زار</p></div>
<div class="m2"><p>در میان با غمش از بهر کنار آوردیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم آورده ام از عشق تو دیوانه دلی</p></div>
<div class="m2"><p>گفت با سلسله آن مشک تتار آوردیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان زیان گشت ز سود او جوی سود نداشت</p></div>
<div class="m2"><p>در جهان لطف و حیل با تو بکار آوردیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقانی که بخاک در تو بگذشتند</p></div>
<div class="m2"><p>همه گفتند کز آنجا دل زار آوردیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن ای دوست که چون ابن یمینت گویند</p></div>
<div class="m2"><p>که ز خاک در آن یار غبار آوردیم</p></div></div>