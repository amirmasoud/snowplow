---
title: >-
    شمارهٔ ۲۶۴
---
# شمارهٔ ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>تا در آمد خط شبرنگ تو پیرامن ماه</p></div>
<div class="m2"><p>کسوت حسن تراشد علم از شعر سیاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچنان کز شکرت سبزه دمیدست و نبات</p></div>
<div class="m2"><p>از لب چشمه حیوان ندمد مهر گیاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حبذا طالع فرخنده آنکس که فتد</p></div>
<div class="m2"><p>نظرش بر رخ میمون تو هر روز پگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمم از خیل خیال تو از آن محرومست</p></div>
<div class="m2"><p>که گذر می نتوان کرد ز دریا بشناه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم ایدل چه گنه دید دلارام ز تو</p></div>
<div class="m2"><p>کت بسیمین ذقن افکند چو یوسف در چاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت کز بهر خدا تهمت بیهوده مکن</p></div>
<div class="m2"><p>ستمی میکند او ورنه که کردست گناه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور بخون ریختنم میل کنی بهر دلت</p></div>
<div class="m2"><p>کند اینکار بچشم ابن یمین بی اکراه</p></div></div>