---
title: >-
    شمارهٔ ۲۶۰
---
# شمارهٔ ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>ای ترک پریچهره از آن جام شبانه</p></div>
<div class="m2"><p>در ده بصبوحی می گلرنگ مغانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفنی که بده جان ز پی بوسه و بستان</p></div>
<div class="m2"><p>بستان و بده تا کی از این عذر و بهانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان بر طبق شوق نهم پیش تو روزی</p></div>
<div class="m2"><p>کائی بصبوحی بر من مست شبانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون آینه رخ از رخ زیبات نتابم</p></div>
<div class="m2"><p>گر اره نهد بر سرم ایام چو شانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با حسن تو و عشق من از وامق و عذرا</p></div>
<div class="m2"><p>هر قصه که گویند بود جمله فسانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون عشق تو در حجره دل صدر نشین شد</p></div>
<div class="m2"><p>گر خواست خرد ور نه برون رفت ز خانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست ابن یمین از می عشق تو چنان مست</p></div>
<div class="m2"><p>کاوازه تسبیح نداند ز ترانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشغول بیاد تو چنانست که گوشش</p></div>
<div class="m2"><p>جز نام تو می نشنود از چنگ و چغانه</p></div></div>