---
title: >-
    شمارهٔ ۲۳۲
---
# شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>بر بیاض مه سواد خط عنبر ساش بین</p></div>
<div class="m2"><p>برگل سوری طراز سنبل رعناش بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خسرو ملک ملاحت آن بت شیرین لبست</p></div>
<div class="m2"><p>بر سر منشور حسن ابروی چون طغراش بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کج نظر گر نیستی بگشای چشم دور بین</p></div>
<div class="m2"><p>کسوت لطف الهی راست بر بالاش بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایدل از تلخی جور او مکن ابرو ترش</p></div>
<div class="m2"><p>چون الف اندر میان جان شیرین جاش بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور ندیدی شاخ سنبل سر فشان در پای سرو</p></div>
<div class="m2"><p>قد چون سرو روان و زلف سر تا پاش بین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خضر را خواهی که بینی بر لب آبحیات</p></div>
<div class="m2"><p>شهپر طوطی بگرد شکر گویاش بین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایکه پندم میدهی یکره بکویش بر گذر</p></div>
<div class="m2"><p>تا مرا معذور داری خنده زیباش بین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتمش جانرا ببوسی با تو سودا میکنم</p></div>
<div class="m2"><p>خنده زد بر من بطعنه گفت آن سوداش بین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میخورد خون دل ابن یمین میگون لبش</p></div>
<div class="m2"><p>ور ز من باور نداری سرخی لبهاش بین</p></div></div>