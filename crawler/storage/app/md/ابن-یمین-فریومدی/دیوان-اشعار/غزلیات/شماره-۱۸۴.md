---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>باد صبا صبحدم بوی تو آورد دوش</p></div>
<div class="m2"><p>داد پیامت بمن برد ز من صبر و هوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ببر آمد بجوش ز آتش سودای تو</p></div>
<div class="m2"><p>وز گذر دیدگان خون دلم شد بجوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع رخت گر زند آتشم اندر جگر</p></div>
<div class="m2"><p>همچو ز پروانه کس نشنود از من خروش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناله کنان من ز تو پیش که خیزم بپای</p></div>
<div class="m2"><p>ظلم چو شه میکند چون بنشینم خموش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه تو گوئی بگوی کز لب شیرین تو</p></div>
<div class="m2"><p>گر چه بود تلخ و تیز یابم از آن ذوق نوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بسرایت رسم پست چو در گشته ام</p></div>
<div class="m2"><p>معتکف آستان حلقه خدمت بگوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سر کویت سوی خلد برین نگذرم</p></div>
<div class="m2"><p>ور بطلب آیدم از بر رضوان سروش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایدل اگر بایدت مرتبه عاشقی</p></div>
<div class="m2"><p>ز ابن یمین یک سخن از سردانش نیوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیز چو سودائیان بر سر بازار عشق</p></div>
<div class="m2"><p>آنده دل میخر و شادی جان میفروش</p></div></div>