---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>ز تاب می چو خوی از روی دلستان بچکد</p></div>
<div class="m2"><p>مرا ز نرگس تر آب ارغوان بچکد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز غنچه لب یاقوت رنگ او چه عجب</p></div>
<div class="m2"><p>که خون شود دل لعل از عروق کان بچکد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زرنگ و بوی ندانم گلاب یا عرق است</p></div>
<div class="m2"><p>خویی که از رخ آنماه مهربان بچکد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شرم عارض چون ماه او شگفت مدار</p></div>
<div class="m2"><p>گر آب از آتش خورشید آسمان بچکد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدان امید که صفرای او شود کمتر</p></div>
<div class="m2"><p>ز ثقبه عنبیم آب ناردان بچکد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن نزار من از عشق او چنان زرد است</p></div>
<div class="m2"><p>کزو بجای عرق آب زعفران بچکد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز لطف خود بسرم دست اگر فرود آرد</p></div>
<div class="m2"><p>چو خوی زهر بن مویم هزار جان بچکد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گهی که ابن یمین وصف آن نگار کند</p></div>
<div class="m2"><p>ز نازکی سخن آید که آب از آن بچکد</p></div></div>