---
title: >-
    شمارهٔ ۲۴۱
---
# شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>صبح دمید ساقیا بزم صبوح ساز کن</p></div>
<div class="m2"><p>بر دل ما ز خرمی در ز بهشت باز کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه که ناز کرده ئی ای بت نازنین من</p></div>
<div class="m2"><p>نیک خوش آیدم ز تو باز در آی و ناز کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آنچه بود زیادتی دست ز آب رز بشوی</p></div>
<div class="m2"><p>وز خبثات آرزو پاک شو و نماز کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صوم و صلوه نافله گر چه ستوده طاعتیست</p></div>
<div class="m2"><p>شاید اگر نباشدت نان بده و نیاز کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز سپید عقل را دیده چنین چه بسته ئی</p></div>
<div class="m2"><p>تا بهوای دل رسی دیده باز باز کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلبل خوشنوا چنان در قفس از زبان بود</p></div>
<div class="m2"><p>دم مزن و نشیمن از دست شهان چو باز کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابن یمین اگر ترا آرزوی سلامتست</p></div>
<div class="m2"><p>رو در آرزوی دل بر رخ خود فراز کن</p></div></div>