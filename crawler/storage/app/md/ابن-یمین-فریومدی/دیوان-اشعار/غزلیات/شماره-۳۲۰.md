---
title: >-
    شمارهٔ ۳۲۰
---
# شمارهٔ ۳۲۰

<div class="b" id="bn1"><div class="m1"><p>لعل لبش کزو بچکد آب زندگی</p></div>
<div class="m2"><p>آورد خط بنام من از بهر بندگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جستجوی عارض چون آفتاب او</p></div>
<div class="m2"><p>هستم چو ماه گرد جهان در دوندگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طوطی جان همی زندم بال تا کند</p></div>
<div class="m2"><p>اندر هوای شکر جانان پرندگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسبت بقامتش نتوان کرد سرو را</p></div>
<div class="m2"><p>کو سرو را میان چمن آن چمندگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ رمیده دانه خال ار ببیندش</p></div>
<div class="m2"><p>از دام زلف او ننماید رمندگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا گشت چون کمان قدم از من چو تیر جست</p></div>
<div class="m2"><p>تیر از کمان هر آینه جوید جهندگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی او نخواهد ابن یمین یکنفس حیات</p></div>
<div class="m2"><p>کز بهر وصل او بودش میل زندگی</p></div></div>