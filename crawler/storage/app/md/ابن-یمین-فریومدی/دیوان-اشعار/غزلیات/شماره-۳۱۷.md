---
title: >-
    شمارهٔ ۳۱۷
---
# شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>صبا چو با سر زلف تو کرد همرازی</p></div>
<div class="m2"><p>زبان گشاد نسیم خوشش بغمازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببوی زلف تو مرغ دل از قفس بپرید</p></div>
<div class="m2"><p>هوای کوی تو بگزید و گشت پروازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل از تو سیر نگردد بجور کز تو کشد</p></div>
<div class="m2"><p>گرش بعنف برانی ورش بلطف بنوازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل از تو سیر نگردد بجور کز تو کشد</p></div>
<div class="m2"><p>گرش بعنف برانی ورش بلطف بنوازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم شکار تو گشت و هنوز در پی او</p></div>
<div class="m2"><p>کمان و تیر ز ابرو و غمزه میسازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدینصفت که دل از دست عاشقان بردی</p></div>
<div class="m2"><p>ترا رسد که کنی بر بتان سرافرازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سر برون نکنم شور لعل شیرینت</p></div>
<div class="m2"><p>گرم بر آتش سوزان چو شمع بگدازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خط حکم تو تا حشر بر ندارم سر</p></div>
<div class="m2"><p>گرم چو خامه بآب سیه در اندازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه با ک ابن یمین را از آنکه سر برود</p></div>
<div class="m2"><p>چو نیست در سرش الا هوای سربازی</p></div></div>