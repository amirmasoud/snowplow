---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>هر نسیمی که ز خاک در جانان باشد</p></div>
<div class="m2"><p>چون دم روح قدس مایه ده جان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بمیدان لطافت ذقنش گوی زند</p></div>
<div class="m2"><p>قامت اهل دل از عشق چو چوگان باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان بدو دادم و دل از سر تحسین میگفت</p></div>
<div class="m2"><p>جان همان به که چو باشد بر جانان باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بزرگی نپذیرفت دمی جان عزیز</p></div>
<div class="m2"><p>گفت در بارم ازین خرده فراوان باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راستی جان من خسته متاعیست حقیر</p></div>
<div class="m2"><p>تحفه ئی سازم ازو لابد ازینسان باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر گدائی که شود شبفته بر عارض شاه</p></div>
<div class="m2"><p>دائم از حرمت او بر در حرمان باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان بتحفه بر جانان مفرست ابن یمین</p></div>
<div class="m2"><p>کاین تکلف مثل زیره و کرمان باشد</p></div></div>