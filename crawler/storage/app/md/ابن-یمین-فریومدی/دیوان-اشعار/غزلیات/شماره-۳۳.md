---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>بر ماه و سرو آید هر لحظه صد قیامت</p></div>
<div class="m2"><p>ز آن سرو ماه طلعت و آنماه سرو قامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم کنم ز کویت عزم سفر ولیکن</p></div>
<div class="m2"><p>چون دیدمت نکردم جز نیت اقامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ماجرای عشقت جان در میانه دل</p></div>
<div class="m2"><p>دل نیست نو ارادت کاندیشد از غرامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور ترسی از ملامت از عاشقی حذر کن</p></div>
<div class="m2"><p>زیرا که راست ناید این کار بی ملامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل بر جفات کردم خوش ز آنکه چون منی را</p></div>
<div class="m2"><p>از چون توئی تمامست این لطف و این کرامت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از منزل سلامت آنلحظه رخت بر بست</p></div>
<div class="m2"><p>مسکین دلم چو بشنید از زیر لب سلامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تعییر میکنندم در عشق و می ندانند</p></div>
<div class="m2"><p>کابن یمین ندارد بر عاشقی ندامت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر مست عشق گردد آنکو امام شهرست</p></div>
<div class="m2"><p>دانم که ننگش آید از منصب امامت</p></div></div>