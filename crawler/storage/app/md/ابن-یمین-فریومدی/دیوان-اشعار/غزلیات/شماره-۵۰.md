---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>روی شهر آرای یارم آفتابی دیگرست</p></div>
<div class="m2"><p>هر زمانی زلف او در پیچ و تابی دگرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر رخ او قطره های خوی چو شبنم بر گلست</p></div>
<div class="m2"><p>هر زمانی چون گلی و چون گلابی دیگرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم از روی خودم روشن نشانی باز ده</p></div>
<div class="m2"><p>گفت آخر روشنست این آفتابی دیگرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آتش سودای عشقش در جهان هر جا دلیست</p></div>
<div class="m2"><p>بر سر خوان هوس هر دم کبابی دیگرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بهار حسن رویش تازه ماند هر زمان</p></div>
<div class="m2"><p>ز ابر چشم اشکبارم فتح با بی دیگرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر روانم درد عشق و بر دلم بار فراق</p></div>
<div class="m2"><p>هر یکی ز اینها خرابی بر خرابی دیگرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وعده وصلش اگر چه دلفریب آمد ولیک</p></div>
<div class="m2"><p>دل بر آن نتوان نهادن کان سرابی دیگرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز رضای او نجوید در جهان ابن یمین</p></div>
<div class="m2"><p>و آن صنم را هر زمان با او عتابی دیگرست</p></div></div>