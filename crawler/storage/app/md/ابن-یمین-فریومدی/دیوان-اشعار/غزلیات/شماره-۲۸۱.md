---
title: >-
    شمارهٔ ۲۸۱
---
# شمارهٔ ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>آبحیاتست یا لبان که تو داری</p></div>
<div class="m2"><p>چشمه نوشست یا دهان که تو داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نظرم آفتاب سایه نشین است</p></div>
<div class="m2"><p>زیر کله روی دلستان که تو داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پسته دهن بسته زان بود که ندارد</p></div>
<div class="m2"><p>چربی و شیرینی زبان که تو داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مایه سودای ماست شعر سیاهت</p></div>
<div class="m2"><p>در بر نازک چو پرنیان که تو داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن تو گنجیست شایگانی و زلفت</p></div>
<div class="m2"><p>مار سر گنج شایگان که تو داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با تو چنانم که در میان من و تو</p></div>
<div class="m2"><p>موی نگنجد جز آن میان که تو داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابن یمین را چو تیر خاک نشین کرد</p></div>
<div class="m2"><p>از کجی ابروی چون کمان که تو داری</p></div></div>