---
title: >-
    شمارهٔ ۳۲۱
---
# شمارهٔ ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>من ندیدم نشنیدم که بود در چمنی</p></div>
<div class="m2"><p>همچو بالای تو سروی و چو رخ نسترنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند از رسته دندان چو عقد گهرت</p></div>
<div class="m2"><p>صدفش جوهر فردی شده یعنی دهنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا گل عارضت اندر چمن حسن بود</p></div>
<div class="m2"><p>کی خوش آید بدلم یاسمنی یاسمنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر از مادر ایام نزاید پسری</p></div>
<div class="m2"><p>چون تو یاقوت لبی سنگدلی سیمتنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دلم بشکند اندر خم زلفت بطفیل</p></div>
<div class="m2"><p>افکنی هر نفس از وی شکنی بر شکنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکن ایدوست بدستان مفکن در پایم</p></div>
<div class="m2"><p>ز آنکه سر حلقه عشاق نیابی چو منی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو شدی یوسف مصری و ولی ابن یمین</p></div>
<div class="m2"><p>گشت یعقوب صفت ساکن بیت الحزنی</p></div></div>