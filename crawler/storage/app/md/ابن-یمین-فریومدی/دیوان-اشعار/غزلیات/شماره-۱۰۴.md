---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>بر گل سیراب سنبل را چو جولان میدهد</p></div>
<div class="m2"><p>بلبل طبع مرا یاد از گلستان میدهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نبات از شکر میگونش سر بر میزند</p></div>
<div class="m2"><p>خضر پنداری نشان از آبحیوان میدهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشک بر کافور میبیزد صبا از زلف او</p></div>
<div class="m2"><p>زان چو انفاس مسیحا راحت جان میدهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بماند تازه گلبرگ رخش در باغ حسن</p></div>
<div class="m2"><p>چشمم آبش از هوا چون ابر نیسان میدهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بجانی میفروشد یکنظر جانان من</p></div>
<div class="m2"><p>شاهد حال است روی او که ارزان میدهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سر ابن یمین هست آنکه جان پا شد بر او</p></div>
<div class="m2"><p>زنده دل آنکس که جان در پای جا نان میدهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سهل باشد جان بتلخی دادنم فرهاد وار</p></div>
<div class="m2"><p>گر لب شیرین او بختم بدندان میدهد</p></div></div>