---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>عشق تو گر ضلال دل ماست گر رشاد</p></div>
<div class="m2"><p>ما را توئی ز هر دو جهان غایت مراد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما عشق تو بمبدء فطرت گزیده ایم</p></div>
<div class="m2"><p>و ز بهر روح ساخته زو توشه معاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر مکر دشمن از تو جدا میکند مرا</p></div>
<div class="m2"><p>لاتنقص المحبه بالهجر و البعاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور از جمالت آتش هجر ار بسوزدم</p></div>
<div class="m2"><p>گیرد هوای کوی تو هر ذره از رماد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محسوب در شمار ولیکن نه آشکار</p></div>
<div class="m2"><p>نقش دهان تنگ تو چون فارد و ز باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نو عروس فکر تو آید بجلوه گاه</p></div>
<div class="m2"><p>آنلحظه یا ز غایت اخلاص یا ز داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رحمی کن ای نگار که در کشتزار عمر</p></div>
<div class="m2"><p>جز کشت خویش ندروی اندر گه حصاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای دل رضای دوست گرت دست میدهد</p></div>
<div class="m2"><p>از دشمنان چه باک که باشند با عناد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ابن یمین بپای تو خواهد فکند سر</p></div>
<div class="m2"><p>کر ذال کرد قافیه و هر چه باد باد</p></div></div>