---
title: >-
    شمارهٔ ۲۲۹
---
# شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>ای عارض گلگونت عکسی زده بر گردون</p></div>
<div class="m2"><p>خورشید شده پیدا ز آن عکس رخ گلگون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون لعل تو سلک در در خنده کند پیدا</p></div>
<div class="m2"><p>با لطف وی از جز عم افتد گهر موزون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیرون نرود یکدم مهرت ز دل تنگم</p></div>
<div class="m2"><p>صد سال اگر باشم در خاک لحد مدفون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلفت بسیه کاری مسند ز قمر سازد</p></div>
<div class="m2"><p>هندو نبود چون او هم مقبل و هم میمون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آندم که سفیده دم زد بر رخ گلگونت</p></div>
<div class="m2"><p>دل میکشدم دامن مانند شفق در خون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دریاب کنون دلرا کز وی رمقی ماندست</p></div>
<div class="m2"><p>چون زهر بجان آمد تریاق چه سودا کنون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ابن یمین جانا یکره سخنی بشنو</p></div>
<div class="m2"><p>در گوش کشند آخر خوبان گهر موزون</p></div></div>