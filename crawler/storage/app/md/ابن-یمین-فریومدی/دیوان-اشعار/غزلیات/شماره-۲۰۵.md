---
title: >-
    شمارهٔ ۲۰۵
---
# شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>تا من زبان چو بلبل خوشگو گشوده ام</p></div>
<div class="m2"><p>گلزار فضل را بصد الحان ستوده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کسب هر هنر که ز مردی و مردمیست</p></div>
<div class="m2"><p>کوشیده ام چو منقلب آن شنوده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر نیمشب بآه دل سوزناک خویش</p></div>
<div class="m2"><p>زنگ هوا ز آینه دل زدوده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بهر رنگ و بوی چو زلف سمنبران</p></div>
<div class="m2"><p>آشفته روزگار و پریشان نبوده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وقت جدال در خم چوکان آسمان</p></div>
<div class="m2"><p>گوی هنر ز جمله اقران ربوده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در باغ فضل ز آتش طبع چو آب خویش</p></div>
<div class="m2"><p>همچون خلیل سنبل و ریحان نموده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داند خرد که پایه تخت سخنوری</p></div>
<div class="m2"><p>بر اوج تاج تارک کیوان بسوده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داماد نو عروس سخن بوده ام و لیک</p></div>
<div class="m2"><p>رخسار او بناخن حرمان شخوده ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ابن یمین مکار بجز تخم نیک از آنک</p></div>
<div class="m2"><p>من آنچه کشته ام بر از آنسان دروده ام</p></div></div>