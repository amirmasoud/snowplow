---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>زین پیش داشتم صنمی سیمبر بدست</p></div>
<div class="m2"><p>اکنون نه عین دارم ازو نه اثر بدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوئی که چشم غیرت ایام خفته بود</p></div>
<div class="m2"><p>کان گنجم اوفتاد چنان بیخبر بدست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بازش ربود از کفم ایام و چشم من</p></div>
<div class="m2"><p>دارد به یادگار وی اکنون گهر بدست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایجان برگزیده کجائی که بیتو دل</p></div>
<div class="m2"><p>دارد همیشه ناله و آه سحر بدست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان در خطر همی فکنم از برای تو</p></div>
<div class="m2"><p>آری نیامدست گهر بیخطر بدست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از چهره در هوای تو زر میدهم ولیک</p></div>
<div class="m2"><p>جانی که رفت باز نیاید بزر بدست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوید بطعنه با دل من هر شبی فلک</p></div>
<div class="m2"><p>بیهوده پر مجه که نیاید قمر بدست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد بار اگر نبات برآید ز خاک من</p></div>
<div class="m2"><p>یکره نیایدم چو تو شاخ شکر بدست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان بذل کرد ابن یمین در هوای تو</p></div>
<div class="m2"><p>عیبش مکن نداشت جز این مختصر بدست</p></div></div>