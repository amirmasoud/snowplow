---
title: >-
    شمارهٔ ۱۹۰
---
# شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>ای بعمد از ده بر لاله تر خال ز مشک</p></div>
<div class="m2"><p>وی نگاریده بر اطراف قمر دال ز مشک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی خوش میدمد از زلف گره بر گرهت</p></div>
<div class="m2"><p>نفس خوش دمد آری بهمه حال ز مشک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طوطی خط تو بر گرد لب چون شکرت</p></div>
<div class="m2"><p>همچو زاغیست بر آورده پر و بال ز مشک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنده خط سیاهم که بر آن روی چو ماه</p></div>
<div class="m2"><p>بر کشیدی ز پی فرخی فال ز مشک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من نگویم که رخت ماه دو هفته است از آنک</p></div>
<div class="m2"><p>بر رخ مه نتوان دید خط و خال ز مشک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با وجود رخ و زلف سیهت ابن یمین</p></div>
<div class="m2"><p>مه نبیند نکند یاد بصد سال ز مشک</p></div></div>