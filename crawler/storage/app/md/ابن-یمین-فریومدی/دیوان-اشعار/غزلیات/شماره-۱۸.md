---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>تا کرد زیر سایه نهان زلفت آفتاب</p></div>
<div class="m2"><p>افتاد همچو ذره مرا دل در اضطراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس که چین زلف ترا گاه وصف گفت</p></div>
<div class="m2"><p>مشک خطا نراند سخن بر ره صواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف تو سنبلیست که لالاش عنبرست</p></div>
<div class="m2"><p>جز خون سوخته نبود هیچ مشک ناب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نازکی برون تنت دل بود پدید</p></div>
<div class="m2"><p>ز انسان که سنگریزه پدیدست اندر آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهم که همچو جان کشم اندر برت و لیک</p></div>
<div class="m2"><p>بی زر ز سیمبر نتوان گشت کامیاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون چنگ سر فکنده به پیشم بغم مدام</p></div>
<div class="m2"><p>کز جور دور کیسه تهی کرد چون رباب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل میل جام لعل تو کردست چون کنم</p></div>
<div class="m2"><p>زین نا خلف که باز فتادست در شراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم مگر بخواب ببینم خیال تو</p></div>
<div class="m2"><p>لیکن مرا خیال محالست بیتو خواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مهر تو باز در دل ابن یمین نشست</p></div>
<div class="m2"><p>یعنی که جای گنج بکنجی بود خراب</p></div></div>