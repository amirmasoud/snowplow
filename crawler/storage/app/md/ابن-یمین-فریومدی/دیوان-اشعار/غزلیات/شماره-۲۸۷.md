---
title: >-
    شمارهٔ ۲۸۷
---
# شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی بدور دو ستکانی</p></div>
<div class="m2"><p>بده بر گل شراب ارغوانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنار جویبار از خرمی شد</p></div>
<div class="m2"><p>چو دوران جوانی از جوانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهان کرد آب را از دل ولی آب</p></div>
<div class="m2"><p>پدید آورد اسرار نهانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چمن چون کلبه جوهر فروش است</p></div>
<div class="m2"><p>ز گوهر های دریائی و کانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سحرگاهان ز بستان سوی مستان</p></div>
<div class="m2"><p>صبا میآرد از گل ارمغانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوشا آنکس که چون نرگس ز مستی</p></div>
<div class="m2"><p>فتد در پای سرو بوستانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طرب امروز با فردا میفکن</p></div>
<div class="m2"><p>چه زاید اینشب حبلی چه دانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مجوی ابن یمین جز نیکنامی</p></div>
<div class="m2"><p>اگر خواهی که دایم زنده مانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمیرد هر که نام نیک ازو ماند</p></div>
<div class="m2"><p>چنین باشد حیات جاودانی</p></div></div>