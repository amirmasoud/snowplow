---
title: >-
    شمارهٔ ۱۶۶
---
# شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>صنما حال زار من بنگر</p></div>
<div class="m2"><p>با غمت کارزار من بنگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در غم لعل گوهر افشانت</p></div>
<div class="m2"><p>جزع یاقوتبار من بنگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگ من در غم تو بار دلست</p></div>
<div class="m2"><p>برگ من بین و بار من بنگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هوای گلت چو بلبل مست</p></div>
<div class="m2"><p>ناله زیر و زار من بنگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اختیار من از جهان رخ تست</p></div>
<div class="m2"><p>خوبی اختیار من بنگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لاله زارست رویم از چشمم</p></div>
<div class="m2"><p>نزهت لاله زار من بنگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منم ابن یمین و کار چنین</p></div>
<div class="m2"><p>چیست تدبیر کار من بنگر</p></div></div>