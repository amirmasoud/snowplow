---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>نرگس مست تو این فتنه که بنیاد نهاد</p></div>
<div class="m2"><p>دل و دین را همه بر آتش و بر باد نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حبذا باد بهاری که ز روی و مویت</p></div>
<div class="m2"><p>بر گل تازه و تر طره شمشاد نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنده قد تو شد سرو سهی از دل پاک</p></div>
<div class="m2"><p>گر چه ز آغاز جهان نام خود آزاد نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقم کردی و گفتی که نکردم هیهات</p></div>
<div class="m2"><p>شور شیرین که چنین در دل فرهاد نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من بخود سوخته او نشدم سوخت مرا</p></div>
<div class="m2"><p>آنکه در سینه سیمین دل فولاد نهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوردن خون جگر سوختگان فرض شناخت</p></div>
<div class="m2"><p>آنکه چشم سیهش سنت بیداد نهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت هست ابن یمین در طلبم بی غم عشق</p></div>
<div class="m2"><p>تهمتی بر من ازینسان ز دل شاد نهاد</p></div></div>