---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>ساقیا موسم آنست که می نوش کنیم</p></div>
<div class="m2"><p>محنت گردش ایام فراموش کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیز چون در چمن افتاد ز بلبل غلغل</p></div>
<div class="m2"><p>قلقل بلبله را یک نفسی گوش کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوستکامی همه با یار کلهدار خوریم</p></div>
<div class="m2"><p>عیش در سایه آن سرو قبا پوش کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ده آن رطل گران تا سبک از قوت می</p></div>
<div class="m2"><p>عقل را واله و سرگشته و مدهوش کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سر ما نرود تا بقیامت مستی</p></div>
<div class="m2"><p>گر می از ساغر لعل لب تو نوش کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا بکی دیگ هوس از پی مهمان خیال</p></div>
<div class="m2"><p>بر سر آتش سودای تو پر جوش کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزها دست زدیم از غم عشقت بر سر</p></div>
<div class="m2"><p>بامیدی که شبی با تو در آغوش کنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسوت حسن چو بر قد تو آراسته اند</p></div>
<div class="m2"><p>تا قیامت علم عشق تو بر دوش کنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیز با ابن یمین شاد بعشرت بنشین</p></div>
<div class="m2"><p>تا نشاط و طرب امروز به از دوش کنیم</p></div></div>