---
title: >-
    شمارهٔ ۲۴۰
---
# شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>شکر میریزد از پسته نگارم در سخن گفتن</p></div>
<div class="m2"><p>نباشد هیچ طوطی را ازین خوشتر سخن گفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز یاد رسته دندان همچون در شهوارش</p></div>
<div class="m2"><p>مرا گوهر فشان گردد زبان اندر سخن گفتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدور اختر چشمش کزو شد چشم جان روشن</p></div>
<div class="m2"><p>نباشد عقل را لایق زماه و خور سخن گفتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آه سرد و اشک گرم خشک و تر بود دایم</p></div>
<div class="m2"><p>لب و چشمم ولی نتوان ز خشک و تر سخن گفتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه عشق او بربود خواب و خور ز من لیکن</p></div>
<div class="m2"><p>نباشد لایق عاشق ز خواب و خور سخن گفتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نسیم صبح را گفتم بگو رمزی ز من با او</p></div>
<div class="m2"><p>که با او جز تو نتواند کسی دیگر سخن گفتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگفت ابن یمین خشک آر و بگذر زین حدیث تر</p></div>
<div class="m2"><p>که با آن سیمبر نتوان بجز از زر سخن گفتن</p></div></div>