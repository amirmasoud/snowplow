---
title: >-
    شمارهٔ ۱۳۵
---
# شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>گر بوصل خودم آنماه زمانی بدهد</p></div>
<div class="m2"><p>دل من روح بشکرانه روانی بدهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشکارا ندهد بوسه ام از بیم رقیب</p></div>
<div class="m2"><p>کاش باری نکند بوسه روانی بدهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بتماشای قدش دل بچمن رفت مگر</p></div>
<div class="m2"><p>سروش از قامت او راست نشانی بدهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندهم صحبت جانان بهمه ملک جهان</p></div>
<div class="m2"><p>نا سپاس است که جانی بجهانی بدهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپر ماه کنم چون زره ارزانکه مرا</p></div>
<div class="m2"><p>غمزه و ابروی او تیر و کمانی بدهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکزمان غیبت دل از بر آنماه چکل</p></div>
<div class="m2"><p>نیست ممکن مگر از جانش ضمانی بدهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حاجب ابروست میان دل و روی چو مهش</p></div>
<div class="m2"><p>یک زمانی ز غمش بو که امانی بدهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روی او وجه زری میطلبد از عشاق</p></div>
<div class="m2"><p>خواست تا مالش هر یک بقلانی بدهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرد اشارت بسوی ابن یمین غمزه او</p></div>
<div class="m2"><p>گفت خوش باش که این وجه فلانی بدهد</p></div></div>