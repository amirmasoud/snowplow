---
title: >-
    شمارهٔ ۱۹۳
---
# شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>ای در زمانه حسن تو چون در بهار گل</p></div>
<div class="m2"><p>ناید به پیش روی تو اندر شمار گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا انتساب گل بتو کردند آمدست</p></div>
<div class="m2"><p>خندان و سرخ روی سوی جویبار گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل را چه نسبت است برویت چو ایمنست</p></div>
<div class="m2"><p>در گلشن جمال تو ز آسیب خار گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بس که سرخ و زرد بر آمد از آنکه شد</p></div>
<div class="m2"><p>از روی لاله رنگ رخت شرمسار گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد صبا حکایت حسنت بگل رساند</p></div>
<div class="m2"><p>از رشک شد چو سنبل تر بیقرار گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>و آن زر ساو بر طبق لعل فام کرد</p></div>
<div class="m2"><p>تا بر سرت کند بتواضع نثار گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای باغبان بیا و قد و خد او نگر</p></div>
<div class="m2"><p>منشان بباغ سرو سهی و مکار گل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب روان و سبزه و جام شراب و رود</p></div>
<div class="m2"><p>گر چه خوشند خوشتر ازین هر چهار گل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اما بسان ابن یمین هم که عاشق است</p></div>
<div class="m2"><p>با روی دلستانش نیاید بکار گل</p></div></div>