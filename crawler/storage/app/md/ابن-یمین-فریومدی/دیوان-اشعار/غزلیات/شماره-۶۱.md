---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>ساقی قدحی در ده گر هیچ می ات باقیست</p></div>
<div class="m2"><p>کز سوختگی جانم در غایت مشتاقیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خادم مسجد را قندیل بکف بینم</p></div>
<div class="m2"><p>از شوق دلم گوید کاین ساغر و آن ساقیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معنی طلب از باطن بگذر زره ظاهر</p></div>
<div class="m2"><p>کار استن صورت سالوسی و زراقیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شنگرف لب لعلت زنگار خط سبزت</p></div>
<div class="m2"><p>از صمغ سرشک من در غایت براقیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستوفی عشق تو در دفتر خرج من</p></div>
<div class="m2"><p>جز صبر نمیراند مجموع غمت باقیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ابن یمین بوسی از لعل تو برباید</p></div>
<div class="m2"><p>معذور همیدارش کان از ره ذواقیست</p></div></div>