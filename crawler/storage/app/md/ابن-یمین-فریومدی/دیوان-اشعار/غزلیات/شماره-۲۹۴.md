---
title: >-
    شمارهٔ ۲۹۴
---
# شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>تا بر سریر حسن تویی ماه‌چهره‌ای</p></div>
<div class="m2"><p>هستم ز عشق تو در شهر شهره‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آن در شاهوار و بناگوش همچو سیم</p></div>
<div class="m2"><p>هستند در مقار نه ماهی و زهره‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون جزع دل‌فریب تو هرگز به جادویی</p></div>
<div class="m2"><p>نامد برون ز حقه افلاک مهره‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم بد از تو دور که مانی به عمر خویش</p></div>
<div class="m2"><p>نقشی نبست چون تو و نگشاد چهره‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاری تراست بر دل عشاق مستمند</p></div>
<div class="m2"><p>هر غمزه‌ای ز چشم تو از زخم دهره‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ابن یمین طمع به وفای تو چون کند</p></div>
<div class="m2"><p>او را بس ار بود ز جفای تو بهره‌ای</p></div></div>