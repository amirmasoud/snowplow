---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>شهره شهر شد از غایت خوبی رویت</p></div>
<div class="m2"><p>ای شده ترک فلک از دل و جان هندویت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست رخسار تو یک ماه که در غره او</p></div>
<div class="m2"><p>جلوه دادست دو عنبر ز هلال ابرویت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم بد دور که بستان ارم را گه حسن</p></div>
<div class="m2"><p>خار اندوه نهادست گل خود رویت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فتنه دور قمر نیست در آفاق کنون</p></div>
<div class="m2"><p>بجز آن غمزه غمازوش جادویت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوان عشقت چو نهادند وصلادر دادند</p></div>
<div class="m2"><p>میخورد دل جگر خویشتن از پهلویت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه کند این دل دیوانه که در پی نرود</p></div>
<div class="m2"><p>چون بزنجیر کشان میبردش گیسویت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من چنین شیفته ز آنم که رگی از سودا</p></div>
<div class="m2"><p>هست پیوسته مرا با دل زار از مویت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عابدان روی سوی قبله اسلام کنند</p></div>
<div class="m2"><p>عارفانرا نبود قبله جان جز کویت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بستم احرام طواف سر کوی تو ز شوق</p></div>
<div class="m2"><p>که ببوسم حجرالاسود خال رویت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواب خرگوش بچشم خرد ابن یمین</p></div>
<div class="m2"><p>میدهد غمزه شیرافکن چون آهویت</p></div></div>