---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>گلهای نوشکفته بهر بوستان که هست</p></div>
<div class="m2"><p>پیش رخ تو خار نماید چنانکه هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با دود و آتش جگر و دل ز رشک تست</p></div>
<div class="m2"><p>هر لاله ئی که باشد و هر ارغوان که هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بهر سرو سرکش تو نیست پس چراست</p></div>
<div class="m2"><p>در جویبار چشم من آب روان که هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دست دیده کار دل من بجان رسید</p></div>
<div class="m2"><p>کو آشکار میکندش هر نهان که هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه یقینست آنکه دهن نیستت ولی</p></div>
<div class="m2"><p>میافکند حدیث توأم در گمان که هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باریکتر ز موی میانت دقیقه ایست</p></div>
<div class="m2"><p>کز وی بجز کمر ندهد کس نشان که هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا دستگیر بنده شوی همچو آستین</p></div>
<div class="m2"><p>باشد سرم همیشه بر این آستان که هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بازار دل چو ز آتش سودای تست گرم</p></div>
<div class="m2"><p>کمتر ز سود نیست مرا هر زیان که هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ابن یمین مخواه دل از دلستان که نیست</p></div>
<div class="m2"><p>گر بایدت بیا ببر این نیم جان که هست</p></div></div>