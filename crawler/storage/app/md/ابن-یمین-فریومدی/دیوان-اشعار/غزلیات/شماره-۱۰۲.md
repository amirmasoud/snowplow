---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>با ما غم هجران تو ای دوست نه آن کرد</p></div>
<div class="m2"><p>کان قصه توانیم بصد سال بیان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خانه دل رخت صبوری بدر انداخت</p></div>
<div class="m2"><p>چه جای صبوری که از اینخانه روان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با باغ و بهار طربم آتش شوقت</p></div>
<div class="m2"><p>آن کرد که با برگ رزان باد خزان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیدا شد ازین اشک روان خلق جهانرا</p></div>
<div class="m2"><p>رازیکه دل غمزده در پرده نهان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دریاب مرا بار دگر زنده کزین پس</p></div>
<div class="m2"><p>هجران تو ای سرو روان قصد روان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چوگان قضا باز چو گوی ابن یمین را</p></div>
<div class="m2"><p>سرگشته و برگشته در آفاق دوان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با ما سرگردون جفا پیشه چو خوش نیست</p></div>
<div class="m2"><p>با خصم و بوی غیر مدارا چه توان کرد</p></div></div>