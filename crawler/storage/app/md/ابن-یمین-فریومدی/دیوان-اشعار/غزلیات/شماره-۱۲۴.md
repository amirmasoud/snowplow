---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>سنبل غالیه گون بر گل تر میشکند</p></div>
<div class="m2"><p>ظلمت شام بر انوار سحر میشکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر زمان پسته شیرینش که شور شهر است</p></div>
<div class="m2"><p>خنده ئی میزند و نرخ شکر میشکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دمی حسن جهانگیر وی از ابرو و چشم</p></div>
<div class="m2"><p>ساخته تیر و کمان قلب دگر میشکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا من از رشته دندانش سخن میگویم</p></div>
<div class="m2"><p>از لطافت سخنم قدر گهر میشکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میکند بر دل من پیرهن صبر قبا</p></div>
<div class="m2"><p>از سر ناز کله گوشه چو بر میشکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناصوابست که آن ترک خطا بی سببی</p></div>
<div class="m2"><p>دل بیمار من خسته جگر میشکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از می عشق چنان مست شدست ابن یمین</p></div>
<div class="m2"><p>که در خانه معشوق بسر میشکند</p></div></div>