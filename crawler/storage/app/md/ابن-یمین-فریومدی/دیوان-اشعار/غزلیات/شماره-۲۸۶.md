---
title: >-
    شمارهٔ ۲۸۶
---
# شمارهٔ ۲۸۶

<div class="b" id="bn1"><div class="m1"><p>آن غالیه گون نقش نگر بر گل سوری</p></div>
<div class="m2"><p>بر ماه دو هفته رقم است از گل سوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم بد حساد که بر کنده ز سر باد</p></div>
<div class="m2"><p>افکند ز تو دورم و فریاد ز دوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ظلمت فرقت برهان ذره وشم ز آنک</p></div>
<div class="m2"><p>چون چشمه خورشید فلک منبع نوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف ار نکند بر رخت آرام عجب نیست</p></div>
<div class="m2"><p>کس بر سر آتش ننمودست صبوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با زلف بگو کابن یمین میخرد از تو</p></div>
<div class="m2"><p>یکموی بجانی بچه در بند قصوری</p></div></div>