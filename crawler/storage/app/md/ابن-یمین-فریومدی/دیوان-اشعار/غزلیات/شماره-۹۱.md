---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>ای لعل درفشان تو کرده بیان روح</p></div>
<div class="m2"><p>نام تو داده خلق جهانرا نشان روح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بسکه روحم از شکرت یافت تربیت</p></div>
<div class="m2"><p>جز شکر شکرت نسراید زبان روح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جسم ترا بعالم خاکی چه نسبت است</p></div>
<div class="m2"><p>روحست جان عالم و جسم تو جان روح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از من مکن کناره که عمریست تا بمهر</p></div>
<div class="m2"><p>میپرورم وفای تو را در میان روح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روح مجسم است وجود تو ز آنسبب</p></div>
<div class="m2"><p>باشد نهان ز دیده خلقان بسان روح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویند روح را نبود در جهان مکان</p></div>
<div class="m2"><p>یاقوت آبدار تو اینک مکان روح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشتی ز جور عشق تو ابن یمین هلاک</p></div>
<div class="m2"><p>میگون لب تو گر نشدی در ضمان روح</p></div></div>