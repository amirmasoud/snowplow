---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>هرگز از یاد نخواهد شدنم صحبت دوست</p></div>
<div class="m2"><p>کی فراموش شود چون همه هستی من اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی سهی سرو و سمن سای تو ایجان جهان</p></div>
<div class="m2"><p>همچو اوراق دلم خون جگر تو بر توست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا برفتی ز برم در نظرم قامت تو</p></div>
<div class="m2"><p>راست مانند سهی سرو روان بر لب جوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفسی وصل ترا من بجهانی ندهم</p></div>
<div class="m2"><p>که قناعت نکنند اهل دل از مغز بپوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر کشد مهر رخت بر دل من تیغ رواست</p></div>
<div class="m2"><p>هر بدی کز طرف دوست رسد جمله نکوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان بکام دل دشمن ندهد پس چکند</p></div>
<div class="m2"><p>آنجگر سوخته ئی کش نرسد دست بدوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه بکام از تو چنین دور فتاد ابن یمین</p></div>
<div class="m2"><p>چه کند دور فلک را چو ستم عادت و خوست</p></div></div>