---
title: >-
    شمارهٔ ۱۵۸
---
# شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>تعالی الله چه رویست آن که دارد ترک سیمین بر</p></div>
<div class="m2"><p>ندیده چون خیال او بتی هرگز بخواب اندر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تماشاگاه جانها را بباغ حسن او صانع</p></div>
<div class="m2"><p>بگرد چشمه حیوان نبات انگیخت چون شکر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پی مور است بر شکر خطش یا دود بر آتش</p></div>
<div class="m2"><p>و یا بر آینه زنگست و یا بر آب نیلوفر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهی دریای حسن او که چون موجی بر انگیزد</p></div>
<div class="m2"><p>شود بر ساحتش پیدا زهر سو توده عنبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دمادم چشم بیمارش ز خونم میخورد شربت</p></div>
<div class="m2"><p>از آن هر لحظه بیماریش آید در نظر خوشتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گدای کوی آن مهوش بشاهی گردن افرازد</p></div>
<div class="m2"><p>اگر بختش نهد بر سر ز خاک پای او افسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلاف دوستان کردی نگارینا چه بر بردی</p></div>
<div class="m2"><p>خلاف آخر تو خود دانی که هرگز می نیارد بر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیا بر چشم من بنشین که تا هر لحظه در پایت</p></div>
<div class="m2"><p>فشاند مردم چشمم هزاران دانه گوهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر ابن یمین روزی بخلوت با تو بنشیند</p></div>
<div class="m2"><p>نسیم زلف تو گردد جهانی را بدو رهبر</p></div></div>