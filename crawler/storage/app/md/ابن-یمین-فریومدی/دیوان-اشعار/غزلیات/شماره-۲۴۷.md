---
title: >-
    شمارهٔ ۲۴۷
---
# شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>یا رب رخ دلدارست یا ماه تمامست این</p></div>
<div class="m2"><p>طوطی شکر افشان شد یا ذوق کلامست این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خالش نگر و زلفش از بهر شکار دل</p></div>
<div class="m2"><p>از مشک سیه دانه و ز غالیه دامست این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دایره خطش سطح مه تابان بین</p></div>
<div class="m2"><p>از صبح دوم نوری در ظلمت شامست این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر راست همی پرسی کو چون قد او سروی</p></div>
<div class="m2"><p>بر جای نماندست این طاوس خرامست این</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل مست غم عشقش از بزم الست آمد</p></div>
<div class="m2"><p>دیگر مدهیدش می زیرا که خرابست این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عشق تو مشتاقان پختند بسی سودا</p></div>
<div class="m2"><p>اما نکند سودی چون نزد تو خامست این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر ابن یمین بر تن بهر قدمت دارد</p></div>
<div class="m2"><p>از گردن این مفلس بردار که وامست این</p></div></div>