---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>بر وصالش یک نفس گر دسترس باشد مرا</p></div>
<div class="m2"><p>حاصل عمر عزیز آن یکنفس باشد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم افکندن ز دست دل سراندر پای دوست</p></div>
<div class="m2"><p>گر زمن بپذیردش این فخر بس باشد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشقم بر روی جانان هر که خواهی گو بدان</p></div>
<div class="m2"><p>عشق او بر من حرام ار بیم کس باشد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنده خاص ملک ز آنم که تا در روز و شب</p></div>
<div class="m2"><p>نی ز شحنه بیم و نی ترس از عسس باشد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان فدای شکر شیرین شور انگیز او</p></div>
<div class="m2"><p>کز فراقش دست بر سر چون مگس باشد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با رخ او ننگرم در هر که کج طبعی بود</p></div>
<div class="m2"><p>با گل ار خاطر بسوی خار و خس باشد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا نبیند بلبل طبعم گل رخسار او</p></div>
<div class="m2"><p>گر جهان باغ ارم گردد قفس باشد مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در سر من نیست الا وصل آن دلبر هوس</p></div>
<div class="m2"><p>تا سرم بر جای باشد این هوس باشد مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در میان ما حجاب ابن یمین افتاد و بس</p></div>
<div class="m2"><p>گر شود یکسر به جانان دسترس باشد مرا</p></div></div>