---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>جز رخ یار من بهار که دید</p></div>
<div class="m2"><p>چون قدش سرو جویبار که دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمه آب خضر جز دهنش</p></div>
<div class="m2"><p>معدن در شاهوار که دید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طرب افزای تر ز یاقوتش</p></div>
<div class="m2"><p>باده لعل خوشگوار که دید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیر دردانه بر بناگوشش</p></div>
<div class="m2"><p>ماهرا زهره گوشوار که دید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاخ سنبل بلطف طره او</p></div>
<div class="m2"><p>کشته بر چین لاله زار که دید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون من از عاشقانش گرم روی</p></div>
<div class="m2"><p>گر چه هستند صد هزار که دید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دستش از پا فکند ابن یمین</p></div>
<div class="m2"><p>دست کس را چنین نگار که دید</p></div></div>