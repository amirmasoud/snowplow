---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>تا سنبل سیراب تو بر لاله گره شد</p></div>
<div class="m2"><p>خورشید تو گفتی که مگر زیر زره شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسکین دل زارم هدف تیر بلا گشت</p></div>
<div class="m2"><p>آندم که کمان خم ابروت بزه شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>احرام طواف سر کوی تو گرفتیم</p></div>
<div class="m2"><p>چون ابروی تو قبله جان که و مه شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمت ز کمان خم ابروی تو تیری</p></div>
<div class="m2"><p>بگشاد ز سر گوشه بر آورده زه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم صفت زلف چو جیم تو توان کرد</p></div>
<div class="m2"><p>خود زای زبانها صفت از عجز گره شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با حسن تو دل نرد هوس باخت ولیکن</p></div>
<div class="m2"><p>جان در گرو حسن تو بسیار فره شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلطان غمت دست چو بگشاد به بیداد</p></div>
<div class="m2"><p>عقلم که رئیس ده دل بود ز ده شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آرزوی سیب زنخدان تو اشکم</p></div>
<div class="m2"><p>چون آب انار آمد و رخساره چو به شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با گریه و با آه دل ابن یمین باش</p></div>
<div class="m2"><p>زین آب و هوا نرگس بیمار تو به شد</p></div></div>