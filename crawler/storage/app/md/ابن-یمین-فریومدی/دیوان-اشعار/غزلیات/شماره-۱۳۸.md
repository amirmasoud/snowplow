---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>ماهروی من اگر پرده ز رخ بگشاید</p></div>
<div class="m2"><p>فلک از عکس ویم ماه دگر بنماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ببیند رخ چون آتش رخشنده در آب</p></div>
<div class="m2"><p>دل خود را چو دل خلق جهان برباید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون رقم بر بقم از نیل صبوحیش زنند</p></div>
<div class="m2"><p>دیده بر نیل رخم آب بقم بگشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق را طعنه دشمن نکند دور ز دل</p></div>
<div class="m2"><p>هیچکس پیکر خورشید بگل ننداید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از غم رسته دندانش چو لولو است مرا</p></div>
<div class="m2"><p>جزع مانند صدف گوهر تر میزاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میکند میل رسن بازی زلفش دل من</p></div>
<div class="m2"><p>این چه سودای محالست که می پیماید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بر آرد بزبان ابن یمین وصف لبش</p></div>
<div class="m2"><p>هست ماننده طوطی که شکر میخاید</p></div></div>