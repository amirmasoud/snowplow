---
title: >-
    شمارهٔ ۳۲۳
---
# شمارهٔ ۳۲۳

<div class="b" id="bn1"><div class="m1"><p>نگارینا نمیشاید ترا گفتن برخ ماهی</p></div>
<div class="m2"><p>که در حسنت کسی همتا ندید از ماه تا ماهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نور روی تو خورشید اگر نه ذره ئی بودی</p></div>
<div class="m2"><p>برین پیروزه او رنگش مسلم کی شدی شاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم را ز آتش اندوه بآب لطف برهاند</p></div>
<div class="m2"><p>ز خاک پایت ار گردی کند با باد همراهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو قصد جان من داری و من روی تو میخواهم</p></div>
<div class="m2"><p>ترا آئین بدی کردن مرا عادت نکو خواهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن بر من ستم چندین که بر آئینه حسنت</p></div>
<div class="m2"><p>نشاند ناگهان زنگی دلم ز آه سحرگاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببازار غم عشقت کسی را میرسد سودا</p></div>
<div class="m2"><p>که سود جان خود داند زیان مالی و جاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه غم ابن یمین را ز آن که جان شد در سر کارت</p></div>
<div class="m2"><p>غمش گر هست ز آن باشد که از حالش نه آگاهی</p></div></div>