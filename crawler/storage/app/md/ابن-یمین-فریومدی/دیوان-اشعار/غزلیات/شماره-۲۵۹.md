---
title: >-
    شمارهٔ ۲۵۹
---
# شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>آمد آن سرو سهی بر رخ نقاب انداخته</p></div>
<div class="m2"><p>سایه شعر سیه بر آفتاب انداخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر کشیده لاله گلبوی را نیل صبوح</p></div>
<div class="m2"><p>سنبل سیراب را در پیچ و تاب انداخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عارضش غرق عرق از می ولی با رنگ و بوی</p></div>
<div class="m2"><p>بود گوئی سیب سرخ اندر گلاب انداخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تار باید دل ز هشیاران و پنداری که هست</p></div>
<div class="m2"><p>نرگس مست ترا در نیم خواب انداخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرده چوگان از کمند زلف مشک افشان خویش</p></div>
<div class="m2"><p>گوی دلها را زغم در اضطراب انداخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هوای آتش رخسار چون گلنار تو</p></div>
<div class="m2"><p>من چو نیلوفر سپر بر روی آب انداخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از هوای خاک پایش آب چشم پر نمم</p></div>
<div class="m2"><p>آتش اندوه را در التهاب انداخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دل ابن یمین مهر رخ چون ماه تو</p></div>
<div class="m2"><p>گنج آبادست در کنج خراب انداخته</p></div></div>