---
title: >-
    شمارهٔ ۲۶۷
---
# شمارهٔ ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>سحرگه آن صنم سرو قد شتاب زده</p></div>
<div class="m2"><p>درآمد از در ابن یمین شراب زده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عرق نشسته ز می بر عذار نازک او</p></div>
<div class="m2"><p>چنانکه بر ورق یاسمین گلاب زده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکنج طره او بر رخش فتاده چنانک</p></div>
<div class="m2"><p>ز رأس عقده مشکین بر آفتاب زده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لبش چو دید دلم را کباب زآتش عشق</p></div>
<div class="m2"><p>نمک بگاه شکر خنده بر کباب زده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزیر خط رخ خوب ترا چنان دیدم</p></div>
<div class="m2"><p>که آتشست بر او آب مشک ناب زده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلطف گفتمش ای نازنین تو عمر منی</p></div>
<div class="m2"><p>بدان دلیل که هستی چنین شتاب زده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منم که مردم چشم خیال پیمایم</p></div>
<div class="m2"><p>که تا خیال تو دیده دلش ز خواب زده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در سرای ترا هر سحر چو فراشان</p></div>
<div class="m2"><p>برفته خاک بمژگان خویش و آب زده</p></div></div>