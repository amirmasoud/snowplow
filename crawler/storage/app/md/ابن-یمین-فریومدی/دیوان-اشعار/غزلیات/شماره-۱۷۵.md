---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>تا سپهر حسن را رخسار تو ماهست و بس</p></div>
<div class="m2"><p>هر سحر در عشق تو کار دلم آهست و بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیتو زارم آنچنان کز زندگیم اصحاب را</p></div>
<div class="m2"><p>هیچ اگر هست آگهی ز آه سحر گاهست و بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم مخمورت ز بیماری من دارد خبر</p></div>
<div class="m2"><p>وز پریشانی حالم زلفت آگاهست و بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد ره گم میکند در تیرگی زلف تو</p></div>
<div class="m2"><p>تا نپنداری دل بیچاره گمراهست و بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر بازار عشقت عقل سودائی من</p></div>
<div class="m2"><p>سود خود داند زیان گر مال و گر جاهست و بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کسیرا در عبادت روی سوی قبله ایست</p></div>
<div class="m2"><p>قبله اقبال من آنروی چون ماهست و بس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رخ چه پنهان میکنی ز ابن یمین کاندر جهان</p></div>
<div class="m2"><p>خود همیدانی که از جانت نکو خواهست و بس</p></div></div>