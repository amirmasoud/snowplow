---
title: >-
    شمارهٔ ۱۷۸
---
# شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>ای ز جام می عشق تو خرد رفته ز هوش</p></div>
<div class="m2"><p>لب و دندان ترا لعل و گهر حلقه بگوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عکس یاقوت لبت سوی بدخشان بردند</p></div>
<div class="m2"><p>آمد اندر رگ کان خون دل لعل بجوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاسخ تلخ تو و خنده شیرین با هم</p></div>
<div class="m2"><p>نوش در نیش نهان گشته و نیش اندر نوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی زیبای تو از زلف گره کردارت</p></div>
<div class="m2"><p>گشته چون آب ز باد سحری جوشن پوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش سیلاب غمم تا بسر زانو بود</p></div>
<div class="m2"><p>امشب ایدوست چه تدبیر که بگذشت ز دوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل من بسته زنجیر سر زلف تو شد</p></div>
<div class="m2"><p>با گرفتار خود ای سست وفا سخت مکوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عهد بستی که در وصل گشائی بر من</p></div>
<div class="m2"><p>چو وفا نیستت ای دلبر بد عهد خموش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخت با ابن یمین دست در آغوش کند</p></div>
<div class="m2"><p>گر شود با تو شبی تا بسحر دست آغوش</p></div></div>