---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>هر کس که بکام از سر کوی تو گذر کرد</p></div>
<div class="m2"><p>از ساده دلی روی ز جنت بسقر کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آنکو بکمان گوشه ابروی تو دل داد</p></div>
<div class="m2"><p>بر رهگذر تیر بلا سینه سپر کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم که ز دست غم تو جان نبرد دل</p></div>
<div class="m2"><p>آنروز که چشمم بجمال تو نظر کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر فتنه که چشم خوشت افکند در آفاق</p></div>
<div class="m2"><p>شد روشنم از روی تو کان دور قمر کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل از رخ زیبای تو میداد نشانی</p></div>
<div class="m2"><p>زین مژده صبا در دهنش خرده زر کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باد از شکن زلف تو بوئی بختا برد</p></div>
<div class="m2"><p>آهو ز حسد نافه پر از خون جگر کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در یاب کنون ابن یمین را که به ناگاه</p></div>
<div class="m2"><p>پرسی که کجا رفت بگویند سفر کرد</p></div></div>