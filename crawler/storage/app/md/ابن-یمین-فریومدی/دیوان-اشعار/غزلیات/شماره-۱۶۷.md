---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>صبحدم باد صبا آمد و آورد خبر</p></div>
<div class="m2"><p>که بصد ناز رسد آن مه تابان ز سفر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون صبا مژده رسانید که دلدار رسید</p></div>
<div class="m2"><p>مرده بودم ز غمش زنده شدم بار دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جهان عشق من و حسن بتم پیدا شد</p></div>
<div class="m2"><p>هیچ پیدا نبود بر دل اصحاب نظر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر میانش کمر از ساعد من میباید</p></div>
<div class="m2"><p>ای دریغا چو کمر دسترسم نیست بزر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بتیرم بزند ترک کمان ابروی من</p></div>
<div class="m2"><p>چاره ئی نیست جز اینم که کنم سینه سپر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تب عشق مرا دوست فسون می نکند</p></div>
<div class="m2"><p>خط چون شهپر طوطی چه کند گردشگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردم از آتش سودای خط مشکینش</p></div>
<div class="m2"><p>می رود ابن یمین را چو قلم دود بسر</p></div></div>