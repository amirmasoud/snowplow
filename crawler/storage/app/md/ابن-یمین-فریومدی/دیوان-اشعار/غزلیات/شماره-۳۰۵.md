---
title: >-
    شمارهٔ ۳۰۵
---
# شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>چوگان ز مشک بر مه تابان کشیده‌ای</p></div>
<div class="m2"><p>مه را چو کوی در خم چوگان کشیده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آورده‌ای ز شعر سیه سایبان حسن</p></div>
<div class="m2"><p>بر فرق آفتاب در فشان کشیده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن خط سبز فام که خضر است نام او</p></div>
<div class="m2"><p>خوش بر کنار چشمه حیوان کشیده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جان و دل که یافته‌ای در کمند عشق</p></div>
<div class="m2"><p>مجموع را به زلف پریشان کشیده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارد هوای دانه خال تو مرغ روح</p></div>
<div class="m2"><p>با آنک دام بر زبر آن کشیده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر میان جان چو الف جایگیر شد</p></div>
<div class="m2"><p>قدت که راست چون الف جان کشیده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون اشک عاشقانت لطیفست و آبدار</p></div>
<div class="m2"><p>گوهر که زیر لعل بدخشان کشیده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم بد از تو دور که در مصر دلبری</p></div>
<div class="m2"><p>خط در جمال یوسف کنعان کشیده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم بر آستان تو جان کرده‌ام نثار</p></div>
<div class="m2"><p>گفتی که باز زیره به کرمان کشیده‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی‌یاد تو نمی‌زند ابن‌یمین دمی</p></div>
<div class="m2"><p>در نام او چرا خط نسیان کشیده‌ای</p></div></div>