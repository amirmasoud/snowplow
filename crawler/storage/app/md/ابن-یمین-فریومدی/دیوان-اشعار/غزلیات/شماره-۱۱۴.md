---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>خطش از ریحان طرازی بر گل سوری کشید</p></div>
<div class="m2"><p>نرگس از مستی چشمش رنج مخموری کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از می عشقش برندی و بقلاشی فتاد</p></div>
<div class="m2"><p>هر که روزی در جهان نامی بمستوری کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سر سرو سهی تا گل ببار آمد ترا</p></div>
<div class="m2"><p>غنچه دلها چو نرگس از تو رنجوری کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پسته شکر فشانت بسکه شیرینکار شد</p></div>
<div class="m2"><p>رخ ز شرمش انگبین در ستر مسروری کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا خراب آباد دل را عشق تو معمار شد</p></div>
<div class="m2"><p>عقل کاستادی نمودی بار مزدوری کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دلم شاکر نگشتی تا نشد خون در غمت</p></div>
<div class="m2"><p>شکر ایزد را که سعی دل بمشکوری کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کو شب وصلی که تا ابن یمین در بندگیت</p></div>
<div class="m2"><p>عرضه دارد آنچه دل از درد مهجوری کشید</p></div></div>