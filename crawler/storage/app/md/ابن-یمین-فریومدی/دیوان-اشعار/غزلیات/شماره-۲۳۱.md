---
title: >-
    شمارهٔ ۲۳۱
---
# شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>ای نرگس مست تو برده دل هشیاران</p></div>
<div class="m2"><p>با عقل خود اغیارند از عشق رخت یاران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف تو کند پیدا احوال پریشانم</p></div>
<div class="m2"><p>چشم تو برد دل را با خانه بیماران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نیست ترا رحمت بر حالت من شاید</p></div>
<div class="m2"><p>خفته چه خبر دارد از حالت بیداران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر خاک سر کویت بادی که گذر یابد</p></div>
<div class="m2"><p>آتش زند از غیرت بر کلبه عطاران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا باده فروش آمد لعل لب میگونت</p></div>
<div class="m2"><p>افتاد بسی نقصان در مکسب خماران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پر مکر و فریبست آن چشم خوش مستانه</p></div>
<div class="m2"><p>زنهار بترس ای دل از فتنه مکاران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا ابن یمین دارد مهر رخ تو در دل</p></div>
<div class="m2"><p>دیوانگئی هستش مانند پری داران</p></div></div>