---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>یا رب این بوی خوش از روضه رضوان برخاست</p></div>
<div class="m2"><p>یا نسیم سحر از ساحت بستان برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا ز چین سر زلفین چو شام بت من</p></div>
<div class="m2"><p>صبحدم باد صبا غالیه افشان برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوی پیراهن یوسف مگر از جانب مصر</p></div>
<div class="m2"><p>از پی راحت یعقوب بکنعان برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرمه روشنی چشم جهان بین من است</p></div>
<div class="m2"><p>هر غباری که ز خاک در جانان برخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان فدای خط مشکینت که چون مهر گیاه</p></div>
<div class="m2"><p>سبز و خرم ز لب چشمه حیوان برخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یافت طوطی خطت خضر صفت آبحیات</p></div>
<div class="m2"><p>گر چه اول بهوای شکرستان برخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خال مشکین تو بر آتش رخ همچو سپند</p></div>
<div class="m2"><p>سوخت در آتشت این دود سیه زان برخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غمزه مست تو در خون دلم دارد دست</p></div>
<div class="m2"><p>زودم از پای در آرد چو بدستان برخاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر بپای تو در افکندم و عقلم میگفت</p></div>
<div class="m2"><p>مور با پای ملخ پیش سلیمان برخاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخن زلف تو ناگه بزبان آوردم</p></div>
<div class="m2"><p>از دهانم صفتش سخت پریشان برخاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر نشیند سخن ابن یمین در دل خلق</p></div>
<div class="m2"><p>چه عجب آن نه چو سوزیست که از جان برخاست</p></div></div>