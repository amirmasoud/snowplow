---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>آمد بهار و وقت نشاطست می بیار</p></div>
<div class="m2"><p>می مایه نشاط بود خاصه در بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبع هوا چو میل سوی اعتدال کرد</p></div>
<div class="m2"><p>یک وزن شد دو کفه میزان روزگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم بوی عود یافت ز خاک چمن صبا</p></div>
<div class="m2"><p>هم آب صندل است روان سوی جویبار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون گل شکفت باده گلگون ز کف منه</p></div>
<div class="m2"><p>کز خاک تو نه دیر که خواهد دمید خار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کش چو خارپشت ز نا جنس خویش سر</p></div>
<div class="m2"><p>می نوش تا شوی چو کشف در کلوخ زار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می نوش اگر چه شرع برین طعن میکند</p></div>
<div class="m2"><p>کاین یک مضرتش بود و منفعت هزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می نوش و نا امید ز غفران حق مباش</p></div>
<div class="m2"><p>کافزون ز جرم بنده بود عفو کردگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابن یمین بکوش که هنگام کار تست</p></div>
<div class="m2"><p>جائی همی روی که درو نیست هیچکار</p></div></div>