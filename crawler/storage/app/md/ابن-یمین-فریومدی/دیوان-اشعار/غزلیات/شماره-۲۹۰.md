---
title: >-
    شمارهٔ ۲۹۰
---
# شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>بحسن روی تو ای آفتاب خرگاهی</p></div>
<div class="m2"><p>ندید دیده گردون ز ماه تا ماهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توئیکه رنگ رخت را جهانیان گویند</p></div>
<div class="m2"><p>که چشم بد مرسادت که صبغه اللهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر رسائی قد تو باغبان بیند</p></div>
<div class="m2"><p>هزار طعنه زند سرو را بکوتاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عشق سلسله زلف عنبرینت دلم</p></div>
<div class="m2"><p>نهاد روی بدیوانگی و گمراهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا که در هوس زلف شام پیکر تو</p></div>
<div class="m2"><p>تنم چو نال شد از ناله سحرگاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه حاجتست بخورشید و ماه با رخ تو</p></div>
<div class="m2"><p>مرا بروز تو خورشیدی و بشب ماهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا که روی تو میخواهد و تو میدانی</p></div>
<div class="m2"><p>که هست عادت ابن یمین نکو خواهی</p></div></div>