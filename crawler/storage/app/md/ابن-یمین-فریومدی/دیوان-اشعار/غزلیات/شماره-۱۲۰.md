---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>ز آنروی طره پر رخ دلدار کج نهند</p></div>
<div class="m2"><p>کاهل خرد طریقه طرار کج نهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرد مناسبت برخ و زلف یار من</p></div>
<div class="m2"><p>گر شاخ سنبل از بر گلنار کج نهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حقا که لاف راستی از سرو بوستان</p></div>
<div class="m2"><p>در پیش قامتش گه رفتار کج نهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سر کلاه حسن نهد شاه اختران</p></div>
<div class="m2"><p>خوبان ز راه ناز چو دستار کج نهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کوی عشق راست نهادند جمله روی</p></div>
<div class="m2"><p>آنها که پای بر سر بازار کج نهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی که با تو راست دلم بر مگرد از آن</p></div>
<div class="m2"><p>انکار کردن از پس اقرار کج نهند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرگار عاشقان خم ابروی جفت تست</p></div>
<div class="m2"><p>در طاقش ار چه قاعده کار کج نهند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر جان طلب کنی بدهم ز انکه اهل دل</p></div>
<div class="m2"><p>کردن بجان مضایقه با یار کج نهند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ابن یمین بعشق تو جان داد و دم نزد</p></div>
<div class="m2"><p>زیرا که عاشقان همه گفتار کج نهند</p></div></div>