---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>شبی خیال تو بر من بصد دلال گذشت</p></div>
<div class="m2"><p>غلام خوابم از آنشب که آنخیال گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آمد از تتق غیب چون غزاله ز میغ</p></div>
<div class="m2"><p>بمن نمود رخ از دور و چون غزال گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان نمود مرا در نظر ز غایت لطف</p></div>
<div class="m2"><p>که پیش تشنه تو گوئی مگر زلال گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخاک پای تو کاندر صفت نمیآید</p></div>
<div class="m2"><p>که بر سرم زغم هجر تو چه حال گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب فراق تو با روز حشر می مانست</p></div>
<div class="m2"><p>کز امتداد ز پنجه هزار سال گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امید هست که نقصان پذیردم غم دل</p></div>
<div class="m2"><p>بدان دلیل که از غایت کمال گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیام دادم و گفتم ز رنج فرقت تو</p></div>
<div class="m2"><p>به لاغری تن زار من از هلال گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ازین سپس نتواند کشید ابن یمن</p></div>
<div class="m2"><p>بلای عشق تو کز حد اعتدال گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جواب داد که بانگ نماز در باقی</p></div>
<div class="m2"><p>نرفت اگر چه که دیریست تا بلال گذشت</p></div></div>