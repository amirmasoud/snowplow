---
title: >-
    شمارهٔ ٩٩ - وله ایضاً
---
# شمارهٔ ٩٩ - وله ایضاً

<div class="b" id="bn1"><div class="m1"><p>جهان جود و کرم ای پناه اهل نیاز</p></div>
<div class="m2"><p>بروی خلق در خرمی ز لطف تو باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکوه و حشمت و اورنگ تاج دولت و دین</p></div>
<div class="m2"><p>که دین ز دولت تو یافت صد سعادت باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توئی بمرتبه شاهی که بندد از پی نام</p></div>
<div class="m2"><p>کمر به پیش تو محمود بنده وش چو ایاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا نظیر بگیتی ندید گر چه بسی</p></div>
<div class="m2"><p>بگشت گرد زمین آسمان بعمر دراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صفای آینه رأی تو کند پیدا</p></div>
<div class="m2"><p>برین صحیفه زنگار فام صورت راز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر چه رأی تو روی آورد رضا ندهد</p></div>
<div class="m2"><p>بدین قدر که قضا باشدش در آن همباز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بعهد عدل تو گر کبک را رسد ستمی</p></div>
<div class="m2"><p>بمأمنی نپناهد بجز نشیمن باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شود بقوت عدل تو پشه پیل افکن</p></div>
<div class="m2"><p>سعادت ار دهدش در هوای تو پرواز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فلک چو صدمت گرز تو دید بر سر خصم</p></div>
<div class="m2"><p>چه گفت گفت که کوپال بیژنست و گراز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بهر نصرت و فیروزی کتابه تست</p></div>
<div class="m2"><p>که وقت جنگ بدشمن چو میرسند فراز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر بروز بود آفتاب تیغ گذار</p></div>
<div class="m2"><p>و گر بوقت شبیخون سپهر تیرانداز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز مهر رأی تو پروانه ئی رسد بسها</p></div>
<div class="m2"><p>فتد ز تابش او شمع آسمان بگداز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهانپناه شها بنده تو ابن یمین</p></div>
<div class="m2"><p>که هست در هنر از جنس خویشتن ممتاز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>امید تربیتش هست و دست آن داری</p></div>
<div class="m2"><p>که یابد از کرمت صد هزار نعمت و ناز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کسی که بود بدوران تو برهنه چو سیر</p></div>
<div class="m2"><p>ز خلعتت همه تن جامه شد بسان پیاز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شکم ز خوان عطای تو چار پهلو کرد</p></div>
<div class="m2"><p>اگر چه بود گرفتار جوع کلبی آز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فضایل تو ز اندازه بیش و نقد سخن</p></div>
<div class="m2"><p>مرا کمست و روا باشد ار کنم ایجاز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بمن رسید ز غیری لطیفه ئی که در اوست</p></div>
<div class="m2"><p>عروس فکر مرا درگه زفاف جهاز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کنم بصورت تضمین ادا که آن سخن است</p></div>
<div class="m2"><p>ز بهر بنده حقیقت ز بهر غیر مجاز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هنر مگیر و فصاحت مگیر و فضل مگیر</p></div>
<div class="m2"><p>نه من غریبم و شاه جهان غریب نواز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بحق نعمت عامت که من بدولت تو</p></div>
<div class="m2"><p>که غیر او نکند اهل فضل را اعزاز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بحاتم ار بجهان آید التجا نکنم</p></div>
<div class="m2"><p>باستخوان رسد ار کاردم ز دست نیاز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همیشه تا بگه شیون و بموسم سور</p></div>
<div class="m2"><p>ز ساز و سوز درآید بکوهسار آواز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بگوش تو مرساد از دیار دشمن و دوست</p></div>
<div class="m2"><p>بهیچ حال جز آواز سوز و ناله ساز</p></div></div>