---
title: >-
    شمارهٔ ۵۴ - قصیده در مدح نظام الدین یحیی کرایی
---
# شمارهٔ ۵۴ - قصیده در مدح نظام الدین یحیی کرایی

<div class="b" id="bn1"><div class="m1"><p>شاه عالم روز کین چون قصد دشمن می‌کند</p></div>
<div class="m2"><p>تیغش از یک تن دو و تیر از دو یک تن می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه شاهان جهان آنکو به گرز گاوسار</p></div>
<div class="m2"><p>شیرمردان را به گاه حمله چون زن می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرور گردنکشان سلطان نظام ملک و دین</p></div>
<div class="m2"><p>آنکه ملک و دین ازو فرش مزین می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب تیغش می‌دهد چون خاک خصمش را به باد</p></div>
<div class="m2"><p>همچو آتش گرچه حصن از سنگ و آهن می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز رزم از بس که ریزد خون بدخواهان ملک</p></div>
<div class="m2"><p>شاخ و برگ سبزه را چون بیخ روین می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر سپرداری بود عادت عدو را همچو ماه</p></div>
<div class="m2"><p>ور چو ماهی بر تن خود پوست جوشن می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز کین با وی کند تیر از کمان سخت شاه</p></div>
<div class="m2"><p>آنچ با برد یمانی نوک سوزن می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دل خصمش که باشد خانه تاریک و تنگ</p></div>
<div class="m2"><p>تیغ روشن گوهرش صد گونه روزن می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سینه دشمن چو گندم می‌شکافد خنجرش</p></div>
<div class="m2"><p>وز وجودش هر جوی صد دانه ارزن می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکه با خسروپرستان می‌زید گرگین‌صفت</p></div>
<div class="m2"><p>خشم شه بر وی جهان چون چاه بیژن می‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سائلان را گاه بزم و بددلان را روز رزم</p></div>
<div class="m2"><p>همتش قارون و دلگرمیش قارن می‌کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می‌دهد بی‌شک عنان مرکب دولت ز دست</p></div>
<div class="m2"><p>از رکاب عزش آنکو عزم رفتن می‌کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با سرخود هم به دست خود همی‌آرد صداع</p></div>
<div class="m2"><p>ابلهی کز بیخ اشتر خار چندن می‌کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دوستدارش را ز لطفش وقت شیون هست سور</p></div>
<div class="m2"><p>دشمن از عنفش به گاه سور شیون می‌کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باد اگر بویی ز خلقش سوی دوزخ می‌برد</p></div>
<div class="m2"><p>گلخنش را چون بهشت از لطف گلشن می‌کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای شهنشاهی که درگاه ترا از حادثات</p></div>
<div class="m2"><p>هرکه بختش رهنمایی کرد مأمن می‌کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شد مجره چون فلاخن آسمان از مهر و ماه</p></div>
<div class="m2"><p>بهر زخم دشمنش سنگ فلاخن می‌کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نکته‌ای کو بود مخفی تاکنون از سر غیب</p></div>
<div class="m2"><p>کلک‌سا آن را به آب تیره روشن می‌کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرکه می‌گوید که بزمت هست چون باغ ارم</p></div>
<div class="m2"><p>گلشن فردوس را نسبت به گلخن می‌کند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر دهد گردون به خصمت نعمتی هیچش مگوی</p></div>
<div class="m2"><p>از پی کشتن چو مرغ او را مسمن می‌کند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شهریارا خاطر ابن یمین از مدح تو</p></div>
<div class="m2"><p>چون فصاحت را به صد برهان مبرهن می‌کند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گوش می‌گردد در اصغا چون بنفشه جمله تن</p></div>
<div class="m2"><p>هرکه دائم ده زبانی همچو سوسن می‌کند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا به گیتی منشی گردون از ارباب سخن</p></div>
<div class="m2"><p>هر یکی را منصبی در خور معین می‌کند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>منصبی بادت که مدحت را عطارد تا ابد</p></div>
<div class="m2"><p>بر بیاض مهر و مه دائم مدون می‌کند</p></div></div>