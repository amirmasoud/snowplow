---
title: >-
    شمارهٔ ١٣۵ - وله ایضاً
---
# شمارهٔ ١٣۵ - وله ایضاً

<div class="b" id="bn1"><div class="m1"><p>یا رب چه موجبست که دستور شه نشان</p></div>
<div class="m2"><p>والا جلال دولت و دین آصف زمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهر سپهر دانش و جان و جهان فضل</p></div>
<div class="m2"><p>حامی ملک و ملت وراعی انس وجان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی نپرسد از ره اشفاق و مرحمت</p></div>
<div class="m2"><p>مولای خویش ابن یمین را که ای فلان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونی و در چه کار و درین موسم از چه خاست</p></div>
<div class="m2"><p>عزم توجهت بجناب خدایگان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلطان نظام دولت و دین شاه تاج بخش</p></div>
<div class="m2"><p>کز قدر هست پایه تختش بر آسمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای پیش رأی انور تو گشته آشکار</p></div>
<div class="m2"><p>سری که هست در حجب آسمان نهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از راه انبساط سئوالی همیکنم</p></div>
<div class="m2"><p>تشریف ده ز بنده نوازی جواب آن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مفتی شرع مکرمت امروز رأی تست</p></div>
<div class="m2"><p>باشد روا که همچو منی شهره جهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر آستان حضرت خورشید ملک و دین</p></div>
<div class="m2"><p>باشم بسان ذره در سایه بی نشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاصه کنون که چون تو هنر پروری بود</p></div>
<div class="m2"><p>در ملک شاه داور و دارا و قهرمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر رأی شاه اگر نکنی حال بنده عرض</p></div>
<div class="m2"><p>زودا که بر فلک رسدم ناله و فغان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا از جهانیان بود اندر جهان اثر</p></div>
<div class="m2"><p>بی تو جهان مباد مقام خدایگان</p></div></div>