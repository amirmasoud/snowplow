---
title: >-
    شمارهٔ ٣١ - قصیده در مدح خاتم الانبیاءعلی مرتضی و بقیه امامان هدی
---
# شمارهٔ ٣١ - قصیده در مدح خاتم الانبیاءعلی مرتضی و بقیه امامان هدی

<div class="b" id="bn1"><div class="m1"><p>مظهر نور نخستین ذات پاک مصطفاست</p></div>
<div class="m2"><p>مصطفی کو اولین و آخرین انبیاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه هستی بر طفیلش حاصل است افلاک را</p></div>
<div class="m2"><p>وین نه من تنها همیگویم بدین گویا خداست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در صفات ذات پاکش زحمت اطناب نیست</p></div>
<div class="m2"><p>گفته شد او صاف او یکسر چو گفتی مصطفاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نبی بگذشت امت را امامی واجبست</p></div>
<div class="m2"><p>وین نه کاری مختصر باشد مر اینرا شرطهاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حکمتست و عصمتست و بخشش و مردانگی</p></div>
<div class="m2"><p>کژ نشین و راست میگو تا زیاران این کراست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اینصفات و زین هزاران بیش و عصمت بر سری</p></div>
<div class="m2"><p>با وصی مصطفی یعنی علی المرتضاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز علی مرتضی در بارگاه مصطفی</p></div>
<div class="m2"><p>هیچکس دیگر به دعوی سلونی بر نخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مصطفی و جمله یارانش مسلم داشتند</p></div>
<div class="m2"><p>اینچنین دعوی چو دانستند کان رمز از کجاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حجت اثبات علمش لو کشف باشد تمام</p></div>
<div class="m2"><p>از فتوت خود چگویم قائل آن هل اتاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>او باستحقاق امام است و بنص مصطفی</p></div>
<div class="m2"><p>بر سر این موجب نص نیز حکم انماست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با چنین فاضل ز مفضولی تراشیدن امام</p></div>
<div class="m2"><p>گر صواب آید ترا باری بنزد من خطاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون گذشت از مرتضی اولاد او را دان امام</p></div>
<div class="m2"><p>اولین زیشان حسن وانگه شهید کربلاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بعد ازو سجاد و آنگه باقر و صادق بود</p></div>
<div class="m2"><p>بعد از او موسی نجی الله و بعد از وی رضاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون گذشتی زو تقی را دان امام آنگه نقی</p></div>
<div class="m2"><p>پس امام عسکری کاهل هدی را پیشواست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بعد ازو صاحب زمان کز سالهای دیر باز</p></div>
<div class="m2"><p>دیده ها در انتظار روی آن فرخ لقاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون کند نور حضور او جهان را با صفا</p></div>
<div class="m2"><p>هر کژی کاندر جهان باشد شود یکباره راست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این بزرگان هر یکی را در جناب ذوالجلال</p></div>
<div class="m2"><p>از بزرگی رفعتی فوق سماوات العلاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بنده خود را گر چه حد آن نمیداند ولیک</p></div>
<div class="m2"><p>دائم از اخلاص ایشان کارش انشاء ثناست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر امید آنکه روز حشر ازینشاهان یکی</p></div>
<div class="m2"><p>گوید این ابن یمین از بندگان خاص ماست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>این عنایت بس بود ابن یمین را بهر آنک</p></div>
<div class="m2"><p>هر که باشد بنده شان در این دو دنیا پادشاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روح پاک هر یکی در جنه المأوی مقیم</p></div>
<div class="m2"><p>بود و باشد کان مقام اتقیا و اصفیاست</p></div></div>