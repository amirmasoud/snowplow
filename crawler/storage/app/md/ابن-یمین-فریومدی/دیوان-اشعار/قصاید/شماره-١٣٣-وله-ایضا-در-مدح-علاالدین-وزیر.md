---
title: >-
    شمارهٔ ١٣٣ - وله ایضاً در مدح علاءالدین وزیر
---
# شمارهٔ ١٣٣ - وله ایضاً در مدح علاءالدین وزیر

<div class="b" id="bn1"><div class="m1"><p>هوای آنرخ چون ماه و زلف غالیه گون</p></div>
<div class="m2"><p>بهیچ حال نخواهد شد از سرم بیرون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غلام دلبر خویشم که بامداد پگاه</p></div>
<div class="m2"><p>چو سر ز حبیب بر آرد بطالع میمون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روا بود که پدید آید آفتاب دگر</p></div>
<div class="m2"><p>ز عکس چهره او بر سپهر آینه گون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیام دادم و گفتم که بوسه ایم بده</p></div>
<div class="m2"><p>بگیر جان بعوض گر چه هست غرقه بخون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جواب داد که از سر برون کن این سودا</p></div>
<div class="m2"><p>که این نشان جنونست و الجنون فنون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شاخ سنبل تر بر سمن کشد مفتول</p></div>
<div class="m2"><p>شود بهر سر مویش هزار دل مفتون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فراز عارض چون روز و زلف او لیلیست</p></div>
<div class="m2"><p>که عاقلان جهانرا همی کند مجنون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبا چو سلسله زلف او بجنباند</p></div>
<div class="m2"><p>خرد برغبت خود سر بر آورد بجنون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز عشق آن دهن همچو میم و زلف چو جیم</p></div>
<div class="m2"><p>مرا شدست قد چون الف خمیده چو نون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو یاد چشمه حیوانش بگذرد بدلم</p></div>
<div class="m2"><p>ز موج چشمه چشمم خجل شود جیحون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سزد که درد دل من دوا پذیر شود</p></div>
<div class="m2"><p>اگر ز حقه لعلش بمن رسد معجون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر چه عارض دلدار من دو هفته مهست</p></div>
<div class="m2"><p>ولی بحسن چو ماه نو است روز افزون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ستم همیکند آن ماهروی بر دل من</p></div>
<div class="m2"><p>سزد که عرضه کنم حال خویشتن اکنون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به پیش سرور گیتی علاء دولت و دین</p></div>
<div class="m2"><p>که باد مهر جلال وی از زوال مصون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگاه عزم دهد خاک را چو با دشتاب</p></div>
<div class="m2"><p>بوقت حزم دهد باد را چو خاک سکون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدست سایس عدلش زبون شد ابلق دهر</p></div>
<div class="m2"><p>اگر چه سرکش و بدخوی و تند بود و حرون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>طبیب حاذق دار الشفای معدلتش</p></div>
<div class="m2"><p>بفتنه داد ز بهر نجات خلق افیون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو نعل گشت هلال و چو جل شد اطلس چرخ</p></div>
<div class="m2"><p>ز بهر موکب جاهش بحکم کن فیکون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز بهر خیمه جاهش سپهر ساخته کرد</p></div>
<div class="m2"><p>ز خیط فجر طناب از عمود صبح ستون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر عدوش کشد سر بماه چون نمرود</p></div>
<div class="m2"><p>فرو برد بزمین آسمانش چون قارون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نخست کسوت خصمش چو کرم قز کفن است</p></div>
<div class="m2"><p>تو آن مبین که بود کسوت اطلس و اکسون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بهای تره یکروزه خوان همت اوست</p></div>
<div class="m2"><p>هر آن ذخیره که در بحر و کان بود مخزون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>معالم کرمش در زمانه قانونی است</p></div>
<div class="m2"><p>که هست مختصری فیض ابراز آن قانون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هزار یک نشود گفته از فضایل او</p></div>
<div class="m2"><p>اگر هزار مجلد بدان کنم مشحون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گهی که حکمت تو با بیان کند تقریر</p></div>
<div class="m2"><p>باستفادتش آید هزار افلاطون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بزرگوار وزیرا توئی که خاطر تو</p></div>
<div class="m2"><p>نماند مشگل و صعبی که آن نکرد زبون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ضمیر روشنت از راه کشف میداند</p></div>
<div class="m2"><p>که چیست راز پس پرده سپهر درون</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زمانه خصم ترا کرد زآن بسنگ نیاز</p></div>
<div class="m2"><p>بسنگ اگر چه که گردن فراز بد چو هیون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سخن بنزد تو آوردن آنچنان باشد</p></div>
<div class="m2"><p>که سوی خطه کرمان کسی برد کمون</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز یمن مدح تو ابن یمین یساری یافت</p></div>
<div class="m2"><p>که کرد جمله جهان پر ز لولو موزون</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دعای جاه تو از هر چه گویم اولیتر</p></div>
<div class="m2"><p>مباد جز باجابت دعاء من مقرون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همیشه تا که بود گوژپشت و سرگردان</p></div>
<div class="m2"><p>زرشک رفعت جاهت سپهر انگلیون</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کسی که با تو نه بر سمت مستقیم بود</p></div>
<div class="m2"><p>خمیده قامت و سرگشته باد چون گردون</p></div></div>