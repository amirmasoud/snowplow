---
title: >-
    شمارهٔ ١١۴ - وله ایضاً
---
# شمارهٔ ١١۴ - وله ایضاً

<div class="b" id="bn1"><div class="m1"><p>این منم باز که در باغ بهشت افتادم</p></div>
<div class="m2"><p>وز سفر کآن بحقیقت سقرست آزادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این نه خوابیست که می بینم اگر پنداری</p></div>
<div class="m2"><p>کز پس آنهمه اندوه چنین دلشادم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه بیداد فلک بود ولی شکربرآنک</p></div>
<div class="m2"><p>داد لطف و کرم والی سلطان دادم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دستگیر ار نشدی حق که توانستی خاست</p></div>
<div class="m2"><p>آنچنان سخت که ناگاه ز پا افتادم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرنه فریاد رسی همچو خرد داشتمی</p></div>
<div class="m2"><p>زود بودی که رسیدی بفلک فریادم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خردم راه قناعت بنمود از ره لطف</p></div>
<div class="m2"><p>جز بدان راه که او گفت قدم ننهادم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منم آن آب قناعت زده بر آتش حرص</p></div>
<div class="m2"><p>که سراسر کره خاک نماید بادم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد چو طفلان دلم از مکتب شاگردی سیر</p></div>
<div class="m2"><p>ز آن زمان باز که پیر خرد است استادم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خالقم را شده ام خادم از اخلاص چنانک</p></div>
<div class="m2"><p>که ز مخدومی مخلوق نیاید یادم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه کنم ملک خراسان چه کشم محنت جان</p></div>
<div class="m2"><p>وقت آنست که پرسی خبر از بغدادم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرنه زین مولد و منشاست ولی سعدی گفت</p></div>
<div class="m2"><p>نتوان مرد بسختی که من اینجا زادم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زین وطن گر بروم هست خریدار بسی</p></div>
<div class="m2"><p>گوهری را که بود زاده طبع رادم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می نخواهم شدن از کوی قناعت بیرون</p></div>
<div class="m2"><p>سیل افلاس گر از بن بکند بنیادم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیرو ابن یمینم ره خرسندی پیش</p></div>
<div class="m2"><p>دو سه روزی که درین دیر خراب افتادم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نبود صحبت شیرین پسران بی شوری</p></div>
<div class="m2"><p>ز آنسبب کوه نشین بر صفت فرهادم</p></div></div>