---
title: >-
    شمارهٔ ٢٣ - قصیده در تعریف بنای مسجد جامع سبزوار و مدح تاج الدین بانی آن
---
# شمارهٔ ٢٣ - قصیده در تعریف بنای مسجد جامع سبزوار و مدح تاج الدین بانی آن

<div class="b" id="bn1"><div class="m1"><p>حبذ اطاقی که جفت این رواق اخضرست</p></div>
<div class="m2"><p>وز بلندی مر زمین را آسمانی دیگرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منتهای اوج او را کس نداند تا کجاست</p></div>
<div class="m2"><p>اینقدر دانند کز ایوان کیوان برترست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طارم نیلوفری زیر و زبر از رشک اوست</p></div>
<div class="m2"><p>گر چه از روی معالی بر جهانی دیگرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا موذن بر سر ایوان او باشد بپای</p></div>
<div class="m2"><p>قامتش ز آسیب چرخ چنبری چون چنبرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر فراز او نمی یارد پریدن جبرئیل</p></div>
<div class="m2"><p>گاه پروازش اگر چه عرش در زیر پرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سقف اینمقصوره کزوی باد قاصر چشم بد</p></div>
<div class="m2"><p>با چنان رفعت چو سقف آسمان پهناورست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جفت طاقش نیست اندر ربع مسکون هیچ چیز</p></div>
<div class="m2"><p>خود چنین باشد بنائی کو بنام داورست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه طاقی زین صفت بستن بدشواری بود</p></div>
<div class="m2"><p>لیکن آسان باشد آنکس را که دولت یاورست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناید از سنگ و گچ و خاک اینچنین طاقی مگر</p></div>
<div class="m2"><p>خاکش از مشکست و گچ کافور و سنگش از زرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مسجد جامع همیخوانندش اما جنتی است</p></div>
<div class="m2"><p>واندرو فواره ئی مانند حوض کوثر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آب آن فواره تا سر بر زد از جیب زمین</p></div>
<div class="m2"><p>از رشاش او هوا را دائما دامن ترست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>معتکف در وی بماند جاودان همچون خضر</p></div>
<div class="m2"><p>ز آنکه آبش همچو آب زندگی جانپرورست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جمعه گر حج مساکین است در هر جامعی</p></div>
<div class="m2"><p>اندرین جامع ز راه رتبه حج اکبرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اندرو یک خانه نتوان یافت کان معمور نیست</p></div>
<div class="m2"><p>وینعجب کاو را بنای سطح گوی اغبرست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سقفهای او ز تاب شعله قندیلها</p></div>
<div class="m2"><p>همچو سقف این رواق نیلگون پر اخترست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر کجا بینی دری دروی ز روی احتشام</p></div>
<div class="m2"><p>پرده ئی از اطلس گردون به پیش آن درست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر که صاحبدل بود زان پرده های زرنگار</p></div>
<div class="m2"><p>تا حجاب القلب سازد پرده بیش اندر خورست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرکجا خشتی سفید و سرخ بینی اندرو</p></div>
<div class="m2"><p>در خیال آجر نماید لیکن از سیم و زرست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نی غلط گفتم چه باشد سیم و زرکان خشتها</p></div>
<div class="m2"><p>از صفای رأی دستور جهان ماه و خورست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آصف ایام تاج ملت و دین کز شرف</p></div>
<div class="m2"><p>خاک پایش خسرو سیاره را تاج سرست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون عرض قایم نباشد جز بذات جوهری</p></div>
<div class="m2"><p>هست دولت آنعرض کاو را وجودش جوهرست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رستم دستانش هست از زیر دستان روز رزم</p></div>
<div class="m2"><p>حاتم طائیش گاه بزم او چون چاکرست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در سخا ابر بهاری نیست چون کان کفش</p></div>
<div class="m2"><p>وین سخن بی اشتباهی عاقلانرا باورست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عقل میداند که باشد بخشش کان سیم و زر</p></div>
<div class="m2"><p>بخشش ابر بهاری چیست آب و آذرست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر بیاض چهره دارد مه ز خط او جواز</p></div>
<div class="m2"><p>در سواد شب از آن سوی منازل رهبرست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>روز جنگ از چنگ او در چشم بدخواهان ملک</p></div>
<div class="m2"><p>غنچه چون پیکان و برگ بید همچون خنجرست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تیغ تیزش چون رقاب دشمنان سازد قراب</p></div>
<div class="m2"><p>در جهانگیری چو تیغ آفتاب خاورست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>طوطی کلکش شکر خاید چو آید در سخن</p></div>
<div class="m2"><p>ور چه در منقار او پیوسته مشک و عنبرست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>روی ملک و پشت دین را سرخ و فربه میکند</p></div>
<div class="m2"><p>کلک گوهر بار او هر چند زرد و لاغرست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شاخ امیدی که از آب دواتش چون قلم</p></div>
<div class="m2"><p>می نیابد پرورش چون شاخ آهو بی برست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صاحبا ابن یمین از یمن مدحت مدتیست</p></div>
<div class="m2"><p>تا بشعر و نثر با شعری و نثری همبرست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بکر فکرش میر باید هوش ارباب خرد</p></div>
<div class="m2"><p>خاصه اکنون کش قبول دلنوازت رهبرست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر بود طوطی طبعش در سخن شیرین زبان</p></div>
<div class="m2"><p>ز آن بود کاندر دهان او ز شکرت شکرست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا نگویند اهل دانش از طریق اعتقاد</p></div>
<div class="m2"><p>سایه ظلمت نما را کافتاب انورست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>با هزاران عز و دولت در جهان پاینده باد</p></div>
<div class="m2"><p>سایه ذات شریفت کآفتاب کشورست</p></div></div>