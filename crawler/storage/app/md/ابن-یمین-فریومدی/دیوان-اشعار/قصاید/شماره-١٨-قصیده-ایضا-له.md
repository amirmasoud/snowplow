---
title: >-
    شمارهٔ ١٨ - قصیده ایضاً له
---
# شمارهٔ ١٨ - قصیده ایضاً له

<div class="b" id="bn1"><div class="m1"><p>ای کاشف اسرار فلک رأی منیرت</p></div>
<div class="m2"><p>وی مظهر انوار سعادات ضمیرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو یوسف مصری و عزیز همه آفاق</p></div>
<div class="m2"><p>بر جمله خزائن بجهان کرد امیرت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای میر محمد توئی آنکس که بمردی</p></div>
<div class="m2"><p>مانند علی نیست در ایام نظیرت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در معرکه خصم تو که بادا بجهان گم</p></div>
<div class="m2"><p>چون زاغ کمان گوشه نشین گشت چو تیرت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر گه که سنان راست کنی بر دل دشمن</p></div>
<div class="m2"><p>گر آهن و سنگست نماید چو حریرت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در پاشی انوار تو از وصف برونست</p></div>
<div class="m2"><p>با فیض کف راد نمودست حقیرت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بزم چو جنت بنشین شاد چو رضوان</p></div>
<div class="m2"><p>کز غالیه حور بسوزند عبیرت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان خوشه انگور که پروین لقب اوست</p></div>
<div class="m2"><p>نشگفت گر آرند گه بزم عصیرت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر پیر و جوانرا که نظر بر رخت افتد</p></div>
<div class="m2"><p>با بخت جوان بیند و با دانش پیرت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در تربیت بنده خود ابن یمین کوش</p></div>
<div class="m2"><p>زیرا که نباشد ز چنین بنده گزیرت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از تست جهان کرم آباد که بادا</p></div>
<div class="m2"><p>دارای جهان تا که جهانست نصیرت</p></div></div>