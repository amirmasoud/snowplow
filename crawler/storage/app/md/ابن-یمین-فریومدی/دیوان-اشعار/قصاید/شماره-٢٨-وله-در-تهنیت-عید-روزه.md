---
title: >-
    شمارهٔ ٢٨ - وله در تهنیت عید روزه
---
# شمارهٔ ٢٨ - وله در تهنیت عید روزه

<div class="b" id="bn1"><div class="m1"><p>شهریارا ماه روزه بر تو میمون باد و هست</p></div>
<div class="m2"><p>همچو عیدت روزها یکسر همایون باد و هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاج ملک و دین علی کز صبغه الله تا ابد</p></div>
<div class="m2"><p>بخت روز افزونت را رخساره گلگون باد و هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نو عروس ملک را همچون تو دامادی نخاست</p></div>
<div class="m2"><p>جای آن داری بگویم بر تو مفتون باد و هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک پایت کز شرف تاج سرشاهان بود</p></div>
<div class="m2"><p>چون گل از خون دل اعدات معجون باد و هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شنید از باغ ملکت بد کنش بوی بهی</p></div>
<div class="m2"><p>چون انارش دل زغم پر قطره خون باد و هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر سعادت کان ازین پس بود و باشد تا ابد</p></div>
<div class="m2"><p>از برای نظم کارت یکسر اکنون باد و هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از فروغ گوهر شهوار تاج خسرویت</p></div>
<div class="m2"><p>چشم حاسد چو نصدف پر در مکنون باد و هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فتنه را دایم ز شربتخانه انصاف تو</p></div>
<div class="m2"><p>پرورش از شیره خشخاش و افیون باد و هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در جهان از لطف ایزد هیچ چیزت نیست کم</p></div>
<div class="m2"><p>وز همه چیزیکه باشد عمرت افزون باد و هست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از گزند روزگار پیر بخت نو جوانت</p></div>
<div class="m2"><p>دایم اندر عصمت دارای بیچون باد و هست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا نباشد کار گردون را سر و پائی پدید</p></div>
<div class="m2"><p>کار خصمت بی سر و پا همچو گردون باد و هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا بود سیماب و گوگرد ابتدای زر و سیم</p></div>
<div class="m2"><p>دشمنت چو نسیم و زر در خاک مدفون باد و هست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا کند از جان نثار حضرت میمون تو</p></div>
<div class="m2"><p>با یسار ابن یمین از در موزون باد و هست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عمرت اندر کامرانی کم مباد از عمر نوح</p></div>
<div class="m2"><p>مالت افزون تر بسی از مال قارون باد و هست</p></div></div>