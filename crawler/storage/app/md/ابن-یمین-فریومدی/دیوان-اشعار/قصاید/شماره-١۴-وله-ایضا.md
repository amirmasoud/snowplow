---
title: >-
    شمارهٔ ١۴ - وله ایضاً
---
# شمارهٔ ١۴ - وله ایضاً

<div class="b" id="bn1"><div class="m1"><p>ای دیده در شناختن حال کاینات</p></div>
<div class="m2"><p>باید که باشدت نظری از سر انات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنیاد کارها همه بر هفت و چار دان</p></div>
<div class="m2"><p>نه از سر تهتک رأی از ره ثبات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان هفت و زین چهار که مجموع یازده است</p></div>
<div class="m2"><p>آباش هفت باشد و چارست امهات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آبا و امهات سه فرزند خاستند</p></div>
<div class="m2"><p>حیوان و معدن و سوم هر سه شان نبات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محصول اینجهان همه زان هر سه زاده اند</p></div>
<div class="m2"><p>خواهی ببحر در نگر و خواه در فلات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وانگه نظر فکن سوی دیوان اختران</p></div>
<div class="m2"><p>مکتوب از وروان شده بی کلک و بی دوات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاهی رسد بخیبت فرزانگان مثال</p></div>
<div class="m2"><p>گاهی بود ببهره دیوانگان برات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه از رخ بساط فلک بیدقی رود</p></div>
<div class="m2"><p>آرد شهان فیل فکن را بشاهمات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایدل بسی رموز و اشارات در رهست</p></div>
<div class="m2"><p>قانون عقل گیر و برو بر ره نجات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر ظن بری که اینهمه موجود خود شدند</p></div>
<div class="m2"><p>پس آن چرا جماد شد این گشت ذوحیات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن کیست کو بداشت بیکجای بحر را</p></div>
<div class="m2"><p>وز حکم کیست گشته روان دجله و فرات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر صورتی بخویشتن ار هست میشدی</p></div>
<div class="m2"><p>کم نامدی ز شیر گوزن و ز گور شات</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر چند هست صورت اجرام بیشمار</p></div>
<div class="m2"><p>دارای جمله غیر یکی نیست بر ثبات</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر یک قبول فیض دگر سان همی کنند</p></div>
<div class="m2"><p>نال ار چه نی بود نشود چون نی فتات</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هستند معترف همه خلقان که خالقیست</p></div>
<div class="m2"><p>تا آن کسان که سجده عزی کنند ولات</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر جوهر و عرض که تو بینی چو ممکنند</p></div>
<div class="m2"><p>دانند عاقلان که بود واجبی بذات</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ذاتی که در مبادی ایجاد جود او</p></div>
<div class="m2"><p>فضل بنین ندید در افضال بر بنات</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن حی لاینام که ذات وی از ازل</p></div>
<div class="m2"><p>دارد فراغ تا ابد از نوم و از سبات</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>او را پرستد آنکه خرد رهنمای اوست</p></div>
<div class="m2"><p>خواهی ز مکه گیرش و خواهی ز سومنات</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر جا که شد کسی چو زملکش برون نشد</p></div>
<div class="m2"><p>منزل چو مرو و بلخ و نشابور و چه هرات</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون پیروی واضع دینیت واجبست</p></div>
<div class="m2"><p>وین هست فرض بر همه کس تا گه فوات</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>باری چو میروی پی سلطان شرع گیر</p></div>
<div class="m2"><p>تا ره بری بروضه رضوان پس از ممات</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دارای دین محمد مرسل که نا او</p></div>
<div class="m2"><p>با نام ذوالجلال قرینست در صلات</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر کو شفای درد خود از مهر او نجست</p></div>
<div class="m2"><p>نومید از نجات رود درگه وفات</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر نکته های ابن یمین را تو منکری</p></div>
<div class="m2"><p>هات الذی عری بک یا منکری وهات</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>میخواستم که قافیه وحدان بود تمام</p></div>
<div class="m2"><p>تا مختلط بهم نشود حنظل و نبات</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خود دیدم آنکه طبع ضعیفم بشایگان</p></div>
<div class="m2"><p>کردست یکدو جا ز سر غفلت التفات</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آری سزد که خورده نگیرند زانکه آب</p></div>
<div class="m2"><p>شیرین و شور هر دو همی خیزد از قنات</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یا رب بنور پاک محمد که عفو کن</p></div>
<div class="m2"><p>ز ابن یمین گناه که طاعت نکرد و فات</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>طاعت ز مفلسان گنهکار خود مجوی</p></div>
<div class="m2"><p>کز مفلسان بشرع نخواهد کسی زکات</p></div></div>