---
title: >-
    شمارهٔ ٧۵ - قصیده در تعریف و تاریخ بنای قصری که نظام الدین یحیی ساخته
---
# شمارهٔ ٧۵ - قصیده در تعریف و تاریخ بنای قصری که نظام الدین یحیی ساخته

<div class="b" id="bn1"><div class="m1"><p>حبذا این قصر جانپرور که تا گشت آشکار</p></div>
<div class="m2"><p>کرد پنهان رخ ز شرم او بهشت کردگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از لطافت هست تا حدی که جفتش در جهان</p></div>
<div class="m2"><p>کس نبیند غیر ازین فیروزه طاق زرنگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خوشی آن منزلت دارد که دی مه را در او</p></div>
<div class="m2"><p>عقل کارآگاه نشناسد ز فصل نوبهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از هوای جانفزای او عجب نآید مرا</p></div>
<div class="m2"><p>گر سخن گوید درو صورت که باشد بر جدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی کمال نور مه نقصان گرفتی در خسوف</p></div>
<div class="m2"><p>گر ز عکس جام وی بودی فروغش مستعار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست بحری پر عجائب کز میان موج او</p></div>
<div class="m2"><p>گوهر شهوار شادی دل افتد بر کنار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باشد از رفعت سپهری زو فروزان گشته مهر</p></div>
<div class="m2"><p>چون بود خسرو در او مسند نشین هنگام بار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسرو جمشید رتبت داور دارا صفت</p></div>
<div class="m2"><p>آفتاب ملک و ملت سایه پروردگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاه یحیی کاندرین شش طاق ایوان سپنج</p></div>
<div class="m2"><p>مثل او ناید پدید از اجتماع هفت و چار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در چنین خرم سرا از گفته ابن یمین</p></div>
<div class="m2"><p>زهره کو تا خوش نوائی برکشد عشاق وار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شرط آداب عبودیت بجا آرد نخست</p></div>
<div class="m2"><p>پس بگوید بی تحاشی پیش تخت شهریار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کای جنابت قبله اقبال اهل روزگار</p></div>
<div class="m2"><p>حمله رستم همه دستانت آید روز کار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا زمین و آسمان منقاد عزم و حزم تست</p></div>
<div class="m2"><p>آن گرفت آرام دایم وین نمیگیرد قرار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور نسیم لطف تو بر بیشه شیران وزد</p></div>
<div class="m2"><p>ور سموم قهر تو یابد سوی دریا گذار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در صدف از تاب قهرت در شود دیگر بی آب</p></div>
<div class="m2"><p>کام شیر شرزه گردد ناف آهوی تتار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فتنه را در باغ عدلت ز آرزوی خواب خوش</p></div>
<div class="m2"><p>ناید اندر دیده چیزی جز خیال کو کنار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نفس نامی را ز ابر دستت ار باشد مدد</p></div>
<div class="m2"><p>تا ابد یابد رهائی از تهی دستی چنار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آفتاب و آسمانت خواندمی گر دیدمی</p></div>
<div class="m2"><p>آفتابی بیزوال و آسمانی با وقار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باد عمرت جاودان تا در پناه جاه تو</p></div>
<div class="m2"><p>صاحب اینقصر سازد از خوشی زین صد هزار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صاحب اعظم غیاث ملک و دین دستور شرق</p></div>
<div class="m2"><p>آنکه با بخت جوانش هست رأی پیر یار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>و آنکه چون گیرد هوای دل غبار آرزو</p></div>
<div class="m2"><p>هیچکس ننشاند الا ابر دستش آن غبار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حزم هشیارش چنان آئین مستی بر فکند</p></div>
<div class="m2"><p>کز سر نرگس نخواهد شد برون هرگز خمار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سال هجرت چون قدم در ذال و نون و ها نهاد</p></div>
<div class="m2"><p>وز مه شوال عشر اوسط آمد در شمار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر در و دیوار این خرم سرا ابن یمین</p></div>
<div class="m2"><p>زر نبودش تا فشاند کرد عقد در نثار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>صاحبا این قصر عالی تا ابد پاینده باد</p></div>
<div class="m2"><p>تا بعز و دولت و اقبال شاه کامکار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قرنها در مسند عزت بکام دوستان</p></div>
<div class="m2"><p>بگذراند اندرین فرخنده کاخ زرنگار</p></div></div>