---
title: >-
    شمارهٔ ١٢۴ - وله ایضاً قصیده در مدح طغایتمورخان
---
# شمارهٔ ١٢۴ - وله ایضاً قصیده در مدح طغایتمورخان

<div class="b" id="bn1"><div class="m1"><p>ترا سزد بصفا ماه آسمان گفتن</p></div>
<div class="m2"><p>ترا بحسن توان زینت جهان گفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر تو از درم ای حور چهره باز آئی</p></div>
<div class="m2"><p>توان مقام مرا خلد جاودان گفتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کج است در نظر قد چون صنوبر تو</p></div>
<div class="m2"><p>سخن ز راستی سرو بوستان گفتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر تو قصد بجانم کنی روا نبود</p></div>
<div class="m2"><p>به پیش طلعت جانان مرا ز جان گفتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بترک جان و جهان زود میتوانم گفت</p></div>
<div class="m2"><p>بترک صحبت جانان نمیتوان گفتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدان هوس که ز طبعت برون کنم صفرا</p></div>
<div class="m2"><p>توان سرشک مرا آب ناردان گفتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز رشک رسته در تو بس عجب نبود</p></div>
<div class="m2"><p>تن ضعیف مرا تار ریسمان گفتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصال همچو توئی گر بجهد دست دهد</p></div>
<div class="m2"><p>توان بترک تن و جان و خان و مان گفتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه لایقست ز لطف چو تو سبکروحی</p></div>
<div class="m2"><p>جواب عاشق بیچاره سرگران گفتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنون چو داد غزل داد طبعت ابن یمین</p></div>
<div class="m2"><p>بوصف خال و خط و زلف دلبران گفتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>میان خامه ببند و زبان او بگشای</p></div>
<div class="m2"><p>پس از غزل بمدیح خدایگان گفتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فروغ اختر شاهنشهی طغایتمور</p></div>
<div class="m2"><p>که میتوانش بحق شاه شه نشان گفتن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زمین درگه او را ز بس جلالت و جاه</p></div>
<div class="m2"><p>توان بگاه بیان سطح آسمان گفتن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جناب حضرت او را رسد ز رفعت و قدر</p></div>
<div class="m2"><p>سخن ز منزلت اوج لا مکان گفتن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بروز معرکه گر برگ نی بکف گیرد</p></div>
<div class="m2"><p>توان ز قوتش آن برگ را سنان گفتن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کمینه بنده که از درگهش روان گردد</p></div>
<div class="m2"><p>توان بکشور اعداش مرزبان گفتن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز عدل او شده با گوسفند گرگ چنان</p></div>
<div class="m2"><p>که میتوان ز شفقت سگ شبان گفتن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جواب خصم ترا زیبد ای همایون فر</p></div>
<div class="m2"><p>برأی پیر و باقبال نوجوان گفتن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همان زمان که نهی پای بر زمین عدو</p></div>
<div class="m2"><p>بر آسمان رسد آوای الامان گفتن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بجز تو در همه عالم نمیرسد کس را</p></div>
<div class="m2"><p>پناه اهل زمین خسرو زمان گفتن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عطای یکدمه بذل ترا بمجلس انس</p></div>
<div class="m2"><p>توان ذخیره صد گنج شایگان گفتن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شنوده ام ز سخنهای سوزنی بیتی</p></div>
<div class="m2"><p>مناسب ار چه توانم نظیر آن گفتن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ولی بصورت تضمین چرا ادا نکنم</p></div>
<div class="m2"><p>چو میتوان سخن خوش برایگان گفتن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دروغ راست نمایست در ولایت شاه</p></div>
<div class="m2"><p>ز عدل او بره با گرگ توأمان گفتن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جهانپناه شها بنده پرورا از آنک</p></div>
<div class="m2"><p>که تا بود بجهان صنعت زبان گفتن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مدیح جاه تو گویم که مدح همچو توئی</p></div>
<div class="m2"><p>زبان بنده تواند بصد بیان گفتن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو یوسفی و سلیمان صفت فریضه شدست</p></div>
<div class="m2"><p>دعات بر همه ابنای انس و جان گفتن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همیشه تا بود اندر حضور اهل خرد</p></div>
<div class="m2"><p>ز ارغوان صفت طبع زعفران گفتن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز دیده بر رخ چون زعفران خصم تو باد</p></div>
<div class="m2"><p>چنانکه می نتوان آب ارغوان گفتن</p></div></div>