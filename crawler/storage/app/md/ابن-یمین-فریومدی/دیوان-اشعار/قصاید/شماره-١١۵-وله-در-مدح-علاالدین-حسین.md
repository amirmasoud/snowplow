---
title: >-
    شمارهٔ ١١۵ - وله در مدح علاءالدین حسین
---
# شمارهٔ ١١۵ - وله در مدح علاءالدین حسین

<div class="b" id="bn1"><div class="m1"><p>پیشتر زین چند گاهی دل پریشان داشتم</p></div>
<div class="m2"><p>خود چه میگویم ز دل صد رنج بر جان داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یوسف مصر کرم را از تکسر شکوه ئی</p></div>
<div class="m2"><p>بود و من یعقوب وش دل بیت احزان داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن علی نام حسن سیرت علاءالدین حسین</p></div>
<div class="m2"><p>کز غم او چشم و دل گریان و بریان داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس که بر خاطر ملالت بود مستولی مرا</p></div>
<div class="m2"><p>همچو گنج آرامگه در کنج ویران داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه بر من ز آن تکسر گشت رمزی آشکار</p></div>
<div class="m2"><p>لیک چون خوش نامدم از خویش پنهان داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور چه یکساعت نبودم دور ازو بی درد دل</p></div>
<div class="m2"><p>لیکن از دیدار او امید درمان داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منت ایزد را که دیدم در زمان صحتش</p></div>
<div class="m2"><p>گشته آمن آنچه دل از وی هراسان داشتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بعد ازین شکرست چون ابن یمین کارم از آنک</p></div>
<div class="m2"><p>حاصلم شد هر چه چشم آن ز یزدان داشتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بماضی شرح دادم اختصاص خود بدان</p></div>
<div class="m2"><p>ظن مبر آنحال ماضی شد که من آن داشتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داشتم دل در هوای او و خواهم داشتن</p></div>
<div class="m2"><p>تا ابد چون دائم او را رکن ایمان داشتم</p></div></div>