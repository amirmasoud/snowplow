---
title: >-
    شمارهٔ ١٩ - ایضاً له در مدح تاج الدین علی
---
# شمارهٔ ١٩ - ایضاً له در مدح تاج الدین علی

<div class="b" id="bn1"><div class="m1"><p>آنکه دست و دل او مظهر جود و کرم است</p></div>
<div class="m2"><p>وانکه در داد و دهش صد چو فریدون و جم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدوه و قبله شاهان جهان خواجه علی</p></div>
<div class="m2"><p>یاور ملک عرب داور ملک عجم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و آنکه تیغش بگه رزم ز خون دل خصم</p></div>
<div class="m2"><p>رود نیلست که سیلش همه آب بقم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فکر صائب نظرش دیده و دانسته که چیست</p></div>
<div class="m2"><p>هر چه بر تخته تقدیر الهی رقم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو عیسی و خضر ذات محمد سیرش</p></div>
<div class="m2"><p>فرخ آثار و مبارک دم و میمون قدم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که با درگه او داد پناه از حدثان</p></div>
<div class="m2"><p>ایمن از حادثه چون صید حریم حرم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همتش گر نشود ضامن روزی نکند</p></div>
<div class="m2"><p>عزم صحرای وجود آنکه بکتم عدم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه سیم و زر کان بیش ز پیش است ولیک</p></div>
<div class="m2"><p>با در افشان کف او وقت عطا کم ز کم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخشش ابر چو فیض کف او نیست چنانک</p></div>
<div class="m2"><p>گاه گاهی بود آن بخشش این دم بدم است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لابهنگام تشهد بود اندر سخنش</p></div>
<div class="m2"><p>چون از آن در گذری صیغه لفظش نعم است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دشمنش ابر بهارست ولیکن نه بجود</p></div>
<div class="m2"><p>ز آنجهت کز دل و از دیده همه سوز و نم است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تیر عدلش نه شگفت ار برداز راست روی</p></div>
<div class="m2"><p>هر چه در پشت کمان از کژی و تاب و خم است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شهریارا ز درت هر که دمی دور فتاد</p></div>
<div class="m2"><p>تا قیامت ز چنین غبن ندیم ندم است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چار پهلو شود از خوان تو چون شیره آش</p></div>
<div class="m2"><p>آزا گر خود همه اعضاش چو کاسه شکم است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر که با لشکر چون مور و ملخ دیدترا</p></div>
<div class="m2"><p>گفت با خسرو سیاره ز انجم حشم است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همچو خورشید گرفتی همه آفاق به تیغ</p></div>
<div class="m2"><p>زانکه زلف ظفرت پرچم رمح و علم است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گشت ظالم شکن از عدل تو مظلوم چنانک</p></div>
<div class="m2"><p>طعمه مورچگان از دل شیر اجم است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر که در بوته اخلاص تو چون زر ننشست</p></div>
<div class="m2"><p>کنده چون سکه و سر کوفته همچون درم است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از ره تیره دلی دشمن تو هست دوات</p></div>
<div class="m2"><p>لیک بس کلسته و بیسر و پا چون قلم است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خسروا داعی درگاه جلال تو رهی</p></div>
<div class="m2"><p>که هوادارتر از جمله عبید و خدم است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با خرد گفت که دانی بچه عیب ابن یمین</p></div>
<div class="m2"><p>از کمان فلک آزرده بتیر ستم است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برکشید از جگر آهی خرد و گفت بدرد</p></div>
<div class="m2"><p>عیبش اینست که مسکین بخرد متهم است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دین پناها چو دل اهل هنر شاد بتست</p></div>
<div class="m2"><p>مپسند آنکه نصیب از فلکش جمله غم است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر تو اندیشه کارش نکنی پس که کند</p></div>
<div class="m2"><p>کوشهی در همه عالم چو تو عالی همم است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>با کریمی چو توأش جستن کام از دگری</p></div>
<div class="m2"><p>چون ز عصفور طلب کردن لحم و دسم است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا بود از سر دانش سخن اهل هنر</p></div>
<div class="m2"><p>آنکه روزی ده خلقان کرم ذوالنعم است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کرمت ضامن ارزاق خلایق بادا</p></div>
<div class="m2"><p>که توئی آنکه کف راد تو کان کرم است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مجلس انس تو خرم چو ارم باد و بود</p></div>
<div class="m2"><p>زانکه از عدل تو عالم بخوشی چون ارم است</p></div></div>