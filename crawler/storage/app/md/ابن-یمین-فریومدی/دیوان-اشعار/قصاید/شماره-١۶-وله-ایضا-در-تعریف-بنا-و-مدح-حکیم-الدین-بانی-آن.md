---
title: >-
    شمارهٔ ١۶ - وله ایضاً در تعریف بنا و مدح حکیم الدین بانی آن
---
# شمارهٔ ١۶ - وله ایضاً در تعریف بنا و مدح حکیم الدین بانی آن

<div class="b" id="bn1"><div class="m1"><p>اینمنزل خجسته که بس روحپرورست</p></div>
<div class="m2"><p>از فرخی و خوش نفسی خلد دیگرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوزد چو آتشی غم دلها هوای او</p></div>
<div class="m2"><p>گوئی که خاکش از ارم آبش ز کوثرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس دلفریب خلق فتادست وضع او</p></div>
<div class="m2"><p>سنگش مگر ز گوهر و خشت وی از زرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در نزهت و لطافت و رفعت نظیر او</p></div>
<div class="m2"><p>جائی نباشد ار بود این سبز منظرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام جهان نمای که خوانندش آفتاب</p></div>
<div class="m2"><p>پیش صفای سایه جامش مکدرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا عکس جامهاش فتادست بر زمین</p></div>
<div class="m2"><p>صحنش چو سقف منظر مینا پر از اخترست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه نکرد بانی او هیچ صورتی</p></div>
<div class="m2"><p>دروی که آن مخالف شرع پیمبرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فخر رسل محمد مرسل که انبیا</p></div>
<div class="m2"><p>جمله سرند و بر سر ایشان چو افسرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن سیدی که خادم او بود جبرئیل</p></div>
<div class="m2"><p>اینجاه با جلالت او بس محقرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شهرت مجوی ابن یمین جز بنعت او</p></div>
<div class="m2"><p>زیرا ظهور ذره بخورشید انورست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رفتیم با تخلص این شعر آبدار</p></div>
<div class="m2"><p>کانرا اگر چنان بگذاریم ابترست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نی نی چو از صفای گچ او چو آینه</p></div>
<div class="m2"><p>صورت نمای تست تو گوئی مصورست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>والا حکیم ملت و دین کاهل فضل را</p></div>
<div class="m2"><p>ذات شریف او اثر لطف داورست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیدا چو آفتاب بر رأی انورش</p></div>
<div class="m2"><p>هر نکته کان نهفته این هفت دفترست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای افصح زمانه که طوطی روح را</p></div>
<div class="m2"><p>الفاظ جانفزای تو چون شیر و شکرست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خصم تو خاکسار چو باد است و زین سبب</p></div>
<div class="m2"><p>چشم و دلش مدام پر از آب و آذرست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بخت تو پایدار بکردار قطب باد</p></div>
<div class="m2"><p>آری بود چو سایه مهریت بر سرست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یعنی علاء دولت و دین آفتاب ملک</p></div>
<div class="m2"><p>کاندر پناه سایه او هفت کشورست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بستند اختران کمر بندگی او</p></div>
<div class="m2"><p>صدق مرا نگر که یکی زان دو پیکرست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جاوید عمر باد که تا در پناه او</p></div>
<div class="m2"><p>مانی درین مقام بجائی که بهترست</p></div></div>