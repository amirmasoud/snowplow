---
title: >-
    شمارهٔ ٧٢ - قصیده در مدح نظام الدین یحیی
---
# شمارهٔ ٧٢ - قصیده در مدح نظام الدین یحیی

<div class="b" id="bn1"><div class="m1"><p>آفرین باد آفرین ای حیدر خنجر گذار</p></div>
<div class="m2"><p>کامد از تیغ تو آبی ملک را بر روی کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدتی بر خویشتن خندید خصمت همچو گل</p></div>
<div class="m2"><p>دست تقدیرش نهاد از خنجرت ناگاه خار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دشمنانرا کار زار از خوبی کردار تست</p></div>
<div class="m2"><p>وینچنین آمد ز مردان کار وقت کارزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منتظر بودند خلقان مدتی این فتح را</p></div>
<div class="m2"><p>جمله را دادی بیکساعت خلاص از انتظار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دشمنت چون حرص و آزی داشت غالب همچو مور</p></div>
<div class="m2"><p>پایمال دهر شد از بهر فتحش همچو مار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تند باد قهر تو چون آتش کین برفروخت</p></div>
<div class="m2"><p>شد بجوش آب روان از چشم خصم خاکسار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوستانرا گشت خندان غنچه دلها چه باک</p></div>
<div class="m2"><p>دشمنانرا دیده شد چون ابر نیسان در بهار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا جهان بودست و باشد نامد و ناید دگر</p></div>
<div class="m2"><p>بر سر میدان مردی چون تو مردی شهسوار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آسمان در سایه خود جز تو هرگز کس ندید</p></div>
<div class="m2"><p>کو جهانگیری به تنهائی کند خورشید وار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دور بادا چشم بد از بال و برز پهلوی</p></div>
<div class="m2"><p>کاهل عالم را چو رستم هست گوئی یادگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ترک رزم آرای گردون گرد دار یابد مجال</p></div>
<div class="m2"><p>کمترین هندوت را چاکر ز بهر افتخار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون عنان عزم تا بی سوی میدان روز رزم</p></div>
<div class="m2"><p>با تو آندم جز رکابت کس نباشد پایدار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر چه هست آبی تنک تیغت ولی در پیش او</p></div>
<div class="m2"><p>کوه آتش گر بود ناچیز گردد چون شرار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر فکند آئین مستی حزم هشیارت چنانک</p></div>
<div class="m2"><p>می نخواهد رست نرگس تا قیامت از خمار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در زمان بخت بیدارت ز بهر خواب خوش</p></div>
<div class="m2"><p>فتنه را دادست دهر از باغ عدلت کو کنار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با تو همراه آمده ز آغاز فطرت چار چیز</p></div>
<div class="m2"><p>هم سعادت هم سخاوت هم شجاعت هم وقار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در درافشانی و زرپاشی خجالتها برند</p></div>
<div class="m2"><p>از کفت باد خزان و از دمت باد بهار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دوستانرا دلنوازی کن که جانبازی کنند</p></div>
<div class="m2"><p>آشنا کن باز را کو خود همیداند شکار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>غم نصیب دشمنان افتاد زین پس شاد باش</p></div>
<div class="m2"><p>تا ابد با دوستان عمری بعشرت میگذار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنزمان کآری بیاد از بندگان خویشتن</p></div>
<div class="m2"><p>چاکرت ابن یمین را هم از ایشان میشمار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شاخ امیدی که بیخش تازه ز آب لطف تست</p></div>
<div class="m2"><p>ز آفتاب ملک و ملت سایه پروردگار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صاحب اعظم نظام الدین که کرده تربیت</p></div>
<div class="m2"><p>رأی او را شاه انجم بهر صیت و اشتهار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شهریار ملک و دین یحیی که دین و ملک را</p></div>
<div class="m2"><p>تا بقا باشد مبادا غیر او کس شهریار</p></div></div>