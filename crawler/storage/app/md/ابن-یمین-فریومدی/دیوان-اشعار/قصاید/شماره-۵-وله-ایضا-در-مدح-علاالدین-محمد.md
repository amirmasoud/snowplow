---
title: >-
    شمارهٔ ۵ - وله ایضاً در مدح علاءالدین محمد
---
# شمارهٔ ۵ - وله ایضاً در مدح علاءالدین محمد

<div class="b" id="bn1"><div class="m1"><p>مدتی گردون ز غیرت داشت سرگردان مرا</p></div>
<div class="m2"><p>زانک در دانش مزید یافت بر اقران مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منت ایزد را که باز از ظلمت حرمان چو خضر</p></div>
<div class="m2"><p>رهنما شد بخت سوی چشمه حیوان مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بودم اندر تیه حیرت مدتی همچون کلیم</p></div>
<div class="m2"><p>برد سوی طور همت پرتو یزدان مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه بودم ساکن بیت الحزن یعقوب وار</p></div>
<div class="m2"><p>نزد یوسف برد بخت از کلبه احزان مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صاحبی کانوار رای مملکت آرای او</p></div>
<div class="m2"><p>وا رهاند از مضیق مشکلات آسان مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حساب کار خود سرگشته بودم مدتی</p></div>
<div class="m2"><p>ره نمود اقبال سوی صاحب دیوان مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صاحب اعظم علاء ملت و دین آنکه هست</p></div>
<div class="m2"><p>مرهم لطفش ز بهر درد دل درمان مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن محمد خلق عیسی دم که وصف ذات او</p></div>
<div class="m2"><p>در نکو گوئی کند مشهور چون حسان مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در حضیض محنت ارچه پایمالم همچو خاک</p></div>
<div class="m2"><p>سرفرازد دولتش بر ذروه کیوان مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر چه کانرا سیم و زر بخشیدن آئین است لیک</p></div>
<div class="m2"><p>پیش دریای کفش ممسک نماید کان مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا همی بینم سخای دست گوهر بار او</p></div>
<div class="m2"><p>ننگ میآید ز جود ابر در نیسان مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون بمیدان اندر آید روز کین جولان کنان</p></div>
<div class="m2"><p>حمله های رستم آید سر بسر دستان مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیزه در کف چون کند آهنگ قلب دشمنان</p></div>
<div class="m2"><p>در ید بیضا نماید پیکر ثعبان مرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عقدهای گوهر موزون من در حضرتش</p></div>
<div class="m2"><p>آید الحق بر مثال زیره و کرمان مرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شعر بروی عرضه کردن مینماید راستی</p></div>
<div class="m2"><p>همچو سحر سامری با موسی عمران مرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با کمال فضلش الحق از فصاحت دم زدن</p></div>
<div class="m2"><p>نطق با قل آید اندر حضرت سحبان مرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صاحبا دانی که از یمن مدیحت مدتیست</p></div>
<div class="m2"><p>تا عطارد مینهد سر بر خط فرمان مرا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بلبل خوشگوی طبعم در قفس فرسوده شد</p></div>
<div class="m2"><p>وقت پرواز است و شادی بر گل و ریحان مرا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زینت و زیور روا باشد که سازد از شبه</p></div>
<div class="m2"><p>گر نباشد دسترس بر گوهر عمان مرا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر نبودی دور محنت چون ثوابت دیر پای</p></div>
<div class="m2"><p>کی چنین سیاره وش کردی فلک حیران مرا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از خواص زعفران بر چهره باشد گر شود</p></div>
<div class="m2"><p>با چنین دلتنگئی چون غنچه لب خندان مرا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیش ازین با خود بهر وقتی تفکر کردمی</p></div>
<div class="m2"><p>کز برای چیست دائم کار بی سامان مرا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا که اینمعنی بپرسیدم ز پیر کاردان</p></div>
<div class="m2"><p>کز چه میدارد فلک در حیز خذلان مرا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفت عقل اکنون هنر عیب است الحق راست گفت</p></div>
<div class="m2"><p>ورنه دیگر چیست چندین موجب حرمان مرا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون همیدیدم که از روی حسد گردون دون</p></div>
<div class="m2"><p>خواهد افکندن چو گوی اندر خم چوگان مرا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عید خود روزی همی داند سپهر بیوفا</p></div>
<div class="m2"><p>کز جفا کاری و بد کیشی کند قربان مرا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بستم احرام همایون حضرتت کالطاف تو</p></div>
<div class="m2"><p>در حوادث کوش دارد زافت دوران مرا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر چه در صورت سفر باشد سقراما ولیک</p></div>
<div class="m2"><p>اینسفر بگشاد در در روضه رضوان مرا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بنده خاص توأم باید که داند رأی تو</p></div>
<div class="m2"><p>سهل باشد گر بداند عامه از خاصان مرا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جان نباشد در تن ابن یمین بی شکر تو</p></div>
<div class="m2"><p>شکر تو فرض است تا باشد بتن در جان مرا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دولتت پاینده بادا تا ابد کز جود تست</p></div>
<div class="m2"><p>هر چه میبینی ز رخت و بخت و خان و مان مرا</p></div></div>