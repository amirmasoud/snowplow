---
title: >-
    شمارهٔ ١٠٢ - قصیده عینیه
---
# شمارهٔ ١٠٢ - قصیده عینیه

<div class="b" id="bn1"><div class="m1"><p>چگویم ازین روزگار مخادع</p></div>
<div class="m2"><p>چه آمد رهی را بروی از وقایع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بصد قرن یک شمه نتوان بیان کرد</p></div>
<div class="m2"><p>که از دور گردون چها گشت واقع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان کوکب سعد من گشت غارب</p></div>
<div class="m2"><p>که گفتی نخواهد شدن نیز طالع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشاده شد و بسته در پیش عزمم</p></div>
<div class="m2"><p>طریق مضار و سبیل منافع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بشرح و بیان راست ناید که ما را</p></div>
<div class="m2"><p>سپهر از مرادات چون گشت مانع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا شربتی داد چون زهر قاتل</p></div>
<div class="m2"><p>ز جام غرور این جهان مخادع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولی شکر اگر شربت او مضر بود</p></div>
<div class="m2"><p>ز الطاف مخدوم خود گشت نافع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنم نفع آن جام پیدا یکایک</p></div>
<div class="m2"><p>بتضمین بیتی دو مشهور شایع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه کشیدیم رنج فراوان</p></div>
<div class="m2"><p>وگر چند بودیم عطشان و جایع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رسیدیم الحمد لله بجائی</p></div>
<div class="m2"><p>که رنج فراوان ما نیست ضایع</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بعالیجنابی سلیمان محلی</p></div>
<div class="m2"><p>که آصف سزد رأی او را متابع</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدرگاه برهان دین آنکه تیغش</p></div>
<div class="m2"><p>در اثبات حق هست برهان قاطع</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهر کرم آنکه چون آفتابست</p></div>
<div class="m2"><p>مضیی ء عوارف مضی ء صنایع</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو دریا بود طبع او پر عجائب</p></div>
<div class="m2"><p>بود همچو کان خاطرش پر بدایع</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عدو را بعنف جگر سوز خافض</p></div>
<div class="m2"><p>ولی را بلطف دلفروز رافع</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو نصر من الله طراز علم کرد</p></div>
<div class="m2"><p>برغبت شدند انس و جنش متابع</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فلک با همه کبریا قدر او را</p></div>
<div class="m2"><p>گرش هست رغبت ورش نیست خاضع</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زهی گشت قانون فضل و هنر را</p></div>
<div class="m2"><p>اشارات کلی رأی تو جامع</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به پیش جنابت چو در پیش قبله</p></div>
<div class="m2"><p>مصلی صفت آسمان گشت راکع</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به بیدای فاقه جگر خستگان را</p></div>
<div class="m2"><p>ینابیع جود تو باشد مشارع</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو آهنگ مدحت کند طبع قائل</p></div>
<div class="m2"><p>چو مینو شود وعظ و مدح تو سامع</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو سوسن زبان گرددش جمله اعضا</p></div>
<div class="m2"><p>شود چون بنفشه همه تن مسامع</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هنر پرورا نیست ابن یمین را</p></div>
<div class="m2"><p>بجز مکرماتت بدین درد راتع</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بجز لطف جان پرورت در حوادث</p></div>
<div class="m2"><p>ندارد ز قهرت فلک هیچ شافع</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>الا تا ز آغاز و انجام دوران</p></div>
<div class="m2"><p>نباشد کس آگه بجز ذات صانع</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو دوران گردون گردان مبیناد</p></div>
<div class="m2"><p>مبادی دور ترا کس مقاطع</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مباد اختری مستقیم از سعادت</p></div>
<div class="m2"><p>ز سمتی که باشد مراد تو راجع</p></div></div>