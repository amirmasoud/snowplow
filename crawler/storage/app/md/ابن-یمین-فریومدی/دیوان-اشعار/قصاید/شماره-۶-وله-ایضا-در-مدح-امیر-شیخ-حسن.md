---
title: >-
    شمارهٔ ۶ - وله ایضاً در مدح امیر شیخ حسن
---
# شمارهٔ ۶ - وله ایضاً در مدح امیر شیخ حسن

<div class="b" id="bn1"><div class="m1"><p>واجب بود از راه نیاز اهل زمن را</p></div>
<div class="m2"><p>در خواستن از حق بدعا شیخ حسن را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنسایه یزدان که چو خورشید بیاراست</p></div>
<div class="m2"><p>رایش بصفا روی زمین را و زمن را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در رسته بازار هنر ملک خریدست</p></div>
<div class="m2"><p>وز گوهر شمشیر ادا کرده ثمن را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر جمله جهان صدقه کند همت رادش</p></div>
<div class="m2"><p>اینخانه ادا را بود اثار نه من را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دل الم فقر برد مرهم جودش</p></div>
<div class="m2"><p>زانگونه که صابون برد از جامه درن را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون از پی تحریر کند خامه گهر بار</p></div>
<div class="m2"><p>قیمت برود مرسله در عدن را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر چرح کمان وش ز پی مدحت جاهش</p></div>
<div class="m2"><p>سوفار صفت تیر گشاد است دهن را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکروز مصافش زتن زار اعادی</p></div>
<div class="m2"><p>صد ساله فزون طعمه دهد زاغ و زغن را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هنگام ملاقات دو صف از تف تیغش</p></div>
<div class="m2"><p>بدرود کند جان سپر برد یمن را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ربقه فرمانش سر آنکس که برون برد</p></div>
<div class="m2"><p>آماده نهاد از پی خود تیغ و کفن را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکس نهد از درگه او روی بغیری</p></div>
<div class="m2"><p>کز جهل کند تره بجا سلوی و من را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ایمظهر الطاف الهی دل پاکت</p></div>
<div class="m2"><p>بشناخته چون مردم یکفن همه فن را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیوسته ز پروانه رأی تو برد نور</p></div>
<div class="m2"><p>این شمع زر اندوده فیروزه لگن را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بانگهت خلق تو ز خود لاف زدن نیست</p></div>
<div class="m2"><p>جز عین خطا نافه آهوی ختن را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از گلشن خلق تو وزد باد بهاری</p></div>
<div class="m2"><p>در باغ که خوشبوی کند صحن چمن را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سرتا سر آفاق چو بگرفت سپاهت</p></div>
<div class="m2"><p>ناچار عدو هاویه بگزید وطن را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شمشیر تو اندر دل چون سنگ عدویت</p></div>
<div class="m2"><p>مانند سنانی که دهد جای مسن را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گه تیغ تو سازد دو تن از یکتن دشمن</p></div>
<div class="m2"><p>گه تیر تو یک تن کند از خصم دو تن را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون دست اجل گردن خصم تو همی بست</p></div>
<div class="m2"><p>از حبل وریدش بسزا یافت رسن را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیداری و هوشیاری بخت تو ربوده است</p></div>
<div class="m2"><p>از چشم بد حاسد جاه تو وسن را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هست ابن یمین داعی جاه تو وبا شد</p></div>
<div class="m2"><p>آگاهی ازین واقف هر سر و علن را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا در نظر دیده وران حسن فزاید</p></div>
<div class="m2"><p>تاب وشکن زلف بت سیم ذقن را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بادا دل اعدای تو از صر صر قهرت</p></div>
<div class="m2"><p>چون زلف بتان کرده ضمان تاب و شکن را</p></div></div>