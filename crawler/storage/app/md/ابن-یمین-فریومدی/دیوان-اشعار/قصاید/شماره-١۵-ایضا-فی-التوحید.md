---
title: >-
    شمارهٔ ١۵ - ایضاً فی التوحید
---
# شمارهٔ ١۵ - ایضاً فی التوحید

<div class="b" id="bn1"><div class="m1"><p>ایدل غافل بدان کاحوال عالم هیچ نیست</p></div>
<div class="m2"><p>پیش زخم حادثات دهر مرهم هیچ نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ز شادی کس نیابد در همه روی زمین</p></div>
<div class="m2"><p>زیر طاق آسمان گوئی که جز غم هیچ نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با خزان عمر و سردی دم باد فنا</p></div>
<div class="m2"><p>نوبهار عمر اگر چه هست خرم هیچ نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سور ایام ولادت و آن نشاط و خرمی</p></div>
<div class="m2"><p>پیش این غم کز پس او هست ماتم هیچ نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آدمی چون هست خاکی بر ره باد فنا</p></div>
<div class="m2"><p>پس حقیقت دان که از وی تا بآدم هیچ نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس نمیداند که بالای فلک احوال چیست</p></div>
<div class="m2"><p>آنچ باری هست زیر چرخ اعظم هیچ نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از برون ماریست دنیا پر ز نقش دلفریب</p></div>
<div class="m2"><p>وز درونش آگه نه آنجا بجز سم هیچ نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بکی گرد زمین گردی بکردار فلک</p></div>
<div class="m2"><p>چون فلک را نیز ازین دور دمادم هیچ نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من گرفتم گشته ئی یم خصم از بس چون یسار</p></div>
<div class="m2"><p>گر بدانی غیر بادی در کف یم هیچ نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ابر با آن سروری و گنج گوهر حاصلش</p></div>
<div class="m2"><p>جز دلی پر آتش و چشمی پر از نم هیچ نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر سلیمانرا بخاتم بود ملک جن و انس</p></div>
<div class="m2"><p>ملک چون بر باد شد پس سعی خاتم هیچ نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود دورانی که جم جام شهنشاهی گرفت</p></div>
<div class="m2"><p>غیر نام اکنون نشان از جام و از جم هیچ نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نادر افتد متفق تدبیر با تقدیر حق</p></div>
<div class="m2"><p>چون اجل آمد دم عیسی مریم هیچ نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حیلت اندر معضلات کار ترک حیلت است</p></div>
<div class="m2"><p>چون بغیر از امتثال حکم مبرم هیچ نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیش از این نامد ز تو کاری که ارزد هیچ چیز</p></div>
<div class="m2"><p>وانچ اکنون کار پنداریش آنهم هیچ نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیروی شرع کن اینست کار ابن یمین</p></div>
<div class="m2"><p>و آنچه غیر از این کنی از بیش و از کم هیچ نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عروه وثقی که دائم باد ایمن ز انفصام</p></div>
<div class="m2"><p>غیر شرع مصطفی در کل عالم هیچ نیست</p></div></div>