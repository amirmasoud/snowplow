---
title: >-
    شمارهٔ ٩۴ - مطلع ثانی
---
# شمارهٔ ٩۴ - مطلع ثانی

<div class="b" id="bn1"><div class="m1"><p>دولت بود مساعد و اقبال و بخت یار</p></div>
<div class="m2"><p>آنرا که کرد بندگی شاه اختیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن شاه داد بخش که دوران دولتش</p></div>
<div class="m2"><p>آرد بمهرگان ستم عهد نو بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلطان شرق و غرب شهنشاه بحر و بر</p></div>
<div class="m2"><p>خورشید ملک سایه الطاف کردگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاه جهان طغایتمور خان که آفتاب</p></div>
<div class="m2"><p>دایم بزیر سایه چترش کند مدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رأیش فکند در دل خورشید آتشی</p></div>
<div class="m2"><p>کانرا ز ثابتات فروزنده شد شرار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر اسب پیلتن خرد او را چو دید گفت</p></div>
<div class="m2"><p>بر شیر آسمان شه سیاره شد سوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در عرض اگر بلجه دریا گذر کنند</p></div>
<div class="m2"><p>خیل و سپاه او که برونند از شمار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گردد شمار چرخ فلک یکعدد فزون</p></div>
<div class="m2"><p>از روی آب بس که رود بر هوا غبار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از رأی پیرو قوت بخت جوان شدست</p></div>
<div class="m2"><p>تا حد قیروانش مسخر ز قندهار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شهباز همتش چو بپرواز برشود</p></div>
<div class="m2"><p>سیمرغ زرنگار فلک را کند شکار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>میپرورد بمهر دل اندر صمیم کان</p></div>
<div class="m2"><p>گردون ز بهر بخشش عامش زر عیار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در روزگار معدلت او گوزن و میش</p></div>
<div class="m2"><p>با شیر گشته همبر و با گرگ همکنار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر منجنیق قهر بگردون روان کند</p></div>
<div class="m2"><p>گردد ز خاک پست تر این نیلگون حصار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاها توئی که خسرو سیاره هر بگاه</p></div>
<div class="m2"><p>بوسد جناب جاه تو از بهر افتخار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حزم تو رسم مستی از آن گونه برفکند</p></div>
<div class="m2"><p>کز چشم دلبران نرود تا ابد خمار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در مصر هر دلی شده مانند زر عزیز</p></div>
<div class="m2"><p>ز آنرو که زر بود بر تو همچو خاک خوار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر ذره ئی ز رأی تو عکسی بر آسمان</p></div>
<div class="m2"><p>اندازد آفتاب دگر گردد آشکار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باد ار فشاند از تف قهرت شراره ئی</p></div>
<div class="m2"><p>بر آب بحر خیزد ازو دود چون بخار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جولان کنان بعرصه میدان آسمان</p></div>
<div class="m2"><p>روزی فتاد باره قدر تو را گذار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نعلی فتاد از سم گردون نورد تو</p></div>
<div class="m2"><p>زودش فلک ز بهر شرف کرد گوشوار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای خسروی که بر درت از سروران عهد</p></div>
<div class="m2"><p>صفها بود کشیده ز هر سو بروز بار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر یک بصفدری و بگردی و پهلوی</p></div>
<div class="m2"><p>از پور زال برده سبق روز کار زار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز آنجمله سروران سر گردنکشان ملک</p></div>
<div class="m2"><p>چون کرد شاه بنده نوازش بزرگوار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برباید از جلالت رتبت بفر شاه</p></div>
<div class="m2"><p>از فرق آفتاب فلک تاج زرنگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>والا نظام دولت و ملت که در جهان</p></div>
<div class="m2"><p>دارد چو آفتاب جهانگیر اشتهار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فرخنده طالعی که شهنشاه عهد راست</p></div>
<div class="m2"><p>کو را چنین خجسته مطیع است و دوستدار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شاها نظام ملت و دین چون بجان کمر</p></div>
<div class="m2"><p>در پای تخت فرخ تو بست بنده وار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گردونش دید پیش تو بر رسم تهنیت</p></div>
<div class="m2"><p>گفت ای ستوده شاه ز شاهان روزگار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>غیر از تو بنده ئی که بود شه نشان که داشت</p></div>
<div class="m2"><p>چشم بد از تو دور وزان گرد نامدار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>او را نواز و تربیت از وی مدار باز</p></div>
<div class="m2"><p>تا مملکت بملک در افزایدت هزار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ختم ثنا کنم پس از این بر دعای خیر</p></div>
<div class="m2"><p>نی بهر آنک بر سخنم نیست اقتدار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اما چو بنده ابن یمین نیک واقف است</p></div>
<div class="m2"><p>بر نازکی طبع تو ای شاه کامکار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آن به که تا ملالت خاطر نباشدت</p></div>
<div class="m2"><p>اطناب را بدل کند اکنون باقتصار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا ز آب و خاک و آتش و با دست در جهان</p></div>
<div class="m2"><p>ترکیب هر چه زیر فلک باشدش قرار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بادا قرار در کنف عدل رأفتت</p></div>
<div class="m2"><p>هر چیز را که هست مرکب ازین چهار</p></div></div>