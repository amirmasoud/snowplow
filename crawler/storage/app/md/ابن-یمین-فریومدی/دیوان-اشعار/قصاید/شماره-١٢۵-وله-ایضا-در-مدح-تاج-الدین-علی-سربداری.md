---
title: >-
    شمارهٔ ١٢۵ - وله ایضاً در مدح تاج الدین علی سربداری
---
# شمارهٔ ١٢۵ - وله ایضاً در مدح تاج الدین علی سربداری

<div class="b" id="bn1"><div class="m1"><p>چون شد سعادت ابدی یار ملک و دین</p></div>
<div class="m2"><p>رونق گرفت بار دگر کار ملک و دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دین و ملک آتش فتنه فرو نشاند</p></div>
<div class="m2"><p>مانند آب خنجر سردار ملک و دین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاج ملوک خواجه علی آنکه زیب و زین</p></div>
<div class="m2"><p>در دین و ملک ازوست باقر ار ملک و دین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سبزست و تازه روضه دین و بهار ملک</p></div>
<div class="m2"><p>تا کلکش آمد ابر گهر بار ملک و دین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زار و نزار ژرده کلکش ز بهر چیست</p></div>
<div class="m2"><p>پیوسته گر بسر نکشد بار ملک و دین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وجه بها چو گوهر تیغست در کفش</p></div>
<div class="m2"><p>نشگفت اگر شدست خریدار ملک و دین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرم است مشتری و گهر میدهد بها</p></div>
<div class="m2"><p>به ز این مخواه گوهر بازار ملک و دین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دایم ز فیض ابر کف درفشان او</p></div>
<div class="m2"><p>کلکش همی کند گهر ایثار ملک و دین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دین پناه ملک ستان گر چه در جهان</p></div>
<div class="m2"><p>بیحد و بیمرند طلبکار ملک و دین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز دولت جوان ترا زان میان نیافت</p></div>
<div class="m2"><p>گردون پیر محرم اسرار ملک و دین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اصحاب ملک و دین همه شاداب و خرمند</p></div>
<div class="m2"><p>ز آندم که هست رأی تو غمخوار ملک و دین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از یمن عدل شامل تو چشم فتنه را</p></div>
<div class="m2"><p>در خواب کرد دولت بیدار ملک و دین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در دین و ملک جز دل خصمت خراب نیست</p></div>
<div class="m2"><p>ز آندم که هست عدل تو معمار ملک و دین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تیغ تو سر بربقه اقرار در کشید</p></div>
<div class="m2"><p>آنرا که بود در دلش انکار ملک و دین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نبود گر اختیار بود دین و ملک را</p></div>
<div class="m2"><p>در به گزین بغیر تو مختار ملک و دین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در نظم دین و ملک شد آثار تیغ تو</p></div>
<div class="m2"><p>فهرست روزنامه اخبار ملک و دین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای سایه خدای توئی آنک بر تو بست</p></div>
<div class="m2"><p>از آفتاب رأی تو انوار ملک و دین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در ظل رأفت ابن یمین را بدار از آنک</p></div>
<div class="m2"><p>تا بود و هست و تا بود آثار ملک و دین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سلطان ستای چون من و سلطان نشان چو تو</p></div>
<div class="m2"><p>نامد پدید و ناید از اقطار ملک و دین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از دین و ملک تا بود آثار در جهان</p></div>
<div class="m2"><p>باری بنام نیک نگهدار ملک و دین</p></div></div>