---
title: >-
    شمارهٔ ۶۵ - ایضاً در مدح نظام الدین یحیی کرابی
---
# شمارهٔ ۶۵ - ایضاً در مدح نظام الدین یحیی کرابی

<div class="b" id="bn1"><div class="m1"><p>گاه آن آمد که عالم جمله باغستان شود</p></div>
<div class="m2"><p>صحن بستان از خوشی چون روضه رضوان شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نرگس رعنا زمستی سر نهد بر پای سرو</p></div>
<div class="m2"><p>غنچه را لب از خواص زعفران خندان شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف سنبل را بیفشاند صبا مشاطه وار</p></div>
<div class="m2"><p>و از دم عنبر نسیمش سایه بخش جان شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابر نیسانی برسم دایگی طفل باغ</p></div>
<div class="m2"><p>از برای شیر دادن جمله تن پستان شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش رخسار گل گردد فروزان ز آب ابر</p></div>
<div class="m2"><p>طبع باد از امتزاج خاک مشک افشان شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر صبا زین دست خیزد زود باشد کز خوشی</p></div>
<div class="m2"><p>عرصه گلشن چو بزم خسرو ایران شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو جمشید فر والا نظام ملک و دین</p></div>
<div class="m2"><p>آنکه دین و ملک ازو با رونق و سامان شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>و آنکه ساز لشکر منصور او را هر بهار</p></div>
<div class="m2"><p>تیغها روید ز بید و غنچه ها پیکان شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و آنکه در هیجا چو گردد غرق تیرش در کمان</p></div>
<div class="m2"><p>جان و تن اعدای او را ترکش و قربان شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که بیند روز کین در رزمگه جولان او</p></div>
<div class="m2"><p>داستان پور دستان پیش او دستان شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وقت زر پاشی و گاه درفشانی دست او</p></div>
<div class="m2"><p>رشک باد اندر خزان و ابر در نیسان شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر کفش را ابر گویم این سخن من بنده را</p></div>
<div class="m2"><p>پیش هر صاحب کمالی موجب نقصان شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فیض دست اوست این کزوی حیات عالمیست</p></div>
<div class="m2"><p>رشحه ابر است آن کو مایه طوفان شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تیغ گوهر دار او ترسد که بخشد گوهرش</p></div>
<div class="m2"><p>در دل تاریک دشمن بهر آن پنهان شود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از سبک سنگی عزمش گر زمین یابد اثر</p></div>
<div class="m2"><p>آسمان وش با گرانی باعث دوران شود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روز حزمش آسمان با آن سبکروحی که هست</p></div>
<div class="m2"><p>حمل یابد از گرانی با زمین یکسان شود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در مکارم هر بنا کان همت رادش نهد</p></div>
<div class="m2"><p>چار رکن آن مشید زین چهار ارکان شود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر جهانگیری تواند کرد شاه اختران</p></div>
<div class="m2"><p>آن بود روزیکه او را بنده فرمان شود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شهریارا کمترین بندگان ابن یمین</p></div>
<div class="m2"><p>چون بدرگاه تو روز بار مدحت خوان شود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر نسیم شعر او بر خاک حسان بگذرد</p></div>
<div class="m2"><p>جان حسان تا قیامت واله و حیران شود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تیر گردون گر تواند گفت شعرش را جواب</p></div>
<div class="m2"><p>از ترفع مسند او ذروه کیوان شود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر چه اینرا شعر میخوانند لیکن گوهریست</p></div>
<div class="m2"><p>کز لطافت خجلت صد گوهر عمان شود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اینچنین گوهر خصوصا در مدیح چون توئی</p></div>
<div class="m2"><p>عقل نپسندد گر اینسان کاسد و ارزان شود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا ز دور آسمان و قرب و بعد آفتاب</p></div>
<div class="m2"><p>گاهگاهی همچو گوی و گاه چون چوگان شود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باد گردان در خم چوگان حکمت همچو گوی</p></div>
<div class="m2"><p>هر سری کز بالش فرمان تو گردان شود</p></div></div>