---
title: >-
    شمارهٔ ۶٩ - ایضاً له
---
# شمارهٔ ۶٩ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>یا رب از من خبری سوی خراسان که برد</p></div>
<div class="m2"><p>قصه درد دل من سوی درمان که برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن ذره که گوید بر خورشید فلک</p></div>
<div class="m2"><p>ناله بلبل شیدا بگلستان که برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس ندانم که رساند بر جانان سخنم</p></div>
<div class="m2"><p>از گدائی سخنی بر در سلطان که برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف او را چو پریشانی خود هست تمام</p></div>
<div class="m2"><p>پیش او نام دل زار پریشان که برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان من تشنه و لعل لب او آب حیات</p></div>
<div class="m2"><p>تشنه ئی را بلب چشمه حیوان که برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یوسف است او و من اندرغم او یعقوبم</p></div>
<div class="m2"><p>سوز یعقوب سوی یوسف کنعان که برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان فرستاد می ایکاش کسی میبردی</p></div>
<div class="m2"><p>تحفه ئی سخت حقیر ست بجانان که برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گیرم احوال دلم دوست رساند بر دوست</p></div>
<div class="m2"><p>وصف شوقم بر آن منبع حیوان که برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنگه از روح قدس عقل بخلوت پرسید</p></div>
<div class="m2"><p>کز شرف ره بسوی ذروه کیوان که برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روح قدسی زسر حیرت و دانش گفتش</p></div>
<div class="m2"><p>آصف عهد یمین دول است آن که برد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه جز دست و دل او بگه جودو کرم</p></div>
<div class="m2"><p>می ندانم بجهان آب یم وکان که برد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بارز دفتر دیوان وجود است ار نی</p></div>
<div class="m2"><p>یاد دفتر که کند راه بدیوان که برد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاطر او بگه فکر برد راه بغیب</p></div>
<div class="m2"><p>راه دشوار بجز خاطرش آسان که برد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صاحبا چون صفت قصر معا لیت رود</p></div>
<div class="m2"><p>نام این طارم فیروزه گردان که برد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شعر نزد تو فرستادم وعقلم میگفت</p></div>
<div class="m2"><p>رشحه کوزه سوی لجه عمان که برد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این ثنا رفع همیکردم و عقلم میگفت</p></div>
<div class="m2"><p>شرم بادت پسرا زیره بکرمان که برد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با همه نضرت و سیرابی گلهای بهشت</p></div>
<div class="m2"><p>شاخ خضرای دمن تحفه برضوان که برد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو سلیمانی ومن مورم و جز مور ضعیف</p></div>
<div class="m2"><p>نزل پای ملخی نزد سلیمان که برد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بیتو زندان بودم گر همه باغ ارم است</p></div>
<div class="m2"><p>بهر گل گر نبود راه ببستان که برد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خود گرفتم من اگر در خور زندان بودم</p></div>
<div class="m2"><p>مفلسی را چو من ای خواجه بزندان که برد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر مرا جان و دل اندر سر کار تو رود</p></div>
<div class="m2"><p>با تو ایجان و دلم نام دل و جان که برد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر دعا ختم کن ای ابن یمین بیش مگوی</p></div>
<div class="m2"><p>نطق باقل بفصاحت بر سبحان که برد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خود گرفتم سخنت هست همه سحر حلال</p></div>
<div class="m2"><p>سحر آخر بسوی موسی عمران که برد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بادت از دور فلک عمر در اقبال فزون</p></div>
<div class="m2"><p>خود بیندیش که تا راه بدوران که برد</p></div></div>