---
title: >-
    شمارهٔ ٨٢ - قصیده گوهر
---
# شمارهٔ ٨٢ - قصیده گوهر

<div class="b" id="bn1"><div class="m1"><p>زهی عقیق تو افشانده بر روان گوهر</p></div>
<div class="m2"><p>ز شرم روی تو آبیست ناروان گوهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهر فشانی لعلت چو آیدم در چشم</p></div>
<div class="m2"><p>فتد ز چشم من زار ناتوان گوهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرشگ بر مژه من ز عکس دندانت</p></div>
<div class="m2"><p>چنان نشست که بر پیکر سنان گوهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو لب بخنده گشائی ز در دندانت</p></div>
<div class="m2"><p>نشست در صدف جان عاشقان گوهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا تفرج این جزع در فشانم کن</p></div>
<div class="m2"><p>کزو شدست پراکنده در جهان گوهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن مگوی که تا هر نفس در آویزد</p></div>
<div class="m2"><p>ز رشک لفظ تو خود را ز ریسمان گوهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو بوسه ئی نفروشی بمن بنقد روان</p></div>
<div class="m2"><p>ببر ز جزع من اینک برایگان گوهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز عکس رسته دندان تو عجب نبود</p></div>
<div class="m2"><p>گرم چو مغز بروید در استخوان گوهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هوای لعل تو کردم فلک بطنزم گفت</p></div>
<div class="m2"><p>که رایگان نتوان یافت ایفلان گوهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترا که در همه عالم وجوه یکشبه نیست</p></div>
<div class="m2"><p>کجا رسد بچنان مفلسی چنان گوهر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگفتم آن گهر و صد چنان بدست آرم</p></div>
<div class="m2"><p>چو یابم از گهر شاه کامران گوهر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>علاء دولت و ملت که فیض بخشش او</p></div>
<div class="m2"><p>درون قلعه خارا بود نهان گوهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر چه کان زره طبع سخت ممسک بود</p></div>
<div class="m2"><p>چو دید همتش آورد با میان گوهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هنر پناه شها کلک تو بغواصی</p></div>
<div class="m2"><p>ز بحر چون شبه آورد بر کران گوهر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیام خود ز دل دشمنش کند تیغش</p></div>
<div class="m2"><p>بلی بسنگ درون میکند مکان گوهر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر پناه بدریا و گر بکوه برد</p></div>
<div class="m2"><p>نیابد از کف در پاش تو امان گوهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز بیم تیغ تو گر بر فراز کوه کشی</p></div>
<div class="m2"><p>چو کهربا شود اندر صمیم کان گوهر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز طیب خلق تو گوئی که غنچه زد نفسی</p></div>
<div class="m2"><p>که کرد ابر بهاریش در دهان گوهر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دهان ابن یمین زان گهر نمای شدست</p></div>
<div class="m2"><p>که هست مدح تو بر خنجر زبان گوهر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همیشه تا دهد اندر جهان ز خامه و تیغ</p></div>
<div class="m2"><p>که آگهی خط چون در گهی نشان گوهر</p></div></div>