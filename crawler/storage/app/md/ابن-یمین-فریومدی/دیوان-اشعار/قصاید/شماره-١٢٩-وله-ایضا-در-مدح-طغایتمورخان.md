---
title: >-
    شمارهٔ ١٢٩ - وله ایضاً در مدح طغایتمورخان
---
# شمارهٔ ١٢٩ - وله ایضاً در مدح طغایتمورخان

<div class="b" id="bn1"><div class="m1"><p>منت خدای را که علی غفله الزمان</p></div>
<div class="m2"><p>پیرانه سر بقوت اقبال نوجوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزدود سرمه وار مرا تیرگی ز چشم</p></div>
<div class="m2"><p>خاک جناب حضرت سلطان کامران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارای دین طغایتمورخان که عدل او</p></div>
<div class="m2"><p>سازد ز گرگ پرورش بره را شبان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاهی که گر خلاف طبیعت دهد مثال</p></div>
<div class="m2"><p>گوی زمین بدور در آید چو آسمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آرد بحکم پوست به پشت پلنگ باز</p></div>
<div class="m2"><p>از پشت زین ارادتش ار باشد اندر آن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دشمن بگاه سورت صفرای کلک او</p></div>
<div class="m2"><p>آرد ز ثقبه عنبی ماء ناردان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهل خرد بتجربه اسرار غیب را</p></div>
<div class="m2"><p>بهتر ز کلک شاه ندیدند ترجمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر یک شرر ز آتش خشمش بگاه کین</p></div>
<div class="m2"><p>یابد گذر بلجه دریای بیکران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ماهی عجب مدان که دم آتشین زند</p></div>
<div class="m2"><p>از تف قهر او چو سمندر در آبدان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از بهر ساز لشکر منصور او کند</p></div>
<div class="m2"><p>چرخ از شهاب تیر وز قوس قزح کمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درعی فراخ چشمه کند جوشن عدو</p></div>
<div class="m2"><p>شاه جهان بناوک دلدوز جانستان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شهباز همتش چو بپرواز بر شود</p></div>
<div class="m2"><p>نسرین چرخ را برباید ز آشیان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از تیغ و تیر شاه مرا روشنشست آنک</p></div>
<div class="m2"><p>یکتن توان دو کرد و دو تن را یکی توان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عین عقاب حادثه از باز رایتش</p></div>
<div class="m2"><p>سیمرغ وار گم شده در قاف قیروان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای ترک میگسار بیا جام می بیار</p></div>
<div class="m2"><p>و آنگاه مطربان خوش آواز را بخوان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا بر کشند نغمه عشاق و این غزل</p></div>
<div class="m2"><p>خواننند روز بار ببزم خدایگان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خیز ای لب تو مایه ده عمر جاودان</p></div>
<div class="m2"><p>در جام لاله فام فکن آب ارغوان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در آب منجمد بفروز آتش مذاب</p></div>
<div class="m2"><p>چون خاک ده بباد فنا انده جهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز آن می که از مسام ترشح گرش بود</p></div>
<div class="m2"><p>آید بجای خون ز بدن قطره روان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جان خواهدم ببوسه بها ترک نوش لب</p></div>
<div class="m2"><p>بادش فدا اگر ببرد هم برایگان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای از هوای شعر سیاه تو از حریر</p></div>
<div class="m2"><p>شد پیکرم ضعیفتر از تار پرنیان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وقت طرب رسید که با نام شهریار</p></div>
<div class="m2"><p>از دشمنان نماند بگیتی درون نشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در ده چنان مئی که ز تأثیر سورتش</p></div>
<div class="m2"><p>گردد خرد سبکرو و سرها شود گران</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صاف و طرب فزای که گوئی سرشته اند</p></div>
<div class="m2"><p>در جان تاک خاصیت طبع زعفران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جان پرورد چو لعل لب روحبخش یار</p></div>
<div class="m2"><p>جامی چو نو بهار بهنگام مهرگان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از دست ساقئی که ز عکس جمال او</p></div>
<div class="m2"><p>شنگرف سوده گردد مغز اندر استخوان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خاصه ببزم خرم شاهی که لطف او</p></div>
<div class="m2"><p>همچون دم مسیح بود مایه بخش جان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شاه جهان طغایتمور خان که حکم اوست</p></div>
<div class="m2"><p>در کاینات مظهر آیات کن فکان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ابن یمین ز دیرگه ای آفتاب ملک</p></div>
<div class="m2"><p>دارد بزیر سایه الطاف تو مکان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در سایه عنایت خود دار بنده را</p></div>
<div class="m2"><p>از تاب آفتاب غم دهر در امان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا برکشد باوج فلک در مدیح تو</p></div>
<div class="m2"><p>شعری که هست شعری گردونش توأمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مهر تو گر بتاب عنایت بپرورد</p></div>
<div class="m2"><p>مهتاب را ظفر نبود بیش بر کتان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا روضه سپهر ز گلهای کاینات</p></div>
<div class="m2"><p>باشد شکفته بر صفت گلشن جهان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بادا گل مراد تو در نوبهار عمر</p></div>
<div class="m2"><p>شاداب و نو شکفته و بی آفت خزان</p></div></div>