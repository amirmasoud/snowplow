---
title: >-
    شمارهٔ ٧١ - قصیده فی مدح پادشاه عالی مقدار تاج الدین علی
---
# شمارهٔ ٧١ - قصیده فی مدح پادشاه عالی مقدار تاج الدین علی

<div class="b" id="bn1"><div class="m1"><p>یارب این خرم نسیم از عالم جان می‌رسد</p></div>
<div class="m2"><p>یا ز بستان ارم یا باغ رضوان می‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا دم پیراهن یوسف شده همراه او</p></div>
<div class="m2"><p>از برای نور چشم پیر کنعان می‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا مشام جان پاک مصطفی را از یمن</p></div>
<div class="m2"><p>همدم آه اویس انفاس رحمان می‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا ز بهر شادی دل‌های غم فرسودگان</p></div>
<div class="m2"><p>از خم زلفین مشک‌افشان جانان می‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا برای راحت ارواح محنت‌دیدگان</p></div>
<div class="m2"><p>چون دم روح القدس با روح و ریحان می‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا بشارت می‌دهد کز قتلگاه دشمنان</p></div>
<div class="m2"><p>بر مراد دوستان رایات سلطان می‌رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاج ملک و دین علی کآنچ از مطالب آرزوست</p></div>
<div class="m2"><p>بودش اکنون با حصول و بیشتر زان می‌رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان فدای این بشارت کاهل دل را خوش‌ترست</p></div>
<div class="m2"><p>زان بشارت کز صبا نزد سلیمان می‌رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر مبشر دوستان او زرافشان می‌کنند</p></div>
<div class="m2"><p>از نشاط آنک شاهنشاه ایران می‌رسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مردم چشم عدو هم می‌کند گوهر نثار</p></div>
<div class="m2"><p>وین عجب نبود ازو کز بحر عمان می‌رسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در صف هیجا ز بهر قهر بدخواهان او</p></div>
<div class="m2"><p>لشکرش را دم‌به‌دم تأیید یزدان می‌رسد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روی خصم از تیغ سبزش گر چه زرد آمد چو زر</p></div>
<div class="m2"><p>لیک در سرخی سرشک او به مرجان می‌رسد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سعد و نحس آسمان چون خیر و شر قسمت کند</p></div>
<div class="m2"><p>قسم او و خصم او هر یک دگرسان می‌رسد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از سعادت افسر و اورنگ می‌افتد بدو</p></div>
<div class="m2"><p>وز شقاوت خصم او را بند و زندان می‌رسد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از سنان آبدار او به دشمن آن رسد</p></div>
<div class="m2"><p>کز شهاب آتشین‌پیکر به شیطان می‌رسد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حاسدش را آرزوی رتبتش باشد ولی</p></div>
<div class="m2"><p>کی زهاب پارگین در آب حیوان می‌رسد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عدل او گشتست شامل خلق عالم را ولیک</p></div>
<div class="m2"><p>از دل و دستش ستم بر بحر و برکان می‌رسد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با عطای بحر دست درفشانش قطره‌ای‌ست</p></div>
<div class="m2"><p>آنچه عالم را ز فیض ابر نیسان می‌رسد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رتبتی میجست خصمش عقل گفت آهسته باش</p></div>
<div class="m2"><p>آهت آخر بس که بر گردون گردان می‌رسد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا همی‌رانم سخن در ارتفاع قدر او</p></div>
<div class="m2"><p>از بلندی شعر من بر اوج کیوان می‌رسد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرچه اهل فضل را از صدمت گردون دون</p></div>
<div class="m2"><p>زخم بی‌اندازه و رنج فراوان می‌رسد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آفتاب لطف او چون سایه دارد بر سرم</p></div>
<div class="m2"><p>دم‌به‌دم درد مرا دارو و درمان می‌رسد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با چنین الطاف بی‌غایت که بادا بر مزید</p></div>
<div class="m2"><p>بر تواتر بنده را از شاه شاهان می‌رسد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زود گویند اهل فریومد به هم کابن‌یمین</p></div>
<div class="m2"><p>اینک از تکلیف دیوان گشته تر خان می‌رسد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شهریارا خاطر وقاد من در مدح تو</p></div>
<div class="m2"><p>بر سر معنی مشکل گر چه آسان می‌رسد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لیک خواهم از ثنا سوی دعا آمد و لیک</p></div>
<div class="m2"><p>هست ناممکن که آن دفتر به پایان می‌رسد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا فلک دایر بود باد اقتضای دور تو</p></div>
<div class="m2"><p>آنچنان کت هر زمان ملکی ز دوران می‌رسد</p></div></div>