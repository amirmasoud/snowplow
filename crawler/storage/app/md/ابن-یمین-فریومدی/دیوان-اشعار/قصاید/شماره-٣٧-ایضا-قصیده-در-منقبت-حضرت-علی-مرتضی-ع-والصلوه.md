---
title: >-
    شمارهٔ ٣٧ - ایضاً قصیده در منقبت حضرت علی مرتضی(ع) والصلوه
---
# شمارهٔ ٣٧ - ایضاً قصیده در منقبت حضرت علی مرتضی(ع) والصلوه

<div class="b" id="bn1"><div class="m1"><p>آنرا که پیشوای دو عالم علی بود</p></div>
<div class="m2"><p>نزد خدای منزلتی بس علی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اقبال دارد آنکه زند دم ز دوستیش</p></div>
<div class="m2"><p>بل بندگی قنبرش از مقبلی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امروز هر دلی که تهی باشد از ولاش</p></div>
<div class="m2"><p>روز جزا ز نار سقر ممتلی بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر اتفاق مرشد و هادی اولیاست</p></div>
<div class="m2"><p>از نور اوست مقتبس آنکو ولی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرطست در نماز جماعت امام را</p></div>
<div class="m2"><p>کاو را از آن میان صفت افضلی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فاضل بجای ماندن و مفضول را امام</p></div>
<div class="m2"><p>کردن نه در طریقه حق مبطلی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کس که مؤمنست بفرمان مصطفی</p></div>
<div class="m2"><p>مولاش اگر عناد ندارد علی بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر فیض او مدد نکند خاطر مرا</p></div>
<div class="m2"><p>آخر مرا بگوی که این پر دلی بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ممدوح از این قبیل که گفتم فضایلش</p></div>
<div class="m2"><p>گفتن مدیح غیر وی از جاهلی بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا زنده ماند ابن یمین کار افضلش</p></div>
<div class="m2"><p>در گلشن مدایح او بلبلی بود</p></div></div>