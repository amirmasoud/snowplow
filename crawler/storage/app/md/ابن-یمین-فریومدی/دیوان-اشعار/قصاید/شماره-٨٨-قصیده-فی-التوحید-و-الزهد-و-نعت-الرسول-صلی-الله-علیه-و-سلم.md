---
title: >-
    شمارهٔ ٨٨ - قصیده فی التوحید و الزهد و نعت الرسول صلی الله علیه و سلم
---
# شمارهٔ ٨٨ - قصیده فی التوحید و الزهد و نعت الرسول صلی الله علیه و سلم

<div class="b" id="bn1"><div class="m1"><p>موسم پیری رسید ایدل جوانی ترک گیر</p></div>
<div class="m2"><p>ز آنکه نالایق بود کار جوان از مرد پیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه آن در تیرگی شام دستت میدهد</p></div>
<div class="m2"><p>می نشاید کرد چون روشن شود صبح منیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذر از کار جهان اکنون که داری اختیار</p></div>
<div class="m2"><p>پیشتر کآن اضطرارت بگذراند ناگزیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیش چون بلبل هوای گلشن دنیا مکن</p></div>
<div class="m2"><p>چون ترا بر سر سمن بشکفت و بر عارض زریر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پای مرغ جان ز دام زلف جانان برگشای</p></div>
<div class="m2"><p>تا زند بر شاخسار سدره و طوبی صفیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل چه بندی اندرین فانی سرای مستعار</p></div>
<div class="m2"><p>چون کس دیگر بود هر ساعت او را مستعیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقد عمر خویش را از غش عصیان پاک دار</p></div>
<div class="m2"><p>ز آنکه ره داری به پیش و زادت اینست ایفقیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ورنه بی برگ و نوا مانی تو در بازار حشر</p></div>
<div class="m2"><p>قلب روی اندود داری وانگهی ناقد بصیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه ریزی از هوا بر خاک آبروی خویش</p></div>
<div class="m2"><p>چون شرر بیرون جهی از آتش چرخ اثیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای دل از چاه ضلالت گر خلاصی بایدت</p></div>
<div class="m2"><p>عروه وثقای شرع احمد مختار گیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تاج بخش انبیا کاندر شب معراج قدس</p></div>
<div class="m2"><p>بر گذشت از عرش و کرسی زد فراز آن سریر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن سپهر شفقت و رحمت که مهرش تافتست</p></div>
<div class="m2"><p>بر وضیع و بر شریف و بر صغیر و بر کبیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>و آنکه رغم دشمنانرا ساخت از سیمین سپر</p></div>
<div class="m2"><p>یکدو قوس اندر خور سدره بانگشت چو تیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کرد بر دعویش اظهار شهادت سوسمار</p></div>
<div class="m2"><p>با عزیز و با ذلیل و با عظیم و با حقیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سایه او کس ندیدی ز آنکه بودی نور پاک</p></div>
<div class="m2"><p>سایه ظلمانی بود باشد محال از مستنیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گوش او در خواب و بیداری و چشم از پیش و پس</p></div>
<div class="m2"><p>بود بر سمع و بصر قادر بفرمان قدیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا نشاند آتش اشراک عالم سوز را</p></div>
<div class="m2"><p>از پی اظهار معجز دست او شد آبگیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکزمان کز متکا مهر نبوت وا گرفت</p></div>
<div class="m2"><p>چوب خشگ مسجد آمد از تأسف در نفیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قطره ئی چند از مساس پای گردون سای او</p></div>
<div class="m2"><p>کرد شیرین آب شور و تلخ را در قعر بیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز اطلس گردون ببالای رفیع قدر او</p></div>
<div class="m2"><p>کسوتی میدوخت خیاط ازل آمد قصیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در جهان ز اهل فصاحت چون کتابش سورتی</p></div>
<div class="m2"><p>کس نیاورد ارچه بعضی بود بعضی را ظهیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نسخ آیاتش بدین غیر او نا ممکن است</p></div>
<div class="m2"><p>ز آنکه نی بهتر از آن باشد نه نیز آنرا نظیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر که سر برتابد از درگاه جنت رتبتش</p></div>
<div class="m2"><p>ز آسمانش آید ندا سحقا لا صحاب السعیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا زبانم میسر اید نعت آن صاحب کمال</p></div>
<div class="m2"><p>از اثیرم گر مروتر گشت در فکرت ضمیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر چه نعتش را چو آرد بر زبان ابن یمین</p></div>
<div class="m2"><p>با زبانی پر زه افتد در کمان از غصه تیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>میشود در وصف او حیران بلکنت این زبان</p></div>
<div class="m2"><p>گر همه منشی دیوان فلک باشد دبیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ور درختان باشد او را کلک و دریاها مداد</p></div>
<div class="m2"><p>ور پی انشا ز اوراق فلک سازد حریر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پس کند تحریر وصف ذات پاکش تا بحشر</p></div>
<div class="m2"><p>در خیالم نیست کآرد در قلم عشر عشیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یا رسول الله اگر چه مجرمم تائب شدم</p></div>
<div class="m2"><p>دستگیری کن مرا در روز سر مستتیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نیستم نومید اگر چه مسرفم زیرا که هست</p></div>
<div class="m2"><p>آیه لا تقنطوا من رحمه الله دلپذیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در طریق اهل عرفان نا امیدی شرط نیست</p></div>
<div class="m2"><p>مصطفی گر چه نذیر آمد هم او آمد بشیر</p></div></div>