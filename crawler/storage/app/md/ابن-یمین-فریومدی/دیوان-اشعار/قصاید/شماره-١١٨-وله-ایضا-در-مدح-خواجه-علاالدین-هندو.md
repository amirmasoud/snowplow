---
title: >-
    شمارهٔ ١١٨ - وله ایضاً در مدح خواجه علاءالدین هندو
---
# شمارهٔ ١١٨ - وله ایضاً در مدح خواجه علاءالدین هندو

<div class="b" id="bn1"><div class="m1"><p>روز جشن عربست ای مه خوبان عجم</p></div>
<div class="m2"><p>وقت شادیست مباش از غم ایام دژم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می خور اندوه و غم گیتی بد عهد مخور</p></div>
<div class="m2"><p>که کرا می نکند گیتی بد عهد بغم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعد ازین رایت عیش و طرب افراخته دار</p></div>
<div class="m2"><p>کز افق ماه نو عید برافراخت علم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز عیدست بخور باده گلگون و بده</p></div>
<div class="m2"><p>کآرزو میکندم باده ولی با تو بهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باده از دست تو در مجلس دستور جهان</p></div>
<div class="m2"><p>آب حیوان بود اندر چمن باغ ارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان فزاید می گلگون ز کف همچو توئی</p></div>
<div class="m2"><p>خاصه در مجلس دارای عرب شاه عجم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آصف عهد علاء دول و دین هندو</p></div>
<div class="m2"><p>که بود تارک ترک فلکش زیر قدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن جوانبخت که دائم فلک پیر بود</p></div>
<div class="m2"><p>بر درش حلقه صفت پشت بخدمت زده خم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکه از غیرت بحر کف او ابر بهار</p></div>
<div class="m2"><p>دارد اندر دل و در دیده مدام آتش و نم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کان ممسک بسخا چون کف رادت نبود</p></div>
<div class="m2"><p>رشحه کوزه شناسد خرد از بحر خضم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دشمن از وی شود انجم صفت از مهر نهان</p></div>
<div class="m2"><p>گر چه ز انجم بودش بر صفت مهر حشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تیغ برانش گه رزم تو گوئی که مگر</p></div>
<div class="m2"><p>رود نیل است روان گشته دراو آب بقم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خسروا چون سخن از رتبت و جاه تو رود</p></div>
<div class="m2"><p>آسمانرا نرسد دم زدن از ملکت جم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کسوت ملک ببالای تو خیاط ازل</p></div>
<div class="m2"><p>آن چنان دوخت که یکحبه نه بیش است و نه کم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا در حضرت میمون تو اقبال گشاد</p></div>
<div class="m2"><p>دولت احرام درش بست چو زوار حرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صیقل رأی تو چون آینه از زنگ زدود</p></div>
<div class="m2"><p>هر چه بر صفحه اوراق بد از ظلم و ظلم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شد سیاه آینه جان بد اندیش ز زنگ</p></div>
<div class="m2"><p>بس که دادش فلک آینه گون عشوه و دم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر بر آهو بره از نام تو حرزی بندند</p></div>
<div class="m2"><p>از دلیری نخورد شیر جز از شیر اجم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خاکپای تو اگر باد رساند بچمن</p></div>
<div class="m2"><p>گردش از دیده نرگس ببرد آفت نم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خلق را گر نزدی داعی جود تو صلا</p></div>
<div class="m2"><p>کی بصحرای وجود آمدی از کتم عدم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حرص را گر چه بود علت جوع کلبی</p></div>
<div class="m2"><p>چار پهلو کند از خوان نوال تو شکم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خرد ار رأی تو بیند که ببازار فلک</p></div>
<div class="m2"><p>از زر مغربی مهر روانست درم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر دعا ختم کند ابن یمین مدح ترا</p></div>
<div class="m2"><p>ز آنکه بر کسوت مدح تو دعائیست علم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا ز نوک قلم کاتب تقدیر بود</p></div>
<div class="m2"><p>بر رخ تخته گردون به بد و نیک رقم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بادش از آب سیه دیده بکردار دوات</p></div>
<div class="m2"><p>هر که سر بر خط فرمانت ندارد چو قلم</p></div></div>