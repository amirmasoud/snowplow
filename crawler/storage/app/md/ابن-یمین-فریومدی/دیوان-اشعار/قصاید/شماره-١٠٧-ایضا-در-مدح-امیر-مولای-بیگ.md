---
title: >-
    شمارهٔ ١٠٧ - ایضاً در مدح امیر مولای بیگ
---
# شمارهٔ ١٠٧ - ایضاً در مدح امیر مولای بیگ

<div class="b" id="bn1"><div class="m1"><p>ندانم آن رخ حورست یا جمال ملک</p></div>
<div class="m2"><p>که رشک میبرد از حسنش آفتاب فلک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسان دائره کارم شدست بی سر و پای</p></div>
<div class="m2"><p>ز عشق آن دهن همچو نقطه کوچک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهی جمال تو بر هم شکسته رونق حور</p></div>
<div class="m2"><p>خهی ز شرم تو اندر حجاب رفته ملک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمان ابروی مشکین کشیده تا بن گوش</p></div>
<div class="m2"><p>گشاده بر هدف جان عاشقان ناوک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدست پسته شرینت شور هفت اقلیم</p></div>
<div class="m2"><p>شگفت نیست چو دروی مرکبست نمک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرد چو زلف ترا دید بر رخت میگفت</p></div>
<div class="m2"><p>که هندوئیست بسی نیکبخت و بس زیرک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رخ چو ماه تو از زیر زلف میتابد</p></div>
<div class="m2"><p>چنانک نور یقین در میان ظلمت شک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم چو ماهی بر خاک میطپد ز آندم</p></div>
<div class="m2"><p>که گرد آب کشیدی ز مشک ناب شبک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکن ستم صنما بر دلم که ناگاهی</p></div>
<div class="m2"><p>رسد بسرور آفاق این سخن یکیک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امیر شاهنشان سرور جهان مولای</p></div>
<div class="m2"><p>که صیت عدل ویست از سماک تا بسمک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>محیط مرکز رفعت که تیغ معدلتش</p></div>
<div class="m2"><p>کند ز صفحه گیتی نشان حادثه حک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو کلکش از پی ضبط جهان میان دربست</p></div>
<div class="m2"><p>فکند مهر شبان گرگ بر سر شیشک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بیقراری کلکش جهان گرفت قرار</p></div>
<div class="m2"><p>چنان کز آب نیابد دگر گزند آهک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بظل رأفتش ار فی المثل رود گنجشک</p></div>
<div class="m2"><p>شود موافق طبعش چو دانه سنگ تفک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز رشک نفحه گلزار خلق فایح او</p></div>
<div class="m2"><p>مژه بدیده دشمن درون شدست خسک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بگاه کوشش و بخشش بر او حسد دارد</p></div>
<div class="m2"><p>روان رستم دستان و یحیی برمک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز بیم خنجر او خصم مأمنی میجست</p></div>
<div class="m2"><p>قضا بگوشه چشمش نمود هفت درک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>توئی که خصم تو در عرضگاه نقد هنر</p></div>
<div class="m2"><p>دو روی همچو زر آمد سیاه دل چو محک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نوشت قاضی تقدیر بر صحیفه دهر</p></div>
<div class="m2"><p>امارت همه روی زمین بنام تو چک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو گشت مرکب قدر تو ابلق گردون</p></div>
<div class="m2"><p>شدند ماه و خورش گوی گردن و طاسک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قضا چو تیغ قدر پیکر تو در گه رزم</p></div>
<div class="m2"><p>ز حرف تیغ تو خواند این که العد و هلک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سزد که خصم تو نالد رباب وار از آنک</p></div>
<div class="m2"><p>خمید قامتش از بار فسق همچو خرک</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حیات حاسد جاهت بیکنفس گروست</p></div>
<div class="m2"><p>رسید نوبت آن کان ازو شود منفک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو گشت ابن یمین مادحت مسلم شد</p></div>
<div class="m2"><p>له ولایه فضل کما الاماره لک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سپاه فکر من آفاق را گرفت چنانک</p></div>
<div class="m2"><p>دعای جاه تو لشکر کش است و فتح یزک</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>منم بتربیت اولی ولیک پیش از ما</p></div>
<div class="m2"><p>وجود فاطمه بودست و غیر برده فدک</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همیشه تا بتموز و به دی خلایق را</p></div>
<div class="m2"><p>دهد بگرمی و سردی خلاص جنبش و تک</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جهان بحکم تو بادا چنانک گر خواهی</p></div>
<div class="m2"><p>کند اشارت تو بسته بر قضا مسلک</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو قمری آنکه بگردنش طوق حکمت نیست</p></div>
<div class="m2"><p>خروس وار مبادش جز اره بر تارک</p></div></div>