---
title: >-
    شمارهٔ ۶١ - ایضاً قصیده در مدح تاج الدین علی
---
# شمارهٔ ۶١ - ایضاً قصیده در مدح تاج الدین علی

<div class="b" id="bn1"><div class="m1"><p>صبح صادق بیرق نور از افق چون برکشید</p></div>
<div class="m2"><p>خسرو سیارگان از باختر لشکر کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه ترک از باختر با تیغ زرین حمله کرد</p></div>
<div class="m2"><p>رای هند از رأی او رایت سوی خاور کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد روان کشتی برود نیل چون ملاح غیب</p></div>
<div class="m2"><p>بادبان از پرنیان زرنگارش در کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نو عروس گلعذار طارم نیلوفری</p></div>
<div class="m2"><p>از حریر زرکش اندر فرق سر معجر کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دختران سبز پوش حجله زربفت را</p></div>
<div class="m2"><p>بر مثال مادران در سیمگون چادر کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر زمان صورتگر فکرت بنوک کلک صنع</p></div>
<div class="m2"><p>بر حریر نیل پیکر صورتی دیگر کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خرد گفتم چه حالست اینکه آئین بند لطف</p></div>
<div class="m2"><p>چارطاق آسمانرا در زر و زیور کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت کاین بهر تفرج همت دارای ملک</p></div>
<div class="m2"><p>دامن رفعت مگر خواهد بر این منظر کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همت او چون قدم در عالم مینا نهد</p></div>
<div class="m2"><p>خواهد اندر پای او دیبای زرد زر کشید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باز گفتم کیست بر روی زمین امروز آنک</p></div>
<div class="m2"><p>همت او پا تواند ز آسمان برتر کشید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت ملک آرای عالم آنکه رایش خط نسخ</p></div>
<div class="m2"><p>بی قلم بر لوح سیمین مه انور کشید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بحر جود و کان احسان تاج ملک و دین علی</p></div>
<div class="m2"><p>آنکه در مردی و رادی نام چون حیدر کشید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنک اعدا میکشند از رمح او در روز کین</p></div>
<div class="m2"><p>آنکه شب دیو از مسیر پیلک اختر کشید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غنچه و بید از برای قتل بدخواهان او</p></div>
<div class="m2"><p>آن بطوع طبع پیکان ساخت وین خنجر کشید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زو عدو ور خود بود در حصن هفت او رای چرخ</p></div>
<div class="m2"><p>آن کشد کز دست حیدر مالک خیبر کشید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر کجا نمرود فعلی میکشد در عهد او</p></div>
<div class="m2"><p>آن خلاقت کز خلیل الله بت آزر کشید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سرکشی ناید ز خصمش ز آنکه چرخ چنبری</p></div>
<div class="m2"><p>گردنش را چون رسن در حلقه چنبر کشید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از طریق خاصیت گشتست حرز ملک و دین</p></div>
<div class="m2"><p>هر رقم کو بر رخ کافور از عنبر کشید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بره را همچون سگ چوپان نگهبان گشت گرگ</p></div>
<div class="m2"><p>عدل او تا خط بطلان ظلم را بر سرکشید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خصمش از بحرین چشم خویشتن غواص وار</p></div>
<div class="m2"><p>تا نثار خاک پای او کند گوهر کشید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سعی باطل کرد آن کز مصر سوی سبزوار</p></div>
<div class="m2"><p>با وجود لفظ شیرینکار او شکر کشید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خسروا منشی گردون استفادت میکند</p></div>
<div class="m2"><p>زین رقم کابن یمین بهر تو در دفتر کشید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر چه میگوید خرد کاین نیست یکسر بار دل</p></div>
<div class="m2"><p>کو بعالی درگه دارای بحر و بر کشید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لیک باید ز او تحمل کردنت از بهر آنک</p></div>
<div class="m2"><p>رنج ارباب هنر دائم هنر پرور کشید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر دعا خواهم ثنا را ختم کردن بعد ازین</p></div>
<div class="m2"><p>ز آنکه ابرامم ز حد بگذشت و دور اندرکشید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا کسی در دار دنیا هرچه کرد از خیر و شر</p></div>
<div class="m2"><p>در سرابستان عقبی بایدش کیفر کشید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در سرا بستان دنیا شاد باش و دوستکام</p></div>
<div class="m2"><p>ز آنکه دشمن رخت هستی زین سرا بر در کشید</p></div></div>