---
title: >-
    شمارهٔ ۶٠ - ایضاً له قصیده در مدح طغاتیمور خان
---
# شمارهٔ ۶٠ - ایضاً له قصیده در مدح طغاتیمور خان

<div class="b" id="bn1"><div class="m1"><p>صبح سعادت از افق خرمی دمید</p></div>
<div class="m2"><p>ساقی بیار باده که وقت طرب رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیز آتش گداخته در آب بسته ریز</p></div>
<div class="m2"><p>یعنی که آبگینه ملون کن از نبید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر گشاد کار بده جام خوشگوار</p></div>
<div class="m2"><p>قفل مراد را نبود همچو می کلید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانی که برد سود ز بازار روزگار</p></div>
<div class="m2"><p>آنکو فروخت انده و شادی دل خرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برنه بدست ابن یمین جام خسروی</p></div>
<div class="m2"><p>کزوی شود چو لاله رخ همچو شنبلید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا درکشد بدولت شاهنشه جهان</p></div>
<div class="m2"><p>کآنرا کسی دگر نتواند چو او کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه جهان طغایتمورخان که آسمان</p></div>
<div class="m2"><p>هر چند گشت گرد زمین مثل او ندید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مانند خضر زنده جاوید ماند آنک</p></div>
<div class="m2"><p>ز آبحیات رأفت او شربتی چشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر خصم او ز اطلس گردون لباس کرد</p></div>
<div class="m2"><p>مانند کرم قز کفن خویشتن تنید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در باغ ملک چون گل اقبال او شکفت</p></div>
<div class="m2"><p>پشت عدو بنفشه وش از بار غم خمید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر کو بساط حضرت میمونش بوسه داد</p></div>
<div class="m2"><p>و آن پرسش و نوازش دلجوی او شنید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از شوق شکر سخن دلگشای او</p></div>
<div class="m2"><p>طوطی جانش از قفس تن برون پرید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نرد مراد باختن آغاز کرد خصم</p></div>
<div class="m2"><p>چون کم زد او اول ازو مهره باز چید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پرواز باز رایت او دشمنی که دید</p></div>
<div class="m2"><p>در گوشه همچو زاغ کمان بایدش خزید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون از صفای رأی وی آگاه گشت صبح</p></div>
<div class="m2"><p>زد آه سرد از حسد و پیرهن درید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آمد بجوش خون عدوش و بسر نرفت</p></div>
<div class="m2"><p>گفتی که موی او چو زرو خونش برمکید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاها ز یمن عدل تو امروز در جهان</p></div>
<div class="m2"><p>آن شد که گرگ از نظر میش می رمید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا در پناه دولت بیدار تست ملک</p></div>
<div class="m2"><p>در خواب رفت فتنه و آشوب آرمید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چندان ز بحر دست تو شد ابر شرمسار</p></div>
<div class="m2"><p>کزوی بجای خوی همه آب حیا چکید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از باغ ملک بوی بهی خاست لاجرم</p></div>
<div class="m2"><p>همچون انار خصم ترا دل ز غم کفید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زلف عروس ملک تو کش نام پرچم است</p></div>
<div class="m2"><p>حبل الله است کش نتواند کسی برید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز آواز کوس و طبل تو بدخواه را برزم</p></div>
<div class="m2"><p>دیدم که دل چو رایت خفاق میطپید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شاها کمینه بنده میمون جناب تو</p></div>
<div class="m2"><p>کز کاینات حضرت عالیت را گزید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شیرین نکرد از عسل روزگار کام</p></div>
<div class="m2"><p>تا کی زمانه منج صفت خواهدش گزید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وقتست اگر برین دل رنجور ناتوان</p></div>
<div class="m2"><p>خواهد نسیم گلشن الطاف او وزید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا ماه و آفتاب برین کاخ زر نگار</p></div>
<div class="m2"><p>خواهند روز و شب ز پی یکدگر دوید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عمر تو همچو ماه نو ای آفتاب ملک</p></div>
<div class="m2"><p>بادا اگر چه قافیه دالست بر مزید</p></div></div>