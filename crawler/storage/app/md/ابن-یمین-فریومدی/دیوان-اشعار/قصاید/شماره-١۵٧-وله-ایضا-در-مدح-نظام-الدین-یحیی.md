---
title: >-
    شمارهٔ ١۵٧ - وله ایضاً در مدح نظام الدین یحیی
---
# شمارهٔ ١۵٧ - وله ایضاً در مدح نظام الدین یحیی

<div class="b" id="bn1"><div class="m1"><p>عیدست در ده ایصنم گلعذار می</p></div>
<div class="m2"><p>بنمای صورت طرب اندر صفای وی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد کار عیش ساخته از عید همچو چنگ</p></div>
<div class="m2"><p>زین پس میان ببند ز بهر طرب چو نی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطرب بگوی نغمه خوش-زهد تا بچند</p></div>
<div class="m2"><p>ساقی بیار ساغر می توبه تا بکی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با جام می نشین که درین دور بی ثبات</p></div>
<div class="m2"><p>صافی دل بدست نیاید چو جام می</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ده میی که عرصه بزم از فروغ او</p></div>
<div class="m2"><p>خوشتر ز نوبهار نماید بماه دی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آن می که گفتمش بصفا هست آفتاب</p></div>
<div class="m2"><p>عقلم شنید گفت چه گفتی خموش هی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از نورش آفتاب اگر مقتبس شود</p></div>
<div class="m2"><p>منشور حسن او نشود در کسوف طی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم پس آفتاب نگویم چه گویمش</p></div>
<div class="m2"><p>گفتا که عکس خاطر دستور نیک پی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>والا نظام دولت و ملت که رأی او</p></div>
<div class="m2"><p>بر روی آفتاب نشاند ز شرم خوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن سروریکه در طلب فرخی همای</p></div>
<div class="m2"><p>آید عقاب رایت او را بزیر پی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گیرد بیک سوار و ببخشد بیک سؤال</p></div>
<div class="m2"><p>در رزم و بزم کشور آفاق و ملک ری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون همتش بعالم علوی سفر کند</p></div>
<div class="m2"><p>آرد بزیر پای ز رفعت سر جدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرگز برزم و بزم درون هیچ بخردی</p></div>
<div class="m2"><p>یک پی نبیندش که نگوید هزار پی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کآمد بفال سعد دگر باره در جهان</p></div>
<div class="m2"><p>رستم ز سمت زابل و حاتم ز راه طی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ننهاد پا ز کتم عدم خلق در وجود</p></div>
<div class="m2"><p>تا جود او نگفت که ارزاقکم علی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نشگفت اگر رود بسر و پای غرم باز</p></div>
<div class="m2"><p>در عهد عدل او زکمان جمله شاخ و پی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گردون پیر گفته بشفقت هزار بار</p></div>
<div class="m2"><p>با بخت او که انبتک الله یا صبی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ایخسروی که رأی تو اندر ضمیر خصم</p></div>
<div class="m2"><p>نور هدایتست نهان در میان غی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سوء المزاج خصم تو چون دیر در کشید</p></div>
<div class="m2"><p>آن به که شربتش بدهند از لعاب حی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قدر ترا ز اطلس گردون کند قبا</p></div>
<div class="m2"><p>ای در کلاه گوشه قدرت شکوه کی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نعل سم سمند تو هر ماه مینهد</p></div>
<div class="m2"><p>بر داغگاه ابلق گردون بجای کی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ارباب فضل را بجناب رفیع تو</p></div>
<div class="m2"><p>چندان تفاخرست که اعراب را بحی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عدل تو گر نه دافع ظلم فلک شدی</p></div>
<div class="m2"><p>بر صفحه وجود نماندی نشان شیی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کار جهانیان بنظام از وجود تست</p></div>
<div class="m2"><p>بادت وجود تا بود اندر زمانه حی</p></div></div>