---
title: >-
    شمارهٔ ١٣ - وله ایضاً فی التوحید
---
# شمارهٔ ١٣ - وله ایضاً فی التوحید

<div class="b" id="bn1"><div class="m1"><p>ایدل گرت شناختن راه حق هواست</p></div>
<div class="m2"><p>خود را بدان که عارف خود عارف خداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم ره مده بخویشتن ار وقت خوش نماند</p></div>
<div class="m2"><p>زیرا که وقت فوت شد اما خوشی بجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دم سر از دریچه دیگر برون کند</p></div>
<div class="m2"><p>گه آب و گاه آتش و گه خاک و گه هواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاهی فرشته گاه پری گاه آدمیست</p></div>
<div class="m2"><p>گه دیو زشت پیکر و گه حور خوش لقاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم زوست نور و ظلمت و هم زوست کفر و دین</p></div>
<div class="m2"><p>هم ماجرا ازوست هم او مایه صفاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دم چو نو عروس کند جلوه دگر</p></div>
<div class="m2"><p>گه بر زمین سهی و گهی بر فلک سهاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معنی یکیست در نظر عقل دور بین</p></div>
<div class="m2"><p>از راه صورت ار چه که بیحد و منتهاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد نام اگر بود بازای حقیقتی</p></div>
<div class="m2"><p>بر اتفاق مذهب فرزانگان رواست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چیزیکه هست هست نه کم میشود نه بیش</p></div>
<div class="m2"><p>و آن خود که نیست نیست چو سیمرغ و کیمیاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگذشت دورها که درین درج زرنگار</p></div>
<div class="m2"><p>نه یک شبه فزود و نه زو یک گهر بکاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر شد نهان بزیر پر زاغ تیره شب</p></div>
<div class="m2"><p>باز سفید روز مپندار کان فناست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جائی اگر ز غیبت او تیره شد جهان</p></div>
<div class="m2"><p>جای دگر ز پرتواش آفاق با ضیاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در دی بزیر خرقه فرو رفت زاهدی</p></div>
<div class="m2"><p>این می پرست اوست که امروز در قباست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مقصود هر دو کون توئی از فنا مترس</p></div>
<div class="m2"><p>چون آب زندگی تو از منبع بقاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ایدل نمود ابن یمین راه حق بتو</p></div>
<div class="m2"><p>گر غیر این طریق صواب آیدت خطاست</p></div></div>