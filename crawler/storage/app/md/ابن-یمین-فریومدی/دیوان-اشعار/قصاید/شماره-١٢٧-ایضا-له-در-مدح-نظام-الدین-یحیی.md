---
title: >-
    شمارهٔ ١٢٧ - ایضاً له در مدح نظام الدین یحیی
---
# شمارهٔ ١٢٧ - ایضاً له در مدح نظام الدین یحیی

<div class="b" id="bn1"><div class="m1"><p>ساقی بیار باده که چون خلد شد چمن</p></div>
<div class="m2"><p>شد باغ روشن از گهر ابر تیره تن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آهوی سرو نافه بیفکند وزین قبل</p></div>
<div class="m2"><p>باد صبا گرفت دم نافه ختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زد بر نوای بلبل شیدا چنار دست</p></div>
<div class="m2"><p>وز ذوق آن برقص در استاد نارون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد روی آبگیر چو سوهان آژده</p></div>
<div class="m2"><p>تا از صبا فتاد بر اندام او شکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لاله ز بس که قطره شبنم برو نشست</p></div>
<div class="m2"><p>شد ساغر عقیق پر از لولو عدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از روشنی انجم گلهای بوستان</p></div>
<div class="m2"><p>گوئی مگر مجره کشیدند بر چمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز برگ بید و سرخ گل اندر جهان که دید</p></div>
<div class="m2"><p>تیغ از زمرد و سپر از گوهر یمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر غنچه در فریب دل عندلیب نیست</p></div>
<div class="m2"><p>بهر چه زر ساو گرفتست در دهن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با شنبلید و نرگس تر نسبتی گرفت</p></div>
<div class="m2"><p>زلف سیاه دلبر و رخسار زرد من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن دلبری که عارض و زلف مسلسلش</p></div>
<div class="m2"><p>بشکست نرخ سنبل و بازار یاسمن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در حیرتم ز طلعت او تا چه خوانمش</p></div>
<div class="m2"><p>ماه شب چهارده یا شمع انجمن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ماه است اگر نه ماه بود خسته محاق</p></div>
<div class="m2"><p>شمع است اگر نه شمع بود بسته لگن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جز خط مشکبوی و رخ جانفزای او</p></div>
<div class="m2"><p>هرگز بنفشه دید کسی رسته بر سمن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جز قد خوش خرام و تن چون حریر او</p></div>
<div class="m2"><p>سرو روان که دید برش برگ نسترن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>میزیبدش که پای نهد بر دو چشم من</p></div>
<div class="m2"><p>زیرا که جویبار سزد سرو را وطن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زلفش بکافری دل زارم اسیر کرد</p></div>
<div class="m2"><p>وانگه فکند بی سببی در چه ذقن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سهلست اگر چو خود ذقنش در چهش فکند</p></div>
<div class="m2"><p>در چه توان شدن بامید چنان رسن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شهری اسیر فتنه و غوغای حسن او</p></div>
<div class="m2"><p>واو بر جناب خسرو آفاق مفتتن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>والا نظام دولت و ملت که ذات او</p></div>
<div class="m2"><p>نور مجسم است ز انوار ذوالمنن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن سروری که از حسد بوی خلق او</p></div>
<div class="m2"><p>بر تن قبا کند گل خوشبوی پیرهن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر خاک اگر ز فیض کفش قطره ئی چکد</p></div>
<div class="m2"><p>طوبی حسد برد بدل از سبزه دمن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گردون شد ازرقی و مه و مهر انوری</p></div>
<div class="m2"><p>بهر ثنا و مدحت آن سرور زمن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حاسد چو اوج جاه وی آورد در خیال</p></div>
<div class="m2"><p>از غم بسان چاه فرو شد بخویشتن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دشمن بروز معرکه از تیغش آن کشد</p></div>
<div class="m2"><p>کز طعنه شهاب کشد در شب اهرمن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای نابسوده اوج جلال تو دست وهم</p></div>
<div class="m2"><p>وی ناسپرده خاک جناب تو پای ظن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در رزم و بزم بر سر اعدا و اولیا</p></div>
<div class="m2"><p>چون ابر درفشانی و چون مهر تیغ زن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا عدل دین پناه تو ضبط جهان نهاد</p></div>
<div class="m2"><p>در یک کنام میچرد آهو و کرگدن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در جنب بارگاه تو کان نسر واقع است</p></div>
<div class="m2"><p>سیمرغ بی ثبات بود راست چون زغن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ابن یمین کمینه ثنا خوان جاه تست</p></div>
<div class="m2"><p>بادا ثنای او بقبول تو مقترن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پیوسته باد مرجع خلقان جناب تو</p></div>
<div class="m2"><p>در نفع و ضر و خیر و شر و شادی و حزن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دائم دل سیاه چو سنگ مخالفان</p></div>
<div class="m2"><p>باد از برای خنجر خونخوار تو مسن</p></div></div>