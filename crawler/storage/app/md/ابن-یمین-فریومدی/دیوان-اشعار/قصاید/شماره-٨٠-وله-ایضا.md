---
title: >-
    شمارهٔ ٨٠ - وله ایضاً
---
# شمارهٔ ٨٠ - وله ایضاً

<div class="b" id="bn1"><div class="m1"><p>دوش بادی مشک‌بو آمد به هنگام سحر</p></div>
<div class="m2"><p>گفتم ای خرم نسیم از خلد می‌آیی مگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کز عبیر تست عطر مجلس اصحاب دل</p></div>
<div class="m2"><p>وز غبار تست نور چشم ارباب نظر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت نی کز خلد زینسان خوش نفس نآید نسیم</p></div>
<div class="m2"><p>بر جناب سرور گردنکشان کردم گذر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صفدر مازندران افراسیاب گرد گیر</p></div>
<div class="m2"><p>آن بمردی در جهان چون رستم دستان سمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیر مقدم گفتمش چون از جنابت میرسید</p></div>
<div class="m2"><p>ای بتو شوقم چو الطاف تو بیش از حد و مر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نامه نامی از آنعالیجناب آورده بود</p></div>
<div class="m2"><p>روح را معنی آن در خور چو طوطی را شکر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نامه بود از راه معنی چون بدو در بنگری</p></div>
<div class="m2"><p>هست در جی پر لطایف هست در جی پر درر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بقعر بحر آن در غوص کردم بارها</p></div>
<div class="m2"><p>یافتم در کسوت وعظ اندر و چندین گهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مشتمل بر قسر و قهر اهل طغیان دیدمش</p></div>
<div class="m2"><p>صورت و معنیش را چون ضبط کردم سر بسر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حق همیداند که تا من روی دوران دیده ام</p></div>
<div class="m2"><p>نامدست از من بجز دین پروری کاردگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من نیم در بند آسایش تو هم دانسته ئی</p></div>
<div class="m2"><p>کز برای نصرت دین بسته ام دائم کمر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نصرت اسلام و قهر کفر از آنسان کرده ام</p></div>
<div class="m2"><p>کافرین گوید اگر بیند امام منتظر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صلح در بسته ام بگشاده ام در جنگ را</p></div>
<div class="m2"><p>ز آنک از کافر نیاید هیچ صلحی معتبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اعتماد جمله بر توفیق یزدانست و بس</p></div>
<div class="m2"><p>ز آنکه توفیق است سوی فتح و نصرت راهبر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از مدد و از عدد بسیار ناید هیچ کار</p></div>
<div class="m2"><p>گر معاون می نگردد کردگار دادگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بس که گردد لشکر بسیار از اندک منهزم</p></div>
<div class="m2"><p>در صف هیجا چو بخشد ایزد قادر ظفر</p></div></div>