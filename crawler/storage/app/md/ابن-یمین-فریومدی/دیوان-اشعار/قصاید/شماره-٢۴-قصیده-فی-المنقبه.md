---
title: >-
    شمارهٔ ٢۴ - قصیده فی المنقبه
---
# شمارهٔ ٢۴ - قصیده فی المنقبه

<div class="b" id="bn1"><div class="m1"><p>خرم دلی که مجمع سودای حیدرست</p></div>
<div class="m2"><p>فرخ سری که خاک کف پای حیدرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جائیکه جبرئیل بدانجا نمیرسد</p></div>
<div class="m2"><p>برتر هزار مرتبه ز آن جای حیدرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دعوت ملائکه بر خوان آرزو</p></div>
<div class="m2"><p>هر نعمتی که هست به آلای حیدرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خطیر معرفت سر کاینات</p></div>
<div class="m2"><p>یک قطره حقیر ز دریای حیدرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علمی که هست عالم افلاک را زبر</p></div>
<div class="m2"><p>عکسی ز نور خاطر دانای حیدرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس حال کاینات به علم الیقین ندید</p></div>
<div class="m2"><p>ور دید کار دیده بینای حیدرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل ار چه در ممالک هستی سرآمدست</p></div>
<div class="m2"><p>دیوانه وار واله و شیدای حیدرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمع جهانفروز که خوانندش آفتاب</p></div>
<div class="m2"><p>برقی ز تاب مشعله رأی حیدرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر ممکن است معجزه ئی از پس نبی</p></div>
<div class="m2"><p>الفاظ جانفزای دلارای حیدرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دانی که عرش چیست بر اهل معرفت</p></div>
<div class="m2"><p>اول قدم ز منبر والای حیدرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از صبغه الله ار بیقین آگهیت نیست</p></div>
<div class="m2"><p>هست اتفاق عقل که سیمای حیدرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز آنروی بر وحوش جهان شیر شد امیر</p></div>
<div class="m2"><p>کان هم یکی ز جمله اسمای حیدرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لطفی که در خزانه غیب است مد خر</p></div>
<div class="m2"><p>اظهار آن بسیرت زیبای حیدرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرزانگان عالم غیب آنچه داشتند</p></div>
<div class="m2"><p>از رازها نهان همه پیدای حیدرست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با جبرئیل هم ننهادند در میان</p></div>
<div class="m2"><p>سری که در صمیم سویدای حیدرست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر چند دارد ابن یمین جرم بیشمار</p></div>
<div class="m2"><p>اما از آن چه باک که مولای حیدرست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیشک بدینوسیله که دارم مقام من</p></div>
<div class="m2"><p>روز جزا بحضرت اعلای حیدرست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نندیشم از تزلزل اقدام کاعتصام</p></div>
<div class="m2"><p>من بنده را بحبل تولای حیدرست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فردا که اختیار دهندم که جای گیر</p></div>
<div class="m2"><p>گیرم بخلد جای که مأوای حیدرست</p></div></div>