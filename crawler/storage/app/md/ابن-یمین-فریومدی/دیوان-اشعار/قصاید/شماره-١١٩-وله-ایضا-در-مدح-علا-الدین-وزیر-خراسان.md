---
title: >-
    شمارهٔ ١١٩ - وله ایضاً در مدح علاء الدین وزیر خراسان
---
# شمارهٔ ١١٩ - وله ایضاً در مدح علاء الدین وزیر خراسان

<div class="b" id="bn1"><div class="m1"><p>زهی جمال تو خورشید آسمان کرم</p></div>
<div class="m2"><p>وجود پاک تو سر خیل دودمان کرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علاء دولت و ملت توئی که یافت خرد</p></div>
<div class="m2"><p>خجسته حضرت والات را مکان کرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دعای دولت تو ورد خویشتن کردی</p></div>
<div class="m2"><p>گر اقتدا بسخن داشتی زبان کرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بصد قران فلک اندر زمانه ننماید</p></div>
<div class="m2"><p>بسان همت تو گوهری ز کان کرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیافت پرورش از چشمه سار آب حیات</p></div>
<div class="m2"><p>براستی چو تو سروی ببوستان کرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار بار اگر خامه را زبان ببری</p></div>
<div class="m2"><p>نه ممکن است که باز استد از بیان کرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهی که نام کرم بر زبان من رفتی</p></div>
<div class="m2"><p>فلک بنام تو دادی مرا نشان کرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه واجبست که شد با کریم طبعی تو</p></div>
<div class="m2"><p>ببخت ابن یمین منقطع زمان کرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپهر سفله سیه کاسگی چو پیشه گرفت</p></div>
<div class="m2"><p>نداد بی جگرم لقمه ئی ز خوان کرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا ز گفته غیری لطیفه ئی یاد است</p></div>
<div class="m2"><p>که آیتیست فرو آمده بشأن کرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ببوی فضل و کرم خان و مان رها کردم</p></div>
<div class="m2"><p>که روی فضل سیه باد و خان و مان کرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه نیک باشد اگر پایمال دهر شود</p></div>
<div class="m2"><p>سری که پیش تو باشد بر آستان کرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همیشه تا ز کریمان بیادگار بود</p></div>
<div class="m2"><p>نوشته بر ورق دهر داستان کرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وجود پاک تو اندر زمانه باقی باد</p></div>
<div class="m2"><p>که از وجود تو دارد حیات جان کرم</p></div></div>