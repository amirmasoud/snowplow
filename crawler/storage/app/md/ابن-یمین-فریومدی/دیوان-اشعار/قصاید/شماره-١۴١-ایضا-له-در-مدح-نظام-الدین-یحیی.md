---
title: >-
    شمارهٔ ١۴١ - ایضاً له در مدح نظام الدین یحیی
---
# شمارهٔ ١۴١ - ایضاً له در مدح نظام الدین یحیی

<div class="b" id="bn1"><div class="m1"><p>باد میمون نهضت رایات شاه دین پناه</p></div>
<div class="m2"><p>آنکه بر خلقش بحق کردست ایزد پادشاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه دین پرور نظام دولت و ملت کز اوست</p></div>
<div class="m2"><p>سربلند و پای برجا در جهان دیهیم و گاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و آنکه شاه اختران دربندگی او کمر</p></div>
<div class="m2"><p>گر نبندد آسمان بر بایدش از سر کلاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرور گردنکشان کز بدو فطرت ثبت کرد</p></div>
<div class="m2"><p>خامه منشی گردون مدح او بر فرق ماه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر جهان پر فتنه و آشوب گردد ایمن است</p></div>
<div class="m2"><p>هر که او را شحنه انصافش آرد در پناه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با وجود او عدو را لاف شاهی کی رسد</p></div>
<div class="m2"><p>آفتاب از ذره بشناسد خرد بی اشتباه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناید از مردمم گیاه آنها که آید ز آدمی</p></div>
<div class="m2"><p>ور چه در صورت بود چون آدمی مردم گیاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر کند دعوی که ملک اوست ملک خافقین</p></div>
<div class="m2"><p>مدعا ثابت بود آنرا که عدل آمد گواه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در شگفتم تا چرا آئینه مه زنگ یافت</p></div>
<div class="m2"><p>چون بگردون بر نشد در عهد عدلش هیچ آه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر رسد بوئی ز حزم او بکاه سر سبک</p></div>
<div class="m2"><p>ورکند عزمش بسوی کوه پا برجا نگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از گرانباری حزمش کاهرا بینی چو کوه</p></div>
<div class="m2"><p>در سبکساری عزمش کوه را یابی چو کاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از نسیم و از سموم لطف و عنفش زهر و نوش</p></div>
<div class="m2"><p>آن ولی را جانفزای و وین عدو را عمر کاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر کرا در مصر عالم کرد لطف او عزیز</p></div>
<div class="m2"><p>روی چون یوسف نهاد از چاه خواری سوی جاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شهریارا بر زمین هر فتنه کآید ز آسمان</p></div>
<div class="m2"><p>ورچه باشد بهر غیری از رعیت تا سپاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون بجمع ساکنان ربع مسکون بگذرد</p></div>
<div class="m2"><p>خانه ابن یمین جوید نخست از گرد راه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آسمان چون دید عدلت را که با ظالم چه کرد</p></div>
<div class="m2"><p>باری آنهم شمه ئی کز عقل بودش انتباه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با خرد گفتم خلاصم زو که یارد داد گفت</p></div>
<div class="m2"><p>آفتاب اوج رفعت سایه لطف اله</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خسرو عادل نظام ملک و دین کز بهر فخر</p></div>
<div class="m2"><p>بر زمین سایند پیش او سرافرزان جباه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طاعت مقبول نبود جز دعای دولتش</p></div>
<div class="m2"><p>وانچ غیر این بود نزد خرد باشد گناه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>میکنم بهر دعا تضمین دو بیت خویشتن</p></div>
<div class="m2"><p>ز آنکه افتد میل تضمین شاعرا نرا گاه گاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عرضه میدارم کنون بر رأی ملک آرای تو</p></div>
<div class="m2"><p>استماعش کن بلطف شاملت زین نیکخواه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا بهنگام کتابت هیچ کاتب در جهان</p></div>
<div class="m2"><p>از دوات و از قلم خالی ندارد دستگاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر که سر بر خط فرمانت ندارد چون قلم</p></div>
<div class="m2"><p>چون دواتش چشم بادا چشمه آب سیاه</p></div></div>