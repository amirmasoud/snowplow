---
title: >-
    شمارهٔ ١۵٠ - وله ایضاً در مدح شهاب الدین زنگی
---
# شمارهٔ ١۵٠ - وله ایضاً در مدح شهاب الدین زنگی

<div class="b" id="bn1"><div class="m1"><p>بهارست ای پسر در ده ز بهر رفع دلتنگی</p></div>
<div class="m2"><p>شرابی چون گل و لاله بخوشبوئی و خوشرنگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگه کن نقش بندی طبیعت را که در بستان</p></div>
<div class="m2"><p>چنان بر آب میبندد هزاران نقش ارژنگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان شد خرم و خندان کنون آمد زمان آن</p></div>
<div class="m2"><p>که بهره مطربی آید ز گردون زهره چنگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببزم خسرو اعظم خدیو خطه عالم</p></div>
<div class="m2"><p>چراغ دوده آدم شهاب ملک و دین زنگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عدو بندی که روز کین اگر پیشش نهنگ آید</p></div>
<div class="m2"><p>ز بهر باز پس گشتن کند رفتار خرچنگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کلاه حکم تا بر سر نهاد از یمن عدل او</p></div>
<div class="m2"><p>بجز چین قبا کس را نبینی چهره آژنگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپهدار صف پنجم که دارد راه سرداری</p></div>
<div class="m2"><p>کند بر درگه جاهش ز بهر نام سرهنگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>براق عزم او را برق اگر هم تک شود روزی</p></div>
<div class="m2"><p>بصد حیلت بر هواری برد با او برون لنگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعادت مسند جاهش برفعت برد بر جائی</p></div>
<div class="m2"><p>که نتواند رسید آنجا خیال مردم بنگی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مه و برجیس گردون را گر از وی رخصتی باشد</p></div>
<div class="m2"><p>کند از بهر شهبازش هم آن طبلی هم این زنگی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برسم نقل اگر خواهد فلک بر فرق سر آرد</p></div>
<div class="m2"><p>ببزم او ز پروین خوشه انگور آونگی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دو توسن بود دوران را و شد آن هر دو رام او</p></div>
<div class="m2"><p>یکی زان اشهب رومی دگر یک ادهم زنگی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهر آلات زین میجست بهر مرکب خاصش</p></div>
<div class="m2"><p>جناقی کرد خورشید و مجره میکند تنگی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طبیب حاذق لطفش تواند برد اگر خواهد</p></div>
<div class="m2"><p>ز نرگس علت کوری ز سوسن آفت گنگی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو شیر شرزه قهرش گشاید پنجه روز کین</p></div>
<div class="m2"><p>کند گر خواهد و گر نی پلنگ تند خورنگی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر گیرد بکف تیغی عدوش از برق رخشنده</p></div>
<div class="m2"><p>کند بر صفحه تیغش ز بخت بد گهر زنگی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سرافرازا تو آن شاهی که کوه پای بر جا را</p></div>
<div class="m2"><p>بجنب حلم تو باشد بسان کاه بی سنگی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سلیمان وش مسلم شد جهانت ز آنک چون آصف</p></div>
<div class="m2"><p>تمامت رأی و تدبیری سراسر عقل و فرهنگی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نسیم گلشن خلقت مشام شیر اگر خواهد</p></div>
<div class="m2"><p>شود خوشبویتر کامش ز ناف آهوی تنگی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فلک خواهد که در رفعت زند با جاه تو پهلو</p></div>
<div class="m2"><p>ولیکن دیدمش با او ندارد حد هم تنگی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر خورشید رأی تو کشد بر کوه تیغ کین</p></div>
<div class="m2"><p>رخ لعلش درون کان شود از بیم نارنگی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کشد آه از دل خصمت سوی گردون گردان سر</p></div>
<div class="m2"><p>چو دیدش کلبه ئی موحش ز تاریکی و از تنگی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ببزم و رزمت ار بیند خرد گوید توئی اکنون</p></div>
<div class="m2"><p>ببخشش حاتم طائی بکوشش رستم جنگی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر افتد سایه خورشید رایت برسها روزی</p></div>
<div class="m2"><p>عجب نبود اگر دایم کند زان پس شباهنگی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز بیم شحنه عدلت خرد را بس عجب ناید</p></div>
<div class="m2"><p>که گیرد زهره رعنا بترک شوخی و شنگی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کند ابن یمین کوته سخن زین پس بپیش تو</p></div>
<div class="m2"><p>مبادا کت صداع آید ازین گفتار آهنگی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همیشه تا بود اورنگ شاه اختران گردون</p></div>
<div class="m2"><p>ترا شاهی مسلم باد کاندر خورد اورنگی</p></div></div>