---
title: >-
    شمارهٔ ١٣٠ - ایضاً له در مدح علاءالدین محمد
---
# شمارهٔ ١٣٠ - ایضاً له در مدح علاءالدین محمد

<div class="b" id="bn1"><div class="m1"><p>مرا که هست زبان تیغ آبدار سخن</p></div>
<div class="m2"><p>گهر نما شد ازو در شاهوار سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رها نمیکند ایام ورنه بگشایم</p></div>
<div class="m2"><p>بدستکاری فکرت گره ز کار سخن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مبارزان سخن چون صف جدال کنند</p></div>
<div class="m2"><p>نخواندم خرد الا که شهسوار سخن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم که خاطر من نو عروس معنی را</p></div>
<div class="m2"><p>بگاه جلوه دهد زینت از نگار سخن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمانه دست تعدی گشاد تا ز حسد</p></div>
<div class="m2"><p>کند بر اهل هنر بسته رهگذار سخن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کراست زهره کزین پس بکارخانه فضل</p></div>
<div class="m2"><p>طراز بر کشد از شعر بر شعار سخن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر نه تربیت خسرو زمان باشد</p></div>
<div class="m2"><p>فرو شود بزمین آب خوشگوار سخن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپهر حشمت و رفعت علاء دولت و دین</p></div>
<div class="m2"><p>که گرد مرکز مدحش بود مدار سخن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محمد بن محمد که در ممالک فضل</p></div>
<div class="m2"><p>ز فر مدحت او بینم اشتهار سخن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخن که آن نه صفات کمال او باشد</p></div>
<div class="m2"><p>سخنورانش نیارند در شمار سخن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بکارگاه طبیعت درون مهندس فکر</p></div>
<div class="m2"><p>ببافت کسوت مدحش بپود و تار سخن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو نای خامه مشکین زبانش نیشکری</p></div>
<div class="m2"><p>نرست بر همه اطراف جویبار سخن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهر فضل شود پر کواکب دری</p></div>
<div class="m2"><p>گهی کز آتش طبع افکند شرار سخن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنفس نامیه گر بوی فضل او برسد</p></div>
<div class="m2"><p>زبان سوسن ازو یابد اقتدار سخن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زهی رفیع محلی که نفس ناطقه را</p></div>
<div class="m2"><p>گلی چو مدح تو نشکفت در بهار سخن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>توئیکه زرگر فطرت زد است سکه مدح</p></div>
<div class="m2"><p>بنام نیک تو بر زر یا عیار سخن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدرگه تو که بازار گوهر هنر است</p></div>
<div class="m2"><p>گشاد قافله سالار فضل بار سخن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کنون چو کلک تو معمار خطه هنر است</p></div>
<div class="m2"><p>خراب می نشود بعد ازین دیار سخن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو کلک تیز زبانت ادا کند سخنی</p></div>
<div class="m2"><p>سزد که ناطقه جانها کند نثار سخن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گهی که موج زند بحر خاطر تو شود</p></div>
<div class="m2"><p>کنار فضل پر از در شاهوار سخن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز ابر دست تو بینم بخشگسال کرم</p></div>
<div class="m2"><p>که میرود بقرار آب چشمه سار سخن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خدایگانا ابن یمین چو مادح تست</p></div>
<div class="m2"><p>بیمن دولت تو دارد آن یسار سخن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که اهل فضا بدین شعر معترف گردند</p></div>
<div class="m2"><p>که نیست همچو وی امروز کامکار سخن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همیشه تا ز لطافت عروس معنی را</p></div>
<div class="m2"><p>بگاه جلوه نشانند در کنار سخن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عروس خوب رخ مدح در کنار تو باد</p></div>
<div class="m2"><p>که در جهان چو توئی نیست خواستار سخن</p></div></div>