---
title: >-
    شمارهٔ ۵٧ - وله ایضاً
---
# شمارهٔ ۵٧ - وله ایضاً

<div class="b" id="bn1"><div class="m1"><p>شاد آنک عیش برطرف بوستان کند</p></div>
<div class="m2"><p>وین موسم بهار بفصل خزان کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فصل بهار موسم گلها و لاله هاست</p></div>
<div class="m2"><p>فصل خزان حقیقت آنرا بیان کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اطفال باغ فصل خزان گر شوند پیر</p></div>
<div class="m2"><p>باد بهار باز ز سرشان جوان کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرمایه نشاط درین هر دو موسم است</p></div>
<div class="m2"><p>شاد آنک او موافقت هر دوشان کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رنگ بهار رنگرزان را مدد دهد</p></div>
<div class="m2"><p>برگ خزان معاونت زرگران کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون کار روزگار بیک حال کس ندید</p></div>
<div class="m2"><p>غافل کسی که فکرت دور زمان کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داند که بس بهار و خزان آید و رود</p></div>
<div class="m2"><p>کایام نام او ز جهان بی نشان کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شادی و غم چو بر گذرست آن صوابتر</p></div>
<div class="m2"><p>کآزاده شاد باشد و از غم کران کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راحست آنکه راحت روح از نسیم اوست</p></div>
<div class="m2"><p>خون در عروق خلق روان چون روان کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنگ است آنکه فطرت گردون نورد اوست</p></div>
<div class="m2"><p>بر خاطرت سرایر انجم عیان کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنگ و شراب هر دو بهم میخورند از آنک</p></div>
<div class="m2"><p>تا بنگ لوت خواهد و می هضم آن کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرکس که سود خویش یکی زین دورا شناخت</p></div>
<div class="m2"><p>سرمایه حیات ز سودا زیان کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای دل حدیث ابن یمین محض حکمتست</p></div>
<div class="m2"><p>تحقیق آن شنو که برمز آن بیان کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیتی که بر تخلص شعرم مقدم است</p></div>
<div class="m2"><p>آنرا بیاد گیرد وزان حرز جان کند</p></div></div>