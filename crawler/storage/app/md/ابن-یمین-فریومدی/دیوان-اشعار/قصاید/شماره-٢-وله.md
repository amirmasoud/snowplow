---
title: >-
    شمارهٔ ٢ - وله
---
# شمارهٔ ٢ - وله

<div class="b" id="bn1"><div class="m1"><p>ایدل ار خواهی گذر برگلشن دارالبقا</p></div>
<div class="m2"><p>جهد کن کز پای خود بیرون کنی خار هوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور نمیخواهی که پای از راه حق یکسو نهی</p></div>
<div class="m2"><p>دست زن در عروه و ثقای شرع مصطفی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راه شرع مصطفی از مرتضی جوزانکه نیست</p></div>
<div class="m2"><p>شهر علم مصطفی را در بغیر از مرتضی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرتضی را دان ولی اهل ایمان تا ابد</p></div>
<div class="m2"><p>چون ز دیوان ابد دارد مثال انما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیر او را کس نزیبد از سلونی دم زدن</p></div>
<div class="m2"><p>زانکه او داناست ما فوق السموات العلا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلعت با زیب و زین انت منی کس نیافت</p></div>
<div class="m2"><p>از نبی الا علی کو داشت فر انبیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سخا بود از دل و دستش خجل دریا و کوه</p></div>
<div class="m2"><p>این سخن دارد مصدق هر که خواند هل اتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بعد ازو در راه دین گر پیشوا خواهی گرفت</p></div>
<div class="m2"><p>بهتر از اولاد معصومش نیابی پیشوا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کیستند اولاد او اول حسن وانگه حسین</p></div>
<div class="m2"><p>آنکه ایشانرا نبی فرمود امام و مقتدا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنده این هر دو مخدومیم در دیوان شرع</p></div>
<div class="m2"><p>میکنم ثابت بحکم مصطفی این مدعا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از نبی من کنت مولاه چو تشریف علیست</p></div>
<div class="m2"><p>از طریق ارثشان بس بندگان باشیم ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بعد از ایشان مقتدی سجاد وانگه باقر است</p></div>
<div class="m2"><p>چون گذشتی جعفر و موسی و سبط او رضا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس تقی آنگه نقی آنگه امام عسگری</p></div>
<div class="m2"><p>بعد ازان مهدی کزو گیرد جهان نور و صفا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کردگارا جان پاک هر یکی زین جمع را</p></div>
<div class="m2"><p>از کرم در صدر فردوس برین ده متکا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخشش ایدل زین بزرگان چونکه هر یک بوده اند</p></div>
<div class="m2"><p>در دریای فتوت گوهر کان سخا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیروی کن گر نجات مخلصانت آرزوست</p></div>
<div class="m2"><p>هر که را با خاندان عصمت آمد انتما</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در طریق دین بهر کس اقتدا فرهنگ نیست</p></div>
<div class="m2"><p>گر کنی باری بمعصومان کن ایدل اقتدا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر چه من کابن یمینم کرده ام عصیان بسی</p></div>
<div class="m2"><p>لیک میدارم توقع زانکه هستم بیریا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دوستدار خاندان مصطفی و مرتضی</p></div>
<div class="m2"><p>کایزد از لطف و کرم بخشد بدان پاکان مرا</p></div></div>