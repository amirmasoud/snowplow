---
title: >-
    شمارهٔ ١٣۴ - وله ایضاً در مدح علاءالدین حسین
---
# شمارهٔ ١٣۴ - وله ایضاً در مدح علاءالدین حسین

<div class="b" id="bn1"><div class="m1"><p>هزار شکر و سپاسم ز خالق ثقلین</p></div>
<div class="m2"><p>که باز کرد ز لطف خودم قریرالعین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنور طلعت میمون قدوه النقبا</p></div>
<div class="m2"><p>ستوده سرور اولاد تارک الثقلین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپهر مهر فتوت جهان جان کرم</p></div>
<div class="m2"><p>علاء دولت و دین افضل زمانه حسین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزرگوار امیری که بر قضا همه وقت</p></div>
<div class="m2"><p>ادای واجب حکمش بود فریضه چو دین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر کشد بمثل تیغ بر سر کهسار</p></div>
<div class="m2"><p>به بیقراری زیبق شود زهم لجین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بچنگ و تیغ گه جنگ میدهد خبرم</p></div>
<div class="m2"><p>ز ذوالفقار و کف مرتضی و حرب حسین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدفع دشمن ازو دوست گر مدد طلبد</p></div>
<div class="m2"><p>جهد ز جای چو برق و نترسد از کم و این</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جواز بر رخ ماه ار بحکم او نبود</p></div>
<div class="m2"><p>نیارد آنک ز شرطین ره برد ببطین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و گر ز پرتو رأیش سها مدد خواهد</p></div>
<div class="m2"><p>شود بزیر شعاعش نهفته پیکر عین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود ز بیم کف راد گوهر افشانش</p></div>
<div class="m2"><p>درون قلعه خارا همیشه مسکن عین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سماع دشمن او درگه طرب نبود</p></div>
<div class="m2"><p>بغیر ناله زیر و بم غراب البین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عدوش اگر ز در بخت امید دل طلبد</p></div>
<div class="m2"><p>بود ز ساحت او رجعتش بخف حنین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر ز روی نسب سیدی مشارک اوست</p></div>
<div class="m2"><p>فضیلت حسبش بس بود تفاوت بین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بصورت ارچه که مانند عین و غین بهم</p></div>
<div class="m2"><p>ولی بنهصد و سی زائدست غین ز عین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هنر پناه امیرا ندارد ابن یمین</p></div>
<div class="m2"><p>بغیر نشر ثنای تو کار تا گه حین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عروس مدح تو از حجله طبیعت من</p></div>
<div class="m2"><p>بگاه جلوه معری شود ز کسوت شین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ور از قبول تو خود زیوری بر او بندند</p></div>
<div class="m2"><p>شکست حور دهد از بس که زیب یابد وزین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ضمیر پاک تو هم بیشک آگهست که من</p></div>
<div class="m2"><p>درین قضیه نیم سالک مسالک هین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همیشه تا بود از عین در زمانه اثر</p></div>
<div class="m2"><p>ز دشمنت نه اثر باد در زمانه نه عین</p></div></div>