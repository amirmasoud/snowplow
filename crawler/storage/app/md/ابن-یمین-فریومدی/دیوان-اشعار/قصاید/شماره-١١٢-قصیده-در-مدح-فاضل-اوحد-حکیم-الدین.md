---
title: >-
    شمارهٔ ١١٢ - قصیده در مدح فاضل اوحد حکیم الدین
---
# شمارهٔ ١١٢ - قصیده در مدح فاضل اوحد حکیم الدین

<div class="b" id="bn1"><div class="m1"><p>این منم یا رب که در دل شادمانی یافتم</p></div>
<div class="m2"><p>و آنچه جستم از قضای آسمانی یافتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با من این الطاف چرخ کینه ورزان میکند</p></div>
<div class="m2"><p>کز خدیو ملک دانش مهربانی یافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اوحد الدنیا حکیم الدین که نپسندد خرد</p></div>
<div class="m2"><p>گر فلک گوید که در دنیاش ثانی یافتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه چون انشای عذب او عطارد دید گفت</p></div>
<div class="m2"><p>گاه پیری لذت عمر جوانی یافتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهربانی با منش این بود کز ارشاد او</p></div>
<div class="m2"><p>بر اقالیم فضایل کامرانی یافتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوی دار الکتب خود راهیم داد از مکرمت</p></div>
<div class="m2"><p>تا درو درجی پر از در معانی یافتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاید ار چون خضر مانم زنده آب حیات</p></div>
<div class="m2"><p>ساغری تا لب پر آب زندگانی یافتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما و عشرت بعد از این بر رغم دشمن تا ابد</p></div>
<div class="m2"><p>کز کف ساقی لطفش دوستکانی یافتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی توانم گفت شکر آنکه از کنج نیاز</p></div>
<div class="m2"><p>ناگهان ره سوی گنج شایگانی یافتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از کتابت بگذر ای ابن یمین تصریح کن</p></div>
<div class="m2"><p>کو ز تصویرات جان افزاش مانی یافتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جاودان بادا مفید و اهل فیضش مستفید</p></div>
<div class="m2"><p>کز بیان او حیات جاودانی یافتم</p></div></div>