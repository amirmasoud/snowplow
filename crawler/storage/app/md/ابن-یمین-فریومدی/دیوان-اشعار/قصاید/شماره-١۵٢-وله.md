---
title: >-
    شمارهٔ ١۵٢ - وله
---
# شمارهٔ ١۵٢ - وله

<div class="b" id="bn1"><div class="m1"><p>تا زمان باشد کسی را در زمان سروری</p></div>
<div class="m2"><p>از علاءالدین و الدنیا نزیبد برتری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن عطا پاش خطا پوشی که از رأی صواب</p></div>
<div class="m2"><p>در ممالک شد مسلم بر سران او را سری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن وزیر شه نشان کالحق بجای خود بود</p></div>
<div class="m2"><p>گر کند در مدح او محمود کار عنصری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشتری از طالعش گوئی سعادت کسب کرد</p></div>
<div class="m2"><p>کین چنین مشهور عالم شد به نیکو اختری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نور رأیش بشکند بازار تاب آفتاب</p></div>
<div class="m2"><p>معجز موسی برد رونق ز سحر سامری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاتم طائی و رستم را بگاه بزم و رزم</p></div>
<div class="m2"><p>ایدل ار با او مشابه در تصور آوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ببینی بخشش و کوشش ازو آن هر دو را</p></div>
<div class="m2"><p>از جوانمردان ندانی وز دلیران نشمری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشتی دریای امر و نهی و حل و عقد را</p></div>
<div class="m2"><p>بادبانی کرده عزم و کرده حزمش لنگری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اوج کیوانرا نگویم آستان قدر اوست</p></div>
<div class="m2"><p>کی بود با ذره تاب آفتاب خاوری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خدمت در گاه او کردن نشان مقبلیست</p></div>
<div class="m2"><p>روی ازو بر تافتن باشد نشان مدبری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خسروا ابن یمین از بندگان خاص تست</p></div>
<div class="m2"><p>مشتکی می بینمش از جور چرخ چنبری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چرخ چوگانی چو گویش مضطرب دارد مدام</p></div>
<div class="m2"><p>صد خلل در کارش آید گر بحالش ننگری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کار او گوهر فروشی و بدین بازار نیست</p></div>
<div class="m2"><p>هیچکس اینجنس را غیر از عطارد مشتری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حاصلش قلبی سیاهست ار چه باشد روز و شب</p></div>
<div class="m2"><p>چشم او در سیم پالائی و رخ در زرگری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر سر بازار دانش چون نهدد کان چو هست</p></div>
<div class="m2"><p>رونق یلخی فروشان بیشتر از جوهری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در خراسان بودن عیسی دم زینسان که اوست</p></div>
<div class="m2"><p>اینچنین بی رونقی از بی خری یا از خری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دارد از لطف تو نزد شاه امید تربیت</p></div>
<div class="m2"><p>تربیت کن چون بحمدالله بدینها قادری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من چه گویم تربیت چون کن چو داند رأی تو</p></div>
<div class="m2"><p>بهتر از شاهان عالم رسم چاکر پروری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا نسیم لطف یزدان بشکفاند هر شبی</p></div>
<div class="m2"><p>اندرین نیلوفری گلزار گلهای طری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بزمت از گل چیده ها بادا بنزهت آنچنانک</p></div>
<div class="m2"><p>زو خجالتها برد این گلشن نیلوفری</p></div></div>