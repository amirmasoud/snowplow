---
title: >-
    شمارهٔ ١۶٢ - وله
---
# شمارهٔ ١۶٢ - وله

<div class="b" id="bn1"><div class="m1"><p>از من ای باد صبا لطف بود گر سحری</p></div>
<div class="m2"><p>ببر خسرو آفاق رسانی خبری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدوه اهل کرم یونس طاهر نسب آنک</p></div>
<div class="m2"><p>بحر و کان را نبود با دل و دستش خطری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن عطا پاش که همتاش بصحرای وجود</p></div>
<div class="m2"><p>نارد از کتم عدم مادر ارکان پسری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و آنکه طوطی طبیعت نشود نطق سرای</p></div>
<div class="m2"><p>تا ز شکرش نبود در دهن او شکری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بدان حضرت با رفعت میمون برسی</p></div>
<div class="m2"><p>عرضه دار از من و از حال من آنجا قدری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که مرا هست یقین آنکه سوی ابن یمین</p></div>
<div class="m2"><p>بودت از عین عنایت گه و بیگه نظری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه خطا رفت که امسال نبینم چون پار</p></div>
<div class="m2"><p>نکند بر در او موکب لطفت گذری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راستی را نپسندد خرد از همچو منی</p></div>
<div class="m2"><p>با وجود چو توئی جستن جود از دگری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفتاب کرمی سایه ازو باز مگیر</p></div>
<div class="m2"><p>تا بدانند که کردست عنایت اثری</p></div></div>