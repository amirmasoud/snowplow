---
title: >-
    شمارهٔ ١۴۴ - وله ایضاً در مدح تاج الدین شیخ علی مؤید
---
# شمارهٔ ١۴۴ - وله ایضاً در مدح تاج الدین شیخ علی مؤید

<div class="b" id="bn1"><div class="m1"><p>فرخنده باد مقدم شاه جهانپناه</p></div>
<div class="m2"><p>خورشید ملک شیخ علی سایه اله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهی که باشد از زبر تخت خسروی</p></div>
<div class="m2"><p>تابان رخش چنانکه ز گردون دو هفته ماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از فر فرق او که در آفاق سرورست</p></div>
<div class="m2"><p>بر آفتاب سایه کند گوشه کلاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایخسروی که صومعه داران قدس را</p></div>
<div class="m2"><p>اینست ورد و بس ز پی شام و صبحگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لطف حق شناس که بر رغم دشمنان</p></div>
<div class="m2"><p>با دوستان بمجلس عشرت نشست شاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یعنی که ماه برج سعادت بکام دل</p></div>
<div class="m2"><p>با آفتاب گشته مقارن به تختگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این آفتاب تا بابد بی زوال باد</p></div>
<div class="m2"><p>داراد حق ز کاستن آن ماه را نگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دور از جناب جاه تو احداث روزگار</p></div>
<div class="m2"><p>کرد اعتدال طبع مرا منحرف ز راه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نوش شرابخانه لطف تو چون رسید</p></div>
<div class="m2"><p>در کام جان من ز کف بخت نیکخواه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از فر شاه و فرخی نوش انجمن</p></div>
<div class="m2"><p>یابم شفا هر آینه زین رنج عمر کاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاها کمینه چاکرت ابن یمین منم</p></div>
<div class="m2"><p>آنم که کرده ئی بعنایت بمن نگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و آنگاه مدح شاه کنم در جهان روان</p></div>
<div class="m2"><p>شعری ربوده گوی ز شعری بقدر و جاه</p></div></div>