---
title: >-
    شمارهٔ ١۵٣ - ایضاً له
---
# شمارهٔ ١۵٣ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>چند گاهی زیر طاق گنبد نیلوفری</p></div>
<div class="m2"><p>خار غم را جفت بودم همچو گلبرگ طری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خامه منشی دیوان سعادت مدتی</p></div>
<div class="m2"><p>در مدد کاری من میکرد سعی سرسری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زرگر محنت شبا روزی ز چشم و رخ مرا</p></div>
<div class="m2"><p>گاه بودی سیم پالاگاه کردی زرگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز اختلاف دور گردون طالع بد خواه من</p></div>
<div class="m2"><p>منقطع میکرد امید از دولت نیک اختری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وقت صید مرغ امنیت همای همتم</p></div>
<div class="m2"><p>گوشه گیری بود چون زاغ کمان از بی پری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه بود اینها و صد چندین ولیکن باک نیست</p></div>
<div class="m2"><p>چون ز لطف ایزدی بر رغم چرخ چنبری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سرم یکبار دیگر سایه رحمت فکند</p></div>
<div class="m2"><p>مظهر نور الهی آفتاب خاوری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اختر برج سعادت آنکه زیبد از شرف</p></div>
<div class="m2"><p>بر مقیمان زمین چون آسمانش سروری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بحر معنی آنکه سلک در الفاطش کند</p></div>
<div class="m2"><p>نو عروس فضل را در گوش و گردن زیوری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قطب اسلام آنکه جن و انس را تسخیر کرد</p></div>
<div class="m2"><p>چون سلیمان و آنگهی بیمنت انگشتری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آستان او که عز پایبوسش یافتست</p></div>
<div class="m2"><p>دارد از ایوان کیوان در جلالت برتری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنکه خاک پای گردون سایش از روی شرف</p></div>
<div class="m2"><p>شاه انجم را کند بر تارک و سر افسری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاک پای اوست آن کحل الجواهر کافتاب</p></div>
<div class="m2"><p>بهر نور چشم خود باشد بجانش مشتری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عقل کل در جستجوی حق بیفتادی ز پا</p></div>
<div class="m2"><p>گر نکردی رأی ملک آرایش او را رهبری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ور نبودی اهل دانش را مربی لطف او</p></div>
<div class="m2"><p>معجر ناهید گشتی طلیسان مشتری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا مدیح جاه تو گویند ماه و آفتاب</p></div>
<div class="m2"><p>خویشتن را می نمایند ارزقی و انوری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دین پناها در مدیحت خاطر ابن یمین</p></div>
<div class="m2"><p>میکند در کارگاه شاعری صد ساحری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر بخاک سامری ز ینشعر بوئی بگذرد</p></div>
<div class="m2"><p>ناله های لامساس آید ز جان سامری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا ز بهر نزهت نظارگان اهل دل</p></div>
<div class="m2"><p>غنچه ها خندان شود در گلشن نیلوفری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دوحه اقبال تو اهل هنر را از کرم</p></div>
<div class="m2"><p>در پناه سایه عالی او میپروری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از نسیم لطف یزدانی و آب زندگی</p></div>
<div class="m2"><p>شاخ او را باد سرسبزی و بیخش را تری</p></div></div>