---
title: >-
    شمارهٔ ٩٠ - وله ایضاً قصیده در مدح تاج  الدین علی سربداری
---
# شمارهٔ ٩٠ - وله ایضاً قصیده در مدح تاج  الدین علی سربداری

<div class="b" id="bn1"><div class="m1"><p>منت ایزد را که بخت نوجوان پیرانه سر</p></div>
<div class="m2"><p>رهنمایم گشت سوی شهریار بحر و بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرور و سردار شرق و غرب تاج ملک و دین</p></div>
<div class="m2"><p>داور و دارای گیتی خسرو جمشید فر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنک می بینند خلقان جهان او را کنون</p></div>
<div class="m2"><p>آنچه زین پس دید خواهند از امام منتظر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و آنکه روز کین بهیبت گر بگردون بنگرد</p></div>
<div class="m2"><p>اختر از گردون جهد بیرون چو از آتش شرر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست چون بهر کار عالمی بر سر زند</p></div>
<div class="m2"><p>آن زمان بینی بیکجا جمع کشته بحر و بر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نا پسند آید مرا تشبیه دست او به ابر</p></div>
<div class="m2"><p>هیچکس گوید که ماند بحر اخضر با شمر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاه بخشش قطره آبست فیض ابر و بس</p></div>
<div class="m2"><p>دست او وقت سخا هم زر فشاند هم گهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که چون انگشتری بر دست رادش بوسه داد</p></div>
<div class="m2"><p>چون نگینش غرقه بینی در میان سیم و زر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مادر ارکان سترون گر شود اکنون رواست</p></div>
<div class="m2"><p>چون محالست اینکه زاید مثل او دیگر پسر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آسمانش را زمین دانی که کی بیند نظیر</p></div>
<div class="m2"><p>آنزمان کآرد بچشم احول او را در نظر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاهباز فتنه چون زاغ کمان شد گوشه گیر</p></div>
<div class="m2"><p>چون عقاب تیر عدلش بر جهان گسترد پر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شد جهان از بیم او از راهزن خالی چنانک</p></div>
<div class="m2"><p>نرگس ایمن میرود با طشت زر بر فرق سر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر چه فرماید برد فرمان قضا از بهر آنک</p></div>
<div class="m2"><p>کفر باشد نقض آن داند قضا خود این قدر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حاسدش در مدت عمر از سحر تا وقت شام</p></div>
<div class="m2"><p>با دلی پر تیر و آهی سرد چون شام و سحر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دشمنش را بخت بد تجرید فرمود آنچنانک</p></div>
<div class="m2"><p>کش نه بینم جز لب و جز دیده هیچ از خشک و تر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر غبار خاک پایش سرمه کردی آسمان</p></div>
<div class="m2"><p>می نگشتی ناخنه از ماه او را در بصر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ور ز دیوانش نبودی ماه را بر رخ جواز</p></div>
<div class="m2"><p>کی توانستی که بودی گاه و بیگه در سفر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آفتاب اجرای نور از رأی او گر یافتی</p></div>
<div class="m2"><p>می نگشتی چون گدایان روز تا شب در بدر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جز جناب او حوالتگه نمی بیند خرد</p></div>
<div class="m2"><p>اهل عالم را ز بهر کسب خیر و دفع شر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خسرو سیارگان از بندگان رأی اوست</p></div>
<div class="m2"><p>بنده وار از بهر آن می بندد از جوزا کمر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر بمهمانی گردون سر در آرد قدر او</p></div>
<div class="m2"><p>آرد از ماه و خورش قرصی دو حالی ما حضر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر فرستادی بگردون رأی او یکذره را</p></div>
<div class="m2"><p>از خسوف و از کسوف ایمن شدندی ماه و خور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر دلش خواهد بیکدم رنگ بزداید زدود</p></div>
<div class="m2"><p>صیقل رأی منیر او ز مرآت قمر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آفتاب از تاب رایش در تب و لرز اوفتد</p></div>
<div class="m2"><p>در چنین رنجی شبا روزی بود بیخواب و خور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خاطرم چون فکرت معنی و لفظ او کند</p></div>
<div class="m2"><p>در ضمیرم نقش نبدد صورت شمع و شکر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر کسی گوید که در ذاتش هنر اینست و آن</p></div>
<div class="m2"><p>عیب آنکس باشد این او هست سر تا پا هنر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شهریارا کامکارا گر اجازت میدهی</p></div>
<div class="m2"><p>عرضه دارم در جنابت یک حدیث مختصر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دور دور تست آمد گاه آن کابن یمین</p></div>
<div class="m2"><p>نگذراند عمر خود زین بیش در بوک و مگر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یا ز دیوان کرم اطلاق کن روزی من</p></div>
<div class="m2"><p>یا نشانم ده جز این گر هست دیوانی دگر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آسمان حکم ترا بادا مسخر چون زمین</p></div>
<div class="m2"><p>تا زمین باشد بزیر و آسمان باشد زبر</p></div></div>