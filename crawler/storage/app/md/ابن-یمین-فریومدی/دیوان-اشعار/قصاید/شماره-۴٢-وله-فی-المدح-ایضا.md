---
title: >-
    شمارهٔ ۴٢ - وله فی المدح ایضاً
---
# شمارهٔ ۴٢ - وله فی المدح ایضاً

<div class="b" id="bn1"><div class="m1"><p>این سعادت بین که باز اندر زمان آمد پدید</p></div>
<div class="m2"><p>وین کرامت بین که ناگه در جهان آمد پدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد فروزان از سپهر سروری ماه دگر</p></div>
<div class="m2"><p>وین جهان پیر را بخت جوان آمد پدید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دریای فتوت از صدف بنمود روی</p></div>
<div class="m2"><p>گوهر کان کرم ناگه زکان آمد پدید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بر سرو سهی شاخی بگردون سر کشید</p></div>
<div class="m2"><p>میوه ئی ز آن شاخ سرکش ناگهان آمد پدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسرو خسرو نشانرا دان تو آنسرو روان</p></div>
<div class="m2"><p>صاین است آن شاخ کز سرو روان آمد پدید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میوه شیرین بکام دوستان زان تازه شاخ</p></div>
<div class="m2"><p>از پی تلخی عیش دشمنان آمد پدید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچنان آزاده شاخی وینچنین نوباوه ئی</p></div>
<div class="m2"><p>هم زبخت خسرو خسرو نشان آمد پدید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسرو عادل که در ایام او بر گوسفند</p></div>
<div class="m2"><p>گرگ ظالم پیشه را مهر شبان آمد پدید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرور گیتی جمال ملک و ملت نیک پی</p></div>
<div class="m2"><p>کز وجودش در تن ایام جان آمد پدید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنک پیش نوک پیکانش بگاه کارزار</p></div>
<div class="m2"><p>جوشن پولاد همچون پرنیان آمد پدید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از درنگ و از شتاب حزم و عزمش شمه ایست</p></div>
<div class="m2"><p>آنچه در طبع زمین و آسمان آمد پدید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شد نهان از ظلمت شب همچو ذره آفتاب</p></div>
<div class="m2"><p>گفت پیش نور رایش چون توان آمد پدید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آبحیوان خاک پای لطف جان افزای اوست</p></div>
<div class="m2"><p>ز آنسبب از وی حیات جاودان آمد پدید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سفره انعام عامش را برسم ما حضر</p></div>
<div class="m2"><p>قرص زر پیکر برین فیروزه خوان آمد پدید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خصمش از خامی خود گر پخت سودای محال</p></div>
<div class="m2"><p>ز آن چه سود آخر چو جانشرا زیان آمد پدید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خسروا ابن یمین را تا ثنا گوی تو شد</p></div>
<div class="m2"><p>خاطری چون ابر نیسان درفشان آمد پدید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ختم کردم بر دعا تا کس نگوید کای فلان</p></div>
<div class="m2"><p>از ملالت بر جبین شه نشان آمد پدید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا زمان باشد بقا بادت که ذات پاک تو بهر</p></div>
<div class="m2"><p>دفع فتنه آخر زمان آمد پدید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عمر تو بادا و فرزندانت بیش از هر چه هست</p></div>
<div class="m2"><p>کانچه میخواهی ز دولت بیش از آن آمد پدید</p></div></div>