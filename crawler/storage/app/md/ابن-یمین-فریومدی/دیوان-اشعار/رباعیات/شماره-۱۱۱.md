---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>تا سقف سپهر نیل پیکر بر پاست</p></div>
<div class="m2"><p>از جمله شهان اگر همیپرسی راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهی بکرم چو ناصر ملت و دین</p></div>
<div class="m2"><p>دارای جهان امیر ابوبکر نخاست</p></div></div>