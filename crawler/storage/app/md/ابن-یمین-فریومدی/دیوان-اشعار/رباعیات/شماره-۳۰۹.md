---
title: >-
    شمارهٔ ۳۰۹
---
# شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>دانی بچه ماند ای بت حور نژاد</p></div>
<div class="m2"><p>خالی که میانه دو ابروت فتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوئی که مگر کاتب تقدیر ز مشک</p></div>
<div class="m2"><p>بر ماه دو نون کشید و یکنقطه نهاد</p></div></div>