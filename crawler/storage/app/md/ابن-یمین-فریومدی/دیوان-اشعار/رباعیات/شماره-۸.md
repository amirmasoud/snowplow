---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ایساقی گلرخ بیار آن آب آتش فام را</p></div>
<div class="m2"><p>غائب مدار از دست می در بزم خسرو جام را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنخسرو خسرو نشان توکال قتلغ کز فلک</p></div>
<div class="m2"><p>ناهید بهر مطربی آید ببزمش نام را</p></div></div>