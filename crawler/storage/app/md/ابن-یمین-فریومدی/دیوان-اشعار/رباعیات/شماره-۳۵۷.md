---
title: >-
    شمارهٔ ۳۵۷
---
# شمارهٔ ۳۵۷

<div class="b" id="bn1"><div class="m1"><p>ای از فلکت سهم سعادت شده تیر</p></div>
<div class="m2"><p>وی از قلمت فایده ها یافته تیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حکم تو هر که سرکشد خوشه صفت</p></div>
<div class="m2"><p>هر موی که روید از تنش باشد تیر</p></div></div>