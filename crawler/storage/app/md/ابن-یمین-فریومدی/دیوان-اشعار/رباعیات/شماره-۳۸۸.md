---
title: >-
    شمارهٔ ۳۸۸
---
# شمارهٔ ۳۸۸

<div class="b" id="bn1"><div class="m1"><p>گفتیم بدانماه کلهدوز امروز</p></div>
<div class="m2"><p>کی طالع دلبری ز حسنت پیروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بستان زر و آنچنانک آید از تو</p></div>
<div class="m2"><p>از بهر کل ما کلهی سیمین دوز</p></div></div>