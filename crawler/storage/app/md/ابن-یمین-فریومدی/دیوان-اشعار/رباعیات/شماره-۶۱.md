---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>ایدل بدو نیک اینجهانی هیچست</p></div>
<div class="m2"><p>وینعالم بی ثبات فانی هیچست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر تا سر نقشها خیالیست بخواب</p></div>
<div class="m2"><p>از خواب در آی تا بدانی هیچست</p></div></div>