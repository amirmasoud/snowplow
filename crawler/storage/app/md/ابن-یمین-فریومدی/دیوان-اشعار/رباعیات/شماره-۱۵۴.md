---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>سلطان گل ار چه با بسی برگ و نواست</p></div>
<div class="m2"><p>هر چند که زر دوخته بر چین قباست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدم که گشاده کف بپیش بلبل</p></div>
<div class="m2"><p>با آنهمه برگ زو نوائی میخواست</p></div></div>