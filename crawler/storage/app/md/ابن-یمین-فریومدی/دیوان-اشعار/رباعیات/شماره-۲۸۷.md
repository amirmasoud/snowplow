---
title: >-
    شمارهٔ ۲۸۷
---
# شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>هر چند که فرزند بسامان باشد</p></div>
<div class="m2"><p>دردیست که بی مایه درمان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زیستنش رنج فراوان باشد</p></div>
<div class="m2"><p>وز مردن او خرابی جان باشد</p></div></div>