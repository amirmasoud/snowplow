---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>می از شر و شور و عربده خالی نیست</p></div>
<div class="m2"><p>خوشتر ز خوشیش ذوقکی حالی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بر سر این ذوقم و با جمله صفا</p></div>
<div class="m2"><p>با کس غرضیم جاهی و مالی نیست</p></div></div>