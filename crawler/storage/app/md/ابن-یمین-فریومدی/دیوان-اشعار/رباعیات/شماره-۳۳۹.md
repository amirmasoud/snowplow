---
title: >-
    شمارهٔ ۳۳۹
---
# شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>رویت که ازو ماه خجل سار شود</p></div>
<div class="m2"><p>زلفت که در او باد گرفتار شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ظلمت این و نور آن در شب و روز</p></div>
<div class="m2"><p>شب روز شود روز شب تار شود</p></div></div>