---
title: >-
    شمارهٔ ۲۳۸
---
# شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>ایدل فلکت گر چه زبون میدارد</p></div>
<div class="m2"><p>وز غصه او چشم تو خون میبارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این همه خوش باش که هر لحظه فلک</p></div>
<div class="m2"><p>نقشی دگر از پرده برون میآرد</p></div></div>