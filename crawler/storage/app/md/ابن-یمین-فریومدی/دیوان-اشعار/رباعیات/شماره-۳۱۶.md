---
title: >-
    شمارهٔ ۳۱۶
---
# شمارهٔ ۳۱۶

<div class="b" id="bn1"><div class="m1"><p>زان پیش که این گنبد دوار نبود</p></div>
<div class="m2"><p>و اندر دو سرا ز خلق دیار نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر نقد تو سکه بدو نیک زدند</p></div>
<div class="m2"><p>خوشباش هر آنچه رفت انگار نبود</p></div></div>