---
title: >-
    شمارهٔ ۲۴۲
---
# شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>بنگر که صبا باز چه نیرنگ آورد</p></div>
<div class="m2"><p>در باغ چه لعبتان خوشرنگ آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آنها همه با گل صفت روی تو گفت</p></div>
<div class="m2"><p>مسکین گل نازک از حیا رنگ آورد</p></div></div>