---
title: >-
    شمارهٔ ۳۶۵
---
# شمارهٔ ۳۶۵

<div class="b" id="bn1"><div class="m1"><p>چون یافت تن از خلعت شه زینت و فر</p></div>
<div class="m2"><p>نیکو سخنی گفت درینمعنی سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا که زبان مدح چون من دارم</p></div>
<div class="m2"><p>پس خلعت سر ز تن بود واجبتر</p></div></div>