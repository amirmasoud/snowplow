---
title: >-
    شمارهٔ ۴۳۷
---
# شمارهٔ ۴۳۷

<div class="b" id="bn1"><div class="m1"><p>چون خواجه بشد چه نام باقی و چه ننگ</p></div>
<div class="m2"><p>چون ماند بوارثان چه یاقوت و چه سنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز آمدنت نیست بمقصود شتاب</p></div>
<div class="m2"><p>روزی دو که در جهانت افتاد درنگ</p></div></div>