---
title: >-
    شمارهٔ ۵۳۹
---
# شمارهٔ ۵۳۹

<div class="b" id="bn1"><div class="m1"><p>ای ابن یمین مشقت کلک ببین</p></div>
<div class="m2"><p>بگریز از او و راحت ملک ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر در نشانده در زمرد خواهی</p></div>
<div class="m2"><p>در غنچه بیا شکوفه بلک ببین</p></div></div>