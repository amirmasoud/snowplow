---
title: >-
    شمارهٔ ۵۸۳
---
# شمارهٔ ۵۸۳

<div class="b" id="bn1"><div class="m1"><p>تا حسن رخ تو گشت پیرایه تو</p></div>
<div class="m2"><p>خورشید پناه داده با سایه تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برسایه بالای تو رشکم باشد</p></div>
<div class="m2"><p>من دور ز تو و سایه همسایه تو</p></div></div>