---
title: >-
    شمارهٔ ۳۱۸
---
# شمارهٔ ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>آن دم که خم عشق بجوش آمده بود</p></div>
<div class="m2"><p>جان از سر مستی بخروش آمده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی که بما کاسه می میدادند</p></div>
<div class="m2"><p>از هر طرفی صدای نوش آمده بود</p></div></div>