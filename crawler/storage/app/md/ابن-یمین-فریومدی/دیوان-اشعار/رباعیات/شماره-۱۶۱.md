---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>رفتی و شکیب از دل عشاق برفت</p></div>
<div class="m2"><p>نقش هنر از صفحه آفاق برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فضل وکرم از زمانه رفتند برون</p></div>
<div class="m2"><p>آن روز که طاهر بن اسحاق برفت</p></div></div>