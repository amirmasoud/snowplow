---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>تا رست بگرد شکرت شاخ نبات</p></div>
<div class="m2"><p>عشاق تو کردند صفتهاش اثبات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتند که دود لاله در غنچه رسید</p></div>
<div class="m2"><p>یا آبحیات شد نهان در ظلمات</p></div></div>