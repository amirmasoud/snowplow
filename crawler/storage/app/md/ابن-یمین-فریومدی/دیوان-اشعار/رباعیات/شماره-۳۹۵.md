---
title: >-
    شمارهٔ ۳۹۵
---
# شمارهٔ ۳۹۵

<div class="b" id="bn1"><div class="m1"><p>ای یاد تو مونس روان همه کس</p></div>
<div class="m2"><p>وی نام تو صیقل زبان همه کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانم بهوای تست شادان همه عمر</p></div>
<div class="m2"><p>جان من تنها نه که جان همه کس</p></div></div>