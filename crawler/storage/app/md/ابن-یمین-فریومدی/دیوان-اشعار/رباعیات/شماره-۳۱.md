---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>گر میطلبی گوهر اسرار طلب</p></div>
<div class="m2"><p>واندر صدف ایندل بیدار طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کوی تو چون راه بدر می نرود</p></div>
<div class="m2"><p>دلدار توئی و هم تو دلدار طلب</p></div></div>