---
title: >-
    شمارهٔ ۲۰۰
---
# شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>خطی که ز رویت ای پریوش برخاست</p></div>
<div class="m2"><p>دودیست کز آتش ترت خوش برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون عارض خوبت آتشی سیرابست</p></div>
<div class="m2"><p>نشگفت اگر دود ز اتش برخاست</p></div></div>