---
title: >-
    شمارهٔ ۳۱۲
---
# شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>گفتم بدل ار چه وقت گفتار نبود</p></div>
<div class="m2"><p>در پاش فکندی سرو او یار نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل گفت که گر جان رود اندر سر او</p></div>
<div class="m2"><p>غم نیست هر آنچه رفته انگار نبود</p></div></div>