---
title: >-
    شمارهٔ ۲۱۱
---
# شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>ای شکر گفتار تو سرمایه روح</p></div>
<div class="m2"><p>وی لعل گهر بار تو پیرایه روح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیسایه بود روح ولیکن رخ تو</p></div>
<div class="m2"><p>هم سایه روح امد و همسایه روح</p></div></div>