---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>ای دل چو نعیم این جهانی شدنی است</p></div>
<div class="m2"><p>وین مملکت حیات فانی شدنی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمگین مشو ار هست و گر نیست از انک</p></div>
<div class="m2"><p>بر وفق قضای آسمانی شدنی است</p></div></div>