---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>آنکس که جهان زوست جهان خود همه اوست</p></div>
<div class="m2"><p>جانانش همیخوانم و جان خود همه اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیزد شرر از آتش و چون در نگری</p></div>
<div class="m2"><p>روشن شودت ازین که آن خود همه اوست</p></div></div>