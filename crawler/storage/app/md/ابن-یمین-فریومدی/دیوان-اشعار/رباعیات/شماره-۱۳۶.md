---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>هر کو بسعادتی رسد روزی بیست</p></div>
<div class="m2"><p>زانپس بشقاوتش بسی باید زیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنگر بگل تازه و زوگیر قیاس</p></div>
<div class="m2"><p>کاندر پی یک خنده که زد چند گریست</p></div></div>