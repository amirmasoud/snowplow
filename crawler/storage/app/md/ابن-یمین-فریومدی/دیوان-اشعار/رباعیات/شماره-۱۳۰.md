---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>رنگ تو بتازگی ز گلنار بهست</p></div>
<div class="m2"><p>بویت زدم نافه تاتار بهست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم تو عجب نادره ئی افتادست</p></div>
<div class="m2"><p>چونست که بی صحت و بیمار بهست</p></div></div>