---
title: >-
    شمارهٔ ۳۲۷
---
# شمارهٔ ۳۲۷

<div class="b" id="bn1"><div class="m1"><p>روی چو مهت رشک پری میگردد</p></div>
<div class="m2"><p>زلف سیهت بدلبری میگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برگرد رخت جلوه کند طره تو</p></div>
<div class="m2"><p>در دور قمر بسروری میگردد</p></div></div>