---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>تا بر تن آزرده تب افتاد مرا</p></div>
<div class="m2"><p>یکروز نکرد هیچکس یاد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا من بزیم ثناء تب گویم از آنک</p></div>
<div class="m2"><p>تب بود که پشت گرمیی داد مرا</p></div></div>