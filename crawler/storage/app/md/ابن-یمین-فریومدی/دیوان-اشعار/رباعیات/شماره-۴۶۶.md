---
title: >-
    شمارهٔ ۴۶۶
---
# شمارهٔ ۴۶۶

<div class="b" id="bn1"><div class="m1"><p>ایدل طلب گذشته امریست محال</p></div>
<div class="m2"><p>امید بنا آمده باشد ز خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون زین دو بدست نیست الا بادی</p></div>
<div class="m2"><p>من خاک توام اگر نگردی از حال</p></div></div>