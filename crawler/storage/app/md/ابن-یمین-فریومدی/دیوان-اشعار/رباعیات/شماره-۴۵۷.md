---
title: >-
    شمارهٔ ۴۵۷
---
# شمارهٔ ۴۵۷

<div class="b" id="bn1"><div class="m1"><p>پیوسته ز رویت ایمه مهر گسل</p></div>
<div class="m2"><p>پروانه نور میبرد شمع چکل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خون دلم خوری حلالت کردم</p></div>
<div class="m2"><p>لیکن نکنم هجر دلازار بحل</p></div></div>