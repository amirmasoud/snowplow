---
title: >-
    شمارهٔ ۵۷۱
---
# شمارهٔ ۵۷۱

<div class="b" id="bn1"><div class="m1"><p>ای مونس چشم من خیال رخ تو</p></div>
<div class="m2"><p>وی دانه مرغ روحم خال رخ تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون عاشق مهجور پریشان از چیست</p></div>
<div class="m2"><p>زلفین تو چون یافت وصال رخ تو</p></div></div>