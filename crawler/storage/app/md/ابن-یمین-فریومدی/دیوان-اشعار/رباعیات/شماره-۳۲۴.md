---
title: >-
    شمارهٔ ۳۲۴
---
# شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>هر نقش که از پرده فلک بنماید</p></div>
<div class="m2"><p>شب جمله مرا شعبده بر میآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرقی که میان او و لعبت باز است</p></div>
<div class="m2"><p>آنستکه این دیر ترک میپاید</p></div></div>