---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای تازه تر از برگ سمن روی ترا</p></div>
<div class="m2"><p>صد عاشق شیداست بهر کوی ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مویت بمیان میرسد و طرفه تر آنک</p></div>
<div class="m2"><p>هرگز نرسد میان بیک سر موی ترا</p></div></div>