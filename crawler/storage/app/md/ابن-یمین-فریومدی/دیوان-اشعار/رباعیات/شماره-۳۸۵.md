---
title: >-
    شمارهٔ ۳۸۵
---
# شمارهٔ ۳۸۵

<div class="b" id="bn1"><div class="m1"><p>آن بت که نکرده ام غمش فاش هنوز</p></div>
<div class="m2"><p>چون دید مرا بسته غمهاش هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خندید و بطعنه گفت ای ابن یمین</p></div>
<div class="m2"><p>عشق تو پرو بال زند فاش هنوز</p></div></div>