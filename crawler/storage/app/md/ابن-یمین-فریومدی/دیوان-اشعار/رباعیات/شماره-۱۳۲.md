---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>نقش عیادت ار چه بصورت عبادتست</p></div>
<div class="m2"><p>لیکن بنقطه ئی ز عبادت زیادتست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرسیدن شکسته دلان اهل فضل را</p></div>
<div class="m2"><p>نقصان فضل نیست کمال سعادتست</p></div></div>