---
title: >-
    شمارهٔ ۵۰۳
---
# شمارهٔ ۵۰۳

<div class="b" id="bn1"><div class="m1"><p>پیوسته اگر با می و با معشوقیم</p></div>
<div class="m2"><p>بر ما چه ملامت چو برآن مرزوقیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار می معشوق میسر ما را</p></div>
<div class="m2"><p>زانست که ما ز بهر آن مخلوقیم</p></div></div>