---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>ای دل ز جهان بجز حقایق مطلب</p></div>
<div class="m2"><p>آسان گذران ره مضایق مطلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیرون ز دهان و از میان صنمی</p></div>
<div class="m2"><p>جز خرده مگیر و جز دقایق مطلب</p></div></div>