---
title: >-
    شمارهٔ ۴۴۵
---
# شمارهٔ ۴۴۵

<div class="b" id="bn1"><div class="m1"><p>امروز بگرما به بت مهر گسل</p></div>
<div class="m2"><p>آلود بگل عارض چون ماه چگل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلقی بتعجب نگران میگفتند</p></div>
<div class="m2"><p>بیرون ز تو خورشید که اندوده بگل</p></div></div>