---
title: >-
    شمارهٔ ۵۴۳
---
# شمارهٔ ۵۴۳

<div class="b" id="bn1"><div class="m1"><p>چون بهر نشاط و خرمی شاه جهان</p></div>
<div class="m2"><p>بگرفت بدست خسروی تیر و کمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیرش ز کمان میشد و میگفت خرد</p></div>
<div class="m2"><p>خورشید شهاب از مه نو کرد روان</p></div></div>