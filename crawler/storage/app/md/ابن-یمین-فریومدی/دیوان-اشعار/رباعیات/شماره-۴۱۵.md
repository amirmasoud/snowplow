---
title: >-
    شمارهٔ ۴۱۵
---
# شمارهٔ ۴۱۵

<div class="b" id="bn1"><div class="m1"><p>آن بت که دلم شیفته شد بر چشمش</p></div>
<div class="m2"><p>مستست بسان نرگس تر چشمش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه گه بکرشمه میزند چشم بهم</p></div>
<div class="m2"><p>یارب مرساد چشم بد در چشمش</p></div></div>