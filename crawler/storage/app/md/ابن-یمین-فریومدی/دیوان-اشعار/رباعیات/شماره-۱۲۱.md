---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>برخیز سحرگه ای صبا چابک و چست</p></div>
<div class="m2"><p>با خواجه شهاب دین بگورست و درست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کز ابن یمین شفقت خود باز مگیر</p></div>
<div class="m2"><p>کو از دل دیده بنده مخلص تست</p></div></div>