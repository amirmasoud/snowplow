---
title: >-
    شمارهٔ ۶۱۵
---
# شمارهٔ ۶۱۵

<div class="b" id="bn1"><div class="m1"><p>ایخواجه توئی انک چو تو گرم روی</p></div>
<div class="m2"><p>در مردی ورادی نبود هیچ گوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم خرکی و هر شب از کاه دریغ</p></div>
<div class="m2"><p>جو خواهد اگر چه می نیرزد بجوی</p></div></div>