---
title: >-
    شمارهٔ ۳۹۳
---
# شمارهٔ ۳۹۳

<div class="b" id="bn1"><div class="m1"><p>یا رب تو جمال آن مه مهر انگیز</p></div>
<div class="m2"><p>آراسته ئی بسنبل عنبر بیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس حکم همی کنی که در وی منگر</p></div>
<div class="m2"><p>اینحکم چنان بود که کژ دار و مریز</p></div></div>