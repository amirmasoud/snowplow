---
title: >-
    شمارهٔ ۱۹۰
---
# شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>هستم صنما ز مهر روی چو مهت</p></div>
<div class="m2"><p>سرگشته و آشفته چو زلف سیهت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ذره دلم میل هوا کرد چو یافت</p></div>
<div class="m2"><p>خورشید رخ از سایه طرف کلهت</p></div></div>