---
title: >-
    شمارهٔ ۵۹۵
---
# شمارهٔ ۵۹۵

<div class="b" id="bn1"><div class="m1"><p>با عشق تو عقل را پریشانی به</p></div>
<div class="m2"><p>بی وصل تو از عمر پشیمانی به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واندر عجبی ز حیرت ابن یمین</p></div>
<div class="m2"><p>حیرت چو ز روی تست حیرانی به</p></div></div>