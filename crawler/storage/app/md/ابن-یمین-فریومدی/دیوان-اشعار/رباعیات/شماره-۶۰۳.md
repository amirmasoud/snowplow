---
title: >-
    شمارهٔ ۶۰۳
---
# شمارهٔ ۶۰۳

<div class="b" id="bn1"><div class="m1"><p>او شاه منست و من مرا ورا بنده</p></div>
<div class="m2"><p>من خود همه اویم و بدو ماننده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون از لبش آب زندگانی خوردم</p></div>
<div class="m2"><p>مانند خضر همیشه باشم زنده</p></div></div>