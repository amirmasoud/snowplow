---
title: >-
    شمارهٔ ۵۸۵
---
# شمارهٔ ۵۸۵

<div class="b" id="bn1"><div class="m1"><p>تا بر در آرزو بود منزل تو</p></div>
<div class="m2"><p>حل می نشود مسئله مشکل تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلرا بهوس کاسه هر آش مکن</p></div>
<div class="m2"><p>تا جام جهان نمای گردد دل تو</p></div></div>