---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>ای بنده بالای تو سرو آزاد</p></div>
<div class="m2"><p>همچون تو بتی مادر ایام نزاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی دلم دهان و چشم خوش تست</p></div>
<div class="m2"><p>بیچاره نگر چه تنگ روزی افتاد</p></div></div>