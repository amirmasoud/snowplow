---
title: >-
    شمارهٔ ۳۶۸
---
# شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>هر چند که پیریم و ضعیفیم و نزار</p></div>
<div class="m2"><p>والات وادات کار افتاده ز کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با اینهمه گر سمنبری لاله عذار</p></div>
<div class="m2"><p>گوید که بگیر بوسه گویم که بیار</p></div></div>