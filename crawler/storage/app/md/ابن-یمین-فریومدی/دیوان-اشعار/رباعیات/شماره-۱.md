---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ایدل همه حاجتی روا باد ترا</p></div>
<div class="m2"><p>لیکن ز من اینمژده ترا باد ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کز هر که نشان بخت تو پرسیدم</p></div>
<div class="m2"><p>گریان شد و بس گفت بقا باد ترا</p></div></div>