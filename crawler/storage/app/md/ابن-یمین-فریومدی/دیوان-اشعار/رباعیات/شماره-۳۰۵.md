---
title: >-
    شمارهٔ ۳۰۵
---
# شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>ملهم چو نعیم این جهان فانی دید</p></div>
<div class="m2"><p>بفروخت جهان را و بدان فقر خرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با هر که در افتاد بدعوی داری</p></div>
<div class="m2"><p>هر روسپیی بدید صد بار درید</p></div></div>