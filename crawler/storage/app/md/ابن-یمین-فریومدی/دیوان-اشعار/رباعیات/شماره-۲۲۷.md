---
title: >-
    شمارهٔ ۲۲۷
---
# شمارهٔ ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>آنرا که زر و سیم بد ار زیز نماند</p></div>
<div class="m2"><p>سهلست دلا گر بتو هم چیز نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نا آمده نامدست و رفته بگذشت</p></div>
<div class="m2"><p>هان یکدمه را باش که این نیز نماند</p></div></div>