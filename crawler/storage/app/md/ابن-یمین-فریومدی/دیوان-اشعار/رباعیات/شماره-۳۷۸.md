---
title: >-
    شمارهٔ ۳۷۸
---
# شمارهٔ ۳۷۸

<div class="b" id="bn1"><div class="m1"><p>سردار جهان امیر حاجی دلیر</p></div>
<div class="m2"><p>کز جنگ چو از جود نمیگردد سیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقتیکه شود سوار بر زر ده سمند</p></div>
<div class="m2"><p>چون رستم دستان بود و رخش بزیر</p></div></div>