---
title: >-
    شمارهٔ ۴۳۳
---
# شمارهٔ ۴۳۳

<div class="b" id="bn1"><div class="m1"><p>ای در سر تو فتاده سودای عراق</p></div>
<div class="m2"><p>بد بود که افتاد ترا رأی عراق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو تا ز خراسان شده ئی بر دم تو</p></div>
<div class="m2"><p>کس می نزند دم بجز از نای عراق</p></div></div>