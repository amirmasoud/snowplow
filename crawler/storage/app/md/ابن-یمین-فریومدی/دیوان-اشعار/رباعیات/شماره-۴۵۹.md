---
title: >-
    شمارهٔ ۴۵۹
---
# شمارهٔ ۴۵۹

<div class="b" id="bn1"><div class="m1"><p>علمی که ترا می نرساند بکمال</p></div>
<div class="m2"><p>مالی که ترا می نکند نیکو حال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگریز از آنعلم و از آنمال ببر</p></div>
<div class="m2"><p>کآن علم ضلال آمد و آنمال و بال</p></div></div>