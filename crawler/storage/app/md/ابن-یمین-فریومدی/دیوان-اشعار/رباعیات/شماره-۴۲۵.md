---
title: >-
    شمارهٔ ۴۲۵
---
# شمارهٔ ۴۲۵

<div class="b" id="bn1"><div class="m1"><p>چون آتش و آب گر نشستی در میغ</p></div>
<div class="m2"><p>هم دست اجل بر تو کشد ناگه تیغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو چاره کار خویشتن کن امروز</p></div>
<div class="m2"><p>ز آن پیش که گویند که بیچاره دریغ</p></div></div>