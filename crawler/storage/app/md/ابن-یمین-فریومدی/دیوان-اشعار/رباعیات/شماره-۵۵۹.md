---
title: >-
    شمارهٔ ۵۵۹
---
# شمارهٔ ۵۵۹

<div class="b" id="bn1"><div class="m1"><p>شاها چو نمیتوان گرفتن کم نان</p></div>
<div class="m2"><p>خوشوقت کسی دان که بود همدم نان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مپسند که بر کنار خوان کرمت</p></div>
<div class="m2"><p>خلقان همه نان خورند و چاکر غم نان</p></div></div>