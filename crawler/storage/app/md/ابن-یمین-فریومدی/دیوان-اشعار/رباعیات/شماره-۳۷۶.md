---
title: >-
    شمارهٔ ۳۷۶
---
# شمارهٔ ۳۷۶

<div class="b" id="bn1"><div class="m1"><p>دلرا سر شعر و شاعری نیست دگر</p></div>
<div class="m2"><p>نی ز آنک برانش قادری نیست دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محمود همیباید ، گرنه بجهان</p></div>
<div class="m2"><p>آن نیست که همچو عنصری نیست دگر</p></div></div>