---
title: >-
    شمارهٔ ۶۰۲
---
# شمارهٔ ۶۰۲

<div class="b" id="bn1"><div class="m1"><p>زنهار در سرای خود پیوسته</p></div>
<div class="m2"><p>میدار بهر حال که باشی بسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دراز پی بستن است وینخوش سخنیست</p></div>
<div class="m2"><p>در بسته خداوند دراز غم رسته</p></div></div>