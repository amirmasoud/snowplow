---
title: >-
    شمارهٔ ۱۹۵
---
# شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>دل در سر زلفین بتان نتوان بست</p></div>
<div class="m2"><p>وز دست فراقشان به جان نتوان جست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندین متپ ای دل که سر زلف بتان</p></div>
<div class="m2"><p>دامیست که تا ابد از ان نتوان رست</p></div></div>