---
title: >-
    شمارهٔ ۶۲۱
---
# شمارهٔ ۶۲۱

<div class="b" id="bn1"><div class="m1"><p>ایگوهر پاک از کدامین کانی</p></div>
<div class="m2"><p>کز غایت روشنی ز ما پنهانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرمان ز وصال تو هم از غفلت ماست</p></div>
<div class="m2"><p>ما در طلب و تو در میان جانی</p></div></div>