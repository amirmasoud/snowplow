---
title: >-
    شمارهٔ ۵۲۶
---
# شمارهٔ ۵۲۶

<div class="b" id="bn1"><div class="m1"><p>آنکس که بدو بود دلم نازستان</p></div>
<div class="m2"><p>میکرد ازو کلاغ پرواز ستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داد از کرم خود زر و سیمم بسیار</p></div>
<div class="m2"><p>گر تو بکرم ازو کمی باز ستان</p></div></div>