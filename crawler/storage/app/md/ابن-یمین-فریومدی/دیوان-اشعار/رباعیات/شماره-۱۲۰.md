---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>تا بسته نگردد بکل ابواب حیات</p></div>
<div class="m2"><p>وز تن نشود منقطع اسباب حیات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امید توان داشت که آید بصفا</p></div>
<div class="m2"><p>از تیرگی محنت و غم آبحیات</p></div></div>