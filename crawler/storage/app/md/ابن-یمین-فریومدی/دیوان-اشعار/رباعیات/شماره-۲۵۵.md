---
title: >-
    شمارهٔ ۲۵۵
---
# شمارهٔ ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>تا ابن یمین از این جهان می نرود</p></div>
<div class="m2"><p>اندوه تواش از دل و جان می نرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ممکن نبود آنکه رود از سر او</p></div>
<div class="m2"><p>سودای تو تا در سر آن می نرود</p></div></div>