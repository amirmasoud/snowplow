---
title: >-
    شمارهٔ ۱۹۱
---
# شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>جان برخی آن پسته شکر شکنت</p></div>
<div class="m2"><p>وان قامت چون سرو و رخ چون شمنت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در وصف دهانت نتوان گفت سخن</p></div>
<div class="m2"><p>زیراک نگنجد سخن اندر دهنت</p></div></div>