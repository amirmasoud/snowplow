---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>آن زخم که بر چهره جانان منست</p></div>
<div class="m2"><p>دردیست که پیوسته بدامان منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی نی غلطم بر رخ او زخمی نیست</p></div>
<div class="m2"><p>آن زخم که دیده ایش بر جان منست</p></div></div>