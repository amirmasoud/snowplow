---
title: >-
    شمارهٔ ۶۰۰
---
# شمارهٔ ۶۰۰

<div class="b" id="bn1"><div class="m1"><p>دلدار مرا دید پریشان گشته</p></div>
<div class="m2"><p>واله شده و بیسر و سامان گشته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با همنفسان خویش میگفت بطنز</p></div>
<div class="m2"><p>کین ابن یمینست بدینسان گشته</p></div></div>