---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>ای لعل لبت مایه‌دهِ آب حیات</p></div>
<div class="m2"><p>بوسی بده از نصاب حُسنت به زکات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامن ز من اندر مکش ای جان و جهان</p></div>
<div class="m2"><p>کز صحبت چوب بید نگریخت نبات</p></div></div>