---
title: >-
    شمارهٔ ۵۰۵
---
# شمارهٔ ۵۰۵

<div class="b" id="bn1"><div class="m1"><p>با دلبر خود همیشه همدم مائیم</p></div>
<div class="m2"><p>در بزم وصال یار محرم مائیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انکس که بیکنظر دو عالم بفروخت</p></div>
<div class="m2"><p>میدان بیقین که در دو عالم مائیم</p></div></div>