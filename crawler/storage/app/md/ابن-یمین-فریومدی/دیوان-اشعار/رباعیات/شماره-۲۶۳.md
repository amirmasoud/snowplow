---
title: >-
    شمارهٔ ۲۶۳
---
# شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>چون گشت زمین از سپه برف سفید</p></div>
<div class="m2"><p>از پرتو خورشید بریدیم امید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیم است که ار غایت سردی هوا</p></div>
<div class="m2"><p>افسرده شود چشمه گرم خورشید</p></div></div>