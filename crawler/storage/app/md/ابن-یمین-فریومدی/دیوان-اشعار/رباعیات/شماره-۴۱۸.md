---
title: >-
    شمارهٔ ۴۱۸
---
# شمارهٔ ۴۱۸

<div class="b" id="bn1"><div class="m1"><p>استاد حسین ای بصفا همچو سروش</p></div>
<div class="m2"><p>با نطق تو سحبان شود از عجز خموش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بیشه مردمی و مردی و هنر</p></div>
<div class="m2"><p>تو شیر نری که نام کردت خرگوش</p></div></div>