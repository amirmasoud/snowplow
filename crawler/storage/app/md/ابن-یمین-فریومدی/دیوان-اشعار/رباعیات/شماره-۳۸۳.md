---
title: >-
    شمارهٔ ۳۸۳
---
# شمارهٔ ۳۸۳

<div class="b" id="bn1"><div class="m1"><p>از دل غم روزگار بر دارد زر</p></div>
<div class="m2"><p>بی زر منشین که کار زر دارد زر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نرگس که بصاحبنظری مشهورست</p></div>
<div class="m2"><p>از چیست از آنکه در نظر دارد زر</p></div></div>