---
title: >-
    شمارهٔ ۴۶۱
---
# شمارهٔ ۴۶۱

<div class="b" id="bn1"><div class="m1"><p>دارد صنم ماهوش زهره جمال</p></div>
<div class="m2"><p>خالی بمیانه دو ابرو و چه خال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوئی که مگر ستاره ئی منکسف است</p></div>
<div class="m2"><p>افتاده میان مشک پیکر دو هلال</p></div></div>