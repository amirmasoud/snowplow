---
title: >-
    شمارهٔ ۳۳۰
---
# شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>گویند هنر مایه اقبال بود</p></div>
<div class="m2"><p>دانش سبب حل هر اشکال بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من تجربه کردم از هنر نان خوردن</p></div>
<div class="m2"><p>برداشتن آب بغربال بود</p></div></div>