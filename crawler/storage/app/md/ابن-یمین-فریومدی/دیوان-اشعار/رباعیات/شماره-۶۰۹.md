---
title: >-
    شمارهٔ ۶۰۹
---
# شمارهٔ ۶۰۹

<div class="b" id="bn1"><div class="m1"><p>رویت که بر اوست مظهر لطف اله</p></div>
<div class="m2"><p>زیباست بر او سه نقطه خال سیاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوبی من و توسه بوسه مشاطه حسن</p></div>
<div class="m2"><p>از عنبر تر نهاده بر صفحه ماه</p></div></div>