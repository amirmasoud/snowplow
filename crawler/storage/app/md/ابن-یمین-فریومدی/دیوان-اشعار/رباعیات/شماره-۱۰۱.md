---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>بی خنده آن پسته شکر شکنت</p></div>
<div class="m2"><p>کس را چه خبر که هست یا نی دهنت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خط مسلسل است بررق حریر</p></div>
<div class="m2"><p>از سنبل تر سلسله ها بر سمنت</p></div></div>