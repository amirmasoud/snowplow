---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>ای کرده هوای مال نا پروایت</p></div>
<div class="m2"><p>بشنو سخنی عرضه کنم بر رایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وارث پسرینه چون نداری باری</p></div>
<div class="m2"><p>بیگار مکن برای دختر گایت</p></div></div>