---
title: >-
    شمارهٔ ۵۸۲
---
# شمارهٔ ۵۸۲

<div class="b" id="bn1"><div class="m1"><p>ای داده گلابرا خجالت خوی تو</p></div>
<div class="m2"><p>مستم ز هوای لب همچون می تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حقا که گرم دست دهد پی کنمش</p></div>
<div class="m2"><p>جز سایه کسی را که بود در پی تو</p></div></div>