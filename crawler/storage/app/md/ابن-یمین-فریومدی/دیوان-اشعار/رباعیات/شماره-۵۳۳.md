---
title: >-
    شمارهٔ ۵۳۳
---
# شمارهٔ ۵۳۳

<div class="b" id="bn1"><div class="m1"><p>اسبی که بمن داد امیر میران</p></div>
<div class="m2"><p>کس یاد نداردش چوان از پیران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهباز هنر نشیمن خود نکند</p></div>
<div class="m2"><p>اسبی که بود لایق گرگین گیران</p></div></div>