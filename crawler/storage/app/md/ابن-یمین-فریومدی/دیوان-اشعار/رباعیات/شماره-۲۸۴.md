---
title: >-
    شمارهٔ ۲۸۴
---
# شمارهٔ ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>گر دوست بکام در کنارم باشد</p></div>
<div class="m2"><p>با نیک و بد جهان چکارم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق وی آبروی من هیچ نماند</p></div>
<div class="m2"><p>ور ماند ز چشم اشکبارم باشد</p></div></div>