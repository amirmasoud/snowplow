---
title: >-
    شمارهٔ ۳۷۴
---
# شمارهٔ ۳۷۴

<div class="b" id="bn1"><div class="m1"><p>داود نبی چو برگشادی اسرار</p></div>
<div class="m2"><p>گفتی بپسر پند من از دل مگذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندک شمر ار هست ترا دوست هزار</p></div>
<div class="m2"><p>ور دشمن تو یکیست بسیار شمار</p></div></div>