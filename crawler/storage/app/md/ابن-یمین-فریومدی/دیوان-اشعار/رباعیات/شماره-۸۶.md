---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>آنمه سوی ما چون نظر سعد نداشت</p></div>
<div class="m2"><p>با ما ز تحملات چیزی نگذاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشوقت دلم که گفتم انگار نبود</p></div>
<div class="m2"><p>بر لوح ضمیر نقش انگاشت نگاشت</p></div></div>