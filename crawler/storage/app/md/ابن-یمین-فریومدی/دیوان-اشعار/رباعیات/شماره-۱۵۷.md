---
title: >-
    شمارهٔ ۱۵۷
---
# شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>در روضه جان تاره نباتی خضرست</p></div>
<div class="m2"><p>شیرین پسری خوش حرکاتی خضرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو خضر مجو آب حیات از ظلمات</p></div>
<div class="m2"><p>در مطلع نور آبحیاتی خضرست</p></div></div>