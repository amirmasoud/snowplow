---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>راحت ز طبیعت جهان مهجورست</p></div>
<div class="m2"><p>ره سوی مراد عاقلان بس دورست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشتر ز عسل مخواه و شیرینتر از او</p></div>
<div class="m2"><p>او نیز چو بنگری قی زنبورست</p></div></div>