---
title: >-
    شمارهٔ ۴۰۵
---
# شمارهٔ ۴۰۵

<div class="b" id="bn1"><div class="m1"><p>ای پسته شیرینت شکر خائی خوش</p></div>
<div class="m2"><p>وی در سرم از زلف تو سودائی خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بتکده ها چون تو بتی نتوان یافت</p></div>
<div class="m2"><p>شنگی خوش و شوخی خوش و رعنائی خوش</p></div></div>