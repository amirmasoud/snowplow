---
title: >-
    شمارهٔ ۵۴۷
---
# شمارهٔ ۵۴۷

<div class="b" id="bn1"><div class="m1"><p>یا رب بچه موجب بمن بیسر و بن</p></div>
<div class="m2"><p>هر لحظه غمی نو دهد اینچرخ کهن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم بقضا رضا ولی میگویم</p></div>
<div class="m2"><p>دردم چو تو داده ئی دوا نیز تو کن</p></div></div>