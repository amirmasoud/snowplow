---
title: >-
    شمارهٔ ۵۵۸
---
# شمارهٔ ۵۵۸

<div class="b" id="bn1"><div class="m1"><p>گه آتش غم شعله زند در دل من</p></div>
<div class="m2"><p>گه آب دو دیده تر کند منزل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایچرخ فلک که باد خاکت بر سر</p></div>
<div class="m2"><p>از دور تو باد است همه حاصل من</p></div></div>