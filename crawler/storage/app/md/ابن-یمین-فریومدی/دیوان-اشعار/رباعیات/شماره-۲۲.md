---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>برخیز اگر دسترسی هست ترا</p></div>
<div class="m2"><p>میدان بیقین که نیست پیوست ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در یاب و مده فرصت امکان از دست</p></div>
<div class="m2"><p>شاید که دگر می نرسد دست ترا</p></div></div>