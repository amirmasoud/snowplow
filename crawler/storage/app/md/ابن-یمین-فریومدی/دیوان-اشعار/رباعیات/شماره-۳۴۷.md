---
title: >-
    شمارهٔ ۳۴۷
---
# شمارهٔ ۳۴۷

<div class="b" id="bn1"><div class="m1"><p>هر چیز کزو هستی تو پیدا شد</p></div>
<div class="m2"><p>هم اوست که گه خامش و گه گویا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آب که شد ابر و ز دریا برخاست</p></div>
<div class="m2"><p>شد قطره و قطره باز با دریا شد</p></div></div>