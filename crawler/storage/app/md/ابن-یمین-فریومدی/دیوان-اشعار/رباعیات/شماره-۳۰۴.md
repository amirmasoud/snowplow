---
title: >-
    شمارهٔ ۳۰۴
---
# شمارهٔ ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>گفت آنکه مرا دید ز طاعات بعید</p></div>
<div class="m2"><p>کاندیشه نمیکنی تو از وعد و وعید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که ز بهر من در استغفارند</p></div>
<div class="m2"><p>آنها که شدند حامل عرش مجید</p></div></div>