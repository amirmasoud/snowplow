---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>ایمظهر الطاف لطیفیت خوشست</p></div>
<div class="m2"><p>با همنفسان ذوق و ظریفیت خوشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برخیز و بیا و یک صراحی می ناب</p></div>
<div class="m2"><p>با خویش بیاور که حریفیت خوشست</p></div></div>