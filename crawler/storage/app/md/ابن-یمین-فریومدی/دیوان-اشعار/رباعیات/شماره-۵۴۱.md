---
title: >-
    شمارهٔ ۵۴۱
---
# شمارهٔ ۵۴۱

<div class="b" id="bn1"><div class="m1"><p>یک لحظه خیال رویت ایدلبر من</p></div>
<div class="m2"><p>بیرون نرود بهیچ رو از سر من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سایه خورشید رخت خواهد خاست</p></div>
<div class="m2"><p>از خال لحد گر گذری پیکر من</p></div></div>