---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>در خاطرم این لطیف مصراع گذشت</p></div>
<div class="m2"><p>کاحوال جهان بسکه بانواع گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بس شه با تیغ که بر اسب غرور</p></div>
<div class="m2"><p>چون باد بدست ماند و اتباع گذشت</p></div></div>