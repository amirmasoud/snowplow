---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>ای سرور عهد هرکه شمشیر تو بست</p></div>
<div class="m2"><p>شیر فلکش به زیر پالان چو خرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدخواه تو خشک‌مغز و گندا چو کماست</p></div>
<div class="m2"><p>وز عمر چو باد بیزنش باد به دست</p></div></div>