---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>تا جیب تو بهر ماه مطلع کردند</p></div>
<div class="m2"><p>ترک صفت ماه مقنع کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن حقه لعل بین که از بهر بهار</p></div>
<div class="m2"><p>چون خوش بزمردش مرصع کردند</p></div></div>