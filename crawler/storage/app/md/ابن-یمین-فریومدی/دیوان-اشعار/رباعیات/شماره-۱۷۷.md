---
title: >-
    شمارهٔ ۱۷۷
---
# شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>کس تیغ چو پهلوان ایران نزدست</p></div>
<div class="m2"><p>خنجر به ازو رستم دستان نزدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زخمی که سپهبد جهان حیدر زد</p></div>
<div class="m2"><p>حقا که ابو لولوئه به زان نزدست</p></div></div>