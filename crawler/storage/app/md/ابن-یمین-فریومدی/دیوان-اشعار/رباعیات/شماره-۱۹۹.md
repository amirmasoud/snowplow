---
title: >-
    شمارهٔ ۱۹۹
---
# شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>قدت ز صنوبری که برخاست بهست</p></div>
<div class="m2"><p>وز سرو که دهقانش بپیراست بهست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دائم سخن از قد چو سروت گویم</p></div>
<div class="m2"><p>آری سخن راست بهر وقت بهست</p></div></div>