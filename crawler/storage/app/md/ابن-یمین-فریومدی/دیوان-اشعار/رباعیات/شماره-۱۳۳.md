---
title: >-
    شمارهٔ ۱۳۳
---
# شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>در کارگه وجود هر نقش که هست</p></div>
<div class="m2"><p>نقاش الست بیتو آن نقش ببست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حیرتم از حال تو تا خود تو که ئی</p></div>
<div class="m2"><p>نی بیتو بود کار و نه کاریت به دست</p></div></div>