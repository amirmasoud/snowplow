---
title: >-
    شمارهٔ ۲۷۴
---
# شمارهٔ ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>در هجر تو از من اثر وعین نماند</p></div>
<div class="m2"><p>کی وصل تو بینم چو مرا عین نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین بیش ببود عین ما را دمعی</p></div>
<div class="m2"><p>اکنون ز غم تو دمع را عین نماند</p></div></div>