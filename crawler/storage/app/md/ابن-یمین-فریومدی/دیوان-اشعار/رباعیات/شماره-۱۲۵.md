---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>در روضه توحید اگر بارت نیست</p></div>
<div class="m2"><p>بر شاخ مراد خویشتن بارت نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خود غم دل گوی مگو با کس از آنک</p></div>
<div class="m2"><p>بیرون ز تو کس محرم اسرارت نیست</p></div></div>