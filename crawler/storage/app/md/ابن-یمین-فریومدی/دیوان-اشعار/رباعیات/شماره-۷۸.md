---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>انسان نه بدینصورت و شکل انسانست</p></div>
<div class="m2"><p>کین شکل ز آمیزش جسم و جانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنی تو که جسم وجان من میگویند</p></div>
<div class="m2"><p>و آنچیز که هر چه هست آنست آنست</p></div></div>