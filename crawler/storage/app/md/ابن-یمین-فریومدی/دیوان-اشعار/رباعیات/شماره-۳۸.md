---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>ای ز آتش سودات دل اندر تب و تاب</p></div>
<div class="m2"><p>وی عالم خاکیم ز عشق تو خراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم ز غمت مردم چشمی چو حباب</p></div>
<div class="m2"><p>دائم ز هوا خیمه زده بر سر آب</p></div></div>