---
title: >-
    شمارهٔ ۱۶۰
---
# شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>زلف تو که سرگشته بکردار منست</p></div>
<div class="m2"><p>آشفته تر از حال من و کار منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودای وی از دماغ بیرون نکنم</p></div>
<div class="m2"><p>هر چند کزو شکست بازار منست</p></div></div>