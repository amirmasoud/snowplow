---
title: >-
    شمارهٔ ۳۰۷
---
# شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>هر کو در جود و مکرمت بسته بود</p></div>
<div class="m2"><p>جز خار مدانش ار چه گلدسته بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیزم بود آنشاخ که بر می نهد</p></div>
<div class="m2"><p>ور چه ز درخت بارور رسته بود</p></div></div>