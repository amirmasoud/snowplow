---
title: >-
    شمارهٔ ۴۵۸
---
# شمارهٔ ۴۵۸

<div class="b" id="bn1"><div class="m1"><p>دریای وجود را یکی دان بمثل</p></div>
<div class="m2"><p>زو رفته بهر سوی هزاران جدول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو راست نگر کانهمه در اصل یکیست</p></div>
<div class="m2"><p>از کژ نظری یکی دو بیند احوال</p></div></div>