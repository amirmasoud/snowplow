---
title: >-
    شمارهٔ ۶۲۲
---
# شمارهٔ ۶۲۲

<div class="b" id="bn1"><div class="m1"><p>در صورت ما جمال خود می‌بینی</p></div>
<div class="m2"><p>در دیده ما خیال خود می‌بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سینه ما سرور خود می‌یابی</p></div>
<div class="m2"><p>در کسوت ما وصال خود می‌بینی</p></div></div>