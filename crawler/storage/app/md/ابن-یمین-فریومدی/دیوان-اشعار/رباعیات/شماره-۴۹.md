---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>افسوس که عمر من ز هفتاد گذشت</p></div>
<div class="m2"><p>بگذشت چنانکه بگذرد باد بدشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آخر کارها فنا خواهد بود</p></div>
<div class="m2"><p>پس مدت عمر ما چه هشتاد و چه هشت</p></div></div>