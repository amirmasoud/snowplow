---
title: >-
    شمارهٔ ۵۱۵
---
# شمارهٔ ۵۱۵

<div class="b" id="bn1"><div class="m1"><p>زین پیش که سودای جنون داشت سرم</p></div>
<div class="m2"><p>بودی هوس مشغله و شور و شرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با عامه از این پس آبحیوان نخورم</p></div>
<div class="m2"><p>گر ز آتش تشنگی بسوزد جگرم</p></div></div>