---
title: >-
    شمارهٔ ۳۶۴
---
# شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>هنگام سپیده دم گل سیب نگر</p></div>
<div class="m2"><p>ز آب بقمش نقطه ز ده باد سحر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوئی رخ نازک دلارام منست</p></div>
<div class="m2"><p>افشانده بر او دیده من خون جگر</p></div></div>