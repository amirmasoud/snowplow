---
title: >-
    شمارهٔ ۴۳۹
---
# شمارهٔ ۴۳۹

<div class="b" id="bn1"><div class="m1"><p>مائیم و می روح فزا چون دم مشک</p></div>
<div class="m2"><p>لیکن بر اهل عصر چه مشک و چه پشک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پای عوام کشته گشتند خواص</p></div>
<div class="m2"><p>آتش چو در افتاد نه تر ماند و نه خشک</p></div></div>