---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>هر روز ترا بمن گمانی دگرست</p></div>
<div class="m2"><p>هر لحظه مرا ز تو زیانی دگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند ترا جهان بکامست ولیک</p></div>
<div class="m2"><p>بیشک پس ازینجهان جهانی دگر است</p></div></div>