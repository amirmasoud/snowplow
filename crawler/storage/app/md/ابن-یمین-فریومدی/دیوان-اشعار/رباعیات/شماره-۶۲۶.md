---
title: >-
    شمارهٔ ۶۲۶
---
# شمارهٔ ۶۲۶

<div class="b" id="bn1"><div class="m1"><p>آمد ز چمن باد صبا مشکین بوی</p></div>
<div class="m2"><p>و آورد بمن از سمن و نسرین بوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی نی غلطم بچین زلف تو گذشت</p></div>
<div class="m2"><p>ورنی ز کجاست در چمن چندین بوی</p></div></div>