---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>آن سرو سهی که جاش در چشم منست</p></div>
<div class="m2"><p>بی لعل لبش بحر گهر چشم منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدم برهش بتازگی آب روان</p></div>
<div class="m2"><p>و آن آب روان هنوز در چشم منست</p></div></div>