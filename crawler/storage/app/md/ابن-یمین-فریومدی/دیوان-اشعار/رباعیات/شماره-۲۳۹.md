---
title: >-
    شمارهٔ ۲۳۹
---
# شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>بر آتش غم بکام دل روزی چند</p></div>
<div class="m2"><p>خواهیم زد آب با دل افروزی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان پیش که بر باد دهد دست اجل</p></div>
<div class="m2"><p>گرد از سر خاک ما جگر سوزی چند</p></div></div>