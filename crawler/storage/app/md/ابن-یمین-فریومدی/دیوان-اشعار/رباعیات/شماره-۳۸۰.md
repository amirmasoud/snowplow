---
title: >-
    شمارهٔ ۳۸۰
---
# شمارهٔ ۳۸۰

<div class="b" id="bn1"><div class="m1"><p>فرزند اعز محمد ای جان پدر</p></div>
<div class="m2"><p>آیا بود اینکه بینمت بار دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غرقم ز غمت چو لاله در خون جگر</p></div>
<div class="m2"><p>وز فکر بنفشه وار زانو بر سر</p></div></div>