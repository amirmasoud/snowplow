---
title: >-
    شمارهٔ ۵۵۴
---
# شمارهٔ ۵۵۴

<div class="b" id="bn1"><div class="m1"><p>دی بر سر خاک دوستی دلبر من</p></div>
<div class="m2"><p>از دیده گلاب ریخت بر برگ سمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم مگری ندا کن آنرا که ترا</p></div>
<div class="m2"><p>آورد بگریه تا زند چاک کفن</p></div></div>