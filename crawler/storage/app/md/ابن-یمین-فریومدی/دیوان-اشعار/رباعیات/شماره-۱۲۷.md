---
title: >-
    شمارهٔ ۱۲۷
---
# شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>سردار جهان خواجه که آئینش سخاست</p></div>
<div class="m2"><p>بحریست کفش که موج آن جمله عطاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من تشنه لب از ساحل آن گشتم باز</p></div>
<div class="m2"><p>این نیز هم از طالع شوریده ماست</p></div></div>