---
title: >-
    شمارهٔ ۲۲۹
---
# شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>آنکس که همای همتش پر دارد</p></div>
<div class="m2"><p>هر روز هوای جای دیگر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بر در خانه ها مقیم است خروس</p></div>
<div class="m2"><p>بنگر که همیشه اره بر سر دارد</p></div></div>