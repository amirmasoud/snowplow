---
title: >-
    شمارهٔ ۵۱۰
---
# شمارهٔ ۵۱۰

<div class="b" id="bn1"><div class="m1"><p>درد دل زار و زردی رخ دارم</p></div>
<div class="m2"><p>صد موکبه غم بر پی صد موکب غم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لطف صنم شنگ کند بیخ غمم</p></div>
<div class="m2"><p>نیکو شکند بسعی صهبا تب غم</p></div></div>