---
title: >-
    شمارهٔ ۶۰۴
---
# شمارهٔ ۶۰۴

<div class="b" id="bn1"><div class="m1"><p>کو بخت که از جهان بیابم بهره</p></div>
<div class="m2"><p>وز گردش آسمان بیابم بهره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی نی همه کارها بکامم شده گیر</p></div>
<div class="m2"><p>کو عمر که تا از آن بیابم بهره</p></div></div>