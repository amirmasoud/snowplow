---
title: >-
    شمارهٔ ۴۵۲
---
# شمارهٔ ۴۵۲

<div class="b" id="bn1"><div class="m1"><p>هم عاشق آنروی چو مه دارم دل</p></div>
<div class="m2"><p>هم بسته آنزلف سیه دارم دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که تو داری دل من گفتا نه</p></div>
<div class="m2"><p>من نیستم آنکس که نگهدارم دل</p></div></div>