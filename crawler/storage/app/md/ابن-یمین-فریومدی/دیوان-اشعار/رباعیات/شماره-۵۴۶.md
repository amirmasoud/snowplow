---
title: >-
    شمارهٔ ۵۴۶
---
# شمارهٔ ۵۴۶

<div class="b" id="bn1"><div class="m1"><p>هرگز دهنت ایصنم سیم سرین</p></div>
<div class="m2"><p>با پسته برابر نکند ابن یمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیراک میان هر دو فرقیست تمام</p></div>
<div class="m2"><p>اینرا لب نوشین بود آنرا شکرین</p></div></div>