---
title: >-
    شمارهٔ ۲۶۰
---
# شمارهٔ ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>چون اشکم ازینچشم چو جیحون بچکد</p></div>
<div class="m2"><p>بر برگ زریر آب طبر خون بچکد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر شام که یاد آرم از انروی چو صبح</p></div>
<div class="m2"><p>همچون شفق از دیده من خون بچکد</p></div></div>