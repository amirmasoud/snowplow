---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>من رفتم و یادگار جانم بر تست</p></div>
<div class="m2"><p>پیدا ز تو دورم و نهانم بر تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلشادم از انکه گر روانم زبرت</p></div>
<div class="m2"><p>هر جا که همی روم روانم بر تست</p></div></div>