---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>روی تو و ماه آسمان هر دو یکیست</p></div>
<div class="m2"><p>قد تو و سرو بوستان هر دو یکیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل برده و جان میبری ایدوست مگر</p></div>
<div class="m2"><p>بازار تو را قلب و روان هر دو یکیست</p></div></div>