---
title: >-
    شمارهٔ ۴۷۹
---
# شمارهٔ ۴۷۹

<div class="b" id="bn1"><div class="m1"><p>ای برده سر زلف تو از جای دلم</p></div>
<div class="m2"><p>و افکنده سر زلف تو بر پای دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درمان دلم لعل لبت گر نکند</p></div>
<div class="m2"><p>پس وای دلم وای دلم وای دلم</p></div></div>