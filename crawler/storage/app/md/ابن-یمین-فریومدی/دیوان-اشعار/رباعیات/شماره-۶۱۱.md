---
title: >-
    شمارهٔ ۶۱۱
---
# شمارهٔ ۶۱۱

<div class="b" id="bn1"><div class="m1"><p>ای دور شب فراق آخر بسر آی</p></div>
<div class="m2"><p>وی نوبت روز وصل یکبار در آی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عمر منی ایشب هجران بگذر</p></div>
<div class="m2"><p>ور جان منی ای نفس صبح بر آی</p></div></div>