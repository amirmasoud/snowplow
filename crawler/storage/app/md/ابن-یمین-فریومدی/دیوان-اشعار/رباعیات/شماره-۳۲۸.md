---
title: >-
    شمارهٔ ۳۲۸
---
# شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>زلف تو که بازی مجازی نکند</p></div>
<div class="m2"><p>جز با دل عشاق تو بازی نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایخسرو شیرین لب خوبان جهان</p></div>
<div class="m2"><p>فرمای که تا دست درازی نکند</p></div></div>