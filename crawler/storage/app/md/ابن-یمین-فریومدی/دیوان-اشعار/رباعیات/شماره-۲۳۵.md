---
title: >-
    شمارهٔ ۲۳۵
---
# شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>ایخواجه علی توئی جهان همه جود</p></div>
<div class="m2"><p>نامد بدلیری تو هرگز بوجود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو رستم زابلی و کو حاتم طی</p></div>
<div class="m2"><p>تا پیش دل و دست تو آرند سجود</p></div></div>