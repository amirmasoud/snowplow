---
title: >-
    شمارهٔ ۵۷۰
---
# شمارهٔ ۵۷۰

<div class="b" id="bn1"><div class="m1"><p>یکچند چراغ آرزوها پف کن</p></div>
<div class="m2"><p>قطع نظر از جمال هر یوسف کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین شهد یک انگشت بکامت در کش</p></div>
<div class="m2"><p>از لذت اگر محو نگردی تف کن</p></div></div>