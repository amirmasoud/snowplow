---
title: >-
    شمارهٔ ۳۶۲
---
# شمارهٔ ۳۶۲

<div class="b" id="bn1"><div class="m1"><p>من برکشم این پیرهن زهد ز سر</p></div>
<div class="m2"><p>پس در کشمت همچو قبا تنگ ببر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آیم چو کلاه بر سر اندر ره عشق</p></div>
<div class="m2"><p>گر دست در ارم بمیانت چو کمر</p></div></div>