---
title: >-
    شمارهٔ ۵۳۶
---
# شمارهٔ ۵۳۶

<div class="b" id="bn1"><div class="m1"><p>ای ترک بیار باده و در گردان</p></div>
<div class="m2"><p>وز خطه دل عنان غم بر گردان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آنروز که گردنامه ئی بنوشتی</p></div>
<div class="m2"><p>بر ماه بخط خود شدم سرگردان</p></div></div>