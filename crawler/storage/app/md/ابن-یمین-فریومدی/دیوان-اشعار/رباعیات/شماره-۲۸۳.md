---
title: >-
    شمارهٔ ۲۸۳
---
# شمارهٔ ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>گر دلبر ما غمخور ما خواهد بود</p></div>
<div class="m2"><p>درد دل ما عین دوا خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانان بعیادت ار قدم رنجه کند</p></div>
<div class="m2"><p>بیماری ما به ز شفا خواهد بود</p></div></div>