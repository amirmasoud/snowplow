---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>ایدل طلب دوست گرت عادت و خوست</p></div>
<div class="m2"><p>بیرون مبر از خویش پی اندر پی دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او جز من و من جز او نه ورحق طلبی</p></div>
<div class="m2"><p>من من نیم انکس که منم اوست که اوست</p></div></div>