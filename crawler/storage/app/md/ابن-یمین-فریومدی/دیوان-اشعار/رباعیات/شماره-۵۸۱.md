---
title: >-
    شمارهٔ ۵۸۱
---
# شمارهٔ ۵۸۱

<div class="b" id="bn1"><div class="m1"><p>ای نور دو دیده آن سزد مذهب تو</p></div>
<div class="m2"><p>گر منبع تحقیق بود مشرب تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبستنی شب جهان میبینی</p></div>
<div class="m2"><p>خوشباش چه دانی که چه زاید شب تو</p></div></div>