---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>آن دل که بر او مهر تو تابان باشد</p></div>
<div class="m2"><p>میسوزد و شمعوار خندان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو جان منی و تا ابد خواهد بود</p></div>
<div class="m2"><p>از تو نبرم تا ببرم جان باشد</p></div></div>