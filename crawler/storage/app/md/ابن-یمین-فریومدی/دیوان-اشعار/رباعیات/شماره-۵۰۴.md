---
title: >-
    شمارهٔ ۵۰۴
---
# شمارهٔ ۵۰۴

<div class="b" id="bn1"><div class="m1"><p>من صحبت جانانه از ان میخواهم</p></div>
<div class="m2"><p>کارام دل و راحت جان میخواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با دوست گرم خلوتکی دست دهد</p></div>
<div class="m2"><p>از مردم دیده هم نهان میخواهم</p></div></div>