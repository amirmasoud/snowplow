---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>جان برخی آنروز که آن در خوشاب</p></div>
<div class="m2"><p>با بنده بصد ناز همی کرد عتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس شب که در این هوس بروز آوردم</p></div>
<div class="m2"><p>کانروز مگر شبی ببینم در خواب</p></div></div>