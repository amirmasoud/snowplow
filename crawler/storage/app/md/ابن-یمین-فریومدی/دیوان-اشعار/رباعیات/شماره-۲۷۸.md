---
title: >-
    شمارهٔ ۲۷۸
---
# شمارهٔ ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>روشن شود آنرا که هدایت باشد</p></div>
<div class="m2"><p>کین دور وجود بینهایت باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دایره هر نقطه که غایت گیری</p></div>
<div class="m2"><p>شاید که همان نقطه بدایت باشد</p></div></div>