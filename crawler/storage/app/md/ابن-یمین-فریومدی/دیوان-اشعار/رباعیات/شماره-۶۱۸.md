---
title: >-
    شمارهٔ ۶۱۸
---
# شمارهٔ ۶۱۸

<div class="b" id="bn1"><div class="m1"><p>ای جسته دوا از در هر بیماری</p></div>
<div class="m2"><p>تا چند ز تقلید تو در هر کاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر صاحب افسری نیاری گشتن</p></div>
<div class="m2"><p>جهدی کن و سرمده فرا افساری</p></div></div>