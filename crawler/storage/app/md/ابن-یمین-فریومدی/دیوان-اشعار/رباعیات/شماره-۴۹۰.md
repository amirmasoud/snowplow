---
title: >-
    شمارهٔ ۴۹۰
---
# شمارهٔ ۴۹۰

<div class="b" id="bn1"><div class="m1"><p>لقمان که حکیمان جهانراست امام</p></div>
<div class="m2"><p>فرمود بحفظ چار در چار مقام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل در گه طاعت و زبان وقت کلام</p></div>
<div class="m2"><p>دیده گه دیدن و شکم وقت طعام</p></div></div>