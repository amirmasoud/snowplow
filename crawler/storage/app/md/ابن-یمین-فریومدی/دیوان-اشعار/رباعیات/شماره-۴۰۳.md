---
title: >-
    شمارهٔ ۴۰۳
---
# شمارهٔ ۴۰۳

<div class="b" id="bn1"><div class="m1"><p>ای پیک چو نامه ام نهی در دستش</p></div>
<div class="m2"><p>صد بوسه نهی ازین رهی بر دستش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وی نامه حسدهاست مرا بر تو از آنک</p></div>
<div class="m2"><p>تو پیش ز من بوسه دهی بر دستش</p></div></div>