---
title: >-
    شمارهٔ ۴۲۳
---
# شمارهٔ ۴۲۳

<div class="b" id="bn1"><div class="m1"><p>ایروی دلارای تو رخشنده چو شمع</p></div>
<div class="m2"><p>بفروز شبی کلبه من بنده چو شمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خنده زنم بیتو بر آن خنده گری</p></div>
<div class="m2"><p>کز گریه خویش میزنم خنده چو شمع</p></div></div>