---
title: >-
    شمارهٔ ۳۹۰
---
# شمارهٔ ۳۹۰

<div class="b" id="bn1"><div class="m1"><p>پروانه صفت در آتشم زاندم باز</p></div>
<div class="m2"><p>کز باد اجل فروشد آنشمع طراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آب دو دیده گر شوم غرقه رواست</p></div>
<div class="m2"><p>چون رفت بخاک آن بت پرورده بناز</p></div></div>