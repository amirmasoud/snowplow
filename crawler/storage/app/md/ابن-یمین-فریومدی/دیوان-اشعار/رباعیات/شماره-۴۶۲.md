---
title: >-
    شمارهٔ ۴۶۲
---
# شمارهٔ ۴۶۲

<div class="b" id="bn1"><div class="m1"><p>در خطه سبزوار دیدم امسال</p></div>
<div class="m2"><p>حالی که نماند بر فرزانه مجال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در رسته بازار کلهدوزانش</p></div>
<div class="m2"><p>برجیست درو جمع بهم بدر و هلال</p></div></div>