---
title: >-
    شمارهٔ ۵۹۲
---
# شمارهٔ ۵۹۲

<div class="b" id="bn1"><div class="m1"><p>ایدل تو مپندار بجانی زنده</p></div>
<div class="m2"><p>لطفیست الهی که بدانی زنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر با تو بود جان بکجا دارد جای</p></div>
<div class="m2"><p>ور بیتو بود پس بچه مانی زنده</p></div></div>