---
title: >-
    شمارهٔ ۵۲۲
---
# شمارهٔ ۵۲۲

<div class="b" id="bn1"><div class="m1"><p>گه رنگ رخت ز عارض گل طلبیم</p></div>
<div class="m2"><p>گه بوی خوشت ز برگ سنبل طلبیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه نغمه دلفریب روح افزایت</p></div>
<div class="m2"><p>از ناله جان فزای بلبل طلبیم</p></div></div>