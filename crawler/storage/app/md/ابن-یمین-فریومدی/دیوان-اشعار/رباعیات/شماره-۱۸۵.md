---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>جانا رخ تو ماه زر افشان منست</p></div>
<div class="m2"><p>میگون لب تو لعل درخشان منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمریست که تا خون دلم لعل تو دید</p></div>
<div class="m2"><p>شکرانه آن هنوز بر جان منست</p></div></div>