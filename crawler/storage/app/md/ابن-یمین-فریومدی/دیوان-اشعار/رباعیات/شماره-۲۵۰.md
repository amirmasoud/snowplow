---
title: >-
    شمارهٔ ۲۵۰
---
# شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>تا دل غم آنجان جهان خواهد خورد</p></div>
<div class="m2"><p>سر نشتر غم بر رگ جان خواهد خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیکان خدنک غمزه خونریزش</p></div>
<div class="m2"><p>در آتش دل آب روان خواهد خورد</p></div></div>