---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>ایدل اگر آسایش جانت هوس است</p></div>
<div class="m2"><p>ور مملکت هر دو جهانت هوس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشنو سخنی ریخته در قالب حق</p></div>
<div class="m2"><p>بر کن طمع از هر چه بدانت هوس است</p></div></div>