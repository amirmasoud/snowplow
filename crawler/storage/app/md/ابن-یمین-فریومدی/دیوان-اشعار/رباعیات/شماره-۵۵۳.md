---
title: >-
    شمارهٔ ۵۵۳
---
# شمارهٔ ۵۵۳

<div class="b" id="bn1"><div class="m1"><p>باد سحری دوش من و یک دو سه تن</p></div>
<div class="m2"><p>بودیم در آرزوی آنسرو چمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتیم که آرد بر ما خاک درش</p></div>
<div class="m2"><p>باد سحر از میانه برخواست که من</p></div></div>