---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>ان روی چو افتاب در چشم منست</p></div>
<div class="m2"><p>وانموی چو مشک ناب در چشم منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدم بگه زوال خورشید رخش</p></div>
<div class="m2"><p>زانلحظه هنوزم آب در چشم منست</p></div></div>