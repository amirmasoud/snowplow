---
title: >-
    شمارهٔ ۴۸۱
---
# شمارهٔ ۴۸۱

<div class="b" id="bn1"><div class="m1"><p>افتاد گره بر تن مجروح سقیم</p></div>
<div class="m2"><p>چون قطره اشک بر رخ زرد یتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زینسان که منم مرا مسیحی باید</p></div>
<div class="m2"><p>تا به کند از رنج تن زار الیم</p></div></div>