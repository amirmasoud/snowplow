---
title: >-
    شمارهٔ ۲۳۰
---
# شمارهٔ ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>ایدل ز کست گر چه نوائی نرسید</p></div>
<div class="m2"><p>لیکن اگر آنکس که ترا کرد پدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر بر خط او نهی همچو قلم</p></div>
<div class="m2"><p>با برهنگی ملک توانی بخشید</p></div></div>