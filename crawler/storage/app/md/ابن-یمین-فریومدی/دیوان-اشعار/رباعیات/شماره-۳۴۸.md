---
title: >-
    شمارهٔ ۳۴۸
---
# شمارهٔ ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>غریب اگر چه وزیر شه جهان باشد</p></div>
<div class="m2"><p>همیشه میل دلش سوی خانمان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه ساعد شاهان بود نشیمن باز</p></div>
<div class="m2"><p>ولی بکام دل باز آشیان باشد</p></div></div>