---
title: >-
    شمارهٔ ۴۹۴
---
# شمارهٔ ۴۹۴

<div class="b" id="bn1"><div class="m1"><p>هر چند بود عارض تو در نظرم</p></div>
<div class="m2"><p>هر لحظه بود شوق رخت بیشترم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم که شود هر سر مویم چشمی</p></div>
<div class="m2"><p>تا بهر تماشا بتو در مینگرم</p></div></div>