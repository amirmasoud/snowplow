---
title: >-
    شمارهٔ ۲۴۸
---
# شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>تا مرغ روانت در قفس خواهد بود</p></div>
<div class="m2"><p>رزقت رسد آن قدر که بس خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون راه نفس بسته شد اندیشه مکن</p></div>
<div class="m2"><p>زانحال که وارثی ز پس خواهد بود</p></div></div>