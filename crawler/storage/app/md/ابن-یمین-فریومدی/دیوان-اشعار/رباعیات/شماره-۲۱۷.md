---
title: >-
    شمارهٔ ۲۱۷
---
# شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>آن بت که دمی جفا فراموش نکرد</p></div>
<div class="m2"><p>با ما نفسی دست در آغوش نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نکته که سر بسر بدو میگفتم</p></div>
<div class="m2"><p>در بود ولی نگار در گوش نکرد</p></div></div>