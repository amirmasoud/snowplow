---
title: >-
    شمارهٔ ۵۹۰
---
# شمارهٔ ۵۹۰

<div class="b" id="bn1"><div class="m1"><p>ای چرخ فلک هزار فریاد از تو</p></div>
<div class="m2"><p>آن به که نیاورد کسی یاد از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرگشته و با لباس محنت زدگان</p></div>
<div class="m2"><p>پیوسته از آنی که نیم شاد از تو</p></div></div>