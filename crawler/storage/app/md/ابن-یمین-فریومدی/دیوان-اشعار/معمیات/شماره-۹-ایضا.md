---
title: >-
    شمارهٔ  ۹ - ایضاً
---
# شمارهٔ  ۹ - ایضاً

<div class="b" id="bn1"><div class="m1"><p>آن چیست که چون ابروی جانان باشد</p></div>
<div class="m2"><p>قلب وی و مستویش یکسان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بحر ضمیر خویشتن شست انداز</p></div>
<div class="m2"><p>کانچیز که در شست فتد آن باشد</p></div></div>