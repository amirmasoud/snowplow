---
title: >-
    شمارهٔ  ۱۱ - ایضاً
---
# شمارهٔ  ۱۱ - ایضاً

<div class="b" id="bn1"><div class="m1"><p>پرسم لغزی ای شده فاش از تو هنر</p></div>
<div class="m2"><p>فکری کن و جهدی کن و بیرون آور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچیست که چون بر شمری مجموعش</p></div>
<div class="m2"><p>نصفش نبود ز عشر او افزونتر</p></div></div>