---
title: >-
    شمارهٔ  ۵ - ایضاً
---
# شمارهٔ  ۵ - ایضاً

<div class="b" id="bn1"><div class="m1"><p>چیست آن گوهر شهوار میان پر زر و سیم</p></div>
<div class="m2"><p>سیم و زر هر دو درو آب و بهم ناممزوج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست چون صبح دوم غرقه زر اندر سیمش</p></div>
<div class="m2"><p>لیک صبحی که نباشد به بلندیش عروج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه بود حقه سیماب و درو مهره زر</p></div>
<div class="m2"><p>گاه از آن حقه کند موذن شبخیز خروج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اجتماع مه و مهرست بدو در همه وقت</p></div>
<div class="m2"><p>چه عجب صورت او هست چه برجی ز بروج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این لغز بر تو اگر حل نکند ابن یمین</p></div>
<div class="m2"><p>نتوانی بدر آوردن الی یوم خروج</p></div></div>