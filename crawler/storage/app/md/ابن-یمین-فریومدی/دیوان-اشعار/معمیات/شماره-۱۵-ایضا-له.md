---
title: >-
    شمارهٔ  ۱۵ - ایضاً له
---
# شمارهٔ  ۱۵ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>چیست آن پیکر پری کردار</p></div>
<div class="m2"><p>گاه مینا برنگ و گه مرجان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوی یاقوت را همی ماند</p></div>
<div class="m2"><p>بسته اندر زمردین چوگان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنگ او همچو گونه معشوق</p></div>
<div class="m2"><p>که رخش گردد از حیا رخشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست برجی ز خلد پنداری</p></div>
<div class="m2"><p>از پی حور ساخته رضوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر حکمت مهندس تقدیر</p></div>
<div class="m2"><p>بر ستونی نهاده آن بنیان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خارج او همه عقیق یمن</p></div>
<div class="m2"><p>داخل او مذهب از عقیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر فرازش نهاده کنگره ها</p></div>
<div class="m2"><p>راست چون تاج بر سر شاهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنگره نیست افسر لعل است</p></div>
<div class="m2"><p>بر بیا کنده زر ساو میان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نازکانی همه جدا گانه</p></div>
<div class="m2"><p>نا بسوده نه انسشان نه جان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو اطفال یک بیک دارند</p></div>
<div class="m2"><p>از زر ناب در دهن پستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر یک از نازکی چنانکه خرد</p></div>
<div class="m2"><p>گویدش ناردانه ایست عیان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فرقه فرقه نشسته همزانو</p></div>
<div class="m2"><p>در بر یکدگر خزیده چنان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که سر موی در نمیگنجد</p></div>
<div class="m2"><p>در میان از توافق ایشان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در میانشان ز زر ورق بسته</p></div>
<div class="m2"><p>پرده ها دست قدرت یزدان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ناز کانند لیک سخت دلند</p></div>
<div class="m2"><p>خود چنین اند نازکان جهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر یک از نازکی و لطف چنانک</p></div>
<div class="m2"><p>بلب هر که در بری دندان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بینیش همچو چشم ابن یمین</p></div>
<div class="m2"><p>از گزند زمانه خون افشان</p></div></div>