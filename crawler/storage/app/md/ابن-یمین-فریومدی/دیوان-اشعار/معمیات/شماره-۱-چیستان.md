---
title: >-
    شمارهٔ  ۱ - چیستان
---
# شمارهٔ  ۱ - چیستان

<div class="b" id="bn1"><div class="m1"><p>چار حرفست نام آن دلبر</p></div>
<div class="m2"><p>که درش قبله ایست مردم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول نام و ثانی و ثالث</p></div>
<div class="m2"><p>خمس نصف است و ربع چارم را</p></div></div>