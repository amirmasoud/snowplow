---
title: >-
    شمارهٔ ٣۵٢
---
# شمارهٔ ٣۵٢

<div class="b" id="bn1"><div class="m1"><p>مرا دوستی کو که با دشمنم</p></div>
<div class="m2"><p>بگوید که این نکته میدار یاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که گر دادت اقبال دور فلک</p></div>
<div class="m2"><p>ور ادبار ازو بهره ما فتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپاس از خدای جهان آفرین</p></div>
<div class="m2"><p>که هر شام آمد پس از بامداد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ادباد و اقبال ما و شما</p></div>
<div class="m2"><p>سپهر برین داد روزی بباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو خواهد گذشتن همان و همین</p></div>
<div class="m2"><p>چرا غم خورم من چه باشی تو شاد</p></div></div>