---
title: >-
    شمارهٔ ٧٣٠
---
# شمارهٔ ٧٣٠

<div class="b" id="bn1"><div class="m1"><p>مجلسی داریم الحق جامع هر کام دل</p></div>
<div class="m2"><p>ای دریغا نیست در وی منظر چالاک تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راستی را جمله اسباب عشرت حاصل است</p></div>
<div class="m2"><p>هیچ دیگر در نمی‌باید در او الاک تو</p></div></div>