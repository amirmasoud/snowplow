---
title: >-
    شمارهٔ ۴۶۴
---
# شمارهٔ ۴۶۴

<div class="b" id="bn1"><div class="m1"><p>گر ترک طمع کنی نباشد</p></div>
<div class="m2"><p>ایدل زکست هراس هرگز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی ز خزانه کسی خواه</p></div>
<div class="m2"><p>کو را نبود مکاس هرگز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چیزیکه دهد که شد مقرر</p></div>
<div class="m2"><p>بر سر ننهد سپاس هرگز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سفله مخواه هیچ زنهار</p></div>
<div class="m2"><p>کاطلس نشود پلاس هرگز</p></div></div>