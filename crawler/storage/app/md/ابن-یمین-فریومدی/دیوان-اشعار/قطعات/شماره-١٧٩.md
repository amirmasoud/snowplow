---
title: >-
    شمارهٔ ١٧٩
---
# شمارهٔ ١٧٩

<div class="b" id="bn1"><div class="m1"><p>هر که رنجی کشید و گنج نهاد</p></div>
<div class="m2"><p>بضرورت به دیگران بگذاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نظر میکنی در آخر کار</p></div>
<div class="m2"><p>حاصل گنج غیر رنج نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرم آنکس که همچو ابن یمین</p></div>
<div class="m2"><p>نخورد وقت شام انده چاشت</p></div></div>