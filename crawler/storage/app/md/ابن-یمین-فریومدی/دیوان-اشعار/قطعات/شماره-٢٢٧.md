---
title: >-
    شمارهٔ ٢٢٧
---
# شمارهٔ ٢٢٧

<div class="b" id="bn1"><div class="m1"><p>ای نسیم صبحدم بگذر بخاک درگهی</p></div>
<div class="m2"><p>کز جلالت با سپهر هفتمین پهلو زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش بلقیس سلیمان مرتبت کز خلق او</p></div>
<div class="m2"><p>هر نسیمی طعنه ئی بر نافه اهو زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عرضه دار اول زمین بوس رهی زانو زده</p></div>
<div class="m2"><p>چون رهی را نیست راه آنکه خود زانو زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس بگو ای آنکه عدلت هست تا حدیکه نیست</p></div>
<div class="m2"><p>شاهباز تند را یارا که بر تیهو زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون روا داری که چوپان تو اندر ملک من</p></div>
<div class="m2"><p>ترکتازی آرد و صد چوب بر هندو زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش نگردد خاطر ابن یمین از عدل تو</p></div>
<div class="m2"><p>تا نگوئی چوب یا ساقش پس از یرغوزند</p></div></div>