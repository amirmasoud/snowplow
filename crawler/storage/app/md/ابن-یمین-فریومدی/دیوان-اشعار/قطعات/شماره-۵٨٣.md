---
title: >-
    شمارهٔ ۵٨٣
---
# شمارهٔ ۵٨٣

<div class="b" id="bn1"><div class="m1"><p>ببابا حیدرم باشد توقع</p></div>
<div class="m2"><p>که چون واقف شود از حال زارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرستد ناخنی سوده زمرد</p></div>
<div class="m2"><p>که تا افعی غم را کور دادم</p></div></div>