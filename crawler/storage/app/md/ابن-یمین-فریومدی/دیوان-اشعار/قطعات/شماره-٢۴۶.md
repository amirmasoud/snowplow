---
title: >-
    شمارهٔ ٢۴۶
---
# شمارهٔ ٢۴۶

<div class="b" id="bn1"><div class="m1"><p>برو ایدوست مپندار که اندر همه عمر</p></div>
<div class="m2"><p>از خط و شعر ترا هیچ گره بگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعر و خط نیست متاعی که بهائی آرد</p></div>
<div class="m2"><p>با تو گویم که چرا تا عجبت ننماید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مصطفی از همه کس بود بدان قادرتر</p></div>
<div class="m2"><p>کین و آنرا به بنان و به بیان آراید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیک آن هر دو پسندیده رایش چو نبود</p></div>
<div class="m2"><p>ننگش آمد که بدان دست و دهن آلاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر تو از امت اوئی چه روی راه خلاف</p></div>
<div class="m2"><p>بر مگرد از رهش ار ملک دو کونت باید</p></div></div>