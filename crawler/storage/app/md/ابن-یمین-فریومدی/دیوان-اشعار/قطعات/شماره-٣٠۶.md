---
title: >-
    شمارهٔ ٣٠۶
---
# شمارهٔ ٣٠۶

<div class="b" id="bn1"><div class="m1"><p>شاها کمینه بنده میمون جناب تو</p></div>
<div class="m2"><p>کز کائنات حضرت عالیت را گزید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیرین نکرده از عسل روزگار کام</p></div>
<div class="m2"><p>تا کی زمانه منج صفت خواهدش گزید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وقتست اگر بر این دل رنجور ناتوان</p></div>
<div class="m2"><p>خواهد نسیم گلشن انصاف تو وزید</p></div></div>