---
title: >-
    شمارهٔ ٨٧٧
---
# شمارهٔ ٨٧٧

<div class="b" id="bn1"><div class="m1"><p>مربی چو محمود باشد گرم</p></div>
<div class="m2"><p>چه سنجد بمیزان من عنصری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو سنجر هنر پروری کو مرا</p></div>
<div class="m2"><p>که تا بشکنم رونق انوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بزرگی این هر دو شاعر ز چیست</p></div>
<div class="m2"><p>ز اکرام محمودی و سنجری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و گر نه نه اینست ابن یمین</p></div>
<div class="m2"><p>چه دارند ایشان ازو برتری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دوران چنانم من اکنون که نیست</p></div>
<div class="m2"><p>ز فکر شعیرم سر شاعری</p></div></div>