---
title: >-
    شمارهٔ ٣٢٣
---
# شمارهٔ ٣٢٣

<div class="b" id="bn1"><div class="m1"><p>فلک آنست که یکروز بپایان نبرد</p></div>
<div class="m2"><p>تا دلم را ببلای چو شبی نسپارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز روشن ز شب تیره سیه تر گردد</p></div>
<div class="m2"><p>گر ز حالم رقمی عقل بر او بنگارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرده روزم چو شب تیره ولی صبح دلم</p></div>
<div class="m2"><p>گر همه خود شب یلداست بروزش آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طمعم هست که روزی بدمد صبح امید</p></div>
<div class="m2"><p>وز شب تیره حرمان اثری نگذارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز روشن چو بر آرد ز افق رایت نور</p></div>
<div class="m2"><p>پرچم شب ز سر جمله جهان بر دارد</p></div></div>