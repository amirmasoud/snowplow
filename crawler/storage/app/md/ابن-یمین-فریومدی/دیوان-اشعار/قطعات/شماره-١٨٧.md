---
title: >-
    شمارهٔ ١٨٧
---
# شمارهٔ ١٨٧

<div class="b" id="bn1"><div class="m1"><p>هر کو درین زمانه طلبکار منصبی است</p></div>
<div class="m2"><p>هیچ از نصاب عقل مر او را نصیب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیتی بجز فریب ندارد طریقه ئی</p></div>
<div class="m2"><p>از وی خلاف وعده نمودن غریب نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرور کند بلطف وز پا افکند بعنف</p></div>
<div class="m2"><p>اینست عادتش زوی اینها عجیب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردون نسب نپرسد و هست از حسب ملول</p></div>
<div class="m2"><p>پیروز روز آنکه حسیب و نسیب نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابن یمین گرت به عمل میل خاطرست</p></div>
<div class="m2"><p>اول بدانکه آخر آن جز مهیب نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حال نجیب و آن عمل عزل او نگر</p></div>
<div class="m2"><p>یک واعظت چو حال تباه نجیب نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون عزل مرد هست بجای طلاق زن</p></div>
<div class="m2"><p>خرم کسیکه قاضی و شیخ و خطیب نیست</p></div></div>