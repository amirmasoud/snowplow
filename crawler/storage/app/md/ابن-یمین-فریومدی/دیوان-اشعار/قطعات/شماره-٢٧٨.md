---
title: >-
    شمارهٔ ٢٧٨
---
# شمارهٔ ٢٧٨

<div class="b" id="bn1"><div class="m1"><p>در باب من ز روی حسد یک دو نا شناس</p></div>
<div class="m2"><p>دمها زدند و کوره تزویر تافتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر کارگاه خبث طبیعت که هستشان</p></div>
<div class="m2"><p>یکچند سال حلیت تلبیس بافتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا در شب ضلال بسعی کمان چرخ</p></div>
<div class="m2"><p>موی غرض بناوک حیلت شکافتند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ظنشان چنان فتاد که غمها به من رسد</p></div>
<div class="m2"><p>از بسکه بهر غمز بهر سو شتافتند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رغما لا نفهم همه نیکی بمن رسید</p></div>
<div class="m2"><p>و ایشان جزای فعل بد خویش یافتند</p></div></div>