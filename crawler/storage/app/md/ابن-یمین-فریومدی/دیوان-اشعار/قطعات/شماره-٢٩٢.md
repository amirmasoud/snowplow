---
title: >-
    شمارهٔ ٢٩٢
---
# شمارهٔ ٢٩٢

<div class="b" id="bn1"><div class="m1"><p>رای مخدوم که از عالم غیب آگاهست</p></div>
<div class="m2"><p>حال من بنده کماهی بیقین میداند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من نه آنم که بجز شعر ندارم هنری</p></div>
<div class="m2"><p>عیب من همت والام خود این میداند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم آنکس که با کسیر هنر خامه من</p></div>
<div class="m2"><p>از شبه ساختن در ثمین میداند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیک ازینگونه مضیق که منم کس نبود</p></div>
<div class="m2"><p>وینسخن بی سخن آن داور دین میداند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه کنم عرضه بر او قصه پر غصه چو او</p></div>
<div class="m2"><p>به ز من حال من زار حزین میداند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دولتش باد که او مصحلت ابن یمین</p></div>
<div class="m2"><p>در همه کار به از ابن یمین میداند</p></div></div>