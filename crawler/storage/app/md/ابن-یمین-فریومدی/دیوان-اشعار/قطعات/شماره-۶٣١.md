---
title: >-
    شمارهٔ ۶٣١
---
# شمارهٔ ۶٣١

<div class="b" id="bn1"><div class="m1"><p>هر بلائی که میشود واقع</p></div>
<div class="m2"><p>درمیان خلایق عالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نکو بنگری طمع باشد</p></div>
<div class="m2"><p>منشأ آن بلا ز بیش و زکم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نبودی طمع نیفتادی</p></div>
<div class="m2"><p>از بهشت برین برون آدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که نقش طمع ز لوح ضمیر</p></div>
<div class="m2"><p>بسترد وارهد ز محنت و غم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از طمع دور باش ابن یمین</p></div>
<div class="m2"><p>گر دلی بایدت خوش و خرم</p></div></div>