---
title: >-
    شمارهٔ ٨٩
---
# شمارهٔ ٨٩

<div class="b" id="bn1"><div class="m1"><p>بردم بنزد خواجه شکایت ز رنج فقر</p></div>
<div class="m2"><p>گفتم دوای این بکف همت شماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر حال من چو یافت وقوف تمام گفت</p></div>
<div class="m2"><p>زین رنج غم مخور که دوایش بدست ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از من گرفت باز طعام و شراب و گفت</p></div>
<div class="m2"><p>اول علاج مردم بیمار احتماست</p></div></div>