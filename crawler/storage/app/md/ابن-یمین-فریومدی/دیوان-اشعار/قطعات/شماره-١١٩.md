---
title: >-
    شمارهٔ ١١٩
---
# شمارهٔ ١١٩

<div class="b" id="bn1"><div class="m1"><p>رزق مقسوم و وقت معلومست</p></div>
<div class="m2"><p>ساعتی بیش و لحظه ئی پس نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر یکی را مقررست که چیست</p></div>
<div class="m2"><p>چه توان کرد اگر ترا بس نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وانکه جفت مراد خود باشد</p></div>
<div class="m2"><p>زیر طاق سپهر اطلس نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر قناعت کنی بخانه تنگ</p></div>
<div class="m2"><p>کمتر از طارم مقرنس نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لذتی کز شراب خرسندیست</p></div>
<div class="m2"><p>در شفا خانه مسدس نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بقدم کوش تا بکام رسی</p></div>
<div class="m2"><p>مرد وامانده کاروان رس نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم ز خود جوی هر چه میجوئی</p></div>
<div class="m2"><p>که بغیر از تو در جهان کس نیست</p></div></div>