---
title: >-
    شمارهٔ ٧٨۵
---
# شمارهٔ ٧٨۵

<div class="b" id="bn1"><div class="m1"><p>ای خردمند اگر همی خواهی</p></div>
<div class="m2"><p>که شوی شهره در نکو کاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهد کن تا غلام و خدمتکار</p></div>
<div class="m2"><p>بیش از ابناء جنس خود داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زانکه روزی یک بیک ایزد</p></div>
<div class="m2"><p>میدهد در کمی و بسیاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نان ز دیوان غیرشان مجراست</p></div>
<div class="m2"><p>وز تو مشهور آدمی ساری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میدهندت بنان و جامه خویش</p></div>
<div class="m2"><p>در مهمات این جهان یاری</p></div></div>