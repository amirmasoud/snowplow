---
title: >-
    شمارهٔ ١۴۶
---
# شمارهٔ ١۴۶

<div class="b" id="bn1"><div class="m1"><p>گردش گردون دون آزاده ها را خسته کرد</p></div>
<div class="m2"><p>کو دل ازاده ئی کز دست او مجروح نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عنا تا کی توان بودن بامید بهی</p></div>
<div class="m2"><p>گر کسی را صبر ایوبست عمر نوح نیست</p></div></div>