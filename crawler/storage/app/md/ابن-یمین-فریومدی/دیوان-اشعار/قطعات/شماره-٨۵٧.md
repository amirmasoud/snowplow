---
title: >-
    شمارهٔ ٨۵٧
---
# شمارهٔ ٨۵٧

<div class="b" id="bn1"><div class="m1"><p>قطب سپهر مکرمت ای یافته دلم</p></div>
<div class="m2"><p>از جود تو چو ذره ز خور تاب زندگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر من که مرده بودم از احداث روزگار</p></div>
<div class="m2"><p>مهرت گشاد بار دگر باب زندگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خشکسال مکرمت ابر سخات زد</p></div>
<div class="m2"><p>بر تاب آتش جگرم آب زندگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز آر از آنشراب کهن شربتی بجام</p></div>
<div class="m2"><p>کآنست رکن اعظم اسباب زندگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر اهتمام لطف تو نبود گسسته دان</p></div>
<div class="m2"><p>از خیمه وجود من اطناب زندگی</p></div></div>