---
title: >-
    شمارهٔ ٢٨
---
# شمارهٔ ٢٨

<div class="b" id="bn1"><div class="m1"><p>علاء دولت و دین آصف سلیمان فر</p></div>
<div class="m2"><p>زهی جناب تو ارباب فضل را مأوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز حادثات کسی کالتجا بجاه تو کرد</p></div>
<div class="m2"><p>درآمد از تف دوزخ بسایه طوبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توئیکه گوهر مدحت بنظم ابن یمین</p></div>
<div class="m2"><p>ز راه مرتبه گشتست زیور دنیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم که نام تو در هر هنر که نام برند</p></div>
<div class="m2"><p>کناره کرد بتابید شعرم از شعرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا بپرور و نام نکو بدست آور</p></div>
<div class="m2"><p>که نام نیک ز هرچ آوری بود اولی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن جدا کند اندر زمانه نیک از بد</p></div>
<div class="m2"><p>نگاه کن که چه خوش گفت صابر اینمعنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه پایه ذکر که از شعر منتشر گشتست</p></div>
<div class="m2"><p>کریم را بمدیح ولئیم را بهجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترا خدای بحمدالله آن کرم دادست</p></div>
<div class="m2"><p>که منشی فلکی مدح تو کند انشا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بقای عمر تو بادا که خود مدایح تو</p></div>
<div class="m2"><p>همیکند کرمت بر سخنوران املا</p></div></div>