---
title: >-
    شمارهٔ ۵١٠
---
# شمارهٔ ۵١٠

<div class="b" id="bn1"><div class="m1"><p>ای دل ز غم منال که از گردش زمان</p></div>
<div class="m2"><p>تنها تو نیستی به جفای زمانه خاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاصیتی است مردم این روزگار را</p></div>
<div class="m2"><p>نتوان به هیچ روی شدن منکر خواص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر فی‌المثل هزار نکویی کنی به خلق</p></div>
<div class="m2"><p>زیشان به جز بدی نتوان یافتن قصاص</p></div></div>