---
title: >-
    شمارهٔ ٧١٨
---
# شمارهٔ ٧١٨

<div class="b" id="bn1"><div class="m1"><p>بر فلک دل منه ار بوی خرد یافته‌ای</p></div>
<div class="m2"><p>که نبینی به وجود آمده ناحق‌تر از او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاقل امروز کسی را نهد این دون‌پرور</p></div>
<div class="m2"><p>که نباشد به جهان هیچکس احمق‌تر از او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاجرم هرکه بود رونق عقلش کمتر</p></div>
<div class="m2"><p>هیچکس را نبود کار به‌رونق‌تر از او</p></div></div>