---
title: >-
    شمارهٔ ۴۶٠
---
# شمارهٔ ۴۶٠

<div class="b" id="bn1"><div class="m1"><p>ای باد صبحدم گذری کن ز راه لطف</p></div>
<div class="m2"><p>بر حضرتی چو کعبه اسلامیان عزیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یعنی جناب سرور گردنکشان عهد</p></div>
<div class="m2"><p>آنکو بنزد خلق جهان چون روان عزیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک درش ببوس بتعظیم و پس بگوی</p></div>
<div class="m2"><p>گر باشدت مجال سخن پیش آن عزیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کابن یمین فروخت بوجه معاش خویش</p></div>
<div class="m2"><p>املاک و هر چه بودش در خان و مان عزیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اکنون نه ملک ماند و نه یکجو بهای ملک</p></div>
<div class="m2"><p>وین خوش که برقرا بماندست نان عزیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نانی در ایندیار بخواری همی خورد</p></div>
<div class="m2"><p>ای کرده کردگار ترا در جهان عزیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لطفی کن و جواز دهش تا از اینمقام</p></div>
<div class="m2"><p>جائی رود که نان نبود همچو جان عزیز</p></div></div>