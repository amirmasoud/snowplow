---
title: >-
    شمارهٔ ٧١٢
---
# شمارهٔ ٧١٢

<div class="b" id="bn1"><div class="m1"><p>یکروز بپرسید منوچهر ز سالار</p></div>
<div class="m2"><p>کاندر همه عالم چه به ای سام نریمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او داد جوابش که درینعالم فانی</p></div>
<div class="m2"><p>گفتار حکیمان به و رفتار فهیمان</p></div></div>