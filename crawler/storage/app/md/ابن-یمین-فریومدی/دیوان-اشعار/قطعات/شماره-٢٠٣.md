---
title: >-
    شمارهٔ ٢٠٣
---
# شمارهٔ ٢٠٣

<div class="b" id="bn1"><div class="m1"><p>ایدل اگر روزی دو سه دنیا نباشد بر مراد</p></div>
<div class="m2"><p>خوش باش کاحوال جهان زانسانکه آید بگذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار جهان برقی بود بر تیرگی رخشان شده</p></div>
<div class="m2"><p>خوش در نظر آید مرا چون رخ نماید بگذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذار گیتی را و زو بگذر چو دانی این قدر</p></div>
<div class="m2"><p>کز ما در آنکو در جهان روزی بزاید بگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مائیم در دست غمش با نیم جانی غرق خون</p></div>
<div class="m2"><p>ایکاشکی بار غمش چون جان رباید بگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر ما چو دور خرمی بگذشت و آمد وقت غم</p></div>
<div class="m2"><p>دل شاد باید داشتن کان هم نپاید بگذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیرت بگردان از بدی وز رنج دهر آسوده شو</p></div>
<div class="m2"><p>کز مردم نیکو سیر هر چ آن نشاید بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تنگنای آرزو مسکین دل ابن یمین</p></div>
<div class="m2"><p>گر حق بخرسندی دری بر وی گشاید بگذرد</p></div></div>