---
title: >-
    شمارهٔ ٢٧٠
---
# شمارهٔ ٢٧٠

<div class="b" id="bn1"><div class="m1"><p>چون جامه چرمین شمرم صحبت نادان</p></div>
<div class="m2"><p>زیرا که گران باشد و تن گرم ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از صحبت نادان بترت نیز بگویم</p></div>
<div class="m2"><p>خویشی که توانگر شد و آزرم ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین هر دو بتر دان تو شهی را که در اقلیم</p></div>
<div class="m2"><p>با خنجر خونریز دل نرم ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین هر سه بتر نیز بگویم که چه باشد</p></div>
<div class="m2"><p>پیری که جوانی کند و شرم ندارد</p></div></div>