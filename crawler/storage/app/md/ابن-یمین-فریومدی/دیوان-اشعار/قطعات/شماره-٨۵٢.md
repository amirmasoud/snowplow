---
title: >-
    شمارهٔ ٨۵٢
---
# شمارهٔ ٨۵٢

<div class="b" id="bn1"><div class="m1"><p>عاقل نکند بهیچ روئی</p></div>
<div class="m2"><p>بیمنفعتی زیارت حی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نزدیک خرد پسند ناید</p></div>
<div class="m2"><p>هر قول که فعل نیست با وی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کو نشود بوصل تو شاد</p></div>
<div class="m2"><p>گر گویم ازو ببر مگو کی</p></div></div>