---
title: >-
    شمارهٔ ٣٧٢
---
# شمارهٔ ٣٧٢

<div class="b" id="bn1"><div class="m1"><p>هر که انبای جنس را خواهد</p></div>
<div class="m2"><p>که سر و سرور خودش دانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در فتوت گرش بود قدمی</p></div>
<div class="m2"><p>همه تاج سر خودش دانند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نباشد ز کهتران بهتر</p></div>
<div class="m2"><p>پس چرا مهتر خودش دانند</p></div></div>