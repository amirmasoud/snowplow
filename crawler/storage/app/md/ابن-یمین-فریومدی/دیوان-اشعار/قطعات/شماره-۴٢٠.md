---
title: >-
    شمارهٔ ۴٢٠
---
# شمارهٔ ۴٢٠

<div class="b" id="bn1"><div class="m1"><p>خیز ای نسیم باد صبا از طریق لطف</p></div>
<div class="m2"><p>بر درگه سپهبد ما ز ندران گذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول سلام من برسان بعد از آن بگو</p></div>
<div class="m2"><p>کای سرور زمانه و سردار نامور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کارت که شد گشاده بتوفیق روزگار</p></div>
<div class="m2"><p>ما را همه خیال چنانست کان مگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعد از قضای ایزد و تأیید بخت نیک</p></div>
<div class="m2"><p>بود اتفاق صحبت ما را در آن اثر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوع دگر سپهبد اگر میبرد گمان</p></div>
<div class="m2"><p>ور ناورد حقیقت اینحال در نظر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آری اگر بکام تو یکره نرفت کار</p></div>
<div class="m2"><p>دارم توقع از کرم و لطف دادگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاحوال تو چنانکه تو خواهی چنان شود</p></div>
<div class="m2"><p>بر موجب اراده ما صد ره دگر</p></div></div>