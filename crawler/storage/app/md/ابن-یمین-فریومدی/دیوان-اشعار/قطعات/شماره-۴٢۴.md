---
title: >-
    شمارهٔ ۴٢۴
---
# شمارهٔ ۴٢۴

<div class="b" id="bn1"><div class="m1"><p>روزی که فتوحی رسد از عالم غیبت</p></div>
<div class="m2"><p>آن روز مبارک شمر و فال نکو گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور به طلبی عمر گرانمایه مفرسای</p></div>
<div class="m2"><p>از کهنه گرت کار بر آید کم نو گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مسکن خویش ار نه بکامست معاشت</p></div>
<div class="m2"><p>بار سفر آنجا که دلت خواست فرو گیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آنکس که دل غمزده ات شاد نگردد</p></div>
<div class="m2"><p>گر خود بمثل جان تو باشد کم او گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و از ابن یمین اینسخن از لطف معانی</p></div>
<div class="m2"><p>بر لوح دلت ثبت کن و عادت و خو گیر</p></div></div>