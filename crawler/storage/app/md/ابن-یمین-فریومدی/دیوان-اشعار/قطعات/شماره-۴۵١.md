---
title: >-
    شمارهٔ ۴۵١
---
# شمارهٔ ۴۵١

<div class="b" id="bn1"><div class="m1"><p>مجلس نوئین اعظم خسرو جمشید فر</p></div>
<div class="m2"><p>سرور گیتی ایسن قتلغ امیر بحر و بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست چون باغ ارم از گلرخان سرو قد</p></div>
<div class="m2"><p>هست چون خلد برین از دلبران سیمبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجلسی زینسان و صاحب مجلسی زآنسان که اوست</p></div>
<div class="m2"><p>هر که بیند روضه رضوانش آید در نظر</p></div></div>