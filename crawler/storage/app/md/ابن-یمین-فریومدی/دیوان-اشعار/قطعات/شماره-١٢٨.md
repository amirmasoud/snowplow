---
title: >-
    شمارهٔ ١٢٨
---
# شمارهٔ ١٢٨

<div class="b" id="bn1"><div class="m1"><p>سود دنیا و دین اگر خواهی</p></div>
<div class="m2"><p>مایه هر دوشان نکو کاریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راحت بندگان حق جستن</p></div>
<div class="m2"><p>عین تقوی و زهد و دینداریست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر در خلد را کلیدی هست</p></div>
<div class="m2"><p>بیش بخشیدن و کم آزاریست</p></div></div>