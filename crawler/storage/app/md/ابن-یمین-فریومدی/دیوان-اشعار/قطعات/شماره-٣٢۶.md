---
title: >-
    شمارهٔ ٣٢۶
---
# شمارهٔ ٣٢۶

<div class="b" id="bn1"><div class="m1"><p>که داند که در وحدت و انزوا</p></div>
<div class="m2"><p>چه آسایش جان بمن میرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشاد است بر من ریاضی کز آن</p></div>
<div class="m2"><p>خرد را نسیم سمن میرسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دمادم لطیفی دگر نزد من</p></div>
<div class="m2"><p>ز آزادگان ز من میرسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسد هر زمانم بدل دلبری</p></div>
<div class="m2"><p>چو سروی که سوی چمن میرسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر او زیور عقده ای گران</p></div>
<div class="m2"><p>ز عمان و ملک یمن میرسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بقیمت بر اهل دانش چنان</p></div>
<div class="m2"><p>که نقد روانش ثمن میرسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معاشی بمن ز آسیای زبر</p></div>
<div class="m2"><p>بلا زحمت کیل و من میرسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه من بر کسی منتی مینهم</p></div>
<div class="m2"><p>نه بر من مشقت بمن میرسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد ابن یمین فارغ از خلق از آن</p></div>
<div class="m2"><p>که رزقش چو سلوی و من میرسد</p></div></div>