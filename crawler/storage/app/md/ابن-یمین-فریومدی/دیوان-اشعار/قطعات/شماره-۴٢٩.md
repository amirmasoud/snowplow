---
title: >-
    شمارهٔ ۴٢٩
---
# شمارهٔ ۴٢٩

<div class="b" id="bn1"><div class="m1"><p>سر افاضل عالم امام عبدالحی</p></div>
<div class="m2"><p>ز هی بخامه گهر پاشتر ز ابر مطیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز اهل فضل توئی آنکه در مراتب شعر</p></div>
<div class="m2"><p>رسیده ئی بکمال و گذشته ئی ز اثیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توئی که خامه زر پیکرت بغواصی</p></div>
<div class="m2"><p>میان ببست و برآورد در ز لجه قیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپهر اگر چه هزاران هزار دیده گشاد</p></div>
<div class="m2"><p>بجز بدیده احوال ترا ندید نظیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز غیرت سخن خوشترت ز شیر و شکر</p></div>
<div class="m2"><p>شود گداخته حاسد چو شکر اندر شیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنرورا بادای حقوق و مدحت تو</p></div>
<div class="m2"><p>ضمیر ابن یمین گر همی کند تقصیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بیش ازین نرسد خاطر مشوش او</p></div>
<div class="m2"><p>تو از بزرگی خود در گذار و خورده مگیر</p></div></div>