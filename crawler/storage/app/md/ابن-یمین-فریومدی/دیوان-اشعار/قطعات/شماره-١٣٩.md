---
title: >-
    شمارهٔ ١٣٩
---
# شمارهٔ ١٣٩

<div class="b" id="bn1"><div class="m1"><p>عماد دولت و دین ای وزیر زاده ملک</p></div>
<div class="m2"><p>چه جای زاده که این کار پیشه و فن تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلیل صحت نفس و طهارت نسبت</p></div>
<div class="m2"><p>غرارت شرف نفس و خلق احسن تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عطارد ار چه که باشد به زیرکی مشهور</p></div>
<div class="m2"><p>ولی چو با تو کنندش قیاس کودن تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه سوسن آزاد ده زبان باشد</p></div>
<div class="m2"><p>ولی چو بنده بوقت ثنات الکن تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدان که تا کمر بندگیت می بندد</p></div>
<div class="m2"><p>سعادت دو جهانی مقیم مسکن تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمینه بنده دیرینه ابن یمین</p></div>
<div class="m2"><p>که در فنون هنر خوشه چین خرمن تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر اینجریده گر اثبات کرد بیتی چند</p></div>
<div class="m2"><p>چو حکم تست بد و نیک آن بگردن تست</p></div></div>