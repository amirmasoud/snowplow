---
title: >-
    شمارهٔ ٣٨٠
---
# شمارهٔ ٣٨٠

<div class="b" id="bn1"><div class="m1"><p>هر که دل بر اصابت خیرات</p></div>
<div class="m2"><p>ببد و نیک مطمئن نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آنکه در طبع خویشتن چو ضمیر</p></div>
<div class="m2"><p>مهر اصحاب مستکن نکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نمیرد ببایدش کشتن</p></div>
<div class="m2"><p>تا هوای جهان عفن نکند</p></div></div>