---
title: >-
    شمارهٔ ٩٨
---
# شمارهٔ ٩٨

<div class="b" id="bn1"><div class="m1"><p>جهان لطف و کرم تاج ملک خواجه علی</p></div>
<div class="m2"><p>توئی که کس ز تو شد هر که در زمانه کس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبیعتی است در احیای مکرمات ترا</p></div>
<div class="m2"><p>که هست خاصیتش آنکه عیسوی نفس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجز خیال کسی شبروی نیارد کرد</p></div>
<div class="m2"><p>در آن دیار که سر پاس پاس تو عسس است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر مهم که نهد رای تو قدم در پیش</p></div>
<div class="m2"><p>هزار منزل ازو آفتاب باز پس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن سرای که وردش ثنای تو نبود</p></div>
<div class="m2"><p>میان اهل سخن هرزه لای چون جرس است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا تو آنچه بتشریف داده ئی همه عمر</p></div>
<div class="m2"><p>ز بهر فخر بر ابنای روزگار بس است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولیک طوطی طبعم که طوطی ملکوت</p></div>
<div class="m2"><p>بجنب او چو بنزد همای خرمگس است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آنکه بال و پری نیستش مناسب حال</p></div>
<div class="m2"><p>فتاده اکثر اوقات دربن قفس است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببخش بال و پری از مثال ترخانیش</p></div>
<div class="m2"><p>که در هوای تو پرواز کردنش هوس است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنون که دسترست هست دستگیرش باش</p></div>
<div class="m2"><p>مده ز دست مر اینوقت را که دسترس است</p></div></div>