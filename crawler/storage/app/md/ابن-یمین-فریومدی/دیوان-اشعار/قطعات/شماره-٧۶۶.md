---
title: >-
    شمارهٔ ٧۶۶
---
# شمارهٔ ٧۶۶

<div class="b" id="bn1"><div class="m1"><p>فخر دین و ملک معنی ای ز نور رای تو</p></div>
<div class="m2"><p>رهروان عالم علوی هدایت یافته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل اول دست تدبیر ترا در کار ملک</p></div>
<div class="m2"><p>چون ید بیضای موسی با کفایت یافته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مفتی رأی جهان آرای تو در مشکلات</p></div>
<div class="m2"><p>هر جوابی را که گفته صد روایت یافته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صاحبا گویا نئی آگه که هست ابن یمین</p></div>
<div class="m2"><p>هر دم از دوران گردون صد نکایت یافته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر دل پر درد خویش از حادثات روزگار</p></div>
<div class="m2"><p>دور غم را چون تسلسل بی نهایت یافته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون زتاب آفتاب حادثات ایمن شدست</p></div>
<div class="m2"><p>آنکه هست از سایه لطفت حمایت یافته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس چرا باید که باشد با نکو ذاتی تو</p></div>
<div class="m2"><p>بنده بد حالی خود بیحد و غایت یافته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سرم دست عنایت در حوادث بر مدار</p></div>
<div class="m2"><p>ای ز تو اهل هنر دائم عنایت یافته</p></div></div>