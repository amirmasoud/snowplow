---
title: >-
    شمارهٔ ١۵٢
---
# شمارهٔ ١۵٢

<div class="b" id="bn1"><div class="m1"><p>گر چو سندان بود ترا دندان</p></div>
<div class="m2"><p>چون کهن شد ز دردمندانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جوانی مرا چو سندان بود</p></div>
<div class="m2"><p>آنچه دندان و وزن دندانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وین زمانم که نوبت پیری است</p></div>
<div class="m2"><p>ضعف دندان و وهن حمدانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر یکی ناتوان شود چه عجب</p></div>
<div class="m2"><p>چند کارم کند نه سندانست</p></div></div>