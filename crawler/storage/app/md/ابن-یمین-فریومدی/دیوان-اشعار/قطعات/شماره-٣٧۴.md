---
title: >-
    شمارهٔ ٣٧۴
---
# شمارهٔ ٣٧۴

<div class="b" id="bn1"><div class="m1"><p>هر کرا با خود مصاحب میکنی</p></div>
<div class="m2"><p>بنگرش تا خویشتن چون میزید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بقدر حال سامانیش هست</p></div>
<div class="m2"><p>میل او کن کو بقانون میزید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور نباشد رونقی در کار او</p></div>
<div class="m2"><p>ز آنکه حد-اوست افزون میزید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سالها گر تربیت خواهیش کرد</p></div>
<div class="m2"><p>همچنان باشد که اکنون میزید</p></div></div>