---
title: >-
    شمارهٔ ۴۶٢
---
# شمارهٔ ۴۶٢

<div class="b" id="bn1"><div class="m1"><p>اگر تنعم و دولت دهد بپوش و بخور</p></div>
<div class="m2"><p>بدوستانت بده آنچه از تو ماند باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر مخالف طبع تو نغمه ئی سازد</p></div>
<div class="m2"><p>مرنج و نیز مرنجان و جان و دل مگداز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که روزگار حرونست و ناگهان بر مد</p></div>
<div class="m2"><p>نه مال ماند و منصب نه جاه ماند و ناز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنانکه گفته درآنقطعه آنحکیم خرد</p></div>
<div class="m2"><p>زمانه با تو نسازد تو با زمانه بساز</p></div></div>