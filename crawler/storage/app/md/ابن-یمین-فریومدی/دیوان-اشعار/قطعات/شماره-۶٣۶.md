---
title: >-
    شمارهٔ ۶٣۶
---
# شمارهٔ ۶٣۶

<div class="b" id="bn1"><div class="m1"><p>یعلم الله که چون شباب گذشت</p></div>
<div class="m2"><p>ذات خود را مسن نمیخواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاقلان از من وز من گفتند</p></div>
<div class="m2"><p>خویشتن را ز من نمیخواهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهوای لطیف خواهم رفت</p></div>
<div class="m2"><p>کین هوای عفن نمیخواهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میل استبرقست و اکسونم</p></div>
<div class="m2"><p>این پلاس خشن نمیخواهم</p></div></div>