---
title: >-
    شمارهٔ ٨٣٢
---
# شمارهٔ ٨٣٢

<div class="b" id="bn1"><div class="m1"><p>خداوندا بر این عالیجنابت</p></div>
<div class="m2"><p>کزو دارد فلک صد شرمساری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فراوان رنج بی راحت کشیدم</p></div>
<div class="m2"><p>کنون سیر آمدم زین هرزه کاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخواهم کرد ازین پس عمر ضایع</p></div>
<div class="m2"><p>کرم باشد گرم معذور داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحمدالله ندارم مال و جاهی</p></div>
<div class="m2"><p>که بستانی بغیر من سپاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو من بر بینوائی دل نهادم</p></div>
<div class="m2"><p>چرا باید تحمل کرد خواری</p></div></div>