---
title: >-
    شمارهٔ ٧۶٠
---
# شمارهٔ ٧۶٠

<div class="b" id="bn1"><div class="m1"><p>شرف دولت و دین زبده اصحاب کرم</p></div>
<div class="m2"><p>ای بذاتت هنر و فضل تولا کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابر کلک گهر افشان تو مانند صدف</p></div>
<div class="m2"><p>دهن آز پر از لؤلؤ لالا کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم بد دور ز خط تو که هر نقطه او</p></div>
<div class="m2"><p>سطح کافور پر از عنبر سارا کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دی ز یاران که چو بختند مقیم در تو</p></div>
<div class="m2"><p>بتولای تو از غیر تبرا کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ورقی چند بمن داد عزیزی و برآن</p></div>
<div class="m2"><p>رأی عالیت اشاره بسوی ما کرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که ز اشعار خود اینچند ورق بیضا را</p></div>
<div class="m2"><p>دارم امید بتو نامه سودا کرده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کردم اثبات بفرمان تو ابیات برو</p></div>
<div class="m2"><p>ز آنچه زین پیشترک داشتم انشا کرده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بجناب تو فرستادم و عقلم میگفت</p></div>
<div class="m2"><p>کای تو بسیار از این ساده دلیها کرده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مثلت هست چو تاجر که رود از پی سود</p></div>
<div class="m2"><p>بسوی بصره و سرمایه ز خرما کرده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو فرشته صفتی ز ابن یمین در گذران</p></div>
<div class="m2"><p>کلماتی که بر او دیو وی املا کرده</p></div></div>