---
title: >-
    شمارهٔ ٣٩٨
---
# شمارهٔ ٣٩٨

<div class="b" id="bn1"><div class="m1"><p>ای نسیم صبحدم ز آنجا که لطف تست خیز</p></div>
<div class="m2"><p>رنجه شو بگذر بدر گاه شه گردون سریر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاج ملک و دین علی کز بدو فطرت آمدست</p></div>
<div class="m2"><p>همدمش بخت جوان و رهنمایش رأی پیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خسرو جمشید فر شاهی که از آغاز کار</p></div>
<div class="m2"><p>دادش ایزد هر چه آید در تصور جز نظیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو منم آن کز بلندی در مدیحت شعر من</p></div>
<div class="m2"><p>بر بیاض مه بمشک سوده بنوشته است تیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نام نیکت چون ز شعری بگذرانیدم بشعر</p></div>
<div class="m2"><p>چون روا داری که فکرم وقف باشد بر شعیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نیم در بند افزونی طلب کردن ولیک</p></div>
<div class="m2"><p>رأی شه دادند که باشد از کفافی نا گزیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ترا بر هر چه خواهی داد ایزد دسترس</p></div>
<div class="m2"><p>پایمردی کن بلطف ابن یمین را دست گیر</p></div></div>