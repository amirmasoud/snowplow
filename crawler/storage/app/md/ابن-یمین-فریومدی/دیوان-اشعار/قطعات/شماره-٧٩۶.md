---
title: >-
    شمارهٔ ٧٩۶
---
# شمارهٔ ٧٩۶

<div class="b" id="bn1"><div class="m1"><p>اگر بروی ترش کار فقر راست شدی</p></div>
<div class="m2"><p>کدوی سرکه بدی با یزید بسطامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و گر بخرقه ازرق تمام گشتی کار</p></div>
<div class="m2"><p>تغار نیل بدی شیخ احمد جامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و گر برقص کسی شهره و علم بودی</p></div>
<div class="m2"><p>امام شهر شدی خرس در نکو نامی</p></div></div>