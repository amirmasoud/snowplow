---
title: >-
    شمارهٔ ٧۶١
---
# شمارهٔ ٧۶١

<div class="b" id="bn1"><div class="m1"><p>صاحب عادل جلال ملک و دین دستور شرق</p></div>
<div class="m2"><p>ای ز رفعت خاک پایت افسر خورشید و ماه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفثه المصدور خواهم عرضه کردن پیش تو</p></div>
<div class="m2"><p>گر چه داند رای صاحب حال من بی اشتباه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر بساط حضرتت چون رخ نهاد ابن یمین</p></div>
<div class="m2"><p>داد بختش همچو فرزین جای در پهلوی شاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از قضا اسبش چو خر اندر خلاب عجز ماند</p></div>
<div class="m2"><p>ای گرفته قدرتت درماندگانرا در پناه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راه تا مقصد بپای پیل صد فرسنگ هست</p></div>
<div class="m2"><p>چون کند اکنون پیاده در رکابت قطع راه</p></div></div>