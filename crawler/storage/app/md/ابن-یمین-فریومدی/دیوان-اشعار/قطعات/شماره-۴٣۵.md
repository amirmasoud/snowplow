---
title: >-
    شمارهٔ ۴٣۵
---
# شمارهٔ ۴٣۵

<div class="b" id="bn1"><div class="m1"><p>شرف مرد بعلمست و کرامت بسجود</p></div>
<div class="m2"><p>نیست بیعلم و عمل هیچکسی را مقدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کراهست حسب گر نسبی نیست چه باک</p></div>
<div class="m2"><p>بیهنر را چه شرف از نسب خویش و تبار</p></div></div>