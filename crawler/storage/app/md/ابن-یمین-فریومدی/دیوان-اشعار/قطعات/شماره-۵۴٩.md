---
title: >-
    شمارهٔ ۵۴٩
---
# شمارهٔ ۵۴٩

<div class="b" id="bn1"><div class="m1"><p>تهتک در سخن گفتن زیانست</p></div>
<div class="m2"><p>تأمل کن تامل کن تامل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بکار بد چه کوشی تا توانی</p></div>
<div class="m2"><p>تعلل کن تعلل کن تعلل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر کاریکه خواهی کردن اول</p></div>
<div class="m2"><p>تعقل کن تعقل کن تعقل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مکن ایجان من از کس شکایت</p></div>
<div class="m2"><p>توکل کن توکل کن توکل</p></div></div>