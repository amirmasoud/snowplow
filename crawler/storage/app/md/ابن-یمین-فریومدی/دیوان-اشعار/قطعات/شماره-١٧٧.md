---
title: >-
    شمارهٔ ١٧٧
---
# شمارهٔ ١٧٧

<div class="b" id="bn1"><div class="m1"><p>وزیر شاه نشان ای یگانه دو جهان</p></div>
<div class="m2"><p>توئی که ذات تو مقصود از سه مولودست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چهار ماه بود تا به پنجگانه حواس</p></div>
<div class="m2"><p>ز شش جهه به دل خسته ام که موعودست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز هفتمین درک انتطار برهانم</p></div>
<div class="m2"><p>امید هشت بهشت ار تراز معبودست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که زیر نه فلک ده دله بصد اخلاص</p></div>
<div class="m2"><p>امیدوار بجان بنده تو محمودست</p></div></div>