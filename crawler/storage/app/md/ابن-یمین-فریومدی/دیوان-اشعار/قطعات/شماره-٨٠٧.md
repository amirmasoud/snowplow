---
title: >-
    شمارهٔ ٨٠٧
---
# شمارهٔ ٨٠٧

<div class="b" id="bn1"><div class="m1"><p>بیادگار من ای یار اگر نگهداری</p></div>
<div class="m2"><p>یکی لطیفه نویسم ز غایت یاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمانه در گذرست و اجل ز پی تازان</p></div>
<div class="m2"><p>بهوش باش که فرصت ز دست نگذاری</p></div></div>