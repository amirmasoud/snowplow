---
title: >-
    شمارهٔ ٧۴١
---
# شمارهٔ ٧۴١

<div class="b" id="bn1"><div class="m1"><p>ای فلکقدری که دایم بر بساط حضرتت</p></div>
<div class="m2"><p>خسروان عهد را چون بندگان ساید جباه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باد زیر پای پیل حادثات افکنده سر</p></div>
<div class="m2"><p>هر که طبعش با تو کژ دارد چو فرزین رسم و راه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنده را در وجه خرجی اسب و استر صرف شد</p></div>
<div class="m2"><p>اینزمان چون وقت رفتن آمدش زین بارگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خودتو انصافم بده آخر روا باشد که من</p></div>
<div class="m2"><p>رخ براه آرم پیاده میروم از پیش شاه</p></div></div>