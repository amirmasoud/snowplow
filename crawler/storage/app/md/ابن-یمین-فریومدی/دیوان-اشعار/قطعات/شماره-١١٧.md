---
title: >-
    شمارهٔ ١١٧
---
# شمارهٔ ١١٧

<div class="b" id="bn1"><div class="m1"><p>دی گفت دوستی که مرا موی رو سفید</p></div>
<div class="m2"><p>بس زود گشت گر چه که آنهم تباه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیکن هنوز موسم آن نیستت برو</p></div>
<div class="m2"><p>مور اخضاب کن که بشرع این گناه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دادم جواب و گفتمش ای آنکه در جهان</p></div>
<div class="m2"><p>از دوستان یکی چو توام نیکخواه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانی سر خضاب چرا نیستم از آنک</p></div>
<div class="m2"><p>باز سفید کم ز کلاغ سیاه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند شام موسم آرام و راحتست</p></div>
<div class="m2"><p>میدان یقین که خوبتر از صبحگاه نیست</p></div></div>