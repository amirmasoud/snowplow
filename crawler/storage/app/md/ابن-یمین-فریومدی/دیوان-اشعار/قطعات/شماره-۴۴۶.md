---
title: >-
    شمارهٔ ۴۴۶
---
# شمارهٔ ۴۴۶

<div class="b" id="bn1"><div class="m1"><p>کسی خوش بر آید در این روزگار</p></div>
<div class="m2"><p>که باشد بدستش یکی از سه کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخستین حکومت که آن منصبی است</p></div>
<div class="m2"><p>کز آنجا گشاید بسی کار و بار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوم کار سرهنگ تندست و تیز</p></div>
<div class="m2"><p>که یکسان بود نزد او مور و مار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر کار از آن هر سه خواهند گیست</p></div>
<div class="m2"><p>که خواهنده نندیشد از ننگ و عار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز هر سو بدست آورد لوت و بوت</p></div>
<div class="m2"><p>بشادی بر آرد زانده دمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو ابن یمین زین سه فرقه نبود</p></div>
<div class="m2"><p>نشد لاجرم حاصل او را یسار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سستی اصحاب دولت کنون</p></div>
<div class="m2"><p>بسختی بسر میبرد روزگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپهرا کفافی نخواهیش داد</p></div>
<div class="m2"><p>ز هی بیحیائی ز خود شرم دار</p></div></div>