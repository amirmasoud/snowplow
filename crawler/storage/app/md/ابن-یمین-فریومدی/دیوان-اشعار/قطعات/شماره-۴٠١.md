---
title: >-
    شمارهٔ ۴٠١
---
# شمارهٔ ۴٠١

<div class="b" id="bn1"><div class="m1"><p>ایدل ازینجهان دلازار در گذر</p></div>
<div class="m2"><p>وز تنگنای گنبد دوار در گذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار جهان نه لایق اهل بصیرتست</p></div>
<div class="m2"><p>فرزانه وار از سر اینکار در گذر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بحر غم ز حرص چو غواص شوخ چشم</p></div>
<div class="m2"><p>غوطه مخور ز گوهر شهوار در گذر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر زخم خار از پی گل بایدت کشید</p></div>
<div class="m2"><p>منگر برنگ و بوی وز گلزار در گذر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر طور همت ار ندهندت جواب خوش</p></div>
<div class="m2"><p>ترک سؤال گیر و ز دیدار در گذر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر طاق نه رواق زر اندودت آرزوست</p></div>
<div class="m2"><p>زین طاق پا برون نه وزین چار در گذر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دار غرور نیست مقام قرار تو</p></div>
<div class="m2"><p>منصور وار از سر این دار در گذر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با مار بهر مهره کسی دوستی نکرد</p></div>
<div class="m2"><p>بر کن طمع ز مهره و از مار در گذر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون میتوان بگلشن روحانیون رسید</p></div>
<div class="m2"><p>سعیی نما وزین ره پر خار در گذر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صد بار گفتمت که نئی مرد اینمقام</p></div>
<div class="m2"><p>چون صدق من یقینت شد این بار در گذر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ابن یمین نشیمن قدس است جای تو</p></div>
<div class="m2"><p>ز این آشیان چو جعفر طیار در گذر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ایخداوندا رسیدت دختری دل شاد دار</p></div>
<div class="m2"><p>ای بسا دختر که باشد در هنر چون صد پسر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ماده گر خوانند خورشید فلک را عیب نیست</p></div>
<div class="m2"><p>ور زحل را نر نویسندان نباشد از هنر</p></div></div>