---
title: >-
    شمارهٔ ٨۴٢
---
# شمارهٔ ٨۴٢

<div class="b" id="bn1"><div class="m1"><p>زر بسیار چه حاجت که کنی صرف بر آنک</p></div>
<div class="m2"><p>خانقاهی بگچ و سنگ بعیوق بری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زر که بر خشت و گلت صرف شود ساده دلا</p></div>
<div class="m2"><p>شرم دار از خرد خود که ز خیرش شمری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سفره گردان کن اگر نام نکو میطلبی</p></div>
<div class="m2"><p>که باین نام ز اعیان جهان در گذری</p></div></div>