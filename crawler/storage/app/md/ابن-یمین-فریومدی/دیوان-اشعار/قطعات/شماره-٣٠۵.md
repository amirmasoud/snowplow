---
title: >-
    شمارهٔ ٣٠۵
---
# شمارهٔ ٣٠۵

<div class="b" id="bn1"><div class="m1"><p>شهریار جهان طغایتمور</p></div>
<div class="m2"><p>شاه سیارگان غلام تو باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام گیتی نما که خورشید است</p></div>
<div class="m2"><p>دور او تا ابد بکام تو باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست بزم تو رشک خلد برین</p></div>
<div class="m2"><p>حور عین ساقی مدام تو باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بمانی خضر صفت جاوید</p></div>
<div class="m2"><p>آبحیوان شراب جام تو باد</p></div></div>