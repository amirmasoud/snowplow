---
title: >-
    شمارهٔ ٨١٠
---
# شمارهٔ ٨١٠

<div class="b" id="bn1"><div class="m1"><p>با آنکه بی نصیبم از مال و جاه دنیا</p></div>
<div class="m2"><p>هرگز حسد نبردم بر منصبی و مالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر هیچکس دلم را حسرت نبود هرگز</p></div>
<div class="m2"><p>الا بر آنکه دارد با دلبری وصالی</p></div></div>