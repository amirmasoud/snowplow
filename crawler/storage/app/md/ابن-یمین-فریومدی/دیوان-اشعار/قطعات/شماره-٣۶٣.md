---
title: >-
    شمارهٔ ٣۶٣
---
# شمارهٔ ٣۶٣

<div class="b" id="bn1"><div class="m1"><p>مرا که طوطی شکر فشان گلشن قدس</p></div>
<div class="m2"><p>چو پیش بلبل نطق اوفتد پر اندازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عروس این تتق سبز زرنگار ز شرم</p></div>
<div class="m2"><p>چو بکر فکر مرا دید زیور اندازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فریب و ریو ز سودائیان بیمایه</p></div>
<div class="m2"><p>بدان رسید که سود و زیان بر اندازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ولی مهابت ان افضل زمین و زمان</p></div>
<div class="m2"><p>که منشی فلکش زیر پا سر اندازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیاث دولت و ملت که بحر خاطر او</p></div>
<div class="m2"><p>گه تلاطم امواج گوهر اندازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک شود همه تن آفتاب اگر رایش</p></div>
<div class="m2"><p>بلطف سایه بر این سبز منظر اندازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان ببست زبانشان که پیش کس پس ازین</p></div>
<div class="m2"><p>که راست زهره که رمزی از آن در اندازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همیشه تا دم باد خزان چو اهل کرم</p></div>
<div class="m2"><p>بروی خاک پر از شاخها زر اندازد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مباد حاسد جاهت جز آنچنان که ز جزع</p></div>
<div class="m2"><p>فراز صفحه زر گوهر تر اندازد</p></div></div>