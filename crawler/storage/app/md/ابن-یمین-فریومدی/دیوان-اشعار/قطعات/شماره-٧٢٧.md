---
title: >-
    شمارهٔ ٧٢٧
---
# شمارهٔ ٧٢٧

<div class="b" id="bn1"><div class="m1"><p>که میرود بجناب رفیع آصف عهد</p></div>
<div class="m2"><p>که با فلک ز علو بر زد آستانه او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیاث دولت و دین هندوی مبارک رای</p></div>
<div class="m2"><p>که مثل او بهنر نیست در زمانه او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ستوده ئی که بود با نسیم الطافش</p></div>
<div class="m2"><p>دم مسیح خجل از هوای خانه او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نپرسدش که چرا وعده را وفا نکند</p></div>
<div class="m2"><p>نداند آنچه بباشد درین بهانه او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر ز بنده برافروخت آتش غضبش</p></div>
<div class="m2"><p>بآب دیده نشاند رهی زبانه او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>و گر زقلت مال است دور باد ازو</p></div>
<div class="m2"><p>که اینقدر نتوان یافت در خزانه او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دام غم نرهد تا بحشر مرغ دلم</p></div>
<div class="m2"><p>اگر وظیفه نیابد دمی ز دانه او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چگونه باز کند خو ز دانه کرمش</p></div>
<div class="m2"><p>دلی که نیست بجز مرغ آشیانه او</p></div></div>