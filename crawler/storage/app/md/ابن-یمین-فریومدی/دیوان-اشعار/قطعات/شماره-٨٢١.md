---
title: >-
    شمارهٔ ٨٢١
---
# شمارهٔ ٨٢١

<div class="b" id="bn1"><div class="m1"><p>ترک شراب کردم از آنرو که دیدمش</p></div>
<div class="m2"><p>کز وی نماند در دل اصحاب طاعتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک کار نیک ازو ندهد هیچکس نشان</p></div>
<div class="m2"><p>الا بهم کشیدن احباب ساعتی</p></div></div>