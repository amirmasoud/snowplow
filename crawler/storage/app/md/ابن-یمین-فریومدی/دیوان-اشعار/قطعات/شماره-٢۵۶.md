---
title: >-
    شمارهٔ ٢۵۶
---
# شمارهٔ ٢۵۶

<div class="b" id="bn1"><div class="m1"><p>ترا فضل بر دیگران بیش از آن نیست</p></div>
<div class="m2"><p>که تو می‌دهی چیز و او می‌ستاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو ندهی و نستاند آن فضل بر خاست</p></div>
<div class="m2"><p>چو اویی و بر او چه رجحان بماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طمع چون بریدم من از مال خواجه</p></div>
<div class="m2"><p>زنش غر که خود را کم از خواجه داند</p></div></div>