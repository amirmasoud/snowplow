---
title: >-
    شمارهٔ ٣٠٢
---
# شمارهٔ ٣٠٢

<div class="b" id="bn1"><div class="m1"><p>شکرها واجب که نفس سرکش بدخوی را</p></div>
<div class="m2"><p>رایض عقلم بزیر زین همت رام کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود در آغاز کارم-دل چو گردون بیقرار</p></div>
<div class="m2"><p>چون بدید انجام آن نیکو چو قطب آرام کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر ضایع میشد اندر پختن سودای خام</p></div>
<div class="m2"><p>پخته نبود هر که زینسان کارهای خام کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل پیر از راه شفقت گفت با من کای جوان</p></div>
<div class="m2"><p>هر که کرد آغاز کاری فکرت انجام کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ جانرا کاشیان برسد ره و طوبی سزد</p></div>
<div class="m2"><p>از برای دانه نتوان پای بند دام کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شنید ابن یمین فرمان سلطان خرد</p></div>
<div class="m2"><p>نفس سرکش امتثال از کام و از ناکام کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کنج عزلت با فراغ خاطریرا همتش</p></div>
<div class="m2"><p>بارگاه هر امیری و وزیری نام کرد</p></div></div>