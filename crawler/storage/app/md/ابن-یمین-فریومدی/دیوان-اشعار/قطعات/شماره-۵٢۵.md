---
title: >-
    شمارهٔ ۵٢۵
---
# شمارهٔ ۵٢۵

<div class="b" id="bn1"><div class="m1"><p>در وصیت از بزرگان جهان</p></div>
<div class="m2"><p>گفت دانائی بفرزند خلف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با کسی کن دوست کو درد و حال</p></div>
<div class="m2"><p>با تو باشد همچو گوهر در صدف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر نگردد از تو چون گردی فقیر</p></div>
<div class="m2"><p>صحبتت داند در آنحالت شرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم نخواهد چون ترا بیند غنی</p></div>
<div class="m2"><p>تا شود مالت بآسانی تلف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور کند گردون ترا در جاه ماه</p></div>
<div class="m2"><p>با تو دارد چهره خود بی کلف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اینت کار خوب اگر گردد تمام</p></div>
<div class="m2"><p>اینت یار نیک اگر آید به کف</p></div></div>