---
title: >-
    شمارهٔ ٢۵٣
---
# شمارهٔ ٢۵٣

<div class="b" id="bn1"><div class="m1"><p>پنجروزی که حیاتست چنان باید زیست</p></div>
<div class="m2"><p>با خلایق که کم و بیش ثنائی ارزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقت رفتن چو رسد نیز چنان باید رفت</p></div>
<div class="m2"><p>که ز بیگانه و از خویش دعائی ارزد</p></div></div>