---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>ای شده ظاهر پرست باطنت آباد کن</p></div>
<div class="m2"><p>خرقه پاکت چه سود چون بدنت پاک نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرد ره عشق را گر قدمی همدم است</p></div>
<div class="m2"><p>صاحب سجاده و شانه و مسواک نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بفلک بر کشی دامن رفعت چو مهر</p></div>
<div class="m2"><p>صبح صفت گر ز صدق جیب دلت چاک نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی براه آر چست ترک گرانی بگیر</p></div>
<div class="m2"><p>هر که سبکبار نیست چابک و چالاک نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بکسی از زرت می نرسد بهره ئی</p></div>
<div class="m2"><p>آنچه تو خوانی زرش ایعجب ار خاک نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که رسد نزد تو روزی خود میخورد</p></div>
<div class="m2"><p>چون نخورد رزق تو ز آمدنش باک نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیک و بد دهر چون میگذرد لاجرم</p></div>
<div class="m2"><p>ابن یمین زین دو حال خرم و غمناک نیست</p></div></div>