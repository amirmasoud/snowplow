---
title: >-
    شمارهٔ ۴٩٧
---
# شمارهٔ ۴٩٧

<div class="b" id="bn1"><div class="m1"><p>شب دراز بتاریکی ار نشینم به</p></div>
<div class="m2"><p>که از چراغ لئیمان بمن رسد تابش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جگر ز آتش حرمان کباب اولی تر</p></div>
<div class="m2"><p>که از سقایه دونان کنند سیرابش</p></div></div>