---
title: >-
    شمارهٔ ٣۶١
---
# شمارهٔ ٣۶١

<div class="b" id="bn1"><div class="m1"><p>مراست صد هنر و نیست زر بدین عیبم</p></div>
<div class="m2"><p>اگر تو طعنه زنی بیهنر نخواهم شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر نصیب خرانست در جهان زر و مال</p></div>
<div class="m2"><p>من از برای زر و مال خر نخواهم شد</p></div></div>