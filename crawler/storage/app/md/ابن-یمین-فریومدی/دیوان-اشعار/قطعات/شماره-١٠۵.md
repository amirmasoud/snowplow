---
title: >-
    شمارهٔ ١٠۵
---
# شمارهٔ ١٠۵

<div class="b" id="bn1"><div class="m1"><p>چون سفیهی زبان دراز کند</p></div>
<div class="m2"><p>که فلانکس بفسق ممتازست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فسق او زین بیان یقین نشود</p></div>
<div class="m2"><p>واین باقرار خویش غمازست</p></div></div>