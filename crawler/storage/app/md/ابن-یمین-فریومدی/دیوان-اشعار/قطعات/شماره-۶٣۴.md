---
title: >-
    شمارهٔ ۶٣۴
---
# شمارهٔ ۶٣۴

<div class="b" id="bn1"><div class="m1"><p>هر کرا با خویشتن حالی بود</p></div>
<div class="m2"><p>کی شود خاطر ز تنهائی دژم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خود اندر کنج عزلت سرخوشست</p></div>
<div class="m2"><p>گر بشادی میگذارد و ر بغم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همدمی کز وی برآساید دلی</p></div>
<div class="m2"><p>گوئیا نامد بهستی از عدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نیم در بنده جاه و منصبی</p></div>
<div class="m2"><p>سهل باشد گر نباشم محتشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در طلبکاری مالی هم نیم</p></div>
<div class="m2"><p>خود کفافی میرسد از بیش و کم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنده ئی باید چو سکه تا فلک</p></div>
<div class="m2"><p>در کف او نرم گرداند درم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر بد و نیک جهان ابن یمین</p></div>
<div class="m2"><p>دل منه چون هست گردان دمبدم</p></div></div>