---
title: >-
    شمارهٔ ۵٧٣
---
# شمارهٔ ۵٧٣

<div class="b" id="bn1"><div class="m1"><p>هر که بندد کمر بخدمت خلق</p></div>
<div class="m2"><p>چون خردمند باشد و فاضل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظرش بر دو چیز اگر نبود</p></div>
<div class="m2"><p>پس بود سعی او از آن باطل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نگردد ز خدمت مخلوق</p></div>
<div class="m2"><p>هیچ از آن هر دو آرزو حاصل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اولا حرمت و دوم نعمت</p></div>
<div class="m2"><p>که از آن حاصلست شادی دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کز پی بیخودی شبانروزی</p></div>
<div class="m2"><p>عمر ضایع چرا کند عاقل</p></div></div>