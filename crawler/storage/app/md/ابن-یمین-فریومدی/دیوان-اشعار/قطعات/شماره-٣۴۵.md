---
title: >-
    شمارهٔ ٣۴۵
---
# شمارهٔ ٣۴۵

<div class="b" id="bn1"><div class="m1"><p>گرم گردون گردان چند روزی</p></div>
<div class="m2"><p>بسر ز آنسان که میباید نگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طمع زو نگسلم یکبارگی هم</p></div>
<div class="m2"><p>برینسان بعد ازین شاید نگردد</p></div></div>