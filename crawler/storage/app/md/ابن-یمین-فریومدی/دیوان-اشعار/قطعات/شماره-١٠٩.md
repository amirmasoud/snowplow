---
title: >-
    شمارهٔ ١٠٩
---
# شمارهٔ ١٠٩

<div class="b" id="bn1"><div class="m1"><p>خدائی که بنیاد هستیت را</p></div>
<div class="m2"><p>بروز ازل اندر افکند خشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل پیکرت را چهل بامداد</p></div>
<div class="m2"><p>بدست خود از راه حکمت سرشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قلم را بفرمود تا بر سرت</p></div>
<div class="m2"><p>همه بودنیها یکایک نوشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نزیبد که گوید ترا روز حشر</p></div>
<div class="m2"><p>که این کار خوبست و آن کار زشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندارد طمع رستن شاخ عود</p></div>
<div class="m2"><p>هر آنکس که بیخ شتر خار کشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو از خط فرمانش بیرون نه اند</p></div>
<div class="m2"><p>چه اصحاب مسجد چه اهل کنشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرد را شگفت آید از عدل او</p></div>
<div class="m2"><p>که اینرا دهد دوزخ انرا بهشت</p></div></div>