---
title: >-
    شمارهٔ ٣٩٢
---
# شمارهٔ ٣٩٢

<div class="b" id="bn1"><div class="m1"><p>ای پسر همنشین اگر خواهی</p></div>
<div class="m2"><p>همنشینی طلب ز خود بهتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آنکه در نفس همدم از همدم</p></div>
<div class="m2"><p>نقش پیدا شود بخیر و بشر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مثل اخکر که با همه گرمی</p></div>
<div class="m2"><p>سرد گردد بوصل خاکستر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور چه باشد فسرده طبع انگشت</p></div>
<div class="m2"><p>چون بآتش رسد شود اخگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر تو خواهی که نیکنام شوی</p></div>
<div class="m2"><p>دور باش از بدان عزیز پدر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وینسخن را که گفت ابن یمین</p></div>
<div class="m2"><p>در صلاح و فساد آن بنگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر پسندیده نایدت مشنو</p></div>
<div class="m2"><p>ور پسند آیدت از آن مگذر</p></div></div>