---
title: >-
    شمارهٔ ٧۴٩
---
# شمارهٔ ٧۴٩

<div class="b" id="bn1"><div class="m1"><p>جلال دولت و دین یونس ایکه جاه تر است</p></div>
<div class="m2"><p>مقام برتر کیوان فروترین پایه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آفتاب بچربدسها بتابش اگر</p></div>
<div class="m2"><p>ز نور رای تواش ذره‌ای بود مایه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عقد گوهر لفظت عروس معنی را</p></div>
<div class="m2"><p>ببسته کلک تو مشاطه وار پیرایه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخشگسال کرم آز تشنه لب دیده</p></div>
<div class="m2"><p>ز ابر خامه تو آنچه کودک از دایه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو آفتاب جهانی و بنده ابن یمین</p></div>
<div class="m2"><p>ترا شدست بامید نور همسایه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر ز طالع شوریده نیست این از چیست</p></div>
<div class="m2"><p>جهانیان ز تو در نور و بنده در سایه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو خود بگو که بدوران چون توئی چو منی</p></div>
<div class="m2"><p>شکسته حال چو این قافیه همی شایه</p></div></div>