---
title: >-
    شمارهٔ ١۴٨
---
# شمارهٔ ١۴٨

<div class="b" id="bn1"><div class="m1"><p>کسی گفت عزت بمال اندرست</p></div>
<div class="m2"><p>که دنیا و دین را درم یاورست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه مردی کند زور بازوی جاه</p></div>
<div class="m2"><p>که بی مال سلطان بی لشکرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تهیدست با هیبت و نام نیک</p></div>
<div class="m2"><p>زن زشتروئی که بیچادرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان مرغ ماند که بر شخص او</p></div>
<div class="m2"><p>پرو پوش بسیار و بس لاغرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دگر کس نگر تا جوابش چه داد</p></div>
<div class="m2"><p>بجاهست اگر آدمی سرورست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بذلت بود مرد مجهول نام</p></div>
<div class="m2"><p>وگر خود ز مال آستانش زرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خردمند را جاه باید نه مال</p></div>
<div class="m2"><p>وگر مال خواهی بجاه اندرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وگر راست خواهی ز سعدی شنو</p></div>
<div class="m2"><p>قناعت ازین هر دو نیکوترست</p></div></div>