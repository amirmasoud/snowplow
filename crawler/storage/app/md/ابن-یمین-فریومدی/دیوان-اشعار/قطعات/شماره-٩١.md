---
title: >-
    شمارهٔ ٩١
---
# شمارهٔ ٩١

<div class="b" id="bn1"><div class="m1"><p>بخور بنوش بپاش و بدان که حاصل عمر</p></div>
<div class="m2"><p>خرد نداشت کسی کو بدیگری بگذاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منه ذخیره که بسیار کس ز غایت حرص</p></div>
<div class="m2"><p>نهاد گنج بصد رنج و دیگری برداشت</p></div></div>