---
title: >-
    شمارهٔ ٨۴۶
---
# شمارهٔ ٨۴۶

<div class="b" id="bn1"><div class="m1"><p>سپهرا من از گردشت فارغم</p></div>
<div class="m2"><p>مرا کی توانی که غمگین کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه نایم که بر بسته باشم کمر</p></div>
<div class="m2"><p>بدان تا مرا کام شیرین کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه نرگس که سر پیشت آرم فرود</p></div>
<div class="m2"><p>گرم افسر و تاج زرین کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرفتم که ایوان قصر مرا</p></div>
<div class="m2"><p>ز خشت زر و نقره پر چین کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمیارزدم این تنعم بدان</p></div>
<div class="m2"><p>که در آخرم خشت بالین کنی</p></div></div>