---
title: >-
    شمارهٔ ٢٠۴
---
# شمارهٔ ٢٠۴

<div class="b" id="bn1"><div class="m1"><p>ای خردمند چو روزی ز جهان خواهی رفت</p></div>
<div class="m2"><p>مدت عمر تو گر پنجه و گر صد باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگمانی که مگر زان شودت حال نکو</p></div>
<div class="m2"><p>نکنی آنچه بر اهل خرد بد باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کز همه خلق جهان سیرت بد نا خوبست</p></div>
<div class="m2"><p>لیک نا خوبتر از مردم بخرد باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگذر از صورت و سیرت بصفا دار از آنک</p></div>
<div class="m2"><p>آدمی شکل بود کو بتر از دد باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکش از ربقه فرمان سر تسلیم و رضا</p></div>
<div class="m2"><p>که شرنگ از لب محبوب طبر زد باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در تصاریف زمان پای بیفشار چو کوه</p></div>
<div class="m2"><p>تا ترا طرف کمر لعل و زمرد باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حسب کوش چه نازی به نسب ابن یمین</p></div>
<div class="m2"><p>رو حسب جو که گهر را نسب از خود باشد</p></div></div>