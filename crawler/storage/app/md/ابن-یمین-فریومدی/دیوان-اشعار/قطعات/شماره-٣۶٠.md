---
title: >-
    شمارهٔ ٣۶٠
---
# شمارهٔ ٣۶٠

<div class="b" id="bn1"><div class="m1"><p>مرد آزاده بگیتی نکند میل دو چیز</p></div>
<div class="m2"><p>تا همه عمر وجودش بسلامت باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زن نخواهد اگرش دختر قیصر بدهند</p></div>
<div class="m2"><p>وام نستاند اگر وعده قیامت باشد</p></div></div>