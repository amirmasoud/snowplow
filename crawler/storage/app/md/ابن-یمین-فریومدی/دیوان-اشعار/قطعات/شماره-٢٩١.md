---
title: >-
    شمارهٔ ٢٩١
---
# شمارهٔ ٢٩١

<div class="b" id="bn1"><div class="m1"><p>در جهان هر جا که هست آزاده ئی</p></div>
<div class="m2"><p>بار غم از تنگدستی میکشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آن مشقت هم چو نیکو بنگری</p></div>
<div class="m2"><p>اکثرش از می پرستی میکشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر حکیمانه است و گر رندانه می</p></div>
<div class="m2"><p>آخر کارش بمستی میکشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرگس اندر مجلس گلها نگر</p></div>
<div class="m2"><p>سر ز مستی سوی پستی میکشد</p></div></div>