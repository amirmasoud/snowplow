---
title: >-
    شمارهٔ ٧٩٨
---
# شمارهٔ ٧٩٨

<div class="b" id="bn1"><div class="m1"><p>ایدل نصیحتی کنم ار زانکه بشنوی</p></div>
<div class="m2"><p>نا داده آب کشت سعادات ندروی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نهار در نهان نکنی آن معاملت</p></div>
<div class="m2"><p>کانگه که آشکار شود زان خجل شوی</p></div></div>