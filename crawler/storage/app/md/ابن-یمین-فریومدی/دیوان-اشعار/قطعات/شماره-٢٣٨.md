---
title: >-
    شمارهٔ ٢٣٨
---
# شمارهٔ ٢٣٨

<div class="b" id="bn1"><div class="m1"><p>بزیارت بر اصحاب مناصب کم رو</p></div>
<div class="m2"><p>گر نخواهی که ز اعزاز تو چیزی کاهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو باران که نخواهند که بسیار شود</p></div>
<div class="m2"><p>ور نیاید ز خدایش بتضرع خواهند</p></div></div>