---
title: >-
    شمارهٔ ٢٣٩
---
# شمارهٔ ٢٣٩

<div class="b" id="bn1"><div class="m1"><p>بهترین مراتب آن باشد</p></div>
<div class="m2"><p>کان بفضل و هنر بدست آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رتبتی کان نباشد استحقاق</p></div>
<div class="m2"><p>زودش اندر بنا شکست آید</p></div></div>