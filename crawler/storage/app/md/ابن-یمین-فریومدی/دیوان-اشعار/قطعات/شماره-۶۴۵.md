---
title: >-
    شمارهٔ ۶۴۵
---
# شمارهٔ ۶۴۵

<div class="b" id="bn1"><div class="m1"><p>الا ایصبا خدمتم عرضه دار</p></div>
<div class="m2"><p>بدرگاه دستور با زیب و زین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرسرکشان کز بلندی قدر</p></div>
<div class="m2"><p>همی بسپرد تارک فرقدین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر بگذرد برق تیغش بکوه</p></div>
<div class="m2"><p>بلرزد چو سیماب در کان لجین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بیم سخای در افشان کفش</p></div>
<div class="m2"><p>شود زرد رخ همچو بیجاده عین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگویش که سوی خراسان خرام</p></div>
<div class="m2"><p>که در دین ز حب وطن نیست شین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همان تا نهد خصم بر سر کلاه</p></div>
<div class="m2"><p>ز ایران برانش بخف حنین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر خصم گوید که من چون تو ام</p></div>
<div class="m2"><p>خرد صدق را باز داند ز بین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بصورت بود عین چون غین لیک</p></div>
<div class="m2"><p>بود نهصد و سی کم از غین عین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسن نیست با عدل تو چون منی</p></div>
<div class="m2"><p>بتیغ ستم خسته همچون حسین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیا کام ابن یمین را برآر</p></div>
<div class="m2"><p>که در ذمت همتت هست دین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو در نیکنامی بمان جاودان</p></div>
<div class="m2"><p>که آمد بد اندیش را حین حین</p></div></div>