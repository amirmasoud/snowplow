---
title: >-
    شمارهٔ ۴٨٧
---
# شمارهٔ ۴٨٧

<div class="b" id="bn1"><div class="m1"><p>بر تو خوانم ز دفتر اخلاق</p></div>
<div class="m2"><p>آیتی در وفا و در بخشش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با تو گویم که چیست غایت حلم</p></div>
<div class="m2"><p>هر که زهرت دهد شکر بخشش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که بخراشدت جگر بجفا</p></div>
<div class="m2"><p>همچو کان کریم زر بخشش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کم مباش از درخت سایه فکن</p></div>
<div class="m2"><p>هر که سنگت زند ثمر بخشش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از صدف یادگیر نکته حلم</p></div>
<div class="m2"><p>هر که سر ببردت گهر بخشش</p></div></div>