---
title: >-
    شمارهٔ ۴٢۵
---
# شمارهٔ ۴٢۵

<div class="b" id="bn1"><div class="m1"><p>زین همدمان فغان که همه مار ماهیند</p></div>
<div class="m2"><p>صورت بسان ماهی و سیرت بسان مار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر سیم خام بماهی طمع مکن</p></div>
<div class="m2"><p>پخته ز بهر مهره نبوسد لبان مار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محبوب اهل دل نشود بد کنش بمال</p></div>
<div class="m2"><p>آخر نه گنج سیم و زر آمد مکان مار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کس چو مور کرد بنان پاره شان کشش</p></div>
<div class="m2"><p>بر ساخت پاد زهر ز آب دهان مار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این مار سیرتان بره آیند وقت مرگ</p></div>
<div class="m2"><p>آید بلی بره چو سرآید زمان مار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون مار هر یکی دو زبانند زهر پاش</p></div>
<div class="m2"><p>با داد و نیمه سر همه را چون زبان مار</p></div></div>