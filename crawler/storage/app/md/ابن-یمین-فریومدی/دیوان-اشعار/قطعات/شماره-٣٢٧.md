---
title: >-
    شمارهٔ ٣٢٧
---
# شمارهٔ ٣٢٧

<div class="b" id="bn1"><div class="m1"><p>کسی کو خموش است و پشمینه پوش</p></div>
<div class="m2"><p>میان خلایق سروشی کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبینی که از جمله میوه ها</p></div>
<div class="m2"><p>به است آنکه پشمینه پوشی کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن سوسن آزادگی یافتست</p></div>
<div class="m2"><p>که باده زبان در خموشی کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر این هر دو گر نرم خوئی چرا</p></div>
<div class="m2"><p>بقصد کسی سخت کوشی کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حکیمانه میگوید ابن یمین</p></div>
<div class="m2"><p>کسی کو که حکمت نیوشی کند</p></div></div>