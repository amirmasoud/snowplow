---
title: >-
    شمارهٔ ٨۶٧
---
# شمارهٔ ٨۶٧

<div class="b" id="bn1"><div class="m1"><p>مکافات بدی کردن حلالست</p></div>
<div class="m2"><p>چو بی جرم از کسی بد دیده باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدی با او بجای خویش باشد</p></div>
<div class="m2"><p>نکوئی کن که نیکو کرده باشی</p></div></div>