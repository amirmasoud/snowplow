---
title: >-
    شمارهٔ ۴٠۴
---
# شمارهٔ ۴٠۴

<div class="b" id="bn1"><div class="m1"><p>با عطارد گفتم آخر با تو دارم نسبتی</p></div>
<div class="m2"><p>چند بد مهری کنی به زین غم کارم بخور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت کای ابن یمین گر قدرتی بودی مرا</p></div>
<div class="m2"><p>کی بدینسان گشتمی گشتمی گرد جهان آسیمه سر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اکثر اوقات باشد درو بال و احتراق</p></div>
<div class="m2"><p>بر سر تیرم همیدارد فلک زیر و زبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رونق کارت ز دستم بر نیمآید ولیک</p></div>
<div class="m2"><p>خواهمت گفت از سر اشفاق پندی معتبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کریمان چون جهان خالی همی بینی مکن</p></div>
<div class="m2"><p>بعد از این عمر گرامی در سر بوک و مگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جگر خوردن بسر بر عمر و بهر یک دو نان</p></div>
<div class="m2"><p>در پی دو نان مپوی و آبروی خود مبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که میخواهد که باشد از هنر با آبروی</p></div>
<div class="m2"><p>سهل باشد گر نباشد در کف او سیم و زر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آهن و فولاد را بنگر که چون شد آبدار</p></div>
<div class="m2"><p>سهل باشد گر نباشد در کف او سیم و زر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آهن و فولاد را بنگر که چون شد آبدار</p></div>
<div class="m2"><p>بر چسان هنگام ضربت میکند پیدا گهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از کدورات حوادث چونکه ماند با صفا</p></div>
<div class="m2"><p>آبروی کوه باشد چشمه ساران هنر</p></div></div>