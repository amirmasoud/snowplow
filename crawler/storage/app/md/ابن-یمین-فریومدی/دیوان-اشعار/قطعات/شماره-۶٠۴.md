---
title: >-
    شمارهٔ ۶٠۴
---
# شمارهٔ ۶٠۴

<div class="b" id="bn1"><div class="m1"><p>روزی گذر فتاد مرا از قضای چرخ</p></div>
<div class="m2"><p>برمنزلی که بار بوددر او یار همدمم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد آمدم ز عهد و وفای قدیم او</p></div>
<div class="m2"><p>جائیکه او نهاد بصدق و صفا قدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باریدم آب دیده و گفتم بسوز دل</p></div>
<div class="m2"><p>کایام خرمی شد و آن زمان غم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیتو چو تون و تنجه نماید بچشم من</p></div>
<div class="m2"><p>گر بگذرم بروضه رضوان و برارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بیتو زندگی بودم مدتی دراز</p></div>
<div class="m2"><p>دانم که در ریاض طرب کمترک چرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حقا که بنده ابن یمین را در آرزوت</p></div>
<div class="m2"><p>بر عمر مانده از پس تو هست صدندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اما همی دهد دل خود را تسلیی</p></div>
<div class="m2"><p>کان چون گذشت بگذرد این روز نیز هم</p></div></div>