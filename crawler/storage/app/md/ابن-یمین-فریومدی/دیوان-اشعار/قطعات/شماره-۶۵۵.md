---
title: >-
    شمارهٔ ۶۵۵
---
# شمارهٔ ۶۵۵

<div class="b" id="bn1"><div class="m1"><p>با گوی گفت چو کان کای در هوای وصلت</p></div>
<div class="m2"><p>پیوسته کار قدم از بار غم خمیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داری دمی سر آن کآئی برم اگر چه</p></div>
<div class="m2"><p>با من ترا نباشد آئین آرمیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویش چه گفت گفتا گر خوانی ار برانی</p></div>
<div class="m2"><p>از دوست یک اشارت از ما به سر دویدن</p></div></div>