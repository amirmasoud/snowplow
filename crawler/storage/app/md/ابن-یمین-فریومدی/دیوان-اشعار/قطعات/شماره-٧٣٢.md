---
title: >-
    شمارهٔ ٧٣٢
---
# شمارهٔ ٧٣٢

<div class="b" id="bn1"><div class="m1"><p>مرد عاقل نرود در پی کاری که در آن</p></div>
<div class="m2"><p>هر کسی تهمت دیگر نهد اندر حق او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاقل آنست که فکرش بمقامی برسد</p></div>
<div class="m2"><p>که بگویند پس از وی همه کس منطق او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیر زین رام کند توسن ایام چنان</p></div>
<div class="m2"><p>کز لگامش نکشد سر پس ازین ابلق او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ز دریای فلک میل گذارش باشد</p></div>
<div class="m2"><p>شود از قوت رایش مه نو زورق او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بدین پایه رسد مرد زند هر سو دست</p></div>
<div class="m2"><p>دست احداث مقید نکند مطلق او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که بر اسب مراد دل خود گشت سوار</p></div>
<div class="m2"><p>شاه گیری نکند پس چه کند بیدق او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میکند ابن یمین نیز در احوال جهان</p></div>
<div class="m2"><p>فکر تا حل شودش مسئله مغلق او</p></div></div>