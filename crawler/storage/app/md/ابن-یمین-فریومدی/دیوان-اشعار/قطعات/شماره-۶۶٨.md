---
title: >-
    شمارهٔ ۶۶٨
---
# شمارهٔ ۶۶٨

<div class="b" id="bn1"><div class="m1"><p>چون بسخن دم زند طوطی طبع خوشم</p></div>
<div class="m2"><p>کز شکر آرد ببار شاخ نبات سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرده صد ساله را زنده کند گر زند</p></div>
<div class="m2"><p>آتش طبعم بر او آبحیات سخن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ابن یمین خیره شد چشم خرد کو چسان</p></div>
<div class="m2"><p>کرد چو عیسی بدم زنده رفات سخن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواست دلم تا سخن پیش بزرگی برد</p></div>
<div class="m2"><p>گفت خرد کامدت وقت وفات سخن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من ز مدیح کسی اجر ندارم طمع</p></div>
<div class="m2"><p>مدحت اینها مرا هست زکات سخن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه ز دیوان کس تا بکنون نامدست</p></div>
<div class="m2"><p>هیچ براتی مرا بهر صلات سخن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنده ز دیوان خود از پی هر کس که خواست</p></div>
<div class="m2"><p>در همه عالم روان کرد برات سخن</p></div></div>