---
title: >-
    شمارهٔ ۶٧۵
---
# شمارهٔ ۶٧۵

<div class="b" id="bn1"><div class="m1"><p>ز هی سعادت من گر نسیم باد شمال</p></div>
<div class="m2"><p>که در ادای رسالت بود چو روح الامین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رساند از من دلخسته مختصر سخنی</p></div>
<div class="m2"><p>بسمع اشرف دارای ملک و داور دین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محیط و مرکز جود آن کریم دریا دل</p></div>
<div class="m2"><p>که در مکان کرم ذات اوست با تمکین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپهر حشمت و رفعت جهان فضل و هنر</p></div>
<div class="m2"><p>نظام دولت و دین سرور زمان و زمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگوید ار چه بود قدرتم بدولت تو</p></div>
<div class="m2"><p>که سبز خنگ فلک را درآورم در زین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ولی زگردش گردون قوی ضعیف تنم</p></div>
<div class="m2"><p>چنانکه می نتوانم که گردم اسب نشین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و گر چه پیر مسیحا دمم ولی نبود</p></div>
<div class="m2"><p>مسیح وار بخر بر نشستنم آئین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیاده نیز نیارم که در سفر باشم</p></div>
<div class="m2"><p>بهر کجا که روی همچو بخت با تو قرین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو از ملازمت و بندگی گزیرم نیست</p></div>
<div class="m2"><p>مرا الاغ مگر استری دهی پس ازین</p></div></div>