---
title: >-
    شمارهٔ ٣٣۵
---
# شمارهٔ ٣٣۵

<div class="b" id="bn1"><div class="m1"><p>گرم بدست فتد ساقی سمن ساقی</p></div>
<div class="m2"><p>که بر لطافت طبعش وثوق من باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شام تا بسحر می خورم که خود زرخش</p></div>
<div class="m2"><p>نماز شام زمان شروق من باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبوح کان نبود پیشتر ز بانگ نماز</p></div>
<div class="m2"><p>بجان دختر رز کان غبوق من باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخواهم آنکه شود ثالثی مزاحم ما</p></div>
<div class="m2"><p>و گر چه محرم صدق و صدوق من باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگاه مستی اگر بوسه ئی ازو خواهم</p></div>
<div class="m2"><p>چنانکه عادت فسق و فسوق من باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شگفتم آید ازو در کنارم ار نکند</p></div>
<div class="m2"><p>ز تندی آنچه سزاوار بوق من باشد</p></div></div>