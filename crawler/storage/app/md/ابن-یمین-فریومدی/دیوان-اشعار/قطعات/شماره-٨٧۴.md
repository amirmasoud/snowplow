---
title: >-
    شمارهٔ ٨٧۴
---
# شمارهٔ ٨٧۴

<div class="b" id="bn1"><div class="m1"><p>مزن دم در آنچت گزیرست ازان</p></div>
<div class="m2"><p>که حمل افتد این شیوه بر بیهشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ایدون بمقدار گوئی سخن</p></div>
<div class="m2"><p>ز خوی خوش خویش در رامشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور از حد برون میبری گفت را</p></div>
<div class="m2"><p>بتیغ زبان خویش را میکشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز گفتن پشیمان بسی دیده ام</p></div>
<div class="m2"><p>ندیدم پشیمان کسی از خامشی</p></div></div>