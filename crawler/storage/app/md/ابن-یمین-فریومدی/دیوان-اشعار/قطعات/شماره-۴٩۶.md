---
title: >-
    شمارهٔ ۴٩۶
---
# شمارهٔ ۴٩۶

<div class="b" id="bn1"><div class="m1"><p>روزی بخرد ابن یمین از غم دل گفت</p></div>
<div class="m2"><p>آندم که فلک بستد ازو هر کم و بیشش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرسید که آیا بجهان هیچ کریمی</p></div>
<div class="m2"><p>باشد که کند چاره درد دل ریشش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتا که بلی شاه ابوبکر علی کوست</p></div>
<div class="m2"><p>شاهی که بود جود و کرم عادت و کیشش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید صفت ذره نوازست از آنست</p></div>
<div class="m2"><p>چون سایه دوان خلق جهان از پس و پیشش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون مرحمت او همه را شامل حالست</p></div>
<div class="m2"><p>بیگانه همان لطف ازو دید که خویشش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر ظلم فلک داد ازو خواه که امروز</p></div>
<div class="m2"><p>نوش کرم او شکند تلخی نیشش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رو معتکف درگه او باش که آنست</p></div>
<div class="m2"><p>جائیکه کنند اهل جهان قبله خویشش</p></div></div>