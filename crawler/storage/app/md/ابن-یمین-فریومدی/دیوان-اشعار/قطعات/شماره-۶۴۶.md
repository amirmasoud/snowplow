---
title: >-
    شمارهٔ ۶۴۶
---
# شمارهٔ ۶۴۶

<div class="b" id="bn1"><div class="m1"><p>ای باد صبحدم گذری کن ز روی لطف</p></div>
<div class="m2"><p>بهر من شکسته محزون ممتحن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوی جناب آصف ثانی علاء دین</p></div>
<div class="m2"><p>کز راه رتبت اوست سلیمان این زمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دستور دین پناه محمد که رأی اوست</p></div>
<div class="m2"><p>در ضبط ملک بر صفت روح در بدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنصاحبی که با نفس خلق فایحش</p></div>
<div class="m2"><p>باشد سیاهروی جهان نافه ختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر باشدت مجال که گوئی حکایتی</p></div>
<div class="m2"><p>این یکسخن بعرض رسان از زبان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کای مفتی شرایع احسان روا بود</p></div>
<div class="m2"><p>کابن یمین ز بهر تو ببرید از وطن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشتی بخشگ راند و خدام آنجناب</p></div>
<div class="m2"><p>غرق بحار جود تو یکسر زمرد و زن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آری اگر رواست تو مخدوم و حاکمی</p></div>
<div class="m2"><p>ور نارواست پس نظری سوی او فکن</p></div></div>