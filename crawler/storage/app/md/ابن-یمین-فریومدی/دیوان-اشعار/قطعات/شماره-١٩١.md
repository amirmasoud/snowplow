---
title: >-
    شمارهٔ ١٩١
---
# شمارهٔ ١٩١

<div class="b" id="bn1"><div class="m1"><p>یک دو نوبت در جناب خسرو جمشید فر</p></div>
<div class="m2"><p>آنکه یابد ملک ازو همچون تن از جان تربیت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانکه بهر بخشش او سیم و زر را میرسد</p></div>
<div class="m2"><p>در صمیم کان ز لطف مهر تابان تربیت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وانکه زیر سایه مهرش عجب ناید مرا</p></div>
<div class="m2"><p>گر ز نور ماه یابد تار کتان تربیت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من بعون رأی پیر و قوت بخت جوان</p></div>
<div class="m2"><p>عرضه کردم شعر و زو دیدم فراوان تربیت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود بتحسین تربیت فرمود و پس نواب را</p></div>
<div class="m2"><p>کرد اشارت تا کند از راه احسان تربیت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عمل نواب را پروای من گوئی نبود</p></div>
<div class="m2"><p>زانکه تا اکنون اثر پیدا نشد زان تربیت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقثه المصدور کردم عرضه تا داند امیر</p></div>
<div class="m2"><p>آنکه اندر شأن منشان بود از اینسان تربیت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تربیت گر میکند خسرو بدوست خود کند</p></div>
<div class="m2"><p>ز آنکه این چاکر نخواهد یافت زیشان تربیت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بود عادت که دائم غنچه دلتنگ را</p></div>
<div class="m2"><p>لب شود خندان چو باید ز ابر نیسان تربیت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ابر نیسان کفش فیاض بادا آنچنانک</p></div>
<div class="m2"><p>یابد از وی غنچه لب‌های خندان تربیت</p></div></div>