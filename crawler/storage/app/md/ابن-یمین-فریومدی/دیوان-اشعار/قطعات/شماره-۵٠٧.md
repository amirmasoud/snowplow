---
title: >-
    شمارهٔ ۵٠٧
---
# شمارهٔ ۵٠٧

<div class="b" id="bn1"><div class="m1"><p>مرا سپهر چو نراد مهره دزد آمد</p></div>
<div class="m2"><p>که دانه دل ازادگان بود خصلش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آن خدنگ بلا کز کمان چرخ جهد</p></div>
<div class="m2"><p>درون سینه فرزانگان بود نصلش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرش عنایت و گر بیعنایتی رواست</p></div>
<div class="m2"><p>که این بنا چه فتادست بی ثبات اصلش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان و هر چه درو هست فارغیم ازان</p></div>
<div class="m2"><p>که داغ هجر نیرزد تنعم وصلش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نبود حبل مودت میان ما و جهان</p></div>
<div class="m2"><p>و کر که بود بهمت همی کنم فصلش</p></div></div>