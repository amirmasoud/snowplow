---
title: >-
    شمارهٔ ۶٢۴
---
# شمارهٔ ۶٢۴

<div class="b" id="bn1"><div class="m1"><p>گر ترا هست خرد یاز بیا ز ابن یمین</p></div>
<div class="m2"><p>یک نصیحت بشنو این ز بزرگان قدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه در دست تو باشد بفشان باک مدار</p></div>
<div class="m2"><p>زان میندیش که از دست برون شد زر و سیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بهر نوع که باشد به شب آری روزی</p></div>
<div class="m2"><p>بخورید و بخوریم و بخرید و بخریم</p></div></div>