---
title: >-
    شمارهٔ ٣٩
---
# شمارهٔ ٣٩

<div class="b" id="bn1"><div class="m1"><p>دیدم برین رواق زبرجد کتابتی</p></div>
<div class="m2"><p>بر لوح لاجورد نوشته بزر ناب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر خانه ئیکه داخل این طاق ازرقست</p></div>
<div class="m2"><p>گر صد هزار سال بپاید شود خراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیرون ازین رواق بنا کن تو خانه ئی</p></div>
<div class="m2"><p>کو آفت خراب نیابد بهیچ باب</p></div></div>