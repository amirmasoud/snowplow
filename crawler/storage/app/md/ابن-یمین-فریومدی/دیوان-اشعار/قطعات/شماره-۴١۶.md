---
title: >-
    شمارهٔ ۴١۶
---
# شمارهٔ ۴١۶

<div class="b" id="bn1"><div class="m1"><p>چون روزگار هست بتصحیف روز کار</p></div>
<div class="m2"><p>پس روز کار خواندنش به که روزگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یعنی که روز کار کنونست کار کن</p></div>
<div class="m2"><p>کین روز چون گذشت دیگر نیست روز کار</p></div></div>