---
title: >-
    شمارهٔ ٧٠٨
---
# شمارهٔ ٧٠٨

<div class="b" id="bn1"><div class="m1"><p>هست کار سعادت دنیا</p></div>
<div class="m2"><p>راست همچون مناره برفین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتاب تموز حادثه ها</p></div>
<div class="m2"><p>برگشاده برو- ز تاب کمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در تزلزل بنای ارکانش</p></div>
<div class="m2"><p>دل برو کی نهند اهل یقین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناگهانی ز هم فرو ریزد</p></div>
<div class="m2"><p>که امید ثبات دارد ازین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کرا آز پیشوائی کرد</p></div>
<div class="m2"><p>باز نشناخت مهر چرخ از کین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر زمانش زبان حرص کند</p></div>
<div class="m2"><p>آیتی دیگر از هوا تلقین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما و یاری و کنج عافیتی</p></div>
<div class="m2"><p>که همینست و بس بهشت برین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اینسخن باور از نمیداری</p></div>
<div class="m2"><p>خیز و رنجه شو و بیا و ببین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زانگروهی که سخره شهرند</p></div>
<div class="m2"><p>تا بدانی که نیست ابن یمین</p></div></div>