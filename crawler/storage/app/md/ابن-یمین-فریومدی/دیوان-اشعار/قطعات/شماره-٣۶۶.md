---
title: >-
    شمارهٔ ٣۶۶
---
# شمارهٔ ٣۶۶

<div class="b" id="bn1"><div class="m1"><p>نهال باغ وزارت علاء دولت و دین</p></div>
<div class="m2"><p>چو سرو بر چمن ملک سر فراز افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عروس فضل که بودی اسیر فاقه و فقر</p></div>
<div class="m2"><p>بروزگار وی اندر نعیم و ناز افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپهرش ار چه ز عین الکمال نقصی جست</p></div>
<div class="m2"><p>و گر چه پایه قدرش در اهتزاز افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و گر چه ماه معالیش در محاق نشست</p></div>
<div class="m2"><p>و گر چه شمع بزرگیش در گداز افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آفتاب ز جاهش نکاست یکذره</p></div>
<div class="m2"><p>نه ماه نیز بصف النعال باز افتاد</p></div></div>