---
title: >-
    شمارهٔ ٣٣٢
---
# شمارهٔ ٣٣٢

<div class="b" id="bn1"><div class="m1"><p>گردون دون بتهمت فضل و هنر مرا</p></div>
<div class="m2"><p>هر لحظه بیگناه عذابی دگر کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاهم چو عود پوست کند بازوگه چو عود</p></div>
<div class="m2"><p>سوزد مرا و گاه چو عودم همی زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر شاخ شادیم که بود در زمین دل</p></div>
<div class="m2"><p>آنرا بباد حادثه از بیخ بر کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر گرد دانه ئی که بر او نام رزق ماست</p></div>
<div class="m2"><p>چون عنکبوت گرد مگس دامها تند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من نیز دشمنی کنم امبار با هنر</p></div>
<div class="m2"><p>باشد بدوستی نظری بر من افکند</p></div></div>