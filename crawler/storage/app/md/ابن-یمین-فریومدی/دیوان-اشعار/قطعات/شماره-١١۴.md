---
title: >-
    شمارهٔ ١١۴
---
# شمارهٔ ١١۴

<div class="b" id="bn1"><div class="m1"><p>دیدم آنکس را که باز همتش</p></div>
<div class="m2"><p>گاه صید باز سیمین طبل ساخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمترین بندگان درگهش</p></div>
<div class="m2"><p>در تماشاگاه او اصطبل ساخت</p></div></div>