---
title: >-
    شمارهٔ ۶٠٠
---
# شمارهٔ ۶٠٠

<div class="b" id="bn1"><div class="m1"><p>خسروانرا دو کار میباید</p></div>
<div class="m2"><p>تا شود کار خسروی بنظام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اولا همتی چو ابر بهار</p></div>
<div class="m2"><p>که دهد بهره خواص و عوام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ثانیا هیبتی که دشمن را</p></div>
<div class="m2"><p>خون چکاند بجای خوی زمسام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر یکی زین دو خصله پامال است</p></div>
<div class="m2"><p>خسرویرا مدار چشم دوام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این دو چیزست و هیچ دیگر نیست</p></div>
<div class="m2"><p>خسرویرا همین دو هست قوام</p></div></div>