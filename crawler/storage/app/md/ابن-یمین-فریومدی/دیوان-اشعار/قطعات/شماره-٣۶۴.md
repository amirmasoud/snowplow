---
title: >-
    شمارهٔ ٣۶۴
---
# شمارهٔ ٣۶۴

<div class="b" id="bn1"><div class="m1"><p>نسیم باد صبا کیست جز تو کز بر من</p></div>
<div class="m2"><p>بنزد خواجه رسالت گذار خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگویدش که گرم بر قرار کار نماند</p></div>
<div class="m2"><p>کدام کار که آن برقرار خواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا که فخر نبودست تا کنون بعمل</p></div>
<div class="m2"><p>قیاس کن که زعزلم چه عار خواهد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو چیز موجب شکرست بنده را گه غزل</p></div>
<div class="m2"><p>که نزد زنده دلانش اعتبار خواهد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی که هیچ نکردست در زمان عمل</p></div>
<div class="m2"><p>که وقت عزل از آن شرمسار خواهد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوم کتابت ارکان دولتت پس از آن</p></div>
<div class="m2"><p>شد آن فسانه که در هر دیار خواهد بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه میکنم عملی را که عزل در پی آن</p></div>
<div class="m2"><p>ز بی ثباتی این روزگار خواهد بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا ثنای تو گفتن نکوترین کاریست</p></div>
<div class="m2"><p>همین بسم به از اینم چه کار خواهد بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من و رسالت صیتت بشش جهات جهان</p></div>
<div class="m2"><p>علی الدوام که این پنج و چار خواهد بود</p></div></div>