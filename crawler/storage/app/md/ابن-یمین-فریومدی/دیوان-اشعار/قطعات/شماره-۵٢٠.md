---
title: >-
    شمارهٔ ۵٢٠
---
# شمارهٔ ۵٢٠

<div class="b" id="bn1"><div class="m1"><p>عزمم درست گشت که نارم دگر بکف</p></div>
<div class="m2"><p>مدح کسی که هست بدو هجو هم دریغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میغند این خسان به نپاشیدن عطا</p></div>
<div class="m2"><p>ز آنرو که جمله صاعقه بارند همچو میغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابن یمین ز همت دونان کرم مجوی</p></div>
<div class="m2"><p>کی کار ذوالفقار کند زنگ خورده تیغ</p></div></div>