---
title: >-
    شمارهٔ ١۵۶
---
# شمارهٔ ١۵۶

<div class="b" id="bn1"><div class="m1"><p>گر آسیای چرخ ترا آرد میکند</p></div>
<div class="m2"><p>باید که همچو قطب نمائی در آن ثبات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی دو گر شود ایام بد کنش</p></div>
<div class="m2"><p>هم عاقبت نکو شود ار باشدت حیات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا زنده ئی مدار ز احداث دهر باک</p></div>
<div class="m2"><p>بیرون ز مرگ سهل بود جمله حادثات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نوازد فلکت غره مباش از پی آن</p></div>
<div class="m2"><p>کش صعودی نبود کونه هبوطی ز پی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور بلندی دهدت بخت بدان نیز مناز</p></div>
<div class="m2"><p>کارتفاعی نبود کش نه سقوطی ز پی است</p></div></div>