---
title: >-
    شمارهٔ ٣۶
---
# شمارهٔ ٣۶

<div class="b" id="bn1"><div class="m1"><p>اگر نیک و گر بد چو خواهد رسید</p></div>
<div class="m2"><p>ز ایام عمر تو روزی بشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببین روز را تا صلاح تو چیست</p></div>
<div class="m2"><p>بغم به که آری بشب یا طرب</p></div></div>