---
title: >-
    شمارهٔ ٢٧۴
---
# شمارهٔ ٢٧۴

<div class="b" id="bn1"><div class="m1"><p>خلق خدا که خدمت دادار میکنند</p></div>
<div class="m2"><p>هستند بر سه قسم که این کار میکنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قسمی شدند از پی جنت خدا پرست</p></div>
<div class="m2"><p>و آن رسم و عادتیست که تجار میکنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قوم دگر کنند پرستش ز بیم او</p></div>
<div class="m2"><p>و اینکار بندگانست که احرار میکنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمعی نظر ازین دو جهت قطع کرده اند</p></div>
<div class="m2"><p>بر کار هر دو طایفه انکار میکنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون غیر خویش مرکز هستی نیافتند</p></div>
<div class="m2"><p>بر گرد خویش دور چو پرگار میکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اینست راه حق که سوم فرقه میروند</p></div>
<div class="m2"><p>سیر و سلوک راه بهنجار میکنند</p></div></div>