---
title: >-
    شمارهٔ ١٠
---
# شمارهٔ ١٠

<div class="b" id="bn1"><div class="m1"><p>بر کاتبان خویشتن املای بد مکن</p></div>
<div class="m2"><p>چون سر زدند از پی تحریر خامه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>املا نگر که بر چه نویسندگان کنی</p></div>
<div class="m2"><p>و ایشان بحضرت که نویسند نامه را</p></div></div>