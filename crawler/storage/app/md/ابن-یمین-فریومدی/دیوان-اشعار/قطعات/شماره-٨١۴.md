---
title: >-
    شمارهٔ ٨١۴
---
# شمارهٔ ٨١۴

<div class="b" id="bn1"><div class="m1"><p>بر میوه های نوبر بستانسرای طبع</p></div>
<div class="m2"><p>کردم بسان ماه بآئین صباغتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوان من بخواه و بتدقیق در نگر</p></div>
<div class="m2"><p>نا کرده هیچ زرگر ازینسان صیاغتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اکنون گذشت انکه کسی گاه نظم و نثر</p></div>
<div class="m2"><p>از من فصاحتی طلبد یا بلاغتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد شکر و صد سپاس کز اشغال روزگار</p></div>
<div class="m2"><p>داد ایزدم فراغت و نیکو فراغتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منبعد ننگرم بجهان و جهانیان</p></div>
<div class="m2"><p>با این فراغت ار بودم هم رفاغتی</p></div></div>