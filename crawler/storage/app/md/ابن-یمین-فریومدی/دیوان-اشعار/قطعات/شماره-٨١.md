---
title: >-
    شمارهٔ ٨١
---
# شمارهٔ ٨١

<div class="b" id="bn1"><div class="m1"><p>اگر در حوادث که پیش آیدت</p></div>
<div class="m2"><p>بدرگاه ایزد پناهد دلت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور از امر و نهیی که فرمود حق</p></div>
<div class="m2"><p>نه افزاید ایچ و نه کاهد دلت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان خاص درگاه یزدان شوی</p></div>
<div class="m2"><p>که یابی ازو هر چه خواهد دلت</p></div></div>