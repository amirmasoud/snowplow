---
title: >-
    شمارهٔ ٧٧۵
---
# شمارهٔ ٧٧۵

<div class="b" id="bn1"><div class="m1"><p>من نخواهم خرید کبر کسی</p></div>
<div class="m2"><p>کاولش نطفه ئی بود مذره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آخرش جیفه ئی شود قذره</p></div>
<div class="m2"><p>و او همه عمر حاصل عذره</p></div></div>