---
title: >-
    شمارهٔ ١٨١
---
# شمارهٔ ١٨١

<div class="b" id="bn1"><div class="m1"><p>هر که در کارها مشاوره کرد</p></div>
<div class="m2"><p>گلبن باغ دولتش بشکفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر مهمی که باشد از بدو نیک</p></div>
<div class="m2"><p>در جهان با دو شخص باید گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اولا آنکه او بحق گوئی</p></div>
<div class="m2"><p>همچو الماس در تواند سفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ثانیا با کسی که صورت صدق</p></div>
<div class="m2"><p>با تو بیرون بیاورد ز نهفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بینی که هر یکی ز ایشان</p></div>
<div class="m2"><p>گرد غم از دلت چگونه برفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن دوست در جهان طاق است</p></div>
<div class="m2"><p>با دل خویش کرد باید جفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور قبول آیدت نصیحت خصم</p></div>
<div class="m2"><p>غم خود خور که روزگار آشفت</p></div></div>