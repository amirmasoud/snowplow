---
title: >-
    شمارهٔ ٧٨۶
---
# شمارهٔ ٧٨۶

<div class="b" id="bn1"><div class="m1"><p>اگر خاطرت میل کاری کند</p></div>
<div class="m2"><p>کزان کار داری امید بهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازین پیشتر عاقلان گفته اند</p></div>
<div class="m2"><p>فارسل حکیما و لا توصهی</p></div></div>