---
title: >-
    شمارهٔ ٢١۶
---
# شمارهٔ ٢١۶

<div class="b" id="bn1"><div class="m1"><p>اهل دنیا سه فرقه بیش نیند</p></div>
<div class="m2"><p>چون طعام اند و همچو دارو و درد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرقه ئی چون طعام در خورند</p></div>
<div class="m2"><p>که از ایشان گزیر نتوان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز جمعی چو داروی دردند</p></div>
<div class="m2"><p>که بدان گه گهست حاجت مرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز جمعی چو درد با ضررند</p></div>
<div class="m2"><p>تا توانی بگرد درد مگرد</p></div></div>