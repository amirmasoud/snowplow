---
title: >-
    شمارهٔ ٨٧٨
---
# شمارهٔ ٨٧٨

<div class="b" id="bn1"><div class="m1"><p>ندیده نان تو اندر جهان کسی هرگز</p></div>
<div class="m2"><p>بسان خاک چنین خوار از پی آنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو آدمی نخورد نان فرشته را ماند</p></div>
<div class="m2"><p>تو قلتبان نخوری نان و دیو را مانی</p></div></div>