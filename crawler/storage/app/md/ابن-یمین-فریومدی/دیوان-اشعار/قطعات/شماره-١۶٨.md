---
title: >-
    شمارهٔ ١۶٨
---
# شمارهٔ ١۶٨

<div class="b" id="bn1"><div class="m1"><p>مدتی در طلب مال جهان کردم سعی</p></div>
<div class="m2"><p>تا بآخر خبرم شد که ز نفعش ضررست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عوض هر چه بمن داد فلک عمر ستد</p></div>
<div class="m2"><p>نکند فایده فریاد که اینش هدرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر ضایع شده و مال نماندست بجا</p></div>
<div class="m2"><p>انده عمر کنون از همه غمها بترست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینزمان یکنفس عمر بملک دو جهان</p></div>
<div class="m2"><p>نفروشم که بچشمم دو جهان مختصرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنجها یافته ام در دل ویران ز هنر</p></div>
<div class="m2"><p>زانکه بحریست ضمیرم که سراسر گهر ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مایل ملک قناعت چو شدم دانستم</p></div>
<div class="m2"><p>که هنر هر چه زیادت شود ان دردسرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بدو نیک جهان هر چه ترا پیش آید</p></div>
<div class="m2"><p>غم مخور شاد بزی زانکه جهان در گذرست</p></div></div>