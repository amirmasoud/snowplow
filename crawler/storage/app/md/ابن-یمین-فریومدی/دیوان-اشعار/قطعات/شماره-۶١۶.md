---
title: >-
    شمارهٔ ۶١۶
---
# شمارهٔ ۶١۶

<div class="b" id="bn1"><div class="m1"><p>صحبت جمغی که ما را دوستان میزیستند</p></div>
<div class="m2"><p>بر مثال صحبت اصحاب کشتی یافتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیکشان سهل انقیاد و نرمخو دیدم نخست</p></div>
<div class="m2"><p>و آخر الامر از طبیعتشان درشتی یافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوب سیرت زیستم با جمله شان و زهر یکی</p></div>
<div class="m2"><p>سر بسر گفتار چون کردار زشتی یافتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با وجود این برایشان هم نگیرم بهر آنک</p></div>
<div class="m2"><p>دوزخی فعلند و اکثر را بهشتی یافتم</p></div></div>