---
title: >-
    شمارهٔ ۵٠۴
---
# شمارهٔ ۵٠۴

<div class="b" id="bn1"><div class="m1"><p>که میبرد سخنی از زبان ابن یمین</p></div>
<div class="m2"><p>بدانجناب که اقبال زیبدش فراش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خجسته در گه شاهنشه زمین و زمان</p></div>
<div class="m2"><p>که هست بر در او شاه انجم از او باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ستوده خسرو افاق تاج دولت و دین</p></div>
<div class="m2"><p>که بادتا بابدبر سریر ملک بقاش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بروز رزم بود تیغش آب آتشبار</p></div>
<div class="m2"><p>بگاه بزم بود کلکش ابر گوهر باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگوید ار چه که دور فلک بکامم نیست</p></div>
<div class="m2"><p>ولی چه باکم ازو هر چه هست گومیباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدولت تو برین آستان همیشه مرا</p></div>
<div class="m2"><p>مرتبست و مهیا همیشه وجه معاش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپهر نیز تفاوت نمیکند چندان</p></div>
<div class="m2"><p>اگر بصلح بود با من ار کند پرخاش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولی بخانه رها کرده ام یتیمی چند</p></div>
<div class="m2"><p>ضعیف و بیحد و بیمر چو دانه خشخاش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه اهل صلاح اند جمله لیک ز فقر</p></div>
<div class="m2"><p>شدند بی سرو سامان چو مردم قلاش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فتاده اند کلحم علی و صم جایع</p></div>
<div class="m2"><p>نه هیچ صامت و ناطق نه هیچ نقد و قماش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منم حواله گه رزق انضعیفی چند</p></div>
<div class="m2"><p>که کاش نیستمی و چه سود گفتن کاش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بینوائی ایشان چو یاد میارم</p></div>
<div class="m2"><p>همی رسد بدل من هزار گونه خراش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بلطف شاملت ای شاه بنده را در یاب</p></div>
<div class="m2"><p>که تا نهفته نیازی من نگردد فاش</p></div></div>