---
title: >-
    شمارهٔ ١٣٢
---
# شمارهٔ ١٣٢

<div class="b" id="bn1"><div class="m1"><p>شکر ایزد اگر نماند زرم</p></div>
<div class="m2"><p>بحر طبعم هنوز پر گهرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نزد جوهر شناس بینا دل</p></div>
<div class="m2"><p>عقد در چون بود چه جای زرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه را در منازل علوی</p></div>
<div class="m2"><p>فکر من پیشوا و راهبرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آتش خاطر اثیر وشم</p></div>
<div class="m2"><p>شعله آفتاب یک شررست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذهن صافیم لوح محفوظ است</p></div>
<div class="m2"><p>کز رموز فلک بر او صورست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نکته های لطیف من چون می</p></div>
<div class="m2"><p>در مزاج عقول کارگرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طوطی طبع عقل اول را</p></div>
<div class="m2"><p>سخن خوشگوار من شکرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه سخن گویم از هنر با کس</p></div>
<div class="m2"><p>سخن من معرف هنرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخن اینست گو بگوی جواب</p></div>
<div class="m2"><p>هر کرا اندرین سخن نظرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کج نشین راست گو بده انصاف</p></div>
<div class="m2"><p>با جزالت نگر چگونه ترست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با چنین حالها که من دارم</p></div>
<div class="m2"><p>بهتر از جمله حالتی دگرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که اگر تاج منتی با آن</p></div>
<div class="m2"><p>بر سر من نهند درد سرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فارغم از جهان و هر چه دروست</p></div>
<div class="m2"><p>چون سر انجام جمله بر گذرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لیکن این روزگار سفله نواز</p></div>
<div class="m2"><p>نیک بد مهر و سخت کینه ورست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ناوکی کز کمان چرخ جهد</p></div>
<div class="m2"><p>سینه من به پیش آن سپرست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>میکشم جور چرخ حادثه زای</p></div>
<div class="m2"><p>وز همه حادثاتم این بترست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کافتاب جهان غیاث الدین</p></div>
<div class="m2"><p>از من دلشکسته بیخبرست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن هنر پروری که ابن یمین</p></div>
<div class="m2"><p>در ره او کمینه خاک درست</p></div></div>