---
title: >-
    شمارهٔ ١۴١
---
# شمارهٔ ١۴١

<div class="b" id="bn1"><div class="m1"><p>فخر آل مصطفی سید لطیف الدین توئی</p></div>
<div class="m2"><p>آنکه پیش رأی پیرت عقل اول کودکیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با صفا از کوکب دری نعلین تو شد</p></div>
<div class="m2"><p>هر کجا تاجی فروزان بر فراز تار کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صیقل رایت بانوار یقین روشن کند</p></div>
<div class="m2"><p>هر کرا آئینه دل تیره از زنگ شکیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر غباری کان ز نعل سم یکران تو خاست</p></div>
<div class="m2"><p>بر رخ زرنیخ فام دشمنانت آهکیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که دارد چون کمان در سر کژی با خدمتت</p></div>
<div class="m2"><p>هر مژه بر چشم شوخش راست همچون ناوکیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنده میمون جناب تست چون ابن یمین</p></div>
<div class="m2"><p>هر کجا پیروز روزی بختیاری زیر کیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ره چاکر نوازی یکزمان با بنده باش</p></div>
<div class="m2"><p>بنده را با مجلس عالیت اندک کار کیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر من از وجه شریعت هست دینی واجبت</p></div>
<div class="m2"><p>گر چه نزد همتت بسیار چیزاند کیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ملک طلق از من ستان در وجه آن تا گویمت</p></div>
<div class="m2"><p>لوحش الله زو که خاک و زر بنزد او یکیست</p></div></div>