---
title: >-
    شمارهٔ ۴٧۵
---
# شمارهٔ ۴٧۵

<div class="b" id="bn1"><div class="m1"><p>صاحبا چون یمین دولت و دین</p></div>
<div class="m2"><p>کرد خالی ز مرغ روح قفس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زو یتیم و یتیمه ئی دیدم</p></div>
<div class="m2"><p>در سرای سپنج مانده و بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون برفت او ز هر طرف برخاست</p></div>
<div class="m2"><p>خاطبانرا بر آن یتیمه هوس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاطبان کفو آن یتیمه نیند</p></div>
<div class="m2"><p>تو بفریاد این یتیم برس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زانکه تا این یتیم زنده بود</p></div>
<div class="m2"><p>ننماید یتیمه روی بکس</p></div></div>