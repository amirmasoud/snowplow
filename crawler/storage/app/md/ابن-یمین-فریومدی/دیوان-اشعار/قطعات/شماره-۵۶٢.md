---
title: >-
    شمارهٔ ۵۶٢
---
# شمارهٔ ۵۶٢

<div class="b" id="bn1"><div class="m1"><p>گر ز اسماء مقولات عشر پرسد کسی</p></div>
<div class="m2"><p>یک بیک بروی شمارم در جواب آنسؤال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوهر و کیف و کم و این و متی آورده اند</p></div>
<div class="m2"><p>وضع و ملک و نسبت است آنگاه فعل و انفعال</p></div></div>