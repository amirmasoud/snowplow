---
title: >-
    شمارهٔ ٧٧٢
---
# شمارهٔ ٧٧٢

<div class="b" id="bn1"><div class="m1"><p>مکروه طبعت آنچه شود واقع ایحکیم</p></div>
<div class="m2"><p>خوردن غمش یکیست ز غمهای زایده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا میشود بکام تو یا خود نمیشود</p></div>
<div class="m2"><p>در هر دو حال خوردن غم را چه فایده</p></div></div>