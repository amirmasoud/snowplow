---
title: >-
    شمارهٔ ۶٨٧
---
# شمارهٔ ۶٨٧

<div class="b" id="bn1"><div class="m1"><p>گفتند چو هست رزق مقسوم</p></div>
<div class="m2"><p>زحمت چه کشی ز بهر جستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که بلی ولی ازین پیش</p></div>
<div class="m2"><p>گشتست حواله گه معین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی یکی بمصر و شامست</p></div>
<div class="m2"><p>و آن دگری بروم و ارمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بنده مبین تو این تکاپوی</p></div>
<div class="m2"><p>کین حکم خدای راند بر من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی هیچ شکی نفاذ یابد</p></div>
<div class="m2"><p>حکمی که کند خدای ذوالمن</p></div></div>