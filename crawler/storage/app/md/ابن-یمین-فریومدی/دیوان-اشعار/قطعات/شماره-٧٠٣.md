---
title: >-
    شمارهٔ ٧٠٣
---
# شمارهٔ ٧٠٣

<div class="b" id="bn1"><div class="m1"><p>هدهد ار افسر به سر بر می‌نهد</p></div>
<div class="m2"><p>پای زشت او و گنداییش بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور بود شهباز را بر پای بند</p></div>
<div class="m2"><p>آن مبین چستی و زیباییش بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور شدست ابن یمین در کار سست</p></div>
<div class="m2"><p>در هنر زور و تواناییش بین</p></div></div>