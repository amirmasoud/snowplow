---
title: >-
    شمارهٔ ١۶٩
---
# شمارهٔ ١۶٩

<div class="b" id="bn1"><div class="m1"><p>مرا نیمه نانی که در خور بود</p></div>
<div class="m2"><p>بدست آورم از ره دهقنت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو دو نان نخواهم نمودن دگر</p></div>
<div class="m2"><p>برای دو نان پیش کس مسکنت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من و گنج آزادگی بعد از این</p></div>
<div class="m2"><p>زهی پادشاهی زهی سلطنت</p></div></div>