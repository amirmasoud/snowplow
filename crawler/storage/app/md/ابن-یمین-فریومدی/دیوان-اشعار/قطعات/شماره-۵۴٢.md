---
title: >-
    شمارهٔ ۵۴٢
---
# شمارهٔ ۵۴٢

<div class="b" id="bn1"><div class="m1"><p>ای افضل زمانه که در عرصه زمین</p></div>
<div class="m2"><p>افراشته ز رای تو شد رایت کمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشنو حکایت دو سه آحاد از آنکه هست</p></div>
<div class="m2"><p>نقصان عقل یکسره در غایت کمال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانم که نشنوی ز چه رو ز آنکه منزلست</p></div>
<div class="m2"><p>در شأن عقل وافر تو آیت کمال</p></div></div>