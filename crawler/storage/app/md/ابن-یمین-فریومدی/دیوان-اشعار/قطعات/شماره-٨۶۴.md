---
title: >-
    شمارهٔ ٨۶۴
---
# شمارهٔ ٨۶۴

<div class="b" id="bn1"><div class="m1"><p>گر تو بر سهل و ممتنع خواهی</p></div>
<div class="m2"><p>خویشتن را که مطلع یابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعر ابن یمین بدست آور</p></div>
<div class="m2"><p>کان همه سهل و ممتنع یابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از لطائف هر آنچه نام بری</p></div>
<div class="m2"><p>در مطاویش مجتمع یابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لفظها موجز و معانی را</p></div>
<div class="m2"><p>عرصه ئی نیک متسع یابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قصه کوته کنم گرش خوانی</p></div>
<div class="m2"><p>بر کسانی که مستمع یابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خجالت در طبایع را</p></div>
<div class="m2"><p>در محاجات منطبع یابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاطر جمله را ز ادراکش</p></div>
<div class="m2"><p>نیک مهجور و منقطع یابی</p></div></div>