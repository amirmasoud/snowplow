---
title: >-
    شمارهٔ ۶٧١
---
# شمارهٔ ۶٧١

<div class="b" id="bn1"><div class="m1"><p>خرم اینقصر دلفروز که بر روی زمین</p></div>
<div class="m2"><p>ارم و خلد برین نیست اگر هست جز این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی چه جای ارم و خلد برینست کز او</p></div>
<div class="m2"><p>هم ارم تیره همی گردد و هم خلد برین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاصه آندم که در او پادشه تاجوران</p></div>
<div class="m2"><p>باشد از بهر تماشا و طرب تخت نشین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاه یحیی جوانبخت که در هیچ هنر</p></div>
<div class="m2"><p>خرد پیر ندیدش بجهان هیچ قرین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه خاک در این کاخ ز عکس رایش</p></div>
<div class="m2"><p>آمد از روی صفا رشک ده ماء معین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وانکه از تربیتش بانی اینقصر مشید</p></div>
<div class="m2"><p>سر ز شعری گذرانید چو اینشعر متین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فخر عالم هبه الله که در دولت شاه</p></div>
<div class="m2"><p>بهزارش به ازین بخت جوان هست ضمین</p></div></div>