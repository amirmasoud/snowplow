---
title: >-
    شمارهٔ ٧۵٧
---
# شمارهٔ ٧۵٧

<div class="b" id="bn1"><div class="m1"><p>روزی ز بهر تجربه بگرفتم آینه</p></div>
<div class="m2"><p>دیدم نشان مرگ در آنجا معاینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگریستم بزاری زار از نهیب مرگ</p></div>
<div class="m2"><p>در حال بر زمین زدم از درد آینه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت آینه مرا چه زنی خیره بر زمین</p></div>
<div class="m2"><p>هرکس که زاد نیز بمیرد هر آینه</p></div></div>