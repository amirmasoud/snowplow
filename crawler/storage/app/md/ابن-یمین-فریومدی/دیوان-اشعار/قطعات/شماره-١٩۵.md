---
title: >-
    شمارهٔ ١٩۵
---
# شمارهٔ ١٩۵

<div class="b" id="bn1"><div class="m1"><p>هر که را دسترس بنقره و زر</p></div>
<div class="m2"><p>باشد و بهره بر ندارد هیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانکه بر آب زندگانی خویش</p></div>
<div class="m2"><p>تخم خیرات می نکارد هیچ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابر او بر زمین تشنه دلان</p></div>
<div class="m2"><p>خشکسال کرم نبارد هیچ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صفر باشد بپیش ابن یمین</p></div>
<div class="m2"><p>صفر را کس چه میشمارد هیچ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقد او بر محک صرافان</p></div>
<div class="m2"><p>بپشیزی عیار نارد هیچ</p></div></div>