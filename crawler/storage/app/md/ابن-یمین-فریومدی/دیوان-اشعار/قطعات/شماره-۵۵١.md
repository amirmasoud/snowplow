---
title: >-
    شمارهٔ ۵۵١
---
# شمارهٔ ۵۵١

<div class="b" id="bn1"><div class="m1"><p>چه باشد ای نفس خرم نسیم شمال</p></div>
<div class="m2"><p>که بگذری سوی آن اختر سپهر جلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدیو کشور دانش نظام ملت و دین</p></div>
<div class="m2"><p>که هست در همه فن همچو یک فنان بکمال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجز لطایف انفاس روحپرور تو</p></div>
<div class="m2"><p>نشان نداد کس اندر زمانه سحر حلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبان بنطق چو بگشاید از سلامت لفظ</p></div>
<div class="m2"><p>گمان بری که ز کوثر روان شدست زلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظر بطلعت میمون او چو بگشائی</p></div>
<div class="m2"><p>چنانکه شرط ادب باشد ای نسیم شمال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگوی قصه هجران ولی چنان بمگوی</p></div>
<div class="m2"><p>که طبع نازک او را فزاید از تو ملال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه حاجتست بتطویل شرح هجرانرا</p></div>
<div class="m2"><p>بس است یک سخن مختصر بحسب الحال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سلام من برسان پس بصورت تضمین</p></div>
<div class="m2"><p>بگوی کابن یمین گفت کای ستوده خصال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز ای آنکه نگفتیم شکر روز وصال</p></div>
<div class="m2"><p>شب فراق نخفتیم تا سحر ز خیال</p></div></div>