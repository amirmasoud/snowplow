---
title: >-
    شمارهٔ ٧٩۴
---
# شمارهٔ ٧٩۴

<div class="b" id="bn1"><div class="m1"><p>آسمان زیر پای خود آرد</p></div>
<div class="m2"><p>هر کرا هست همت عالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانکه باشد خسیس طبع و لئیم</p></div>
<div class="m2"><p>سر فرو آورد بحمالی</p></div></div>