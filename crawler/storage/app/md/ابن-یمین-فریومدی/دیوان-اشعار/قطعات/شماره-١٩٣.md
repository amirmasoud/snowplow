---
title: >-
    شمارهٔ ١٩٣
---
# شمارهٔ ١٩٣

<div class="b" id="bn1"><div class="m1"><p>غرمائی که داشتم زین پیش</p></div>
<div class="m2"><p>که از ایشان بمن رسیدی رنج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو قارون فرو شدند بخاک</p></div>
<div class="m2"><p>جمله و باز ماند ز ایشان گنج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر یکی را بغیر مظلمه نیست</p></div>
<div class="m2"><p>هیچ حاصل درین سرای سپنج</p></div></div>