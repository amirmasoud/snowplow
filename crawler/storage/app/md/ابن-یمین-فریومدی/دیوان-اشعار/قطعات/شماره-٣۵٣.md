---
title: >-
    شمارهٔ ٣۵٣
---
# شمارهٔ ٣۵٣

<div class="b" id="bn1"><div class="m1"><p>منه بر جهان دل که معشوق تست</p></div>
<div class="m2"><p>که او چون تو عاشق فراوان کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببر تا توانی ازین گرگ پیر</p></div>
<div class="m2"><p>که او دائما شیر مردان کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارد غم از چشم گریان کس</p></div>
<div class="m2"><p>که بسیار با روی خندان کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توقع مکن هیچ بهبود از او</p></div>
<div class="m2"><p>که بیمار خود را بدرمان کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حذر کن ازو همچو سیمرغ زال</p></div>
<div class="m2"><p>که این زال رستم فراوان کشد</p></div></div>