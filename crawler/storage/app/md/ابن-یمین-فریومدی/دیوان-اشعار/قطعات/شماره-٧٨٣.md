---
title: >-
    شمارهٔ ٧٨٣
---
# شمارهٔ ٧٨٣

<div class="b" id="bn1"><div class="m1"><p>اگرچه رزق مقسوم است می‌جوی</p></div>
<div class="m2"><p>که خوش فرمود این معنی معزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که یزدان رزق اگر بی‌سعی دادی</p></div>
<div class="m2"><p>به مریم کی ندا کردی که هزی</p></div></div>