---
title: >-
    شمارهٔ ٨٣٠
---
# شمارهٔ ٨٣٠

<div class="b" id="bn1"><div class="m1"><p>خداوندا همیشه بود ما را</p></div>
<div class="m2"><p>کتان و صوف و کمخاو عتابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زر و اجناس و غله اسب و استر</p></div>
<div class="m2"><p>ز پالانی و زینی و رکابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنون از جور چرخ ناسزاگار</p></div>
<div class="m2"><p>چنان گشتست حالم از خرابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر سالی بجوئی در سرایم</p></div>
<div class="m2"><p>بجز غم هیچ مالی را نیابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازینسان خانه در عالم که دیدست</p></div>
<div class="m2"><p>عفا الله خانه بوبکر ربابی</p></div></div>