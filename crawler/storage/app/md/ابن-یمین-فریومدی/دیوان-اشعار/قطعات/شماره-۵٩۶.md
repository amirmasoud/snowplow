---
title: >-
    شمارهٔ ۵٩۶
---
# شمارهٔ ۵٩۶

<div class="b" id="bn1"><div class="m1"><p>چون سرانجام زینخرابه رباط</p></div>
<div class="m2"><p>رخت بربست بایدت ناکام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس همان به بود که واو وداع</p></div>
<div class="m2"><p>متصل باشدت بسین سلام</p></div></div>