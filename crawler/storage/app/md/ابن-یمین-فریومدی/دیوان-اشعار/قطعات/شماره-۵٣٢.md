---
title: >-
    شمارهٔ ۵٣٢
---
# شمارهٔ ۵٣٢

<div class="b" id="bn1"><div class="m1"><p>سیه باد روی سپهر کبود</p></div>
<div class="m2"><p>که با کینه جفتست و با مهر طاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعیسی مریم خری میدهد</p></div>
<div class="m2"><p>بکون خری میدهد صد یراق</p></div></div>