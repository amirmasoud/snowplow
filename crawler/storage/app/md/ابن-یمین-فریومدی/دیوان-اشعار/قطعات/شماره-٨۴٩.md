---
title: >-
    شمارهٔ ٨۴٩
---
# شمارهٔ ٨۴٩

<div class="b" id="bn1"><div class="m1"><p>صحبت صاحبنظر باید که باشد با دو کس</p></div>
<div class="m2"><p>یا کریمی نامجوی و یا حکیمی راستگوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ز جود آن درین دنیا بیابد بکام دل</p></div>
<div class="m2"><p>یا ز علم این بدان دنیا بیابد آبروی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خرد داری مشو یکدم جدا زین هر دو تن</p></div>
<div class="m2"><p>ور نیابی هر دو را باری یکی زینها بجوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور یکی را هم نیابی این خود اندر عهد ماست</p></div>
<div class="m2"><p>کنج عزلت گیر و دیگر در پی دنیا مپوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خویشتن را در خطر مفکن بامید بهی</p></div>
<div class="m2"><p>کز کنار چشمه ناید تا ابد سالم سبوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عزت ار خواهی که یابی خیز چون ابن یمین</p></div>
<div class="m2"><p>آب خورسندی بجوی و دست ازین دو نان بشوی</p></div></div>