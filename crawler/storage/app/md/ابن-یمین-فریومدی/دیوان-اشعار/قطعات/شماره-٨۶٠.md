---
title: >-
    شمارهٔ ٨۶٠
---
# شمارهٔ ٨۶٠

<div class="b" id="bn1"><div class="m1"><p>کسیکه سفله و ادنای خلق بوده بود</p></div>
<div class="m2"><p>اگر بگیرد امروز ماه تا ماهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان بود که کدو همسر چنار بود</p></div>
<div class="m2"><p>ولیک ناید ازو مسند شهنشاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مریز آب رخ از بهر نان تو ایدرویش</p></div>
<div class="m2"><p>که خاک بر سر این خواجگان ناگاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برو بملک قناعت در آو ایمن باش</p></div>
<div class="m2"><p>ز کردگار جهان خواه هر چه میخواهی</p></div></div>