---
title: >-
    شمارهٔ ٧٣٨
---
# شمارهٔ ٧٣٨

<div class="b" id="bn1"><div class="m1"><p>ای کریمی که مادر ارکان</p></div>
<div class="m2"><p>چون تو فرزند راد نازاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست گوهر فشانت درگه جود</p></div>
<div class="m2"><p>از کرم داد مکرمت داده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی توان گفت ابر چون کف تست</p></div>
<div class="m2"><p>عقل داند گهر ز بیجاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال خود عرضه میکنم بر تو</p></div>
<div class="m2"><p>ای کریم جواد آزاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باده من بنده را بقرض انداخت</p></div>
<div class="m2"><p>کز جهان باد منقرض باده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر خلاصم دهی ز دست غریم</p></div>
<div class="m2"><p>گردد اسباب عیشم آماده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قافیه گر چه دال خواهد شد</p></div>
<div class="m2"><p>بعد ازین ما و روی سجاده</p></div></div>