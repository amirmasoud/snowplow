---
title: >-
    شمارهٔ ١٣۶
---
# شمارهٔ ١٣۶

<div class="b" id="bn1"><div class="m1"><p>صاحب اعظم کریم الدین سر گردنکشان</p></div>
<div class="m2"><p>ایکه در مردی و رادی چون تو سر داری نخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رأی پیرت گر چه باشد یاور اندر کارها</p></div>
<div class="m2"><p>لیک چون بخت جوانت در جهان یاری نخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فتنه را در خواب مستی سر فروشد تا بدید</p></div>
<div class="m2"><p>در جهان چون حزم هشیار تو بیداری نخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چه کرد از بهر نظم ملک و ملت رأی تو</p></div>
<div class="m2"><p>در ضمیر آسمان بر کارش انکاری نخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فتنه تا در پیش عدلت سر صراحی وش نهاد</p></div>
<div class="m2"><p>در جهان غیر از پیاله هیچ خونخواری نخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آز را در خشگسال مکرمت یکدم که دید</p></div>
<div class="m2"><p>کش ز ابر دست گوهر بارت ادراری نخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صاحبا گوهر فروشی میکنم از من بخر</p></div>
<div class="m2"><p>کاینچنین جنس نفیس از هیچ بازاری نخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش ازین گر شاعران بودند چون ابن یمین</p></div>
<div class="m2"><p>شاعری قادرتر از وی اینزمان باری نخاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بانوا دارش که در گلزار مدحت بلبلی است</p></div>
<div class="m2"><p>بلبلی چون او به دورانها ز گلزاری نخاست</p></div></div>