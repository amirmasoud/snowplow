---
title: >-
    شمارهٔ ١۴٩
---
# شمارهٔ ١۴٩

<div class="b" id="bn1"><div class="m1"><p>کسی کو طریق تواضع رود</p></div>
<div class="m2"><p>کند بر سریر شرف سلطنت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ولیکن تو جایش بدان و مکن</p></div>
<div class="m2"><p>ملک سیرتی درگه شیطنت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تواضع بود با بزرگان ادب</p></div>
<div class="m2"><p>بود با فرومایگان مسکنت</p></div></div>