---
title: >-
    شمارهٔ ٢۵
---
# شمارهٔ ٢۵

<div class="b" id="bn1"><div class="m1"><p>مرا فلک بمواعید میفریفت ولیک</p></div>
<div class="m2"><p>از آن هزار یکی با رهی نکرد وفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمانه چند گهی در هوای بوک و مگر</p></div>
<div class="m2"><p>غرور داده بامید ثم خیر مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو زان غرور بجز رنج دل نشد حاصل</p></div>
<div class="m2"><p>ملول گشت ز اصحاب منصب والا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحسب حال خود اینک بصورت تضمین</p></div>
<div class="m2"><p>بر اهل معرفت این بیت میکنم املا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا سخن ز مفاعیل و فاعلات بود</p></div>
<div class="m2"><p>من از کجا سخن سر مملکت ز کجا</p></div></div>