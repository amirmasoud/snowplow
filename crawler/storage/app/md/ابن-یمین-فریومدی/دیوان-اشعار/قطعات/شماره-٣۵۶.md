---
title: >-
    شمارهٔ ٣۵۶
---
# شمارهٔ ٣۵۶

<div class="b" id="bn1"><div class="m1"><p>محیط و مرکز افضال زین ملت و دین</p></div>
<div class="m2"><p>توئیکه چون تو جوانبخت چرخ پیر ندید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپهر اگر چه بهر سو هزار دیده گشاد</p></div>
<div class="m2"><p>بجز بدیده احول ترا نظیر ندید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال در همه عالم بگشت و همچو توئی</p></div>
<div class="m2"><p>بچار بالش امکان درون امیر ندید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قیاس کلک تو با تیر میگرفت سپهر</p></div>
<div class="m2"><p>شکوه کلک ترا هیچ کم ز تیر ندید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز راه بنده نوازی اشارتی کردی</p></div>
<div class="m2"><p>به بنده ئی که ز فرمانبری گزیر ندید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلاف رأی تو چندانکه عقل صورت بست</p></div>
<div class="m2"><p>بهیچ روی در آئینه ضمیر ندید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکسته بسته مدیحی چنین که میبینی</p></div>
<div class="m2"><p>بر این جریده نبشت ار چه دلپذیر ندید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگیر خرده بر آن بنده ئی که طاعت خویش</p></div>
<div class="m2"><p>بجز متابعت خاطر خطیر ندید</p></div></div>