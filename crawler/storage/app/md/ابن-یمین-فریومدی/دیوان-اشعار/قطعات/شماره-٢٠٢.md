---
title: >-
    شمارهٔ ٢٠٢
---
# شمارهٔ ٢٠٢

<div class="b" id="bn1"><div class="m1"><p>الهی مرا چون سرای سپنج</p></div>
<div class="m2"><p>سرانجام باید بغیری سپرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازین منزلم اندک اندک مبر</p></div>
<div class="m2"><p>که خوش مرد آنکو بیکبار مرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخواهم حیاتی که مر شخص را</p></div>
<div class="m2"><p>گر انسان بود زنده نتوان شمرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سعادت رفیق کسی کرد حق</p></div>
<div class="m2"><p>که او را ز گیتی بیکبار برد</p></div></div>