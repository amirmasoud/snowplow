---
title: >-
    شمارهٔ ٨۴
---
# شمارهٔ ٨۴

<div class="b" id="bn1"><div class="m1"><p>بنام ایزد زهی خرم سرائی</p></div>
<div class="m2"><p>که چون فردوس اعلی دلگشایست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هواش از اعتدال طبع دائم</p></div>
<div class="m2"><p>چو انفاس مسیحا جان فزایست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غبار آستانش از خوش نسیمی</p></div>
<div class="m2"><p>بسان ناف آهو مشک زایست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درو گر سوز باشد مشک و عودست</p></div>
<div class="m2"><p>و گر نالد کسی آن چنگ و نایست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز نور جام چون ماه تمامست</p></div>
<div class="m2"><p>که چون مهر از جان ظلمت زدایست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر اسرار فلک واقف توان شد</p></div>
<div class="m2"><p>که همچون جام جم گیتی نمایست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بخشد سایه سقفش سعادت</p></div>
<div class="m2"><p>چه جای سایه فر همایست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لطیف آمد عمارتهاش یکسر</p></div>
<div class="m2"><p>بلی معمار او لطف خدایست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فلک حیران شود زین بیت معمور</p></div>
<div class="m2"><p>چو بیند کش زمین آرام جایست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرای است این ندانم یا بهشتست</p></div>
<div class="m2"><p>بهشتست این ندانم یا سرایست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز خلق خوش نسیم صاحب او</p></div>
<div class="m2"><p>هوا دروی همیشه عطر سایست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صفا دروی چو رای صاحبش باد</p></div>
<div class="m2"><p>که الحق با صفا و نیک رایست</p></div></div>