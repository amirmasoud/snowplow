---
title: >-
    شمارهٔ ٣٢١
---
# شمارهٔ ٣٢١

<div class="b" id="bn1"><div class="m1"><p>غلام همت آنم که همچو باد سحر</p></div>
<div class="m2"><p>ز بار معصیت خود چو بید میلرزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگوی زاهد مغرور را که مدت عمر</p></div>
<div class="m2"><p>برسم اهل ریا طاعتی همی ورزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که بیش رنجه مدار و مرنج بهر جنان</p></div>
<div class="m2"><p>که دیده ئی پس مردن ز خاک سر بر زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخاکپای قناعت که نزد ابن یمین</p></div>
<div class="m2"><p>جهان به رنجش آزاده ئی نمیارزد</p></div></div>