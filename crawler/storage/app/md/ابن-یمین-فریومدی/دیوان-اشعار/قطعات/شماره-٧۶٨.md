---
title: >-
    شمارهٔ ٧۶٨
---
# شمارهٔ ٧۶٨

<div class="b" id="bn1"><div class="m1"><p>گفتم دلا توئیکه همه عمر بوده ئی</p></div>
<div class="m2"><p>بر مطلب و مقاصد خود کامران شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رای تو در تفحص اسرار کاینات</p></div>
<div class="m2"><p>بگذشته از مکان زبر لامکان شده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هنگام نظم گوهر شهوار خاطرت</p></div>
<div class="m2"><p>چون ابر نوبهار جواهر فشان شده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردون پیر بر تو اگر جست برتری</p></div>
<div class="m2"><p>غالب بر او بقوت بخت جوان شده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر گه که رای انور تو گشته آشکار</p></div>
<div class="m2"><p>خورشید همچو ذره بسایه نهان شده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اکنون بگوی کز چه سبب در میان خلق</p></div>
<div class="m2"><p>مردی بسان لطف و کرم بر کران شده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل از زبان دل نفسی زد براستی</p></div>
<div class="m2"><p>سرمایه حیات چو آب روان شده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت آنهمه فضایل و آداب و علم و حلم</p></div>
<div class="m2"><p>کم نیست بلکه بیشترک هم ازان شده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لیکن چه سود مایه من نیست جز هنر</p></div>
<div class="m2"><p>وان نیز نفص اکثر اهل زمان شده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دارم مفرحی که ز ترکیب گوهرش</p></div>
<div class="m2"><p>زو دل گرفته قوت و او قوت جان شده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ابن یمین بساغر تضمین چشاندت</p></div>
<div class="m2"><p>کان حسب حال اوست بگیتی عیان شده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بازار فضل کاسد و سرمایه در تلف</p></div>
<div class="m2"><p>نرخ متاع فاتر و سودش زیان شده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ما را هنر متاع و خریدار عیبجوی</p></div>
<div class="m2"><p>ز آنست نام ما بجهان بی نشان شده</p></div></div>