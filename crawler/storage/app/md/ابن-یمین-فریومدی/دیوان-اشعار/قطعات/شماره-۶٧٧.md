---
title: >-
    شمارهٔ ۶٧٧
---
# شمارهٔ ۶٧٧

<div class="b" id="bn1"><div class="m1"><p>سخن فرزند جان و بکر فکرست</p></div>
<div class="m2"><p>بهر نا اهل و دون نتوانش دادن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین فرزند دشوارت دهد دست</p></div>
<div class="m2"><p>بود عیبی ز دست آسانش دادن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخن بکری بود پرورده فکر</p></div>
<div class="m2"><p>که بر جان میتوان فرمانش دادن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین بکری ز عاقل نیست لایق</p></div>
<div class="m2"><p>بدست این و دست آنش دادن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خصوصا آنکه چون کابینش خواهند</p></div>
<div class="m2"><p>بود دشوار تر از جانش دادن</p></div></div>