---
title: >-
    شمارهٔ ٨۶٣
---
# شمارهٔ ٨۶٣

<div class="b" id="bn1"><div class="m1"><p>گر تمتع ترا ز نقره و زر</p></div>
<div class="m2"><p>اینقدر بس که قابض آنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک سخن بیغرض ز من بشنو</p></div>
<div class="m2"><p>غم خود خور که سخت نادانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه نهی سیم و زر بدشواری</p></div>
<div class="m2"><p>تا خورد دشمنت بآسانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر مراد از زرت وجود زرست</p></div>
<div class="m2"><p>فرض کردم که سر بسر کانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ز گنج خودت نصیبی نیست</p></div>
<div class="m2"><p>تو مر آن گنج را نگهبانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بشنو این نکته را ز ابن یمین</p></div>
<div class="m2"><p>که ترا هست مشفق جانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیم آن به که رغم دشمن را</p></div>
<div class="m2"><p>در ره دوستان بر افشانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مال تو داد دشمنت بدهد</p></div>
<div class="m2"><p>گر تو زو داد دوست نستانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمع جمع انگهی توانی شد</p></div>
<div class="m2"><p>کافکنی سیم در پریشانی</p></div></div>