---
title: >-
    شمارهٔ ۶٢١
---
# شمارهٔ ۶٢١

<div class="b" id="bn1"><div class="m1"><p>قدوه اهل کرم ای زبده آزادگان</p></div>
<div class="m2"><p>اکرم الاخوان شهاب الدین که بادی دوستکام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ره چاکر نوازی قصه ئی اصغا نمای</p></div>
<div class="m2"><p>کرده از بیم ملالت در وی ایجازی تمام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنده با جمعی خواص مجلس روحانیان</p></div>
<div class="m2"><p>خلوتی دارد مصفا از کدورات عوام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موضعی از خرمی زیباتر از باغ ارم</p></div>
<div class="m2"><p>وز ره امن و فراغت غیرت دار السلام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیک در وی پای بند صحبت اصحاب نیست</p></div>
<div class="m2"><p>این کنایت هیچ دانی از چه باشد از مدام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همتت گر ضامن اسباب جمعیت شود</p></div>
<div class="m2"><p>چون ثریا منخرط کردند در سلک نظام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ورنه ابناء الکرام اندر پی بنت الکروم</p></div>
<div class="m2"><p>چون بنات النعش بگریزند از هم و السلام</p></div></div>