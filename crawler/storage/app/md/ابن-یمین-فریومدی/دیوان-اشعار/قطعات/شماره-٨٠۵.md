---
title: >-
    شمارهٔ ٨٠۵
---
# شمارهٔ ٨٠۵

<div class="b" id="bn1"><div class="m1"><p>با حریفان بر بساط دهر ای نیکوخصال</p></div>
<div class="m2"><p>راستی کن پیشه همچون سرو اگر آزاده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بکوشی در شرف ز آبا زیادت می‌شوی</p></div>
<div class="m2"><p>از موالید سه تا چون بهترین افتاده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ده‌هزارت خصم اگر باشد چو اندر حصن صبر</p></div>
<div class="m2"><p>خانه گیری خوش نشین کان جمله را استاده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تکیه کمتر کن بر آمال طویل ابن یمین</p></div>
<div class="m2"><p>جز برین عمر قصیرش چون بنا ننهاده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مضیق ششدر حرص ار نیفتی مهره‌وار</p></div>
<div class="m2"><p>بند هر منصوبه را کآرد فلک بگشاده‌ای</p></div></div>