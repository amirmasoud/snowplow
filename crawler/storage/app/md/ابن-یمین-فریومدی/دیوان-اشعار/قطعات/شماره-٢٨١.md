---
title: >-
    شمارهٔ ٢٨١
---
# شمارهٔ ٢٨١

<div class="b" id="bn1"><div class="m1"><p>دامن مرد کاهلی چو گرفت</p></div>
<div class="m2"><p>گله از گردش زمانه کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مطرب از کار چون فروماند</p></div>
<div class="m2"><p>خشم بر گوشه چغانه کند</p></div></div>