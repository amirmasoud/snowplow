---
title: >-
    شمارهٔ ۶٠٨
---
# شمارهٔ ۶٠٨

<div class="b" id="bn1"><div class="m1"><p>سپهر مهر جلالت جلال دولت و دین</p></div>
<div class="m2"><p>توئی که رأی ترا شاه انجم است غلام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسیکه سر ننهد پیش تو صراحی وار</p></div>
<div class="m2"><p>مدام در دل او باد خون ناب چو جام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بمن رسید بشارت که رأی آن داری</p></div>
<div class="m2"><p>که حال بنده رسانی ز تفرقه بنظام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان مبشر فرخنده من چنین گفتم</p></div>
<div class="m2"><p>که عرضه دار بدان مقتدای جمله کرام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که بس عجب نبود کز هزار فرسنگی</p></div>
<div class="m2"><p>نسیم جود تو من بنده را رسد بمشام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>علی الخصوص که قرنی زیادتست که من</p></div>
<div class="m2"><p>که بر جناب تو دارم چو آستانه مقام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اساس تربیتم کرده ئی و خوش کاریست</p></div>
<div class="m2"><p>تمام کن که بود نظم کار در اتمام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هلال اگر چه خوش آید بچشم خلق ولی</p></div>
<div class="m2"><p>ز روی حسن کجا میرسد بماه تمام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بکوش ابن یمین را بکام دل برسان</p></div>
<div class="m2"><p>ز لطف خویش که بادت جهان همیشه بکام</p></div></div>