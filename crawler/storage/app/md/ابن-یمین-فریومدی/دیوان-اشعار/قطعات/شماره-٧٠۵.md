---
title: >-
    شمارهٔ ٧٠۵
---
# شمارهٔ ٧٠۵

<div class="b" id="bn1"><div class="m1"><p>هیچ دانی که مردمی چه بود</p></div>
<div class="m2"><p>روز قوت فروتنی کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیم و زر بیقیاس بخشیدن</p></div>
<div class="m2"><p>گاه قدرت غضب فرو خوردن</p></div></div>