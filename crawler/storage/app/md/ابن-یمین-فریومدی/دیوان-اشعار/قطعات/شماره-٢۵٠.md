---
title: >-
    شمارهٔ ٢۵٠
---
# شمارهٔ ٢۵٠

<div class="b" id="bn1"><div class="m1"><p>پنج روزیکه در کشاکش غم</p></div>
<div class="m2"><p>در سرای سپنج خواهی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر فزون از کفاف میطلبی</p></div>
<div class="m2"><p>طالب درد و رنج خواهی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مال کز وی تمتعت نبود</p></div>
<div class="m2"><p>چه کنی مار گنج خواهی بود</p></div></div>