---
title: >-
    شمارهٔ ٢۴٧
---
# شمارهٔ ٢۴٧

<div class="b" id="bn1"><div class="m1"><p>بشمس دولت و دین مفخر زمان و زمین</p></div>
<div class="m2"><p>سلام من که رساندم پیام من که برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لطیف طبع جهان آنکه گر رسد سوی گل</p></div>
<div class="m2"><p>نسیم لطف وی از رشک پیرهن بدرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روان ز نفحه اخلاق او بیاساید</p></div>
<div class="m2"><p>چو از نسیم بهاری که بر چمن گذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگویدش که بساطی بتو نشان دادند</p></div>
<div class="m2"><p>که دل بجانب او همچون جان ز تن بپرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسیط خاک بجست و چنان بساط نیافت</p></div>
<div class="m2"><p>رهی که تا ز پی نرد خویشتن بخرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرش بابن یمین از ره کرم بخشی</p></div>
<div class="m2"><p>تر از فرط سخا حاتم ز من شمرد</p></div></div>