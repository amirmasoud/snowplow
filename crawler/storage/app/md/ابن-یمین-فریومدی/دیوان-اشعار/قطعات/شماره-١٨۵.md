---
title: >-
    شمارهٔ ١٨۵
---
# شمارهٔ ١٨۵

<div class="b" id="bn1"><div class="m1"><p>هر کس که حال دینی و عقبی شناخت او</p></div>
<div class="m2"><p>زین بس ملول حال بدان سخت مایل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیزیکه هست مرتبه اولش هلاک</p></div>
<div class="m2"><p>ترسان بود ز آخر او کو نه غافل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و آنچیز کآخرش بجز از مرگ هیچ نیست</p></div>
<div class="m2"><p>دانی که رغبتش نکند هر که عاقل است</p></div></div>