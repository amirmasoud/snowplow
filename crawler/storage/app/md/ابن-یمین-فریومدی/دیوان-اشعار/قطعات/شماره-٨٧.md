---
title: >-
    شمارهٔ ٨٧
---
# شمارهٔ ٨٧

<div class="b" id="bn1"><div class="m1"><p>بهر روزی بهر دری چه روی</p></div>
<div class="m2"><p>ای ز ضعف دل اعتقاد تو سست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه بری آبروی چون نانی</p></div>
<div class="m2"><p>نخورد کس از آنچه روزی تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نیوشی و گر نه من گفتم</p></div>
<div class="m2"><p>گفتنی ها تمام راست و درست</p></div></div>