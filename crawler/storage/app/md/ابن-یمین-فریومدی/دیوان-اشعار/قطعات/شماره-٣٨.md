---
title: >-
    شمارهٔ ٣٨
---
# شمارهٔ ٣٨

<div class="b" id="bn1"><div class="m1"><p>دو مشفق اند ادیب و طبیب بر سر تو</p></div>
<div class="m2"><p>نگاه دار بعزت دل ادیب و طبیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز درد خسته شوی گر بنالد از تو طبیب</p></div>
<div class="m2"><p>بجهل بسته شوی گر برنجد از تو ادیب</p></div></div>