---
title: >-
    شمارهٔ ٢٧٩
---
# شمارهٔ ٢٧٩

<div class="b" id="bn1"><div class="m1"><p>دو دوست با هم اگر یکدلند در همه حال</p></div>
<div class="m2"><p>هزار طعنه دشمن به نیم جو نخرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور اتفاق نمایند و عزم جزم کنند</p></div>
<div class="m2"><p>سزد که قلعه افلاک را زهم بدرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مثال این بنمایم ترا ز مهره نرد</p></div>
<div class="m2"><p>یکان یکان بسوی خانه راه می نبرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ولی دو مهره چو هم پشت هم یکدگر گردند</p></div>
<div class="m2"><p>دگر تپانچه دشمن بهیچ رو نخورند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکوش ابن یمین دوستی بدست آور</p></div>
<div class="m2"><p>که دشمنان سوی یک کس بصد کژی نگرند</p></div></div>