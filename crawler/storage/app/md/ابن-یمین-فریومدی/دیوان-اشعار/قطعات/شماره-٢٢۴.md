---
title: >-
    شمارهٔ ٢٢۴
---
# شمارهٔ ٢٢۴

<div class="b" id="bn1"><div class="m1"><p>آصف ثانی علاء دولت و دین</p></div>
<div class="m2"><p>چون سلیمان جهان بکام تو باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بود عمر جاودانت چو خضر</p></div>
<div class="m2"><p>چشمه آبحیات جام تو باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهره ازهر کنیز مجلس تست</p></div>
<div class="m2"><p>خسرو سیارگان غلام تو باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زینت و زیب و بهای سکه ملک</p></div>
<div class="m2"><p>از لقب فرخ و ز نام تو باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفته قضا تیغ آبدار ترا</p></div>
<div class="m2"><p>جفن عدو کمترین نیام تو باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خصم جگر تشنه را در آتش غم</p></div>
<div class="m2"><p>آبخور از چشمه حسام تو باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر شفق و صبح کز افق بدمد</p></div>
<div class="m2"><p>فرخی فال صبح و شام تو باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا که بود سبز خنک چرخ شموس</p></div>
<div class="m2"><p>ابلق تند زمانه رام تو باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وز کرمت زیر پای ابن یمین</p></div>
<div class="m2"><p>باره رهوار خوش لکام تو باد</p></div></div>