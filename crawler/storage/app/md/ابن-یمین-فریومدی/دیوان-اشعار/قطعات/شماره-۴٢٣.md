---
title: >-
    شمارهٔ ۴٢٣
---
# شمارهٔ ۴٢٣

<div class="b" id="bn1"><div class="m1"><p>در باب تواضع آنچه دانی</p></div>
<div class="m2"><p>با خلق جهان بجای می آر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کافزوده کند ترا تواضع</p></div>
<div class="m2"><p>نزدیک کریم طبع مقدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اما چو لئیم طبع باشد</p></div>
<div class="m2"><p>افتد ز تواضعت به پندار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بروی نظر از تکبر افکن</p></div>
<div class="m2"><p>و آن جزء ادب تمام بشمار</p></div></div>