---
title: >-
    شمارهٔ ٢١٢
---
# شمارهٔ ٢١٢

<div class="b" id="bn1"><div class="m1"><p>آسمان قدر وزیرا چو تو بر روی زمین</p></div>
<div class="m2"><p>تا زمان هست نبودست و بزرگی نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه ملک کرمی در بر خود فرزین وار</p></div>
<div class="m2"><p>جای دادی و پسندد ز تو هرکس شنود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنده بر رقعه اخلاص چو رخ راست رواست</p></div>
<div class="m2"><p>نه چو فرزین که از اینگوشه بدان گوشه دود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من چو پیلم که فرا پیش تو از بیشه خویش</p></div>
<div class="m2"><p>نه چو پشه که دل من بهمه کس گرود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مفتی شرع مکارم چو توئی هست روا</p></div>
<div class="m2"><p>کز بساط کرمت بنده پیاده برود</p></div></div>