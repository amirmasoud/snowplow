---
title: >-
    شمارهٔ ۶٣٠
---
# شمارهٔ ۶٣٠

<div class="b" id="bn1"><div class="m1"><p>من نیم چون مرد سالوس و فریب</p></div>
<div class="m2"><p>پرده ناموس خود خود میدرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قلب خود را سکه رندی زده</p></div>
<div class="m2"><p>پیش صرافان عالم میبرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ز دخت رز بریدم زهد نیست</p></div>
<div class="m2"><p>مصلحت را راه او می نسپرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوی خون آید ز وصل دخت رز</p></div>
<div class="m2"><p>تا بمانم سوی او می ننگرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیک هر وقت از زمرد گون کنب</p></div>
<div class="m2"><p>کوری افعی غم را میخورم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا بر اینقانونم ای ابن یمین</p></div>
<div class="m2"><p>کس نبینی ز اهل معنی منکرم</p></div></div>