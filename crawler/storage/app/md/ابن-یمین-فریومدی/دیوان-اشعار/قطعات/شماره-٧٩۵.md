---
title: >-
    شمارهٔ ٧٩۵
---
# شمارهٔ ٧٩۵

<div class="b" id="bn1"><div class="m1"><p>آنکس که بود بعلم و حکمت عالی</p></div>
<div class="m2"><p>بر گفته او نقیض آرم حالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوید که خلاء نزد خرد هست محال</p></div>
<div class="m2"><p>کندوله من هست زگندم خالی</p></div></div>