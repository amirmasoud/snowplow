---
title: >-
    شمارهٔ ١۶۶
---
# شمارهٔ ١۶۶

<div class="b" id="bn1"><div class="m1"><p>مطبخی ایست ناگوار مرا</p></div>
<div class="m2"><p>شهره گشته بآش پختن کست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بشام از سحر بود بنگی</p></div>
<div class="m2"><p>وز سحر تا بشام باشد مست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه از مایعات یافت بریخت</p></div>
<div class="m2"><p>و آنچه از جامدات جست شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بقبض آورد عصای کلیم</p></div>
<div class="m2"><p>ور بود سوی ذوالفقارش دست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دایم آنش بود تنور آشاب</p></div>
<div class="m2"><p>اکره الجیش این بود پیوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنگر تا بغیر ابن یمین</p></div>
<div class="m2"><p>اینچنین مطبخی کسی را هست</p></div></div>