---
title: >-
    شمارهٔ ۵۶٩
---
# شمارهٔ ۵۶٩

<div class="b" id="bn1"><div class="m1"><p>وارث املاک اینجو سعد دین مسعود آنک</p></div>
<div class="m2"><p>عرضه خواهم داشتن در خدمت او شرح حال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خراسان چون نهادم پای در ملک عراق</p></div>
<div class="m2"><p>بود اول کس که کردم بر درش حط رحال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راستی را نیک توجیهی بترحیبم بگفت</p></div>
<div class="m2"><p>آنچنان کآید ز ذات پاک هر نیکو خصال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بخرجی احتیاجم دید دیناری هزار</p></div>
<div class="m2"><p>از کرم ده شانزده انعام کرد اما عوال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد از آن آنرا حوالت کرد با فرزانه ئی</p></div>
<div class="m2"><p>گر بزی روشندلی صاحب کمال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راستی را آنچه من دیدم ز نا اهلی او</p></div>
<div class="m2"><p>شرح آن نتوان که بیرونست از حد مقال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با چنان نیکی که اول خواجه سعد الدین نمود</p></div>
<div class="m2"><p>حیف بود آخر زدن بر طبل بدنامی دوال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوئیا کز من گناهی بس بزرگ آمد پدید</p></div>
<div class="m2"><p>کو چنین ناگه مرا افکند با سگ در جوال</p></div></div>