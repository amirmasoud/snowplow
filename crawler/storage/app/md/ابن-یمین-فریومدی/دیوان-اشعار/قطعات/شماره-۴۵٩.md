---
title: >-
    شمارهٔ ۴۵٩
---
# شمارهٔ ۴۵٩

<div class="b" id="bn1"><div class="m1"><p>همانا که شاهنشه بی نظیر</p></div>
<div class="m2"><p>کزو تازه شد رسم تاج و سریر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمور خان شهنشاه جمشید فر</p></div>
<div class="m2"><p>که هم تاج بخش است و هم تختگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر اخلاص من بنده یاد آورد</p></div>
<div class="m2"><p>ببخت جوان بیند و رأی پیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که ابن یمین بر گل مدح کس</p></div>
<div class="m2"><p>جز او گر زند بلبل آسا نفیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه بظاهر بود نام غیر</p></div>
<div class="m2"><p>ولیکن مراد او بود در ضمیر</p></div></div>