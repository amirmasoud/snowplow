---
title: >-
    شمارهٔ ٢٠۵
---
# شمارهٔ ٢٠۵

<div class="b" id="bn1"><div class="m1"><p>ای دل ار چند در سفر خطرست</p></div>
<div class="m2"><p>کس سفر بیخطر کجا یابد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه اندر سفر بدست آید</p></div>
<div class="m2"><p>مرد آن در حضر کجا یابد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که در سایه گشت گوشه نشین</p></div>
<div class="m2"><p>تابش ماه و خور کجا یابد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وانکه در بحر غوطه می نخورد</p></div>
<div class="m2"><p>سلک در و گهر کجا یابد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وانکه پهلو تهی کند ازکان</p></div>
<div class="m2"><p>صره سیم و زر کجا یابد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز کز آشیان برون نپرد</p></div>
<div class="m2"><p>بر شکاری ظفر کجا یابد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر هنرمند گوشه ئی گیرد</p></div>
<div class="m2"><p>کام دل از هنر کجا یابد</p></div></div>