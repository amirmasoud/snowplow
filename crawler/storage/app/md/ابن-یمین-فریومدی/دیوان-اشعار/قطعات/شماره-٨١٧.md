---
title: >-
    شمارهٔ ٨١٧
---
# شمارهٔ ٨١٧

<div class="b" id="bn1"><div class="m1"><p>برای نعمت دنیا مکش مذلت خلق</p></div>
<div class="m2"><p>که نزد اهل خرد زین سبب خسی باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خون دیده غذا گر کنی از آن خوشتر</p></div>
<div class="m2"><p>که زیر منت احسان ناکسی باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر قبول کنی پند من از آن خوشتر</p></div>
<div class="m2"><p>و گر نه همچو سگان در بدر بسی باشی</p></div></div>