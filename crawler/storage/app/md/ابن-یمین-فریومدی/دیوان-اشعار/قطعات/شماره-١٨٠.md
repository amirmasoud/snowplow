---
title: >-
    شمارهٔ ١٨٠
---
# شمارهٔ ١٨٠

<div class="b" id="bn1"><div class="m1"><p>هر یکی از شهان بوقت شکار</p></div>
<div class="m2"><p>صید دیگر کند بقوت بخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه یحیی چو عزم صید کند</p></div>
<div class="m2"><p>شهریاران رباید از سر تخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باد پاینده تا جهان گیرد</p></div>
<div class="m2"><p>بمساعی بخت و بازوی سخت</p></div></div>