---
title: >-
    شمارهٔ ۶٢٢
---
# شمارهٔ ۶٢٢

<div class="b" id="bn1"><div class="m1"><p>گر بدست آید مرا در تیه حیرت یک جوین</p></div>
<div class="m2"><p>قانعم منت پذیر از من و از سلوی نیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور پلاسی باشدم از نقش منت بی علم</p></div>
<div class="m2"><p>طالب دیبای چین و اطلس و خارا نیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دم فرو بستم بکلی از مدیح و از غزل</p></div>
<div class="m2"><p>بشنو از من کز چه معنی در پی اینها نیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از کسی لطفی نمی بینم که گویم مدح او</p></div>
<div class="m2"><p>بر جمال دلبری هم عاشق و شیدا نیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوبهار شادمانی و گل عشرت نماند</p></div>
<div class="m2"><p>بلبلم اندر خزان غم از آن گویا نیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بود در کنج خلوت فکر بکرم همنشین</p></div>
<div class="m2"><p>راست گو ابن یمین در جنت المأوا نیم</p></div></div>