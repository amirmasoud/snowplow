---
title: >-
    شمارهٔ ٨٧٩ - القطعه فی الشعر
---
# شمارهٔ ٨٧٩ - القطعه فی الشعر

<div class="b" id="bn1"><div class="m1"><p>نگار ما هر خم چون گره زند بر موی</p></div>
<div class="m2"><p>دل هزار در آرد بعقده هر موی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز روی لطف چو نیلوفرست بر سر آب</p></div>
<div class="m2"><p>فراز عارض آنسرو یاسمنبر موی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بغیر زلف و رخ همچو سنبل و گل او</p></div>
<div class="m2"><p>بر آفتاب که دیدست سایه گستر موی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسیم غالیه زلف از چه داد بر رخ او</p></div>
<div class="m2"><p>چو بوی خوش ندهد بر فراز آذر موی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندانم از چه بر آن گونه تیره دل گردد</p></div>
<div class="m2"><p>نگشته دور دمی ز آنرخ منور موی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد آفتاب فلک زیر ابر غالیه فام</p></div>
<div class="m2"><p>چو بر عذار پراکنده کرد دلبر موی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بعینه مژه اشکبار من بودی</p></div>
<div class="m2"><p>گر آن نگار بیاراستی بگوهر موی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسان پیکر زار و نزار من باشد</p></div>
<div class="m2"><p>گه خضاب کند گر کسی مزعفر موی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهر که بر رخ زیبای اوست آشفته</p></div>
<div class="m2"><p>بسان ابن یمین آمدست بر سر موی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نمیکند صنما بعد ازین طبیعت من</p></div>
<div class="m2"><p>مسامحت که نشاند ردیف دیگر موی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از آنکه افضل عالم غیاث ملت و دین</p></div>
<div class="m2"><p>که گاه نظم شکافد برأی انور موی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز راه بنده نوازی بجملگی کردست</p></div>
<div class="m2"><p>ردیف گفته خود در مدیح چاکر موی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو موی بر سر اصحاب با دو هست که نیست</p></div>
<div class="m2"><p>کسیکه در سخن آرد چو آن سخنور موی</p></div></div>