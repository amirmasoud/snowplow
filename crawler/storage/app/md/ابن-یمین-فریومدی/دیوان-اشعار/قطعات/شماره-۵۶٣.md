---
title: >-
    شمارهٔ ۵۶٣
---
# شمارهٔ ۵۶٣

<div class="b" id="bn1"><div class="m1"><p>گرت میل باشد که در پارسی</p></div>
<div class="m2"><p>همی دال را باز دانی ز ذال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگویم یکی ضابطه یاد گیر</p></div>
<div class="m2"><p>که آن را نیابی به گیتی همال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر پیش ازو حرف عله بود</p></div>
<div class="m2"><p>به جز ذال معجم ندارد مجال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور آن حرف جز حرف عله بود</p></div>
<div class="m2"><p>نگه کن که آن حرف را چیست حال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر هست ساکن تواش دال دان</p></div>
<div class="m2"><p>وگر نه همان ذال معجم نه دال</p></div></div>