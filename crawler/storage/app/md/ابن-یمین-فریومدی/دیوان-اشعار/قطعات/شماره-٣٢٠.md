---
title: >-
    شمارهٔ ٣٢٠
---
# شمارهٔ ٣٢٠

<div class="b" id="bn1"><div class="m1"><p>غیاث دولت و دین آنکه طوطی جانرا</p></div>
<div class="m2"><p>ز شکر سخن خوش اداش چینه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان فضل که پیر خرد به نسبت او</p></div>
<div class="m2"><p>تهی ز جمله فضائل چو طفل دینه بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهال مهر و یم در میان جان همه عمر</p></div>
<div class="m2"><p>بسان دانه دل در صمیم سینه بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سفینه ئی برهی داد پر ز بحر گهر</p></div>
<div class="m2"><p>سفینه ئی که در او روح را سکینه بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سفینه ئی که در الفاظ عذب او معنی</p></div>
<div class="m2"><p>بلطف همچو می صاف در قنینه بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه گفت گفت که دیباچه ئی نویس بر او</p></div>
<div class="m2"><p>که گنجهای گهر اندر او دفینه بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جواب دادم و گفتم مگر نئی آگاه</p></div>
<div class="m2"><p>ز من که با من از آنسان فلک بکینه بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که پیش صدمت دورش بنای هستی من</p></div>
<div class="m2"><p>چنانکه بر گذر سنگ آبگینه بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا که با من از اینسان ستم کند گردون</p></div>
<div class="m2"><p>چه جای کتبت دیباچه سفینه بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر قبول کند عذر من خداوندم</p></div>
<div class="m2"><p>ز جانش ابن یمین بنده کمینه بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ور اعتذار منش دلپذیر می ناید</p></div>
<div class="m2"><p>روا نباشد و شرط کرم چنین نه بود</p></div></div>