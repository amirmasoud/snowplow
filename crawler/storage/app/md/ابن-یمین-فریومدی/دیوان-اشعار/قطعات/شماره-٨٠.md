---
title: >-
    شمارهٔ ٨٠
---
# شمارهٔ ٨٠

<div class="b" id="bn1"><div class="m1"><p>از مال مهتری نبود کسب فضل کن</p></div>
<div class="m2"><p>کانکس که فاضلست بگیتی مسو دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جهل با غناست همه عاران کس است</p></div>
<div class="m2"><p>با فقر ساختیم که فخر محمدست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز آمدم از آنچه هوا بود رهنماش</p></div>
<div class="m2"><p>عقلم نمود راه که این عود احمدست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون با قضا مرام موافق نهاده اند</p></div>
<div class="m2"><p>زندان مرا مقابل صرح ممرد است</p></div></div>