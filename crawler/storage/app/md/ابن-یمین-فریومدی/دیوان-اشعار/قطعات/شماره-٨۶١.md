---
title: >-
    شمارهٔ ٨۶١
---
# شمارهٔ ٨۶١

<div class="b" id="bn1"><div class="m1"><p>گر کسی با تو بد کند زنهار</p></div>
<div class="m2"><p>جز بنیکی جزای آن نکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بدی گر کسی کند سودی</p></div>
<div class="m2"><p>از نکوئی تو هم زیان نکنی</p></div></div>