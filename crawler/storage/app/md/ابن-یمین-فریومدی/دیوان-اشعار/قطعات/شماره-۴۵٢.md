---
title: >-
    شمارهٔ ۴۵٢
---
# شمارهٔ ۴۵٢

<div class="b" id="bn1"><div class="m1"><p>مرا نام اگر نیک و گر بد بود</p></div>
<div class="m2"><p>چو رفتم از آنم چه فخر و چه عار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی را سزد فخر و ار عار بود</p></div>
<div class="m2"><p>که ماند ز من در جهان یادگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس از من اگر هر چه باشد رواست</p></div>
<div class="m2"><p>چو من دامن افشانده ام زین غبار</p></div></div>