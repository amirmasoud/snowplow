---
title: >-
    شمارهٔ ۵١۴
---
# شمارهٔ ۵١۴

<div class="b" id="bn1"><div class="m1"><p>منم آنکس که بهر گوهر فضل</p></div>
<div class="m2"><p>گشته در بحر فکرتم غواص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدحت گفته های من گویند</p></div>
<div class="m2"><p>اهل تمییز از عوام و خواص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر عطارد نکوهدم شاید</p></div>
<div class="m2"><p>ز آنکه القاص لایحب القاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نه از طبع جوهریم بدی</p></div>
<div class="m2"><p>سیم گشتی بقدر کم ز رصاص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه زین پیش بود اهل نفاق</p></div>
<div class="m2"><p>اینزمان میزند دم از اخلاص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیک ممکن نگرددش بحیل</p></div>
<div class="m2"><p>رستن از من و لات حین مناص</p></div></div>