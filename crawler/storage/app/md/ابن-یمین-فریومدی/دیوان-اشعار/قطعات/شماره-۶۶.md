---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>ایزدا مستحق عفو توأم</p></div>
<div class="m2"><p>ز آنکه من بنده را گناه بسیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه تو خود را عفو همیدانی</p></div>
<div class="m2"><p>پس برین قول بیخلاف بایست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عفو کردن پس از گناه بود</p></div>
<div class="m2"><p>بیگنه را بعفو حاجت نیست</p></div></div>