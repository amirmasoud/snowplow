---
title: >-
    شمارهٔ ۵۵٣
---
# شمارهٔ ۵۵٣

<div class="b" id="bn1"><div class="m1"><p>دیدم پریر ساده غلام بخارئیی</p></div>
<div class="m2"><p>زیبا و دلفریب و نکو فعل و خوب قول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمم بر اوفتاد طمع کردم اندرو</p></div>
<div class="m2"><p>سرپوش بردم از سر خوان بی هراس و هول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم بغیر بوسه دهی هیچ دیگرم</p></div>
<div class="m2"><p>بشنید و خوش برآمد و خندید و گفت هول</p></div></div>