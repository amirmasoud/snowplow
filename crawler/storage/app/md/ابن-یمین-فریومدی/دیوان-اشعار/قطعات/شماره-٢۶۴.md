---
title: >-
    شمارهٔ ٢۶۴
---
# شمارهٔ ٢۶۴

<div class="b" id="bn1"><div class="m1"><p>جهان کشور دانش شه ممالک فضل</p></div>
<div class="m2"><p>جمال دولت و دین صاحب کریم نژاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توئی که منشی گردون بسان شاگردان</p></div>
<div class="m2"><p>خطابت از ره تعظیم میکند استاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو کلکت از پی نظم جهان میان دربست</p></div>
<div class="m2"><p>چه عقده بود که از کار مملکت نگشاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهیچ دور بجز ذات پر فضائل تو</p></div>
<div class="m2"><p>نشان نداد کسی آدم فرشته نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بپیش نفحه اخلاق روحپرور تو</p></div>
<div class="m2"><p>بیان معجز عیسی بود سراسر باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بندگیت کسی کو چو نی میان بربست</p></div>
<div class="m2"><p>بسان سرو شد از بند بندگی آزاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمینه بنده داعی جاهت ابن یمین</p></div>
<div class="m2"><p>که هست مهر تو از بدو فطرتش همزاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بحضرت تو فرستاد یک سفینه چنانک</p></div>
<div class="m2"><p>ازو شود دل غمناک اهل دانش شاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدان امید که چون بگذرد برو نظرت</p></div>
<div class="m2"><p>ز حال بنده درگاه خویشت آید یاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بحر فضل توئی زان سفینه داد بتو</p></div>
<div class="m2"><p>که کس سفینه بجز سوی بحر نفرستاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همیشه تا اثر فضل در جهان باشد</p></div>
<div class="m2"><p>بجز جناب تو مأوی اهل فضل مباد</p></div></div>