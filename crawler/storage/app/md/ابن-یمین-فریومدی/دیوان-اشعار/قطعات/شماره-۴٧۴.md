---
title: >-
    شمارهٔ ۴٧۴
---
# شمارهٔ ۴٧۴

<div class="b" id="bn1"><div class="m1"><p>سعی در تنقیص قدر خویش کرد</p></div>
<div class="m2"><p>هر که کرد اهمال در تکمیل نفس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بارها ای نفس نافرمان ترا</p></div>
<div class="m2"><p>گفته ام کز حرص بر دنیا مچفس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آبرو خواهی چو خاک افتاده باش</p></div>
<div class="m2"><p>نی چو آتش از هوا در تاب و تفس</p></div></div>