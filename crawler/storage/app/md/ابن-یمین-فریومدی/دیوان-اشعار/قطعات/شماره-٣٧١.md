---
title: >-
    شمارهٔ ٣٧١
---
# شمارهٔ ٣٧١

<div class="b" id="bn1"><div class="m1"><p>هر که در اصل بد نهاد افتاد</p></div>
<div class="m2"><p>هیچ نیکی ازو مدار امید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آنکه هرگز بجهد نتوان کرد</p></div>
<div class="m2"><p>از کلاغ سیاه باز سفید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دون پرستی مکن که می نشود</p></div>
<div class="m2"><p>در صفا هیچ ذره چون خورشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کرا دور چرخ جامی داد</p></div>
<div class="m2"><p>بابصارت نکشت چون جمشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بید را گر بپرورند چو عود</p></div>
<div class="m2"><p>بر نیاید نسیم عود از بید</p></div></div>