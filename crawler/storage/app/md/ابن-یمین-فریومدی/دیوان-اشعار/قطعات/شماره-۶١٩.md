---
title: >-
    شمارهٔ ۶١٩
---
# شمارهٔ ۶١٩

<div class="b" id="bn1"><div class="m1"><p>فیلسوفی که من در اسرارش</p></div>
<div class="m2"><p>ببد و نیک بود می محرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت پندی سه چار فرمودست</p></div>
<div class="m2"><p>شاه کسری سر ملوک عجم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اولین پند آنکه مالش نیست</p></div>
<div class="m2"><p>هیچ کامی نیابد از عالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دومین پند آنکه جفتش نیست</p></div>
<div class="m2"><p>نیستش دل بهیچ رو خرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سومین آنکه نیست فرزندش</p></div>
<div class="m2"><p>نیست تا هست پشت او محکم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چارمین آنکه هر که این هر سه</p></div>
<div class="m2"><p>نیستش نیستش بگیتی غم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر تو خواهی که خوش گذاری عمر</p></div>
<div class="m2"><p>آنچه یابی بده ز بیش و ز کم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پند چارم گزین که آخر حرفیست</p></div>
<div class="m2"><p>کاندرو هست بیغمی مدغم</p></div></div>