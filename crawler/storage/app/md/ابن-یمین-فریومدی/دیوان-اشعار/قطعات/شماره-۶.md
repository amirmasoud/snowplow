---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ای بسا دوستان که بگزیدم</p></div>
<div class="m2"><p>تا بدیشان بمالم اعد را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راستی را بسعیشان ایام</p></div>
<div class="m2"><p>داد مالش ولی بسی ما را</p></div></div>