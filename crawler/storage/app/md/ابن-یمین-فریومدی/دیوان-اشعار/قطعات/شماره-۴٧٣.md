---
title: >-
    شمارهٔ ۴٧٣
---
# شمارهٔ ۴٧٣

<div class="b" id="bn1"><div class="m1"><p>ز اقتضای دور گردون گر پدید آید ترا</p></div>
<div class="m2"><p>چند روزی در جهان بر قول و فعلی دسترس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشنو از ابن یمین پندی بغایت سودمند</p></div>
<div class="m2"><p>با سلامت عمر اگر داری بسر بردن هوس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدمگوی و بدمکن با هیچکس در هیچ حال</p></div>
<div class="m2"><p>تا نه بدگوید کست نی باشدت بیمی ز کس</p></div></div>