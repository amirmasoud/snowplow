---
title: >-
    شمارهٔ ١٣
---
# شمارهٔ ١٣

<div class="b" id="bn1"><div class="m1"><p>خرد چون کند دوستی با کسی</p></div>
<div class="m2"><p>که با دشمنان باشد او را صفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدار از بدان چشم نیکی از آنک</p></div>
<div class="m2"><p>شکر کس نخورد از نی بوریا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبان بره آن به که دارد نگاه</p></div>
<div class="m2"><p>از آن سگ که با گرگ گشت آشنا</p></div></div>