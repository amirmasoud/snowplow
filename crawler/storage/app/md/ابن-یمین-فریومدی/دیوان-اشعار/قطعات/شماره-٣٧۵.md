---
title: >-
    شمارهٔ ٣٧۵
---
# شمارهٔ ٣٧۵

<div class="b" id="bn1"><div class="m1"><p>هر که را داد نعمتی ایزد</p></div>
<div class="m2"><p>و او از و نی چشاند و نه چشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک الموت را بقا بادا</p></div>
<div class="m2"><p>تا ز قهرش بیک نفس بکشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و آنگه آنرا بسان جان از وی</p></div>
<div class="m2"><p>بستاند بدیگری بخشد</p></div></div>