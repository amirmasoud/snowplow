---
title: >-
    شمارهٔ ٢٣
---
# شمارهٔ ٢٣

<div class="b" id="bn1"><div class="m1"><p>فراخ دستی ز اندازه مگذران چندان</p></div>
<div class="m2"><p>که آفتاب معاشت بدل شود بسها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه نیز پیرو امساک را زبونی کن</p></div>
<div class="m2"><p>چنانکه دامن همت دهی ز دست رها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وسط گزین که گزیدست سید عربی</p></div>
<div class="m2"><p>بدین حدیث که خیر الامور اوسطها</p></div></div>