---
title: >-
    شمارهٔ ٢۴٢
---
# شمارهٔ ٢۴٢

<div class="b" id="bn1"><div class="m1"><p>با خرد گفتم ای مدبر کار</p></div>
<div class="m2"><p>که بدانش چو تو نشان ندهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیست حکمت که از خزانه غیب</p></div>
<div class="m2"><p>برگ کاهی به راستان ندهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخسیسان دهند نعمت و ناز</p></div>
<div class="m2"><p>اهل دلرا بجان امان ندهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچه با جاهلان سفله دهند</p></div>
<div class="m2"><p>با بزرگان نکته دان ندهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنج و دولت دهند نادانرا</p></div>
<div class="m2"><p>با هنر پیشه نیم نان ندهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سفله بر صدر و اهل دانش را</p></div>
<div class="m2"><p>بغلط ره بر آستان ندهند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گجروان را دهند خرمن ها</p></div>
<div class="m2"><p>قوت یکشب به نیکوان ندهند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگسان را دهند شکر و قند</p></div>
<div class="m2"><p>با همایان جز استخوان ندهند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل گفت اینحدیث نشنیدی</p></div>
<div class="m2"><p>هر که را این دهند آن ندهند</p></div></div>