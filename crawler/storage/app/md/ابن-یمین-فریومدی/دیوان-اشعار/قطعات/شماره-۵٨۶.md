---
title: >-
    شمارهٔ ۵٨۶
---
# شمارهٔ ۵٨۶

<div class="b" id="bn1"><div class="m1"><p>بحمدالله مرا هستند فرزندان روحانی</p></div>
<div class="m2"><p>که حوراشان بپروردست در آغوش و رضوان هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سراسر بر جهانگیری چو شاه اختران قادر</p></div>
<div class="m2"><p>عراق آورده زیر حکم اقلیم خراسان هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دمی با هر که بنشینند بگشایند بر طبعش</p></div>
<div class="m2"><p>ز صورتهای پر معنی در صد باغ و بستان هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر مجلس که بگشاید یکی زیشان در حکمت</p></div>
<div class="m2"><p>دمادم اهل مجلس را کند خندان و گریان هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز لطف هر یکی گشته است غرق اندر خوی خخلت</p></div>
<div class="m2"><p>نم سرچشمه زمزم چه زمزم آب حیوان هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سه چارم نیز میباشند فرزندان جسمانی</p></div>
<div class="m2"><p>ولی من فارغم زیشان و از من نیز ایشان هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز فرزندان جسمانی ندارم چشم جمعیت</p></div>
<div class="m2"><p>کزیشان روز و شب هستم دل افکار و پریشان هم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بقای جان فرزندان روحانی من بادا</p></div>
<div class="m2"><p>که من زیشان شدم شهره بایران و بتوران هم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کر ای آن کنند الحق که چون ابن یمین سازم</p></div>
<div class="m2"><p>یکایک را وطن در دل نه در دل بلکه در جان هم</p></div></div>