---
title: >-
    شمارهٔ ۵۵٠
---
# شمارهٔ ۵۵٠

<div class="b" id="bn1"><div class="m1"><p>تو بدی میکنی و میخواهی</p></div>
<div class="m2"><p>کآیدت نیک پیش در همه حال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیک پاداش بد نخواهد شد</p></div>
<div class="m2"><p>بگذر ایخواجه از خیال محال</p></div></div>