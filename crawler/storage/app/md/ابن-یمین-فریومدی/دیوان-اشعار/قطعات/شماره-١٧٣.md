---
title: >-
    شمارهٔ ١٧٣
---
# شمارهٔ ١٧٣

<div class="b" id="bn1"><div class="m1"><p>مرا مذهب اینست گیری تو نیز</p></div>
<div class="m2"><p>همین ره گرت مردی و مردمیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بعد از نبی مقتدای بحق</p></div>
<div class="m2"><p>علی ابن بوطالب هاشمیست</p></div></div>