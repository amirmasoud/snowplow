---
title: >-
    شمارهٔ ٣٩١
---
# شمارهٔ ٣٩١

<div class="b" id="bn1"><div class="m1"><p>اهل خرد که دنیی فانی طلب کنند</p></div>
<div class="m2"><p>جز بر سه چیز نیست در آن حالشان نظر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا بر کمال عزت و یا اکتساب مال</p></div>
<div class="m2"><p>یا بر حصول راحت این نفس خیره سر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهی که دسترس بودت بر مراد دل</p></div>
<div class="m2"><p>بشنو بگوش هوش ز من پند معتبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر آرزوی عزت جاوید بایدت</p></div>
<div class="m2"><p>بر کن دل از جهان که متاعیست مختصر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وز بهر سیم و زر پی دنیا همی دوی</p></div>
<div class="m2"><p>باری بکوش تا بودت عقل راهبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پایت مگر بگنج قناعت فرو شود</p></div>
<div class="m2"><p>یا در کفت چو خاک شود بیعیار زر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور میل خاطرت سوی آسایش دل است</p></div>
<div class="m2"><p>پس جان خود مکن سپر ناوک خطر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زحمت مکش که روزی خلقان مقدرست</p></div>
<div class="m2"><p>آنرا بجهد می نتوان کرد بیشتر</p></div></div>