---
title: >-
    شمارهٔ ١٧٢
---
# شمارهٔ ١٧٢

<div class="b" id="bn1"><div class="m1"><p>معنی طلب که بر در و دیوار صورتست</p></div>
<div class="m2"><p>مغزست نزد مردم دانا هنر نه پوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون پیاز جمله تن ار پوست گشته ئی</p></div>
<div class="m2"><p>گند دماغ از تو نه دشمن خرد نه دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معنی نو طلب منگر جامه کهن</p></div>
<div class="m2"><p>بگذر ز صورت بد اگر سیرتش نکوست</p></div></div>