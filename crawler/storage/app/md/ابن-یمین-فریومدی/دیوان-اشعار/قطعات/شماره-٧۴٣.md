---
title: >-
    شمارهٔ ٧۴٣
---
# شمارهٔ ٧۴٣

<div class="b" id="bn1"><div class="m1"><p>ای نسیم سپیده دم بگذر</p></div>
<div class="m2"><p>از سر لطف بامداد پگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجناب رفیع افضل دهر</p></div>
<div class="m2"><p>قطب اقطاب شیخ فضل الله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه درشان او بود منزل</p></div>
<div class="m2"><p>آیت رفعت و جلالت جاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وانگه باشد بخدمتش بر در</p></div>
<div class="m2"><p>پشت گردون بسان حلقه دو تاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وانکه خورشید رای روشن او</p></div>
<div class="m2"><p>ببرد تیرگی ز چهره ماه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وانکه در حادثات اهل هنر</p></div>
<div class="m2"><p>باجنابش همی برند پناه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برسان بندگی بصد اخلاص</p></div>
<div class="m2"><p>از من دوستدار دولتخواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس بگویش چو رای ا نور تو</p></div>
<div class="m2"><p>هست از سر کانیات اگاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چیست موجب که نیست آگه از انک</p></div>
<div class="m2"><p>هست حال رهی عظیم تباه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زین بتر هم شود اگر نکند</p></div>
<div class="m2"><p>همت خواجه سوی بنده نگاه</p></div></div>