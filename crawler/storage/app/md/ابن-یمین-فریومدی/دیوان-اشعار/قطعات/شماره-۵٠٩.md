---
title: >-
    شمارهٔ ۵٠٩
---
# شمارهٔ ۵٠٩

<div class="b" id="bn1"><div class="m1"><p>هر نکته که از گفتن آن بیم گزندست</p></div>
<div class="m2"><p>از دشمن و از دوست نهان دار چو جانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر گاه که خواهی بتوان گفت و چو گفتی</p></div>
<div class="m2"><p>هر وقت که خواهی نتون کرد نهانش</p></div></div>