---
title: >-
    شمارهٔ ۴٨۵
---
# شمارهٔ ۴٨۵

<div class="b" id="bn1"><div class="m1"><p>بستم احرام آستانه شاه</p></div>
<div class="m2"><p>بامید سخاوت عامش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آنکه مشهور بود در عالم</p></div>
<div class="m2"><p>صیت انعام و ذکر اکرامش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود نهادند پیش من کاری</p></div>
<div class="m2"><p>که بود نام بد سرانجامش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من گرفتم ز فقر بپذیرم</p></div>
<div class="m2"><p>آنچه در ننگ افکند نامش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همت شه رضا چگونه دهد</p></div>
<div class="m2"><p>که ز توزیع باشد انعامش</p></div></div>