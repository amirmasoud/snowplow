---
title: >-
    شمارهٔ ۶٣٩
---
# شمارهٔ ۶٣٩

<div class="b" id="bn1"><div class="m1"><p>ایدل ار ننگ داری از نقصان</p></div>
<div class="m2"><p>جز سلوک ره کمال مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه عقلت بدان دهد دستور</p></div>
<div class="m2"><p>جز بدان کار اشتغال مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرف نفس اگر همی خواهی</p></div>
<div class="m2"><p>با فرومایه قیل و قال مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بامیدی که شم خیر بود</p></div>
<div class="m2"><p>از در راحت ارتحال مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم که فردا رسد مخور امروز</p></div>
<div class="m2"><p>ترک شادی بنقد حال مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرض نفس نفیس را هرگز</p></div>
<div class="m2"><p>در پی مال پایمال مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منت از دوست بهر دنیائی</p></div>
<div class="m2"><p>ور بود حاتم احتمال مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجز و بیچارگی بهیچ سبیل</p></div>
<div class="m2"><p>دشمن ار هست پور زال مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بشنو این پندها ز ابن یمین</p></div>
<div class="m2"><p>ور مفید است از آن ملال مکن</p></div></div>