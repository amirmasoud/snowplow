---
title: >-
    شمارهٔ ۶٨٣
---
# شمارهٔ ۶٨٣

<div class="b" id="bn1"><div class="m1"><p>که باشد آنکه رساند ز راه لطف و کرم</p></div>
<div class="m2"><p>رسالتی بجناب خدایگان از من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کراست قدرت آن کین سخن فرو خواند</p></div>
<div class="m2"><p>بسمع اشرف سردار شه نشان از من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امیر عالم عادل که به ز مدحت او</p></div>
<div class="m2"><p>کسی سخن نشنیدست در جهان از من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان رافت و رحمت امیر شیخ علی</p></div>
<div class="m2"><p>که ذکر خیر کند دائمش زبان از من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگویدش که ز شه داشتم توقع آنک</p></div>
<div class="m2"><p>هم آشکار کند یاد و هم نهان از من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر ز طالع شوریده نیست بهر چرا</p></div>
<div class="m2"><p>نکرد یاد شهنشاه کامران از من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روا بود که جهان کرم ستلمش بیک</p></div>
<div class="m2"><p>مدیح خود بستاند برایگان از من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی که با من از اینسان کند تو خود دانی</p></div>
<div class="m2"><p>که واجبش چه بود لیک ناید آن از من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منم که جز بمدیحش سخن روا نکنم</p></div>
<div class="m2"><p>علاقه تا نکند منقطع روان از من</p></div></div>