---
title: >-
    شمارهٔ ٢٨٧
---
# شمارهٔ ٢٨٧

<div class="b" id="bn1"><div class="m1"><p>دوش در تنگنای عقل مرا</p></div>
<div class="m2"><p>با خرد صحبت اتفاق افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم از راه لطف نوعی کن</p></div>
<div class="m2"><p>تا شوم از غمان دهر آزاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت یاری طلب که در عالم</p></div>
<div class="m2"><p>شهر بند وفا کند بنیاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جهان هیچکس ندیدم کو</p></div>
<div class="m2"><p>عاقبت دوستی بباد نداد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون چنین است هر که در عالم</p></div>
<div class="m2"><p>فرد گردد خداش خیر دهاد</p></div></div>