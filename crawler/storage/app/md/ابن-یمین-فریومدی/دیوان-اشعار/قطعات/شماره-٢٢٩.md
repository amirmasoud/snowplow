---
title: >-
    شمارهٔ ٢٢٩
---
# شمارهٔ ٢٢٩

<div class="b" id="bn1"><div class="m1"><p>اقبال را بقا نبود دل بر او مبند</p></div>
<div class="m2"><p>عمری که در غرور گذاری هبا بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور نیست باورت ز من اینک تو خود ببین</p></div>
<div class="m2"><p>اقبال را چو قلب کنی لابقا بود</p></div></div>