---
title: >-
    شمارهٔ ٣٩٠
---
# شمارهٔ ٣٩٠

<div class="b" id="bn1"><div class="m1"><p>ای شهنشاه بختیار که هست</p></div>
<div class="m2"><p>در همه کار دولتت یاور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکجا رایت تو روی آرد</p></div>
<div class="m2"><p>نصرت ایزدش بود رهبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهریاران نهند بهر شرف</p></div>
<div class="m2"><p>بر سر از خاک پای تو افسر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ره بنده پروری بشنو</p></div>
<div class="m2"><p>قصه پر ز غصه چاکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لاشه اسبی فتاد مرکب من</p></div>
<div class="m2"><p>بروش از همه خران کمتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست آن گاو گوش اشتر دل</p></div>
<div class="m2"><p>اسب صورت ولی بمعنی خر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من بر آنم که داند این معنی</p></div>
<div class="m2"><p>شاه گیتی پناه دین پرور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که چو من شهسوار معنی را</p></div>
<div class="m2"><p>نبود خر مناسب و در خور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برهان بنده را ازین غم و رنج</p></div>
<div class="m2"><p>تا نگهداردت ز غم داور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرکبی باد پای بخش مرا</p></div>
<div class="m2"><p>بسرین فربه و میان لاغر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راهواری بخوشروی چون آب</p></div>
<div class="m2"><p>رهنوردی بتیزی صرصر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا نباشد گمان که با بنده</p></div>
<div class="m2"><p>بی عنایت شدست شاه مگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این یک اسبم ببخش و عمرت باد</p></div>
<div class="m2"><p>تا ببخشی چنین هزار دگر</p></div></div>