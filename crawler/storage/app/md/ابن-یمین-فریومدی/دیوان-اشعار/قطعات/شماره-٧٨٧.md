---
title: >-
    شمارهٔ ٧٨٧
---
# شمارهٔ ٧٨٧

<div class="b" id="bn1"><div class="m1"><p>ای پیک پی خجسته نسیم سحرگهی</p></div>
<div class="m2"><p>لطفی کن از برای دل خسته رهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذر بدانجناب که از لطف صاحبش</p></div>
<div class="m2"><p>یا بی نشان خلد چو در وی قدم نهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یعنی جناب حضرت شاهی که می نهد</p></div>
<div class="m2"><p>شیر فلک ز هیبت او سر بر و بهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرخنده تاج دولت و دین کاهل فضل را</p></div>
<div class="m2"><p>دوران اوست موسم آسایش و بهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اول ببوس خاک درش وانگه اینسخن</p></div>
<div class="m2"><p>بر گوی و مگذر از سر ایجاز و کوتهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گو با وجود جود تو آن کو مراد دل</p></div>
<div class="m2"><p>بر آستان غیر تو جوید ز ابلهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دنب لاشه سگ طلب دنبه میکند</p></div>
<div class="m2"><p>و آماس باز می نشناسد ز فربهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اکنونکه روزگار برآشفت و فتنه گشت</p></div>
<div class="m2"><p>و آفاق شد ز مردی و وز مردمی تهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مردی بسان رستم دستان تو میکنی</p></div>
<div class="m2"><p>داد کرم چو حاتم طائی تو میدهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون در زمانه ز اهل هنر با خبر توئی</p></div>
<div class="m2"><p>بادا ز حال ابن یمین نیزت آگهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا خرگه سپهر منور بود بماه</p></div>
<div class="m2"><p>بادت معاشرت همه با ماه خر گهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رایت بهر طریق که تابد عنان عزم</p></div>
<div class="m2"><p>اقبال در رکاب تو بادا بهمرهی</p></div></div>