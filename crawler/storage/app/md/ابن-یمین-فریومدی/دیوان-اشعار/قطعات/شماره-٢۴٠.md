---
title: >-
    شمارهٔ ٢۴٠
---
# شمارهٔ ٢۴٠

<div class="b" id="bn1"><div class="m1"><p>ببزم آصف جمشید رتبت</p></div>
<div class="m2"><p>گهی کابن یمین از پا نشیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندارد خویشتن را در مضیقی</p></div>
<div class="m2"><p>ز نا اهلی اگر ادنا نشیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فروتر پایه دارد مرد نادان</p></div>
<div class="m2"><p>اگر چه برتر از دانا نشیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندارد قدر گوهر هیچ خاشاک</p></div>
<div class="m2"><p>بدریا گر چه او بالا نشیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زحل هرگز نگردد سعد اکبر</p></div>
<div class="m2"><p>بجای ار چند ازو اعلی نشیند</p></div></div>