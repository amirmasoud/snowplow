---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>آنکه از برق سحاب کرم شامل او</p></div>
<div class="m2"><p>تا ابد طی را دل و جان در تابست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آنکه خصمش بمثل گر بود از آهن و روی</p></div>
<div class="m2"><p>درگه معرکه لرزنده تر از سیمابست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیغ چون آب وی و سینه پر آتش خصم</p></div>
<div class="m2"><p>دشنه رستم دستان و دل سهرابست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اتفاق همه خلقان جهان هست بر آنک</p></div>
<div class="m2"><p>پهلوانی که بدین زور وتوان و تابست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حارس و حامی اقلیم هنر شیخ علیست</p></div>
<div class="m2"><p>که به بیداری او چشم فلک در خوابست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بجز این ورد ندارند گروهی که مدام</p></div>
<div class="m2"><p>رویشان از پی طاعت بسوی محرابست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که سرافراز جهان شیخ علی باقی باد</p></div>
<div class="m2"><p>کز نم ابر کفش کشت امل شادابست</p></div></div>