---
title: >-
    شمارهٔ ۵۴۴
---
# شمارهٔ ۵۴۴

<div class="b" id="bn1"><div class="m1"><p>ای برادر هیچ اگر داری ز حال خود خبر</p></div>
<div class="m2"><p>پس چرا باید که باشد یکدمت پروای قال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تو حد کوش و وقت خویشرا ضایع مکن</p></div>
<div class="m2"><p>ا ز تکثر می نیاید هیچ حاصل جز ملال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچه داری گر بر آن افزون کنی نقصان تست</p></div>
<div class="m2"><p>و آنچه دانی گر بیفزائی بر آن یابی کمال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل کارآگاه کو را میبرازد سروری</p></div>
<div class="m2"><p>حیف باشد گر کنی از بهر مالش پایمال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مال اگر ز ابن یمین مایل بغیری شد چه شد</p></div>
<div class="m2"><p>گفته ام با دل که از بهر منال ایدل منال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مال را زآغاز فطرت در طبیعت هست میل</p></div>
<div class="m2"><p>واضع اسماش گوئی بهر این گفتست مال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی بمعشوقی که هر دم عاشق او دیگریست</p></div>
<div class="m2"><p>ملتفت گردند از عین بصیرت اهل حال</p></div></div>