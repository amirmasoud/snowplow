---
title: >-
    شمارهٔ ۵٩۵
---
# شمارهٔ ۵٩۵

<div class="b" id="bn1"><div class="m1"><p>چو در دنیا نخواهد ماند چیزی</p></div>
<div class="m2"><p>ز بد کردار و نیکوکار جز نام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بکسب نیکنامی کوش و نیکی</p></div>
<div class="m2"><p>که نیکو را نکو باشد سرانجام</p></div></div>