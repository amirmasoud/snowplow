---
title: >-
    شمارهٔ ١١٠
---
# شمارهٔ ١١٠

<div class="b" id="bn1"><div class="m1"><p>در جهان هر چه میکنند عوام</p></div>
<div class="m2"><p>نزد خاصان رسوم و عاداتست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انقطاع از رسوم این حضرات</p></div>
<div class="m2"><p>اتصال همه سعاداتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راه تقلید محض را بستن</p></div>
<div class="m2"><p>افتتاح در مراداتست</p></div></div>