---
title: >-
    شمارهٔ ٨٠٢
---
# شمارهٔ ٨٠٢

<div class="b" id="bn1"><div class="m1"><p>ای صاحبی که یابد از لطف دلگشایت</p></div>
<div class="m2"><p>محبوس چاه محنت از بند غم رهائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر پرتوی ز رایت بر خاک تیره افتد</p></div>
<div class="m2"><p>هر ذره آفتابی گردد بروشنائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنم که فکر بکرم با زیور مدیحت</p></div>
<div class="m2"><p>مشهور عالمی شد در حسن و دلربائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیوسته ام بمهرت وز دیگران گسسته</p></div>
<div class="m2"><p>در دیده خاک پایت کرده بتوتیائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم بصیقل لطف آئینه دلم را</p></div>
<div class="m2"><p>روزی بشادکامی از زنگ غم زدائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان پس که چند گاهی بودم بر تو گفتی</p></div>
<div class="m2"><p>در حضرتت بخوانم اصغا اگر نمائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر هرگزم نبینی در خاطرت نیایم</p></div>
<div class="m2"><p>وانگه که پیشت آیم گوئی فلان کجائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرگز مباد بندی بر کارت اوفتاده</p></div>
<div class="m2"><p>از کارم ار چه بندی هرگز نمیگشائی</p></div></div>