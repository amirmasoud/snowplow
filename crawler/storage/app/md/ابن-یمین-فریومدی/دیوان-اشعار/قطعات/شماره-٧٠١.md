---
title: >-
    شمارهٔ ٧٠١
---
# شمارهٔ ٧٠١

<div class="b" id="bn1"><div class="m1"><p>هر چند روزگار کند پست مرد را</p></div>
<div class="m2"><p>از همت بلند نشاید بکاستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رزقت چو از خزانه خالق مقدرست</p></div>
<div class="m2"><p>دون همتی بود ز در خلق خواستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنشین بعزت از پی کاریکه کار تست</p></div>
<div class="m2"><p>تا پیش کس بپای نباید بخاستن</p></div></div>