---
title: >-
    شمارهٔ ٢۶٩
---
# شمارهٔ ٢۶٩

<div class="b" id="bn1"><div class="m1"><p>حبذا روزگار بیعقلان</p></div>
<div class="m2"><p>کز خرابی عقل آبادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل و غم را بهم گذاشته اند</p></div>
<div class="m2"><p>وز حماقت همیشه دلشادند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا عقل هست شادی نیست</p></div>
<div class="m2"><p>عقل و غم هر دو توامان زادند</p></div></div>