---
title: >-
    شمارهٔ ٢۴٣
---
# شمارهٔ ٢۴٣

<div class="b" id="bn1"><div class="m1"><p>باغبانی بنفشه می انبود</p></div>
<div class="m2"><p>گفتم ای گوژ پشت جامه کبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چه رسمیست در جهان که توراست</p></div>
<div class="m2"><p>پیر نا گشته بر شکستی زود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت پیران شکسته دهرند</p></div>
<div class="m2"><p>در جوانی شکسته باید بود</p></div></div>