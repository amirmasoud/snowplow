---
title: >-
    شمارهٔ ٧٠۴
---
# شمارهٔ ٧٠۴

<div class="b" id="bn1"><div class="m1"><p>هر کرا یاد بنده میآید</p></div>
<div class="m2"><p>بکرم بندگی من برسان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان بمفرد نمیدهم زحمت</p></div>
<div class="m2"><p>که همی ترسم از ملالتشان</p></div></div>