---
title: >-
    شمارهٔ ٩٣
---
# شمارهٔ ٩٣

<div class="b" id="bn1"><div class="m1"><p>براه راست توانی رسید در مقصود</p></div>
<div class="m2"><p>تو راست باش که هر دولتی که هست تراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو چوب راست بر آتش دریغ میداری</p></div>
<div class="m2"><p>کجا بآتش دوزخ برند مردم راست</p></div></div>