---
title: >-
    شمارهٔ ۴٨۴
---
# شمارهٔ ۴٨۴

<div class="b" id="bn1"><div class="m1"><p>اول ببین مواقع اقدام خویشتن</p></div>
<div class="m2"><p>در نه قدم از آن پس و با احتیاط باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که بی درنگ بمقصود خود رسی</p></div>
<div class="m2"><p>پیوسته مستقیم رو بر صراط باش</p></div></div>