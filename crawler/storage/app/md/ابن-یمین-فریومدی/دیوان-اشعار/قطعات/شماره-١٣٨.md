---
title: >-
    شمارهٔ ١٣٨
---
# شمارهٔ ١٣٨

<div class="b" id="bn1"><div class="m1"><p>عالیجناب حضرت دستور شه نشان</p></div>
<div class="m2"><p>کز وی بجاه گنبد گردون فروتر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایوان او کجاست ندانم که بر ثری</p></div>
<div class="m2"><p>کز آستانش گنبد گردان فروتر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در نزهت از مکانت آن بزم دلگشای</p></div>
<div class="m2"><p>صد پا به پیش روضه رضوان فروتر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در وی که هیچ دیده ندیدست ناپسند</p></div>
<div class="m2"><p>آخر چرا فرشته ز شیطان فروتر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرعون برترست ز موسی بمرتبت</p></div>
<div class="m2"><p>هارون بقدر و جاه ز هامان فروتر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مپسند صاحبا که در آنمجلس رفیع</p></div>
<div class="m2"><p>دانا کسی بود که ز نادان فروتر است</p></div></div>