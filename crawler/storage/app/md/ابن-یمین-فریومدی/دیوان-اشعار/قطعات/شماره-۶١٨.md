---
title: >-
    شمارهٔ ۶١٨
---
# شمارهٔ ۶١٨

<div class="b" id="bn1"><div class="m1"><p>ظفر نیافت خردمند در جهان روزی</p></div>
<div class="m2"><p>بهیچ فائده دیگر از حضور کرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمانه هیچ تعدی نکرد با خاصان</p></div>
<div class="m2"><p>بتر ز صحبت مشتی عوام کالانعام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عهد کردم که بعد ازین همه عمر</p></div>
<div class="m2"><p>غصه روز و رنج شب نکشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس خود را ادب کنم بهنر</p></div>
<div class="m2"><p>رنج و الام بی ادب نکشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قصه خود بنزد کس نبرم</p></div>
<div class="m2"><p>منت هیچ زن جلب نکشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لقمه و گوشه ئی کفاف منست</p></div>
<div class="m2"><p>گوشه ئی گیرم و تعب نکشم</p></div></div>