---
title: >-
    شمارهٔ ١
---
# شمارهٔ ١

<div class="b" id="bn1"><div class="m1"><p>ای نسیم صبحدم بگذر بخاک درگهی</p></div>
<div class="m2"><p>کز غبارش چشم جان گشتست نورانی مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درگه آنکس که تصدیقش کند قاضی عقل</p></div>
<div class="m2"><p>گر کند دعوی که میزیبد جهانبانی مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آصف ثانی علاء ملک و دین کز احتشام</p></div>
<div class="m2"><p>یاد داد ایامش ایام سلیمانی مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب ملک و ملت آسمان داد و دین</p></div>
<div class="m2"><p>آنکه آید ذات پاکش ظل یزدانی مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون زمین بوسیده باشی قصه ابن یمین</p></div>
<div class="m2"><p>عرضه کن تا از تو باشد منت جانی مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کو بهجرت استجازت کردم از درگاه تو</p></div>
<div class="m2"><p>تا بلطف از تنگنای عیش برهانی مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اینسخن دیدم که نامد رأی انور را پسند</p></div>
<div class="m2"><p>وز جبینش گشت پیدا خشم پنهانی مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یعلم الله کین از آن کردم که گفتم من کیم</p></div>
<div class="m2"><p>تا بپیش خویش خوانی یا ز پس رانی مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ورنه آندم یابم آزادی ز بند روزگار</p></div>
<div class="m2"><p>کز عداد بندگان خویش گردانی مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حاش لله کز گدائی درت تا زنده ام</p></div>
<div class="m2"><p>دور گردم ور بود امید سلطانی مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم درینمعنی ز درج غیر دری یافتم</p></div>
<div class="m2"><p>بر فشانم چون بدست آمد بآسانی مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاک درگاه تو نفروشم بملک هر دو کون</p></div>
<div class="m2"><p>آنچنان نادان نیم آخر تو میدانی مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جاودان اقبال بادت تا بفضل کردگار</p></div>
<div class="m2"><p>از سپهر ظلم پرور داد بستانی مرا</p></div></div>