---
title: >-
    شمارهٔ ٧۶٩
---
# شمارهٔ ٧۶٩

<div class="b" id="bn1"><div class="m1"><p>گر حال نیک خواهی فرزند را همیشه</p></div>
<div class="m2"><p>آموزش ای برادر قرآن و خط و پیشه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیرا که پیشش آید روزیکه کارش افتد</p></div>
<div class="m2"><p>چون پیشه ئی نداند بیل و کلنگ و تیشه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کو خطی نخواند یا پیشه ئی نداند</p></div>
<div class="m2"><p>بس گاو و خر چراند در کوه و دشت و بیشه</p></div></div>