---
title: >-
    شمارهٔ ۴٣٢
---
# شمارهٔ ۴٣٢

<div class="b" id="bn1"><div class="m1"><p>شاه جهان طغایتمور خان تاجبخش</p></div>
<div class="m2"><p>کز قدر و جاه بر سر کیوان نهد سریر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهی که از جلالت جاه و علو قدر</p></div>
<div class="m2"><p>تیرش دبیر میسزد و مشتری وزیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از گلشن مکارم اخلاق او برد</p></div>
<div class="m2"><p>باد صبا بعرصه عالم دم عبیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از یمن کردگار بتأیید بخت یافت</p></div>
<div class="m2"><p>چیزی که گنج داشت در امکان بجز نظیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیرون کشد ز عرصه عالم عدوش را</p></div>
<div class="m2"><p>احداث دهر بر صفت موی از خمیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیکان آب داده او روزکار زار</p></div>
<div class="m2"><p>بیرون جهد ز جوشن و خفتان که از حریر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حکمی که بر سپهر کند بخت نوجوانش</p></div>
<div class="m2"><p>جز امتثال آن نکند این سپهر پیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گشت بد سگال وی از زندگی نفور</p></div>
<div class="m2"><p>زانش همی رسد بفلک هر زمان نفیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در تیره شب بدیده موران فرو کند</p></div>
<div class="m2"><p>شست وی از کمان کیانی هزار تیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از بیقرار خامه او ملک را قرار</p></div>
<div class="m2"><p>ای چشم دین و ملک بنور رخش قریر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاها توئی که بر فلک اجرام سعد را</p></div>
<div class="m2"><p>در نیک و بد موافق رایت بود مسیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از عکس تیغ سبز تو شد کور دشمنت</p></div>
<div class="m2"><p>افعی بلی ز عکس زمرد شود ضریر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خصم تو گر شود همه تن جامه چون پیاز</p></div>
<div class="m2"><p>زودش فلک برهنه بسازد بسان سیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بهر وجود جود تو پیدا کند فلک</p></div>
<div class="m2"><p>مالی که هست گر ز قلیل و اگر کثیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا دست درفشانت ببخش در آمدست</p></div>
<div class="m2"><p>در کاینات می نتوان یافت یک فقیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در روزگار عدل تو از یمن رافتت</p></div>
<div class="m2"><p>نوشد بره دلیر ز پستان شیر شیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دوران بعهد عدل تو در حفظ کاروان</p></div>
<div class="m2"><p>از ترک رهزن فلکی میدهد سفیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شاها منم که بلبل خوشگوی طبع من</p></div>
<div class="m2"><p>در گلشن مدایح تو میزند صفیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شعر مرا تو قدر شناسی که در جهان</p></div>
<div class="m2"><p>چون رای روشنت نبود ناقدی بصیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دارم طمع که ابن یمین را عنایتت</p></div>
<div class="m2"><p>گردد زجور حادثه دهر دستگیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا من قصایدی کنم انشا بمدح تو</p></div>
<div class="m2"><p>کاندر کمان فتد ز حسد بر سپهر تیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا از امیر و بنده بگیتی نشان بود</p></div>
<div class="m2"><p>تابنده چون امیر بندرت بود خطیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بادا کمینه بنده میمون جناب تو</p></div>
<div class="m2"><p>بر هر که در زمانه امارت کند امیر</p></div></div>