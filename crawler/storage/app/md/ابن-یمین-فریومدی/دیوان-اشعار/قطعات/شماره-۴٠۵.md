---
title: >-
    شمارهٔ ۴٠۵
---
# شمارهٔ ۴٠۵

<div class="b" id="bn1"><div class="m1"><p>باشد لئیم در نظر عقل چون شبه</p></div>
<div class="m2"><p>بیقیمت و کریم بود پر بها چو در</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون قدر هر یکی بر دانا مقررست</p></div>
<div class="m2"><p>بشنو نصیحتی ز من ای نامدار حر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با مردم کریم بپیوند و دوست باش</p></div>
<div class="m2"><p>وز مردم لئیم چو از دشمنان ببر</p></div></div>