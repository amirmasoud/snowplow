---
title: >-
    شمارهٔ ٨٣٣
---
# شمارهٔ ٨٣٣

<div class="b" id="bn1"><div class="m1"><p>خداوندا بحق آن کرامت</p></div>
<div class="m2"><p>که ما را در ازل کردی گرامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنزدیک ملایک نفس ما شد</p></div>
<div class="m2"><p>بتعلیم اسامی از تو سامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ما نادیده استحقاق احسان</p></div>
<div class="m2"><p>لقد اعطیتها فوق المرامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا کافتاد عقد صحت ذات</p></div>
<div class="m2"><p>ز دستان فلک در بی نظامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز لطف خود بدین معنی نگه کن</p></div>
<div class="m2"><p>و بدل حال سقمی بالسلامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اذا ابدات بالاحسان تمم</p></div>
<div class="m2"><p>فما الاحسان الا بالتمامی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنام نیک نیزم هم بمیران</p></div>
<div class="m2"><p>بود عمر مخلد نیکنامی</p></div></div>