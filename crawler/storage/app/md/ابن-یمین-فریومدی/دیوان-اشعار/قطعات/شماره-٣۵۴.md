---
title: >-
    شمارهٔ ٣۵۴
---
# شمارهٔ ٣۵۴

<div class="b" id="bn1"><div class="m1"><p>مرد باید بهر کجا باشد</p></div>
<div class="m2"><p>عزت خویشتن نگهدارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود پسندی و ابلهی نکند</p></div>
<div class="m2"><p>هر چه کبر و منی است بگذارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بطریقی رود که مردم را</p></div>
<div class="m2"><p>سر موئی ز خود نیازارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه کس را ز خویش به داند</p></div>
<div class="m2"><p>هیچکس را حقیر نشمارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر و زر در طلب نهد آنگه</p></div>
<div class="m2"><p>تا مگر دوستی بدست آرد</p></div></div>