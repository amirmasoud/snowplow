---
title: >-
    رباعی شمارهٔ ۲
---
# رباعی شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>تا ذات نهاده در صفاتیم همه</p></div>
<div class="m2"><p>عین خرد و سفرهٔ ذاتیم همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا در صفتیم در مماتیم همه</p></div>
<div class="m2"><p>چون رفت صفت عین حیاتیم همه</p></div></div>