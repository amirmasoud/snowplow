---
title: >-
    قصیدهٔ شمارهٔ ۲۱۷
---
# قصیدهٔ شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>مکر جهان را پدید نیست کرانه</p></div>
<div class="m2"><p>دام جهان را زمانه بینم دانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانه به دام اندرون مخور که شوی خوار</p></div>
<div class="m2"><p>چون سپری گشت دانه چون خر لانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طاعت پیش آرو علم جوی ازیراک</p></div>
<div class="m2"><p>طاعت و علم است بند و فند زمانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با تو روان است روزگار حذر کن</p></div>
<div class="m2"><p>تا نفریبد در این رهت بروانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سبزه جوانی است مر تو را چه شتابی</p></div>
<div class="m2"><p>از پس این سبزه همچو گاو جوانه؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیک نگه کن که در حصار جوانیت</p></div>
<div class="m2"><p>گرگ درنده است در گلوت و مثانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست رست نیست جز به خواب و خور ایراک</p></div>
<div class="m2"><p>شهر جوانی پر از زر است و رسانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیری اگر تو درون شوی ز در شهر</p></div>
<div class="m2"><p>سخت کند بر تو در به تنبه و فانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عالم دجال توست و تو به دروغش</p></div>
<div class="m2"><p>بسته‌ای و مانده‌ای و کشده یگانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قصهٔ دجال پر فریب شنودی</p></div>
<div class="m2"><p>گوش چه داری چو عامه سوی فسانه؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر به سخنهاش خلق فتنه شود پاک</p></div>
<div class="m2"><p>پس سخن اوست بانگ چنگ و چغانه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گوش تو زی بانگ اوست و خواندن او را</p></div>
<div class="m2"><p>بر سر کوی ایستاده‌ای به بهانه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس به گرانی روی گهی سوی مسجد</p></div>
<div class="m2"><p>سوی خرابات همچو تیر نشانه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دیو بخندد ز تو چو تو بنشینی</p></div>
<div class="m2"><p>روی به محراب و دل به سوی چمانه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از پس دیوی دوان چو کودک لیکن</p></div>
<div class="m2"><p>رود و می‌استت ز لیبیا و لکانه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مؤمنی و می خوری، به جز تو ندیدم</p></div>
<div class="m2"><p>در جسد مؤمنانه جان مغانه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قول و عمل چیست جز ترازوی دینی</p></div>
<div class="m2"><p>قول و عمل ورز و راست‌دار زبانه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>راه نمایدت سوی روضهٔ رضوان</p></div>
<div class="m2"><p>گر بروی بر رهی در این دو میانه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دام جهان است برتو و خبرت نیست</p></div>
<div class="m2"><p>گاهی مستی و گه خمار شبانه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پیش تو آن راست قدر کو شنواندت</p></div>
<div class="m2"><p>پیش ترنگ چغانه لحن ترانه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>راه خران است خواب و خوردن و رفتن</p></div>
<div class="m2"><p>خیره مرو با خرد به راه خرانه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از خور زی خواب شو زخواب سوی خور</p></div>
<div class="m2"><p>تات برون افگند زمان به کرانه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گنبد گردنده خانه‌ای است سپنجی</p></div>
<div class="m2"><p>مهر چه بندی بر این سپنجی خانه؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آمدنی اندر این سرای کسانند</p></div>
<div class="m2"><p>خیره برون شو تو زین سرای کسانه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرگ ستانه است در سرای سپنجی</p></div>
<div class="m2"><p>بگذری آخر تو زین بلند ستانه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دختر و مادرت از این ستانه برون شد</p></div>
<div class="m2"><p>رفت بد و نیک و شد فلان و فلانه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تنگ فراز آمده است حالت رفتنت</p></div>
<div class="m2"><p>سود نداردت کرد گربه به شانه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در ره غمری به یک مراغه چه جوئی</p></div>
<div class="m2"><p>ای خر دیوانه، در شتاب و دوانه؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اسپ جهان چون همی بخواهدت افگند</p></div>
<div class="m2"><p>علم تو را بس بود اسپ عقل دهانه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفتهٔ حجت به جمله گوهر علم است</p></div>
<div class="m2"><p>گوهر او را ز جانت ساز خزانه</p></div></div>