---
title: >-
    قصیدهٔ شمارهٔ ۱۰۲
---
# قصیدهٔ شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>یکی خانه کردند بس خوب و دلبر</p></div>
<div class="m2"><p>درو همچنو خانه بی‌حد و بی‌مر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خانهٔ مهین درنشاندند جفتان</p></div>
<div class="m2"><p>به یک جا دو خواهر زن و دو برادر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو زن خفته‌اند و دو مرد ایستاده</p></div>
<div class="m2"><p>نهفته زنان زیر شویان خود در</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه کمتر شوند این چهار و نه افزون</p></div>
<div class="m2"><p>نه هرگز بدانند به را ز بتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ولیکن کم و بیش و خوبی و زشتی</p></div>
<div class="m2"><p>به فرزندشان داد یزدان داور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سه فرزند دارند پیدا و پنهان</p></div>
<div class="m2"><p>ازیشان دو پیدا و یکی مستر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیاید برون آن مستر به صحرا</p></div>
<div class="m2"><p>نشسته نهفته است بر سان دختر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وز این هر یکی هفت فرزند دیگر</p></div>
<div class="m2"><p>بزاده‌است نه هیچ بیش و نه کمتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز هر هفتی از جملهٔ این سه هفتان</p></div>
<div class="m2"><p>یکی مهتر آمد بر آن شش که کهتر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وزین بیست و یک تن یکی پادشا شد</p></div>
<div class="m2"><p>دگر جمله گشتند او را مسخر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همی گوید آن پادشا هر چه خواهد</p></div>
<div class="m2"><p>همه دیگران مانده خاموش و مضطر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به خانهٔ مهین در همیشه است پران</p></div>
<div class="m2"><p>پس یکدگر دو مخالف کبوتر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگیرند جفت و نسازند یک جا</p></div>
<div class="m2"><p>نباشند هرگز جدا یک ز دیگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به خانهٔ کهین در نیایند هرگز</p></div>
<div class="m2"><p>که خانهٔ مهین استشان جا و در خور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسا خانه‌ها کان به پرواز ایشان</p></div>
<div class="m2"><p>شد آباد و بس نیز شد زیر و از بر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کبوتر که دیده‌است کز گردش او</p></div>
<div class="m2"><p>جهان را گهی خیر زاید گهی شر؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به خانهٔ کهین در همیشه سه مهمان</p></div>
<div class="m2"><p>از این دو کبوتر خورد نعمت و بر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیابد هگرز آن سه مهمان چهارم</p></div>
<div class="m2"><p>نه این دو کبوتر بیابد سدیگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سه مهمان نه یکسان و هر سه مخالف</p></div>
<div class="m2"><p>وگرچه پدرشان یکی بود و مادر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ازیشان یکی کینه‌دار است و بدخو</p></div>
<div class="m2"><p>دگر شاد و جویای خواب است و یا خور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سوم‌شان به و مه که هرگز نجوید</p></div>
<div class="m2"><p>مگر خیر بی‌شر و یا نفع بی‌ضر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سه مهمان به یک خانه در باز کرده</p></div>
<div class="m2"><p>بر اندازهٔ خویش هر یک یکی در</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همی هر یکی گوید آن دیگران را</p></div>
<div class="m2"><p>که «زین در درآئید کاین راه بهتر»</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر زین سه آنک او شریف و والا</p></div>
<div class="m2"><p>مر آن دیگران را سرآرد به چنبر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خداوند آن خانه آزاد گردد</p></div>
<div class="m2"><p>هم امروز اینجا و هم روز محشر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وگر این یکی را فریبند آن دو</p></div>
<div class="m2"><p>خداوند خانه بماند در آذر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بد و نیک چون نیست امروز یکسان</p></div>
<div class="m2"><p>چنان دان که فردا نباشند هم سر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شناسی تو خانهٔ مهین و کهین را</p></div>
<div class="m2"><p>بخانهٔ تو هست این سه تن نیک بنگر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کبوتر تو را بر سر است ایستاده</p></div>
<div class="m2"><p>که از زیر پرش نیاری برون سر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نگر کان چه تخم است کامروز کاری</p></div>
<div class="m2"><p>همان بایدت خورد فردا ازو بر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>درختی شگفت است مردم که بارش</p></div>
<div class="m2"><p>گهی نیش وزهر است وگه نوش و شکر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یکی برگ او مبرم و شاخ بسد</p></div>
<div class="m2"><p>یکی برگ او گزدم و شاخ نشتر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خوی نیک مبرم خوی بد چو گزدم</p></div>
<div class="m2"><p>بدی و بهی نیش و نوش است هم بر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو گزدم بینداز و بردار مبرم</p></div>
<div class="m2"><p>تو بردار آن نوش و از نیش بگذر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دو مرد است مردم توانا و دانا</p></div>
<div class="m2"><p>جز این هر که بینی به مردمش مشمر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تواناست بر دانش خویش دانا</p></div>
<div class="m2"><p>نه داناست آنک او تواناست بر زر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هزاران توان یافت خنجر به دانش</p></div>
<div class="m2"><p>یکی علم نتوان گرفتن به خنجر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>توانا دو گونه است هر چند بینی</p></div>
<div class="m2"><p>یکی زو جوان است و دیگر توانگر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جوان را جوانی فلک باز خواهد</p></div>
<div class="m2"><p>ستاند توان از توانگر ستمگر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به چیزی دگر نیست داننده دانا</p></div>
<div class="m2"><p>ستمگار زی او یکی‌اند و داور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کسی چون ستاند ز یاقوت قوت؟</p></div>
<div class="m2"><p>چگونه رباید کسی بو ز عنبر؟</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به دانش گرای، ای برادر، که دانش</p></div>
<div class="m2"><p>تو را بر گذارد از این چرخ اخضر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به دانش توانی رسید، ای برادر،</p></div>
<div class="m2"><p>از این گوی اغبر به خورشید ازهر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جهان خار خشک است و دانش چو خرما</p></div>
<div class="m2"><p>تو از خار بگریز وز بار می‌خور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جهان آینه است و درو هر چه بینی</p></div>
<div class="m2"><p>خیال است و ناپایدار و مزور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جوانیش پیری شمر، مرده زنده</p></div>
<div class="m2"><p>شرابش سراب و منور مغبر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>جهان بحر ژرف است و آبش زمانه</p></div>
<div class="m2"><p>تو را کالبد چون صدف جانت گوهر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اگر قیمتی در خواهی که باشی</p></div>
<div class="m2"><p>به آموختن گوهر جان بپرور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بیندیش تا: چیست مردم که او را</p></div>
<div class="m2"><p>سوی خویش خواند ایزد دادگستر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چه خواهد همی زو که چونین دمادم</p></div>
<div class="m2"><p>پیمبر فرستد همی بر پیمبر؟</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بر اندیش کاین جنبش بی‌کرانه</p></div>
<div class="m2"><p>چرا اوفتاد اندر این جسم اکبر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>که جنباند این را به همواری ایدون؟</p></div>
<div class="m2"><p>چه خواهد که آرد به حاصل از ایدر؟</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر از نور ظلمت نیاید چرا پس</p></div>
<div class="m2"><p>تو پیدائی و کردگار تو مضمر؟</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>وگر نیست مر قدرتش را نهایت</p></div>
<div class="m2"><p>چرا پس که هست آفریده مقدر؟</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ور از راست کژی نشاید که آید</p></div>
<div class="m2"><p>چرا هست کردهٔ مصور مصور؟</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ور آباد خواهد که دارد جهان را</p></div>
<div class="m2"><p>چرا بیشتر زو خراب است و بی‌بر؟</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بیابان بی‌آب و کوه شکسته</p></div>
<div class="m2"><p>دو صدبار بیش است از شهر و کردر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بدین پرده اندر نیابد کسی ره</p></div>
<div class="m2"><p>جز آن کس که ره را بجوید ز رهبر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ره سر یزدان که داند؟ پیمبر</p></div>
<div class="m2"><p>پیمبر سپرده است این سر به حیدر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>اگر تو مقری ز من خواه پاسخ</p></div>
<div class="m2"><p>وگر منکری پس تو پاسخ بیاور</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز خانهٔ کهین و مهین و از آن دور</p></div>
<div class="m2"><p>کبوتر جوابم بیاور مفسر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بگو آن دو خواهر زن و دو برادر</p></div>
<div class="m2"><p>کدامند و فرزندشان ماده و نر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بیان کن که از چیست تقصیر عالم</p></div>
<div class="m2"><p>جوابم ده از خشک این شعر وز تر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ندانی به حق خدای و نداند</p></div>
<div class="m2"><p>کس این جز که فرزند شبیر و شبر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>جهان را بنا کرد از بهر دانش</p></div>
<div class="m2"><p>خدای جهاندار بی‌یار و یاور</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>تو گوئی که چون و چرا را نجویم</p></div>
<div class="m2"><p>سوی من همین است بس مذهب خر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>تو را بهره از علم خار است یا که</p></div>
<div class="m2"><p>مرا بهره مغز است و دانهٔ مقشر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>سوی گاو یکسان بود کاه و دانه</p></div>
<div class="m2"><p>به کام خر اندر چه میده چه جو در</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>منم بستهٔ بند آن کو ز مردم</p></div>
<div class="m2"><p>چنان است سنگ یاقوت احمر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو مدحت به آل پیمبر رسانم</p></div>
<div class="m2"><p>رسد ناصبی را ازو جان به غرغر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>جزیرهٔ خراسان چو بگرفت شیطان</p></div>
<div class="m2"><p>درو خار بنشاند و بر کند عرعر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>مرا داد دهقانی این جزیره</p></div>
<div class="m2"><p>به رحمت خداوند هر هفت کشور</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>خداوند عصر آنکه چون من مرو را</p></div>
<div class="m2"><p>ده و دو ستاره است هریک سخن‌ور</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چو مردم زحیوان بهست و مهست او</p></div>
<div class="m2"><p>ز مردم بهین و مهین است یکسر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به نورش خورد مؤمن از فعل خود بر</p></div>
<div class="m2"><p>به نازش برد کافر از کرده کیفر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو بر منبر جد خود خطبه خواند</p></div>
<div class="m2"><p>باستدش روح الامین پیش منبر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو آن شیر پیکر علامت ببندد</p></div>
<div class="m2"><p>کند سجده بر آسمانش دو پیکر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>نه جز امر او را فلک هست بنده</p></div>
<div class="m2"><p>نه جز تیغ او راست مریخ چاکر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به لشکر بنازند شاهان و دایم</p></div>
<div class="m2"><p>ز شاهان عصر است بر درش لشکر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>درش دشت محشر تنش کان گوهر</p></div>
<div class="m2"><p>دلش بحر اخضر کفش نهر کوثر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>اگر سوی قیصر بری نعل اسپش</p></div>
<div class="m2"><p>ز فخرش بیاویزد از گوش قیصر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>همی تا جهان است وین چرخ اخضر</p></div>
<div class="m2"><p>بگردد همی گرد این گوی اغبر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>هزاران درود و دو چندان تحیت</p></div>
<div class="m2"><p>از ایزد بر آن صورت روح پیکر</p></div></div>