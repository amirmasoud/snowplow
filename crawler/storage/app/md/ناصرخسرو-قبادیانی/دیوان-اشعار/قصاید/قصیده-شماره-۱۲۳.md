---
title: >-
    قصیدهٔ شمارهٔ ۱۲۳
---
# قصیدهٔ شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>چه بود این چرخ گردان را که دیگر گشت سامانش؟</p></div>
<div class="m2"><p>به بستان جامهٔ زربفت بدریدند خوبانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منقش جامه‌هاشان را که‌شان پوشید فروردین</p></div>
<div class="m2"><p>فرو شست از نگار و نقش ماه مهر و آبانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همانا با خزان گل را به بستان عهد و پیمان بود</p></div>
<div class="m2"><p>که پنهان شد چو بدگوهر خزان بشکست پیمانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سر بنهاد شاخ گل به باغ آن تاج پر درش</p></div>
<div class="m2"><p>به رخ بر بست خورشید آن نقاب خز خلقانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همان که سر که پوشیدش به دیبا باد نوروزی</p></div>
<div class="m2"><p>خزانی باد پنهان کرد در محلوج کوهانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی گردنده گوئی بر شد از دریا سوی گردون</p></div>
<div class="m2"><p>که جز کافور و مروارید و گوهر نیست در کانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهنگی را همی ماند که گردون را بیوبارد</p></div>
<div class="m2"><p>چو از دریا برآمد جوش از بحر هر عصیانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نباشد جز که یک میدان نشیب و کوه و هامونش</p></div>
<div class="m2"><p>نیاید بیش یک لقمه خراب و خاک و عمرانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نپوشد جز بدو عالم ز خز و تو ز پیراهن</p></div>
<div class="m2"><p>نگردد جز که از خورشید فرسوده گریبانش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بغرد همچو اژدرها چو بر عالم بیاشوبد</p></div>
<div class="m2"><p>ببارد آتش و دود از میان کام و دندانش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خزینهٔ آب و آتش گشت بر گردون که پنداری</p></div>
<div class="m2"><p>زخشم خویش و از رحمت مرکب کرد یزدانش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بمیرد چون بگرید سیر تا هشیار پندارد</p></div>
<div class="m2"><p>که چیزی جز که گریه نیست ترکیب تن و جانش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مگر تخت سلیمان است کز دریا سحرگاهان</p></div>
<div class="m2"><p>نباشد زی که و هامون مگر بر باد جولانش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنین تیره چرائی، ای مبارک تخت رخشنده؟</p></div>
<div class="m2"><p>همانا کز سلیمانت بدزدیدند دیوانش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو مرغان را همی سایه کنی امروز، اگر روزی</p></div>
<div class="m2"><p>تو را سایه همی کردند و، او را نیز، مرغانش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فلک را پرده و که را کلاه و خاک را خیمه</p></div>
<div class="m2"><p>میانجی کرد یزدانت میان چرخ و ارکانش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو دایهٔ مهربانی جمله فرزندان عالم را</p></div>
<div class="m2"><p>همی گردی کجا هستند در آباد و ویرانش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به فعل خوب تو خوب است روی زشت تو زی آن</p></div>
<div class="m2"><p>که او مر آفرینش را بداند راه و سامانش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه اندر صورت خوب است زیب مرد و نیکوئی</p></div>
<div class="m2"><p>ولیکن در خوی خوب است خوبی‌ی مرد و در دانش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سخن عنوان نامهٔ مردم آمد، هر که را خواهی</p></div>
<div class="m2"><p>که برخوانی به چشم گوش بنگر سوی عنوانش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دو صورت هست مردم را به هر دو بنگر و بررس</p></div>
<div class="m2"><p>به چشم از روی پیدائش به گوش از جان پنهانش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نپرسد مرد را کس که «ت چرا رخ نیست چون دیبا؟»</p></div>
<div class="m2"><p>ولیکن «چونکه نادانی؟» بسی گویند مردانش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نکوهش مرگ را ماند، ستایش زندگانی را،</p></div>
<div class="m2"><p>چو نادانی بود علت مدان جز علم درمانش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بمیرد صورت جسمی، سخن ماند ز ما زنده،</p></div>
<div class="m2"><p>سخن دان را بر این دعوی چو خورشید است برهانش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همی طاووس را بکشی ز بهر پر رنگینش</p></div>
<div class="m2"><p>بداری زنده بلبل را ز بهر خوب الحانش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به حکمت کوش تا باشد که باشی بلبل یزدان</p></div>
<div class="m2"><p>بمانی جاودان اندر بهشت خلد رضوانش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نبینی چند احسان کرد بی طاعت بجای تو؟</p></div>
<div class="m2"><p>اگر طاعت کنی بی شک مضاعف گردد احسانش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نبینی، گر خردمندی، که تو کرسی یزدانی؟</p></div>
<div class="m2"><p>نبینی کز جهان جز بر تو ننبشته است فرمانش؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زمین خوان خدای است، ای برادر، پر ز نعمت‌ها</p></div>
<div class="m2"><p>که جز مردم نیابد بر همی از نعمت و خوانش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نیابد آن خوشی حیوان که مردم یابد از دنیا</p></div>
<div class="m2"><p>و گرچه زو فزون از ما تواند خورد حیوانش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ندارد شادمانش روی خوب و خز و سقلاطون</p></div>
<div class="m2"><p>نبخشد بوی خوش هرگز عبیر و عنبر و بانش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بیابان است اگر باع است یکسان است سوی او</p></div>
<div class="m2"><p>نه شاد و خوش کند اینش نه مستوحش کند آنش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پدید آمد، پس ای دانا، که عالم خوان یزدان است</p></div>
<div class="m2"><p>و حیوان چونکه طفلانند و جز تو نیست مهمانش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مر این را چاشنی پندار و شکرش کن زیادت را</p></div>
<div class="m2"><p>و گر کفرانش پیش آری بترس از بند و تاوانش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به چشم دل نکو بنگر ببین این خوان پر نعمت</p></div>
<div class="m2"><p>که بنهاده است پیش تو در این زنگاری ایوانش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر دانی که مهمانی چرا پس پست ننشستی؟</p></div>
<div class="m2"><p>بباید بهر تو یکسر زخوان ساران و پایانش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که جز تو نیز خواهد بود مهمانان مر ایزد را</p></div>
<div class="m2"><p>که می‌خواند در این خوان‌شان ازو افلاک و دورانش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو را افلاک و دوران خواند در میدان یزدانی</p></div>
<div class="m2"><p>برونت رفت باید تا نگردد تنگ میدانش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همی خواهندت از میدان برون راندن به دشواری</p></div>
<div class="m2"><p>که با هر خوانده‌ای این است رسم و سیرت و سانش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زمان چوگان گردون است و میدان خاک و تو بر وی</p></div>
<div class="m2"><p>مگر گوئی یکی گردنده گوئی پیش چوگانش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یکی زندان تنگ است این که باغش ظن برد نادان</p></div>
<div class="m2"><p>سوار است آنکه پندارد که بستان است زندانش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>حذر کن زین ره افگن یار و بد خو دشمن خندان</p></div>
<div class="m2"><p>که تا خلقت نگیرد ناگهان نشناسی آسانش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر با میر صحبت کرد میرانید میرش را</p></div>
<div class="m2"><p>و گر با خان برادر شد خیانت دید ازو خانش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نیاساید ز بیدادی که مرکب تیز رو دارد</p></div>
<div class="m2"><p>فرو سایدت اگر سنگی که بس تیز است سوهانش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بکش نفس ستوری را به دشنهٔ حکمت و طاعت</p></div>
<div class="m2"><p>بکش زین دیو دستت را که بسیار است دستانش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یکی غول فریبنده است نفس آرزو خواهت</p></div>
<div class="m2"><p>که بی‌باکی چرا خورش است و نادانی بیابانش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به ره باز آید این گم راه دیوت گر بخواهی تو</p></div>
<div class="m2"><p>مسلمانی بیابد گر خرد باشد سلیمانش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>که را عقل از فضایل خلعتی دینی بپوشاند</p></div>
<div class="m2"><p>نداند کرد از آن خلعت هگرز این دیو عریانش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مرا در پیرهن دیوی منافق بود و گردن کش</p></div>
<div class="m2"><p>ولیکن عقل یاری داد تا کردم مسلمانش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مرا در دین نپندارد کسی حیران و گم بوده</p></div>
<div class="m2"><p>جز آن حیران که حیرانی دگر کرده‌است حیرانش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مرا گویند بد دین است و فاضل، بهتر آن بودی</p></div>
<div class="m2"><p>که دینش پاک بودی و نبودی فضل چندانش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نبیند چشم ناقص طلعت پر نور فاضل را</p></div>
<div class="m2"><p>که چشمش را بخست از دیدن او خار نقصانش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که چون خفاش نتواند که بیند روی من نادان</p></div>
<div class="m2"><p>زمن پنهان شود زیرا منم خورشید رخشانش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مغیلان است جاهل پیشم و، من پیش او ریحان</p></div>
<div class="m2"><p>ندارد پیش ریحانم خطر ناخوش مغیلانش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همی گوید «بپرسیدش پس از ایمان به فرقان او</p></div>
<div class="m2"><p>به پیغمبر رسول مصطفی از فضل یارانش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>اگر کمتر ندارد مر علی را از همه یاران</p></div>
<div class="m2"><p>نباشد جز که باطل زی خدای اسلام و ایمانش»</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>اگر منکر شوم دعویش را بر کفر و جهل من</p></div>
<div class="m2"><p>گواهی یکسره بدهند جهال خراسانش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چرا گوید خردمند آنچه ندهد بر صواب آن</p></div>
<div class="m2"><p>گوائی عقل بی آفت نه نیز آیات فرقانش؟</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چرا گویم که بهتر بود در عالم کسی زان کس</p></div>
<div class="m2"><p>که بر اعدای دین بر تیغ محنت بود بارانش؟</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از آن سید که از فرمان رب‌العرش پیغمبر</p></div>
<div class="m2"><p>وصی کردش در آن معدن که منبر بود پالانش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>از آن مشهور شیر نر که اندر بدر و در خیبر</p></div>
<div class="m2"><p>هوا از چشم خون بارید بر صمصام خندانش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>شدی حیران و بی سامان و کردی نرم گردن را</p></div>
<div class="m2"><p>اگر دیدی به صف دشمنان سام نریمانش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>کسی کو دیگری را برگزیند بر چنین حری</p></div>
<div class="m2"><p>بپرسد روز حشر ایزد ز تن بی روی بهتانش</p></div></div>