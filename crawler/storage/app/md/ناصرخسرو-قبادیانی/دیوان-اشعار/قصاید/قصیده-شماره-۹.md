---
title: >-
    قصیدهٔ شمارهٔ ۹
---
# قصیدهٔ شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>حکیمان را چه می‌گویند چرخ پیر و دوران‌ها</p></div>
<div class="m2"><p>به سیر اندر ز حکمت بر زبان مهر و آبان‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خزان گوید به سرماها همین دستان دی و بهمن</p></div>
<div class="m2"><p>که گویدشان همی بی‌شک به گرماها حزیران‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به قول چرخ گردان بر زبان باد نوروزی</p></div>
<div class="m2"><p>حریر سبز در پوشند بستان و بیابان‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درخت بارور فرزند زاید بی‌شمار و مر</p></div>
<div class="m2"><p>در آویزند فرزندان بسیارش ز پستان‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فراز آیند از هر سو بسی مرغان گوناگون</p></div>
<div class="m2"><p>پدید آرند هر فوجی به لونی دیگر الحان‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سان پر ستاره آسمان گردد سحرگاهان</p></div>
<div class="m2"><p>ز سبزهٔ آب‌دار و سرخ گل وز لاله بستان‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به گفتار که بیرون آورد چندان خز و دیبا</p></div>
<div class="m2"><p>درخت مفلس و صحرای بیچاره ز پنهان‌ها؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نداند باغ ویران جز زبان باد نوروزی</p></div>
<div class="m2"><p>به قول او کند ایدون همی آباد ویران‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو از برج حمل خورشید اشارت کرد زی صحرا</p></div>
<div class="m2"><p>به فرمانش به صحرا بر مطرا گشت خلقان‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگون‌سار ایستاده مر درختان را یکی بینی</p></div>
<div class="m2"><p>دهان‌هاشان روان در خاک بر کردار ثعبان‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درختان را بهاران کار بندانند و تابستان</p></div>
<div class="m2"><p>ولیکن‌شان نفرماید جز آسایش زمستان‌ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به قول ماه دی آبی که یازان باشد و لاغر</p></div>
<div class="m2"><p>بیاساید شب و روز و بر آماسد چو سندان‌ها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که گوید گور و آهو را که جفت آنگاه بایدتان</p></div>
<div class="m2"><p>همی جستن که زادن‌تان نباشد جز به نیسان‌ها؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در آویزد همی هر یک بدین گفتارها زینها</p></div>
<div class="m2"><p>صلاح خویش را گوئی به چنگ خویش و دندان‌ها</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چرا واقف شدند اینها بر این اسرار و، ای غافل،</p></div>
<div class="m2"><p>نگشته ستی تو واقف بر چنین پوشیده فرمان‌ها؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدین دهر فریبنده چرا غره شدی خیره؟</p></div>
<div class="m2"><p>ندانستی که بسیار است او را مکر و دستان‌ها؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نجوید جز که شیرین جان فرزندانش این جانی</p></div>
<div class="m2"><p>ندارد سود با تیغش نه جوشن‌ها نه خفتان‌ها</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی گوید به فعل خویش هر کس را ز ما دایم</p></div>
<div class="m2"><p>که «من همچون تو، ای بیهوش، دیده ستم فراوان‌ها</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر با تو نمی‌دانی چه خواهم کرد، نندیشی</p></div>
<div class="m2"><p>که امسال آن کنم با تو که کردم پار با آنها؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همی بینی که روز و شب همی گردی به ناکامت</p></div>
<div class="m2"><p>به پیش حادثات من چو گوئی پیش چوگان‌ها</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز میدان‌های عمر خویش بگذشتی و می‌دانی</p></div>
<div class="m2"><p>که هرگز باز نائی تو سوی این شهره میدان‌ها</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که آراید، چه گوئی، هر شبی این سبز گنبد را</p></div>
<div class="m2"><p>بدین نو رسته نرگس‌ها و زراندود پیکان‌ها؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر بیدار و هشیاری و گوشت سوی من داری</p></div>
<div class="m2"><p>بیاموزم تو را یک یک زبان چرخ و دوران‌ها</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همی گویند کاین کهسارهای محکم و عالی</p></div>
<div class="m2"><p>نرسته ستند در عالم مگر کز نرم باران‌ها</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زمین کو مایهٔ‌تنهاست دانا را همی گوید</p></div>
<div class="m2"><p>که اصلی هست جان‌ها را که سوی او شود جان‌ها</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به تاریکی دهد مژده همیشه روشنائی مان</p></div>
<div class="m2"><p>که از دشوارها هرگز نباشد خالی آسان‌ها</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به مال و قوت دنیا مشو غره چو دانستی</p></div>
<div class="m2"><p>که روزی آهوان بودند آن پرآرد انبان‌ها</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وگر دشواریی بینی مشو نومید از آسانی</p></div>
<div class="m2"><p>که از سرگین همی روید چنین خوش بوی ریحان‌ها</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چهارت بند بینم کرده اندر هفتمین زندان</p></div>
<div class="m2"><p>چرا ترسی اگر از بند بجهانند و زندان‌ها؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در این صندوق ساعت عمرها را دهر بی‌رحمت</p></div>
<div class="m2"><p>همی برما بپیماید بدین گردنده پنگان‌ها</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز عمر این جهانی هر که حق خویش بستاند</p></div>
<div class="m2"><p>برون باید شدنش از زیر این پیروزه ایوان‌ها</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو زین منزلگه کم بیشها بیرون شود زان پس</p></div>
<div class="m2"><p>نیابد راه سوی او زیادت‌ها و نقصان‌ها</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در این الفنج گه جویند زاد خویش بیداران</p></div>
<div class="m2"><p>که هم زادست بر خوان‌ها و هم مال است در کان‌ها</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بماند تشنه و درویش و بیمار آنکه نلفنجد</p></div>
<div class="m2"><p>در این ایام الفغدن شراب و مال و درمان‌ها</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که را ناید گران امروز رفتن بر ره طاعت</p></div>
<div class="m2"><p>گران آید مر آن کس را به روز حشر میزان‌ها</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به نعمت‌ها رسند آنها که ورزیدند نیکی‌ها</p></div>
<div class="m2"><p>به شدتها رسند آنها که بشکستند پیمان‌ها</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خداوند جهان باتش بسوزد بد فعالان را</p></div>
<div class="m2"><p>برین قایم شده‌است اندر جهان بسیار برهان‌ها</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ازیرا ما خداوند درختانیم و سوی ما</p></div>
<div class="m2"><p>سزای سوختن گشتند بد گوهر مغیلان‌ها</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدی با جهل یارانند، هر کو بد کنش باشد</p></div>
<div class="m2"><p>نپرهیزد زبد گرچه مقر آید به فرقان‌ها</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نبینی حرص این جهال بر کردار بد زان پس</p></div>
<div class="m2"><p>که پیوسته همی درند بر منبر گریبان‌ها</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به زیر قول چون مبرم نگر فعل چو نشترشان</p></div>
<div class="m2"><p>به سان نامه‌های زشت زیر خوب عنوان‌ها</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز بهتان گویدت پرهیز کن وانگه به طمع خود</p></div>
<div class="m2"><p>بگوید صد هزاران بر خدای خویش بهتان‌ها</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر یک دم به خوان خوانی مرورا، مژده‌ور گردد</p></div>
<div class="m2"><p>به خوانی در بهشت عدن پر حلوا و بریان‌ها</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به باغی در که مرغان از درختانش به پیش تو</p></div>
<div class="m2"><p>فرود افتد چو بریان شکم آگنده بر خوان‌ها</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چنین باغی نشاید جز که مر خوارزمیانی را</p></div>
<div class="m2"><p>که بردارند بر پشت و به گردن بار کپان‌ها</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چنین چو گفتی ای حجت که بر جهال این امت</p></div>
<div class="m2"><p>فرو بارد ز خشم تو همی اندوه طوفان‌ها؟</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بر این دیوان اگر نفرین کنی شاید که ایشان را</p></div>
<div class="m2"><p>همی هر روز پرگردد به نفرین تو دیوان‌ها</p></div></div>