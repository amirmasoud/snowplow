---
title: >-
    قصیدهٔ شمارهٔ ۲۶۰
---
# قصیدهٔ شمارهٔ ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>ای گرد گرد گنبد طارونی</p></div>
<div class="m2"><p>یکبارگی بدین عجبی چونی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردان منم به حال و نه گردونم</p></div>
<div class="m2"><p>گردان نه‌ای به حال و تو گردونی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر راه نیست سوی تو پیری را</p></div>
<div class="m2"><p>مر پیری مرا ز چه قانونی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیرا که روزگار دهد پیری</p></div>
<div class="m2"><p>وز زیر روزگار تو بیرونی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اکنونیان روان و تو برجائی</p></div>
<div class="m2"><p>زیرا که نیست جسم تو اکنونی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درویش توست خلق به عمر ایراک</p></div>
<div class="m2"><p>از عمر بی‌کناره تو قارونی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درویش دون بود، همه دونانند</p></div>
<div class="m2"><p>اینها و، بر نهاده به تو دونی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کس که دون شمارد قارون را</p></div>
<div class="m2"><p>از ناکسیش باشد و مجنونی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرزند توست خلق و مر ایشان را</p></div>
<div class="m2"><p>تو مادر مبارک و میمونی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر راه خلق سوی دگر عالم</p></div>
<div class="m2"><p>یکی رباط یا یکی آهونی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای پیر، بر گذشته جوانی چون</p></div>
<div class="m2"><p>دیوانه‌وار غمگن و محزونی؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیوی است کودکی، تو به دیوی بر،</p></div>
<div class="m2"><p>گر دیو نیستی، ز چه مفتونی؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پنجاه و اند سال شدی، اکنون</p></div>
<div class="m2"><p>بیرون فگن ز سرت سرا کونی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گوئی که روزگار دگرگون شد</p></div>
<div class="m2"><p>ای پیر ساده‌دل، تو دگرگونی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سروی بدی به قد و به رخ لاله</p></div>
<div class="m2"><p>اکنون به رخ زریر و به قد نونی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گلگون رخت چو شست بهار ازور</p></div>
<div class="m2"><p>بگذشت گل بگشت ز گلگونی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مال تو عمر بود بخوردی پاک</p></div>
<div class="m2"><p>آن را به بی‌فساری و ملعونی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اکنون ز مفلسی چه نوی چندین</p></div>
<div class="m2"><p>بر درد مالی و غم مغبونی؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن کس که دی همیت فریغون خواند</p></div>
<div class="m2"><p>اکنون به سوی او نه فریغونی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وان را که نوش و شهد و شکر بودی</p></div>
<div class="m2"><p>امروز زهر و حنظل و طاعونی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با تو فلک به جنگ و شبیخون است</p></div>
<div class="m2"><p>پس تو چه مرد جنگ و شبیخونی؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هرشب زخونت چون بخورد لختی</p></div>
<div class="m2"><p>چیزی نمانی ار همه جیحونی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر خون تو نخورد به شب گردون</p></div>
<div class="m2"><p>پس کوت آن رخان طبرخونی؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مشغول تن مباش کزو حاصل</p></div>
<div class="m2"><p>نایدت چیز جز همه وارونی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از حلق چون گذشت شود یکسان</p></div>
<div class="m2"><p>با نان خشک قلیهٔ هارونی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جان را به علم و طاعت صابون زن</p></div>
<div class="m2"><p>جامه است مر تو را همه صابونی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خاک است مشک و عنبر و تو خاکی</p></div>
<div class="m2"><p>گرچه ز مشک و عنبر معجونی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ملکت نماند و گنج برافریدون</p></div>
<div class="m2"><p>ایمن مباش اگر تو فریدونی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>افزونیی که خاک شود فردا</p></div>
<div class="m2"><p>آن بی‌گمان کمی است نه افزونی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کار خر است خواب و خور ای نادان</p></div>
<div class="m2"><p>پس خر توی اگر تو همیدونی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مردم ز علم و فضل شرف یابد</p></div>
<div class="m2"><p>نز سیم و زر و از خز طارونی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از علم یافت نامور افلاطون</p></div>
<div class="m2"><p>تا روز حشر نام فلاطونی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>با جاهلان از آرزوی دانش</p></div>
<div class="m2"><p>با قال و قیل و حیلت و افسونی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از جهل خویشتن چو خود آگاهی</p></div>
<div class="m2"><p>پس سوی خویشتن فتنه و شمعونی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دانا به یک سؤال برون آرد</p></div>
<div class="m2"><p>جهل نهفته از تو به هامونی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو سوی خاص خلق سیه‌سنگی</p></div>
<div class="m2"><p>گر سوی عام لولوی مکنونی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>علم است کیمیای بزرگی‌ها</p></div>
<div class="m2"><p>شکر کندت اگر همه هپیونی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شاگرد اهل علم شوی به زان</p></div>
<div class="m2"><p>کاکنون رهی و چاکر خاتونی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مردم شوی به علم چو ماذون کو</p></div>
<div class="m2"><p>داعی شود به علم ز ماذونی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ذوالنونی از قیاس تو ای حجت</p></div>
<div class="m2"><p>دریاست علم دین و تو ذوالنونی</p></div></div>