---
title: >-
    قصیدهٔ شمارهٔ ۲۵
---
# قصیدهٔ شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>بر تو این خوردن و این رفتن و این خفتن و خاست</p></div>
<div class="m2"><p>نیک بنگر که، که افگند، وز این کار چه خواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به ناکام تو بود این همه تقدیر، چرا</p></div>
<div class="m2"><p>به همه عمر چنین خواب و خورت کام و هواست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شدی فتنهٔ ناخواستهٔ خویش؟ بگوی،</p></div>
<div class="m2"><p>راست می‌گوی، که هشیار نگوید جز راست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور تو خود کردی تقدیر چنین بر تن خویش</p></div>
<div class="m2"><p>صانع خویش تو پس خود و، این قول خطاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راست آن است که این بند خدای است تورا</p></div>
<div class="m2"><p>اندر این خانه و، این خانه تو را جای چراست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به چرا فتنه شدن کار ستور است، تورا</p></div>
<div class="m2"><p>این همه مهر بر این جای چرا، چون و چراست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه اندوه تو و بیم تو از کاستن است،</p></div>
<div class="m2"><p>ای فزوده ز چرا، چاره نیابی تو ز کاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیر گردنده فلک چون طلبی خیره بقا؟</p></div>
<div class="m2"><p>که به نزد حکما، گشتن از آیات فناست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشتن حال تو و گشتن چرخ و شب و روز</p></div>
<div class="m2"><p>بر درستی، که جهان جای بقا نیست گواست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منزل توست جهان ای سفری جان عزیز</p></div>
<div class="m2"><p>سفرت سوی سرائی است که آن جای بقاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مخور انده چو از این جای همی برگذری</p></div>
<div class="m2"><p>گرچه ویران بود این منزل، دینت به نواست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پست منشین که تو را روزی از این قافله گاه،</p></div>
<div class="m2"><p>گرچه دیر است، همان آخر بر باید خاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>توشه از طاعت یزدانت همی باید کرد</p></div>
<div class="m2"><p>که در این صعب سفر طاعت او توشهٔ ماست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیکی الفنج و ز پرهیز و خرد پوش سلاح</p></div>
<div class="m2"><p>که بر این راه یکی منکر و صعب اژدرهاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهترین راه گزین کن که دو ره پیش تو است</p></div>
<div class="m2"><p>یک رهت سوی نعیم است و دگر سوی بلاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از پس آنکه رسول آمده با وعد و وعید</p></div>
<div class="m2"><p>چند گوئی که بدو نیک به تقدیر و قضاست؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گنه و کاهلی خود به قضا بر چه نهی؟</p></div>
<div class="m2"><p>که چنین گفتن بی‌معنی کار سفهاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر خداوند قضا کرد گنه بر سر تو</p></div>
<div class="m2"><p>پس گناه تو به قول تو خداوند توراست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بد کنش زی تو خدای است بدین مذهب زشت</p></div>
<div class="m2"><p>گرچه می‌گفت نیاری، کت ازین بین قفاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اعتقاد تو چنین است، ولیکن به زبان</p></div>
<div class="m2"><p>گوئی او حاکم عدل است و حکیم الحکماست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با خداوند زبانت به خلاف دل توست</p></div>
<div class="m2"><p>با خداوند جهان نیز تو را روی و ریاست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به میان قدر و جبر رود اهل خرد،</p></div>
<div class="m2"><p>راه دانا به میانهٔ دو ره خوف و رجاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به میان قدر و جبر ره راست بجوی</p></div>
<div class="m2"><p>که سوی اهل خرد جبر و قدر درد و عناست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>راست آن است ره دین که پسند خرد است</p></div>
<div class="m2"><p>که خرد اهل زمین را ز خداوند عطاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عدل بنیاد جهان است، بیندیش که عدل</p></div>
<div class="m2"><p>جز به حکم خرد از جور به حکم که جداست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خرد است آنکه چو مردم سپس او برود</p></div>
<div class="m2"><p>گر گهر روید در زیر پیش خاک سزاست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خرد آن است که مردم ز بها و شرفش</p></div>
<div class="m2"><p>از خداوند جهان اهل خطاب است و ثناست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خرد از هر خللی پشت و ز هر غم فرج است</p></div>
<div class="m2"><p>خرد از بیم امان است و ز هر درد شفاست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خرد اندر ره دنیا سره یار است و سلاح</p></div>
<div class="m2"><p>خرد اندر ره دین نیک دلیل است و عصاست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بی خرد گرچه رها باشد در بند بود</p></div>
<div class="m2"><p>با خرد گرچه بود بسته چنان دان که رهاست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای خردمند نگه کن به ره چشم خرد</p></div>
<div class="m2"><p>تا ببینی که بر این امت نادان چه وباست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اینت گوید «همه افعال خداوند کند</p></div>
<div class="m2"><p>کار بنده همه خاموشی و تسلیم و رضاست »</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وانت گوید «همه نیکی ز خدای است ولیک</p></div>
<div class="m2"><p>بدی ای امت بدبخت همه کار شماست»</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وانگه این هر دو مقرند که روزی است بزرگ</p></div>
<div class="m2"><p>هیچ شک نیست که آن روز مکافات و جزاست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو مرا کار نباشد نبوم اهل جزا</p></div>
<div class="m2"><p>اندر این قول خرد را بنگر راه کجاست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون بود عدل بر آنک او نکند جرم، عذاب؟</p></div>
<div class="m2"><p>زی من این هیچ روا نیست اگر زی تو رواست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حاکم روزی قضای تو شده مست سدوم!</p></div>
<div class="m2"><p>نه حکیم است که سازندهٔ گردنده سماست؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اندر این راه خرد را به سزا نیست گذر</p></div>
<div class="m2"><p>بر ره و رسم خرد رو، که ره او پیداست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مر خداوند جهان را بشناس و بگزار</p></div>
<div class="m2"><p>شکر او را که تو را این دو به از ملک سباست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>حکمت آموز و، کم آزار و، نکو گو و بدانک</p></div>
<div class="m2"><p>روز حشر این همه را قیمت و بازار و بهاست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مردم آن است که دین است و هنر جامهٔ او</p></div>
<div class="m2"><p>نه یکی بی هنر و فضل که دیباش قباست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جهد کن تا به سخن مردم گردی و، بدان</p></div>
<div class="m2"><p>که به جز مرد سخن خلق همه خار و گیاست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همچنان چون تن ما زنده به آب است و هوا</p></div>
<div class="m2"><p>سخن خوب، دل مردم را آب و هواست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سخن خوب ز حجت شنو ار والائی</p></div>
<div class="m2"><p>که سخن‌هاش سوی مردم والا، والاست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر سخنهای کسائی شده پیرند و ضعیف</p></div>
<div class="m2"><p>سخن حجت با قوت و تازه و برناست</p></div></div>