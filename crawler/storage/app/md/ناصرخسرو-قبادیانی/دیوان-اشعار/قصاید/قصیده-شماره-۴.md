---
title: >-
    قصیدهٔ شمارهٔ ۴
---
# قصیدهٔ شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>سلام کن ز من ای باد مر خراسان را</p></div>
<div class="m2"><p>مر اهل فضل و خرد را نه عام نادان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خبر بیاور ازیشان به من چو داده بوی</p></div>
<div class="m2"><p>ز حال من به حقیقت خبر مر ایشان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگویشان که جهان سر و من چو چنبر کرد</p></div>
<div class="m2"><p>به مکر خویش و، خود این است کار گیهان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگر که تان نکند غره عهد و پیمانش</p></div>
<div class="m2"><p>که او وفا نکند هیچ عهد و پیمان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلان اگر به شک است اندر آنچه خواهد کرد</p></div>
<div class="m2"><p>جهان بدو، بنگر، گو، به چشم بهمان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین همه بستاند به جمله هر چه‌ش داد</p></div>
<div class="m2"><p>چنانکه بازستد هرچه داده بود آن را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آنکه در دهنش این زمان نهد پستان</p></div>
<div class="m2"><p>دگر زمان بستاند به قهر پستان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگه کنید که در دست این و آن چو خراس</p></div>
<div class="m2"><p>به چند گونه بدیدید مر خراسان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به ملک ترک چرا غره‌اید؟ یاد کنید</p></div>
<div class="m2"><p>جلال و عزت محمود زاولستان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجاست آنکه فریغونیان زهیبت او</p></div>
<div class="m2"><p>ز دست خویش بدادند گوزگانان را؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو هند را به سم اسپ ترک ویران کرد</p></div>
<div class="m2"><p>به پای پیلان بسپرد خاک ختلان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کسی چنو به جهان دیگری نداد نشان</p></div>
<div class="m2"><p>همی به سندان اندر نشاند پیکان را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو سیستان ز خلف، ری زرازیان، بستد</p></div>
<div class="m2"><p>وز اوج کیوان سر برفراشت ایوان را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فریفته شده می‌گشت در جهان و، بلی</p></div>
<div class="m2"><p>چنو فریفته بود این جهان فراوان را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شما فریفتگان پیش او همی گفتید</p></div>
<div class="m2"><p>«هزار سال فزون باد عمر سلطان را»</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به فر دولت او هر که قصد سندان کرد</p></div>
<div class="m2"><p>به زیر دندان چون موم یافت سندان را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پریر قبلهٔ احرار زاولستان بود</p></div>
<div class="m2"><p>چنانکه کعبه است امروز اهل ایمان را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کجاست اکنون آن فر و آن جلالت و جاه</p></div>
<div class="m2"><p>که زیر خویش همی دید برج سرطان را؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بریخت چنگش و فرسوده گشت دندانش</p></div>
<div class="m2"><p>چو تیز کرد برو مرگ چنگ و دندان را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بسی که خندان کرده‌است چرخ گریان را</p></div>
<div class="m2"><p>بسی که گریان کرده‌است نیز خندان را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قرار چشم چه داری به زیر چرخ؟ چو نیست</p></div>
<div class="m2"><p>قرار هیچ به یک حال چرخ گردان را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کناره گیر ازو کاین سوار تازان است</p></div>
<div class="m2"><p>کسی کنار نگیرد سوار تازان را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بترس سخت ز سختی چو کاری آسان شد</p></div>
<div class="m2"><p>که چرخ زود کند سخت کار آسان را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برون کند چو درآید به خشم گشت زمان</p></div>
<div class="m2"><p>ز قصر قیصر را و زخان و مان خان را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر آسمان ز کسوف سیه رهایش نیست</p></div>
<div class="m2"><p>مر آفتاب درفشان و ماه تابان را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>میانه کار بباش، ای پسر، کمال مجوی</p></div>
<div class="m2"><p>که مه تمام نشد جز ز بهر نقصان را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز بهر حال نکو خویشتن هلاک مکن</p></div>
<div class="m2"><p>به در و مرجان مفروش خیره مر جان را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نگاه کن که به حیلت همی هلاک کنند</p></div>
<div class="m2"><p>ز بهر پر نکو طاوسان پران را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر شراب جهان خلق را چو مستان کرد</p></div>
<div class="m2"><p>توشان رها کن چون هشیار مستان را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نگاه کن که چو فرمان دیو ظاهر شد</p></div>
<div class="m2"><p>نماند فرمان در خلق خویش یزدان را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به قول بندهٔ یزدان قادرند ولیک</p></div>
<div class="m2"><p>به اعتقاد همه امتند شیطان را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بگویشان که شما به اعتقاد دیوانید</p></div>
<div class="m2"><p>که دیو خواند خوش‌آید همیشه دیوان را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو مست خفت به بالینش بر تو، ای هشیار،</p></div>
<div class="m2"><p>مزن گزافه به انگشت خویش پنگان را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زیان نبود و نباشد ازو چنانکه نبود</p></div>
<div class="m2"><p>زیان ز معصیت دیو مر سلیمان را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو را تن تو چو بند است و این جهان زندان</p></div>
<div class="m2"><p>مقر خویش مپندار بند و زندان را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز علم و طاعت جانت ضعیف و عریان است</p></div>
<div class="m2"><p>به علم کوش و بپوش این ضعیف عریان را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به فعل بندهٔ یزدان نه‌ای به نامی تو</p></div>
<div class="m2"><p>خدای را تو چنانی که لاله نعمان را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به آشکاره تن اندر که کرد جان پنهان؟</p></div>
<div class="m2"><p>به پیش او دار این آشکار و پنهان را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خدای با تو بدین صنع نیک احسان کرد</p></div>
<div class="m2"><p>به قول و فعل تو بگزار شکر احسان را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جهان زمین و سخن تخم و جانت دهقان است</p></div>
<div class="m2"><p>به کشت باید مشغول بود دهقان را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چرا کنون که بهار است جهد آن نکنی</p></div>
<div class="m2"><p>که تا یکی به کف آری مگر ز مستان را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>من این سخن که بگفتم تو را نکومثل است</p></div>
<div class="m2"><p>مثل بسنده بود هوشیار مردان را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دل تو نامهٔ عقل و سخنت عنوان است</p></div>
<div class="m2"><p>بکوش سخت و نکو کن ز نامه عنوان را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تو را خدای ز بهر بقا پدید آورد</p></div>
<div class="m2"><p>تو را و خاک و هوا و نبات و حیوان را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نگاه کن که بقا را چگونه می‌کوشد</p></div>
<div class="m2"><p>به خردگی منگر دانهٔ سپندان را</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بقا به علم خدا اندر است و، فرقان است</p></div>
<div class="m2"><p>سرای علم و، کلید و درست فرقان را</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اگر به علم و بقا هیچ حاجت است تورا</p></div>
<div class="m2"><p>سوی درش بشتاب و بجوی دربان را</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>در سرای نه چوب است بلکه دانایی است</p></div>
<div class="m2"><p>که بنده نیست ازو به خدای سبحان را</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به جد او و بدو جمله باز یابد گشت</p></div>
<div class="m2"><p>به روز حشر همه مؤمن و مسلمان را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مرا رسول رسول خدای فرمان داد</p></div>
<div class="m2"><p>به مؤمنان که بدانند قدر فرمان را</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کنون که دیو خراسان به جمله ویران کرد</p></div>
<div class="m2"><p>ازو چگونه ستانم زمین ویران را</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو خلق جمله به بازار جهل رفته‌ستند</p></div>
<div class="m2"><p>همی ز بیم نیارم گشاد دکان را</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مرا به دل ز خراسان زمین یمگان است</p></div>
<div class="m2"><p>کسی چرا طلبد مر مرا و یمگان را</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ز عمر بهره همین است مر مرا که به شعر</p></div>
<div class="m2"><p>به رشته می‌کنم این زر و در و مرجان را</p></div></div>