---
title: >-
    قصیدهٔ شمارهٔ ۲۲۱
---
# قصیدهٔ شمارهٔ ۲۲۱

<div class="b" id="bn1"><div class="m1"><p>تا کی خوری دریغ ز برنائی؟</p></div>
<div class="m2"><p>زین چاه آرزو ز چه برنائی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانست بایدت چو بیفزودی</p></div>
<div class="m2"><p>کاخر، اگرچه دیر، بفرسائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنگر که عمر تو به رهی ماند</p></div>
<div class="m2"><p>کوتاه، اگر تو اهل هش و رائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر روز منزلی بروی زین ره</p></div>
<div class="m2"><p>هرچند کارمیده و بر جائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیر کبود چرخ بی‌آسایش</p></div>
<div class="m2"><p>هرگز گمان مبر که بیاسائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر مرکب زمانه نشسته‌ستی</p></div>
<div class="m2"><p>زو هیچ رو نه‌ای که فرود آئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیری نهاد خنجر بر نایت</p></div>
<div class="m2"><p>تا کی خوری دریغ ز برنائی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناخن ز دست حرص به خرسندی</p></div>
<div class="m2"><p>چون نشکنی و پست نپیرائی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان را به آتش خرد و طاعت</p></div>
<div class="m2"><p>از معصیت چرا که نپالائی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پنجاه سال براثر دیوان</p></div>
<div class="m2"><p>رفتی به بی‌فساری و رسوائی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر معصیت گماشته روز و شب</p></div>
<div class="m2"><p>جان و دل و دو گوش و دو بینائی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک روز چونکه نیکی بلفنجی</p></div>
<div class="m2"><p>کمتر بود ز رشتهٔ یکتائی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بند قبای چاکری سلطان</p></div>
<div class="m2"><p>چون از میان ریخته نگشائی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرمان کردگار یله کرده</p></div>
<div class="m2"><p>شه را لطف کنی که «چه فرمائی؟»</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مؤذن چو خواندت زپی مسجد</p></div>
<div class="m2"><p>تو اوفتاده ژاژ همی خائی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ور شاه خواندت به سوی گلشن</p></div>
<div class="m2"><p>ره را به چشم و روی بپیمائی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا مذهب تو این بود و سیرت</p></div>
<div class="m2"><p>جز مرجحیم را تو کجا شائی؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در کار خویش غافل چون باشی؟</p></div>
<div class="m2"><p>بر خویشتن مگر به معادائی!</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون سوی علم و طاعت نشتابی؟</p></div>
<div class="m2"><p>ای رفتنی شده چه همی پائی؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بی علم دین همی چه طمع داری؟</p></div>
<div class="m2"><p>در هاون آب خیره چرا سائی؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عاصی سزای رحمت کی باشد؟</p></div>
<div class="m2"><p>خورشید را همی به گل اندائی!</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رحمت نه خانه‌ای است بلند و خوش</p></div>
<div class="m2"><p>نه جامه‌ای است رنگی و پهنائی!</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دین است و علم رحمت، خود دانی</p></div>
<div class="m2"><p>او را اگر تو ز اهل تؤلائی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رحمت به سوی جان تو نگراید</p></div>
<div class="m2"><p>تا تو به‌سوی رحمت نگرائی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بخشایش از که چشم همی داری؟</p></div>
<div class="m2"><p>برخویشتن خود از چه نبخشائی؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یک چند اگر زراه بیفتادی</p></div>
<div class="m2"><p>زی راه باز شو که نه شیدائی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شاید که صورت گنهانت را</p></div>
<div class="m2"><p>اکنون به دست توبه بیارائی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اول خطا ز آدم و حوا بد</p></div>
<div class="m2"><p>تو هم ز نسل آدم وحوائی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بشتاب سوی طاعت و زی دانش</p></div>
<div class="m2"><p>غره مشو به مهلت دنیائی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آن کن ز کارها که چو دیگر کس</p></div>
<div class="m2"><p>آن را کند بر آنش تو بستائی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در کارهای دینی و دنیائی</p></div>
<div class="m2"><p>جز همچنان مباش که بنمائی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زنهار که به‌سیرت طراران</p></div>
<div class="m2"><p>ارزن نموده ریگ نپیمائی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>با مردم نفایه مکن صحبت</p></div>
<div class="m2"><p>زیرا که از نفایه بیالائی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون روزگار برتو بیاشوبد</p></div>
<div class="m2"><p>یک چند پیشه کن تو شکیبائی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زیرا که گونه گونه همی گردد</p></div>
<div class="m2"><p>جافی جهان ،چو مردم سودائی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر صحبت نفایه و بی‌دانش</p></div>
<div class="m2"><p>بگزین به‌طبع وحشت تنهائی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بر خوی نیک و عدل وکم آزاری</p></div>
<div class="m2"><p>بفزای تا کمال بیفزائی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ای بی‌وفا زمانه تو مر ما را،</p></div>
<div class="m2"><p>هرچند بی‌وفائی ،در بائی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز آبستنی تهی نشوی هرگز</p></div>
<div class="m2"><p>هرچند روز روز همی زائی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زیرا ز بهر نعمت باقی تو</p></div>
<div class="m2"><p>سرمایه توانگری مائی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پیدات دیگر است و نهان دیگر</p></div>
<div class="m2"><p>باطن چو خا رو ظاهر خرمائی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>امروز هرچه‌مان بدهی، فردا</p></div>
<div class="m2"><p>از ما مکابره همه بربائی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>داند خرد همی که بر این عادت</p></div>
<div class="m2"><p>کاری بزرگ را شده برپایی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جان گوهر است و تن صدف گوهر</p></div>
<div class="m2"><p>در شخص مردمی و تو دریائی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بل مردم است میوه تو را و، تو</p></div>
<div class="m2"><p>یکی درخت خوب مهیائی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>معیوب نیستی تو ولیکن ما</p></div>
<div class="m2"><p>بر تو نهیم عیب ز رعنائی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ای حجت زمین خراسان تو</p></div>
<div class="m2"><p>هرچند قهر کردهٔ غوغائی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>پنهان شدی ولیک به حکمت‌ها</p></div>
<div class="m2"><p>خورشیدوار شهره و پیدائی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از شخص تیره گرچه به یمگانی</p></div>
<div class="m2"><p>از قول خوب بر سر جوزائی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از هرچه گفته‌ام نه همی جویم</p></div>
<div class="m2"><p>جز نیکی، ای خدای تو دانائی</p></div></div>