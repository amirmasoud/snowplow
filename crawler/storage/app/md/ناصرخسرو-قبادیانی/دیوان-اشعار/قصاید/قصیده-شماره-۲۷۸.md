---
title: >-
    قصیدهٔ شمارهٔ ۲۷۸
---
# قصیدهٔ شمارهٔ ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>چنین در کارها بسیار مندیش</p></div>
<div class="m2"><p>مگو ورنه بکن کاری که گفتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نباید کز چنین تدبیر بسیار</p></div>
<div class="m2"><p>ز تاریکی به تاریکی درافتی</p></div></div>