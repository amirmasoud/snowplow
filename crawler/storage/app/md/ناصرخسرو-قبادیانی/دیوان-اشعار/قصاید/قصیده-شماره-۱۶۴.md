---
title: >-
    قصیدهٔ شمارهٔ ۱۶۴
---
# قصیدهٔ شمارهٔ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>گر تو ای چرخ گردان مادرم</p></div>
<div class="m2"><p>چون نه‌ای تو دیگر و من دیگرم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خردمندان، که باشد در جهان</p></div>
<div class="m2"><p>با چنین بد مهر مادر داورم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونکه من پیرم جهان تازه جوان</p></div>
<div class="m2"><p>گر نه زین مادر بسی من مهترم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشکلی پیش آمده‌ستم بس عجب</p></div>
<div class="m2"><p>ره نمی‌داند بدو در خاطرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا همی برمن زمانه بگذرد</p></div>
<div class="m2"><p>یا همی من بر زمانه بگذرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرگ مردم خوار گشته‌است این جهان</p></div>
<div class="m2"><p>بنگر اینک گر نداری باورم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون جهان می‌خورد خواهد مرمرا</p></div>
<div class="m2"><p>من غم او بیهده تا کی خورم؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای برادر، گر ببینی مر مرا</p></div>
<div class="m2"><p>باورت ناید که من آن ناصرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون دگرگون شد همه احوال من</p></div>
<div class="m2"><p>گر نشد دیگر به گوهر عنصرم؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حسن و بوی و رنگ بود اعراض من</p></div>
<div class="m2"><p>پاک بفگند این عرضها جوهرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شیر غران بودم اکنون روبهم</p></div>
<div class="m2"><p>سرو بستان بودم اکنون چنبرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لاله‌ای بودم به بستان خوب رنگ</p></div>
<div class="m2"><p>تازه، و اکنون چون بر نیلوفرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن سیه مغفر که بر سر داشتم</p></div>
<div class="m2"><p>دست شستم سال بربود از سرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر شدم غره به دنیا لاجرم</p></div>
<div class="m2"><p>هر جفائی را که دیدم درخورم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر تو را دنیا همی خواند به زرق</p></div>
<div class="m2"><p>من دروغ و زرق او را منکرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن کند با تو که با من کرد راست</p></div>
<div class="m2"><p>پیش من بنشین و نیکو بنگرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فعل های او زمن بر خوان که من</p></div>
<div class="m2"><p>مر تو را زین چرخ جافی محضرم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای مسلمانان، به دنیا مگروید</p></div>
<div class="m2"><p>من شما را زو گواه حاضرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با شما گر عهد بست ابلیس ازو</p></div>
<div class="m2"><p>گر وفا یابید ازو من کافرم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>این جهان بود، ای پسر، عمری دراز</p></div>
<div class="m2"><p>هر سوئی یار و رفیق بهترم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رفته‌ام با او به تاریکی بسی</p></div>
<div class="m2"><p>تا تو گفتی دیگری اسکندرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زیرپای خویش بسپرد او مرا</p></div>
<div class="m2"><p>من ره او نیز هرگز نسپرم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر جهان با من کنون خنجر کشد</p></div>
<div class="m2"><p>علم توحید است با وی خنجرم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نیز از این عالم نباشم برحذر</p></div>
<div class="m2"><p>زانکه من مولای آل حیدرم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>افسر عالم امام روزگار</p></div>
<div class="m2"><p>کز جلالش بر فلک سود افسرم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فر او پر نور کرد اشعار من</p></div>
<div class="m2"><p>گرت باید بنگر اینک دفترم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای خردمندی که نامم بشنوی</p></div>
<div class="m2"><p>زین خران گر هوشیاری مشمرم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وز محال عام نادان همچو روز</p></div>
<div class="m2"><p>پاک دان هم بستر و هم چادرم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هیچ با بوبکر و با عمر لجاج</p></div>
<div class="m2"><p>نیست امروز و نه روز محشرم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کار عامه است این چنین ترفندها</p></div>
<div class="m2"><p>نازموده خیره خیره مشکرم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن همی گوید که سلمان بود امام</p></div>
<div class="m2"><p>وین همی گوید که من با عمرم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اینت گوید مذهب نعمان به است</p></div>
<div class="m2"><p>وانت گوید شافعی را چاکرم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر بخرم هیچ کس را بر گزاف</p></div>
<div class="m2"><p>همچو ایشان لامحاله من خرم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مر مرا بر راه پیغمبر شناس</p></div>
<div class="m2"><p>شاعرم مشناس اگرچه شاعرم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چند پرسی «بر طریق کیستی؟»</p></div>
<div class="m2"><p>بر طریق و ملت پیغمبرم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون سوی معروف معروفم چه باک</p></div>
<div class="m2"><p>گر سوی جهال زشت و منکرم!</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر به حجت پیشم آید آفتاب</p></div>
<div class="m2"><p>بی‌گمان گردی کزو روشن‌ترم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ظاهری را حجت ظاهر دهم</p></div>
<div class="m2"><p>پیش دانا حجت عقلی برم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پیش دانا به آستین دست دین</p></div>
<div class="m2"><p>روی حق از گرد باطل بسترم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نیست برمن پادشاهی آز را</p></div>
<div class="m2"><p>میر خویشم، نیست مثلی همبرم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر تو را گردن نهم از بهر مال</p></div>
<div class="m2"><p>پس خطا کرده‌است بر من مادرم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ای برادر، کوه دارم در جگر</p></div>
<div class="m2"><p>چون شوی غره به شخص لاغرم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>برتر از گردون گردانم به قدر</p></div>
<div class="m2"><p>گرچه یک چندی بدین شخص اندرم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شخص جان من به سان منظری است</p></div>
<div class="m2"><p>تا از این منظر به گردون بر پرم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مر مرا زین منظر خوب، ای پسر</p></div>
<div class="m2"><p>رفته گیر و مانده اینجا منظرم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>منبر جان است شخصم گوش‌دار</p></div>
<div class="m2"><p>پند گیر اکنون که من بر منبرم</p></div></div>