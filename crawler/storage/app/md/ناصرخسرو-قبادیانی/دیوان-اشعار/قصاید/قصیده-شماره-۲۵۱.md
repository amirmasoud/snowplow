---
title: >-
    قصیدهٔ شمارهٔ ۲۵۱
---
# قصیدهٔ شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>آسایشت نبینم ای چرخ آسیائی</p></div>
<div class="m2"><p>خود سوده می‌نگردی ما را همی بسائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را همی فریبد گشت دمادم تو</p></div>
<div class="m2"><p>من در تو چون بپایم گر تو همی نپائی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس بی‌وفا و مهری کز دوستان یکدل</p></div>
<div class="m2"><p>نور جمال و رونق خوش خوش همی ربائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کو همیت جوید تو زو همی گریزی</p></div>
<div class="m2"><p>این است رسم زشتی و آثار بی‌وفائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسیار گشت دورت تا مرد بی‌تفکر</p></div>
<div class="m2"><p>گوید همی قدیمی بی‌حد و منتهائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایام بر دو قسم است آینده و گذشته</p></div>
<div class="m2"><p>وان را به وقت حاضر باشد ازین جدائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس تو به وقت حاضر نزدیک مرد دانا</p></div>
<div class="m2"><p>زان رفته انتهائی ز آینده ابتدائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس تو که روزگارت با اول است و آخر</p></div>
<div class="m2"><p>هرچند دیر مانی میرنده همچو مائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وان را که بی‌بصارت یافه همی در آید</p></div>
<div class="m2"><p>بر محدثیت بس باد از گشتنت گوائی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرگز قدیم باشد جنبدهٔ مکانی؟</p></div>
<div class="m2"><p>زین قول می‌بخندد شهری و روستائی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پرگرد باغ و بی‌بر شاخ و خلنده خاری</p></div>
<div class="m2"><p>تاریک چاه و ناخوش زشت و درشت جائی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جز زاد ساختن را از بهر راه عقبی</p></div>
<div class="m2"><p>هشیار و پیش بین را هرگز بکار نائی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن را که دست و رویت چون دوستان ببوسد</p></div>
<div class="m2"><p>چون گرگ روی و دستش بشخاری و بخائی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صیاد بی‌محابا هرگز چو تو ندیدم</p></div>
<div class="m2"><p>غدار گنده پیری پر مکر و با روائی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرکس پس تو آید از مکر وز مرائی</p></div>
<div class="m2"><p>گوئی که من تو راام چونان که تو مرائی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای داده دل به دنیا، از پیش و پس نگه کن</p></div>
<div class="m2"><p>بندیش تا چه کردی بنگر که تا کجائی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از بس خطا و زلت ناخوب‌ها که کردی</p></div>
<div class="m2"><p>در چنگل عقابی در کام اژدهائی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر هوش یار داری امروز بایدت جست</p></div>
<div class="m2"><p>ای هوشیار مردم، زین اژدها رهائی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زین اژدهای پیسه نتواندت رهاندن</p></div>
<div class="m2"><p>ای پر خطا و زلت، جز رحمت خدائی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>با خویشتن بیندیش، ای دوست، تابدانی</p></div>
<div class="m2"><p>کز فعل خویش هر بد هر زشت را سزائی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رفتند همرهانت منشین بساز توشه</p></div>
<div class="m2"><p>مر معدن بقا را زین منزل فنائی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جز خواب و خور نبینم کارت، مگر ستوری؟</p></div>
<div class="m2"><p>بر سیرت ستوران گر مردمی چرائی؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بس سالها برآمد تا تو همی بپوئی</p></div>
<div class="m2"><p>زین پوی پوی حاصل پررنج و درد پائی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مر هر که را بینی یا هر کجا نشینی</p></div>
<div class="m2"><p>گاهی ز درد نالی گاهی ز بی‌نوائی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کشت خدای بودی اکنون تو زرد گشتی</p></div>
<div class="m2"><p>گاه درودن آمد بیهوده چون درائی؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر تو ز بهر خدمت رفتن به پیش میران</p></div>
<div class="m2"><p>اندر غم قبائی تو از در قفائی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از بس که بر تو بگذشت این آسیای گیتی</p></div>
<div class="m2"><p>چون مرد آسیابان پر گرد آسیائی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اکنون که از تو بنهفت آن بت رخ زدوده</p></div>
<div class="m2"><p>آن به که مهر او را از دل فرو زدائی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ترسم به دل فروشد از سرت آن سیاهی</p></div>
<div class="m2"><p>وز دل به سر برآمد زان بیم روشنائی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ورنه به کار دنیا چون جلد و سخت کوشی</p></div>
<div class="m2"><p>وانگه به کار دین در بی‌توش و سست رائی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چندین چرا خرامی آراسته بگشی</p></div>
<div class="m2"><p>در جبهٔ بهائی گر نیستی بهائی؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تن زیر زیب و زینت جان بی‌جمال و رونق</p></div>
<div class="m2"><p>با صورت رجالی بر سیرت نسائی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>طاووس خواستندت می‌آفرید از اول</p></div>
<div class="m2"><p>طاووس مردمی تو ایدون همی نمائی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از دوستی دنیا بندهٔ امیر و شاهی</p></div>
<div class="m2"><p>وز آرزوی مرکب خمیده چون حنائی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کی بازگشت خواهی زی خالق، ای برادر</p></div>
<div class="m2"><p>آنگه که نیز خدمت مخلوق را نشائی؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر توبه کرد خواهی زان پیش باید این کار</p></div>
<div class="m2"><p>کز تنت باز خواهند این گوهر عطائی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون نیز هیچ طاقت بر کردنت نماند</p></div>
<div class="m2"><p>آنگاه کرد خواهی پرهیز و پارسائی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر همت تو این است، ای بی‌تمیز، پس تو</p></div>
<div class="m2"><p>با کردگار عالم در مکر و کیمیائی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ور سوی تو صواب است این کار سوی دانا</p></div>
<div class="m2"><p>والله که بر خطائی حقا که بر خطائی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون آشنات باشد ابلیس مکر پیشه</p></div>
<div class="m2"><p>با زرق و مکر یابی ناچاره آشنائی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نشگفت اگر نداند جز مکر خلق ایراک</p></div>
<div class="m2"><p>چیزی نماند جز نام از دین مصطفائی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دجال را نبینی بر امت محمد</p></div>
<div class="m2"><p>گسترده در خراسان سلطان و پادشائی؟</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یارانش تشنه یکسر و ز دوستی‌ی ریاست</p></div>
<div class="m2"><p>هریک همی به حیلت دعوی کند سقائی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بازار زهد کاسد، سوق فسوق رایج</p></div>
<div class="m2"><p>افگنده خوار دانش، گشته روان مرائی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ترکان به پیش مردان زین پیش در خراسان</p></div>
<div class="m2"><p>بودند خوار و عاجز همچون زنان سرائی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>امروز شرم ناید آزاده زادگان را</p></div>
<div class="m2"><p>کردن به پیش ترکان پشت از طمع دوتائی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آب طمع ببرده‌است از خلق شرم یارب</p></div>
<div class="m2"><p>ما را توی نگهبان زین آفت سمائی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تو شعرهای حجت بر خویشتن به حجت</p></div>
<div class="m2"><p>برخوان اگر کهن گشت آن گفتهٔ کسائی</p></div></div>