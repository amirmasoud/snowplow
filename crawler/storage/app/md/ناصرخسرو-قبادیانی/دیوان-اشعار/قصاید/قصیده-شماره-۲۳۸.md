---
title: >-
    قصیدهٔ شمارهٔ ۲۳۸
---
# قصیدهٔ شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>ای شده مشغول به ناکردنی،</p></div>
<div class="m2"><p>گرد جهان بیهده تا کی دنی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آهن اگر چند گران شد، تورا</p></div>
<div class="m2"><p>سلسله بایدت ازو ده منی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونکه نشوئی به خرد روی جهل</p></div>
<div class="m2"><p>برنکشی از سرت آهرمنی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچه نه خوش است و نه نیکو برش</p></div>
<div class="m2"><p>تخمش خواهیم که نپراگنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمرت شاخی است پر از بار و خار</p></div>
<div class="m2"><p>چون تو همه خار همی برچنی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مردم اگر جان و تن است از چه روی</p></div>
<div class="m2"><p>فتنه تو بر جانت نه‌ای بر تنی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جانت برهنه است و تو این تار و پود</p></div>
<div class="m2"><p>بر تن تاریک همی بر تنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جوشن روشن خرد توست تن</p></div>
<div class="m2"><p>تو نه همه این تن چون جوشنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان تو چون بفگند این جوشنت</p></div>
<div class="m2"><p>باز دهد جوشنت این روشنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تنت به جان، ای پسر، آبستن است</p></div>
<div class="m2"><p>باز رهد روزی از آبستنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مادر تن را پسر این جان توست</p></div>
<div class="m2"><p>مادر باقی و پسر رفتنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در شکم مادر خود بخت نیک</p></div>
<div class="m2"><p>چونکه نکوشی که به حاصل کنی؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر طلب طاعت و نیکی و زهد</p></div>
<div class="m2"><p>چونکه نه دامن به کمر در زنی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مریم عمران نشد از قانتین</p></div>
<div class="m2"><p>جز که به پرهیز برو برزنی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طاعت و نیکی و صلاح است بخت</p></div>
<div class="m2"><p>خوردنیئی نیست نه پوشیدنی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جهد کن ار عهد تو را بشکنند</p></div>
<div class="m2"><p>تا تو مگر عهد کسی نشکنی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آز نگردد ابدا گرد آنک</p></div>
<div class="m2"><p>در شکم مادر گردد غنی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون تو که باشد چو تو را بخت نیک</p></div>
<div class="m2"><p>مادرزادی بود و معدنی؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرت مراد است کز این ژرف چاه</p></div>
<div class="m2"><p>خویشتن، ای پیر، برون افگنی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زین رمه یک سو شو و از دل بشوی</p></div>
<div class="m2"><p>ریم فرومایگی و ریمنی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو به مثل بی‌خرد و علم و زهد</p></div>
<div class="m2"><p>راست چو کنجارهٔ بی‌روغنی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>روز تو کی نیک شود تا چنین</p></div>
<div class="m2"><p>فتنهٔ این خانهٔ بی‌روزنی؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دیو دل از صحبت تو برکند</p></div>
<div class="m2"><p>چون تو دل از مهر جهان برکنی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بسته در این خانهٔ تاریک و تنگ</p></div>
<div class="m2"><p>شاد چرائی؟ که نه در گلشنی!</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چرخ همی خرد بخواهدت کوفت</p></div>
<div class="m2"><p>خردتر از سرمه‌گر از آهنی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون تو بسی خورده است این گنده پیر</p></div>
<div class="m2"><p>از چه نشستی تو بدین ایمنی؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دی شد و امروز نپاید همی</p></div>
<div class="m2"><p>دی شد و تو منتظر بهمنی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گاه گریزانی از باد سرد</p></div>
<div class="m2"><p>گاه بر امید گل و سوسنی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>روی به دانش کن و رنجه مکن</p></div>
<div class="m2"><p>دل به غم این تن فرسودنی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا نشود جانت به دانش تمام</p></div>
<div class="m2"><p>فخر نشاید که کنی، نه منی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دشمن دانا شدی از فضل او</p></div>
<div class="m2"><p>فضل طلب کن چه کنی دشمنی؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مؤذن ما را مزن و بدمگوی</p></div>
<div class="m2"><p>لحن خوش آموز و تو کن مؤذنی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جای حکیمان مطلب بی‌هنر</p></div>
<div class="m2"><p>زانکه نیاید ز کدو هاونی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مرد خردمند به حکمت شود</p></div>
<div class="m2"><p>تو چه خردمند به پیراهنی؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بار خدائی به سرشت اندر است</p></div>
<div class="m2"><p>مردم را، گر بکند کردنی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جای تو ایوان و گه گلشن است</p></div>
<div class="m2"><p>کاهلیت کرد چنین گلخنی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ور به بسندی به ستوری چنین</p></div>
<div class="m2"><p>تا به ابد یار غم و شیونی</p></div></div>