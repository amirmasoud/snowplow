---
title: >-
    قصیدهٔ شمارهٔ ۲۶۸
---
# قصیدهٔ شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>چنین زرد و نوان مانند نالی</p></div>
<div class="m2"><p>نکرده‌ستم غم دلبر غزالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه آنم من که خنبانید یارد</p></div>
<div class="m2"><p>مرا هجران بدری چون هلالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه مالیده است زیر پا چو خوسته</p></div>
<div class="m2"><p>مرا چون جاهلان را آز مالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم خوبان و آز مال دنیا</p></div>
<div class="m2"><p>کجا باشد همال بی‌همالی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه شب گرد چشم من نگردد</p></div>
<div class="m2"><p>ز خیل خواب و آرامش خیالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی تابد ز چرخ سبز عیوق</p></div>
<div class="m2"><p>چو ز آتش بر صحیفهٔ آب خالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ثریا همچو بگسسته جمیلی</p></div>
<div class="m2"><p>هلال ایدون چو خمیده خلالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب تیره ستاره گرد او در</p></div>
<div class="m2"><p>چو حورانند گرد زشت زالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا تا صبح بشکافد دل شب</p></div>
<div class="m2"><p>نیابد دل ز رنج آرام و هالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درخشد روی صبح از مغرب شب</p></div>
<div class="m2"><p>منور همچو صدقی ز افتعالی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیابد آنگهی عقل مدبر</p></div>
<div class="m2"><p>از اینجا در طریق دین مثالی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز نور صبح مر شب را ببیند</p></div>
<div class="m2"><p>گریزنده چو ز ایمانی ضلالی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ضلالت عزت ایمان نیابد</p></div>
<div class="m2"><p>چو زری کی بود هرگز سفالی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگرچه شب بپوشد روی صورت</p></div>
<div class="m2"><p>نگردد صورت از حالی به حالی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جمال و زیب زیبا کم نگردد</p></div>
<div class="m2"><p>اگر چندش بپوشی در جوالی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نباشد خوار هرگز مرد دانا</p></div>
<div class="m2"><p>بدان که‌ش خوار دارد بدخصالی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر اجلالش کندشاید، وگرنه</p></div>
<div class="m2"><p>نجوید برتر از حکمت جلالی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نباشد چون امیر و شاه و خان را</p></div>
<div class="m2"><p>حکیمان را به مال اندر جمالی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جواب سایل شاهان بگوید</p></div>
<div class="m2"><p>تگینی یا طغانی یا ینالی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ولیکن عاجز و خامش بماند</p></div>
<div class="m2"><p>چو از چون و چرا باشد سؤالی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ایا گردنت بسته بر در شاه</p></div>
<div class="m2"><p>ضیاعی یا عقاری یا عقالی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کمالت کو؟ کمال اندر کمال است</p></div>
<div class="m2"><p>سوی دانا به از مالی کمالی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه آن داناست کز محراب و منبر</p></div>
<div class="m2"><p>همی گوید گزافه قال قالی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر نادان بگیرد جای دانا</p></div>
<div class="m2"><p>به هرحالی نباشد جز محالی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه بیش از شیر باشد گرچه باشد</p></div>
<div class="m2"><p>درنده پیش شیر اندر شگالی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدادم ناصبی را پاسخ حق</p></div>
<div class="m2"><p>نخواهم کرد زین بیش احتمالی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو دشمن دشمنی را کرد پیدا</p></div>
<div class="m2"><p>نشاید نیز کردن پای مالی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به من ناکرده قصد خواسته و خور</p></div>
<div class="m2"><p>نماند اندر خراسان بد فعالی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جز آن جرمی ندانم خویشتن را</p></div>
<div class="m2"><p>که بی‌حجت نمی‌گویم مقالی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز یزدان جز که از راه محمد</p></div>
<div class="m2"><p>ندارم چشم فصلی و اتصالی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه زو برتر کسی دانم به عالم</p></div>
<div class="m2"><p>نه بهتر ز ال او بشناسم آلی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به جان اندر بکشتم حب ایشان</p></div>
<div class="m2"><p>کسی کشته است ازین بهتر نهالی؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حرامی ره نیابد زی من ایرا</p></div>
<div class="m2"><p>همی ترسم مدام از هر حلالی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نگردد چون منی خود گرد بیشی</p></div>
<div class="m2"><p>نه گرد حیلت از بهر منالی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جهان را دیدم و خلق آزمودم</p></div>
<div class="m2"><p>به هر میدان درون جستم مجالی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نه مالی دیدم افزون از قناعت</p></div>
<div class="m2"><p>نه از پرهیز برتر احتیالی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ازان پس که‌م فصاحت بنده گشته است</p></div>
<div class="m2"><p>چگونه بنده باشم پیش لالی؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چرا خواهد مرا نادان متابع؟</p></div>
<div class="m2"><p>نیابد روبه از شیران عیالی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چگونه تکیه یارد کرد هرگز</p></div>
<div class="m2"><p>ممیز مرد بر پوسیده نالی؟</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نگیرم پیش رو مر جاهلی را</p></div>
<div class="m2"><p>که نشناسد نگاری از نکالی</p></div></div>