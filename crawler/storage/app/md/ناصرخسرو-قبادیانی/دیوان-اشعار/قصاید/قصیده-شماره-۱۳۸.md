---
title: >-
    قصیدهٔ شمارهٔ ۱۳۸
---
# قصیدهٔ شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>گنبد پیروزه‌گون پر ز مشاعل</p></div>
<div class="m2"><p>چند بگشته است گرد این کرهٔ گل؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علت جنبش چه بود از اول بودش؟</p></div>
<div class="m2"><p>چیست درین قول اهل علم اوایل؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیست مر این قبه را محرک اول؟</p></div>
<div class="m2"><p>چیست از این کار کرد شهره به حاصل؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پس بی‌فعلی آنکه فعل ازو بود</p></div>
<div class="m2"><p>از چه قبل گشت باز صانع و فاعل؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز که به حاجت نجنبد آنکه بجنبد</p></div>
<div class="m2"><p>وین نشود بر عقول مبهم و مشکل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حال ز بی‌فعل اگر به فعل بگردد</p></div>
<div class="m2"><p>آن ازلی حال بود محدث و زایل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه مر او را بر این مقام بگیری</p></div>
<div class="m2"><p>گرچه سوار است عاجز آید و راجل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>علت جنبش چه چیز ؟ حاجت ناقص</p></div>
<div class="m2"><p>حاصل صفت چه چیز؟ مردم عاقل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناقص محتاج را کمال که بخشد</p></div>
<div class="m2"><p>جز گهری بی‌نیاز و ساکن و کامل؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بار درخت جهان چه آمد؟ مردم</p></div>
<div class="m2"><p>بار درختان ز تخمهاست دلایل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بار چو فرزند و، تخم او پدر اوست</p></div>
<div class="m2"><p>از جو جو زایدو از پلپل پلپل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو که بر تخم عالمی که مر او را</p></div>
<div class="m2"><p>برگ سخن گفتن است و بار فضایل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صانع مصنوع را تو باشی فرزند</p></div>
<div class="m2"><p>پس چو پدر شو کریم و عادل و فاضل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قول مسیح آنکه گفت «زی پدر خویش</p></div>
<div class="m2"><p>می‌شوم» این رمز بود نزد افاضل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عاقل داند که او چه گفت ولیکن</p></div>
<div class="m2"><p>رهبان گمراه گشت و هرقل جاهل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرکه نداند که این لطیف سخن گوی</p></div>
<div class="m2"><p>از چه قبل بسته شد چنین به سلاسل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بند بدید است بسته چون نه بدید است</p></div>
<div class="m2"><p>بند همی بیند از عروق و مفاصل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>غافل ساهی است از شناختن خویش</p></div>
<div class="m2"><p>تا بتوانی مجوی صحبت غافل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از پس دانش قدم نهاد نیارد</p></div>
<div class="m2"><p>باز شود پیش یک درم به دو منزل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای زپس مال در بمانده شب و روز</p></div>
<div class="m2"><p>نیستی الا که سایه‌ای متمول</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دل بنهادی به ذل از قبل مال</p></div>
<div class="m2"><p>علت ذل تو گشت در بر تو دل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مال چنه است و زمانه دام جهان است</p></div>
<div class="m2"><p>ای همه سال به دام پر چنه مایل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرغ که در دام پر چنه طمع افگند</p></div>
<div class="m2"><p>بخت بد آنگاه خاردش رگ بسمل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حرص بینداز و آب‌روی نگه‌دار</p></div>
<div class="m2"><p>ستر قناعت به روی خویش فروهل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فتنه مشو خیره بر حمایل زرین</p></div>
<div class="m2"><p>علم نکوتر، زعلم ساز حمایل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فتنهٔ این روزگار پر غش و غلی</p></div>
<div class="m2"><p>زانکه نگشته است جانت بی غش و بی غل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سائل دانا نماند هیچ کس امروز</p></div>
<div class="m2"><p>سائل شاهند خلق و سائل عامل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر تو به سوی سؤال علم شتابی</p></div>
<div class="m2"><p>پیش تو عامل ذلیل گردد و سائل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در ره دین پوی بر ستور شریعت</p></div>
<div class="m2"><p>وز علما دان در این طریق منازل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر تو ببری به جهد بادیهٔ جهل</p></div>
<div class="m2"><p>آب تو را بس جواب و، زاد مسائل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر ره غولان نشسته‌اند حذر کن</p></div>
<div class="m2"><p>باز نهاده دهان‌ها چو حواصل</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دشمن عدلند و ضد حکمت اگر چند</p></div>
<div class="m2"><p>یکسره امروز حاکمند و معدل</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر یکی از بهر صید این ضعفا را</p></div>
<div class="m2"><p>تیز چو نشپیل کرده‌اند انامل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بنگرشان تا به چشم سرت ببینی</p></div>
<div class="m2"><p>جایگه حق گرفته هیکل باطل</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خامش و آهستگان به روز ولیکن</p></div>
<div class="m2"><p>در می و مجلس به شب به سان جلاجل</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر که ثوابش شراب و ساقی حور است</p></div>
<div class="m2"><p>تکیه زده با موافقان متقابل</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>و امروز اینجا همی نیاید هرگز</p></div>
<div class="m2"><p>عاجل نقدش دهد به نسیهٔ آجل</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هیچ نبیند که رنج بیند یک روز</p></div>
<div class="m2"><p>ظالم در روزگار خویش و نه قاتل</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بلکه ستمکش به رنج و در بمیرد</p></div>
<div class="m2"><p>باز ستمگار دیر ماند و مقبل</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>این همه مکر است از خدای تعالی</p></div>
<div class="m2"><p>منشین ایمن ز مکرش ای متغافل</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>راحت و رنج از بهشت خلد و ز دوزخ</p></div>
<div class="m2"><p>چاشنیی‌دان در این سرای به عاجل</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بحر عظیم از قیاس عالم عالی است</p></div>
<div class="m2"><p>کشتی او چیست؟ این قباب اسافل</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>باز جهان بحر دیگر است و بدو در</p></div>
<div class="m2"><p>شخص تو کشتی است و عمر باد مقابل</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>باد مقابل چو راند کشتی را راست</p></div>
<div class="m2"><p>هم برساندش، اگر چه دیر، به ساحل</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ساحل تو محشر است نیک بیندیش</p></div>
<div class="m2"><p>تا به چه بار است کشتیت متحمل</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بارش افعال توست، وان همه فردا</p></div>
<div class="m2"><p>شهره بباشد سوی شعوب و قبایل</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بنگر تا عقل کان رسول خدای است</p></div>
<div class="m2"><p>برتو چه خواند که کرده‌ای ز رذایل</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بنگر، پیوستی آنچه گفت بپیوند؟</p></div>
<div class="m2"><p>بنگر، بگسستی آنچه گفت که بگسل؟</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اینجا بنگر حساب خویش هم امروز</p></div>
<div class="m2"><p>کاینجا حاضر شدند مرسل و مرسل</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تا به تغافل ز کار خویش نیفتی</p></div>
<div class="m2"><p>فردا ناگه به رنج نامتبدل</p></div></div>