---
title: >-
    قصیدهٔ شمارهٔ ۱۶۵
---
# قصیدهٔ شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>اگر با خرد جفت و اندر خوریم</p></div>
<div class="m2"><p>غم‌خور چو خر چندو تاکی خوریم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سزد کز خری دور باشیم ازانک</p></div>
<div class="m2"><p>خداوند و سالار گاو و خریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر خر همی کشت حالی چرد</p></div>
<div class="m2"><p>چرا ما نه از کشت باقی چریم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه فضل آوریم، ای پسر، بر ستور</p></div>
<div class="m2"><p>اگر همچو ایشان خوریم و مریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرو سو نخواهیم شد ما همی</p></div>
<div class="m2"><p>که ما سر سوی گنبد اخضریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر از علم و طاعت برآریم پر</p></div>
<div class="m2"><p>از این‌جا به چرخ برین بر پریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به چرخ برین بر پرد جان ما</p></div>
<div class="m2"><p>گر او را به خورهای دین‌پروریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه‌ایم ایدری ما به جان و خرد</p></div>
<div class="m2"><p>وگر چند یک چندگاه ایدریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به زنجیر عنصر ببستندمان</p></div>
<div class="m2"><p>چو دیوانگان زان به بند اندریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلی بندو زندان ما عنصر است</p></div>
<div class="m2"><p>وگر چند ما فتنه بر عنصریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به بند ستوری درون بسته‌ایم</p></div>
<div class="m2"><p>وگر چند بسته بدان گوهریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به زندان پیشین درون نیستیم</p></div>
<div class="m2"><p>نبینی که بر صورت دیگریم؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نبینی که از بی‌تمیزی ستور</p></div>
<div class="m2"><p>چو بی بر چنار است و ما بروریم؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو عرعر نگونسار مانده نه‌ایم</p></div>
<div class="m2"><p>اگر چند با قامت عرعریم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چرا بنده شدمان درخت و ستور؟</p></div>
<div class="m2"><p>بیا تا به کار اندرون بنگریم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سزد گر چو این هر دو مشغول خور</p></div>
<div class="m2"><p>نباشیم ازیرا که ما بهتریم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سر از چرخ نیلوفری برکشیم</p></div>
<div class="m2"><p>به دانش که داننده نیلوفریم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به دانش رگ مکر و زنگار جهل</p></div>
<div class="m2"><p>ز بن بگسلیم و ز دل بستریم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به بیداد و بیدادگر نگرویم</p></div>
<div class="m2"><p>که ما بندهٔ داور اکبریم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر داد خواهیم در نیک و بد</p></div>
<div class="m2"><p>به دادیم معذور و اندر خوریم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو خود بد کنیم از که خواهیم داد؟</p></div>
<div class="m2"><p>مگر خویشتن را به داور بریم!</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چرا پس که ندهیم خود داد خود</p></div>
<div class="m2"><p>ازان پس که خود خصم و خود داوریم؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به دست من و توست نیک اختری</p></div>
<div class="m2"><p>اگر بد نجوئیم نیک اختریم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر دوست داریم نام نکو</p></div>
<div class="m2"><p>چرا پس نه نام نکو گستریم؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همی سرو باید که خوانندمان</p></div>
<div class="m2"><p>اگر چند خمیده چون چنبریم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نخواهیم اگر چند لاغر بویم</p></div>
<div class="m2"><p>که فربه بداند که ما لاغریم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بیا تا به دانش به یک سو شویم</p></div>
<div class="m2"><p>زلشکر وگر چند از این لشکریم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بیائید تا لشکر آز را</p></div>
<div class="m2"><p>به خرسندی از گرد خود بشکریم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برآئیم بر پایهٔ مردمی</p></div>
<div class="m2"><p>مر این ناکسان را به کس نشمریم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به دشمن نمائیم روشن که ما</p></div>
<div class="m2"><p>به دنیا و دین بر سر دفتریم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ازیرا سر دفتریم، ای پسر،</p></div>
<div class="m2"><p>که ما شیعت اهل پیغمبریم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به ریگ هبیر اندرون تشنه‌اند</p></div>
<div class="m2"><p>همه خلق و ما برلب کوثریم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو، ای ناصبی، گر زحد بگذری</p></div>
<div class="m2"><p>به بیهوده گفتار، ما نگذریم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پیمبر سر دین حق است و ما</p></div>
<div class="m2"><p>از این نامور تن مطیع سریم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر تو مر این قول را منکری</p></div>
<div class="m2"><p>چنان دان که ما مر تو را منکریم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر تو بر این تن سری آوری</p></div>
<div class="m2"><p>دگر سر بیاور که ما ناوریم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز پیغمبر ما وصی حیدر است</p></div>
<div class="m2"><p>چنین زین قبل شیعت حیدریم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز فرزند او خلق را رهبری است</p></div>
<div class="m2"><p>که ما بر پی و راه آن رهبریم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سر و افسر دین حق است و ما</p></div>
<div class="m2"><p>چنین فخر امت بدان افسریم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگر تو به آل نبی کافری</p></div>
<div class="m2"><p>به طاغوت تو نیز ما کافریم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ملامت مکن‌مان اگر ما چو تو</p></div>
<div class="m2"><p>بخیره ره جاهلی نسپریم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سپاس است بر ما خداوند را</p></div>
<div class="m2"><p>که نه چون تو نادان و بد محضریم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به غوغای نادان چه غره شوی؟</p></div>
<div class="m2"><p>چه لافی که «ما بر سر منبریم»؟</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز یاجوج و ماجوج مان باک نیست</p></div>
<div class="m2"><p>که ما بر سر سد اسکندریم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اگر سگ به محرابی اندر شود</p></div>
<div class="m2"><p>مر آن را بزرگی سگ نشمریم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چه باک است اگر نیست مان فرش و قصر</p></div>
<div class="m2"><p>چو در دین توانگرتر از قیصریم؟</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>عزیزیم بر چشم دانا چو زر</p></div>
<div class="m2"><p>به چشم تو در خاک و خاکستریم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>علی‌مان اساس است و جعفر امام</p></div>
<div class="m2"><p>نه چون تو ز دشت علی جعفریم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از اهل خراسان چه گویندمان</p></div>
<div class="m2"><p>که گویند «ما کاتب و شاعریم»؟</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اگر راست گویند گویند «ما</p></div>
<div class="m2"><p>همه راوی و ناسخ ناصریم»</p></div></div>