---
title: >-
    قصیدهٔ شمارهٔ ۶۱
---
# قصیدهٔ شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>چند گوئی که؟ چو ایام بهار آید</p></div>
<div class="m2"><p>گل بیاراید و بادام به بار آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی بستان را چون چهرهٔ دلبندان</p></div>
<div class="m2"><p>از شکوفه رخ و از سبزه عذار آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی گلنار چو بزداید قطر شب</p></div>
<div class="m2"><p>بلبل از گل به سلام گلنار آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاروار است کنون بلبل و تا یک چند</p></div>
<div class="m2"><p>زاغ زار آید، او زی گلزار آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل سوار آید بر مرکب و، یاقوتین</p></div>
<div class="m2"><p>لاله در پیشش چون غاشیه‌دار آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باغ را از دی کافور نثار آمد</p></div>
<div class="m2"><p>چون بهار آید لولوش نثار آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل تبار و آل دارد همه مه‌رویان</p></div>
<div class="m2"><p>هر گهی کاید با آل و تبار آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بید با باد به صلح آید در بستان</p></div>
<div class="m2"><p>لاله با نرگس در بوس و کنار آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باغ مانندهٔ گردون شود ایدون که‌ش</p></div>
<div class="m2"><p>زهره از چرخ سحرگه به نظار آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این چنین بیهده‌ای نیز مگو با من</p></div>
<div class="m2"><p>که مرا از سخن بیهده عار آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شست بار آمد نوروز مرا مهمان</p></div>
<div class="m2"><p>جز همان نیست اگر ششصد بار آید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که را شست ستمگر فلک آرایش</p></div>
<div class="m2"><p>باغ آراسته او را به چه کار آید؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سوی من خواب و خیال است جمال او</p></div>
<div class="m2"><p>گر به چشم توهمی نقش و نگار آید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نعمت و شدت او از پس یکدیگر</p></div>
<div class="m2"><p>حنظلش با شکر، با گل خار آید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روز رخشنده کزو شاد شود مردم</p></div>
<div class="m2"><p>از پس انده و رنج شب تار آید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا نراند دی دیوانه‌ت خوی بد</p></div>
<div class="m2"><p>نه بهار آید و نه دشت به بار آید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فلک گردان شیری است رباینده</p></div>
<div class="m2"><p>که همی هر شب زی ما به شکار آید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر که پیش آیدش از خلق بیوبارد</p></div>
<div class="m2"><p>گر صغار آید و یا نیز کبار آید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نشود مانده و نه سیر شود هرگز</p></div>
<div class="m2"><p>گر شکاریش یکی یا دو هزار آید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر عزیز است جهان و خوش زی نادان</p></div>
<div class="m2"><p>سوی من، باری، می ناخوش و خوار آید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر کسی را ز جهان بهرهٔ او پیداست</p></div>
<div class="m2"><p>گر چه هر چیزی زین طبع چهار آید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>می بکار آید هر چیز به جای خویش</p></div>
<div class="m2"><p>تری از آب و شخودن ز شخار آید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نرم و تر گردد و خوش خوار و گوارنده</p></div>
<div class="m2"><p>خار بی‌طعم چو در کام حمار آید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سازگاری کن با دهر جفاپیشه</p></div>
<div class="m2"><p>که بدو نیک زمانه به قطار آید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کر بدآمدت گهی، اکنون نیک آید</p></div>
<div class="m2"><p>کز یکی چوب همی منبر و دار آید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گه نیازت به حصار آید و بندو دز</p></div>
<div class="m2"><p>گاه عیبت ز دزو بند و حصار آید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گه سپاه آرد بر تو فلک داهی</p></div>
<div class="m2"><p>گه تو را مشفق و یاری ده و یار آید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نبود هرگز عیبی چو هنر، هرچند</p></div>
<div class="m2"><p>هنر زید سوی عمر و عوار آید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مر مرا گوئی برخیز که بد دینی</p></div>
<div class="m2"><p>صبر کن اکنون تا روز شمار آید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گیسوی من به سوی من ندو ریحان است</p></div>
<div class="m2"><p>گر به چشم تو همی تافته مار آید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شاخ پربارم زی چشم بنی زهرا</p></div>
<div class="m2"><p>پیش چشم تو همی بید و چنار آید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ور همی گوئی من نیز مسلمانم</p></div>
<div class="m2"><p>مر تو را با من در دین چه فخار آید؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>من تولا به علی دارم کز تیغش</p></div>
<div class="m2"><p>بر منافق شب و بر شیعه نهار آید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فضل بر دود ندانی که بسی دارد</p></div>
<div class="m2"><p>نور اگر چند همی هردو ز نار آید؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون برادر نبود هرگز همسایه</p></div>
<div class="m2"><p>گرچه با مرد به کهسار و به غار آید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سنگ چون زر نباشد به بها هرچند</p></div>
<div class="m2"><p>سنگ با زر همی زیر عیار آید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دین سرائی است برآوردهٔ پیغمبر</p></div>
<div class="m2"><p>تا همه خلق بدو در به قرار آید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به سرا اندر دانی که خداوندش</p></div>
<div class="m2"><p>نه چنان آید چون غله گزار آید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>علی و عترت اوی است مر آن را در</p></div>
<div class="m2"><p>خنک آن کس که در این ساخته‌دار آید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خنک آن را که به علم و به عمل هر شب</p></div>
<div class="m2"><p>به سرا اندر با فرش و ازار آید</p></div></div>