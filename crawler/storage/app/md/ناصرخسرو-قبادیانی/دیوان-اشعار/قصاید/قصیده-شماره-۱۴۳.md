---
title: >-
    قصیدهٔ شمارهٔ ۱۴۳
---
# قصیدهٔ شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>مانده به یمگان به میان جبال</p></div>
<div class="m2"><p>نیستم از عجز و نه نیز از کلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکسره عشاق مقال منند</p></div>
<div class="m2"><p>در گه و بیگه به خراسان رجال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وز سخن ونامهٔ من گشت خوار</p></div>
<div class="m2"><p>نامهٔ مانی و نگارش نکال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نام سخن‌های من از نثر و نظم</p></div>
<div class="m2"><p>چیست سوی دانا؟ سحر حلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر شنوندی همی اشعار من</p></div>
<div class="m2"><p>گنگ شدی رؤبه و عجاج لال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور به زمین آمدی از چرخ تیر</p></div>
<div class="m2"><p>برقلم من شده بودی عیال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور به گمان است دل تو درین</p></div>
<div class="m2"><p>چاشنیم گیر چه باید جدال؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز سخن من ز دل عاقلان</p></div>
<div class="m2"><p>مشکل و مبهم را نارد زوال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیره نکرده‌است دلم را چنین</p></div>
<div class="m2"><p>نه غم هجران و نه شوق وصال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق محال است نباشد هگرز</p></div>
<div class="m2"><p>خاطر پرنور محل محال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نظم نگیرد به دلم در غزل</p></div>
<div class="m2"><p>راه نگیرد به دلم بر غزال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از چو منی صید نیابد هوا</p></div>
<div class="m2"><p>زشت بود شیر شکار شگال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیست هوا را به دلم در مقر</p></div>
<div class="m2"><p>نیست مرا نیز به گردش مجال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل به مثل نال و هوا آتش است</p></div>
<div class="m2"><p>دور به از آتش سوزنده، نال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیست بدین کنج درون نیز گنج</p></div>
<div class="m2"><p>نامدم اینجای ز بهر منال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مال نجسته‌است به یمگان کسی</p></div>
<div class="m2"><p>زانکه نبوده است خود اینجای مال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیز در این کنج مرا کس نبود</p></div>
<div class="m2"><p>خویش و نه همسایه و نه عم و خال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بل چو هزیمت شدم از پیش دیو</p></div>
<div class="m2"><p>گفت مرا بختم از اینجا «تعال»</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با دل رنجور در این تنگ جای</p></div>
<div class="m2"><p>مونس من حب رسول است و آل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چشم همی دارم تا در جهان</p></div>
<div class="m2"><p>نو چه پدید آید از این دهر زال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر تو نی آگاهی از این گند پیر</p></div>
<div class="m2"><p>منت خبر گویم از این بد فعال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سیرت او نیست مگر جادوی</p></div>
<div class="m2"><p>عادت او نیست مگر کاحتیال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تاج نهد بر سرت، آنگاه باز</p></div>
<div class="m2"><p>خرد بکوبدت به زیر نعال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بی‌هنرت گر بگزیند چو زر</p></div>
<div class="m2"><p>بی‌گنهت خوار کند چون سفال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر نه همی با ما بازی کند</p></div>
<div class="m2"><p>چند برون آردمان چون خیال؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زید شده تشنه به ریگ هبیر</p></div>
<div class="m2"><p>عمرو شده غرقه در آب زلال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رنجه زگرمای تموز آن و، این</p></div>
<div class="m2"><p>خفته و آسوده به زیر ظلال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ازچه کند دهر جز از سنگ سخت</p></div>
<div class="m2"><p>ایدون این نرم و رونده رمال؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وز چه پدید آورد این زال را؟</p></div>
<div class="m2"><p>جز که ازین دخترکی با جمال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دیر نپاید به یکی حال بر</p></div>
<div class="m2"><p>این فلک جاهل بی‌خواب و هال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زود بگرداند اقبال و سعد</p></div>
<div class="m2"><p>زان ملک مقبل مسعود فال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مهتر و کهتر همه با او به خشم</p></div>
<div class="m2"><p>عالم و جاهل همه زو نال نال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نیست کسی جز من خشنود ازو</p></div>
<div class="m2"><p>نیک نگه کن به یمین و شمال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کیست جز از من که نشد پیش او</p></div>
<div class="m2"><p>روی سیه کرده به ذل سال؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>راست که از عادتش آگه شدم</p></div>
<div class="m2"><p>زان پس بر منش نرفت افتعال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ای رهی و بندهٔ آز و نیاز</p></div>
<div class="m2"><p>بوده به نادانی هفتاد سال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یک ره از این بندگی آزاد شو</p></div>
<div class="m2"><p>ای خر بدبخت، برآی از جوال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گرت نباید که شوی زار و خوار</p></div>
<div class="m2"><p>گوش طمع سخت بگیر و بمال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دست طمع کرده میان تو را</p></div>
<div class="m2"><p>پیش شه و میر دو تا چون دوال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سیل طمع برد تو را آب‌روی</p></div>
<div class="m2"><p>پای طمع کوفت تو را فرق و یال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ذل بود بار نهال طمع</p></div>
<div class="m2"><p>نیک بپرهیز از این بد نهال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کم خور و مفروش به نان آب‌روی</p></div>
<div class="m2"><p>سنگ خور از ننگ و سفال سکال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زشت بود بودن آزاده را</p></div>
<div class="m2"><p>بندهٔ طوغان و عیال ینال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شرم نداری همی از نام زشت</p></div>
<div class="m2"><p>بر طمع آنکه شوی خوب حال؟</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>من نشوم گر بشود جان من</p></div>
<div class="m2"><p>پیش کسی که‌ش نپسندم همال</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بلخ تو را دادم و یمگان ستد</p></div>
<div class="m2"><p>وین درهٔ تنگ و جبال و تلال</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون ز تو من باز گسستم ز من</p></div>
<div class="m2"><p>بگسل و کوتاه کن این قیل و قال</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دست من و دامن آل رسول</p></div>
<div class="m2"><p>وز دگران پاک بریدم حبال</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از پس آن کس که تو خواهی برو</p></div>
<div class="m2"><p>نیست مرا با تو جدال و مقال</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>فصل کند داوری ما به حشر</p></div>
<div class="m2"><p>آنکه جز او نیست دگر ذوالجلال</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فردا معلوم تو گردد که کیست</p></div>
<div class="m2"><p>پیش خدا از تو و من بر ضلال</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بد چه سگالی که فرومایگی است</p></div>
<div class="m2"><p>خیره بر این حجت نیکو سگال</p></div></div>