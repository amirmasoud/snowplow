---
title: >-
    قصیدهٔ شمارهٔ ۱۷۶
---
# قصیدهٔ شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>ای شده مشغول به کار جهان</p></div>
<div class="m2"><p>غره چرائی به جهان جهان؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیگ جهانی تو بیندیش نیک</p></div>
<div class="m2"><p>سخره گرفته است تو را این جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پس خویشت بدواند همی</p></div>
<div class="m2"><p>گه سوی نوروز و گهی زی خزان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو نه دیوی به همه عمر خویش</p></div>
<div class="m2"><p>از پس این دیو چرائی دوان؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش تو در می‌رود او کینه‌ور</p></div>
<div class="m2"><p>تو زپس او چه دوی شادمان؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ نترسی که تو را این نهنگ</p></div>
<div class="m2"><p>ناگه یک روز کشد در دهان؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرت به مغز اندر هوش است و رای</p></div>
<div class="m2"><p>روی بگردان ز دروغ زمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آزت هر روز به فردا دهد</p></div>
<div class="m2"><p>وعدهٔ چیزی که نباشد چنان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیر شدت بر غم و سختی و رنج</p></div>
<div class="m2"><p>بر طمع راحت شخص جوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر تو به امید بهی، روز روز</p></div>
<div class="m2"><p>چرخ و زمان می‌شمرد سالیان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دشمن توست ای پسر این روزگار</p></div>
<div class="m2"><p>نیست به تو در طمعش جز به جان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کژدم دارد بسی از بهر تو</p></div>
<div class="m2"><p>کرده نهان زیر خز و پرنیان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای شده غره به جهان، زینهار</p></div>
<div class="m2"><p>کایمن بنشینی از این بدنشان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو به در او شده زنهار خواه</p></div>
<div class="m2"><p>دشنه همی مالدت او بر فسان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون تو بسی خورده است این اژدها</p></div>
<div class="m2"><p>هان به حذرباش ز دندانش، هان!</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نامهٔ شاهان عجم پیش خواه</p></div>
<div class="m2"><p>یک ره و بر خود به تامل بخوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کوت فریدون و کجا کیقباد؟</p></div>
<div class="m2"><p>کوت خجسته علم کاویان؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سام نریمان کو و رستم کجاست</p></div>
<div class="m2"><p>پیشرو لشکر مازندران؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بابک ساسان کو و کو اردشیر؟</p></div>
<div class="m2"><p>کوست؟ نه بهرام نه نوشیروان!</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>این همه با خیل و حشم رفته‌اند</p></div>
<div class="m2"><p>نه رمه مانده است کنون نه شبان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رهگذر است این نه سرای قرار</p></div>
<div class="m2"><p>دل منه اینجا و مرنجان روان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ایزد زی خویش همی خواندت</p></div>
<div class="m2"><p>ای شده فتنه به زمین و زمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چند چپ و راست بتابی ز راه</p></div>
<div class="m2"><p>چون نروی راست در این کاروان؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چند ربودی و ربائی هنوز</p></div>
<div class="m2"><p>توشه در این ره ز فلان و فلان؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باک نداری که در این ره به زرق</p></div>
<div class="m2"><p>که بفروشی بدل زعفران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فردا زین خواب چه آگه شوی</p></div>
<div class="m2"><p>سود نداردت خروش و فغان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چونکه نیندیشی از آن روز جمع</p></div>
<div class="m2"><p>کانجا باشند کهان و مهان؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آنجا آن روز نگیردت دست</p></div>
<div class="m2"><p>نه پسر و نه پدر مهربان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زیر گناهان گران و وبال</p></div>
<div class="m2"><p>سست شدت گردن و پشت و میان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خیره چه گوئی تو که «بادی است این</p></div>
<div class="m2"><p>در شکم و پشت و میانم روان؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نیست مرا وقت ضعیفی هنوز</p></div>
<div class="m2"><p>بشکند این را شکر و بادیان»</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روی نخواهی که به قبله کنی</p></div>
<div class="m2"><p>تات نخوابند چو تخته ستان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جز به گه بازپسین دم زدن</p></div>
<div class="m2"><p>از تو نجبند به شهادت زبان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چونکه به پرهیز و به توبه، سبک</p></div>
<div class="m2"><p>نفگنی از گردن بار گران؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا تو یکی خانهٔ نو ساختی</p></div>
<div class="m2"><p>یکسره همسایه‌ت بی‌خان و مان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در سپه جهل بسی تاختی</p></div>
<div class="m2"><p>اکنون یک چند گران کن عنان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دیو قرین تو چرا گشت اگر</p></div>
<div class="m2"><p>دل به گمان نیست تو را در قران</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر به گمانی ز قران کریم</p></div>
<div class="m2"><p>خود ببری کیفر از این بدگمان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سود نداردت پشیمان شدن</p></div>
<div class="m2"><p>خود شود آن روز گمانت عیان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جان تو از بهر عبادت شده است</p></div>
<div class="m2"><p>بسته در این خانه پر استخوان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کان تو است ای تن و طاعت گهر</p></div>
<div class="m2"><p>گوهر بیرون کن از این تیره کان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جانت سوار است و تنت اسپ او</p></div>
<div class="m2"><p>جز به سوی خیر و صلاحش مران</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خود سپس آرزوی تن مرو</p></div>
<div class="m2"><p>چون خره بد سپس ماکیان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گیتی دریا و تنت کشتی است</p></div>
<div class="m2"><p>عمر تو باد است و تو بازارگان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>این همه مایه است که گفتم تو را</p></div>
<div class="m2"><p>مایه به باد از چه دهی رایگان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ای پسر خسرو حکمت بگو</p></div>
<div class="m2"><p>تات بود طاقت و توش و توان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ای به خراسان در سیمرغ‌وار</p></div>
<div class="m2"><p>نام تو پیدا و تن تو نهان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>در سپه علم حقیقت تو را</p></div>
<div class="m2"><p>تیر کلام است و زبانت کمان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>روز و شب از بحر سخن همچنین</p></div>
<div class="m2"><p>در همی جوی و همی برفشان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تا ز تو میراث بماند سخن</p></div>
<div class="m2"><p>چون بروی زی سفر جاودان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خیز به فرمان امام جهان</p></div>
<div class="m2"><p>برکش در بحر سخن بادبان</p></div></div>