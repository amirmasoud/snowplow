---
title: >-
    قصیدهٔ شمارهٔ ۱۲۲
---
# قصیدهٔ شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>پشتم قوی به فضل خدای است و طاعتش</p></div>
<div class="m2"><p>تا دررسم مگر به رسول و شفاعتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش خدای نیست شفیعم مگر رسول</p></div>
<div class="m2"><p>دارم شفیع پیش رسول آل و عترتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با آل او روم سوی او هیچ باک نیست</p></div>
<div class="m2"><p>برگیرم از منافق ناکس شناعتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دین خدای ملک رسول است و، خلق پاک</p></div>
<div class="m2"><p>امروز امتان رسولند و رعیتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر سوی آل مرد شود مال او چرا</p></div>
<div class="m2"><p>زی آل او نشد ز پیمبر شریعتش؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر بندهٔ تو طاعت تو نیست نیم از انک</p></div>
<div class="m2"><p>پیغمبر تو راست ز طاعت بر امتش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتت که بنده را تو به بی‌طاعتی مکش</p></div>
<div class="m2"><p>وانگه نکشتت ار تو نبودی به طاعتش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر حمایتی تو ز پیغمبر خدای</p></div>
<div class="m2"><p>مشکن حمایتش که بزرگ است حمایتش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیغمبر است پیش رو خلق یکسره</p></div>
<div class="m2"><p>کز قاف تا به قاف رسیده است دعوتش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آل پیمبر است تو را پیش رو کنون</p></div>
<div class="m2"><p>از آل او متاب و نگه‌دار حرمتش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرزند اوست حرمت او چون ندانیش</p></div>
<div class="m2"><p>پس خیره خیر امید چه داری به رحمتش؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آگه نه‌ای مگر که پیمبر کرا سپرد</p></div>
<div class="m2"><p>روز غدیر خم ز منبر ولایتش؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن را سپرد کایزد مر دین و خلق را</p></div>
<div class="m2"><p>اندر کتاب خویش بدو کرد اشارتش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن را که چون چراغ بدی پیش آفتاب</p></div>
<div class="m2"><p>از کافران شجاعت پیش شجاعتش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن را که همچو سنگ سر مره روز بدر</p></div>
<div class="m2"><p>در حرب همچو موم شد از بیم ضربتش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن را که در رکوع غنی کرد بی سؤال</p></div>
<div class="m2"><p>درویش را به پیش پیمبر سخاوتش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن را که چون دو نام نهادش رسول حق</p></div>
<div class="m2"><p>امروز نیز دوست سوی خلق کنیتش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن را که هر شریفی نسبت بدو کنند</p></div>
<div class="m2"><p>زیرا که از رسول خدای است نسبتش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن را که کس به جای پیمبر جز او نخفت</p></div>
<div class="m2"><p>با دشمنان صعب به هنگام هجرتش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن را که مصطفی، چو همه عاجز آمدند،</p></div>
<div class="m2"><p>در حرب روز بدر بدو داد رایتش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شیری، مبارزی، که سرشته است کردگار</p></div>
<div class="m2"><p>اندر دل مبارز مردان محبتش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در حربگه پیمبر ما معجزی نداشت</p></div>
<div class="m2"><p>از معجزات نیز قوی‌تر ز قوتش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قسمت نشد به خلق درون دوزخ و بهشت</p></div>
<div class="m2"><p>بر کافر و مسلمان الا به قسمتش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در بود مر مدینهٔ علم رسول را</p></div>
<div class="m2"><p>زیرا جز او نبود سزای امانتش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر علم بایدت به در شهر علم شو</p></div>
<div class="m2"><p>تا بر دلت بتابد نور سعادتش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>او آیت پیمبر ما بود روز حرب</p></div>
<div class="m2"><p>از ذوالفقار بود و ز صمصام آیتش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گنج خدای بود رسول و، ز خلق او</p></div>
<div class="m2"><p>گنج رسول خاطر او بود و فکرتش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر کو عدوی گنج رسول است بی گمان</p></div>
<div class="m2"><p>جز جهل و نحس نیست نشان سلامتش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شیر خدای را چو مخالف شود کسی</p></div>
<div class="m2"><p>هرگز مکن مگر به خری هیچ تهمتش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شیر خدای بود علی، ناصبی خر است</p></div>
<div class="m2"><p>زیرا همیشه می‌برمد خر ز هیبتش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هرک آفت خلاف علی بود در دلش</p></div>
<div class="m2"><p>تو روی ازو بتاب و بپرهیز از آفتش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>لیکن چو حرمت تو بدارد تو از گزاف</p></div>
<div class="m2"><p>مشکن، ز بهر حرمت اسلام، حرمتش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اندر مناظره سخن سرد ازو مگیر</p></div>
<div class="m2"><p>زیرا که نیست جز سخن سرد آلتش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون علم نیستش که بگوید، جز این محال</p></div>
<div class="m2"><p>چون بند سخت گشت چه چیز است حیلتش؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دشنام دارد او همه حجت کنون ولیک</p></div>
<div class="m2"><p>روزشمار که شنود این سست حجتش؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دعوی همی کند که من اهل جماعتم</p></div>
<div class="m2"><p>لیکن ز جمع دیو گشن شد جماعتش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ابلیس قادر است ولیکن به خلق در</p></div>
<div class="m2"><p>جز بر دروغ و حیله‌گری نیست قدرتش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>قیمت سوی خدای به دین است و خلق را</p></div>
<div class="m2"><p>آن است قیمتی که به دین است قیمتش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نصرت به دین کن ای بخرد مر خدای را</p></div>
<div class="m2"><p>گر بایدت که بهره بیابی ز نصرتش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>غره مشو به دولت و اقبال روزگار</p></div>
<div class="m2"><p>زیرا که با زوال همال است دولتش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دنیا به سوی من به مثل بی‌وفا زنی است</p></div>
<div class="m2"><p>نه شاد باش ازو نه غمی شو ز فرقتش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نیک است ازان که نیک و بدش بر گذشتنی است</p></div>
<div class="m2"><p>چیزی دگر همی نشناسم فضیلتش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زهر است نعمتش چو نیابد همی رها</p></div>
<div class="m2"><p>از مرگ هر کسی که چشیده است نعمتش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>با محنتش به نعمتش اندر مکن طمع</p></div>
<div class="m2"><p>زیرا ز نعمتش نشود دور محنتش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شاید که همتم نبود صحبت جهان</p></div>
<div class="m2"><p>چون نیست جز که مالش من هیچ همتش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بسیار داد خلعتم اول وزان سپس</p></div>
<div class="m2"><p>از من یگان یگان همه بربود خلعتش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از روزگار و خلق ملولم کنون ازانک</p></div>
<div class="m2"><p>پشتم به کردگار و رسول است و ملتش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بی‌حاجتم به فضل خداوند، لاجرم،</p></div>
<div class="m2"><p>اندر جهان ز هر که به من نیست حاجتش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تا در دلم قران مبارک قرار یافت</p></div>
<div class="m2"><p>پر برکتست و خیر دل از خیر و برکتش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>منت خدای را که نکرده‌است منتی</p></div>
<div class="m2"><p>پشتم به زیر بار مگر فضل و منتش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>منت خدای را که به وجود امام حق</p></div>
<div class="m2"><p>بشناختم به حق و یقین و حقیقتش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آن بی قرین ملک که جز او نیست در جهان</p></div>
<div class="m2"><p>کز ملک دیو یکسره خالی است ملکتش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>با طلعت مبارک مسعود او ز سعد</p></div>
<div class="m2"><p>خالی است مشتری را در قوس طلعتش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>یارب، به فضل خویش تو توفیق ده مرا</p></div>
<div class="m2"><p>تا روز و شب بدارم طاعت به طاعتش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>و اندر رضای او گه و بیگه به شعر زهد</p></div>
<div class="m2"><p>مر خلق را به رشته کنم علم و حکمتش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مستنصری معانی و حکمت به نظم و نثر</p></div>
<div class="m2"><p>بر امتت که خواند الا که حجتش؟</p></div></div>