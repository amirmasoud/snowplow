---
title: >-
    قصیدهٔ شمارهٔ ۲۸۱
---
# قصیدهٔ شمارهٔ ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>تا کی از آرزوی جاه و خطر</p></div>
<div class="m2"><p>به در شاه و زی امیر شوی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دشمن من شدی بدانکه چو من</p></div>
<div class="m2"><p>حاضر آیم تو می حسیر شوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهد آموختن بباید کرد</p></div>
<div class="m2"><p>گرت باید که بی‌نظیر شوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که نمیرند جمله باخطران</p></div>
<div class="m2"><p>تا تو، ای بی‌خطر، خطیر شوی</p></div></div>