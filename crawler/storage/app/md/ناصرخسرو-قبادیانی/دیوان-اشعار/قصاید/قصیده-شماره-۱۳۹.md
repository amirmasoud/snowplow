---
title: >-
    قصیدهٔ شمارهٔ ۱۳۹
---
# قصیدهٔ شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>این باز سیه پیسه نگر بی‌پر و چنگال</p></div>
<div class="m2"><p>کو هیچ نه آرام همی یابد و نه هال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی آنکه ببینش تو خوش خوش برباید</p></div>
<div class="m2"><p>گاهی زن و فرزند گهی جان و گهی مال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بر تو همی تیز کند چنگ پس او را</p></div>
<div class="m2"><p>جوینده چرائی تو به دندان و به چنگال؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پر تو و بال تو جوانی و جمال است</p></div>
<div class="m2"><p>وین باز نخواهد به جز این پر و جز این بال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه منظر و قد صنمی را شکند پست</p></div>
<div class="m2"><p>گه منظر و کاخ ملکی را کند اطلال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>احوال دگر گردد ازو بر من و بر تو</p></div>
<div class="m2"><p>هموار و، نخواهد شدن او را دگر احوال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرهیز که زو پیری غل است و مر او را</p></div>
<div class="m2"><p>نه گردن و دست است و نه قید است و نه اغلال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مانندهٔ ماری است که نیمیش سپید است</p></div>
<div class="m2"><p>از سوی سرو، زشت و سیاه است به دنبال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با مردم هشیار فصیح است اگر چند</p></div>
<div class="m2"><p>گنگ است سوی بی‌خرد و بی‌سخن و لال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روز و مه و سالش نکند پست ازیراک</p></div>
<div class="m2"><p>پاینده بدو پست شده روز و مه و سال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای خواجه، از این باز وزین مار حذر کن</p></div>
<div class="m2"><p>زیرا الف پشت تو زینهاست شده دال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنگر که بدل کرد به امروز تو را دی</p></div>
<div class="m2"><p>مر پار تو را باز همو کرد به امسال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دیدی که نه عم بودی و نه خال کسی را</p></div>
<div class="m2"><p>او کرد تو را عم و همو کرد تو را خال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنگر که کجا خواهدت این باز همی برد</p></div>
<div class="m2"><p>دیوانه مباش آب مپیمای به غربال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مالیده شدی در طلب مال چو تسمه</p></div>
<div class="m2"><p>تا کی زنی اندر طلب مال کنون فال؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اکنون که نیامدت به کف مال و شدت عمر</p></div>
<div class="m2"><p>ای بی‌خرد این دست بر آن دست همی مال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زینجای چو چیپال تهی‌دست برون رفت</p></div>
<div class="m2"><p>محمود که چندان بستد مال ز چیپال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن جاه و جلالت که به مالت بود امروز</p></div>
<div class="m2"><p>آن سوی خردمند نه جاه است و نه اجلال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جاهی و جمالی که به صندوق درون است</p></div>
<div class="m2"><p>جاهی و جمالی است گران سنگ و پرآخال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جاهت به خرد باید و اجلال به دانش</p></div>
<div class="m2"><p>تا هیچ نبایدت نه صندوق و نه حمال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون تنت نکو حال شد از مال ازان پس</p></div>
<div class="m2"><p>جان را به خرد باید کردنت نکو حال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دانا به سخنهای خوش و خوب شود شاد</p></div>
<div class="m2"><p>نادان به سرود و غزل و مطرب و قوال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن را که بیهوده سخن شاد شود جانش</p></div>
<div class="m2"><p>بفروش به یک دسته خس تره به بقال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وان مرد که او کتب فتاوی و حیل ساخت</p></div>
<div class="m2"><p>بر صورت ابدال بد و سیرت دجال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حیلت نه ز دین است، اگر بر ره دینی</p></div>
<div class="m2"><p>حیلت مسگال ایچ و حذر دار ز محتال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر دام نبودیش چنین حیلت و رخصت</p></div>
<div class="m2"><p>این خلق نپذرفتی ازو «حدثنا قال»</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>امثال قران گنج خدای است، چه گوئی</p></div>
<div class="m2"><p>از «حدثنا قال» گشاده شود امثال؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر علم مثل معتمدان آل رسولند</p></div>
<div class="m2"><p>راهت ننماید سوی آن علم جز این آل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قفل است مثل، گر تو بپرسی ز کلیدش</p></div>
<div class="m2"><p>پر علت جهل است تو را اکحل و قیفال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پر توست مثلهای قران، تا نگزاریش</p></div>
<div class="m2"><p>آسان نشود بر تو نه امثال و نه اهوال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گوئی قتبی مشکل قرآن بگشاده است</p></div>
<div class="m2"><p>تکیه زده‌ای خیره بر آن خشک شده نال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کس بند خدائی به سگالش نگشاید</p></div>
<div class="m2"><p>با بند خدائی ره بیهوده بمسگال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دادمت نشان سوی طبیبی که‌ت از این درد</p></div>
<div class="m2"><p>تدبیر وی آرد به سوی بهتری اقبال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر جان تو پر کینهٔ آن شهره طبیب است</p></div>
<div class="m2"><p>شو درد و بلا می کش و همواره همی نال</p></div></div>