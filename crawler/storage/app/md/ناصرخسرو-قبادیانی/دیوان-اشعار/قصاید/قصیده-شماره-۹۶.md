---
title: >-
    قصیدهٔ شمارهٔ ۹۶
---
# قصیدهٔ شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>ای گشته جهان و خوانده دفتر</p></div>
<div class="m2"><p>بندیش ز کار خویش بهتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چرخ بلند را همی بین</p></div>
<div class="m2"><p>پر خاک و هوا و آب و آذر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک گوهر تر و نام او بحر</p></div>
<div class="m2"><p>یک گوهر خشک و نام او بر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وین ابر به جهد خشک‌ها را</p></div>
<div class="m2"><p>زان جوهر تر همی کند تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیچاره نبات را نیبنی</p></div>
<div class="m2"><p>همواره جوان از این دو گوهر؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وین جانوران روان گرفته</p></div>
<div class="m2"><p>بیچاره نبات را مسخر؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برطبع و نبات و جانور پاک</p></div>
<div class="m2"><p>ای پیر تو را که کرد مهتر؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین پیش چه نیکی آمد از تو</p></div>
<div class="m2"><p>وز گاو گنه چه بود و از خر؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو بی‌هنری چرا عزیزی؟</p></div>
<div class="m2"><p>او بی‌گنهی چراست مضطر؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دانی که چنین نه عدل باشد</p></div>
<div class="m2"><p>پس چون مقری به عدل داور؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وان کس که چنین عزیز کردت</p></div>
<div class="m2"><p>از بهر تو کرد گوهر و زر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زیرا که نکرد هیچ حیوان</p></div>
<div class="m2"><p>از گوهر و زر تاج و افسر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر گور و گوزن اگر امیر است</p></div>
<div class="m2"><p>از قوت خویش و دل غضنفر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون نیست خرد میان ایشان</p></div>
<div class="m2"><p>درویش نه این، نه آن توانگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این میر و عزیز نیست برگاه</p></div>
<div class="m2"><p>وان خوار و ذلیل نیست بر در</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شادی و توانگری خرد راست</p></div>
<div class="m2"><p>هر دو عرضند و عقل جوهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاخی است خرد سخن برو برگ</p></div>
<div class="m2"><p>تخمی است خرد سخن ازو بر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زیر سخن است عقل پنهان</p></div>
<div class="m2"><p>عقل است عروس و قول چادر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دانای سخن نکو کند باز</p></div>
<div class="m2"><p>از روی عروس عقل معجر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو روی عروس خویش بنمای</p></div>
<div class="m2"><p>ای گشته جهان و خوانده دفتر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فتنه چه شدی چنین بر این خاک؟</p></div>
<div class="m2"><p>یک ره برکن سوی فلک سر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از گوهر و از نبات و حیوان</p></div>
<div class="m2"><p>برخاک ببین سه‌خط مسطر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هفت است قلم مر این سه خط را</p></div>
<div class="m2"><p>در خط و قلم به عقل بنگر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بندیش نکو که این سه خط را</p></div>
<div class="m2"><p>پیوسته که کرد یک به دیگر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گشتنت ستوروار تا کی</p></div>
<div class="m2"><p>با رود و می و سرود و ساغر؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خرسند شدی به خور ز گیتی</p></div>
<div class="m2"><p>زیرا تو خری جهان چرا خور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بررس ز چرا و چون، چرائی</p></div>
<div class="m2"><p>شادان به چرا چو گاو لاغر؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بندیش که کردگار گیتی</p></div>
<div class="m2"><p>از بهر چه آوریدت ایدر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بنگر به چه محکمی ببسته‌است</p></div>
<div class="m2"><p>مرجان تو را بدین تن اندر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>او راست به‌پای بی‌ستونی</p></div>
<div class="m2"><p>این گنبد گردگرد اخضر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون کار به بند کرد، بی‌شک</p></div>
<div class="m2"><p>پر بند بود سخنش یکسر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون چنبر بی‌سر است فرقان</p></div>
<div class="m2"><p>خیره چه دوی به گرد چنبر؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>با بند مچخ که سخت گردد</p></div>
<div class="m2"><p>چون باز بتابی از رسن سر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گاورسه چو کرد می ندانی</p></div>
<div class="m2"><p>بایدت سپرد زر به زرگر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پیدا چو تن تو است تنزیل</p></div>
<div class="m2"><p>تاویل درو چو جان مستر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گویند که پیش، ازین گهر کوفت</p></div>
<div class="m2"><p>در ظلمت، زیر پی سکندر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>امروز به زیر پای دین است</p></div>
<div class="m2"><p>اندر ظلمات غفلت و شر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هزمان بزند بعاد ما را</p></div>
<div class="m2"><p>از مغرب حق باد صرصر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سوراخ شده است سد یاجوج</p></div>
<div class="m2"><p>یک چند حذر کن ای برادر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بر منبر حق شده است دجال</p></div>
<div class="m2"><p>خامش بنشین تو زیر منبر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اشتر چو هلاک گشت خواهد</p></div>
<div class="m2"><p>آید به سر چه و لب جر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آنک او به مراد عام نادان</p></div>
<div class="m2"><p>بر رفت به منبر پیمبر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گفتا که منم امام و، میراث</p></div>
<div class="m2"><p>بستد ز نبیرگان و دختر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>روی وی اگر سپید باشد</p></div>
<div class="m2"><p>روی که بود سیه به محشر؟</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>صعبی تو و منکری گر این کار</p></div>
<div class="m2"><p>نزدیک تو صعب نیست و منکر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ور می بروی تو با امامی</p></div>
<div class="m2"><p>کاین فعل شده است ازو مشهر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>من با تو نیم که شرم دارم</p></div>
<div class="m2"><p>از فاطمه و شبیر و شبر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جای حذر است از تو ما را</p></div>
<div class="m2"><p>گر تو نکنی حذر ز حیدر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ای گمره و خیره چون گرفتی</p></div>
<div class="m2"><p>گمراه‌ترین دلیل و رهبر؟</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>من با تو سخن نگویم ایراک</p></div>
<div class="m2"><p>کری تو و رهبر از تو کرتر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>من میوهٔ دین همی چرم شو</p></div>
<div class="m2"><p>چون گاو توخار وخس همی چر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شو پنبهٔ جهل بر کن از گوش</p></div>
<div class="m2"><p>بشنو سخنی به طعم شکر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>رخشنده‌تر از سهیل و خورشید</p></div>
<div class="m2"><p>بوینده‌تر از عبیر و عنبر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>آن است به نزد مرد عاقل</p></div>
<div class="m2"><p>مغز سخن خدای اکبر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>او را بردم به سنگ تا زود</p></div>
<div class="m2"><p>پیشت بدمد ز سنگ عبهر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>آنگاه نجوئی آب چاهی</p></div>
<div class="m2"><p>هر گه که چشیدی آب کوثر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>پرخاش مکن سخن بیاموز</p></div>
<div class="m2"><p>از من چه رمی چو خر ز نشتر؟</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>پر خرد است علم تاویل</p></div>
<div class="m2"><p>پرید هگرز مرغ بی‌پر؟</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>از مذهب خصم خویش بررس</p></div>
<div class="m2"><p>تا حق بدانی از مزور</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>حجت نبود تو را که گوئی</p></div>
<div class="m2"><p>من مؤمنم و جهود کافر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گوئی که صنوبرم، ولیکن</p></div>
<div class="m2"><p>زی خصم، تو خاری او صنوبر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هش دار و مدار خوار کس را</p></div>
<div class="m2"><p>مرغان همه را حبیره مشمر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>غره چه شدی به خنجر خویش</p></div>
<div class="m2"><p>مر خصم تو را ده است خنجر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>از بیم شدن ز دست او روم</p></div>
<div class="m2"><p>مانده‌است چنان به روم قیصر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>با خصم مگوی آنچه زی تو</p></div>
<div class="m2"><p>معلوم نباشد و مقرر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>منداز بخیره نازموده</p></div>
<div class="m2"><p>زی باز چو کودکان کبوتر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>پرهیز کن اختیار و حکمت</p></div>
<div class="m2"><p>تا نیک بود به حشرت اختر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>اندر سفری بساز توشه</p></div>
<div class="m2"><p>یاران تو رفته‌اند بی‌مر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بی‌زاد مشو برون و مفلس</p></div>
<div class="m2"><p>زین خیمهٔ بی‌در مدور</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بهتر سخنان و پند حجت</p></div>
<div class="m2"><p>صد بار تو را ز شیر مادر</p></div></div>