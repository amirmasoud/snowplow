---
title: >-
    قصیدهٔ شمارهٔ ۱۱۸
---
# قصیدهٔ شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>ای خداوند این کبود خراس</p></div>
<div class="m2"><p>صد هزاران تو را ز بنده سپاس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که به آل رسول خویش مرا</p></div>
<div class="m2"><p>برهاندی از این رمهٔ نسناس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا متابع بوم رسول تو را</p></div>
<div class="m2"><p>نروم بر مراد خویش و قیاس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم مقصر بوم به روز و به شب</p></div>
<div class="m2"><p>به سپاست بر آورم انفاس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکر و حمد تو را زبان قلم است</p></div>
<div class="m2"><p>بندگان را و روز و شب قرطاس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نامه‌ها پیش تو همی آید</p></div>
<div class="m2"><p>هم ز بیدار دل هم از فرناس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ کاری از این دو نامه برون</p></div>
<div class="m2"><p>نکند کافر و خدای‌شناس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتش دوزخ است ناقد خلق</p></div>
<div class="m2"><p>او شناسد ز سیم پاک نحاس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داد من بی‌گمان بر آیدمی</p></div>
<div class="m2"><p>روز حشر از نبیرهٔ عباس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وز گروهی که با رسول و کتاب</p></div>
<div class="m2"><p>فتنه‌گشتند بریکی به قیاس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این ستوران کرده در گردن</p></div>
<div class="m2"><p>رسن جهل و سلسلهٔ وسواس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من چه کردم اگر بدان جاهل</p></div>
<div class="m2"><p>نفرستاد وحی رب‌الناس؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با نبوت چه کار بود او را</p></div>
<div class="m2"><p>چون برفت از پس رش و کرباس؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لاجرم امتش به برکت او</p></div>
<div class="m2"><p>کوفته‌ستند پای خویش به فاس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دو مخالف بخواند امت را</p></div>
<div class="m2"><p>چو دو صیاد صید را سوی داس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برده‌گشتند یکسر این ضعفا</p></div>
<div class="m2"><p>وان دو صیاد هر یکی نخاس</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به خراسی کشید هر یک‌شان</p></div>
<div class="m2"><p>که سزاوارتر ز خر به خراس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر چه کان گفت «لایجوز چنین»</p></div>
<div class="m2"><p>آن دگر گفت «عندنا لاباس»</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اینت مسکر حرام کرد چو خوگ</p></div>
<div class="m2"><p>وانت گفتا بجوش و پر کن طاس</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دو مخالف امام گشته‌ستند</p></div>
<div class="m2"><p>چون سیاه و سپید و خز و پلاس</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نشد از ما بدین رسن یک تا</p></div>
<div class="m2"><p>هر که بشناخت پای خویش از راس</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لیکن اندر دل خسان آسان</p></div>
<div class="m2"><p>چون به خس مار درخزد خناس</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از ره نام همچو یک دگرند</p></div>
<div class="m2"><p>سوی بی‌عقل هرمس و هرماس</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لیکن از راه عقل هشیاران</p></div>
<div class="m2"><p>بشناسند فربهی ز اماس</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای خردمند هوش دار که خلق</p></div>
<div class="m2"><p>بس به اسداس در زدند اخماس</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سخت بد گشت نقدها مستان</p></div>
<div class="m2"><p>درم از کس مگر به سخت مکاس</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دور باش از مزوری که به مکر</p></div>
<div class="m2"><p>دام قرطاس دارد و انقاس</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تیزتر گشت و جهل را بازار</p></div>
<div class="m2"><p>سوی جهال صد ره از الماس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نیست از نوع مردم آنک امروز</p></div>
<div class="m2"><p>شخص و انواع داند و اجناس</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خرد و جهل کی شوند عدیل؟</p></div>
<div class="m2"><p>بز را نیست آشنا رواس</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>می‌شتابد چو سیل سوی نشیب</p></div>
<div class="m2"><p>خلق سوی نشاط و لهو و لباس</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من همانا که نیستم مردم</p></div>
<div class="m2"><p>چون نیم مرد رود و مجلس و کاس</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا اساس تنم به پای بود</p></div>
<div class="m2"><p>نروم جز که بر طریق اساس</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پاس دارم ز دیو و لشکر او</p></div>
<div class="m2"><p>به سپاس خدای بر تن، پاس</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نبوم ناسپاس ازو که ستور</p></div>
<div class="m2"><p>سوی فرزانه بهتر از نسپاس</p></div></div>