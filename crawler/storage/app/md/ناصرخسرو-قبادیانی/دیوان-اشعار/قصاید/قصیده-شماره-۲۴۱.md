---
title: >-
    قصیدهٔ شمارهٔ ۲۴۱
---
# قصیدهٔ شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>این چه خیمه است این که گوئی پر گهر دریاستی</p></div>
<div class="m2"><p>یا هزاران شمع در پنگان از میناستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باغ اگر بر چرخ بودی لاله بودی مشتری</p></div>
<div class="m2"><p>چرخ اگر در باغ بودی گلبنش جوزاستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از گل سوری ندانستی کسی عیوق را</p></div>
<div class="m2"><p>این اگر رخشنده بودی یا گر آن بویاستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبح را بنگر پس پروین روان گوئی مگر</p></div>
<div class="m2"><p>از پس سیمین تذروی بسدین عنقاستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی مشرق را بیاراید به بوقلمون سحر</p></div>
<div class="m2"><p>تا بدان ماند که گوئی مسند داراستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جرم گردون تیره و روشن درو آیات صبح</p></div>
<div class="m2"><p>گوئی اندر جان نادان خاطر داناستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماه نو چون زورق زرین نگشتی هر مهی</p></div>
<div class="m2"><p>گر نه این گردنده چرخ نیلگون دریاستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست این دریا بل این پردهٔ بهشت خرم است</p></div>
<div class="m2"><p>ور نه این پرده بهشتستی نه پر حوراستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلکه مصنوعی تمام است این به قول منطقی</p></div>
<div class="m2"><p>گر تمام آن است کو را نیست هرگز کاستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آسیائی راست است این کابش از بیرون اوست</p></div>
<div class="m2"><p>زان همی گردد، شنودم این حدیث از راستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آسیابان را ببینی چون ازو بیرون شوی</p></div>
<div class="m2"><p>واندر اینجا دیدیی چشمت اگر بیناستی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چیست، بنگر، زاسیا مر آسیابان را غله؟</p></div>
<div class="m2"><p>گر نبایستیش غله آسیا ناراستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عقل اشارت نفس دانا را همی ایدون کند</p></div>
<div class="m2"><p>کاین همانا ساخته کرده ز بهر ماستی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روزگار و چرخ و انجم سر به سر بازیستی</p></div>
<div class="m2"><p>گرنه این روز دراز دهر را فرداستی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نفس ما بر آسیا کی پادشا گشتی به عقل</p></div>
<div class="m2"><p>گر نه نفس مردمی از کل خویش اجزاستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چرخ می‌گوید به گشتن‌ها که من می‌بگذرم</p></div>
<div class="m2"><p>جز همین چیزی نگفتی گر چو ما گویاستی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قول او را بشنود دانا ز راه گشتنش</p></div>
<div class="m2"><p>گشتنش آواستی گر همچو ماش آواستی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کس نمی‌داند کز این گنبد برون احوال چیست</p></div>
<div class="m2"><p>سر فرو کردی اگر شخصی بر این بالاستی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نیست چیزی دیدنی زینجا برون و زین قبل</p></div>
<div class="m2"><p>می‌گمان آید کز این گنبد برون صحراستی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دهر خود می‌بگذرد یا حال او می‌بگذرد</p></div>
<div class="m2"><p>حال گشتن نیستی گر دهر بی‌مبداستی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر کسی چیزی همی گوید زتیره رای خویش</p></div>
<div class="m2"><p>تا گمان آیدت کو قسطای بن لوقاستی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این همی گوید که گرمان نیستی دو کردگار</p></div>
<div class="m2"><p>نیستی واجب که هرگز خار با خرماستی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نور و خیر و پاک و خوب اندر طبایع کی چنین</p></div>
<div class="m2"><p>ظلمت و شر و پلید و زشت را اعداستی؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وانت گوید گر جهان را صانعی عادل بدی</p></div>
<div class="m2"><p>بر جهان و خلق یکسر داد او پیداستی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ریگ و شورستان و سنگ و دشت و غار و آب‌شور</p></div>
<div class="m2"><p>کشت و میوه‌ستان و راغ و باغ چون دیباستی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این چرا بندهٔ ضعیف و چاکر و ساسیستی</p></div>
<div class="m2"><p>وان چرا شاه و قوی و مهتر و والاستی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ور جهان را یکسره ایزد مسلمان خواستی</p></div>
<div class="m2"><p>جز مسلمان نه جهودستی و نه ترساستی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وانت گوید جمله عدل است این و ما را بندگی است</p></div>
<div class="m2"><p>خواست او را بود و باشد، نیست ما را خواستی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من بگفتی راستی گر از زبان این خسان</p></div>
<div class="m2"><p>عاقلان را گوش کردن قول ما یاراستی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر بشایستی که دینی گستریدی هر خسی</p></div>
<div class="m2"><p>کردگار اندر جهان پیغمبر ننشاستی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر تفاوت نیستی یکسان بدی مردم همه</p></div>
<div class="m2"><p>هر کسی در ذات خود یکتا و بی‌همتاستی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وین چنین اندر خرد واجب نیابد نیز ازانک</p></div>
<div class="m2"><p>هر کسی همتای خلقستی و خود یکتاستی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وانچه کز جستن محال آید نشاید بودن آن</p></div>
<div class="m2"><p>پس نشاید گفتن «ار هستی چنین زیباستی»</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پس محال آورد حال دهر قول آنکه گفت</p></div>
<div class="m2"><p>«بهزیستی گرنه این مولای و آن مولاستی»</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وانکه گوید «خواست ما را نیست» می‌گوید خرد</p></div>
<div class="m2"><p>کاین همانا قول مردی مست یا شیداستی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>این چنین بی‌هوش در محراب و منبر کی شدی</p></div>
<div class="m2"><p>گر به چشم دل نه جمله عامه نابیناستی؟</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هوشیاران را همی ماند به خاموشی ولیک</p></div>
<div class="m2"><p>چون سخن گوید تو گوئی سرش پر سوداستی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>روی زی محراب کی کردی اگر نه در بهشت</p></div>
<div class="m2"><p>بر امید نان و دیگ قلیه و حلواستی؟</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جای کم‌خواران و ابدالان کجا بودی بهشت</p></div>
<div class="m2"><p>گر براندازهٔ شکم و معدهٔ اینهاستی؟</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گوئی از امر خدای است، ای پسر، بر مرد عقل</p></div>
<div class="m2"><p>امر ازو برخاستی گر عقل ازو برخاستی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عقل در ترکیب مردم ز آفرینش حاکم است</p></div>
<div class="m2"><p>گر نه عقلستی برو نه چون و نه ایراستی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خلق و امر او راست هردو، کرد و فرمود آنچه خواست</p></div>
<div class="m2"><p>کی روا باشد که گوئی زین سپس «گر خواستی»؟</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر شنودی، ای برادر، گفتمت قولی تمام</p></div>
<div class="m2"><p>پاک و با قیمت که گوئی عنبر ساراستی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وانکه می‌گوید که «حجت گر حکیمستی چرا</p></div>
<div class="m2"><p>در درهٔ یمگان نشسته مفلس و تنهاستی؟»</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نیست آگه زانکه گر من همچو بد حالمی</p></div>
<div class="m2"><p>پشت من چون پشت او پیش شهان دوتاستی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>من نخواهم کانچه دارد شاه ملکستی مرا</p></div>
<div class="m2"><p>وانچه من دانم ز هر فن علمها اوراستی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>من به یمگان خوار و زار و بی‌نوا کی ماندمی</p></div>
<div class="m2"><p>گرنه کار دین چنین در شور و در غوغاستی؟</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کی شده‌ستی نفس من بر پشت حکمت‌ها سوار</p></div>
<div class="m2"><p>گرنه پشت من سوار دلدل شهباستی؟</p></div></div>