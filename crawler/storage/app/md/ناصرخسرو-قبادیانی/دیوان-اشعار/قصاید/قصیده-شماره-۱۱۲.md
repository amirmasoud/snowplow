---
title: >-
    قصیدهٔ شمارهٔ ۱۱۲
---
# قصیدهٔ شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>نگه کن زده صف دو انبوه لشکر</p></div>
<div class="m2"><p>یکی را یکی ایستاده برابر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه آن جای این را نه این جای آن را</p></div>
<div class="m2"><p>بگردند هردو به هردو صف اندر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دو سوی صف دو برادر مبارز</p></div>
<div class="m2"><p>ابا هر یکی پنج فرزند در خور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسولی شغب کو میان دو صف‌شان</p></div>
<div class="m2"><p>دوان زین برادر سوی آن برادر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسولی که پیغام او از پس او</p></div>
<div class="m2"><p>همی ماند اندر میان دو لشکر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنند آتشی هر دو لشکر ولیکن</p></div>
<div class="m2"><p>همه روی بر روی بنهند یکسر</p></div></div>