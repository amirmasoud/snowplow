---
title: >-
    قصیدهٔ شمارهٔ ۲۴۹
---
# قصیدهٔ شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>چه چیز بهتر و نیکوتر است در دنیی؟</p></div>
<div class="m2"><p>سپاه نی ملکی نی ضیاع نی رمه نی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن شریف‌تر و بهتر است سوی حکیم</p></div>
<div class="m2"><p>ز هرچه هست در این ره گذار بی‌معنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدین سخن شده‌ای تو رئیس جانوران</p></div>
<div class="m2"><p>بدین فتادند ایشان به زیر بیع و شری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن که بانگ توست او جدا نگر به چه شد</p></div>
<div class="m2"><p>ز بانگ آن دگران جز به حرف‌های هجی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگاه کن که بدین حرف‌ها چگونه خبر</p></div>
<div class="m2"><p>به جان زید رساند زبان عمرو همی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز این حدیث خبر نیست سوی جانوران</p></div>
<div class="m2"><p>خرد گوای من است اندر این قوی دعوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن زجملهٔ حیوان به ما رسید، چنانک</p></div>
<div class="m2"><p>ز ما به جمله به جان نبی رسید نبی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن نهان ز ستوران به ما رسید، چو وحی</p></div>
<div class="m2"><p>نهان رسید ز ما زی نبی به کوه حری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو وحی خوب نمودم ضمیر بینا را</p></div>
<div class="m2"><p>ببین تو گر چه نبیندش خاطر اعمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ستور و مردم و پیغمبر، این سه مرتبت است</p></div>
<div class="m2"><p>بدین دو وحی جدا مانده هر یک از دگری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر گزیده به وحی است زی خدای رسول</p></div>
<div class="m2"><p>تو گزیده و حیوان به جملگی پژوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به دل ببین که نه دیدن همه به چشم بود</p></div>
<div class="m2"><p>به دست بیند قصاب لاغر از فربی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به لوح محفوظ‌اندر نگر که پیش تست</p></div>
<div class="m2"><p>درو همی نگرد جبرئیل و بویحیی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به پیش توست ولیکن خط فریشتگان</p></div>
<div class="m2"><p>همی ندانی خواندن گزافه بی‌املی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مگر که یاد نداری که چشم تو نشناخت</p></div>
<div class="m2"><p>به خط خویش الف را مگر بجهد از بی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خط فریشتگان را همی بخواهی خواند</p></div>
<div class="m2"><p>چنین به بی‌ادبی کردن و لجاج و مری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به چشم قول خدای از جهان او بشنو</p></div>
<div class="m2"><p>که نه سخن نشنوده است کس مگر به ندی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به راه چشم شنو قول این جهان که حکیم</p></div>
<div class="m2"><p>به راه چشم شنوده است گفتهٔ دنیی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به راه چشم شنود از درخت قول خدای</p></div>
<div class="m2"><p>که «من خدای جهانم» به طور بر موسی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سخن نگوید جز با زبان و کام شکر</p></div>
<div class="m2"><p>نگفت نیز مگر با کفت سخن حنی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به نزد شکر رازی است کز جهان آن را</p></div>
<div class="m2"><p>شکر همی نکند جز به سوی کام انهی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>روا بود که نیابد ز خلق راز خدای</p></div>
<div class="m2"><p>مگر که سوی یکی بهتر از همه مجری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شنود قول الهی و کار کرد بران</p></div>
<div class="m2"><p>جهان به جمله ز چرخ بروج تا به ثری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ندارد این زمی و آب هیچ کار جز آنک</p></div>
<div class="m2"><p>به جهد روح‌نما را همی دهند اجری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زحل همی چکند؟ آنچه هست کار زحل</p></div>
<div class="m2"><p>سهی همی چکند؟ آنچه هست کار سهی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همیت گوید هریک که کار خویش بکن</p></div>
<div class="m2"><p>اگرت چشم درست است درنگر باری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خدای ما سوی ما نامه‌ای نوشت شگفت</p></div>
<div class="m2"><p>نوشته‌هاش موالید و آسمانش سحی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شریفتر سخنی مردم است، کاین نامه</p></div>
<div class="m2"><p>ز بهر این سخنان کرد کردگار انشی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سخن که دید سخن گوی و عالم و زنده؟</p></div>
<div class="m2"><p>چنین سزد سخن کردگار خلق، بلی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رسول خود سخنی باشد از خدای به خلق</p></div>
<div class="m2"><p>چنانکه گفت خداوند خلق در عیسی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو را سخن نه بدان داده‌اند تا تو زبان</p></div>
<div class="m2"><p>برافگنی به خرافات خندناک جحی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سخن به منزلت مرکب است جان تو را</p></div>
<div class="m2"><p>برو توانی رفتن به سوی شهر هدی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در هدی بگشاید مگر کلید سخن</p></div>
<div class="m2"><p>همو گشاید درهای آفت و بلوی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گهی سخن حسک و زهر و خنجر است و سنان</p></div>
<div class="m2"><p>گهی سخن شکر و قند و مرهم است و طلی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زبان به کام در افعی است مرد نادان را</p></div>
<div class="m2"><p>حذرت باید کردن همی از آن افعی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سخن سپارد بی‌هوش را به بند و بلا</p></div>
<div class="m2"><p>سخن رساند هشیار را به عهد و لوی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مباش بر سخن خویش فتنه چون طوطی</p></div>
<div class="m2"><p>سخن نخست بیاموز و پس بده فتوی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به اسپ و جامهٔ نیکو چرا شدی مشغول؟</p></div>
<div class="m2"><p>سخنت نیکو باید نه طیلسان و ردی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سخن مجوی فزون زانکه حق توست از من</p></div>
<div class="m2"><p>که آن ربی بود و نیست‌مان حلال ربی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>روا بود که ز بهر سخن به مصر شوی</p></div>
<div class="m2"><p>وگر همه به مثل جان و دل همی به کری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که کیمیای سعادت در این جهان سخن است</p></div>
<div class="m2"><p>بزرجمهر چنین گفته بود با کسری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دریغ‌دار ز نادان سخن که نیست صواب</p></div>
<div class="m2"><p>به پیش خوگ نهادن نه من و نه سلوی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زنا بود که سخن را به اهل جهل دهی</p></div>
<div class="m2"><p>زنا مکن که نه خوب است زی خدای زنی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سخن ز دانا بشنو زبون خویش مباش</p></div>
<div class="m2"><p>مگیر خیره چو مجنون سخنت را لیلی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>رها شد از شکم ماهی و شب و دریا</p></div>
<div class="m2"><p>به یک سخن چو شنودیم یونس بن متی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>اگر نخواهی تا خیره و خجل مانی</p></div>
<div class="m2"><p>مگوی خیره سخن جز که براساس و بنی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>برادرند به یک‌جا دروغ و رسوائی</p></div>
<div class="m2"><p>جدا ندید مرین را ازان هگرز کسی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دروغ سوی هنرپیشگان روا نشود</p></div>
<div class="m2"><p>وگرچه روی و ریا را همی کند آری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دروغ‌گوی به آخر نکال و شهره شود</p></div>
<div class="m2"><p>چنانکه سوی خردمند شهره شد مانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بگیر هدیه ز حجت به وصف‌های سخن</p></div>
<div class="m2"><p>بر از معانی شعری به روشنی شعری</p></div></div>