---
title: >-
    قصیدهٔ شمارهٔ ۱۲۸
---
# قصیدهٔ شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>بفریفت این زمان چو آهرمنش</p></div>
<div class="m2"><p>تا همچو موم نرم کند آهنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکو به گرد این زن پرمکر گشت</p></div>
<div class="m2"><p>گر ز آهنست نرم کند گردنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خیر خیر کرد نخواهی ستم</p></div>
<div class="m2"><p>بر خویشتن حذر کن ازین بد کنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این دهر بی‌وفا که نزاید هگرز</p></div>
<div class="m2"><p>جز شر و شور از شب آبستنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایمن مشو زکینهٔ او ای پسر</p></div>
<div class="m2"><p>هرچند شادمان بود و خوش‌منش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر روی بی‌خرد نبود شرم و آب</p></div>
<div class="m2"><p>پرهیز کن مگرد به پیرامنش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تن به تیغ تیز جدا کرده به</p></div>
<div class="m2"><p>آن سر که باک نیستش از سرزنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون مرد شوربخت شد و روز کور</p></div>
<div class="m2"><p>خشکی و درد سر کند از روغنش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چ او گران بخرد ارزان شود</p></div>
<div class="m2"><p>در خنب و خنبه ریگ شود ارزنش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر هرکه تیر راست کند بخت بد</p></div>
<div class="m2"><p>بر سینه چون خمیر شود جوشنش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون تنگ سخت کرد برو روزگار</p></div>
<div class="m2"><p>جامهٔ فراخ تنگ شود بر تنش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ابر بهار و باد صبا نگذرند</p></div>
<div class="m2"><p>با بخت گشته بر در و بر روزنش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وان را که روزگار مساعد شود</p></div>
<div class="m2"><p>با ناوکی نبرد کند سوزنش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور بنگرد به دشت سوی خار خشک</p></div>
<div class="m2"><p>از شاخ او سلام کند سوسنش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پروین به جای قطره ببارد ز میغ</p></div>
<div class="m2"><p>گر میغ بگذرد ز بر برزنش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آویخته است زهرش در نوش او</p></div>
<div class="m2"><p>آمیخته است تیره‌ش با روشنش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وین زهرگن ز ما کند از بهر او</p></div>
<div class="m2"><p>روشن چو زهره روی چو آهرمنش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آگه منم ز خوی بد او ازانک</p></div>
<div class="m2"><p>کس نازمود هرگز بیش از منش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زی من یکی است نیک و بد دهر ازانک</p></div>
<div class="m2"><p>سورش بقا ندارد و نه شیونش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مفگن سپر چو تیغ بر آهخت و نیز</p></div>
<div class="m2"><p>غره مشو به لابهٔ مرد افگنش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر روی تو به کینه بخواهد شخود</p></div>
<div class="m2"><p>چون عاقلان به صبر بچن ناخنش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر دشمن ضعیف مدار ایمنی</p></div>
<div class="m2"><p>وز خویشتن به نیکوی ایمن کنش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وان را که دست خویش بیابی برو</p></div>
<div class="m2"><p>غافل مباش و بیخ ز بن برکنش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وان را که حاسد است حسد خود بس است</p></div>
<div class="m2"><p>اندر دل ایستاده به پاداشنش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زان رنجه‌تر کسی نبود در جهان</p></div>
<div class="m2"><p>کاندر دلش نشسته بود دشمنش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هرکو زنفس خویش بترسد کسی</p></div>
<div class="m2"><p>نتواند، ای پسر، که کند ایمنش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>احسنت و زه مگوی بدآموز را</p></div>
<div class="m2"><p>زیرا که پاک نیست دل و دامنش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خواهد که خرمن تو بسوزند نیز</p></div>
<div class="m2"><p>هر مدبری که سوخته شد خرمنش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دست از دروغ زن بکش و نان مخور</p></div>
<div class="m2"><p>با کرویا و زیره و آویشنش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وصف دروغ نیز دروغ است ازانک</p></div>
<div class="m2"><p>پایان رود طبیعت پالاونش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مشنو دروغ تا نشوی خوار ازانک</p></div>
<div class="m2"><p>چون سیم قلب قلب بود خازنش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در هاونی که صبر بکوبد طبیب</p></div>
<div class="m2"><p>چون صبر تلخ تلخ شود هاونش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گلشن چو کرد مرد درو کاه دود</p></div>
<div class="m2"><p>گلخن شود زدود سیه گلشنش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز اندیشگان بیهده زاید دروغ</p></div>
<div class="m2"><p>همچون شبه سیاه بود معدنش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پر نور ایزد است دل راست گوی</p></div>
<div class="m2"><p>ز اسفندیار داد خبر بهمنش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون راست بود خوب نماید سخن</p></div>
<div class="m2"><p>در خوب جامه خوب شود آگنش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از علم زاید و زخرد قول راست</p></div>
<div class="m2"><p>چون مرد نیک نیک بود مسکنش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فرزند جز کریم نباشد بخوی</p></div>
<div class="m2"><p>چون همچو مرد بود نکوخو زنش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای حجت زمین خراسان بگوی</p></div>
<div class="m2"><p>بر راستی سخن که توی ضامنش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ابلیس در جزیرهٔ تو برنشست</p></div>
<div class="m2"><p>بر بی‌فسار سخت کش توسنش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سالوک‌وار زد به کمرش اندرون</p></div>
<div class="m2"><p>از بهر حرب دامن پیراهنش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جز صبر هیچ حیله ندانم تو را</p></div>
<div class="m2"><p>با مکر دیو و با سپه کودنش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خاموش تو که گوش خرد کر کرد</p></div>
<div class="m2"><p>بر زیر و بم حنجرهٔ مذنش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هرچند بی‌شمار مر او را فن است</p></div>
<div class="m2"><p>خوار است سوی مرد ممیز فنش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هرک اعتماد کرد بر این بی‌وفا</p></div>
<div class="m2"><p>از بیخ و بار برکند این ریمنش</p></div></div>