---
title: >-
    قصیدهٔ شمارهٔ ۱۵۱
---
# قصیدهٔ شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>به راه دین نبی رفت ازان نمی‌یاریم</p></div>
<div class="m2"><p>که راه با خطر و ما ضعیف و بی‌یاریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون روز دزد ره ما گرفت اگر به سفر</p></div>
<div class="m2"><p>بجز به شب نرویم، ای پسر، سزاواریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازین به ستان ستاره به روز پنهانیم</p></div>
<div class="m2"><p>ز چشم خلق و به شب رهبریم و بیداریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وگر به شخص ز جاهل نهان شدیم، به علم</p></div>
<div class="m2"><p>چو آفتاب سوی عاقلان پدیداریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حکمت است و خرد بر فرود مردان را</p></div>
<div class="m2"><p>و گرنه ما همه از روی شخص همواریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی ز ما چو گل است و یکی چو خار به طبع</p></div>
<div class="m2"><p>اگرچه یکسره جمله به سان گلزاریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن به علم بگوئیم تا ز یک‌دیگر</p></div>
<div class="m2"><p>جدا شویم که ما هر دو اهل گفتاریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن پدید کند کز من و تو مردم کیست</p></div>
<div class="m2"><p>که بی‌سخن من و تو هردو نقش دیواریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان، خدای جهان را مثل چوبستانی است</p></div>
<div class="m2"><p>که ما به جمله بدین بوستان در اشجاریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیای تا من و تو هر دو، ای درخت خدا،</p></div>
<div class="m2"><p>ز بار خویش یکی چاشنی فرو باریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لجاج و مشغله ماغاز تا سخن گوئیم</p></div>
<div class="m2"><p>که ما ز مشغلهٔ تو ز خانه آواریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر تو ای بخرد ناصبی مسلمانی</p></div>
<div class="m2"><p>تو را که گفت که ما شیعت اهل زناریم؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>محمد و علی از خلق بهترند چه بود</p></div>
<div class="m2"><p>گر از فلان و فلان شان بزرگتر داریم؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خزینه‌دار خدایندو، سرهای خدای</p></div>
<div class="m2"><p>همی به ما برسانند کاهل اسراریم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به غار سنگین در نه، به غار دین اندر</p></div>
<div class="m2"><p>رسول را، ز دل پاک صاحب الغاریم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز علم بهرهٔ ما گندم است و بهر تو کاه</p></div>
<div class="m2"><p>گمان مبر که چو تو ماستور و که خواریم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به خمر دین چو تو خر، مست گشته‌ای شاید</p></div>
<div class="m2"><p>که خویشتن بکشیم از تو ما که هشیاریم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز بهر تو که همی خویشتن هلاک کنی</p></div>
<div class="m2"><p>به بی هشی، همگان روز و شب به تیماریم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو آگهیم که مستی و بی‌خرد، ما را</p></div>
<div class="m2"><p>اگرچه سخت بیازاری از تو نازاریم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وز آن قبل که تو حکمت شنود نتوانی</p></div>
<div class="m2"><p>همیشه با تو به حکمت دهان به مسماریم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو را که مار گزیده‌است حیله تریاق است</p></div>
<div class="m2"><p>ز ما بخواه، گمان چون بری که ما ماریم؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو گرد چون و چرا گر همی نیاری گشت</p></div>
<div class="m2"><p>چرا و چون تو را ما به جان خریداریم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خرد ز بهر چه دادندمان، که ما به خرد</p></div>
<div class="m2"><p>گهی خدای‌پرست و گهی گنه‌کاریم؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>«مکن بدی تو و نیکی بکن» چرا فرمود</p></div>
<div class="m2"><p>خدای ما را گر ما نه حی و مختاریم؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چرا که گرگ ستمگاره نیست سوی خدای</p></div>
<div class="m2"><p>به فعل خویش گرفتار و ، ما گرفتاریم؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چرا به بانگ و خروش و فغان بی معنی</p></div>
<div class="m2"><p>کلنگ نیست سبکسار و ما سبکساریم؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چرا بر آهو و نخچیر روزه نیست و نماز؟</p></div>
<div class="m2"><p>چرا من و تو بدین کارها گران‌باریم؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه داد یزدان ما را ز جملگی حیوان</p></div>
<div class="m2"><p>مگر خرد که بدان بر ستور سالاریم؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر به فضل و خرد بر خران خداوندیم</p></div>
<div class="m2"><p>همان به فضل و خرد بندگان جباریم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خرد تواند جستن ز کار چون و چرا</p></div>
<div class="m2"><p>که بی‌خرد به مثل ما درخت بی‌باریم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خرد چرا که نجوید که ما به امر خدای</p></div>
<div class="m2"><p>چرا که یک مه تا شب به روز ناهاریم؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به خون ناحق ما را چرا نمیراند</p></div>
<div class="m2"><p>خدای، گر سوی او خونی و ستمگاریم؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وگر گناه نخواهد ز ما و ما بکنیم</p></div>
<div class="m2"><p>نه بنده‌ایم خداوند را که قهاریم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وگر به خواست وی آید همی گناه از ما</p></div>
<div class="m2"><p>نه‌ایم عاصی بل نیک و خوب کرداریم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر مر این گره سخت را تو بگشائی</p></div>
<div class="m2"><p>حقت به جان به دل بنده‌وار بگزاریم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وگر تو گرد چنین کارها نیاری گشت</p></div>
<div class="m2"><p>مگرد، وز بر ما دور شو، که ما یاریم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وگر بپرسی از این مشکلات مر ما را</p></div>
<div class="m2"><p>به پیش حملهٔ تو پای، سخت بفشاریم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به دست خاطر روشن بنای مشکل را</p></div>
<div class="m2"><p>برآوریم به چرخ و به زر بنگاریم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مبارزان سپاه شریعتیم و قران</p></div>
<div class="m2"><p>از آنکه شیعت حیدر، سوار کراریم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به نزد مردم بیمار ناخوش است شکر</p></div>
<div class="m2"><p>شگفت نیست که ما نزد تو ز کفاریم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یکی ز ما و هزار از شما اگر چه شما</p></div>
<div class="m2"><p>چو مار و مورچه بسیار و ما نه بسیاریم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سپه نباشد پانصد ستور بر یک مرد</p></div>
<div class="m2"><p>روا بود که شما را سپاه نشماریم</p></div></div>