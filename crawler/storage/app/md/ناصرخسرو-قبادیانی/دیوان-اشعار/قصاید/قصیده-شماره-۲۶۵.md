---
title: >-
    قصیدهٔ شمارهٔ ۲۶۵
---
# قصیدهٔ شمارهٔ ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>نگه کن سحرگه به زرین حسامی</p></div>
<div class="m2"><p>نهان کرده در لاژوردین نیامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که خوش خوش برآردش ازو دست عالم</p></div>
<div class="m2"><p>چو برقی که بیرون کشی از غمامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی گند پیر است شب زشت و زنگی</p></div>
<div class="m2"><p>که زاید همی خوب رومی غلامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وجود از عدم همچنین گشت پیدا</p></div>
<div class="m2"><p>از اول که نوری کنون از ظلامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مپندار بر روز شب را مقدم</p></div>
<div class="m2"><p>چو هر بی‌تفکر یله‌گوی عامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که شب نیست جز نیستی‌ی روز چیزی</p></div>
<div class="m2"><p>نه بی‌خانه‌ای هست موجود بامی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر چند هر پختنی خام باشد</p></div>
<div class="m2"><p>نه چون تر و پخته بود خشک و خامی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظامی به از بی‌نظامی وگرچه</p></div>
<div class="m2"><p>نظامی نگیرد مگر بی‌نظامی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسوی تمامی رود بودنی‌ها</p></div>
<div class="m2"><p>به قوت تمام است هر ناتمامی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو در راه عمری همیشه شتابان</p></div>
<div class="m2"><p>در این ره نشایدت کردن مقامی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به منزل رسی گرچه دیر است، روزی</p></div>
<div class="m2"><p>چو می‌بری از راه هر روز گامی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نبینی که‌ت افگند چون مرغ نادان</p></div>
<div class="m2"><p>ز روز و شبان دهر در پیسه دامی؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نویدت دهد هر زمانی به فردا</p></div>
<div class="m2"><p>نویدی که آن را نباشد خرامی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که را داد تا تو همی چشم داری</p></div>
<div class="m2"><p>فزون از لباس و شراب و طعامی؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>منش پنجه و هشت سال آزمودم</p></div>
<div class="m2"><p>نکرد او به کارم فزون زین قیامی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی مرکبی داده بودم رمنده</p></div>
<div class="m2"><p>ازین سرکشی بدخوئی بد لگامی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همی تاخت یک چند چون دیو شرزه</p></div>
<div class="m2"><p>پس هر مرادی و عیشی و کامی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا دید بر مرکبی تند و سرکش</p></div>
<div class="m2"><p>حکیمی کریمی امامی همامی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>«چرا» گفت ک «این را لگامی نسازی</p></div>
<div class="m2"><p>که با آن ازو نیز ناید دلامی؟»</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز هر کس بجستم فساری و قیدی</p></div>
<div class="m2"><p>بهر رایضی نیز دادم پیامی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نشد نرم و ناسود تا بر نکردم</p></div>
<div class="m2"><p>بسر بر مر او را ز عقل اوستامی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کنون هر حکیمی به اندیشه گوید</p></div>
<div class="m2"><p>که هرگز ندیدم چنین نرم و رامی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>طمع بود آنکه‌م همی تاخت هرسو</p></div>
<div class="m2"><p>شب و روز با من همی زد لطامی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو زو بازگشتم ندیدم به عاجل</p></div>
<div class="m2"><p>به دنیا و دین خود اندر قوامی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جهان هرچه دادت همی باز خواهد</p></div>
<div class="m2"><p>نهاده است بی‌آب رخ چون رخامی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به هر دم کشیدن همی وام خواهی</p></div>
<div class="m2"><p>بهر دم زدن می‌دهی باز وامی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کم از دم چه باشد، چو می‌باز خواهد</p></div>
<div class="m2"><p>چرا چشم داری عطا زو حطامی؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که دیدی که زو نعره‌ای زد به شادی</p></div>
<div class="m2"><p>که زو برنیاورد ای وای مامی؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که بودی آنکه بخرید سودی ز عالم</p></div>
<div class="m2"><p>که نستد فزون از مصیبت ورامی؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حذر دار تا ریش نکندت ازیرا</p></div>
<div class="m2"><p>حسامی است این، ای برادر، حسامی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرا دانی از وی که کرده‌است ایمن؟</p></div>
<div class="m2"><p>کریمی حکیمی همامی امامی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که فانی جهان از فنا امن یابد</p></div>
<div class="m2"><p>اگر زو بیابد جواب سلامی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر صورتش را ندیدی ندیدی</p></div>
<div class="m2"><p>به دین بر ز یزدان دادار نامی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وگر لشکر او ندیدی نبیند</p></div>
<div class="m2"><p>چنان جز به محشر دو چشمت زحامی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به جودش بشست این جهان دست از من</p></div>
<div class="m2"><p>نه جوری کشم زو نه نیز انتقامی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>برابر شدم بی‌طمع با امیری</p></div>
<div class="m2"><p>که بایدش بی‌چاشت از شام شامی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو من هر حلالی بدو باز دادم</p></div>
<div class="m2"><p>چگونه فریبد مرا زو حرامی؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سرم زیر فرمان شاهی نیارد</p></div>
<div class="m2"><p>نه تختی نه گاهی نه رودی نه جامی</p></div></div>