---
title: >-
    قصیدهٔ شمارهٔ ۱۱۱
---
# قصیدهٔ شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>برآمد سپاه بخار از بحار</p></div>
<div class="m2"><p>سوارانش پر در کرده کنار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخ سبز صحرا بخندید خوش</p></div>
<div class="m2"><p>چو بر وی سیاه ابر بگریست زار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل سرخ بر سر نهاد و ببست</p></div>
<div class="m2"><p>عقیقین کلاه و پرندین ازار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدرید بر تن سلب مشک بید</p></div>
<div class="m2"><p>زجور زمستان به پیش بهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بازوی پر خون درون بید سرخ</p></div>
<div class="m2"><p>بزد دشنه زین غم هزاران هزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بس سرد گفتارهای شمال</p></div>
<div class="m2"><p>بریده شد از گل دل جویبار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبینی که هر شب سحرگه هنوز</p></div>
<div class="m2"><p>دواج سمور است بر کوهسار؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبا آید اکنون به عذر شمال</p></div>
<div class="m2"><p>سحرگاه تازان سوی لاله‌زار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بشویدش عارض به لولوی تر</p></div>
<div class="m2"><p>بیالایدش رخ به مشکین عذار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیارد سوی بوستان خلعتی</p></div>
<div class="m2"><p>که لولوش پود است و پیروزه تار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوی گلبن زرد استام زر</p></div>
<div class="m2"><p>سوی لالهٔ سرخ جام عقار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سوی مادر سوسن تازه تاج</p></div>
<div class="m2"><p>سوی دختر نسترن گوشوار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به سر بر نهد نرگس نو به باغ</p></div>
<div class="m2"><p>به اردیبهشت افسر شاهوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نوان و خرامان شود شاخ بید</p></div>
<div class="m2"><p>سحرگاه چون مرکب راهوار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دهد دست و سر بوس گل را سمن</p></div>
<div class="m2"><p>چو گیرد سمن را گل اندر کنار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شگفتی نگه کن به کار جهان</p></div>
<div class="m2"><p>وزو گیر بر کار خویش اعتبار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که تا شادمانه نگردد زمین</p></div>
<div class="m2"><p>نپوشد هوا جامهٔ سوکوار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو نسرین بخندد شود چشم گل</p></div>
<div class="m2"><p>به خون سرخ چون چشم اسفندیار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو نرگس شود باز چون چشم باز</p></div>
<div class="m2"><p>شود پای بط بر چنار آشکار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پر از چین شود روی شاهسپرم</p></div>
<div class="m2"><p>چو تازه شود عارض گلنار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نگه کن به لاله و به ابر و ببین</p></div>
<div class="m2"><p>جدا نار از دود، وز دود نار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سوی شاخ بادام شو بامداد</p></div>
<div class="m2"><p>اگر دید خواهی همی قندهار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>و گر انده از برف بودت مجوی</p></div>
<div class="m2"><p>ز مشکین صبا بهتر انده گسار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نگه کن بدین بی‌فساران خلق</p></div>
<div class="m2"><p>تو نیز از سر خود فرو کن فسار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر نیست سوی تو داری دگر</p></div>
<div class="m2"><p>همه هوش و دل سوی این دار دار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وگر نیستت طمع باغ بهشت</p></div>
<div class="m2"><p>چو خر خوش بغلت اندر این مرغزار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نگه دار اندر زیان آن خویش</p></div>
<div class="m2"><p>چنانکه‌ت بگفته‌است بسیار خوار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به نسیه مده نقد اگر چند نیز</p></div>
<div class="m2"><p>به خرما بود وعده و نقد خار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کرا معده خوش گردد از خار و خس</p></div>
<div class="m2"><p>شود کامش از شیر و روغن فگار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چه باید تو را سلسبیل و رحیق</p></div>
<div class="m2"><p>چو خرسند گشتی به سرکه و شخار؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جهان ره گذار است، اگر عاقلی</p></div>
<div class="m2"><p>نباید نشستنت بر ره گذار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ستور است مردم در این ره چنانک</p></div>
<div class="m2"><p>بریده نگردد قطار از قطار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شتابنده جمله که یک دم زدن</p></div>
<div class="m2"><p>نپاید کسی را برادر نه یار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ره تو کدام است از این هر دو راه؟</p></div>
<div class="m2"><p>بیندیش و برگیر نیکو شمار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر سازوار است و خوش مر تو را</p></div>
<div class="m2"><p>بت رود ساز و می خوشگوار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وز این حالها تو به کردار خواب</p></div>
<div class="m2"><p>نگردی همی سرد زین روزگار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وز این ایستادن به درگاه شاه</p></div>
<div class="m2"><p>وز این خواستن سوی دهدار بار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>وز این بند و بگشای و بستان و ده</p></div>
<div class="m2"><p>وز این هان و هین و از این گیر و دار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>وز این در کشیدن به بینی خویش</p></div>
<div class="m2"><p>ز بهر طمع این و آن را مهار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گمانی مبر کاین ره مردم است</p></div>
<div class="m2"><p>بر این کار نیکو خرد برگمار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همی خویشتن شهره خواهی به شهر</p></div>
<div class="m2"><p>که من چاکر شاهم و شهریار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شکار یکی گشتی از بهر آنک</p></div>
<div class="m2"><p>مگر دیگری را بگیری شکار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بدان تا به من برنهی بار خویش</p></div>
<div class="m2"><p>یکی دیگرت کرد سر زیر بار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ستوری تو سوی من از بهر آنک</p></div>
<div class="m2"><p>همی باز نشناسی از فخر عار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو را ننگ باید همی داشتن</p></div>
<div class="m2"><p>بخیره همی چون کنی افتخار؟</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ستور از کسی به که بر مردمی</p></div>
<div class="m2"><p>بعمدا ستوری کند اختیار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز مردم درختی نه‌ای بارور</p></div>
<div class="m2"><p>بلندی و بی‌بر چو بید و چنار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اگر میوه داری نشد هیچ بید</p></div>
<div class="m2"><p>به دانش تو باری بشو میوه‌دار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دریغ این قد و قامت مردمی</p></div>
<div class="m2"><p>بدین راستی بر تو، ای نابکار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اگر باز گردی ز راه ستور</p></div>
<div class="m2"><p>شود بید تو عود ناچار و چار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>وگر همچنین خود بمانی چو دیو</p></div>
<div class="m2"><p>دل از جهل پر دود و سر پرخمار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کسی برتو نتواند، از جهل،بست</p></div>
<div class="m2"><p>یکی حرف دانش به سیصد نوار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تو را صورت مردمی داده‌اند</p></div>
<div class="m2"><p>مکن خیره مر خویشتن را حمار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بکن جهد آن تا شوی مردمی</p></div>
<div class="m2"><p>مکن با خدای جهان کارزار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تو را روی خوب است لیکن بسی است</p></div>
<div class="m2"><p>به دیوار گرمابه‌ها بر نگار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به دانش تو صورت‌گر خویش باش</p></div>
<div class="m2"><p>برون آی از این ژرف چه مردوار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خرد ورز ازیرا سوی هوشمند</p></div>
<div class="m2"><p>زجاهل بسی به بود موش و مار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو مر خویشتن را بدانی به حق</p></div>
<div class="m2"><p>در این ژرف زندان نگیری قرار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز کردار بد باز گردی به عذر</p></div>
<div class="m2"><p>چو هشیار مردان سوی کردگار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>مر این گوهر ایزدی را به علم</p></div>
<div class="m2"><p>بشوئی ز زنگار عیب و عوار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ازیرا که آتش، چو شد زر پاک،</p></div>
<div class="m2"><p>برو کرد نتواند از اصل کار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز حجت شنو حجت ای منطقی</p></div>
<div class="m2"><p>ز هر عیب صافی چو زر عیار</p></div></div>