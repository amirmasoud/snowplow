---
title: >-
    قصیدهٔ شمارهٔ ۲۷۲
---
# قصیدهٔ شمارهٔ ۲۷۲

<div class="b" id="bn1"><div class="m1"><p>ای داده دل و هوش بدین جای سپنجی</p></div>
<div class="m2"><p>بیم است که از کبر در این جای نگنجی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>والله که نیاید به ترازوی خرد راست</p></div>
<div class="m2"><p>گر نعمت دنیا را با رنج بسنجی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور مملکت روم بگیری چو سکندر</p></div>
<div class="m2"><p>هرگز نشود ملک تو این جای سپنجی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وز بند و بلای فلکی رسته نگردی</p></div>
<div class="m2"><p>هرچند تو را بنده شود رومی و طنجی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون روزی تو نانی و یک مشت برنج است</p></div>
<div class="m2"><p>از بهر چه چندین به شب و روز برنجی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور همچو خز و بز بپوشدت گلیمی</p></div>
<div class="m2"><p>خزت چه همی باید و دیبای ترنجی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فردات تهی دست به کنجی بسپارند</p></div>
<div class="m2"><p>هرچند ملک‌وار کنون بر سر گنجی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صنعت به تو ضایع شد ازیرا که شب و روز</p></div>
<div class="m2"><p>مشغول به شطرنج و به نرد و شش و پنجی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بهر چه دادند تو را عقل، چه گوئی؟</p></div>
<div class="m2"><p>ناخوش بخوری چون خر و چون غلبه بلنجی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وز بهر چه دادند تو را بار خدائی؟</p></div>
<div class="m2"><p>وز بهر چه شد بنده تو را هندو و زنجی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زیرا که تو بیش آمدی اندر دین زیشان</p></div>
<div class="m2"><p>پس چون نکنی شکر و زیادت نلفنجی؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>امروز که شاهی و رتب فنج بیندیش</p></div>
<div class="m2"><p>زیرا که نماند ابدی شاهی و فنجی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از مکر خداوند همی هیچ نترسی</p></div>
<div class="m2"><p>زان است که با بنده پر از مکر و شکنجی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اندیشه کن از بندگی امروز که بنده‌ت</p></div>
<div class="m2"><p>در پیش به پای است و تو بنشسته به شنجی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همچون کدوئی سوی نبیدو، سوی مزگت</p></div>
<div class="m2"><p>آگنده به گاورس دو خرواری غنجی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با مسجد و با مؤذن چون سر که و ترفی</p></div>
<div class="m2"><p>با مسخره و مطرب چون شیر و برنجی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>والله که نسجند نماز تو ازیراک</p></div>
<div class="m2"><p>روی تو به قبله است و به دل با دف و صنجی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا خوی تو این است اگر گوهر سرخی</p></div>
<div class="m2"><p>نزدیک خردمند زراندود برنجی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رخسار تو را ناخن این چرخ شکنجید</p></div>
<div class="m2"><p>تو چند لب و زلفک بت روی شکنجی؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لختی به ترنج از قبل جانت میان سخت</p></div>
<div class="m2"><p>از بهر تن این سست میان چند ترنجی؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن است خردمند که خوردنش خلنج</p></div>
<div class="m2"><p>زان است که تو بی‌خرد از کاسه خلنجی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گرگی تو که بی‌نفعی و بی‌خنج ولیکن</p></div>
<div class="m2"><p>خود روز و شب اندر طلب نفعی و خنجی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همسایهٔ بی‌فایده گر شاید ما را</p></div>
<div class="m2"><p>همسایهٔ نیک است به افرنجه فرنجی</p></div></div>