---
title: >-
    قصیدهٔ شمارهٔ ۲۱۱
---
# قصیدهٔ شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>ای زود گرد گنبد بر رفته</p></div>
<div class="m2"><p>خانهٔ وفا به دست جفا رفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر من چرا گماشته‌ای خیره</p></div>
<div class="m2"><p>چندین هزار مست بر آشفته؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این دشته بر کشیده همی تازد</p></div>
<div class="m2"><p>وان با کمان و تیر برو خفته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینم کند به خطبه درون نفرین</p></div>
<div class="m2"><p>وانم به نامه فریه کند سفته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من خیره مانده زیرا با مستان</p></div>
<div class="m2"><p>هر دو یکی است گفته و ناگفته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته سخن چو سفته گهر باشد</p></div>
<div class="m2"><p>ناگفته همچو گوهر ناسفته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیدار کرد ما را بیداری</p></div>
<div class="m2"><p>پنهان ز بیم مستان بنهفته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خرگوش‌وار دیدم مردم را</p></div>
<div class="m2"><p>خفته دو چشم باز و خرد رفته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک خیل خوگ‌وار درافتاده</p></div>
<div class="m2"><p>با یکدگر چو دیوان کالفته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک جوق بر مثال خردمندان</p></div>
<div class="m2"><p>با مرکب و عمامهٔ زربفته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر سام یارده ز شر منبر</p></div>
<div class="m2"><p>گویان به طمع روز و شبان لفته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مستان و بیهشان چو بدیدندم</p></div>
<div class="m2"><p>شمع خرد فروخته بگرفته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زود از میان خویش براندندم</p></div>
<div class="m2"><p>پر درد جان و ز انده دل کفته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن جانور که سرگین گرداند</p></div>
<div class="m2"><p>زهر است سوی او گل بشکفته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیدار چون نشست بر خفته</p></div>
<div class="m2"><p>خفته ز عیب خویش شود تفته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زیرا که سخت زود سوی بیدار</p></div>
<div class="m2"><p>پیدا شود فضیحتی از خفته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای درها به رشته در آوردم</p></div>
<div class="m2"><p>روز چهارم از سومین هفته</p></div></div>