---
title: >-
    قصیدهٔ شمارهٔ ۲۸
---
# قصیدهٔ شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>شاخ شجر دهر غم و مشغله بار است</p></div>
<div class="m2"><p>زیرا که بر این شاخ غم و مشغله بار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنک او چو من از مشغله و رنج حذر کرد</p></div>
<div class="m2"><p>با شاخ جهان بیهده شورید نیارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با شاخ تو ای دهر و به درگاه تو اندر</p></div>
<div class="m2"><p>ما را به همه عمر نه کار است و نه بار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بار من، ای سفله، فگندی ز خر خویش</p></div>
<div class="m2"><p>اندر خر من چونکه نگوئیت چه بار است؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردار تو را هیچ نه اصل است و نه مایه</p></div>
<div class="m2"><p>گفتار تو را هیچ نه پود است و نه تار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>احسان و وفای تو به حدی است بس اندک</p></div>
<div class="m2"><p>لیکن حسد و مگر تو بی‌حد و کنار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صندوقچهٔ عدل تو مانده است به طرطوس</p></div>
<div class="m2"><p>دستارچهٔ جور تو در پیش کنار است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشگفت که من زیر تو بی‌خواب و قرارم</p></div>
<div class="m2"><p>هر گه که نه خواب است تو را و نه قرار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیچیده به مسکین تن من در به شب و روز</p></div>
<div class="m2"><p>همواره ستمگاره و خونخواره دو ما راست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای تن به یقین دان که تو را عاقبت کار</p></div>
<div class="m2"><p>چون گرد تو پیچیده دو مار است دماراست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناچار از اینجات برد آنکه بیاورد</p></div>
<div class="m2"><p>این نیست سرای تو که این راه گذار است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنگر که به چشمت شکم مادر، پورا،</p></div>
<div class="m2"><p>امروز در این عالم چون ناخوش و خوار است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اینجا بنمانی چو در آنجای نماندی</p></div>
<div class="m2"><p>تقدیر قیاسیت بدینجای به کار است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر نیست به غم جان تو بر رفتن از آنجا</p></div>
<div class="m2"><p>از رفتن ازین جای چرا دلت فگار است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای مانده در این راه‌گذر، راحله‌ای ساز</p></div>
<div class="m2"><p>از علم و ز پرهیز که راهت به قفار است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو خفته و پشتت ز بزه گشته گران بار</p></div>
<div class="m2"><p>با بار گران خفتن از اخلاق حمار است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بی‌هیچ گنه چونکه ببستندت ازین سان</p></div>
<div class="m2"><p>بی‌هیچ گنه بند کشیدن دشوار است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر هر که گنه کرد یکی بند نهادند</p></div>
<div class="m2"><p>بی هیچ گنه چونکه تو را بند چهار است؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پربند حصاری است روان تنت روان را</p></div>
<div class="m2"><p>در بند و حصاری تو، ازین کار تو زار است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر بند و حصار از قبل دشمن باید</p></div>
<div class="m2"><p>چون دشمن تو با تو در این بند و حصار است؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این کالبد جاهل خوش خوار تو گرگی است</p></div>
<div class="m2"><p>وین جان خردمند یکی میش نزار است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گوی از همه مردان خرد جمله ربودی</p></div>
<div class="m2"><p>گر میش نزار تو بر این گرگ سوار است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تن چاکر جان است مرو از پسش ایراک</p></div>
<div class="m2"><p>رفتن به مراد و سپس چاکر عار است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دستارت نیاید ز نوار ای پسر ایراک</p></div>
<div class="m2"><p>هرچند پر از نقش نوار است نوار است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جان تو درختی است خرد بار و سخن برگ</p></div>
<div class="m2"><p>وین تیره جسد لیف درشت و خس و خار است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نی‌نی که تو بر اشتر تن شهره سواری</p></div>
<div class="m2"><p>و اندر ره تو جوی و جر و بیشه و غار است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زین اشتر بی‌باک و مهارش به حذر باش</p></div>
<div class="m2"><p>زیرا که شتر مست و برو مار مهار است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باز خردت هست، بدو فضل و ادب گیر</p></div>
<div class="m2"><p>مر باز خرد را ادب و فضل شکار است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پرهیز کن از جهل به آموختن ایراک</p></div>
<div class="m2"><p>جهل است مثل عورت و پرهیز ازار است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در سایهٔ دین رو که جهان تافته ریگ است</p></div>
<div class="m2"><p>با شمع خرد باش که عالم شب تار است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بشکن به سر بی‌خردان در به سخن جهل</p></div>
<div class="m2"><p>زیرا که سخن آب خوش و جهل خمار است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بر علم تو حق است گزاریدن حکمت</p></div>
<div class="m2"><p>بگزار حق علم گرت دست گزار است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مر شاخ خرد را سخن حکمت برگ است</p></div>
<div class="m2"><p>دریای سخن را سخن پند بخار است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ای گشته دل تو سیه از گرد جهالت</p></div>
<div class="m2"><p>با این دل چون قار تو را جای وقار است؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون قار سیه نیست دل ما و پر از گرد</p></div>
<div class="m2"><p>گرچه دل چون قار تو پر گرد و غبار است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خرما و ترنج و بهی و گوز بسی هست</p></div>
<div class="m2"><p>زین سبز درختان، نه همه بید و چنار است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آن سر که به زیر کله و از بر تخت است</p></div>
<div class="m2"><p>در مرتبه دور است از آن سر که به دار است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اندر خور افسر شود از علم به تعلیم</p></div>
<div class="m2"><p>آن سر که ز بس جهل سزاوار فسار است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بیهوده و دشنام مگردان به زبان بر</p></div>
<div class="m2"><p>کاین هر دو ز تو یار تو را زشت نثار است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دشنام دهی باز دهندت ز پی آنک</p></div>
<div class="m2"><p>دشنام مثل چون درم دیر مدار است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دم بر تو شمرده‌است خداوند ازیراک</p></div>
<div class="m2"><p>فرداش به هر دم زدنی با تو شمار است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یارت ز خرد باید و طاعت به سوی آنک</p></div>
<div class="m2"><p>او را نه عدیل است و نه فرزند و نه یار است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اندر حرم آی، ای پسر، ایراک نمازی</p></div>
<div class="m2"><p>کان را به حرم در کند از مزد هزار است</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بشناس حرم را که هم اینجا به در توست</p></div>
<div class="m2"><p>با بادیه و ریگ و مغیلانت چه کار است؟</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کم بیش نباشد سخن حجت هرگز</p></div>
<div class="m2"><p>زیرا سخنش پاک‌تر از زر عیار است</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زر چون به عیار آمد کم بیش نگیرد</p></div>
<div class="m2"><p>کم بیش شود زری کان با غش وبار است</p></div></div>