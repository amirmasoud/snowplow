---
title: >-
    قصیدهٔ شمارهٔ ۱۵۷
---
# قصیدهٔ شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>ای شسته سر و روی به آب زمزم</p></div>
<div class="m2"><p>حج کرده چومردان و گشته بی‌غم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افزون زچهل سال جهد کردی</p></div>
<div class="m2"><p>دادی کم و خود هیچ نستدی کم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسیار بدین و بدان به حیلت</p></div>
<div class="m2"><p>کرباس بدادی به نرخ مبرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا پاک شد اکنون ز تو گناهان</p></div>
<div class="m2"><p>مندیش به دانگی کنون ز عالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افسوس نیاید تو را از این کار</p></div>
<div class="m2"><p>بر خویشتن این رازها مفرخم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین سود نبینم تو را ولیکن</p></div>
<div class="m2"><p>ایمن نه‌ای ای خر ز بیم بیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از درد جراحت رهد کسی کو</p></div>
<div class="m2"><p>از سر که نهد وز شخار مرهم؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کم بیشک پیمانه و ترازوی</p></div>
<div class="m2"><p>هرگز نشود پاک ز آب زمزم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر خویشتن ار تو بپوشی آن را</p></div>
<div class="m2"><p>آن نیست بسوی خدای مبهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از باد فراز آمد و به دم شد</p></div>
<div class="m2"><p>آن مال حرامی چه باد و چه دم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زین کار که کردی برون زده‌ستی</p></div>
<div class="m2"><p>بر خویشتن، ای خر، ستون پشکم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدارشو از خواب جهل و برخوان</p></div>
<div class="m2"><p>یاسین و به جان و به تن فرو دم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بفریفت تو را دیو تا گلیمی</p></div>
<div class="m2"><p>بفروختت، ای خر، به نرخ ملحم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گوئی که به سور اندرم، ولیکن</p></div>
<div class="m2"><p>از دور بماند به سور ماتم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در شور ستانت چنان گمان است</p></div>
<div class="m2"><p>کان میوه‌ستان است و باغ خرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از سیم طراری مشو به مکه</p></div>
<div class="m2"><p>مامیز چنین زهر و شهد برهم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر راه به دین اندرون برد راست</p></div>
<div class="m2"><p>زین خم چه جهی بیهده بدان خم؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر ز آدمی، ای پور، توبه باید</p></div>
<div class="m2"><p>کردن زگناهانت همچو آدم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر رنجه‌ای از آفتاب عصیان</p></div>
<div class="m2"><p>از توبه درون شو به زیر طارم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر رحمت و نعمت چرید خواهی</p></div>
<div class="m2"><p>از علم چر امروز و بر عمل چم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مر تخم عمل را به نم نه از علم</p></div>
<div class="m2"><p>زیرا که نرویدت تخم بی‌نم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آویخته از آسمان هفتم</p></div>
<div class="m2"><p>اینجا رسنی هست سخت محکم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن را نتوانی تو دید هرگز</p></div>
<div class="m2"><p>با خاطر تاریک و چشم یرتم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شو دست بدو در زن و جدا شو</p></div>
<div class="m2"><p>زین گم ره کاروان و بی‌شبان رم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>علم است مجسم، ندید هرگز</p></div>
<div class="m2"><p>کس علم به عالم جز از مجسم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آید به دلم کز خدا امین است</p></div>
<div class="m2"><p>بر حکمت لقمان و ملکت جم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مهمان و جراخوار قصر اویند</p></div>
<div class="m2"><p>با قیصر و خاقان امیر دیلم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در حشر مکرم بود کسی کو</p></div>
<div class="m2"><p>گشته‌است به اکرام او مکرم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر خلق مقدم شد او به حکمت</p></div>
<div class="m2"><p>با حکمت نیکو بود مقدم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>این دهر همه پشت و ملک او روی</p></div>
<div class="m2"><p>این خلق صفر جمله واو محرم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زو یافت جهان قدر و قیمت ایراک</p></div>
<div class="m2"><p>او شهره نگین است و دهر خاتم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>او داد مرا بر رمه شبانی</p></div>
<div class="m2"><p>زین می نروم با رمه رمارم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای تشنه تو را من رهی نمودم،</p></div>
<div class="m2"><p>گر مست نه‌ای سخت، زی لب یم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر تو بپذیری زمن نصیحت</p></div>
<div class="m2"><p>از چاه برآئی به چرخ اعظم</p></div></div>