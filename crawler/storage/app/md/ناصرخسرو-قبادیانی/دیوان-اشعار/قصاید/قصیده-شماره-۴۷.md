---
title: >-
    قصیدهٔ شمارهٔ ۴۷
---
# قصیدهٔ شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>نشنیده‌ای که زیر چناری کدوبنی</p></div>
<div class="m2"><p>بررُست و بردمید برو بر، به روز بیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرسید از آن چنار که «تو چند ساله‌ای؟»</p></div>
<div class="m2"><p>گفتا «دویست باشد و اکنون زیادتی است»</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خندید ازو کدو که «من از تو به بیست روز</p></div>
<div class="m2"><p>برتر شدم بگو تو که این کاهلی ز چیست»</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>او را چنار گفت که «امروز ای کدو</p></div>
<div class="m2"><p>با تو مرا هنوز نه هنگام داوری است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فردا که بر من و تو وزد باد مهرگان</p></div>
<div class="m2"><p>آنگه شود پدید که از ما دو، مرد کیست»</p></div></div>