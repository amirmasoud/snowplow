---
title: >-
    قصیدهٔ شمارهٔ ۱۰۶
---
# قصیدهٔ شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>بنالم به تو ای علیم قدیر</p></div>
<div class="m2"><p>از اهل خراسان صغیر و کبیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه کردم که از من رمیده شدند</p></div>
<div class="m2"><p>همه خویش و بیگانه بر خیر خیر؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مقرم به فرقان و پیغمبرت</p></div>
<div class="m2"><p>نه انباز گفتم تو را نه نظیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگفتم مگر راست، گفتم که نیست</p></div>
<div class="m2"><p>تو را در خدائی وزیر ای قدیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به امت رسانید پیغام تو</p></div>
<div class="m2"><p>رسولت محمد بشیر و نذیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قران را به پیغمبرت ناورید</p></div>
<div class="m2"><p>مگر جبرئیل آن مبارک سفیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مقرم به مرگ و به حشر و حساب</p></div>
<div class="m2"><p>کتابت ز بر دارم اندر ضمیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نخوردم برایشان به جان زینهار</p></div>
<div class="m2"><p>نجستم سپاه و کلاه و سریر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلیمان نیم، همچو دیوان ز من</p></div>
<div class="m2"><p>چرا شد رمیده کبیر و صغیر؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همان ناصرم من که خالی نبود</p></div>
<div class="m2"><p>زمن مجلس میر و صدر وزیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به نامم نخواندی کس از بس شرف</p></div>
<div class="m2"><p>ادیبم لقب بود و فاضل دبیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ادب را به من بود بازو قوی</p></div>
<div class="m2"><p>به من بود چشم کتابت قریر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به تحریر الفاظ من فخر کرد</p></div>
<div class="m2"><p>همی کاغذ از دست من بر حریر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دبیری یکی خرد فرزند بود</p></div>
<div class="m2"><p>نشد جز به الفاظ من سیر شیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دبیران اسیرند پیش سخن</p></div>
<div class="m2"><p>سخن پیش طبعم به طبع است اسیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر سیر کشتم همی بشکفید</p></div>
<div class="m2"><p>به اقبال من نرگس از تخم سیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا بود حاصل ز یاران خویش</p></div>
<div class="m2"><p>به شخص جوان اندرون عقل پیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کنون زان فزونم به هر فضل و علم</p></div>
<div class="m2"><p>که طبعم روان است و خاطر منیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بجای است در من به فضل خدای</p></div>
<div class="m2"><p>همان فهم و آن طبع معنی پذیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به چاه اندرون بودم آن روز من</p></div>
<div class="m2"><p>بر آوردم ایزد به چرخ اثیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از این قدر کامروز دارم به علم</p></div>
<div class="m2"><p>نبوده‌ستم آن روز عشر عشیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر آنگه به دنیا تنم شهره بود</p></div>
<div class="m2"><p>کنون بهترم چون به دینم شهیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر از خاک و از آب بودم، کنون</p></div>
<div class="m2"><p>گلابم شد آن آب و، خاکم عبیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کنون میر پیشم ندارد خطر</p></div>
<div class="m2"><p>گر آنگه خطر داشتم پیش میر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز دین‌اند پیشم به دنیا درون</p></div>
<div class="m2"><p>عزیزان ذلیل و خطیران حقیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر میر میر است و کامش رواست</p></div>
<div class="m2"><p>چنان که‌ش گمان است، گو شو ممیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کرا بانگ و نامش شود زیر خاک</p></div>
<div class="m2"><p>چه شادی کند خیره بر بانگ زیر؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه بایدت رغبت به شیره کنون</p></div>
<div class="m2"><p>که چون شیر گشته‌است بر سرت قیر؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گلی تازه بوده‌ستی، آری، ولیک</p></div>
<div class="m2"><p>شده‌ستی کنون پژمریده زریر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نیارد کنون تازگی باز تو</p></div>
<div class="m2"><p>نه خورشید تابان نه ابر مطیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یکی سرو بودی چو آهن قوی</p></div>
<div class="m2"><p>تو را سرو چنبر شد آهن خمیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هژیرت سخن باید، ای پیر، اگر</p></div>
<div class="m2"><p>نباشد، چه باک است، رویت هژیر؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو تیرت سخن باید ایرا که نیست</p></div>
<div class="m2"><p>گناه تو گر نیست قدت چو تیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدان منگر ای خواجه کز ظاهری</p></div>
<div class="m2"><p>نبینی همی مرد دین را ظهیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بصارت بیلفغد باید که تو</p></div>
<div class="m2"><p>ز خر به نه ای گر به چشمی بصیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بیاموز و ماموز مر عام را</p></div>
<div class="m2"><p>زعلم نهانی قلیل و کثیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به خوشهٔ قران در ببین دانه را</p></div>
<div class="m2"><p>به انگور دین در رها کن عصیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر از تو چو از من نفورست خلق</p></div>
<div class="m2"><p>تو را به، مکن هیچ بانگ و نفیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دلم پر ز درد است، جهال خلق</p></div>
<div class="m2"><p>زمن جمله زین‌اند دل پر زحیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگر عامه بد گویدم زان چه باک؟</p></div>
<div class="m2"><p>رها کرده‌ام پیش موشان پنیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نجنبد زجای،ای‌پسر،چون درخت</p></div>
<div class="m2"><p>به باد سحرگاه کوه ثبیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگر دیو بستد خراسان ز من</p></div>
<div class="m2"><p>گواه منی ای علیم قدیر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خراسانیان گر نجستند دین</p></div>
<div class="m2"><p>بتر زین که خودشان گرفتی مگیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به پیش ینال و تگین چون رهی</p></div>
<div class="m2"><p>دوانند یکسر غنی و فقیر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو عادند و ترکان چو باد عقیم</p></div>
<div class="m2"><p>بدین باد گشتند ریگ هبیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مثالی از امثال قرآن تو را</p></div>
<div class="m2"><p>نمودم نکو بنگر، ای تیز ویر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بیاویزد آن کس به غدر خدای</p></div>
<div class="m2"><p>که بگریزد از عهد روز غدیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چه گوئی به محشر اگر پرسدت</p></div>
<div class="m2"><p>از آن عهد محکم شبر با شبیر؟</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گر امروز غافل توی همچنین</p></div>
<div class="m2"><p>بر این درد فردا بمانی حسیر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>وگر پند گیری زحجت، به حشر</p></div>
<div class="m2"><p>تو را پند او بس بود دستگیر</p></div></div>