---
title: >-
    قصیدهٔ شمارهٔ ۱۶۰
---
# قصیدهٔ شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>این چه خلق و چه جهان است، ای کریم؟</p></div>
<div class="m2"><p>کز تو کس ر امی نبینم شرم و بیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راست کردند این خران سوگند تو</p></div>
<div class="m2"><p>پرکنی زینها کنون بی‌شک جحیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وان بهشتی با فراخی‌ی آسمان</p></div>
<div class="m2"><p>نیست آن از بهر اینها ای رحیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زانک ازینها خود تهی ماند بهشت</p></div>
<div class="m2"><p>ور به تنگی نیست نیم از چشم میم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر شب بی‌طاعتی فتنه است خلق</p></div>
<div class="m2"><p>کس نمی‌جوید ز صبح دین نسیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس نمی‌خرد رحیق و سلسبیل</p></div>
<div class="m2"><p>روی زی غسلین نهادند و حمیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از در مهلت نیند اینها ولیک</p></div>
<div class="m2"><p>تو، خدایا، هم کریمی هم حلیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای رحیم از توست قوت برحذر</p></div>
<div class="m2"><p>مر مرا از مکر شیطان رجیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من نگویم تو قدیم و محدثی</p></div>
<div class="m2"><p>کافریدهٔ توست محدث یا قدیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زاده و زاینده چون گوید کسیت؟</p></div>
<div class="m2"><p>هردو بندهٔ توست زاینده و عقیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در حریم خانهٔ پیغمبرت</p></div>
<div class="m2"><p>مر مرا از توست دو جهانی نعیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو سزائی گر بداری بنده را</p></div>
<div class="m2"><p>اندر این بی‌رنج و پرنعمت حریم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مر مرا غربت ز بهر دین توست</p></div>
<div class="m2"><p>وین سوی من بس عظیم است ای عظیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هم غریبم مرد باید، بی‌گمان</p></div>
<div class="m2"><p>بی‌رفیق و خویش و بی‌یار و ندیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در غریبی نان دستاسین و دوغ</p></div>
<div class="m2"><p>به چو در دوزخ ز قوم و خون و ریم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرکه را محنت نه جاویدی بود</p></div>
<div class="m2"><p>محنت او محنتی باشد سلیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر ندارم اسپ، خر بس مرکبم</p></div>
<div class="m2"><p>ور نیابم خز، درپوشم گلیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دام دیو است، ای کبل، بر پای و سر</p></div>
<div class="m2"><p>مر تو را دستار خیش و کفش ادیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من ز بهر دین شدم چون زر زرد</p></div>
<div class="m2"><p>تو ز دین ماندی چو سیم از بهر سیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از دروغ توست در جانم دریغ</p></div>
<div class="m2"><p>وز ستم توست ریشم پرستیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چند جوئی آنچه ندهندت همی؟</p></div>
<div class="m2"><p>چیز ناموجود کی جوید حکیم؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در مقام بی‌بقا ماندن مجوی</p></div>
<div class="m2"><p>تا نمانی در عذاب ایدون مقیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در ره عمری شتابان روز و شب</p></div>
<div class="m2"><p>ای برادر گر درستی یا سقیم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>می‌روی هموار و گوئی کایدرم</p></div>
<div class="m2"><p>مار می‌گیری که این ماهی است شیم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چشم داری ماه را تا نو شود</p></div>
<div class="m2"><p>تا بیابی از سپنجی سیم تیم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرگ را می‌جوئی و آگه نه‌ای</p></div>
<div class="m2"><p>من چنین نادان ندیدم، ای کریم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سال سی خفتی کنون بیدار شو</p></div>
<div class="m2"><p>گر نخفتی خواب اصحاب الرقیم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر تنت وام است جانت، گر چه دیر</p></div>
<div class="m2"><p>باز باید داد وام، ای بد غریم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جور بر بیوه و یتیم خود مکن</p></div>
<div class="m2"><p>ای ستم‌گر بر زن بیوه و یتیم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زان مقام اندیش کانجا همبرند</p></div>
<div class="m2"><p>با رعیت هم امیر و هم زعیم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از که دادت حجت این پند تمام؟</p></div>
<div class="m2"><p>از امام خلق عالم بوتمیم</p></div></div>