---
title: >-
    قصیدهٔ شمارهٔ ۱۷۳
---
# قصیدهٔ شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>ای تن تیره اگر شریفی اگر دون</p></div>
<div class="m2"><p>نبسهٔ گردونی و نبیرهٔ گردون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست به نسبت بس افتخار که هرگز</p></div>
<div class="m2"><p>نبسهٔ گردون دون نبود مگر دون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه شریف است همچو دون نه به ترکیب</p></div>
<div class="m2"><p>از رگ و موی است و استخوان و پی و خون؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو شریفی و بهتری تو ز خویشان</p></div>
<div class="m2"><p>چونکه بری سوی خویش خویش شبیخون؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلکه به جان است، نه به تن، شرف مرد</p></div>
<div class="m2"><p>نیست جسدها همه مگر گل مسنون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن صدف است ای پسر، به دین و به دانش</p></div>
<div class="m2"><p>جانت بپرور درو چو لؤلؤ مکنون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهرون از علم شد سمر به جهان در</p></div>
<div class="m2"><p>گر تو بیاموزی، ای پسر، تی اهرون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیک و بد و دیوی و فریشتگی را</p></div>
<div class="m2"><p>سوی خردمند هست مایه و قانون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مادر دیوان یکی فریشته بوده است</p></div>
<div class="m2"><p>فعل بدش کرد زشت و فاسق و ملعون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راه تو زی خیر و شر هر دو گشاده است</p></div>
<div class="m2"><p>خواهی ایدون گرای و خواهی ایدون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیو و فرشته به خاک و آب درون شد</p></div>
<div class="m2"><p>دیو مغیلان شد و فریشته زیتون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>داد کن ار نام نیک خواهی ازیراک</p></div>
<div class="m2"><p>نامور از داد گشت شهره فریدون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هزل ز کس مشنو و مگوی ازیراک</p></div>
<div class="m2"><p>عقل تو را دشمن است هزل، چو هپیون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چند بنالی که بد شده‌است زمانه؟</p></div>
<div class="m2"><p>عیب تنت بر زمانه برفگنی چون؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرگز کی گفت این زمانه که «بد کن»؟</p></div>
<div class="m2"><p>مفتون چونی به قول عامهٔ مفتون؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو شده‌ای دیگر، این زمانه همان است،</p></div>
<div class="m2"><p>کی شود ای بی‌خرد زمانه دگرگون؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دل به یقین ای پسر خزینهٔ دین است</p></div>
<div class="m2"><p>چشم تو چون روزن است و گوش چو پرهون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گوهر دین چون در این خزینه نهادی</p></div>
<div class="m2"><p>روزن و پرهون رو تو سخت کن اکنون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روزن و پرهون چو بسته گشت، خیانت</p></div>
<div class="m2"><p>راه نیابد بسوی گوهر مخزون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>منگر سوی حرام و جز حق مشنو</p></div>
<div class="m2"><p>تا نبرد دیو دزد سوی تو آهون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>توبه کن از هر بدی به تربیت دین</p></div>
<div class="m2"><p>جانت چو پیراهن است و توبه چو صابون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زنده به آبند زندگان که چنین گفت</p></div>
<div class="m2"><p>ایزد سبحان بی‌چگونه و بی‌چون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هرکه مر این آب را ندید، در این آب</p></div>
<div class="m2"><p>تشنه چو هاروت ماند غرقه چو ذوالنون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زنده نباشد حقیقت آنکه بمیرد</p></div>
<div class="m2"><p>گرچه به خاک اندرون نباشد مدفون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زنده ز ما ای پسر نه این تن خاکی است</p></div>
<div class="m2"><p>سوی پیامبر، نه نیز سوی فلاطون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بلکه ز ما زنده و شریف و سخن گوی</p></div>
<div class="m2"><p>نیست مگر جان بر خجسته و میمون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زنده به آب خدای خواهی گشتن</p></div>
<div class="m2"><p>نه تو به جیحون مرده و نه به سیحون</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر که بدین آب مرده زنده شد، او را</p></div>
<div class="m2"><p>زنده نخواند مگر که جاهل و مجنون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مردم اگر ز آب مرده زنده بماندی</p></div>
<div class="m2"><p>خلق نمردی هگرز برلب جیحون</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آب خدای آنکه مرده زنده بدو کرد</p></div>
<div class="m2"><p>آن پسر بی‌پدر برادر شمعون</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در دهن پاک خویش داشت مر آن را</p></div>
<div class="m2"><p>وز دهنش جز به دم نیامد بیرون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اصل سخنها دم است سوی خردمند</p></div>
<div class="m2"><p>معنی، باشد سخن به دم شده معجون</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر به فسون زنده کرد مرده مسیحا</p></div>
<div class="m2"><p>جز سخن خوب نیست سوی من، افسون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بنگر نیکو تو، از پی سخن، ادریس</p></div>
<div class="m2"><p>چون به مکان‌العلی رسید ز هامون</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر تو بیاموزی ای پسر سخن خوب</p></div>
<div class="m2"><p>خوار شود پیش تو خزانهٔ قارون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گرچه عزیز است زر زرت ندهد میر</p></div>
<div class="m2"><p>چون سخنت خوب و خوش نیامد و موزون</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گفتهٔ دانا چو ماه نو به فزون است</p></div>
<div class="m2"><p>گفتهٔ نادان چنان کهن شده عرجون</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فضل طبرخون نیافت سنجد هرگز</p></div>
<div class="m2"><p>گرچه زدیدن چو سنجد است طبرخون</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فضل سخن کی شناسد آنکه نداند</p></div>
<div class="m2"><p>فضل اساس و امام و حجت و ماذون؟</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>طبع تو ای حجت خراسان در زهد</p></div>
<div class="m2"><p>در همی درکشد به رشته همیدون</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون دلت از بلخ شد به یمگان خرسند</p></div>
<div class="m2"><p>پس چه فریدون به سوی تو چه فریغون</p></div></div>