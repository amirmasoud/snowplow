---
title: >-
    قصیدهٔ شمارهٔ ۵۴
---
# قصیدهٔ شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>مردم نبود صورت مردم حکما اند</p></div>
<div class="m2"><p>دیگر خس و خارند و قماشاتِ دغااند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینها که نیند از تو سزای که و کهدان</p></div>
<div class="m2"><p>مرحور وجنان راتو چه گوئی که سزااند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باندوه چرایند شب و روز بمانده</p></div>
<div class="m2"><p>از چون و چرا زانکه ستوران چرااند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این خیل چرا چویند و زخیل چراجوی</p></div>
<div class="m2"><p>این خلق بداندیش کزین گونه جرااند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عالم انسانی مردم چو نبات است</p></div>
<div class="m2"><p>اینها چون ریاحین‌اند آنها چو گیااند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دست شه اینها سپرغمند کماهی</p></div>
<div class="m2"><p>در پیش خر آنها چو گیاهند و غذااند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر تو سپر غمی شوی، این پور، به طاعت</p></div>
<div class="m2"><p>آنهات گزینند که بر ما امرااند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دانا بر من کیست جز آنها که در امت</p></div>
<div class="m2"><p>خیرالبشراند و خلف اهل عبااند؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایشان که به فرمان خدا از پدر و جد</p></div>
<div class="m2"><p>میمون خلفااند و بر امت خلفااند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنها که به تایید الهی به ره دین</p></div>
<div class="m2"><p>اندر شب گم راهی اجرام سمااند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنها که مرایشان را اندر شرف و فضل</p></div>
<div class="m2"><p>مردان و زنان جمله عبیداند و امااند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنها که به تقدیر جهان داور ما را</p></div>
<div class="m2"><p>از درد جهالت به نکو پند شفااند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنها که جهان را به چراغی که خداوند</p></div>
<div class="m2"><p>بفروختش اندر شب دین روی ضیااند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنها که گوااند بر این خلق و برایشان</p></div>
<div class="m2"><p>زایزد پدر و جد بحق عدل گوااند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنها که زپاکیزه نسب شیعت خود را</p></div>
<div class="m2"><p>از حوض جد خویش و نیا آب سقااند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنها که گه حمله به تایید الهی</p></div>
<div class="m2"><p>چون ما ز ستوران چراینده جدااند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنها که بریشان ما را همه هموار</p></div>
<div class="m2"><p>میراث نیائیم که میراث نیااند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آنها که چو محراب شریفند و مقدم</p></div>
<div class="m2"><p>دیگر به صفا جمله وضیعند و ورااند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حجاج و کریمان و حکیمان جهانند</p></div>
<div class="m2"><p>ویشان به ره حکمت قبلهٔ حکمااند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کعبهٔ شرف و علم خفیات کتاب است</p></div>
<div class="m2"><p>ویشان به مثل کعبهٔ رکن‌اند و صفااند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زیشان به هر اقلیم یکی تند زبانی است</p></div>
<div class="m2"><p>گویا به صلاح گرهی کز صلحااند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر اهل ولا ابر صلاحند و بر آنهاک</p></div>
<div class="m2"><p>نه اهل ولااند مثل باد بلااند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کوهی است به هر کشور از ایشان که از این خلق</p></div>
<div class="m2"><p>آنها که نبینند نه از اهل ولااند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کوهی که برو چشمهٔ پاک آب حیات است</p></div>
<div class="m2"><p>نخچیر درو مؤمن و کبگان علمااند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کوهی است به یمگان که بینند گروهیش</p></div>
<div class="m2"><p>کز چشم حقیقت سپر سر صفااند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کوهی که درو نور الهی است جواهر</p></div>
<div class="m2"><p>آنها که همی جویند جوهر به کجااند؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زین گوهر باقی نکند هیچ کسی قصد</p></div>
<div class="m2"><p>کز کوردلی شیفته برادر فنااند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آن است مرا کز دل با من به مرا نیست</p></div>
<div class="m2"><p>آنها نه مرااند که با من به مرااند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در گرد دل من به مرا هرگز ره نیست</p></div>
<div class="m2"><p>پاکیزه که بی‌هیچ مرااند مرااند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مر گوهر با قیمت و با فضل و بها را</p></div>
<div class="m2"><p>اینها نه سزااند که بی‌قدر و بهااند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از عدل و صواب است بقا زاده و اینها</p></div>
<div class="m2"><p>نه اهل بقااند که بر جور و خطااند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پشه ز چه یک روز زید، پیل دوصد سال؟</p></div>
<div class="m2"><p>زیرا ز پشه پیلان در رنج و عنااند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عدلی است عطا ز ایزد ما را و ز دوزخ</p></div>
<div class="m2"><p>آنند رها کز در این شهره عطااند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر عادلی از طاعت بگزار حق وقت</p></div>
<div class="m2"><p>بنگر به بصیرت که در این‌جا بصرااند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وانها که ندانند به طاعت حق روزی</p></div>
<div class="m2"><p>بر جور و جفااند نه بر عدل و وفااند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یارب، چه شد آن خلق که بر آل پیمبر</p></div>
<div class="m2"><p>چون کژدم و مارند و چو گرگان و قلااند؟</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اینها که همی دشمن اولاد رسولند</p></div>
<div class="m2"><p>از مادر اگر هرگز نایند روااند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دانم که رها یابد از دوزخ ابلیس</p></div>
<div class="m2"><p>گر ز آتش این قوم بدین فعل رهااند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دانم که بدین فعل که می‌بینم هر چند</p></div>
<div class="m2"><p>گویند تو راایم حقیقت نه تورااند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آنها که تورااند ز فعل بد اینها</p></div>
<div class="m2"><p>درمانده و دل خسته و با درد و بکااند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دانند که در عالم دین شهره لوائی است</p></div>
<div class="m2"><p>پنهان شده در سایهٔ این شهره لوااند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آن شمس که روزیش برآری تو زمغرب</p></div>
<div class="m2"><p>از فضل تو خواهنده مرو را به دعااند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا جای پدر باز ستانند ز دیوان</p></div>
<div class="m2"><p>اینها که سزای صلوات‌اند و ثنااند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای امت برگشته ز اولاد پیمبر</p></div>
<div class="m2"><p>اولاد پیمبر حکم روز قضااند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>این قوم که این راه نمودند شما را</p></div>
<div class="m2"><p>زی آتش جاوید دلیلان شمااند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>این رشوت خواران فقهااند شما را</p></div>
<div class="m2"><p>ابلیس فقیه است گر اینها فقهااند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از بهر قضا خواشتن و خوردن رشوت</p></div>
<div class="m2"><p>فتنه همگان بر کتب بیع و شرااند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>رشوت بخورند آنگه رخصت بدهندت</p></div>
<div class="m2"><p>نه اهل قضااند بل از اهل قفااند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بر من ز شما نیست سفاهت عجب ایرا</p></div>
<div class="m2"><p>آنند که در دین فقهااند سفهااند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گر احمد مرسل پدر امت خویش است</p></div>
<div class="m2"><p>جز شیعت و فرزند وی اولاد زنااند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ما بر اثر عترت پیغمبر خویشیم</p></div>
<div class="m2"><p>و اولاد زنا بر اثر رای و هوااند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اسلام ردائی ز رسول است و، امامان</p></div>
<div class="m2"><p>از عترت او، حافظ این شهره ردااند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>آنان که فلان است و فلان زمرهٔ ایشان</p></div>
<div class="m2"><p>نزدیک حکیمان زدر عیب و هجااند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ما را چو کند پیر چه گوئیم که رهبر</p></div>
<div class="m2"><p>در دین حق از عترت پیغمبر مااند؟</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ای حجت، می‌گوی سخنهای به حجت</p></div>
<div class="m2"><p>زیرا که صبائی تو و خصمانت هبااند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>موسی زمان را تو یکی شهره عصائی</p></div>
<div class="m2"><p>وانکه نشناسند که خصمان عقلااند</p></div></div>