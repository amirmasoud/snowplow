---
title: >-
    قصیدهٔ شمارهٔ ۲۷۰
---
# قصیدهٔ شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>بینی آن باد که گوئی دم یارستی</p></div>
<div class="m2"><p>یاش بر تبت و خرخیز گذارستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیستی چون سخن یار موافق خوش</p></div>
<div class="m2"><p>گر نه او پیش رو فوج بهارستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نبودی شده ایمن دل بید از باد</p></div>
<div class="m2"><p>برگش از شاخ برون جست نیارستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور نه می لشکر نوروز فراز آید</p></div>
<div class="m2"><p>کی هوا یکسره پر گرد و غبارستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فوج فوج ابر همی آید پنداری</p></div>
<div class="m2"><p>بر سر دریا اشتر به قطارستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشترانند بر این چرخ روان ور نی</p></div>
<div class="m2"><p>دشت همواره نه چون پیسه مهارستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه همانا که بر این اشتر نوروزی</p></div>
<div class="m2"><p>جز که کافور و در و گوهر بارستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دشت گلگون شد گوئی که پرندستی</p></div>
<div class="m2"><p>آب میگون شد گوئی که عقارستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرنه می می‌خوردی نرگس‌تر از جوی</p></div>
<div class="m2"><p>چشم او هرگز پر خواب و خمارستی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>واتش اندر دل خاک ار نزدی نوروز</p></div>
<div class="m2"><p>کی هوا ایدون پر دود و بخارستی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاخ گل گر نکشیدی ستم از بهمن</p></div>
<div class="m2"><p>نه چینن زرد و نوان و نه نزارستی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای به نوروز شده همچو خران فتنه</p></div>
<div class="m2"><p>من نخواهم که مرا همچو تو یارستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گوئی «امسال تهی دست چه دانم کرد؟»</p></div>
<div class="m2"><p>کاشک امسال تو را کار چو پارستی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دلم از تو به همه حال بشستی دست</p></div>
<div class="m2"><p>گر تو را در خور دل دست گزارستی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فتنهٔ سبزه شدت دل چو خر، ای بیهش</p></div>
<div class="m2"><p>فتنه سبزه نشدی گر نه حمارستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیست فرقی به میان تو و آن خر</p></div>
<div class="m2"><p>جز همی باید که‌ت پای چهارستی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سیرتی بهتر از این یافتیی بی‌شک</p></div>
<div class="m2"><p>گرت ننگستی از این سیرت و عارستی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر گل حکمت بر جان تو بشکفتی</p></div>
<div class="m2"><p>مر تو را باغ بهاری چه بکارستی؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مجلست بستانستی و رفیقان را</p></div>
<div class="m2"><p>از درخت سخن خوب ثمارستی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وین گل و لالهٔ خاکی که همی روید</p></div>
<div class="m2"><p>با گل دانش پیشت خس و خارستی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پیش گلزار سخن‌های حکیمانه‌ت</p></div>
<div class="m2"><p>کار لاله بد و کار گل زارستی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مردم آن است که چون مرد ورا بیند</p></div>
<div class="m2"><p>گوید «ای کاش که‌م این صاحب غارستی»</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فضل بایدش و خرد بار که خرما بن</p></div>
<div class="m2"><p>گر نه بار آوردی یار چنارستی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خرد است آنکه اگر نور چراغ او</p></div>
<div class="m2"><p>نیستی عالم یکسر شب تارستی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خرد است آنکه اگر نیستی او از ما</p></div>
<div class="m2"><p>نه صغارستی هرگز نه کبارستی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر نبوده‌ستی این عقل به مردم در</p></div>
<div class="m2"><p>خلق یکسر بتر از کژدم و مارستی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو چه گوئی که اگر عقل نبوده‌ستی</p></div>
<div class="m2"><p>یک تن از مردم سالار هزارستی؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ورنه با عقل همی جهل جفا جستی</p></div>
<div class="m2"><p>گرد دانا جهلا را چه مدارستی؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سر به جهل از خرد و حق همی تابد</p></div>
<div class="m2"><p>آنکه حق است که بر سرش فسارستی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یله کی کردی هر فاحشه را جاهل</p></div>
<div class="m2"><p>گر نه از بیم حد و کشتن و دارستی؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آنکه طبع یله کردی به خوشی هرگز</p></div>
<div class="m2"><p>معصفر گونه و نیروی شخارستی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ای دهان باز نهاده به جفای من</p></div>
<div class="m2"><p>راست گوئی که یکی کهنه تغارستی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چند گوئی که «از آن تنگ دره حجت</p></div>
<div class="m2"><p>هم برون آیدی ار نیک سوارستی» ؟</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اندر این تنگ حصارم ننشستی دل</p></div>
<div class="m2"><p>گرنه گرد دلم از عقل حصارستی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کار تو گر به میان من و تو ناظر</p></div>
<div class="m2"><p>حاکمی عادل بودی بس خوارستی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کار دنیا گر بر موجب عقلستی</p></div>
<div class="m2"><p>مر مرا خیره درین کنج چه کارستی؟</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بل سخن‌های دلاویز بلند من</p></div>
<div class="m2"><p>بر سر گنبد گردنده عذارستی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ور سخن‌هام فلاطون بشنوده‌ستی</p></div>
<div class="m2"><p>پیش من حیران چون نقش جدارستی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یوز و باز سخن و نکته‌م را بی‌شک</p></div>
<div class="m2"><p>دل دانای سخن پیشه شکارستی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دهر پر عیبم همچون که تو بگزیدی</p></div>
<div class="m2"><p>گر مرا تن چو تو پر عیب و عوارستی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مر مرا گر پس دانش نشده‌ستی دل</p></div>
<div class="m2"><p>همچو تو اسپ و غلامان و عقارستی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بی‌شمارستی مال و خدم و ملکم</p></div>
<div class="m2"><p>گر نه بیمم همه از روز شمارستی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بی‌قرارستی جانم چو تو در کوشش</p></div>
<div class="m2"><p>گر بدانستی کاین جای قرارستی</p></div></div>