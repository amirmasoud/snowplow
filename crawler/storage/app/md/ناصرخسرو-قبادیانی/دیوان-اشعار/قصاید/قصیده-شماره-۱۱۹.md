---
title: >-
    قصیدهٔ شمارهٔ ۱۱۹
---
# قصیدهٔ شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>ای بستهٔ خود کرده دل خلق به ناموس</p></div>
<div class="m2"><p>ز اندیشه تو را رفته به هر جانب جاسوس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اثبات یقین تو به معقول چه سود است،</p></div>
<div class="m2"><p>چون نیست یقین نفی گمان تو به محسوس؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چند سخن گوئی از حق و حقیقت؟</p></div>
<div class="m2"><p>آب حیوان جوئی در چشمهٔ مطموس!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر رای تو کفر است مکن پیدا ایمان</p></div>
<div class="m2"><p>ور جای تو دیر است مزن پنهان ناقوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای آنکه همه زرقی در فعل چو روباه،</p></div>
<div class="m2"><p>وی آنکه همه رنگی در وصف چو طاووس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کی روی آخر ز پی حج به زیارت</p></div>
<div class="m2"><p>از طوس سوی مکه، وز مکه سوی طوس؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نیست ز کان علت مقصود، پس ای دوست</p></div>
<div class="m2"><p>چه مکه و چه کعبه و چه طوس و چه طرطوس</p></div></div>