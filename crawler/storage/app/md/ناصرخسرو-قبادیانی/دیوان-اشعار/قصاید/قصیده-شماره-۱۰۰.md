---
title: >-
    قصیدهٔ شمارهٔ ۱۰۰
---
# قصیدهٔ شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>اصل نفع و ضر و مایهٔ خوب و زشت و خیر و شر</p></div>
<div class="m2"><p>نیست سوی مرد دانا در دو عالم جز بشر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اصل شر است این حشر کز بوالبشر زاد و فساد</p></div>
<div class="m2"><p>جز فساد و شر هرگز کی بود کار حشر؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیر و شر آن جهان از بهر او شد ساخته</p></div>
<div class="m2"><p>زانک ازو آید به ایمان و به عصیان خیر و شر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای برادر، چشم من زینها و زین عالم همی</p></div>
<div class="m2"><p>لشکری انبوه بیند بر رهی پر جوی و جر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز شکسته بسته بیرون چون تواند شد چو بود</p></div>
<div class="m2"><p>مرد مست و چشم کور و پای لنگ و راه تر؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نه‌ای مست از ره مستان و شر و شورشان</p></div>
<div class="m2"><p>دورتر شو تا بسر درناید اسپت، ای پسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نخواهی رنج گر از گرگنان پرهیز کن</p></div>
<div class="m2"><p>جهل گر است ای پسر پرهیز کن زین زشت‌گر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهل را گرچه بپوشی خویشتن رسوا کند</p></div>
<div class="m2"><p>گر چه پوشیده بماند گر جهل از گر بتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیستی مردم تو بل خر مردمی، زیرا که من</p></div>
<div class="m2"><p>صورت مردم همی بینم تو را و فعل خر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز کم آزاری نباشد مردمی، گر مردمی</p></div>
<div class="m2"><p>چون بیازاری مرا؟ یا نیستی مردم مگر؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرگ درنده ندرد در بیابان گرگ را</p></div>
<div class="m2"><p>گر همی دعوی کنی در مردمی مردم مدر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نفع و ضر و خیر و شر از کارهای مردم است</p></div>
<div class="m2"><p>پس تو چون بی‌نفع و خیری بل همه شری و ضر؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تن به جر گیرد همی مر جانت را در جر کشد</p></div>
<div class="m2"><p>جان به جر اندر بماند چونش گیرد تن به جر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیش جان تو سپر کرده‌است یزدان تنت را</p></div>
<div class="m2"><p>تو چرا جان را همی داری به پیش تن سپر؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خواب و خور کار تن تیره است، تو مر جانت را</p></div>
<div class="m2"><p>چون کنی رنجه چو گاو و خر ز بهر خواب و خور؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مردمان از تو بخندند، ای برادر، بی گمان</p></div>
<div class="m2"><p>چون پلاس ژنده را سازی زدیبا آستر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر شکر خوردی پریرو، دی یکی نان جوین</p></div>
<div class="m2"><p>همبر است امروز ناچار آن جوین با آن شکر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>داد تن دادی، بده جان را به دانش داد او</p></div>
<div class="m2"><p>یافت از تو تن بطر در کار جانت کن نظر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جانت آزادی نیابد جز به علم از بندگی</p></div>
<div class="m2"><p>گر بدین برهانت باید، شو به دین اندر نگر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مردم دانا مسلمان است، نفروشدش کس</p></div>
<div class="m2"><p>مردم نادان اگر خواهی ز نخاسان بخر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تن به جان یابد خطر زیرا که تن زنده بدوست</p></div>
<div class="m2"><p>جان به دانش زنده ماند زان بدون یابد خطر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جان مردم را دو قوت بینم از علم و عمل</p></div>
<div class="m2"><p>چون درختی که‌ش عمل برگ است و از علم است بر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جانت را دانش نگه دارد زدوزخ همچنانک</p></div>
<div class="m2"><p>بر نگه دارد درختان را از آتش وز تبر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر نتابی سر ز دانش از تو تابد آفتاب</p></div>
<div class="m2"><p>وز سعادت، ای پسر، بر آسمان سایدت سر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مر تو را بر آسمان باید شدن، زیرا خدای</p></div>
<div class="m2"><p>می نخواند جز تو را نزدیک خویش از جانور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر فلک بی‌پا و پر دانی که نتوانی شدن</p></div>
<div class="m2"><p>پس چرا بر ناوری از دین و دانش پای و پر؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از حریصی‌ی کار دنیا می‌نپردازی همی</p></div>
<div class="m2"><p>خانه بس تنگ است و تاری می‌نبینی راه در</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خاک را بر زر گزیده‌ستی چو نادانان ازانک</p></div>
<div class="m2"><p>خاک پیش توست و زر را می نیابی جز خبر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همچو کرم سرکه‌ای ناگه زشیرین انگبین</p></div>
<div class="m2"><p>با خرد چون کرم چون گشتی به بیهوشی سمر؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بس ترش و تنگ جای است این ازیرا مر تو را</p></div>
<div class="m2"><p>خم سرکه است این جهان، بنگر به عقل، ای بی‌بصر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جانت را اندر تن خاکی به دانش زر کن</p></div>
<div class="m2"><p>چون همی ناید برون هرگز مگر کز خاک زر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همچنان کاندر جهان آتش نسوزد زر همی</p></div>
<div class="m2"><p>زر جانت را نسوزد آتش سوزان سقر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ره گذار است این جهان ما را، بدو دل در مبند</p></div>
<div class="m2"><p>دل نبندد هوشیار اندر سرای ره‌گذر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زیر پای روزگار اندر بماندم شصت سال</p></div>
<div class="m2"><p>تا به زیر پای بسپردم سر، این مردم سپر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دست و پایم خشک بسته است این جهان بی دست و پای</p></div>
<div class="m2"><p>زیب و فرم پاک برده‌است اینچنین بی زیب و فر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نیستم با چرخ گردان هیچ نسبت جز بدانک</p></div>
<div class="m2"><p>همچو خود بینم همی او را مقیم اندر سفر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کار من گفتار خوب و، رای علم و طاعت است</p></div>
<div class="m2"><p>کار این دولاب گشتن گاه زیر و گه زبر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نیستم فرزند او زیرا که من زو بهترم</p></div>
<div class="m2"><p>جانور فرزند ناید هرگز از بی‌جان پدر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نیست جز دولاب گردون چون به گشتن‌های خویش</p></div>
<div class="m2"><p>آب ریزد بر زمین می تا بروید زو شجر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>وانگهی پیداست چون زو فایده جمله توراست</p></div>
<div class="m2"><p>کاین ز بهر تو همی گردد چنین بی‌حد و مر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مردم از ترکیب نیکو خود جهانی دیگر است</p></div>
<div class="m2"><p>مختصر، لیکن سخن‌گوی است و هم تدبیر گر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پس همی بینی که جز از بهر ما یزدان ما</p></div>
<div class="m2"><p>نافریده‌است این جهان را، ای جهان مختصر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تن تو را گور است بی‌شک، مر تو را پس وعده کرد</p></div>
<div class="m2"><p>روزی از گورت برون آرد خدای دادگر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تنت همچون گور خاک است، ای پسر، مپسند هیچ</p></div>
<div class="m2"><p>جانت را در خاک تیره جاودانه مستقر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خاک تیره بد مقر است، ای برادر، شکر کن</p></div>
<div class="m2"><p>ایزدت را تا برون آردت از این تیره مقر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>انچه گفتم یاد گیر و آنچه بنمودم ببین</p></div>
<div class="m2"><p>ور نه همچون کور و کر عامه بمانی کور و کر</p></div></div>