---
title: >-
    قصیدهٔ شمارهٔ ۱۵۳
---
# قصیدهٔ شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>گر مستمند و با دل غمگینم</p></div>
<div class="m2"><p>خیره مکن ملامت چندینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیرا که تا به صبح شب دوشین</p></div>
<div class="m2"><p>بیدار داشت بادک نوشینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حیران و دل شکسته چنین امروز</p></div>
<div class="m2"><p>از رنج وز تفکر دوشینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنهار ظن مبر که چنین مسکین</p></div>
<div class="m2"><p>اندر فراق زلفک مشکینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا ز انده و غم الفی سیمین</p></div>
<div class="m2"><p>ایدون چنین چو نونی زرینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نسرین زنخ صنم چه کنم اکنون</p></div>
<div class="m2"><p>کز عارضین چو خوشهٔ نسرینم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بل روز و شب به قولی پوشیده</p></div>
<div class="m2"><p>پندی همی دهند به هر حینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آئین این دو مرغ در این گنبد</p></div>
<div class="m2"><p>پریدن و شتاب همی بینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس من به زیر پر دو مرغ اندر</p></div>
<div class="m2"><p>ظن چون بری که ساکن بنشینم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در مسکنی که هیچ نفرساید</p></div>
<div class="m2"><p>فرسوده گشت هیکل مسکینم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در لشکر زمانه بسی گشتم</p></div>
<div class="m2"><p>پر گرد ازین شده است ریاحینم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از دیدن دگر دگر آئینش</p></div>
<div class="m2"><p>دیگر شده‌است یکسره آئینم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بازی گری است این فلک گردان</p></div>
<div class="m2"><p>امروز کرد تابعه تلقینم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زیرا که دی به جلوه برون آورد</p></div>
<div class="m2"><p>آراسته به حلهٔ رنگینم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر بستر جهالت و آگنده</p></div>
<div class="m2"><p>یکسر به خواب غفلت بالینم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>و امروز باز پاک ز من بربود</p></div>
<div class="m2"><p>آن حلهای خوب و نوآئینم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکچند پیشگاه همی دیدی</p></div>
<div class="m2"><p>در مجلس ملوک و سلاطینم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آزرده این و آن به حذر از من</p></div>
<div class="m2"><p>گفتی مگر نژادهٔ تنینم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آهو خجل ز مرکب رهوارم</p></div>
<div class="m2"><p>طاووس زشت پیش نمد زینم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>واکنون ز گشت دهر دگر گشتم</p></div>
<div class="m2"><p>گوئی نه آن سرشت و نه آن طینم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زین گونه کرد با من بازی‌ها</p></div>
<div class="m2"><p>پرکین دل از جفای فلک زینم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>واکنون که چون شناختمش زین پس</p></div>
<div class="m2"><p>برگردم و ازو بکشم کینم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نندیشم از ملوک و سلاطینش</p></div>
<div class="m2"><p>دیگر کنم رسوم و قوانینم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>با زخم دیو دنیا بس باشد</p></div>
<div class="m2"><p>پرهیز جوشن و زرهم دینم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سلطان بس است بر فلک جافی</p></div>
<div class="m2"><p>فخر تبار طاها و یاسینم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>«مستنصر از خدای» دهد نصرت</p></div>
<div class="m2"><p>زین پس بر اولیای شیاطینم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ارجو که باز بنده شود پیشم</p></div>
<div class="m2"><p>آن بی‌وفا زمانهٔ پیشینم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مجلس به فر دولت او فردا</p></div>
<div class="m2"><p>جز در کنار حورا نگزینم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خورشید پیشکار و قمر ساقی</p></div>
<div class="m2"><p>لاله سماک و نرگس پروینم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>منگر بدان که در درهٔ یمگان</p></div>
<div class="m2"><p>محبوس کرده‌اند مجانینم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مغلوب گشت از اول ازاین دیوان</p></div>
<div class="m2"><p>نوح رسول، من نه نخستینم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فخرم بس آنکه در ره دین حق</p></div>
<div class="m2"><p>بر مذهب امام میامینم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر حب آل احمد شاید گر</p></div>
<div class="m2"><p>لعنت همی کنند ملاعینم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر اهل آفرین نیمی هرگز</p></div>
<div class="m2"><p>جهال چون کنندی نفرینم؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از جان پاک رفته به علیین</p></div>
<div class="m2"><p>وز جسم تیره مانده به سجینم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شاید اگر ز جسم به زندانم</p></div>
<div class="m2"><p>کز علم دین شکفته بساتینم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سقراط اگر به رجعت باز آید</p></div>
<div class="m2"><p>عشری گمان‌بریش ز عشرینم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بازی است پیش حکمت یونانم</p></div>
<div class="m2"><p>زیرا که ترجمان طواسینم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر ناصبی مثل مگسی گردد</p></div>
<div class="m2"><p>بگذشت نارد از سر عرنینم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون من سخن به شاهین برسنجم</p></div>
<div class="m2"><p>آفاق و انفس‌اند موازینم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نپسندم ار بگردد و بگراید</p></div>
<div class="m2"><p>بر ذره‌ای زبانهٔ شاهینم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زیرا که بر گرفت به دست عقل</p></div>
<div class="m2"><p>ایزد غشاوت از دو جهان بینم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زی جوهری علوی رهبر گشت</p></div>
<div class="m2"><p>این جوهر کثیف فرودینم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زانم به عقل صافی کاندر دین</p></div>
<div class="m2"><p>بر سیرت مبارز صفینم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نزدیک عاقلان عسل النحلم</p></div>
<div class="m2"><p>واندر گلوی جاهل غسلینم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از من چو خر ز شیر مرم چندین</p></div>
<div class="m2"><p>ساکن سخن شنو که نه سنگینم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>افسانها به من بر چون بندی</p></div>
<div class="m2"><p>گوئی که من به چین و به ماچینم؟</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بر من گذر یکی که به یمگان در</p></div>
<div class="m2"><p>مشهورتر از آذر برزینم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شهد و طبرزدم ز ره معنی</p></div>
<div class="m2"><p>گرچه به نام تیغ و تبرزینم</p></div></div>