---
title: >-
    قصیدهٔ شمارهٔ ۹۴
---
# قصیدهٔ شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>برکن زخواب غفلت پورا سر</p></div>
<div class="m2"><p>واندر جهان به چشم خرد بنگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار خر است خواب و خور ای نادان</p></div>
<div class="m2"><p>با خر به خواب و خور چه شدی در خور؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایزد خرد ز بهر چه داده‌ستت؟</p></div>
<div class="m2"><p>تا خوش بخسپی و بخوری چون خر؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر نه به سر کلاه خرد وانگه</p></div>
<div class="m2"><p>بر کن به شب یکی سوی گردون سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوئی که سبز دریا موجی زد</p></div>
<div class="m2"><p>وز قعر برفگند به سر گوهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیره شب و ستاره درو، گوئی</p></div>
<div class="m2"><p>در ظلمت است لشکر اسکندر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پروین چو هفت خواهر چون دایم</p></div>
<div class="m2"><p>بنشسته‌اند پهلوی یک دیگر؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون است زهره چون رخ ترسنده</p></div>
<div class="m2"><p>مریخ همچو دیدهٔ شیر نر؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شعری چو سیم خود شد، یا خود شد</p></div>
<div class="m2"><p>عیوق چون عقیق چنان احمر؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر مبرم کبود چنین هر شب</p></div>
<div class="m2"><p>چندین هزار چون شکفد عبهر؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گوئی که در زدند هزاران جای</p></div>
<div class="m2"><p>آتش به گرد خرمن نیلوفر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر آتش است چون که در این خرمن</p></div>
<div class="m2"><p>هرگز فزون نگشت و نشد کمتر؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی‌روغن و فتیله و بی‌هیزم</p></div>
<div class="m2"><p>هرگز نداد نورو فروغ آذر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر آتش آن بود که خورش خواهد</p></div>
<div class="m2"><p>آتش نباشد آنکه نخواهد خور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بنگر که از بلور برون آید</p></div>
<div class="m2"><p>آتش همی به نور و شعاع خور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خورشید صانع است مر آتش را</p></div>
<div class="m2"><p>بشناس از آتش ای پسر آتش‌گر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ور لشکری است این که همی بینی</p></div>
<div class="m2"><p>سالار و میر کیست بر این لشکر؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سقراط هفت میر نهاد این را</p></div>
<div class="m2"><p>تدبیر ساز و کارکن و رهبر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سبز است ماه و گفت کزو روید</p></div>
<div class="m2"><p>در خاک ملح و، سیم به سنگ اندر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مریخ زاید آهن بد خو را</p></div>
<div class="m2"><p>وز آفتاب گفت که زاید زر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برجیس گفت مادر ارزیز است</p></div>
<div class="m2"><p>مس را همیشه زهره بود مادر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سیماب دختر است عطارد را</p></div>
<div class="m2"><p>کیوان چو مادر است و سرب دختر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این هفت گوهران گدازان را</p></div>
<div class="m2"><p>سقراط باز بست به هفت اختر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر قول این حکیم درست آید</p></div>
<div class="m2"><p>با او مرا بس است خرد داور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زیرا که جمله پیشه‌وران باشند</p></div>
<div class="m2"><p>اینها به کار خویش درون مضطر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سالار کیست پس چو از این هفتان</p></div>
<div class="m2"><p>هر یک موکل است به کاری بر؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سالار پیشه‌ور نبود هرگز</p></div>
<div class="m2"><p>بل پیشه‌ور رهی بود و چاکر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آن است پادشا که پدید آورد</p></div>
<div class="m2"><p>این اختران و این فلک اخضر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>واندر هوا به امر وی استاده است</p></div>
<div class="m2"><p>بی‌دار و بند پایهٔ بحر و بر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وایدون به امر او شد و تقدیرش</p></div>
<div class="m2"><p>با خاک خشک ساخته آب تر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چندین همی به قدرت او گردد</p></div>
<div class="m2"><p>این آسیای تیز رو بی در</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وین خاک خشک زشت بدو گیرد</p></div>
<div class="m2"><p>چندین هزار زینت و زیب و فر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وین هر چهار خواهر زاینده</p></div>
<div class="m2"><p>با بچگان بی‌عدد و بی مر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تسبیح می‌کنندش پیوسته</p></div>
<div class="m2"><p>در زیر این کبود و تنک چادر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تسبیح هفت چرخ شنوده‌ستی</p></div>
<div class="m2"><p>گر نیست گشته گوش ضمیرت کر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دست خدای اگر نگرفته‌ستی</p></div>
<div class="m2"><p>حسرت خوری بسی و بری کیفر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چشمیت می‌بباید و گوشی نو</p></div>
<div class="m2"><p>از بهر دیدن ملک اکبر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آنجا به پیش خود ندهد بارت</p></div>
<div class="m2"><p>گر چشم و گوش تو نبری زایدر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ایزد بر آسمانت همی خواند</p></div>
<div class="m2"><p>تو خویشتن چرا فگنی در جر؟</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از بهر بر شدن سوی علیین</p></div>
<div class="m2"><p>از علم پای ساز و، ز طاعت پر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ای کوفته مفازهٔ بی‌باکی</p></div>
<div class="m2"><p>فربه شده به جسم و، به جان لاغر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در گردن جهان فریبنده</p></div>
<div class="m2"><p>کرده دو دست و بازوی خود چنبر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ایدون گمان بری که گرفته‌ستی</p></div>
<div class="m2"><p>دربر به مهر، خوب یکی دلبر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>واگاه نیستی که یکی افعی</p></div>
<div class="m2"><p>داری گرفته تنگ و خوش اندر بر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر خویشتن کشی ز جهان، ورنی</p></div>
<div class="m2"><p>بر تو به کینه او بکشد خنجر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زین بی‌وفا، وفا چه طمع داری؟</p></div>
<div class="m2"><p>چون در دمی به بیخته خاکستر؟</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون تو بسی به بحر درافگنده است</p></div>
<div class="m2"><p>این صعب دیو جاهل بدمحضر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>وز خلق چون تو غرقه بسی کرده‌است</p></div>
<div class="m2"><p>این بحر بی‌کرانهٔ بی‌معبر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گریست این جهان به مثل، زیرا</p></div>
<div class="m2"><p>بس ناخوش است و، خوش بخارد گر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>با طبع ساز باشد، پنداری</p></div>
<div class="m2"><p>شیری است تازه، پخته و پر شکر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>لیکن چو کرد قصد جفا، پیشش</p></div>
<div class="m2"><p>خاقان خطر ندارد و نه قیصر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گاهی عروس‌وارت پیش آید</p></div>
<div class="m2"><p>با گوشوار و یاره و با افسر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>باصد کرشمه بسترد از رویت</p></div>
<div class="m2"><p>با شرم گرد باستی و معجر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گاهی هزبروار برون آید</p></div>
<div class="m2"><p>با خشم عمرو و با شغب عنتر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دیوانه‌وار راست کند ناگه</p></div>
<div class="m2"><p>خنجر به سوی سینه‌ت و، زی حنجر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در حرب این زمانهٔ دیوانه</p></div>
<div class="m2"><p>از صبر ساز تیغ و، ز دین مغفر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>وز شاخ دین شکوفهٔ دانش چن</p></div>
<div class="m2"><p>وز دشت علم سنبل طاعت چر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کاین نیست مستقر خردمندان</p></div>
<div class="m2"><p>بلک این گذرگهی است، برو بگذر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شاخی که بار او نبود ما را</p></div>
<div class="m2"><p>آن شاخ پس چه بی‌برو چه برور</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دنیا خطر ندارد یک ذره</p></div>
<div class="m2"><p>سوی خدای داور بی‌یاور</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نزدیک او اگر خطرش هستی</p></div>
<div class="m2"><p>یک شربت آب کی خوردی کافر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>الفنج گاه توست جهان، زینجا</p></div>
<div class="m2"><p>برگیر زود زاد ره محشر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بل دفتری است این که همی بینی</p></div>
<div class="m2"><p>خط خدای خویش بر این دفتر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>منکر مشو اشارت حجت را</p></div>
<div class="m2"><p>زیرا هگرز حق نبود منکر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>خط خدای زود بیاموزی</p></div>
<div class="m2"><p>گر در شوی به خانهٔ پیغمبر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گر درشوی به خانه‌ش، بر خاکت</p></div>
<div class="m2"><p>شمشاد و لاله روید و سیسنبر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ندهد خدای عرش در این خانه</p></div>
<div class="m2"><p>راهت مگر به راهبری حیدر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>حیدر، که زو رسید و ز فخر او</p></div>
<div class="m2"><p>از قیروان به چین خبر خیبر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>شیران ز بیم خنجر او حیران</p></div>
<div class="m2"><p>دریا به پیش خاطر او فرغر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>قولش مقر و مایهٔ نور دل</p></div>
<div class="m2"><p>تیغش مکان و معدن شور و شر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ایزد عطاش داد محمد را</p></div>
<div class="m2"><p>نامش علی شناس و لقب کوثر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>گرت آرزوست صورت او دیدن</p></div>
<div class="m2"><p>وان منظر مبارک و آن مخبر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بشتاب سوی حضرت مستنصر</p></div>
<div class="m2"><p>ره را ز فخر جز به مژه مسپر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>آنجاست دین و دنیا را قبله</p></div>
<div class="m2"><p>وانجاست عز و دولت را مشعر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>خورشید پیش طلعت او تیره</p></div>
<div class="m2"><p>گردون بجای حضرت او کردر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ای یافته به تیغ و بیان تو</p></div>
<div class="m2"><p>زیب و جمال معرکه و منبر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بی‌صورت مبارک تو، دنیا</p></div>
<div class="m2"><p>مجهول بود و بی‌سلب و زیور</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>معروف شد به علم تو دین، زیرا</p></div>
<div class="m2"><p>دین عود بود و خاطر تو مجمر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ای حجت زمین خراسان، زه!</p></div>
<div class="m2"><p>مدح رسول و آل چنین گستر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ای گشته نوک کلک سخن گویت</p></div>
<div class="m2"><p>در دیدهٔ مخالف دین نشتر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>دیبا همی بدیع برون آری</p></div>
<div class="m2"><p>اندر ضمیر توست مگر ششتر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بر شعر زهد گفتن و بر طاعت</p></div>
<div class="m2"><p>این روزگار مانده‌ت را بشمر</p></div></div>