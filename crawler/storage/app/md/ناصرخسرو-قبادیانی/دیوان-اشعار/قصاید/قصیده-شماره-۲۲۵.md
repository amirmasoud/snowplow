---
title: >-
    قصیدهٔ شمارهٔ ۲۲۵
---
# قصیدهٔ شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>ای آنکه ندیم باده و جامی</p></div>
<div class="m2"><p>تا عمر مگر برین بفرجامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دشت حریر سبز در پوشد</p></div>
<div class="m2"><p>وآید به نشاط حسی از نامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه رفته به دشت با تماشائی</p></div>
<div class="m2"><p>گه خفته به زیر شاخ بادامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگذشت تموز سی چهل بر تو</p></div>
<div class="m2"><p>از بهر چه مانده‌ای بدین خامی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش است تو را سحرگهان رفتن</p></div>
<div class="m2"><p>از جامه به جام، اگر بننجامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیکن فلکت همی بفرجامد</p></div>
<div class="m2"><p>فرجام نگر، چه فتنه بر جامی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دایم به شکار در همی تازی</p></div>
<div class="m2"><p>و آگاه نه‌ای که مانده در دامی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز خاک ز دهر نیست بهر تو</p></div>
<div class="m2"><p>هرچند که بر فلک چو بهرامی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فردا به عصا همیت باید رفت</p></div>
<div class="m2"><p>امروز چنین چو کبگ چه خرامی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قد الفیت لام شد، بنگر،</p></div>
<div class="m2"><p>منگر چندین به زلفک لامی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از حرص به وقت چاشت چون کرگس</p></div>
<div class="m2"><p>در چاچ و، به وقت شام در شامی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون داد بخواهم از تو بس تندی</p></div>
<div class="m2"><p>لیکن چو ستم کنی خویش و رامی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ایدون شب و روز بر ستم کردن</p></div>
<div class="m2"><p>استاده ز بهر اسپ و استامی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در دنیا سخت سختی و در دین</p></div>
<div class="m2"><p>بس سست و میانه‌کار و هنگامی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سوی تو نیامده است پیغمبر</p></div>
<div class="m2"><p>یا تو نه سزا و اهل پیغامی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر روز به مذهب دگر باشی</p></div>
<div class="m2"><p>گه در چه ژرف و گاه بر بامی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا بی‌ادبی همی توانی کرد</p></div>
<div class="m2"><p>خون علما به دم بیاشامی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لیکن چو کسیت میهمانی کرد</p></div>
<div class="m2"><p>از پر خوردن همی نیارامی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر ناصبیت برد عمر باشی</p></div>
<div class="m2"><p>ور شیعی خواندت علی نامی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وانگه که شدی ضعیف بنشینی</p></div>
<div class="m2"><p>با زهد چو بو یزید بسطامی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با عامه خلق گوئی از خاصم</p></div>
<div class="m2"><p>لیکن سوی خاص کمتر از عامی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای حجت از این چنین بی‌آزرمان</p></div>
<div class="m2"><p>تا چند کشی محال و ناکامی؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از خوگ به باغ در چه افزاید</p></div>
<div class="m2"><p>جز زشتی و خامی و بی‌اندامی؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ابلیس عدو است مر تو را زیرا</p></div>
<div class="m2"><p>تو آدم اهل و اهل احکامی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مشتاب به خون جام ازیرا تو</p></div>
<div class="m2"><p>مر نوح زمان خویش را سامی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از روح شریف همچو ارواحی</p></div>
<div class="m2"><p>گرچه به‌تن از جهان اجسامی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای معدن فتح ونصر مستنصر</p></div>
<div class="m2"><p>شاهان همه روبه و تو ضرغامی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>من بنده توانگرم به علم تو</p></div>
<div class="m2"><p>زیرا تو توانگر از جهان تامی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر کاری را بود سرانجامی</p></div>
<div class="m2"><p>تو عالم حس را سرانجامی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>من بر سر دشمنانت صمصامم</p></div>
<div class="m2"><p>تو صاحب ذوالفقار و صمصامی</p></div></div>