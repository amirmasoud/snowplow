---
title: >-
    قصیدهٔ شمارهٔ ۲۰۱
---
# قصیدهٔ شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>چیست آن لشکر فریشتگان</p></div>
<div class="m2"><p>که بیایند از آسمان پران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوی آن مرده‌ای که زنده شود</p></div>
<div class="m2"><p>چون بشویندش آن فریشتگان؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چیست آن مردهٔ فریشته خوار</p></div>
<div class="m2"><p>به بهار و به تیره و تابستان؟</p></div></div>