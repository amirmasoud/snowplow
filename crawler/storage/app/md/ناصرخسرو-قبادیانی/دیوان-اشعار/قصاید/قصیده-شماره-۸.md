---
title: >-
    قصیدهٔ شمارهٔ ۸
---
# قصیدهٔ شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>نیکوی تو چیست و خوش چه، ای برنا؟</p></div>
<div class="m2"><p>دیباست تو را نکو و خوش حلوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنگر که مر این دو را چه می‌داند</p></div>
<div class="m2"><p>آن است نکو و خوش سوی دانا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلوا نخورد چو جو بیابد خر</p></div>
<div class="m2"><p>دیبا نبود به گاو بر زیبا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز مردم با خرد نمی‌یابد</p></div>
<div class="m2"><p>هنگام خورو بطر خوشی زینها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حلوا به خرد همی دهد لذت</p></div>
<div class="m2"><p>قیمت به خرد همی گرد دیبا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان را به خرد نکو چو دیبا کن</p></div>
<div class="m2"><p>تا مرد خرد نگویدت «رعنا»</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرم است نکو بحق و، خوش دانش</p></div>
<div class="m2"><p>هر دو خوش و خوب و در خور و همتا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیبای دل است شرم زی عاقل</p></div>
<div class="m2"><p>حلوای دل است علم زی والا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حورا توی ار نکو و با شرمی</p></div>
<div class="m2"><p>گر شرمگن و نکو بود حورا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر شرم نیایدت ز نادانی</p></div>
<div class="m2"><p>بی‌شرم‌تر از تو کیست در دنیا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کوری تو کنون به وقت نادانی</p></div>
<div class="m2"><p>آموختنت کند بحق بینا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو عورت جهل را نمی‌بینی</p></div>
<div class="m2"><p>آنگاه شود به چشم تو پیدا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این عورت بود آنکه پیدا شد</p></div>
<div class="m2"><p>در طاعت دیو از آدم و حوا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای آدمی ار تو علم ناموزی</p></div>
<div class="m2"><p>چون مادر و چون پدر شوی رسوا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون پست بودت قامت دانش</p></div>
<div class="m2"><p>چون سرو چه سود مر تو را بالا؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دانا ز تو چون چرا و چون پرسد</p></div>
<div class="m2"><p>بالات سخن نگوید، ای برنا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاید که ز بیم شرم و رسوائی</p></div>
<div class="m2"><p>در جستن علم دل کنی یکتا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ناموخت خدای ما مر آدم را</p></div>
<div class="m2"><p>چون عور برهنه گشت جز کاسما</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بنگر که چه بود نیک آن اسما</p></div>
<div class="m2"><p>منگر به دروغ عامه و غوغا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا نام کسی نخست ناموزی</p></div>
<div class="m2"><p>در مجمع خلق چون کنیش آوا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از نام به نامدار ره یابد</p></div>
<div class="m2"><p>چون عاقل و تیزهش بود جویا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خرسند مشو به نام بی معنی</p></div>
<div class="m2"><p>نامی تهی است زی خرد عنقا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این عالم مرده سوی من نام است</p></div>
<div class="m2"><p>آن عالم زنده ذات او والا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سوی همه خیر راه بنماید</p></div>
<div class="m2"><p>این نام رونده بر زبان ما</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دو نام دگر نهاد روم و هند</p></div>
<div class="m2"><p>این را که تو خوانیش همی خرما</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بوی است نه عین و نون و با و را</p></div>
<div class="m2"><p>نام معروف عنبر سارا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چندین عجبی ز چه پدید آمد</p></div>
<div class="m2"><p>از خاک به زیر گنبد خضرا؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این رستنی است و ناروان هرسو</p></div>
<div class="m2"><p>وان بی‌سخن است و این سیم گویا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>این زشت سپید و آن سیه نیکو</p></div>
<div class="m2"><p>آن گنده و تلخ وین خوش و بویا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از چشمهٔ چشم و از یکی صانع</p></div>
<div class="m2"><p>یاقوت چراست آن و این مینا؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>این جزو کهاست چونش بشناسی</p></div>
<div class="m2"><p>بر کل دلیل گرددت اجزا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از علت بودش جهان بررس</p></div>
<div class="m2"><p>بفگن به زبان دهریان سودا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>انگار که روز آخر است امروز</p></div>
<div class="m2"><p>زیرا که هنوز نامده‌است فردا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون آخر عمر این جهان آمد</p></div>
<div class="m2"><p>امروز، ببایدش یکی مبدا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کشتی خرد است دست در وی زن</p></div>
<div class="m2"><p>تا غرقه نگردی اندر این دریا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر با خردی چرا نپرهیزی</p></div>
<div class="m2"><p>ای خواجه از این خورنده اژدرها؟</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>با طاعت و ترس باش همواره</p></div>
<div class="m2"><p>تا از تو به دل حسد برد ترسا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پرهیز به طاعت و به دانش کن</p></div>
<div class="m2"><p>بر خیره مده به جاهلان لالا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا بسته نگیردت یکی جاهل</p></div>
<div class="m2"><p>هر روز به سان گاوک دوشا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از طاعت و علم نردبانی کن</p></div>
<div class="m2"><p>وانگه برشو به کوکب جوزا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زین چرخ برون، خرد همی گوید،</p></div>
<div class="m2"><p>صحراست یکی و بی‌کران صحرا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زانجا همی آید اندر این گنبد</p></div>
<div class="m2"><p>از بهر من و تو این همه نعما</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هرگز نشده است خلق از این زندان</p></div>
<div class="m2"><p>جز کز ره نردبان علم آنجا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چون جانت به علم شد بر آن معدن</p></div>
<div class="m2"><p>سرما ز تو دور ماند و هم گرما</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بپرست خدای را و تو بشناس</p></div>
<div class="m2"><p>از با صفت و ز بی‌صفت تنها</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وان را که فلک به امر او گردد</p></div>
<div class="m2"><p>ایزدش مگوی خیره، ای شیدا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کان بندهٔ ایزد است و فرمان بر</p></div>
<div class="m2"><p>مولای خدای را مدان مولا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>وز راز خدای اگر نه‌ای آگه</p></div>
<div class="m2"><p>بر حجت دین چرا کنی صفرا؟</p></div></div>