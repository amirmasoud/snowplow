---
title: >-
    قصیدهٔ شمارهٔ ۱۲۶
---
# قصیدهٔ شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>چون گشت جهان را دگر احوال عیانیش؟</p></div>
<div class="m2"><p>زیرا که بگسترد خزان راز نهانیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر حسرت شاخ گل در باغ گوا شد</p></div>
<div class="m2"><p>بیچارگی و زردی و کوژی و نوانیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا زاغ به باغ اندر بگشاد فصاحت</p></div>
<div class="m2"><p>بر بست زبان از طرب لحن غوانیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرمنده شد از باد سحر گلبن عریان</p></div>
<div class="m2"><p>وز آب روان شرمش بربود روانیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کهسار که چون رزمهٔ بزاز بد اکنون</p></div>
<div class="m2"><p>گر بنگری از کلبهٔ نداف ندانیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون زر مزور نگر آن لعل بدخشیش</p></div>
<div class="m2"><p>چون چادر گازر نگر آن برد یمانیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس باد جهد سرد ز که لاجرم اکنون</p></div>
<div class="m2"><p>چون پیر که یاد آیدش از روز جوانیش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خورشید بپوشید ز غم پیرهن خز</p></div>
<div class="m2"><p>این است همیشه سلب خوب خزانیش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر مفرش پیروزه به شب شاه حبش را</p></div>
<div class="m2"><p>آسوده و پاکیزه و بلور است اوانیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنگر به ستاره که بتازد سپس دیو</p></div>
<div class="m2"><p>چون زر گدازیده که بر قیر چکانیش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مانند یکی جام یخین است شباهنگ</p></div>
<div class="m2"><p>بزدوده به قطر سحری چرخ کیانیش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر نیست یخین چونکه چو خورشید بر آید</p></div>
<div class="m2"><p>هر چند که جویند نیابند نشانیش؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پروین به چه ماند؟ به یکی دستهٔ نرگس</p></div>
<div class="m2"><p>یا نسترن تازه که بر سبزه فشانیش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وین دهر دونده به یکی مرکب ماند</p></div>
<div class="m2"><p>کز کار نیاساید هر چند دوانیش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گیتیت یکی بندهٔ بدخوست مخوانش</p></div>
<div class="m2"><p>زیرا ز تو بدخو بگریزد چو بخوانیش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بی‌حاصل و مکار جهانی است پر از غدر</p></div>
<div class="m2"><p>باید که چو مکار بخواندت برانیش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جز حنظل و زهرت نچشاند چو بخواندت</p></div>
<div class="m2"><p>هرچند که تو روز و شبان نوش چشانیش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از بهر جفا سوی تو آمد، به در خویش</p></div>
<div class="m2"><p>مگذار و ز در زود بران گر بتوانیش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دشمن، چو نکو حال شدی، گرد تو گردد</p></div>
<div class="m2"><p>زنهار مشو غره بدان چرب زبانیش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چونان که چو بز بهتر و فربه‌تر گردد</p></div>
<div class="m2"><p>از بهر طمع بیش کند مرد شبانیش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرچند که دیر آید سوی تو بیاید،</p></div>
<div class="m2"><p>چون سوی پدرت آمد، پیغام نهانیش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فرزند بسی دارد این دهر جفا جوی</p></div>
<div class="m2"><p>هریک بد و بی‌حاصل چون مادر زانیش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ناکس به تو جز محنت و خواری نرساند</p></div>
<div class="m2"><p>گر تو به مثل بر فلک ماه رسانیش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>طاعت به گمانی بنمایدت ولیکن</p></div>
<div class="m2"><p>لعنت کندت گر نشود راست گمانیش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بد فعل و عوان گر چه شود دوست به آخر</p></div>
<div class="m2"><p>هم بر تو به کار آرد یک روز عوانیش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گه غدر کند بر تو گه مکر فروشد</p></div>
<div class="m2"><p>صد لعنت بر صنعت و بر بازرگانیش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر گاه نبینی مگر آن را که سزا هست</p></div>
<div class="m2"><p>کز گاه برانگیزی و در چاه نشانیش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پند و سخن خوب بر آن سفله دریغ است</p></div>
<div class="m2"><p>زنهار که از نار جویی بد برهانیش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پند تو تبه گردد در فعل بد او</p></div>
<div class="m2"><p>پرواره کژ آید چو بود کژ مبانیش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون پند نپذرفت زخود دور کنش زود</p></div>
<div class="m2"><p>تا جان عزیزت برهانی ز گرانیش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زیرا که چو تیر کژ تو راست نباشد</p></div>
<div class="m2"><p>آن به که به زودی سوی بدخواه جهانیش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آن است خردمند که جز بر طلب فضل</p></div>
<div class="m2"><p>ضایع نشود یک نفس از عمر زمانیش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وز خلق تواضع نکند بدگهری را</p></div>
<div class="m2"><p>هرچند که بسیار بود گوهر کانیش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کان مرد سوی اهل خرد سست بود سخت</p></div>
<div class="m2"><p>کز بهر طمع سست شود سخت کمانیش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در صدر خردمندان بی‌فضل نه خوب است</p></div>
<div class="m2"><p>چون رشتهٔ لولو که بود سنگ میانیش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون راه نجوئی سوی آن بار خدائی</p></div>
<div class="m2"><p>کز خلق چو یزدان نشناسد کس ثانیش؟</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>صد بندهٔ مطواع فزون است به درگاه</p></div>
<div class="m2"><p>از قیصری و سندی و بغدادی و خانیش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مستنصر بالله که او فضل خدای است</p></div>
<div class="m2"><p>موجود و مجسم شده در عالم فانیش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آنکو سرش از فضل خداوند بتابد</p></div>
<div class="m2"><p>فردا نکند آتش و اغلال شبانیش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ایزدش عطا داد به پیغمبر ازیراک</p></div>
<div class="m2"><p>اوی است حقیقت یکی از سبع مثانیش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در عالم دین او سوی ما قول خدای است</p></div>
<div class="m2"><p>قولی که همه رحمت و فضل است معانیش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>با همت عالیش فلک را و زمین را</p></div>
<div class="m2"><p>پست است بلندی و حقیر است کلانیش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون مرکب او تیز شود کرد نیارد</p></div>
<div class="m2"><p>تنین فلک روز ملاقات عنانیش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>غره نکند هر که بدیده است سپاهش</p></div>
<div class="m2"><p>این عالم ازان پس به فراخی مکانیش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ناید حسد و رشک کمین چاکر او را</p></div>
<div class="m2"><p>نز ملک فلانی و نه از مال فلانیش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هر کو رهیش گشت چو من بنده ازان پس</p></div>
<div class="m2"><p>از علم و هنر باشد دینار و شیانیش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بر عالم علویش گمان بر چو فرشته</p></div>
<div class="m2"><p>هرچند که اینجا بود این جسم عیانیش</p></div></div>