---
title: >-
    قصیدهٔ شمارهٔ ۶
---
# قصیدهٔ شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>نکوهش مکن چرخ نیلوفری را</p></div>
<div class="m2"><p>برون کن ز سر باد و خیره‌سری را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بری دان از افعال چرخ برین را</p></div>
<div class="m2"><p>نشاید ز دانا نکوهش بری را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی تا کند پیشه، عادت همی کن</p></div>
<div class="m2"><p>جهان مر جفا را، تو مر صابری را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم امروز از پشت بارت بیفگن</p></div>
<div class="m2"><p>میفگن به فردا مر این داوری را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو تو خود کنی اختر خویش را بد</p></div>
<div class="m2"><p>مدار از فلک چشم نیک اختری را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به چهره شدن چون پری کی توانی؟</p></div>
<div class="m2"><p>به افعال ماننده شو مر پری را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدیدی به نوروز گشته به صحرا</p></div>
<div class="m2"><p>به عیوق ماننده لالهٔ طری را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر لاله پر نور شد چون ستاره</p></div>
<div class="m2"><p>چرا زو نپذرفت صورت گری را؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو با هوش و رای از نکو محضران چون</p></div>
<div class="m2"><p>همی برنگیری نکو محضری را؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگه کن که ماند همی نرگس نو</p></div>
<div class="m2"><p>ز بس سیم و زر تاج اسکندری را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درخت ترنج از بر و برگ رنگین</p></div>
<div class="m2"><p>حکایت کند کلهٔ قیصری را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپیدار مانده‌است بی‌هیچ چیزی</p></div>
<div class="m2"><p>ازیرا که بگزید او کم بری را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر تو از آموختن‌سر بتابی</p></div>
<div class="m2"><p>نجوید سر تو همی سروری را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بسوزند چوب درختان بی‌بر</p></div>
<div class="m2"><p>سزا خود همین است مر بی‌بری را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>درخت تو گر بار دانش بگیرد</p></div>
<div class="m2"><p>به زیر آوری چرخ نیلوفری را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نگر نشمری، ای برادر، گزافه</p></div>
<div class="m2"><p>به دانش دبیری و نه شاعری را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که این پیشه‌ها ییست نیکو نهاده</p></div>
<div class="m2"><p>مر الفغدن نعمت ایدری را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دگرگونه راهی و علمی است دیگر</p></div>
<div class="m2"><p>مرالفغدن راحت آن سری را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بلی این و آن هر دو نطق است لیکن</p></div>
<div class="m2"><p>نماند همی سحر پیغمبری را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو کبگ دری باز مرغ است لیکن</p></div>
<div class="m2"><p>خطر نیست با باز کبگ دری را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پیمبر بدان داد مر علم حق را</p></div>
<div class="m2"><p>که شایسته دیدش مر این مهتری را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به هارون ما داد موسی قرآن را</p></div>
<div class="m2"><p>نبوده‌است دستی بران سامری را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو را خط قید علوم است و، خاطر</p></div>
<div class="m2"><p>چو زنجیر مر مرکب لشکری را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو با قید بی اسپ پیش سواران</p></div>
<div class="m2"><p>نباشی سزاوار جز چاکری را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ازین گشته‌ای، گر بدانی تو، بنده</p></div>
<div class="m2"><p>شه شگنی و میر مازندری را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر شاعری را تو پیشه گرفتی</p></div>
<div class="m2"><p>یکی نیز بگرفت خنیاگری را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو برپائی آنجا که مطرب نشیند</p></div>
<div class="m2"><p>سزد گر ببری زبان جری را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صفت چند گوئی به شمشاد و لاله</p></div>
<div class="m2"><p>رخ چون مه و زلفک عنبری را؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به علم و به گوهر کنی مدحت آن را</p></div>
<div class="m2"><p>که مایه است مر جهل و بد گوهری را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به نظم اندر آری دروغی طمع را</p></div>
<div class="m2"><p>دروغ است سرمایه مر کافری را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پسنده است با زهد عمار و بوذر</p></div>
<div class="m2"><p>کند مدح محمود مر عنصری را؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من آنم که در پای خوگان نریزم</p></div>
<div class="m2"><p>مر این قیمتی در لفظ دری را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو را ره نمایم که چنبر کرا کن</p></div>
<div class="m2"><p>به سجده مر این قامت عرعری را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کسی را برد سجده دانا که یزدان</p></div>
<div class="m2"><p>گزیده‌ستش از خلق مر رهبری را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کسی را که بسترد آثار عدلش</p></div>
<div class="m2"><p>ز روی زمین صورت جائری را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>امام زمانه که هرگز نرانده است</p></div>
<div class="m2"><p>بر شیعتش سامری ساحری را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه ریبی به جز حکمتش مردمی را</p></div>
<div class="m2"><p>نه عیبی به جز همتش برتری را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو با عدل در صدر خواهی نشسته</p></div>
<div class="m2"><p>نشانده در انگشتری مشتری را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بشو زی امامی که خط پدرش است</p></div>
<div class="m2"><p>به تعویذ خیرات مر خیبری را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ببین گرت باید که بینی به ظاهر</p></div>
<div class="m2"><p>ازو صورت و سیرت حیدری را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نیارد نظر کرد زی نور علمش</p></div>
<div class="m2"><p>که در دست چشم خرد ظاهری را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگر ظاهری مردمی را بجستی</p></div>
<div class="m2"><p>به طاعت، برون کردی از سر خری را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ولیکن بقر نیستی سوی دانا</p></div>
<div class="m2"><p>اگر جویدی حکمت باقری را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مرا همچو خود خر همی چون شمارد؟</p></div>
<div class="m2"><p>چه ماند همی غل مر انگشتری را؟</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نبیند که پیشش همی نظم و نثرم</p></div>
<div class="m2"><p>چو دیبا کند کاغذ دفتری را؟</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بخوان هر دو دیوان من تا ببینی</p></div>
<div class="m2"><p>یکی گشته با عنصری بحتری را</p></div></div>