---
title: >-
    قصیدهٔ شمارهٔ ۱۳۰
---
# قصیدهٔ شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>هر کس به نسب نیک ندانی و به آلش</p></div>
<div class="m2"><p>بر نسبت او نیست گوا به ز فعالش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیرا که درختی که مر او را نشناسند</p></div>
<div class="m2"><p>بارش خبر آرد که چه بوده است نهالش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قول تو چه بار است و تو پربار درختی</p></div>
<div class="m2"><p>آباد درختی که چو خرماست مقالش!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فضل و ادب مرد مهین نسبت اوی است</p></div>
<div class="m2"><p>شاید که نپرسی ز پدر وز عم خالش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کوزه چو آب خوش خوردی نبود باک</p></div>
<div class="m2"><p>گر چون خز ادکن نبود نرم سفالش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حکمت و علم است جمال تن مردم</p></div>
<div class="m2"><p>نه در حشم و اسپ و جمال است جمالش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنجا که سخن دان بگشاید در منطق</p></div>
<div class="m2"><p>از مرد سخن هرگز گویند نعالش؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفسی که ندارد پر و بال از حکم و علم</p></div>
<div class="m2"><p>آنجا که بود علم بسوزد پر و بالش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر دانا پرسد که «چرا خاک چو شد سنگ</p></div>
<div class="m2"><p>چون خاک نیاغازد چون آب زلالش؟»</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس حلق گشاده به خرافات و محالات</p></div>
<div class="m2"><p>کو بسته شود سخت بدین سست سؤالش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر نیست به جعبه‌ش در چون تیر مقالی</p></div>
<div class="m2"><p>کس دست نگیرند ز پیروز و ینالش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور نیست به دیبا تنش آراسته، شاید،</p></div>
<div class="m2"><p>چون خویشتن آراست به دیبای خصالش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهل آتش جان آمد و جان نال جهالت</p></div>
<div class="m2"><p>وز آتش نادان نرهد هرگز نالش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون زانچه نداندش بپرسند سؤالی</p></div>
<div class="m2"><p>از هول شود زایل ازو خوابش و هالش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وز گاه بیفتد به سوی چاه فرودین</p></div>
<div class="m2"><p>وز صدر برانند سوی صف نعالش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای کرده تو را بسته و مطواع فلان میر</p></div>
<div class="m2"><p>آن میخ کشن ساز و سیه اسپ عقالش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو همبر آن میر شوی گر طمع خویش</p></div>
<div class="m2"><p>بیرون کنی از دولت و از نعمت و مالش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>میری بود آنکو چو به گرمابه درآید</p></div>
<div class="m2"><p>خالی شود از ملکت و از جاه و جلالش؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وانجا که سخن خیزد از چند و چه و چون</p></div>
<div class="m2"><p>دانای سخن پیشه بخندد ز اقوالش!</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بل میر حکیمی است که اندر دل اوی است</p></div>
<div class="m2"><p>خیل و حشم و مملکت و گنج و رجالش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وانجا که سخن خیزد از آیات الهی</p></div>
<div class="m2"><p>سقراط سزد چاکرو ادریس عیالش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن را نبرم مال همی ظن که خداوند</p></div>
<div class="m2"><p>در سنگ نهاده‌است و در این خاک و رمالش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بل مال یکی جوهر عالی است که دانا</p></div>
<div class="m2"><p>داند که خرد شاید صندوق و جوالش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن مال خدای است که زنهار نهاده‌است</p></div>
<div class="m2"><p>اندر دل پاکیزهٔ پیغمبر و آلش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن آب حیات است که جاوید بماند</p></div>
<div class="m2"><p>نفسی که ازین داد کریم متعالش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زین مال و ازین آب رسید احمد تازی</p></div>
<div class="m2"><p>در عالم گویندهٔ دانا به کمالش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نور ازلی را چو دلش راست به پذیرفت</p></div>
<div class="m2"><p>الله زمین شد که ندیدند مثالش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وز برکت این نور فرو خواند قران را</p></div>
<div class="m2"><p>بنبشته بر افلاک و بر و بحر و جبالش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وان کس که همی گوید کاواز شنودی</p></div>
<div class="m2"><p>مندیش از آن جاهل و منیوش محالش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وین نور بر اولاد نبی باقی گشته است</p></div>
<div class="m2"><p>کز نفس پیمبر به وصی بود وصالش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زیرا که نشد دادگر از کرده پشیمان</p></div>
<div class="m2"><p>نه نیز ز کاری بگرفته است ملالش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زین نور بیابی تو اگر سخت بکوشی</p></div>
<div class="m2"><p>با آنکه نیابی ز همه خلق همالش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آن کس که گرش اعمی در خواب بیند</p></div>
<div class="m2"><p>روشن شودش دیده ز پر نور خیالش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن کس که اگر نامش بر دهر بخوانند</p></div>
<div class="m2"><p>فرخنده شود ساعت و روز و مه و سالش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا بود قضا بود وفادار یمینش</p></div>
<div class="m2"><p>تا هست قدر هست رضاخواه شمالش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عالم به مثل بدخو و ناساز عروسی است</p></div>
<div class="m2"><p>وز خلق جهان نیست جز او شوی حلالش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هر کو به زنا قصد جهان دارد از اوباش</p></div>
<div class="m2"><p>بس زود بیارند در این ننگ و نکالش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کی نرم کند جز که به فرمان روانش</p></div>
<div class="m2"><p>این شیر به زیر قدمت گردن و یالش؟</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا سعد خداوند به من بنده بپیوست</p></div>
<div class="m2"><p>بگسست زمن دهر و برستم ز وبالش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>امروز کزو طالع مسعود شده‌ستم</p></div>
<div class="m2"><p>از دهر کی اندیشم وز بیم زوالش؟</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر کو سرش از طاعت آن شیر بتابد</p></div>
<div class="m2"><p>گر شیر نر است او، بخورد ماده شگالش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ور طالع فالش به مثل مشتری آید</p></div>
<div class="m2"><p>مریخ نهد داغی بر طلعت فالش</p></div></div>