---
title: >-
    قصیدهٔ شمارهٔ ۲۵۰
---
# قصیدهٔ شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>شبی تاری چو بی‌ساحل دمان پر قیر دریائی</p></div>
<div class="m2"><p>فلک چون پر ز نسرین برگ نیل اندوده صحرائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشیب و توده و بالا همه خاموش و بی‌جنبش</p></div>
<div class="m2"><p>چو قومی هر یکی مدهوش و درمانده به سودائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمانه رخ به قطران شسته وز رفتن برآسوده</p></div>
<div class="m2"><p>که گفتی نافریده ستش خدای فرد فردائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه از هامون سودائی تحیر هیچ کمتر شد</p></div>
<div class="m2"><p>نه نیز از صبح صفرائی بجنبید ایچ صفرائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه نور از چشم‌ها یارست رفتن سوی صورت‌ها</p></div>
<div class="m2"><p>نه سوی هیچ گوشی نیز ره دانست آوائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدل کرده جهان سفله هستی را به ناهستی</p></div>
<div class="m2"><p>فرو مانده بدین کار اندرون گردون چو شیدائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برآسوده ز جنبش‌ها و قال و قیل دهر ایدون</p></div>
<div class="m2"><p>که گفتی نیست در عالم نه جنبائی نه گویائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندید از صعب تاریکی و تنگی زیر این خیمه</p></div>
<div class="m2"><p>نه چشم باز من شخصی نه جان خفته رؤیائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا چون چشم دل زی خلق، چشم سر به سوی شب</p></div>
<div class="m2"><p>چو اندر لشکری خفته یکی بیدار تنهائی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کواکب را همی دیدم به چشم سر چو بیداران</p></div>
<div class="m2"><p>به چشم دل نمی‌بینم یکی بیدار دانائی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندیدم تا ندیدم دوش چرخ پر کواکب را</p></div>
<div class="m2"><p>به چشم سر در این عالم یکی پر حور خضرائی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر سرا به ضرا در ندیده‌ستی بشو بنگر</p></div>
<div class="m2"><p>ستاره زیر ابر اندر چو سرا زیر ضرائی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو خوشهٔ نسترن پروین درفشنده به سبزه بر</p></div>
<div class="m2"><p>به زر و گوهران آراسته خود را چو دارائی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نهاده چشم سرخ خویش را عیوق زی مغرب</p></div>
<div class="m2"><p>چو از کینه معادی چشم بنهد زی معادائی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو در تاریک چه یوسف منور مشتری در شب</p></div>
<div class="m2"><p>درو زهره بمانده زرد و حیران چون زلیخائی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنیسهٔ مریمستی چرخ گفتی پر ز گوهرها</p></div>
<div class="m2"><p>نجوم ایدون چو رهبانان و دبران چون چلیبائی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا بیدار مانده چشم و گوش و دل که چون یابم</p></div>
<div class="m2"><p>به چشم از صبح برقی یا به گوش از وحش هرائی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که نفس ار چه نداند، عقل پر دانش همی داند</p></div>
<div class="m2"><p>که در عالم نباشد بی‌نهایت هیچ مبدائی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو زاغ شب به جابلسا رسید از حد جابلقا</p></div>
<div class="m2"><p>برآمد صبح رخشنده چو از یاقوت عنقائی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گریزان شد شب تیره ز خیل صبح رخشنده</p></div>
<div class="m2"><p>چنان چون باطل از حقی و ناپیدا ز پیدائی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خجل گشتند انجم پاک چون پوشیده رویانی</p></div>
<div class="m2"><p>که مادرشان بیند روی بگشاده مفاجائی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه همواره در خورشید پیوستند و ناچاره</p></div>
<div class="m2"><p>به کل خویش پیوندد سرانجامی هر اجزائی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنین تا کی کنی حجت تو این وصف نجوم و شب؟</p></div>
<div class="m2"><p>سخن را اندر این معنی فگندی در درازائی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز بالای خرد بنگر یکی در کار این عالم</p></div>
<div class="m2"><p>ازیرا از خرد برتر نیابی هیچ بالائی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یکی دریاست این عالم پر از لولوی گوینده</p></div>
<div class="m2"><p>اگر پر لولوی گویا کسی دیده است دریائی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زمانه است آب این دریا و این اشخاص کشتی‌ها</p></div>
<div class="m2"><p>ندید این آب و کشتی را مگر هشیار بینائی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز بهر بیشی و کمی به خلق اندر پدید آمد</p></div>
<div class="m2"><p>که ناپیدا بخواهد شد بر این سان صعب غوغائی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فلان از بهر بهمان تا مرو را صید چون گیرد</p></div>
<div class="m2"><p>ازو پوشیده هر ساعت همی سازد معمائی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همی بینی به چشم دل به دلها در ز بهر آن</p></div>
<div class="m2"><p>که بستاند قبای ژنده یا فرسوده یکتائی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>محسن را دگر مکری و حسان را دگر کیدی</p></div>
<div class="m2"><p>و جعفر را دگر روئی و صالح را دگر رائی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رئیسان و سران دین و دنیا را یکی بنگر</p></div>
<div class="m2"><p>که تا بینی مگر گرگی همی یا باد پیمائی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به چشم سر نگه کن پس به دل بیندیش تا یابی</p></div>
<div class="m2"><p>یکی با شرم پیری یا یکی مستور برنائی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کجا باشد محل آزادگان را در چنین وقتی</p></div>
<div class="m2"><p>که بر هر گاهی و تختی شه و میر است مولائی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مدارا کن مده گردن خسیسان را چو آزادان</p></div>
<div class="m2"><p>که از تنگی کشیدن به بسی کردن مدارائی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر دانی که نا مردم نداند قیمت مردم</p></div>
<div class="m2"><p>مبر مر خویشتن را خیره زی مردم همانائی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نبینی بر گه شاهی مگر غدار و بی‌باکی</p></div>
<div class="m2"><p>نیابی بر سر منبر مگر رزاق و کانائی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یجوز و لایجوز ستش همه فقه از جهان لیکن</p></div>
<div class="m2"><p>سر استر ز مال وقف گشته‌ستش چو جوزائی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تهی تر دانش از دانش ازان کز مغز ترب ارچه</p></div>
<div class="m2"><p>به منبر بر همی بینیش چون قسطای لوقائی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>حصاری به ز خرسندی ندیدم خویشتن را من</p></div>
<div class="m2"><p>حصاری جز همین نگرفت ازین بیش ایچ کندائی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به پیش ناکسی ننهم به خواری تن چو نادانان</p></div>
<div class="m2"><p>نهد کس نافهٔ مشکین به پیش گنده غوشائی؟</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شکیبا گردد آن کس کو زمن طاعت طمع دارد</p></div>
<div class="m2"><p>ازیرا کارش افتاده است با صعبی شکیبائی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به طمع مال دونی مر مرا همتا کجا یابد</p></div>
<div class="m2"><p>ازان پس که‌م گزید از خلق عالم نیست همتائی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خداوندی که گر بر خاک دست شسته بفشاند</p></div>
<div class="m2"><p>ز هر قطره به خاک اندر پدید آید ثریائی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نه بی‌نور لقای او نجوم سعد را بختی</p></div>
<div class="m2"><p>نه با پهنای ملک او فلک را هیچ پهنائی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>محلی داد و علمی مر مرا جودش که پیش من</p></div>
<div class="m2"><p>نه دانا هست دانائی نه والا هست والائی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>من از دنیا مواسائی همی یابم به دین اندر</p></div>
<div class="m2"><p>که از دنیا و دین کس را چنان نامد مواسائی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سپاس آن بی همال و یار و با قدرت توانا را</p></div>
<div class="m2"><p>کزو یابد توانائی به عالم هر توانائی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>یکی دیبا طرازیدم نگاریده به حکمت‌ها</p></div>
<div class="m2"><p>که هرگز تا ابد ناید چنین از روم دیبائی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>درختی ساختم مانند طوبی خرم و زیبا</p></div>
<div class="m2"><p>که هر لفظیش دیناری است و هر معنیش خرمائی</p></div></div>