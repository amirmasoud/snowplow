---
title: >-
    قصیدهٔ شمارهٔ ۲۴۶
---
# قصیدهٔ شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>شادی و جوانی و پیشگاهی</p></div>
<div class="m2"><p>خواهی و ضعیفی و غم نخواهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیکن به مراد تو نیست گردون</p></div>
<div class="m2"><p>زین است به کار اندرون تباهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهی که بمانی و هم نمانی</p></div>
<div class="m2"><p>خواهی که نکاهی و هم بکاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونان که فزودی بکاهی ایراک</p></div>
<div class="m2"><p>بر سیرت و بر عادت گیاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چاهی است جهان ژرف و ما بدو در</p></div>
<div class="m2"><p>جوئیم همی تخت و گاه شاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چاه گه و شه چگونه باشد؟</p></div>
<div class="m2"><p>نشنود کسی پادشای چاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای در طلب پادشاهی، از من</p></div>
<div class="m2"><p>بررس که چه چیز است پادشاهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر خوی ستوران مشو به که بر</p></div>
<div class="m2"><p>بر گه چه نشینی چو اهل کاهی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مردم چو پذیرای دانش آمد</p></div>
<div class="m2"><p>گردنش بدادند مور و ماهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون گشت به دانش تمام آنگه</p></div>
<div class="m2"><p>گردن دهدش چرخ و دهر داهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دانش نبود آنکه پیش شاهان</p></div>
<div class="m2"><p>یکتاه قدت را کند دوتاهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این آز بود، ای پسر، نه دانش</p></div>
<div class="m2"><p>یکباره چنین خر مباش و ساهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درویشی اگر بی‌تمیز و علمی</p></div>
<div class="m2"><p>هرچند که با مال و ملک و جاهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن علم نباشد که بر سپیدی</p></div>
<div class="m2"><p>به همانش نبشته است با سیاهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>علم آن بود، آری، که مردم آن را</p></div>
<div class="m2"><p>برخواند از این صنعت الهی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این علم اگر حاضر است پیشت</p></div>
<div class="m2"><p>یزدان به تو داده است پیشگاهی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ور نیستی آگاه ازین بجویش</p></div>
<div class="m2"><p>زیرا که کنون بر سر دوراهی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پرهیز کن از لهو ازانکه هرگز</p></div>
<div class="m2"><p>سرمایه نکرده است هیچ لاهی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مشغول مشو همچو این ستوران</p></div>
<div class="m2"><p>از علم الهی بدین ملاهی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دین است سر و این جهان کلاه است</p></div>
<div class="m2"><p>بی‌سر تو چرا در غم کلاهی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با مال و سپاهی ز دین و دانش</p></div>
<div class="m2"><p>هرچند که بی‌مال و بی‌سپاهی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ور دانش و دین نیستت به چاهی</p></div>
<div class="m2"><p>هرچند که با تاج و تخت و گاهی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای مانده به کردار خویش غافل</p></div>
<div class="m2"><p>از امر الهی و از نواهی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از جهل قوی‌تر گنه چه باشد؟</p></div>
<div class="m2"><p>خیره چه بری ظن که بی‌گناهی؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از علم پناهی بساز محکم</p></div>
<div class="m2"><p>تا روز ضرورت بدو پناهی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پندی بده ای حجت خراسان</p></div>
<div class="m2"><p>روشن که تو بر چرخ فضل ماهی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هرچند که از دهر با سفاهت</p></div>
<div class="m2"><p>با ناله و با درد و رنج و آهی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زیرا که تو در شارسان حکمت</p></div>
<div class="m2"><p>با نعمت و با مال و دست گاهی</p></div></div>