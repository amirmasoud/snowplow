---
title: >-
    قصیدهٔ شمارهٔ ۱۱۷
---
# قصیدهٔ شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>کسی پر خانه دشتی دید هرگز</p></div>
<div class="m2"><p>نه دیوار و نه در بل پست و موجز؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو لشکر صف‌زده در خانه‌هاشان</p></div>
<div class="m2"><p>پس هر لشکری یکی مجاهز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وزیر و شاه و پیلان و سواران</p></div>
<div class="m2"><p>ستاده بر طرف‌ها دو مبارز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیاده با سواران جمله بی‌جان</p></div>
<div class="m2"><p>وزیر و شاه بی‌فرمان و عاجز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زخم و بند و کشتن گشته مشغول</p></div>
<div class="m2"><p>نه آنجا گرد و خون و نه هزاهز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه از خانه برون رفت آنکه بگریخت</p></div>
<div class="m2"><p>نه خونی را دیت بایست هرگز</p></div></div>