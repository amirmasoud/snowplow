---
title: >-
    قصیدهٔ شمارهٔ ۵۱
---
# قصیدهٔ شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>از اهل ملک در این خیمهٔ کبود که بود</p></div>
<div class="m2"><p>که ملک ازو نربود این بلند چرخ کبود؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آنکه بر طلب مال، عمر مایه گرفت</p></div>
<div class="m2"><p>چو روزگار بر آمد نه مایه ماند و نه سود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو عمر سوده شد و، مایه عمر بود تو را</p></div>
<div class="m2"><p>تو را ز مال که سوداست، اگر نه سود، چه سود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فزودگان را فرسوده گیر پاک همه</p></div>
<div class="m2"><p>خدای عزوجل نه فزود و نه فرسود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خدای را به صفات زمانه وصف مکن</p></div>
<div class="m2"><p>که هر سه وصف زمانه است هست و باشد و بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی است با صفت و بی‌صفت نگوئیمش</p></div>
<div class="m2"><p>نچیز و چیز مگویش، که مان چنین فرمود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خدای را بشناس و سپاس او بگزار</p></div>
<div class="m2"><p>که جز بر این دو نخواهیم بود ما ماخوذ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به فعل و قول زبان یکنهاد باش و مباش</p></div>
<div class="m2"><p>به دل خلاف زبان چون پشیز زر اندود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو نرم گویم با تو مرا درشت مگو</p></div>
<div class="m2"><p>مسوز دست جز آن را که مر تو را برهود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز خاک و آتش و آبی، به رسم ایشان رو</p></div>
<div class="m2"><p>که خاک خشک و درشت است و آب نرم و نسود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مباش مادح خویش و، مگوی خیره مرا</p></div>
<div class="m2"><p>که «من ترنج لطیفم خوش و تو بی مزه تود»</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر کسی بگرفتی به زور و جهد شرف</p></div>
<div class="m2"><p>به عرش بر بنشستی به سرکشی نمرود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهود را چه نکوهی؟ که تو به سوی جهود</p></div>
<div class="m2"><p>بسی نفایه‌تری زانکه سوی توست جهود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ستوده سوی خردمند شو به دانش ازانک</p></div>
<div class="m2"><p>بحق ستوده رسول است کش خدای ستود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یقین بدان که ز پاکیزگی است پیوسته</p></div>
<div class="m2"><p>به جان پاک رسول از خدای و خلق درود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر نخواهی کائی به محشر آلوده</p></div>
<div class="m2"><p>ز جهل جان و، ز بد دل، ببایدت پالود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو را چگونه پساود هگرز پاکی و علم</p></div>
<div class="m2"><p>که جان و دلت جز از جهل و فعل بد نپسود؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به مال و ملک و به اقبال دهر غره مشو</p></div>
<div class="m2"><p>که تو هنوز ز آتش ندیده‌ای جز دود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جهان مثل چو یکی منزل است بر ره و خلق</p></div>
<div class="m2"><p>درو همی گذرد فوج فوج زودا زود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برادر و پدر و مادرت همه رفتند</p></div>
<div class="m2"><p>تو چند خواهی اندر سفر چنین آسود؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تنت چو پیرهنی بود جانت را و، کنون</p></div>
<div class="m2"><p>همه گسست و بفرسوده گشت تارش و پود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ربود خواهد از تنت پیرهن اکنون</p></div>
<div class="m2"><p>همان که تازگی و رنگ پیرهنت ربود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو باد پیمودی همچو غافلان و فلک</p></div>
<div class="m2"><p>به کیل روز و شبان بر تو عمر تو پیمود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو سالیان‌ها خفتی و آنکه بر تو شمرد</p></div>
<div class="m2"><p>دم شمردن تو، یک نفس زدن نغنود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کنون بباید رفتن سبک به قهر و، سرت</p></div>
<div class="m2"><p>پر از بخار خمار است و چشم خواب آلود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو عبرت دو جهانی که می‌روی و، دلت</p></div>
<div class="m2"><p>ز بخت نا خشنود و خدای ناخشنود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نگاه کن که چه حاصل شدت به آخر کار</p></div>
<div class="m2"><p>از انکه دست و سر و روی سوختی و شخود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چرا به رنج تن بی‌خرد طلب کردی</p></div>
<div class="m2"><p>فزونئی که به عمر تو اندرون نفزود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدان که: هر چه بکشتی ز نیک و بد، فردا</p></div>
<div class="m2"><p>ببایدت همه ناکام و کام پاک درود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدانکه بر تو گواهی دهند هر دو به حق</p></div>
<div class="m2"><p>دو چشم هر چه بدید و دو گوش هر چه شنود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به گمرهی نبود عذر مر تو را پس ازانک</p></div>
<div class="m2"><p>تو را دلیل خداوند راه راست نمود</p></div></div>