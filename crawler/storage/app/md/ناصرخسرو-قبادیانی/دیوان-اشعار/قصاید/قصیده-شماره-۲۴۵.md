---
title: >-
    قصیدهٔ شمارهٔ ۲۴۵
---
# قصیدهٔ شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>ای به خطاها بصیر و جلد وملی</p></div>
<div class="m2"><p>نایدت از کار خویش، خود خجلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ نیابی مرا ز پند و قران</p></div>
<div class="m2"><p>وز غزل و می به طبع در بشلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاصل ناید به جسم و جان تو در</p></div>
<div class="m2"><p>از غزل و می مگر که مفتعلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون عسلی شد زخانت زرد، چرا</p></div>
<div class="m2"><p>با غزل و می به طبع چون عسلی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از غزل و می چو تیر و گل نشود</p></div>
<div class="m2"><p>پشت چو چوگان و روی چون عسلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه برو گفته‌ای سرود و غزل</p></div>
<div class="m2"><p>از تو گسست و تو زو نمی‌گسلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او چو فرو هشت زیر پای تو را</p></div>
<div class="m2"><p>چونکه تو او را ز دل برون نهلی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سنگ تو از گشت چرخ گشت چو گل</p></div>
<div class="m2"><p>کی نگرد سوی تو کنون چگلی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا که چو گل بر بدیدت آن چگلی</p></div>
<div class="m2"><p>هیچ نبودش گمان که تو ز گلی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تازه گلی به درخت ولیک فلک</p></div>
<div class="m2"><p>زو همه بربود تازگی و گلی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر خللی سخت، هیچ خشم مگیر</p></div>
<div class="m2"><p>ازمن اگر گفتمت که بر خللی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور نه جوان شو که هیچ کل نرهد</p></div>
<div class="m2"><p>جز که به جعد سیه ز ننگ کلی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مصحف و تسبیح را سپس چه نهی</p></div>
<div class="m2"><p>چون سپس بربط و می و غزلی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عاجز چونی ز خیر و حق و صواب</p></div>
<div class="m2"><p>ای به خطاها بصیر و جلد و ملی؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون به سجود و رکوع خم ندهی</p></div>
<div class="m2"><p>پشت شنیعت همی کند دغلی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مجلس می را سبکتر از کدوی</p></div>
<div class="m2"><p>مزگت ما را گران‌تر از وحلی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حلهٔ پیریت برفگند جهان</p></div>
<div class="m2"><p>نیست به از زهد و دین کنونت حلی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مستحلا، پیر مستحل نسزد</p></div>
<div class="m2"><p>چونکه نخواهی ازین و آن بحلی؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چونکه ندارد همیت باز کنون</p></div>
<div class="m2"><p>حلیت پیری ز جهل و مستحلی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روز شتاب و خطا گذشت، کنون</p></div>
<div class="m2"><p>وقت صواب است و روز محتملی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پیر پر آهستگی و حلم بود</p></div>
<div class="m2"><p>تو همه پر مکر و زرق و پر حیلی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نام نهی اهل علم و حکمت را</p></div>
<div class="m2"><p>رافضی و قرمطی و معتزلی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رافضیم سوی تو و تو سوی من</p></div>
<div class="m2"><p>ناصبئی نیست جای تنگ دلی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ناصبیا، نیستت مناظره جز</p></div>
<div class="m2"><p>آنکه ز بوبکر به نبود علی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>علم تو حیله است و بانگ بی‌معنی</p></div>
<div class="m2"><p>سوی من، ای ناصبی، تهی دهلی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رخصت داده است مر تو را که بخور</p></div>
<div class="m2"><p>شهره امامت نبید قطربلی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حبل خدائی محمد است چرا</p></div>
<div class="m2"><p>تو به رسن‌های خلق متصلی؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رخصت و حیلت مهارهای تو شد</p></div>
<div class="m2"><p>تو سپس این مهارها جملی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>حیلت و رخصت هبل نهاد تو را</p></div>
<div class="m2"><p>تو تبع مکر حیله‌گر هبلی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نیست امامی پس از رسول مرا</p></div>
<div class="m2"><p>کوفی نه موصلی و نه ختلی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>من ز رسول خدای بی‌بدلم</p></div>
<div class="m2"><p>با بدل خود تو رو که با بدلی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>لات و عزی و منات اگر ولی‌اند</p></div>
<div class="m2"><p>هرسه تو را، مر مرا علی است ولی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ناصبی، ای حجت، ار چه با جدل است</p></div>
<div class="m2"><p>پای ندارد به پیش تو جدلی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>لشکر دیوند جمله اهل جدل</p></div>
<div class="m2"><p>تو جدلی را به حلق در اجلی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خلق همه فتنهٔ بر مثل‌اند</p></div>
<div class="m2"><p>تو ز پس مغز و معنی مثلی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مغز تو داری و پوست اهل مثل</p></div>
<div class="m2"><p>از همگان تو نفور از این قبلی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بی‌امل‌اند این خران ز دانهٔ تو</p></div>
<div class="m2"><p>مردمی از کاه و دانه یا ابلی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون ز ستوری به مردمی نشوی</p></div>
<div class="m2"><p>ای پسر، و از خری برون نچلی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>عامه ستور است و فانی است ستور</p></div>
<div class="m2"><p>ای که خردمند مردم است ازلی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>باد ندارد خطر به پیش جبل</p></div>
<div class="m2"><p>ایشان بادند و تو مثل جبلی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>میر گر از مال و ملک با ثقل است</p></div>
<div class="m2"><p>تو ز کمال و ز علم با ثقلی</p></div></div>