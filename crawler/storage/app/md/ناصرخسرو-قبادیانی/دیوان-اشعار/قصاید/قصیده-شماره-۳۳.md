---
title: >-
    قصیدهٔ شمارهٔ ۳۳
---
# قصیدهٔ شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>چون در جهان نگه نکنی چون است؟</p></div>
<div class="m2"><p>کز گشت چرخ دشت چو گردون است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در باغ و راغ مفرش زنگاری</p></div>
<div class="m2"><p>پر نقش زعفران و طبر خون است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وان ابر همچو کلبهٔ ندافان</p></div>
<div class="m2"><p>اکنون چو گنج لولوی مکنون است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر چرخ، همچو لاله به دشت اندر،</p></div>
<div class="m2"><p>مریخ چون صحیفهٔ پر خون است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جون است باغ و، شاخ سمن پروین</p></div>
<div class="m2"><p>گر ماه نو خمیده چو عرجون است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با چرخ پر ستاره نگه کن چون</p></div>
<div class="m2"><p>پر لاله سبزه در خور و مقرون است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون روی لیلی است گل و پیشش</p></div>
<div class="m2"><p>سرو نوان چو قامت مجنون است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون مشتری است زرد گلت لیکن</p></div>
<div class="m2"><p>این مشتری به عنبر معجون است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مشرق ز نور صبح سحرگاهان</p></div>
<div class="m2"><p>رخشان به سان طارم زریون است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوئی میان خیمهٔ پیروزه</p></div>
<div class="m2"><p>پر زاب زعفران یکی آهون است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دشت ار چنین نبود به ماه دی</p></div>
<div class="m2"><p>باردی بهشت ماه چنین چون است؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صحرا به لاژورد و زر و شنگرف</p></div>
<div class="m2"><p>از بهر چه منقش و مدهون است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاکی که مرده بود و شده ریزان</p></div>
<div class="m2"><p>واکنده چون شد و ز چه گلگون است؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این مشک بوی سرخ گل زنده</p></div>
<div class="m2"><p>زان زشت خاک مردهٔ مدفون است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این مرده را که کرد چنین زنده؟</p></div>
<div class="m2"><p>هر کس که این نداند مغبون است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این کار از آنکه زنده کند آن را</p></div>
<div class="m2"><p>ایزد به حشر مایه و قانون است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وان خشک خار و خس که بسوزندش</p></div>
<div class="m2"><p>فرعون بی‌سلامت و قارون است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این مرده لاله را که شود زنده</p></div>
<div class="m2"><p>نم سلسبیل و محشر هامون است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>واندر حریر سبز و ستبرق‌ها</p></div>
<div class="m2"><p>سیب و بهی چو موسی و هارون است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دوزخ تنور شاید مر خس را</p></div>
<div class="m2"><p>گل را بهشت باغ همایون است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اندر بهشت خواهد بد میوه</p></div>
<div class="m2"><p>آنجا چنین که ایدر و اکنون است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پس هم کنون تو نیز بهشتی شو</p></div>
<div class="m2"><p>کان از قیاس نیز همیدون است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه خار در خور طبق و نحل است</p></div>
<div class="m2"><p>نه گل سزای آتش و کانون است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پس نیست جای مؤمن پاکیزه</p></div>
<div class="m2"><p>دوزخ، که جای کافر ملعون است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه در بهشت خلد شود کافر</p></div>
<div class="m2"><p>کان جایگاه مؤمن میمون است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بندیش از این ثواب و عقاب اکنون</p></div>
<div class="m2"><p>کاین در خرد برابر و موزون است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر دیگر است مردم و گل دیگر</p></div>
<div class="m2"><p>این را بهشت نیز دگرگون است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خرما و میوه‌ها به بهشت اندر</p></div>
<div class="m2"><p>دانی که زین بهست که ایدون است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای رفته بر علوم فلاطونی</p></div>
<div class="m2"><p>این علمها تمام فلاطون است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آن فلسفه است وین سخن دینی</p></div>
<div class="m2"><p>این شکر است و فلسفه هپیون است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از علم خاندان رسول است این</p></div>
<div class="m2"><p>نه گفتهٔ عمرو فریغون است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در خانهٔ رسول چو ماه نو</p></div>
<div class="m2"><p>تاویل روز روز برافزون است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دو کار، خوی نیک و کم آزاری،</p></div>
<div class="m2"><p>فرزند را وصیت مامون است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر بدخو است خار و سمن خوش‌خو</p></div>
<div class="m2"><p>این خود چرا گرامی و آن دون است؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دل را به دین بپوش که دین دل را</p></div>
<div class="m2"><p>در خورد بام و ساخته پرهون است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جان را به علم شوی که مرجان را</p></div>
<div class="m2"><p>علم، ای پسر، مبارک صابون است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بحر است علم را به مثل فرقان</p></div>
<div class="m2"><p>وز بحر علم امام چو جیحون است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جیحون خوش است و با مزه و دریا</p></div>
<div class="m2"><p>از ناخوشی چو زهر و چو طاعون است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای علم جوی، روی به جیحون نه</p></div>
<div class="m2"><p>گر جانت بر هلاک نه مفتون است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دریا نه آب، بل به مثل آب است</p></div>
<div class="m2"><p>چون بر لبش نه تین و نه زیتون است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گرد مثل مگرد که علم او</p></div>
<div class="m2"><p>از طاقت تو جاهل بیرون است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تاویل کن طلب که جهودان را</p></div>
<div class="m2"><p>این قول پند یوشع بن نون است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تاویل بر گزیدهٔ مار جهل</p></div>
<div class="m2"><p>ای هوشیار نادره افسون است</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تاویل حق در شب ترسائی</p></div>
<div class="m2"><p>شمع و چراغ عیسی و شمعون است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>این علم را قرارگه و گشتن</p></div>
<div class="m2"><p>اندر میان حجت و ماذون است</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>این راز را درست کسی داند</p></div>
<div class="m2"><p>که‌ش دل به علم دعوت مشحون است</p></div></div>