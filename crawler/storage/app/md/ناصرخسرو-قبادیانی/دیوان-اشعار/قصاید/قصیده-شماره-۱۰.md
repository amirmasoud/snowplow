---
title: >-
    قصیدهٔ شمارهٔ ۱۰
---
# قصیدهٔ شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>ای گشته جهان و دیده دامش را</p></div>
<div class="m2"><p>صد بار خریده مر دلامش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر لفظ زمانه هر شبانروزی</p></div>
<div class="m2"><p>بسیار شنوده‌ای کلامش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته‌است تو را که «بی مقامم من»</p></div>
<div class="m2"><p>تا چند کنی طلب مقامش را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بارنده به دوستان و یاران بر</p></div>
<div class="m2"><p>نم نیست غم است مر غمامش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون داد نوید رنج و دشواری</p></div>
<div class="m2"><p>آراسته باش مر خرامش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر یخ بنویس چون کند وعده</p></div>
<div class="m2"><p>گفتار محال و قول خامش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز کشتن یار خویش و فرزندان</p></div>
<div class="m2"><p>کاری مشناس مر حسامش را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون چاشت کند ز خویش و پیوندت</p></div>
<div class="m2"><p>تو ساخته باش کار شامش را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بر تو سلام خوش کند روزی</p></div>
<div class="m2"><p>دشنام شمار مر سلامش را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کس را به نظام دیده‌ای حالی</p></div>
<div class="m2"><p>کو رخنه نکرد مر نظامش را؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وز باب و ز مام خویش نربودش</p></div>
<div class="m2"><p>یا زو نر بود باب و مامش را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پرهیز کن از جهان بی‌حاصل</p></div>
<div class="m2"><p>ای خورده جهان و دیده دامش را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>و آگاه کن، ای برادر، از غدرش</p></div>
<div class="m2"><p>دور و نزدیک و خاص و عامش را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن را که همی ازو طمع دارد</p></div>
<div class="m2"><p>گو «ساخته باش انتقامش را»</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر بر فلک است بام کاشانه‌ش</p></div>
<div class="m2"><p>چون دشت شمار پست بامش را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من کز همه حال و کارش آگاهم</p></div>
<div class="m2"><p>هرگز طلبم مراد و کامش را؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وین دل که حلال او نمی‌جوید</p></div>
<div class="m2"><p>چون خواهد جست مر حرامش را؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن را طلب، ای جهان، که جویایست</p></div>
<div class="m2"><p>این بی‌مزه ناز و عز و رامش را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>واشفته بدو سپاری و برکه</p></div>
<div class="m2"><p>شاهنشه ری کنی غلامش را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وز مشتری و قمر بیارائی</p></div>
<div class="m2"><p>مرقبقب زین و اوستامش را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آخر بدهی به ننگ و رسوائی</p></div>
<div class="m2"><p>بی شک یک روز لاف و لامش را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هرچند که شاه نامور باشد</p></div>
<div class="m2"><p>نابوده کنی نشان و نامش را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>واشفته کنی به دست بیدادی</p></div>
<div class="m2"><p>احوال به نظم و نغز و رامش را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بشنو پدرانه، ای پسر، پندی</p></div>
<div class="m2"><p>آن پند که داد نوح سامش را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پرهیز کن از کسی که نشناسد</p></div>
<div class="m2"><p>دنیی و نعیم بی‌قوامش را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وز دل به چراغ دین و علم حق</p></div>
<div class="m2"><p>نتواند برد مر ظلامش را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زو دست بشوی و جز به خاموشی</p></div>
<div class="m2"><p>پاسخ مده، ای پسر، پیامش را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بگذارش تا به دین همی خرد</p></div>
<div class="m2"><p>دنیای مزور و حطامش را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>منگر به مثل جز از ره عبرت</p></div>
<div class="m2"><p>رخسارهٔ خشک چون رخامش را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بل تا بکشد به مکر زی دوزخ</p></div>
<div class="m2"><p>دیو از پس خویشتن لگامش را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر راه امام خود همی نازد</p></div>
<div class="m2"><p>او را مپذیر و مه امامش را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دیوی است حریص و کام او حرصش</p></div>
<div class="m2"><p>بشناس به هوش دیو و کامش را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون صورت و راه دیو او دیدی</p></div>
<div class="m2"><p>بگذار طریقت نغامش را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وانکه بگزار شکر ایزد را</p></div>
<div class="m2"><p>وین منت و نعمت تمامش را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وامی است بزرگ شکر او بر تو</p></div>
<div class="m2"><p>بگزار به جهد و جد وامش را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شکری بگزار علم و دینش را</p></div>
<div class="m2"><p>زان به که شراب یا طعامش را</p></div></div>