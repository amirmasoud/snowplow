---
title: >-
    قصیدهٔ شمارهٔ ۲۷۷
---
# قصیدهٔ شمارهٔ ۲۷۷

<div class="b" id="bn1"><div class="m1"><p>ای خواجه، تو را در دل اگر هست صفائی</p></div>
<div class="m2"><p>بر هستی آن چونکه تو را نیست ضیائی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور باطنت از نور یقین هست منور</p></div>
<div class="m2"><p>بر ظاهر آن چونکه تو را نیست گوائی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آری چو بود ظاهر تحقیق، ز تلبیس</p></div>
<div class="m2"><p>پیدا شود او، همچو صوابی ز خطائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در وصف چو خیری نبود خلق پرستی</p></div>
<div class="m2"><p>در صید چو بازی نبود جوجه ربائی</p></div></div>