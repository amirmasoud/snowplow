---
title: >-
    قصیدهٔ شمارهٔ ۷۸
---
# قصیدهٔ شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>آمد بهار و نوبت صحرا شد</p></div>
<div class="m2"><p>وین سال خورده گیتی برنا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب چو نیل برکه‌ش میگون شد</p></div>
<div class="m2"><p>صحرای سیمگونش خضرا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وان باد چون درفش دی و بهمن</p></div>
<div class="m2"><p>خوش چون بخار عود مطرا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیچاره مشک بید شده عریان</p></div>
<div class="m2"><p>با گوشوار و قرطهٔ دیبا شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخسار دشت‌ها همه تازه شد</p></div>
<div class="m2"><p>چشم شکوفه‌ها همه بینا شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بینا و زنده گشت زمین زیرا</p></div>
<div class="m2"><p>باد صبا فسون مسیحا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بستان ز نو شکوفه چوگردون شد</p></div>
<div class="m2"><p>تا نسترن به سان ثریا شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نیست ابر معجزهٔ یوسف</p></div>
<div class="m2"><p>صحرا چرا چو روی زلیخا شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بشکفت لاله چون رخ معشوقان</p></div>
<div class="m2"><p>نرگس به سان دیدهٔ شیدا شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از برف نو بنفشه گر ایمن گشت</p></div>
<div class="m2"><p>ایدون چرا چو جامهٔ ترسا شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تیره شد آب و گشت هوا روشن</p></div>
<div class="m2"><p>شد گنگ زاغ و بلبل گویا شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بستان بهشت‌وار شد و لاله</p></div>
<div class="m2"><p>رخشان به سان عارض حورا شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون هندوان به پیش گل و بلبل</p></div>
<div class="m2"><p>زاغ سیاه بنده و مولا شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وان گلبن چو گنبد سیمینش</p></div>
<div class="m2"><p>آراسته چو قبهٔ مینا شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون عمروعاص پیش علی دی مه</p></div>
<div class="m2"><p>پیش بهار عاجر و رسوا شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>معزول گشت زاغ چنین زیرا</p></div>
<div class="m2"><p>چون دشمن نبیرهٔ زهرا شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کفر و نفاق از وی چو عباسی</p></div>
<div class="m2"><p>بر جامهٔ سیاهش پیدا شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خورشید فاطمی شد و باقوت</p></div>
<div class="m2"><p>برگشت و از نشیب به بالا شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا نور او چو خنجر حیدر شد</p></div>
<div class="m2"><p>گلبن قوی چو دلدل شهبا شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خورشید چون به معدن عدل آمد</p></div>
<div class="m2"><p>با فصل زمهریر معادا شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>افزون گرفت روز چو دین و شب</p></div>
<div class="m2"><p>ناقص چو کفر و تیره چو سودا شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اهل نفاق گشت شب تیره</p></div>
<div class="m2"><p>رخشنده روز از اهل تولا شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گیتی به سان خاطر بی‌غفلت</p></div>
<div class="m2"><p>پرنور و نفع و خیر ازیرا شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون بود تیره همچو دل جاهل</p></div>
<div class="m2"><p>واکنون چرا چوخاطر دانا شد؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زیرا که سید همه سیاره</p></div>
<div class="m2"><p>اندر حمل به عدل توانا شد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عدل است اصل خیر که نوشروان</p></div>
<div class="m2"><p>اندر جهان به عدل مسما شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بنگر کز اعتدال چو سر برزد</p></div>
<div class="m2"><p>با خور چه چند چیز هویدا شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بنگر که این غریژن پوسیده</p></div>
<div class="m2"><p>یاقوت سرخ و عنبر سارا شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>علم است و عدل نیکی و رسته گشت</p></div>
<div class="m2"><p>آنکو بدین دو معنی گویا شد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>داد خرد بده که جهان ایدون</p></div>
<div class="m2"><p>از بهر عقل و عدل مهیا شد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زیبا به علم شو که نه زیباست</p></div>
<div class="m2"><p>آن کس که او به دنیا زیبا شد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>او را مجوی و علم طلب زیرا</p></div>
<div class="m2"><p>بس کس که او فریفته به آوا شد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>غره مشو بدان که کسی گوید</p></div>
<div class="m2"><p>بهمان فقیه بلخ و بخارا شد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زیرا که علم دینی پنهان شد</p></div>
<div class="m2"><p>چون کار دین و علم به غوغا شد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مپذیر قول جاهل تقلیدی</p></div>
<div class="m2"><p>گرچه به‌نام شهرهٔ دنیا شد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون و چرا بجوی که بر جاهل</p></div>
<div class="m2"><p>گیتی چو حلقه تنگ از اینجا شد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>با خصم گوی علم که بی‌خصمی</p></div>
<div class="m2"><p>علمی نه پاک شد نه مصفا شد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زیرا که سرخ روی برون آمد</p></div>
<div class="m2"><p>هر کو به پیش حاکم تنها شد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خوی مهان بگیر و تواضع کن</p></div>
<div class="m2"><p>آن را که او به دانش والا شد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کز قعر چاه تا به کران رایش</p></div>
<div class="m2"><p>ایدون به چرخ بر به مدارا شد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خاک سیه به‌طاعت خرمابن</p></div>
<div class="m2"><p>بنگر چگونه خوش خوش خرما شد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دانش گزین و صبر طلب زیرا</p></div>
<div class="m2"><p>دارا به صبر و دانش دارا شد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خوی کرام گیر که حری را</p></div>
<div class="m2"><p>خوی کریم مقطع و مبدا شد</p></div></div>