---
title: >-
    قصیدهٔ شمارهٔ ۱۳
---
# قصیدهٔ شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>خداوندی که در وحدت قدیم است از همه اشیا</p></div>
<div class="m2"><p>نه اندر وحدتش کثرت، نه محدث زین همه تنها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه گوئی از چه او عالم پدید آورد از لولو</p></div>
<div class="m2"><p>که نه مادت بد و صورت، نه بالا بود و نه پهنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی گوئی که بر معلول خود علت بود سابق</p></div>
<div class="m2"><p>چنان چون بر عدد واحد، و یا بر کل خود اجزا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به معلولی چو یک حکم است و یک وصف آن دو عالم را</p></div>
<div class="m2"><p>چرا چون علت سابق توانا باشد و دانا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آنچ امروز نتواند به فعل آوردن از قوت</p></div>
<div class="m2"><p>نیاز و عجز اگر نبود ورا چه دی و چه فردا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی گوئی زمانی بود از معلول تا علت</p></div>
<div class="m2"><p>پس از ناچیز محض آورد موجودات را پیدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمانی کز فلک زاید فلک نابوده چون باشد</p></div>
<div class="m2"><p>زمان و چیز ناموجود و ناموجود بی‌مبدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر هیچیز را چیزی نهی قایم به ذات خود</p></div>
<div class="m2"><p>پس آمد نفس وحدت را مضاد و مثل در آلا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و گر زین صورت هیچیز حرف و صوت می‌خواهی</p></div>
<div class="m2"><p>مسلم شد که بی‌معلول نبود علت اسما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تقدم هست یزدان را چو بر اعداد وحدان را</p></div>
<div class="m2"><p>زمان حاصل مکان باطل حدث لازم قدم بر جا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مکن هرگز بدو فعلی اضافت گر خرد داری</p></div>
<div class="m2"><p>بجز ابداع یک مبدع کلمح العین او ادنا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگو فعلش بدان گونه که ذاتش منفعل گردد</p></div>
<div class="m2"><p>چنان کز کمترین قصدی به گاه فعل ذات ما</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مجوی از وحدت محضش برون از ذات او چیزی</p></div>
<div class="m2"><p>که او عام است و ماهیات خاص اندر همه احیا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر از هر بینشش بیرون کنی وصفی برو مفزا</p></div>
<div class="m2"><p>دو باشد بی‌خلاف آنگه نه فرد و واحد و یکتا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر چه بی‌عدد اشیا همی بینی در این عالم</p></div>
<div class="m2"><p>ز خاک و باد و آب و آتش و کانی و از دریا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو هاروت ار توانستی که اینجا آئی از گردون</p></div>
<div class="m2"><p>از اینجا هم توانی شد برون چون زهرهٔ زهرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز گوهر دان نه از هستی فزونی اندر این معنی</p></div>
<div class="m2"><p>که جز یک چیز را یک چیز نبود علت انشا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خرد دان اولین موجود، زان پس نفس و جسم آنگه</p></div>
<div class="m2"><p>نبات و گونهٔ حیوان و آنگه جانور گویا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همی هریک به خود ممکن بدو موجود ناممکن</p></div>
<div class="m2"><p>همی هریک به خود پیدا بدو معدوم ناپیدا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چه گوئی چیست این پرده بر این سان بر هوا برده</p></div>
<div class="m2"><p>چو در صحرای آذرگون یکی خرگاه از مینا؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به خود جنبد همی، ور نی کسی می‌داردش جنبان</p></div>
<div class="m2"><p>و یا بهر چه گردان شد بدین سان گرد این بالا؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو در تحدید جنبش را همی نقل مکان گوئی</p></div>
<div class="m2"><p>و یا گردیدن از حالی به حالی دون یا والا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بیان کن حال و جایش را اگر دانی، مرا، ورنی</p></div>
<div class="m2"><p>مپوی اندر ره حکمت به تقلید از سر عمیا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو نه گنبد همی گوئی به برهان و قیاس، آخر</p></div>
<div class="m2"><p>چه گوئی چیست از بیرون این نه گنبد خضرا؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر بیرون خلا گوئی خطا باشد، که نتواند</p></div>
<div class="m2"><p>بدو در صورت جسمی بدین سان گشته اندروا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وگر گوئی ملا باشد روا نبود که جسمی را</p></div>
<div class="m2"><p>نهایت نبود و غایت به سان جوهر اعلا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چه می‌دارد بدین گونه معلق گوی خاکی را</p></div>
<div class="m2"><p>میان آتش و آب و هوای تندر و نکبا؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر اجزای جهان جمله نهی مایل بر آن جزوی</p></div>
<div class="m2"><p>که موقوف است چون نقطه میان شکل نه سیما</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چرا پس چون هوا او را به قهر از سوی آب آرد</p></div>
<div class="m2"><p>به ساعت باز بگریزد به سوی مولد و منشا؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر ضدند اخشیجان را هر چار پیوسته</p></div>
<div class="m2"><p>بوند از غایت وحدت برادروار در یک جا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>و گر گوئی که در معنی نیند اضداد یک دیگر</p></div>
<div class="m2"><p>تفاوت از چه شان آمد میان صورت و اسما؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز اول هستی خود را نکو بشناس و آنگاهی</p></div>
<div class="m2"><p>عنان برتاب از این گردون وزین بازیچهٔ غبرا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو اسرار الهی را کجا دانی؟ که تا در تو</p></div>
<div class="m2"><p>بود ابلیس با آدم کشیده تیغ در هیجا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو از معنی همان بینی که در بستان جان پرور</p></div>
<div class="m2"><p>ز شکل و رنگ گل بیند دو چشم مرد نابینا</p></div></div>