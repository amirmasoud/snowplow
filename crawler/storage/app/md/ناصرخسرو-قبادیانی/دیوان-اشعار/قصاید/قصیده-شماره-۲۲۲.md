---
title: >-
    قصیدهٔ شمارهٔ ۲۲۲
---
# قصیدهٔ شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>چو رسم جهان جهان پیش بینی</p></div>
<div class="m2"><p>حذر کن ز بدهاش اگر پیش‌بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تاریکی اندر گزاف از پس او</p></div>
<div class="m2"><p>مدو کت برآید به دیوار بینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همانا چنین مانده زین پست از آنی</p></div>
<div class="m2"><p>که در انده اسپ رهوار و زینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو استر سزاوار پالان و قیدی</p></div>
<div class="m2"><p>اگر از پی استر و زین حزینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان مادری گنده پیر است، بر وی</p></div>
<div class="m2"><p>مشو فتنه، گر در خور حور عینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مادر مکن دست، ازیرا که بر تو</p></div>
<div class="m2"><p>حرام است مادر اگر ز اهل دینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی گوهر آسمانی است مردم</p></div>
<div class="m2"><p>که ایزد به بندی ببستش زمینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به شخص گلین چونکه معجب شده‌ستی؟</p></div>
<div class="m2"><p>در این گل بیندیش تا چون عجینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه در خورد در است گل، پس توزین تن</p></div>
<div class="m2"><p>بپرهیز، ازیرا که در ثمینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وطن مر تو را در جهان برین است</p></div>
<div class="m2"><p>تو هرچند امروز در تیره طینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان مهین را به جان زیب و فری</p></div>
<div class="m2"><p>اگرچه بدین تن جهان کهینی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهان برین و فرودین توی خود</p></div>
<div class="m2"><p>به تن زین فرودین به جان زان برینی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سزای همه نعمت این و آنی</p></div>
<div class="m2"><p>ز حکمت ازیرا هم آنی هم اینی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به جان خانهٔ حکمت و علم و فضلی</p></div>
<div class="m2"><p>به تن غایت صنع جان‌آفرینی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر می‌شناسی جهان‌آفرین را</p></div>
<div class="m2"><p>سزاوار هر نعمت و آفرینی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وگر بد سگالی و نشناسی او را</p></div>
<div class="m2"><p>مکافات بد جز بدی خود نبینی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جهانا من از تو هراسان ازانم</p></div>
<div class="m2"><p>که بس بد نشانی و بد همنشینی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خسیسی که جز با خسیسان نسازی</p></div>
<div class="m2"><p>قرینت نیم من که تو بد قرینی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر آزادگان کبر داری ولیکن</p></div>
<div class="m2"><p>ینال و تگین را ینال و تگینی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی بی‌خرد را به گه بر نشانی</p></div>
<div class="m2"><p>یکی بی‌گنه را به سر برنشینی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هم آن را که خود خوانده باشی برانی</p></div>
<div class="m2"><p>هم آن را کنی خوار کش برگزینی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر مردمی بودیئی گفتمی مر</p></div>
<div class="m2"><p>تو را من که دیوانه‌ای راستینی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ولیکن تو این کار ساز اختران را</p></div>
<div class="m2"><p>به فرمان یزدان حصاری حصینی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به خاصه تو ای نحس خاک خراسان</p></div>
<div class="m2"><p>پر از مار و کژدم یکی پارگینی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برآشفته‌اند از تو ترکان نگوئی</p></div>
<div class="m2"><p>میان سگان در یکی ارزبینی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>امیرانت اصل فسادند و غارت</p></div>
<div class="m2"><p>فقیهانت اهل می و ساتگینی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مکان نیستی تو نه دنیا نه دین را</p></div>
<div class="m2"><p>کمین‌گاه ابلیس شوم لعینی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فساد و جفا و بلا و عنا را</p></div>
<div class="m2"><p>براحرار گیتی قراری مکینی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو ای دشمن خاندان پیمبر</p></div>
<div class="m2"><p>ز بهر چه همواره با من به کینی؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو را چشم درد است و من آفتابم</p></div>
<div class="m2"><p>ازیرا ز من رخ پر آژنگ و چینی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سخن تا نگوئی به دینار مانی</p></div>
<div class="m2"><p>ولیکن چو گفتی پشیزی مسینی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو تیره گمانی تو و من یقینم</p></div>
<div class="m2"><p>تو خود زین که من گفتمت بر یقینی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو مر زرق را چون همی فقه خوانی</p></div>
<div class="m2"><p>چه مرد سخن‌های جزل و متینی؟</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خراسان چو بازار چین کرده‌ام من</p></div>
<div class="m2"><p>به تصنیف‌های چو دیبای چینی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو یکسر معین تو گشتند دیوان</p></div>
<div class="m2"><p>وز ابلیس نحس لعین مستعینی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کمینه معینند دیوانت یکسر</p></div>
<div class="m2"><p>که تو خر نه هم گوشهٔ بو معینی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به میدان تو من همی اسپ تازم</p></div>
<div class="m2"><p>تو خوش خفته چون گربه در پوستینی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو ای حجت مؤمنان خراسان</p></div>
<div class="m2"><p>امام زمان را امین و یمینی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برانندت آن گه که ایزدت خواند</p></div>
<div class="m2"><p>به عالم درون آیةالعالمینی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دل مؤمنان را ز وسواس امانی</p></div>
<div class="m2"><p>سر ناصبی را به حجت کدینی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جز از بهر مالش نجوید تو را کس</p></div>
<div class="m2"><p>همانا که تو روغن یاسمینی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بها گیر و رخشانی ای شعر ناصر</p></div>
<div class="m2"><p>مگر خود شعری، بدخشی نگینی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بر اعدای دین زهری و مؤمنان را</p></div>
<div class="m2"><p>غذائی، مگر روغن و انگبینی؟</p></div></div>