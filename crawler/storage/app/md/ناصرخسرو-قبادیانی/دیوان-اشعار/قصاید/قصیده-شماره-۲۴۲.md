---
title: >-
    قصیدهٔ شمارهٔ ۲۴۲
---
# قصیدهٔ شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>دگر ره باز با هر کوهساری</p></div>
<div class="m2"><p>بخار آورد پیدا خار خاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همان شخ که‌ش حریرین بود قرطه</p></div>
<div class="m2"><p>همی از خر بر بندد ازاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ابر اندر حصاری گشت کهسار</p></div>
<div class="m2"><p>شنوده‌ستی حصاری در حصاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی فرش پرندین برنوردد</p></div>
<div class="m2"><p>شمال اکنون زهر کوهی و غاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خزان از مهرگان دارد پیامی</p></div>
<div class="m2"><p>سوی هر باغ و دشت مرغزاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پر از بادست که را سر دگر بار</p></div>
<div class="m2"><p>گران‌تر زو ندیدم بادساری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو ابدالان همیشه در رکوع است</p></div>
<div class="m2"><p>به باغ اندر ز بر هر میوه‌داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز هر شاخی یکی میوه در آویخت</p></div>
<div class="m2"><p>چو از پستان مادر شیرخواری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو مستوفی شد اکنون، زان بخواهد</p></div>
<div class="m2"><p>شمال از هر درخت اکنون شماری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز چندین پر زر و زیور عروسان</p></div>
<div class="m2"><p>کنون تا نه فراوان روزگاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نماند با عروسی روی بندی</p></div>
<div class="m2"><p>نه طوق و یاره‌ای یا گوشواری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بهر حمله شمال اکنون بریزد</p></div>
<div class="m2"><p>گنه ناکرده خون لاله‌زاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بلی زار است کار گل ولیکن</p></div>
<div class="m2"><p>به زاری نیست همچون لاله زاری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به خون اندر همی غلتد که دهقان</p></div>
<div class="m2"><p>نبیند خون او را خواستاری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهی برشاخ ازاین اندوه مانده است</p></div>
<div class="m2"><p>نژند و زرد همچون سوکواری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جهان چون شاد خواری بود لیکن</p></div>
<div class="m2"><p>بماند آن شاد خوار اکنون چوخواری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به پیری و به خواری باز گردد</p></div>
<div class="m2"><p>به آخر هر جوان و شاد خواری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جهان با هیچ‌کس صحبت نجوید</p></div>
<div class="m2"><p>کزو بر ناورد روزی دماری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو گشت آشفته گردد پیشگاهی</p></div>
<div class="m2"><p>رهی و بنده پیش پیشکاری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خر بدخوست این پر بار محنت</p></div>
<div class="m2"><p>حرونی پر عواری بی‌فساری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیابی از خردمندان کسی را</p></div>
<div class="m2"><p>که او را اندر این خر نیست باری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نگه کن تا بر این خر کس نشسته است</p></div>
<div class="m2"><p>که این بد خر نکرده‌ستش فگاری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ازو پرهیز کن چون گشتی آگاه</p></div>
<div class="m2"><p>که جز فعل بد او را نیست کاری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>منش بسیار دیدم و آزمودم</p></div>
<div class="m2"><p>چه گویم؟ گویم این ماری است، ماری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جز از غدر و جفا هرچند گشتم</p></div>
<div class="m2"><p>ندیدم کار او را پود و تاری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کجا نوری پدید آید هم‌آنجا</p></div>
<div class="m2"><p>ز بد فعلی برانگیزد غباری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو را چون غمگساری داد گیتی</p></div>
<div class="m2"><p>دلت شاد است و داری کاروباری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نه‌ای آگه که گر غمی نبودی</p></div>
<div class="m2"><p>نبایستت هرگز غمگساری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نباید تا نباشد جرم عذری</p></div>
<div class="m2"><p>نه صلحی، تا نباشد کارزاری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جهان جای خلاف و بر فرودست</p></div>
<div class="m2"><p>جزین مر مردمان را نیست کاری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو معذوری که نشناسیش ازیرا</p></div>
<div class="m2"><p>نخسته‌ستت هنوز از دهر خاری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو با او، ای پسر، روگر خوش آمدت</p></div>
<div class="m2"><p>پدر را هیچ عذری نیست باری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گرفتم در کنارش روزگاری</p></div>
<div class="m2"><p>کنون شاید کزو گیرم کناری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اگر من به اختیارم برتن خویش</p></div>
<div class="m2"><p>نکردم جز که پرهیز اختیاری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خلاف است اهل دین را اهل دنیا</p></div>
<div class="m2"><p>بداند هر حکیمی بی‌مداری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نکرد این اختیار از خلق عالم</p></div>
<div class="m2"><p>جز ابدالی حکیمی بختیاری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مرا دین است یارو جفت،هرگز</p></div>
<div class="m2"><p>اگر حق را نباشد حق‌گزاری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر با من نسازند اهل دنیا</p></div>
<div class="m2"><p>به من بر آن نباشد هیچ عاری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خرد ما را به کار آید اگر چند</p></div>
<div class="m2"><p>نمی‌دارد به کارش نابکاری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خرد بار درخت مردم آمد</p></div>
<div class="m2"><p>بدو باغی جدا گشت از چناری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خرد بر دلت بنگاری ازیرا</p></div>
<div class="m2"><p>ازو به نیست مر دل را نگاری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سواری گر خرد برتو سوار است</p></div>
<div class="m2"><p>که همچون تو نبیند کس سواری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مرا شهری است این دل پر ز حکمت</p></div>
<div class="m2"><p>مرا بین تا ببینی شهریاری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بگوش دل نگر زی من که چشمت</p></div>
<div class="m2"><p>یکی از من نبیند از هزاری</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ببین در لفظ و معنی‌ها و رمزم</p></div>
<div class="m2"><p>بهاری در بهاری در بهاری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مرا این روزگار آموزگار است</p></div>
<div class="m2"><p>کزین به نیست‌مان آموزگاری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز بسیاری که بردم بار رنجش</p></div>
<div class="m2"><p>شدم، گرچه نبودم، بردباری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مجوی از کس شکاری گر نخواهی</p></div>
<div class="m2"><p>که جوید دیگری از تو شکاری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خردمندا، تو را شعرم نثار است</p></div>
<div class="m2"><p>نثاری کان به است از هر نثاری</p></div></div>