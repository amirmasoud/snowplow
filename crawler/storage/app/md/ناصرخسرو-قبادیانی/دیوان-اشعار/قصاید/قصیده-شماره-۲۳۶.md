---
title: >-
    قصیدهٔ شمارهٔ ۲۳۶
---
# قصیدهٔ شمارهٔ ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>بگذر ای باد دل‌افروز خراسانی</p></div>
<div class="m2"><p>بر یکی مانده به یمگان دره زندانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر این تنگی بی‌راحت بنشسته</p></div>
<div class="m2"><p>خالی از نعمت وز ضیعت و دهقانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برده این چرخ جفا پیشه به بیدادی</p></div>
<div class="m2"><p>از دلش راحت وز تنش تن آسانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل پراندوه‌تر از نار پر از دانه</p></div>
<div class="m2"><p>تن گدازنده‌تر از نال زمستانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داده آن صورت و آن هیکل آبادان</p></div>
<div class="m2"><p>روی زی زشتی و آشفتن و ویرانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشته چون برگ خزانی ز غم غربت</p></div>
<div class="m2"><p>آن رخ روشن چون لالهٔ نعمانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی بر تافته زو خویش چو بیگانه</p></div>
<div class="m2"><p>دستگیریش نه جز رحمت یزدانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌گناهی شده همواره برو دشمن</p></div>
<div class="m2"><p>ترک و تازی و عراقی و خراسانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهنه جویان و جزین هیچ بهانه نه</p></div>
<div class="m2"><p>که تو بد مذهبی و دشمن یارانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه سخن گویم من با سپه دیوان؟</p></div>
<div class="m2"><p>نه مرا داد خداوند سلیمانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیش نایند همی هیچ مگر کز دور</p></div>
<div class="m2"><p>بانگ دارند همی چون سگ کهدانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از چنین خصم یکی دشت نیندیشم</p></div>
<div class="m2"><p>به گه حجت، یارب تو همی دانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لیکن از عقل روا نیست که از دیوان</p></div>
<div class="m2"><p>خویشتن را نکند مرد نگه‌بانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرد هشیار سخن‌دان چه سخن گوید</p></div>
<div class="m2"><p>با گروهی همه چون غول بیابانی؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که بود حجت بیهوده سوی جاهل</p></div>
<div class="m2"><p>پیش گوساله نشاید که قران‌خوانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نکند با سفها مرد سخن ضایع</p></div>
<div class="m2"><p>نان جو را که دهد زیرهٔ کرمانی؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن همی گوید امروز مرا بد دین</p></div>
<div class="m2"><p>که به جز نام نداند ز مسلمانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای نهاده بر سر اندر کله دعوی</p></div>
<div class="m2"><p>جانت پنهان شده در قرطه نادانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به که باید گرویدن زپس ازاحمد؟</p></div>
<div class="m2"><p>چیست نزد تو برین حجت‌برهانی؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو چه دانی که بود آنکه خر لنگت</p></div>
<div class="m2"><p>تو همی براثر استر او رانی؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون تو بدبخت فضولی نه چو گمراهان</p></div>
<div class="m2"><p>انده جهل خوری و غم حیرانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سخت بی پشت بوند و ضعفا قومی</p></div>
<div class="m2"><p>که تو پشت و سپه و قوت ایشانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون نکوشی که بپوشی شکم و عورت</p></div>
<div class="m2"><p>دیگران را چه دهی خیره گریبانی؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر کسی دیبا پوشد تو چرا نازی</p></div>
<div class="m2"><p>چو خود اندر سلب ژنده و خلقانی؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر تن خویش تو را قرطه کرباسی</p></div>
<div class="m2"><p>به چو بر خالت دیبای سپاهانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فضل یاران نکند سود تو را فردا</p></div>
<div class="m2"><p>چو پدید آید آن قوت پنهانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هیچ از آن فضل ندادند تو را بهری</p></div>
<div class="m2"><p>یا سزاوار ندیدندت و ارزانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پیش من چون بنجنبدت زبان هرگز؟</p></div>
<div class="m2"><p>خیره پیش ضعفا ریش همی لانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خرداومند سخن‌دان به‌تو برخندد</p></div>
<div class="m2"><p>چو مر آن بی‌خردان را تو بگریانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر تو را یاران زهاد وبزرگان‌اند</p></div>
<div class="m2"><p>چون تو بر سیرت وبر سنت دیوانی؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سیرت راه‌زنان داری لیکن تو</p></div>
<div class="m2"><p>جز که بستان و زر و ضیعت نستانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روز با روزه و با ناله و تسبیحی</p></div>
<div class="m2"><p>شب با مطرب و با باده ریحانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>باده پخته حلال است به نزد تو</p></div>
<div class="m2"><p>که تو بر مذهب بو یوسف و نعمانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کتب حیلت چون آب ز بر داری</p></div>
<div class="m2"><p>مفتی بلخ‌و نیشابور و هری زانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر کسی چون ز قضا سخت شود بندی</p></div>
<div class="m2"><p>تو مر آن را به یکی نکته بگردانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>با چنین حکم مخالف که همی بینی</p></div>
<div class="m2"><p>تو فرومایه پدرزاده شیطانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا به گفتاری پربار یکی نخلی</p></div>
<div class="m2"><p>چون به فعل آئی پرخار مغیلانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>من از استاد تو دیو و ز تو بیزارم</p></div>
<div class="m2"><p>گفتم اینک سخن کوته و پایانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>روی زی حضرت آل نبی آوردم</p></div>
<div class="m2"><p>تا بدادند مرا نعمت دوجهانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگر او خانه و از اهل جدا ماندم</p></div>
<div class="m2"><p>جفت گشته‌ستم با حکمت لقمانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پیش داعی من امروز چو افسانه است</p></div>
<div class="m2"><p>حکمت ثابت بن قرهٔ حرانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>داغ مستنصر بالله نهاده‌ستم</p></div>
<div class="m2"><p>بر برو سینه و بر پهنهٔ پیشانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آن خداوند که صد شکر کند قیصر</p></div>
<div class="m2"><p>گر به باب الذهب آردش به دربانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فضل دارد چو فلک بر زمی از فخرش</p></div>
<div class="m2"><p>سنگ درگاهش بر لعل بدخشانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>میرزاده است و ملک زاده به درگاهش</p></div>
<div class="m2"><p>بسی از رازی وز خانه و سامانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>که بدان حضرت جدان و نیاکان‌شان</p></div>
<div class="m2"><p>پیش ازین آمده بودند به مهمانی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>این چنین احسان بر خلق کرا باشد</p></div>
<div class="m2"><p>جز کسی را که ندارد ز جهان ثانی؟</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ای به ترکیب شریف تو شده حاصل</p></div>
<div class="m2"><p>غرض ایزدی از عالم جسمانی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نور از اقبال و ز سلطان تو می‌جوید</p></div>
<div class="m2"><p>چون بتابد ز شرف کوکب سرطانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آنکه عاصی شد مر جد تو آدم را</p></div>
<div class="m2"><p>چون تو را دید بسی خورد پشیمانی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گر بدو بنگری امروز یکی لحظت</p></div>
<div class="m2"><p>طاعتی گردد و بیچاره و فرمانی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گیتی امید به اقبال تو می‌دارد</p></div>
<div class="m2"><p>که ازو گرد به شمشیر بیوشانی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو بدو بنگری آنگاه به صلح آید</p></div>
<div class="m2"><p>این خلاف از همه آفاق و پریشانی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو به بغداد فروآئی پیش آرد</p></div>
<div class="m2"><p>دیو عباسی فرزند به قربانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سنگ یمگان دره زی من رهی طاعت</p></div>
<div class="m2"><p>فضلها دارد بر لولوی عمانی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نعمت عالم باقی چو مرا دادی</p></div>
<div class="m2"><p>چه براندیشم ازاین بی مزهٔ فانی؟</p></div></div>