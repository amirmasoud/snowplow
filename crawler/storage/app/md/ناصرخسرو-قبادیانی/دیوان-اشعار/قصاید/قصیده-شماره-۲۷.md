---
title: >-
    قصیدهٔ شمارهٔ ۲۷
---
# قصیدهٔ شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>باز جهان تیز پر و خلق شکار است</p></div>
<div class="m2"><p>باز جهان را جز از شکار چه کار است؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست جهان خوار سوی ما، ز چه معنی</p></div>
<div class="m2"><p>خوردن ما سوی باز او خوش و خوار است؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قافله هرگز نخورد و راه نزد باز</p></div>
<div class="m2"><p>باز جهان ره زن است و قافله‌خوار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صحبت دنیا مرا نشاید ازیراک</p></div>
<div class="m2"><p>صحبت او اصل ننگ و مایهٔ عار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صحبت دنیا به سوی عاقل و هشیار</p></div>
<div class="m2"><p>صحبت دیوار پر ز نقش و نگار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار جهان همچو کار بی‌هش مستان</p></div>
<div class="m2"><p>یکسره ناخوب و پر ز عیب و عوار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لاجرم از خلق جز که مست و خسان را</p></div>
<div class="m2"><p>بر در این مست بر، نه جاه و نه بار است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوی جهان بار مر تو راست ازیراک</p></div>
<div class="m2"><p>معده‌ت پر خمر و مغز پر ز خمار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جانت شش ماه پر ز مهر خزان است</p></div>
<div class="m2"><p>شش مه ازان پس پر از نشاط بهار است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا به عصیر و به سبزه شاد نباشی!</p></div>
<div class="m2"><p>خوردن و رفتن به سبزه کار حمار است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غره چرا گشته‌ای به مکر زمانه</p></div>
<div class="m2"><p>گر نه دماغت پر از فساد و بخار است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دستهٔ گل گر تو را دهد تو چنان دانک</p></div>
<div class="m2"><p>دستهٔ گل نیست آن، که پشتهٔ خار است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>میوهٔ او را نه هیچ بوی و نه رنگ است</p></div>
<div class="m2"><p>جامهٔ او را نه هیچ پود و نه تار است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روی امیدت به زیر گرد نمیدی است</p></div>
<div class="m2"><p>گرت گمان است کاین سرای قرار است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روی نیارم سوی جهان که بیارم</p></div>
<div class="m2"><p>کاین به سوی من بتر ز گرسنه مار است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر که بدانست خوی او ز حکیمان</p></div>
<div class="m2"><p>همره این مار صعب رفت نیار است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رهبری از وی مدار چشم که دیو است</p></div>
<div class="m2"><p>میوهٔ خوش زو طمع مکن که چنار است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بهرهٔ تو زین زمانه روز گذاری است</p></div>
<div class="m2"><p>بس کن ازو این قدر که با تو شمار است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جان عزیز تو بر تو وام خدای است</p></div>
<div class="m2"><p>وام خدای است بر تو، کار تو زار است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جز به همان جان گزارده نشود وام</p></div>
<div class="m2"><p>گرت چه بسیار مال و دست گزار است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این رمه مر گرگ مرگ راست همه پاک</p></div>
<div class="m2"><p>آنکه چون دنبه است و آنکه خشک و نزار است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مانده به چنگال گرگ مرگ شکاری</p></div>
<div class="m2"><p>گر چه تو را شیر مرغزار شکار است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر تو از این گرگ دردمند و فگاری</p></div>
<div class="m2"><p>جز تو بسی نیز دردمند و فگار است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای شده غره به مال و ملک و جوانی</p></div>
<div class="m2"><p>هیچ بدینها تو را نه جای فخار است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فخر به خوبی و زر و سیم زنان راست</p></div>
<div class="m2"><p>فخر من و تو به علم و رای و وقار است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چونکه به من ننگری ز کبر و سیاست؟</p></div>
<div class="m2"><p>من چه کنم گر تو را ضیاع و عقار است؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>من شرف و فخر آل خویش و تبارم</p></div>
<div class="m2"><p>گر دگری را شرف به آل و تبار است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آنکه بود بر سخن سوار، سوار اوست</p></div>
<div class="m2"><p>آن که نه سوار است کو بر اسپ سوار است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شهره درختی است شعر من که خرد را</p></div>
<div class="m2"><p>نکته و معنی برو شکوفه و بار است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>علم عروض از قیاس بسته حصاری است</p></div>
<div class="m2"><p>نفس سخن گوی من کلید حصار است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرکب شعر و هیون علم و ادب را</p></div>
<div class="m2"><p>طبع سخن سنج من عنان و مهار است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا سخنم مدح خاندان رسول است</p></div>
<div class="m2"><p>نابغه طبع مرا متابع و یار است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خیل سخن را رهی و بندهٔ من کرد</p></div>
<div class="m2"><p>آنکه ز یزدان به علم و عدل مشار است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مشتری اندر نمازگاه مر او را</p></div>
<div class="m2"><p>پیش رو و، جبرئیل غاشیه‌دار است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>طلعت «مستنصر از خدای» جهان را</p></div>
<div class="m2"><p>ماه منیر است و، این جهان شب تار است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>روح قدس را ز فخر روزی صد راه</p></div>
<div class="m2"><p>گرد درو مجلسش مجال و مدار است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قیصر رومی به قصر مشرف او در</p></div>
<div class="m2"><p>روز مظالم ز بندگان صغار است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خلق شمارند و او هزار ازیراک</p></div>
<div class="m2"><p>هر چه شمار است جمله زیر هزار است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>رایت او روز جنگ شهره درختی است</p></div>
<div class="m2"><p>کش ظفر و فتح برگ‌ها و ثمار است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مرکب او را چو روی سوی عدو کرد</p></div>
<div class="m2"><p>نصرت و فتح از خدای عرش نثار است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خون عدو را چو خویش بدو داد</p></div>
<div class="m2"><p>دیگ در قصر او بزرگ طغار است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پیش عدوخوار ذوالفقار خداوند</p></div>
<div class="m2"><p>شخص عدو روز گیر و دار خیار است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا ننهد سر به خط طاعت او بر</p></div>
<div class="m2"><p>ناصبی شوم را سر از در دار است</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ناصبی شوم را به مغز سر اندر</p></div>
<div class="m2"><p>حکمت حجت بخار و دود شخار است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نیست سر پر فساد ناصبی شوم</p></div>
<div class="m2"><p>از در این شعر، بل سزای فسار است</p></div></div>