---
title: >-
    قصیدهٔ شمارهٔ ۴۵
---
# قصیدهٔ شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>هر چه دور از خرد همه بند است</p></div>
<div class="m2"><p>این سخن مایهٔ خردمند است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کارها را بکشی کرد خرد</p></div>
<div class="m2"><p>بر ره ناسزا نه خرسند است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل مپیوند تا نشاید بود</p></div>
<div class="m2"><p>گرت پاداش ایچ پیوند است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وهم جانت مبر به جز توحید</p></div>
<div class="m2"><p>کان دگر کیمیای دلبند است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخت اندر نگر موحد باش</p></div>
<div class="m2"><p>که سلب را بپا که افگنده است؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر خداوندی از نیاز مترس</p></div>
<div class="m2"><p>که رهی مر تو را خداوند است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غمت آسان گذار نیز و بدان</p></div>
<div class="m2"><p>مادرت برگذار فرزند است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای رفیق اندرون نگر به جهان</p></div>
<div class="m2"><p>تا چو تو چند بود یا چند است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این جهان نیست با تو عمر دراز</p></div>
<div class="m2"><p>مر تو را عمر خود دم و بند است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مکن امید دور آز دراز</p></div>
<div class="m2"><p>گردش چرخ بین که گریند است</p></div></div>