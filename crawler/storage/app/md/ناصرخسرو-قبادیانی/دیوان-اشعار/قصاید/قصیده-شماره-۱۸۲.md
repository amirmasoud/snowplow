---
title: >-
    قصیدهٔ شمارهٔ ۱۸۲
---
# قصیدهٔ شمارهٔ ۱۸۲

<div class="b" id="bn1"><div class="m1"><p>بشنو که چه گوید همیت دوران</p></div>
<div class="m2"><p>پیغام ازین چرخ گرد گردان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین قبهٔ پر چشمهای بیدار</p></div>
<div class="m2"><p>زین طارم پر شمع‌های رخشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این سبز بیابان که چون شب آید</p></div>
<div class="m2"><p>پر لاله شود همچو باغ نیسان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وین بحر بی‌آرامش نگون‌سار</p></div>
<div class="m2"><p>آراسته قعرش به در و مرجان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین کلهٔ نیلی کزو نمایند</p></div>
<div class="m2"><p>رخشنده رخان دختران ریان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیغام فلک بر زبان دوران</p></div>
<div class="m2"><p>آن است به سوی نبات و حیوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کای نو شدگانی که می‌فزائید</p></div>
<div class="m2"><p>یک روز بکاهید هم بر این سان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چونان که همی بامداد روشن</p></div>
<div class="m2"><p>تاریک شود وقت شام‌گاهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نابوده که بوده شود نپاید</p></div>
<div class="m2"><p>زین است جهان در زوال و سیلان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جنبنده همه جمله بودگانند</p></div>
<div class="m2"><p>برهانت بس است بر فنای گیهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اولاد جهان چون همی نپایند</p></div>
<div class="m2"><p>پاینده نباشد همان پدرشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو عالم خردی ضعیف و دانا</p></div>
<div class="m2"><p>وین عالم مردی بزرگ و نادان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عمر تو چو تو خرد و، عمر عالم</p></div>
<div class="m2"><p>مانند کلان شخص او فراوان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن عمر که آخر فنا پذیرد</p></div>
<div class="m2"><p>پیوسته بود به ابتداش پایان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرسودن اشخاص بودشی را</p></div>
<div class="m2"><p>ایام بسنده است تیز سوهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرچ آن به زمان باقی است بودش</p></div>
<div class="m2"><p>سوهان زمانش بساید آسان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس عالم گر بی‌زمانه بوده است</p></div>
<div class="m2"><p>نابود شود بی‌زمان به فرمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آباد که کرده‌است این جهان را؟</p></div>
<div class="m2"><p>ناچار همان کس کندش ویران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از بهر که کرد آنکه کرد، گوئی،</p></div>
<div class="m2"><p>این پر ز نعیم و فراخ بستان؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از بهر چه کرد آنکه کرد پنهان</p></div>
<div class="m2"><p>در خاک سیه زر و، سیم در کان؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زندان تو است این اگرت باغ است</p></div>
<div class="m2"><p>بستان نشناسی همی ز زندان؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر خویشتن این بندهای بسته</p></div>
<div class="m2"><p>بنگر به رسن‌های سخت و الوان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بنگر که بدین بند بسته در، چیست</p></div>
<div class="m2"><p>در بند چرا بسته گشت پنهان؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در بند بود مستمند بندی</p></div>
<div class="m2"><p>تو شاد چرائی به بند و خندان؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بندی که شنوده است مانده هموار</p></div>
<div class="m2"><p>بر هر که رها شد ز بند گریان؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این قفل که داند گشادن از خلق؟</p></div>
<div class="m2"><p>آن کیست که بگشاد قفل یزدان؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون باز نجوئی که اندر این باب</p></div>
<div class="m2"><p>تازیت چه گفت و چه گفت دهقان؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یا از طلب این چنین معانی</p></div>
<div class="m2"><p>مشغول شده‌ستی به فرج و دندان؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وان را که همی جوید این چنین‌ها</p></div>
<div class="m2"><p>می چیز نبخشند ترکمانان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گویدت فلان ک «ز چنین سخن‌ها</p></div>
<div class="m2"><p>مانده است به زندان فلان به یمگان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>منگر به سخن‌های او ازیرا</p></div>
<div class="m2"><p>ترکانش براندند از خراسان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه میر خراسان پسندد او را</p></div>
<div class="m2"><p>نه شاه کرکان نه میر جیلان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر مذهب او حق و راست بودی</p></div>
<div class="m2"><p>در بلخ بدی به اتفاق اعیان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>این بیهده‌ها را اگر ندانی</p></div>
<div class="m2"><p>در کار نیایدت هیچ نقصان»</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای کرده تو را فتنه اهل باطل</p></div>
<div class="m2"><p>بر حدثنا عن فلان و بهمان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر جهل تو را درد کردی، از تو</p></div>
<div class="m2"><p>بر گنبد کیوان رسیدی افغان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مغز است تو را ریم گرچه شوئی</p></div>
<div class="m2"><p>دستار به صابون و تن به اشنان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>طعنه چه زنی مر مرا بدان که‌م</p></div>
<div class="m2"><p>از خانه براندند اهل عصیان؟</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زیرا که براندند مصطفی را</p></div>
<div class="m2"><p>ذریت شیطان از اهل و اوطان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بر نوح همی سرزنش نیامد</p></div>
<div class="m2"><p>کو رفت به کوه از میان طوفان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>من بستهٔ آداب و فضل خویشم</p></div>
<div class="m2"><p>در تنگ زمینی زجور دیوان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از لحن فراوان و خوش بماند</p></div>
<div class="m2"><p>در تنگ قفس‌ها هزاردستان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>وز بهر هنر گوز را به خردی</p></div>
<div class="m2"><p>بیرون فگنند از میان اغصان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چون من به بیان بر زبان گشادم</p></div>
<div class="m2"><p>لرزان شود آفاق و لولو ارزان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خورشید به آواز خاطرم را</p></div>
<div class="m2"><p>گوید که فگندی مرا ز سرطان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در دین به خراسان که شست جز من</p></div>
<div class="m2"><p>رخسارهٔ دعوی به آب برهان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>پیغام فلک مر تو را نمایم</p></div>
<div class="m2"><p>بر خاک نبشته به خط رحمان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چشمیت گشایم کزو ببینی</p></div>
<div class="m2"><p>بنوشته به خط خدای فرقان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>لیکن ننمایت راه هارون</p></div>
<div class="m2"><p>تا باز نگردی ز راه هامان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دیوان برمیدند چون بدیدند</p></div>
<div class="m2"><p>در دست من انگشتری‌ی سلیمان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زین است که ایدون خران دین را</p></div>
<div class="m2"><p>از من بفشرده است سخت پالان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>من شیعت اولاد مصطفی‌ام</p></div>
<div class="m2"><p>در دین نروم جز به راه ایشان</p></div></div>