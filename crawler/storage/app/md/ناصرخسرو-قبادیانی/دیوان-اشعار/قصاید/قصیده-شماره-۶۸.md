---
title: >-
    قصیدهٔ شمارهٔ ۶۸
---
# قصیدهٔ شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>بالای هفت چرخ مدور دو گوهرند</p></div>
<div class="m2"><p>کز نور هر دو عالم و آدم منورند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر مشیمهٔ عدم از نطفهٔ وجود</p></div>
<div class="m2"><p>هر دو مصورند ولی نامصورند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محسوس نیستند و نگنجند در حواس</p></div>
<div class="m2"><p>نایند در نظر که نه مظلم نه انورند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پروردگان دایهٔ قدسند در قدم</p></div>
<div class="m2"><p>گوهرنیند اگرچه به اوصاف گوهرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین سوی آفرینش و زان سوی کاینات</p></div>
<div class="m2"><p>بیرون و اندرون زمانه مجاورند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر جهان نیند هم ایشان و هم جهان</p></div>
<div class="m2"><p>در ما نیند و در تن ما روح پرورند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویند هر دو هر دو جهانند، از این قبل</p></div>
<div class="m2"><p>در هفت کشورند و نه در هفت کشورند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این روح قدس آمد و آن ذات جبرئیل</p></div>
<div class="m2"><p>یعنی فرشتگان پرانند و بی‌پرند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌بال در نشیمن سفلی گشاده پر</p></div>
<div class="m2"><p>بی پر بر آشیانهٔ علوی همی پرند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با گرم و سرد عالم و خشک و تر جهان</p></div>
<div class="m2"><p>چون خاک و باد هم نفس آب و آذرند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در گنج خانهٔ ازل و مخزن ابد</p></div>
<div class="m2"><p>هر دو نه جوهرند ولی نام جوهرند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم عالم‌اند و آدم و هم دوزخ و بهشت</p></div>
<div class="m2"><p>هم حاضرند و غایب و هم زهر و شکرند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وز نور تا به ظلمت و ز اوج تا حضیض</p></div>
<div class="m2"><p>وز باختر به خاور وز بحر تا برند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هستند و نیستند و نهانند و آشکار</p></div>
<div class="m2"><p>زان بی تواند و با تو به یک خانه اندرند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در عالم دوم که بود کارگاهشان</p></div>
<div class="m2"><p>ویران کنندگان بنا و بناگرند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روزی دهان پنج حواس و چهار طبع</p></div>
<div class="m2"><p>خوالیگران نه فلک و هفت اخترند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وز مشرفان ده‌اند به‌گرد سرایشان</p></div>
<div class="m2"><p>زان پنج اندرون و از آن پنج بردرند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در پیش هر دو هر دو دکان‌دار آسمان</p></div>
<div class="m2"><p>استاده هر چه دیر فروشد همی خرند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وان پادشاه ده سر و شش روی و هفت چشم</p></div>
<div class="m2"><p>با چار خصمشان به یکی خانه اندرند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جوهر نیند و جوهر ایشان بود عرض</p></div>
<div class="m2"><p>محور نهادهٔ عرضند و نه محورند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خوانند برتو نامهٔ اسرار بی‌حروف</p></div>
<div class="m2"><p>دانند کرده‌های تو بی آنکه بنگرند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیدا از آن شدند که گشتند ناپدید</p></div>
<div class="m2"><p>زان بی تن و سرند که اندر تن و سرند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وین از صفت بود که نگنجند در جهان</p></div>
<div class="m2"><p>وانگاه در تن و سر ما هر دو مضمرند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن جایگان بهر تو را ساختند جای</p></div>
<div class="m2"><p>ور نه کدام جای؟ که از جای برترند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سوی تو آمدند ز جائی که جای نیست</p></div>
<div class="m2"><p>آنجا فرشته‌اند و بدین‌جا پیمبرند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بالای مدرج ملکوت‌اند در صفات</p></div>
<div class="m2"><p>چون ذات ذوالجلال نه عنصر نه جوهرند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با آنکه هست هر دو جهان ملک این و آن</p></div>
<div class="m2"><p>نفس تو را اگر تو بخواهی مسخرند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفتارشان بدان و به گفتار کار کن</p></div>
<div class="m2"><p>تا از خدای عزوجل وحیت آورند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بنگر به سایرات فلک را که بر فلک</p></div>
<div class="m2"><p>ایشان زحضرت ملک‌العرش لشکرند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بی‌دانشان اگرچه نکوهش کنندشان</p></div>
<div class="m2"><p>آخر مدبران سپهر مدورند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چندین هزار دیده و گوش از برای چیست؟</p></div>
<div class="m2"><p>زیشان سخن مگوی که هم کور و هم کرند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گوئی مرا که گوهر دیوان ز آتش است</p></div>
<div class="m2"><p>دیوان این زمانه همه از گل مخمرند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جز آدمی نزاد ز آدم در این جهان</p></div>
<div class="m2"><p>وینها از آدم‌اند چرا جملگی خرند؟</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دعوی کنند چه که براهیم زاده‌ایم؟</p></div>
<div class="m2"><p>چون ژرف بنگری همه شاگرد آزرند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در بزم‌گاه مالک ساقی‌ی زبانیند</p></div>
<div class="m2"><p>این ابلهان که در طلب جام کوثرند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خوشی کجاست اینجا؟ کاینجا برادران</p></div>
<div class="m2"><p>از بهر لقمه‌ای هم خصم برادرند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بعد از هزار سال همانی که اولت</p></div>
<div class="m2"><p>زین در درآورند و از آن در برون برند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اینها که آمدند چه دیدند از این جهان؟</p></div>
<div class="m2"><p>رفتند و ما رویم و بیایند و بگذرند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>وینها که خفته‌اند در این خاک سالها</p></div>
<div class="m2"><p>از یک نشستن پدرانند و مادرند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>وینها که دم زدند به حب علی همی</p></div>
<div class="m2"><p>گر زانکه دوستند چرا خصم عمرند؟</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>وینها که هستشان به ابوبکر دوستی</p></div>
<div class="m2"><p>گر دوستند چونکه همه خصم حیدرند؟</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>وین سنیان که سیرتشان بغض حیدر است</p></div>
<div class="m2"><p>حقا که دشمنان ابوبکر و عمرند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر عاقلی ز هر دو جماعت سخن مگوی</p></div>
<div class="m2"><p>بگذارشان بهم که نه افلج نه قمبرند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هان‌تا از آن گروه نباشی که در جهان</p></div>
<div class="m2"><p>چون گاو می‌خورند و چون گرگان همی درند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>یا کافری به قاعده یا مؤمنی به حق</p></div>
<div class="m2"><p>همسایگان من نه مسلمان نه کافرند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ناصر غلام و چاکر آن کس که این بگفت</p></div>
<div class="m2"><p>«جان و خرد رونده بر این چرخ اخضرند»</p></div></div>