---
title: >-
    قصیدهٔ شمارهٔ ۲۱۹
---
# قصیدهٔ شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>بگسل رسن از بی‌فسار عامه</p></div>
<div class="m2"><p>مشغول چه باشی به بارنامه؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو خود قلم کردگار حقی</p></div>
<div class="m2"><p>احسنت و زهی هوشیار خامه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قول تو خط توست، مر خرد را</p></div>
<div class="m2"><p>سامه کن و بیرون مشو ز سامه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منیوش مگر پند خوب و حکمت</p></div>
<div class="m2"><p>برگوش همه خلق خاص و عامه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی جامه شریفی ازانکه جانت</p></div>
<div class="m2"><p>معروف به خط است نه به جامه</p></div></div>