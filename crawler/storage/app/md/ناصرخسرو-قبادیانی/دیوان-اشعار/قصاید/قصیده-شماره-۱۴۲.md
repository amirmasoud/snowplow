---
title: >-
    قصیدهٔ شمارهٔ ۱۴۲
---
# قصیدهٔ شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>گسستم ز دنیای جافی امل</p></div>
<div class="m2"><p>تو را باد بند و گشاد و عمل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غزال و غزل هر دوان مر تو را</p></div>
<div class="m2"><p>نجویم غزال و نگویم غزل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا، ای پسر، عمر کوتاه کرد</p></div>
<div class="m2"><p>فراخی‌ی امید و درازی‌ی امل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمانه به کردار مست اشتری</p></div>
<div class="m2"><p>مرا پست بسپرد زیر سبل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسی دیدم اجلال و اعزازها</p></div>
<div class="m2"><p>ز خواجهٔ جلیل و امیر اجل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ولیکن ندارد مرا هیچ سود</p></div>
<div class="m2"><p>امیر اجل چون بیاید اجل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر عاریت باز خواهد ز ما</p></div>
<div class="m2"><p>زمانه نه جنگ آید و نه جدل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنانک آمدی رفت باید همی</p></div>
<div class="m2"><p>به تقدیر ایزد تعالی وجل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تهی رفت خواهی چنانک آمدی</p></div>
<div class="m2"><p>نماند همی ملک و مال و ثقل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرو مفلس آنجا؛ که معلوم توست</p></div>
<div class="m2"><p>که مر مفلسان را نباشد محل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو ورزه به ابکاره بیرون شود</p></div>
<div class="m2"><p>یکی نان بگیرد به زیر بغل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو بی‌توشه خواهی همی برشدن</p></div>
<div class="m2"><p>از این تیره مرکز به چرخ زحل؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پشیزی که امروز بدهی ز دل</p></div>
<div class="m2"><p>درمیت بدهند فردا بدل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ولیکن کسی کو نداده است دوغ</p></div>
<div class="m2"><p>چرا دارد امید شیر و عسل؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به بغداد رفتی به ده نیم سود</p></div>
<div class="m2"><p>بریدی بسی بر و بحر و جبل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خدایت یکی را به ده وعده کرد</p></div>
<div class="m2"><p>بده گر نداری به دل در خلل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جهان جای الفنج غلهٔ تو است</p></div>
<div class="m2"><p>چه بی کار باشی در این مستغل؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جهان را به سایهٔ درختی زدند</p></div>
<div class="m2"><p>حکیمان هشیار دانا مثل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بپرهیز از این بی‌وفا سایه زانک</p></div>
<div class="m2"><p>بسی داند این سایه مکر و حیل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گهی دست می‌یابد و گاه پای</p></div>
<div class="m2"><p>به یک دست و یک پای لنگ است و شل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به دست زمانه کند آسمان</p></div>
<div class="m2"><p>همی ساخته قصرها را طلل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به مکر جهان سجده کردند خلق</p></div>
<div class="m2"><p>همی پیش ازین پیش لات و هبل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حدیث هبل سوی دانا نبود</p></div>
<div class="m2"><p>شگفتی‌تر ازین پیش لات و هبل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حدیث هبل سوی دانا نبود</p></div>
<div class="m2"><p>شگفتی‌تر از کار حرب جمل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وز این قوم کز فتنگی مانده‌اند</p></div>
<div class="m2"><p>هنوز اندر آن زشت و تیره وحل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چگونه برد حمله بر شیر میش</p></div>
<div class="m2"><p>کسی این ندیده‌است از اهل ملل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو ای بی‌خرد گر نه دیوانه‌ای</p></div>
<div class="m2"><p>مر آن میش را چون شده‌ستی حمل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به خونابه شوئی همی روی خویش</p></div>
<div class="m2"><p>سزای تو جاهل بد آن مغتسل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو را علت جهل کالفته کرد</p></div>
<div class="m2"><p>کزین صعبتر نیست چیز از علل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نبینی که عرضه کند علتت</p></div>
<div class="m2"><p>همی جان مسکینت را بر وجل؟</p></div></div>