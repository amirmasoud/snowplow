---
title: >-
    قصیدهٔ شمارهٔ ۲۴۸
---
# قصیدهٔ شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>گرت باید که تن خویش به زندان ندهی</p></div>
<div class="m2"><p>آن به آید که دل خویش به شیطان ندهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیو مهمان دل توست نگر تا به گزاف</p></div>
<div class="m2"><p>این گزین خانه بدان بیهده مهمان ندهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرزو را و حسد را مده اندر دل جا</p></div>
<div class="m2"><p>گر همی خواهی تا خانه به ماران ندهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو مر آز و حسد را بسپاری دل خویش</p></div>
<div class="m2"><p>ندهند آنچه تو خواهی به تو تا جان ندهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آز بر جانت نگهبان بلا گشت بکوش</p></div>
<div class="m2"><p>تا مگر جانت بدین زشت نگهبان ندهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نبرده است تو را دیو فریبنده ز راه</p></div>
<div class="m2"><p>چونکه از طاعت و دانش حق یزدان ندهی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه را پیش جز از بختهٔ پخته ننهی</p></div>
<div class="m2"><p>مؤمنی را که ضعیف است یکی نان ندهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آشکارا دهی آن اندک و بی‌مایه زکات</p></div>
<div class="m2"><p>رشوت حاکم جز در شب و پنهان ندهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچه کان را ببری تو همی از حق خدای</p></div>
<div class="m2"><p>بی‌گمان جز که به سلطان و تاوان ندهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از غم مزد سر ماه که آن یک درم است</p></div>
<div class="m2"><p>کودک خویش به استاد و دبستان ندهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرچه کان را به دل خوش ندهی از پی مزد</p></div>
<div class="m2"><p>آن به کار بزه جز کز بن دندان ندهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر تو را دیو سلیمان ز سلیمان نفریفت</p></div>
<div class="m2"><p>چون همی حق سلیمان به سلیمان ندهی؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پرفضول است سرت هیچ نخواهی شب و روز</p></div>
<div class="m2"><p>که نو این را بستانی و کهن آن ندهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیشه‌ای سخت نکوهیده گزیدی، چه بود</p></div>
<div class="m2"><p>کز فلان زر نستانی و به بهمان ندهی؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل درویش مسوز و مستان زو و مده</p></div>
<div class="m2"><p>گرت باید که تنت به آتش سوزان ندهی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه بود، نیک بیندیش به تدبیر خرد،</p></div>
<div class="m2"><p>که ز حامد نستانی و به حمدان ندهی؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جان پرمایه همی چون بفروشی بنچیز</p></div>
<div class="m2"><p>چیز پرمایه همان به که به ارزان ندهی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دیو بی‌فرمان بنشیند بر گردن تو</p></div>
<div class="m2"><p>چو تو گردن به خداوندهٔ فرمان ندهی؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاخ زنبور به انگور تو افگنده‌ستی</p></div>
<div class="m2"><p>چو نیت کردی کانگور به دهقان ندهی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نیت نیک رساند به تو نیکی و صلاح</p></div>
<div class="m2"><p>دل هشیار نگر خیره به مستان ندهی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نخوری از رز و ز ضعیت و ز کشت و درود</p></div>
<div class="m2"><p>بر تابستان تاش آب زمستان ندهی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چه طمع داری در حلهٔ صد رنگ بهشت</p></div>
<div class="m2"><p>چون به درویش یکی پارهٔ خلقان ندهی؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مر مؤذن را جو نانی دشوار دهی</p></div>
<div class="m2"><p>مر فسوسی را دینار جز آسان ندهی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از تو درویشان کرباس نیابند و گلیم</p></div>
<div class="m2"><p>مطربان را جز دیبای سپاهان ندهی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وام خواهی و نخواهی مگر افزونی و چرب</p></div>
<div class="m2"><p>باز اگر باز دهی جز که به نقصان ندهی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وز پی داوری و درد سر و جنگ و جلب</p></div>
<div class="m2"><p>جز همه عاریتی چیز گروگان ندهی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دعوی دوستی یاران داری همه روز</p></div>
<div class="m2"><p>چونکه دانگی به کسی از پی ایشان ندهی؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای فضولی، تو چه دانی که که بودند ایشان</p></div>
<div class="m2"><p>چون تو دل در طلب طاعت و ایمان ندهی؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از تنت چون ندهی حق شریعت به نماز؟</p></div>
<div class="m2"><p>وز زبان چونکه به خواندن حق فرقان ندهی؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو که نادانی شاید که فسار خر خویش</p></div>
<div class="m2"><p>به یکی دیگر بیچاره و نادان ندهی؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گرگ بسیار فتاده است در این صعب رمه</p></div>
<div class="m2"><p>آن به آید که خر خویش به گرگان ندهی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سخن حجت بپذیر و نگر تا به گزاف</p></div>
<div class="m2"><p>سخنش را به ستوران خراسان ندهی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خر نداند خطر سنبل و ریحان، زنهار</p></div>
<div class="m2"><p>که مراین خر رمه را سنبل و ریحان ندهی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همه افسار بدادند به نعمان، تو بکوش</p></div>
<div class="m2"><p>بخرد تا مگر افسار به نعمان ندهی</p></div></div>