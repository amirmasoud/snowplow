---
title: >-
    قصیدهٔ شمارهٔ ۷۴
---
# قصیدهٔ شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>صبا باز با گل چه بازار دارد؟</p></div>
<div class="m2"><p>که هموارش از خواب بیدار دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به رویش همی بر دمد مشک سارا</p></div>
<div class="m2"><p>مگر راه بر طبل عطار دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی راز گویند تا روز هر شب</p></div>
<div class="m2"><p>ازیرا به بهمن گل آزار دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو بیمارگون شد ز نم چشم نرگس</p></div>
<div class="m2"><p>مر او را همی لاله تیمار دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سحر گه نگه کن که بر دست سیمین</p></div>
<div class="m2"><p>به زر اندرون در شهوار دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه غواص گوهر نه عطار عنبر</p></div>
<div class="m2"><p>به نزدیک نرگس چه مقدار دارد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنالد همی پیش گلزار بلبل</p></div>
<div class="m2"><p>که از زاغ آزار بسیار دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زره پوش گشتند مردان بستان</p></div>
<div class="m2"><p>مگر باغ با زاغ پیکار دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون تیرگلبن عقیق و زمرد</p></div>
<div class="m2"><p>از این کینه بر پر و سوفار دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیابد کنون داد بلبل که بستان</p></div>
<div class="m2"><p>همه خیل نیسان و ایار دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عروس بهاری کنون از بنفشه</p></div>
<div class="m2"><p>گشن جعد وز لاله رخسار دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیا تا ببینی شگفتی عروسی</p></div>
<div class="m2"><p>که زلفین و عارض به خروار دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نگویم که طاووس نر است گلبن</p></div>
<div class="m2"><p>که گلبن همی زین سخن عار دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه طاووس نر از وشی پر دارد</p></div>
<div class="m2"><p>نه از سرخ یاقوت منقار دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه در پر و منقار رنگین سرشته</p></div>
<div class="m2"><p>چو گل مشک خر خیز و تاتار دارد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه گوئی جهان این همه زیب و زینت</p></div>
<div class="m2"><p>کنون بر همان خاک و کهسار دارد؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه گوئی که پوشیده این جامه‌ها را</p></div>
<div class="m2"><p>همان گنده پیر چو کفتار دارد؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به سر پر درخت گل از برف و برگش</p></div>
<div class="m2"><p>گهی معجر و گاه دستار دارد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی جادوست این که او را نبیند</p></div>
<div class="m2"><p>جز آن کز چنین کار تیمار دارد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نگه کن شگفتی به مستان بستان</p></div>
<div class="m2"><p>که هر یک چه بازار و کاچار دارد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نهاده به سر بر سمن تاج و، نرگس</p></div>
<div class="m2"><p>به دست اندرون در و دینار دارد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سوی خویش خواند همی بی‌هشان را</p></div>
<div class="m2"><p>همه سیرت و خوی طرار دارد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدانی که مست است هر رستنی‌ای</p></div>
<div class="m2"><p>نبینی که چون سر نگونسار دارد؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نگردد به گفتار مستانه غره</p></div>
<div class="m2"><p>کسی کو دل و جان هشیار دارد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر آتش زنش، ای خردمند، زیرا</p></div>
<div class="m2"><p>که هشیار مر مست را خوار دارد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نگه کن که با هر کس این پیر جادو</p></div>
<div class="m2"><p>دگرگونه گفتار و کردار دارد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مکن دست پیشش اگر عهد گیرد</p></div>
<div class="m2"><p>ازیرا که در آستی مار دارد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شدت پارو پیرارو، امسالت اینک</p></div>
<div class="m2"><p>روش بر ره پار و پیرار دارد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>درخت جهان را مجنبان ازیرا</p></div>
<div class="m2"><p>درخت جهان رنج و غم بار دارد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مده در بهای جهان عمر کوته</p></div>
<div class="m2"><p>که جز تو جهان پر خریدار دارد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به زنهار گیتی مده دل نه رازت</p></div>
<div class="m2"><p>که گیتی نه راز و نه زنهار دارد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یکی منزل است این که هرک اندرو شد</p></div>
<div class="m2"><p>برون آمدن سخت دشوار دارد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی میزبان است کو میهمان را</p></div>
<div class="m2"><p>دهان و شکم خشک و ناهار دارد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدان میهمان ده مر این میزبان را</p></div>
<div class="m2"><p>که او قصد این دیو غدار دارد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به یک سو شو از راه و بنگر به عبرت</p></div>
<div class="m2"><p>که با این گروه او چه بازار دارد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پر از خنده روی و لب و، دل ز کینه</p></div>
<div class="m2"><p>برایشان پر از خشم و زنگار دارد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو را گر بدین دست بر منبر آرد</p></div>
<div class="m2"><p>بدان دست دیگر درون‌دار دارد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو راهت گشاده کند زی مرادی</p></div>
<div class="m2"><p>چنان دان که در پیش دیوار دارد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مرا پرس از مکر او کاستینم</p></div>
<div class="m2"><p>ز مکرش به خون دل آهار دارد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همیشه در راحت این دیو بدخو</p></div>
<div class="m2"><p>برآزاد مردان به مسمار دارد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جفا و ستم را غنیمت شمارد</p></div>
<div class="m2"><p>وفا و کرم را به بیگار دارد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خردمند با اهل دنیا به رغبت</p></div>
<div class="m2"><p>نه صحبت نه کار و بیاوار دارد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ولیکن همی با سفیه آشنائی</p></div>
<div class="m2"><p>به ناکام و ناچار هنجار دارد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که خواهد که‌ش آن بد کنش درست باشد؟</p></div>
<div class="m2"><p>که جوید که از بی‌خرد یار دارد؟</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بدو ده رفیقان او را ازیرا</p></div>
<div class="m2"><p>سبکسار قصد سبکسار دارد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جز آن نیست بیدار کو دست و دل را</p></div>
<div class="m2"><p>از این دیو کوتاه و بیدار دارد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مر این بی‌وفا را ببیند حقیقت</p></div>
<div class="m2"><p>کرا چشم دل نور دین‌دار دارد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جهان پیشه کاری است ای مرد دانا</p></div>
<div class="m2"><p>که بر سر یکی نام بردار دارد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>حقیقت ببیند دگر سال خود را</p></div>
<div class="m2"><p>چو چشم و دل خویش زی پار دارد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نشاید نکوهش مرو را که یزدان</p></div>
<div class="m2"><p>در این کار بسیار اسرار دارد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زدانا بس است آن نکوهش مرو را</p></div>
<div class="m2"><p>که او را نه دانا نه سالار دارد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یکی بوستان است عالم که یزدان</p></div>
<div class="m2"><p>ز مردم درو کشت و اشجار دارد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از اینجا همی خیزدش غله لیکن</p></div>
<div class="m2"><p>بدان عالم دیگر انبار دارد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>همه برزگاران اویند یکسر</p></div>
<div class="m2"><p>مسلمان و، ترسا که زنار دارد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>یکی را زمین سنان است و شوره</p></div>
<div class="m2"><p>یکی کشت و پالیز و شد کار دارد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>یکی چون درختی بهی چفده از بر</p></div>
<div class="m2"><p>یکی گردنی چون سپیدار دارد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>یکی تخم خورده‌است وز بی‌فلاحی</p></div>
<div class="m2"><p>همی گاو همواره بی‌کار دارد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>یکی تخم کرده‌است وز کار گاوش</p></div>
<div class="m2"><p>تن کار کن لاغر و زار دارد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مراین هردو را هیچ دهقان عادل</p></div>
<div class="m2"><p>چه گوئی که یکسان و هموار دارد؟</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>یکی روزنامه است مر کارها را</p></div>
<div class="m2"><p>که آن را جهان‌دار دادار دارد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بیاموز و آنگه بکن کار دنیی</p></div>
<div class="m2"><p>که کار ای پسر دانش و کار دارد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>جز آن را مدان رسته از بند آتش</p></div>
<div class="m2"><p>که کردار در خورد گفتار دارد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نصیحت پذیرد ز گفتار حجت</p></div>
<div class="m2"><p>کسی کو دل و خوی احرار دارد</p></div></div>