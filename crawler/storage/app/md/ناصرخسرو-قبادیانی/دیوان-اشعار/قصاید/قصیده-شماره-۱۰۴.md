---
title: >-
    قصیدهٔ شمارهٔ ۱۰۴
---
# قصیدهٔ شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>ای خوانده بسی علم و جهان گشته سراسر،</p></div>
<div class="m2"><p>تو بر زمی و از برت این چرخ مدور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چرخ مدور چه خطر دارد زی تو</p></div>
<div class="m2"><p>چون بهرهٔ خود یافتی از دانش مضمر؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کی تو به تن بر خوری از نعمت دنیا؟</p></div>
<div class="m2"><p>یک چند به جان از نعم دانش برخور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی سود بود هر چه خورد مردم در خواب</p></div>
<div class="m2"><p>بیدار شناسد مزهٔ منفعت و ضر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خفته چه خبر دارد از چرخ و کواکب؟</p></div>
<div class="m2"><p>دادار چه رانده است بر این گوی مغبر؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این خاک سیه بیند و آن دایرهٔ سبز</p></div>
<div class="m2"><p>گه روشن و گه تیره گهی خشک و گهی تر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت همه آن داند کز خاک بر آید</p></div>
<div class="m2"><p>با خاک همان خاک نکو آید و درخور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با صورت نیکو که بیامیزد با او</p></div>
<div class="m2"><p>با جبهٔ سقلاطون با شعر مطیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با تشنگی و گرسنگی دارد محنت</p></div>
<div class="m2"><p>سیری شمرد خیر و همه گرسنگی شر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدار شو از خواب خوش، ای خفته چهل سال،</p></div>
<div class="m2"><p>بنگر که ز یارانت نماندند کس ایدر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از خواب و خور انباز تو گشته است بهائم</p></div>
<div class="m2"><p>آمیزش تو بیشتر است انده کمتر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چیزی که ستورانت بدان با تو شریکند</p></div>
<div class="m2"><p>منت ننهد بر تو بدان ایزد داور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نعمت نبود آنکه ستوران بخورندش</p></div>
<div class="m2"><p>نه ملک بود آنکه به دست آرد قیصر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر ملک به دست آری و نعمت بشناسی</p></div>
<div class="m2"><p>مرد خرد آنگاه جدا داندت از خر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بندیش که شد ملک سلیمان و سلیمان</p></div>
<div class="m2"><p>چونان که سکندر شد با ملک سکندر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>امروز چه فرق است از این ملک بدان ملک؟</p></div>
<div class="m2"><p>این مرده و آن مرده و املاک مبتر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگذشته چه اندوه و چه شادی بر دانا</p></div>
<div class="m2"><p>نا آمده اندوه و گذشته است برابر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اندیشه کن از حال براهیم و ز قربان</p></div>
<div class="m2"><p>وان عزم براهیم که برد ز پسر سر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر کردی این عزم کسی ز آزر فکرت</p></div>
<div class="m2"><p>نفرین کندی هر کس بر آزر بتگر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر مست نه ای منشین با مستان یکجا</p></div>
<div class="m2"><p>اندیشه کن از حال خود امروز نکوتر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>انجام تو ایزد به قران کرد وصیت</p></div>
<div class="m2"><p>بنگر که شفیع تو کدام است به محشر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فرزند تو امروز بود جاهل و عاصی</p></div>
<div class="m2"><p>فردات چه فریاد رسد پیش گروگر؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یا گرت پدر گبر بود مادر ترسا</p></div>
<div class="m2"><p>خشنودی ایشان به جز آتش چه دهد بر؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دانی که خداوند نفرمود به جز حق</p></div>
<div class="m2"><p>حق گوی و حق اندیش و حق آغاز و حق آور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قفل از دل بردار و قران رهبر خود کن</p></div>
<div class="m2"><p>تا راه شناسی و گشاده شودت در</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ور راه نیابی نه عجب دارم ازیراک</p></div>
<div class="m2"><p>من چون تو بسی بودم گمراه و محیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بگذشته زهجرت پس سیصد نود و چار</p></div>
<div class="m2"><p>بنهاد مرا مادر بر مرکز اغبر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بالندهٔ بی‌دانش مانند نباتی</p></div>
<div class="m2"><p>کز خاک سیه زاید وز آب مقطر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از حال نباتی برسیدم به ستوری</p></div>
<div class="m2"><p>یک چند همی بودم چون مرغک بی پر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در حال چهارم اثر مردمی آمد</p></div>
<div class="m2"><p>چون ناطقه ره یافت در این جسم مکدر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پیموده شد از گنبد بر من چهل و دو</p></div>
<div class="m2"><p>جویان خرد گشت مرا نفس سخن‌ور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رسم فلک و گردش ایام و موالید</p></div>
<div class="m2"><p>از دانا بشنیدم و برخواند ز دفتر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون یافتم از هرکس بهتر تن خود را</p></div>
<div class="m2"><p>گفتم «ز همه خلق کسی باید بهتر:</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون باز ز مرغان و چو اشتر ز بهائم</p></div>
<div class="m2"><p>چون نخل ز اشجار و چو یاقوت ز جوهر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون فرقان از کتب و چو کعبه ز بناها</p></div>
<div class="m2"><p>چون دل ز تن مردم و خورشید ز اختر»</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز اندیشه غمی گشت مرا جان به تفکر</p></div>
<div class="m2"><p>ترسنده شد این نفس مفکر ز مفکر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از شافعی و مالک وز قول حنیفی</p></div>
<div class="m2"><p>جستم ره مختار جهان داور رهبر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر یک به یکی راه دگر کرد اشارت</p></div>
<div class="m2"><p>این سوی ختن خواند مرا آن سوی بربر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون چون و چرا خواستم و آیت محکم</p></div>
<div class="m2"><p>در عجز به پیچیدند، این کور شد آن کر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یک روز بخواندم ز قران آیت بیعت</p></div>
<div class="m2"><p>کایزد به قران گفت که «بد دست من از بر»</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آن قوم که در زیر شجر بیعت کردند</p></div>
<div class="m2"><p>چون جعفر و مقداد و چو سلمان و چو بوذر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گفتم که «کنون آن شجر و دست چگونه است،</p></div>
<div class="m2"><p>آن دست کجا جویم و آن بیعت و محضر؟»</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گفتند که «آنجانه شجر ماندو نه آن دست</p></div>
<div class="m2"><p>کان جمع پراگنده شد آن دست مستر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آنها همه یاران رسولند و بهشتی</p></div>
<div class="m2"><p>مخصوص بدان بیعت و از خلق مخیر»</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گفتم که «به قرآن در پیداست که احمد</p></div>
<div class="m2"><p>بشیر و نذیر است و سراج است و منور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ور خواهد کشتن به دهن کافر او را</p></div>
<div class="m2"><p>روشن کندش ایزد بر کامهٔ کافر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون است که امروز نمانده‌است از آن قوم؟</p></div>
<div class="m2"><p>جز حق نبود قول جهان داور اکبر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ما دست که گیریم و کجا بیعت یزدان</p></div>
<div class="m2"><p>تا همجوم مقدم نبود داد مخر؟</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ما جرم چه کردیم نزادیم بدان وقت؟</p></div>
<div class="m2"><p>محروم چرائیم ز پیغمبر و مضطر؟»</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>رویم چو گل زرد شد از درد جهالت</p></div>
<div class="m2"><p>وین سرو به ناوقت بخمید چو چنبر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز اندیشه که خاک است و نبات است و ستور است</p></div>
<div class="m2"><p>بر مردم در عالم این است محصر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>امروز که مخصوص‌اند این جان و تن من</p></div>
<div class="m2"><p>هم نسخهٔ دهرم من و هم دهر مکدر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دانا به مثل مشک و زو دانش چون بوی</p></div>
<div class="m2"><p>یا هم به مثل کوه و زو دانش چون زر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چون بوی و زر از مشک جدا گردد وز سنگ</p></div>
<div class="m2"><p>بی قدر شود سنگ و شود مشک مزور</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>این زر کجا در شود از مشک ازان پس؟</p></div>
<div class="m2"><p>خیزم خبری پرسم از آن درج مخبر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>برخاستم از جای و سفر پیش گرفتم</p></div>
<div class="m2"><p>نز خانم یاد آمد و نز گلشن و منظر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>از پارسی و تازی وز هندی وز ترک</p></div>
<div class="m2"><p>وز سندی و رومی و ز عبری همه یکسر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>وز فلسفی و مانوی و صابی و دهری</p></div>
<div class="m2"><p>درخواستم این حاجت و پرسیدم بی‌مر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>از سنگ بسی ساخته‌ام بستر و بالین</p></div>
<div class="m2"><p>وز ابر بسی ساخته‌ام خیمه و چادر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گاهی به نشیبی شده هم گوشهٔ ماهی</p></div>
<div class="m2"><p>گاهی به سر کوهی برتر ز دو پیکر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گاهی به زمینی که درو آب چو مرمر</p></div>
<div class="m2"><p>گاهی به جهانی که درو خاک چو اخگر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گه دریا گه بالا گه رفتن بی‌راه</p></div>
<div class="m2"><p>گه کوه و گهی ریگ و گهی جوی و گهی جر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گه حبل به گردن بر مانند شتربان</p></div>
<div class="m2"><p>گه بار به پشت اندر مانندهٔ استر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>پرسنده همی رفتم از این شهر بدان شهر</p></div>
<div class="m2"><p>جوینده همی گشتم از این بحر بدان بر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گفتند که «موضوع شریعت نه به عقل است</p></div>
<div class="m2"><p>زیرا که به شمشیر شد اسلام مقرر»</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گفتم که «نماز از چه بر اطفال و مجانین</p></div>
<div class="m2"><p>واجب نشود تا نشود عقل مجبر؟»</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>تقلید نپذرفتم و حجت ننهفتم</p></div>
<div class="m2"><p>زیرا که نشد حق به تقلید مشهر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ایزد چو بخواهد بگشاید در رحمت</p></div>
<div class="m2"><p>دشواری آسان شود و صعب میسر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>روزی برسیدم به در شهری کان را</p></div>
<div class="m2"><p>اجرام فلک بنده بد، افلاک مسخر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>شهری که همه باغ پر از سرو و پر از گل</p></div>
<div class="m2"><p>دیوار زمرد همه و خاک مشجر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>صحراش منقش همه مانندهٔ دیبا</p></div>
<div class="m2"><p>آبش عسل صافی مانندهٔ کوثر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>شهری که درو نیست جز از فضل منالی</p></div>
<div class="m2"><p>باغی که درو نیست جز از عقل صنوبر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>شهری که درو دیبا پوشند حکیمان</p></div>
<div class="m2"><p>نه تافتهٔ ماده و نه بافتهٔ نر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>شهری که من آنجا برسیدم خردم گفت</p></div>
<div class="m2"><p>«اینجا بطلب حاجت و زین منزل مگذر»</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>رفتم بر دربانش و بگفتم سخن خود</p></div>
<div class="m2"><p>گفتا «مبر اندوه که شد کانت به گوهر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>دریای معین است در این خاک معانی</p></div>
<div class="m2"><p>هم در گرانمایه و هم آب مطهر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>این چرخ برین است پر از اختر عالی</p></div>
<div class="m2"><p>لابل که بهشت است پر از پیکر دلبر»</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>رضوانش گمان بردم این چون بشنیدم</p></div>
<div class="m2"><p>از گفتن با معنی و از لفظ چو شکر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گفتم که «مرا نفس ضعیف است و نژند است</p></div>
<div class="m2"><p>منگر به درشتی‌ی تن وین گونهٔ احمر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>دارو نخورم هرگز بی حجت و برهان</p></div>
<div class="m2"><p>وز درد نیندیشم و ننیوشم منکر»</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>گفتا «مبر انده که من اینجای طبیبم</p></div>
<div class="m2"><p>بر من بکن آن علت مشروح و مفسر»</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>از اول و آخرش بپرسیدم آنگاه</p></div>
<div class="m2"><p>وز علت تدبیر که هست اصل مدبر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>وز جنس بپرسیدم وز صنعت و صورت</p></div>
<div class="m2"><p>وز قادر پرسیدم و تقدیر مقدر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>کاین هر دو جدا نیست یک از دیگر دایم</p></div>
<div class="m2"><p>چون شاید تقدیم یکی بر دوی دیگر؟</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>او صانع این جنبش و جنبش سبب او</p></div>
<div class="m2"><p>محتاج غنی چون بود و مظلم انور؟</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>وز حال رسولان و رسالات مخالف</p></div>
<div class="m2"><p>وز علت تحریم دم و خمر مخمر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>وانگاه بپرسیدم از ارکان شریعت</p></div>
<div class="m2"><p>کاین پنج نماز از چه سبب گشت مقرر؟</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>وز روزه که فرمودش ماه نهم از سال</p></div>
<div class="m2"><p>وز حال زکات درم و زر مدور</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>وز خمس فی و عشر زمینی که دهند آب</p></div>
<div class="m2"><p>این از چه مخمس شد و آن از چه معشر؟</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>وز علت میراث و تفاوت که درو هست</p></div>
<div class="m2"><p>چون برد برادر یکی و نیمی خواهر؟</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>وز قسمت ارزاق بپرسیدم و گفتم</p></div>
<div class="m2"><p>«چون است غمی زاهد و بی‌رنج ستمگر؟</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بینا و قوی چون زید و آن دگری باز</p></div>
<div class="m2"><p>مکفوف همی زاید و معلول ز مادر؟</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>یک زاهد رنجور و دگر زاهد بی‌رنج!</p></div>
<div class="m2"><p>یک کافر شادان و دگر کافر غمخور!</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ایزد نکند جز که همه داد، ولیکن</p></div>
<div class="m2"><p>خرسند نگردد خرد از دیده به مخبر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>من روز همی بینم و گوئی که شب است این</p></div>
<div class="m2"><p>ور حجت خواهم تو بیاهنجی خنجر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>گوئی «به فلان جای یکی سنگ شریف است</p></div>
<div class="m2"><p>هر کس که زیارت کندش گشت محرر</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>آزر به صنم خواند مرا و تو به سنگی</p></div>
<div class="m2"><p>امروز مرا پس به حقیقت توی آزر»</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>دانا که بگفتمش من این دست به برزد</p></div>
<div class="m2"><p>صد رحمت هر روز بر آن دست و بر آن بر</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>گفتا «بدهم داروی با حجت و برهان</p></div>
<div class="m2"><p>لیکن بنهم مهری محکم به لبت بر»</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ز آفاق و ز انفس دو گوا حاضر کردش</p></div>
<div class="m2"><p>بر خوردنی و شربت و من مرد هنرور</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>راضی شدم و مهر بکرد آنگه و دارو</p></div>
<div class="m2"><p>هر روز به تدریج همی داد مزور</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>چون علت زایل شد بگشاد زبانم</p></div>
<div class="m2"><p>مانند معصفر شد رخسار مزعفر</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>از خاک مرا بر فلک آورد جهاندار</p></div>
<div class="m2"><p>یک برج مرا داد پر از اختر ازهر</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چون سنگ بدم، هستم امروز چو یاقوت</p></div>
<div class="m2"><p>چون خاک بدم، هستم امروز چو عنبر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>دستم به کف دست نبی داد به بیعت</p></div>
<div class="m2"><p>زیر شجر عالی پر سایهٔ مثمر</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>دریای بشنیدی که برون آید از آتش؟</p></div>
<div class="m2"><p>روبه بشنیدی که شود همچو غضنفر؟</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>خورشید تواند که کند یاقوت از سنگ</p></div>
<div class="m2"><p>کز دست طبایع نشود نیز مغیر؟</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>یاقوت منم اینک و خورشید من آن کس</p></div>
<div class="m2"><p>کز نور وی این عالم تاری شود انور</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>از رشک همی نام نگویمش در این شعر</p></div>
<div class="m2"><p>گویم که «خلیلی است که‌ش افلاطون چاکر</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>استاد طبیب است و مؤید ز خداوند</p></div>
<div class="m2"><p>بل کز حکم و علم مثال است و مصور»</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>آباد بر آن شهر که وی باشد دربانش</p></div>
<div class="m2"><p>آباد بر آن کشتی کو باشد لنگر</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>ای معنی را نظم سخن سنج تو میزان،</p></div>
<div class="m2"><p>ای حکمت را بر تو که نثری است مسطر،</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>ای خیل ادب صف‌زده اندر خطب تو،</p></div>
<div class="m2"><p>ای علم‌زده بر در فضل تو معسکر،</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>خواهم که ز من بندهٔ مطواع سلامی</p></div>
<div class="m2"><p>پوینده و پاینده چو یک ورد مقمر</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>زاینده و باینده چو افلاک و طبایع</p></div>
<div class="m2"><p>تا بنده و رخشنده چو خورشید و چو اختر</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چون قطره چکیده ز بر نرگس و شمشاد</p></div>
<div class="m2"><p>چون باد وزیده ز بر سوسن و عبهر</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>چون وصل نکورویان مطبوع و دل‌انگیز</p></div>
<div class="m2"><p>چون لفظ خردمندان مشروح و مفسر</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>پر فایده و نعمت چون ابر به نوروز</p></div>
<div class="m2"><p>کز کوه فرو آید چو مشک معطر</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>وافی و مبارک چود دم عیسی مریم</p></div>
<div class="m2"><p>عالی و بیاراسته چون گنبد اخضر</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>زی خازن علم و حکم و خانهٔ معمور</p></div>
<div class="m2"><p>با نام بزرگ آن که بدو دهر معمر</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>زی طالع سعد و در اقبال خدائی</p></div>
<div class="m2"><p>فخر بشر و بر سر عالم همه افسر</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>مانند و جگر گوشهٔ جد و پدر خویش</p></div>
<div class="m2"><p>در صدر چو پیغمبر و در حرب چو حیدر</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>بر مرکبش از طلعت او دهر مقمر</p></div>
<div class="m2"><p>وز مرکب او خاک زمین جمله معنبر</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>بر نام خداوند بر این وصف سلامی</p></div>
<div class="m2"><p>در مجلس برخواند ابو یعقوب ازبر</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>وانگاه بر آن کس که مرا کرده‌است آزاد</p></div>
<div class="m2"><p>استاد و طبیب من و مایهٔ خرد و فر</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>ای صورت علم و تن فضل و دل حکمت</p></div>
<div class="m2"><p>ای فایدهٔ مردمی و مفخر مفخر</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>در پیش تو استاده بر این جامهٔ پشمین</p></div>
<div class="m2"><p>این کالبد لاغر با گونهٔ اصفر</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>حقا که به جز دست تو بر لب ننهادم</p></div>
<div class="m2"><p>چون بر حجرالاسود و بر خاک پیمبر</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>شش سال ببودم بر ممثول مبارک</p></div>
<div class="m2"><p>شش سال نشستم به در کعبه مجاور</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>هر جا که بوم تا بزیم من گه و بیگاه</p></div>
<div class="m2"><p>در شکر تو دارم قلم و دفتر و محبر</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>تا عرعر از باد نوان است همی باد</p></div>
<div class="m2"><p>حضرت به تو آراسته چون باغ به عرعر</p></div></div>