---
title: >-
    قصیدهٔ شمارهٔ ۱۲۰
---
# قصیدهٔ شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>مرد را خوار چه دارد؟ تن خوش خوارش</p></div>
<div class="m2"><p>چون تو را خوار کند چون نکنی خوارش ؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که او انده و تیمار تو را کوشد</p></div>
<div class="m2"><p>تو بخیره چه خوری انده و تیمارش؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تن همان خاک گران سیه است ار چند</p></div>
<div class="m2"><p>شاره زربفت کنی قرطه و شلوارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن تو خادم این جان گرانمایه است</p></div>
<div class="m2"><p>خادم جان گرانمایه همی دارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نخواهی که تو را خوار و زبون گیرد</p></div>
<div class="m2"><p>برتر از قدرش و مقدارش مگذارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن درخت است و خرد بار و، دروغ و مکر</p></div>
<div class="m2"><p>خس و خار است، حذر کن ز خس و خارش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خار و خس بفگن از این شهره درخت ایرا</p></div>
<div class="m2"><p>کز خس و خار نیابی مزه جز خارش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یار خرماست یکی خار، بتر یاری</p></div>
<div class="m2"><p>یار بد عار بود دایم بر یارش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یار بد خار توست، ای پسر، از یارت</p></div>
<div class="m2"><p>دور باش و به جز از خار مپندارش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یار چون خار تو را زود بیازارد</p></div>
<div class="m2"><p>گر نخواهی که بیازاری مازارش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که با اوت همی صحبت رای آید</p></div>
<div class="m2"><p>بر رس، ای پور، نخست از ره و رفتارش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سیرت خوب طلب باید کرد از مرد</p></div>
<div class="m2"><p>گرچه خوب است مشو غره به دیدارش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صورت خوب بسی باشد بی حاصل</p></div>
<div class="m2"><p>بر در و درگه و بر خانه و دیوارش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرچه خرما بن سبز است، درخت سبز</p></div>
<div class="m2"><p>هست بسیار که خرما نبود بارش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرکه بی‌سیرت خوب است و نکو صورت</p></div>
<div class="m2"><p>جز همان صورت دیوار مینگارش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بد کنش را به سخن دست مده بر بد</p></div>
<div class="m2"><p>که به تو باز رسد سرزنش از کارش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سر پیکان نشود در سپر و جوشن</p></div>
<div class="m2"><p>تا نباشد سپس اندر پر و سوفارش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صحبت نادان مگزین که تبه دارد</p></div>
<div class="m2"><p>اندکی فایده را یاوهٔ بسیارش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>میوه چون اندک باشد به درختی بر</p></div>
<div class="m2"><p>بی‌مزه ماند در برگ به خروارش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ره و هنجار ستمگار همه زشت است</p></div>
<div class="m2"><p>ای خردمند مرو بر ره و هنجارش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرکه او بر ره کفتار رود، بی‌شک</p></div>
<div class="m2"><p>سوی مردار نماید ره کفتارش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرد را چون نبود جز که جفا، پیشه</p></div>
<div class="m2"><p>مارش انگار نه مردم، سوی ما مارش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مار مردم نیت بد بود اندر دل</p></div>
<div class="m2"><p>بد نیت را جگر افگار کند مارش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر که را قولش با فعل نباشد راست</p></div>
<div class="m2"><p>در در دوستی خویش مده بارش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سیر گرداندت از گفتن بی‌معنی</p></div>
<div class="m2"><p>تا مگر سیر کنی معدهٔ ناهارش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هم از آن کیسه دهش نقد که او دادت</p></div>
<div class="m2"><p>نقد او باید بردنت به بازارش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زرق پیش آر چو رزاق شود با تو</p></div>
<div class="m2"><p>سر به سر باش و همی باش به مقدارش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر همی خفته گمانیت برد خفته است</p></div>
<div class="m2"><p>خفته بگذار و مکن بیهده بیدارش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سخن از مردم دین‌دار شنو، وان را</p></div>
<div class="m2"><p>که ندارد دین، منگر سوی دینارش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زنگ دارد دل بد دین، من ازان ترسم،</p></div>
<div class="m2"><p>که بیالاید زو دلت به زنگارش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه مکان است سخن را سر بی‌مغزش</p></div>
<div class="m2"><p>نه مقر است خرد را دل چون قارش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نیست آمیخته با آب هنر خاکش</p></div>
<div class="m2"><p>نیست آویخته در پود خرد تارش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نبری رنج برو بهتر، چون رنجه است</p></div>
<div class="m2"><p>او ز گفتار تو، همچون تو ز گفتارش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خویشتن رنجه مکن نیز چو می‌دانی</p></div>
<div class="m2"><p>که نخواهندت پرسید ز کردارش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چه شوی غره به راهش چو همی بینی</p></div>
<div class="m2"><p>که همی غره کند گنبد دوارش؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رنجه و افگار شوی زو که چو خار است او</p></div>
<div class="m2"><p>خارت افگار کند چون کنی افگارش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به حذر باش، نباید که چو می‌کوشی</p></div>
<div class="m2"><p>خود نگیریش و، بمانی تو گرفتارش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نیک بنگر که کجا می‌بردت گیتی</p></div>
<div class="m2"><p>چون همی تازی بر مرکب رهوارش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از تو هموار همی دزدد عمرت را</p></div>
<div class="m2"><p>چرخ بیدادگر و گشتن هموارش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پارش امسال فسانه است به پیش ما</p></div>
<div class="m2"><p>هم فسانه شود امسالش چون پارش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نیست دشوار جهان بدتر از آسانش</p></div>
<div class="m2"><p>چون همی بگذرد آسانش و دشوارش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زو مبین نیک و بد و زشت و نکو هرگز</p></div>
<div class="m2"><p>بل ز سازندهٔ او بین و ز سالارش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون همی بر من زنهار خورد دنیا</p></div>
<div class="m2"><p>خویشتن چون دهی، ای پور، به زنهارش؟</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هر که را چرخ ستمگار برد بر گاه</p></div>
<div class="m2"><p>بفگند باز خود از گاه نگونسارش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا به پیکار بود، صلح طمع می‌دار</p></div>
<div class="m2"><p>چون به صلح آمد می‌ترس ز پیکارش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چاره کن، خوش خوش ازو دست بکش، زیرا</p></div>
<div class="m2"><p>یله بایدت همی کرد به ناچارش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>این جهان پیرزنی سخت فریبنده‌است</p></div>
<div class="m2"><p>نشود مرد خردمند خریدارش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>پیش ازان کز تو ببرد تو طلاقش ده</p></div>
<div class="m2"><p>مگر آزاد شود گردنت از عارش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سخن حجت مرغی است که بر دانا</p></div>
<div class="m2"><p>پند بارد همه از پرش و منقارش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گر به پند اندر رغبت کنی، ای خواجه،</p></div>
<div class="m2"><p>پند نامه است تو را دفتر و اشعارش</p></div></div>