---
title: >-
    قصیدهٔ شمارهٔ ۲۱۸
---
# قصیدهٔ شمارهٔ ۲۱۸

<div class="b" id="bn1"><div class="m1"><p>داری سخنی خوب گوش یا نه؟</p></div>
<div class="m2"><p>کامروز نه هشیاری از شبانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حکمت نتوانی شنود ازیرا</p></div>
<div class="m2"><p>فتنهٔ غزل نغزی و ترانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد پرده میان تو و ان حکمت</p></div>
<div class="m2"><p>آن پرده که بستند بر چغانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردم نشده‌ستی چو می‌ندانی</p></div>
<div class="m2"><p>جز خفتن و خور چون ستور لانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این خانه چگونه بکرد و، که نهاد</p></div>
<div class="m2"><p>این گوی سیاه اندر این میانه؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنگر که چرا کرد صنع صانع</p></div>
<div class="m2"><p>از دام چه غافل شوی به دانه؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندیش که نابوده بوده گردد</p></div>
<div class="m2"><p>تا پیش نباشد یکی بهانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این نفس خوشی جوی را نبینی</p></div>
<div class="m2"><p>درمانده بدین بند و شادمانه؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای رس به جز از بهر تو نگردد</p></div>
<div class="m2"><p>این خانهٔ رنگین بر رسانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیوار بلند است تا نبیند</p></div>
<div class="m2"><p>کانجاش چه ماند از برون خانه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون خانهٔ بیگانه‌ش آشنا شد</p></div>
<div class="m2"><p>خو کرد در این بند و زاولانه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن است گمانش کنون که این است</p></div>
<div class="m2"><p>او را وطن و جای جاودانه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بل دهر درختی است و نفس مرغی</p></div>
<div class="m2"><p>وین کالبد او را چو آشیانه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای کرده خرد بر دهان جانت</p></div>
<div class="m2"><p>از آهن حکمت یکی دهانه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دانی که نیاوردت آنکه آورد</p></div>
<div class="m2"><p>خیره به گزاف اندر این خزانه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بل تا بنماید تو را بر این لوح</p></div>
<div class="m2"><p>آیات و علامات بی‌کرانه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کردند تو را دور از این میانت</p></div>
<div class="m2"><p>گه چشم و گهی حلق و گه مثانه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گوئی که جوانم، به باغ‌ها در</p></div>
<div class="m2"><p>بسیار شود خشک و، تر جوانه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون دید خردمند روی کاری</p></div>
<div class="m2"><p>خیره نکند گربه را به شانه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیدار و هشیوار مرد ننهد</p></div>
<div class="m2"><p>دل بر وطن و خانهٔ کسانه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بشنو سخن این کبود گنبد</p></div>
<div class="m2"><p>فتنه چه شوی خیره بر فسانه؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر هرچه برون زین نشان دهندت</p></div>
<div class="m2"><p>بکمانه ازین یابی و کمانه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شخص تو یکی دفتر است روشن</p></div>
<div class="m2"><p>بنوشته برو سیرت زمانه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>این عالم سنگ است و آن دگر زر</p></div>
<div class="m2"><p>عقل است ترازوی راستانه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون راست بود سنگ با ترازو</p></div>
<div class="m2"><p>جز راست نگوید سخن زبانه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آن کس که زبانش به ما رسانید</p></div>
<div class="m2"><p>پیغام جهان داور یگانه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>او بود زبانهٔ ترازوی عقل</p></div>
<div class="m2"><p>گشته به همه راستی نشانه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر عالم دین عالی آسمان شد</p></div>
<div class="m2"><p>بر خانهٔ حق محکم آستانه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در خانهٔ دین چونکه می‌نیائی؟</p></div>
<div class="m2"><p>استاده چه ماندی بر آستانه؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هاروت همانا که بست راهت</p></div>
<div class="m2"><p>زی خانه بدان بند جاودانه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در خانه شدم بی‌تو من ازیرا</p></div>
<div class="m2"><p>هاروت تو را هست و مر مرا نه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زین است بر او قال و قیل قولت</p></div>
<div class="m2"><p>وز خمر خم است پر و چمانه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زین به نبود مذهبی که گیری</p></div>
<div class="m2"><p>از بیم عنانیش و تازیانه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گوئی که حلال است پخته مسکر</p></div>
<div class="m2"><p>با سنبل و با بیخ رازیانه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای ساخته مکر و کتاب حیلت</p></div>
<div class="m2"><p>کاین گفت فلانی ز بو فلانه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر شوم تن خویش سخت کردی</p></div>
<div class="m2"><p>از جهل در هاویه به فانه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آن کس که تو را داد صدر آتش</p></div>
<div class="m2"><p>خود رفت بدان جای چاکرانه</p></div></div>