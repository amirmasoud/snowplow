---
title: >-
    قصیدهٔ شمارهٔ ۱۶
---
# قصیدهٔ شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>آن چیست یکی دختر دوشیزهٔ زیبا</p></div>
<div class="m2"><p>از بوی و مزه چون شکر و عنبر سارا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زو بوسه بیابی اگر او را بزنی کارد</p></div>
<div class="m2"><p>هر چند تو با کارد بوی آن تن تنها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون کارد زدیش آنگه پیش تو بیفتد</p></div>
<div class="m2"><p>مانند دو کاسه که بود پر ترحلوا</p></div></div>