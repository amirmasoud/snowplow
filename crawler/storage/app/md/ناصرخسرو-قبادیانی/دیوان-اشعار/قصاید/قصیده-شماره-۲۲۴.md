---
title: >-
    قصیدهٔ شمارهٔ ۲۲۴
---
# قصیدهٔ شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>ای کرده سرت خو به بی‌فساری</p></div>
<div class="m2"><p>تا کی بود این جهل و بادساری؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دشت خطا خیره چند تازی؟</p></div>
<div class="m2"><p>چون سر ز خطا باز خط ناری؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر سر ز خطا باز خط ناری</p></div>
<div class="m2"><p>دانم به حقیقت کز اهل ناری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاری است خطا زهر بار، تاکی</p></div>
<div class="m2"><p>تو پشت در این زهر بار خاری؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل است به سوی صواب رهبر</p></div>
<div class="m2"><p>با راه‌برت چون به خار خاری؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون با خرد، ای بی‌خرد، نسازی</p></div>
<div class="m2"><p>جز رنج نبینی و سوکواری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوئی که «چرا روزگار جافی</p></div>
<div class="m2"><p>با من نکند هیچ بردباری؟»</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این بند نبینی که بر تو بستند؟</p></div>
<div class="m2"><p>در بند همی چون کنی سواری؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواهی که تماشاکنی به نزهت</p></div>
<div class="m2"><p>به خیره در این چاه تنگ و تاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز کانده و غم ندروی و حسرت</p></div>
<div class="m2"><p>هرگاه که تخم محال کاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنگه گنه ز روزگار بینی</p></div>
<div class="m2"><p>وز جهل معادای روزگاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناید ز جهان هیچ کار و باری</p></div>
<div class="m2"><p>الا که به تقدیر و امر باری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هش‌دار که عالم سرای کار است</p></div>
<div class="m2"><p>مشغول چه باشی به نابکاری؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنگر که پس از نیستی چگونه</p></div>
<div class="m2"><p>با جاه شدستی و کامگاری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دانی که تو را کردگار عالم</p></div>
<div class="m2"><p>داده‌است به حق داد کردگاری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر تو ندهی داد او به طاعت</p></div>
<div class="m2"><p>در خورد عذابی و ذل و خواری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیداد کنی با بزرگ داور</p></div>
<div class="m2"><p>زنهار مکن زینهار خواری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر کار فلک گرد گشتن آمد</p></div>
<div class="m2"><p>دین کار تو است و مرد کاری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون کار به مقدار خویش کردی</p></div>
<div class="m2"><p>رفتی به ره عز و بختیاری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر گیتی تیمار تو ندارد</p></div>
<div class="m2"><p>آن به که تو تیمار او نداری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زیرا که همی هرچگونه باشد</p></div>
<div class="m2"><p>هم بگذرد این مدت شماری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زی لابه و زاریت ننگرد چرخ</p></div>
<div class="m2"><p>هرچند که لابه کنی و زاری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دیوی است ستمگاره نفس حسی</p></div>
<div class="m2"><p>کو مایهٔ جهل است و بی‌فساری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یاری ز خرد خواه، وز قناعت</p></div>
<div class="m2"><p>برکشتن این دیو کارزاری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بس کس که بر امید پیشگاهی</p></div>
<div class="m2"><p>زو ماند به خواری و پیشکاری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بی‌نام بسی گشت ازو و بی‌نان</p></div>
<div class="m2"><p>اندر طلب نان و نامداری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زنهار بدین زینهار خواره</p></div>
<div class="m2"><p>ندهی خرد و جان زینهاری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زیر قدمت بسپرد به خواری</p></div>
<div class="m2"><p>هرگه که تو دل را بدو سپاری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ماری است گزنده طمع که ماران</p></div>
<div class="m2"><p>زین مار برند ای رفیق ماری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر در دلت این مار جای گیرد</p></div>
<div class="m2"><p>چون تو نبود کس به دل فگاری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بی‌باکی اگر مار را به دل در</p></div>
<div class="m2"><p>با پاک خرد جای داد یاری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>با عقل مکن یار مر طمع را</p></div>
<div class="m2"><p>شاید که نخواهی ز مار یاری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نیکو مثل است آن که «جای خالی</p></div>
<div class="m2"><p>بهتر چو پر از گرگ مرغزاری»</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هرچند که غمگین بود نخواهد</p></div>
<div class="m2"><p>از پشه خردمند غمگساری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آن کوش که دست از طمع بشوئی</p></div>
<div class="m2"><p>وین سفله جهان را بدو گذاری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وز روزی و از مال و تن‌درستی</p></div>
<div class="m2"><p>وز فکرت و از علم و هوشیاری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مر نعمت یزدان بی‌قرین را</p></div>
<div class="m2"><p>یک یک به تن خویش برشماری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>و اندیشه کنی سخت کاندر این‌بند</p></div>
<div class="m2"><p>از بهر چرا گشته‌ای حصاری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>وانگاه، که داده‌ستت اندر این بند</p></div>
<div class="m2"><p>بر جانوران جمله شهریاری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ایشان همه چون سرنگون و خوارند</p></div>
<div class="m2"><p>ایدون و تو چون سرو جویباری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جستند درین، هر کسی طریقی</p></div>
<div class="m2"><p>این رفت به ایوان و آن بخاری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رازیت جز آن گفت کان چغانی</p></div>
<div class="m2"><p>بلخیت نه آن گفت کان بخاری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گشتی متحیر که اندر این ره</p></div>
<div class="m2"><p>گامی نتوانی که در گزاری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گوئی به ضرورت که این چنین است</p></div>
<div class="m2"><p>لیکنت همی ناید استواری</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>رازی است بزرگ این و صعب، او را</p></div>
<div class="m2"><p>تنگ است به دلها درون مجاری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>اهل تو مر این راز را اگر تو</p></div>
<div class="m2"><p>در بند خداوند ذوالفقاری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ور گردن تو طوق او ندارد</p></div>
<div class="m2"><p>بر خشک بخیره مران سماری</p></div></div>