---
title: >-
    بخش ۱ - فصل
---
# بخش ۱ - فصل

<div class="n" id="bn1"><p>گوییم که چو موالید از نبات و حیوان اندر عالم «پدید» آینده است به صورت هایی که آن صورت ها بر امهات – که آن طبایع است – نیست و موالید را حیات است و امهات موات است و اندر مدتی زمانی هر چیزی را از آن به کمال خویش رسیدن است – چه از مردم و چه از جز مردم – و پس از آن هم بدان تدریج که پیدا آمدنش بر آن بود ناپدید شونده است، چنانکه خدای تعالی همی گوید اندر ایجاد و اعدام مردم، قوله: (الله الذی خلقکم من ضعف ثم جعل من بعد ضعف قوه ثم جعل من بعد قوه ضعفا و شیبه یخلق ما یشا و هو العلیم القدیر)، بر خردمندان واجب است که حال خویش باز جویند تا از کجا همی آیند و کجا همی شوند و اندیشه کنند تا ببینند به چشم بصیرت مر خویشتن را اندر سفری رونده که مر آن رفتن را هیچ درنگی و ایستادنی نیست، از بهر آنکه تا مردم اندر این عالم است، از دو حرکت افزایش و کاهش خالی نیست، و حرکت نباشد مگر اندر زمان، و زمان چیز محرک به دو قسمت است: یکی از او گذشته است و دیگر نا آمده است، و میان این دو قسم زمان که مر چیز متحرک راست، برزخی است که آن قسمت پذیر نیست، بر مثال خطی که میان آفتاب و سایه باشد که نه از آفتاب باشد و نه از سایه، و مر آن برزخ را که میان این دو قسم زمان (است) به تازی « الان» گویند و به پارسی « اکنون» گویندش و مر او را هیچ بعدی و کشیدگی نیست و او نه از زمان گذشته است و نه از زمان آینده، بل که این نام مر آن برزخ را که او اکنون نام است، به گشتن احوال جسم متحرک واجب آمده است و دو زمان بدین برزخ مر چیز متحرک را از یکدیگر جدا شده است و بر این (معنی اندر این کتاب به جای خویش سخن گفته شود. و چو مردم) مر خویشتن را به همه عمر خویش بر برزخ اکنون (همی یابد و زمان گذشته بر او همی) فزاید- بدانچه عدد حرکاتش همی فزاید هر ساعتی و زمان چیز متحرک عدد (حرکات اوست-) و زمان آینده اش نقصان همی پذیرد، همی داند- اگر خردمند است- که او بر مثال مسافری است (کاندر همه) زمان خویش مر او را بر یک جای چشم زخمی درنگ ممکن نیست کردن تا از این خط که زمان اوست در نگذرد (و به نقطه نقطه) اکنون ها مر این خط را نپیماید. پس بر این مسافر خردمند واجب است که باز جوید تا از کجا آمده است (و) چو دانست که از کجا آمده است، معلوم او شود که کجا همی شود و چو دانست که کجا همی شود، معلوم (او شود آنجا) که همی شود مر او را به چه چیز حاجت خواهد بودن و زاد مسافران برگیرد که مسافر بی زاد از هلاک بر خطر باشد و خدای تعالی همی گوید، قوله: (و تزودوا فان خیر الزادالتقوی)، و اندر این قول که همی گوید: زاد برگیرید، پوشیده گفته شده است که شما بر سفرید. و چو حال این است و ما مر بیشتر مردم را اندر نگریستن اندر این باب غافل یافتیم و نادانان امت مر حق را خوار گرفته بودند و بر امثال و ظواهر کتاب خدای ایستاده و ممثولات و بواطن را از آن دست بازداشته و بر محسوسات و کثایف فتنه گشته و از معقولات و لطایف دور مانده و مر هوس ها را که به هوای مختلف خویش ریاست جویان اندر دین استخراج کرده اند، فقه نام نهاده و مر دانایان را به علم حقایق و (مر بینندگان را به چشم) بصائر و (مر) جویندگان علم حق را و جدا کنندگان جوهر باقی ثابت را از جوهر فانی مستحیل، ملحد و بد دین و قرمطی نام نهاده، واجب دیدیم مر این کتاب را اندر این معنی تألیف کردن و نام نهادیم مر این کتاب را به زاد المسافر و یاری بر تمام کردن این کتاب از خدای خواهیم به میانجی خداوند زمان خویش، معد ابی تمیم الامام المستنصر بالله امیر المومنین- صلوات الله علیه و علی آبائه الطاهرین و ابنائه الاکرمین- و خردمندان را بنماییم به برهان های عقلی و به حجت های خلقی که آمدن مردم از کجاست و بازگشتن به چیست و ظاهر کنیم به آیت ها از کتاب خدای تعالی که قرآن است و رسول مصطفی (ص) بدان فرستاده شد سوی خلق تا مر ایشان را از این خواب که بیشتر مردمان اندر آن غرقه اند، بیدار کند. و نادانان امت که بر هوای خویش متابع رای و قیاس شدند، از رسیدن به علم الهی بدان بازماندند که مدعیان را اندر امامت که آن نتیجه نبوت بود متابعت کردند و بدان از معانی رمزهای کتاب خدای (دور ماندند، چنانکه خدای تعالی حکایت کند) از رسول خویش که بدو – سبحانه – بنالید از قومی که معانی قرآن را دست (بازداشتند و بر امثال بیایستادند، بدین)آیت، قوله: (و قال الرسول یرب ان قومی اتخذوا هذا القرءان مهجور) (و) وصیت (ما مر خردمندان را) آن است که مر این کتاب را به آهستگی تامل کنند تا زاد خویش را اندر این سفر از او بیابند و برگیرند (و چو بیابند بدانند) که مثل ما اندر بیرون آوردن این علم لطیف و دشوار و بایسته مثل کسی است که (چاه های ژرف بکند) و کاریزهای عظیم ببرد، تا مر آب (خوش) را از چاه و قعر خاک بر هامون براند تا تشنگان (مسافران بدان برسند) و هلاک نشوند، مر این چشمه آب خوش را از دیوانگان امت صیانت کنند تا مر این را به جهل (و سفه) پلید و تیره نکنند، بل به خاک و ریگ بینبارندش. و توفیق از خدای است بر گفتار صواب اندر (ارشاد) خلق. انه خیر موفق و معین.</p></div>
<div class="n" id="bn2"><p>قول نخستین: اندر قول که آن در علم حاضران است.</p></div>
<div class="n" id="bn3"><p>قول دوم: اندر کتابت که آن در علم غایبان است.</p></div>
<div class="n" id="bn4"><p>قول سیوم: اندر حواس ظاهر.</p></div>
<div class="n" id="bn5"><p>قول چهارم: اندر حواس باطن.</p></div>
<div class="n" id="bn6"><p>قول پنجم: اندر جسم و اقسام او.</p></div>
<div class="n" id="bn7"><p>قول ششم: اندر حرکت و انواع او.</p></div>
<div class="n" id="bn8"><p>قول هفتم: اندر نفس.</p></div>
<div class="n" id="bn9"><p>قول هشتم: اندر هیولی.</p></div>
<div class="n" id="bn10"><p>قول نهم: اندر مکان.</p></div>
<div class="n" id="bn11"><p>قول دهم: اندر زمان.</p></div>
<div class="n" id="bn12"><p>قول یازدهم: اندر ترکیب.</p></div>
<div class="n" id="bn13"><p>قول دوازدهم: اندر فاعل و منفعل.</p></div>
<div class="n" id="bn14"><p>قول سیزدهم: اندر حدث عالم.</p></div>
<div class="n" id="bn15"><p>قول چهاردهم: اندر اثبات صانع.</p></div>
<div class="n" id="bn16"><p>قول پانزدهم: اندر صانع عالم جسم که چیست.</p></div>
<div class="n" id="bn17"><p>قول شانزدهم: اندر مبدع حق- سبحانه – ومبدع.</p></div>
<div class="n" id="bn18"><p>قول هفدهم: اندر قول و کتابت خدای.</p></div>
<div class="n" id="bn19"><p>قول هشدهم: اندر لذات و اثبات آن.</p></div>
<div class="n" id="bn20"><p>قول نوزدهم: اندر علت بودش عالم جسم.</p></div>
<div class="n" id="bn21"><p>قول بیستم : اندر آنکه چرا خدای عالم (را) پیش از آنکه آفرید، نیافرید.</p></div>
<div class="n" id="bn22"><p>قول بیست و یکم : اندر چگونگی پیوستن نفس به جسم.</p></div>
<div class="n" id="bn23"><p>قول بیست و دوم : اندر چرایی پیوستن نفس به جسم.</p></div>
<div class="n" id="bn24"><p>قول بیست و سیوم : اندر اثبات مخصص به دلالات مختص.</p></div>
<div class="n" id="bn25"><p>قول بیست و چهارم : اندر بود و هست و باشد.</p></div>
<div class="n" id="bn26"><p>قول بیست و پنجم : اندر آنکه مردم از کجا آمد و کجا همی شود.</p></div>
<div class="n" id="bn27"><p>قول بیست و ششم : اندر شرح مذهب تناسخ.</p></div>
<div class="n" id="bn28"><p>قول بیست و هفتم : اندر اثبات ثواب و عقاب.</p></div>