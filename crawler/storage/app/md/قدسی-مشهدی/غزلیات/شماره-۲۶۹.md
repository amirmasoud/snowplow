---
title: >-
    شمارهٔ ۲۶۹
---
# شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>ز عقده‌ها که فلک نذر کار من دارد</p></div>
<div class="m2"><p>شکفته‌ام، که غم روزگار من دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شود چو محو تماشای یار، داغ شوم</p></div>
<div class="m2"><p>که حیرت آینه را در شمار من دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیافت در چمنم سبزه‌ای که زرد کند</p></div>
<div class="m2"><p>چه شکوه‌ها که خزان از بهار من دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سفینه کرده فلک اختیار، پنداری</p></div>
<div class="m2"><p>خبر ز گریه بی‌اختیار من دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز پشت آینه در سینه‌اش خلد مژگان</p></div>
<div class="m2"><p>کسی که آینه پیش نگار من دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جدا ز موی سیاهش، ز تیرگی روزم</p></div>
<div class="m2"><p>هزار طعنه به شب‌های تار من دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه مایه خون که ز دست حناست در جگرم</p></div>
<div class="m2"><p>که روی بر کف پای نگار من دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به گرد خویش ز یاران کسی نمی‌بینم</p></div>
<div class="m2"><p>به غیر غم که یمین و یسار من دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به کس نمی‌رسد این عقده‌ها که بر فلک است</p></div>
<div class="m2"><p>ذخیره‌ایست که از بهر کار من دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به سوی کلبه تارم به ناز می‌آید</p></div>
<div class="m2"><p>مگر صبا خبر از زلف یار من دارد؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز آشیان چو رهاندی، به دام هم مگذار</p></div>
<div class="m2"><p>که عمرهاست قفس انتظار من دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طمع بریده‌ام از خشک و تر، ولی چه کنم</p></div>
<div class="m2"><p>به سیل اشک که سر در کنار من دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قرار صبر به خود چون دهم، که بی‌صبری</p></div>
<div class="m2"><p>قرارها به دل بی‌قرار من دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز نم چو آینه محروم باد چشم کسی</p></div>
<div class="m2"><p>که طعنه بر مژه اشکبار من دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس از هلاک، رساند به آب، خاک مرا</p></div>
<div class="m2"><p>ز الفتی که صبا با غبار من دارد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو عندلیب ازان رو مرید گلزارم</p></div>
<div class="m2"><p>که نرگسش نظر از چشم یار من دارد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همای حسن نیاورده سر ز بیضه برون</p></div>
<div class="m2"><p>ز فیض عشق، هوای شکار من دارد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ازان ز گوهر معنی چکد زلال حیات</p></div>
<div class="m2"><p>که تکیه بر سخن آبدار من دارد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>محیط فیض، گره‌های گوهر معنی</p></div>
<div class="m2"><p>به نذر خامه گوهر نگار من دارد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خدای را بگشا فصل گل در قفسم</p></div>
<div class="m2"><p>که هر طرف چمنی انتظار من دارد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هزار بار مرا با وجود آنکه گداخت</p></div>
<div class="m2"><p>هنوز عشق، سخن در عیار من دارد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مگر به سلسله عشق برخورم ز جنون</p></div>
<div class="m2"><p>وگرنه عقل چه پروای کار من دارد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جنون رسیده به جایی مرا، که چون مجنون</p></div>
<div class="m2"><p>هزار سنگ به کف، انتظار من دارد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به یاد گلشن کوی تو چشم خونبارم</p></div>
<div class="m2"><p>هزار خرمن گل در کنار من دارد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه دیده‌هاست که دریای خشک لب ز جهان</p></div>
<div class="m2"><p>به یمن دیده تر، بر کنار من دارد</p></div></div>