---
title: >-
    شمارهٔ ۳۲۶
---
# شمارهٔ ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>تماشای گلی کرد آنچنان محو گلستانم</p></div>
<div class="m2"><p>ک گردید آشیان عندلیبان، چشم حیرانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم خوش رام شد با من، مگر کز ناتوانی‌ها</p></div>
<div class="m2"><p>گمان تار مویی برد ازان زلف پریشانم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال غمزه‌اش دارد چنان سر در پی دلها</p></div>
<div class="m2"><p>که ناخن می‌زند بر پاره‌های دل به دامانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مکن در سایه من خواب اگر آسودگی خواهی</p></div>
<div class="m2"><p>که دهقان بر سر ره کرده وقف سنگ طفلانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملال‌انگیز باشد صحبت آشفتگان قدسی</p></div>
<div class="m2"><p>ز بس دلتنگ بودم، غنچه شد گل در گریبانم</p></div></div>