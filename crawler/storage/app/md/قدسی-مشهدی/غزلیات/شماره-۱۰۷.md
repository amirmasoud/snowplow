---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>هرگزم عشق چنین در رگ جان چنگ نداشت</p></div>
<div class="m2"><p>نغمه تا بود بدین نازکی آهنگ نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناله از جای دگر خورد به گوشم ورنه</p></div>
<div class="m2"><p>مطرب این نغمه در آواز دف و چنگ نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق تا دید مرا زار، چنین زار ندید</p></div>
<div class="m2"><p>شوق تا داشت مرا تنگ چنین تنگ نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود کج‌بینی ما باعث حرمان ورنه</p></div>
<div class="m2"><p>هیچ‌وقت آینه حسن بتان زنگ نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق را شیوه دگر گشته وگرنه زین پیش</p></div>
<div class="m2"><p>داشت نیرنگ، ولی این همه نیرنگ نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شکستن به نوا می‌رسدم دل ورنه</p></div>
<div class="m2"><p>هرگز این شیشه چنین آرزوی سنگ نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر ز هم‌صحبتی‌ام یار کند ننگ چه غم</p></div>
<div class="m2"><p>شکر لله که غم از ضحبت من ننگ نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قدسی از روز ازل کز عدم آمد به وجود</p></div>
<div class="m2"><p>از در صلح درآمد به کسی جنگ نداشت</p></div></div>