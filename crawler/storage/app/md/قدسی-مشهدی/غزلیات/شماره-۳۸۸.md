---
title: >-
    شمارهٔ ۳۸۸
---
# شمارهٔ ۳۸۸

<div class="b" id="bn1"><div class="m1"><p>عمری چو جاهلان پی چون و چرا شدم</p></div>
<div class="m2"><p>بستم ز حرف لب، چو به حرف آشنا شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای از گلیم فقر نکردم فزون دراز</p></div>
<div class="m2"><p>با آنکه در دیار سخن، پادشا شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زد بر زمین همان نفسش هرچه برگرفت</p></div>
<div class="m2"><p>عمری چو برگ گل پی باد صبا شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخر شدم چو سبزه لگدکوب خاص و عام</p></div>
<div class="m2"><p>گر چند روز، قابل نشو و نما شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر همیشه سایه‌ام از دست خویش بود</p></div>
<div class="m2"><p>کی ملتفت به سایه بال هما شدم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشتم تمام عشق و ز خود، کام یافتم</p></div>
<div class="m2"><p>آخر به مدعای دل مدعا شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دارم چو صبح، آینه مهر در بغل</p></div>
<div class="m2"><p>قدسی ازان سبب همه صدق و صفا شدم</p></div></div>