---
title: >-
    شمارهٔ ۲۲۱
---
# شمارهٔ ۲۲۱

<div class="b" id="bn1"><div class="m1"><p>به عزم جلوه چو آن گلعذار برخیزد</p></div>
<div class="m2"><p>نشان عافیت از روزگار برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شوق تیر نگنجند آهوان در پوست</p></div>
<div class="m2"><p>به عزم صید چو آن شهسوار برخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر به مرده صد ساله بگذری ز لحد</p></div>
<div class="m2"><p>ز شوق روی تو بی‌اختیار برخیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سوی من چو گذر کرده‌ای دمی بنشین</p></div>
<div class="m2"><p>که حسرت از دل امیدوار برخیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم فراق تو چون تیغ بر میان بندد</p></div>
<div class="m2"><p>ز زندگانی خضر اعتبار برخیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز ابر دیده ازان سیل اشک می‌بارم</p></div>
<div class="m2"><p>که غیر ازان سر کو، چون غبار برخیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به داغ عشق تو قدسی چو جان دهد، ز گلش</p></div>
<div class="m2"><p>به جای سبزه، دل داغدار برخیزد</p></div></div>