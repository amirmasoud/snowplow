---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>عشق را چون شعله غیر از سوختن دربار نیست</p></div>
<div class="m2"><p>هرکه شد ز اهل سلامت مرد این بازار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاش یک بار افتدش بر گلشن کویت گذر</p></div>
<div class="m2"><p>آنکه گوید سرو را پا هست چون رفتار نیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماجرای عشق چندان هست کایشان را بس است</p></div>
<div class="m2"><p>عاشقان را پرسش روز جزا در کار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غنچه از بهر صبا چیده‌ست بر هم برگ گل</p></div>
<div class="m2"><p>ورنه مرغان چمن را آشیان جز خار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون گره بر رشته افتد دست دست ناخن است</p></div>
<div class="m2"><p>بر دل آزرده‌ام رحمی به از آزار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باغ را نظاره‌گی چون دیده در مژگان گرفت</p></div>
<div class="m2"><p>بلبلان را ناله تنها از جفای خار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کفر و دین منسوخ گشت و عشق در کار خودست</p></div>
<div class="m2"><p>قید عاشق همچو شغل سبحه و زنار نیست</p></div></div>