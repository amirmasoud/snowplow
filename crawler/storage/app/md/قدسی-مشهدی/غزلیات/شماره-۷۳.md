---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>فتنه‌جویی ز بت خویش مرا باور نیست</p></div>
<div class="m2"><p>گر دمی بر سر نازست دمی دیگر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکوه از خامی عاشق نکند معشوقی</p></div>
<div class="m2"><p>حسن از عشق در آیین وفا کمتر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر ظرفی که ندارم چه کشم رنج خمار</p></div>
<div class="m2"><p>شیشه را بر لب خود گیرم اگر ساغر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سینه سوراخ شد از گرمی خونم گویا</p></div>
<div class="m2"><p>که ز خونابه حسرت مژه امشب تر نیست</p></div></div>