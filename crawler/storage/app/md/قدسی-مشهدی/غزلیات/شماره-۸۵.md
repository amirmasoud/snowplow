---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>از پریشانی اگر حاصل شود کامم رواست</p></div>
<div class="m2"><p>در خم زلفت دلم را شانه محراب دعاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه دست کوتهم بیگانه است از گردنت</p></div>
<div class="m2"><p>همتی دارم که با سرو بلندت آشناست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میرم از غیرت چو چشم حسرتم در بر کشد</p></div>
<div class="m2"><p>خاک راهت را که چشم توتیا را توتیاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست در زلف تو دارم چون توانم بود امن؟</p></div>
<div class="m2"><p>بر تنم هر تار پیراهن به جای اژدهاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردم چشمم پریشانند از بی‌طاقتی</p></div>
<div class="m2"><p>تا دلم را دست بی‌تابی در آن زلف دوتاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با خیال خاک پایت الفتی دارد از آن</p></div>
<div class="m2"><p>مردم چشم مرا صد چشم حسرت در قفاست</p></div></div>