---
title: >-
    شمارهٔ ۲۷۹
---
# شمارهٔ ۲۷۹

<div class="b" id="bn1"><div class="m1"><p>خامه در وصف لبت کار مسیحا می‌کند</p></div>
<div class="m2"><p>حرف زلفت بر ورق خط را چلیپا می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نباشد هیچ عضوی بر تنش بی درد عشق</p></div>
<div class="m2"><p>لاله داغ خویش را قسمت بر اعضا می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالمی را ابر نیسانی به طوفان می‌دهد</p></div>
<div class="m2"><p>تا به دریا قطره‌ای را در صدف جا می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه پیشش اعتباری نیست عمر خضر را</p></div>
<div class="m2"><p>کی ز حال کشتگان خویش پروا می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از هجوم تیرباران غمت در سینه‌ام</p></div>
<div class="m2"><p>ناله جای خود به صد تشویش پیدا می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر دل ما دارد از روی محبت نکته‌ها</p></div>
<div class="m2"><p>کی کند با دیگران عشق آنچه با ما می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشتی نوح است گویی گشته بر طوفان سوار</p></div>
<div class="m2"><p>هرکه از چشم ترم نعلین در پا می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش آزادان بود قدر اسیران بیشتر</p></div>
<div class="m2"><p>بر سر سرو سهی قمری ازان جا می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از ترحم بر دل ما ناخنی هرگز نزد</p></div>
<div class="m2"><p>آنکه از کار گرفتاران گره وا می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دل بیرحم خوبان هیچ تاثیری نکرد</p></div>
<div class="m2"><p>ناله قدسی که جا در سنگ خارا می‌کند</p></div></div>