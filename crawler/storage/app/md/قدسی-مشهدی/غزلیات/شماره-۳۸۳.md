---
title: >-
    شمارهٔ ۳۸۳
---
# شمارهٔ ۳۸۳

<div class="b" id="bn1"><div class="m1"><p>تا آفرین شست تو گوید زبان زخم</p></div>
<div class="m2"><p>پیکان زبان خویش کند در دهان زخم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک دم بمان چو زخم زدی، تا که بشنوی</p></div>
<div class="m2"><p>تحسین تیغ خود ز لب خون‌فشان زخم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جنس جفا کسی نستاند به نیم جو</p></div>
<div class="m2"><p>جایی که تیغ یار گشاید دکان زخم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس گرم شکر گفتن بازوی قاتل است</p></div>
<div class="m2"><p>ترسم که تیغ، آب شود در دهان زخم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل را پر از ذخیره لذت کند کنار</p></div>
<div class="m2"><p>غلتد چو نیم بسمل تو در میان زخم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زخم خدنگ عشق چو درمان‌پذیر نیست</p></div>
<div class="m2"><p>بیهوده، ای مسیح مکن امتحان زخم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قدسی دلم شکاف شکاف است در درون</p></div>
<div class="m2"><p>بر سینه گرچه نیست ز بیرون نشان زخم</p></div></div>