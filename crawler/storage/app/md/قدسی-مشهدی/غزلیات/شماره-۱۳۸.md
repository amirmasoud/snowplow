---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>ما را ز دست جور تو پای گریز نیست</p></div>
<div class="m2"><p>راحت، نصیب دیده خونابه ریز نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد سنگ، خاک در کف طفلان ز انتظار</p></div>
<div class="m2"><p>داغم ازین خرابه که دیوانه‌خیز نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با دشمنم چه کار که از بی‌تعلقی</p></div>
<div class="m2"><p>با دوست هم مرا سر و برگ ستیز نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بزم اهل درد به یک جو نمی‌خرند</p></div>
<div class="m2"><p>گر شیشه‌ای ز سنگ بلا ریزریز نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوبان این دیار ندارند یک شهید</p></div>
<div class="m2"><p>دردا که تیغ غمزه درین شهر تیز نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویا ز چشم حلقه زلفش فتاده است</p></div>
<div class="m2"><p>باد صبا که می‌وزد و مشک بیز نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داغم که دم ز سوز محبت چرا زند</p></div>
<div class="m2"><p>پروانه را که بال و پر شعله ریز نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قدسی فتاده‌ام به طلسمی که چون قفس</p></div>
<div class="m2"><p>صد رخنه بیش دارد و راه گریز نیست</p></div></div>