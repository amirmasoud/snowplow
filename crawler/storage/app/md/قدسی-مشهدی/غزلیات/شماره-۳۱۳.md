---
title: >-
    شمارهٔ ۳۱۳
---
# شمارهٔ ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>سوزم همیشه از نفس آتشین خویش</p></div>
<div class="m2"><p>چون شمع ایستاده‌ام، اما به کین خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ظاهر شود ز درد سر اکسیر سازی‌ام</p></div>
<div class="m2"><p>صندل کنم ز بس که طلا بر جبین خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شوق دامنت همه تن دست گشته‌ام</p></div>
<div class="m2"><p>چون شمع می‌کشم نفس از آستین خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهرت به تازه‌ساختن داغ یافته‌ام</p></div>
<div class="m2"><p>هرکس برای نام خراشد نگین خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی ترا من از همه‌کس پیش دیده‌ام</p></div>
<div class="m2"><p>امّیدوارم از نظر پیش‌بین خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شرم آنکه کینه چرا پیشه کرده‌ام</p></div>
<div class="m2"><p>افشانده‌ام گره چو عرق بر جبین خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با چرا به مهر نیاری شبی به روز</p></div>
<div class="m2"><p>شاید ز روزگار بگیریم کین خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملک دلم خراب نگردد، که بی‌نزاع</p></div>
<div class="m2"><p>داغ تراش کشیده به زیر نگین خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر آستین به دیده رسانم شب فراق</p></div>
<div class="m2"><p>دریای خون روان کنم از آستین خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا برگزیده‌ایم ترا از جهانیان</p></div>
<div class="m2"><p>یک دم نبسته‌ایم لب از آفرین خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دین، دین دلبرم بود و کفر، کفر عشق</p></div>
<div class="m2"><p>هرگز نداشتم خبر از کفر و دین خویش</p></div></div>