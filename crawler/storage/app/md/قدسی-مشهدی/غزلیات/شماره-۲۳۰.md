---
title: >-
    شمارهٔ ۲۳۰
---
# شمارهٔ ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>در هجرت، از شکست، دلم را اثر نماند</p></div>
<div class="m2"><p>زین پیش بی تو بود قرارم، دگر نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمعی که تازه کشته شود، دودش اندک است</p></div>
<div class="m2"><p>بازآ که بی تو یک نفسم بیشتر نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بلبلان، بقای شما باد در چمن</p></div>
<div class="m2"><p>کز ما به نیم چاشت چو شبنم اثر نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقتی به پرسش دلم آمد خدنگ یار</p></div>
<div class="m2"><p>کز گریه نیم قطره خون در جگر نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر من ز باغ وصل چنان بسته‌اند راه</p></div>
<div class="m2"><p>کامیدواری‌ام به نسیم سحر نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قدسی چنان گداخت ز تاثیر درد عشق</p></div>
<div class="m2"><p>کز نام‌بردنش به زبانها خبر نماند</p></div></div>