---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>گر گشایم لب دمی، عالم پر افغان می‌شود</p></div>
<div class="m2"><p>گر کنم دور آستین از دیده، طوفان می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنبه برخواهم گرفت از داغهای خویشتن</p></div>
<div class="m2"><p>مژده ده پروانه را کامشب چراغان می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا مباد از پیش من بر هم خورد بازار ابر</p></div>
<div class="m2"><p>گریه کمتر می‌کنم روزی که باران می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به کام دل درم من هم گریبانی چو شمع</p></div>
<div class="m2"><p>تا به عطف دامن از چشمم گریبان می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون زلیخا قدر یوسف را چه می‌داند کسی</p></div>
<div class="m2"><p>گر خریدار او نباشد مصر کنعان می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیل اشکم خشت دیگر بر زمین افکند، ازان</p></div>
<div class="m2"><p>هر خراب، آباد و هر آباد، ویران می‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد دل آشفته قدسی می‌خورد بر یکدگر</p></div>
<div class="m2"><p>تا به امداد صبا، زلفی پریشان می‌شود</p></div></div>