---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>هر سر موی من از دود تو در فریادست</p></div>
<div class="m2"><p>ناله‌ام نغمه نی نیست که گویی بادست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده بی‌نور شود گر نکنم گریه چو شمع</p></div>
<div class="m2"><p>مردم چشم مرا خانه ز سیل آبادست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تندی خوی تو از تاله فرو بسته لبم</p></div>
<div class="m2"><p>ورنه حیثیت مرغان چمن فریادست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برگ سبزی به چمن کو که نشوید ابرش؟</p></div>
<div class="m2"><p>بر خط سبز مکن تکیه که سرو آزادست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر چارسوی عشق هنرمندانند</p></div>
<div class="m2"><p>هرکه شاگردی این طایفه کرد استادست</p></div></div>