---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>روح‌القدس ار دیده گشاید به جمالت</p></div>
<div class="m2"><p>ایمن ننشیند ز فریب خط و خالت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هیچ چمن چون تو گلی نیست، ندانم</p></div>
<div class="m2"><p>دهقان ز گلستان که آورده نهالت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد سر اعجاز تو گردم که شود شب</p></div>
<div class="m2"><p>آتشکده سینه گلستان ز خیالت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شادم که مرا قابل هجران شمری هم</p></div>
<div class="m2"><p>من کیستم و آرزوی بزم وصالت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای مرغ حرم بر قفس مرغ گرفتار</p></div>
<div class="m2"><p>زنهار مزن پر که بسوزد پر و بالت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب‌ها تو به خواب خوش و از شوق تو قدسی</p></div>
<div class="m2"><p>گردد همه شب گرد سراپای خیالت</p></div></div>