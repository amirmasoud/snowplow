---
title: >-
    شمارهٔ ۴۲۱
---
# شمارهٔ ۴۲۱

<div class="b" id="bn1"><div class="m1"><p>می‌شود هر دم پریشان زلف بر رخسار او</p></div>
<div class="m2"><p>کز پریشان خاطری یادش دهد هر تار او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بیابان محبت سرسری مگذر چو باد</p></div>
<div class="m2"><p>کز گریبان گل دمد، دامن چو گیرد خار او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سر کویش مسیحا تن به بیماری دهد</p></div>
<div class="m2"><p>تا کند چون ناتوانان تکیه بر دیوار او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باغ امید مرا ترسم نماند میوه‌ای</p></div>
<div class="m2"><p>ناامیدی چند سازد رخنه در دیوار او؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خشت خشت خانه گل را صبا بر باد داد</p></div>
<div class="m2"><p>با وجود آنکه عمری بود خود معمار او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در میان خنده، چشم گل ز شبنم شد پر آب</p></div>
<div class="m2"><p>صبحدم چون کرد بلبل ناله‌ای در کار او</p></div></div>