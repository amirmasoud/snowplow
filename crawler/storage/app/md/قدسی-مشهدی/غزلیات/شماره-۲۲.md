---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>وبال جان اسیران مکن رهایی را</p></div>
<div class="m2"><p>مده به اهل وفا یاد بی‌وفایی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مرگ هم نبریدم به هرکه پیوستم</p></div>
<div class="m2"><p>کسی نخوانده چو من جزو آشنایی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میسرست وصالت مرا ولی چه وصال</p></div>
<div class="m2"><p>که یاد می‌کنم ایام بی‌نوایی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهی ستاره روشن که دیده شب چو چراغ</p></div>
<div class="m2"><p>تمام کرد به روی تو روشنایی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا ز عشق بتان پیشه مشق رسوایی‌ست</p></div>
<div class="m2"><p>فکنده‌ام ز قلم حرف پارسایی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جز تو قدسی اگر داده دل به یار دگر</p></div>
<div class="m2"><p>قبول کرده ز بت دعوی خدایی را</p></div></div>