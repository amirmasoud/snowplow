---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>چو نمی‌کنی نگاهی به ستم مران خدا را</p></div>
<div class="m2"><p>نکنی اگر نوازش مشکن دل گدا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه حیرتم که هرگز چو نبوده آشنایی</p></div>
<div class="m2"><p>به جهان که گفته چند این سخنان آشنا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شدی تمام خواهش چه زنی در اجابت؟</p></div>
<div class="m2"><p>به دم فسرده هرگز نبود اثر دعا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شده قاصد آنچنان کم به میان دوست‌داران</p></div>
<div class="m2"><p>که ز مصر سوی کنعان نفتد گذر صبا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفسی ز من نگشتی دل نازک تو غافل</p></div>
<div class="m2"><p>به تو گر خدای دادی دل مهربان ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز طراوت جمالت به هزار دیده مرغان</p></div>
<div class="m2"><p>نکنند فرق از هم به چمن گل و گیا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم عشق را به صد جان چو کنند بیع قدسی</p></div>
<div class="m2"><p>ندهی ز دست ارزان گهر گران‌بها را</p></div></div>