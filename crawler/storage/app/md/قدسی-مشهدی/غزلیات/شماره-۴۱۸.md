---
title: >-
    شمارهٔ ۴۱۸
---
# شمارهٔ ۴۱۸

<div class="b" id="bn1"><div class="m1"><p>چرخ چون کشتی رود بر روی آب از چشم من</p></div>
<div class="m2"><p>خانه ناموس طوفان شد خراب از چشم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که از دیدار خود محروم می‌خواهد مرا</p></div>
<div class="m2"><p>بگذرد شبها خیالش در نقاب از چشم من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شعله خون آلوده آید از دل اخگر برون</p></div>
<div class="m2"><p>گر به روی آتش افشانند آب از چشم من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نه سودای گل روی تو می‌پختم، چرا</p></div>
<div class="m2"><p>دوش می‌آمد به جای خون، گلاب از چشم من؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز و شب روی تو دارم در نظر، نبود عجب</p></div>
<div class="m2"><p>گر که جای اشک ریزد آفتاب از چشم من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده پرخون برون آید به جای گل ز شاخ</p></div>
<div class="m2"><p>گر به گلشن قطره افشاند سحاب از چشم من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گریه‌ام شد مانع نظّاره، روز وصل هم</p></div>
<div class="m2"><p>تا به کی نظّاره باشد در عذاب از چشم من؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر کنم قدسی در آتش جای با این اشک گرم</p></div>
<div class="m2"><p>شعله گردد در دل اخگر کباب از چشم من</p></div></div>