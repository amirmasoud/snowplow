---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>دستم ز جام عکس می لاله‌گون گرفت</p></div>
<div class="m2"><p>گل چیدم آنقدر که کفم رنگ خون گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ممنون دُرد و صاف حریفان نمی‌شود</p></div>
<div class="m2"><p>چون نرگس آنکه ساغر خالی، شگون گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از اشک بی‌ملاحظه مرغان باغ را</p></div>
<div class="m2"><p>این شرم بس که دامن گل رنگ خون گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون مهر در رگ همه کس جای کرده‌ام</p></div>
<div class="m2"><p>قدسی شکست رنگ مرا هرکه خون گرفت</p></div></div>