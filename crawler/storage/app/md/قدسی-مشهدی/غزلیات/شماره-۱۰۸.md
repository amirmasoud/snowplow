---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>رسید یار و ز من بر سر عتاب گذشت</p></div>
<div class="m2"><p>چه گویمت که چه بر دل ز اضطراب گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبرد غنچه بختم سوی شکفتن راه</p></div>
<div class="m2"><p>گل امیدم ازین باغ در نقاب گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجاست عشق که در دیده‌ام نمک پاشد</p></div>
<div class="m2"><p>که روزگار به آسودگی و خواب گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بزم شوق گر این نشاه می‌دهد می عشق</p></div>
<div class="m2"><p>هزار حیف ز عمری که بی‌شراب گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگه ز رشک به رویش نبرد ره قدسی</p></div>
<div class="m2"><p>چو روزگار تو محروم از آفتاب گذشت</p></div></div>