---
title: >-
    شمارهٔ ۲۱۶
---
# شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>در چمن کی دلم از فیض هوا بگشاید؟</p></div>
<div class="m2"><p>پرده بگشا که ز رویت دل ما بگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیش این باغ به اندازه یک تنگدل است</p></div>
<div class="m2"><p>کاش گل غنچه شود تا دل ما بگشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سر نکهت زلفت چو صبا می‌لرزم</p></div>
<div class="m2"><p>که مبادا سر زلف تو صبا بگشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمرها رفت که لب‌تشنه تیغ ستمیم</p></div>
<div class="m2"><p>رحمتی کو که رگ ابر بلا بگشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوی پیراهن یوسف به صبا باز دهند</p></div>
<div class="m2"><p>هر کجا یوسف من بند قبا بگشاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بود بوی سر زلف تو همراه صبا</p></div>
<div class="m2"><p>بوستان دست به تاراج صبا بگشاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا که از سینه برون کرده غمی باز، که عشق</p></div>
<div class="m2"><p>می‌فرستد به دلم مژده که جا بگشاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آسمان چون مه نو، گر همه ناخن گردد</p></div>
<div class="m2"><p>نتواند گره از رشته ما بگشاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچ‌کس رشته ز مکتوب دلم باز نکرد</p></div>
<div class="m2"><p>سر این نامه مگر روز جزا بگشاید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قدسی از عشق رهایی مطلب کاین صیاد</p></div>
<div class="m2"><p>بند بر دل چو نهد، رشته ز پا بگشاید</p></div></div>