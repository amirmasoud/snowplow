---
title: >-
    شمارهٔ ۳۹۲
---
# شمارهٔ ۳۹۲

<div class="b" id="bn1"><div class="m1"><p>چو درد عشق تو کرد آشنای خویشتنم</p></div>
<div class="m2"><p>غمت چرا نخورد غم برای خویشتنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم چه یافته در کوچه پریشانی؟</p></div>
<div class="m2"><p>که می‌برد همه عمر از قفای خویشتنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا کرشمه دیگر فریب داد ای گل</p></div>
<div class="m2"><p>به رنگ و بو نرود دل ز جای خویشتنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو دل که قطره خون است و خون خورد دایم</p></div>
<div class="m2"><p>یکی شدم به غم و خود غذای خویشتنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس که کرد علاج و نداشت سود مرا</p></div>
<div class="m2"><p>طبیب کرد خجل از دوای خویشتنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز عشق، گرد جنون نیست بر جبین دلم</p></div>
<div class="m2"><p>که کرد چشم خرد، توتیای خویشتنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به من ز دوستی‌ات شد جهان چنان دشمن</p></div>
<div class="m2"><p>که در هراس ز بند قبای خویشتنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شده‌ست پی سپر راه عشق تا پایم</p></div>
<div class="m2"><p>بود چو نقش قدم، رخ به پای خویشتنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برای گشت چمن مضطرب نیم چو نسیم</p></div>
<div class="m2"><p>چو شعله رقص‌کنان در هوای خویشتنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیم به عیب کس از عیب خویشتن مشغول</p></div>
<div class="m2"><p>تمام خارم و مخصوص پای خویشتنم</p></div></div>