---
title: >-
    شمارهٔ ۴۴۱
---
# شمارهٔ ۴۴۱

<div class="b" id="bn1"><div class="m1"><p>ما چو پروانه نسوزیم به داغ غلطی</p></div>
<div class="m2"><p>شمع در محفل ما سوخت دماغ غلطی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز ما را ز شب تیره جدا نتوان کرد</p></div>
<div class="m2"><p>صبح برکرده ز خورشید، چراغ غلطی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ره عشق گذشتم ز خرد، گام نخست</p></div>
<div class="m2"><p>که نیندازم از ره به سراغ غلطی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرده ساقی عشقم که به صد گردش جام</p></div>
<div class="m2"><p>از حریفم ننوازد به ایاغ غلطی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکر لله که ز سودای تو گرم است دلم</p></div>
<div class="m2"><p>نیستم سوخته چون لاله به داغ غلطی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خویش را در دل صحرای جنون گم کردم</p></div>
<div class="m2"><p>عقل در یافتنم سوخت دماغ غلطی</p></div></div>