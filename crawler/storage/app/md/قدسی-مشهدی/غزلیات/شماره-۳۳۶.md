---
title: >-
    شمارهٔ ۳۳۶
---
# شمارهٔ ۳۳۶

<div class="b" id="bn1"><div class="m1"><p>چه شعله‌ها زده سر ز آتشی که من دارم</p></div>
<div class="m2"><p>هزار نشاه نو زین می کهن دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به آن رسیده که عشقم به غربت اندازد</p></div>
<div class="m2"><p>ازین ملال غریبی که در وطن دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر کجا که تو باشی، فغان من آنجاست</p></div>
<div class="m2"><p>اگرچه در قفسم، ناله در چمن دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسید وعده رفتن، مرو ز بالینم</p></div>
<div class="m2"><p>که مانده یک نفس و با تو صد سخن دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا و سینه تنگم شکاف ساز و ببین</p></div>
<div class="m2"><p>چو غنچه جز دل پرخون چه در کفن دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کوی او همه شب روشن است دیده من</p></div>
<div class="m2"><p>بود به خانه مه، روزنی که من دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر به سینه زنم صد شکاف، معذورم</p></div>
<div class="m2"><p>دلی فتاده در آن چاک پیرهن دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز کوی او به جفا پا نمی‌کشم قدسی</p></div>
<div class="m2"><p>نظر ز همت مجنون و کوهکن دارم</p></div></div>