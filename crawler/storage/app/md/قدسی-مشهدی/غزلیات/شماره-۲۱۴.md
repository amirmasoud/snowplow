---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>هرگز مرا به کعبه ز دیر التجا نشد</p></div>
<div class="m2"><p>یک حاجتم نماند که آنجا روا نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بختم فریب جلوه نیک‌اختری نخورد</p></div>
<div class="m2"><p>فرقم زبون سایه بال هما نشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حیرت از شکستگی شیشه دلم</p></div>
<div class="m2"><p>با آنکه هرگز از کف خوبان رها نشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز وصال نیست جز این حیرتم که چون</p></div>
<div class="m2"><p>در دیده‌ام نظاره ازین بیش جا نشد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا عشق، توبه داد دلم را ز ترک خویش</p></div>
<div class="m2"><p>یک سجده‌ام ز طاعت خوبان قضا نشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باشد هنوز حسرت تیر تو در دلم</p></div>
<div class="m2"><p>با آنکه یک خدنگ تو از دل خطا نشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ننشست فتنه‌ای ز حوادث درین دیار</p></div>
<div class="m2"><p>کز قامت تو فتنه دیگر بپا نشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزی به شام برد به کوری، چو خفتگان</p></div>
<div class="m2"><p>صبحی که چشم مهر به روی تو وا نشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک بار یافتم ز تو دستور سجده‌ای</p></div>
<div class="m2"><p>چون سایه‌ام ز خاک، دگر تن جدا نشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با آنکه نقد عمر، مرا صرف دوست شد</p></div>
<div class="m2"><p>یک روزه دین مدت وصلش ادا نشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرجا حدیث زلف تو مذکور شد مرا</p></div>
<div class="m2"><p>بر تن کدام مو که زبان دعا نشد؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما را همین بس است که بیگانه شد ز غیر</p></div>
<div class="m2"><p>قدسی چه غم که یار به ما آشنا نشد</p></div></div>