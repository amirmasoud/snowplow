---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>هر روز به من یار ز نو بر سر نازست</p></div>
<div class="m2"><p>پیوسته مرا لذت آغاز نیاز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذار که در تیرگی بخت بمانم</p></div>
<div class="m2"><p>آیینه چو روشن شود افشاگر رازست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوتاه امل باش که چون رشته سوزن</p></div>
<div class="m2"><p>پیوسته گره می‌خورد آن سر که دراز است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بت نه همین زینت بتخانه مایی</p></div>
<div class="m2"><p>در کعبه هم ابروی تو محراب نمازست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پستی فطرت چه شوی بسته صورت؟</p></div>
<div class="m2"><p>یک گام به معراج حقیقت ز مجازست</p></div></div>