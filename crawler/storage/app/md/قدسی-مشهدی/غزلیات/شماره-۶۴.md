---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>بی تو شب تا روز چون شمعم به چشم تر گذشت</p></div>
<div class="m2"><p>اشک دامانم گرفت و آتشم از سر گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر راهش ندارم لذتی از انتظار</p></div>
<div class="m2"><p>یار پنداری که امروز از ره دیگر گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه مشکل بود عمری حالم از نادیدنش</p></div>
<div class="m2"><p>دوش با من بود و بر من حال مشکل‌تر گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوق چون زور آورد اندیشه طاقت چه سود</p></div>
<div class="m2"><p>دست و پا نتوان زدن جایی که آب از سر گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که از چشمم گریزان است آن آب حیات</p></div>
<div class="m2"><p>چون ز من بگذشت پنداری ز آتش برگذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الحذر از آه قدسی کامشب از درد فراق</p></div>
<div class="m2"><p>تا به لب از سینه آهش بر سر نشتر گذشت</p></div></div>