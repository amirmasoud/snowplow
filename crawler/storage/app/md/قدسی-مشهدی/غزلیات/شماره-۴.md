---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>به پیامی که کند باد صبا یاد مرا</p></div>
<div class="m2"><p>روم از دست ندانم که چه افتاد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کمند سر زلف تو گرفتار مباد</p></div>
<div class="m2"><p>آنکه خواهد کند از قید تو آزاد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دشمنی کر پی بیداد مرا یاد کند</p></div>
<div class="m2"><p>به از آن دوست که هرگز نکند یاد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش وقت سحر از حسرت گل مرغ چمن</p></div>
<div class="m2"><p>ناله‌ای کرد که آورد به فریاد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن ستم کرد شب هجر که در روز وصال</p></div>
<div class="m2"><p>نتوان کرد به صد عذر ستم شاد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاد از آنم به خرابی که چو ویران گردد</p></div>
<div class="m2"><p>خانه چون گل نتوان ساختن آباد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچنان دور فتادم ز خرابات که دوش</p></div>
<div class="m2"><p>سبحه چون آبله از دست نیفتاد مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نکنم ترک نظربازی خوبان قدسی</p></div>
<div class="m2"><p>به جز این شیوه نیاموخته استاد مرا</p></div></div>