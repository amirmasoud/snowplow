---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>ایام بهارست و هوای چمنم نیست</p></div>
<div class="m2"><p>شادم به غمت ذوق گل و یاسمنم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر شور قیامت شود از خاک نخیزم</p></div>
<div class="m2"><p>چون غنچه سر نشو و نما در کفنم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون گلشن تصویر، گلم بوی ندارد</p></div>
<div class="m2"><p>با آنکه گل ساخته‌ای در چمنم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون عکس در آیینه گهی، گاه در آبم</p></div>
<div class="m2"><p>بیرون ز دل صاف‌ضمیران وطنم نیست</p></div></div>