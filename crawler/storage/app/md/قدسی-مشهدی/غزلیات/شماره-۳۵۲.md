---
title: >-
    شمارهٔ ۳۵۲
---
# شمارهٔ ۳۵۲

<div class="b" id="bn1"><div class="m1"><p>عافیت غم را مداوا کرد و زین غم سوختم</p></div>
<div class="m2"><p>هرکسی از داغ سوزد، من ز مرهم سوختم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خنده‌های شادی گل در چمن داغم نکرد</p></div>
<div class="m2"><p>غنچه را دیدم غمی دارد، ازان غم سوختم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در محبت شعله افزون گردد آتش را ز آب</p></div>
<div class="m2"><p>تا چو شمعم بود در هر قطره‌ای نم، سوختم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس که دارم ذوق غم، هرجا که دیدم ماتمی‌ست</p></div>
<div class="m2"><p>من در آن ماتم، فزون از اهل ماتم سوختم</p></div></div>