---
title: >-
    شمارهٔ ۴۲۳
---
# شمارهٔ ۴۲۳

<div class="b" id="bn1"><div class="m1"><p>جان چیست کش فدا نکنم از برای تو؟</p></div>
<div class="m2"><p>خاکم به سر اگر نکنم جان فدای تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنهان ز غیر، شب همه شب با چراغ چشم</p></div>
<div class="m2"><p>در کوچه تو می‌طلبم نقش پای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پروا نمی‌کنی و من از ناله‌های شب</p></div>
<div class="m2"><p>پر کرده گوش چرخ ز دست جفای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل می‌بری و فکر اسیران نمی‌کنی</p></div>
<div class="m2"><p>بیچاره آن کسی که شود مبتلای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش از تو بوی مهر و وفایی شنیده‌ام</p></div>
<div class="m2"><p>گرد سر تو گردم و مهر و وفای تو</p></div></div>