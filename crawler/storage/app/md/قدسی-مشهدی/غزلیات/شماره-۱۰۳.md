---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>تا صبا با آن سر زلف پریشان آشناست</p></div>
<div class="m2"><p>صد گره از غیرتم با رشته جان آشناست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم هجوم آورد و من در فکر بی‌سامانی‌ام</p></div>
<div class="m2"><p>میزبان خجلت کشد هرچند مهمان آشناست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه باداباد ما کشتی در آب انداختیم</p></div>
<div class="m2"><p>گر بود بیگانه باد شرطه طوفان آشناست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمرها شد حسرت چاک گریبان می‌کشم</p></div>
<div class="m2"><p>با وجود آنکه دستم با گریبان آشناست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از غرور حسن ظاهر می‌کند بیگانگی</p></div>
<div class="m2"><p>ورنه عمری شد به من از خویش پنهان آشناست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>استخوانم حالتی دارد که چون گردد هدف</p></div>
<div class="m2"><p>می‌شناسد ناوکش را زانکه پیکان آشناست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده قدسی حسد ورزیده در راه حرم</p></div>
<div class="m2"><p>بر کف پایی که با خار مغیلان آشناست</p></div></div>