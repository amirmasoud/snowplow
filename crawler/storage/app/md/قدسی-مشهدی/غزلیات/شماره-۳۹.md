---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>پرهیز ده ز هجر گرفتار خویش را</p></div>
<div class="m2"><p>بنگر شکسته‌رنگی بیمار خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر ذخیره شب هجر تو روز وصل</p></div>
<div class="m2"><p>کم کرد دیده گریه بسیار خویش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیداد دوست چون ستم چرخ عام نیست</p></div>
<div class="m2"><p>دانسته‌ام غرور ستمکار خویش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز شغل دوستی نبود کار دیگرم</p></div>
<div class="m2"><p>شکر خدا که یافته‌ام کار خویش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدسی هوای باغ و لب جو چه می‌کنی</p></div>
<div class="m2"><p>دریاب فیض سایه دیوار خویش را</p></div></div>