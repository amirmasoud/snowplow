---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>تا ز رویش گلستان کردم نگاه خویش را</p></div>
<div class="m2"><p>خود زدم آتش به دست خود گیاه خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکوه‌ای در دل گذشت از هجر او تیغم سزاست</p></div>
<div class="m2"><p>هیچ‌کس چون خود نمی‌داند گناه خویش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو خواب‌آلوده از کاروان افتاده دور</p></div>
<div class="m2"><p>در تماشایش نظر گم کرده راه خویش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌شود معلوم سوز سینه از دود جگر</p></div>
<div class="m2"><p>همچو مشک آورده‌ام با خود گواه خویش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم از سوز درون رمزی و دل‌ها شد کباب</p></div>
<div class="m2"><p>وای اگر می‌دادم از دل رخصت آه خویش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست قدسی شام تنهایی جز او کس بر سرم</p></div>
<div class="m2"><p>چون ندارم عزت بخت سیاه خویش را؟</p></div></div>