---
title: >-
    شمارهٔ ۱۹۵
---
# شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>کسی کو عشق‌بازی پیشه دارد</p></div>
<div class="m2"><p>کی از رسوا‌شدن اندیشه دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ریشی که خون از وی نجوشد</p></div>
<div class="m2"><p>چو سنگی دان که زخم تیشه دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکش از سینه ریشم که تیرت</p></div>
<div class="m2"><p>بود نخلی که در جان ریشه دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل قدسی نمی‌ترسد ز شادی</p></div>
<div class="m2"><p>که شیری چون غمت در بیشه دارد</p></div></div>