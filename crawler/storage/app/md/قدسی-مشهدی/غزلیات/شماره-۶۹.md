---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>زاهد ز منع تو دل صد بینوا شکست</p></div>
<div class="m2"><p>خون پیاله ریختی و رنگ ما شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آگه نیم که سنگ کجا خورده شیشه‌ام</p></div>
<div class="m2"><p>دانم که دل شکسته ندانم کجا شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امشب که بود نکهت پیراهن امید</p></div>
<div class="m2"><p>طالع نگر که خار به پای صبا شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامن‌کشان گذشتی و صد جیب پاره شد</p></div>
<div class="m2"><p>بیگانه گشتی و دل صد آشنا شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی دهیم جلوه دل زنگ بسته را؟</p></div>
<div class="m2"><p>هرکس شکست آینه ما بجا شکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خارخار سینه دلم را قرار نیست</p></div>
<div class="m2"><p>بازم ز رهگذار که خاری به پا شکست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق قدم به کوی سلامت نمی‌نهد</p></div>
<div class="m2"><p>خواهد برای شیشه خویش از خدا شکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سنجیده دل به شادی عالم غم ترا</p></div>
<div class="m2"><p>خاکش به سر که گوهر غم را بها شکست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قدسی به کام خویش مراد انتخاب کن</p></div>
<div class="m2"><p>چون لطف یار قفل در مدعا شکست</p></div></div>