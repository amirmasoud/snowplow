---
title: >-
    شمارهٔ ۳۴۶
---
# شمارهٔ ۳۴۶

<div class="b" id="bn1"><div class="m1"><p>ما چشم سیه بر شجر طور نداریم</p></div>
<div class="m2"><p>جز لاله درین بادیه منظور نداریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این طرفه که پیوسته گرفتار خماریم</p></div>
<div class="m2"><p>با آنکه لب از می چو قدح دور نداریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کام دل از خنجر قصاب نگیریم</p></div>
<div class="m2"><p>از دامن او دست به ساطور نداریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی روی تو گر آینه گردیم، ملولیم</p></div>
<div class="m2"><p>ور مهر شویم، از تو جدا نور نداریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سایه جغد است نشاط ابد ما</p></div>
<div class="m2"><p>آن روز که ماتم نبود، سور نداریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از حسرت نزدیکی خورشید، هلاکیم</p></div>
<div class="m2"><p>هرچند که تاب نظر از دور نداریم</p></div></div>