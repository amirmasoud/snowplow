---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>گذشت فصل گل و رغبت چمن باقی‌ست</p></div>
<div class="m2"><p>وداع کرد شراب و خمار من باقی‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برای جیب دریدن عزیز دارم دست</p></div>
<div class="m2"><p>اگرچه پیرهنم پاره شد کفن باقی‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا گمان که سخن شد تمام و نشنیدی</p></div>
<div class="m2"><p>سخن نمی‌شنوی ورنه صد سخن باقی‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کفایت است دلیل بقای ناز و نیاز</p></div>
<div class="m2"><p>فسانه‌ای که ز شیرین و کوه‌کن باقی‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکست جام و حریفان شدند و مرد چراغ</p></div>
<div class="m2"><p>ز سادگی دل من خوش که انجمن باقی‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر روی به سفر غربت است و غم قدسی</p></div>
<div class="m2"><p>وگر سفر نکنی محنت وطن باقی‌ست</p></div></div>