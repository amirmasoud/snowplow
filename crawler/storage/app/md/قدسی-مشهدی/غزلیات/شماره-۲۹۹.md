---
title: >-
    شمارهٔ ۲۹۹
---
# شمارهٔ ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>آغشته‌ام چو پنبه ز خوناب داغ خویش</p></div>
<div class="m2"><p>منت ز شاخ گل نپذیرم به باغ خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آفتاب با همه کس گرمخون مباش</p></div>
<div class="m2"><p>پروانه را دلیر مکن بر چراغ خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوی گلم دماغ خراشد درین چمن</p></div>
<div class="m2"><p>در باغم و چو غنچه بگیرم دماغ خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را چو کرد دستخوش خویش درد عشق</p></div>
<div class="m2"><p>خود آستین زنیم به شمع و چراغ خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایام گل گذشت و شراب طرب نماند</p></div>
<div class="m2"><p>شد وقت آنکه پر کنم از خون ایاغ خویش</p></div></div>