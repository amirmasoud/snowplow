---
title: >-
    شمارهٔ ۲۸۴
---
# شمارهٔ ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>بی درد عشق، شادی و غم را چه اعتبار</p></div>
<div class="m2"><p>بی خاک درگه تو قسم را چه اعتبار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقد سرشک می‌دهدم دیده دم‌بدم</p></div>
<div class="m2"><p>در کیسه کریم، درم را چه اعتبار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دودی ز شعله بس بودم، داغ گو مباش</p></div>
<div class="m2"><p>هرجا قناعت است، کرم را چه اعتبار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما تاج‌بخش خاک‌نشینیم، پیش ما</p></div>
<div class="m2"><p>جم را چه قدر و مسند جم را چه اعتبار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر باد رفت ملک سلیمان و حشمتش</p></div>
<div class="m2"><p>اینجا غرور خیل و حشم را چه اعتبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گیرم که ره برد به دل عاشقان هوس</p></div>
<div class="m2"><p>در کعبه فرض کن که صنم را چه اعتبار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نقش پا ز خاک‌نشینان آن دریم</p></div>
<div class="m2"><p>در کوی دوست، مسند جم را چه اعتبار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیوانگان به داغ فرود آورند سر</p></div>
<div class="m2"><p>اینجا نگین خاتم جم را چه اعتبار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر عاشقی، به منزل مقصود راه بر</p></div>
<div class="m2"><p>قدسی بنای دیر و حرم را چه اعتبار</p></div></div>