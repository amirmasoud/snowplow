---
title: >-
    شمارهٔ ۱۵۳
---
# شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>از چشمه‌سار چشمم از بس که نم برآید</p></div>
<div class="m2"><p>ترسم که رفته رفته طوفان غم برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از اتحاد چشمم با پای، در ره عشق</p></div>
<div class="m2"><p>مالم چو دیده بر خاک، نقش قدم برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دست شام هجران گیرد گلوی شب را</p></div>
<div class="m2"><p>مشکل که تا قیامت، از صبح، دم برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در موج‌خیز دریا هر لحظه نیست طوفان</p></div>
<div class="m2"><p>کز رشک آب چشمم دریا به هم برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بار محنت دل فرسود جسم قدسی</p></div>
<div class="m2"><p>یک مشت استخوان چند با کوه غم برآید؟</p></div></div>