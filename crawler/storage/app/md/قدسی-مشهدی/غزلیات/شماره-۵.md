---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>تا بود گریه کی آباد شود خانه ما؟</p></div>
<div class="m2"><p>جغد را پای به گل رفته به ویرانه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما از آن سوختگانیم که معمار ازل</p></div>
<div class="m2"><p>طرح آتشکده برداشت ز کاشانه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق پیوسته به دنبال دلم می‌گردد</p></div>
<div class="m2"><p>شعله آید به طلبکاری پروانه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جرم می خوردن ما نیست کم از طاعت کس</p></div>
<div class="m2"><p>کار صد توبه کند گریه مستانه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون تهی دیده که آرد به کسی روی نیاز</p></div>
<div class="m2"><p>چشم بر چشم صراحی زده پیمانه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرف دیوانه شنیدن ز خردمندی نیست</p></div>
<div class="m2"><p>عاقلان گوش نکردند بر افسانه ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون سپندی که بود بر سر آتش قدسی</p></div>
<div class="m2"><p>هرگز آرام نگیرد دل دیوانه ما</p></div></div>