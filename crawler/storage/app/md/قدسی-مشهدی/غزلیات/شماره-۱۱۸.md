---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>کعبه عشق است کانجا هیچ محمل ره نیافت</p></div>
<div class="m2"><p>کس به جز عاشق در آن وادی و منزل ره نیافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتاب آمد که بیند عارضش بی‌اختیار</p></div>
<div class="m2"><p>از هجوم غمزه، از روزن به محفل ره نیافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان شب تارم نداند صبح کز خون دلم</p></div>
<div class="m2"><p>سوی روزن هرگزم خورشید از گل ره نیافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وه چه صید لاغری قدسی، که مُردی و ز ننگ</p></div>
<div class="m2"><p>ذوق بسمل‌کردنت در طبع قاتل ره نیافت</p></div></div>