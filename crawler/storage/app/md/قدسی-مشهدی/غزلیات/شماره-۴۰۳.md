---
title: >-
    شمارهٔ ۴۰۳
---
# شمارهٔ ۴۰۳

<div class="b" id="bn1"><div class="m1"><p>در میان بیخودی، آرامش دل یافتم</p></div>
<div class="m2"><p>من ز گرداب محبت، ذوق ساحل یافتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خضر و سرگردانی سرچشمه حیوان، که من</p></div>
<div class="m2"><p>لذت عمر ابد از تیغ قاتل یافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اضطراب ناز از بهر نیازست اینقدر</p></div>
<div class="m2"><p>کعبه را هم تیره‌روز از هجر محمل یافتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذوق سرگردانی‌ام آواره دارد هر طرف</p></div>
<div class="m2"><p>ورنه من در گام اول، ره به منزل یافتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نهم سر در پی دل، وز که جویم زو سراغ؟</p></div>
<div class="m2"><p>در بیابانی که صد چون خضر، بسمل یافتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرد شمع دل به گردم همچو فانوس خیال</p></div>
<div class="m2"><p>تا چو قدسی جای او در خلوت دل یافتم</p></div></div>