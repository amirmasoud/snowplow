---
title: >-
    شمارهٔ ۲۸۸
---
# شمارهٔ ۲۸۸

<div class="b" id="bn1"><div class="m1"><p>نگهت فتنه‌گر و عربده‌سازست هنوز</p></div>
<div class="m2"><p>سرمه در چشم تو، همخانه نازست هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تازه شد دوستی ما به خط تازه تو</p></div>
<div class="m2"><p>ناز کن، ناز که آغاز نیازست هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راه نزدیک حرم، سعی مرا ناقص کرد</p></div>
<div class="m2"><p>لیک شادم که ره شوق درازست هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک شد پیکر محمود و ز تاثیر وفا</p></div>
<div class="m2"><p>دل او در شکن زلف ایازست هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد ز میخانه و خم کعبه مقصد نزدیک</p></div>
<div class="m2"><p>چشم کج‌بین به ره دور حجازست هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش حسن تو ننشسته هنوز از گرمی</p></div>
<div class="m2"><p>دل خلقی ز تو در سوز و گداز است هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه نبود سر مویی ز حقیقت خالی</p></div>
<div class="m2"><p>دل قدسی ز پی عشقِ مجازست هنوز</p></div></div>