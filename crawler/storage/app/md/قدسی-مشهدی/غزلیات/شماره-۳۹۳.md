---
title: >-
    شمارهٔ ۳۹۳
---
# شمارهٔ ۳۹۳

<div class="b" id="bn1"><div class="m1"><p>در انتظار تو شد عمرها که چشم به راهم</p></div>
<div class="m2"><p>مپیچ سر ز نیازم، متاب رخ ز گناهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه لذت است ببین انتظار آمدنت را</p></div>
<div class="m2"><p>که در برابر چشم منی و چشم به راهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا همای خرد گو به فرق سایه میفکن</p></div>
<div class="m2"><p>به سر، سیاهی داغ جنون بس است پناهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در انتظار تو ای شمع بزم، تا سحر امشب</p></div>
<div class="m2"><p>چو شمع بر سر مژگان نشسته بود نگاهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عشق، کار دل و دیده‌ام ز یکدگر آید</p></div>
<div class="m2"><p>بود چو شمع، یکی نور چشم و شعله آهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز زلف یار به روز سیاه خویش نشستم</p></div>
<div class="m2"><p>مباد آنکه نشیند کسی به روز سیاهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه می‌کنم که همای سعادتم به سر آید؟</p></div>
<div class="m2"><p>زمانه گو به تمسخر، پری مزن به کلاهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منم که یوسف مصر معانی‌ام به حقیقت</p></div>
<div class="m2"><p>برادران طریقت فکنده‌اند به چاهم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهار تازه‌ام، ایام را ملول نکردم</p></div>
<div class="m2"><p>به فرق خاک بود گرچه نودمیده گیاهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه از وصال تسلی، نه در فراق صبوری</p></div>
<div class="m2"><p>مرا بسوز چو قدسی، که معترف به گناهم</p></div></div>