---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>بی‌درد خسته‌ای که به درمان شد آشنا</p></div>
<div class="m2"><p>شوریده آن سری که به سامان شد آشنا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فیض شانه یافت دل از زلف هرچه بافت</p></div>
<div class="m2"><p>شد مفت خوشه‌چین چو به دهقان شد آشنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بلبل از مطالعه صفحه رخت</p></div>
<div class="m2"><p>چشمم همین به خط گلستان شد آشنا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آگه ز شوق گریه بی‌اختیار نیست</p></div>
<div class="m2"><p>هرکس چو غنچه با لب خندان شد آشنا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌رحمی سرشک من افکندش از نظر</p></div>
<div class="m2"><p>هر پاره دلم که به مژگان شد آشنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنیاد عشق و حسن ز یک آب و یک گل است</p></div>
<div class="m2"><p>بنگر چگونه مصر به کنعان شد آشنا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جانم چو شمع بر سر مژگان کند سماع</p></div>
<div class="m2"><p>تا دیده‌ام به جلوه خوبان شد آشنا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیگر چو شانه هیچ نشد جمع در کفم</p></div>
<div class="m2"><p>تا پنجه‌ام به زلف پریشان شد آشنا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دیده‌ام ز گریه نگیرد دمی قرار</p></div>
<div class="m2"><p>چندان که طفل اشک به دامان شد آشنا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مهرم چو صبح بر همه‌کس آشکار شد</p></div>
<div class="m2"><p>روزی که دست من به گریبان شد آشنا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آمد غمش ز هر طرف ای عیش همتی</p></div>
<div class="m2"><p>بیگانه گو برو که فراوان شد آشنا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باشد ز باد شرطه خطر در محیط عشق</p></div>
<div class="m2"><p>امن است کشتی‌ای که به طوفان شد آشنا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عمری شدم به ناله هم‌آواز عندلیب</p></div>
<div class="m2"><p>تا نغمه‌ام به گوش گلستان شد آشنا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دردی که آن دوا نپذیرفت راحت است</p></div>
<div class="m2"><p>دردی بلا بود که به درمان شد آشنا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دیدم ز دوستان ستمی کز قیاس آن</p></div>
<div class="m2"><p>بیگانه کف به کف زد و حیران شد آشنا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قدسی به خاک پای تو مالید چشم تر</p></div>
<div class="m2"><p>لب‌تشنه‌ای به چشمه حیوان شد آشنا</p></div></div>