---
title: >-
    شمارهٔ ۳۴۹
---
# شمارهٔ ۳۴۹

<div class="b" id="bn1"><div class="m1"><p>سر تا قدم ز داغ تمنا در آتشم</p></div>
<div class="m2"><p>شاخ گلم مگر، که سراپا در آتشم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کرده‌اند نسبت آتش به خوی دوست</p></div>
<div class="m2"><p>هرجا که آتشی‌ست، من آنجا در آتشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترسم غم تو جای کند گرم در دلی</p></div>
<div class="m2"><p>ورنه چرا ز گرمی دلها در آتشم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخسار یوسف از عرق حسن درگرفت</p></div>
<div class="m2"><p>یعنی ز رشک عشق زلیخا در آتشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهم ز طبع شعله کنم جذب سوختن</p></div>
<div class="m2"><p>چون دود، ازان همیشه بود جا در آتشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم ترم چو شیشه ز بس خون گرم ریخت</p></div>
<div class="m2"><p>از موج خون چو ساغر صهبا در آتشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دورم کند ز خویشتن از ننگ، چون سپند</p></div>
<div class="m2"><p>هرچند افکنند به عمدا در آتشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر طور دل، تجلی غیرت فکنده نور</p></div>
<div class="m2"><p>از سوز رشک خواهش موسی در آتشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روی تو در برابر و دل خون ز بیم هجر</p></div>
<div class="m2"><p>موج زلال خضرم و گویا در آتشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هردم ز جای خویشتن از اضطراب دل</p></div>
<div class="m2"><p>قدسی چنان جهم، که مگر پا در آتشم</p></div></div>