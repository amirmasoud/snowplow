---
title: >-
    شمارهٔ ۲۶۱
---
# شمارهٔ ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>ز من ترسم عنان آن نرگس جادو بگرداند</p></div>
<div class="m2"><p>بگردد روی بخت از من، گر از من رو بگرداند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم را ضعف غالب شد، ز ننگ لاغری ترسم</p></div>
<div class="m2"><p>عنان از صید من عشق قوی بازو بگرداند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود گر سبحه از خاکم، مسلمان بگسلد تارش</p></div>
<div class="m2"><p>شوم گر شعله، ز آتش روی خود هندو بگرداند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه تنها بت ز من برگشته همچون روزگار من</p></div>
<div class="m2"><p>ز ننگ سجده‌ام، محراب هم ابرو بگرداند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بازار جهان، جنس وفا را کس نمی‌گیرد</p></div>
<div class="m2"><p>دلم تا کی متاع خویش را هر سو بگرداند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نسیم صبحدم هرچند باشد محرم گلشن</p></div>
<div class="m2"><p>گر از رشکم شود آگاه، از ره رو بگرداند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جست و جوی او هر لحظه صد ره چشم گریانم</p></div>
<div class="m2"><p>مرا چون قطره خون بر سر هر مو بگرداند</p></div></div>