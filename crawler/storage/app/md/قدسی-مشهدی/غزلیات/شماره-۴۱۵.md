---
title: >-
    شمارهٔ ۴۱۵
---
# شمارهٔ ۴۱۵

<div class="b" id="bn1"><div class="m1"><p>در کوه و دشت، پهن شود تا نشان من</p></div>
<div class="m2"><p>ای لاله کاش داغ تو بودی ازان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که حرف تیغ بتان شد زبانزدم</p></div>
<div class="m2"><p>شد ریشه ریشه چون قلم مو، زبان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر پهلویم چو شمع نمی‌داشت، چربیی</p></div>
<div class="m2"><p>کی سوختی به علت، مغز استخوان من؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در غیرتم که سایه چرا با تو همره است</p></div>
<div class="m2"><p>دنبال کس مباد دل بدگمان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم حسود، بر دل چاکم خورد هنوز</p></div>
<div class="m2"><p>رشکی که بر قفس نخورد آشیان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بالیده‌ام ز شوق، که مویی نمی‌زند</p></div>
<div class="m2"><p>موی میان او ز تن ناتوان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر ناله کز برای تو باشد توان شناخت</p></div>
<div class="m2"><p>یا رب مباد گوش کسی بر فغان من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سررشته محبت اگر آیدم به دست</p></div>
<div class="m2"><p>سوزد چو شمع بر سر آن رشته، جان من</p></div></div>