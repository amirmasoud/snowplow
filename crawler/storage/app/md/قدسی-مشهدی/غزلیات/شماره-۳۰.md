---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>می‌زند نشتر تدبیر شب و روز مرا</p></div>
<div class="m2"><p>مصلحت چیست به این مصلحت آموز مرا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست حق نمکی بر منش از دیده شور</p></div>
<div class="m2"><p>آنکه چشم بدش افکند به این روز مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عید نوروز من آنست که پیشم باشی</p></div>
<div class="m2"><p>چون نباشی تو چه عیدست و چه نوروز مرا؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طبعم افسرده شد از فکر حریفی خواهم</p></div>
<div class="m2"><p>که کند گرم به یک بیت گلوسوز مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌برد هر نفسم بر سر راهی چو صبا</p></div>
<div class="m2"><p>بوالهوس کرده نگاه هوس‌اندوز مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرده انگشت‌نما داغ جنونم قدسی</p></div>
<div class="m2"><p>چه کند بهتر ازین کوکب فیروز مرا</p></div></div>