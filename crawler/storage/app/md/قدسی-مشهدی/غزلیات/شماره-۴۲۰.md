---
title: >-
    شمارهٔ ۴۲۰
---
# شمارهٔ ۴۲۰

<div class="b" id="bn1"><div class="m1"><p>من نمی‌گویم به چشمم نه قدم، یا بر زمین</p></div>
<div class="m2"><p>چشم من فرش است هرجا می‌نهی پا بر زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشتی چشم تر من بود با دریا قدر</p></div>
<div class="m2"><p>اشک زور آورد، آمد پشت دریا بر زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیر پای عاشقان باشد زمین و آسمان</p></div>
<div class="m2"><p>عشق را یک پای بر عرش است و یک پا بر زمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جای برگردون بود افتادگان عشق را</p></div>
<div class="m2"><p>من هم از افتادگان عشقم، اما بر زمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمرها شد کشتی من با نکویان آشناست</p></div>
<div class="m2"><p>دل ز دست هر یکی افتاده صد جا بر زمین</p></div></div>