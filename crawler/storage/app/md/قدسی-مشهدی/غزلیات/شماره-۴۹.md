---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>گشته چون آینه روشن دل بی‌کینه ما</p></div>
<div class="m2"><p>تا فتد عکس جمال تو در آیینه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمزه‌ات ناوک بیداد نیاورده به زه</p></div>
<div class="m2"><p>دل به خون گشته ز مژگان تو در سینه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرمی سلسه عشق ز داغ دل ماست</p></div>
<div class="m2"><p>گوهر درد برد عشق ز گنجینه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق پیوسته به تعلیم جنون مشغول است</p></div>
<div class="m2"><p>رسم آزادشدن نیست در آدینه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش بودیم ز وصل تو چو قدسی محروم</p></div>
<div class="m2"><p>هست تا روز جزا حسرت دوشینه ما</p></div></div>