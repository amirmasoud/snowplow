---
title: >-
    شمارهٔ ۲۹۱
---
# شمارهٔ ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>در کوی تو فردوس تمنی نکند کس</p></div>
<div class="m2"><p>با نور رخت، یاد تجلی نکند کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجا رسم، اظهار کنم بی‌کسی خویش</p></div>
<div class="m2"><p>کز کشتنم اندیشه دعوی نکند کس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی دولت دیدار تو آرام محال است</p></div>
<div class="m2"><p>گر دل به خیال تو تسلی نکند کس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صحت بر ما خسته‌دلان راه ندارد</p></div>
<div class="m2"><p>اینجا هوس شربت عیسی نکند کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظاره غم از دل بی‌درد چه جویی</p></div>
<div class="m2"><p>بینش طمع از دیده اعمی نکند کس</p></div></div>