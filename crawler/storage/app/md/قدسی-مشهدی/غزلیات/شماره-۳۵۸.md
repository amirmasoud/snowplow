---
title: >-
    شمارهٔ ۳۵۸
---
# شمارهٔ ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>به آشنایی چشم تو ناتوان شده‌ام</p></div>
<div class="m2"><p>چو مو ضعیف ز سودای آن میان شده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلیده در خم زلف تو ناخنش به دلم</p></div>
<div class="m2"><p>منه ز دست، که با شانه همزبان شده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز چاک سینه نفس بایدم کشید چو صبح</p></div>
<div class="m2"><p>ببین ز هجر تو امشب چه ناتوان شده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هما دوباره به من سر فرو نمی‌آورد</p></div>
<div class="m2"><p>ازان چو شمع سراپا یک استخوان شده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هوای ابرم اگر شاد می‌کند، چه عجب</p></div>
<div class="m2"><p>به آفتاب ز مهر تو بدگمان شده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو تیغ زن، که من از شکوه لب نمی‌بندم</p></div>
<div class="m2"><p>چو خامه گر همه تن در سر زبان شده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همان لبم کند اظهار بی‌وفایی گل</p></div>
<div class="m2"><p>چو غنچه گر همه دل عقده زبان شده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منم ز قدرشناسان گل که مدت‌هاست</p></div>
<div class="m2"><p>به باغ رفته به گلگشت و باغبان شده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز حال خویش فراموش کرده‌ام قدسی</p></div>
<div class="m2"><p>دمی به مرغ چمن گر هم‌آشیان شده‌ام</p></div></div>