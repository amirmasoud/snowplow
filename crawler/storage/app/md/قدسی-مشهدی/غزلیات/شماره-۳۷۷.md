---
title: >-
    شمارهٔ ۳۷۷
---
# شمارهٔ ۳۷۷

<div class="b" id="bn1"><div class="m1"><p>کرده تا عشق تو چون نقش قدم پامالم</p></div>
<div class="m2"><p>هیچ‌کس نیست که حسرت نخورد بر حالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بیابان بلا، گو مدد خضر مباش</p></div>
<div class="m2"><p>غم به هرسو که روم، می‌رود از دنبالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم، مشتاق و حیا قفل زبان می‌گردد</p></div>
<div class="m2"><p>صد سخن در دل و پیش تو ز حسرت لالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواری عشق چنانم ز نظرها افکند</p></div>
<div class="m2"><p>که در آیینه نیاید به نظر، تمثالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نهالی که موافق فتدش آب و هوا</p></div>
<div class="m2"><p>هست در عشق به از سال دگر، هر سالم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من مجنون چو به صحرا روم از شهر، آیند</p></div>
<div class="m2"><p>آهوان تا در دروازه به استقبالم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمع را رقص نماید تپش پروانه</p></div>
<div class="m2"><p>آشنا کاش ز بیگانه بپرسد حالم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قید جاوید، مرا قوت پرواز دهد</p></div>
<div class="m2"><p>ترسم افتم ز هوا، گر بگشایی بالم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نکند همتم اقبال سوی بخت بلند</p></div>
<div class="m2"><p>ورنه پستی نبود قاعده اقبالم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه چو جم جام شناسم، نه چو خضر آب حیات</p></div>
<div class="m2"><p>کرده خونابه‌کشی از همه فارغ‌بالم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قدسی از ناله ماتم‌زدگان یابم فیض</p></div>
<div class="m2"><p>هرگز از جا نبرد زمزمه اقبالم</p></div></div>