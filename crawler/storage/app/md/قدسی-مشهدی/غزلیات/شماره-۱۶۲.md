---
title: >-
    شمارهٔ ۱۶۲
---
# شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>در جلوه‌گری چون تو کسی یاد ندارد</p></div>
<div class="m2"><p>نادر بود آن شیوه که استاد ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی سعی تو گیراست خیال سر زلفت</p></div>
<div class="m2"><p>این دام روان حاجت صیاد ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر عضو مرا طاقت صد داغ دگر هست</p></div>
<div class="m2"><p>با غمزه بگو دست ز بیداد ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل گشته تسلی به همینم که محبت</p></div>
<div class="m2"><p>شرط است که تا داردم، آزاد ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چشمه حیوان مطلب زندگی خضر</p></div>
<div class="m2"><p>کاین فیض به جز خنجر جلاد ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد رخنه چو گل در دلم انداخته تیغش</p></div>
<div class="m2"><p>کس بهتر ازین خانه آباد ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیوار غم از گریه کی از پای درآید</p></div>
<div class="m2"><p>کاشانه صبرست که بنیاد ندارد</p></div></div>