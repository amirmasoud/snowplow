---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>چیزی نشد معلوم من از صحبت فرزانه‌ها</p></div>
<div class="m2"><p>بر قلب رسوایی زدم زین پس من و دیوانه‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بیم سیل اشک من نیک و بد روی زمین</p></div>
<div class="m2"><p>تا مردمان چشم خود بیرون شدند از خانه‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خود تهیدستم چه شد دستی ندارم بر فلک</p></div>
<div class="m2"><p>چشک و دل من پر بود گنج است در ویرانه‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از گفتگوی این و آن تا کی فرورفتن توان</p></div>
<div class="m2"><p>مردم ز غفلت تا به کی خواب آرد این افسانه‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ساقی روشندلان بازآ که اهل بزم را</p></div>
<div class="m2"><p>گردید قالب‌ها تهی پر شد ز خون پیمانه‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغان این بستان‌سرا رام و اسیرند از ازل</p></div>
<div class="m2"><p>صیاد گو منت مکش از دام‌ها و دانه‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناخن به دل‌ها می‌زنند از طره‌های مهوشان</p></div>
<div class="m2"><p>شاید اگر صاحب‌دلان ممنون شوند از شانه‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر گرد شمع عارضت ای قبله روحانیان</p></div>
<div class="m2"><p>خیل ملک پر می‌زنند از شوق چون پروانه‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قدسی ز بهر دوستی، هرکس تردد می‌کند</p></div>
<div class="m2"><p>من هم پی پروانه‌ای گردم در این کاشانه‌ها</p></div></div>