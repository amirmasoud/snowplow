---
title: >-
    شمارهٔ ۳۷۶
---
# شمارهٔ ۳۷۶

<div class="b" id="bn1"><div class="m1"><p>با یارم و در دفع غم اسباب ندارم</p></div>
<div class="m2"><p>در بحر چو کشتی روم و آب ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز سجده ابروی توام نیست عبادت</p></div>
<div class="m2"><p>پروای نماز و سر محراب ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانسته لبم لذت خونابه کشیدن</p></div>
<div class="m2"><p>معذورم اگر ذوق می ناب ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با دولت بیدار، نسازد غم جاوید</p></div>
<div class="m2"><p>آزردگی از بخت گرانخواب ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سودای دلم جوش برآرد ز نصیحت</p></div>
<div class="m2"><p>قدسی سر دلسوزی احباب ندارم</p></div></div>