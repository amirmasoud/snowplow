---
title: >-
    شمارهٔ ۳۱۷
---
# شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>روشن شود ز دود دماغم چراغ فیض</p></div>
<div class="m2"><p>فیض است آنقدر، که ندارم دماغ فیض</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک شاخ گل ز گل نشود پاک، گردوکون</p></div>
<div class="m2"><p>تا حشر گل برند به خرمن ز باغ فیض</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هر طرف دریچه فیضی‌ست بر دلم</p></div>
<div class="m2"><p>بیهوده از در که کنم من سراغ فیض؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر مرکّب قلم فیض بخش من</p></div>
<div class="m2"><p>آورده‌اند دوده ز دود چراغ فیض</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی نموده نذر حریفان به بزم نظم</p></div>
<div class="m2"><p>روز ازل که ریخته می در ایاغ فیض</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای آنکه برده ذوق سماعت ز خویشتن</p></div>
<div class="m2"><p>ترسم که آستین بزنی بر چراغ فیض</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سنگ کاهلی، در اندیشه را مبند</p></div>
<div class="m2"><p>قدسی دگر مسوز دلم را به داغ فیض</p></div></div>