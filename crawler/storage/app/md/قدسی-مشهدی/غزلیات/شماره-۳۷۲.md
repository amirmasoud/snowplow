---
title: >-
    شمارهٔ ۳۷۲
---
# شمارهٔ ۳۷۲

<div class="b" id="bn1"><div class="m1"><p>اگر دور از دلارای خود افتم</p></div>
<div class="m2"><p>به دست طبع خودرای خود افتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سودای دو عالم باز مانم</p></div>
<div class="m2"><p>زمانی گر به سودای خود افتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبم گرم است جا بر آستانت</p></div>
<div class="m2"><p>مباد آن روز، کز جای خود افتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگیرد دامنم گر خاک کویت</p></div>
<div class="m2"><p>شوم زنجیر و در پای خود افتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غریبان را دهم در دیده ماوا</p></div>
<div class="m2"><p>ز غربت گر به ماوای خود افتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تمنای فلک آن است قدسی</p></div>
<div class="m2"><p>که من دور از تمنای خود افتم</p></div></div>