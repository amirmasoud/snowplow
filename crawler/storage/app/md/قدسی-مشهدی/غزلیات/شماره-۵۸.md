---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>چند باشد دل ز وصل دل‌ربایی بی‌نصیب</p></div>
<div class="m2"><p>چند باشد گوشم از آواز پایی بی‌نصیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخ مپوش از من گهِ نظّاره این عیب است عیب</p></div>
<div class="m2"><p>کز سر خوان کریم آید گدایی بی‌نصیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند آیم بر سر راه و ز بیم خوی تو</p></div>
<div class="m2"><p>چشم از نظّاره و لب از دعایی بی‌نصیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت رفتن جسم قدسی را مسوز ای آه گرم</p></div>
<div class="m2"><p>تا نگردد ز استخوان او همایی بی‌نصیب</p></div></div>