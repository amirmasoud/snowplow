---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>تهی ز می نتوان یافتن ایاغ مرا</p></div>
<div class="m2"><p>به آفتاب نسب می‌رسد چراغ مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم تو گر نکشد دامنم ازین کشور</p></div>
<div class="m2"><p>چنان روم که نیابی دگر سراغ مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ناز ناخن اهل ملامتم چه نیاز</p></div>
<div class="m2"><p>چو گر مخونی غم تازه کرد داغ مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو غنچه چند زیم تنگدل ز خاطر جمع؟</p></div>
<div class="m2"><p>نسیم کو که پریشان کند دماغ مرا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم ز باد خزان تازه می‌شود قدسی</p></div>
<div class="m2"><p>چه احتیاج نسیم بهار باغ مرا؟</p></div></div>