---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>نشاط ما اسیران از دل اندوهگین باشد</p></div>
<div class="m2"><p>نمی‌بندیم لب از خنده، تا خاطر غمین باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خون چون خودی آن غمزه را آلوده نپسندم</p></div>
<div class="m2"><p>به قاصد جان دهم، گر مژده قتلم یقین باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرست از گریه پنهان دلم، کو دامن صحرا؟</p></div>
<div class="m2"><p>مرا تا چند سامان جگر در آستین باشد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم را گرچه خون کردی، خدنگت را نشان گشتم</p></div>
<div class="m2"><p>که پیکانش درون سینه دل را جانشین باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه حاصل زین که دامن از اسیران در نمی‌چینی</p></div>
<div class="m2"><p>اسیری را که بند دست، چین آستین باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به صد حسرت چو میرم بر سر راهش، مشوییدم</p></div>
<div class="m2"><p>که گرد انتظارم تا قیامت بر جبین باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدارا گر کند با خصم کلکم، گو مشو ایمن</p></div>
<div class="m2"><p>زبان شمع اگر چرب است، اما آتشین باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مکش گو آسمان زحمت پی بهبود احوالم</p></div>
<div class="m2"><p>چه سود از تربیت آن را که بخت بد قرین باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به عشق از ناسپاسی‌های دل بر خویش می‌لرزم</p></div>
<div class="m2"><p>که گر چون غنچه خون گردد، همان اندوهگین باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به فکر عافیت اوقات خود ضایع مکن قدسی</p></div>
<div class="m2"><p>چو صیادی که بهر صید لاغر در کمین باشد</p></div></div>