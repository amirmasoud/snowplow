---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>رغیرتم پوشیده از چشم بدان، خوب مرا</p></div>
<div class="m2"><p>داده جا در پرده دل طفل محجوب مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدعی بر خویش می‌پیچد چو مکتوب از حسد</p></div>
<div class="m2"><p>ناگرفت از دست قاصد یار مکتوب مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عکس رویش را زچشم آیینه دل جذب کرد</p></div>
<div class="m2"><p>دل به دست دیده هم نگذاشت مطلوب مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاید از آشفتگی‌های دلم یادش دهد</p></div>
<div class="m2"><p>ای صبا آشفته‌تر کن زلف محبوب مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز دل دیوانه با من هیچ‌کس همدم نبود</p></div>
<div class="m2"><p>مانده‌ام تنها کجا بردید محبوب مرا؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل به جان آمد ز غم خواهد شد افغانش بلند</p></div>
<div class="m2"><p>بیش ازین تاب صبوری نیست ایوب مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی گشاید دور از آن رخ، دل نظر بر هیچ‌کس</p></div>
<div class="m2"><p>بسته عشق از غیر یوسف، دیده یعقوب مرا</p></div></div>