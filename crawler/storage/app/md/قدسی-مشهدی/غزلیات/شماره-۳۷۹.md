---
title: >-
    شمارهٔ ۳۷۹
---
# شمارهٔ ۳۷۹

<div class="b" id="bn1"><div class="m1"><p>خون می‌چکد از دیده ز نظاره داغم</p></div>
<div class="m2"><p>تا خون نشود، وا نشود غنچه باغم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیدا نبود دل ز هجوم غم معشوق</p></div>
<div class="m2"><p>پنهان شده از کثرت پروانه، چراغم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بر لبم انگشت زنی، جوش زند خون</p></div>
<div class="m2"><p>کان غمزه ز خون کرد لبالب چو ایاغم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون زنده کند صور سرافیل دلم را؟</p></div>
<div class="m2"><p>گر بوی تو در حشر ندارد به دماغم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون سبزه، ز گل تا به ابد خضر بروید</p></div>
<div class="m2"><p>هرجا که نهد پا غم عشقت به سراغم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الماس چو تبخاله برآید ز لب مست</p></div>
<div class="m2"><p>گر بر دهن شیشه نهی پنبه داغم</p></div></div>