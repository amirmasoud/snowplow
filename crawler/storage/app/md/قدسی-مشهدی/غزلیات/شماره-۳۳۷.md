---
title: >-
    شمارهٔ ۳۳۷
---
# شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>گر شرم وصالت نبود قفل زبانم</p></div>
<div class="m2"><p>گویم که فراق تو چها کرد به جانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هنگام شکایت ز تو، از بس که گزیدم</p></div>
<div class="m2"><p>چون بار صنوبر شده صد پاره زبانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لرزد چو جرس بر سر هر ناله مرا دل</p></div>
<div class="m2"><p>گویا که به غمهای تو پیوسته فغانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بر ورق دل نهم انگشت، ز گرمی</p></div>
<div class="m2"><p>چون خامه مو دود برآید ز بنانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امروز نیم رانده ز بزم تو چو قدسی</p></div>
<div class="m2"><p>عمری‌ست که از دور به حسرت نگرانم</p></div></div>