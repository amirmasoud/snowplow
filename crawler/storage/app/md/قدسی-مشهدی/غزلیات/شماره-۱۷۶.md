---
title: >-
    شمارهٔ ۱۷۶
---
# شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>مرا چو کار بدان زلف تابدار افتاد</p></div>
<div class="m2"><p>نماند تاب دل و عقده‌ام به کار افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز من چو غنچه نپوشی جمال اگر دانی</p></div>
<div class="m2"><p>به دل ز دیدن رویت چه خارخار افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غلام بخت سیاهم چرا که می‌دانم</p></div>
<div class="m2"><p>ز نسبتش سر و کارم به زلف یار افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا چو آینه شاید به دست خود گیرد</p></div>
<div class="m2"><p>خوشم که دیده‌ چو آیینه‌ام ز کار افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جدا ز روی تو داد گریستن دادم</p></div>
<div class="m2"><p>ز گریه چشم مرا دجله در کنار افتاد</p></div></div>