---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>سخن ز غیر مپرسید بینوایی را</p></div>
<div class="m2"><p>که کرده ورد زبان حرف آشنایی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حدیث هجر به گوش دلم چنان تلخ است</p></div>
<div class="m2"><p>که حرف موج بگویند ناخدایی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دماغ غنچه معطر شد از نسیم سحر</p></div>
<div class="m2"><p>کشیده شانه مگر زلف مشک‌سایی را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز رشک هر مژه در چشم من شود خاری</p></div>
<div class="m2"><p>به کوی دوست چو بینم برهنه پایی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چراغ حسن تو را روشنی نگردد کم</p></div>
<div class="m2"><p>اگر بهشت کنی کلبه گدایی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو سوی دیر روی، سبحه را بنه قدسی</p></div>
<div class="m2"><p>مبر به مجلس دردی‌کشان ریایی را</p></div></div>