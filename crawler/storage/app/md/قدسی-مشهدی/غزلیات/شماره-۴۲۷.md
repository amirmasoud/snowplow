---
title: >-
    شمارهٔ ۴۲۷
---
# شمارهٔ ۴۲۷

<div class="b" id="bn1"><div class="m1"><p>خوی تو در جفا شکسته</p></div>
<div class="m2"><p>عهدت کمر وفا شکسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیگانگی تو خار حسرت</p></div>
<div class="m2"><p>در جان صد آشنا شکسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا از جگر که یادگارست؟</p></div>
<div class="m2"><p>خاری که مرا به پا شکسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن کس که دلم شکسته، داند</p></div>
<div class="m2"><p>کاین شیشه به مدعا شکسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر هرکه کشید تیغ، از رشک</p></div>
<div class="m2"><p>رنگ من مبتلا شکسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیرت کش ساغرم، به سنگی</p></div>
<div class="m2"><p>صد جام جهان‌نما شکسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا رب زن ناله‌ام، به آهی</p></div>
<div class="m2"><p>هنگامه صد دعا شکسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بتکده دلم شد آباد</p></div>
<div class="m2"><p>بازار کلیسا شکسته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا رب که شکستگی مبیناد</p></div>
<div class="m2"><p>آن کس که دل مرا شکسته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکس که بدید رنگ قدسی</p></div>
<div class="m2"><p>داند که دلش کجا شکسته</p></div></div>