---
title: >-
    شمارهٔ ۴۳۴
---
# شمارهٔ ۴۳۴

<div class="b" id="bn1"><div class="m1"><p>به نومیدی خوشم، ناکامی‌ام کام است پنداری</p></div>
<div class="m2"><p>دلم را بی سرانجامی سرانجام است پنداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شراب ناامیدی خوش گوارا شد مزاجم را</p></div>
<div class="m2"><p>حریفان را می وصل تو در جام است پنداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان روز و شب، بی دوستان فرقی نمی‌بینم</p></div>
<div class="m2"><p>به چشمم اول صبح آخر شام است پنداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گوشم امشب آواز جرس نزدیک می‌آید</p></div>
<div class="m2"><p>ز من تا محمل مقصود، یک گام است پنداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیال وصل بستن، بهتر از وصلش کند شادم</p></div>
<div class="m2"><p>نشاط نشاه‌ام در خنده جام است پنداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز اهل خانقه قدسی بسی شید و ریا دیدم</p></div>
<div class="m2"><p>به چشمم حلقه توحیدشان دام است پنداری</p></div></div>