---
title: >-
    شمارهٔ ۲۰۰
---
# شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>میرم ار خوی ستمکاری ز سر بیرون کند</p></div>
<div class="m2"><p>شمع را گر تن نکاهد زندگانی چون کند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دایه را پستان به ناخن می‌تراشد طفل عشق</p></div>
<div class="m2"><p>تا دمی شیرش مبادا در گلو بی خون کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه می‌خواهد غمی بردارد از روی دلم</p></div>
<div class="m2"><p>کاش دل را از شکاف سینه‌ام بیرون کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل که نتواند رفو زد چاک جیب خویش را</p></div>
<div class="m2"><p>چاره چاک دل مرغ گلستان چون کند؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز لیلی بر سر بالین مجنون می‌رود</p></div>
<div class="m2"><p>چند عاشق شکوه از بی‌مهری گردون کند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش ما و بخت، قدسی چون بد افتاد از ازل</p></div>
<div class="m2"><p>هرچه در دل نقش بندم، بخت دیگرگون کند</p></div></div>