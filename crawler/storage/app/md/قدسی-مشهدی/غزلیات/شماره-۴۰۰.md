---
title: >-
    شمارهٔ ۴۰۰
---
# شمارهٔ ۴۰۰

<div class="b" id="bn1"><div class="m1"><p>دوش بر خاک درت عرض جبین می‌کردم</p></div>
<div class="m2"><p>وز جبین درخور آن، سجده گزین می‌کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خیال تو به آیینه گمان می‌بردم</p></div>
<div class="m2"><p>در دل آینه، چون عکس، کمین می‌کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من چه دانم که مثنا نشود وصل تو، کاش</p></div>
<div class="m2"><p>روز اول، نگه بازپسین می‌کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تهمت خرمی از هر دو جهان برمی‌خاست</p></div>
<div class="m2"><p>گر به اندازه غم، ناله حزین می‌کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نسیم سحری همره خویشم می‌برد</p></div>
<div class="m2"><p>از سر زلف بتان، غارت چین می‌کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق می‌گویم و رخساره به خون می‌شویم</p></div>
<div class="m2"><p>عمرها خدمت دل بهر همین می‌کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگرفتم ز لب نوش تو بوسی، ای کاش</p></div>
<div class="m2"><p>تازه، کامی ز می عشق چنین می‌کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فال حسرت زدم و گشت اجابت شب دوش</p></div>
<div class="m2"><p>کاش امروز دعایی به ازین می‌کردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا نگاهی نکند سوی تو پنهان از من</p></div>
<div class="m2"><p>در پی دیده چو دل، دوش کمین می‌کردم</p></div></div>