---
title: >-
    شمارهٔ ۲۸۳
---
# شمارهٔ ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>ای دست تو به کینه ز دوران درازتر</p></div>
<div class="m2"><p>چشمت ز حادثات جهان فتنه‌سازتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان که آن صنم گره از زلف باز کرد</p></div>
<div class="m2"><p>در وصف خویش کرد زبانم درازتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاید که دود از دل گردون برآورد</p></div>
<div class="m2"><p>کو ناله‌ای ز ناله من جهان‌گدازتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناز تو می‌کشد به نیازم، وگرنه نیست</p></div>
<div class="m2"><p>آزاده‌ای ز من به جهان بی‌نیازتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شام فراق اگرچه مرا دیده باز بود</p></div>
<div class="m2"><p>صبح وصال بودم ازان دیده بازتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون عشق، خواند گرچه مرا خانه زاد، حسن</p></div>
<div class="m2"><p>پرورده است لیک ز خویشم بنازتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قدسی به گرد مرکز انصاف گشته‌ام</p></div>
<div class="m2"><p>از خال او ندیده دلم، دلنوازتر</p></div></div>