---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>نیست باکی گر به دستم غنچه سیراب نیست</p></div>
<div class="m2"><p>در دل من غنچه پیکان او نایاب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلوه صبح است شامم را به یاد روی دوست</p></div>
<div class="m2"><p>آسمان را بر شب من منت مهتاب نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوق دیدار تو چندان لذت از یک دیدنت</p></div>
<div class="m2"><p>بر سر هم ریخت در چشمم که جای خواب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون گری قدسی که دارد گریه خونین اثر</p></div>
<div class="m2"><p>پاره دل را چه شد در دیده گر خوناب نیست</p></div></div>