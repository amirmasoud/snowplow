---
title: >-
    شمارهٔ ۳۶۹
---
# شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>خضر اگر آب حیات آورد، خون دانسته‌ام</p></div>
<div class="m2"><p>هرچه پیش آمد، ز بخت واژگون دانسته‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز از روزم بتر شد، شوق بر شوقم فزود</p></div>
<div class="m2"><p>هرچه ناصح خواند در گوشم، فسون دانسته‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دید اندک گرمی‌ای از غیر، از من پا کشید</p></div>
<div class="m2"><p>دوست از دشمن نمی‌داند، کنون دانسته‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا دماغم را سر زلفت پریشان کرده‌است</p></div>
<div class="m2"><p>عقل اگر کرده‌ست تدبیری، جنون دانسته‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دل من بهر دلهای دگر غم می‌بری</p></div>
<div class="m2"><p>از دلم این غم نخواهد شد برون، دانسته‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با غم دنیا، غم دوری ندارد نسبتی</p></div>
<div class="m2"><p>می‌کند قدسی مرا این غم زبون، دانسته‌ام</p></div></div>