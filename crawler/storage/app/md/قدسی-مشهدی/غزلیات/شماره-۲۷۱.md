---
title: >-
    شمارهٔ ۲۷۱
---
# شمارهٔ ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>با من، غمت ز مهر، دویی در میان ندید</p></div>
<div class="m2"><p>کس شعله را به خار چنین مهربان ندید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد چمن ز خاطر مرغ اسیر رفت</p></div>
<div class="m2"><p>چون دلنشینی قفس از آشیان ندید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بود، روزگار به افسردگی گذاشت</p></div>
<div class="m2"><p>کس رنگ خنده بر لب این بوستان ندید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردید چون خیال و ز دلها خبر گرفت</p></div>
<div class="m2"><p>تیر ترا دمی که دلم در کمان ندید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی از دلم ز دعوی پیکان کشید دست؟</p></div>
<div class="m2"><p>تا سینه پای تیر ترا در میان ندید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گو دیده مرا پی ابروی او ببین</p></div>
<div class="m2"><p>آن‌کس که بحر را پی کشتی روان ندید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک ره ز چهره پرده برافکن خدای را</p></div>
<div class="m2"><p>آیینه کس همیشه در آیینه‌دان ندید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفتی به باغ و زنده جاوید شد چمن</p></div>
<div class="m2"><p>دید از تو باغ، آنچه ز آب روان ندید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرگز نخورد داغ دلم آب ناخنی</p></div>
<div class="m2"><p>آن گلبنم که تربیت باغبان ندید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می‌دید کاش گونه زردم در آینه</p></div>
<div class="m2"><p>آن‌کس که روی آینه را زرنشان ندید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کام دلم ز سیر کواکب روا نشد</p></div>
<div class="m2"><p>لب تشنه فیض آب ز ریگ روان ندید</p></div></div>