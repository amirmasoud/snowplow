---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>مرا چو لاله ز بخت سیه رهایی نیست</p></div>
<div class="m2"><p>شب مرا به دم صبح آشنایی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو نقش زلف تو بندم چرا نریزم اشک</p></div>
<div class="m2"><p>که می‌رسد شب و در خانه روشنایی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز من برای چه رنجیده باز بر سر هیچ</p></div>
<div class="m2"><p>بهانه‌جوی مرا گر سر جدایی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خون دیده مشو دامن مرا زاهد</p></div>
<div class="m2"><p>که قید عشق بتان قید پارسایی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بقا کمند تو دارد ازآن حسد بردم</p></div>
<div class="m2"><p>بر آن اسیر که در طالعش رهایی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ره گذار هوس بسته‌اند بر چمنم</p></div>
<div class="m2"><p>گل بهار مرا رنگ بی‌وفایی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین دیار ندیدیم جز دل قدسی</p></div>
<div class="m2"><p>شکسته‌ای که نیازش به مومیایی نیست</p></div></div>