---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>شمع وصلت هرکه را شب خانه روشن می‌کند</p></div>
<div class="m2"><p>روزنش در خانه، کار چشم دشمن می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تازه شد داغ کهن بر دستم از بس سوده شد</p></div>
<div class="m2"><p>آستین بر آتش من کار دامن می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاش در میخانه هم خالی کند پیمانه‌ای</p></div>
<div class="m2"><p>آنکه قندیل حرم را پر ز روغن می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد اگر بر سایه دیوار گلشن می‌وزد</p></div>
<div class="m2"><p>بلبل از کنج قفس بنیاد شیون می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌کند خار گل ناچیده از دستم برون</p></div>
<div class="m2"><p>تنگ چشمی بین که با من چشم سوزن می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خانه‌ام می‌سوزد و همسایه‌ام آگاه نیست</p></div>
<div class="m2"><p>ای خوش آن آتش که دودش میل روزن می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حرف صلح کل زند قدسی عجب دیوانه‌ایست</p></div>
<div class="m2"><p>عالمی را بی‌سبب با خویش دشمن می‌کند</p></div></div>