---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>بر جرعه‌نوش عشق، بجز خون حلال نیست</p></div>
<div class="m2"><p>زان رو به دل ز خوردن خونم ملال نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون مرا بریز که در شرع دوستی</p></div>
<div class="m2"><p>خون‌ریختن شهید وفا را وبال نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار دل است پر مزن ای طایر حرم</p></div>
<div class="m2"><p>پرواز بوستان محبت به بال نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل‌دوختن به وعده معشوق بی‌وفا</p></div>
<div class="m2"><p>جز آرزوی خام و خیال محال نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رضوان که می‌ستود گلستان خویش را</p></div>
<div class="m2"><p>انصاف داد خود که چو بزم وصال نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در باغ تا ز داغ جگر پنبه کنده‌ای</p></div>
<div class="m2"><p>قدسی چه گل که در عرق انفعال نیست</p></div></div>