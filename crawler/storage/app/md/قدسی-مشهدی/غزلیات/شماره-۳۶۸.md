---
title: >-
    شمارهٔ ۳۶۸
---
# شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>امشب ز دیده از قدح افزون گریستم</p></div>
<div class="m2"><p>تا دل چو شیشه داشت نمی، خون گریستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک بار، دیده‌ام به غلط فال خواب زد</p></div>
<div class="m2"><p>عمری ز شرمساری آن، خون گریستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تلخی ندید عیش حریفان ز گریه‌ام</p></div>
<div class="m2"><p>چون آمدم ز میکده بیرون، گریستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کس به عشق او نبرد پی ز گریه‌ام</p></div>
<div class="m2"><p>شبها به شهر و روز به هامون گریستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روید به جای سبزه ازان خاک، نخل سرو</p></div>
<div class="m2"><p>هرجا به یاد قد آن موزون گریستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طوافن پناه برد به گیتی ز گریه‌ام</p></div>
<div class="m2"><p>ای نوح سر بر آر، ببین چون گریستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اول شدم شکفته ز ارسال نامه‌اش</p></div>
<div class="m2"><p>آخر ز شرمساری مضمون گریستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویند دل به گریه تهی می‌شود ز درد</p></div>
<div class="m2"><p>چون درد من فزود، چو افزون گریستم؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکس که دید اشک من، اندوهگین شود</p></div>
<div class="m2"><p>قدسی ز بس که با دل محزون گریستم</p></div></div>