---
title: >-
    شمارهٔ ۲۵۲
---
# شمارهٔ ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>مرا هر قطره‌ای کز دیده در دامن فرو ریزد</p></div>
<div class="m2"><p>شود چشمی و خون بر حال چشم من فرو ریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز مهر عارض مهتاب سیمایت عجب نبود</p></div>
<div class="m2"><p>اگر پیراهن من چون کتان از تن فرو ریزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یاد عارضت چون گریه‌ام بر دیده زور آرد</p></div>
<div class="m2"><p>به جای آب، مهر و ماه در دامن فرو ریزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز تاب عارض خورشیدرویان، مردم چشمم</p></div>
<div class="m2"><p>به جای اشک خونین، اخگر از دامن فرو ریزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهارست و به طرف بوستان از تاب دل قدسی</p></div>
<div class="m2"><p>برآری گر نفس، برگ گل و سوسن فرو ریزد</p></div></div>