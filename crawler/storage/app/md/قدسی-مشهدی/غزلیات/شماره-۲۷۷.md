---
title: >-
    شمارهٔ ۲۷۷
---
# شمارهٔ ۲۷۷

<div class="b" id="bn1"><div class="m1"><p>جز محبت، سینه‌ام علم دگر پیدا نکرد</p></div>
<div class="m2"><p>چون صدف، کس انتخاب قطره از دریا نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق و معشوق را شرط است با هم سوختن</p></div>
<div class="m2"><p>شمع درنگرفت تا پروانه‌ای پیدا نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کلبه تنگ مرا جای دو خون‌آلوده نیست</p></div>
<div class="m2"><p>تا نرفت از جا دلم، در سینه پیکان جا نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردم از بی‌طاقتی بسیار بر گرد چمن</p></div>
<div class="m2"><p>شمع را در بزم، جز پروانه کس رسوا نکرد</p></div></div>