---
title: >-
    شمارهٔ ۲۳۸
---
# شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>عالمی بر خویش بالیدم چو از من یاد کرد</p></div>
<div class="m2"><p>بنده‌ام تا کرد، گویی بنده‌ای آزاد کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صید ما را احتیاج زحمت صیاد نیست</p></div>
<div class="m2"><p>خون گرم از دل روان شد چون ز تیغش یاد کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسم معموری همین در کوچه سیل است و بس</p></div>
<div class="m2"><p>عاقبت اشکم به کام این شهر را آباد کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حرف مرهم در میان آورد با زخمم طبیب</p></div>
<div class="m2"><p>نوک مژگان را خیال دشنه فولاد کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدعی را بهره‌ای چون از هنرمندی نبود</p></div>
<div class="m2"><p>حرف عیب دیگران را جزو استعداد کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر بیدادگر، بیداد آید عاقبت</p></div>
<div class="m2"><p>تیشه کی با بیستون کرد آنچه با فرهاد کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوی مجنون، گر نه امشب ناقه ره گم کرده بود</p></div>
<div class="m2"><p>محمل لیلی چرا بیش از جرس فریاد کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناخنی از شانه در زلف تو بر داغش نخورد</p></div>
<div class="m2"><p>دل به این امید، عمری تکیه بر شمشاد کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قدسی آن خشتی که من زادم ز مادر بر سرش</p></div>
<div class="m2"><p>عشق آن را برد هرجا، خانه‌ای بنیاد کرد</p></div></div>