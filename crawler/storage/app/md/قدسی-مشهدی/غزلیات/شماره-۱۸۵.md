---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>تا عشق مرا بر سر بازار نیاورد</p></div>
<div class="m2"><p>حسن از نگه گرم، خریدار نیاورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پهلو به صبا می‌زند این حرف که گویم</p></div>
<div class="m2"><p>با شانه، که از زلف تو یک تار نیاورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خواب، سر زلف تو بسیار گرفتم</p></div>
<div class="m2"><p>جز خواب پریشان ثمری بار نیاورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغم که چرا جاذبه ناله بلبل</p></div>
<div class="m2"><p>گل را به چمن از سر بازار نیاورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نرگس جادوگر او تا به مسیحا</p></div>
<div class="m2"><p>صدبار خبر برد که یک‌بار نیاورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قدسی نکنی شکوه ز سودای محبت</p></div>
<div class="m2"><p>تسبیح که برد از تو که زنار نیارود؟</p></div></div>