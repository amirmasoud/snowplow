---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>خط تو سرمه کشد دیده تمنا را</p></div>
<div class="m2"><p>لب تو تازه کند روح صد مسیحا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود به مرهم راحت همیشه طعنه‌فروش</p></div>
<div class="m2"><p>کسی که یافت دلش ذوق داغ سودا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود بر اهل محبت حرام آسایش</p></div>
<div class="m2"><p>تبسمی که کند تازه زخم دلها را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب نباشد اگر در محبت یوسف</p></div>
<div class="m2"><p>دوباره عشق جوانی دهد زلیخا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زهی تصرف خوبان که شیخ صنعان هم</p></div>
<div class="m2"><p>کمند گردن جان کرد زلف ترسا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آتش است ز حسنی دلم که شعله او</p></div>
<div class="m2"><p>برآورد ز تماشای طور، موسی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برای آنکه شود وصل یار زود آخر</p></div>
<div class="m2"><p>ستاره بدم امروز کرده فردا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خون دیده و دل در خیال عارض دوست</p></div>
<div class="m2"><p>کنم به لاله و گل فرش، روی صحرا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا به دیده ما سیر کن نه در گلشن</p></div>
<div class="m2"><p>به برگ گل مکن آزرده آن کف پا را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه شد که دامن قدسی ز خون دیده پرست</p></div>
<div class="m2"><p>کسی ز موج نکرده‌ست منع دریا را</p></div></div>