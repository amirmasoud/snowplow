---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>یکی بود به نظر نیستی و هستی ما</p></div>
<div class="m2"><p>تفاوتی نبود در خمار و مستی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به می‌پرست مزن طعنه زآنکه کمتر نیست</p></div>
<div class="m2"><p>ز می‌پرستی او خویشتن‌پرستی ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود به دیده نادیده برگ کاه چون کوه</p></div>
<div class="m2"><p>بلند قدر نماید فلک ز پستی ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذشت موسم اندوه و وقت عیش آمد</p></div>
<div class="m2"><p>رسید نوبت ایام تنگدستی ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجب که روز جزا هم توان عمارت کرد</p></div>
<div class="m2"><p>خراب‌کرده عشق است ملک هستی ما</p></div></div>