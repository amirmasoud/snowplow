---
title: >-
    شمارهٔ ۳۵۶
---
# شمارهٔ ۳۵۶

<div class="b" id="bn1"><div class="m1"><p>روزی که ناخنی نزند عشق بر دلم</p></div>
<div class="m2"><p>چون آتش فسرده و چون صید بسملم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد برگ گل که جمع کنی غنچه‌ای شود</p></div>
<div class="m2"><p>آسان گره نخورده چنین، کار مشکلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارد ز بس که بر نظر پاکم اعتماد</p></div>
<div class="m2"><p>پروانه خود چراغ در آرد به محفلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افکند ناتوانی‌ام از خود به گوشه‌ای</p></div>
<div class="m2"><p>چون قطره عرق، بن مویی‌ست منزلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کو باد شرطه‌ای که به طوفانم افکند؟</p></div>
<div class="m2"><p>آن باد شرطه نیست که آرد به ساحلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محکم گرفته دامنم این خاک آستان</p></div>
<div class="m2"><p>گویا سرشته‌اند ز خاک درت گلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انداز دل مرا به سر تیر می‌برد</p></div>
<div class="m2"><p>بیدرد را گمان که ز صیاد غافلم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کوته‌نظر زند به گل و لاله دست و من</p></div>
<div class="m2"><p>از همت بلند، به سرو تو مایلم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خواندنم حدیث بدآموز نشنوی</p></div>
<div class="m2"><p>گر بشنوی که بی تو چها رفته بر دلم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نقش پی دلیل، کم از چشم بد نبود</p></div>
<div class="m2"><p>بنگر که بی‌خطر گذراند از چه منزلم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ضعفم ازان گذشته که صیدم کند کسی</p></div>
<div class="m2"><p>بی‌رشته‌ام مقید و بی تیغ بسملم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در گردش است کاسه چشمم پیاله‌وار</p></div>
<div class="m2"><p>ساقی اگر رود نفسی از مقابلم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قدسی نظر به شاهسواران بود مرا</p></div>
<div class="m2"><p>مجنون نیم، به ناقه چکار و به محملم</p></div></div>