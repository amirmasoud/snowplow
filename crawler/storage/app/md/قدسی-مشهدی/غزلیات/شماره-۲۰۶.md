---
title: >-
    شمارهٔ ۲۰۶
---
# شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>مرده بودم از خمار می، شرابم زنده کرد</p></div>
<div class="m2"><p>کشته بود آتش مرا، ساقی به آبم زنده کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نصیحت‌های غمخواران، جنون بازم خرید</p></div>
<div class="m2"><p>گلشن افسرده بودم آفتابم زنده کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرده بودم در کفن، افکند از عارض نقاب</p></div>
<div class="m2"><p>همچو کرم پیله شوقش در نقابم زنده کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی تو ضعفم بود غالب، مرده‌ام پنداشتند</p></div>
<div class="m2"><p>مژده وصلت رسید و اضطرابم زنده کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زندگی از هر عذابی هست مشکلتر مرا</p></div>
<div class="m2"><p>بعد مردن باید از بهر عذابم زنده کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خجالت مرده بودم کز چه بی او زنده‌ام</p></div>
<div class="m2"><p>تا خیال او کُشد باز از حجابم، زنده کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس که افغان دوستم قدسی، اجل چون در رسد</p></div>
<div class="m2"><p>می‌تواند مطرب از صوت ربابم زنده کرد</p></div></div>