---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>طبیب من چه شد گر مهربان نیست؟</p></div>
<div class="m2"><p>من بیمار را پروای جان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غرور خضر، عاشق برنتابد</p></div>
<div class="m2"><p>محبت کم ز عمر جاودان نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی‌جوشند با هم ناتوانان</p></div>
<div class="m2"><p>که با هم، موی را خون در میان نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بیماری سپردم تن چو نرگس</p></div>
<div class="m2"><p>که در عالم طبیب مهربان نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندارم بهره‌ای از مومیایی</p></div>
<div class="m2"><p>شکست دل شکست استخوان نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان چون بود و نابودش مساوی‌ست</p></div>
<div class="m2"><p>چرا گوید کسی کاین هست و آن نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان افسرده خواهد روزگارم</p></div>
<div class="m2"><p>که پنداری مرا در جسم جان نیست</p></div></div>