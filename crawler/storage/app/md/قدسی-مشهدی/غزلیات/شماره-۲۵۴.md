---
title: >-
    شمارهٔ ۲۵۴
---
# شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>سوای تو در سینه هر خام نگنجد</p></div>
<div class="m2"><p>خوش باش که این باده به هر جام نگنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوقی که من از دیدن رخسار تو دیدم</p></div>
<div class="m2"><p>در حوصله دیده ایام نگنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نور توام هر بن مو مطلع صبح است</p></div>
<div class="m2"><p>در ملک تنم تیرگی شام نگنجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وصل تو کجا و من بی‌ظرف که از شوق</p></div>
<div class="m2"><p>در حوصله‌ام لذت پیغام نگنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سینه عشاق هوس راه ندارد</p></div>
<div class="m2"><p>بت در حرم کعبه اسلام نگنجد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قدسی نبود رنگ وفا در رخ خوبان</p></div>
<div class="m2"><p>در دفتر خوبان ز وفا نام نگنجد</p></div></div>