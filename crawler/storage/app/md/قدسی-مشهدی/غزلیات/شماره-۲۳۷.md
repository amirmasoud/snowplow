---
title: >-
    شمارهٔ ۲۳۷
---
# شمارهٔ ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>زورم به یک اشاره ابرو نمی‌رسد</p></div>
<div class="m2"><p>هرگز به ناتوانی من مو نمی‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قمری فکنده طوق به تقلید در گلو</p></div>
<div class="m2"><p>چون گردنش به حلقه گیسو نمی‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انصاف بین که پای به دامن کشیده‌ام</p></div>
<div class="m2"><p>با جامه‌ای که تا سر زانو نمی‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب‌تشنگان ناز، تسلی نمی‌شوند</p></div>
<div class="m2"><p>تا چین زلف یار به ابرو نمی‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازچشم تو که دیده بد دور باد ازو</p></div>
<div class="m2"><p>غیر از نگاه دور به آهو نمی‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلفت به بردن دلم اعجاز می‌کند</p></div>
<div class="m2"><p>نوبت بدان دو نرگس جادو نمی‌رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل گوشه‌گیر موی تو گردید از ازل</p></div>
<div class="m2"><p>چون گوشه‌ای بدان خم گیسو نمی‌رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل در میان گرفته سر زلف یار را</p></div>
<div class="m2"><p>ای شانه دور شو به تو یک مو نمی‌رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قدسی چو تیغ آه ضعیفان شود بلند</p></div>
<div class="m2"><p>کس را سخن ز قوت بازو نمی‌رسد</p></div></div>