---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>مرا به ناله شد آن سرو سیم‌تن باعث</p></div>
<div class="m2"><p>چنان که بلبل شوریده را چمن باعث</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو خواستی ز برم تند بگذری ورنه</p></div>
<div class="m2"><p>برای مکث توان کرد صد سخن باعث</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غزال قدس که دیدی اسیر دانه و دام؟</p></div>
<div class="m2"><p>اگر نمی‌شدی آن سرو سیم‌تن باعث</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همیشه باعث عشق بتان دل قدسی‌ست</p></div>
<div class="m2"><p>چنان که سجده بت راست برهمن باعث</p></div></div>