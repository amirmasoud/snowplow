---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>فسون ناله‌ام شب بسته خواب پاسبانش را</p></div>
<div class="m2"><p>که با هر سر نباشد آشنایی آسمانش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز چاک سینه‌ام دل می‌کند نظاره زلفش</p></div>
<div class="m2"><p>چو مرغی کز قفس بیند به حسرت آشیانش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوازد ظاهر و در دل خیال کشتنم دارد</p></div>
<div class="m2"><p>به حال خویشتن پی برده‌ام لطف نهانش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اسیر عشق را فرض است عزت بعد مردن هم</p></div>
<div class="m2"><p>میندازید بر خاک مذلت استخوانش را</p></div></div>