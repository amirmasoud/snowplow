---
title: >-
    شمارهٔ ۲۲۷
---
# شمارهٔ ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>من و آیینه حسنی که تابش را بسوزاند</p></div>
<div class="m2"><p>دلم را سجده‌های گرم او ابرو بسوزاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دماغم پر شد از سودای آتشپاره‌ای چندان</p></div>
<div class="m2"><p>که شب در کنج تنهایی سرم زانو بسوزاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم را کرد بوی نافه سرگردان به صحرایی</p></div>
<div class="m2"><p>که ریگ دشتش از گرمی سم آهو بسوزاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همان خاکسترم چون گرد از دنبال او افتد</p></div>
<div class="m2"><p>گرم صد ره به رنگ آن غمزه جادو بسوزاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو قدسی بعد ازین دست من و دامان غمناکی</p></div>
<div class="m2"><p>دلم را بستر آسودگی، پهلو بسوزاند</p></div></div>