---
title: >-
    شمارهٔ ۴۱۳
---
# شمارهٔ ۴۱۳

<div class="b" id="bn1"><div class="m1"><p>توان غم تو ز جان خراب دزدیدن</p></div>
<div class="m2"><p>اگر ز شعله توان اضطراب دزدیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حباب‌وار برآور ز آب دیده سری</p></div>
<div class="m2"><p>چو دیده چند توان سر در آب دزدیدن؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال هندوی چشم تو در نمی‌آید</p></div>
<div class="m2"><p>به چشم خلق، مگر بهر خواب دیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز موج گریه‌ام افتد به گردن خورشید</p></div>
<div class="m2"><p>ز روی آب، شکم چون حباب دزدیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلی که وصل تو جوید به حیله، آن یابد</p></div>
<div class="m2"><p>که طفل مکتبی از آفتاب دزدیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو گل ز پرده برون‌آ، که بشکفد گلشن</p></div>
<div class="m2"><p>چو غنچه، روی چرا در نقاب دزدیدن</p></div></div>