---
title: >-
    شمارهٔ ۲۲۶
---
# شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>نشاه می‌خواستم از باده، خمارم دادند</p></div>
<div class="m2"><p>روز روشن طلبیدم شب تارم دادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناله صبحدم و آه شب و گریه شام</p></div>
<div class="m2"><p>هرچه در عشق بتان بود به کارم، دادند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هریک از گبر و مسلمان ز خودم می‌دانند</p></div>
<div class="m2"><p>خود ندانم که به کیش که قرارم دادند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکجا جای کنم، سبزه دمد از نم اشک</p></div>
<div class="m2"><p>در خزان شاد ازانم که بهارم دادند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان به تعجیل چنان می‌رود امشب، که مگر</p></div>
<div class="m2"><p>وعده وصل تو در روز شمارم دادند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راه آمد شد دل می‌طلبیدم از چشم</p></div>
<div class="m2"><p>مردمان جا به سر کوچه یارم دادند</p></div></div>