---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>نوک مژگان چه حیرت گر ز دلها بگذرد؟</p></div>
<div class="m2"><p>دل چه باشد تیر عشق از سنگ خارا بگذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خنجر ناز تو در دل حسرت دیدار را</p></div>
<div class="m2"><p>خون کند، تا بر لبم حرف تمنا بگذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند بر ما طعنه عشق بتان ای شیخ شهر</p></div>
<div class="m2"><p>شیخ صنعان را بگو کز عشق ترسا بگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دورباش غمزه را نازم، کزان کو آفتاب</p></div>
<div class="m2"><p>دیده را بر پشت پا دوزد چو زانجا بگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدن قدسی چنین بیمار و ناپرسیدنش</p></div>
<div class="m2"><p>از تو این بد می‌نماید ورنه بر ما بگذرد</p></div></div>