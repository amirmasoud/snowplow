---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>ای دل می امید دگر بر تو حرام است</p></div>
<div class="m2"><p>کم‌حوصله‌ای خون جگر بر تو حرام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه رنگ وفاداری و نه بوی محبت</p></div>
<div class="m2"><p>در پرده شو ای گل که نظر بر تو حرام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای گردش افلاک به صبحی نرسیدی</p></div>
<div class="m2"><p>گویا شب مایی که سحر بر تو حرام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدسی چو سر از سلسله عشق کشیدی</p></div>
<div class="m2"><p>یاری طلب از تیغ که سر بر تو حرام است</p></div></div>