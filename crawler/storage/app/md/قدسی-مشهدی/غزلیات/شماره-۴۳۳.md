---
title: >-
    شمارهٔ ۴۳۳
---
# شمارهٔ ۴۳۳

<div class="b" id="bn1"><div class="m1"><p>یار بی‌پروا و ما را آرزوی دل بسی</p></div>
<div class="m2"><p>کار خواهد بود با یاری چنین، مشکل بسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان من! دلسوزی پروانه طرز دیگرست</p></div>
<div class="m2"><p>گرچه باشد شمع را جوینده در محفل بسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوته اندیشیم ما و کعبه مقصود دور</p></div>
<div class="m2"><p>سوده شد پای امید و راه تا منزل بسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریه دیر آمد به یادم، اشک ازان شد بی‌اثر</p></div>
<div class="m2"><p>کی دهد حاصل، چو ماند تخم زیر گل بسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز از راه حرمجویان کسی خاری نچید</p></div>
<div class="m2"><p>راه طی کردم به مژگان از پی محمل بسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باده غم گرچه با ما کرد تلخیها به بزم</p></div>
<div class="m2"><p>وقت ساقی خوش، کزو دیدیم روی دل بسی</p></div></div>