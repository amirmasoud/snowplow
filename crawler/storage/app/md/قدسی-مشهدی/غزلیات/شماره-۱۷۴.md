---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>کی بی توام نظاره به چشم آشنا شود؟</p></div>
<div class="m2"><p>بنمای روی خود که مرا دیده وا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوی در تو کعبه روان پی نمی‌برند</p></div>
<div class="m2"><p>گر سنگشان به زیر قدم توتیا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرگس دهد پیاله خالی به دست تو</p></div>
<div class="m2"><p>از بس که پیش چشم تو بی دست و پا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکرنگم آنچنان که به شمشیر آفتاب</p></div>
<div class="m2"><p>باور مکن که روز من از شب جدا شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افتد ز اضطراب دل من در اضطراب</p></div>
<div class="m2"><p>پهلوی من، به بزم تو آن را که جا شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر روی دوستان به نظر زنده‌ام چو شمع</p></div>
<div class="m2"><p>میرم، اگر به هم مژه‌ام آشنا شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیرون شدم ز بزم تو از حرف بوالهوس</p></div>
<div class="m2"><p>مرغ از چمن رمیده ز رشک صبا شود</p></div></div>