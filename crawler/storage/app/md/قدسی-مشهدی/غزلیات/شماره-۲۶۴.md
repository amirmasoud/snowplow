---
title: >-
    شمارهٔ ۲۶۴
---
# شمارهٔ ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>در آتشم از چهره برافروخته‌ای چند</p></div>
<div class="m2"><p>چون شعله ز هم سرکشی آموخته‌ای چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشن نشود بخت ز جمعیت داغش</p></div>
<div class="m2"><p>در سینه دلم ساخته با سوخته‌ای چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ریزند به سر خاک، پی صید ضعیفی</p></div>
<div class="m2"><p>چون دام به هم، چشم تهی دوخته‌ای چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون جلد کتابند، بغل کرده پر اجزا</p></div>
<div class="m2"><p>یک حرف ز صد سطر نیاموخته‌ای چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدسی مکن از اهل زمان شکوه، چه داری</p></div>
<div class="m2"><p>چشم خوشی از ناخوشی آموخته‌ای چند؟</p></div></div>