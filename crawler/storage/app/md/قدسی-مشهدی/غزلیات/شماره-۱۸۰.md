---
title: >-
    شمارهٔ ۱۸۰
---
# شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>نام تو بردم آتش شوقم به جان فتاد</p></div>
<div class="m2"><p>باز این نهفتنی سخنم بر زبان فتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طفلی بود که خون دلم خورده جای شیر</p></div>
<div class="m2"><p>هر قطره اشک کز مژه خون‌فشان فتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غوغای رستخیز برآمد ز هر طرف</p></div>
<div class="m2"><p>چشمت مگر به نیم‌نگه در زمان فتاد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دیده‌ام خیال تو هرچند سیر کرد</p></div>
<div class="m2"><p>هرجا نظر فکند بر آب روان فتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آگه ز حال غرقه به خونان نه‌ای، ای رفیق</p></div>
<div class="m2"><p>کشتی ز موج‌خیز غمت بر کران فتاد</p></div></div>