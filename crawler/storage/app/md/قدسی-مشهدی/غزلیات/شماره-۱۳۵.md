---
title: >-
    شمارهٔ ۱۳۵
---
# شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>از ضعف، ناله‌ام به سراغ اثر نرفت</p></div>
<div class="m2"><p>بیمار ماندم و به مسیحا خبر نرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشکم ز باد دستی مژگان به خاک ریخت</p></div>
<div class="m2"><p>کس را چو من ز رشته ستم بر گهر نرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داغم ز ناتوانی فریاد خویشتن</p></div>
<div class="m2"><p>کز بس ضعیف بود ز یاد اثر نرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز نرفت قاصد اشک من از پی‌اش</p></div>
<div class="m2"><p>کز رشک، دیده چند قدم پیشتر نرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باشد حرام، بی طلب درد، زندگی</p></div>
<div class="m2"><p>آن رگ، بریده به، که پی نیشتر نرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناصح نبست لب ز ملامت به کشتنم</p></div>
<div class="m2"><p>در راه عشق رفت سر و دردسر نرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر وی ز هر طرف نظری باز شد به عیب</p></div>
<div class="m2"><p>آن را که چشم جانب عیب از هنر نرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا مغز استخوان، دم نظّاره‌ام چو شمع</p></div>
<div class="m2"><p>جز وی ز تن نرفت که نور بصر نرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر حال دل چگونه بگریم که در دلم</p></div>
<div class="m2"><p>یک قطره خون نماند که از چشم تر نرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از دیده موج اشک به صد خون دل گذشت</p></div>
<div class="m2"><p>طوفان هم از سفینه ما بی‌خطر نرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رسوای خلق کرد مرا اشک پرده‌در</p></div>
<div class="m2"><p>نگریستم دمی، که به عالم خبر نرفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیغام ما ز هند به ایران که می‌برد؟</p></div>
<div class="m2"><p>صد نامه‌آور آمد و یک نامه بر نرفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گردون به صد شکست تن از من رضا نشد</p></div>
<div class="m2"><p>بر شیشه هم ز سنگ، جفا اینقدر نرفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باریک اگر شوی به سخن بهترک بود</p></div>
<div class="m2"><p>هرگز کسی چو رشته به مغز گهر نرفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مگذار گو میان شهیدان عشق پا</p></div>
<div class="m2"><p>اول قدم کسی که به خون تا کمر نرفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>داغم که وقت رفتن شبگیر، سوی باغ</p></div>
<div class="m2"><p>بلبل چرا به غارت باد سحر نرفت</p></div></div>