---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>در راه تا رود ز من آن نازنین جدا</p></div>
<div class="m2"><p>دستش جدا عنان کشد و آستین جدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بر نشان پای تو مالم رح نیاز</p></div>
<div class="m2"><p>نتوان چو سایه کرد مرا از زمین جدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از لذت خدنگ ستم عضوعضو من</p></div>
<div class="m2"><p>هریک کنند شست ترا آفرین جدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم عاشق وفایم و هم بنده جفا</p></div>
<div class="m2"><p>دارم به سینه داغ جدا بر جبین جدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من ترک عالمی ز برای تو کرده‌ام</p></div>
<div class="m2"><p>از من مشو برای دل آن و این جدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قدسی ندید دولت وصلت به خواب هم</p></div>
<div class="m2"><p>از چون تویی فتاده کسی این چنین جدا</p></div></div>