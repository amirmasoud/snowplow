---
title: >-
    شمارهٔ ۳۹۴
---
# شمارهٔ ۳۹۴

<div class="b" id="bn1"><div class="m1"><p>رخ تو تا شده غایب به صورت از نظرم</p></div>
<div class="m2"><p>کمر به دشمنی خواب بسته چشم ترم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلک ز رشک جدا کردم از تو، ورنه هنوز</p></div>
<div class="m2"><p>نبود وقت جدایی و موسم سفرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فراق در گلویم ریخت زهر غم چندان</p></div>
<div class="m2"><p>که گشت معدن الماس، سینه و جگرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دیده غنچه کند کار خنجر و پیکان</p></div>
<div class="m2"><p>جدا ز دوست گر افتد به بوستان گذرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مار به مهر تو پیدا شد آنقدر دشمن</p></div>
<div class="m2"><p>که روز و شب ز گریبان خویش بر حذرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز گشت چرخ، سرم گر چو خاک فرساید</p></div>
<div class="m2"><p>هوای وصل تو بیرون نمی‌رود ز سرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جدا ز صبح وصال تو در شب هجران</p></div>
<div class="m2"><p>انیس گریه و دمساز ناله سحرم</p></div></div>