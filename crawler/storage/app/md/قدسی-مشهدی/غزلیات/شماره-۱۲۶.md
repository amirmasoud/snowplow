---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>کس چه داند از چه در دل آه شبگیرم شکست</p></div>
<div class="m2"><p>نامه‌ای بر پر نبستم در کمان تیرم شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ تدبیرم به سوی بام وصلش می‌پرید</p></div>
<div class="m2"><p>از قضا در راه بال مرغ تقدیرم شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرده‌ام در خدمتت تقصیر و از تاثیر آن</p></div>
<div class="m2"><p>پشت امیدم خمید و رنگ تقصیرم شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صورت خود می‌کشیدم بهر پابوسش به راه</p></div>
<div class="m2"><p>انفعال این تمنا رنگ تصویرم شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باخبر شد از شکست خود دل آگاه من</p></div>
<div class="m2"><p>آستین دست قضا چون بهر تخمیرم شکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی شوم غمگین که چون قدسی مرید باده‌ام</p></div>
<div class="m2"><p>پشت صد اندوه را یک همت پیرم شکست</p></div></div>