---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>خلاصی‌ام ز کمند تو در ضمیر مباد</p></div>
<div class="m2"><p>اگر اسیر تو نبود دلم اسیر مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهفته مهر تو در سینه ورنه می‌گفتم</p></div>
<div class="m2"><p>چو صبح سینه چاکم رفوپذیر مباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی‌دهی می وصلم که تنگ‌حوصله‌ای</p></div>
<div class="m2"><p>مباد ساقی مجلس بهانه‌گیر، مباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دعا کنید که پرویز را پس از فرهاد</p></div>
<div class="m2"><p>گذار بر طرف قصر و جوی شیر مباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم ز فرقت همدرد خویش، قدسی سوخت</p></div>
<div class="m2"><p>که گفته بود ترا در جهان نظیر مباد</p></div></div>