---
title: >-
    شمارهٔ ۲۵۰
---
# شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>وجودم را نه از آتش، نه از گل پرورش دادند</p></div>
<div class="m2"><p>سراپایم ز نور عشق چون دل پرورش دادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منه بر سینه داغ عشق، در بیرون چرا سوزی</p></div>
<div class="m2"><p>چراغی کز برای خلوت دل پرورش دادند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یاد شعله دایم چون دل پروانه در جوشم</p></div>
<div class="m2"><p>نمی‌دانم چرا ترکیبم از گل پرورش دادند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مقام لیلی‌اش در کعبه دل بود، حیرانم</p></div>
<div class="m2"><p>که مجنون را چرا بر ذوق محمل پرورش دادند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گهی فانوس دیرم، گه چراغ کعبه، کز مهرت</p></div>
<div class="m2"><p>چو ماه نو مرا منزل به منزل پرورش دادند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محبت از پی دل برد قدسی را به صحرایی</p></div>
<div class="m2"><p>که خاکش را به خون صید بسمل پرورش دادند</p></div></div>