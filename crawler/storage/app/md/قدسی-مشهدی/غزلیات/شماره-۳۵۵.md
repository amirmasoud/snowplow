---
title: >-
    شمارهٔ ۳۵۵
---
# شمارهٔ ۳۵۵

<div class="b" id="bn1"><div class="m1"><p>دایم چو غنچه سر به گریبان گریستم</p></div>
<div class="m2"><p>پیدا شکفته بود و پنهان گریستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجا چو غنچه تنگدلی چند یافتم</p></div>
<div class="m2"><p>رفتم چو ابر و بر سر ایشان گریستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شمع، زندگانی من صرف گریه شد</p></div>
<div class="m2"><p>تا آخرین نفس، ز تف جان گریستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ابر هرزه آب رخ خود مبر، که من</p></div>
<div class="m2"><p>چندان که ممکن است چو باران گریستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز ز گریه روی نکردم ترش چو ابر</p></div>
<div class="m2"><p>دایم چو شیشه با لب خندان گریستم</p></div></div>