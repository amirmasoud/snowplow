---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>خوشم به درد مکن ای دوا عذاب را</p></div>
<div class="m2"><p>مکن مکن که عمارت کند خراب مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه آتشی تو نمی‌دانم ای بهشتی روی</p></div>
<div class="m2"><p>که ذوق گریه عشق تو کرد آب مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هجوم گریه نمی‌دانم اینقدر دانم</p></div>
<div class="m2"><p>که جای بر سر آب است چون حباب مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شکوه ستمت مردم و همان خجلم</p></div>
<div class="m2"><p>برون نبرد اجل هم از این حجاب مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عنان لطف کشیدی و پایمال نمود</p></div>
<div class="m2"><p>سبک عنانی صبر گران رکاب مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از قضا به همین خوش‌دلم که چون قدسی</p></div>
<div class="m2"><p>نبرد قسمت ازین در به هیچ باب مرا</p></div></div>