---
title: >-
    شمارهٔ ۳۱۰
---
# شمارهٔ ۳۱۰

<div class="b" id="bn1"><div class="m1"><p>مردم از غیرت، جدا از صحبت اغیار باش</p></div>
<div class="m2"><p>چند روزی هم به رغم غیر، با ما یار باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزم مارا همچو شمع از نور عارض برفروز</p></div>
<div class="m2"><p>غیر هم گو امشبی حسرت‌کش دیدار باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من نمی‌خواهم وصالی را که هجرش در پی است</p></div>
<div class="m2"><p>دیده گو خونابه ریز و سینه گو افگار باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سر کویت به ناکامی ز رشک مدعی</p></div>
<div class="m2"><p>هرچه باداباد خواهم رفت گو دشوار باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند قدسی از می عصیان کشی رطل گران؟</p></div>
<div class="m2"><p>لحظه‌ای هم جرعه‌نوش جام استغفار باش</p></div></div>