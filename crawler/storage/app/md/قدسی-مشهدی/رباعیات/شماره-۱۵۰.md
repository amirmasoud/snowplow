---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>ویرانه شدی، گمان آبادی چیست</p></div>
<div class="m2"><p>غم باید خورد اینقدر شادی چیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محکوم به حکم شرع می‌باید بود</p></div>
<div class="m2"><p>در بندگی‌ات دعوی آزادی چیست</p></div></div>