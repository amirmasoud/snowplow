---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>نی بی دم نایی از نوا بی‌برگ است</p></div>
<div class="m2"><p>بی ملک و درم، شه چو گدا بی‌برگ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سیم نباشد چه دهند اهل کرم؟</p></div>
<div class="m2"><p>کم‌سایه بود درخت تا بی‌برگ است</p></div></div>