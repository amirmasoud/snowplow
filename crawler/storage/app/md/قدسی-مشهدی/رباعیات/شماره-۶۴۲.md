---
title: >-
    شمارهٔ ۶۴۲
---
# شمارهٔ ۶۴۲

<div class="b" id="bn1"><div class="m1"><p>گر سوخته آتش بی دود شوی</p></div>
<div class="m2"><p>هرچند زیانی، همه سر سود شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنجی بنشین و نیستی جوی و مترس</p></div>
<div class="m2"><p>بود تو کدام است که نابود شوی؟</p></div></div>