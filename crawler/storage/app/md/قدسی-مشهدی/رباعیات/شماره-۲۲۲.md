---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>غم‌دیده، فریب سور عالم نخورد</p></div>
<div class="m2"><p>بر هم خورد ار جهان، دلش غم نخورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر از سر بازار، قیامت خیزد</p></div>
<div class="m2"><p>سودای من و عشق تو بر هم نخورد</p></div></div>