---
title: >-
    شمارهٔ ۴۲۹
---
# شمارهٔ ۴۲۹

<div class="b" id="bn1"><div class="m1"><p>گردون که ندانی سبب خیر و شرش</p></div>
<div class="m2"><p>پیداست هزار نفع در هر ضررش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرکوب فلک، تو را به اصلاح آرد</p></div>
<div class="m2"><p>خرمن زند آن نخل که بشکست سرش</p></div></div>