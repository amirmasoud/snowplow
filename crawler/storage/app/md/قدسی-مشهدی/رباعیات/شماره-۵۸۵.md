---
title: >-
    شمارهٔ ۵۸۵
---
# شمارهٔ ۵۸۵

<div class="b" id="bn1"><div class="m1"><p>بد نیز مباش اگر نه خود به گردی</p></div>
<div class="m2"><p>چون نگذازی تن از چه فربه گردی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرم که گدای شهر نشمارندت</p></div>
<div class="m2"><p>باری مکن آنچه خواجه ده گردی</p></div></div>