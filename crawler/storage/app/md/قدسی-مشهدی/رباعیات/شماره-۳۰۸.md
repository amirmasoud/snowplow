---
title: >-
    شمارهٔ ۳۰۸
---
# شمارهٔ ۳۰۸

<div class="b" id="bn1"><div class="m1"><p>آن را که دل از دو کون آزاد بود</p></div>
<div class="m2"><p>کی طبع به وصل این و آن شاد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحر از گهر و حباب دارد دو گره</p></div>
<div class="m2"><p>در یک گره آب و در یکی باد بود</p></div></div>