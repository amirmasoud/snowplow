---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>باید به مدارا طلبید آن مه را</p></div>
<div class="m2"><p>سرعت نبود هنر دل آگه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار چو انگشت به هنگام شمار</p></div>
<div class="m2"><p>افتان خیزان به سر رسان این ره را</p></div></div>