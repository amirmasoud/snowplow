---
title: >-
    شمارهٔ ۴۹۷
---
# شمارهٔ ۴۹۷

<div class="b" id="bn1"><div class="m1"><p>دل پیش تو ای دلبر کاشی داریم</p></div>
<div class="m2"><p>در بزم تو جای بر حواشی داریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاران همه میل آب‌پاشان دارند</p></div>
<div class="m2"><p>ما با تو سر نیازپاشی داریم</p></div></div>