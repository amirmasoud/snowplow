---
title: >-
    شمارهٔ ۴۷۷
---
# شمارهٔ ۴۷۷

<div class="b" id="bn1"><div class="m1"><p>در عشق، به جز زیان ندارد سودم</p></div>
<div class="m2"><p>از تیرگی اختر خود خشنودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بخت سیاه، بر سرم منت‌هاست</p></div>
<div class="m2"><p>چون لاله، چراغ روشن است از دودم</p></div></div>