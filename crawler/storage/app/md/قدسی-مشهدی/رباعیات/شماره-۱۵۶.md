---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>در پیش تو دیوانه و فرزانه یکی‌ست</p></div>
<div class="m2"><p>قدر خزف و گوهر یکدانه یکی‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آینه با روشنی دیده، چرا</p></div>
<div class="m2"><p>در چشم تو آشنا و بیگانه یکی‌ست؟</p></div></div>