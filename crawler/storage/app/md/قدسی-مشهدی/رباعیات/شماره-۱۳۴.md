---
title: >-
    شمارهٔ ۱۳۴
---
# شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>پروردن عشق با خرد هر دو نکوست</p></div>
<div class="m2"><p>تا زان دو نتیجه‌ای برد دشمن و دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نشو و نما به خاک، بی‌بهره بود</p></div>
<div class="m2"><p>هم پوست جدا ز مغز و هم مغز ز پوست</p></div></div>