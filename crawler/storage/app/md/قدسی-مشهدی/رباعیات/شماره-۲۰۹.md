---
title: >-
    شمارهٔ ۲۰۹
---
# شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>چون آب روان نیارمیدن دارد</p></div>
<div class="m2"><p>چون باد به شش جهت دویدن دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل با تو و دیده شوق دیدن دارد</p></div>
<div class="m2"><p>یعنی ز خبر پیش رسیدن دارد</p></div></div>