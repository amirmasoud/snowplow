---
title: >-
    شمارهٔ ۵۰۱
---
# شمارهٔ ۵۰۱

<div class="b" id="bn1"><div class="m1"><p>در باب وجودت ای خداوند جهان</p></div>
<div class="m2"><p>هر طایفه گفته صد دلیل و برهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در وحدت ذات تو به هنگام بیان</p></div>
<div class="m2"><p>هر دسته، چو کلک مو، یکی کرده زبان</p></div></div>