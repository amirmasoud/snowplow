---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>یا رب که فسانه مختصر کن ما را</p></div>
<div class="m2"><p>خواب مستی ز سر به در کن ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای پاکی مرد بی نگاه تو محال</p></div>
<div class="m2"><p>صد خرده بگیر و یک نظر کن ما را</p></div></div>