---
title: >-
    شمارهٔ ۲۹۶
---
# شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>آن قوم که بر خوان سخاوت نمکند</p></div>
<div class="m2"><p>در زیر فلک نی‌اند، گویا ملکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همت که بود پیشه مردان، مطلب</p></div>
<div class="m2"><p>از طایفه‌ای که زیر دست فلکند</p></div></div>