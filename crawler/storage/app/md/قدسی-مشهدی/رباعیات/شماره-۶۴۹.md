---
title: >-
    شمارهٔ ۶۴۹
---
# شمارهٔ ۶۴۹

<div class="b" id="bn1"><div class="m1"><p>با رستم گُرد اگر هماورد تویی</p></div>
<div class="m2"><p>یا در صف کینه‌آوران فرد تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردی نبود برآمدن با چو خودی</p></div>
<div class="m2"><p>با نفس خود ار برآمدی، مرد تویی</p></div></div>