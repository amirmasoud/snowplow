---
title: >-
    شمارهٔ ۵۵۶
---
# شمارهٔ ۵۵۶

<div class="b" id="bn1"><div class="m1"><p>عشقت که به شعله راه پروانه زده</p></div>
<div class="m2"><p>اول آتش در من دیوانه زده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودای تو در دلم تعلق نگذاشت</p></div>
<div class="m2"><p>این دزد، ره قافله در خانه زده</p></div></div>