---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>پیش از خبر آیی نفسی، خوب‌ترست</p></div>
<div class="m2"><p>سبقت نکند بر تو کسی، خوب‌ترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند که هست خوبِ خوب، آمدنت</p></div>
<div class="m2"><p>زودآمدنت ازان بسی خوب‌ترست</p></div></div>