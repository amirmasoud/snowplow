---
title: >-
    شمارهٔ ۵۳۴
---
# شمارهٔ ۵۳۴

<div class="b" id="bn1"><div class="m1"><p>زاهد، تا چند زرق و خودکامی تو؟</p></div>
<div class="m2"><p>داغ است سراپا دلم از خامی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو نامه اعمال که ظاهر گردد</p></div>
<div class="m2"><p>بدنامی عاشق و نکونامی تو</p></div></div>