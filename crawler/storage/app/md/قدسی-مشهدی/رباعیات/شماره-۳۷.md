---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>آمد ز ازل گوهر معنی کمیاب</p></div>
<div class="m2"><p>آراسته ظاهران ندانند این باب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن روز که خلق صورت و معنی شد</p></div>
<div class="m2"><p>کردند قبول صورت آیینه و آب</p></div></div>