---
title: >-
    شمارهٔ ۴۵۶
---
# شمارهٔ ۴۵۶

<div class="b" id="bn1"><div class="m1"><p>بی روی تو زد غبار در چشمم چنگ</p></div>
<div class="m2"><p>چشمم نه ز درد و گریه برکرد این رنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آینه‌ای را که پرستارش نیست</p></div>
<div class="m2"><p>آن آینه را غبار گیرد یا زنگ</p></div></div>