---
title: >-
    شمارهٔ ۱۷۰
---
# شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>روزی صوفی در تصوف می‌سفت</p></div>
<div class="m2"><p>پرسید یکی ازو در آن گفت و شنفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینها که تو می‌گویی اگر گفته خدای</p></div>
<div class="m2"><p>چون پیغمبر به امّنان فاش نگفت؟</p></div></div>