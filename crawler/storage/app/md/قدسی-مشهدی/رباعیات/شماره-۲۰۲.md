---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>آن کس که وطن به چرخ اعلا دارد</p></div>
<div class="m2"><p>از قید تعلق، به زمین جا دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیوند رساندش به پستی، ور نه</p></div>
<div class="m2"><p>خود شعله شمع میل بالا دارد</p></div></div>