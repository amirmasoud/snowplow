---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>گر یافته‌ای حقیقت عالم را</p></div>
<div class="m2"><p>پیوند به اوست خاتم و آدم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس را به خیال که سرگشته مکن</p></div>
<div class="m2"><p>بنمای باد نیر از دو عالم را</p></div></div>