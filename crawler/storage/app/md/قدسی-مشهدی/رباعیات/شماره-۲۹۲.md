---
title: >-
    شمارهٔ ۲۹۲
---
# شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>آن را که خدا به بندگی یاد کند</p></div>
<div class="m2"><p>آزادگی‌اش چگونه دلشاد کند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی به جهان که دارد آزادی را؟</p></div>
<div class="m2"><p>آن بنده که خواجه‌اش خود آزاد کند</p></div></div>