---
title: >-
    شمارهٔ ۶۰۷
---
# شمارهٔ ۶۰۷

<div class="b" id="bn1"><div class="m1"><p>در وحدت ذات، گفته گوهرپاشی</p></div>
<div class="m2"><p>رمزی که بود فاش‌تر از هر فاشی:</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقاش اگر به نقش در می‌آمد</p></div>
<div class="m2"><p>هر نقش ز کلک او شدی نقاشی</p></div></div>