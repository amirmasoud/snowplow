---
title: >-
    شمارهٔ ۳۲۷
---
# شمارهٔ ۳۲۷

<div class="b" id="bn1"><div class="m1"><p>صاحب‌نظری کز پی دیدار رود</p></div>
<div class="m2"><p>باید که چو آفتاب بسیار رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از یک سوی دیوار گر افتد به زمین</p></div>
<div class="m2"><p>از سوی دگر بر سر دیوار رود</p></div></div>