---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>طبع شررانگیز به شر مجبورست</p></div>
<div class="m2"><p>بدطینت اگر بدی کند معذورست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی نفس بد از شکستگی نیک شود؟</p></div>
<div class="m2"><p>شمشیر شکسته چون شود، ساطور است</p></div></div>