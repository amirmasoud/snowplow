---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>هر ذات مگو به ذات جاوید رسد</p></div>
<div class="m2"><p>هر سایه کجا به سایه بید رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پرتو مهر، ذره گردد موجود</p></div>
<div class="m2"><p>اما نتواند که به خورشید رسد</p></div></div>