---
title: >-
    شمارهٔ ۶۰۴
---
# شمارهٔ ۶۰۴

<div class="b" id="bn1"><div class="m1"><p>بر آسایش، مدار ننهاد کسی</p></div>
<div class="m2"><p>بی رنج، قدم به کار ننهاد کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چون قلمش در استخوان مغز نکاست</p></div>
<div class="m2"><p>سر بر خط روزگار ننهاد کسی</p></div></div>