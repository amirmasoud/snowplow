---
title: >-
    شمارهٔ ۵۷۱
---
# شمارهٔ ۵۷۱

<div class="b" id="bn1"><div class="m1"><p>همت طلبی، به تربت مجنون آی</p></div>
<div class="m2"><p>با دیده نمناک و دل پرخون آی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی نگذاری دل پروانه ز دست</p></div>
<div class="m2"><p>در خانه چراغ بر کن و بیرون آی</p></div></div>