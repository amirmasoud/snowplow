---
title: >-
    شمارهٔ ۳۸۹
---
# شمارهٔ ۳۸۹

<div class="b" id="bn1"><div class="m1"><p>ای دوست چنین ز دوست‌داران مگذر</p></div>
<div class="m2"><p>چون سیل که بگذرد بهاران، مگذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند گذشتگی عنان تو گسست</p></div>
<div class="m2"><p>از خود بگذر ولی ز یاران مگذر</p></div></div>