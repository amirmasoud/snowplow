---
title: >-
    شمارهٔ ۵۳۷
---
# شمارهٔ ۵۳۷

<div class="b" id="bn1"><div class="m1"><p>در بزم جهان، شمع شب‌افروزی کو؟</p></div>
<div class="m2"><p>در هفت فلک، اختر فیروزی کو؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی: نبود به یک روش، سیر فلک</p></div>
<div class="m2"><p>عمری‌ست که شب می‌گذرد، روزی کو؟</p></div></div>