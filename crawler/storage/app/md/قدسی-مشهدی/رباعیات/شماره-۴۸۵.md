---
title: >-
    شمارهٔ ۴۸۵
---
# شمارهٔ ۴۸۵

<div class="b" id="bn1"><div class="m1"><p>گردون زند از کین تو هر صبحی دم</p></div>
<div class="m2"><p>روزی کند از عمر تو هر شامی کم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اختر کند از برای خاکت از هم</p></div>
<div class="m2"><p>آری دندان رنج کشد بهر شکم</p></div></div>