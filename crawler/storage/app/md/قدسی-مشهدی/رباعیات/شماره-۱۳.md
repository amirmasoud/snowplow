---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>هرچند خرد جلوه دهد سامان را</p></div>
<div class="m2"><p>از جهل نجات کی دهد نادان را؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نتوان به نعیم خلد باز آوردن</p></div>
<div class="m2"><p>از رغبتِ جو، طبیعت حیوان را</p></div></div>