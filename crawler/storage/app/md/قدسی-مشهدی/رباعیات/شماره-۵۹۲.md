---
title: >-
    شمارهٔ ۵۹۲
---
# شمارهٔ ۵۹۲

<div class="b" id="bn1"><div class="m1"><p>گر شعر نگویم نه ز شعرم عاری</p></div>
<div class="m2"><p>دارم سخنی گوش به من گر داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فکرم بسیار و هر یکی سبقت‌جوی</p></div>
<div class="m2"><p>بیکار چرا نمانم از پرکاری؟</p></div></div>