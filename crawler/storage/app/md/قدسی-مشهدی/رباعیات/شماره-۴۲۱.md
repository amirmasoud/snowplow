---
title: >-
    شمارهٔ ۴۲۱
---
# شمارهٔ ۴۲۱

<div class="b" id="bn1"><div class="m1"><p>از یک جنسند گرچه نیک و بد ناس</p></div>
<div class="m2"><p>زین همجنسان بایدت امید و هراس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیکان و زره، هر دو ز آهن باشند</p></div>
<div class="m2"><p>آن قصد تو دارد، این تو را دارد پاس</p></div></div>