---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>خواهی که کنی قبله سر کویت را</p></div>
<div class="m2"><p>از خواهش کس، ترش مکن رویت را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خلق، گشاده‌روی شو چون محراب</p></div>
<div class="m2"><p>تا سجده برند طاق ابرویت را</p></div></div>