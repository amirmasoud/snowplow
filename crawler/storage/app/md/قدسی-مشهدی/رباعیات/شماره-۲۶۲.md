---
title: >-
    شمارهٔ ۲۶۲
---
# شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>آسوده ز صید عام کی خواهی شد</p></div>
<div class="m2"><p>فارغ ز خیال خام کی خواهی شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمرت همه صرف علم شد، کو علمت؟</p></div>
<div class="m2"><p>انگاره شدی، تمام کی خواهی شد</p></div></div>