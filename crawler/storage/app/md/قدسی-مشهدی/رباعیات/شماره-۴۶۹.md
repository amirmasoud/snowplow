---
title: >-
    شمارهٔ ۴۶۹
---
# شمارهٔ ۴۶۹

<div class="b" id="bn1"><div class="m1"><p>از گریه نکرد ضعف در دیده مقام</p></div>
<div class="m2"><p>وز درد نشد نظاره بر دیده حرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در رهگذر مرغ خیالت چشمم</p></div>
<div class="m2"><p>در زیر غبار مانده چون حلقه دام</p></div></div>