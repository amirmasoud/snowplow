---
title: >-
    شمارهٔ ۲۹۹
---
# شمارهٔ ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>از عشق دلی که دیده دوزد چه کند</p></div>
<div class="m2"><p>چون آتش عشق برفروزد چه کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر شیوه معشوق کند عاشق کار</p></div>
<div class="m2"><p>پروانه چو شمع اگر نسوزد چه کند</p></div></div>