---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>آن را که سری به عشق عالم‌سوزست</p></div>
<div class="m2"><p>بختش مسعود و طالعش فیروز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون که شب از موی سفیدم روزست</p></div>
<div class="m2"><p>معلومم شد که عشق پیرآموزست</p></div></div>