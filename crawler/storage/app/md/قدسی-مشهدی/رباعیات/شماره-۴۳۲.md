---
title: >-
    شمارهٔ ۴۳۲
---
# شمارهٔ ۴۳۲

<div class="b" id="bn1"><div class="m1"><p>خورشید که از پی بروند افلاکش</p></div>
<div class="m2"><p>باید جستن به دیده نمناکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکش ارزد به خون صد چشمه خضر</p></div>
<div class="m2"><p>آبی که خورد به راه دریا، خاکش</p></div></div>