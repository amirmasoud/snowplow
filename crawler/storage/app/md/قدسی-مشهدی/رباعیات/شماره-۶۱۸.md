---
title: >-
    شمارهٔ ۶۱۸
---
# شمارهٔ ۶۱۸

<div class="b" id="bn1"><div class="m1"><p>عاشق نشود هلاک از بی‌برگی</p></div>
<div class="m2"><p>کی نخل فتد به خاک از بی‌برگی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد غم سامان خرداندیشان را</p></div>
<div class="m2"><p>مجنون‌شده را چه باک از بی‌برگی؟</p></div></div>