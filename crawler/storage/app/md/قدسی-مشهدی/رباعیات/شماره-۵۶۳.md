---
title: >-
    شمارهٔ ۵۶۳
---
# شمارهٔ ۵۶۳

<div class="b" id="bn1"><div class="m1"><p>هرگز نشدم جرعه‌کش از جام گله</p></div>
<div class="m2"><p>در زیر لبم شکست پیغام گله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارد چو دلم تاب جفای همه‌کس</p></div>
<div class="m2"><p>شرمم بادا ز بردن نام گله</p></div></div>