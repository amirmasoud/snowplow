---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>گر عقل رمید، عشق دلبر باقی‌ست</p></div>
<div class="m2"><p>صد دجله خون بر مژه تر باقی‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل هست درون سینه گر آه نماند</p></div>
<div class="m2"><p>گر شعله ز پا نشست، اخگر باقی‌ست</p></div></div>