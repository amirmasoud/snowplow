---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>زاهد گوید کلامم از دفتر توست</p></div>
<div class="m2"><p>صوفی گوید مستی‌ام از ساغر توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر قوم که هست، بازگشتش بر توست</p></div>
<div class="m2"><p>هر سو که رود قافله، منزل در توست</p></div></div>