---
title: >-
    شمارهٔ ۴۵۴
---
# شمارهٔ ۴۵۴

<div class="b" id="bn1"><div class="m1"><p>تا این کهن آسیا نکرده‌ست درنگ</p></div>
<div class="m2"><p>از جود، شود فیض‌رسان کی دلتنگ؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دانه که بهره‌ای ازو می‌یابند</p></div>
<div class="m2"><p>یا تخم شود به خاک، یا نرم به سنگ</p></div></div>