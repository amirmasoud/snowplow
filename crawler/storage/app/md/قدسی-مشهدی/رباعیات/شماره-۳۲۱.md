---
title: >-
    شمارهٔ ۳۲۱
---
# شمارهٔ ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>ای بوده در آنچه بوده و هست و بوَد</p></div>
<div class="m2"><p>معبود بر آن چه بوده و هست و بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر موجودی که هست فانی گردد</p></div>
<div class="m2"><p>هستی تو هر آن چه بوده و هست و بود</p></div></div>