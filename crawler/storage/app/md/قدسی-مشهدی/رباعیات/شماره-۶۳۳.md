---
title: >-
    شمارهٔ ۶۳۳
---
# شمارهٔ ۶۳۳

<div class="b" id="bn1"><div class="m1"><p>در بام شریعتی، دگر پر نزنی</p></div>
<div class="m2"><p>خاک در علمی، در دیگر نزنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر قدر مقام خویش را بشناسی</p></div>
<div class="m2"><p>گامی دگر از فلک فروتر نزنی</p></div></div>