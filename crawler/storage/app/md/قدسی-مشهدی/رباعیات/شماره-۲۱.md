---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>در حضرت دوست انس جان را چه بقا</p></div>
<div class="m2"><p>باقی همه اوست، این و آن را چه بقا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرسند به جانی و تسلی به جهان</p></div>
<div class="m2"><p>جان را چه ثبات است و جهان را چه بقا</p></div></div>