---
title: >-
    شمارهٔ ۴۲۸
---
# شمارهٔ ۴۲۸

<div class="b" id="bn1"><div class="m1"><p>آن کس که به معصیت فرو رفته سرش</p></div>
<div class="m2"><p>افسوس خورد، چون شود از خود خبرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باری که شتر فروکشد در مستی</p></div>
<div class="m2"><p>آید چو به حال خویش، بیند اثرش</p></div></div>