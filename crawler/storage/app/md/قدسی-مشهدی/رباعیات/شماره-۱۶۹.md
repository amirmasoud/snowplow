---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>نتوان گهر راز به هر مثقب سفت</p></div>
<div class="m2"><p>دوزند زبان و گوش ازین گفت و شنفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اظهار مکن آنچه نمی‌شاید کرد</p></div>
<div class="m2"><p>زنهار مگو آن چه نمی‌باید گفت</p></div></div>