---
title: >-
    شمارهٔ ۵۴۳
---
# شمارهٔ ۵۴۳

<div class="b" id="bn1"><div class="m1"><p>مشتاق جمال را چه بیگاه و چه گاه</p></div>
<div class="m2"><p>پیوسته بود بر مژه، چون شمع، نگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشتاب که از خیال رویت دارم</p></div>
<div class="m2"><p>یک چشم به نظاره و یک چشم به راه</p></div></div>