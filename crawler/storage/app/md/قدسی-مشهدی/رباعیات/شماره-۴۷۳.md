---
title: >-
    شمارهٔ ۴۷۳
---
# شمارهٔ ۴۷۳

<div class="b" id="bn1"><div class="m1"><p>زان روز که از مادر گیتی زادم</p></div>
<div class="m2"><p>یک لحظه نرفته حسن و عشق از یادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صلوات‎فرست جلوه شیرینم</p></div>
<div class="m2"><p>تعویذنویس بازوی فرهادم</p></div></div>