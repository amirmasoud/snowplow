---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>با مهر تو هر جان به تنی آینه است</p></div>
<div class="m2"><p>هر برگ گلی به گلشنی آینه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دل که به فیض آشنا شد ...</p></div>
<div class="m2"><p>روشن چو شود، هر آهنی آینه است</p></div></div>