---
title: >-
    شمارهٔ ۳۵۸
---
# شمارهٔ ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>هرچند تو را عذاب می‌افزاید</p></div>
<div class="m2"><p>میلت به سوی شراب می‌افزاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عصیان تو، از دامن تر، روز به روز</p></div>
<div class="m2"><p>چون وزن نمد در آب می‌افزاید</p></div></div>