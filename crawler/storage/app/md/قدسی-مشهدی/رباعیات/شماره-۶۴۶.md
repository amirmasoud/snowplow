---
title: >-
    شمارهٔ ۶۴۶
---
# شمارهٔ ۶۴۶

<div class="b" id="bn1"><div class="m1"><p>هرچند که دارد آسمان مهر و مهی</p></div>
<div class="m2"><p>از جای درآردش ز چشمت نگهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بحر، ز هر قطره تنک ظرف ترست</p></div>
<div class="m2"><p>با آن که حباب را چو دریاست تهی</p></div></div>