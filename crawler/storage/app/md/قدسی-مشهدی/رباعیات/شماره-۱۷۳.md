---
title: >-
    شمارهٔ ۱۷۳
---
# شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>هر دل که ازو عشق شماری نگرفت</p></div>
<div class="m2"><p>بگداخت زر خویش و عیاری نگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق گردید و ره به معشوق نبرد</p></div>
<div class="m2"><p>شد گرد، ولی پی سواری نگرفت</p></div></div>