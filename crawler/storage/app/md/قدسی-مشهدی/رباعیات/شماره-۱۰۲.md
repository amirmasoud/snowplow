---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>ای تازه جوان، کمان تندت به کف است</p></div>
<div class="m2"><p>تیرت گذرا یک سر تیر از هدف است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر گرم متاز رخش، دوری دوری‌ست</p></div>
<div class="m2"><p>گر این طرف منزل، اگر آن طرف است</p></div></div>