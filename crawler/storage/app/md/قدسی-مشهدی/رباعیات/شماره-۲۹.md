---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>گیرم که ز اصل خود کند فرع، حجاب</p></div>
<div class="m2"><p>کی اصل جدا ز فرع خود دارد تاب؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پهلو دزدد گرچه حباب از دریا</p></div>
<div class="m2"><p>دریا پهلو تهی نسازد ز حباب</p></div></div>