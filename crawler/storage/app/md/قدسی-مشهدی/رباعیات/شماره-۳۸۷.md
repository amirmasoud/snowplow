---
title: >-
    شمارهٔ ۳۸۷
---
# شمارهٔ ۳۸۷

<div class="b" id="bn1"><div class="m1"><p>از رنگ نفس، هوس شود رنگین‌تر</p></div>
<div class="m2"><p>بی‌آیینان شوند بی‌آیین‌تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون تلخ شوند میوه‌های شیرین</p></div>
<div class="m2"><p>رسم است که تلخ‌تر شود شیرین‌تر</p></div></div>