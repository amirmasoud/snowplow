---
title: >-
    شمارهٔ ۵۵۸
---
# شمارهٔ ۵۵۸

<div class="b" id="bn1"><div class="m1"><p>کی دل شود از هوای خود شرمنده</p></div>
<div class="m2"><p>یا نفس ز مدعای خود شرمنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرمنده‌ام از خدای خود من که مباد</p></div>
<div class="m2"><p>کافر هم، از خدای خود شرمنده</p></div></div>