---
title: >-
    شمارهٔ ۴۹۳
---
# شمارهٔ ۴۹۳

<div class="b" id="bn1"><div class="m1"><p>خواهم ز گذشته‌ها روایت نکنم</p></div>
<div class="m2"><p>وز محنت آینده شکایت نکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بر رخ خلق بندم و در کنجی</p></div>
<div class="m2"><p>بنشینم و با کسی حکایت نکنم</p></div></div>