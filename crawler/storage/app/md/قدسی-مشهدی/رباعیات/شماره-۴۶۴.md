---
title: >-
    شمارهٔ ۴۶۴
---
# شمارهٔ ۴۶۴

<div class="b" id="bn1"><div class="m1"><p>از مهره گردون نکشید آن که ستم</p></div>
<div class="m2"><p>ذکرش به زبان میار و وصفش به رقم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انگشت منه بر ورق ناصافان</p></div>
<div class="m2"><p>بر کاغذ بی‌مهره، رود کند قلم</p></div></div>