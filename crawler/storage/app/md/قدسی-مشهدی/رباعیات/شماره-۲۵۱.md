---
title: >-
    شمارهٔ ۲۵۱
---
# شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>اسباب تعلق همه عارت بخشد</p></div>
<div class="m2"><p>فقرست که تاج افتخارت بخشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچیز که دانسته ازان ترک کنی</p></div>
<div class="m2"><p>حق در عوض یکی هزارت بخشد</p></div></div>