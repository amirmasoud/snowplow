---
title: >-
    شمارهٔ ۶۳۵
---
# شمارهٔ ۶۳۵

<div class="b" id="bn1"><div class="m1"><p>با فقر و فنا، چون فقرا سر چه کنی؟</p></div>
<div class="m2"><p>از باده تقلید، گلو تر چه کنی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نیست تو را ترشح رحمت حق</p></div>
<div class="m2"><p>مانند سحاب، خرقه در بر چه کنی؟</p></div></div>