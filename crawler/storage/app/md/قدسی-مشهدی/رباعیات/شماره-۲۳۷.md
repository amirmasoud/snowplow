---
title: >-
    شمارهٔ ۲۳۷
---
# شمارهٔ ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>از سینه مرا نفس دژم می‌خیزد</p></div>
<div class="m2"><p>چون نی ز مشام، دود غم می‌خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون غنچه اگر دل مرا بشکافی</p></div>
<div class="m2"><p>صد پرده خون ز روی هم می‌خیزد</p></div></div>