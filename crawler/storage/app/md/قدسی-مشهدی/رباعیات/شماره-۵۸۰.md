---
title: >-
    شمارهٔ ۵۸۰
---
# شمارهٔ ۵۸۰

<div class="b" id="bn1"><div class="m1"><p>رنجورم و خسته‌دل، طبیبان مددی!</p></div>
<div class="m2"><p>غربت شده دشمنم، حبیبان مددی!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حب‌الوطنم ز جای برداشته دل</p></div>
<div class="m2"><p>وقت آمده، ای شام غریبان مددی!</p></div></div>