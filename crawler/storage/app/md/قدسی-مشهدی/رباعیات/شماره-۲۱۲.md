---
title: >-
    شمارهٔ ۲۱۲
---
# شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>با جوهر ذات، هرکه یاری دارد</p></div>
<div class="m2"><p>مردانه زبان به حرف جاری دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن را که قوی‌ست دل، زبانش تیزست</p></div>
<div class="m2"><p>شمشیر به قبض استواری دارد</p></div></div>