---
title: >-
    شمارهٔ ۱۹۱
---
# شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>گر کار به رندان قدح‌نوش افتد</p></div>
<div class="m2"><p>در عذر گنه، دل ز ره هوش افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلگرمی طاعت رود از اشک ریا</p></div>
<div class="m2"><p>از آب فسرده، دیگ از جوش افتد</p></div></div>