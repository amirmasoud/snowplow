---
title: >-
    شمارهٔ ۳۸۸
---
# شمارهٔ ۳۸۸

<div class="b" id="bn1"><div class="m1"><p>در پرده ز محتسب شراب اولی‌تر</p></div>
<div class="m2"><p>پوشیدن کار ناصواب اولی‌تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فعل بد خویش را نهان می‌دارم</p></div>
<div class="m2"><p>باشد رخ زشت در نقاب اولی‌تر</p></div></div>