---
title: >-
    شمارهٔ ۱۹۵
---
# شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>زرّاق کجا گرد حقیقت گردد؟</p></div>
<div class="m2"><p>پیوسته در آرایش صورت گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پوشیده نگشت عیب شیاد به ریش</p></div>
<div class="m2"><p>کی بز را دم پوشش عورت گردد؟</p></div></div>