---
title: >-
    شمارهٔ ۳۲۳
---
# شمارهٔ ۳۲۳

<div class="b" id="bn1"><div class="m1"><p>آن کش نظر بلند و برجسته بود</p></div>
<div class="m2"><p>چون عشق، ز بند دو جهان رسته بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوچک‌خردان زیر فلک در قیدند</p></div>
<div class="m2"><p>کودک در مهد، دست و پا بسته بود</p></div></div>