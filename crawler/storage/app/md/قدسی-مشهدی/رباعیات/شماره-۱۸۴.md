---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>بگزیده و نگزیده درین باغ مراد</p></div>
<div class="m2"><p>هر گوشه نشسته‌اند با خاطر شاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگشا به گل چیده و ناچیده نظر</p></div>
<div class="m2"><p>این قسمت آتش است و آن روزی باد</p></div></div>