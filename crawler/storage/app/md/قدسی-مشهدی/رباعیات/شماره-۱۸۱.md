---
title: >-
    شمارهٔ ۱۸۱
---
# شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>ننمود ز جیب آسمان، سینه صبح</p></div>
<div class="m2"><p>بشکست مگر کلید گنجینه صبح؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که گریست چشم قدسی، گویا</p></div>
<div class="m2"><p>زنگار گرفت امشب آیینه صبح</p></div></div>