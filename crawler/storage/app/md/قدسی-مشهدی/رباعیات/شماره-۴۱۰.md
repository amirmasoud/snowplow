---
title: >-
    شمارهٔ ۴۱۰
---
# شمارهٔ ۴۱۰

<div class="b" id="bn1"><div class="m1"><p>قدسی، منم و دلی چو آتش همه سوز</p></div>
<div class="m2"><p>رنگم ز شراب عافیت، گو مفروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر غمزده جز بخت سیه نیست شگون</p></div>
<div class="m2"><p>محروم بود ز شعله پروانه به روز</p></div></div>