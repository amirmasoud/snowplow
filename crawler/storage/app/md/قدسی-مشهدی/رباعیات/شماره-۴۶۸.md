---
title: >-
    شمارهٔ ۴۶۸
---
# شمارهٔ ۴۶۸

<div class="b" id="bn1"><div class="m1"><p>از مژده خویش پیشتر نه دو سه گام</p></div>
<div class="m2"><p>بشتاب که خرسند نگردم به پیام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوشم خبرت جوید و چشمم دیدار</p></div>
<div class="m2"><p>گو چشم ز گوش پیشتر یابد گام</p></div></div>