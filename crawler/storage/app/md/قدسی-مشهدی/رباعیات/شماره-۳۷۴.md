---
title: >-
    شمارهٔ ۳۷۴
---
# شمارهٔ ۳۷۴

<div class="b" id="bn1"><div class="m1"><p>از حادثه گر چرخ شود زیر و زبر</p></div>
<div class="m2"><p>نقصان نپذیرد هنر اهل هنر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فیض هنر بود که هرگز نشود</p></div>
<div class="m2"><p>از شورش بحر، تیره‌دل آب گهر</p></div></div>