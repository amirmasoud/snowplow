---
title: >-
    شمارهٔ ۱۴۹
---
# شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>در سینه دلت کام چه می‌داند چیست</p></div>
<div class="m2"><p>ذوق غم ایام چه می‌داند چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغی که طلسم آشنایی بشکست</p></div>
<div class="m2"><p>آزادگی دام چه می‌داند چیست</p></div></div>