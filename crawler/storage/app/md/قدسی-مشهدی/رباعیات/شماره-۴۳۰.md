---
title: >-
    شمارهٔ ۴۳۰
---
# شمارهٔ ۴۳۰

<div class="b" id="bn1"><div class="m1"><p>این یا رب گرمی که تو داری پاسش</p></div>
<div class="m2"><p>نی خضر بود حریف، نی الیاسش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کشت اجابتی که بر چرخ رسید</p></div>
<div class="m2"><p>مد الف آه تو باشد داسش</p></div></div>