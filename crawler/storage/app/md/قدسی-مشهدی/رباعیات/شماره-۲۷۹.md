---
title: >-
    شمارهٔ ۲۷۹
---
# شمارهٔ ۲۷۹

<div class="b" id="bn1"><div class="m1"><p>آن قوم که دین عشق‌کیشان دارند</p></div>
<div class="m2"><p>بر چرخ، ز اهل قدس، خویشان دارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنها که هوس را بت خود ساخته‌اند</p></div>
<div class="m2"><p>ایشان دانند و آنچه ایشان دارند</p></div></div>