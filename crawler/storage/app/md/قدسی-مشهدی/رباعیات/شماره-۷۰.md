---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>آن کز ازلش به زهد و تقوی کارست</p></div>
<div class="m2"><p>ترک دو جهان، برش مگو دشوار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر اهل صلاح، تهمت دامن تر</p></div>
<div class="m2"><p>چسباندن خاک خشک بر دیوارست</p></div></div>