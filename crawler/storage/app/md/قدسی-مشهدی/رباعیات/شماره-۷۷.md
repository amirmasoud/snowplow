---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>از درد نیافت ضعف بر چشمم دست</p></div>
<div class="m2"><p>وین پرده تار، گریه بر دیده نبست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آمدن پیک خیالت به دلم</p></div>
<div class="m2"><p>برخاست ز دل غبار و بر دیده نشست</p></div></div>