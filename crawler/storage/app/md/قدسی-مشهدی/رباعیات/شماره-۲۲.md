---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>هرگز نکشد مد طمع، خامه ما</p></div>
<div class="m2"><p>از حرف قناعت است پر نامه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انصاف نگر که پا به دامن داریم</p></div>
<div class="m2"><p>با آن که به زانو نرسد جامه ما</p></div></div>