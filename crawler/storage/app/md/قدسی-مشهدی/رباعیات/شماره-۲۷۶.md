---
title: >-
    شمارهٔ ۲۷۶
---
# شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>آنها که دم از گلشن اسرار زدند</p></div>
<div class="m2"><p>این نغمه به گوش هر گرفتار زدند:</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از قید منالید، که در گلشن دهر</p></div>
<div class="m2"><p>آزادی را ز سرو، بر دار زدند</p></div></div>