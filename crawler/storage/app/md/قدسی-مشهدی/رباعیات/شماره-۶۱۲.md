---
title: >-
    شمارهٔ ۶۱۲
---
# شمارهٔ ۶۱۲

<div class="b" id="bn1"><div class="m1"><p>یابی چو ز اهل فضل، شخصی عاصی</p></div>
<div class="m2"><p>نیکو نبود به فضل بی‌اخلاصی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود گو که زیان به اصل گوهر چه رسد</p></div>
<div class="m2"><p>گر بدگهری پیشه کند غواصی</p></div></div>