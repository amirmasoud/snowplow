---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>چون ساخته شد کار جهان را اسباب</p></div>
<div class="m2"><p>برداشتن چیزی ازان بود صواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جمله برداشتنی‌ها، اول</p></div>
<div class="m2"><p>برداشته شد ز پیش چشم تو حجاب</p></div></div>