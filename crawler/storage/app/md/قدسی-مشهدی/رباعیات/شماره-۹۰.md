---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>آن دلبر ناپدید خودکام کجاست</p></div>
<div class="m2"><p>آن پرده‌نشین شهره ایام کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد همه‌جا و نیست جایش پیدا</p></div>
<div class="m2"><p>گویید که آن جان دلارام کجاست</p></div></div>