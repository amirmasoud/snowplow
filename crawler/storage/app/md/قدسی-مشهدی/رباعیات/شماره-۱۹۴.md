---
title: >-
    شمارهٔ ۱۹۴
---
# شمارهٔ ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>وصل چو تویی در آرزو کی گنجد؟</p></div>
<div class="m2"><p>بی ترک جهان، یک سر مو کی گنجد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این لقمه بزرگ آمد و بسیار بزرگ</p></div>
<div class="m2"><p>با پنج انگشت در گلو کی گنجد؟</p></div></div>