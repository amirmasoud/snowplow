---
title: >-
    شمارهٔ ۴۲۴
---
# شمارهٔ ۴۲۴

<div class="b" id="bn1"><div class="m1"><p>بی لخت جگر چو لاله در راغ مباش</p></div>
<div class="m2"><p>بی آتش دل چو غنچه در باغ مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نقش قدم بسوز اگر سرگرمی</p></div>
<div class="m2"><p>در عشق، کم از فتیله داغ مباش</p></div></div>