---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>عالم که اله آفریده‌ست آن را</p></div>
<div class="m2"><p>بد گفتن آن، رخنه کند ایمان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشناختگان نیک نمی‌بینندش</p></div>
<div class="m2"><p>بد ممکن نیست دیده عرفان را</p></div></div>