---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>هرچند که نفس درخور شمشیرست</p></div>
<div class="m2"><p>از تنگی عقل، طبع او را زیرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افتاد چو خلق را به قحطی سر و کار</p></div>
<div class="m2"><p>قدر سگ آسیا فزون از شیر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>