---
title: >-
    شمارهٔ ۱۹۷
---
# شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>مگذار گره به کار ما برگردد</p></div>
<div class="m2"><p>مپسند که روزگار ما برگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتی تو و روزگار برگشته ز ما</p></div>
<div class="m2"><p>برگرد که روزگار ما برگردد</p></div></div>