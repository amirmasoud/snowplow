---
title: >-
    شمارهٔ ۲۶۵
---
# شمارهٔ ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>دل گر لمعات اختر خود داند</p></div>
<div class="m2"><p>کی مهر بتان را هنر خود داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرهیز کند ز صورت بی‌معنی</p></div>
<div class="m2"><p>گر آینه قدر جوهر خود داند</p></div></div>