---
title: >-
    شمارهٔ ۳۷۵
---
# شمارهٔ ۳۷۵

<div class="b" id="bn1"><div class="m1"><p>از قدر هنر، بی‌هنران را چه خبر؟</p></div>
<div class="m2"><p>نیکو نبود نفی هنر ز اهل هنر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقدار سخن، سخن‌شناسان دانند</p></div>
<div class="m2"><p>پوشیده مباد از گوهرسنج، گوهر</p></div></div>