---
title: >-
    شمارهٔ ۶۴۳
---
# شمارهٔ ۶۴۳

<div class="b" id="bn1"><div class="m1"><p>تا کی مشغول عالم خاک شوی</p></div>
<div class="m2"><p>وز بود و نبود شاد و غمناک شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین هستی موهوم اگر پاک شوی</p></div>
<div class="m2"><p>از خیل مجردات افلاک شوی</p></div></div>