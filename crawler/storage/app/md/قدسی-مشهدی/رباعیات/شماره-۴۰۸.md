---
title: >-
    شمارهٔ ۴۰۸
---
# شمارهٔ ۴۰۸

<div class="b" id="bn1"><div class="m1"><p>موجود بود گرچه سراپا عاجز</p></div>
<div class="m2"><p>چون چرخ نباشد دل دانا عاجز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشتی ز پی کنار سرگردان است</p></div>
<div class="m2"><p>ورنه نبود موج به دریا عاجز</p></div></div>