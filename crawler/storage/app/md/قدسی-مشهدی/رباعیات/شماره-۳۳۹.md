---
title: >-
    شمارهٔ ۳۳۹
---
# شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>پاکیزه‌سرشت عاجز غم نشود</p></div>
<div class="m2"><p>کار هنر از عارضه در هم نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند شود آب، کم از جوشیدن</p></div>
<div class="m2"><p>از جوشش بحر آب گهر کم نشود</p></div></div>