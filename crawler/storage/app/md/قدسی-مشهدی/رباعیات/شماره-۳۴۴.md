---
title: >-
    شمارهٔ ۳۴۴
---
# شمارهٔ ۳۴۴

<div class="b" id="bn1"><div class="m1"><p>تا در کف توست دل کی آزاده شود؟</p></div>
<div class="m2"><p>هر دم به خیال دگر آماده شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل در نظر تو دارد این نقش و نگار</p></div>
<div class="m2"><p>آیینه چو از نظر رود ساده شود</p></div></div>