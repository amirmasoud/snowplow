---
title: >-
    شمارهٔ ۳۰۴
---
# شمارهٔ ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>در معرفت آن که عشق بر عقل افزود</p></div>
<div class="m2"><p>در راه طلب نگشت سعیش نابود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی مغز نمی‌کند خرد نشو و نما</p></div>
<div class="m2"><p>از کاشتن دانه بی مغز چه سود؟</p></div></div>