---
title: >-
    شمارهٔ ۵۴۴
---
# شمارهٔ ۵۴۴

<div class="b" id="bn1"><div class="m1"><p>کرد از ره راست چون رسولت آگاه</p></div>
<div class="m2"><p>این ره مگذار و عیش کن خاطرخواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صحرای فراخ شرع افتاده به پیش</p></div>
<div class="m2"><p>بر راهروان تنگ چرا سازی راه؟</p></div></div>