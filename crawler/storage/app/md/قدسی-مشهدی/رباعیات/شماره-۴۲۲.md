---
title: >-
    شمارهٔ ۴۲۲
---
# شمارهٔ ۴۲۲

<div class="b" id="bn1"><div class="m1"><p>احوال زمانه از ستم‌کیشان پرس</p></div>
<div class="m2"><p>نیک و بد خویش را، هم از خویشان پرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کوتهی سپهر اگر می‌پرسی</p></div>
<div class="m2"><p>از بخت بلند کوته‌اندیشان پرس</p></div></div>