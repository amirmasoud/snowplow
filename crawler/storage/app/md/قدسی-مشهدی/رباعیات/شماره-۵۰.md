---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>ناچار به هجر یار می‌باید ساخت</p></div>
<div class="m2"><p>گر گل نبود، به خار می‌باید ساخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را به وفای وعده‌اش نتوان بست</p></div>
<div class="m2"><p>از وعده به انتظار می‌باید ساخت</p></div></div>