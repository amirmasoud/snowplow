---
title: >-
    شمارهٔ ۳۵۷
---
# شمارهٔ ۳۵۷

<div class="b" id="bn1"><div class="m1"><p>در معرکه مردی که ازو کار آید</p></div>
<div class="m2"><p>با شیر نرش پنجه‌زدن عار آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن را که هراسی به دل از رهگذری‌ست</p></div>
<div class="m2"><p>نقش پی مور، در نظر مار آید</p></div></div>