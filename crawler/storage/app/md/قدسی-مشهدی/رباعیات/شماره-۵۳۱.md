---
title: >-
    شمارهٔ ۵۳۱
---
# شمارهٔ ۵۳۱

<div class="b" id="bn1"><div class="m1"><p>هر فعل که از تو در شمارست، ببین</p></div>
<div class="m2"><p>گردون به تلافی‌اش سوارست، ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دادند به دست چپ عنان راکب را</p></div>
<div class="m2"><p>با راست‌روان چه اختیارست، ببین</p></div></div>