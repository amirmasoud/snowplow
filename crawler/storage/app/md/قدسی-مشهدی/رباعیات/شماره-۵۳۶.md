---
title: >-
    شمارهٔ ۵۳۶
---
# شمارهٔ ۵۳۶

<div class="b" id="bn1"><div class="m1"><p>گر دلشده‌ای، جان غم‌اندوزت کو؟</p></div>
<div class="m2"><p>در دیده نگاه حسرت‌افروزت کو؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نرگس اگر کور نه‌ای، کو اشکت؟</p></div>
<div class="m2"><p>چون آتش اگر نمرده‌ای، سوزت کو؟</p></div></div>