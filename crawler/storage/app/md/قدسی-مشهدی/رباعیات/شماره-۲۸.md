---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>چندین به خرابی فلک چیست شتاب؟</p></div>
<div class="m2"><p>خواهد شدن این قطره پر باد، خراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی ماحصل است طعنه بر چرخ زدن</p></div>
<div class="m2"><p>بی نیش فرو نشیند آماس حباب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>