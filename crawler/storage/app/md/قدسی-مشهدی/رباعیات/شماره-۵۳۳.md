---
title: >-
    شمارهٔ ۵۳۳
---
# شمارهٔ ۵۳۳

<div class="b" id="bn1"><div class="m1"><p>اوراد من است داستان غم تو</p></div>
<div class="m2"><p>پیداست ز چهره‌ام نشان غم تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سهل است اگر زیان جانی افتد</p></div>
<div class="m2"><p>یا رب که نبینیم زیان غم تو</p></div></div>