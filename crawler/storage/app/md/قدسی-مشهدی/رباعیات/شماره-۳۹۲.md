---
title: >-
    شمارهٔ ۳۹۲
---
# شمارهٔ ۳۹۲

<div class="b" id="bn1"><div class="m1"><p>از دولت وصل کس مبادا مهجور</p></div>
<div class="m2"><p>بی نور حضور دوست دل را چه حضور؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگاه فتاد آهن آیینه ز نور</p></div>
<div class="m2"><p>از آینه فرق چیست تا نعل ستور؟</p></div></div>