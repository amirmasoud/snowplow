---
title: >-
    شمارهٔ ۵۹۷
---
# شمارهٔ ۵۹۷

<div class="b" id="bn1"><div class="m1"><p>این ره که تو سر کرده‌ای از مغروری</p></div>
<div class="m2"><p>مشکل که نجات بخشدت از دوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شام عسل، تمام چاه است این راه</p></div>
<div class="m2"><p>پوشیده سرش به پرده زنبوری</p></div></div>