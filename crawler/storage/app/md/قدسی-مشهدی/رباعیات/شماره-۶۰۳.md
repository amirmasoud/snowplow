---
title: >-
    شمارهٔ ۶۰۳
---
# شمارهٔ ۶۰۳

<div class="b" id="bn1"><div class="m1"><p>تجرید گزین تا به نوایی برسی</p></div>
<div class="m2"><p>بگسل ز تعلق که به جایی برسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگریز ز کوچه بند نی چون نغمه</p></div>
<div class="m2"><p>شاید که به گوش آشنایی برسی</p></div></div>