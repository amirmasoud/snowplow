---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>با آن که سبک‌تری ز تو در دین نیست</p></div>
<div class="m2"><p>گویی که جز آرام، مرا آیین نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غفلت نگذارد که در آیی از جای</p></div>
<div class="m2"><p>سنگینی خواب آدمی، تمکین نیست</p></div></div>