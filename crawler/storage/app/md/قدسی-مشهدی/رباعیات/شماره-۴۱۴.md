---
title: >-
    شمارهٔ ۴۱۴
---
# شمارهٔ ۴۱۴

<div class="b" id="bn1"><div class="m1"><p>شب‌های دراز رفت و خوابی نه هنوز</p></div>
<div class="m2"><p>خمیازه مرا کشت و شرابی نه هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک ره به کفم جام صبوحی نرسید</p></div>
<div class="m2"><p>صد صبح دمید و آفتابی نه هنوز</p></div></div>