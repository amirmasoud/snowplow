---
title: >-
    شمارهٔ ۲۰۱
---
# شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>گردون خود را گرچه سخی پندارد</p></div>
<div class="m2"><p>جز کام دل همچو خودی برنارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل سختان را ز یکدگر فتح بود</p></div>
<div class="m2"><p>قفل آهن، کلید از آهن دارد</p></div></div>