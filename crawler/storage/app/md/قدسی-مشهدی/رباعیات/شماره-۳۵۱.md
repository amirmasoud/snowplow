---
title: >-
    شمارهٔ ۳۵۱
---
# شمارهٔ ۳۵۱

<div class="b" id="bn1"><div class="m1"><p>چرخم چو ز کشمیر به لاهور کشید</p></div>
<div class="m2"><p>فرمان کتابه‌ای ز درگاه رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فکرم چو کتابه را به انجام رساند</p></div>
<div class="m2"><p>تاریخ بود کتابه عرش مجید</p></div></div>