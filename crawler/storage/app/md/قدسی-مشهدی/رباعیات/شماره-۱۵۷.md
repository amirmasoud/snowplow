---
title: >-
    شمارهٔ ۱۵۷
---
# شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>آن گل که وفای بلبلانش حالی‌ست</p></div>
<div class="m2"><p>در پرسش ما قرین فارغ‌بالی‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد مجلس داشت با حریفان همه شب</p></div>
<div class="m2"><p>یک بار نگفت جای قدسی خالی‌ست</p></div></div>