---
title: >-
    شمارهٔ ۲۲۳
---
# شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>هر صبح، فلک کم هر اختر گیرد</p></div>
<div class="m2"><p>کز مهر، کسی یک اخترش برگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش‌زنه را بسی شرر صرف شود</p></div>
<div class="m2"><p>کز یک شررش، سوخته‌ای درگیرد</p></div></div>