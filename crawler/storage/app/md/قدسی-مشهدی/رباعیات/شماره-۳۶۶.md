---
title: >-
    شمارهٔ ۳۶۶
---
# شمارهٔ ۳۶۶

<div class="b" id="bn1"><div class="m1"><p>نتوان به خدا به زعم ادراک رسید</p></div>
<div class="m2"><p>از عجز دعا ز دل بر افلاک رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانست که معرفت همین باشد و بس</p></div>
<div class="m2"><p>عارف چو به کنه ما عرفناک رسید</p></div></div>