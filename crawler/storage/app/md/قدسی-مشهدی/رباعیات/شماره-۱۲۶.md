---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>از گریه و درد دیده را کی خطرست؟</p></div>
<div class="m2"><p>تاریکی چشم من ز جای دگرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری‌ست که عکس یار دور از نظرست</p></div>
<div class="m2"><p>این آینه را غبار ازان رهگذرست</p></div></div>