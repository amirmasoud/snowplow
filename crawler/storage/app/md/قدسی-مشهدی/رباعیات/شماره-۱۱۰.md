---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>جان در تن مرد، حجت یزدان است</p></div>
<div class="m2"><p>بر ذات صمد، هر احدی برهان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند که جان زنده به جانان باشد</p></div>
<div class="m2"><p>نتوان گفتن که جان به تن جانان است</p></div></div>