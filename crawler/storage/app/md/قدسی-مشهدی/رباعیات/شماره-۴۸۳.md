---
title: >-
    شمارهٔ ۴۸۳
---
# شمارهٔ ۴۸۳

<div class="b" id="bn1"><div class="m1"><p>از خاک درت گر چو صبا برخیزم</p></div>
<div class="m2"><p>یا رب به کجا فتم، کجا برخیزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کوی تو افتاده سرم بر سر راه</p></div>
<div class="m2"><p>هر چند ز ره چو نقش پا برخیزم</p></div></div>