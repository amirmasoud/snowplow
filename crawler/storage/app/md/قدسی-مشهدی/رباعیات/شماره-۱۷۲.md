---
title: >-
    شمارهٔ ۱۷۲
---
# شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>آنم که به من هیچ‌کس الفت نگرفت</p></div>
<div class="m2"><p>کس دست من از راه محبت نگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کارم به مثل آینه در زنگ است</p></div>
<div class="m2"><p>کز خلق، به هیچ وجه، صورت نگرفت</p></div></div>