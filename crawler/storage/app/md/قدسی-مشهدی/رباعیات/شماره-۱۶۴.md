---
title: >-
    شمارهٔ ۱۶۴
---
# شمارهٔ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>دانی که چرا قضا چو نقش تو نگاشت</p></div>
<div class="m2"><p>سرو چمنت را به بلندی نفراشت؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر فتنه که داشت، صرف چشمانت کرد</p></div>
<div class="m2"><p>زین بیش، قضا فتنه و آشوب نداشت</p></div></div>