---
title: >-
    شمارهٔ ۴۰۶
---
# شمارهٔ ۴۰۶

<div class="b" id="bn1"><div class="m1"><p>نتوان ز قضا گریختن با تک و تاز</p></div>
<div class="m2"><p>با چرخ چه چاره از جدل کردن ساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرم که شود ناخن تدبیر دراز</p></div>
<div class="m2"><p>نتوان گره ستاره را کردن باز</p></div></div>