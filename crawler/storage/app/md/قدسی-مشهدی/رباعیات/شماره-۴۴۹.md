---
title: >-
    شمارهٔ ۴۴۹
---
# شمارهٔ ۴۴۹

<div class="b" id="bn1"><div class="m1"><p>ای عمر گرامی به ریا کرده تلف</p></div>
<div class="m2"><p>دل را خبر از ذکر نه و سبحه به کف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آرایش ظاهر چه کنی آینه‌وار؟</p></div>
<div class="m2"><p>رو باطنت آراسته گردان چو صدف</p></div></div>