---
title: >-
    شمارهٔ ۴۴۵
---
# شمارهٔ ۴۴۵

<div class="b" id="bn1"><div class="m1"><p>پیدا بود از اشک فروهشته شمع</p></div>
<div class="m2"><p>کز تخم شرر چه بر دهد کشته شمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آویخته شد بر سر بازار ز دار</p></div>
<div class="m2"><p>شد پنبه حلاج مگر رشته شمع</p></div></div>