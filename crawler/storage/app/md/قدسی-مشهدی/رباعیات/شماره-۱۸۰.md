---
title: >-
    شمارهٔ ۱۸۰
---
# شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>خون شد جگر امشبم ز نادیدن صبح</p></div>
<div class="m2"><p>گویا که بریده شد پی توسن صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه سحرم اگر مددکار شود</p></div>
<div class="m2"><p>از پنجه خورشید کشم دامن صبح</p></div></div>