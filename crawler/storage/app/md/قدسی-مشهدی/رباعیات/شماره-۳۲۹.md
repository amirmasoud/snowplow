---
title: >-
    شمارهٔ ۳۲۹
---
# شمارهٔ ۳۲۹

<div class="b" id="bn1"><div class="m1"><p>خونم ز ره نظر به در چون نرود؟</p></div>
<div class="m2"><p>دلتنگ شوم ز دیده گر خون نرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رحم است بر آن که راه روزن بندد</p></div>
<div class="m2"><p>کز خانه‌اش آفتاب بیرون نرود</p></div></div>