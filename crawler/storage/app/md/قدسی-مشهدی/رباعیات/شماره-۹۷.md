---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>قدسی غم عشق، هم‌نشین تو بس است</p></div>
<div class="m2"><p>داغ ستم دوست، قرین تو بس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای داغ، تو هم دگر چه خواهی کردن</p></div>
<div class="m2"><p>ملکی چو دلم زیر نگین تو بس است</p></div></div>