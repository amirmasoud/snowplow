---
title: >-
    شمارهٔ ۶۲۰
---
# شمارهٔ ۶۲۰

<div class="b" id="bn1"><div class="m1"><p>آن رند که در مثل ندارد بدلی</p></div>
<div class="m2"><p>دی گفت برای اهل عرفان مثلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز بندگی از خداشناسان نسزد</p></div>
<div class="m2"><p>از علم چه سود اگر نباشد عملی</p></div></div>