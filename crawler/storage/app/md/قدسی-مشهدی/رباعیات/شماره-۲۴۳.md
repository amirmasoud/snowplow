---
title: >-
    شمارهٔ ۲۴۳
---
# شمارهٔ ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>کی دهر حقیقت گل و خس پرسد</p></div>
<div class="m2"><p>از نخل قدیم و شاخ نورس پرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای وای بر اهل دولت امروز، اگر</p></div>
<div class="m2"><p>گیتی پسر کیستی از کس پرسد</p></div></div>