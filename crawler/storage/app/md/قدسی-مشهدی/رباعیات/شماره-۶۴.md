---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>دانم ندهد به گفتگو وصلش دست</p></div>
<div class="m2"><p>این غصه، لبم را ز سخن‌گفتن بست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا حسن طلب بسته لبم را قدسی</p></div>
<div class="m2"><p>چون نقطه نمی‌توان به حرفم پیوست</p></div></div>