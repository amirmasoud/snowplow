---
title: >-
    شمارهٔ ۵۶۴
---
# شمارهٔ ۵۶۴

<div class="b" id="bn1"><div class="m1"><p>از خلق جهان، چه نیک و بد، یک قلمه</p></div>
<div class="m2"><p>حق در دنیا برآورد کار همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند که باشد رمه غافل ز شبان</p></div>
<div class="m2"><p>غافل نشود شبان ز احوال رمه</p></div></div>