---
title: >-
    شمارهٔ ۳۳۱
---
# شمارهٔ ۳۳۱

<div class="b" id="bn1"><div class="m1"><p>شوریده عشق اگر غم‌آلود شود</p></div>
<div class="m2"><p>از طینت پاک، زود خشنود شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرچشمه چو تیره گردد از کاویدن</p></div>
<div class="m2"><p>آید چو به حال، رفع آن زود شود</p></div></div>