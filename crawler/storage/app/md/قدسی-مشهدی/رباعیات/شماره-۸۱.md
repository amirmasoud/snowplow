---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>بر من ز تمنای دل کام‌پرست</p></div>
<div class="m2"><p>افتاده ز بس شکست بر روی شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگاه که خون شود دلم شاد شوم</p></div>
<div class="m2"><p>شاید که به خون دل ز خون شویم دست</p></div></div>