---
title: >-
    شمارهٔ ۶۱۹
---
# شمارهٔ ۶۱۹

<div class="b" id="bn1"><div class="m1"><p>گر بتوانی، به نفس خود کن جدلی</p></div>
<div class="m2"><p>سلطانی راست فقر، نعم‌البدلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین هستی موهوم خرابی، ور نه</p></div>
<div class="m2"><p>ویرانه نیستی ندارد خللی</p></div></div>