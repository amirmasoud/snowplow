---
title: >-
    شمارهٔ ۵۰۳
---
# شمارهٔ ۵۰۳

<div class="b" id="bn1"><div class="m1"><p>بی راهبری که سوزدش بهر تو جان</p></div>
<div class="m2"><p>افروختن شمع محبت نتوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی شعله و موم ربط یابند به هم؟</p></div>
<div class="m2"><p>تا پای فتیله‌ای نباشد به میان</p></div></div>