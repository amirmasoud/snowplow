---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>ای گلشن سودا، گل داغ تو کجاست</p></div>
<div class="m2"><p>ای انجمن گرم، چراغ تو کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر پیدایی، این همه پنهانی چیست</p></div>
<div class="m2"><p>ور پنهانی، بگو سراغ تو کجاست</p></div></div>