---
title: >-
    شمارهٔ ۴۶۳
---
# شمارهٔ ۴۶۳

<div class="b" id="bn1"><div class="m1"><p>از بس که فسرد از نفس سرد، تنم</p></div>
<div class="m2"><p>در پیراهن، چو مرده‌ای در کفنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این طرفه که لب نبندم از خنده چو گل</p></div>
<div class="m2"><p>با آنکه چو غنچه، مرده خون در بدم</p></div></div>