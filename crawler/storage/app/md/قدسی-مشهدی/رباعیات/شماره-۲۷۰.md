---
title: >-
    شمارهٔ ۲۷۰
---
# شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>قدسی شب وصل، دل در امّید ببند</p></div>
<div class="m2"><p>وز دود جگر، روزن خورشید ببند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک بار ز درد برکش از دل آهی</p></div>
<div class="m2"><p>بر چرخ، طلوع صبح، جاوید ببند</p></div></div>