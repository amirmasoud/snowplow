---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>اشک آمده مصدر اثر در همه باب</p></div>
<div class="m2"><p>خوش منت‌هاست بر سر از چشم پر آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا گریه بود، پی سخن گم نشود</p></div>
<div class="m2"><p>در گل نشود نقش پی از باد خراب</p></div></div>