---
title: >-
    شمارهٔ ۶۲۸
---
# شمارهٔ ۶۲۸

<div class="b" id="bn1"><div class="m1"><p>حیرت‌زده را چه غم ز سرگردانی</p></div>
<div class="m2"><p>از بی‌برگی چه باک و بی‌سامانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از محنت هجر و لذت وصل، دلم</p></div>
<div class="m2"><p>آسود، که آسوده شود حیرانی</p></div></div>