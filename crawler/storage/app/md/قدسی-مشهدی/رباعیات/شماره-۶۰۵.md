---
title: >-
    شمارهٔ ۶۰۵
---
# شمارهٔ ۶۰۵

<div class="b" id="bn1"><div class="m1"><p>خرسند بود چند به نام از تو کسی</p></div>
<div class="m2"><p>بخرام، که نگرفته خرام از تو کسی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچیز که داری تو، ز هم خوب‌ترست</p></div>
<div class="m2"><p>خود گو که طلب کند کدام از تو کسی</p></div></div>