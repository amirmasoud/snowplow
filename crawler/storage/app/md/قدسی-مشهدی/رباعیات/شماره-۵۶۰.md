---
title: >-
    شمارهٔ ۵۶۰
---
# شمارهٔ ۵۶۰

<div class="b" id="bn1"><div class="m1"><p>ای همچو خرد در همه فن سنجیده</p></div>
<div class="m2"><p>نام تو کس از من به بدی نشنیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دشنام نویسی تو و من همچو دعا</p></div>
<div class="m2"><p>برخوانم و بوسم و نهم بر دیده</p></div></div>