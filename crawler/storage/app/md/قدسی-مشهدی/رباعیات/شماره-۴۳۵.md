---
title: >-
    شمارهٔ ۴۳۵
---
# شمارهٔ ۴۳۵

<div class="b" id="bn1"><div class="m1"><p>ای فیض ازل با نفست دوشادوش</p></div>
<div class="m2"><p>از غیب، تمنای دلت کرده سروش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را ز دعا مکن فراموش که هست</p></div>
<div class="m2"><p>بر صوت دعای تو اجابت را گوش</p></div></div>