---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>برگ از طوبی به کوشش باد نریخت</p></div>
<div class="m2"><p>اختر به زمین ز سنگ بیداد نریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگرفت هنر ز مرد، بیداد زمان</p></div>
<div class="m2"><p>جوهر به گداختن ز فولاد نریخت</p></div></div>