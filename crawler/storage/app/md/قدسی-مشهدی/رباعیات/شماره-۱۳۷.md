---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>ای عشق که جنگ عالمی بر سر توست</p></div>
<div class="m2"><p>دل از بر هرکه رفت بیرون، بر توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین جمع که جمعیت‌شان بر در توست</p></div>
<div class="m2"><p>هر فرد که باز بینی از دفتر توست</p></div></div>