---
title: >-
    شمارهٔ ۳۳۳
---
# شمارهٔ ۳۳۳

<div class="b" id="bn1"><div class="m1"><p>از هوش رود چو با تو دل یار شود</p></div>
<div class="m2"><p>کم‌حوصله، زود مست دیدار شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز ز جوش باده، خم بی‌خبرست</p></div>
<div class="m2"><p>فردا که تهی شود، خبردار شود</p></div></div>