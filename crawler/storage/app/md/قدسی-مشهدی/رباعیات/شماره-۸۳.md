---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>قدسی همه کارت اثر نفس و هواست</p></div>
<div class="m2"><p>این بی‌خردی ز کودکی یا سوداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی که به دست تو رسد نامه جرم</p></div>
<div class="m2"><p>آن روز کنی فرق ز دست چپ، راست</p></div></div>