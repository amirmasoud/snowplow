---
title: >-
    شمارهٔ ۵۸۱
---
# شمارهٔ ۵۸۱

<div class="b" id="bn1"><div class="m1"><p>از دوری خود، بی پر و بالم کردی</p></div>
<div class="m2"><p>دانسته گرفتار ملالم کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتی ز نظر، ولی نرفتی از یاد</p></div>
<div class="m2"><p>دیوانه سودای محالم کردی</p></div></div>