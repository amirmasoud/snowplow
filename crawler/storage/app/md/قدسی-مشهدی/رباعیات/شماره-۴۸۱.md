---
title: >-
    شمارهٔ ۴۸۱
---
# شمارهٔ ۴۸۱

<div class="b" id="bn1"><div class="m1"><p>با آن که گذشت در نقاب از نظرم</p></div>
<div class="m2"><p>گویی که فتاد آفتاب از نظرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود را ز میان بردم و او را دیدم</p></div>
<div class="m2"><p>شد پاره ز بیخودی حجاب از نظرم</p></div></div>