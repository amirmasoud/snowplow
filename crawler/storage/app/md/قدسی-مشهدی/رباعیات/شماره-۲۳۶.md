---
title: >-
    شمارهٔ ۲۳۶
---
# شمارهٔ ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>افتاده عشق، کی ز جا برخیزد</p></div>
<div class="m2"><p>نقشش مگر از باد فنا برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خاک، همان ز خاک افتاده‌ترست</p></div>
<div class="m2"><p>هرچند ز خاک، نقش پا برخیزد</p></div></div>