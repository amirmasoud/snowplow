---
title: >-
    شمارهٔ ۱۹۰
---
# شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>از باده عشق، هرکه بیهوش افتد</p></div>
<div class="m2"><p>تا روز جزا واله و مدهوش افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق به ملامت نکند ترک ز عشق</p></div>
<div class="m2"><p>کی بحر به آب سرد از جوش افتد؟</p></div></div>