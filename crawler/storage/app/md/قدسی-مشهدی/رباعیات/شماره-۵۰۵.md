---
title: >-
    شمارهٔ ۵۰۵
---
# شمارهٔ ۵۰۵

<div class="b" id="bn1"><div class="m1"><p>جایی که بود پای محبت به میان</p></div>
<div class="m2"><p>عاشق نبود بر دل معشوق گران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قمری به سر سرو نگر، تا دانی</p></div>
<div class="m2"><p>آن بار ازین می‌کشد، این ناز ازان</p></div></div>