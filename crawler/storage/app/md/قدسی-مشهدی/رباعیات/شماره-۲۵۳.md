---
title: >-
    شمارهٔ ۲۵۳
---
# شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>این نفس که فقر کاش پاکش بکشد</p></div>
<div class="m2"><p>پر بی‌دردست، دردناکش بکشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افتادگی‌ام ز سرکشی داد نجات</p></div>
<div class="m2"><p>آتش چو بلند گشت، خاکش بکشد</p></div></div>