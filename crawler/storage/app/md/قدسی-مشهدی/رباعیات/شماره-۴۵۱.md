---
title: >-
    شمارهٔ ۴۵۱
---
# شمارهٔ ۴۵۱

<div class="b" id="bn1"><div class="m1"><p>خوش نیست حقیقت و مجاز از هم پاک</p></div>
<div class="m2"><p>این هردو به هم خوشند تا وقت هلاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معنی نکند نشو و نما بی صورت</p></div>
<div class="m2"><p>هر دانه ز پوست می‌نهد ریشه به خاک</p></div></div>