---
title: >-
    شمارهٔ ۲۹۰
---
# شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>این خلق مجازی نه ز اهل هوشند</p></div>
<div class="m2"><p>گر اهل ردا، وگر مرقّع‌پوشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمیزششان به هم ندارد مزه‌ای</p></div>
<div class="m2"><p>با هم چو شراب بی‌نمک می‌جوشند</p></div></div>