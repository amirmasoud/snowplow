---
title: >-
    شمارهٔ ۴۷۰
---
# شمارهٔ ۴۷۰

<div class="b" id="bn1"><div class="m1"><p>چندی ز خرد، پختگی اندوخته‌ام</p></div>
<div class="m2"><p>چندی چو شرر، سوختن آموخته‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این دم که به حال خود نظر دوخته‌ام</p></div>
<div class="m2"><p>نی خامم و نی پخته و نی سوخته‌ام</p></div></div>