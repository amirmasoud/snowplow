---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>جز عشق تو غم از دل غمناک نرُفت</p></div>
<div class="m2"><p>دل را ز هوس، جز نظر پاک نرُفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهر تو برد کدورت از دل، آری</p></div>
<div class="m2"><p>کس سایه چو آفتاب از خاک نرُفت</p></div></div>