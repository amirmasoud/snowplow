---
title: >-
    شمارهٔ ۶۰۸
---
# شمارهٔ ۶۰۸

<div class="b" id="bn1"><div class="m1"><p>گر از یاری، مدام از خود باشی</p></div>
<div class="m2"><p>ور زان خودی، به نام از خود باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک ذره‌ات از خود نبود، تا به خودی</p></div>
<div class="m2"><p>بیخود چو شوی، تمام از خود باشی</p></div></div>