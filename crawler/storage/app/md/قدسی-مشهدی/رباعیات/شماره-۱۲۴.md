---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>امید به عقل، چون ثمر از بیدست</p></div>
<div class="m2"><p>عشق است کزو چشم هزار امیدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی عشق، خرد نتیجه کی می‌بخشد؟</p></div>
<div class="m2"><p>فیض دم صبح از اثر خورشیدست</p></div></div>