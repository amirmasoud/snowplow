---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>تا مهر تو در سینه صد چاک نشست</p></div>
<div class="m2"><p>گردی ز حسد بر دل افلاک نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیوست به تن، ز جان چو بگذشت غمت</p></div>
<div class="m2"><p>این تیر ز صید جست و در خاک نشست</p></div></div>