---
title: >-
    شمارهٔ ۴۵۲
---
# شمارهٔ ۴۵۲

<div class="b" id="bn1"><div class="m1"><p>بس تجربه کردیم درین عالم خاک</p></div>
<div class="m2"><p>نیکان، نیکی کنند تا وقت هلاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بد سر نزند ز نیک طینت هرگز</p></div>
<div class="m2"><p>آری اصل نهال می‌باید پاک</p></div></div>