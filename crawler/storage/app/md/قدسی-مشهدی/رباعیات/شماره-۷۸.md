---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>این پرده به روی دیده‌ام درد نبست</p></div>
<div class="m2"><p>وین خار به چشمم مژه تر نشکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی روی تو از بس که غبارآلودست</p></div>
<div class="m2"><p>تیر نگهم به دیده در خاک نشست</p></div></div>