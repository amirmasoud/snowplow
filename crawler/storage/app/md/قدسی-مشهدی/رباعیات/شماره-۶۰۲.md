---
title: >-
    شمارهٔ ۶۰۲
---
# شمارهٔ ۶۰۲

<div class="b" id="bn1"><div class="m1"><p>شمعی تو، ولی ز انجمن پرهیزی</p></div>
<div class="m2"><p>جانی و ز صحبت بدن پرهیزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من با تو یکیّ و تو ز من پرهیزی</p></div>
<div class="m2"><p>من بعد مگر ز خویشتن پرهیزی</p></div></div>