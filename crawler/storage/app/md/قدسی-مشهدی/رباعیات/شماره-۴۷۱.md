---
title: >-
    شمارهٔ ۴۷۱
---
# شمارهٔ ۴۷۱

<div class="b" id="bn1"><div class="m1"><p>هر دم نتوان کرد به جامی مستم</p></div>
<div class="m2"><p>یک جرعه خراب داردم تا هستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی که قدم نهاد در کوی تو دل</p></div>
<div class="m2"><p>اول، ره بیرون شدنش را بستم</p></div></div>