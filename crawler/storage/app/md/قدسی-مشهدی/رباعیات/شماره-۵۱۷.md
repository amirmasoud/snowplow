---
title: >-
    شمارهٔ ۵۱۷
---
# شمارهٔ ۵۱۷

<div class="b" id="bn1"><div class="m1"><p>از نامه رسی پیش، چه خواهد بودن</p></div>
<div class="m2"><p>آری خبر خویش، چه خواهد بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در وادی زودآمدن از خامه شوق</p></div>
<div class="m2"><p>اظهار ازین بیش چه خواهد بودن</p></div></div>