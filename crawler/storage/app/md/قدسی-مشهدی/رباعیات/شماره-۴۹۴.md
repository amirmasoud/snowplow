---
title: >-
    شمارهٔ ۴۹۴
---
# شمارهٔ ۴۹۴

<div class="b" id="bn1"><div class="m1"><p>قدسی هوس کام‌پرستی نکنم</p></div>
<div class="m2"><p>لب بر لب خم چو خشت و مستی نکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب روز شود ز برق آهم، اما</p></div>
<div class="m2"><p>بر صبح به خنده پیش‌دستی نکنم</p></div></div>