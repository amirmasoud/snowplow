---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>دود دلم آسمان‌گدازست امشب</p></div>
<div class="m2"><p>چون راه عدم، دور و درازست امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد دور بگشت چرخ و بیدار نشد</p></div>
<div class="m2"><p>خورشید مگر به خواب نازست امشب؟</p></div></div>