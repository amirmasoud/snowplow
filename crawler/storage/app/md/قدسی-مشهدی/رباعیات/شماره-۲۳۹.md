---
title: >-
    شمارهٔ ۲۳۹
---
# شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>کو عشق که اهل درد را بشناسد؟</p></div>
<div class="m2"><p>مردی باید که مرد را بشناسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیگانه و آشنا سوارند تمام</p></div>
<div class="m2"><p>کو دیده‌وری که گرد را بشناسد؟</p></div></div>