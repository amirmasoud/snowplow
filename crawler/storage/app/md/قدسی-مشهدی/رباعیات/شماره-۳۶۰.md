---
title: >-
    شمارهٔ ۳۶۰
---
# شمارهٔ ۳۶۰

<div class="b" id="bn1"><div class="m1"><p>کی عشق برون از دل پر خون آید؟</p></div>
<div class="m2"><p>... نشسته چون رود، چون آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معنی نکند ز جای خود نقل مکان</p></div>
<div class="m2"><p>هرچند که از کلام بیرون آید</p></div></div>