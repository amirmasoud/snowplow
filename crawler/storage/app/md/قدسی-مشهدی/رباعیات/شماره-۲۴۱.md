---
title: >-
    شمارهٔ ۲۴۱
---
# شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>مستغرق حال، قال را نشناسد</p></div>
<div class="m2"><p>بی عشق، کس اهل حال را نشناسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می، پخته و خام را ز هم فرق کند</p></div>
<div class="m2"><p>چون آب، کسی سفال را نشناسد</p></div></div>