---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>جان از تو دمی برد دمی دیگر باخت</p></div>
<div class="m2"><p>تن از تو گهی قوی شد و گاه گداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه در نظر آیی و نه در دل گنجی</p></div>
<div class="m2"><p>ای عشق، حقیقت تو را کس نشناخت</p></div></div>