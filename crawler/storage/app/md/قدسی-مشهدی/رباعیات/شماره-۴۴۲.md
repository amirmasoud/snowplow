---
title: >-
    شمارهٔ ۴۴۲
---
# شمارهٔ ۴۴۲

<div class="b" id="bn1"><div class="m1"><p>هست از بن هر موی، مرا بر تن خویش</p></div>
<div class="m2"><p>دشمن‌کده‌ای به زیر پیراهن خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بستم کمر دشمنی خود به میان</p></div>
<div class="m2"><p>بازم سر دوستی‌ست با دشمن خویش</p></div></div>