---
title: >-
    شمارهٔ ۲۵ - در توصیف باغ جهان‌آرای اکبرآباد
---
# شمارهٔ ۲۵ - در توصیف باغ جهان‌آرای اکبرآباد

<div class="b" id="bn1"><div class="m1"><p>تعالی‌لله ازین باغ دل‌افروز</p></div>
<div class="m2"><p>که شامش راست فیض صبح نوروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوایش طبع‌ها را معتدل ساز</p></div>
<div class="m2"><p>درختان هم‌سر و مرغان هم‌آواز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرات از شرم باغ اکبرآباد</p></div>
<div class="m2"><p>چو گل اوراق خوبی داده بر باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر بر سبزه‌اش غلتیده کشمیر؟</p></div>
<div class="m2"><p>که باشد حسن سبزانش جهان‌گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در باغش صلای عام داده</p></div>
<div class="m2"><p>چو طاق ابروی خوبان، گشاده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مهرش داده فروردین دل از دست</p></div>
<div class="m2"><p>هزار اردیبهشت از بوی او مست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نوعی گلبن این باغ خوش‌بوست</p></div>
<div class="m2"><p>که گویی ریشه‌اش در ناف آهوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین گلشن، بتان هرجا نشستند</p></div>
<div class="m2"><p>به تار زلف، سنبل دسته بستند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به روی سبزه‌اش فرش ارجمندان</p></div>
<div class="m2"><p>تذرو سرو او، همت‌بلندان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درختانش به هم پیوسته مایل</p></div>
<div class="m2"><p>صنوبر عاشق سروش به صد دل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر سروش قد خوبان چه سنجد؟</p></div>
<div class="m2"><p>ز حرف راست باید کس نرنجد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نمی‌گوید بهای جلوه چندست</p></div>
<div class="m2"><p>دماغ سرو این گلشن بلندست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به جیب غنچه گر چاکی فتاده</p></div>
<div class="m2"><p>دری بر گلشن دیگر گشاده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بهار از خانه‌زادان حریمش</p></div>
<div class="m2"><p>نسیم از بی‌قراران قدیمش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز جویش آب حیوان تاب دارد</p></div>
<div class="m2"><p>که چندین خضر را سیراب دارد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هوایش می‌دهم از نم گواهی</p></div>
<div class="m2"><p>که می‌آید ز مرغان کار ماهی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نهال سیب او چون قامت یار</p></div>
<div class="m2"><p>نمی‌آرد به جز سیب ذقن بار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بلندی‌های سروش بد مبیناد</p></div>
<div class="m2"><p>که از پرواز قمری گشته آزاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دهد آواز مرغ این گلستان</p></div>
<div class="m2"><p>ز معجز، نغمه داوود را جان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ره دل‌ها چنان زد حسن آواز</p></div>
<div class="m2"><p>که بلبل بر سر گل می‌کند ناز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درین گلزار، بی گل نیست یک خار</p></div>
<div class="m2"><p>دمیده بلبلش را گل ز منقار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز گل افتاده چندان رنگ بر رنگ</p></div>
<div class="m2"><p>که جای ناله بر بلبل شده تنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گل افشرد آنچنان پهلوی بلبل</p></div>
<div class="m2"><p>که بلبل غنچه شد در پهلوی گل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در او آب بقا یک جویبارست</p></div>
<div class="m2"><p>هوایش را دم عیسی غبارست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز شوق نرگسش، چشم بتان مست</p></div>
<div class="m2"><p>به باد غنچه‌اش دل داده از دست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در آبش از قران ماه و ماهی</p></div>
<div class="m2"><p>دهد عکس گل و غنچه گواهی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شفق را لاله او سرخ‌رو کرد</p></div>
<div class="m2"><p>خضر از شبنمش می در سبو کرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هما در سایه بیدش نشسته</p></div>
<div class="m2"><p>که منشور شرف بر بال بسته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هوا گل را چنان سیراب دارد</p></div>
<div class="m2"><p>که برگ گل ز خود شبنم برآرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز عکس سبزه و سنبل درین باغ</p></div>
<div class="m2"><p>کند مژگان پر طاووس را داغ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چه شد گر چشم بت دل می‌رباید</p></div>
<div class="m2"><p>به چشم نرگس او درنیاید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جز این نشنیدم از بالای سروش</p></div>
<div class="m2"><p>که مرغ سدره می‌زیبد تذروش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز بس کوته بود از قامتش دست</p></div>
<div class="m2"><p>به صد تشویش قمری دل در او بست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز شوق حوضش از بس بود بی‌تاب</p></div>
<div class="m2"><p>چو نیلوفر، فلک سر بر زد از آب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نگردد تا فضایش ظلمت‌آلود</p></div>
<div class="m2"><p>چراغ لاله را در دل گره دود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نهان در زیر هر برگش بهاری</p></div>
<div class="m2"><p>ز شبنم هر گل او چشمه‌ساری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنان بر تازگی نخلش سوارست</p></div>
<div class="m2"><p>که گویی سایه‌اش ابر بهارست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>غزال آرد به سوی این گلستان</p></div>
<div class="m2"><p>برای چشم نرگس تحفه مژگان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز شرم بیدمشکش نافه در دشت</p></div>
<div class="m2"><p>چو مجنون همنشین آهوان گشت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دل قمری درین فردوس آباد</p></div>
<div class="m2"><p>نپردازد به سرو از حسن شمشاد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز هر جانب چو مجنونان خودرای</p></div>
<div class="m2"><p>فکنده بید مجنون، زلف در پای</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چه شد گر بید مجنون است موزون</p></div>
<div class="m2"><p>به موزونی بود مشهور، مجنون</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بود هرجا چو گل مسندنشینی</p></div>
<div class="m2"><p>به پهلویش چو نسرین نازنینی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به ساز و برگ خود خیری چه نازد</p></div>
<div class="m2"><p>که از صد ماه نو یک بدر سازد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>درین گلشن مجو از خس نشانه</p></div>
<div class="m2"><p>ز برگ گل نهد مرغ آشیانه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زبان شکوه اینجا بسته بلبل</p></div>
<div class="m2"><p>ملایم‌تر بود خار از رگ گل</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گل این بوستان را خار و خس نیست</p></div>
<div class="m2"><p>محبت خانه است اینجا هوس نیست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کف پا را کند خاکش حنایی</p></div>
<div class="m2"><p>که هست از لاله در شنجرف‌سایی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کشیده گرد او دیواری از گل</p></div>
<div class="m2"><p>سر دیوار او را خار، سنبل</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نمی‌باشد هوا چندین معطر</p></div>
<div class="m2"><p>مگر دارد چنارش گوی عنبر؟</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز بوی ارغوان، گل رفته از دست</p></div>
<div class="m2"><p>شراب ارغوانی داردش مست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز دل کیفیت تاکش برد هوش</p></div>
<div class="m2"><p>نگاه از دیدنش بی باده در جوش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>صبا کرد آتش گل را چنان تیز</p></div>
<div class="m2"><p>که شاخ گل شد آتشبار گلریز</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نماند از غایت سبزی درین باغ</p></div>
<div class="m2"><p>تفاوت در میان طوطی و زاغ</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گرو برده‌ست در افسانه گل</p></div>
<div class="m2"><p>زبان بلبل از آواز بلبل</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گل از بس گرمی بازار دارد</p></div>
<div class="m2"><p>عرق پیوسته بر رخسار دارد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>درین گلزار، پرکاری بود گل</p></div>
<div class="m2"><p>که پرگارش بود منقار بلبل</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سوادش چون سواد خط خوبان</p></div>
<div class="m2"><p>بود سیراب از چاه زنخدان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>فشاند بلبلش چون گرد گیسو</p></div>
<div class="m2"><p>ز بار منت افتد ناف آهو</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به خاکش هم گرفتارست بلبل</p></div>
<div class="m2"><p>که باشد گلبنش تا ریشه در گل</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چه باشد وسعت مشرب؟ فضایش</p></div>
<div class="m2"><p>مسیحا کیست؟ شاگرد هوایش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز گل، نَرد شکفتن برده خارش</p></div>
<div class="m2"><p>خزان را دست کوتاه از بهارش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>درین باغ ار شود جبریل نازل</p></div>
<div class="m2"><p>بماند چون نهالش پای در گل</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>درین گلشن که شد از جان سرشته</p></div>
<div class="m2"><p>ز بس نازک‌مزاجی عام گشته</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شود گر برگ گل دمساز بلبل</p></div>
<div class="m2"><p>نخیزد بی خراش آواز بلبل</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>درین بستان ز شرم سرو آزاد</p></div>
<div class="m2"><p>زلیخا را گریزد یوسف از یاد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>زده بر سرو، پهلو، بوته گل</p></div>
<div class="m2"><p>شده هم‌آشیان قمریّ و بلبل</p></div></div>
<div class="b" id="bn68"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>
<div class="b2" id="bn69"><p>***</p></div>
<div class="b" id="bn70"><div class="m1"><p>مرا باغ جهان‌آرا بهشت است</p></div>
<div class="m2"><p>بهشت این باغ باشد، ورنه زشت است</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گلشن را باغبان تا دسته بسته</p></div>
<div class="m2"><p>به یوسف مانده بازار شکسته</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>هوایش در کمال اعتدال است</p></div>
<div class="m2"><p>کمال اعتدالش بی‌زوال است</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ز نخل این چمن، شاخ فکنده</p></div>
<div class="m2"><p>بماند جاودان چون خضر، زنده</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به صد منت ز روی این گلستان</p></div>
<div class="m2"><p>قضا برداشت طرح باغ رضوان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>درین گلشن ندارد قدر خاشاک</p></div>
<div class="m2"><p>ارم گو از خیابان سینه کن چاک</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>برای چشم‌زخم این گلستان</p></div>
<div class="m2"><p>سپند از خال حور آورده رضوان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>قدم بیرون منه زنهار ازین باغ</p></div>
<div class="m2"><p>که دارد عالمی را لاله‌اش داغ</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>هوای این گلستان صحّت‌افزاست</p></div>
<div class="m2"><p>نسیم اینجا هوادار مسیحاست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بهار این گلستان بی زوال است</p></div>
<div class="m2"><p>شکست رنگ گل اینجا محال است</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>هوایی بس ملایم‌تر ز مرهم</p></div>
<div class="m2"><p>نیارد زخم گل را چون فراهم؟</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>مسیحا روز و شب در کار اینجا</p></div>
<div class="m2"><p>چرا نرگس بود بیمار اینجا؟</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>مگو قمری به سروش گشته پابست</p></div>
<div class="m2"><p>بود خضری، عصای سبز در دست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بدین گلشن بود این نه چمن را</p></div>
<div class="m2"><p>همان ربطی که با جان است تن را</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>زبانم این چمن را تا ثناگوست</p></div>
<div class="m2"><p>نمی‌گنجد ز گل چون غنچه در پوست</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>خیال سنبلش تا نقش بستم</p></div>
<div class="m2"><p>قلم، شد دسته سنبل به دستم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>میان گلبن او شد خرد گم</p></div>
<div class="m2"><p>ببستم لب چو غنچه از تکلم</p></div></div>
<div class="b2" id="bn87"><p>***</p></div>
<div class="b" id="bn88"><div class="m1"><p>گل و شمشاد باغ شاه عادل</p></div>
<div class="m2"><p>دوانَد چون محبت ریشه در دل</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چراغ دوده گیتی ستانی</p></div>
<div class="m2"><p>بهار گلشن صاحبقرانی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>نهال بوستان سرفرازی</p></div>
<div class="m2"><p>شهاب‌الدین محمد شاه غازی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>برای خطبه نامش مکرر</p></div>
<div class="m2"><p>ملایک کرده‌اند از بال، منبر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ز رویش مهر انور، شرمگینی</p></div>
<div class="m2"><p>ز رنگش خرمن گل خوشه‌چینی</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ز عدل او جهان گردیده آباد</p></div>
<div class="m2"><p>به دیدارش دل خلق جهان شاد</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>کند طغرای جودش را چو انشا</p></div>
<div class="m2"><p>به آب زر، قلم شوید زبان را</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ز سوادی نثارش در ته آب</p></div>
<div class="m2"><p>نیاسود از تپش گوهر چو سیماب</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>کرم را داد دستی از هر انگشت</p></div>
<div class="m2"><p>کفش را پنج دریا جمع در مشت</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>برای خنده دارد کبک طناز</p></div>
<div class="m2"><p>به عهدش داغ‌ها از سینه باز</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>هوای خدمتش چون در سر آرند</p></div>
<div class="m2"><p>چو هدهد تاج‌داران پر برآرند</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>نه تنها با درم دارد نگین را</p></div>
<div class="m2"><p>گرفته نام او روی زمین را</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>سزد کز نام شاهنشاه عالم</p></div>
<div class="m2"><p>چو نقش زر ببالد نقش خاتم</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>کفش پیمانه دریای اکرام</p></div>
<div class="m2"><p>لباس خاص لطفش، رحمت عام</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>سخن را تازگی از دولت اوست</p></div>
<div class="m2"><p>بلندی‌های شعر از همت اوست</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>به دریای عطایش بهر تقسیم</p></div>
<div class="m2"><p>ز ماهی مضطرب‌تر بدره سیم</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>سخن را بازگردانم عماری</p></div>
<div class="m2"><p>به سوی باغ چون باد بهاری</p></div></div>
<div class="b2" id="bn105"><p>***</p></div>
<div class="b" id="bn106"><div class="m1"><p>درین گلشن اساسی بود عالی</p></div>
<div class="m2"><p>که چرخ افتاده بودش بر حوالی</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بنایی توامان با چرخ اعظم</p></div>
<div class="m2"><p>به دیوارش بقا را بست محکم</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>ز گردون گوی همدوشی ربوده</p></div>
<div class="m2"><p>به رفعت چرخ‌ها چرخش ستوده</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>بهشت از شرم حسنش ناپدیدار</p></div>
<div class="m2"><p>برش بتخانه چین نقش دیوار</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>در کعبه درش را حلقه در گوش</p></div>
<div class="m2"><p>دو عالم چارچوبش را در آغوش</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>ز گلمیخ درش خورشید در تاب</p></div>
<div class="m2"><p>دو پیکر، پیکرش را کرده محراب</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>دل از زلفین و زنجیرش چنان شاد</p></div>
<div class="m2"><p>که چشم و زلف دلبر رفته از یاد</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>صفای این عمارت شد چنان عام</p></div>
<div class="m2"><p>که می‌کرد اصفهان از او صفا وام</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>ز افتادن چو حسنش را بود عار</p></div>
<div class="m2"><p>نمی‌دانم که چون افتاده پرکار؟</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>گذشته برج او را از فلک سر</p></div>
<div class="m2"><p>ملایک گشته برجش را کبوتر</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>قضا از حسن محضش آفریده</p></div>
<div class="m2"><p>کسش در هیچ آن، بی آن ندیده</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>چو در طرحش به خاکستر فتد کار</p></div>
<div class="m2"><p>نویسد بر صفاهان سرمه، معمار</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چو با قصر فلک گردد هم‌آغوش</p></div>
<div class="m2"><p>نداند کس، که را برتر بود دوش</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>برای دفع چشم زخم مردم</p></div>
<div class="m2"><p>سپند پیش طاقش گشته انجم</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>صفای طاق این زیبا عمارت</p></div>
<div class="m2"><p>چو ابروی بتان دل کرده غارت</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>هوس اینجا بود با عشق، هم‌سنگ</p></div>
<div class="m2"><p>کز استحکام منزل نشکند رنگ</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>در استحکام این پاینده ایوان</p></div>
<div class="m2"><p>بقا چون صورت دیوار، حیران</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>اگر زین در غباری خیزد از جای</p></div>
<div class="m2"><p>بماند سال‌ها چون چرخ بر پای</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>رسانده استواری را به معراج</p></div>
<div class="m2"><p>بقا را برج او بر سر بود تاج</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>در و دیوار او آشوب چین است</p></div>
<div class="m2"><p>اگر نقشی ز مانی مانده، این است</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>ز قدر او سخت چون برشمارم</p></div>
<div class="m2"><p>بلندی‌های فکر آید به کارم</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>کسی کاین جا میسر شد سجودش</p></div>
<div class="m2"><p>سپهر آیینه زانو نمودش</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>نظر چون در تماشایش کنم باز</p></div>
<div class="m2"><p>کند پیش از نگاهم دیده پرواز</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>در و دیوار آن باشد منور</p></div>
<div class="m2"><p>مگر ز آیینه زد خشتش سکندر؟</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>بنایی کاین چنین افکند معمار</p></div>
<div class="m2"><p>سزد آنجا سکندر گر کند کار</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>به این منزل بود روشن زمانه</p></div>
<div class="m2"><p>که چشم است این و عالم چشم‌خانه</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>ازین دولت‌سرا، دولت به کام است</p></div>
<div class="m2"><p>جهان را بخت بیدار این مقام است</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>چنین جایی ندارد یاد، دوران</p></div>
<div class="m2"><p>تو گر داری گمان، گوی است و چوگان</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>ز قدر این عمارت چند پرسی؟</p></div>
<div class="m2"><p>تماشا کن به عجز عرش و کرسی</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>الهی این بنا معمور بادا</p></div>
<div class="m2"><p>چو چشم بد غم از وی دور بادا</p></div></div>
<div class="b2" id="bn136"><p>***</p></div>
<div class="b" id="bn137"><div class="m1"><p>ز باغ افتاده بحری بر کرانه</p></div>
<div class="m2"><p>که یک مدّش بود طول زمانه</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>فلک از جزر و مدّش در کشاکش</p></div>
<div class="m2"><p>ز رشک موج او بر روی آتش</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>ببندد قطره او گر میان چست</p></div>
<div class="m2"><p>تواند هفت دریا را ورق شست</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>چه گویم وصف این دریا که بینی</p></div>
<div class="m2"><p>جز این، کز آسمان آید زمینی</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>چو آگه شد ز بحر اکبرآباد</p></div>
<div class="m2"><p>ز قید دجله شد آزاد بغداد</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>چه دریا منبع آب حیاتی</p></div>
<div class="m2"><p>به سویش بازگشت هر فراتی</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>حبابش برده گوی دلربایی</p></div>
<div class="m2"><p>ز پستان نکویان ختایی</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>ز فیلان نهر را هر سو شکافی</p></div>
<div class="m2"><p>خیابانی به راه کوه قافی</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>عیان از سادگی راز نهانش</p></div>
<div class="m2"><p>شده افتادگی پای روانش</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>ز جیبش گرچه گوهر آشکارست</p></div>
<div class="m2"><p>چو دلق بینوایان بی‌نگارست</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>نهاده پشت راحت گرچه بر خاک</p></div>
<div class="m2"><p>همان در گردش است از چشم نمناک</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>ز ابرو کرده کشتی گلرخانش</p></div>
<div class="m2"><p>چنان کز پرده دل بادبانش</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>ز کشتی موج دریا نیست پیدا</p></div>
<div class="m2"><p>که کشتی هست بیش از موج دریا</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>همه مردم‌نشین چون خانه چشم</p></div>
<div class="m2"><p>به خوبی غیرت کاشانه چشم</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>به هر کشتی نشسته چند یاری</p></div>
<div class="m2"><p>شده هر ماه نو خورشیدزاری</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>به گاه سیر این بحر معظّم</p></div>
<div class="m2"><p>چو در کشتی نشیند شاه عالم</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>ز بس بر خویش بالد آب دریا</p></div>
<div class="m2"><p>شود هم‌عِقد، گوهر با ثریا</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>نه کشتی یافت بر پابوس شه دست</p></div>
<div class="m2"><p>مه نو خویش را بر آسمان بست</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>مگر چشم ملایک بود در خواب؟</p></div>
<div class="m2"><p>که کشتی گوی فرصت زد درین باب</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>نشست آن نوگل باغ بهشتی</p></div>
<div class="m2"><p>به سان توتیا در چشم کشتی</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>ز شوق از بس که دریا رفت بالا</p></div>
<div class="m2"><p>ز مرغابی فلک نشناخت خود را</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>چو شد کشتی‌نشین آن بحر خوبی</p></div>
<div class="m2"><p>حسد بر چوب کشتی برد طوبی</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>ز رشک بحر شد پرخون، دل بر</p></div>
<div class="m2"><p>که از خشکی به دریا رفت گوهر</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>چه کشتی، منبع دریای رحمت</p></div>
<div class="m2"><p>ز فیض عالمی جویای رحمت</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>ز شوق این بحر یا رب در چه کارست</p></div>
<div class="m2"><p>که بر وی ابر رحمت را گذارست</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>چو سیرش سوی کشتی رهنمون شد</p></div>
<div class="m2"><p>ز دریا تلخی و شوری برون شد</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>صدف بر گرد کشتی گوهر افشاند</p></div>
<div class="m2"><p>حباب از تن سر و ماهی زر افشاند</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>ز بحر آن گوهر خوبی گذر کرد</p></div>
<div class="m2"><p>سراسر آب دریا را گوهر کرد</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>ز ملّاحان آن کشتی چه پرسی</p></div>
<div class="m2"><p>که هریک حامل عرشند و کرسی</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>به دریا گرچه کشتی بی‌شمارست</p></div>
<div class="m2"><p>زهی دریا که بر کشتی سوارست</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>ندانم این سفینه از چه باب است</p></div>
<div class="m2"><p>که جای شاه‌بیت انتخاب است</p></div></div>
<div class="b2" id="bn168"><p>***</p></div>
<div class="b" id="bn169"><div class="m1"><p>سحر چون عندلیب آمد به فریاد</p></div>
<div class="m2"><p>هوای باغ بازم در سر افتاد</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>بهشتی را که چندین یاد کردم</p></div>
<div class="m2"><p>به مدحش عالمی را شاد کردم</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>بگویم کز که بود و از کجا خاست</p></div>
<div class="m2"><p>که این فردوس را این گونه آراست</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>ز ابر رحمتی بود این گلستان</p></div>
<div class="m2"><p>که بر وی ریختی گوهر چو باران</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>غلامان درش کیوان و برجیس</p></div>
<div class="m2"><p>کنیزش را کنیزی کرده بلقیس</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>فلک بر درگهش چون حلقه میم</p></div>
<div class="m2"><p>ز مهدش مهر و مه، گوی زر و سیم</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>ز مژگان گرد چشمش فوج عصمت</p></div>
<div class="m2"><p>نظرهای بلندش اوج عصمت</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>به درج پادشاهی گوهری داشت</p></div>
<div class="m2"><p>بلند اقبال نیکو اختری داشت</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>ندیده سایه او را فرشته</p></div>
<div class="m2"><p>ز نور رحمتش یزدان سرشته</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>بود هرکس مثل در پارسایی</p></div>
<div class="m2"><p>ز چشم او کند عصمت گدایی</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>ز شرم او چنان آیینه شد آب</p></div>
<div class="m2"><p>که از دستش حنا را شست سیلاب</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>ملایک فرش بال آرند ز افلاک</p></div>
<div class="m2"><p>که مهدش را نیفتد سایه بر خاک</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>..............................</p></div>
<div class="m2"><p>به غیر از عصمت از عصمت چه آید؟</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>به او آن باغ را تملیک فرمود</p></div>
<div class="m2"><p>که پروازش سوی باغ دگر بود</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>کلید باغ را پیشش فرستاد</p></div>
<div class="m2"><p>که باشد باغ، ملک سرو آزاد</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>به گل داد اختیار گلستان را</p></div>
<div class="m2"><p>پس آنگه خود به جانان داد جان را</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>برون شد زین جهان پرندامت</p></div>
<div class="m2"><p>شفیعش باد خاتون قیامت</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>پس آنگه آن نهال مهرپرور</p></div>
<div class="m2"><p>که گلشن را بدو بخشید مادر</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>قبول باغ کرد از مادر خویش</p></div>
<div class="m2"><p>به گلبن، گل فرود آرد سر خویش</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>نهاد آن شاهزاده بهر تعظیم</p></div>
<div class="m2"><p>به دست خویش بر سر تاج تسلیم</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>نرفته این گلستان جای دوری</p></div>
<div class="m2"><p>بهشتی را به حوری داده حوری</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>زهی خاک جنابت تاج فغفور</p></div>
<div class="m2"><p>غبار آستانت سرمه حور</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>کسی نشنیده دولتمند چون تو</p></div>
<div class="m2"><p>پدر این، مادر آن، فرزند چون تو</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>به دولت باد دایم بارگاهت</p></div>
<div class="m2"><p>بود لطفا پدر پشت و پناهت</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>الهی تا بود گلزار عالم</p></div>
<div class="m2"><p>ز آب خضر باد این باغ خرم</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>فضایش سیرگاه گلرخان باد</p></div>
<div class="m2"><p>برِ رو نوبر این بوستان باد</p></div></div>