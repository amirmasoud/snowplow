---
title: >-
    شمارهٔ ۳۱ - تعریف توکل و قصه رهزن فقیر شده و حال بت‌پرست تارک دنیا
---
# شمارهٔ ۳۱ - تعریف توکل و قصه رهزن فقیر شده و حال بت‌پرست تارک دنیا

<div class="b" id="bn1"><div class="m1"><p>زنده‌دلی بهر تماشای هند</p></div>
<div class="m2"><p>رفت ز کشمیر به اقصای هند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راهزنی دید، شده خرقه‌پوش</p></div>
<div class="m2"><p>لب به جز از ذکر الهی خموش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پختگی از هر طرف آموخته</p></div>
<div class="m2"><p>چون نفس از گام زدن سوخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وادی تجرید شده منزلش</p></div>
<div class="m2"><p>رنگ تعلق نه در آب و گلش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رایحه‌ای از نفسش مشک ناب</p></div>
<div class="m2"><p>برگ گلی از چمنش آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در همه دل کرده چو اندیشه جا</p></div>
<div class="m2"><p>با همه چشمی چو نگاه آشنا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فسق به تقویش مبدل شده</p></div>
<div class="m2"><p>آرزوی نفس معطل شده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسته دلش بر کمر از توبه کیش</p></div>
<div class="m2"><p>عزم جدالش به جدل‌های پیش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از عمل خویش گرفته کنار</p></div>
<div class="m2"><p>شسته سیاهی ز بدن صبح‌وار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرده به العفو بدل الصبوح</p></div>
<div class="m2"><p>چیده گل توبه ز باغش نصوح</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از می حق، مست اناالحق شده</p></div>
<div class="m2"><p>نیستی‌اش هستی مطلق شده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شسته ز آلودگی نفس، دست</p></div>
<div class="m2"><p>ماهی توفیق فکنده به شست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سوخته اعمال بد خویش را</p></div>
<div class="m2"><p>ساخته مرهم جگر ریش را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنچه توان گفت ز بد کان شده</p></div>
<div class="m2"><p>کرده و از کرده پشیمان شده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر زره کینه، تغافل‌فروش</p></div>
<div class="m2"><p>خرقه رحمت چو سحابش به دوش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دانه تسبیح ز مژگان تر</p></div>
<div class="m2"><p>در کفش از آبله سیراب‌تر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تافته رو از همه کس بی ریا</p></div>
<div class="m2"><p>وز دو جهان، روی به سوی خدا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کرده سر کوه ندامت مقام</p></div>
<div class="m2"><p>آمده قانع به حلال از حرام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دید جوان زنده‌دلش خیره ماند</p></div>
<div class="m2"><p>وز روش روشن او تیره ماند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفت به رهزن که چه حال است این</p></div>
<div class="m2"><p>با همه نقصان، چه کمال است این</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سوی ورع گشت که رهبر تو را؟</p></div>
<div class="m2"><p>وز چه شد این ملک مسخر تو را؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیشه تو راهزنی بود و بس</p></div>
<div class="m2"><p>بال خود از شهد تو شستی مگس</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گشته‌ای از تیغ به تسبیح شاد</p></div>
<div class="m2"><p>سبحه و تیغت که گرفته و که داد؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قاید راه تو درین ره که شد؟</p></div>
<div class="m2"><p>مشتری جنس تو در چه که شد؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بادِ که افشاند بهار تو را؟</p></div>
<div class="m2"><p>سنگ که زد شیشه کار تو را؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نخل تو را بود جز آتش حرام</p></div>
<div class="m2"><p>گلشن قدسش ز چه رو شد مقام؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>راهزن از وی چو شنید این مقال</p></div>
<div class="m2"><p>دُر ز صدف ریخت به تقریر حال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت که روزی به هوای درم</p></div>
<div class="m2"><p>دربدرم داشت سراغ کرم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قامت خود چون علم افراختم</p></div>
<div class="m2"><p>وز مژه چون خامه قدم ساختم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کس خبر از کعبه جودم نداد</p></div>
<div class="m2"><p>راه به بتخانه بخلم فتاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از در بتخانه درون آمدم</p></div>
<div class="m2"><p>بی درمی یافت که چون آمدم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خانه‌ای از سیم و زر آراسته</p></div>
<div class="m2"><p>بیشتر از خواسته، ناخواسته</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>رشک خم باده ز یاقوت ناب</p></div>
<div class="m2"><p>روزن او، طعنه‌زن آفتاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از زر و سیمش در و دیوار پر</p></div>
<div class="m2"><p>همچو صدف فرش زمینش ز دُر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آب گهر گر حرکت داشتی</p></div>
<div class="m2"><p>ساحتش از سیل بینباشتی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بود در آن خانه بتی از رخام</p></div>
<div class="m2"><p>برهمنی برده به پیشش قیام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ناخنی از پنجه تواناترش</p></div>
<div class="m2"><p>سلسله پا شده موی سرش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رشته جام ساخته زنار او</p></div>
<div class="m2"><p>محض توجه شده در کار او</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دل ز خیال همه پرداخته</p></div>
<div class="m2"><p>عشق بتی را بت خود ساخته</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بند تحیر زده بر پا و دست</p></div>
<div class="m2"><p>بی‌حرکت مانده چو بت، بت‌پرست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گفتمش ای بر سر این گنج امیر</p></div>
<div class="m2"><p>با قدری سیم و زرم دست گیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رخ ز غم زر شده چون زر مرا</p></div>
<div class="m2"><p>مفلسی آورده بدین در مرا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>من ز فراق درمم خوار و زار</p></div>
<div class="m2"><p>خفته تو بر روی درم سکه‌وار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بخل مکن پیشه به دلسوزی‌ام</p></div>
<div class="m2"><p>بر تو نوشته‌ست قضا روزی‌ام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>عشق درم در دلم افکنده شور</p></div>
<div class="m2"><p>گر تو نبخشی، بستانم به زور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کیسه تهی، دست تهی، دل تهی</p></div>
<div class="m2"><p>نیست در افلاس مرا کوتهی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>حسرت زرهای توام کرده داغ</p></div>
<div class="m2"><p>ساخته روشن طمعم را چراغ</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>من به سوال از وی و او در جواب</p></div>
<div class="m2"><p>لب ز سخن شسته به هفتاد آب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کرده سکوت ابدی اختیار</p></div>
<div class="m2"><p>همچو زبانی که بیفتد ز کار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>جامه چو بر قد سوالم ندوخت</p></div>
<div class="m2"><p>چهره‌ام از آتش کین برفروخت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا به غضب تیغ برافراشتم</p></div>
<div class="m2"><p>تخم وجودش به عدم کاشتم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بر قفسش تیغ چو روزن گشاد</p></div>
<div class="m2"><p>مرغ دلش در قدم بت فتاد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>داعیه کردم که ببینم دلش</p></div>
<div class="m2"><p>تا چه شد از سجده بت حاصلش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دست چو بردم به دل بت‌پرست</p></div>
<div class="m2"><p>جای دل او بتم آمد به دست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بس که دلش واله و حیران شده</p></div>
<div class="m2"><p>آینه صورت جانان شده</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>آینه‌اش لیک هم‌آغوش زنگ</p></div>
<div class="m2"><p>عکس در او مانده چو صورت به سنگ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بر دلش افتاد مرا چون نظر</p></div>
<div class="m2"><p>آتش غیرت ز دلم کرد سر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تیغ فکندم ز میان در زمان</p></div>
<div class="m2"><p>دامن پرهیز زدم بر میان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>درصدد ترک مناهی شدم</p></div>
<div class="m2"><p>محرم توفیق الهی شدم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کم ز برهمن نه ای، ای خودپرست</p></div>
<div class="m2"><p>دامن حق را نگذاری ز دست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چند چو بهمان و فلان زیستن؟</p></div>
<div class="m2"><p>کم ز برهمن نتوان زیستن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ای به گمان خوش که مگر عاقلی</p></div>
<div class="m2"><p>غافلی از خود، که عجب غافلی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بر هوس خود چو شکست آوری</p></div>
<div class="m2"><p>دامن معشوق به دست آوری</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گرچه به هر حرف نهد خامه سر</p></div>
<div class="m2"><p>لیکن ازان حرف ندارد خبر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>واله معشوق شو آیینه‌وار</p></div>
<div class="m2"><p>کز تو شود صورت او آشکار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چشمه فیض از دل دانا طلب</p></div>
<div class="m2"><p>گوهر سیراب ز دریا طلب</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نغمه ناهید ز ناهید پرس</p></div>
<div class="m2"><p>راه به خورشید، ز خورشید پرس</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شعله نماید به خود از نور خویش</p></div>
<div class="m2"><p>راه به پروانه مهجور خویش</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تا نکند مرغ، غلط، راه باغ</p></div>
<div class="m2"><p>هر طرف افروخته گل صد چراغ</p></div></div>