---
title: >-
    شمارهٔ ۸ - تعریف باغ فیض‌بخش
---
# شمارهٔ ۸ - تعریف باغ فیض‌بخش

<div class="b" id="bn1"><div class="m1"><p>ز باغ فیض‌بخشم دل بود شاد</p></div>
<div class="m2"><p>کز ایام جوانی می‌دهد یاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حصاری گرد این گلشن کشیدند</p></div>
<div class="m2"><p>ز گوهر، مهره دیوار چیدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو محراب درش را سرو دیده</p></div>
<div class="m2"><p>موذن‌وار قامت برکشیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شوخی، سبزه‌اش پیش از دمیدن</p></div>
<div class="m2"><p>نیاساید ز مشق قد کشیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس برگ تماشا می‌کند ساز</p></div>
<div class="m2"><p>بود نارسته، چشم نرگسش باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوایش می‌زند از تازگی دم</p></div>
<div class="m2"><p>به روی سبزه می‌غلتد چو شبنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هر جانب نظر از دیده رفتی</p></div>
<div class="m2"><p>به روی برگ گل غلتیده رفتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گلش را چون برد محمل کش باد</p></div>
<div class="m2"><p>شقایق چون جرس آید به فریاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز تاثیر هوا در سایه گل</p></div>
<div class="m2"><p>رود تا ناف آهو بیخ سنبل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز خاک این چمن گر پر کنی مشت</p></div>
<div class="m2"><p>گلی روید چو نرگس از هر انگشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز هر جانب نسیم از غنچه تر</p></div>
<div class="m2"><p>گشوده حقه‌های بوسه را سر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به سیر سنبلش چون خیزم از جای</p></div>
<div class="m2"><p>کنم وام از سر زلف بتان، پای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز شوخی آنچنان گردیده گستاخ</p></div>
<div class="m2"><p>که پیش از وعده می‌روید گل از شاخ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>میاور گو سیاهی لاله از داغ</p></div>
<div class="m2"><p>که خط سبز خواهد قطعه باغ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گل این باغ، دلتنگی ندیده</p></div>
<div class="m2"><p>ز گلبن، غنچه چون بلبل پریده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به وصفش تا کشم بر صفحه مدّی</p></div>
<div class="m2"><p>شود هر نونهالش، سرو قدّی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به مدحش سر کنم تا داستانی</p></div>
<div class="m2"><p>شود هر طفل این گلشن، جوانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به صنعت باغبانانش چو خیزند</p></div>
<div class="m2"><p>ز رنگ گل، چمن‌ها رنگ ریزند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شکفتن آشیان بسته‌ست بر شاخ</p></div>
<div class="m2"><p>که کی بیرون خرامد غنچه از کاخ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بهشتش می‌نوشتی خامه غیب</p></div>
<div class="m2"><p>تنزل گر نبودی در ثنا عیب</p></div></div>