---
title: >-
    شمارهٔ ۴ - تعریف باغ و بهار و سرسبزی کشمیر
---
# شمارهٔ ۴ - تعریف باغ و بهار و سرسبزی کشمیر

<div class="b" id="bn1"><div class="m1"><p>چرا افسرده‌ای قدسی و دل‌گیر؟</p></div>
<div class="m2"><p>نظر بگشای، کشمیرست، کشمیر!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تماشا کن که هنگام تماشاست</p></div>
<div class="m2"><p>خریدار متاع عین، اینجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زند مرغ چمن هر سو منادی</p></div>
<div class="m2"><p>که فصل گل، بود ایام شادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر دیوارش از گل رشک چین است</p></div>
<div class="m2"><p>سر سبزی که می‌گویند، این است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جای سبزه، در دامان کهسار</p></div>
<div class="m2"><p>کشیده سرو، سر بر چرخ دوّار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوایی بلبلش در چنگ دارد</p></div>
<div class="m2"><p>که در یک پرده صد آهنگ دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درختانش ز بس دارند آزرم</p></div>
<div class="m2"><p>چنارش ساق خود پوشیده از شرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گلش را یک به یک می‌بردمی نام</p></div>
<div class="m2"><p>زبان را گر بقا می‌بود در کام</p></div></div>