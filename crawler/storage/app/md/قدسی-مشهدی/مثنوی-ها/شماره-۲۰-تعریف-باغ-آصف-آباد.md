---
title: >-
    شمارهٔ ۲۰ - تعریف باغ آصف‌آباد
---
# شمارهٔ ۲۰ - تعریف باغ آصف‌آباد

<div class="b" id="bn1"><div class="m1"><p>چو آمد سوی باغ آصف‌آباد</p></div>
<div class="m2"><p>سلیمان ملک خود را رونما داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به آبش، آب زمزم چون ستیزد؟</p></div>
<div class="m2"><p>که این از چشمه، آن از چاه خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قرین می‌گشت با این چشمه، زمزم</p></div>
<div class="m2"><p>اگر می‌بود در کشمیر، آن هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی‌باشد گواراتر ازین آب</p></div>
<div class="m2"><p>نوشته خضر، صد محضر درین باب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به صافی، صاف‌تر از ماه بی‌میغ</p></div>
<div class="m2"><p>گرو برده به سردی از دم تیغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دل فیض روانی می‌چشاند</p></div>
<div class="m2"><p>که در صافی به شعر صاف ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زند چون چشمه جوش از سردی آب</p></div>
<div class="m2"><p>نماند بر فلک خورشید را تاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آشامیدن این رشک کوثر</p></div>
<div class="m2"><p>بود هر گام، خضرآباد دیگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز مشرق تا به مغرب گر شتابی</p></div>
<div class="m2"><p>چنین سرچشمه‌ای، دیگر نیابی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود سرچشمه تسنیم و کوثر</p></div>
<div class="m2"><p>ز فیض باغ رضوان تازه و تر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همین آب است آب زندگانی</p></div>
<div class="m2"><p>برو از خضر بشنو گر ندانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درین چشمه نماید عکس زنگی</p></div>
<div class="m2"><p>چو در آیینه‌ها عکس فرنگی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بود بر خاک حیف این رشک زمزم</p></div>
<div class="m2"><p>به روی سبزه می‌زید چو شبنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شبم روشن بود زین چشمه آب</p></div>
<div class="m2"><p>به بر گو تیرگی را گرد مهتاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز شوقش چشمه‌سار کوه الوند</p></div>
<div class="m2"><p>رساند اشک حسرت تا دماوند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز شرمش آب حیوان را جبین تر</p></div>
<div class="m2"><p>دهد باج گواراییش، کوثر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بود برّنده‌تر از آب شمشیر</p></div>
<div class="m2"><p>مخور این آب تا از نان شوی سیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نباشد هیچ‌کس بی‌بهره زین آب</p></div>
<div class="m2"><p>بیا گو سلسبیل و فیض دریاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بود برّندگی سر خیل فوجش</p></div>
<div class="m2"><p>بر این برهان قاطع تیغ موجش</p></div></div>