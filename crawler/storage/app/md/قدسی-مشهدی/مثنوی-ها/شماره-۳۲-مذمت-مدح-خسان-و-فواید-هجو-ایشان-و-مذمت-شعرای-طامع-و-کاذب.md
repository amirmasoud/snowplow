---
title: >-
    شمارهٔ ۳۲ - مذمّت مدح خسان و فواید هجو ایشان و مذمّت شعرای طامع و کاذب
---
# شمارهٔ ۳۲ - مذمّت مدح خسان و فواید هجو ایشان و مذمّت شعرای طامع و کاذب

<div class="b" id="bn1"><div class="m1"><p>دوش به رسواشدن عالمی</p></div>
<div class="m2"><p>بود سرم بر سر زانو دمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناخن طبعم پی مضمون بکر</p></div>
<div class="m2"><p>عقده‌گشا گشت ز گیسوی فکر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معنی باریک گزیدم بسی</p></div>
<div class="m2"><p>چون مژه، مو در خره چیدم بسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب همه شب خاک هجا بیختم</p></div>
<div class="m2"><p>بر سر هرکس قدری ریختم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاعر هاجی ز ثناگر به است</p></div>
<div class="m2"><p>تیزی شمشیر ز جوهر به است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بود از مدح، خسان را هجا</p></div>
<div class="m2"><p>بهر تردد نبود سر چو پا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شعله چو ساکن شود، افسرده دان</p></div>
<div class="m2"><p>زنده که خونش نبود، مرده دان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آهن آیینه چو افتد ز نور</p></div>
<div class="m2"><p>کس نکند فرق ز نعل ستور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز به هجا، کلک سزاوار نیست</p></div>
<div class="m2"><p>مار که زهرش نبود مار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشئه دهد انجم و افلاک را</p></div>
<div class="m2"><p>زان رگ تلخی که بود تاک را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گلبن ازان روز که سر پیش کرد</p></div>
<div class="m2"><p>تربیت خار ز گل بیش کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تلخی من در سخن آید به کار</p></div>
<div class="m2"><p>خوش نبود باده شیرین گوار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرکه خورد مشتم و گوید سخن</p></div>
<div class="m2"><p>مشت خورد بار دگر بر دهن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیم کُش از خاک چو برداشت سر</p></div>
<div class="m2"><p>کرد تقاضا پی تیغ دگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مار طبیعت که ندارد شرنگ</p></div>
<div class="m2"><p>فرق چه زو تا به طناب دو رنگ؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روی طبیعت ز سخن برمتاب</p></div>
<div class="m2"><p>نور بود ماحصل آفتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر قلمم دست منه زینهار</p></div>
<div class="m2"><p>زهر بود در بن دندان مار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پیشتر از خصم به تندی مکوش</p></div>
<div class="m2"><p>آتش اگر برنفروزد، مجوش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زان که دهد زاده خود را به باد</p></div>
<div class="m2"><p>حامله چون پیشتر از وعده زاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لیک تو هم خصم چو افکند تیر</p></div>
<div class="m2"><p>ضربت تیغ از سر او وا مگیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دشمن اگر کوه شود زو ملنگ</p></div>
<div class="m2"><p>تیغ زبان رخنه نگردد ز سنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کوه که تمکین بود از وی صواب</p></div>
<div class="m2"><p>عیب نداند سبکی در جواب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باده ز تلخی کند آشوب را</p></div>
<div class="m2"><p>ارّه به دندانه بُرد چوب را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کوهکنان را نبود غم ز جنگ</p></div>
<div class="m2"><p>شیشه‌گران راست غم از جنگ سنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تیغ زبان را چو قلم ساز تیز</p></div>
<div class="m2"><p>یا چو زبان در پس دندان گریز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نظم تو را هجو بود پاسبان</p></div>
<div class="m2"><p>پیرهن مغز بود استخوان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جز به هجا نظم نیابد نظام</p></div>
<div class="m2"><p>زان که شود پنجه به ناخن تمام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سر کنم اول ز گروهی سخن</p></div>
<div class="m2"><p>طایفه‌ای زشت، نه مرد و نه زن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رفته ز چشم همه چون شیشه آب</p></div>
<div class="m2"><p>کرده شکم‌ها چو سبو پر شراب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زن نه و چون زن همه دنبال زیب</p></div>
<div class="m2"><p>آب نه و رفته همه رو به شیب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>باد چه؟ مشّاطه گیسویشان</p></div>
<div class="m2"><p>خاک چه، سیلی‌خور زانویشان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بس که چو نی هرکسشان داده دم</p></div>
<div class="m2"><p>کرده شکمشان چو نی انبان، ورم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آب حیا رفته ز رخسارشان</p></div>
<div class="m2"><p>لای قدح آب رخ کارشان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شیشه قاروره نه و دم‌به‌دم</p></div>
<div class="m2"><p>کرده ز بول دگری پر، شکم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شب همه شب چون هوس می کنند</p></div>
<div class="m2"><p>راه چو کشتی به شکم طی کنند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خوار به چشم همه کس چون غبار</p></div>
<div class="m2"><p>دیده چو عینک دو، ولی رو چهار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مرده هم خورده به رغبت چو گور</p></div>
<div class="m2"><p>پخته، ولی خام خورش چون تنور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رسم فتادن شد از ایشان پدید</p></div>
<div class="m2"><p>چرخ ز افتادن ایشان خمید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جور و جفا عام شد از کینه‌شان</p></div>
<div class="m2"><p>رسم وفا نیست در آیینشان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دیده گشودم به تماشایشان</p></div>
<div class="m2"><p>باز رسیدم به سراپایشان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یافت نشد بر تن این قوم سست</p></div>
<div class="m2"><p>موضع روییدن مویی درست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کرده به بر جا همه کس را چو دلق</p></div>
<div class="m2"><p>ره نه و چون راه، گذرگاه خلق</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر یک ازین قوم پس از سادگی</p></div>
<div class="m2"><p>کرده مباهات به قوّادگی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>صورت خود خاک سر کویشان</p></div>
<div class="m2"><p>دیده در آیینه زانویشان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>روز همه غاشیه بر دوش هم</p></div>
<div class="m2"><p>چون مژه شب خفته در آغوش هم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از بُنه شرم برون برده رخت</p></div>
<div class="m2"><p>دیده چو آیینه فولاد، سخت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گرسنه چشمان نفاق و حسد</p></div>
<div class="m2"><p>جان حسد را دل ایشان جسد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کرده وفا را خجل از زندگی</p></div>
<div class="m2"><p>داده حسد را خط پایندگی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در روش خویش مگو کوتهند</p></div>
<div class="m2"><p>با همه‌کس تا همه‌جا همرهند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>صحبت این قوم بود ناپسند</p></div>
<div class="m2"><p>نم نبود آینه را سودمند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گرمی‌شان چون تب مرگ است زشت</p></div>
<div class="m2"><p>بی رخ این طایفه دوزخ بهشت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>صحبت این طایفه بی برگ به</p></div>
<div class="m2"><p>زانچه دهد ایزدشان، مرگ به</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گلشن خوبی که خوش آب و هواست</p></div>
<div class="m2"><p>تازگی او ز بهار حیاست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>در چمن حسن، ادب آبروست</p></div>
<div class="m2"><p>در گل رخسار، حیا رنگ و بوست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>لاله‌عذاری که حجابش نماند</p></div>
<div class="m2"><p>برگ گلی دان که گلابش نماند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گل چو شود دستزد خار و خس</p></div>
<div class="m2"><p>کی زندش بر سر دستار، کس؟</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>حسن بتان را نشناسی به رنگ</p></div>
<div class="m2"><p>زانکه به میزان ندهد رنگ، سنگ</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>باید، اگر رنگ بود در حساب</p></div>
<div class="m2"><p>لاله دهد بیشتر از گل، گلاب</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گِل به ازان گل که گلابیش نیست</p></div>
<div class="m2"><p>خاک در آن دیده که آبیش نیست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>پاکی دامن ز نکویان نکوست</p></div>
<div class="m2"><p>آینه را زخم قفا داغ روست</p></div></div>