---
title: >-
    شمارهٔ ۲۲ - صدای سروش برای ثناگستری چمن کشمیر
---
# شمارهٔ ۲۲ - صدای سروش برای ثناگستری چمن کشمیر

<div class="b" id="bn1"><div class="m1"><p>مرا این نغمه مالد دم به دم گوش</p></div>
<div class="m2"><p>که بلبل در چمن عیب است خاموش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز لب مهر خموشی زود برگیر</p></div>
<div class="m2"><p>زبان را در پس دندان مکن پیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه خاموشی، چمن را گوش کر نیست</p></div>
<div class="m2"><p>فغان عندلیبان بی‌اثر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محیط جسم را نطق است گوهر</p></div>
<div class="m2"><p>زبان بی سخن، برگی‌ست بی بر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دریای سخن، از یک صدف در</p></div>
<div class="m2"><p>جهانی را توان کرد از گهر پر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن روح است و پیکر جوهر جان</p></div>
<div class="m2"><p>سخن را هست منت بر سر جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی را بر سخن انگشت رد نیست</p></div>
<div class="m2"><p>سخن از ملک جان است، از جسد نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن سازد جوان چرخ کهن را</p></div>
<div class="m2"><p>چه منت‌هاست بر گردون سخن را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخن منسوخ بودی گر در ایام</p></div>
<div class="m2"><p>نبردی هیچ‌کس را، هیچ‌کس نام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخن را گر قضا از عرصه رُفتی</p></div>
<div class="m2"><p>به عالم، کس چه گفتی یا شنفتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زبانی کز سخن بیکار باشد</p></div>
<div class="m2"><p>زبان صورت دیوار باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سخن اصل وجود کاینات است</p></div>
<div class="m2"><p>سخن پیرایه ذات و صفات است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بس طبعم به فکر گلشن افتاد</p></div>
<div class="m2"><p>سخن با غنچه در یک پیرهن زاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حدیث گل چنان افسانه‌ام شد</p></div>
<div class="m2"><p>که بلبل آمد و پروانه‌ام شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به قمری گفتم از سرو آنقدر من</p></div>
<div class="m2"><p>که طوق از منتم سودش به گردن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ثناگستر نباشم چون چمن را؟</p></div>
<div class="m2"><p>کند حرف چمن، رنگین سخن را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زبانم حرف گل چون کرد آغاز</p></div>
<div class="m2"><p>به جای گوش، گل را شد دهن باز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حکایت آنقدر گفتم ز بستان</p></div>
<div class="m2"><p>که اعضایم شد اجزای گلستان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به باغ فکر ازین گلشن ستایی</p></div>
<div class="m2"><p>کفی دارم ز گل چیدن حنایی</p></div></div>