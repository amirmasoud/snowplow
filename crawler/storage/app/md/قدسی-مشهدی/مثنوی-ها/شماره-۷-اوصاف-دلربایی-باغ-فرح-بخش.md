---
title: >-
    شمارهٔ ۷ - اوصاف دلربایی باغ فرح‌بخش
---
# شمارهٔ ۷ - اوصاف دلربایی باغ فرح‌بخش

<div class="b" id="bn1"><div class="m1"><p>مرا باغ فرح‌بخش است منظور</p></div>
<div class="m2"><p>ندارم آرزوی روضه حور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفته سروش از آزادگان باج</p></div>
<div class="m2"><p>رسانده سرفرازی را به معراج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز هر برگش گلستانی نمایان</p></div>
<div class="m2"><p>چو از آیینه عکس روی جانان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمینش سبزه را پاینده دارد</p></div>
<div class="m2"><p>رطوبت را هوایش زنده دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیابانش بود فردوس اکبر</p></div>
<div class="m2"><p>لبالب شاه نهر از آب کوثر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که دیده جز درین فردوس ثانی؟</p></div>
<div class="m2"><p>خیابانی ز آب زندگانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به پای شاه‌نهر افتاده دریا</p></div>
<div class="m2"><p>دُر شهوار ازو دارد تمنا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جدا گردد چو آب از چشمه‌سارش</p></div>
<div class="m2"><p>کشد دریا به عزت در کنارش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درین گلشن برای هر نهالی</p></div>
<div class="m2"><p>بهار آورده تشریف کمالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترشح‌های ابر نوبهاری</p></div>
<div class="m2"><p>چمن را روز و شب در تازه‌کاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درختان در روش پر کرده بیرون</p></div>
<div class="m2"><p>ازان روی فلک سر کرده بیرون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز شاخ گلبنش تا غنچه‌ای زاد</p></div>
<div class="m2"><p>شکفتن را شکفتن می‌دهد یاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز خاکش تا نهال تازه‌ای جست</p></div>
<div class="m2"><p>به رعنایی صنوبر را کمر بست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کند بوی بهش رنجور را نغز</p></div>
<div class="m2"><p>سخن را حرف بادامش دهد مغز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نباشد سیب او را تاب دندان</p></div>
<div class="m2"><p>مگر خورد آب از چاه زنخدان؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز امرودش بچش کاین شهد نایاب</p></div>
<div class="m2"><p>مکرر قند را از شرم کرد آب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به رنگ و بو سزد گر سیب این باغ</p></div>
<div class="m2"><p>سمرقند و صفاهان را کند داغ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ندارد هیچ سیب این دلپذیری</p></div>
<div class="m2"><p>خلاف است آن که آرد سیب، سیری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ازان شد شاه آلو، نام گیلاس</p></div>
<div class="m2"><p>که نیکو داشت عرض میوه را پاس</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کسی کاو لعل را رنگین شمارد</p></div>
<div class="m2"><p>خبر از رنگ شاه آلو ندارد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شود لعل بدخشانت فراموش</p></div>
<div class="m2"><p>ز شاه آلو کنی گر حلقه در گوش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ازان نخلش برآرد لعل رخشان</p></div>
<div class="m2"><p>که دارد ریشه در کوه بدخشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>درین بستان بود پیوسته در کار</p></div>
<div class="m2"><p>به شفتالوربایی بوسه یار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ازان عنّاب را شد لاله وصّاف</p></div>
<div class="m2"><p>که از عنّاب گردد رنگ خون صاف</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز بس تاکش کشیده سر بر افلاک</p></div>
<div class="m2"><p>خورد بر خوشه پروین، سر تاک</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حدیث میوه‌اش گفتم ز هر باب</p></div>
<div class="m2"><p>چو بردم نام شفتالو، شدم آب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نهال جعفری با سرو همسر</p></div>
<div class="m2"><p>شده سوسن هم‌آغوش صنوبر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو از شبنم دهان غنچه وا شد</p></div>
<div class="m2"><p>تبسم خنده دندان‌نما شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز بس هر سو دوید و شمع افروخت</p></div>
<div class="m2"><p>چراغ لاله را در دل نفس سوخت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنان برگ گلش پر آب و تاب است</p></div>
<div class="m2"><p>که گویی غنچه مینای گلاب است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نهال تازه‌اش چندان قد افراخت</p></div>
<div class="m2"><p>که قمری سرو خود را دید و نشناخت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>درین گلشن، نگاه چشم بینا</p></div>
<div class="m2"><p>بود کابین عروسان چمن را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نسیم این چمن در دیده خار</p></div>
<div class="m2"><p>گلستان ارم را کرده بیدار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کسی از فیض این گلشن چه گوید</p></div>
<div class="m2"><p>که جای گل بهار از خاک روید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سرشته از دماغِ تر، هوایش</p></div>
<div class="m2"><p>گریزان بی‌دماغی از فضایش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز گلبن، گل به چندان رنگ زد جوش</p></div>
<div class="m2"><p>که شد عیب گل رعنا فراموش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حباب اینجا هوا را می‌فشارد</p></div>
<div class="m2"><p>که بحر آبی به روی کار آرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز شبنم بس که خاکش کامیاب است</p></div>
<div class="m2"><p>بر او نقش قدم نقشی بر آب است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز دیگر بوستان‌ها، این گلستان</p></div>
<div class="m2"><p>بود ممتاز، چون یوسف ز اخوان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گلش آسوده از صوت هزارست</p></div>
<div class="m2"><p>که مدهوش از صدای آبشارست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مگر فواره سر بر اوج سوده</p></div>
<div class="m2"><p>نگاری ساعد سیمین نموده</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پی صرف چمن، فواره بی‌تاب</p></div>
<div class="m2"><p>دمادم سیم ساعد می‌کند آب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دهد گر آبشار آبی به نازش</p></div>
<div class="m2"><p>همان ساعت دهد فواره بازش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>درین گلشن به رغم یزد و کاشان</p></div>
<div class="m2"><p>بود هر ماه، سی روز آب‌پاشان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو در خلد، آن چه بایستی ندیدند</p></div>
<div class="m2"><p>ازان باغ فرح‌بخش آفریدند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>فرح‌بخش است نام این بوستان را</p></div>
<div class="m2"><p>ازان بخشد فرح، خلق جهان را</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز شوخی نرگس این باغ شاید</p></div>
<div class="m2"><p>که مژگان تماشایی رباید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ارم در پشت دیوارش نشسته</p></div>
<div class="m2"><p>خجل چون عندلیب پرشکسته</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>فرح‌بخش از دو عالم دل‌پذیرست</p></div>
<div class="m2"><p>بهشت و شاه‌نهرش جوی شیر است</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ندیده در جهان کس این چنین جای</p></div>
<div class="m2"><p>فرح‌بخش و فرحناک و فرح‌زای</p></div></div>