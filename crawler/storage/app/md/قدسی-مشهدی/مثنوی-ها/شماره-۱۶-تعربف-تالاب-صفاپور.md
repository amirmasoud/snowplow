---
title: >-
    شمارهٔ ۱۶ - تعربف تالاب صفاپور
---
# شمارهٔ ۱۶ - تعربف تالاب صفاپور

<div class="b" id="bn1"><div class="m1"><p>بود جام جهان‌بین گرچه پرنور</p></div>
<div class="m2"><p>ندارد نور تالاب صفاپور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آبش عکس کشتی‌ها نمودار</p></div>
<div class="m2"><p>چو از آیینه عکس ابروی یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عکس گل در آب آتش فتاده</p></div>
<div class="m2"><p>چنان کز آب، یابی فیض باده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من و نظّاره این طرفه تالاب</p></div>
<div class="m2"><p>به بر گو طرفه بغداد را آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روان کوهکن در آب لارست</p></div>
<div class="m2"><p>مگر از جوی شیرش یادگار است؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب مهتاب و سیر روی دریا</p></div>
<div class="m2"><p>کند آیینه دل را مصفا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه دریا، آسمان برقراری</p></div>
<div class="m2"><p>ز گل‌های کَوَل، خورشیدزاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قضا از سیم نابش آفریده</p></div>
<div class="m2"><p>به غیر از موج گل، طوفان ندیده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لبالب گشته بحر از لولوی تر</p></div>
<div class="m2"><p>در او کشتی روان در آب گوهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به هر جانب که کشتی رو نهاده</p></div>
<div class="m2"><p>چو رود نیل، آبش کوچه داده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز کشتی‌های لعلی شد گلستان</p></div>
<div class="m2"><p>مگو دریا ندارد حاصل کان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شده مخصوص هر کشتی، بهاری</p></div>
<div class="m2"><p>ز گلگون چهره هر یک لاله‌زاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بس کشتی فلک در زر گرفته</p></div>
<div class="m2"><p>جهان را گنج بادآور گرفته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خرامان کشتی رنگین، به لنگر</p></div>
<div class="m2"><p>چو طاووسان کشیده چتر بر سر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه کشتی‌ها درین دریا روانند</p></div>
<div class="m2"><p>که طاووسان گلزار جنانند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به خوبی، هر سفینه نازنینی</p></div>
<div class="m2"><p>گرفته در برش کشتی‌نشینی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سبدهای گلند این نازنینان</p></div>
<div class="m2"><p>گل روی سبد، کشتی‌نشینان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نهد بر آب دریا گرچه سینه</p></div>
<div class="m2"><p>رود بر روی موج گل، سفینه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نظر بر سطح آبش چون گماری</p></div>
<div class="m2"><p>ببین، گر طاقت نظّاره داری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بهار دیگر و کشمیر دیگر</p></div>
<div class="m2"><p>بهشتی در میان آب کوثر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بهشتی از ته دریا نمودار</p></div>
<div class="m2"><p>چنان کز دیده تر، عکس دلدار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چمن‌ها در میان آب، پیدا</p></div>
<div class="m2"><p>چو روی نوخطان در دیده ما</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بهشت است این، که تا کشمیر را دید</p></div>
<div class="m2"><p>سر از شرمش به زیر آب دزدید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کَوَل در غنچگی تیری دواند</p></div>
<div class="m2"><p>که باج از لعل پیکانی ستاند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه دولت دارد این تالاب در سر</p></div>
<div class="m2"><p>که از نیلوفرش گیرد هما، فر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ورع این بحر را بیند چو در خواب</p></div>
<div class="m2"><p>رود بی خود به سیر عالم آب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بود سیمین‌برانش در خزینه</p></div>
<div class="m2"><p>ز صد گنج روان در هر سفینه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز بس کز قعر دریا سبزه زد سر</p></div>
<div class="m2"><p>زمرد شد ز عکس سبزه، گوهر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دل از طوفان معنی بود در جوش</p></div>
<div class="m2"><p>فسون حرف موجم کرد خاموش</p></div></div>