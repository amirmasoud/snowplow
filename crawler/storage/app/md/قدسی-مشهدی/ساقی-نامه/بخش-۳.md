---
title: >-
    بخش ۳
---
# بخش ۳

<div class="b" id="bn1"><div class="m1"><p>کسی را که در طبع انصاف نیست</p></div>
<div class="m2"><p>بود گر مه، آیینه‌اش صاف نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو دانی و صاحب سخن‌پروری</p></div>
<div class="m2"><p>به جان سخن، کز سخن نگذری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عالم ز صد بهره‌مند از سخن</p></div>
<div class="m2"><p>شود نام یک تن بلند از سخن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدار از سخن هر کسی گو نصیب</p></div>
<div class="m2"><p>دهد دل به یک آشنا صد غریب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی شعر را گو حقیقت مدان</p></div>
<div class="m2"><p>نمی‌افتد از کار طبع روان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز مردم به گوهر نپرداختن</p></div>
<div class="m2"><p>نکرد ابر ترک گهر ساختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن را چه پروای هر نارس است</p></div>
<div class="m2"><p>برای سخن، یک سخن‌رس بس است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه شد گر بود چشم اختر به خواب؟</p></div>
<div class="m2"><p>جهان را کفاف است یک آفتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه شد گر ندارد سخن مشتری؟</p></div>
<div class="m2"><p>تهی نیست بازار از جوهری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه هرکس بود با سخن آشنا</p></div>
<div class="m2"><p>سخن را سخن‌سنج داند ادا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عنان سخن نیست در دست زاغ</p></div>
<div class="m2"><p>سخن را کند سبز طوطی باغ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عجب نیست دارد سخن را چو پاس</p></div>
<div class="m2"><p>گهرناشناسی ز گوهرشناس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز اهل غرض نیست پروا مرا</p></div>
<div class="m2"><p>که در دل دهد بی‌غرض جا مرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز حرف کج‌اندیش پروا که راست؟</p></div>
<div class="m2"><p>نیندیشد از دخل کج، فکر راست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عقیق ار کَنی بهر خاتم رواست</p></div>
<div class="m2"><p>خراشیدن روی گوهر خطاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود کاوش چشمه، مرگ صفا</p></div>
<div class="m2"><p>چرا دخل در شعر چندین، چرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>میفکن چنان در سخن رست‌خیز</p></div>
<div class="m2"><p>که معنی شود بسمل از فکر تیز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سخن‌ور بود با خضر هم‌ثبات</p></div>
<div class="m2"><p>که نوشد ز شعر تر، آب حیات</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سخن‌ور زند لاف پایندگی</p></div>
<div class="m2"><p>که باشد سخن چشمه زندگی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به اشعار رنگین قلم در تلاش</p></div>
<div class="m2"><p>به کف گو می ارغوانی مباش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مکن چون نگین، خانه زر هوس</p></div>
<div class="m2"><p>بود جزو تقطیع، یک بیت بس</p></div></div>
<div class="b2" id="bn22"><p>***</p></div>
<div class="b" id="bn23"><div class="m1"><p>سخن را سخن‌ور کند پایمال</p></div>
<div class="m2"><p>که گوهر فروشد به مشت سفال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سخن گشته پامال مشتی فضول</p></div>
<div class="m2"><p>ملولم ازین بوالفضولان ملول</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بود شعرشان را به صد قال و قیل</p></div>
<div class="m2"><p>ز لب، انتهای سفر تا سبیل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز مضمون مردم سرودم زنند</p></div>
<div class="m2"><p>بیارند و بر روی مردم زنند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز لفظی که انکار معنی کند</p></div>
<div class="m2"><p>ازان، کس بر ایشان چه دعوی کند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بود شعر ازین قوم چون در امان؟</p></div>
<div class="m2"><p>که جوهر تراشند از استخوان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ربایند از من دُری چون به فن</p></div>
<div class="m2"><p>فروشند بازش به تحسین من</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز تاراج این فرقه زن به مزد</p></div>
<div class="m2"><p>خریدار کالای خویشم ز دزد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چه معنی که فرزند خود خوانده‌اند</p></div>
<div class="m2"><p>که نام و لباسش نگردانده‌اند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز اظهار معنی به من در خروش</p></div>
<div class="m2"><p>چو غواص گوهر به دریا فروش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز تاراج معنی گرفته نصیب</p></div>
<div class="m2"><p>اسیرآوران یتیم و غریب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به ترتیب دیوان معین همند</p></div>
<div class="m2"><p>چه دیوان که دیوان ازان می‌رمند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پی خواندن شعر دمساز هم</p></div>
<div class="m2"><p>به تحسین بیجا، هم‌آواز هم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز تحسین بیجای هم، زیر قرض</p></div>
<div class="m2"><p>اداکردنش را شمارند فرض</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چه شد گر شد اجزای دیوان درست؟</p></div>
<div class="m2"><p>به شیرازه محکم نشد شعر سست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو تقطیع ابیات هم می‌کنند</p></div>
<div class="m2"><p>قلم‌وار، مصرع قدم می‌کنند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کتاب ار به خشتی شدی هم‌بها</p></div>
<div class="m2"><p>چها می‌زدندی به قالب، چها</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بود طبع این فرقه خودپرست</p></div>
<div class="m2"><p>جز انصاف نزدیک ما هرچه هست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مقید به وزن سخن کمترند</p></div>
<div class="m2"><p>ز هم شعر را ریش‌پیما خرند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گمان تو این است ای خودپسند</p></div>
<div class="m2"><p>که ریش درازست شعر بلند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به اشعار برجسته چندین ملاف</p></div>
<div class="m2"><p>که معنی ازان جسته تا کوه قاف</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در فیض بر روی کس بسته نیست</p></div>
<div class="m2"><p>تو هم جستجو کن تنت خسته نیست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چه عیب است در نارسی‌های روچ؟</p></div>
<div class="m2"><p>گرت هست مغزی، نگو حرف پوچ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چه اندوزی از جامه خوش‌قماش؟</p></div>
<div class="m2"><p>برو در قماش سخن کن تلاش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مکن خودفروشی به دستار زر</p></div>
<div class="m2"><p>که باشد سخن را عیار دگر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>میاور ز طومار شعرت سجل</p></div>
<div class="m2"><p>به محضر چه حاجت مملّ مخل</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز بسیار گفتن نگه‌دار دم</p></div>
<div class="m2"><p>مکن اینقدر بر شنیدن ستم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گمانم که باشد فزون‌تر نیاز</p></div>
<div class="m2"><p>به طومار شعرت ز عمر دراز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به اندازه کن صرف گفتار خویش</p></div>
<div class="m2"><p>نمک شوری آرد ز اندازه بیش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چرا شعر چندان مکرر شود</p></div>
<div class="m2"><p>که گوش نیوشندگان کر شود</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو پرسندت از قصه باستان</p></div>
<div class="m2"><p>ز گفتار خود سر کنی داستان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به گفتن مکن اینقدر عمر صرف</p></div>
<div class="m2"><p>که از مستمع جان رود از تو حرف</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سخن را چنان امتدادی مده</p></div>
<div class="m2"><p>که از گوش‌ها پنبه روید چو به</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو بگذشت ز اندازه افسانه‌ات</p></div>
<div class="m2"><p>مخوان، تا نخوانند دیوانه‌ات</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>پی صحبت گوش چندین مکوش</p></div>
<div class="m2"><p>زبان باش از خستگی گو خموش</p></div></div>
<div class="b2" id="bn58"><p>***</p></div>
<div class="b" id="bn59"><div class="m1"><p>صراحی به گوش قدح گفت دوش</p></div>
<div class="m2"><p>که خون می‌چکد از زبان خموش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زند نشتر خار، گلبرگ تر</p></div>
<div class="m2"><p>حذر از زبان خموشان، حذر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سخن‌های ناگفته اکثر نکوست</p></div>
<div class="m2"><p>خموشی زبان‌دان این گفتگوست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو بلبل شوی چند افغان‌فروش؟</p></div>
<div class="m2"><p>چو پروانه خود را بسوزان خموش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مپیچ آنقدر در زبان‌آوری</p></div>
<div class="m2"><p>که ممنون شود گوش کر از کری</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>حرام است خواندن ز اندازه بیش</p></div>
<div class="m2"><p>چو بلبل مشو مست آواز خویش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تو را کرده گفت از شنو بی‌خبر</p></div>
<div class="m2"><p>زبان تو گوش تو را کرده کر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کنی امتحان گوش خود را به هوش</p></div>
<div class="m2"><p>زبان تو فرصت دهد گر به گوش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>اگر پرسد از عقل کل کس نشان</p></div>
<div class="m2"><p>ز جزو خود آری سخن در میان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به پر گفتن شعر، راغب مباش</p></div>
<div class="m2"><p>زیان خود و سود کاتب مباش</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به ترتیب دیوان چو آیی به جوش</p></div>
<div class="m2"><p>به روغن فتد نان کاغذفروش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ورق آنچنانت سیه‌روی ساخت</p></div>
<div class="m2"><p>که نتوانی از رو ورق را شناخت</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ازان رو کمی در سخن یاب شد</p></div>
<div class="m2"><p>که شیرین بود هرچه کمیاب شد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>به معنی کسانی که سنجیده‌اند</p></div>
<div class="m2"><p>ز یک حرف، صد حرف فهمیده‌اند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>اگر شاعری در سخن کن تلاش</p></div>
<div class="m2"><p>که لفظش چو معنی بود خوش‌قماش</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به خواندن مکن آنچنان وجد و حال</p></div>
<div class="m2"><p>که تحسین گفتن شود پایمال</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز تحسین جاهل میفزا طرب</p></div>
<div class="m2"><p>کند کار طاووس گوساله شب</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بس است این سخن گر کسی در ده است</p></div>
<div class="m2"><p>که نفرین ز تحسین بیجا به است</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>نفهمیده هرکس که تحسین کند</p></div>
<div class="m2"><p>نه تحسین که بر شعر نفرین کند</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ز هر نکته آنها که فهمیده‌اند</p></div>
<div class="m2"><p>به جنباندن سر نجنبیده‌اند</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نفهمیده تحسینی از راه دور</p></div>
<div class="m2"><p>عجب ریشخندی بود در حضور</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>سخن غور ناکرده تحسین چرا</p></div>
<div class="m2"><p>بهاری نه، فریاد رنگین چرا</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>دل از حرف نادان بر آتش بود</p></div>
<div class="m2"><p>سخن‌سنج دانا، سخن‌کش بود</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بود فکر یک مصرع آبدار</p></div>
<div class="m2"><p>چو صیاد بی صید روز شکار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>میان دو مصراع بیگانگی</p></div>
<div class="m2"><p>چو عیب کمان دان ز یکخانگی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ز معنی چو بر خود نبالیده‌ای</p></div>
<div class="m2"><p>چه حاصل که لفظی تراشیده‌ای</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>درین حرف کس را چه دعوی بود</p></div>
<div class="m2"><p>که مقصود از لفظ، معنی بود</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>نباشد چو سیمین تنی در میان</p></div>
<div class="m2"><p>چو سودست از دیدن پرنیان؟</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>سخن بهر معنی تند تار و پود</p></div>
<div class="m2"><p>ز دیبای چین بی بت چین چه سود</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>به معنی بود خاطر از لفظ شاد</p></div>
<div class="m2"><p>ز گلشن به جز گل چه باشد مراد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>گل و لاله دانند تا خار و خس</p></div>
<div class="m2"><p>که از چشمه مقصود، آب است و بس</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>که بهر مکیدن نهد لب بر آن</p></div>
<div class="m2"><p>نباشد اگر مغز در استخوان</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>ز معنی‌ست مصراع، مصراع کس</p></div>
<div class="m2"><p>غرض روشنی باشد از شمع و بس</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ز مصراع، بی مغز رنگین مبال</p></div>
<div class="m2"><p>غرض میوه است از وجود نهال</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بود معنی خشک در لفظ صاف</p></div>
<div class="m2"><p>چو شمشیر چوبین به زرین غلاف</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>دل خود به معنی گرو کن، گرو</p></div>
<div class="m2"><p>به بازار صورت‌فروشان مرو</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ز دل معنی خویش کن آشکار</p></div>
<div class="m2"><p>به صورت مپرداز آیینه‌وار</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>چه شد زین که آیینه صورت‌گرست</p></div>
<div class="m2"><p>چو معنیش در صورت دیگرست</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>تناسب در الفاظ دان بی‌بدل</p></div>
<div class="m2"><p>نه چندان که در معنی افتد خلل</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>در آن صورت از لفظ، نسبت به جاست</p></div>
<div class="m2"><p>که از نسبتش جان معنی نکاست</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>تناسب چرا ره به جایی برد</p></div>
<div class="m2"><p>که نسبت ز بی‌‌نسبتی خون خورد</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>در آرایش لفظ چندان مکوش</p></div>
<div class="m2"><p>که رخسار معنی شود پرده‌پوش</p></div></div>