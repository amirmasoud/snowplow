---
title: >-
    بخش ۶
---
# بخش ۶

<div class="b" id="bn1"><div class="m1"><p>تنومندی و دست زورش حلال</p></div>
<div class="m2"><p>که موری نشد در رهش پایمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو افشرد بر سنگ پای درنگ</p></div>
<div class="m2"><p>چو خون از رگ لعل جوشید رنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یادش کجا میوه‌ای رخ نمود؟</p></div>
<div class="m2"><p>که در خامی از کار نگذشته بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پرواز گامش چه کوه و چه دشت</p></div>
<div class="m2"><p>به یادش توان از دو عالم گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگردیده در دل خیال دوال</p></div>
<div class="m2"><p>برون جسته از عرصه ماه و سال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زند تا لگد بر سر راه دور</p></div>
<div class="m2"><p>کشد راه را پیش، دستش به زور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به یادش مگر جیب دل پاره گشت؟</p></div>
<div class="m2"><p>که چاکش ز دامان محشر گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نبینی به جز راه در هیچ حال</p></div>
<div class="m2"><p>که افتاده‌ای را کند پایمال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به ناخن کند سینه خاک، چاک</p></div>
<div class="m2"><p>که آرام را در سپارد به خاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز شوخی به بستن نمی‌داد دست</p></div>
<div class="m2"><p>چگونه پی‌اش بر زمین نقش بست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نهان از نظر می‌رود چون پری</p></div>
<div class="m2"><p>ولیکن چو انسان به فرمانبری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود سرکش اما به حکم هنر</p></div>
<div class="m2"><p>ز آرام آسودگان رام‌تر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به جولان‌گری چون نسیم بهار</p></div>
<div class="m2"><p>سبک‌روتر از آب در سبزه‌زار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز تمکین دل کوه را سوخته</p></div>
<div class="m2"><p>به شبنم سبک‌روحی آموخته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خیالش اگر بگذرد از کران</p></div>
<div class="m2"><p>شود در صدف آب گوهر روان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به یک گام جستی ازین نُه حصار</p></div>
<div class="m2"><p>گر از برق نعلش نبودی جدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برش خواه ره بیش و خواه اندکی</p></div>
<div class="m2"><p>به پیشش بلندی و پستی یکی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز نعلش زمین شد ز سیاره پر</p></div>
<div class="m2"><p>سپهر از خوی او پر از ماه و خور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خیالش اگر بگذرد بر سراب</p></div>
<div class="m2"><p>زمین را به ناخن رساند به آب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو ادراک اهل ذکا، تیز و تند</p></div>
<div class="m2"><p>ز تندیش بازار مهمیز، کند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو ملّاح نام آردش بر زبان</p></div>
<div class="m2"><p>شود کشتی آزاد از بادبان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به راهی کزو رفت پیک امید</p></div>
<div class="m2"><p>نشد صبح دوری در آن ره سفید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مگر عازمش یافت در قطع راه؟</p></div>
<div class="m2"><p>که دوری به نزدیکی آرد پناه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به راهی که یک بار پیموده است</p></div>
<div class="m2"><p>ز نزدیکی خود ره آسوده است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به راهی که عزمش تکاپوی کرد</p></div>
<div class="m2"><p>ازان راه برخاست دوری چو گرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز دامان زینش قضا داده بال</p></div>
<div class="m2"><p>محال است همراهی او، محال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو بر سطح خارا کند سخت، پا</p></div>
<div class="m2"><p>گشاید گلویش چو سنگ آسیا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دویدن زند چند بر وی نیاز؟</p></div>
<div class="m2"><p>کند کاش وسعت بغل پهن باز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کند پس کشیدن عنان را کمین</p></div>
<div class="m2"><p>که پیشی به گردش رساند جبین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مرصع یراقش به یاقوت و در</p></div>
<div class="m2"><p>میان خالی اما کفل کیسه پر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نمی‌دانم این پرهنر از کجاست</p></div>
<div class="m2"><p>که یک موی او را دو عالم بهاست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به چندین هنر می‌خرندش چنین</p></div>
<div class="m2"><p>منال از هنر، گو کسی بعد ازین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سوارش نجنبانده بند قبا</p></div>
<div class="m2"><p>سمش کرده طی از سمک تا سما</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>درنگش مگر سرعت‌انگیز شد؟</p></div>
<div class="m2"><p>که بازار طی زمان تیز شد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سوارش به منزل چه آزاد رفت</p></div>
<div class="m2"><p>که جنباندن پایش از یاد رفت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر این ابلق، افلاک را حیرت است</p></div>
<div class="m2"><p>که تا خانه زین پر از دولت است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز پرواز نعلش که راند سخن؟</p></div>
<div class="m2"><p>که حیرت ندوزد به میخش دهن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سرین منعم از بسته ریو و رنگ</p></div>
<div class="m2"><p>میان مفلس و تنگ بالای تنگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به هیکل چرا رام هرکس بود؟</p></div>
<div class="m2"><p>قوی هیکلی، هیکلش بس بود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو بر خویش گیر سر راه ناز</p></div>
<div class="m2"><p>سراپا زند بر سراپا نیاز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز نعلش سزد تیغ روز نبرد</p></div>
<div class="m2"><p>که انگیزد از خون بدخواه، گرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نشان سمش بر زمین نقش بست</p></div>
<div class="m2"><p>برای زمین، طرفه نقشی نشست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نیابند در چارسوی جهان</p></div>
<div class="m2"><p>چو نقد سمش، نقد دیگر روان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به موی دمش زلف خوبان اسیر</p></div>
<div class="m2"><p>ز مشت سمش سنگ خارا خمیر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چنین رهنوردی که دارد به یاد؟</p></div>
<div class="m2"><p>که نعلش کند حلقه در گوش باد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کشی صورت ناخنش گر به سنگ</p></div>
<div class="m2"><p>جهد از رگ سنگ، خون بی‌درنگ</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بود دانه‌اش گر ز کشتن مراد</p></div>
<div class="m2"><p>دهد تخم، ناکشته خرمن به باد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز دستش مددجویی‌ای بسته پا</p></div>
<div class="m2"><p>جلوگیر کن بخت برگشته را</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به سعیش چنان سعی را گرم، پشت</p></div>
<div class="m2"><p>که سیماب را طعن آرام کشت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زمین از پی‌اش گر پذیرد نشان</p></div>
<div class="m2"><p>کند خاک در چشم ریگ روان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نشان پی‌اش در گل رهگذر</p></div>
<div class="m2"><p>ز سیاره با سیر نزدیک‌تر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زند پای سعیش چو ناخن به سنگ</p></div>
<div class="m2"><p>دَرَد سنگ خارا لباس درنگ</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کند بس که در زیر پایش سماع</p></div>
<div class="m2"><p>بود آسمان با زمین در نزاع</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مصوّر بر او نقش پیشی نبست</p></div>
<div class="m2"><p>مگر بگذرد نقش پایش ز دست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خرامنده سیلی که وقت سکون</p></div>
<div class="m2"><p>ز غیرت کند در دل کوه، خون</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز دنبال او برق در جستجو</p></div>
<div class="m2"><p>به صحرا به ریگ روان شد فرو</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>غبار سمش سرمه چشم باد</p></div>
<div class="m2"><p>متاع درنگ از شتابش کساد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>شتابش ز ره گر نپیچد عنان</p></div>
<div class="m2"><p>درنگ آید از بی‌درنگی به جان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز طبعش روش یاد گیرد نهال</p></div>
<div class="m2"><p>ز چشمش خورد خون غیرت غزال</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نبودی اگر موی او رنگ بست</p></div>
<div class="m2"><p>چو رنگ حنا زود رفتی ز دست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کلاه از نمد زین او کرده ماه</p></div>
<div class="m2"><p>که بی سر کند سیر گردون، کلاه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گرش آرد آرام پا در رکاب</p></div>
<div class="m2"><p>نهد پا به دروازه اضطراب</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو گام فراخش بود دزد راه</p></div>
<div class="m2"><p>ز آسیب، ره را که دارد نگاه؟</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>درنگ از شتابش کند اضطراب</p></div>
<div class="m2"><p>ز بیم درنگش بلرزد شتاب</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو چوگان شود دست آن پرهنر</p></div>
<div class="m2"><p>برد گوی شوخی ز میدان به در</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>روان خرد، واله هوش او</p></div>
<div class="m2"><p>صبا کشته خنجر گوش او</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به زور قدم، وزن فرسنگ برد</p></div>
<div class="m2"><p>خط دوری از صفحه ره سترد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نهد وقت گردش چو بر خاره پا</p></div>
<div class="m2"><p>به چرخش درآرد چو سنگ آسیا</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به سرعت چنان دست و پا در جدل</p></div>
<div class="m2"><p>که از همرهی مانده داغ کفل</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ز همراهی‌اش بعد چندین شتاب</p></div>
<div class="m2"><p>ز داغ کفل نگذرد آفتاب</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چه منزل که پیمود و منزل نکرد</p></div>
<div class="m2"><p>که شد در رهش خاک منزل به گرد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ببین بر سر ره چه بیداد برد</p></div>
<div class="m2"><p>که پیش سمش باد را باد برد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>رهی را که پیمودنش ساز کرد</p></div>
<div class="m2"><p>ز انجام بگذشت و آغاز کرد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>همان به که طی سازم این قال و قیل</p></div>
<div class="m2"><p>برانگیزم اسبی به تعریف فیل</p></div></div>
<div class="b2" id="bn75"><p>***</p></div>
<div class="b" id="bn76"><div class="m1"><p>تعالی الله از پیکر نوربخت</p></div>
<div class="m2"><p>که هم نوربخت است و هم نیک‌بخت</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بود هیکلش کوه قدر و شکوه</p></div>
<div class="m2"><p>کجک بر سرش سرکش کاف کوه</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ز چو کندی‌اش کس فتد در گمان</p></div>
<div class="m2"><p>که وارون شده کرسی آسمان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به خرطوم دارد فلک را نگاه</p></div>
<div class="m2"><p>که از نقش پایش نیفتد به چاه</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>کند سحر، خرطوم او دم‌به‌دم</p></div>
<div class="m2"><p>ز خرطوم، دهلیز راه عدم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>به خرطوم او دست‌بازی خطاست</p></div>
<div class="m2"><p>عجب سیلی این نهر را در قفاست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ندیده‌ست ایام، فیلی چنین</p></div>
<div class="m2"><p>کزو پر بود آسمان و زمین</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بود سایه‌اش ملک هندوستان</p></div>
<div class="m2"><p>که گردون ندیده سوادی چنان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>گرفته فرو از سما تا سمک</p></div>
<div class="m2"><p>به هم خلقتی برده گوی از فلک</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>فلک را به صورت چو گردد دچار</p></div>
<div class="m2"><p>شود معنی جزو و کل آشکار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بود معدن زیرکی پیکرش</p></div>
<div class="m2"><p>تو گویی بود عقل کل در سرش</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به گوشش نظر کن شعورش بدان</p></div>
<div class="m2"><p>دهد گوش پهن از فراست نشان</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ندارد به غیر از شنیدن هوس</p></div>
<div class="m2"><p>بزرگان همه گوش باشند و بس</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ز فهمیدگی‌ها، چو اهل یقین</p></div>
<div class="m2"><p>نفهمیده ننهاده پا بر زمین</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>شمار نظر کرده در چشم مور</p></div>
<div class="m2"><p>که دارد به قدر بزرگی شعور</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>ندارد به جز خاکساری هوس</p></div>
<div class="m2"><p>کمال بزرگی همین است و بس</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ز وصفش فلک گفتگو می‌کند</p></div>
<div class="m2"><p>بزرگی ز بالای او می‌کند</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>به خرطوم، ز اختر بود دانه‌چین</p></div>
<div class="m2"><p>بزرگیش آن داده، بینیش این</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>خورد کشته آسمان را خوید</p></div>
<div class="m2"><p>بزرگی به این تنگ‌چشمی که دید؟</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>شود تکیه‌گاهش اگر کوه قاف</p></div>
<div class="m2"><p>فتد کوه را از کمرگاه، ناف</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>گه پویه، بر خاک، پایی فشرد</p></div>
<div class="m2"><p>که از ثقل، گاو زمین جان نبرد</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>ز دندان به ناخن ندارد نیاز</p></div>
<div class="m2"><p>که چندان که چینند، گردد دراز</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>بود بر تن آیینه‌اش خوش‌نما</p></div>
<div class="m2"><p>ز خاکستر آیینه یابد جلا</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ندانم که بی پایه آسمان</p></div>
<div class="m2"><p>به بالای او رفته چون فیل‌بان؟</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>نهد بر سر سایه خود چو پای</p></div>
<div class="m2"><p>نجنبد دگر چون شب غم ز جای</p></div></div>