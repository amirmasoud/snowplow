---
title: >-
    بخش ۲۳
---
# بخش ۲۳

<div class="b" id="bn1"><div class="m1"><p>چو خواهی تماشا کنی اصل و فرع</p></div>
<div class="m2"><p>بود عینک دوربین عین شرع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو شو ذوق خودآگاهی‌ات زین سخن</p></div>
<div class="m2"><p>برو دست در دامن شرع زن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکن گوش بر گفته بوالفضول</p></div>
<div class="m2"><p>توسل مکن جز به آل رسول</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه پرسی تو از بنده راز نهفت</p></div>
<div class="m2"><p>خدا گفته است آنچه بایست گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازین پیش گفت آنچه پرسی ز ما</p></div>
<div class="m2"><p>رسول خدا از کلام خدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مریدان ناقص ز تقصیر خویش</p></div>
<div class="m2"><p>کرامات بندند بر پیر خویش</p></div></div>
<div class="b2" id="bn7"><p>***</p></div>
<div class="b" id="bn8"><div class="m1"><p>نباشد کمالی چو دفع گزند</p></div>
<div class="m2"><p>سپندی بسوزان برای سپند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز سوز دلم، دیده دارد حجاب</p></div>
<div class="m2"><p>عرق کرده ابر از تب آفتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگیر از پی عشق، گو عقل فال</p></div>
<div class="m2"><p>که با هم نجوشند شیر و غزال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندارند ذوق هوس، اهل درد</p></div>
<div class="m2"><p>ز جوش افکند دیگ را آب سرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صدف‌وار، مشکل بود بی‌شکست</p></div>
<div class="m2"><p>که آید دل پاک‌طینت به دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پریشان چو شد دل، کند فیض سر</p></div>
<div class="m2"><p>دهد در پراکندگی دانه بر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فزاید طرب، داغ، دیوانه را</p></div>
<div class="m2"><p>چراغان، بود عید، پروانه را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز دل سوی خود ره برند اهل حال</p></div>
<div class="m2"><p>در آیینه بینند عکس جمال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر افتادگان پا مزن زینهار</p></div>
<div class="m2"><p>بود نخل افتاده را، شعله بار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مشو پرده‌در گر صبا نیستی</p></div>
<div class="m2"><p>مجو گنج، گر اژدها نیستی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نکواژدهایی‌ست مرد خسیس</p></div>
<div class="m2"><p>که با گنج گوهر بود خاک‌لیس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کرم پیشه کن تا مکرّم شوی</p></div>
<div class="m2"><p>قدم پیش نه تا مقدّم شوی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کسی را که همت به کار آیدش</p></div>
<div class="m2"><p>سزد گر ز تعظیم، عار آیدش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تواضع ز منعم خسیسی بود</p></div>
<div class="m2"><p>نگهداری پیسه، پیسی بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گرت می‌خلد خار خار کرم</p></div>
<div class="m2"><p>تواضع مکن صرف، جای درم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تهی کف به عیب غنی گو مکوش</p></div>
<div class="m2"><p>که عیب است زردار و زر عیب‌پوش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نداند دل آزرده زیبا و زشت</p></div>
<div class="m2"><p>بر مرده، بالین چه دیبا، چه خشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تکبّر کند مرد دنیاپرست</p></div>
<div class="m2"><p>شود سرگران، خوشه چون دانه بست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر این است دارنده را زندگی</p></div>
<div class="m2"><p>تهی‌کیسگی به ز دارندگی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به هر رقعه خرقه صد محضرست</p></div>
<div class="m2"><p>که درویشی از خواجگی بهترست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ضعیفی بود به ز تن‌پروری</p></div>
<div class="m2"><p>مه نو عزیزست از لاغری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مِه از کِه گر امداد جوید رواست</p></div>
<div class="m2"><p>برآید ز پهلوی چپ، تیغ راست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به ناآموزده مفرمای کار</p></div>
<div class="m2"><p>که در روشنایی چو نورست نار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به چشم تو روشن نماید ز دور</p></div>
<div class="m2"><p>اگر دیده سازد کسی از بلور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کند خام از پخته پیدا، شراب</p></div>
<div class="m2"><p>که بهتر شناسد سبو را، ز آب؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به چَه درنیفتی، که از راه دور</p></div>
<div class="m2"><p>نماید یکی، آب شیرین و شور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به آب ریا کشته سبز این چمن</p></div>
<div class="m2"><p>مخور گول عمامه نارون</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پی بدره زاهد فشاند اشک ناب</p></div>
<div class="m2"><p>بود دام صیاد ماهی در آب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دلت مرده و خواهشت زنده است</p></div>
<div class="m2"><p>بلی، دام در خاک گیرنده است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو خواهی به دل سیم جیحون کشی</p></div>
<div class="m2"><p>نفس را به گرداب وارون کشی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بود چاره زرق، مشکل پدید</p></div>
<div class="m2"><p>بود قفل وسواس، خود بی‌کلید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نصیحت شنو گو میفزا کسی</p></div>
<div class="m2"><p>که دارد دماغ نصیحت‌گری؟</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به دست آمدت گرچه بی‌رنج، گنج</p></div>
<div class="m2"><p>مده مزد مزدور نابرده رنج</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز دشمن بتر، یار خشم‌آورست</p></div>
<div class="m2"><p>شود تلخ‌تر، آنچه شیرین‌ترست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز نشتر شود رگ جراحت‌پذیر</p></div>
<div class="m2"><p>چه حاصل ز پیچیدنش در حریر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مزن لاف، گو مرد لاف از دروغ</p></div>
<div class="m2"><p>که صبح نخستین ندارد فروغ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خطا از اصیلان نباشد صواب</p></div>
<div class="m2"><p>سبک‌سر مبادا کسی چون حباب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>صدف دارد از بردباری ثبات</p></div>
<div class="m2"><p>حباب سبک‌سر بود کم‌حیات</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گرت اصل خواهش بود ای حکیم</p></div>
<div class="m2"><p>کند عالمی را گدا، یک کریم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خطا هم ز بخشنده باشد صواب</p></div>
<div class="m2"><p>به دوران زدن جام بخشد شراب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به دریا کند موج ابرو تُنُک</p></div>
<div class="m2"><p>که چون زهد خشک است و خشکی خُنُک</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نظر بر تهی‌دیدگان نیست نغز</p></div>
<div class="m2"><p>چه حاصل ز بادام نابسته مغز؟</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مزن چنگ در دامن حرص و آز</p></div>
<div class="m2"><p>که مرد از قناعت شود بی‌نیاز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هوس ملک دین را ز بنیاد کند</p></div>
<div class="m2"><p>امیری کند نفس امّاره چند؟</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بپرهیز ازان قوم ناحق‌شناس</p></div>
<div class="m2"><p>که حق نمک را ندارند پاس</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چه شورش فکندند در انجمن</p></div>
<div class="m2"><p>نمک‌خوارگان نمکدان‌شکن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مدان دعوی تیره‌روزان گزاف</p></div>
<div class="m2"><p>که در چشمه جوشد ز گل، آب صاف</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به یک حرف رنگین، لب هوشمند</p></div>
<div class="m2"><p>زبان را گشاید ز صد ساله بند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز معنی جهان پر ز نقش و نگار</p></div>
<div class="m2"><p>تو را چشم بر صورت آیینه‌وار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو سیماب تا نیم جانیت هست</p></div>
<div class="m2"><p>مده دامن بی‌قراری ز دست</p></div></div>
<div class="b2" id="bn58"><p>***</p></div>
<div class="b" id="bn59"><div class="m1"><p>ز مردن دلم جز به این شاد نیست</p></div>
<div class="m2"><p>که روز جزا از کسم داد نیست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دلم را تُنُک ظرفی‌ای داده دست</p></div>
<div class="m2"><p>که بر سنگم از شیشه افتد شکست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز تنگی چنان عالم آمد به هم</p></div>
<div class="m2"><p>که نه جای شادی‌ست، نه جای غم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زهی وسعت آسمان دو رنگ</p></div>
<div class="m2"><p>که بر تار مویی کند جای تنگ</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو از درد، غم بر خود آسان کنم</p></div>
<div class="m2"><p>اگر درد نبود، چه درمان کنم؟</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مده گو غم از دست، بیداد را</p></div>
<div class="m2"><p>کجا می‌برم خاطر شاد را</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز گلشن، پی گل نگیرم سراغ</p></div>
<div class="m2"><p>شود روشن از لاله‌ام چشم داغ</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کی از غم بود باک، دیوانه را</p></div>
<div class="m2"><p>چه نقصان ز سیلاب، ویرانه را</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سر من به داغ جنون شد گرو</p></div>
<div class="m2"><p>خرد گو سر خویش گیر و برو</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بود طشت آتش ز داغم به سر</p></div>
<div class="m2"><p>که بازار سودا شود گرم‌تر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نشد خواهشم نفس را پایمزد</p></div>
<div class="m2"><p>تهی‌کیسه شرمنده باشد ز دزد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به درد دلم کی دوا می‌رسد؟</p></div>
<div class="m2"><p>اثر می‌رود، تا دعا می‌رسد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>اگر مورم آید به همسایگی</p></div>
<div class="m2"><p>شود خودفروش از تُنُک‌مایگی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چه شد گر دل از نغمه از هوش رفت</p></div>
<div class="m2"><p>کزین گوش آمد، وزآن گوش رفت</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سجودم به آن طاق ابرو رواست</p></div>
<div class="m2"><p>که آید به محراب کج، قبله راست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بود نیت، آشفته‌ای را حلال</p></div>
<div class="m2"><p>که در شانه زلف دیده‌ست فال</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>مرا دایم از گلفروش است داد</p></div>
<div class="m2"><p>که بر عندلیبان کند گل مراد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ز بس تیره سوزد چراغ سخن</p></div>
<div class="m2"><p>سخن هم ندارد دماغ سخن</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>شد از شاعری عزتم برطرف</p></div>
<div class="m2"><p>گهرسازی آرد شکست صدف</p></div></div>
<div class="b2" id="bn78"><p>***</p></div>
<div class="b" id="bn79"><div class="m1"><p>جوی به ز صد ملک کیخسروی</p></div>
<div class="m2"><p>که خود کاری آن را و خود بدروی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>به خاکستر از ملک خود ساختن</p></div>
<div class="m2"><p>به از چنگ در کشور انداختن</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ندارد شرر گرچه در سنگ تاب</p></div>
<div class="m2"><p>کند در جلای وطن اضطراب</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>گهر را که بر تاج و تخت است امید</p></div>
<div class="m2"><p>به راه صدف، چشم گشته سفید</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ازان لعل را نعل در آتش است</p></div>
<div class="m2"><p>که جا لعل را در بدخشان خوش است</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>خورد آب هرکس ز آبشخوری</p></div>
<div class="m2"><p>به جایی بود میل هر عنصری</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>سبک‌سر کند ترک ماوای خویش</p></div>
<div class="m2"><p>به دامن بود کوه را پای خویش</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>به صورت بود خار، غربت نصیب</p></div>
<div class="m2"><p>مبادا کسی غیر معنی، غریب</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>غبار آورد از وطن گر صبا</p></div>
<div class="m2"><p>به چشم غریبان بود توتیا</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>که دیده‌ست تنهانشینی چو من؟</p></div>
<div class="m2"><p>بدن در غریبیّ و جان در وطن</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ز بس داده غربت دلم را فریب</p></div>
<div class="m2"><p>ز جا درنیابم به نظم غریب</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>چه حیرت گر از خود حذر می‌کنم؟</p></div>
<div class="m2"><p>به خود هم غریبانه سر می‌کنم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>اگر در وطن مرگ گردد نصیب</p></div>
<div class="m2"><p>بود بهتر از زنده بودن غریب</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ز بس کز غریبی دل‌افسرده‌ام</p></div>
<div class="m2"><p>تو گویی که در زندگی مرده‌ام</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>به غربت، چو مو بر سر آتشم</p></div>
<div class="m2"><p>به این ضعف، چون بار غربت کشم؟</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>درختی که افکندش از پای، بخت</p></div>
<div class="m2"><p>به گلخن کشدشاخش از باغ، رخت</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بر آن کبک، شاهین ترحّم کند</p></div>
<div class="m2"><p>که چون بیضه کرد، آشیان گم کند</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>ترحّم بر آن صید باشد ضرور</p></div>
<div class="m2"><p>کز آبشخور خویش افتاده دور</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>اگر بلبلی گم کند آشیان</p></div>
<div class="m2"><p>به چشمش نماید قفس، بوستان</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چو ماهی ز سرچشمه افتاده دور</p></div>
<div class="m2"><p>سوی تابه‌اش می‌کشد بخت شور</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>بد و نیک را جا به مامن خوش است</p></div>
<div class="m2"><p>اگر خار، اگر گل، به گلشن خوش است</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>به گیتی اگر پادشا، ور گداست</p></div>
<div class="m2"><p>چو افتاد از جای خود، بینواست</p></div></div>