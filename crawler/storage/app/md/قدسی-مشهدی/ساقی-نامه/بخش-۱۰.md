---
title: >-
    بخش ۱۰
---
# بخش ۱۰

<div class="b" id="bn1"><div class="m1"><p>ز چشم ضعیفان گو افتاده‌تر</p></div>
<div class="m2"><p>در او گنج قارون عیان در نظر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قضا کرد چون خندقش را شکاف</p></div>
<div class="m2"><p>برآورد سامان صد کوه قاف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به فرض ار به قعرش فتد آفتاب</p></div>
<div class="m2"><p>دگر برنیاید به چندین طناب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حصارش به این قلعه گردد قرین</p></div>
<div class="m2"><p>شود آسمان گر مربع‌نشین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکفت از فصیلش سپهر کبود</p></div>
<div class="m2"><p>مگر زهره را شانه در کار بود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طلسمی چنین را ز نام‌آوران</p></div>
<div class="m2"><p>کسی نشکند غیر صاحبقران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سرکوب برجش درین نُه حصار</p></div>
<div class="m2"><p>نباشد دمی پاسبان را قرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببالد مگر عمرها طاق عرش</p></div>
<div class="m2"><p>که تا پای برجش رسد ساق عرش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز سختی به خیبر بود توامان</p></div>
<div class="m2"><p>به رفعت گرو برده از آسمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندیده چنین قلعه‌ای چرخ پیر</p></div>
<div class="m2"><p>چو آسیر، هرسو هزارش اسیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حصاری به رفعت ز گردون فزون</p></div>
<div class="m2"><p>ز برجش ستون بر سر بیستون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر از نه فلک بگذراند طناب</p></div>
<div class="m2"><p>نیابد بر این قلعه دست آفتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به گردون‌نوردی ازان است طاق</p></div>
<div class="m2"><p>که یک بار پیموده راهش بُراق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز پیرامنش اختران نیک‌بخت</p></div>
<div class="m2"><p>ز نظّاره‌اش دیده‌ها گشته سخت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به ذکرش گشاید قلم گر زبان</p></div>
<div class="m2"><p>سخن را رسد پایه بر آسمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نمایان ز هر فُرجه، توپ دگر</p></div>
<div class="m2"><p>ز روزن برو کرده آشوب سر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که کرد این بنا را به این محکمی</p></div>
<div class="m2"><p>نمی‌آید این کار از آدمی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مگر راست کردند دیوان به جهد</p></div>
<div class="m2"><p>حصاری ز بهر سلیمان عهد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دری را که پیدا نمی‌شد کلید</p></div>
<div class="m2"><p>به دوران شاه جهان شد پدید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فتادش به زیر آفتاب از فراز</p></div>
<div class="m2"><p>شکسته‌ست رنگش ازان روز باز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به سنگش مکن آسمان گو ستیز</p></div>
<div class="m2"><p>که چون شیشه خواهد شدن ریزریز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که دید آسمانی ز یک پاره‌سنگ؟</p></div>
<div class="m2"><p>که تیر شهابش بود از تفنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همه آهنین حربه‌اش جا‌به‌جاست</p></div>
<div class="m2"><p>مگر سنگ این قلعه آهنرباست؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ندارد چنین قلعه‌ای چرخ یاد</p></div>
<div class="m2"><p>که تا خاکریزش نرفته‌ست باد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حصاری نمودار چرخ بلند</p></div>
<div class="m2"><p>ولی مهر را کوته از وی کمند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گذشته ز برج فلک، باره‌اش</p></div>
<div class="m2"><p>شکسته فلک شیشه بر خاره‌اش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سر کنگرش پیش فرزانه‌ها</p></div>
<div class="m2"><p>کلید دکن راست، دندانه‌ها</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز فتحش فتوحات آید پدید</p></div>
<div class="m2"><p>که دیده‌ست قفلی سراسر کلید؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به وصفش کنم ناخن فکر، بند</p></div>
<div class="m2"><p>که گر افتم، افتم به فکر بلند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به اوجش به همت توان برد راه</p></div>
<div class="m2"><p>نه هر همتی، همت پادشاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خداوند اقبال، شاه جهان</p></div>
<div class="m2"><p>کزو فتح شد قلعه آسمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کشد قبضه تیغش از اقتدار</p></div>
<div class="m2"><p>ز فولاد، بر گرد عالم حصار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز سرکوب عدلش، حصار ستم</p></div>
<div class="m2"><p>فتاده‌ست در خاکریز عدم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بود آیت سجده‌اش بر جبین</p></div>
<div class="m2"><p>که هرکس که خواند، ببوسد زمین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز کشورستانان این آستان</p></div>
<div class="m2"><p>کمین بنده پادشاه جهان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو آهنگ تسخیر کشور کند</p></div>
<div class="m2"><p>حصار فلک را مسخر کند</p></div></div>
<div class="b2" id="bn37"><p>***</p></div>
<div class="b" id="bn38"><div class="m1"><p>زهی برج شاهنشه دین پناه</p></div>
<div class="m2"><p>که هم شاه برج است و هم برج شاه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اساسی چو بنیاد دولت قوی</p></div>
<div class="m2"><p>جهان کهن را بنای نوی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به سویش کند ماه اگر کج نگاه</p></div>
<div class="m2"><p>کشد کنگرش اره بر فرق ماه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در اتمام این قصر گوهرنگار</p></div>
<div class="m2"><p>بقا عمرها بوده مزدورکار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زهی خوش اساسی و پایندگی</p></div>
<div class="m2"><p>که قصر بهشتش کند بندگی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تهی کرد گیتی بسی دُرج را</p></div>
<div class="m2"><p>که پر کرد از گوهر این برج را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز چینی پر از ظرف‌های گزین</p></div>
<div class="m2"><p>که هرگز ندیده‌ست فغفور چین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شد از لعل و یاقوت، جوهرنشان</p></div>
<div class="m2"><p>مگر دسته گردیده رگ‌های کان؟</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز یاقوت و لعلش بود سنگ و خشت</p></div>
<div class="m2"><p>چنین برج باشد مگر در بهشت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز بالای این برج گردون سپر</p></div>
<div class="m2"><p>چو فواره می‌جوشد آب گهر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>طلاکاری‌اش سربه‌سر دلپذیر</p></div>
<div class="m2"><p>نهان کرده زر در عصا چرخ پیر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تمام از زر پخته و سیم خام</p></div>
<div class="m2"><p>درونش مرصع چو بیرون جام</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در آیینه کاری ندارد قرین</p></div>
<div class="m2"><p>همین است دُرج پر اختر، همین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز جامش عرقناک، یاقوت تر</p></div>
<div class="m2"><p>زمینش صدف‌وار فرش از گهر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گل این‌جا ز یاقوت احمر بود</p></div>
<div class="m2"><p>چو نسرین که از سنگ مرمر بود</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>درش را رسد بر در کعبه ناز</p></div>
<div class="m2"><p>که برعکس آن است پیوسته باز</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ز بس چشم و زلف بتان طراز</p></div>
<div class="m2"><p>ز زنجیر و زرفین بود بی‌نیاز</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بود بر فرازش کبوتر ملک</p></div>
<div class="m2"><p>ستونی بود زیر سقف فلک</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز بالایش آید کسی چون به زیر</p></div>
<div class="m2"><p>توان سیر افلاک کردن دلیر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>عروس جهان کرده ساعد بلند</p></div>
<div class="m2"><p>که در جیب گردون کند پنجه بند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چه شمعی‌ست این برج عالی جناب</p></div>
<div class="m2"><p>که پروانه‌اش درخورست آفتاب</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز هر روزنش مشرقی جلوه‌گر</p></div>
<div class="m2"><p>که خورشیدی از جام دارد به بر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>عمود فلک گر دهندش قرار</p></div>
<div class="m2"><p>بماند بنای فلک پایدار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مکن بهر فردوس اینجا تلاش</p></div>
<div class="m2"><p>نه‌ای چون کبوتر، دو برجی مباش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به گردش بود آسمان را مدار</p></div>
<div class="m2"><p>که بی برج، صورت نگیرد حصار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نیابد خلل دست بر آسمان</p></div>
<div class="m2"><p>بود پای این برج تا در میان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سر کنگرش در مقام عتاب</p></div>
<div class="m2"><p>کند پنجه در پنجه آفتاب</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نگردد ازان سبزه چرخ، زرد</p></div>
<div class="m2"><p>که دارد ز فوّاره‌اش آبخورد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>اگر بیندش چشم هندوسرشت</p></div>
<div class="m2"><p>بدیهیش گردد وجود بهشت</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>جهان‌دیده گوید به بانگ دهل</p></div>
<div class="m2"><p>که این چارباغ‌است، یک دسته گل</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نظر کن بر این برج نیکوسرشت</p></div>
<div class="m2"><p>که یک دسته شد هشت باغ بهشت</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چو در وی نشیند شه کامیاب</p></div>
<div class="m2"><p>به برج حمل جا کند آفتاب</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>پی دست قدرت برد آستین</p></div>
<div class="m2"><p>همین است معراج دولت، همین</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بود مجلس خاص شاه جهان</p></div>
<div class="m2"><p>ننازد به این برج چون آسمان؟</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>حمل باشد از نسبتش کامیاب</p></div>
<div class="m2"><p>ازین برج یابد شرف آفتاب</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به خوبی‌ست چشم و چراغ جهان</p></div>
<div class="m2"><p>بود دسته‌ای گل ز باغ جهان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تهی نیست یک لحظه از شمع دین</p></div>
<div class="m2"><p>همین است فانوس قدرت، همین</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>زهی دست معمار معجزنمای</p></div>
<div class="m2"><p>که داد آسمان را به یک برج جای</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تو گویی اساسش ز بس محکمی</p></div>
<div class="m2"><p>ز دل‌های سنگین ندارد کمی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ببین این بنا را چه دولت بود</p></div>
<div class="m2"><p>که فانوس شمع سعادت بود</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>فنا پیش ازین هرچه می‌خواست، کرد</p></div>
<div class="m2"><p>بقا زین بنا قامتی راست کرد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>درونش ز نقاشی رنگ‌زنگ</p></div>
<div class="m2"><p>به هم جای آمیزش رنگ، تنگ</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>تماشای نقاشی این بنا</p></div>
<div class="m2"><p>نگه را کند محو در دیده‌ها</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ز خامی به نقشش مبین بی‌درنگ</p></div>
<div class="m2"><p>مشو غافل از پختگی‌های رنگ</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>به نقاشی این بهشت برین</p></div>
<div class="m2"><p>چها کرده نقاش سحرآفرین</p></div></div>
<div class="b2" id="bn83"><p>***</p></div>
<div class="b" id="bn84"><div class="m1"><p>زهی سحرپرداز صورت‌نگار</p></div>
<div class="m2"><p>که معنی ز صورت کند آشکار</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>کشد خضر کلکش که معجز نماست</p></div>
<div class="m2"><p>رهی از مخالف به مغلوب، راست</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>اگر پیکری را کشد رعشه‌ناک</p></div>
<div class="m2"><p>چو برگ خزان‌دیده افتد به خاک</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چو خواهد کند نقش سروی درست</p></div>
<div class="m2"><p>کشد شکل آزادی‌اش را نخست</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>نگارد چو در خانه‌ای آفتاب</p></div>
<div class="m2"><p>در آن خانه شب درنیاید به خواب</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>شود پیکری را چو صورت‌نگار</p></div>
<div class="m2"><p>کشد معنی‌اش را نخست آشکار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>اگر شکل مشرق کشد شب به خواب</p></div>
<div class="m2"><p>همان لحظه طالع شود آفتاب</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>نهالی که از کلک او رسته است</p></div>
<div class="m2"><p>ز تردستی‌اش میوه رو شسته است</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چو گیرد به کف کلک گلشن‌نگار</p></div>
<div class="m2"><p>دمد شاخ نسرین چو صبح آشکار</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>وزد گر نسیمی در آن بوستان</p></div>
<div class="m2"><p>ز غیرت رمد بلبل از آشیان</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ز شبنم عذار گلش در گلاب</p></div>
<div class="m2"><p>جهد غنچه از جوش بلبل ز خواب</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>به تصویر گل، غنچه ناکرده روی</p></div>
<div class="m2"><p>صبا برده عطر گلش کو به کوی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>ز مو چهره گل نپرداخته</p></div>
<div class="m2"><p>که زد بر نوا بلبل ساخته</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>کشد بر ورق تا شتاب و درنگ</p></div>
<div class="m2"><p>پی رفتن گل ز رنگی به رنگ</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>قلم شکل سروی نپرداخته</p></div>
<div class="m2"><p>که بست آشیان بر سرش فاخته</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>هنوزش قلم کار در لاله داشت</p></div>
<div class="m2"><p>که در چشم نرگس نظر می‌گماشت</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>کشد شکل خار آنچنان آب‌دار</p></div>
<div class="m2"><p>که روید ز تردستی‌اش گل ز خار</p></div></div>