---
title: >-
    بخش ۲۴
---
# بخش ۲۴

<div class="b" id="bn1"><div class="m1"><p>به مغرب ازان مهر شد زردچهر</p></div>
<div class="m2"><p>که از خاک مشرق زمین زاد مهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دریا چو شد قطره‌ای بی‌نصیب</p></div>
<div class="m2"><p>نپاید بسی، بس که باشد غریب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازان شمع افراخت بالای خود</p></div>
<div class="m2"><p>که پا برنمی‌دارد از جای خود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرنجان غریب دل‌افسرده را</p></div>
<div class="m2"><p>که مردی نباشد لگد مرده را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا بود در ملک خود جای گرم</p></div>
<div class="m2"><p>به مهرم دل نیک و بد بود نرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبودم دل‌آزرده از هیچ‌کس</p></div>
<div class="m2"><p>به کام دلم بخت می‌زد نفس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه کار و بارم سرانجام داشت</p></div>
<div class="m2"><p>دلم طایر عیش در دام داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دل تخم غربت نمی‌کاشتم</p></div>
<div class="m2"><p>صدف‌وار، جا در گهر داشتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی‌کرد طبعم هوای سفر</p></div>
<div class="m2"><p>نبودم هوایی برای سفر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهی طالع و بخت ناارجمند</p></div>
<div class="m2"><p>که قسمت ز ایران به هندم فکند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا آنچنان بخت در آب راند</p></div>
<div class="m2"><p>که آخر به خاک سیاهم نشاند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شکایت ندارم ز هندوستان</p></div>
<div class="m2"><p>به جانم ز بی‌مهری دوستان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا بارالها به ایران رسان</p></div>
<div class="m2"><p>به درگاه شاه خراسان رسان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به جایی ازان آستانم مبر</p></div>
<div class="m2"><p>که باشد صدف، جای امن گهر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ندانسته‌ام قدر کالای خویش</p></div>
<div class="m2"><p>پشیمانم از عزم بی‌جای خویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وطن هم ز حرمان من گشته داغ</p></div>
<div class="m2"><p>مبادا ز بلبل تهی، صحن باغ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیابی به گلشن گل و لاله‌ای</p></div>
<div class="m2"><p>که گوشی ندارند بر ناله‌ای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز گلشن چو بیرون رود عندلیب</p></div>
<div class="m2"><p>شود گوش گل از نوا بی‌نصیب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پریشان بود بی‌شکن زلف یار</p></div>
<div class="m2"><p>چمن بی‌طراوت بود بی بهار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برازنده گوشوارست گوش</p></div>
<div class="m2"><p>خم از باده آید به جوش و خروش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وطن را دل از غربتم گشت داغ</p></div>
<div class="m2"><p>ز روغن دهد روشنایی چراغ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به سامان نماند تهی گشته کاخ</p></div>
<div class="m2"><p>ثمرگر نباشد، چه حاصل ز شاخ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چراغ تن از نور جان روشن است</p></div>
<div class="m2"><p>ز آیینه آیینه‌‌دان روشن است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نگه‌دار در خانه خویش، جای</p></div>
<div class="m2"><p>نگین در نگین‌دان بود خوش‌نمای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من ناتوان را مبین خوار و زار</p></div>
<div class="m2"><p>به مژگان بود دیده را اعتبار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به روی خراشیده من مبین</p></div>
<div class="m2"><p>که زیب نگین‌خانه باشد نگین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر بیش اگر کم، رضایم رضا</p></div>
<div class="m2"><p>مرا شکر نعمت نگردد قضا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز ایران به هندوستان آمدم</p></div>
<div class="m2"><p>به امّید گوهر به کان آمدم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به دست آمد از بخت، آن گوهرم</p></div>
<div class="m2"><p>که در هند، حسرت به ایران خورم!</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قفس زآهن و مرغ بی بال و پر</p></div>
<div class="m2"><p>به گلشن که از ما رساند خبر؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دریغا که عنقاست یک آشنا</p></div>
<div class="m2"><p>که از من به ایران رساند دعا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>الهی تو دردم به درمان رسان</p></div>
<div class="m2"><p>مرا بار دیگر به ایران رسان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به وصل خراسان دلم شاد کن</p></div>
<div class="m2"><p>ز هند جگرخوارم آزاد کن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سزاوار بخت‌ارجمندی نیم</p></div>
<div class="m2"><p>همین عیب من بس، که هندی نیم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>درین ملکم اعزاز و اکرام هست</p></div>
<div class="m2"><p>مرا هم به قدر هنر، نام هست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنان بر خود از ذوق بالیده‌ام</p></div>
<div class="m2"><p>که چون نغمه در تار گنجیده‌ام!</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مرا شعر تر از وطن رخت بست</p></div>
<div class="m2"><p>گهر زآب خود شوید از بحر، دست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به من بی‌کسی راست ربط قدیم</p></div>
<div class="m2"><p>ز بطن صدف گوهر آمد یتیم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>توطّن کسی را که در طوس نیست</p></div>
<div class="m2"><p>بر اوقات خویشش جز افسوس نیست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گر از خار، گل را به خنجر زنند</p></div>
<div class="m2"><p>ازان به که چینند و بر سر زنند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جدایی ز پروردگان است سخت</p></div>
<div class="m2"><p>بود کنده پای دهقان، درخت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دو چشم امیدم به ره گشته چار</p></div>
<div class="m2"><p>که قاصد کی آید ز یار و دیار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کسی کز می انتظارست مست</p></div>
<div class="m2"><p>به آواز پایی دهد دل ز دست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>درین تنگنا، رستن از قید به</p></div>
<div class="m2"><p>چو آواز نی می‌جهم از گره</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به صورت غریبم، به معنی غریب</p></div>
<div class="m2"><p>به شاه غریبان رسم عنقریب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>فلک زود آسود از مهر، زود</p></div>
<div class="m2"><p>چه افسرده بوده‌ست این مشت دود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به عزت، بنای که را برفراخت؟</p></div>
<div class="m2"><p>که آخر به خواری خرابش نساخت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>که را برد طالع به چرخ برین؟</p></div>
<div class="m2"><p>که آخر نینداختش بر زمین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کسی را که بالا برد روزگار</p></div>
<div class="m2"><p>رساند به گردونش از راه دار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز گردون تهی دار پهلو، تهی</p></div>
<div class="m2"><p>که پهلو ندارد به این فربهی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مدار فلک را رها کن، رها</p></div>
<div class="m2"><p>که بی دانه ننشسته این آسیا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نکرد آسمان خانه‌ای را بنا</p></div>
<div class="m2"><p>که آخر ندادش به سیل فنا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چه رنگین بنا این چمن راست یاد</p></div>
<div class="m2"><p>که چون برگ گل رفته حسنش به باد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>عمارت مکن خانه زرنگار</p></div>
<div class="m2"><p>درین خاک، تخم خرابی مکار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چه عالی بناهای نیکوسرشت</p></div>
<div class="m2"><p>که شد خاک و دهقان در آن دانه کشت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بسی خانه باید ز بنیاد کند</p></div>
<div class="m2"><p>که تا گردد ایوان قصری بلند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز حرص افکنی بر بنایی شکست</p></div>
<div class="m2"><p>که گردی ازان بر تو خواهد نشست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نمود فلک را نباشد قرار</p></div>
<div class="m2"><p>بود رنگ فیروزه ناپایدار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>جهانت بود گر به زیر نگین</p></div>
<div class="m2"><p>بود عاقبت از جهان‌آفرین</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بسی نام کاین گنبد لاجورد</p></div>
<div class="m2"><p>به سنگ مزار از نگین نقل کرد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بسی عالم‌آرا به چرخ کبود</p></div>
<div class="m2"><p>چو خورشید بررفت و آمد فرود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نبینی درین بوستان یک گیاه</p></div>
<div class="m2"><p>که ننشسته باشد به خاک سیاه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چنان بی‌ثبات است این بوستان</p></div>
<div class="m2"><p>که سبقت کند بر بهارش خزان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>درختی نبینی ز نو یا کهن</p></div>
<div class="m2"><p>که از باد صرصر نیفتد ز بُن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کسی میوه کام ازین بوستان</p></div>
<div class="m2"><p>نچیده به کام دل دوستان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>اگر پخته این میوه، گر نارس است</p></div>
<div class="m2"><p>ز برچیدنی‌هاش، دامن بس است</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>درین بوستان برگ سبزی نخاست</p></div>
<div class="m2"><p>که باد خزانش به زردی نکاست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>درین بزم، شمعی نشد سرفراز</p></div>
<div class="m2"><p>که در سرفرازی نبودش گذاز</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>برو تکیه بر جاه دنیا مکن</p></div>
<div class="m2"><p>که آن برکند نخلت آخر ز بُن</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>درین بوستان، لاله گر نیست داغ</p></div>
<div class="m2"><p>چرا برنکرد از ته دل، چراغ</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تو را کرده گلبن پر از گل کنار</p></div>
<div class="m2"><p>به نیک و بد بوستانت چه کار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چه کارت به نخل بلندست و پست</p></div>
<div class="m2"><p>مکن ارّه شاخی که خواهد شکست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>درین بوستان، دل مده جز به خار</p></div>
<div class="m2"><p>خریداری گل به بلبل گذار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به زرق و ریا، لاله این چمن</p></div>
<div class="m2"><p>عصا و ردا کرده جزو بدن</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به گلشن نماند ازان پایدار</p></div>
<div class="m2"><p>که بخشیده گل عمر خود را به خار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>سر زلف سنبل به جز پیچ نیست</p></div>
<div class="m2"><p>به جز تاب در روی گل هیچ نیست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>کجا لاله را برفروزد چراغ؟</p></div>
<div class="m2"><p>نباشد اگر در میان، پای داغ</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>دماغی ندارد بنفشه مگر؟</p></div>
<div class="m2"><p>که با گل کند بی‌دماغانه سر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به سنبل درین بوستان هم‌فنیم</p></div>
<div class="m2"><p>پریشان‌دماغان این گلشنیم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>گل از هفته‌ای بیش بر بار نیست</p></div>
<div class="m2"><p>ولی شاخ یک روز بی خار نیست</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چمن با بهار و خزان کرده خو</p></div>
<div class="m2"><p>که این شستشو داده، آن رُفت‌ورو</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>درین گلستان، جای آرام نیست</p></div>
<div class="m2"><p>سحر گر شکفتش گلی، شام نیست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ز گلشن همینم خوش افتاده است</p></div>
<div class="m2"><p>که از راستی، سرو آزاده است</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>تو را راستی از کجی وارهاند</p></div>
<div class="m2"><p>نگویی که از رستگاری چه ماند</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>اگر راستی، جز پی دل مرو</p></div>
<div class="m2"><p>که پیکان بود تیر را پیشرو</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بود راست، ره، مرد آگاه را</p></div>
<div class="m2"><p>گر افعی نه‌ای، کج مرو راه را</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>مکش از ره راست پا، کان خوش است</p></div>
<div class="m2"><p>کجی در سر زلف خوبان خوش است</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>به طبعم کجا فکر کج آشناست</p></div>
<div class="m2"><p>بود راست‌رو آب در جوی راست</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ز فانوس بر شمع گردد عیان</p></div>
<div class="m2"><p>که محفل بود تنگ بر راستان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>به مقصد مکن راست‌رو گو شتاب</p></div>
<div class="m2"><p>دهد بوسه پای چپ اول رکاب</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>به دنیا مزن دست زاندازه بیش</p></div>
<div class="m2"><p>مکن طوق گردن قوی بهر خویش</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>فزون عمرت از ترک دنیا شود</p></div>
<div class="m2"><p>کشد رشته قد، چون گره وا شود</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>تردد مکن بهر میراث‌خوار</p></div>
<div class="m2"><p>تویی ضامن رزق، یا کردگار؟</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>کنی عمر صرف و نداری الم</p></div>
<div class="m2"><p>غمینی چو دانگی شد از کیسه کم</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>پی ناخوشی تا کی این خودکشی؟</p></div>
<div class="m2"><p>همانا که در زحمتی از خوشی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>ز دست تهی نالی و کیسه پر</p></div>
<div class="m2"><p>اگر مستحقی، زکاتش بخور</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>به زر، بت‌پرستیت ازان نقش بست</p></div>
<div class="m2"><p>که هر زرپرستی بود بت‌پرست</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>برای جدل چاره‌ای کن نکو</p></div>
<div class="m2"><p>که چاک گریبان گذشت از رفو</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>بسی جامه از چرم در بر کنی</p></div>
<div class="m2"><p>که یک چرم صندوق، پر زر کنی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>عبث پاسبان را میفکن به رنج</p></div>
<div class="m2"><p>به از اژدهایی تو در پاس گنج</p></div></div>
<div class="b" id="bn101"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>