---
title: >-
    بخش ۱۹
---
# بخش ۱۹

<div class="b" id="bn1"><div class="m1"><p>نه در دیده نور و نه در دل حضور</p></div>
<div class="m2"><p>بود پیر افتاده را خانه گور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پیری مدار از جوانی امید</p></div>
<div class="m2"><p>نگردد سیه باز، موی سفید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز رنگ طبیعی مکن اجتناب</p></div>
<div class="m2"><p>نشاید جوان شد به موی خضاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سفیدی مو شد به پیری نصیب</p></div>
<div class="m2"><p>شکوفه پس از میوه باشد غریب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو را گشت سنبل به نسرین بدل</p></div>
<div class="m2"><p>نچیدی گل از باغ حسن عمل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مسازش عوض، شد چو دندان تلف</p></div>
<div class="m2"><p>کجا می‌کند کار گوهر، صدف؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز پیری چو افتاد بر چهره چین</p></div>
<div class="m2"><p>به خود از جوانی بساطی مچین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر آن پیر خندد اجل دم‌به‌دم</p></div>
<div class="m2"><p>که گیرد خم زلف با پشت خم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هنگام پیری مکن ساز و برگ</p></div>
<div class="m2"><p>که آغاز پیری‌ست انجام مرگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به درمان، جوان را بود احتیاج</p></div>
<div class="m2"><p>شود درد پیری به مردن علاج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به پیری مکن زندگانی هوس</p></div>
<div class="m2"><p>جوانی بود زندگانی و بس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دریغا که عهد جوانی گذشت</p></div>
<div class="m2"><p>جوانی مگو، زندگانی گذشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز پیران شعار جوانی مجوی</p></div>
<div class="m2"><p>چو پژمرده شد گل، چه رنگ و چه بوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مزن پیر از ضعف گو پیچ و تاب</p></div>
<div class="m2"><p>شود زرد، وقت غروب آفتاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز پیران رطوبت مجو در دماغ</p></div>
<div class="m2"><p>که بی‌ روغن، افسرده باشد چراغ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مکن پیر گو دعوی سرکشی</p></div>
<div class="m2"><p>ز خاکستر آید کجا آتشی؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برو خنده بر ضعف پیران مزن</p></div>
<div class="m2"><p>تو هم ای جوان، پیر خواهی شدن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بود پیر، افتاده روزگار</p></div>
<div class="m2"><p>بر افتادگان پا مزن زینهار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنان قطع شد از جوانی امید</p></div>
<div class="m2"><p>که چون زال، مو روید از تن سفید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مکن از حنا موی خود رنگ بست</p></div>
<div class="m2"><p>جوانی به نیرنگ ناید به دست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سفید و سیاه از تو گردد به خشم</p></div>
<div class="m2"><p>که با ظلمت موی، شد نور چشم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا کرده پیری چنان ناامید</p></div>
<div class="m2"><p>که پیش جوانان نگردم سفید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو صبح آن که مهرش بود بر اثر</p></div>
<div class="m2"><p>جوان خیزد از خواب پیرانه‌سر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شکست آنچنان مو سفیدی پرم</p></div>
<div class="m2"><p>که از بیم، سودا پرید از سرم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جهان را چه دستی‌ست در شستشوی</p></div>
<div class="m2"><p>که بی آب شوید سیاهی ز موی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به پیری ز طفلی شدم هم‌عنان</p></div>
<div class="m2"><p>ندانم جوانی چه شد در میان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به طفلی مرا کس به مکتب نداد</p></div>
<div class="m2"><p>که روشن کنم خود به پیری سواد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سوی مشک من برد کافور راه</p></div>
<div class="m2"><p>یکی شد به چشمم سفید و سیاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز موی سفید آنچنانم نفور</p></div>
<div class="m2"><p>که زنگی به چشمم بود به ز حور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هوا از سرم یک سر مو نرفت</p></div>
<div class="m2"><p>سیاهی ز مو رفت و از رو نرفت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بشد رنگ مژگان چو موی ذقن</p></div>
<div class="m2"><p>جوانی نرفته‌ست از چشم من</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بزن دست و پا تا جوانیت هست</p></div>
<div class="m2"><p>که پیر از عصا بسته بر چوب، دست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برو دل به عهد جوانی منه</p></div>
<div class="m2"><p>که ناداده، ایام گوید بده</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جوانیت بازی‌ست پر کرده باز</p></div>
<div class="m2"><p>جهد از نظر، تا کنی چشم باز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>قدت شد ز پیری چو دال ای علیل</p></div>
<div class="m2"><p>چه دانی که بر مرگ باشد دلیل</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شود چند عینک سپردار چشم؟</p></div>
<div class="m2"><p>نظر کن که از دست شد کار چشم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نظر رخت از دیده برچیده رفت</p></div>
<div class="m2"><p>ز عینک، سپرداری دیده رفت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فلک کاسه زانویت چون شکست</p></div>
<div class="m2"><p>پی مومیایی مکن کفچه دست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نشد از عصا پای سست استوار</p></div>
<div class="m2"><p>چه کار آید از پای چوبین، چه کار؟</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز پیری‌ست کار جوانی محال</p></div>
<div class="m2"><p>کهن نخل، کی بر دهد چون نهال؟</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز پیشانی‌ات تا ذقن چین رسید</p></div>
<div class="m2"><p>چه حاصل ز چینی که مشکش پرید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو نور از نظر رفت و قوت ز پا</p></div>
<div class="m2"><p>چه یاری دهد عینکت یا عصا؟</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بود پیش اهل نظر ناگوار</p></div>
<div class="m2"><p>به امداد عینک تماشای یار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جوان را ز پیری نباشد خبر</p></div>
<div class="m2"><p>ز نخل کهن پرس جور تبر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>فلک در جوانی به کامت نگشت</p></div>
<div class="m2"><p>تو نگذشتی از کام و دوران گذشت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جوانیّ و گرم است هنگامه‌ات</p></div>
<div class="m2"><p>چه می‌آوری بر سر نامه‌ات</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>جوان گرچه سوزد ز حرمان زر</p></div>
<div class="m2"><p>ولی پیر ازان بیش یابد اثر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو از بیشه آتش برآرد دمار</p></div>
<div class="m2"><p>کند بیشتر در نی خشک، کار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خطا گفتم این، خرده بر من مگیر</p></div>
<div class="m2"><p>گدای جوان به ز سلطان پیر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو قدر جوانی ندانسته‌ای</p></div>
<div class="m2"><p>دل خود به شاخ هوس بسته‌ای</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خزان‌دیده به داند از رنگ کار</p></div>
<div class="m2"><p>که دارد چه در بار، نخل از بهار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>جوانی که با رنج و سستی بود</p></div>
<div class="m2"><p>به از پیری و تندرستی بود</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تو را ضعف پیری چو بی‌پا کند</p></div>
<div class="m2"><p>عصا زور حرصت دو بالا کند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پر از چین رخ، آیینه مگذار پیش</p></div>
<div class="m2"><p>به سوهان مکن روی آیینه ریش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز موی سیاه آنکه چیند سفید</p></div>
<div class="m2"><p>کند ناامیدی جدا از امید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شدی پیر و دل از هوس کوبه‌کوی</p></div>
<div class="m2"><p>مگر بر دلت ریخت ظلمت ز موی؟</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>زنی نامه خویش اگر بر کلاه</p></div>
<div class="m2"><p>کند باز موی سرت را سیاه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گرانی ز سر منتقل شد به گوش</p></div>
<div class="m2"><p>سبک شد سراپا، نیاسود دوش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز خاک تو دوران به فکر سبو</p></div>
<div class="m2"><p>تو در استخوان‌بندی آرزو</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>قدت گشت چوگان گوی عصا</p></div>
<div class="m2"><p>همان ذوق بازیت مانده به جا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز پیری چو ایام پشتت شکست</p></div>
<div class="m2"><p>به بازی، عصا نیزه دادت به دست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تنت گر ضعیف، آرزویت قوی‌ست</p></div>
<div class="m2"><p>کهن مرد را، حرص، رو در نوی‌ست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تن آزاده و دل گرفتار حرص</p></div>
<div class="m2"><p>به این ضعف، چون می‌کشی بار حرص؟</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دمادم اجل شحنه‌وارت به زور</p></div>
<div class="m2"><p>کشان می‌برد تا به زندان گور</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>رخ از چین پر از زخم شمشیر مرگ</p></div>
<div class="m2"><p>مچین بر سر یکدگر ساز و برگ</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>اذانی نگفتی و صبحت دمید</p></div>
<div class="m2"><p>بکش قامتی، زان که قامت خمید</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>اجل خشت زیر سرت داده ساز</p></div>
<div class="m2"><p>تو بر نازبالش نهی سر به ناز</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو در خاک، آخر فروکش کنی</p></div>
<div class="m2"><p>برای که ایوان منقّش کنی؟</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بود خوابگاه تو در زیر خاک</p></div>
<div class="m2"><p>به مژگان چرا می‌کنی خانه پاک؟</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>کند عاقبت منتهی ساز و برگ</p></div>
<div class="m2"><p>جوانی به پیریّ و پیری به مرگ</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>یکی در حق عمر خوش گفته است</p></div>
<div class="m2"><p>که رفته‌ست، تا گفته‌ای رفته است</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چنان عمر دارد به رفتن شتاب</p></div>
<div class="m2"><p>که پیریش سبقت کند بر شباب</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>درین بوستان، گر کهن گر نواند</p></div>
<div class="m2"><p>به رنگ گل آیند و چون بو روند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ز حورت چه حاصل به موی چو شیر</p></div>
<div class="m2"><p>که باریک زنگی گریزد ز پیر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>مشو غرّه، گر مادر روزگار</p></div>
<div class="m2"><p>دو روزی به مهرت کشد در کنار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>که شیری که کردت به طفلی به کام</p></div>
<div class="m2"><p>به پیری برآرد چو موی از مسام</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سیاهی ز مویم فلک شُست چُست</p></div>
<div class="m2"><p>ولی سرنوشت بدم را نشست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ز دلقی که شد تار و پودش کهن</p></div>
<div class="m2"><p>نشاید امید نوی داشتن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چو ناچار باید ازین دار رفت</p></div>
<div class="m2"><p>خوش آن‌کس که این ره به هنجار رفت</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>فلک در جوانی حساب از تو داشت</p></div>
<div class="m2"><p>ز پیری، ولی بر سرت پنبه کاشت</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بنا کرده دوران به عزم سفر</p></div>
<div class="m2"><p>ز مو، سایبان سفیدت به سر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>مجو پنبه داغ سودا ز کس</p></div>
<div class="m2"><p>ز موی سرت پنبه داغ، بس</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>گذارد چو در تیرگی بخت رو</p></div>
<div class="m2"><p>برد اندک‌اندک سیاهی ز مو</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>دل از شب به یک دم شود ناامید</p></div>
<div class="m2"><p>شود گرچه صبح اندک‌اندک سفید</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>سیاهیّ مو، ماند بسیار کم</p></div>
<div class="m2"><p>که دیده‌ست زاغی چنین زودرَم؟</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>شده خدمن ریش، جو گندمت</p></div>
<div class="m2"><p>جوی شرم چون نیست از مردمت؟</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چرا جوفروشیّ و گندم‌نمای؟</p></div>
<div class="m2"><p>ز خلق ار نترسی، بترس از خدای</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>مسم بود از صحبت او طلا</p></div>
<div class="m2"><p>کجا شد عیار جوانی، کجا؟</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>قد از ضعف پیری شده چون کمان</p></div>
<div class="m2"><p>همین پوستی مانده بر استخوان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>مکن تا ز دستت برآید، چنان</p></div>
<div class="m2"><p>که خود پیر باشیّ و حرصت جوان</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>دل خویش را از هوس پاک کن</p></div>
<div class="m2"><p>طمع را ز خود پیش در خاک کن</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>سر ره دگر بر که گیری به دشت؟</p></div>
<div class="m2"><p>که چون سیل عهد جوانی گذشت؟</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>به دوران ما رفت دور شباب</p></div>
<div class="m2"><p>تو گویی که بود آبروی حباب</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بدن را نماند به جا آبرو</p></div>
<div class="m2"><p>قوی چون شود ضعف پیری بر او</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>دم از عجز پیری چرا می‌زنی؟</p></div>
<div class="m2"><p>که بی دست و پا، دست و پا می‌زنی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>مزن دم ز زور قدیمی، مزن</p></div>
<div class="m2"><p>که از بادی افتد درخت کهن</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>کسی را کند گر اجل دستگیر</p></div>
<div class="m2"><p>ازان به، که پیریش سازد اسیر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>به دندان چو شد رخنه‌افکن قضا</p></div>
<div class="m2"><p>درآید ازان رخنه ضعف قوا</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>شدی پیر و انداز می می‌کنی</p></div>
<div class="m2"><p>نگویی ز می توبه کی می‌کنی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>سری در جوانی به طاعت برآر</p></div>
<div class="m2"><p>که افسوس پیری نیاید به کار</p></div></div>