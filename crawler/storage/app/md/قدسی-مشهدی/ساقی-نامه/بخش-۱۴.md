---
title: >-
    بخش ۱۴
---
# بخش ۱۴

<div class="b" id="bn1"><div class="m1"><p>مرا بود از دوستان دوستی</p></div>
<div class="m2"><p>که بودیم چون مغز در پوستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدقّق چنان در خفیّ و جلی</p></div>
<div class="m2"><p>که از دقتش دق کند بوعلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رصدبند قانون ناز و نیاز</p></div>
<div class="m2"><p>ز دل ره به دل کن چو تسبیح ساز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر صدق کیشان ز جوش و خروش</p></div>
<div class="m2"><p>چو صبح از گریبان برآید به جوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو افکند صبح ضمیرش نقاب</p></div>
<div class="m2"><p>نهد بر زمین پشت دست آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیرزد به سیم دغل بی خلاف</p></div>
<div class="m2"><p>برش تیزبازاری موشکاف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شفا، یک مسیحادم از کوی او</p></div>
<div class="m2"><p>اشارات، درسی ز ابروی او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود علم اشفاق بر طاق ازو</p></div>
<div class="m2"><p>رسیده به معراج، اشراق ازو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به مشّائیان دامنی برفشاند</p></div>
<div class="m2"><p>کزان فلسفی را بر آتش نشاند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تن بخردی، جان فهمیدگی</p></div>
<div class="m2"><p>ازو تازه، ایمان فهمیدگی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر علم او، علم‌ها جهل محض</p></div>
<div class="m2"><p>..............................</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>..............................</p></div>
<div class="m2"><p>ز انشای او نقره تازگی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تتبّع ز املای او برملا</p></div>
<div class="m2"><p>نمایان ادافهمی‌اش از ادا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پذیرد ز آشفتگی، خرمی</p></div>
<div class="m2"><p>برش کار درهم، کند درهمی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گلیم ضلالت ازو تارتار</p></div>
<div class="m2"><p>برآورده از جهل، علمش دمار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که دید از خراسان چنین گوهری؟</p></div>
<div class="m2"><p>که هر ذره دارد به مهرش سری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غباری که برخیزد از خاوران</p></div>
<div class="m2"><p>بود سرمه چشم یونانیان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز یونان فهمیدگی هرکه خاست</p></div>
<div class="m2"><p>بود پیش حرفش الف‌وار، راست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر خواند از حکمتش یک ورق</p></div>
<div class="m2"><p>ارسطو بشوید کتاب از عرق</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دهد جکمتش می چو در پای خم</p></div>
<div class="m2"><p>فلاطون می‌اش را بود لای خم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به انداز معنی چنان می‌رسد</p></div>
<div class="m2"><p>که جویای گوهر به کان می‌رسد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو بیند ز کس نقطه‌ای را سقیم</p></div>
<div class="m2"><p>به بالین ز اصلاحش آرد حکیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه از کم کند کم، نه از بیش بیش</p></div>
<div class="m2"><p>بود محض انصاف در کار خویش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قبولش ز صد نکته بوالعجب</p></div>
<div class="m2"><p>به تحسین بیجا نجنبانده لب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز قدر سخن، با سخن اکتساب</p></div>
<div class="m2"><p>کند آنچه با کان کند آفتاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خمی در نمازش مسلم بود</p></div>
<div class="m2"><p>به این راستی آدمی کم بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کند زندگی بر مراد سخن</p></div>
<div class="m2"><p>چو او کی رسد کس به داد سخن؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر زر به خروار، اگر در به من</p></div>
<div class="m2"><p>نگیرد ز کس تحفه غیر از سخن</p></div></div>
<div class="b2" id="bn29"><p>***</p></div>
<div class="b" id="bn30"><div class="m1"><p>شبی شد مرا زالکی میهمان</p></div>
<div class="m2"><p>که زال فلک بود پیشش جوان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز تاریخ خود یاد آرد همین</p></div>
<div class="m2"><p>که آمد ملک پیش ازو بر زمین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خجل ششدر ابرویش از گشاد</p></div>
<div class="m2"><p>وجودش خیالی چو خال زیاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همین است از سن خویشش به یاد</p></div>
<div class="m2"><p>که پیش از ازل داده دندان به باد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جهان بود از روز و شب ناامید</p></div>
<div class="m2"><p>که می‌گشت موی سیاهش سفید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به صد قرن پیش از فلک گشته پیر</p></div>
<div class="m2"><p>ازل شسته در پیش او لب ز شیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>لب گور، خندان ز خندیدنش</p></div>
<div class="m2"><p>اجل مویه بر گر خود از دیدنش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز بس ناتوانی قدش کرده خم</p></div>
<div class="m2"><p>طبق‌زن شده فرج و بینی به هم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به تنگ آمده گوشه‌گیری ازو</p></div>
<div class="m2"><p>کمانی که دیده‌ست تیری ازو؟</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کند گر ز گیسوی خود گرد پاک</p></div>
<div class="m2"><p>کند جای چون دانه در زیر خاک</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>درین خاکش آب و هوا ساخته</p></div>
<div class="m2"><p>چو مشک، آب در پوست انداخته</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز چشمش که از روشنی ساده است</p></div>
<div class="m2"><p>گو افتادن، اندر گو افتاده است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شده میخ‌کوب قدم مشت او</p></div>
<div class="m2"><p>خمیدن خمیده‌ست در پشت او</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو نی پوستش خشک بر استخوان</p></div>
<div class="m2"><p>ز تحریک باد نفس در فغان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز تحریک گیسو، تنش دردناک</p></div>
<div class="m2"><p>برای اجل، تلّه زیر خاک</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>فرو ریزد از رعشه دستش ز هم</p></div>
<div class="m2"><p>چو دست لئیمان ز باد کرم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ضعیفیش از پوست برچیده آب</p></div>
<div class="m2"><p>چو مشکی که خشکیده در آفتاب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو یاران ناساز از یکدگر</p></div>
<div class="m2"><p>ندارند اعضایش از هم خبر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تن از بی‌غذاییش چون نال بود</p></div>
<div class="m2"><p>که قوتش همین خوردن سال بود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نیالوده از لقمه کام هوس</p></div>
<div class="m2"><p>غذایش همین خوردن سال و بس</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>که دیده‌ست زالی به سامان چنین</p></div>
<div class="m2"><p>ز چین، فرج بالای هم تا جبین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>عذارش کبود ابلق از خال نیل</p></div>
<div class="m2"><p>فروهشته بینی چو خرطوم فیل</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دو دندان پیشش به حدی دراز</p></div>
<div class="m2"><p>که با آن کند بند شلوار باز</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بر اعضای او رسته موی درشت</p></div>
<div class="m2"><p>ز قاقم برش نرم‌تر، خارپشت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ز چرخ کهنسال، بدپیرتر</p></div>
<div class="m2"><p>ز نقد بخیلان زمین‌گیرتر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سرش گشته خالی به سودا ز هوش</p></div>
<div class="m2"><p>زبانش ز شیر سخن، پاک‌دوش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>وجودش سبک‌تر ز بال مگس</p></div>
<div class="m2"><p>همین در تنش جان گران بود و بس</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز گند دهانش نفس در گریز</p></div>
<div class="m2"><p>ز نور نظر، دیده‌اش پاک‌بیز</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کدویی‌ست از مغز خالی سرش</p></div>
<div class="m2"><p>اسیر بلا رعشه در پیکرش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز سرپنجه با رعشه دارد ستیز</p></div>
<div class="m2"><p>حصار اجل را تنش خاکریز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سر رفته در دوش را، چون کشف</p></div>
<div class="m2"><p>برآرد گهی بهر آب و علف</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گه از چرخ نالان بود چرخه‌وار</p></div>
<div class="m2"><p>گه از ضعف پیچان چو تار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گرو برده رویش به سردی ز دی</p></div>
<div class="m2"><p>اجل جان نبرده ز دیدار وی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به نادیدنش زندگی در گرو</p></div>
<div class="m2"><p>کند داس ابروی او جان درو</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>اجل را ز دیدار او صد فتوح</p></div>
<div class="m2"><p>خط چین پیشانی‌اش قبض روح</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>زهی رعشه‌ناکی که روز نخست</p></div>
<div class="m2"><p>ز سیماب گردیدش اعضا درست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به ناخن جدا مو ز اندام کرد</p></div>
<div class="m2"><p>تن از کندن مو چو بادام کرد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>همین صرفه‌اش بس ز قد دو تا</p></div>
<div class="m2"><p>که موی سرش بافد انگشت پا</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بدل گشته صبح امیدش به شام</p></div>
<div class="m2"><p>چراغ دلش کرده روغن تمام</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چنان کرده خود را به خال کبود</p></div>
<div class="m2"><p>که آورده گویی فلک را فرود</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>برون رفته از پوشش خواب و خور</p></div>
<div class="m2"><p>که دیده‌ست انبانی از هیچ پر؟</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو چادر به دوش افکند دم مزن</p></div>
<div class="m2"><p>چه به زان که باشد اجل در کفن؟</p></div></div>
<div class="b2" id="bn72"><p>***</p></div>
<div class="b" id="bn73"><div class="m1"><p>ازان خم شد از غمزه مژگان یار</p></div>
<div class="m2"><p>که خوابیده، بهتر کند نیزه کار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چه گویم ز باریکی آن کمر؟</p></div>
<div class="m2"><p>ز معنیّ باریک، باریک‌تر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>شهیدان خود را کند گر کفن</p></div>
<div class="m2"><p>ببالند بر خویش، یک پیرهن</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به زلفش قوی شانه را دست زور</p></div>
<div class="m2"><p>ز عکس لبش چشم آیینه شور</p></div></div>
<div class="b2" id="bn77"><p>***</p></div>
<div class="b" id="bn78"><div class="m1"><p>نشاط است در آسمان و زمین</p></div>
<div class="m2"><p>به عالم که دیده‌ست سوری چنین؟</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>فضای جهان پر زر و زیور است</p></div>
<div class="m2"><p>تو گویی فلک یک صدف گوهرست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>فلک زین چراغان سورست داغ</p></div>
<div class="m2"><p>که چون لاله از خاک روید چراغ</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>جهان برگ عشرت ز بس کرده ساز</p></div>
<div class="m2"><p>طرب را دهن مانده از خنده باز</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>به رقص آسمان شد جدا از زمین</p></div>
<div class="m2"><p>همین است معراج عشرت، همین</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>کند رقص از ذره تا آفتاب</p></div>
<div class="m2"><p>ندیده چنین روز گیتی به خواب</p></div></div>
<div class="b2" id="bn84"><p>***</p></div>
<div class="b" id="bn85"><div class="m1"><p>بود نغمه آن غارت هوش‌ها</p></div>
<div class="m2"><p>که جایش طرب رفته در گوش‌ها</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چو از پرده ساز، سر بر کند</p></div>
<div class="m2"><p>رود پرده گوش چادر کند</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>عروسی بود رهزن عقل و هوش</p></div>
<div class="m2"><p>که بی‌پرده از لب نباید به گوش</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>نزد هرگز از دلبران چگل</p></div>
<div class="m2"><p>به جز نغمه در پرده کس راه دل</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>زند زلف خوبان به صد اضطراب</p></div>
<div class="m2"><p>ز تحریر آوازشان پیچ و تاب</p></div></div>
<div class="b2" id="bn90"><p>***</p></div>
<div class="b" id="bn91"><div class="m1"><p>بود با هوا بد، جواهرفروش</p></div>
<div class="m2"><p>که رفت آب گوهر به گرما ز جوش</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ز بس در بدن‌ها هوا کرد کار</p></div>
<div class="m2"><p>جهد از بن مو عرق چون شرار</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>هوا شد چنان گرم از تاب میغ</p></div>
<div class="m2"><p>که شد آتش‌افشان دم سرد تیغ</p></div></div>
<div class="b2" id="bn94"><p>***</p></div>
<div class="b" id="bn95"><div class="m1"><p>نمایان چو ماه نو از لاغری</p></div>
<div class="m2"><p>چو ابروی خوبان، همه دلبری</p></div></div>
<div class="b2" id="bn96"><p>***</p></div>
<div class="b" id="bn97"><div class="m1"><p>که دیده جز این توپ گیتی‌گشا؟</p></div>
<div class="m2"><p>که غاری کند کار صد اژدها</p></div></div>
<div class="b2" id="bn98"><p>***</p></div>
<div class="b" id="bn99"><div class="m1"><p>حریفان خوش از سردی روزگار</p></div>
<div class="m2"><p>که بازی نسوزد ز کس در قمار</p></div></div>