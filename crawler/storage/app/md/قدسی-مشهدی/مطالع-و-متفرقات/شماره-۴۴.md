---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>گر صبا را ره نبودی در گلستان کسی</p></div>
<div class="m2"><p>اینقدر بر بلبلان کی سوختی جان کسی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای چون محکم کنم در بزم سرگرمان عشق؟</p></div>
<div class="m2"><p>گر نخیزد شعله چون شمع از گریبان کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدسی از جود کریمان شرم می‌آید مرا</p></div>
<div class="m2"><p>دست بی‌شرمی نخواهم زد به دامان کسی</p></div></div>