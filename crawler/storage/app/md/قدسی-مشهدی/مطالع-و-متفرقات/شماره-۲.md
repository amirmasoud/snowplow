---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>عنان به دست شتاب است تا درنگ ترا</p></div>
<div class="m2"><p>کسی چون صلح نفهمد زبان جنگ ترا</p></div></div>