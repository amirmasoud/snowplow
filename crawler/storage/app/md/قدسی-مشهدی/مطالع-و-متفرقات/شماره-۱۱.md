---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>بی غم چه گویمت که دلم چون در آتش است</p></div>
<div class="m2"><p>لیلی به ناز رفته و مجنون در آتش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرویز گو بسوز که فرهاد را هنوز</p></div>
<div class="m2"><p>نعل محبت از پی گلگون در آتش است</p></div></div>