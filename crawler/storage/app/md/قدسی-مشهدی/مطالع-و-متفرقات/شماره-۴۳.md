---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>دل که ربود از برم، ساغر می‌پرست من</p></div>
<div class="m2"><p>گفتمش از که خواهمش، گفت ز چشم مست من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی مجلس بلا، هیچ پیاله در جهان</p></div>
<div class="m2"><p>پر نکند ز خون دل، کش ندهد به دست من</p></div></div>