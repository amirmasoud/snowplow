---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>افروختی ز باده و رنگ بتان شکست</p></div>
<div class="m2"><p>یک گل شکفت و رونق صد گلستان شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دادی چو دل ز دست، رهایی طمع مدار</p></div>
<div class="m2"><p>عشق آن طلسم نیست که آن را توان شکست</p></div></div>