---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>باز دل پای‌بند دام کسی‌ست</p></div>
<div class="m2"><p>روز و شب گوش بر پیام کسی‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو نفس بعد ازین ز سینه تنگ</p></div>
<div class="m2"><p>پا برون نه، که این مقام کسی‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ‌کس نیست در جهان آزاد</p></div>
<div class="m2"><p>هرکه را بنگری به دام کسی‌ست</p></div></div>