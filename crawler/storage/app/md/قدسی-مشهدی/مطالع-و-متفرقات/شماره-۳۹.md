---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>غم رفت از دل من و از سینه داغ هم</p></div>
<div class="m2"><p>مهتاب نیست کلبه ما را چراغ هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد داغ سوختیم و ز دل تیرگی نرفت</p></div>
<div class="m2"><p>روشن نکرد کلبه ما را چراغ هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدسی خبر ز قافله طاقتم مپرس</p></div>
<div class="m2"><p>این کاروان گذشته ز ما بی سراغ هم</p></div></div>