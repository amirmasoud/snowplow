---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>از جوش دلم دیده پر از پاره خون است</p></div>
<div class="m2"><p>در چشم ترم هر مژه فواره خون است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من گریه‌کنان بر سر آن کوی و ز هر سو</p></div>
<div class="m2"><p>خلقی به سرم جمع به نظاره خون است</p></div></div>