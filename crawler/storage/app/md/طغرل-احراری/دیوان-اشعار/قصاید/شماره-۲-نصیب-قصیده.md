---
title: >-
    شمارهٔ ۲ - نصیب قصیده
---
# شمارهٔ ۲ - نصیب قصیده

<div class="b" id="bn1"><div class="m1"><p>ساقی قدح لبریز کن زان می که طغیان پرورد</p></div>
<div class="m2"><p>مستی فزاید، غم برد، شادی دهد، جان پرورد!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میخانه را در باز کن ساغر کشی آغاز کن</p></div>
<div class="m2"><p>دل را به می دمساز کن صد گونه الحان پرورد!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از باده گلگون به من سرشار ده در انجمن</p></div>
<div class="m2"><p>تا گویم از مستی سخن گفتار حسان پرورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در زیر سرو و پای جو وه‌وه چه خوش باشد سبو!</p></div>
<div class="m2"><p>کردست بر دل غم غلو می ده که فرحان پرورد!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فصل بهار آید همی گل در کنار آید همی</p></div>
<div class="m2"><p>صوت هزار آید همی زان رو که افغان پرورد!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گلشن ز مرد فام شد وقت می گل‌فام شد</p></div>
<div class="m2"><p>میل همه در جام شد گر ساقی احسان پرورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>احمال آری تا به کی عمریست مخمورم ز می</p></div>
<div class="m2"><p>فریاد می‌سازم چو نی کز نی نیستان پرورد!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان می که روح‌افزا بود گنگ ار خورد گویا شود</p></div>
<div class="m2"><p>یک جرعه در دریا شود صد دُرّ غلتان پرورد!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دل چکانی جان شود کفر ار خورد ایمان شود</p></div>
<div class="m2"><p>نوشد ملک حیران شود او راد سبحان پرورد!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عصفور را عنقا کند جهال را دانا کند</p></div>
<div class="m2"><p>اموات را احیا کند عیسی‌صفت جان پرورد!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زو مور اژدرها شود سیمرغ را از هم درد</p></div>
<div class="m2"><p>چون بر سر عنقا دود حکم سلیمان پرورد!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گل بشکفد از نکهتش کوثر خجل از هیئتش</p></div>
<div class="m2"><p>آب حیات از لذتش در تاریکستان پرورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در دل فزاید شور و شر سودا براندازد ز سر</p></div>
<div class="m2"><p>ریزد فرو همچون مطر گوهر به نیسان پرورد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یک قطره افتد در زمین گردد زمین دُرّ سمین</p></div>
<div class="m2"><p>نوشد اگر روح‌الامین اخبار یزدان پرورد!</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل را ضیا و نور ازو تن را سراسر زور ازو</p></div>
<div class="m2"><p>ما را جمال حور ازو در خلد رضوان پرورد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غم را براندازد ز دل زو آب حیوان منفعل</p></div>
<div class="m2"><p>سقای کوثر زو خجل زیرا که احسان پرورد!</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عکسش اگر در کوه فتد بی‌جاده و گوهر دمد</p></div>
<div class="m2"><p>همچون بدخشان تا ابد لعل درخشان پرورد!</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در کشور ملک بدن جز می نباشد تهمتن</p></div>
<div class="m2"><p>هر قطره‌اش دُرّ عدن صد رنگ الوان پرورد!</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مسکین خورد حاتم شود ذی افسر خاتم شود</p></div>
<div class="m2"><p>هر دون چشد رستم شود ملک سیستان پرورد!</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر بهره گیرد دیو ازو معدوم گردد ریو ازو</p></div>
<div class="m2"><p>همچون مصاف گیو ازو در جنگ ترکان پرورد!</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اندر خرابات مغان یکسر همه پیر و جوان</p></div>
<div class="m2"><p>از وجد گویند هر زمان می ده که دهقان پرورد!</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مطرب به صورت ارغنون لحن عراقی را کنون</p></div>
<div class="m2"><p>از پرده‌ات آور برون مجلس گلستان پرورد!</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هی می بده هی می بکش هی می بخور هی باده چش</p></div>
<div class="m2"><p>نوش از می خورشیدوَش آبیست انسان پرورد!</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جام جهان‌بینش لقب از ساقی گلرو طلب</p></div>
<div class="m2"><p>یاری بود یاقوت‌لب چون غنچه خندان پرورد!</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زلف سیاهش سرنگون ماری که باشد پرفسون</p></div>
<div class="m2"><p>بر گردن خود ذوفنون دامی که پیچان پرورد!</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سرو و صنوبر از قدش خورشید تابان از خدش</p></div>
<div class="m2"><p>بشکسته بیضا را یدش لؤلؤ ز دندان پرورد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چشمش به هنگام نگه ز ابرو قزح تیر از مژه</p></div>
<div class="m2"><p>خون هزاران بی‌گنه بر خاک یکسان پرورد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ناری بود رخسار او ماری بود زنار او</p></div>
<div class="m2"><p>بی‌نار او بیمار او با خود چه درمان پرورد؟!</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر عارض همچون شفق بنگر چه خوش باشد عرق</p></div>
<div class="m2"><p>گلبرگ رویش هر ورق دعوی به برهان پرورد!</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گل منفعل از روی او شبنم خجل از خوی او</p></div>
<div class="m2"><p>وز کاکل و گیسوی او سنبل پریشان پرورد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خضر خطش هادی مرا بر آب حیوان رهنما</p></div>
<div class="m2"><p>اسکندر از ظلمت برآ زیرا که نقصان پرورد!</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هندوی خالش را نگر بر مصحف رویش مگر</p></div>
<div class="m2"><p>ترسای شوخ بی‌خبر در حفظ قرآن پرورد؟!</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از رخ نقاب اندازد او مه در حجاب اندازد او</p></div>
<div class="m2"><p>همچون شهاب اندازد او تیری که پیکان پرورد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آید به سوی بوستان بهر تماشا اقحوان</p></div>
<div class="m2"><p>صد چشم آرد تا که آن خود را نگهبان پرورد!</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از لعلت ای زیباصنم فرمان بده بوسی زنم</p></div>
<div class="m2"><p>خال سیاهت برکَنَم هندو نه ایمان پرورد!</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بوسی اگر ندهی مرا ای دلبر گلگون‌قبا</p></div>
<div class="m2"><p>لب را کشایم با هجا حرفی که چندان پرورد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>لطفت به طغرل عام کن بوسی بدو انعام کن</p></div>
<div class="m2"><p>«سرخوش ورا با جام کن تا مدح سلطان پرورد»</p></div></div>