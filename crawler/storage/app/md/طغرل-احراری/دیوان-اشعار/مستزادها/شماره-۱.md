---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ای عکس رخ جان دهد آیینه دل را</p></div>
<div class="m2"><p>چون معجز عیسی!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طوطی شده حیران سخن‌های تو جانا</p></div>
<div class="m2"><p>با آن لب گویا!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا با قد شمشاد گذشتی سوی گلشن</p></div>
<div class="m2"><p>قمری به فغان شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرگس پی نظاره شود دیده سراپا</p></div>
<div class="m2"><p>از بهر تماشا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از شرم جمال تو شده یوسف مصری</p></div>
<div class="m2"><p>در زاویه چاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سودای تو دارد به سر وامق و عذرا</p></div>
<div class="m2"><p>تنها نه زلیخا!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیراهن گل در چمن از شوق تو چاک است</p></div>
<div class="m2"><p>از فرقت رویت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خون بسته دل غنچه ز لعل تو همانا</p></div>
<div class="m2"><p>چون باده به مینا!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باد سحر از نکهت زلفت به گلستان</p></div>
<div class="m2"><p>آورد نسیمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سوسن به زبان طعنه زد آهوی خطا را</p></div>
<div class="m2"><p>زان رفت به صحرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خضر خط تو داده به ریحان ادب ناز</p></div>
<div class="m2"><p>از قاعده بو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در کشور خوبی نبود لاله‌عذارا</p></div>
<div class="m2"><p>چون تو شه والا!</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا لشکر حسن تو به تاراج ز هر سو</p></div>
<div class="m2"><p>آورد شبیخون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چشم از پی جان بردن و رخ از پی یغما</p></div>
<div class="m2"><p>در ملک دل ما!</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طغرل به خیال سر زلف تو اسیر است</p></div>
<div class="m2"><p>رستن نتواند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زنجیر بود هر سر مو پای جنون را</p></div>
<div class="m2"><p>زان زلف مطرا!</p></div></div>