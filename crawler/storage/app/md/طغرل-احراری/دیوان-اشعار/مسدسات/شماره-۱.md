---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>مه را چه مناسبت به رویت</p></div>
<div class="m2"><p>گل را چه مثل به رنگ و بویت؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش نشود حریف خویت</p></div>
<div class="m2"><p>فردوس نمونه‌ای ز کویت</p></div></div>
<div class="b2" id="bn3"><p>بیند به چمن قد نکویت</p>
<p>صلصل برود ز باغ سویت!</p></div>
<div class="b" id="bn4"><div class="m1"><p>یارب چه بلا تو نازنینی</p></div>
<div class="m2"><p>مهر فلک و مه زمینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر تا به قدم تو انگبینی</p></div>
<div class="m2"><p>انگشتر حسن را نگینی</p></div></div>
<div class="b2" id="bn6"><p>آیینه فتد اگر به رویت</p>
<p>حیران شود از رخ نکویت!</p></div>
<div class="b" id="bn7"><div class="m1"><p>رخسار تو طعنه کرده با گل</p></div>
<div class="m2"><p>چاه ذقنت به چاه بابل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلف سیهت به جعد سنبل</p></div>
<div class="m2"><p>طرز نگهت به نشئه مل!</p></div></div>
<div class="b2" id="bn9"><p>گل جامه درد ز شوق رویت</p>
<p>عنبر شکند ز قدر مویت!</p></div>
<div class="b" id="bn10"><div class="m1"><p>زهر غم عشق تو چشیدم</p></div>
<div class="m2"><p>راحت سر مو ز تو ندیدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بار المت بسی کشیدم</p></div>
<div class="m2"><p>تا آنکه به عاشقی رسیدم</p></div></div>
<div class="b2" id="bn12"><p>ای آنکه منم در آرزویت</p>
<p>روزان و شبان به جستجویت!</p></div>
<div class="b" id="bn13"><div class="m1"><p>غیر از غم تو به سر ندارم</p></div>
<div class="m2"><p>در عشق تو زار و بی‌قرارم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وصف تو بود همین شعارم</p></div>
<div class="m2"><p>در گلشن مدح تو هزارم!</p></div></div>
<div class="b2" id="bn15"><p>هرچند نه محرمم به کویت</p>
<p>شادم به حدیث گفتگویت!</p></div>
<div class="b" id="bn16"><div class="m1"><p>ای روی مه تو آفتابم</p></div>
<div class="m2"><p>پیچ و خم زلف تو طنابم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر قطره شبنمت گلابم</p></div>
<div class="m2"><p>در آتش عشق تو کبابم!</p></div></div>
<div class="b2" id="bn18"><p>فریاد ز دست خلق و خویت</p>
<p>یک جرعه ندیدم از سبویت</p></div>
<div class="b" id="bn19"><div class="m1"><p>نخلیست قدت ز باغ خوبی</p></div>
<div class="m2"><p>کش طعنه زند به سرو و طوبی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یوسف به درت به خاکروبی</p></div>
<div class="m2"><p>جویان تو شرقی و جنوبی!</p></div></div>
<div class="b2" id="bn21"><p>در هر چمنیست آبرویت</p>
<p>در هر وطنیست گفتگویت!</p></div>
<div class="b" id="bn22"><div class="m1"><p>یکبار نپرسی حال طغرل</p></div>
<div class="m2"><p>مهجور تو از وصال طغرل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دائم تویی در خیال طغرل</p></div>
<div class="m2"><p>دانی تو اگر کمال طغرل</p></div></div>
<div class="b2" id="bn24"><p>از لطف و کرم بری به سویت</p>
<p>همدم کنی با سگان کویت!</p></div>