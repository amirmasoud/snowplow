---
title: >-
    شمارهٔ ۲۴۰
---
# شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>نوبهار ایجادم رونق حمل دارم</p></div>
<div class="m2"><p>همچو گلبن معنی غنچه در بغل دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داده تا مرا رخصت مرشد خراباتم</p></div>
<div class="m2"><p>غیر ملت عشاق کی غم ملل دارم؟!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف او نمی‌گردد مانع جنون من</p></div>
<div class="m2"><p>همچو شمع بزم وصل آه بی‌محل دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پایمال دونانم گر ز پستی طالع</p></div>
<div class="m2"><p>از بلندی همت مسند زحل دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوانده‌ام کتاب غم در سلوک مجنونی</p></div>
<div class="m2"><p>عامل جنونم لیک وضع بی‌عمل دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به کی ز نادانی می‌بری حسد با من؟!</p></div>
<div class="m2"><p>این همه سخندانی قسمت از ازل دارم!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فهم معنی‌ام دارد قدر و شکل اسطرلاب</p></div>
<div class="m2"><p>هندسی اگر خوانم حرفی از جمل دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گردش فلک اکنون گر مرا دهد فرصت</p></div>
<div class="m2"><p>نسخه‌ها همی‌سازم بیم از اجل دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سخن نمی‌باشد دیگری نظیر من</p></div>
<div class="m2"><p>غیر حضرت بیدل من کجا بدل دارم؟!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>محو حیرتم طغرل من ز مصرع بیدل</p></div>
<div class="m2"><p>بی‌تو زنده‌ام یعنی مرگ بی‌اجل دارم!</p></div></div>