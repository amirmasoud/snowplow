---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>تا زلف تو در خرمن گل غالیه‌بیز است</p></div>
<div class="m2"><p>در دور قمر بین که قران مشتری‌ریز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم که به دل رخنه زند مردم چشمت</p></div>
<div class="m2"><p>ورنه ز چه رو خنجر مژگان تو تیز است؟!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مقتول تو را نیست ازآن صورت مردن</p></div>
<div class="m2"><p>برق دم شمشیر تو تا آیینه ریز است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زحمتکش عشقم که چه هجران و چه وصلت</p></div>
<div class="m2"><p>از دست جفای تو کجا جای گریز است؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرجا که ز نقش کف پای تو نشانیست</p></div>
<div class="m2"><p>چون عرصه محشر همه دلم غلغله‌خیز است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افسانه حسن تو هر آن گوش که بشنید</p></div>
<div class="m2"><p>در دعوی زیبایی یوسف به ستیز است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکس که سواد خط مشکین تو را دید</p></div>
<div class="m2"><p>در قافیه حسن بگفتا که تمیز است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرهنگ سخن‌های دقیق من طغرل</p></div>
<div class="m2"><p>نه طور «صراح» است نه گفتار «همیز» است</p></div></div>