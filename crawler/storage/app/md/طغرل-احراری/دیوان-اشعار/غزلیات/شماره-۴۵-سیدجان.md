---
title: >-
    شمارهٔ ۴۵ - سیدجان
---
# شمارهٔ ۴۵ - سیدجان

<div class="b" id="bn1"><div class="m1"><p>سنبل از آشفتگی زلف کژت یک پیچ و تاب</p></div>
<div class="m2"><p>مصحف روی توام تفسیر دارد صد کتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یوسف از شرم جمالت محو زندان گشته است</p></div>
<div class="m2"><p>ای جمال عالم‌آرای تو روشن ز آفتاب!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در تمنای وصالت دین و دل بر باد شد</p></div>
<div class="m2"><p>عاقبت دستم به دامان تو در یوم‌الحساب!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام می با غیر می‌نوشی تو ای رعنا چرا؟!</p></div>
<div class="m2"><p>ز آتش حسرت دل و جان مرا سازی کباب!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب و تاب عارض و زلفت ندارد بوستان</p></div>
<div class="m2"><p>سنبل و گل هر دو از زلف و رخت بی آب و تاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست از عشقت نصیب من به جز داغ فراق</p></div>
<div class="m2"><p>از می وصل تو در گیتی نگشتم کامیاب!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طغرل از عکس جمالش محو حیرت گشته‌ام</p></div>
<div class="m2"><p>تا فکند از عارض ماه خود آن دلبر نقاب!</p></div></div>