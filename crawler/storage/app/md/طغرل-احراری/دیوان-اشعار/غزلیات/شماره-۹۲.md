---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>ساقیا باده ده بهار گذشت</p></div>
<div class="m2"><p>رونق عیش روزگار گذشت!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در میخانه و سر خم را</p></div>
<div class="m2"><p>باز کن وقت انتظار گذشت!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچه داری بریز در جامم</p></div>
<div class="m2"><p>دردسر بی‌حد از خمار گذشت!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاهد گل ز بوستان امروز</p></div>
<div class="m2"><p>توسن باد را سوار گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پس محمل جمازه گل</p></div>
<div class="m2"><p>ناله بلبل و هزار گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم نرگس به هر طرف نگران</p></div>
<div class="m2"><p>از ره باغ شرمسار گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اقحوان بر امید اردیبهشت</p></div>
<div class="m2"><p>دیده‌اش بر قفا دچار گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ضیمران پایمال صرصر شد</p></div>
<div class="m2"><p>کله لاله ز افتخار گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نوبت عهد بوستان افروز</p></div>
<div class="m2"><p>چون وفا و وصال یار گذشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جام را زورق یم می‌کن</p></div>
<div class="m2"><p>لنگر صبر از قرار گذشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خیز و این وقت را غنیمت دان</p></div>
<div class="m2"><p>فرصت عمر بیم‌دار گذشت!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حبذا چنگ و شاهد و لب جو</p></div>
<div class="m2"><p>مطربا ساز کن که کار گذشت!</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طغرل از جبر چرخ می‌نالم</p></div>
<div class="m2"><p>به من از جبرش بی‌شمار گذشت</p></div></div>