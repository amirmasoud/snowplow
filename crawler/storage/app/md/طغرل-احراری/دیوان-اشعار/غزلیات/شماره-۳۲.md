---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>قامتی در زیر بار عشق خم داریم ما</p></div>
<div class="m2"><p>نغمه‌ای از ساز دل بی زیر و بم داریم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکساری نیست کم از دستگاه اعتبار</p></div>
<div class="m2"><p>بوریای فقر همچون تخت جم داریم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساز بی‌آهنگ ما مطبوع طبع کس نشد</p></div>
<div class="m2"><p>هرچه می‌آید ز ما بر خود ستم داریم ما!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هستی ما نیست اینجا غیر سامان عرق</p></div>
<div class="m2"><p>کز نگین خویش جای نای نم داریم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر به ما از جام قسمت باده عشرت نشد</p></div>
<div class="m2"><p>شهد راحت از نصیب زهر غم داریم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست کرداری که از ما دستگیر ما شود</p></div>
<div class="m2"><p>دست امیدی به دامان کرم داریم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از وجود ما که امکان ننگ دارد دم به دم</p></div>
<div class="m2"><p>هستی آغاز و انجام عدم داریم ما!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر همین باشد سلوک شیوه اخوان دهر</p></div>
<div class="m2"><p>کی امید از لطف خال و مهر عم داریم ما؟!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر نزد با نام ما یک اختر از برج شرف</p></div>
<div class="m2"><p>طالعی ز اقبال خود بسیار کم داریم ما!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهر غم در کام ما هرگز نباشد کارگر</p></div>
<div class="m2"><p>در مذاق خویش تریاکی ز سم داریم ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مدعای کام ما حاصل نشد از هیچ کس</p></div>
<div class="m2"><p>همچو مجنون عاقبت از خلق رم داریم ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>محمل ما می‌رود طغرل به آهنگ نفس</p></div>
<div class="m2"><p>تا درین وادی ز شوق او قدم داریم ما</p></div></div>