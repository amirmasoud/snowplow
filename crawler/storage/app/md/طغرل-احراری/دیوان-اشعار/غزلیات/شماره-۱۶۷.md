---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>ای غنچه‌دهن لب تو شکر</p></div>
<div class="m2"><p>دندان تو همچو در و گوهر!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای عارض تو حریر از گل</p></div>
<div class="m2"><p>وی زلف سیاه تو معنبر!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای نخل قدت برابر جان!</p></div>
<div class="m2"><p>رخسار مهت به جان برابر!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمت به کرشمه ریخت خونم</p></div>
<div class="m2"><p>مژگانت برم کشیده خنجر!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوبان جهان فزون ولیکن</p></div>
<div class="m2"><p>مانند تو کس ندیده دلبر!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوبی به تو ختم عشق با من</p></div>
<div class="m2"><p>این هر دو شد از قضا مقرر!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم کشور حسن را امیری</p></div>
<div class="m2"><p>هم ملک قلوب را تو صفدر!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شرمنده ز چهره نکویت</p></div>
<div class="m2"><p>گر دیده همه بتان آذر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طغرل به حواله دو چشمت</p></div>
<div class="m2"><p>در دشت فراق گشته ششدر</p></div></div>