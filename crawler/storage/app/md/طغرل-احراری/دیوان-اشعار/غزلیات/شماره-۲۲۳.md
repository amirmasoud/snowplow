---
title: >-
    شمارهٔ ۲۲۳
---
# شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>ای نرگس مستت چمن آرای تغافل</p></div>
<div class="m2"><p>بازار تو گرم است ز سودای تغافل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سرمه چو بخت سیه ما همه اکنون</p></div>
<div class="m2"><p>خوابیده مگر فتنه و غوغای تغافل؟!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غفلت اثر شوق بود غنچه لعلت</p></div>
<div class="m2"><p>خون گشت دل ما به تمنای تغافل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان در طلبت داده به امید نگاهی</p></div>
<div class="m2"><p>آخر شدم از چشم تو رسوای تغافل!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک جرعه‌ای از باده شوق تو مرا بس</p></div>
<div class="m2"><p>در انجمن ناز ز مینای تغافل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شوریست ز کیفیت میخانه چشمت</p></div>
<div class="m2"><p>حیرانم ازین مستی صهبای تغافل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بادیه عشق دویدیم و ندیدیم</p></div>
<div class="m2"><p>جز چشم تو نخچیر به صحرای تغافل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با مصلحت عشق به گلزار جمالش</p></div>
<div class="m2"><p>چشمی بگشا بهر تماشای تغافل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از وصل تو محروم من از طالع پستم</p></div>
<div class="m2"><p>از ناز بلندست مگر جای تغافل؟!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای هم‌نفسان خار ره عشق برآرید</p></div>
<div class="m2"><p>با سوزن مژگان من از پای تغافل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طغرل چه خوش است موجه دریای معانی</p></div>
<div class="m2"><p>من کشته تمکینم و رسوای تغافل</p></div></div>