---
title: >-
    شمارهٔ ۲۸۲
---
# شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>برد دلم را ز ره زلف سمن‌سای او</p></div>
<div class="m2"><p>کرد شهید نگه نرگس شهلای او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشته به هم توأمان از خط کلک ازل</p></div>
<div class="m2"><p>دست من و دامنش فرق من و پای او!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی بود اندر زمین در خور او مسکنی؟!</p></div>
<div class="m2"><p>منظر چشمم بود منزل و مأوای او!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردن سرو سهی خم شده از انفعال</p></div>
<div class="m2"><p>دیده مگر در چمن قامت زیبای او؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرش خرام رهش چشم امیدم بود</p></div>
<div class="m2"><p>تا نرسد بر زمین نقش کف پای او!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه مسیحا به لب زنده کند مرده را</p></div>
<div class="m2"><p>در فن احیاگری ره نبرد جای او!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که به روز آورد شام فراق تو را</p></div>
<div class="m2"><p>گر نه به حالش کنی رحم بود وای او!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌شکند چون خزف قیمت در عدن</p></div>
<div class="m2"><p>همچو سخن‌های من لعل شکرخای او!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باد تو را آفرین طغرل شاهین وطن</p></div>
<div class="m2"><p>کردی تمام از سخن وصف سراپای او!</p></div></div>