---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>ای لعل لب تو جان عاشق</p></div>
<div class="m2"><p>ابروی کژت کمان عاشق!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز و شب و سال و مه نباشد</p></div>
<div class="m2"><p>جز نام تو بر زبان عاشق!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌خنده لعل تو نخندد</p></div>
<div class="m2"><p>یکبار ز غم دهان عاشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مسلخ عشق سر بریدن</p></div>
<div class="m2"><p>باشد مگر امتحان عاشق؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌روی گل تو همچو بلبل</p></div>
<div class="m2"><p>بر چرخ رسد فغان عاشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگه به لبان لبان کشایی</p></div>
<div class="m2"><p>خندد به لبان لبان عاشق!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو روح روان عاشقانی!</p></div>
<div class="m2"><p>رفتی تو رود روان عاشق!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکبار ز راه مهربانی</p></div>
<div class="m2"><p>بگذر طرف مکان عاشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای شوخ چرا خبر نداری</p></div>
<div class="m2"><p>از طغرل ناتوان عاشق؟!</p></div></div>