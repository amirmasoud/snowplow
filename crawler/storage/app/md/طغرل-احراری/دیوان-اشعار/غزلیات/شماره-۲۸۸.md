---
title: >-
    شمارهٔ ۲۸۸
---
# شمارهٔ ۲۸۸

<div class="b" id="bn1"><div class="m1"><p>ای غنچه خندان من از بوستان کیستی؟!</p></div>
<div class="m2"><p>وی دشمن ایمان من از دوستان کیستی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیری زده بر جان من مژگان ناوک‌افکنت</p></div>
<div class="m2"><p>ای ترک تیرانداز من ابروکمان کیستی؟!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو قدت اندر چمن بشکست قدر نارون</p></div>
<div class="m2"><p>ای عرعر طوبی‌شکن سرو روان کیستی؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من طوطی‌ام شکرشکن هندوستانم زلف تو</p></div>
<div class="m2"><p>با خال هندوی لبت هندوستان کیستی؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لعل می‌آرد برون گوهر بدخشان لبت</p></div>
<div class="m2"><p>ای صد بدخشان در لبت تو خود ز کان کیستی؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نالیدم از شب تا سحر بشنیدی افغان مرا</p></div>
<div class="m2"><p>یک ره نگفتی ای صنم کاندر فغان کیستی؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان مرا بی‌روی تو آرام نبود در بدن</p></div>
<div class="m2"><p>بهر خدا با من بگو آرام جان کیستی؟!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیریست با فکر رسا صید معانی می‌کنی</p></div>
<div class="m2"><p>ای طغرل اوج سخن از آشیان کیستی؟!</p></div></div>