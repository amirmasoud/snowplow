---
title: >-
    شمارهٔ ۲۸۶
---
# شمارهٔ ۲۸۶

<div class="b" id="bn1"><div class="m1"><p>دل پیش تو کشف راز کرده</p></div>
<div class="m2"><p>دیوان غم تو باز کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زلف تو می‌کند حکایت</p></div>
<div class="m2"><p>افسانه خود دراز کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قانون غمت به صوت «عشاق»</p></div>
<div class="m2"><p>زیر و بم عشق ساز کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لبیک زنم به طوف کویت</p></div>
<div class="m2"><p>آهنگ ره حجاز کرده!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لعل لب تو تبرزدآرا</p></div>
<div class="m2"><p>بی‌قدرتر از پیاز کرده!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صراف دکان عشق ما را</p></div>
<div class="m2"><p>در بوته غم گداز کرده!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از شوخی جلوه تذروست</p></div>
<div class="m2"><p>کان بخیه به چشم باز کرده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیداد و ستم به حال محمود</p></div>
<div class="m2"><p>زلف سیه ایاز کرده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امروز قضا ز نام طغرل</p></div>
<div class="m2"><p>عنوان سخن طراز کرده</p></div></div>