---
title: >-
    شمارهٔ ۲۱۵
---
# شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>دارد چمن ز رونق فصل بهار رنگ</p></div>
<div class="m2"><p>هر برگ گل گرفته مگر از هزار رنگ؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز در بساط گلستان به فرش ناز</p></div>
<div class="m2"><p>خوابیده است شاهد گل در کنار رنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مانند زاهدان تو به سجاده چمن</p></div>
<div class="m2"><p>از دانه‌های سبحه شبنم شمار رنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارد به باغ عزم سفر کاروان گل</p></div>
<div class="m2"><p>گویا که می‌رود ز پی نوبهار رنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بس که ریختم به فراقش سرشک غم</p></div>
<div class="m2"><p>چشمم برد به گریه ز شمع مزار رنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نظاره کن به باغ که فرصت غنیمت است</p></div>
<div class="m2"><p>از بس که گشته توسن گل را سوار رنگ!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیرنگی است رنگ خم نیلی فلک</p></div>
<div class="m2"><p>گر نیست باور تو ازین خم برآر رنگ!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل را به پیش روی تو سامان خجلت است</p></div>
<div class="m2"><p>نبود به جز عرق به رخ شرمسار رنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طغرل نگر که حضرت بیدل چه گفته است</p></div>
<div class="m2"><p>اینجاست بی‌بقا گل و بی‌اعتبار رنگ!</p></div></div>