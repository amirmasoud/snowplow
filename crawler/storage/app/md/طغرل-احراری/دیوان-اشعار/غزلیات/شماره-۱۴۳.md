---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>غمزه مردم‌شکارت غارت جان می‌کند</p></div>
<div class="m2"><p>عشوه عابدفریبت قصد ایمان می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برقع از رخ برفکن بی‌پرده گردد عالمی</p></div>
<div class="m2"><p>عکس رویت طعنه با خورشید تابان می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داغ عشق توست ای لیلی نه مجنون را و بس</p></div>
<div class="m2"><p>پیچ و تاب سنبلت آشوب دوران می‌کند!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خنده زن بر رویم ای گل تا نگریم بعد ازین</p></div>
<div class="m2"><p>ورنه سیل اشک من در بحر طوفان می‌کند!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این چه بی‌رحمی است خوان عاشقان را از غضب</p></div>
<div class="m2"><p>وا کشیده بر سر هر میخ مژگان می‌کند!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خط ریحان لعل او مرا معلوم شد</p></div>
<div class="m2"><p>لشکر شام حبش این شهر ویران می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طغرل ار خواهی وصالش ناله کن شام و سحر</p></div>
<div class="m2"><p>ناله بلبل سحر گل در گلستان می‌کند</p></div></div>