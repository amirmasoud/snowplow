---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>اگر دل محو آن رخسار زیباست</p></div>
<div class="m2"><p>ز جوهر موج این آیینه دریاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندارد داغدار عشق قدری</p></div>
<div class="m2"><p>وطن با لاله اندر کوه و صحراست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشو در مهر امکان همچو شبنم</p></div>
<div class="m2"><p>جهان در سایه این بال عنقاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به وقت عجز دشمن دوست گردد</p></div>
<div class="m2"><p>به زانوی سکندر فرق داراست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حریم حرمت دلدار دور است</p></div>
<div class="m2"><p>بسی در راه عشقش زیر و بالاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود عشاق مست باده غم</p></div>
<div class="m2"><p>عروج نشئه ما کی ز میناست؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به عالم هرکجا باشد اگر دل</p></div>
<div class="m2"><p>اسیر جعد آن زلف مطراست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بساط عشق شد تا مسند ما</p></div>
<div class="m2"><p>کلاه افتخار ما فلک‌ساست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دویی را نیست ره در مسکن عشق</p></div>
<div class="m2"><p>دل عاشق ازین سودا مبراست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دهد صد مرده را جان از تکلم</p></div>
<div class="m2"><p>به احیا لعل او رشک مسیحاست!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به یاد گیسویش اشکم گره زد</p></div>
<div class="m2"><p>ز موج این بحر را زنجیر برپاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خدا را جانب ما کن نگاهی</p></div>
<div class="m2"><p>سفید از انتظارت دیده ماست!</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خوشا زین مصرع بیدل که طغرل</p></div>
<div class="m2"><p>خیالی سد راه عبرت ماست</p></div></div>