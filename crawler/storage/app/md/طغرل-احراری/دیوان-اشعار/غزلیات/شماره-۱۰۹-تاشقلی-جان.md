---
title: >-
    شمارهٔ ۱۰۹ - تاشقلی‌جان
---
# شمارهٔ ۱۰۹ - تاشقلی‌جان

<div class="b" id="bn1"><div class="m1"><p>تا کلک صنع چشم تو سرمشق ناز کرد</p></div>
<div class="m2"><p>همچون تذرو ناز تو در چشم باز کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد سلاح غمزه‌ات از بهر هوش من</p></div>
<div class="m2"><p>کرد آنچنان به من که به محمود ایاز کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهد لبت که باد تبرزد غلام او</p></div>
<div class="m2"><p>نرخ نبات مصر به رخ پیاز کرد!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قد بلند سرو تو قمری به باغ دید</p></div>
<div class="m2"><p>کوکو زد و به پیش تو عرض نیاز کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لعل از بدخش خیزد و گوهر ز لعل تو</p></div>
<div class="m2"><p>داند هرآنکه لعل و گهر امتیاز کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یابد نوا ز پرده «عشاق » هرکه او</p></div>
<div class="m2"><p>زیر و بم ترانه عشق تو ساز کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جیب قبا چو شانه دریدم ز کوتهی</p></div>
<div class="m2"><p>دست قضا که دامن زلفت دراز کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این طفل اشک راز نهان درون من</p></div>
<div class="m2"><p>آمد به نزد مردم و افشای راز کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نوشید ز سلسبیل و ورود عافیت بهشت</p></div>
<div class="m2"><p>هرکس به طاق ابروی تو یک نماز کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صراف عشق نقد دل طغرل مرا</p></div>
<div class="m2"><p>روز ازل به بوته محنت‌گداز کرد</p></div></div>