---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>دارم هوای عشق تو عمریست بر سرا</p></div>
<div class="m2"><p>بهر خدا ز راه وفا هیچ نگذرا!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ملک عشق مسند شاهی گزیده‌ام</p></div>
<div class="m2"><p>تا کرده‌ام ز نقش کف پات افسرا!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر مدعی به دعوی عشقت نه صادق است</p></div>
<div class="m2"><p>دارم ز رنگ زرد بهر تو محضرا!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ دلم به سوی تو پرواز می‌کند</p></div>
<div class="m2"><p>باشد رسا به شوق وصال تو شهپرا!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آهم نمی‌کند به دل سخت او اثر</p></div>
<div class="m2"><p>او راست دل مگر از سنگ مرمرا؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دی با قد بلند گذر کرد سوی باغ</p></div>
<div class="m2"><p>افتاد سرو و طوبی و شمشاد و عرعرا!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طغرل به کسب نکهت گیسوی او مگر</p></div>
<div class="m2"><p>شوری است بین باد صبا بین صرصرا؟!</p></div></div>