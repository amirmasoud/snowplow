---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>زلف مشکین تا به دور ماه رویش هاله زد</p></div>
<div class="m2"><p>شور طوفان از نوایم در نیستان ناله زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد از بهر خدا سوز درون ما مپرس</p></div>
<div class="m2"><p>کز حدیث آتش عشقش لبم تبخاله زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تخم امیدی که اندر مزرع مهر و وفا</p></div>
<div class="m2"><p>کشته بودم از سحاب ناامیدی ژاله زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که بر رخسار او خال سیاهش دید گفت</p></div>
<div class="m2"><p>هندوی آتش‌پرستی خیمه در بنگاله زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالمی محنت به رنگی از جمالش می‌کشد</p></div>
<div class="m2"><p>چاک شد پیراهن گل داغ بر دل لاله زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌سزد در ملک خوبی گردد او فرمانروا</p></div>
<div class="m2"><p>بس که در اوج ملاحت اخترش دنباله زد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردش دور فلک طغرل ز تقدیر ازل</p></div>
<div class="m2"><p>قرعه نام مرا ز اندوه چندین‌ساله زد</p></div></div>