---
title: >-
    شمارهٔ ۱۳۸ - باسط‌جان
---
# شمارهٔ ۱۳۸ - باسط‌جان

<div class="b" id="bn1"><div class="m1"><p>باده عشق تو در هر دل اگر جوش کند</p></div>
<div class="m2"><p>اگرش آب حیات است کجا نوش کند؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آرزومند وصال تو اگر هوشناک است</p></div>
<div class="m2"><p>به یکی قطره می وصل تو بیهوش کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر خود گرچه بر افلاک نو ساید</p></div>
<div class="m2"><p>حلقه داغ غلامی تو در گوش کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طوطی هند شکرخایی لعلت بیند</p></div>
<div class="m2"><p>از خجالت سخن خویش فراموش کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم آهوی تو آهو به ختن می‌گیرد</p></div>
<div class="m2"><p>بلکه آهو به سیه‌چشمی آهوش کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از قضا خط برات اجل آرد هرکس</p></div>
<div class="m2"><p>چون دو پیکر ز کمربند تو آغوش کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نخل شمشاد خرام قد او را بیند</p></div>
<div class="m2"><p>بی‌ابا سجده محراب دو ابروش کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خم شود قامت او چون قد طغرل عمری</p></div>
<div class="m2"><p>هرکسی بار غم عشق تو بر دوش کند!</p></div></div>