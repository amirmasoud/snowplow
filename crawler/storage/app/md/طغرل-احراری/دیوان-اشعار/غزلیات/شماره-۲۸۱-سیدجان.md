---
title: >-
    شمارهٔ ۲۸۱ - سیدجان
---
# شمارهٔ ۲۸۱ - سیدجان

<div class="b" id="bn1"><div class="m1"><p>سرو و صنوبر شد خجل از قامت زیبای او</p></div>
<div class="m2"><p>آشفته سنبل با سمن از زلف عنبرسای او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک سو دریده پیرهن گل از گل رویش نگر</p></div>
<div class="m2"><p>خون بسته در دل غنچه را دیگر طرف سودای او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دل هوای عشق او داغست چون من لاله هم</p></div>
<div class="m2"><p>تیری کجا ننشسته است از نرگس شهلای او؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون از خرامان رفتنش شمشاد حسرت آورد</p></div>
<div class="m2"><p>وه این چه قد و قامت است جانست سر تا پای او؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آید اگر سوی چمن سرسبز گردد انجمن!</p></div>
<div class="m2"><p>خضر نبی پیدا شود از نقش خاک پای او!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نالم چو نی از دوری‌اش روز و شب و شام و سحر</p></div>
<div class="m2"><p>باشد نصیب من شود یک جرعه از صهبای او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بس غلام درگهش گشتند یکسر صادقان</p></div>
<div class="m2"><p>طغرل غلام صادق است گلشن بود مأوای او!</p></div></div>