---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>می‌برد لعل لب او نشئه از موج شراب</p></div>
<div class="m2"><p>می‌کشد دامان زلفش از گریبان گلاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لمعه برق رخش هر دم به عشاق آن کند</p></div>
<div class="m2"><p>در بیابان تشنه را تشویش نیرنگ سراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر زمان دل آرزوی آب تیغت می‌کند</p></div>
<div class="m2"><p>تا به کی شمشیر احسان تو باشد در قراب؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توشه لخت جگر دارم به راهت دود غم</p></div>
<div class="m2"><p>می‌زند فواره ترسم تیره گردد این کباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسخه دیوان حسنت را چسان احصا کنم؟!</p></div>
<div class="m2"><p>گر جهان دفتر شود وصف تو ناید در حساب؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای جفا اندیشه بدخو خدا را شفقتی!</p></div>
<div class="m2"><p>چند باشم از شکنج محنت غم در عذاب؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرده استاد ازل امروز با صد خون دل</p></div>
<div class="m2"><p>غنچه‌ات را از گلستان لطافت انتخاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا دم روز قیامت مادر ایام را</p></div>
<div class="m2"><p>چون تو فرزندی نباشد خانه‌زاد آفتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گیسوی شیرین بود زنجیر پای کوهکن</p></div>
<div class="m2"><p>طره لیلی بود در گردن مجنون طناب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از سویدای دلت زن رشحه فال امید</p></div>
<div class="m2"><p>موجب باران بود گر تیره بنماید سحاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شام غفلت رفت آخر صبح نومیدی دمید</p></div>
<div class="m2"><p>تا کجا آباد باشد خانه هستی خراب؟!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گویمش صد آفرین با خاطر دراک او</p></div>
<div class="m2"><p>هرکه بنویسد به اشعار من طغرل جواب!</p></div></div>