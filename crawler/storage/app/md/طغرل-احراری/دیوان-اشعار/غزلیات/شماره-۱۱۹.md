---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>هر کرا دل محو آن آیینه‌رخسار شد</p></div>
<div class="m2"><p>جلوه آیینه او شوخی دیدار شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم مستش کرده هردم تازه کیش کافری</p></div>
<div class="m2"><p>اهرمن را زلف او تا حلقه زنار شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو موج از باد می‌لرزم ز چین ابرویش</p></div>
<div class="m2"><p>رحم نبود مهره تیغی که ناهموار شد!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست سودایی کوکب جز رضای مشتری</p></div>
<div class="m2"><p>تا متاع حسن او را گرمی بازار شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا زدم در بارگاه سینه شادروان غم</p></div>
<div class="m2"><p>خیمه‌ام را یاد مژگانش چه خوش مسمار شد!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خیال نرگس او می‌زدم فال طرب</p></div>
<div class="m2"><p>نقطه دل مرکز این حلقه پرگار شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنقدر نالیدم از هجران او شب تا سحر</p></div>
<div class="m2"><p>هر بن مو بر تنم در ناله موسیقار شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست از جام وصال او به زاهد بهره‌ای</p></div>
<div class="m2"><p>شومی بخت بدش نیرنگ استغفار شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جوهر عرض دل عشاق از محنت‌بری‌ست</p></div>
<div class="m2"><p>از صفا آیینه عمری پشت بر دیوار شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غیر غفلت نیست در چشم تحیرمنصبان</p></div>
<div class="m2"><p>دیده مخمل کی از خواب گران بیدار شد؟!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد ز قتلم قصر بنیاد محبت واژگون</p></div>
<div class="m2"><p>خون من آخر حنای پنجه معمار شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست حسنش گر گریبان چمن هر سو کشید</p></div>
<div class="m2"><p>پای زلفش لیک نقش دامن گلزار شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس که معدومیست چون عنقا نشان آن دهن</p></div>
<div class="m2"><p>از وجودش دم زدم صبحی تبسم زار شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای خوشا از مصرع بیدل که طغرل گفته است</p></div>
<div class="m2"><p>آب گردید انتظار و عالم دیدار شد</p></div></div>