---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>می‌روم از خویش هر ساعت ز دنبال نفس</p></div>
<div class="m2"><p>محمل ما را بود ساز شکست دل جرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌توان امروز بودن فرش پای آفتاب</p></div>
<div class="m2"><p>شب‌روان راه را چون سایه نبود بیم کس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نباشی رهنورد وادی دشت عدم</p></div>
<div class="m2"><p>کی رسی در جاده اقبال این بال فرس؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌فنا حاصل نگردد دولت دیدار او</p></div>
<div class="m2"><p>کس چسان سازد تمنای وصال او هوس؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پختگان را کی بود از صحبت دونان گزیر</p></div>
<div class="m2"><p>شعله را بسیار باشد احتیاج خار و خس!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کارگاه باغ امکان را بود نیرنگ‌ها</p></div>
<div class="m2"><p>زاغ در صحرا و بلبل گشته محبوس قفس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خانه دل را تو از زنگ کدورت صاف کن</p></div>
<div class="m2"><p>می‌شود آیینه اینجا تیره از جوش نفس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه را باشد به قدر مشرب خود جاده‌ای</p></div>
<div class="m2"><p>کی بود فیل دمان را تاب یک گشت فرس؟!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اشک در راه طلب باشد دلیل مدعا</p></div>
<div class="m2"><p>کاغذ ابری بود آیین عشق بوالهوس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل به دزدی می‌رود امشب به جعد زلف او</p></div>
<div class="m2"><p>کی بود عیار را اندیشه چوب عسس؟!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طغرل از ضبط نفس فال سعادت می‌زنم</p></div>
<div class="m2"><p>بر سر مرغ سخن بال هما باشد قفس</p></div></div>