---
title: >-
    شمارهٔ ۲۸۴
---
# شمارهٔ ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>دلبرم را هست گویا سیر صحرا آرزو</p></div>
<div class="m2"><p>لاله را باشد از آن داغ سویدا آرزو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر زمان مد نگه را رخصت نظاره کن</p></div>
<div class="m2"><p>باشدت گر از جمال او تماشا آرزو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک سخن از لعل جان‌بخشش تو را کافی بود</p></div>
<div class="m2"><p>گر بود در دل تو را اعجاز عیسی آرزو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من گرفتار ویم بلبل اسیر گل بود</p></div>
<div class="m2"><p>هر کرا باشد به دل نوعی تمنا آرزو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا توانی گیر از میخانه چشمش قدح</p></div>
<div class="m2"><p>گر تو را باشد خیال جام و مینا آرزو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر عزیز مصر را محرم شد اما کی بود</p></div>
<div class="m2"><p>جز هوای عشق یوسف با زلیخا آرزو؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حلقه زنجیر او از حلقه چشمم کنید</p></div>
<div class="m2"><p>گر بود یار مرا خلخال در پا آرزو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوانده‌ام بسیار از لوح کتاب نوخطان</p></div>
<div class="m2"><p>طغرل ما راست اکنون خط طغرا آرزو</p></div></div>