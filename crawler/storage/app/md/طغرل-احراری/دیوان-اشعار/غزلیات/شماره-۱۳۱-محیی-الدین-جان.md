---
title: >-
    شمارهٔ ۱۳۱ - محیی‌الدین‌جان
---
# شمارهٔ ۱۳۱ - محیی‌الدین‌جان

<div class="b" id="bn1"><div class="m1"><p>مهر عشقش در ازل خط جبینم کرده‌اند</p></div>
<div class="m2"><p>نام مجنون را ازآن نقش نگینم کرده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حلقه پرپیچ و تاب نذر زلفش دل ربود</p></div>
<div class="m2"><p>همچو اسکندر که با ظلمت قرینم کرده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاد دادن از وجود آن دهن وهم است و بس</p></div>
<div class="m2"><p>بس که اندر نیستی عین‌الیقینم کرده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سر اخلاص کردم آستان او وطن</p></div>
<div class="m2"><p>خاک کویش بهتر از خلد برینم کرده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لاله‌آسا داغ‌های سینه صدچاک را</p></div>
<div class="m2"><p>از هوای عشق آن نازآفرینم کرده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دین و دل بر باد داد و محرم وصلش نکرد</p></div>
<div class="m2"><p>وامق و فرهاد با صبر آفرینم کرده‌اند!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناله و فریاد من تأثیر نآرد در دلش</p></div>
<div class="m2"><p>نیست عیب او مرا بخت اینچنینم کرده‌اند!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یار را تنها نمی‌یابم که گویم راز دل</p></div>
<div class="m2"><p>دشمن و اغیار از هرسو کمینم کرده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جامه عریان ما فارغ از لون هواست</p></div>
<div class="m2"><p>پیرهن در عشق او رنگ زمینم کرده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنقدر در راه او از سر قدم‌فرسا شدم</p></div>
<div class="m2"><p>شهرت نام جنون با من ازینم کرده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کاتبان قسمت و تقدیر از فکر رسا</p></div>
<div class="m2"><p>خرمن ملک سخن را خوشه‌چینم کرده‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نخل اشعار تو طغرل نام می‌آرد ثمر</p></div>
<div class="m2"><p>گلشن توشیح را خاصیت اینم کرده‌اند!</p></div></div>