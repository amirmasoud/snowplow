---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>از هجوم اشک بر دریا گذر داریم ما</p></div>
<div class="m2"><p>ریشه نخل امید از چشم تر داریم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که باشد جنس ما را گرمی بازار غم</p></div>
<div class="m2"><p>گوییا دکانی از سنگ شرر داریم ما!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رهروان عشق را زاد دگر در کار نیست</p></div>
<div class="m2"><p>توشه‌ای در راهش از خون جگر داریم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساز قانون محبت را نباشد زیر و بم</p></div>
<div class="m2"><p>در طریق عشق دائم پا ز سر داریم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش در بزم ادب بودیم سرمست طرب</p></div>
<div class="m2"><p>کز خمار باده اکنون درد سر داریم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده ما از غبار سرمه دامنگیر شد</p></div>
<div class="m2"><p>توتیا از خاک پایش در نظر داریم ما!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غنچه‌سان جمعیت دل‌ها ز دلبر داشتیم</p></div>
<div class="m2"><p>دلبر ما رفت از دل کی خبر داریم ما؟!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنقدر آماده شهد معانی کرده‌ایم</p></div>
<div class="m2"><p>بند در بند سخن از نیشکر داریم ما!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کفر باشد در سلوک عشق عیب عاشقان</p></div>
<div class="m2"><p>کی ز تیر طعنه زاهد حذر داریم ما؟!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طغرل آسان کی بود غواصی بحر سخن؟!</p></div>
<div class="m2"><p>سر به جیب فکر از بهر گهر داریم ما!</p></div></div>