---
title: >-
    شمارهٔ ۲۱۰
---
# شمارهٔ ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>از کمال روشنی در دیده شد جای چراغ</p></div>
<div class="m2"><p>کس نباشد در جهان امروز همتای چراغ!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه تاریک است و تو مغرور شمع دل مشو</p></div>
<div class="m2"><p>روشنی هرگز نمی‌بینی تو در پای چراغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مقصد از سوز و گداز و گریه و آه شبش</p></div>
<div class="m2"><p>غیر جانبازی نمی‌باشد تمنای چراغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در شبستان جنون اندر سلوک عاشقی</p></div>
<div class="m2"><p>بال صد پروانه می‌چینی ز گل‌های چراغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صحبت نااهل باشد موجب آزار دل</p></div>
<div class="m2"><p>بیشتر از آب باشد شور و غوغای چراغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشتری جوشد اگر در ظلمت‌آباد جهان</p></div>
<div class="m2"><p>کی کسی داند به جز پروانه سودای چراغ؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به صبح حشر جز پروانه شمع ادب</p></div>
<div class="m2"><p>نیست دیگر ماهی ای طغرل به دریای چراغ!</p></div></div>