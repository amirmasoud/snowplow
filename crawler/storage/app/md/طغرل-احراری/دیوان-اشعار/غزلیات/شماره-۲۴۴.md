---
title: >-
    شمارهٔ ۲۴۴
---
# شمارهٔ ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>وه که محرم در حریمش غیر و من اندر درم</p></div>
<div class="m2"><p>شیوه دلبر گر اینست چون ازین دل بر برم؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مقام وصل بانگ بی‌محل دارد رقیب</p></div>
<div class="m2"><p>می‌کند گوش خرد را صوت این انکر کرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ره عشق تو گشتم پادشاه عاشقان</p></div>
<div class="m2"><p>مسند غم تختگاهم چهره اصفر فرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در تمنای غبار نقش پایت خاک به</p></div>
<div class="m2"><p>یابد از ظل همای دولت ار افسر سرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از هجوم داغ عشقت شد دلم آتش نصب</p></div>
<div class="m2"><p>چون سمندر خو به آتش گیر زین مجمر مرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنقدر جمعیت شوقت سرشک ایجاد کرد</p></div>
<div class="m2"><p>از هجوم اشک حسرت گشت تا بستر ترم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طغرل از قید سر زلفش کجا خواهم رسید</p></div>
<div class="m2"><p>تا ابد در بوته غم سازد او اخگر گرم؟!</p></div></div>