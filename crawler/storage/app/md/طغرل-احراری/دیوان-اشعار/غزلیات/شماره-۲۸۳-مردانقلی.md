---
title: >-
    شمارهٔ ۲۸۳ - مردانقلی
---
# شمارهٔ ۲۸۳ - مردانقلی

<div class="b" id="bn1"><div class="m1"><p>ماه تمام من که منم مستمند تو</p></div>
<div class="m2"><p>مرغ دلم فتاده به دام کمند تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتار خویش کبک دری ترک می‌کند</p></div>
<div class="m2"><p>بیند اگر خرام قد دلپسند تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ملک حسن خسرو فرمانروا تویی</p></div>
<div class="m2"><p>دل نیست در جهان که نباشد به بند تو!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای قامت بلند تو سرمشق دلبری</p></div>
<div class="m2"><p>جانم فدای قامت بالا بلند تو!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرد مفاخرت ز فلک می‌برد زمین</p></div>
<div class="m2"><p>زیرا که محرم است به نعل سمند تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قربان آن شوم که تو را آفریده است</p></div>
<div class="m2"><p>در هر کجا بود سخن از چون و چند تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لب‌های غنچه وا نشود گر روی به باغ</p></div>
<div class="m2"><p>از شرم خنده لب شیرین ز قند تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یاد از هوای سجده ابروت می‌کند</p></div>
<div class="m2"><p>طغرل نکرده وهم ز تیغ پرند تو</p></div></div>