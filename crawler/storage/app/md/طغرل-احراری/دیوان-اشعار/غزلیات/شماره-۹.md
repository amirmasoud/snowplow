---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>اگر اینست تندی توسن جولان شتابش را</p></div>
<div class="m2"><p>غبار خاک شو تا وارسی بوس رکابش را!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز تار رشته اندیشه دوزم جامه نازش</p></div>
<div class="m2"><p>حدیث نکهت گل می‌درد طرف نقابش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر خوانی خطی از مصحف رخساره لیلی</p></div>
<div class="m2"><p>نثار روضه مجنون نما نقد ثوابش را!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل حیرت‌پرستم را نباشد جز تپش سودی</p></div>
<div class="m2"><p>مگر آرام بخشد جلوه او اضطرابش را؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به لب زان خط موزونش نهادم مهر خاموشی</p></div>
<div class="m2"><p>قلم از موی چینی می‌توان کردن کتابش را!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مضراب غمش کس را نباشد ناخن دخلی</p></div>
<div class="m2"><p>مگر از ناله بلبل کند تار ربابش را؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به تحریر سواد نسخه آشفته زلفش</p></div>
<div class="m2"><p>ز موج می رقم باید شکست پیچ و تابش را!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر داری خیال خاک پای توسنش بودن</p></div>
<div class="m2"><p>نما ورد زبان یا لیتنی کنت ترابش را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش آمد در مذاقم طغرل این یک مصرع بیدل</p></div>
<div class="m2"><p>برین سرچشمه رحمی کن که موجی نیست آبش را!</p></div></div>