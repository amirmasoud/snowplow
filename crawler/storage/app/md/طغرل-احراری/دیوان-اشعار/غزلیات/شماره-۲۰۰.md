---
title: >-
    شمارهٔ ۲۰۰
---
# شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>بتی دارم که چتر مهر باشد زیب اورنگش</p></div>
<div class="m2"><p>فلک بر چشم سازد سرمه خاک لعل شبرنگش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هجرانش به تلخی جان شیرین می‌کنم لیکن</p></div>
<div class="m2"><p>چو فرهادم درین کوهسار و کی اندیشم از سنگش؟!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از فکر میانش کی برون آیم سر مویی؟!</p></div>
<div class="m2"><p>بدین مضمون در آغوش سخن بگرفته‌ام تنگش!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گلگشت چمن هرگه نقاب از رخ براندازد</p></div>
<div class="m2"><p>عرق بر روی گل گل می‌کند از شوخی رنگش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تبسم با عتاب او سلوک داوری دارد</p></div>
<div class="m2"><p>چو برگ بید می‌لرزم من از این صلح و این جنگش!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فسون چشم جادویش به رنگی می‌برد دل را</p></div>
<div class="m2"><p>که امکان رهایی نیست از آیین نیرنگش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به چین بهزاد جز چین جبین دیگر نمی‌بیند</p></div>
<div class="m2"><p>که باشد چین زلف او گریبانگیر ارژنگش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه سان سر برکشم ز امرش که چون رنگ حنای او</p></div>
<div class="m2"><p>عنان توسن عمرم بود امروز در چنگش!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه خوش گفتست طغرل حضرت بحر سخن بیدل</p></div>
<div class="m2"><p>به تاراج جنون دادم چه هستی و چه فرهنگش!</p></div></div>