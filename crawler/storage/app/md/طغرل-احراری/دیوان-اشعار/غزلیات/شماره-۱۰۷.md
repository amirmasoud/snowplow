---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>بت نامهربانم کی ز حال من خبر دارد</p></div>
<div class="m2"><p>ز مهرش نگذرم یک ذره گر از من گذر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روم هر دم به یاد آتش رخساره‌اش از خود</p></div>
<div class="m2"><p>شکست رنگ من آهنگ پرواز شرر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشم چون شانه از مضمون فکر تام گیسویش</p></div>
<div class="m2"><p>که دامان خیالم رتبه جیب سحر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به راه انتظارش بسته محمل ناقه هوشم</p></div>
<div class="m2"><p>پریدن‌های رنگم ساز آهنگ سفر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بزم وصل او شد جوهر دل عرض خاموشی</p></div>
<div class="m2"><p>به جز حیرت دگر آیینه ما کی هنر دارد؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یک نیم‌نگه از نرگس شوخش مشو ایمن</p></div>
<div class="m2"><p>که قانون طلسمش ساز نیرنگ دگر دارد!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببین آهنگ بال‌افشانی صید محبت را</p></div>
<div class="m2"><p>که از ذوق تپیدن بسمل او بال و پر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به کوه از تلخی هجران چه غم فرهاد محزون را</p></div>
<div class="m2"><p>صدای تیشه‌اش چون نی ز شیرینی شکر دارد!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پریشانی نسازد کم سر مو خاطر جمعش</p></div>
<div class="m2"><p>اگر مجنون خیال زلف لیلی بیشتر دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز مهر عارض زلفش مرا این شبهه روشن شد</p></div>
<div class="m2"><p>که گر صد شام نومیدیست امید سحر دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوشا از مصرع سلطان او رنگ سخن طغرل</p></div>
<div class="m2"><p>تو اکنون ناله کن بیدل که آهنگت اثر دارد</p></div></div>