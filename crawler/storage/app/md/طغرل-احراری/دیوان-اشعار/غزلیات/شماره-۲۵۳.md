---
title: >-
    شمارهٔ ۲۵۳
---
# شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>گرفتار کمند آن خم زلف بناگوشم</p></div>
<div class="m2"><p>حباب‌آسا به سودای خیالش خانه‌بردوشم!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ادیبا با من از درس خرد دیگر مگو حرفی</p></div>
<div class="m2"><p>چو مجنون در خیال طره لیلی بود هوشم!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی‌خواهم به غیر از دولت دیدار وصل او</p></div>
<div class="m2"><p>اگر فرزند خورشید فلک آید در آغوشم!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا از زندگی بهتر که مردن در تمنایش</p></div>
<div class="m2"><p>گر در جام جم آب حیات آید نمی‌نوشم!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همین شد سرنوشت قسمت روز ازل با من</p></div>
<div class="m2"><p>قضا از حلقه‌ای داغ غلامی کرد در گوشم!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نباشد درخور من هیچ لاف نغمه شوقش</p></div>
<div class="m2"><p>ز تشویر حرارت‌های عشق یار در جوشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوشا در پیکر من کسوت خاک درش طغرل</p></div>
<div class="m2"><p>به جای پیرهن جز او ز عالم چشم می‌پوشم!</p></div></div>