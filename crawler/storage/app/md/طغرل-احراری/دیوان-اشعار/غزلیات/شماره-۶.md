---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>نزهت کویت برد رونق گلزار را</p></div>
<div class="m2"><p>شهره حسنت درد پرده اسرار را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زخم فراق تو را هیچ به غیر از اجل</p></div>
<div class="m2"><p>چاره نباشد دگر بنده ناچار را!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیند اگر برهمن طره شبرنگ تو</p></div>
<div class="m2"><p>از خم زلفت کند حلقه زنار را!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نغمه شوقت کند پرده مضراب غم</p></div>
<div class="m2"><p>از بم و زیر جنون زیر و بم تار را!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مسند کوی تراست رتبه اورنگ جم،</p></div>
<div class="m2"><p>کرد قضا قسمتم سایه دیوار را!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مردنم از رشک به باد صبا گر کند</p></div>
<div class="m2"><p>محرم خاک درت دیده اغیار را!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باد تو را آفرین طغرل رنگین سخن،</p></div>
<div class="m2"><p>بلبل گویا تویی گلشن اشعار را!</p></div></div>