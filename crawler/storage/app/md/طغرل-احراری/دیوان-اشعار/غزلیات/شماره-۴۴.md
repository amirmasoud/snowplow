---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>ای ز رخسار تو گل شرمنده در گلزارها</p></div>
<div class="m2"><p>وز خرامت آب را زنجیر حیرانی به پا!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دم به دم سوی تو قلاب محبت می‌کشد</p></div>
<div class="m2"><p>جز دلیل عشق نبود کس به سویت رهنما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست غیر از مد آه خود عصای دگرم</p></div>
<div class="m2"><p>قامتم شد زیر بار محنت عشقت دو تا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من شهید تیغ هجرم سوی من گامی بزن</p></div>
<div class="m2"><p>کاش از خونم بود اندر کف پایت حنا!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در طریق عشوه رفتار تو دستورالعمل</p></div>
<div class="m2"><p>در سلوک فتنه آیین تو سرمشق جفا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نشینی پیش رویت فتنه‌ها پیدا شود</p></div>
<div class="m2"><p>ور خرامی، هر طرف آشوب خیزد از قفا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر نفس از شوق بی‌تقیید آیین ادب</p></div>
<div class="m2"><p>طائر نظاره سازد جانب کویت هوا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کور به چشمی ندارد سرمه از گرد رهت!</p></div>
<div class="m2"><p>کیست در عالم نسازد خاک پایت توتیا؟!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی بود امروز در عالم کسی از عاشقان</p></div>
<div class="m2"><p>زیر بار عشق تو مانند من صبرآزما؟!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پرده زیر و بم قانون مضراب جنون</p></div>
<div class="m2"><p>کرده ز آهنگ فراقت ساز «عشاق » از «نوا»</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌توان دریافت ساز راحت آزادگان</p></div>
<div class="m2"><p>از طراز آستین شاه و نقش بوریا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بارگاه فقر کم از مسند فغفور نیست</p></div>
<div class="m2"><p>از شکست چینی می‌آید به گوشم این صدا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای خوشا، طغرل که بیدل می‌سراید مصرعی</p></div>
<div class="m2"><p>خفته در خون شهیدت جوش گلزار بقا</p></div></div>