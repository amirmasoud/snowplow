---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>برد زلفش گرو از مشکلم آسانی را</p></div>
<div class="m2"><p>کرد نقشی به دلم خط پریشانی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غنچه‌اش خندد اگر هیچ نباشد قدری</p></div>
<div class="m2"><p>پیش شفتالوی او قیمت خوبانی را!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گیر از طره او سرخط آشفته‌دلی</p></div>
<div class="m2"><p>نیست استاد دگر درس پریشانی را!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق می‌خواهی؟ شو از بند تعلق آزاد!</p></div>
<div class="m2"><p>رگ گل خار بود بلبل زندانی را!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بار منت نکشد کسوت ما از سوزن</p></div>
<div class="m2"><p>بخیه حاجت نبود جامه عریانی را!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست در کلبه ما ننگ خراب‌آبادی</p></div>
<div class="m2"><p>گنج بسیار بود مخزن ویرانی را!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاقبت اشک تو گردد گهر بی‌قیمت</p></div>
<div class="m2"><p>ریز در کام صدف گوهر نیسانی را!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای که در صورت خوبی ز همه بهزادی</p></div>
<div class="m2"><p>نیست در نقش تو حدی قلم مانی را!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روز عید است نشین در بر من یک ساعت</p></div>
<div class="m2"><p>وقف نظاره کنم دیده قربانی را!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کی به سلک فضلا آیی؟! نداری جوهر!</p></div>
<div class="m2"><p>بیش ازین سکه مزن لاف سخندانی را!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آفرین باد برین مصرع بیدل طغرل</p></div>
<div class="m2"><p>چین دامان ادب کن خط پیشانی را!</p></div></div>