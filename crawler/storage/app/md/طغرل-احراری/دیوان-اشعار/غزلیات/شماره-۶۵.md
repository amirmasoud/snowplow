---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>چند روزی در جهان ای عمر مهمانی بس است</p></div>
<div class="m2"><p>از حصول مدعا آه پشیمانی بس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آرزوی جاه داری گر ز نقش اعتبار</p></div>
<div class="m2"><p>یاد تعمیر خیالت خانه ویرانی بس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشتی از درس کمال امروز غافل مر تو را</p></div>
<div class="m2"><p>همچو یبروج‌الصنم یک نام انسانی بس است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند گویی کز تعلق‌های امکان بگذرم</p></div>
<div class="m2"><p>بگذری گر از جهان یک دامن افشانی بس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیر در راه طلب از نقش پای او سبق</p></div>
<div class="m2"><p>مر تو را آیینه‌سان یک چشم حیرانی بس است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کجا خواهی کز آشفتن به جمعیت رسم</p></div>
<div class="m2"><p>از تصورهای زلفش یک پریشانی بس است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر همی‌خواهی که اسپ خویش از فرزین کنی</p></div>
<div class="m2"><p>در بساط نرد غم یک دانه گردانی بس است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوش از سیب زنخدانش گرفتی بهره‌ای</p></div>
<div class="m2"><p>از لب لعلش تو را امروز خوبانی بس است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا به کی گویی که خوانم دفتر عشاق را؟!</p></div>
<div class="m2"><p>از کتاب محنتش یک سطر اگر خوانی بس است!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کعبه بیت اللّٰه فکر مرا طغرل کنون</p></div>
<div class="m2"><p>از شکار صید معنی‌ها یکی بانی بس است!</p></div></div>