---
title: >-
    شمارهٔ ۲۳۰
---
# شمارهٔ ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>بشنود بلبل اگر اندر گلستان ناله‌ام</p></div>
<div class="m2"><p>می‌کند اندر چمن تمهید سامان ناله‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر ساز خویش نی از چوب طوبا بایدم</p></div>
<div class="m2"><p>تا رسد در گوش آن سرو خرامان ناله‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عرض مطلب نیست حاجت پیش ارباب کرم</p></div>
<div class="m2"><p>سر زند در پاش از چاک گریبان ناله‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوختم پروانه‌سان در آتش شوق عمل</p></div>
<div class="m2"><p>می‌کند در شام غم جوش چراغان ناله‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زینهار ای آرزو گستاخ بر آن در مرو</p></div>
<div class="m2"><p>خسرو غم را بود امروز دربان ناله‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوشه‌چین خرمن گیسوی مشکینش بودم</p></div>
<div class="m2"><p>زآن سبب رفته‌ست اندر سنبلستان ناله‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساز ما هر چند در بازار عبرت کم‌بهاست</p></div>
<div class="m2"><p>نزد عشاق است لیکن خوشتر از جان ناله‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست حاجت در مریض عشق داروی دگر</p></div>
<div class="m2"><p>هست در زخم غم او بس که درمان ناله‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نالم از یک مصرع بیدل که طغرل گفته است</p></div>
<div class="m2"><p>درد آن دارم که خواهد شد پریشان ناله‌ام</p></div></div>