---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>در محفل رقیبان رویت شکفته گل گل</p></div>
<div class="m2"><p>با ما رسید نوبت کار تو شد تغافل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر محیط عشقت عمریست ناخدایم</p></div>
<div class="m2"><p>افکنده خویشتن را در کشتی توکل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مژگان کشیده صف صف چشمان سحر سازت</p></div>
<div class="m2"><p>قصد کدام داری با این همه تجمل؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شور طرب فزاید از خنده دهانت</p></div>
<div class="m2"><p>وصف تو را نماید مینا به بانگ قل قل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با یاد عارض تو گل جامه را دریده</p></div>
<div class="m2"><p>بی‌تو به باغ و گلشن در آه و ناله بلبل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داغ است لاله را دل از حسرت جمالت</p></div>
<div class="m2"><p>خون می‌خورد ز لعلت پیوسته شیشه مل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آشفته روزگارم طغرل به دور زلفت</p></div>
<div class="m2"><p>بادا همیشه ما را تا حشر این تسلسل!</p></div></div>