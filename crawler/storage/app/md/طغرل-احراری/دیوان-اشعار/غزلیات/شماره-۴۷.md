---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>می‌نماید بر لب جو عکس ماه من در آب</p></div>
<div class="m2"><p>می‌توان از ماه تا ماهی همه بودن در آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست سودی سفله را از صحبت روشندلان</p></div>
<div class="m2"><p>سخت رسر، می‌شود آید اگر آهن در آب!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت زاهد خویش را در خواب دیدم در برش</p></div>
<div class="m2"><p>گفتمش تعبیر خواب خود بگو روشن در آب!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در تلاش عکس یک رو صد گریبان پاره شد</p></div>
<div class="m2"><p>کی به آسانی رسد آیینه را دامن در آب؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوی سبقت می‌برم امروز از فتوای شک</p></div>
<div class="m2"><p>مردم آبی اگر دعوا کند با من در آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ ممکن نیست در گرداب این امواج غم</p></div>
<div class="m2"><p>کشتی مقصود را بی‌ناخدا رفتن در آب!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سلوک عشق کم از بچه بط نیستی</p></div>
<div class="m2"><p>در رضای دوست هردم می‌دهد او تن در آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیر طوفان سرشکم طغرل از هجران او</p></div>
<div class="m2"><p>به که از این خاکساری‌ها مرا مردن در آب!</p></div></div>