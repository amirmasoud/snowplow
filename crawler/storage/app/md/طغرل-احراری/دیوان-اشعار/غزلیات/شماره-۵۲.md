---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>می‌رسد از ناله بر گردون لوای عندلیب</p></div>
<div class="m2"><p>شاخ گل چون تخت جم باشد برای عندلیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چمن امروز در دربار شاهنشاه گل</p></div>
<div class="m2"><p>هیچ کس را نیست گستاخی سوای عندلیب!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با همه خواری نگردد دور از طرف چمن</p></div>
<div class="m2"><p>آفرین بر عهد میثاق و وفای عندلیب!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بود صد زیر و بم در نغمه موسیقار را</p></div>
<div class="m2"><p>کی نشیند در سلوک ناله جای عندلیب؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باغ از باد خزان دارد لباس ماتمی</p></div>
<div class="m2"><p>برگ‌ریزی‌های گل باشد عزای عندلیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جذب معشوقان بود هر دم کمند عاشقان</p></div>
<div class="m2"><p>کس نباشد سوی گلشن رهنمای عندلیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این همه با یاد گل شب تا سحر جان می‌کند</p></div>
<div class="m2"><p>گر بود صد جان مرا باشد فدای عندلیب!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوش دیدم با هزاران ناله اندر صحن باغ</p></div>
<div class="m2"><p>گر نه گل رحم آورد امروز وای عندلیب!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ختم سازی در چمن درس غم عشاق را</p></div>
<div class="m2"><p>گوش اندازی اگر بر ناله‌های عندلیب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس که دارد ناله اینجا جلوه رنگ اثر</p></div>
<div class="m2"><p>باشدم امیدواری از دعای عندلیب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این حدیث ای باغبان از من بگو در گوش گل</p></div>
<div class="m2"><p>رحم می‌باید کنون در التجای عندلیب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وه چه خوش گفتست طغرل بیدل بحر سخن</p></div>
<div class="m2"><p>شرم دار از دیدن گل بی‌رضای عندلیب!</p></div></div>