---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>اگر اینست اکنون قدر رفعت آشنایش را</p></div>
<div class="m2"><p>ز چرخ آید به شاگردی مسیحا پاسبانش را!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به روی صفحه هردم از نی کلکم شکر ریزد</p></div>
<div class="m2"><p>به هنگام رقم گر در قلم آرم زبانش را!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به مضمون میانش گر کمر بندم ز مو لیکن</p></div>
<div class="m2"><p>خلل از سایه مو می‌رسد موی میانش را!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخندد غنچه در گلشن ز حسرت خون دل گردد</p></div>
<div class="m2"><p>اگر در باغ ناگه بشنود وصف دهانش را!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود درس خط او بی‌اشارات لبش مشکل</p></div>
<div class="m2"><p>که باشد ترجمان دیگری مر ترجمانش را!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهانی بسمل داغ خدنگ او بود لیکن</p></div>
<div class="m2"><p>نشانی جز دل من کی بود تیر کمانش را؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نباشد هیچ ممکن بوالهوس را ذره‌ای مهرش</p></div>
<div class="m2"><p>که می‌باشد اثر از سایه عنقا نشانش را!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مکن جز آه بلبل شعله شمع دلیل خود</p></div>
<div class="m2"><p>که غیر از ناله کی گیرد درین راه کس عنانش را؟!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شود در باغ آخر طوق قمری پایبند غم</p></div>
<div class="m2"><p>گر آزادی همین باشد قد سرو روانش را!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوشا طغرل ازین یک مصرع بیدل که می‌گوید</p></div>
<div class="m2"><p>که یا رب مهربان گردان دل نامهربانش را!</p></div></div>