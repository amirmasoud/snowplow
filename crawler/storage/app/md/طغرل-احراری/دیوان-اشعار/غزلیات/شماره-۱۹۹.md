---
title: >-
    شمارهٔ ۱۹۹
---
# شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>هر کس که پر از باده عشق است ایاغش</p></div>
<div class="m2"><p>جز بوی گل عیش نباشد به دماغش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سرو ز تشویش تعلق بود آزاد</p></div>
<div class="m2"><p>آن را که ز خاکستر قمریست سراغش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون لاله درین باغ به نیرنگ محبت</p></div>
<div class="m2"><p>کردند نشان توسن عمر تو ز داغش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفتم به تماشای بهار چمن عشق</p></div>
<div class="m2"><p>جز سنبل آشفته دگر نیست به باغش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک ذره گرت مهر و وفا هست توان کرد</p></div>
<div class="m2"><p>از سایه عنقا اثر رنگ سراغش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کس که بود آتش سودا به سر او</p></div>
<div class="m2"><p>روشن بود از روغن پروانه چراغش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راهیست ز تقلید که در باغ حقیقت</p></div>
<div class="m2"><p>یاد از روش کبک دهد جلوه زاغش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرمایه صد عیش به یک ذره نسنجد</p></div>
<div class="m2"><p>در زاویه غم بود آن را که فراغش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای سوخته داغ هوس بگذر ازین غم</p></div>
<div class="m2"><p>از بس که نداری خبر لاله باغش!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طغرل به جهانی ندهم مصرع بیدل</p></div>
<div class="m2"><p>خورشید نه جنسی است که جویی به چراغش!</p></div></div>