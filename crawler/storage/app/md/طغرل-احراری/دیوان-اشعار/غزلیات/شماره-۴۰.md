---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>عمرها شد می‌زنم در راه عشقش گام‌ها</p></div>
<div class="m2"><p>رفتن رنگ است از ما بستن احرام‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اعتبار ما و من از نشئه کیف و کم است</p></div>
<div class="m2"><p>فال فرصت می‌زند هر دم صدای جام‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرخ و زرد این جهان را حاجت اکسیر نیست</p></div>
<div class="m2"><p>انقلاب رنگ دارد گردش ایام‌ها!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارد الهام تسلی از تپش ذوق دلم</p></div>
<div class="m2"><p>شهپر جبریل باشد شوخی پیغام‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صید قلاب محبت را رهایی مشکل است</p></div>
<div class="m2"><p>خون بسمل می‌تپد از حلقه این دام‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ کس در دهر از جام امل مسرور نیست</p></div>
<div class="m2"><p>تلخی زهر است یکسر شربت این کام‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون سحر خلق جهان کافور دارند در نفس</p></div>
<div class="m2"><p>پختگی هرگز نباشد قسمت این خام‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد فلاطون از ره حکمت نمی‌سازد علاج</p></div>
<div class="m2"><p>خشکی نبض جنون از روغن بادام‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طغرل از آوازه هستی عرق گل کرده‌ایم</p></div>
<div class="m2"><p>حاصلی جز شرم نبود در نگین زین نام‌ها</p></div></div>