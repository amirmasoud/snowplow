---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>پرتو آیینه‌ام حیرت‌بیانی می‌کند</p></div>
<div class="m2"><p>صورت بهزاد فکر من نه مانی می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وضع میزان خیالم را مپرس از نازکی</p></div>
<div class="m2"><p>سایه مویی به یاد آید گرانی می‌کند!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقده‌های مشکل رمز و کنایات مرا</p></div>
<div class="m2"><p>فهمد آن دانا که حل بوالمعانی می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سریر موشکافی شاهم از فکر رسا</p></div>
<div class="m2"><p>در دیار نظم طبعم حکمرانی می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اختر طالع به اوج دانشم دنباله زد</p></div>
<div class="m2"><p>ملک معنی را دلم صاحبقرانی می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه شد در بحر اشعارم نخستین ناخدا</p></div>
<div class="m2"><p>زورق از فهم بلند خویش ثانی می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهره نرد بساط بیت موزون مرا</p></div>
<div class="m2"><p>هرکسی با قدر دانش دانه‌رانی می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در شکار صید معنی‌های وحشت‌رام من</p></div>
<div class="m2"><p>گه خدنگ از غمزه که ابرو کمانی می‌کند؟!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طغرل‌آسا روز و شب در اوج مضمون‌های بکر</p></div>
<div class="m2"><p>طائر رمز نکاتم پرفشانی می‌کند</p></div></div>