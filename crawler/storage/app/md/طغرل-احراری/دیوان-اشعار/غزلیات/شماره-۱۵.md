---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>بس که شهد مدعا حال نشد کام مرا</p></div>
<div class="m2"><p>کرده‌اند از سنگ نومیدی مگر جام مرا؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غرق موج شبنم خجلت شود رخسار او</p></div>
<div class="m2"><p>هرکه جوید چون نگین گر از جهان نام مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنقدر تمهید سامان جنون دارد دلم</p></div>
<div class="m2"><p>نیست جز خشکی تقاضا طبع بادام مرا!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دل عاشق نباشد رتبه پست و بلند</p></div>
<div class="m2"><p>فرق چون آیینه از در کی بود بام مرا؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند پاشیدم سرشک دانه تخم عمل</p></div>
<div class="m2"><p>کاش افتد صید مطلب حلقه دام مرا!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساز قانون مروت کن به مضراب کرم</p></div>
<div class="m2"><p>از صبا گر بشنوی آهنگ پیغام مرا!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرصت تمهید هستی نیست در باغ جهان</p></div>
<div class="m2"><p>چون شرر یکسان شمر آغاز و انجام مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرخط درس کمال دیگرت در کار نیست</p></div>
<div class="m2"><p>سبحه کن اندر سلوک عشق هر گام مرا!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طغرل از تمکین دل سامان راحت داشتم</p></div>
<div class="m2"><p>بی‌قراری برد آخر ذوق آرام مرا!</p></div></div>