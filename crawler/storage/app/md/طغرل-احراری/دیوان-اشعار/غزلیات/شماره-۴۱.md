---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>اگر اینست با خوبان عالم آشنایی‌ها</p></div>
<div class="m2"><p>توان کردن چو نی فریاد از دست جدایی‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نآمد زورق من در کنار ساحل وصلش</p></div>
<div class="m2"><p>به بحر عشق هر چندی که کردم ناخدایی‌ها!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توان دریافت از مضمون من کوتاهی فکرم</p></div>
<div class="m2"><p>که نخلش را مثل کردم به شمشاد از رسایی‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به روی خویش تا کردی دچار آیینه حیرانم</p></div>
<div class="m2"><p>که جوهر را نمی‌زیبد جلا از خودنمایی‌ها!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وفا چون عمر من ای بی‌وفا هرگز نمی‌سازی</p></div>
<div class="m2"><p>مگر از عمر آموزی تو رسم بی‌وفایی‌ها؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خاک آرم به زور پنجه گفتار اغیارت</p></div>
<div class="m2"><p>اگر در عشق تو با من کند زورآزمایی‌ها!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بحمدالله که با دیر محبت مرشدم طغرل</p></div>
<div class="m2"><p>به می آلوده کردم پیرهن از پارسایی‌ها!</p></div></div>