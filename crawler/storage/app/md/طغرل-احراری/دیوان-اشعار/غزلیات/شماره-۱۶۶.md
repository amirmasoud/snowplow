---
title: >-
    شمارهٔ ۱۶۶
---
# شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>در حریم یار گر آری گذر</p></div>
<div class="m2"><p>ای صبا از من به او پیغام بر!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تو احوال مرا پرسد بگو</p></div>
<div class="m2"><p>بنده خود را ز محنت باز خر!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بگوید نقد عشقش صرف کیست؟</p></div>
<div class="m2"><p>گوی سودای تو را دارد به سر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ورد من پرسد فراموشت مباد</p></div>
<div class="m2"><p>گوی می‌گوید ز غیرت الحذر!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از که عشق آموخت گوید گو ز من</p></div>
<div class="m2"><p>عشقبازی من است ارث پدر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از غمم فریاد دارد گفت اگر</p></div>
<div class="m2"><p>گوی با یاد تو هر شام و سحر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاد راه عشق گوید چیست گو</p></div>
<div class="m2"><p>توشه عاشق بود لخت جگر!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در فراقم گفت می‌ریزد سرشک؟</p></div>
<div class="m2"><p>ابر کی خالی بود گو از مطر؟!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از قضا پرسد ز آغاز غمم</p></div>
<div class="m2"><p>گوی انجام محبت از قدر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میوه نخلم اگر گوید که چیست؟</p></div>
<div class="m2"><p>گوی ثروت را جفا باشد ثمر!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ور بگوید با چه می‌ماند دلم؟</p></div>
<div class="m2"><p>سنگ را گفتن توان گو موم تر!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جوید اندر نرمی خویش نظیر</p></div>
<div class="m2"><p>تو حایت سر کن از نار شرر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عارضم روشن اگر گوید ز چیست؟</p></div>
<div class="m2"><p>در جوابش گوی روشن از قمر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خواهد او با نرگس مستش شبیه</p></div>
<div class="m2"><p>بی‌توقف پرده بادام در!</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با خط لعلش اگر جوید عدیل</p></div>
<div class="m2"><p>گوی آن‌دم داستان نیلوفر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وز لبش ار گفتگویی آورد</p></div>
<div class="m2"><p>در میان آور تو حرف نیشکر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باز ای باد صبا زنهار گو</p></div>
<div class="m2"><p>از چه دور انداخت ما را از نظر؟!</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر بود مقصود او نقد عیار</p></div>
<div class="m2"><p>چون صدف دارم ولی پر از گهر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خاطر او را اگر میل طلاست</p></div>
<div class="m2"><p>چهره زرد مرا بشمار زر!</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صبح رویش از بداهت دم زند</p></div>
<div class="m2"><p>بی‌تأمل گوی فیهی النظر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در فراقش گر مثل خواهد بگو</p></div>
<div class="m2"><p>قطعه‌ای باشد ز هجرانت سقر!</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مسکنم پرسد گر آن لیلی اساس</p></div>
<div class="m2"><p>گوی مجنون را نمی‌باشد مقر!</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نسبت خود را ز خوبان جست گو</p></div>
<div class="m2"><p>امتیاز بیت طغرل از شکر!</p></div></div>