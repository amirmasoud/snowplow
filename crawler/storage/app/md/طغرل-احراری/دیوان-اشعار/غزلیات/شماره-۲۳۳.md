---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>طرب‌پیمای عشقم خاکساری‌هاست معراجم</p></div>
<div class="m2"><p>تپیدن‌ها بود طومار نبض سعی‌الحاجم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چراغم یافت آخر از خموشی سرفرازی‌ها</p></div>
<div class="m2"><p>مرصع شد ز سنگ سرمه گویا گوهر تاجم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سفیر بلبل آهم چو نی گر ناله انگیزد</p></div>
<div class="m2"><p>نیستان یک قلم آتش شود ز اخگر دهد باجم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سعی گفتگوی او سرم در پای دار آمد</p></div>
<div class="m2"><p>به فتوای محبت گوییا منصور حلاجم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا ای مشرق صبح سعادت در برم یک دم</p></div>
<div class="m2"><p>که من از بهر تشریف قدومت سخت محتاجم!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازآن روزی که نخل قامتت بشکست طوبی را</p></div>
<div class="m2"><p>به سودای خیال سرو بالای تو دراجم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلند است آنقدر فکر بلندم در سخن طغرل</p></div>
<div class="m2"><p>تو پنداری که اندر تار و پود شعر نساجم</p></div></div>