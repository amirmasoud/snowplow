---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>ای روی تو آفتاب خاور</p></div>
<div class="m2"><p>لعل لب تو زلال کوثر!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد معجزه مسیح داری</p></div>
<div class="m2"><p>افسوس که نیستی پیمبر!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرق تو میان خوبرویان</p></div>
<div class="m2"><p>چون فرق عرض بود ز جوهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر مصر جمال را عزیزی</p></div>
<div class="m2"><p>یوسف نبود تو را برابر!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای آمده با قد بلندت</p></div>
<div class="m2"><p>شمشاد غلام و سرو چاکر!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همشیره کهتر تو مهتاب</p></div>
<div class="m2"><p>خورشید بود تو را برادر!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک قدم تو کحل بادا</p></div>
<div class="m2"><p>در چشم سپهر چرخ اخضر!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خاک عدم به ذوق خیزم</p></div>
<div class="m2"><p>رانی به سرم تو گر تکاور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امروز شکسته بیت طغرل</p></div>
<div class="m2"><p>بازار نبات و نرخ شکر!</p></div></div>