---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>چه خاصیت بود یارب نصیب چشم زهگیرش</p></div>
<div class="m2"><p>که در پرواز می‌آید دل از شوق پر تیرش؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال ابرویش کن قطعه رنگین هوس داری</p></div>
<div class="m2"><p>که سرمشق شهادت نیست غیر از مد شمشیرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سودای سر بازار غم دیوانه می‌گردد</p></div>
<div class="m2"><p>اگر افتد به پای عقل زلف همچو زنجیرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز مضمون بلندی‌های قدش یک قلم گویم</p></div>
<div class="m2"><p>قلم از شاخ طوبا کن اگر خواهی تو تحریرش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو من هر کس حدیث لعل شیرینش بیان سازد</p></div>
<div class="m2"><p>شکر ریزد به هنگام سخن از شهد تقریرش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمند جذبه شوقش عجب خاصیتی دارد</p></div>
<div class="m2"><p>که باشد دانه‌های دام مطلب خون نخچیرش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکست رنگ جرعت شد نصیب خامه مانی</p></div>
<div class="m2"><p>که امکان درستی نیست اندر نقش تصویرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه افسون است یارب همچو مخمل نرگس او را</p></div>
<div class="m2"><p>که می‌باشد به خواب ناز و بیدار است تأثیرش؟!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به شام هجر از صبح وصال او مشو غافل</p></div>
<div class="m2"><p>که می‌جوشد ز پستان عمل شیر از تباشیرش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه درد سر مرا از اعتبارات محک اکنون</p></div>
<div class="m2"><p>عیار صافی‌ای دارم که حاجت نیست اکسیرش!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز معمار قضا هرگز نبیند روی آبادی</p></div>
<div class="m2"><p>بنای خانه هستی که ویرانست تعمیرش!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خوشا از مصرع موزون دریای سخن طغرل</p></div>
<div class="m2"><p>عرق کرد آه من آخر ز خجلت‌های تأثیرش!</p></div></div>