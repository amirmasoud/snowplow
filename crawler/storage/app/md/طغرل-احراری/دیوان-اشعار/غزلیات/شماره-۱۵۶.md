---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>ز جوش اشک در آبم گهربار اینچنین باید</p></div>
<div class="m2"><p>ز شام غم سیه روزم شب تار اینچنین باید!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی‌باشد به غیر یأس دیگر دستگیر من</p></div>
<div class="m2"><p>به نکبت‌خانه دنیا مددگار اینچنین باید!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نداند کفر زلفش هیچ آیین مسلمانی</p></div>
<div class="m2"><p>به کیش اهرمن البته زنار اینچنین باید!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دست ناکسی همسنگ پای مور گردیدم</p></div>
<div class="m2"><p>به میزان محبت وزن و مقدار اینچنین باید!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>متاع هر دو عالم نقد محنت را نمی‌ارزد</p></div>
<div class="m2"><p>قماش جنس غم را نرخ بازار اینچنین باید!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ادیب طفل اشک عاشقان باشد سرشک من</p></div>
<div class="m2"><p>بلی در سلک جوهر در شهوار اینچنین باید!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پر پروانه باشد روغن شمع وفا امشب</p></div>
<div class="m2"><p>به عرض مدعا سامان اظهار اینچنین باید!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرق گل می‌کند از جوهر عکس رخش هر دم</p></div>
<div class="m2"><p>به حیرت خانه آیینه معمار اینچنین باید!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هزارش آفرین طغرل برین یک مصرع بیدل</p></div>
<div class="m2"><p>ز هر مو دام بر دوشم گرفتار اینچنین باید!</p></div></div>