---
title: >-
    شمارهٔ ۱۴۴
---
# شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>عارض چون ماهش از برقع نمایان می‌کند</p></div>
<div class="m2"><p>عالمی را شمع رخسارش چراغان می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خیال غنچه‌اش دارم به دل جمعیتی</p></div>
<div class="m2"><p>خاطرم را یاد آن گیسو پریشان می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش با یاد رخش از دیده باریدم گهر</p></div>
<div class="m2"><p>موج اشکم در جهان امروز طوفان می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل به کوی یار می‌باشد ولی هر دم چو مور</p></div>
<div class="m2"><p>آن تفاخرها که در دست سلیمان می‌کند!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای پری‌رو نقش نیرنگ کدامین صورتی؟!</p></div>
<div class="m2"><p>چشم صد آیینه را عکس تو حیران می‌کند!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر همین باشد گداز شعله شمع وفا</p></div>
<div class="m2"><p>خانه زنبور را پروانه ویران می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با مریض عشق دارو نیست در دکان دهر</p></div>
<div class="m2"><p>مشکل مجنون ما را لیلی آسان می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حیرتی دارم که طغرل بلبل از شب تا سحر</p></div>
<div class="m2"><p>در حضور شاهد گل سخت افغان می‌کند</p></div></div>