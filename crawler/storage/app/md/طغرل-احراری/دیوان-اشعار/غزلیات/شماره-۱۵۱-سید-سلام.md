---
title: >-
    شمارهٔ ۱۵۱ - سید سلام
---
# شمارهٔ ۱۵۱ - سید سلام

<div class="b" id="bn1"><div class="m1"><p>سرم به زیر قدوم تو گر غبار شود</p></div>
<div class="m2"><p>ز یمن مقدم تو خوان من مزار شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی گذر به سر تربتم که بعد از مرگ</p></div>
<div class="m2"><p>ز خاک من به رهت نرگس انتظار شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دمی به سوی چمن بگذر و تماشا کن</p></div>
<div class="m2"><p>که صحن باغ پر از غل غل هزار شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سمن که زلف تو را دید سر فرود آرد</p></div>
<div class="m2"><p>گل از طراوت رخساره تو خوار شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لبت که وقت تکلم حیات می‌بخشد</p></div>
<div class="m2"><p>مسیح از لب لعل تو شرمسار شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امیر کشور حسنی و نیست مانندت</p></div>
<div class="m2"><p>هر آن که روی تو را دید بی‌قرار شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منم که حلقه به گوش توام نمی‌پرسی</p></div>
<div class="m2"><p>چه می‌شود که تو را طغرل اعتبار شود؟!</p></div></div>