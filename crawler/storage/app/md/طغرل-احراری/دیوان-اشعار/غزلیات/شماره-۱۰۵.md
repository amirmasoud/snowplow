---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>ندانم ساغر عشرت کرا سرشار می‌گردد</p></div>
<div class="m2"><p>که امشب چشم ساقی چون قدح بیدار می‌گردد!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رگ دست مریض عشق دارد شوخی دیگر</p></div>
<div class="m2"><p>فلاطون از خمار نبض او بیمار می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلندی‌های سرو از پستی اقبال قمری شد</p></div>
<div class="m2"><p>نباشد آه بلبل در چمن گل خوار می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیال طره لیلی بود زنجیر پای او</p></div>
<div class="m2"><p>اگر مجنون ما در کوچه و بازار می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر محفل که شمع عارض او پرتوافکن شد</p></div>
<div class="m2"><p>چو من پروانه بر گرد سرش بسیار می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر از مشکلات زلف او نحوی کنی روشن</p></div>
<div class="m2"><p>خفای درس الفت معنی تکرار می‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشاید هر که بر رویش دری از خانه حیرت</p></div>
<div class="m2"><p>ولی نقش وجودش صورت دیوار می‌گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دست اهرمن چون شانه گر آید سر مویی</p></div>
<div class="m2"><p>سواد کفر زلفش حلقه زنار می‌گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شدم پروانه این مصرع بیدل ازآن طغرل</p></div>
<div class="m2"><p>چو شمع از عضو عضوم آگهی سرشار می‌گردد</p></div></div>