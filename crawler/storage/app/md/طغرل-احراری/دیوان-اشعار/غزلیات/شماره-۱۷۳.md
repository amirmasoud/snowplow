---
title: >-
    شمارهٔ ۱۷۳
---
# شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>شب که با یاد رخت داشت دلم سوز و گداز</p></div>
<div class="m2"><p>برق آهم به تکاپوی تو شد در تک و تاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوشم از سر به خیال سر زلف تو برفت</p></div>
<div class="m2"><p>همچو موسی طرف طور دوان از پی راز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صانع روز ازل را تو چه خوش مصنوعی</p></div>
<div class="m2"><p>که به عکس تو بود آیینه را روی نیاز!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لعبتان را روش پرده افسون یک موست</p></div>
<div class="m2"><p>کرده زیر و بم عشق تو مرا لعبت باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون به هنگام خرامی تو به گلگشت چمن</p></div>
<div class="m2"><p>چشم نرگس به تماشای جمالت شده باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مست صهبای جمالی و به خود می‌نازی</p></div>
<div class="m2"><p>یا مگر کلک قضا بسته اساس تو ز ناز؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرغ دل بس که به تیر نگهت معتاد است</p></div>
<div class="m2"><p>صرف هر سفله مکن جانب طغرل انداز!</p></div></div>