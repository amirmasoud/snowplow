---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>ای از بهار عارضت نظاره را جان در بغل</p></div>
<div class="m2"><p>آیینه دارد از رخت جوش چراغان در بغل!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد عاشق کی کند در گوش معشوقش اثر؟!</p></div>
<div class="m2"><p>بلبل ندارد در چمن جز آه و افغان در بغل!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دیده در بزم ادب مغرور آسایش مشو</p></div>
<div class="m2"><p>یک صبح وصلش را بود صد شام هجران در بغل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از یاد تیر غمزه‌اش ایمن نباشد سینه‌ام</p></div>
<div class="m2"><p>چشم خدنگ‌انداز او خوابیده پیکان در بغل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در چارسوی عشق او واکرده دکان جنون</p></div>
<div class="m2"><p>از بهر تعلیقم جنون در پای مردان در بغل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواندیم دوش اندر چمن از دفتر اوراق گل</p></div>
<div class="m2"><p>مشکل که آید دلبرت امروز آسان در بغل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بهر تعلیق جنون در پای مردم می‌فتد</p></div>
<div class="m2"><p>طفل سرشکم گوییا دارد دبستان در بغل!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چین جبین منعمان کی منع عبرانش کند</p></div>
<div class="m2"><p>چشم گدا بیند اگر دست کریمان در بغل؟!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داروی دیگر کی بود اندر مریض عشق او؟!</p></div>
<div class="m2"><p>بیمار چشمش را بود پیوسته درمان در بغل!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شمع از گداز عارضت در گریه از شب تا سحر</p></div>
<div class="m2"><p>چیدست دامن تا کمر گویا گریبان در بغل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دوش این غزل در گوش من می‌گفت دهقان سخن</p></div>
<div class="m2"><p>نظمی که سعدی گفته است دارد «گلستان » در بغل!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طغرل هزاران آفرین بر مصرع بحر سخن</p></div>
<div class="m2"><p>ای از خرامت نقش پا خورشید تابان در بغل!</p></div></div>