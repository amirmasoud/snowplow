---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>آفرین شست خدنگ چشم زهگیر تو را</p></div>
<div class="m2"><p>کز دو صد دل بگذراند ناوک تیر تو را!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بند سودای غم یوسف زلیخا کی شدی</p></div>
<div class="m2"><p>گر بدیدی پیچ و تاب زلف زنجیر تو را؟!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رمز سامان تغافل آنقدر فهمیده‌ام</p></div>
<div class="m2"><p>کرده استاد ازل از نام تخمیر تو را!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان کند بهزاد از راه تحیر تا ابد</p></div>
<div class="m2"><p>هرکجا بیند اگر او نقش تصویر تو را!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیر تیغ ابرویت تنها نه من جان می‌دهم</p></div>
<div class="m2"><p>کیست جان خود نسازد تحفه شمشیر تو را؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست ممکن وصف تو ای تحفه آیات حسن</p></div>
<div class="m2"><p>کس نمی‌داند چه خواهد کرد تفسیر تو را!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با قدت ای نخل گلزار بهار دلبری</p></div>
<div class="m2"><p>راست گو پرسم من از میخانه تا پیر تو را!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خانه دل را عمارت‌ها نمودی از غمت</p></div>
<div class="m2"><p>هیچ کس ویران نخواهد کرد تعمیر تو را!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لعل خوبان را شکر تلخی نماید با لبت</p></div>
<div class="m2"><p>مادر دهر از شکر جوشیده تا شیر تو را!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طغرل از نظمم سراپا بین که می‌ریزد شکر</p></div>
<div class="m2"><p>بس که بنمودم ز سر تا پای تحریر تو را!</p></div></div>