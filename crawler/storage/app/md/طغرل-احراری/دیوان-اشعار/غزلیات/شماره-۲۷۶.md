---
title: >-
    شمارهٔ ۲۷۶
---
# شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>شوخ شهرآشوب من گر بی‌حجاب آید برون</p></div>
<div class="m2"><p>با تماشای جمالش آفتاب آید برون!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از می وصلش رقیبان جمله یکسر کامیاب</p></div>
<div class="m2"><p>سوی عاشق آن جفاجو با عتاب آید برون!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چمن بی‌روی او با گل چسان سازم نگه؟!</p></div>
<div class="m2"><p>شبنم خجلت به رویم چون گلاب آید برون!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنقدر با یاد او از دیده باریدم گهر</p></div>
<div class="m2"><p>موج طوفان سرشکم را حباب آید برون!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوختم در مجمر عشق تو ای نازآفرین</p></div>
<div class="m2"><p>هر دمی از دل مرا بوی کباب آید برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌تو عمری همچو نی با ناله دارم الفتی</p></div>
<div class="m2"><p>برق آهم هر نفس همچون شهاب آید برون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیست طغرل امت پیغمبر خضر خطش؟!</p></div>
<div class="m2"><p>ای خوش آن پیغمبری کو با کتاب آید برون!</p></div></div>