---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>کس نباشد به سیر این گلزار</p></div>
<div class="m2"><p>تا کند امتیاز گل از خار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر حصول مزارع امید</p></div>
<div class="m2"><p>دانه‌های سرشک غم می‌کار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غره منشین که فتنه‌ها دارد</p></div>
<div class="m2"><p>گردش انقلاب لیل و نهار!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست در ساز عاشقان اثری</p></div>
<div class="m2"><p>نیست این زیر و بم به موسیقار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مثل دهر خواب خرگوش است</p></div>
<div class="m2"><p>گرچه باشد دو چشم او بیدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قوت جذب عشق می‌آرد</p></div>
<div class="m2"><p>سر منصور ما به پای دار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رتبه تخت جم شود حاصل</p></div>
<div class="m2"><p>عاشقان را ز سایه دیوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سر دل گذر چو من اکنون</p></div>
<div class="m2"><p>آرزویت بود اگر دلدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سحر زاهد نگر که می‌باشد</p></div>
<div class="m2"><p>شعبه‌ای از فسونش استغفار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس که در زلف شاهدان چمن</p></div>
<div class="m2"><p>زده مشاطه آب و رنگ بهار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو آیینه دشت و کوه و دره</p></div>
<div class="m2"><p>می‌نماید به چشم من هموار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بخت بد بر سرم نشسته چنان</p></div>
<div class="m2"><p>چغد ویرانه بر سر دیوار!</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>امتیازی نمی‌توان کردن</p></div>
<div class="m2"><p>حلقه زلف او ز حلقه مار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طغرلم محو مصرع بیدل</p></div>
<div class="m2"><p>کم ما‌ هم مدان کم از بسیار</p></div></div>