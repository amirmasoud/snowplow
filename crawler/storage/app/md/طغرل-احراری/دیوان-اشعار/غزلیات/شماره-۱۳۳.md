---
title: >-
    شمارهٔ ۱۳۳
---
# شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>ساغر عیش ابد گرچه به دستم دادند</p></div>
<div class="m2"><p>راه راحت همه از جام الستم دادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاروانان قضا و قدر از هشیاری</p></div>
<div class="m2"><p>چون حنا بسته مرا بر کف مستم دادند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکجا باشم اگر بی‌اثر داغ نیم</p></div>
<div class="m2"><p>شعله شوقم و بر خاک نشستم دادند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سفالی نرسد ساز درست‌آیینم</p></div>
<div class="m2"><p>چینی‌ام گرچه ز فغفور شکستم دادند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود تحقیق عدم صورت امکان وجود</p></div>
<div class="m2"><p>نیستی آیینه‌ای بود ز هستم دادند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صید مطلب نشود بسته قلاب هوس</p></div>
<div class="m2"><p>ماهی بحر ادب گر چه به شستم دادند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خانه‌بر‌دوش خیالم به سخن همچو حباب</p></div>
<div class="m2"><p>واژگونی همه از طالع پستم دادند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه خوش است مصرع دریای معانی طغرل</p></div>
<div class="m2"><p>وصل می‌خواستم آیینه به دستم دادند</p></div></div>