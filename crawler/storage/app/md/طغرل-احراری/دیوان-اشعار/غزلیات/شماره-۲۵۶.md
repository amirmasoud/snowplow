---
title: >-
    شمارهٔ ۲۵۶
---
# شمارهٔ ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>آبله شد سد ره منزلم</p></div>
<div class="m2"><p>دست کرم را عرق سائلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت دلم در پی عرض ادب</p></div>
<div class="m2"><p>بار امانت نکشد محملم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دور مبادا ز جبینم عرق</p></div>
<div class="m2"><p>داد به طوفان هوس ساحلم!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌روم از خویش به مشق تپش</p></div>
<div class="m2"><p>بسمل شوقم ادیب قاتلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سبز نشد مزرع آمال من</p></div>
<div class="m2"><p>نیست به جز یأس دگر حاصلم!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند روی از پی این داروگیر؟!</p></div>
<div class="m2"><p>گشت چو منصور حق از باطلم!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دور سخن آمده اکنون به من</p></div>
<div class="m2"><p>سلسله حلقه این محفلم!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سالک راهی نشود باورت</p></div>
<div class="m2"><p>هرچه زبان گفت ز حرف دلم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حرمت خونم که حلال تو باد</p></div>
<div class="m2"><p>ناز به تیغ تو کند بسملم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوش گذشتم سوی باغ سخن</p></div>
<div class="m2"><p>گل دمد امروز ز آب و گلم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طغرل ازین مصرع بیدل خوشم</p></div>
<div class="m2"><p>بی‌تو فتادست الم بر دلم!</p></div></div>