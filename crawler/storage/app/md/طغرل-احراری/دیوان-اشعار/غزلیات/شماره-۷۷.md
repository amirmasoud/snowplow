---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>شور دریای محبت موجی از آب من است</p></div>
<div class="m2"><p>برق شمشیر حوادث لمعه تاب من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرده‌دار ساز قانون بم و زیر غمم</p></div>
<div class="m2"><p>نغمه «عشاق » از آهنگ مضراب من است!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست جز تعبیر حیرانی مرا فال دگر</p></div>
<div class="m2"><p>فرش مخمل سرگذشت قصه خواب من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌زند مهر دلم گر بخیه در جیب سحر</p></div>
<div class="m2"><p>چاک دامان کتان لیکن ز مهتاب من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوانده‌ام درس ادب را پیش استاد جنون</p></div>
<div class="m2"><p>قد ماه نو دوتا از وضع آداب من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوش طوفان سرشکم دارد آهنگ دگر</p></div>
<div class="m2"><p>خاک صحرای جنون تمهید سیلاب من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طاقت یک عالم از ابروی یارم طاق شد</p></div>
<div class="m2"><p>سجده‌گاه صد جبین از شوق محراب من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای خوشا از مصرع بیدل که طغرل گفته است</p></div>
<div class="m2"><p>بزم گردون صبح خیز از لعل سیراب من است</p></div></div>