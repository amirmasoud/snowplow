---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>ز معمار خرابی بس که همچون گنج معمورم</p></div>
<div class="m2"><p>چو مرهم عاقبت گل می‌کند از زخم ناسورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز آهنگ محبت نیست در مضراب من دیگر</p></div>
<div class="m2"><p>همین «عشاق » می‌آید به گوش از ساز تنبورم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا اندیشه هجر و خیال وصل کی باشد؟!</p></div>
<div class="m2"><p>به ذوق کوی او نآید به خاطر جنت حورم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا هرچند صحرای جنون چون لاله مسکن شد</p></div>
<div class="m2"><p>ولی از الفت داغ غم او سخت مسرورم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکردم با ادیب عشق جز مشق جنون دیگر</p></div>
<div class="m2"><p>اگر آهی کشم در بزم او چون شمع معذورم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همین بس نشئه کیفیت خمیازه شوقش</p></div>
<div class="m2"><p>به ساغر الفتی دارد نگاه چشم مخمورم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازآن روزی که من در وادی هجرش وطن دارم</p></div>
<div class="m2"><p>نباشد صبح عشرت در پس این شام دیجورم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز سلطان غمش نبود به طغرایی مثال من</p></div>
<div class="m2"><p>به غیر از سایه بخت سیاه خویش منشورم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قلم از بهر تحریرت ز چوب دار می‌باید</p></div>
<div class="m2"><p>که تا بنویسی شرح قصه‌های خون منصورم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هزاران آفرین طغرل به عجز حضرت بیدل</p></div>
<div class="m2"><p>ز دشت بی‌خودی می‌آیم از وضع ادب دورم</p></div></div>