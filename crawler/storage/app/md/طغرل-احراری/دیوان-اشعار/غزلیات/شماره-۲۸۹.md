---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>یاد ایامی که سوی ما خرامی داشتی</p></div>
<div class="m2"><p>با کف از میخانه الطاف جامی داشتی!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهار عارضت نظاره‌ام می‌چید گل</p></div>
<div class="m2"><p>چون نگه در خانه چشمم مقامی داشتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انجمن‌آرای بزم ما حدیث وصل بود</p></div>
<div class="m2"><p>حیف گفتاری که زان شکر به کامی داشتی!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از طریق و شیوه عهد و وفا گشتی برون</p></div>
<div class="m2"><p>با مروت بین خوبان گر چه نامی داشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مژده و پیغام قاصد بود انفاس مسیح</p></div>
<div class="m2"><p>از لب جان‌بخش با ما گر پیامی داشتی!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاص بود مر عام را و عام بود مر خاص را</p></div>
<div class="m2"><p>از نوازش‌ها که لطف خاص و عامی داشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلبه ما طغرل از خورشید پرتو می‌زند</p></div>
<div class="m2"><p>سوی این کاشانه گویا سیر بامی داشتی!</p></div></div>