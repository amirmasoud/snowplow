---
title: >-
    شمارهٔ ۱۸۰
---
# شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>هرچه جز عشقش مرا عار است و بس</p></div>
<div class="m2"><p>مدعا از یاری‌ام یار است و بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست بار دیگری بر دوش من</p></div>
<div class="m2"><p>قامت خم زیر این بار است و بس!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی رسم در وصل او از بخت بد</p></div>
<div class="m2"><p>پیش رویم بس که دیوار است و بس؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنقدر در عشق او خون شد دلم</p></div>
<div class="m2"><p>اشک من چون دانه نار است و بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست کاری در جهان جز عاشقی</p></div>
<div class="m2"><p>کارهای دهر بیکار است و بس!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان شیرین می‌کند در بیستون</p></div>
<div class="m2"><p>قسمت فرهاد کوهسار است و بس!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش چشم عاشق مجنون ما</p></div>
<div class="m2"><p>کوه و صحرا جمله هموار است و بس!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست حق دعوای هر کس در جهان</p></div>
<div class="m2"><p>یک سر منصور بر دار است و بس!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌سبب کی می‌رود موسی به طور؟!</p></div>
<div class="m2"><p>مطلبش یک عرض دیدار است و بس!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حلقه‌های کفر زلفش دم به دم</p></div>
<div class="m2"><p>برهمن را تار زنار است و بس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت هر کس دید چشم و ابرویش</p></div>
<div class="m2"><p>در رواق کعبه معمار است و بس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مدعا حاصل نشد از باغ دهر</p></div>
<div class="m2"><p>جای گل در دشت من خار است و بس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طره‌اش هرگه که بر دوش افکند</p></div>
<div class="m2"><p>در نظر چون حلقه مار است و بس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حبذا طغرل که بیدل گفته است</p></div>
<div class="m2"><p>چشم وا کن شش جهت یار است و بس</p></div></div>