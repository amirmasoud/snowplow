---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>هرکه آمد به سوی خانه ما</p></div>
<div class="m2"><p>خواند شه‌بیت آستانه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساز عشاق ما نوای تو شد</p></div>
<div class="m2"><p>این بود تا ابد ترانه ما!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که مجنون وادی عشقیم</p></div>
<div class="m2"><p>گشت عالم پر از فسانه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز پریشانی کس نمی‌فهمد</p></div>
<div class="m2"><p>یک سر مو زبان شانه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پر عنقا نشان هستی ماست</p></div>
<div class="m2"><p>کی رسد تیر بر نشانه ما؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاهبازیم ما به صید سخن</p></div>
<div class="m2"><p>اوج معنی است آشیانه ما!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهد مضمون گرفت راه نفس</p></div>
<div class="m2"><p>تا که خاموشی بهانه ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از عدم آمدیم تا به وجود</p></div>
<div class="m2"><p>همچو ما نیست در زمانه ما!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بحر عشقیم و قلزم معنی</p></div>
<div class="m2"><p>نیست پیدا ولی کرانه ما!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس که دهقان مزرع عملیم</p></div>
<div class="m2"><p>دل دمد لیک جای دانه ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طغرلم محو مصرع بیدل</p></div>
<div class="m2"><p>جبهه سوز است آستانه ما!</p></div></div>