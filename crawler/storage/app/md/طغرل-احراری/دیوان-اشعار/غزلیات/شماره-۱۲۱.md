---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>از تبسم لعل او تا نشئه از مل می‌کشد</p></div>
<div class="m2"><p>چهره مینا عرق از شرم قل‌قل می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خدنگ ناز چشم شوخ او ایمن مباش</p></div>
<div class="m2"><p>نشتر مژگان او خون از رگ گل می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمرها بهر وصال اندر بیابان غمش</p></div>
<div class="m2"><p>کاروان شوق من بار توکل می‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از فریب دانه خال سیاهش کن حذر</p></div>
<div class="m2"><p>مرغ دل را دام زلفش بی‌تحمل می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساز صوت و نغمه‌ای دارم که هرجا مطربیست</p></div>
<div class="m2"><p>تار قانونم ز مد آه بلبل می‌کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نسخه آشفته جزو پریشان غمم</p></div>
<div class="m2"><p>مانی و بهزاد تصویرم ز سنبل می‌کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دماغ خشک بخت تیره واژون من</p></div>
<div class="m2"><p>روغن بادام را از نبض کاکل می‌کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نرگس سحرآفرینش بهر تعلیم فسون</p></div>
<div class="m2"><p>بی‌ابا هاروت را از چاه بابل می‌کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناخن برهان طغرل چون به دور زلف او</p></div>
<div class="m2"><p>خار تطبیق از کف پای تسلسل می‌کشد؟!</p></div></div>