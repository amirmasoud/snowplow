---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>ای بهار ناز بهر زینت گل‌ها بیا!</p></div>
<div class="m2"><p>سیر دریا آرزو داری به چشم ما بیا!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده‌ام دور از رخت تمهید سامان جنون</p></div>
<div class="m2"><p>ای سویدای دلم را دافع سودا بیا!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از وفا داریم هر دم آرزوی مقدمت</p></div>
<div class="m2"><p>هر بن مو چشم امید است سوی ما بیا!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی بود بیمی به ما زین سست‌مغزان دغل</p></div>
<div class="m2"><p>گر بیایی جانب ما سخت بی‌پروا بیا!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ز هجران تو آشفتست چون اوراق گل</p></div>
<div class="m2"><p>مرحبا ای غنچه جمعیت دل‌ها بیا!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست قانون محبت ساز نیرنگ دویی</p></div>
<div class="m2"><p>ای تن من خاک پایت آمدی تنها بیا!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای خوش آن مصرع که طغرل می‌سراید بیدلی</p></div>
<div class="m2"><p>یا مرا از خود ببر آن جا که هستی یا بیا!</p></div></div>