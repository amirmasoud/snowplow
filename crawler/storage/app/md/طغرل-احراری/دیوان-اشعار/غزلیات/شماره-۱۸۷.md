---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>ز خون دیده من نامه به یار نویس</p></div>
<div class="m2"><p>به جای مهر که آمد تو انتظار نویس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن ز زلف درازش اگر کنی کوته</p></div>
<div class="m2"><p>حدیث طره او را به پشت مار نویس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو من به دفتر حسنش تراست میل رقم</p></div>
<div class="m2"><p>رسیدی بر رخ او نوبت بهار نویس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر به وصف رخش رفت نوبت تحریر</p></div>
<div class="m2"><p>سواد نسخه او از خط غبار نویس!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فسانه‌های سرشکم به خط یاقوتی</p></div>
<div class="m2"><p>به صفحه‌ای که نویسی به آب نار نویس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قلم به حرف محبت ز آه بلبل کن</p></div>
<div class="m2"><p>یکی از حکایت عشاق از هزار نویس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر نویسی تو خطی به سوی یار از من</p></div>
<div class="m2"><p>همین قدر که ز عشق تو شرمسار نویس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو حق شناسی تاریخ رأیت منصور</p></div>
<div class="m2"><p>به هر کجا که نویسی به چوب دار نویس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوشا ز مصرع سلطان معرفت طغرل</p></div>
<div class="m2"><p>به جای هر الف انگشت زنهار نویس</p></div></div>