---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ز عارض برقع افکندی کشودی روی زیبا را</p></div>
<div class="m2"><p>ز برق رخ زدی آتش بساط خرمن ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سفید از انتظار مقدمت شد چشم امیدم</p></div>
<div class="m2"><p>خوشا روزی که همچون نور بر چشمم نهی پا را!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شدم چون لاله از داغ فراقت بارها اخگر</p></div>
<div class="m2"><p>نهادی بر دلم باری دگر داغ سویدا را!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگردد مانع جولان عاشق زیر و بم هرگز</p></div>
<div class="m2"><p>که فرقی نیست اندر ساز مجنون کوه و صحرا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر محفل که آن شوخ پری‌رو در سخن آید</p></div>
<div class="m2"><p>برد اعجاز لعلش قدر انفاس مسیحا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیال طره‌اش در گردن عاشق بود دامی</p></div>
<div class="m2"><p>رهایی نیست ممکن از کمند زلف او ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به روی شاهد گل غازه زد مشاطه قدرت</p></div>
<div class="m2"><p>به عبرت چشم بگشا گر هوس داری تماشا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشان هستی ما محض امکان است در عالم</p></div>
<div class="m2"><p>هوس داری ز ما می‌کن سراغ نام عنقا را!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>الهی تکیه‌گاهی نیست ما را جز خمار می</p></div>
<div class="m2"><p>عصای ما ضعیفان کن خیال قد مینا را!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نمودم آن قدر تمهید سامان سخن طغرل</p></div>
<div class="m2"><p>به گوش شاهد معنی کنم عقد ثریا را</p></div></div>