---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>حضور خویش خواهی جبر ما را</p></div>
<div class="m2"><p>تو مغروری به حسن ای بیمدارا!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سیخ ناز مژگانت کشیدی</p></div>
<div class="m2"><p>دل دلدادگان مبتلا را!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سراسر قتل اهل عشق کردی</p></div>
<div class="m2"><p>نما لطفی به محزونان خدا را!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنالم گر ز هجرت نرم گردد</p></div>
<div class="m2"><p>دل سخت هزاران سنگ خارا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا با ما نسازی یک تکلم؟!</p></div>
<div class="m2"><p>به معراجش رسانیدی جفا را!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر چندی نیایی جانب من</p></div>
<div class="m2"><p>ولی از دور می‌سازم دعا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نسیمی گر صبا آرد ز کویت</p></div>
<div class="m2"><p>بدو ارسال دارم مدعا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز هجرت طغرل زار بلاکش</p></div>
<div class="m2"><p>بمیرد چیست فرمان تو یارا؟!</p></div></div>