---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>نباشد التفاتی با من اکنون تیغ قاتل را</p></div>
<div class="m2"><p>که سازد ناخدا در موج خونم مطلب دل را!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یاد حسرت لعلش من از خود می‌روم هردم</p></div>
<div class="m2"><p>که می‌بندد به دوش ناله من بار محمل را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیم گر محرم وصلش به هجرش شکر می‌سازم</p></div>
<div class="m2"><p>به جای شهد نوشم در غمش زهر هلاهل را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر تسلیم را چون شمع وقف خنجر او کن</p></div>
<div class="m2"><p>ز برق آتش تیغش چراغ افروز بسمل را!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یک آیینه عرض مطلب خود می‌توان کردن</p></div>
<div class="m2"><p>که جز حیرت نمی‌باشد دو مرآت مقابل را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برای احتیاج از خجلت عرض نیاز خود</p></div>
<div class="m2"><p>عرق از جبهه طوفان می‌کند هر لحظه سائل را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نباشد زاهدان را جز غرور جاه اندر دل</p></div>
<div class="m2"><p>ز مشق حق‌پرستی کس نفهمد قبح باطل را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر خواهی که ره یابی تو در سرمنزل زلفش</p></div>
<div class="m2"><p>مقابل ساز در پیشانه خود کوکب دل را!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پسند خاطرش اشعار طغرل شد عجب نبود</p></div>
<div class="m2"><p>پسندد مصطفی مداحی سحبان وائل را!</p></div></div>