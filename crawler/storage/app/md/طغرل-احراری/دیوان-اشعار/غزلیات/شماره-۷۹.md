---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>تیری که از کمان نگاه تو جسته است</p></div>
<div class="m2"><p>چون ناوک فراق دل ما شکسته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صیاد تیرافکن و آهوست چشم تو</p></div>
<div class="m2"><p>این طرفه آهویی که با مردم نشسته است!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس نیست در جهان که نباشد اسیر تو</p></div>
<div class="m2"><p>یک دل ز بند حلقه زلفت نرسته است!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کرده‌ایم یاد کمان دو ابرویت</p></div>
<div class="m2"><p>پیکان غم به سینه ما دسته‌دسته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خالی است بر لب تو او یا زاغ در چمن</p></div>
<div class="m2"><p>یا هندویی که بر لب کوثر نشسته است؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در محفلی که ساز بم و زیر وصل توست</p></div>
<div class="m2"><p>صوت «فراق » ناخن مطرب شکسته است!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مد نگه به سایه مژگان انتظار</p></div>
<div class="m2"><p>چون عنکبوت خانه‌ام از موی بسته است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نبود سواد خط عذارش برات ما</p></div>
<div class="m2"><p>دود و غبار آتشین دل‌های خسته است!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طغرل غلام مصرع زیبای بیدلم</p></div>
<div class="m2"><p>آسودگی ز کشور ما بار بسته است</p></div></div>