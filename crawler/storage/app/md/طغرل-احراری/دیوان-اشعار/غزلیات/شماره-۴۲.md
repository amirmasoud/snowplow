---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>ز خود بسیار دور افتادم از معنی قرینی‌ها</p></div>
<div class="m2"><p>مرا سرمشق حیرانیست اکنون موی چینی‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توان در مزرع باغ جهان گل از ادب چیدن</p></div>
<div class="m2"><p>که صد خرمن نمایی حاصل ازین خوشه‌چینی‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرادی کی بود غیر از تسلی‌بخش آرامش</p></div>
<div class="m2"><p>نباشد بهره لیلی را ازین محمل‌نشینی‌ها!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شرم آن که فردا محرم مهر بتان باشی</p></div>
<div class="m2"><p>به شبنم غوطه زن امروز از خجلت جبینی‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلند افتاده از نااعتباری اعتبار من</p></div>
<div class="m2"><p>که از نامم عرق گل می‌کند از بی‌نگینی‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دمی ز اندیشه دل در پی صید معانی شو</p></div>
<div class="m2"><p>اگر باشد رسا فکر تو در معنی کمینی‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کنون جنس قماش خویش را طغرل به عرض آرم</p></div>
<div class="m2"><p>مرادی کی شود حاصل ازین خلوت‌گزینی‌ها؟!</p></div></div>