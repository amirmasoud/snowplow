---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>ماه من هرگه کشاید طره لبلاب را</p></div>
<div class="m2"><p>می‌برد از جعد هر زن گوش آب و تاب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افکند از رتبه از رخ افکند جلباب را</p></div>
<div class="m2"><p>درد انوار جمالش صافی مهتاب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مصحف رویش که باشد از خط ریحان صنع</p></div>
<div class="m2"><p>کاتب قدرت کجا ماند غلط اعراب را؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در غمش کلک دبیر قلب من خط می‌زند</p></div>
<div class="m2"><p>از تپیدن اضطراب نسخه سیماب را!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌دواند ز اشک خونین هر زمان رود دلم</p></div>
<div class="m2"><p>بی‌ابا بر روی من این طفل بی‌آداب را!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چمن از طرز رفتارش چه می‌پرسی ز من</p></div>
<div class="m2"><p>کز خرامش پای در زنجیر باشد آب را!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرق بسیار است بین ما و زاهد در سجود</p></div>
<div class="m2"><p>کی برابر می‌کنم با ابرویش محراب را؟!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کوه کن می‌کن به ناخن کوه تن را از غمش</p></div>
<div class="m2"><p>نغمه دیگر بود آهنگ این مضراب را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از هجوم اشک خود هر لحظه می‌ترسم کز آن</p></div>
<div class="m2"><p>خاک این صحرا به سنگ آرد سر سیلاب را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روز و شب طغرل نوید انتظار مقدمش</p></div>
<div class="m2"><p>می‌برد از چشم حیرانم چو مخمل خواب را</p></div></div>