---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>ماه من امروز در مصر ملاحت شاه شد</p></div>
<div class="m2"><p>یوسف از شرم جمال او به زیر چاه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام می بی‌یاد لعل او نصیب من مباد</p></div>
<div class="m2"><p>همدم بزمم اگر چندی که مهر و ماه شد!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تار گیسوی کمندش گشت دام مرغ دل</p></div>
<div class="m2"><p>دانه خال سیاهش تا مرا دلخواه شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی امان یابد ز تیر ناوک دلسوز او</p></div>
<div class="m2"><p>سینه آن کس که محنت‌سنج این درگاه شد؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از هلال ابرویش شرمنده گردد ماه نو</p></div>
<div class="m2"><p>وز صفای عارضش خورشید رنگ کاه شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باج خوبی را ز خوبان جهان گیرد رواست</p></div>
<div class="m2"><p>بس که نام او به خوبی در همه افواه شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ازل کلک قضا تصویر رویش می‌کشید</p></div>
<div class="m2"><p>بلبل روح من از بوی گلش آگاه شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این معما حل کند هر کس بدو طغرل غلام</p></div>
<div class="m2"><p>در بیابان توشح مطلبم همراه شد</p></div></div>