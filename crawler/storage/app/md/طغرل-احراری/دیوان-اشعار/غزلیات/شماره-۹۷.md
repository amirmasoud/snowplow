---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>پریشان جعد سنبل از سر زلف سمن‌سایت</p></div>
<div class="m2"><p>گشاده غنچه از خندیدن لعل شکرخایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود نظاره چون آیینه لذتگیر دیدارت</p></div>
<div class="m2"><p>سراپا دیده نرگس بود محو تماشایت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهارستان گلزار جمالت عالمی دارد</p></div>
<div class="m2"><p>سری کو تا بود خالی ز سودای تمنایت؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی دور از برم، جان از تنم آید برون آن دم</p></div>
<div class="m2"><p>بیا تا جان دمد در تن ز طرز آمدن‌هایت!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به راهت ز انتظاری دیده ما شد سفید اینجا</p></div>
<div class="m2"><p>ببخشا توتیای دیده از خاک کف پایت!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز حسرت مردم ای بدخو نشینی چند با تنها</p></div>
<div class="m2"><p>بود آیا که من بینم تو را خالی ز تنهایت؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دل مهر تو دارد طغرل از اغیار پنهانی</p></div>
<div class="m2"><p>بیا سویم الف‌آسا میان جان دهم جایت!</p></div></div>