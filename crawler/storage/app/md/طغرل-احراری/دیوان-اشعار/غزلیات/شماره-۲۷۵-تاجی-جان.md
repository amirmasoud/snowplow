---
title: >-
    شمارهٔ ۲۷۵ - تاجی‌جان
---
# شمارهٔ ۲۷۵ - تاجی‌جان

<div class="b" id="bn1"><div class="m1"><p>تاب رخت ندارد خورشید طاق گردون</p></div>
<div class="m2"><p>بشکسته قدر یاقوت از این دو لعل میگون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز چون تو شاهی نبود به ملک خوبی</p></div>
<div class="m2"><p>یکسر سپاه غمزه همراه چشم مفتون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جاری شود ز چشمم گریم اگر ز هجرت</p></div>
<div class="m2"><p>مردم به آب ماند نهری شود چو جیحون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یارب ز ظلم کوشی دائم چرا خموشی</p></div>
<div class="m2"><p>رخ را همیشه پوشی از عاشقان محزون؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام شراب تا کی با غیر ما بنوشی؟!</p></div>
<div class="m2"><p>ای لیلی زمانه رحمی به حال مجنون!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر حریم وصلت محرم همیشه اغیار</p></div>
<div class="m2"><p>با ما که مردم از غم لطفی به حق بیچون!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نامت به هر سر بیت بنهاده همچو تاجی</p></div>
<div class="m2"><p>جان شد برون که طغرل تا وا کشید مضمون</p></div></div>