---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>یک دل آسوده در این باغ نیست</p></div>
<div class="m2"><p>لاله‌ای نبود که او را داغ نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قتل تیهو بس که کار طغرل است</p></div>
<div class="m2"><p>صلصل و دراج صید زاغ نیست</p></div></div>