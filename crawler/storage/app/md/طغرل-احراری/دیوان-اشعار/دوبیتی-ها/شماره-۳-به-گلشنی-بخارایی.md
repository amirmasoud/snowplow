---
title: >-
    شمارهٔ ۳ - به گلشنی بخارایی
---
# شمارهٔ ۳ - به گلشنی بخارایی

<div class="b" id="bn1"><div class="m1"><p>مدتی شد که یار نامه نکرد</p></div>
<div class="m2"><p>رقم عنبرین شمامه نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا مگر کاغذ و مداد نداشت</p></div>
<div class="m2"><p>یا مرا لایق به خامه نکرد؟!</p></div></div>