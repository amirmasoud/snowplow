---
title: >-
    شمارهٔ ۸ - بر غزل شمس الدین شاهین
---
# شمارهٔ ۸ - بر غزل شمس الدین شاهین

<div class="b" id="bn1"><div class="m1"><p>جعد طره‌اش دیدم خاطرم پریشان شد</p></div>
<div class="m2"><p>یاد عارضش کردم کلبه‌ام گلستان شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درس محنتش خواندم مشکلاتم آسان شد</p></div>
<div class="m2"><p>محو جلوه‌اش گشتم تا رخش نمایان شد</p></div></div>
<div class="b2" id="bn3"><p>رفتم آنقدر از خود کآیینه به سامان شد!</p></div>
<div class="b" id="bn4"><div class="m1"><p>ناقه سرشک من بسته در رهش محمل</p></div>
<div class="m2"><p>از سپهر چشمانم دم به دم شود نازل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌ابا به پای او می‌فتد ازین غافل</p></div>
<div class="m2"><p>گریه‌ای نمی‌سازد منع انبساط دل</p></div></div>
<div class="b2" id="bn6"><p>غنچه را کجا شبنم تگمه گریبان شد؟!</p></div>
<div class="b" id="bn7"><div class="m1"><p>بر سرم ز هجرانش شورش قیامت رفت</p></div>
<div class="m2"><p>از هوای عشق او بی‌حدم ملامت رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حاصل امید من جمله در غرامت رفت</p></div>
<div class="m2"><p>اندک‌اندک این امید در پی ندامت رفت</p></div></div>
<div class="b2" id="bn9"><p>پشت دست ما کم‌کم نذر زخم دندان شد</p></div>
<div class="b" id="bn10"><div class="m1"><p>گیرودار صد موسی از تجلی طور است</p></div>
<div class="m2"><p>شوکت سلیمانی در ترحم مور است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صبح مطلب عاشق بعد شام دیجور است</p></div>
<div class="m2"><p>ریشه تا دواند تاک جلوه‌گاه انگور است</p></div></div>
<div class="b2" id="bn12"><p>کوششی به عرض آمد آبله نمایان شد</p></div>
<div class="b" id="bn13"><div class="m1"><p>کشتی امیدم را ز اشک ناخدا کردم</p></div>
<div class="m2"><p>در محیط اندوهش بی‌ابا رها کردم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با خیال سودایش عمر خود ادا کردم</p></div>
<div class="m2"><p>در هوای وصل او دوش گریه‌ها کردم</p></div></div>
<div class="b2" id="bn15"><p>قطره‌قطره اشک من دانه‌دانه مرجان شد</p></div>
<div class="b" id="bn16"><div class="m1"><p>لشکر الم‌هایش کرده در دلم منزل</p></div>
<div class="m2"><p>از نتیجه عشقش جز ستم نشد حاصل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مردم از غم هجرش تا شدم به او واصل</p></div>
<div class="m2"><p>بی‌تردد اسباب حل نمی‌شود مشکل</p></div></div>
<div class="b2" id="bn18"><p>کز کشاکش ناخن کار عقده آسان شد</p></div>
<div class="b" id="bn19"><div class="m1"><p>بوعلی بود پیشم در سلوک مجنونی</p></div>
<div class="m2"><p>دانشم کند اکنون از ارسطو افزونی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سینه بلیناسش طغرلم کند خونی</p></div>
<div class="m2"><p>گشت حاصل شاهین شوکت فلاطونی</p></div></div>
<div class="b2" id="bn21"><p>بعد ازین به ختلان هم گیرودار یونان شد!</p></div>