---
title: >-
    فی تحمید الحمید المجید عز شانه
---
# فی تحمید الحمید المجید عز شانه

<div class="b" id="bn1"><div class="m1"><p>بنده ام از جان خدایی را که او جان آفرید</p></div>
<div class="m2"><p>مهر و ماه و انجم و گردون گردان آفرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست صنع لایزالش، روز قدرت در ازل</p></div>
<div class="m2"><p>پاره ی یاقوت در فیروزه ایوان آفرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زابر فیض، اندر بن دریای حکمت، در صدف</p></div>
<div class="m2"><p>لؤلؤ شهوار از یک قطره باران آفرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از برای آنکه بنشانند بر تخت زرش</p></div>
<div class="m2"><p>پرتوی از مهر در لعل بدخشان آفرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست صنعش آتش خورشید را در کان فکند</p></div>
<div class="m2"><p>وین همه یاقوت و لعل اندر دل کان آفرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا همیشه دامن من معدن جوهر بود</p></div>
<div class="m2"><p>در کنار جزع من یاقوت و مرجان آفرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندرین درج زمرد، در شب تقدیر خویش</p></div>
<div class="m2"><p>صد هزاران لؤلؤ شهوار رخشان آفرید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گه بهار شادمانی، گه خزان بی غمی</p></div>
<div class="m2"><p>گاه تابستان خرم، گه زمستان آفرید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رعد، غران کرد و جسم باد بی آرام ساخت</p></div>
<div class="m2"><p>برق، خندان کرد و چشم ابر گریان آفرید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا به قدرت کرد گریان دیده ی ابر بهار</p></div>
<div class="m2"><p>در میان خارها گلبرگ خندان آفرید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نار چون پستان خوبان در چمن پربار کرد</p></div>
<div class="m2"><p>به به رنگ عاشقان در طرف بستان آفرید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در لب جانان حیات جاودانی جمع کرد</p></div>
<div class="m2"><p>بر رخ خوبان سر زلف پریشان آفرید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گل رخ و نسرین بر و مشکین خط و مه وش نگاشت</p></div>
<div class="m2"><p>دلبر سنگین دل و سیمین زنخدان آفرید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر صنع لایزال از بهر خلط عاشقان</p></div>
<div class="m2"><p>در خط و خال و لب و رخسار جانان آفرید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عاشق ذات جلال خویش در صحرای عشق</p></div>
<div class="m2"><p>از شراب لایزالی مست و حیران آفرید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ماه چابک سیر در یک جا نمی گردد مقیم</p></div>
<div class="m2"><p>ماه را سرگشته در گردون گردان آفرید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا حساب ملک بنویسد به کلک معرفت</p></div>
<div class="m2"><p>تیر پیر چرخ را منشی دیوان آفرید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا نوازد چنگ و گوید قول و نوشد جام می</p></div>
<div class="m2"><p>در فلک گوینده ای خوب و غزل خوان آفرید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در جهان آفرینش بر سر تخت مراد</p></div>
<div class="m2"><p>پادشاه چرخ چارم را زرافشان آفرید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا سراندازی کند از سرکشی در شرق و غرب</p></div>
<div class="m2"><p>ترک خون ریز فلک با تیغ بران آفرید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا کند حل نکته های شرع در دارالقضا</p></div>
<div class="m2"><p>قاضی گردون گردان را سخندان آفرید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیر پر دستان که بر هفتم فلک فرمان ده است</p></div>
<div class="m2"><p>بر سر شش طاق گردونش نگهبان آفرید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حکمتش را بین که در دارالشفای معرفت</p></div>
<div class="m2"><p>درد دل را از وصال خویش درمان آفرید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خاک و باد و آب و آتش، مهر و ماه و روز و شب</p></div>
<div class="m2"><p>جسم و جان و عقل و دانش، جمله یزدان آفرید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آفریده ست او ترا از نور خویش اندر ازل</p></div>
<div class="m2"><p>گرچه پنداری ترا از چار ارکان آفرید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این همه قدرت که در دنیا و عقبا جمع کرد</p></div>
<div class="m2"><p>صنع بیچونش همه از بهر انسان آفرید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گاه بر فوق ثریا، گاه در تحت الثری</p></div>
<div class="m2"><p>در جهان معرفت انسان بدین سان آفرید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از برای کژنشینان دوزخ تابنده ساخت</p></div>
<div class="m2"><p>وز برای راست بینان باغ رضوان آفرید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از برای کافران، دنیا چو جنت راست کرد</p></div>
<div class="m2"><p>وز برای مؤمنان، دنیا چو زندان آفرید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در جهان آدم ز محرومی ملامتها کشید</p></div>
<div class="m2"><p>گویی آدم در جهان از بهر حرمان آفرید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از برای آنکه تا یونس رود در بطن حوت</p></div>
<div class="m2"><p>ماهیئی را در بن دریای عمان آفرید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در ازل، رزاق روزی ده، ز خوان فیض خویش</p></div>
<div class="m2"><p>جسم ایوب از برای قوت کرمان آفرید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون نمود ادریس، در باغ بهشت از امر خود</p></div>
<div class="m2"><p>از برای حله دوزی در تنش جان آفرید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گاه ابراهیم را در آتش سوزان فکند</p></div>
<div class="m2"><p>گاه اسماعیل را از بهر قربان آفرید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گاه یوسف را عزیز مصر کرد از بعد حزن</p></div>
<div class="m2"><p>گاه یعقوب از برای بیت الاحزان آفرید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سجده کن پیش حکیمی را که امر حکمتش</p></div>
<div class="m2"><p>وصل یوسف را دوای پیر کنعان آفرید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا به زیر پا درآرد شرق و غرب از دستبرد</p></div>
<div class="m2"><p>بر سر باد صبا تخت سلیمان آفرید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا حیات جاودان یابند در ملک وجود</p></div>
<div class="m2"><p>از برای خضر و الیاس آب حیوان آفرید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کوری نمرود کابراهیم در آتش افکند</p></div>
<div class="m2"><p>قدرتش از آتش سوزان گلستان آفرید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا در آب رود نیل نیستی غرقش کند</p></div>
<div class="m2"><p>دفع فرعون لعین، موسی عمران آفرید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از برای آنکه سحر ساحران باطل کند</p></div>
<div class="m2"><p>قدرت بیچون او از چوب ثعبان آفرید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>عیسی اندر خاک راه آمد ز مادر در وجود</p></div>
<div class="m2"><p>جایگاهش بر سپهر چارم از آن آفرید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از برای آنکه تاج مهر بر فرقش نهد</p></div>
<div class="m2"><p>تخت گاهش در بر خورشید تابان آفرید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چون زکریا شد ز بیم خصم پنهان در درخت</p></div>
<div class="m2"><p>اره بر فرق سرش، دارای دوران آفرید</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا کند اظهار معجز، صالح خلوت نشین</p></div>
<div class="m2"><p>ناقه را از سنگ خارا سهل و آسان آفرید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>صد هزار و بیست و چار آمد پیمبر در وجود</p></div>
<div class="m2"><p>مصطفای مجتبا بر جمله سلطان آفرید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>این همه پیغمبران اندر رکاب او روند</p></div>
<div class="m2"><p>از برای آنکه او سالار ایشان آفرید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>عارضش زیباتر از برگ گل و نسرین نگاشت</p></div>
<div class="m2"><p>قامتش رعناتر از سرو خرامان آفرید</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از برای آنکه تا نعت نبی گوید ز جان</p></div>
<div class="m2"><p>طبع حیدر در سخنگویی چو حسان آفرید</p></div></div>