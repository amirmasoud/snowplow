---
title: >-
    فی نعت النبی صلی الله علیه و سلم
---
# فی نعت النبی صلی الله علیه و سلم

<div class="b" id="bn1"><div class="m1"><p>شمس و قمر ز پرتو رای محمدست</p></div>
<div class="m2"><p>دنیا و آخرت ز برای محمدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شهر اندرون که درو جز خدای نیست</p></div>
<div class="m2"><p>دل خانه ی حق است که جای محمدست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید آسمان که جهان غرق نور اوست</p></div>
<div class="m2"><p>یک ذره روشنی ز صفای محمدست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا دیده ام لقای محمد بسان نور</p></div>
<div class="m2"><p>در دیده ام همیشه لقای محمدست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در درج سینه گوهر ایمان نهاده ام</p></div>
<div class="m2"><p>وین نیز هم ز بحر عطای محمدست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر روز آفتاب که عام است فیض او</p></div>
<div class="m2"><p>سرگشته در فلک به هوای محمدست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اول به راه شرع قدم نه که روز حشر</p></div>
<div class="m2"><p>باشد رضای حق چو رضای محمدست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در حشر با سلوک رسولان برابرست</p></div>
<div class="m2"><p>اعمال امتی که سزای محمدست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حیدر که در دهن شکر نعت می‌نهد</p></div>
<div class="m2"><p>چون طوطیی ز بهر ثنای محمدست</p></div></div>