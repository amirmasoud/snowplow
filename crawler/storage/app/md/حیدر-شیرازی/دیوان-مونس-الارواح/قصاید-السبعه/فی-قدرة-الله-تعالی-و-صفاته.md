---
title: >-
    فی قدرة الله تعالی و صفاته
---
# فی قدرة الله تعالی و صفاته

<div class="b" id="bn1"><div class="m1"><p>قادری کز قدرت این عالم پدیدار آورد</p></div>
<div class="m2"><p>خلق عالم را به عشق خویش در کار آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نافه از آهو و نور از نار و لعل از خاره سنگ</p></div>
<div class="m2"><p>گوهر از بحر و زر از کان و گل از خار آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حکم او از رعد غران کوس سلطانی زند</p></div>
<div class="m2"><p>امر او از ابر گریان در شهوار آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خار تیز از قدرت او بوستان پر گل کند</p></div>
<div class="m2"><p>چوب خشک از حکمت او میوه ها بار آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوبهار صنع بفرستد، بروید گل ز خار</p></div>
<div class="m2"><p>بلبلان را مست و لایعقل به گلزار آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فصل نیسان چون بگرید ابر و خندد روی دشت</p></div>
<div class="m2"><p>لاله ها در باغ از قدرت نمودار آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ضیمران تار و بنفشه کوژ و لاله شرمسار</p></div>
<div class="m2"><p>ارغوان خونین و گل شوخ و سمن زار آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیشتر از آنکه در پوشد گل رعنا قبا</p></div>
<div class="m2"><p>نرگس سرمست در بستان کله دار آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فصل تابستان به قدرت در میان بوستان</p></div>
<div class="m2"><p>صد هزاران صنع گوناگون ز اشجار آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میوه های خوب شفتالود و امرود و ترنج</p></div>
<div class="m2"><p>سیب و زردآلود و نارنج و به و نار آورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از هوای شام، نفح عنبر سارا دهد</p></div>
<div class="m2"><p>وز نسیم صبح، بوی مشک تاتار آورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پادشاه پادشاهان روز محشر در بهشت</p></div>
<div class="m2"><p>عاشقان مست را با خویش در کار آورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در بر ایشان به دست قدرت خود از کرم</p></div>
<div class="m2"><p>دم به دم از حوض کوثر جام اسرار آورد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گه به قدرت مهر رخشان را به شب پنهان کند</p></div>
<div class="m2"><p>گه به حکمت روز روشن از شب تار آورد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در سحر چون وانشاند شمع ماه از باد صبح</p></div>
<div class="m2"><p>مشعل خورشید در عالم پدیدار آورد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا درین مرکز فراوان نقش قدرت برکشد</p></div>
<div class="m2"><p>مهر همچون نقطه و گردون چو پرگار آورد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر شب اندر مجلس گردون ز بهر روشنی</p></div>
<div class="m2"><p>از مه و انجم چراغ و شمع بسیار آورد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مهر و ماه و مشتری و زهره و بهرام و تیر</p></div>
<div class="m2"><p>با زحل هر هفت در گردون به رفتار آورد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هست اقرارم که گبر و کافرست و دوزخی</p></div>
<div class="m2"><p>هر که او در قدرتش یک ذره انکار آورد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شکر از نی، گندم از گل، میوه‌ها از چوب خشک</p></div>
<div class="m2"><p>از برای بندهٔ بیچارهٔ زار آورد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وآنگهی بر دست قدرت از میان باغ صنع</p></div>
<div class="m2"><p>میوهٔ الوان گوناگون به بازار آورد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بنده‌ام از جان خدایی را که بهر بندگان</p></div>
<div class="m2"><p>این همه قدرت درین اطوار و ادوار آورد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گاه یوسف را به ده درهم فروشد در جهان</p></div>
<div class="m2"><p>گاه در مصرش جهانی را خریدار آورد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>امر او هنگام قدرت در جهان معجزات</p></div>
<div class="m2"><p>عیسی یک روزه را ناگه به گفتار آورد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خود کند تقدیر و منصور از انا الحق دم زند</p></div>
<div class="m2"><p>و آنگهش از غیرت خود بر سر دار آورد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گاه زهر جان ستان از زخم مار آرد پدید</p></div>
<div class="m2"><p>گاه دفع رنج آن از مهره ی مار آورد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گرچه نمرود دنس لاف الوهیت زند</p></div>
<div class="m2"><p>پشه ای بفرستد و او را نگونسار آورد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ورچه فرعون لعین آن دعوی باطل کند</p></div>
<div class="m2"><p>همچو خس در رود نیل او را گرفتار آورد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر به دست امر بردارد حجاب از روی کار</p></div>
<div class="m2"><p>از هدایت کافر صد ساله اقرار آورد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حیدر توحید گوی از غایت صدق و صفا</p></div>
<div class="m2"><p>روی دل پیوسته بر درگاه دادار آورد</p></div></div>