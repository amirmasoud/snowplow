---
title: >-
    شمارهٔ ۷۴ - و له
---
# شمارهٔ ۷۴ - و له

<div class="b" id="bn1"><div class="m1"><p>اگر آن یار یار من باشد</p></div>
<div class="m2"><p>مونس روزگار من باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیم باشد که از میان بروم</p></div>
<div class="m2"><p>اگر او در کنار من باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اختیار جهانیان است او</p></div>
<div class="m2"><p>لاجرم اختیار من باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی محبوب و زلف مشک افشان</p></div>
<div class="m2"><p>سنبل و نوبهار من باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه بود عمر و زندگانی من</p></div>
<div class="m2"><p>گه خداوندگار من باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به کارم نظر کند دلبر</p></div>
<div class="m2"><p>در جهان کار کار من باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرده بر خویش پاره پاره کنم</p></div>
<div class="m2"><p>گر نه او پرده دار من باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حیدر این شعر تر که می‌گوید</p></div>
<div class="m2"><p>سخنش یادگار من باشد</p></div></div>