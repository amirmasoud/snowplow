---
title: >-
    شمارهٔ ۵۸ - و له ایضا
---
# شمارهٔ ۵۸ - و له ایضا

<div class="b" id="bn1"><div class="m1"><p>خواهم که حاجت من بیدل روا کنی</p></div>
<div class="m2"><p>خواهم که با وصال خودم آشنا کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فخر پای بر سر هفت آسمان نهم</p></div>
<div class="m2"><p>روزی اگر نظر به من بینوا کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کی کمان چاچی ابرو کشی به من</p></div>
<div class="m2"><p>تا کی به تیر غمزه مرا مبتلا کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چین زلف خویش مرا ره نمی دهی</p></div>
<div class="m2"><p>اصل تو از خطاست، از آن رو خطا کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ترک تنگ چشم جفاکار جنگجو!‏</p></div>
<div class="m2"><p>با عاشقان خویش چرا ماجرا کنی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در صفه ی صفا به تو دارم توقعی</p></div>
<div class="m2"><p>کز روی لطف با من مسکین صفا کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و آن گه شوی طبیب من زار ناتوان</p></div>
<div class="m2"><p>وز لعل خویش درد دلم را دوا کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حیدر اگر دعاش کنی منتی منه</p></div>
<div class="m2"><p>داعی دولتی، چه شود گر دعا کنی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور خلق روزگار زنندت به تیغ تیز</p></div>
<div class="m2"><p>شاید اگر حوالت آن با خدا کنی</p></div></div>