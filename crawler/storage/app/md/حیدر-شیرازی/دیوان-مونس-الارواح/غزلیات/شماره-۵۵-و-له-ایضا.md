---
title: >-
    شمارهٔ ۵۵ - و له ایضا
---
# شمارهٔ ۵۵ - و له ایضا

<div class="b" id="bn1"><div class="m1"><p>به حرف من چو نهاد این زمان نگار انگشت</p></div>
<div class="m2"><p>به اشک دیدهٔ خونین کنم نگار انگشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگفتم از لب شیرین مرا بده کامی</p></div>
<div class="m2"><p>نهاد بر لب شیرین آبدار انگشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محقق از خط ریحان یار رشک برد</p></div>
<div class="m2"><p>در آن زمان که نویسد خط غبار انگشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن زمان که تو از دست من به در رفتی</p></div>
<div class="m2"><p>همی‌گزیم به دندان هزار بار انگشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چنان که تو با ما بدین نسق باشی</p></div>
<div class="m2"><p>برآوریم ز دستت به زینهار انگشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو گویم اشهد ان لا اله الا الله</p></div>
<div class="m2"><p>برآوریم به پیش تو آشکار انگشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز حیدر- ای صنم!- ار خرده‌ای پدید آمد</p></div>
<div class="m2"><p>به حرف او منه- ای سرو گل عذار!- انگشت</p></div></div>