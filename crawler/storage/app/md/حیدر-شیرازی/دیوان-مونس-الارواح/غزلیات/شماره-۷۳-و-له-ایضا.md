---
title: >-
    شمارهٔ ۷۳ - و له ایضا
---
# شمارهٔ ۷۳ - و له ایضا

<div class="b" id="bn1"><div class="m1"><p>من نکنم توبه ز معشوق و می</p></div>
<div class="m2"><p>توبه ز معشوقه و می تا به کی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رمضان گر بجهم روز عید</p></div>
<div class="m2"><p>روزه ی سی روزه گشایم به می</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که میسر شودش وصل یار</p></div>
<div class="m2"><p>به که میسر شودش ملک کی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیدر دل خسته بود بی نوا</p></div>
<div class="m2"><p>در رمضان همچو دف و چنگ و نی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عید کند بلبل طبعش نوا</p></div>
<div class="m2"><p>از هوس روی چو گلزار وی</p></div></div>