---
title: >-
    شمارهٔ ۲۸ - و له ایضا
---
# شمارهٔ ۲۸ - و له ایضا

<div class="b" id="bn1"><div class="m1"><p>تا کی من دیوانه گرفتار تو باشم</p></div>
<div class="m2"><p>در بند سر زلف سیه کار تو باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آرزوی قامت رعنای تو میرم</p></div>
<div class="m2"><p>در حسرت یاقوت شکربار تو باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان خسته ی آن غمزه ی غماز تو بینم</p></div>
<div class="m2"><p>دل بسته ی آن طره ی طرار تو باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی من دلداده ز هجران تو سوزم</p></div>
<div class="m2"><p>تا کی من سرگشته طلبکار تو باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای راحت جان من دل خسته، زمانی</p></div>
<div class="m2"><p>خواهم که تو یار من و من یار تو باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در باغ جهان سنبل گلبوی تو بویم</p></div>
<div class="m2"><p>در طرف چمن بلبل گلزار تو باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چند من زار جگرخوار، چو حیدر</p></div>
<div class="m2"><p>مجروح و پریشان و دل افگار تو باشم؟</p></div></div>