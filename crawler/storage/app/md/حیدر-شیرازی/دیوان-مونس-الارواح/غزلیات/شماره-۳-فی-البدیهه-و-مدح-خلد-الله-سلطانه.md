---
title: >-
    شمارهٔ ۳ - فی البدیهه و مدح خلد الله سلطانه
---
# شمارهٔ ۳ - فی البدیهه و مدح خلد الله سلطانه

<div class="b" id="bn1"><div class="m1"><p>علی الصباح که سلطان طارم ازرق</p></div>
<div class="m2"><p>فراز گنبد فیروزه گون بزد سنجق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بیم خنجر خورشید، لشکر انجم</p></div>
<div class="m2"><p>ز روی چرخ گریزان شدند چون زیبق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خواب صبح چون ملاح روز سر برداشت</p></div>
<div class="m2"><p>روانه کرد درین بحر نیلگون زورق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز روی صدق، به صابون مهر، گازر روز</p></div>
<div class="m2"><p>سحر ز دامن گردون بشست روی شفق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آفتاب برآمد، درآمد از در من</p></div>
<div class="m2"><p>بتی که از رخ او ماه را بود رونق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکسته سنبل او نرخ عنبر سارا</p></div>
<div class="m2"><p>گرفته عارض او بر گل و ریاحین دق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فراز سرو قدش یاسمین و سنبل و گل</p></div>
<div class="m2"><p>درون پیرهنش برگ لاله و زنبق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشیده از سر زلف آفتاب در زنجیر</p></div>
<div class="m2"><p>فکنده بر مه تابان ز مشک ناب وهق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز عکس آتش رخسار چون بهار، گلش</p></div>
<div class="m2"><p>گلاب گشته و افتاده در میان عرق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشاط در دل و در دست جام لعل مذاب</p></div>
<div class="m2"><p>شراب در دست و در پاکشان کشان یلمق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به عشوه ترک کمان دار ناوک اندازش</p></div>
<div class="m2"><p>نشانده در دل من تیغ غمزه نا بر حق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به شکر شکر شیرین دوست، طوطی نطق</p></div>
<div class="m2"><p>دهان به چرب زبانی گشاده چون فستق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگفتم: ای بت ساقی، بیار جام شراب</p></div>
<div class="m2"><p>کنون که مطربم انگشت می زند بر رق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نهاد بر کف من جام باده، گفتا: نوش! ‏</p></div>
<div class="m2"><p>چون نوش کردم از دست او می راوق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز عشق نرگس سرمست و نوبهار رخش</p></div>
<div class="m2"><p>بسان غنچه دریدم ز عاشقی قرطق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز بهر آنکه شوم سرفراز همچو کلاه</p></div>
<div class="m2"><p>به خاک پاش فتادم چون موزه و بشمق</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه گفت؟ گفت که حیدر برو به خدمت شاه</p></div>
<div class="m2"><p>بخوان به حضرت أعلی قصیده ای مغلق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دراز همچو سعادت به پادشاهی رو</p></div>
<div class="m2"><p>که پادشاهی و دولت بدو بود ملحق</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر آن کسی که سر از خط او بگرداند</p></div>
<div class="m2"><p>به زخم تیغ زبان چون قلم کنندش شق</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به گرد سور سرایش که به ز فردوس است</p></div>
<div class="m2"><p>بود ز روی زمین تا به عرش یک خندق</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گهی که شاه رخ آرد بر اسب در عرصه</p></div>
<div class="m2"><p>هزار پیل چو فرزین برد به یک بیدق</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به پایگاه جلالش همی کشند به دست</p></div>
<div class="m2"><p>ز بهر پیشکش، این تند سرکش ابلق</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مطهرست به عالم چو یحیی معصوم</p></div>
<div class="m2"><p>از آنکه نام وی از نام او بود مشتق</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یگانه نصرت دین یحیی ستوده خصال</p></div>
<div class="m2"><p>دلیر و صفدر آفاق، پادشاه بحق</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به پادشاهی و بخشندگی و شرم و ادب</p></div>
<div class="m2"><p>تویی که برده ای از خسروان عهد سبق</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شهان دور که مشهور عالمند امروز</p></div>
<div class="m2"><p>به مجلس تو چو طفلان گرفته اند سبق</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ترا به معرفت حق به حق شناخته ام</p></div>
<div class="m2"><p>تو اهل معرفتی در جهان جان الحق</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به معرفت دو جهان را گرفتی از مردی</p></div>
<div class="m2"><p>چه حاجت است ترا در جهان به تیغ و درق</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدین دلیری و گردافکنی و شیردلی</p></div>
<div class="m2"><p>هر آن کسی که ترا دید می زند صدق</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حروف انجم و نه دفتر سپهر امروز</p></div>
<div class="m2"><p>بود ز دفتر قدر تو کم ز نیم ورق</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز خدمتت نگریزد فلک به هیچ طریق</p></div>
<div class="m2"><p>رخ از در تو نتابد ملک به هیچ نسق</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به قاف قرب ز سیمرغ پیشتر باشد</p></div>
<div class="m2"><p>اگر دمی به هوای تو پر زند عقعق</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سپهر پیر که اقرار بندگی تو کرد</p></div>
<div class="m2"><p>به پیش خلق جهان باز می دهد لاحق</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به هیچ روی نباشد عزیز در بر خلق</p></div>
<div class="m2"><p>کسی که با تو دورویی کند بسان هبق</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر آن کسی که علم شد به دوستداری تو</p></div>
<div class="m2"><p>به پیش خلق بود سرافراز چون بیرق</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عدوی ناکس بی آبرو- که بادا خاک!- ‏</p></div>
<div class="m2"><p>در آتش حسد و حرص می شود محرق</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به خالقی که بگسترد هفت فرش زمین</p></div>
<div class="m2"><p>به قادری که برافراخت نه فلک معلق</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به صانعی که شب از بهر روشنایی خلق</p></div>
<div class="m2"><p>فروخت مشعله ی ماه در میان غسق</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به مصطفی که خدا در ازل به کن فیکون</p></div>
<div class="m2"><p>بیافرید جهان را به عشق او مطلق</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به یاوران و به اولاد پاک زاهر او</p></div>
<div class="m2"><p>که جز به دوستی حق نمی زنند نطق</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به ان یکاد که از بهر چشم زخم دمند</p></div>
<div class="m2"><p>به قل أعوذ که باشد کلام رب فلق</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به چار طبع و سه روح و سه نفس و سه مولد</p></div>
<div class="m2"><p>به شش جهان و به هفت اختر به نه جوسق</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به رهروان طریقت، به عاشقان خدای</p></div>
<div class="m2"><p>که از ریاضت در چشمشان نماند رمق</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به آفتاب که هر صبحدم ز غایت مهر</p></div>
<div class="m2"><p>فراز طارم نیلوفری زند سنجق</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به مطبخ شب و گاو سپهر و بره ی چرخ</p></div>
<div class="m2"><p>به قرص ماه و به نه گرد خوان و هفت طبق</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>که در میانه ی شاهان به شرم و حلم و ادب</p></div>
<div class="m2"><p>نظیر تو نبود زیر گنبد ازرق</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شها! اگر چه که گنج سخن بپاشیدم</p></div>
<div class="m2"><p>نخواستم ز کسی در زمانه یک دانق</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خسیس را نخرم من به هیچ در عالم</p></div>
<div class="m2"><p>اگر چه در بر او سندس است و استبرق</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به شهر یزد شب و روز مبرز الشعرا</p></div>
<div class="m2"><p>هنوز می زند از روی جاهلی بق بق</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شده ست خوار و خلق در میان خلق جهان</p></div>
<div class="m2"><p>مباد هیچ کسی در زمانه خوار و خلق</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو شاهباز حقیقت کجا پرد عصفور</p></div>
<div class="m2"><p>به صوت و نغمه ی بلبل کجا رسد لق لق</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کنیم شکر خدای جهان که هست امروز</p></div>
<div class="m2"><p>کسی که بازشناسد همای را از بق</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همان به است که کوته کنم حدیث دراز</p></div>
<div class="m2"><p>ز بهر آنکه درازی به غایت است احمق</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>همیشه تا به گه شام زورق خورشید</p></div>
<div class="m2"><p>شود درین بن دریای قار مستغرق</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>زبهر آنکه تو دایم به حق همی کوشی</p></div>
<div class="m2"><p>نگاهدار وجود تو باد دایم حق!‏</p></div></div>