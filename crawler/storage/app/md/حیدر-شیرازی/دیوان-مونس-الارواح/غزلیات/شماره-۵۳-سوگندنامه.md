---
title: >-
    شمارهٔ ۵۳ - سوگندنامه
---
# شمارهٔ ۵۳ - سوگندنامه

<div class="b" id="bn1"><div class="m1"><p>به آفتابِ جمالت، به نورِ صبحِ جلال</p></div>
<div class="m2"><p>به آستینِ خیالت، به آستانِ وصال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان دو سنبلِ پرتاب و سروِ سوسن‌بوی</p></div>
<div class="m2"><p>بدان دو نرگسِ پرخواب و غمزهٔ قتال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان رسن که به مکر و فریب و شیوه و غنج</p></div>
<div class="m2"><p>هزار یوسفِ دل کرده‌ای در آن چَهْ چال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بوسهٔ تو که بر عاشقان شده‌ست حرام</p></div>
<div class="m2"><p>به خون من که به دین تو گشته است حلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سرخیِّ رخ باده، به زردیِّ رخ من</p></div>
<div class="m2"><p>به سبزیِّ خطِ سبزت، به دل‌سیاهیِّ خال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آتشِ رخِ خوبت، به آبِ دیدهٔ من</p></div>
<div class="m2"><p>به خاکِ کعبهٔ کویت، به بوی بادِ شمال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به آهِ سوزِ غریبان، به شامِ تنهایی</p></div>
<div class="m2"><p>به محنتِ شبِ هجران، به ذوقِ روزِ وصال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدان خدای که چون ابرو و رخِ تو، قمر</p></div>
<div class="m2"><p>ز صُنعِ خویش کند گاه بدر و گاه هلال؛</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که از فراقِ رُخَت نوش می‌کند حیدر</p></div>
<div class="m2"><p>ز دستِ غصه قدح‌های زهر مالامال</p></div></div>