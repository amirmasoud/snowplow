---
title: >-
    شمارهٔ ۵۴ - و له ایضا
---
# شمارهٔ ۵۴ - و له ایضا

<div class="b" id="bn1"><div class="m1"><p>چنان که قطرهٔ شبنم ز ارغوان بچکد</p></div>
<div class="m2"><p>عرق ز عارض آن ماه مهربان بچکد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شرم آتش و آب رخش به طرف چمن</p></div>
<div class="m2"><p>گل آب گردد و بر روی گلستان بچکد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ز دیدهٔ من یک زمان نهان گردد</p></div>
<div class="m2"><p>هزار قطرهٔ خونم ز دیدگان بچکد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دست دیده و دل روز و شب به فریادم</p></div>
<div class="m2"><p>ز بس که خون دل از دیده‌ام روان بچکد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گهر چو رستهٔ دندان او کجا باشد؟</p></div>
<div class="m2"><p>که قطره‌ای است از ابر درفشان بچکد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو ترک عربده جویش سنان غمزه کشد</p></div>
<div class="m2"><p>عجب نباشد اگر خونش از سنان بچکد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر به نرگس مستش نظر کنم از شرم</p></div>
<div class="m2"><p>هزار قطره گلابش ز ارغوان بچکد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به یاد آتش و آب لب شکرریزش</p></div>
<div class="m2"><p>‏«هزار بیت بگویم که آب از آن بچکد»‏</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو وصف گوهر دندان او کند حیدر</p></div>
<div class="m2"><p>هزار لؤلؤ شهوارش از زبان بچکد</p></div></div>