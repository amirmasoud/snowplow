---
title: >-
    شمارهٔ ۶۴ - خواجوی کرمانی غیبت شیخ سعدی کرد این مدح گفته شد
---
# شمارهٔ ۶۴ - خواجوی کرمانی غیبت شیخ سعدی کرد این مدح گفته شد

<div class="b" id="bn1"><div class="m1"><p>حیات جاودان شد جان سعدی</p></div>
<div class="m2"><p>ز عشق آمد پدید ایمان سعدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخنگویان بیفتند از فصاحت</p></div>
<div class="m2"><p>اگر آیند در میدان سعدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تیغ نظم چون آفاق بگرفت</p></div>
<div class="m2"><p>به شرق و غرب شد فرمان سعدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درآری زیر فرمان چار ارکان</p></div>
<div class="m2"><p>اگر آری بجا ارکان سعدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و گر خواهی، بیابی از حقیقت</p></div>
<div class="m2"><p>دلیل عشق از برهان سعدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسان پارسایان، خلق عالم</p></div>
<div class="m2"><p>شدند از جان و دل حیران سعدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلا! گر روز عید وصل خواهی</p></div>
<div class="m2"><p>به کیش عشق شو قربان سعدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مبر در پیش شاعر نام خواجو</p></div>
<div class="m2"><p>به کیش عشق شو قربان سعدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مبر در پیش شاعر نام خواجو</p></div>
<div class="m2"><p>که او دزدی است از دیوان سعدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو نتواند که با من شعر گوید</p></div>
<div class="m2"><p>چرا گوید سخن در شأن سعدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگس بنگر که از شوخی که دارد</p></div>
<div class="m2"><p>حلاوت می برد از خوان سعدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر حیدر ببازد جان چه باشد</p></div>
<div class="m2"><p>هزارش جان فدای جان سعدی</p></div></div>