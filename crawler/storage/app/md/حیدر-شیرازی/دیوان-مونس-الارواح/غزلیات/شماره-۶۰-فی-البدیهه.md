---
title: >-
    شمارهٔ ۶۰ - فی البدیهه
---
# شمارهٔ ۶۰ - فی البدیهه

<div class="b" id="bn1"><div class="m1"><p>نوروز و عید ماست که روی تو دیده‌ایم</p></div>
<div class="m2"><p>وز شام غم به صبح سعادت رسیده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای نور دیده! چهرهٔ روشن به ما نمای</p></div>
<div class="m2"><p>کز بهر دیدن تو سراپای دیده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آرزوی روی و لب جان‌فزای تو</p></div>
<div class="m2"><p>صد باز پشت دست به دندان گزیده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگذر دمی به ناز که دیبای روی زرد</p></div>
<div class="m2"><p>از احترام در قدمت گستریده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گنج وصل خویش ز بهر شفای دل</p></div>
<div class="m2"><p>تریاک ده که زهر دمادم چشیده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بهر پای اسب تو در دامن بصر</p></div>
<div class="m2"><p>دردانه‌ها به خون جگر پروریده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در زلف مشکبار تو چون حیدر ضعیف</p></div>
<div class="m2"><p>پیوسته‌ایم، وز همه عالم بریده‌ایم</p></div></div>