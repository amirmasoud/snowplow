---
title: >-
    شمارهٔ ۶ - مرثیهٔ سلطان ابوسعید
---
# شمارهٔ ۶ - مرثیهٔ سلطان ابوسعید

<div class="b" id="bn1"><div class="m1"><p>باز ازین واقعه ما را دل و جان می‌سوزد</p></div>
<div class="m2"><p>نه دل ما، که دل خلق جهان می‌سوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوییا آتش دوزخ به جهان در زده‌اند</p></div>
<div class="m2"><p>که دل مرد و زن و پیر و جوان می‌سوزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنگ در چنگ مغنی ز درون می‌نالد</p></div>
<div class="m2"><p>شمع در مجلس ما گریه‌کنان می‌سوزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه منم سوخته در دهر که از آتش مهر</p></div>
<div class="m2"><p>دل چرخ از غم سلطان جهان می‌سوزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوسعید آن شه فرخ‌رخ فرخنده‌وصال</p></div>
<div class="m2"><p>آنکه در ماتم او کون و مکان می‌سوزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاه در خاک نهاد این فلک چابک سیر</p></div>
<div class="m2"><p>در همه جایگهی آتش از آن می‌سوزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق سوخته با انده و غم می‌سازد</p></div>
<div class="m2"><p>حیدر خسته‌دل از عشق فلان می‌سوزد</p></div></div>