---
title: >-
    شمارهٔ ۲۴ - و له ایضا
---
# شمارهٔ ۲۴ - و له ایضا

<div class="b" id="bn1"><div class="m1"><p>تا به شیراز نگارم ز سفر باز آمد</p></div>
<div class="m2"><p>راحت روح من خسته جگر باز آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناگه آن ماه پری چهره روان از چشمم</p></div>
<div class="m2"><p>همچو سیاره شد و همچو قمر باز آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن صنم نور بصر بود، برفت از بصرم</p></div>
<div class="m2"><p>وین زمان در بصرم نور بصر باز آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو پسته من دل خسته نگنجم در پوست</p></div>
<div class="m2"><p>که مرا یار شکرخنده ز در باز آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رد مگردان دل حیدر که قبول تو شود</p></div>
<div class="m2"><p>خاصه اکنون که به پیش تو نظرباز آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل به مصر دهنت رفت و شد آنجا عاشق</p></div>
<div class="m2"><p>چون تواند دل از آن تنگ شکر باز آمد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل من از همه باز آمد و در دست غمت</p></div>
<div class="m2"><p>همچو مستی است که در پیش خطر باز آمد</p></div></div>