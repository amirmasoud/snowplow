---
title: >-
    شمارهٔ ۱۵ - و له ایضا
---
# شمارهٔ ۱۵ - و له ایضا

<div class="b" id="bn1"><div class="m1"><p>پرتو روی تو از روی صفا می‌بینم</p></div>
<div class="m2"><p>مردم چشم تو در عین حیا می‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در لبت می‌نگرم، جوهر جان می‌یابم</p></div>
<div class="m2"><p>در رخت می‌نگرم، صنع خدا می‌بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شام گیسوی تو یا ملک ختن می‌نگرم؟</p></div>
<div class="m2"><p>زلف پرچین تو یا مشک خطا می‌بینم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرد کوه از سر غیرت چو کمر می‌گردم</p></div>
<div class="m2"><p>زآنکه اندام تو در بند قبا می‌بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون صبا بی سر و پا می‌روم و سودایی</p></div>
<div class="m2"><p>تا سر زلف تو در دست صبا می‌بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل یکتای خود از غایت سرگردانی</p></div>
<div class="m2"><p>بسته در سلسلهٔ زلف دوتا می‌بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به هم درشکند قلب اسیران کمند</p></div>
<div class="m2"><p>ترک سرمست تو با تیغ جفا می‌بینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل که از غصه به تنگ آمد و ناپیدا شد</p></div>
<div class="m2"><p>اثرش در دهن تنگ شما می‌بینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تو چندان که به من جور و جفا می‌آید</p></div>
<div class="m2"><p>همچنان در دل خود مهر و وفا می‌بینم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حیدر از آرزوی آتش و آب رخ تو</p></div>
<div class="m2"><p>خاکش از مهر تو بر باد هوا می‌بینم</p></div></div>