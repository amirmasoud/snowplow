---
title: >-
    شمارهٔ ۵۶ - مدح شیراز
---
# شمارهٔ ۵۶ - مدح شیراز

<div class="b" id="bn1"><div class="m1"><p>دلم ز خطهٔ شیراز و قوم او شادست</p></div>
<div class="m2"><p>که بِهْ ز خطهٔ مصر و دمشق و بغدادست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر طرف که روم دلبری شکردهن است</p></div>
<div class="m2"><p>به هرکجا نگرم لعبتی پریزادست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طواف خطهٔ شیراز می‌کنم شب و روز</p></div>
<div class="m2"><p>که همچو کعبه عزیز و لطیف‌بنیادست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو یار حورْوَشَم با شراب دست دهد</p></div>
<div class="m2"><p>به لاله‌زار خرامم که جنت‌آبادست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درآ به باغ ملک، بند غم ز دل بگشا</p></div>
<div class="m2"><p>ببین که لاله‌عذارم چو سرو آزادست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا که جنت و کوثر مصلی و رکنی است</p></div>
<div class="m2"><p>ببین که باغ ارم باغ جعفرآبادست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر که خسرو شیرین‌دهن نمی‌داند</p></div>
<div class="m2"><p>که شور شکر شیرین ز سوز فرهادست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آب و آتش عشقش چگونه خاک شوم</p></div>
<div class="m2"><p>که خاکساری من در هوای او بادست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی‌خورد غم دنیا و آخرت حیدر</p></div>
<div class="m2"><p>دلش به شادی وصل تو در جهان شادست</p></div></div>