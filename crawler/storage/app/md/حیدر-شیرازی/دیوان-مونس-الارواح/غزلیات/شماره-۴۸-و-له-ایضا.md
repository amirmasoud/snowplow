---
title: >-
    شمارهٔ ۴۸ - و له ایضا
---
# شمارهٔ ۴۸ - و له ایضا

<div class="b" id="bn1"><div class="m1"><p>به ناز اگر ز در آن سرو ناز باز آید</p></div>
<div class="m2"><p>به صید کردن مرغ دلم چو باز آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ناز در چمن جان خود کنم جایش</p></div>
<div class="m2"><p>اگر به طرف چمن همچو سرو ناز آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به مجلسی اگر آن رود را به چنگ آرم</p></div>
<div class="m2"><p>چو رود، کار من بی نوا بساز آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که دیده به محراب ابرویت افکند</p></div>
<div class="m2"><p>به پیش قبله ی روی تو در نماز آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آنکه روز رخت را شبی نظاره کند</p></div>
<div class="m2"><p>دل رمیده ی من در شب دراز آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر آنکه رشته ی مهر تو در دلش باشد</p></div>
<div class="m2"><p>چو شمع از آتش مهر تو در گداز آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز غصه حیدر آشفته دل به پای طلب</p></div>
<div class="m2"><p>همیشه بر سر کوی تو از نیاز آید</p></div></div>