---
title: >-
    شمارهٔ ۶۲ - و له ایضا
---
# شمارهٔ ۶۲ - و له ایضا

<div class="b" id="bn1"><div class="m1"><p>ساقی! بیار باده و مطرب بساز ساز</p></div>
<div class="m2"><p>تا برگ عیش را بود از نغمه تو ساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر کعبه کوی دوست بود می کنم طواف</p></div>
<div class="m2"><p>ور قبله روی یار بود می برم نماز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا همچو باز دیده فروختم ز غیر</p></div>
<div class="m2"><p>جز بر رخ تو می نکنم هر دو دیده باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک دم کرشمه ای کن و صد بی نوا بسوز</p></div>
<div class="m2"><p>روزی عنایتی کن و با عاشقی بساز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا قامت و رخ تو بدیدم نمی کنم</p></div>
<div class="m2"><p>هرگز تفرج گل خندان و سرو ناز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رامین شدی و کعبه ی خود ساز کوی ویس</p></div>
<div class="m2"><p>محمود باش و قبله ی خود کن رخ ایاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیدر! حمایت سر زلفش چه می‌کنی</p></div>
<div class="m2"><p>کوته زبان چگونه حکایت کند دراز؟</p></div></div>