---
title: >-
    شمارهٔ ۱۹ - و له ایضا
---
# شمارهٔ ۱۹ - و له ایضا

<div class="b" id="bn1"><div class="m1"><p>روز نوروز و می و مطرب و معشوق و بهار</p></div>
<div class="m2"><p>مستی و عشرت و آغوش و بر و بوس و کنار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سمن و نسترن و یاسمن و سرو چمن</p></div>
<div class="m2"><p>سوسن و برگ گل و جام می و روی نگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کام و آرام و نشاط و طرب و عیش و خوشی</p></div>
<div class="m2"><p>سرو و شمشاد و گل و سنبل و سیب و به و نار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستی نرگس و رنگ چمن و سایه ی بید</p></div>
<div class="m2"><p>غلغل بلبل و بوی گل و آواز هزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیوه و ناز و عتاب صنم سیم اندام</p></div>
<div class="m2"><p>شاهد و شمع و شراب و بت شیرین گفتار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باغ و صحرا و لب جوی و قد سرو سهی</p></div>
<div class="m2"><p>عنبر و عود و عبیر و سمن و مشک تتار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم در شاهد و گل در بر و ساغر در دست</p></div>
<div class="m2"><p>گوش بر مطرب و می در سر و لب بر لب یار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یار در مجلس و مه در خور و حیدر با دوست</p></div>
<div class="m2"><p>باده در ساغر و جان در بر و دل با دلدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش بود این همه گر دست دهد وقت صبوح</p></div>
<div class="m2"><p>خوش بود این همه گر جمع شود فصل بهار</p></div></div>