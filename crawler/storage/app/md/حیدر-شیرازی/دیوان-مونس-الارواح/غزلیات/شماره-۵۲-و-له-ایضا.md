---
title: >-
    شمارهٔ ۵۲ - و له ایضا
---
# شمارهٔ ۵۲ - و له ایضا

<div class="b" id="bn1"><div class="m1"><p>خیال دوست که در خواب می‌کند بازی</p></div>
<div class="m2"><p>درون دیدهٔ پُرآب می‌کند بازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آفتاب، سر زلف عنبرافشانش</p></div>
<div class="m2"><p>چو هندویی ز سر تاب می‌کند بازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز طوطی خط او چون نبات بگدازم</p></div>
<div class="m2"><p>از آنکه با شکر ناب می‌کند بازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گرد چشمهٔ نوش و لب شکربارش</p></div>
<div class="m2"><p>بنفشه با گل سیراب می‌کند بازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن زمان که پرید از برم کبوتر دل</p></div>
<div class="m2"><p>در آن دو طرّهٔ پرتاب می‌کند بازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا و بر رخ همچون زرم تماشا کن</p></div>
<div class="m2"><p>که آب دیده چو سیماب می‌کند بازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز نازکی تن چو قاقمش به رنج آید</p></div>
<div class="m2"><p>اگرچه بر سر سنجاب می‌کند بازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تنم که غرقهٔ دریای اشک خونین شد</p></div>
<div class="m2"><p>چو ماهی است که در آب می‌کند بازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درون خانهٔ دلبر نمی‌رود حیدر</p></div>
<div class="m2"><p>چو حلقه بر در ازین باب می‌کند بازی</p></div></div>