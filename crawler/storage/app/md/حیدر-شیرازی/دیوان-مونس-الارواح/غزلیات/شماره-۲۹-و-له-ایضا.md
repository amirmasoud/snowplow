---
title: >-
    شمارهٔ ۲۹ - و له ایضا
---
# شمارهٔ ۲۹ - و له ایضا

<div class="b" id="bn1"><div class="m1"><p>جانا! دل من چون دهن تنگ تو تنگ است</p></div>
<div class="m2"><p>پشتم ز خیال سر زلف تو چو چنگ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای دلم از بهر تو در بحر محیط است</p></div>
<div class="m2"><p>کام دلم از کام تو در کام نهنگ است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر حال من زار جگرخوار نبخشی</p></div>
<div class="m2"><p>آن دل که تو داری مگر از آهن و سنگ است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خونم بخوری، دل ببری، چهره بپوشی</p></div>
<div class="m2"><p>ای ماه پری چهره نگویی که چه ینگ است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکرانه دهم جان و کنم آشتی از نو</p></div>
<div class="m2"><p>گر زآنکه ترا با من دل سوخته جنگ است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن چشم خویش دلکش سرمست جهانگیر</p></div>
<div class="m2"><p>ترکی است کمان دار که با تیر خدنگ است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خوی تو بیداد کشد حیدر بیدل</p></div>
<div class="m2"><p>خوی تو چه خوبی است مگر خوی پلنگ است؟</p></div></div>