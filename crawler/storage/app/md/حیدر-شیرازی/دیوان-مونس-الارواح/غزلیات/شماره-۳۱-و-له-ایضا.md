---
title: >-
    شمارهٔ ۳۱ - و له ایضا
---
# شمارهٔ ۳۱ - و له ایضا

<div class="b" id="bn1"><div class="m1"><p>من عاشق آن سنگدل تنگ دهانم</p></div>
<div class="m2"><p>دل بسته آن پسته ی شیرین فلانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم که برش بر بر چون سیم بسایم</p></div>
<div class="m2"><p>خواهم که لبش بر لب شیرین برسانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آیا بود آن روز که در مجلس شادی</p></div>
<div class="m2"><p>بنشینم و در صحبت خویشش بنشانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه بوسه دهم بر لب شیرین چو قندش</p></div>
<div class="m2"><p>گه کام دل از پسته ی تنگش بستانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر کفر دو زلفش ببرد دنیی و دینم</p></div>
<div class="m2"><p>ور غمزه ی شوخش بستاند دل و جانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من ترک چنان ترک پری چهره نگویم</p></div>
<div class="m2"><p>همچون سر و زر در قدمش جان بفشانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فریاد که چون حیدر ازین داغ جگر سوز</p></div>
<div class="m2"><p>از هفت فلک می گذرد آه و فغانم</p></div></div>