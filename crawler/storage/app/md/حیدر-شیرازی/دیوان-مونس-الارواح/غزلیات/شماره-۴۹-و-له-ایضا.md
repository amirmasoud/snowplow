---
title: >-
    شمارهٔ ۴۹ - و له ایضا
---
# شمارهٔ ۴۹ - و له ایضا

<div class="b" id="bn1"><div class="m1"><p>به حسرت از قفس سینه مرغ جان برود</p></div>
<div class="m2"><p>چو از برابرم آن یار دلستان برود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یکدم از سپر نه سپهر درگذرد</p></div>
<div class="m2"><p>دمی که ناوک آه من از کمان برود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا بیا و دمی در کنار من بنشین</p></div>
<div class="m2"><p>که اگر دمی بنشینی غم از میان برود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شود چو زلف سیاه تو دیده ها تاریک</p></div>
<div class="m2"><p>چو نور روی تو از چشم عاشقان برود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به باغ اگر گل رخسار خویش عرضه دهی</p></div>
<div class="m2"><p>ز رشک، رنگ ز رخسار ارغوان برود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر آن کسی که بهشت رخ تو می طلبد</p></div>
<div class="m2"><p>به جستجوی تو خواهد که از جهان برود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مصر کوی تو گشتم مقیم و گفت رقیب</p></div>
<div class="m2"><p>عجب بود ز مگس کز شکرستان برود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به مصلحت نفسی پیش عاشقان بنشین</p></div>
<div class="m2"><p>که گر کناره کنی خون درین میان برود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رقیب گفت که حیدر برو ز پیش رخش</p></div>
<div class="m2"><p>چگونه بلبل بیدل ز گلستان برود؟</p></div></div>