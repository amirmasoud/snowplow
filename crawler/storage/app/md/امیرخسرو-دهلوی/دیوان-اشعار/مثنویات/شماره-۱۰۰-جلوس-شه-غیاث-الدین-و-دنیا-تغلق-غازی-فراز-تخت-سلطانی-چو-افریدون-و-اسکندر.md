---
title: >-
    شمارهٔ ۱۰۰ - جلوس شه غیاث الدین و دنیا تغلق غازی فراز تخت سلطانی چو افریدون و اسکندر
---
# شمارهٔ ۱۰۰ - جلوس شه غیاث الدین و دنیا تغلق غازی فراز تخت سلطانی چو افریدون و اسکندر

<div class="b" id="bn1"><div class="m1"><p>مبارک روز شنبه گاه پیشین</p></div>
<div class="m2"><p>گه هنگامی است با انوار بیش این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان از چشمه خود روی شسته</p></div>
<div class="m2"><p>که و مه سبحه و سجاده جسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مؤذن قامت خود بر کشیده</p></div>
<div class="m2"><p>جماعت صف به مسجد بر کشیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ممالک گیر سلطان جهان بخت</p></div>
<div class="m2"><p>در آن ساعت برآمد بر سر تخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سریر آراست ماه و آفتابش</p></div>
<div class="m2"><p>غیاث دین و دنیا شد خطابش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ملایک جمله گفتندش همانگه</p></div>
<div class="m2"><p>دعا: خلد الرحمن ملکه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خروش کوس، گیتی را خبر کرد</p></div>
<div class="m2"><p>دل بد خواه را زیر و زبر کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موافق ریخت گوهرها ز حد بیش</p></div>
<div class="m2"><p>مخالف هم ولیک از دیدهٔ خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فلک شادی بدو ران و زمان داد</p></div>
<div class="m2"><p>جهان را مژدهٔ امن و امان داد</p></div></div>