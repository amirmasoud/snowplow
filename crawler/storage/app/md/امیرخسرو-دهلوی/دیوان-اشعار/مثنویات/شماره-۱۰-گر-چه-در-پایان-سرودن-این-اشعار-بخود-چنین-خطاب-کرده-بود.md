---
title: >-
    شمارهٔ ۱۰ - گر چه در پایان سرودن این اشعار بخود چنین خطاب کرده بود:
---
# شمارهٔ ۱۰ - گر چه در پایان سرودن این اشعار بخود چنین خطاب کرده بود:

<div class="b" id="bn1"><div class="m1"><p>لیک اگر پند من آری به گوش</p></div>
<div class="m2"><p>مصلحت آن ست که مانی خموش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چل شد و درین جهت آمد نشست</p></div>
<div class="m2"><p>پیش مبین بیش که افتی به شست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو بت توبه‌ست ، گرانی مکن</p></div>
<div class="m2"><p>روی به پیر یست، جوانی مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بار خدایا! من غافل به راز،</p></div>
<div class="m2"><p>این ورق ساده که بستم طراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه که امروز جمال من ست</p></div>
<div class="m2"><p>عاقبت الا مر وبال من ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عفو کن آن را که رضای تو نیست</p></div>
<div class="m2"><p>تو به ده از هر چه برای تو نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون زتو شد این همه ناچیز چیز</p></div>
<div class="m2"><p>هم تو کنی در دل خلقی عزیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عیب شناسان به کمین من‌اند</p></div>
<div class="m2"><p>بی هنر ان جمله به کین من آند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو به کرم ، عیب من عیب کوش</p></div>
<div class="m2"><p>در نظر عیب شناسان بپوش</p></div></div>