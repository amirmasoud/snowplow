---
title: >-
    شمارهٔ ۶۱ - ترصیع
---
# شمارهٔ ۶۱ - ترصیع

<div class="b" id="bn1"><div class="m1"><p>باده نوشین به صفا خواست کرد</p></div>
<div class="m2"><p>وعدهٔ دوشین به وفا راست کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نور هدایت به چراغم رسان</p></div>
<div class="m2"><p>بوی عنایت به دماغم رسان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمزدگان را به طرف دلگشای</p></div>
<div class="m2"><p>گمشدگان را به کرم رهنمای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طفل گیار از هوا ریخت شیر</p></div>
<div class="m2"><p>مغز جهان را ز صبا زد عبیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گم شده‌ام راه نمایم تو باش</p></div>
<div class="m2"><p>بی بصرم نور فزایم تو باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برق بهر سوی بتابی دگر</p></div>
<div class="m2"><p>دشت زهر جوی بیابی دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر طرفش ره بشتابی دگر</p></div>
<div class="m2"><p>هر قدمش سیر بر آبی دگر</p></div></div>