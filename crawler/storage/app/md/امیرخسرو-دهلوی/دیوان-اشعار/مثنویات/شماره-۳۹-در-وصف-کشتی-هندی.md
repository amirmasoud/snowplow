---
title: >-
    شمارهٔ ۳۹ - در وصف کشتی هندی
---
# شمارهٔ ۳۹ - در وصف کشتی هندی

<div class="b" id="bn1"><div class="m1"><p>ساخته از حکمت کار آگهان</p></div>
<div class="m2"><p>خانهٔ گردنده بگرد جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نادرهٔ حکم خدای حکیم</p></div>
<div class="m2"><p>خانه روان ، خانگیانش مقیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اهل سفر را همه بروی گذر</p></div>
<div class="m2"><p>همره اوساکن و او در سفر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاه روشن همره او گشته آب</p></div>
<div class="m2"><p>آبله در پاش شده از حباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عکس که بنمود باب اندرون</p></div>
<div class="m2"><p>کشتی خصم ست که بینی نگون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماه رسن بسته چو دلو استوار</p></div>
<div class="m2"><p>یافته در خانهٔ ماهی قرار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماه نوی کاصل وی از سال خواست</p></div>
<div class="m2"><p>یک مه نو گشته بده سال راست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گشته گهٔ سیر، هلالش زبون</p></div>
<div class="m2"><p>عکس هلال ست باب اندرون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صورت آن تخته که بد بی‌بها</p></div>
<div class="m2"><p>عین چو ابرو شده بر چشمها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لیک جزین فرق ندانم کنون</p></div>
<div class="m2"><p>کاوست سر افراخته ابر نگون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ابروی او داده بهر چشم نور</p></div>
<div class="m2"><p>چشم بد از ابروی نیکوش دور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو کمان پر خم و تیره از میان</p></div>
<div class="m2"><p>تیر ستاده ست و کمانش روان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او برسد تیر فلک را به اوج</p></div>
<div class="m2"><p>تیر به تیرش نرسد گاه موج</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیشتر از مرغ پرد در کشاد</p></div>
<div class="m2"><p>پیشتر از باد رود روز باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وقت دو منزل بدمی بل دو چند</p></div>
<div class="m2"><p>بار سن و سلسله و تخته بند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بسته به زنجیر مسلسل دراز</p></div>
<div class="m2"><p>بحر روان زو شده زنجیر ساز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همچو کمان پر خم و تیز از میان</p></div>
<div class="m2"><p>پر، چو حواصل، زد و سو کرده باز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرغ که آن از پر چو بین پرد</p></div>
<div class="m2"><p>طرفه بود لیک نه چندین پرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر طرفش ره بشتاب دگر</p></div>
<div class="m2"><p>هر قدمش سیر برآب دیگر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از تگ طوفان شکنش در شتاب</p></div>
<div class="m2"><p>معجز نوح آمده بر روی آب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر چه زد ریا گذرد بیش و کم</p></div>
<div class="m2"><p>آب نباشد مگرش تا شکم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دیده شب و روز بسی گرم و سرد</p></div>
<div class="m2"><p>رفته بهر سوز پی آب خورد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لطمه زنان بر رخ دریا به زور</p></div>
<div class="m2"><p>آب ازان لطمه به فریاد و شور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا عمل بحر شدش مستقیم</p></div>
<div class="m2"><p>آمده از عبرهٔ دریاش سیم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پیشهٔ ملاح در و شیم پاش</p></div>
<div class="m2"><p>تیشهٔ نجار از و در خراش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرکب بحری زسفر گشته چوب</p></div>
<div class="m2"><p>بر طرف بحر شده پای کوب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بگذرد از آب سوارش به خواب</p></div>
<div class="m2"><p>غرقه نگردد چو سواران آب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در ته او آب سبک خیز نیست</p></div>
<div class="m2"><p>گر چه که صد نیزه بود، تیز نیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در ره بی آب نداند شدن</p></div>
<div class="m2"><p>کیست که بی آب تواند شدن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>موج گران یافت سبک بر رود</p></div>
<div class="m2"><p>ارچه گران گشت سبک تر رود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شاه در آن خانهٔ چو بین نشست</p></div>
<div class="m2"><p>وز پل چو بین همه دریا ببست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آب شد از بحر روان تخته پوش</p></div>
<div class="m2"><p>کرده ز هر تخته معلم خروش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>موج سوی جاریه می‌برد دست</p></div>
<div class="m2"><p>بیل به سیلیش همی کرد پست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نعره ملاح که می‌شد به اوج</p></div>
<div class="m2"><p>بر تن خود لرزه همی کرد موج</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سلسلهٔ موج ز دامی که بافت</p></div>
<div class="m2"><p>ماهی از آن دام خلاصی نیافت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بس که بجوشید زمین همچو دیگ</p></div>
<div class="m2"><p>آب روان تشنهٔ گل شد به ریگ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آب از آن غلغل ز اندازه بیش</p></div>
<div class="m2"><p>گرد نمی‌گشت به گرد آب خویش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>عکس رسنها که فرو شد باب</p></div>
<div class="m2"><p>بست به پهلوی نهنگان طناب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کشتی شه تیزتر از تیر گشت</p></div>
<div class="m2"><p>در زدن چشم ز دریا گذشت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>راست که شه بر لب دریا رسید</p></div>
<div class="m2"><p>گوهر خود بر لب دریا بدید</p></div></div>