---
title: >-
    شمارهٔ ۶۵ - بمن فی العشق مات و حی فیه
---
# شمارهٔ ۶۵ - بمن فی العشق مات و حی فیه

<div class="b" id="bn1"><div class="m1"><p>سر نامه به نام آن خداوند</p></div>
<div class="m2"><p>که دلها را به خوبان داد پیوند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز عشق آراست لوح آب و گل را</p></div>
<div class="m2"><p>بدان جان، زندگی بخشید دل را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز زلف و رخ، بتان را روز و شب داد</p></div>
<div class="m2"><p>وزان نظاره جانها را طرب داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلم را داد سودای الهی</p></div>
<div class="m2"><p>که بنوشت این سپیدی و سیاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بتان چین و خوبان طرازی</p></div>
<div class="m2"><p>پدید آورد بهر عشق بازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرشمه داد چشم نیکوان را</p></div>
<div class="m2"><p>شکار شیر فرمود آهوان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مسلسل کرد زلف ماهرویان</p></div>
<div class="m2"><p>مشوش روزگار مهر جویان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز هی نقاش صورت های زیبا</p></div>
<div class="m2"><p>که پشت خاک ازو شد روی دیبا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمک بخش دهن‌های شکر خند</p></div>
<div class="m2"><p>حلاوت پرور لبهای چون قند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیاراید به مروارید گل پوش</p></div>
<div class="m2"><p>عروسان چمن را گردن و گوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نهد در صبح مهری کاندر افلاک</p></div>
<div class="m2"><p>به رسم عاشقان دامن کند چاک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز هستی هر چه دارد صورت بود</p></div>
<div class="m2"><p>ز سر عشق کرد آن جمله موجود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بادم داد شمع و روشنائی</p></div>
<div class="m2"><p>نهاد ابلیس را داغ جدائی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو بر نوح از تف غیرت زند برق</p></div>
<div class="m2"><p>به طوفان مردم چشمش کند غرق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به نوری بخشد ابراهیم را راه</p></div>
<div class="m2"><p>که در چشمش نیاید انجم و ماه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو خواهد عین یعقوب از پسر نور</p></div>
<div class="m2"><p>ز عینش قرة العینش کند دور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کند بر موسی آن راز آشکارا</p></div>
<div class="m2"><p>که تاب آن نیارد کوه خارا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو تاب مهر بر روح الله افشاند</p></div>
<div class="m2"><p>ز مهر و دوستی جان خودش خواند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو مهرش زد به زلف مصطفی دست</p></div>
<div class="m2"><p>چنان صد جان به تار موی اوبست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جمالی داد احمد را بدرگاه</p></div>
<div class="m2"><p>که چاک افتاد زان در سینهٔ ماه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به یارنش هم ز دل چاشنی داد</p></div>
<div class="m2"><p>ز سوز، آن شمعها را روشنی داد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بامت هم رسید آن شعلهٔ شوق</p></div>
<div class="m2"><p>که چون پروانه جان دادند از آن ذوق</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همو راند ز در نامقبلان را</p></div>
<div class="m2"><p>همو خواند بخود صاحب دلان را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گهی بخشد جنیدی را کلاهی</p></div>
<div class="m2"><p>که تنها ز اهل دل باشد سپاهی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گهی با شبلی آن همت کند ضم</p></div>
<div class="m2"><p>که صید خویش نپسندد دو عالم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گهی در پیش شاد روان اسرار</p></div>
<div class="m2"><p>نماید جلوهٔ منصور برادر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همو داند که این راز نهان چیست</p></div>
<div class="m2"><p>چه داند مردم گم گشته، کان چیست؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شناسای ضمیر راز دانان</p></div>
<div class="m2"><p>مراد سینه‌های پاک جانان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز لیلی او به دفتر زد رقم را</p></div>
<div class="m2"><p>همو پرداخت از مجنون قلم را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنان بخشد به خسرو شربت کام</p></div>
<div class="m2"><p>که از شیرین و شکر خوش کند کام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کند فرهاد را روزی چنان تنگ</p></div>
<div class="m2"><p>که میرد، سنگ بر دل، در دل سنگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه جرمی دارد آن کو کام کم یافت</p></div>
<div class="m2"><p>نه کاری بیش کرد آن کین کرم یافت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نوشته بر سر ما یفعل الله</p></div>
<div class="m2"><p>چرا و چون کجا گنجد درین راه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر آنچه او کرد گر خوب است و گر زشت</p></div>
<div class="m2"><p>خردمند آن همه جز خوب ننوشت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ازو دان هر چه هست ار هست ور نیست</p></div>
<div class="m2"><p>که هست و نیست «کن» جزوی دگر نیست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بهر کس نعمت شایان سپرده</p></div>
<div class="m2"><p>خرد را گنج بی پایان سپرده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پس آنگه عشق را کرده اشارت</p></div>
<div class="m2"><p>که اندر گنج عقل افگنده غارت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز گنج عقل «خسرو» را خبر نیست</p></div>
<div class="m2"><p>درو جز عاشقی عیبی دگر نیست</p></div></div>