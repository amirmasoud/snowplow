---
title: >-
    شمارهٔ ۲۲ - پیام پدر
---
# شمارهٔ ۲۲ - پیام پدر

<div class="b" id="bn1"><div class="m1"><p>ای سر از آئین وفا تافته !</p></div>
<div class="m2"><p>وز تو دلم تافتگی یافته !</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه به غیبت شدئی کینه توز</p></div>
<div class="m2"><p>رنجه چه‌داری به حضورم هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با چو منی ، دور کن از سر منی</p></div>
<div class="m2"><p>چون به صفت من توام و تو منی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر کمر کینه کنی استوار</p></div>
<div class="m2"><p>پیش تو بیش از تو درایم به کار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور به مدارا کشد این گفت و گوی</p></div>
<div class="m2"><p>نیز نتابم ز وفای تو روی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیک بشرطی که درین رای من</p></div>
<div class="m2"><p>جای پدر گیرم و تو جای من</p></div></div>