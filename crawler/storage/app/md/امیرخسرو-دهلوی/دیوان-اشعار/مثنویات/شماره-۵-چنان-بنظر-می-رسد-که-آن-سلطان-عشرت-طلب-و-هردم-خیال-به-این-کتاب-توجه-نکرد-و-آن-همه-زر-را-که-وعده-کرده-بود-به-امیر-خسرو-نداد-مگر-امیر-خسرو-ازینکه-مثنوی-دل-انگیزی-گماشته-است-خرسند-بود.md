---
title: >-
    شمارهٔ ۵ - چنان بنظر می‌رسد که آن سلطان عشرت طلب و هردم خیال، به این کتاب توجه نکرد و آن همه زر را که وعده کرده بود به امیر خسرو نداد، مگر امیر خسرو ازینکه مثنوی دل‌انگیزی گماشته است ، خرسند بود:
---
# شمارهٔ ۵ - چنان بنظر می‌رسد که آن سلطان عشرت طلب و هردم خیال، به این کتاب توجه نکرد و آن همه زر را که وعده کرده بود به امیر خسرو نداد، مگر امیر خسرو ازینکه مثنوی دل‌انگیزی گماشته است ، خرسند بود:

<div class="b" id="bn1"><div class="m1"><p>گر چه شد از بهر چنین نامه‌ای</p></div>
<div class="m2"><p>داد مرا گرمی هنگامه ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناز پی آن شد قلم سحر سنج</p></div>
<div class="m2"><p>کز پی آین مار نشینم به گنج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من که نهادم ز سخن گنج پاک</p></div>
<div class="m2"><p>گنج زر اندر نظرم چیست ؟ خاک!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دهدم تا جور سر بلند</p></div>
<div class="m2"><p>در نتوان باز به در یافگند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور ندهد زان خودم رایگان</p></div>
<div class="m2"><p>رنجه نگردم چو تهی مایگان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک جو از ین فن چو به دامن نهم</p></div>
<div class="m2"><p>ده کنم آن را و به صد تن دهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیرم و رنج از پی یاران برم</p></div>
<div class="m2"><p>نی چو سگ خانه که تنها خورم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این همه شربت نه بدان کرده‌ام</p></div>
<div class="m2"><p>کاب ز دریای کرم خورده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر همه دانند که چندین گهر</p></div>
<div class="m2"><p>کس نفشاند بدو سه بدره زر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور دیدم گنج فریدون و جم</p></div>
<div class="m2"><p>هدیهٔ یک حرف بود، بلکه کم !</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر صفتی را که بر انگیختم</p></div>
<div class="m2"><p>شعبدهٔ تازه درو ریختم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مور شدم بر شکر خویش و بس</p></div>
<div class="m2"><p>در نزدم دست به حلوای کس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر چه که از دل در مکنون کشم</p></div>
<div class="m2"><p>زهرهٔ آن نیست که بیرون کشم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زانکه نگه می‌کنم از هر کران</p></div>
<div class="m2"><p>ایمنی ام نست زغارت گران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دزد متاع من و با من به جوش</p></div>
<div class="m2"><p>شان به زبان آوری و من خموش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نقد مرا پیش من آرند راست</p></div>
<div class="m2"><p>من کنم «احسنت!» کز آن شماست!</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شرم ندارند و بخوانند گرم</p></div>
<div class="m2"><p>با من ومن هیچ نگویم ز شرم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>طرفه که شان دزد من از شرم پاک</p></div>
<div class="m2"><p>حاجب کالا من و من شرم ناک</p></div></div>