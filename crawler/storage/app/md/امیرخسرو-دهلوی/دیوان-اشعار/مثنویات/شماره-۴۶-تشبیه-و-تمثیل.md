---
title: >-
    شمارهٔ ۴۶ - تشبیه و تمثیل
---
# شمارهٔ ۴۶ - تشبیه و تمثیل

<div class="b" id="bn1"><div class="m1"><p>لشکر اسلام که آنجا رسید</p></div>
<div class="m2"><p>بود زمین تشنه که دریا رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود به یک جای صف تیغ و تیر</p></div>
<div class="m2"><p>همچو نیستان به لب آبگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیز تگ و گوش چو پیکان پدید</p></div>
<div class="m2"><p>بر سر یک تیر دو پیکان که دید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دائرهٔ خیمه به سبزی قطار</p></div>
<div class="m2"><p>ابر فرود آمده در مرغزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیک گران سنگ سبک ایستاد</p></div>
<div class="m2"><p>تند چو ابری که رود روز باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طرفه عروسی شده آراسته</p></div>
<div class="m2"><p>آئینهٔ از آب روان خواسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو دو آئینه مقابل ز تاب</p></div>
<div class="m2"><p>آب در آن عکس نما، رو در آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قطره که شد زابر چکان بر هوا</p></div>
<div class="m2"><p>مهرهٔ بلور شده در هوا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باده چو خورشید پگه تا به شام</p></div>
<div class="m2"><p>کرده طلوعی و غروبی به جام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رود زن از سینه برون برده صبر</p></div>
<div class="m2"><p>آب چکان دست چو باران ز ابر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پشت وی از بار گهر خم زده</p></div>
<div class="m2"><p>چون به سحر گلشن شبنم زده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز ابروی خم پشت کمان ساخته</p></div>
<div class="m2"><p>تیر مژه نیم کش انداخته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر دو به یک تن چو دو پیکر شدند</p></div>
<div class="m2"><p>بر فلک تخت چو مه بر شدند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سایه یکی کرد دو فر همای</p></div>
<div class="m2"><p>پایه یکی ساخت دو لشکر گشای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شاخ بهم سود دو سرو جوان</p></div>
<div class="m2"><p>موج بهم داد دو آب روان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گشت یکی باغ وفا داد و جوی</p></div>
<div class="m2"><p>گشت یکی منبع صفا را دو روی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گشت زمین آب دو باران چشید</p></div>
<div class="m2"><p>مغز جهان بوی دو بستان کشید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چرخ یکی شد به دو ماه تمام</p></div>
<div class="m2"><p>بزم یکی شد به دو دور مدام</p></div></div>