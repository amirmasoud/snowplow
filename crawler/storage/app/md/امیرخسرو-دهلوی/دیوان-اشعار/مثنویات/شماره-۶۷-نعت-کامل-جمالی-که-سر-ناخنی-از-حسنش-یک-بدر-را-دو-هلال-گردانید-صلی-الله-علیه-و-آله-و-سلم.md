---
title: >-
    شمارهٔ ۶۷ - نعت کامل جمالی که سر ناخنی از حسنش یک بدر را دو هلال گردانید، صلی الله علیه و آله و سلم
---
# شمارهٔ ۶۷ - نعت کامل جمالی که سر ناخنی از حسنش یک بدر را دو هلال گردانید، صلی الله علیه و آله و سلم

<div class="b" id="bn1"><div class="m1"><p>محمد کایت نورست رویش</p></div>
<div class="m2"><p>سواد روشن و اللیل، مویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرامی نازنین حضرت پاک</p></div>
<div class="m2"><p>کزو نازند هم انجم هم افلاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو نور پاکش اول مشعل افروخت</p></div>
<div class="m2"><p>مه و خورشید شمع خویش از آن سوخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم از معشوق و عاشق نیست تمییز</p></div>
<div class="m2"><p>محب صانع و محبوب او نیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به قلب عرش گشته مسند آر ای</p></div>
<div class="m2"><p>به عرش قلب رایت کرده بر پای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بشر دری دریای وجودش</p></div>
<div class="m2"><p>جهان یک قطره از باران جودش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهی امی، نظر بر لوح بازش</p></div>
<div class="m2"><p>قلم سر گشته در سودای رازش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حریم الله ز محمودی مقامش</p></div>
<div class="m2"><p>ید الله دستگاه احترامش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گهی همخوان مسکینان به قوتی</p></div>
<div class="m2"><p>گهی مهمان بغار عنکبوتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به عون امت مسکین و محتاج</p></div>
<div class="m2"><p>شفاعت را به بالا کرده معراج</p></div></div>