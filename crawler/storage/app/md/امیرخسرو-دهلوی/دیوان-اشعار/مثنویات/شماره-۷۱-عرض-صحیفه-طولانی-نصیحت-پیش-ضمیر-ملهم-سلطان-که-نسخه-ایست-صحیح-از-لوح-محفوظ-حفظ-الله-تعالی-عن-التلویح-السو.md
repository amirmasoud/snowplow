---
title: >-
    شمارهٔ ۷۱ - عرض صحیفهٔ طولانی نصیحت، پیش ضمیر ملهم سلطان، که نسخه‌ایست صحیح از لوح محفوظ حفظ الله تعالی عن التلویح السو
---
# شمارهٔ ۷۱ - عرض صحیفهٔ طولانی نصیحت، پیش ضمیر ملهم سلطان، که نسخه‌ایست صحیح از لوح محفوظ حفظ الله تعالی عن التلویح السو

<div class="b" id="bn1"><div class="m1"><p>گرفتن سهل باشد، این جهان را</p></div>
<div class="m2"><p>کلید آن جهان، باید شهان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن بس بر همین کز تیغ و از رای</p></div>
<div class="m2"><p>همه دنیا گرفتی، شسته بر جای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به همت آسمان را قلعه کن باز</p></div>
<div class="m2"><p>به ملک خشکی و تری مکن ناز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بکن کاری همین جا تا توانی</p></div>
<div class="m2"><p>که آنجا هم، چو اینجا، ملک رانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مسلم بایدت گر پادشاهی</p></div>
<div class="m2"><p>بباید کردن از دلها گدائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دعا زین به نمیدانم به جایت</p></div>
<div class="m2"><p>که از دلها حشم بخشد خدایت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن تیغ سیاست را چنان تیز</p></div>
<div class="m2"><p>که چون آتش، نداند کرد پرهیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شه آن به کاو عمل چون آب راند</p></div>
<div class="m2"><p>که هم جان بخشد و، هم جان ستاند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی کاو مملکت را بد سگال است</p></div>
<div class="m2"><p>بکش، کان خون، بی حرمت حلال است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به کار دیگران، بر شعله زن آب</p></div>
<div class="m2"><p>خرد بیدار دار و تیغ در خواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو هستندت همه پائین پرستان</p></div>
<div class="m2"><p>زبر دستی مکن بر زیر دستان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رهت چون رفت خلق از دیده در پیش</p></div>
<div class="m2"><p>رهٔ خود را تو روب از دیدهٔ خویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به چندین، مشعل امشب کار ره کن</p></div>
<div class="m2"><p>ره ظلمات فردا را نگه کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازینجا بر چراغی گر توانی</p></div>
<div class="m2"><p>که تا آن‌جا به تاریکی نمانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چراغی نی که باد از وی برد نور</p></div>
<div class="m2"><p>چراغی کان نمیرد از دم صور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مشو مغرور این مشتی خیالات</p></div>
<div class="m2"><p>که در پیش تو می‌آید به حالات</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جهان خوابیست پیش چشم بیدار</p></div>
<div class="m2"><p>به خوابی دل نه بندد مرد هوشیار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو یک ذره غباری از زمینی</p></div>
<div class="m2"><p>که اندر خواب خود را کوه بینی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو بر تو دست تقدیر آورد زور</p></div>
<div class="m2"><p>کنی روشن که جمشیدی و یامور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بخواب اندر مگر موشی شتر شد</p></div>
<div class="m2"><p>ز پری تنش دل نیز پر شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز خواب خوش بر آمد شاد گشته</p></div>
<div class="m2"><p>همی شد سو به سو پر باد گشته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بنا گاه اشتری باری برو ریخت</p></div>
<div class="m2"><p>ز صد من یک جو آزاری برو ریخت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ته آن بار مسکین موش درماند</p></div>
<div class="m2"><p>به مسکینی جمازه در عدم راند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خوش است این خوابهائی خوش به تعبیر</p></div>
<div class="m2"><p>اگر بر عکس ننمایند تأثیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو بازیچه است ملک سست بنیاد</p></div>
<div class="m2"><p>بدین بازیچه چون طفلان مشو شاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نمیگویم که ترک خسروی کن،</p></div>
<div class="m2"><p>رهٔ کم توشگان را پیروی کن،</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو کی این پای ره پیمای داری</p></div>
<div class="m2"><p>که زنجیر زر اندر پای داری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تواین ره کی روی کز ناز و تمکین</p></div>
<div class="m2"><p>زنی ده گام بر یک خشت زرین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به دل اصحاب دل را آشنا باش</p></div>
<div class="m2"><p>درون درویش و بیرون پادشاه باش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به شاهی سهل باشد ملک را نی</p></div>
<div class="m2"><p>به ملک بندگی رس گر توانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه اندک، کارها بسیار کردی</p></div>
<div class="m2"><p>ولی بهر دل خود کارکردی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کنون کار از پی آن کن که هر بار</p></div>
<div class="m2"><p>دهد در کار اندک، مزد بسیار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو توقیعی که اندر پادشاهی است</p></div>
<div class="m2"><p>خلافت نامهٔ ملک خدائی است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ستون ملک نبود پایهٔ تخت</p></div>
<div class="m2"><p>نه چوب چتر باشد عمدهٔ بخت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بسی دیدم کمرهای کریمان</p></div>
<div class="m2"><p>همه در یتیمش از یتیمان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جفای خلق پیش شاه گویند</p></div>
<div class="m2"><p>جفا چون شه کند، داد از که جویند؟</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه هر فرقی سزای تاج شاهی است</p></div>
<div class="m2"><p>نه هر سر لایق صاحب کلاهی است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همه باشند بهر تاج محتاج</p></div>
<div class="m2"><p>یکی را زانهمه روزی شود تاج</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فلک هر لحظه میدوزد کلاهی</p></div>
<div class="m2"><p>کزان تاجی نهد بر فرق شاهی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کسی را تاج زر بر سر دهد زیب</p></div>
<div class="m2"><p>که ناید بر ضعیف از تختش آسیب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>رساند از کف خود جمله را بهر</p></div>
<div class="m2"><p>کز آن پروردهٔ راحت شود دهر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>غم عالم چنان باشد به جانش</p></div>
<div class="m2"><p>که باشد عالمی غم بهر آنش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جهانداری به از عالم ستانی</p></div>
<div class="m2"><p>که از خورشید ناید سائبانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>رعیت چون خلل یابد ز بنیاد</p></div>
<div class="m2"><p>کجا ماند بنای دولت آباد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>رعیت مایهٔ بنیاد مال است</p></div>
<div class="m2"><p>زمال اسباب ملک آماده حال ست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو تیشه بشکند از راندن سخت</p></div>
<div class="m2"><p>نه کرسی ساختن بتوان و نی تخت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کسی کاز بهر تو صد رنج ورزد،</p></div>
<div class="m2"><p>ز تو آخر به یک راحت خیزد؟</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نه شه را از گل دیگر سرشتند</p></div>
<div class="m2"><p>نه نعمت زان او تنها نوشتند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو ماهم گوهریم از یک خزانه،</p></div>
<div class="m2"><p>چرا گنجد تفاوت در میانه ؟؟</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کند شیر، ار بخوردن، بخل گرگی</p></div>
<div class="m2"><p>برو تهمت بود نام بزرگی!</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>درخت ار سایه نبود بر زمینش</p></div>
<div class="m2"><p>چرا خلقی بود سایه نشینش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بداد دست ده، تا صد شود شاد</p></div>
<div class="m2"><p>به دست داد ماند کشور آباد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کند ابری که دایم سایه‌بانی</p></div>
<div class="m2"><p>به از باران که باشد ناگهانی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>فروخوان نامهٔ مظلوم زان پیش</p></div>
<div class="m2"><p>که بینی رو سیه زو نامهٔ خویش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سپید است ار چه ایوان شهنشاه</p></div>
<div class="m2"><p>سیه گردد ز دود تیرهٔ آه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>عنان شاه گر بر آسمان است</p></div>
<div class="m2"><p>دعا را دست بالاتر از آن است</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ته غار اژدهای با چنان زور</p></div>
<div class="m2"><p>شود مسکین چو در چشمش خزد مور</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>توان بی توانان هست چندان</p></div>
<div class="m2"><p>که پیچد سخت دست زورمندان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>پگه خیز است خورشید سمائی</p></div>
<div class="m2"><p>که دارد عالمی زو روشنائی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو سلطان بندگی را پیش گیرد</p></div>
<div class="m2"><p>خدا آن بندگی زو درپذیرد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>وگر شد رسم شاهان جام گلگون</p></div>
<div class="m2"><p>به اندازه نه از اندازه بیرون</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>مبین یک جرعه در طاس شرابی</p></div>
<div class="m2"><p>که طوفان است از بهر خرابی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سرود و لهو هم باید به مقدار</p></div>
<div class="m2"><p>که چون بسیار شد، عکس آورد بار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نشاید تا بدان حد نغمه و نای</p></div>
<div class="m2"><p>که پای تخت هم بر خیزد از جای</p></div></div>