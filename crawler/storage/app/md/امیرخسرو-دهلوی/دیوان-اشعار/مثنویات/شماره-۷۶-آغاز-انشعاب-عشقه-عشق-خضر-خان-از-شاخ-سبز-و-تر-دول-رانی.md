---
title: >-
    شمارهٔ ۷۶ - آغاز انشعاب عشقهٔ عشق خضر خان از شاخ سبز و تر دول رانی
---
# شمارهٔ ۷۶ - آغاز انشعاب عشقهٔ عشق خضر خان از شاخ سبز و تر دول رانی

<div class="b" id="bn1"><div class="m1"><p>همیشه دور چرخ لاجوردی</p></div>
<div class="m2"><p>نداند پیشه‌ای جز ره نوردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دورش هر یکی گردش به کاریست</p></div>
<div class="m2"><p>به ریز هر یکی دیگر شماریست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونی امید پاینده است و نی بیم</p></div>
<div class="m2"><p>خوش آنکس کاونهد گردن به تسلیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو نتوان رشتهٔ گردون گستن</p></div>
<div class="m2"><p>بباید دل درو ناچار بستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه داند طوطی کافتاده در دام</p></div>
<div class="m2"><p>که از شکر دهندش طعمه در کام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه داند باز چون بندند پایش</p></div>
<div class="m2"><p>که دست شاه خواهد بود جایش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دری کو خواست شد بر افسر خاص</p></div>
<div class="m2"><p>رسد در گنج شاه از دست غواص</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خدایا، هر که را نعمت دهی بیش</p></div>
<div class="m2"><p>در آموزش، سپاس نعمت خویش!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنین خواندم در آن دیباچهٔ راز</p></div>
<div class="m2"><p>که هر حرفی ازو می‌کرد صد ناز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که چون شاهنشه جمشید مسند</p></div>
<div class="m2"><p>علاء الدین والد نیا محمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به ملک دهلی از عون الهی</p></div>
<div class="m2"><p>برامد بر سریر پادشاهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سری کز باد کین دیدش خطرناک</p></div>
<div class="m2"><p>باب تیغ کردش طعمهٔ خاک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم اندر هندرایان را رهی کرد</p></div>
<div class="m2"><p>هم از تاتار غزنین را تهی کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>الغخان معظم را بفرمود</p></div>
<div class="m2"><p>که لشکر جانب دریا کشد زود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در آن حد «کرن رای» ای بود با نام</p></div>
<div class="m2"><p>به قدرت کامکار اندر همه کام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ازو رایان ساحل در تف و تاب</p></div>
<div class="m2"><p>روان در بحر و بر فرمانش چون آب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو تیغ افشاند بر وی خان مغفور</p></div>
<div class="m2"><p>ز میدان تیره دل چو سایه از نور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حرمهای مهین رای والا</p></div>
<div class="m2"><p>سرا پا غرقه در لولوی لالا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به دست افتاد با پیل و خزانه</p></div>
<div class="m2"><p>جهانی پر شد از رانی و رانه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بتانی ستاره بدیده نی ماه</p></div>
<div class="m2"><p>نه چشم بد در ایشان یافته راه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سران جمله خوبان گل اندام</p></div>
<div class="m2"><p>پری روئی که «کنولادی» بدش نام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو دیده ز ارجمندی نازنین خوی</p></div>
<div class="m2"><p>چو جان پوشیده از بینندگان روی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرامی آفتابی سایه پرورد</p></div>
<div class="m2"><p>ولی خورشیدش از هیبت شده زرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>امانت دار آن خان جهانگیر</p></div>
<div class="m2"><p>که از عصمت بران آهو نزد تیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به فیروزی چو باز آمد از آن فتح</p></div>
<div class="m2"><p>به پیش تخت شه زد بوسه بر سطح</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به عرض بارگاه آورد در پیش</p></div>
<div class="m2"><p>متاع و پیل و اسپ و زر ز حد بیش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نهانی تحفهٔ کان پیشکش کرد</p></div>
<div class="m2"><p>هم آن نازک تنان ما هوش کرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سران جمله «کنولا دی را نی»</p></div>
<div class="m2"><p>سزای خدمت تخت کیانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنان ماهی و آن انجم به دنبال</p></div>
<div class="m2"><p>به فرمان در حرم رفتند در حال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو آمد در شبستان شه آن شمع</p></div>
<div class="m2"><p>پریشان خاطرش گشت اندکی جمع</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چنان افشرد بهر بندگی پای</p></div>
<div class="m2"><p>که کرد اندر دل شاه جهان جای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کسی کش بخت و دولت پای گیرد</p></div>
<div class="m2"><p>به چشم بختیاران جای گیرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>غرض القصد «کنو لادی رانی»</p></div>
<div class="m2"><p>دو دختر داشت گاه کامرانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو رانی، سوی حضرت شد سبک پای</p></div>
<div class="m2"><p>بماند آن هر دو گوهر در کف «رای»</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنان افتاد حکم ایزد پاک</p></div>
<div class="m2"><p>که شد در بزرگ اندر دل خاک</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دویم را عمر شش مه بود رفته</p></div>
<div class="m2"><p>که بودان شش مهمه ماه دو هفته</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پری روی ز مردم حور زاده</p></div>
<div class="m2"><p>سپهرش نام «دیولدی» نهاده</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو «کنولادی» در را صدف بود</p></div>
<div class="m2"><p>به خدمت پیش شاه بحر کف بود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همی کرد آن چنان خدمت به درگاه</p></div>
<div class="m2"><p>که حاصل می‌شدش خوشنودی شاه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شبی خوش دید دارای زمن را</p></div>
<div class="m2"><p>به عرض آورد راز خویشتن را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که از شاخ جوانی بر درختم</p></div>
<div class="m2"><p>دو غنچه ناشگفته داشت بختم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو زینجا باد اقبال آن طرف تاخت</p></div>
<div class="m2"><p>مرا زانجا ربود این جانب انداخت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شدم من خوش ز بخت روشن خویش</p></div>
<div class="m2"><p>ولی ماند ان دو گل در گلشن خویش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>یکی زان دو سپرد اندر جوانی</p></div>
<div class="m2"><p>پرستاران شه را زندگانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دوم مانده است و، چون پیوند خون است</p></div>
<div class="m2"><p>دل من بهر آن خون، بی‌سکون‌ست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دمی گر مهر شه بر بنده تابد</p></div>
<div class="m2"><p>به گرمی خون به خون پیوند یابد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو شه را در شد این دیباچه در گوش</p></div>
<div class="m2"><p>نموداری دگر رو دادش از هوش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به دل می‌گشت جستن هر زمانش</p></div>
<div class="m2"><p>پرستاری ز بهر خضر خانش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>موافق باز خواندش در دل آن گفت</p></div>
<div class="m2"><p>ستاره خواست تا مه را کند جفت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>برای کار دان فرمان فرستاد</p></div>
<div class="m2"><p>که ما را بخت آگاهی چنان داد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>که داری در سرای دولت خویش</p></div>
<div class="m2"><p>مبارک روی دختی دولت اندیش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو بر طغرای فرمان دیده سائی</p></div>
<div class="m2"><p>ز دو دیده فرست آن روشنائی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که گردد بیت این خورشید معمور</p></div>
<div class="m2"><p>شود روشن شبستانش بدان نور</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سریر آرای ملک هندوان «کرن»</p></div>
<div class="m2"><p>که بد صاحب‌قران «رای» ای در آن قرن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ازین شادی که آمد ناگهانش</p></div>
<div class="m2"><p>نگنجید اندرون پوست جانش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کجا در ذره گنجد این که خورشید</p></div>
<div class="m2"><p>دهد نزد خودش پیوند جاوید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو با چشمه کند بحر آشنائی</p></div>
<div class="m2"><p>شود آن چشمه هم بحر از روانی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بران شد کان طرب را کار سازد</p></div>
<div class="m2"><p>علم بر پشت پیلان بر فرازد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>متاع قیمتی صد پیل بالا</p></div>
<div class="m2"><p>ز دیبا و خز و لولوی لالا</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دگر کالای گوناگون نه چندان</p></div>
<div class="m2"><p>که گنجد در خیال هوشمندان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>پس آن که با هزار امیدواری</p></div>
<div class="m2"><p>نشاند نازنین را در عماری</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>فرستد سوی دولت خانهٔ تخت</p></div>
<div class="m2"><p>که آن دولت رسد در خانهٔ بخت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>درین اثنا چنان شد شاه را رای</p></div>
<div class="m2"><p>که بستاند از آن رای «کرن» جای</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بران سو نامزد گشتند در دم</p></div>
<div class="m2"><p>الفغان معظم پنجمین هم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>امیران دگر باجیش و انبوه</p></div>
<div class="m2"><p>که از پامال اسپان سرمه شد کوه</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو در «گجرات» رفت آن لشکر سخت</p></div>
<div class="m2"><p>بخاک افگند رای کاردان رخت</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو آنجا، نی صلاح جان و تن دید</p></div>
<div class="m2"><p>هزیمت را سلاح خویشتن دید</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نبرد از هم دمان و خون و پیوند</p></div>
<div class="m2"><p>به جز خاص شبستان لعبتی چند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نهان از دیدهٔ مردم پری وار</p></div>
<div class="m2"><p>بسوی «دیوگیر» افگند رهوار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>رسید انجا و گشت ایمن ز خون‌ریز</p></div>
<div class="m2"><p>عنان را نرم کرد از جنبش تیز</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو «سنکهن دیو» پور رای رایان</p></div>
<div class="m2"><p>بشد آگاه ز آگاهی سرایان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>که «گرن» از «گوجرات» آمد برین سوی</p></div>
<div class="m2"><p>ز تاب تیغ ترکان تافته روی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به پرده دختری دارد نهفته</p></div>
<div class="m2"><p>گلی پوشیده روی ناشگفته</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>لطافت مایه‌ای چون آب باران</p></div>
<div class="m2"><p>سزای تخت گاه تاجداران</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>طمع در بست «سنگهن» تا به صد جهد</p></div>
<div class="m2"><p>برد در برج خویش آن ماه را مهد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>برادر را که «بهلیم» بود نامش</p></div>
<div class="m2"><p>بخواند و گرد حمال پیامش</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بران سو رفت «بهلیم دیو» چون باد</p></div>
<div class="m2"><p>به مهمان راز مهمانی برون داد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو «کرن» از ردهٔ بخت پریشان</p></div>
<div class="m2"><p>حمایت جوی بود از سوی ایشان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نیارست اندران پیغام نه کرد</p></div>
<div class="m2"><p>ضرورت باز حل پیوند مه کرد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>فرستادند بر بومی همای</p></div>
<div class="m2"><p>مه روشن به کام اژدهائی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چو یک فرسنگ ماند اندر تگاپوی</p></div>
<div class="m2"><p>که اندر «دیو گیر» آرد پری روی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>سپاه شه که بود اندر پی «کرن»</p></div>
<div class="m2"><p>که کردی در زمانی کار یک قرن</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چو باد تند زد ناگه بر ایشان</p></div>
<div class="m2"><p>همه جمعیت خس شد پریشان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به کوه و دشت سر زد «کرن» سر کش</p></div>
<div class="m2"><p>سپاهی در عقب چون کوه آتش</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چنان بگرفت زاندیشه سر خویش</p></div>
<div class="m2"><p>که چون اندیشه نا پیدا شد از پیش</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>در آن جنبش «دولرانی» که بختش</p></div>
<div class="m2"><p>بری میخواست چیدن از درختش</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>دوان می‌شد به پشت باد پائی</p></div>
<div class="m2"><p>چو گل کش باد بر گیرد ز جائی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>به پیکان گوش او کز اوج و از پست</p></div>
<div class="m2"><p>بسان تیر می‌شد شست در شست</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>غرض ناگه رسید از غیب تیری</p></div>
<div class="m2"><p>که تیر چرخ زان بر زد نفیری</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بماند آن رخش آتش پای سرکش</p></div>
<div class="m2"><p>گرفت ماه شد در برج آتش</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>الغفان در حرم میداشت مستور</p></div>
<div class="m2"><p>چو فرزند خودش در پردهٔ نور</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چو فرمان شد که آن ریحان فردوس</p></div>
<div class="m2"><p>به شهر آرند چون برجیس در قوس</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>رسانیدند در ایوان جمشید</p></div>
<div class="m2"><p>به جلباب حیا پوشیده خورشید</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>کنون بین کاختر هر هفت کرده</p></div>
<div class="m2"><p>چها بیرون دهد از هفت پرده</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بیا مطرب بساز ابریشمی چنگ</p></div>
<div class="m2"><p>برین شادی که آمد دوست در چنگ</p></div></div>