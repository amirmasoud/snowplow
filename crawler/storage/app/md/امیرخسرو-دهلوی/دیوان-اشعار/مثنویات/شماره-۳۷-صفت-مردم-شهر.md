---
title: >-
    شمارهٔ ۳۷ - صفت مردم شهر
---
# شمارهٔ ۳۷ - صفت مردم شهر

<div class="b" id="bn1"><div class="m1"><p>مردم او جمله فرشته سرشت</p></div>
<div class="m2"><p>خوش دل و خوش خوی چو اهل بهشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه ز صنعت به همه عالم است</p></div>
<div class="m2"><p>هست در ایشان و زیادت هم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیشتراز علم و ادب بهره مند</p></div>
<div class="m2"><p>اهل سخن خود که شمارد که چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر طرفی سحر بیانی نوست</p></div>
<div class="m2"><p>ریزهٔ چین کمتر شان خسروست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پنج هزار از ملک نامدار</p></div>
<div class="m2"><p>لشکر شان بیشتر از صد هزار</p></div></div>