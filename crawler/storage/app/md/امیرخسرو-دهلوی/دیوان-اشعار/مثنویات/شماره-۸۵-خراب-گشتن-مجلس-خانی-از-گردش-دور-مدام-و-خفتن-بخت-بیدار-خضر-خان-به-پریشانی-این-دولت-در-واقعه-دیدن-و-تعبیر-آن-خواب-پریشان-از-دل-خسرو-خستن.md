---
title: >-
    شمارهٔ ۸۵ - خراب گشتن مجلس خانی از گردش دور مدام، و خفتن بخت بیدار خضر خان، به پریشانی این دولت در واقعه دیدن و تعبیر آن خواب پریشان از دل خسرو خستن
---
# شمارهٔ ۸۵ - خراب گشتن مجلس خانی از گردش دور مدام، و خفتن بخت بیدار خضر خان، به پریشانی این دولت در واقعه دیدن و تعبیر آن خواب پریشان از دل خسرو خستن

<div class="b" id="bn1"><div class="m1"><p>بسی دیدم درین گردنده دولاب</p></div>
<div class="m2"><p>ندیدم هیچ دورش بر یکی آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر خورشید این ساعت بلند است</p></div>
<div class="m2"><p>زمان دیگر از پستی نژند است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکن تکیه به صد رو مسند و تخت</p></div>
<div class="m2"><p>خس است این جمله چون بادی وزو سخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز تاراج سپهر دون بیندیش</p></div>
<div class="m2"><p>که صد شه را کند یک لحظه درویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علمهای جهان بر عکس هم هست</p></div>
<div class="m2"><p>که بر ملکی گدائی را دهد دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون از سینه بیرون ریزم این جوش</p></div>
<div class="m2"><p>که روشن شد هم از دیده هم از گوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که چون شه را به شخص ناز پرورد</p></div>
<div class="m2"><p>رسید از تند باد آسمان گرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تغیر یافت ره اندر مزاجش</p></div>
<div class="m2"><p>نشستند اهل دانش در علاجش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به تب لرزه شده خور زان تب نرم</p></div>
<div class="m2"><p>که آن خورشید را اندام شد گرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنانش در جگر ره یافت آزار</p></div>
<div class="m2"><p>کز آزارش جگر گوشه شد افگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خضر خان کو نهالی بود زان باغ</p></div>
<div class="m2"><p>چو لاله داشت زان غم بر جگر داغ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به رسم نذر گفت ار به شود شاه</p></div>
<div class="m2"><p>پیاده در زیارتها کنم راه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز نذرش لختی از شه رفت سستی</p></div>
<div class="m2"><p>پدید آمد نشان تندرستی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روان گشت آن مهین سر بلندان</p></div>
<div class="m2"><p>پیاده سوی «هتنا پور» خندان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو او پای بلورین سود بر خاک</p></div>
<div class="m2"><p>ستاره خواست زیر افتد ز افلاک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ملوک از باد بر خاک اوفتادند</p></div>
<div class="m2"><p>به همراهی در آن ره رو نهادند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه گلها به پای سرو خفتند</p></div>
<div class="m2"><p>طریق مصلحت راباز گفتند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به غلطیدند پیش راهوارش</p></div>
<div class="m2"><p>که تا کردند بر مرکب سوارش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روان شد سوی «هتناپور» پویان</p></div>
<div class="m2"><p>به صد خواهش حیات شاه جویان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که چون عزم زیارت کرد چون تیر</p></div>
<div class="m2"><p>نشد بهر زیارت جانب پیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نرفت آن سو گهٔ باز آمدن نیز</p></div>
<div class="m2"><p>که پوشید آسمانش چشم تمیز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو بر رویش قضا می‌خواست گردی</p></div>
<div class="m2"><p>نبردش در پناه نیک مردی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مخالف کاو محل میخواست خالی</p></div>
<div class="m2"><p>چو خالی دید کرد آفت سگالی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به فتنه راست کرد اندیشهٔ خویش</p></div>
<div class="m2"><p>به حضرت رفت بی اندیشه در پیش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برون داد آن چنان راز نهان را</p></div>
<div class="m2"><p>که باور شد دل شاه جهان را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>الپخان را گوزنی ساخت با شیر</p></div>
<div class="m2"><p>زد اول نیش وانگه راند شمشیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو از کار الپخان سینه پرداخت</p></div>
<div class="m2"><p>سبک تدبیر کار خضر خان ساخت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ستد فرمانی از فرماندهٔ دهر</p></div>
<div class="m2"><p>چو ماری هر خطش دیباچهٔ زهر</p></div></div>