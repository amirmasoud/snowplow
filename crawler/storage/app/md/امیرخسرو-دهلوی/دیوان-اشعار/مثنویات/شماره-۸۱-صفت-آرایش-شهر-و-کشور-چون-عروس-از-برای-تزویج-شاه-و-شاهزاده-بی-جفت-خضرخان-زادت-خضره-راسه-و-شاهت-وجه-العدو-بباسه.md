---
title: >-
    شمارهٔ ۸۱ - صفت آرایش شهر و کشور، چون عروس، از برای تزویج شاه و شاهزادهٔ بی جفت، خضرخان، زادت خضره راسه، و شاهت وجه العدو بباسه!
---
# شمارهٔ ۸۱ - صفت آرایش شهر و کشور، چون عروس، از برای تزویج شاه و شاهزادهٔ بی جفت، خضرخان، زادت خضره راسه، و شاهت وجه العدو بباسه!

<div class="b" id="bn1"><div class="m1"><p>زهی بستان آن شه را جمالی</p></div>
<div class="m2"><p>که باشد چون خضر خانش نهالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو الهام الهی شاه را گفت</p></div>
<div class="m2"><p>که آن در سعادت را کند جفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشارت کرد تا در گردش دهر</p></div>
<div class="m2"><p>بیارایند یک سر کشور و شهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمر بر بست در کارش زمانه</p></div>
<div class="m2"><p>به خرج آمد خزانه در خزانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگرداگرد قصر پادشاهی</p></div>
<div class="m2"><p>برآمد قبه از مه تا به ماهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان از قبه‌های کارداران</p></div>
<div class="m2"><p>شده چون روی دریا روز باران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر جانب که مردم بر زمین رفت</p></div>
<div class="m2"><p>همه بر فرش دیباهای چین رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بس شارع که خفت اندر خز ناب</p></div>
<div class="m2"><p>زمین را کس نه دید الا که در خواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز هر سو خاسته غلغل بران سان</p></div>
<div class="m2"><p>که گشته شهر سلطان شهر یزدان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دهل در بانگ و رخشان پیش او تیغ</p></div>
<div class="m2"><p>چو بانگ رعد و رخش برق در میغ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر آواز دهل مرد سلح کار</p></div>
<div class="m2"><p>معلق زن به نوبت نوبتی دار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رسن باز آن به بالای رسنها</p></div>
<div class="m2"><p>چو دلها گیسوان را در شکنها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه با آن حبل پیچان کرده بازی</p></div>
<div class="m2"><p>که خود با رشتهٔ جان کرده بازی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرو برده مشعبد تیغ چون آب</p></div>
<div class="m2"><p>چو مستسقی که نوشد شربت ناب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نموده چهره با زان گونه گون ریو</p></div>
<div class="m2"><p>گهی خود را پری کرده گهی دیو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز دهر آموخته گوئی دو رنگی</p></div>
<div class="m2"><p>که گه رو می‌نماید گاه زنگی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو شاه سازها چنگست ز آهنگ</p></div>
<div class="m2"><p>بزه بر بسته ده جا تیر را چنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز یک ساقش شده مو تا زمین پست</p></div>
<div class="m2"><p>دگر ساقیش بی مو چون کف دست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دف از دیوار خود حصن حصین است</p></div>
<div class="m2"><p>حصار چوب و صحن کاغذین است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نگر در چنگ و بر بط فرق روشن</p></div>
<div class="m2"><p>که هست آن سر بزرگ و این فروتن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نواگر کاسهٔ طنبور حالی</p></div>
<div class="m2"><p>به غایت کاسه‌ای پر لیک خالی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گران سر از کدوی خویش طنبور</p></div>
<div class="m2"><p>فرو غلطیده نی مست و نه مخمور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به رسم هند گوناگون مزامیر</p></div>
<div class="m2"><p>به جانها بسته اشکال از بم و زیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دگر ساز برنجین نام آن «تال»</p></div>
<div class="m2"><p>بر انگشت پری رویان قتال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دو روئین تن که روباروی در حرب</p></div>
<div class="m2"><p>چو دف در پارسی میزان هر ضرب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کشیده تنبک هندی، فغانی</p></div>
<div class="m2"><p>شده تنبک زنش، چون ترجمانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خمیر خام، کش بر روز ده پست</p></div>
<div class="m2"><p>نموده صد دقیقه پخته هر دست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عجب رود از کمین دندان نموده</p></div>
<div class="m2"><p>لبش نی و دهن خندان نموده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پری رویان هندی جادوی ساز</p></div>
<div class="m2"><p>ز لب کرده در دیوانگی باز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گرفته چون پیاله تال در دست</p></div>
<div class="m2"><p>نه از می کز سرود خویشتن مست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سرود دلکش از لبهای خوبان</p></div>
<div class="m2"><p>شتابان سوی گردون پای کوبان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به رقص و جست خوبان هوا باز</p></div>
<div class="m2"><p>نهاده پای بر بالای آواز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پرنده همچو طاوسان والا</p></div>
<div class="m2"><p>معلق زن کبوتر سان به بالا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بجستن فرق شان گشته فلک سای</p></div>
<div class="m2"><p>بگاه رقص بیزار از زمین پای</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بهر چشمک زدن کشته جوانی</p></div>
<div class="m2"><p>بهر خنده زدن بربوده جانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خیال زلف شان در جان یاران</p></div>
<div class="m2"><p>چو شام اندر خیال روزه داران</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز زلف افگنده تا پا دام عشاق</p></div>
<div class="m2"><p>بران پا دام بسته ماهی ساق</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو شد عالم همه در زیور و زیب</p></div>
<div class="m2"><p>کلاه قبه‌ها با مه زد آسیب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اشارت شد ز در گه کاهل تقویم</p></div>
<div class="m2"><p>شمارند اختیاری را به تنجیم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مه روزه دراز درجک برون داد</p></div>
<div class="m2"><p>چو روز از مطلع دولت شد آباد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کشاده گویم این تاریخ ابجد</p></div>
<div class="m2"><p>به سال یازده از بعد هفصد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به روز چارشنبه مه سه و بیست</p></div>
<div class="m2"><p>ز روزه خلق اندر بهترین زیست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به ترتیب آن چنان کاقبال می‌خواست</p></div>
<div class="m2"><p>نشستند اهل اقبال از چپ و راست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بهر کس هدیه دادند از خزائن</p></div>
<div class="m2"><p>خراج مصر و محصول مدائن</p></div></div>