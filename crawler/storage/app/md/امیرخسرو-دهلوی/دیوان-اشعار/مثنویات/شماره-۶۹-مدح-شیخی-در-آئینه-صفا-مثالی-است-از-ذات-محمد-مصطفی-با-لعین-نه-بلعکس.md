---
title: >-
    شمارهٔ ۶۹ - مدح شیخی در آئینهٔ صفا مثالی است از ذات محمد مصطفی با لعین نه بلعکس
---
# شمارهٔ ۶۹ - مدح شیخی در آئینهٔ صفا مثالی است از ذات محمد مصطفی با لعین نه بلعکس

<div class="b" id="bn1"><div class="m1"><p>پس از دیباچهٔ نعت رسالت</p></div>
<div class="m2"><p>ز ذکر پیر به باشد مقالت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظام الدین حق فرخنده نامی</p></div>
<div class="m2"><p>که دین حق گرفت از وی نظامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خطابش راست دو نقطه فرو خوان</p></div>
<div class="m2"><p>نشان نقطهای انبیاء دان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حدیثش چون خبر در امر و در نهی</p></div>
<div class="m2"><p>به یک پایه فرود از پایهٔ وحی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سریر آرای فقر از صف ابرار</p></div>
<div class="m2"><p>سریر مصطفی را عمدهٔ کار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ضمیرش محرم دیرینهٔ عشق</p></div>
<div class="m2"><p>نیاز خازن گنجینهٔ عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلش کز شوق دارد در دو داغی</p></div>
<div class="m2"><p>رواق قدس را روشن چراغی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی کو صوف او در بر گرفته</p></div>
<div class="m2"><p>قضا از وی قلم را بر گرفته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خدایا آن گزیده بندهٔ خاص</p></div>
<div class="m2"><p>که هست الحمد لله جفت اخلاص</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به قربت، هم نشین مصطفی باد</p></div>
<div class="m2"><p>در آن قرب، ایستادش بهر ما باد</p></div></div>