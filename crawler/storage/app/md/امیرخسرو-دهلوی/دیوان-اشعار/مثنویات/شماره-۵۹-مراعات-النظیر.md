---
title: >-
    شمارهٔ ۵۹ - مراعات النظیر
---
# شمارهٔ ۵۹ - مراعات النظیر

<div class="b" id="bn1"><div class="m1"><p>آهوی مشکین و سرش باد شاخ</p></div>
<div class="m2"><p>وز دم او مشک به صحرا فراخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم چو بر گلشن بختش فتاد</p></div>
<div class="m2"><p>گشت پیاده چو گل از پشت باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی چو گل بود به پشت زمین</p></div>
<div class="m2"><p>گشت زمین پر سمن و یاسمین</p></div></div>