---
title: >-
    شمارهٔ ۹۴ - اثبات ملک هند به حجت که جنت است حجت همه به قاعدهٔ عقل استوار
---
# شمارهٔ ۹۴ - اثبات ملک هند به حجت که جنت است حجت همه به قاعدهٔ عقل استوار

<div class="b" id="bn1"><div class="m1"><p>کشور هند است بهشتی به زمین</p></div>
<div class="m2"><p>حجتش اینک به رخ صفحه ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حجت ثابت چو در آن نیست شکی</p></div>
<div class="m2"><p>هفت بگویم به درستی نه یکی:</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اولش این است که آدم به جنان</p></div>
<div class="m2"><p>چون ز عصی خستگی‌ای یافت چنان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زخم عصی خورد بد انسان ز کمین</p></div>
<div class="m2"><p>کز فلک افتاد به سختی به زمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عصمت حق داشت همی چون نگهش</p></div>
<div class="m2"><p>خارهٔ کهسار شد اطلس به تهش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمدن از خلد به هندش بد از آن</p></div>
<div class="m2"><p>کان گل جنت که زدش باد خزان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر به خراسان و عرب تازی و چین</p></div>
<div class="m2"><p>یک نفسی بهره گرفتی به زمین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرمی و سردی خراسان و عرب</p></div>
<div class="m2"><p>وان به ری و چین عذابیست عجب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زو شده پرورده به فردوس درون</p></div>
<div class="m2"><p>چونش بودی طاقت این دیده و خون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گشت محقق چو چنین وصف متین</p></div>
<div class="m2"><p>کاین حد هند است به فردوس برین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هند چو از خلد نشان بود درو</p></div>
<div class="m2"><p>ز امر خدایش قدم آسود درو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور نه بدان نازکی از جای دگر</p></div>
<div class="m2"><p>آمدی از رنج فتادی به ضرر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حجت دیگر که ز طاووس کشم</p></div>
<div class="m2"><p>مرغ خرد را به زمین بوس کشم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر نه بهشت است همین هند چرا</p></div>
<div class="m2"><p>از پی طاووس جنان گشت سرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیست چنین طایر فردوسی اگر</p></div>
<div class="m2"><p>بویی از باغ بدی جای دگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لابد ازین جای بدان جای شدی</p></div>
<div class="m2"><p>وز پی رفتن همه تن پای شدی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بود همین جا چوز فردوس اثری</p></div>
<div class="m2"><p>جانب دیگر نفتادش گذری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حجتم اینست سوم گر به شکی</p></div>
<div class="m2"><p>کامدن مار ز باغ فلکی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بود به همراهی طاوس وصفی</p></div>
<div class="m2"><p>قصه چنین گفت فقیه حنفی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لیک جز از هند دگر یافت محل</p></div>
<div class="m2"><p>زانکه همه نیش زدن داشت عمل</p></div></div>