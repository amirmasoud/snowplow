---
title: >-
    شمارهٔ ۹۲ - گزیدهٔ از سپهر اول
---
# شمارهٔ ۹۲ - گزیدهٔ از سپهر اول

<div class="b" id="bn1"><div class="m1"><p>دگر گفت کامروز در هر دیار</p></div>
<div class="m2"><p>غزل کوی گشته ست بیش از شمار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه کس به یک قسم درمانده‌اند</p></div>
<div class="m2"><p>ز قسم دگر بی خبر مانده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندانیم کس را به طبع و سرشت</p></div>
<div class="m2"><p>که یک شعر تحقیق داند نوشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر گفت: سعدی نه از کس کم است</p></div>
<div class="m2"><p>که موج غزل هاش در عالم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دگر گفت: کزوی شناسی به است</p></div>
<div class="m2"><p>که بت سوزی از بت شناسی به است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر گفت: کز راه خوانندگی</p></div>
<div class="m2"><p>زند هر کسی لاف دانندگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولی ما کسی را سخن در نهیم</p></div>
<div class="m2"><p>کزو مایه صد گونه گوهر نهیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر اوضاع ابداع قادر بود</p></div>
<div class="m2"><p>صدور حکم را مصادر بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرش نظم وگر نثر باید نگاشت</p></div>
<div class="m2"><p>نگارد بدان سان که باید نگاشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به مطبوع و مصنوع جادو بود</p></div>
<div class="m2"><p>دقایق درو موی در مو بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه نو کند سبک‌های سخن</p></div>
<div class="m2"><p>که کرباس نو به ز خز کهن!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو هر کس به مقدار خود گفت چیز</p></div>
<div class="m2"><p>در افشان شد از لب جهان شاه نیز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که از نکته بیزان دانش سکال</p></div>
<div class="m2"><p>بدین گونه ما را رسیده ست حال:</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که در عهد خود هر سخن گستری</p></div>
<div class="m2"><p>که خاص کسی بود در کشوری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به مقدار ترتیب گفتار خویش</p></div>
<div class="m2"><p>مثالی که بست از نمودار خویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو منعم سخن را خریدار بود</p></div>
<div class="m2"><p>سخن لاجرم نیز بسیار بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به قیمت خریدند حرف سیاه</p></div>
<div class="m2"><p>بهای شبه گوهر آمد ز شاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نمطهای خاقانی مدح سنج</p></div>
<div class="m2"><p>نه پنهانست کش چون فشاندند گنج</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همان عنصری کاو سخن پیش برد</p></div>
<div class="m2"><p>بهر نظم صد بدره زر بیش برد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مثل شد ز فردوسی نامدار</p></div>
<div class="m2"><p>به شهنامه گنجینهٔ سهل بار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو این بود رسم گران‌مایگان</p></div>
<div class="m2"><p>که دادند گنجی بهر شایگان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه مازان بزرگان به همت کمیم!</p></div>
<div class="m2"><p>کز ایشان علم بود ما عالمیم!</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خدا داده زان‌ها که در عالم است</p></div>
<div class="m2"><p>به گنجینهٔ ما چه مایه کم است؟؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به خواهنده بخشش چرا کم دهیم؟!</p></div>
<div class="m2"><p>اگر دست شد، هر دو عالم دهیم!</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نبوده است شاهی به زیر فلک</p></div>
<div class="m2"><p>که ده لک دهد سکه یابیست لک</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نخست آن جهان شاه داد این صلا</p></div>
<div class="m2"><p>که او بود دنیا و دین را علاء</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دهش بیش از اندازه زو گشت عام</p></div>
<div class="m2"><p>ولیکن شد از من که قطبم تمام!</p></div></div>