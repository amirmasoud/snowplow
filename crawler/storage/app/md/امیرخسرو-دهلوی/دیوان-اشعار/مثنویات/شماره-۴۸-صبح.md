---
title: >-
    شمارهٔ ۴۸ - صبح
---
# شمارهٔ ۴۸ - صبح

<div class="b" id="bn1"><div class="m1"><p>چون دل شب حاملهٔ مهر گشت</p></div>
<div class="m2"><p>بر شب حامل مه کامل گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حامل یک ماهه نه بل یکشبه</p></div>
<div class="m2"><p>تاجوری زاد در آن کوکبه</p></div></div>