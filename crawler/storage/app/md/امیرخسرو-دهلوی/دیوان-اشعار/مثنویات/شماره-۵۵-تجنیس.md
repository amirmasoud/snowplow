---
title: >-
    شمارهٔ ۵۵ - تجنیس
---
# شمارهٔ ۵۵ - تجنیس

<div class="b" id="bn1"><div class="m1"><p>باش به کامم که به کام توام</p></div>
<div class="m2"><p>زنده و نازنده به نام توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حکمت و حکمش که ندارد زوال</p></div>
<div class="m2"><p>هم ز خلل خالی و هم از خیال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر در تو آمده‌ام شرمسار</p></div>
<div class="m2"><p>از شر من در گذر و در گزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشتر پویندهٔ پولاد پای</p></div>
<div class="m2"><p>کوه نما از تن کوهان نمای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابر شده کوه بلند از شکوه</p></div>
<div class="m2"><p>برق شده بر سر او تیغ کوه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب معانی ز دلم زاد زود</p></div>
<div class="m2"><p>آتش طبعم به قلم داد دود</p></div></div>