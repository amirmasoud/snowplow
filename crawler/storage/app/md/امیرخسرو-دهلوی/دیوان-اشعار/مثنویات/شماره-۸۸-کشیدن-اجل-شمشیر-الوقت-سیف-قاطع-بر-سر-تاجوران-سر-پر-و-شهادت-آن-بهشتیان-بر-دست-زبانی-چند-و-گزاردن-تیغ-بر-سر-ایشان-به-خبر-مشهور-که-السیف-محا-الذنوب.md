---
title: >-
    شمارهٔ ۸۸ - کشیدن اجل، شمشیر الوقت سیف قاطع، بر سر تاجوران سر پر، و شهادت آن بهشتیان بر دست زبانی چند، و گزاردن تیغ بر سر ایشان به خبر مشهور، که «السیف محاء الذنوب»
---
# شمارهٔ ۸۸ - کشیدن اجل، شمشیر الوقت سیف قاطع، بر سر تاجوران سر پر، و شهادت آن بهشتیان بر دست زبانی چند، و گزاردن تیغ بر سر ایشان به خبر مشهور، که «السیف محاء الذنوب»

<div class="b" id="bn1"><div class="m1"><p>شراب عشق بازان آب تیغ است</p></div>
<div class="m2"><p>بهر عاشق چنین آبی دریغ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شنیدی قصه یوسف که تا چون</p></div>
<div class="m2"><p>بتان را در دست شوند از خون؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زنی کان حسن را نظاره کرده</p></div>
<div class="m2"><p>ترنجش بر کف و کف پاره کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عروسانی که حسن شه پسندند</p></div>
<div class="m2"><p>حنا بر دست خود زینگونه بندند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه داغست این، که هر جا، می نشانم</p></div>
<div class="m2"><p>چه خونست این، که هر سو، می فشانم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی روشن کند این آتشین سوز</p></div>
<div class="m2"><p>که روزی سوخته باشد بدین روز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه هر دل داند این داغ نهان را</p></div>
<div class="m2"><p>نه هر کس پی فتد این سوز جان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی کاگاه شد زین قصهٔ درد</p></div>
<div class="m2"><p>ز هر حرفی به سینه دشنه‌ای خورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بر عاشق اشارت تیغ خون است</p></div>
<div class="m2"><p>سیاست کردن از رحمت برون است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خضر خانی که چون وحش شکاری</p></div>
<div class="m2"><p>ز غمزه داشت در جان زخم کاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشستی عاقبت زان زخم دلدوز</p></div>
<div class="m2"><p>به روز ماتم خود بهترین روز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه حاجت بود چرخ بی‌وفا را؟</p></div>
<div class="m2"><p>برو راندن، ز خون، تیغ جفا را؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ولیکن چون چنانش بود تقدیر</p></div>
<div class="m2"><p>گسستن کی تواند بسته زنجیر!</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مع القصه، نهانی دان این راز</p></div>
<div class="m2"><p>ز گنج راز زینسان در کنند باز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که چون سلطان مبارک شاه بی مهر</p></div>
<div class="m2"><p>ز تلخی، گشت، بر خویشان، ترش چهر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صلاح ملک در خونریز شان دید</p></div>
<div class="m2"><p>سزاواری به تیغ تیزشان دید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بران شد تا کند از کین سگالی</p></div>
<div class="m2"><p>ز انباز، آن ملک، اقلیم خالی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نهان سوی خضر خان کس فرستاد</p></div>
<div class="m2"><p>نموداری به عذر از دل برون داد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که ای شمعی ز مجلس دور مانده</p></div>
<div class="m2"><p>تنت بی‌تاب و رخ بی نور مانده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو میدانی که از من نیست این کار،</p></div>
<div class="m2"><p>ستم کش ماند و یکسو شد ستمکار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کنون ما هم در آن هنجار کاریم</p></div>
<div class="m2"><p>به هنجاری ازین بندت براریم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو در خوردی، که باشی مسند آرای،</p></div>
<div class="m2"><p>بر اقلیمی کنیمت کار فرمای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ولی مهر کسی کاندر دلت رست</p></div>
<div class="m2"><p>نه در خورد علو همت تست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>«دول رانی» که در پیشت کنیزیست</p></div>
<div class="m2"><p>کنیز ارمه بود، هم سهل چیزیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شنیدم کانچنان گشت ارجمندت</p></div>
<div class="m2"><p>که شد پابوس او سرو بلندت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه بس زیبا بود، کز چشم کوتاه،</p></div>
<div class="m2"><p>پرستار پرستاری شود شاه!</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کدو در صحن بستان کیست، باری؟</p></div>
<div class="m2"><p>که جوید سر بلندی با چناری؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تمنای دل ما میکند خواست</p></div>
<div class="m2"><p>که زان زانو نشین بربایدت خاست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو زینجا رفت، باز اینجا فرستش،</p></div>
<div class="m2"><p>به پائین گاه تخت ما فرستش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو سودای دلت کم گشت چیزی</p></div>
<div class="m2"><p>دهیمت باز تا باشد کنیزی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو شد پیغام گوئی، برد پیغام</p></div>
<div class="m2"><p>خضر خان را نماند اندر دل آرام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز خشم و غصه کرد آن ماه در سلخ</p></div>
<div class="m2"><p>چو می، هم گریه و هم خنده تلخ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نخست از دیده لب را جوش خون داد</p></div>
<div class="m2"><p>پس آلوده به خون پاسخ برون داد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که شه را، ملک رانی چون وفا کرد،</p></div>
<div class="m2"><p>دولرانی به من باید رها کرد!</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو دولت دور گشت از خانی من</p></div>
<div class="m2"><p>دولرانی است دولت رانی من</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در این دولت هم از من دور خواهی،</p></div>
<div class="m2"><p>مرا بی دولت و بی نور خواهی!</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو با من همسر است این یار جانی،</p></div>
<div class="m2"><p>سر من دور کن، زان پس تو دانی!</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پیام‌آور چو زان جان غم اندود</p></div>
<div class="m2"><p>به برج شاه برد آن آتشین دود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شهنشه گرم گشت از پای تا فرق</p></div>
<div class="m2"><p>به گرمی، خیره خندی کرد، چون برق</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>برآمد شعلهٔ کین را زبانه</p></div>
<div class="m2"><p>بهانه جوی را نوشد بهانه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به تندی سر سلاحی را طلب کرد</p></div>
<div class="m2"><p>که باید صد کروه امروز شب کرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رو اندر «گوالیر» این دم، نه بس دیر</p></div>
<div class="m2"><p>سر شیران ملک افگن به شمشیر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که من ایمن شوم ز انبازی ملک</p></div>
<div class="m2"><p>که هست این فتنه کمتر بازی ملک</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به فرمان شد روان، مرد ستمگار</p></div>
<div class="m2"><p>کبوتر پای بند و جره ناهار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شبا روزی برید آن چند فرسنگ</p></div>
<div class="m2"><p>رسید و بر ز بر کرد، از ته، آهنگ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>رسانید آنچه فرمان بودش از تخت</p></div>
<div class="m2"><p>بشد اهل قلعه در کاری چنان سخت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>درون رفتند سرهنگان بی‌باک</p></div>
<div class="m2"><p>به بی‌باکی در آن عصمت گهٔ پاک</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>برو پوشیدگان، هوئی در افتاد</p></div>
<div class="m2"><p>کزان هو، لرزه در بام و در افتاد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در آن برج، از شغب هر تیر شد قوس</p></div>
<div class="m2"><p>قیامت، میهمان آمد به فردوس</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز کنج حجرها با صد نژندی</p></div>
<div class="m2"><p>برون جستند نر شیران به تندی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز باز و و زور، و از تن، تاب رفته</p></div>
<div class="m2"><p>توان مرده خرد در خواب رفته</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شد اندر غصه شادی خان والا</p></div>
<div class="m2"><p>مدد جست از پناه حق تعالی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سبک در کوتوال آویخت تا دیر</p></div>
<div class="m2"><p>ته افگندش، بکشتن جست شمشیر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو شمشیر ظفر گم گشته پودش</p></div>
<div class="m2"><p>از آن نیروی بی حاصل، چه سودش؟</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>عوانان در دویدند از چپ و راست</p></div>
<div class="m2"><p>در افتادند وان افتاده بر خاست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زهی سگساری چرخ زبون‌گیر</p></div>
<div class="m2"><p>که شیران راسگان سازند نخچیر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو بستند آن دو دولتمند را سخت</p></div>
<div class="m2"><p>زمانه بست دست دولت و بخت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>فتادند آن شگر فان در زبونی</p></div>
<div class="m2"><p>برام‌د سو به سو شمشیر خونی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو جست آواز بی رحمی زخنجر</p></div>
<div class="m2"><p>درآمد خونی بی رحمت از در</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>عفا الله بر چنان روهای چون ماه</p></div>
<div class="m2"><p>کسی چون بر کند شمشیر کین خوا!</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کرا در دل نیاید سو ز جانی</p></div>
<div class="m2"><p>ز افسوس چنان عمر و جوانی؟!</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>فلک را باد یارب سینه صد چاک!</p></div>
<div class="m2"><p>کزینسان ارجمندان را کند خاک!</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>غرض کس را برایشان چون نشد رای</p></div>
<div class="m2"><p>که گردد تیغ خون را کار فرمای</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بجنبید از میان چون تند بادی</p></div>
<div class="m2"><p>فروتر نسبتی هندو نژادی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>غم افزائی، چو عیش تنگ حالان</p></div>
<div class="m2"><p>کژ اندیشی، چو عقل خردسالان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>درازش سبلتی پیچیده بر گوش</p></div>
<div class="m2"><p>ز سبلت کرده خود را حلقه در گوش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سبک زان صف سرهنگان برون جست</p></div>
<div class="m2"><p>تو گوئی خواهد از وی موج خون جست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز راه مهر دامن در کشیده</p></div>
<div class="m2"><p>به خونریز آستینها بر کشیده</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ز فرماینده، تیغ گوهرین جست</p></div>
<div class="m2"><p>کشید و کرد دامان قبا چست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>برآمد گرد آن سرو گرامی</p></div>
<div class="m2"><p>که از سر سبزی خود بود نامی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>شهادت خاست از خضر اندران کاخ</p></div>
<div class="m2"><p>چو تسبیح درخت از سبزی شاخ</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>از آن بانگ شهادت کامد از شاه</p></div>
<div class="m2"><p>شهادت گوئی شد مهر و هم ماه</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سپر می‌کرد خورشید از تن خویش</p></div>
<div class="m2"><p>ولی تقدیر یکسو کردش از پیش</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>کند تیغ قضا چون قطع امید</p></div>
<div class="m2"><p>نه مه داند سپر کردن نه خورشید</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به یک ضربت که آن نامهربان کرد</p></div>
<div class="m2"><p>سر شه در کنارش میهمان کرد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>قضا کامد ز بهرش ز آسمان زیر</p></div>
<div class="m2"><p>قلم چون رانده بودش، راند شمشیر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ز خون او چو رنگین کرد جا را</p></div>
<div class="m2"><p>هم از خونش نوشت این ماجرا را</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو از تیغ آن سر والا، قلم شد</p></div>
<div class="m2"><p>خط مشکین او خونین رقم شد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چو گرد رویش از خون سیل در گشت</p></div>
<div class="m2"><p>گل لعل وی از خون لعل تر گشت</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ز گردن موج خون کش پیش می‌رفت</p></div>
<div class="m2"><p>دون سوی نگار خوش می‌رفت</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>«دول رانی» که با فرخندگی بود</p></div>
<div class="m2"><p>دوید این خون و با آن خون درآمیخت</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>«دول رانی» که با فرخندگی بود</p></div>
<div class="m2"><p>خضر خان را زلال زندگی بود</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چو خضر چرخ، با او در کمین گشت</p></div>
<div class="m2"><p>همان آب حیاتش تیغ کین گشت</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چو دیدم اندرین شیشه به تمیز</p></div>
<div class="m2"><p>بسی هست آب حیوان خضر کش تیز</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بر آمد جان عاشق خون فشانان</p></div>
<div class="m2"><p>ولی می‌گشت گرداگرد جانان</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>گلی کز وی چکید از قطره‌ای خوی</p></div>
<div class="m2"><p>فشاندی، خون خود، صد بنده به روی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>تنی کاسیب گل بودی دریغش</p></div>
<div class="m2"><p>فلک، بین تا چسان زو زخم تیغش</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>زهی خونابهٔ مردم که گردون</p></div>
<div class="m2"><p>ز شیرش پرورد، آنگه خورد خون</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>نگر تا چند گردد دور افلاک،</p></div>
<div class="m2"><p>که یک نوباوه بیرون آرد از خاک؟</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>چو گشت این سرو بن، در زیور و زیب</p></div>
<div class="m2"><p>بخاک اندازدش، باز از یک آسیب!</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>چه باشد خضر خان، بل صدخضر نیز</p></div>
<div class="m2"><p>ازین خضرای رنگین گشت ناچیز</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بس آن به کادمی در جان سپردن</p></div>
<div class="m2"><p>بقای خضر یابد بعد مردن</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چو خون خضر خان در خاک در شد</p></div>
<div class="m2"><p>ز خونش هر گیا خضری دگر شد</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بگرد بار خود می‌گشت جانش</p></div>
<div class="m2"><p>همی گفت این حکایت از زبانش</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>که ای جان من و آشوب جانم</p></div>
<div class="m2"><p>که در کار تو شد جان و جهانم</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>چون من بهرت، ز جان کردم جدائی</p></div>
<div class="m2"><p>مبری ز آشنایان، اشنائی</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بهر جائی که خون راند این تن پاک</p></div>
<div class="m2"><p>گیاه مهر، خواهد رستن از خاک</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ز خون و خاکم این رنگین گیاجوی</p></div>
<div class="m2"><p>از آن گوگرد سرخ این کیمیا جوی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>نه مرگست این که آید به پایان</p></div>
<div class="m2"><p>ولی مرگست دوری ز آشنایان</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>جدائی‌های هر پیوندم از بند</p></div>
<div class="m2"><p>نه چون درد جدائی شد ز پیوند</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>خضر خان، کاب حیوان بودر در جام،</p></div>
<div class="m2"><p>درین دریای خون، گم شد سرانجام!</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>غرض، چون خضر خورد آن شربت حور</p></div>
<div class="m2"><p>همان می‌خورد «شادی خان» هم از دور</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>«دول رانی» در آن خونابه سرگم</p></div>
<div class="m2"><p>چو ماه چارده در جمع انجم</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>ز زخم ماه نو، در هر کناره</p></div>
<div class="m2"><p>به صد پاره رخی چون ماه پاره</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>ز زخمی کاندران رخساره می‌شد</p></div>
<div class="m2"><p>دل خورشید، صد جا، پاره می‌شد</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>نه زان رخساره می‌شد پاره‌ای دور</p></div>
<div class="m2"><p>که از مه دور می‌شد، پارهٔ نور</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>صباحت هم بران رخسار گلگون</p></div>
<div class="m2"><p>همی کرد از جراحت گریهٔ خون</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>ز چشم و رخ که خون بیرون همی‌رفت</p></div>
<div class="m2"><p>بهر سو سیلهای خون همی رفت</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>در آن موها که پیچ بیکران بود</p></div>
<div class="m2"><p>دل خان جست و جانش همدران بود</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ولی چون رفته را باز آمدن نیست</p></div>
<div class="m2"><p>غم بیهوده جز رنج بدن نیست</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>چو حال اینست به کاز طبع ناساز</p></div>
<div class="m2"><p>روم اندر سر گفتار خود باز</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>چو شد هنگام آن کان کشته‌ای چند</p></div>
<div class="m2"><p>به زندان ابد مانند در بند</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>شهیدان را ز مشهد گاه خون‌ریز</p></div>
<div class="m2"><p>روان کردند سوی خوابگه تیز</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>به «جی مندر» که برجی زان حصار است</p></div>
<div class="m2"><p>شهان را کاندران شاهان خوش خواب</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>به سنگین حجرهٔ در فرجهٔ تنگ</p></div>
<div class="m2"><p>نهان کردند شان چون لعل در سنگ</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چو پنهان گشت در سنگ آن گهرها</p></div>
<div class="m2"><p>جدا شد مهرهٔ دولت ز سرها</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>فرواندند ز آسیب زمانه</p></div>
<div class="m2"><p>فراموش اندران فرموش خانه</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>فراوان یاد دارد، چرخ بد خوی</p></div>
<div class="m2"><p>فرامش گشتگان، زینسان، بهر کوی</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>خردمندی که بندد در جهان دل</p></div>
<div class="m2"><p>دل از نام خردمندیش بگسل</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>بد و نیک ار نمی‌دانی ز هر باب</p></div>
<div class="m2"><p>تو هم زین نامه عبرت گیر و دریاب</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>به عشق آویز وزان سرمایه جو بهر</p></div>
<div class="m2"><p>که فارغ گردی از نیک و بد دهر</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>وگر در عشق‌بازی ره ندانی</p></div>
<div class="m2"><p>در آموزی گر این افسانه خوانی</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>که در هر بیت او پوشیده کاریست</p></div>
<div class="m2"><p>ز خون عاشقان نقش و نگاریست</p></div></div>