---
title: >-
    شمارهٔ ۴۱ - در وصف پیل جنگی هندی
---
# شمارهٔ ۴۱ - در وصف پیل جنگی هندی

<div class="b" id="bn1"><div class="m1"><p>پیل چو کوهی که بود بی‌سکون</p></div>
<div class="m2"><p>چارستون زیر که بی ستون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان جل زر نیش به فرو شکوه</p></div>
<div class="m2"><p>سایه همی کرد به بالای کوه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سود بگردون سر شنگرف سای</p></div>
<div class="m2"><p>رنگ شفق زو شده شنگرف زای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیچش خرطوم بسان کمند</p></div>
<div class="m2"><p>اژدری افتاده ز کوه بلند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اژدر آن کوه شده پارپیچ</p></div>
<div class="m2"><p>مار ازو یافته در غار پیچ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر شده بالا و سوارش بلند</p></div>
<div class="m2"><p>چون دو پیاده به پس پیل بند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ته پا کوه زمین سای او</p></div>
<div class="m2"><p>پایهٔ کوهی به صفت پای او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان سپر انگیزیی سهمناک</p></div>
<div class="m2"><p>در تهٔ پایش سپری گشته خاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاه ز بندی که به پایش فگند</p></div>
<div class="m2"><p>مات شده صد شه از آن پیل بند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر به سپل پای برادر ز جای</p></div>
<div class="m2"><p>سلسله فریاد بر ارد ز پای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کشتی عاج است تو گوئی روان</p></div>
<div class="m2"><p>گشته دو گوشش زد و سو بادبان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گوش که با چشم همی کرد لاغ</p></div>
<div class="m2"><p>مروحه‌ای بود به پیش چراغ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طرفه که آن مروحه ز آسیب باد</p></div>
<div class="m2"><p>هیچ گزندی به چراغش نداد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر کشد از تارک بدخواه مغز</p></div>
<div class="m2"><p>وزین دندان کند این کار نغز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در صف کین کرده به دندان ستیز</p></div>
<div class="m2"><p>خون عدو خورده به دندان تیز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خصم ترش را که بدندان درید</p></div>
<div class="m2"><p>زان ترشی کندی دندان ندید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون جرسش، در روش، آواز داد</p></div>
<div class="m2"><p>گنبد گردنده صدا باز داد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بانگ بلندش زده با رعد کوس</p></div>
<div class="m2"><p>ابر بلندش به قدم داد بوس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خورده زخم خانهٔ دولت شراب</p></div>
<div class="m2"><p>مست شده، کرده جهانی خراب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از می شه بس که رخش یافت رنگ</p></div>
<div class="m2"><p>کرد فراموش خورشهای بنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا ز می مجلس شه مژده یافت</p></div>
<div class="m2"><p>بنگ رها کرد و به مجلس شتافت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>الغرض آن پیل و همان تاج و تخت</p></div>
<div class="m2"><p>کان نرسد جز به خداوند بخت</p></div></div>