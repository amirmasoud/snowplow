---
title: >-
    شمارهٔ ۵۰ - عکس و تبدیل
---
# شمارهٔ ۵۰ - عکس و تبدیل

<div class="b" id="bn1"><div class="m1"><p>چرخ نداند در و دیوار کس</p></div>
<div class="m2"><p>تکیه به دیوار و درش کرده بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم یک خانه و صد خرمی</p></div>
<div class="m2"><p>خانهٔ یک مردم و صد مردمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چتر شه آنست که شد چرخ ماه</p></div>
<div class="m2"><p>چرخ مه این است که شد چتر شاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور قلم از سحر زبان بر کشم</p></div>
<div class="m2"><p>سحر زبان را به قلم در کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب فرو ماند چو کوه از شهاب</p></div>
<div class="m2"><p>کوه درامد به تزلزل چو آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم پدر بهر جگر گوشه تر</p></div>
<div class="m2"><p>گوشهٔ هر چشم شده پر جگر</p></div></div>