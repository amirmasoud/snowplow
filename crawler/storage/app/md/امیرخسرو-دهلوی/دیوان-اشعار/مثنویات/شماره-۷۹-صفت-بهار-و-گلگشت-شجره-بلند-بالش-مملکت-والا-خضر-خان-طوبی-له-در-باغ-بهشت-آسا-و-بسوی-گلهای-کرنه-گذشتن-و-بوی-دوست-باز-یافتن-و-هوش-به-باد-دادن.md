---
title: >-
    شمارهٔ ۷۹ - صفت بهار، و گلگشت شجرهٔ بلند بالش مملکت والا خضر خان طوبی له، در باغ بهشت آسا، و بسوی گلهای کرنه گذشتن، و بوی دوست باز یافتن، و هوش به باد دادن
---
# شمارهٔ ۷۹ - صفت بهار، و گلگشت شجرهٔ بلند بالش مملکت والا خضر خان طوبی له، در باغ بهشت آسا، و بسوی گلهای کرنه گذشتن، و بوی دوست باز یافتن، و هوش به باد دادن

<div class="b" id="bn1"><div class="m1"><p>صبا چون باغ را پیرایه نو کرد</p></div>
<div class="m2"><p>دل بلبل به روی گل گرو کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین موسم که از دل‌های پر سوز</p></div>
<div class="m2"><p>به شسته گرد غم باران نوروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل شاه از جدائی ریش مانده</p></div>
<div class="m2"><p>گرفتار هوای خویش مانده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بشنیدی از مرغی نوائی</p></div>
<div class="m2"><p>برآوردی به درد از سینه وائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر سوی که ابری سر کشیدی</p></div>
<div class="m2"><p>چو ابراز دیده بارانش چکیدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تمام ار باز رانم شرح این حال</p></div>
<div class="m2"><p>نگوم حال یک شب تا به یک سال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به فردوس حرم باغیت دلکش</p></div>
<div class="m2"><p>که فردوس ارم نبود چنان خوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به کشور، هر کجا، نادر نهالی</p></div>
<div class="m2"><p>درو نوشیده از کوثر زلالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز گلهای خراسان گونه گونه</p></div>
<div class="m2"><p>نموده هر یکی دیگر نمونه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دمیده برگ نازک یاسمین را</p></div>
<div class="m2"><p>لباس پرنیان داده زمین را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر آب نسترن نسرین شکرخند</p></div>
<div class="m2"><p>چو دو هم شیرهٔ نزدیک مانند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز گلهای تر هندوستان هم</p></div>
<div class="m2"><p>شده سر گشته با دو بوستان هم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گل کوزه که دور چرخ گردان</p></div>
<div class="m2"><p>پدید از خاک پاک هند کرد آن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گل صد برگ را خوبی ز حد بیش</p></div>
<div class="m2"><p>نموده صدق ورق دیباچهٔ خویش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسان دفتر شیرازه بسته</p></div>
<div class="m2"><p>ز هر برگش سرشک شیر جسته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر چه پارسی نامند اینها</p></div>
<div class="m2"><p>ولی در هند زادند از زمینها</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر این گل در دیار پارسی زاد،</p></div>
<div class="m2"><p>چرا زونیست در گفتارشان یاد؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسی گلهای دیگر هندوی نام</p></div>
<div class="m2"><p>کز ایشان بود برد مشک خطا وام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قرنفل هم ز هند ستانست ور دی</p></div>
<div class="m2"><p>که از نام عرب شد شهر گردی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گل ما را به هندی نام زشت است</p></div>
<div class="m2"><p>و گر نه هر گلی باغ بهشت است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر این گل خواستی در روم یا شام</p></div>
<div class="m2"><p>که بودی پارسی یا تازیش نام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کدامی گل چنین باشد که سالی</p></div>
<div class="m2"><p>دهد بو دور مانده از نهالی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بتان هند را نسبت همین است</p></div>
<div class="m2"><p>به هر یک موی شان صد ملک چین است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چه یاد آری سپید و سرخ را روی</p></div>
<div class="m2"><p>چو گلهای خراسان رنگ بی بوی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>و گر پرسی خبر از روم و از روس</p></div>
<div class="m2"><p>از ایشان نیز ناید لابه و لوس</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سپید و سرو همچون کندهٔ یخ</p></div>
<div class="m2"><p>کز ایشان رم خورد کانون دوزخ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خطای تنگ چشم و پست بینی</p></div>
<div class="m2"><p>مغل را چشم و بینی خود نه بینی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>لب تا تار خود خندان نباشد</p></div>
<div class="m2"><p>ختن را خود نمک چندان نباشد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به مصر و روم هم سیمین خدانند</p></div>
<div class="m2"><p>ولی چستی و چالاکی ندانند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر چه بیشتر هندوستان زاد</p></div>
<div class="m2"><p>به سبزی می‌زند چون سرو آزاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ولی بسیار با شد سبزهٔ تر</p></div>
<div class="m2"><p>به لطف از لاله و نسرین نکوتر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بسی زیبا کنیز سبز فام است</p></div>
<div class="m2"><p>که صد چون سرو آزادش غلام است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه چون طاوس بی دنبال زشت اند</p></div>
<div class="m2"><p>که در خوبی چو طاوس بهشت‌اند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سه گونه رنگ هندوستان زمین است</p></div>
<div class="m2"><p>سیاه وسبز گندم گون همین است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به گندم گونست میل آدمی زاد</p></div>
<div class="m2"><p>که این فتنه ز آدم یافت بنیاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یکی گندم به کام اندر نمک ده</p></div>
<div class="m2"><p>ز صد قرص سپیدی بی نمک به</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سیه را خود بریده جایگاه است</p></div>
<div class="m2"><p>که اندر دیده هم مردم سیاه است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز بهر دیده با ید سرمه را سود</p></div>
<div class="m2"><p>سپیده عارضی رنگی است بی سود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ازین هر دو نکوتر رنگ سبز است</p></div>
<div class="m2"><p>که زیب اختران ز او رنگ سبز است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به رنگ سبز رحمت‌ها سرشت است</p></div>
<div class="m2"><p>که رنگ سبز پوشان بهشت است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دل اندر سبزه‌ها بی گل شکیباست</p></div>
<div class="m2"><p>گلی بی سبزه در بستان نه زیباست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به رنگ سبز زین بهتر چه مقدار</p></div>
<div class="m2"><p>که از نام خضر خان دارد آثار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خدایا تا گیاها سبز رویست</p></div>
<div class="m2"><p>خضر در باغ و سبزه چشمه جویست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خضر خان با دو دیولدی رانی</p></div>
<div class="m2"><p>به هم چو خضر و آب زندگانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خضر خانی که نورسته درختش</p></div>
<div class="m2"><p>به آب زندگی پرورده بختش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گلش بی آب از تاب درونی</p></div>
<div class="m2"><p>جگر باران ز نرگسهای خونی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در آن خرم بهار خاطر افروز</p></div>
<div class="m2"><p>بگردان چمن می‌گشت یک روز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو مرغان نالهای زار می‌کرد</p></div>
<div class="m2"><p>دل مرغان باغ افگار می‌کرد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز آهی کز دل غمناک می‌زد</p></div>
<div class="m2"><p>همه گلها گریبان چاک می‌زد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گل کر نه شگفته بر درختان</p></div>
<div class="m2"><p>به بوی خوش چو خلق نیک بختان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو در رفت آن نسیم اندر دماغش</p></div>
<div class="m2"><p>به سینه تازه شد دیرینه داغش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به زاری گفت کای گل کاشکی من</p></div>
<div class="m2"><p>گیاهی بودمی، چون تو، به گلشن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که تو آنجا گذر داری و من نی</p></div>
<div class="m2"><p>گل آنجا محرم است و نارون نی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از آن گل کاوست در صد پردهٔ مستور</p></div>
<div class="m2"><p>من مسکین به بوئی قانع از دور</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چه بختست این که تو از بخشش غیب</p></div>
<div class="m2"><p>خزی که در گریبان گاه در جیب</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>جوابش را دهان کر نه بشگفت</p></div>
<div class="m2"><p>که آخر کرنه هم بشنوم گفت</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بدو گویم هر آن رازی که گویی</p></div>
<div class="m2"><p>بجویم زو هر آن حاجت که جوئی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>پس آنگه گفت شه با صد خرابی</p></div>
<div class="m2"><p>که هر باری که آنجا بار یابی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بگوئی از من نادیده کامی</p></div>
<div class="m2"><p>به صد خون دل آلوده، سلامی</p></div></div>