---
title: >-
    بخش ۱ - سرآغاز
---
# بخش ۱ - سرآغاز

<div class="b" id="bn1"><div class="m1"><p>خداوندا دلم را چشم بگشای</p></div>
<div class="m2"><p>به معراج یقینم راه بنمای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به رحمت باز کن گنجینهٔ جود</p></div>
<div class="m2"><p>درونم خوان بشاد روان مقصود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلی بخش از ثنای خویش معمور</p></div>
<div class="m2"><p>زبانی ز آفرین دیگران دور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دراسانیم شکر اندیش گردان</p></div>
<div class="m2"><p>به دشواری سپاسم بیش گردان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امیدم را به جائی کش عماری</p></div>
<div class="m2"><p>که باشد پیشگاه رستگاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو خود برداشتی اول ز خاکم</p></div>
<div class="m2"><p>مده آخر به طوفان هلاکم</p></div></div>