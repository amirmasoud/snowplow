---
title: >-
    بخش ۶ - آغاز داستان
---
# بخش ۶ - آغاز داستان

<div class="b" id="bn1"><div class="m1"><p>به تاریخ عجم دانندهٔ راز</p></div>
<div class="m2"><p>چنین کرد این حکایت را سرآغاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چون خورشید هرمز رفت در خاک</p></div>
<div class="m2"><p>کشید اکلیل خسرو سر بر افلاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان را خسرو از سر کار نو کرد</p></div>
<div class="m2"><p>کرم را در جهان بازار نو کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ترتیب جهان بودی شب و روز</p></div>
<div class="m2"><p>گهی لشکر کش و گه مجلس افروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان آراست ملک از دانش و داد</p></div>
<div class="m2"><p>که شهر آسوده گشت و کشور آباد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مقیمان زمین زان مهربانی</p></div>
<div class="m2"><p>همه مشغول عیش و کامرانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باشگ و ناله کس ننمودی آهنگ</p></div>
<div class="m2"><p>مگر چشم صراحی و رگ چنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بجز چو بین که در ره خار بودش</p></div>
<div class="m2"><p>وزو پای مراد افگار بودش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبود از کین دران فرخنده ایام</p></div>
<div class="m2"><p>کس آهن دلت را ز چو بینه بهرام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از او او رنگ هرمز را نوی بود</p></div>
<div class="m2"><p>که هرمز را سپهداری قوی بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو هرمز سوی خاقانش فرستاد</p></div>
<div class="m2"><p>به کوشش ملک خاقان داد بر باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رسید اندر مداین باده و گیر</p></div>
<div class="m2"><p>کشیده پور خاقان را به زنجیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گلو بسته بسی میر ولایت</p></div>
<div class="m2"><p>غنیتمهای چینی بی نهایت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو آن فیروزمندی دید از و شاه</p></div>
<div class="m2"><p>تغیر یافت اندر خاطرش راه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز غیرت کرد طعن بی کرانش</p></div>
<div class="m2"><p>نوید پنبه داد و دوکدانش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ازین وحشت که بر بهرام ره یافت</p></div>
<div class="m2"><p>چو وحشی جست و روی از مردمی تافت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز طاعتگه به عصیان دور می‌بود</p></div>
<div class="m2"><p>گهی پیدا گهی مستور می‌بود</p></div></div>