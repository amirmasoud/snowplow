---
title: >-
    بخش ۱۳ - عشرت کردن خسرو و شیرین بر لب  شهر و دو افسانه گفتن آنان
---
# بخش ۱۳ - عشرت کردن خسرو و شیرین بر لب  شهر و دو افسانه گفتن آنان

<div class="b" id="bn1"><div class="m1"><p>شبی همچون سواد دیده پر نور</p></div>
<div class="m2"><p>هوا عنبر فشان چون طره حور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمانه برگ عشرت ساز کرده</p></div>
<div class="m2"><p>فلک درهای دولت باز کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرو مرده چراغ صبح گاهی</p></div>
<div class="m2"><p>نشاط خواب کرده مرغ و ماهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مقیمان زمین در پردهٔ راز</p></div>
<div class="m2"><p>عروسان فلک در جلوهٔ ناز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کواکب در میان سرمهٔ ناب</p></div>
<div class="m2"><p>درست افگنده مروارید شب تاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشاده شب در این طاووس گون باغ</p></div>
<div class="m2"><p>دم طاوس را بر سینه زاغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرو برده زمانه جام جمشید</p></div>
<div class="m2"><p>شده مه در زمین مهمان خورشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز قصر آهنگ صحرا کرد خسرو</p></div>
<div class="m2"><p>کشیده بارگه بر سبزهٔ نو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لب شهر و دو مطرب زخمه درود</p></div>
<div class="m2"><p>غبار غم جهان را کرد بدرود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>معنبر شمعهای مجلس افروز</p></div>
<div class="m2"><p>گشاده در دل شب روزن روز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بخور مجمر از عود قماری</p></div>
<div class="m2"><p>زده ره چون نسیم نو بهاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نهانی مجلسی کز هیچ سوئی</p></div>
<div class="m2"><p>به جز محرم نمی‌گنجید موئی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ملک را داده گردون دوتا پشت</p></div>
<div class="m2"><p>بشارت نامهٔ مقصود در مشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صنم با او برسم دل نوازی</p></div>
<div class="m2"><p>نشسته بر سریر سرفرازی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ستد جام شراب از دست ساقی</p></div>
<div class="m2"><p>دمی خورد و به خسرو داد باقی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که چون من چاشنی گیرم ازین جام</p></div>
<div class="m2"><p>ازانکن چاشنی لعل من وام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دو بوسی زان به نوش و ناز بستان</p></div>
<div class="m2"><p>یکی وام ده و صد باز بستان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نشاید عاشقان را می پرستی</p></div>
<div class="m2"><p>کزان دیوانگی خیزد نه مستی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شراب و عاشقی چونشد به هم یار</p></div>
<div class="m2"><p>معاذ الله به رسوائی کشد کار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به جائی کاتشی در خرمن افتد</p></div>
<div class="m2"><p>کجا میرد چو در وی روغن افتد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو خورد آن باده را مست جگر خوار</p></div>
<div class="m2"><p>به دستوری شد از شیرین شکرخوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دهان را با دهانش هم نفس کرد</p></div>
<div class="m2"><p>لبش بوسید و هم بر بوسه بس کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز مقصود آنچه باید در نظرگاه</p></div>
<div class="m2"><p>غم و اندیشه زحمت برده از راه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گهی جستند از می جان نوازی</p></div>
<div class="m2"><p>گهی کردند با هم بوسه بازی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گه او در زلف این شبگیر کردی</p></div>
<div class="m2"><p>به گردن زلف را زنجیر کردی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گهی این جعد او بگشادی از ناز</p></div>
<div class="m2"><p>دل درمانده را کردی گره باز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گه آن با این عتاب اندیش گشتی</p></div>
<div class="m2"><p>شفاعت خواه جرم خویش گشتی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که این افسانه‌های ناز گفتی</p></div>
<div class="m2"><p>ز هجران سرگذشتی باز گفتی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گه او از دل برون دادی هوائی</p></div>
<div class="m2"><p>به گریه باز راندی ماجرائی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در ان مجلس که بد از عشق بازار</p></div>
<div class="m2"><p>خرد در خواب بود و فتنه بیدار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز بس عشرت همه شب تا سحرگاه</p></div>
<div class="m2"><p>بهشت این جهانی بود خرگاه</p></div></div>