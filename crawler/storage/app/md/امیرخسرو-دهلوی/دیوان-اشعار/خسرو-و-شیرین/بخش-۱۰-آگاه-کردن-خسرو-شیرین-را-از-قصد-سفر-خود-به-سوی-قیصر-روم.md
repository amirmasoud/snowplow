---
title: >-
    بخش ۱۰ - آگاه کردن خسرو شیرین را از قصد سفر خود به سوی قیصر روم
---
# بخش ۱۰ - آگاه کردن خسرو شیرین را از قصد سفر خود به سوی قیصر روم

<div class="b" id="bn1"><div class="m1"><p>حلاوت سنج شیرین شکر خند</p></div>
<div class="m2"><p>چنین برداشت مهر از حقه قند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که با خسرو چو شیرین بست پیمان</p></div>
<div class="m2"><p>که این بلقیس گردد آن سلیمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک بر رسم اول چند گاهی</p></div>
<div class="m2"><p>به مهر از دور می‌کردش نگاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شیرین گفت میدانی که کارم</p></div>
<div class="m2"><p>پریشانست همچون روزگارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا در ملک خود کاری درافتاد</p></div>
<div class="m2"><p>رسیدم با تو کاری دیگر افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون کامیدم از تو یافت یاری</p></div>
<div class="m2"><p>به ملکم نیز هست امیدواری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفتم از رخت فال مبارک</p></div>
<div class="m2"><p>که تاجم باز گردد سوی تارک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرم دستوریی باشد ز رایت</p></div>
<div class="m2"><p>بر ارم سر بروم از زیر پایت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برآمد همچو مه در شامل دیجور</p></div>
<div class="m2"><p>سوار سایه شد خورشید پر نور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برون راند آن شب فرخنده ز آن بوم</p></div>
<div class="m2"><p>مبارک روی شد بر قیصر روم</p></div></div>