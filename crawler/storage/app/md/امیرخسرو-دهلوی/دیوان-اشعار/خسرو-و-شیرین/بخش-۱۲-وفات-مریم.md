---
title: >-
    بخش ۱۲ - وفات مریم
---
# بخش ۱۲ - وفات مریم

<div class="b" id="bn1"><div class="m1"><p>شناسای معانی موبد پیر</p></div>
<div class="m2"><p>چنین کرد این خبر در نامه تحریر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چون خسرو ستد گنجینهٔ روم</p></div>
<div class="m2"><p>خلافش رومیان را گشت معلوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو غالب گشته بود از تیغ کین خواه</p></div>
<div class="m2"><p>نداد اندیشه را در خویشتن راه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبانی پوزشی کان در حرم کرد</p></div>
<div class="m2"><p>ز مریم چند گاه آن نیز کم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شیرین عیش مریم بود چون تلخ</p></div>
<div class="m2"><p>ازین کاهش فتاد آن ماه در سلخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به تن عیسی جانش مانده بی دم</p></div>
<div class="m2"><p>تنش چو ن رشتهٔ مریم شد از غم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بیماری به بستر خفت ماهی</p></div>
<div class="m2"><p>و زان پس جست دیگر خواب گاهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملک بایست و نابایست برخاست</p></div>
<div class="m2"><p>به صد شادی بساط ماتم آراست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل از سودای شیرین در غم افگند</p></div>
<div class="m2"><p>بهانه بر فراق مریم افگند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به ماتم کرد پیراهن بسی چاک</p></div>
<div class="m2"><p>ولیکن در هوای یار چالاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شیرین دید کز خس رفته شد راه</p></div>
<div class="m2"><p>به بی صبری شتابان گشت چون ماه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رسید آن در بی قیمت به دریا</p></div>
<div class="m2"><p>چو خور در بره و مه در ثریا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گلشن تر شد خزان را باد بنشست</p></div>
<div class="m2"><p>به آزادی چو سرو آزاد بنشست</p></div></div>