---
title: >-
    بخش ۳۲ - رفتن خسرو به سوی قصر شیرین و در  بستن شیرین به روی خسرو
---
# بخش ۳۲ - رفتن خسرو به سوی قصر شیرین و در  بستن شیرین به روی خسرو

<div class="b" id="bn1"><div class="m1"><p>چو بستان تازه گشت از باد نوروز</p></div>
<div class="m2"><p>جهان بستد بهار عالم افروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آسیب صبا در جلوه شد باغ</p></div>
<div class="m2"><p>به غارت داد بلبل خانهٔ زاغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوا کرد از گل آشوب خزان دور</p></div>
<div class="m2"><p>به مشک‌تر به دل شد گرد کافور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عروس غنچه را نو شد عماری</p></div>
<div class="m2"><p>کمر بر بست گل در پرده داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنفشه سر بر آورد از لب جوی</p></div>
<div class="m2"><p>زمین گشت از ریاحین عنبرین بوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نسیم صبح گاه از مشک بوئی</p></div>
<div class="m2"><p>هزاران نافه در بر داشت گوئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حریر گل ورق در خون سرشته</p></div>
<div class="m2"><p>برات عیش بر ساقی نوشته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملک بر عزم صحرا بارگی جست</p></div>
<div class="m2"><p>به پشت باد سرو نازنین رست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نخست از گشت کرد آهنگ نخجیر</p></div>
<div class="m2"><p>فرود آورد هر مرغی به یک تیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به گلزار آمد از نخجیرگه شاد</p></div>
<div class="m2"><p>بساط افگند زیر سرو شمشاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به می بنشست با خاصان درگاه</p></div>
<div class="m2"><p>بر آمد بانگ نوشا نوش بر ماه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیاپی گر چه می میکرد بر کار</p></div>
<div class="m2"><p>نمی‌رفت از سرش سودای دلدار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شکیبا بود تا هشیاریی داشت</p></div>
<div class="m2"><p>کفایت را عنان از دست نگذاشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو سرها گرم شد از باده‌ای چند</p></div>
<div class="m2"><p>زبان بگشاد با آزاده‌ای چند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که نوروز آمد و گلزار بشگفت</p></div>
<div class="m2"><p>صبا با گل پیام عاشقان گفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روان شد باد جام لاله بر دست</p></div>
<div class="m2"><p>خمار نرگس بیمار بشکست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه کس با حریفی باغ در باغ</p></div>
<div class="m2"><p>مرا در دل ز دوری داغ بر داغ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه شادند و جانم در عذابست</p></div>
<div class="m2"><p>که می بی روی خوبان زهر ناب است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو چندی زین سخنها گفت حالی</p></div>
<div class="m2"><p>دل از اندیشه لختی کرد خالی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جنیبت جست و ز دل بار برداشت</p></div>
<div class="m2"><p>ره مشکوی آن دلدار برداشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روان گشت از شراب لعل سرخوش</p></div>
<div class="m2"><p>ولی از سوز سینه دل پر آتش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو آمد تا به قصر نازنین تنگ</p></div>
<div class="m2"><p>ز مغزش هوش رفت از سینه فرهنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خبر بردند بر سرو گلندام</p></div>
<div class="m2"><p>که طوبی بر در فردوس زد گام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به لرزید از هراس آن دستهٔ گل</p></div>
<div class="m2"><p>کزان سیلاب تندش بشکند پل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شکوه ننگ و نام آواره گردد</p></div>
<div class="m2"><p>لباس عصمتش صد پاره گردد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صواب آن دید رای هوشیارش</p></div>
<div class="m2"><p>که ندهد راه در ایوان بارش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عمل داران درگه را به فرمود</p></div>
<div class="m2"><p>که بشتابند پیش آهنگ شه زود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دویدند آن همه فرمان پذیران</p></div>
<div class="m2"><p>به استقبال شاه تخت گیران</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو آمد بر در قصر دلارام</p></div>
<div class="m2"><p>کزان شیرین سخن شیرین کند کام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دری بر بسته دید و میزبان دور</p></div>
<div class="m2"><p>مه اندر برج عصمت مانده مستور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تعجب کرد و حیران ماند زان کار</p></div>
<div class="m2"><p>که نخل بارور چون گشت بی بار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جهان شب شد به چشم نیم خوابش</p></div>
<div class="m2"><p>که ماند اندر پس کوه آفتابش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به خواری بازگشتن خواست در حال</p></div>
<div class="m2"><p>که خواندش نازنین ز آواز خلخال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ملک را کامد آن آواز در گوش</p></div>
<div class="m2"><p>به جان بی خبر باز آمدش هوش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو سر بر کرد سوی قصر والا</p></div>
<div class="m2"><p>زمین بوسیده ماه سرو بالا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دمید از هر دو جانب صبح امید</p></div>
<div class="m2"><p>مقابل شد به دلگرمی دو خورشید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پری روی از مژه می ریخت آبی</p></div>
<div class="m2"><p>به روی میهمان میزد گلابی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به نظاره فرو ماندند تا دیر</p></div>
<div class="m2"><p>نمی‌گشت از تماشا چشمشان سیر</p></div></div>