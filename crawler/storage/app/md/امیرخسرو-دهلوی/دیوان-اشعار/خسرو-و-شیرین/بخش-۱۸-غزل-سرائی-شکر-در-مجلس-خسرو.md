---
title: >-
    بخش ۱۸ - غزل سرائی شکر در مجلس خسرو
---
# بخش ۱۸ - غزل سرائی شکر در مجلس خسرو

<div class="b" id="bn1"><div class="m1"><p>چه فرخ روزگاری باشد آن روز</p></div>
<div class="m2"><p>که گردد هم نشین دو یار دل سوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه سرمایهٔ عشرت مهیا</p></div>
<div class="m2"><p>ز موج شادمانی دل چو دریا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مراد و خوش دلی و کامرانی</p></div>
<div class="m2"><p>نشاط عشق و آغاز جوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی را کاین همه یک جا دهد دست</p></div>
<div class="m2"><p>گر از دولت بنازد جای آن هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا کاین دولت امروز است در چنگ</p></div>
<div class="m2"><p>به دولت چون ننوشم جام گلرنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمان چون رفت دیگر یافت نتوان</p></div>
<div class="m2"><p>عنان زندگانی تافت نتوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بساکس کانده فردا کشیدند</p></div>
<div class="m2"><p>که دی مردند و فردا را ندیدند</p></div></div>