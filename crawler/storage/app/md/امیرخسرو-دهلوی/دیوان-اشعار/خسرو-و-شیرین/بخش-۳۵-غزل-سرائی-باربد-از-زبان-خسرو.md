---
title: >-
    بخش ۳۵ - غزل سرائی باربد از زبان خسرو
---
# بخش ۳۵ - غزل سرائی باربد از زبان خسرو

<div class="b" id="bn1"><div class="m1"><p>چو فرخ ساعتی باشد که تقدیر</p></div>
<div class="m2"><p>دو عاشق را کند با هم به تدبیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی خوش خوش به شادی جام گیرند</p></div>
<div class="m2"><p>گهی در بزم وصل آرام گیرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی با سرو سنبل دست مالند</p></div>
<div class="m2"><p>گهی افسانهٔ هجران سکالند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه از لبها نصیب جان ربایند</p></div>
<div class="m2"><p>گه از دلها غبار غم زدایند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی کاین خواب بختش راستین است</p></div>
<div class="m2"><p>کلید دولتش در آستین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهشت و بوستان بی‌دوست زشتست</p></div>
<div class="m2"><p>به روی دوستان زندان بهشتست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من و جام می و زلف دوتاهت</p></div>
<div class="m2"><p>بهشت و باغ من روی چو ماهت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو من زان روی گلرنگ شدم شاد</p></div>
<div class="m2"><p>رها کن سرخ گل را برد باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو در آغوشم آمد سرو گل روی</p></div>
<div class="m2"><p>ممان گو هیچ سروی بر لب جوی</p></div></div>