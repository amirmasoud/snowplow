---
title: >-
    بخش ۲ - در توحید
---
# بخش ۲ - در توحید

<div class="b" id="bn1"><div class="m1"><p>به نام آنکه جان را زندگی داد</p></div>
<div class="m2"><p>طبیعت را به جان پایندگی داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خداوندیکه حکمت بخش خاکست</p></div>
<div class="m2"><p>کمینه بخشش او جان پاکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو کون از صنع او یک گل به باغی</p></div>
<div class="m2"><p>ز ملکش نه فلک یک شب چراغی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رموز آموز عقل نکته پیوند</p></div>
<div class="m2"><p>شناسائی ده جان خردمند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بصارت بخش چشم پیش بینان</p></div>
<div class="m2"><p>تمنای درون شب نشینان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جواهر بند ناهید از ثریا</p></div>
<div class="m2"><p>چراغ افروز در قعر دریا</p></div></div>