---
title: >-
    بخش ۳۳ - گفتگوی خسرو و شیرین
---
# بخش ۳۳ - گفتگوی خسرو و شیرین

<div class="b" id="bn1"><div class="m1"><p>به زاری گفت کای جانم بتو شاد</p></div>
<div class="m2"><p>غمت شادی فزای جان من باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزرگیهای بی اندازه کردی</p></div>
<div class="m2"><p>که با خردان بزرگی تازه کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بود این بی سبب در پرده ماندن</p></div>
<div class="m2"><p>غریبان را ز در بیرون نشاندن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا بگذاشتی در خاک خواری</p></div>
<div class="m2"><p>چو مه بر آسمان گشتی حصاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوابش داد شمشاد قصب پوش</p></div>
<div class="m2"><p>که دولت پادشه را حلقه در گوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر بالا شدم چون دیدمت مست</p></div>
<div class="m2"><p>مکن از سرزنش سرو مرا پست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>توانم کز وفاداری درین راه</p></div>
<div class="m2"><p>دهم تن در رضای خدمت شاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرود آیم ازین منظر خرامان</p></div>
<div class="m2"><p>کمر بندم بر آئین غلامان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ولی ترسم که وا ماند ز پرواز</p></div>
<div class="m2"><p>تذرو نازنین در چنگل باز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو شاه و عاشق و دیوانه و مست</p></div>
<div class="m2"><p>چو در دامت فتادم چون توان رست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برو خود را به بازار شکر بند</p></div>
<div class="m2"><p>که شیرین انگبین است و شکر قند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لب شیرین که جز با جان نسازد</p></div>
<div class="m2"><p>شکر داند کزو چون می‌گدازد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مبر نام شکر گر خود نبات است</p></div>
<div class="m2"><p>که شیرین شربت آب حیاتست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شکر گر چه دهد ذوق زبانی</p></div>
<div class="m2"><p>ولی شیرینست ذوق زندگانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو خوش خوش با پری رویان دمساز</p></div>
<div class="m2"><p>بهر گلزار چون بلبل به پرواز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مده دمهای سردم را به خود راه</p></div>
<div class="m2"><p>که از آه ایمنست آئینه ماه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حذر کن زین فغان آتش آلود</p></div>
<div class="m2"><p>که دیوارت سیه گردد بدین دود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نبینی کاه جان مستمندی</p></div>
<div class="m2"><p>بر آن کنگر بیندازد کمندی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>درافگن زلف تا زآن رشته ناز</p></div>
<div class="m2"><p>شوم با چنبر گردون رسن باز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وگر بالا نخوانی زین مغاکم</p></div>
<div class="m2"><p>مران از در نه آخر کم ز خاکم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وگر راضی بدان شد لعبت نور</p></div>
<div class="m2"><p>که بوسیم استان دولت از دور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که باشد ذره‌ای از خویش نومید</p></div>
<div class="m2"><p>که خواهد تکیه بر بازوی خورشید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وگر محراب دیگر پیش کردم</p></div>
<div class="m2"><p>هوای نفس کافر کیش کردم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جوانی تهمت مرد است دانی</p></div>
<div class="m2"><p>بترس از تهمت روز جوانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من ار نرخ شکر پرسیدم از مار</p></div>
<div class="m2"><p>فگندی از بهشتم دوزخی وار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز شور شکرم تسکین نباشد</p></div>
<div class="m2"><p>شکر چون شور شد شیرین نباشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نکردم من گناهی ور که کردم</p></div>
<div class="m2"><p>شفاعت خواهد اینک روی زردم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گناهم گر ببخشی شرمسارم</p></div>
<div class="m2"><p>وگر خون ریزیم هم با تو یارم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدین خواری مرنجان بی خودی را</p></div>
<div class="m2"><p>مکافات است آخر هم بدی را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به خوش خوئی توان با دوستان زیست</p></div>
<div class="m2"><p>چو بدخو دوست باشد دشمنی چیست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گلی کز بوی خوش نبود نشانش</p></div>
<div class="m2"><p>رها کن تا برد باد خزانش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به آزار غریبان دست مگشای</p></div>
<div class="m2"><p>که غافل نیست دوران سبک پای</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جفائی کان ز تو بر همرانست</p></div>
<div class="m2"><p>بتو نزدیکتر از دیگرانست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دگر باره پری روی فسون ساز</p></div>
<div class="m2"><p>فسونی تازه کرد از چشم غماز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دعا از زیر لب پرواز می داد</p></div>
<div class="m2"><p>سخن را چاشنی از ناز می داد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که شاها تا ابد شاه جهان باش</p></div>
<div class="m2"><p>ز مشرق تا به مغرب کامران باش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شکوهت را فلک زیر نگین باد</p></div>
<div class="m2"><p>کلید عالمت در آستین باد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>من آن طاووس رنگینم در این باغ</p></div>
<div class="m2"><p>که دود دل سیاهم کرد چون زاغ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نه تسکینی که خود را باز جویم</p></div>
<div class="m2"><p>نه دلسوزی که با او راز گویم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ندانم کاین گره تا چون کنم باز</p></div>
<div class="m2"><p>که با بیگانه نتوان گفت این راز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نبینم ره چو رویت بینم از دور</p></div>
<div class="m2"><p>چو مرغ شب که کورش بینی از نور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>برانم زین دل دیوانهٔ خویش</p></div>
<div class="m2"><p>که آتش در زنم در خانهٔ خویش</p></div></div>