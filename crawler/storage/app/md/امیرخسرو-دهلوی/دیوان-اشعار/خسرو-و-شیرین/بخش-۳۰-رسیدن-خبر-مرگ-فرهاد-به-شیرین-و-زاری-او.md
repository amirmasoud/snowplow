---
title: >-
    بخش ۳۰ - رسیدن خبر مرگ فرهاد به شیرین  و زاری او
---
# بخش ۳۰ - رسیدن خبر مرگ فرهاد به شیرین  و زاری او

<div class="b" id="bn1"><div class="m1"><p>که چون فرهاد روز خود به سر برد</p></div>
<div class="m2"><p>چو شمع صبح دم در سوختن مرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلل در عشق شیرین در نیامد</p></div>
<div class="m2"><p>بر آمد جان و شیرین بر نیامد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خبر بردند بر شیرین خون ریز</p></div>
<div class="m2"><p>که خون کوهکن را ریخت پرویز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه گفتند کاین رسمی نو افتاد</p></div>
<div class="m2"><p>که شیرین کشت و خون بر خسرو افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روان شد نازنین کز راه یاری</p></div>
<div class="m2"><p>شهید خویش را گرید به زاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بالینگاه او شد با دلی تنگ</p></div>
<div class="m2"><p>به آب دیده شست از خون او سنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشارت کرد تا فرمان برانش</p></div>
<div class="m2"><p>بشستند از گلاب و زعفرانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کفن کردند و بسپردند غمناک</p></div>
<div class="m2"><p>غریبی را به غربت خانهٔ خاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسی بگریست شیرین بر غریبیش</p></div>
<div class="m2"><p>فزونتر زان ز بهر بی نصیبیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه در دست آمد آن نامهربان را</p></div>
<div class="m2"><p>که بی جرمی بکشت آن بی‌زبان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو نتوانست خونم را پی افگند</p></div>
<div class="m2"><p>گناهم را سیاست بر وی افگند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو فردا دست خون در دامن آید</p></div>
<div class="m2"><p>دیت بر خسرو و خون بر من آید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به خدمت بود فرتوتی کهنسال</p></div>
<div class="m2"><p>چو گردون در جهان سوزی شده زال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نگون پشتی ولیکن کژ خرامان</p></div>
<div class="m2"><p>مهی در سلخ و نامش ماه سامان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهر جا در مصیبت روفته جای</p></div>
<div class="m2"><p>بهر کو در عروسی کوفته پای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گشاده گریهٔ تزویر چون می</p></div>
<div class="m2"><p>هزاران اهرمن حل کرده در وی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فریب انگیزی از گیرائی گفت</p></div>
<div class="m2"><p>که کردی پشه و سیمرغ را جفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز داروها که کار آید زنان را</p></div>
<div class="m2"><p>ز ره برده بسی سیمین تنان را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مفرحها ز مروارید و از در</p></div>
<div class="m2"><p>که خوبان را برد هوش از بلا در</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گیاهانی به تسخیر ازموده</p></div>
<div class="m2"><p>بهر ذره دو صد ابلیس سوده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو در گوش آمدش گفتار شیرین</p></div>
<div class="m2"><p>به دندان خست لب زان کار شیرین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که بانو را پرستاری چو من پیش</p></div>
<div class="m2"><p>پس آنگه بهر ناچیزی دلش ریش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به فرما تا به یک پوشیده نیرنگ</p></div>
<div class="m2"><p>کنم صحرای عالم بر شکر تنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شکیبا کرد شیرین را فسونش</p></div>
<div class="m2"><p>نوازشها نمود از حد فزونش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به گرمی داد فرمان تا براند</p></div>
<div class="m2"><p>شکر را شربت شیرین چشاند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عجوز کاردان ز آنجا به تعجیل</p></div>
<div class="m2"><p>روان شد تا سپاهان میل بر میل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به چاره ره در ایوان شکر کرد</p></div>
<div class="m2"><p>چو موری کو به خوزستان گذر کرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بیامد تا بر شکر به صد نوش</p></div>
<div class="m2"><p>نهاد از مهربانی حلقه در گوش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو محرم شد همه شادی و غم را</p></div>
<div class="m2"><p>به مادر خواندگی بر زد علم را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز شیرین کاری جادو زن پیر</p></div>
<div class="m2"><p>مزاجش با شکر در خورد چون شیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پری روی از چنان جادو زبانی</p></div>
<div class="m2"><p>جدا بودن نیارستی زمانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گهیش از عشق خسرو راز گفتی</p></div>
<div class="m2"><p>گهش ز اندوه شیرین باز گفتی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عجوز فتنه باوی روی در روی</p></div>
<div class="m2"><p>درون رفته به شکر موی در موی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چنان افتاد وقتی فرصت کار</p></div>
<div class="m2"><p>که کرد آهنگ می سرو سمن بار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به قدر هفته‌ای در کامرانی</p></div>
<div class="m2"><p>پیاپی داشت دور دوستکانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بخار باده در سر کرد کارش</p></div>
<div class="m2"><p>صداع انگیز شد مغز از خمارش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فتادش در مزاج از رنج سستی</p></div>
<div class="m2"><p>به بیماری کشیدش تندرستی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز بالین جستن سرو خرامان</p></div>
<div class="m2"><p>به سامان کاری آمد ماه سامان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به تدبیر آستین بالید و بنشست</p></div>
<div class="m2"><p>همی آمیخت نیرنگی بهر دست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گمان بر اعتمادش بسته بیمار</p></div>
<div class="m2"><p>کبوتر نازک و شاهین ستم کار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو ناگه یافت آن فرصت که می جست</p></div>
<div class="m2"><p>به نوشین شربتی زهرش فرو شست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>قدح پر کرد و در دست شکر داد</p></div>
<div class="m2"><p>لبش را ز آخرین شربت خبر داد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو ماه نازنین کرد آن قدح نوش</p></div>
<div class="m2"><p>درون نازکش افتاد در جوش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خرابی یافت اندر قالبش راه</p></div>
<div class="m2"><p>ز پرواز از عدم شد جانش آگاه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نخست از بی خودی خود را بهش کرد</p></div>
<div class="m2"><p>وداع مادر فرزندکش کرد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>که رحمت بر تو باد ای مادر پیر</p></div>
<div class="m2"><p>که در زحمت نکردی هیچ تقصیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز تو آن سایه دیدم بر سر خویش</p></div>
<div class="m2"><p>که امیدم نبود از مادر خویش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کشد تقدیر جان کم نصیبان</p></div>
<div class="m2"><p>گنه بر مرگ و تهمت بر طبیبان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز من با شرط تعظیمی که دانی</p></div>
<div class="m2"><p>زمین بوسی به بزم خسروانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به مالی زیر پایش دیده غمناک</p></div>
<div class="m2"><p>بگوئی آسمان را قصهٔ خاک</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>که ما رفتیم با جان پر امید</p></div>
<div class="m2"><p>ترا جان تازه با دو عمر جاوید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>درین گفتن پلک در هم غنودش</p></div>
<div class="m2"><p>درامد خواب مرگ و در ربودش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز هر چشم انجمن را خون برآمد</p></div>
<div class="m2"><p>نفیر از انجم گردون بر آمد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جوان مردان به سرها خاک کردند</p></div>
<div class="m2"><p>عروسان آستین‌ها چاک کردند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز مژگان خلق خون دیده پالود</p></div>
<div class="m2"><p>برامد ناله‌های آتش آلود</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به خسرو نیز گشت آن قصه روشن</p></div>
<div class="m2"><p>که مهمان شد شکر در سبز گلشن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نشست از سوگواری با تنی چند</p></div>
<div class="m2"><p>به ماتم چاک زد پیراهنی چند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز نرگس بهر آن سرو خرامان</p></div>
<div class="m2"><p>به خاک افشاند در دامان به دامان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به صد تلخی ز شیرین کرد فریاد</p></div>
<div class="m2"><p>که به زین خواست نتوان خون فرهاد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>عمل‌ها را جزاها در کمین است</p></div>
<div class="m2"><p>جزای آن که من کردم همین است</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نکو را نیک و بد را بد شمار است</p></div>
<div class="m2"><p>به پاداش عمل گیتی به کار است</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو خسرو جرم خود را یافت پاداش</p></div>
<div class="m2"><p>پشیمان وار گشت از دیده خون پاش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>طمع یک بارگی برداشت از دوست</p></div>
<div class="m2"><p>رضا بی مغز گشت و کینه بی پوست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز ارمن در مدائن رفت غمناک</p></div>
<div class="m2"><p>ز حسرت کام خشک و دیده نمناک</p></div></div>