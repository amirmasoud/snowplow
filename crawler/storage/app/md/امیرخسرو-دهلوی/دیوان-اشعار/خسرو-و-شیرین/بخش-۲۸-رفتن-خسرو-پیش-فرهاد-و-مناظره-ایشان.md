---
title: >-
    بخش ۲۸ - رفتن خسرو پیش فرهاد و مناظرهٔ ایشان
---
# بخش ۲۸ - رفتن خسرو پیش فرهاد و مناظرهٔ ایشان

<div class="b" id="bn1"><div class="m1"><p>شهنشه گفت کز بخت دل افروز</p></div>
<div class="m2"><p>به جوی شیر خواهم رفت امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشید از تن لباس مرزبانان</p></div>
<div class="m2"><p>برون آمد بر آئین شتابان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن سو پرس پرسان کوه بر کوه</p></div>
<div class="m2"><p>به جوی شیر شد تنها ز انبوه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تماشا کرد لختی بر لب جوی</p></div>
<div class="m2"><p>بدید آن سنگها را روی در روی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر نقش هنر چون نقش بینی</p></div>
<div class="m2"><p>نظر می‌کرد و می گفت آفرینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو دید آن اوستادی را به بنیاد</p></div>
<div class="m2"><p>به بنیاد دگر شد سوی استاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جوانی دید در هیکل چو کوهی</p></div>
<div class="m2"><p>ز فر مهتران در وی شکوهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرامی پیکرش مانده خیالی</p></div>
<div class="m2"><p>چنان بدری ز غم گشته هلالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلا بیش از شمردن دیده جانش</p></div>
<div class="m2"><p>سزاوار شمردن استخوانش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رخش پر خون و سر تا پای پر خاک</p></div>
<div class="m2"><p>میان خاک و خون غلطیده غمناک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگفتش کیستی و در چه سازی</p></div>
<div class="m2"><p>بگفتا عاشقم در جان گدازی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفتش عشقبازی را نشان چیست</p></div>
<div class="m2"><p>بگفتا آنکه داند در بلا زیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگفتش عاشقان زین ره چه پویند</p></div>
<div class="m2"><p>بگفتا دل دهند و درد جویند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگفتش دل چرا با خود ندارند</p></div>
<div class="m2"><p>بگفتا خوبرویان کی گذارند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگفتش مذهب خوبان کدامست</p></div>
<div class="m2"><p>بگفتا کش فریب و عشوه نامست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بگفتش پیشهٔ دیگر چه دانند</p></div>
<div class="m2"><p>بگفتا غم دهند و جان ستانند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگفتش تلخی غم هیچ کم نیست</p></div>
<div class="m2"><p>بگفتا گر غم شیریسنت غم نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگفت از درویش چونی درین سوی</p></div>
<div class="m2"><p>بگفتا مردم از غم دور از آن روی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفتش بر تو اندازد گهی نور</p></div>
<div class="m2"><p>بگفت آری ولیکن چون مه از دور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگفت او را مبین تا زنده مانی</p></div>
<div class="m2"><p>بگفتا مرگ به زان زندگانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگفت ار زو به جان باشد زیانی</p></div>
<div class="m2"><p>بگفت ارزان بود جورش به جانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگفتش دور کن زان دوست یاری</p></div>
<div class="m2"><p>بگفت این نیست شرط دوست داری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگفت او شهر سوز و خامکار است</p></div>
<div class="m2"><p>بگفتا عشق را با این چکار است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بگفت از عشق او تا کی خوری غم</p></div>
<div class="m2"><p>بگفتا تا زیم در مردگی هم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگفتش گر بمیری در هوایش</p></div>
<div class="m2"><p>بگفتا در عدم گویم دعایش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بگفتش گر سرت برد به شمشیر</p></div>
<div class="m2"><p>بگفتا هم به سویش بینم از زیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بگفت ار خون تو ریزد جفایش</p></div>
<div class="m2"><p>بگفتا هم بمیرم در هوایش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بگفت آخر نه خونریزی وبالست</p></div>
<div class="m2"><p>بگفت ار دوست می‌ریزد حلالست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگفت ار بگذرد سوی تو ناگاه</p></div>
<div class="m2"><p>بگفت از دیده روبم پیش او راه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بگفتش گر نهد بر چشم تو پای</p></div>
<div class="m2"><p>بگفت از چشم در جان سازمش جای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بگفت ار بینیش در خواب قامت</p></div>
<div class="m2"><p>بگفتا بر نخیزم تا قیامت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بگفت آید گهی خوابت درین باب</p></div>
<div class="m2"><p>بگفت آری برادر خواندهٔ خواب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بگفت ار گوید از ناخن بکن سنگ</p></div>
<div class="m2"><p>بگفتا کاوم از مژگان به فرسنگ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بگفتش خوش بزی چند از غم دوست</p></div>
<div class="m2"><p>بگفتا چون زیم چون جان من اوست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بگفت از عشق جانت در هلاکست</p></div>
<div class="m2"><p>بگفتا عاشقان را زین چه باکست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زهر چش گفت دارای زمانه</p></div>
<div class="m2"><p>جوابی بازدادش عاشقانه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تعجب کرد شه زان استواری</p></div>
<div class="m2"><p>وزان سوزش به چندان پخته کاری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کسی کز عشق درد آشام باشد</p></div>
<div class="m2"><p>اگر پخته نباشد خام باشد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو دیدش کو وفا را پای دارد</p></div>
<div class="m2"><p>قدم در دوستی بر جای دارد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زبان را داشت زان جولان گری باز</p></div>
<div class="m2"><p>بر آئین دگر شد نکته پرداز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مزاجش را به پوزش راز پرسید</p></div>
<div class="m2"><p>وزان حال پریشان باز پرسید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که چونی وز کجا افتادت این روز</p></div>
<div class="m2"><p>که می سوزد دل من بر تو زین سوز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جوابش داد مرد غم سرشته</p></div>
<div class="m2"><p>که این بود از قضا بر من نبشته</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو باشد دست تقدیرم عناگیر</p></div>
<div class="m2"><p>کجا بیرون توانم شد ز تقدیر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بگفت دیده چون دل مایل افتاد</p></div>
<div class="m2"><p>بلای دیده لابد بر دل افتاد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ازین پیشم نبود این بانگ و فریاد</p></div>
<div class="m2"><p>که طبعم بنده بود و جانم آزاد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ملک گفت اندک اندک پر شد این سیل</p></div>
<div class="m2"><p>به پستی هم بران نسبت کند میل</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دل اندر چیز دیگر بند و می‌کوش</p></div>
<div class="m2"><p>کش از خاطر کنی عمدا فراموش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چنان آزاد گردی روزکی چند</p></div>
<div class="m2"><p>که ناری بیش یاد این مهر و پیوند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بگفت آن گه توان برجستن از چاه</p></div>
<div class="m2"><p>که تا زانو بود یا تا کمرگاه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگر چه هست شیرین جان مسکین</p></div>
<div class="m2"><p>ولیکن نیست شیرین‌تر ز شیرین</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو از دل رفت شیرین جان چه باشد</p></div>
<div class="m2"><p>چو خصم خانه شد مهمان که باشد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مرا تا جان بود ترکش نگیرم</p></div>
<div class="m2"><p>وگر میرم رها کن تا بمیرم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>منه بر جان من بندی که داری</p></div>
<div class="m2"><p>به خسرو گوی هر پندی که داری</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هر آن کس کو دهد دیوانه را پند</p></div>
<div class="m2"><p>نخوانندش خردمندان خردمند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گر از لعلش مرا روزیست جامی</p></div>
<div class="m2"><p>رسم زو عاقبت روزی به کامی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>وگر نبود ز بختم فتح بابی</p></div>
<div class="m2"><p>گدائی مرده گیر اندر خرابی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو لوح زندگانی شد ز من پاک</p></div>
<div class="m2"><p>چه خواهد ماندن از من پاره‌ای خاک</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تو خسرو را نصیحت کن در این درد</p></div>
<div class="m2"><p>که خواهد ماندن از تاج و نگین فرد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دل شه زین جواب آتش انگیز</p></div>
<div class="m2"><p>به جوش آمد چو دیگی ز آتش تیز</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به منزل شد ز کوهستان اندوه</p></div>
<div class="m2"><p>غبار کوه کن بر سینه چون کوه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز فرهاد آنچه در دل داشت حالی</p></div>
<div class="m2"><p>دل اندر پیش یاران کرد خالی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ندیمان کان سخن در گوش کردند</p></div>
<div class="m2"><p>نبد جای و سخن خاموش کردند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>فرو بستند لب در کار شیرین</p></div>
<div class="m2"><p>عجب ماندند از گفتار شیرین</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ملک گفت این وجود خاک بنیاد</p></div>
<div class="m2"><p>خرابم شد ز سنگ انداز فرهاد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>اگر خون ریزمش بر رسم شاهان</p></div>
<div class="m2"><p>مبارک نیست خون بی‌گناهان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ور این اندیشه را در خویش گیرم</p></div>
<div class="m2"><p>عجب نبود گر از غیرت بمیرم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بباید رفت راهم را به هنجار</p></div>
<div class="m2"><p>که پایم وارهد ز آشوب این خار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بزرگ امید گفت این سهل کاریست</p></div>
<div class="m2"><p>به مژگان خارم ار در پات خاریست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>روان کن هرزه گوئی را که در حال</p></div>
<div class="m2"><p>برو از مردن شیرین زند فال</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>اگر میرد فتوح خویش گیریم</p></div>
<div class="m2"><p>و گر نه کار دیگر پیش گیریم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خوش آمد شاه را آن چاره سازی</p></div>
<div class="m2"><p>نمودش مرگ آن بیچاره بازی</p></div></div>