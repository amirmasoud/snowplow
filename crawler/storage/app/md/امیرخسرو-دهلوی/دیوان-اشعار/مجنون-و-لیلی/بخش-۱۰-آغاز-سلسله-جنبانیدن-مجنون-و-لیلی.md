---
title: >-
    بخش ۱۰ - آغاز سلسله جنبانیدن مجنون و لیلی
---
# بخش ۱۰ - آغاز سلسله جنبانیدن مجنون و لیلی

<div class="b" id="bn1"><div class="m1"><p>دندانه گشای قفل این راز</p></div>
<div class="m2"><p>زین گونه در سخن کند باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کان روز که زاد قیس فرخ</p></div>
<div class="m2"><p>رخشنده شد آن قبیله را رخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان نور خجستهٔ شب افروز</p></div>
<div class="m2"><p>بر عامریان خجسته شد روز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنشست پدر به شادمانی</p></div>
<div class="m2"><p>بگشاد دری به مهمانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>واندر پس پرده مادرش نیز</p></div>
<div class="m2"><p>آراست ز صفه تا به دهلیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوبان قبیله را طلب کرد</p></div>
<div class="m2"><p>آفاق ز نغمه بر طرف کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جستند حکیم طالع اندیش</p></div>
<div class="m2"><p>کاگه کند از حکایت پیش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دانا بشمار خود نظر کرد</p></div>
<div class="m2"><p>گفت آنچه سر از شمار بر کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کاین طفل مبارک اختر خوب</p></div>
<div class="m2"><p>یوسف صفتی شود چو یعقوب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با آنکه ز گردش زمانه</p></div>
<div class="m2"><p>در فضل و هنر شود یگانه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لیکن فتدش گهٔ جوانی</p></div>
<div class="m2"><p>در سر هوسی، چنانکه دانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از عشق بتی نژند گردد</p></div>
<div class="m2"><p>دیوانه و مستمند گردد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اندیشه چنان کند به زارش</p></div>
<div class="m2"><p>کاز دست رود عنان کارش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مادر پدر از چنین شماری</p></div>
<div class="m2"><p>ماندند، دمی، به خار خاری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لیکن ز نشاط روی فرزند</p></div>
<div class="m2"><p>گشتند، بهر چه هست، خرسند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن نکته به سهل بر گرفتند</p></div>
<div class="m2"><p>و آیین طرب ز سر گرفتند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یک چند چو دور چرخ در گشت</p></div>
<div class="m2"><p>آن گلبن تر شگفته‌تر گشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سالش به شمار پنجم افتاد</p></div>
<div class="m2"><p>زو نور به چرخ و انجم افتاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شد تازه، چو نیم رسته سروی</p></div>
<div class="m2"><p>یا بال دمیده نو تذروی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نزد همه شد به هوشمندی</p></div>
<div class="m2"><p>چون مردم دیده، ز ارجمندی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زیرک دلیش چو باز خواندند</p></div>
<div class="m2"><p>در پیش معلمش نشاندند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دانای رقم ز بهر تعلیم</p></div>
<div class="m2"><p>کردش به کنار تخته تسلیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جهد ادبش بدان چه دانست</p></div>
<div class="m2"><p>می کرد چنانچ می توانست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آراسته مکتبی چو باغی</p></div>
<div class="m2"><p>هر لاله درو، چو شب چراغی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زین سوی نشسته کودکی چند</p></div>
<div class="m2"><p>آزاده و زیرک و خردمند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زان سوی ز دختران چون حور</p></div>
<div class="m2"><p>مسجد شده چون بهشت پر نور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر تازه رخی چو دستهٔ گل</p></div>
<div class="m2"><p>بر گل زده جنتهای سنبل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بود از صف آن بتان چون ماه</p></div>
<div class="m2"><p>ماهی، زده آفتاب را، راه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لیلی نامی که مه غلامش</p></div>
<div class="m2"><p>خالش نقطی ز نقش نامش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مشعل کش آفتاب و انجم</p></div>
<div class="m2"><p>دیوانه کن پری و مردم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سلطان شکر لبان آفاق</p></div>
<div class="m2"><p>لشکر شکن شکیب عشاق</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سر تا به قدم کرشمه و ناز</p></div>
<div class="m2"><p>هر سر کش حسن و هم سرانداز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نازی و هزار فتنه در دهر</p></div>
<div class="m2"><p>چشمی و هزار کشته در شهر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نی بت که چراغ بت پرستان</p></div>
<div class="m2"><p>طاوس بهشت و کبک بستان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اندر صف آن بتان شیرین</p></div>
<div class="m2"><p>چون زهره به ثور و مه به پروین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زانو زده قیس در دگر سوی</p></div>
<div class="m2"><p>هم چرب زبان و هم سخن گوی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نازک چو نهال نو دمیده</p></div>
<div class="m2"><p>خوش طبع و لطیف و آرمیده</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شیرین سخنی که هوش می‌برد</p></div>
<div class="m2"><p>رونق ز شکر فروش می‌برد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>وان لاله رخان ارغوان ساق</p></div>
<div class="m2"><p>نیز از دل و جانش گشته مشتاق</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ایشان همه را بقیس میلی</p></div>
<div class="m2"><p>وان سوخته در هوای لیلی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>لیلی خود ازو خراب جان تر</p></div>
<div class="m2"><p>گشته نفس از نفس گران‌تر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هر دو به نظاره روی در روی</p></div>
<div class="m2"><p>در رفته خیال موی در موی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>لب مانده ز گفت و زبان هم</p></div>
<div class="m2"><p>دل گشته بهم یکی و جان هم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>این زو به غم و گداز مانده</p></div>
<div class="m2"><p>دل بسته و دیده باز مانده</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>وان کرده نظر به روی این گرم</p></div>
<div class="m2"><p>وافگنده ز دیده برقع شرم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>این گفته غم خود از رخ زرد</p></div>
<div class="m2"><p>او داده جوابش از از دم سرد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>این دیده درو به چشم پاکی</p></div>
<div class="m2"><p>او نیز، ولی به شرمناکی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>این گشته به آب دیدگان مست</p></div>
<div class="m2"><p>او شسته ز جان خویشتن دست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>این کام خود از فغان خود دوخت</p></div>
<div class="m2"><p>او، سینهٔ خود، ز آه خود سوخت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سلطان خرد برون شد از تخت</p></div>
<div class="m2"><p>هم خانه به باد داد و هم رخت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فریاد شبان بمانده از کار</p></div>
<div class="m2"><p>میش آبله پای و گرگ خون‌خوار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مستان ز شراب خانه جسته</p></div>
<div class="m2"><p>خم بر سر محتسب شکسته</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مجنون ز نسیم آن خرابی</p></div>
<div class="m2"><p>شد بی خبر از تنگ شرابی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از خون جگر شراب می‌خورد</p></div>
<div class="m2"><p>وز پهلوی خود کباب می‌خورد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دزدیده درو نگاه می‌کرد</p></div>
<div class="m2"><p>می‌دید ز دور و آه می‌کرد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>می‌بود ز نیک و بد هراسش</p></div>
<div class="m2"><p>می‌داشت خرد هنوز پاسش</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>اندیشه هنوز خام بودش</p></div>
<div class="m2"><p>دل در غم ننگ و نام بودش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چون لاله، جبین شگفته می‌داشت</p></div>
<div class="m2"><p>داغی به جگر، نهفته می‌داشت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>می‌سوخت چو شمع با رخ زرد</p></div>
<div class="m2"><p>در گریه و سوز خنده می‌کرد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دانا رقمش به تخته می‌جست</p></div>
<div class="m2"><p>او تخته به آب دیده می‌شست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>استاد، سخن ز علم می‌راند</p></div>
<div class="m2"><p>او جمله کتاب عشق می‌خواند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>وان لعبت دردمند دل تنگ</p></div>
<div class="m2"><p>دل داده به باد و مانده بی سنگ</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>خون دلش از صفای سینه</p></div>
<div class="m2"><p>پیدا چو می اندر آب گینه</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بر چهره ز شرم پرده می‌دوخت</p></div>
<div class="m2"><p>و آتش به دلش گرفته می‌سوخت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>هر چند که غنچه بود سر بست</p></div>
<div class="m2"><p>می‌کرد ز بوی خلق را مست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بودند به زاری آن دو غم خوار</p></div>
<div class="m2"><p>در چنبر یکدگر گرفتار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>یاران که به هر کناره بودند</p></div>
<div class="m2"><p>دزدیده در آن نظاره بودند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>می‌کرد دو سینه جوش بر جوش</p></div>
<div class="m2"><p>می‌رفت دو قصه گوش بر گوش</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>این داشت فسانه در مدارا</p></div>
<div class="m2"><p>او گفت حکایت آشکارا</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>رازی که ز سینها بجوشد</p></div>
<div class="m2"><p>او باز کند گر این بپوشد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>باشد چو خریطه پر ز سوزن</p></div>
<div class="m2"><p>بندی دهنش، جهد ز روزن</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بر روی محیط پل توان بست</p></div>
<div class="m2"><p>نتوان لب خلق را زبان بست</p></div></div>