---
title: >-
    بخش ۱۷ - شنیدن لیلی، آوازهای دف تزویج مجنون، و ازان حرارت سوخته شدن
---
# بخش ۱۷ - شنیدن لیلی، آوازهای دف تزویج مجنون، و ازان حرارت سوخته شدن

<div class="b" id="bn1"><div class="m1"><p>گویندهٔ این کهن فسانه</p></div>
<div class="m2"><p>زان شعله چنین کشد زبانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کان شمع نهان گداز شب خیز</p></div>
<div class="m2"><p>پروانه صفت بر آتش تیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کبکی که شکسته بال باشد</p></div>
<div class="m2"><p>شاهین زندش چه حال باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون غم زده را در آن تحیر</p></div>
<div class="m2"><p>از خوردن غم درونه شد پر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس کانده سینه شد فزونش</p></div>
<div class="m2"><p>از دل به دهن رسید خونش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیمار دلش، به جان نگنجید</p></div>
<div class="m2"><p>جان خود چه، که در جهان نگنجید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد در پی آنکه دل بکاود</p></div>
<div class="m2"><p>وز غم قدری برون تراود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاغذ طلبید و خامه برداشت</p></div>
<div class="m2"><p>ترتیب سواد نامه برداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سودای جگر به نامه می‌ریخت</p></div>
<div class="m2"><p>خونابه ز نوک خامه می‌ریخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کاغذ چو تمام شد، نوردش</p></div>
<div class="m2"><p>از خون دو دیده مهر کردش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وانگه طلبید قاصدی چست</p></div>
<div class="m2"><p>کز باد به تک حریف می‌جست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دادش که: ببر بر آن خرابش</p></div>
<div class="m2"><p>باز آور به من رسان جوابش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قاصد شد و آن صحیفه را برد</p></div>
<div class="m2"><p>وآنجا که سپردنیست، بسپرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مجنون، که بدید نامهٔ دوست</p></div>
<div class="m2"><p>می‌خواست برون فتادن از پوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دید از قلم جراحت انگیز</p></div>
<div class="m2"><p>در دوده سرشته آتش تیز</p></div></div>