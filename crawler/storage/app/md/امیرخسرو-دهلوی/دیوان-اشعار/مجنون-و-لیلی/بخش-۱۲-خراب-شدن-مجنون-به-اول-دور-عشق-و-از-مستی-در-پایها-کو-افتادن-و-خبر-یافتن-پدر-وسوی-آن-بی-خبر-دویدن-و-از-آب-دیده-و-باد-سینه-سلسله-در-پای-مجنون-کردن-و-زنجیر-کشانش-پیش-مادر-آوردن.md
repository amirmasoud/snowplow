---
title: >-
    بخش ۱۲ - خراب شدن مجنون به اول دور عشق، و از مستی، در پایهاء کو افتادن، و خبر یافتن پدر، وسوی آن بی خبر دویدن، و از آب دیده و باد سینه سلسله در پای مجنون کردن، و زنجیر کشانش پیش مادر آوردن
---
# بخش ۱۲ - خراب شدن مجنون به اول دور عشق، و از مستی، در پایهاء کو افتادن، و خبر یافتن پدر، وسوی آن بی خبر دویدن، و از آب دیده و باد سینه سلسله در پای مجنون کردن، و زنجیر کشانش پیش مادر آوردن

<div class="b" id="bn1"><div class="m1"><p>چون ماند پری‌وش حصاری</p></div>
<div class="m2"><p>در حجرهٔ غم به سوگواری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قیس از هوس جمال دلبند</p></div>
<div class="m2"><p>در درس ادب دوید یک چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گوشهٔ صحن و کنج دیوار</p></div>
<div class="m2"><p>می‌کرد سرود عشق تکرار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی صرفه همی شتافت چون کور</p></div>
<div class="m2"><p>بی رشته همی تنید چون مور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آهی به جگر فرود می‌خورد</p></div>
<div class="m2"><p>والماس به سینه خرد می‌کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین گونه به چاره‌ای که دانست</p></div>
<div class="m2"><p>می‌کرد شکیب تاتوانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون سیل غمش رسید بر فرق</p></div>
<div class="m2"><p>از پرده برون فتاد چون برق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیرون شد و کرد پیرهن چاک</p></div>
<div class="m2"><p>و افگند به تارک از زمین خاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گریان به زمین فتاد بی تاب</p></div>
<div class="m2"><p>بر خاک، مراغه کرد چون آب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برداشت ز خانه راه صحرا</p></div>
<div class="m2"><p>چون خضر نمود میل خضرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌رفت چو باد کوه بر کوه</p></div>
<div class="m2"><p>خلقی ز پسش دوان به انبوه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کس ز لطافت جوانیش</p></div>
<div class="m2"><p>می‌خورد، فسوس زندگانیش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اینش ز درونه پند می‌داد</p></div>
<div class="m2"><p>وانش به جفا گزند می‌داد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طفلان به نظاره سنگ در دست</p></div>
<div class="m2"><p>اینش زد و آن شکست و آن خست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با این شغبی که در گذر بود</p></div>
<div class="m2"><p>دیوانه ز خویش بی خبر بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>می‌راند ز آب دیده رودی</p></div>
<div class="m2"><p>می‌گفت، چو بی‌دلان، سرودی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>می‌زد ز درون جان دم سرد</p></div>
<div class="m2"><p>زآن باد چو ریگ رقص می‌کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون گشت یقین که مرد دل ریش</p></div>
<div class="m2"><p>دارد سفری دراز در پیش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زین غم همه در گداز گشتند</p></div>
<div class="m2"><p>گریان به قبیله باز گشتند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رازش به زمانه عام کردند</p></div>
<div class="m2"><p>مجنون زمانش نام کردند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بردند خبر ز روزگارش</p></div>
<div class="m2"><p>سوی پدر بزرگوارش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کان رو که تو می‌فشاندیش گرد</p></div>
<div class="m2"><p>ز آسیب زمانه لطمه‌ای خورد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر در پی او شوی به پرواز</p></div>
<div class="m2"><p>باشد که هنوز یابیش باز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیر از خبری چنان جگر دوز</p></div>
<div class="m2"><p>زد نعرهٔ از درون پر سوز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خون از جگر دریده می‌ریخت</p></div>
<div class="m2"><p>نی نی که جگر ز دیده می‌ریخت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر جا جگرش به چشم تر بود</p></div>
<div class="m2"><p>کش دل سوی گوشه جگر بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از دم همه خون جگر همی کرد</p></div>
<div class="m2"><p>و ز بی جگری جگر همی خورد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اشکش به جگر نمک نه کم داشت</p></div>
<div class="m2"><p>گویی نمک و جگر بهم داشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وان مادر دردمند پر جوش</p></div>
<div class="m2"><p>کان قصه شنید گشت بی هوش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>غلطید به خاک تیره مویان</p></div>
<div class="m2"><p>آن گمشده را به خاک جویان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>موی از دل ناامید می‌کند</p></div>
<div class="m2"><p>پیچه ز سر سپید می‌کند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بیچاره پدر دوید بیرون</p></div>
<div class="m2"><p>همراه سرشک و همدمش خون</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>می‌رفت ز سوز دل شتابان</p></div>
<div class="m2"><p>فریاد کنان بهر بیابان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون گشت بسی به دشت و کهسار</p></div>
<div class="m2"><p>از کوه شنید نالهٔ زار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اندر پی آن ترانه زد گام</p></div>
<div class="m2"><p>افگنده ز اشک، باده در جام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دریافت حریف را چو مستان</p></div>
<div class="m2"><p>با زمزمهٔ هزار دستان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>می‌گفت دران فراق خون ریز</p></div>
<div class="m2"><p>با خود غزلی جراحت انگیز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون چشم پدر فتاد بر وی</p></div>
<div class="m2"><p>شد سست ز سختی غمش پی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون سوختگان دوید سویش</p></div>
<div class="m2"><p>بنشست به گریه پیش رویش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دیدش چو چراغ مرده بی‌نور</p></div>
<div class="m2"><p>دور از من و تو، ز خویشتن دور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون روی پدر بدید فرزند</p></div>
<div class="m2"><p>لختی دل پاره یافت پیوند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خم کرد تن ستم رسیده</p></div>
<div class="m2"><p>مالید به پای پیر دیده</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>پیر، از جگر کباب گشته</p></div>
<div class="m2"><p>رخ شست، به خون آب گشته</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بگریست برو به خسته جانی</p></div>
<div class="m2"><p>بوسید سرش به مهربانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>می‌سوخت به زاری از گزندش</p></div>
<div class="m2"><p>می‌داد ز سوز سینه پندش:</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کای شمع دل و چراغ دیده</p></div>
<div class="m2"><p>وی میوهٔ جان و باغ دیده</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>با آن خردی که داشت رایت،</p></div>
<div class="m2"><p>چون در وحل اوفتاد پایت؟</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>درد که نهاد بر تو این بار؟</p></div>
<div class="m2"><p>سودای که کرد با تو این کار؟</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>باد که وزید بر چراغت؟</p></div>
<div class="m2"><p>آه که به سینه کرد داغت؟</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بودم به گمان که گاه پیری</p></div>
<div class="m2"><p>مونس شوی ام به دستگیری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>رو در که کنم که در چنین سوز؟</p></div>
<div class="m2"><p>روزی به شب آرم اندرین روز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دریاب که عمر بر سر آمد</p></div>
<div class="m2"><p>طوفان اجل به سر در آمد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>پیری هوس جوانیم برد</p></div>
<div class="m2"><p>مرگ آمد و زندگانیم برد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چندین نه بس است تخلی دهر؟</p></div>
<div class="m2"><p>دیگر، چه کنی تو عیش من زهر؟</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>آتش که به شعله خوی دارد،</p></div>
<div class="m2"><p>روغن زدنش چه روی دارد؟</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>من خود ز زمانه پا براهم،</p></div>
<div class="m2"><p>تو رشته چه می‌بری به چاهم؟</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تنگست دلم، مپوی چندین</p></div>
<div class="m2"><p>دل تنگی من مجوی چندین</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ای جان پدر، به خانه باز آی</p></div>
<div class="m2"><p>وی مرغ، به آشیانه باز آی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بشتاب که نادرین غم آباد</p></div>
<div class="m2"><p>پیش از اجلم رسی به فریاد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زین پس که بجستنم شتابی</p></div>
<div class="m2"><p>جوئیم بسی، ولی نیابی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>وان مادر تو که در نقابست</p></div>
<div class="m2"><p>او هم ز غمت چو من خرابست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زان پیش که دیده را کند پیش،</p></div>
<div class="m2"><p>محروم مدارش از رخ خویش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ماییم دو تیره روز بی کس</p></div>
<div class="m2"><p>یک دیده به چشم ما تویی، بس</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مپسند که از جمال تو دور</p></div>
<div class="m2"><p>بی دیده شویم و بلکه بی نور</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>آخر پدر توام، نه اغیار</p></div>
<div class="m2"><p>بیگانه مشو چنین به یک بار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بیمار اگر چه دردناکست</p></div>
<div class="m2"><p>بیمار پرست در هلاکست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ز آنجا که یکیست خون و پیوند</p></div>
<div class="m2"><p>مرگ پدرست رنج فرزند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز آزردن دست و پا توان زیست،</p></div>
<div class="m2"><p>ز آزار جگر توان زیست؟</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>این جای نه جای تست، برخیز</p></div>
<div class="m2"><p>وین کار نه کار تست، بگریز</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گیرم که به غم زبون توان بود،</p></div>
<div class="m2"><p>بی خانه و جای، چون توان بود؟</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گر زآن منی، از آن من باش</p></div>
<div class="m2"><p>ور نه به مراد خویشتن باش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>هر چند که عشق جمله در دست</p></div>
<div class="m2"><p>نیرو شکن صلاح مردست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>مرد ار چه به سوزدش، همه تن</p></div>
<div class="m2"><p>دودی ندهد، برون ز روزن</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>مسپار بدست دیو تن را</p></div>
<div class="m2"><p>گرد آر عنان خویشتن را</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>زین غم همه گر مراد یارست</p></div>
<div class="m2"><p>غم هیچ مخور که در کنارست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>گر بر مه آسمان نهی هوش</p></div>
<div class="m2"><p>کوشم که رسانمت در آغوش</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>آن مه که دلت ازو خرابست</p></div>
<div class="m2"><p>لیلیست نه آخر آفتابست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ننشینم تا به چاره و رای</p></div>
<div class="m2"><p>با او ننشانمت به یک جای</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>لیکن نکنی چو دیو را بند</p></div>
<div class="m2"><p>دیوانه نشد سزای پیوند</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>این دیو دلی رها کن از خوی</p></div>
<div class="m2"><p>مردم شو و راه مردمی جوی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>تا بود که ز عون بخت پر نور</p></div>
<div class="m2"><p>هم خوابه شود فرشته با حور!</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>مجنون چو نوید کام بشنود</p></div>
<div class="m2"><p>بنشست ز مغزش اندکی دود</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>با پیر به شرم گفت گریان</p></div>
<div class="m2"><p>کای ز آتش من دل تو بریان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>از من به من آنچه یک گزندست</p></div>
<div class="m2"><p>دانم که ترا هزار چندست</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>لیکن چکنم، که نفس خود کام</p></div>
<div class="m2"><p>از حیله و دم نمی‌شود رام</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>خوگیر، که از بلا گریزم،</p></div>
<div class="m2"><p>از بند قضا کجا گریزم؟</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بی چاره وجود سست تدبیر</p></div>
<div class="m2"><p>مرغیست به ریسمان تقدیر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>آن روز که بودم از غم آزاد</p></div>
<div class="m2"><p>می‌بود برای خود دلم شاد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>و اکنون که نه بر فرار خویشم</p></div>
<div class="m2"><p>این هم نه باختیار خویشم</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>پروانهٔ شمع را که فرمود</p></div>
<div class="m2"><p>کاو از تن خود برآورد دود؟</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>آنک آفت آسمان نداند</p></div>
<div class="m2"><p>داند چو دران شکنجه ماند</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>گر کار به دست خویش بودی</p></div>
<div class="m2"><p>کار همه خلق پیش بودی</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چون نیست ز مردم آنچه زاید</p></div>
<div class="m2"><p>تسلیم شدم بهر چه آید</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>تا یاری جان به قالبم هست</p></div>
<div class="m2"><p>جان بدهم و یار ندهم از دست</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>با همسر او شوم چو افسر</p></div>
<div class="m2"><p>یا در سر کار او کنم سر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>های ای پدر من و سر من</p></div>
<div class="m2"><p>من گوهر تو تو افسر من</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>زین گونه که بهر من دویدی</p></div>
<div class="m2"><p>آزرده شدی و رنج دیدی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>غم خوارگیم فگندت از زیست</p></div>
<div class="m2"><p>ور تو نخوری غم، دگر کیست؟</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>زین غم چو مرا قرار بر تست</p></div>
<div class="m2"><p>غم زآن منست و بار بر تست</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>درد دل خسته را دوا کن</p></div>
<div class="m2"><p>وآن وعده که کرده‌ای وفا کن!</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>پذرفت پدر که سخت کوشد</p></div>
<div class="m2"><p>کالا خرد و درم فروشد</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>آن چاره کند که تا تواند</p></div>
<div class="m2"><p>دیوانه به ماه نور ساند</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>مجنون به وثیقتی چنان چست</p></div>
<div class="m2"><p>شد با پدر و رضای او جست</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>با هم دو ستم کش زمانه</p></div>
<div class="m2"><p>رفتند ز دشت سوی خانه</p></div></div>