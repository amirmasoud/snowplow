---
title: >-
    بخش ۹ - حکایت جوان مردی شیر خدا
---
# بخش ۹ - حکایت جوان مردی شیر خدا

<div class="b" id="bn1"><div class="m1"><p>بود یدالله بوغا در مصاف</p></div>
<div class="m2"><p>با یکی از کینه وران در طواف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حمله بسی کرد سوار دلیر</p></div>
<div class="m2"><p>گبر ستیزنده نیامد به زیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به چنان کش مکش از دستبرد</p></div>
<div class="m2"><p>شد ز دو سوالت پیکار خرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دو دلاور چون به کین آمدند</p></div>
<div class="m2"><p>گرم ز توسن به زمین آمدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست به هم بر زده زان داوری</p></div>
<div class="m2"><p>پای فشردند به زور آوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیدر کرار بسی کرد جهد</p></div>
<div class="m2"><p>کاختر دشمن بزمین برد مهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون گه آن شد که به خون کردنش</p></div>
<div class="m2"><p>دور کند بار سر از گردنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زد به دلیری سگ زور آزمای</p></div>
<div class="m2"><p>آب دهن بر رخ شیر خدای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخت به پیچید به خشم اژدها</p></div>
<div class="m2"><p>کرد ز ته صید مخالف رها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس که در آویخت درو خشمناک</p></div>
<div class="m2"><p>کان زده با دگر زد به خاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زد سرش از خنجر و سینه شکافت</p></div>
<div class="m2"><p>سر زده در پیش پیمبر شتافت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت رسولش که چو خصم درشت</p></div>
<div class="m2"><p>به رزمی آورد به صد حیله پشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چیست که بگرفتی و بگذاشتی</p></div>
<div class="m2"><p>بار دگر دست به خون داشتی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت نیوشندهٔ ایزد شناس</p></div>
<div class="m2"><p>کایزدم آورد به مغز این هراس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من چو شدم چیره بر آن سخت کوش</p></div>
<div class="m2"><p>آب دهن زد به رخ من ز جوش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در غضب آورد مرا نفس خام</p></div>
<div class="m2"><p>در دهن نفس نهادم لگام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کانچه غزا زین غضب آرام بجای</p></div>
<div class="m2"><p>بهر خودست این نه ز بهر خدای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گشت ضروری که رها کردمش</p></div>
<div class="m2"><p>پس ادب از بهر خدا کردمش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آنکه جهادش ز پی دین بود</p></div>
<div class="m2"><p>این کند و شرط غزا این بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرد غزا جز ز پی دین نکرد</p></div>
<div class="m2"><p>دید بسی خسرو اگر این نکرد</p></div></div>