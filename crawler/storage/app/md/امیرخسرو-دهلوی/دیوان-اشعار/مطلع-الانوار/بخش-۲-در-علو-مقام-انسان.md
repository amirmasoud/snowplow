---
title: >-
    بخش ۲ - در علو مقام انسان
---
# بخش ۲ - در علو مقام انسان

<div class="b" id="bn1"><div class="m1"><p>ای ز ازل گوهر پاک آمده</p></div>
<div class="m2"><p>گوهر تو زیور خاک آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنبر نه چرخ بسی بیخت خاک</p></div>
<div class="m2"><p>تا تو برون آمدی ای در پاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن خلقی تو که ز روز نخست</p></div>
<div class="m2"><p>کون به مهمانی شش روز تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود ز پدر گر چه کنون آمدی</p></div>
<div class="m2"><p>یا پدر از حجله برون آمدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دفتر معنی تو ز بر خواندهٔ</p></div>
<div class="m2"><p>تختهٔ اسما ز پدر خواندهٔ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرصهٔ عالم به مسافت تراست</p></div>
<div class="m2"><p>دولت آدم به خلافت تراست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعل دگرگون زده اسپ به طعن</p></div>
<div class="m2"><p>بر رخ ابلیس شده داغ لعن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرخ و زمین امر قضایت نبشت</p></div>
<div class="m2"><p>لوح و قلم سر هدایت نبشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حبل و رید تو فگنده بلند</p></div>
<div class="m2"><p>در شرف کنگر الله کمند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نور تو هنگامهٔ انجم شکست</p></div>
<div class="m2"><p>دست تو تسبیح ملایک گسست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان و جهان همه عالم تویی</p></div>
<div class="m2"><p>وانچه نگنجد به جهان هم تویی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هفت در از گوهر تیغ تو زنگ</p></div>
<div class="m2"><p>نه کمر از دور میان تو ننگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گنج خدا را تو کلید آمدی</p></div>
<div class="m2"><p>نز پی بازیچه پدید آمدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چرخ که از گوهر احسانت ساخت</p></div>
<div class="m2"><p>آیینه صورت رحمانت ساخت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آینه زینگونه که داری بچنگ</p></div>
<div class="m2"><p>آه و هزار آه که داری به زنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ور تو همان آب و گلی در سرشت</p></div>
<div class="m2"><p>پخته شو از مایه گلخن خشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرتبه‌ای جو که برانی به ماه</p></div>
<div class="m2"><p>کس نخورد شربت باران ز چاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بس که مه نور ره بالا گزید</p></div>
<div class="m2"><p>اول ذوالنون شد و پس بایزید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هیچ کسی ره سوی بالا نیافت</p></div>
<div class="m2"><p>تا قدم از همت والا نیافت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برنروی یک قدم از جای خویش</p></div>
<div class="m2"><p>تا ننهی بر دو جهان پای خویش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دیدهٔ اندیشه فلک بیز دار</p></div>
<div class="m2"><p>رخنه ببین پیک نظر تیز دار</p></div></div>