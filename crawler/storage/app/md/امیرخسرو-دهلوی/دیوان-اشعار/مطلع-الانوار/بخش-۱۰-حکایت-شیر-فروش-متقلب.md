---
title: >-
    بخش ۱۰ - حکایت شیر فروش متقلب
---
# بخش ۱۰ - حکایت شیر فروش متقلب

<div class="b" id="bn1"><div class="m1"><p>داشت شبانی رمه در کوهسار</p></div>
<div class="m2"><p>پیر و جوان گشته ازو شیر خوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیر که از بز به سبو ریختی</p></div>
<div class="m2"><p>آب در آن شیر درآمیختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بردی از آن آب ملمع به شیر</p></div>
<div class="m2"><p>نقرهٔ چون شیر ز برنا و پیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزی که آن کوه به صحرای خاک</p></div>
<div class="m2"><p>سیل درآمد رمه را برد پاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه جهان سوختهٔ شیر کرد</p></div>
<div class="m2"><p>سوخته شد ناگه از آن شیر سرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیر خنک از تف و تابش بسوخت</p></div>
<div class="m2"><p>جملهٔ آن شیر ز آبش بسوخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواجه چو شد با غم و آزار خفت</p></div>
<div class="m2"><p>کارشناسیش در آن کار گفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کان همه آب تو که در شیر بود</p></div>
<div class="m2"><p>شد همه سیل و رمه را در ربود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرد شبان زان سخن آمد ستوه</p></div>
<div class="m2"><p>ماند سرافگنده چو سیلاب کوه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خسرو اگر دین طلبی از خدای</p></div>
<div class="m2"><p>زین دل خاین به دیانت گرای</p></div></div>