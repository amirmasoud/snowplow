---
title: >-
    شمارهٔ ۲۷۵
---
# شمارهٔ ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>خوش بود آن بیدلی کز غم امانیش نیست</p></div>
<div class="m2"><p>مرده بود آن دلی کاه و فغانیش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر خدا، ای جوان، تا بتوانی مدار</p></div>
<div class="m2"><p>حرمت پیری که میل سوی جوانیش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاش نبودی مرا تهمت جایی به تن</p></div>
<div class="m2"><p>کش اگر از یار امان، از غم امانیش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سینه که بیدل بماند آه و فغانیش هست</p></div>
<div class="m2"><p>دل که ز هجران بسوخت نام و نشانیش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوسه به قیمت دهد، جان ببرد رایگان</p></div>
<div class="m2"><p>قیمت بوسیش هست، منت جانیش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرو قدا، رد مکن گریه زارم، ازآنک</p></div>
<div class="m2"><p>خشک بود آن چمن کاب روانیش نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر دم سردی کشم، روی مگردان ز من</p></div>
<div class="m2"><p>نیست گلی کاندرو باد خزانیش نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پسته بسته دهن پیش دهانت گهی</p></div>
<div class="m2"><p>لب ز سخن تر نکرد کاب دهانیش نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قصه خسرو بخوان، چون تو درون دلی</p></div>
<div class="m2"><p>گر ز همه کس نهانست، از تو نهانیش نیست</p></div></div>