---
title: >-
    شمارهٔ ۱۱۸۶
---
# شمارهٔ ۱۱۸۶

<div class="b" id="bn1"><div class="m1"><p>کسی که هست نظر بر جمال میمونش</p></div>
<div class="m2"><p>زهی نشاط دل و طالع همایونش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آب خضر که محلول اوست مایه لطف</p></div>
<div class="m2"><p>که در لطافت محلول ریخت بی چونش؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوس ندید که خورشید و ماه خاک شوند</p></div>
<div class="m2"><p>در آن زمین که زند گام سم گلگونش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به یک حدیث کند تلخی غمش همه محو</p></div>
<div class="m2"><p>چو زهر ناب که جادو کند به افسونش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غلام آن نفسم کامدم به خانه او</p></div>
<div class="m2"><p>به خشم گفت که از در کنید بیرونش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز غمزه گر چه کشش بی دریغ می کند او</p></div>
<div class="m2"><p>حیات خواهم با او همه برافزونش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصال عشق به صدق آن بود که چون لیلی</p></div>
<div class="m2"><p>به خاک رفت، در آغوش خفت مجنونش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوشم ز گریه چشمم، اگر چه غم زاید</p></div>
<div class="m2"><p>ز چاشنی مفرح ز در مکنونش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد از تو خون دل خسرو آب، شادم، از آنک</p></div>
<div class="m2"><p>نماز از خوی پا شستن تو شد خونش</p></div></div>