---
title: >-
    شمارهٔ ۱۸۹۹
---
# شمارهٔ ۱۸۹۹

<div class="b" id="bn1"><div class="m1"><p>تو، ای پسر، که از این سو سوار می گذری</p></div>
<div class="m2"><p>مرا کش ارز که برای شکار می گذری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دوستان که به جولانگه تو خاک شدند</p></div>
<div class="m2"><p>به شوخی تو که ای شرمسار می گذری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار دل به دوال عنایت آویزان</p></div>
<div class="m2"><p>تو بر شکسته از ایشان سوار می گذری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جراحتی به جز این نیست آشنایان را</p></div>
<div class="m2"><p>که آشنایی و بیگانه وار می گذری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه مرهمی که فزون است در دم، ار چه دمی</p></div>
<div class="m2"><p>هزار بار به جان فگار می گذری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو مست خراب چه دانی که تا چه می گذرد؟</p></div>
<div class="m2"><p>در آن دلی که به شبهای تار می گذری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو در درون دل تنگ من خلی همه شب</p></div>
<div class="m2"><p>گلی، ولی به دلم همچو خار می گذری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قرار وصل خوش است ار چه دیر می بینم</p></div>
<div class="m2"><p>ولی چه سود که زود از قرار می گذری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلاست ناله خسرو، برون میا زین بیش</p></div>
<div class="m2"><p>که مست می رسی و در خمار می گذری</p></div></div>