---
title: >-
    شمارهٔ ۱۰۹۰
---
# شمارهٔ ۱۰۹۰

<div class="b" id="bn1"><div class="m1"><p>ای ترا در زیر هر لب شکرستانی دگر</p></div>
<div class="m2"><p>جز لبت ما را نمک ندهد نمکدانی دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من غم دل گویم و تو همچنان مشغول ناز</p></div>
<div class="m2"><p>تو به شهری دیگر و من در بیابانی دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من به تو حیران، تو می گویی که پیمان تازه کن</p></div>
<div class="m2"><p>بار اول عمر و آنگه عهد و پیمانی دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وه که چندان جان محنت کش مرا سوزی، بسوز</p></div>
<div class="m2"><p>خانه خالی کن که آدم باز مهمانی دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من در ین سودا ز جان خویشتن سیر آمدم</p></div>
<div class="m2"><p>آنکه زو سیری نیاید هست او جانی دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان لب چون آب حیوان کشته شد شهری تمام</p></div>
<div class="m2"><p>ای خضر، بنما، اگر هست آب حیوانی دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر دل من غارت کافر میارید، ای بتان</p></div>
<div class="m2"><p>زانکه بود این کافرستان را مسلمانی دگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چه ممکن بود کردم چاره و درمان خویش</p></div>
<div class="m2"><p>بعد ازین جز جان سپردن نیست درمانی دگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با چنین خونابه دست از چشمها، خسرو، بشوی</p></div>
<div class="m2"><p>زانکه این خانه نیارد تاب بارانی دگر</p></div></div>