---
title: >-
    شمارهٔ ۴۷۱
---
# شمارهٔ ۴۷۱

<div class="b" id="bn1"><div class="m1"><p>دل باز به جوش آمد، جانان که می آید</p></div>
<div class="m2"><p>بیمار به هوش آمد، در مان که می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وه جان کسان هر سو، صد قلب روان از پس</p></div>
<div class="m2"><p>خوانیش چنین لشکر، سلطان که می آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل، تو نمی گفتی کاینک ز پی مردن</p></div>
<div class="m2"><p>اسباب مهیا کن آن جان که می آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان خال و خط مشکین با جمله بلا دیدم</p></div>
<div class="m2"><p>این آیت رحمت بین در شان که می آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ترک، مگو آخر بهر دل مسکینی</p></div>
<div class="m2"><p>کز سوی تو بر جانم پیکان که می آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود نامه خویش آورد از بهر قصاص من</p></div>
<div class="m2"><p>سر خاک ره قاصد فرمان که می آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیل مژه را رخنه انباشه شد، یارب</p></div>
<div class="m2"><p>کان آب به چشم من تازان که می آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسرو به رهش باری قربان شد و بریان هم</p></div>
<div class="m2"><p>تا باز ببین کان هم مهمان که می آید</p></div></div>