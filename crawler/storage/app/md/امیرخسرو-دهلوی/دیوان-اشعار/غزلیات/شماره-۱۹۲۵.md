---
title: >-
    شمارهٔ ۱۹۲۵
---
# شمارهٔ ۱۹۲۵

<div class="b" id="bn1"><div class="m1"><p>زهی رویت شکفته لاله زاری</p></div>
<div class="m2"><p>در حسن ترا گل پرده داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخت را بهتر از مه می شمارم</p></div>
<div class="m2"><p>وزین بهتر نمی دانم شماری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درخت صندل آمد قامت تو</p></div>
<div class="m2"><p>که می پیچد در او زلفت چو ماری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روان کردی سمند کامران را</p></div>
<div class="m2"><p>نترسیدی که برخیزد غباری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دنبالت روان شد آب چشمم</p></div>
<div class="m2"><p>که ریزد بر سر راهت نثاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو خود رفتی به تسکین دل من</p></div>
<div class="m2"><p>خیال خویش را بفرست باری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخواهم یادگاری از تو، لیکن</p></div>
<div class="m2"><p>خیال است اینکه بدهی یادگاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم یک چند بود اندر پس کار</p></div>
<div class="m2"><p>فراقت باز پیش آورد کاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گلی نشکفته بختم را ز وصلت</p></div>
<div class="m2"><p>ز غم هر موی بر تن گشت خاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز شاخ وصل چون برگی ندارم</p></div>
<div class="m2"><p>بخواهم از جناب شاه باری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بحر نظم خسرو در نثارت</p></div>
<div class="m2"><p>کشد هر لحظه در شاهسواری</p></div></div>