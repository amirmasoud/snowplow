---
title: >-
    شمارهٔ ۷۴۶
---
# شمارهٔ ۷۴۶

<div class="b" id="bn1"><div class="m1"><p>هرکسی روز وداع از پی محمل می‌شد</p></div>
<div class="m2"><p>تو مپندار که آن دلبرم از دل می‌شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ منزل نشود قافله از آب جدا</p></div>
<div class="m2"><p>زان که پیش از همه سیلاب به منزل می‌شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم، از محمل آن جان جهان برگردم</p></div>
<div class="m2"><p>پایم از خون دل سوخته در گل می‌شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساربان خیمه به صحرا زد و اینم عجب است</p></div>
<div class="m2"><p>که قیامت نشد آن روز که محمل می‌شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راستی هر که در آن شکل و شمایل می‌دید</p></div>
<div class="m2"><p>همچو من فتنه در آن شکل و شمایل می‌شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پند عاقل نکند سود که در بند فراق</p></div>
<div class="m2"><p>دل دیوانه ندیدیم که عاقل می‌شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگذر از خویش که بی‌طبع مسالک، خسرو</p></div>
<div class="m2"><p>هیچ سالک نشنیدیم که واصل می‌شد</p></div></div>