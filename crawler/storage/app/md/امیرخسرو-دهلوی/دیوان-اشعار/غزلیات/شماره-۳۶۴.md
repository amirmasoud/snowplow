---
title: >-
    شمارهٔ ۳۶۴
---
# شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>ترک مستم که قصد ایمان داشت</p></div>
<div class="m2"><p>چشم او میل غارت جان داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون من چون شراب می جوشد</p></div>
<div class="m2"><p>وز دلم هم کباب بریان داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده در می فشاند در دامن</p></div>
<div class="m2"><p>گوییا آستین مرجان داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در باغ بهشت بگشادند</p></div>
<div class="m2"><p>باد گویی کلید رضوان داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه دیدم که از نسیم صبا</p></div>
<div class="m2"><p>همچو من دست در گریبان داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رازم از پرده برملا افتاد</p></div>
<div class="m2"><p>چند شاید به صبر پنهان داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسروا، ترک جان بباید گفت</p></div>
<div class="m2"><p>که به یک دل دو دوست نتوان داشت</p></div></div>