---
title: >-
    شمارهٔ ۳۰۳
---
# شمارهٔ ۳۰۳

<div class="b" id="bn1"><div class="m1"><p>زیر کله نمونه روی تو مه نداشت</p></div>
<div class="m2"><p>کس ماه را نمونه به زیر کله نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگرفت چارسوی رخت زلف و هیچ وقت</p></div>
<div class="m2"><p>یک شب جهان چو روی تو در چارده نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ضبط آفتاب نشد ملک نیم روز</p></div>
<div class="m2"><p>کز زلف عنبرین تو قیر سیه نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش آتشی به سینه همی زد هوای تو</p></div>
<div class="m2"><p>بگریخت اشک و سوخته شد دل چو ره نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خونم بخورد و چشم تو لب تر نکرد، ازآنک</p></div>
<div class="m2"><p>دود دگر نوشت و خط تو نگه داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با این همه وفای تو دارد میان جان</p></div>
<div class="m2"><p>دل خود ز دست رفت، چو او کس نگه داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خون نوشته ام به دو رخ ماجرای عشق</p></div>
<div class="m2"><p>از بس که در سفینه دل جایگه نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک وعده تو در حق خسرو به سر نشد</p></div>
<div class="m2"><p>گویی که باد بود که بار گنه نداشت</p></div></div>