---
title: >-
    شمارهٔ ۱۱۹۲
---
# شمارهٔ ۱۱۹۲

<div class="b" id="bn1"><div class="m1"><p>لب نگر وان دهان خندانش</p></div>
<div class="m2"><p>وان خم طره پریشانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی چون بامداد تابستان</p></div>
<div class="m2"><p>زلف همچون شب زمستانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیر بالای او بخست مرا</p></div>
<div class="m2"><p>از گشاد ره گریبانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامن از ما همی کشد امروز</p></div>
<div class="m2"><p>چنگ ما روز حشر و دامانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوفته ماند شخص چون زر من</p></div>
<div class="m2"><p>از دل سخت همچو سندانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون فرو برد در دلم دندان</p></div>
<div class="m2"><p>جان فرستم به مزد دندانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل من گشت خون و خون دلم</p></div>
<div class="m2"><p>آب شد در چه زنخدانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسروا، پرسشی بکن که به دل</p></div>
<div class="m2"><p>خار دارم ز نوک مژگانش</p></div></div>