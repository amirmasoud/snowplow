---
title: >-
    شمارهٔ ۱۳۱۳
---
# شمارهٔ ۱۳۱۳

<div class="b" id="bn1"><div class="m1"><p>بسیار خواهم از نظر تا روی او یکسو کنم</p></div>
<div class="m2"><p>می خواست چشمم سوی او، از چه دگر سو رو کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر می ندانم کز وفا دور است خوی نازکت</p></div>
<div class="m2"><p>این چشم خون پالای را در چشم آن بدخو کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چار سوی آرزو کاری ست با رویت مرا</p></div>
<div class="m2"><p>رو سوی من کن یک زمان تا کار خود یکسو کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پهلو کنم از غم که او بشکست پهلوی مرا</p></div>
<div class="m2"><p>من خود سگم گر فی المثل، شیرم ز غم پهلو کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیماریی دارم نهان زان نرگس جادوی تو</p></div>
<div class="m2"><p>دردم زیادت می شود هر چند می دارو کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نگذارند زلف تو بویی، به جایش جا کنم</p></div>
<div class="m2"><p>هر جا که زلفت بگذرد، خاک زمین را بو کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو همه تن موی شد در آرزوی روی تو</p></div>
<div class="m2"><p>یک مویت از سر کم شود، این را به جای او کنم</p></div></div>