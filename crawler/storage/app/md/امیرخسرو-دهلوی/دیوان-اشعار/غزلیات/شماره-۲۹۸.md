---
title: >-
    شمارهٔ ۲۹۸
---
# شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>لشکر کشید عشق و دلم ترک جان گرفت</p></div>
<div class="m2"><p>صبر گریز پای سر اندر جهان گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که ترک من کن و آزاد شو ز غم</p></div>
<div class="m2"><p>آسان به ترک همچو تویی چون توان گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای آشنا که گریه کنان پند می دهی</p></div>
<div class="m2"><p>آب از برون مریز که آتش به جان گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظاره هم نکرد گه سوختن مرا</p></div>
<div class="m2"><p>آن کس که آتشم زد و از من کران گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در طوق بندگیش رود دل به عاقبت</p></div>
<div class="m2"><p>هر فاخته که خدمت سرو روان گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اکنون که تازیانه هجران کشید دل</p></div>
<div class="m2"><p>جان رمیده را که تواند عنان گرفت؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو کز اوست تشنه شمشیر آبدار</p></div>
<div class="m2"><p>ز آتش چه غم که دشمنش اندر زبان گرفت</p></div></div>