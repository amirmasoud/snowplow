---
title: >-
    شمارهٔ ۱۳۳۰
---
# شمارهٔ ۱۳۳۰

<div class="b" id="bn1"><div class="m1"><p>هر شبی چون یاد آن رخسار گلناری کنم</p></div>
<div class="m2"><p>تا به وقت صبح از مژگان گهرباری کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاه از تف دهان دامن بسوزم زهد را</p></div>
<div class="m2"><p>گه ز دود سینه سقف آسمان تاری کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیر مژگانش به جانم تا رسید از نوک آه</p></div>
<div class="m2"><p>زخمها هر صبح در نه طاق زنگاری کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تمنای جفای او به خونریزم بود</p></div>
<div class="m2"><p>شحنه غم را به خون خویش هم یاری کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ضربت غم می خورم سلطانی آسا تا به کی</p></div>
<div class="m2"><p>قبله جان روی آن رخسار گلناری کنم</p></div></div>