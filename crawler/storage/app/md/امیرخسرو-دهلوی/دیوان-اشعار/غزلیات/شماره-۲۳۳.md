---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>ساقیا، باده ده امروز که جانان اینجاست</p></div>
<div class="m2"><p>سر گلزار نداریم که بستان اینجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دگرم نقل و شرابی نبود، گو کم باش</p></div>
<div class="m2"><p>گریه تلخ و شکر خنده پنهان اینجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناله چندین مکن، ای فاخته، کامشب در باغ</p></div>
<div class="m2"><p>با گلی ساز که آن سرو خرامان اینجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم ز در باز رو، ای باد و نسیم گل را</p></div>
<div class="m2"><p>باز بر باز که آن غنچه خندان اینجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یار در سینه و من در سکرات اجلم</p></div>
<div class="m2"><p>دست در سینه من سای و ببین جان اینجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواه، ای جان، برو و خواه همی باش که من</p></div>
<div class="m2"><p>مردنی نیستم امروز که جانان اینجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای مگس، چند به گرد لب آن مست پری</p></div>
<div class="m2"><p>کنجهای دهنش بین شکرستان اینجاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خنده ضایع مکن، ای کان نمک، در هر جای</p></div>
<div class="m2"><p>پاره های جگر سوخته بریان اینجاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سالها آن دل گم گشته که جستی، خسرو</p></div>
<div class="m2"><p>هم همین جاش طلب، زلف پریشان اینجاست</p></div></div>