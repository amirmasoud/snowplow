---
title: >-
    شمارهٔ ۱۰۳۴
---
# شمارهٔ ۱۰۳۴

<div class="b" id="bn1"><div class="m1"><p>یاران که بوده اند ندانم کجا شدند؟</p></div>
<div class="m2"><p>یارب، چه روز بود که از ما جدا شدند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نوبهار آید و پرسد ز دوستان</p></div>
<div class="m2"><p>گو، ای صبا، که آن همه گلها گیا شدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای گل، چو آمدی ز زمین گو چگونه اند؟</p></div>
<div class="m2"><p>آن رویها که در ته گرد فنا شدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن سروران که تاج سر خلق بوده اند</p></div>
<div class="m2"><p>اکنون نظاره کن که همه خاک پا شدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دنیاست خونبهای بسی خلق، وین زمان</p></div>
<div class="m2"><p>بسیار کس که بر سر این خونبها شدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خورشید بوده اند که رفتند زیر خاک</p></div>
<div class="m2"><p>آن ذره ها که چون همه اندر هوا شدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنها که سیمیای جهان شان فریب داد</p></div>
<div class="m2"><p>بگذاشتند کنج و پی کیمیا شدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بازیچه ایست طفل فریب این متاع دهر</p></div>
<div class="m2"><p>بی عقل مردمان که بدین مبتلا شدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کس را چه شد که نقد مرادی نمی رسد</p></div>
<div class="m2"><p>مانا که خازنان فلک بینوا شدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خسرو، گریز کن که وفا نیست در جهان</p></div>
<div class="m2"><p>زاهل جهان که همچو جهان بی وفا شدند</p></div></div>