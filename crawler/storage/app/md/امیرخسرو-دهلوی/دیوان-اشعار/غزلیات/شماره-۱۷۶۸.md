---
title: >-
    شمارهٔ ۱۷۶۸
---
# شمارهٔ ۱۷۶۸

<div class="b" id="bn1"><div class="m1"><p>به گردت باد سردی هر دم از عشاق دیوانه</p></div>
<div class="m2"><p>پریشانی زلفت را فراهم کی کند شانه؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلای جان شدی و من هم اول روز دانستم</p></div>
<div class="m2"><p>که روزی بهر ما فتنه شود آن شکل ترکانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا خود شورشی بوده ست، عشقت یار شد با آن</p></div>
<div class="m2"><p>حدیث من بدان ماند که دیوان کار دیوانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل و جان گر چه با من صحبتی دارند دیرینه</p></div>
<div class="m2"><p>ولیکن چون زیم بی دوست با این چند بیگانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بدنامی و رسوایی اسیران را مزن طعنه</p></div>
<div class="m2"><p>تو، ای زاهد، ندیده ستی بلای چشم مستانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه یاران به گشت باغ و میل من به کنج غم</p></div>
<div class="m2"><p>یکی زندان نماید بوستان بر مرغ ویرانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگون کن، ساقیا، خم را که این آتش که من می دارم</p></div>
<div class="m2"><p>به دریا نیز ننشیند، چه جای طاس و پیمانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اثر در جانست مستی را اگر در آب و گل بودی</p></div>
<div class="m2"><p>سبو را مست و غلطان دیدمی در صحن میخانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرم خون ریزد آن سلطان، فدای بندگان او</p></div>
<div class="m2"><p>که عاشق کز بلا ترسد نباشد مرد مردانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گه کشتن بود در پیش خوبان رونق عاشق</p></div>
<div class="m2"><p>به گاه جانفروشی گرمی بازار پروانه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شب خسرو همه در قصه خوبان به روز آمد</p></div>
<div class="m2"><p>سگان را در نفیر و پاسبانان را در افسانه</p></div></div>