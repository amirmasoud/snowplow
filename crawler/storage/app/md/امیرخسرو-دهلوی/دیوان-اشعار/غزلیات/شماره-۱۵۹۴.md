---
title: >-
    شمارهٔ ۱۵۹۴
---
# شمارهٔ ۱۵۹۴

<div class="b" id="bn1"><div class="m1"><p>چنین که بی تو زمان نمی توان بودن</p></div>
<div class="m2"><p>نه مردمی بود از چشم ما نهان بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دمی به سوی من آی، ار چه عیب شاهان است</p></div>
<div class="m2"><p>به کنج محنت درویش میهمان بودن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دیده گوهر و در بر درت فشانم، از آنک</p></div>
<div class="m2"><p>نه دوستیست به کوی تو رایگان بودن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبور بودنم از دیدن رخت گویند</p></div>
<div class="m2"><p>چرا ز دیده نباشم، اگر توان بودن؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سینه ام نه همانا برون روی همه عمر</p></div>
<div class="m2"><p>چنین که خوی شدت در میان جان بودن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ملامتت نکنم گر جفا کنی، زیراک</p></div>
<div class="m2"><p>رها نمی کندت حسن مهربان بودن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بند سخت شدن، در شکنجه جان دادن</p></div>
<div class="m2"><p>از آن به است که در بند نیکوان بودن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طریق بوالهوسان است نی ره عشاق</p></div>
<div class="m2"><p>ز عشق لاف، پس از فتنه بر کران بودن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مپرس قصه خسرو، چه جای پرس آن را</p></div>
<div class="m2"><p>که حیرت رخت آموخت بی زبان بودن</p></div></div>