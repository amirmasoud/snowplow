---
title: >-
    شمارهٔ ۱۸۷۸
---
# شمارهٔ ۱۸۷۸

<div class="b" id="bn1"><div class="m1"><p>بت من، بت پرست را چه زنی؟</p></div>
<div class="m2"><p>مستم از عشق، مست را چه زنی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی خود پوش، چشم را چه کنی</p></div>
<div class="m2"><p>بت شکن، بت پرست را چه زنی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آخر از شست دور کن یک تیر</p></div>
<div class="m2"><p>به یکی تیر شست را چه زنی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالمی در رهت نشسته بماند</p></div>
<div class="m2"><p>راه اهل نشست را چه زنی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من که بر آستانت پست شدم</p></div>
<div class="m2"><p>لگد قهر، پست را چه زنی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون زبردست را نیاری زد</p></div>
<div class="m2"><p>خود بگو زیر دست را چه زنی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیغ بهر شکست کافر زن</p></div>
<div class="m2"><p>خسرو پر شکست را چه زنی؟</p></div></div>