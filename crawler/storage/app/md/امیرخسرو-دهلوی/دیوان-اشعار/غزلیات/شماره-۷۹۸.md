---
title: >-
    شمارهٔ ۷۹۸
---
# شمارهٔ ۷۹۸

<div class="b" id="bn1"><div class="m1"><p>گر سخن زان لب چون نوش شود</p></div>
<div class="m2"><p>پسته را خنده فراموش شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور حدیث در دندانت کنم</p></div>
<div class="m2"><p>صدف آنجا همه تن گوش شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آسمان روی تو گر مه بیند</p></div>
<div class="m2"><p>بر زمین افتد و بیهوش شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل که از روی تو ریزد به سخن</p></div>
<div class="m2"><p>گر بچینند یک آغوش شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باده بر یاد لب شیرینت</p></div>
<div class="m2"><p>همه گر زهر بود، نوش شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل که پوشیده به زلفت پیوست</p></div>
<div class="m2"><p>ترسم از غم که سیه پوش شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوش با مات سری خوش بوده ست</p></div>
<div class="m2"><p>خوش بود امشب، اگر دوش شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر کنی میل به سوی خسرو</p></div>
<div class="m2"><p>شاه کی همدم جادوش شود</p></div></div>