---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>طاقت دوری نماند عاشق دلتنگ را</p></div>
<div class="m2"><p>واگهیی کس نداد، آن پسر شنگ را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاه خرامیدنش یک نظری هر که دید</p></div>
<div class="m2"><p>پیش فرامش نکرد آن قد و آن رنگ را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنده نخواند کنون جز غزل نوخطان</p></div>
<div class="m2"><p>کاب دو چشمم بشست دفتر فرهنگ را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشک من گوژ پشت دید گه ناله چرخ</p></div>
<div class="m2"><p>گفت که ای خوش نوا، ترک مکن چنگ را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست شکسته دلم، خواست شکستن بتر</p></div>
<div class="m2"><p>سخت گره بر مزن گیسوی شبرنگ را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوش ز یاد رخت اشک جگر سوز من</p></div>
<div class="m2"><p>شد به هوا پر بسوخت، مرغ شب آهنگ را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با دل سنگیت هیچ کرد نیارم همی</p></div>
<div class="m2"><p>گر چه که از تیر آه رخنه کنم سنگ را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بکنی آشتی جان بفروشم و لیک</p></div>
<div class="m2"><p>تو به بها می خری جان کسی جنگ را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در طلبت عاشقان گر قدم از سر کنند</p></div>
<div class="m2"><p>هیچ نپرسند باز منزل و فرسنگ را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوش پسرا، چشم تست تنگ و من اندر عجب</p></div>
<div class="m2"><p>باز کجا می کشی این همه نیرنگ را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرد جهان شد سمر قصه خسرو و لیک</p></div>
<div class="m2"><p>عشق به صحرا نهاد راز دل تنگ را</p></div></div>