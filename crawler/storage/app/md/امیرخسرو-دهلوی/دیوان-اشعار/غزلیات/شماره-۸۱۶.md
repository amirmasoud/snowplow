---
title: >-
    شمارهٔ ۸۱۶
---
# شمارهٔ ۸۱۶

<div class="b" id="bn1"><div class="m1"><p>من دلبری ندیدم کش زین نهاد باشد</p></div>
<div class="m2"><p>زین فتنه ها دلم را بسیار یاد باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذشت دی به شادی و امروز نامرادی</p></div>
<div class="m2"><p>آری نه کارها را دایم مراد باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نزلی دگر طلب کن، ای دل، ز کویش ایرا</p></div>
<div class="m2"><p>در شهر عشقبازان غم خانه زاد باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آید به عشق پیدا مردی که غازیان را</p></div>
<div class="m2"><p>میدان تیغ بازی میدان داد باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دوست، چند سوزی کاخر چرا خوری غم؟</p></div>
<div class="m2"><p>آن کیست کو نخواهد پیوسته شاد باشد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو خوشی به خونم، من خویش را بسوزم</p></div>
<div class="m2"><p>جایی که آب نبود، روزی که باد باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی که پیش هر کس چندین مگیر نامم</p></div>
<div class="m2"><p>این زار مانده دل را کی ایستاد باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تعلیم نیست حاجت غم را به سینه خستن</p></div>
<div class="m2"><p>در استخوان شکستن گرگ اوستاد باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترسم به نامرادی جان در دهم به عشقت</p></div>
<div class="m2"><p>گر پیش تو بمیرم آن هم مراد باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون شاهد است ساقی، یکسو نهیم توبه</p></div>
<div class="m2"><p>در کوی بت پرستان تقوی فساد باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسم الله آنچه خواهی، فرمای، خسرو اینک</p></div>
<div class="m2"><p>فرمان دوستان را بر جان مفاد باشد</p></div></div>