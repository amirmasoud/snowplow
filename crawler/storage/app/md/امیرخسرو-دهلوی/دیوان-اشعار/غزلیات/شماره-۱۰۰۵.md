---
title: >-
    شمارهٔ ۱۰۰۵
---
# شمارهٔ ۱۰۰۵

<div class="b" id="bn1"><div class="m1"><p>خوش آن شب که چشمم بر آن نای بود</p></div>
<div class="m2"><p>مژه هر زمان اشک پالای بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا، ای جهان، بر سر من بگرد</p></div>
<div class="m2"><p>که این سر شبی زیر آن پای بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنم بر در دوست پامال گشت</p></div>
<div class="m2"><p>چه تدبیر چون خاک آن جای بود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب دوش هم بد نبود از خیال</p></div>
<div class="m2"><p>اگر چه دراز و غم افزای بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمی های دوشینه مستم هنوز</p></div>
<div class="m2"><p>میی کز دو چشم جگرزای بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگویم چه خوش داشت وقت مرا</p></div>
<div class="m2"><p>سرودی که از ناله و وای بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بکش زارم،ای عشق، کان دل نماند</p></div>
<div class="m2"><p>که صبر مرا کارفرمای بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیفتاد چندین دل خلق دی</p></div>
<div class="m2"><p>که شانه تراگیسوافزای بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی کار زان لب دریغم مدار</p></div>
<div class="m2"><p>که تا بود خسرو شکرخای بود</p></div></div>