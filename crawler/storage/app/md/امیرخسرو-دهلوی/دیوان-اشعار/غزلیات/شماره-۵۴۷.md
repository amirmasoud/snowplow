---
title: >-
    شمارهٔ ۵۴۷
---
# شمارهٔ ۵۴۷

<div class="b" id="bn1"><div class="m1"><p>دل عاشق چرا شیدا نباشد</p></div>
<div class="m2"><p>به عشق اندر جهان رسوا نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگویی تا به کی، ای شوخ دلبر</p></div>
<div class="m2"><p>ترا پروای حال ما نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بستان لطافت سرو باشد</p></div>
<div class="m2"><p>ولی چون قد او رعنا نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کدامین دیده در وی نیست حیران؟</p></div>
<div class="m2"><p>مگر چشمی که او بینا نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه دل باشد که غافل باشد از یار</p></div>
<div class="m2"><p>نه سر باشد که پر سودا نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نوعی دل ز خسرو در تو بستم</p></div>
<div class="m2"><p>که با غیر توام پروا نباشد</p></div></div>