---
title: >-
    شمارهٔ ۱۹۵۲
---
# شمارهٔ ۱۹۵۲

<div class="b" id="bn1"><div class="m1"><p>مردانه می کشد به جفایم ستمگری</p></div>
<div class="m2"><p>تا میرم و دگر ندهم دل به دیگری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راحت بود سیاست آن کس که بایدش</p></div>
<div class="m2"><p>از غمزه دور باشی و از ناز خنجری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم که دوش با تو نشستیم، راست است</p></div>
<div class="m2"><p>بر خویش بسته ام به هوس خواب دیگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از غم مگر ز وادی هجر استخوان بود</p></div>
<div class="m2"><p>کز کعبه امید بیاید کبوتری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماییم و خواب و بازوی آن یار زیر سر</p></div>
<div class="m2"><p>وه کی نهد تو در خم بازوی ما سری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی ره کند به کلبه ما چون تو آفتاب</p></div>
<div class="m2"><p>ما ناخدای باز کند ز آسمان دری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یارب حلال خواب خوش، ار چه شبی ز غم</p></div>
<div class="m2"><p>روزی نبود پهلوی ما را ز بستری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسرو به سایه ای ز درخت تو قانع است</p></div>
<div class="m2"><p>آن دولت از کجا که به دست افتدش بری</p></div></div>