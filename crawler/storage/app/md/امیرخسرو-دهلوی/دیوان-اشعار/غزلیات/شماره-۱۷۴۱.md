---
title: >-
    شمارهٔ ۱۷۴۱
---
# شمارهٔ ۱۷۴۱

<div class="b" id="bn1"><div class="m1"><p>ای از گل تو ما را در دیده خار مانده</p></div>
<div class="m2"><p>وز نوک غمزه تو جانم فگار مانده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نقش تو زمانه در پیرهن کشیده</p></div>
<div class="m2"><p>در کارگاه گردون مه نیم کار مانده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بو که چون تو ماهی بینم به طالع خود</p></div>
<div class="m2"><p>هر ب به گریه چشمم اندر شمار مانده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس دل که هست هر دم از ناردان لعلت</p></div>
<div class="m2"><p>در پرده قطره قطره همچون انار مانده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو رفتی و دل من دنبال کرده چشمت</p></div>
<div class="m2"><p>مگذار دوستان را دل پر غبار مانده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی تو درون جانم زارست، چون کنم من</p></div>
<div class="m2"><p>بیرون چو می نیاید، این جان زار مانده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رحمی کز انتظارت دو چشم چار کردم</p></div>
<div class="m2"><p>وز گریه هست صد جو در هر چهار مانده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دستم بگیر، یارا، یاری بکن که هستم</p></div>
<div class="m2"><p>در محنت جدایی دستی ز کار مانده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تن موی گشت، گه گه زان می کنم عزیزش</p></div>
<div class="m2"><p>کز زلف تست ما را این یادگار مانده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمرم که رفت بی تو اندر حساب ناید</p></div>
<div class="m2"><p>وامی ست بهر خسرو بر روزگار مانده</p></div></div>