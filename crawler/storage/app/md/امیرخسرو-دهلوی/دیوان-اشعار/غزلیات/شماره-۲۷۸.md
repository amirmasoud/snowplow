---
title: >-
    شمارهٔ ۲۷۸
---
# شمارهٔ ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>آنکه مزاج دلش باز ندانم که چیست</p></div>
<div class="m2"><p>رفتن او کشتن است، باز ندانم که چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این منم از پشت کوژ چنگ حریفان عشق</p></div>
<div class="m2"><p>زار بنالم، ولی خار ندانم که چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مست شبانه است یار خواب خماری به سر</p></div>
<div class="m2"><p>بوی لبش از می است، گاز ندانم که چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار بهانه طلب با من شوریده بخت</p></div>
<div class="m2"><p>نیست بدانسان که بود باز ندانم که چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسرو مسکین ازو شهره هر کوی شد</p></div>
<div class="m2"><p>وان دل او را هنوز راز ندانم که چیست</p></div></div>