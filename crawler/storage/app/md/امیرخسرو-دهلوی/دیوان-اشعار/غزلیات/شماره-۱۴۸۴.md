---
title: >-
    شمارهٔ ۱۴۸۴
---
# شمارهٔ ۱۴۸۴

<div class="b" id="bn1"><div class="m1"><p>چو نام تو در نامه‌ای دیده‌ام</p></div>
<div class="m2"><p>به نامت که بر دیده مالیده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یاد زمین‌بوس درگاه تو</p></div>
<div class="m2"><p>سراپای آن نامه بوسیده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز نام تو آن نامه نامدار</p></div>
<div class="m2"><p>سر بندگی برنپیچیده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز این یک هنر نیست مکتوب را</p></div>
<div class="m2"><p>وگر نیست، باری من این دیده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که آن‌ها که در روی او خوانده‌ام</p></div>
<div class="m2"><p>جوابی از او باز نشنیده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قلم چون سر یک زبانیش نیست</p></div>
<div class="m2"><p>از آن ناتراشیده ببریده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولی اینکه بنهاد سر بر خطم</p></div>
<div class="m2"><p>از او راستی را پسندیده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زبانم چو یارای نطقش نماند</p></div>
<div class="m2"><p>زبانی ز نی برتراشیده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا، ای دبیر، ار نداری مداد</p></div>
<div class="m2"><p>سیاهی برون آور از دیده‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخن‌های بگزیده بنویس و گوی</p></div>
<div class="m2"><p>که ای مونس و یار بگزیده‌ام!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو زلف تو شوریده شد حال من</p></div>
<div class="m2"><p>ببخشای بر حال شوریده‌ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سیه کرده‌ام نامه از دود دل</p></div>
<div class="m2"><p>سیه‌روتر از خاک کن دیده‌ام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو خسرو در این رقعه از سوز دل</p></div>
<div class="m2"><p>به نی آتش تیز پوشیده‌ام</p></div></div>