---
title: >-
    شمارهٔ ۲۰۱
---
# شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>سرو بستان ملاحت قامت رعنای تست</p></div>
<div class="m2"><p>نور چشم عاشقان خسته خاک پای تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من نه تنها گشته ام شیدای دردت جان من</p></div>
<div class="m2"><p>هر که را جان و دل و دینی بود شیدای تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیر اعظم که لاف از قرب عیسی می زند</p></div>
<div class="m2"><p>ذره ای از پرتو رخسار مه سیمای تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در درون مسجد و دیر و خرابات و کنشت</p></div>
<div class="m2"><p>هر کجا، رفتم، همه شور تو و غوغای تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانم از غیرت ز دست جاهلان سوزید، از انک</p></div>
<div class="m2"><p>سر و را گویند مانند قد رعنای تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به ملک دلبری سلطان شدی ای شاه حسن</p></div>
<div class="m2"><p>هر کجا سلطانی و شاهی بود لالای تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وعده دیدار خود کردی به فردا، زان سبب</p></div>
<div class="m2"><p>جان خسرو منتظر بر وعده فردای تست</p></div></div>