---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>مرا وقتی دلی آزاد بوده ست</p></div>
<div class="m2"><p>درونم بی غم و جان شاد بوده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمک زد شوخی اندر جان و نو کرد</p></div>
<div class="m2"><p>جراحتها که در بنیاد بوده ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه خوش بوده ست عقل مصلحت جوی</p></div>
<div class="m2"><p>که چندی زین بلا آزاد بوده ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگارا، هیچ گاهی یاد داری</p></div>
<div class="m2"><p>کزین بیچارگانت یاد بوده ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب آمد، باد برد از جای خویشم</p></div>
<div class="m2"><p>که بوی زلف تو با باد بوده ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به فریادت بخواندم دی و مردم</p></div>
<div class="m2"><p>که جانم همره فریاد بوده ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جفا کش خسروا، گر دوست پیوست</p></div>
<div class="m2"><p>نصیب عاشقان بیداد بوده ست</p></div></div>