---
title: >-
    شمارهٔ ۹۱۰
---
# شمارهٔ ۹۱۰

<div class="b" id="bn1"><div class="m1"><p>کسی که عشق نورزد سیاه دل باشد</p></div>
<div class="m2"><p>چو سر ز خاک لحد بر زند، خجل باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی که سر ننهد در رهش، چه سر دارد؟</p></div>
<div class="m2"><p>دلی که جان ندهد در غمش، چه دل باشد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوای دوست ز سر کی برون کند عاشق</p></div>
<div class="m2"><p>هزار سال اگر زیر خشت و گل باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هجر سلسله شوق منقطع نشود</p></div>
<div class="m2"><p>مرا که رشته جان با تو متصل باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر به تیغ جدایی مرا نخواهد کشت</p></div>
<div class="m2"><p>بهل که تا بکشد کو ز من بحل باشد</p></div></div>