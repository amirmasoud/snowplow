---
title: >-
    شمارهٔ ۳۳۰
---
# شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>چه داغهاست که بر سینه فگارم نسیت</p></div>
<div class="m2"><p>چه دردهاست که بر جان بی قرارم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم ز کوشش خون گشت و کام دل نرسید</p></div>
<div class="m2"><p>چه سود دارد بخشش، چو بخت یارم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خاک کوی بسازم، چو خاک یار نیم</p></div>
<div class="m2"><p>بر آستانه بمیرم چو پیش بارم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشم به دولت خواری و ملک تنهایی</p></div>
<div class="m2"><p>که التفات کسی را به روزگارم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا مپرس که در دم نهان نخواهد ماند</p></div>
<div class="m2"><p>که اعتماد برین چشم اشکبارم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفس به آخرم آمد، ازان دهی سخنی!</p></div>
<div class="m2"><p>که بهر کوی عدم هیچ یادگارم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملامتش رسد از خونم، این همی کشدم</p></div>
<div class="m2"><p>وگرنه بیم ز شمشیر آبدارم نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بس که در دل خسرو سواریش ننشست</p></div>
<div class="m2"><p>به عمر یک نفسی بر پی غبارم نیست</p></div></div>