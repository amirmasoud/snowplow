---
title: >-
    شمارهٔ ۶۷۲
---
# شمارهٔ ۶۷۲

<div class="b" id="bn1"><div class="m1"><p>هر شبم جان بر لب آید، ناله زار آورد</p></div>
<div class="m2"><p>تا کدامین باد بویی زان جفا کار آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت آن شوخ و دل خون گشته را با خود ببرد</p></div>
<div class="m2"><p>عاقبت روزی همان خونش گرفتار آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوستان، من کی هوس دارم به نالیدن، ولی</p></div>
<div class="m2"><p>درد چون در سینه باشد، ناله زار آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آرزومندان به آب دیده معذورند، از آنک</p></div>
<div class="m2"><p>فرقت روی عزیزان گریه بسیار آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بو که بزیم باد را گویید تا بهر خورش</p></div>
<div class="m2"><p>پاره ای خاک از برای جان افگار آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد گله دارم، ولی آن رو چو آید در نظر</p></div>
<div class="m2"><p>کیست کان ساعت زبانم را به گفتار آورد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غمزه خونریز تو مر زاهد صد ساله را</p></div>
<div class="m2"><p>موی پیشانی گرفته سوی خمار آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب ز می توبه کنم از بیم ناز شاهدان</p></div>
<div class="m2"><p>بامدادم روی ساقی باز در کار آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین دل خودکام کار من به رسوایی کشید</p></div>
<div class="m2"><p>خسروا، فرمان دل بردن همین بار آورد</p></div></div>