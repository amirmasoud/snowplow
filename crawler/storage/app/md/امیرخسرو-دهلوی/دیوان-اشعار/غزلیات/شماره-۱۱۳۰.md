---
title: >-
    شمارهٔ ۱۱۳۰
---
# شمارهٔ ۱۱۳۰

<div class="b" id="bn1"><div class="m1"><p>نازنینان و چاربالش ناز</p></div>
<div class="m2"><p>خاکساران و آستان نیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جور و خواری کشیدن از محبوب</p></div>
<div class="m2"><p>خوش تر است از هزار نعمت و ناز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوش مجنون و حلقه لیلی</p></div>
<div class="m2"><p>سر محمود و آستان ایاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نام و ناموس و دین و دنیا را</p></div>
<div class="m2"><p>چه محل پیش عاشق جانباز؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که عیبم همی کنی در عشق</p></div>
<div class="m2"><p>یک نظر بر جمال او انداز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق در هر دلی فرو ناید</p></div>
<div class="m2"><p>زانکه هر سینه نیست محرم راز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من ازین در کجا توانم رفت؟</p></div>
<div class="m2"><p>مرغ پر بسته کی کند پرواز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نی قراری که لب فرو بندم</p></div>
<div class="m2"><p>نی مجالی که برکشم آواز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر به بوی تو جان برافشانم</p></div>
<div class="m2"><p>هم به بوی تو زنده گردم باز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه گفتار دشمنان مشنو</p></div>
<div class="m2"><p>یک دم آخر به دوستان پرداز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ساعتی این شکسته را دریاب</p></div>
<div class="m2"><p>یک زمان این غریب را بنواز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>امشب از رفته باز نتوان گفت</p></div>
<div class="m2"><p>زانکه شب کوته است و قصه دراز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خسرو ار گریه کرد، معذور است</p></div>
<div class="m2"><p>کش چو شمع است کار سوز و گداز</p></div></div>