---
title: >-
    شمارهٔ ۷۷۳
---
# شمارهٔ ۷۷۳

<div class="b" id="bn1"><div class="m1"><p>خم زلف تو که زنجیر جنون می خوانند</p></div>
<div class="m2"><p>ای خوش آن طایفه کاین سلسله می جنبانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای صبا، نرم تری روب غبار زلفش</p></div>
<div class="m2"><p>که دران مشتی زندانی بی سامانند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب آمد همه را مردنم از هجر و مرا</p></div>
<div class="m2"><p>عجب از خلق که بزیند چو تنها مانند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان عاشق چو برون رفت نخوانندش باز</p></div>
<div class="m2"><p>زانکه در دل دگری هست که جانش خوانند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد خوبان جهان، عاشق بیتاب مگرد</p></div>
<div class="m2"><p>که جوان وتر و نوخاسته و نادانند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد امروز سر توبه شکستن دارد</p></div>
<div class="m2"><p>می فروشان اگر این دلق کهن بستانند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این چه شوخی ست که گویی دل من دزدیدی؟</p></div>
<div class="m2"><p>این ز تو آید و ز آنان که ترا می مانند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنده ام خواه قبولم کن و خواهی رد، ازآنک</p></div>
<div class="m2"><p>عزت و خواری در کوی وفا یکسانند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زندگان این همه خواهند که در تو نگرند</p></div>
<div class="m2"><p>مردگان نیز، به جان تو اگر بتوانند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باد حسنت همه خوبان جهان را بشکست</p></div>
<div class="m2"><p>بعد ازین سرو نخیزد که اگر بنشانند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می برد حسرت پابوس تو خسرو در خاک</p></div>
<div class="m2"><p>چون شود خاک، بگو تا به رهت افشانند</p></div></div>