---
title: >-
    شمارهٔ ۲۷۱
---
# شمارهٔ ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>تیر کدامین بلاست کان به کمان تو نیست</p></div>
<div class="m2"><p>دست کدامین دل است کان به عنان تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وجه همه نیکوان از دل ما راجع است</p></div>
<div class="m2"><p>زانکه به خط‌هایشان هیچ نشان تو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشقم اگر می کشد تو مکش، ای پندگو</p></div>
<div class="m2"><p>جان من است آخر این، وای که جان تو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی دلیم گفت از آن صد دلش افزون زکف</p></div>
<div class="m2"><p>هر چه کشم سوی خویش، گوید، از آن تو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نام وفا برده ای، شرم نداری ز خلق</p></div>
<div class="m2"><p>عرض متاعی مکن کان به دکان تو نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمزه زده استی چنانک می بکنم جان ز ذوق</p></div>
<div class="m2"><p>توشه عشق است این، زخم سنان تو نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز مدار، ار کنم رخنه دل پر ز خاک</p></div>
<div class="m2"><p>دودکش این دل است، غالیه دان تو نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کور شد این دل، فتاد در چه تاریک غم</p></div>
<div class="m2"><p>باد ازین کورتر، گر نگران تو نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیغ زن و وارهان خسرو درمانده را</p></div>
<div class="m2"><p>سود وی است این و زان هیچ زیان تو نیست</p></div></div>