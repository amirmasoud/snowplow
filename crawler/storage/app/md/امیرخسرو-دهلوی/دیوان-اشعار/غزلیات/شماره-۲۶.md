---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>وه که سوز درونم خبری نیست ترا</p></div>
<div class="m2"><p>در غمت مردم و با من نظری نیست ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر کوی تو فریاد که از راه وفا</p></div>
<div class="m2"><p>خاک ره گشتم و بر من گذری نیست ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارم آن سر که سرم در سر و کار تو شود</p></div>
<div class="m2"><p>با من دلشده هر چند سری نیست ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیگران گر چه دم از مهر و وفای تو زنند</p></div>
<div class="m2"><p>به وفای تو که چون من دگری نیست ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسروا، ناله و فریاد به جایی نرسد</p></div>
<div class="m2"><p>یارب، این گریه خونین اثری نیست ترا</p></div></div>