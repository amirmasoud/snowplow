---
title: >-
    شمارهٔ ۸۷۳
---
# شمارهٔ ۸۷۳

<div class="b" id="bn1"><div class="m1"><p>بر من کنون که بی تو جهان تیره فام شد</p></div>
<div class="m2"><p>ای شمع جان، در آی که روزم به شام شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو خوش به ناز خفته که عیشت حلال بادت</p></div>
<div class="m2"><p>مسکین کسی که خواب به چشمش حرام باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر مرغ شاد با گل و هر سرو در چمن</p></div>
<div class="m2"><p>بیچاره بلبلی که گرفتار دام شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناز و کرشممه ای که کنی هر دم، ای صبا</p></div>
<div class="m2"><p>می زیبدت که پیش تو سلطان غلام شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آستانت لاف رسیدن کرا رسد</p></div>
<div class="m2"><p>آن را که زیر پای دو عالم دو گام شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی نه ای تمام به عشق، آری این سخن</p></div>
<div class="m2"><p>دانی، چو بشنوی که فلانی تمام شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدنامی است عشق بتان، دور به زما</p></div>
<div class="m2"><p>آن عاشقی که دور ز ما نیکنام شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دی آن کلاه زهد که صوفی به فرق داشت</p></div>
<div class="m2"><p>بر دست ساقیی چو تو امروز جام شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو که زیست با همه خوبان به تو سنی</p></div>
<div class="m2"><p>اینک به نیم چابک عشق تو رام شد</p></div></div>