---
title: >-
    شمارهٔ ۶۵۹
---
# شمارهٔ ۶۵۹

<div class="b" id="bn1"><div class="m1"><p>سبزه ای سبز است و آب روشن و سرو بلند</p></div>
<div class="m2"><p>باده صافی به جام آبگون باید فگند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جای بلبل هست بر سرو بلند و زین قبیل</p></div>
<div class="m2"><p>هست جای آنکه بلبل می پرد زینسان بلند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرگس اندر عین مستی سوی گل چشمک زن است</p></div>
<div class="m2"><p>ورنه گل بر سبزه هم چندین نکردی ریشخند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل ازان کم عمر شد کاو ییشتر از عمر خویش</p></div>
<div class="m2"><p>دام داد آن را که از وی وقت گل شد بهره مند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقیا، می چاشنی کن بعد ازان درده، ازانک</p></div>
<div class="m2"><p>گر ترش باشد می آن را چاشنی باید ز قند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بند بندم را جدا کرده است دست غم به تیغ</p></div>
<div class="m2"><p>تو به خون گرم می پیوند کن بندم ز بند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاهد مجلس، بپوشان رو که من از بیم چشم</p></div>
<div class="m2"><p>پیش رویت پای می کوبم بر آتش چون سپند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر دل خسرو رسن بازی کند با زلف تو</p></div>
<div class="m2"><p>رشته یک چندی درازش ده ز زلف چون کمند</p></div></div>