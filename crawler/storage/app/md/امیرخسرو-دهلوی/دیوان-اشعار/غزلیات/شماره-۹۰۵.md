---
title: >-
    شمارهٔ ۹۰۵
---
# شمارهٔ ۹۰۵

<div class="b" id="bn1"><div class="m1"><p>چمن ز سبزه خطی بر رخ جمیل کشید</p></div>
<div class="m2"><p>به باغ سرو روان قامت طویل کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به رنگ و بوی بیاراست گلستان خود را</p></div>
<div class="m2"><p>به گوشه های گلستان بنفشه نیل کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بتان آزری از بتکده برون جستند</p></div>
<div class="m2"><p>چو لاله زار به دشت آتش خلیل کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهار در ره آیندگان باغ نگر</p></div>
<div class="m2"><p>که فرش دیده نرگس به چند میل کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرودگویان بلبل به جام لاله شتافت</p></div>
<div class="m2"><p>گهی خفیف گرفت و گهی ثقیل کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهشت شد چمن و خوش کسی که با خوبان</p></div>
<div class="m2"><p>در آن بهشت شرابی چو سلسبیل کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به می سبیل کنم خون خود که خوبان را</p></div>
<div class="m2"><p>به سوی خویش توانم بدین سبیل کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نهاد نرگس بیمار چون به بالین سر</p></div>
<div class="m2"><p>حباب از آب روان شیشه دلیل کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوید خوی ز بناگوش پیل مست سحاب</p></div>
<div class="m2"><p>شب از هلال کژک بر سرای پیل کشید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوال داد میی کز رکاب اهل کرم</p></div>
<div class="m2"><p>دوال بستد و در گردن بخیل کشید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برون خرام کنون، خسروا، اگر خواهی</p></div>
<div class="m2"><p>قدح به روی خود و صورت جمیل کشید</p></div></div>