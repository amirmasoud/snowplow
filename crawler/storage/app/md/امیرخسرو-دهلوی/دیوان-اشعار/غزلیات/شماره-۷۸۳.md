---
title: >-
    شمارهٔ ۷۸۳
---
# شمارهٔ ۷۸۳

<div class="b" id="bn1"><div class="m1"><p>بزم ما را یک دو خواب آلوده اند</p></div>
<div class="m2"><p>مست و خوش، گویی شراب آلوده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سایه پروردند وز خط سیاه</p></div>
<div class="m2"><p>سایه را بر آفتاب آلوده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جامه بر اندام شان گویی ز لطف</p></div>
<div class="m2"><p>برگ گل را از گلاب آلوده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می میان شیشه صافی نگر</p></div>
<div class="m2"><p>آتشی گویی به آب آلوده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می نبیند سوی ما ساقی، ازانک</p></div>
<div class="m2"><p>چشمهایش مست و خواب آلوده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب شو، ای چشمه خون، کز شراب</p></div>
<div class="m2"><p>دست آن مست خراب آلوده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یارب آن سرخی لبش را از می است</p></div>
<div class="m2"><p>یا خودش از خون ناب آلوده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس به اشک آلوده شخصم، گوئیا</p></div>
<div class="m2"><p>سیخی از آب کباب آلوده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست خسرو را سؤالی زان دهن</p></div>
<div class="m2"><p>کز پیش راه جواب آلوده اند</p></div></div>