---
title: >-
    شمارهٔ ۹۲۹
---
# شمارهٔ ۹۲۹

<div class="b" id="bn1"><div class="m1"><p>خطی که بر سمن آن گلعذار بنویسد</p></div>
<div class="m2"><p>بنفشه نسخه آن بر بهار بنویسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسیم باد صبا شرح آن خط ریحان</p></div>
<div class="m2"><p>به مشک بر ورق لاله زار بنویسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسا رساله که در آب چشم ما دریا</p></div>
<div class="m2"><p>به دیده بر گهر آبدار بنویسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به روزگار تواند اسیر درد و فراق</p></div>
<div class="m2"><p>که شمه ای ز غم روزگار بنویسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یاد لعل تو هر لحظه چشم من فصلی</p></div>
<div class="m2"><p>بدین دو لعل جواهر نگار بنویسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سواد خط تو یاقوت اگر دهد دستش</p></div>
<div class="m2"><p>بر آفتاب به خط غبار بنویسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدیث خون دلم این خلیفه چشمم</p></div>
<div class="m2"><p>ازان به گرد لب جویبار بنویسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فلک چو قصه منصور بشنود، خسرو</p></div>
<div class="m2"><p>به خون سوخته بر پای دار بنویسد</p></div></div>