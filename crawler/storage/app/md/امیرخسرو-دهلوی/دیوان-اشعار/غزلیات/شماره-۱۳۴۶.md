---
title: >-
    شمارهٔ ۱۳۴۶
---
# شمارهٔ ۱۳۴۶

<div class="b" id="bn1"><div class="m1"><p>شب من سیه شد از غم، مه من کجات جویم؟</p></div>
<div class="m2"><p>به شب دراز هجران مگر از خدات جویم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه ای آن گلی که آرد سوی مات هیچ بادی</p></div>
<div class="m2"><p>ز پی دل خود است این که من از صبات جویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخنت به سرو گویم، خبرت ز باد پرسم</p></div>
<div class="m2"><p>تو درون دیده و دل، ز کسان چرات جویم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دل و دو دیده و جان همه جا نهفته هستی</p></div>
<div class="m2"><p>چو نبینم آشکارا، به کدام جات جویم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو که بر در تو گم شد سرو تاج پادشاهان</p></div>
<div class="m2"><p>چه خیال فاسد است این که من گدات جویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل من گرفت از دین، بت من کجات یابم؟</p></div>
<div class="m2"><p>شب من سیه شد از غم، مه من کجات جویم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تن زار من شکستی، دل و جان فدات سازم</p></div>
<div class="m2"><p>طلب ار کنی سر من، ز سر رضات جویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو ز آه دردمندان سوی تو رود بلایی</p></div>
<div class="m2"><p>به میان سپر شوم هم ره آن بلات جویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر گم شده بجوید مگر از در تو خسرو</p></div>
<div class="m2"><p>ز کجاست بخت آنم که به زیر پات جویم</p></div></div>