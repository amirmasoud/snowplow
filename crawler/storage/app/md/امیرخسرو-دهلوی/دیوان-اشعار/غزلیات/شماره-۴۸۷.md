---
title: >-
    شمارهٔ ۴۸۷
---
# شمارهٔ ۴۸۷

<div class="b" id="bn1"><div class="m1"><p>عشاق حیات از لب خندان تو یابند</p></div>
<div class="m2"><p>خوبان عمل فتنه ز دیوان تو یابند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببینم مه از جیب سپهر و نکشد دل</p></div>
<div class="m2"><p>کان مه که برد دل ز گریبان تو یابند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاید که به شکرانه دهندت سر دیگر</p></div>
<div class="m2"><p>آنان که سر خویش به چوگان تو یابند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بخت کسانی که به رغم من محروم</p></div>
<div class="m2"><p>بوسیدن پای سگ دربان تو یابند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فردای قیامت که به انصاف رسد خلق</p></div>
<div class="m2"><p>زنگار گرفته همه پیکان تو یابند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر خاک وجودم، ز پس مرگ ببیزند</p></div>
<div class="m2"><p>بس دست تظلم که به دامان تو یابند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر جا که گریزد دل سودازده من</p></div>
<div class="m2"><p>بازش به سر زلف پریشان تو یابند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق از کشدم، منت هجران تو بر من</p></div>
<div class="m2"><p>کاین مرتبه از دولت هجران تو یابند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر سوختگان کم ز یکی خنده که باری</p></div>
<div class="m2"><p>داد جگر خود ز نمکدان تو یابند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در یوزه جان می کند از لعل تو خسرو</p></div>
<div class="m2"><p>کان چاشنی از چشمه حیوان تو یابند</p></div></div>