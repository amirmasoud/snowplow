---
title: >-
    شمارهٔ ۳۱۵
---
# شمارهٔ ۳۱۵

<div class="b" id="bn1"><div class="m1"><p>از عشق اگر دلت چو کبابی به تابه ایست</p></div>
<div class="m2"><p>دل باشد ار ز نرخ کبابت کبابه ایست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دل که در تنی به هوایی مقید است</p></div>
<div class="m2"><p>دل نیست آن که شاهدی اندر نقابه ایست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناخوش تر است بوی تو هر چند کز غرور</p></div>
<div class="m2"><p>بر گلخنت ز مشک و ز عنبر گلابه ایست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای آنکه آب خوش خوری از تشنگی فسق</p></div>
<div class="m2"><p>باقی ز آبخورد تو بانگ شرابه ایست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وه وه که تا بلند کنی ز اطلس فلک</p></div>
<div class="m2"><p>در پای آن بلند قدم پای تابه ایست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رهبر ز شوق گیر که جایی رسی، از آنک</p></div>
<div class="m2"><p>دنیاست غول رهزن و عالم خرابه ایست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در زنده عیب زنده دلان نیست خود به نقص</p></div>
<div class="m2"><p>در آب خضر، اگر چه گلش آفتابه ایست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از شیشه سپهر طلب می که در صفت</p></div>
<div class="m2"><p>بر وی فرشته هم چو مگس بر قرابه ایست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو کجات صورت معنی دهد جمال</p></div>
<div class="m2"><p>ز آیینه دلی که سیه همچو تابه ایست</p></div></div>