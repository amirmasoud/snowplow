---
title: >-
    شمارهٔ ۵۱۹
---
# شمارهٔ ۵۱۹

<div class="b" id="bn1"><div class="m1"><p>جانا، اگرم درد تو دیوانه نسازد</p></div>
<div class="m2"><p>خلقی همه از حال من افسانه نسازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خون من خسته نشانی تو همی زلف</p></div>
<div class="m2"><p>کان موی پریشان ترا شانه نسازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چیزی ست درین دل که چنین می شوم از نی</p></div>
<div class="m2"><p>عاقل به ستم خود را دیوانه نسازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون منی، ای دل، ز جگر هم بده آبی</p></div>
<div class="m2"><p>کاین سوخته را شربت بیگانه نسازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باده به سفال آر که ما درد کشانیم</p></div>
<div class="m2"><p>کس از پی ما ساغر و پیمانه نسازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک ره عشاق نیر زد سرم، آری</p></div>
<div class="m2"><p>دولت به سر هیچ کسان خانه نسازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون عاشق صادق شدی، ایمن منشین، زانک</p></div>
<div class="m2"><p>شمشیر بلا بر سر مردانه نسازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن را که بود سوختگی چشم و چراغش</p></div>
<div class="m2"><p>چون سرمه ز خاکستر پروانه نسازد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سودای بتان از سر خسرو شدنی نیست</p></div>
<div class="m2"><p>کاین مرغ وطن جز که به ویرانه نسازد</p></div></div>