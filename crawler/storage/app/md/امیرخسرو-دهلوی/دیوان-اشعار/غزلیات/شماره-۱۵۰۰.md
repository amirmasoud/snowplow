---
title: >-
    شمارهٔ ۱۵۰۰
---
# شمارهٔ ۱۵۰۰

<div class="b" id="bn1"><div class="m1"><p>باز این دل من رو به که آورد، ندانم؟</p></div>
<div class="m2"><p>وان صبر که بوده ست، کجا کرد، ندانم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب ها منم و گوشه غم حال من این است</p></div>
<div class="m2"><p>حال دل آواره شبگرد ندانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن گرد که می خیزد ازان راه ببینید</p></div>
<div class="m2"><p>و آن کیست سوار از پی آن گرد، ندانم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشک از سفر کوی ویم تحفه غم آورد</p></div>
<div class="m2"><p>من خوش تر ازین هیچ ره آورد ندانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بازم به جگر می خلد آن قامت چون تیر</p></div>
<div class="m2"><p>ساقی، قدح باده که من درد ندانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یاری که برنجد ز جفا، یار نگویم</p></div>
<div class="m2"><p>مردی که بترسد ز بلا، مرد ندانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از هر که بپرسند بگوید که چه خسرو</p></div>
<div class="m2"><p>یک سوخته حادثه پرورد ندانم</p></div></div>