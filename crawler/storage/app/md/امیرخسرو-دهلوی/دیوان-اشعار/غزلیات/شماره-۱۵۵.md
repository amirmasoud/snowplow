---
title: >-
    شمارهٔ ۱۵۵
---
# شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>من و شب زندگانی من اینست</p></div>
<div class="m2"><p>دل و غم شادمانی من اینست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه شب خون دل نوشم به یادش</p></div>
<div class="m2"><p>شراب ارغوانی من اینست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی نالم به شب بیداری هجر</p></div>
<div class="m2"><p>سرود میهمانی من اینست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من و کنج غم و شبهای تاریک</p></div>
<div class="m2"><p>طرب جای نهانی من اینست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببندد چشم من بر من خیالش</p></div>
<div class="m2"><p>که شبها یار جانی من اینست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نباید کاید از تنگی من تنگ</p></div>
<div class="m2"><p>برین دل بدگمانی من اینست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز عشقش گاه میرم، گه زیم باز</p></div>
<div class="m2"><p>طریق زندگانی من اینست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رها کن تا بمیرم زیر پایت</p></div>
<div class="m2"><p>که عمر جاودانی من اینست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس ست این قیمت خسرو که گویی</p></div>
<div class="m2"><p>غلام رایگانی من اینست</p></div></div>