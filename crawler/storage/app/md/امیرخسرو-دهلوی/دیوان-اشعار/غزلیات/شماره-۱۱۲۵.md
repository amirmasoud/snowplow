---
title: >-
    شمارهٔ ۱۱۲۵
---
# شمارهٔ ۱۱۲۵

<div class="b" id="bn1"><div class="m1"><p>تن پیر گشت و آرزوی دل جوان هنوز</p></div>
<div class="m2"><p>دل خون شد و حدیث بتان بر زبان هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمرم به آخر آمد و روزم به شب رسید</p></div>
<div class="m2"><p>مستی و بت پرستی من همچنان هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آهنگ کرده سوی بتان جان کمترین</p></div>
<div class="m2"><p>کافر دلان حسن درون سوی جان هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد غم رسید و مرگ هنوزم نمی رسد</p></div>
<div class="m2"><p>صد کعبه رفت و مهر دلم رایگان هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالم تمام پر ز شهیدان خفته گشت</p></div>
<div class="m2"><p>ترک مرا خدنگ بلا در کمان هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیدار مانده شب همه خلق از نفیر من</p></div>
<div class="m2"><p>وان چشم نیم مست به خواب گران هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر دم کرشمه های وی افزون و آنگهی</p></div>
<div class="m2"><p>خسرو ز بند او به امید امان هنوز</p></div></div>