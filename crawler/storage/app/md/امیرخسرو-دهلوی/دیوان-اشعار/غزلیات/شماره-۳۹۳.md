---
title: >-
    شمارهٔ ۳۹۳
---
# شمارهٔ ۳۹۳

<div class="b" id="bn1"><div class="m1"><p>یا رب، آن زلف تو هیچ اشکنه بی دل هست؟</p></div>
<div class="m2"><p>دیر باز است که اندر دلم این مشکل هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیف باشد که بگویم که مه و خورشیدی</p></div>
<div class="m2"><p>هم تو بنگر که بدان هر دو کسی مایل هست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منزلت گفتم مانا که همین در دل ماست</p></div>
<div class="m2"><p>چو ببینیم که به هر جات همین منزل هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به خاک در خویشم نگری افتاده</p></div>
<div class="m2"><p>خود بگویی که چنین آدمیی از گل هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روسیاهم، حبشی گوی من سوخته را</p></div>
<div class="m2"><p>وگرم داغ درون نیست، برون دل هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشمم از هجر تو دریا شد و در خیل خیال</p></div>
<div class="m2"><p>ای بسا مردم آبی که درین ساحل هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چند شمشیر چنان بر من بیچاره زنی</p></div>
<div class="m2"><p>باری این مرتبه همچو منی قابل هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دردم آنکس که نداند دهدم پند، آری</p></div>
<div class="m2"><p>در جهان نیز بسی بی خبر و غافل هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پی عشق نصیحت چه کنی خسرو را</p></div>
<div class="m2"><p>باری آن کس که نصیحت شنود عاقل هست</p></div></div>