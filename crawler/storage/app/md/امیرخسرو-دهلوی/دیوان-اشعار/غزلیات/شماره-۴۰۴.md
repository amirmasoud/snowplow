---
title: >-
    شمارهٔ ۴۰۴
---
# شمارهٔ ۴۰۴

<div class="b" id="bn1"><div class="m1"><p>رخش بدیدم و گفتم که بوستان این است</p></div>
<div class="m2"><p>لبش به خنده در آمد که قوت جان این است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن کشیدم ازان لب که در دهان تو چیست</p></div>
<div class="m2"><p>شکر بریختن آمد که در دهان این است؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهان او به گمان افگند، یقین کردم</p></div>
<div class="m2"><p>که کس یقین نکند آن دهان، گمان این است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذر ز دیده گشادم میان باریکش</p></div>
<div class="m2"><p>به پیچ پیچ در آمد که ریسمان این است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمر گرفتم و گفتم که در میان چیزی ست</p></div>
<div class="m2"><p>به پیچ زد سخنم را که در میان این است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفتمش که به خورشید بر توان رفت</p></div>
<div class="m2"><p>نمود زلف مسلسل که ریسمان این است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به عجز چهره نمودم که رنگ رویم بین</p></div>
<div class="m2"><p>به ناز خنده به من زد که زعفران این است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خطش بدیده ای، ای سبزه، بعد ازین گل را</p></div>
<div class="m2"><p>به ریش خند بخندان که بوستان این است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جمال او به فلک عرضه کردم و خورشید</p></div>
<div class="m2"><p>نمود چهره که پرکاله ای ازان این است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زبان کشید که شمع بتان شدم، گفتم</p></div>
<div class="m2"><p>هزارخانه به خود خواند کاسمان این است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روان چو باد بدادی به بنده خسرو اسپ</p></div>
<div class="m2"><p>چو باد اسپ دهی بخشش روان این است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به نام نیک ترا عمر جاودان بادا</p></div>
<div class="m2"><p>تو نام نیک طلب عمر جاودان این است</p></div></div>