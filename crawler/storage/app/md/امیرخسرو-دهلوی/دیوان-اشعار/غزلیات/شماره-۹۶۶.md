---
title: >-
    شمارهٔ ۹۶۶
---
# شمارهٔ ۹۶۶

<div class="b" id="bn1"><div class="m1"><p>دل که نز عشق پاره پاره بود</p></div>
<div class="m2"><p>دل نگویم که سنگ خاره بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیرمردی که از جفای جوان</p></div>
<div class="m2"><p>خون نخورده ست شیرخواره بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای که مه با کمال خوبی خویش</p></div>
<div class="m2"><p>پیش روی تو پیشکاره بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که یکبار دید روی ترا</p></div>
<div class="m2"><p>تا زند در غم دوباره بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ز کافر بود هزار سوار</p></div>
<div class="m2"><p>چشم تو میر آن هزاره بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون لبت را به گاز پاره کنم</p></div>
<div class="m2"><p>لب نباشد نبات پاره بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست یک چاره وصل را، وانگاه</p></div>
<div class="m2"><p>می زیم من هزار چاره بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاک پای تو می کشم در چشم</p></div>
<div class="m2"><p>مگر این اشک را کناره بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر شبی خسرو است و بیداری</p></div>
<div class="m2"><p>مونسش گر بود، ستاره بود</p></div></div>