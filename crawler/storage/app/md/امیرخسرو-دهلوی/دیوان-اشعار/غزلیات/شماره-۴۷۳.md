---
title: >-
    شمارهٔ ۴۷۳
---
# شمارهٔ ۴۷۳

<div class="b" id="bn1"><div class="m1"><p>شمع من اگر یک شب از خانه برون آید</p></div>
<div class="m2"><p>از هر طرفی صد جان پروانه برون آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد جامه قبا گردد از هر طرفی، چون او</p></div>
<div class="m2"><p>کژ کرده کلاه از سر مستانه برون آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بی خبر و طفلان سنگی به کف از هر سو</p></div>
<div class="m2"><p>شسته به کمین تا کی دیوانه برون آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریاد که از یاری عمری به جفا باشم</p></div>
<div class="m2"><p>چون گاه وفا باشد بیگانه برون آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر روز بری جویم از بخت، محال است این</p></div>
<div class="m2"><p>خوشه ز پی شش ماه از دانه برون آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر وجه قرار من هست از رخ تو مردن</p></div>
<div class="m2"><p>وه کز خط تو ناگه پروانه برون آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در کشتن خود یارم، من از تو چه غم دارم؟</p></div>
<div class="m2"><p>گر جان ز پی خسرو خصمانه برون آید</p></div></div>