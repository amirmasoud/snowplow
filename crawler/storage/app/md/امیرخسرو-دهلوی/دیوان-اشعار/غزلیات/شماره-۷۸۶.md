---
title: >-
    شمارهٔ ۷۸۶
---
# شمارهٔ ۷۸۶

<div class="b" id="bn1"><div class="m1"><p>آنچه بتوان، در غمت جان می کشد</p></div>
<div class="m2"><p>تا بدان غایت که بتوان، می کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کشد خط بر مسلمانی لبت</p></div>
<div class="m2"><p>وانگه از خون مسلمان می کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده تا خط ترا بالای لب</p></div>
<div class="m2"><p>باد خط بر آب حیوان می کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن روز افزونت از اوج کمال</p></div>
<div class="m2"><p>روی مه را داغ نقصان می کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف کاید بر لبت، گویی که دیو</p></div>
<div class="m2"><p>خاتم از دست سلیمان می کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنچه دل یک چند از زلفت کشید</p></div>
<div class="m2"><p>از لب لعلت دو چندان می کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر ز شوخی تیر بر دل می زنی</p></div>
<div class="m2"><p>خسرو بیچاره از جان می کشد</p></div></div>