---
title: >-
    شمارهٔ ۱۷۵۶
---
# شمارهٔ ۱۷۵۶

<div class="b" id="bn1"><div class="m1"><p>چو خاست صبحدم آن مه ز خواب پژمرده</p></div>
<div class="m2"><p>گل رخش ز خمار شراب پژمرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شدند خوبان پژمرده زان جمال چنانک</p></div>
<div class="m2"><p>شود شکوفه تر ز آفتاب پژمرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آفتاب مرو ماه من که نآرد تاب</p></div>
<div class="m2"><p>رخت که می شود از ماهتاب پژمرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببردی آب، همه گلرخان دو تا گشتند</p></div>
<div class="m2"><p>چو آن گلی که کشندش گلاب پژمرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدید نرگس بستان به خواب چشم ترا</p></div>
<div class="m2"><p>شد از تحیر آن هم به خواب پژمرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا بگیر چو گل لعل بر رخ از دم سرد</p></div>
<div class="m2"><p>که تو به توست همه خون ناب پژمرده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصال خواست ز تو خسرو و جوانی یافت</p></div>
<div class="m2"><p>که گشت غنچه دل زان جواب پژمرده</p></div></div>