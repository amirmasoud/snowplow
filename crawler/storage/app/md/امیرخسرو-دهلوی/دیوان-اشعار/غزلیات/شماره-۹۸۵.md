---
title: >-
    شمارهٔ ۹۸۵
---
# شمارهٔ ۹۸۵

<div class="b" id="bn1"><div class="m1"><p>با تو در سینه جان نمی‌گنجد</p></div>
<div class="m2"><p>تو درونی از آن نمی‌گنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناتوانم ز عشق و هیچ علاج</p></div>
<div class="m2"><p>در دل ناتوان نمی‌گنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنگ دارد دل مرا که در او</p></div>
<div class="m2"><p>جز تو کس، ای جوان، نمی‌گنجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچنانی نشسته اندر دل</p></div>
<div class="m2"><p>که نفس هم در آن نمی‌گنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌نگنجی تو در میانهٔ جان</p></div>
<div class="m2"><p>لیک جان در میان نمی‌گنجد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم تو آشکار خواهم کرد</p></div>
<div class="m2"><p>چه کنم، در نهان نمی‌گنجد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق در سر فتاد و عقل برفت</p></div>
<div class="m2"><p>کاین دو در یک مکان نمی‌گنجد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا که خسرو زبان گشاد از تو</p></div>
<div class="m2"><p>سخنش در جهان نمی‌گنجد</p></div></div>