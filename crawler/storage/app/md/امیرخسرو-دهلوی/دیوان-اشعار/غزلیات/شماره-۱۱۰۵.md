---
title: >-
    شمارهٔ ۱۱۰۵
---
# شمارهٔ ۱۱۰۵

<div class="b" id="bn1"><div class="m1"><p>از چشم تو که هست ز تو جان شکارتر</p></div>
<div class="m2"><p>دل نیست در جهان ز دل من فگارتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می گوی تلخ از آن لب شیرین که زهر تست</p></div>
<div class="m2"><p>ز آب حیات بر دل و جان سازگارتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلق از تو با کمال وفا با شکایتند</p></div>
<div class="m2"><p>من هر چه بیش می کشیم شرمسارتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش تو جان شکافم و باور نیایدت</p></div>
<div class="m2"><p>هرگز ندیده ام ز تو بی استوارتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که هوشیار شو، ای دل، به کار عشق</p></div>
<div class="m2"><p>عقلم به گوش گفت ز من هوشیارتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عشق بدگوار بود پند دشمنان</p></div>
<div class="m2"><p>حقا که پند دوست از آن ناگوارتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرسی که چون نخست دلت بیقرار نیست</p></div>
<div class="m2"><p>گر باورم کنی قدری بیقرارتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخ هر چه بیش بر در تو می زنم به سنگ</p></div>
<div class="m2"><p>بختم نگر که هست زرم بی عیارتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم خود برون برآر، چو خسرو بگویدت</p></div>
<div class="m2"><p>کاخر ز چیست چشم من سوکوارتر؟</p></div></div>