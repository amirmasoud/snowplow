---
title: >-
    شمارهٔ ۲۰۹
---
# شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>بی رخت از پا فتادم، بی لبت رفتم ز دست</p></div>
<div class="m2"><p>قدر گل بلبل شناسد، قدر باده می پرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد، از بدنامیم دیگر مترسان، زانکه من</p></div>
<div class="m2"><p>گر برآرم نام نیکو، پیش بدنامان بد است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آشنایی در وجود جوهر فردم نماند</p></div>
<div class="m2"><p>مشکل ما هست اکنون زان دهان نیست هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوی چشمانش مبینید، ای رقیبان، زینهار</p></div>
<div class="m2"><p>غارت دین می کنند آن کافران نیم مست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حلقه های زلف ترکان بوالعجب دام بلاست</p></div>
<div class="m2"><p>هر که افتاد اندر آن دام از گرفتاری برست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در میان ما و تو حایل نباشد بحر و کوه</p></div>
<div class="m2"><p>رهروان را کی بود اندیشه از بالا و پست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از وجود خاکی من گر چه گردی خاسته ست</p></div>
<div class="m2"><p>عاقبت خواهد به آب دیده در کویت نشست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر به قدت سرفرازی می کند طوبی به خلد</p></div>
<div class="m2"><p>روز حشر از رشک خواهم شاخ های او شکست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو خسرو کی رهد از بند خویش و هر دو کون</p></div>
<div class="m2"><p>هر که دل در حلقه زنجیر گیسویی نبست</p></div></div>