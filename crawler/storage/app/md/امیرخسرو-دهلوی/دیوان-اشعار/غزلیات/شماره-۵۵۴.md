---
title: >-
    شمارهٔ ۵۵۴
---
# شمارهٔ ۵۵۴

<div class="b" id="bn1"><div class="m1"><p>دلم بی وصل جانان جان نخواهد</p></div>
<div class="m2"><p>که عاشق جان بی جانان نخواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل دیوانگان عاقل نگردد</p></div>
<div class="m2"><p>سر شوریدگان سامان نخواهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طبیب عاشقان درمان نسازد</p></div>
<div class="m2"><p>مریض عاشقی درمان نخواهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر صد روضه بر آدم کنی عرض</p></div>
<div class="m2"><p>برون از گلشن رضوان نخواهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ورش صد بنیامین را هست یعقوب</p></div>
<div class="m2"><p>بغیر از یوسف کنعان نخواهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر گویم، خلاف عقل باشد</p></div>
<div class="m2"><p>که مفلس مملکت خوبان نخواهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کجا خسرو لب شیرین نجوید</p></div>
<div class="m2"><p>چرا بلبل گل خندان نخواهد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم جز روی و موی گلعذاران</p></div>
<div class="m2"><p>تماشای گل و ریحان نخواهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز رویش می گریزد زلف مشکین</p></div>
<div class="m2"><p>که پند و صحبت خاقان نخواهد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از آن خسرو ز دهلی رفت بیرون</p></div>
<div class="m2"><p>که ملک هندویی سلطان نخواهد</p></div></div>