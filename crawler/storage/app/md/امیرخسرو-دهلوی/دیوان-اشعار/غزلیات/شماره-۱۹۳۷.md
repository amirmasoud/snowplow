---
title: >-
    شمارهٔ ۱۹۳۷
---
# شمارهٔ ۱۹۳۷

<div class="b" id="bn1"><div class="m1"><p>گر به کمند زلف تو من نه چنین اسیرمی</p></div>
<div class="m2"><p>کی به کمند ابرویت خسته زخم تیرمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست یقین چو مردنم، از غم دوریش مکش</p></div>
<div class="m2"><p>باری اگر بمیرمی، در قدم تو میرمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بودم اسیر کافران وقتی و در فراق تو</p></div>
<div class="m2"><p>در هوسم که این زمان کاش همان اسیرمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پند دهند کز بتان چشم ببند جان من</p></div>
<div class="m2"><p>باز کشید تا مگر بند کسی پذیرمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترک سخن بگو که شدملک جهان از آن من</p></div>
<div class="m2"><p>آه که تنگ در برت یک شب اگر بگیرمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طعنه زنی که خسروا، ملک جهان ستانمی</p></div>
<div class="m2"><p>گر به ولایت سخن مثل تو بی نظیرمی</p></div></div>