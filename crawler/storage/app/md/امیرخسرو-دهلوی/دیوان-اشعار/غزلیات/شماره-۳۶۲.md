---
title: >-
    شمارهٔ ۳۶۲
---
# شمارهٔ ۳۶۲

<div class="b" id="bn1"><div class="m1"><p>سرو را با قد تو هستی نیست</p></div>
<div class="m2"><p>میلش الا به سوی پستی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دهان و میانت می بینم</p></div>
<div class="m2"><p>نیستی هست، لیک هستی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه گاهم به قبله بودی روی</p></div>
<div class="m2"><p>تا تو در پیش من نشستی، نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهد با عشق در نیامیزد</p></div>
<div class="m2"><p>بت پرستی خداپرستی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برگ صبری که پیش از اینم بود</p></div>
<div class="m2"><p>سرو من تا تو برشکستی، نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ترا دست جور بر سر ماست</p></div>
<div class="m2"><p>کار ما جز که زیردستی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستی گفتی ز عشق خسرو را</p></div>
<div class="m2"><p>عشق دیوانگی ست، مستی نیست</p></div></div>