---
title: >-
    شمارهٔ ۱۸۵۰
---
# شمارهٔ ۱۸۵۰

<div class="b" id="bn1"><div class="m1"><p>بختم از خواب در آمد چو تو با من خفتی</p></div>
<div class="m2"><p>نه در آغوش که در دیده روشن خفتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دمی گردی و در دیده ناخفته دوست</p></div>
<div class="m2"><p>دوستانه ز پی کوری دشمن خفتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاد داری که شبی هر دو به بستان بودیم</p></div>
<div class="m2"><p>من به خار و خس و تو در گل و گلشن خفتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این چه عید است که خسرو ز تو قدری دریافت</p></div>
<div class="m2"><p>که تو با او همه شب دست به گردن خفتی</p></div></div>