---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>بس که اندر دل فرو بردم هوای نیش را</p></div>
<div class="m2"><p>شعله افزون تر برآمد سوز داغ خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دشمنی دارم که جان قربانی او می کنم</p></div>
<div class="m2"><p>زانکه تیری در خور است این کافر بدکیش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چاشنی درد دل آنکس که نشناسد حقش</p></div>
<div class="m2"><p>بردل مجروح خود مرهم شناسد نیش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشک طوفان ریز، بهر جستن وصلم چه سود؟</p></div>
<div class="m2"><p>شست نتوان چون ز بخت مدبران درویش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر به یک غمزه نمردم من، مکن خسته دلم</p></div>
<div class="m2"><p>ناوکی گر رفت کج، نتوان شکستن کیش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پندگو کایدبرین دل سوخته گویی خس است</p></div>
<div class="m2"><p>کو به اصلاح چراغ آید بسوزد خویش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز چون از دست مقبل در هوا گیرد شکار</p></div>
<div class="m2"><p>مرغ بریان ز آستین بیرون برد درویش را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسروا، دیده فرو بند و مبین روی رقیب</p></div>
<div class="m2"><p>زانکه مرهم خوش نباشد دیده های ریش را</p></div></div>