---
title: >-
    شمارهٔ ۲۸۰
---
# شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>چون غم هجران او نداشت نهایت</p></div>
<div class="m2"><p>عاقبت اندوه عشق کرد سرایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقت نیامد بتا، که از سر انصاف</p></div>
<div class="m2"><p>سوی ضعیفان نظر کنی به عنایت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غایت آنها که از جفای تو دیدم</p></div>
<div class="m2"><p>نور یقین داشت در دلم به سرایت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تنم از دست غم ز پای در آمد</p></div>
<div class="m2"><p>سرنکشم، تا منم، ز قید و فایت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر تو به تیغم زنی خلاص نباشد</p></div>
<div class="m2"><p>زخم تو خوشتر که از رقیب حمایت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرح غم عشق بیش ازین ز چه گویم</p></div>
<div class="m2"><p>شوق من وجور او رسید به غایت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بت نامهربان شوخ ستمگر</p></div>
<div class="m2"><p>از تو کنم یا ز روزگار شکایت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنچه من از روزگار سفله کشیدم</p></div>
<div class="m2"><p>پیش تو گویم ز روزگار حکایت</p></div></div>