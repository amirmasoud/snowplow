---
title: >-
    شمارهٔ ۱۰۸۰
---
# شمارهٔ ۱۰۸۰

<div class="b" id="bn1"><div class="m1"><p>صبح است و دهر از خرمی چون روضه رضوان نگر</p></div>
<div class="m2"><p>جنبیدن باد صبا جلوه گر بستان نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خندید خورشید فلک چون سرخ گل در بوستان</p></div>
<div class="m2"><p>از خنده آن سرخ گل آفاق را خندان نگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چشمه خورشید اگر آبی ندیده ستی گهی</p></div>
<div class="m2"><p>خیزند چون ز خواب خوش، رو شستن خوبان نگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رکن سریر مملکت کز دولت قطب جهان</p></div>
<div class="m2"><p>ارکان ملک و دین قوی از روی چار ارکان نگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>والا حسن دستور شه کز بهر وجه عالمی</p></div>
<div class="m2"><p>از کف دستش هر خطی دیباچه احسان نگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنموده پیش مهر و مه از لوح محفوظ آیتی</p></div>
<div class="m2"><p>کاینک ز بهر عمر خود منشور جاویدان نگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر صبح مشرق، خسروا، از آسمان طالع شود</p></div>
<div class="m2"><p>صبح سعادت را طلوع از فر خسروخان نگر</p></div></div>