---
title: >-
    شمارهٔ ۱۶۰۲
---
# شمارهٔ ۱۶۰۲

<div class="b" id="bn1"><div class="m1"><p>روی ترش کرده به یاران مبین</p></div>
<div class="m2"><p>سرکه فروشی مکن، ای انگبین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چاه مزن زیر لب چون سمن</p></div>
<div class="m2"><p>رخنه مکن در شکم یاسمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی زمین را تویی آب حیات</p></div>
<div class="m2"><p>تشنه ز تو هر که به روی زمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف که شد طوق گلوی تو، کرد</p></div>
<div class="m2"><p>سلسله در گردن ماء معین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی گنهی چشم ز ما برمگیر</p></div>
<div class="m2"><p>بی سببی چهره ز ما در مچین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیک از آن چشم کمین می کنی</p></div>
<div class="m2"><p>دیده بد نیز ببین در کمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پای برین دیده پر خون منه</p></div>
<div class="m2"><p>بیهده در خون و دلم در مشین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای که ز روی تو جهان روشن است</p></div>
<div class="m2"><p>آه من سوخته را هم ببین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو آخر چو سگ از خود مران</p></div>
<div class="m2"><p>چند چو رو به کنیم پوستین</p></div></div>