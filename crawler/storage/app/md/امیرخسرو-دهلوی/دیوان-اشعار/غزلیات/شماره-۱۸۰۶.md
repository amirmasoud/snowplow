---
title: >-
    شمارهٔ ۱۸۰۶
---
# شمارهٔ ۱۸۰۶

<div class="b" id="bn1"><div class="m1"><p>ز رحمت چشم بر چاکر نداری</p></div>
<div class="m2"><p>نداری رحمت، ای کافر، نداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم بردی و خوشتر آنکه گر من</p></div>
<div class="m2"><p>بگویم بیدلم، باور نداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگو در من مبین، در دیگران بین</p></div>
<div class="m2"><p>که مثل خویش در کشور نداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پشت پای خود بنگر که وقت است</p></div>
<div class="m2"><p>از این آیینه بهتر نداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کله را کج منه چندین بر آن سر</p></div>
<div class="m2"><p>که تا با ما کجی در سر نداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخور خون دل و دیده مکن، ای آب</p></div>
<div class="m2"><p>نه خون من که خواب و خور نداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چودل برداشتن اندیشه ات بود</p></div>
<div class="m2"><p>چرا سنگی به کشتن برنداری؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حدیث خسرو اندر گوش می کن</p></div>
<div class="m2"><p>ز بهر گوش اگر گوهر نداری</p></div></div>