---
title: >-
    شمارهٔ ۸۵۸
---
# شمارهٔ ۸۵۸

<div class="b" id="bn1"><div class="m1"><p>باز آن سوار مست به نخچیر می رود</p></div>
<div class="m2"><p>دستم ز کار و کار ز تدبیر می رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کاشکی که بر دل خونین من رسد</p></div>
<div class="m2"><p>آن تیر او که بر دل نخچیر می رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او اسپ می دواند و ما کشته می شویم</p></div>
<div class="m2"><p>لشکر هلاک می شود و میر می رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقاش چین به قبله محراب ابرویش</p></div>
<div class="m2"><p>از بهر توبه کردن تصویر می رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من بیهشم، که می دهد از سرو من نشان؟</p></div>
<div class="m2"><p>این باد مشکبو که به شبگیر می رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر ساعتی که می گذرد قامتش به دل</p></div>
<div class="m2"><p>گویا که در درونه من تیر می رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیوانه شد دلم، ره زلف تو بر گرفت</p></div>
<div class="m2"><p>مسکین به پای خویش به زنجیر می رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشقت نه سرسری ست که با عشق آدمی</p></div>
<div class="m2"><p>با جان برآید آنگه و با شیر می رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما و شراب و شاهد و مستی و عاشقی</p></div>
<div class="m2"><p>کایین صوفیان همه تزویر می رود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نزدیک شد هلاکت خسرو ز دوریت</p></div>
<div class="m2"><p>در کار او هنوز، چه تقصیر می رود؟</p></div></div>