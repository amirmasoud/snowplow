---
title: >-
    شمارهٔ ۱۷۱۹
---
# شمارهٔ ۱۷۱۹

<div class="b" id="bn1"><div class="m1"><p>ای آرزوی هزار سینه</p></div>
<div class="m2"><p>وندر دل تو هزار کینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستم ز برت که هست پیدا</p></div>
<div class="m2"><p>در جامه چو می در آبگینه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر قطره خون ز چشم من هست</p></div>
<div class="m2"><p>بر خاتم عاشقی نگینه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای عقل که پندنامه خوانی</p></div>
<div class="m2"><p>در آب روان کن این سفینه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طاقت به دلم نماند، یارب</p></div>
<div class="m2"><p>انزل لقلوبنا سکینه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجنون خراب سینه داند</p></div>
<div class="m2"><p>اندوه من خراب سینه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ننگ همه عاشقانست خسرو</p></div>
<div class="m2"><p>مپسند سفال در خزینه</p></div></div>