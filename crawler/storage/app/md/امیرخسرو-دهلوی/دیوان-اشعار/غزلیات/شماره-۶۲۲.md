---
title: >-
    شمارهٔ ۶۲۲
---
# شمارهٔ ۶۲۲

<div class="b" id="bn1"><div class="m1"><p>چون ز نسیم صبحدم زلف تو در هوا شود</p></div>
<div class="m2"><p>سنگ بود نه آدمی، هر که نه مبتلا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سحری که ترک من سر ز خمار بر کند</p></div>
<div class="m2"><p>بس که نماز مردمان هر طرفی قضا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن تو هم به کودکی آفت شهر گشت اگر</p></div>
<div class="m2"><p>زین چه که هست ذره ای برگذرد، بلا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این همه نسخه کاینه می ببرد ز روی تو</p></div>
<div class="m2"><p>گرنه به مهر و مه رسد پس تو بگو، کجا شود؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد خزان که بشکند شاخ جوانی چمن</p></div>
<div class="m2"><p>بر سر زلف، ار شبی برگذرد، صبا شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبزه خط نهان مکن تا بکنم نظاره ای</p></div>
<div class="m2"><p>پیش که در میان گل سبزه تو گیا شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر کویت از طرب، گو چه غلط شود مرا؟</p></div>
<div class="m2"><p>وعده وصل تو شبی، گر به غلط وفا شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طعنه زنند هر کسی شادی بزی و غم مخور</p></div>
<div class="m2"><p>خسرو خسته می زید، گر ز غمش رها شود</p></div></div>