---
title: >-
    شمارهٔ ۵۲۴
---
# شمارهٔ ۵۲۴

<div class="b" id="bn1"><div class="m1"><p>چون مرغ سحر از غم گلزار بنالد</p></div>
<div class="m2"><p>از غم دل دیوانه من زار بنالد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر گه که به گوشش برسد ناله زارم</p></div>
<div class="m2"><p>بر درد من سوخته دل زار بنالد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سوزش من جان زن و مرد بسوزد</p></div>
<div class="m2"><p>وز ناله زارم در و دیوار بنالد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای آنکه ز دردت خبری نیست، مکن عیب</p></div>
<div class="m2"><p>گر سوخته ای از دل افگار بنالد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسرو، اگر از درد بنالد، چه توان گفت؟</p></div>
<div class="m2"><p>عیبی نتوان کرد که بیمار بنالد</p></div></div>