---
title: >-
    شمارهٔ ۱۷۹۰
---
# شمارهٔ ۱۷۹۰

<div class="b" id="bn1"><div class="m1"><p>گرچه به هر سخن دلم از تن ربوده‌ای</p></div>
<div class="m2"><p>با این همه بگوی، که جانم فزوده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمت به غمزه بردن دل‌ها نمونه‌ای‌ست</p></div>
<div class="m2"><p>تا تو بدین بهانه چه دل‌ها ربوده‌ای!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رویت درون پرده و صد پرده چاک از او</p></div>
<div class="m2"><p>شادی به روزگار کسی کِش نموده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بالین گردناک مرا طعنه می‌زنی</p></div>
<div class="m2"><p>جانا، به تکیه‌گاه غریبان نبوده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسان مگیر آه و دم سرد من، از آنک</p></div>
<div class="m2"><p>خردی و گرم و سرد جهان نآزموده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی که خون به دست خودت ریز، ای رقیب</p></div>
<div class="m2"><p>شکرانه بر من است که از وی شنوده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی داند انده شب تنهانشستگان؟</p></div>
<div class="m2"><p>ای آن که مست در بر جانان غنوده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای مرغ آب، عربده دریات سهل بود</p></div>
<div class="m2"><p>پروانه‌وار سینه بر آتش نسوده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بد گفت عاشقانت چنین کرد، خسروا</p></div>
<div class="m2"><p>رنجه مشو که کِشته خود را دروده‌ای</p></div></div>