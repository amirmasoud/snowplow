---
title: >-
    شمارهٔ ۵۷۰
---
# شمارهٔ ۵۷۰

<div class="b" id="bn1"><div class="m1"><p>مه او چون به ماهی برنیاید</p></div>
<div class="m2"><p>شهی زینسان به گاهی برنیاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو زلف کافر هندونژادت</p></div>
<div class="m2"><p>ز هندستان سپاهی برنیاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به اورنگ ملاحت تا به محشر</p></div>
<div class="m2"><p>چو او گلچهره شاهی برنیاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل افروزی چو او خورشید تابان</p></div>
<div class="m2"><p>ز طرف بارگاهی برنیاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر او را سرو گویم راست ناید</p></div>
<div class="m2"><p>که با قدش گیاهی برنیاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمانی نگذرد کز خاک کویش</p></div>
<div class="m2"><p>نفیر دادخواهی برنیاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گنه کارم چرا کان آتشم نیست؟</p></div>
<div class="m2"><p>کز دود گناهی بر نیاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برو خسرو که آهنگ درایی</p></div>
<div class="m2"><p>درین کشور ز راهی برنیاید</p></div></div>