---
title: >-
    شمارهٔ ۱۲۹۰
---
# شمارهٔ ۱۲۹۰

<div class="b" id="bn1"><div class="m1"><p>ز عشقت بیقرارم، با که گویم؟</p></div>
<div class="m2"><p>ز هجرت خوار و زارم، با که گویم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی پرسی ز احوالم که چونی</p></div>
<div class="m2"><p>پریشان روزگارم، با که گویم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی خواهم که بفرستم سلامی</p></div>
<div class="m2"><p>چو یک محرم ندارم، با که گویم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه یک محرم که راز دل توان گفت</p></div>
<div class="m2"><p>فراوان راز دارم، با که گویم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم بردی، غم کارم نخوردی</p></div>
<div class="m2"><p>خراب است روزگارم، با که گویم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندارد جز تمنای تو خسرو</p></div>
<div class="m2"><p>جمالت دوست دارم، با که گویم؟</p></div></div>