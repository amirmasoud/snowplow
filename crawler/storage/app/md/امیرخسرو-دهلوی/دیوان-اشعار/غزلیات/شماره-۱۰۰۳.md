---
title: >-
    شمارهٔ ۱۰۰۳
---
# شمارهٔ ۱۰۰۳

<div class="b" id="bn1"><div class="m1"><p>بر آن است جانم که ناگه برآید</p></div>
<div class="m2"><p>چو از بهر یک دیدنت می نپاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مزن غمزه چون من ز هجران بمردم</p></div>
<div class="m2"><p>که کس تیغ بر کشتگان نازماید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازان دیده بر خاک پای تو سایم</p></div>
<div class="m2"><p>که زنگار اشکم ز راهت زداید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلت در قبا راست کاری نداند</p></div>
<div class="m2"><p>چو کج باشد آیینه رو کج نماید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر در وفاهای وعده بخیلی</p></div>
<div class="m2"><p>جوانمردی عشق چندین نشاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگو، خسروا، «ترک دلبند خودگیر»</p></div>
<div class="m2"><p>دلم با دگر کس کجا می گشاید؟</p></div></div>