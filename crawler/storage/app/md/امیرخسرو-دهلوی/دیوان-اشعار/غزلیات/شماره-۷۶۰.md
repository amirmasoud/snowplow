---
title: >-
    شمارهٔ ۷۶۰
---
# شمارهٔ ۷۶۰

<div class="b" id="bn1"><div class="m1"><p>به سر من اگر آن طرفه پسر باز آید</p></div>
<div class="m2"><p>عمر من هر چه برفته ست ز سر باز آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زو نبودم به نظر قانع و می کردم ناز</p></div>
<div class="m2"><p>کار من کاش کنون هم به نظر باز آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه من رفت که از حسن به شکلی دگر است</p></div>
<div class="m2"><p>وه که ماهی برود، شکل دگر باز آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوش و دل رفت، به جان آمدنش می خواهم</p></div>
<div class="m2"><p>چه کنم؟ چیزی ازان رفته مگر باز آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برو، ای صورت آن چشم که در چشم منی</p></div>
<div class="m2"><p>که نرفته ست ز کویش ز سفر باز آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده چندان به کف پای سفیدش مالم</p></div>
<div class="m2"><p>که سیاهش کنم از مالش، اگر باز آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طرفه تیریست که بر سینه زند هجرانش</p></div>
<div class="m2"><p>کز جگر بگذرد و هم به جگر باز آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه گربه رسد آبم به کمر، باز رود</p></div>
<div class="m2"><p>باز چون گریه کنم تا به کمر باز آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خبری هم نفرستاد که گر باز رود</p></div>
<div class="m2"><p>خسرو بی خبر، آخر به خبر باز آید</p></div></div>