---
title: >-
    شمارهٔ ۱۴۶۳
---
# شمارهٔ ۱۴۶۳

<div class="b" id="bn1"><div class="m1"><p>ما در این شهر پای بند توایم</p></div>
<div class="m2"><p>عاشق قامت بلند توایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرده آن دهان چون پسته</p></div>
<div class="m2"><p>کشته آن لب چو قند توایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می دوانی و می کشی ما را</p></div>
<div class="m2"><p>چون بدیدی که در کمند توایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای جفا بر دلم پسندیده</p></div>
<div class="m2"><p>دوستی بود، ار سپند توایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گو رفیقان، سفر کنید که ما</p></div>
<div class="m2"><p>نتوانیم، پای بند توایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گذری می کن، ار طبیب منی</p></div>
<div class="m2"><p>آتش می نه، ار سپند توایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز پرسی تو حال خسرو را</p></div>
<div class="m2"><p>تا چه غایت نیازمند توایم</p></div></div>