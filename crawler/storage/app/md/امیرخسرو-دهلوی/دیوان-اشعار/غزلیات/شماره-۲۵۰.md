---
title: >-
    شمارهٔ ۲۵۰
---
# شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>گر چه سرو باغ را بالا خوش است</p></div>
<div class="m2"><p>با قد رعنای تو ما را خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهر عشقت کام عیشم تلخ کرد</p></div>
<div class="m2"><p>هست تلخ این چاشنی، اما خوش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر غمت غیری خورد، ناخوش شوم</p></div>
<div class="m2"><p>خوردن غمها همین این جا خوش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تو نایی، چیست این جور رقیب؟</p></div>
<div class="m2"><p>خار می دانی که با خرما خوش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی تو من باری نیم خوش هیچ وقت</p></div>
<div class="m2"><p>وقت تو خوش که تو را بی ما خوش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شعله در دل یار در جان کسی زید</p></div>
<div class="m2"><p>ناتوانی کش تب و حلوا خوش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان سنگین می کنم تا زنده ام</p></div>
<div class="m2"><p>مردن فرهاد با خارا خوش است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت فردا زلف مشکینم نگر</p></div>
<div class="m2"><p>امشبم بر بوی آن فردا خوش است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتیم ناخوش چرایی خسروا؟</p></div>
<div class="m2"><p>چون کنم، چون شکل آن بالا خوش است</p></div></div>