---
title: >-
    شمارهٔ ۱۲۸۴
---
# شمارهٔ ۱۲۸۴

<div class="b" id="bn1"><div class="m1"><p>مرا دل ده که من سنگی ندارم</p></div>
<div class="m2"><p>به جز خون جگر رنگی ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل من برده ای نیکوش، می دار</p></div>
<div class="m2"><p>وگر بد داریش جنگی ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر کویی گرم رسوا کند عشق</p></div>
<div class="m2"><p>چو من عاشق شدم، ننگی ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرود درد خود با خویش گویم</p></div>
<div class="m2"><p>که نالان تر ز خود چنگی ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز من تا صبر صد فرسنگ راه است</p></div>
<div class="m2"><p>ولی من پای فرسنگی ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دهندم پند و با من در نگیرد</p></div>
<div class="m2"><p>که من عقلی و فرهنگی ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منم خسرو که از غم کوه فرهاد</p></div>
<div class="m2"><p>به سینه دارم و سنگی ندارم</p></div></div>