---
title: >-
    شمارهٔ ۱۰۹۵
---
# شمارهٔ ۱۰۹۵

<div class="b" id="bn1"><div class="m1"><p>زلفت از باد دگر باشد و از شانه دگر</p></div>
<div class="m2"><p>هست یک فتنه لبت، نرگس مستانه دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در غمت جان ز تنم رفت و خیال تو بماند</p></div>
<div class="m2"><p>عاقبت خویش دگر باشد و بیگانه دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل آسوده دگر، حال پریشان دگر است</p></div>
<div class="m2"><p>شهر آباد دگر باشد و ویرانه دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اهل صورت که خودآرای بود، سوختنی است</p></div>
<div class="m2"><p>کرم شب تاب دگر باشد و پروانه دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دل افسانه که گفتی و ببردی خوابم</p></div>
<div class="m2"><p>بهر خواب اجلم گوی یک افسانه دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به تکلف بشود عشق گران جان خرد</p></div>
<div class="m2"><p>بیهش باده دگر باشد و دیوانه دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاقبت گشت دروغ آنچه گمان می بردند</p></div>
<div class="m2"><p>که چو خسرو نبود عاقل و فرزانه دگر</p></div></div>