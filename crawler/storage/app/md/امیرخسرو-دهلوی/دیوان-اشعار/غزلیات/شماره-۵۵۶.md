---
title: >-
    شمارهٔ ۵۵۶
---
# شمارهٔ ۵۵۶

<div class="b" id="bn1"><div class="m1"><p>سوار من که ره در سینه دارد</p></div>
<div class="m2"><p>زبان پر مهر و دل پر کینه دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال اسپ او، شطرنج بازی</p></div>
<div class="m2"><p>همه با استخوان سینه دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سم بوسیدن شکر دهانان</p></div>
<div class="m2"><p>سمند او به پا شیرینه دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازین پس ما و درویشی، چو درویش</p></div>
<div class="m2"><p>هوس پوشیدن پشمینه دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کند بر ما جفاها و نداند</p></div>
<div class="m2"><p>که حق صحبت دیرینه دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین مه نیست امروزینه این جور</p></div>
<div class="m2"><p>که دل بر دوستان پر کینه دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل خسرو به پا مالد نترسد</p></div>
<div class="m2"><p>مگر پا بر سر گنجینه دارد</p></div></div>