---
title: >-
    شمارهٔ ۱۶۹۴
---
# شمارهٔ ۱۶۹۴

<div class="b" id="bn1"><div class="m1"><p>بیا، ای باغ جان، تا بنگرم سرو روان تو</p></div>
<div class="m2"><p>مرا، دربان، رها کن تا بمیرد باغبان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز فریادم بنالد کوه و ره ندهی به سوی خود</p></div>
<div class="m2"><p>تعالی الله چه سنگ است این دل نامهربان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسوزم و آه برنارم، گرفتم مردمی آمد</p></div>
<div class="m2"><p>نه آخر دوستم من، چون روا دارم زیان تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخواهی دید کز ظلم تو ناگه بهترین روزی</p></div>
<div class="m2"><p>من مظلوم خواهم هر دو دست اندر عنان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا گفتی «که باشی تو که بوسی آستان من »</p></div>
<div class="m2"><p>گر آن گستاخیم بخشی، غلام رایگان تو!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وگر زین ننگ می داری که خود را ز آن تو گفتم</p></div>
<div class="m2"><p>من تنها از آن خود، دل و جانم از آن تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو آگه نی و من با تو ازینسان عشق می سازم</p></div>
<div class="m2"><p>که خود را گه گهی دشنام گویم از زبان تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رقیبا، گفتیم کو گفت خاکم در دهان کردی</p></div>
<div class="m2"><p>تو گر این راست می گویی، شکر اندر دهان تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به حیله زیستی خسرو که دی پیش آمد و دیدی</p></div>
<div class="m2"><p>کنون باز آمد آن مردم کش، اینک بهر جان تو</p></div></div>