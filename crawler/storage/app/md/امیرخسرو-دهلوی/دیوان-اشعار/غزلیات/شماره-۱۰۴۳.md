---
title: >-
    شمارهٔ ۱۰۴۳
---
# شمارهٔ ۱۰۴۳

<div class="b" id="bn1"><div class="m1"><p>دلی کاو عاشق روییست در گلزار نگشاید</p></div>
<div class="m2"><p>گر کاندر دل یاری ست از اغیار نگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو، ای باد و تماشا دیگران را بر بسوی گل</p></div>
<div class="m2"><p>که ما را غنچه پر خون است، در گلزار نگشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه طالع دارم این کز آسمان یک کاروان غم</p></div>
<div class="m2"><p>که آید بر زمین، جز بر دل من بار نگشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا در کار خود کند است دندان، زان ترش ابرو</p></div>
<div class="m2"><p>بدین دندان که من دارم گره از کار نگشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسیر کفر گیسوی صنم چون برهمن باید</p></div>
<div class="m2"><p>که گر رگهای او بگسلد گره زنار نگشاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زند بسیار لاف زهد و تقوی پارسا، لیکن</p></div>
<div class="m2"><p>همان بهتر که چشم خود در آن رخسار نگشاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جرم عشق اگر کافر کنندم خلق گو، می کن</p></div>
<div class="m2"><p>مرا باری زیان هرگز به استغفار نگشاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه ساعت بود آن کاندر رخ او سرخ شد چشمم</p></div>
<div class="m2"><p>که جز خون هر دمی زین دیده بیدار نگشاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل خود با در و دیوار خالی می کند خسرو</p></div>
<div class="m2"><p>بمیرد گر غم خود با در و دیوار نگشاید</p></div></div>