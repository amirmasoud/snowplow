---
title: >-
    شمارهٔ ۸۵۹
---
# شمارهٔ ۸۵۹

<div class="b" id="bn1"><div class="m1"><p>چشم تو خفته ایست که در خواب می رود</p></div>
<div class="m2"><p>زلف تو آفتی ست که در تاب می رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هندوی سنبل تو چه دزد دلاور است؟</p></div>
<div class="m2"><p>کو شب به روشنایی مهتاب می رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دم ز شور پسته شیرین تو مرا</p></div>
<div class="m2"><p>دامن پر از سرشک چو عناب می رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشتم در آب دیده چنان غرق کاین زمان</p></div>
<div class="m2"><p>صد نیزه برتر از سر من آب می رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی عنان سرکش گلگون کشیده دار</p></div>
<div class="m2"><p>کاین بادپای عمر به اشتاب می رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را ز طاق ابروی جانان گریز نیست</p></div>
<div class="m2"><p>زاهد اگر به گوشه محراب می رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو چو گشت معتکف آستان دوست</p></div>
<div class="m2"><p>هرگز به طعن دشمن ازین باب می رود؟</p></div></div>