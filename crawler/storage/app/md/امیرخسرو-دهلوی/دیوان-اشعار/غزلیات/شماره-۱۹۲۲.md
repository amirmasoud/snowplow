---
title: >-
    شمارهٔ ۱۹۲۲
---
# شمارهٔ ۱۹۲۲

<div class="b" id="bn1"><div class="m1"><p>امید نبود ار چه مرا یک نظر از وی</p></div>
<div class="m2"><p>هم دید که بسیار بود این قدر از وی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلطان ز کجا بر هوسش چشم نگارد؟</p></div>
<div class="m2"><p>درویش که در یوزه کند یک نظر از وی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل می کشدم جانب آن غنچه هنوزم</p></div>
<div class="m2"><p>هست ار چه که صد تیر بلا در نظر از وی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پژمرده مباد، ار چه خورد از جگرم آب</p></div>
<div class="m2"><p>آن شاخ جوانی که نخوردیم بر از وی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش از دل من یاد نمی کرد خیالش</p></div>
<div class="m2"><p>کاین رفته کجا شد که نیامد خبر از وی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد جان به فدایش که گه کشتن عشاق</p></div>
<div class="m2"><p>بنمایدم از دور که گیرند بر از وی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از موی تو بر پای ملائک نهد اشکل</p></div>
<div class="m2"><p>حسنت که نگشته ست خیال بشر از وی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دور از تو مرا دور کنند از تو و گویم</p></div>
<div class="m2"><p>دور از همه کس بود توانم مگر از وی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در کشتن ما عیب کنندش همه، لیکن</p></div>
<div class="m2"><p>گر عیب نگیری، چه خوش است این هنر از وی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من داشته جان را به صد افسانه همه شب</p></div>
<div class="m2"><p>وانگه همه جنبیدن باد سحر از وی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مپسند که میرم چو سگان بر سر راهت</p></div>
<div class="m2"><p>خسرو سگ خانه ست، مبندید در از وی</p></div></div>