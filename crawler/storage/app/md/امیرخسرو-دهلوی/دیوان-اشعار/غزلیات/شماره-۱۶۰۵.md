---
title: >-
    شمارهٔ ۱۶۰۵
---
# شمارهٔ ۱۶۰۵

<div class="b" id="bn1"><div class="m1"><p>گواه جبین است بر درد من</p></div>
<div class="m2"><p>سرشک روان بر رخ زرد من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببخشای بر ناله عندلیب</p></div>
<div class="m2"><p>الا، ای گل نازپرورد من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که گر هم بدین نوع باشد فراق</p></div>
<div class="m2"><p>به کوی تو آرد صبا گرد من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که دیده ست هرگز چنین آفتی؟</p></div>
<div class="m2"><p>کزو می برآید دم سرد من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فغان من از دست جور تو نیست</p></div>
<div class="m2"><p>که از طالع مادر آورد من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من اندر خور بندگی نیستم</p></div>
<div class="m2"><p>وز اندازه بیرون تو در خورد من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو دردی نداری که دردت مباد</p></div>
<div class="m2"><p>از آن رحمتت نیست بر درد من</p></div></div>