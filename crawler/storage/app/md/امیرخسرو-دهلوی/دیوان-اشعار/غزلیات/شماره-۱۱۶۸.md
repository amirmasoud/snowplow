---
title: >-
    شمارهٔ ۱۱۶۸
---
# شمارهٔ ۱۱۶۸

<div class="b" id="bn1"><div class="m1"><p>آنکه از جان دوست تر می دارمش</p></div>
<div class="m2"><p>گر مرا بگذاشت من نگذارمش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بدو دارم ز من رنجید و رفت</p></div>
<div class="m2"><p>می دهم جان تا مگر باز آرمش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه در خون دل من خسته است</p></div>
<div class="m2"><p>من دو چشم خویش می پندارمش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قالب بی روح دارم، می برم</p></div>
<div class="m2"><p>تا به خاک کوی او بسپارمش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می دهم جان روز و شب در کوی دوست</p></div>
<div class="m2"><p>گوهری زین بیش اگر در کارمش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی در پای تو می مالم، مرنج</p></div>
<div class="m2"><p>گر به روی سخت خود می آرمش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه رویش داد بر بادم چو زلف</p></div>
<div class="m2"><p>همچنان جانب نگه می دارمش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه هست او یار من، من یار او</p></div>
<div class="m2"><p>من کجا یارم که گویم یارمش!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچ رحمی نیست بر بیمار خویش</p></div>
<div class="m2"><p>آن طبیبی را که من بیمارمش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با دل خود گفتم او را، چیستی؟</p></div>
<div class="m2"><p>گفت خسرو، او گل و من خارمش</p></div></div>