---
title: >-
    شمارهٔ ۱۶۳۶
---
# شمارهٔ ۱۶۳۶

<div class="b" id="bn1"><div class="m1"><p>از آن خویش کنم من که جان دهم بستان</p></div>
<div class="m2"><p>که ز آن خود نشنوی تو به حیله و دستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدین صفت که ز سر تا قدم همه شکری</p></div>
<div class="m2"><p>حلال بادت شیری که خوردی از پستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه باشد ار به سر وقت من رسی وقتی</p></div>
<div class="m2"><p>چو مکرمان به سوی کلبه تهی دستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برون خرام که تا پارسای ثابت حال</p></div>
<div class="m2"><p>فدم درست نیارد نهاد چون مستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا که دعوی بازار زهد و تقوی بود</p></div>
<div class="m2"><p>به یک کرشمه چشمت تمام بشکست آن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من ضعیف چه مرد غمت که بازوی عشق</p></div>
<div class="m2"><p>به پنجه تاب دهد دست رستم دستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صلای عیش دهندم مرا که دل جایی ست</p></div>
<div class="m2"><p>چه جای رفتن باغ است و گشتن بستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غلام ناله دیوانگان روی توام</p></div>
<div class="m2"><p>خوش است زمزمه مرغ در بهارستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گهی گهی دل من شاد کن به دشنامی</p></div>
<div class="m2"><p>دعای خسرو مسکین بدین قدر بستان</p></div></div>