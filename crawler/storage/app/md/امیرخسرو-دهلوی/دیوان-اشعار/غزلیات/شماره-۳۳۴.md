---
title: >-
    شمارهٔ ۳۳۴
---
# شمارهٔ ۳۳۴

<div class="b" id="bn1"><div class="m1"><p>سپیده دم که زمانه ز رخ نقاب انداخت</p></div>
<div class="m2"><p>به زلف تیره شب نور صبح تاب انداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کلید زر شد و بگشاد آفتاب فلک</p></div>
<div class="m2"><p>به دیده ها که شب تیره قفل خواب انداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سحر جواهر انجم یگان یگان دزدید</p></div>
<div class="m2"><p>چو صبح پرده دریدش بر آفتاب انداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چگونه صبح بخندد که روی ابر سیاه</p></div>
<div class="m2"><p>سفیده کرد و ز دیبا بر او نقاب انداخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدید از دل دیر سیا شب روشن</p></div>
<div class="m2"><p>کمان چرخ همان تیر کز شهاب انداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کنج روزن و در گذشت ماهتاب نهان</p></div>
<div class="m2"><p>چو مهر خنجر کین سوی ماهتاب انداخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به آخر آمده شب را به وقت صبح نفس</p></div>
<div class="m2"><p>که تیغ خورد و ز خورشید خون ناب انداخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برفت شب ز پی زنده داشتن خود را</p></div>
<div class="m2"><p>به پرتو نظر شیخ کامیاب انداخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فلک جنابا، بپذیر بنده خسرو را</p></div>
<div class="m2"><p>چه خویش را به جناب فلک جناب انداخت</p></div></div>