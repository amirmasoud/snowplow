---
title: >-
    شمارهٔ ۲۰۷
---
# شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>تا خیال روی او را دیده در تب دیده است</p></div>
<div class="m2"><p>مردم چشمم به خون در اشک ما غلتیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چرا با شمع رویش آتش تب یار شد</p></div>
<div class="m2"><p>دل چو دود زلف او بر خود بسی پیچیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر لبش هر داغ جانسوزی که بس تبخاله شد</p></div>
<div class="m2"><p>زان جراحت بر دل و جان من شوریده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش بر بالین یارم شمع از غم پیش من</p></div>
<div class="m2"><p>تا سحر بیچاره بر جان همچو من لرزیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون به نوک غمزه آن بت از لب من خون گشاد</p></div>
<div class="m2"><p>در تن من هم ز غیرت خون من شوریده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ندارد طاقتی کز آب خیزد دمی</p></div>
<div class="m2"><p>نرگس بیمار یارم درد سر چون دیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوش چون آمد خیال سرو قدش پیش من</p></div>
<div class="m2"><p>تا سحر خسرو به جایش گرد سر گردیده است</p></div></div>