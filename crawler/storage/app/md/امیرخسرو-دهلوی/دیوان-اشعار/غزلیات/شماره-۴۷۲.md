---
title: >-
    شمارهٔ ۴۷۲
---
# شمارهٔ ۴۷۲

<div class="b" id="bn1"><div class="m1"><p>ما را تو صنم باشی، دیگر به چه کار آید</p></div>
<div class="m2"><p>با لعل جگر سوزت، شکر به چه کار آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خنجر کشی از مژگان بر سینه من، چون من</p></div>
<div class="m2"><p>بی تیغ شدم کشته، خنجر به چه کار آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کافر خط هندویت جایی که کشد ما را</p></div>
<div class="m2"><p>یارب که به هندوستان کافر به چه کار آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل از پی آن خواهم تا خون شود از عشقت</p></div>
<div class="m2"><p>گر کار بدین ناید، دیگر به چه کار آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گوهر عشق خود زیور کنمت، بنگر</p></div>
<div class="m2"><p>خوبی چو فزون باشد، زیور به چه کار آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد خسته درون من از بیم جفا کیشان</p></div>
<div class="m2"><p>چون می ندهد دادم، داور به چه کار آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اختر شمرم هر شب در طالع خود، لیکن</p></div>
<div class="m2"><p>چون کار قضا دارد، اختر به چه کار آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر جان و دل خسرو هر لحظه نهد باری</p></div>
<div class="m2"><p>کاین عاشق مسکین هم دیگر به چه کار آید</p></div></div>