---
title: >-
    شمارهٔ ۷۹۰
---
# شمارهٔ ۷۹۰

<div class="b" id="bn1"><div class="m1"><p>لعل شیرینی چو خندان می‌شود</p></div>
<div class="m2"><p>در جهان شیرینی ارزان می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قد او هرگه که جولان می‌کند</p></div>
<div class="m2"><p>گوییا سرو خرامان می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرتو رویش چو می‌تابد ز دور</p></div>
<div class="m2"><p>آفتاب از شرم پنهان می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قصه زلفش چون نمی‌گویم به کس</p></div>
<div class="m2"><p>زان که خاطره‌ها پریشان می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من نه تنها می‌شوم حیران او</p></div>
<div class="m2"><p>هرکه او را دید حیران می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مه چو می‌گوید، چه بنوازم ترا؟</p></div>
<div class="m2"><p>تا نگه کردم، پشیمان می‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه را شاهی عالم آرزوست</p></div>
<div class="m2"><p>بنده درگاه سلطان می‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسروی کز کلک گوهربار او</p></div>
<div class="m2"><p>کار بی‌سامان به سامان می‌شود</p></div></div>