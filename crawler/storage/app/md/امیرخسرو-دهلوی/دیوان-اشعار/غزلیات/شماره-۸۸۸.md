---
title: >-
    شمارهٔ ۸۸۸
---
# شمارهٔ ۸۸۸

<div class="b" id="bn1"><div class="m1"><p>آن خون که گاه مستی از آن مست ما چکد</p></div>
<div class="m2"><p>از زلف فتنه بارد و از جان بلا چکد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوید چو رخ به صبح، کند غرقه خلق را</p></div>
<div class="m2"><p>هر قطره ای که از رخ آن آشنا چکد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای زاهد، از دعای بد ایمن مشو که شب</p></div>
<div class="m2"><p>مستان دعا کنند، که خون از دعا چکد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام لبت که محتشمان را حلال باد</p></div>
<div class="m2"><p>زو جرعه ای چه باشد، اگر بر گدا چکد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردم در این هوس که شبی سر نهم به پاش</p></div>
<div class="m2"><p>زانگونه کاب چشم منش زیر پا چکد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک درت به چشم من، از گریه خون خورم</p></div>
<div class="m2"><p>تا خود جزای چشم من آن توتیا چکد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محکم قبا مبند که دامن بگیردت</p></div>
<div class="m2"><p>خون هزار دل که ز بند قبا چکد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمشیر آبدار کشیدی بر اهل عشق</p></div>
<div class="m2"><p>دولت بود که ضربی از آن سوی ما چکد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو می روی و از پی خونریز خویشتن</p></div>
<div class="m2"><p>خسرو دوان که تا خوی اسپت کجا چکد؟</p></div></div>