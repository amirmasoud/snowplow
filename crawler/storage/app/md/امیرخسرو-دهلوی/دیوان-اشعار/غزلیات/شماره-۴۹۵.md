---
title: >-
    شمارهٔ ۴۹۵
---
# شمارهٔ ۴۹۵

<div class="b" id="bn1"><div class="m1"><p>بویی ز سر زلف نگارین به من آرید</p></div>
<div class="m2"><p>یک تار ازان طره مشکین به من آرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مخمورم و جانم به سوی می نگران است</p></div>
<div class="m2"><p>آن باده که در داد نخستین به من آرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهید که از خاک برآیم پس صد سال</p></div>
<div class="m2"><p>از میکده بوی می رنگین به من آرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر گه که غمی گشت پدید از دل، گفتم</p></div>
<div class="m2"><p>غم را نخورد جز دل غمگین به من آرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان می سپرد از غم هجران تو خسرو</p></div>
<div class="m2"><p>روزی خبر عاشق مسکین به من آرید</p></div></div>