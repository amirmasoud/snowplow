---
title: >-
    شمارهٔ ۴۶۵
---
# شمارهٔ ۴۶۵

<div class="b" id="bn1"><div class="m1"><p>رخی داری که وصف آن به خاطر درنمی‌گنجد</p></div>
<div class="m2"><p>شراب لذت دیدار در ساغر نمی‌گنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی را در دهان تنگ خود چندین شکر گنجد</p></div>
<div class="m2"><p>که تو می‌خندی و اندر جهان شکر نمی‌گنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا چیده بود آن مو همه کز لب برون آری</p></div>
<div class="m2"><p>ز تنگی در دهان تو چو مویی در نمی‌گنجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیالت چون به چشم آمد، برون شد مردم چشمم</p></div>
<div class="m2"><p>که در یک دیده مردم دو مردم در نمی‌گنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا سودای آن خط همچو دفتر ساخت تو بر تو</p></div>
<div class="m2"><p>بگردانم ورق اکنون که در دفتر نمی‌گنجد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درآ در چشم و بیرون کن خیالات دگر کآنجا</p></div>
<div class="m2"><p>نگنجد مو که دو سلطان به یک کشور نمی‌گنجد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا گویی که دل بر یار دیگر نه، نهم، لیکن</p></div>
<div class="m2"><p>همین در دل تو می‌گنجی، کس دیگر نمی‌گنجد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز هجرت موی شد خسرو، ولی از شادی وصلت</p></div>
<div class="m2"><p>ببین آن موی را باری که در کشور نمی‌گنجد</p></div></div>