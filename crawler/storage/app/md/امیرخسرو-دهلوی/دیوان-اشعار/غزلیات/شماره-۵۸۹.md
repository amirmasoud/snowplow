---
title: >-
    شمارهٔ ۵۸۹
---
# شمارهٔ ۵۸۹

<div class="b" id="bn1"><div class="m1"><p>آن را غم تو یار باشد</p></div>
<div class="m2"><p>با خوش دلیش چکار باشد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صوفی چو شکست توبه، ساقی</p></div>
<div class="m2"><p>مگذار که هوشیار باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مستی که سبو کشد، مپندار</p></div>
<div class="m2"><p>کورا قدم استوار باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می حاجت نیست مستیم را</p></div>
<div class="m2"><p>در چشم تو تا خمار باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان دادم و داغ عشق بردم</p></div>
<div class="m2"><p>کانجا ز تو یادگار باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>معذور بود ز ناله بلبل</p></div>
<div class="m2"><p>جایی که گل و بهار باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شک نیست که نشتری چشیده ست</p></div>
<div class="m2"><p>جنگی که فغانش زار باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرهم چو نمی پذیرد این دل</p></div>
<div class="m2"><p>بگذار که تا فگار باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو به غلامیت عزیز است</p></div>
<div class="m2"><p>گر خوار کنیش، خوار باشد</p></div></div>