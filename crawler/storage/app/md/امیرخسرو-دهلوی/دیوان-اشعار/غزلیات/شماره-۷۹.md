---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>شبم خیال تو بس، با قمر چه کار مرا</p></div>
<div class="m2"><p>من و چو کوه شبی، با سحر چه کار مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من آستان تو بوسم، حدیث لب نکنم</p></div>
<div class="m2"><p>چو من به خاک خوشم، با شکر چه کار مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبینم آن لب خندان ز بیم جان یک ره</p></div>
<div class="m2"><p>ز دور سنگ خورم، با گهر چه کار مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پدر بزاد مرا بهر آن که تو کشیم</p></div>
<div class="m2"><p>وگرنه با چو تو زیبا پسر، چه کار مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر قضاست که میرم به عشق تو، آری</p></div>
<div class="m2"><p>به کارهای قضا و قدر چه کار مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به طاعتم طلبند و به عشرتم خوانند</p></div>
<div class="m2"><p>من و غم تو، به کار دگر چه کار مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طلاق داده دل و عقل و هوش را، خسرو</p></div>
<div class="m2"><p>به گشت کوی تو با این حشر چه کار مرا</p></div></div>