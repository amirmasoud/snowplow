---
title: >-
    شمارهٔ ۲۴۷
---
# شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>صفتی ست آب حیوان، زدهان نوشخندت</p></div>
<div class="m2"><p>اثری ست جان شیرین، ز لبان همچو قندت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کدام سرو بینم که ز تو صبور باشم</p></div>
<div class="m2"><p>که دراز ماند در دل هوس قد بلندت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خزان هجر مردم، چه کمت شود که ما را</p></div>
<div class="m2"><p>به غلط گلی شکفتی ز دهان نوشخندت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم و هزار پیچش ز خیال زلف در دل</p></div>
<div class="m2"><p>به کجا روم که جانم رهد از خم کمندت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رهت فتاده مردم روشی نما به جولان</p></div>
<div class="m2"><p>که چو مردنی ست باری به ته سم سمندت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تو دور چند سوزم به میان آتش غم</p></div>
<div class="m2"><p>همه غیرتم ز عود و همه رشکم از سپندت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کن اشارتی چو شاهی که برند بند بندم</p></div>
<div class="m2"><p>که ز لطف این سیاست برهم مگر ز بندت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بزن، ای رفیق، آتش که اثر نماندم تا</p></div>
<div class="m2"><p>تو رهی ز مالش، من، من سوخته ز بندت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مپز این خیال خسرو که به عشق در نمانی</p></div>
<div class="m2"><p>بود ار چه زاهل شهری شب و روز ریشخندت</p></div></div>