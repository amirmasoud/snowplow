---
title: >-
    شمارهٔ ۲۴۲
---
# شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>ستمی کز تو کشد مرد، ستم نتوان گفت</p></div>
<div class="m2"><p>نام بیداد تو جز لطف و کرم نتوان گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آرزوی تو ز روی دگران کم نشود</p></div>
<div class="m2"><p>حاجت کعبه به دیدار حرم نتوان گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن تو خانه برانداز مسلمانانست</p></div>
<div class="m2"><p>ناز هم یارب و زنهار که کم نتوان گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چه سرهای عزیزان به درت خاک شده ست</p></div>
<div class="m2"><p>وه که آن خاک قدم خاک قدم نتوان گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رشکم آید که برم نام تو پیش دگران</p></div>
<div class="m2"><p>ذکر انصاف تو در پیش تو هم نتوان گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون منی باید تا باورش آید غم من</p></div>
<div class="m2"><p>تو که دیوانه و مستی به تو غم نتوان گفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن توبه و آنگه ز جمال خوبان</p></div>
<div class="m2"><p>به که دادند سر زیر علم نتوان گفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قاریی از پی دین برهمنی را می کشت</p></div>
<div class="m2"><p>گفت از بهر سری ترک صنم نتوان گفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسروا گر کشدت یار، مگو کاین ستم است</p></div>
<div class="m2"><p>عدل خوبان را به بیهوده ستم نتوان گفت</p></div></div>