---
title: >-
    شمارهٔ ۱۳۷۷
---
# شمارهٔ ۱۳۷۷

<div class="b" id="bn1"><div class="m1"><p>بر جمالت مبتلایم، چون کنم؟</p></div>
<div class="m2"><p>من به عشقت برنیایم، چون کنم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لاف عشقت می زنم، جانا، ولی</p></div>
<div class="m2"><p>پس فقیر بینوایم، چون کنم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی «از کویم برو، بیگانه باش »</p></div>
<div class="m2"><p>با سگانت آشنایم، چون کنم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر به شاهان در نمی آرد حریف</p></div>
<div class="m2"><p>من که درویش و گدایم، چون کنم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزگاری شد که از لعل لبش</p></div>
<div class="m2"><p>کشته یک مرحبایم، چون کنم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خسرو بیچاره می گوید به صدق</p></div>
<div class="m2"><p>«عاشق روی شمایم، چون کنم؟»</p></div></div>