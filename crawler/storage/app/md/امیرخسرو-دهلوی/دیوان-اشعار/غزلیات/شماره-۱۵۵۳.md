---
title: >-
    شمارهٔ ۱۵۵۳
---
# شمارهٔ ۱۵۵۳

<div class="b" id="bn1"><div class="m1"><p>ای به کویت هر سحرگه جای تنها ماندگان</p></div>
<div class="m2"><p>رحمتی بر چشم خون پالای تنها ماندگان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون به کویت دوست تنها پای را خاکی کند</p></div>
<div class="m2"><p>کس به جز گریه نشوید پای تنها ماندگان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد تن باشد، ولیکن نی بسان درد دل</p></div>
<div class="m2"><p>گر مثل گردون رود بالای تنها ماندگان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با چنین شبها که من دارم، چه باشد، وه که گر</p></div>
<div class="m2"><p>یادت آید روزی از شبهای تنها ماندگان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی منت گویم ز تو«حالم توانی گوش کرد؟»</p></div>
<div class="m2"><p>کانده سخت است در سودای تنها ماندگان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتی از تنهائیم، آخر نیامد وقت آن</p></div>
<div class="m2"><p>کت گذر باشد به محنت جای تنها ماندگان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماند اینم آفتاب و مه که در شبهای غم</p></div>
<div class="m2"><p>سایه باشد مونس شبهای تنها ماندگان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آفتاب چرخ تنها سوزد و گوید «مسوز»</p></div>
<div class="m2"><p>وای تنها ماندگان، ای وای تنها ماندگان!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو غم خسرو کجا دانی که نشنیدی گهی</p></div>
<div class="m2"><p>ناله و فریاد درد افزای تنها ماندگان</p></div></div>