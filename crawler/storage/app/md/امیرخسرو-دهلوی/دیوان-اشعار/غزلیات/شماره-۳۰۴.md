---
title: >-
    شمارهٔ ۳۰۴
---
# شمارهٔ ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>ای باد، ازان بهار خبر ده که تا کجاست</p></div>
<div class="m2"><p>دزدیده زان نگار خبر ده که تا کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر هیچ در رهی گذرانش رسیده ای</p></div>
<div class="m2"><p>یک ره ازان سوار خبر ده که تا کجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من همچو گل بسوختم از آفتاب غم</p></div>
<div class="m2"><p>آن سرو سایه دار خبر ده که تا کجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من ز آب دیده شربت غم نوش می کنم</p></div>
<div class="m2"><p>آن لعل خوشگوار خبر ده که تا کجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خونم ز غم چو نافه بماند اندرون پوست</p></div>
<div class="m2"><p>آن زلف مشکبار خبر ده که تا کجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانم چو سرمه سوده شد از سنگ آرزو</p></div>
<div class="m2"><p>آن چشم پر خمار خبر ده که تا کجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای پیک تیز رو، برو، آن یار را بپرس</p></div>
<div class="m2"><p>کز من برفت یار، خبر ده که تا کجاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای مرغ نامه بر، پر تو گر نوشته شد</p></div>
<div class="m2"><p>باز آی زینهار خبر ده که تا کجاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو که این حدیث ز یاری شنیده ای</p></div>
<div class="m2"><p>بر پر، وزان دیار خبر ده که تا کجاست</p></div></div>