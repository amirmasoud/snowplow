---
title: >-
    شمارهٔ ۲۹۵
---
# شمارهٔ ۲۹۵

<div class="b" id="bn1"><div class="m1"><p>خونخوار چشم تو که ره مرد و زن زده ست</p></div>
<div class="m2"><p>هر شب به خوابگاه من ممتحن زده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خاک راه بوسم و از خود به غیرتم</p></div>
<div class="m2"><p>آه از صبا که بوسه ترا بر دهن زده ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل دامنت گرفت و رها چون کند کسی</p></div>
<div class="m2"><p>پیری که بوی یوسفش از پیرهن زده ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه گه بیامدی به سوی کاروان صبر</p></div>
<div class="m2"><p>لیکن بلای غمزه تو راه من زده ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی بیا که شب به میان کرد زهد و رفت</p></div>
<div class="m2"><p>زان یک غزل که صبحدم آن راهزن زده ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای پارسا، چه سر زنیم تو، که می فروش</p></div>
<div class="m2"><p>صد کوزه بر سر من توبه شکن زده ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دی گفتی، آه می زنی از مات شرم نیست</p></div>
<div class="m2"><p>آتش زده ست درمن و زان یک سخن زده ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزم چو بی ویست شبش خواب دیده ام</p></div>
<div class="m2"><p>کان جان پاک تکیه به پهلوی من زده ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر کوه باد ناله خسرو نه بر دلت</p></div>
<div class="m2"><p>کاین تیشه ایست سخت که آن کوهکن زده ست</p></div></div>