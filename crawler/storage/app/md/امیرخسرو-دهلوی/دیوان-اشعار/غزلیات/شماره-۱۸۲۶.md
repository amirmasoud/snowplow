---
title: >-
    شمارهٔ ۱۸۲۶
---
# شمارهٔ ۱۸۲۶

<div class="b" id="bn1"><div class="m1"><p>جان به فدات می کنم، بو که از آن من شوی</p></div>
<div class="m2"><p>مرده تنی من ببین، کوش کز آن من شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد به بقین دیگران ماه تمام روی تو</p></div>
<div class="m2"><p>چشمه آفتاب شو، گر به گمان من شوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند به چربی زبان همچو چراغ سوزیم</p></div>
<div class="m2"><p>سوخته عاقبت گهی هم به زبان من شوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به فغان من ترا دردسری ست، باز ده</p></div>
<div class="m2"><p>نیستم آن طمع که تو دردستان من شوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیم بگیرم از برت، گر بکنی عنایتی</p></div>
<div class="m2"><p>وام بخواهم از لبت، گر تو ضمان من شوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برگذر دو چشم من کاب روانست در گذر</p></div>
<div class="m2"><p>پیش که غرقه ناگهان ز آب روان من شوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتنه خسروی به رخ، پهلوی من نشین دمی</p></div>
<div class="m2"><p>بو که به چیزی از بلا، فتنه نشان من شوی</p></div></div>