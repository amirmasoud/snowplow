---
title: >-
    شمارهٔ ۱۶۱۹
---
# شمارهٔ ۱۶۱۹

<div class="b" id="bn1"><div class="m1"><p>جانا همان و دل همان درد من شیدا همان</p></div>
<div class="m2"><p>هر کس به سودای گلی، جان مرا سودا همان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در باغ هر کس از گلی مست و من شوریده را</p></div>
<div class="m2"><p>دیده به سوی سرو و گل اندر دل شیدا همان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویند کز بهر چرا چندین خوری غم، چون کنم</p></div>
<div class="m2"><p>کآمد خوشی بخش همه، بخش من تنها همان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد، به محرابم مخوان، صوفی، ز تسبیحم مگوی</p></div>
<div class="m2"><p>ماییم گویی ذنبی محراب و درد ما همان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سویش به پای خود شدم، وز پای دیگر آمدم</p></div>
<div class="m2"><p>این بار سر خواهم نهاد آن را که مست پا همان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانا، چه گویم درد خود با تو که بهر جان من</p></div>
<div class="m2"><p>تو دل همان داری و من آن لعبت خارا همان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل پر ز سودای لبت، در سینه جانی خشک و بس</p></div>
<div class="m2"><p>نرخ متاع از حد برون، درویش را کالا همان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتی وجودت خاک شد، آن خاک را جا بر درم</p></div>
<div class="m2"><p>من زحمت خود می برم، ماند مگر بر جا همان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چندان بی جویی کشتنم کان غم که دارد هجر تو</p></div>
<div class="m2"><p>خواهی شنیدن ناگهان امروز تا فردا همان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پندم دهند و نشنوم، خواهم که هم صبری کنم</p></div>
<div class="m2"><p>چون تو به خاطر بگذری، دل باز خسرو را همان</p></div></div>