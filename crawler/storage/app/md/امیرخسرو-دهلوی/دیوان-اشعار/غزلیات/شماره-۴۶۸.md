---
title: >-
    شمارهٔ ۴۶۸
---
# شمارهٔ ۴۶۸

<div class="b" id="bn1"><div class="m1"><p>دروغ و راستی کان غمزه غماز پیوندد</p></div>
<div class="m2"><p>درد صد پرده عاشق ز لب وان باز پیوندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلا را نو کند رسم و طریق فتنه نو سازد</p></div>
<div class="m2"><p>چو او اول کرشمه با طریق ناز پیوندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سینه نارسیده بگذرد و ندر جگر شیند</p></div>
<div class="m2"><p>خدنگی با کمان کان ترک تیرانداز پیوندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خون گرم دل پیوست با او گر بری دل را</p></div>
<div class="m2"><p>چو خون گرم است هر صد بار دیگر باز پیوندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا چه حد وصلست، این قدر بس قرب او باشد</p></div>
<div class="m2"><p>سخن با یکدگر کاواز با آواز پیوندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه باشد حال من جایی که هر شب بهر تاراجم</p></div>
<div class="m2"><p>خیالش ساخته با این دل ناساز پیوندد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همی گویند جان خواهی، مجو پیوند ازو، خسرو</p></div>
<div class="m2"><p>ز بهر زیستن گنجشک با شهباز پیوندد</p></div></div>