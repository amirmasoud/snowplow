---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>باشد آن روزی که بینم غمگسار خویش را</p></div>
<div class="m2"><p>شادمان یابم دل امیدوار خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد دو چشمم ز انتظارش چار در راه امید</p></div>
<div class="m2"><p>چار جانب وقف کردم هر چهار خویش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاید ار بر خاک خسپم همچو گل پر خون کنار</p></div>
<div class="m2"><p>کز چنان سروی تهی کردم کنار خویش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک می بیزم به دامان، چون کنم گم کرده ام</p></div>
<div class="m2"><p>در میان خاک در آبدار خویش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مست گشتی چون ترا پیمانه پر داده ست دوست</p></div>
<div class="m2"><p>خیز و بستان ساغر و بشکن خمار خویش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می نپرسد، گر غباری دارد آن خاکی ز من</p></div>
<div class="m2"><p>تا به آب دیده بنشانم غبار خویش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل که از جعد تو بدخو شد نمی گیرد قرار</p></div>
<div class="m2"><p>ساعتی بفرست جعد همچو مار خویش را</p></div></div>