---
title: >-
    شمارهٔ ۱۰۰۹
---
# شمارهٔ ۱۰۰۹

<div class="b" id="bn1"><div class="m1"><p>دل از بند زلفت رها کی شود؟</p></div>
<div class="m2"><p>دلت با دلم آشنا کی شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگویی که از لعل سیراب تو</p></div>
<div class="m2"><p>مراد دل ما رواکی شود؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولی مرهم لعل خودکام تو</p></div>
<div class="m2"><p>به کام دل ریش ما کی شود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی شد دل از بند زلفش رها</p></div>
<div class="m2"><p>کنون دل نهادیم تا کی شود؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجا همدم و یار خسرو شوی!</p></div>
<div class="m2"><p>که شه همنشین گدا کی شود؟</p></div></div>