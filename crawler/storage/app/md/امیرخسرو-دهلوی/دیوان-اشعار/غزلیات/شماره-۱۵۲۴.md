---
title: >-
    شمارهٔ ۱۵۲۴
---
# شمارهٔ ۱۵۲۴

<div class="b" id="bn1"><div class="m1"><p>بر آن رویی که نتوان می گرفتن</p></div>
<div class="m2"><p>ترش بر روی ما تا کی گرفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حلالش باد خونم آن چنان، کوست</p></div>
<div class="m2"><p>جفایت چون توان بر وی گرفتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبا بستان کباب نیم سوزم</p></div>
<div class="m2"><p>به دستش ده به جای می گرفتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجا افتاده ای، زاهد، ز ما دور؟</p></div>
<div class="m2"><p>نشاید مفلسان را پی گرفتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین کز غمزه شوخت امان یافت</p></div>
<div class="m2"><p>بخواهد فتنه روم و ری گرفتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا هم هست شوقی، لیک فرق است</p></div>
<div class="m2"><p>بتا از سوختن تا خوی گرفتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تو در خان و مان سوزی اشارت</p></div>
<div class="m2"><p>ز خسرو آتش اندر نی گرفتن</p></div></div>