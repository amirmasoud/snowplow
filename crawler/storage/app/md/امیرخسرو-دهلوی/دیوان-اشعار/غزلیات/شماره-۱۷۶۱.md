---
title: >-
    شمارهٔ ۱۷۶۱
---
# شمارهٔ ۱۷۶۱

<div class="b" id="bn1"><div class="m1"><p>جهان تا مه روشنت ساخته</p></div>
<div class="m2"><p>ز دلها فلک خرمنت ساخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخ خویش تا بیند اندر رخت</p></div>
<div class="m2"><p>مه آیینه روشنت ساخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قضا کرده یک جا هزار آرزو</p></div>
<div class="m2"><p>خلاصه کشیده، تنت ساخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غمت پر ز خون کرده دلها بسی</p></div>
<div class="m2"><p>وزان غنچه ها گلشنت ساخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میا تنگ، اگر خسرو تنگ دل</p></div>
<div class="m2"><p>دل تنگ را مسکنت ساخته</p></div></div>