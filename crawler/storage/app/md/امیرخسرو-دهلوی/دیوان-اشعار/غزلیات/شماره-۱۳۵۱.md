---
title: >-
    شمارهٔ ۱۳۵۱
---
# شمارهٔ ۱۳۵۱

<div class="b" id="bn1"><div class="m1"><p>ما به کوی تو سگانیم و به راه تو خسیم</p></div>
<div class="m2"><p>این که پیش تو پس است، از همه رو نیز پسیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر یک سجده به راه تو سراسر عشقیم</p></div>
<div class="m2"><p>بهر یک بوسه به پای تو لبالب هوسیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگران را چه کنی گرد رخ خویش سپند</p></div>
<div class="m2"><p>کز پی سوختنی هم من و دل هر دو بسیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نوازند رقیبان تو ما را، خاکیم</p></div>
<div class="m2"><p>ور بسوزند، بسوزیم که خاشاک و خسیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما که باشیم که ما را سگ خود نام نهی؟</p></div>
<div class="m2"><p>این سخن با دگری گوی که ما هیچ کسیم!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در میان هیچ نه و خشک زبانی به دهان</p></div>
<div class="m2"><p>عالمی کرده پر آواز تو گویی جرسیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عذر تقصیر نخواهیم که از خدمت رفت</p></div>
<div class="m2"><p>گر خدا خواسته باشد که به خدمت برسیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به یکی جرعه می باز خر از خود ما را</p></div>
<div class="m2"><p>که به بازار فنا در گرو یک نفسیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو همایی به کرم سایه فکن بر خسرو</p></div>
<div class="m2"><p>که ز ناچیزی چون سایه پر مگسیم</p></div></div>