---
title: >-
    شمارهٔ ۹۰۰
---
# شمارهٔ ۹۰۰

<div class="b" id="bn1"><div class="m1"><p>ز گشت مست رسید و به هوش خویش نبود</p></div>
<div class="m2"><p>دلم ز صبر بسی لاف زد، ولیش نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زدند راه دلم آهوان بی انصاف</p></div>
<div class="m2"><p>که از هزار خدنگش یکی به کیش نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به صد هزار دلش عاشقان خریدارند</p></div>
<div class="m2"><p>بهای یوسف اگر هفده قلب بیش نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل او فگند مرا در چه زنخدانش</p></div>
<div class="m2"><p>وگرنه چشم من خون گرفته پیش نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نبود امشب سوزنده مرا جز تپ</p></div>
<div class="m2"><p>دل ار چه بود، ولیکن به دست خویش نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمک به ریش من، ای پارسا، مزن از پند</p></div>
<div class="m2"><p>به شکر آنکه دلت هیچگاه ریش نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوش است عشق به گفتن، ولی چه دانی درد</p></div>
<div class="m2"><p>ترا که بود لبی و نمک به ریش نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو وصل می طلبی خسرو، از بلا مگریز</p></div>
<div class="m2"><p>که در جهان عسلی بی گزند نیش نبود</p></div></div>