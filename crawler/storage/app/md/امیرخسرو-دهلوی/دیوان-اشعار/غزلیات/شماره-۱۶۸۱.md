---
title: >-
    شمارهٔ ۱۶۸۱
---
# شمارهٔ ۱۶۸۱

<div class="b" id="bn1"><div class="m1"><p>مست آمد آن نگار که ما مست روی او</p></div>
<div class="m2"><p>دیوانگیست کار من از جستجوی او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خود برید چشم من از روی مردمی</p></div>
<div class="m2"><p>گر آرزو کنید که بینید روی او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر خاک کوی وی دل من دوش گم شده ست</p></div>
<div class="m2"><p>یک ره طلب کنید دل از خاک کوی او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهید تا چو من نشوید از بلای هجر</p></div>
<div class="m2"><p>در من نگه کنید و ببینید سوی او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر تلخ پاسخی دهد از خوی تلخ خویش</p></div>
<div class="m2"><p>هم بشنوید و تلخ مدانید خوی او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر هیچ نیست، پیش نسیم صبا روید</p></div>
<div class="m2"><p>بر خسرو شکسته رسانید بوی او</p></div></div>