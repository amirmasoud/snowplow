---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>شکل دل بردن که تو داری نباشد دلبری را</p></div>
<div class="m2"><p>خواب بندی های چشمت کم بود جادوگری را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ز هجران شد زحل در طالعم کی بوسم آن پا</p></div>
<div class="m2"><p>این سعادت دست ندهد جز مبارک اختری را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین هوس مردم که وقتی سر نهم بر آستانت</p></div>
<div class="m2"><p>بین چه جایی می نهم من هم چنین مدبر سری را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند گویی سوز خود روشن کن از داری زبانی</p></div>
<div class="m2"><p>چون نخیزد شعله تا کی دم دمم خاکستری را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر من بد روز بس کز غم قیامت هاست هر شب</p></div>
<div class="m2"><p>روز من روزی مبادا تا قیامت کافری را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می زنندم طعنه کاخر دل که گم کردی بجوی</p></div>
<div class="m2"><p>من که خود را کرده ام گم چون بجویم دیگری را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوستان گویند ناگه مرد خواهی بر در او</p></div>
<div class="m2"><p>دولتم نبود که گردم خاک از آنگونه دری را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی چو من سوزند یاران گرچه دلسوزند، لیکن</p></div>
<div class="m2"><p>عود چون سوزد بود دل گرمیی هم مجمری را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آه پنهانی خود خوردن که خسرو راست زان بت</p></div>
<div class="m2"><p>بوالعجب تر زین فرو بردن که یارد خنجری را</p></div></div>