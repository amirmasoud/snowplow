---
title: >-
    شمارهٔ ۱۴۰۱
---
# شمارهٔ ۱۴۰۱

<div class="b" id="bn1"><div class="m1"><p>خیز، ای به دل نشسته که بیدل نشسته ایم</p></div>
<div class="m2"><p>مگسل ز ما که بهر تو از خود گسسته ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه، ار به روی تو نگشاییم ما شبی</p></div>
<div class="m2"><p>چشمی که در فراق تو شبها نشسته ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آلوده جفای تو جان می رود درون</p></div>
<div class="m2"><p>هر چند کز خدنگ جفای تو خسته ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سامان ز ما طلب مکن، ای پارسا، که من</p></div>
<div class="m2"><p>میخواره و سفال به تارک شکسته ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ده شراب شادی از آن رو که عقل رفت</p></div>
<div class="m2"><p>دانی که از کدام بلا باز رسته ایم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خسرو، چه جای صرفه جان است و بیم سر</p></div>
<div class="m2"><p>ما را که پیش سنگ ملامت نشسته ایم</p></div></div>