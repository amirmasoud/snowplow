---
title: >-
    شمارهٔ ۱۸۷۶
---
# شمارهٔ ۱۸۷۶

<div class="b" id="bn1"><div class="m1"><p>گاهم ز غمزه‌ها هدف تیر می‌کنی</p></div>
<div class="m2"><p>گاهم زبون چشم زبون گیر می‌کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من جامه کاغذین کنم از رشک کاغذت</p></div>
<div class="m2"><p>کان را چو برگ که هدف تیر می‌کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون‌ها که می‌خورانیم، از تو بدین خوشم</p></div>
<div class="m2"><p>گویی به کام من شکر و شیر می‌کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب گوییا به خواب لبم بر دهان تست</p></div>
<div class="m2"><p>این خواب را بگو که چه تعبیر می‌کنی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من از غمت خمیده، تو گویی جوان شدی</p></div>
<div class="m2"><p>خوش خنده‌ای‌ست اینکه به تدبیر می‌کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی بلا رسد که به خواریت می‌کشد</p></div>
<div class="m2"><p>جان عزیز من، تو چه تقصیر می‌کنی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هردم مگو، ز یاری خسرو مراست شک</p></div>
<div class="m2"><p>زیرا سخن مخالف تقدیر می‌کنی</p></div></div>