---
title: >-
    شمارهٔ ۷۵۵
---
# شمارهٔ ۷۵۵

<div class="b" id="bn1"><div class="m1"><p>سرو در باغ اگر همچو تو موزون خیزد</p></div>
<div class="m2"><p>ای بسا ناله که از بلبل مفتون خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیک بختی که تواند به تو دیدن هر روز</p></div>
<div class="m2"><p>شادمان خسپد و بر طالع میمون خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساکنان سر کوی تو نباشند به هوش</p></div>
<div class="m2"><p>کان زمینی ست که از وی همه مجنون خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیک خواهان به سر پند و من بدخو را</p></div>
<div class="m2"><p>هر دم اندیشه و سودای دگرگون خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبرم از روی نگارین تو فرماید خلق</p></div>
<div class="m2"><p>وه که این کار ز دست چو منی چون خیزد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوز عشقم چو ز دل خواست، بگفتم به طبیب</p></div>
<div class="m2"><p>گفت این علت از آنهاست که از خون خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشک خسرو همه خون است و حذر زین دریا</p></div>
<div class="m2"><p>کاین نه موجی ست که از دجله و جیحون خیزد</p></div></div>