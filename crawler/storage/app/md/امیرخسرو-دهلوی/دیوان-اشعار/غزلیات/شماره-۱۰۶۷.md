---
title: >-
    شمارهٔ ۱۰۶۷
---
# شمارهٔ ۱۰۶۷

<div class="b" id="bn1"><div class="m1"><p>مهری که بود با منت، آن گوییا نبود</p></div>
<div class="m2"><p>آن پرسش زمان به زمان گوییا نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نامم که برده ای و نشانم که داده ای</p></div>
<div class="m2"><p>زان روزگار نام و نشان گوییا نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گلشنی که با گل و مل بوده ایم خوش</p></div>
<div class="m2"><p>آمد خزان و بویی ازان گوییا نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اول که دیدمت ز سیه رویی آن نفس</p></div>
<div class="m2"><p>گوییا نشستم دل و جان گوییا نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یادی مکن به مردمی از بنده، پیش ازان</p></div>
<div class="m2"><p>گویند مردمان که فلان گوییا نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دی ناگهانش دیدم و رفتم که بنگرم</p></div>
<div class="m2"><p>در پیش دیده نگران گوییا نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد قصه داشت خسرو مسکین ز درد خویش</p></div>
<div class="m2"><p>چون پیش او رسید زبان گوییا نبود</p></div></div>