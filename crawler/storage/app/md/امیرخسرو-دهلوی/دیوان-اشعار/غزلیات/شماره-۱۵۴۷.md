---
title: >-
    شمارهٔ ۱۵۴۷
---
# شمارهٔ ۱۵۴۷

<div class="b" id="bn1"><div class="m1"><p>گر چه ز خوی نازکت سوخته گشت جان من</p></div>
<div class="m2"><p>سوی تو می کشد هنوز این دل ناتوان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواب نماند خلق را در همه شهر از غمت</p></div>
<div class="m2"><p>دور شنیده می شود در دل شب فغان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ غبارت از درون می نپذیر دم سکون</p></div>
<div class="m2"><p>گر چه شد آب جمله خون در تن ناتوان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وه که ز جور چون تویی نام غبار بر زبان</p></div>
<div class="m2"><p>نیست کسی که بفگند خاک بر این دهان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر دهیم به جان امان، نزل ره تو عمر من</p></div>
<div class="m2"><p>ور کشیم به رایگان گرد سر تو جان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتیم از چه ناخوشی، رنج تو چیست، بازگو؟</p></div>
<div class="m2"><p>دوری دوستان و بس، دور ز دوستان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس که توشوخ و دلبری، گم شود ار دل کسی</p></div>
<div class="m2"><p>گر چه که دیگری برد، بر تو بود گمان من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دور مکن ز دامنش گرد من، ای صبا، از آنک</p></div>
<div class="m2"><p>در ره او از این هوس خاک شد استخوان من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خون دل من آب شد از پی روی شستنش</p></div>
<div class="m2"><p>خواب نمی رود هنوز از پی این جوان من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خشم کنان بیا که ما صلح کنیم یکدگر</p></div>
<div class="m2"><p>جان و دل من آن تو، رنج و غم تو آن من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دوش ز آه دل مرا سوخته بود لب، ولی</p></div>
<div class="m2"><p>بخت من آنک نام شه بود برین زبان من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاه جهان جلال دین، آنک به یک اشارتش</p></div>
<div class="m2"><p>دولت بیکرانه شد محنت بی کران من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگذرد و نیوفتد هیچ به خسروش نظر</p></div>
<div class="m2"><p>پیک شتاب می رود ترک سبک عنان من</p></div></div>