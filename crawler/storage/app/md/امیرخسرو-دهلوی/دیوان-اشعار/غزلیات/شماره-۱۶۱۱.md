---
title: >-
    شمارهٔ ۱۶۱۱
---
# شمارهٔ ۱۶۱۱

<div class="b" id="bn1"><div class="m1"><p>گر ز شوخی نیستت پروای من</p></div>
<div class="m2"><p>رحمتی بر چشم خون پالای من!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناگهان گر گشت کویت می کنم</p></div>
<div class="m2"><p>چشم من در غیرت است از پای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من چو جان بدهم، سگ خود را مگوی</p></div>
<div class="m2"><p>تا نگهدارد به کویت جای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دلم گر کرته تنگ آمد ترا</p></div>
<div class="m2"><p>خود قبا کن این دل یکتای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوزش من از چراغ خانه پرس</p></div>
<div class="m2"><p>کوست سوزان هر دم از سودای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سنگهایی کان به کویت می خورم</p></div>
<div class="m2"><p>گو گوارا باد بر رسوای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان خسرو در دو چشمت یک نظر</p></div>
<div class="m2"><p>گر چه سرزد این قدر کالای من</p></div></div>