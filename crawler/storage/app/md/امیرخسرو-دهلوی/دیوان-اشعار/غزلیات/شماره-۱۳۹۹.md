---
title: >-
    شمارهٔ ۱۳۹۹
---
# شمارهٔ ۱۳۹۹

<div class="b" id="bn1"><div class="m1"><p>رحمی که بر در تو غریب اوفتاده‌ام</p></div>
<div class="m2"><p>در خون دل ز دست تو چون جام باده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی باد صبح بوی تو آورد سوی من</p></div>
<div class="m2"><p>امروز دل به سوی تو بر باد داده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بهر نیم‌بوسه که بر پای تو دهم</p></div>
<div class="m2"><p>یارب که چند بار به پایت فتاده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخر چه شد که چشم ببستی به روی من</p></div>
<div class="m2"><p>زین سان که من به روی تو ابرو گشاده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن روز نیست کز تو نمی‌زایدم غمی</p></div>
<div class="m2"><p>غم نیست، چون من از پی این روز زاده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی «دل شکسته بنه بر دو زلف من»</p></div>
<div class="m2"><p>من خود شکسته‌وار بر این دل نهاده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رو بر مراد خسرو دل‌خسته یک دمی</p></div>
<div class="m2"><p>تا چند گوییم که ببین ایستاده‌ام!</p></div></div>