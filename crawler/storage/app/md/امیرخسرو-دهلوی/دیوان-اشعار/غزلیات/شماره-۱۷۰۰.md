---
title: >-
    شمارهٔ ۱۷۰۰
---
# شمارهٔ ۱۷۰۰

<div class="b" id="bn1"><div class="m1"><p>من ار چه هر شب از شبهای هجرش می کنم ناله</p></div>
<div class="m2"><p>ز آه من مبادا بر لبش آزار تبخاله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا از ناله خود صد خراش است و یکی راحت</p></div>
<div class="m2"><p>که می بشناسد آن سلطان سگان خویش را ناله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گذشت از حد درازی شبم ترسم که ناگاهان</p></div>
<div class="m2"><p>شود شبهای بی پایان در این یک روز صد ساله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببینم در رخت گر ره بود در آتش و تیغم</p></div>
<div class="m2"><p>دوم ز انسان که گویی می روم بر سوسن و لاله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه خوش جان دادنی باشد که من از تلخی مردن</p></div>
<div class="m2"><p>تو بخشی از لب خویش آخرش شربت در آن حاله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرم چون خاک زیر پای توسن پی سپر سازی</p></div>
<div class="m2"><p>همت نگذارم و گردی شوم، آیم ز دنباله</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فراقت کشت خسرو را که ترسیدی ز روز بد</p></div>
<div class="m2"><p>ملخ زد کشت دهقان را که می ترسید از ژاله</p></div></div>