---
title: >-
    شمارهٔ ۱۴۳۲
---
# شمارهٔ ۱۴۳۲

<div class="b" id="bn1"><div class="m1"><p>غمم بکشت که از یار مانده ام، چه کنم؟</p></div>
<div class="m2"><p>به دست هجر گرفتار مانده ام، چه کنم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نماند طاقت زاری و ناله ام، آن شوخ</p></div>
<div class="m2"><p>نمی رود ز دل زار، مانده ام، چه کنم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برون دهم غم هجران و باورم نکند</p></div>
<div class="m2"><p>اسیر صحبت اغیار مانده ام، چه کنم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدم ز یار و ز خویش و ز جان و دل بیزار</p></div>
<div class="m2"><p>که هم ز خویش و هم از یار مانده ام، چه کنم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی کشند که منگر به روی خوب چو ما</p></div>
<div class="m2"><p>به عالم از پی این کار مانده ام، چه کنم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی کنند ملامت که چند گریه خون</p></div>
<div class="m2"><p>ز زخم غمزه دل افگار مانده ام، چه کنم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رقیب گفت که «مخمور از چه ای، خسرو؟»</p></div>
<div class="m2"><p>بسی شب است که بیدار مانده ام، چه کنم؟</p></div></div>