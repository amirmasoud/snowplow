---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>نسیما، آن گل شبگیر چونست</p></div>
<div class="m2"><p>چسانش بینم و تدبیر چونست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگویی این چنین بهر دل من</p></div>
<div class="m2"><p>که آن بالای همچون تیر چونست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز لب، آید همی بوی شرابش</p></div>
<div class="m2"><p>دهانش داد بوی شیر چونست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من از وی نیم کشت غمزه گشتم</p></div>
<div class="m2"><p>هنوزم تا به سر تقدیر چونست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چشمش به کشتن کرد تقصیر</p></div>
<div class="m2"><p>لبش در عذر آن تقصیر چونست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نپرسد هرگز آن مست جوانی</p></div>
<div class="m2"><p>که حال توبه آن پیر چونست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به گاه خفتن تشویش عشاق</p></div>
<div class="m2"><p>ز آه و ناله شبگیر چونست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز زلفش سوخت جان خسرو، آری</p></div>
<div class="m2"><p>بگو، آن دام مردم گیر چونست</p></div></div>