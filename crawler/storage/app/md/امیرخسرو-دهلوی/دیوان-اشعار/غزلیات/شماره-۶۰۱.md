---
title: >-
    شمارهٔ ۶۰۱
---
# شمارهٔ ۶۰۱

<div class="b" id="bn1"><div class="m1"><p>آن کیست که از خدا نترسد؟</p></div>
<div class="m2"><p>وز شست ید قضا نترسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرعون چو دید دست موسی</p></div>
<div class="m2"><p>کور است که از عصا نترسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن را که چو مصطفی دلیل است</p></div>
<div class="m2"><p>در قافله از بلا نترسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یوسف به دو کون می فروشند</p></div>
<div class="m2"><p>کو مرد که از بها نترسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورشید که چتر دار شاه است</p></div>
<div class="m2"><p>از سایه هر گدا نترسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش همگی گل است و ریحان</p></div>
<div class="m2"><p>آن را که جز از خدا نترسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو به طواف کوی جانان</p></div>
<div class="m2"><p>گر سر برود، ز پا نترسد</p></div></div>