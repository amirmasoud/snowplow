---
title: >-
    شمارهٔ ۱۵۲۳
---
# شمارهٔ ۱۵۲۳

<div class="b" id="bn1"><div class="m1"><p>خوش آمد با توام دیدار کردن</p></div>
<div class="m2"><p>نظر در روی چون گلنار کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشیدن باده بر روی تو، وانگاه</p></div>
<div class="m2"><p>تماشای گل و گلزار کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه خوش باشد ترا از خواب مستی،</p></div>
<div class="m2"><p>به زخم بوسه ها بیدار کردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز من در پیش تو کاری نیابد</p></div>
<div class="m2"><p>به جز نظاره دیدار کردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیارم از لبت دل را جدا کرد</p></div>
<div class="m2"><p>که نتوان خون ز خون بیزار کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جرم عشق اگر خونم بریزند</p></div>
<div class="m2"><p>نخواهم هرگز استغفار کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به شمشیری نگردم منکر از عشق</p></div>
<div class="m2"><p>ز تو کشتن، ز من اقرار کردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگو خسرو که اینها گفتنی نیست</p></div>
<div class="m2"><p>نمی شاید سخن بسیار کردن</p></div></div>