---
title: >-
    شمارهٔ ۱۰۷۴
---
# شمارهٔ ۱۰۷۴

<div class="b" id="bn1"><div class="m1"><p>ای دل، ز بتان دو دیده برگیر</p></div>
<div class="m2"><p>اندیشه ز عالم دگر گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا شحنه غم ترا درین راه</p></div>
<div class="m2"><p>سر بر نگرفت، پای برگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شور و شر بیخودیست اینجا</p></div>
<div class="m2"><p>با خود شو و ترک شور و شر گیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی نی غلطم که چون اسیران</p></div>
<div class="m2"><p>دنباله جعدهای ترگیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر درد سریت هست از عشق</p></div>
<div class="m2"><p>با درد بساز و ترک سر گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرباز مکش ز پای خوبان</p></div>
<div class="m2"><p>گر بی سپر است، بی سپر گیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاکی که بر او بتی گذشته ست</p></div>
<div class="m2"><p>از مردم دیده در گهر گیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاری که بر او گلی نشسته ست</p></div>
<div class="m2"><p>در دیده میل سرمه برگیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور عقل رهت زند به کویش</p></div>
<div class="m2"><p>ترک من مست بی خبر گیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خسرو، بنشین و دختر رز</p></div>
<div class="m2"><p>با خوش پسران سیمبر گیر</p></div></div>