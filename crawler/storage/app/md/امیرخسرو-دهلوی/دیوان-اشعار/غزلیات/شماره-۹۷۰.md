---
title: >-
    شمارهٔ ۹۷۰
---
# شمارهٔ ۹۷۰

<div class="b" id="bn1"><div class="m1"><p>هر که بر گفته تو گوش نهاد</p></div>
<div class="m2"><p>ز آتش دل به سینه جوش نهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رویت از زلف عنبرین مه را</p></div>
<div class="m2"><p>حلقه بندگی به گوش نهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو ثابت قدم به پیش قدت</p></div>
<div class="m2"><p>نتواند که پا به هوش نهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلق را لعلت از شکر بکشد</p></div>
<div class="m2"><p>خونبها بر شکر فروش نهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیش زنبور غمزه تو خورد</p></div>
<div class="m2"><p>از لبت هر که دل به نوش نهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد خیال تو راست با خسرو</p></div>
<div class="m2"><p>روزی از کج نهد، به دوش نهد</p></div></div>