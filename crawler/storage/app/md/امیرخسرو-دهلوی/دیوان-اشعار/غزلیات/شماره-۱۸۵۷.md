---
title: >-
    شمارهٔ ۱۸۵۷
---
# شمارهٔ ۱۸۵۷

<div class="b" id="bn1"><div class="m1"><p>بی تو، ای بی تو به جان آمده جانم، چونی؟</p></div>
<div class="m2"><p>کز پی کاهش من روز به روز افزونی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش از این گر چه جفاهات بسی بود، ولی</p></div>
<div class="m2"><p>نه چنین بود از این بیشتری کاکنونی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان همی خواستی از من که به افسون ببری</p></div>
<div class="m2"><p>جان من رفت و تو هم بر سر آن افسونی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند گویی که چه حال است دل تنگ ترا</p></div>
<div class="m2"><p>آن چنان است که تو از دل من بیرونی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حال خونابه خسرو دل خسرو داند</p></div>
<div class="m2"><p>تو چه دانی که نه در آب و نه اندر خونی</p></div></div>