---
title: >-
    شمارهٔ ۱۸۳۷
---
# شمارهٔ ۱۸۳۷

<div class="b" id="bn1"><div class="m1"><p>باز بهر جان ما را ناز در سر می‌کنی</p></div>
<div class="m2"><p>دیده بیننده را هردم به خون تر می‌کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چو مویم می‌کنی، بهر عدم هم دولت است</p></div>
<div class="m2"><p>زانکه ره دورست و بار من سبک‌تر می‌کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتابی تو، ولی زآنجا که روز چون منی‌ست</p></div>
<div class="m2"><p>کی سر اندر خانه تاریک من در می‌کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی از دل دور کن جان را و هم با من بساز</p></div>
<div class="m2"><p>شرم بادت خویش را با جان برابر می‌کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌کنی آن خنده‌ای تا ریش من بهتر شود</p></div>
<div class="m2"><p>باز خنده می‌زنی و آزار دیگر می‌کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بت بدکیش، چشم نامسلمان را بپوش</p></div>
<div class="m2"><p>در مسلمانی چرا تاراج کافر می‌کنی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر زمان گویی که حال خویش پیش من بگوی</p></div>
<div class="m2"><p>آری آری، گفت خسرو نیک باور می‌کنی</p></div></div>