---
title: >-
    شمارهٔ ۱۲۷۵
---
# شمارهٔ ۱۲۷۵

<div class="b" id="bn1"><div class="m1"><p>زین پای ادب نیست که در کوی تو آیم</p></div>
<div class="m2"><p>سازم ز دو دیده قدم و سوی تو آیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کاش شوم زودتری خاک که باری</p></div>
<div class="m2"><p>با باد شرم همره و پهلوی تو آیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کوی تو گمره شوم از بوی تو با آنک</p></div>
<div class="m2"><p>آنجا همه زان رهبری بوی تو آیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشیدی و من ذره، کنم بی سر و پا رقص</p></div>
<div class="m2"><p>آن لحظه که در جلوه گه روی تو آیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی که سیاست کنمت، کی بود آن تا</p></div>
<div class="m2"><p>گل بسته و آراسته در کوی تو آیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی که برو جان ببر از من، چه روم چون</p></div>
<div class="m2"><p>هر جا که روم بسته به یک موی تو آیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرسی غم خسرو ز پی شرح، زبان کو</p></div>
<div class="m2"><p>چون پیش نمکدان سخنگوی تو آیم</p></div></div>