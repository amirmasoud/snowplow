---
title: >-
    شمارهٔ ۱۸۲۵
---
# شمارهٔ ۱۸۲۵

<div class="b" id="bn1"><div class="m1"><p>ای چهره زیبای تو رشک بتان آزری</p></div>
<div class="m2"><p>هر چند وصفت می کنم، در حسن از آن زیباتری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز نیاید در نظر نقشی ز رویت خوبتر</p></div>
<div class="m2"><p>شمسی ندانم یا قمر، حوری ندانم یا پری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفاق را گردیده ام، مهر بتان ورزیده ام</p></div>
<div class="m2"><p>بسیار خوبان دیده ام، اما تو چیز دیگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای راحت و آرام جان، با قد چون سرو روان</p></div>
<div class="m2"><p>زینسان مرو دامن کشان، کآرام جانم می بری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عزم تماشا کرده ای، آهنگ صحرا کرده ای</p></div>
<div class="m2"><p>جان و دل ما برده ای، این است رسم دلبری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالم همه یغمای تو، خلقی همه شیدای تو</p></div>
<div class="m2"><p>آن نرگس رعنای تو آورده کیش کافری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو غریب است و گدا، افتاده در شهر شما</p></div>
<div class="m2"><p>باشد که از بهر خدا سوی غریبان بنگری</p></div></div>