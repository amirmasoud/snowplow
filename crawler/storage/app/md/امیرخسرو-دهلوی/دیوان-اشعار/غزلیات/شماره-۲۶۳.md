---
title: >-
    شمارهٔ ۲۶۳
---
# شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>رنگی از حسن تو در روی گل است</p></div>
<div class="m2"><p>وز لب لعلت خیالی در مل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خیال نرگس جادوی تو</p></div>
<div class="m2"><p>در چمن ها چشم نرگس بر گل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نسیم صبح کی بیرون رود</p></div>
<div class="m2"><p>بوی گل کاندر دماغ بلبل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از کمند عنبرین گیسوی تو</p></div>
<div class="m2"><p>ملتهب کی دل شود، گر دلدل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رحم کن بر خسرو، ار بشنیده ای</p></div>
<div class="m2"><p>کز فغانش عالمی در غلغل است</p></div></div>