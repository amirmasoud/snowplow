---
title: >-
    شمارهٔ ۷۳۷
---
# شمارهٔ ۷۳۷

<div class="b" id="bn1"><div class="m1"><p>عمر نو گشت مرا باز که جان باز آمد</p></div>
<div class="m2"><p>وز پس عمری آن جان جهان باز آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ره ده، ای دیده و خار مژه را یک سو کن</p></div>
<div class="m2"><p>که خرامان و خوش آن سرو روان باز آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان من چشم از آنگه که به روی تو فتاد</p></div>
<div class="m2"><p>جز تو در غیر توان دید؟ از آن باز آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز نامد دل من، گر چه به کویت صدبار</p></div>
<div class="m2"><p>شادمان رفت و به فریاد و فغان باز آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کسم گویم باز آی ازان تا برهی</p></div>
<div class="m2"><p>گر دل این است که دارم نتوان باز آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنده خسرو که ز تو دیده بپوشید و برفت</p></div>
<div class="m2"><p>چون میسر نشدش، ناله کنان باز آمد</p></div></div>