---
title: >-
    شمارهٔ ۱۷۵۸
---
# شمارهٔ ۱۷۵۸

<div class="b" id="bn1"><div class="m1"><p>ای فراق تو یار دیرینه</p></div>
<div class="m2"><p>غم تو غمگسار دیرینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد تو میهمان هر روزه</p></div>
<div class="m2"><p>داغ تو یادگار دیرینه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غرق خونم که می خلد هر دم</p></div>
<div class="m2"><p>در دلم خار خار دیرینه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کسی را می و یاری ست و من</p></div>
<div class="m2"><p>بی خبر از خمار دیرینه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچگه در حضور خواهم گفت</p></div>
<div class="m2"><p>محنت انتظار دیرینه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دریغا که خاک خواهم شد</p></div>
<div class="m2"><p>با دل پر غبار دیرینه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای صبا، زینهار یاد دهش</p></div>
<div class="m2"><p>گه گه از دوستدار دیرینه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه گاهی خرامشی نکنی</p></div>
<div class="m2"><p>بر سر خاک یار دیرینه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند گاهی خلاص یافته بود</p></div>
<div class="m2"><p>جانم از کار و بار دیرینه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وه که باز آمدی و خسرو را</p></div>
<div class="m2"><p>بردی از دل قرار دیرینه</p></div></div>