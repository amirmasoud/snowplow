---
title: >-
    شمارهٔ ۱۰۴۲
---
# شمارهٔ ۱۰۴۲

<div class="b" id="bn1"><div class="m1"><p>چو باد صبح به آن سرو خوش خرام شود</p></div>
<div class="m2"><p>سلام گویم و جان همره سلام شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غلام اویم و هر کس که بیند آن صورت</p></div>
<div class="m2"><p>ضرورت است که همچو منش غلام شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عنایتی که رهی نیم کشت غمزه تست</p></div>
<div class="m2"><p>به یک اشارت ابروی تو تمام شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جدا کنی تو و من پیش خلق شکر کنم</p></div>
<div class="m2"><p>مرا جمال تو باید که نیک نام شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب و دهان و رخت هر یکی بلای دل اند</p></div>
<div class="m2"><p>یکی دلم چه کند، جانب کدام شود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به چند سوز دل از آه کار پخته کنم</p></div>
<div class="m2"><p>دگر ره از خنکی های بخت خام شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به فتوی خط او کآیتی ست می ترسم</p></div>
<div class="m2"><p>که خواب بر همه کس بعد ازین حرام شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میان غم زدگانم بخوان که پیش ملک</p></div>
<div class="m2"><p>فقیر نیز بگنجد که بار عام شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببرد خواب ز همسایه ناله خسرو</p></div>
<div class="m2"><p>مباد مرغ چمن پای بند دام شود</p></div></div>