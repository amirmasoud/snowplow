---
title: >-
    شمارهٔ ۱۸۵۳
---
# شمارهٔ ۱۸۵۳

<div class="b" id="bn1"><div class="m1"><p>باز، ای سرو خرامان، ز کجا می آیی؟</p></div>
<div class="m2"><p>کز برای دل دیوانه ما می آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کشد هجر و ره آمدنت می طلبم</p></div>
<div class="m2"><p>چیست فرمان تو، جانا، به کجا می آیی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ز جا می روی از خویش نباشد عجبی</p></div>
<div class="m2"><p>عجب این است که چون باز به جا می آیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای خوش آن کشته که شد در ته شمشیر و بزیست</p></div>
<div class="m2"><p>که در آن دم تو به نظاره ما می آیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوزت، ای عشق، همه خرمن جانها سوزد</p></div>
<div class="m2"><p>شرم ناید که بر این برگ گیا می آیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زندگانیت نمی سازد دانم، خسرو</p></div>
<div class="m2"><p>آخر این کوی فلان است که تا می آیی!</p></div></div>