---
title: >-
    شمارهٔ ۱۸۸۰
---
# شمارهٔ ۱۸۸۰

<div class="b" id="bn1"><div class="m1"><p>گر منت می کنم عنان گیری</p></div>
<div class="m2"><p>تا کی از چون منت کران گیری؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر زمان از کرشمه ابرو</p></div>
<div class="m2"><p>بهر خونریز من کمان گیری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل گرفتار تو از آن کردم</p></div>
<div class="m2"><p>که مرا از برای جان گیری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غمزه و چشم تو نکو داند</p></div>
<div class="m2"><p>این زبون کردن، آن زبان گیری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتابی، ولی نخواهم گفت</p></div>
<div class="m2"><p>که تو زان چیزها جهان گیری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بین دهان چو خاتم خود را</p></div>
<div class="m2"><p>تا خود انگشت در دهان گیری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منم و هر دو مردم چشمم</p></div>
<div class="m2"><p>که دو سه بنده رایگان گیری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوسه گفتی و گر لبت گیرم</p></div>
<div class="m2"><p>این نباید، حساب آن گیری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویدت دل که ترک خسرو گیر</p></div>
<div class="m2"><p>ترسم از کودکی همان گیری</p></div></div>