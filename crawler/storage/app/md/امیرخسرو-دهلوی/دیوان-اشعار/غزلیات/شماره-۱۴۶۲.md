---
title: >-
    شمارهٔ ۱۴۶۲
---
# شمارهٔ ۱۴۶۲

<div class="b" id="bn1"><div class="m1"><p>ما که در راه غم قدم زده ایم</p></div>
<div class="m2"><p>بر خط عافیت رقم زده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به طوفان عشق غرقه شدیم</p></div>
<div class="m2"><p>بر سر نه فلک قدم زده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدمی کو به راه عشق شتافت</p></div>
<div class="m2"><p>دیده بر راه آن قدم زده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون که اندر وجود نیست ثبات</p></div>
<div class="m2"><p>دست در نامه عدم زده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آستین بر زد آب دیده به رقص</p></div>
<div class="m2"><p>بس که در سینه ساز غم زده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سر نیستی چو سلطانی</p></div>
<div class="m2"><p>هستی هر دو کون کم زده ایم</p></div></div>