---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>روزگاری شد که دل با داغ هجران خو گرفت</p></div>
<div class="m2"><p>از نصیحت باز کی گردد دلی کان خو گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشکل است آزاد بودن، دل که با دلبر نشست</p></div>
<div class="m2"><p>مردنست، از تن، جدایی دل که با جان خو گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل بیرون شد ز من، پرسیدمش کین چیست، گفت</p></div>
<div class="m2"><p>ما که هوشیاریم با دیوانه نتوان خو گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من شبی چون کوه دارم زین دل کوتاه روز</p></div>
<div class="m2"><p>خرم آن ذره که با خورشید تابان خو گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طاقت رویت ندارم، گرچه می‌دانم از آنک</p></div>
<div class="m2"><p>چشم بی‌اقبال من با پای دربان خو گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طاقت رویت ندارم، گرچه می‌دانم، از آنک</p></div>
<div class="m2"><p>چشم بی‌اقبال من با پای دربان خو گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آگهی کی دارد از اسکندر تشنه‌جگر</p></div>
<div class="m2"><p>خضر تنها خوار کو با آب حیوان خو گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل به زلفت ماند، ازو بوی مسلمانی مجو</p></div>
<div class="m2"><p>زان که عمری رفت کاندر کافرستان خو گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر خیالت مونس دل شد مرا، بازش مدار</p></div>
<div class="m2"><p>هم به من بگذار کین یوسف به زندان خو گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مردمان گویند خسرو چونی از سر کو ب عشق</p></div>
<div class="m2"><p>چون بود، گویی که آن با زخم چوگان خو گرفت</p></div></div>