---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>دل مسکین من در بند مانده ست</p></div>
<div class="m2"><p>اسیر یار شکر خند مانده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نماند اندر دل من درد را جای</p></div>
<div class="m2"><p>مده پندم نه جای پند مانده ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نصیحت گوی من، لختی دعا گوی</p></div>
<div class="m2"><p>که یک بیچاره ای در بند مانده ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جان پیوند کردم عاشقی را</p></div>
<div class="m2"><p>کنون جان رفت و آن پیوند مانده ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من امشب باری از دوری بمردم</p></div>
<div class="m2"><p>هنوز، ای پاسبان، شب چند مانده ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رهاوی ساز کن، ای مطرب صبح</p></div>
<div class="m2"><p>که مطرب هم به زیر افگند مانده ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بتا، از در مران بیچاره ای را</p></div>
<div class="m2"><p>که در کوی تو حاجتمند مانده ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به می سوگند خوردم جرعه ای بخش</p></div>
<div class="m2"><p>که ما را در گلو سوگند مانده ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز غم گفتی که خسرو زنده چون ماند</p></div>
<div class="m2"><p>دروغی گفته و خرسند مانده ست</p></div></div>