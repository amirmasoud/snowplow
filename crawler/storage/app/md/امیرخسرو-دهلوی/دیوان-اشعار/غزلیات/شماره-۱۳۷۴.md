---
title: >-
    شمارهٔ ۱۳۷۴
---
# شمارهٔ ۱۳۷۴

<div class="b" id="bn1"><div class="m1"><p>ای رخت چون ماه و از مه بیش هم</p></div>
<div class="m2"><p>خسته کردی سینه ما، ریش هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمزه تو بر صف خوبان زند</p></div>
<div class="m2"><p>گر نرنجی بر دل درویش هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیره کردی عیش ما و روز دل</p></div>
<div class="m2"><p>روزگار عقل دور اندیش هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نوازش نیست کشتن، گفتمت</p></div>
<div class="m2"><p>کاهلی کردی در آن فرویش هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشتم از دست جفایت خویش را</p></div>
<div class="m2"><p>بر تو آسان کردم و بر خویش هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می رود صبر من آواره ز من</p></div>
<div class="m2"><p>پس نمی بیند ز بینم و پیش هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما و زنار مغانه کز بتان</p></div>
<div class="m2"><p>وین نماز، استغفرالله، کیش هم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه بر جانم قیامتها از اوست</p></div>
<div class="m2"><p>تا قیامت عمر بادش بیش هم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر زمان گویی که نوش من خوش است</p></div>
<div class="m2"><p>گر ز خسرو پرسی، ای جان، نیش هم</p></div></div>