---
title: >-
    شمارهٔ ۱۵۱۵
---
# شمارهٔ ۱۵۱۵

<div class="b" id="bn1"><div class="m1"><p>مخند از درد من، جانا، نه بر بازی ست آه من</p></div>
<div class="m2"><p>درون تا آتشی نبود، نخیزد دود از روزن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جامه گر چه جان پاره کنی، کی باورم داری؟</p></div>
<div class="m2"><p>ترا کاسیب خواری هیچ گه نگرفت در دامن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گناهی جز وفاداری من اندر خود نمی بینم</p></div>
<div class="m2"><p>ندانم تا که فرمودت که دل از دوستان بر کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر از ناز خون ریزی، حلالت کردم، ای بدخو</p></div>
<div class="m2"><p>وگراز دوست جان خواهی، رضایت خواهم، ای دشمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا در باغ می خوانی، مگر آگه نه ای از خود؟</p></div>
<div class="m2"><p>رها کن تا ترا ببینم، چه جای لاله و نسرن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الا، ای ساقی مستان، طفیل جرعه رندان</p></div>
<div class="m2"><p>شرابی گر نمی ارزم، سفالی بر سرم بشکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببر از من همه اسباب هستی جز وفای خود</p></div>
<div class="m2"><p>که آن در خاک خواهد رفت، دور از روی تو با من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رقیبا، گردنت بار گران را بر نمی تابد</p></div>
<div class="m2"><p>تو از خون مسلمانان گرانباری مکن گردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برفت از یاد خسرو زاد و بوم کهنه در کویش</p></div>
<div class="m2"><p>چو مرغی در قفس ماند، فرامش گرددش مسکن</p></div></div>