---
title: >-
    شمارهٔ ۲۶۴
---
# شمارهٔ ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>ای نسیم صبحدم، یارم کجاست</p></div>
<div class="m2"><p>غم ز حد بگذشت، غمخوارم کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواب در چشمم نمی آید به شب</p></div>
<div class="m2"><p>آن چراغ چشم بیدارم کجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوست گفت آشفته گرد و زار باش</p></div>
<div class="m2"><p>دوستان، آشفته و زارم، کجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیستم آسوده و کارش دمی</p></div>
<div class="m2"><p>یار، آن آسوده از کارم، کجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به گوش او رسانم حال خویش</p></div>
<div class="m2"><p>ناله های خسرو زارم کجاست</p></div></div>