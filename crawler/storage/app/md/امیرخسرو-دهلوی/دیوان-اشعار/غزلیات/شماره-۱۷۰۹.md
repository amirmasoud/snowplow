---
title: >-
    شمارهٔ ۱۷۰۹
---
# شمارهٔ ۱۷۰۹

<div class="b" id="bn1"><div class="m1"><p>گشادم دیده روی تو ناگه</p></div>
<div class="m2"><p>به جانم در شدی ناکرده آگه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر گویم که از جورت کنم آه</p></div>
<div class="m2"><p>زنی فی الحال تیغ و گوییم وه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدت شاخ انار و روی تو نار</p></div>
<div class="m2"><p>تعالی الله از آن قد اناره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر پرتو زند خورشید رویت</p></div>
<div class="m2"><p>بسوزد مه درون هفت خرگه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن با چشم خود نرگس مقابل</p></div>
<div class="m2"><p>کسی آیینه ننهد پیش امقه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صفا از روی او برد آینه، به</p></div>
<div class="m2"><p>بنامیزد زهی دخل موجه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگریم هر سحر بر یاد رویت</p></div>
<div class="m2"><p>که باران خوش بود اندر سحرگه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به گفت خسرو ار خط موی معنی</p></div>
<div class="m2"><p>مسلسل کرد اعزالله شانه</p></div></div>