---
title: >-
    شمارهٔ ۱۸۴۴
---
# شمارهٔ ۱۸۴۴

<div class="b" id="bn1"><div class="m1"><p>ز نظر اگر چه دوری، شب و روز در حضوری</p></div>
<div class="m2"><p>ز وصال شربتم ده که بسوختم ز دوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم و شبی و گشتی به خرابه های هجران</p></div>
<div class="m2"><p>که عظیم دور ماندم ز ولایت صبوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو به اختیار خاطر غم عشق برگزیدم</p></div>
<div class="m2"><p>ز جفا هر آنچه آید بکشیم از ضروری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من اگر هلاک گردم، تو چه التفات داری؟</p></div>
<div class="m2"><p>که ز غفلت جوانی به کرشمه غروری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه خیال بر دو چشمم، نه یکی هزار منت</p></div>
<div class="m2"><p>که توام ز دولت او شب و روز در حضوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چمن اینچنین نخندد، تو مگر بهشت و باغی</p></div>
<div class="m2"><p>بشر اینچنین نباشد، تو مگر پری و حوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گذری اگر توانی به بهار عاشقان کن</p></div>
<div class="m2"><p>که ز اشک من به صحرا همه لاله است و سوری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به شب فراق خسرو چو چراغ سوخت آخر</p></div>
<div class="m2"><p>شبش ار چه تیره تر شد، به چراغ از تو نوری</p></div></div>