---
title: >-
    شمارهٔ ۱۸۴۸
---
# شمارهٔ ۱۸۴۸

<div class="b" id="bn1"><div class="m1"><p>عالم آشوب تر از طره طرار خودی</p></div>
<div class="m2"><p>فتنه انگیزتر از غمزه خونخوار خودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای افشرده و زانو زده ای در کاری</p></div>
<div class="m2"><p>دامنت چون بگرفته ست و تو در کار خودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آیت حسنی و پیچیده به طومار دو زلف</p></div>
<div class="m2"><p>پیچ بر پیچ ز نیرنگ به طومار خودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر گرفتار توام، نیست گرفتی بر من</p></div>
<div class="m2"><p>که تو نیز از رسن زلف گرفتار خودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبر من طره طرار تو گر باز دهد!</p></div>
<div class="m2"><p>یا شریک عمل طره طرار خودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوش بوسی بزدم بر لبت، آزرده شدی</p></div>
<div class="m2"><p>باز کن لب، نه اگر بر سر آزار خودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وام بردی دل خسرو به گواهی دو چشم</p></div>
<div class="m2"><p>اینک اینک خط تو، گر نه به اقرار خودی</p></div></div>