---
title: >-
    شمارهٔ ۴۸۱
---
# شمارهٔ ۴۸۱

<div class="b" id="bn1"><div class="m1"><p>چون بهر خرامیدن بارم ز زمین خیزد</p></div>
<div class="m2"><p>بس دشنه که یاران را اندر دل و دین برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر و قد نوخیزش بنشت مرا در دل</p></div>
<div class="m2"><p>نه دل که به جان شیند سروی که چنین خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبها که کنم ناله بر یاد قدش، از من</p></div>
<div class="m2"><p>قامت شنود مؤذن چون بانگ پسین خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویی که صبا دل را برداشت ز جای خود</p></div>
<div class="m2"><p>چون در تگ اسپ خود آن ماه ز زین خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس کز حسد چشمش بیمار شود نرگس</p></div>
<div class="m2"><p>از شاخ عصا سازد، آنگه ز زمین خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تیغ کشد بر من، من سر نکشم از وی</p></div>
<div class="m2"><p>کز من همه مهر آید، وز وی همه کین خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترسان گذرم سویش کز گوشه چشم او</p></div>
<div class="m2"><p>با تیر و کمان ناگه ترکی ز کمین خیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من سوخته عشقم، تو دم دمیم ای دل</p></div>
<div class="m2"><p>این سوخته را آتش آخر هم ازین خیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر لعل لبش یابد زان گونه گزد خسرو</p></div>
<div class="m2"><p>کز کار بر آن خاتم صد نقش نگین خیزد</p></div></div>