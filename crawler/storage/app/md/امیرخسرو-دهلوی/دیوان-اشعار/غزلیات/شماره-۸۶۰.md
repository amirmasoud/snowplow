---
title: >-
    شمارهٔ ۸۶۰
---
# شمارهٔ ۸۶۰

<div class="b" id="bn1"><div class="m1"><p>دل می بری به رفتن و هر کو چنان زود</p></div>
<div class="m2"><p>مردم زمین ز دیده کند تا بدان رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هنگام باز رفتن تو مردن من است</p></div>
<div class="m2"><p>ناچار مردنی بود آن دم که جان رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر خامشی که روی تو بیند فغان کند</p></div>
<div class="m2"><p>هر گه که پیر سوی تو آید، جوان رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من منت جفای تو بر جان نهم، از آنک</p></div>
<div class="m2"><p>شمشیر دوستان همه بر نیکوان رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوشم که نام تو نبرم، لیک چون کنم؟</p></div>
<div class="m2"><p>چون هر چه در دل است مرا بر زبان رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسان مگیر آه و دم سرد عاشقان</p></div>
<div class="m2"><p>ای دل، مباد بر تو که باد خزان رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فریاد خواسته ست، بگوییش، ای رقیب</p></div>
<div class="m2"><p>تا چند گه ز دیده مردم نهان رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای مه، کجا رسی به رکاب نگار من</p></div>
<div class="m2"><p>گیرم که خود عنان تو بر آسمان رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما را نه بخت یار و نه یار آشنا، دریغ</p></div>
<div class="m2"><p>این عمر بی بدل که همه رایگان رود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خسرو، اگر بتان به قصاصش روان کنند</p></div>
<div class="m2"><p>خوشدل چنان رود که کسی میهمان رود</p></div></div>