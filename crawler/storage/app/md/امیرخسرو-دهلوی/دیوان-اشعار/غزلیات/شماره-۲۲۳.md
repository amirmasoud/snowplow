---
title: >-
    شمارهٔ ۲۲۳
---
# شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>کشته تیغ جفایت دل درویش من است</p></div>
<div class="m2"><p>خسته تیر بلایت جگر ریش من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیک خواهی که کند منع ز عشق تو مرا</p></div>
<div class="m2"><p>منکری دان به حقیقت که بداندیش من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر گروهی بگزیدند به عالم دینی</p></div>
<div class="m2"><p>عاشقی دین من و بی خبری کیش من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبر دارم کم و شوق رخ او از حد بیش</p></div>
<div class="m2"><p>غیر ازین نیست دگر هر چه کم و بیش من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم، از نوش لبت کام که یابد، گفتا</p></div>
<div class="m2"><p>آنکه مجروح تر از غمزه چون نیش من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر دل از ما ببرید و به تو پیوست، چه باک</p></div>
<div class="m2"><p>آشنا با تو و بیگانه ز من، خویش من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان ازین بادیه خسرو، نتوان برد به جهد</p></div>
<div class="m2"><p>آه ازین وادی خونخوار که در پیش من است</p></div></div>