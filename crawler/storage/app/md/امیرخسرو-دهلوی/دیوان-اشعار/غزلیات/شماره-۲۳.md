---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>در خم گیسوی کافر کیش داری تارها</p></div>
<div class="m2"><p>بهر گمره کردن پاکانست این زنارها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرده بردار از رخی کان مایه دیوانگیست</p></div>
<div class="m2"><p>کز دماغ عاقلان بیرون برد پندارها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فتنه و جور است و آفت کار زار حسن تو</p></div>
<div class="m2"><p>حسن را آری بود اینگونه دست افزارها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آشتی ده با لبم لب را که آزارم به کام</p></div>
<div class="m2"><p>کز پس آن آشتی خوش باشد این آزارها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خارخاری در دلست و غنجهای خون بران</p></div>
<div class="m2"><p>چون کنم چون خود جز این گل نشکند زین خارها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست در کوی تو بستانهای غم تا بنگری</p></div>
<div class="m2"><p>سبزه ها کز گریه رسته از ته دیوارها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق کاه و علف دل نیست، بل نقل سگانست</p></div>
<div class="m2"><p>چون دل گاوان که بفروشند در بازارها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناله ای دارم کش از دل گر برآرم بگسلد</p></div>
<div class="m2"><p>باربرداران مهار و بوستان افسارها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتمش جان می کنم خون می خورم بهر تو، گفت</p></div>
<div class="m2"><p>خسروا، مشتاق را جز این نباشد کارها</p></div></div>