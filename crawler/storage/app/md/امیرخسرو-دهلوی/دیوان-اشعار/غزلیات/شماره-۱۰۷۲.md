---
title: >-
    شمارهٔ ۱۰۷۲
---
# شمارهٔ ۱۰۷۲

<div class="b" id="bn1"><div class="m1"><p>مسلمانان، گرفتارم گرفتار</p></div>
<div class="m2"><p>وزین جال دل افگارم گرفتار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظر بر نیکوان چندان نهادم</p></div>
<div class="m2"><p>که شد ناگه دل زارم گرفتار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو خود کردم نظر در روی خوبان</p></div>
<div class="m2"><p>بدین محنت سزاوارم گرفتار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمند گیسو افگنده ست و کرده</p></div>
<div class="m2"><p>یکی خونریز عیارم گرفتار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گسستن را ندارم طاقت ار چه</p></div>
<div class="m2"><p>ز موی او به یک تارم گرفتار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبم را حال کی داند که هرگز</p></div>
<div class="m2"><p>به روز من نشد یارم گرفتار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برو از دیده خسرو که بادا</p></div>
<div class="m2"><p>به آب چشم بیدارم گرفتار</p></div></div>