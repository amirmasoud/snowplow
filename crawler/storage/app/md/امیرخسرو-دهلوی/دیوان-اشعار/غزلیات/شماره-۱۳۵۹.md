---
title: >-
    شمارهٔ ۱۳۵۹
---
# شمارهٔ ۱۳۵۹

<div class="b" id="bn1"><div class="m1"><p>سوی من بین که ز هجرت به گداز آمده‌ام</p></div>
<div class="m2"><p>روی بنمای که پیشت به نیاز آمده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سر زلف درازت کششی داشتمی</p></div>
<div class="m2"><p>زان کشش به شب‌های دراز آمده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تو رفتم، چه کنم صبر چو نتوانستم</p></div>
<div class="m2"><p>اینک آشفته و عاجز شده باز آمده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر در ابروی تو بینم من مدهوش، مرنج</p></div>
<div class="m2"><p>چه کنم، مست به محراب نماز آمده‌ام؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل من جان به تو بخشید و منم پروانه</p></div>
<div class="m2"><p>وز پی سوختن شمع طراز آمده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خسروم، از چو منی دور مکن چشم که من</p></div>
<div class="m2"><p>خاک درگاه شه بنده‌نواز آمده‌ام</p></div></div>