---
title: >-
    شمارهٔ ۱۵۰۳
---
# شمارهٔ ۱۵۰۳

<div class="b" id="bn1"><div class="m1"><p>از پس عمر شبی هم نفس یار شدم</p></div>
<div class="m2"><p>خواب بود آن همه گویی تو، چو بیدار شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقتی آن چشمه خورشید بدین سوی نتافت</p></div>
<div class="m2"><p>گر چه در کوی غمش سایه دیوار شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موی گشتم ز غم و بار اجل می بندم</p></div>
<div class="m2"><p>ره دراز است، نکو شد که سکیار شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توبه ام بود ز شاهد، کنون ای زاهد، دور</p></div>
<div class="m2"><p>که دگر بار ز سر بر سر این کار شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طوف کوی تو همه از سر من بیرون رفت</p></div>
<div class="m2"><p>آنکه که گه در چمن و گاه به گلزار شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سگان سر کوی تو مرا شرم گرفت</p></div>
<div class="m2"><p>بس که در گرد سر کوی تو بسیار شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفت شبها و مرا صبح مرادی ندمید</p></div>
<div class="m2"><p>نه ز چشمت به حد زیستن افگار شدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسروم، بر سر هر کو شده رسوای جهان</p></div>
<div class="m2"><p>طرفه کاندوه ترا محرم اسرار شدم</p></div></div>