---
title: >-
    شمارهٔ ۳۹۷
---
# شمارهٔ ۳۹۷

<div class="b" id="bn1"><div class="m1"><p>رخت کز آتش تبها به تاب در عرق است</p></div>
<div class="m2"><p>چو نیک می نگرم آفتاب در عرق است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خونش گر شد آن روی و در عرق افتاد</p></div>
<div class="m2"><p>همیشه شمع منور به تاب در عرق است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گرد عارض و روی تو خط خوی آلود</p></div>
<div class="m2"><p>محقق است که چون مشک ناب در عرق است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو عکس روی خوی آلوده اش به جام افتاد</p></div>
<div class="m2"><p>قدح چو با دل پر خون شراب در عرق است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز رشک آنکه عرق بر رخش چرا غلتید</p></div>
<div class="m2"><p>سرشک دیده ما چون حباب در عرق است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز غیرت این تن خسرو چو در تب و سوز است</p></div>
<div class="m2"><p>دلش بر آتش غم چون کباب در عرق است</p></div></div>