---
title: >-
    شمارهٔ ۱۲۱۳
---
# شمارهٔ ۱۲۱۳

<div class="b" id="bn1"><div class="m1"><p>دی مست می رفتی، بتا، رو کرده از ما یک طرف</p></div>
<div class="m2"><p>شبدیز را مطلق عنان پیچیده عمدا یک طرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بر رخ زیبای تو افتاده زاهد را نظر</p></div>
<div class="m2"><p>تسبیح زهدش یک طرف، مانده مصلا یک طرف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیری که دی زد بر دلم، پیداست تا غایت به من</p></div>
<div class="m2"><p>پیکان و کلکش یک طرف، سوفار و پرها یک طرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چار حد کوی خود افتاده بینی بنده را</p></div>
<div class="m2"><p>تن یک طرف،جان یک طرف،سریک طرف،پایک طرف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلطان خوبان می رسدهر سو گروه عاشقان</p></div>
<div class="m2"><p>چاووش شه کو تا کند مشتی گدا را یک طرف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوشین شراب لعل او شد مجلس ما بی خبر</p></div>
<div class="m2"><p>ساقی صراحی یک طرف، مستان رسوا یک طرف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان خسرو دل خسته را خون ریختن فرموده است</p></div>
<div class="m2"><p>خلقی به منت یک طرف، آن شوخ تنها یک طرف</p></div></div>