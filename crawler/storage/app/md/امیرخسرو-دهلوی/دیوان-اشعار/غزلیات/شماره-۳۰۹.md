---
title: >-
    شمارهٔ ۳۰۹
---
# شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>از بند زلف غمزدگان را سبب فرست</p></div>
<div class="m2"><p>وز قند لعل دلشدگان را طرب فرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از من به فن لب آمده جانی ربوده ای</p></div>
<div class="m2"><p>یک بوسه نامزد کن و بازم به لب فرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو ماه و من چو تار قصب در غمت ضعیف</p></div>
<div class="m2"><p>ای ماهتاب، نور به تار قصب فرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروز چون به خنده رطب لب گشوده ای</p></div>
<div class="m2"><p>ما را خبر از آن رطب بوالعجب فرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلطانی از پی تو فرستاد جان، تو نیز</p></div>
<div class="m2"><p>از وعده وصال به جانش طرب فرست</p></div></div>