---
title: >-
    شمارهٔ ۱۸۶۹
---
# شمارهٔ ۱۸۶۹

<div class="b" id="bn1"><div class="m1"><p>ای رفته در غریبی، باز آکه عمر و جانی</p></div>
<div class="m2"><p>یا خود چو عمر رفته باز آمدن ندانی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در راه تو بمیرم، گرچه ترا نبینم</p></div>
<div class="m2"><p>باری خلاص یابم از ننگ زندگانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زانجا که رفته ای تو، نفرستی ار سلامی</p></div>
<div class="m2"><p>بر دست باد باری از خاک ره نشانی!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفتی و زآرزویت بر لب رسید جانم</p></div>
<div class="m2"><p>مانا که زنده یابی، باز آاگر توانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ما چو آشنایان برداشتند دل را</p></div>
<div class="m2"><p>ای جان زار مانده، تو هم ببر گرانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای صاحب سلامت، خفته به خواب مستی</p></div>
<div class="m2"><p>تو در شب فراقت احوال من چه دانی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین بخت نابسامان کامی نیافت خسرو</p></div>
<div class="m2"><p>برباد آرزو شد سرمایه جوانی</p></div></div>