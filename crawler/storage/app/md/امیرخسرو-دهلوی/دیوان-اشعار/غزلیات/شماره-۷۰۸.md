---
title: >-
    شمارهٔ ۷۰۸
---
# شمارهٔ ۷۰۸

<div class="b" id="bn1"><div class="m1"><p>شب زیاد تو مرا تا به سحر خواب نبرد</p></div>
<div class="m2"><p>دیده آبی زد و از دیده من تاب نبرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بدین خواب نخفتم که ببینم رویت</p></div>
<div class="m2"><p>ناگهان روی تو دیدم همه شب خواب نبرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می برد آب دو چشمم که خیالی شده ام</p></div>
<div class="m2"><p>خوش خیال تو که از دیده من آب نبرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل سنگین تو وزنم ننهد، وه که کسی</p></div>
<div class="m2"><p>سنگ قلب تو ازین سینه قلاب نبرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نامسلمان دل من در خم ابروی تو مرد</p></div>
<div class="m2"><p>هیچ کس هندوی ما را سوی محراب نبرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین رخ زرد چه پیچم سخنی در زلفت</p></div>
<div class="m2"><p>هیچ کس حاجت زرگر به سر تاب نبرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زخمهایی که ز نوک قلمت بود در او</p></div>
<div class="m2"><p>در دل خویش نگه داشت، به اصحاب نبرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رقعه ای دوش فرستادی و مسکین خسرو</p></div>
<div class="m2"><p>خواند در روشنی آه و به مهتاب نبرد</p></div></div>