---
title: >-
    شمارهٔ ۵۰۲
---
# شمارهٔ ۵۰۲

<div class="b" id="bn1"><div class="m1"><p>روزی مگر این بسته در ما بگشایند</p></div>
<div class="m2"><p>وز لطف من گشمده را راه نمایند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خلق جهان حال من خسته بدانند</p></div>
<div class="m2"><p>از عین تحیر سرانگشت بخایند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمری ست که از جور فلک با غم و دردم</p></div>
<div class="m2"><p>زین بیش مگر درد به دردم بیفزایند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی در بخت من بیچاره ببندند</p></div>
<div class="m2"><p>وقتی ست که از روی ترحم بگشایند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنهار که دل در فلک و دهر نبندی</p></div>
<div class="m2"><p>کایشان ز جهان یکسره بی مهر و وفایند</p></div></div>