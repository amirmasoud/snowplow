---
title: >-
    شمارهٔ ۶۳۳
---
# شمارهٔ ۶۳۳

<div class="b" id="bn1"><div class="m1"><p>ای خوش آن وقتی که ما را دل به جای خویش بود</p></div>
<div class="m2"><p>کام کام خویش بود و رای رای خویش بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هوای نیکوان می بود تا از دست رفت</p></div>
<div class="m2"><p>چون کند مسکین، گرفتار هوای خویش بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلق گوید ترک دل چون کردی، آخر هر چه بود</p></div>
<div class="m2"><p>دیده و دانسته بود و آشنای خویش بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نگهدارم که بی خوبان نبودی یک زمان</p></div>
<div class="m2"><p>حاش لله دل نبوده ست، این بلای خویش بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من به غیبت بد نگویم آن غریب رفته را</p></div>
<div class="m2"><p>زانکه گر بد بود و گر نیکو، برای خویش بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دی مرا در خون بدید و رخ بگردانید و رفت</p></div>
<div class="m2"><p>من چنین دانم، پشیمان از خطای خویش بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای مسلمانان، به جایی کان پسر حاضر بود</p></div>
<div class="m2"><p>کیست باری دل که بتواند به جای خویش بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یار من ار چه بد من بر زبانش می گذشت</p></div>
<div class="m2"><p>لیک می دانم دلش سوی گدای خویش بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از کجا مست آمدی، ای مه، که غارت شد نماز</p></div>
<div class="m2"><p>پارسایی را که مشغول دعای خویش بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنده خسرو جان شیرین در سر و کار تو کرد</p></div>
<div class="m2"><p>کامده پیش بلا مسکین به پای خویش بود</p></div></div>