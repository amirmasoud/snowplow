---
title: >-
    شمارهٔ ۱۱۶۳
---
# شمارهٔ ۱۱۶۳

<div class="b" id="bn1"><div class="m1"><p>صبح دولت می دمد از روی آن خورشیدوش</p></div>
<div class="m2"><p>در چنین فرخ صبوحی، ساقیا، یک جام کش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش ما کی فرو میرد بدین گونه که می</p></div>
<div class="m2"><p>تا خط بغداد دارد ساقی و ما دجله کش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون من از بازوی همت روز را بر شب زنم</p></div>
<div class="m2"><p>در نیارم سر به تاج روم و اکلیل حبش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون مه نخشب بتان خالی نباشند از دروغ</p></div>
<div class="m2"><p>تا نداری استوار از خود درون آری مکش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می که بر ما زهر شد هم تو کنی آب حیات</p></div>
<div class="m2"><p>تا نگیری عیب ما اول بگو یا خود بچش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر لبت گازی زدم، بر دل و دین و خرد</p></div>
<div class="m2"><p>مهره بر چین، چون که نقش کعبتین آمد دوشش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهترین روز مرا روز بدی آمد، از آنک</p></div>
<div class="m2"><p>هست خسرو شیشه و آن سنگدل دیوانه وش</p></div></div>