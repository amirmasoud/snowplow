---
title: >-
    شمارهٔ ۱۳۳۷
---
# شمارهٔ ۱۳۳۷

<div class="b" id="bn1"><div class="m1"><p>نی مجال آن که او را از دل خود برکشم</p></div>
<div class="m2"><p>نی دل خالی که در دل دلبری دیگر کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده را گر حق آن نبود که دید او روی تو</p></div>
<div class="m2"><p>من ز خونهایی کزو خوردم ز چشمش برکشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نترسم زان که در خونابه ماند یار من</p></div>
<div class="m2"><p>برکشم دیده به جای دیده او را در کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در رهی، کو رفت، این سر تا نگردد خاک ره</p></div>
<div class="m2"><p>هم به خاک راه او زان خاک راهش برکشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر خودش خوانم، فضولی بین که می خواهم، به جهد</p></div>
<div class="m2"><p>چشمه خورشید را در جنب نیلوفر کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاقبت روشن شود همسایگان را سوز من</p></div>
<div class="m2"><p>گر چه آه آتشین از خلق پنهان در کشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بر آن سون تواند داشت، خسرو، سالها</p></div>
<div class="m2"><p>گر توانم یک سخن زان لعل جان پرور کشم</p></div></div>