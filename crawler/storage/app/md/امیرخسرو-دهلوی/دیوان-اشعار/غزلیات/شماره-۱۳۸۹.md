---
title: >-
    شمارهٔ ۱۳۸۹
---
# شمارهٔ ۱۳۸۹

<div class="b" id="bn1"><div class="m1"><p>ابر بهار باران، وین چشم خونفشان هم</p></div>
<div class="m2"><p>بلبل به باغ نالان، عاشق به صد فغان هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صحرا و بوستان خوش، وین جان زار مانده</p></div>
<div class="m2"><p>ناسایدی به صحرا، در باغ و بوستان هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز آ که شهر بی تو تاریک و تیره باشد</p></div>
<div class="m2"><p>در شهر بی تو نتوان، والله که در جهان هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نامم نشانه ای شد در تهمت ملامت</p></div>
<div class="m2"><p>ای کاشکی نبودی نام من و نشان هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این است مردن من، ای خیره کش، که هستی</p></div>
<div class="m2"><p>ز آب حیات خوشتر، وز عمر جاودان هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهی به دیده بنشین، خواهی به سینه جا کن</p></div>
<div class="m2"><p>سلطان هر دو ملکی، این زان تست و آن هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی « به حجت خط شد ملک من دل تو»</p></div>
<div class="m2"><p>گر راست پرسی از من، جانان تویی و جان هم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد منت از تو بر من کز دولت جمالت</p></div>
<div class="m2"><p>بدنام شهر گشتم، رسوای مردمان هم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد نرخ بنده خسرو از چشم تو نگاهی</p></div>
<div class="m2"><p>گر این قدر نیرزد، بنده به رایگان هم</p></div></div>