---
title: >-
    شمارهٔ ۴۷۵
---
# شمارهٔ ۴۷۵

<div class="b" id="bn1"><div class="m1"><p>گفتم که ترا آخر دل خانه نمی یابد</p></div>
<div class="m2"><p>گفتا که پی گنجم ویرانه نمی یابد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که بسوزم جان بر آتش روی تو</p></div>
<div class="m2"><p>گفتا که چراغم را پروانه نمی یابد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم که به چشمم شین، یک گوشه دگر مردم</p></div>
<div class="m2"><p>گفتا من تنها را هم خانه نمی یابد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم که شوم محرم در مجلس خاص تو</p></div>
<div class="m2"><p>گفتا که حریف ما دیوانه نمی یابد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که به دام غم هر لحظه مرا مفگن</p></div>
<div class="m2"><p>گفتا که چنین مرغی بی دانه نمی یابد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم که ز عشقم ده پروانه آزادی</p></div>
<div class="m2"><p>گفتا خط عارض بس، پروانه نمی یابد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم که بود مونس در هجر تو خسرو را</p></div>
<div class="m2"><p>گفتا که خیال ما بیگانه نمی یابد</p></div></div>