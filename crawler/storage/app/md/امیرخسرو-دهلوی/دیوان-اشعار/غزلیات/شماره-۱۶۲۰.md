---
title: >-
    شمارهٔ ۱۶۲۰
---
# شمارهٔ ۱۶۲۰

<div class="b" id="bn1"><div class="m1"><p>صبح دمید و روز شد، شمع به گوشه نه کنون</p></div>
<div class="m2"><p>شمع چه، آفتاب هم، چون تو نشسته ای درون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی حسن خود تو شو، ساقی خون خویش من</p></div>
<div class="m2"><p>تو ز پیاله باده خور، من ز دل کباب خون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گریه چشم من نگر،سوز ندارد آبجو</p></div>
<div class="m2"><p>ناله زار من شنو،درد ندارد ارغنون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تو که شمع سینه ای سوخته گشت جان من</p></div>
<div class="m2"><p>جان به چسان برون کشم تا تو روی زدل برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فتوی بت پرستیم داد رخ تو،چون کنم</p></div>
<div class="m2"><p>چون به شریعت غمت مفتی عقل شد برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طره مشک سای تو ظل معطر الصبا</p></div>
<div class="m2"><p>نرگس نیم مست تو باب مهیج الجنون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لاله ستان عاشقان بهر رخ تو خون دل</p></div>
<div class="m2"><p>نوشد و بر همین دهد دیدن روی لاله گون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من زوجود بی خبر خیل خیال در نظر</p></div>
<div class="m2"><p>بهر به خواب در کشم، تشنگیم شود فزون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیشه تیز عشق را تاب کی آرد آدمی؟</p></div>
<div class="m2"><p>گر چه ستون سنگ هست،ورچه که هست بیستون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ساغر آرزوی من، وه که چگونه پر شود؟</p></div>
<div class="m2"><p>چرخ چنین که میدهد دور به کاسه نگون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهد حسود، خسروا، در طلب مراد دل</p></div>
<div class="m2"><p>رام کسی نمی شود بخت به حیله و فسون</p></div></div>