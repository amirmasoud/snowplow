---
title: >-
    شمارهٔ ۶۱۳
---
# شمارهٔ ۶۱۳

<div class="b" id="bn1"><div class="m1"><p>جانم فدای قامتی کافاق را حیران کند</p></div>
<div class="m2"><p>از ناز چون گردد روان، رو در میان جان کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جور و گر رحمت کند من راضیم از جان و دل</p></div>
<div class="m2"><p>بگذار خود کام مرا تا هر چه خواهد آن کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانا، بر آب چشم من خنده به رعنای مزن</p></div>
<div class="m2"><p>هر قطره کز چشمم چکد، صد خانه را ویران کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن نیم جانی که از غمش مانده ست آن هم رفته دان</p></div>
<div class="m2"><p>یک ره به زیر هر دو لب گو خنده پنهان کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من بر درش جان می کنم در آرزوی یک نظر</p></div>
<div class="m2"><p>با آنکه دشوار آیدش کار مرا آسان کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باری طبیب از بهر من زحمت چه می بیند دگر؟</p></div>
<div class="m2"><p>عیسی به جان آید، اگر درد مرا درمان کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای آن که پندم می دهی کز دل برون کن راز را</p></div>
<div class="m2"><p>از دیده فرمانت کشم، گر دل مرا فرمان کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیهوده چندین بتا، خون در مسلمانی مکن</p></div>
<div class="m2"><p>اسلام کی داند کسی کو غارت ایمان کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر خسروا، خون ریزدت پرسش مکن، گردن بنه</p></div>
<div class="m2"><p>کز مصلحت نبود برون هر خون که آن سلطان کند</p></div></div>