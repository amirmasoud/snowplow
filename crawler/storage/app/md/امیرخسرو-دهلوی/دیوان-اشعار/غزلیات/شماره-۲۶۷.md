---
title: >-
    شمارهٔ ۲۶۷
---
# شمارهٔ ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>ای دهانت، چشمه آب حیات</p></div>
<div class="m2"><p>شمع رویت آفتاب کاینات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دلم از شادی وصلت نماند</p></div>
<div class="m2"><p>از کمند غم نمی یابم نجات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گریه را مپسند هر دم تا به کی</p></div>
<div class="m2"><p>پیش چشم از گریه جیحون و فرات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاتش هجرت تن خاکی بسوخت</p></div>
<div class="m2"><p>تا کدامین باد آرد سوی مات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که بی تو زنده ماند مرده به</p></div>
<div class="m2"><p>جز وصالت نیست مقصود از حیات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ندیدی سبزه ای بر آب خضر</p></div>
<div class="m2"><p>گرد آن شکر ببین رسته نبات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بت پرستان گر ز تو آگه شوند</p></div>
<div class="m2"><p>یاد نارند از بتان سومنات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از شراب شب نشینان در خمار</p></div>
<div class="m2"><p>هات کأسا یا حبیبی بالغدات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو ذره در هوای مهر تو</p></div>
<div class="m2"><p>نیست خسرو را دمی صبر و ثبات</p></div></div>