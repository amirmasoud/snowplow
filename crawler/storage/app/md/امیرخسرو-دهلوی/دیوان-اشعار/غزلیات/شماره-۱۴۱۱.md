---
title: >-
    شمارهٔ ۱۴۱۱
---
# شمارهٔ ۱۴۱۱

<div class="b" id="bn1"><div class="m1"><p>امشب من آن نیم که فغان را فرو برم</p></div>
<div class="m2"><p>طوفان کنم ز دیده، جهان را فرو برم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمعی به سینه و نتوانم برون دهم</p></div>
<div class="m2"><p>جان سوخت، چند سوز نهان را فرو برم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشناختم که لذت شمشیر و تیر چیست؟</p></div>
<div class="m2"><p>هر دم ز بس که آه و فغان را فرو برم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خونابه می خورم ز دل آن دولت از کجا؟</p></div>
<div class="m2"><p>کز لعل یار شربت جان را فرو برم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسرت فرو برم، چو به سینه گره شود</p></div>
<div class="m2"><p>آشام خون دل کنم، آن را فرو برم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی سنگ ماند و نی دل سنگین در این خراب</p></div>
<div class="m2"><p>تا طعنه های پیر و جوان را فرو برم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وه گر نمودی، ای اجل، آخر به پای زود</p></div>
<div class="m2"><p>تا من ز خویش نام و نشان را فرو برم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزی به روی ترشی از ابروی تو نرفت</p></div>
<div class="m2"><p>تا کی ز دور آب دهان را فرو برم؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من خسروم، شکر شکن، اما به ذکر دوست</p></div>
<div class="m2"><p>خواهم ز ذوق کام و زبان را فرو برم</p></div></div>