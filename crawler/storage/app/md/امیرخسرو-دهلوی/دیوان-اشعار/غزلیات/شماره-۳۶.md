---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>ای رخ زیبای تو آینه سینه ها</p></div>
<div class="m2"><p>روی ترا در خیال زین نمط آیینه ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمزه مزن کان خیال تا به جگرها نشست</p></div>
<div class="m2"><p>تیغ بلارک دمید وای که بر سر سینه ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاد توام می کند کار جواب هلاک</p></div>
<div class="m2"><p>خواب که بیند گدا حاصل گنجینه ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس که ز رویت نمود خانه مرا پر خیال</p></div>
<div class="m2"><p>مر همه دیوارهاست پیش من آیینه ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبر نمودی مرا از نظری پیش از این</p></div>
<div class="m2"><p>حسن توام توبه داد زان همه پیشینه ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل که ز دعوی صبر لاف همی زد کنون</p></div>
<div class="m2"><p>بین که چه خوش می کشد هجر از و کینه ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شعله دیرینه را داغ ز دل رفته بود</p></div>
<div class="m2"><p>نوپسری تازه کرد آن همه دیرینه ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>توبه شکن صوفیا، خرقه به می شو که هست</p></div>
<div class="m2"><p>بر قصب شاهدان خرقه پشمینه ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چرخ بشد، ساقیا، دوش می با صفا</p></div>
<div class="m2"><p>درد به خسرو رسان، زان همه دوشینه ها</p></div></div>