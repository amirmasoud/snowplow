---
title: >-
    شمارهٔ ۱۸۷۵
---
# شمارهٔ ۱۸۷۵

<div class="b" id="bn1"><div class="m1"><p>ای باد صبحگاه به من نام او بگوی</p></div>
<div class="m2"><p>خوناب غیرتم به لب جام او بگوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان بو که خوش برآیدم امروز پیش او</p></div>
<div class="m2"><p>چیزی دگر مگوی، همین نام او بگوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بستان دعای سوخته ای، وز لبش مرا</p></div>
<div class="m2"><p>آلوده کرشمه دشنام او بگوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار است یا خیال؟ نمی دانم اینقدر</p></div>
<div class="m2"><p>آن کیست در طواف بر آن بام او بگوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبها منم ز غمزه او غرق خون ناب</p></div>
<div class="m2"><p>این ماجرا به نرگس خودکام او بگوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیغام داد کز سر تیغت سر افگنم</p></div>
<div class="m2"><p>حاجت به تیغ نیست، به پیغام او بگوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وامی ست جان خسرو از آن روی همچو مه</p></div>
<div class="m2"><p>گر ممکن است بر رخ گلفام او بگوی</p></div></div>