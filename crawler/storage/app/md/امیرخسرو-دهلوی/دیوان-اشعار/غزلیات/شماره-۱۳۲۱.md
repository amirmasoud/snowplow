---
title: >-
    شمارهٔ ۱۳۲۱
---
# شمارهٔ ۱۳۲۱

<div class="b" id="bn1"><div class="m1"><p>بر در تو ز دشمنان گر چه که صد جفا کشم</p></div>
<div class="m2"><p>دوستیم حرام باد ار ز تو پای واکشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غنچه دل به نازکی بشکندم بسان گل</p></div>
<div class="m2"><p>صبحدمی که ناگهان بوی خوش از صبا گشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طعنه زنی تو از جفا، من نه به ترک از صفا</p></div>
<div class="m2"><p>تحفه پادشاه را پیش در گدا کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرم ز دیده نایدم کو به تو دید، وانگهی</p></div>
<div class="m2"><p>خاک درت گذاشتم، منت توتیا کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشت فراق و کافرم، وه که بیا و زنده کن</p></div>
<div class="m2"><p>پیش چنان لب و دهان منت جان چرا کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر به در تو کرده خون می کنیم، ز در درون</p></div>
<div class="m2"><p>ناشده سر چو خاک راه، از تو چگونه پا کشم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وای که خونم آب شد، چند ز دیده خون خورم</p></div>
<div class="m2"><p>آه که سوخت جان من، چند ز دل بلا کشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر شبم از خیال تو دل ندهد زبان زدن</p></div>
<div class="m2"><p>من به چنین عقوبتی تا به سحر کجا کشم؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخت ستیزه کار من این همه تاخت بر سرم</p></div>
<div class="m2"><p>خسرو مستمند را چند به ماجرا کشم</p></div></div>