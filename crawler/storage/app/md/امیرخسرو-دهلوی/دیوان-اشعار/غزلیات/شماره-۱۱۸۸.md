---
title: >-
    شمارهٔ ۱۱۸۸
---
# شمارهٔ ۱۱۸۸

<div class="b" id="bn1"><div class="m1"><p>شد آن که پای مرا بوسه می زند او باش</p></div>
<div class="m2"><p>بیار باده که گشتم قلندر و قلاش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو تو به رفت سر صوفیی چو من، ای مست</p></div>
<div class="m2"><p>به جرعه تر کن و هم از سفال خم بتراش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا ز مقنع زاهد کنید خرقه زهد</p></div>
<div class="m2"><p>کزین لباس فرو پوشم آن عبادت فاش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم ز عشق تو خشخاش ذره ذره ولی</p></div>
<div class="m2"><p>به مته چند توان سر برید از خشخاش؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدیم ما همه بی پوست، بس که چهره ما</p></div>
<div class="m2"><p>بر آستانه سیمین بران گرفت خراش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بزم آنکه دعایی کنند اهل صفا</p></div>
<div class="m2"><p>زهی سعادت، اگر طعنه ام زنند اوباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر ز خامه کج افتاد نقش ما، چه کنیم؟</p></div>
<div class="m2"><p>چگونه عیب توانیم کرد بر نقاش!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نبود بر در مسجد چو خسروا، بارم</p></div>
<div class="m2"><p>گرو به خانه خمار کردم این تن لاش</p></div></div>