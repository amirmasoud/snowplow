---
title: >-
    شمارهٔ ۱۷۲۷
---
# شمارهٔ ۱۷۲۷

<div class="b" id="bn1"><div class="m1"><p>همه شب رود رهی رو به ره صبا نشسته</p></div>
<div class="m2"><p>همه کس به خواب راحت، من مبتلا نشسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غرضی ورای امکان چه خیال فاسد است این</p></div>
<div class="m2"><p>هوش جمال سلطان به دل گدا نشسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفسی فرو نبردم که نه انده تو خوردم</p></div>
<div class="m2"><p>تو بگو که چون زیم من به در هوا نشسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو در آی و غمزه ای زن که نهند پیش بت سر</p></div>
<div class="m2"><p>به ستانه ای که باشد صف پارسا نشسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببر، ای دل اسیران، به کجا گریزم از تو</p></div>
<div class="m2"><p>به حوالی دو چشمت حشم بلا نشسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه شب صبا به بویت، من سوخته چه گویم؟</p></div>
<div class="m2"><p>که چهاست در دل من ز دم صبا نشسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو ز ناله من از من سزد ار جدا نشینی</p></div>
<div class="m2"><p>که ز دست خویش من هم ز خودم جدا نشسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگرست رسم خوبان که به سر شوند راضی</p></div>
<div class="m2"><p>منم این که اندرین ره به ره رضا نشسته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر کوی تست خسرو شب و روز، چون کنم من</p></div>
<div class="m2"><p>که توام نمی گذاری نفسی به ما نشسته</p></div></div>