---
title: >-
    شمارهٔ ۱۱۳۵
---
# شمارهٔ ۱۱۳۵

<div class="b" id="bn1"><div class="m1"><p>بر جمالت همچنان من عاشق زارم هنوز</p></div>
<div class="m2"><p>ناله ای کز سوز عشقت داشتم دارم هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای طبیب مهربان، چون رنجه فرمودی قدم</p></div>
<div class="m2"><p>از سر بالین من مگذر که بیمارم هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای به قول دشمنان کوشیده در آزار من</p></div>
<div class="m2"><p>دوستم، با من مشو دشمن که من یارم هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرده ام بی یار و پندارم که دارم زندگی</p></div>
<div class="m2"><p>جان من رفته ست و من با خود نمی آرم هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلق گویندم که خسرو، جامه شیخی بپوش</p></div>
<div class="m2"><p>چون بپوشم، کز میان نگشوده زنارم هنوز</p></div></div>