---
title: >-
    شمارهٔ ۹۱۳
---
# شمارهٔ ۹۱۳

<div class="b" id="bn1"><div class="m1"><p>فغان که جان من از عاشقی به جان آمد</p></div>
<div class="m2"><p>ز دست چشم و دل خویش در فغان آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به راه دیدم و گفتم رود به خانه، نرفت</p></div>
<div class="m2"><p>به سویم آمد و اندر میان جان آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندیده بودم و دعوی صبر می کردم</p></div>
<div class="m2"><p>دلم نماند در آن دم که ناگهان آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو دیر زی که مرا جان من بکشت امروز</p></div>
<div class="m2"><p>نظاره تو که چون عمر جاودان آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گردن دگران آمدم شب از کویت</p></div>
<div class="m2"><p>به پای خویش ز کوی تو چون توان آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم تو دوش همی برد جان، به دل شد صلح</p></div>
<div class="m2"><p>دل کسان که خیال تو در میان آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گران نیامده کوه غم تو بر دل من</p></div>
<div class="m2"><p>دمی ز وصل زدم، بر دلت گران آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز ابرویت که به کشتی سرنگون ماند</p></div>
<div class="m2"><p>امید غرق شد و عمر بر کران آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمانده بود ز خسرو اثر که دی ناگاه</p></div>
<div class="m2"><p>تو رخ نمودی و بیچاره زان جهان آمد</p></div></div>