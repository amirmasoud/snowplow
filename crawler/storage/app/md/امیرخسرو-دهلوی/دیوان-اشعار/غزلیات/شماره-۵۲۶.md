---
title: >-
    شمارهٔ ۵۲۶
---
# شمارهٔ ۵۲۶

<div class="b" id="bn1"><div class="m1"><p>جایی گذرت، ای بت چالاک، نیفتد</p></div>
<div class="m2"><p>کز هر طرفی در جگری چاک نیفتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عرصه بستان جهان، سرو قباپوش</p></div>
<div class="m2"><p>خیزد بسی، اما چو تو چالاک نیفتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر در ته پای تو نخواهد که کند فرش</p></div>
<div class="m2"><p>نور مه و خورشید بر افلاک نیفتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون ریز ز عشاق و فگن لعل بساطی</p></div>
<div class="m2"><p>تا سایه بالای تو بر خاک نیفتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر بار میا پیش من خسته بیدل</p></div>
<div class="m2"><p>تا این دل بدبخت به تاباک نیفتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهم که ز سر خیزم و در پای تو افتم</p></div>
<div class="m2"><p>جان باز چو من عاشق بی باک نیفتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای شوخ، مکن لاغ که خوش کرد ترا عشق</p></div>
<div class="m2"><p>شعله ز پی لاغ به خاشاک نیفتد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رحمت مکن، ار گریه کند عاشق بد چشم</p></div>
<div class="m2"><p>کز دیده ناپاک در پاک نیفتد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش می گذری بی خبر از گریه خسرو</p></div>
<div class="m2"><p>هشدار کت آه دل غمناک نیفتد</p></div></div>