---
title: >-
    شمارهٔ ۳۲۴
---
# شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>مرا کرشمه آن ترک گلعذار بکشت</p></div>
<div class="m2"><p>مرا شکنجه آن جعد همچو مار بکشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوار می شد و یک شکل و صد هزار نظر</p></div>
<div class="m2"><p>هم اولین نظرم شکل آن سوار بکشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر که باد صبا برد رخش گلگونش</p></div>
<div class="m2"><p>که جان سوختگان را چراغ وار بکشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طلب که می کند امروز خون من که مرا</p></div>
<div class="m2"><p>کمان عشق به پیکان آبدار بکشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آشکار و نهان چونکه ز آن خویشم دید</p></div>
<div class="m2"><p>نهانیم بر خود خواند و آشکار بکشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار بار، ازان ترک خیره کش، فریاد</p></div>
<div class="m2"><p>که همچو من نه یکی بلکه صد هزار بکشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو ماهیی که در افتد به دام خسرو را</p></div>
<div class="m2"><p>به قید زلف در افگند و زار زار بکشت</p></div></div>