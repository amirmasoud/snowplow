---
title: >-
    شمارهٔ ۳۷۸
---
# شمارهٔ ۳۷۸

<div class="b" id="bn1"><div class="m1"><p>بتی کز ویم رو به دیوانگی ست</p></div>
<div class="m2"><p>اگر جان توان برد فرزانگی ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زدم دی به زنجیر گیسوش دست</p></div>
<div class="m2"><p>مرا گفت، باز این چه دیوانگی ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم برد بر بوسه پروانه وار</p></div>
<div class="m2"><p>ستد جان که این حق پروانگی ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درونم پر از یار گشت و هنوز</p></div>
<div class="m2"><p>ازان سو که یارست بیگانگی ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگارا، خیال ترا مدتی ست</p></div>
<div class="m2"><p>که با مردم دیده همخانگی ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا کشتی آخر تراکس نگفت؟</p></div>
<div class="m2"><p>که بیچاره کشتن نه مردانگی ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد از عشق خال تو خسرو هلاک</p></div>
<div class="m2"><p>چو مرغی که مرگش زبی دانگی ست</p></div></div>