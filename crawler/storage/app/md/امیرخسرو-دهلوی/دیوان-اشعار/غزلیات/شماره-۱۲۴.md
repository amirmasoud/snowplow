---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>ای ترک کمان ابرو، من کشته ابرویت</p></div>
<div class="m2"><p>ملک همه چین و هند، ندهم به یکی مویت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقتی به طفیل گوی بنواز سرم آخر</p></div>
<div class="m2"><p>تا چند به هر زخمی حسرت خورم از کویت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی که بدین سودا غمناک چه می گردی</p></div>
<div class="m2"><p>آواره دلی دارم در حلقه گیسویت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مسجد چه روم چندین، آخر چه نمازست این</p></div>
<div class="m2"><p>رویم به سوی قبله دل جانب ابرویت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبها همه کس خفته جز من که به بیداری</p></div>
<div class="m2"><p>افسانه دل گویم در پیش سگ کویت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه نام گلی گویم، گه نام گلستانی</p></div>
<div class="m2"><p>زینگونه در اندازم هر جا سخن از رویت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوی گل ازین پیشم در باغ نمودی ره</p></div>
<div class="m2"><p>بادی نوزید از تو گمره شدم از بویت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان در طلبت همره تا باز رهد زین غم</p></div>
<div class="m2"><p>فریاد که بادی هم ناید گهی از سویت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش تو بگو کای بت سوزند چو هندویم</p></div>
<div class="m2"><p>بر آینه ریز آنگه خاکستر هندویت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر در خم چوگانت راضی ست بدین خسرو</p></div>
<div class="m2"><p>آن بخت کرا کارد سر در خم بازویت</p></div></div>