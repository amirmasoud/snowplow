---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>افسوس ازین عمر که بر باد هوا رفت</p></div>
<div class="m2"><p>کاری به جهان نی به مراد دل ما رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید من از اوج جوانی چو برآمد</p></div>
<div class="m2"><p>بس ذره سرگشته که بر باد هوا رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم ز در خویش مران، گفت که بگذر</p></div>
<div class="m2"><p>زین کوچه که داند که چو تو چند گدا رفت؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس را چه غم ار رفت دل سوخته من</p></div>
<div class="m2"><p>بوده ست از آن من، اگر رفت مرا رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن صبر که می گفتم من کوه گران سنگ</p></div>
<div class="m2"><p>بادی بوزید از تو ندانم که کجا رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم که زیم بی تو، دوری مکش اکنون</p></div>
<div class="m2"><p>گر از من درویش حدیثی به خطا رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رنجه نشوم گر به جفا سر بریم، ز آنک</p></div>
<div class="m2"><p>بسیار چنین ها به سر اهل وفا رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو دیر بزی کز گل بارانت نشان نیست</p></div>
<div class="m2"><p>هر ذره که از کوی تو با باد صبا رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما را چه حد صبر به هجر تو، چو خسرو</p></div>
<div class="m2"><p>آمد به درت باز به سر آنکه به پا رفت</p></div></div>