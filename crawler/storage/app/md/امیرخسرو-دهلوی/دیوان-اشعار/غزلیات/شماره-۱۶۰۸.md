---
title: >-
    شمارهٔ ۱۶۰۸
---
# شمارهٔ ۱۶۰۸

<div class="b" id="bn1"><div class="m1"><p>چه کنم کز دل من آن صنم آید بیرون</p></div>
<div class="m2"><p>با دل از سلسله خم به خم آید بیرون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر، ای آه درون مانده، دمی بیرون رو</p></div>
<div class="m2"><p>مگر از دل قدری دود غم آید بیرون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مژه تست چو پیکان کج اندر جگرم</p></div>
<div class="m2"><p>بکشم، لیکن با جان بهم آید بیرون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان رود، لیک دم مهر و وفایت گردد</p></div>
<div class="m2"><p>آخر این روز که از سینه ام آید بیرون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من و رسوایی جاوید که عشق تو بلاست</p></div>
<div class="m2"><p>هر که افتاد درین فتنه، کم آید بیرون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر معمای خطت را به خرد برخوانند</p></div>
<div class="m2"><p>قصه بیدلی از هر رقم آید بیرون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنگ را ماند خسرو که زند چون ره عشق</p></div>
<div class="m2"><p>ناله از هر رگ او زیر و بم آید بیرون</p></div></div>