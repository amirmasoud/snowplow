---
title: >-
    شمارهٔ ۱۱۷۵
---
# شمارهٔ ۱۱۷۵

<div class="b" id="bn1"><div class="m1"><p>دیدم چو آفتابی در سایه کلاهش</p></div>
<div class="m2"><p>سایه گرفته مه را زان طره سیاهش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که در کلاهش بر دوختم دو دیده</p></div>
<div class="m2"><p>بادامه ای نشاندم بر پسته کلاهش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او چشم داشت بر من، من زلف او گرفتم</p></div>
<div class="m2"><p>تا بو که زنده مانم زان غمزه در پناهش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل رفت و در زنخدانش آواز دادم او را</p></div>
<div class="m2"><p>گفت اینکم معلق در نیمه راه چاهش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنوشت عارضش خط از بهره عرض خوبی</p></div>
<div class="m2"><p>آنکه به گرد عارض صف می کشد سپاهش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من چشم می نیارم کز وی نگاه دارم</p></div>
<div class="m2"><p>یارب مگر تو داری از چشم من نگاهش!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرد آن گنه که خسرو بخشیده خواست بوسی</p></div>
<div class="m2"><p>بخشید نیست، جانا، گر هست این گناهش</p></div></div>