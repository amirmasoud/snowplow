---
title: >-
    شمارهٔ ۱۲۶۵
---
# شمارهٔ ۱۲۶۵

<div class="b" id="bn1"><div class="m1"><p>آن نرگس پر ناز و جفا را ز که دانیم؟</p></div>
<div class="m2"><p>وان غمزه بی مهر و وفا را ز که دانیم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر یار جفا کرد، گنه بر دل ریش است</p></div>
<div class="m2"><p>ای خلق جفاگوی شما را ز که دانیم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم ز پی کشتن آن زلف تو جنبند</p></div>
<div class="m2"><p>ای خرمن گل باد صبا را ز که دانیم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر شب که بود ماه که بر بام برآید</p></div>
<div class="m2"><p>آن شعمره انگشت نمارا ز که دانیم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی که به دل راز که داری تو، درین دل</p></div>
<div class="m2"><p>آخر خبرت نیست که ما راز که دانیم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیوانگی خسرو از اندیشه شد آخر</p></div>
<div class="m2"><p>آن سلسله زلف دو تا را ز که دانیم؟</p></div></div>