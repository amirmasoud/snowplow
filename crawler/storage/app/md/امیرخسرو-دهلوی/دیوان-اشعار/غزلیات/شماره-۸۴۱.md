---
title: >-
    شمارهٔ ۸۴۱
---
# شمارهٔ ۸۴۱

<div class="b" id="bn1"><div class="m1"><p>رندان پاکباز که از خود بریده‌اند</p></div>
<div class="m2"><p>در هرچه هست حسن دلارام دیده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خودبین نیند، زان همه چون چشم مرده‌اند</p></div>
<div class="m2"><p>روشندلند، از آن همه چون نور دیده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون رهروان ز منزل هستی گذشته‌اند</p></div>
<div class="m2"><p>بی‌خویش رفته‌اند و به مقصد رسیده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آزاد گشته‌اند به کلی ز هردو کون</p></div>
<div class="m2"><p>وز جان و دل غلامی جانان خریده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با غم نشسته‌اند وز شادی گذشته‌اند</p></div>
<div class="m2"><p>از تن رمیده‌اند و به جان آرمیده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از گفتگوی نیک و بد خلق رسته‌اند</p></div>
<div class="m2"><p>تا مرحبایی از لب دلبر شنیده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو، چه گویی از خم ساقی، من کزان</p></div>
<div class="m2"><p>جام از شراب ساقی وحدت کشیده‌اند</p></div></div>