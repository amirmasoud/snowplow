---
title: >-
    شمارهٔ ۱۱۵۷
---
# شمارهٔ ۱۱۵۷

<div class="b" id="bn1"><div class="m1"><p>ما به جان درمانده و دل سوی ما می خواندش</p></div>
<div class="m2"><p>وه که این بر خود نبخشوده کجا می خواندش؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا هوس بد زیستن، دل را همی گفتم مخوان</p></div>
<div class="m2"><p>چون ز جان برخاستم بگذار تا می خواندش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ستاده بهر رفتن دین و دل بیگانه خواه</p></div>
<div class="m2"><p>غیرتی هم نیست کز دست صبا می خواندش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیز، ای ابرو ببر زین دیده آبی و بشوی</p></div>
<div class="m2"><p>پای آن سرو و بگو آنگه که ما می خواندش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردمان را زو بلای دل، مرا تشویش جان</p></div>
<div class="m2"><p>من قیامت خوانم و خلقی بلا می خواندش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم او در جادویی تا خلق دیوانه شوند</p></div>
<div class="m2"><p>خلق دیوانه شده هر دم دعا می خواندش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوانمش در جان و گوید خانه من نیست این</p></div>
<div class="m2"><p>با چنین بیگانگی دل آشنا می خواندش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما و مردن بر درش، مشتاق را با آن چه کار؟</p></div>
<div class="m2"><p>کو همی راند ز پیش خویش یا می خواندش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راست می گویند، باشد کور عاشق، زانکه نیست</p></div>
<div class="m2"><p>خاک پایش، چشم خسرو توتیا می خواندش</p></div></div>