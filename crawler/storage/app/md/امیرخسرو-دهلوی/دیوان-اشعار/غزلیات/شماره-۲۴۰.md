---
title: >-
    شمارهٔ ۲۴۰
---
# شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>هر که راکن مکن هوش و خرد در کارست</p></div>
<div class="m2"><p>مشنو، از وی سخن عشق که او هشیارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که بر جان ننهی منت تیر خوبان</p></div>
<div class="m2"><p>پای ازین دایره گرد آر که ره پر خارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نامه گو باش سیه روی هم از رسوایی</p></div>
<div class="m2"><p>دل کشیدن ز خط خوش پسران دشوارست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای مؤذن که مرا جانب مسجد خوانی</p></div>
<div class="m2"><p>کار خود کن که مرا با می و شاهد کارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تن که بر وی نوزد باد هوایی، مرده ست</p></div>
<div class="m2"><p>دل که در وی نبود زندگیی، مردارست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غازی پیر کند ریش به خون سرخ و منم</p></div>
<div class="m2"><p>مفسد پیر و خضابم می چون گلنارست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پی دارو در دیده کشد خلق شراب</p></div>
<div class="m2"><p>داروی دیده من خاک در خمارست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بت پرستم من گمره که تو زاهد خوانی</p></div>
<div class="m2"><p>وین که تسبیح به دستم نگری زنارست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسروا، در دل افسرده نگیرد غم عشق</p></div>
<div class="m2"><p>هست جایی اثر سوز نمک کافگارست</p></div></div>