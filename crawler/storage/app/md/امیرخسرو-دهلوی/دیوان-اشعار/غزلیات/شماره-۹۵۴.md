---
title: >-
    شمارهٔ ۹۵۴
---
# شمارهٔ ۹۵۴

<div class="b" id="bn1"><div class="m1"><p>تا رخ تو زلف ترا پیش کرد</p></div>
<div class="m2"><p>زلف تو مه را به پس خویش کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم تو دی ملک جهان می گرفت</p></div>
<div class="m2"><p>مست شد آن غمزه و فرویش کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش دهانت نمکی می فشاند</p></div>
<div class="m2"><p>قطره چکید و جگرم ریش کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرد دلم پاره و دانی که کرد</p></div>
<div class="m2"><p>تیر تو، ای کافر بدکیش، کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم تو در خواب شد او را بگوی</p></div>
<div class="m2"><p>در نتوان بر سگ خود پیش کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خامه خسرو نتواند نوشت</p></div>
<div class="m2"><p>آنچه غمت بر من درویش کرد</p></div></div>