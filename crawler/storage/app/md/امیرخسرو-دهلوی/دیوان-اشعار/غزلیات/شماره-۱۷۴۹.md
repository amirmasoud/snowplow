---
title: >-
    شمارهٔ ۱۷۴۹
---
# شمارهٔ ۱۷۴۹

<div class="b" id="bn1"><div class="m1"><p>بیا شبی بر من سرخوش از شراب شده</p></div>
<div class="m2"><p>که بهر نقل تو دارم دلی کباب شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خراب کرده همه عاقلان عالم را</p></div>
<div class="m2"><p>خصلت چو هر سر مه بر سر شراب شده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب است زلف تو یکسو شده ز رخ، می نوش</p></div>
<div class="m2"><p>کنون که ابر گشاده ست و ماهتاب شده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وفا مکن که بود عیب خوبرویان را</p></div>
<div class="m2"><p>که جان دوست گذارند تا خراب شده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهشت روی تو بادا همیشه خوش، هر چند</p></div>
<div class="m2"><p>که هست بهر من آن دوزخ عذاب شده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آب کرده ز سوز آفتاب خود را غرق</p></div>
<div class="m2"><p>رخت چو غرق خوی از تف آفتاب شده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسان طفل کز آواز خوش به خواب شود</p></div>
<div class="m2"><p>ز آه و ناله من بخت من به خراب شده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من از تو باده طلب کرده و تو با دشنام</p></div>
<div class="m2"><p>جواب داده و من مست آن جواب شده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگو که گریه خون نیستش ز دوری من</p></div>
<div class="m2"><p>چنین که از غم تو خون خسرو آب شده</p></div></div>