---
title: >-
    شمارهٔ ۱۷۵۲
---
# شمارهٔ ۱۷۵۲

<div class="b" id="bn1"><div class="m1"><p>مدار جان من از بهر جان ما روزه</p></div>
<div class="m2"><p>از آنکه جانی و جان را دهد عنا روزه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب پر از می و گویی که روزه می دارم</p></div>
<div class="m2"><p>تو خود بگوی که باشد چنین روا روزه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر تو روزه برای خدای می داری</p></div>
<div class="m2"><p>مدار بیش برای خدای را روزه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دیده ساخته ام شربتی، ولی نخوری</p></div>
<div class="m2"><p>اگر به روزه ترا خوش بود، خوشا روزه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک ابرویت نگرم، روزه گیرم از پی وصل</p></div>
<div class="m2"><p>به دیدن مه ابرو کنم قضا روزه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببرد تشنگی خلق را که از لب تو</p></div>
<div class="m2"><p>به آب چشمه حیوان شد آشنا روزه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نوحه کرد لبالب لبان خسرو را</p></div>
<div class="m2"><p>فقاع از آن لب شیرین گشاد تا روزه</p></div></div>