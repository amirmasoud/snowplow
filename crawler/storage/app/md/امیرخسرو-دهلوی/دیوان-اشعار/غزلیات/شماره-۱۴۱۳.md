---
title: >-
    شمارهٔ ۱۴۱۳
---
# شمارهٔ ۱۴۱۳

<div class="b" id="bn1"><div class="m1"><p>فریاد از این جفا که من از یار می کشم</p></div>
<div class="m2"><p>اندک همی شمارم و بسیار می کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکم که کوب می خورم و پست می شوم</p></div>
<div class="m2"><p>مورم که رنج می برم و بار می کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر از جفای او دلم افگار می شود</p></div>
<div class="m2"><p>بازش هم اندرین دل افگار می کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همسایه می بسوزد و فریاد می کند</p></div>
<div class="m2"><p>زان ناله ها که من پس دیوار می کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر یار هم جفا بود، ار گویمش به روی</p></div>
<div class="m2"><p>جوری که من ز یار جفا کار می کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در ذکر او چه منع ز فریاد، آخرش</p></div>
<div class="m2"><p>پیکانست کز جگر، نه ز پا، خار می کشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روشن چو روز گشت در آفاق سوز من</p></div>
<div class="m2"><p>این شعله کز جگر به شب تار می کشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسرو خراب گشته و جان هم شده خراب</p></div>
<div class="m2"><p>کز دیده باده های چو گلنار می کشم</p></div></div>