---
title: >-
    شمارهٔ ۵۳۸
---
# شمارهٔ ۵۳۸

<div class="b" id="bn1"><div class="m1"><p>ز اهل عقل نپسندد خردمند</p></div>
<div class="m2"><p>که دارد رفتنی را پای در بند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نصیب امروز برگیر از متاعی</p></div>
<div class="m2"><p>که فردا گرددش غیری خداوند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لباس زندگی بر خود مکن تنگ</p></div>
<div class="m2"><p>که چون شد پاره، نتوان کرد پیوند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به صورت خوش مشو، از روی معنی</p></div>
<div class="m2"><p>نی خامه نکوتر از نی قند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نصیحت گوهری دان کان نزیبد</p></div>
<div class="m2"><p>مگر در گوش دانا و خردمند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مخور غم بهر فرزندی و مالی</p></div>
<div class="m2"><p>که مالت دین بس است و صبر فرزند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر خواهی نبینی رنج بسیار</p></div>
<div class="m2"><p>به اندک مایه راحت باش خرسند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به رعنایی منه بر خاکیان پای</p></div>
<div class="m2"><p>که ایشان همچو تو بودند یک چند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شنو، ای دوست، پند، اما چو خسرو</p></div>
<div class="m2"><p>مشو کو گوید و خود نشنود پند</p></div></div>