---
title: >-
    شمارهٔ ۱۸۱۴
---
# شمارهٔ ۱۸۱۴

<div class="b" id="bn1"><div class="m1"><p>دیوانه شدم ز یار بدخوی</p></div>
<div class="m2"><p>بیگانه پرست و آشنا روی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بردن عاشقانست خویش</p></div>
<div class="m2"><p>من جان نبرم ازان جفاجوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جعد ترش تن چو مویم</p></div>
<div class="m2"><p>در تافته گشت موی در موی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرسند نشان صبر، گویم</p></div>
<div class="m2"><p>گامی دو سه از عدم بر آن سوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهم به درت روم به صد آه</p></div>
<div class="m2"><p>سوزم سر و پای خود در آن کوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او گر چه به سوز من نبیند</p></div>
<div class="m2"><p>باری رسدش ز داغ من بوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی، به زکات می پرستان</p></div>
<div class="m2"><p>از من به دو جرعه غم فروشوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای دیده، به سوز من ببخشای</p></div>
<div class="m2"><p>کامروز تراست آب در جوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو چو به نیک گویی تست</p></div>
<div class="m2"><p>یاد آر او را به گفت بدگوی</p></div></div>