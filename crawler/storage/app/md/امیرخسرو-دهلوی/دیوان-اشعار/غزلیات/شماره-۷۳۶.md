---
title: >-
    شمارهٔ ۷۳۶
---
# شمارهٔ ۷۳۶

<div class="b" id="bn1"><div class="m1"><p>خشمگین یار مرا دل به رضا باز آمد</p></div>
<div class="m2"><p>گل بد عهد به بستان وفا باز آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن همه مستی و شوخی و بلا انگیزی</p></div>
<div class="m2"><p>باز جان من دلسوخته را باز آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند گاهی دلم از فتنه امان یافته بود</p></div>
<div class="m2"><p>وه که این درد دل رفته کجا باز آمد!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتابی که سیه روی ویم زین دم سرد</p></div>
<div class="m2"><p>قدری نرم شد و بر سر ما باز آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه همواره جفا بود و ستم عادت او</p></div>
<div class="m2"><p>کرد آهنگ وفا و ز جفا باز آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دعا پیش خود آوردمش، اما عجب است</p></div>
<div class="m2"><p>در جهان عمر کسی کی به دعا باز آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون دران کوی روم، خلق برآرد فریاد</p></div>
<div class="m2"><p>کاینک آن شهره انگشت نما باز آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل گمگشته خود جستم و دربانش گفت</p></div>
<div class="m2"><p>که دل رفته درین کوی کرا باز آمد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زاهدا، توبه مفرما ز رخ خوب که من</p></div>
<div class="m2"><p>بت پرستم، نتوانم به خدا باز آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دی ز بوی تو به حیله ز صبا جان بردم</p></div>
<div class="m2"><p>باز آن وقت شد و باد صبا باز آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خسروا، تن به قضا ده که هواهای کهن</p></div>
<div class="m2"><p>تازه شد از سر و ایام بلا باز آمد</p></div></div>