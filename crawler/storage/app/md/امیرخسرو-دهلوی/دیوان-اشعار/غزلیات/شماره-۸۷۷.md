---
title: >-
    شمارهٔ ۸۷۷
---
# شمارهٔ ۸۷۷

<div class="b" id="bn1"><div class="m1"><p>باد صبا ز نافه چینت نمی رسد</p></div>
<div class="m2"><p>بویی به عاشقان غمینت نمی رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک توییم و چشم تو بر ما نمی فتد</p></div>
<div class="m2"><p>ماهی و پرتوی به زمینت نمی رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمعی که آسمان و زمین زو منورند</p></div>
<div class="m2"><p>در روشنی به عکس جبینت نمی رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم که کام دل بستانم ز لعل تو</p></div>
<div class="m2"><p>دستم به پسته شکرینت نمی رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای درج لعل دوست مگر خاتم جمی</p></div>
<div class="m2"><p>زینسان که دست کس به نگینت نمی رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز ترا چنان که تویی کس نشان نداد</p></div>
<div class="m2"><p>پای گمان به حد یقینت نمی رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مفتی، مپوی بر در زندان که امر و نهی</p></div>
<div class="m2"><p>بر عاشقان بی دل و دینت نمی رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با خار ساز، خسرو، اگر گل به دست نیست</p></div>
<div class="m2"><p>کز گلشن زمانه جز اینت نمی رسد</p></div></div>