---
title: >-
    شمارهٔ ۱۶۰
---
# شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>مرا در سر هوای نازنینی ست</p></div>
<div class="m2"><p>کز او تاراج شد هر جا که دینی ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخواهد رفت مهرش از دل من</p></div>
<div class="m2"><p>اگر چه با منش هر لحظه کینی ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پریشان حالت است از یاد زلفش</p></div>
<div class="m2"><p>به گیتی هر کجا خلوت نشینی ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هجوم جان مشتاقان بر آن لب</p></div>
<div class="m2"><p>چو غوغای مگس بر انگبینی ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنم چون خاک شد، رنجه مکن پای</p></div>
<div class="m2"><p>ترا هم زیر پا آخر زمینی ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهار من تویی، زانم چه سود است</p></div>
<div class="m2"><p>که در عالم گلی یا یاسمینی ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل از پیشت سلامت چون توان برد</p></div>
<div class="m2"><p>که در هر گوشه چشمت کمینی ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مجو آخر تو هشیاری ز خسرو</p></div>
<div class="m2"><p>که عشق و عقل را دیرینه کینی ست</p></div></div>