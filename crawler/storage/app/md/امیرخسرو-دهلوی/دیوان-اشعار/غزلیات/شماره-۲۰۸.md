---
title: >-
    شمارهٔ ۲۰۸
---
# شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>تا خیال نقطه خالت سواد چشم ماست</p></div>
<div class="m2"><p>خاک پایت مردم چشم مرا چون تو توتیاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاجت کحل الجواهر نیست آنکس را که نیست</p></div>
<div class="m2"><p>سرمه از گرد ره توسن که نور چشم ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا گل رخسار تو بشکفت در باغ وجود</p></div>
<div class="m2"><p>عشقبازان را چو بلبل کار با برگ و نواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به طاق ابرویت آورده ام روی نیاز</p></div>
<div class="m2"><p>می نپندازم نمازم اندر این قبله رو است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نافه آهوی چینی کو به زلفت دم زند</p></div>
<div class="m2"><p>نیست آهویی مر او را، زانکه در اصلش خطاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جعد مرغولت که در هر بند او صد حلقه است</p></div>
<div class="m2"><p>دام دلهای اسیران گرفتار بلاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که در کوی تو بویی برد، از عالم گذشت</p></div>
<div class="m2"><p>هر که از دردت نصیبی یافت، فارغ از دواست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جام می از دست هشیاران مجلس تیره گشت</p></div>
<div class="m2"><p>مفردی از خود گذشته دردی آشامی کجاست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی رخ و زلف سیاهش از هواداری خویش</p></div>
<div class="m2"><p>خسرو دلخسته را همدم به روز و شب صباست</p></div></div>