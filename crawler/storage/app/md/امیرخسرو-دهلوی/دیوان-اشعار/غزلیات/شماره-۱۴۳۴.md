---
title: >-
    شمارهٔ ۱۴۳۴
---
# شمارهٔ ۱۴۳۴

<div class="b" id="bn1"><div class="m1"><p>گذشت یار و نسازم به خوی او، چه کنم؟</p></div>
<div class="m2"><p>چو صبر نیست ز روی نکوی او، چه کنم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رقیب گویدم، ای خون گرفته، چشم ببند</p></div>
<div class="m2"><p>چو عاشقم من مسکین به روی او، چه کنم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شدم اسیر سمند و خلاص می جویم</p></div>
<div class="m2"><p>ولیک می کشدم دل به سوی او، چه کنم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جوی اوست کنون آب و من چنین تشنه</p></div>
<div class="m2"><p>ولی ز خون من است آب جوی او، چه کنم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روم به باغ بدین بو که خوش شود دل تنگ</p></div>
<div class="m2"><p>به هیچ باغ نیابم چو موی او، چه کنم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه جای آنست که گویندم «آبروی مریز»</p></div>
<div class="m2"><p>بسوخته ست مرا آرزوی او، چه کنم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتادگی خودش عرضه می دهم از پی</p></div>
<div class="m2"><p>فتاده چند براین خاک کوی او، چه کنم؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو شیر خورد همه خون خسرو آن بدخو</p></div>
<div class="m2"><p>ز شیرخوارگی این است خوی او، چه کنم</p></div></div>