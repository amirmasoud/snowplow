---
title: >-
    شمارهٔ ۵۹۵
---
# شمارهٔ ۵۹۵

<div class="b" id="bn1"><div class="m1"><p>چون سرو تو از قبا برآید</p></div>
<div class="m2"><p>آه از من مبتلا برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با یاد خط تو زنده گردم</p></div>
<div class="m2"><p>گر از گل من گیا برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جایی که تو همچو مه برآیی</p></div>
<div class="m2"><p>مه پیش رخت کجا برآید؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مه برنابد برابر تو</p></div>
<div class="m2"><p>گر فرمایی، برابر آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از قبله ابروی تو هر شب</p></div>
<div class="m2"><p>بس دست که در دعا برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش آی که بهر دیدن تو</p></div>
<div class="m2"><p>جان منتظر است تا برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چند در انتظار داریش</p></div>
<div class="m2"><p>می آریی زود یا برآید؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنگم که ز دست تو نفیرم</p></div>
<div class="m2"><p>از هر سو مو جدا برآید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با تو دل من چو برنیاید</p></div>
<div class="m2"><p>بیم است که جان ما برآید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک لحظه به کار او فروشو</p></div>
<div class="m2"><p>تا کام یکی گدا برآید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خسرو که در آب دیده غرق است</p></div>
<div class="m2"><p>بازا آکه به آشنا برآید</p></div></div>