---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>آنجاست دل من و هم آنجاست</p></div>
<div class="m2"><p>کان کج کله بلند بالاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوابش دیدیم دوش و مستیم</p></div>
<div class="m2"><p>کان خواب هنوز در سر ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آهسته رو، ای صبا، بدان بام</p></div>
<div class="m2"><p>کان مست شبانه من آنجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رحمی نکند بر این دل پیر</p></div>
<div class="m2"><p>یاری که چو بخت خویش برناست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دوزخ، اگر نشان بپرسند</p></div>
<div class="m2"><p>من گویم خوابگاه تنهاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می کش که به هر چهار مذهب</p></div>
<div class="m2"><p>خونم هدرست و خانه یغماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتند دلت خوش است، آری</p></div>
<div class="m2"><p>در گونه روی بنده پیداست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خون می کنی و خبر نداری</p></div>
<div class="m2"><p>بیچاره کسی که ناشکیباست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو، جان ده که اندرین راه</p></div>
<div class="m2"><p>کاری به سخن نمی شود راست</p></div></div>