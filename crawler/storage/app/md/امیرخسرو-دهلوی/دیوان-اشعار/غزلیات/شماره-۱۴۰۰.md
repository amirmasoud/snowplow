---
title: >-
    شمارهٔ ۱۴۰۰
---
# شمارهٔ ۱۴۰۰

<div class="b" id="bn1"><div class="m1"><p>تا دامن از بساط جهان در کشیده ایم</p></div>
<div class="m2"><p>رخت خرد به کوی قلندر کشیده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ساقی، از قرابه فرو ریز می که ما</p></div>
<div class="m2"><p>خونابه ها ز شیشه اخضر کشیده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حقه سفید و سیه بر بساط خاک</p></div>
<div class="m2"><p>چون پر دغاست، باده احمر کشیده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فقر است و صد هزار معانی درو چو موی</p></div>
<div class="m2"><p>آن را گلیم کرده و در سر کشیده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون جیب حرص پر نشد از حاصل جهان</p></div>
<div class="m2"><p>دامان همت از سر آن در کشیده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سنگ زن عیار زر، ایرا گلی ست زرد</p></div>
<div class="m2"><p>چون در ترازوی خردش بر کشیده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو نه کودکیم که جوییم سرخ و زرد</p></div>
<div class="m2"><p>چون بالغان دل از زر و گوهر کشیده ایم</p></div></div>