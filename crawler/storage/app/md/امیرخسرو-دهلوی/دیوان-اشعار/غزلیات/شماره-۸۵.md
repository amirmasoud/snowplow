---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>من به هوس همی خورم ناوک سینه دوز را</p></div>
<div class="m2"><p>تا نکنی ملامتی غمزه کینه توز را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دین هزار پارسا در سر گیسوی تو شد</p></div>
<div class="m2"><p>چند به ناکسان دهی سلسله رموز را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویم وصل، گوییم رو که هنوز چند گه</p></div>
<div class="m2"><p>وای که چون برون برم از دلت این هنوز را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قصه عشق خود رود پیش فسردگان ولی</p></div>
<div class="m2"><p>سنگتراش کی خرد گوهر شب فروز را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی نیم مست من جام لبالب آر تا</p></div>
<div class="m2"><p>نقل معاشران کنم این دل خام سوز را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس که ز آه ناکسان تیره شده ست روز من</p></div>
<div class="m2"><p>نیست دو دیده بنگرم این شب تیره روز را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان چو خسروی و بس زخم تو وه که برکسی</p></div>
<div class="m2"><p>باری اگر همی زنی تیر درونه دوز را</p></div></div>