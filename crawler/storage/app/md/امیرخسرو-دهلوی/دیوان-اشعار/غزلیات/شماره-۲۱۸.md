---
title: >-
    شمارهٔ ۲۱۸
---
# شمارهٔ ۲۱۸

<div class="b" id="bn1"><div class="m1"><p>رفتی از پیش من و نقش تو از پیش نرفت</p></div>
<div class="m2"><p>کیست کو دید به رخسار تو وز خویش نرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ترا دیدم، کم رفت خیالت ز دلم</p></div>
<div class="m2"><p>کم چه باشد که خود خاطر من خویش نرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ گاهی به سوی بند نیایی، آری</p></div>
<div class="m2"><p>هیچ کاری به مراد دل درویش نرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب کنی وعده و فردات ز خاطر برود</p></div>
<div class="m2"><p>از تو این ناز و فراموشی و فرویش نرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی سبب نیست گذرهای خیالت بر من</p></div>
<div class="m2"><p>بی سبب گرگ مکابر به سوی میش نرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیر مژگان ترا جستن دلها کیش است</p></div>
<div class="m2"><p>عالمی کشته شد و تیر تو از کیش نرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من رسوا شده را خودکش و مفگن به رقیب</p></div>
<div class="m2"><p>که بدین روز کسی پیش بداندیش نرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل به مرهم چه گذاریم که بر یاد لبت</p></div>
<div class="m2"><p>هیچ وقتی دل ما را نمک از ریش نرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسروا، تن زن و بنشین پس کار خود، از آنک</p></div>
<div class="m2"><p>جگرت خون شد و کار دلت از پیش نرفت</p></div></div>