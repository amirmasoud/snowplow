---
title: >-
    شمارهٔ ۸۷۲
---
# شمارهٔ ۸۷۲

<div class="b" id="bn1"><div class="m1"><p>زان گل که اندکی بته مشک ناب شد</p></div>
<div class="m2"><p>بسیار خلق از مژه در خون خضاب شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خردگیش دیدم و گفتم که مه شوی</p></div>
<div class="m2"><p>او خود برای سوزش خلق آفتاب شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن سادگی که داشت، به سرخی شدش به دل</p></div>
<div class="m2"><p>قندی که داشت نیشکر او، شراب شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر خدا دگر به دل من گذر مکن</p></div>
<div class="m2"><p>ای چشمه حیات که خون من آب شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز بوی خون نیامد از او در دماغ من</p></div>
<div class="m2"><p>از زلف او گهی که جهان مشک ناب شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای پندگوی، نزد تو سهل است عشق،لیک</p></div>
<div class="m2"><p>مسکین کسی که جان و دل او خراب شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دی در چمن شدم بگشاید مگر دلم</p></div>
<div class="m2"><p>آهی زدم که آن همه گلها گلاب شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خواب پیش چهره خسرو پدید گشت</p></div>
<div class="m2"><p>سلطان گذشت و قصه ما نقش آب شد</p></div></div>