---
title: >-
    شمارهٔ ۱۷۴۶
---
# شمارهٔ ۱۷۴۶

<div class="b" id="bn1"><div class="m1"><p>شمع فلک برآید با آتشین زبانه</p></div>
<div class="m2"><p>ساقی نامسلمان درده می مغانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشتی من روان کن مانا کرانه یابم</p></div>
<div class="m2"><p>دریای غم ندارد چون هیچ جا کرانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون توبه ام شکستی گر نیست وجه باده</p></div>
<div class="m2"><p>بفروش خانه من با آن شرابخانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می نیم خورد خود ده ور پاره برنجی</p></div>
<div class="m2"><p>دل بر لب تو دارم، می خواستن بهانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی نی که از رخ خود بیهوش کن که باری</p></div>
<div class="m2"><p>یکدم خلاص یابم از محنت زمانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رو تا رویم بیرون دستم به گردن تو</p></div>
<div class="m2"><p>تو بیخود صبوحی، من بیهش زمانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای مه غلام حسنت، چون در خمار باشی</p></div>
<div class="m2"><p>نی رو ز خواب شسته نه موی کرده شانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مطرب به رود خود زن دستی به ابر باران</p></div>
<div class="m2"><p>وین زهد خشک ما راتر کن به یک ترانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو خراب مطرب تو مست ناز و سرخوش</p></div>
<div class="m2"><p>هان در چنین نشاطی یک رقص عاشقانه</p></div></div>