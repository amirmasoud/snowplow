---
title: >-
    شمارهٔ ۸۴۵
---
# شمارهٔ ۸۴۵

<div class="b" id="bn1"><div class="m1"><p>دریاب کز فراق تو جانم به لب رسید</p></div>
<div class="m2"><p>در آرزوی روی تو روزم به شب رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزم به غم گذشت و شبم تا چسان رود؟</p></div>
<div class="m2"><p>روزی عجب گذشت و شبی بوالعجب رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز آی تا به بوسه فشانم به پای تو</p></div>
<div class="m2"><p>کز عشق پای بوس تو جانم به لب رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین پس به جان غمزدگان از کجا رسد؟</p></div>
<div class="m2"><p>کان رفته بازگشت و زمان طرب رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسرو ندیده بود ادب روزگار هیچ</p></div>
<div class="m2"><p>اینک ز حادثات جهانش ادب رسید</p></div></div>