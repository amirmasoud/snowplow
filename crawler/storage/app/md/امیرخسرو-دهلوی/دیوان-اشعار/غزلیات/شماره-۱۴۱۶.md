---
title: >-
    شمارهٔ ۱۴۱۶
---
# شمارهٔ ۱۴۱۶

<div class="b" id="bn1"><div class="m1"><p>هر روز دیده بر ره یاد صبا نهم</p></div>
<div class="m2"><p>بر دیدگان خاک درش توتیا نهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زو صد جفا کشم که نیارم به روی گفت</p></div>
<div class="m2"><p>کاین درد خود چگونه بر آن وفا نهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندهم برون غمش که مرا خود بسوخت غم</p></div>
<div class="m2"><p>دلهای دیگران چه دگر در بلا نهم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتند «یاد می کندت » دل نمی دهد</p></div>
<div class="m2"><p>کاین تهمت دروغ بر آن آشنا نهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهان مجالس نیست که سر بر درش نهند</p></div>
<div class="m2"><p>چون من گدا رسیده که کاسه کجا نهم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی چو خواست کشتنم از بوی تو صبا</p></div>
<div class="m2"><p>آن به که جان ببوسم و پیش صبا نهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون دل ز گفت دیده مرا سوخت در به در</p></div>
<div class="m2"><p>بیرون کشم، به پیش دل مبتلا نهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شبها که گرد کوی تو گردم، به یک قدم</p></div>
<div class="m2"><p>اول نهم دو دیده و آنگاه پا نهم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگذار پاره پاره کنم بر تو خویش را</p></div>
<div class="m2"><p>پس طعمه پیش هر سگ کویت جدا نهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتی که «گل به جای رخم بین » زهی خطا</p></div>
<div class="m2"><p>کان دل گر آه می نکنم، بر گیا نهم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زین گونه کز لبت سخنی نیست روزیم</p></div>
<div class="m2"><p>زنهار پر جراحت خسرو دوا نهم!</p></div></div>