---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>روی تو به پیش نظر آسایش جان است</p></div>
<div class="m2"><p>آزادگی جان من، ار هست، همان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شهر چو تو فتنه و مردم کش و بیداد</p></div>
<div class="m2"><p>من زیستن خلق ندانم که چسان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کو دل شده ای کت نظری دیده و مرده</p></div>
<div class="m2"><p>جانش به عدم رفته و سویت نگران است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترکی که دو ابروش نشسته ست به دلها</p></div>
<div class="m2"><p>قربانش هزار است اگر چش دو کمان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی بر چو تو خورشید رسم من که به خواری</p></div>
<div class="m2"><p>بر خاک در تو سر من نیز گران است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق است، ز بابل خرد افسونش، چه داند</p></div>
<div class="m2"><p>هر چند که بنیاد خرد از همدان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر خون جگر گریه کند عاشق شهوت</p></div>
<div class="m2"><p>آن دانش که حیضش ز ره دیده روان است</p></div></div>