---
title: >-
    شمارهٔ ۱۲۶۸
---
# شمارهٔ ۱۲۶۸

<div class="b" id="bn1"><div class="m1"><p>عمری شد و ما عاشق و دیوانه بماندیم</p></div>
<div class="m2"><p>در دام چو مرغ از هوس دانه بماندیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر مرغ ز باغی و گلی بهره گرفتند</p></div>
<div class="m2"><p>مائیم که چون بوم به ویرانه بماندیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وقتی دل و جان و خردی همره ما بود</p></div>
<div class="m2"><p>عشق آمد و زیشان همه بیگانه بماندیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاران چو فرشته ز خرابات رمیدند</p></div>
<div class="m2"><p>ما چون مگسان بر سر خمخانه بماندیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کوی بتان رفت همه عمر، دریغا</p></div>
<div class="m2"><p>چون برهمن پیر به بتخانه بماندیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بخت سیه روی، تو خوش خفت که شبها</p></div>
<div class="m2"><p>ما با دل خود بر سر افسانه بماندیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاکستری افتاده نه دم مانده و نه دود</p></div>
<div class="m2"><p>زیر قدم شمع چو پروانه بماندیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناگاه پری صورتی اندر نظر آمد</p></div>
<div class="m2"><p>دیدیم در آن صورت و دیوانه بماندیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو، به زبانها که فتادیم ز زلفش</p></div>
<div class="m2"><p>گویی تو که موییم که در شانه بماندیم</p></div></div>