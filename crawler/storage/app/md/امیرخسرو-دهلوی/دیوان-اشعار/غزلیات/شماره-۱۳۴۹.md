---
title: >-
    شمارهٔ ۱۳۴۹
---
# شمارهٔ ۱۳۴۹

<div class="b" id="bn1"><div class="m1"><p>وقت آنست که ما رو به خرابان نهیم</p></div>
<div class="m2"><p>چند بر زرق و ریا نام مناجات نهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر فروشیم مصلا ز پی می، به ازآنک</p></div>
<div class="m2"><p>رخت تزویر به بازار مکافات نهیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مست گر، پای بلغزد، چو در آن ثابت است</p></div>
<div class="m2"><p>دیده بر پاش به صد عذر و مراعات نهیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده داریم و دل و جان و تن از عشق خراب</p></div>
<div class="m2"><p>بر خرابی دو سه در وجه خرابات نهیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق صورت خوبیم که خلقی همه سر</p></div>
<div class="m2"><p>بر در کعبه و ما بر قدم لات نهیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاه جان گشت چو بازیچه نفس کج باز</p></div>
<div class="m2"><p>بینم اندر محل شه رخ و سر مات نهیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل خسرو که همه شیشه می می سنجد</p></div>
<div class="m2"><p>سنگ قلب است که در پله طاعات نهیم</p></div></div>