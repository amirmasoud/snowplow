---
title: >-
    شمارهٔ ۳۴۴
---
# شمارهٔ ۳۴۴

<div class="b" id="bn1"><div class="m1"><p>مرا به سوی تو پیوند دوستی خام است</p></div>
<div class="m2"><p>به آفتاب ز ذره چه جای پیغام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار جان مقدس شدند خاکستر</p></div>
<div class="m2"><p>هنوز پختن سودات از آدمی خام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیار ساقی دریای می که جانم سوخت</p></div>
<div class="m2"><p>ز جاجه دل من گر چه دوزخ آشام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازان چراغ که دلهای خلق می سوزد</p></div>
<div class="m2"><p>چراغها به سر کوی تو به هر شام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خطاست نسبت بالای تو به سرو، که سرو</p></div>
<div class="m2"><p>نه شوخ وشنگ خرام است و مست و خودکام است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم که بستده ای باز ده که که لاف زنم</p></div>
<div class="m2"><p>که این خرابه ز سلطان خویش انعام است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زکوة حسن کم از یک نظاره آخر کار</p></div>
<div class="m2"><p>گدای کوی توام، گر چه خسروم نام است</p></div></div>