---
title: >-
    شمارهٔ ۱۷۸
---
# شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>یک موی ترا هزار دام است</p></div>
<div class="m2"><p>یک روی ترا هزار نام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان سرو به بوستان بلند است</p></div>
<div class="m2"><p>کز قد تو قایم المقام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر مه به تو ناتمام پیوست</p></div>
<div class="m2"><p>رخسار تو، ماه من تمام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف سیهت فتاده در پای</p></div>
<div class="m2"><p>بهر دل خلق پای دام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانا لب تو، اگر ببوسد</p></div>
<div class="m2"><p>فتوی ندهد که می حرام است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می بگذارد دل از تو، زیراک</p></div>
<div class="m2"><p>تو آبی و آن سفال خام است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو به تو هم عنان نخواهم</p></div>
<div class="m2"><p>زین توسن چرخ بدلگام است</p></div></div>