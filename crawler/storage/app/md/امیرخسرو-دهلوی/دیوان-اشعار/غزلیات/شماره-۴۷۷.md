---
title: >-
    شمارهٔ ۴۷۷
---
# شمارهٔ ۴۷۷

<div class="b" id="bn1"><div class="m1"><p>چشمت گهی از غمزه هشیار نخواهد شد</p></div>
<div class="m2"><p>وین دل ز خراش او بی خار نخواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تیغ زنی بر تن، ور نیش زنی بر جان</p></div>
<div class="m2"><p>ناگاه رود جانش، بیمار نخواهد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشقت ز پی کشتن مردانه به کار آمد</p></div>
<div class="m2"><p>شادم ز غمت باری بیکار نخواهد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر ما فتد ار تابی زان رخ، چه شوی رنجه؟</p></div>
<div class="m2"><p>مهتاب ز افتادن افگار نخواهد شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیهوده چه گریم خون اصلاح دل خود را؟</p></div>
<div class="m2"><p>تقویم چو از جدول طومار نخواهد شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خونخوار بود، خسرو، عاشق ز چنین باده</p></div>
<div class="m2"><p>مست است که تا محشر هشیار نخواهد شد</p></div></div>