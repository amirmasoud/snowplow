---
title: >-
    شمارهٔ ۱۴۵۷
---
# شمارهٔ ۱۴۵۷

<div class="b" id="bn1"><div class="m1"><p>نیامده ست به چشم آدمی بدین سانم</p></div>
<div class="m2"><p>پری و یا ملکی، چیستی، نمی دانم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظر به روی تو کرده دو دیده حیران شد</p></div>
<div class="m2"><p>تو رفتی از نظر و من هنوز حیرانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گمان مبر که گذارم ز دست دامن تو</p></div>
<div class="m2"><p>اگر چه از دو جهان آستین برافشانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان مقابل تو باد عاشقی در سر</p></div>
<div class="m2"><p>همی روم که به شمشیر رو نگردانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر، ای سوار، کمان در کشی به کشتن من</p></div>
<div class="m2"><p>سپر ز دیده بود گاه تیر بارانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دریده پرده دل تیر غمزه تو، چنانک</p></div>
<div class="m2"><p>شکاف گشت همه رازهای پنهانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به صبر گفتم «یک لحظه مونس من باش »</p></div>
<div class="m2"><p>جواب داد که «از هجر نیست درمانم »</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرشمه تو و جور رقیب و درد فراق</p></div>
<div class="m2"><p>بدین صفت من بیچاره زیست نتوانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش آن زمان که حریف معاشران بودم</p></div>
<div class="m2"><p>فراغ شاهد و می بود و برگ بستانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندانم آن همه هم صحبتان کجا رفتند؟</p></div>
<div class="m2"><p>که هیچ بار نیامد خبر از ایشانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنون ز دولت عشقت امید خسرو نیست</p></div>
<div class="m2"><p>که بیش جمع شود خاطر پریشانم</p></div></div>