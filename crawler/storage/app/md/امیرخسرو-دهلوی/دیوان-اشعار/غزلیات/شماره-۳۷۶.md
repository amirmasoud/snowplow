---
title: >-
    شمارهٔ ۳۷۶
---
# شمارهٔ ۳۷۶

<div class="b" id="bn1"><div class="m1"><p>صبا کو به بوی تو جان پرور است</p></div>
<div class="m2"><p>دل خلق را سوی تو رهبر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دنباله زلف مگذار کار</p></div>
<div class="m2"><p>دلی را کز آن زلف در هم تر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برون بر ازین چشم پر خون من</p></div>
<div class="m2"><p>که از خون چرا آستانت تر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سراندازیم به که رانی ز در</p></div>
<div class="m2"><p>که سر بی در دوست درد سر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دریغ است خاک درت بر سرم</p></div>
<div class="m2"><p>که این سر نه لایق بدان افسر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهی طعن جاوید خورشید را</p></div>
<div class="m2"><p>که گویند معشوق نیلوفر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگس قند و پروانه آتش گزید</p></div>
<div class="m2"><p>هوس دیگر و عاشقی دیگر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بمیرم درین سوز من عاقبت</p></div>
<div class="m2"><p>که هیزم پس از شعله خاکستر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کجا یابم آن خانه ویران شده</p></div>
<div class="m2"><p>که هر شب به جان خراب اندر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه داند ملک خفته، در خواب ناز</p></div>
<div class="m2"><p>که نالان کدامیش در پیش در است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز در باری دیده خسرو مرنج</p></div>
<div class="m2"><p>که خود عاشقان را همین زیور است</p></div></div>