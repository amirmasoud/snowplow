---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>دیوانه کرد زلف تو در یک نظر مرا</p></div>
<div class="m2"><p>فریاد ازان دو سلسله مشک تر مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنگین دل تو سخت تر از سنگ مرمر است</p></div>
<div class="m2"><p>کوه غم است بر دل ازان سنگ، مر مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دی غمزه تو کرد اشارت به سوی لب</p></div>
<div class="m2"><p>تا بوسه ای دهد ز شکر خوبتر مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رویت گل و لبت شکر و این عجب که نیست</p></div>
<div class="m2"><p>جز درد سر به حاصل ازان گل شکر مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم لب ترا که مرا عشوه ای بده</p></div>
<div class="m2"><p>از خود نداد عشوه کسی را مگر مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون من ترا درون دل خویش داشتم</p></div>
<div class="m2"><p>آخر چه دشنه داشته ای در جگر مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خسروت شمار وصال است هر شبی</p></div>
<div class="m2"><p>یک شب هم از طفیلی خسرو شمر مرا</p></div></div>