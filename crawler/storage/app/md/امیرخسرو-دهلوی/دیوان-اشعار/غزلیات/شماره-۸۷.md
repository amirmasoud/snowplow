---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>نازکیی که دیده ام آن رخ همچو لاله را</p></div>
<div class="m2"><p>سوزم و بر نیاورم پیش وی آه و ناله را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چو سگان فغان کنند از رخش اهل نه فلک</p></div>
<div class="m2"><p>ساخت مه چهارده آن بت هجده ساله را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل نماند در سری، صبر نماند در دلی</p></div>
<div class="m2"><p>برگل و لاله کس چنین کژ ننهد کلاله را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوخته رخت اگر سوی چمن گذر کند</p></div>
<div class="m2"><p>در دل خود گمان برد شعله گرم لاله را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوسه اگر همی دهی، بر لب خود حواله کن</p></div>
<div class="m2"><p>رشوت تست جان من از پی این حواله را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من به نظاره ای خوشم، وصل چه حد من بود</p></div>
<div class="m2"><p>حوصله مگس مدان کو بخورد نواله را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل خط و وام دادمت، هوش و خرد سپردمت</p></div>
<div class="m2"><p>جانست هنوز دادنی، پاره مکن قباله را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو ز پیاله می خوری من همه خون که دمبدم</p></div>
<div class="m2"><p>حق لبم همی دهی از لب خود پیاله را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل که فسرده تر بود هم به گدازش آورد</p></div>
<div class="m2"><p>ناله خسروش چنان کاتش تیز ژاله را</p></div></div>