---
title: >-
    شمارهٔ ۱۶۴۳
---
# شمارهٔ ۱۶۴۳

<div class="b" id="bn1"><div class="m1"><p>منم و خیال بازی شب و روز با جوانان</p></div>
<div class="m2"><p>ز خط خوش تو با خود ورقم خیال جوانان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که زید به شهر ازینان که اسیر تو جهانی</p></div>
<div class="m2"><p>تو چو خونیان ظالم ز کرشمه تیغ رانان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو که پیر زهد و تقوی به خرامشی کشد صد</p></div>
<div class="m2"><p>چه غمت بود عفاالله ز هلاکت جوانان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن فراقت از دل، هوس هلاک من شد</p></div>
<div class="m2"><p>چو نفیر و آه و جانم به حضور ناتوانان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من و حیرت و خموشی، تو نشناسی این معما</p></div>
<div class="m2"><p>که حدیث خوش نگفتی به زبان بی زبانان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه کنم، چه حیله سازم که به جان رسید کارم</p></div>
<div class="m2"><p>که ز طعن خلق نادان، به زبان کاردانان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو اگر چه گاه گاهی نکنی نگه به خسرو</p></div>
<div class="m2"><p>چه خوش است، وه که جانش به حدیث بدگمانان!</p></div></div>