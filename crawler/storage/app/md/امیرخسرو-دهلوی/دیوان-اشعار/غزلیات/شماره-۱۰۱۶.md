---
title: >-
    شمارهٔ ۱۰۱۶
---
# شمارهٔ ۱۰۱۶

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی و می در ده که گل در بوستان آمد</p></div>
<div class="m2"><p>زجام لاله بلبل مست گشت و در فغان آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرابی خورد غنچه از هوای ابر در پرده</p></div>
<div class="m2"><p>صبا ناگه لبش بوسید و بویش در دهان آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان غنچه و گل از پی زر بود اشکالی</p></div>
<div class="m2"><p>گشاد آن عقده مشکل، صبا چون در میان آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفیر بلبلان نگذاشت خوردن چشم نرگس را</p></div>
<div class="m2"><p>شبی گر خواب اندر دیده آن ناتوان آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه سرو را بادی ست در سر هم به پیش گل</p></div>
<div class="m2"><p>قیامی می کند کآزادگی را این نشان آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر چه بوستان بر رو زمانی خوب شد از گل</p></div>
<div class="m2"><p>به روی خوش به روی خویش آخر چون توان آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الا، ای ماه خرگاهی که ماندی در پس پرده</p></div>
<div class="m2"><p>برون آی و تماشا کن که گل در بوستان آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گلستانی ست خاک آستانت از رخ خوبان</p></div>
<div class="m2"><p>که مرغ آن گلستان خسرو سحرالبیان مد</p></div></div>