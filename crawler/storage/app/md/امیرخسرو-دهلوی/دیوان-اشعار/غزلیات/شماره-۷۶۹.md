---
title: >-
    شمارهٔ ۷۶۹
---
# شمارهٔ ۷۶۹

<div class="b" id="bn1"><div class="m1"><p>جان فدای پسرانی که نکورو باشند</p></div>
<div class="m2"><p>راحت جانست جفاشان چو جفاجو باشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود ز خوبان پری چهره همین کار آید</p></div>
<div class="m2"><p>که ستمگاره و مردم کش و بدخو باشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غنچه سان بهر جدایی همه رو پشت شوند</p></div>
<div class="m2"><p>گل صفت بهر جفا را همه تن رو باشند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه کند آهوی مسکین که سبک جان ندهد؟</p></div>
<div class="m2"><p>شهسواران که به دنباله آهو باشند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر درت گر چه بنا کرده عشاق بسی ست</p></div>
<div class="m2"><p>غرق خونند کسانی که در آن کو باشند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقان در روش عشق مسلمان نشوند</p></div>
<div class="m2"><p>که نه در سوختن خویش چو هندو باشند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در همه مستی من باش تو، ور فرمایی</p></div>
<div class="m2"><p>دل و جان نیز به یک گوشه و یکسو باشند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صفت نرگس جادوی تو کردن نارند</p></div>
<div class="m2"><p>شاعران گر چه چو خسرو همه جادو باشند</p></div></div>