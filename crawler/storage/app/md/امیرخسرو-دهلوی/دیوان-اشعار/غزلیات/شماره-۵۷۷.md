---
title: >-
    شمارهٔ ۵۷۷
---
# شمارهٔ ۵۷۷

<div class="b" id="bn1"><div class="m1"><p>از یاد تو دل جدا نخواهد شد</p></div>
<div class="m2"><p>وز بند تو جان رها نخواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را به تو دادم و نمی دانی</p></div>
<div class="m2"><p>چون می دانم مرا نخواهد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیوند تو از تو نگسلم هرگز</p></div>
<div class="m2"><p>تا جامه جان قبا نخواهد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیر مژه می زنی که کس پیشت</p></div>
<div class="m2"><p>چون من هدف بلا نخواهد شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بوسه دمی شمار، گو می کن</p></div>
<div class="m2"><p>من می شمرم، دغا نخواهد شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یارب، به کجا گریزم از تیرت؟</p></div>
<div class="m2"><p>هر جا که روم خطا نخواهد شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می گو سخنی، مترس از غمزه</p></div>
<div class="m2"><p>مست است و برین گوا نخواهد شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دردی دارم به سینه از عشقت</p></div>
<div class="m2"><p>کان درد کهن دوا نخواهد شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتی که غلام من نشد خسرو</p></div>
<div class="m2"><p>هم خواهد شد، چرا نخواهد شد؟</p></div></div>