---
title: >-
    شمارهٔ ۱۲۷۴
---
# شمارهٔ ۱۲۷۴

<div class="b" id="bn1"><div class="m1"><p>هر دم غم خود با دل افگار بگویم</p></div>
<div class="m2"><p>چون زهره آن نیست که با یار بگویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دشنام که می گفت شبی، هم ز زبانش</p></div>
<div class="m2"><p>هر دم به هوس خود را صد بار بگویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر شب روم اندر سر آن کوی و غم خود</p></div>
<div class="m2"><p>چون نشنود او، با در و دیوار بگویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کو جان گرفتار که باور کند از من؟</p></div>
<div class="m2"><p>گر من غم این جان گرفتار بگویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افگار کنم همچو دل خود دل آن کس</p></div>
<div class="m2"><p>کورا سخنی زان دل افگار بگویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب خواب شبم نی که مگر بینمت آنجا</p></div>
<div class="m2"><p>خونابه این دیده بیدار بگویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دردی ست در این سینه که بیرون نتوان داد</p></div>
<div class="m2"><p>حیف است که درد تو به اغیار بگویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خون شد ز نهفتن دل و اکنون روم، ای جان</p></div>
<div class="m2"><p>رسوا شرم و بر سر بازار بگویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک روز بپرس آخر از آن محنت شبها</p></div>
<div class="m2"><p>تا کی غم خسرو به شب تار بگویم؟</p></div></div>