---
title: >-
    شمارهٔ ۳۴۱
---
# شمارهٔ ۳۴۱

<div class="b" id="bn1"><div class="m1"><p>بیا که بی تو دل خسته غرق خوناب ست</p></div>
<div class="m2"><p>مرا نه طاقت صبر و نه زهره خواب ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب امید مرا روز روشنایی نیست</p></div>
<div class="m2"><p>جز از رخ تو که در تیره شب چو مهتاب ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی ببین که دل من چگونه می سوزد</p></div>
<div class="m2"><p>درون زلف تو گویی که کرم شب تاب ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو چشم تو که همی کعبتین غلطان است</p></div>
<div class="m2"><p>مقامرست، ولی معتکف به محراب ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جور چشم تو تن در دهم به بیماری</p></div>
<div class="m2"><p>چو نقد عافیت اندر زمانه نایاب ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخ چو آب حیات تو آب بنده بریخت</p></div>
<div class="m2"><p>هنوز دوستی بنده هم بر آن آب ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر آب دیده کنم، طعنه های سخت مزن</p></div>
<div class="m2"><p>که همچو خشت زدن در میانه آب ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حکایت من و تو پوست باز کرد ز من</p></div>
<div class="m2"><p>مگر شنو مثل گوسفند و قصاب ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو قلب می زنی و بد نگویدت خسرو</p></div>
<div class="m2"><p>چو نیست آن ز تو، این از سپهر قلاب ست</p></div></div>