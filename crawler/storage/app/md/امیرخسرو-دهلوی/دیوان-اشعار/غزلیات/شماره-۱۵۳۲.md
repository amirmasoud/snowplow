---
title: >-
    شمارهٔ ۱۵۳۲
---
# شمارهٔ ۱۵۳۲

<div class="b" id="bn1"><div class="m1"><p>تا از بر تو جدا شدم من</p></div>
<div class="m2"><p>یارب که غمت چه کرد با من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دیدن تو ز دست رفتم</p></div>
<div class="m2"><p>ای کاش ندیدمی ترا من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیماب شدی و از خیالت</p></div>
<div class="m2"><p>در خویش گمم چو کیمیا من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفت آن که به یکدیگر رسیدیم</p></div>
<div class="m2"><p>من بعد کجا تو و کجا من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیرم به غمم رها کنی تو</p></div>
<div class="m2"><p>هرگز غم تو رها کنم من!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر زنده بمانم اندر این غم</p></div>
<div class="m2"><p>جز مرگ نخواهم از خدا من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس نیست بدین ستم گرفتار</p></div>
<div class="m2"><p>با خسرو دل شکسته یا من</p></div></div>