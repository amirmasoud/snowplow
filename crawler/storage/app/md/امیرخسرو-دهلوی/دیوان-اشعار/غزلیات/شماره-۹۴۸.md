---
title: >-
    شمارهٔ ۹۴۸
---
# شمارهٔ ۹۴۸

<div class="b" id="bn1"><div class="m1"><p>جماعتی که ز هم صحبتان جدا باشند</p></div>
<div class="m2"><p>چگونه با خرد و صبر آشنا باشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هلاکت من بیچاره از کسانی پرس</p></div>
<div class="m2"><p>که چندگه ز عزیزان خود جدا باشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بنده پرسی کاخر کجا همی باشی؟</p></div>
<div class="m2"><p>ز خان و مان بدرافتادگان کجا باشند؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شهر چون تو حریفی بلای توبه خلق</p></div>
<div class="m2"><p>عجب ز زاهد و صوفی که پارسا باشند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شراب صاف و سلامت ز بهر بیخبری ست</p></div>
<div class="m2"><p>ولیک با خبران تشنه بلا باشند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلا، ز کرده خود سوختی، نمی گفتی</p></div>
<div class="m2"><p>که خوبرویان البته بیوفا باشند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلای عشق بکش، خسروا، چو آن مرغان</p></div>
<div class="m2"><p>که بند چنگل شاهین پادشا باشند</p></div></div>