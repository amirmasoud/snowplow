---
title: >-
    شمارهٔ ۸۳۵
---
# شمارهٔ ۸۳۵

<div class="b" id="bn1"><div class="m1"><p>دی زخم ناخنش به رخ چمن سمن چه بود؟</p></div>
<div class="m2"><p>وان در همی به سلسله پرشکن چه بود؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آلوده خمار چرا بود نرگسش؟</p></div>
<div class="m2"><p>پژمردگیش در گل و در نسترن چه بود؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن لحظه کامد ار نه فرشته ست یا پری</p></div>
<div class="m2"><p>گاه نظاره مردن هر مرد و زن چه بود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون من و می دگران گر نخورده بود</p></div>
<div class="m2"><p>آن رنگ خون و بوی میش در دهن چه بود؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این شادیم بکشت که خوش بود با همه</p></div>
<div class="m2"><p>و آن برشکستنش به کرشمه ز من چه بود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخ جمله را نمود و مرا گفت، تو مبین</p></div>
<div class="m2"><p>زین نفف مست و بیخبرم، کاین سخن چه بود؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیری ز جان نبود، گر این خون گرفته را</p></div>
<div class="m2"><p>سیراب دیدنش سوی آن غمزه زن چه بود؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر جان یوسف از عدم این سو نیامده ست</p></div>
<div class="m2"><p>آن تن که دیدمش به ته پیرهن چه بود؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کشتن صلاح بود، چو رسوا شدیم، از آنک</p></div>
<div class="m2"><p>تدبیر پرده پوشی ما جز کفن چه بود؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوش آن زمان که رفت ز پیش تو، خسروا</p></div>
<div class="m2"><p>چون ماند جان و دل چه شد و حال تن چه بود؟</p></div></div>