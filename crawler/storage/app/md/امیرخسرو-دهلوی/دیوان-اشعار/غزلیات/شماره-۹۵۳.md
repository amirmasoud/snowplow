---
title: >-
    شمارهٔ ۹۵۳
---
# شمارهٔ ۹۵۳

<div class="b" id="bn1"><div class="m1"><p>دل ز تو بی غم نتوانیم کرد</p></div>
<div class="m2"><p>درد تراکم نتوانیم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جرعه ای از جام جفا می کشیم</p></div>
<div class="m2"><p>رطل دمادم نتوانیم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرد غمت بر دل مسکین ما</p></div>
<div class="m2"><p>آن چه که بر غم نتوانیم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش تو خواهیم که آهی کنیم</p></div>
<div class="m2"><p>آه که آن هم نتوانیم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خنکی های دم سرد خویش</p></div>
<div class="m2"><p>دست فراهم نتوانیم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با دل ریش از تو به هر غصه ای</p></div>
<div class="m2"><p>قصه مرهم نتوانیم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو، از آن خیر نیابیم برگ</p></div>
<div class="m2"><p>حله آدم نتوانیم کرد</p></div></div>