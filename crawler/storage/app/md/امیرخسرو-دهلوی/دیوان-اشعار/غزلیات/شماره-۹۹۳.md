---
title: >-
    شمارهٔ ۹۹۳
---
# شمارهٔ ۹۹۳

<div class="b" id="bn1"><div class="m1"><p>دلبرم بی وفاست، چتوان کرد</p></div>
<div class="m2"><p>میل او با جفاست، چتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دل پادشاه کشور حسن</p></div>
<div class="m2"><p>فارغ از هر گداست، چتوان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماجراها میان حسن و وفاست</p></div>
<div class="m2"><p>حسن دور از وفاست، چتوان کرد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلبر بیوفای عهد شکن</p></div>
<div class="m2"><p>چون نه بر عهد ماست، چتوان کرد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از غمت جان به لب رسید مرا</p></div>
<div class="m2"><p>چون ترا این رضاست، چتوان کرد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن بت سست عهد سخت کمال</p></div>
<div class="m2"><p>ظلم پیشش رواست، چتوان کرد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون هنوز آن نگار شهر آشوب</p></div>
<div class="m2"><p>بر سر ماجراست، چتوان کرد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل به شوخی ربود از دستم</p></div>
<div class="m2"><p>دلبر دلرباست، چتوان کرد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلی اختیار تو خسرو</p></div>
<div class="m2"><p>چون به دست قضاست، چتوان کرد؟</p></div></div>