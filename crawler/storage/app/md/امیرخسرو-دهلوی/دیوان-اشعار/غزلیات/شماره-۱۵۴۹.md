---
title: >-
    شمارهٔ ۱۵۴۹
---
# شمارهٔ ۱۵۴۹

<div class="b" id="bn1"><div class="m1"><p>رفتی و شد بی تو جانم زار، باز آی و ببین</p></div>
<div class="m2"><p>سینه ای دارم ز هجر افگار، باز آی و ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر راه تو زان بادی که از سویت رسید</p></div>
<div class="m2"><p>دیده من پر خس و پر خار، باز آی و ببین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بیایی و ببینی حال من از گفت من</p></div>
<div class="m2"><p>بو که بزیم جان من، یک بار باز آی و ببین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تو رفتی از من و من از خود، کنون لطف کن</p></div>
<div class="m2"><p>گاه رفتن آخرین دیدار، باز آی و ببین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من نمی گویم «بیا، وین شخص چون مویم نگر»</p></div>
<div class="m2"><p>از خم گیسوی خود، یک بار بازآی و ببین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ندیدی سوزش مجنون ز درد و داغ عشق</p></div>
<div class="m2"><p>درد و داغ خسرو غمخوار، باز آی و ببین</p></div></div>