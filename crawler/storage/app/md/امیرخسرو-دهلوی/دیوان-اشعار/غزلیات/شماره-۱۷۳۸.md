---
title: >-
    شمارهٔ ۱۷۳۸
---
# شمارهٔ ۱۷۳۸

<div class="b" id="bn1"><div class="m1"><p>ای جهان چشم سیاهت بسته</p></div>
<div class="m2"><p>فتنه خود را به پناهت بسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسمان دست مه از رشته صبح</p></div>
<div class="m2"><p>پیش آن روی چو ماهت بسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم پیچیده مرا چون طومار</p></div>
<div class="m2"><p>پس به تعویذ کلاهت بسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده ره داد ترا اندر چشم</p></div>
<div class="m2"><p>خون دل آمده راهت بسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل من غرقه خون است که شد</p></div>
<div class="m2"><p>در سر زلف دو تاهت بسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواب گر چشم جهان می بندد</p></div>
<div class="m2"><p>ماند از آن چشم سیاهت بسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خطت آورد سپه بر من و شد</p></div>
<div class="m2"><p>مه به فتراک سپاهت بسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان برآرم ز زنخدان تو، تا</p></div>
<div class="m2"><p>نشد از خط سر چاهت بسته</p></div></div>