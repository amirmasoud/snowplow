---
title: >-
    شمارهٔ ۱۰۳۹
---
# شمارهٔ ۱۰۳۹

<div class="b" id="bn1"><div class="m1"><p>تنها غم خود گفتن با یار چه خوب آید؟</p></div>
<div class="m2"><p>از گاز بر آن لبها آزار چه خوب آید؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانان چو دهد فرمان در کشتن مشتاقان</p></div>
<div class="m2"><p>پیش نظرش رفتن بر دار چه خوب آید؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می سوزم و می گردم گرد سر شمع خود</p></div>
<div class="m2"><p>رقاصی پروانه بر نار، چه خوب آید؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم بار جفا بردم، هم جام جفا خوردم</p></div>
<div class="m2"><p>اینکار که من کردم، از یار چه خوب آید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن روز که جان بدهم در حسرت پابوسش</p></div>
<div class="m2"><p>بر خاک من آن بت را رفتار چه خوب آید؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی که پس از عمری شب روز کند با من</p></div>
<div class="m2"><p>شب تا به سحر پیشش گفتار چه خوب آید؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من خود بکشم خود را از دست غمش، لیکن</p></div>
<div class="m2"><p>یارب که هم از دستش این کار چه خوب آید؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون پیش بتان زاهد تسبیح گسل گردد</p></div>
<div class="m2"><p>از رشته تسبیحش زنار چه خوب آید؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون دوست کند بر جان دعوی خداوندی</p></div>
<div class="m2"><p>در بندگی از خسرو اقرار چه خوب آید؟</p></div></div>