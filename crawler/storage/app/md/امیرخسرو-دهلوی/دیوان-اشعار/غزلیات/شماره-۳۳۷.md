---
title: >-
    شمارهٔ ۳۳۷
---
# شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>کجاست دل که غمت را نهان تواند داشت</p></div>
<div class="m2"><p>به صبر کوشد و خود را بر آن تواند داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کام دشمنم از هجر و دوستی نه که او</p></div>
<div class="m2"><p>دلی به سوی من ناتوان تواند داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشید خصم تو تیغ و مرا شفیعی نه</p></div>
<div class="m2"><p>که دست مصلحتی در میان تواند داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببرد دزد غم دل که یار خواب آلود</p></div>
<div class="m2"><p>چگونه پاس دل دوستان تواند داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خراب چشم خودم وین نه آن می است که چشم</p></div>
<div class="m2"><p>شراب خوار مرا میهمان تواند داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسوزم و نزنم دم که نیست همدردی</p></div>
<div class="m2"><p>که راز سوخته ای را نهان تواند داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همی کشند که نامش مبر، چو در دلم اوست</p></div>
<div class="m2"><p>زیان چگونه زبان در دهان تواند داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نماند از مه و خورشید نازنین مرا</p></div>
<div class="m2"><p>حیات باد که او جایشان تواند داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>متاع عمر که بر باد می رود از دست</p></div>
<div class="m2"><p>مگر که لشکر رطل گران تواند داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عنایتی بکن، ای دوست، بنده خسرو را</p></div>
<div class="m2"><p>سر نیاز بر آن آستان تواند داشت</p></div></div>