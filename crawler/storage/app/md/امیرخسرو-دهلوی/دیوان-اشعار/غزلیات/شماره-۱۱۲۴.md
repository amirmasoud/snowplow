---
title: >-
    شمارهٔ ۱۱۲۴
---
# شمارهٔ ۱۱۲۴

<div class="b" id="bn1"><div class="m1"><p>دل ز تن بردی و در جانی هنوز</p></div>
<div class="m2"><p>دردها دادی و درمانی هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشکارا سینه ام بشکافتی</p></div>
<div class="m2"><p>همچنان در سینه پنهانی هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک دل کردی خراب از تیغ کین</p></div>
<div class="m2"><p>واندر این ویرانه سلطانی هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دو عالم قیمت خود گفته ای</p></div>
<div class="m2"><p>نرخ بالا کن که ارزانی هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون کس یارب نگیرد دامنت</p></div>
<div class="m2"><p>گر چه در خون ناپشیمانی هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جور کردی سالها چون کافران</p></div>
<div class="m2"><p>بهر رحمت نامسلمانی هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما ز گریه چون نمک بگداختیم</p></div>
<div class="m2"><p>تو به خنده شکرستانی هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان ز بند کالبد آزاد گشت</p></div>
<div class="m2"><p>دل به گیسوی تو زندانی هنوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیری و شاهد پرستی ناخوش است</p></div>
<div class="m2"><p>خسروا، تا کی پریشانی هنوز</p></div></div>