---
title: >-
    شمارهٔ ۱۱۷۸
---
# شمارهٔ ۱۱۷۸

<div class="b" id="bn1"><div class="m1"><p>ابر خوش است و وقت خوش است و هوای خوش</p></div>
<div class="m2"><p>ساقی مست داده به مستان صلای خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باران خوش رسید و حریفان عیش را</p></div>
<div class="m2"><p>گشت آشنای جان و زهی آشنای خوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امروز پارسایی زاهد زبی زریست</p></div>
<div class="m2"><p>کو زر که بی خبر شود آن پارسای خوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن کس ز هوشیاری عقل است بی خبر</p></div>
<div class="m2"><p>کز باده بی خبر نشود در هوای خوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه دعای توبه خوش است، ای فرشته، هان</p></div>
<div class="m2"><p>تا سوی آسمان نبری این دعای خوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مستان عشق را دل و جان وقف شاهد است</p></div>
<div class="m2"><p>حجت ز خط ساقی و مطرب گوای خوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی روی خوب خوش نبود دل به هیچ جا</p></div>
<div class="m2"><p>گل گر چه خوبرو بود و باغ جای خوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق بتان، اگر چه بلایی ست جانگداز</p></div>
<div class="m2"><p>خسرو به جان و دیده خرید این بلای خوش</p></div></div>