---
title: >-
    شمارهٔ ۵۳۷
---
# شمارهٔ ۵۳۷

<div class="b" id="bn1"><div class="m1"><p>لب از تو وز شکر پیمانه ای چند</p></div>
<div class="m2"><p>رخ از تو وز ختن بتخانه ای چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو در پیمودن آری خرمن حسن</p></div>
<div class="m2"><p>روان کن سوی ما پیمانه ای چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درازی هست در موی تو چندان</p></div>
<div class="m2"><p>که می باید به هر مو شانه ای چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیازارد گرت زان شانه مویی</p></div>
<div class="m2"><p>به پیشت بشکنم دندانه ای چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر آن روی آتشناک گردم</p></div>
<div class="m2"><p>بیاید شمع را پروانه ای چند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به زلف و عارضت دلهای سوزان</p></div>
<div class="m2"><p>شب است و آتش و دیوانه ای چند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مخسپ امشب که از بی خوابی خویش</p></div>
<div class="m2"><p>بگویم پیش تو افسانه ای چند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز چشمم دانه دانه می چکد آب</p></div>
<div class="m2"><p>چو مرغان قانعم با دانه ای چند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوشم با عشق تو بی عقل و بی جان</p></div>
<div class="m2"><p>نگنجد در میان بیگانه ای چند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر آگرد دلم کز جستجویت</p></div>
<div class="m2"><p>مرا هم کشته شد ویرانه ای چند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>براتم کن ز لب بوسی و بنویس</p></div>
<div class="m2"><p>هم از خون دلم پروانه ای چند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وگر نیشی زند از غمزه مست</p></div>
<div class="m2"><p>ز خسرو بشنود افسانه ای چند</p></div></div>