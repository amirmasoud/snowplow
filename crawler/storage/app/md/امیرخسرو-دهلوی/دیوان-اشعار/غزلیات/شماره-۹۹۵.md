---
title: >-
    شمارهٔ ۹۹۵
---
# شمارهٔ ۹۹۵

<div class="b" id="bn1"><div class="m1"><p>آنچه یک چند آب حیوان کرد</p></div>
<div class="m2"><p>لب لعلت هزار چندان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بدید آفتاب رنگ لبت</p></div>
<div class="m2"><p>لعل را زیر سنگ پنهان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابر از رشک در دندانت</p></div>
<div class="m2"><p>گوهر خویش را پریشان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو بت آزری و نقش رخت</p></div>
<div class="m2"><p>آتش سینه را گلستان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نروید گلی چو تو در باغ</p></div>
<div class="m2"><p>از دم سرد من زمستان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم بن دور از چنان رویی</p></div>
<div class="m2"><p>که از او چشم دور نتوان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشقان را نهاد چشم تو بند</p></div>
<div class="m2"><p>وانگه اندر چه زنخدان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل در آویخت جعد تو به رسن</p></div>
<div class="m2"><p>وانگه از غمزه تیر باران کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچ روزی نگشت سایه که غم</p></div>
<div class="m2"><p>نه سرم راچوسایه گردان کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گشت ویران زگریه خانه چشم</p></div>
<div class="m2"><p>غم چنین چند خانه ویران کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دید خسرو خطت چوبالب گفت</p></div>
<div class="m2"><p>که خضرمیل آب حیوان کرد</p></div></div>