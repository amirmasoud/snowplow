---
title: >-
    شمارهٔ ۱۷۵۳
---
# شمارهٔ ۱۷۵۳

<div class="b" id="bn1"><div class="m1"><p>مهی در آمده و در درونه جا کرده</p></div>
<div class="m2"><p>برفته جان و به تو جای خود رها کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه چشمها که به ره ماند بهر آمدنت</p></div>
<div class="m2"><p>چه دیده ها که سمند تو زیر پا کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبود قیمت یوسف ز هفده قلب فزون</p></div>
<div class="m2"><p>هزار جانت فزون یوسفان بها کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نعوذبالله گویم که پیش چشم تو باد</p></div>
<div class="m2"><p>هر آنچه چشم تو بر روزگار ما کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیالت آمده هر دم ز بهر کشتن من</p></div>
<div class="m2"><p>دویده گریه من پیش و مرحبا کرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نپرسد از تو کسی گر چه از کرشمه و ناز</p></div>
<div class="m2"><p>قصاص می کنی و بر گناه ناکرده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا به سایه بالای خود یکی بنواز</p></div>
<div class="m2"><p>که سرو نیز گهی سایه بر گیا کرده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو خیره دیدنی من نگر که هر باری</p></div>
<div class="m2"><p>غبار خنگ من درویزه از صبا کرده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جان خزیده دلم از تو بوسه ها، وان را</p></div>
<div class="m2"><p>ذخیره بهر زمین بوس پادشا کرده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دعای خسرو جز دیدن جمال تو نیست</p></div>
<div class="m2"><p>به پیش دیده خود هر کجا دعا کرده</p></div></div>