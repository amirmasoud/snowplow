---
title: >-
    شمارهٔ ۷۲۰
---
# شمارهٔ ۷۲۰

<div class="b" id="bn1"><div class="m1"><p>مست من بی خبر از بزم چو در خانه شود</p></div>
<div class="m2"><p>جان به همراهی آن نرگس مستانه شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دشمن جان خودم پیش تو، ای تیرانداز</p></div>
<div class="m2"><p>دوست نبود که بلا ببیند و بیگانه شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در تو حیرانست نمی داند نظارگیت</p></div>
<div class="m2"><p>آن گهی خواهد دانست که در خانه شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کنم شکر جفایت که چوشه ریزد خون</p></div>
<div class="m2"><p>بندگان را همه گفتار ندیمانه شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بسا خلق که زنار مغان خواهد بست</p></div>
<div class="m2"><p>باش تا زلف تو در کشمکش شانه شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با چنان سلسله زلف که لیلی دارد</p></div>
<div class="m2"><p>حق به دست دل مجنونست که دیوانه شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقیا، بو که نظر بر شودم بر نظرت</p></div>
<div class="m2"><p>باده می ریز که تا بر سر پیمانه شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسکه پروانه شود سوخته شمع ز عشق</p></div>
<div class="m2"><p>عارف از سوختگی عاشق پروانه شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه شب خسرو و افسانه یار و هر بار</p></div>
<div class="m2"><p>قدری گوید و سر بر سر افسانه شود</p></div></div>