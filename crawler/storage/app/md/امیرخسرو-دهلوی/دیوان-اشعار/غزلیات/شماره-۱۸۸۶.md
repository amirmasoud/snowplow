---
title: >-
    شمارهٔ ۱۸۸۶
---
# شمارهٔ ۱۸۸۶

<div class="b" id="bn1"><div class="m1"><p>ای شب تیره به گیسوی کسی می مانی</p></div>
<div class="m2"><p>وی مؤذن تو به فریاد رسی می مانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه خبر داری از آن قافله، ای مرغ سحر؟</p></div>
<div class="m2"><p>که ز فریاد به نالان جرسی می مانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گریه می خواست همی آیدم از دیدن تو</p></div>
<div class="m2"><p>زان که، ای سرو، به بالای کسی می مانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمرم آن است که در دیده همی آیی، لیک</p></div>
<div class="m2"><p>مردن این است که در دیده بسی می مانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد شبم چشم به ره مانده و روزی که رسی</p></div>
<div class="m2"><p>طاقتم نیست، اگر یک نفسی می مانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر، ای دل، چه کنم با تو، به هر جا که روی</p></div>
<div class="m2"><p>عاقبت بسته به دام هوسی می مانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه سوزنده چرا دود ز تو برنآرد؟</p></div>
<div class="m2"><p>خسروا، چون تو نزاری، به خسی می مانی</p></div></div>