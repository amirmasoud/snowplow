---
title: >-
    شمارهٔ ۸۷۶
---
# شمارهٔ ۸۷۶

<div class="b" id="bn1"><div class="m1"><p>از حال مات هیچ حکایت نمی رسد</p></div>
<div class="m2"><p>در کار مات بیش عنایت نمی رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند بگسلد چو بغایت رسید عشق</p></div>
<div class="m2"><p>جانم گسست و عشق بغایت نمی رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گمره چنان شده ست دلم با دهان تو</p></div>
<div class="m2"><p>کش از کتاب صبر هدایت نمی رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگذشت دوش زلف و رخت پیش چشم من</p></div>
<div class="m2"><p>ماهی گذشت و شب به نهایت نمی رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خون نوشته قصه دردت رسول اشک</p></div>
<div class="m2"><p>هر روز در کدام ولایت نمی رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای عقل، بگذر از سر خسرو که مر ترا</p></div>
<div class="m2"><p>در کار اهل عشق کفایت نمی رسد</p></div></div>