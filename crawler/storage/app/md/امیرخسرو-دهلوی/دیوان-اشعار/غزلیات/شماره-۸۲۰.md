---
title: >-
    شمارهٔ ۸۲۰
---
# شمارهٔ ۸۲۰

<div class="b" id="bn1"><div class="m1"><p>هر لحظه چشم شوخت ناز دگر فروشد</p></div>
<div class="m2"><p>جوینده بش باید، گر بیشتر فروشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با آنکه ما نیرزیم از چشم تو نگاهی</p></div>
<div class="m2"><p>هم می دهیم جانی، گر یک نظر فروشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیوسته گرم بادا بازار تو که در وی</p></div>
<div class="m2"><p>لعل تو جان ستاند، چشمم جگر فروشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بفروختند خلقی جان و جهان ز بهرت</p></div>
<div class="m2"><p>اندر جهان کسی خود حسن اینقدر فروشد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوز از جهان برآرد هر روز خنده تو</p></div>
<div class="m2"><p>لختی نمک بگو تا روز دگر فروشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد جان شیرین ارزد هنگام تلخ گفتن</p></div>
<div class="m2"><p>آن تلخ پاسخی کو تا زان دگر فروشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذکر لب و دهانت در هر دهن نگنجد</p></div>
<div class="m2"><p>سرگشته مفلسی کو در و گهر فروشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رعنا بود نه عاشق کاندیشه دارد از جان</p></div>
<div class="m2"><p>کز بهر سهل نقدی عیار سر فروشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دارنده سر فروشد بهر بتان و خسرو</p></div>
<div class="m2"><p>گر چه جوی نیر زد، روی چو زر فروشد</p></div></div>