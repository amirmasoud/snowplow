---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>ای خط خوش از مشک تر انگیخته مه را</p></div>
<div class="m2"><p>بر دفتر طاعت رقمی رانده گنه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افگند دل ما همه در چاه زنخدان</p></div>
<div class="m2"><p>وانگاه بپوشید به سبزه سر چه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند که زلف تو سپاهیست جهانگیر</p></div>
<div class="m2"><p>هر روز پریشان نتوان کرد سپه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیراهن یک شهر ز دست تو قبا شد</p></div>
<div class="m2"><p>یک بار چنین کج منه، ای شوخ، کله را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسرو نگرفت از تب عشق تو قراری</p></div>
<div class="m2"><p>چه جای قرارست در آتشکده که را</p></div></div>