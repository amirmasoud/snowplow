---
title: >-
    شمارهٔ ۳۷۱
---
# شمارهٔ ۳۷۱

<div class="b" id="bn1"><div class="m1"><p>سر زلف تو تا بجنبیده‌ست</p></div>
<div class="m2"><p>بوی مشک ختا بجنبیده‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی خون آمد از صبا ماناک</p></div>
<div class="m2"><p>عاشقی را هوا بجنبیده‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بجنبید زلف او از باد</p></div>
<div class="m2"><p>ناف آهو ز جا بجنبیده‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما و دیوانگی دگر کان زلف</p></div>
<div class="m2"><p>باز بر جان ما بجنبیده‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوش دل‌ها به گرد او گویی</p></div>
<div class="m2"><p>قلب صد یاد را بجنبیده‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر جگرگوشه نیست چشم مرا</p></div>
<div class="m2"><p>خون چشمم چرا بجنبیده‌ست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌رود ذکر رفتنش بسیار</p></div>
<div class="m2"><p>باز جای بلا بجنبیده‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دی شنیدم ز آه سرد منش</p></div>
<div class="m2"><p>دل چون آسیا بجنبیده‌ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یاد خسرو نمی‌کند، یا رب</p></div>
<div class="m2"><p>کاین سخن از کجا بجنبیده‌ست</p></div></div>