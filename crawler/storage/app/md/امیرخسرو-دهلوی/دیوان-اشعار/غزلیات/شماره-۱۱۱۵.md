---
title: >-
    شمارهٔ ۱۱۱۵
---
# شمارهٔ ۱۱۱۵

<div class="b" id="bn1"><div class="m1"><p>فزون شد عشق جانان روز تا روز</p></div>
<div class="m2"><p>کجا زین پس شب ما و کجا روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بیهوشی ندانم روز و شب را</p></div>
<div class="m2"><p>شبم گویی یکی گشته ست با روز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل است این، هیچ پیدا نیست، با خون</p></div>
<div class="m2"><p>شب است این، هیچ پیدا نیست، یا روز؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه خفتی؟ خیز، ای مرغ سحر خیز</p></div>
<div class="m2"><p>ترا روزی همی باید، مرا روز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگو، جانا، که روزی بر تو آیم</p></div>
<div class="m2"><p>ندارد چون شب اندوه ما روز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو خوش خفته به خواب ناز تا صبح</p></div>
<div class="m2"><p>مرا بیدار باید بود تا روز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه عیش است این که خسرو را به هجرت</p></div>
<div class="m2"><p>شود هر شب به زاری و دعا روز</p></div></div>