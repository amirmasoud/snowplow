---
title: >-
    شمارهٔ ۵۳۰
---
# شمارهٔ ۵۳۰

<div class="b" id="bn1"><div class="m1"><p>کجا بودی، بگو، ای سرو آزاد؟</p></div>
<div class="m2"><p>که رویت دیدم و اقبال رو داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر جانب همی رفتم ز مستی</p></div>
<div class="m2"><p>که ناگه چشم مستت بر من افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لبت همشیره شد با جان شیرین</p></div>
<div class="m2"><p>بدانگونه که عشق و فتنه همزاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگردان روی، گر چه من خرابم</p></div>
<div class="m2"><p>که بوده ست این خرابه وقتی آباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگردان روی از من، گر توانی</p></div>
<div class="m2"><p>که من پا بستم و تو مرغ آزاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو نازک چون ز افغانم نرنجی</p></div>
<div class="m2"><p>که از فریاد کوه آید به فریاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نصیحت گو، تو درد من ندانی</p></div>
<div class="m2"><p>که من در بسملم، تو مرغ آزاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدم چندین، چو خاکستر شد این دل</p></div>
<div class="m2"><p>که گرما خوردگان را خوش بود باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو با جان خواست رفتن یادش، ای دل</p></div>
<div class="m2"><p>رها کن تا بمیرم هم درین یاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به کویش خاک شد بیچاره خسرو</p></div>
<div class="m2"><p>فدای خاک پای آن صنم باد</p></div></div>