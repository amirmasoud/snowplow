---
title: >-
    شمارهٔ ۹۶۸
---
# شمارهٔ ۹۶۸

<div class="b" id="bn1"><div class="m1"><p>دل ز نادیدنت به جان نشود</p></div>
<div class="m2"><p>اگرم هوش بیش از آن نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مخرام اینچنین به نازکه تا</p></div>
<div class="m2"><p>خلق را جان و دل زیان نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده را خاک پات روشن شد</p></div>
<div class="m2"><p>نور بر دیده ها گران نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو چسان می رباییم، باری</p></div>
<div class="m2"><p>تن مرده به حیله جان نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغکت، بیند ار به باغ روی</p></div>
<div class="m2"><p>پیش هرگز به آشیان نشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق پشتم شکست و کیش گراینست</p></div>
<div class="m2"><p>تیر خسرو چرا کمان نشود؟</p></div></div>