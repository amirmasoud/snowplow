---
title: >-
    شمارهٔ ۱۶۵۹
---
# شمارهٔ ۱۶۵۹

<div class="b" id="bn1"><div class="m1"><p>ترکی ست بدخو آنکه من دارم سر و سودای او</p></div>
<div class="m2"><p>چشمی ست کافر آنکه شد جان و دلم یغمای او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکلی به دل پنهان شده، بالا بلای جان شده</p></div>
<div class="m2"><p>ای صد چو من قربان شده بر قد و بر بالای او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل زان سر زلف دو تا زیر کلاهش کرده جا</p></div>
<div class="m2"><p>گر جان من پرسی کجا، اینک ته یک پای او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زو ناوک و از من تنی، زو تیغ وز من گردنی</p></div>
<div class="m2"><p>این است رای چون منی تا خود چه باشد رای او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر خواست بریدن سرم، زان رفت بر تن خنجرم</p></div>
<div class="m2"><p>تا وقت مردن بنگرم باری رخ زیبای او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امروز در جانم سخن، فردای وصلم در دهن</p></div>
<div class="m2"><p>او در غم امروز من، من در غم فردای او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تن شد به رنج آموخته، دل شد به درد افروخته</p></div>
<div class="m2"><p>جان با بدن هم سوخته از آتش سودای او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر شب روم با چشم تر آنجا که بود آن سیم بر</p></div>
<div class="m2"><p>گر چه از او نبود اثر، باری ببینم جای او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در چشم من آن خاک پا گه سرمه شد، گه توتیا</p></div>
<div class="m2"><p>درمان چشم آمد مرا، خسرو، به خاک پای او</p></div></div>