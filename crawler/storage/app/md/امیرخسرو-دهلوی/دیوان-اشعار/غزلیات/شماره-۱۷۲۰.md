---
title: >-
    شمارهٔ ۱۷۲۰
---
# شمارهٔ ۱۷۲۰

<div class="b" id="bn1"><div class="m1"><p>عید است خوبان نیم شب در کوی خمار آمده</p></div>
<div class="m2"><p>سرمست گشته صبحدم، غلتان به بازار آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عید آمد از چرخ برین، پر شادمانی بین زمین</p></div>
<div class="m2"><p>مه را چو زرین جام بین از بهر خمار آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با ظلمت شب شکل مه چون ناخن شیر سیه</p></div>
<div class="m2"><p>آهوی مشرق رو به ره افتاده افسار آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینک سپیده کرد اثر، در صبح عیدی کن نظر</p></div>
<div class="m2"><p>وز می رخ مستان نگر چون برگ گلنار آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمه که آب آرد برون دیدی به کهسار اندرون</p></div>
<div class="m2"><p>بین چشمه آتش که چون بیرون ز کهسار آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دهرهای بی سکون چون سلخ شد مه بین که چون</p></div>
<div class="m2"><p>پهلوگه سلخش که چون بی هیچ آزار آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز از لطافت سر به سر کرده لبان نغزتر</p></div>
<div class="m2"><p>هر یک بر آیین دگر خونریز و خونخوار آمده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویی که ابر اندر فلک پیلی ست آن بی هیچ شک</p></div>
<div class="m2"><p>وان پیل را زرین کجک بر سر نگونسار آمده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>انگشترین بی نگین وز بهر آن انگشترین</p></div>
<div class="m2"><p>چندین هزار انگشت بین هر سو پدیدار آمده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر کس به کف کرده ملی، هر دل شکفته چون گلی</p></div>
<div class="m2"><p>وز کوس هر سو غلغلی در چرخ دوار آمده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شب کس نخفته خواب را، خوبان گلاب ناب را</p></div>
<div class="m2"><p>نقل و می و جلاب را هر سو خریدار آمده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خوش خوش گلاب مشکبو گشته روان از چار سو</p></div>
<div class="m2"><p>زو خانه و بازار و کو چون صحن گلزار آمده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شب مار دودانگیز دان، صبح از دمش خنده زنان</p></div>
<div class="m2"><p>گویی که ضحاکی ست آن اندر دم مار آمده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خورشید تیغ آتشین زنگار چرخش همنشین</p></div>
<div class="m2"><p>آن تیغ را بر چرخ بین روشن ز زنگار آمده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در خانه هر خورشیدوش گلگونه تر کرده خوش</p></div>
<div class="m2"><p>خورشید تیغ آتشین زنگار چرخش همنشین در</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در خانه هر خورشیدوش گلگونه تر کرده خوش</p></div>
<div class="m2"><p>مژگان چو تیر نیم کش، لبها چو سوفار آمده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در عید گه گشته روان هر سوی چون پیر و جوان</p></div>
<div class="m2"><p>هم عقل برده هم روان دل دزد و طرار آمده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رانده براق صفت شکن در عیدگه شاه ز من</p></div>
<div class="m2"><p>بسته به گردش آن چمن، چون شه به پیکار آمده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عالم گرفته نور خور، ور کس درو کرده نظر</p></div>
<div class="m2"><p>عطش دماغش را نگر از تاب انوار آمده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برتافته جعد سیه، وز ناز کج کرده کله</p></div>
<div class="m2"><p>وز روی ایشان عیدگه یغما و خونخوار آمده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جوشان به مرکب گرم رو، در دیده میدان کرده نو</p></div>
<div class="m2"><p>در هر رکابش نوبه نو گنبدگری کار آمده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>میخواره را امروز بین غرق شراب شکرین</p></div>
<div class="m2"><p>موری ست اندر انگبین گویی گرفتار آمده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنگ از نوای ارغنون از بس که جانی کرده خون</p></div>
<div class="m2"><p>تن تن کنان جانی برون از زیر هر تار آمده</p></div></div>