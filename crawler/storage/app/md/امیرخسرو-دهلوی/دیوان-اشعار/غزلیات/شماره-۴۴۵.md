---
title: >-
    شمارهٔ ۴۴۵
---
# شمارهٔ ۴۴۵

<div class="b" id="bn1"><div class="m1"><p>دلم را گاه آن آمد که کام از عیش برگیرد</p></div>
<div class="m2"><p>ز دست ساقی دوران چو گردون جام زر گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملامت می کند ما را خرد در عشق ورزیدن</p></div>
<div class="m2"><p>دل عاشق کجا قول خود را معتبر گیرد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عیاری کسی آرد شبی معشوق خود در بر</p></div>
<div class="m2"><p>که جان بر کف نهد تا روز ترک خواب و خور گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز راز خلوت ما شمع چون روشن کند رمزی</p></div>
<div class="m2"><p>بگو پروانه تا خادم زبان شمع برگیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر لشگر کشد سلطان به ویرانی، چه غم باشد؟</p></div>
<div class="m2"><p>گدایی را که صد کشور به یک آه سحر گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر از دست غمت خسرو شود فانی، ندارد غم</p></div>
<div class="m2"><p>به پایت گر دهد جان را، حیات نو ز سر گیرد</p></div></div>