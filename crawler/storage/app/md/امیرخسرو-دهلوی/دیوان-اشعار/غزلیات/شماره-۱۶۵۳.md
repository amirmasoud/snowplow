---
title: >-
    شمارهٔ ۱۶۵۳
---
# شمارهٔ ۱۶۵۳

<div class="b" id="bn1"><div class="m1"><p>ای رهزن عشاق، چه عیار کسی تو</p></div>
<div class="m2"><p>وی ماه شب افروز، چه طرار کسی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون است می نوشگوارت ز دل خلق</p></div>
<div class="m2"><p>ای ظالم بی مهر، چه خونخوار کسی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند که گویند مکن جور، کنی بیش</p></div>
<div class="m2"><p>زین خوی مخالف چه جفا کار کسی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خنجر زنی از غمزه و رحمت نکنی هیچ</p></div>
<div class="m2"><p>زین بیش عفاالله چه ستمگار کسی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر جان ندهم، سر نهم، آزرده کنی دل</p></div>
<div class="m2"><p>هم جان و سر تو که دل آزار کسی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوارم کنی و عزتم این بس که بگویی</p></div>
<div class="m2"><p>کای بر درم افتاده، تویی خوار کسی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چندین که جفا برد ز تو خسرو مسکین</p></div>
<div class="m2"><p>روزیش نگفتی که وفادار کسی تو</p></div></div>