---
title: >-
    شمارهٔ ۹۷۷
---
# شمارهٔ ۹۷۷

<div class="b" id="bn1"><div class="m1"><p>لب لعل تو جز به جان نبرد</p></div>
<div class="m2"><p>آشکارا برد، نهان نبرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان بدینسان که می برد لب تو</p></div>
<div class="m2"><p>هیچ کس از لب تو جان نبرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرود مه بر اوج در شب تار</p></div>
<div class="m2"><p>تا ز زلف تو نردبان نبرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش ازین بر خودم یقینی بود</p></div>
<div class="m2"><p>که دلم هیچ دلستان نبرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو ببردی همه یقین دلم</p></div>
<div class="m2"><p>بر طریقی که کس گمان نبرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم پر خون کشم به پیش تو، لیک</p></div>
<div class="m2"><p>کس جگر پیش میهمان نبرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر دو چشمم روان بود کشتی</p></div>
<div class="m2"><p>کاین همه عمر بر کران نبرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برد از ضعف هر طرف بادم</p></div>
<div class="m2"><p>هرگزم بر تو ناگهان نبرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو افتاد بر در تو چو خاک</p></div>
<div class="m2"><p>باد را گو کز آستان نبود</p></div></div>