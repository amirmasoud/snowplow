---
title: >-
    شمارهٔ ۴۶۷
---
# شمارهٔ ۴۶۷

<div class="b" id="bn1"><div class="m1"><p>چه خوش صبحی دمید امشب مرا از روی یار خود</p></div>
<div class="m2"><p>گلستان حیاتم تازه گشت از نوبهار خود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحمدالله که کشت بخت بر داد و نشد ضایع</p></div>
<div class="m2"><p>هر آنچ از دیده باران ریختم بر روزگار خود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر هجران قیامت بود کان بگذشت خود بر من</p></div>
<div class="m2"><p>در فردوس دیدم باز از روی نگار خود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمار غم نمی دانم که پیش دوستان گویم</p></div>
<div class="m2"><p>که من چیزی نمی دانم ز درد بیشمار خود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل و جان کز پی من رنجها دیدند در هجران</p></div>
<div class="m2"><p>نمودم هر دو را آن روی، کردم شرمسار خود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا آسوده باری دیده، گر چه رنجه شد پایش</p></div>
<div class="m2"><p>که مالیدم همه شب دیده را بر پای یار خود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو من بی دولتی، آنگه نظر در چون تو دلداری</p></div>
<div class="m2"><p>چه بخت است این و چه اقبال، حیرانم به کار خود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو بوسم لطف کردی و شدم هم در یکی بیهش</p></div>
<div class="m2"><p>رها کن تا ز سر گیرم که گم کردم شمار خود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من اینک رفتم، آن پا بر سرم رنجه کنی گه گه</p></div>
<div class="m2"><p>که در کوی تو خاکی می گذارم یادگار خود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خواب ست اینکه می گویی به پیش مردمان، خسرو</p></div>
<div class="m2"><p>ترا کو خواب تا ببینی ازینها در کنار خود</p></div></div>