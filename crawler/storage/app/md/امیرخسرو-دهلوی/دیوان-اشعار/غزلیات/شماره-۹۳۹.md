---
title: >-
    شمارهٔ ۹۳۹
---
# شمارهٔ ۹۳۹

<div class="b" id="bn1"><div class="m1"><p>نه بخت آنکه به موی تو راه خواهم کرد</p></div>
<div class="m2"><p>ز خواب یا به خیالت نگاه خواهم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین که جان به لب آمد مرا ز درد فراق</p></div>
<div class="m2"><p>شکیب سهل بود، چندگاه خواهم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو هیچ قصه شبهای مات باور نیست</p></div>
<div class="m2"><p>کنون ستاره و مه را گواه خواهم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی رود ز من آن آفت نظر ترسم</p></div>
<div class="m2"><p>که عمر در سر این یک نگاه خواهم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بپوش چشم من و آب دیدگان امروز</p></div>
<div class="m2"><p>که من نظاره آن کج کلاه خواهم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گذر چه می کنی آخر به سویم، ای ساقی</p></div>
<div class="m2"><p>مکن که توبه عمرم تباه خواهم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بهر آنکه نبینم برابرت سایه</p></div>
<div class="m2"><p>ز دود سینه جهانی سیاه خواهم کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرا مقابل روی تو می شود آخر؟</p></div>
<div class="m2"><p>مبین در آینه، جانا، که آه خواهم کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جفا که می رود امشب ز هجر بر خسرو</p></div>
<div class="m2"><p>حکایت ار بزنم، صبحگاه خواهم کرد</p></div></div>