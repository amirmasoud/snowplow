---
title: >-
    شمارهٔ ۱۷۹۴
---
# شمارهٔ ۱۷۹۴

<div class="b" id="bn1"><div class="m1"><p>چه بد کردیم کز ما برشکستی؟</p></div>
<div class="m2"><p>ز غم بر جان ما نشتر شکستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روان شد گریه تا گیرد عنانت</p></div>
<div class="m2"><p>گذشتی و عنان را بر شکستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا در طعنه خصمان فگندی</p></div>
<div class="m2"><p>به سنگ ناکسان گوهر شکستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنم خستی و خونم نوش کردی</p></div>
<div class="m2"><p>چرا می خوردی و ساغر شکستی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم را خرد بشکستی به هجران</p></div>
<div class="m2"><p>قوی بتخانه ای را در شکستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه شکل است این که دین را غارتیدی؟</p></div>
<div class="m2"><p>چه نازست این که هم، کافر، شکستی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه بانگ پای اسپ است این که در وجد؟</p></div>
<div class="m2"><p>نوا در حلق خنیاگر شکستی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگویم زلف کان دزد سیه را</p></div>
<div class="m2"><p>نکو کردی که پا و سر شکستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گره محکم زدی بر جان خسرو</p></div>
<div class="m2"><p>که زلف عنبرین را بر شکستی</p></div></div>