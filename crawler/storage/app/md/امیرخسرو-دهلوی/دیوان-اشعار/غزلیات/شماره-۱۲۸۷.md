---
title: >-
    شمارهٔ ۱۲۸۷
---
# شمارهٔ ۱۲۸۷

<div class="b" id="bn1"><div class="m1"><p>به دست باد، کان سو جان فرستم</p></div>
<div class="m2"><p>مرا بویی ست آخر آن فرستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر خود تیر بر جانم گشایی</p></div>
<div class="m2"><p>به استقبال تیرت جان فرستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کشتن خون بهایم آنقدر بس</p></div>
<div class="m2"><p>که گویی بهر خون فرمان فرستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همایی چون تو، وانگه استخوانم</p></div>
<div class="m2"><p>بگو تا بر سگ دربان فرستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر گوید، برنجد از طفیلی</p></div>
<div class="m2"><p>سری در خدمت چوگان فرستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نماند اندر تنم نقدی که بر شاه</p></div>
<div class="m2"><p>خراجی زین ده ویران فرستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تیزی نظر کش نه به شمشیر</p></div>
<div class="m2"><p>که خسرو را به تو قربان فرستم</p></div></div>