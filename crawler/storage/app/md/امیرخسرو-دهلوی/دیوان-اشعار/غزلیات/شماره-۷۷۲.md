---
title: >-
    شمارهٔ ۷۷۲
---
# شمارهٔ ۷۷۲

<div class="b" id="bn1"><div class="m1"><p>یار باز آمد و بوی گل و ریحان آورد</p></div>
<div class="m2"><p>خنده باغ مرا گریه هجران آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز گلهای نو از درد کهن یادم داد</p></div>
<div class="m2"><p>غنچه ها بر جگرم زخم چو پیکان آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فصل نوروز که آورد طرب بر همه خلق</p></div>
<div class="m2"><p>چشم بد روز مرا موسم باران آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر سحر باد که بر سینه من می گذرد</p></div>
<div class="m2"><p>در چمن بوی کباب از پی مستان آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوی آن گمشده خویش نمی یابم هیچ</p></div>
<div class="m2"><p>زان چه سودم که صبا بوی گلستان آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به چه کار آید بی سرو خودم، گر چه بهار</p></div>
<div class="m2"><p>سوی هر باغ بسی سرو خرامان آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نتوان زیست به جان دگران، گر چه صبا</p></div>
<div class="m2"><p>جای خاشاک ز کوی تو همه جان آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باد یارب چو رقیب تو پریشان همه وقت</p></div>
<div class="m2"><p>که ترا بر سر دلهای پریشان آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با چنان روزنی، ار بر دل خسرو صد تیر</p></div>
<div class="m2"><p>بتوان خوردن و بر روی تو نتوان آورد</p></div></div>