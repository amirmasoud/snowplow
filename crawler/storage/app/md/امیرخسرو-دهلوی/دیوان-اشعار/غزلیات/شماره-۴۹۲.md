---
title: >-
    شمارهٔ ۴۹۲
---
# شمارهٔ ۴۹۲

<div class="b" id="bn1"><div class="m1"><p>ای کز رخ تو دیده، همه جان و جهان دید</p></div>
<div class="m2"><p>در حیرت آنم که ترا چون بتوان دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با قد تو بلبل سخن سرو همی گفت</p></div>
<div class="m2"><p>آن دید گل سوری و در سرو روان دید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیچاره دلم در شکن زلف تو خون شد</p></div>
<div class="m2"><p>آری، چه کند، مصلحت وقت در آن دید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان از شکر وصل تو بی بهره نمانده ست</p></div>
<div class="m2"><p>زیرا که در آن خوردن زهری به گمان دید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را به دهانت نرسد دست، خوش آنکس</p></div>
<div class="m2"><p>کز چاشنی لعل تو دستی به دهان دید</p></div></div>