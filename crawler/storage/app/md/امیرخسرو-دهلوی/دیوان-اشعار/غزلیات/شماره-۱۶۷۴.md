---
title: >-
    شمارهٔ ۱۶۷۴
---
# شمارهٔ ۱۶۷۴

<div class="b" id="bn1"><div class="m1"><p>عاشق و دیوانه ام، سلسله یار کو</p></div>
<div class="m2"><p>سینه ز هجران بسوخت، شربت دیدار کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه گلستان خوش است، ورچه چمن دلکش است</p></div>
<div class="m2"><p>آن همه دیدم، ولی آن گل رخسار کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناله هر عاشقی از دل افگار خویش</p></div>
<div class="m2"><p>از من مسکین مپرس کان دل افگار کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش من بت پرست هست به کشتن سزا</p></div>
<div class="m2"><p>تیغ سیاست کجاست، بازوی این کار کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه که دعوی عشق، پس غم جان، چون بود</p></div>
<div class="m2"><p>دوستی جان گرفت، دوستی یار کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وه که جمالی چنان روزی این چشم نیست</p></div>
<div class="m2"><p>دیده بیدار هست، دولت بیدار کو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سخن درد ما گوش نهد گر چه یار</p></div>
<div class="m2"><p>خسرو بیچاره را طاقت گفتار کو</p></div></div>