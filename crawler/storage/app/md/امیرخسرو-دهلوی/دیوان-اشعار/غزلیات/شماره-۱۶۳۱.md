---
title: >-
    شمارهٔ ۱۶۳۱
---
# شمارهٔ ۱۶۳۱

<div class="b" id="bn1"><div class="m1"><p>بنشین نفسی کز همه لطف تو بس است این</p></div>
<div class="m2"><p>بستان که ز جانم نفس باز پس است این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هستی من چند زنی شعله هجران</p></div>
<div class="m2"><p>آخر دل و جان است، نه خاشاک و خس است این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم که گزیدم لب چون قند تو در خواب</p></div>
<div class="m2"><p>خندید و شکر ریخت که خواب مگس است این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای باد، برو این نفس از ما برسانش</p></div>
<div class="m2"><p>کای عیسی جانها، گرو یک نفس است این</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش می کنم اندر هوس روی تو جانی</p></div>
<div class="m2"><p>هست ار چه خوش آینده و ناخوش، هوس است این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم که به فریاد رس از غمزه خویشت</p></div>
<div class="m2"><p>تیری به من انداخت که فریادرس است این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من بنده آن چشم که از گوشه چشمم</p></div>
<div class="m2"><p>شب دیدی و گفتی که بر این در چه کس است این؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسرو چه کند ناله عشاق، میا تنگ</p></div>
<div class="m2"><p>کاخر هم ازان قافله بانگ جرس است این</p></div></div>