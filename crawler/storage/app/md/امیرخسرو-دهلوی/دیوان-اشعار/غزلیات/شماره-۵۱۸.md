---
title: >-
    شمارهٔ ۵۱۸
---
# شمارهٔ ۵۱۸

<div class="b" id="bn1"><div class="m1"><p>ما را غم آن شوخ، اگر بنده نسازد</p></div>
<div class="m2"><p>این غمزده با حال پراکنده نسازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیرین دهنش نازده صنع خدایست</p></div>
<div class="m2"><p>ورنه لب مردم ز شکر خنده نسازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر تا به قدم جمله هنر دارد و خوبی</p></div>
<div class="m2"><p>عیبش همه آن است که با بنده نسازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اکنون که مرا کشت، بگویند که باری</p></div>
<div class="m2"><p>خود را به ستم غمکش و شرمنده نسازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانا، ز غمت مردم و از جور برستم</p></div>
<div class="m2"><p>گر بار دگر لعل توام بنده نسازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی که به افتادگی خویش دلت سوخت</p></div>
<div class="m2"><p>خود را که بود پیش تو کافگنده نسازد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آخر ز دل خسرو بیچاره برون شو</p></div>
<div class="m2"><p>کاین خانه درین آتش سوزنده نسازد</p></div></div>