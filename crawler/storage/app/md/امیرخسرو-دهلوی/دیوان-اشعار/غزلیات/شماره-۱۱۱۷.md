---
title: >-
    شمارهٔ ۱۱۱۷
---
# شمارهٔ ۱۱۱۷

<div class="b" id="bn1"><div class="m1"><p>گشادی چشم خواب آلود را باز</p></div>
<div class="m2"><p>در فتنه به عالم کرده ای باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دور ماه رویت زلف شبرو</p></div>
<div class="m2"><p>پریشان کاری اکنون کرد آغاز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خط سبزت، اگر نه خضر وقت است</p></div>
<div class="m2"><p>چرا شد با لب جان بخش دمساز؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بستان گر روی، در سجده آید</p></div>
<div class="m2"><p>به پیش قامتت سرو سرافراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ربودی دل ز من، وانگه سپردی</p></div>
<div class="m2"><p>به دست طره دلدوز غماز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه جای جان که بر دل می زند تیر؟</p></div>
<div class="m2"><p>چو گردد ترک چشمت ناوک انداز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر ندهی به عمری کام خسرو</p></div>
<div class="m2"><p>روا باشد، به غیر او مپرداز</p></div></div>