---
title: >-
    شمارهٔ ۴۷۸
---
# شمارهٔ ۴۷۸

<div class="b" id="bn1"><div class="m1"><p>آن را که سر و کاری با چون تو نگار افتد</p></div>
<div class="m2"><p>سر پیش تو دربا زد چون کار به کار افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنگ است نه دل کو را با زلف تو افتد خویش</p></div>
<div class="m2"><p>بس طرفه بود سنگی کو بر سر مار افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افتد چو تو برخیزی در پای تو صد عاشق</p></div>
<div class="m2"><p>زین جمله چه برخیزد، با آنکه هزار افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان خاک شود زین غم کز زلف تو وامانده</p></div>
<div class="m2"><p>گل خشک شود برجا گر یاد بهار افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد گریه کند مردم تا تو به کنار آیی</p></div>
<div class="m2"><p>صد موج زند دریا تا در به کنار افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ناوک مژگانت افغان نکنم هرگز</p></div>
<div class="m2"><p>گه گه گذر بلبل هم بر سر خار افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>القصه برآوردی گردی ز دل خسرو</p></div>
<div class="m2"><p>هم دیده نمی خواهد کش با تو غبار افتد</p></div></div>