---
title: >-
    شمارهٔ ۱۳۶۸
---
# شمارهٔ ۱۳۶۸

<div class="b" id="bn1"><div class="m1"><p>گر چه از عقل و دیده و جان برخیزم</p></div>
<div class="m2"><p>حاش لله که ز سودای فلان برخیزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک زمان پیش من، ای جان و جهانم، بنشین</p></div>
<div class="m2"><p>تا بدان خوشدلی از جان و جهان برخیزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتیم یا ز من و یا ز سر جان برخیز</p></div>
<div class="m2"><p>از تو نتوانم، لیک از سر جان برخیزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پس مرگ اگر بر سر خاکم گذری</p></div>
<div class="m2"><p>بانگ پایت شنوم، نعره زنان برخیزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گه حشر چو از خاک برانگیزندم</p></div>
<div class="m2"><p>هم ز بهر تو به هر سو نگران برخیزم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوسم هست که پیش تو دمی بنشینم</p></div>
<div class="m2"><p>وز سر هر چه بگویی، پس ازان برخیزم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردم دیده مرا بهر تو در خون بنشاند</p></div>
<div class="m2"><p>من به رویت نگرم، وز سر جان برخیزم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناتوان گشتم ازان گونه که نتوان برخاست</p></div>
<div class="m2"><p>ور مرا دست بگیری تو روان برخیزم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسروم بیهده مپسند که هر دم با تو</p></div>
<div class="m2"><p>شادمان شینم و با آه و فغان برخیزم</p></div></div>