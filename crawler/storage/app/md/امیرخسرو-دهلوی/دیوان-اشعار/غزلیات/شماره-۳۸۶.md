---
title: >-
    شمارهٔ ۳۸۶
---
# شمارهٔ ۳۸۶

<div class="b" id="bn1"><div class="m1"><p>کسی را به دور حسن تو پروای خواب نیست</p></div>
<div class="m2"><p>کو دل کزان دو نرگس رعنا خراب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای محتسب که منع سر اندازیم کنی</p></div>
<div class="m2"><p>بگذر ز ما که مستی ما از شراب نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر جان ما ز غمزه مزن تیر دمبدم</p></div>
<div class="m2"><p>بر بی گناه ظلم پیاپی ثواب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل خواست بوسه ای ز لبت، بر دهان زدی</p></div>
<div class="m2"><p>در روزگار مثل تو حاضر جواب نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردم مقابل رخ تو آفتاب را</p></div>
<div class="m2"><p>چیزی ست در رخ تو که در آفتاب نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چند قصد خسرو بیچاره می کنی</p></div>
<div class="m2"><p>یک شیوه کن که حاجت چندین عتاب نیست</p></div></div>