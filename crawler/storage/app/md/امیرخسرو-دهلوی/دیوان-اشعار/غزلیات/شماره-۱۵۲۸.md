---
title: >-
    شمارهٔ ۱۵۲۸
---
# شمارهٔ ۱۵۲۸

<div class="b" id="bn1"><div class="m1"><p>ای میر همه شکر فروشان</p></div>
<div class="m2"><p>توبه شکن صلاح کوشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشاق ز دست چون تو ساقی</p></div>
<div class="m2"><p>خونابه به جای باده نوشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میکده غمت سفالی</p></div>
<div class="m2"><p>نرخ همه معرفت فروشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک خرقه رخت درست نگذاشت</p></div>
<div class="m2"><p>در صومعه کبود پوشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کاوش کنه خوبی تو</p></div>
<div class="m2"><p>کند است خیال تیزهوشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پرده چو گل دمی برون آی</p></div>
<div class="m2"><p>باد همه نیکوان فروشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوش وقت تو کآگهی نداری</p></div>
<div class="m2"><p>از آتش سینه های جوشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیدار نگشت نرگس مست</p></div>
<div class="m2"><p>از ناله بلبل خروشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تو سخنی به هر ولایت</p></div>
<div class="m2"><p>خسرو به ولایت خموشان</p></div></div>