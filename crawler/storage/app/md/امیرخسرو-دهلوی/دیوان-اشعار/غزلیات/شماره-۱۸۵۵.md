---
title: >-
    شمارهٔ ۱۸۵۵
---
# شمارهٔ ۱۸۵۵

<div class="b" id="bn1"><div class="m1"><p>چو منی را مده از دست که کمتر یابی</p></div>
<div class="m2"><p>نه چون من یابی هر یار که دیگر یابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدر من می نشناسی که چسانم به وفا</p></div>
<div class="m2"><p>باش تا صحبت یاران دگر دریابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میر خوبان ولایت شدی، از ما می پرس</p></div>
<div class="m2"><p>کاین ولایت نه همه عمر مقرر یابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قاب و قوسین خدایست کمان ابرو</p></div>
<div class="m2"><p>نه کمانی که به دکان کمانگر یابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیکویی داری، اندر حق خسرو کن صرف</p></div>
<div class="m2"><p>که بسی خوبی از این دولت بیمر یابی</p></div></div>