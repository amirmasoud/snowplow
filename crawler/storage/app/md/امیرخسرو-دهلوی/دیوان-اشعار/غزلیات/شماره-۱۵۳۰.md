---
title: >-
    شمارهٔ ۱۵۳۰
---
# شمارهٔ ۱۵۳۰

<div class="b" id="bn1"><div class="m1"><p>ای آرزوی امیدواران</p></div>
<div class="m2"><p>ای مرهم درد دل فگاران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دشمنی آنچه بود، کردی</p></div>
<div class="m2"><p>ای دوست، چنین کنند یاران؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا سایه زلف تو بدیدم</p></div>
<div class="m2"><p>دیوانه شدم چو سایه داران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افگند تنم چو موی باریک</p></div>
<div class="m2"><p>در زیر گلیم سوگواران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می گریم بر غریبی خویش</p></div>
<div class="m2"><p>چون ابر به موسم بهاران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر شرح دهم غم تو صد سال</p></div>
<div class="m2"><p>یک قصه نگویم از هزاران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن ها که تو می کنی برین دل</p></div>
<div class="m2"><p>از دل نشود به روزگاران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با این همه چشم بر سر راه</p></div>
<div class="m2"><p>می دارم چون امیدواران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا کی گذری به سوی خسرو</p></div>
<div class="m2"><p>چون بر سر کشت خشک باران</p></div></div>