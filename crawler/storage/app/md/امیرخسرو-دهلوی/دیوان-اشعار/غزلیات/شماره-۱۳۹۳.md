---
title: >-
    شمارهٔ ۱۳۹۳
---
# شمارهٔ ۱۳۹۳

<div class="b" id="bn1"><div class="m1"><p>چون نآرم آنکه فارغ زان آشنا گریزم</p></div>
<div class="m2"><p>گه در فسون نشینم، گه در دعای گریزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی کشنده او خود همره صبا شد</p></div>
<div class="m2"><p>خلق از سموم وادی، من از صبا گریزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمشیر بر کشیده عشق و مرا در آن کوی</p></div>
<div class="m2"><p>پای خرد شکسته چون از بلا گریزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جانور که باشد بگریزد از بلایی</p></div>
<div class="m2"><p>من خود بلای خویشم، از خود کجا گریزم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسرو، مگوی در کش پا از طواف کویش</p></div>
<div class="m2"><p>کو نیست آن حریفی کز وی به پا گریزم</p></div></div>