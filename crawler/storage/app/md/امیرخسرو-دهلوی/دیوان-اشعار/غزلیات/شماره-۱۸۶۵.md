---
title: >-
    شمارهٔ ۱۸۶۵
---
# شمارهٔ ۱۸۶۵

<div class="b" id="bn1"><div class="m1"><p>تا فراقت تاخت بر من بارگی</p></div>
<div class="m2"><p>ساختم با محنت و آوارگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ز ما بردی، زهی جان پروری</p></div>
<div class="m2"><p>خون ما خوردی، خهی غمخوارگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چار و ناچارت چو ما فرمان بریم</p></div>
<div class="m2"><p>چاره ما ساز در بیچارگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون عنان صبر بردی از کفم</p></div>
<div class="m2"><p>یک زمان در کش عنان بارگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وارهان یکدم از این بیداد و غم</p></div>
<div class="m2"><p>زانکه شد بیداد غم یکبارگی</p></div></div>