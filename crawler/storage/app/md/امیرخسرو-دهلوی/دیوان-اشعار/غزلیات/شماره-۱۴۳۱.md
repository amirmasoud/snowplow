---
title: >-
    شمارهٔ ۱۴۳۱
---
# شمارهٔ ۱۴۳۱

<div class="b" id="bn1"><div class="m1"><p>نهفته خورد می آن شوخ و منکر است به رویم</p></div>
<div class="m2"><p>کجاست دولت آنم که تا دهانش ببویم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خراب این هوسم که بود به خواب صبوحی</p></div>
<div class="m2"><p>من آن دهان می آلود ز آب دیده بشویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبیش دیدم در خواب، سالهاست که هر شب</p></div>
<div class="m2"><p>ز شام تا سحر آن خواب پیش خویش بگویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر ز وادی جانان صبا برد خبر من</p></div>
<div class="m2"><p>که کاروان سلامت گذر نکرد به سویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر زمینی هر کس گلی شوند و گیایی</p></div>
<div class="m2"><p>منم که مهرگیایی شوم به کوی تو رویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ناتوانیم از وی چه آن حال که بپرسیش؟</p></div>
<div class="m2"><p>همین بس است که من سر بر آستانه اویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کنون که توبه شکستم، کدوی می به سرم نه</p></div>
<div class="m2"><p>چنان که کاسه سر بشکند ز بار سبویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو بر گلوی من ار تیغ آبدار برانی</p></div>
<div class="m2"><p>بسی ز شربت آب حیات به به گلویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگو که خار جفایم ببین و مگذر ازین سوی</p></div>
<div class="m2"><p>نه خسروم من، اگر سوی تو به دیده نپویم</p></div></div>