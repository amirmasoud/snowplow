---
title: >-
    شمارهٔ ۱۶۱۵
---
# شمارهٔ ۱۶۱۵

<div class="b" id="bn1"><div class="m1"><p>دل گمگشته به بازار خریدن نتوان</p></div>
<div class="m2"><p>ور دهد لابه، چو تو یار خریدن نتوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشوه می ده که خریدار به جانم تا آنک</p></div>
<div class="m2"><p>این متاعی ست که بسیار خریدن نتوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردمی کن قدری، چند درشتی و جفا؟</p></div>
<div class="m2"><p>گل خرد هر که بود، خار خریدن نتوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه دل نیک نباشد، تو جوانی آخر</p></div>
<div class="m2"><p>جان من، روز و شب آزار خریدن نتوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی گناهی تلف سوختگان سهل مگیر</p></div>
<div class="m2"><p>زانک جان است به بازار خریدن نتوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان به سودات نهم، لیک بدین نقد حقیر</p></div>
<div class="m2"><p>ناز آن نرگس بیمار خریدن نتوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما هلاک و تو به درویش نبینی، چه کنم؟</p></div>
<div class="m2"><p>دولت و بخت به بازار خریدن نتوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسروا، زر به میان آر، چه جای سخن است</p></div>
<div class="m2"><p>بر چون سیم به گفتار خریدن نتوان</p></div></div>