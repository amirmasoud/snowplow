---
title: >-
    شمارهٔ ۱۸۹۲
---
# شمارهٔ ۱۸۹۲

<div class="b" id="bn1"><div class="m1"><p>عزیزی همچو جان، ار چه چو خاکم خوار بگذاری</p></div>
<div class="m2"><p>به حق عزتی کاندر دل من دارد آن خواری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جفا پیرایه حسن است، آن کن جان من بر من</p></div>
<div class="m2"><p>که خوبان را نزیبد زیور مهر و وفاداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تیغم گر کنی صد شاخ و از بیخم بیندازی</p></div>
<div class="m2"><p>ترا سرسبز می خواهم، ندارم برگ بیزاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز غمزه کشتیم، اکنون به بوسیدن لبی تر کن</p></div>
<div class="m2"><p>کرم کن آخر این شربت که زخمی خورده ام کاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو گم کردم به زیر خاک در کوی فراموشان</p></div>
<div class="m2"><p>فرامش گشتگان خاک را گه گاهی یاد آری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وه، ای خواب اجل، آخر نخواهی آمدن وقتی</p></div>
<div class="m2"><p>هم امروزم به خوبان خوش که من مردم ز بیداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هشیاری ندارم تاب غم، ساقی، بیار آن می</p></div>
<div class="m2"><p>که آتش رنگ شد، آتش زنم در روی هوشیاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مزن، ای دوست، چندین بر گرفتاران دل طعنه</p></div>
<div class="m2"><p>مبادا هیچ دشمن را به دست دل گرفتاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به صد جان شکر می گوید، جفاهای ترا خسرو</p></div>
<div class="m2"><p>شکایت گونه ای دارد هم از تو گر بدین کاری</p></div></div>