---
title: >-
    شمارهٔ ۱۵۶۳
---
# شمارهٔ ۱۵۶۳

<div class="b" id="bn1"><div class="m1"><p>ناز در چشم و کرشمه در سر ابرو مکن</p></div>
<div class="m2"><p>ور کنی خیر و بلا، باری نظر هر سو مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز می داری ز کشتن نرگس بدمست را</p></div>
<div class="m2"><p>این فسون گیرا نمی آید، بر آن جادو مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوسه ای دادی و کشتی، وه که دیگر گاه گاه</p></div>
<div class="m2"><p>درد عاشق را به درمان می کنی، بد خو مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیغ بر وی زن که پیشت لاف آزادی زند</p></div>
<div class="m2"><p>ما گرفتاریم، تندی بر سر ابرو مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد دل می گویم و با آنکه خوی نازکت</p></div>
<div class="m2"><p>گر دل اینجانیست، باری سوی دیگر رو مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تشنه خون مسلمان است چشم کافرت</p></div>
<div class="m2"><p>گر مسلمانی تو کافر، گفت آن هندو مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرده عشاق تو صد پاره خواهد شد چو گل</p></div>
<div class="m2"><p>باده را گستاخ با آن زلف عنبر بو مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من که از جان دست شستم، دادن پندم چه سود؟</p></div>
<div class="m2"><p>ای طبیب، ار هشیاری، مرده را دارو مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای که چون خسرو گرفتار هوای دل نه ای</p></div>
<div class="m2"><p>عافیت خواهی، نظر اندر رخ نیکو مکن</p></div></div>