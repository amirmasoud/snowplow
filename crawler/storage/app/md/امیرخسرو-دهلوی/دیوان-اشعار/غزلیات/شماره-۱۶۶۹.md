---
title: >-
    شمارهٔ ۱۶۶۹
---
# شمارهٔ ۱۶۶۹

<div class="b" id="bn1"><div class="m1"><p>از من، ای ساده پسر، دور مشو</p></div>
<div class="m2"><p>برشکسته مگذر دور مشو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه سر تا به قدم از نمکی</p></div>
<div class="m2"><p>هم از این خسته جگر دور مشو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردنم از غم تو نزدیک است</p></div>
<div class="m2"><p>یک زمانیم ز سر دور مشو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرو از پیش من و بهر خدا</p></div>
<div class="m2"><p>مطلق از پیش نظر دور مشو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تری دیده پر خون دیدی</p></div>
<div class="m2"><p>وه کزین دیده تر دور مشو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب به خسرو ده و آنگاه به لاغ</p></div>
<div class="m2"><p>با مگس گو، ز شکر دور مشو</p></div></div>