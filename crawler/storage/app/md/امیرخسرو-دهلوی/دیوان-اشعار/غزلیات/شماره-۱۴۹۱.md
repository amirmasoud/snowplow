---
title: >-
    شمارهٔ ۱۴۹۱
---
# شمارهٔ ۱۴۹۱

<div class="b" id="bn1"><div class="m1"><p>عاشق شدم، با یار بدعهد وغا کردم</p></div>
<div class="m2"><p>زان شوخ جفا دیدم، هر چند وفا کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب، چه شد آن پر فن، دل را که ستد از من</p></div>
<div class="m2"><p>من هوش که را دادم، من صبر کجا کردم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطرب غزلی تر زد، درد کهنم نو شد</p></div>
<div class="m2"><p>معذور بدم، جانا، گر جامه قبا کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک چند ز هر سودا باز آمده بود این دل</p></div>
<div class="m2"><p>ناگاه ترا دیدم، بر خویش بلا کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دی روی نکویت را اندک ترکی دیدم</p></div>
<div class="m2"><p>لیک از پی چشم بد بسیار دعا کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم که «مگر چندی ایمن زیم از غمها»</p></div>
<div class="m2"><p>دل دور نشد از تو، هر چند جدا کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر هر صنمی رفتم، در هر پسری دیدم</p></div>
<div class="m2"><p>ننشست کسی در دل چندانش که جا کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بار دگر خسرو دل بر پسران ننهد</p></div>
<div class="m2"><p>در کشمکش عشقت نیکوش سزا کردم</p></div></div>