---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>وقتی اندر سر کویی گذری بود مرا</p></div>
<div class="m2"><p>وندران کوی نهانی نظری بود مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان به جایست، ولی زنده نیم من، زیرا</p></div>
<div class="m2"><p>مایه عمر به جز جان دگری بود مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مست گشتم که شبش دیدم و در خواب هنوز</p></div>
<div class="m2"><p>به گه صبح ز مستی اثری بود مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه کس را خور و خواب و من بیچاره خراب</p></div>
<div class="m2"><p>ای خوشا وقت که خوابی و خوری بود مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ازین بودم ازین پیش، اگر هیچ نبود</p></div>
<div class="m2"><p>باری از جنس صبوری قدری بود مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باری از دیده مریزید گلابی که به عمر</p></div>
<div class="m2"><p>لذت از عشق همین درد سری بود مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ یاد آمدت، ای فتنه که وقتی زین پیش</p></div>
<div class="m2"><p>عاشق سوخته در به دری بود مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواستم دی که نمازی بکنم پیش خیال</p></div>
<div class="m2"><p>لیک آلوده به دامان جگری بود مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچ کس نبود کاندک غمی او را نبود</p></div>
<div class="m2"><p>لیکن از دولت تو بیشتری بود مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نروم پیش که یاد آیی و دیوانه شوی</p></div>
<div class="m2"><p>آنکه گه گه به گلستان گذری بود مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پاسبان روز هم از قصه خسرو بشنود</p></div>
<div class="m2"><p>کامشب از گریه چه ناخوش سحری بود مرا</p></div></div>