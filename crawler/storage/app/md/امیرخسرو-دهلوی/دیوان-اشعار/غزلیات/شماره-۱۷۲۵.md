---
title: >-
    شمارهٔ ۱۷۲۵
---
# شمارهٔ ۱۷۲۵

<div class="b" id="bn1"><div class="m1"><p>دوش در آمد از درم تازه چو باد صبحگه</p></div>
<div class="m2"><p>مشک فشانده بر قبا غالیه سوده بر کله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که دو دیده سیه بر کف پای سودمش</p></div>
<div class="m2"><p>گشت سفید چشم من شد کف پای او سیه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست گرفتمش که دل حامل درد شد ببین</p></div>
<div class="m2"><p>گر چه گرفته حامله بر طبق سفید مه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوه غم است بر دلم، کاه شده ز غم تنم</p></div>
<div class="m2"><p>پیش تو می کشم بگیر آنچه که هست کوه و که</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی نماست چشم من خاک در تو اندرو</p></div>
<div class="m2"><p>آب چو با صفا بود خاک بینمش به ته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این دل کور بیشتر بر زنخت گذر کند</p></div>
<div class="m2"><p>مرگ به خنده در شود کور چو بگذرد به چه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عارض گندمین تو هست گزیدنم هوس</p></div>
<div class="m2"><p>گر ز بهشت روی خود افگینم بدین گنه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوده ام اندر این سخن صبح رسید از افق</p></div>
<div class="m2"><p>ساخت به طره ماه من طره صبح را هبه</p></div></div>