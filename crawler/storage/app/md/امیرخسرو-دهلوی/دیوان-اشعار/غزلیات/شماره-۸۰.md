---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>عشق از پی جان گرفت ما را</p></div>
<div class="m2"><p>خلقی به زبان گرفت ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرسند به عافیت نبودیم</p></div>
<div class="m2"><p>اینک حق آن گرفت ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو قد او به ناز و فتنه</p></div>
<div class="m2"><p>هر لحظه روان گرفت ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دیده، چه ریزی از برون آب؟</p></div>
<div class="m2"><p>کاین شعله به جان گرفت ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچون کایینه گیرد آتش</p></div>
<div class="m2"><p>عشق تو چنان گرفت ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خواب، برو که باز امشب</p></div>
<div class="m2"><p>سودای فلان گرفت ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویند که مرگ طرفه خوابی ست</p></div>
<div class="m2"><p>این خواب گران گرفت ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترسم که برون برد ز عالم</p></div>
<div class="m2"><p>این غم که عنان گرفت ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خندید بر اهل درد خسرو</p></div>
<div class="m2"><p>درد دل شان گرفت ما را</p></div></div>