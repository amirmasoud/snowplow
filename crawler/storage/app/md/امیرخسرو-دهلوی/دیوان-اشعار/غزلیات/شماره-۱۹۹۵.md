---
title: >-
    شمارهٔ ۱۹۹۵
---
# شمارهٔ ۱۹۹۵

<div class="b" id="bn1"><div class="m1"><p>گر تو یک ناوک از آن چشم سیه بستانی</p></div>
<div class="m2"><p>ملک نه چرخ ز خورشید و ز مه بستانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عارضت ماند در آبنوس جان ای سلطان</p></div>
<div class="m2"><p>چه شود گر نفسی عرض سپه بستانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن دلی کش همه خوبان نتوانند ستد</p></div>
<div class="m2"><p>تو از آن چشم سیه نیم نگه بستانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی گرو جان دهم و بوسه همی خواهم وام</p></div>
<div class="m2"><p>لیک شرطی که یکی بدهی و ده بستانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدنت شد گنهم منتهی بر دیده نهم</p></div>
<div class="m2"><p>گر کشی چشمم و انصاف گنه بستانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان دهم نه کنی ارزد نه یکی صد جان آن</p></div>
<div class="m2"><p>که به صد ناز از آن گفتن نه بستانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان گریزانست ز خسرو اگر آن سو ای باد</p></div>
<div class="m2"><p>بگذری بوی از آن زلف سیه بستانی</p></div></div>