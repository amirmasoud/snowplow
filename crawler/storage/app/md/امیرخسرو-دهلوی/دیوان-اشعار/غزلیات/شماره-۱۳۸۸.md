---
title: >-
    شمارهٔ ۱۳۸۸
---
# شمارهٔ ۱۳۸۸

<div class="b" id="bn1"><div class="m1"><p>من عاشقم، نه رعنا، کز دوست کام خواهم</p></div>
<div class="m2"><p>کامم همین کزان در خاکی به کام خواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم هوس که میرم، در پیش تو کیم من؟</p></div>
<div class="m2"><p>نه خضر و نه مسیحا، نه این مقام خواهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از زنده داری شب چون نیم کشته گشتم</p></div>
<div class="m2"><p>از کشتگانت مانا خوابی تمام خواهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من خون دیده نوشتم، این است عشرت من</p></div>
<div class="m2"><p>آیا چه جای بادا بی تو که جام خواهم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یابم اگر گدایی شامی به گرد کویت</p></div>
<div class="m2"><p>نقصان بود به همت، گر ملک شام خواهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدن ز بس که بینم حسن تو دیگران را</p></div>
<div class="m2"><p>نه گل درست بینم، نه مه تمام خواهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود را سلام گویم از تو، بدین شوم خویش</p></div>
<div class="m2"><p>تو زر پخته بخشی، من سیم خام خواهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر درد عشقبازی، خسرو دوا نخواهد</p></div>
<div class="m2"><p>دردش نصیب من شد، من بر دوام خواهم</p></div></div>