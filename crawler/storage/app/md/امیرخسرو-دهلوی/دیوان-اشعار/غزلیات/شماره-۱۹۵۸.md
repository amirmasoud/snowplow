---
title: >-
    شمارهٔ ۱۹۵۸
---
# شمارهٔ ۱۹۵۸

<div class="b" id="bn1"><div class="m1"><p>ای که به چشم تو نیایم همی</p></div>
<div class="m2"><p>یک نظر آخر به چو من درهمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت که از مات فراموش گشت</p></div>
<div class="m2"><p>کاش فراموش شوی یکدمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم غم بی تو مرا بر دل است</p></div>
<div class="m2"><p>لیک دلت را چه غم از عالمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی غمی از عمر قوی شادییست</p></div>
<div class="m2"><p>شادی آن کس که ندارد غمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این دل در پیش که خالی کنم</p></div>
<div class="m2"><p>وه که ندام به جهان محرمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست درین درد من خسته را</p></div>
<div class="m2"><p>مرگ سزاوارترین مرهمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر من اگر گریه نمی آیدت</p></div>
<div class="m2"><p>وام کن از دیده خسرو نمی</p></div></div>