---
title: >-
    شمارهٔ ۸۱۴
---
# شمارهٔ ۸۱۴

<div class="b" id="bn1"><div class="m1"><p>گر جام غم فرستی، نوشم که غم نباشد</p></div>
<div class="m2"><p>کانجا که عشق باشد، این مایه کم نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودای تست در جان، نقشت درون سینه</p></div>
<div class="m2"><p>حرفی برون نیفتد تا سر قلم نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من خود فتوح دانم مردن به تیغت، اما</p></div>
<div class="m2"><p>بر تیغ تو چه گویی، یعنی ستم نباشد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خونم حلال بادش تا کس دیت نجوید</p></div>
<div class="m2"><p>کاندر قصاص خوبان قاضی حکم نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دوست، تا نخندی بر پای لغز عاشق</p></div>
<div class="m2"><p>دانی که مست مسکین ثابت قدم نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نزدیک اهل بینش کور است و کور بی شک</p></div>
<div class="m2"><p>عاشق که پیش چشمش رنگین صنم نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی که عشق نفتد تا خوب نبود، آری</p></div>
<div class="m2"><p>نارد شراب مستی تا جام جم نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای باد صبحگاهی، کافاق می نوردی</p></div>
<div class="m2"><p>گر دیده ای، نشان ده، جایی که غم نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو، تو خودنشینی با عاشقان، و لیکن</p></div>
<div class="m2"><p>در صیدگاه شیران سگ محترم نباشد</p></div></div>