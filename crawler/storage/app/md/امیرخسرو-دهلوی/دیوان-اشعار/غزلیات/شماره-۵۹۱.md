---
title: >-
    شمارهٔ ۵۹۱
---
# شمارهٔ ۵۹۱

<div class="b" id="bn1"><div class="m1"><p>آن دوست که بود خصم جان شد</p></div>
<div class="m2"><p>آن صبر که داشتم نهان شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما خود به حصور مرده بودیم</p></div>
<div class="m2"><p>خاصه که فراق در میان شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افسوس که شادیی ندیدم</p></div>
<div class="m2"><p>وین عمر عزیز رایگان شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دوست، نیافتیم کامی</p></div>
<div class="m2"><p>دشمن به دروغ بدگمان شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که اسیر گردی، ای دل</p></div>
<div class="m2"><p>دیدی که به عاقبت همان شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل بر دگری نهم، ولیکن</p></div>
<div class="m2"><p>عاشق به ستم نمی توان شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دی دلبر من سواره می رفت</p></div>
<div class="m2"><p>اشکم بدوید و همعنان شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مطرب غزلی ز شوق بر خواند</p></div>
<div class="m2"><p>خونابه ز چشم من روان شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از گریه من رقیب بدخوی</p></div>
<div class="m2"><p>با آن همه حسم مهربان شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از بسکه علاج درد من کرد</p></div>
<div class="m2"><p>بیچاره طبیب ناتوان شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خسرو به کجا ببست راهی</p></div>
<div class="m2"><p>گیرم همه خلق یک زبان شد</p></div></div>