---
title: >-
    شمارهٔ ۸۱۵
---
# شمارهٔ ۸۱۵

<div class="b" id="bn1"><div class="m1"><p>سروی چو قامت تو در بوستان نباشد</p></div>
<div class="m2"><p>زیرا که بوستان را سرو روان نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جا که بگذری تو، باشد زیان دلها</p></div>
<div class="m2"><p>در شهر کس نباشد کش زین زیان نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمت به نیم غمزه صد جان فروشد، آری</p></div>
<div class="m2"><p>رخت مقامران را نرخ گران نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گستاخی است از من کان «پا به چشم من نه »</p></div>
<div class="m2"><p>من خود ترا بگویم، گر جای آن نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویند، خسروا، از عشق خود را چه فاش کردی؟</p></div>
<div class="m2"><p>خود رنگ عشق بازان از رخ نهان نباشد</p></div></div>