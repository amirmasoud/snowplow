---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>دلم در عاشقی آواره شد آواره تر بادا</p></div>
<div class="m2"><p>تنم از بیدلی بیچاره شد بیچاره تر بادا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تاراج عزیزان زلف تو عیاری ای دارد</p></div>
<div class="m2"><p>به خونریز غریبان چشم تو عیاره تر بادا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخت تازه ست و بهر مردن خود تازه تر خواهم</p></div>
<div class="m2"><p>دلت خاره ست و بهر کشتن من خاره تر بادا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ای زهد، دعای خیر می گویی مرا این گو</p></div>
<div class="m2"><p>که آن آواره از کوی بتان آواره تر بادا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه گویند کز خونخواریش خلقی به جان آمد</p></div>
<div class="m2"><p>من این گویم که بهر جان من خونخواره تر بادا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل من پاره گشت از غم نه زانگونه که به گردد</p></div>
<div class="m2"><p>وگر جانان بدین شاد است، یارب، پاره تر بادا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو با تر دامنی خو کرد خسرو با دو چشم تر</p></div>
<div class="m2"><p>به آب چشم پاکان دامنش همواره تر بادا</p></div></div>