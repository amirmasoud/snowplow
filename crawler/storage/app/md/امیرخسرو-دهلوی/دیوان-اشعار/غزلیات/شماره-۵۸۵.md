---
title: >-
    شمارهٔ ۵۸۵
---
# شمارهٔ ۵۸۵

<div class="b" id="bn1"><div class="m1"><p>از رنگ رخت قمر توان کرد</p></div>
<div class="m2"><p>وز لعل لبت شکر توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر از دهنت خبر توان یافت</p></div>
<div class="m2"><p>در راه عدم سفر توان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماییم دو دیده وقف کرده</p></div>
<div class="m2"><p>سویت نظری مگر توان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بردار ز روی طره کاین دم</p></div>
<div class="m2"><p>شام غم ما سحر توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسرو چو اسیر گشت بر وی</p></div>
<div class="m2"><p>می کن که ازین بتر توان کرد</p></div></div>