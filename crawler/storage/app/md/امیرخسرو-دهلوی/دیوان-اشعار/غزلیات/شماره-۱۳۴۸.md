---
title: >-
    شمارهٔ ۱۳۴۸
---
# شمارهٔ ۱۳۴۸

<div class="b" id="bn1"><div class="m1"><p>نفسی برون ندادم که حدیث دل نگفتم</p></div>
<div class="m2"><p>سخنی نگفتم از تو که ز دیده در نسفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه کنون نهفته گریم که شدم ز عشق رسوا</p></div>
<div class="m2"><p>که به روی آبم آمد، غم دل که می نهفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از آن گهی که دیدم به دو چشم خوابناکت</p></div>
<div class="m2"><p>به دو چشم خوابناکت که اگر شبی بخفتم!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه خلق خواند مجنون ز پی توام که هر دم</p></div>
<div class="m2"><p>به صبا پیام دادم، به پرنده راز گفتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من اگر ز دیده رفتم سر کوی تو، چه رنجی</p></div>
<div class="m2"><p>که رهی ز دور رفتم، نه ستانه تو رفتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب من هزار ساله، تو به سینه طرفه کاری</p></div>
<div class="m2"><p>که هزار ساله راهم به میان و با تو خفتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رسدت که بوی خسرو نکشی که نازنینی</p></div>
<div class="m2"><p>که من آن گل عذابم که ز خار غم شگفتم</p></div></div>