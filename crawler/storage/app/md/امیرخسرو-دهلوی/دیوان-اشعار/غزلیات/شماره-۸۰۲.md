---
title: >-
    شمارهٔ ۸۰۲
---
# شمارهٔ ۸۰۲

<div class="b" id="bn1"><div class="m1"><p>نیست به دست امید بخت مرا آن کمند</p></div>
<div class="m2"><p>کافتدش از هیچ رو صید مرادی به بند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دعوی عیاریم رفت به کویش فرود</p></div>
<div class="m2"><p>ز آنکه سرم پست شد کنگر قصرش بلند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی سر و پا می دویم تا به کجا سر نهیم</p></div>
<div class="m2"><p>بارگی شاه شد گردن ما در کمند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنگ میا زآه من، چشم بدان از تو دور</p></div>
<div class="m2"><p>نیست رخ خوب را چاره ز دود سپند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ره جولانت چون دیده ما خاک شد</p></div>
<div class="m2"><p>دیده بسی در رهست دور ترک ران سمند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هستم ازان گفت تلخ در سکرات فنا</p></div>
<div class="m2"><p>از دمت آخر دمی چاشنیی ده ز قند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که به بازار حسن قیمت خوبان کنی</p></div>
<div class="m2"><p>پیش زلیخا مگوی «یوسفی آنجا به چند؟»</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوخته از پند خلق سوخته تر می شود</p></div>
<div class="m2"><p>کاتش عشق است تیز باد وزان است پند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو اگر عاشقی بیم ز کشتن مدار</p></div>
<div class="m2"><p>پیش رخ نیکوان جان نبود ارجمند</p></div></div>