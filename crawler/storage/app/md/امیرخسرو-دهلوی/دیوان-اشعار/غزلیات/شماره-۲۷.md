---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>خبرت هست که از خویش خبر نیست مرا</p></div>
<div class="m2"><p>گذری کن که ز غم راهگذر نیست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سرم در سر سودات رود نیست عجب</p></div>
<div class="m2"><p>سر سودای تو دارم غم سر نیست مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آب دیده که به صد خون دلش پروردم</p></div>
<div class="m2"><p>هیچ حاصل به جز از خون جگر نیست مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی رخت اشک همی بارم و گل می کارم</p></div>
<div class="m2"><p>غیر از این کار کنون کار دگر نیست مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محنت زلف تو تا یافت ظفر بر دل من</p></div>
<div class="m2"><p>بر مراد دل خود هیچ ظفر نیست مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر زلف تو زانروی ظفر ممکن نیست</p></div>
<div class="m2"><p>که تواناییی چون باد سحر نیست مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل پروانه صفت گر چه پر و بال بسوخت</p></div>
<div class="m2"><p>همچنان ز آتش عشق تو اثر نیست مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم آن شمع که در سوز چنان بی خبرم</p></div>
<div class="m2"><p>که گرم سر ببرند هیچ خبر نیست مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا که آمد رخ زیبات به چشم خسرو</p></div>
<div class="m2"><p>بر گل و لاله کنون میل نظر نیست مرا</p></div></div>