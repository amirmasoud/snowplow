---
title: >-
    شمارهٔ ۱۷۳۳
---
# شمارهٔ ۱۷۳۳

<div class="b" id="bn1"><div class="m1"><p>هر شب از سودای آن زلف سیاه</p></div>
<div class="m2"><p>بگذرانم از فلک من دود آه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر کنی دعوی خوبی، می رسد</p></div>
<div class="m2"><p>شاهدان داری دو رخ چون مهر و ماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه را با ابرویت نسبت کنم</p></div>
<div class="m2"><p>شرمساری چون نبینم زین گناه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون چندین سوخته در گردنش</p></div>
<div class="m2"><p>آنکه نامش کرده ای زلف سیاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک دل ملک تو شد، ای شاه حسن</p></div>
<div class="m2"><p>کامران بنشین به صدر بارگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خسروش خلوتگه دیدار ساخت</p></div>
<div class="m2"><p>دیده را چون دید روشن جایگاه</p></div></div>