---
title: >-
    شمارهٔ ۱۵۵۵
---
# شمارهٔ ۱۵۵۵

<div class="b" id="bn1"><div class="m1"><p>دوش سرمست آن نگار نازنین آمد برون</p></div>
<div class="m2"><p>همچو طاووسی که از خلد برین آمد برون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قامت زیبا و رویی چون بهار آراسته</p></div>
<div class="m2"><p>راستی گویی که سرو راستین آمد برون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او میان مطلق ندارد، این که می بینیم چیست؟</p></div>
<div class="m2"><p>تار مویی کز دو زلف عنبرین آمد برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نازنینا، تا میان خویش بنمایی مرا</p></div>
<div class="m2"><p>ز انتظام دیده باریک بین آمد برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون سخن می گویی، از روی تو می گوید سخن</p></div>
<div class="m2"><p>صورتی کز خامه نقاش چین آمد برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا بدید انگشترین لعل تو، خسرو، پدید</p></div>
<div class="m2"><p>دیده را آب از لب انگشترین آمد برون</p></div></div>