---
title: >-
    شمارهٔ ۱۷۳۵
---
# شمارهٔ ۱۷۳۵

<div class="b" id="bn1"><div class="m1"><p>ای ترا جور و جفا آیین همه</p></div>
<div class="m2"><p>خشم و نازت بر من مسکین همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با رقیبان تو، ای جان، چه کنم</p></div>
<div class="m2"><p>ظالم اند و بی کس و بی دین همه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داغ حسرت بر دلم ماندی و رفت</p></div>
<div class="m2"><p>جان من میر منی با این همه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالمی را با رخت عیش است و من</p></div>
<div class="m2"><p>تلخ کامم زان لب شیرین همه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شب هجران غمت با روی خویش</p></div>
<div class="m2"><p>می فشانم در سحر پروین همه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای ترا بنده شده شاهان هند</p></div>
<div class="m2"><p>وی غلامت دلبران چین همه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست مانندت، بس جستیم، هیچ</p></div>
<div class="m2"><p>در ختا و خلخ و سقین همه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش رویت در چمن گشتند آب</p></div>
<div class="m2"><p>از خجالت لاله و نسرین همه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چه می خواهی بکن، چون مر ترا</p></div>
<div class="m2"><p>می رود بر خسرو مسکین همه</p></div></div>