---
title: >-
    شمارهٔ ۲۱۲
---
# شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>خانه ام ویران شد از سودای خوبان عاقبت</p></div>
<div class="m2"><p>گشت دل مدهوش و دل شیدای خوبان عاقبت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هشت سر بر دوش من باری و باری می کشم</p></div>
<div class="m2"><p>تا مگر اندازمش در پای خوبان عاقبت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رأی آن دارم که خونم را بریزند اهل حسن</p></div>
<div class="m2"><p>شد موافق رای من با رای خوبان عاقبت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه بی مهرند مهرویان به عشاق، ای رقیب</p></div>
<div class="m2"><p>جان عاشق می شود مأوای خوبان عاقبت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبر و هوشم از سواد زلف جانان گشت کم</p></div>
<div class="m2"><p>شد همین سود من از سودای خوبان عاقبت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بارها گفتم که ندهم دل به خوبان، لیک دل</p></div>
<div class="m2"><p>گشت از جان بنده و مولای خوبان عاقبت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر دل مجروح خسرو دلبران را نیست رحم</p></div>
<div class="m2"><p>جان به زاری داد از سودای خوبان عاقبت</p></div></div>