---
title: >-
    شمارهٔ ۱۵۵۹
---
# شمارهٔ ۱۵۵۹

<div class="b" id="bn1"><div class="m1"><p>جان من از بیدلان، آخر گهی یادی بکن</p></div>
<div class="m2"><p>ور به انصافی نمی ارزیم، بیدادی مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادمانیهاست از حسن و جوانی در دلت</p></div>
<div class="m2"><p>شکر آن را یک نظر در حال ناشادی بکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر شبی ماییم و تنهایی و زندان و فراق</p></div>
<div class="m2"><p>گر توانی از فرامش گشتگان یادی بکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به دولت خانه وصلم نخوانی، ای پسر</p></div>
<div class="m2"><p>باری اینجا آی و سر در محنت آبادی بکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امشب این هجران عاشق کش نخواهد کشتنم</p></div>
<div class="m2"><p>ای مؤذن، گر نمردی، بانگ و فریادی بکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک کویت کردم اندر چشم تو زین آب و گل</p></div>
<div class="m2"><p>هم درین خانه ز بهر خویش بنیادی بکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشک خسرو را نهان در کوی خود راهی بده</p></div>
<div class="m2"><p>جوی شیرین را روان از خون فرهادی بکن</p></div></div>