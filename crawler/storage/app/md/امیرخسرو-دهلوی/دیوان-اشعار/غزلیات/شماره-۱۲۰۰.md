---
title: >-
    شمارهٔ ۱۲۰۰
---
# شمارهٔ ۱۲۰۰

<div class="b" id="bn1"><div class="m1"><p>گه گه نظری باز مدار از من درویش</p></div>
<div class="m2"><p>چون منعم بخشنده به در یوزه درویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را دل صد پاره و لعلت نمک آلود</p></div>
<div class="m2"><p>مشمار که تا روز اجل به شود این ریش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن تو فزون باد و جفای تو فزون تر</p></div>
<div class="m2"><p>تا درد دل خسته من کم نشود بیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانا، مکش اکنونم ازان شیوه که دانی</p></div>
<div class="m2"><p>کان صبر نمانده ست که می کردم ازین پیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش باش که آن غمزه خون ریز تو ما را</p></div>
<div class="m2"><p>چندان نگزارد که گشایی تو سر کیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایمن ز خیال تو نیم با همه پرسش</p></div>
<div class="m2"><p>قصاب نه از مهر کند تربیت میش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی منگر توبه، قدح بر سر من ریز</p></div>
<div class="m2"><p>تا غرقه شود این خرد مصلحت اندیش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایمان من اندر شکن زلف بتان شد</p></div>
<div class="m2"><p>کافر کندم دل که اگر گردم ازین کیش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای آن که زنی طعنه به خسرو ز پی عشق</p></div>
<div class="m2"><p>تو فارغی از درد که من خوردم ازین نیش</p></div></div>