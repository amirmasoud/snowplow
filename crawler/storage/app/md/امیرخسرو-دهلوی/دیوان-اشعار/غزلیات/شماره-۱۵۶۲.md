---
title: >-
    شمارهٔ ۱۵۶۲
---
# شمارهٔ ۱۵۶۲

<div class="b" id="bn1"><div class="m1"><p>ترک من بر عزم رفتن تیر در ترکش مکن</p></div>
<div class="m2"><p>غمزه خون ریز را بر فتنه لشکرکش مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان دل سنگین چو کردی تیر پیکان مژه</p></div>
<div class="m2"><p>تا مرا جان هست در تن تیر در ترکش مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نداری زان لب شیرین شکر ورزیدنم</p></div>
<div class="m2"><p>خنده دزدیده زان لبهای شکروش مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پایی کوبان می رود خنگت بر آتش لاخ نه</p></div>
<div class="m2"><p>گو برای جان ما را لعل در آتش مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرخ مه گم کرد و زلفت یافت، پنهانش مدار</p></div>
<div class="m2"><p>هفت دوران است سیار فلک را شش مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش رفته ست آب چشمم، خسرو از بهر وداع</p></div>
<div class="m2"><p>ابر بارانی ست در ره، تنگ بر ابرش مکن</p></div></div>