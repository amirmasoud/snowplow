---
title: >-
    شمارهٔ ۶۲۶
---
# شمارهٔ ۶۲۶

<div class="b" id="bn1"><div class="m1"><p>هر که دمی به یاد آن دلبر مه لقا زند</p></div>
<div class="m2"><p>شاه پیاده بر درش آید و مرحبا زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در همه عمر یک نفس روی نتابم از درش</p></div>
<div class="m2"><p>گر دو هزار مدعی طعنه ام از قفا زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر گل تازه رنگ و بو برگ و نوا اگر نبود</p></div>
<div class="m2"><p>لاف محبت از چه رو بلبل خوش نوازند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همنفسی ز کوی او غیر صبا ندیده ام</p></div>
<div class="m2"><p>کو نفسی به پیشم از رهگذر صفا زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناله زار شد روان جانب دوست، ای صبا</p></div>
<div class="m2"><p>زود رسان که حلقه ای بر در آشنا زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیل سرشک و خون دل چند بود روا، بگو</p></div>
<div class="m2"><p>تا که ز روی مردمی دیده به روی ما زند</p></div></div>