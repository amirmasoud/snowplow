---
title: >-
    شمارهٔ ۱۹۵۶
---
# شمارهٔ ۱۹۵۶

<div class="b" id="bn1"><div class="m1"><p>بسی نماند که جانی برون رود ز غریبی</p></div>
<div class="m2"><p>هنوز می نرساند مرا ز زلف تو طیبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مباد خواب خوش آن شوخ را که غمزه شوخش</p></div>
<div class="m2"><p>فگند خار مغیلان به خوابگاه غریبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز درد عشق بمردم خبر دهید، رفیقان</p></div>
<div class="m2"><p>اگر مفرح صبر است در دکان طبیبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندادیم چو ضمانی به تیغ راضیم، اکنون</p></div>
<div class="m2"><p>اشارتی به کرم، جان من، به سوی رقیبی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بت پرست شدم از تو، بعد ازین من و کویت</p></div>
<div class="m2"><p>به دوش رشته زناری و به دست صلیبی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زکوة حسن بده زان به هر چه می رسی، ار چه</p></div>
<div class="m2"><p>نمی رسد به گدایان دور مانده نصیبی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به گاه دیدن تو خسرو از بلا چه خورد غم</p></div>
<div class="m2"><p>چه غم نظارگی شاه را ز چوب نقیبی</p></div></div>