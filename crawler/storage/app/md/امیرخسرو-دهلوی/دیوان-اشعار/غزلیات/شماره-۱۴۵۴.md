---
title: >-
    شمارهٔ ۱۴۵۴
---
# شمارهٔ ۱۴۵۴

<div class="b" id="bn1"><div class="m1"><p>رخی که بر کف پای تو سیم تن مالم</p></div>
<div class="m2"><p>دریغم آید، اگر بر گل و سمن مالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن شبی که کنم گشت کوی تو همه روز</p></div>
<div class="m2"><p>دو دیده را به کف پای خویشتن مالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرم به راه سنان روید از هوای رخت</p></div>
<div class="m2"><p>به زیر پای چو نسرین و نسترن مالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به یاد تو همه شب خون خورم، چو روز شود</p></div>
<div class="m2"><p>ز بیم سنگدلان خاک بر دهن مالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غبار کوی تو با خویشتن برم در خاک</p></div>
<div class="m2"><p>عبیر رحمت جاوید بر کفن مالم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بهر یوسف خود نیست گریه ام، تا چند</p></div>
<div class="m2"><p>ز دیده خون دروغین به پیرهن مالم!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر رسد رخ خسرو به پاش، هر دم رخ</p></div>
<div class="m2"><p>به صد نیاز ته پای مرد و زن مالم!</p></div></div>