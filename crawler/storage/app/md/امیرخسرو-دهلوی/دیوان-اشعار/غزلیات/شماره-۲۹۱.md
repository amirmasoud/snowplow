---
title: >-
    شمارهٔ ۲۹۱
---
# شمارهٔ ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>هر سو که با هزار کرشمه خرام تست</p></div>
<div class="m2"><p>صد دل فتاده پیش به هر نیم گام تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وه آن تویی و یا مه گردون و یا خیال</p></div>
<div class="m2"><p>ماهی که گاه گاه به بالای بام تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانم فدای زلف تو آندم که پرسمت</p></div>
<div class="m2"><p>کاین چیست موی بافته، گویی که دام تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود را ز تو سلام کنم زان همی زیم</p></div>
<div class="m2"><p>میرم ازین گمان نبرم کاین سلام تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستی گرم تمام بسوزد عجب مدار</p></div>
<div class="m2"><p>زینسان که دل به پختن سودای خام تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون می کشی مرا ز کف خویش بیش ازین</p></div>
<div class="m2"><p>یک جرعه ای بریز که ای کشته شام تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خونم نگین نگین که فرو می چکد ز چشم</p></div>
<div class="m2"><p>بر هر نگین ز کلک وفا نقش نام تست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جانی که هست در کف اندیشه ها گرو</p></div>
<div class="m2"><p>بر رخ ز خون قباله نوشتم که نام تست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو که هندوانه سخن کج کج آورد</p></div>
<div class="m2"><p>یک خنده کن وظیفه او، چون غلام تست</p></div></div>