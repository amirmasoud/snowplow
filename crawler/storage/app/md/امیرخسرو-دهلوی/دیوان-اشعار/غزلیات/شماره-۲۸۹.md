---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>ناوک زنی چو غمزه او در زمانه نیست</p></div>
<div class="m2"><p>چون جان من خدنگ بلا را نشانه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوانه گشت خلق و به صحرا افتاد، ازانک</p></div>
<div class="m2"><p>در شهر بی حکایت تو هیچ خانه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز با خط تو عشق نبازند عاشقان</p></div>
<div class="m2"><p>در خط دیگران رقم عاشقانه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من در دم پسین، تو بهانه گمان بری</p></div>
<div class="m2"><p>معلوم گرددت نفسی کاین بهانه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صعب آتشیست عشق که گشتند صبر و دل</p></div>
<div class="m2"><p>خاکستر و درون و برون شان زبانه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشنو حدیث بی خبران در بیان عشق</p></div>
<div class="m2"><p>دانی که احسن القصص اندر فسانه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان خاک آستانه که پیمان عاشقان</p></div>
<div class="m2"><p>یک ذره غبار بر آن آستانه نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای پندگو، چه در پی جانم نشسته ای</p></div>
<div class="m2"><p>انگار کان پرنده درین آشیانه نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کوه گران زناله ما گم شود به رقص</p></div>
<div class="m2"><p>خسرو، به تای نغمه زنان این ترانه نیست</p></div></div>