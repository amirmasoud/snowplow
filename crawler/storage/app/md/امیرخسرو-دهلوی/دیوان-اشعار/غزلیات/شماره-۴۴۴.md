---
title: >-
    شمارهٔ ۴۴۴
---
# شمارهٔ ۴۴۴

<div class="b" id="bn1"><div class="m1"><p>سپهر هفتمین کانجا بسی برج روان گردد</p></div>
<div class="m2"><p>به هر برجی خیالی ده که خورشید روان گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه شکل است آن ز بهر کشتن خلقی بنامیزد</p></div>
<div class="m2"><p>گه از دزدیده بنماید گه از شوخی نهان گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز حسن خود چه در سر می کنی باد، ای درخت گل</p></div>
<div class="m2"><p>نهان نیم خیزش باش تا سرو روان گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که گرد آرد ز شادی جان گمره را دران ساعت</p></div>
<div class="m2"><p>که جان گرد خیال او، خیالش گرد جان گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیاید کوه جور از وی گران، لیک این گران جوری</p></div>
<div class="m2"><p>که در پیشش نیارد دم زدن کش دل گران گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر ز دیدنم مگری که رسوا می کنی ما را</p></div>
<div class="m2"><p>چه بندم حیله چون بی خواست چشم من روان گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رخی سویم نه و در ما نگاه حیرتی افگن</p></div>
<div class="m2"><p>ازان پیشم که زیر خاک مهره رایگان گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا گردد به کام من فلک کان مه رسد زین سو</p></div>
<div class="m2"><p>وگر گردد هم از فرمان شاه کامران گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وصال، اهل هوس جویند، خسرو را بس این دولت</p></div>
<div class="m2"><p>که او در کوی او بدنام و خلقی بدگمان گردد</p></div></div>