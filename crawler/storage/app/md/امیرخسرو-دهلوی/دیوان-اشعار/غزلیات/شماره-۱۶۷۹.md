---
title: >-
    شمارهٔ ۱۶۷۹
---
# شمارهٔ ۱۶۷۹

<div class="b" id="bn1"><div class="m1"><p>هر شب منم فتاده به گرد سرای تو</p></div>
<div class="m2"><p>تا روز آه و ناله کنم از برای تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی که ذره ذره شود استخوان من</p></div>
<div class="m2"><p>باشد هنوز در دل تنگم هوای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگز شب وصال تو روزی نشد مرا</p></div>
<div class="m2"><p>ای وای بر کسی که بود مبتلای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان را روان برای تو خواهم نثار کرد</p></div>
<div class="m2"><p>دستم نمی دهد که نهم سر به پای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانا، بیا ببین تو شکسته دلی من</p></div>
<div class="m2"><p>عمری گذشته است منم آشنای تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر حال زار من نظری کن ز روی لطف</p></div>
<div class="m2"><p>تو پادشاه حسنی و خسرو گدای تو</p></div></div>