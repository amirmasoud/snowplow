---
title: >-
    شمارهٔ ۱۸۳۳
---
# شمارهٔ ۱۸۳۳

<div class="b" id="bn1"><div class="m1"><p>رخ خوبت به چه ماند، به گلستان و بهاری</p></div>
<div class="m2"><p>چشم مست تو بدان نرگس رعنای خماری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می روی در ره و می گردد جان گرد سر تو</p></div>
<div class="m2"><p>هم بدان گونه که گرد سر گل باشد خاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیغ بگذار که باری حق عشقت بگذارم</p></div>
<div class="m2"><p>گر نه آنی تو که با ما حق صحبت بگذاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیهده ست این که سر کوی تو باران دو چشمم</p></div>
<div class="m2"><p>کز وفا خوشه نیابم که تو این تخم بکاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شادمانم به غمت گر چه دل سوخته خون شد</p></div>
<div class="m2"><p>شاد بادا دل تو گر چه ز ما یاد نیاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صید آن چشم شدم، گر کشدم نیست ملامت</p></div>
<div class="m2"><p>گر بجویند ز ترکان دیت خون شکاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای خیال رخ آن یار جدامانده درین دل</p></div>
<div class="m2"><p>او چون مهمان نرسد، خانه به صورت چه نگاری؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای که بی فایده پندم دهی، آن روی ندیده</p></div>
<div class="m2"><p>گر ببینیش تو هم گوش به آن پند نداری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آبگینه ست دل نازک بی طاقت خسرو</p></div>
<div class="m2"><p>بشکند وه که چنین گر تو ز دستت بگذاری</p></div></div>