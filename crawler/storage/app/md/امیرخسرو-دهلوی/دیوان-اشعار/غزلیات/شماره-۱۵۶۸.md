---
title: >-
    شمارهٔ ۱۵۶۸
---
# شمارهٔ ۱۵۶۸

<div class="b" id="bn1"><div class="m1"><p>ای دل، از آنها که رفت، گر بتوانی مکن</p></div>
<div class="m2"><p>یاد جوانی بلاست بیش تو دانی مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قسم خود، ای جان، ز تن جمله گرفتی، کنون</p></div>
<div class="m2"><p>خانه تو دیگر است خیز و گرانی مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای لب و چشمت بلا غمزه پنهان مزن</p></div>
<div class="m2"><p>تیغ بزن آشکار، داغ نهانی مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند خرامان روی، وه که بترس از خدا</p></div>
<div class="m2"><p>غارت پیران راه بین و جوانی مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند بخواهی ز جور بر سر افتادگان</p></div>
<div class="m2"><p>می بتوانی، ولیک گر بتوانی مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن چو میدان دهد، گوی ز سرها متاب</p></div>
<div class="m2"><p>رخش بقا سرکش است، سست عنانی مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهل دل ار پیش ازین کشته خوبان شدند</p></div>
<div class="m2"><p>باقی ازان تواند، دل نگرانی مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نرم تری زن گره برسر ابروی ناز</p></div>
<div class="m2"><p>حال دلم دیده ای، سخت کمانی مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسن تو عالم گرفت، خورده ز خسرو مگیر</p></div>
<div class="m2"><p>مرغ سلیمان بس است، مرغ زبانی مکن</p></div></div>