---
title: >-
    شمارهٔ ۷۸۸
---
# شمارهٔ ۷۸۸

<div class="b" id="bn1"><div class="m1"><p>ای که بر من جور تو بسیار شد</p></div>
<div class="m2"><p>زاریم بشنو که کارم زار شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من که اندر سر جنونی داشتم</p></div>
<div class="m2"><p>خاصه سودای تو با آن یار شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا لبت بر نقطه جان خط کشید</p></div>
<div class="m2"><p>نقطه جان من از پرگار شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا تو دست و پا نهادی حسن را</p></div>
<div class="m2"><p>نیکوان را دست و پا بیکار شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش پنهان می کشیدم زلف تو</p></div>
<div class="m2"><p>چشم مستت ناگهان بیدار شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از عزیزی مردم چشم منی</p></div>
<div class="m2"><p>گر چه در چشم تو مردم خوار شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو از ابروی خود سازد کمان</p></div>
<div class="m2"><p>پس به پیش خسرو خاور کشد</p></div></div>