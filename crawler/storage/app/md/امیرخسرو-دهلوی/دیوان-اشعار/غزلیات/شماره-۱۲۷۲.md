---
title: >-
    شمارهٔ ۱۲۷۲
---
# شمارهٔ ۱۲۷۲

<div class="b" id="bn1"><div class="m1"><p>گمراه شدم، ره سوی جانان ز که پرسم؟</p></div>
<div class="m2"><p>وز هجر بمردم، خبر جان ز که پرسم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سرزنش مرده دلان جان به لب آمد</p></div>
<div class="m2"><p>داروی دل زار پریشان ز که پرسم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواب اجلم در سر و من مست خیالت</p></div>
<div class="m2"><p>تعبیر چنین خواب پریشان ز که پرسم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشت آن لب سر سبز مرا، گو ز من او را</p></div>
<div class="m2"><p>کای خضر، ره چشمه حیوان ز که پرسم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای رایت حسن تو روان کشتن عشاق</p></div>
<div class="m2"><p>در آدمیان فتوی قربان ز که پرسم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک درد تو گردد دو، گرم زانکه نپرسی</p></div>
<div class="m2"><p>این درد که را گویم و درمان ز که پرسم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برد از دل من نقش بتان سحر دو چشمت</p></div>
<div class="m2"><p>سحری که تو از دل بروی آن ز که پرسم؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواهم که کشم پیش دو بادام تو خود را</p></div>
<div class="m2"><p>سلطان دو به یک مرتبه، فرمان ز که پرسم؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دادند نشان دل خسرو سوی چشمت</p></div>
<div class="m2"><p>مست است چو آن نرگس فتان، ز که پرسم؟</p></div></div>