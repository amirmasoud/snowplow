---
title: >-
    شمارهٔ ۱۹۲۷
---
# شمارهٔ ۱۹۲۷

<div class="b" id="bn1"><div class="m1"><p>گل آمد و هر مرغی زد نغمه به هر باغی</p></div>
<div class="m2"><p>هر فاخته ای دارد با همسر خود داغی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از باد صبا هر کس بشکفته چو گل خرمن</p></div>
<div class="m2"><p>آن باد که من جویم کی می وزد از باغی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کس غم خود گویان با قمری و با بلبل</p></div>
<div class="m2"><p>من سوخته می جویم رو کرده سیه زاغی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من سوخته ام، زاهد، تو طعنه زنی هر دم</p></div>
<div class="m2"><p>تا چند نهی داغی ما را زبر داغی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسرو نشود هرگز عشق و خردت با هم</p></div>
<div class="m2"><p>کان زاغ نمی گنجد در خانه انباغی</p></div></div>