---
title: >-
    شمارهٔ ۹۶۴
---
# شمارهٔ ۹۶۴

<div class="b" id="bn1"><div class="m1"><p>تا ترا جسم و جان شکار بود</p></div>
<div class="m2"><p>هر که را دل بود، فگار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشت خال لب توام، آری</p></div>
<div class="m2"><p>مگس شهد زهردار بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کسی کز لب تو می نوشد</p></div>
<div class="m2"><p>تا زید هم در آن خمار بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن زمانی که سوی تست دو چشم</p></div>
<div class="m2"><p>این دوا کاشکی دوچار بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که در کوی شاهدان می خورد</p></div>
<div class="m2"><p>پیش ما مسجدش چه کار بود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پارسایی که چون جوانانست</p></div>
<div class="m2"><p>در نمازش کجا قرار بود؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مست اگر دوزخیست، گو می باش</p></div>
<div class="m2"><p>عاشقان را ز توبه عار بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم مرا سوخت، ور چه شرح دهم</p></div>
<div class="m2"><p>بی غمان را کی استوار بود؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گریه ام خوش نیایدت، آری</p></div>
<div class="m2"><p>شربت درد خوشگوار بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دلم با چنین روارو غم</p></div>
<div class="m2"><p>خرمی را چگونه بار بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پای تو زین پس و سر خسرو</p></div>
<div class="m2"><p>عمر باید که پایدار بود</p></div></div>