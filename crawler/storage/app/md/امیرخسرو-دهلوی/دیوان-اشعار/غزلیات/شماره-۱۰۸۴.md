---
title: >-
    شمارهٔ ۱۰۸۴
---
# شمارهٔ ۱۰۸۴

<div class="b" id="bn1"><div class="m1"><p>گر تو کلاه کج نهی، هوش ز ما شود مگر</p></div>
<div class="m2"><p>ور شکنی به بر قبا، کر ته قبا شود مگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خفته به است نرگست، ور بگشائیش دمی</p></div>
<div class="m2"><p>شهر تمام کو به کو، پر ز بلا شود مگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مست و خراب شو روان پای به هر طرف فگن</p></div>
<div class="m2"><p>دیده که خاک شد به ره، در ته پا شود مگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم تو مست شد، بکن مست ترش ز خون من</p></div>
<div class="m2"><p>زان همه تیر بی خطا، یک دو خطا شود مگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنده چشم تو شدم، آن دو از آن من نشد</p></div>
<div class="m2"><p>خدمت لعل تو کنم، این دو مرا شود مگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مردم دیده مانده را بر در خویشتن ببین</p></div>
<div class="m2"><p>در دل همچو سنگ تو میل وفا شود مگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل که خراب داشتم در بر من رها نشد</p></div>
<div class="m2"><p>خواهم ازین خراب تر، از تو رها شود مگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سر زلفش، ای صبا، سوی من آر گه گهی</p></div>
<div class="m2"><p>دل که ز جای خود بشد تا که به جا شود مگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو خسته را اگر دل ندهد خیال تو</p></div>
<div class="m2"><p>جان و تنم ز یکدگر هر دو جدا شود مگر</p></div></div>