---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>ز دور نیست میسر نظر به روی تو ما را</p></div>
<div class="m2"><p>چه دولتی ست تعالی الله از قد تو قبا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آنگهی که تو سلطان به ملک دل بنشستی</p></div>
<div class="m2"><p>نشاط و خواب به شبها حرام گشت گدا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز تیغ کش به حضورم که پادشاه بتانی</p></div>
<div class="m2"><p>به دور باش فراقم مکش ز بهر خدا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه در دل ما ماند یادگار جفایت</p></div>
<div class="m2"><p>مباد آنکه رود از درونه یاد تو ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دریغ جان که یکی بیش نیست ورنه ز چشمت</p></div>
<div class="m2"><p>به نرخ نیک خریدن توان متاع بلا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرامشی سرکو که گه از گهی به کرشمه</p></div>
<div class="m2"><p>که زیر خاک کنی زنده کشتگان بلا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مفرحی که طبیبان دهند دوست ندارم</p></div>
<div class="m2"><p>که برد لذت دردت ز کام ذوق دوا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو جان دهم قدمی سویم آوری که عزیزان</p></div>
<div class="m2"><p>گلی دریغ ندارند خاک اهل وفا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه من اسیر بتانم به اختیار و لیکن</p></div>
<div class="m2"><p>گسست می نتواند کسی کمند قضا را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نسیم هم نرسد زو گهی که زنده بمانم</p></div>
<div class="m2"><p>مگر که بر سر کویش گذر نماند صبا را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به چشم خسرو از آنگه که جا گرفت خیالش</p></div>
<div class="m2"><p>ز آب چشم به هر سوگلی شکفت صبا را</p></div></div>