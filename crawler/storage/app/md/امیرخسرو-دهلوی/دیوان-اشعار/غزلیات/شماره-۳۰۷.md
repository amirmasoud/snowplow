---
title: >-
    شمارهٔ ۳۰۷
---
# شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>ای پیر، خاک پای تو نور سعادت است</p></div>
<div class="m2"><p>مقراض توبه تو چو لای شهادت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستی تو آن نظام که نون خطاب تو</p></div>
<div class="m2"><p>محراب راست کرده برای عبادت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دید آنکه طلعت تو و بیداریش نبود</p></div>
<div class="m2"><p>هست آن سگی که خفتن صبحش به عادت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو شمع صبح، شعله شوقی که از تو خاست</p></div>
<div class="m2"><p>زان هر یکی شراره چراغ هدایت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علامه ای که معرفت انبیاش هست</p></div>
<div class="m2"><p>او را به پیش تو محل استفادت است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عهد تو قیام جهان از وجود تست</p></div>
<div class="m2"><p>مانند صورتی که قیامش به مادت است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر یک مرید تو چو هلالی ست از رکوع</p></div>
<div class="m2"><p>هر شب هلال وار ازان در زیادت است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بتوان مرید گفت مرید ترا که اوست</p></div>
<div class="m2"><p>آن مردمی که فتنه عین سعادت است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امید کز تو واصل گردد چو خرد و پیر</p></div>
<div class="m2"><p>خسرو که بی وصال چو حرف ارادت است</p></div></div>