---
title: >-
    شمارهٔ ۱۹۹۴
---
# شمارهٔ ۱۹۹۴

<div class="b" id="bn1"><div class="m1"><p>خیالی کرده‌ام وین از خیال خود نمی‌دانی</p></div>
<div class="m2"><p>ز ابرو پرس اگر جور هلال خود نمی‌دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهادی سنبله بر مشتری و می‌کشی خلقی</p></div>
<div class="m2"><p>منت آگه کنم گر تو وبال خود نمی‌دانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز جولان سمندت دور بادا چشم بد گرچه</p></div>
<div class="m2"><p>صف موران مسکین پایمال خود نمی‌دانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مه دوهفته می‌خوانی رخ خود را و من چون مه</p></div>
<div class="m2"><p>همی‌کاهم که در خوبی کمال خود نمی‌دانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگو ای شاخ خلق از دیدنم بهر چه می‌میرند</p></div>
<div class="m2"><p>تو یعنی از بلای زلف و خال خود نمی‌دانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دمی با مردم دیده نشستی پس دم دیگر</p></div>
<div class="m2"><p>اگر زین هم نشینی بد ملال خود نمی‌دانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخواهد رفت ناگه جان . . . خود درین خسرو</p></div>
<div class="m2"><p>که حالی در چنین نظاره حال خود نمی‌دانی</p></div></div>