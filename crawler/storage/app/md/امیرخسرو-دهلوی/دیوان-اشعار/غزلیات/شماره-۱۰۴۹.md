---
title: >-
    شمارهٔ ۱۰۴۹
---
# شمارهٔ ۱۰۴۹

<div class="b" id="bn1"><div class="m1"><p>غم کشت مرا آن بت نوشاد نیامد</p></div>
<div class="m2"><p>گنجشک برمد از خفه، صیاد نیامد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق شدم، این بود گنه، وای که هجرش</p></div>
<div class="m2"><p>جان برد و ازین یک گنه آزاد نیامد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر گریه عاشق که زدم خنده، نمردم</p></div>
<div class="m2"><p>تا پیش دو چشم من ناشاد نیامد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه سود ازین مردن بی بهره که شیرین</p></div>
<div class="m2"><p>روزی به سر تربت فرهاد نیامد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی که شبی زود رسم، روز بدم بین</p></div>
<div class="m2"><p>کان نیز به روز دگرت یاد نیامد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با خاک نسازد، چه کند این تن خاکی</p></div>
<div class="m2"><p>امروز که از جانب تو باد نیامد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاراج خیالت شدم و بدرقه صبر</p></div>
<div class="m2"><p>آنجا که مرا دوش ره افتاد نیامد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فریاد کنان دی به سر کوی تو رفتم</p></div>
<div class="m2"><p>جز گریه کسی در پی فریاد نیامد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو، به ستم جان ده و انصاف مجو، زآنک</p></div>
<div class="m2"><p>در مذهب خوبان روش داد نیامد</p></div></div>