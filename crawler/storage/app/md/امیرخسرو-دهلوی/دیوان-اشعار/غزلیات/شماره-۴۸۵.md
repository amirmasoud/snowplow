---
title: >-
    شمارهٔ ۴۸۵
---
# شمارهٔ ۴۸۵

<div class="b" id="bn1"><div class="m1"><p>زلفین تو سرگشته چو باد سحرم کرد</p></div>
<div class="m2"><p>خاک سر کویت چو صبا دربدرم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خود ز تو دیوانه مطلق شده بودم</p></div>
<div class="m2"><p>زنجیر سر زلف تو دیوانه ترم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم به من افگن نظری، چشم ببستی</p></div>
<div class="m2"><p>تا چشم خوشت بسته آن یک نظرم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندر نظرم داشت خیال تو و اشکم</p></div>
<div class="m2"><p>سر تا قدم آلوده خون جگرم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بفروخت مرا بر کف اندیشه خیالت</p></div>
<div class="m2"><p>من اینقدر ارزم که خیال تو کرم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسوده دلی داشتم و بی خبر از عشق</p></div>
<div class="m2"><p>ناگاه در آمد غم تو بیخبرم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو طلب وصل تو می کرد که هجرت</p></div>
<div class="m2"><p>زین جای حوالت به سرای دگرم کرد</p></div></div>