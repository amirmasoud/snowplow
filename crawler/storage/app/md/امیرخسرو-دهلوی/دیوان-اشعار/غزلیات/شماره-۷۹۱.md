---
title: >-
    شمارهٔ ۷۹۱
---
# شمارهٔ ۷۹۱

<div class="b" id="bn1"><div class="m1"><p>شکل موزونت که در دل جا کند</p></div>
<div class="m2"><p>هر که بیند در جهان، سودا کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با قدت بر جا نماند پای سرو</p></div>
<div class="m2"><p>باغبانش گر چه پا بر جاکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسخه ای از روی تو نتوان ستد</p></div>
<div class="m2"><p>گر علم سر زیر پا بالا کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق زلفین مشک آلود تست</p></div>
<div class="m2"><p>باد کز گل عنبر سارا کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راز می ترسم که در صحرا نهد</p></div>
<div class="m2"><p>اشک من چون روی در صحرا کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب چشمم از ستادن فارغ است</p></div>
<div class="m2"><p>باد اگر زنجیرش اندر پا کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چند در خود دیدن، آخر فرصتی</p></div>
<div class="m2"><p>چشم را، تا یک نظر در ما کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جرعه کز جام لبت بیرون فتد</p></div>
<div class="m2"><p>عاشقان را بیخود و شیدا کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون که از مستی بغلتد چشم تو</p></div>
<div class="m2"><p>تکیه بر لطف شه والا کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز آفتاب تیغ او دشمن به رزم</p></div>
<div class="m2"><p>گونه گونه رنگ چون خرما کند</p></div></div>