---
title: >-
    شمارهٔ ۱۰۴۱
---
# شمارهٔ ۱۰۴۱

<div class="b" id="bn1"><div class="m1"><p>ترکی که جست و جوی دل من جز او نبود</p></div>
<div class="m2"><p>او را دلی نبود که در جست و جو نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامن کشید از من خاکی بسان گل</p></div>
<div class="m2"><p>گویی کش از بهار وفا هیچ بو نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمشیر مهر زد به من بی دل و برید</p></div>
<div class="m2"><p>شمشیر نیک بود، بریدن نکو نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بفریفت مر مرا به سخنهای دلفریب</p></div>
<div class="m2"><p>ورنه دل مرا سر هر گفت و گو نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حیرتم که یارب، از او بود این کرم</p></div>
<div class="m2"><p>با خود به جای او دگری بود، او نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با او نبود آنک چنانها همی نمود</p></div>
<div class="m2"><p>با آنک می نمود چنانها جز او نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو بساز با شب تنهائی فراق</p></div>
<div class="m2"><p>گر گویمت که شمع کجا رفت، کاو نبود</p></div></div>