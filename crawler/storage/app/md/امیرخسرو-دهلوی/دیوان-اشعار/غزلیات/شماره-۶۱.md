---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>بشکافت غم این جان جگرخواره ما را</p></div>
<div class="m2"><p>یا رب، چه وبال آمده سیاره ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتند رفیقان دل صد پاره ببردند</p></div>
<div class="m2"><p>کردند رها دامن صد پاره ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر همره ایشان روی، ای باد، در آن راه</p></div>
<div class="m2"><p>زنهار بجویی دل آواره ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبها به دل از سوز جگر می کشدم آه</p></div>
<div class="m2"><p>آه ار خبرستی بت عیاره ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی نکند یاد که شبهای جدایی</p></div>
<div class="m2"><p>چون می گذرد عاشق بیچاره ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوی جگر سوخته بگرفت همه کوی</p></div>
<div class="m2"><p>آتش بزن این کلبه خونخواره ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیدند سر شکم همه همسایه و گفتند</p></div>
<div class="m2"><p>این سیل عجب گر نبرد خانه ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز خسته و افگار نخواهد دل خسرو</p></div>
<div class="m2"><p>خویی ست بدین بخت ستمگاره ما را</p></div></div>