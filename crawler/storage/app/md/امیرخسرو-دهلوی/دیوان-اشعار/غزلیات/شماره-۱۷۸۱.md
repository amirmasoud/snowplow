---
title: >-
    شمارهٔ ۱۷۸۱
---
# شمارهٔ ۱۷۸۱

<div class="b" id="bn1"><div class="m1"><p>ای که در هیچ غمی با دل من یار نه ای</p></div>
<div class="m2"><p>سوی من بین، اگر اندر سر آزار نه ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تو هر روز گرفتار بلایی گردم</p></div>
<div class="m2"><p>تو چه دانی که در این روز گرفتار نه ای؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر شب از ناله من خواب نیاید کس را</p></div>
<div class="m2"><p>خفته ای تو که در این واقعه بیدار نه ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با من خسته کم رویم ز تو در دیوارست</p></div>
<div class="m2"><p>می کن آخر سخنی، صورت دیوار نه ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نار دانی ز دو لب بر من بیمار فرست</p></div>
<div class="m2"><p>شکر آن را که چو من در هم و بیمار نه ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از برای دل من جان من امروز ببر</p></div>
<div class="m2"><p>گر چه عهدی ست به دنباله این کار نه ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار بنشست مرا در دل و من می دانم و او</p></div>
<div class="m2"><p>خسروا، خیز که تو محرم اسرار نه ای</p></div></div>