---
title: >-
    شمارهٔ ۹۳۲
---
# شمارهٔ ۹۳۲

<div class="b" id="bn1"><div class="m1"><p>دلی که نرگس مستش به ناز بستاند</p></div>
<div class="m2"><p>کراست زهره کز آن حیله ساز بستاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهی نواله شیرین دهان آن کس را</p></div>
<div class="m2"><p>که چاشنی خود ازان لب به گاز بستاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببرد جانم و ای کاشکی که ندهد باز</p></div>
<div class="m2"><p>نداد بوسه و یارب که باز بستاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشا جوانی و مستی من دران ساعت</p></div>
<div class="m2"><p>که من پیاله دهم، او به ناز بستاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیال برد صلاح مرا که روزی او</p></div>
<div class="m2"><p>مرا ز خویشتن اندر نماز بستاند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر آستانش برم آب دیده را به نیاز</p></div>
<div class="m2"><p>مگر که تحفه اهل نیاز بستاند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی که دل ز خم زلف او برون آرد</p></div>
<div class="m2"><p>کبوتری ست که از چنگ باز بستاند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم فروشد صد جان که تار مویش را</p></div>
<div class="m2"><p>ز بهر مایه عمر دراز بستاند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قوی دلی که به معشوق او سپر سازد</p></div>
<div class="m2"><p>نکو دلی که ز محمود ایاز بستاند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسوخت خسرو و در آتش غمت بگداخت</p></div>
<div class="m2"><p>مراد از تو به سوز و گداز بستاند</p></div></div>