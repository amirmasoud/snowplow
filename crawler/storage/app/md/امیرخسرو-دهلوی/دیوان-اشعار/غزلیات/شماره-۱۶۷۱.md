---
title: >-
    شمارهٔ ۱۶۷۱
---
# شمارهٔ ۱۶۷۱

<div class="b" id="bn1"><div class="m1"><p>پرده صبرم درید غمزه دلدوز تو</p></div>
<div class="m2"><p>زهره من آب کرد عشق جهانسوز تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من که سحر هر شبی دم نزنم تا به صبح</p></div>
<div class="m2"><p>ترسم روشن شود مهر دل افروز تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنگ گل عارضت روز به روز است نو</p></div>
<div class="m2"><p>خارکشی را چه رنگ از گل نوروز تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هندوی چشم ترا غارت ترکان چین</p></div>
<div class="m2"><p>نیکویی آموخته است زلف بدآموز تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا تو بر اهل صواب تیر زنی بی خطا</p></div>
<div class="m2"><p>هست کمان بلند ابروی کین توز تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خسرو بیچاره کرد وقف هوای تو دل</p></div>
<div class="m2"><p>گر چه پی جانست گرد غمزه دلدوز تو</p></div></div>