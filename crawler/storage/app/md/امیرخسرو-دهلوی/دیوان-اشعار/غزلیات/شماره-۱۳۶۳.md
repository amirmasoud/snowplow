---
title: >-
    شمارهٔ ۱۳۶۳
---
# شمارهٔ ۱۳۶۳

<div class="b" id="bn1"><div class="m1"><p>خرم آن روز که من آن رخ زیبا بینم</p></div>
<div class="m2"><p>او کند ناز و من از دور تماشا بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوش مه دیدم و گفتم که ترا می ماند</p></div>
<div class="m2"><p>زهره ام نیست ازین شرم که بالا بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لشکر جانش که پیراهن دلها گویی</p></div>
<div class="m2"><p>بس منش خواهم از اغیار که تنها بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل من گاه خرامیدنش از دست برفت</p></div>
<div class="m2"><p>هر کجا پای نهاده ست من آنجا بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل نه و صبر نه و هوش نه و طاقت نه</p></div>
<div class="m2"><p>من در آن صورت زیبا به چه یارا بینم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وعده فرداست به فردا بکشم، من، مگر از آنک</p></div>
<div class="m2"><p>بامدادان رخ شهزاده والا بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمس آفاق خضر خان که به لطف جان بخش</p></div>
<div class="m2"><p>هر دمش معجزه خضر و مسیحا بینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آخر، ای شاخ گل تازه نوبر، تا چند</p></div>
<div class="m2"><p>خار حسرت خورم و جانب خرما بینم؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کیست خسرو که کند بوسه ز پای تو هوس؟</p></div>
<div class="m2"><p>این بسم نیست که از دور در آن پابینم</p></div></div>