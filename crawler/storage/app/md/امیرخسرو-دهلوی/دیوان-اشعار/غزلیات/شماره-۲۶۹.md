---
title: >-
    شمارهٔ ۲۶۹
---
# شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>نرگس مست تو خواب آلوده است</p></div>
<div class="m2"><p>لب لعل تو شراب آلوده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آگه از ناله من کی گردد</p></div>
<div class="m2"><p>چشم مست تو که خواب آلوده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خویی کز عارض تو باز شده ست</p></div>
<div class="m2"><p>برگ گل را به گلاب آلوده ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب تو در دل من بنشسته ست</p></div>
<div class="m2"><p>نمکی را به کباب آلوده ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازتری خواست چکیدن آری</p></div>
<div class="m2"><p>لب تو کز می ناب آلوده ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن تلخ تو زان شیرینست</p></div>
<div class="m2"><p>که شکر را به جواب آلوده ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنده خسرو چه گنه کرد امروز</p></div>
<div class="m2"><p>که حدیثت به عتاب آلوده ست</p></div></div>