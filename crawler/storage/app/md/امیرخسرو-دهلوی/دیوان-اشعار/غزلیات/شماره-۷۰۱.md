---
title: >-
    شمارهٔ ۷۰۱
---
# شمارهٔ ۷۰۱

<div class="b" id="bn1"><div class="m1"><p>دوش ناگه به من دلشده آن مه برسید</p></div>
<div class="m2"><p>دل به مقصود خود المنت لله برسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز می گفتمی افسانه هجران با خویش</p></div>
<div class="m2"><p>تا بدان لحظه که بالای سرم مه برسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پی کوری آن کس که نیارد دیدن</p></div>
<div class="m2"><p>مژده نور بصر بر من آگه برسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمد آن روشنی چشم به استقبالش</p></div>
<div class="m2"><p>مردم دیده روان تا به سر ره برسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آمد آن ساده زنخ، بر من بیهوش زد آب</p></div>
<div class="m2"><p>بر من تشنه نگه کن که چسان چه برسید؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گریه بر سوز منش آمده بر سوختگان</p></div>
<div class="m2"><p>آن چه باران کرم بود که ناگه برسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل ستد از من بیمار و به پرسش نامد</p></div>
<div class="m2"><p>چون خبر یافت که جان می دهم، آنگه برسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می کشیدم سر زلفش ز قفا جانب روی</p></div>
<div class="m2"><p>تا شب تار به نزدیک سحرگه برسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسروا، گر رسد ابله به بهشتی چه عجب؟</p></div>
<div class="m2"><p>عجب این بین که بهشتی سوی ابله برسید</p></div></div>