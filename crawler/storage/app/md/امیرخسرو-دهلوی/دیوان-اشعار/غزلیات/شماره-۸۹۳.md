---
title: >-
    شمارهٔ ۸۹۳
---
# شمارهٔ ۸۹۳

<div class="b" id="bn1"><div class="m1"><p>دلم ز دست برفته ست و پیش باز نیابد</p></div>
<div class="m2"><p>نوازشی هم از آن یار دلنواز نیاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمام عرصه عالم سپاه فتنه بگیرد</p></div>
<div class="m2"><p>اگر ز عارض یارم خط جواز نیاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درید پرده، فرو ریخت راز دل بر صحرا</p></div>
<div class="m2"><p>ز پرده ای که چنین شد حجاب راز نیاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بتا به ناز بکشتی هزار صاحب دل را</p></div>
<div class="m2"><p>کسی به پیش تو میرد که گاه ناز نیاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو خاک پای تو گشتم بگو که در ته پایت</p></div>
<div class="m2"><p>به خاک روفتن آن گیسوی دراز نیاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرم بگویی «بوسه بزن بر آن لب شیرین »</p></div>
<div class="m2"><p>مرا ز غایت شادی دهن فراز نیاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر به باغ رسد قامت بلند تو روزی</p></div>
<div class="m2"><p>عجب بود که اگر سرو در نماز نیاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دهند پند که بازآ، من آن مجال ندارم</p></div>
<div class="m2"><p>که هر که رفت به کویت به خانه باز نیاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان بسوخت حدیث نیازمندی خسرو</p></div>
<div class="m2"><p>خنک بود سخنی کز سر نیاز نیاید</p></div></div>