---
title: >-
    شمارهٔ ۷۴۸
---
# شمارهٔ ۷۴۸

<div class="b" id="bn1"><div class="m1"><p>گر خم طره ز روی تو جدا خواهد شد</p></div>
<div class="m2"><p>نام رخساره تو نام سما خواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جعد زنجیر نمای تو بلایی ست کز او</p></div>
<div class="m2"><p>پای دل بسته به زنجیر بلا خواهد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف هم چون رسنت ماه سما را بگرفت</p></div>
<div class="m2"><p>من ندانم که درین ماه چها خواهد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حاجت آن است که من بر در تو کشته شوم</p></div>
<div class="m2"><p>هیچگه حاجت این خسته روا خواهد شد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین کشاکش که تنت راست ببینی، خسرو</p></div>
<div class="m2"><p>ناگهان بند ز بند تو جدا خواهد شد</p></div></div>