---
title: >-
    شمارهٔ ۱۲۱۵
---
# شمارهٔ ۱۲۱۵

<div class="b" id="bn1"><div class="m1"><p>رسید دوش ندایی ازین بلند رواق</p></div>
<div class="m2"><p>که، ای مقیم زوایای شهربند فراق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین حضیض چرا گشته ای چنین محبوس؟</p></div>
<div class="m2"><p>گذر چو طایر قدسی از اوج این نه طاق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مناققند و ریای جمیع اهل بشر</p></div>
<div class="m2"><p>بیا به صحبت یاران بی ریا و نفاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا به روز ازل با حبیب عهدی بود</p></div>
<div class="m2"><p>چه آمدت که فراموش کرده ای میثاق؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرو به قول مخالف به هرزه راه حجاز</p></div>
<div class="m2"><p>وگرنه راه نیابی به پرده عشاق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی که مسکن اصلیش عالم علویست</p></div>
<div class="m2"><p>چه می کند به خراسان، چه می رود به عراق؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خویش بگذر و باز آی سوی ما، خسرو</p></div>
<div class="m2"><p>که نیست خوش تر از این جای در همه آفاق</p></div></div>