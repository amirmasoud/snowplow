---
title: >-
    شمارهٔ ۶۲۹
---
# شمارهٔ ۶۲۹

<div class="b" id="bn1"><div class="m1"><p>چند گاهی دگر ار چشم تو در ناز بماند</p></div>
<div class="m2"><p>ای بسا دل که در آن طره طناز بماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کعبتینی تو که غلتانی ازان چشم مقامر</p></div>
<div class="m2"><p>ای بسا سیل کز آن چشم روان باز بماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاتم اندر دهن انگشت بگیرد ز دهانت</p></div>
<div class="m2"><p>ور دهانش ار کشی انگشت دهان باز بماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی تو دیدم و خط دود رسانید به چشمت</p></div>
<div class="m2"><p>ترسم آن دود به دنباله غماز بماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زر ندارم ز پی وصل، تنی دارم چون زر</p></div>
<div class="m2"><p>لیکن آن تیر به دندان به ته گاز بماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نازکم کن که نکویی به کسی دیر نماند</p></div>
<div class="m2"><p>زشت باشد که نکویی برود ناز بماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل خسرو به جفا سوختی و راز برون شد</p></div>
<div class="m2"><p>پرده دل چو بسوزد ز کجا راز بماند؟</p></div></div>