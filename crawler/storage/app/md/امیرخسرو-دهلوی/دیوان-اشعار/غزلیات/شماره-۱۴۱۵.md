---
title: >-
    شمارهٔ ۱۴۱۵
---
# شمارهٔ ۱۴۱۵

<div class="b" id="bn1"><div class="m1"><p>نی پای آن که از سر کویت سفر کنم</p></div>
<div class="m2"><p>نی دست آنکه دست به زلف تو در کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندین شبم گذشت به کنج خراب خویش</p></div>
<div class="m2"><p>ممکن نشد که لوح صبوری ز بر کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماهی متاع صبر کنم جمع و ز آب چشم</p></div>
<div class="m2"><p>در مجلس خیال تو یک روزتر کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوابم نماند و خواب اجل هم خوش است، لیک</p></div>
<div class="m2"><p>گر خشتی ز آستانه تو زیر سر کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمری گذشت، هیچ نیامد زمان آنک</p></div>
<div class="m2"><p>روزی به روی تو شب غم را سحر کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذوق جفا و جور تو بر من حرام باد</p></div>
<div class="m2"><p>گر من به جز وفای تو کاری دگر کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمت به خواب ناز و مرا قصه ای دراز</p></div>
<div class="m2"><p>آمد شبم به روز، سخن مختصر کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کس به سوی حور رود، من به سوی تو</p></div>
<div class="m2"><p>چون بامداد حشر سر از خواب بر کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روزی گذشته بود برای سوار و من</p></div>
<div class="m2"><p>هر بامداد آیم و آن سو نظر کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دردش به از سر است و من سر بریده را</p></div>
<div class="m2"><p>آن سر کجا که در سر آن درد سر کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یاران ز پند بس که ز خسرو رها نشد</p></div>
<div class="m2"><p>آن دل که پیش تیر ملامت سپر کنم</p></div></div>