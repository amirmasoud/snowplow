---
title: >-
    شمارهٔ ۱۹۵۰
---
# شمارهٔ ۱۹۵۰

<div class="b" id="bn1"><div class="m1"><p>یک ره بکن ز غمزه خونین اشارتی</p></div>
<div class="m2"><p>کافتد ز فتنه در همه آفاق غارتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندین به شهر دزدی دلها کجا شود؟</p></div>
<div class="m2"><p>در دیده گر ز چشم تو نبود اشارتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن را که می کشی، به ازین نیست خونبهاش</p></div>
<div class="m2"><p>از سر کنیش زنده، گر آیی زیارتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بی رخت عمارت عمرم کند سپهر</p></div>
<div class="m2"><p>بادا خراب، یارب ازینسان عمارتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویند دوست وعده به شمشیر می کند</p></div>
<div class="m2"><p>آن بخت کو که یابم از ایشان بشارتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من وصف آن جمال چگونه کنم که هیچ</p></div>
<div class="m2"><p>فیروزمند نیست بر آنم عبارتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق آتش است، خسرو، اگر سوزدت مرنج</p></div>
<div class="m2"><p>دانی که آتشی نبود بی حرارتی</p></div></div>