---
title: >-
    شمارهٔ ۱۸۷۷
---
# شمارهٔ ۱۸۷۷

<div class="b" id="bn1"><div class="m1"><p>ای یار پرنمک، جگرم ریش می‌کنی</p></div>
<div class="m2"><p>قصد هلاک سوخته خویش می‌کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دیده شرم دار، گرت بیم آه نیست</p></div>
<div class="m2"><p>بی‌موجبی چرا دل من ریش می‌کنی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آخر کجا روا بود، ای ناخدای ترس</p></div>
<div class="m2"><p>این سلطنت که با من درویش می‌کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای آنکه پند می‌دهیم از برای عشق</p></div>
<div class="m2"><p>چندین مدم که آتش من بیش می‌کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانا، ز طعنه کشته شدم، کاین دل مرا</p></div>
<div class="m2"><p>آماج تیر دشمن بدکیش می‌کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشمت به خواب می‌رود، آن مست را بگوی</p></div>
<div class="m2"><p>آخر چه کرده‌ایم که در پیش می‌کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جوری که می‌کنی تو، مرا آن نمی‌کشد</p></div>
<div class="m2"><p>این می‌کشد که پیش بداندیش می‌کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بوسه خواهم از مژه، گویی جواب تلخ</p></div>
<div class="m2"><p>بوسه مده، چرا سخن از نیش می‌کنی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو به آرزو چو خیالت به جان خرید</p></div>
<div class="m2"><p>در کار او هنوز چه فرویش می‌کنی؟</p></div></div>