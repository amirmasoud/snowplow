---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>رسید باد صبا تازه کرد جان مرا</p></div>
<div class="m2"><p>نهفته داد به من بوی دلستان مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخفت نرگس و فریاد کم کن، ای بلبل</p></div>
<div class="m2"><p>کنون که خواب گرفته است ناتوان مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبا سواد چمن زا چو نسخه کرد بر آب</p></div>
<div class="m2"><p>به گل نمود که بنگر خط روان مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا گذر به گلستان بس است، لیک چه سود</p></div>
<div class="m2"><p>که سوی من گذری نیست گلستان مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گمان همی بردم کز فراق او بزیم</p></div>
<div class="m2"><p>غم نهفته یقین می کند گمان مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشان نماند ز نقشم، کجاست عارض او</p></div>
<div class="m2"><p>که در کشد قلم این نقش بی نشان مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فغان من ز کجا بشنود به گوش آن شوخ</p></div>
<div class="m2"><p>که خود نمی شنود گوش من فغان مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرید جانب او مرغ روح و با من گفت</p></div>
<div class="m2"><p>که من شدم، تو نگهدار آشیان مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش آن دمی که در آید سپیده دم ز درم</p></div>
<div class="m2"><p>پر از ستاره و مه ساخت خانمان مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرم برید و به دستم نهاد و راه نمود</p></div>
<div class="m2"><p>که خیز و زو سر خود گیر و بخش جان مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نهاد بر لب من لب، نماند جای سخن</p></div>
<div class="m2"><p>که مهر کرد به انگشتری دهان مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رو، ای صبا و بگو سرو رفته را، باز آی</p></div>
<div class="m2"><p>به نوبهار بدل کن یکی خزان مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اسیر زلف ویم با خودم ببر، ای باد</p></div>
<div class="m2"><p>وگرنه زاغ برد با تو استخوان مرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز رفتن تو به جان آمدم، نمی دانم</p></div>
<div class="m2"><p>که رفتنت ز کجا خاست بهر جان مرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل شکسته خسرو به جانب تو شتافت</p></div>
<div class="m2"><p>غریب نیست، نگهدار میهمان مرا</p></div></div>