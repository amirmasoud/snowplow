---
title: >-
    شمارهٔ ۷۱۶
---
# شمارهٔ ۷۱۶

<div class="b" id="bn1"><div class="m1"><p>هر شکر خنده که آن لعل شکر خنده کند</p></div>
<div class="m2"><p>بر دل زیرک و بر جان خردمند کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف ازان می برد آن شوخ که شبهای غمم</p></div>
<div class="m2"><p>گر شود کوته، از آنجا همه پیوند کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن خیال است که آیینه نماید چو تویی</p></div>
<div class="m2"><p>آینه ماه شما را به که مانند کند؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیم شب ز آتش دل روز کنم در تو، ولی</p></div>
<div class="m2"><p>دل چه داند که چنین روز شبی چند کند؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیسوی پر گرهت رشته بت را ماند</p></div>
<div class="m2"><p>که دل گرم من سوخته را بند کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون وفا نیست ترا، خسرو مسکین چه کند؟</p></div>
<div class="m2"><p>دل ضرورت به جفاهای تو خرسند کند</p></div></div>