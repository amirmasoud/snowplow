---
title: >-
    شمارهٔ ۱۴۲۹
---
# شمارهٔ ۱۴۲۹

<div class="b" id="bn1"><div class="m1"><p>دلم ز دست تو خون شد، ندانم این به که گویم؟</p></div>
<div class="m2"><p>علاج خود ز که سازم، دوای دل ز که جویم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بریخت اشک من آن را که پاره گشت دروغم</p></div>
<div class="m2"><p>برفت آب من آن را که رخنه گشت سبویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از این دو دیده پر آب من که ریخته بادا</p></div>
<div class="m2"><p>چه آب ریختگی ها که آمده ست به رویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رهی به کوی تو جویم که گویمت سخن خود</p></div>
<div class="m2"><p>تو سوی خود ندهی ره، ندانم این به که گویم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تویی چو چشمه آب حیات و من به تو تشنه</p></div>
<div class="m2"><p>نخورده شربتی، آخر چگونه دست بشویم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میار طره فراهم، فرو گذار که بر من</p></div>
<div class="m2"><p>کند هر آنچه بیاید، چو می بیاید از اویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تن چو موی مرا بگسل و بسوز در آتش</p></div>
<div class="m2"><p>که پی گسست در آمد غمت به شخص چو مویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تبسمی که تو آنجا نه دلبری، گل باغی</p></div>
<div class="m2"><p>نوازشی که من اینجا، نه خسروم، سگ کویم</p></div></div>