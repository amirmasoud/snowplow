---
title: >-
    شمارهٔ ۱۷۲۹
---
# شمارهٔ ۱۷۲۹

<div class="b" id="bn1"><div class="m1"><p>نوبهار است و چمن جلوه جوزا کرده</p></div>
<div class="m2"><p>ابرها ریختنی لؤلؤی لالا کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گره طره سنبل ز صبا جستم، گفت</p></div>
<div class="m2"><p>«دامن لاله پر از عنبر سارا کرده »</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر گل و لاله تر می رود و نیک ببین</p></div>
<div class="m2"><p>پای آلوده به خون پایچه بالا کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقان رفته به گلزار و دل سوخته را</p></div>
<div class="m2"><p>به تکلف ز گل و لاله شکیبا کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که را بر جگر از فتنه خوبان داغی ست</p></div>
<div class="m2"><p>من هم از گل گله ای از رخ زیبا کرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داشته چشم به نرگس بر هر گل که رسید</p></div>
<div class="m2"><p>به هوس دیده خویشش به ته پا کرده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می شنودی که گل و لاله به باغ و نرگس</p></div>
<div class="m2"><p>مطربان را به نوا بلبل گویا کرده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس از این ما و شراب و چمن و مشتی چند</p></div>
<div class="m2"><p>دل و دین را به سر شاهد و صهبا کرده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنده خسرو ز شکر ریزی و صفت هر روز</p></div>
<div class="m2"><p>کلک خود را به دو دندانه شکرخا کرده</p></div></div>