---
title: >-
    شمارهٔ ۱۰۱۹
---
# شمارهٔ ۱۰۱۹

<div class="b" id="bn1"><div class="m1"><p>چو صبح از روی نورانی نقاب تار بگشاید</p></div>
<div class="m2"><p>نسیم از هر طرف صد نافه تاتار بگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نباشد حاجت مطرب حریفان صبوحی را</p></div>
<div class="m2"><p>چو مرغ صبحگاهی ناله های زار بگشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش آن عاشق که خوابش برده باشد در پس عمری</p></div>
<div class="m2"><p>چو خیزد ناگهان، دیده به روی یار بگشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غلام خواب آن شوخم کز آواز خوش ساقی</p></div>
<div class="m2"><p>به صد ناز و کرشمه نرگس بیمار بگشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلت نگشاید، الا با لب و روی بتان، خسرو</p></div>
<div class="m2"><p>دل هر کس، ولی از سبزه گلزار بگشاید</p></div></div>