---
title: >-
    شمارهٔ ۹۵۰
---
# شمارهٔ ۹۵۰

<div class="b" id="bn1"><div class="m1"><p>جوان و پیر که در بند مال و فرزندند</p></div>
<div class="m2"><p>نه عاقلند که طفلان ناخردمند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جماعتی که بگریند بهر عیش و منال</p></div>
<div class="m2"><p>یقین بدان تو که بر خویشتن همی خندند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش آن کسان که برفتند پاک چون خورشید</p></div>
<div class="m2"><p>که سایه ای به سر این جهان نیفگندند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خانه ای که ره جان نمی توان بستن</p></div>
<div class="m2"><p>چه ابلهند کسانی که دل همی بندند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سبزه زار فلک طرفه باغبانانند</p></div>
<div class="m2"><p>که هر نهال که شاندند باز برکندند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمال طلعت هم صحبتان غنیمت دان</p></div>
<div class="m2"><p>که می روند نه زانسان که باز پیوندند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بقا که نیست درو حاصلی، همه هیچ است</p></div>
<div class="m2"><p>چو بنگری همه مردم به هیچ خرسندند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بساز توشه ز بهر مسافران وجود</p></div>
<div class="m2"><p>که میهمان عزیزند و روزکی چندند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر تو آدمیی، در کسان به طنز مبین</p></div>
<div class="m2"><p>که بهتر از من و تو بنده خداوندند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترا به از عمل خیر نیست فرزندی</p></div>
<div class="m2"><p>که دشمنند ترا زادگان نه فرزندند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مجوی دنیا، اگر اهل همتی، خسرو</p></div>
<div class="m2"><p>که از همای به مردار میل نپسندند</p></div></div>