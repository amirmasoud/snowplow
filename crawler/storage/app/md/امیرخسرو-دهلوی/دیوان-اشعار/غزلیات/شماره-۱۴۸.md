---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>مجو صبرم که جای آن نمانده ست</p></div>
<div class="m2"><p>مران از در که پای آن نمانده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مبین در سجده های زرقم، ای بت</p></div>
<div class="m2"><p>که این طاعت سزای آن نمانده ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببوسم پای بت را وان نیرزد</p></div>
<div class="m2"><p>که در سینه صفای آن نمانده ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلی دارم که مانده ست از پی عشق</p></div>
<div class="m2"><p>خرد جویی، برای آن نمانده ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا، بگذار جان بدهم در این کوی</p></div>
<div class="m2"><p>که هنگام دوای آن نمانده ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خموش، ای پندگو، چون من نماندم</p></div>
<div class="m2"><p>ز من بگذر که جای آن نمانده ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسان در باغ و من در گوشه غم</p></div>
<div class="m2"><p>که خسرو را هوای آن نمانده ست</p></div></div>