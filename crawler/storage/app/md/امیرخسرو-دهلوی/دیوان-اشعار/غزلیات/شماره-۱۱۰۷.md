---
title: >-
    شمارهٔ ۱۱۰۷
---
# شمارهٔ ۱۱۰۷

<div class="b" id="bn1"><div class="m1"><p>نه نرگس است ز چشم خوش تو عربده جوتر</p></div>
<div class="m2"><p>نه سنبل است ز زلف کج تو غالیه بوتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه سوخت مرا هجر خام و وعده رویت</p></div>
<div class="m2"><p>خوشم که دوزخ نقد از بهشت نسیه نکوتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از قضاست که میرم به بند سلسله مویان</p></div>
<div class="m2"><p>بیا که نیست کس از تو به دهر سلسله موتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سخت چشمی یاران کشی همیشه چو ترکی</p></div>
<div class="m2"><p>که از گروهه سنگین کند شکار کبوتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرابم ار ندهی تیغ ران به خلق که باری</p></div>
<div class="m2"><p>ز دولت تو کنم ازان دگر شراب گلو تر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مبین که مایه دیوانگیست عشق تو، این بین</p></div>
<div class="m2"><p>که عقل اولین از وی پیاده ایست فروتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرت بگوید از آن منی مرنج ز خسرو</p></div>
<div class="m2"><p>که نیست زو کسی اندر زمانه بیهده گوتر</p></div></div>