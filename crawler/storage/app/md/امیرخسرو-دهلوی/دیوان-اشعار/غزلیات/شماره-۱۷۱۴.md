---
title: >-
    شمارهٔ ۱۷۱۴
---
# شمارهٔ ۱۷۱۴

<div class="b" id="bn1"><div class="m1"><p>ای آرزوی دل شکسته</p></div>
<div class="m2"><p>ما در دل تو شکسته بسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس دل که به دولت فراقت</p></div>
<div class="m2"><p>از ننگ حیات باز رسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجروح لبت بسی ست، کس دید</p></div>
<div class="m2"><p>یک خرما را هزار هسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل کوفته من چو آهن سرد</p></div>
<div class="m2"><p>زان گونه که صد شرار جسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سروت چو برای جان ما خاست</p></div>
<div class="m2"><p>برخاسته و به جان نشسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندوه من ار نهند بر کوه</p></div>
<div class="m2"><p>که را بینی کمر شکسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر خسرو غمزه ای تمام است</p></div>
<div class="m2"><p>شمشیر چرا زنی دو دسته؟</p></div></div>