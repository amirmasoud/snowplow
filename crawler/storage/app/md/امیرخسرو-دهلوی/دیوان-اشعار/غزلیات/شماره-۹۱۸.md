---
title: >-
    شمارهٔ ۹۱۸
---
# شمارهٔ ۹۱۸

<div class="b" id="bn1"><div class="m1"><p>لبالب آر قدح کز گلو فرود آید</p></div>
<div class="m2"><p>مگر که از دلم این آرزو فرود آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگوی تو به که آید فرود می ز سرم</p></div>
<div class="m2"><p>مباد کز سر من این سبو فرود آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز می چه توبه که گر ذوق آن کند معلوم</p></div>
<div class="m2"><p>فرشته چون مگس آنجا به بو فرود آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بند مردنم امروز، ساقیا، بگذار</p></div>
<div class="m2"><p>که باده از سر آن ماهر و فرود آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زهد تخته ورد و دعای من باشد</p></div>
<div class="m2"><p>سفال خم که خط می برو فرود آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بهر بردن دلهای خلق سیل بلاست</p></div>
<div class="m2"><p>هر آن عرق که ز روی نکو فرود آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدین صفت که همی خون خوریم بر در تو</p></div>
<div class="m2"><p>ترا چگونه می اندر گلو فرود آید؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوش آن زمان که به یاد تو هر شبم تا روز</p></div>
<div class="m2"><p>ز دیده خون جگر سو به سو فرود آید؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نقاب واکن و لبهای عاشقان دربند</p></div>
<div class="m2"><p>مگر که خسرو ازین گفتگو فرود آید</p></div></div>