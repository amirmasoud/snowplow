---
title: >-
    شمارهٔ ۶۴۱
---
# شمارهٔ ۶۴۱

<div class="b" id="bn1"><div class="m1"><p>خوبرویان چون به سلطانی علم بالا کشند</p></div>
<div class="m2"><p>شیر مردان را به زیر تیغ جانفرسا کشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان کنان شب زنده دارند اهل عشق و در سخن</p></div>
<div class="m2"><p>صبح وار از آفتاب خود دمی بالا کشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیر عاشق پیشه ام، به کاین مصلای مرا</p></div>
<div class="m2"><p>خدمتی را زیر پای شاهد رعنا کشند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسکه از رفتار خوش پای تو در جانم نشست</p></div>
<div class="m2"><p>رخنه گردد جانم، ار خار ترا از پا کشند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کرشمه لام الف کن زلف را بالای خویش</p></div>
<div class="m2"><p>تا از آن بر نام هر مهروی نام لاکشند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصل من این بس که خون من بریزند و ز خون</p></div>
<div class="m2"><p>نقش من با نقش آن صورتگران یکجا کشند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با وجود خویشتن ما را دویی باشد، لیک</p></div>
<div class="m2"><p>باک نبود گر کسان اره به فرق ما کشند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسته حال خسرو از شیرینی عیش و نشاط</p></div>
<div class="m2"><p>برکشیدی راست همچون هسته کز خرما کشند</p></div></div>