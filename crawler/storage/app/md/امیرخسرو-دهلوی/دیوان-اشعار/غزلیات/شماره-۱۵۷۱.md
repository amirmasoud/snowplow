---
title: >-
    شمارهٔ ۱۵۷۱
---
# شمارهٔ ۱۵۷۱

<div class="b" id="bn1"><div class="m1"><p>یک ره ز در برون آ، قصد هزار جان کن</p></div>
<div class="m2"><p>قربان هزار چون من بر چشم ناتوان کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رویت بلاست، بنما، تا جان دهند خلقی</p></div>
<div class="m2"><p>در عهد خود ازینسان نرخ بلاگران کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دیدن تو مردم تا بزیم و نمیرم</p></div>
<div class="m2"><p>در شخص مرده من خود رابیار و جان کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نوک غمزه تا کی خونها کنی دمادم</p></div>
<div class="m2"><p>شهری بکشتی، اکنون شمشیر در میان کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کویش غم تو بگسست بند بندم</p></div>
<div class="m2"><p>یک جرعه ای میم ده پیوند استخوان کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از لب چو دیگرانم چون شکری ببخشی</p></div>
<div class="m2"><p>باری طفیل ایشان خاکی در این و آن کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر دل بری، توانی، ور جان بری ز من هم</p></div>
<div class="m2"><p>تسلیم تست خسرو خواه این و خواه آن کن</p></div></div>