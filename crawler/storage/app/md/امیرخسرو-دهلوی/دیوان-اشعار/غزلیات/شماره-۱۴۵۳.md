---
title: >-
    شمارهٔ ۱۴۵۳
---
# شمارهٔ ۱۴۵۳

<div class="b" id="bn1"><div class="m1"><p>همه شب از تو به دیوار خانه غم گویم</p></div>
<div class="m2"><p>فسانه گویم و با چشم پر زنم گویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو غنچه گشت دلم خون و قصه تو ز رشک</p></div>
<div class="m2"><p>دلم نخواست که با باد صبحدم گویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو خود یقینست که خوش گردی از غمم، لیکن</p></div>
<div class="m2"><p>کجاست دولت آنم که با تو غم گویم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش آن شبی که تو در خواب ناز باشی و من</p></div>
<div class="m2"><p>نیاز خویش بدان زلف خم به خم گویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سکون دل را گویم «فلان از آن من است »</p></div>
<div class="m2"><p>چنان اگر چه نباشد، دروغ هم گویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو آن که می دهیم پند، بگذر از سر من</p></div>
<div class="m2"><p>همان بس است که من درد خویش کم گویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدث جان دژم پرسدم همه کس و من</p></div>
<div class="m2"><p>همه حکایت آن نرگس دژم گویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مخوان به قبله ام، ای پارسا، روا داری</p></div>
<div class="m2"><p>که تو هوالله گویی و من صنم گویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرنج از شغب بی تکلف خسرو</p></div>
<div class="m2"><p>سرود نیست که او را به زیر و بم گویم</p></div></div>