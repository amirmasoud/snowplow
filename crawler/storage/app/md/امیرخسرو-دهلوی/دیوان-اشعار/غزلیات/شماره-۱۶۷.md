---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>تماشا گاه جانها شد خیالت</p></div>
<div class="m2"><p>تمناگاه دلها زلف و خالت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به غلطم بی خبر چون قرعه فال</p></div>
<div class="m2"><p>چو بینم طلعت فرخنده فالت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدارا این چشم من چون دلو پر آب</p></div>
<div class="m2"><p>که باشد آفتاب من و بالت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشارت کردی از ابرو به خونم</p></div>
<div class="m2"><p>مرا باری مبارک شد جمالت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه جان از لب درون آمد نه بیرون</p></div>
<div class="m2"><p>بلا شد عشق پا بوس خیالت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو خوش می می خوری از خون نابم</p></div>
<div class="m2"><p>اگر ننگی نیاید زین سفالت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو حالم شد پریشان بی تو آخر</p></div>
<div class="m2"><p>بگو آخر که خسرو چیست حالت</p></div></div>