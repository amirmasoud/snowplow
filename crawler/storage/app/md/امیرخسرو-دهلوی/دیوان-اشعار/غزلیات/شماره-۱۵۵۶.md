---
title: >-
    شمارهٔ ۱۵۵۶
---
# شمارهٔ ۱۵۵۶

<div class="b" id="bn1"><div class="m1"><p>نام گل بردن به پیشت بر زبان آید گران</p></div>
<div class="m2"><p>دم زدن بی یاد رویت از دهان آید گران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ترازوی دل ار سنجم ترا با جان خویش</p></div>
<div class="m2"><p>از لطافت تو سبک آیی و جان آید گران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابرویت در سینه ام بنشست و می لرزم ز بیم</p></div>
<div class="m2"><p>کاین چنین توزی بر آن زیبا کمان آید گران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بمیرم از غمت، روزی ندارم غم، جز آنک</p></div>
<div class="m2"><p>بر چنان خاک عزیزان استخوان آید گران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر خیالت برد جانم بر زبان نآرم، از آنک</p></div>
<div class="m2"><p>منت کم همتان برمیهمان آید گران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن گرانی دارم از غمهات با این لاغری</p></div>
<div class="m2"><p>سایه او بر زمین و آسمان آید گران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تنگ نآید عاشق، ار صد جور از جانان رسد</p></div>
<div class="m2"><p>گر بریزد ابر کی بر ناودان آید گران؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه موری گشتم از خواری گرانم بر همه</p></div>
<div class="m2"><p>بوالعجب موری که بر جمله جهان آید گران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه پند دوستان تلخ است، ای خسرو، نکوست</p></div>
<div class="m2"><p>کز طبیبان کن مکن بر ناتوان آید گران</p></div></div>