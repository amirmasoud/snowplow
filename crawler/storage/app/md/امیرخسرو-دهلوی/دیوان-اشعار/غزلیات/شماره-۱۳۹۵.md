---
title: >-
    شمارهٔ ۱۳۹۵
---
# شمارهٔ ۱۳۹۵

<div class="b" id="bn1"><div class="m1"><p>رفتیم ما و دل به یکی سو گذاشتیم</p></div>
<div class="m2"><p>جان خراب نیز همان سو گذاشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماییم و راه دوری و تا باز کی رسد؟</p></div>
<div class="m2"><p>جان و دلی که بر سر آن کو گذاشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذاشتیم روی عزیزی که سالها</p></div>
<div class="m2"><p>عمر عزیز خویش بر آن رو گذاشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن بخت کو که در خم بازو کشیم باز</p></div>
<div class="m2"><p>آن گردنی که از غم بازو گذاشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن دل که او ز ما سر مویی جدا نبود</p></div>
<div class="m2"><p>آویخته به حلقه آن مو گذاشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل بوی وصل داشت، کنون رنگ خون گرفت</p></div>
<div class="m2"><p>این رنگ از آن ما شد و آن بو گذاشتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر بار گفته ای که ز پهلوی من برو</p></div>
<div class="m2"><p>رفتیم اینک از تو و پهلو گذاشتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوبی که دل به صحبت یاران گرفته بود</p></div>
<div class="m2"><p>بگسست سلک صحبت و آن خو گذاشتیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین پس وفا ز عمر نجوییم، خسروا</p></div>
<div class="m2"><p>چون روی دوستان وفا جو گذاشتیم</p></div></div>