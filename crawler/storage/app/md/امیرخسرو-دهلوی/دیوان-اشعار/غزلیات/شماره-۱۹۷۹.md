---
title: >-
    شمارهٔ ۱۹۷۹
---
# شمارهٔ ۱۹۷۹

<div class="b" id="bn1"><div class="m1"><p>نوبهار آمد و بگذشت به شادی مه دی</p></div>
<div class="m2"><p>اینک اینک که سراپای گل و آتش وی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد ازین جامه لطیف و تنک و تر پوشند</p></div>
<div class="m2"><p>چو گل تازه بتان ختن و خلخ و ری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نازنینا، عرق از روی تو بر گل بچکید</p></div>
<div class="m2"><p>می ممزوج لبالب برسان پی بر پی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پاک کن خوی ز بنا گوش که این مردم چشم</p></div>
<div class="m2"><p>خون خود ریزد هر جا که بریزد ز تو خوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رو سوی آب و به یک خنده پر از شکر کن</p></div>
<div class="m2"><p>بر لب جوی به هر جا که روی روید نی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیز و گلگشت چمن کن که نمانده ست به راه</p></div>
<div class="m2"><p>چشم نرگس که ز تو زان ره بخرامی یک پی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون خسرو به قدح کن، اگرت می باید</p></div>
<div class="m2"><p>عاشق تست، مبادا که بگوید هی هی</p></div></div>