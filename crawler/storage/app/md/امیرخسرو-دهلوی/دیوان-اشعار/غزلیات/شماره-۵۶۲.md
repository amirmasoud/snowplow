---
title: >-
    شمارهٔ ۵۶۲
---
# شمارهٔ ۵۶۲

<div class="b" id="bn1"><div class="m1"><p>دلی کو چون تو دلداری ندارد</p></div>
<div class="m2"><p>بر اهل عشق مقداری ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سر تا پای زلفت یک شکن نیست</p></div>
<div class="m2"><p>که در هر مو گرفتاری ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندانم زاهدی کز کفر زلفت</p></div>
<div class="m2"><p>به زیر خرقه زناری ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کدامین گل به بستان سرخ روید</p></div>
<div class="m2"><p>که از تو در جگر خاری ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهان پسته ماند با دهانت</p></div>
<div class="m2"><p>ولیکن نغز گفتاری ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی کو روی تو دیده ست، هرگز</p></div>
<div class="m2"><p>نظر بر پند غمخواری ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من از خمخانه دردی کشیدم</p></div>
<div class="m2"><p>که آنجا محتسب کاری ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که آب خوش خورد از عقل آن کس</p></div>
<div class="m2"><p>که ره در کوی خماری ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا و دست گیر افتاده ای را</p></div>
<div class="m2"><p>که جز تو در جهان یاری ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگو کز هجر من چون است خسرو</p></div>
<div class="m2"><p>امید زیستن باری ندارد</p></div></div>