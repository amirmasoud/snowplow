---
title: >-
    شمارهٔ ۱۹۳۱
---
# شمارهٔ ۱۹۳۱

<div class="b" id="bn1"><div class="m1"><p>ای زلف تو هر گره گشادی</p></div>
<div class="m2"><p>وی خط تو خطه و سوادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای چشم مرا چراغ خانه</p></div>
<div class="m2"><p>در سر مکن از کرشمه بادی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در راه نیاز می نهی پای</p></div>
<div class="m2"><p>خوش راهی و بوالعجب نهادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب چشم تو خلق را همی کشت</p></div>
<div class="m2"><p>چونست ز ما نکرد یادی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک فوج ز غمزه نامزد کن</p></div>
<div class="m2"><p>تا با صف غم کنم جهادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر می دادم به هر نگاری</p></div>
<div class="m2"><p>گر تیغ غمت زیان ندادی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرگشته نبودی، ار دل من</p></div>
<div class="m2"><p>در دست خط تو چون فتادی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرکار اگر به دست خویش است</p></div>
<div class="m2"><p>از دایره پا برون نهادی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو تیر ستم گشاده و من</p></div>
<div class="m2"><p>دل بسته بر اینچنین گشادی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر از ستم تو بد گریزان</p></div>
<div class="m2"><p>ایام چو خسروی نزادی</p></div></div>