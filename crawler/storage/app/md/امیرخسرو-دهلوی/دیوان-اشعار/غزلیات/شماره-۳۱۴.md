---
title: >-
    شمارهٔ ۳۱۴
---
# شمارهٔ ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>آن خط پر بلا که در آغاز رستن است</p></div>
<div class="m2"><p>با او چه فتنه ها که در انبار رستن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساکن تری که می دمد آن سبزه بر گلت</p></div>
<div class="m2"><p>نی کاهلی که سبزه ات از باز رستن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آغاز خط به ما منما و مکش، ازانک</p></div>
<div class="m2"><p>هر آفتی که هست، در آغاز رستن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با ما روا مدار که آید برون ز پوست</p></div>
<div class="m2"><p>آن دشمن کشنده که بر ساز رستن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترسم که راز خسرو از این دل برون دمد</p></div>
<div class="m2"><p>خط با لبت نهفته که در راز رستن است</p></div></div>