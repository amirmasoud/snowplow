---
title: >-
    شمارهٔ ۱۰۲۵
---
# شمارهٔ ۱۰۲۵

<div class="b" id="bn1"><div class="m1"><p>به کوی عاشقی از عافیت نشان ندهند</p></div>
<div class="m2"><p>هر آن کسی که بدو این دهند، آن ندهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو عشق جان بردت، شکر گوی، کاین دولت</p></div>
<div class="m2"><p>عطیه ایست که کس را به رایگان ندهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گران رکابی دل برد جمله توسنیم</p></div>
<div class="m2"><p>خوش آن کسان که دل خویش را عنان ندهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دست می نتوان داد خوبرویان را</p></div>
<div class="m2"><p>اگر چه داد دل یار مهربان ندهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرت بتی و شرابی ست وقت را خوش دان</p></div>
<div class="m2"><p>که در جهان به کسی عمر جاودان ندهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفتمش که بکش تا بمیرم و برهم</p></div>
<div class="m2"><p>جواب داد که راحت به عاشقان ندهند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو یار نیست به تسکین خلق نتوان زیست</p></div>
<div class="m2"><p>که دوستان اگرم دل دهند، جان ندهند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو جان دهم به غمش، در رهش کنیدم خاک</p></div>
<div class="m2"><p>حقیقت است که جایم بر آستان ندهند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهی حلاوت تیغ از کف نکورویان</p></div>
<div class="m2"><p>اگر به دست رقیبان بدگمان ندهند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو دل حریف تو شد زینهار، ای ساقی</p></div>
<div class="m2"><p>تنک شراب مرا ساغر گران ندهند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به جور ترک جوانان طریق خسرو نیست</p></div>
<div class="m2"><p>همین بود که ز خون ریزیش امان ندهند</p></div></div>