---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>باز آرزوی آن بت چین می‌کند مرا</p></div>
<div class="m2"><p>معلوم شد که فتنه کمین می‌کند مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌خواندم گدای خود و گویی آن زمان</p></div>
<div class="m2"><p>ملک دو کون زیر نگین می‌کند مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از من مپرس کز چه دل دوست شد به باد</p></div>
<div class="m2"><p>در وی ببین که بی دل و دین می‌کند مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه من به اختیار چنین مست و بی‌خودم</p></div>
<div class="m2"><p>چیزی‌ست در دلم که چنین می‌کند مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه از تو می‌کنند همه عاشقان و من</p></div>
<div class="m2"><p>از دست دل که سوخته، این می‌کند مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد منت خیال تو بر خسرو است، از آنک</p></div>
<div class="m2"><p>گه‌گه به خواب با تو قرین می‌کند مرا</p></div></div>