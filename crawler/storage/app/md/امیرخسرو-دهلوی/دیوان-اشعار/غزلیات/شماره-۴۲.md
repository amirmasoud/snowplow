---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>هنگام آشتی ست بت خشمناک را</p></div>
<div class="m2"><p>دل خوش کنیم لذت روحی فداک را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خشم بود تا به سر ابرویش گره</p></div>
<div class="m2"><p>من زان شکنجه ساخته بودم هلاک را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش وقت آنکه گفت مرا پای من ببوس</p></div>
<div class="m2"><p>شرمنده وار بوسه زد این بنده خاک را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانا، مبر ز بنده از این پس که بر درت</p></div>
<div class="m2"><p>کرده ست پر زخون جگر صحن خاک را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس کز برای آشتی چون تو جنگجوی</p></div>
<div class="m2"><p>آورده ام شفیع شهیدان پاک را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند از مژه اشارت لطفم، ندانی آنک</p></div>
<div class="m2"><p>سوزن ستان بود جگر چاک چاک را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوشنود اگر به جان شود آن دوست، خسروا</p></div>
<div class="m2"><p>عاشق به خویش ره ندهد ترس و باک را</p></div></div>