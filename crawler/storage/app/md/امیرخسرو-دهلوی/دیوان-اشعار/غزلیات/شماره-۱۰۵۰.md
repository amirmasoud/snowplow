---
title: >-
    شمارهٔ ۱۰۵۰
---
# شمارهٔ ۱۰۵۰

<div class="b" id="bn1"><div class="m1"><p>کدام دل که تو غمزده زدی فگار نشد؟</p></div>
<div class="m2"><p>کدام کس که ترا دید و بی قرار نشد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرام باد زخاک تو بر در هر چشم</p></div>
<div class="m2"><p>که هیچ بهره این چشم خاکسار نشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسوخت ناله من سنگ را، عجب سنگ است</p></div>
<div class="m2"><p>دلت که سوخته زین ناله های زار نشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظاره می کنم از دور، می خورم جگری</p></div>
<div class="m2"><p>که جز به دامنم این نقل خوشگوار نشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان پر از گل و سرو روانم از من دور</p></div>
<div class="m2"><p>حساب من به جهان گوییا بهار نشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوشا کرشمه آن یار، دوش زاری من</p></div>
<div class="m2"><p>به دیده برشکن داد و شرمسار نشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>متاع وصل نه اندر قیاس همت ماست</p></div>
<div class="m2"><p>که مرغ سدره غلیواژ را شکار نشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به عشق دوزخی خام سوز شد خسرو</p></div>
<div class="m2"><p>ازان که سوخت درین کار پخته کار نشد</p></div></div>