---
title: >-
    شمارهٔ ۳۵۳
---
# شمارهٔ ۳۵۳

<div class="b" id="bn1"><div class="m1"><p>شب گذشته ست و اول سحر است</p></div>
<div class="m2"><p>بانگ بلبل به می نویدگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقت او خوش که در چنین وقتی</p></div>
<div class="m2"><p>باده بر دست و نازنین به بر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشتی باده نه به کف، باری</p></div>
<div class="m2"><p>عمر ازینسان رود چو بر گذر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند گویی که مست و بی خبری</p></div>
<div class="m2"><p>هر که او مست نیست بی خبر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صرفه خشک زاهدان را باد</p></div>
<div class="m2"><p>هر چه ما راست در شراب تر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقیا، غوطه ده مرا در می</p></div>
<div class="m2"><p>که می آشام شعله در جگر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه بد مستی است عیب حریف</p></div>
<div class="m2"><p>کندن ریش محتسب هنر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر به میخانه مفسدان شراب</p></div>
<div class="m2"><p>پادشاهند، بنده خاک در است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسروا، چند از گنه ترسی</p></div>
<div class="m2"><p>رو که عفو خدای معتبر است</p></div></div>