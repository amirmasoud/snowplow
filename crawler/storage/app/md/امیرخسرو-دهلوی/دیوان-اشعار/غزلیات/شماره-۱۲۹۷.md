---
title: >-
    شمارهٔ ۱۲۹۷
---
# شمارهٔ ۱۲۹۷

<div class="b" id="bn1"><div class="m1"><p>امشب سوی دوست راه گیریم</p></div>
<div class="m2"><p>می بر رخ همچو ماه گیریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی زهد فروختیم بسیار</p></div>
<div class="m2"><p>امروز ز می پناه گیریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اقرار به می کنیم و شاهد</p></div>
<div class="m2"><p>بر خود همه را گواه گیریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنار کمر، سبوی می تاج</p></div>
<div class="m2"><p>ترک کمر و کلاه گیریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اکنون که قلم ز کار ما خاست</p></div>
<div class="m2"><p>چون ترک خط سیاه گیریم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن دوست که با صلاح کو شد</p></div>
<div class="m2"><p>با دشمن کینه خواه گیریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی جان زیادتی ست ما را</p></div>
<div class="m2"><p>کان سلسله دوتاه گیریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنمای رخ چو گل که ناله</p></div>
<div class="m2"><p>چون بلبل صبحگاه گیریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می خواند اجل بر آستانت</p></div>
<div class="m2"><p>بوسی بزنیم و راه گیریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیوانه شدیم، خسرو، اکنون</p></div>
<div class="m2"><p>آن سلسله چو شاه گیریم</p></div></div>