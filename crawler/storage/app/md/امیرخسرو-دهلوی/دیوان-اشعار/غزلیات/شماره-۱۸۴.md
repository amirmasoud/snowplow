---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>روز نوروزست و ساقی جام صهبا برگرفت</p></div>
<div class="m2"><p>هر کسی با شاهد و می راه صحرا بر گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد ره بر چشم خود نرگس که دردش هم نکرد</p></div>
<div class="m2"><p>خوبرویی را که پا بهر تماشا برگرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو با خوبان خرامش کرد و نی می خواست، لیک</p></div>
<div class="m2"><p>پا نکردش پا اگر چه بیشتر پا برگرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست صحرا چون کف دست و بر او لاله چو جام</p></div>
<div class="m2"><p>خوش کف دستی که چندین جام صهبا برگرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرگس اندر پیش گل، گر جام می بر سر کشید</p></div>
<div class="m2"><p>باغبانش مست و لایعقل از آنجا برگرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لاله را سودای خامی بود، با صد شربت ابر</p></div>
<div class="m2"><p>از دماغ لاله نتوانست سودا بر گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چمن رفتم که نرگس چینم از پهلوی گل</p></div>
<div class="m2"><p>چشم نتوانستم از روهای زیبا برگرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کار با دیوانگی افتاد خسرو را، از آنک</p></div>
<div class="m2"><p>سر ز می خوردن نخواهد ساقی ما برگرفت</p></div></div>