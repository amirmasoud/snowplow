---
title: >-
    شمارهٔ ۱۱۹۸
---
# شمارهٔ ۱۱۹۸

<div class="b" id="bn1"><div class="m1"><p>دل من دست بازی می کند هر لحظه با مویش</p></div>
<div class="m2"><p>معاذالله که گر ناگه ببیند چشم بدخویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی کز در برون آید به عیاری و رعنایی</p></div>
<div class="m2"><p>زهی تاراج جان و دل به هر سو کاوفتد هویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرفته آتش اندر جان و می سوزد همه مستی</p></div>
<div class="m2"><p>من از خود بی خبر، مشغول در نظاره رویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نرمی شانه کن در مویش، ای مشاطه کز دردش</p></div>
<div class="m2"><p>رگ جان بگسلد ما را، مبادا بگسلد مویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گذشته ست آن که مستم کردی از بویش، صبا، اکنون</p></div>
<div class="m2"><p>خرابم هم به بوی خود که از من می زند بویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخی بر خاک می سایم کیم من تا قبول افتد</p></div>
<div class="m2"><p>نماز ناروای من به محراب دو ابرویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازان ابروی کژ کو با کمان هندوان ماند</p></div>
<div class="m2"><p>نزد جز تیر زهر آلود بر جان چشم هندویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه عیش است اینکه من این جا و جان من بر رعنا</p></div>
<div class="m2"><p>دوان سرگشته همچون گرد بادی بر سر کویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل گم کرده می جستم میان خاک کوی او</p></div>
<div class="m2"><p>به خنده گفت چون خسرو نخواهی یافت، می جویش</p></div></div>