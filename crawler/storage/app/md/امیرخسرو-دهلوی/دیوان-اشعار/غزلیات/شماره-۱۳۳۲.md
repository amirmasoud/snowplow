---
title: >-
    شمارهٔ ۱۳۳۲
---
# شمارهٔ ۱۳۳۲

<div class="b" id="bn1"><div class="m1"><p>بخت اگر یاری دهد چون جان در آغوشش کنم</p></div>
<div class="m2"><p>تلخ گوید ز آن لب و همچون شکر نوشش کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر من عقل، اگر دعوی هشیاری کند</p></div>
<div class="m2"><p>روی تو بنمایم و از خویش بیهوشش کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش عشقش فرو پوشم درین شخص چوکاه</p></div>
<div class="m2"><p>شعله روشن تر شود هر چند خس پوشش کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر فرو آرم ز دوش و رانم اندر راه او</p></div>
<div class="m2"><p>چون فرو مانم ز رفتن باز بر دوشش کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتاب عارض آن مه که در یاد من است</p></div>
<div class="m2"><p>کافرم تا صبح محشر گر فراموشش کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کو سگی از کوی تو تا از برای زندگی</p></div>
<div class="m2"><p>من دم او گیرم و چون حلقه درگوشش کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آشنا باید که گیرد دست خسرو، زان زمین</p></div>
<div class="m2"><p>هین در آبم، زانکه چون دریاست، در جوشش کنم</p></div></div>