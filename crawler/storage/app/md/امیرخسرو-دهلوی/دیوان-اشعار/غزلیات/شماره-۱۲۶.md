---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>تقدیر که یک چند مرا از تو جدا داشت</p></div>
<div class="m2"><p>از جان گله دارم که مرا زنده چرا داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندوه جدایی ز کسی پرس که یک چند</p></div>
<div class="m2"><p>دور فلک از صبحت یارانش جدا داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیوار ترا من حله خار نخواهم</p></div>
<div class="m2"><p>هجرت به دلم گر چه که صد رخنه روا داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغی دگر اینست که از گریه بشستم</p></div>
<div class="m2"><p>آن داغ که دامانت ز خون دل ما داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صوفی که خرامیدن تو دیده به صد صدق</p></div>
<div class="m2"><p>بدرید مصلا و کله در ته پا داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خسرو به وفای تو دهد جان که در آفاق</p></div>
<div class="m2"><p>گویند همه کان سگ دیوانه وفا داشت</p></div></div>