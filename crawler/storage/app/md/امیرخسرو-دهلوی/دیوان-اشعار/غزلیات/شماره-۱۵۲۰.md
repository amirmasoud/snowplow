---
title: >-
    شمارهٔ ۱۵۲۰
---
# شمارهٔ ۱۵۲۰

<div class="b" id="bn1"><div class="m1"><p>خمار و خواب و چشم کافرش بین</p></div>
<div class="m2"><p>شکنج و پیچش زلف ترش بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل پاکان و جان پارسایان</p></div>
<div class="m2"><p>هلاک غمزه های ساحرش بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو غوغای مگس در خانه شهد</p></div>
<div class="m2"><p>نفیر مستمندان بر درش بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جای آب اگر ساکن ندیدی</p></div>
<div class="m2"><p>درون پیرهن سیمین برش بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بتا جعدت پر از دلهاست خواهی</p></div>
<div class="m2"><p>گره بگشا، به هر مو اندرش بین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه شب باده نوشیده ست تا روز</p></div>
<div class="m2"><p>هنوز آن خواب مستی در سرش بین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدیدم یک رهش، دیوانه گشتم</p></div>
<div class="m2"><p>دلم گوید که بار دیگرش بین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم را سوختی ور باورت نیست</p></div>
<div class="m2"><p>درونم چاک کن، خاکسترش بین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو گوید خسرو از غم گریه چشم</p></div>
<div class="m2"><p>ز خاک پای شاه کشورش بین</p></div></div>