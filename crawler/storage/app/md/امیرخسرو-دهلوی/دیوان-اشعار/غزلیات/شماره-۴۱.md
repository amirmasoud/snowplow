---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>جان بر لب است عاشق بخت آزمای را</p></div>
<div class="m2"><p>دستورییی به خنده لب جانفزای را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون مرا بریز و زخونابه وا رهان</p></div>
<div class="m2"><p>خیریست، این بکن ز برای خدای را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی به مهر و مه نگر و ترک من بگوی</p></div>
<div class="m2"><p>این رو که داد مهر و مه خودنمای را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان شوخ چون وفا طلبم من که بر درش</p></div>
<div class="m2"><p>هرگز ز ننگ می نگرد این گدای را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>واگشتی، ای صبا، چو بر آن کوی بگذری</p></div>
<div class="m2"><p>آسیب بر چه می زنی آن بوسه جای را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطرب، بزن رهی و مبین زهد من، از آنک</p></div>
<div class="m2"><p>بر سبحه منست شرف چنگ و نای را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نازک مگوی ساعد خوبان که خرد کرد</p></div>
<div class="m2"><p>چندین هزار بازوی زورآزمای را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای دوست، عشق چون همه چشم است و گوش نیست</p></div>
<div class="m2"><p>چه جای پند خسرو شوریده رای را</p></div></div>