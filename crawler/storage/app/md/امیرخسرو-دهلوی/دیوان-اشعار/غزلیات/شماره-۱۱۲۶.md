---
title: >-
    شمارهٔ ۱۱۲۶
---
# شمارهٔ ۱۱۲۶

<div class="b" id="bn1"><div class="m1"><p>افتادگان راه توییم از سر نیاز</p></div>
<div class="m2"><p>دستی بگیر و در قدمت سر ز ما بباز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع جهانفروز تویی در جهان، ولی</p></div>
<div class="m2"><p>ماییم از برای تو در سوز و در گداز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ما چه احتراز نمودی که در جهان</p></div>
<div class="m2"><p>هرگز نکرد شمع ز پروانه احتراز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو نماز جانب محراب می کنی</p></div>
<div class="m2"><p>ما می کنیم در خم ابروی تو نماز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببرید زلف و کرد به خسرو اشارتی</p></div>
<div class="m2"><p>یعنی که عمر تست نمی خواهمش دراز</p></div></div>