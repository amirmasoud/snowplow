---
title: >-
    شمارهٔ ۱۵۶۹
---
# شمارهٔ ۱۵۶۹

<div class="b" id="bn1"><div class="m1"><p>از شب گیسوی تست روشنی روز من</p></div>
<div class="m2"><p>از رخ چون انجمت روشنی انجمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا که شکسته دلم صحبت زلفت گزید</p></div>
<div class="m2"><p>صحبت دل کرد اثر، زلف تو شد پرشکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سر زلفت نخاست این دل گردن زده</p></div>
<div class="m2"><p>من ز سرش خواستم، گردن او را بزن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من همه سر می نهم پیش تو بی گفت تو</p></div>
<div class="m2"><p>تو همه سر می کشی پیش من از گفت من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر رخ خسرو بماند نقش ز خوبان دل</p></div>
<div class="m2"><p>تا دل پرخون اوست نقش رخت را وطن</p></div></div>