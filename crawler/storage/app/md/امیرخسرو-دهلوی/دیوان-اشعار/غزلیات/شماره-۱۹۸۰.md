---
title: >-
    شمارهٔ ۱۹۸۰
---
# شمارهٔ ۱۹۸۰

<div class="b" id="bn1"><div class="m1"><p>ای معدن ناز، ناز تا کی؟</p></div>
<div class="m2"><p>بر من در تو فراز تا کی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حسرت یک نظر بمردیم</p></div>
<div class="m2"><p>چشم تو به خواب ناز تا کی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو ابروی خویش می پرستی</p></div>
<div class="m2"><p>در قبله کج نماز تا کی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمعم خوانی و سوزیم زار</p></div>
<div class="m2"><p>بر سوخته ها گداز تا کی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس نیست هلاک من به زلفت</p></div>
<div class="m2"><p>دیگر شب من دراز تا کی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیری که بر سینه خورد محمود</p></div>
<div class="m2"><p>در کشمکش ایاز تا کی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخل تو برای نیم بوسی</p></div>
<div class="m2"><p>بر خسرو پاک باز تا کی</p></div></div>