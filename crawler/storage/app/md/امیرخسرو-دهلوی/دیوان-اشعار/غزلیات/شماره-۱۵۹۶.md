---
title: >-
    شمارهٔ ۱۵۹۶
---
# شمارهٔ ۱۵۹۶

<div class="b" id="bn1"><div class="m1"><p>ز زلف تو کمر فتنه بر میان بستن</p></div>
<div class="m2"><p>ز من به یک سر مویت همه جهان بستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل پر آتش من زان به زلف در بستی</p></div>
<div class="m2"><p>که بس عجب بد آتش به ریسمان بستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عشق طره تو نافه می کند آهو</p></div>
<div class="m2"><p>وگر که چند گره به شکم توان بستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگار بستن تو جادویی است اندر دست</p></div>
<div class="m2"><p>کزان نگار توان دست جاودان بستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز ناتوانی چشمت جهان چو گشت خراب</p></div>
<div class="m2"><p>طبیب را نبود چاره از دکان بستن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیال روی تو شد شهربند سینه من</p></div>
<div class="m2"><p>همای را نتوان جز به استخوان بستن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبست خسرو مسکین دلی به تو که تراست</p></div>
<div class="m2"><p>اگر چه نیز گشاید از این میان بستن</p></div></div>