---
title: >-
    شمارهٔ ۵۲۹
---
# شمارهٔ ۵۲۹

<div class="b" id="bn1"><div class="m1"><p>هر کس که تقرب ز وصال تو نجوید</p></div>
<div class="m2"><p>واندر ره ادراک جمال تو نپوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فردا که شب وعده دیدار سر آید</p></div>
<div class="m2"><p>رهبر نبود سوی تو چندان که نجوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فردا که تو در گلشن فردوس خرامی</p></div>
<div class="m2"><p>طوبی، ادب آنست، که در راه نروید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شک نیست که چرخ از پی صد دور بیاید</p></div>
<div class="m2"><p>مهر تو ز هر ذره خاکم که ببوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریاد ز غوغای رقیبان که نمانند</p></div>
<div class="m2"><p>تا با تو کسی درد دل خویش بگوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدار حرام است کسی را که چو خسرو</p></div>
<div class="m2"><p>از دیده به خون دل خود دست بشوید</p></div></div>