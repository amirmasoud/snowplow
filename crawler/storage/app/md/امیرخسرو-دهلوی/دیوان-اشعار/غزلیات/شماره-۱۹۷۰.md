---
title: >-
    شمارهٔ ۱۹۷۰
---
# شمارهٔ ۱۹۷۰

<div class="b" id="bn1"><div class="m1"><p>نیست در شهر گرفتارتر از من دگری</p></div>
<div class="m2"><p>نبد او تیر غم افگارتر از من دگری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر کوی تو دانم که سگان بسیاراند</p></div>
<div class="m2"><p>نیک بنمای وفادارتر از من دگری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وه که آن روی به جز من دگری را منمای</p></div>
<div class="m2"><p>تا نمیرد ز غمت زارتر از من دگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرمسارم ز گران جانی خود، زانکه نماند</p></div>
<div class="m2"><p>بر سر کوی تو بسیارتر از من دگری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محنت عشق و غم دوری و بدخویی دوست</p></div>
<div class="m2"><p>نکشد این همه دشوارتر از من دگری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاروان رفت و مرا بار بلایی در دل</p></div>
<div class="m2"><p>چون روم، نیست گران بارتر از من دگری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقیا، برگذر از من که به خواب اجلم</p></div>
<div class="m2"><p>باز چو اکنون تو هشیارتر از من دگری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسروم، بهر بتان کوی به کو سرگردان</p></div>
<div class="m2"><p>در جهان نبود بیکارتر از من دگری</p></div></div>