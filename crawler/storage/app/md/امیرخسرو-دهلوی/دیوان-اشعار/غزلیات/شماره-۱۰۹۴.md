---
title: >-
    شمارهٔ ۱۰۹۴
---
# شمارهٔ ۱۰۹۴

<div class="b" id="bn1"><div class="m1"><p>یکی امروز سر زلف پریشان بگذار</p></div>
<div class="m2"><p>شانه تا کی بود انگشت به دندان بگذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سرم نیست به سامان ز غمت هیچ مگوی</p></div>
<div class="m2"><p>مر مرا هم به من بی سرو سامان بگذار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیک دانند لب و چشم تو مردم کشتن</p></div>
<div class="m2"><p>تو مشو رنجه و این کار بدیشان بگذار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طره را کار مفرمای به شهر آشوبی</p></div>
<div class="m2"><p>دیو را شغل گرفتن به سلیمان بگذار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوییم جان غمین تو، گرفتار من است</p></div>
<div class="m2"><p>دو جهان گشت گرفتار تو، یک جان بگذار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ز درماندگی عشق ترا دردی هست</p></div>
<div class="m2"><p>هم بدان درد قناعت کن و درمان بگذار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسروا، یا به گریبان وفا سر در کن</p></div>
<div class="m2"><p>یا ز کف دامن اندیشه خوبان بگذار</p></div></div>