---
title: >-
    شمارهٔ ۱۰۶۹
---
# شمارهٔ ۱۰۶۹

<div class="b" id="bn1"><div class="m1"><p>امروز که از باران شد سبزه رعنا تر</p></div>
<div class="m2"><p>سیم و زر گل جمله گشتند به صحرا تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>احوال دو چشم من در گریه یکی بنگر</p></div>
<div class="m2"><p>چون خانه پر روزن اینجاتر و آنجاتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد جان نه یکی باید تا صرف کنم در ره</p></div>
<div class="m2"><p>گردد چو کف پایت در راه تماشاتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آهنگ برون داری، آب است به ره، ای چشم</p></div>
<div class="m2"><p>زین راه تفحص کن خشک است زمین یا تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سبزه خرامیدن کردی هوس و شستن</p></div>
<div class="m2"><p>خود سبزه نخواهد بود از خط تو رعناتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بالاتر هر جا دو چشم تو همی بینم</p></div>
<div class="m2"><p>ابروی تو می بینم از چشم تو بالاتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو، صفت خوبان می گوی که خود نبود</p></div>
<div class="m2"><p>در هیچ گلستانی بلبل ز تو گویاتر</p></div></div>