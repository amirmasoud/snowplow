---
title: >-
    شمارهٔ ۱۸۳
---
# شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>وقتی غباری زآستان بفرست سوی چاکرت</p></div>
<div class="m2"><p>تا کی تهی چشم کند با دیده‌ام خاک درت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستی بده، ای آشنا، درماندگان را، چون که شد</p></div>
<div class="m2"><p>غرقه به هر یک قطره خوی صد دل به رخسار ترت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دریافتم دل دزدیت، از غمزه غماز تو</p></div>
<div class="m2"><p>آن پرده ما باز شد، چون گشت پیدا گوهرت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ابر، گه‌گاهی بگو آن چشمه خورشید را</p></div>
<div class="m2"><p>در قعر دریا خشک شد از تشنگی نیلوفرت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه ز رحمت آیتی شب‌ها عذابی بر دلم</p></div>
<div class="m2"><p>از بس که آیات الم خوانم همه شب از برت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر کم از نظاره‌ای از دور در نخل قدت</p></div>
<div class="m2"><p>دست امیدم کوته است از شاخ سبز نوبرت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بند پروازست جان، بگذار سیرت بنگرم</p></div>
<div class="m2"><p>زینسان که بینم حال خود مهمان که بینم دیگرت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌کن جفا تا پیش تو می‌ریزم از دیده گوهر</p></div>
<div class="m2"><p>زیرا که تو زیبا رخی زین به نباشد زیورت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویی به خنده، خسروا، زان توام، گرچه نه‌ای</p></div>
<div class="m2"><p>تسکین جان خویش را ناچار دارم باورت</p></div></div>