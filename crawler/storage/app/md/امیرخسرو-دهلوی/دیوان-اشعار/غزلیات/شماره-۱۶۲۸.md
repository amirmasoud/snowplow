---
title: >-
    شمارهٔ ۱۶۲۸
---
# شمارهٔ ۱۶۲۸

<div class="b" id="bn1"><div class="m1"><p>چشم است، یارب، آنچنان یا خود بلای جان من</p></div>
<div class="m2"><p>جور است از آنسان دلستان یا غارت ایمان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوخ و مقامر پیشه ای، قتال بی اندیشه ای</p></div>
<div class="m2"><p>خونین چو شیرین تیشه ای، صیدت دل قربان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر روز آیم سوی تو، دل جویم از گیسوی تو</p></div>
<div class="m2"><p>کان دل که دارد خوی تو، بوده ست روزی آن من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از غارت خوبان مرا جان رها شد مبتلا</p></div>
<div class="m2"><p>تو، شوخ، دیگر از کجا پیدا شدی از جان من؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای، گنج دلها، مستیت، در قتل چابکدستیت</p></div>
<div class="m2"><p>درد من آمد مستیت، دیوانگی درمان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هجرم بکشت و شوق هم، روزی نگفتی از کرم</p></div>
<div class="m2"><p>چون است در شبهای غم، آن عاشق حیران من؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با عاشقان تنگدل زینسان منه در جنگ دل</p></div>
<div class="m2"><p>آخر بترس، ای سنگدل، ز آه دل بریان من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خیز، ای صبای مشک بو، بر گلرخ من راه جو</p></div>
<div class="m2"><p>حال من مسکین بگو، در خدمت جانان من</p></div></div>