---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>آن طره به روی مه بنهاد سر خود را</p></div>
<div class="m2"><p>از خط غبار آن رخ پوشیده خور خود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دید گل رویش در صحن چمن، زان گل</p></div>
<div class="m2"><p>ایثار قدومش کرد از شرم زر خود را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مانند قدش بستان چون دید سهی سروی</p></div>
<div class="m2"><p>زیر قدمش سبزه بنهاد سر خود را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدم به رقیب او بنشسته سگ کویش</p></div>
<div class="m2"><p>گفتم که فلان اکنون و ایافت خر خود را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ناصح بیهوده چندین چه دهی پندم</p></div>
<div class="m2"><p>بگذار مرا بگذار، می خار سر خود را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان بند قبا دارم پیوسته به دل غصه</p></div>
<div class="m2"><p>کاندر پی جان من بربست بر خود را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتا ز درم خسرو، منزل به دگر جا کن</p></div>
<div class="m2"><p>گفتم که سگ خانه نگذاشت در خود را</p></div></div>