---
title: >-
    شمارهٔ ۱۰۰۷
---
# شمارهٔ ۱۰۰۷

<div class="b" id="bn1"><div class="m1"><p>دو چشمت که تیر بلا می زند</p></div>
<div class="m2"><p>چنان تیر بهر چرا می زند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمان جانب دیگری می کشد</p></div>
<div class="m2"><p>ولی تیر بر جان ما می زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهی دیده کز شوخی و چابکی</p></div>
<div class="m2"><p>کجا می نماید، کجا می زند؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو زلف تو از پشتی روی او</p></div>
<div class="m2"><p>شب تیره را در قفا می زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هنگام رفتار بالای تو</p></div>
<div class="m2"><p>تگ کبک را زاغ پا می زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بوی ترا در چمن می برد</p></div>
<div class="m2"><p>نسیم بهار از صبا می زند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوا می زند بلبل از راه عشق</p></div>
<div class="m2"><p>ولی راه این بینوا می زند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مریز آب خسرو همین غم بس است</p></div>
<div class="m2"><p>که آتش درین مبتلا می زند</p></div></div>