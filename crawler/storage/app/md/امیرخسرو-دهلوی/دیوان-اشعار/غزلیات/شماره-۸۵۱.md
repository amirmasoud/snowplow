---
title: >-
    شمارهٔ ۸۵۱
---
# شمارهٔ ۸۵۱

<div class="b" id="bn1"><div class="m1"><p>ما را شکنج زلف تو در پیچ و تاب برد</p></div>
<div class="m2"><p>آرام و صبر از دل و از دیده خواب برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از راه دل در آمد و از روزن دماغ</p></div>
<div class="m2"><p>رختی که دیده بسته به مشکین طناب برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی عجب مدار که طوفان برآورد</p></div>
<div class="m2"><p>باران اشک دیده که دست از سحاب برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمم که بود خانه خیل خیال تو</p></div>
<div class="m2"><p>عمرت دراز باد که آن خانه آب برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهد برای مجلس رندان باده نوش</p></div>
<div class="m2"><p>دوش آمد و به دوش سبوی شراب برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوران پیریم به سر آورد روز شیب</p></div>
<div class="m2"><p>هجران یار رونق عهد شباب برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو بسی خطا که به طغرای دلبران</p></div>
<div class="m2"><p>خواهد برات نامه به روز حساب برد</p></div></div>