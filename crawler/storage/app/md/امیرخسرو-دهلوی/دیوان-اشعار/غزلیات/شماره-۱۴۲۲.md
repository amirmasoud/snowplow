---
title: >-
    شمارهٔ ۱۴۲۲
---
# شمارهٔ ۱۴۲۲

<div class="b" id="bn1"><div class="m1"><p>اگر نه روی تو ببینم، به ماهتاب نبینم</p></div>
<div class="m2"><p>وگر چه ماه بتابد، به ماه تاب نبینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن زمان که نبینم ترا به چشم چو ابرم</p></div>
<div class="m2"><p>چنان ببارد باران که آفتاب نبینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خانه سایه همی گیردم ز فکرت زلفت</p></div>
<div class="m2"><p>که آفتاب در این خانه خراب نبینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وصال خواهم و این در به روی من که گشاید</p></div>
<div class="m2"><p>ز خنده شکرینت چو فتح باب نبینم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به وصل چند توان گفتنم «هنوز توقف »</p></div>
<div class="m2"><p>کنم توقف، اگر عمر را شتاب نبینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طمع بود ز دهان تو شربتیم، ولیکن</p></div>
<div class="m2"><p>سؤال از که کنم، چون ره جواب نبینم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو دل سخن نشنود و تو عاقبت بربودی</p></div>
<div class="m2"><p>روان بکش که نگه داشتن صواب نبینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز آب می نرود از دو چشم خسرو و ترسم</p></div>
<div class="m2"><p>که چند روز دگر خون رود که آب نبینم</p></div></div>