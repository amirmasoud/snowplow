---
title: >-
    شمارهٔ ۶۷۱
---
# شمارهٔ ۶۷۱

<div class="b" id="bn1"><div class="m1"><p>از دل غمگین هوای دلستانم چون رود</p></div>
<div class="m2"><p>یا سر سودای آن سرو روانم چون رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا توانایی بدم، بار غمش بردم به جان</p></div>
<div class="m2"><p>خود کنون عشقش ز جان ناتوانم چون رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دلم نیش جفایش گر رود، نبود عجب</p></div>
<div class="m2"><p>لذت دشنام او هرگز ز جانم چون رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غمزه قصاب او می ریزدم خون، شاکرم</p></div>
<div class="m2"><p>جای شکرست، این شکایت بر زبانم چون رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد مردن، گر شوم خاک و تنم گردد غبار</p></div>
<div class="m2"><p>داغ مهر او ز مغز استخوانم چون رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ز پا افتم دران کوی و رود تیغم به سر</p></div>
<div class="m2"><p>زینقدر از دل غم آن دلستانم چون رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قد یارم از نظر گه گه رود خسرو، ولی</p></div>
<div class="m2"><p>نقش روی او ز چشم خونفشانم چون رود</p></div></div>