---
title: >-
    شمارهٔ ۹۸۲
---
# شمارهٔ ۹۸۲

<div class="b" id="bn1"><div class="m1"><p>هر که را یاریار می افتد</p></div>
<div class="m2"><p>مقبل و بختیار می افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بسا در که در محیط سرشک</p></div>
<div class="m2"><p>هر دمم در کنار می افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقرب او چو حلقه می گردد</p></div>
<div class="m2"><p>تاب در جان مار می افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شام زلفش چو می رود در چین</p></div>
<div class="m2"><p>شور در زنگبار می افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرنه مست است جادویش، ز چه روی</p></div>
<div class="m2"><p>بر یمین و یسار می افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل صد برگ را دگر در دام</p></div>
<div class="m2"><p>همچو بلبل هزار می افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ز حالش همی کنم تقریر</p></div>
<div class="m2"><p>بخیه بر روی کار می افتد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم از شوق چشم سرمتش</p></div>
<div class="m2"><p>دم به دم در کنار می افتد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رحم بر آن پیاده کو هر دم</p></div>
<div class="m2"><p>در کمند سوار می افتد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که او خوار می فتد، خسرو</p></div>
<div class="m2"><p>همچو ما باده خوار می افتد</p></div></div>