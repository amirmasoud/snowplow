---
title: >-
    شمارهٔ ۴۵۸
---
# شمارهٔ ۴۵۸

<div class="b" id="bn1"><div class="m1"><p>چو جان عاشقان آن ماه را سلطان و خان سازد</p></div>
<div class="m2"><p>جهانی پیش او خود را غلام رایگان سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرامان می رود آن شوخ و در وی عالمی حیران</p></div>
<div class="m2"><p>بزرگ آن صانعی کز آب آن سرو روان سازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر ابرو خال دارد آن بت و جانم فدای او</p></div>
<div class="m2"><p>در آن دم کو بسی دل طعمه زاغ و کمان سازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر آن چشم گردم، چون به ناز و شیوه و شوخی</p></div>
<div class="m2"><p>گهی مستی نماید، گاه خود را ناتوان سازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزاران را ببین چون خاک در کویش پراگنده</p></div>
<div class="m2"><p>که آن بازنده شطرنج هوس زین استخوان سازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود معشوق چون شمعی، خوش آن پروانه عاشق</p></div>
<div class="m2"><p>که مهمانش رسد وز شعله نقل میهمان سازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امان هرگز نباشد عاشق بیچاره را از غم</p></div>
<div class="m2"><p>مگر آنگه که کوی خویش را دارالامان سازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به بیماری غم خسرو، برای زیستن هر دم</p></div>
<div class="m2"><p>نوای خویش را از خون دل تعویذ جان سازد</p></div></div>