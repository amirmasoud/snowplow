---
title: >-
    شمارهٔ ۸۹۷
---
# شمارهٔ ۸۹۷

<div class="b" id="bn1"><div class="m1"><p>از آنگهی که گشادم به رویت این نظر خود</p></div>
<div class="m2"><p>چه خون که خوردم ازین چشم پر در و گهر خود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به باغ رفتم و قوتی ز بوی گل بگرفتم</p></div>
<div class="m2"><p>ز بس که سوختم از تاب سوزش جگر خود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجات بینم و بر بام تو چگونه برآیم؟</p></div>
<div class="m2"><p>هزار وای که مرغان نمی دهند پر خود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرم که بر درت افتاد تا که پات نرنجد</p></div>
<div class="m2"><p>به پشت پا چو کلوخیش دور کن ز در خود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بنده روی ببیند، بر آن شود که بگردد</p></div>
<div class="m2"><p>هزار بار به گرد سر دو چشم تر خود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم صدق ندارد به کار عشق، چه بودی</p></div>
<div class="m2"><p>وه این نگین دروغی جدا کن از کمر خود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز عشق آنکه رسیده سپر ندیده خدنگت</p></div>
<div class="m2"><p>بر آنست دیده خسرو که بفگند سپر خود</p></div></div>