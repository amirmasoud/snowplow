---
title: >-
    شمارهٔ ۱۹۶۶
---
# شمارهٔ ۱۹۶۶

<div class="b" id="bn1"><div class="m1"><p>بهاری این چنین خرم، مرا آواره دل جایی</p></div>
<div class="m2"><p>من و کنج غم و هر کس به باغی و تماشایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سوی سرو پا در گل روان شد خلق و من آنم</p></div>
<div class="m2"><p>که خواهم خاک گشتن زیر پای سر و بالایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز هجران خون همی گریم، نروید جز گیاه غم</p></div>
<div class="m2"><p>چنین ابری معاذالله اگر بارد به صحرایی!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو ای کم گوی از کویش، بکش پا، من همی گویم</p></div>
<div class="m2"><p>که پیشش سر نهی از من، اگر پیش آیدت جایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کویت سنگ سارم، گر تو بنوازی به یک سنگم</p></div>
<div class="m2"><p>بیا نظاره کن، باری جمال حال رسوایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خاری کز جفایت می خلد در سینه، خرسندم</p></div>
<div class="m2"><p>اگر از نخل بالایت نمی ارزم به خرمایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کباب خام سوزی را حریف چاشنی داند</p></div>
<div class="m2"><p>که از سوز جگر وقتی چو من پخته ست سودایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر زیر و زبر شد ذره، گو می شو، چه باب است این</p></div>
<div class="m2"><p>که یاد آید گهی خورشید را از بی سرو پایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو، ای عاقل، که از خسرو سر و سامان همی جویی</p></div>
<div class="m2"><p>رها کن، وه چه می جویی ز مجنونی و شیدایی</p></div></div>