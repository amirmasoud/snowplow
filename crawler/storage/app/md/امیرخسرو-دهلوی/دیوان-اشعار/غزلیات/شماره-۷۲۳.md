---
title: >-
    شمارهٔ ۷۲۳
---
# شمارهٔ ۷۲۳

<div class="b" id="bn1"><div class="m1"><p>مرد صاحب نظر از کوی تو آسان نرود</p></div>
<div class="m2"><p>هر که راجان بود، از خدمت جانان نرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه در عشق رخت لاف هواداری زد</p></div>
<div class="m2"><p>به جفا از درت، ای خسرو خوبان، نرود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خیال من سودا زده اندر ره عمر</p></div>
<div class="m2"><p>یک نفس صورت آن سرو خرامان نرود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار حسن تو رسیده ست به جایی که سزد</p></div>
<div class="m2"><p>که به عهدت سخن از یوسف کنعان نرود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با خضر ذکر لب لعل تو می باید گفت</p></div>
<div class="m2"><p>تا دگر در طلب چشمه حیوان نرود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باغبان ار رخ زیبای تو بیند، دیگر</p></div>
<div class="m2"><p>از پی چیدن گل سوی گلستان نرود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با وصال تو ندارم سر بستان و بهشت</p></div>
<div class="m2"><p>هر که را باغچه ای هست، به بستان نرود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسرو خسته که مانده ست به دهلی در بند</p></div>
<div class="m2"><p>آه، اگر زو خبری سوی خراسان نرود</p></div></div>