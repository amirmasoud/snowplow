---
title: >-
    شمارهٔ ۷۲۹
---
# شمارهٔ ۷۲۹

<div class="b" id="bn1"><div class="m1"><p>چه خوش است از جگر سوخته بویی که زند</p></div>
<div class="m2"><p>در فلکها فگند رخنه ز مویی که زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر سربازی و یا صاحب حالی باشد</p></div>
<div class="m2"><p>زلف چوگان وش کژباز تو گویی که زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیک بخت آنکه کند مست و خرابش گه هوش</p></div>
<div class="m2"><p>از لب لعل می آلود تو بویی که زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من که میخواره خامم به سرم باید دید</p></div>
<div class="m2"><p>محتسب پر ز می خشم سبویی که زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی من گشت ز محراب، بگردد ناچار</p></div>
<div class="m2"><p>پنجه حسن بتان لطمه به رویی که زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بسا خواب صبوحی که به تاراج برند</p></div>
<div class="m2"><p>هر شب آن راهزن راه به سویی که زند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقل و می از دل خسرو خورد آن شاهسوار</p></div>
<div class="m2"><p>خیمه عیش و طرب بر لب جویی که زند</p></div></div>