---
title: >-
    شمارهٔ ۱۱۵۰
---
# شمارهٔ ۱۱۵۰

<div class="b" id="bn1"><div class="m1"><p>پیش چشم خود مگو، گر با تو گویم سوز خویش</p></div>
<div class="m2"><p>زانکه می دانی مزاج غمزه کین توز خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمزه را گویی چو شاهان زن که نه مردانگیست</p></div>
<div class="m2"><p>بر گدایان آزمودن خنجر فیروز خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من چو گردم کشته، گه گاهی بگردانی به زلف</p></div>
<div class="m2"><p>جان من گرد سر آن ناوک دل دوز خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همره جان کردم از جولانت گردی تا کنم</p></div>
<div class="m2"><p>توشه فردای حشر این نعمت امروز خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک شد جانها به ره، مپسند از بهر خدا</p></div>
<div class="m2"><p>این غبار غم بر آن روی جهان افروز خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر شبی پیش چراغی سوز خود گویم، از آنک</p></div>
<div class="m2"><p>سوخته با سوخته بیرون فشاند سوز خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دلم باز آمد او، یاری کن، ای خون جگر</p></div>
<div class="m2"><p>تا بگریم سیر من بر روزگار و روز خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنده خسرو بر رخ از خون حرف بی صبری نوشت</p></div>
<div class="m2"><p>تا کند تعلیم رسوایی به صبرآموز خویش</p></div></div>