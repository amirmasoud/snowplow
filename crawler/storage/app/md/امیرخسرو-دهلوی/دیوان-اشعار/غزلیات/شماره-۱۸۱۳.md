---
title: >-
    شمارهٔ ۱۸۱۳
---
# شمارهٔ ۱۸۱۳

<div class="b" id="bn1"><div class="m1"><p>سزد گر نیکویی در من ببینی</p></div>
<div class="m2"><p>که خودکام و جوان و نازنینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گاه خنده چون دندان نمایی</p></div>
<div class="m2"><p>مرا اندر میان چشم شینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مسلمان دیدمت، زان دل سپردم</p></div>
<div class="m2"><p>ندانستم که تو کافر چنینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مه و خورشید را بسیار دیدم</p></div>
<div class="m2"><p>بهی از هر که می گویم، نه اینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عیش خوش ترش خوشنودم از تو</p></div>
<div class="m2"><p>که گاهی سرکه گاهی انگبینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز جان آیم به استقبال تیرت</p></div>
<div class="m2"><p>که بر من راست کرده در کمینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا گر در همی چینی ز چشمم</p></div>
<div class="m2"><p>به شرط آنکه مهره برنچینی</p></div></div>