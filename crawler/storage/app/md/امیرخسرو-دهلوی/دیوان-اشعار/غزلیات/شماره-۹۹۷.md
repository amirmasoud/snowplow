---
title: >-
    شمارهٔ ۹۹۷
---
# شمارهٔ ۹۹۷

<div class="b" id="bn1"><div class="m1"><p>دل با درد را کجا یابند؟</p></div>
<div class="m2"><p>گونه زرد را کجا یابند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار اندوه بی دلان، چه خوش است؟</p></div>
<div class="m2"><p>نفس سرد را کجا یابند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوبروی من از بتان فرد است</p></div>
<div class="m2"><p>این چنین فرد را کجا یابند؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون منی کو که حال من پرسد</p></div>
<div class="m2"><p>یار همدرد را کجا یابند؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبرم از دست غم گریخت، کنون</p></div>
<div class="m2"><p>آن جهانگرد را کجا یابند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که در عشق جان دهد مرد است</p></div>
<div class="m2"><p>این چنین مرد را کجا یابند؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سگ کویی ست خسرو اندر عشق</p></div>
<div class="m2"><p>شیر ناورد را کجا یابند؟</p></div></div>