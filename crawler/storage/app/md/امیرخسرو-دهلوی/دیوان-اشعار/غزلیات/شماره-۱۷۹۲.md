---
title: >-
    شمارهٔ ۱۷۹۲
---
# شمارهٔ ۱۷۹۲

<div class="b" id="bn1"><div class="m1"><p>آن دل خراب شد که تو آباد دیده‌ای</p></div>
<div class="m2"><p>وان سینه غم گرفت که تو شاد دیده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بازارِ عیش و خانهٔ هستی و کویِ عقل</p></div>
<div class="m2"><p>ویرانه‌ها شد آن همه کآباد دیده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمری‌ست تا به دامِ بلایی اسیر ماند</p></div>
<div class="m2"><p>آن جانِ نازنین که تو آزاد دیده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نزدِ من، ای حسود، تو بایستیی کنون</p></div>
<div class="m2"><p>تا خان و مانِ دل همه بر باد دیده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای پندگوی، همرهِ من در عدم نه‌ای</p></div>
<div class="m2"><p>تا از غمِ وِیَم علف و زاد دیده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای مرغِ عاشق ار تو بدانسته‌ای وفا</p></div>
<div class="m2"><p>در رنجِ خویش راحتِ صیاد دیده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو، به بوستان چه رَوی دل دگر طرف؟</p></div>
<div class="m2"><p>کاش از نخست در گل و شمشاد دیده‌ای</p></div></div>