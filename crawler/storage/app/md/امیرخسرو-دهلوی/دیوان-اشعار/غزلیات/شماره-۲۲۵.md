---
title: >-
    شمارهٔ ۲۲۵
---
# شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>یارب، اندر سر هر موی تو چندان چه خم است</p></div>
<div class="m2"><p>زیر آن موی رخت از گل خندان چه کم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند گویی که مکن صورت جورم از چشم</p></div>
<div class="m2"><p>مردم چشم تو خود صورت جور و ستم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما چو از زلف تو زنار ببستیم، اکنون</p></div>
<div class="m2"><p>هم به روی تو اگر روی مرا بر صنم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاه گاهی که دمی نیم دمی همچو مسیح</p></div>
<div class="m2"><p>زندگانی اگرم هست همان نیم دم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای لب از خون دلم شسته ز بهر خونم</p></div>
<div class="m2"><p>تا چه در دست که لبهای ترا در شکم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل من سوی عدم رفت به همراهی صبر</p></div>
<div class="m2"><p>از لب خود خبری پرس که راه عدم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماند با خط تو چسبیده سیاهی دو چشم</p></div>
<div class="m2"><p>زان که خط توتر و دیده من نیز نم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه سبب خط ترا ماه بود در فرمان</p></div>
<div class="m2"><p>مگر از خامه دستور عطارد رقم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگر از جرعه جام کرمت شسته شود</p></div>
<div class="m2"><p>دل خسرو که بیالوده ز اندوه و غم است</p></div></div>