---
title: >-
    شمارهٔ ۱۲۰۵
---
# شمارهٔ ۱۲۰۵

<div class="b" id="bn1"><div class="m1"><p>غم دل زان خورم کانجاست آن بالای چون سیمش</p></div>
<div class="m2"><p>وگر نه دل که دشمن شد مرا، چه جای تعظیمش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دهانش میم مقصود است و صد سبق از غمش خواندم</p></div>
<div class="m2"><p>نشد ممکن که یک روزی نهم انگشت بر میمش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزاران جان مسکینان دو نیم است از دهان او</p></div>
<div class="m2"><p>که آن سلطان بخنده می کند هر لحظه دو نیمش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم را بذل جان فرمود پیراهن که می لرزد</p></div>
<div class="m2"><p>بسان مدخلان ترسم بران اندام چون سیمش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مبادا حسن او را روز نیکو جز همان رویش</p></div>
<div class="m2"><p>که بهر کشتن ما ناز و شوخی کرد تعلیمش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حکیم آن ماه را با من قران گفت و نمی دانم</p></div>
<div class="m2"><p>که خواهم بوسه داد و یا بخواهم سوخت تقویمش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهانی خوشدلی بودم که ناگه زد غمش بر من</p></div>
<div class="m2"><p>نبینی یک ده آبادان کنون در هفت اقلیمش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصیت می کنم جان را که هر دم بر سرش گردی</p></div>
<div class="m2"><p>وصیت این کنم باری چو خواهم کرد تسلیمش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به کویش رفت خسرو تا دل گم گشته را جوید</p></div>
<div class="m2"><p>بدیدش ناگهانی و فتاد از بهر جان بیمش</p></div></div>