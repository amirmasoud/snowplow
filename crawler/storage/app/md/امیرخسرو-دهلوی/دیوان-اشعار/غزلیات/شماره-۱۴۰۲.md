---
title: >-
    شمارهٔ ۱۴۰۲
---
# شمارهٔ ۱۴۰۲

<div class="b" id="bn1"><div class="m1"><p>بخرام تا به زیر قدم پی سپر شویم</p></div>
<div class="m2"><p>خاکیم در رهت، قدمی خاک تر شویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بخششی دگر نکنی، خون من بریز</p></div>
<div class="m2"><p>باری بدین بهانه به نامت سمر شویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقلم ز نام و ننگ خبر می دهد هنوز</p></div>
<div class="m2"><p>بنمای یک کرشمه که تا بی خبر شویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبها قرار نیست، دمی گر بود قرار</p></div>
<div class="m2"><p>بادی وزد زلف تو زیر و زبر شویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را نماند خواب، رها کن که بعد از این</p></div>
<div class="m2"><p>بر پات رو نهیم و به خواب دگر شویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را دگر مگوی که جای حواله نیست</p></div>
<div class="m2"><p>دل کو که ناوک دگری را سپر شویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مقصود خسرو است ز تو یک نظر که تا</p></div>
<div class="m2"><p>هر روز نیم کشته آن یک نظر شویم</p></div></div>