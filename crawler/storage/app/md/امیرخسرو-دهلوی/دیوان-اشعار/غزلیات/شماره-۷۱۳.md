---
title: >-
    شمارهٔ ۷۱۳
---
# شمارهٔ ۷۱۳

<div class="b" id="bn1"><div class="m1"><p>شب ز سوزی که بر این جان حزین می‌گذرد</p></div>
<div class="m2"><p>شعله آه من از چرخ برین می‌گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم و گریه خون هر شب و کس آگه نیست</p></div>
<div class="m2"><p>با که گویم که مرا حال چنین می‌گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوزم آن نیست که از تشنگیم سینه بسوخت</p></div>
<div class="m2"><p>آن است سوزم که به دل ماء معین می‌گذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد، از صومعه زنهار که بیرون نروی</p></div>
<div class="m2"><p>که ازان سوی بلای دل و دین می‌گذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌گذشتی شب و از ماه برآمد فریاد</p></div>
<div class="m2"><p>کاین چه فتنه است که بر روی زمین می‌گذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باد از بوی تو مست است دلیریش نگر</p></div>
<div class="m2"><p>که دوان پیش شه تخت‌نشین می‌گذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قطب دنیا که فلک هرچه کند کار تمام</p></div>
<div class="m2"><p>همه در حضرت آن رای متین می‌گذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر کنی جور وگر تیغ زنی بر خسرو</p></div>
<div class="m2"><p>همچنان دان که همان نیز و همین می‌گذرد</p></div></div>