---
title: >-
    شمارهٔ ۹۸۱
---
# شمارهٔ ۹۸۱

<div class="b" id="bn1"><div class="m1"><p>هر کرا خال عنبرین باشد</p></div>
<div class="m2"><p>گر کند ناز، نازنین باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمزه ات چون کمین کند بر خلق</p></div>
<div class="m2"><p>ترک جانباز در کمین باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی تو خرمن گلی ست، از آنک</p></div>
<div class="m2"><p>خرمن ماه خوشه چین باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ترا نیز قصد جان و دل است</p></div>
<div class="m2"><p>کار ما نزد عقل و دین باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سماعی که عشقبازان را</p></div>
<div class="m2"><p>بزم پر آه آتشین باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آستین برفشان که بهر نثار</p></div>
<div class="m2"><p>همه را جان در آستین باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش رخساره منور تو</p></div>
<div class="m2"><p>روی خورشید بر زمین باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آفرین بر جمال تو که بر او</p></div>
<div class="m2"><p>ز آفریننده آفرین باشد</p></div></div>