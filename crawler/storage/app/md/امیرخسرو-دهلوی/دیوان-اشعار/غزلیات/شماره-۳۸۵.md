---
title: >-
    شمارهٔ ۳۸۵
---
# شمارهٔ ۳۸۵

<div class="b" id="bn1"><div class="m1"><p>باز مست آمدنش نازکنان از جایی ست</p></div>
<div class="m2"><p>زان یکی کار در آن کنج دهان از جایی ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل سبک می شودم، دوش مگر غایب بود</p></div>
<div class="m2"><p>این زمان در سرش، این خواب گران از جایی ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز دیوانه ام و سلسله صبر کسی ست</p></div>
<div class="m2"><p>آب چشمم به چپ و راست دوان از جایی ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من ز تو صبر ندارم، تو نکو می دانی</p></div>
<div class="m2"><p>این همه ناز تو، ای جان جهان، از جایی ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند خونابه من بینی و نادان گردی</p></div>
<div class="m2"><p>اشک من آخر ازین گونه روان از جایی ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من چه زهره که دل گم شده جویم ز تو لیک</p></div>
<div class="m2"><p>مردمان را که رود بر تو گمان، از جایی ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر رهت، هیچ گلی نشکفد، ای باد، ازانک</p></div>
<div class="m2"><p>با تو امروز نسیم است که آن از جایی ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود گرفتم که بپوشد غم خود را خسرو</p></div>
<div class="m2"><p>نامت آخر شب و روزش به زبان از جایی ست</p></div></div>