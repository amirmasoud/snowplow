---
title: >-
    شمارهٔ ۱۵۱۹
---
# شمارهٔ ۱۵۱۹

<div class="b" id="bn1"><div class="m1"><p>مبارک باد، ماه روزه داران!</p></div>
<div class="m2"><p>بدان مستی فزای هوشیاران!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مده، ای محتسب، تشویش چشمش</p></div>
<div class="m2"><p>که در خواب خوشند آن پر خماران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز گریه بیش می سوزیم با آنک</p></div>
<div class="m2"><p>نگیرد هیمه آتش ز باران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخت در چشم مشتاقان چنانست</p></div>
<div class="m2"><p>که شربت در دهان روزه داران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورد خون من آن کافر همه روز</p></div>
<div class="m2"><p>گوارا باد می بر باده خواران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غنیمت دار خواب بی غمی را</p></div>
<div class="m2"><p>که شب ناخوش بود بر سوکواران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیار آن ده قدح، ای ساقی هوش</p></div>
<div class="m2"><p>که بر خسرو نبود این می گواران</p></div></div>