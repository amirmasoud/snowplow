---
title: >-
    شمارهٔ ۱۲۸۳
---
# شمارهٔ ۱۲۸۳

<div class="b" id="bn1"><div class="m1"><p>ز هر موی تو دل در بند دارم</p></div>
<div class="m2"><p>دلم خون گشت، پنهان چند دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سوگند تو جان را بسته ام، وای</p></div>
<div class="m2"><p>که چندش دل بر این سوگند دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمت با خویشتن گویم همه شب</p></div>
<div class="m2"><p>بدینسان خویش را خرسند دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برو جایی که من می دانم، ای باد</p></div>
<div class="m2"><p>که من آنجا دلی در بند دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا از صحبت جان شرم بادا</p></div>
<div class="m2"><p>که با جز تو چرا پیوند دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دهندم پند گفتار تو در گوش</p></div>
<div class="m2"><p>چه گوش خویش سوی پند دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خسرو ده که من ناداده وامی</p></div>
<div class="m2"><p>بر آن لبهای شکر خند دارم</p></div></div>