---
title: >-
    شمارهٔ ۶۰۳
---
# شمارهٔ ۶۰۳

<div class="b" id="bn1"><div class="m1"><p>ناله برآید هر طرف کان بت خرامان در رسد</p></div>
<div class="m2"><p>فریاد بلبل خوش بود چون گل به بستان در رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خود نخواهم برد جان از سختی هجران، ولی</p></div>
<div class="m2"><p>ای عمر، چندان صبر کن کان سست پیمان در رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمد خیالش نیم شب، جان دادم و گشتم خجل</p></div>
<div class="m2"><p>خجلت بود درویش را، یکدم چو مهمان در رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب در میان کشتگان بشیند چون نالیدنم</p></div>
<div class="m2"><p>گفتا که می کن، یک دو شب این هم به پایان در رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دل که بدخو می کنی از دیدنش چشم مرا</p></div>
<div class="m2"><p>معلوم گردد، باش تا شبهای هجران در رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امروز میرم پیش تو شرمسار دل شوی</p></div>
<div class="m2"><p>بر تو چه منت جان من، فردا که فرمان در رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آزرده تر زان است دل پیشت که بود اول بسی</p></div>
<div class="m2"><p>ویرانه ویرانه تر شود جایی که سلطان در رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر پنج روز نیکویی چندین مناز و بد مکن</p></div>
<div class="m2"><p>تا چشم را بر هم زنی، بینی که پایان در رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر خسروا، می سوزدت از خامیش رنجه مشو</p></div>
<div class="m2"><p>بسیار باید تا هنوز آن شوخ نادان در رسد</p></div></div>