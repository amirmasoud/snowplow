---
title: >-
    شمارهٔ ۹۹۶
---
# شمارهٔ ۹۹۶

<div class="b" id="bn1"><div class="m1"><p>دل بدین و بدو نخواهم داد</p></div>
<div class="m2"><p>جز به یار نکو نخواهم داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی تو، ای آرزوی سینه من</p></div>
<div class="m2"><p>سینه را آرزو نخواهم داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهر تو بر کسی نخواهم بست</p></div>
<div class="m2"><p>آب حیوان به جو نخواهم داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به بستان شکوفه خواهم شد</p></div>
<div class="m2"><p>بیوفایی چو تو نخواهم داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوسه ای گفته ای، توقف چیست؟</p></div>
<div class="m2"><p>یا بده یا بگو، «نخواهم » داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با رخت سوی گل نظر نکنم</p></div>
<div class="m2"><p>دل به رنگ و به بو نخواهم داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سگ کویت گزید خسرو را</p></div>
<div class="m2"><p>بعد ازین هم از او نخواهم داد</p></div></div>