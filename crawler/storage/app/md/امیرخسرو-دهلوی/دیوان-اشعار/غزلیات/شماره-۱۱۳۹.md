---
title: >-
    شمارهٔ ۱۱۳۹
---
# شمارهٔ ۱۱۳۹

<div class="b" id="bn1"><div class="m1"><p>خرابی من از آن چشم پر خماری پرس</p></div>
<div class="m2"><p>هلاک جانم از آن لاله بهاری پرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز زخم غمزه چه پرسی که در جگر چند است؟</p></div>
<div class="m2"><p>ز صد فزونست، ولی زخمهای کاری پرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غلام چشم توام، گر چه ناوک تو خوش است</p></div>
<div class="m2"><p>ولیک لذت آن از دل شکاری پرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم که زود فراموش می کند خود را</p></div>
<div class="m2"><p>مپرس هیچ ز هجران و بیقراری پرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مراست دردسری از خمار مستی عشق</p></div>
<div class="m2"><p>علاج دردم از آن نرگس خماری پرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجاست دولت آنم که بر درت باشم؟</p></div>
<div class="m2"><p>نشان من به سر کوی خاکساری پرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رو، ای صبا و ز بهر مسافران فراق</p></div>
<div class="m2"><p>از آن دو لب سخنی چند یادگاری پرس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرود ذوق فراوان شنیده ای، اکنون</p></div>
<div class="m2"><p>بیا، ز خسرو ذوق فغان و زاری پرس</p></div></div>