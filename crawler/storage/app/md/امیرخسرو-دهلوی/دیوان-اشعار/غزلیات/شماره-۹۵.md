---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>شب به روز آمد بسی کز دل نهادی یاد را</p></div>
<div class="m2"><p>جان ز تن آمد برون بویی ندادی باد را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر به دیوار سرایت می زنم تا بنگری</p></div>
<div class="m2"><p>زانکه با باز شکاری خوش بود صیاد را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بازوی هجرت قوی در کشتن بیچارگان</p></div>
<div class="m2"><p>چون قصاص افزون فتد عادت شود جلاد را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان به فریادم برآمد، لیک صد جان آرزو</p></div>
<div class="m2"><p>بشنوی و راه ندهی سوی جان فریاد را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که می گویی که وقتی لوح صبرت باد برد</p></div>
<div class="m2"><p>سالها شد تا فرامش کرده ام آن یاد را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این همه خونابه کاشامم همی زین روز بد</p></div>
<div class="m2"><p>بهترین روزی خلل اندازد این بنیاد را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چند گریم چون سیه رویی عشقم از قضاست</p></div>
<div class="m2"><p>آب کی شستن تواند داغ مادرزاد را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا به سوی گفت شیرین ست، دل خارا و کوه</p></div>
<div class="m2"><p>کندن از ناخن چو گل چیدن بود فرهاد را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نوک مژگان تو در دل ماند خسرو را چنانک</p></div>
<div class="m2"><p>در رگ بیمار نشتر بشکند فصاد را</p></div></div>