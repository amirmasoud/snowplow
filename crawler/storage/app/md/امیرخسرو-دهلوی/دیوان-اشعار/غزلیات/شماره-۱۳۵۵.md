---
title: >-
    شمارهٔ ۱۳۵۵
---
# شمارهٔ ۱۳۵۵

<div class="b" id="bn1"><div class="m1"><p>من اگر بر در تو هر شبی افغان نکنم</p></div>
<div class="m2"><p>خویش را شهره و بدنام بدینسان نکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دهم دردسری تنگ میا بر من، ازآنک</p></div>
<div class="m2"><p>نتوانم که تو را بینم و افغان نکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی از یاد رخت پیش گلی خواهم مرد</p></div>
<div class="m2"><p>من همان به که گذر بیش به بستان نکنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وه که دیوانه دلم باز به بازار افتاد</p></div>
<div class="m2"><p>من نمی گفتم کافسانه هجران نکنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم خورد این دل بیچاره، زبانش دادی</p></div>
<div class="m2"><p>بعد از این چاره همانست که درمان نکنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آشنایان همه بیگانه شدند از من، از آنک</p></div>
<div class="m2"><p>هر کسی مصلحتی گوید و من آن نکنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکر گویم ز تو، ای توبه که کورم کردی</p></div>
<div class="m2"><p>تا نظر بازی از این پیش به خوبان نکنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلق گویند «دعا خواه ز خوبان » نروم</p></div>
<div class="m2"><p>روزگار خوش درویش پریشان نکنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند گویند، که خسرو، ز بتان چشم بدوز</p></div>
<div class="m2"><p>گر میسر شودم روی بدیشان نکنم</p></div></div>