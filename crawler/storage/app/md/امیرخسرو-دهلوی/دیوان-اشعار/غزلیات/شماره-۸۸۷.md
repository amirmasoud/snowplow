---
title: >-
    شمارهٔ ۸۸۷
---
# شمارهٔ ۸۸۷

<div class="b" id="bn1"><div class="m1"><p>یک روز یار اگر قدمی سوی من زند</p></div>
<div class="m2"><p>بخت رمیده خیمه به پهلوی من زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم هزار جان ز خدا تا کنم نثار</p></div>
<div class="m2"><p>در هر قدم که سرو سمن بوی من زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خورد دوست نیست مگر اشک چشم من</p></div>
<div class="m2"><p>در پیش مردمان همه در روی من زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردم در انتظار که کی حلقه بر درم</p></div>
<div class="m2"><p>زلف نگار سلسله گیسوی من زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمش هزار قلب شکست، از مژه هنوز</p></div>
<div class="m2"><p>لشکر کشد که بر دل بدخوی من زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خسرو، ز باد صبح رخش دم زنیم و بس</p></div>
<div class="m2"><p>لاف محبتش سر هر موی من زند</p></div></div>