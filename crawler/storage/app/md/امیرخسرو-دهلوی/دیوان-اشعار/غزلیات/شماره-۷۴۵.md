---
title: >-
    شمارهٔ ۷۴۵
---
# شمارهٔ ۷۴۵

<div class="b" id="bn1"><div class="m1"><p>بس که خون جگر از راه نظر بیرون شد</p></div>
<div class="m2"><p>دل نمی باید ازین ورطه ره بیرون شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناوک چشم تو تا خون دلم ریخت ز چشم</p></div>
<div class="m2"><p>در میان دل و چشم من آن دم خون شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تب هجر بمردیم به کنج غم و هیچ</p></div>
<div class="m2"><p>کس نپرسید که آن خسته غمگین چون شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چو ماه نو ازان مهر جدا افتادم</p></div>
<div class="m2"><p>عمر من کم شد و مهر رخ او افزون شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نه زنجیر دل از طره خوبان کردند</p></div>
<div class="m2"><p>زلف لیلی ز چه رو سلسله مجنون شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار چون درج عقیقی به تبسم بگشاد</p></div>
<div class="m2"><p>چشم خسرو چو صدف پر ز در مکنون شد</p></div></div>