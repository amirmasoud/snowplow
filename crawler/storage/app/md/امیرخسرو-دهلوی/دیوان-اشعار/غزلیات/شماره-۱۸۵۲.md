---
title: >-
    شمارهٔ ۱۸۵۲
---
# شمارهٔ ۱۸۵۲

<div class="b" id="bn1"><div class="m1"><p>نوبهار است و گل و موسم عید، ای ساقی</p></div>
<div class="m2"><p>باده نوش و گذر از وعد و وعید، ای ساقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز محشر نبود هیچ حسابش به یقین</p></div>
<div class="m2"><p>هر که در کوی مغان گشت شهید، ای ساقی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشت پیمانه چو تسبیح روان در کف شیخ</p></div>
<div class="m2"><p>تا ز لعل تو یکی جرعه کشید، ای ساقی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حاصل از عمر ندارد به جز از حسرت و درد</p></div>
<div class="m2"><p>هر که عید است ز میخانه بعید، ای ساقی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه در کوی محبت قدم از صدق نهاد</p></div>
<div class="m2"><p>دگر او پند ادیبان نشنید، ای ساقی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بارها کرده بدم توبه ز می، باز مرا</p></div>
<div class="m2"><p>چشم مست تو به میخانه کشید، ای ساقی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهد از شرم تو دایم سرانگشت گزد</p></div>
<div class="m2"><p>جز در میکده جایی مگرید، ای ساقی</p></div></div>