---
title: >-
    شمارهٔ ۱۳۳۹
---
# شمارهٔ ۱۳۳۹

<div class="b" id="bn1"><div class="m1"><p>خرم آن روزی که من با دوست کاری داشتم</p></div>
<div class="m2"><p>با وصال او به شادی روزگاری داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داشتم، باری از این اندیشه کاید جان برون</p></div>
<div class="m2"><p>بر زبان راندن نمی آرم که یاری داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تن چو گل صد پاره شد، از بس که غلتیدم به خاک</p></div>
<div class="m2"><p>از فسون آن که خرم نوبهاری داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش نیاید کایم از خانه برون کاین خانه را</p></div>
<div class="m2"><p>دوست می دارم که در وی دوستداری داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست رنجی گر تن از غم مو شد و رنج است و بس</p></div>
<div class="m2"><p>کان ز تار موی خوبان یادگاری داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند گویی «صبر کن تا روز شادی در رسد»</p></div>
<div class="m2"><p>طاقتم شد، صبر کردم تا قراری داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق گوید، خسروا، وقتی دل خوش داشتی</p></div>
<div class="m2"><p>این زمان چون نیست، چون گویم که «آری داشتم »</p></div></div>