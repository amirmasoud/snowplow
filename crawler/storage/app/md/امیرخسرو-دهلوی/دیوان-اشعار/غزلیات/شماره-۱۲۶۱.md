---
title: >-
    شمارهٔ ۱۲۶۱
---
# شمارهٔ ۱۲۶۱

<div class="b" id="bn1"><div class="m1"><p>ای گل، صفت حسنت بر وجه حسن گویم</p></div>
<div class="m2"><p>سر تا به قدم جانی، کفر است که تن گویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن میم دهان داند از ابروی چون نونش</p></div>
<div class="m2"><p>نی نی که غلط گفتم، من دانم و من گویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هی هی سخن کفرست آن موی رسن گفتن</p></div>
<div class="m2"><p>ببریده زبان بادم، گر بیش رسن گویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلفی که ازو آید بویی چو دم عیسی</p></div>
<div class="m2"><p>بس فکر خطا باشد گر مشک ختن گویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمم که دو صد دریا دارد نه به هر مژگان</p></div>
<div class="m2"><p>این قلزم پر خون را چون نام عدن گویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیراهن خود گلها سازند قبا در خون</p></div>
<div class="m2"><p>گر از رخ جانبخشت وصفی به ختن گویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی ز دهان من، خسرو، تو حدیثی گوی</p></div>
<div class="m2"><p>در وصف دهان تو من خود چه سخن گویم؟</p></div></div>