---
title: >-
    شمارهٔ ۸۹۰
---
# شمارهٔ ۸۹۰

<div class="b" id="bn1"><div class="m1"><p>به بام خویش چو آن ماه کج کلاه برآید</p></div>
<div class="m2"><p>نفیر و ناله من بر سپهر و ماه برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگه تو داریش از سوز جان خلق، خدایا</p></div>
<div class="m2"><p>چو او خرامد هر سو، هزار آه برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو چشم سرخ کنم بر رخش، ز دیده رود خون</p></div>
<div class="m2"><p>هزار آه که داد از دل سیاه برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فتاد در زنخ او، دلا، بمیر که زلفش</p></div>
<div class="m2"><p>نه رشته ایست کز او غرقه ای ز چاه برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز روی خوب مراد تو می دهند، ولیکن</p></div>
<div class="m2"><p>هزار توبه کجا پیش این گناه برآید؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبی پگاه ترک سر ز خواب ناز برآور</p></div>
<div class="m2"><p>که آفتاب نیارد که صبحگاه برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین که اختر خسرو به زیر خاک فرو شد</p></div>
<div class="m2"><p>مگر ز دولت شاه جهان پناه برآید</p></div></div>