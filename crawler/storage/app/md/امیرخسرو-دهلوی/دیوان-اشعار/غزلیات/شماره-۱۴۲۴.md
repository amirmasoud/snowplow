---
title: >-
    شمارهٔ ۱۴۲۴
---
# شمارهٔ ۱۴۲۴

<div class="b" id="bn1"><div class="m1"><p>برفت عمر و به سوی خدای روی نکردم</p></div>
<div class="m2"><p>بشد غنیمت و اوقات جستجوی نکردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز لوث فسق دل من چگونه دست بشوید؟</p></div>
<div class="m2"><p>به غسل جای ندامت چو دیده چوی نکردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیاه رویی خود را به آب دیده نشستم</p></div>
<div class="m2"><p>به صف مردان خود را سفید روی نکردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طریق شیردلی های شبروان چه شناسم</p></div>
<div class="m2"><p>که صحبتی دو سه شب باسگان کوی نکردم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجا به حضرت سلطان قبول حال بیاید</p></div>
<div class="m2"><p>سری که در خم چوگان عشق گوی نکردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دماغ کرد چنینم که طیب خلق ندانم</p></div>
<div class="m2"><p>زکام داشت بر آنم که مشک بوی نکردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ترک خوی بدم می دهند پند، ولیکن</p></div>
<div class="m2"><p>کنون چگونه کنم، کز نخست خوی نکردم؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تمام عمر برانداختم به کذب که هرگز</p></div>
<div class="m2"><p>به صدق پیش خدا قامت دو توی نکردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وبال من همه شعر آمد و دریغ که خسرو</p></div>
<div class="m2"><p>نگفت «خاموش » و من ترک گفتگوی نکردم</p></div></div>