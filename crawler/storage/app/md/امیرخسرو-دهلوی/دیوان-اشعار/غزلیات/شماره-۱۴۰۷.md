---
title: >-
    شمارهٔ ۱۴۰۷
---
# شمارهٔ ۱۴۰۷

<div class="b" id="bn1"><div class="m1"><p>شب تا به روز خون جگر نوش کرده ام</p></div>
<div class="m2"><p>خوش عشرتی ست این که شب دوش کرده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون شد حرام شرع، ولی من چو عاشقم</p></div>
<div class="m2"><p>بر من حلال باد که خوش نوش کرده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر سرو و لاله ای بر برم نیست، این بس است</p></div>
<div class="m2"><p>کز خون دیده لاله در آغوش کرده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی «به فرق بر سر کویم طواف کن »</p></div>
<div class="m2"><p>زین لطف پای خویش فراموش کرده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این سر که نیست یک نفس از درد عشق دور</p></div>
<div class="m2"><p>باری ز محنتی ست که بر دوش کرده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکشید وه مرا که نخفته ست آن نگار</p></div>
<div class="m2"><p>زان ناله ها که شب من بیهوش کرده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویند «کز چه عاشق دیوانه ای گشته ای؟»</p></div>
<div class="m2"><p>گفتار خسرو است که در گوش کرده ام</p></div></div>