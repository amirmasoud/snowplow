---
title: >-
    شمارهٔ ۳۸۴
---
# شمارهٔ ۳۸۴

<div class="b" id="bn1"><div class="m1"><p>بدان بهانه که حسنی ست بس فراوانت</p></div>
<div class="m2"><p>جفا بکن که هر آن کرده نیست تاوانت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهی که چاک به دامان جانم افگنده ست</p></div>
<div class="m2"><p>همان مهی ست که طالع شد از گریبانت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی که جان به سر یک نظاره خواهند داد</p></div>
<div class="m2"><p>رهاش کن که نگه می کند فراوانت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نزد تست دلم باژگونه کن که در او</p></div>
<div class="m2"><p>کنی نظاره که چندست داغ پنهانت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگر که از زنخت چند دل به چاه افتاد</p></div>
<div class="m2"><p>که تا لب است پر از جان چه زنخدانت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درونت در جگر سوخته کشم هر چند</p></div>
<div class="m2"><p>که سر به سر ز نمک ساخته ست یزدانت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نیم خنده چو صد جان دهی تو خسرو را</p></div>
<div class="m2"><p>به نیم جان چه توان داد مزد دندانت</p></div></div>