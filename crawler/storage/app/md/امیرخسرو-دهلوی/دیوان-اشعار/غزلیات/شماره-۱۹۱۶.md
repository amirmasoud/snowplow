---
title: >-
    شمارهٔ ۱۹۱۶
---
# شمارهٔ ۱۹۱۶

<div class="b" id="bn1"><div class="m1"><p>ای که امروز به زیبایی او می نازی</p></div>
<div class="m2"><p>جای آن است که بر ماه کنی طنازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوسه ای چند بخواهم ز لبت</p></div>
<div class="m2"><p>چشم تو گر نکند پیش لبت غمازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا که در سینه کنون تخم وفایت کارد</p></div>
<div class="m2"><p>اشک با خون دل بنده کند انبازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود کشی عاشق و بر طره مشکین بندی</p></div>
<div class="m2"><p>خود دلم دزدی و اندر سر زلف اندازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از رخت بنده چه بریست به جز دلسوزی؟</p></div>
<div class="m2"><p>بلبل از لاله چه آموخت جز آتش بازی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم تو با همه بد می کند، الا با تو</p></div>
<div class="m2"><p>زانکه با غمزه بدساز نکو می سازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من ز اندوه چو خسرو به تو پرداخته ام</p></div>
<div class="m2"><p>تو پی آنکه به من هیچ نمی پردازی</p></div></div>