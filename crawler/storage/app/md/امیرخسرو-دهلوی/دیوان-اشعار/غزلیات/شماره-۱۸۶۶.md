---
title: >-
    شمارهٔ ۱۸۶۶
---
# شمارهٔ ۱۸۶۶

<div class="b" id="bn1"><div class="m1"><p>آمد آن شادی جان بر ما دی</p></div>
<div class="m2"><p>شادی افزود مرا بر شادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پایش افتادم و لب بگرفتم</p></div>
<div class="m2"><p>گفت، بگذار، کجا افتادی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم آن کردم، چون باد صبا</p></div>
<div class="m2"><p>از دل غنچه گره نگشادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو در آرزوی بندگیت</p></div>
<div class="m2"><p>گله ها می کند از آزادی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاد داری که از این پیش ز لطف</p></div>
<div class="m2"><p>باده بر یاد خودم می دادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد بیداد تو بر خسرو جور</p></div>
<div class="m2"><p>نستد دارویی از بیدادی</p></div></div>