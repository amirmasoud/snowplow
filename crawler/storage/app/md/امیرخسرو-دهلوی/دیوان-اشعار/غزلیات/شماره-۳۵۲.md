---
title: >-
    شمارهٔ ۳۵۲
---
# شمارهٔ ۳۵۲

<div class="b" id="bn1"><div class="m1"><p>دامن گل ز ابر پر گهر است</p></div>
<div class="m2"><p>باغ را زیب و زینت دگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غنچه بر باد داد دل، چو گشاد</p></div>
<div class="m2"><p>چشم بر گل که مو به روی فر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یکی جام کش رسید از دور</p></div>
<div class="m2"><p>نرگس افتاده مست و بی خبر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه از سرو می برد بلبل</p></div>
<div class="m2"><p>نیک یکبارگی بلند پر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه تسخیر کیف یحیی الارض</p></div>
<div class="m2"><p>خواند بلبل به خط سبزه در است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل ورق راست کرده از شبنم</p></div>
<div class="m2"><p>مهره آن ورق همه گهر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوستان را کنون ز بهر نشاط</p></div>
<div class="m2"><p>جانب باغ و بوستان گذر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ورق گل اگر لطیف افتاد</p></div>
<div class="m2"><p>خط سبزه ازان لطیف تر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نرود سوی باغ خسرو، ازآنک</p></div>
<div class="m2"><p>باغ او بزم شاه نامور است</p></div></div>