---
title: >-
    شمارهٔ ۱۰۲۳
---
# شمارهٔ ۱۰۲۳

<div class="b" id="bn1"><div class="m1"><p>رسید موسم عید و صلای می درداد</p></div>
<div class="m2"><p>پیاله بر کف خوبان ماه پیکر داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میی که ساقی رعنا ز خون مستان خورد</p></div>
<div class="m2"><p>چه خوابها که بدان غمزه های کافر داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر بر آب خود آیم ز خشکی روزه</p></div>
<div class="m2"><p>دو سه پیاله بباید مرا سراسر داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسان نیمه بیضه ز جام نقره تمام</p></div>
<div class="m2"><p>که نقل مجلس مستان بط و کبوتر داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خضر بریخت به ساغر ز می که آب حیات</p></div>
<div class="m2"><p>پس آن گهی به کف ثانی سکندر داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر آستانش، خسرو، نثار موسم عید</p></div>
<div class="m2"><p>به وزن شعر همه برکشیده گوهر داد</p></div></div>