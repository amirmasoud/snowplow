---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>یار دل برداشت وز رنج دل ما غم نداشت</p></div>
<div class="m2"><p>زهره ام کرد آب و تیمار من در هم نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریه ها کردم که خون شد سنگ خارا را جگر</p></div>
<div class="m2"><p>سنگدل یارم که چشمش قطره زان نم نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماجرای درد خود بر روی او صد بار پیش</p></div>
<div class="m2"><p>یک به یک گفتیم و او را ذره ای زان غم نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دی برون رفتم فغانها کردم و بگریستم</p></div>
<div class="m2"><p>بود او در خواب مستی و غم عالم نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش بیخود بوده ام در بستر غم تا به چاشت</p></div>
<div class="m2"><p>همچنان می سوخت شمع و دیده من دم نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای که گویی خوشدلی، یارب، همین در عهد ما</p></div>
<div class="m2"><p>گشت پنهان یا کسی خود از بنی آدم نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبر خود یکبارگی زانگونه از ما برگذشت</p></div>
<div class="m2"><p>هیچ گه گویی که با ما آشنایی هم نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیر زی، ای عشق کز اقبال تو پاینده بود</p></div>
<div class="m2"><p>این متاع انده و غم، هیچ چیزی کم نداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این دل خسرو که از عشق جوانان پخته شد</p></div>
<div class="m2"><p>همچنان خون ماند کز شیرین لبی مرهم نداشت</p></div></div>