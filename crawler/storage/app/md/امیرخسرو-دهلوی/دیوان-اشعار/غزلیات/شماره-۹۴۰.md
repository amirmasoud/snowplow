---
title: >-
    شمارهٔ ۹۴۰
---
# شمارهٔ ۹۴۰

<div class="b" id="bn1"><div class="m1"><p>اگر چه با تو حدیث جفا بخواهم کرد</p></div>
<div class="m2"><p>ولیک، تا بتوانم، وفا بخواهم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من این بلا همه از دیده دیده ام او را</p></div>
<div class="m2"><p>بنا نمودن رویت سزا بخواهم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به راه وصل به یک بوسه جان بخواهم یافت</p></div>
<div class="m2"><p>ولیک وقت شمردن ادا بخواهم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خطاست بوسه زدن بر لب و دهان تو، لیک</p></div>
<div class="m2"><p>تو خواه تیغ بزن، من خطا بخواهم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشم به کوی تو ناگه رقیب کافرکیش</p></div>
<div class="m2"><p>من این غزا ز برای خدا بخواهم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو دین به کار بتان رفت پیش بت، پس ازین</p></div>
<div class="m2"><p>نماز اگر چه نباشد روا، بخواهم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر آن نماز که ناکرده ماند پیش بتان</p></div>
<div class="m2"><p>اگر خدای بخواهد، قضا بخواهم کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>و ان یکاد به روی نکو بخواهم خواند</p></div>
<div class="m2"><p>نه بهر دیده بد هم دعا بخواهم کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو دل برفت ز خسرو، چه سود بندد صبر؟</p></div>
<div class="m2"><p>چو دل بیامد، وقف شما بخواهم کرد</p></div></div>