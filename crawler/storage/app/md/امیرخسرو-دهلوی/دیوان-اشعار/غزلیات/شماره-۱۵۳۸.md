---
title: >-
    شمارهٔ ۱۵۳۸
---
# شمارهٔ ۱۵۳۸

<div class="b" id="bn1"><div class="m1"><p>مانا که بگشاید دلم، بندی ز گیسو باز کن</p></div>
<div class="m2"><p>گم گشتگان عشق را پنهان یکی آواز کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمهاست در هر دل ز تو، هر یک به دیگر چاشنی</p></div>
<div class="m2"><p>ما نیز گرم ذوق غم با هر یکی انباز کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گو تا مرا در کوی تو سوسند پیش عاشقان</p></div>
<div class="m2"><p>بازار تو چون گرم شد، بر من دو دیده باز کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه جان درون و گه برون، کارم مگر یکتا شود</p></div>
<div class="m2"><p>نازی که اول کرده ای، یک بار دیگر باز کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش رقیب کافرت در داد ما را چشم تو</p></div>
<div class="m2"><p>گر ذکر کشتن می کنی، هم ذکر آن غماز کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز آمد این باد صبا، آورد بویی از چمن</p></div>
<div class="m2"><p>ای مرغ جان، بشکن قفس، هم سوی او پرواز کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگشاد عشق از دیده خون، نالان شو، ای شخص نگون</p></div>
<div class="m2"><p>آمد شراب تو کنون، جنگ کهن را ساز کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون زاهد ما توبه را بشکست، عاشق شد ترا</p></div>
<div class="m2"><p>خواهی برو جرعه فشان، خواهیش سنگ انداز کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بت پرستان را رسد بر تارک از خواری لگد</p></div>
<div class="m2"><p>آغاز آن، ای محتسب، زین پیر شاهد باز کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خسرو، تو بر وی کی رسی، لیکن به کویش کن گذر</p></div>
<div class="m2"><p>در خاک با هر ذره ای بنشین، بیان راز کن</p></div></div>