---
title: >-
    شمارهٔ ۱۵۴۶
---
# شمارهٔ ۱۵۴۶

<div class="b" id="bn1"><div class="m1"><p>آفت زهد و توبه شد ترک شرابخوار من</p></div>
<div class="m2"><p>یار گر اوست، کی شود توبه و زهد یار من؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده هجر خوردنم، رنج خمار در تنم</p></div>
<div class="m2"><p>جز ز حلاوت لبش نشکند این خمار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای چو تویی نخاسته پهلوی من نشین دمی</p></div>
<div class="m2"><p>تا بنشیند از درون آتش انتظارمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رغبت اگر نمی کنم، ساقی خون خود شوم</p></div>
<div class="m2"><p>مطرب رایگان تو ناله زیروار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی تو دو چشم چار شد، خاک در تو سرمه ام</p></div>
<div class="m2"><p>سرمه گر از تو بایدم، خاک به هر چهار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون تو سوار بگذری، دیده گهر فشان کنم</p></div>
<div class="m2"><p>خواه قبول و خواه رد، نیست جز این نثار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس که پر از غبار شد دل ز تو، گر نفس زنم</p></div>
<div class="m2"><p>خاک به رویم افگند، این دل پر غبار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنجه مشو به کشتنم، زان که به رخصت غمت</p></div>
<div class="m2"><p>فتنه تمام می کند محنت نیم کار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لاغ مکن که خسروا، دامن خود ز من مکش</p></div>
<div class="m2"><p>چون که ز دست من بشد دامن اختیار من</p></div></div>