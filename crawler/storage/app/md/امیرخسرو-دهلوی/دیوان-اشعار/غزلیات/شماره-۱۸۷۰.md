---
title: >-
    شمارهٔ ۱۸۷۰
---
# شمارهٔ ۱۸۷۰

<div class="b" id="bn1"><div class="m1"><p>ای باد باز بر سر کوی که می روی؟</p></div>
<div class="m2"><p>بوی که رهبرت شد و سوی که می روی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان گل و شکوفه که هستند خاک پات</p></div>
<div class="m2"><p>در جستجوی روی نکوی که می روی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با این نسیم خوش که تو داری به بوستان</p></div>
<div class="m2"><p>جایی دگر بگو که به بوی که می روی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زینگونه کز تو طره سنبل معطر است</p></div>
<div class="m2"><p>تو بهر بوی کردن بوی که می روی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش می شود دلت که گذر می کنی به باغ</p></div>
<div class="m2"><p>دانی به گرد گلبن روی که می روی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنجا روی مگر که جهانی اسیر دل</p></div>
<div class="m2"><p>در کوی تو روان، تو به کوی که می روی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو ز تشنگی بیابان هجر سوخت</p></div>
<div class="m2"><p>ای آب زندگی، تو به جوی که می روی</p></div></div>