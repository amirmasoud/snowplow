---
title: >-
    شمارهٔ ۱۰۷۷
---
# شمارهٔ ۱۰۷۷

<div class="b" id="bn1"><div class="m1"><p>ای شمع، رخ تو مطلع نور</p></div>
<div class="m2"><p>زین حسن و جمال چشم بد دور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با پرتو عارض تو خورشید</p></div>
<div class="m2"><p>چون شمع در آفتاب بی نور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخسار تو در جهان فروزی</p></div>
<div class="m2"><p>ماننده آفتاب مشهور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از روی تو شام صبح گردد</p></div>
<div class="m2"><p>وز زلف تو صبح شام دیجور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>انگیخته شام را ز خورشید</p></div>
<div class="m2"><p>آمیخته مشک را ز کافور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دست غم تو در زمانه</p></div>
<div class="m2"><p>یک خانه دل نماند معمور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر دار غمت حلال باشد</p></div>
<div class="m2"><p>زو وصل تو گشته همچو منصور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاطر نرود به گلستانی</p></div>
<div class="m2"><p>آن را که جمال تست منظور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو که همیشه بر در تست</p></div>
<div class="m2"><p>از درگه خود مکن ورا دور</p></div></div>