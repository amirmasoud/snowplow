---
title: >-
    شمارهٔ ۱۱۶۵
---
# شمارهٔ ۱۱۶۵

<div class="b" id="bn1"><div class="m1"><p>آن سخن گفتن تو هست هنوزم در گوش</p></div>
<div class="m2"><p>وان شکر خنده شیرین تو از چشمه نوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریه می آیدم از دور به آواز بلند</p></div>
<div class="m2"><p>که ازان گریه نمی آیدم آواز به گوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر و قد، از چمن سبز به بیرون چه روی؟</p></div>
<div class="m2"><p>سر برون نازده از لاله تر مرزنگوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش در خواب بدیدم رخ چون خورشیدت</p></div>
<div class="m2"><p>نیم شب روز شد از شعله آهم شب دوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای به خشم از بر من رفته و تنها خفته</p></div>
<div class="m2"><p>چشم را گوی که چندین طرف خواب بپوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خسروا، گرم برون می دودت خواب از چشم</p></div>
<div class="m2"><p>دیگ دل شد مگر از پختن سودا خاموش</p></div></div>