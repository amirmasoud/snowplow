---
title: >-
    شمارهٔ ۱۶۵۵
---
# شمارهٔ ۱۶۵۵

<div class="b" id="bn1"><div class="m1"><p>ای سبزه دمانید به گرد قمر از مو</p></div>
<div class="m2"><p>سر سبزی خط سیهت سر به سر از مو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مویی ست دهان تو و در موی شکافی</p></div>
<div class="m2"><p>هنگام سخن ریخته لؤلؤی تر از مو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس موی میانت نکند یک سر مو فرق</p></div>
<div class="m2"><p>تا ساخته ای موی میان را کمر از مو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیرون ز خیال تو که ماننده مویی ست</p></div>
<div class="m2"><p>کس بر تن سیمینت نبندد اثر از مو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز عارض سیمین تو بر طره شبرنگ</p></div>
<div class="m2"><p>هرگز نشنیدیم طلوع قمر از مو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر طرف بناگوش تو آن طره مشکین</p></div>
<div class="m2"><p>صد سلسله انگیخته بر یکدگر از مو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو که به وصف دهنت موی شکاف ست</p></div>
<div class="m2"><p>یک نکته نگوید ز دهانت مگر از مو</p></div></div>