---
title: >-
    شمارهٔ ۱۲۱۰
---
# شمارهٔ ۱۲۱۰

<div class="b" id="bn1"><div class="m1"><p>گل ز بیم باد زیر پرده می دارد چراغ</p></div>
<div class="m2"><p>آری، آری، باد را طاقت نمی آرد چراغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر شبی پروین که عکس خویش در آب افگند</p></div>
<div class="m2"><p>آسمان گویی میان آب می کارد چراغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگ می ریزد ز گل، دانم خزان خواهد رسید</p></div>
<div class="m2"><p>میهمان آید به خانه چون که گل بارد چراغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون در افتد برق در ابر سیه نظاره کن</p></div>
<div class="m2"><p>ابر را شب داند و آن را چه پندارد چراغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابرها تیره ست نگذارم می روشن ز کف</p></div>
<div class="m2"><p>کس به تاریکی روان از دست نگذارد چراغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی چراغی می جهان بر دیده خسرو شب است</p></div>
<div class="m2"><p>ساقی خورشید رویی کو که بسپارد چراغ؟</p></div></div>