---
title: >-
    شمارهٔ ۱۲۹۱
---
# شمارهٔ ۱۲۹۱

<div class="b" id="bn1"><div class="m1"><p>نهانی چند سوی یار بینم</p></div>
<div class="m2"><p>نهان دارم غم و آزار بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز صد جانب نظر دزدم که یک ره</p></div>
<div class="m2"><p>به دزدی سوی آن عیار بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی تنهاش خواهم یافت، یارب</p></div>
<div class="m2"><p>که بی اندیشه آن رخسار بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین هم هیچگه باشد، خدایا؟</p></div>
<div class="m2"><p>که سیر آن روی چون گلنار بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه عمرم در این حسرت به سر شد</p></div>
<div class="m2"><p>که رویش بینم و بسیار بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تماشا حیف باشد بی رخ دوست</p></div>
<div class="m2"><p>که جانان نبود و گلزار بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به روی گل توان دیدن چمن را</p></div>
<div class="m2"><p>چو گل نبود چه بینم، خار بینم؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رو، ای رضوان، تو دانی و بهشتت</p></div>
<div class="m2"><p>مرا بگذار تا دیدار بینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز غم شب می نخسپم، باشد آن روز</p></div>
<div class="m2"><p>که بخت خویش را بیدار بینم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرو گویم به چشمت قصه خویش</p></div>
<div class="m2"><p>اگر آن مست را هشیار بینم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنین کافتاد خسرو در ره عشق</p></div>
<div class="m2"><p>ره بیرون شدن دشوار بینم</p></div></div>