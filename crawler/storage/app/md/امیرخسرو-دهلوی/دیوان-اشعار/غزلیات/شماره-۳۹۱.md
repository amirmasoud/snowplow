---
title: >-
    شمارهٔ ۳۹۱
---
# شمارهٔ ۳۹۱

<div class="b" id="bn1"><div class="m1"><p>خنده هرگز دهنی همچو دهان تو نیافت</p></div>
<div class="m2"><p>سخن ار آب نشد طعم زبان تو نیافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده باریکی عالم همه موی اندر موی</p></div>
<div class="m2"><p>دید، لیکن سر مویی چو میان تو نیافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با چنین حسن و لطافت که تویی، دیده دهر</p></div>
<div class="m2"><p>سوی که دید که آن را نگران تو نیافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پسته کو تنگ دهان بود دهان بسته بماند</p></div>
<div class="m2"><p>خویش را دید که هم سنگ دهان تو نیافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گه صبح چو گل خنده زنان می رفتی</p></div>
<div class="m2"><p>باد هر چند که بشتافت نشان تو نیافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل به گلزار در از تابش خورشید بسوخت</p></div>
<div class="m2"><p>زان که او سایه ای از سرو روان تو نیافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پای خسرو خلش از خار جفا بار نداشت</p></div>
<div class="m2"><p>تا سرش ریخته در پای عنان تو نیافت</p></div></div>