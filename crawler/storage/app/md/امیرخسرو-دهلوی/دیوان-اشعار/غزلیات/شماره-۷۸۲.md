---
title: >-
    شمارهٔ ۷۸۲
---
# شمارهٔ ۷۸۲

<div class="b" id="bn1"><div class="m1"><p>عافیت را بر زمین گردی نماند</p></div>
<div class="m2"><p>مردمی را در جان مردی نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک بر فرق جهان زان کز وفا</p></div>
<div class="m2"><p>در همه روی زمین گردی نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان نمی خیزد چمن کز بهر او</p></div>
<div class="m2"><p>مرصبا را هم دم سردی نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کیمیا شد زر چنان کز رنگ او</p></div>
<div class="m2"><p>بوستان را هم گل زردی نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غصه را بر خود فروبر، خسروا</p></div>
<div class="m2"><p>چون همه درد است و همدردی نماند</p></div></div>