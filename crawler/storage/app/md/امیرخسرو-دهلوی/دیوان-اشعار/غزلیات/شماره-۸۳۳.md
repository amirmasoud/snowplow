---
title: >-
    شمارهٔ ۸۳۳
---
# شمارهٔ ۸۳۳

<div class="b" id="bn1"><div class="m1"><p>دی مست بوده ام که ز خویشم خبر نبود</p></div>
<div class="m2"><p>من بودم و دو محرم و یاری دگر نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می رفت آن سوار و بر او بود چشم من</p></div>
<div class="m2"><p>می شد ز سینه جان و در آنم نظر نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوز دلم بدید و ز چشمش نمی نریخت</p></div>
<div class="m2"><p>این یار خانه سوخته را اینقدر نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیوانه کرد عاشقی و بیدلی مرا</p></div>
<div class="m2"><p>یارب، دلم که برد، کجا شد، مگر نبود؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش بوده ام که با تو نگاهی نداشتم</p></div>
<div class="m2"><p>باری ز آب دیده ام این درد سر نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوش آمدی و معذرتی گر نکردمت</p></div>
<div class="m2"><p>معذور دار از آنک ز خویشم خبر نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر من ز روزگار بسی فتنه می گذشت</p></div>
<div class="m2"><p>چشمت بلا شد، ارنه به جانم خطر نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیوسته روز غمزدگان تیره بود، لیک</p></div>
<div class="m2"><p>از روزگار تیره من تیره تر نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو ز بهر عشق گذشته چه غم خوری؟</p></div>
<div class="m2"><p>چون رفت، گومباش، اگر بود و گر نبود</p></div></div>