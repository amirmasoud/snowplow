---
title: >-
    شمارهٔ ۱۹۹۳
---
# شمارهٔ ۱۹۹۳

<div class="b" id="bn1"><div class="m1"><p>جهانی به خواب خوشست و من از غم به بیداری</p></div>
<div class="m2"><p>خورد هر کس آب خوش دل من به خونخواری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب از غم بود صد سالم همه شب ز غم ناله</p></div>
<div class="m2"><p>نباشد چنین حالم گرم دل کند یاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گو کنم چو کم کاری هوای چون تو یاری</p></div>
<div class="m2"><p>جفا کن کنون باری که میرم به دشواری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زدی غمزه و هر دم نمایی رخ و لب هم</p></div>
<div class="m2"><p>چه بخشی کنون مرهم که زخمی زدی کاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بر کنگرش جو جو ترا جلوه باید نو</p></div>
<div class="m2"><p>رگ جانم ببرد خسرو کمندت به دست آری</p></div></div>