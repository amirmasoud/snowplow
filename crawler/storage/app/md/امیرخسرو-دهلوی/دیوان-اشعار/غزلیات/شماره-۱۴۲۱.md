---
title: >-
    شمارهٔ ۱۴۲۱
---
# شمارهٔ ۱۴۲۱

<div class="b" id="bn1"><div class="m1"><p>رو زردی از من است ز چشم سیه گرم</p></div>
<div class="m2"><p>ورنه کی آیی آن که من اندر تو بنگرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من دانم ولی که شده ست آب جوی او</p></div>
<div class="m2"><p>کز دست چشم خویش چه خونابه می خورم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جستن شکوفه روی تو شد روان</p></div>
<div class="m2"><p>بادی که از جوانی خود بود در سرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اکنون که مر مرا غم تو سرخ روی کرد</p></div>
<div class="m2"><p>پیش که گویم این غم و این زر کجا برم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگشا نقاب کز رخ چون آفتاب تو</p></div>
<div class="m2"><p>روز فرود رفته خود را برآورم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل چون چراغ سوخته شد ز آتش فراق</p></div>
<div class="m2"><p>از شام غم هنوز به تاریکی اندرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سودای خاک پای تو تا در سر من است</p></div>
<div class="m2"><p>سر در کلاه سبز فلک در نیاورم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من خسروم، ولیک نگر کز فراق تو</p></div>
<div class="m2"><p>گویی که از نگارش شاپور دفترم</p></div></div>