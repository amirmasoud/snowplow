---
title: >-
    شمارهٔ ۱۳۲۸
---
# شمارهٔ ۱۳۲۸

<div class="b" id="bn1"><div class="m1"><p>ای سفر کرده ز چشم و در دل و جانی مقیم</p></div>
<div class="m2"><p>روزها شد تا نیاید از سر کویت نسیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش از آن روزی که جان را با بدن شد اتحاد</p></div>
<div class="m2"><p>عشق تو با جان من بودند یاران قدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس مقیم کعبه مقصود نتواند شدن</p></div>
<div class="m2"><p>تا نگردد خاک پای محرمان آن حریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باده نوشیدن به خلوت لذتی دارد مدام</p></div>
<div class="m2"><p>خاصه آن ساعت که باشد نازک اندامی ندیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشک گردد از سموم قهر تو آب حیات</p></div>
<div class="m2"><p>زنده گردد از نسیم لطف تو عظم رمیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدعی، فقرم مبین کز دولت عشقش مرا</p></div>
<div class="m2"><p>هر نفس در یتیمی می دهد طبع کریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم به مکتوبی ز خسرو یاد می کن گاه گاه</p></div>
<div class="m2"><p>چند باشی محترز از طعنه مشتی لئیم</p></div></div>