---
title: >-
    شمارهٔ ۲۸۶
---
# شمارهٔ ۲۸۶

<div class="b" id="bn1"><div class="m1"><p>بیدار شو، دلا، که جهان جای خواب نیست</p></div>
<div class="m2"><p>ایمن درین خرابه نشستن صواب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خفتگان خواب چه پرسی که حال چیست؟</p></div>
<div class="m2"><p>زان خواب خوش که هیچ کسی را جواب نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون هیچ دوست نیست وفادار زیر خاک</p></div>
<div class="m2"><p>معمور خسته ای که چو گور خراب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون مست را خبر نبود از جفای دهر</p></div>
<div class="m2"><p>بر هوشیار به ز شراب و کباب نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طیب حیات خواستن از آسمان خطاست</p></div>
<div class="m2"><p>کز شیشه ای ذلیل امید صواب نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقی ز جام عشق به خسرو رسان نمی</p></div>
<div class="m2"><p>زیرا که مست کارتر از وی شراب نیست</p></div></div>