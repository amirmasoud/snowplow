---
title: >-
    شمارهٔ ۲۹۶
---
# شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>تا دیده در جمال تو دیدن گرفته است</p></div>
<div class="m2"><p>خونابه ها ز چشم چکیدن گرفته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهر و مه است در نظرم کم ز ذره ای</p></div>
<div class="m2"><p>تا خاک آب دیده کشیدن گرفته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون کرده ایم نسبت گل با جمال او</p></div>
<div class="m2"><p>دل هم ز شوق جامه دریدن گرفته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی پند واعظم بنشیند به گوش دل</p></div>
<div class="m2"><p>گوشم که خواری تو شنیدن گرفته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جان هزار گونه جراحت پدید شد</p></div>
<div class="m2"><p>لب را به قهر ما چو گزیدن گرفته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل را هوای شربت و آب زلال نیست</p></div>
<div class="m2"><p>در عاشقی چو زهر چشیدن گرفته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا گفته ای که جانب خسرو همی روم</p></div>
<div class="m2"><p>اشکش ز دیده پیش دویدن گرفته است</p></div></div>