---
title: >-
    شمارهٔ ۱۳۸۷
---
# شمارهٔ ۱۳۸۷

<div class="b" id="bn1"><div class="m1"><p>روی تو ماه سما می گوییم</p></div>
<div class="m2"><p>موی تو مشک ختا می گوییم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش آن قامت چون نیشکرت</p></div>
<div class="m2"><p>سرو را ز هر گیا می گوییم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا ترا یک نظر از ما نرسد</p></div>
<div class="m2"><p>گر چه انگشت نما می گوییم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده را خاک درت می دانیم</p></div>
<div class="m2"><p>تا ندانی که ریا می گوییم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکر آن است که اندر لب تست</p></div>
<div class="m2"><p>سخن این است که ما می گوییم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصه خود ز لبت می جوییم</p></div>
<div class="m2"><p>غصه خویش ترا می گوییم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کعبتین است دو چشمت کو را</p></div>
<div class="m2"><p>مهره بازی به دغا می گوییم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طاق محراب دو ابروت ز دور</p></div>
<div class="m2"><p>ما ببینیم و دعا می گوییم</p></div></div>