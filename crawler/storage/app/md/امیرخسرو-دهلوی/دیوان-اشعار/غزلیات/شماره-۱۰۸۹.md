---
title: >-
    شمارهٔ ۱۰۸۹
---
# شمارهٔ ۱۰۸۹

<div class="b" id="bn1"><div class="m1"><p>یا رب، آن رویست یا گلبرگ خندان در نظر</p></div>
<div class="m2"><p>یا رب، آن بالاست یا سرو خرامان در نظر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خوش آن ساعت که بینم آن رخ و گیرم لبش</p></div>
<div class="m2"><p>باده خوش بر کف و گلنار خندان در نظر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا تو، ای سرو خرامان، در چمن بگذشته ای</p></div>
<div class="m2"><p>می نیاید پیش بلبل را گلستان در نظر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در تو می بینم ز دود دل ز حسرت بیقرار</p></div>
<div class="m2"><p>تشنه را کی سود دارد آب حیوان در نظر؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک زمان از دل فرونایی همه شب تا به روز</p></div>
<div class="m2"><p>گر چه باشد تا به روزم ماه تابان در نظر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در نظرها صورت جان، گر نیاید، گو میا</p></div>
<div class="m2"><p>در تو بینم کایدم چیزی به از جان در نظر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلق گل بینند و من روی تو، زیرا خوش تر است</p></div>
<div class="m2"><p>یک نظر در دوست از صد ساله بستان در نظر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دندان تو زان بینم که دل می خواهدم</p></div>
<div class="m2"><p>ورنه دریا نایدم از بذل سلطان در نظر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شه علاء الدین والدنیا محمد کآمده ست</p></div>
<div class="m2"><p>خلق را عین الیقین زو ظل یزدان در نظر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از پی آن را که گیرد سبق فیروزی سپهر</p></div>
<div class="m2"><p>حرف تیغش را همی دارد فراوان در نظر</p></div></div>