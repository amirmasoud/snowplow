---
title: >-
    شمارهٔ ۵۶۹
---
# شمارهٔ ۵۶۹

<div class="b" id="bn1"><div class="m1"><p>به سالی کی چنین ماهی برآید؟</p></div>
<div class="m2"><p>وگر آید، ز چه گاهی برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز رخسارش ز حسن جعد مشکین</p></div>
<div class="m2"><p>کجا از تیره شب ماهی برآید؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر آیینه حسن است روشن</p></div>
<div class="m2"><p>بگیرد زنگ، اگر آهی برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسا خرمن که در یکدم بسوزد</p></div>
<div class="m2"><p>از آن آتش که ناگاهی برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه شب تا سحر بیدار باشم</p></div>
<div class="m2"><p>بود کان مه سحرگاهی برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گدایی گر به کویی دل فروشد</p></div>
<div class="m2"><p>که از جان بگذرد، شاهی برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجب نبود در آن میخانه خسرو</p></div>
<div class="m2"><p>گر از پیکار گمراهی برآید</p></div></div>