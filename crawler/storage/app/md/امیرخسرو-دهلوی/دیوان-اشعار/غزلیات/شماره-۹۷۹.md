---
title: >-
    شمارهٔ ۹۷۹
---
# شمارهٔ ۹۷۹

<div class="b" id="bn1"><div class="m1"><p>مدتی شد که یار می ناید</p></div>
<div class="m2"><p>وان بت گلعذار می ناید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان خود را شکار او کردم</p></div>
<div class="m2"><p>رغبتش بر شکار می ناید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شمارند بس که یارانش</p></div>
<div class="m2"><p>بنده خود در شمار می ناید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا برآورد گرد از دلها</p></div>
<div class="m2"><p>زو دلی بی غبار می ناید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزگاری که پیشم آمد ازو</p></div>
<div class="m2"><p>پیش او روزگار می ناید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آرزویم کنار او چه شود؟</p></div>
<div class="m2"><p>کارزو در کنار می ناید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل من کز قرار خویش برفت</p></div>
<div class="m2"><p>دیر شب برقرار می ناید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مکن، ای دوست، ذکر صبر به عشق</p></div>
<div class="m2"><p>که مرا استوار می ناید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسروا، گرد عشق می گردی</p></div>
<div class="m2"><p>مگرت جان به کار می ناید</p></div></div>