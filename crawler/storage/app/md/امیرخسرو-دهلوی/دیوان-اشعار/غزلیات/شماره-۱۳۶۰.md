---
title: >-
    شمارهٔ ۱۳۶۰
---
# شمارهٔ ۱۳۶۰

<div class="b" id="bn1"><div class="m1"><p>بی تو امید ندارم که زمانی بزیم</p></div>
<div class="m2"><p>سهل آنست که تا چند به جانی بزیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخصت زیستنم نیست ز چشم تو، ولی</p></div>
<div class="m2"><p>گر دهد غمزه شوخ تو امانی، بزیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دهان تو یقین نیست، رها کن بازی</p></div>
<div class="m2"><p>چند گاهی که توانم به گمانی بزیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست ده بر دهن خویش به بوسی تو مرا</p></div>
<div class="m2"><p>مگر از لطف تو دستی به دهانی بزیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسروم، لیک چو فرهاد شدم کشته عشق</p></div>
<div class="m2"><p>گر بگویی که چگونه ست فلانی بزیم</p></div></div>