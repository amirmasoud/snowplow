---
title: >-
    شمارهٔ ۴۹۷
---
# شمارهٔ ۴۹۷

<div class="b" id="bn1"><div class="m1"><p>یک خنده بزن، زان لب لعل شکرآلود</p></div>
<div class="m2"><p>بر عاشق مسکین که رخ از خون تر آلود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک شب ز برای دل من محرم من باش</p></div>
<div class="m2"><p>بشنو ز دلم چند حدیث جگر آلود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مانا که بپرسی ز دل من که چه کردی؟</p></div>
<div class="m2"><p>در کوی تو کز خون همه دیوار و در آلود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانها که گرفتار لبت گشت چه دانی؟</p></div>
<div class="m2"><p>پرواز مجو از مگسان شکر آلود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق که بمیرد ز رخ زرد چه خیزد؟</p></div>
<div class="m2"><p>عشق است دروغش که مسی را به زر آلود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نزل غم تو باد حرامم به فراغت</p></div>
<div class="m2"><p>گر چشم دلم هیچ گه از خواب و خور آلود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آسوده به خاک درت، اینک سر خسرو</p></div>
<div class="m2"><p>زان صندل راحت که برین درد سر آلود</p></div></div>