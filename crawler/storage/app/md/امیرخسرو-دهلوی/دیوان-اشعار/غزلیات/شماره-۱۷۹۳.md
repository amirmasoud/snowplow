---
title: >-
    شمارهٔ ۱۷۹۳
---
# شمارهٔ ۱۷۹۳

<div class="b" id="bn1"><div class="m1"><p>تو با آن رو بگو مه را، چه باشی؟</p></div>
<div class="m2"><p>تو با آن رخ بگو شه را، چه باشی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببین آیینه و خود را صفت کن</p></div>
<div class="m2"><p>حدیث زهره و مه را چه باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلا، زینسان چه می نالی در آن کوی؟</p></div>
<div class="m2"><p>گدایان شبانگه را چه باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بمیر ای مرغ تشنه در بیابان</p></div>
<div class="m2"><p>امید ابر ناگه را چه باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه سویت، خسروا، دارد جدا گوش</p></div>
<div class="m2"><p>به کویش ناله و وه را چه باشی</p></div></div>