---
title: >-
    شمارهٔ ۱۶۹۷
---
# شمارهٔ ۱۶۹۷

<div class="b" id="bn1"><div class="m1"><p>کارم از دست برفته ست ز نادیدن تو</p></div>
<div class="m2"><p>زین پس، ای دیده، کجا ما و کجا دیدن تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کجا وقت که در کوچه ما به جولان رفتن</p></div>
<div class="m2"><p>دل بدزدیدن و دزدیده به ما دیدن تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن به خونریز خود از چشم رضا دیدن من</p></div>
<div class="m2"><p>و آن بر احوال من از چشم جفا دیدن تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال زار گذر من شب تیره دانی</p></div>
<div class="m2"><p>که چه فرق است ز نادیدن تا دیدن تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواست خسرو که نبیند غمی، اما چه کند</p></div>
<div class="m2"><p>دیدنی بود، نگارا، غم نادیدن تو</p></div></div>