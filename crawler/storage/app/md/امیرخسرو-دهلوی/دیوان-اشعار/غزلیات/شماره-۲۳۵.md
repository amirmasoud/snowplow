---
title: >-
    شمارهٔ ۲۳۵
---
# شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>گر بگویم که درون دل من پنهان چیست</p></div>
<div class="m2"><p>خود بگویی و بدانی که غم هجران چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خستگان تو که دور از تو، نه نزدیک تواند</p></div>
<div class="m2"><p>تو چه دانی که همه شب به دل ایشان چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشتنم خواهی، اینک سر و اینک خنجر</p></div>
<div class="m2"><p>می کشی یا بزیم چند گهی، فرمان چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد تو آتش و آب از دل و چشمم بگشاد</p></div>
<div class="m2"><p>به جز از سوختن و غرقه شدن درمان چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق داند که زمین را ز چه شوید اشکم</p></div>
<div class="m2"><p>نوح داند که جهان را سبب طوفان چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارم امید که چون بخت در آرم به برت</p></div>
<div class="m2"><p>تا ز تو بخت من بی سر و بی سامان چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آشکارا بکشم زانکه بمردم به خیال</p></div>
<div class="m2"><p>کان شکر خنده به زیر لب تو پنهان چیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور نخواهی به شکر کشت من مسکین را</p></div>
<div class="m2"><p>لب شیرین شکنت را به شکر دندان چیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زلف را پرس، اگرت نیست یقین کز زلفت</p></div>
<div class="m2"><p>حال خسرو به شب تیره بی پایان چیست</p></div></div>