---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>مرا دردیست اندر دل که درمان نیستش یارا</p></div>
<div class="m2"><p>من و دردت، چو تو درمان نمی خواهی دل ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم امروز، و صحرایی و آب ناخوش از دیده</p></div>
<div class="m2"><p>چو مجنون آب خوش هرگز ندادی وحش صحرا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبت خوش باد و خواب مستیت سلطان و من هم خوش</p></div>
<div class="m2"><p>شبی گر چه نیاری یاد بیداران شبها را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عشق ار عاشقی میرد، گنه بر عشق ننهد کس</p></div>
<div class="m2"><p>که بهر غرقه کردن عیب نتوان کرد دریا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بمیرند و برون ندهند مشتاقان دم حسرت</p></div>
<div class="m2"><p>کله ناگه مبادا کج شود آن سرو بالا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نومیدی به سر شد روزگار من که یک روزی</p></div>
<div class="m2"><p>عنان گیری نکرد امید، هم عمر روان ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مزن لاف صبوری خسروا در عشق کاین صرصر</p></div>
<div class="m2"><p>به رقص آرد چو نفخ صور، کوه پای بر جا را</p></div></div>