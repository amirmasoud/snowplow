---
title: >-
    شمارهٔ ۲۵۸
---
# شمارهٔ ۲۵۸

<div class="b" id="bn1"><div class="m1"><p>یار ما را عزم رایی دیگر است</p></div>
<div class="m2"><p>باز در بند جفایی دیگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نظر می آیدم گلها بسی</p></div>
<div class="m2"><p>چون کنم آن روی جایی دیگر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر یکی چشمم به رویش روشن است</p></div>
<div class="m2"><p>خاک پایش توتیایی دیگر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقیا، می ده که بر یاد لبت</p></div>
<div class="m2"><p>بامی امروزم صفایی دیگر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با رقیبان ساختن بیچارگی است</p></div>
<div class="m2"><p>محنت هجران بلایی دیگر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوستدارانت بسی هستند، لیک</p></div>
<div class="m2"><p>خسرو مسکین، گدایی دیگر است</p></div></div>