---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>تا نظر سوی دو چشم تست یاران ترا</p></div>
<div class="m2"><p>کی بود پیکاری آن مردم شکاران ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا شدند اندر کشش دو چشم تو خنجز گذار</p></div>
<div class="m2"><p>شغلها فرمود اجل خنجر گذاران ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوجوان گشتی و شکل ناز را نشناختی</p></div>
<div class="m2"><p>جای تسکین نیست زین پس بیقراران ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کرا امروز خواندی باز فردا کشتیش</p></div>
<div class="m2"><p>بارک الله این چه اقبال است یاران ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دلم خوش کردی از امید پیکان ریختن</p></div>
<div class="m2"><p>نام شد باران رحمت تیر باران ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرمسار یک نظر گشتیم و هست از چشم تو</p></div>
<div class="m2"><p>یک نظر دیگر توقع شرمساران ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از لب تو تشنگان محروم و ساغر بهره مند</p></div>
<div class="m2"><p>مرهمی باید هم آخر دلفگاران ترا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خون تیره می خورند از چشم تو عشاق تو</p></div>
<div class="m2"><p>نوش باد این می به یادت درد خواران ترا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاه حسنی و بلا و فتنه پیشت یادگار</p></div>
<div class="m2"><p>شرم بادا قتل خسرو کارداران ترا</p></div></div>