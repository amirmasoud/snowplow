---
title: >-
    شمارهٔ ۸۵۲
---
# شمارهٔ ۸۵۲

<div class="b" id="bn1"><div class="m1"><p>خوبان گمان مبر که ز اولاد آدمند</p></div>
<div class="m2"><p>جانند یا فرشته و یا روح اعظمند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان انگبین چه ناله کنی، زانکه دائما</p></div>
<div class="m2"><p>مرغان عرش بر مگس از شهد بر مکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوانید روح وامق و مجنون وویس را</p></div>
<div class="m2"><p>کایشان درون پرده این راز محرمند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای سلسبیل راحت و ای چشمه حیات</p></div>
<div class="m2"><p>بر تشنگان سوخته لطفی که در همند!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاغان نمی زنند به کویت که می خورند</p></div>
<div class="m2"><p>مشتاق را که سوخته آتش غمند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر شب منم ز نقش خیال تو در گریز</p></div>
<div class="m2"><p>چون بوم و شپرک که ز خورشید می رمند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو که زنده نیست، نصیحت چه می کنند؟</p></div>
<div class="m2"><p>باد مسیح بر سگ مرده چه می دمند؟</p></div></div>