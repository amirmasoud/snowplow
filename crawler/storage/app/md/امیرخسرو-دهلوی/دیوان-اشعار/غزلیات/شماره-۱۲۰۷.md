---
title: >-
    شمارهٔ ۱۲۰۷
---
# شمارهٔ ۱۲۰۷

<div class="b" id="bn1"><div class="m1"><p>چون سبزه بر دمید ز گلزار یار خط</p></div>
<div class="m2"><p>دارم غبار خاطر از آن مشکبار خط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانا، محقق است که جز کاتب ازل</p></div>
<div class="m2"><p>بر برگ لاله ات ننوشت از غبار خط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاقوت جوهر دهنت آب زندگیست</p></div>
<div class="m2"><p>کز وی مدام زنده بود خضروار خط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشک خطت که هست روان تر ز آب جوی</p></div>
<div class="m2"><p>بر خوانده ام ندیده شد، ای گلعذار، خط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تو دلم به باغ و بهاری نمی کشد</p></div>
<div class="m2"><p>باغ من است روی تو و نوبهار خط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یارب، چه خوش به خامه تقدیر دست صنع؟</p></div>
<div class="m2"><p>بنوشته است بر ورق روی یار خط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو، چه وجه بود که نادیده روی او</p></div>
<div class="m2"><p>آرد لبش به خون من دلفگار خط</p></div></div>