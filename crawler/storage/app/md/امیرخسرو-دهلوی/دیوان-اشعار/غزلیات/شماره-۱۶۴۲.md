---
title: >-
    شمارهٔ ۱۶۴۲
---
# شمارهٔ ۱۶۴۲

<div class="b" id="bn1"><div class="m1"><p>ز سر کرشمه یک ره نظری به روی من کن</p></div>
<div class="m2"><p>به عنایتی که دانی گذری به سوی من کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم و دلی و دردی ز غمت چو ناتوانان</p></div>
<div class="m2"><p>به زکوة تندرستی نظری به سوی من کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه بوی عود نبود که به رغبتش بسوزی</p></div>
<div class="m2"><p>دل سوخته ست، قدری نظری به بوی من کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرست رسم خوبان که به مو نهند دلها</p></div>
<div class="m2"><p>دل خود بیار و جایش به تن چو موی من کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دو زلف طوق دارت، نه یکی که صد به هر خم</p></div>
<div class="m2"><p>وگرت هزار باشد، همه در گلوی من کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن خاکیم لبالب همه پر ز خونست از تو</p></div>
<div class="m2"><p>لب خویش را تو ساقی، ز سر سبوی من کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگران مشو ز خسرو که بد است حالش آخر</p></div>
<div class="m2"><p>نفسی بیا و بنشین، بد من نکوی من کن</p></div></div>