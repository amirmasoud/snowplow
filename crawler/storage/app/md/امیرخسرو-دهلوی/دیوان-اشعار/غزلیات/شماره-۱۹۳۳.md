---
title: >-
    شمارهٔ ۱۹۳۳
---
# شمارهٔ ۱۹۳۳

<div class="b" id="bn1"><div class="m1"><p>گر چشم من در روی آن خورشید رخسار آمدی</p></div>
<div class="m2"><p>آخر شب امید را صبحی پدیدار آمدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی دوم چون بیخودی در کویت ار بختم بدی</p></div>
<div class="m2"><p>یا پای در سنگ آمدی یا سر به دیوار آمدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دوست بودی یار من، کی خواستی آزار من؟</p></div>
<div class="m2"><p>آسان گرفتی کار من، هر چند دشوار آمدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پشت من از غم گشت خم، کز بخت بنمودی ستم</p></div>
<div class="m2"><p>هرگز چنین خاری ز غم بر جان غمخوار آمدی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دردی که دارم در نهان کز یار جستی کس نشان</p></div>
<div class="m2"><p>هر موی من گشتی زبان، یک یک به گفتار آمدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کی ز بیداری مرا باشد دو دیده در هوا</p></div>
<div class="m2"><p>ای کاش! تیری از سما بر چشم بیدار آمدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو چنان گشت از سخن، کاندر میان انجمن</p></div>
<div class="m2"><p>از دوست در گفتی سخن، دشمن به گفتار آمدی</p></div></div>