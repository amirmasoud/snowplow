---
title: >-
    شمارهٔ ۳۲۸
---
# شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>کسی که عشق نبازد نه آدمی سنگ است</p></div>
<div class="m2"><p>بلای عشق کشد هر که آدمی رنگ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه نقش بندی از اندیشه ای که بی عشق است</p></div>
<div class="m2"><p>چه روی بینی از آیینه ای که در زنگ است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار پاره کنم جان مگر که در گنجد</p></div>
<div class="m2"><p>که چشم خوبان همچون دهان شان تنگ است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رها کنید که تن در دهم به بدنامی</p></div>
<div class="m2"><p>که نام نیک در آیین عاشقی ننگ است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سماع در دل من کار کرد و سینه بسوخت</p></div>
<div class="m2"><p>هنوز مطرب ما را ترانه در چنگ است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو، ای صنم، که مرا در دلی چه سود ازان</p></div>
<div class="m2"><p>که در میان من و دل هزار فرسنگ است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جنگ تیغ مکش، سر به آشتی برگیر</p></div>
<div class="m2"><p>که حاصل است به صلحت هر آنچه در جنگ است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خشم می روی و در تو کی رسد خسرو</p></div>
<div class="m2"><p>که ره دراز و قدم سست و بارگی لنگ است</p></div></div>