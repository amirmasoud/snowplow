---
title: >-
    شمارهٔ ۹۸۴
---
# شمارهٔ ۹۸۴

<div class="b" id="bn1"><div class="m1"><p>عاشق از سینه جان برون گیرد</p></div>
<div class="m2"><p>تا غمت را به جان درون گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی او گر شود گرفته ببین</p></div>
<div class="m2"><p>گر نبینی که ماه چون گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگران از پری فسون گیرند</p></div>
<div class="m2"><p>از دو چشمت پری فسون گیرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محنت و غم حریف و مونس وی</p></div>
<div class="m2"><p>چون تواند که دل سکون گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی تو این چشم خون گرفته بسی</p></div>
<div class="m2"><p>آخر این آب چند خون گیرد</p></div></div>