---
title: >-
    شمارهٔ ۵۷۱
---
# شمارهٔ ۵۷۱

<div class="b" id="bn1"><div class="m1"><p>سر زلف تو یاری را نشاید</p></div>
<div class="m2"><p>که دشمن دوست داری را نشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه زلفت آرد تاب بازی</p></div>
<div class="m2"><p>ولی باد بهاری را نشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلا، خود را به چشم او مده، زانک</p></div>
<div class="m2"><p>مقام استواری را نشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حریفش بوده ام شب مگری، ای چشم</p></div>
<div class="m2"><p>که این شربت خماری را نشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جان کندن رها کن نیم کشته</p></div>
<div class="m2"><p>که این تن زخم کاری را نشاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرابم کرد چشمت، راست گفتند</p></div>
<div class="m2"><p>که ترک مست یاری را نشاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مران از در که خسرو بنده تست</p></div>
<div class="m2"><p>عزیزش کن که خواری را نشاید</p></div></div>