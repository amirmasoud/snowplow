---
title: >-
    شمارهٔ ۴۳۳
---
# شمارهٔ ۴۳۳

<div class="b" id="bn1"><div class="m1"><p>ز عارض، طره بالا کن که کار خلق در هم شد</p></div>
<div class="m2"><p>علم برکش که بر خوبانت سلطانی مسلم شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فگندی برقع از روی و زیعقوبان بشد دیده</p></div>
<div class="m2"><p>گذشتی بر سر بازار و حسن یوسفان کم شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم می خواستی پاره، عفاک الله چنان دیدی</p></div>
<div class="m2"><p>مرا می خواستی رسوا، بحمدالله که آن هم شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که داند خاک من دور از سر کویت کجا افتد؟</p></div>
<div class="m2"><p>خوش آن سرها که راه تو خاک نعل ادهم شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا دادم دل و تن خال را و جان دو چشمت را</p></div>
<div class="m2"><p>من و عشقت کنون، کز سوی خویشم سینه بیغم شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گریبان گیری، ای زاهد، چه فرمایی رقیبان را؟</p></div>
<div class="m2"><p>کز و در عهد حسنش دامن صحبت فراهم شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برون افتاد چون نامحرمان از پرده دل جان</p></div>
<div class="m2"><p>از آنگه کاندرین پرده خیال دوست محرم شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عنانش گیر و مگذار، ای رقیب، از خانه بیرونش</p></div>
<div class="m2"><p>که از دمهای سرد عاشقان در تاب و در هم شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زبان گر تیشه فرهاد گردد پندگویان را</p></div>
<div class="m2"><p>چه غم، چون در دل خسرو بنای دوست محکم شد</p></div></div>