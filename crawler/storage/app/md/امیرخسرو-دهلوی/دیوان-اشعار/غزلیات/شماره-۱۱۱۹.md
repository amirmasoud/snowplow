---
title: >-
    شمارهٔ ۱۱۱۹
---
# شمارهٔ ۱۱۱۹

<div class="b" id="bn1"><div class="m1"><p>مبتلا شد چون دل مسکین به زلف یار باز</p></div>
<div class="m2"><p>جان سلامت کی توان بردن ازان طرار باز؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل به ابروی بتان دارد چو اقرار درست</p></div>
<div class="m2"><p>می کند از مومنی تصدیق آن اقرار باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو بستان در چمن چون دید رفتار ترا</p></div>
<div class="m2"><p>از خجالت خشک بر جا ماند از رفتار باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ غمخواری ندارم در غم عشق تو من</p></div>
<div class="m2"><p>هم مگر لطف تو گردد بنده را غمخوار باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چاره بیچارگان چون در لب شیرین تست</p></div>
<div class="m2"><p>دامنت خواهم گرفت، ای صنم، ناچار باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند گه پر کار چرخ ار کرد از هم مان جدا</p></div>
<div class="m2"><p>عاقبت با هم رسانید آن سر پرگار باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر جمالت دل نه اکنون عاشق است، ای جان من</p></div>
<div class="m2"><p>مهر تو در سینه دارم مدمت بسیار باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر هوای وصل آن مه داری، ای خسرو، به جان</p></div>
<div class="m2"><p>چشم غیرت را بدوز از دیدن اغیار باز</p></div></div>