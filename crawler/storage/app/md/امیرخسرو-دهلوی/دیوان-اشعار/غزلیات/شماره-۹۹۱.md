---
title: >-
    شمارهٔ ۹۹۱
---
# شمارهٔ ۹۹۱

<div class="b" id="bn1"><div class="m1"><p>خم زلفت که مشک چین آمد</p></div>
<div class="m2"><p>با گل و لاله همنشین آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب لعل تو کان پر از گهر است</p></div>
<div class="m2"><p>خاتم حسن را نگین آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوه را سایه دار نتوان کرد</p></div>
<div class="m2"><p>جز دو زلفت که بر سرین آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه گل ناز می کند بر شاخ</p></div>
<div class="m2"><p>نه چو روی تو نازنین آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که پیکان تیز غمزه تو</p></div>
<div class="m2"><p>تشنه خون حور عین آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صورت این کن که چین ابرویت</p></div>
<div class="m2"><p>صورت حسن را چو چین آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگزیدم لبت که خون آید</p></div>
<div class="m2"><p>خون برون نامد، انگبین آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از شب زلف تو برست دلم</p></div>
<div class="m2"><p>گشت روشن که خسرو این آمد</p></div></div>