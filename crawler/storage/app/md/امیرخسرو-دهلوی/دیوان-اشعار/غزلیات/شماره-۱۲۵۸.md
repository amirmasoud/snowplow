---
title: >-
    شمارهٔ ۱۲۵۸
---
# شمارهٔ ۱۲۵۸

<div class="b" id="bn1"><div class="m1"><p>چو دادی مژده این نعمتم کت روی بنمایم</p></div>
<div class="m2"><p>رها کن کز کف پای تو زنگ دیده بزدایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پات ار دیده سایم، زنده گردم، لیک کشت آنم</p></div>
<div class="m2"><p>کز این خون غم آلوده چگونه پات آلایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خون دیده خود شرمسارم پیش تو، کز وی</p></div>
<div class="m2"><p>همه یاقوت قلب این نثار آن چنانم پایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهانی نرخ یک نظاره کردی در جمال خود</p></div>
<div class="m2"><p>دو عالم گر بود دستم، برین بالا بیفزایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بمیرم زین هوس کاید شبی خواب و ترا بینم</p></div>
<div class="m2"><p>چو از خواب اندر آیم، هم به رویت چشم بگشایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شنیدن چون توانم ذکرت از گفتار هر غیری</p></div>
<div class="m2"><p>چو گویم نام تو، خواهم زبان خود فرو خایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مزن طعنه که از کویم عزیز چشمها گشتی</p></div>
<div class="m2"><p>که آخر خاک در می ریزم، این، نه سرمه می سایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بباید سوختن صد بار و بازم آفرید از سر</p></div>
<div class="m2"><p>کز آنسان پاک گردم کاتشت را سوختن شایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دعا این می کند خسرو که گردم خاک در کویت</p></div>
<div class="m2"><p>مگر بختم کند یاری که روزی زیر پات آیم</p></div></div>