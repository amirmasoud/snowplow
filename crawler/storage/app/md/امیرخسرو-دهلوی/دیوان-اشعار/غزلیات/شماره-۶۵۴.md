---
title: >-
    شمارهٔ ۶۵۴
---
# شمارهٔ ۶۵۴

<div class="b" id="bn1"><div class="m1"><p>یا رب، آن بالا مگر از آب حیوان ریختند</p></div>
<div class="m2"><p>یا بسی جان کسان بگداختند، آن ریختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیره جانهای شیرین برکشیدند از نخست</p></div>
<div class="m2"><p>وین تن نازک ازان شیرینی جان ریختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا خوی ریخت از رویت، ملاحت مایه بست</p></div>
<div class="m2"><p>چاشنی گیران خوبی در نمکدان ریختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین هوس کز ران یکرانت فرو شانند گرد</p></div>
<div class="m2"><p>آبروی خویش بسیاری که خوبان ریختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیش تلخم با خیال لعل جان افزات هست</p></div>
<div class="m2"><p>شربت زهری که در وی آب حیوان ریختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شعله می خیزد ز گور کشتگانت گاه نور</p></div>
<div class="m2"><p>بس که زیر خاک با دلهای سوزان ریختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو چشم نامسلمان تو بی رحمت نه اند</p></div>
<div class="m2"><p>کافران چین که خونهای مسلمان ریختند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از گناه نیکوان، یارب، مرا سوزی نخست</p></div>
<div class="m2"><p>گر چه آن مردم کشان خونها فراوان ریختند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاقبت بر روی آب آورد راز بیدلان</p></div>
<div class="m2"><p>گر چه گریه در شب تاریک پنهان ریختند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خسروا، مگری که جز خاشاک بدنامی نرست</p></div>
<div class="m2"><p>دیده های عاشقان هر جا که باران ریختند</p></div></div>