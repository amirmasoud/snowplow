---
title: >-
    شمارهٔ ۱۶۰۱
---
# شمارهٔ ۱۶۰۱

<div class="b" id="bn1"><div class="m1"><p>سبزه همان و گل و صحرا همان</p></div>
<div class="m2"><p>باغ همان، سایه همان، جا همان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد چمن شاهد زیبا بسی</p></div>
<div class="m2"><p>در دل من شاهد زیبا همان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پهلوی من صد بت جان بخش، وای</p></div>
<div class="m2"><p>آن که مرا می کشد، الا همان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چمنی هر کس و من بر درش</p></div>
<div class="m2"><p>باغ من آن است و تماشا همان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نام نماند از دل و جان و هنوز</p></div>
<div class="m2"><p>عشق همان است و تمنا همان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم مرا سیل ز دریا گذشت</p></div>
<div class="m2"><p>سوختگی دل شیدا همان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قهر تو لطفی ست که عشاق را</p></div>
<div class="m2"><p>خار همان باشد و خرما همان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرق میان دو لبت کی توان</p></div>
<div class="m2"><p>خضر همان است و مسجد همان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تو بلا وز دل خسرو رضا</p></div>
<div class="m2"><p>کز تو همان شاید و از ما همان</p></div></div>