---
title: >-
    شمارهٔ ۱۰۸۸
---
# شمارهٔ ۱۰۸۸

<div class="b" id="bn1"><div class="m1"><p>گر هنر داری مرنج، ار کم نشینی بر ستور</p></div>
<div class="m2"><p>زیر عیسی خر نگر، زیر خزان یکران تور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز حروفی نام رخش و داردت هر جا، چه سود؟</p></div>
<div class="m2"><p>در عرب وی را کمیت است اسم و در تاتار بور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیک و بد در آدمی پنهان نمی ماند، چنانک</p></div>
<div class="m2"><p>نافه در جیب ملوک و باده در جام بلور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس را چون رام جویی، ساکنی بهتر ز جهد</p></div>
<div class="m2"><p>پیل را چون پست خواهی، چاره نیکوتر ز زور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند بهر کنجدی کش خورده نتوانی، ز حرص</p></div>
<div class="m2"><p>پا نهی، کایی تهی تگ در ره پیلان چو مور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>احمقی باشد که گنجی دارد و خرجیش نیست</p></div>
<div class="m2"><p>بر ستور انبار گوهر کی بود سود ستور؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مزد دارد عرض بخشش پیش دکان بخیل</p></div>
<div class="m2"><p>خیر باشد چاه کندن بر لب دریای شور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوار نبود مکر می کو گردد از افلاس خوار</p></div>
<div class="m2"><p>عور نبود منفقی کو گردد از انفاق عور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در عیار سیم و زر تا کی پرستی سنگ را؟</p></div>
<div class="m2"><p>باش تا سیم تو گردد گور و گردد سنگ گور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترک در دنباله کور و ز گورش یاد نه</p></div>
<div class="m2"><p>گور دنبالش روان زانگونه کو دنبال کور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صنع یزدان شد چنان، از دیده عیبیش مبین</p></div>
<div class="m2"><p>حسن در زنگ و حبش چون عقل در ملتان و غور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با تن سیمین، چو گنج خویش یابی زیر خاک</p></div>
<div class="m2"><p>زال زر رویین تن و پولادوند و سیمجور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پر نگیرد بنده خواهش، ذره ذره کن چو ریگ</p></div>
<div class="m2"><p>روغن اندر ریگ ریزی، بیشتر گردد صبور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خام تر گردد ز پند معنوی دانای خام</p></div>
<div class="m2"><p>کورتر گردد ز باد عیسوی دجال کور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر به پند از فسق باز آیی چو خسرو، ای حکیم</p></div>
<div class="m2"><p>در جنب سر شستنت باید، چه دریا و چه خور!</p></div></div>