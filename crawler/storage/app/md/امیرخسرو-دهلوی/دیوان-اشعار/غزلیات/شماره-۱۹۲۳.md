---
title: >-
    شمارهٔ ۱۹۲۳
---
# شمارهٔ ۱۹۲۳

<div class="b" id="bn1"><div class="m1"><p>من باد نخواهم که وزد بر چو تو باغی</p></div>
<div class="m2"><p>تا از تو نسیمی نرساند به دماغی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش دولت مرغی که خورد بر ز تو، ماییم</p></div>
<div class="m2"><p>کز دور خرابیم به بویی چو تو باغی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خواه به بازار شوم، خواه به بستان</p></div>
<div class="m2"><p>ما را ز رخت سوی دگر نیست فراغی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر جلوه طاووس ز روی تو نبینیم</p></div>
<div class="m2"><p>در کوی تو میریم به مهمانی زاغی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو داغ جگر را چه شناسی که نبودت</p></div>
<div class="m2"><p>جز از می گلرنگ به دامان تو داغی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پروانه که جان را به سر شمع فدا کرد</p></div>
<div class="m2"><p>در مشهد خویش از تن خود سوخت چراغی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن به که من سوخته پیش تو نیایم</p></div>
<div class="m2"><p>زیبا نبود پیش گلی بانگ کلاغی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لاغ است ترا کشتن، اگر لطف دگر نیست</p></div>
<div class="m2"><p>باری ز من دلشده یاد آر به لاغی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نآمد ز دل خسته خبر، گر چه که خسرو</p></div>
<div class="m2"><p>از گریه دوانید چپ و راست الاغی</p></div></div>