---
title: >-
    شمارهٔ ۱۷۰۳
---
# شمارهٔ ۱۷۰۳

<div class="b" id="bn1"><div class="m1"><p>ای جان، چو سخن گویم مستانه و رندانه</p></div>
<div class="m2"><p>سرمستم و لایعقل زان نرگس مستانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرسد ز سرشک خون جانم ز غمت، آری</p></div>
<div class="m2"><p>پر گشته مرا آخر در عشق تو پیمانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دوست، سر زلفت در سینه من بگشا</p></div>
<div class="m2"><p>زنجیر نه این در را، سرهاست درین خانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با عشق دو چشمش چون رفتی ز پی کویش</p></div>
<div class="m2"><p>خسرو، تو رهی رفتی رندانه و یارانه</p></div></div>