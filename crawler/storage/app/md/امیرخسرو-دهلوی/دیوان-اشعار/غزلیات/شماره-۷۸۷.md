---
title: >-
    شمارهٔ ۷۸۷
---
# شمارهٔ ۷۸۷

<div class="b" id="bn1"><div class="m1"><p>ترک من چون تیر مژگان برکشد</p></div>
<div class="m2"><p>ماه گردون را سپر در سر کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دلم تیرش ترازویی شود</p></div>
<div class="m2"><p>وز درون سینه جان می برکشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون رسن بازی کند زلفین او</p></div>
<div class="m2"><p>گردن خورشید در چنبر کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل کنم بر آتش رویش کباب</p></div>
<div class="m2"><p>چون لب میگون او ساغر کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمت از مژگان چون نوک قلم</p></div>
<div class="m2"><p>بر فسون جادوان خط در کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راست گویی، مردم چشم من است</p></div>
<div class="m2"><p>چون قبای آبگون در بر کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خط طوطی رنگ او، یارب، کجاست؟</p></div>
<div class="m2"><p>تا به منقار از لبش شکر کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مست کرده نرگس غلتان او</p></div>
<div class="m2"><p>وز مژه بر جان من خنجر کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از لبت چون باده نوشان خیال</p></div>
<div class="m2"><p>چشم خسرو خانه خمار شد</p></div></div>