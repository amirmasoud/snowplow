---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>الا دمعی سارعت والهوا</p></div>
<div class="m2"><p>وقد ذاب قلبی هو والنوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسیرست ازان میر خوبان دلم</p></div>
<div class="m2"><p>به دردی که هرگز ندیدم دوا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اذا اشرق الشمش من صدغه</p></div>
<div class="m2"><p>فنعم الهوا فی جناتی هوا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم خون شد و ناید ار باروت</p></div>
<div class="m2"><p>بر این ماجرا چشمم اینک گوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ولی الموالی علی حبه</p></div>
<div class="m2"><p>و لکنه فی بوادی لوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بتا نا مسلمانیی می کنی</p></div>
<div class="m2"><p>که در کافرستان نباشد روا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و قد و قدالبین نیرانه</p></div>
<div class="m2"><p>ترقی دخانی بجوالهوا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بماندم من اندر چنین حالتی</p></div>
<div class="m2"><p>نگفتی که حالت چه شد، خسروا</p></div></div>