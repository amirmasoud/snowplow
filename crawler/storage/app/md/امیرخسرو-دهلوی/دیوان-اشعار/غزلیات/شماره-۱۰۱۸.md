---
title: >-
    شمارهٔ ۱۰۱۸
---
# شمارهٔ ۱۰۱۸

<div class="b" id="bn1"><div class="m1"><p>هوایی خرم است و ابر لولوبار می بارد</p></div>
<div class="m2"><p>زلال زندگی بر شاخ خضر آثار می بارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به روی سبزه های تر که قطره می چکد، گویی</p></div>
<div class="m2"><p>که بر سطح زمرد دانه های نار می بارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل سرخ انار از شاخ سبزش چون چکاند خون</p></div>
<div class="m2"><p>تو پنداری که طوطی گوهر از منقار می بارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرامان سرو من مست و لطافت می چکد از وی</p></div>
<div class="m2"><p>چه ناز است و کرشمه وه کزان رفتار می بارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هوای ابر عاشق را غم آرد، آن همه قطره</p></div>
<div class="m2"><p>ز بهر جان عاشق خنجر خونخوار می بارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر غرق عرق رخساره خوبان ندیده ستی</p></div>
<div class="m2"><p>نگه کن قطره های خوش که بر گلزار می بارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرشته چون مگس پا بسته می گردد به شیرینی</p></div>
<div class="m2"><p>چو در وصف تو خسرو شکر از گفتار می بارد</p></div></div>