---
title: >-
    شمارهٔ ۱۱۸۷
---
# شمارهٔ ۱۱۸۷

<div class="b" id="bn1"><div class="m1"><p>نظر ز دیده بدزدم چو بنگرم رویش</p></div>
<div class="m2"><p>که دیده نیز نخواهم که بنگرد سویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا به دیده درون خواب از کجا آید؟</p></div>
<div class="m2"><p>که شب نماند به عالم ز پرتو رویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولی ز رویش اگر در جهان نماند شبی</p></div>
<div class="m2"><p>هزار شب نتوان ساختن ز یک مویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز فرق تا به قدم ماه نو شد و پهلو</p></div>
<div class="m2"><p>بدان امید که پهلو نهد به پهلویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس که آینه گشته ست روی زانوی من</p></div>
<div class="m2"><p>که آینه ز چه شد همنشین زانویش؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مردمی اگر آیم به کوی او روزی</p></div>
<div class="m2"><p>سگم کند به فسونهای چشم جادویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدین صفت که کنم کام عیش را شیرین</p></div>
<div class="m2"><p>شراب تلخ نماند ز تلخی خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوش آن کسی که کشد جرعه ای ز جام لبش</p></div>
<div class="m2"><p>که مست گشت جهانی چو خسرو از بویش</p></div></div>