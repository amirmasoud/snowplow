---
title: >-
    شمارهٔ ۱۴۰
---
# شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>دلی کش صبر نبود آن من نیست</p></div>
<div class="m2"><p>کسی کو دل دهد جانان من نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کبابم ساخت، این خونابه زان ست</p></div>
<div class="m2"><p>گنه بر دیده گریان من نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه مضمون من شهری فرو خواند</p></div>
<div class="m2"><p>که مهر صبر در فرمان من نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو می سوز ای دل و مگری تو، ای چشم</p></div>
<div class="m2"><p>که شعله در خور طوفان من نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخش دیدم به دل گفتم چه گویی؟</p></div>
<div class="m2"><p>که یعنی این بلا بر جان من نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نصیحت از خرد جستم، خرد گفت</p></div>
<div class="m2"><p>که بر دیوانگان فرمان من نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب دوشینه جان سویش چنان رفت</p></div>
<div class="m2"><p>که زان اوست گویی ز آن من نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو تیرم زد، کشید آلوده خون</p></div>
<div class="m2"><p>به خنده گفت کاین پیکان من نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسوزد خسروا، دلها چه نیکوست</p></div>
<div class="m2"><p>که گوش خلق بر افغان من نیست</p></div></div>