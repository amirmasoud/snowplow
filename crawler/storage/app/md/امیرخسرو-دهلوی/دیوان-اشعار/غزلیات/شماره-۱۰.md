---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>گل من سبزه زاری کرد پیدا</p></div>
<div class="m2"><p>زمانه نوبهاری کرد پیدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در این موسم که از تأثیر نوروز</p></div>
<div class="m2"><p>جهان نو روزگاری کرد پیدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز کوه ابر سنگ ژاله افتاد</p></div>
<div class="m2"><p>زر گل را، عیاری کرد پیدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدم موی و فرو رفتم به رویش</p></div>
<div class="m2"><p>همانم خارخاری کرد پیدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهانی خارخاری داشت آن شوخ</p></div>
<div class="m2"><p>به حمدالله که باری کرد پیدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببین خسرو، اگر جانت به کار است</p></div>
<div class="m2"><p>که جان را باز کاری کرد پیدا</p></div></div>