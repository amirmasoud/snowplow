---
title: >-
    شمارهٔ ۱۶۹۲
---
# شمارهٔ ۱۶۹۲

<div class="b" id="bn1"><div class="m1"><p>ای گلستان ترا بالای سرو</p></div>
<div class="m2"><p>وز تو زیب قامت زیبای سرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکل سرو ار چه به بستانها خوش است</p></div>
<div class="m2"><p>با چنان قدی کرا پروای سرو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کرا با گلعذاری سر خوش است</p></div>
<div class="m2"><p>کی سر باغ است با سودای سرو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راستی گویم مرا با تست کار</p></div>
<div class="m2"><p>راست ناید کار از بالای سرو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می درم بر یاد بالایت چو گل</p></div>
<div class="m2"><p>جامه پیش قامت یکتای سرو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچگه باشد که زیر پای تو</p></div>
<div class="m2"><p>سر نهم چون سبزه زیر پای سرو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسروت بر چشمها جا کرد، ازآنک</p></div>
<div class="m2"><p>بر گذار سرو باشد جای سرو</p></div></div>