---
title: >-
    شمارهٔ ۲۷۰
---
# شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>ای که روی تو حیات جان است</p></div>
<div class="m2"><p>دیده جایت شده جای آنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه را از رخ چون خورشیدت</p></div>
<div class="m2"><p>در شب چاردهم نقصانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخن اندر لب تو دل ببرد</p></div>
<div class="m2"><p>دل چه باشد، سخن اندر جان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی لبت هر لب لعلی که گزم</p></div>
<div class="m2"><p>سنگ ریزه به ته دندانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناتوانم، که غمت با من کرد</p></div>
<div class="m2"><p>هر چه از جور و جفا بتوانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلک در گشت مرا ز آب دو چشم</p></div>
<div class="m2"><p>تار هر رشته که در دندانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به گه گریه سواد چشمم</p></div>
<div class="m2"><p>تیره، گویی که شب بارانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتیم غم مخور و آسان گیر</p></div>
<div class="m2"><p>این به گفتن، صنما، آسانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دور از شعله آه خسرو!</p></div>
<div class="m2"><p>که دلش سوخته هجرانست</p></div></div>