---
title: >-
    شمارهٔ ۱۰۷۵
---
# شمارهٔ ۱۰۷۵

<div class="b" id="bn1"><div class="m1"><p>ای لعل لبت چو بر شکر شیر</p></div>
<div class="m2"><p>شکر ز لب تو چاشنی گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زلف بریدنت دل من</p></div>
<div class="m2"><p>دیوانه شد و برید زنجیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلفش بگرفت و کرد در هم</p></div>
<div class="m2"><p>فریاد هزار باد شبگیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می گیری و می زنی به تیرم</p></div>
<div class="m2"><p>من کشته شدم، ازین زد و گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مادر چو تویی نزاد بر تو</p></div>
<div class="m2"><p>چون دیده فرو نیاورد شیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تقصیر نمی کنی تو هر چند</p></div>
<div class="m2"><p>تقدیر همی کند چه تقصیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پند تو بسته ماند خسرو</p></div>
<div class="m2"><p>بیچاره کجا رود ز زنجیر!</p></div></div>