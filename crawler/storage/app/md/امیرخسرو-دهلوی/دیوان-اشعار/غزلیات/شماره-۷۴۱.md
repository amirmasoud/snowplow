---
title: >-
    شمارهٔ ۷۴۱
---
# شمارهٔ ۷۴۱

<div class="b" id="bn1"><div class="m1"><p>گر مرا هیچ مرادی پس ازین پیش آمد</p></div>
<div class="m2"><p>حاسدم را ز حسد روز پسین پیش آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه در خاطر من غیر ترا داشت گمان</p></div>
<div class="m2"><p>شرم بادش ز خود آن دم که یقین پیش آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خم تست و سر زلف تو، ار جان طلبند</p></div>
<div class="m2"><p>زیر هر سلسله چاه کمین پیش آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طلب روی تو کردم، شب زلف آمد پیش</p></div>
<div class="m2"><p>آفت کفر، بلی، در ره دین پیش آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طعنه زد عشق تو بر دل که مرو از این راه</p></div>
<div class="m2"><p>این مثل را که ازان بگذری این پیش آمد</p></div></div>