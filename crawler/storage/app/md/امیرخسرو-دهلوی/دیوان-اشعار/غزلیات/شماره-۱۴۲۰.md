---
title: >-
    شمارهٔ ۱۴۲۰
---
# شمارهٔ ۱۴۲۰

<div class="b" id="bn1"><div class="m1"><p>گر خود سخن ز زهره و از ماه بشنوم</p></div>
<div class="m2"><p>نبود چنان کز آن بت دلخواه بشنوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیخوابیم بکشت، وه از من که هر شبی</p></div>
<div class="m2"><p>بنشینم و فسانه آن ماه بشنوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیغم زن، ای رقیب، که قربان شوم ترا</p></div>
<div class="m2"><p>آن دم که من روارو آن ماه بشنوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آواز ارغنون ندهد ذوقم آن چنان</p></div>
<div class="m2"><p>کاواز پای اسب تو ناگاه بشنوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل پاره های خون فگند همچو برگ گل</p></div>
<div class="m2"><p>چون بوی تو ز باد سحرگاه بشنوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود را کنم سپند و نخواهم ترا گزند</p></div>
<div class="m2"><p>از عاشقان چو بر در تو آه بشنوم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدح و ثنا خسرو خوبان که گفته ای</p></div>
<div class="m2"><p>خسرو بخوانش تا من گمراه بشنوم</p></div></div>