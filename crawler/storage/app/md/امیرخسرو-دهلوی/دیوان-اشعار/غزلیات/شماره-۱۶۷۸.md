---
title: >-
    شمارهٔ ۱۶۷۸
---
# شمارهٔ ۱۶۷۸

<div class="b" id="bn1"><div class="m1"><p>گر باده می خورم به سر من خمار تو</p></div>
<div class="m2"><p>ور در چمن روم به دلم خارخار تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون شد ز نالشم جگر سنگ و همچنان</p></div>
<div class="m2"><p>با سنگ خویشتن دل با استوار تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دیدن تو مست و خرابم تمام روز</p></div>
<div class="m2"><p>جان می کنم تمام شب اندر خمار تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیرون جهان سمند که پیشت به صد هوس</p></div>
<div class="m2"><p>مردن به پای خویشتن آید شکار تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل راتب غم تو چو بی من نمی خورد</p></div>
<div class="m2"><p>شرمنده دلم من و دل شرمسار تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمرم به یاری سگ کوی تو شد به سر</p></div>
<div class="m2"><p>روزی نگفتیش که چگونه ست یار تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داغ تو دارم ار نکنم خدمتی دگر</p></div>
<div class="m2"><p>کم زانکه با زمین برم این یادگار تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر کدام روز بود عقل و جان و دل</p></div>
<div class="m2"><p>گر این متاع خرج نگردد به کار تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد پاره شد چو غنچه دل خسرو و هنوز</p></div>
<div class="m2"><p>باری گلی کشفت مرا در بهار تو</p></div></div>