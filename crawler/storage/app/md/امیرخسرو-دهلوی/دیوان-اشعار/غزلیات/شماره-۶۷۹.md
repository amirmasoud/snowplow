---
title: >-
    شمارهٔ ۶۷۹
---
# شمارهٔ ۶۷۹

<div class="b" id="bn1"><div class="m1"><p>فرخ آن عیدی که جان قربانی جانان بود</p></div>
<div class="m2"><p>خرم آن جانی که پیش نیکوان قربان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نگوید نازنین من مبارک باد عید</p></div>
<div class="m2"><p>جان شکر ریزی کند، دیده گلاب افشان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بذله گوی و عشوه ساز و شوخ چشم و غمزه زن</p></div>
<div class="m2"><p>خوبرویی کاین چنین باشد بلای جان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب چشمم روز عید از آستانش بازداشت</p></div>
<div class="m2"><p>باز دارد از صلا عیدی که در باران بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان دهد، جانا، دهانت هر که را شربت دهد</p></div>
<div class="m2"><p>اینچنین شربت نباشد، چشمه حیوان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر شادی صورت میمون تو هر روز نیست</p></div>
<div class="m2"><p>عید تا سالی، چه غم باشد، اگر قربان بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رو به گاه تیغ راندن سوی قربانی مدار</p></div>
<div class="m2"><p>تا مگر جان دادن آن بیچاره را آسان بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوستان از صحبت ما، گر چه آزاد آمدند</p></div>
<div class="m2"><p>تا زید خسرو، غلام و بنده ایشان بود</p></div></div>