---
title: >-
    شمارهٔ ۷۳۴
---
# شمارهٔ ۷۳۴

<div class="b" id="bn1"><div class="m1"><p>باش تا بار دگر آن پسر این سو آید</p></div>
<div class="m2"><p>مست و خوش پیش ملامتگر بدخو آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه من کشته شوم زان، که بگوید به کمند؟</p></div>
<div class="m2"><p>وه که آن عشوه گری هات چه نیکو آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه اندر دلم و پیش دو چشمم، یارب</p></div>
<div class="m2"><p>پیش آن نرگس خونخواره جادو آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه بد گفت مرا روی چو ماهش بینید</p></div>
<div class="m2"><p>آن همه در نظر من بر سر او آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل که در زلف گره بست غم آن نیست، غم آنست</p></div>
<div class="m2"><p>که به خفتن گرهش در سر پهلو آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست زان شوخ، همه از دل پر خون من است</p></div>
<div class="m2"><p>هر دمم این همه خونابه که بر رو آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسروا، زمزمه عشق نهان نتوان داشت</p></div>
<div class="m2"><p>هر کجا عود بر آتش بنهی، بو آید</p></div></div>