---
title: >-
    شمارهٔ ۹۳۷
---
# شمارهٔ ۹۳۷

<div class="b" id="bn1"><div class="m1"><p>وفا ز یار جفاکار چون نمی آید</p></div>
<div class="m2"><p>جفا ز یار وفادار هم نمی شاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جفا چه باشد و نام وفا که باز برد؟</p></div>
<div class="m2"><p>به حضرتی که دو عالم به هیچ برناید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا ز جمله جهان صحبت تو می باید</p></div>
<div class="m2"><p>ترا ز خدمت من ذره ای نمی باید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رغم خاطر من قول دشمنان کردی</p></div>
<div class="m2"><p>چه طالعی ست مرا آه، تا چه پیش آید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منوش می به حریفان سفله طبع خسیس</p></div>
<div class="m2"><p>که تا به وقت خمارت صداع نفزاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آبروی محبت که بی غرض بشنو</p></div>
<div class="m2"><p>که از مصاحب ناجنس هیچ نگشاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بترس از آه دل من که مبتلای توام</p></div>
<div class="m2"><p>به سالها دگرت کی چو من به دست آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به روز وصل تو دارد خبر، دل شادی</p></div>
<div class="m2"><p>مرا دو دیده شب هجر خون بپالاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه خلوت خسرو منور است، ولی</p></div>
<div class="m2"><p>به جز حضور تواش هیچ در نمی باید</p></div></div>