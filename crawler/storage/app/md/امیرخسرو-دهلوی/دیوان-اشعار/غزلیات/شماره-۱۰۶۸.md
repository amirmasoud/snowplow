---
title: >-
    شمارهٔ ۱۰۶۸
---
# شمارهٔ ۱۰۶۸

<div class="b" id="bn1"><div class="m1"><p>سوار چابک من پیش چشم من مگذر</p></div>
<div class="m2"><p>مرا بکشتی، ازین سو ز بهر من مگذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببین که چشم کسی چون بود، ز بهر خدا</p></div>
<div class="m2"><p>بدین صفت که تویی پیش مرد و زن مگذر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهانه می طلبند اهل دل که جان بدهند</p></div>
<div class="m2"><p>بپوش روی، وگرنه در انجمن مگذر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرم به خاک ره تست، پرشکسته مرو</p></div>
<div class="m2"><p>نماز می کنم، آخر ز پیش من مگذر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دیده و دل و جان بگذری که جان توام</p></div>
<div class="m2"><p>رواست زان همه بگذر، ازین سخن مگذر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غبارهاست ز جعد تو در دلم بسیار</p></div>
<div class="m2"><p>کشان به روی زمین جعد چون سمن مگذر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلا، ز زلف گذر بر لبت اگر نتوان</p></div>
<div class="m2"><p>ولیک تا بتوانی از آن دهن مگذر</p></div></div>