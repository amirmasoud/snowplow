---
title: >-
    شمارهٔ ۱۸۱۹
---
# شمارهٔ ۱۸۱۹

<div class="b" id="bn1"><div class="m1"><p>ای آنکه تمام هم چو ماهی</p></div>
<div class="m2"><p>با زلف چو چتر پادشاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم ز برای نقش و زلفت</p></div>
<div class="m2"><p>از دیده برون کشد سیاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خط سیاه خود ببینی</p></div>
<div class="m2"><p>بر مشک دهی به خون گواهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای زلف ترت مراغه کرده</p></div>
<div class="m2"><p>بر روی تو چون در آب ماهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر چه شود گر از لب خویش</p></div>
<div class="m2"><p>یک بوسه برای من بخواهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خسرو خسته رو بگردان</p></div>
<div class="m2"><p>زان رو که تمام همچو ماهی</p></div></div>