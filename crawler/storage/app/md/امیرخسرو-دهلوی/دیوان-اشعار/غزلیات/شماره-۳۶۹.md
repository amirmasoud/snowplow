---
title: >-
    شمارهٔ ۳۶۹
---
# شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>هر که روی تو دید جان دانست</p></div>
<div class="m2"><p>لب شیرینت، را همان دانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن تو عالمی بخواهد سوخت</p></div>
<div class="m2"><p>هم در آغاز می توان دانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرخ کردی به بوسه ای جانی</p></div>
<div class="m2"><p>بنده بخرید و رایگان دانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذقنت چه نمود و دل به خیال</p></div>
<div class="m2"><p>بوسه ای زد، مگر دهان دانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ز هجر تو بس که تنگ آمد</p></div>
<div class="m2"><p>مرگ را عمر جاودان دانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دی به کویت تن ضعیف مرا</p></div>
<div class="m2"><p>زاغ بربود و استخوان دانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غمزه تو زبان کشید ز من</p></div>
<div class="m2"><p>که مرا نیک بی زبانی دانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرد بر من دلت به نادانی</p></div>
<div class="m2"><p>هر چه از جور بیکران دانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش ازین غم نبود خسرو را</p></div>
<div class="m2"><p>غم که دانست این زمان دانست</p></div></div>