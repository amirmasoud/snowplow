---
title: >-
    شمارهٔ ۱۸۸۵
---
# شمارهٔ ۱۸۸۵

<div class="b" id="bn1"><div class="m1"><p>تو خود به غمزه سراسر کرشمه و نازی</p></div>
<div class="m2"><p>چه حاجت است که با ما کرشمه ای سازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تیغ بازی مژگان مریز خون مرا</p></div>
<div class="m2"><p>که نیست ریختن خون عاشقان بازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب آمدی و نگفتم به کس، ولی چه کنم؟</p></div>
<div class="m2"><p>که بوی زلف به همسایه کرد غمازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حدیث حسن کسی را به عهد تو نرسد</p></div>
<div class="m2"><p>ترا رسد که، نگارا، به حسن ممتازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن شده ست لگدکوب بلبلان سر سرو</p></div>
<div class="m2"><p>که پیش قامت تو می کند سرافرازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو جان به پای تو انداختم، خیال بگفت</p></div>
<div class="m2"><p>که من از آن توام تا تو دل نیندازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رضا به کشتن خود داد خسروت که ز لب</p></div>
<div class="m2"><p>به زنده کردن او چون مسیح پردازی</p></div></div>