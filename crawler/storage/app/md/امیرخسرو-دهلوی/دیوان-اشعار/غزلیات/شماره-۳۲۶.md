---
title: >-
    شمارهٔ ۳۲۶
---
# شمارهٔ ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>شب فراق سیاه و مرا سیاه تر است</p></div>
<div class="m2"><p>که شام تا سحرم زلف یار در نظر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چگونه تیره نباشد رخم که شمع مراد</p></div>
<div class="m2"><p>نمی فروزد ازین آتشی که در جگر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگو که چند شوی بی خبر ز مستی عشق</p></div>
<div class="m2"><p>کسی که مستیش از عشق نیست بی خبر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آن بلا که رسد از بدان رسد همه را</p></div>
<div class="m2"><p>ز نیکوانست مرا هر بلا که گرد سر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفیر و ناله خلق از جفای خار بود</p></div>
<div class="m2"><p>اگر ز بلبل پرسی جفای گل بتر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به تشنگی بیابان عشق شد معلوم</p></div>
<div class="m2"><p>که سایه شین سلامت نه مرد این سفر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به پای بوس هوس بردنم فضول بود</p></div>
<div class="m2"><p>همین بس است که بالینم آستان در است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگو که گر بکشد عشق مات، عیب مگیر</p></div>
<div class="m2"><p>چه جای عیب که خود عشق را همین هنر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو مست بودی و خسرو خراب تو سحری</p></div>
<div class="m2"><p>گذشت عمر و هنوزم خمار آن سحر است</p></div></div>