---
title: >-
    شمارهٔ ۳۹۰
---
# شمارهٔ ۳۹۰

<div class="b" id="bn1"><div class="m1"><p>زلف مشکینش که گویی را به چوگان یافته ست</p></div>
<div class="m2"><p>گو به صحن دیده بازی کن که میدان یافته ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تو گوی چرخ بردی زان ز نخ چوگان چرخ</p></div>
<div class="m2"><p>ای بسا بازی که آن گوی ز نخدان یافته ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد بر گو در زنخدان گر دمی آرد خطت</p></div>
<div class="m2"><p>مشک زلفت را که بر هر سو پریشان یافته ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تشنه ای را با دهانت آشنایی ده که او</p></div>
<div class="m2"><p>تا به لب چاه زنخ پر آب حیوان یافته ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خنده تو یافته ست اندر دهان کان گهر</p></div>
<div class="m2"><p>گوهر خود را خجل بیرون دهد کان یافته ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بناگوشت دلم گم شد، کسی حاضر نبود</p></div>
<div class="m2"><p>جز خط و زلفت کسی احضار ایشان یافته ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز وصلم هست کوتاه و شب هجرم دراز</p></div>
<div class="m2"><p>گر دم سرد جهان رسم زمستان یافته ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهسوارا را، گوی در میدان زیبایی فگن</p></div>
<div class="m2"><p>خاصه کاعظم باربک از شاه جولان یافته ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر گهر کان غیر مدحت در دهان خسرو است</p></div>
<div class="m2"><p>سنگ ریزه ست، آنکه اندر زیر دندان یافته ست</p></div></div>