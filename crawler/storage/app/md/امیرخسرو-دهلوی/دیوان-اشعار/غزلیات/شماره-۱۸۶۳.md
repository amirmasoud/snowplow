---
title: >-
    شمارهٔ ۱۸۶۳
---
# شمارهٔ ۱۸۶۳

<div class="b" id="bn1"><div class="m1"><p>ای ز رویت چشم جان را روشنی</p></div>
<div class="m2"><p>زلف مشکن تا دلم را نشکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم ایمن شو که من زآن توام</p></div>
<div class="m2"><p>عید بر عمر است و آنگه ایمنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چیست کز دستم نمی نوشی شراب؟</p></div>
<div class="m2"><p>روشنم شد تشنه خون منی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر زمان گویی منال از دوستان</p></div>
<div class="m2"><p>چه اندر بازی، ای یار، افگنی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر این جان است کز تن می رود</p></div>
<div class="m2"><p>آخر این تیغ است و بر من می زنی!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مانده با دامان آن یوسف دلم</p></div>
<div class="m2"><p>آخر این خون هم در آن پیراهنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پاک دامانی، تو دانی چاره چیست</p></div>
<div class="m2"><p>ما و معشوق و می و تردامنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا چه خواهد شد، ندانم حال من</p></div>
<div class="m2"><p>من اسیر و تیغ خوبان گردنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسروا، از کندن جان چاره نیست</p></div>
<div class="m2"><p>چون نمی آری که دل را بر کنی</p></div></div>