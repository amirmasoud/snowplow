---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>در هجر توام کار به جز آه و فغان نیست</p></div>
<div class="m2"><p>در پیش توام دان که زبانم به دهان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی دوست اگر خلق به جان می زید و سر</p></div>
<div class="m2"><p>هم جان و سر دوست که ما را سر آن نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سهل است اگر هر دو جهان باز گذارند</p></div>
<div class="m2"><p>از بهر نگاری که چو او در دو جهان نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما زنده بدوییم که جان می رود از ما</p></div>
<div class="m2"><p>بر وی که به معشوقه زید منت آن نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشنو سخن عاشقی از هرزه زبانان</p></div>
<div class="m2"><p>کاین کار دل است ای پسر و کار زبان نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی که هم آغوش خیالم به چه سانی</p></div>
<div class="m2"><p>خواب خوش مجنون به بر دوست نهان نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو ز تو گر دل بستد صاحب حسنی</p></div>
<div class="m2"><p>خوش باش که یوسف به یکی قلب گران نیست</p></div></div>