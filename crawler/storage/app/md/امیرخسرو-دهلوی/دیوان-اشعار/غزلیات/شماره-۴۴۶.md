---
title: >-
    شمارهٔ ۴۴۶
---
# شمارهٔ ۴۴۶

<div class="b" id="bn1"><div class="m1"><p>بسند است آنکه زلف بناگوشت علم گیرد</p></div>
<div class="m2"><p>مفرما عارض چون سیم را کز خط حشم گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو سبزه خویش را خط تو خواند جای آن دارد</p></div>
<div class="m2"><p>که گل از خنده بر خاک افتد و غنچه شکم گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس از ماهیت می بینم، مه من کج مکن ابرو</p></div>
<div class="m2"><p>گره مفگن به پیشانی که مه در غره کم گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم سوی دهانت می رود، چون در تو می بینم</p></div>
<div class="m2"><p>مگر می خواهد از بیم فنا راه عدم گیرد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیالت بیشتر می بینم اندر دیده پر نم</p></div>
<div class="m2"><p>اگر چه روی در آیینه ننماید چو دم گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ستم در عهد تو زان گونه خونین شد که هر ساعت</p></div>
<div class="m2"><p>اجل بهر شفاعت آید و دست ستم گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا بر تخت وصلت ناخن پایی نگرددتر</p></div>
<div class="m2"><p>اگر اطراف عالم سر به سر سیلاب غم گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حدیث دیده و دل چون نویسد سوی تو خسرو</p></div>
<div class="m2"><p>که کاغذتر شود از گریه، آتش در قلم گیرد</p></div></div>