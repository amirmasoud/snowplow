---
title: >-
    شمارهٔ ۶۸۵
---
# شمارهٔ ۶۸۵

<div class="b" id="bn1"><div class="m1"><p>گر نمی بینم دمی در روی او غم می کشد</p></div>
<div class="m2"><p>ور کسی پهلوی او می بینم آن هم می کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من به عشق یک نظر می میرم واو با کسان</p></div>
<div class="m2"><p>چون زید مسکین گرفتاری کش این غم می کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من ز محرم حیله می پرسم کز این غم چون زیم</p></div>
<div class="m2"><p>وین خود از کشتن بتر کز طعنه محرم می کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کشد از چشم و خوشتر آنکه می گوید که خلق</p></div>
<div class="m2"><p>خود همی میرند، کس را چشم پرنم می کشد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دل خسته، چه جویی، مرهم از شیرین لبی؟</p></div>
<div class="m2"><p>کو به شوخی دردمندان را به مرهم می کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند پوشم گریه را تا کس نداند راز من؟</p></div>
<div class="m2"><p>بیشتر هر جا مرا این چشم پرنم می کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلف را زین گونه، جانا، هم مده رشته دراز</p></div>
<div class="m2"><p>کو هزاران بسته را در زیر هر خم می کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کرشمه خلق را تا می توانی می کشی</p></div>
<div class="m2"><p>ور کسی از تو رها شد زلف در هم می کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسروا، کی غم خورد، گر تو بمیری در غمش</p></div>
<div class="m2"><p>آنکه صد همچون تو عاشق را به یک دم می کشد</p></div></div>