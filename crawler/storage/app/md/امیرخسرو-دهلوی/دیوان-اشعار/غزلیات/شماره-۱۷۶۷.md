---
title: >-
    شمارهٔ ۱۷۶۷
---
# شمارهٔ ۱۷۶۷

<div class="b" id="bn1"><div class="m1"><p>ای دل، ار تو عاشقی، زین غم خلاص جان مخواه</p></div>
<div class="m2"><p>کار را سامان مجو و درد را درمان مخواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بلا و فتنه ترسی، چشم در خوبان منه</p></div>
<div class="m2"><p>بیم چاوشان کنی، در یوزه از سلطان مخواه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار محمل راند، در ویرانه هجران بمیر</p></div>
<div class="m2"><p>نوح کشتی برد، ما را غوطه در طوفان مخواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دشمن کش دوست می خوانی، مرادت کی دهد؟</p></div>
<div class="m2"><p>نام قصاب ار خضر شد، چشمه حیوان مخواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهسوارا، ناوک مژگان زدی جان بستدی</p></div>
<div class="m2"><p>بیشتر زان چون ندارم، مزد آن پیکان مخواه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تن عاشق ز بهر خون او پرسش مکن</p></div>
<div class="m2"><p>از بز قربان ز بهر کشتنش فرمان مخواه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تن نه مستورست، عصمت از سگ گلخن مجوی</p></div>
<div class="m2"><p>دل نه آبادست، عشره از ده ویران مخواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاک پایش را به دل می خواهی، ای دیده، خطاست</p></div>
<div class="m2"><p>گوهری را کش دو عالم قیمت است ارزان مخواه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من اسیر شاهد و تو زهد خواهی، ای رفیق</p></div>
<div class="m2"><p>آنچه ناید از من رسوای تر دامان، مخواه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زاری خسرو مجو در سینه های بی خبر</p></div>
<div class="m2"><p>ناله مرغ اسیر از بلبل بستان مخواه</p></div></div>