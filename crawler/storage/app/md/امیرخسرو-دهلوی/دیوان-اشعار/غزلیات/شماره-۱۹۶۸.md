---
title: >-
    شمارهٔ ۱۹۶۸
---
# شمارهٔ ۱۹۶۸

<div class="b" id="bn1"><div class="m1"><p>من اشک بیدلان را خنده می پنداشتم روزی</p></div>
<div class="m2"><p>کنون بر می دهد تخمی که من می کاشتم روزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم اول روز کان زلف سیاهم پیش چشم آمد</p></div>
<div class="m2"><p>دل من زد که از وی شام گردد چاشتم روزی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو، ای ناخورده جام عشق، هشیاری مکن دعوی</p></div>
<div class="m2"><p>که من هم خویش را هشیار می پنداشتم روزی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو چشمم بر رخش داده به کویش در نهم، پایی</p></div>
<div class="m2"><p>هم از خاک درش این رخنه می انباشتم روزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل از درد کهن خون گشت و محرومی بختم بین</p></div>
<div class="m2"><p>کز آب دیده رازی بر درش بنگاشتم روزی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو گر بر جای دل داری، مرا گر نیست دل بر جا</p></div>
<div class="m2"><p>مزن بر حال من طعنه که من هم داشتم روزی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملامت سوخت خسرو را، همه پاداش آن است این</p></div>
<div class="m2"><p>که بر اهل ملامت بد همی انگاشتم روزی</p></div></div>