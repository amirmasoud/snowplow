---
title: >-
    شمارهٔ ۱۸۱۸
---
# شمارهٔ ۱۸۱۸

<div class="b" id="bn1"><div class="m1"><p>ای برده دلم به دلستانی</p></div>
<div class="m2"><p>هم جان منی و هم جهانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان می رودم برون و غم نیست</p></div>
<div class="m2"><p>غم زانست که در میان جانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دود از دل عاشقان برآرد</p></div>
<div class="m2"><p>حسن تو ز آتش جوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سوز غم تو برنخیزم</p></div>
<div class="m2"><p>با آنکه بر آتشم نشانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگشای دهان خویش تا دست</p></div>
<div class="m2"><p>شوییم ز آب زندگانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر شب منم و خیال زلفت</p></div>
<div class="m2"><p>شبهای دراز و پاسبانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من خواهم داد جان به عشقت</p></div>
<div class="m2"><p>هر چند تو قدر آن ندانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دوستی تو ناتوانم</p></div>
<div class="m2"><p>ای دوست، ببر اگر توانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو که بمرد، زنده گردد</p></div>
<div class="m2"><p>گر دم دهدش مسیح ثانی</p></div></div>