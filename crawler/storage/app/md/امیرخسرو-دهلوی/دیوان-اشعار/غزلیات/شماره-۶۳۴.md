---
title: >-
    شمارهٔ ۶۳۴
---
# شمارهٔ ۶۳۴

<div class="b" id="bn1"><div class="m1"><p>تا جهان بود، از جهان هرگز دلم خرم نبود</p></div>
<div class="m2"><p>خرمی خود هیچگه گویی که در عالم نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه کار عاشقان پیوسته سامانی نداشت</p></div>
<div class="m2"><p>اینچنین یک بارگی هم ابتر و در هم نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم برون ز اندازه شد ما را و دل بر جا نماند</p></div>
<div class="m2"><p>ای خوش آن وقتی که دل بر جای بود و غم نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم همه وقت و طرب یکدم بود، باری مرا</p></div>
<div class="m2"><p>در تمام عمر می اندیشم آن یکدم نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرخ اگر بد با دل خرم بود، با من چراست؟</p></div>
<div class="m2"><p>تا دل من بود، باری هیچگه خرم نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با دل مجروح رفتم دی به دکان طبیب</p></div>
<div class="m2"><p>حقه را چون باز کرد، از بخت من مرهم نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم این غمهای دل بیرون دهم تا وارهم</p></div>
<div class="m2"><p>در همه عالم بجستم هیچ جا محرم نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آدمی خوشدل نباشد، گر چه در جنت بود</p></div>
<div class="m2"><p>آدمی خود کی تواند بود، چون آدم نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دهر با مردم نسازد، زان خران دارند گنج</p></div>
<div class="m2"><p>ور نه این مردار در ویرانه او کم نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر توانی، خسروا، دل را عمارت کن، از آنک</p></div>
<div class="m2"><p>در جهان کس را بنای آب و گل محکم نبود</p></div></div>