---
title: >-
    شمارهٔ ۱۳۸۶
---
# شمارهٔ ۱۳۸۶

<div class="b" id="bn1"><div class="m1"><p>گر سخن زان قد رعنا گویم</p></div>
<div class="m2"><p>بیش از آن است که زیبا گویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با چنان قد چو کمر بربندی</p></div>
<div class="m2"><p>جای آن است که بر جا گویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا تو در سینه درونی دل را</p></div>
<div class="m2"><p>تیر در خانه جوزا گویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل من حامل غم کردی و من</p></div>
<div class="m2"><p>زاده الله تعالی گویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دو چشمم که در آب اند یکی</p></div>
<div class="m2"><p>هر یکی دو یم دریا گویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی رقیب آی شبی تا پیشت</p></div>
<div class="m2"><p>حال خود گویم و تنها گویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر نهم بر کف پایت، وانگاه</p></div>
<div class="m2"><p>لیتنی کنت ترابا گویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن چنان سوخته ام از جورت</p></div>
<div class="m2"><p>که بسوزد دلت او را گویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حال خسرو نگر، ابرو مشکن</p></div>
<div class="m2"><p>گر نگویم سخنی یا گویم</p></div></div>