---
title: >-
    شمارهٔ ۳۲۱
---
# شمارهٔ ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>ز خون دل که به رخسار ماجرای من است</p></div>
<div class="m2"><p>بخوان به لطف که دیباچه وفای من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس رسیده به آخر، هوس نماند جز این</p></div>
<div class="m2"><p>که بشنوم ز تو کاین مردان از برای من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جای دعای غمت می کنم که دیر زیاد</p></div>
<div class="m2"><p>کزو فزایش این درد بی دوای من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درون جان تویی از بهر آنش دارم دوست</p></div>
<div class="m2"><p>وگرنه جان مرا بی تو یک بلای من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فضول بین تو که جایی همی نهم خود را</p></div>
<div class="m2"><p>که زیر پای سگ کوی دوست جای من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه حد دعوی نیلوفر آنکه لاف غرور</p></div>
<div class="m2"><p>زند که چشمه خورشید آشنای من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسوختم ز دل و هم به پیش دل گفتم</p></div>
<div class="m2"><p>که روز این دل بد روز من بلای من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا روم که مرا کرد بوی او گمراه</p></div>
<div class="m2"><p>که هر سپیده دم آن بوی آشنای من است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنال پیش درش، خسروا، که آن سلطان</p></div>
<div class="m2"><p>شناخته ست که این ناله گدای من است</p></div></div>