---
title: >-
    شمارهٔ ۱۵۴۱
---
# شمارهٔ ۱۵۴۱

<div class="b" id="bn1"><div class="m1"><p>آمد بهار، ای یار من، بشکفت گلها در چمن</p></div>
<div class="m2"><p>شد در نوا هر بلبلی بر شاخ سرو و نارون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باد صبا گلریز شد، ساقی، بده می تا شوم</p></div>
<div class="m2"><p>گه از خمار چشم تو مست و گه از دردی دن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با عارض زیبای تو ما را چه جای باغ و گل</p></div>
<div class="m2"><p>با قامت رعنای تو چه جای سرو و نارون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چندان به یاد عارضت بارم ز جوی دیده خون</p></div>
<div class="m2"><p>تا لاله هایت را دمد سنبل بر اطراف چمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمم چو در هر گوشه ای سرشار دارد چشمه ای</p></div>
<div class="m2"><p>در چشمم ار ناری گهی، باری بیا در چشم من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شادم اگر میرم زغم، باری ز محنت وارهم</p></div>
<div class="m2"><p>از هجرت، ای زیبا صنم، تا چند باشم ممتحن؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاهیم سازد بی خبر، گاهیم نآرد در نظر</p></div>
<div class="m2"><p>با عاشقان آن چشم را باز این چه سحر است و فتن؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داریم با زلفت، بتا، وقت خوش و این قصه را</p></div>
<div class="m2"><p>مگشای با باد صبا، وقت مرا بر هم مزن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از انتظارت دیده ها شد خسرو بیچاره را</p></div>
<div class="m2"><p>ای یوسف فرخ لقا، بویی فرست از پیرهن</p></div></div>