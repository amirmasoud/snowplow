---
title: >-
    شمارهٔ ۱۷۵۹
---
# شمارهٔ ۱۷۵۹

<div class="b" id="bn1"><div class="m1"><p>ای رخت شمع حسن برکرده</p></div>
<div class="m2"><p>شب عشاق را سحر کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه به زلف تو گم شده، خود را</p></div>
<div class="m2"><p>می بجوید چراغ بر کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب تو بر شکر نهاده خراج</p></div>
<div class="m2"><p>چشم تو اندکی نظر کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن من نی شد و خیال لبت</p></div>
<div class="m2"><p>بند بندم چو نیشکر کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عکس دندان تو به طرف دهن</p></div>
<div class="m2"><p>قطره اشک را سحر کرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پختگی دلم که پر خون است</p></div>
<div class="m2"><p>دمبدم از غم تو سر کرده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی خبر کرد ناله گوش مرا</p></div>
<div class="m2"><p>لیک گوش ترا خبر کرده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بینمت یک شبی به خانه خویش</p></div>
<div class="m2"><p>چو مهی سر به عقده در کرده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو چو آب حیات بر سر من</p></div>
<div class="m2"><p>من به پای تو دیده تر کرده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خسرو اندر میانت پیچیده</p></div>
<div class="m2"><p>موی را خم ز مو کمر کرده</p></div></div>