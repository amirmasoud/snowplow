---
title: >-
    شمارهٔ ۳۵۹
---
# شمارهٔ ۳۵۹

<div class="b" id="bn1"><div class="m1"><p>رخ تو نور دیده قمر است</p></div>
<div class="m2"><p>لب تو سرخ رویی شکر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با تو، ای یکسر آمده به دلم</p></div>
<div class="m2"><p>که کند شرکتی، گدا و سر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار دیگر مکن، مکن شوخی</p></div>
<div class="m2"><p>زانکه، ای شوخ کار تو دگر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ز پای خودم دهی خاکی</p></div>
<div class="m2"><p>خاک پای تو سرمه بصر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زار زار از غم تو می میرم</p></div>
<div class="m2"><p>چون نه زور است بنده را نه زر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نظری کن کز آن دو چشم سیاه</p></div>
<div class="m2"><p>دیده در انتظار یک نظر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنده خسرو در آرزوی لبت</p></div>
<div class="m2"><p>نمک تو که نیش در جگر است</p></div></div>