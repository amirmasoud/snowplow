---
title: >-
    شمارهٔ ۱۶۲۱
---
# شمارهٔ ۱۶۲۱

<div class="b" id="bn1"><div class="m1"><p>ای مشک وام داده زلفت به آهوی چین</p></div>
<div class="m2"><p>زان زلف مشکفامت عشاق گشته مشکین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برخاست بوی ریحان زان طره چو سنبل</p></div>
<div class="m2"><p>بنشست باد بستان زان عارض چو نسرین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک ره به نیم خنده دندان نمای ما را</p></div>
<div class="m2"><p>تا اوفتادن آید دندانه های پروین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسیار روی خوبان دیدم، ولیک بی تو</p></div>
<div class="m2"><p>خاطر نمی پذیرد از هیچ روی تسکین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون من نمی توانم برخاستن ز عشقت</p></div>
<div class="m2"><p>گه گه اگر توانی نزد من آی و بنشین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیراهن جفا را هر روز می بپوشی</p></div>
<div class="m2"><p>حالم چو نیک دانی بر خود مپوش چندین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لب خواهد از تو خسرو، گویی که هیچ ندهم</p></div>
<div class="m2"><p>گر هیچ نیست، جانا، باری زبان شیرین</p></div></div>