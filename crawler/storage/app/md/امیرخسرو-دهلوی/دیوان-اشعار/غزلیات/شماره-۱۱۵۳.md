---
title: >-
    شمارهٔ ۱۱۵۳
---
# شمارهٔ ۱۱۵۳

<div class="b" id="bn1"><div class="m1"><p>ای جفا آموخته، از غمزه بدخوی خویش</p></div>
<div class="m2"><p>نیکویی ناموزی آخر از رخ نیکوی خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می روم در راه بیداد و جفا از خوی بد</p></div>
<div class="m2"><p>بد نباشد گر دمی باز ایستی از خوی خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون تنم از ناتوانی موی شد بی هیچ فرق</p></div>
<div class="m2"><p>فرق کن گر می توانی از تنم تا موی خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون به پهلوی خودم در رنج و بس ترسم که پیش</p></div>
<div class="m2"><p>خویشتن را هم ببینم بعد ازین پهلوی خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی من از اشک و رویت از صفا آیینه شد</p></div>
<div class="m2"><p>روی خود در روی من بین، روی من در روی خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک دم، ای آیینه جان، رو نما تا جا کنم</p></div>
<div class="m2"><p>بر سر دست خودت یا بر سر زانوی خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم باشد زیر ابرو، ور تو باشی چشم من</p></div>
<div class="m2"><p>از عزیزی شانمت بالاتر از ابروی خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از نزاری آن چنان گشتم که گر می بنگرم</p></div>
<div class="m2"><p>می توانم دیدن از یک سوی دیگر سوی خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک شبی دزدیده می خواهم که آیم سوی تو</p></div>
<div class="m2"><p>که شفیع عفو باشی بر سگان کوی خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر خیال قامتت اندر سر سرو اوفتد</p></div>
<div class="m2"><p>سرنگون همچون خیال خود فتد و در جوی خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گوش هندو پاره باشد، ور منم هندوی تو</p></div>
<div class="m2"><p>پاره کن گوش و مکن پاره دل هندوی خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر زمان گویی که خسرو جادویی چون می کنی</p></div>
<div class="m2"><p>این مپرس از من، بپرس از غمزه جادوی خویش</p></div></div>