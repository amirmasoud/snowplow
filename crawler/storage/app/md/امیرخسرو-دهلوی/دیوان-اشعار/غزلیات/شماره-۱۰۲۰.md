---
title: >-
    شمارهٔ ۱۰۲۰
---
# شمارهٔ ۱۰۲۰

<div class="b" id="bn1"><div class="m1"><p>سفیده دم چو در از ابر درفشان بچکد</p></div>
<div class="m2"><p>به کام لاله و سون زلال جان بچکد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روان کن آن می چون آفتاب گرماگرم</p></div>
<div class="m2"><p>چنان که خوی ز بناگوش دوستان بچکد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شراب آب حیات است وجان ما مسرور</p></div>
<div class="m2"><p>که مرده زنده کند چون به خاکدان بچکد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشا کشیدن می بر بساط سبزه چو ابر</p></div>
<div class="m2"><p>کشیده باشد و باران یگان یگان بچکد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان بر آب خود آید چمن ز ابر بهار</p></div>
<div class="m2"><p>که هر زمان تری از شاخ ارغوان بچکد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به روی نازک گل تیز منگر، ای نرگس</p></div>
<div class="m2"><p>که خون ز رویش ترسم بناگهان بچکد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز شاخ سبزه چنان آب می چکد ز تری</p></div>
<div class="m2"><p>که در ز خانه خسرو به هر زمان بچکد</p></div></div>