---
title: >-
    بخش ۵
---
# بخش ۵

<div class="b" id="bn1"><div class="m1"><p>دلم چون به گوهر کشی خاص گشت</p></div>
<div class="m2"><p>به دریای اندیشه غواص گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر غوطه چندان فرو ریخت در</p></div>
<div class="m2"><p>که دریا تهی گشت و آفاق پر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نثاری کزان در برانگیختم</p></div>
<div class="m2"><p>به درگاه پیغمبرش ریختم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من افشاندم و آسمان برگرفت</p></div>
<div class="m2"><p>عطارد ببوسید و بر سر گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دریغ آمدم کاینچنین گوهری</p></div>
<div class="m2"><p>برم تحفه در خدمت دیگری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ادب نایدم بیش ازین در ضمیر</p></div>
<div class="m2"><p>کزان سازم آرایش مدح پیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پناه جهان دین حق را نظام</p></div>
<div class="m2"><p>ره قدس را پیشوای تمام</p></div></div>