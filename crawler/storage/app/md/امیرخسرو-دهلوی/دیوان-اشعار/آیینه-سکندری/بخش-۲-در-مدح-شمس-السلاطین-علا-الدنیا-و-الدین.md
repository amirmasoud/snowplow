---
title: >-
    بخش ۲ - در مدح شمس السلاطین علاء الدنیا و الدین
---
# بخش ۲ - در مدح شمس السلاطین علاء الدنیا و الدین

<div class="b" id="bn1"><div class="m1"><p>خرامان شو ای خامهٔ گنج ریز</p></div>
<div class="m2"><p>به در سفتن الماس را دار تیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن را چنان پایه بر کش به ماه</p></div>
<div class="m2"><p>که بوسد به جرأت کف پای شاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علاء دین اسکندر تاج بخش</p></div>
<div class="m2"><p>زرفعت به گردون روان کرد رخش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محمد جهانگیر حیدر مصاف</p></div>
<div class="m2"><p>که از پیش او پس خزد کوه قاف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هنرمندکش برگ نبود فراخ</p></div>
<div class="m2"><p>چه میوه دهد دیگری را ز شاخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به شهر این مثل شهرهٔ عالمست</p></div>
<div class="m2"><p>که هرکش هنر بیش روزی کم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا صد فغان زین هنرهای خام</p></div>
<div class="m2"><p>که نزد خرد هست عیبش تمام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه روز عمرم به خفتن گذشت</p></div>
<div class="m2"><p>شب من در افسانه گفتن گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون در باز کردم نخست از قلم</p></div>
<div class="m2"><p>ز مطلع به انوار دادم علم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وزان انگبین شربت انگیختم</p></div>
<div class="m2"><p>به شیرین و خسرو فرو ریختم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وز انجا فرس پیشتر تاختم</p></div>
<div class="m2"><p>به مجنون و لیلی سرافراختم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنون بر سریر هنر پروری</p></div>
<div class="m2"><p>کنم جلوهٔ ملک اسکندری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز دانا هر آن در که نا سفته ماند</p></div>
<div class="m2"><p>فشانم به نوعی که دانم فشاند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هنر پرور گنجه گویای پیش</p></div>
<div class="m2"><p>که گنج هنر داشت ز اندازه بیش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نظر چون براین جام صهبا گماشت</p></div>
<div class="m2"><p>ستد صافی و درد بر ما گذاشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من ار چه بدانمی گران سر شوم</p></div>
<div class="m2"><p>کجا با حریفان برابر شوم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سکندر که فرخ جهان شاه بود</p></div>
<div class="m2"><p>به فرخندگی خاص درگاه بد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گروهی زدند از ولایت درش</p></div>
<div class="m2"><p>گروهی نبشتند پیغمبرش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به تحقیق چون کرده شد باز جست</p></div>
<div class="m2"><p>درستی شدش بر ولایت درست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شگفتی که دانا برو باز بست</p></div>
<div class="m2"><p>گر اعجاز نبود کرامات هست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مگس بهر آن دست مالد به درد</p></div>
<div class="m2"><p>که نارد ز صد کاسه یک لقمه خورد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ازان مار بر خویش پیچد به رنج</p></div>
<div class="m2"><p>که روزیش خاک است بالای گنج</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر از خوان من نبودت توشهٔ</p></div>
<div class="m2"><p>جوی باشد آخر ز هر خوشه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو یک جو به یک سال گردد منی</p></div>
<div class="m2"><p>پس از روزگاری شود خرمنی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کنون دارم امید کین تخم پاک</p></div>
<div class="m2"><p>بسی خوشهٔ‌تر بر ارد ز خاک</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نیندیشی اول چو در پیشها</p></div>
<div class="m2"><p>سرانجام پیش آید اندیشها</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کند هر کسی پیشهٔ خویشتن</p></div>
<div class="m2"><p>به مقدار اندیشهٔ خویشتن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>قلم ران این نامهٔ چون بهشت</p></div>
<div class="m2"><p>چنین کرد دیباچه را سر به نشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که چون شد به خاک اختر فیلقوس</p></div>
<div class="m2"><p>به پای سکندر جهان داد بوس</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در عدل راکرد زآنگونه باز</p></div>
<div class="m2"><p>که هم خوابهٔ کبک شد جره باز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو پرداخت از دشمنان مرز و بوم</p></div>
<div class="m2"><p>به کشور گشایی روان شد ز روم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نخست آرم از رزم خاقان سخن</p></div>
<div class="m2"><p>که دیدم به تاریخهای کهن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نظامی که کرد آن جریده نگاه</p></div>
<div class="m2"><p>در آشتی زد میان دو شاه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دگر گونه خواندم من این راز را</p></div>
<div class="m2"><p>دگرگون زدم لابد این ساز را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وگرنه لطافت ندارد بسی</p></div>
<div class="m2"><p>که مر گفته را باز گوید کسی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به تاریخ شاهان پیشین و حال</p></div>
<div class="m2"><p>چنان خواندم این حرف دیرینه سال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که دولت چو رو در سکندر نهاد</p></div>
<div class="m2"><p>سران را به درگاه او سر نهاد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در آفاق نام ظفر زنده کرد</p></div>
<div class="m2"><p>بزرگان آفاق را بنده کرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو بر بیشتر خسروان چیره گشت</p></div>
<div class="m2"><p>به شاهی و لشکر کشی خیره گشت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>رها کرد بر دیگران راه را</p></div>
<div class="m2"><p>به خاقان چین راند بنگاه را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بر آهنگ چین خوش دل و شاد کام</p></div>
<div class="m2"><p>همی کرد منزل به منزل خرام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به خاقان چین داد ز اورنگ روم</p></div>
<div class="m2"><p>پیامی که پولاد را کرد موم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که بر ما چو کرد ایزد کار ساز</p></div>
<div class="m2"><p>در کارسازی و اقبال باز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>درین دم که بند قبا را به کین</p></div>
<div class="m2"><p>به بستیم بر چین و خاقان چین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اگر سر در آری و فرمان بری</p></div>
<div class="m2"><p>به آزادی از تیغ ما جان بری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>و گر نه بدین هندی آب دار</p></div>
<div class="m2"><p>بر ارم ز ترکان چینی دمار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نپوشیده بشنید و برداشت راه</p></div>
<div class="m2"><p>به خاقان رسانید پیغام شاه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جهاندار خاقان فرخنده بخت</p></div>
<div class="m2"><p>دل آزرده شد زان نمودار سخت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>پس آنگه به آینده داد از ستیز</p></div>
<div class="m2"><p>یکی مشت خاک و یکی تیغ تیز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بدو گفت آنجا براین هر دو چیز</p></div>
<div class="m2"><p>که هست اندرین هر دو رمزی عزیز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بگو آنچه گویی خطا و صواب</p></div>
<div class="m2"><p>منت زین بتر باز گویم جواب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گر آهن هوس داری اینک به دست</p></div>
<div class="m2"><p>وگر گنج و زر بایدت خاک هست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شتابان ز خاقان دو حمال راز</p></div>
<div class="m2"><p>رسیدند پیش سکندر فراز</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نموداری آورده دادند پیش</p></div>
<div class="m2"><p>نمودند راز ره آورد خویش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سکندر بخندید از ان داوری</p></div>
<div class="m2"><p>دران نکته دید از فلک یاوری</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به آینهٔ شاه چین باز گفت</p></div>
<div class="m2"><p>که تدبیر ما گشت با کام جفت</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز خاقان بما کاین دو کالا رسید</p></div>
<div class="m2"><p>نموداری از فتح والا رسید</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو دشمن به ما تیغ خود خود سپرد</p></div>
<div class="m2"><p>کنون کی تواند سر از تیغ برد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دگر آنکه بر ما فرستاد خاک</p></div>
<div class="m2"><p>نشان خود از خاک چین کرد پاک</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گرفتم به فال اینکه بی چشم و کین</p></div>
<div class="m2"><p>زمین را به من داد خاقان چین</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>فرستاده زان پاسخ نغزوار</p></div>
<div class="m2"><p>سرو پای گم کرده بی مغزوار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هراسان به درگاه خاقان شتافت</p></div>
<div class="m2"><p>فرو ریخت پیشش جوابی که یافت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بجوشید خاقان و شد خشمناک</p></div>
<div class="m2"><p>خیال محابا ز دل کرد پاک</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>فرستاد فرمان که بر عزم کار</p></div>
<div class="m2"><p>فراهم شود لشکر از هر دیار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز آب الق تا به دریای چین</p></div>
<div class="m2"><p>چو دریای چین شد ز لشکر زمین</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>فرود آمدند از دو جانب دو شاه</p></div>
<div class="m2"><p>کشیدند تا آسمان بارگاه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو صبح از افق تیغ بیرون کشید</p></div>
<div class="m2"><p>همه دامن چرخ در خون کشید</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>سکندر جهان گرد کشور گشای</p></div>
<div class="m2"><p>به آرایش لشکر آورد رای</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دگر سوی خاقان لشکر شکن</p></div>
<div class="m2"><p>چو کوهی سر افراخت شد تیغ زن</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>هزاهز در آمد به هر دو سپاه</p></div>
<div class="m2"><p>روا رو برآمد به خورشید و ماه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بیابان همه بیشه شیر گشت</p></div>
<div class="m2"><p>جهانی پر از تیر و شمشیر گشت</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز لرز زمین زبر قلب روان</p></div>
<div class="m2"><p>در اندام گاو آرد گشت استخوان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>غبار زمین کله بر ماه بست</p></div>
<div class="m2"><p>نفس را درون گلو راه بست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ز موج سلاح و ز گرد زمین</p></div>
<div class="m2"><p>گلین آسمان شد زمین آهنین</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به دریای آهن جهان گشت غرق</p></div>
<div class="m2"><p>هوا پر ز میغ و زمین پر ز برق</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>وزان سوی خاقان شوریده مغز</p></div>
<div class="m2"><p>جهان گشت پر سوس و برگ بید</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>روان کرد شه تخت جمشید را</p></div>
<div class="m2"><p>به منزل رها کرد خورشید را</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>به جولان گه آمد صف آراسته</p></div>
<div class="m2"><p>به کوشش چو خورشید شد خاسته</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>وزان شوی خاقان شوریده مغز</p></div>
<div class="m2"><p>زنا آمد فتح در پای لغز</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>رسولی فرستاد بر شاه روم</p></div>
<div class="m2"><p>که تنگ آمد از دستت این مرز و بوم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>تو ای تاجور کامدی در نبرد</p></div>
<div class="m2"><p>به مردی کن این داوری نی به مرد</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>به پیکار اگر با منی کینه سنج</p></div>
<div class="m2"><p>سپه را چه بیهوده داری به رنج؟</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چو کاری میان من و تست بس</p></div>
<div class="m2"><p>چه جوئیم فریاد فریاد رس</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بیا تا به هم دست بیرون کنیم</p></div>
<div class="m2"><p>زره در خوی و تیغ در خون کنیم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>زما هر دو تن هر که ماند به جای</p></div>
<div class="m2"><p>بود بر سر روم و چین کدخدای</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چو نزد سکندر رسید این پیام</p></div>
<div class="m2"><p>در ان کام جویی دلش یافت کام</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>سوی حرب گه تاخت با ساز جنگ</p></div>
<div class="m2"><p>بر انسان که نخجیر جوید پلنگ</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>میانجی به خاقان خیر گفت باز</p></div>
<div class="m2"><p>که اینک برزم آمد ان رزم ساز</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>روان شد به جولانگری ساخته</p></div>
<div class="m2"><p>ز رخت بقا خانه پرداخته</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>چو پیلان جنگی دران لعیگاه</p></div>
<div class="m2"><p>در آمد به شطرنج بازی دو شاه</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>نخست از کمان ناوک انداختند</p></div>
<div class="m2"><p>ز یکدیگر آماجگه ساختند</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چو بودند هر دو هنرمند و چست</p></div>
<div class="m2"><p>نیامد بر آماج تیری درست</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ز ناوک سوی نیزه بردند دست</p></div>
<div class="m2"><p>زهر دو در ان نیز مویی نخست</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>به شمشیر گشتند دست آزمای</p></div>
<div class="m2"><p>دران هم نشد قالبی زخم سای</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>چو کردند چندان که بود از هنر</p></div>
<div class="m2"><p>نگشتند فیروز بر یکدگر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>به نیروی بازوی پولاد لخت</p></div>
<div class="m2"><p>دوال کمرها گرفتند سخت</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>چو پیلان که خرطوم در هم زنند</p></div>
<div class="m2"><p>به پیچند و خرطوم را خم زنند</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>به تاب و توان در هم آمیختند</p></div>
<div class="m2"><p>قیامت ز یکدیگر انگیختند</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>هم آخر قوی دست شد شاه روم</p></div>
<div class="m2"><p>ز جا در ربودش چو نخلی ز موم</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>فرس تاخت باز و برافراخته</p></div>
<div class="m2"><p>ز بازو کسی را ستون ساخته</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>خروش از صف رومیان شد به ابر</p></div>
<div class="m2"><p>ز ترکان چینی تهی گشت صبر</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>در افتاد در قلب خاقان شکست</p></div>
<div class="m2"><p>برآورد رومی به تاراج دست</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>سکندر بفرمود تا بی‌دریغ</p></div>
<div class="m2"><p>سلاح افگنان را نرانند تیغ</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>به پیمان شه زینهاری کنند</p></div>
<div class="m2"><p>بران زینهار استواری کنند</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>و گر کس به مردی برابر شود</p></div>
<div class="m2"><p>نکوشند کز تیغ بی سر شود</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>به نیرنگ و هنجار اسیرش کنند</p></div>
<div class="m2"><p>چو در تابد آماج تیرش کنند</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>کسی کو به گیتی بود هوشمند</p></div>
<div class="m2"><p>نیابد ز آسیب گیتی گزند</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>به اندیشه بنیاد کاری کنند</p></div>
<div class="m2"><p>کزان خویش را در حصاری کند</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>بزرگی کسی را دهد دستگاه</p></div>
<div class="m2"><p>که دارد پناهنده‌ای را پناه</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>نه زان ماکیان کمتری در شمار</p></div>
<div class="m2"><p>که بر چوزگان سازدار</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>بزرگان که کهتر نوازی شد</p></div>
<div class="m2"><p>نه رسم بزرگی به بازی کنند</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>سر مرد بهر سری کردن است</p></div>
<div class="m2"><p>چو نبود سری بار بر کردن است</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>ولیکن سران را توان کرد فرد</p></div>
<div class="m2"><p>که با زیردستان بود پای مرد</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>کسی بر سر خلق زیبد امیر</p></div>
<div class="m2"><p>که افتادگان را بود دستگیر</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>کشایندهٔ نافهٔ این سواد</p></div>
<div class="m2"><p>سر نافهٔ چین بدینسان کشاد</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>که چون فرخ اسکندر سرفراز</p></div>
<div class="m2"><p>به فیروزی از ملک چین گشت باز</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>بهین روزی از موسم نوبهار</p></div>
<div class="m2"><p>که گیتی شد از خرمی چون نگار</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>هم از اول بامداد آفتاب</p></div>
<div class="m2"><p>بفرخنده طالع در آمد ز خواب</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>ز باد بهاری هوا مشک بوی</p></div>
<div class="m2"><p>عروس جهان ز آب گل شسته روی</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>شده جلوه‌گر نازنینان باغ</p></div>
<div class="m2"><p>رخ آراسته هر یکی چون چراغ</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>بساط گل از سبزه گلشن شده</p></div>
<div class="m2"><p>چراغ گل از باد روشن شده</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>به لاله ز فردوس جام آمده</p></div>
<div class="m2"><p>ز رضوان به گلبن سلام آمده</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>شده مشکبو غنچه در زیر پوست</p></div>
<div class="m2"><p>چو تعویذ مشکین به بازوی دوست</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>بنفشه سر زلف را خم زده</p></div>
<div class="m2"><p>گره در دل غنچه محکم زده</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>ز بس تری اندام زیبای گل</p></div>
<div class="m2"><p>شده پاره پاره سرا پای گل</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>شده سرخ گل مفرش بوستان</p></div>
<div class="m2"><p>به صحرا برون آمده دوستان</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>هوا بر سر سبزه می‌ریخت سیم</p></div>
<div class="m2"><p>مراغه همی کرد بر گل نسیم</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>بهر شاخ مرغ ارغنوان ساخته</p></div>
<div class="m2"><p>بهر نغمه گل بن سر انداخته</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>ازان نغمه کو غارت هوش کرد</p></div>
<div class="m2"><p>مغنی تر نم فراموش کرد</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>غزل خوانی بلبل صبح خیز</p></div>
<div class="m2"><p>تمنای میخوارگان کرد تیز</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>ز آواز دراج و رقص تذرو</p></div>
<div class="m2"><p>سبک گشت در خاستن پای سرو</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>ز نالیدن قمری خوش نوا</p></div>
<div class="m2"><p>کبوتر معلق زنان در هوا</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>بهر سو گل و غنچه نوشخند</p></div>
<div class="m2"><p>ملک در میان همچو سرو بلند</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>به بزم ار چه دلبر ز حد بیش بود</p></div>
<div class="m2"><p>دلش همبران دلبر خویش بود</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>نشانده صنم را به پهلوی خود</p></div>
<div class="m2"><p>چو آیینه نزدیک زانوی خود</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>بهر دورش آن ساقی نیم خواب</p></div>
<div class="m2"><p>ز لب نقل می داد و از کف شراب</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>به عشرت نشسته دو سرو جوان</p></div>
<div class="m2"><p>پیاپی شده دوستگانی روان</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>ملک عاشق رویش از جان و تن</p></div>
<div class="m2"><p>برانسان که او عاشق خویشتن</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>گهی گل همی ریخت اندر کنار</p></div>
<div class="m2"><p>گهی دست می سود بر سیب و نار</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>چو می‌رغبت عاشقان تازه کرد</p></div>
<div class="m2"><p>شکیب از میان عزم دروازه کرد</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>چنان باده در نازنین راه یافت</p></div>
<div class="m2"><p>کزو شرم را دست کوتاه یافت</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>هوای دلش قفل عصمت شکست</p></div>
<div class="m2"><p>عنان تکلف ربودش ز دست</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>به افسونگری چنگ را بر گرفت</p></div>
<div class="m2"><p>فسونش به دیو و پری در گرفت</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>ازان نغمه کاندر پری خانه شد</p></div>
<div class="m2"><p>سلیمان پری وار دیوانه شد</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>بر ایین خوبان ز شوخی و ناز</p></div>
<div class="m2"><p>سرودی برآورد عاشق خواز</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>برو تازه بود آن گل مشک بوی</p></div>
<div class="m2"><p>که بویش جهان را کند تازه روی</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>گه از رنگ تر عشوه بازی کند</p></div>
<div class="m2"><p>گه از بوی خوش دل نوازی کند</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>چو بشگفت گل خوش بود بوستان</p></div>
<div class="m2"><p>ولیکن به همراهی دوستان</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>چو سازنده ارغنون توی نوش</p></div>
<div class="m2"><p>بدین رهزنی کرد با تاراج هوش</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>ز سرها خرد رفت و سرمست رفت</p></div>
<div class="m2"><p>ملک را عنان دل از دست رفت</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>به خوبان دیگر اشارت نمود</p></div>
<div class="m2"><p>که هر یک به سویی چمیدند زو</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>نهی گشت خرگاه شاهنشهی</p></div>
<div class="m2"><p>ولیکن شه از خویشتن شد تهی</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>حکیم الهی طلب کرد شاه</p></div>
<div class="m2"><p>که بستند تا عقد خورشید و ماه</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>ملک سر خوش و نازنین نیم مست</p></div>
<div class="m2"><p>دو عاشق به یکدیگر آورده دست</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>رسانیده این خضر صافی صفات</p></div>
<div class="m2"><p>به اسکندر تشنه آب حیات</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>چو نوشیدن از دست جانان بود</p></div>
<div class="m2"><p>هر آبی که هست آب حیوان بود</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>گهی نار با سیب پیوسته بود</p></div>
<div class="m2"><p>گه از ناردان سیب را خسته بود</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>به گنجینه آرزو دست برد</p></div>
<div class="m2"><p>کلید خزینه به خازن سپرد</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>بکان گهر شاخ مرجان نشاند</p></div>
<div class="m2"><p>گهر سفت و یاقوت بیرون فشاند</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>چو خورشید را چشم در خواب رفت</p></div>
<div class="m2"><p>پیاله فتاد و می ناب رفت</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>به بر بط نی زهرهٔ پرده ساز</p></div>
<div class="m2"><p>شد از پرده تار بر بط نواز</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>به پرده درون خسرو پرده پوش</p></div>
<div class="m2"><p>به خاتون پرده نشین داد هوش</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>چو مرغی خود از دام نجهد مدام</p></div>
<div class="m2"><p>دگر مرغ را کی رهاند ز دام</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>طبیبی که پیوسته بیمار ماند</p></div>
<div class="m2"><p>نشاید به بالین بیمار خواند</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>کسی کو ندانست راز جهان</p></div>
<div class="m2"><p>جهان آفرین را چه داند نهان</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>ادب را نگهدار کز هیچ رای</p></div>
<div class="m2"><p>خدا را نداند کسی جز خدای</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>شناسنده حرف دانند گی</p></div>
<div class="m2"><p>چنین کرد ازین تخته خوانندگی</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>که چون بیرون آمد فلاتون ز آب</p></div>
<div class="m2"><p>تن خاکی از موج توفان خراب</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>نبودش سر یاری مردمان</p></div>
<div class="m2"><p>روان شد سوی کوه چون بی گمان</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>زهر بوم برداشت آهنگ خویش</p></div>
<div class="m2"><p>چو سیمرغ بنشست با سنگ خویش</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>دهان را ز اشام و خور بند کرد</p></div>
<div class="m2"><p>به شاخ گیا سینه خرسند کرد</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>نیایش‌گر پرده راز گشت</p></div>
<div class="m2"><p>به راز اندران پرده دم ساز گشت</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>چنان گشت کوشنده در بندگی</p></div>
<div class="m2"><p>که شد سرفراز از سرافکندگی</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>ز شب زنده داری دلش زنده شد</p></div>
<div class="m2"><p>چراغش خورشید رخشنده شد</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>برآمد میان همه خاص و عام</p></div>
<div class="m2"><p>فلاتون حکیم الهیش نام</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>ز نامش که در شهر و کشور رسید</p></div>
<div class="m2"><p>حکایت به گوش سکندر رسید</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>هوس داشت اسکندر کاردان</p></div>
<div class="m2"><p>به دیدار آن مرد بسیار دان</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>فرستاد پنهان بلیناس را</p></div>
<div class="m2"><p>که از کان برون آرد الماس را</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>به فرمان فرمانروای جهان</p></div>
<div class="m2"><p>روان گشت دانا چو کار آگهان</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>ز اندیشه دادش فلاتون جواب</p></div>
<div class="m2"><p>که ذره ندارد سر آفتاب</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>من اینجا که گشتم ز دل توشه گیر</p></div>
<div class="m2"><p>ز غوغای عالم شدم گوشه‌گیر</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>فرستاده کوشش فراوان نمود</p></div>
<div class="m2"><p>نیوشند را رای رفتن نبود</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>بلیناس چون دید کان هوشمند</p></div>
<div class="m2"><p>کند وقت خود را بخود ارجمند</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>که آمد چو بیرون فلاتون ز آب؟</p></div>
<div class="m2"><p>بشر باز شد در حین خاک رفت</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>شنیده سخن یک به یک باز گفت</p></div>
<div class="m2"><p>چو شه رغبت دیدنش پیش داشت</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>دل اندر پی رغبت خویش داشت</p></div>
<div class="m2"><p>سبک بارگی جست و بر داشت راه</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>به برج عطارد روان شد چو ماه</p></div>
<div class="m2"><p>نه بود از بزرگان به دنبال کس</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>جز از هوشمندان تنی چند و بس</p></div>
<div class="m2"><p>سر کوهکن سوی کهسار کرد</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>به کوه آمد و سر سوی غار کرد</p></div>
<div class="m2"><p>چو در غار شد کرد مرکب رها</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>به غار اندرون رفت چون اژدها</p></div>
<div class="m2"><p>نگه کرد در کنج آن تنگ نای</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>فرشته وشی دید مردم نمای</p></div>
<div class="m2"><p>لگیمی در آورده در گرد دوش</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>خزیده چو روباه پشمینه پوش</p></div>
<div class="m2"><p>کسی گنجش اندر سفالینه خم</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>کلید زبان در دهان کرده گم</p></div>
<div class="m2"><p>مبرا شده دل ز غم خوردنش</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>رگ اندر تنش رو نما از صفا</p></div>
<div class="m2"><p>نماینده چون رسته در کهربا</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>ز تاب درون در افشان او</p></div>
<div class="m2"><p>حکایت کنان روی رخشان او</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>چو سیمای شه دید برخاست زود</p></div>
<div class="m2"><p>به رسم بزرگان تواضع نمود</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>پس آنگه گفت از دل عذرخواه</p></div>
<div class="m2"><p>دعای سزاوار تعظیم شاه</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>بپرسید کاقبال شاه جهان</p></div>
<div class="m2"><p>برین سو چرا رنجه شد ناگهان</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>جهاندار فرمود کز دیر باز</p></div>
<div class="m2"><p>به دیدار تو بود ما را نیاز</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>کنونم که آن آرزو دست دادش</p></div>
<div class="m2"><p>سر گنج پنهان بباید گشاد</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>چو دانست دانای دریا قیاس</p></div>
<div class="m2"><p>که آمد خریدار گوهر شناس</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>به همان نوزیش بگرفت دست</p></div>
<div class="m2"><p>نشاندش به تعظیم و خود هم نشست</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>سخن راز هر پرده ساز کرد</p></div>
<div class="m2"><p>ز راز نهان پرده را باز کرد</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>بهر باز پرسی که شه می‌نمود</p></div>
<div class="m2"><p>حکیمش به اندیشه ره می‌نمود</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>نخستش بپرسید کای گنج راز</p></div>
<div class="m2"><p>ازین گوشه گیری چه داری نیاز</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>برون آی ازین غار چون اژدها</p></div>
<div class="m2"><p>وگر غار گنج است هم کن رها</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>به دستوری خویش دستت دهم</p></div>
<div class="m2"><p>به همدستی خود نشستت دهم</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>ارسطو که جز رای والاش نیست</p></div>
<div class="m2"><p>تو همتاش باشی که همتاش نیست</p></div></div>
<div class="b" id="bn209"><div class="m1"><p>فلاتون چو بشنید گفتار شاه</p></div>
<div class="m2"><p>فرو شد به کار خود از کار شاه</p></div></div>
<div class="b" id="bn210"><div class="m1"><p>برون داد پاسخ به شرمندگی</p></div>
<div class="m2"><p>که ای تو از آفاق را زندگی</p></div></div>
<div class="b" id="bn211"><div class="m1"><p>نماند آن شکوفه به گلزار من</p></div>
<div class="m2"><p>که آید بدان بو خریدار من</p></div></div>
<div class="b" id="bn212"><div class="m1"><p>چه جنبانی آن خل بن را به زور</p></div>
<div class="m2"><p>که شد خار او تیر و خرماش گور</p></div></div>
<div class="b" id="bn213"><div class="m1"><p>چو شاخ تهی را کنی سنگسار</p></div>
<div class="m2"><p>ز بالا همان سنگ بارد نه بار</p></div></div>
<div class="b" id="bn214"><div class="m1"><p>نگویم به دستوریم شاد کن</p></div>
<div class="m2"><p>که دستوریم بخش و آزاد کن</p></div></div>