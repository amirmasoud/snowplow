---
title: >-
    بخش  ۱۰ - ۹ ابوالقاسم علی الکُرّکانی، رضی اللّه عنه
---
# بخش  ۱۰ - ۹ ابوالقاسم علی الکُرّکانی، رضی اللّه عنه

<div class="n" id="bn1"><p>و منهم: قطب زمانه و در زمانهٔ خود یگانه، ابوالقاسم علی الکرّکانی، رضی اللّه عنه و ارضاه</p></div>
<div class="n" id="bn2"><p>اندر وقت خود بی نظیر است و اندر زمانه بی بدیل. وی را ابتدا سخت نیکو بوده است و اسفاری سخت به شرط معاملت و اندروقت وی روی دل همه اهل درگاه بدوست و اعتماد جمله طالبان بر او و اندر کشف وقایع مریدان آیتی است ظاهر، و به فنون علم عالم است و مریدان وی هر یکی عالمی را زینتی‌اند و از پس او مر او را خلفی نیکو ماند ان شاء اللّه که مقتدای قوم باشد. و آن لسان الوقت است، ابوعلی الفضل بن محمد الفارمدی ابقاه اللّه که نصیب خود اندر حق آن بزرگ فرو گذاشته باشد و از کل اعراض کرده و حق تعالی و تقدس مر او را به برکات خود زبان حال آن سید گردانیده.</p></div>
<div class="n" id="bn3"><p>روزی من پیش خدمت شیخ نشسته بودم و احوال و نمودهای خود می‌شمردم؛ به حکم آن که روزگار خود را بر او سره کنم؛ که ناقد وقت است و وی رضی اللّه عنه آن به حرمت می‌شنید و مرا نخوت کودکی وآتش جوانی بر گفتار آن حریص می‌کرد و خاطری صورت می‌گشت که: «مگر این پیر را در ابتدا بر این کوی گذری نبوده است که چندین خضوع می‌کند اندر حق من و نیاز می‌نماید؟» اندر حال، وی این در باطن من بدید، گفت: «دوست پدر، این خضوع من نه مر تو را و یا حال تراست. کَیْ محول احوال در محل محال آید؟ که این خضوع، من محول احوال را می‌کنم و این عام باشد مر همه طالبان را نه خاص مر تو را.» چون این بشنیدم از دست بیفتادم. وی آن اندر من بدید، گفت: «ای پسر، آدمی را با این طریقت، نسبت بیش از این نیست که چون وی را به طریقت بازبندند پندار یافت آن بگیردش، و چون از آن معزول کنندش به عبارت پندارش برسد. پس نفی و اثبات و فقد و وجود وی هر دو پندار باشد و آدمی هرگز از بند پندار نرهد. وی را باید که درگاه بندگی گیرد و جملهٔ نسبت‌ها از خود دفع کند، به‌جز نسبت مردمی و فرمانبرداری.»</p></div>
<div class="n" id="bn4"><p>و از بعد آن مرا با وی اسرار بسیار بود، و اگر به اظهار آیات وی مشغول شوم از مقصود بازمانم. واللّه اعلم.</p></div>
<div class="b2" id="bn5"><p> </p></div>