---
title: >-
    بخش  ۳ - ۲ ابوعبداللّه الحسین بن علی بن ابی طالب، رضی اللّه عنهما
---
# بخش  ۳ - ۲ ابوعبداللّه الحسین بن علی بن ابی طالب، رضی اللّه عنهما

<div class="n" id="bn1"><p>و منهم: شمع آل محمد، و از جملهٔ علایق مجرد سید زمانه خود، ابوعبداللّه الحسین بن علی بن ابی طالب، رضی اللّه عنهما</p></div>
<div class="n" id="bn2"><p>از محققان اولیا بود و قبلهٔ اهل بلا و قتیل دشت کربلا و اهل این قصه بر درستی حال وی متفق‌اند که تا حق ظاهر بود مر حق را متابع بود چون حق مفقود شد شمشیر بر کشید و تا جان عزیز فدای شهادت خدای عزّ و جلّ نکرد نیارامید.</p></div>
<div class="n" id="bn3"><p>و رسول را علیه السّلام اندر وی نشانهایی بود که او بدان مخصوص بود؛ چنان‌که عمر بن الخطاب رضی اللّه عنه روایت کند که: روزی به نزدیک پیغمبر علیه السّلام اندر آمدم. وی را دیدم حسین را بر پشت خود افکنده بود و رشته‌ای اندر دهان خود گرفته و سر رشته به دست حسین داده؛ تا حسین می‌رفت و وی علیه السّلام از پس حسین به زانوها می‌رفت. من چون آن بدیدم گفتم: «نِعْمَ الجَمَلُ جَمَلُک یا باعبدِاللّهِ.» پیغمبر گفت، علیه السّلام: «نِعمَ الرّاکبُ هو یا عمرُ.»</p></div>
<div class="n" id="bn4"><p>و وی را کلام لطیف است اندر طریقت حق، و رموز بسیار و معاملت نیکو. از وی روایت آرند گفت: «أشْفَقُ الإخوانِ عَلَیک دینُکَ.»</p></div>
<div class="n" id="bn5"><p>شفیق‌ترین برادران تو بر تو دین توست؛ از آن‌چه نجات مرد اندر متابعت دین بود و هلاکش اندر مخالفت آن. پس مرد خردمند آن بود که به فرمان مشفقان بود و شفقت آن بر خود بداند و جز بر متابعت آن نرود و برادر آن بود که نصیحت نماید و در شفقت نبندد.</p></div>
<div class="n" id="bn6"><p>و اندر حکایات یافتم که روزی مردی به نزدیک وی آمد و گفت: «یا فرزند رسول خدای عزّ و جلّ مردی درویشم و اطفال دارم مرا از تو قوت امشب می‌باید.» حسین وی را گفت: «بنشین که ما را رزقی در راه است تا بیارند.» بسی برنیامد که پنج صُرّه بیاوردند از نزد معاویه اندر هر صرّه‌ای هزار دینار و گفتند که: «معاویه از تو عذر می‌خواهد و می‌گوید: این قدر در وجه کهتران صرف فرماید کرد تا بر اثر این، تیمار نیکوتر داشته آید.» حسین رضی اللّه عنه اشارت کرد که: «بدان درویش دهید.» آن پنج صره بدو دادند و از وی عذرها خواست که: «بس دیر ماندی، و این بس بی خطر عطایی بود که یافتی و اگر ما دانستیمی که این مقدار است تو را انتظار ندادیمی. ما را معذور دار؛ که ما از اهل بلاییم و از همه راحت دنیا باز مانده‌ایم و مرادهای دنیای خود گم کرده‌ایم و زندگانی به مراد دیگران می‌باید کرد.»</p></div>
<div class="n" id="bn7"><p>و مناقب وی اشهر آن است که بر هیچ کس از امت پوشیده باشد. واللّه اعلم.</p></div>
<div class="b2" id="bn8"><p> </p></div>