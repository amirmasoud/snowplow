---
title: >-
    بخش ۱۳ - خواب دیدن ضحاک و تعبیر جستن از موبدان
---
# بخش ۱۳ - خواب دیدن ضحاک و تعبیر جستن از موبدان

<div class="b" id="bn1"><div class="m1"><p>در ایوان شاهی شب تیره باز</p></div>
<div class="m2"><p>به خواب اندرون بود با ارنواز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان دید کز شاخ شاهنشهان</p></div>
<div class="m2"><p>سه جنگی پدید آمد از ناگهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو مهتر یکی کهتر اندر میان</p></div>
<div class="m2"><p>به بالای سرو و به فر کیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمر بستن و رفتن شاهوار</p></div>
<div class="m2"><p>به دست اندرون گرزه گاوسار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دمان پیش ضحاک رفتی به جنگ</p></div>
<div class="m2"><p>زدی بر سرش گرزه گاورنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکایک همین کرد کهتر به سال</p></div>
<div class="m2"><p>ز سرتا به پایش کشیدی دوال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدان ره دو دستش ببستی چو سنگ</p></div>
<div class="m2"><p>نهادی به گردنش بر پالهنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی تاختی تا دماوند کوه</p></div>
<div class="m2"><p>کشان و دوان از پی آن گروه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی چاه بد اندر آن کوه پست</p></div>
<div class="m2"><p>به چاه اندرون بر دو دستش ببست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بپیچید ضحاک بیدادگر</p></div>
<div class="m2"><p>بدریدش از هول گفتی جگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی بانگ بر زد به خواب اندرون</p></div>
<div class="m2"><p>که سوزان شد آن خانه صد ستون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بجستند خورشیدرویان ز جای</p></div>
<div class="m2"><p>از آن غلغل نامور کدخدای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنین گفت ضحاک را ارنواز</p></div>
<div class="m2"><p>که شاها چه بودت نگوئی به راز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که خفته به آرام در خان خویش</p></div>
<div class="m2"><p>بدین سان بترسیدی از جان خویش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زمین هفت کشور به فرمان تست</p></div>
<div class="m2"><p>دد و دیو مردم نگهبان تست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به خورشید رویان سپه‌دار گفت</p></div>
<div class="m2"><p>که این خواب را باز باید نهفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر ایدون که این داستان بشنوید</p></div>
<div class="m2"><p>شودتان دل از جان من ناامید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به شاه گرانمایه گفت ارنواز</p></div>
<div class="m2"><p>که بر ما بباید گشادنت راز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>توانیم کردن مگر چاره‌ای</p></div>
<div class="m2"><p>که بی‌چاره‌ای نیست پتیاره‌ای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سپه‌بد گشاد آن نهان از نهفت</p></div>
<div class="m2"><p>همه خواب یک یک به ایشان بگفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین گفت با نامور خوبروی</p></div>
<div class="m2"><p>که مگذار این را ره چاره‌جوی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نگین زمانه سر تخت تست</p></div>
<div class="m2"><p>جهان روشن از نامور بخت تست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو داری جهان زیر انگشتری</p></div>
<div class="m2"><p>دد و مردم و مرغ و دیو و پری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز هر کشوری گرد کن مهتران</p></div>
<div class="m2"><p>ز اخترشناسان و افسون‌گران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سخن سر به سر مهتران را بگوی</p></div>
<div class="m2"><p>پژوهش کن و رازها بازجوی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نگه کن که هوش تو بر دست کیست</p></div>
<div class="m2"><p>ز مردم نژادست یا خود پریست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو دانستیش چاره‌کن آن زمان</p></div>
<div class="m2"><p>به خیره مترس از بد بدگمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شه پرمنش را خوش آمد سخن</p></div>
<div class="m2"><p>که آن سرو پروین رخ افگند بن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جهان از شب تیره چو پر زاغ</p></div>
<div class="m2"><p>همان‌گه سر از کوه بر زد چراغ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو گفتی که بر گنبد لاجورد</p></div>
<div class="m2"><p>بگسترد خورشید یاقوت زرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سپهبد هر آنجا که بد مؤبدی</p></div>
<div class="m2"><p>سخن‌دان و بیدار دل بخردی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز کشور به نزدیک خویش آورید</p></div>
<div class="m2"><p>بگفت آن جگر خسته خوابی که دید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بخواند و به یک جایشان گرد کرد</p></div>
<div class="m2"><p>وزیشان همی جست درمان درد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بگفتا مرا زود آگه کنید</p></div>
<div class="m2"><p>روان را سوی روشنی ره کنید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نهانی سخن کردشان خواستار</p></div>
<div class="m2"><p>ز نیک و بد و گردش روزگار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که بر من زمانه کی آید به سر</p></div>
<div class="m2"><p>کرا باشد این تاج و تخت و کمر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر این راز بر ما بباید گشاد</p></div>
<div class="m2"><p>دگر سر به خواری بباید نهاد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>لب مؤبدان خشک و رخسارتر</p></div>
<div class="m2"><p>زبان پر زگفتار با یک‌دگر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که گر بودنی بازگوئیم راست</p></div>
<div class="m2"><p>شود سر سبکسار و تن بی‌بهاست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>وگر نشنود بودنی‌‌ها درست</p></div>
<div class="m2"><p>بباید هم‌اکنون ز جان دست شست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سه روز اندر آن کار شد روزگار</p></div>
<div class="m2"><p>سخن کس نیارست کرد آشکار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به روز چهارم برآشفت شاه</p></div>
<div class="m2"><p>بر آن مؤبدان نماینده راه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که گر زنده‌تان دار باید به سود</p></div>
<div class="m2"><p>وگرنه نهانی بباید نمود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همه مؤبدان سرفگنده نگون</p></div>
<div class="m2"><p>به دو نیمه دل دیدگان پر زخون</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از آن نامداران بسیار هوش</p></div>
<div class="m2"><p>یکی بود بینادل و راست گوش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خردمند و بیدار و زیرک به نام</p></div>
<div class="m2"><p>از آن مؤبدان او زدی پیش‌گام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دلش تنگ‌تر گشت و بی‌باک شد</p></div>
<div class="m2"><p>گشاده‌زبان نزد ضحاک شد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بدو گفت پردخته کن سر زباد</p></div>
<div class="m2"><p>که جز مرگ را کس ز مادر نزاد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جهاندار پیش از تو بسیار بود</p></div>
<div class="m2"><p>که تخت مهی را سزاوار بود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>فراوان غم و شادمانی شمرد</p></div>
<div class="m2"><p>چو روز درازش سرآمد بمرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگر باره آهنینی به پای</p></div>
<div class="m2"><p>سپهرت بساید نمانی به جای</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کسی را بود زین سپس تخت تو</p></div>
<div class="m2"><p>به خاک اندر آرد سر بخت تو</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کجا نام او آفریدون بود</p></div>
<div class="m2"><p>زمین را سپهری همایون بود</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هنوز آن سپهبد ز مادر نزاد</p></div>
<div class="m2"><p>نیامد گه ترسش و سرد باد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو او زاید از مادر پرهنر</p></div>
<div class="m2"><p>بسان درختی بود بارور</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به مردی رسد برکشد سر به ماه</p></div>
<div class="m2"><p>کمر جوید و تاج و تخت و کلاه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به بالا شود چون یکی سرو برز</p></div>
<div class="m2"><p>به گردن برآرد ز پولاد گرز</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>زند بر سرت گرزه گاوروی</p></div>
<div class="m2"><p>ببنددت آرد از ایوان به کوی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بدو گفت ضحاک ناپاک دین</p></div>
<div class="m2"><p>چرا بنددم چیست از منش کین</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دلاور بدو گفت اگر بخردی</p></div>
<div class="m2"><p>کسی بی‌بهانه نسازد بدی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بیاید به دست تو هوش پدرش</p></div>
<div class="m2"><p>از آن درد گردد پر از کینه سرش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>یکی گاو پرمایه خواهد بدن</p></div>
<div class="m2"><p>جهان جوی را دایه خواهد بدن</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تبه گردد آن هم به دست تو بر</p></div>
<div class="m2"><p>بدین کین کشد گرزه گاوسر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو ضحاک بشنید بگشاد گوش</p></div>
<div class="m2"><p>ز تخت اندر افتاد و زو رفت هوش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گرانمایه از تخت بلند</p></div>
<div class="m2"><p>بتابید رویش ز بیم گزند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو آمد دل تاجور باز جای</p></div>
<div class="m2"><p>به تخت کئی اندر آورد پای</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نشان فریدون به گرد جهان</p></div>
<div class="m2"><p>همی بازجست از کیان و مهان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نه آرام بودش نه خواب و نه خورد</p></div>
<div class="m2"><p>شده روز روشن برو لاجورد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بر آمد برین روزگاری دراز</p></div>
<div class="m2"><p>که شد اژدهاپی به تنگی فراز</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>خجسته فریدون ز مادر بزاد</p></div>
<div class="m2"><p>جهان را یکی دیگر آمد نهاد</p></div></div>