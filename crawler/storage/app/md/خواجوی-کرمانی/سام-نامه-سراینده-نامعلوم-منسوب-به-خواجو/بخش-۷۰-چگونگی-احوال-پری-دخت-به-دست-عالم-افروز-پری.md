---
title: >-
    بخش ۷۰ - چگونگی احوال پری‌دخت به دست عالم‌افروز پری
---
# بخش ۷۰ - چگونگی احوال پری‌دخت به دست عالم‌افروز پری

<div class="b" id="bn1"><div class="m1"><p>پریزاد چون کرد بر وی کمین</p></div>
<div class="m2"><p>ببردش به کین بر سپهر برین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدو گفت از سام برتاب روی</p></div>
<div class="m2"><p>دگر وصل او را مکن آرزوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پری‌دخت مهوش بپیچید سر</p></div>
<div class="m2"><p>ز گفتار آن جادوی حیله‌گر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی گفت تا جان به تن هست رام</p></div>
<div class="m2"><p>دلم هست در بند دیدار سام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برآشفت ازو عالم افروز باز</p></div>
<div class="m2"><p>همی جنگ و کین گستری کرد باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سحر و افسونگری برگشاد</p></div>
<div class="m2"><p>پری‌دخت را جا به صندوق داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیالود صندوق را او به قیر</p></div>
<div class="m2"><p>دل ماه‌سیما شد از غم اسیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وز آن پس به گردون برافراختش</p></div>
<div class="m2"><p>به دریای چین اندر انداختش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخ آورد از اینجا به نزدیک سام</p></div>
<div class="m2"><p>بدان تا مر او را درآرد به دام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سراینده دهقان با رای و فر</p></div>
<div class="m2"><p>چنین داد از دیوزاده خبر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که چون ره‌سپر شد به خاورزمین</p></div>
<div class="m2"><p>همی خواست کاید سوی شهر چین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شبی راه گم کرد و ره را نیافت</p></div>
<div class="m2"><p>سراسیمه بر سوی خلخ شتافت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سه روز و سه شب رفت بر روی دشت</p></div>
<div class="m2"><p>چهارم بر آسیائی گذشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز بی‌زادی ره بدش جان نژند</p></div>
<div class="m2"><p>برآسود لختی بجا هوشمند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخوابید بر روی صحرا دلیر</p></div>
<div class="m2"><p>چو دو دیده‌اش گشت از خواب سیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برآورد از خواب سر شیر نر</p></div>
<div class="m2"><p>سوی آسیا شد روان ره‌سپر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>طلب کرد از آسیابان خورش</p></div>
<div class="m2"><p>بدان تا روان را دهد پرورش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ازو آسیابان بترسید سخت</p></div>
<div class="m2"><p>بلرزید برسان برگ درخت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شد از دیوزاده دلش پر ز بیم</p></div>
<div class="m2"><p>روانش ز اندوه شد بر دو نیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روان هر چه بودش نهان خوردنی</p></div>
<div class="m2"><p>بیاورد با ساز گستردنی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر آن چیز کو داشت مرد دلیر</p></div>
<div class="m2"><p>سراسر بخورد و نگردید سیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو آن دید زو آسیابان غریو</p></div>
<div class="m2"><p>همی گفت این است از تخم دیو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بترسید و گردید ازو در نهان</p></div>
<div class="m2"><p>روان گشت و می‌جست از وی امان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو فرهنگ دید اندر آن مرحله</p></div>
<div class="m2"><p>زناگه بیامد یکی قافله</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یکی کاروان بود آراسته</p></div>
<div class="m2"><p>فراوان به همراهشان خواسته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خروشان ز ره همچو دود آمدند</p></div>
<div class="m2"><p>برآورد یکسر فرود آمدند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سوی کاروان رفت آن تیزکام</p></div>
<div class="m2"><p>به چربی بپرسید احوال سام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رسیدند از دی همه کاروان</p></div>
<div class="m2"><p>شتر ماند بر جا و شد ساربان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مه کاروان بود مرد دلیر</p></div>
<div class="m2"><p>بیامد به نزدیک آن مرد چیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فراوان بپرسید و بنواختش</p></div>
<div class="m2"><p>بر خویش نزدیک بنشاختش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بسی مرحبا کرد و پرسش گرفت</p></div>
<div class="m2"><p>همی بد ز یال و برش در شگفت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برو دیوزاده چو گردید رام</p></div>
<div class="m2"><p>پژوهش گرفت از گرانمایه سام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دگر گفت با او که ای نیک‌مرد</p></div>
<div class="m2"><p>چشیده به گیتی بسی گرم و سرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مرا بازگو کز کجا می‌رسی</p></div>
<div class="m2"><p>ز چین یا ز راه ختا می‌روی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنین داد پاسخ که از کاشغر</p></div>
<div class="m2"><p>نخستین شدم سوی چین ره‌سپر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به چین ارچه سود و زیان یافتم</p></div>
<div class="m2"><p>ز بهر سفر زود بشتافتم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کنون مرمرا روز فرخ بود</p></div>
<div class="m2"><p>که رویم سوی شهر خلخ بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سه روز دگر چون ببریم راه</p></div>
<div class="m2"><p>به خلخ درآئیم در صبحگاه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کنون آن جوانی که جستی نشان</p></div>
<div class="m2"><p>بر شاه چین است با سرکشان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدو گفت فرهنگ با هوش و رای</p></div>
<div class="m2"><p>که از مرحمت شو مرا رهنمای</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز من دور کن خشم بیداد و کین</p></div>
<div class="m2"><p>رسانم سوی چین ازین سرزمین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خردمند گفتش که ای نیک بهر</p></div>
<div class="m2"><p>چو از دشت سازیم منزل به شهر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کنم یار با تو یکی پیش‌بین</p></div>
<div class="m2"><p>بدان تا رساند تو را سوی چین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بگفت و بیاورد بهرش خورش</p></div>
<div class="m2"><p>بخورد و روان یافت ازو پرورش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز جا جست آن گرد سالار مرد</p></div>
<div class="m2"><p>بدان کاروان را روان گرد کرد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بگفتا ندارید از دل هراس</p></div>
<div class="m2"><p>که او نیست چون اهرمن ناسپاس</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سمند سخن را براند درشت</p></div>
<div class="m2"><p>نمودید از چه برو جمله پشت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نگوید سخن از ره مهر و کین</p></div>
<div class="m2"><p>همی راه جوید سوی شهر چین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو چنگ سخن را چنین ساز کرد</p></div>
<div class="m2"><p>دل کاروان را همه باز کرد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شبانگه از آنجا ببستند بار</p></div>
<div class="m2"><p>براندند تا گشت روز آشکار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سر چشمه‌ساری رسیدند تنگ</p></div>
<div class="m2"><p>فکندند بار شتر بی‌درنگ</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو آسوده گشتند در مرحله</p></div>
<div class="m2"><p>یکی پیک آمد سوی قافله</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز ره چون بر کاروان شد فراز</p></div>
<div class="m2"><p>بپرسید ازو پیر سالار باز</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>که برگو مرا تا کجا می‌روی</p></div>
<div class="m2"><p>چنین تندر در راه چرا می‌روی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بدو گفت در بندر رادیون</p></div>
<div class="m2"><p>یکی کشتی آمد ز دریا برون</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همانا که در روی دریا ژرف</p></div>
<div class="m2"><p>به ایشان نموده جهان این شگرف</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>یکی تنگ صندوق محکم به قیر</p></div>
<div class="m2"><p>بدو در بود لاله‌روئی اسیر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>از آن مردمان در فراز آمدند</p></div>
<div class="m2"><p>ابا یکدگر رزم‌ساز آمدند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همی گفت هر یک که این مهربان</p></div>
<div class="m2"><p>مرا هست در خورد ایوان و خوان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به هم جمله را رای آورد شد</p></div>
<div class="m2"><p>رخ پاژ خواهان ازو زرد شد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بگفتند کاین رای فرخ بود</p></div>
<div class="m2"><p>که او در خور شاه خلخ بود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>طغان شاه جنگ‌آور شیرکین</p></div>
<div class="m2"><p>که او هست فرزند فغفور چین</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>پذیرفت یک تن از آن کاروان</p></div>
<div class="m2"><p>که بود او مه جمله کاروان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>یکی نامه بنوشت نزدیک شاه</p></div>
<div class="m2"><p>درافکند دردم مرا سوی راه</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کنون سوی خلخ شتابم چنین</p></div>
<div class="m2"><p>به نزد طغان شاه فغفور چین</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بدان تا سپارم بدو نامه را</p></div>
<div class="m2"><p>به هم برزنم جنگ و افسانه را</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو بشنید ازو دیوزاده سخن</p></div>
<div class="m2"><p>خروشید و گردید چون اهرمن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز جا جست آن ره‌سپر را دو دست</p></div>
<div class="m2"><p>بینداخت رو پشت بر خاک بست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چنین گفت با مردم کاروان</p></div>
<div class="m2"><p>که آگاه باشید ازو یک زمان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>مبادا که از بند گردد رها</p></div>
<div class="m2"><p>که مانید یکسر به دام بلا</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بباشید چندان ورا پائیدار</p></div>
<div class="m2"><p>که من بازگردم به دریا کنار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چنین گفت دانا که آن نامور</p></div>
<div class="m2"><p>ز خاور چو شد سوی چین ره‌سپر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>رهش گم شد و در بیابان شتافت</p></div>
<div class="m2"><p>ز بس رنج جانش ز سر بر بیافت</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>شبی چون سر او درآمد به خواب</p></div>
<div class="m2"><p>درآمد به گوش دلش این جواب</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>که دری گرانمایه آری به دست</p></div>
<div class="m2"><p>کزان خرمی شاد گردی و مست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>درین ره اگر زحمت آری و رنج</p></div>
<div class="m2"><p>سرانجام برداری از رنج گنج</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>یکی هدیه گردد ز بخت تو رام</p></div>
<div class="m2"><p>مرآن هدیه را می‌بری نزد سام</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>که سام دلاور شود شادمان</p></div>
<div class="m2"><p>ز شادی شود همچو سرو روان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>پس آنگاه آن شیر با گیر و دار</p></div>
<div class="m2"><p>بیامد به نزدیک دریاکنار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بدان تا پژوهش کند راز را</p></div>
<div class="m2"><p>بداند سرانجام و آغاز را</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>سراینده نامه باستان</p></div>
<div class="m2"><p>چنین گفت این نامور داستان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>که آن کشتی از شهر بربر زمین</p></div>
<div class="m2"><p>همی رفت گویا سوی شهر چین</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>وطن داشت در وی یکی کاروان</p></div>
<div class="m2"><p>کزو خیره شد دیده باژوان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>مه کاروان بد مقارن به نام</p></div>
<div class="m2"><p>گرانمایه مردی ابا نام و کام</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>همی رفت صندوق در روی آب</p></div>
<div class="m2"><p>همی زد زمان تا زمان پیچ و تاب</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>گرفتند چون نامور کاروان</p></div>
<div class="m2"><p>سرش برگشادند اندر زمان</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بدو در پری‌دخت را بود جای</p></div>
<div class="m2"><p>چو دیدند ماندند بی‌هوش و رای</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>همی گفت هر یک مرا این سزاست</p></div>
<div class="m2"><p>که مه پیکر و دلبر و دلرباست</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>به قارن سپردش به آرام خویش</p></div>
<div class="m2"><p>سخن راند هر گونه از کم و بیش</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>نخستین بدو گفت برگو کئی</p></div>
<div class="m2"><p>به صندوق جا کرده بهر چه‌ای</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>به گیتی بگو باب و مام تو کیست</p></div>
<div class="m2"><p>نژاد از که داری و نام تو چیست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>پریوش سر درج گوهر گشاد</p></div>
<div class="m2"><p>به پاسخ چنین کرد ازین گونه یاد</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>که باب مرا نام مهیار بود</p></div>
<div class="m2"><p>به هر کاروان بارسالار بود</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>خردمند بود و گرانمایه بود</p></div>
<div class="m2"><p>سرش چتر خورشید را سایه بود</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>به عزم تجارت به کشتی نشست</p></div>
<div class="m2"><p>ز طوفان دل اهل کشتی بخست</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>چو گردید در کشتی آرام و جا</p></div>
<div class="m2"><p>همان‌گاه شد بخت بد رام ما</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>یکی باد برخاست ناگه شگرف</p></div>
<div class="m2"><p>درانداخت کشتی به دریای ژرف</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>هوا را همه باد و باران گرفت</p></div>
<div class="m2"><p>ز باران همه بحر، طوفان گرفت</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>من از هوش رفتم ندارم خبر</p></div>
<div class="m2"><p>که بابا ز بد خود چه آمد به سر</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>مقارن، خردمند و فرزانه بود</p></div>
<div class="m2"><p>به تدبیر تمجید و مردانه بود</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>ندید هیچ با گفتگویش فروغ</p></div>
<div class="m2"><p>بدانست کو گفت این را دروغ</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بگفتش که چون سرو آزاده‌ای</p></div>
<div class="m2"><p>همانا که تو پادشازاده‌ای</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>بدین فر و این زیور و روی و موی</p></div>
<div class="m2"><p>تو هستی ز شاهان دیهیم جوی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چو گفت این ببوسید روی زمین</p></div>
<div class="m2"><p>بدان ماه‌چهره گرفت آفرین</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>وز آن پس به سوگند لب برگشاد</p></div>
<div class="m2"><p>ز یزدان دارنده می‌کرد یاد</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>که در پرده کم زن مر این ساز را</p></div>
<div class="m2"><p>ولی راست گو با من این راز را</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>که راز دلت را نگویم به کس</p></div>
<div class="m2"><p>همی در نهانی زنم این جرس</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>اگر بر سرم سنگ بارد جهان</p></div>
<div class="m2"><p>ز من راز جوید جهان هر زمان</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>همانا نهان دارم این راز را</p></div>
<div class="m2"><p>نهانی برم در گل این راز را</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>سخن هر چه او گفت مقارن شنید</p></div>
<div class="m2"><p>ز کژی سوی راستی بگروید</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>نخستین بدو نام خود بازگفت</p></div>
<div class="m2"><p>پس آنگه بگفت این حدیث شگفت</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>ز سام سرافراز افسون نمای</p></div>
<div class="m2"><p>چو آگاه شد مرد باهوش و رای</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>زمانی به پیش اندر افکند سر</p></div>
<div class="m2"><p>وز آن پس بدو گفت کای سیمبر</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>چه سازم که آگه بشد باژوان</p></div>
<div class="m2"><p>فرستاد نامه به نزد طغان</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>طغان‌شه ازین حال آگه شود</p></div>
<div class="m2"><p>همه روز شادیت کوته شود</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چو پرسش کند پاسخش چون دهم</p></div>
<div class="m2"><p>ز چنگش بگو در جهان چون رهم</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>پری‌دخت گفتش مشو زین دژم</p></div>
<div class="m2"><p>مدار از طغان شاه در دل الم</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>اگر او پژوهش کند راز من</p></div>
<div class="m2"><p>چنین پاسخش گوی در انجمن</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>که سالار بربر یکی نازنین</p></div>
<div class="m2"><p>روان کرد از بهر فغفور چین</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>چو از شهر بربر شده ره‌سپر</p></div>
<div class="m2"><p>فتادست کشتی به گرداب در</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>ندیده رهائی چو از بحر ژرف</p></div>
<div class="m2"><p>نمودست زین‌گونه کار شگرف</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>به صندوق در کرده مه روی را</p></div>
<div class="m2"><p>پریوش نگار سمن بوی را</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>فکندست در بحرش از ناگهان</p></div>
<div class="m2"><p>بدان تا بیابد ز دریا امان</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>گرفتم چو صندوق آن نازنین</p></div>
<div class="m2"><p>نهانی چنین گفت و نام این چنین</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>کنون مرو را سوی چین می‌برم</p></div>
<div class="m2"><p>بر شاه توران زمین می‌برم</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>گر از من طغان شاه پرسید راز</p></div>
<div class="m2"><p>ابا او بدین سان شوم نغمه‌ساز</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>بپوشم رخ و کم دهم پاسخش</p></div>
<div class="m2"><p>به افسون فرستم سوی خلخش</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>مقارن پذیرفت گفتار او</p></div>
<div class="m2"><p>ز کشتی به هامون نهادند رو</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>علم چون ز کشتی به صحرا زدند</p></div>
<div class="m2"><p>سراپرده بیرون دریا زدند</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>سمن بوی را بس خریدار بود</p></div>
<div class="m2"><p>دل هر کس او را هوادار بود</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>همی گفت با انجمن باژدار</p></div>
<div class="m2"><p>که اینک ز خلخ رسد شهریار</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>برآرد درون مقارن به تیغ</p></div>
<div class="m2"><p>برون آورد ماهوش را به میغ</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>کجا چشمشان بود در راه شاه</p></div>
<div class="m2"><p>مگر دیوزاده بیاید ز راه</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>چو دیدند او را همه کاروان</p></div>
<div class="m2"><p>بماندند بر جا خلیده روان</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>به چربی سخن راند فرهنگ باز</p></div>
<div class="m2"><p>از آن کاروان باز پرسید راز</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>کس از بیم پاسخ‌ ندادی بدوی</p></div>
<div class="m2"><p>سبک دیوزاده دژم کرد روی</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>ز بازارگانان یکی ره گرفت</p></div>
<div class="m2"><p>بپرسید از وی حدیث شگفت</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>همی گفت و می‌زد ورا شهریار</p></div>
<div class="m2"><p>ازو خواست بازارگان زینهار</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>در رازهای نهان باز کرد</p></div>
<div class="m2"><p>ز صندوق و مهوش سخن ساز کرد</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>که چون کرد کشتی به رفتن شتاب</p></div>
<div class="m2"><p>بدیدیم صندوق بر روی آب</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>گشودیم بود اندر آن دلبری</p></div>
<div class="m2"><p>سمن عارضی سرو سیمین‌بری</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>همه مهر او خواهش آراستیم</p></div>
<div class="m2"><p>سراسر مر او را همه خواستیم</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>مقارن که او بار سالار ماست</p></div>
<div class="m2"><p>به هر رنج و سختی نگهدار ماست</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>چو در روی آن حوروش بنگرید</p></div>
<div class="m2"><p>همانا دلش مهر او برگزید</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>به منزلگه خویش بردش فراز</p></div>
<div class="m2"><p>برو مهربان شد بت دلنواز</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>برو چون کسی را نبد دسترس</p></div>
<div class="m2"><p>دگر روی او را ندیدست کس</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>کنون ماه‌چهره به خرگاه اوست</p></div>
<div class="m2"><p>مقارن به صد جان هواخواه اوست</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>چو آگاه شد دیوزاده ازان</p></div>
<div class="m2"><p>روان شد به نزد مه کاروان</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>مقارن چو دیدش بترسید سخت</p></div>
<div class="m2"><p>بلرزید بر خود چو شاخ درخت</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>پری‌دخت را گفت کای مهرجوی</p></div>
<div class="m2"><p>سوی ما دمان دیو آورده‌ روی</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>پری‌دخت چون در نهان بنگرید</p></div>
<div class="m2"><p>یکی پیکر دیوزاده بدید</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>بنالید بر خالق ذوالمنن</p></div>
<div class="m2"><p>ز یال و ز کوپال آن اهرمن</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>به دل گفت اگر جان برم زین دلیر</p></div>
<div class="m2"><p>نگردم به دست وی اینجا اسیر</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>بسی کام گیرم ز فرخنده سام</p></div>
<div class="m2"><p>کنم کار خود را به گیتی تمام</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>تو اینجا مر این داستان را بدار</p></div>
<div class="m2"><p>سخن بشنو از آن یل نامدار</p></div></div>