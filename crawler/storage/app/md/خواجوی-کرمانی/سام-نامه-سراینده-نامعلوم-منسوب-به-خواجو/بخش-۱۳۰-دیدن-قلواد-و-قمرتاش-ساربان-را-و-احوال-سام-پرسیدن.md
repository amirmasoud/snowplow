---
title: >-
    بخش ۱۳۰ - دیدن قلواد و قمرتاش، ساربان را و احوال سام پرسیدن
---
# بخش ۱۳۰ - دیدن قلواد و قمرتاش، ساربان را و احوال سام پرسیدن

<div class="b" id="bn1"><div class="m1"><p>بدو گفت کای مرد صحرانشین</p></div>
<div class="m2"><p>دلیر و خردمند این سرزمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در اینجا ندیدی یکی نوجوان</p></div>
<div class="m2"><p>برهنه سراپا و جانش نوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درخشنده روئی و فرخ فری</p></div>
<div class="m2"><p>دلیری گوی شیر کی منظری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که رنجیده از ما و پنهان شده</p></div>
<div class="m2"><p>درین مرغزاران شتابان شده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر داری از وی نشانی بگوی</p></div>
<div class="m2"><p>که از بهر اوئیم در پویه پوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو ساربان گفت کای شیرمرد</p></div>
<div class="m2"><p>بود چشمه‌ای نام خوش لاجورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدیدم جوانی بدانجا پگاه</p></div>
<div class="m2"><p>دریده قبا و فتاده کلاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فراوان بر او وحشیان گشته رام</p></div>
<div class="m2"><p>چو شیران در آن چشمه دارد کنام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همانا که مرد شما آن بود</p></div>
<div class="m2"><p>و یا صید نخجیریابان بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندارد شب و روز آرام و خواب</p></div>
<div class="m2"><p>همیشه ز دیده گشودست آب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به خود مویه دارد همی زارزار</p></div>
<div class="m2"><p>غریوان و گریان چو ابر بهار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدو گفت قلواد آری هم اوست</p></div>
<div class="m2"><p>که ازهجر اومان بدرید پوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نشان سرچشمه لاجورد</p></div>
<div class="m2"><p>بپرسید و آمد به مانند گرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قمرتاش دنباله آمد چون باد</p></div>
<div class="m2"><p>بر آن چشمه نزدیک آن پاکزاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدیدند یل را برهنه به تن</p></div>
<div class="m2"><p>شده وحشیان گرد او انجمن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همان دم چو هرای اسبان شنید</p></div>
<div class="m2"><p>به مانند وحشی از ایشان رمید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدان شیرمردان غریوان گرفت</p></div>
<div class="m2"><p>شتابان ره کوهساران گرفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به آواز گفتند هر دو دلیر</p></div>
<div class="m2"><p>که ای نامور پهلو گردگیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گریزان چرائی ز ما این چنین</p></div>
<div class="m2"><p>که از چین رسیدیم زی سرزمین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پری‌دخت را آوریدیم پیام</p></div>
<div class="m2"><p>به نزدیک سالار فرخنده سام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نمردست آن شمع مجلس فروز</p></div>
<div class="m2"><p>تو هم همچو پروانه خود رامسوز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که زنده است آن ماه سیمین بدن</p></div>
<div class="m2"><p>فروزنده گشته از آن انجمن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به بیهوده خود را چه سازی تباه</p></div>
<div class="m2"><p>نیاری تو یاد از منوچهر شاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درین دام بیهوده دل بسته‌ای</p></div>
<div class="m2"><p>کزینسان دل و جان و تن خسته‌ای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو نام پری‌دخت بشنید سام</p></div>
<div class="m2"><p>بخندید در حال و گردید رام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نگه کرد یاران خود را بدید</p></div>
<div class="m2"><p>ز شادی رخش همچو گل بشکفید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به قلواد گفت ای دلیر جوان</p></div>
<div class="m2"><p>روانم ازین گفته شد شادمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو برگوی از مه چه داری خبر</p></div>
<div class="m2"><p>کجا باشد آن دلبر سیمبر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که از کار او گشت با من بری</p></div>
<div class="m2"><p>مر آن حوروش مایه دلبری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بیارم برت آن مه دلپذیر</p></div>
<div class="m2"><p>نترسم ز فغفور و دستور پیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برفت و نیامد دگر باز پس</p></div>
<div class="m2"><p>گمانم گرفتار شد در قفس</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>و یا آنکه او داد بر من فریب</p></div>
<div class="m2"><p>چو می‌دیدم از هجر من ناشکیب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>و یا جادوئی بود آن سیمتن</p></div>
<div class="m2"><p>که بنمود این گونه خود را به من</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ندانم چگونه است انجام کار</p></div>
<div class="m2"><p>که مردم در امید آن گلعذار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدو گفت قلواد کای پیلتن</p></div>
<div class="m2"><p>برهمن چنین گفت با من سخن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همه کار دیوان و اسم طلسم</p></div>
<div class="m2"><p>بدو گفت یکسر گو پاک اسم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو بشنید ازو سام بسیار هوش</p></div>
<div class="m2"><p>دگر ره برآورد بر مه خروش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بپرسید از جای دیوان جنگ</p></div>
<div class="m2"><p>همان ابرها آن دد دیوچنگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که از جان شومش برآرم دمار</p></div>
<div class="m2"><p>اگر زنده باشد بت قندهار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بپرسید دیگر که این مرد کیست</p></div>
<div class="m2"><p>رخ خوب این مرد بر گرد چیست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چه جوید درین راه دور و دراز</p></div>
<div class="m2"><p>بگو راز او را بر من فراز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>قمرتاش بوسید روی زمین</p></div>
<div class="m2"><p>فراوان برو برگرفت آفرین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همه در عشق پری‌نوش گفت</p></div>
<div class="m2"><p>چو همدرد دید او چو گل برشکفت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدو گفت گر چرخ دادم دهد</p></div>
<div class="m2"><p>درین نامرادی مرادم دهد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تن ابرها را درآرم ز پا</p></div>
<div class="m2"><p>ببینم دگر چهره دلربا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تن شاه فغفور از تیغ تیز</p></div>
<div class="m2"><p>به مانند نی سازمش ریزریز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همان پیر دستور ناپاک مرد</p></div>
<div class="m2"><p>که او نیز ما را زهم دور کرد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جهان کنم پیش چشمش سیاه</p></div>
<div class="m2"><p>بیندازم او را ابر فر و جاه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نشانم تو را بر سر تخت چین</p></div>
<div class="m2"><p>پری‌نوش را سازمت هم‌قرین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>و گر کشته گردم درین راه دور</p></div>
<div class="m2"><p>دهد داد تو داور ماه و هور</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو گفتار سام یل آمد به بن</p></div>
<div class="m2"><p>قمرتاش بگشاد راز سخن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>که ای سام فرخنده نامدار</p></div>
<div class="m2"><p>بوی شادمان تا بود روزگار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به کام تو بادا همه کار تو</p></div>
<div class="m2"><p>خداوند گیتی نگهدار تو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>من از بهر تو چین و فغفور چین</p></div>
<div class="m2"><p>همان یاره و طوق و تاج و نگین</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همان پیر دستور بی رای و فر</p></div>
<div class="m2"><p>غلامان و ترکان زرین‌کمر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به مهر تو ماند این همه دستگاه</p></div>
<div class="m2"><p>گرفتم ابا گرد قلواد راه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همی خواهم از کردگار بلند</p></div>
<div class="m2"><p>که خرسند گردی و هم ارجمند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ببینی رخ دخت فغفور چین</p></div>
<div class="m2"><p>سرت برفرازد ز چرخ برین</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به یزدان پرستی عهد درست</p></div>
<div class="m2"><p>ورا شاد گرداند آنجا نخست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>جهان دیده قلواد از بهر سام</p></div>
<div class="m2"><p>همه ساز آورده بر وی تمام</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز اسباب و از ساز رزم و شتاب</p></div>
<div class="m2"><p>ببسته سراسر به پشت غراب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بپوشید اسباب رزم آن سوار</p></div>
<div class="m2"><p>نشست از بر باره راهوار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بدان تا بیاید بر ابرها</p></div>
<div class="m2"><p>به خاک اندر آرد سر اژدها</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بلد گشت قلواد و ره برگرفت</p></div>
<div class="m2"><p>به کوه فنا راه دل برگرفت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>براندند اسبان به روز و به شب</p></div>
<div class="m2"><p>که آمد تن اسب در ره به تب</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همه راهشان لاله و مرغزار</p></div>
<div class="m2"><p>همه جای می‌بود جای شکار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>همه لاله و سوسن و ارغوان</p></div>
<div class="m2"><p>همه نرگس و گل کران تا کران</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>درختان بسیار همه میوه‌دار</p></div>
<div class="m2"><p>سر اندر سر آورده بر شاخسار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>برافروخته هر طرف چهره سیب</p></div>
<div class="m2"><p>چو رخسار خوبان شده دلفریب</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>انارش چو عشاق دل پر ز خون</p></div>
<div class="m2"><p>ز خونش همه آبله در درون</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>شده برگ انجیر بالا و زیر</p></div>
<div class="m2"><p>چو دایه که پستان کند پر ز شیر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز انگور ماننده سنبله</p></div>
<div class="m2"><p>چو عاشق دلش گشته پر آبله</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ز هر شاخ نشگفته خندان گلی</p></div>
<div class="m2"><p>ز غنچه شده غنچکی بلبلی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به رقص آمده در چمن نار و سرو</p></div>
<div class="m2"><p>دلش پر ز خون گشته فرخ تذرو</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>عروسان بستان اردی بهشت</p></div>
<div class="m2"><p>حنا بسته هر سو در آن جوی و کشت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>هوا جام نوروز نوشیده بود</p></div>
<div class="m2"><p>زمین جامه سبز پوشیده بود</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو آن دشت فرخنده سام سوار</p></div>
<div class="m2"><p>بدید گور و آمد به سوی شکار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>فکندند نخجیر بر آبگیر</p></div>
<div class="m2"><p>کبابی بکردند و خوردند سیر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چو راندند اسبان در آن مرغزار</p></div>
<div class="m2"><p>به زیر آمد از اسب سام سوار</p></div></div>