---
title: >-
    بخش ۱۴۱ - احوال سام از دیو ره‌دار و عجایبها از مرغ دیدن
---
# بخش ۱۴۱ - احوال سام از دیو ره‌دار و عجایبها از مرغ دیدن

<div class="b" id="bn1"><div class="m1"><p>نگه کرد سام اندر آن برز کوه</p></div>
<div class="m2"><p>زمین را همی دید ازو در ستوه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی دیو جنگی سه سر بر سرش</p></div>
<div class="m2"><p>یکی طوق زرین به گردن درش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مه موی اندام چون گوسفند</p></div>
<div class="m2"><p>رسیده سرش تا به چرخ بلند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چهارش بدی دست آن تیره روی</p></div>
<div class="m2"><p>به هر دستی یک حربه جنگجوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شمشیر و خنجر ز گرز گران</p></div>
<div class="m2"><p>به دست دگر درقه بودش دمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی نعره زد بر سپهدار سام</p></div>
<div class="m2"><p>که ای بی‌خرد خیره تیره کام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه مردی و بوم و نژادت ز کیست</p></div>
<div class="m2"><p>بدین دشت برگو مراد تو چیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا رفت خواهی درین راه سخت</p></div>
<div class="m2"><p>که ریزد ز اندیشه برگ درخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگر سیر گشتی تو از جان خویش</p></div>
<div class="m2"><p>که از مرگ کردی تو درمان خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین راه هرگز نپرد عقاب</p></div>
<div class="m2"><p>ستاره نبیند زمینش به خواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نریمان درین دشت آمد به جنگ</p></div>
<div class="m2"><p>نیاورد یک دم درین که درنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندانم تو را چیست ایدر گذر</p></div>
<div class="m2"><p>که اندیشه نارد در این که گذر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نگه کرد مرغی به مانند پیل</p></div>
<div class="m2"><p>کبودیش بر تن چو دیبای نیل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دو پایش به مانند مرجان به رنگ</p></div>
<div class="m2"><p>بدش سینه برسان پشت پلنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به منقار یاقوت چشمش سیاه</p></div>
<div class="m2"><p>ز پرواز او تیره رخسار ماه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سخن‌گوی مانند آدم زبان</p></div>
<div class="m2"><p>به پرواز در چرخ نعره زنان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز بالا درآمد چو سیلی به زیر</p></div>
<div class="m2"><p>بیازید چنگال مانند شیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به چنگال بربود ده نره دیو</p></div>
<div class="m2"><p>دگر ره به افلاک در شد غریو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خروشان همی رفت تا نزد ابر</p></div>
<div class="m2"><p>رها کردشان سوی دشت هژبر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز بالا فتادند در دشت کین</p></div>
<div class="m2"><p>که گشتند چون توتیا بر زمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شگفتی ازو ماند سالار نیو</p></div>
<div class="m2"><p>بنالید بر صنع کیهان خدیو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سوی دیو ره‌دار آمد به جنگ</p></div>
<div class="m2"><p>همان ابر بارنده خونش به چنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدو گفت کای دیو برگشته بخت</p></div>
<div class="m2"><p>چسان دیدی این رزم و پیکار سخت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خدای جهان آفریدست مرغ</p></div>
<div class="m2"><p>بدین سان کسی کم بدیدست مرغ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدین رنگ و این پیکر پرنهیب</p></div>
<div class="m2"><p>غریوان درآمد ز بالا به شیب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدو گفت کاین مرد شدادیست</p></div>
<div class="m2"><p>ازین او بدین‌گونه او عادیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تن او ز شداد آمد پدید</p></div>
<div class="m2"><p>از آن رو بدین سان کسی کم شنید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شد آشفته زین گفتگو پهلوان</p></div>
<div class="m2"><p>بزد تیغ بر دیو تیره روان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سرش را درافکند در خاک دهر</p></div>
<div class="m2"><p>همه زندگانی بدو گشت زهر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یکی دیگرش تیغ زد بر میان</p></div>
<div class="m2"><p>که شد پیکر دیو ازو پرنیان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همان مرغ یک نعره‌ای برکشید</p></div>
<div class="m2"><p>که گشتند دیوان همه ناپدید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یکی زان همه نره دیوان نماند</p></div>
<div class="m2"><p>که سام سپهدار حیران بماند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>روان مرغ فرخنده با آفرین</p></div>
<div class="m2"><p>ز پرواز آمد به روی زمین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ثنا گفت بر سام پهلو غمین</p></div>
<div class="m2"><p>به کام دلش باد چرخ برین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سر دشمنانش نگونسار باد</p></div>
<div class="m2"><p>تو را دیده بخت بیدار باد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدو گفت ای مرغ فرخنده فر</p></div>
<div class="m2"><p>درآور جهان را ابر زیر فر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ندیدم یکی مرغ همتای تو</p></div>
<div class="m2"><p>به فر و به یال و به بالای تو</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سزد گر که گوئی چه مرغی به نام</p></div>
<div class="m2"><p>که شد زنده از فر تو نیز سام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>توئی آن که سیمرغ دانات خواند</p></div>
<div class="m2"><p>ز کردار تو داستان‌ها براند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به گیتی همه سایه پر تست</p></div>
<div class="m2"><p>به ویژه چنین رزم از فر توست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به پاسخ بدو گفت مرغ گزین</p></div>
<div class="m2"><p>که ای سام سالار ایران زمین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نه مرغم نه سیمرغ در روزگار</p></div>
<div class="m2"><p>بوم کمترین بنده کردگار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مرا نام فرهنگ جنی شناس</p></div>
<div class="m2"><p>که دارم به یزدان فراوان سپاس</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پرستار تسلیم شاهم یکی</p></div>
<div class="m2"><p>ز تو آرزویم بود اندکی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کمربسته هر جا پس و پیش تو</p></div>
<div class="m2"><p>همی کام دارم کم و بیش تو</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>اگر با من ایدر تو پیمان کنی</p></div>
<div class="m2"><p>زبان را به پیمان گروگان کنی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>برآری همی آرزویم به دهر</p></div>
<div class="m2"><p>کنی شهد بر من همی جام زهر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>پس آنگه به گیتی همه کام تو</p></div>
<div class="m2"><p>برآرم ز آغاز و انجام تو</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چنین گفت فرهنگ کای گردسام</p></div>
<div class="m2"><p>مرا خواهری هست شهنازنام</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همی سال ده شد که آن پاک جسم</p></div>
<div class="m2"><p>گرفتار گشتست اندر طلسم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بکردم بسی چاره در روزگار</p></div>
<div class="m2"><p>نیامد به دستم مر آن گلعذار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شنیدم ز دانادلی بیش و کم</p></div>
<div class="m2"><p>که گردی پدید آید از تخم جم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>طلسمی که جمشید ایدر ببست</p></div>
<div class="m2"><p>به نیروی بازو بخواهد شکست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به میدان ببندد تن ابرها</p></div>
<div class="m2"><p>از آن بند و بندساز گردد رها</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کجا نام او سام نیرم بود</p></div>
<div class="m2"><p>میان دلیران چو او کم بود</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کنون آن توئی ای گو سرفراز</p></div>
<div class="m2"><p>که آری تن دشمن اندر گداز</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بدو گفت سام نریمان‌نژاد</p></div>
<div class="m2"><p>که آرم هر آن چیز کردی تو یاد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو این گفته بشنید فرهنگ دیو</p></div>
<div class="m2"><p>زمین را ببوسید در پیش نیو</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بزد بال و دردم سر اندر کشید</p></div>
<div class="m2"><p>ز چشم سپهدار شد ناپدید</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همان شب دلاور در آن دشت ماند</p></div>
<div class="m2"><p>سفیده دمان باز زان جا براند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو یک هفته در ره دمی نارمید</p></div>
<div class="m2"><p>به هشتم به نزدیک دریا رسید</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو دریای آخو ورا نام بود</p></div>
<div class="m2"><p>سپهبد در آن ساحل آمد فرود</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بفرمود ملاح آمد دوان</p></div>
<div class="m2"><p>طلب کرد کشتی هم اندر زمان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بدو گفت ازین آب دریا مرا</p></div>
<div class="m2"><p>رسان از ثری تا ثریا مرا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>سوی شهر شداد عادم رسان</p></div>
<div class="m2"><p>ازین سو بدان سو چو بادم رسان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بدان تا یکی دوزخش بنگرم</p></div>
<div class="m2"><p>برافروزد آنجا یکی آذرم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چه سانست دریا و چند است راه</p></div>
<div class="m2"><p>درین ره چه بینم ز جادو سپاه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>طلسمی که جمشید جم ساخته است</p></div>
<div class="m2"><p>سرش را به گردون برافراخته است</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>کجا بینم او را زرانداب کوه</p></div>
<div class="m2"><p>چگونه است او را ز دیوان گروه</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>همه یک به یک پیش من بازگوی</p></div>
<div class="m2"><p>سر قصه بگشا همه راز گوی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به پاسخ بدو گفت ملاح پیر</p></div>
<div class="m2"><p>که چون اندر آئی درین آبگیر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چهل روز اگر باد باشد مراد</p></div>
<div class="m2"><p>رسانم تو را سوی شداد عاد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چهارت جزیره درآید به پیش</p></div>
<div class="m2"><p>که تیره شود مرد را تیره کیش</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نخستین ببینی تو سگسار شهر</p></div>
<div class="m2"><p>که بر جان شیرین رسانند زهر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>یکی پادشاه است نامش کلاب</p></div>
<div class="m2"><p>ببندد به چشم یلان راه خواب</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>سپاهش فزون است از چون و چند</p></div>
<div class="m2"><p>ازو هست ترسان سپهر بلند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>از آنجا چو رفتی یکی هفته راه</p></div>
<div class="m2"><p>جزیره است مانند دود سیاه</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>که شهر زنانست ای پهلوان</p></div>
<div class="m2"><p>همه ماهرو همچو سرو روان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نباشد در آن شهر فرخنده مرد</p></div>
<div class="m2"><p>پی مرد باشند دلها به درد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو ز آنجا گذشتی ایا نامدار</p></div>
<div class="m2"><p>یکی شهر بینی در آن رهگذار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>که باشد در آنجا مکان پری</p></div>
<div class="m2"><p>یکی پادشاه است پر داوری</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ورا شاه تسلیم خوانی به نام</p></div>
<div class="m2"><p>فکنده ز هر گونه در راه دام</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نترسد ز شداد و وز لشکرش</p></div>
<div class="m2"><p>چهل روز بینی همی کشورش</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>از آنجا یکی کوه باشد بزرگ</p></div>
<div class="m2"><p>به بالا دراز و به پیکر سترگ</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>میان دره آتش افروخته</p></div>
<div class="m2"><p>فراوان در او آدمی سوخته</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>همه هیمه آتشش آدم است</p></div>
<div class="m2"><p>بسی دیو و جادو در آنجا کم است</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ز یک سو بود نیم تن مردمان</p></div>
<div class="m2"><p>فرود آورند ز آسمان اختران</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>زبان پر ز تابی و پرپیچ و تاب</p></div>
<div class="m2"><p>شده نامشان مالکان عذاب</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ز یک سوی آن که چو رفتی ز راه</p></div>
<div class="m2"><p>پس آنگه روی سوی شداد شاه</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>به نزدیک شهرش همه باغ و کشت</p></div>
<div class="m2"><p>بهشتش ببینی چو زرینه خشت</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>ولیکن تو را هست از آنجا گذار</p></div>
<div class="m2"><p>نتابی درین ره ایا نامدار</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>اگر مرغ باشی نپری به بال</p></div>
<div class="m2"><p>ز بیداد شداد آن بدسگال</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>به پاسخ بدو گفت سام دلیر</p></div>
<div class="m2"><p>که ما را گذر ده درین آبگیر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>پس آنگه ببینی که کیهان خدیو</p></div>
<div class="m2"><p>چه سازد بر شداد ناپاک دیو</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>تو کشتی بیاور مرا بر نشان</p></div>
<div class="m2"><p>تماشا کن این رزم گردنکشان</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>چه سازم درین راه با نیم تن</p></div>
<div class="m2"><p>کنم پاک آن بوم را ز اهرمن</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بدو گفت ملاح کای پهلوان</p></div>
<div class="m2"><p>دلیر و جهانجوی و روشن روان</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چه نامت بخوانم به گیتی درون</p></div>
<div class="m2"><p>که خواهی که ریزی ز شداد خون</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>نژاد از که داری و تخم و گهر</p></div>
<div class="m2"><p>چه نامی به نام و که باشد پدر</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>بدو پهلوان نام خود کرد یاد</p></div>
<div class="m2"><p>منم سام نام نریمان‌نژاد</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>که املاق ناپاک را کشت شیر</p></div>
<div class="m2"><p>درآورد روئینه‌تن را به زیر</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بدو گفت آری بدیدم ورا</p></div>
<div class="m2"><p>همان نام فرخ شنیدم ورا</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>که در رزم کشتی مکوکال دیو</p></div>
<div class="m2"><p>ببستی دو دست نهنکال دیو</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>پری‌دخت فغفور را عاشقی</p></div>
<div class="m2"><p>به مردی و مردانگی لایقی</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>همه شب همی گفت از روی مهر</p></div>
<div class="m2"><p>چنین تا گه نقاش نیلی سپهر</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>سفیداج زد بر رخ آسمان</p></div>
<div class="m2"><p>به شنگرف آراست چرخ روان</p></div></div>