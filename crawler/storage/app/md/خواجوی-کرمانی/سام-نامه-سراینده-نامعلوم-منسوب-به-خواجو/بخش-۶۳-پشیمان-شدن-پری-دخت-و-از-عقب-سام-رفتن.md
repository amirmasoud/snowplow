---
title: >-
    بخش ۶۳ - پشیمان شدن پری‌دخت و از عقب سام رفتن
---
# بخش ۶۳ - پشیمان شدن پری‌دخت و از عقب سام رفتن

<div class="b" id="bn1"><div class="m1"><p>سخن گوی دهقان فرخ‌نژاد</p></div>
<div class="m2"><p>چنین از پری‌دخت مه کرد یاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چون از گو شیردل دور ماند</p></div>
<div class="m2"><p>چو باد از پیَش اسب گلگون براند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تهی مغزی و سرکش و تندخوی</p></div>
<div class="m2"><p>ازینها پشیمانی آرد به روی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدل سنگ برزد به سنگین دلی</p></div>
<div class="m2"><p>در آن کار حیران شد از مشکلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خروشش دم صبحگاهی ببست</p></div>
<div class="m2"><p>نفیرش دم مرغ و ماهی ببست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل تنگ را آب کرد از سرشک</p></div>
<div class="m2"><p>جهان غرق خوناب کرد از سرشک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسی دست بر دل زد از دست دل</p></div>
<div class="m2"><p>کش از خون دل پا فروشد به گل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو مهجور ماند از وفادار خویش</p></div>
<div class="m2"><p>خجل شد ز گفتار و کردار خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو مه مهد بر ابر که کوب بست</p></div>
<div class="m2"><p>چو خورشید بر کوه? زین نشست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به آئین ترکان پرخاشخر</p></div>
<div class="m2"><p>روان گشت با تیغ و تیر و سپر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه ملک هستی ز ره برگرفت</p></div>
<div class="m2"><p>پی اسب که‌کوب شه برگرفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بری شد ز دل تا به دلبر رسید</p></div>
<div class="m2"><p>برون شد ز خود تا به او در رسید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز نرگس شده بر سمن سیل‌ریز</p></div>
<div class="m2"><p>ز خون جگر نرگسش سیل‌خیز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فروشسته از اشک یاقوت خام</p></div>
<div class="m2"><p>ز زلف شب تیره کرده ظلام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دلش رفته و از پی دل شده</p></div>
<div class="m2"><p>ز دست دلش پای در گل شده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ره دور از راه افتاده دور</p></div>
<div class="m2"><p>زده شاهرخ از شه افتاده دور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دمیده شب تیره زین سبز باغ</p></div>
<div class="m2"><p>برافروخته زنگی شب چراغ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فلک را ز اکلیل برجبه تاج</p></div>
<div class="m2"><p>زده ماه را مهر بر تخت عاج</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز مهتاب روشن شده کار شب</p></div>
<div class="m2"><p>ز انجم شده گرم بازار شب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خوش‌آوای گردون هم‌آوای مرغ</p></div>
<div class="m2"><p>زده چنگ در ناله نای مرغ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تبیره‌زن نوبتی شام را</p></div>
<div class="m2"><p>به نوبت زده نوبت بام را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پری‌دخت چون اسب سرکش براند</p></div>
<div class="m2"><p>فلک در تکاپوی او بازماند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به هر منزلی کو علم برکشید</p></div>
<div class="m2"><p>ز چشمش بسی چشمها شد پدید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به هر چشمه ساری که مهرخ نشست</p></div>
<div class="m2"><p>از آن چشمه در دم شقایق برست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به هر موضعی کو برآورد دم</p></div>
<div class="m2"><p>زمین از سرشکش برآورد نم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قضا را جنیبت بدان بیشه راند</p></div>
<div class="m2"><p>که مر سام را پای در گل بماند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نظر کرد که پیکر سام دید</p></div>
<div class="m2"><p>که بر طرف نخجیرگه می‌چرید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدانست کان مرغ بی‌بال و پر</p></div>
<div class="m2"><p>در آن آشیان ساختست آبخور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فَرَس پیشتر راند و بشناختش</p></div>
<div class="m2"><p>بر مردم دیده جا ساختش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رخش دید گلگون ز خوناب چشم</p></div>
<div class="m2"><p>لب چشمه پر گوهر از آب چشم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز خون جگر تر شده دامنش</p></div>
<div class="m2"><p>گیا بردمیده ز پیراهنش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در آن چشمه کو رخ به خون شسته بود</p></div>
<div class="m2"><p>ز خون دلش ارغوان رسته بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به سوفار آهی که برمی‌کشید</p></div>
<div class="m2"><p>تتقهای چرخی به هم می‌درید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به هر نوبتی کو همی زد خروش</p></div>
<div class="m2"><p>سپهر سرافکنده می‌شد ز هوش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به سوز نفس کز جگر می ‌گشاد</p></div>
<div class="m2"><p>مه از بام نه پایه درمی‌فتاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدان گونه آتش ز دل برفروخت</p></div>
<div class="m2"><p>که بر وی پری‌دخت را دل بسوخت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بیامد که در پایش افتد چو موی</p></div>
<div class="m2"><p>به چوگان زلفش درآید چو گوی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خرد برزدش نعره کای بیخرد</p></div>
<div class="m2"><p>خردمندت از مردی این نشمرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گرش زانکه می‌آزمائی رواست</p></div>
<div class="m2"><p>که در زور مردانگی تا کجاست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>وگر زانکه زین‌سان طلبگار تست</p></div>
<div class="m2"><p>پریوار مهرش هوادار تست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کند سوی آهوی مستت گذر</p></div>
<div class="m2"><p>و یا همچو آهو رباید ز بر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به خورشید اگر دست بردی چه سود</p></div>
<div class="m2"><p>بَرو دستبردی بباید نمود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>برانگیخت که کوب سرکش ز جای</p></div>
<div class="m2"><p>بزد بانگ بر سام فرخنده‌رای</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که اینجا چه جوئی و کام تو چیست</p></div>
<div class="m2"><p>نژاد از که داری و نام تو چیست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بگفتا که گم کرده‌ام نام خویش</p></div>
<div class="m2"><p>همی خواهم از دادگر کام خویش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بگفتا اگر عاشقی جان بده</p></div>
<div class="m2"><p>وگرنه برو ترک جانان بده</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بگفتا که گر جان دهم در خور است</p></div>
<div class="m2"><p>که جانم پری‌دخت مه‌پیکر است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بگفتا که ای نام بد کرده مرد</p></div>
<div class="m2"><p>حدیثی که گوئی ازو برمگرد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو جانت پری‌دخت گلگون بود</p></div>
<div class="m2"><p>تنت زنده از جان جدا چون بود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بگفتا جدائی ز ناکامی است</p></div>
<div class="m2"><p>نکونامی عشق بدنامی است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بگفتا که دل برکن از مهر او</p></div>
<div class="m2"><p>برون کن ز دل طلعت چهر او</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بگفتا که دل کو سخن در دل است</p></div>
<div class="m2"><p>چو شد دل مرا کار از آن مشکل است</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بگفتا چرا دل بدادی ز دست</p></div>
<div class="m2"><p>فتادی ز دستان چو ماهی به شست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بگفتا به شوخی ز دستم ربود</p></div>
<div class="m2"><p>کنون چون دل از دست دادم چه سود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بگفتا مده دل به تیمار و درد</p></div>
<div class="m2"><p>که انده برآرد ز غمخوار گرد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بگفتا چه گوئی ز احوال دل</p></div>
<div class="m2"><p>که از دل بماندست پایم به گل</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بگفتا تو اینجا درنگ آوری</p></div>
<div class="m2"><p>که بر خانه شاه ننگ آوری</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بگفتا رها کرده‌ام نام و ننگ</p></div>
<div class="m2"><p>بود کان پری را درآرم به چنگ</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بگفتا صبوری ز سیمین‌برش</p></div>
<div class="m2"><p>گرفتی کنار از میان لاغرش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بگفتا ز دلبر گرفتم کنار</p></div>
<div class="m2"><p>کند خون به چشمم سر اندر کنار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بگفتا که در صورت جان ببین</p></div>
<div class="m2"><p>ز زلف و رخش کفر و ایمان ببین</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بگفتا که تا زنده‌ام جانم اوست</p></div>
<div class="m2"><p>دل و دیده و کفر و ایمانم اوست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بگفتا که آرام دارد دلت</p></div>
<div class="m2"><p>نه دل با دل‌آرام دارد دلت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بگفت اوست جان را دل‌آرام دل</p></div>
<div class="m2"><p>که قوت روانست و آرام دل</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بگفتا گرش باز بینی دگر</p></div>
<div class="m2"><p>ز باغ رخش لاله چینی دگر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بگفتا که دارم به دل این هوس</p></div>
<div class="m2"><p>ولی وصل عنقا نیابد مگس</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بگفتا چرا بی‌رخش زنده‌ای</p></div>
<div class="m2"><p>از آن همچو زلفش پراکنده‌ای</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بگفتا دریغ است از آن لب سخن</p></div>
<div class="m2"><p>چو نامش برآید مبر نام من</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بگفتا هم‌اکنونت از گرد راه</p></div>
<div class="m2"><p>بگیرم برم تا به نزدیک شاه</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>منم آنکه چون تیغ کین برکشم</p></div>
<div class="m2"><p>سر چرخ گردون به چنبر کشم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بگرید ز نوک سنان من ابر</p></div>
<div class="m2"><p>بدرد جگرگاه درنده ببر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>من آن شیرگیر پلنگ‌افکنم</p></div>
<div class="m2"><p>که چنگال در شیر گردون زنم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>مرا پیل خوانند جنگ‌آوران</p></div>
<div class="m2"><p>همه سرفرازان و کنداوران</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گرایدون که گرشسب جنگ‌آوری</p></div>
<div class="m2"><p>همین دم ز دستم کجا جان بری</p></div></div>