---
title: >-
    بخش ۱۹۷ - رسیدن سام در برابر ابرها
---
# بخش ۱۹۷ - رسیدن سام در برابر ابرها

<div class="b" id="bn1"><div class="m1"><p>سپیده چو از کوه برکرد سر</p></div>
<div class="m2"><p>سیاهی نهان گشت در بحر و بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گلگونه بر روی افلاک زد</p></div>
<div class="m2"><p>گریبان شب را دگر چاک زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپهبد به طاقی رسید از فنا</p></div>
<div class="m2"><p>سرش رفته بر چرخ و بس دلگشا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی صفه از سنگ پرداخته</p></div>
<div class="m2"><p>همه طرح از رنگ انداخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی تخت از سنگ مرمر بدید</p></div>
<div class="m2"><p>که آن کوه از آن سنگ بد ناپدید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشسته بر آن تخت دیو نژند</p></div>
<div class="m2"><p>سرش پر ز کینه دلش پرگزند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به گردش ز دیوان بدی بی‌شمار</p></div>
<div class="m2"><p>همه بدنژادان خنجرگزار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه همچو کوه و به بالا دراز</p></div>
<div class="m2"><p>به بازو قوی و به دندان گراز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرفته همه گرز و خنجر به دست</p></div>
<div class="m2"><p>همه خشمگین همچو شیران مست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به یک دست آن دیو پر مکر و فن</p></div>
<div class="m2"><p>پری‌دخت بنشسته با صد حزن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو زلف خودش گشته پر پیچ و تاب</p></div>
<div class="m2"><p>ز نرگس به رویش دو دیده گلاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهانی ستمدیده آن ماه چهر</p></div>
<div class="m2"><p>هلالی شده گلرخ ماه مهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو بالای سام نریمان بدید</p></div>
<div class="m2"><p>چو غنچه دل بسته در خون کشید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دگر زندگی ماه از سر گرفت</p></div>
<div class="m2"><p>روان دگر از جهان بر گرفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو سام دلاور رخ حور دید</p></div>
<div class="m2"><p>غم هجر از دلش دور ریخت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دلش رفت از دست و پایش بجا</p></div>
<div class="m2"><p>تو گفتی بشد هوش رزم‌آزما</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پری را بر نره دیوان بدید</p></div>
<div class="m2"><p>ز غیرت دلاور یکی بردمید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برآورد نعره سپهدار یل</p></div>
<div class="m2"><p>که ای نابکاران درآمد اجل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>منم سام غمدیده خسته دل</p></div>
<div class="m2"><p>به هجران جانان خود بسته دل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کشیده بسی درد راه دراز</p></div>
<div class="m2"><p>چو زر رفته در بوتهای گداز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به فرمان یزدان کیهان خدیو</p></div>
<div class="m2"><p>برآرم دمار از تن نره دیو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو دیوان شنیدند نام خدا</p></div>
<div class="m2"><p>ز اندیشه جستند یک یک ز جا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>درافتاد لرزه بدان بدتنان</p></div>
<div class="m2"><p>یکی حمله کردند اهریمنان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>غزنکان جادو درآمد نخست</p></div>
<div class="m2"><p>دل از جان شومش به یک ره بشست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدو گفت کای سام تیره روان</p></div>
<div class="m2"><p>نهادی همی نام خود پهلوان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدین ژاژخوانی شدی نامدار</p></div>
<div class="m2"><p>ندیدی هم‌آورد در روزگار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نمایم به تو دستبرد یلی</p></div>
<div class="m2"><p>که دیگر نگوئی منم زابلی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی دیو دیگر درآمد ز جای</p></div>
<div class="m2"><p>که عفریبت بد نام آن تیره رای</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یکی از چپ آمد دگر سوی راست</p></div>
<div class="m2"><p>که تا جان پهلو درآید به کاست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کشیده همه حربه جان‌ستان</p></div>
<div class="m2"><p>به پیکار آن شیر زابلستان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دلاور پیاده بشد از غراب</p></div>
<div class="m2"><p>گره بر جبین زد درآمد شتاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بنالید بر کردگار جهان</p></div>
<div class="m2"><p>که ای پاک دادار شاهنشهان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مدد از تو خواهم که یزدان توئی</p></div>
<div class="m2"><p>روان بخش بر دردمندان توئی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گدای توام مر مرا دستگیر</p></div>
<div class="m2"><p>منم همچو موئی و دشمن گزیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به دیوان دژخیم در کوهسار</p></div>
<div class="m2"><p>به کوه فنا شد مرا کارزار</p></div></div>