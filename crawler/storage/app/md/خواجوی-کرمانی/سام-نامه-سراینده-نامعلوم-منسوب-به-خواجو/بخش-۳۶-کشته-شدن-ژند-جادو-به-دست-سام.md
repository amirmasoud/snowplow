---
title: >-
    بخش ۳۶ - کشته شدن ژند جادو به دست سام
---
# بخش ۳۶ - کشته شدن ژند جادو به دست سام

<div class="b" id="bn1"><div class="m1"><p>وز آن جا به گنجینه دژ کرد روی</p></div>
<div class="m2"><p>بر آهنگ جادو شده جنگ‌جوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر شهریاران کشورگشای</p></div>
<div class="m2"><p>که بد سام آن گرد فرخنده رای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خروشنده چون ابر بر پشت کوه</p></div>
<div class="m2"><p>شده کوه از بیم او بر ستوه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو خورشید درمانی از سندروس</p></div>
<div class="m2"><p>زده موج بر گنبد آبنوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علم‌های زرین پرچم سیاه</p></div>
<div class="m2"><p>ز ماهی علم برکشیده به ماه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه ماه‌پیکر درفشان درفش</p></div>
<div class="m2"><p>بدو شفت‌های حریر و بنفش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان‌سوز ترکان خنجرگذار</p></div>
<div class="m2"><p>گرفته به کف خنجر زرنگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقیقی عقابان زرینه چنگ</p></div>
<div class="m2"><p>زده چنگ در چرخ فیروزه رنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی سام چون نزد قلعه رسید</p></div>
<div class="m2"><p>همه کوه دریای آتش بدید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان را بدید او به جوش آمده</p></div>
<div class="m2"><p>ز تابش فلک در خروش آمده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شده شیر گردون ز شعله کباب</p></div>
<div class="m2"><p>به جوش آمده چشمه آفتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو سام نریمان چنان حال دید</p></div>
<div class="m2"><p>دم آتش افشان ز دل برکشید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برآشفت بر مرکب بادپای</p></div>
<div class="m2"><p>چو دریای آتش برآمد ز جای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی نام یزدان فراوان بخواند</p></div>
<div class="m2"><p>عنان بر زد و دیو سرکش براند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو بگذشت آتش سر سرکشان</p></div>
<div class="m2"><p>ندید از فروزنده آتش نشان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بسی آفرین کرد بر کردگار</p></div>
<div class="m2"><p>پس آنگه برون شد به سوی حصار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز ناگه برآمد یکی تیره گرد</p></div>
<div class="m2"><p>خروشان چو شیر و غریوان چو رعد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از آن گرد و دم برق جستن گرفت</p></div>
<div class="m2"><p>دل سام یل کی شکفتن گرفت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در آن گرد تیره یکی بنگرید</p></div>
<div class="m2"><p>به گرد اندرون سام نیرم بدید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به دستش سیاه اژدهائی چو مار</p></div>
<div class="m2"><p>یکی دیو پتیاره مانند قار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به قد چو شب تیره‌روزان دراز</p></div>
<div class="m2"><p>برون کرده دندان چو نیش گراز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو چشمش به سام نریمان فتاد</p></div>
<div class="m2"><p>درآمد به سام نریمان چو باد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو پیلی شده بر پلنگی سوار</p></div>
<div class="m2"><p>بغرید مانند رعد بهار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بترسید بر خویشتن نامدار</p></div>
<div class="m2"><p>بغرید مانند رعد بهار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خداوند جان آفرین را بخواند</p></div>
<div class="m2"><p>پس آنگاه بورش همی پیش راند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کمانی که گرشاسب در چنگ داشت</p></div>
<div class="m2"><p>در آن لحظ سام از میان برفراشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کمان را بمالید و بگرفت دست</p></div>
<div class="m2"><p>خدنگی برآورد و بگشود شست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنان زد بر آن پیل‌پیکر پلنگ</p></div>
<div class="m2"><p>که از سهم تیرش فرو رفت خنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو آن ژند جادو چنان حال دید</p></div>
<div class="m2"><p>همه مکر و نیرنگ پامال دید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز پیشش دو تا کوه‌پیکر بجست</p></div>
<div class="m2"><p>به کوه کمرکش درآورد دست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برآورد که پاره‌ای همچو باد</p></div>
<div class="m2"><p>بیفکند بر سام فرخ‌نژاد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کجا دید سام آن‌چنان بر ز سنگ</p></div>
<div class="m2"><p>بجست از تکاور بسان پلنگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به هامون درآمد فرود از ستون</p></div>
<div class="m2"><p>برآورد آن ابر بارنده خون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بزد بر کمرگاه ژند نژند</p></div>
<div class="m2"><p>سر و دست و دوشش به صحرا فکند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو ناچیز شد جادوی خیره سر</p></div>
<div class="m2"><p>روان‌آفرین کرد بر دادگر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پس آنگه به گنجینه دژ رو نهاد</p></div>
<div class="m2"><p>به تندی و تیزی به مانند باد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یکی کوه دید آسمانش کمر</p></div>
<div class="m2"><p>به ایوان کیوان برآورده سر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ره کهکشانش ره کهکشان</p></div>
<div class="m2"><p>سرش سر به سر بر سر کهکشان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بر آن برج کیوان یکی کنگرد</p></div>
<div class="m2"><p>نهم تاج چرخش یکی پنجره</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فراز نهم منظرش رزمگاه</p></div>
<div class="m2"><p>حریم ششم غرفه‌اش بزمگاه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شه طارم چارمش پرده‌وار</p></div>
<div class="m2"><p>یزکدار بهرام خنجرگذار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بر آن قلعه همچو نیلی حصار</p></div>
<div class="m2"><p>نکردی همی مرغ فکرت گذار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>درش را سپهر برین آستان</p></div>
<div class="m2"><p>به بامش زحل کمترین پاسبان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فلک نقشی از طاق ایوان او</p></div>
<div class="m2"><p>طلایه مه و مهر دربان او</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مرو را ز یاقوت رخشنده در</p></div>
<div class="m2"><p>ز یاقوت رخشنده رخشنده‌تر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ستاده به بام آتشین‌پیکری</p></div>
<div class="m2"><p>برآورده الماس‌گون خنجری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کمین کرده بر در یکی نره‌شیر</p></div>
<div class="m2"><p>ز بالای که رو نهاده به زیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چنان بر یل شیردل حمله برد</p></div>
<div class="m2"><p>که شیر سپهر از نهیبش بمرد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>برآورد شمشیر از هوش دل</p></div>
<div class="m2"><p>سر و شش فرو کوفت بر گوش دل</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بدانست سام یل آن آذری</p></div>
<div class="m2"><p>طلسم است و مکر است و جادوگری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یکی نعره زد سام بگشود دست</p></div>
<div class="m2"><p>به زخم عمودش به هم برشکست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>برآمد ز ایوان طراقا طراق</p></div>
<div class="m2"><p>فرود آمد آن صورت از پیش طاق</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به هامون نگون درفتاد از فراز</p></div>
<div class="m2"><p>هم اندر زمان شد در قلعه باز</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو سام آن چنان قلعه در باز دید</p></div>
<div class="m2"><p>به ایوان کاخش علم بر کشید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به برجش برآمد چو سلطان شرق</p></div>
<div class="m2"><p>خور از خجلتش در عرق گشته غرق</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تفرج‌کنان گرد آن بارگاه</p></div>
<div class="m2"><p>برآمد چو بر چرخ گردنده ماه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سرائی پدید آمد از لاجورد</p></div>
<div class="m2"><p>ز یاقوت دیوار و ایوانش زرد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در ایوان درختی ز زر ساخته</p></div>
<div class="m2"><p>سر از طاق ایوان برافراخته</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو بتخانه چین ز نقش و نگار</p></div>
<div class="m2"><p>روان‌بخش و دلکش چو زلفین یار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>یکی تخت فیروزه در پیش‌گاه</p></div>
<div class="m2"><p>پری‌پیکری همچو تابنده ماه</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به گیسو فرو بسته در پای تخت</p></div>
<div class="m2"><p>برو سایه افکنده زرین درخت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>مه غیرت و شمسه خاوری</p></div>
<div class="m2"><p>بت رشک بتخانه آذری</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مسلسل شبش بر رخ روز بود</p></div>
<div class="m2"><p>مهش غیرت عالم‌افروز بود</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>شکر شوری از شهد شکر وشش</p></div>
<div class="m2"><p>گهر آب از آن لعل چون آتشش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شبش خادم سنبل عنبرین</p></div>
<div class="m2"><p>مه از طلعت خرمنش خوشه چین</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چنین گفت سامش که ای حور زاد</p></div>
<div class="m2"><p>بگو کیستی وز که داری نژاد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بدینجا که آوردت ای سیمتن</p></div>
<div class="m2"><p>چرا پای‌بندی به مشکین رسن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بت شکرین لعل شیرین زبان</p></div>
<div class="m2"><p>شکر خنده‌ای کرد و گفت ای جوان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>منم دخت خاقان پری‌نوش نام</p></div>
<div class="m2"><p>درافتاده چون مرغ وحشی به دام</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به شبگون سلاسل به بند اندرم</p></div>
<div class="m2"><p>به مشکین رسن در کمند اندرم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>مرا ژند جادو کمین برگشود</p></div>
<div class="m2"><p>ز ایوان خاقان چینم ربود</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>به مکر و حیل در کمندم فکند</p></div>
<div class="m2"><p>به گنجینه دژ پای بندم فکند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>تو نیز ای به طلعت فروزنده ماه</p></div>
<div class="m2"><p>بگو چون فتادی بدین جایگاه</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>که جادو درین قلعه دارد قرار</p></div>
<div class="m2"><p>نیارد برو مرغ کردن گذار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>درین قلعه سیمرغ پرافکند</p></div>
<div class="m2"><p>سپردار گردون سپر افکند</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>برو رحم کن بر جوانی خویش</p></div>
<div class="m2"><p>ببخشای بر زندگانی خویش</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>مبادا که آن جادوی نابکار</p></div>
<div class="m2"><p>بیاید ز جانت برآرد دمار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بدو سام گفت ای مه مهربان</p></div>
<div class="m2"><p>شب تیره‌ات ماه را سایبان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نگر تا نگوئی ز جادوی مست</p></div>
<div class="m2"><p>که گیتی ز سحرش سراسر برست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>به شمشیر کین داد بستاندمش</p></div>
<div class="m2"><p>به سوی جهنم فرستادمش</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>مخور غم که ما را ازو غم نبود</p></div>
<div class="m2"><p>که شمشیرم از سحر او کم نبود</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>کنون ای پری چهره سیم‌بر</p></div>
<div class="m2"><p>بگو کز پری‌دخت داری خبر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>پری‌نوش گفت ای برادر خموش</p></div>
<div class="m2"><p>که جانم برآمد ازین غم به جوش</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به چین هر دومان چون دو خواهر بدیم</p></div>
<div class="m2"><p>ولی هر یک از یک برادر بدیم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>از اول گرانمایه خاقان چین</p></div>
<div class="m2"><p>به زیر نگین داشت خاورزمین</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ازین دیر خاکی چو محمل براند</p></div>
<div class="m2"><p>به فغفور چین مملکت بازماند</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چو زلف پری‌دخت طوطی‌خرام</p></div>
<div class="m2"><p>دراز است اگر قصه گویم تمام</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>کسی را چو من بخت وارون مباد</p></div>
<div class="m2"><p>دل خسته در ورطه خون مباد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>تو نیز از پری دخت سیمین بدن</p></div>
<div class="m2"><p>چو بیگانه‌ای از چه رانی سخن</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>روان سام احوال خود شرح داد</p></div>
<div class="m2"><p>که در دام آن دخت چون اوفتاد</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>دگر گفت کای سرو پسته دهن</p></div>
<div class="m2"><p>جمال تو فال همایون من</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چو آن ترک سیمین‌بر سنگدل</p></div>
<div class="m2"><p>چنان تنگ چشمست و من تنگدل</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>برو راز خویش از چه پیدا کنم</p></div>
<div class="m2"><p>وزو کام دل چون تمنا کنم</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بگفت این و آهی ز دل برفروخت</p></div>
<div class="m2"><p>بت لاله‌رخ را برو دل بسوخت</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>به لولو دو رخسار میگون بخست</p></div>
<div class="m2"><p>تو گوئی که از چشمش آتش بجست</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>ز بادام، گلبرگ را آب داد</p></div>
<div class="m2"><p>به فندق، سر زلف را تاب داد</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>پس آنگه شکرخای شیرین سخن</p></div>
<div class="m2"><p>شکر ریخت از شهد شیرین شکن</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>سر درج یاقوت بگشود و گفت</p></div>
<div class="m2"><p>که مشک تتاری نشاید نهفت</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چه پوشیده داری ز من ماجرا</p></div>
<div class="m2"><p>که این درد را از من آید دوا</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>اگر دور گردون به چینم برد</p></div>
<div class="m2"><p>سوی شاه توران زمینم برد</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>به دیدار عمم به هامون رسم</p></div>
<div class="m2"><p>پری‌دخت را بینم و این بسم</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>رسانم دلت را ز دلبر به کام</p></div>
<div class="m2"><p>برون آرمت همچو آهو ز دام</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>روان سام بر وی ثنا گسترید</p></div>
<div class="m2"><p>پس آنگه ز بندش برون آورید</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>زمانی بگشتند با یک‌دگر</p></div>
<div class="m2"><p>رسیدند ناگه به کاخی دگر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>ز فیروزه دیدند ایوان چهار</p></div>
<div class="m2"><p>درو سیمگون قبه زرنگار</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>فکنده برو کرسی از لعل فام</p></div>
<div class="m2"><p>نهاده برو لوحی از سیم خام</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>در و بند او صندل خام بود</p></div>
<div class="m2"><p>نگارش همه نقره خام بود</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>نوشته بر آن لوح سیمین به زر</p></div>
<div class="m2"><p>که ای تاجور سام والاگهر</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>هر آن گه که آئی بدین سرزمین</p></div>
<div class="m2"><p>نگه کن به جمشید با فر و دین</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>بدان ای سرافراز والاگهر</p></div>
<div class="m2"><p>کجا برفزودی تو از ما گهر</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>به فرمان من بود دیو و پری</p></div>
<div class="m2"><p>چنین هفت کشور زمین سرسری</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>شب و روز جز شاد نگذاشتم</p></div>
<div class="m2"><p>ز هر شادی‌ای بهره برداشتم</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>اگر چه بدم گنج شاهی بسی</p></div>
<div class="m2"><p>بدان گونه رفتم که کمتر کسی</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>چنین آمد این گنبد بی‌درنگ</p></div>
<div class="m2"><p>نخستین دهد شهد وانگه شرنگ</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>نگر تا نباشی به دم استوار</p></div>
<div class="m2"><p>به من بنگر و زو دل ایمن مدار</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چو من پادشاهی در عالم نبود</p></div>
<div class="m2"><p>ندیده چو من نیز چرخ کبود</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>گو پهلوان کرد فیروزبخت</p></div>
<div class="m2"><p>که زیبد سپهر و مهت تاج و تخت</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چو گنجینه‌دژ را مسخر کنی</p></div>
<div class="m2"><p>طلسمی به فرزانگی بشکنی</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چو این قبه را ساختی آشیان</p></div>
<div class="m2"><p>فرو شو بدین پایه نردبان</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>که تا گنج جمشید آری به چنگ</p></div>
<div class="m2"><p>برآری سر از چرخ فیروزه رنگ</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>تو دانی نیای تو جمشید بود</p></div>
<div class="m2"><p>که تاجش نمودار خورشید بود</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>بدان ای جهان پهلو سرفراز</p></div>
<div class="m2"><p>که گردد به دست تو این گنج باز</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>چو باشد به گنج منت دسترس</p></div>
<div class="m2"><p>ببخشای و محروم مگذار کس</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>خوری یا خورندش به هر کس که زیست</p></div>
<div class="m2"><p>که چون ندهی و بنهی آن از تو نیست</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>همه سال اندر توانا نه‌ای</p></div>
<div class="m2"><p>که امروز اینجا و فردا نه‌ای</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>چو دستت نباشد به چیزی رسان</p></div>
<div class="m2"><p>چه آن تو باشد چه آن کسان</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>ببخشای خود هر چه داری به زیست</p></div>
<div class="m2"><p>که داند که فردا سرانجام چیست</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>پناهت بداد آفرین دار و بس</p></div>
<div class="m2"><p>که از هر بدی هست فریادرس</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>چو برخوانی این لوح سیمین تمام</p></div>
<div class="m2"><p>ز جمشید بادت درود و سلام</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>چو سام یل آن لوح سیمین بخواند</p></div>
<div class="m2"><p>بسی دُر بدان لوح زرین فشاند</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>روان سام چون چشم را کرد باز</p></div>
<div class="m2"><p>به زیر زمین دید راهی دراز</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>ز مرمر در آن پای‌ها ساخته</p></div>
<div class="m2"><p>همه خشت زرین درانداخته</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>فرو شد در آن پایه فرخنده سام</p></div>
<div class="m2"><p>نگه کرد یک دم در آنجا تمام</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>دری دید عالی ز سنگ رخام</p></div>
<div class="m2"><p>بر آن قفل افکنده از سیم خام</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>بیازید بازو و بگشاد دست</p></div>
<div class="m2"><p>در و قفل زرین به هم برشکست</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>پدید آمد ایوان زرین چهار</p></div>
<div class="m2"><p>چو بتخانه چین به رنگ و نگار</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>چهل خم درو پر ز لعل و گهر</p></div>
<div class="m2"><p>همه درکشیده به زنجیر زر</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>بدان هر یکی گوهر شب‌چراغ</p></div>
<div class="m2"><p>درخشنده هر یک چو در شب چراغ</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>چو سام آنچنان دید با دلنواز</p></div>
<div class="m2"><p>برون آمد از گنج آن سرفراز</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>چو خورشید تابان گردون رکیب</p></div>
<div class="m2"><p>به بالا برآمد چون ابر از نهیب</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>پری نوش را بر تکاور نشاند</p></div>
<div class="m2"><p>به فرقش در و لعل و گوهر فشاند</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>شد اندر رکاب سمنبر چو باد</p></div>
<div class="m2"><p>پیاده سوی کاروان رو نهاد</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>پری‌وش چو خورشید و گلگون چو ابر</p></div>
<div class="m2"><p>همی سام چون پیل غران هژبر</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>یکی همچو مه از سر کوه‌سار</p></div>
<div class="m2"><p>یکی سایه مانده از مهر یار</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>یکی آفتابی رسید به کوه</p></div>
<div class="m2"><p>یکی از رهی گشته از غم ستوه</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>یکی صبح از بام سر برزده</p></div>
<div class="m2"><p>یکی صبح تا شام بر سر زده</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>یکی چون پری جسته از دست دیو</p></div>
<div class="m2"><p>یکی را چو دیوانه جان در غریو</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>چنین تا رسیدند در قافله</p></div>
<div class="m2"><p>علم برکشیدند بر مرحله</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>همه کاروان گهر افشاندند</p></div>
<div class="m2"><p>به پای فرس‌شان زر افشاندند</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>چو آگه شد آن پیر سالار باز</p></div>
<div class="m2"><p>که سام آمد از جنگ آن سرفراز</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>به خیمه درآوردشان پیرکار</p></div>
<div class="m2"><p>گهر کرد بر سام نیرم نثار</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>بگفت این زمان جای آرام نیست</p></div>
<div class="m2"><p>مرا جز به گنجینه دژکام نیست</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>سبک درنشینم ازینجا کنون</p></div>
<div class="m2"><p>بدان دژ رویم و به پشت هیون</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>به هامون کشیم آن گرانمایه گنج</p></div>
<div class="m2"><p>فرامش کنیم آن همه درد و رنج</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>پس آنگاه سام نریمان برین</p></div>
<div class="m2"><p>برآمد چو مه بر سپهر برین</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>همه برنشستند کنداوران</p></div>
<div class="m2"><p>شتابنده بر پشت که پیکران</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>همان دم رسیدند بر تیغ کوه</p></div>
<div class="m2"><p>بگشتند از آن راه گردان ستوه</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>خروشان به گنجینه دژ شدند</p></div>
<div class="m2"><p>به ایوان ژند بداختر شدند</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>به هر گوشه گنجی ز زر یافتند</p></div>
<div class="m2"><p>به هر گنج گنجی دگر یافتند</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>تفرج‌کنان گرد آن بارگاه</p></div>
<div class="m2"><p>بگشتند با سام گیتی‌پناه</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>پس آنگه به گنج اندرون تاختند</p></div>
<div class="m2"><p>ز یاقوت و زر در بپرداختند</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>چو سام آن چنان گنج‌دژ برگشاد</p></div>
<div class="m2"><p>جهان کرد از گنج جمشید یاد</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>هزار و دو صد اشتر از سیم و زر</p></div>
<div class="m2"><p>دو صد استر بردعی پر گهر</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>چو عود و قماری چو دیبای چین</p></div>
<div class="m2"><p>چو یاقوت رمان چو در ثمین</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>چو فیروز? سبز و مشک ختن</p></div>
<div class="m2"><p>چو لعل بدخشان عقیق یمن</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>به پشت ستوران دریا گذار</p></div>
<div class="m2"><p>به هامون کشیدند از آن کوهسار</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>علم‌های دیبا برافراشتند</p></div>
<div class="m2"><p>سوی مرز چین راه برداشتند</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>گروهی هیونان البرزران</p></div>
<div class="m2"><p>شتابنده در زیر بار گران</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>از آنجا علم سوی صحرا زدند</p></div>
<div class="m2"><p>دلیران همه سر به بالا زدند</p></div></div>