---
title: >-
    بخش ۱۳۱ - رسیدن سام و قلواد و قمرتاش و جنگ کردن با دیوانه شاه‌پور
---
# بخش ۱۳۱ - رسیدن سام و قلواد و قمرتاش و جنگ کردن با دیوانه شاه‌پور

<div class="b" id="bn1"><div class="m1"><p>به یاد پری‌دخت فرخنده سام</p></div>
<div class="m2"><p>زمانی بخوابید بیدار سام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بالین او شیر قلواد بود</p></div>
<div class="m2"><p>ز دیدار او کو بسی شاد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ناگه خروشی برآمد چو ابر</p></div>
<div class="m2"><p>تو گفتی که بگذشت آنجا هژبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلرزید آن دشت از آواز او</p></div>
<div class="m2"><p>فلک کر شد از نعره ساز او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جا جست قلواد بیدار دل</p></div>
<div class="m2"><p>بر آن نعره بنهاد هشیار دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگه کرد مردی چو نر اژدها</p></div>
<div class="m2"><p>درآمد به حمله چو ابر بلا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی دید ماننده نره دیو</p></div>
<div class="m2"><p>که بر چرخ گردون رساندی غریو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به تن همچو کوه و به بازو قوی</p></div>
<div class="m2"><p>به بالا بلند و به فر گوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به سر موش ژولیده همچو شیر</p></div>
<div class="m2"><p>به تن زورمند و به حمله دلیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کفی بر لب آورده چون پیل مست</p></div>
<div class="m2"><p>چو گرزی گرفته یکی چوبدست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر و تن به آهن گرفته همه</p></div>
<div class="m2"><p>ز پیکرش و هامون نهفته همه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به بر کرده جامه ز چرم پلنگ</p></div>
<div class="m2"><p>غریونده برسان غران پلنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برآورد نعره که ای بدتنان</p></div>
<div class="m2"><p>ستمکاره و رهزن اهریمنان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درین دشت فرخ چرا آمدید</p></div>
<div class="m2"><p>شتابان بگو کز کجا آمدید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ندانید که این جا کنام من است</p></div>
<div class="m2"><p>سر چرخ گردون به دام من است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درین دشت شیر ژیان نگذرد</p></div>
<div class="m2"><p>اگر زان که آید تنش بشکرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نپرد عقاب اندرین سرزمین</p></div>
<div class="m2"><p>چو پرد همی پر بریزد به کین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدو گفت قلواد چندین مدم</p></div>
<div class="m2"><p>که هرگز ندیدی نهنگ دژم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه هر جای تندی به کار آیدت</p></div>
<div class="m2"><p>که ناگه یکی کارزار آیدت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ندیدی به از خود شدستی دلیر</p></div>
<div class="m2"><p>که رو به نسنجد به چنگال شیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برآشفت آن مرد ژولیده موی</p></div>
<div class="m2"><p>به حمله درآمد سوی نامجوی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدو حمله آورد قلواد شیر</p></div>
<div class="m2"><p>بغرید مانند شیر دلیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بیفکند شمشیر بر شیر مست</p></div>
<div class="m2"><p>بزد چوب و شمشیر بر هم شکست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دگر چوب زد بر سر دوش او</p></div>
<div class="m2"><p>که قلواد فرخ درآمد برو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قمرتاش آمد کمانی به چنگ</p></div>
<div class="m2"><p>ز ترکش کزین کرد تیر خدنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بپیوست بر چرخ و بگشاد پر</p></div>
<div class="m2"><p>بدزدید سر مرد پرخاشخر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خدنگش چو رو کرد آمد چو شیر</p></div>
<div class="m2"><p>بزد چوب بر بازوی آن دلیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که افتاد از زخم یک چوبدست</p></div>
<div class="m2"><p>قمرتاش و قلواد بر هم ببست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دگرباره زد نعره‌ای چون هژبر</p></div>
<div class="m2"><p>خروشنده شد همچو غرنده ابر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وز آن نعره مرد با دار و گیر</p></div>
<div class="m2"><p>ز جا جست سام سوار دلیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بمالید مژگان و وی را بدید</p></div>
<div class="m2"><p>که ماننده شیر نر می‌دمید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فرو بسته دست دو فرخنده مرد</p></div>
<div class="m2"><p>به خواری فکنده به دشت نبرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برآشفت از کار او سام یل</p></div>
<div class="m2"><p>به رزمش درآمد چو پیل اجل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدو گفت ای بدرگ دیوزاد</p></div>
<div class="m2"><p>چه نامی به نام از که داری نژاد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که بربسته‌ای این یلان را دو دست</p></div>
<div class="m2"><p>خروشی به ماننده پیل مست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سر راه از ما چه بگرفته‌ای</p></div>
<div class="m2"><p>به بیهوده زینسان چرا تفته‌ای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چه کردی درین دشت و یار تو کیست</p></div>
<div class="m2"><p>درین جای فرخنده کار تو چیست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ندانی که هر جای تندی به کار</p></div>
<div class="m2"><p>نیاید نترسی تو از روزگار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به پاسخ بدو گفت آشفته مرد</p></div>
<div class="m2"><p>ندیدی مرا روز دشت نبرد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نیاید کسی پیش من روز جنگ</p></div>
<div class="m2"><p>نه نراژدها و نه غران نهنگ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ندانی مگر نام شاپور شیر</p></div>
<div class="m2"><p>که آرد به نیرو سر چرخ زیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>من از عادیانم ز مغرب زمین</p></div>
<div class="m2"><p>ز شدادیانم پر از خشم و کین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همه مغرب از من رسیده به جان</p></div>
<div class="m2"><p>همی مرگ می‌جوید از من امان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بسا کس ز نیروی من کشته شد</p></div>
<div class="m2"><p>سراپای شاهان پر آغشته شد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نداری اگر باور این گفتگو</p></div>
<div class="m2"><p>چو شیر ژیان اندر آر و برو</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نمایم تو را زود چنگال خویش</p></div>
<div class="m2"><p>برافروزم از تو یکی جان خویش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بداختر کنم اختر شوم تو</p></div>
<div class="m2"><p>نمانم به گیتی بر و بوم تو</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بگفت و بغرید چون پیل مست</p></div>
<div class="m2"><p>درانداخت بر سام یل چوبدست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سپر بر سر آورد فرخنده سام</p></div>
<div class="m2"><p>ز نیرو یکی پیش بنهاد گام</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>فرو کوفت آن چوب بر پهلوان</p></div>
<div class="m2"><p>که از زخم او گشت هامون نوان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بپیچید از آواز بر چرخ پیر</p></div>
<div class="m2"><p>تو گفتی که کیوان درآورد زیر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سپهبد بدانست کو پهلو است</p></div>
<div class="m2"><p>به هنگام پیکار مرد گو است</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بدو گفت شاپور چونست مرد</p></div>
<div class="m2"><p>به میدان آورد روز نبرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بگفت و دگر حمله آورد سخت</p></div>
<div class="m2"><p>که از بیم لرزید شاخ درخت</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سپهبد ز نیرو خبردار شد</p></div>
<div class="m2"><p>به دل گفت اکنون مرا کار شد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ندیدم بدین زور و بازو کسی</p></div>
<div class="m2"><p>اگر چند من رزم دیدم بسی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نباید که این کشته گردد نخست</p></div>
<div class="m2"><p>بگیرم ورا زنده و تندرست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به ایران برم زنده زین رزمگاه</p></div>
<div class="m2"><p>بدان تا ببیند منوچهر شاه</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو بر حمله آورد شاپور شیر</p></div>
<div class="m2"><p>پس آنگه چنین گفت سام دلیر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>که ازگرز من هم یکی نوش کن</p></div>
<div class="m2"><p>نبرد خودت را فراموش کن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بدان تا بدانی تو پیلان مست</p></div>
<div class="m2"><p>ننازی تو دیگر بدین چوبدست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بگفت و برآورد گرز گران</p></div>
<div class="m2"><p>فرو کوفت چون پتک آهنگران</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بلرزید میدان از آن روز جنگ</p></div>
<div class="m2"><p>بغلطید در قله کوه سنگ</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بلرزید ماهی ابر زیر گاو</p></div>
<div class="m2"><p>زمانه نیاورد از آن گرز تاو</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شگفتی فرو ماند آشفته مرد</p></div>
<div class="m2"><p>که هرگز ندیدم بدین سان نبرد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بسی آفرین کرد بر پهلوان</p></div>
<div class="m2"><p>بدان زور و بازوی روشن روان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دگر ره درآمد یل نامدار</p></div>
<div class="m2"><p>ثنا خواند بر داور کردگار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>عمود دگر زد چنان بر سرش</p></div>
<div class="m2"><p>که آتش علم بر زد از پیکرش</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ز دودش چنان تیره شد روزگار</p></div>
<div class="m2"><p>که نه سام پیدا نه آن مرد کار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>خروشان به هم هر دو آویختند</p></div>
<div class="m2"><p>بسی زهر بر همدگر ریختند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>همی کرد زد گرز از چوبدست</p></div>
<div class="m2"><p>که نامد به چیزی ز سامش شکست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چو صد حمله کردند رد و بدل</p></div>
<div class="m2"><p>که دل خون شد از پیک و بیم اجل</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سرانجام سام یل دیوبند</p></div>
<div class="m2"><p>به شاپور افکند خم کمند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گرفت آن خم خام از زور دست</p></div>
<div class="m2"><p>همه چرم شیر و پلنگان گسست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>که او را ببندد به خم کمند</p></div>
<div class="m2"><p>بیازید دست آن یل دیوبند</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چنین تا که عنبرفشان شد هوا</p></div>
<div class="m2"><p>نهان گشت کافور و عنبر روا</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>زمانه همه گرد عنبر گرفت</p></div>
<div class="m2"><p>جهان چادر مشک در سر گرفت</p></div></div>