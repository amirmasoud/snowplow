---
title: >-
    بخش ۴۷ - غزل خواندن سام بر در حرم‌سرای پری‌دخت
---
# بخش ۴۷ - غزل خواندن سام بر در حرم‌سرای پری‌دخت

<div class="b" id="bn1"><div class="m1"><p>نگارا سمن عارضا دلبرا</p></div>
<div class="m2"><p>بتا ماه‌رویا پری پیکرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمال تو شمعست و پروانه دل</p></div>
<div class="m2"><p>هوای تو گنج است و ویرانه دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندیدم چو پسته دهان تو هیچ</p></div>
<div class="m2"><p>نبستم طمع در میان تو هیچ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهان تو همچون میان نیست هست</p></div>
<div class="m2"><p>وگر زان که گوئی چنان نیست هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر صبح دعوی کند صادقست</p></div>
<div class="m2"><p>که بر مهر رویت چو من عاشقست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وگر آفتابست روشن ببین</p></div>
<div class="m2"><p>که دارد ز مهرت دل آتشین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سیمین تنت کوه گیرد کمر</p></div>
<div class="m2"><p>که خود را توان بر تو بستن مگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا بی سر زلفت آرام نیست</p></div>
<div class="m2"><p>برون از تو دل را دلارام نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وگر چشم مست تو گوید که هست</p></div>
<div class="m2"><p>گواهی نشاید شنیدن ز مست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درخشان عذار تو در شب چراغ</p></div>
<div class="m2"><p>درافشان لبت گوهر شب چراغ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر زلف شوریده‌ات دام دل</p></div>
<div class="m2"><p>لب لعل جان پرورت کام دل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دو زلفت دو هندوی عنبرفروش</p></div>
<div class="m2"><p>دو لعلت دو طوطی شکر فروش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فریبنده جادوت مخمور هست</p></div>
<div class="m2"><p>دو آشفته هندوت آتش‌پرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حبش را ز هندوی زلف تو داغ</p></div>
<div class="m2"><p>مقیم آهویت خفته بر طرف باغ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در آن زلف شوریده پر شکست</p></div>
<div class="m2"><p>همه هندوانند آتش‌پرست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دلاویز مویت یا مشکناب</p></div>
<div class="m2"><p>دل افروز رویت یا آفتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو افکند زلف تو بر آب شست</p></div>
<div class="m2"><p>چرا همچو هندو بر آتش‌پرست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کمانی چو ابروی شوخت که دید</p></div>
<div class="m2"><p>که جمشید او را نشاید کشید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سر زلفت آشفته حال از چه روست</p></div>
<div class="m2"><p>که ما را دل خسته در بند اوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو چشم تو فتنه است گو خفته باش</p></div>
<div class="m2"><p>ز روی تو گو آب گل رفته باش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چرا چشم مستت به محراب شد</p></div>
<div class="m2"><p>که در طاق محراب در خواب شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو دانی که در نرگست خواب نیست</p></div>
<div class="m2"><p>که مستست و در خواب و محراب نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دو ابرو سر حاجتی داردت</p></div>
<div class="m2"><p>که پیوسته سر سوی گوش آردت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دلم قامتت زان تمنا کند</p></div>
<div class="m2"><p>که آتش همه میل بالا کند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>میان تو همچون دهان هیچ نیست</p></div>
<div class="m2"><p>ترا تا کمر در میان هیچ نیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ترا زان سر فتنه‌انگیزی است</p></div>
<div class="m2"><p>که چشم تو در عین خون‌ریزی است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چرا نالم از زلف مدپوش تو</p></div>
<div class="m2"><p>پراکنده گشتست بر دوش تو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شب تارت امید مشک تتار</p></div>
<div class="m2"><p>ز هندوت صد شور در زنگبار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نسیمت ز چین و شبت روز نوش</p></div>
<div class="m2"><p>چو بازار چین گشته عنبر فروش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خطت مشک بویست و خود مشک پوش</p></div>
<div class="m2"><p>رخت لفروز است و لب می فروش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به دریای عشق تو دردانه دل</p></div>
<div class="m2"><p>ز زنجیر زلف تو دیوانه دل</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو در دستم آن زلف سرکش فتاد</p></div>
<div class="m2"><p>از آن جان و تن در کشاکش فتاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ترا طره در عین طراریست</p></div>
<div class="m2"><p>از آن رو که کارش سیه کاریست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>قدم چون کمان باشد و دل چو تیر</p></div>
<div class="m2"><p>که تیر و کمان باشدم دلپذیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مکن بر خطا پیش برچین زرنگ</p></div>
<div class="m2"><p>برون آ چو آئینه چین ز زنگ</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>برون آ و در چشم من تکیه ده</p></div>
<div class="m2"><p>که سرو سهی در لب چشمه به</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو ترکی و خال تو هندو چراست</p></div>
<div class="m2"><p>که از هندوان ترک تازی خطاست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو خوش باش کان جغد در آتشست</p></div>
<div class="m2"><p>که پیوسته ابروی خوبت خوشست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برفتی و نقشت نرفت از خیال</p></div>
<div class="m2"><p>که جانی و از جان نگیرد ملال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شبت گرد خورشید عالم‌فروز</p></div>
<div class="m2"><p>حبش تاختن کرد بر نیم‌روز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شبت را مه و مهر در حیز است</p></div>
<div class="m2"><p>از آن خادم سنبلت عنبر است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دهان توام در کمان افکند</p></div>
<div class="m2"><p>میان توام در میان افکند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دو چشم سیاهت کماندار مست</p></div>
<div class="m2"><p>که پیوسته کارش کمانداریست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز چشم خوشت چشم بد دور باد</p></div>
<div class="m2"><p>روانم ز عشق تو پر نور باد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو دستان آن مرغ دستان سرا</p></div>
<div class="m2"><p>شنیدند مستان بستان سرا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مه و مطرب از دست بنهاد عود</p></div>
<div class="m2"><p>برآمد به سوز دل از عود دود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نهادند بر قول او جمله گوش</p></div>
<div class="m2"><p>برفتند یکباره جمله ز هوش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>پری‌دخت از آن خسروانی سرود</p></div>
<div class="m2"><p>برفت از دل تنگش آرام زود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>رخش در چمن هچو گل برشگفت</p></div>
<div class="m2"><p>سر حلقه لعل بگشود و گفت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>که تا پاسبان شد ترنم‌نواز</p></div>
<div class="m2"><p>نیامد به خود بلبل مست باز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چه حالست کامشب چنین می‌زند</p></div>
<div class="m2"><p>به چوبک ره عقل و دین می‌زند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مگر چو من او نیز دل‌داده‌ایست</p></div>
<div class="m2"><p>دلش در کمند پریزاده‌ایست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نوا هر دم از بی‌نوائی زند</p></div>
<div class="m2"><p>ولیکن دم از آشنائی زند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>یکی گفت بلبل به وقت سحر</p></div>
<div class="m2"><p>بود نال? زار او زارتر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>یکی گفت قمری به وقت بهار</p></div>
<div class="m2"><p>برآورده از صبح بانگ هزار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>یکی گفت مست است و مست خراب</p></div>
<div class="m2"><p>برآرد فغان چون نیابد شراب</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>یکی گفت ار زان که درویش مست</p></div>
<div class="m2"><p>چه درمان که کارش برون شد ز دست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>یکی گفت چوبک زن بام ما</p></div>
<div class="m2"><p>مگر گشته عاشق به ایام ما</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دمی بر سرودش نهادند گوش</p></div>
<div class="m2"><p>برآمد ز مرغ صراحی خروش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>می‌ تلخ در ساغر انداختند</p></div>
<div class="m2"><p>ز لب شیر در شکر انداختند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>فلک شیشه می چو بر سنگ زد</p></div>
<div class="m2"><p>بت چنگ زن چنگ در چنگ زد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو او ناله ز ایوان به کیوان رساند</p></div>
<div class="m2"><p>که سام آن غزل را به پایان رساند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>زمانی بگردید بر طرف بام</p></div>
<div class="m2"><p>پس آنگه برآمد چو ماه تمام</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز روزن نظر در شبستان فکند</p></div>
<div class="m2"><p>دلش آتش سینه در جان فکند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز ایوان بهشتی پر از حور دید</p></div>
<div class="m2"><p>ز نزدیکیش چشم بد دور دید</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سمن عارضان چهره آراسته</p></div>
<div class="m2"><p>سر زلف بر گل بپیراسته</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نواساز مستان نوا ساخته</p></div>
<div class="m2"><p>ترنم به طارم درانداخته</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو مرغ سحر در سماع آمده</p></div>
<div class="m2"><p>ولی طره‌شان در نزاع آمده</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نوا بانگ بر نیم مستان زده</p></div>
<div class="m2"><p>قدح خنده بر می‌پرستان زده</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>مه دلبران شاه مه‌ پیکران</p></div>
<div class="m2"><p>بت گلرخان سرو سیمین بران</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به اورنگ زرین نشسته چو ماه</p></div>
<div class="m2"><p>فروهشته از چهره شعر سیاه</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>درافکنده در زلف مشکین گره</p></div>
<div class="m2"><p>برافکنده بر برگ نسرین‌زره</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو تنگ شکر در شکرگون پرند</p></div>
<div class="m2"><p>شکر ریخته از نمکدان به قند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گهی قول عشاق می‌کرد گوش</p></div>
<div class="m2"><p>گهی باده لعل می‌کرد نوش</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>نسیمش گره در خم مو فکند</p></div>
<div class="m2"><p>سر زلفش از چهره یک سوفکند</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تو گفتی شب تیره رخشنده ماه</p></div>
<div class="m2"><p>به درآمد از زیر ابر سیاه</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بت چنگ زن چنگ در رود زد</p></div>
<div class="m2"><p>دو تا چشم دلبر دم از رود زد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بگرئید چون شمع بر کار خویش</p></div>
<div class="m2"><p>بخندید بر گریه زار خویش</p></div></div>