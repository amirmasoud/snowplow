---
title: >-
    بخش ۱۳۹ - مکر کردن قهرمان به سام و آمدن فرهنگ، آگاه کردن سام را
---
# بخش ۱۳۹ - مکر کردن قهرمان به سام و آمدن فرهنگ، آگاه کردن سام را

<div class="b" id="bn1"><div class="m1"><p>دگر قهرمان چاره نو گزید</p></div>
<div class="m2"><p>قلم بر سر حرف دولت کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ایوان سرویش یکی چاه کرد</p></div>
<div class="m2"><p>ز شیطان دل خویش گمراه کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بپوشید رویش به خاشاک و خاک</p></div>
<div class="m2"><p>که از سام نیرم برآرد هلاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیفکند مسند برآراست بزم</p></div>
<div class="m2"><p>در آن دوستی چاره سازید رزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین گفت با سام یل قهرمان</p></div>
<div class="m2"><p>که ای گرد روشن دل مهربان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی باغ دارم چو خرم بهشت</p></div>
<div class="m2"><p>که رضوان گلش را سراسر بکشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز لاله نهاده به فرودس داغ</p></div>
<div class="m2"><p>ز گل کرده روشن هزاران چراغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خوبان در آن جلوه‌گر تازه سرو</p></div>
<div class="m2"><p>ز خونخواره عشاق در وی تذرو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خطیبی شده بلبل از شاخ گل</p></div>
<div class="m2"><p>ز عکس رخ آب گردیده گل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زبان کرده سوسن به سنبل دراز</p></div>
<div class="m2"><p>شده نرگس مست ازو پر ز ناز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو مخمور افکنده سر را به پیش</p></div>
<div class="m2"><p>دل غنچه از خار غم گشته ریش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درو غنچه زرد پیش سرای</p></div>
<div class="m2"><p>شده هاون زر در آن مشک سای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زده تکیه هر سو چو شاخ سمن</p></div>
<div class="m2"><p>شده گل بت بلبلش برهمن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شکوفه به ریحان درم ریخته</p></div>
<div class="m2"><p>ابا سیم و عنبر درآمیخته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سر تاج یاقوت بستان فروز</p></div>
<div class="m2"><p>به بر حسن گل عنبر تر زده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>و یا خط نورسته مشکبار</p></div>
<div class="m2"><p>نمایان شده بر رخ گلعذار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چمن چون جوانیست آراسته</p></div>
<div class="m2"><p>برو خال و خط جمله پیراسته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو وصفش کنم قصه گردد دراز</p></div>
<div class="m2"><p>ندانی همه تا بدانی تو باز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو بشنید ازو سام رزم آزما</p></div>
<div class="m2"><p>بدو گفت تازم بر ابرها</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که جانانه را باز چنگ آورم</p></div>
<div class="m2"><p>سر نره دیوان به سنگ آورم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نشاید دگر مانم ایدر به بزم</p></div>
<div class="m2"><p>که پیش است بسیار پیکار و رزم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نیارست جانم به چنگ بلا</p></div>
<div class="m2"><p>دل من به هجران او مبتلا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ره دور در پیش و ساغر به دست</p></div>
<div class="m2"><p>بر عاشف صادق اینها بد است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدو گفت یک روز دیگر بپا</p></div>
<div class="m2"><p>برافروز بستان به فرخ‌لقا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من آن باغ را فرخ آباد نام</p></div>
<div class="m2"><p>نهادم که بیند سرافراز سام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بگفتند از آن بزم و برخاستند</p></div>
<div class="m2"><p>ز چهره رخ باغ آراستند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که ناگه بر سام فرهنگ دیو</p></div>
<div class="m2"><p>درآمد ثنا خواند بر گرد نیو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نوشته یکی کاغذی در نهان</p></div>
<div class="m2"><p>بدادش به سالار روشن روان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بپرسید نامش که ای دیو نر</p></div>
<div class="m2"><p>نژاد از که داری بگو سر به سر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدو گفت فرهنگ دیو گوم</p></div>
<div class="m2"><p>نهانی یکی بنده پهلوم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز تسلیم جنی رسانم پیام</p></div>
<div class="m2"><p>به نزدیک سالار بیدار سام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من آنم که عاق ستمکاره را</p></div>
<div class="m2"><p>مر آن زشت ناپاک بدکاره را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سرش را بکندم ز ملک بدن</p></div>
<div class="m2"><p>گریزان شدند از برم انجمن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بترسید سقلاب تیره روان</p></div>
<div class="m2"><p>گریزان شد از پیش من در زمان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همه کوه و هامون به هم برشکست</p></div>
<div class="m2"><p>همه سحر و جادوی آمد به پست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به فر خداوند جان آفرین</p></div>
<div class="m2"><p>زمان و زمین شد چو خلد برین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>امیدم به دادار پروردگار</p></div>
<div class="m2"><p>که شادان بوی تا بود روزگار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بزودی ببینی رخ یار خویش</p></div>
<div class="m2"><p>ولیکن مشو غافل از کار خویش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که اینجا بود مرز جادوگران</p></div>
<div class="m2"><p>به دم درکشند اژدهای دمان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بسی شاد شد پهلو تیزچنگ</p></div>
<div class="m2"><p>دعا کرد بر دیو با فر و هنگ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>وز آن پس سپهبد سر نامه خواند</p></div>
<div class="m2"><p>وز آن خط جهاندار خیره بماند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نوشته که ای سام رزم‌آزما</p></div>
<div class="m2"><p>چو آئی درین باغ و بستان سرا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یکی مسندی بینی آراسته</p></div>
<div class="m2"><p>به زر و به زیور بپیراسته</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نیاری در آن جایگه برنشست</p></div>
<div class="m2"><p>که نام بلندی در آری به پست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همان قهرمان را بر آنجا نشان</p></div>
<div class="m2"><p>بدان تا ببینی ز دشمن نشان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دلاور به تسلیم کرد آفرین</p></div>
<div class="m2"><p>بنالید آنگه به جان آفرین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همی گفت کای داور راستان</p></div>
<div class="m2"><p>به جان من خسته بخشی امان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دگر ره نهان گشت فرهنگ دیو</p></div>
<div class="m2"><p>سوی باغ درشد سپهدار نیو</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شگفتی بهشتی در آنجا بدید</p></div>
<div class="m2"><p>نه زان سان بدید و نه هرگز شنید</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چنین گفت پس سام با قهرمان</p></div>
<div class="m2"><p>که بنشین به جای خودت یک زمان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>من اینجا نشینم تو بالا نشین</p></div>
<div class="m2"><p>که تو پادشاهی به تاج و نگین</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بدو شاه سقلاب گفت از شماست</p></div>
<div class="m2"><p>که من زیردستم شما را سزاست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گرفتش دو بازو سپهدار سام</p></div>
<div class="m2"><p>کشیدش بدان مسند تیره فام</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ز چاره همی خواست کار و نشست</p></div>
<div class="m2"><p>بدان چاره خود درافتاد پست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به زوبین و خنجر به تیغ و سنان</p></div>
<div class="m2"><p>درافتاد آنگاه و بسپرد جان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همان چه که خود کند در وی فتاد</p></div>
<div class="m2"><p>چنین دارم از مرد گوینده یاد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>که زنهار هرگز مکن تیره چاه</p></div>
<div class="m2"><p>به راه کسان تا نیفتی به چاه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اگر بدکنش باشی و بدگهر</p></div>
<div class="m2"><p>زمانه همین کارت آرد به سر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مکن بد که تا بد نبینی به پیش</p></div>
<div class="m2"><p>چنین است آزاده را دین و کیش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بخندید سام و وز آن جایگاه</p></div>
<div class="m2"><p>چو برگشت آمد سوی تختگاه</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نشست از بر تخت سقلاب سام</p></div>
<div class="m2"><p>به فرمان او شد همه خاص و عام</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>سپه یکسره سوی درگاه سام</p></div>
<div class="m2"><p>رسیدند گردان فرخنده نام</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بیاراست قانون شاهی به داد</p></div>
<div class="m2"><p>در عدل و بخشش به هر کس گشاد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>جهان عدل شمشیر از سر گرفت</p></div>
<div class="m2"><p>چو دلبر همی عدل دلبر گرفت</p></div></div>