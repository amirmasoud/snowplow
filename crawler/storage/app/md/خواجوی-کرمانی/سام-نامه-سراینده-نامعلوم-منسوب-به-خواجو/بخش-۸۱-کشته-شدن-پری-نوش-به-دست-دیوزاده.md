---
title: >-
    بخش ۸۱ - کشته شدن پری‌نوش به دست دیوزاده
---
# بخش ۸۱ - کشته شدن پری‌نوش به دست دیوزاده

<div class="b" id="bn1"><div class="m1"><p>یکی سنگ زد آن‌چنان بر سرش</p></div>
<div class="m2"><p>که افتاد از زین زر پیکرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روان از پری‌نوش در شد زمان</p></div>
<div class="m2"><p>برآمد ز جان سپاهش فغان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سبک بارگیها برانگیختند</p></div>
<div class="m2"><p>ابا دیوزاده درآویختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به یک ره بر او برگشادند دست</p></div>
<div class="m2"><p>برآویخت با سرکشان پیل مست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وزین سوی فرشاد پرخاشجوی</p></div>
<div class="m2"><p>درآورد با لاله‌رخ رو به روی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پری‌دخت را بی‌شمر برشمرد</p></div>
<div class="m2"><p>وز آن پس به پیکار کین دستبرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برآویخت چون اهرمن با پری</p></div>
<div class="m2"><p>قران کرد مریخ با مشتری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پریچهره گلگون برانگیخت تند</p></div>
<div class="m2"><p>نشد هیچ با رزم فرشاد کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان تیغ کین زد ورا بر کمر</p></div>
<div class="m2"><p>که دو نیمه گشت و درآمد به سر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز کینه روان جان فرشاد شد</p></div>
<div class="m2"><p>به گردون گردنده فریاد شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپه را ز پیکار دل سیر گشت</p></div>
<div class="m2"><p>چو دشمن به سالارشان چیر گشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به پس در پری‌دخت و فرهنگ گرد</p></div>
<div class="m2"><p>برفتند باز از پی دستبرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل لشکر از رزم و کینه نژند</p></div>
<div class="m2"><p>ز بهر دو سالار دل مستمند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گریزان و گریان به هر سو دوان</p></div>
<div class="m2"><p>ز بهر دو سالار خود خونفشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پری‌دخت و فرهنگ و چندین سوار</p></div>
<div class="m2"><p>رسیدند با تیغ زهر آبدار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گریزندگان سیه گشته بخت</p></div>
<div class="m2"><p>بماندند بیچاره زان کار سخت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همی گفت هر یک به دیگر چنین</p></div>
<div class="m2"><p>که ما را مگر بخت بد شد قرین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یسار و یمین پیش و پس دشمن است</p></div>
<div class="m2"><p>هوا پر ز دام دلیر افکن است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به بالا کمند است و در زیر تیر</p></div>
<div class="m2"><p>به حلیه نیابیم راه گریز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه سالار داریم و نه رای جنگ</p></div>
<div class="m2"><p>نداریم در کینه هیجا و جنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به کین چون دلیران برافراشتند</p></div>
<div class="m2"><p>ازیشان یکی زنده نگذاشتند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکی مرد از آن لشکر نامور</p></div>
<div class="m2"><p>بشد تا کند شاه را باخبر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو از تیغ کندآوران جان ببرد</p></div>
<div class="m2"><p>ز بس بیم از اسب افتاد و مرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در آن جنگ آن مهوش گل‌عذار</p></div>
<div class="m2"><p>به قلوش بشد مانع از کارزار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که تا مردی خود نماید تمام</p></div>
<div class="m2"><p>که هست از دل و جان طلبکار سام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو از جنگ و کین دل بپرداختند</p></div>
<div class="m2"><p>همی پرسش از یکدگر ساختند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برو قلوش گرد راز نهفت</p></div>
<div class="m2"><p>هر آنچه که بد سر به سر بازگفت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز قلوش بپرسید فرهنگ گرد</p></div>
<div class="m2"><p>که چون سام در رزم کین دست برد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دلیران چو از کین بپرداختند</p></div>
<div class="m2"><p>درفش ستیزه بینداختند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چه سان رفت با شاه چینی نبرد</p></div>
<div class="m2"><p>که او هست در نزد مردان مرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از آن پس ز فرهنگ پرسید راز</p></div>
<div class="m2"><p>بگفتش سراسر حدیث دراز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پریوش بپرسید احوال سام</p></div>
<div class="m2"><p>وز آن راحت روح و آرام و کام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بماندند آن شب در آن مرغزار</p></div>
<div class="m2"><p>دم صبح بستند بر پیل بار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سوی لشکر سام کردند روی</p></div>
<div class="m2"><p>همه همچو صرصر شده تیزپوی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به ره در کنون سرکشان را بمان</p></div>
<div class="m2"><p>سخن بشنو از سام روشن‌روان</p></div></div>