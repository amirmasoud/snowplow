---
title: >-
    بخش ۱۰۸ - جنگ تکش خان با سام نریمان
---
# بخش ۱۰۸ - جنگ تکش خان با سام نریمان

<div class="b" id="bn1"><div class="m1"><p>تکش خان جنگی شه تاشکن</p></div>
<div class="m2"><p>که چون او نبد در جهان رزمزن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا شاه را بود مهتر پسر</p></div>
<div class="m2"><p>وزو بد همه تاشکن سر به سر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون آن داوری دید آهیخت تیغ</p></div>
<div class="m2"><p>درآمد به خون ریختن بی‌ دریغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر لشکر زابل آمد فراز</p></div>
<div class="m2"><p>بیفکند ازیشان بسی رزمساز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمین را ز خون ارغوان خیز کرد</p></div>
<div class="m2"><p>هر آنگه که آهنگ آویز کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گهی تیغ زد گاه پیچان سنان</p></div>
<div class="m2"><p>ندیده کسش هیچ دست و عنان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو چندین سران را سرآورد و بست</p></div>
<div class="m2"><p>به گردان زابل درآمد شکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همانگه خبر شد به نزدیک سام</p></div>
<div class="m2"><p>که آمد سوی کین یکی خویشکام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو گوئی که سوزنده‌تر ز آذرست</p></div>
<div class="m2"><p>نیندیشد ار دهر پرلشکر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پی کینه و رزم دل آکنده کرد</p></div>
<div class="m2"><p>سپاه گشن را پراکنده کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنون ای جهان‌پهلوان پاکدین</p></div>
<div class="m2"><p>کسی نیست کو را درآرد ز زین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شگفتی فروماند سام دلیر</p></div>
<div class="m2"><p>برانگیخت اسب از پی دار و گیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدان سان ز جا اندر آمد غراب</p></div>
<div class="m2"><p>که از گرد او رخ نهفت آفتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به میدان بدان گونه جولان نمود</p></div>
<div class="m2"><p>که گرد سمش شد به چرخ کبود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو باد اندر آمد میان سپاه</p></div>
<div class="m2"><p>جهان دید از نامداران سیاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز خون دشت کین رود جیحون شده</p></div>
<div class="m2"><p>دل جنگی از پرده بیرون شده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فتاده تن گشته در خاک و خون</p></div>
<div class="m2"><p>از آن گشته نام‌آوران بی سکون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تکش خان خروشنده چون پیل مست</p></div>
<div class="m2"><p>یکی تیغ پر خون گرفته به دست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چپ و راست را آورد او ستیز</p></div>
<div class="m2"><p>وزو بود نام‌آوران در گریز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جهان‌پهلوان چون بدیدش چنان</p></div>
<div class="m2"><p>به دستی کمند و به دستی سنان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برو برخروشید کای نامدار</p></div>
<div class="m2"><p>مکن ترکتازی یکی پایدار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر سرکشی جنگجو شو ز مرد</p></div>
<div class="m2"><p>بدان تا ببینی ز مردان مرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تکش خان نگه کرد بر پهلوان</p></div>
<div class="m2"><p>عنان تاخت از رزم نام آوران</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو باد اندر آمد به نزدیک سام</p></div>
<div class="m2"><p>خروشید ازو سام و برگفت نام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نظر کرد بر وی گو نامور</p></div>
<div class="m2"><p>یل شیردل دید بازیب و فر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بنفشه رمیده ز برگ گلش</p></div>
<div class="m2"><p>زده تکیه بر نسترن سنبلش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ببرد از همه ساز آن شیر کین</p></div>
<div class="m2"><p>چو شاخ گلی برنشسته به زین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برو سام یل را بجنبید مهر</p></div>
<div class="m2"><p>همانگه بدو گفت کای خوب چهر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زمن نام جستی شنو نام من</p></div>
<div class="m2"><p>ولیکن ز گفتار ده کام من</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مرا سام نام است ای ارجمند</p></div>
<div class="m2"><p>سرآمد ز من مکر و افسون و بند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سزد گر به من برگشائی دو لب</p></div>
<div class="m2"><p>نسازی نهان نام و گوئی نسب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنین پاسخ آراستش نامدار</p></div>
<div class="m2"><p>که من هستم از گوهر شهریار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدو سام یل گفت کای نوجوان</p></div>
<div class="m2"><p>نکردی چو گفتم تو نامت نهان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بیا روی برتاب از کیش بت</p></div>
<div class="m2"><p>ازین پس ستایش مکن پیش بت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به یزدان روزی‌دهنده گرای</p></div>
<div class="m2"><p>که کردست گرنده گردون به پای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز بت روی برتاب و هم برشکن</p></div>
<div class="m2"><p>ببخشم به تو خاور و تاشکن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سر شاه چین را ببرم به کین</p></div>
<div class="m2"><p>از آن پس تو را شاه سازم به چین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تکش خان نپذرفت گفتار سام</p></div>
<div class="m2"><p>رخ آورد از کین به پیکار سام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برآویخت با او جهان پهلوان</p></div>
<div class="m2"><p>شد از گردشان قیرگون آسمان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به تیغ و سنان و به گرز و کمند</p></div>
<div class="m2"><p>برآویختند آن دو گرد بلند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دل هر دو از درد هم گشته خون</p></div>
<div class="m2"><p>ولیکن نگشتند از هم زبون</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز ناگه جهان پهلو نامور</p></div>
<div class="m2"><p>بزد دست و بگرفت او را کمر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز بس مهر دل در ربودش ز جا</p></div>
<div class="m2"><p>تکش خان فغان کرد کای پاکزاد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>برون کن زمانی سر از خشم و کین</p></div>
<div class="m2"><p>مرا باز بنشان بر افراز زین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>که از بت رخ خویش برتافتم</p></div>
<div class="m2"><p>سوی دین دادار بشتافتم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به زین درنشاندنش جهانجوی باز</p></div>
<div class="m2"><p>تکش خان بدو گفت کای سرفراز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چنین بود پیمان من در نهان</p></div>
<div class="m2"><p>که هر کس ببندد مرا از مهان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همانگه شهی را گذارم به جای</p></div>
<div class="m2"><p>ببندم کمر نزد آن پاکرای</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کنون تو ز مردی و نیروی دست</p></div>
<div class="m2"><p>مرا ساختی ای سرفراز پست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به گیتی کمینه غلامم تو را</p></div>
<div class="m2"><p>چو بخت نکوخواه رامم تو را</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ازو شاد شد پهلو نامور</p></div>
<div class="m2"><p>تکش خان ز بت باز پیچید سر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>جهان آفریننده را یاد کرد</p></div>
<div class="m2"><p>ز یادش دل خویش را شاد کرد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>وز آن پس برانگیخت اسب نبرد</p></div>
<div class="m2"><p>بر لشکرش رفت بر سان گرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خروشید کای لشکر نامجوی</p></div>
<div class="m2"><p>سراسر ز بت بازتابید روی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>که در من در جهان ز اهل ایمان شدم</p></div>
<div class="m2"><p>هوا خواه سام نریمان شدم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شما نیز رخ را سوی کین کشید</p></div>
<div class="m2"><p>به نزد سپاه شه چین کشید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز گفت تکش خان نیزه‌گذار</p></div>
<div class="m2"><p>بجنبید جنگ‌آوران چل هزار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>همه تیغ و نیزه برافراختند</p></div>
<div class="m2"><p>سوی قلب فغفور برتاختند</p></div></div>