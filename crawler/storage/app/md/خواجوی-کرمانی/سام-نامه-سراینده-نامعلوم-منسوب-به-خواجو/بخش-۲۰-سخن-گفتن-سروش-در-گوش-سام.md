---
title: >-
    بخش ۲۰ - سخن گفتن سروش در گوش سام
---
# بخش ۲۰ - سخن گفتن سروش در گوش سام

<div class="b" id="bn1"><div class="m1"><p>به گوشش فرو گفت فرخ سروش</p></div>
<div class="m2"><p>که از دست دادی دل و عقل و هوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که گفتت به هر صورتی سر درآر</p></div>
<div class="m2"><p>تصور کن از نقش صورت نگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر آن کو به دل صورت اندیش نیست</p></div>
<div class="m2"><p>یقینم که او جای معنیش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذر کن ز دل تا به دلبر رسی</p></div>
<div class="m2"><p>ز سر درگذر تا به سرور رسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر اهل دلی دل به دلبر سپار</p></div>
<div class="m2"><p>چو از دل برآئی دم از دل برآر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم سرد را همدم خویش کن</p></div>
<div class="m2"><p>ز مژگان نمک بر دل ریش کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می صاف از دردی دیده ساز</p></div>
<div class="m2"><p>کباب از دل خون چکانید ساز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل خسته در پای دلبر نشان</p></div>
<div class="m2"><p>به سرو روانش روان برفشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بساز از سر زلف او دام دل</p></div>
<div class="m2"><p>برآر از لب لعل او کام دل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین ره قدم بر سر خویش نه</p></div>
<div class="m2"><p>وز آن پس سر خویش را پیش نه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر مرد راهی ز خود درگذر</p></div>
<div class="m2"><p>به منزلگه بیخودی برگذر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به چین رو که فالت همایون شود</p></div>
<div class="m2"><p>ز ماه رخش مهرت افزون شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به چین زلف دلبر توانی کشید</p></div>
<div class="m2"><p>که از چین شود نافه چین پدید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برو خون خور و سنبلش بر سرآر</p></div>
<div class="m2"><p>که از خون بود اصل مشک تتار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صوابست راه ختا رفتنت</p></div>
<div class="m2"><p>ولی خون خود باد در گردنت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ره چین سپر چون مغ بت‌پرست</p></div>
<div class="m2"><p>که در چین دهد نقش استاد دست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو سام یل از خواب سر برگرفت</p></div>
<div class="m2"><p>ز مهر رخش چهره در زر گرفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه گلزار دید و نه قصر بلند</p></div>
<div class="m2"><p>نه بستان‌سرای و نه نیلی پرند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ستاره غراب سیه بر سرش</p></div>
<div class="m2"><p>فگنده ز خود سایه بر پیکرش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به یاد آمدش صورت دلربا</p></div>
<div class="m2"><p>گهر ریخت از چرخ بر کهربا</p></div></div>