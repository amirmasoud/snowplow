---
title: >-
    بخش ۱۷۹ - آمدن خاتوره مادر عوج به یاری شداد
---
# بخش ۱۷۹ - آمدن خاتوره مادر عوج به یاری شداد

<div class="b" id="bn1"><div class="m1"><p>چو خورشید بنهاد بر سر کلاه</p></div>
<div class="m2"><p>به تخت فلک باز بنشست شاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزد سکه بر چرخ گردنده هور</p></div>
<div class="m2"><p>زمین و زمان گشت ازو پر ز نور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خبر شد به نزدیک شداد عاد</p></div>
<div class="m2"><p>که خاتوره آمد ز ره همچو باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به همراه او لشکر بیکران</p></div>
<div class="m2"><p>همه دیوچهران و تیره روان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آمد پسر را چنان خسته دید</p></div>
<div class="m2"><p>ز زخم سپهبد تنش بسته دید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بپرسید از عوج دردت ز کیست</p></div>
<div class="m2"><p>تن زخم و رخسار زردت ز چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به تو کس نتابد به میدان جنگ</p></div>
<div class="m2"><p>چرائی ز کار زمانه به تنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدین یال و کوپال غمگین مباش</p></div>
<div class="m2"><p>همی شاد می‌باش و پرکین مباش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که من روز را پیش او شب کنم</p></div>
<div class="m2"><p>یکی مرگ را پیش او تب کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به افسون ببندم مر او را دو دست</p></div>
<div class="m2"><p>به بندش درآرم چو پیلان مست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که باشد به گیتی یکی زابلی</p></div>
<div class="m2"><p>که بر ما زند خنجر کابلی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفت و همه زخم او را ببست</p></div>
<div class="m2"><p>به افسونگری زود بگشاد دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی گفت خاتوره بدنژاد</p></div>
<div class="m2"><p>به شداد کای شاه با دین و داد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جهان را همه زیب از فر توست</p></div>
<div class="m2"><p>کجا مهر در سایه پر توست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جهان از تو گشته است یکسر پدید</p></div>
<div class="m2"><p>چرا بنده بدگهر سرکشید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدو گفت شداد کای نازنین</p></div>
<div class="m2"><p>ابر جاودان اوستاد گزین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز من گشته بیزار سام سوار</p></div>
<div class="m2"><p>نخواند مرا هیچ پروردگار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به دیوان ستایش نماید همی</p></div>
<div class="m2"><p>در کین به رویم گشاید همی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه لشکرم را سراسر بکشت</p></div>
<div class="m2"><p>رخش سوی یزدان به من کرده پشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ولی من خداوند بخشایشم</p></div>
<div class="m2"><p>پی بندگان من در آسایشم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ابر بندگان بخشش آرم به دهر</p></div>
<div class="m2"><p>کشم از بدنشان همه جام زهر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ولیک اگر خشم آرم یکی</p></div>
<div class="m2"><p>نماند ازین بندگانم یکی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنین داد پاسخ مر او را شدید</p></div>
<div class="m2"><p>کزین سان سخنها نباید شنید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کنون چون چنان گشت پیکار سام</p></div>
<div class="m2"><p>به میدان بسازیم ما کار سام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو فردا برآید بلندآفتاب</p></div>
<div class="m2"><p>سر جنگجویان درآید ز خواب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به میان شتابم به پیکار و جنگ</p></div>
<div class="m2"><p>جهان را کنم پیش او تار و تنگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کشم من ورا یا شوم کشته زار</p></div>
<div class="m2"><p>من و تیغ و میدان و سام سوار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نمانم که پی بر زمین برنهد</p></div>
<div class="m2"><p>به میدان دگر ره ز کین سر نهد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز گیتی براندازم او را نشان</p></div>
<div class="m2"><p>تن کشته‌اش را بیارم کشان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بگفت و دو جام پیاپی کشید</p></div>
<div class="m2"><p>به رزم سپهبد همی می کشید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یکی هفته مجلس بیاراستند</p></div>
<div class="m2"><p>می و رود و رامشگران خواستند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به هشتم درآمد غو نای و کوس</p></div>
<div class="m2"><p>زمانه شد از گرد چون آبنوس</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ببستند بر چار پیل سفید</p></div>
<div class="m2"><p>یکی تخت مانند فیروزه شید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به بالاش چتری همه هفت رنگ</p></div>
<div class="m2"><p>خوش آینده برسان پشت پلنگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>درفشی همه پیکرش زر ناب</p></div>
<div class="m2"><p>درخشنده ماننده آفتاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نشست از بر تخت شداد عاد</p></div>
<div class="m2"><p>دلش پر ز آتش سرش پر ز باد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کشیدند صف لشکر عادیان</p></div>
<div class="m2"><p>بیاراستند جمله شدادیان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به میدان کشیدند پس پنج صف</p></div>
<div class="m2"><p>خروشنده و بر لب آورده کف</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ستادند بر هر صفی صدهزار</p></div>
<div class="m2"><p>همه عادی بدرگ کینه‌دار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به یک دست خاتوره بدگهر</p></div>
<div class="m2"><p>به دست دگر عوج بیدادگر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زمانه ازیشان شده همچو نیل</p></div>
<div class="m2"><p>نوان کوه و هامون ابر پای پیل</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زمانه همه کوه آهن شده است</p></div>
<div class="m2"><p>زمین پاک در زیر جوشن شده است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز های و ز هو گوش افلاک کر</p></div>
<div class="m2"><p>زمین و زمان بسته بر کین کمر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جهاندیده سالار بیدار سام</p></div>
<div class="m2"><p>برون شد چو شیر دمان از کنام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سراپا به خفتان جمشید جم</p></div>
<div class="m2"><p>نشست از بر اسب شیر دژم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همان شاه نوشاد زرین کلاه</p></div>
<div class="m2"><p>ابا نامور شاه تسلیم شاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بیاراسته هر دو سر لشکری</p></div>
<div class="m2"><p>ز مردم ز دیوان و هم از پری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو شد راست آن میمنه میسره</p></div>
<div class="m2"><p>چو شیر ژیان پیش برج بره</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به قلب اندر آن سام لشکرپناه</p></div>
<div class="m2"><p>درخشان چو بر آسمان گرد ماه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زمین کرد با چرخ گردون وداع</p></div>
<div class="m2"><p>سر چرخ گردون شده پر صداع</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز بوق و ز کوس و دم کره‌نا</p></div>
<div class="m2"><p>تو گفتی زمین سر به سر شد ز جا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دکان اجل را قضا باز کرد</p></div>
<div class="m2"><p>قدر بر بلا هر دم آواز کرد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که امروز بازار گرم است و سخت</p></div>
<div class="m2"><p>که بندند بر تخته مرگ رخت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به سواد همه جان و تن می‌دهند</p></div>
<div class="m2"><p>به بادفنا انجمن می‌دهند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همه دوستی رفته در گوشه‌ای</p></div>
<div class="m2"><p>همی خواست بهر خودش توشه‌ای</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مدارا و مهر از جهان دور شد</p></div>
<div class="m2"><p>اجل را همی کینه مزدور شد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به لشکر چنین گفت شداد عاد</p></div>
<div class="m2"><p>که ای نامداران مغرب‌نژاد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بباید سواری چو نر اژدها</p></div>
<div class="m2"><p>کزو سام جنگی نیابد رها</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شتابد به میدان غریوان به جنگ</p></div>
<div class="m2"><p>سر سام نیرم بگیرد به چنگ</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هر آن کس که بیرون رود زین میان</p></div>
<div class="m2"><p>بغرد به میدان چو شیر ژیان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ببندد دو دست گو سیستان</p></div>
<div class="m2"><p>براندازد این گرد گیتی ستان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هر آن چیز خواهد مر او را دهم</p></div>
<div class="m2"><p>سپاس دگر بر تن او نهم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>که شد دست این مرد بر ما دراز</p></div>
<div class="m2"><p>نتابید کس پیش این رزمساز</p></div></div>