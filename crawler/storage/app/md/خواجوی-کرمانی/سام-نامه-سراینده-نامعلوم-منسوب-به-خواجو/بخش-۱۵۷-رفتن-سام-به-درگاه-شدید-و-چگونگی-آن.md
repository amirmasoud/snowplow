---
title: >-
    بخش ۱۵۷ - رفتن سام به درگاه شدید و چگونگی آن
---
# بخش ۱۵۷ - رفتن سام به درگاه شدید و چگونگی آن

<div class="b" id="bn1"><div class="m1"><p>به ره بود چشم سپهدار سام</p></div>
<div class="m2"><p>ندانست تا صبح او شد چو شام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که فرهنگ جنی درآمد برش</p></div>
<div class="m2"><p>خبر داد از پهلو و لشکرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که شاپور را بند شد پا و جسم</p></div>
<div class="m2"><p>دگر گرد قلواد شد در طلسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه داستان‌ها سراسر بگفت</p></div>
<div class="m2"><p>از آن گفتنش سام شد در شگفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بپیچید از آن گفته سام سوار</p></div>
<div class="m2"><p>که برگشته شد بخت آن نامدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گهی بهر قلواد در شور بود</p></div>
<div class="m2"><p>گهی دل پر از خون شاپور بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرانگشت خایید و افسوس خورد</p></div>
<div class="m2"><p>که صد حیف ازین هر دو سالار کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که بیچاره گشتند در کارزار</p></div>
<div class="m2"><p>به خیره شدند کشته در روزگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دریغ از جهاندیده قلواد شیر</p></div>
<div class="m2"><p>که از تیغ او مهر گشتی زریر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که ناگه درافتادش از دهر اسم</p></div>
<div class="m2"><p>گرفتار گردید اندر طلسم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدو گفت رحمان جنی که هیچ</p></div>
<div class="m2"><p>پی جان قلواد جان را مپیچ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که اندر طلسمش نیاید گزند</p></div>
<div class="m2"><p>تو دل را مدار هیچ ازین دردمند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که من دیده‌ام در شمار سپهر</p></div>
<div class="m2"><p>نبریده زو چرخ گردنده مهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گشاد طلسمات در دست توست</p></div>
<div class="m2"><p>بلندی جادوگران پست توست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه آلت شاه جمشید جم</p></div>
<div class="m2"><p>تو برداری از گنج او بیش و کم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرفتار بودست رضوان ما</p></div>
<div class="m2"><p>وز آن آتش هجر بر جان ما</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی دیو آنجا نگهبان بود</p></div>
<div class="m2"><p>که در سحر استاد دیوان بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بود نام شومش غزنکان دیو</p></div>
<div class="m2"><p>که هستند ازو جادویان در غریو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برادر بود او به روئینه تن</p></div>
<div class="m2"><p>نبینی چو او دیو در انجمن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بباید ورا کشت از تیغ جم</p></div>
<div class="m2"><p>که گردد ازو تیغ عنقا دژم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو رفتی من از پی همی دخترم</p></div>
<div class="m2"><p>فرستم به دانش تو را چاکرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که شمسه تو را رهنمون آمدی</p></div>
<div class="m2"><p>مگر بخت جادو نگون آمدی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ازو سام فرخنده دل شاد شد</p></div>
<div class="m2"><p>به ماننده تیغ پولاد شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بپوشید آنگاه ساز نبرد</p></div>
<div class="m2"><p>پی جان قلواد رخسار زرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درآمد به هامون به پشت غراب</p></div>
<div class="m2"><p>عنان داد بر اسب دل پرشتاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>روان شد به خرگاه جنگی شدید</p></div>
<div class="m2"><p>که بیند چگونه است دیو پلید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وز آن رو درآمد همی قهقهام</p></div>
<div class="m2"><p>که شاپور پیچیده بر خم خام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>درآورد او را در آن بارگاه</p></div>
<div class="m2"><p>چو شیری که آید به نخجیرگاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدان سان شدید پلیدش بدید</p></div>
<div class="m2"><p>بپیچید از خشم و گفت ای پلید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو از ما چرا روی برگاشتی</p></div>
<div class="m2"><p>خدائی شداد بگذاشتی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برفتی و گشتی تو یزدان پرست</p></div>
<div class="m2"><p>چرا باز کردی ز شداد دست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چه دیدی تو از داور آسمان</p></div>
<div class="m2"><p>که گشتی ز شدادیان بدگمان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو را خال باشد همی قهقهام</p></div>
<div class="m2"><p>برفتی و گشتی تو در نزد سام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پس آنگه به من جنگجو آمدی</p></div>
<div class="m2"><p>ز یزدان پر از گفت‌گو آمدی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همه نام شدادیان را به باد</p></div>
<div class="m2"><p>بدادی نترسیدی از بیم عاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بیا پوزش آور به یزدان مگرد</p></div>
<div class="m2"><p>رگ خون گرمت به گردان چه کرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو را پهلوانی و لشکر دهم</p></div>
<div class="m2"><p>درفش سواران کشور دهم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مبر نام یزدان دگر در جهان</p></div>
<div class="m2"><p>مکن خویشتن را تو از گمرهان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به پاسخ بدو گفت شاپور شیر</p></div>
<div class="m2"><p>نترسم ز شداد و از دار و گیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سرم در ره پاک یزدان بود</p></div>
<div class="m2"><p>به مهرش کجا با کم از جان بود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو هر چیز خواهی بکن کم مکن</p></div>
<div class="m2"><p>که من رو نگردانم از این سخن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خدای جهانم همی یاور است</p></div>
<div class="m2"><p>که روزی ده بندگان یکسرست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز شاپور نام خدا چون شنید</p></div>
<div class="m2"><p>بپیچید چون مار بر خود شدید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به تندی چنین گفت با قهقام</p></div>
<div class="m2"><p>مترس هیچ از زور بازوی سام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همین دم ورا زنده بر دار کن</p></div>
<div class="m2"><p>میان دلیران ورا خوار کن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زبانش برون کن کنون از دهن</p></div>
<div class="m2"><p>که عبرت پذیرد ازو انجمن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بخندید شاپور دیوانه‌وار</p></div>
<div class="m2"><p>که ای کافر گمره دیوسار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نیاری بریدن سر مو مرا</p></div>
<div class="m2"><p>نباشد درین کار آهو مرا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مرا ایزد پاک جان آفرین</p></div>
<div class="m2"><p>نگهدار باشد به روی زمین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>درین گفتگو بود شاپور شیر</p></div>
<div class="m2"><p>که سام دلاور درآمد دلیر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>پیاده شد از اسب چون رزمساز</p></div>
<div class="m2"><p>درآمد به خرگه گو سرفراز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یکی بارگه دید سر بر سپهر</p></div>
<div class="m2"><p>برو قبه زر نمودار مهر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همه عادیان را سپهبد بدید</p></div>
<div class="m2"><p>نشسته بر آن تخت جنگی شدید</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>همه چهارصد کافر بدلقا</p></div>
<div class="m2"><p>نشسته در آن بزم چون اژدها</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>یکی زشت‌رو بود کوراب نام</p></div>
<div class="m2"><p>نشسته ابر کرسی لعل فام</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بدو سام گفتا که ای رزمخواه</p></div>
<div class="m2"><p>مرا جای ده اندرین بزمگاه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>که دارم یکی گفتگو با شدید</p></div>
<div class="m2"><p>به پاسخ درآیم به گفت و شنید</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>برآشفت کوراب با سام یل</p></div>
<div class="m2"><p>بغرید بر سام همچون اجل</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به دشنام بگشاد ناگه زبان</p></div>
<div class="m2"><p>به سود و زیان گشت جانش زیان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز جا جست و بر پهلوان حمله کرد</p></div>
<div class="m2"><p>ز گرمی درآمد به گفتار سرد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>برآشفت ازو سام پرخاشخر</p></div>
<div class="m2"><p>یکی مشت زد بر سر بدگهر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>که مغزش فرو ریخت در بارگاه</p></div>
<div class="m2"><p>نشست از بر جای او رزمخواه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>شدیدش چنان زور بازو بدید</p></div>
<div class="m2"><p>رخش گشت از بیم جان شنبلید</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بتندید کایرانی شوربخت</p></div>
<div class="m2"><p>نبینی مرا بر نشسته به تخت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دلیری نمائی به گردان کین</p></div>
<div class="m2"><p>بویژه دلیران مغرب زمین</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نداری ز من شرم و آزرم هیچ</p></div>
<div class="m2"><p>به خون ریختن دست داری بسیچ</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دلیری بکشتی که اندر جهان</p></div>
<div class="m2"><p>یک یموی او به ز شاهنشهان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز بهر کئی نیز در جستجو</p></div>
<div class="m2"><p>مرادی که داری هم ایدر بگو</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نشاید همین دم ازین عادیان</p></div>
<div class="m2"><p>دلیری ز گردان شدادیان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به کینه به سوی تو یازند چنگ</p></div>
<div class="m2"><p>نفش در گلوی تو سازند تنگ</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بگفتا منم سام نیرم نژاد</p></div>
<div class="m2"><p>کمر بسته بر رزم شداد عاد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بدان آمدم سوی مغرب سپاه</p></div>
<div class="m2"><p>به فرمان رای منوچهر شاه</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>که بندم دو بازوی شداد عاد</p></div>
<div class="m2"><p>شدید بداندیش بی‌دین و داد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دوانم پیاده به ایران زمین</p></div>
<div class="m2"><p>به فرمان یزدان جان‌آفرین</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>منم آن که دو رخش کشتم به آب</p></div>
<div class="m2"><p>شد از من بهشتش سراسر خراب</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>دگر آنکه پیش تو مهمان بدم</p></div>
<div class="m2"><p>ابر رسم و آئین یزدان بدم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>روان بزرگان بر آن گواست</p></div>
<div class="m2"><p>که مهمان ابر میزبان پادشاست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چرا جای ننمود و تندی نمود</p></div>
<div class="m2"><p>زبان را به تندی و تیزی گشود</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به من لاجرم خوی آشفته شد</p></div>
<div class="m2"><p>به ناگه به یک مشت من کشته شد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>دلیران مغرب چو سام گزین</p></div>
<div class="m2"><p>بدیدند گشتند اندوهگین</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بدان تندگفتار و آن زهر چشم</p></div>
<div class="m2"><p>بر ابروی پرچین و پر قهر و خشم</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>شگفتی بماندند از آن رزمخواه</p></div>
<div class="m2"><p>بکردند پنهان برو بر نگاه</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>به ناگه ز جا خاست جنگی خشاش</p></div>
<div class="m2"><p>همه راز مردی خود کرد فاش</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>که دادار مغرب بود سخت‌گیر</p></div>
<div class="m2"><p>که روبه شود پیش او نره شیر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>یکی بنده‌اش عوج باشد به جنگ</p></div>
<div class="m2"><p>ز دریای اخضر برآرد نهنگ</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ورا چار فرسنگ بالا بود</p></div>
<div class="m2"><p>یکی فرسخش نیز پنها بود</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>دو پا بر زمین و سرش بر سماک</p></div>
<div class="m2"><p>گر او را ببینی شوی زهره‌چاک</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ازین رزم در دم بگردان عنان</p></div>
<div class="m2"><p>نشاید کز ایشان ببینی زیان</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بدو گفت سام یل ای بدگهر</p></div>
<div class="m2"><p>همه بندگانیم بر دادگر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>که ایزد یکی باشد اندر جهان</p></div>
<div class="m2"><p>ازو گشته پیدا کهان و مهان</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>اگر عوج و گر چرخ و گر مهر و ماه</p></div>
<div class="m2"><p>ازو یافته در جهان دستگاه</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ندانم جز او در جهان کردگار</p></div>
<div class="m2"><p>که پروردگار است بر مور و مار</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ازو آتش و خاک و آبست و باد</p></div>
<div class="m2"><p>کمینه بود بنده شداد عاد</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>که او داده بد افسر و گنج و تخت</p></div>
<div class="m2"><p>ز دادار برگشت برگشته بخت</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>مرا ایزد از بهر آن آفرید</p></div>
<div class="m2"><p>که شداد بیدار بودش شدید</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>سراسر به شمشیر سازم دو نیم</p></div>
<div class="m2"><p>نداده به گیتی مرا ترس و بیم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>خشاش این سخن را ازو گوش کرد</p></div>
<div class="m2"><p>ز کینه همه خون او جوش کرد</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>برآورد شمشیر زهرآبدار</p></div>
<div class="m2"><p>درآمد به پیکار سام سوار</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>بدو گفت کای بدرگ بدگهر</p></div>
<div class="m2"><p>بگیر از کفم تیغ پرخاشخر</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>بگفت و بیازید آنگاه چنگ</p></div>
<div class="m2"><p>بجوشید برسان غران پلنگ</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>برون کرد تیغ از کفش شیرمرد</p></div>
<div class="m2"><p>بدو حمله آورد گرد نبرد</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بزد بر سرش پهلوان گزین</p></div>
<div class="m2"><p>که از هر دو پایش برون شد ز کین</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>به دو نیم شد گرد جنگی خشاش</p></div>
<div class="m2"><p>همه راز مردی خود کرد فاش</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بلرزید از بیم بر خود شدید</p></div>
<div class="m2"><p>چو آن زور و پیکار را بنگرید</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>برادر ورا نام کورنگ بود</p></div>
<div class="m2"><p>که پیوسته در کینه و جنگ بود</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>چو زان سان برادرش را کشته دید</p></div>
<div class="m2"><p>برو بخت بیدار برگشته دید</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>ز جا جست و از کین بغل برگشاد</p></div>
<div class="m2"><p>مدد خواست از فر شداد عاد</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>غریوان درآمد به پیکار سام</p></div>
<div class="m2"><p>برو برخروشید و برگفت نام</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>چو سام آنچنان دید در انجمن</p></div>
<div class="m2"><p>نکرد از بد و نیک با وی سخن</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>همان تیغ زد مرد را بر میان</p></div>
<div class="m2"><p>که از زخم او شد تنش پرنیان</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>چو مردی نمود اندر آن بارگاه</p></div>
<div class="m2"><p>بدو حمله کردند مغرب سپاه</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بدو چهارصد تیغ افراشتند</p></div>
<div class="m2"><p>به گردون همی نعره بگذاشتند</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>دلاور درآمد به پیکارشان</p></div>
<div class="m2"><p>همان زخم می‌کرد در کارشان</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>برآمد غریو ده و دار گیر</p></div>
<div class="m2"><p>روان گشت در دم یکی آبگیر</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>همه بازگشتند از موج زن</p></div>
<div class="m2"><p>سپهبد نهنگی در آن انجمن</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>به ناگاه دریای خون شد روان</p></div>
<div class="m2"><p>بسی عادیان را سرآمد زمان</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>صد و سی تن از تیغ او کشته شد</p></div>
<div class="m2"><p>به خون تخت و خرگاه آغشته شد</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>به ناگه برخاست دستور پیر</p></div>
<div class="m2"><p>درافتاد در پای سالار شیر</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>به پوزش درآمد که ای نامدار</p></div>
<div class="m2"><p>ز خون بارگه شد همه رودبار</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>یکی رزم کردی که اندر جهان</p></div>
<div class="m2"><p>نکرده کس از آشکار و نهان</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>ولی چنگ از جنگ کوتاه کن</p></div>
<div class="m2"><p>ازین پیر فرسوده بشنو سخن</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>بس است این که کشتی درین بارگاه</p></div>
<div class="m2"><p>زمین و زمان گشت یکسر سیاه</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>ولی جنگ در روی میدان خوش است</p></div>
<div class="m2"><p>چو نی جان گردان رزم آتش است</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>ببوسید مر سام را دست و پا</p></div>
<div class="m2"><p>که کوتاه کن رزم جنگ آزما</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>برین برنهادند انجام جنگ</p></div>
<div class="m2"><p>که فردا به میدان درآید نهنگ</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>به دستور مغرب چنین گفت سام</p></div>
<div class="m2"><p>که شاپور را دست بگشا زخام</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>بدان تا من این رزم کوته کنم</p></div>
<div class="m2"><p>زمانی به آسودگی ره کنم</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>گشودند شاپور را هر دو دست</p></div>
<div class="m2"><p>پس آنگه به زین دلیری نشست</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>برون رفت سالار ایران سپاه</p></div>
<div class="m2"><p>بیامد به نزدیک تسلیم شاه</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>برو سر به سر داستان بازگفت</p></div>
<div class="m2"><p>که تسلیم از رزم او شد شگفت</p></div></div>