---
title: >-
    بخش ۲۱ - بی‌تابی کردن سام نریمان از حرمان پریدخت
---
# بخش ۲۱ - بی‌تابی کردن سام نریمان از حرمان پریدخت

<div class="b" id="bn1"><div class="m1"><p>به ناکام بر پشت جرمه نشست</p></div>
<div class="m2"><p>به خون جگر شست از خویش دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سرو خرامان برآورد خم</p></div>
<div class="m2"><p>زده بر فلک ز آتش دل علم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخ آورد در دم سوی نیمروز</p></div>
<div class="m2"><p>همی تاخت از صبح تا نیمروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه راهی بدید و نه رهبر به دست</p></div>
<div class="m2"><p>نه دل برقرار و نه دلبر به دست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در اندیشه کایا چه پیش آیدم</p></div>
<div class="m2"><p>اگر جان برآید کنون شایدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب فرقتش چون به پایان برم</p></div>
<div class="m2"><p>ز دریای عشقش کجا جان برم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمانه به هر صورتم خون خورد</p></div>
<div class="m2"><p>ازین صورتم تا چه نقش آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر ار درنیارد پری پیکرم</p></div>
<div class="m2"><p>ندانم چه آرد قضا بر سرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازینم چه گویند اهل شناخت</p></div>
<div class="m2"><p>که نقش رخش دید و جان را بباخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرا جان نکردم همان دم نثار</p></div>
<div class="m2"><p>که بستم دل خسته در زلف یار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برین گونه می‌گفت و خون می‌گریست</p></div>
<div class="m2"><p>چه گویم در آن لحظه چون می‌گریست</p></div></div>