---
title: >-
    بخش ۶۲ - برگشتن سام از پای قصر و سر به کوه و بیابان گذاشتن
---
# بخش ۶۲ - برگشتن سام از پای قصر و سر به کوه و بیابان گذاشتن

<div class="b" id="bn1"><div class="m1"><p>عنان برزده سر به صحرا نهاد</p></div>
<div class="m2"><p>سرشکش روان رو به دریا نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دست دلش دست بر دل بماند</p></div>
<div class="m2"><p>ز خون جگر پای در گل بماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان آتشی از جگر برفروخت</p></div>
<div class="m2"><p>که از ماه تا برج ماهی بسوخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوا کله عنبرین بسته بود</p></div>
<div class="m2"><p>زمانه به انفاس رو شسته بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب از ابر خم بر خم افکند جعد</p></div>
<div class="m2"><p>شده کوس گردون‌بر از بانگ رعد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوا هر نفس گشته کافوربیز</p></div>
<div class="m2"><p>زمین هر طرف گشته کافورخیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زده برق بر برق کهسار تیغ</p></div>
<div class="m2"><p>روان گشته طوفان آبی ز میغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تبیره زنان رعد در دمدمه</p></div>
<div class="m2"><p>دم افسرده‌تر گشته مردم همه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زده باد بهمن دم از زمهریر</p></div>
<div class="m2"><p>فرو رفته گیتی به دریای قیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان رفته از برق و باران به باد</p></div>
<div class="m2"><p>شبی زان صفت روزی کس مباد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه راهی که آن را بود منزلی</p></div>
<div class="m2"><p>نه بحری که پیدا بود ساحلی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همی سام می‌رفت رو در قفا</p></div>
<div class="m2"><p>ز دلبر جدا مانده وز دل جدا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دمش بد ز سرما فسرده نفس</p></div>
<div class="m2"><p>چو خر در دجل بازمانده فرس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه روئی که روی آورد سوی یار</p></div>
<div class="m2"><p>نه راهی که بیرون رود از دیار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه صبری که برگردد از یار خویش</p></div>
<div class="m2"><p>نه هوشی که گردد پی کار خویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روان کرده در چشم‌ها چشم‌ها</p></div>
<div class="m2"><p>ولیکن روان کرده در ره رها</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گه از دیده زورق فکنده در آب</p></div>
<div class="m2"><p>گه از سینه آتش زدی در سحاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گهی باره در رود راندی ز خشم</p></div>
<div class="m2"><p>گهی گفتی و خون فشاندی ز چشم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز بس غصه با درد دل بود جفت</p></div>
<div class="m2"><p>ز غم این نفس ابتدا کرد و گفت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ایا ابر تردامن تیره دل</p></div>
<div class="m2"><p>ز دست توام پا فروشد به گل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز تردامنی آب خود ریختی</p></div>
<div class="m2"><p>ولی آتش از جانم انگیختی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سبک بادبان برکشیدی به ماه</p></div>
<div class="m2"><p>شدی همچو گیسوی یارم سیاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چرا تیره گرنه تو بخت منی</p></div>
<div class="m2"><p>چرا سست و طرار و تردامنی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به در پرده‌ات پرده من مدر</p></div>
<div class="m2"><p>مکن سرکشی از سرم درگذر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرا از تو آخر چه آید به سر</p></div>
<div class="m2"><p>که می بینمت سخت سستی و تر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به هر کوی پائی به سنگ آیدت</p></div>
<div class="m2"><p>که آن سنگدل مهر ننمایدت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سرشکت چرا راه دریا گرفت</p></div>
<div class="m2"><p>مگر کارت از چرخ بالا گرفت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو می‌گرئی و برق می‌خنددت</p></div>
<div class="m2"><p>چه گرئی که کس گریه نپسنددت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ترا بر هوا کار بر هم فتاد</p></div>
<div class="m2"><p>کسی چون تو یا رب هوائی مباد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز باران درم ریختی بر سرم</p></div>
<div class="m2"><p>سیه‌رو چرائی چو داری کرم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه بهمن و دم را ز بهمن زنی</p></div>
<div class="m2"><p>چو ذالی و بی‌مهر و تر دامنی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گه از رعد دل در خروش آیدت</p></div>
<div class="m2"><p>گه از رشک دریا به جوش آیدت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>روی همچو لوکان سر در هوا</p></div>
<div class="m2"><p>کف از لب فشانی بگو تا کجا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گهی دم ز کافور بیزی زنی</p></div>
<div class="m2"><p>گهی لاف سیلاب‌خیزی زنی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدین سان که که را گرفتی کمر</p></div>
<div class="m2"><p>که اندازی از زخم تیرش سپر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نگویم که آب روانی کجا</p></div>
<div class="m2"><p>اگر زانکه گویم نباشد روا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر آبی ز دریا برآورده‌اند</p></div>
<div class="m2"><p>ز دریا ترا بر سر آورده‌اند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرا کین همه کام بر دل بماند</p></div>
<div class="m2"><p>ز دست توام پای در گل بماند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو چون برق جستی فتادی چرا</p></div>
<div class="m2"><p>بجستی و بر باد دادی چرا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگر بر دلم رحمت آری نکوست</p></div>
<div class="m2"><p>که برقی است امشب که بر بام اوست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مکن تندی ای باد با روی سرد</p></div>
<div class="m2"><p>فسرده دم و کژرو هرزه گرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>برو گرم دم‌سردی از حد مبر</p></div>
<div class="m2"><p>به بادم مده از سرم درگذر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>غمم همدم و ناله همره بسست</p></div>
<div class="m2"><p>دلم همره و غصه محرم بسست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سرشک ار چه بازش نرانم ز چشم</p></div>
<div class="m2"><p>برآنم که بازش برانم ز چشم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>وگر دمبدم قاصدی بایدت</p></div>
<div class="m2"><p>کزو آب بر روی کار آیدت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ببین کآب چشمم چه سان می‌رود</p></div>
<div class="m2"><p>کز آن آب آب روان می‌رود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دلم چون بدان دل گسل بازماند</p></div>
<div class="m2"><p>دل خسته را دل به دل بازماند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ولیکن همان به که در پیش اوست</p></div>
<div class="m2"><p>که ریش است و او مرهم ریش اوست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز ما عشقبازی نباشد خطا</p></div>
<div class="m2"><p>وز آن ترکتازی نباشد خطا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بدین گونه می‌گفت و می‌راند اسب</p></div>
<div class="m2"><p>ز چشم اشک می‌راند و می‌ماند اسب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نه کس همدش جز غم عشقبار</p></div>
<div class="m2"><p>نه قلواد و نه قلوش غمگسار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو در بند افتاد سام گزین</p></div>
<div class="m2"><p>برفتند هر دو به خاور زمین</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که بودند آن هر دو عاشق به سوز</p></div>
<div class="m2"><p>به خاور رسیدند بر دلفروز</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>یکی مهرافروز را دل بداد</p></div>
<div class="m2"><p>یکی شمسه از راه بردش چو باد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>برفتند هر دو به خاورزمین</p></div>
<div class="m2"><p>که تا لشکر آرند یکسر به چین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در آن شب کجا سام بیدل به راه</p></div>
<div class="m2"><p>همی گفت و می‌راند تا صبح‌گاه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو مرغ سحرخوان فغان برکشید</p></div>
<div class="m2"><p>جهان مژده صبح صادق شنید</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>فلک، میغ را قبه در هم شکست</p></div>
<div class="m2"><p>هوا از دم باد و باران بجست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دریده شد این پرده نیلگون</p></div>
<div class="m2"><p>نهفته شدش طارم سرنگون</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو آن ابر بارنده محمل براند</p></div>
<div class="m2"><p>سیاهی بر آن سبز گلشن نماند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بجنباند مرغ سحر بال را</p></div>
<div class="m2"><p>به جنبش درآورد خلخال را</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز ناگه به سرچشمه‌ای در رسید</p></div>
<div class="m2"><p>چراگاه و مأوای نخجیر دید</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>فرود آمد و اسب در بیشه راند</p></div>
<div class="m2"><p>بر آن چشمه از چشمها خون فشاند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دلش پیش یار و غمش پیش دل</p></div>
<div class="m2"><p>غم دلبرش مرهم ریش دل</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نه در دل که از غم برد جان به در</p></div>
<div class="m2"><p>نه در سر که بردارد از پای سر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چرا گر شده اسب که پیکرش</p></div>
<div class="m2"><p>گذشته ز خون دل آب از سرش</p></div></div>