---
title: >-
    بخش ۵۷ - مناظره کرده پری‌دخت با سام
---
# بخش ۵۷ - مناظره کرده پری‌دخت با سام

<div class="b" id="bn1"><div class="m1"><p>فلک حلقه‌ای از کمر ترکشت</p></div>
<div class="m2"><p>شه سرکش چرخ ترکش کشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه گوئی ز راه دراز آمدم</p></div>
<div class="m2"><p>برو باز شو کز تو باز آمدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه نامی که نامم بدادی به ننگ</p></div>
<div class="m2"><p>مران بر زبانم که دادی به جنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قمررخ رخش را قمررخ نهاد</p></div>
<div class="m2"><p>که رخ بر رخ چون تو فرخ نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برو با نگاری که داری بساز</p></div>
<div class="m2"><p>به زاری بسوز و به خواری بساز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگو کز تو دل برنشاید گرفت</p></div>
<div class="m2"><p>به یک دل دو دلبر نشاید گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا چون تو پسته دهان تنگ نیست</p></div>
<div class="m2"><p>که حاصل ز نام تو جز ننگ نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برو بازپس گرد و ره پیش‌گیر</p></div>
<div class="m2"><p>سر ما نداری سر خویش گیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی مرد سرپنج? عشق تست</p></div>
<div class="m2"><p>که همچون تو قلب آید و نادرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو گویا که با عشق‌بازی کنی</p></div>
<div class="m2"><p>که با هر کسی عشق‌بازی کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برفتی و نرد دغا باختی</p></div>
<div class="m2"><p>زدی مهره لیکن خطا باختی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگاری گرفتی که در خورد تست</p></div>
<div class="m2"><p>به میدان خوبی هم‌آورد تست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کنون رحم کردی و بازآمدی</p></div>
<div class="m2"><p>به بیچارگی چاره‌ساز آمدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من و آرزویت کجا تا کجا</p></div>
<div class="m2"><p>که ناید ز ترک ختائی خطا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو در مهر چون بینمت ناتمام</p></div>
<div class="m2"><p>چگونه پسندم ترا صبح و شام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گهی پاسبانم به زاری کشی</p></div>
<div class="m2"><p>گهی باغبانم به خواری کشی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو راز از دلی می‌گشادم چو اشک</p></div>
<div class="m2"><p>که ازچشم مردم فتادم چو اشک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شدم در هوای تو رسوای دهر</p></div>
<div class="m2"><p>به دیوانگی شهر گشتم به شهر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگرچند روزی فتادی به بند</p></div>
<div class="m2"><p>ولیکن مهی آمدت در کمند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من خسته در بند سودای تو</p></div>
<div class="m2"><p>پریشان چو جعد سمن سای تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه کس همدمم جز دم صبحدم</p></div>
<div class="m2"><p>نه کس همزبانم برون از قلم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به مهر تو زان سر برافراشتم</p></div>
<div class="m2"><p>که سر چون قلم بر خطت داشتم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قلم این زمان از تو سر خواستست</p></div>
<div class="m2"><p>که چو طره کج باختی راستست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دمت آتش است و تو افسرده‌ای</p></div>
<div class="m2"><p>ولی درنگیرد غلط کرده‌ای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مزن دم که با ما نه‌ای هم‌نفس</p></div>
<div class="m2"><p>فسونم مدم زانکه با دست و بس</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه طفلم که گوئی به یک دانه نار</p></div>
<div class="m2"><p>ستانم ازو گوهر شاهوار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرا گر تو گوئی که سروی رواست</p></div>
<div class="m2"><p>ولیکن نیاید به قد تو راست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شکر خایم و تلخ پاسخ نیم</p></div>
<div class="m2"><p>سمن بویم اما قمر رخ نیم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>توئی آنکه از تو بسی فتنه خاست</p></div>
<div class="m2"><p>ور از بنده آزاد گردی رواست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وگر زانکه بالای سر بینیم</p></div>
<div class="m2"><p>به بام آمدم تا چو خور بینیم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>منم ابر گرینده شب تا سحر</p></div>
<div class="m2"><p>بود ابر گرینده بالای سر</p></div></div>