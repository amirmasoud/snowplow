---
title: >-
    بخش ۱۸۱ - کشته شدن خاتوره به دست سام
---
# بخش ۱۸۱ - کشته شدن خاتوره به دست سام

<div class="b" id="bn1"><div class="m1"><p>چو پوشید گردون پلاس سیاه</p></div>
<div class="m2"><p>چو ابروی پیران آینده راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو لشکر جدا گشت از یکدگر</p></div>
<div class="m2"><p>طلایه برون رفت بر دشت و در</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین گفت خاتوره شداد را</p></div>
<div class="m2"><p>که امشب به آتش دهم باد را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمانم ز سام نریمان اثر</p></div>
<div class="m2"><p>برآرم دمار از تن بدگهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه آتش کین ببارم بر او</p></div>
<div class="m2"><p>همه روز بدبختی آرم بر او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سفیده مر او را ببینی بجا</p></div>
<div class="m2"><p>مگر سوخته جمله پرده‌سرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی داستان مانم اندر جهان</p></div>
<div class="m2"><p>که ماند ز من در میان مهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدو گفت شداد کای پرهنر</p></div>
<div class="m2"><p>ببینم چه سازی بدان بدگهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زجا خاست خاتوره اندرسرا</p></div>
<div class="m2"><p>همان دم بیامد به پرده سرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه ساز افسونگری برگرفت</p></div>
<div class="m2"><p>دگر بدنژادیش از سر گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیامد به یک سوی آتش فروخت</p></div>
<div class="m2"><p>همه نفت با قیر و گوگرد سوخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به افسون لب شوم را برگشاد</p></div>
<div class="m2"><p>بر آمد ز روی هوا تند باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی صاعقه در زمان شد پدید</p></div>
<div class="m2"><p>که روی هوا شد ازو ناپدید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جهان را سراسر سیاهی گرفت</p></div>
<div class="m2"><p>همه کار گردون تباهی گرفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنان باد برزد که برخاست سخت</p></div>
<div class="m2"><p>که برکند از ریشه خود درخت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سراپرده‌ها را همه باد برد</p></div>
<div class="m2"><p>تو گفتی که روی زمین برشمرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هیونان و اسبان همه برد باد</p></div>
<div class="m2"><p>کس این گونه بادی ندارد به یاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نگه کرد سوی هوا پهلوان</p></div>
<div class="m2"><p>زمانه از آن باد گشته نوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به ناگه دگر آتش آمد پدید</p></div>
<div class="m2"><p>کزان ابر بارید بر پاک دید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سپهبد فرو ماند از آن تیره میغ</p></div>
<div class="m2"><p>که می‌جست زان برق مانند تیغ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یکی میغ بسته چو قیر سیاه</p></div>
<div class="m2"><p>نه گردون هویدا نه پروین نه ماه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه نوشاد پیدا نه تسلیم شاه</p></div>
<div class="m2"><p>از آن آتش و باد و ابر سیاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برفتند یکسر سوی برزکوه</p></div>
<div class="m2"><p>از آن آتش و باد گشته ستوه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دلاور بنالید بر کردگار</p></div>
<div class="m2"><p>که از تست نیک و بد روزگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همی گفت کای داور آب و خاک</p></div>
<div class="m2"><p>همی جان تو بخشی و سازی هلاک</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز تو آتش و آب آمد پدید</p></div>
<div class="m2"><p>بجز تو دگر جسم و جانم ندید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به هر سو که بینم تو آنجا بوی</p></div>
<div class="m2"><p>مکانت ثری تا ثریا بوی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو دانی همه آشکار و نهان</p></div>
<div class="m2"><p>تو داری زمین و زمان جهان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو را زار هر کس همی روشن است</p></div>
<div class="m2"><p>مر این پیر جادو که در جوشن است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بگفتا منم زار و بیچاره‌ای</p></div>
<div class="m2"><p>گرفتار در دست پتیاره‌ای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به هر سو که بینم غمم بر غم است</p></div>
<div class="m2"><p>غم و رنج و اندوه من درهم است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ازین غم رها ساز جان مرا</p></div>
<div class="m2"><p>توانی ده این ناتوان مرا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بگفت و بمالید رخ بر زمین</p></div>
<div class="m2"><p>بسی خواند بر کردگار آفرین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که ناگه درآمد به گوشش سروش</p></div>
<div class="m2"><p>که ای سام بیدار چندین مجوش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چراغ تو روشن شود در جهان</p></div>
<div class="m2"><p>سرافکنده پیشت کهان و مهان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز آتش مکن کار بر خویش تنگ</p></div>
<div class="m2"><p>که خاتوره آراسته ریو و رنگ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو او را بکشتی رها یافتی</p></div>
<div class="m2"><p>رها از دم اژدها یافتی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خدای جهان را همی یاد دار</p></div>
<div class="m2"><p>دگر کار دشمن همه باد دار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو بشنید برجست فرخنده سام</p></div>
<div class="m2"><p>لبی پر ز خنده دلی شادکام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همان دم برآمد به پشت غراب</p></div>
<div class="m2"><p>دلی پر ز کینه دو ابرو به تاب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گرفته به کف تیغ جمشید جم</p></div>
<div class="m2"><p>همی رفت مانند شیر دژم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همی گفت جادو همی جست باز</p></div>
<div class="m2"><p>که آرد سر بدکنش را به گاز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که آمد به ناگاه فرهنگ دیو</p></div>
<div class="m2"><p>ثنا گفت بر پهلو گرد نیو</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدو گفت کای سام روشن‌گهر</p></div>
<div class="m2"><p>نگه کن سوی کوه جادو نگر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>که خاتوره مام عوج پلید</p></div>
<div class="m2"><p>چنین ابر و آتش ازو شد پدید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به انگشت بنمود پتیاره را</p></div>
<div class="m2"><p>مر آن زشت جادوی خونخواره را</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نگه کرد سامش بدان تیغ کوه</p></div>
<div class="m2"><p>نشسته ز سحرش زمانه ستوه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>برانگیخت چرمه ز جا همچو شیر</p></div>
<div class="m2"><p>روان شد به نزدیک آن گنده پیر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>فرود آمد از پشت اسب سیاه</p></div>
<div class="m2"><p>برآمد از آن کوه گرد سیاه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سرش همچو گنبد تنش چون منار</p></div>
<div class="m2"><p>همی قی ازو کرد مردار خوار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به تن موی او همچو یال ستور</p></div>
<div class="m2"><p>ز زشتی نتابید بر روش هور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دو بینیش چون کشتی سرنگون</p></div>
<div class="m2"><p>که افتاده باشد به دریای خون</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>لبانش به ماننده تنگ نیل</p></div>
<div class="m2"><p>دو گوشش به ماننده گوش پیل</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نشسته به ماننده برزکوه</p></div>
<div class="m2"><p>تن کوه از پیکر او ستوه</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>برش نفت و آتش ز گوگرد و قیر</p></div>
<div class="m2"><p>رخ مرگ از اندیشه او زریر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو سام نریمان مر او را بدید</p></div>
<div class="m2"><p>شگفتی فرو ماند از آن پاک دید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بنالید بر داور آسمان</p></div>
<div class="m2"><p>کزو گشت پیدا زمین و زمان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چنین زشت پتیاره ناپاک دین</p></div>
<div class="m2"><p>ازو گشت پیدا دلی پر زکین</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بکی نعره زد سام پرخاشخر</p></div>
<div class="m2"><p>که ای زشت پتیاره بدگهر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چه باشی درین که به افسونگری</p></div>
<div class="m2"><p>ببین تیغ تیزم که جان بسپری</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نترسی ز دادار پروردگار</p></div>
<div class="m2"><p>که آتش بباری به مردان کار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ندانی مگر نام من در مصاف</p></div>
<div class="m2"><p>که لرزد ز تیغم دل کوه قاف</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چنین گفت خاتوره نام من است</p></div>
<div class="m2"><p>ز نیرنگ کرکس به دام من است</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به گیتی منم مام عوج عنق</p></div>
<div class="m2"><p>که یازم چو چنگال را بر افق</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>جهان را به یک دم به هم برزنم</p></div>
<div class="m2"><p>منم سرو میدان اگرچه زنم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هژبر دلیریم پیچان شود</p></div>
<div class="m2"><p>ز زورم تن کوه بیجان شود</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ندیدی مرا تو به آوردگاه</p></div>
<div class="m2"><p>از آن برنهادی به مردی کلاه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو بشنید ازو سام یل بردمید</p></div>
<div class="m2"><p>لب پهلوانی به دندان گزید</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>کشید از میان او یمانی پرند</p></div>
<div class="m2"><p>درآمد چو طهمورث دیوبند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>برو حمله آورد سام سوار</p></div>
<div class="m2"><p>که خاتوره برجست در کوهسار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>یکی گاو کوهی گرفته به دست</p></div>
<div class="m2"><p>درآمد به حمله سوی پیل مست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>برانداخت بر سوی فرخنده سام</p></div>
<div class="m2"><p>سپر بر سر آورد آن نیکنام</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چنان بر سپر زد که گردید خورد</p></div>
<div class="m2"><p>فرو ریخت بر گرد سالار گرد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>سپهبد بنالید بر دادگر</p></div>
<div class="m2"><p>درآمد به پیکار آن بدگهر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بزد تیغ و افکند دستش به خاک</p></div>
<div class="m2"><p>ز خاتوره گفتی برآمد هلاک</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به یک دست بر پهلوان حمله کرد</p></div>
<div class="m2"><p>از آن کوه برخاست بر چرخ گرد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>از آن کوه افکند آن شوربخت</p></div>
<div class="m2"><p>به یک دست برکند از بن درخت</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>درانداخت بر تارک پهلوان</p></div>
<div class="m2"><p>که از ضرب دستش جهان شد نوان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>درخت گشن شاخ را بر سپر</p></div>
<div class="m2"><p>چنان زد که بشکست بر یکدگر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>سر پهلوان را نیامد زیان</p></div>
<div class="m2"><p>به دشنام بگشاد آنگه زبان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بدو گفت کای جادو زشت کار</p></div>
<div class="m2"><p>همین دم برآرم ز جانت دمار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>همین دم تنت را به شمشیر تیز</p></div>
<div class="m2"><p>در آورد سازم همی ریزه ریز</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>دگر پورت آن شوم برگشته بخت</p></div>
<div class="m2"><p>به میدان کنم پیکرش لخت لخت</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>براندازم این تخمه عادیان</p></div>
<div class="m2"><p>نمانم دگر نام شدادیان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بگفت و همان حربه نامدار</p></div>
<div class="m2"><p>که از شاه جم بد بدو یادگار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>درآورد از فره دادگر</p></div>
<div class="m2"><p>یکی حمله آورد پرخاشخر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بدان پای خاتوره افکند زیر</p></div>
<div class="m2"><p>خروشید آن زشت دل‌ناپذیر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بدو گفت کای سام نیرم‌نژاد</p></div>
<div class="m2"><p>به پورم دهد گر خبر زان که باد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>که خاتوره از تیغ سام سوار</p></div>
<div class="m2"><p>بشد کشته از تیغ آن نامدار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>اگر چرخ گردی تنت بر درد</p></div>
<div class="m2"><p>وگر کوه باشی تو را بشکرد</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>نیابی ز چنگال پورم امان</p></div>
<div class="m2"><p>همان دم تو را بر سر آرد زمان</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بدو گفت سامش نترسم ز کس</p></div>
<div class="m2"><p>به فرمان یزدان فریاد رس</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>برم عوج ماننده پشه نیست</p></div>
<div class="m2"><p>ز پیکار او هیچم اندیشه نیست</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>چنانش کنم کار در دشت زار</p></div>
<div class="m2"><p>که گرید بدو عرصه روزگار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بگفت و دگر پایش افکند پست</p></div>
<div class="m2"><p>خروشید آنگه چو پیلان مست</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بشد جان خاتوره زو دردناک</p></div>
<div class="m2"><p>بیفتاد بیچاره بر روی خاک</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>به بالای او نیم فرسنگ بیش</p></div>
<div class="m2"><p>تنش بود پر موی چون گاومیش</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>به تیغ چهارم یل نامور</p></div>
<div class="m2"><p>چنان زد ابر فرق آن بدگهر</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>که نیمی سرش بر زمین اوفتاد</p></div>
<div class="m2"><p>به سوی جهنم بشد بدنژاد</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>جهانجو به پیش جهان‌آفرین</p></div>
<div class="m2"><p>فراوان بمالید رخ بر زمین</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>همی گفت کای داور مور و مار</p></div>
<div class="m2"><p>جهاندار بیدار پروردگار</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>مرا فرهی ده به میدان جنگ</p></div>
<div class="m2"><p>که بر دیو جادو کنم کار تنگ</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ولیکن دگر دشمنی هست سخت</p></div>
<div class="m2"><p>که از زور او کوه شد لخت لخت</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>به میدان مرا کامکاری دهی</p></div>
<div class="m2"><p>که یابم ز پیکار او فرهی</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>پس آنگه پری‌دخت فغفور چین</p></div>
<div class="m2"><p>به دست آورم دل بشویم ز کین</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>نشست از بر اسب و آمد به زیر</p></div>
<div class="m2"><p>سوی لشکر آمد غریوان چو شیر</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بیامد به لشکر همه باز گفت</p></div>
<div class="m2"><p>بماندند گردان ازو در شگفت</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>به پهلو همی آفرین خواندند</p></div>
<div class="m2"><p>ورا پهلوان زمین خواندند</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>که هرگز مبیناد چشمت بدی</p></div>
<div class="m2"><p>بود یار تو فره ایزدی</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ببین تا دگر پیر گوهرفروش</p></div>
<div class="m2"><p>گهر ریخت در نطع فیروزه‌پوش</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>برافروخت بازار از نور ماه</p></div>
<div class="m2"><p>چو شمعی به دست غلام سیاه</p></div></div>