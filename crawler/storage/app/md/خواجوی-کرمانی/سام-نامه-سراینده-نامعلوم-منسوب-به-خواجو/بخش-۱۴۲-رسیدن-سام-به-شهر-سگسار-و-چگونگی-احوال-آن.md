---
title: >-
    بخش ۱۴۲ - رسیدن سام به شهر سگسار و چگونگی احوال آن
---
# بخش ۱۴۲ - رسیدن سام به شهر سگسار و چگونگی احوال آن

<div class="b" id="bn1"><div class="m1"><p>به کشتی درآمد سپهدار سام</p></div>
<div class="m2"><p>چو غواص در آب کرد او مقام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همان چار کشتی برافراختند</p></div>
<div class="m2"><p>به دریای اخضر همی تاختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی باد برخاست دلخواهشان</p></div>
<div class="m2"><p>بیاراست گفتی همی کارشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو شب راند کشتی چو ملاحیان</p></div>
<div class="m2"><p>دوم روز بگشود او بادبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سواد یکی شهر آمد پدید</p></div>
<div class="m2"><p>شتابان چو کشتی به ساحل رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ساحل یلان بار انداختند</p></div>
<div class="m2"><p>به سوی جزیره یکی تاختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درآمد در آن شهر شاپور شیر</p></div>
<div class="m2"><p>سگان دید چون اژدهای دلیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو سر دید از آن مردمان یک به یک</p></div>
<div class="m2"><p>به یک روی آدم به یک روی سگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به صورت چنان سگ به گفتار زشت</p></div>
<div class="m2"><p>تو گفتی ز دوزخ بدیشان سرشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو ایشان بدیدند شاپور را</p></div>
<div class="m2"><p>مر آن نامور شیربازو را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شگفتی به شاپور درماندند</p></div>
<div class="m2"><p>ورا پیش سالارشان خواندند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفتند با شاه کردار او</p></div>
<div class="m2"><p>که یک روی دارد یکی نامجوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نباشد نشان سگی در تنش</p></div>
<div class="m2"><p>ز چرم پلنگ است پیراهنش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو این گفته بشنید ازیشان کلاب</p></div>
<div class="m2"><p>بیارید گفتا ورا با شتاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ببردند شاپور در پیش شاه</p></div>
<div class="m2"><p>درآمد به خرگاه آن کینه خواه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی اهرمن دید تن همچو کوه</p></div>
<div class="m2"><p>زمین زیر پایش سراسر ستوه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سری چون سر سگ سری آدمی</p></div>
<div class="m2"><p>ندید هیچ با آدمش مردمی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز شاپور پرسید چون آمدی</p></div>
<div class="m2"><p>همانا که ایدر به خون آمدی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه جوئی تو در شهر سگسار شاه</p></div>
<div class="m2"><p>مگر اوفتادی به ناگه ز راه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ندانسته ایدر فتادت گذر</p></div>
<div class="m2"><p>به چنگال این مردم کینه ور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فکندی به دندان ما جان خویش</p></div>
<div class="m2"><p>ندیدی درین درد دامان خویش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بسی دیر سالست تا آدمی</p></div>
<div class="m2"><p>نخوردیم و هستیم از ایدر غمی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کنونت بدریم از یکدگر</p></div>
<div class="m2"><p>که دیگر نیارد کس ایدر گذر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدو گفت شاپور کای مرد شوم</p></div>
<div class="m2"><p>همین دم خرابست این مرز و بوم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نبینی ز سگسار ای شه یکی</p></div>
<div class="m2"><p>نمانند بر جا یکی بیشکی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سپهدار سام است کآید ز راه</p></div>
<div class="m2"><p>نهنگ دمان گرد ایران پناه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شتابد به پیکار شداد و عاد</p></div>
<div class="m2"><p>براندازد آن تیره بدنهاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به فرمان یزدان جان آفرین</p></div>
<div class="m2"><p>بگیرد همه مرز مغرب زمین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کلاب این سخن را چو در گوش کرد</p></div>
<div class="m2"><p>به مانند دریا دلش جوش کرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به لشکر چنین گفت کاندر دمید</p></div>
<div class="m2"><p>تنش را به چنگال در خون کشید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو لشکر شنیدند مانند کوه</p></div>
<div class="m2"><p>بدو حمله کردند چندین گروه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بغرید شاپور مانند مست</p></div>
<div class="m2"><p>سوی چوبدستش درآورد دست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی را بزد چوب بر فرق سر</p></div>
<div class="m2"><p>که چون توتیا گشت بر یکدگر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به سگ صورتان حمله آورد شیر</p></div>
<div class="m2"><p>میان سگان نعره برزد دلیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پرآواز سگ شد همه رزمگاه</p></div>
<div class="m2"><p>زمین از سگان گشت یکسر سیاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ولیکن ازیشان بیفکند چهل</p></div>
<div class="m2"><p>سراسر زمین گشت از خون چو گل</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سیه کرد از گرد پی هور را</p></div>
<div class="m2"><p>به دندان گزیدند شاپور را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نبد خسته شاپور فرخنده شیر</p></div>
<div class="m2"><p>خردمند و دریادل و جان‌پذیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سگان گرد او نعره برداشتند</p></div>
<div class="m2"><p>همه نعره از چرخ بگذاشتند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو خورشید برگشت بیگاه شد</p></div>
<div class="m2"><p>سپهبد ز کارش چو آگاه شد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به ملاح گفت ای جهانجوی شیر</p></div>
<div class="m2"><p>خردمند و بیدار و بسیار دیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همانا که شاپور گو بسته شد</p></div>
<div class="m2"><p>و یا وی ز سگساریان خسته شد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شوم تا ببینم که چونست جنگ</p></div>
<div class="m2"><p>چرا کرد شاپور چندین درنگ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به توفیق یزدان پروردگار</p></div>
<div class="m2"><p>به سگسار سازم همه روز تار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بگفت و بپوشید جنگی زره</p></div>
<div class="m2"><p>بزد بر دو ابروی پرچین گره</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کمر بست و بگرفت نیزه به دست</p></div>
<div class="m2"><p>سپر زد ابر گفت چون پیل مست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نشست آن دلاور به زین غراب</p></div>
<div class="m2"><p>بیامد غریوان به سوی کلاب</p></div></div>