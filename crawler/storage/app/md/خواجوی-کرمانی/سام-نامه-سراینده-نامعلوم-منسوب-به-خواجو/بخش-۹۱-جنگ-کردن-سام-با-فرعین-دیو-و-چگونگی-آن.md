---
title: >-
    بخش ۹۱ - جنگ کردن سام با فرعین دیو و چگونگی آن
---
# بخش ۹۱ - جنگ کردن سام با فرعین دیو و چگونگی آن

<div class="b" id="bn1"><div class="m1"><p>ازین پس سخن گویم از سام نیو</p></div>
<div class="m2"><p>همان هم ز پیکار فرعین دیو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو فرعین لشکر به کشتی نشاند</p></div>
<div class="m2"><p>بر آن ژرف دریای بی بن براند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین گفتن فرعین دیو دمان</p></div>
<div class="m2"><p>همانا ز دیوان سر آمد زمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شما یکسره خود به چنگال تیز</p></div>
<div class="m2"><p>نمائید بر دشمنان رستخیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دیوان به چنگال و گردان به تیر</p></div>
<div class="m2"><p>ببود آب دریا چو شنجرف و قیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن ماهیان گشته از تیر پر</p></div>
<div class="m2"><p>ز شصت دلیران و از تاب خور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آمد شد تیر بر سر و گوش</p></div>
<div class="m2"><p>تو گفتی هوا خانه چوب‌پوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بس خون که از هر سوئی ریختند</p></div>
<div class="m2"><p>ز خون آب دریا برآمیختند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زبس کشته دیوان به آب اندرون</p></div>
<div class="m2"><p>بدی قوت دو سال ماهی فزون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل سام نیرم ز غم خسته شد</p></div>
<div class="m2"><p>که آن جنگ و اشتاب آهسته شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی نعره زد هم اندر شتاب</p></div>
<div class="m2"><p>که تا شد بدان دیوها زهره آب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بیمش بسی دیو شد سرنگون</p></div>
<div class="m2"><p>سیه گشت‌شان اندرون و برون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به آواز گفت از نژاد و گهر</p></div>
<div class="m2"><p>که سامم به نام و نریمان پدر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنین تا به جمشید شاه جهان</p></div>
<div class="m2"><p>پدر بر پدر یاد دارم نهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه رفته اندر جهان فراخ</p></div>
<div class="m2"><p>به من مانده مردی و ایوان کاخ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه نام و ننگ از پی عشق یار</p></div>
<div class="m2"><p>بدادم نه دل هست نه صبر و قرار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه اندیشه از دیو دارم نه جنگ</p></div>
<div class="m2"><p>نترسم ز نر اژدها و پلنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر آن کو به مردی دلاورترند</p></div>
<div class="m2"><p>به یک زخم پیکار من بنگرند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفت این و بگرفت تیر و کمان</p></div>
<div class="m2"><p>نشان کرده اندر تن بدگمان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز زخم تن دیو اندر شتاب</p></div>
<div class="m2"><p>ز پشت سیم دیو رفتی به آب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر آن تیر کز زخم و از پشت جست</p></div>
<div class="m2"><p>بینداختی چار و سه گشت پست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو افکند دیوان صدوشصت وچار</p></div>
<div class="m2"><p>به تیر و کمان اندر آن کارزار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دگر نیزه کو بد صد و سی ارش</p></div>
<div class="m2"><p>طلب کرد و آورد در زیرکش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به کشتی زدش نیزه اندر شتاب</p></div>
<div class="m2"><p>بزد کشتی و ریخت دیوان در آب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فراوان ز دیوان به دریا نگون</p></div>
<div class="m2"><p>شده غرقه عرق دریای خون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یکی دیو با هول مانند برق</p></div>
<div class="m2"><p>به پولاد و جوشن تنش بود عرق</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گرفتش کمرگاه سام دلیر</p></div>
<div class="m2"><p>مگر کش رباید به مردی ز شیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سپهبد یکی تیغ زد بر سرش</p></div>
<div class="m2"><p>به دو نیمه شد تا رگ و پیکرش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به لشکر جنین گفت پس پهلوان</p></div>
<div class="m2"><p>که ای رزم‌دیده دلاور گوان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدانید در روز رزم و هنر</p></div>
<div class="m2"><p>که یک دشت گوری یکی شیر نر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به چنگال و دندان و موران و مار</p></div>
<div class="m2"><p>برآرد مثل گر بود صدهزار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بگفت این و آهنگ کردن گرفت</p></div>
<div class="m2"><p>ز کشتی به کشتی سپردن گرفت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برآمد ز تیغش ز دیوان غریو</p></div>
<div class="m2"><p>همه روی دریا پر از لاش دیو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به گرز و به نیزه ز شمشیر و تیر</p></div>
<div class="m2"><p>برآوردی از جهان دیوان نفیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز کشتی به کشتی همی شد دلیر</p></div>
<div class="m2"><p>همی گشت می‌جست دشمن چو شیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو بر آتش خور بپوشید دود</p></div>
<div class="m2"><p>از آن نره دیوان دو بهره نبود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شب تیره چون شد میانجی ز راه</p></div>
<div class="m2"><p>جدا کردشان جمله از هم سپاه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز هر دو طرف لنگر انداختند</p></div>
<div class="m2"><p>به خاب و خورش گردن افراختند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ببود از دو سو نعره پاسبان</p></div>
<div class="m2"><p>چو رعد از دل تیره ابر شبان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همه دیوها را دل و دیده خون</p></div>
<div class="m2"><p>که فردا چه سازیم تدبیر چون</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به صد زحمت از چنگ یل جسته‌ایم</p></div>
<div class="m2"><p>ز هر رستگان پنج و شش خسته‌ایم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به نزد نهنکال پیکی روان</p></div>
<div class="m2"><p>دواندند کای نامور پهلوان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز دیوان دو بهره بهی شد به جنگ</p></div>
<div class="m2"><p>چو سام نریمان نباشد نهنگ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کسی مرد پیکار این مرد نیست</p></div>
<div class="m2"><p>اگر کام خواهی ازیدر مایست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در آن دم که با او رسیدیم تنگ</p></div>
<div class="m2"><p>فرو کوفتیم آن زمان طبل جنگ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به کشتی روان سام چون پیل مست</p></div>
<div class="m2"><p>صد و شصت گز نیزه دارد به دست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سنانش چو در جنگ آرد گذار</p></div>
<div class="m2"><p>ز کشتی دیوان برآرد دمار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز کشتی به کشتی درآید چو شیر</p></div>
<div class="m2"><p>ز دیوان جنگی نماید اسیر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز تیر و کمانش چه گوئیم باز</p></div>
<div class="m2"><p>به هر زخم تیری دو سه رزم‌ساز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز گرزش به هر سر که آید به خشم</p></div>
<div class="m2"><p>ز سر مغز بیرون جهد نیز چشم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز دریا به دیوان سرآید زمان</p></div>
<div class="m2"><p>همانا که یک تن نیابیم امان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به یک ساعت این یل که حمله برد</p></div>
<div class="m2"><p>بود قوت صد ساله ماهی خورد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به بیچارگی تا به روز دگر</p></div>
<div class="m2"><p>کشیم انتظار تو ای نامور</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اگر دیرتر آمدی جنگ‌جوی</p></div>
<div class="m2"><p>بود سام چون سنگ و دیوان سبوی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همه خورد خواهد شکستن به راه</p></div>
<div class="m2"><p>کند شاه بر کار ما خود نگاه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بدین سان دل‌افکار و زاریم ما</p></div>
<div class="m2"><p>دل و دیده در انتظاریم ما</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>اگر گل به دست تو باشد مبوی</p></div>
<div class="m2"><p>دل نره دیوان خود را بجوی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>یکی دیو پوینده پر غریو</p></div>
<div class="m2"><p>بیامد به نزد نهنکال دیو</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سراسر بدو باز گفت آنچه بود</p></div>
<div class="m2"><p>نهنکال از غصه برجست زود</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>روان شد ابا لشکر بی‌شمار</p></div>
<div class="m2"><p>همه نره دیوان جنگی کار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>همه لشکرش را به کشتی نشاند</p></div>
<div class="m2"><p>به دریا و او خود پیاده بماند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>همی آب دریا بدش تا کمر</p></div>
<div class="m2"><p>خورش در پر گوش کردی گذر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هر آن کش بدیدی برفتی ز هوش</p></div>
<div class="m2"><p>بدان زشتی و سهم آن دیو زوش</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>از آن سو چو شب رفت اندرسحاب</p></div>
<div class="m2"><p>برافکند خورشید رخشان نقاب</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز نو کینه جنگ کردند ساز</p></div>
<div class="m2"><p>همه آدم و دیو شد رزم‌ساز</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>دو لشکر سر از خواب برداشتند</p></div>
<div class="m2"><p>زکین تیغ و نیزه برافراشتند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>یکی دیو را نام قوپیل بود</p></div>
<div class="m2"><p>به پیش آمد و پیش‌دستی نمود</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>فغان کرد کای نره دیوان جنگ</p></div>
<div class="m2"><p>به سنگ گران سنگ بگشای چنگ</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بکوشید تا لشکر سام یل</p></div>
<div class="m2"><p>ز کشته شود روی دریا چو تل</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>که این دم بیاید نهنکال دیو</p></div>
<div class="m2"><p>رسد زود ز آدم برآرد غریو</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ز سام و ز لشکر برآرد دمار</p></div>
<div class="m2"><p>بگیرد کندشان همه خوار و زار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>کسی را کجا تاب آن روز کین</p></div>
<div class="m2"><p>نماند یکی آدمی بر زمین</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بگفت این و آهنگ آویز کرد</p></div>
<div class="m2"><p>ز دیوان صف جنگ را تیز کرد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>یکی را نبد رحم بر جان کس</p></div>
<div class="m2"><p>نه فریادخواه و نه فریادرس</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بدان گفته سام اندر آمد به گوش</p></div>
<div class="m2"><p>درآورد گرز گران را به دوش</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ولی از گران‌سنگی گرز او</p></div>
<div class="m2"><p>کسی را نبد تاب آن برز او</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ز کشتی به کشتی برفتی چو گرد</p></div>
<div class="m2"><p>همی کشت دیوان جنگی نبرد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ز یک سوی قلواد و قلوش دگر</p></div>
<div class="m2"><p>به سوی دگر جنگ را چاره‌گر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بکشتند بسیار دیوان سران</p></div>
<div class="m2"><p>شماره نه پیدا بدی اندر آن</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>شده دیو ترسان و گردون دلیر</p></div>
<div class="m2"><p>به بدخواه برگشته بودند چیر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>نه پائی که جائی گریزان شوند</p></div>
<div class="m2"><p>نه دستی که کوی گریبان شوند</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>پدر بازنشناخت فرزند را</p></div>
<div class="m2"><p>برادر نه هم خویش و پیوند را</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ز تیر دلیران همچون تگرگ</p></div>
<div class="m2"><p>ببارید و بر زنده شد عمر مرگ</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ز زنهار دیوان و از الامان</p></div>
<div class="m2"><p>فغان‌شان رسیده به هفت آسمان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ز هر سوی دلیری به پیکار بود</p></div>
<div class="m2"><p>نبد جنگ گر جنگ پیکار بود</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بگفتند با یکدگر دیو دون</p></div>
<div class="m2"><p>نمانیم ما زنده چون مرده چون</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>نداریم ما تاب یل روز جنگ</p></div>
<div class="m2"><p>همانا زمان‌مان رسیده است تنگ</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>درین گفتگو و ازین رزم سیر</p></div>
<div class="m2"><p>شده دیوها را سر از رزم سیر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ز دیوان غریو و ز مردان خروش</p></div>
<div class="m2"><p>ز خون دلیران زدی بحر جوش</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>برآمد فغانی و شور و غریو</p></div>
<div class="m2"><p>که هیهات از زور سردست نیو</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>چو سام آنچنان کرد مدهوش شد</p></div>
<div class="m2"><p>یکی ساعتی نیز خاموش شد</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چو باهوش آمد بغرید باز</p></div>
<div class="m2"><p>که ای پهلوانان گردن فراز</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>مترسید از آب و از دیو نر</p></div>
<div class="m2"><p>که اکنون به حکم یکی دادگر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>کنم سرنگون کشتیانشان همه</p></div>
<div class="m2"><p>کنم طعمه ماهیانشان همه</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بگفت و درآمد دگر باره باز</p></div>
<div class="m2"><p>چو کوهی مر آن کرده گردن‌فراز</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بزد باز برکشتی آن نیزه را</p></div>
<div class="m2"><p>یکی روز آورد استیزه را</p></div></div>