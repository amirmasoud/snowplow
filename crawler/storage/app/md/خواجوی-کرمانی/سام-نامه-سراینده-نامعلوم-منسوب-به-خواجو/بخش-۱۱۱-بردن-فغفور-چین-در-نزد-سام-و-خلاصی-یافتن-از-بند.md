---
title: >-
    بخش ۱۱۱ - بردن فغفور چین در نزد سام و خلاصی یافتن از بند
---
# بخش ۱۱۱ - بردن فغفور چین در نزد سام و خلاصی یافتن از بند

<div class="b" id="bn1"><div class="m1"><p>تمرتاش با وی نشد هیچ رام</p></div>
<div class="m2"><p>ببردش همانگه به نزدیک سام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سران سپه زین خبر یافتند</p></div>
<div class="m2"><p>سراسر سوی سام بشتافتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدیدند فغفور را بسته دست</p></div>
<div class="m2"><p>بر پهلو یل سرافکنده پست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تمرتاش جنگی سخن کرد ساز</p></div>
<div class="m2"><p>به پرده همی راند با سام راز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو گشتند نام‌آوران انجمن</p></div>
<div class="m2"><p>تمرتاش پردخته شد از سخن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز ناگه جهان‌پهلو نامور</p></div>
<div class="m2"><p>به فغفور کرد از سر کین نظر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدو گفت کای شاه وارونه‌رای</p></div>
<div class="m2"><p>هم اکنون سرت را درآرم ز پای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به آتش بسوزم همه کشورت</p></div>
<div class="m2"><p>نهم بر سر موج خون افسرت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو دانی که با من چه کردی به دهر</p></div>
<div class="m2"><p>همه نوشم از کار تو گشت زهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همان‌گه طلب کرد دژخیم را</p></div>
<div class="m2"><p>بگفتش ز دل دور کن بیم را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر شاه چین را جدا کن ز تن</p></div>
<div class="m2"><p>که آسوده گردد ز کین انجمن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سبک مرد خونریز آهیخت تیغ</p></div>
<div class="m2"><p>که خون ریزد از شاه چین بی‌دریغ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پی لابه بگشاد فغفور لب</p></div>
<div class="m2"><p>چو دید آن که روزش شود تیره شب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به سام دلاور خروشید و گفت</p></div>
<div class="m2"><p>کزین پس نگردم به بد یار و جفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو کام دل خود نیابم ز بت</p></div>
<div class="m2"><p>همان به که رخ را بتابم ز بت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو پاکان درآیم سوی دین تو</p></div>
<div class="m2"><p>برانم ز دل یکسره کین تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بخندید ازو سام گردنفراز</p></div>
<div class="m2"><p>بدو گفت از من نیابی جواز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کنون کت روان در دم اژدهاست</p></div>
<div class="m2"><p>رخ خود نهی سوی دادار راست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز بحر فنا چون که یابی نجات</p></div>
<div class="m2"><p>دگر ره خداوند تو هست لات</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز بحر فنا چون که یابی نجات</p></div>
<div class="m2"><p>گر ره خداوند تو هست لات</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گهی درپذیرم من این گفتگو</p></div>
<div class="m2"><p>که از بت بتابی به یکباره روی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بسان تکش خان شه تاشکن</p></div>
<div class="m2"><p>که آوره بر جان بتها شکن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز رهبان برآری به یک ره روان</p></div>
<div class="m2"><p>صنم پست سازی و سازی فغان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به پا اندر آری سر بتکده</p></div>
<div class="m2"><p>ببندی درو پیل و اسب و دده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو این کرده باشی بدانم نخست</p></div>
<div class="m2"><p>که در تن نباشد دلت هیچ سست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنین پاسخش داد فغفور شاه</p></div>
<div class="m2"><p>که سازم کنون دیر و بت را تباه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بتان را سراسر به هم بر زنم</p></div>
<div class="m2"><p>صنم را ازین غم به خاک افکنم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زبانش همی داد زینسان خبر</p></div>
<div class="m2"><p>دلش داشت راز نهانی به سر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز گفتار او سام گردید شاد</p></div>
<div class="m2"><p>سبک بند از یال او برگشاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شه چین به ناکام شد ز اهل دین</p></div>
<div class="m2"><p>بیاورد سام دلاور به چین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همه دیر و بت‌خانه‌ها کرد پست</p></div>
<div class="m2"><p>صنم را بفرمود تا بت شکست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رها کرد مه‌روی را او ز بند</p></div>
<div class="m2"><p>نشاندش بر افراز گاه بلند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وز آن پس بیامد به نزدیک سام</p></div>
<div class="m2"><p>بدو گفت کز دل شدم با تو رام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بتان را ز گیتی برانداختم</p></div>
<div class="m2"><p>پری‌دخت را هم رها ساختم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ولیکن دو هفته مرا ده امان</p></div>
<div class="m2"><p>که سازم ازو مر تو را شادمان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خز و لعل و در و گهر بی فسوس</p></div>
<div class="m2"><p>به کار آورم از برای عروس</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وز آن پس ز شاهان برآرم سرت</p></div>
<div class="m2"><p>بیارم شبی ماهرو در برت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جهانجو پذیرفت گفتار او</p></div>
<div class="m2"><p>ولیکن نبد آگه از کار او</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چه خوش گفت جمشید فرخنده بهر</p></div>
<div class="m2"><p>کز افعی مجو در جهان غیر زهر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شه چین دگر ره به یاری بخت</p></div>
<div class="m2"><p>رها گشت و بر شد برافراز تخت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>برو سروران آفرین خواندند</p></div>
<div class="m2"><p>دگر باره‌اش شاه چین خواندند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو بنشست بر تخت فغفور شاه</p></div>
<div class="m2"><p>جهان‌پهلوان شد به سوی سپاه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تکش خان و فرهنگ و قلواد شیر</p></div>
<div class="m2"><p>تمرتاش و هم قلوش شیرگیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نشستند با نامور پهلوان</p></div>
<div class="m2"><p>کشیدند باده به روشن‌روان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تمرتاش از آن خوابش آمد به یاد</p></div>
<div class="m2"><p>چنین گفت کای پهلو پاکزاد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز دلدار مه رو نیابم نشان</p></div>
<div class="m2"><p>چنین چند چشمم بود جانفشان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به چربی چنین داد سامش جواب</p></div>
<div class="m2"><p>که بر دل منه هیچ راه شتاب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شکیبائی ار پیشه سازی نکوست</p></div>
<div class="m2"><p>بدان تا نماید تو را چهره دوست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در آن رو شه چین به دستور گفت</p></div>
<div class="m2"><p>که رای نکو با دلم گشت جفت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اگر تیره گردد رخ اخترم</p></div>
<div class="m2"><p>که ندهم بداندیش را دخترم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فلک گر ز هم بگسلد بند من</p></div>
<div class="m2"><p>نبیند دگر سام فرزند من</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همان به که سازیم حیلت‌گری</p></div>
<div class="m2"><p>بجوئیم با او دگر داوری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>برو خوش برآئیم شادی کنیم</p></div>
<div class="m2"><p>که و مه دگرباره رادی کنیم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو او گردد از هر سوئی بی‌گمان</p></div>
<div class="m2"><p>برآریم آوازه‌ای ناگهان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>که سرو سمن بوی رنجور شد</p></div>
<div class="m2"><p>ز بس ضعف از خرمی دور شد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو روزی درین گفتگو بگذرد</p></div>
<div class="m2"><p>شبی زین شبستان رویم از خرد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>درآریم آن زشت‌خو را ز گاه</p></div>
<div class="m2"><p>گزینیم جایش نهانی ز چاه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز ناگاه بنیاد شیون کنیم</p></div>
<div class="m2"><p>همه چین ز ماتم چو گلخن کنیم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>که ببریده شد از پریوش روان</p></div>
<div class="m2"><p>دریغا از آن سروزاد جوان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز ماتم چون چین اندر آید به جوش</p></div>
<div class="m2"><p>دژم رو شود سام یل زان خروش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>پژوهش چو گردد ز غم جان دهد</p></div>
<div class="m2"><p>و یا رخ روان سوی ایران نهد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>درین گفتگو شاه را باز دار</p></div>
<div class="m2"><p>کزو سام خواهد شدن سوگوار</p></div></div>