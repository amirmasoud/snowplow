---
title: >-
    بخش ۱۵۳ - در صفت بهشت شداد گوید
---
# بخش ۱۵۳ - در صفت بهشت شداد گوید

<div class="b" id="bn1"><div class="m1"><p>از آن منزل آمد سوی باغ و کشت</p></div>
<div class="m2"><p>خرامید از آنجا به سوی بهشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه شصت در شصت بد لاله‌زار</p></div>
<div class="m2"><p>ز مرغان به هر گوشه‌ای ناله زار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر شاخ مرغی کشیده نوا</p></div>
<div class="m2"><p>زمین مخمل سبز و مشکین هوا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هر گوشه نساج ابر بهار</p></div>
<div class="m2"><p>کشیده زبان را به هر جای بار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز زربفت و دیبا همه گونه‌گون</p></div>
<div class="m2"><p>قضا بافته داده یکسر برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بساط زمانه همه رنگ رنگ</p></div>
<div class="m2"><p>ز سبزه برون داده از سبزه رنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عروسان بستان رخ آراسته</p></div>
<div class="m2"><p>حنا بسته و چهره پیراسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنفشه چو زلف پراکنده سر</p></div>
<div class="m2"><p>شده سنبل از دور بر یکدگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرفته به کف لاله خونین باغ</p></div>
<div class="m2"><p>سراپایش خونین و دل پر ز داغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شده داد خیری ز درد سمن</p></div>
<div class="m2"><p>ستاده به پیش حکیم چمن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرفته عصا نرگس خسته زار</p></div>
<div class="m2"><p>سرافکنده در پیش بیماروار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شد بلبل از شاخ دستان نواز</p></div>
<div class="m2"><p>گشاده گلش گوش از روی باز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه حسن و عشقی که در کار بود</p></div>
<div class="m2"><p>همه صنع یزدان دادار بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جز ایزد که آرد مر این را پدید</p></div>
<div class="m2"><p>گه آرد پدید و گهی ناپدید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به یک دست آن باغ و آن لاله کشت</p></div>
<div class="m2"><p>سراسر همه بود زرینه خشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شده بیست فرسنگ بالای باغ</p></div>
<div class="m2"><p>دگر بیست فرسنگ پهنای باغ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ازو سال پانصد که استادکار</p></div>
<div class="m2"><p>همی ساخت او را و بد نیمه‌کار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ندیده بدش هیچ شداد عاد</p></div>
<div class="m2"><p>ز مغروری گنج ناپاک زاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از آن گونه چون گردد آراسته</p></div>
<div class="m2"><p>بیاید ببیند همه خواسته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پس از عمر نهصد گذشته به سال</p></div>
<div class="m2"><p>که خواهد تماشا کند بدسگال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که آید به صد کام آن پرشتاب</p></div>
<div class="m2"><p>چو زو پای ناپاکش اندر رکاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکی بر رکاب و دگر بر زمین</p></div>
<div class="m2"><p>که مرگ آید از پیش جان آفرین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کند قبض روحش هم اندر زمان</p></div>
<div class="m2"><p>نبخشید او را دمی هم امان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سپهبد درآمد به نزد بهشت</p></div>
<div class="m2"><p>همه قصر یاقوت زرینه خشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه باغ از صندل و عود و عاج</p></div>
<div class="m2"><p>به زینت ز گردون گرفته خراج</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چهل قصر بودش همه پرگهر</p></div>
<div class="m2"><p>در و بام یکسر همه سیم و زر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چهل قصر دیگر ابر گرد قصر</p></div>
<div class="m2"><p>ز یاقوت آراست استاد عصر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تراشیده فواره از لعل و سیم</p></div>
<div class="m2"><p>از آن سیم پاشیده در یتیم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چهل بد خیابان چمن در چمن</p></div>
<div class="m2"><p>گل او ز یاقوت سیمین سمن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>درختان سراسر همه سیم و زر</p></div>
<div class="m2"><p>برو میوه آویخته از گهر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همه سیم او گوهر شب‌چراغ</p></div>
<div class="m2"><p>درختان شده همچو روشن چراغ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه نار و یاقوت آویخته</p></div>
<div class="m2"><p>ز فیروزه برگش درآویخته</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بهش کهربا بد میانش تهی</p></div>
<div class="m2"><p>همه شاخ و برگش بسان بهی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شکوفه زر و نخل از سیم خام</p></div>
<div class="m2"><p>چو پروین که بر گرد ماه تمام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گل شنبلیدش ز یاقوت زرد</p></div>
<div class="m2"><p>بنفشه تراشیده از لاجورد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زمرد همه سبز در صحن باغ</p></div>
<div class="m2"><p>زبرجد شده سرو بالای راغ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شده نیم نرگس میانه ز زر</p></div>
<div class="m2"><p>تراشیده غنچه همه از گهر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ابرشاخ مرغان همه زر خشک</p></div>
<div class="m2"><p>شکمشان مجوف همه پر ز مشک</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز یاقوت منقار و از لعل بال</p></div>
<div class="m2"><p>شده چنگش از سیم و زر پایمال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو سام نریمان چنان باغ دید</p></div>
<div class="m2"><p>ز رشکش دل دهر را داغ دید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شده نازنینان در آن انجمن</p></div>
<div class="m2"><p>همه زلف پرچینشان پرشکن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سراسر ز دیبا همه حله پوش</p></div>
<div class="m2"><p>پراکنده زلف و قدح کرده نوش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همه همچو چشم خوش خویش مست</p></div>
<div class="m2"><p>میان گل و لاله غلطیده پست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پری‌پیکران هر طرف نیم‌مست</p></div>
<div class="m2"><p>ز فیروزه و لعل شیشه به دست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نشسته عرق بر جبینشان ز می</p></div>
<div class="m2"><p>ز گل ریخته لاله مانند خوی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همه از می شیشه گلگون شده</p></div>
<div class="m2"><p>ز لبها دل شیشه پر خون شده</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همه گشته غلطان چه چشم و چه جان</p></div>
<div class="m2"><p>همه روی بر رو زبان بر زبان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خرامان گروه دگر در چمن</p></div>
<div class="m2"><p>ابا ناز با یکدگر در سخن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>همه همچو طاوس آراسته</p></div>
<div class="m2"><p>به زر و جواهر بپیراسته</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همه ماهرویان ماچین و چین</p></div>
<div class="m2"><p>همه دلربا و همه دلنشین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همه حور عین و همه دلفریب</p></div>
<div class="m2"><p>که بردند از جان دلها شکیب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>پس استاد در باغ در کار بود</p></div>
<div class="m2"><p>یکی جادوی زشت بدکار بود</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کجا نام ارجان جادو بدی</p></div>
<div class="m2"><p>که از راست‌گوئی به یک سو بدی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو از دور بر سام یل بنگرید</p></div>
<div class="m2"><p>گره زد به ابرو برو بردمید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بدو گفت کای بدرگ بدسرشت</p></div>
<div class="m2"><p>چه کردی بدین گونه اندر بهشت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تو را چیست نام و چسان آمدی</p></div>
<div class="m2"><p>چگونه ز جادوگران آمدی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بهشت است نه جای تو کس است</p></div>
<div class="m2"><p>ولیکن چو افتادی اکنون به شست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>برون رو که تا زنده مانی به دهر</p></div>
<div class="m2"><p>وگرنه کنم بر تو این نوش زهر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>منم نام ارجان فولادخا</p></div>
<div class="m2"><p>به سر کاری باغ مانده به جا</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز بیمم نیارد کس ایدر گذر</p></div>
<div class="m2"><p>ندانم کئی تو بدین یال و فر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به پاسخ بدو گفت سامم به نام</p></div>
<div class="m2"><p>ز ایران به مغرب گرفتم کنام</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>کمربسته بر رزم شداد عاد</p></div>
<div class="m2"><p>بدان تا دهم نام شومش به باد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بکشتم همه دیو آتش‌پرست</p></div>
<div class="m2"><p>ز من شد سمندر ابر خاک پست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کنون نوبت تست و باغ زرین</p></div>
<div class="m2"><p>که تا هر دو را پاک سازم ز کین</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو بشنید ارجان سرش خیره شد</p></div>
<div class="m2"><p>جهان پیش چشمش همی تیره شد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به افسون درآمد به سام سوار</p></div>
<div class="m2"><p>که تا بر سر آرد ورا روزگار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دهان را چو غاری ز هم برگشاد</p></div>
<div class="m2"><p>در گلخن آتش اندر گشاد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>همه باغ پر شد به آتش به دم</p></div>
<div class="m2"><p>دلاور شد از آتش او دژم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>سپر در سر آورد سالار شیر</p></div>
<div class="m2"><p>ز دود و ز آتش سر آورد زیر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>که ناگاه شاپور فرزانه مرد</p></div>
<div class="m2"><p>ز پشت اندر آمد دلیر نبرد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بزد چوب بر فرق آن بدگهر</p></div>
<div class="m2"><p>که مغز سرش کوفت بر یکدگر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>گروه دگر حمله کردند پاک</p></div>
<div class="m2"><p>ز ماتم به گردون فکندند خاک</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>خروشان بگفتند کای بدنژاد</p></div>
<div class="m2"><p>نترسی مگر تو ز شداد عاد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>اگر باد او را دهد آگهی</p></div>
<div class="m2"><p>فرستند ز دیوان یکی را شهی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>همین دم تو را همچو سنگ سیاه</p></div>
<div class="m2"><p>کند تیره روز تو گردد سیاه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو بشنید شاپور آمد به جنگ</p></div>
<div class="m2"><p>به یک دم زمین ساخت پشت پلنگ</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>زمین کرد از آن مردمان اسپری</p></div>
<div class="m2"><p>که ناگه رسیدند دیو و پری</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>همان شاه جنی به سر بر کلاه</p></div>
<div class="m2"><p>رسیدند با دیو جنی سیاه</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>سپاه اندر آمد فزون از هزار</p></div>
<div class="m2"><p>همه پر شد آن باغ گوهرنگار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>سپهبد چنین گفت یغما کنید</p></div>
<div class="m2"><p>ز من رزم میدان تماشا کنید</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>نمانید یک شاخ برگ درخت</p></div>
<div class="m2"><p>همه آلت رزم سازید رخت</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>درختان قصرش بکندند زود</p></div>
<div class="m2"><p>از آن باغ شداد برخاست دود</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>گرفتند و کندند و بردند بوم</p></div>
<div class="m2"><p>غلامان و اسبان آن مرز و بوم</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>نماندند در باغ شداد چیز</p></div>
<div class="m2"><p>که ارزد در آن جایگه یک پشیز</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بهشتش به تاراج شد اسپری</p></div>
<div class="m2"><p>به فرموده سام و شاه پری</p></div></div>