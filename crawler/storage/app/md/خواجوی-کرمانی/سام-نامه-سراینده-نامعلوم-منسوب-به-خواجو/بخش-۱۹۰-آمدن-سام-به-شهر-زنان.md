---
title: >-
    بخش ۱۹۰ - آمدن سام به شهر زنان
---
# بخش ۱۹۰ - آمدن سام به شهر زنان

<div class="b" id="bn1"><div class="m1"><p>بپاسخ بدو گفت شاه زن است</p></div>
<div class="m2"><p>که زین سان شتابنده و پازن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ورا نام حورا بود در جهان</p></div>
<div class="m2"><p>که نامش نگنجد همی در زبان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به آهنگ روی تو گشته سوار</p></div>
<div class="m2"><p>شتابان رسیده درین چشمه سار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو گفت پس کای زن ماهروی</p></div>
<div class="m2"><p>مرا چون شناسد مه مشک موی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدو آشنائی نکردم دمی</p></div>
<div class="m2"><p>نبودم به او یک زمان همدمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه داند که من کیستم در جهان</p></div>
<div class="m2"><p>بزرگم و یا کهترین مهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدو گفت پاسخ که ای رزمجوی</p></div>
<div class="m2"><p>شناسم ما مردمان را به بوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو اندر رسیدی در آن چشمه سار</p></div>
<div class="m2"><p>شنیدیم بوی تو را ای سوار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون پادشه مر تو را خواسته است</p></div>
<div class="m2"><p>ز بهر رخت خویش آراسته است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین گفتگو بود فرخنده سام</p></div>
<div class="m2"><p>خرامنده آن سرو طوطی خرام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیامد بر سام نیرم نشست</p></div>
<div class="m2"><p>بیازید و بگرفت دستش به دست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدو گفت خندان که شاد آمدی</p></div>
<div class="m2"><p>درین شهر زیبا چو باد آمدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه نامی و ایدر چرا آمدی</p></div>
<div class="m2"><p>به شهر زنان از کجا آمدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ندیدم هرگز چو تو نامدار</p></div>
<div class="m2"><p>به بالا چو سرو و به رخ چون بهار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نماید که مردی دلاور بوی</p></div>
<div class="m2"><p>به دریای مردی شناور بوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه داری مرادت به من بازگوی</p></div>
<div class="m2"><p>مپوشان ز من راز خود بازگوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو گفت سام نریمان منم</p></div>
<div class="m2"><p>ستاننده جان دیوان منم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرفتار عشقم نه عقل و نه دین</p></div>
<div class="m2"><p>به بند پری‌دخت فغفور چین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ربودست او را ز من ابرها</p></div>
<div class="m2"><p>فتاده به کام یکی اژدها</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من ایدر کمربسته از بهر این</p></div>
<div class="m2"><p>شتابم غریوان به مغرب زمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>براندازم آن دیو ناپاک را</p></div>
<div class="m2"><p>فشانم ابر تارکش خاک را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که اویش ربودست از رنگ و ریو</p></div>
<div class="m2"><p>رهانم پری‌‌دخت از چنگ دیو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو بشنید حوران فرخ‌نژاد</p></div>
<div class="m2"><p>بدو گفت آنها که کردی به یاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه بود نیکو ولی ای جوان</p></div>
<div class="m2"><p>زتخم دلیران روشن روان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خطرها بسی هست در راه تو</p></div>
<div class="m2"><p>کزو تیره گردد همه راه تو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر تو بیائی به مهمان من</p></div>
<div class="m2"><p>شوی شاد یک هفته از خوان من</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پس آنگه ابا تو بیایم به راه</p></div>
<div class="m2"><p>به کوه فنا سوی آن کینه‌خواه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو را یار باشم درین ره همی</p></div>
<div class="m2"><p>نگردی ز پیکار دیوان غمی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نمایم تو را من شگفتی بسی</p></div>
<div class="m2"><p>که هرگز ندیدم درین ره کسی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدو گفت پهلو که ای دلربا</p></div>
<div class="m2"><p>شتابم به تنها به کوه فنا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرا یار دادار یزدان بس است</p></div>
<div class="m2"><p>که در دهر او بیکسان را کس است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بسی کرد خواهش بدان نازنین</p></div>
<div class="m2"><p>که آخر رضا گشت گرد گزین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نشست از بر اسب و آمد به شهر</p></div>
<div class="m2"><p>که یابد از آن ماه‌دیدار بهر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نگه کرد شهری بسان بهشت</p></div>
<div class="m2"><p>تو گفتی که رضوان درو گل بکشت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همه شهر و بازار پر ماهروی</p></div>
<div class="m2"><p>به هر گوشه‌ای دلبر مشکبوی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سراسر زنان طلعتی همچو ماه</p></div>
<div class="m2"><p>به هر کوی و برزن یکی بزمگاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پر از مشک و عنبر همه بام و بر</p></div>
<div class="m2"><p>کزو مست گردید آن نامور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چنین تا به ایوان حورا رسید</p></div>
<div class="m2"><p>شگفتی سراپرده‌شان را بدید</p></div></div>