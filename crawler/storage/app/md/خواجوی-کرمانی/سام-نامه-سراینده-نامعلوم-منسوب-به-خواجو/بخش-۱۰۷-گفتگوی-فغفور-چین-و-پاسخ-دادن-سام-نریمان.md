---
title: >-
    بخش ۱۰۷ - گفتگوی فغفور چین و پاسخ دادن سام نریمان
---
# بخش ۱۰۷ - گفتگوی فغفور چین و پاسخ دادن سام نریمان

<div class="b" id="bn1"><div class="m1"><p>شه چین چو آن سرکشان را بدید</p></div>
<div class="m2"><p>رخش شد از اندوه چون شنبلید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگرداند از کینه و رزم روی</p></div>
<div class="m2"><p>دلش گشت از نو دگر مهرجوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برانگیخت بور ابرش تیزگام</p></div>
<div class="m2"><p>گرفت از ره ایمنی دست سام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دیو دژآگه پژوهش گرفت</p></div>
<div class="m2"><p>جهان‌پهلوان را نکوهش گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که چون رزم جستی ز دیو دژم</p></div>
<div class="m2"><p>سزد گر بگوئی مرا بیش و کم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که تا تو شدی سوی پیکار دیو</p></div>
<div class="m2"><p>دلم بد ز اندیشه‌ات پر غریو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخندید ازو سام و لب برگشاد</p></div>
<div class="m2"><p>همه رزم و پیکار او کرد یاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز ناگاه فغفور چین بنگرید</p></div>
<div class="m2"><p>به زیر قبا جعبه‌اش را بدید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو گفتی که نوشش همه شد کبست</p></div>
<div class="m2"><p>ز دستش به تندی جدا کرد دست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وز آن پس بدو گفت کای نیک‌بهر</p></div>
<div class="m2"><p>سزد گر رخ آری کنون سوی شهر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر غم درآیم در زیر بند</p></div>
<div class="m2"><p>بسازیم با خوشدلی روز چند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدانگه که گردی ز می بی‌خبر</p></div>
<div class="m2"><p>پری‌دخت را اندر آری به بر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو نام پری‌دخت بشنید سام</p></div>
<div class="m2"><p>تو گفتی ورا روز شد همچو شام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو آتش دلش اندر آمد ز جای</p></div>
<div class="m2"><p>ز کردار فغفور و شاه ختای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز غم چهره‌اش زعفران بار شد</p></div>
<div class="m2"><p>سرشکش نشانی ز گلنار شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به فغفور گفت ای شه زشت‌خو</p></div>
<div class="m2"><p>چه کردم ز بد با تو خود بازگو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز خاور چو راندم سوی چین سمند</p></div>
<div class="m2"><p>نخستین کمین برگشودم به ژند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مر او را به خنجر سر انداختم</p></div>
<div class="m2"><p>وز آن به دژ سر برافراختم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بکندم ز بن بیخ و بنیاد را</p></div>
<div class="m2"><p>ز محنت رهاندم پریزاد را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه گوهر و لعل و در ثمین</p></div>
<div class="m2"><p>کشیدم ز زرینه دژ سوی چین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سرم را ز گردان برافراختی</p></div>
<div class="m2"><p>ولیکن به بندم درانداختی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگرچه در آن بند بودم نژند</p></div>
<div class="m2"><p>ولی دادگستر رهاندم ز بند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو از کار من آگهی یافتی</p></div>
<div class="m2"><p>سوی رزم و پیکار بشتافتی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فراوان کشیدی به من تیغ تیز</p></div>
<div class="m2"><p>ز نابخردی باز جستی گریز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگرچه کشیدم بسی درد و رنج</p></div>
<div class="m2"><p>ولی شاد گشتم ز پرمایه گنج</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو گنجم به دست اندر افتاد باز</p></div>
<div class="m2"><p>به نیکی تو را کردم آگه ز راز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همانگه یکی نامه آراستی</p></div>
<div class="m2"><p>ز من آن پریروی را خواستی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نپیچیدم از امر تو هیچ سر</p></div>
<div class="m2"><p>سپردم به تو لاله رخ را دگر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پذیرفتی ای شاه بیدادخوی</p></div>
<div class="m2"><p>که آری به نیکی سوی داد روی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به خوبی دهی ماهرو را به من</p></div>
<div class="m2"><p>نرانی ز پیکار و کینه سخن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برادر پسر را سرافراختی</p></div>
<div class="m2"><p>مرا در جهان جفت غم ساختی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من آرم بدین رزم پرخاش را</p></div>
<div class="m2"><p>تو سر برفرازی تمرتاش را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز من نیکی آمد همی از تو بد</p></div>
<div class="m2"><p>چنین بد ز شاهنشهی کی سزد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کنون چاره از نو بیاراستی</p></div>
<div class="m2"><p>پی خون من لشکر آراستی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>می جنگ و پیکار نوشیده‌ای</p></div>
<div class="m2"><p>نهان آلت رزم پوشیده‌ای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که در بزم بر من سرآری زمان</p></div>
<div class="m2"><p>کنی لشکرم را ازین غم نوان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بگفت و خروشید مانند میغ</p></div>
<div class="m2"><p>همانگه بزد دست و آهیخت تیغ</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سپر بر سر آورد فغفور چین</p></div>
<div class="m2"><p>بزد بر سر نامدار گزین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنان زد که سر تا به سر برشکافت</p></div>
<div class="m2"><p>ز تیغش شهشاه چین سر بتافت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به تندی برانگیخت اسب نوند</p></div>
<div class="m2"><p>به نیرنگ سر دور کرد از گزند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خروشید از آن پس به ترکان چین</p></div>
<div class="m2"><p>که یکسر برآرید شمشیر کین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ممانید تا او شود چیردست</p></div>
<div class="m2"><p>بکوشید با او چو پیلان مست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همانگه ز پیکار فغفور شاه</p></div>
<div class="m2"><p>ز جا اندر آمد سراسر سپاه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>برانگیختند اسب گندآوران</p></div>
<div class="m2"><p>کشیدند یکسر پرندآوران</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جهان شد به پیکار پر رستخیز</p></div>
<div class="m2"><p>که قلواد آمد به شهر ستیز</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>پس و پشت او لشکر زابلی</p></div>
<div class="m2"><p>کشیده همه خنجر کابلی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سپرهای زرین گرفته به چنگ</p></div>
<div class="m2"><p>یکایک خروشان چو شیر و پلنگ</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همان لشکر خلخ و خاوری</p></div>
<div class="m2"><p>برافراخته سر پی داوری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>درآورده بر یال مرکب عنان</p></div>
<div class="m2"><p>به سوی هم آورد داده سنان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دو لشکر به هم آنچنان باز خورد</p></div>
<div class="m2"><p>که گردنده گردون نهان شد ز گرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>غو کوس و شیپور و آواز نای</p></div>
<div class="m2"><p>تو گفتی زمین اندر آمد ز جای</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز سم ستوران زمین شد ستوه</p></div>
<div class="m2"><p>درآورد لرزه به هامون و کوه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز خون شد دم تیغها ژاله‌ریز</p></div>
<div class="m2"><p>وز آن ژاله صحرای کین لاله‌خیز</p></div></div>