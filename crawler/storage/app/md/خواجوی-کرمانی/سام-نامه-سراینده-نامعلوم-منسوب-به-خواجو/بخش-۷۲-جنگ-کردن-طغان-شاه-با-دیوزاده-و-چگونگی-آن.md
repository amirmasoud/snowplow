---
title: >-
    بخش ۷۲ - جنگ کردن طغان شاه با دیوزاده و چگونگی آن
---
# بخش ۷۲ - جنگ کردن طغان شاه با دیوزاده و چگونگی آن

<div class="b" id="bn1"><div class="m1"><p>طغان شاه چون باد بر شد به زین</p></div>
<div class="m2"><p>برانگیخت نام‌آوران را به کین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبک دیوزاده کمین روی کرد</p></div>
<div class="m2"><p>به خون یلان دشت را جوی کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خروشنده گردید چون سنهراس</p></div>
<div class="m2"><p>ازو شد دل سرکشان پرهراس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درآمد میان سپه همچو شیر</p></div>
<div class="m2"><p>ز هر سو برو بر ببارید تیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسیدی چو پیکان بر آن نامدار</p></div>
<div class="m2"><p>تو گفتی که با دست در کوهسار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طغان شاه چو از وی بدید آن نبرد</p></div>
<div class="m2"><p>دلش گشت پر درد و رخساره زرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در مکر و حیلت‌گری باز کرد</p></div>
<div class="m2"><p>چنین چنگ گفتار را ساز کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که باید به نرمی ورا ساخت رام</p></div>
<div class="m2"><p>وز آن پس سرش را کشیدن به دام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز نیرو چو دارد سر سرکشی</p></div>
<div class="m2"><p>ببندیمش از داروی بیهشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همانگه سپه را ز کین بازداشت</p></div>
<div class="m2"><p>وز آن پس سر از سرکشان برفراشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنین گفت با دیوزاده طغان</p></div>
<div class="m2"><p>که چون تو نباشد کسی از گوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همان به که بندی بر من کمر</p></div>
<div class="m2"><p>سپه بد شوی بر سران سر به سر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو را شاه بربر ز بربرزمین</p></div>
<div class="m2"><p>فرستاد از بهر فغفور چین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کجا هست فغفور چین باب من</p></div>
<div class="m2"><p>وزو هست روشن جهان تاب من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو تو سرکشی سازی ای نامور</p></div>
<div class="m2"><p>تو را باز خواهم روان از پدر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو بدهد تو را یابی از من بدی</p></div>
<div class="m2"><p>کنون شو به من رام اگر بخردی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وگر رای داری به درگاه شاه</p></div>
<div class="m2"><p>یک امشب به خلخ بمان صبحگاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ابا کاروان ره‌سپر شو به چین</p></div>
<div class="m2"><p>ببر همره خویشتن نازنین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو بشنید ازو دیوزاده سخن</p></div>
<div class="m2"><p>بدو گفت کای مرد شمشیر زن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پسنده نبودی چو در داوری</p></div>
<div class="m2"><p>کنون روی کردی به حلیت‌گری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدان تا چو آیم سوی خان تو</p></div>
<div class="m2"><p>به یک دست چوبی به ایوان تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به بیهوش دارویم آری به دام</p></div>
<div class="m2"><p>وز آن پس بخواهی ز مهروی کام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرا نیست نزدیک خلخ گذر</p></div>
<div class="m2"><p>از ایدر به چین می‌شوم ره‌سپر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یکی مایه‌ور بود در کاروان</p></div>
<div class="m2"><p>که همیشه بود از مقارن نوان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز ناگه به نزد طغان باز شد</p></div>
<div class="m2"><p>بدو زار برگفت و غماز شد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنین گفت کان ماه سیما کنیز</p></div>
<div class="m2"><p>ز بربر نیامد ابا گنج و چیز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همانا به دریاش دریافتند</p></div>
<div class="m2"><p>چو صندوق را قفل برتافتند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همانا ترا در جهان خواهرست</p></div>
<div class="m2"><p>دو صد چون مقارن ورا چاکر است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مه مهوشان است و طوطی کلام</p></div>
<div class="m2"><p>نجوید ز شاهان کسی غیر سام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ازین است که پنهان کند نام او</p></div>
<div class="m2"><p>دگر این غلام است مر سام را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از آن رخ نتابد به پیکار و کین</p></div>
<div class="m2"><p>که مر لاله رخ را برد سوی چین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قضا دیوزاده ز دور این شنید</p></div>
<div class="m2"><p>همانگه چو آتش ز جا بردمید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شتابان دوید از بر او دمان</p></div>
<div class="m2"><p>ندادش به یک لحظه او را امان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدان سان بزد بر سرش چوبدست</p></div>
<div class="m2"><p>که در پای اسب طغان گشت پست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بشد جانش از صرف این گفتگو</p></div>
<div class="m2"><p>به غماز گفتن مکن آرزو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>طغان چون ز خواهر خبردار شد</p></div>
<div class="m2"><p>برو روز روشن شب تار شد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به دل گفت اگر پیچم از رزم سر</p></div>
<div class="m2"><p>همانا ز من روی تابد پدر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شوم در میان شهان بی‌منش</p></div>
<div class="m2"><p>سرم پست گردد ز پیرامنش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر رزم جویم ازین مرد نیو</p></div>
<div class="m2"><p>مبادا که از من برآرد غریو</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دگرباره چنگ سخن ساز کرد</p></div>
<div class="m2"><p>در حقه و مکر را باز کرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که اکنون بر افلاک بر شد سرم</p></div>
<div class="m2"><p>که سام است جوینده خواهرم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همان به که کاخم گلستان کنی</p></div>
<div class="m2"><p>ابا او گذر در شبستان کنی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در ایوان چو او برفرازد علم</p></div>
<div class="m2"><p>ازو شاد گردند اهل حرم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به دیدار او شادمانی کنند</p></div>
<div class="m2"><p>از آن خرمی زندگانی کنند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو یک هفته ماند درین سرزمین</p></div>
<div class="m2"><p>به هشتم شتابید زی مرز چین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ازیدر به چین گر شوی ره‌سپر</p></div>
<div class="m2"><p>همانا ز من خشم گیرد پدر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دگر آن که باشد سرم زیر ننگ</p></div>
<div class="m2"><p>که در شهر خلخ نسازد درنگ</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سخن زین نشان گفته بی‌حد برو</p></div>
<div class="m2"><p>همی خواست کو را درآرد برو</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز هامون مر او را به ایوان برد</p></div>
<div class="m2"><p>درونش به تیغ جفا بشکرد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نپذرفت فرهنگ گفتار او</p></div>
<div class="m2"><p>سبک داد پاسخ سزاوار او</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>که بنویس یک نامه نزد پدر</p></div>
<div class="m2"><p>بدو قید کن راز من سر به سر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>که او را به دل در بسی بد شتاب</p></div>
<div class="m2"><p>نیامد به خلخ گذر کرد آب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>پری‌دخت همراه او شد روان</p></div>
<div class="m2"><p>سزد گر نگردد دل شه نوان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نه از شاه ترسم نه از لشکرش</p></div>
<div class="m2"><p>بدین چوب بر هم زنم کشورش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تو خواهی که از فکر افسون و بند</p></div>
<div class="m2"><p>سرم را درآری به خم کمند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ولیکن من از تو به حیلت‌گری</p></div>
<div class="m2"><p>بسی برترم درگه داوری</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>طغان چون بدید آن که آن نامور</p></div>
<div class="m2"><p>ز گفتش بپیچید یکباره سر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بدان جنگجویان خروشید باز</p></div>
<div class="m2"><p>که گردید با اهرمن رزم‌ساز</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همه رزم شیران کمین آورید</p></div>
<div class="m2"><p>سرش را ز کین بر زمین آورید</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سپه چون شنیدند گفتار شاه</p></div>
<div class="m2"><p>براندند باره سوی کینه خواه</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>دگر ره بدو اندر آویختند</p></div>
<div class="m2"><p>همه گرز و خنجر فروریختند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بدیشان درافتاد فرهنگ باز</p></div>
<div class="m2"><p>ابا جنگیان گشت او رزم ساز</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گرفتند ناگاهش اندر میان</p></div>
<div class="m2"><p>زدندش به هر سو به گرز گران</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>پری‌دخت مه‌رو چو آگاه شد</p></div>
<div class="m2"><p>مر آن نامور را هواخواه شد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به دل گفت اگرچه طغان سرکشست</p></div>
<div class="m2"><p>ولی دیوزاده به کین آتش است</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>طغان سوی او گر شتابد به جنگ</p></div>
<div class="m2"><p>سپه این کشد او نماید درنگ</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سپاهش دمادم رسد او ز شهر</p></div>
<div class="m2"><p>نیابد ز کین هیچ فرهنگ بهر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز فرهنگ مردی به یک سو شود</p></div>
<div class="m2"><p>هنرهاش در رزم آهو شود</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به بند اندر آرند ناگه سرش</p></div>
<div class="m2"><p>به خون غرقه گردد بر و پیکرش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>طغان شاه گیرد ورا ناگهان</p></div>
<div class="m2"><p>بریزد سبک خونش اندر نهان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>همان به که پوشم سلاح نبرد</p></div>
<div class="m2"><p>برآیم به که پیکر تیز گرد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>سوی رزم و کین دل نهاده شوم</p></div>
<div class="m2"><p>مگر یار با دیوزاده شوم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>رهانم من او را ز چنگ طغان</p></div>
<div class="m2"><p>دلیران کین را ببرم روان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>وگر کشته گردم به میدان کار</p></div>
<div class="m2"><p>سر سام نیرم بود پایدار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>همیشه دلش شاد و خرم بود</p></div>
<div class="m2"><p>که چون او به گیتی دگر کم بود</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>امیدم به دادار دارنده مهر</p></div>
<div class="m2"><p>که بینم رخ سام فرخنده چهر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>کنم جان شیرین فدای سرش</p></div>
<div class="m2"><p>چو پروانه جان بازم اندر برش</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بگفت و نکرد هیچ‌جا او درنگ</p></div>
<div class="m2"><p>به نزدش طلب کرد اسباب جنگ</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>مقارن به اندرز بگشاد لب</p></div>
<div class="m2"><p>نپذرفت اندرزش آن خوش‌لقب</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو بیچاره گردید ساز نبرد</p></div>
<div class="m2"><p>بیاورد بهر پریوش چو گرد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>پری‌دخت پوشید اسباب جنگ</p></div>
<div class="m2"><p>کمر چون دل عاشقان کرد تنگ</p></div></div>