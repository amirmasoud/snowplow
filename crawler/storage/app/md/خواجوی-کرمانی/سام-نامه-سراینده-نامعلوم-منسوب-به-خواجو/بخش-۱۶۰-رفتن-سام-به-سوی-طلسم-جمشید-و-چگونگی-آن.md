---
title: >-
    بخش ۱۶۰ - رفتن سام به سوی طلسم جمشید و چگونگی آن
---
# بخش ۱۶۰ - رفتن سام به سوی طلسم جمشید و چگونگی آن

<div class="b" id="bn1"><div class="m1"><p>به تسلیم جنی چنین گفت سام</p></div>
<div class="m2"><p>ز پیکار طلاج گسترده دام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندیدم به میدان چو آن بدکنش</p></div>
<div class="m2"><p>کزو تیره گردد یلان را منش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکایک سلیحم برون شد ز دست</p></div>
<div class="m2"><p>ز نیرنگ طلاج جادوپرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرانجام از من بشد ناپدید</p></div>
<div class="m2"><p>ازو شاد شد جان جنگی شدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندانم چه چاره سگالم بدو</p></div>
<div class="m2"><p>بدان تا براندازم آن کینه جو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پاسخ بدو گفت کای نامدار</p></div>
<div class="m2"><p>یکی داستان گویمت گوش دار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسی دام دارد ز افسون به دست</p></div>
<div class="m2"><p>نتابید به میدان او پیل مست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه سنگ آهن ربا دارد او</p></div>
<div class="m2"><p>از آن تخم نیرنگ می‌کارد او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدین گرز و شمشیر و تیر و سنان</p></div>
<div class="m2"><p>نگیرد کس او را همی در جهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ورا تیغ و گرزی که اندر طلسم</p></div>
<div class="m2"><p>نهادست جمشید فرخنده اسم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نوشته برو نام یزدان پاک</p></div>
<div class="m2"><p>کزو جان جادوست اندوهناک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر او را به دست اندر آری همی</p></div>
<div class="m2"><p>همه کار جادو سر آری همی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به سوی طلسمت بیاید شدن</p></div>
<div class="m2"><p>شکستن مر او را و باز آمدن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همان گرد قلواد یابد رها</p></div>
<div class="m2"><p>به رنجی یکی گنج یا بی‌بها</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کنم شمسه را همرهت در زمان</p></div>
<div class="m2"><p>شکست اندر آری تو بر بدگمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رهانی ازین رنج رضوان ما</p></div>
<div class="m2"><p>سرافرازی از بخت این جان ما</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگوید به تو شمسه کار طلسم</p></div>
<div class="m2"><p>بداند سراسر شمار طلسم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگرچه تو ایدر شوی بهر نام</p></div>
<div class="m2"><p>سر ما ز دشمن درآید به دام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرفتار گردیم پیش شدید</p></div>
<div class="m2"><p>ولیکن به ما بد نخواهد رسید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>درآید سر ما به زنجیر و بند</p></div>
<div class="m2"><p>تو آنجا چو آئی تن ارجمند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برآری دمار از تن بدسگال</p></div>
<div class="m2"><p>نباشد به گیتی تو را کس همال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خدای جهان یار جان تو باد</p></div>
<div class="m2"><p>بلا بر تن بدگمان تو باد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو بشنید ازو سام خندید سخت</p></div>
<div class="m2"><p>که بیدار گردید بغنوده بخت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سه جام از می لعل‌گون نوش کرد</p></div>
<div class="m2"><p>سخنهای رفته فراموش کرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پس آنگه بپوشید اسباب جنگ</p></div>
<div class="m2"><p>ببستند بر اسب زین خدنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نشست از بر چرمه ره‌نورد</p></div>
<div class="m2"><p>همان شمسه ماه همراه کرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همانگه بیامد به سوی طلسم</p></div>
<div class="m2"><p>پراندیشه در جستجوی طلسم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی آتشی دید ناگه ز دور</p></div>
<div class="m2"><p>درخشان به مانند رخسار هور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدان روشنائی بشد نامدار</p></div>
<div class="m2"><p>همی راند آن باره راهوار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شتابنده با شمسه ماهرو</p></div>
<div class="m2"><p>همه راه جوینده و پویه پو</p></div></div>