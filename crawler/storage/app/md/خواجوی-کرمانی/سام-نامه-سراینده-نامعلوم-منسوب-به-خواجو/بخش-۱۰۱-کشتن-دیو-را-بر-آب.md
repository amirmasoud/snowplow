---
title: >-
    بخش ۱۰۱ - کشتن دیو را بر آب
---
# بخش ۱۰۱ - کشتن دیو را بر آب

<div class="b" id="bn1"><div class="m1"><p>بگفت و همان دم به جا برنشست</p></div>
<div class="m2"><p>که در آب او را کند زود پست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گردن زدش سام یل خنجری</p></div>
<div class="m2"><p>که آزاد برجست در داوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگفتا برون رفت باید همی</p></div>
<div class="m2"><p>و یا غرق خون رفت باید همی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگفت ار کنی پاره پاره مرا</p></div>
<div class="m2"><p>نجنبم از این آب ای سرورا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی سام سوگند خورد آن زمان</p></div>
<div class="m2"><p>به ارواج گرشسب روشن روان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اترط به جمشید فرخنده پی</p></div>
<div class="m2"><p>به تاج و به تخت منوچهر کی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که گر برنگردی بگردانمت</p></div>
<div class="m2"><p>همه کتف و شانه بدرانمت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی‌رفت آن دیو از خیرگی</p></div>
<div class="m2"><p>زدش سام یل خنجر از زیرکی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نشست اندر اندام او یک وجب</p></div>
<div class="m2"><p>بلرزید بر خویش دیو از غضب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفتا که ساما مزن خنجرم</p></div>
<div class="m2"><p>که با خود دمی مصلحت بنگرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روان کرد آن خنجرش در غلاف</p></div>
<div class="m2"><p>ولی شاخ او داشت از روی لاف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفتا اگر من معلق زنم</p></div>
<div class="m2"><p>ترا اندرین بحر آب افکنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شود طعمه ماهیان پیکرت</p></div>
<div class="m2"><p>برند هر یکی پاره‌ای از برت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که داری که آرد درین جا مدد</p></div>
<div class="m2"><p>جوابم بگو ای یل پر خرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگفت ای نهنکال شاخ ترا</p></div>
<div class="m2"><p>رها کی کنم اندرین ماجرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو خواهی بخواب و تو خواهی نشین</p></div>
<div class="m2"><p>تو دانی و این دشن? سهمگین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برآورد خنجر دگر از نیام</p></div>
<div class="m2"><p>که تا بردراند تن دیو خام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نهنکال در دم فغان کرد باز</p></div>
<div class="m2"><p>که ای پهلوان سام گردن‌فراز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کنم رحم بر تو جوانی هنوز</p></div>
<div class="m2"><p>نداری تو رحم ای یل کین‌فروز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگفت و بخوابید دیو لعین</p></div>
<div class="m2"><p>در آن توی دریای ماچین و چین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یکی غوطه‌ خورد آن زمان سام شیر</p></div>
<div class="m2"><p>برآورد خنجر سبک آن دلیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بزد بر سر دیوک تیره کار</p></div>
<div class="m2"><p>که آزاد برجست آن خیره کار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باستاد از ترس خنجر به پا</p></div>
<div class="m2"><p>بگفتا که ای سام کشتی مرا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بسی زخم بر من زدی در نبرد</p></div>
<div class="m2"><p>درآوردی این لشکرم را به گرد</p></div></div>