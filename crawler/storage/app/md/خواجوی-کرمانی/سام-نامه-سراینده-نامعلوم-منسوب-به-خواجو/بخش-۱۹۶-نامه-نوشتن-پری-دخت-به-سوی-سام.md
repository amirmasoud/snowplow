---
title: >-
    بخش ۱۹۶ - نامه نوشتن پری‌دخت به سوی سام
---
# بخش ۱۹۶ - نامه نوشتن پری‌دخت به سوی سام

<div class="b" id="bn1"><div class="m1"><p>گشادند دست پری‌دخت ماه</p></div>
<div class="m2"><p>یکی نامه بنوشت بر رزمخواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر نامه نام خدای جهان</p></div>
<div class="m2"><p>که او کام داده همی بر مهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمان و زمین و سپهر آفرید</p></div>
<div class="m2"><p>درشتی و نرمی و مهر آفرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازو آفرین باد بر سام یل</p></div>
<div class="m2"><p>که آمد سوی دیو همچون اجل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یک رزم ترسید ازو نره دیو</p></div>
<div class="m2"><p>به در رفت از چنگ سالار نیو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون لابه و پوزش آراسته است</p></div>
<div class="m2"><p>پی مهربانیت برخواسته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فراموش کرده همه بدخوئی</p></div>
<div class="m2"><p>شده چاکر سام بازوقوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو هم رو زمیدان رزمش بتاب</p></div>
<div class="m2"><p>درین کینه چندین میاور شتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان کن که آن رای مردان بود</p></div>
<div class="m2"><p>لب بخت از آن کار خندان بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخن در لباس آوریدم میان</p></div>
<div class="m2"><p>به نزدیک سالار ایرانیان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گمانش که از مهربانی نوشت</p></div>
<div class="m2"><p>به نزدیک آن پهلوانی نوشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندانست آن دیو وارونه‌رای</p></div>
<div class="m2"><p>که از چیست گفتار آن دلربای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی مهر بر گوشه نامه کرد</p></div>
<div class="m2"><p>که یعنی ز دیوان برآور تو گرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بشد شاد از ماهرو ابرها</p></div>
<div class="m2"><p>که از رزم و پیکار گشتم رها</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مر آن نامه برداشت دیوی نهاد</p></div>
<div class="m2"><p>که در دم رساند بر پاک‌زاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وز آن روی برگشت از رزم سام</p></div>
<div class="m2"><p>سوی گرد قلواد بگذارد گام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو سر به سر داستان بازگفت</p></div>
<div class="m2"><p>که قلواد از آن رزم شد در شگفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسی آفرین کرد بر پهلوان</p></div>
<div class="m2"><p>که برگشت از آن دیو تیره روان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدو گفت یزدان تو را یار باد</p></div>
<div class="m2"><p>تن دشمنت پر ز آزار باد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مبیناد چشم تو هرگز بدی</p></div>
<div class="m2"><p>که پیوسته در کامرانی بدی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درین گفتگو بود قلواد شیر</p></div>
<div class="m2"><p>ازو شادمان جان آن شیرگیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که دیوی درآمد زبان برگشاد</p></div>
<div class="m2"><p>زمین را ببوسید و نامه بداد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سپهبد چو مهر پری‌دخت دید</p></div>
<div class="m2"><p>همه درد و اندوه پردخت دید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سراسر بخواندش سپهدار گو</p></div>
<div class="m2"><p>دلش شادمان شد روانش به نو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدانست گفتار او را که چیست</p></div>
<div class="m2"><p>که یعنی کمر بند و آنجا مایست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شتابان درآ سوی این کوهسار</p></div>
<div class="m2"><p>ازین دیو جادو برآور دمار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بخندید و بوسید و بر سر نهاد</p></div>
<div class="m2"><p>بدو دیو گفت آن که ای پاک‌زاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که دل را تهی کردم از ابرها</p></div>
<div class="m2"><p>ز چنگال تیزم شد ایدر رها</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به یزدان که او را نیارم به تیغ</p></div>
<div class="m2"><p>در کین ببستم به یک ره دریغ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گذشتم ز کینش چو خواهش نمود</p></div>
<div class="m2"><p>به خواهشگری ماهرو برفزود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ولیکن از ایدر مرا ره‌نما</p></div>
<div class="m2"><p>بدان تا ببینم رخ دلربا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر آن چیز کو خود بگوید تمام</p></div>
<div class="m2"><p>نگردد از آن هیچ فرخنده سام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سر و جان و تن زیر فرمان اوست</p></div>
<div class="m2"><p>همه ملک جانم به پیمان اوست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدو گفت آن دیو کای پهلوان</p></div>
<div class="m2"><p>کمربسته در رزم دیو دمان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نتابد به میدان تو ابرها</p></div>
<div class="m2"><p>تن هیچ کس ناید از تو رها</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به پیمان تو را می‌برم سوی کوه</p></div>
<div class="m2"><p>که دیوی ز رزمت نیاید ستوه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پس آنگه مرادت درآید به دست</p></div>
<div class="m2"><p>نشینی و از وصل گردی تو مست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به پاسخ بدو گفت سام سوار</p></div>
<div class="m2"><p>که ای دیو اندیشه در دل مدار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بگفت و ز جا جست سالار شیر</p></div>
<div class="m2"><p>کمر بر میان بست مرد دلیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به قلواد جنگی چنین گفت کو</p></div>
<div class="m2"><p>که ایدر یکی چاره آمد ز نو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پری‌دخت رنگی بیاراسته است</p></div>
<div class="m2"><p>همه بخت افتاده برخاسته است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تو جا را نگه دار در پای کوه</p></div>
<div class="m2"><p>که سازم تن دیو جنگی ستوه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به زابل زبانی بدو راز گفت</p></div>
<div class="m2"><p>همه چاره ماهرو باز گفت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بپوشید آنگاه خفتان جنگ</p></div>
<div class="m2"><p>به پیکار آن دیو شد تیزچنگ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نشست از بر اسب پولاد سم</p></div>
<div class="m2"><p>خروشید ماننده گاو دم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همان دیو همراه آمد به راه</p></div>
<div class="m2"><p>بر آن کوه بر شد ستور سیاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>غرابش ببریده کوه فنا</p></div>
<div class="m2"><p>خروشید ماننده اژدها</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو آمد بر آن کوه‌پایه فراز</p></div>
<div class="m2"><p>گره در برو پهلو رزمساز</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به هر سوی انبوه دیوان بدید</p></div>
<div class="m2"><p>سراسر ز کینه غریوان بدید</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خدای جهان را همی یاد کرد</p></div>
<div class="m2"><p>کزو بود فیروز روز نبرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بنالید بر داور داوران</p></div>
<div class="m2"><p>کزو بود پیکار نام‌آوران</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همی گفت کای داور کردگار</p></div>
<div class="m2"><p>تو آگاهی از گردش روزگار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که چندین ستم دیدم از روزگار</p></div>
<div class="m2"><p>تو دانی ایا داور کردگار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کنونم درین رزم فیروز ساز</p></div>
<div class="m2"><p>سر دیو جنگی درآرم به گاز</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>که بی رای تو دیده را نور نیست</p></div>
<div class="m2"><p>تن و جانم از گردش هور نیست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سر دیو گمره درآور به بند</p></div>
<div class="m2"><p>تو کردی زمین پست و گردون بلند</p></div></div>