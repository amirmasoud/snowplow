---
title: >-
    بخش ۱۲۹ - رفتن قمرتاش و قلواد به طلب سام نریمان
---
# بخش ۱۲۹ - رفتن قمرتاش و قلواد به طلب سام نریمان

<div class="b" id="bn1"><div class="m1"><p>دگر ره قمرتاش و قلواد شیر</p></div>
<div class="m2"><p>براندند اسب آن دو مرد دلیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نجستند روز و شب آرام و خواب</p></div>
<div class="m2"><p>به هر راه و بیره گرفته شتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خبر جوی گشتند هر جا بسی</p></div>
<div class="m2"><p>نیامد ازو آگهی از کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هر دشت و هامون همی تاختند</p></div>
<div class="m2"><p>علم بر سر کوهی افراختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین تا برآمد بدین سان دو ماه</p></div>
<div class="m2"><p>نماند هیچ دشت و بیابان و راه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که ایشان نکردند از آنجا گذر</p></div>
<div class="m2"><p>نجستند از شیر فرخ خبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین تا به دامان یک کوهسار</p></div>
<div class="m2"><p>شتابان رسیدند هر دو سوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدیدند کوهی سرش بر فلک</p></div>
<div class="m2"><p>شده دیده‌بان بر سر کُه ملک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زحل چون کلاغی به بالای او</p></div>
<div class="m2"><p>زمانه نهان زیر پهنای او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سراسر همه سنبل و لاله‌زار</p></div>
<div class="m2"><p>ز مرغان همه گوشه‌ای ناله زار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دو فرسخ بدان کوه رفتند باز</p></div>
<div class="m2"><p>بر افراز رفتند دو سرفراز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز لعل و ز یاقوت و فیروزه کان</p></div>
<div class="m2"><p>بدیدند آن هر دو آزادگان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدیدند هر سو فراوان بلور</p></div>
<div class="m2"><p>که بودی درخشنده مانند هور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به ناگه یکی گنبدی یافتند</p></div>
<div class="m2"><p>شتابان بدان قبه بشتافتند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برهمن یکی پر هنر پیر بود</p></div>
<div class="m2"><p>کهنسال و بسیار تدبیر بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گیا بسته بر دو سر چون عذار</p></div>
<div class="m2"><p>پرستش کنان پیش پروردگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو دیدند آن پیر را هردوان</p></div>
<div class="m2"><p>به تن لاغر و لیک روشن‌روان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پیاده شدند و برفتند پیش</p></div>
<div class="m2"><p>ستایش نمودند به آئین و کیش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که ای پیر بیدار فرخنده دل</p></div>
<div class="m2"><p>خردمند و آزاده و زنده دل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>درین که چگونه به سر می‌بری</p></div>
<div class="m2"><p>نه یاری و نه همدم و یاوری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیائی چرا سوی آباد شهر</p></div>
<div class="m2"><p>که از ناز و نعمت بیابی تو بهر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدین تلخ کامی چنین زیستن</p></div>
<div class="m2"><p>بباید به حال تو بگریستن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه نیکوست تنها درین کوهسار</p></div>
<div class="m2"><p>که تنها نباشد بجز کردگار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چنین داد پاسخ برهمن بدوی</p></div>
<div class="m2"><p>که ای نامداران پرخاشجوی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که گیتی سرائی است پر ریو و رنگ</p></div>
<div class="m2"><p>خردمند مردم نسازد درنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دو در دارد این باغ در رهگذر</p></div>
<div class="m2"><p>ازین در درآئی روی زان به در</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر آن کو درین کاخ منزل کند</p></div>
<div class="m2"><p>دو پای دل خویش در گل کند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رهائی نیابد ازین کاخ باز</p></div>
<div class="m2"><p>بماند تنش در غم رنج و آز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کسی جان کند جمع آرد و بال</p></div>
<div class="m2"><p>دلش خوش کند بر زر و سیم و مال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدان تا که دارند او را عزیز</p></div>
<div class="m2"><p>ببیند ستاده غلام و کنیز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو گردد بدین ساز و آئین و برگ</p></div>
<div class="m2"><p>به ناگه شبیخون درآید ز مرگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>امانش نبخشد همی یک زمان</p></div>
<div class="m2"><p>نه بخشد به مال و نه بدهد امان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به ناچار جان را سپارد دگر</p></div>
<div class="m2"><p>دلش در پی خانه و سیم و زر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نباشد چو آزاده نیک رای</p></div>
<div class="m2"><p>به دوزخ شتابد روانش به جای</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دگر زان که چیزش نیاید به دست</p></div>
<div class="m2"><p>شود از پریشانی حال پست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شب و روز در فکر و طغیان بود</p></div>
<div class="m2"><p>به فکر همان لقمه نان بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همان به که کنجی گزیند به دهر</p></div>
<div class="m2"><p>شود پیش دادار چون نیک‌بهر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو در زندگی هر چه بودش بهشت</p></div>
<div class="m2"><p>پس از مرگ جایش بود در بهشت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از آن گشت ما را غم از دل تهی</p></div>
<div class="m2"><p>جز این رو نداریم هیچ آگهی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدو گفت قلواد کای پرخرد</p></div>
<div class="m2"><p>ازین بیش اندیشه برنگذرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>برهمن چنین پاسخش داد باز</p></div>
<div class="m2"><p>چهار است نیکی تو ای سرفراز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نخستین زبان خوش و مهربان</p></div>
<div class="m2"><p>بدان تا که هرگز نیابی زیان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دوم راست گو و سیم راست باش</p></div>
<div class="m2"><p>چو دره گهی سوی مردم بپاش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چهارم توکل به دادار کن</p></div>
<div class="m2"><p>ازین پیر فرخنده بشنو سخن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دگر چارچیز است کار بدی</p></div>
<div class="m2"><p>که تابد تو را از ره ایزدی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دو رنگی و حق ناشناسی بود</p></div>
<div class="m2"><p>بر این رو همه ناسپاسی بود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سیم بخل کو از همه بدتر است</p></div>
<div class="m2"><p>وگر دور باشی ازو درخور است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چهارم دل کس میازار هیچ</p></div>
<div class="m2"><p>که بدتر نباشد ز آزار هیچ</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو اینها بجا آوری شاد باش</p></div>
<div class="m2"><p>ز بند غم خویش آزاد باش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دگر گفت قلواد کای نیک مرد</p></div>
<div class="m2"><p>که از گفتت جانم آمد به درد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سوال دگر دارم از مرد پیر</p></div>
<div class="m2"><p>که هر چیز گفتی بود دلپذیر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مرا سروری هست با ناز و کام</p></div>
<div class="m2"><p>که خوانند او را سپهدار سام</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به باغ خرد آشیان داشته است</p></div>
<div class="m2"><p>هنر زو به عالم سرافراشته است</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو طوطی که شکر ازو دور شد</p></div>
<div class="m2"><p>دلش سوی پرواز مزدور شد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو بازی گه گردد ز شه خشمگین</p></div>
<div class="m2"><p>هوائی شد و باز نامد به کین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>و یا همچو بلبل که گل را ندید</p></div>
<div class="m2"><p>بپرید از باغ و سر درکشید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو سیمرغ کز قاف پرواز کرد</p></div>
<div class="m2"><p>هوائی شد و رفتن آغاز کرد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ندیدیم او را به جائی نشان</p></div>
<div class="m2"><p>شدیم از غمش دیده گوهرفشان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تو را گر خبر هست ما را بگوی</p></div>
<div class="m2"><p>نشانی ده از سام پرخاشجوی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>پری‌دخت را هم ندیده است باز</p></div>
<div class="m2"><p>وزین درد شد جان ما درگذار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ندانیم کاین هر دو را روزگار</p></div>
<div class="m2"><p>کجا برد و چونست انجام کار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تن ما شد از تاختن کوفته</p></div>
<div class="m2"><p>ز بسیاری رنج آشوفته</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بدو گفت آنگاه پس برهمن</p></div>
<div class="m2"><p>که ای نامداران شمشیر زن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بگویم همه کار و کردار سام</p></div>
<div class="m2"><p>که چونست انجام سالار سام</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ورا رنج بسیار پیش اندرست</p></div>
<div class="m2"><p>همه نوش او زیر نیش اندرست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>از اینجا به ده روز راه دراز</p></div>
<div class="m2"><p>چو رانید هر دو به درد و گداز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ببینید ناگه یکی ساربان</p></div>
<div class="m2"><p>خردمند پیری و شیرین‌زبان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>فراوان هیون بینی آنجا گله</p></div>
<div class="m2"><p>در آن دشت و آن چشمه گشته یله</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خبر زو بپرسید زان رزمساز</p></div>
<div class="m2"><p>که دارد خبرهای او جمله باز</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ببینید سرچشمه لاجورد</p></div>
<div class="m2"><p>زمینش ز لاله شده سرخ و زرد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>دگر از پری‌دخت گوئی سخن</p></div>
<div class="m2"><p>فتاده به چنگ یکی اهرمن</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>کجا نام آن شوم شد ابرها</p></div>
<div class="m2"><p>نیابد ازو اژدها خود رها</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>یکی خیره‌سر دیو بسیار هوش</p></div>
<div class="m2"><p>که از ژرف دریا برآرد خروش</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>هزاران هزارش بود لشکری</p></div>
<div class="m2"><p>همه دیو جادوی با داوری</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>طلسم است بر دست آن نابکار</p></div>
<div class="m2"><p>که جمشید بسته است در روزگار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>وز آن چار عنصر نهادست نام</p></div>
<div class="m2"><p>ببسته است بر نام فرخنده سام</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ز خاک و ز آتش ز آب و ز باد</p></div>
<div class="m2"><p>بیاراست جمشید فرخ نهاد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>کنون ابرها دارد آنجا نشست</p></div>
<div class="m2"><p>ابا نره دیوان جادوپرست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بیاید سپهدار فرخ به اسم</p></div>
<div class="m2"><p>بزودی به هم بر زند آن طلسم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>طلسم دگر باز پیدا کند</p></div>
<div class="m2"><p>ز بهر گر کس هویدا کند</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>که ازتخم آن نامداران بوند</p></div>
<div class="m2"><p>که یک دم ز کینه همی نغنود</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>پس از سال نهصد دگر سی و پنج</p></div>
<div class="m2"><p>درآید یکی مرد جوینده گنج</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>درآید طلسم سپهبد به دست</p></div>
<div class="m2"><p>ازو چاره سام یابد شکست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بسی رنج دارد جهاندار سام</p></div>
<div class="m2"><p>که تا کار مردی بسازد تمام</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>پس آنگه پری‌دخت سیمین عذار</p></div>
<div class="m2"><p>به دست اندر آرد گو نامدار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>شتابید از ایدر به نزدیک او</p></div>
<div class="m2"><p>که روشن شود جان تاریک او</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>زمین بوسه دادند هر دو جوان</p></div>
<div class="m2"><p>بجستند از جادو روشن روان</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>وز آن کوهپایه به زیر آمدند</p></div>
<div class="m2"><p>شتابان سوی سام شیر آمدند</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>براندند آن هر دو شیر شکار</p></div>
<div class="m2"><p>چنین تا پدید آمد آن چشمه‌سار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>هوائی بدیدند همچون بهشت</p></div>
<div class="m2"><p>تو گفتی که از مشک دارد سرشت</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>یله گشته هر سو فراوان شتر</p></div>
<div class="m2"><p>ز کوهان اشتر جهان گشته پر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بزرگان گردنکش سرفراز</p></div>
<div class="m2"><p>همه همچو کشتی همه چون جهاز</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>همه ژاژخواه گشته اما خموش</p></div>
<div class="m2"><p>برهنه ولی جمله پشمینه پوش</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ز مستی برآورده بر چرخ سر</p></div>
<div class="m2"><p>نهاده سر اندر پی یکدگر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>یکایک کف انداز گشته شگرف</p></div>
<div class="m2"><p>تو گفتی در آن دشت باریده برف</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>به دنبال ایشان یکی ساربان</p></div>
<div class="m2"><p>نهاده سر اندر پی اشتران</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بدو گفت قلواد ناگه یکی</p></div>
<div class="m2"><p>کزو باز پرسد خبر اندکی</p></div></div>