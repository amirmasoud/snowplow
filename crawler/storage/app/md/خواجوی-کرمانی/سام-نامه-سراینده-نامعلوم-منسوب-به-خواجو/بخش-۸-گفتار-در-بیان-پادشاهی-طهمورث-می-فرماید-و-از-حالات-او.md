---
title: >-
    بخش ۸ - گفتار در بیان پادشاهی طهمورث می‌فرماید و از حالات او
---
# بخش ۸ - گفتار در بیان پادشاهی طهمورث می‌فرماید و از حالات او

<div class="b" id="bn1"><div class="m1"><p>زمانه ندادش زمانی درنگ</p></div>
<div class="m2"><p>شد آن روز هوشنگ و آن فرهنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نپیوست خواهد جهان با تو مهر</p></div>
<div class="m2"><p>نه نیز آشکارا نمایدت چهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پسر بد مر او را یکی هوشمند</p></div>
<div class="m2"><p>گرانمایه طهمورث دیو بند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیامد به تخت پدر بر نشست</p></div>
<div class="m2"><p>به شاهی کمر بر میان برببست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه موبدان را زلشکر بخواند</p></div>
<div class="m2"><p>به خوبی چه مایه سخنها براند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین گفت کامروز تخت و کلاه</p></div>
<div class="m2"><p>مرا زیبد آن تاج و آئین و گاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان از بدیها بشویم به رای</p></div>
<div class="m2"><p>پس آنگه درنگی کنم کرد پای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زهر جای کوته کنم دست دیو</p></div>
<div class="m2"><p>که من بود خواهم جهان را خدیو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر آن چیز کاندر جهان سودمند</p></div>
<div class="m2"><p>کنم آشکارا گشایم ز بند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس از پشت میش و بره پشم و موی</p></div>
<div class="m2"><p>برید و به رشتن نهادند روی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به کوشش ازو کرد پوشش به جای</p></div>
<div class="m2"><p>به گستردنی بد هم او رهنمای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز پویندگان هر چه بد پیش رو</p></div>
<div class="m2"><p>خورش کردشان سبزه و کاه و جو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رمنده ددان را همه بنگرید</p></div>
<div class="m2"><p>سیه‌گوش و یوز از میان برگزید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به چاره بیاوردش از دشت و کوه</p></div>
<div class="m2"><p>به تنگ آمدند زانکه بد زان گروه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز مرغان همه آنکه بد نیک ساز</p></div>
<div class="m2"><p>چو باز و چو شاهین گردن فراز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیاموخت و آموختن‌شان گرفت</p></div>
<div class="m2"><p>جهانی ازو ماند اندر شگفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بفرمودشان تا نوازند گرم</p></div>
<div class="m2"><p>نخوانندشان جز به آواز نرم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو این کرده شد ماکیان و خروس</p></div>
<div class="m2"><p>کجا برخروشند گه زخم کوس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به چاره به نزدیک مردم کشید</p></div>
<div class="m2"><p>نهفته همه سودمندی گزید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنین گفت کین را نیایش کنید</p></div>
<div class="m2"><p>جهان آفرین را ستایش کنید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که او دادمان بر ددان دستگاه</p></div>
<div class="m2"><p>ستایش مر او را که بنمود راه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مر او را یکی پاک دستور بود</p></div>
<div class="m2"><p>که رایش ز کردار بد دور بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گزیده به هر جای و شیداسپ نام</p></div>
<div class="m2"><p>نزد جز به نیکی به هر جای گام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه روز بسته ز خوردن دو لب</p></div>
<div class="m2"><p>به پیش جهاندار بر پای شب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنان بر دل هر کسی بود دوست</p></div>
<div class="m2"><p>نماز شب و روزه آئین اوست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سر مایه بد اختر شاه را</p></div>
<div class="m2"><p>در بند بد جای بدخواه را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همه راه نیکی نمودی به شاه</p></div>
<div class="m2"><p>همه راستی خواستی پایگاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنان شاه پالوده گشت از بدی</p></div>
<div class="m2"><p>که تابید ازو فره ایزدی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همان اهرمن را به افسون ببست</p></div>
<div class="m2"><p>چو بر تندرو بارگی برنشست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زمان تا زمان زینش برساختی</p></div>
<div class="m2"><p>همی گرد گیتیش برتاختی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو دیوان بدیدند کردار اوی</p></div>
<div class="m2"><p>کشیدند گردن ز گفتار اوی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شدند انجمن دیو بسیار مر</p></div>
<div class="m2"><p>که پردخت مانند ازو گاه فر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو طهمورث آگه شد از کارشان</p></div>
<div class="m2"><p>برآشفت و بشکست بازارشان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به عزم جهاندار بستش میان</p></div>
<div class="m2"><p>به گردن برآورد گرز گران</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همه نیزه‌داران و افسون‌گران</p></div>
<div class="m2"><p>برفتند جادو سپاهی گران</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دمنده سیه دیوشان پیش رو</p></div>
<div class="m2"><p>همی به آسمان برکشیدند غو</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جهاندار طهمورث پاک دین</p></div>
<div class="m2"><p>بیامد کمربسته بر رزم و کین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یکایک بیاراست با دیو جنگ</p></div>
<div class="m2"><p>نبد جنگشان را فراوان درنگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ازیشان دو بهره به افسون ببست</p></div>
<div class="m2"><p>دگرشان به گرز گران کرد پست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کشیدندشان کشته و بسته خوار</p></div>
<div class="m2"><p>به جان خواستند از زمان زینهار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که ما را مکش تا یکی نوهنر</p></div>
<div class="m2"><p>بیاموزی از ماکت آید به بر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یکی نامور دادشان زینهار</p></div>
<div class="m2"><p>بدان تا نهانی کنند آشکار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو آزادشان شد سر از بند او</p></div>
<div class="m2"><p>بجستند ناچار پیوند او</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نوشتن به خسرو بیاموختند</p></div>
<div class="m2"><p>دلش را به دانش بیفروختند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نبشته همانا که نزدیک سی</p></div>
<div class="m2"><p>چه رومی چه تازی و چه پارسی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چه سغدی چه چینی و چه پهلوی</p></div>
<div class="m2"><p>نگارنده آن کجا بشنوی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>جهان‌دار سی‌سال ازین بیشتر</p></div>
<div class="m2"><p>چگونه پدید آوریدی هنر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>برفت او و سر شد برو روزگار</p></div>
<div class="m2"><p>همه رنج او ماند ازو یادگار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جهان را مپرور چه خواهی درود</p></div>
<div class="m2"><p>چو می‌بدروی پروریدن چه سود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>برآری یکی را به چرخ بلند</p></div>
<div class="m2"><p>بیاریش ناگه به خاک نژند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گرانمایه جمشید فرزند اوی</p></div>
<div class="m2"><p>کمر بست و یکدل پُر از پند اوی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>برآمد بران تخت فرخ پدر</p></div>
<div class="m2"><p>به رسم کیان بر سرش تاج زر</p></div></div>