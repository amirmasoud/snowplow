---
title: >-
    بخش ۶۱ - مناظره کردن پری‌دخت با سام
---
# بخش ۶۱ - مناظره کردن پری‌دخت با سام

<div class="b" id="bn1"><div class="m1"><p>مه قندهار آفتاب طراز</p></div>
<div class="m2"><p>بت قندلب دلبر دلنواز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قصب پوش ماه و گره‌گیر موی</p></div>
<div class="m2"><p>گل اندام سرو سمن برگ‌روی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فروزنده خورشید طوبی خرام</p></div>
<div class="m2"><p>خرامنده طاووس طوطی خرام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگارین نسرین بر و سروقد</p></div>
<div class="m2"><p>پری‌دخت مه‌پیکر زهره خد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر درج لولوی تر برگشود</p></div>
<div class="m2"><p>در شهد و تنگ شکر برگشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به تنگ شکر نرخ شکر شکست</p></div>
<div class="m2"><p>به درج گهر قدر گوهر شکست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بیغاره گفت ای سر سرکشان</p></div>
<div class="m2"><p>که هم شه‌نشانی و هم شه‌نشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شه شرق فراش خلوتگهت</p></div>
<div class="m2"><p>سپهر برین خاکبوس رهت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو مهری و ماه سپهرت غلام</p></div>
<div class="m2"><p>کند مهر هر مه به برجی مقام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو عمری و با کس نپائی چو عمر</p></div>
<div class="m2"><p>چو رفتی دگر بازنائی چو عمر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو بادی به سودای شب میوزی</p></div>
<div class="m2"><p>که هر لحظه در بوستانی خزی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو سروی و مثل تو سروی نخاست</p></div>
<div class="m2"><p>گرانبارم، آزاد سازی رواست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو جانی و با غم چه دارم ترا</p></div>
<div class="m2"><p>برو تا به یزدان سپارم ترا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به چشم چو آهو مکن روبهی</p></div>
<div class="m2"><p>مرا خواب خرگوش تا کی دهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر آهوی من نام شیری مبر</p></div>
<div class="m2"><p>ز آهوی من شیرگیری نگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیا گر سیه کاریت آرزوست</p></div>
<div class="m2"><p>ز زلفم بیاموز کین کار اوست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر همچو جانم شوی مهره‌باز</p></div>
<div class="m2"><p>مکن این سخن پیش زلفم دراز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به طرازی تا سر برآورده‌ای</p></div>
<div class="m2"><p>کجی را تو سرمایه‌ات کرده‌ای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز زلفم بیاموز کج باختن</p></div>
<div class="m2"><p>به ناراستی سر برافراختن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برو دست ازین جعد مشکین مدار</p></div>
<div class="m2"><p>وگرنه به شوریدگی سر برآر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>منه دل برین زلف پرتاب و پیچ</p></div>
<div class="m2"><p>چو دیوانه بر مار افعی مپیچ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>میا پیش این نرگس می‌پرست</p></div>
<div class="m2"><p>که ترکست و بدمست و خنجر به دست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو سازی ز مشکن کمندم گره</p></div>
<div class="m2"><p>که کار تو زان می‌فتد در گره</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به بار من ار نبودت دسترس</p></div>
<div class="m2"><p>که از سروبن بر نخوردست کس</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به طراری زلفم از ره مرو</p></div>
<div class="m2"><p>بدین ریسمان پاره در چه مرو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مبر نام دل، آخرت ننگ نیست</p></div>
<div class="m2"><p>که این جنس در شهر ما تنگ نیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بگفت این و گرداند رخ را ز سام</p></div>
<div class="m2"><p>درون شد به قصرش چو ماه تمام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سهی سرو بستان آزادگان</p></div>
<div class="m2"><p>صف‌آرای میدان دل‌دادگان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شب‌افروز ایوان روشن دلان</p></div>
<div class="m2"><p>مه شب‌روان قبله مقبلان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گل باغ شوق اختر برج عشق</p></div>
<div class="m2"><p>شه ملک غم گوهر درج عشق</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یل کامجو سام آشفته کار</p></div>
<div class="m2"><p>پراکنده احوال از عشق یار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو از مهر آن ماه برداشت دل</p></div>
<div class="m2"><p>به ناکام بگذشت و بگذاشت دل</p></div></div>