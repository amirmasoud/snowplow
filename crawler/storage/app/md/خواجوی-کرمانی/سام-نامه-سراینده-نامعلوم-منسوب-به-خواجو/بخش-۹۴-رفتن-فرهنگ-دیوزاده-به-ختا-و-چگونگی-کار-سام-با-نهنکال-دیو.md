---
title: >-
    بخش ۹۴ - رفتن فرهنگ دیوزاده به ختا و چگونگی (کار) سام با نهنکال دیو
---
# بخش ۹۴ - رفتن فرهنگ دیوزاده به ختا و چگونگی (کار) سام با نهنکال دیو

<div class="b" id="bn1"><div class="m1"><p>سر بادبان چون شد اندر هوا</p></div>
<div class="m2"><p>روان گشت زورق به راه ختا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین گفتگوها شب اندر رسید</p></div>
<div class="m2"><p>دو لشکر در آن جایگه آرمید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز کشتی همه لنگر انداختند</p></div>
<div class="m2"><p>ز هر در حکایت همی خواستند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نهادند اصل سخن را بر آن</p></div>
<div class="m2"><p>که در شب گریزند کندآوران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفتند تدبیر این است و بس</p></div>
<div class="m2"><p>اگر کس نیاید شما را زپس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفتند اندر دل شب رویم</p></div>
<div class="m2"><p>که دیویم ما سر به سر شب رویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو یک پاس از تیره شب درگذشت</p></div>
<div class="m2"><p>شب آهنگ بر چرخ گردان بگشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن لشکر دیو ببد ده‌هزار</p></div>
<div class="m2"><p>نکردند اندر دل شب قرار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو پنجاه کشتی بد از آمدن</p></div>
<div class="m2"><p>کجا ده بماندند از آن انجمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>براندند کشتی چو باد بهار</p></div>
<div class="m2"><p>ازیشان همه بیخبر نامدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو از شرفه بام نیلی حصار</p></div>
<div class="m2"><p>پدید آمد ان خشت زرین‌نگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو قلوش چو قلواد دو پهلوان</p></div>
<div class="m2"><p>رسیدند آن دم بر پهلوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که از لشکر دیو یک تن نماند</p></div>
<div class="m2"><p>که نکبت بر ایشان فلک برنشاند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هزیمت گرفتند دیوان نر</p></div>
<div class="m2"><p>کسی نیست ز ایشان درین بحر و بر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو بشنید سام نریمان‌نژاد</p></div>
<div class="m2"><p>روان سجده شکر کردش چو باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ستایش بسی کرد بر کردگار</p></div>
<div class="m2"><p>که ای آفریننده مور و مار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بجز تو کسی‌ام خداوند نیست</p></div>
<div class="m2"><p>کسی کین نداند خردمند نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا ده توانائی و فرهی</p></div>
<div class="m2"><p>که هستی خداوند، من چون رهی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سر از سجده شکر برداشت باز</p></div>
<div class="m2"><p>بزد نعره آن گرد گردنفراز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که ای پهلوانان جنگ آوران</p></div>
<div class="m2"><p>بدانید یکسر ز پیر و جوان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همین دم نهنکال در می‌رسد</p></div>
<div class="m2"><p>ابا گرز و گوپال و دیوان ز ره</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ببینیم تا گردش روزگار</p></div>
<div class="m2"><p>چه آرد به ما بر ازین تیره‌کار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دل اندر خداوند بندید و بس</p></div>
<div class="m2"><p>که جز او نداریم فریادرس</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بگفت و توکل به دادار کرد</p></div>
<div class="m2"><p>برآورد از دل همی آه سرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که آیا پری‌دخت ماهم کجاست</p></div>
<div class="m2"><p>ندانم درین بحر راهم کجاست</p></div></div>