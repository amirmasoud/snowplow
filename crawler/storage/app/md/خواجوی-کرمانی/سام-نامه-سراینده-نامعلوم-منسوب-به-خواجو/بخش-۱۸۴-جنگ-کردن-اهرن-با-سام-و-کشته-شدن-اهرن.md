---
title: >-
    بخش ۱۸۴ - جنگ کردن اهرن با سام و کشته شدن اهرن
---
# بخش ۱۸۴ - جنگ کردن اهرن با سام و کشته شدن اهرن

<div class="b" id="bn1"><div class="m1"><p>یکی نعره زد اهرمن اهرمن</p></div>
<div class="m2"><p>که در لرزه آمد زمین و زمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگفتا منم اهرن مغربی</p></div>
<div class="m2"><p>هر انگشت دستم بود عقربی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز کهسار قافم زبان بی‌گزاف</p></div>
<div class="m2"><p>نیارم که گویم ز خود هیچ لاف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پی رزم و پیکار سام آمدم</p></div>
<div class="m2"><p>در آورد جویائی نام آمدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز من سام یل را دهید آگهی</p></div>
<div class="m2"><p>که آمد سواری ابا فرهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک را به میدان کنم سرنگون</p></div>
<div class="m2"><p>برانم به هر سو یکی جوی خون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه دشت از کشته سازم چو کوه</p></div>
<div class="m2"><p>ز نر اژدها من نگردم ستوه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگفت و به لشکر یکی حمله کرد</p></div>
<div class="m2"><p>زمین را ز خون کرد چون لاله زرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به شمشیر سیصد تن افکنده خوار</p></div>
<div class="m2"><p>ز طنجه جوانان نیزه‌گذار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو سام نریمان چنان حال دید</p></div>
<div class="m2"><p>همی غلغل نامداران شنید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدانست پیکار اهرن سوار</p></div>
<div class="m2"><p>بدو حمله آورد در کارزار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی در خروشید کای بدنژاد</p></div>
<div class="m2"><p>نمانی تو و نیر شداد عاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز گردان طنجه چرا کشته‌ای</p></div>
<div class="m2"><p>ز مین را به خونشان بیاغشته‌ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ندیدی ز خود دست بالای دست</p></div>
<div class="m2"><p>از آن رو به میدان شدی پیل مست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ولیکن جان بر تو تیره کنم</p></div>
<div class="m2"><p>به شداد چنگال خیره کنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>منم سام پور نریمان گرد</p></div>
<div class="m2"><p>که رنگ از رخ نره شیران ببرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شنیدی چه کردم به طلاج دیو</p></div>
<div class="m2"><p>ز جانش چگونه برآمد غریو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به میدان چگونه بکشتم شدید</p></div>
<div class="m2"><p>به یک تیغم از دهر شد ناپدید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همان رزم ارقم شنیدی دگر</p></div>
<div class="m2"><p>که آمد ز زخمم چسان او به سر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدو گفت گفتار بیهوده بس</p></div>
<div class="m2"><p>به من گر بود مر تو را دسترس</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پس آنگه به میدان توئی شیر کین</p></div>
<div class="m2"><p>که از پشت زینم زنی بر زمین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگفت و عمود گران سنگ را</p></div>
<div class="m2"><p>برآورد و آراست دو چنگ را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بزد گرز بر تارک پهلوان</p></div>
<div class="m2"><p>که میدان از آن زخم آمد نوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بپیچید آواز بر چرخ پیر</p></div>
<div class="m2"><p>جهان پر شد از نعره دار و گیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دگر حمله آورد بر سوی سام</p></div>
<div class="m2"><p>بدو داد پهلو در آورد نام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دویم گرز را کوفت آن بدگهر</p></div>
<div class="m2"><p>که شد پهن در دست پهلو سپر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سوم حمله آورد بر سوی پشت</p></div>
<div class="m2"><p>از آن حمله‌ها باد بودش به مشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بخندید ازو سام نیرم‌نژاد</p></div>
<div class="m2"><p>که ای بی‌هنر خیره پر ز باد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یکی گرز از بازوی من بگیر</p></div>
<div class="m2"><p>بدان تا بدانی تو بازوی شیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بگفت و درآمد بسان نهنگ</p></div>
<div class="m2"><p>سوی گرزه گاوسر برد چنگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>غرابش برانگیخت و افراخت دست</p></div>
<div class="m2"><p>درآمد به حمله چو پیلان مست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سپر بر سر آورد اهرن چو باد</p></div>
<div class="m2"><p>ز شداد عادی بسی کرد یاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ازو خواست فیروزی رزمگاه</p></div>
<div class="m2"><p>نبود آگه از داور هور و ماه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که سام سپهبد برو بربتاخت</p></div>
<div class="m2"><p>چو شیران بدو بازوش برفراخت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یکی گرز زد بر سرش پهلوان</p></div>
<div class="m2"><p>که شد نرم بر دشت کین استخوان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بزد خویش را آنگهی بر سپاه</p></div>
<div class="m2"><p>جهان پیش شدادیان شد سیاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو گرگی که آید به سوی گله</p></div>
<div class="m2"><p>سر از تن همی کرد هر دم یله</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بیفکند کشته در آن کارزار</p></div>
<div class="m2"><p>به دشمن همه کار را کرد خوار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به هر سو عنان تکاور کشید</p></div>
<div class="m2"><p>چو مرغی همه دانها را بچید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به شداد بردند ازین آگهی</p></div>
<div class="m2"><p>که شد روی میدان ز اهرن تهی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سپاهت همه سام یکسر بکشت</p></div>
<div class="m2"><p>همه کار گردون بشد زو درشت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بلرزید از خشم شداد عاد</p></div>
<div class="m2"><p>چو کردند در پیشش این گفته باد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دگرباره ملاح گیتی‌سپهر</p></div>
<div class="m2"><p>به دریا روان ساخت کشتی مهر</p></div></div>