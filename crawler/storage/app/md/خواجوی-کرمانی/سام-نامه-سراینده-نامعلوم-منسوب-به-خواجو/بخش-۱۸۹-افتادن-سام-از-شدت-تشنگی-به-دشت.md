---
title: >-
    بخش ۱۸۹ - افتادن سام از شدت تشنگی به دشت
---
# بخش ۱۸۹ - افتادن سام از شدت تشنگی به دشت

<div class="b" id="bn1"><div class="m1"><p>چو لعل از خور کان برآورد سر</p></div>
<div class="m2"><p>ز زربفت کوه کمرکش کمر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شه مشرق از تیغ که تیغ زد</p></div>
<div class="m2"><p>سر تیغ بر جوشن میغ زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دشتی فتادند بس هولناک</p></div>
<div class="m2"><p>که از هول او دیو گشتی هلاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه پر ز شر بود پر شعله‌زار</p></div>
<div class="m2"><p>به جای گیا بسته بر نوک خار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخورده زمینش بجز آفتاب</p></div>
<div class="m2"><p>ندیده است ریگش یکی قطره آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سمومش چو آتش فروزان شده</p></div>
<div class="m2"><p>سمندر در آن دشت سوزان شده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمین تابه و ریگ سوزنده بود</p></div>
<div class="m2"><p>دل شعله در وی فروزنده بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان ریگ گرمش زمین تاب بود</p></div>
<div class="m2"><p>که نعل ستوران در آن آب بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فروماند از راه رفتن غراب</p></div>
<div class="m2"><p>تن پهلوان زیر خفتان کباب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیفتاد شاپور در ریک گرم</p></div>
<div class="m2"><p>ز گرمی بدرید بر تنش چرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همان گرد قلواد بیتاب شد</p></div>
<div class="m2"><p>به هر سو شتابان پی آب شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپهبد شگفتی در آن ره بماند</p></div>
<div class="m2"><p>جهان آفرین را فراوان بخواند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که ای دادگر ایزد دستگیر</p></div>
<div class="m2"><p>ازین دشت خونخواره‌ام دست‌گیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو دانی نهان من و آشکار</p></div>
<div class="m2"><p>که چونست احوال سام سوار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دلم پر ز دردست و جان پر الم</p></div>
<div class="m2"><p>رهم دور و تن زار و جانم دژم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه راهی که جانان به دست آورم</p></div>
<div class="m2"><p>نه شادی که بر غم شکست آورم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه نوشی که جان را درآرم به جای</p></div>
<div class="m2"><p>نه هوشی که تن را درآرم ز پای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شگفتی مرا درد پیش آمدست</p></div>
<div class="m2"><p>که رنجم ز هر بار بیش آمدست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفت و بیفتاد بر روی خاک</p></div>
<div class="m2"><p>دهان گشت از تشنگی چاک چاک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دل از جان شیرین روان برگرفت</p></div>
<div class="m2"><p>تو گفتی که آتش به جان برگرفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زمانی چو در خاک گریان بخفت</p></div>
<div class="m2"><p>بر آن ریگ تفتید بریان بخفت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که ناگاه بانگی برآمد بلند</p></div>
<div class="m2"><p>که ای نامور پهلو دیوبند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدین ره چرا آمدی پویه‌پوی</p></div>
<div class="m2"><p>ندانی که این رنجت آمد به روی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نیامد کسی سوی این راه سخت</p></div>
<div class="m2"><p>مگر آن که برگشت از فر و بخت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شگفتی بود راه شهر زنان</p></div>
<div class="m2"><p>نتابد کسی سوی این ره عنان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به مردان خطر دارد این راه شوم</p></div>
<div class="m2"><p>نزیبد به مردان این مرز و بوم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کنون چون گذارت بدین ره فتاد</p></div>
<div class="m2"><p>تو را هوش فرخنده در چه فتاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی پند ایدر ز من گوش کن</p></div>
<div class="m2"><p>مگردان ز گفتار با من سخن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یکی چشمه ساریست ایدر ز دور</p></div>
<div class="m2"><p>درو آب بینی همه تلخ و شور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به تیمار و اندوه بسته شوی</p></div>
<div class="m2"><p>چو خوردی از آن آب خسته شوی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دگر چشمه آب شیرین چو شهد</p></div>
<div class="m2"><p>نشیمنگه شاه خوبان عهد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که حورا شهش نام خوانی همی</p></div>
<div class="m2"><p>جز آن نام دیگر ندانی همی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همان دم در آن چشمه حاضر شوند</p></div>
<div class="m2"><p>به دیدار خوب تو ناظر شوند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رهائی نیابی به گیتی ازوی</p></div>
<div class="m2"><p>گرفتار گردی بدان ما‌ه‌روی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بمانی در آن شهر تا جاودان</p></div>
<div class="m2"><p>شوی زار و بیمار و تیره‌روان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو را گفتم ایدر تو دانی دگر</p></div>
<div class="m2"><p>ایا نامور سام پرخاشخر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو بشنید ازو سام بر پای خاست</p></div>
<div class="m2"><p>نگه کرد هر سوی بر چپ و راست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همان شمسه را دید سالار گرد</p></div>
<div class="m2"><p>که از بهر پهلو بسی رنج برد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بخندید ازو سام نیرم‌نژاد</p></div>
<div class="m2"><p>بدو گفت کای دلبر حورزاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مرا را بنمای بر چشمه سار</p></div>
<div class="m2"><p>ببین تا چه خواهد دگر کردگار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نشست از بر اسب گرد دلیر</p></div>
<div class="m2"><p>به قلواد و شاپور دو شیرگیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>روان شد سوی چشمه خوشگوار</p></div>
<div class="m2"><p>به همراه شمسه چو خرم بهار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو فرسخ برفتند از آن راه پنج</p></div>
<div class="m2"><p>ز گرما کشیدند بسیار رنج</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>رسیدند نزدیک چشمه فراز</p></div>
<div class="m2"><p>از آن آب شد شادمان رزم‌ساز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از آن آب گشتند تازه روان</p></div>
<div class="m2"><p>سر و تن بشست آن گو پهلوان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دگر شمسه گردید از آن ناپدید</p></div>
<div class="m2"><p>سپهدار از آب جامی کشید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که ناگاه برخاست از راه گرد</p></div>
<div class="m2"><p>چو گردی که گیتی همه تیره کرد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سپاهی برون آمد از تیره راه</p></div>
<div class="m2"><p>به پیش اندر آن پادشاهی چو ماه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سراسر زنان دید شیر ژیان</p></div>
<div class="m2"><p>همه تنگ بسته گرفته عنان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همه همچو طاوس آراسته</p></div>
<div class="m2"><p>به هر طوق پیرایه پیراسته</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو مردان همه ساز خفتان به بر</p></div>
<div class="m2"><p>همه ساز و اسباب یکسر ز زر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو سام نریمان بدیشان بدید</p></div>
<div class="m2"><p>شگفتی فروماند و دم در کشید</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>یکی نازنین دید چون تازه سرو</p></div>
<div class="m2"><p>لب لعل او همچو خون تذرو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دو نرگس چو دو جادوی سحرساز</p></div>
<div class="m2"><p>که کرده به هر سوی صد سحر ساز</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>رخی چون گل تازه در بوستان</p></div>
<div class="m2"><p>دهانش به کام دل دوستان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به رخسار آن ماه خال سیاه</p></div>
<div class="m2"><p>به مانند زنگی به شبهای ماه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>لبش شور در جان شکر زده</p></div>
<div class="m2"><p>نمک بر دل خستگان در زده</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>جهانی ملاحت به هم کرده جمع</p></div>
<div class="m2"><p>چو پروانه خوبان او گشته شمع</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همه نازنینان ساغر به دست</p></div>
<div class="m2"><p>چو چشم خوش خویش گردیده مست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به ناگاه مر سام را یافتند</p></div>
<div class="m2"><p>همان دم سوی گرد بشتافتند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>یکی نازنین اسب خود پیش راند</p></div>
<div class="m2"><p>بسی آفرین بر سپهبد بخواند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>که ای مرد فرخنده شاد آمدی</p></div>
<div class="m2"><p>بدین بوم و بر همچو باد آمدی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تو را باد فرخنده این آمدن</p></div>
<div class="m2"><p>بدین فر و بالا چنین آمدن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سپهبد ز جا خاست بنواختش</p></div>
<div class="m2"><p>بر مردم دیده جا ساختش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بپرسید کای نازنینان که‌اید</p></div>
<div class="m2"><p>بدین سان شتابان بگو از چه‌اید</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کجا می‌روید ای پری‌پیکران</p></div>
<div class="m2"><p>چه نام است سرکرده دلبران</p></div></div>